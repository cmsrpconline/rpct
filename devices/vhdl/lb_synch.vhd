library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;

use work.std_logic_1164_ktp.all;
use work.tridaq.all;
use work.lb_synch_iis.all;

entity lb_synch is
  port(
    data_in					:in    TSLV(LDEMUX_DATA_OUT-1 downto 0);
    data_out					:out   TSLV(LDEMUX_DATA_OUT-1 downto 0);
    -- fast signal interface
    win_open					:in    TSL;
    win_close					:in    TSL;
    clock					:in    TSL;
    l1a						:in    TSL;
    pretrg0					:in    TSL;
    pretrg1					:in    TSL;
    bcn0					:in    TSL;
    -- internal bus interface
    II_addr					:in    TSLV(IILB_ADDR_SIZE-1 downto 0);
    II_data					:inout TSLV(II_DATA_SIZE-1 downto 0);
    II_oper					:in    TSL;
    II_write					:in    TSL;
    II_save					:in    TSL;
    II_reset					:in    TSL;
    II_ack					:inout TSL;
    -- diagnostics signal
    led						:out   TSLV(5 downto 0)
  );
end lb_synch;

library lpm;
use lpm.lpm_components.all;

use work.vcomponent.all;
use work.LPMcomponent.all;
use work.LPMsynchro.all;
use work.LPMdiagnostics.all;

architecture behaviour of lb_synch is

  constant IIPar				:TVII := TVIICreate(VIIItemDeclList,IILB_ADDR_SIZE,II_DATA_SIZE);

  signal   IIVecInt,IIVecExt,IIVecEna		:TSLV(VII(IIPar)'high downto VEC_INDEX_MIN);
  signal   IIDataSig				:TSLV(II_DATA_SIZE-1 downto 0);
  signal   IIAckInvSig				:TSL;
  signal   LIIDataExport			:TSLV(II_DATA_SIZE-1 downto 0);
  signal   LIIDataExportEnable			:TSL;

  signal   ProcReqSig				:TSL;
  signal   ProcWinRateAckSig			:TSL;
  signal   ProcFullRateAckSig			:TSL;
  signal   ProcBxHistAckSig			:TSL;
  signal   ProcAckSig				:TSL;

  signal   HistTimerLimitSig			:TSLV(HIST_TIME_SIZE-1 downto 0);
  signal   HistTimerCountSig			:TSLV(HIST_TIME_SIZE-1 downto 0);
  signal   HistTimerStartSig			:TSL;
  signal   HistTimerStopSig			:TSL;
  signal   HistTimerClkEnaSig			:TSL;
  signal   TimerEngineClkEnaSig			:TSL;
  signal   TriggerEnaSelect			:TN;
  signal   TriggerSig				:TSL;
  signal   HistClkEnaSig			:TSL;
  --
  signal   RateWinMemWrSig			:TSL;
  signal   RateWinMemDataOutSig			:TSLV(II_DATA_SIZE-1 downto 0);
  signal   RateFullMemWrSig			:TSL;
  signal   RateFullMemDataOutSig		:TSLV(II_DATA_SIZE-1 downto 0);
  --
  signal   L, H					:TSL;
  signal   ClockInvSig				:TSL;
  signal   DataSynchSig				:TSLV(RATE_MEM_DATA_SIZE-1 downto 0);
  signal   DataSynchMaskSig			:TSLV(RATE_MEM_DATA_SIZE-1 downto 0);
  signal   DataSynchMaskDelSig			:TSLV(RATE_MEM_DATA_SIZE-1 downto 0);
  signal   DataSynchORSig			:TSL;
  signal   DataInReg				:TSLV(RATE_MEM_DATA_SIZE-1 downto 0);
  signal   DataInDelReg				:TSLV(RATE_MEM_DATA_SIZE-1 downto 0);
  signal   DataInPulseSig			:TSLV(RATE_MEM_DATA_SIZE-1 downto 0);
  signal   DataInPulseTrgSig			:TSLV(RATE_MEM_DATA_SIZE-1 downto 0);
  signal   DataOutPulseSig			:TSLV(RATE_MEM_DATA_SIZE-1 downto 0);
  signal   DataOutPulseDelSig			:TSLV(RATE_MEM_DATA_SIZE-1 downto 0);
  signal   DataWinDelaySig			:TSLV(DATA_DELAY_SIZE-1 downto 0);
  signal   DataPulseDelaySig			:TSLV(DATA_DELAY_SIZE-1 downto 0);
  --
  signal   L1AReg				:TSL;
  signal   BCN0Reg				:TSL;
  signal   Pretrg0Reg				:TSL;
  signal   Pretrg1Reg				:TSL;
  signal   LocalTrgReg				:TSL;

begin	  

  L <= '0'; H <= '1';
  ClockInvSig <= not(clock);

  winsyn: WINDOW_SYNCHRO
    generic map (
      LPM_DATA_WIDTH		=> RATE_MEM_DATA_SIZE,
      OUTPUT_REGISTERED		=> TRUE
    )
    port map(
      resetN			=> II_reset,
      win_open			=> win_open,
      win_close			=> win_close,
      clock_out			=> clock,
      data_in			=> data_in(RATE_MEM_DATA_SIZE-1 downto 0),
      data_out			=> DataSynchSig
    );

  DataSynchMaskSig <= DataSynchSig and IIConnGetWordData(IIVecInt,IIPar,WORD_CHAN_ENA,0);
  --data_out         <= TSLVresize(DataSynchMaskSig,LDEMUX_DATA_OUT,'0');
  
  DataWinDelaySig  <= IIConnGetWordData(IIVecInt,IIPar,WORD_WIN_DELAY,0);
  DataWinDelay: DPM_PIPE
    generic map (
      LPM_DATA_WIDTH		=> RATE_MEM_DATA_SIZE,
      LPM_DELAY_WIDTH		=> DATA_DELAY_SIZE

    )
    port map(
      resetN			=> II_reset,
      clock			=> clock,
      clk_ena			=> H,
      delay			=> DataWinDelaySig,
      data_in			=> DataSynchMaskSig,
      data_out			=> DataSynchMaskDelSig
    );
  
  DataSynchORSig   <= OR_REDUCE(DataSynchMaskDelSig);
  data_out         <= TSLVresize(DataSynchMaskDelSig,LDEMUX_DATA_OUT,'0');

  --
  -- input rate histogram
  --
  process(ii_reset, win_close) is
  begin
    if(ii_reset='0') then
      DataInReg    <= (others => '0');
      DataInDelReg <= (others => '0');
    elsif(win_close'event and win_close='1') then
      DataInReg    <= data_in(RATE_MEM_DATA_SIZE-1 downto 0);
      DataInDelReg <= DataInReg;
    end if;
  end process;

  DataInPulseSig <= not(DataInDelReg) and DataInReg;
  DataInPulseTrgSig(RATE_MEM_DATA_SIZE-2 downto 0) <= DataInPulseSig(RATE_MEM_DATA_SIZE-2 downto 0);
  DataInPulseTrgSig(RATE_MEM_DATA_SIZE-1) -->
    <= TriggerSig when (IIConnGetBitsData(IIVecInt,IIPar,BITS_STATUS_TRG_DAQ,0)="1") else
       DataInPulseSig(RATE_MEM_DATA_SIZE-1);

  clk_syn: CLOCK_SYNCHRO
    generic map (
      LPM_DATA_WIDTH		=> RATE_MEM_DATA_SIZE,
      INPUT_REGISTERED		=> FALSE,
      OUTPUT_REGISTERED		=> TRUE
    )
    port map (
      resetN			=> II_reset,
      clock_in			=> win_close,
      data_in			=> DataInPulseTrgSig,
      clock_out			=> clock,
      data_out			=> DataOutPulseSig
    );

  DataPulseDelaySig  <= IIConnGetWordData(IIVecInt,IIPar,WORD_PULSE_DELAY,0);
  DataPulseDelay: DPM_PIPE
    generic map (
      LPM_DATA_WIDTH		=> RATE_MEM_DATA_SIZE,
      LPM_DELAY_WIDTH		=> DATA_DELAY_SIZE

    )
    port map (
      resetN			=> II_reset,
      clock			=> clock,
      clk_ena			=> H,
      delay			=> DataPulseDelaySig,
      data_in			=> DataOutPulseSig,
      data_out			=> DataOutPulseDelSig
    );
  
  --
  -- timer engine
  --
  ProcReqSig <= TSLconv(IIConnGetBitsData(IIVecInt,IIPar,BITS_STATUS_PROC_REQ,0));
  ProcAckSig <= ProcFullRateAckSig and ProcFullRateAckSig;

  HistTimerClkEnaSig <= TSLconv(IIConnGetBitsData(IIVecInt,IIPar,BITS_STATUS_RATE_ENA,0));
  HistTimerStartSig  <= TSLconv(IIConnGetBitsData(IIVecInt,IIPar,BITS_STATUS_RATE_START,0));
  HistTimerLimitSig  <= IIConnGetWordData(IIVecInt,IIPar,WORD_HIST_TIME_LIMIT,0);

  HistTimer: LPM_TIMER_ENGINE 
    generic map (
      LPM_TIMER_SIZE	=> HIST_TIME_SIZE
    )
    port map (
      resetN		=> ii_reset,
      clock		=> clock,
      clk_ena_in	=> HistTimerClkEnaSig,
      clk_ena_out	=> TimerEngineClkEnaSig,
      start		=> HistTimerStartSig,
      stop		=> HistTimerStopSig,
      limit		=> HistTimerLimitSig,
      count		=> HistTimerCountSig
    );

  --
  -- rates
  --
  TriggerEnaSelect <= TNconv(IIConnGetBitsData(IIVecInt,IIPar,BITS_STATUS_TRG_SEL,0));
  TriggerSig       <= Pretrg0Reg when (TriggerEnaSelect=TRG_COND_SEL_PRETRG0) else
                      Pretrg1Reg when (TriggerEnaSelect=TRG_COND_SEL_PRETRG1) else
	              '1';
  HistClkEnaSig    <= (TriggerSig or not(TSLconv(IIConnGetBitsData(IIVecInt,IIPar,BITS_STATUS_TRG_RATE,0))))
                      and TimerEngineClkEnaSig;
  RateWinMemWrSig <= IIConnGetAreaWrite(IIVecEna,IIPar,AREA_MEM_WIN_RATE,ii_save);
  RateWinMem: RATE
    generic map(
      LPM_DATA_WIDTH	=> RATE_MEM_DATA_SIZE,
      LPM_COUNT_WIDTH	=> RATE_MEM_COUNT_SIZE,
      LPM_MDATA_WIDTH	=> II_DATA_SIZE
    )
    port map(
      resetN		=> ii_reset,
      clock		=> clock,
      data_in		=> DataSynchMaskDelSig,
      clk_ena		=> HistClkEnaSig,
      sim_loop		=> L,
      proc_req		=> ProcReqSig,
      proc_ack		=> ProcWinRateAckSig,
      memory_address_in	=> ii_addr(RATE_MEM_ADDR_SIZE-1 downto 0),
      memory_data_in	=> ii_data,
      memory_data_out	=> RateWinMemDataOutSig,
      memory_wr		=> RateWinMemWrSig
  );

  RateFullMemWrSig <= IIConnGetAreaWrite(IIVecEna,IIPar,AREA_MEM_FULL_RATE,ii_save);
  RateFullMem: RATE
    generic map(
      LPM_DATA_WIDTH	=> RATE_MEM_DATA_SIZE,
      LPM_COUNT_WIDTH	=> RATE_MEM_COUNT_SIZE,
      LPM_MDATA_WIDTH	=> II_DATA_SIZE
    )
    port map(
      resetN		=> ii_reset,
      clock		=> clock,
      data_in		=> DataOutPulseDelSig,
      clk_ena		=> HistClkEnaSig,
      sim_loop		=> L,
      proc_req		=> ProcReqSig,
      proc_ack		=> ProcFullRateAckSig,
      memory_address_in	=> ii_addr(RATE_MEM_ADDR_SIZE-1 downto 0),
      memory_data_in	=> ii_data,
      memory_data_out	=> RateFullMemDataOutSig,
      memory_wr		=> RateFullMemWrSig
  );

  --
  -- fast interface
  --
  process(ii_reset, clock) is
  begin
    if(ii_reset='0') then
      L1AReg <= '0';
      BCN0Reg <= '0';
      Pretrg0Reg <= '0';
      Pretrg1Reg <= '0';
    elsif(clock'event and clock='1') then
      L1AReg <= l1a;
      BCN0Reg <= bcn0;
      Pretrg0Reg <= pretrg0;
      Pretrg1Reg <= pretrg1;
    end if;
  end process;

  --
  -- internal interface
  --
  
  process(ii_reset, ii_save)
  begin
    if(ii_reset='0') then
      IIVecInt <= IIReset(IIVecInt,IIPar);
    elsif(ii_save'event and ii_save='1') then
      if(ii_oper='0') then
        IIVecInt <= IISaveAccess(IIVecInt,IIPar,ii_addr,IIDataSig);
      end if;
    end if;
  end process;
  --
  IIVecEna <= IIWriteEnable(IIVecInt,IIPar,ii_oper,ii_write,ii_addr,IIDataSig);

  IIVecExt <= (IIWriteAccess(IIVecInt,IIPar,ii_oper,ii_write,ii_addr,IIDataSig)
              or IIConnPutWordData(IIVecInt,IIPar,WORD_IDENTIFIER,0,TSLVconv(LS_IDENTIFIER,II_DATA_SIZE))
              or IIConnPutWordData(IIVecInt,IIPar,WORD_VERSION,0,TSLVconv(LS_VERSION,II_DATA_SIZE))
              or IIConnPutBitsData(IIVecInt,IIPar,BITS_STATUS_RATE_STOP,0,TSLVconv(HistTimerStopSig))
              or IIConnPutBitsData(IIVecInt,IIPar,BITS_STATUS_PROC_ACK,0,TSLVconv(ProcAckSig))
              or IIConnPutWordData(IIVecInt,IIPar,WORD_HIST_TIME_COUNT,0,HistTimerCountSig)

              or IIConnPutAreaData(IIVecInt,IIPar,AREA_MEM_WIN_RATE, RateWinMemDataOutSig)
              or IIConnPutAreaData(IIVecInt,IIPar,AREA_MEM_FULL_RATE, RateFullMemDataOutSig)
	    );
  LIIDataExport <= IIReadAccess(IIVecExt,IIPar,ii_oper,ii_write,ii_addr);
  --
  LIIDataExportEnable <= not(ii_oper) and ii_write;
  CIIData: ALTERA_BUSTRI
    generic map (LPM_WIDTH => II_DATA_SIZE)
    port map    (data_in   => LIIDataExport,
                 data_out  => IIDataSig,
                 data_tri  => ii_data,
                 ena       => LIIDataExportEnable);
  --
  IIAckInvSig <= not(ii_oper);
  CIIAck: ALTERA_TRI
    port map(data_in  => L, 
             data_tri => ii_ack,
             ena      => IIAckInvSig
    );

--  data_pulse: LPM_PULSE_GENER
--    generic map (
--      LPM_PULSE_WIDTH		=> LED_PULSE_WIDTH,
--      LPM_PULSE_INVERT		=> TRUE
--    )
--    port map (
--      resetN			=> II_reset,
--      clock			=> clock,
--      trigger			=> DataSynchORSig,
--      pulse_out			=> led(0)
--    );
 
  led_clock: LPM_CLOCK_PULSER
    generic map (
      LPM_DIVIDE_PAR		=> CLK_FREQ/LED_PULSE_FREQ,
      LPM_DATA_PULSE_WIDTH	=> LED_PULSE_WIDTH,
      LPM_PULSE_INVERT		=> TRUE	
    )
    port map (
      resetN			=> II_reset,
      clock			=> clock,
      pulse			=> led(3)
    );

  led(0) <= not(OR_REDUCE(DataInPulseTrgSig));
  led(1) <= not(DataSynchORSig);
  led(2) <= not(TriggerSig);
  --led(3) <= '0' when ((L1AReg=BCN0Reg) and (Pretrg0Reg=Pretrg1Reg)) else
  --          '1';

end behaviour;

