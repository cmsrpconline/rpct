/***********************************************************************\
*                                                                       *
* This file was created by Component Internal Interface Engine software *
*  Copyright(c) 2000-2006 by Krzysztof Pozniak (pozniak@ise.pw.edu.pl)  *
*                           All Rights Reserved.                        *
*                                                                       *
\***********************************************************************/

#ifndef CII_TB3_OPTO_CFG_TAB_H_
#define CII_TB3_OPTO_CFG_TAB_H_

#include <stdio.h>
#include <cii_lib.h>

const struct CCII_CONFIG_TABLE CII_TB3_OPTO[] = {
  {   0, "$TB3_OPTO",                    ITEM_TYPE_COMP,   16,   18,    1, INTERFACE_NA,     -1,   0,          -1, "TB3_OPTO"},
  {   0, "CHECK_SUM",                    ITEM_TYPE_IPAR,    0,    1,    1, INTERFACE_NA,      0,   0,   776256432, NULL},
  {   0, "WORD_CHECKSUM",                ITEM_TYPE_WORD,   16,    1,    1, INTERFACE_RO,      0,  -1,          -1, NULL},
  {   0, "WORD_BOARD",                   ITEM_TYPE_WORD,   16,    1,    1, INTERFACE_RO,      1,  -1,          -1, NULL},
  {   0, "WORD_IDENTIFIER",              ITEM_TYPE_WORD,   16,    1,    1, INTERFACE_RO,      2,  -1,          -1, NULL},
  {   0, "WORD_VERSION",                 ITEM_TYPE_WORD,   16,    1,    1, INTERFACE_RO,      3,  -1,          -1, NULL},
  {   0, "WORD_USER_REG1",               ITEM_TYPE_WORD,   16,    1,    1, INTERFACE_IR,      4,  -1,          -1, NULL},
  {   0, "WORD_USER_REG2",               ITEM_TYPE_WORD,   16,    1,    1, INTERFACE_IR,      5,  -1,          -1, NULL},
  {   0, "BITS_STATUS_PLL_UNLOCK",       ITEM_TYPE_BITS,    1,    1,    1, INTERFACE_RO,      6,   0,          -1, NULL},
  {   0, "BITS_STATUS_STROBE_UNLOCK",    ITEM_TYPE_BITS,    1,    1,    1, INTERFACE_RO,      6,   1,          -1, NULL},
  {   0, "BITS_STATUS_TTC_DATA_DELAY",   ITEM_TYPE_BITS,    3,    1,    1, INTERFACE_IR,      6,   2,          -1, NULL},
  {   0, "BITS_STATUS_TRG_DATA_SEL",     ITEM_TYPE_BITS,    3,    1,    1, INTERFACE_IR,      6,   5,          -1, NULL},
  {   0, "BITS_TEST_CNT_CLK40",          ITEM_TYPE_BITS,    4,    1,    1, INTERFACE_RO,      7,   0,          -1, NULL},
  {   0, "BITS_TEST_CNT_CLK80",          ITEM_TYPE_BITS,    4,    1,    1, INTERFACE_RO,      7,   4,          -1, NULL},
  {   0, "BITS_TEST_CNT_BCN0",           ITEM_TYPE_BITS,    4,    1,    1, INTERFACE_RO,      7,   8,          -1, NULL},
  {   0, "BITS_TLK_OPTO_SIG",            ITEM_TYPE_BITS,    3,    1,    1, INTERFACE_RO,      8,   0,          -1, NULL},
  {   0, "BITS_TLK_ENABLE",              ITEM_TYPE_BITS,    3,    1,    1, INTERFACE_IR,      8,   3,          -1, NULL},
  {   0, "BITS_TLK_LCK_REFN",            ITEM_TYPE_BITS,    3,    1,    1, INTERFACE_IR,      8,   6,          -1, NULL},
  {   0, "BITS_TLK_RX_ERROR",            ITEM_TYPE_BITS,    3,    1,    1, INTERFACE_RO,      8,   9,          -1, NULL},
  {   0, "BITS_TLK_RX_NO_DATA",          ITEM_TYPE_BITS,    3,    1,    1, INTERFACE_RO,      8,  12,          -1, NULL},
  {   0, "WORD_REC_FAST_DATA_DELAY",     ITEM_TYPE_WORD,    9,    1,    1, INTERFACE_IR,      9,  -1,          -1, NULL},
  {   0, "WORD_REC_DATA_DELAY",          ITEM_TYPE_WORD,   12,    1,    1, INTERFACE_IR,     10,  -1,          -1, NULL},
  {   0, "WORD_REC_CHECK_ENA",           ITEM_TYPE_WORD,    3,    1,    1, INTERFACE_IR,     11,  -1,          -1, NULL},
  {   0, "WORD_REC_CHECK_DATA_ENA",      ITEM_TYPE_WORD,    3,    1,    1, INTERFACE_IR,     12,  -1,          -1, NULL},
  {   0, "WORD_REC_TEST_ENA",            ITEM_TYPE_WORD,    3,    1,    1, INTERFACE_IR,     13,  -1,          -1, NULL},
  {   0, "WORD_REC_TEST_RND_ENA",        ITEM_TYPE_WORD,    3,    1,    1, INTERFACE_IR,     14,  -1,          -1, NULL},
  {   0, "WORD_REC_TEST_DATA",           ITEM_TYPE_WORD,   32,    3,    1, INTERFACE_RO,     15,  -1,          -1, NULL},
  {   0, "WORD_REC_TEST_OR_DATA",        ITEM_TYPE_WORD,    4,    3,    1, INTERFACE_RO,     21,  -1,          -1, NULL},
  {   0, "WORD_REC_ERROR_COUNT",         ITEM_TYPE_WORD,   16,    1,    1, INTERFACE_RW,     24,  -1,          -1, NULL},
  {   0, "WORD_SEND_CHECK_ENA",          ITEM_TYPE_WORD,    3,    1,    1, INTERFACE_IR,     25,  -1,          -1, NULL},
  {   0, "WORD_SEND_CHECK_DATA_ENA",     ITEM_TYPE_WORD,    3,    1,    1, INTERFACE_IR,     26,  -1,          -1, NULL},
  {   0, "WORD_SEND_TEST_ENA",           ITEM_TYPE_WORD,    3,    1,    1, INTERFACE_IR,     27,  -1,          -1, NULL},
  {   0, "WORD_SEND_TEST_RND_ENA",       ITEM_TYPE_WORD,    3,    1,    1, INTERFACE_IR,     28,  -1,          -1, NULL},
  {   0, "WORD_SEND_TEST_DATA",          ITEM_TYPE_WORD,   24,    3,    1, INTERFACE_IR,     29,  -1,          -1, NULL},
  {   0, "BITS_H0_CHAN_ENA",             ITEM_TYPE_BITS,    3,    1,    1, INTERFACE_IR,     35,   0,          -1, NULL},
  {   0, "BITS_H0_RND_ENA",              ITEM_TYPE_BITS,    3,    1,    1, INTERFACE_IR,     35,   3,          -1, NULL},
  {   0, "WORD_TTC_BCN",                 ITEM_TYPE_WORD,   12,    1,    1, INTERFACE_RO,     36,  -1,          -1, NULL},
  {   0, "WORD_TTC_EVN",                 ITEM_TYPE_WORD,   24,    1,    1, INTERFACE_RO,     37,  -1,          -1, NULL},
  {   0, "WORD_TTC_TEST_DATA",           ITEM_TYPE_WORD,    6,    1,    1, INTERFACE_RO,     39,  -1,          -1, NULL},
  {   0, "WORD_BCN0_DELAY",              ITEM_TYPE_WORD,    8,    1,    1, INTERFACE_IR,     40,  -1,          -1, NULL},
  {   0, "WORD_DATA_TRG_DELAY",          ITEM_TYPE_WORD,    8,    1,    1, INTERFACE_IR,     41,  -1,          -1, NULL},
  {   0, "BITS_DAQ_EMPTY",               ITEM_TYPE_BITS,    1,    1,    1, INTERFACE_RO,     42,   0,          -1, NULL},
  {   0, "BITS_DAQ_EMPTY_ACK",           ITEM_TYPE_BITS,    1,    1,    1, INTERFACE_RO,     42,   1,          -1, NULL},
  {   0, "BITS_DAQ_LOST",                ITEM_TYPE_BITS,    1,    1,    1, INTERFACE_RO,     42,   2,          -1, NULL},
  {   0, "BITS_DAQ_LOST_ACK",            ITEM_TYPE_BITS,    1,    1,    1, INTERFACE_RO,     42,   3,          -1, NULL},
  {   0, "BITS_DAQ_TIMER_START",         ITEM_TYPE_BITS,    1,    1,    1, INTERFACE_IR,     42,   4,          -1, NULL},
  {   0, "BITS_DAQ_TIMER_STOP",          ITEM_TYPE_BITS,    1,    1,    1, INTERFACE_RO,     42,   5,          -1, NULL},
  {   0, "BITS_DAQ_TIMER_LOC_ENA",       ITEM_TYPE_BITS,    1,    1,    1, INTERFACE_IR,     42,   6,          -1, NULL},
  {   0, "BITS_DAQ_TIMER_TRIG_SEL",      ITEM_TYPE_BITS,    3,    1,    1, INTERFACE_IR,     42,   7,          -1, NULL},
  {   0, "BITS_DAQ_PROC_REQ",            ITEM_TYPE_BITS,    1,    1,    1, INTERFACE_IR,     42,  10,          -1, NULL},
  {   0, "BITS_DAQ_PROC_ACK",            ITEM_TYPE_BITS,    1,    1,    1, INTERFACE_RO,     42,  11,          -1, NULL},
  {   0, "BITS_DAQ_NON_EMPTY_ENA",       ITEM_TYPE_BITS,    1,    1,    1, INTERFACE_IR,     42,  12,          -1, NULL},
  {   0, "BITS_DAQ_WR_ADDR",             ITEM_TYPE_BITS,    7,    1,    1, INTERFACE_RO,     43,   0,          -1, NULL},
  {   0, "BITS_DAQ_WR_ACK",              ITEM_TYPE_BITS,    1,    1,    1, INTERFACE_RO,     43,   7,          -1, NULL},
  {   0, "BITS_DAQ_RD_ADDR",             ITEM_TYPE_BITS,    7,    1,    1, INTERFACE_RW,     44,   0,          -1, NULL},
  {   0, "BITS_DAQ_RD_ACK",              ITEM_TYPE_BITS,    1,    1,    1, INTERFACE_RO,     44,   7,          -1, NULL},
  {   0, "WORD_DAQ_TIMER_LIMIT",         ITEM_TYPE_WORD,   40,    1,    1, INTERFACE_IR,     45,  -1,          -1, NULL},
  {   0, "WORD_DAQ_TIMER_COUNT",         ITEM_TYPE_WORD,   40,    1,    1, INTERFACE_RO,     48,  -1,          -1, NULL},
  {   0, "WORD_DAQ_TIMER_TRG_DELAY",     ITEM_TYPE_WORD,    8,    1,    1, INTERFACE_IR,     51,  -1,          -1, NULL},
  {   0, "WORD_DAQ_DATA_DELAY",          ITEM_TYPE_WORD,    8,    1,    1, INTERFACE_IR,     52,  -1,          -1, NULL},
  {   0, "WORD_DAQ_MASK",                ITEM_TYPE_WORD,    8,    1,    1, INTERFACE_IR,     53,  -1,          -1, NULL},
  {   0, "BITS_PULSER_OUT_ENA",          ITEM_TYPE_BITS,    1,    1,    1, INTERFACE_IR,     54,   0,          -1, NULL},
  {   0, "BITS_PULSER_LOOP_ENA",         ITEM_TYPE_BITS,    1,    1,    1, INTERFACE_IR,     54,   1,          -1, NULL},
  {   0, "BITS_PULSER_TIMER_START",      ITEM_TYPE_BITS,    1,    1,    1, INTERFACE_IR,     54,   2,          -1, NULL},
  {   0, "BITS_PULSER_TIMER_STOP",       ITEM_TYPE_BITS,    1,    1,    1, INTERFACE_RO,     54,   3,          -1, NULL},
  {   0, "BITS_PULSER_TIMER_LOC_ENA",    ITEM_TYPE_BITS,    1,    1,    1, INTERFACE_IR,     54,   4,          -1, NULL},
  {   0, "BITS_PULSER_TIMER_TRIG_SEL",   ITEM_TYPE_BITS,    3,    1,    1, INTERFACE_IR,     54,   5,          -1, NULL},
  {   0, "BITS_PULSER_REPEAT_ENA",       ITEM_TYPE_BITS,    1,    1,    1, INTERFACE_IR,     54,   8,          -1, NULL},
  {   0, "BITS_PULSER_PROC_REQ",         ITEM_TYPE_BITS,    1,    1,    1, INTERFACE_IR,     54,   9,          -1, NULL},
  {   0, "BITS_PULSER_PROC_ACK",         ITEM_TYPE_BITS,    1,    1,    1, INTERFACE_RO,     54,  10,          -1, NULL},
  {   0, "WORD_PULSER_LENGTH",           ITEM_TYPE_WORD,    7,    1,    1, INTERFACE_IR,     55,  -1,          -1, NULL},
  {   0, "WORD_PULSER_TIMER_LIMIT",      ITEM_TYPE_WORD,   40,    1,    1, INTERFACE_IR,     56,  -1,          -1, NULL},
  {   0, "WORD_PULSER_TIMER_COUNT",      ITEM_TYPE_WORD,   40,    1,    1, INTERFACE_RO,     59,  -1,          -1, NULL},
  {   0, "WORD_PULSER_TIMER_TRG_DELAY",  ITEM_TYPE_WORD,    8,    1,    1, INTERFACE_IR,     62,  -1,          -1, NULL},
  {   0, "AREA_MEM_DAQ_DIAG",            ITEM_TYPE_AREA,  112,  128,    1, INTERFACE_RW,   1024,  -1,          -1, NULL},
  {   0, "AREA_MEM_PULSE",               ITEM_TYPE_AREA,   57,  128,    1, INTERFACE_RW,   2048,  -1,          -1, NULL},
  {  -1, NULL                            ITEM_TYPE_COMP,   -1,   -1,   -1, INTERFACE_NA,     -1,  -1,           0, NULL}
};

#endif /*CII_TB3_OPTO_CFG_TAB_H_*/

