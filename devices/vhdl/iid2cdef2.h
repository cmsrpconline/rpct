
   
#define pow2(id) (1 << (id))

#undef IIDEC_IDEN_VAL
#define IIDEC_IDEN_VAL(id,type,value,comment) \
  const type IID_CLASS :: id = value ;
#undef IIDEC_IDEN_ENU
#define IIDEC_IDEN_ENU(id,type,comment) \
  const type IID_CLASS :: id = __LINE__ ;
#undef IIDEC_COM_LINE
#define IIDEC_COM_LINE(text)
#undef IIDEC_ITEM_BEG
#define IIDEC_ITEM_BEG(type,id,width,num,parent,write,read,name,function,descr)
#undef IIDEC_ITEM_CON
#define IIDEC_ITEM_CON(type,id,width,num,parent,write,read,name,function,descr)
#undef IIDEC_ITEM_END
#define IIDEC_ITEM_END(type,id,width,num,parent,write,read,name,function,descr)

#include IID_FILE
             
#undef IID_FILE
#undef IID_CLASS


#undef pow2
