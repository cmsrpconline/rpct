
   
#define pow2(id) (1 << (id))

#undef IIDEC_IDEN_VAL
#define IIDEC_IDEN_VAL(id,type,value,comment) \
  const type IID_CLASS :: id = value ;
#undef IIDEC_IDEN_ENU
#define IIDEC_IDEN_ENU(id,type,comment) \
  const type IID_CLASS :: id = __LINE__ ;
#undef IIDEC_COM_LINE
#define IIDEC_COM_LINE(text)
#undef IIDEC_ITEM_BEG
#define IIDEC_ITEM_BEG(type,id,width,num,parent,write,read,name,function,descr)
#undef IIDEC_ITEM_CON
#define IIDEC_ITEM_CON(type,id,width,num,parent,write,read,name,function,descr)
#undef IIDEC_ITEM_END
#define IIDEC_ITEM_END(type,id,width,num,parent,write,read,name,function,descr)

#include IID_FILE


TMemoryMap* IID_CLASS ::BuildMemoryMap()
{
  TMemoryMap::TDeclItems items;

#undef IIDEC_IDEN_VAL
#define IIDEC_IDEN_VAL(id,type,value,comment)
#undef IIDEC_IDEN_ENU
#define IIDEC_IDEN_ENU(id,type,comment)
#undef IIDEC_COM_LINE
#define IIDEC_COM_LINE(text)
#undef IIDEC_ITEM_BEG
#define IIDEC_ITEM_BEG(type,id,width,num,parent,write,read,name,function,descr) \
items.push_back( TIIDevice::createTVIIItemDecl( type , IID_CLASS :: id , width , num , IID_CLASS :: parent , write , read ,  name, function , descr ) );
#undef IIDEC_ITEM_CON
#define IIDEC_ITEM_CON(type,id,width,num,parent,write,read,name,function,descr) \
IIDEC_ITEM_BEG( type , id , width , num , parent , write , read , name , function , descr)
#undef IIDEC_ITEM_END
#define IIDEC_ITEM_END(type,id,width,num,parent,write,read,name,function,descr) \
IIDEC_ITEM_BEG( type , id , width , num , parent , write , read , name , function , descr)

#include IID_FILE


    return new TMemoryMap(items, II_ADDR_SIZE, II_DATA_SIZE);
}


TMemoryMap::TDeclItems* IID_CLASS ::GetDeclItems()
{                              
  TMemoryMap::TDeclItems* result = new TMemoryMap::TDeclItems();
  TMemoryMap::TDeclItems& items = *result;

#undef IIDEC_IDEN_VAL
#define IIDEC_IDEN_VAL(id,type,value,comment)
#undef IIDEC_IDEN_ENU
#define IIDEC_IDEN_ENU(id,type,comment)
#undef IIDEC_COM_LINE
#define IIDEC_COM_LINE(text)
#undef IIDEC_ITEM_BEG
#define IIDEC_ITEM_BEG(type,id,width,num,parent,write,read,name,function,descr) \
items.push_back( TIIDevice::createTVIIItemDecl( type , IID_CLASS :: id , width , num , IID_CLASS :: parent , write , read ,  name, function , descr ) );
#undef IIDEC_ITEM_CON
#define IIDEC_ITEM_CON(type,id,width,num,parent,write,read,name,function,descr) \
IIDEC_ITEM_BEG( type , id , width , num , parent , write , read , name , function , descr)
#undef IIDEC_ITEM_END
#define IIDEC_ITEM_END(type,id,width,num,parent,write,read,name,function,descr) \
IIDEC_ITEM_BEG( type , id , width , num , parent , write , read , name , function , descr)

#include IID_FILE

  return result;
}


#undef IID_FILE
#undef IID_CLASS


#undef pow2
