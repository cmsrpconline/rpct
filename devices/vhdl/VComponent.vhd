library ieee;
use ieee.std_logic_1164.all;
use work.std_logic_1164_ktp.all;

package VComponent is
  
  -------------------------------------------------------------------
  -- VPipeline
  -------------------------------------------------------------------
  type TVPipe
    is record
      length				:TVL;
      width				:TVL;
    end record;
  function TVPipeCreate(length, width :TVL) return TVPipe;
  function VPipe(par :TVPipe) return TSLV;
  function VPipePosOut(vec :TSLV; par :TVPipe; pos: TVI) return TSLV;
  function VPipeLasTPOut(vec :TSLV; par :TVPipe) return TSLV;
  function VPipeStep(vec :TSLV; par :TVPipe; clock_ena :TSL; d: TSLV) return TSLV;

  -------------------------------------------------------------------
  -- VFifo
  -------------------------------------------------------------------
  type TVFifoMode is (TVFIFO_NOREG, TVFIFO_REG, TVFIFO_BUF);
  type TVFifo
    is record
      length				:TVL;
      width				:TVL;
      mode				:TVFifoMode;
      \pipe_par\			:TVPipe;
      \empty\				:TVR;
      \pipe\				:TVR;
      \cnt\				:TVR;
      \buf\				:TVR;
      \empty_out\			:TVI;
      \full_out\			:TVI;
      \end\				:TVI;
    end record;
  function TVFifoCreate(length, width :TVL; mode :TVFifoMode)  return TVFifo;
  function VFifo(vec :TSLV; par :TVFifo; pos: TVI) return TSLV;
  function VFifo(vec :TSLV; par :TVFifo) return TSLV;
  function VFifo(par :TVFifo; empty_vec :TSLV) return TSLV;
  function VFifo(par :TVFifo) return TSLV;
  function VFifoStep(vec :TSLV; par :TVFifo; put, get :TSL; d: TSLV) return TSLV;
  function VFifoEmpty(vec :TSLV; par :TVFifo) return TSL;
  function VFifoFull(vec :TSLV; par :TVFifo) return TSL;

  -------------------------------------------------------------------
  -- VCounter
  -------------------------------------------------------------------

  type TVCountMode is (TVCOUNT_PROG, TVCOUNT_STATIC, TVCOUNT_MAX, TVCOUNT_LOOP);
  type TVCountBody is (TVCOUNT_ALL, TVCOUNT_PROC, TVCOUNT_RANGE, TVCOUNT_CNT, TVCOUNT_OVER);
  type TVCount
    is record
      modules				:TN;
      width				:TVL;
      mode				:TVCountMode;
      \cnt_size\			:TVL;
      \range_pos\			:TVI;
      \cnt_pos\				:TVI;
      \over_pos\			:TVI;
    end record;
  
  function TVCountCreate(mode :TVCountMode; modules :TN; width :TVL)  return TVCount;
  function VCount(vec :TSLV; par :TVCount; proc :TVCountBody)  return TSLV;
  function VCount(par :TVCount; proc :TVCountBody)  return TSLV;
  function VCountStep(vec :TSLV; par :TVCount; clock_ena :TSL; static_range :TSLV) return TSLV;
  function VCountStep(vec :TSLV; par :TVCount; clock_ena :TSL) return TSLV;
  function VCountRange(vec :TSLV; par :TVCount; range_val :TSLV) return TSLV;


-------------------------------------------------------------------
-- Internal Interface (II)
-------------------------------------------------------------------

  type TVIIItemType  is (
    VII_PAGE,
    VII_AREA,
    VII_WORD,
    VII_VECT,
    VII_BITS
  );
  
  type TVIIItemWrType is (
    VII_WNOACCESS,
    VII_WACCESS
  );
  
  type TVIIItemRdType is (
    VII_RNOACCESS,
    VII_REXTERNAL,
    VII_RINTERNAL
  );

  constant VII_ITEM_NAME_LEN		:TP := 32;
  constant VII_ITEM_DESCR_LEN		:TP := 64;
  type TVIIItemFun is (
    VII_FUN_UNDEF,
    VII_FUN_HIST,
    VII_FUN_RATE
  );

  type TVIIItemDecl is record
    ItemType				:TVIIItemType;
    ItemID				:TN;
    ItemWidth				:TVL;
    ItemNumber				:TN;
    ItemParentID			:TN;
    ItemWrType				:TVIIItemWrType;
    ItemRdType				:TVIIItemRdType;
    ItemName				:TS(VII_ITEM_NAME_LEN downto 1);
    ItemFun				:TVIIItemFun; -- HIST, COUNT, UNDEF
    ItemDescr				:TS(VII_ITEM_DESCR_LEN downto 1);
  end record;
  type TVIIItemDeclList is array (TN range<>) of TVIIItemDecl;

  type TVIIItem is record
    ItemType				:TVIIItemType;
    ItemID				:TN;
    ItemParentID			:TVI;
    ItemWidth				:TVL;
    ItemNumber				:TN;
    ItemWrType				:TVIIItemWrType;
    ItemWrPos				:TVI;
    ItemRdType				:TVIIItemRdType;
    ItemRdPos				:TVI;
    ItemAddrPos				:TVI;
    ItemAddrLen				:TVL;
  end record;
  type TVII is array (TN range<>) of TVIIItem;

  --function \_TVIIItemListGen\(list :TVIIItemDeclList; addr_width, data_width, -->
  --  page_num, item_num, vect_num :TN) -->
  --  return TVII;
  function VIINameConv(name :TS) return TS;
  function VIIDescrConv(name :TS) return TS;
  function TVIICreate(list :TVIIItemDeclList; addr_width, data_width :TVL) return TVII;
  function VII(par :TVII) return TSLV;
  function IIReset(vec :TSLV; par :TVII) return TSLV;
  function IISaveAccess(vec :TSLV; par :TVII; addr, data_in :TSLV) return TSLV;
  function IIWriteAccess(vec :TSLV; par :TVII; enableN, WriteN :TSL; addr, data_in :TSLV) return TSLV;
  function IIWriteEnable(vec :TSLV; par :TVII; enableN, WriteN :TSL; addr, data_in :TSLV) return TSLV;
  function IIReadAccess(vec :TSLV; par :TVII; enableN, WriteN :TSL; addr :TSLV) return TSLV;

  function IIConnPutWordData(vec :TSLV; par :TVII; item_id :TN; pos :TVI; data_in :TSLV) return TSLV;
  function IIConnGetWordData(vec :TSLV; par :TVII; item_id :TN; pos :TVI) return TSLV;

  function IIConnPutBitsData(vec :TSLV; par :TVII; item_id :TN; pos :TVI; data_in :TSLV) return TSLV;
  function IIConnSetBitsData(vec :TSLV; par :TVII; item_id :TN; pos :TVI; data_in :TSLV) return TSLV;
  function IIConnGetBitsData(vec :TSLV; par :TVII; item_id :TN; pos :TVI) return TSLV;
  function IIConnGetBitsWrite(vec :TSLV; par :TVII; item_id :TN; writeN :TSL) return TSL;
  function IIConnGetBitsEnable(vec :TSLV; par :TVII; item_id :TN) return TSL;

  function IIConnPutAreaData(vec :TSLV; par :TVII; item_id :TN; data_in :TSLV) return TSLV;
  function IIConnGetAreaWrite(vec :TSLV; par :TVII; item_id :TN; writeN :TSL) return TSL;
  function IIConnGetAreaEnable(vec :TSLV; par :TVII; item_id :TN) return TSL;

end VComponent;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
use work.std_logic_1164_ktp.all;

package body VComponent is

  -------------------------------------------------------------------
  -- VPipeline
  -------------------------------------------------------------------
  function TVPipeCreate(length, width :TVL)  return TVPipe is
    variable par :TVPipe;
  begin
    par.length := length;
    par.width := width;
    return(par);
  end function;
  
  -------------------------------------------------------------------

  function VPipe(par :TVPipe) return TSLV is
  begin
    return(A2DInitArea(par.length,par.width,'0'));
  end function;

  -------------------------------------------------------------------

  function VPipePosOut(vec :TSLV; par :TVPipe; pos: TVI) return TSLV is
  begin
    return(SLVNorm(A2DGetRow(vec,par.length,par.width,pos)));
  end function;
  
  -------------------------------------------------------------------

  function VPipeLasTPOut(vec :TSLV; par :TVPipe) return TSLV is
  begin
    return(SLVNorm(A2DGetLastRow(vec,par.length,par.width)));
  end function;
  
  -------------------------------------------------------------------

  function VPipeStep(vec :TSLV; par :TVPipe; clock_ena :TSL; d: TSLV) return TSLV is
    variable v :TSLV(vec'length-1 downto VEC_INDEX_MIN);
  begin
    v := vec;
    if(clock_ena='1') then
      for step in par.length-1 downto 1 loop
        v := A2DSetRow(v,par.length,par.width,step,A2DGetRow(v,par.length,par.width,step-1));
      end loop;
      v := A2DSetRow(v,par.length,par.width,VEC_INDEX_MIN,d);
    end if;
    return(v);
  end function;


  -------------------------------------------------------------------
  -- VFifo
  -------------------------------------------------------------------

  function \_VFifoCntVal\(vec :TSLV; par :TVFifo) return TVI is
  begin
    return(TNconv(TSLVResize(vec,par.\cnt\)));
  end function;
  
  -------------------------------------------------------------------

  function TVFifoCreate(length, width :TVL; mode :TVFifoMode)  return TVFifo is
    variable par :TVFifo;
  begin
    par.length := length;
    par.width := width;
    par.mode := mode;
    par.\pipe_par\    := TVPipeCreate(length, width);
    par.\empty\.r     := VEC_INDEX_MIN;
    par.\empty\.l     := par.\empty\.r  +par.width-1;
    par.\pipe\.r      := par.\empty\.l  +1;
    par.\pipe\.l      := par.\pipe\.r   +VPipe(par.\pipe_par\)'length-1;
    par.\cnt\.r       := par.\pipe\.l   +1;
    par.\cnt\.l       := par.\cnt\.r    +TVLcreate(length)-1;
    if(par.mode=TVFIFO_NOREG)then
      par.\buf\.r     := NO_VEC_INDEX;
      par.\buf\.l     := NO_VEC_INDEX;
      par.\empty_out\ := NO_VEC_INDEX;
      par.\full_out\  := NO_VEC_INDEX;
      par.\end\       := par.\cnt\.l;
    else
      par.\buf\.r     := par.\cnt\.l    +1;
      par.\buf\.l     := par.\buf\.r    +par.width-1;
      par.\empty_out\ := par.\buf\.l    +1;
      par.\full_out\  := par.\empty_out\+1;
      par.\end\       := par.\full_out\;
    end if;
    return(par);
  end function;
  
  -------------------------------------------------------------------

  function VFifo(vec :TSLV; par :TVFifo; pos: TVI) return TSLV is
  begin
    for index in 1 to par.length loop
      if (index=pos) then
        return(VPipePosOut(TSLVResize(vec,par.\pipe\),par.\pipe_par\,index-1));
      end if;
    end loop;
    return(TSLVResize(vec,par.\empty\));
  end function;
  
  -------------------------------------------------------------------

  function VFifo(vec :TSLV; par :TVFifo) return TSLV is
  begin
    if(par.mode=TVFIFO_NOREG) then
      return(VFifo(vec,par,\_VFifoCntVal\(vec,par)));
    else
      return(TSLVResize(vec,par.\buf\));
    end if;
  end function;
  
  -------------------------------------------------------------------

  function VFifo(par :TVFifo; empty_vec :TSLV) return TSLV is
    variable v :TSLV(VFifo(par)'high downto VEC_INDEX_MIN);
  begin
    v := (others=>'0');
    TSLVput(v,par.\empty\,TSLVResize(empty_vec,par.\empty\,empty_vec(empty_vec'low)));
    TSLVput(v,TSLVResize(v,par.\pipe\),VPipe(par.\pipe_par\));
    return(v);
  end function;

  -------------------------------------------------------------------

  function VFifo(par :TVFifo) return TSLV is
  begin
    return(TSLVconv(TSL'('0'),par.\end\+1));
  end function;

  -------------------------------------------------------------------

  function VFifoStep(vec :TSLV; par :TVFifo; put, get :TSL; d: TSLV) return TSLV is
    variable v :TSLV(vec'length-1 downto VEC_INDEX_MIN);
    variable cnt :TVI;

  begin
    v := vec;
    cnt := \_VFifoCntVal\(vec,par);
    
    if(par.mode=TVFIFO_BUF) then
      TSLVput(v,par.\buf\,VFifo(v,par,cnt));
      v(par.\empty_out\) := TSLconv(cnt=VEC_INDEX_MIN);
      v(par.\full_out\) := TSLconv(cnt=par.length);
    end if;
    
    if(get='1' and cnt > VEC_INDEX_MIN) then
      cnt := cnt-1;
    end if;
    if(par.mode=TVFIFO_REG) then
      TSLVput(v,par.\buf\,VFifo(v,par,cnt));
    end if;
    if(put='1' and cnt < par.length) then
      cnt := cnt+1;
      TSLVput(v,par.\pipe\,VPipeStep(TSLVResize(vec,par.\pipe\),par.\pipe_par\,'1',d));
    end if;
    
    TSLVput(v,par.\cnt\,TSLVconv(cnt,TSLVResize(vec,par.\cnt\)'length));
    if(par.mode=TVFIFO_REG) then
      v(par.\empty_out\) := TSLconv(cnt=VEC_INDEX_MIN);
      v(par.\full_out\) := TSLconv(cnt=par.length);
    end if;
    return(v);
  end function;
  
  -------------------------------------------------------------------

  function VFifoEmpty(vec :TSLV; par :TVFifo) return TSL is
  begin
    if(par.mode=TVFIFO_NOREG) then
      return(TSLconv(\_VFifoCntVal\(vec,par)=VEC_INDEX_MIN));
    else
      return(vec(par.\empty_out\));
    end if;
  end function;

  -------------------------------------------------------------------

  function VFifoFull(vec :TSLV; par :TVFifo) return TSL is
  begin
    if(par.mode=TVFIFO_NOREG) then
      return(TSLconv(\_VFifoCntVal\(vec,par)=par.length));
    else
      return(vec(par.\full_out\));
    end if;
  end function;

  -------------------------------------------------------------------
  -- VCounter
  -------------------------------------------------------------------
  function TVCountCreate(mode :TVCountMode; modules :TN; width :TVL) return TVCount is
    variable par :TVCount;
  begin
    par.mode := mode;
    par.modules := modules;
    par.width := width;
    par.\cnt_size\ := par.modules*par.width;
    par.\range_pos\ := VEC_INDEX_MIN;
    case mode is
      when TVCOUNT_PROG =>
        par.\cnt_pos\ := par.\cnt_size\;
      when TVCOUNT_STATIC
         | TVCOUNT_MAX
         | TVCOUNT_LOOP =>
        par.\cnt_pos\ := VEC_INDEX_MIN;
    end case;
    par.\over_pos\ := par.\cnt_pos\+par.\cnt_size\;
    return(par);
  end function;
  
  -------------------------------------------------------------------

  function VCount(vec :TSLV; par :TVCount; proc :TVCountBody)  return TSLV is
  begin
    case proc is
      when TVCOUNT_PROC =>
        return(vec(par.\over_pos\ downto par.\cnt_pos\));
      when TVCOUNT_RANGE =>
        return(vec(par.\range_pos\+par.\cnt_size\-1 downto par.\range_pos\));
      when TVCOUNT_CNT =>
        return(vec(par.\cnt_pos\+par.\cnt_size\-1 downto par.\cnt_pos\));
      when TVCOUNT_OVER =>
        return(vec(par.\over_pos\ downto par.\over_pos\));
      when others =>
        null;
    end case;
    return(vec);
  end function;
  
  -------------------------------------------------------------------

  function VCount(par :TVCount; proc :TVCountBody)  return TSLV is
  begin
    return(VCount(TSLVconv(TSL'('0'),par.\over_pos\+1),par,proc));
  end function;
  
  -------------------------------------------------------------------

  function VCountStep(vec :TSLV; par :TVCount; clock_ena :TSL; static_range :TSLV) return TSLV is
    constant ad				:TA2D := (par.modules,par.width);
    variable v :TSLV(vec'length-1 downto VEC_INDEX_MIN);
    variable cnt :TSLV(VCount(par,TVCOUNT_CNT)'length-1 downto VEC_INDEX_MIN);
    variable carry :TSL;
  begin
    v := vec;
    cnt := VCount(v,par,TVCOUNT_CNT);
    if (clock_ena='1') then
      if((par.mode /= TVCOUNT_LOOP)
         and
         (
           ((par.mode = TVCOUNT_PROG) and (cnt = VCount(v,par,TVCOUNT_RANGE)))
           or
           ((par.mode = TVCOUNT_STATIC) and (cnt = static_range))
           or
           ((par.mode = TVCOUNT_MAX) and (AND_REDUCE(cnt) = '1'))
         )
        )
      then
        v(VCount(par,TVCOUNT_OVER)'high) := '1';
      else
        for index in 0 to par.modules-1 loop
          carry := AND_REDUCE(A2DGetRow(cnt,ad,index));
          cnt := A2DSetRow(cnt,ad,index,A2DGetRow(cnt,ad,index)+1);
          exit when (carry = '0');
        end loop;            
        v(VCount(par,TVCOUNT_OVER)'high) := '0';
      end if;
    end if;
    TSLVput(v,VCount(par,TVCOUNT_CNT),cnt);
    return(VCount(v,par,TVCOUNT_PROC));
  end function;
  
  -------------------------------------------------------------------

  function VCountStep(vec :TSLV; par :TVCount; clock_ena :TSL) return TSLV is
    variable stat_range :TSLV(VCount(vec,par,TVCOUNT_CNT)'length-1 downto VEC_INDEX_MIN);
  begin
    if (par.mode = TVCOUNT_PROG) then
      stat_range := VCount(vec,par,TVCOUNT_RANGE);
    else
      stat_range := (others => '0');
    end if;
    return(VCountStep(vec,par,clock_ena,stat_range));
  end function;  
  
  -------------------------------------------------------------------

  function VCountRange(vec :TSLV; par :TVCount; range_val: TSLV) return TSLV is
    variable v :TSLV(vec'length-1 downto VEC_INDEX_MIN);
  begin
    v := vec;
    if(par.mode=TVCOUNT_PROG) then
      TSLVput(v,VCount(par,TVCOUNT_RANGE),range_val);
    end if;
    return(VCount(v,par,TVCOUNT_RANGE));
  end function;


-------------------------------------------------------------------
-- Internal Interface (II)
-------------------------------------------------------------------

  function \_TVIIItemListGen\(list :TVIIItemDeclList; addr_width, data_width, -->
                              page_num, item_num, vect_num :TN)             return TVII is
    variable par :TVII(0 to item_num);
    variable par_index :TVI;
    variable page_vec :TVIV(0 to page_num-1);
    variable page_len_vec :TVIV(0 to page_num-1);
    variable vect_vec :TVIV(0 to maximum(vect_num,1)-1);
    variable vect_width :TVIV(vect_vec'range);
    variable vect_addr :TVIV(vect_vec'range);
    variable page_cnt :TVI;
    variable vect_cnt :TVI;
    variable item_id :TN;
  begin

    -- physical items (without pages) setting
    par_index := VEC_INDEX_MIN;
    for index in list'range loop
      case list(index).ItemType is
        when VII_AREA | VII_WORD | VII_BITS =>
          par(par_index).ItemType     :=list(index).ItemType;
          par(par_index).ItemID       :=list(index).ItemID;
          par(par_index).ItemParentID :=list(index).ItemParentID;
          par(par_index).ItemWidth    :=list(index).ItemWidth;
          par(par_index).ItemNumber   :=list(index).ItemNumber;
          par(par_index).ItemWrType   :=list(index).ItemWrType;
          par(par_index).ItemRdType   :=list(index).ItemRdType;
          par(par_index).ItemWrPos    :=NO_VEC_INDEX;
          par(par_index).ItemRdPos    :=NO_VEC_INDEX;
          par(par_index).ItemAddrPos  :=NO_VEC_INDEX;
          par(par_index).ItemAddrLen  :=NO_VEC_LEN;
          par_index := par_index +1;
        when VII_PAGE | VII_VECT =>
          null;
      end case;
    end loop;
    -- interconnections for vectors
    if(vect_num>0) then
      vect_cnt := VEC_INDEX_MIN;
      for index in list'range loop
        case list(index).ItemType is
          when VII_VECT =>
            vect_vec(vect_cnt)   := index;
            vect_width(vect_cnt) := VEC_INDEX_MIN;
	    vect_addr(vect_cnt)  := NO_VEC_INDEX;
            vect_cnt             := vect_cnt+1;
          when others =>
            null;
        end case;
      end loop;
      for vect_index in vect_vec'range loop
        item_id := list(vect_vec(vect_index)).ItemID;
        for index in 0 to par'length-2 loop
          case par(index).ItemType is
            when VII_BITS =>
              if (par(index).ItemParentID=item_id) then
                par(index).ItemAddrLen := par(index).ItemNumber * par(index).ItemWidth;
                if(par(index).ItemAddrLen<=data_width) then
                  par(index).ItemParentID := vect_index;
                  par(index).ItemAddrLen := par(index).ItemAddrLen+vect_width(vect_index)-1;
                  if ((vect_width(vect_index)/data_width)/=(par(index).ItemAddrLen/data_width)) then
                    vect_width(vect_index) := (par(index).ItemAddrLen/data_width)*data_width;
		  end if;
                  par(index).ItemAddrLen := vect_width(vect_index);
                  vect_width(vect_index) := vect_width(vect_index) + par(index).ItemNumber*par(index).ItemWidth;
		else
                  par(index).ItemParentID := NO_VEC_INDEX; -- ! can't divide common bits
                end if;
              end if;
            when others =>
              null;
          end case;
        end loop;
      end loop;
    end if;
    -- interconnection for pages;
    page_len_vec := (others => VEC_INDEX_MIN);
    page_cnt := VEC_INDEX_MIN;
    for index in list'range loop
      case list(index).ItemType is
        when VII_PAGE =>
          page_vec(page_cnt) := index;
          page_cnt := page_cnt+1;
        when others =>
          null;
      end case;
    end loop;
    for page_index in page_vec'range loop
      item_id := list(page_vec(page_index)).ItemID;
      for index in 0 to par'length-2 loop
        if (par(index).ItemAddrPos=NO_VEC_INDEX) then
          case par(index).ItemType is
            when VII_WORD =>
              if (par(index).ItemParentID=item_id) then
                par(index).ItemAddrPos := page_len_vec(page_index);
                par(index).ItemAddrLen := (par(index).ItemWidth-1)/data_width+1;
                page_len_vec(page_index) :=  page_len_vec(page_index)+par(index).ItemAddrLen*par(index).ItemNumber;
                par(index).ItemParentID := page_index;
              end if;
            when VII_BITS =>
              if (list(vect_vec(par(index).ItemParentID)).ItemParentID=item_id) then
	        if(vect_addr(par(index).ItemParentID)=NO_VEC_INDEX) then
                  vect_addr(par(index).ItemParentID) := page_len_vec(page_index);
                  page_len_vec(page_index) :=  page_len_vec(page_index)+((vect_width(par(index).ItemParentID)-1)/data_width)+1;
	        end if;
                par(index).ItemAddrPos  := vect_addr(par(index).ItemParentID)+(par(index).ItemAddrLen/data_width);
                par(index).ItemAddrLen  := par(index).ItemAddrLen-(par(index).ItemAddrLen/data_width)*data_width;
                par(index).ItemParentID := page_index;
	      end if;
            when VII_AREA =>
              if (par(index).ItemParentID=item_id) then
-- KP           par(index).ItemAddrLen := SLVMax(par(index).ItemNumber)+1;
                par(index).ItemAddrLen := (par(index).ItemWidth-1)/data_width+1; --KP
                par(index).ItemNumber := (SLVMax(par(index).ItemNumber-1)+1); --KP
--KP            par(index).ItemAddrPos := (((page_len_vec(page_index)-1)/par(index).ItemAddrLen)+1) -->
--KP                                      * par(index).ItemAddrLen;
                par(index).ItemAddrPos := ((page_len_vec(page_index)+(par(index).ItemNumber*par(index).ItemAddrLen)-1) -->
		                          /(par(index).ItemNumber*par(index).ItemAddrLen)) * par(index).ItemNumber; --KP
                page_len_vec(page_index) :=  page_len_vec(page_index)+(par(index).ItemNumber*par(index).ItemAddrLen);
                -- !! important: area can't be divided on parts
--KP            par(index).ItemAddrLen := 1;
                par(index).ItemParentID := page_index;
	      end if;
            when others =>
              null;
          end case;
        end if;
      end loop;
    end loop;
    -- calculates external addresses for each page
    page_cnt := 2**TVLcreate(maximum(page_len_vec)-1);
    for page_index in page_vec'range loop
      par_index := page_cnt * page_index;
      for index in 0 to par'length-2 loop
        case par(index).ItemType is
          when VII_AREA | VII_WORD | VII_BITS =>
            if (par(index).ItemParentID=page_index) then
              par(index).ItemAddrPos := par(index).ItemAddrPos + par_index;
            end if;
          when others =>
            null;
        end case;
      end loop;
    end loop;
    -- calculates positions in vector and vector lenhth
    par_index := VEC_INDEX_MIN;
    for index in 0 to par'length-2 loop
      case par(index).ItemType is
        when VII_WORD | VII_BITS =>
          if (par(index).ItemWrType=VII_WACCESS) then
            par(index).ItemWrPos := par_index;
            par_index := par_index + par(index).ItemNumber * par(index).ItemWidth;
          end if;
          case par(index).ItemRdType is
            when VII_RINTERNAL =>
              par(index).ItemRdPos := par(index).ItemWrPos;
            when VII_REXTERNAL =>
              par(index).ItemRdPos := par_index;
              par_index := par_index + par(index).ItemNumber * par(index).ItemWidth;
            when others =>
              null;
          end case;
        when VII_AREA =>
          if (par(index).ItemWrType=VII_WACCESS) then
            par(index).ItemWrPos := par_index;
--KP        par_index := par_index + par(index).ItemWidth;
            par_index := par_index + minimum(par(index).ItemWidth,data_width); --KP
          end if;
          case par(index).ItemRdType is
            when VII_REXTERNAL =>
              par(index).ItemRdPos := par_index;
--KP          par_index := par_index + par(index).ItemWidth;
              par_index := par_index + minimum(par(index).ItemWidth,data_width); --KP
            when others =>
              null;
          end case;
        when others =>
          null;
      end case;
    end loop;
    -- special last page stores global parametres
    par(par'length-1).ItemType := VII_PAGE;
    par(par'length-1).ItemWidth := data_width;
    par(par'length-1).ItemNumber := addr_width;
    par(par'length-1).ItemAddrPos := par_index;
    return(par);
  end;

  function VIINameConv(name :TS) return TS is
    variable var: TS(VII_ITEM_NAME_LEN downto 1);
  begin
    var := (others => ' ');
    if(name'length>0) then
      for index in name'range loop
        var(index) := name(index);
      end loop;
    end if;
    return (var);
  end;

  function VIIDescrConv(name :TS) return TS is
    variable var: TS(VII_ITEM_DESCR_LEN downto 1);
  begin
    var := (others => ' ');
    if(name'length>0) then
      for index in name'range loop
        var(index) := name(index);
      end loop;
    end if;
    return (var);
  end;

  function TVIICreate(list :TVIIItemDeclList; addr_width, data_width :TVL) return TVII is
    variable page_num :TN;
    variable item_num :TN;
    variable vect_num :TN;
  begin
    page_num :=0;
    item_num :=0;
    vect_num :=0;
    for index in list'range loop
      case list(index).ItemType is
        when VII_PAGE =>
          page_num := page_num+1;
        when VII_AREA | VII_WORD | VII_BITS =>
          item_num := item_num+1;
        when VII_VECT =>
          vect_num := vect_num+1;
        when others =>
          null;
      end case;
    end loop;
    return(\_TVIIItemListGen\(list,addr_width,data_width,page_num,item_num,vect_num));
  end;
  
  function VII(par :TVII) return TSLV is
    variable vec :TSLV(par(par'length-1).ItemAddrPos-1 downto 0);
  begin
    vec := (others => '0');
    return(vec);
  end;
  
  function \IIVecInit\(vec :TSLV; par :TVII; reset_mode :TL) return TSLV is
    variable len :TVI;
    variable outvec :TSLV(vec'range);
  begin
    if (reset_mode=FALSE) then
      outvec := (others =>'0');
    end if;
    for index in 0 to par'length-2 loop
      case par(index).ItemType is
        when VII_WORD | VII_BITS =>
          len := (par(index).ItemNumber * par(index).ItemWidth)-1;
          case par(index).ItemRdType is
            when VII_RINTERNAL =>
              if (reset_mode=TRUE) then
                for pos in par(index).ItemWrPos+len downto par(index).ItemWrPos loop
                  outvec(pos) := '0';
                end loop;
              else
                for pos in par(index).ItemWrPos+len downto par(index).ItemWrPos loop
                  outvec(pos) := vec(pos);
                end loop;
              end if;
            when others =>
              null;
          end case;
        when others =>
          null;
      end case;
    end loop;
    return(outvec);
  end function;

  function IIReset(vec :TSLV; par :TVII) return TSLV is
  begin
    return(\IIVecInit\(vec,par,TRUE));
  end function;

  function \_IIWriteAccess\(vec :TSLV; par :TVII; enableN, WriteN :TSL; addr,data_in :TSLV; save_mode :TL) return TSLV is
    variable VecRange :TVR;
    variable BitRange :TVR;
    variable val_addr :TVI;
    variable base_addr :TVI;
    variable vec_pos :TVI;
    variable LocVec :TSLV(vec'range);
  begin
    if (save_mode=TRUE) then
      LocVec := vec;
    else
      LocVec := \IIVecInit\(vec,par,FALSE);
    end if;
    if(enableN='0' and WriteN='0') then
      VecRange.l := NO_VEC_INDEX; VecRange.r := NO_VEC_INDEX;
      val_addr := TNconv(addr);
      for index in 0 to par'length-2 loop
        if((par(index).ItemWrPos/=NO_VEC_INDEX) and (par(index).ItemWrType=VII_WACCESS) -->
           and (  ((save_mode=FALSE) and (par(index).ItemRdType=VII_REXTERNAL)) -->
               or ((save_mode=TRUE)  and (par(index).ItemRdType=VII_RINTERNAL)))) then
          case par(index).ItemType is
            when VII_WORD =>
              for pos in VEC_INDEX_MIN to par(index).ItemNumber-1 loop            
                base_addr := par(index).ItemAddrPos+pos*par(index).ItemAddrLen;
                if ((val_addr>=base_addr) and (val_addr<(base_addr+par(index).ItemAddrLen))) then
                  for part in VEC_INDEX_MIN to par(index).ItemAddrLen-1 loop
                    if (base_addr + part = val_addr) then
                      vec_pos := par(index).ItemWrPos;
                      VecRange.r := vec_pos+pos*par(index).ItemWidth + part*par(par'length-1).ItemWidth;
                      VecRange.l := minimum(VecRange.r+par(index).ItemWidth, -->
                                    VecRange.r+par(par'length-1).ItemWidth);
                      VecRange.l := minimum(VecRange.l, -->
                                    vec_pos+par(index).ItemNumber*par(index).ItemWidth)-1;
                      TSLVput(LocVec,VecRange,TSLVResize(data_in,TVLcreate(VecRange),'1'));
                      return(LocVec);
                    end if;
                  end loop;
                  return(LocVec);
                end if;
              end loop;
            when VII_BITS =>
              if (val_addr=par(index).ItemAddrPos) then
                VecRange.r := par(index).ItemWrPos;
                VecRange.l := VecRange.r+par(index).ItemWidth*par(index).ItemNumber-1;
                BitRange.r := par(index).ItemAddrLen;
                BitRange.l := par(index).ItemAddrLen+par(index).ItemWidth*par(index).ItemNumber-1;
                TSLVput(LocVec,VecRange,TSLVResize(data_in,BitRange));
              end if;
--KP        when VII_AREA =>
--KP          if ((val_addr>=par(index).ItemAddrPos) and (val_addr<(par(index).ItemAddrPos+par(index).ItemNumber))) then
--KP            VecRange.r :=par(index).ItemWrPos;
--KP            VecRange.l := minimum(VecRange.r+par(index).ItemWidth, -->
--KP                          VecRange.r+par(par'length-1).ItemWidth)-1;
--KP            TSLVput(LocVec,VecRange,TSLVResize(data_in,TVLcreate(VecRange),'1'));
--KP            return(LocVec);
--KP          end if;
            when VII_AREA => --KP caly ponizszy case !!!
              for part in VEC_INDEX_MIN to par(index).ItemAddrLen-1 loop            
                base_addr := par(index).ItemAddrPos+part*par(index).ItemNumber;
                if ((val_addr>=base_addr) and (val_addr<(base_addr+par(index).ItemNumber))) then
                  VecRange.r := par(index).ItemWrPos;
                  VecRange.l := minimum(VecRange.r+par(index).ItemWidth, -->
                                        VecRange.r+par(par'length-1).ItemWidth)-1;
                  TSLVput(LocVec,VecRange,TSLVResize(data_in,TVLcreate(VecRange),'1'));
                  return(LocVec);
                end if;
              end loop;
            when others =>
              null;
          end case;
	end if;
      end loop;
    end if;
    return(LocVec);
  end function;

  function IISaveAccess(vec :TSLV; par :TVII; addr,data_in :TSLV) return TSLV is
  begin
    return(\_IIWriteAccess\(vec,par,'0','0',addr,data_in,TRUE));
  end function;

  function IIWriteAccess(vec :TSLV; par :TVII; enableN, WriteN :TSL; addr,data_in :TSLV) return TSLV is
  begin
    return(\_IIWriteAccess\(vec,par,enableN,WriteN,addr,data_in,FALSE));
  end function;

  function IIWriteEnable(vec :TSLV; par :TVII; enableN, WriteN :TSL; addr,data_in :TSLV) return TSLV is
    variable VecRange :TVR;
    variable val_addr :TVI;
    variable base_addr :TVI;
    variable vec_pos :TVI;
    variable LocVec :TSLV(vec'range);
  begin
    LocVec := (others => '0');
    if(enableN='0' and WriteN='0') then
      VecRange.l := NO_VEC_INDEX; VecRange.r := NO_VEC_INDEX;
      val_addr := TNconv(addr);
      for index in 0 to par'length-2 loop
        if(par(index).ItemWrPos/=NO_VEC_INDEX and par(index).ItemRdType=VII_REXTERNAL) then
          case par(index).ItemType is
            when VII_WORD =>
              for pos in VEC_INDEX_MIN to par(index).ItemNumber-1 loop            
                base_addr := par(index).ItemAddrPos+pos*par(index).ItemAddrLen;
                if ((val_addr>=base_addr) and (val_addr<(base_addr+par(index).ItemAddrLen))) then
                  for part in VEC_INDEX_MIN to par(index).ItemAddrLen-1 loop
                    if (base_addr + part = val_addr) then
                      vec_pos := par(index).ItemWrPos;
                      VecRange.r := vec_pos+pos*par(index).ItemWidth + part*par(par'length-1).ItemWidth;
                      VecRange.l := minimum(VecRange.r+par(index).ItemWidth, -->
                                    VecRange.r+par(par'length-1).ItemWidth);
                      VecRange.l := minimum(VecRange.l, -->
                                    vec_pos+par(index).ItemNumber*par(index).ItemWidth)-1;
                      TSLVput(LocVec,TSLVnew(VecRange,'1'));
                      return(LocVec);
                    end if;
                  end loop;
                  return(LocVec);
                end if;
              end loop;
            when VII_BITS =>
              if (val_addr=par(index).ItemAddrPos) then
                VecRange.r :=par(index).ItemWrPos;
                LocVec(VecRange.r) := '1';
              end if;
            when VII_AREA =>
--KP          if ((val_addr>=par(index).ItemAddrPos) and (val_addr<(par(index).ItemAddrPos+par(index).ItemNumber))) then
              if ((val_addr>=par(index).ItemAddrPos) --> --KP
	      and (val_addr<(par(index).ItemAddrPos+par(index).ItemNumber*par(index).ItemAddrLen))) then --KP
                VecRange.r :=par(index).ItemWrPos;
                LocVec(VecRange.r) := '1';
                return(LocVec);
              end if;
            when others =>
              null;
          end case;
	end if;
      end loop;
    end if;
    return(LocVec);
  end function;

  function IIReadAccess(vec :TSLV; par :TVII; enableN, WriteN :TSL; addr :TSLV) return TSLV is 
    variable VecRange :TVR;
    variable BitRange :TVR;
    variable val_addr :TVI;
    variable base_addr :TVI;
    variable vec_pos :TVI;
    variable data_tmp :TSLV(par(par'length-1).ItemWidth-1 downto 0);
  begin
    VecRange.l := NO_VEC_INDEX; VecRange.r := NO_VEC_INDEX;
    val_addr := TNconv(addr);
    data_tmp := (others => '1');
    if(enableN='0' and writeN='1') then
      for index in 0 to par'length-2 loop
        if (par(index).ItemRdPos/=NO_VEC_INDEX) then
          case par(index).ItemType is
            when VII_WORD =>
              for pos in VEC_INDEX_MIN to par(index).ItemNumber-1 loop            
                base_addr := par(index).ItemAddrPos+pos*par(index).ItemAddrLen;
                if ((val_addr>=base_addr) and (val_addr<(base_addr+par(index).ItemAddrLen))) then
                  for part in VEC_INDEX_MIN to par(index).ItemAddrLen-1 loop
                    if (base_addr + part = val_addr) then
                      vec_pos := par(index).ItemRdPos;
                      VecRange.r := vec_pos+pos*par(index).ItemWidth + part*par(par'length-1).ItemWidth;
                      VecRange.l := minimum(VecRange.r+par(index).ItemWidth, -->
                                    VecRange.r+par(par'length-1).ItemWidth);
                      VecRange.l := minimum(VecRange.l, -->
                                    vec_pos+par(index).ItemNumber*par(index).ItemWidth)-1;
                      data_tmp := TSLVResize(SLVNorm(TSLVResize(vec,VecRange)),data_tmp,'1');
                      return(data_tmp);
                    end if;
                  end loop;
                  return(data_tmp);
                end if;
              end loop;
            when VII_BITS =>
              if (val_addr=par(index).ItemAddrPos) then
                VecRange.r := par(index).ItemRdPos;
                VecRange.l := VecRange.r+par(index).ItemWidth*par(index).ItemNumber-1;
                BitRange.r := par(index).ItemAddrLen;
                BitRange.l := par(index).ItemAddrLen+par(index).ItemWidth*par(index).ItemNumber-1;
                TSLVput(data_tmp,BitRange,TSLVResize(vec,VecRange));
              end if;
--KP        when VII_AREA =>
--KP          if ((val_addr>=par(index).ItemAddrPos) and (val_addr<(par(index).ItemAddrPos+par(index).ItemNumber))) then
--KP            VecRange.r :=par(index).ItemRdPos;
--KP            VecRange.l := minimum(VecRange.r+par(index).ItemWidth, -->
--KP                          VecRange.r+par(par'length-1).ItemWidth)-1;
--KP            data_tmp := TSLVResize(SLVNorm(TSLVResize(vec,VecRange)),data_tmp,'1');
--KP            return(data_tmp);
--KP          end if;
            when VII_AREA => --KP caly ponizszy case !!!
              for part in VEC_INDEX_MIN to par(index).ItemAddrLen-1 loop            
                base_addr := par(index).ItemAddrPos+part*par(index).ItemNumber;
                if ((val_addr>=base_addr) and (val_addr<(base_addr+par(index).ItemNumber))) then
                  VecRange.r := par(index).ItemRdPos; 
                  VecRange.l := minimum(VecRange.r+par(index).ItemWidth, -->
                                        VecRange.r+par(par'length-1).ItemWidth)-1;
                  data_tmp := TSLVResize(SLVNorm(TSLVResize(vec,VecRange)),data_tmp,'1');
                  return(data_tmp);
                end if;
              end loop;
            when others =>
              null;
          end case;
	end if;
      end loop;
    end if;
    return(data_tmp);
  end function;

  function IIConnPutWordData(vec :TSLV; par :TVII; item_id :TN; pos :TVI; data_in :TSLV) return TSLV is 
    variable VecRange :TVR;
    variable LocVec :TSLV(vec'range);
  begin
    LocVec := (others => '0');
    for index in 0 to par'length-2 loop
      if(par(index).ItemID=item_id and par(index).ItemType=VII_WORD and par(index).ItemRdType=VII_REXTERNAL) then
        VecRange.r := par(index).ItemRdPos+pos*par(index).ItemWidth;
        VecRange.l := VecRange.r+par(index).ItemWidth-1;
        TSLVput(LocVec,VecRange,data_in);
      end if;
    end loop;
    return(LocVec);
  end function;

  function IIConnGetWordData(vec :TSLV; par :TVII; item_id :TN; pos :TVI) return TSLV is 
    variable VecRange :TVR;
  begin
    VecRange := (NO_VEC_INDEX, NO_VEC_INDEX);
    for index in 0 to par'length-2 loop
      if(par(index).ItemID=item_id and par(index).ItemType=VII_WORD and par(index).ItemWrType=VII_WACCESS) then
        VecRange.r := par(index).ItemWrPos+pos*par(index).ItemWidth;
        VecRange.l := VecRange.r+par(index).ItemWidth-1;
        return(SLVNorm(TSLVResize(vec,VecRange)));
      end if;
    end loop;
    return(TSLVResize(vec,VecRange));
  end function;


  function IIConnPutBitsData(vec :TSLV; par :TVII; item_id :TN; pos :TVI; data_in :TSLV) return TSLV is 
    variable VecRange :TVR;
    variable LocVec :TSLV(vec'range);
  begin
    LocVec := (others => '0');
    for index in 0 to par'length-2 loop
      if(par(index).ItemID=item_id and par(index).ItemType=VII_BITS and par(index).ItemRdType=VII_REXTERNAL) then
        VecRange.r := par(index).ItemRdPos+pos*par(index).ItemWidth;
        VecRange.l := VecRange.r+par(index).ItemWidth-1;
        TSLVput(LocVec,VecRange,data_in);
      end if;
    end loop;
    return(LocVec);
  end function;

  function IIConnSetBitsData(vec :TSLV; par :TVII; item_id :TN; pos :TVI; data_in :TSLV) return TSLV is 
    variable VecRange :TVR;
    variable LocVec :TSLV(vec'range);
  begin
    LocVec := (others => '0');
    for index in 0 to par'length-2 loop
      if(par(index).ItemID=item_id and par(index).ItemType=VII_BITS and par(index).ItemRdType=VII_RINTERNAL) then
        VecRange.r := par(index).ItemWrPos+pos*par(index).ItemWidth;
        VecRange.l := VecRange.r+par(index).ItemWidth-1;
        TSLVput(LocVec,VecRange,data_in);
      end if;
    end loop;
    return(LocVec);
  end function;

  function IIConnGetBitsData(vec :TSLV; par :TVII; item_id :TN; pos :TVI) return TSLV is 
    variable VecRange :TVR;
  begin
    VecRange := (NO_VEC_INDEX, NO_VEC_INDEX);
    for index in 0 to par'length-2 loop
      if(par(index).ItemID=item_id and par(index).ItemType=VII_BITS and par(index).ItemWrType=VII_WACCESS) then
        VecRange.r := par(index).ItemWrPos+pos*par(index).ItemWidth;
        VecRange.l := VecRange.r+par(index).ItemWidth-1;
        return(SLVNorm(TSLVResize(vec,VecRange)));
      end if;
    end loop;
    return(TSLVResize(vec,VecRange));
  end function;

  function IIConnGetBitsWrite(vec :TSLV; par :TVII; item_id :TN; writeN :TSL) return TSL is 
  begin
    for index in 0 to par'length-2 loop
      if(par(index).ItemID=item_id and par(index).ItemType=VII_BITS and par(index).ItemWrType=VII_WACCESS) then
        return(vec(par(index).ItemWrPos) and (not writeN));
      end if;
    end loop;
    return('0');
  end function;

  function IIConnGetBitsEnable(vec :TSLV; par :TVII; item_id :TN) return TSL is 
  begin
    return(IIConnGetBitsWrite(vec,par,item_id,'0'));
  end function;

  function IIConnPutAreaData(vec :TSLV; par :TVII; item_id :TN; data_in :TSLV) return TSLV is 
    variable VecRange :TVR;
    variable LocVec :TSLV(vec'range);
  begin
    LocVec := (others => '0');
    for index in 0 to par'length-2 loop
      if(par(index).ItemID=item_id and par(index).ItemType=VII_AREA and par(index).ItemRdType=VII_REXTERNAL) then
        VecRange.r := par(index).ItemRdPos;
--KP    VecRange.l := VecRange.r+par(index).ItemWidth-1;
        VecRange.l := minimum(VecRange.r+par(index).ItemWidth, --> --KP
                              VecRange.r+par(par'length-1).ItemWidth)-1; --KP
        TSLVput(LocVec,VecRange,data_in);
      end if;
    end loop;
    return(LocVec);
  end function;

  function IIConnGetAreaWrite(vec :TSLV; par :TVII; item_id :TN; writeN :TSL) return TSL is 
  begin
    for index in 0 to par'length-2 loop
      if(par(index).ItemID=item_id and par(index).ItemType=VII_AREA and par(index).ItemWrType=VII_WACCESS) then
        return(vec(par(index).ItemWrPos) and (not writeN));
      end if;
    end loop;
    return('0');
  end function;

  function IIConnGetAreaEnable(vec :TSLV; par :TVII; item_id :TN) return TSL is 
  begin
    return(IIConnGetAreaWrite(vec,par,item_id,'0'));
  end function;

end VComponent;


-------------------------------------------------------------------
-- VPipeline
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.std_logic_1164_ktp.all;

entity CVPipe is
  generic(
    constant LPM_DATA_SIZE		:TVL := 8;
    constant LPM_DATA_STEP		:TVL := 4
  );
  port(
    clock				:in  TSL;
    clock_ena				:in  TSL;
    resetN				:in  TSL;
    data_in				:in  TSLV(LPM_DATA_SIZE-1 downto VEC_INDEX_MIN);
    data_out				:out TSLV(LPM_DATA_SIZE-1 downto VEC_INDEX_MIN)
  );
end CVPipe;

library ieee;
use ieee.std_logic_1164.all;
use work.std_logic_1164_ktp.all;
use work.VComponent.all;

architecture behaviour of CVPipe is

  constant par				:TVPipe := TVPipeCreate(LPM_DATA_STEP,LPM_DATA_SIZE);
  signal   vec				:TSLV(VPipe(par)'high downto VEC_INDEX_MIN);

begin

  process (resetN,clock) is
  begin
    if (resetN='0') then
      vec <= VPipe(par);
    elsif (clock'event and clock='1') then
      vec <= VPipeStep(vec,par,clock_ena,data_in);
    end if;
  end process;

  data_out <=VPipeLasTPOut(vec,par);
  
end behaviour;


-------------------------------------------------------------------
-- VFifo
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.std_logic_1164_ktp.all;
use work.VComponent.all;

entity CVFifo is
  generic(
    constant LPM_DATA_SIZE		:TVL := 4;
    constant LPM_DATA_STEP		:TVL := 8;
    constant LPM_MODE			:TVFifoMode := TVFIFO_NOREG;
    constant LPM_EMPTY_DATA		:TSLV := "1"
  );
  port(
    clock				:in  TSL;
    resetN				:in  TSL;
    put					:in  TSL;
    get					:in  TSL;
    data_in				:in  TSLV(LPM_DATA_SIZE-1 downto VEC_INDEX_MIN);
    data_out				:out TSLV(LPM_DATA_SIZE-1 downto VEC_INDEX_MIN);
    empty				:out TSL;
    full				:out TSL
  );
end CVFifo;

library ieee;
use ieee.std_logic_1164.all;
use work.std_logic_1164_ktp.all;
use work.VComponent.all;

architecture behaviour of CVFifo is

  constant par				:TVFifo := TVFifoCreate(LPM_DATA_STEP,LPM_DATA_SIZE,LPM_MODE);
  signal   vec				:TSLV(VFifo(par)'high downto VEC_INDEX_MIN);

begin

  process (resetN,clock) is
  begin
    if (resetN='0') then
      vec <= VFifo(par,LPM_EMPTY_DATA);
    elsif (clock'event and clock='1') then
      vec <= VFifoStep(vec,par,put,get,data_in);
    end if;
  end process;

  data_out <= VFifo(vec,par);
  empty <= VFifoEmpty(vec,par);
  full <= VFifoFull(vec,par);
  
end behaviour;


-------------------------------------------------------------------
-- VCounter
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.std_logic_1164_ktp.all;
use work.VComponent.all;

entity CVCount is
  generic (
    constant LPM_WIDTH			:in TVI := 2;
    constant LPM_MODULES		:in TN := 3;
    constant LPM_COUNT_MODE		:in TVCountMode := TVCOUNT_PROG;
    constant LPM_COUNT_MAX		:in THV := "2E"
  );
  port(
    clock				:in  TSL;
    clock_ena				:in  TSL;
    resetN				:in  TSL;
    range_val				:in  TSLV((LPM_MODULES*LPM_WIDTH-1) downto VEC_INDEX_MIN);
    range_write				:in  TSL;
    data_cnt				:out TSLV(LPM_MODULES*LPM_WIDTH-1 downto VEC_INDEX_MIN);
    over_cnt				:out TSL
  );
end CVCount;

library ieee;
use ieee.std_logic_1164.all;
use work.std_logic_1164_ktp.all;
use work.VComponent.all;

architecture behaviour of CVCount is

  constant par				:TVCount := TVCountCreate(LPM_COUNT_MODE,LPM_MODULES,LPM_WIDTH);
  signal   vec				:TSLV(VCount(par,TVCOUNT_ALL)'high downto VEC_INDEX_MIN);

begin
        
  process (resetN,clock) is
  begin
    if (resetN='0') then
      TSLVputS(vec,VCount(par,TVCOUNT_PROC));
    elsif (clock'event and clock='1') then
      TSLVputS(vec,VCountStep(vec,par,clock_ena,TSLVconv(LPM_COUNT_MAX,VCount(par,TVCOUNT_CNT))));
    end if;
  end process;

  lab1:
  if(par.mode=TVCOUNT_PROG) generate
    process (resetN,range_write) is
    begin
      if (resetN='0') then
        TSLVputS(vec,VCount(par,TVCOUNT_RANGE));
      elsif (range_write'event and range_write='1') then
        TSLVputS(vec,VCountRange(vec,par,range_val));
      end if;
    end process;
  end generate;

  data_cnt <= VCount(vec,par,TVCOUNT_CNT);
  over_cnt <= vec(VCount(par,TVCOUNT_OVER)'high);
  
end behaviour;


-------------------------------------------------------------------
-- Internal Interface (II)
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.std_logic_1164_ktp.all;
use work.VComponent.all;

entity CVII is
  generic (
    constant LPM_ADDR_WIDTH		:in TVL := 4;
    constant LPM_DATA_WIDTH		:in TVL := 4;
    constant LPM_TEST_WIDTH		:in TVL := 8
  );
  port(
    resetN				:in  TSL;
    enableN				:in  TSL;
    writeN				:in  TSL;
    saveN				:in  TSL;
    addr				:in  TSLV(LPM_ADDR_WIDTH-1 downto VEC_INDEX_MIN);
    data_in				:in  TSLV(LPM_DATA_WIDTH-1 downto VEC_INDEX_MIN);
    data_out				:out TSLV(LPM_DATA_WIDTH-1 downto VEC_INDEX_MIN);
    test_in				:in  TSLV(LPM_TEST_WIDTH-1 downto VEC_INDEX_MIN);
    test_out				:out TSLV(LPM_TEST_WIDTH-1 downto VEC_INDEX_MIN);
    test1_out				:out TSLV(LPM_TEST_WIDTH-1 downto VEC_INDEX_MIN);
    enable_out				:out TSLV(LPM_TEST_WIDTH-1 downto VEC_INDEX_MIN);
    bits1_in				:in TSLV(LPM_DATA_WIDTH/2-1 downto VEC_INDEX_MIN);
    bits1_out				:out TSLV(LPM_DATA_WIDTH/2-1 downto VEC_INDEX_MIN);
    bits2_in				:in  TSLV(LPM_DATA_WIDTH/2-1 downto VEC_INDEX_MIN);
    bits2_out				:out TSLV(LPM_DATA_WIDTH/2-1 downto VEC_INDEX_MIN);
    bits_wr_out				:out TSL;
    mem_wr_out				:out TSL;
    mem_ena_out				:out TSL;
    mem_in				:in  TSLV(LPM_DATA_WIDTH-1 downto VEC_INDEX_MIN)
  );
end CVII;

library ieee;
use ieee.std_logic_1164.all;
use work.std_logic_1164_ktp.all;
use work.VComponent.all;

architecture behaviour of CVII is

  constant PAGE_REGISTERS		:TN := 0;
  constant PAGE_PIPELINE		:TN := PAGE_REGISTERS+1;
  constant PAGE_SIMULATOR		:TN := PAGE_PIPELINE+1;
  constant PAGE_HISTOGRAM		:TN := PAGE_SIMULATOR+1;
  constant PAGE_RATE			:TN := PAGE_HISTOGRAM+1;

  constant VECT_SELECT			:TN := PAGE_RATE+1;
 
  constant WORD_VERSION			:TN := 0;
  constant WORD_STATUS			:TN := WORD_VERSION+1;
  constant WORD_COUNT			:TN := WORD_STATUS+1;
  constant WORD_SIM_LEN			:TN := WORD_COUNT+1;
  constant WORD_IN_INV			:TN := WORD_SIM_LEN+1;
  constant WORD_IN_MASK			:TN := WORD_IN_INV+1;
  constant WORD_IN_SHIFT		:TN := WORD_IN_MASK+1;
  constant WORD_IN_PIED			:TN := WORD_IN_MASK+1;
  constant WORD_HIST_MASK		:TN := WORD_IN_PIED+1;
  constant WORD_HIST_COMP		:TN := WORD_HIST_MASK+1;
  constant WORD_TIME0			:TN := WORD_HIST_COMP+1;
  constant WORD_SIM_PAGE_REG0		:TN := WORD_TIME0+1;
  constant WORD_STAT_PAGE_REG0		:TN := WORD_SIM_PAGE_REG0+1;
  constant WORD_END			:TN := WORD_STAT_PAGE_REG0+1;

  constant MEM_PIPELINE			:TN := WORD_END;
  constant MEM_END			:TN := MEM_PIPELINE+1;

  constant BITS_SEL1			:TN := MEM_END;
  constant BITS_SEL2			:TN := BITS_SEL1+1;
  constant BITS_END			:TN := BITS_SEL2;

  constant VIIItemDeclList		:TVIIItemDeclList :=(
  -- item type,        item ID,            width, num,      parent ID,   write type,    read type                     name,          type, description
    ( VII_PAGE, PAGE_REGISTERS,                0,   0, PAGE_REGISTERS, VII_WNOACCESS, VII_RNOACCESS,        VIINameConv(" "), VII_FUN_UNDEF, VIIDescrConv(" ")),
    ( VII_WORD, WORD_STATUS,      LPM_TEST_WIDTH,   1, PAGE_REGISTERS, VII_WACCESS,   VII_REXTERNAL,   VIINameConv("STATUS"), VII_FUN_UNDEF, VIIDescrConv("status register")),
    ( VII_WORD, WORD_COUNT,       LPM_DATA_WIDTH,   2, PAGE_REGISTERS, VII_WACCESS,   VII_RINTERNAL,    VIINameConv("COUNT"),  VII_FUN_RATE, VIIDescrConv("counter")),
    ( VII_WORD, WORD_SIM_LEN,     LPM_TEST_WIDTH,   1, PAGE_REGISTERS, VII_WACCESS,   VII_RINTERNAL,  VIINameConv("SIM_LEN"), VII_FUN_UNDEF, VIIDescrConv("simulator length")),
    ( VII_VECT, VECT_SELECT,                   0,   0, PAGE_REGISTERS, VII_WNOACCESS, VII_RNOACCESS,   VIINameConv("SELECT"), VII_FUN_UNDEF, VIIDescrConv("selector word")),
    ( VII_BITS, BITS_SEL1,      LPM_DATA_WIDTH/2,   1,    VECT_SELECT, VII_WACCESS,   VII_RINTERNAL,     VIINameConv("SEL1"), VII_FUN_UNDEF, VIIDescrConv("selector 1")),
    ( VII_BITS, BITS_SEL2,      LPM_DATA_WIDTH/2,   1,    VECT_SELECT, VII_WACCESS,   VII_REXTERNAL,     VIINameConv("SEL2"), VII_FUN_UNDEF, VIIDescrConv("selector 2")),
    ( VII_PAGE, PAGE_PIPELINE,                 0,   0,  PAGE_PIPELINE, VII_WNOACCESS, VII_RNOACCESS,        VIINameConv(" "), VII_FUN_UNDEF, VIIDescrConv(" ")),
    ( VII_AREA, MEM_PIPELINE,   2*LPM_DATA_WIDTH,   3,  PAGE_PIPELINE, VII_WACCESS,   VII_REXTERNAL, VIINameConv("PIPELINE"),  VII_FUN_HIST, VIIDescrConv("input histogram"))
  );

  constant par				:TVII := TVIICreate(VIIItemDeclList,LPM_ADDR_WIDTH,LPM_DATA_WIDTH);
  signal   vecint,vecext,vecena,vecset	:TSLV(VII(par)'high downto VEC_INDEX_MIN);
        
begin
  process(resetN, saveN, vecset)
  begin
    if(resetN='0') then
      vecint <= IIReset(vecint,par);
    elsif(saveN'event and saveN='1') then
      if(enableN='0') then
        vecint <= IISaveAccess(vecint,par,addr,data_in);
      end if;
    end if;
  end process;

  vecset <= IIConnSetBitsData(vecint,par,BITS_SEL1,0,bits1_in);

  vecena <= IIWriteEnable(vecint,par,enableN,writeN,addr,data_in);

  vecext <= (IIWriteAccess(vecint,par,enableN,writeN,addr,data_in)
          or IIConnPutWordData(vecint,par,WORD_STATUS,0,test_in)
          or IIConnPutBitsData(vecint,par,BITS_SEL2,0,bits2_in)
          or IIConnPutAreaData(vecint,par,MEM_PIPELINE,mem_in));
  data_out <= IIReadAccess(vecext,par,enableN,writeN,addr);
  --
  test_out <= IIConnGetWordData(vecext,par,WORD_SIM_LEN,0);
  test1_out <= IIConnGetWordData(vecext,par,WORD_STATUS,0);
  enable_out <= IIConnGetWordData(vecena,par,WORD_STATUS,0);
  --
  bits1_out <= IIConnGetBitsData(vecext,par,BITS_SEL1,0);
  bits2_out <= IIConnGetBitsData(vecext,par,BITS_SEL2,0);
  --
  bits_wr_out <= IIConnGetBitsWrite(vecena,par,BITS_SEL2,saveN);
  mem_wr_out  <= IIConnGetAreaWrite(vecena,par,MEM_PIPELINE,saveN);
  mem_ena_out <= IIConnGetAreaEnable(vecena,par,MEM_PIPELINE); 


end behaviour;

