library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;
library lpm;
use lpm.lpm_components.all;
use work.std_logic_1164_ktp.all;
use work.vcomponent.all;
use work.LPMcomponent.all;
use work.tridaq.all;

entity controler is
  port(
    -- isa interface
    isa_ior					:in    TSL;
    isa_iow					:in    TSL;
    isa_memr					:in    TSL;
    isa_memw					:in    TSL;
    isa_aen					:in    TSL;
    isa_cs1					:in    TSL;
    isa_cs2					:in    TSL;
    isa_cs3					:in    TSL;
    isa_cs4					:in    TSL;
    isa_reset					:in    TSL;
    isa_sbhe					:in    TSL;
    isa_bale					:in    TSL;
    isa_ad					:in    TSLV(23 downto 7);
    isa_iochrdy					:out   TSL;
    isa_iocs16					:inout TSL;
    isa_memcs16					:inout TSL;
    isa_int4					:out   TSL;
    isa_int5					:out   TSL;
    -- internal vme interface
    vme_oper					:in    TSL;
    vme_write					:in    TSL;
    vme_save					:in    TSL;
    vme_reset					:in    TSL;
    -- internal bus interface
    ii_addr					:in    TSLV(II_ADDR_SIZE-1 downto 0);
    ii_data					:inout TSLV(II_DATA_SIZE-1 downto 0);
    ii_oper					:inout TSL;
    ii_write					:inout TSL;
    ii_save					:inout TSL;
    ii_reset					:inout TSL;
    ii_irq					:inout TSL;
    -- local signal for ii_save generation
    lisa_saverq					:out   TSL;
    lisa_savedel				:in    TSL;
    -- data and address buffors control
    lisa_den					:out   TSL;
    lisa_ddir					:out   TSL;
    lisa_aen					:out   TSL;
    -- external isa interface enable signal
    lisa_enable					:in    TSL;
    -- altera boot loader
    alt_nconf					:out   TSL;
    alt_pgm_data				:out   TSLV(ALT_PGM_DATA_SIZE-1 downto 0);
    alt_dclk					:out   TSL;
    alt_conf_done				:in    TSL;
    alt_init_done				:in    TSL;
    alt_nstatus					:in    TSL;
    -- i2c interface
    i2c_sda					:inout TSL;
    i2c_scl					:inout TSL;
    -- delay interface
    delay_data					:out   TSLV(DELAY_DATA_SIZE-1 downto 0);
    -- ttc interface
    ttc_clk					:in    TSL;
    ttc_clk_des1				:in    TSL;
    ttc_clk_des2				:in    TSL;
    ttc_l1a					:in    TSL;
    ttc_bcn0					:in    TSL;
    ttc_brcst1_data				:in    TSLV(TTC_BROADCAST1_DATA_SIZE-1 downto 0);
    ttc_brcst2_data				:in    TSLV(TTC_BROADCAST2_DATA_SIZE-1 downto 0);
    ttc_brcst_str1				:in    TSL;
    ttc_brcst_str2				:in    TSL;
    -- fast signal interface
    fs_clock					:out   TSL;
    clock					:in    TSL;
    fs1_l1a					:out   TSL;
    fs1_pretrg0					:out   TSL;
    fs1_pretrg1					:out   TSL;
    fs1_bcn0					:out   TSL;
    fs2_l1a					:out   TSL;
    fs2_pretrg0					:out   TSL;
    fs2_pretrg1					:out   TSL;
    fs2_bcn0					:out   TSL;
    -- additional fast signals
    ext_clock					:in    TSL;
    ext_trigger					:in    TSL;
    int_clock					:in    TSL;
    -- diagnostics signal
    led						:out   TSLV(3 downto 0);
    conf_led					:out   TSL
);
end controler;    

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;
library lpm;
use lpm.lpm_components.all;
use work.std_logic_1164_ktp.all;
use work.vcomponent.all;
use work.LPMcomponent.all;
use work.tridaq.all;
use work.control_iis.all;

architecture behaviour of controler is

  signal L, H					:TSL;
  signal LISAMemAddrValidSig			:TSL;
  signal LISAPortAddrValidSig			:TSL;
  signal LISAMemRefSig				:TSL;
  signal LISAPortRefSig				:TSL;
  signal LISAWriteSig				:TSL;
  signal LISAReadSig				:TSL;

  signal ISAOperSig				:TSL;
  signal ISAWriteSig				:TSL;
  signal ISASaveSig				:TSL;

  signal IIDataOutSig				:TSLV(II_DATA_SIZE-1 downto 0);
  signal IIOperOutSig				:TSL;
  signal IIWriteOutSig				:TSL;
  signal IISaveOutSig				:TSL;
  signal IIIrqOutSig				:TSL;
  signal IIResetOutSig				:TSL;
  signal IIOperSig				:TSL;
  signal IIWriteSig				:TSL;
  signal IISaveSig				:TSL;
  signal IIIrqSig				:TSL;
  signal IIResetSig				:TSL;
  signal LIIEnableSig				:TSL;
  signal LIIAddrLocSig				:TSLV(II_ADDR_USER_SIZE-1 downto 0);
  signal IIDataSig				:TSLV(II_DATA_SIZE-1 downto 0);
  signal LIIDataExport				:TSLV(II_DATA_SIZE-1 downto 0);
  signal LIIDataExportEnable			:TSL;

  signal I2CsclSig				:TSL;
  signal I2CsdaSig				:TSL;

  signal ClkSelSig				:TVI;
  signal ClkSig					:TSL;
  signal L1AReg					:TSL;
  signal BCN0Reg				:TSL;
  signal Brcst1DataReg				:TSLV(TTC_BROADCAST1_DATA_SIZE-1 downto 0);
  signal Brcst2DataReg				:TSLV(TTC_BROADCAST2_DATA_SIZE-1 downto 0);
  signal ExtTrgReg				:TSL;

  constant IIPar				:TVII := TVIICreate(VIIItemDeclList,II_ADDR_SIZE,II_DATA_SIZE);
  signal   IIVecInt,IIVecExt,IIVecEna		:TSLV(VII(IIPar)'high downto VEC_INDEX_MIN);

begin
  L <= '0'; H <= '1';
  IIIrqOutSig <= '0'; -- temporary !!!!
  -- obsluga sygnalu blokujacego kontroler isa
  IIOperOutSig  <= ISAOperSig  when (lisa_enable='1') else vme_oper;
  IIWriteOutSig <= ISAWriteSig when (lisa_enable='1') else not(vme_write);
  IISaveOutSig  <= ISASaveSig  when (lisa_enable='1') else vme_save;
  IIResetOutSig <= '0'       when (    TNconv(IIConnGetWordData(IIVecExt,IIPar,WORD_RESET,0))=16#13#
                                   and TSLconv(IIConnGetWordData(IIVecEna,IIPar,WORD_RESET,0))='1'    ) else
                   isa_reset when (lisa_enable='1')                                                  else
		   vme_reset;
  CIIOper:  ALTERA_TRI port map (data_in => IIOperOutSig,  data_out => IIOperSig,  data_tri => ii_oper,  ena => H);
  CIIWrite: ALTERA_TRI port map (data_in => IIWriteOutSig, data_out => IIWriteSig, data_tri => ii_write, ena => H);
  CIISave:  ALTERA_TRI port map (data_in => IISaveOutSig,  data_out => IISaveSig,  data_tri => ii_save,  ena => H);
  CIIReset: ALTERA_TRI port map (data_in => IIResetOutSig, data_out => IIResetSig, data_tri => ii_reset, ena => H);
  CIIIrq:   ALTERA_TRI port map (data_in => IIIrqOutSig,   data_out => IIIrqSig,   data_tri => ii_irq,   ena => H);
  lisa_aen <= '0' when (lisa_enable='1') else '1';

  --sygnal przerwania jest po prostu transmitowany z interfejsu wewnetrznego
  isa_int5 <= IIIrqSig;
  --sygnal LISAWriteSig informuje, ze ma miejsce zapis
  LISAWriteSig <= not (isa_iow and isa_memw);
  --sygnal LISAReadSig informuje, ze ma miejsce odczyt
  LISAReadSig <= not (isa_ior and isa_memr);
  --sygnal LISAMemRefSig informuje, ze ma miejsce odwolanie do pamieci
  LISAMemRefSig <= not (isa_memw and isa_memr and isa_aen);
  --sygnal LISAPortRefSig informuje, ze ma miejsce odwolanie do io
  LISAPortRefSig <= not (isa_iow and isa_ior and isa_aen);
 
  --sygnal LISAMemAddrValidSig okresla, czy zakres adresow przy odwolaniu do pamieci jest poprawny, czy nie
  LISAMemAddrValidSig <= '0' when (LISAMemRefSig='0')            else
                         '1' when (isa_ad(23 downto 16) = x"b0") else
                         '0';
 
  --sygnal LISAPortAddrValidSig okresla, czy zakres adresow przy odwolaniu do io jest poprawny, czy nie
  LISAPortAddrValidSig <= '0' when (LISAPortRefSig='0')          else
                          '1' when (isa_ad(15 downto 8) = x"33") else
                          '0';
  --sterowanie liniami memcs16 i iocs16. musimy je stosownie ustawiac, gdy wystepuje transfer
  CISAMemCs16: ALTERA_TRI port map (data_in => L, data_tri => isa_memcs16, ena => LISAMemAddrValidSig);
  CISAIoCs16:  ALTERA_TRI port map (data_in => L, data_tri => isa_iocs16,  ena => LISAPortAddrValidSig);

  --sterowanie sygnalem save. najpierw wysylamy sygnal i_saverq, a potem czekamy na sygnal i_savedel
  lisa_saverq <= not IIWriteOutSig;
  ISASaveSig  <= IIWriteSig or (not lisa_savedel);

  --sterowanie linia ii_oper
  ISAOperSig <= '1' when ((LISAMemAddrValidSig or LISAPortAddrValidSig)='0') else --w pozosta�ych przypadkach jeden z adres�w jest wa�ny
                '0' when (LISAReadSig='1')                                   else --je�li odczyt, to nie ma problemu hazardu
                IIWriteSig or (not LISAWriteSig);
  --sterowanie linia ii_write
  ISAWriteSig <= '1' when ((LISAMemAddrValidSig or LISAPortAddrValidSig)='0') else --
                 '1' when (LISAReadSig='1')                                   else --je�li odczyt, trzymamy wysoko
                 (not LISAWriteSig) and (IIOperSig or IIWriteSig);

  --sterowanie kierunkiem transmisji bufora danych
  lisa_ddir <= '0' when (LISAReadSig='1') else '1';
  --sterowanie aktywno�ci� bufora danych
  lisa_den <= '1' when (lisa_enable='0')                   else
              '0' when ((LISAReadSig or LISAWriteSig)='1') else
              '1';

  isa_iochrdy <= '1';
  isa_int4 <= '0';

  CI2Cscl: ALTERA_TRI port map (data_in => IIWriteOutSig, data_out => I2CsclSig, data_tri => i2c_scl, ena => lisa_enable);
  CI2Csda: ALTERA_TRI port map (data_in => IIOperOutSig,  data_out => I2CsdaSig, data_tri => i2c_sda,  ena => lisa_enable);

  -- internal interface
  LIIEnableSig <= not(TSLconv(ii_addr(II_ADDR_SIZE-1 downto II_ADDR_USER_SIZE)=II_CONTROL_BASE_ADDR and IIOperSig='0'));
  LIIAddrLocSig <= ii_addr(II_ADDR_USER_SIZE-1 downto 0);

  process(IIResetSig, IISaveSig, IIWriteSig, LIIEnableSig, LIIAddrLocSig)
  begin
    if(IIResetSig='0') then
      IIVecInt <= IIReset(IIVecInt,IIPar);
    elsif(IISaveSig'event and IISaveSig='1') then
      if(LIIEnableSig='0') then
        IIVecInt <= IISaveAccess(IIVecInt,IIPar,LIIAddrLocSig,IIDataSig);
      end if;
    end if;
  end process;

  IIVecEna <= IIWriteEnable(IIVecInt,IIPar,LIIEnableSig,IIWriteSig,LIIAddrLocSig,IIDataSig);
  IIVecExt <= (  IIWriteAccess(IIVecInt,IIPar,LIIEnableSig,IIWriteSig,LIIAddrLocSig,IIDataSig)
              or IIConnPutWordData(IIVecInt,IIPar,WORD_IDENTIFIER,0,TSLVconv(CONTR_IDENTIFIER,II_DATA_SIZE))
              or IIConnPutWordData(IIVecInt,IIPar,WORD_VERSION,0,TSLVconv(CONTR_VERSION,II_DATA_SIZE))
              or IIConnPutBitsData(IIVecInt,IIPar,BITS_STATUS_NSTATUS,0,TSLVconv(alt_nstatus))
              or IIConnPutBitsData(IIVecInt,IIPar,BITS_STATUS_CONFDONE,0,TSLVconv(alt_conf_done))
              or IIConnPutBitsData(IIVecInt,IIPar,BITS_STATUS_INITDONE,0,TSLVconv(alt_init_done))
              or IIConnPutBitsData(IIVecInt,IIPar,BITS_STATUS_ISA_ENA,0,TSLVconv(lisa_enable))
	      );
  LIIDataExport <=IIReadAccess(IIVecExt,IIPar,LIIEnableSig,IIWriteSig,LIIAddrLocSig); -- TSLVconv(257,II_DATA_SIZE); --
  LIIDataExportEnable <= not(LIIEnableSig) and IIWriteSig;
  CIIData: ALTERA_BUSTRI
    generic map (LPM_WIDTH => II_DATA_SIZE)
    port map    (data_in   => LIIDataExport,
                 data_out  => IIDataSig,
                 data_tri  => ii_data,
                 ena       => LIIDataExportEnable);

  -- Altera boot interface
  alt_pgm_data <= IIDataSig(alt_pgm_data'range) when IIConnGetAreaEnable(IIVecEna,IIPar,AREA_BOOT)='1' else
		  (others => '1');
  alt_nconf <= TSLconv(IIConnGetBitsData(IIVecExt,IIPar,BITS_STATUS_NCONF,0));
  alt_dclk <= IIConnGetAreaWrite(IIVecEna,IIPar,AREA_BOOT,IISaveSig);


  -- delay chip interface
  delay_data <= IIConnGetWordData(IIVecExt,IIPar,WORD_DELAY,0);

  -- fast signal interface
  ClkSelSig <= TNconv(IIConnGetBitsData(IIVecExt,IIPar,BITS_STATUS_CLOCK_SEL,0));
  ClkSig <= ttc_clk      when (ClkSelSig=CLOCK_SEL_TTC_CLK)  else
            ttc_clk_des1 when (ClkSelSig=CLOCK_SEL_TTC_DES1) else
            ttc_clk_des2 when (ClkSelSig=CLOCK_SEL_TTC_DES2) else
            int_clock    when (ClkSelSig=CLOCK_SEL_INT)      else
            ext_clock    when (ClkSelSig=CLOCK_SEL_EXT)      else
	    '0';
  fs_clock <= ClkSig;

  process(IIResetSig, clock)
  begin
    if(IIResetSig='0') then
      L1AReg <= '0';
      BCN0Reg <= '0';
      Brcst1DataReg <= (others => '0');
      Brcst2DataReg <= (others => '0');
      ExtTrgReg <= '0';
    elsif(clock'event and clock='1') then
      L1AReg <= ttc_l1a;
      BCN0Reg <= ttc_bcn0;
      ExtTrgReg <= ext_trigger;
      if (ttc_brcst_str1='1') then
        Brcst1DataReg <= ttc_brcst1_data;
      end if;
      if (ttc_brcst_str2='1') then
        Brcst2DataReg <= ttc_brcst2_data;
      end if;
    end if;
  end process;
  
  fs1_l1a <= L1AReg or ExtTrgReg;
  fs1_bcn0 <= BCN0Reg;
  fs1_pretrg0 <= AND_REDUCE(Brcst1DataReg);
  fs1_pretrg1 <= OR_REDUCE(Brcst2DataReg);
  fs2_l1a <= L1AReg;
  fs2_bcn0 <= BCN0Reg;
  fs2_pretrg0 <= AND_REDUCE(Brcst1DataReg);
  fs2_pretrg1 <= OR_REDUCE(Brcst2DataReg);
  
  -- diagnostic interface
  --led <= (others=>'0') when ((isa_cs1=isa_cs2) and (isa_cs3=isa_cs4) -->
  --	  and (isa_sbhe=isa_bale) and (isa_ad(7)='0')) else (others=>'1');
  led(0) <= IIOperSig and IIResetSig;
  led(1) <= LIIEnableSig and IIResetSig;
  led(2) <= IIWriteSig and IIResetSig;
  led(3) <= IISaveSig and IIResetSig;

  conf_led <= '0';

end behaviour;
