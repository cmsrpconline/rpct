library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;
library lpm;
use lpm.lpm_components.all;
use work.std_logic_1164_ktp.all;
use work.vcomponent.all;
use work.LPMcomponent.all;
use work.tridaq.all;
package ldemux_iis is
--
-- internal interface
--
constant   RATE_MEM_DATA_SIZE  :TN  :=minimum(64,LDEMUX_DATA_OUT); --  
constant   RATE_MEM_COUNT_SIZE  :TN  :=32; --  
constant   RATE_MEM_ADDR_SIZE  :TVL  :=TVLcreate(RATE_MEM_DATA_SIZE-1)+SLVPartAddrExpand(RATE_MEM_COUNT_SIZE,II_DATA_SIZE); --  
constant   HIST_TIME_SIZE  :TN  :=40; --  
constant   LDEMUX_IDENTIFIER  :TS  :="DM"; --  
constant   LDEMUX_VERSION  :THV  :="0100"; --  
constant   PAGE_REGISTERS  :TN  := 10; --    
constant   PAGE_MEM_RATE  :TN  := 11; --    
constant   WORD_IDENTIFIER  :TN  := 13; --    
constant   WORD_VERSION  :TN  := 14; --    
constant   VECT_STATUS  :TN  := 16; --    
constant   BITS_STATUS_RATE_START  :TN  := 17; --    
constant   BITS_STATUS_RATE_STOP  :TN  := 18; --    
constant   BITS_STATUS_RATE_ENA  :TN  := 19; --    
constant   BITS_STATUS_PROC_REQ  :TN  := 20; --    
constant   BITS_STATUS_PROC_ACK  :TN  := 21; --    
constant   BITS_STATUS_DEC1_EOD  :TN  := 22; --    
constant   BITS_STATUS_DEC2_EOD  :TN  := 23; --    
constant   BITS_STATUS_CHAN_SEL  :TN  := 24; --    
constant   WORD_DEC1_CHAN_ADDR  :TN  := 26; --    
constant   WORD_DEC2_CHAN_ADDR  :TN  := 27; --    
constant   WORD_HIST_TIME_LIMIT  :TN  := 29; --    
constant   WORD_HIST_TIME_COUNT  :TN  := 30; --    
constant   AREA_MEM_RATE  :TN  := 32; --    
--  " item type                 item ID                 width                  num           parent ID     write type      read type               name                            type     description"  
--
constant VIIItemDeclList	:TVIIItemDeclList :=(  
(    VII_PAGE  ,             PAGE_REGISTERS     ,                      0  ,                     0    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv(" ")   ,                     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,           WORD_IDENTIFIER     ,           II_DATA_SIZE  ,                     1    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("IDENTIFIER")   ,            VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,              WORD_VERSION     ,           II_DATA_SIZE  ,                     1    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("VERSION")   ,               VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_VECT  ,               VECT_STATUS     ,                      0  ,                     0    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv("STATUS")   ,                VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_STATUS_RATE_START     ,                      1  ,                     1    ,          VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("STATUS_RATE_START")   ,     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,    BITS_STATUS_RATE_STOP     ,                      1  ,                     1    ,          VECT_STATUS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("STATUS_RATE_STOP")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_STATUS_RATE_ENA     ,                      1  ,                     1    ,          VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("STATUS_RATE_ENA")   ,       VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_STATUS_PROC_REQ     ,                      1  ,                     1    ,          VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("STATUS_PROC_REQ")   ,       VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_STATUS_PROC_ACK     ,                      1  ,                     1    ,          VECT_STATUS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("STATUS_PROC_ACK")   ,       VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_STATUS_DEC1_EOD     ,                      1  ,                     1    ,          VECT_STATUS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("STATUS_DEC1_EOD")   ,       VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_STATUS_DEC2_EOD     ,                      1  ,                     1    ,          VECT_STATUS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("STATUS_DEC2_EOD")   ,       VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_STATUS_CHAN_SEL     ,           TVLcreate(1)  ,                     1    ,          VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("STATUS_CHAN_SEL")   ,       VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,       WORD_DEC1_CHAN_ADDR     ,   CHAMBER_ADDRESS_SIZE  ,                     1    ,       PAGE_REGISTERS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("WORD_DEC1_CHAN_ADDR")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,       WORD_DEC2_CHAN_ADDR     ,   CHAMBER_ADDRESS_SIZE  ,                     1    ,       PAGE_REGISTERS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("WORD_DEC2_CHAN_ADDR")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,      WORD_HIST_TIME_LIMIT     ,         HIST_TIME_SIZE  ,                     1    ,       PAGE_REGISTERS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("HIST_TIME_LIMIT")   ,       VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,      WORD_HIST_TIME_COUNT     ,         HIST_TIME_SIZE  ,                     1    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("HIST_TIME_COUNT")   ,       VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(    VII_PAGE  ,              PAGE_MEM_RATE     ,                      0  ,                     0    ,        PAGE_MEM_RATE         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv(" ")   ,                     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_AREA  ,             AREA_MEM_RATE     ,    RATE_MEM_COUNT_SIZE  ,    RATE_MEM_DATA_SIZE    ,        PAGE_MEM_RATE         ,     VII_WACCESS  ,   VII_REXTERNAL   ,   VIINameConv("MEM_RATE")   ,              VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ) 
);
end ldemux_iis;
