library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;

use work.std_logic_1164_ktp.all;
use work.tridaq.all;
use work.link.all;

entity lb_coder is
  port(
    -- synchronized data
    data_in					:in    TSLV(LDEMUX_DATA_OUT-1 downto 0);
    -- slave LB channel 0
    LS0_data					:in    TSLV(LB_SYNCHRO_DATA-1 downto 0);
    LS0_phase					:in    TSL;
    LS0_clk					:in    TSL;
    -- slave LB channel 1
    LS1_data					:in    TSLV(LB_SYNCHRO_DATA-1 downto 0);
    LS1_phase					:in    TSL;
    LS1_clk					:in    TSL;
    -- master LB channel 0
    LM0_data					:out   TSLV(LB_SYNCHRO_DATA-1 downto 0);
    LM0_phase					:out   TSL;
    -- master LB channel 0
    LM1_data					:out   TSLV(LB_SYNCHRO_DATA-1 downto 0);
    LM1_phase					:out   TSL;
    -- GOL output
    GOL_data					:out   TSLV(LB_GOL_DATA-1 downto 0);
    -- fast signal interface
    clock					:in    TSL;
    l1a						:in    TSL;
    pretrg0					:in    TSL;
    pretrg1					:in    TSL;
    bcn0					:in    TSL;
    -- internal bus interface
    II_addr					:in    TSLV(IILB_ADDR_SIZE-1 downto 0);
    II_data					:inout TSLV(II_DATA_SIZE-1 downto 0);
    II_oper					:in    TSL;
    II_write					:in    TSL;
    II_save					:in    TSL;
    II_reset					:in    TSL;
    II_ack					:inout TSL;
    -- diagnostics signal
    led						:out   TSLV(5 downto 0)
  );
end lb_coder;

library lpm;
use lpm.lpm_components.all;

use work.vcomponent.all;
use work.LPMcomponent.all;
use work.LPMsynchro.all;
use work.LPMdiagnostics.all;
use work.lb_coder_iis.all;

architecture behaviour of lb_coder is

  component synchro
    port(
      resetN			:in  TSL;
      clock			:in  TSL;
      bcn0			:in  TSL;
      data_in			:in  TSLV(LB_SYNCHRO_DATA-1 downto 0);
      tmark			:in  TSL;
      dphase			:in  TSL;
      data_out			:out TSLV(LINK_DATA_SIZE-1 downto 0);
      clock_quality		:out TSL;
      phase_quality		:out TSL;
      data_parity		:out TSL;
      BCN0_synch		:out TSL;
      BCN_synch			:out TSL;
      latency			:out TSLV(SYN_PIPE_SIZE-1 downto 0)
    );
  end component;

  component fcoder is
    port (
      resetN				:in  TSL;
      clock				:in  TSL;
      data_in				:in  TChanOut;
      part_data				:out TPartData;
      part_code				:out TPartCode;
      part_time				:out TPartTime;
      end_data				:out TSL
    );
  end component;

  constant IIPar				:TVII := TVIICreate(VIIItemDeclList,IILB_ADDR_SIZE,II_DATA_SIZE);
  signal   IIVecInt,IIVecExt,IIVecEna		:TSLV(VII(IIPar)'high downto VEC_INDEX_MIN);
  signal   IIDataSig				:TSLV(II_DATA_SIZE-1 downto 0);
  signal   IIAckInvSig				:TSL;
  signal   LIIDataExport			:TSLV(II_DATA_SIZE-1 downto 0);
  signal   LIIDataExportEnable			:TSL;
  --
  signal   L, H					:TSL;
  signal   DataInReg				:TSLV(LDEMUX_DATA_OUT-1 downto 0);
  --
  signal   L1AReg				:TSL;
  signal   BCN0Reg				:TSL;
  signal   Pretrg0Reg				:TSL;
  signal   Pretrg1Reg				:TSL;
  signal   LocalTrgReg				:TSL;
  signal   BCNCnt				:TSLV(LB_BCN_SIZE-1 downto 0);
  --
  signal   Syn0_DataOutSig			:TSLV(LINK_DATA_SIZE-1 downto 0);
  signal   Syn0_ClockQualitySig			:TSL;
  signal   Syn0_PhaseQualitySig			:TSL;
  signal   Syn0_DataParitySig			:TSL;
  signal   Syn0_BCN0SynchSig			:TSL;
  signal   Syn0_BCNSynchSig			:TSL;
  signal   Syn0_LatencySig			:TSLV(SYN_PIPE_SIZE-1 downto 0);
  signal   Syn1_DataOutSig			:TSLV(LINK_DATA_SIZE-1 downto 0);
  signal   Syn1_ClockQualitySig			:TSL;
  signal   Syn1_PhaseQualitySig			:TSL;
  signal   Syn1_DataParitySig			:TSL;
  signal   Syn1_BCN0SynchSig			:TSL;
  signal   Syn1_BCNSynchSig			:TSL;
  signal   Syn1_LatencySig			:TSLV(SYN_PIPE_SIZE-1 downto 0);
  signal   LSMUX0_DataSig			:TSLV(GLA(GLA'left).l downto 0);
  signal   LSMUX1_DataSig			:TSLV(GLA(GLA'left).l downto 0);
  signal   LMUX0_DataSig			:TSLV(GLA(GLA'left).l downto 0);
  signal   LMUX1_DataReg			:TSLV(GLA(GLA'left).l downto 0);
  signal   LMUXPartDataSig			:TPartData;
  signal   LMUXPartCodeSig			:TPartCode;
  signal   LMUXPartTimeSig			:TPartTime;
  signal   LMUXEndDataSig			:TSL;
  --
  signal   HistTimerLimitSig			:TSLV(HIST_TIME_SIZE-1 downto 0);
  signal   HistTimerCountSig			:TSLV(HIST_TIME_SIZE-1 downto 0);
  signal   HistTimerStartSig			:TSL;
  signal   HistTimerStopSig			:TSL;
  signal   HistTimerClkEnaSig			:TSL;
  signal   TimerEngineClkEnaSig			:TSL;
  signal   ProcReqSig				:TSL;
  signal   ProcAckSig				:TSL;
  --
  signal   HistPartClkEnaSig			:TSL;
  signal   HistPartMemWrSig			:TSL;
  signal   HistPartMemDataOutSig		:TSLV(II_DATA_SIZE-1 downto 0);
  signal   HistPartProcAckSig			:TSL;
  signal   HistCodeClkEnaSig			:TSL;
  signal   HistCodeMemWrSig			:TSL;
  signal   HistCodeMemDataOutSig		:TSLV(II_DATA_SIZE-1 downto 0);
  signal   HistCodeProcAckSig			:TSL;
  signal   HistCodeData				:TSLV(HIST_CODE_SIZE-1 downto 0);

begin	  

  L <= '0'; H <= '1';

  syn0: component synchro
    port map(
      resetN			=> II_reset,
      clock			=> clock,
      bcn0			=> BCN0Reg,
      data_in			=> LS0_data,
      tmark			=> LS0_clk,
      dphase			=> LS0_phase,
      data_out			=> Syn0_DataOutSig,
      clock_quality		=> Syn0_ClockQualitySig,
      phase_quality		=> Syn0_PhaseQualitySig,
      data_parity		=> Syn0_DataParitySig,
      BCN0_synch		=> Syn0_BCN0SynchSig,
      BCN_synch			=> Syn0_BCNSynchSig,
      latency			=> Syn0_LatencySig
    );

  LSMUX0_DataSig(GLA(GLA_ITEM_BCN0).l)   <= BCN0Reg;
  LSMUX0_DataSig(GLA(GLA_ITEM_BCN).l downto GLA(GLA_ITEM_BCN).r) <= BCNcnt;
  LSMUX0_DataSig(GL(GL'left).l downto 0) <= Syn0_DataOutSig;
  LSMUX0_DataSig(GLA(GLA_ITEM_PARITY).l) <= not(XOR_REDUCE(LSMUX0_DataSig(GLA(GLA_ITEM_BCN0).l downto 0)));
  
  syn1: component synchro
    port map(
      resetN			=> II_reset,
      clock			=> clock,
      bcn0			=> BCN0Reg,
      data_in			=> LS1_data,
      tmark			=> LS1_clk,
      dphase			=> LS1_phase,
      data_out			=> Syn1_DataOutSig,
      clock_quality		=> Syn1_ClockQualitySig,
      phase_quality		=> Syn1_PhaseQualitySig,
      data_parity		=> Syn1_DataParitySig,
      BCN0_synch		=> Syn1_BCN0SynchSig,
      BCN_synch			=> Syn1_BCNSynchSig,
      latency			=> Syn1_LatencySig
    );

  LSMUX1_DataSig(GLA(GLA_ITEM_BCN0).l)   <= BCN0Reg;
  LSMUX1_DataSig(GLA(GLA_ITEM_BCN).l downto GLA(GLA_ITEM_BCN).r) <= BCNcnt;
  LSMUX1_DataSig(GL(GL'left).l downto 0) <= Syn1_DataOutSig;
  LSMUX1_DataSig(GLA(GLA_ITEM_PARITY).l) <= not(XOR_REDUCE(LSMUX1_DataSig(GLA(GLA_ITEM_BCN0).l downto 0)));
  
  coder: fcoder
    port map(
      resetN			=> II_reset,
      clock			=> clock,
      data_in			=> DataInReg,
      part_data			=> LMUXPartDataSig,
      part_code			=> LMUXPartCodeSig,
      part_time			=> LMUXPartTimeSig,
      end_data			=> LMUXEndDataSig
    );

  LMUX0_DataSig(GL(GL_ITEM_CHAMBER).l downto GL(GL_ITEM_CHAMBER).r) <= TSLVconv(0,CHAMBER_ADDRESS_SIZE);
  LMUX0_DataSig(GL(GL_ITEM_TIME).l downto GL(GL_ITEM_TIME).r) <= LMUXPartTimeSig;
  LMUX0_DataSig(GL(GL_ITEM_PARTITION).l downto GL(GL_ITEM_PARTITION).r) <= LMUXPartCodeSig;
  LMUX0_DataSig(GL(GL_ITEM_DATA).l downto GL(GL_ITEM_DATA).r) <= LMUXPartDataSig;
  LMUX0_DataSig(GL(GL_ITEM_END_DATA).l) <= LMUXEndDataSig;
  LMUX0_DataSig(GL(GL_ITEM_HALF_PART).l) <= H;
  LMUX0_DataSig(GLA(GLA_ITEM_BCN0).l)   <= BCN0Reg;
  LMUX0_DataSig(GLA(GLA_ITEM_BCN).l downto GLA(GLA_ITEM_BCN).r) <= BCNcnt;
  LMUX0_DataSig(GLA(GLA_ITEM_PARITY).l) <= not(XOR_REDUCE(LMUX0_DataSig(GLA(GLA_ITEM_BCN0).l downto 0)));
  LM0_data  <= SLVDemux(LMUX0_DataSig,2,TNconv(clock));
  LM0_phase <= '1' when clock='1' else '0';

  GOL_data <= TSLVresize(LMUX0_DataSig,GOL_data'length,'0');

  process(ii_reset, clock)
  begin
    if(ii_reset='0') then
      DataInReg    <= (others => '0');
      L1AReg       <= '0';
      BCN0Reg      <= '0';
      Pretrg0Reg   <= '0';
      Pretrg1Reg   <= '0';
      BCNCnt      <= (others => '0');
      LMUX1_DataReg <= (others => '0');
    elsif(clock'event and clock='1') then
      DataInReg <= data_in;
      L1AReg <= l1a;
      BCN0Reg <= bcn0;
      Pretrg0Reg <= pretrg0;
      Pretrg1Reg <= pretrg1;
      if (bcn0='1') then
        BCNCnt <= (others => '0');
      else
        BCNCnt <= BCNCnt+1;
      end if;
      if(IIConnGetBitsData(IIVecInt,IIPar,BITS_STATUS_CHAN_SEL,0)="0") then
        LMUX1_DataReg <= LSMUX0_DataSig;
      else
        LMUX1_DataReg <= LSMUX1_DataSig;
      end if;
    end if;
  end process;

  LM1_data  <= SLVDemux(LMUX1_DataReg,2,TNconv(clock));
  LM1_phase <= '1' when clock='1' else '0';

  --
  -- timer engine
  --
  ProcReqSig <= TSLconv(IIConnGetBitsData(IIVecInt,IIPar,BITS_STATUS_PROC_REQ,0));
  ProcAckSig <= HistPartProcAckSig and HistCodeProcAckSig;
  --
  HistTimerClkEnaSig <= TSLconv(IIConnGetBitsData(IIVecInt,IIPar,BITS_STATUS_RATE_ENA,0));
  HistTimerStartSig  <= TSLconv(IIConnGetBitsData(IIVecInt,IIPar,BITS_STATUS_RATE_START,0));
  HistTimerLimitSig  <= IIConnGetWordData(IIVecInt,IIPar,WORD_HIST_TIME_LIMIT,0); 

  HistTimer: LPM_TIMER_ENGINE 
    generic map (
      LPM_TIMER_SIZE	=> HIST_TIME_SIZE
    )
    port map (
      resetN		=> ii_reset,
      clock		=> clock,
      clk_ena_in	=> HistTimerClkEnaSig,
      clk_ena_out	=> TimerEngineClkEnaSig,
      start		=> HistTimerStartSig,
      stop		=> HistTimerStopSig,
      limit		=> HistTimerLimitSig,
      count		=> HistTimerCountSig
    );

  --
  -- coder engine histograms
  --

  HistPartMemWrSig <= IIConnGetAreaWrite(IIVecEna,IIPar,AREA_MEM_HIST_PART,ii_save);
  
  HistPartComp: DPM_PROG_HIST1
    generic map (
      LPM_DATA_WIDTH	=> HIST_PART_SIZE,
      LPM_COUNT_WIDTH	=> HIST_MEM_COUNT_SIZE,
      LPM_MDATA_WIDTH	=> II_DATA_SIZE
    )
    port map (
      resetN		=> ii_reset,
      clock		=> clock,
      data_in		=> LMUXPartDataSig,
      clk_ena		=> TimerEngineClkEnaSig,
      sim_loop		=> L,
      proc_req		=> ProcReqSig,
      proc_ack		=> HistPartProcAckSig,
      memory_address_in	=> ii_addr(HIST_PART_ADDR_SIZE-1 downto 0),
      memory_data_in	=> ii_data,
      memory_data_out	=> HistPartMemDataOutSig,
      memory_wr		=> HistPartMemWrSig
    );

  HistCodeData <= (TSLVconv(LMUXEndDataSig) & (LMUXPartCodeSig & LMUXPartTimeSig));
  HistCodeMemWrSig <= IIConnGetAreaWrite(IIVecEna,IIPar,AREA_MEM_HIST_CODE,ii_save);
  
  HistCodeComp: DPM_PROG_HIST1
    generic map (
      LPM_DATA_WIDTH	=> HIST_CODE_SIZE,
      LPM_COUNT_WIDTH	=> HIST_MEM_COUNT_SIZE,
      LPM_MDATA_WIDTH	=> II_DATA_SIZE
    )
    port map (
      resetN		=> ii_reset,
      clock		=> clock,
      data_in		=> HistCodeData,
      clk_ena		=> TimerEngineClkEnaSig,
      sim_loop		=> L,
      proc_req		=> ProcReqSig,
      proc_ack		=> HistCodeProcAckSig,
      memory_address_in	=> ii_addr(HIST_CODE_ADDR_SIZE-1 downto 0),
      memory_data_in	=> ii_data,
      memory_data_out	=> HistCodeMemDataOutSig,
      memory_wr		=> HistCodeMemWrSig
    );

  --
  -- internal interface
  --
  process(ii_reset, ii_save)
  begin
    if(ii_reset='0') then
      IIVecInt <= IIReset(IIVecInt,IIPar);
    elsif(ii_save'event and ii_save='1') then
      if(ii_oper='0') then
        IIVecInt <= IISaveAccess(IIVecInt,IIPar,ii_addr,IIDataSig);
      end if;
    end if;
  end process;
  --
  IIVecEna <= IIWriteEnable(IIVecInt,IIPar,ii_oper,ii_write,ii_addr,IIDataSig);

  IIVecExt <= (IIWriteAccess(IIVecInt,IIPar,ii_oper,ii_write,ii_addr,IIDataSig)
              or IIConnPutWordData(IIVecInt,IIPar,WORD_IDENTIFIER,0,TSLVconv(LD_IDENTIFIER,II_DATA_SIZE))
              or IIConnPutWordData(IIVecInt,IIPar,WORD_VERSION,0,TSLVconv(LD_VERSION,II_DATA_SIZE))
              or IIConnPutBitsData(IIVecInt,IIPar,BITS_STATUS_RATE_STOP,  0,TSLVconv(HistTimerStopSig))
              or IIConnPutBitsData(IIVecInt,IIPar,BITS_STATUS_PROC_ACK,   0,TSLVconv(ProcAckSig))
              or IIConnPutBitsData(IIVecInt,IIPar,BITS_SYN0_LATENCY,      0,Syn0_LatencySig)
              or IIConnPutBitsData(IIVecInt,IIPar,BITS_SYN1_LATENCY,      0,Syn1_LatencySig)
              or IIConnPutBitsData(IIVecInt,IIPar,BITS_SYN0_CLK_QUALITY,  0,TSLVconv(Syn0_ClockQualitySig))
              or IIConnPutBitsData(IIVecInt,IIPar,BITS_SYN0_PHASE_QUALITY,0,TSLVconv(Syn0_PhaseQualitySig))
              or IIConnPutBitsData(IIVecInt,IIPar,BITS_SYN0_DATA_PARITY,  0,TSLVconv(Syn0_DataParitySig))
              or IIConnPutBitsData(IIVecInt,IIPar,BITS_SYN0_BCN0_SYNCH,   0,TSLVconv(Syn0_BCN0SynchSig))
              or IIConnPutBitsData(IIVecInt,IIPar,BITS_SYN0_BCN_SYNCH,    0,TSLVconv(Syn0_BCNSynchSig))
              or IIConnPutBitsData(IIVecInt,IIPar,BITS_SYN1_CLK_QUALITY,  0,TSLVconv(Syn1_ClockQualitySig))
              or IIConnPutBitsData(IIVecInt,IIPar,BITS_SYN1_PHASE_QUALITY,0,TSLVconv(Syn1_PhaseQualitySig))
              or IIConnPutBitsData(IIVecInt,IIPar,BITS_SYN1_DATA_PARITY,  0,TSLVconv(Syn1_DataParitySig))
              or IIConnPutBitsData(IIVecInt,IIPar,BITS_SYN1_BCN0_SYNCH,   0,TSLVconv(Syn1_BCN0SynchSig))
              or IIConnPutBitsData(IIVecInt,IIPar,BITS_SYN1_BCN_SYNCH,    0,TSLVconv(Syn1_BCNSynchSig))

              or IIConnPutAreaData(IIVecInt,IIPar,AREA_MEM_HIST_PART, HistPartMemDataOutSig)
              or IIConnPutAreaData(IIVecInt,IIPar,AREA_MEM_HIST_CODE, HistCodeMemDataOutSig)
	    );
  LIIDataExport <= IIReadAccess(IIVecExt,IIPar,ii_oper,ii_write,ii_addr);
  --
  LIIDataExportEnable <= not(ii_oper) and ii_write;
  CIIData: ALTERA_BUSTRI
    generic map (LPM_WIDTH => II_DATA_SIZE)
    port map    (data_in   => LIIDataExport,
                 data_out  => IIDataSig,
                 data_tri  => ii_data,
                 ena       => LIIDataExportEnable);
  --
  IIAckInvSig <= not(ii_oper);
  CIIAck: ALTERA_TRI
    port map(data_in  => L, 
             data_tri => ii_ack,
             ena      => IIAckInvSig
    );
  --
  -- diagnostic interface
  --
  led_clock: LPM_CLOCK_PULSER
    generic map (
      LPM_DIVIDE_PAR		=> CLK_FREQ/LED_PULSE_FREQ,
      LPM_DATA_PULSE_WIDTH	=> LED_PULSE_WIDTH,
      LPM_PULSE_INVERT		=> TRUE	
    )
    port map (
      resetN			=> II_reset,
      clock			=> clock,
      pulse			=> led(3)
    );
  --
  led(0) <= not(TimerEngineClkEnaSig);
  led(1) <= not(LMUXEndDataSig);
  led(2) <= not(OR_REDUCE(DataInReg(RATE_MEM_DATA_SIZE-1 downto 0)));
--  led(3) <= '0' when ((L1AReg=BCN0Reg) and (Pretrg0Reg=Pretrg1Reg)) else
--            '1';

end behaviour;

