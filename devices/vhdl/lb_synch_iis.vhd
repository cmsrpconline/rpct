library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;
library lpm;
use lpm.lpm_components.all;
use work.std_logic_1164_ktp.all;
use work.vcomponent.all;
use work.LPMcomponent.all;
use work.tridaq.all;
package lb_synch_iis is
--
-- internal interface
--
constant   RATE_MEM_DATA_SIZE  :TN  :=minimum(64,LDEMUX_DATA_OUT); --  
constant   RATE_MEM_COUNT_SIZE  :TN  :=32; --  
constant   RATE_MEM_ADDR_SIZE  :TVL  :=TVLcreate(RATE_MEM_DATA_SIZE-1)+SLVPartAddrExpand(RATE_MEM_COUNT_SIZE,II_DATA_SIZE); --  
constant   DATA_DELAY_SIZE  :TN  :=5; --  
constant   HIST_TIME_SIZE  :TN  :=40; --  
constant   TRG_COND_SEL_NON  :TN  :=VEC_INDEX_MIN; --  
constant   TRG_COND_SEL_PRETRG0  :TN  :=TRG_COND_SEL_NON+1; --  
constant   TRG_COND_SEL_PRETRG1  :TN  :=TRG_COND_SEL_PRETRG0+1; --  
constant   TRG_COND_SEL_NUM  :TN  :=TRG_COND_SEL_PRETRG1+1; --  
constant   TRG_COND_SEL_SIZE  :TN  :=TVLcreate(TRG_COND_SEL_NUM-1); --  
constant   LS_IDENTIFIER  :TS  :="LS"; --  
constant   LS_VERSION  :THV  :="0201"; --  
constant   PAGE_REGISTERS  :TN  := 19; --    
constant   PAGE_MEM_WIN_RATE  :TN  := 20; --    
constant   PAGE_MEM_FULL_RATE  :TN  := 21; --    
constant   WORD_IDENTIFIER  :TN  := 23; --    
constant   WORD_VERSION  :TN  := 24; --    
constant   WORD_CHAN_ENA  :TN  := 25; --    
constant   VECT_STATUS  :TN  := 27; --    
constant   BITS_STATUS_RATE_START  :TN  := 28; --    
constant   BITS_STATUS_RATE_STOP  :TN  := 29; --    
constant   BITS_STATUS_RATE_ENA  :TN  := 30; --    
constant   BITS_STATUS_PROC_REQ  :TN  := 31; --    
constant   BITS_STATUS_PROC_ACK  :TN  := 32; --    
constant   BITS_STATUS_TRG_SEL  :TN  := 33; --    
constant   BITS_STATUS_TRG_RATE  :TN  := 34; --    
constant   BITS_STATUS_TRG_DAQ  :TN  := 35; --    
constant   WORD_HIST_TIME_LIMIT  :TN  := 37; --    
constant   WORD_HIST_TIME_COUNT  :TN  := 38; --    
constant   WORD_WIN_DELAY  :TN  := 40; --    
constant   WORD_PULSE_DELAY  :TN  := 41; --    
constant   AREA_MEM_WIN_RATE  :TN  := 43; --    
constant   AREA_MEM_FULL_RATE  :TN  := 44; --    
--  " item type                 item ID                  width                 num           parent ID     write type      read type               name                           type     description"  
--
constant VIIItemDeclList	:TVIIItemDeclList :=(  
(    VII_PAGE  ,             PAGE_REGISTERS     ,                       0  ,                    0    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv("REGISTERS")   ,            VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,           WORD_IDENTIFIER     ,            II_DATA_SIZE  ,                    1    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("IDENTIFIER")   ,           VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,              WORD_VERSION     ,            II_DATA_SIZE  ,                    1    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("VERSION")   ,              VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,             WORD_CHAN_ENA     ,      RATE_MEM_DATA_SIZE  ,                    1    ,       PAGE_REGISTERS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("CHAN_ENA")   ,             VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_VECT  ,               VECT_STATUS     ,                       0  ,                    0    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv("STATUS")   ,               VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_STATUS_RATE_START     ,                       1  ,                    1    ,          VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("STATUS_RATE_START")   ,    VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,    BITS_STATUS_RATE_STOP     ,                       1  ,                    1    ,          VECT_STATUS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("STATUS_RATE_STOP")   ,     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_STATUS_RATE_ENA     ,                       1  ,                    1    ,          VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("STATUS_RATE_ENA")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_STATUS_PROC_REQ     ,                       1  ,                    1    ,          VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("STATUS_PROC_REQ")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_STATUS_PROC_ACK     ,                       1  ,                    1    ,          VECT_STATUS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("STATUS_PROC_ACK")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,      BITS_STATUS_TRG_SEL     ,       TRG_COND_SEL_SIZE  ,                    1    ,          VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("COND_TRG_SEL")   ,         VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_STATUS_TRG_RATE     ,                       1  ,                    1    ,          VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("COND_TRG_RATE")   ,        VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,      BITS_STATUS_TRG_DAQ     ,                       1  ,                    1    ,          VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("COND_TRG_DAQ")   ,         VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,      WORD_HIST_TIME_LIMIT     ,          HIST_TIME_SIZE  ,                    1    ,       PAGE_REGISTERS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("HIST_TIME_LIMIT")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,      WORD_HIST_TIME_COUNT     ,          HIST_TIME_SIZE  ,                    1    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("HIST_TIME_COUNT")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,            WORD_WIN_DELAY     ,         DATA_DELAY_SIZE  ,                    1    ,       PAGE_REGISTERS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("WORD_WIN_DELAY")   ,       VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,          WORD_PULSE_DELAY     ,         DATA_DELAY_SIZE  ,                    1    ,       PAGE_REGISTERS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("WORD_PULSE_DELAY")   ,     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(    VII_PAGE  ,          PAGE_MEM_WIN_RATE     ,                       0  ,                    0    ,    PAGE_MEM_WIN_RATE         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv("MEM_WIN_RATE")   ,         VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_AREA  ,         AREA_MEM_WIN_RATE     ,     RATE_MEM_COUNT_SIZE  ,   RATE_MEM_DATA_SIZE    ,    PAGE_MEM_WIN_RATE         ,     VII_WACCESS  ,   VII_REXTERNAL   ,   VIINameConv("MEM_WIN_RATE")   ,         VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(    VII_PAGE  ,         PAGE_MEM_FULL_RATE     ,                       0  ,                    0    ,   PAGE_MEM_FULL_RATE         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv("MEM_FULL_RATE")   ,        VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_AREA  ,        AREA_MEM_FULL_RATE     ,     RATE_MEM_COUNT_SIZE  ,   RATE_MEM_DATA_SIZE    ,   PAGE_MEM_FULL_RATE         ,     VII_WACCESS  ,   VII_REXTERNAL   ,   VIINameConv("MEM_FULL_RATE")   ,        VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ) 
);
end lb_synch_iis;
