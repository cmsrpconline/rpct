/***********************************************************************\
*                                                                       *
* This file was created by Component Internal Interface Engine software *
*  Copyright(c) 2000-2006 by Krzysztof Pozniak (pozniak@ise.pw.edu.pl)  *
*                           All Rights Reserved.                        *
*                                                                       *
\***********************************************************************/


// constants
const TN IID_CLASS :: LHC_CLK_FREQ = 40000000;
const TN IID_CLASS :: TTC_BCN_EVT_WIDTH = 12;
const TN IID_CLASS :: TTC_BCN_WIDTH = 12;
const TN IID_CLASS :: TTC_EVN_WIDTH = 24;
const TN IID_CLASS :: TTC_BROADCAST1_WIDTH = 4;
const TN IID_CLASS :: TTC_BROADCAST2_WIDTH = 2;
const TN IID_CLASS :: TTC_BROADCAST_SIZE = 6;
const TN IID_CLASS :: TTC_DOUT_ID_WIDTH = 8;
const TN IID_CLASS :: TTC_SUBADDR_WIDTH = 8;
const TN IID_CLASS :: TTC_DQ_WIDTH = 4;
const TN IID_CLASS :: TTC_ID_MODE_WIDTH = 16;
const TN IID_CLASS :: RPC_FEBRE11_DATA_WIDTH = 128;
const TN IID_CLASS :: RPC_FEBRE11_TEST_WIDTH = 32;
const TN IID_CLASS :: RPC_FEBSTD_DATA_WIDTH = 96;
const TN IID_CLASS :: RPC_FEBSTD_TEST_WIDTH = 24;
const TN IID_CLASS :: RPC_LB_DATA_PART_WIDTH = 8;
const TN IID_CLASS :: RPC_LB_TIME_MAX = 7;
const TN IID_CLASS :: RPC_LB_CHAMBER_MAX = 2;
const TN IID_CLASS :: RPC_LB_CHAMBER_NUM = 3;
const TN IID_CLASS :: RPC_LB_FAST_DATA_WIDTH = 5;
const TN IID_CLASS :: RPC_LB_TEST_PART_WIDTH = 8;
const TN IID_CLASS :: GOL_DATA_SIZE = 32;
const TN IID_CLASS :: RPC_LB_TEST_PART_NUM = 4;
const TN IID_CLASS :: RPC_LBRE11_DATA_POS_MAX = 15;
const TN IID_CLASS :: RPC_LBRE11_DATA_POS_WIDTH = 4;
const TN IID_CLASS :: RPC_LBRE11_TIME_WIDTH = 3;
const TN IID_CLASS :: RPC_LBRE11_CHAMBER_WIDTH = 2;
const TN IID_CLASS :: RPC_LBRE11_LMUX_WIDTH = 19;
const TN IID_CLASS :: RPC_LBSTD_DATA_POS_MAX = 11;
const TN IID_CLASS :: RPC_LBSTD_DATA_POS_WIDTH = 4;
const TN IID_CLASS :: RPC_LBSTD_TIME_WIDTH = 3;
const TN IID_CLASS :: RPC_LBSTD_CHAMBER_WIDTH = 2;
const TN IID_CLASS :: RPC_LBSTD_LMUX_WIDTH = 19;
const TN IID_CLASS :: RPC_LBSTD_BCN_WIDTH = 7;
const TN IID_CLASS :: RPC_LINK_STRUC_PART_DATA = 0;
const TN IID_CLASS :: RPC_LINK_STRUC_PART_CODE = 1;
const TN IID_CLASS :: RPC_LINK_STRUC_PART_TIME = 2;
const TN IID_CLASS :: RPC_LINK_STRUC_END_DATA = 3;
const TN IID_CLASS :: RPC_LINK_STRUC_HALF_PART = 4;
const TN IID_CLASS :: RPC_LINK_STRUC_CHAN_NUM = 5;
const TN IID_CLASS :: RPC_LINK_STRUC_FAST_DATA = 6;
const TN IID_CLASS :: RPC_PAC_QUALITY_NUM_MAX = 7;
const TN IID_CLASS :: RPC_PAC_QUALITY_WIDTH = 3;
const TN IID_CLASS :: RPC_PAC_CODE_NUM_MAX = 31;
const TN IID_CLASS :: RPC_PAC_CODE_WIDTH = 5;
const TN IID_CLASS :: RPC_PAC_DATA_WIDTH = 9;
const TN IID_CLASS :: RPC_PAC_SORT_NUM = 4;
const TN IID_CLASS :: RPC_PAC_STRUC_SIGN = 0;
const TN IID_CLASS :: RPC_PAC_STRUC_CODE = 1;
const TN IID_CLASS :: RPC_PAC_STRUC_QUALITY = 2;
const TN IID_CLASS :: I2C_AREA_ADDR_SIZE = 2;
const TN IID_CLASS :: I2C_AREA_ADDR_ITEM = 4;
const TN IID_CLASS :: I2C_AREA_DATA_SIZE = 16;
const TN IID_CLASS :: I2C_AREA_SEL_SIZE = 2;
const TN IID_CLASS :: TB_BASE_CNTRL = 0;
const TN IID_CLASS :: TB_BASE_SORTER = 1;
const TN IID_CLASS :: TB_BASE_OPTO = 2;
const TN IID_CLASS :: TB_BASE_LDPAC = 8;
const TN IID_CLASS :: TB_BASE_RMB = 12;
const TN IID_CLASS :: TB_BASE_NUM = 13;
const TN IID_CLASS :: TB_BASE_SIZE = 4;
const TN IID_CLASS :: TB_II_ADDR_WIDTH_HARD = 20;
const TN IID_CLASS :: TB_II_ADDR_WIDTH_SOFT = 18;
const TN IID_CLASS :: TB_II_BASE_WIDTH = 4;
const TN IID_CLASS :: TB_II_ADDR_WIDTH_USER = 14;
const TN IID_CLASS :: TB_II_DATA_WIDTH = 16;
const TN IID_CLASS :: TB_II_INT_WIDTH = 3;
const TN IID_CLASS :: TB_MUX_MULTIPL = 8;
const TN IID_CLASS :: TB_MUX_MULTIPL_WIDTH = 3;
const TN IID_CLASS :: TB_FAST_BUS_WIDTH = 3;
const TN IID_CLASS :: TB_LINK_NUM = 18;
const TN IID_CLASS :: TB_OPTO_CHAN_NUM = 3;
const TN IID_CLASS :: TB_OPTO_BOARD_NUM = 6;
const TN IID_CLASS :: TB_OPTO_MUX_MULTIPL = 8;
const TN IID_CLASS :: TB_OPTO_MUX_DATA_WIDTH = 3;
const TN IID_CLASS :: TB_OPTO_DATA_WIDTH = 24;
const TN IID_CLASS :: TB_OPTO_BCN_WIDTH = 4;
const TN IID_CLASS :: TB_OPTO_MUX_DELAY_WIDTH = 3;
const TN IID_CLASS :: TB_PAC_BOARD_NUM = 4;
const TN IID_CLASS :: TB_PAC_NUM = 12;
const TN IID_CLASS :: TB_PAC_MUX_MULTIPL = 8;
const TN IID_CLASS :: TB_PAC_MUX_DATA_WIDTH = 14;
const TN IID_CLASS :: TB_PAC_DATA_WIDTH = 112;
const TN IID_CLASS :: TB_PAC_BCN_WIDTH = 3;
const TN IID_CLASS :: TB_PAC_MUX_DELAY_WIDTH = 3;
const TN IID_CLASS :: TB_PAC_GB_DATA_WIDTH = 3;
const TN IID_CLASS :: RPC_GB_DATA_SIZE = 2;
const TN IID_CLASS :: RPC_PAC_GB_PHI_ADDR_SIZE = 4;
const TN IID_CLASS :: RPC_PAC_GB_ETA_ADDR_SIZE = 2;
const TN IID_CLASS :: RPC_PAC_GB_DATA_SIZE = 17;
const TN IID_CLASS :: TB_SORT_MUX_MULTIPL = 8;
const TN IID_CLASS :: TB_SORT_MUX_DATA_WIDTH = 9;
const TN IID_CLASS :: TB_SORT_DATA_WIDTH = 72;
const TN IID_CLASS :: TB_SORT_BCN_WIDTH = 3;
const TN IID_CLASS :: TB_SORT_MUX_DELAY_WIDTH = 3;
const TN IID_CLASS :: TB_BOARD_NUM = 9;
const TS IID_CLASS :: TB_BOARD_IDENTIFIER = "TB";
const TN IID_CLASS :: TB_TIMER_SIZE = 40;
const TN IID_CLASS :: TB_TRIGGER_SEL_L1A = 0;
const TN IID_CLASS :: TB_TRIGGER_SEL_PRETRG0 = 1;
const TN IID_CLASS :: TB_TRIGGER_SEL_PRETRG1 = 2;
const TN IID_CLASS :: TB_TRIGGER_SEL_PRETRG2 = 3;
const TN IID_CLASS :: TB_TRIGGER_SEL_NUM = 4;
const TN IID_CLASS :: TB_TRIGGER_SEL_SIZE = 2;
const TS IID_CLASS :: TB_RMB_IDENTIFIER = "RM";
const THV IID_CLASS :: TB_RMB_VERSION = "0300";
const TN IID_CLASS :: TB_RMB_GOH_DATA_WIDTH = 16;
const TN IID_CLASS :: TB_OPTO_MUX_LINE_NUM = 54;
const TN IID_CLASS :: TB_RMB_DATA_DELAY_WIDTH = 2;
const TN IID_CLASS :: TB_RMB_BCN0_DELAY_SIZE = 7;
const TN IID_CLASS :: TB_RMB_TRG_DELAY_SIZE = 7;
const TN IID_CLASS :: TB_RMB_TRIG_COUNT_SIZE = 32;
const TN IID_CLASS :: RMB_DATA_DELAY_SIZE = 7;
const TN IID_CLASS :: LINK_ERROR_COUNT_SIZE = 16;
const TN IID_CLASS :: RECEIVER_ERROR_COUNT_SIZE = 32;
const TN IID_CLASS :: DAQ_DATA_DELAY_SIZE = 5;
const TN IID_CLASS :: DAQ_DIAG_INDATA_SIZE = 455;
const TN IID_CLASS :: DAQ_DIAG_DATA_SIZE = 472;
const TN IID_CLASS :: DAQ_DIAG_TRIG_NUM = 1;
const TN IID_CLASS :: DAQ_DIAG_TIMER_SIZE = 36;
const TN IID_CLASS :: DAQ_DIAG_MASK_WIDTH = 16;
const TN IID_CLASS :: DAQ_DIAG_ADDR_WIDTH = 6;
const TN IID_CLASS :: DAQ_DIAG_ADDR_POS = 64;
const TN IID_CLASS :: DAQ_DIAG_MEM_DATA_SIZE = 478;
const TN IID_CLASS :: DAQ_MEM_ADDR_SIZE = 11;
const TN IID_CLASS :: TRG_COND_SEL_NONE = 0;
const TN IID_CLASS :: TRG_COND_SEL_L1A = 1;
const TN IID_CLASS :: TRG_COND_SEL_PRETRG0 = 2;
const TN IID_CLASS :: TRG_COND_SEL_PRETRG1 = 3;
const TN IID_CLASS :: TRG_COND_SEL_PRETRG2 = 4;
const TN IID_CLASS :: TRG_COND_SEL_BCN0 = 5;
const TN IID_CLASS :: TRG_COND_SEL_LOCAL = 6;
const TN IID_CLASS :: TRG_COND_SEL_NUM = 7;
const TN IID_CLASS :: TRG_COND_SEL_SIZE = 3;
const TN IID_CLASS :: DATA_SIZE_LFSR_WIDTH = 4;

// parameters
const TN IID_CLASS :: II_ADDR_SIZE  = 18;
const TN IID_CLASS :: II_DATA_SIZE  = 16;
const TN IID_CLASS :: CHECK_SUM = 1477624727;

// interfaces
const TN IID_CLASS :: PAGE_UNIVERSAL = 0;
const TN IID_CLASS :: WORD_CHECKSUM = 1;
const TN IID_CLASS :: WORD_BOARD = 2;
const TN IID_CLASS :: WORD_IDENTIFIER = 3;
const TN IID_CLASS :: WORD_VERSION = 4;
const TN IID_CLASS :: WORD_USER_REG1 = 5;
const TN IID_CLASS :: WORD_USER_REG2 = 6;
const TN IID_CLASS :: WORD_LFSR_STATE = 7;
const TN IID_CLASS :: WORD_TEST_PINS = 8;
const TN IID_CLASS :: VECT_CMD = 9;
const TN IID_CLASS :: BITS_CMD_START = 10;
const TN IID_CLASS :: BITS_CMD_STOP = 11;
const TN IID_CLASS :: BITS_CMD_RST = 12;
const TN IID_CLASS :: BITS_CMD_L1A = 13;

TMemoryMap* IID_CLASS ::BuildMemoryMap() {
  const TVIIItemDecl declItems[] =
  {
    { VII_WORD, WORD_CHECKSUM, 16, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL, VIINameConv("CHECKSUM"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_BOARD, 16, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL, VIINameConv("BOARD"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_IDENTIFIER, 16, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL, VIINameConv("IDENTIFIER"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_VERSION, 16, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL, VIINameConv("VERSION"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_USER_REG1, 16, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL, VIINameConv("USER_REG1"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_USER_REG2, 16, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL, VIINameConv("USER_REG2"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_LFSR_STATE, 4, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL, VIINameConv("LFSR_STATE"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_TEST_PINS, 8, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL, VIINameConv("TEST_PINS"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_VECT, VECT_CMD, 0, 0, PAGE_UNIVERSAL, VII_WNOACCESS, VII_RNOACCESS, VIINameConv("CMD"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_CMD_START, 1, 1, VECT_CMD, VII_WACCESS, VII_RNOACCESS, VIINameConv("START"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_CMD_STOP, 1, 1, VECT_CMD, VII_WACCESS, VII_RNOACCESS, VIINameConv("STOP"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_CMD_RST, 1, 1, VECT_CMD, VII_WACCESS, VII_RNOACCESS, VIINameConv("RST"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_CMD_L1A, 1, 1, VECT_CMD, VII_WACCESS, VII_RNOACCESS, VIINameConv("L1A"), VII_FUN_UNDEF, VIIDescrConv(" ")}
  };
  const TVIIItem viiItems[] =
  {
    { VII_WORD, 1, -1, 16, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 0, 1}, //WORD_CHECKSUM
    { VII_WORD, 2, -1, 16, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 1, 1}, //WORD_BOARD
    { VII_WORD, 3, -1, 16, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 2, 1}, //WORD_IDENTIFIER
    { VII_WORD, 4, -1, 16, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 3, 1}, //WORD_VERSION
    { VII_WORD, 5, -1, 16, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 4, 1}, //WORD_USER_REG1
    { VII_WORD, 6, -1, 16, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 5, 1}, //WORD_USER_REG2
    { VII_WORD, 7, -1, 4, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 6, 1}, //WORD_LFSR_STATE
    { VII_WORD, 8, -1, 8, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 7, 1}, //WORD_TEST_PINS
    { VII_BITS, 10, -1, 1, 1, VII_WACCESS, -1, VII_RNOACCESS, -1, 8, 0}, //BITS_CMD_START
    { VII_BITS, 11, -1, 1, 1, VII_WACCESS, -1, VII_RNOACCESS, -1, 8, 1}, //BITS_CMD_STOP
    { VII_BITS, 12, -1, 1, 1, VII_WACCESS, -1, VII_RNOACCESS, -1, 8, 2}, //BITS_CMD_RST
    { VII_BITS, 13, -1, 1, 1, VII_WACCESS, -1, VII_RNOACCESS, -1, 8, 3} //BITS_CMD_L1A
  };
  return new TMemoryMap(declItems, sizeof declItems / sizeof declItems[0],
    viiItems, sizeof viiItems / sizeof viiItems[0], II_ADDR_SIZE, II_DATA_SIZE, CHECK_SUM);
}
