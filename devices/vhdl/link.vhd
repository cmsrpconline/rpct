library ieee;
use ieee.std_logic_1164.all;
use work.std_logic_1164_ktp.all;
use work.tridaq.all;

package link is

  -------------------------------------------------------------------
  -- link decoder single module
  -------------------------------------------------------------------
  --
  -- link I/O types
  subtype  TChanAddr is TSLV(CHAMBER_ADDRESS_SIZE-1 downto 0);
  subtype  TPartData is TSLV(DATA_PARTITION_SIZE-1 downto 0);
  subtype  TPartCode is TSLV(DATA_PARTITION_CODE_SIZE-1 downto 0);
  subtype  TPartTime is TSLV(TIME_PARTITION_CODE_SIZE-1 downto 0);
  subtype  TChanOut  is TSLV(LDEMUX_DATA_OUT-1 downto 0);

end link;

-------------------------------------------------------------------
-- lcoder module
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
use work.std_logic_1164_ktp.all;
use work.tridaq.all;
use work.link.all;

entity coder is
  port (
    resetN				:in  TSL;
    clock				:in  TSL;
    data_in				:in  TChanOut;
    part_data				:out TPartData;
    part_code				:out TPartCode;
    part_time				:out TPartTime;
    end_data				:out TSL
  );
end coder;

architecture behaviour of coder is

  type TLinkPos is record
    PartData				:TPartData;
    PartCode				:TPartCode;    
    PartTime				:TPartTime;
    EndData				:TSL;
  end record;

  type TLinkVec is array (TIME_PARTITION_CODE_MAX downto 0) of TLinkPos;
  
  signal DataInReg			:TChanOut;
  signal LinkPipe			:TLinkVec;
  signal IndexCnt			:TI range -1 to TIME_PARTITION_CODE_MAX;
   
begin

  process(clock, resetN)
    variable LinkPipeVar		:TLinkVec;
    variable IndexCntVar		:TI range -1 to TIME_PARTITION_CODE_MAX;
    variable FirstPos			:TI range 0 to LDEMUX_DATA_OUT-1;
  
  begin

    if (resetN='0') then

      DataInReg <= (others =>'0');
      for index in LinkPipe'range loop
        LinkPipe(index).PartData <= (others => '0');
        LinkPipe(index).PartCode <= (others => '0');
        LinkPipe(index).PartTime <= (others => '0');
        LinkPipe(index).EndData  <= '0';
      end loop;
      IndexCnt <= -1;

    elsif (clock'event and clock='1') then

      DataInReg <= data_in;
      LinkPipeVar := LinkPipe;
      IndexCntVar := maximum(IndexCnt-1,-1);

      LinkPipeVar(TIME_PARTITION_CODE_MAX-1 downto 0) := LinkPipeVar(TIME_PARTITION_CODE_MAX downto 1);
      LinkPipeVar(TIME_PARTITION_CODE_MAX).PartData := (others => '0');
      LinkPipeVar(TIME_PARTITION_CODE_MAX).PartCode := (others => '0');
      LinkPipeVar(TIME_PARTITION_CODE_MAX).PartTime := (others => '0');
      LinkPipeVar(TIME_PARTITION_CODE_MAX).EndData  := '0';
      for part_pos in DATA_PARTITION_CODE_MAX downto 0 loop
        FirstPos:=part_pos*DATA_PARTITION_SIZE;
        if (OR_REDUCE(DataInReg(FirstPos+DATA_PARTITION_SIZE-1 downto FirstPos))='1') then
	  if(IndexCntVar<TIME_PARTITION_CODE_MAX) then
	    IndexCntVar := IndexCntVar+1;
            LinkPipeVar(IndexCntVar).PartData := DataInReg(FirstPos+DATA_PARTITION_SIZE-1 downto FirstPos);
            LinkPipeVar(IndexCntVar).PartCode := TSLVconv(part_pos,DATA_PARTITION_CODE_SIZE);
            LinkPipeVar(IndexCntVar).PartTime := TSLVconv(IndexCntVar,TIME_PARTITION_CODE_SIZE);
            LinkPipeVar(IndexCntVar).EndData  := '0';
	  else
            LinkPipeVar(TIME_PARTITION_CODE_MAX).EndData  := '1';
	  end if;
        end if;
      end loop;

      LinkPipe <= LinkPipeVar;
      IndexCnt <= IndexCntVar;
    end if;

    part_data <= LinkPipe(0).PartData;
    part_code <= LinkPipe(0).PartCode;
    part_time <= LinkPipe(0).PartTime;
    end_data  <= LinkPipe(0).EndData;

  end process;

end behaviour;

-------------------------------------------------------------------
-- lcoder module
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
use work.std_logic_1164_ktp.all;
use work.tridaq.all;
use work.link.all;

entity fcoder1 is
  port (
    resetN				:in  TSL;
    clock				:in  TSL;
    data_in				:in  TChanOut;
    part_data				:out TPartData;
    part_code				:out TPartCode;
    part_time				:out TPartTime;
    end_data				:out TSL
  );
end fcoder1;

architecture behaviour of fcoder1 is

  type TLinkPos	is record
    PartData				:TPartData;
    PartCode				:TPartCode;    
    PartTime				:TPartTime;
    EndData				:TSL;
  end record;
  type TLinkVec				is array (TIME_PARTITION_CODE_MAX downto 0) of TLinkPos;

  type TPartDataVec			is array (DATA_PARTITION_CODE_MAX downto 0) of TPartData;

  subtype TPartMux			is TI range -1 to DATA_PARTITION_CODE_MAX;
  type TPartMuxVec			is array (TIME_PARTITION_CODE_MAX downto 0) of TPartMux;
  
  signal DataInReg			:TPartDataVec;
  signal DataInDel			:TPartDataVec;
  signal LinkPipe			:TLinkVec;
  signal PartMux			:TPartMuxVec;
  signal EndDataReg			:TSL;
  signal IndexCnt			:TI range -1 to TIME_PARTITION_CODE_MAX;
   
begin

  process(clock, resetN)
    variable PartMuxVar			:TPartMuxVec;
    variable EndDataVar			:TSL;
    variable IndexCntVar		:TI range -1 to TIME_PARTITION_CODE_MAX;
    variable FirstPos			:TI range 0 to LDEMUX_DATA_OUT-1;
  
  begin

    if (resetN='0') then

      for index in DataInDel'range loop
        DataInReg(index) <= (others =>'0');
        DataInDel(index) <= (others =>'0');
      end loop;
      for index in LinkPipe'range loop
        LinkPipe(index).PartData <= (others => '0');
        LinkPipe(index).PartCode <= (others => '0');
        LinkPipe(index).PartTime <= (others => '0');
        LinkPipe(index).EndData  <= '0';
      end loop;
      PartMux <= (others => 0);-- -1);
      EndDataReg <= '0';
      IndexCnt <= 0;-- -1;

    elsif (clock'event and clock='1') then

      for index in DataInDel'range loop
        FirstPos:=index*DATA_PARTITION_SIZE;
        DataInReg(index) <= data_in(FirstPos+DATA_PARTITION_SIZE-1 downto FirstPos);
      end loop;
      DataInDel <= DataInReg;
      IndexCntVar := maximum(IndexCnt-1,-1);

      -- multiplexer selecting
      for time_pos in TIME_PARTITION_CODE_MAX downto 0 loop
	if (PartMux(time_pos)=-1) then
	  if (time_pos=TIME_PARTITION_CODE_MAX) then
            LinkPipe(TIME_PARTITION_CODE_MAX).PartData <= (others => '0');
            LinkPipe(TIME_PARTITION_CODE_MAX).PartCode <= (others => '0');
            LinkPipe(TIME_PARTITION_CODE_MAX).PartTime <= (others => '0');
            LinkPipe(TIME_PARTITION_CODE_MAX).EndData  <= '0';
	  else
            LinkPipe(time_pos) <= LinkPipe(time_pos+1);
	  end if;
        else
          LinkPipe(time_pos).PartData <= DataInDel(PartMux(time_pos));
          LinkPipe(time_pos).PartCode <= TSLVconv(PartMux(time_pos),DATA_PARTITION_CODE_SIZE);
          LinkPipe(time_pos).PartTime <= TSLVconv(time_pos,TIME_PARTITION_CODE_SIZE);
	  if (time_pos=TIME_PARTITION_CODE_MAX) then
            LinkPipe(time_pos).EndData  <= EndDataReg;
	  else
            LinkPipe(time_pos).EndData  <= '0';
	  end if;
        end if;
      end loop;
      -- multiplexers setup
      PartMuxVar := (others => -1);
      for part_pos in DATA_PARTITION_CODE_MAX downto 0 loop
	EndDataVar := '0';
        if (OR_REDUCE(DataInReg(part_pos))='1') then
	  if(IndexCntVar<TIME_PARTITION_CODE_MAX) then
	    IndexCntVar := IndexCntVar+1;
	    PartMuxVar(IndexCntVar) := part_pos;
	  else
            EndDataVar := '1';
	  end if;
        end if;
      end loop;

      PartMux    <= PartMuxVar;
      EndDataReg <= EndDataVar;
      IndexCnt   <= IndexCntVar;
    end if;

    part_data <= LinkPipe(0).PartData;
    part_code <= LinkPipe(0).PartCode;
    part_time <= LinkPipe(0).PartTime;
    end_data  <= LinkPipe(0).EndData;

  end process;

end behaviour;

-------------------------------------------------------------------
-- lcoder module
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
use work.std_logic_1164_ktp.all;
use work.tridaq.all;
use work.link.all;

entity fcoder2 is
  port (
    resetN				:in  TSL;
    clock				:in  TSL;
    data_in				:in  TChanOut;
    part_data				:out TPartData;
    part_code				:out TPartCode;
    part_time				:out TPartTime;
    end_data				:out TSL
  );
end fcoder2;

architecture behaviour of fcoder2 is

  type TLinkPos	is record
    PartData				:TPartData;
    PartCode				:TPartCode;    
    PartTime				:TPartTime;
    EndData				:TSL;
  end record;
  type TLinkVec				is array (TIME_PARTITION_CODE_MAX downto 0) of TLinkPos;

  subtype TPartDel			is TI range -1 to TIME_PARTITION_CODE_MAX;
  type TPartDelVec			is array (DATA_PARTITION_CODE_MAX downto 0) of TPartDel;

  type TPartDataVec			is array (DATA_PARTITION_CODE_MAX downto 0) of TPartData;

  signal DataInReg			:TPartDataVec;
  signal DataInDel			:TPartDataVec;
  signal LinkPipe			:TLinkVec;
  signal BusyPart			:TSLV(DATA_PARTITION_CODE_MAX downto 0);
  signal PartDelVec			:TPartDelVec;
  signal EndDataReg			:TSL;
  signal IndexCnt			:TI range -1 to TIME_PARTITION_CODE_MAX;
   
begin

  process(DataInReg)
  begin
    for part_pos in DataInDel'range loop
      BusyPart(part_pos) <= OR_REDUCE(DataInReg(part_pos));
    end loop;
  end process;

  process(clock, resetN)
    variable LinkPipeVar		:TLinkVec;
    variable EndDataVar			:TSL;
    variable FirstPos			:TI range 0 to LDEMUX_DATA_OUT-1;
    variable BusyPrevPartCount		:TN;
  
  begin

    if (resetN='0') then

      for index in DataInDel'range loop
        DataInReg(index) <= (others =>'0');
        DataInDel(index) <= (others =>'0');
      end loop;
      for index in LinkPipe'range loop
        LinkPipe(index).PartData <= (others => '0');
        LinkPipe(index).PartCode <= (others => '0');
        LinkPipe(index).PartTime <= (others => '0');
        LinkPipe(index).EndData  <= '0';
      end loop;
      PartDelVec <= (others => 0);-- -1);
      EndDataReg <= '0';
      IndexCnt <= 0;-- -1;

    elsif (clock'event and clock='1') then

      for index in DataInDel'range loop
        FirstPos:=index*DATA_PARTITION_SIZE;
        DataInReg(index) <= data_in(FirstPos+DATA_PARTITION_SIZE-1 downto FirstPos);
      end loop;
      DataInDel <= DataInReg;

      -- multiplexer selecting
      LinkPipeVar := LinkPipe;
      LinkPipeVar(TIME_PARTITION_CODE_MAX-1 downto 0) := LinkPipeVar(TIME_PARTITION_CODE_MAX downto 1);
      LinkPipeVar(TIME_PARTITION_CODE_MAX).PartData := (others => '0');
      LinkPipeVar(TIME_PARTITION_CODE_MAX).PartCode := (others => '0');
      LinkPipeVar(TIME_PARTITION_CODE_MAX).PartTime := (others => '0');
      LinkPipeVar(TIME_PARTITION_CODE_MAX).EndData  := '0';
      for part_pos in DATA_PARTITION_CODE_MAX downto 0 loop
	if (PartDelVec(part_pos)/=-1) then
          LinkPipeVar(PartDelVec(part_pos)).PartData := DataInDel(part_pos);
          LinkPipeVar(PartDelVec(part_pos)).PartCode := TSLVconv(part_pos,DATA_PARTITION_CODE_SIZE);
          LinkPipeVar(PartDelVec(part_pos)).PartTime := TSLVconv(PartDelVec(part_pos),TIME_PARTITION_CODE_SIZE);
	  if (PartDelVec(part_pos)=TIME_PARTITION_CODE_MAX) then
            LinkPipeVar(PartDelVec(part_pos)).EndData  := EndDataReg;
	  else
            LinkPipeVar(PartDelVec(part_pos)).EndData  := '0';
	  end if;
        end if;
      end loop;
      LinkPipe <= LinkPipeVar;
      -- multiplexers setup
      for part_pos in DATA_PARTITION_CODE_MAX downto 0 loop
	EndDataVar := '0';
        if (BusyPart(part_pos)='1') then
	  BusyPrevPartCount := SLVBit1Count(BusyPart(DATA_PARTITION_CODE_MAX downto part_pos));
	  if(BusyPrevPartCount+IndexCnt<=TIME_PARTITION_CODE_MAX) then
	    PartDelVec(part_pos) <= BusyPrevPartCount+IndexCnt;
	  else
            EndDataVar := '1';
            PartDelVec(part_pos) <= -1;
	  end if;
	else
          PartDelVec(part_pos) <= -1;
        end if;
      end loop;

      EndDataReg <= EndDataVar;
      IndexCnt   <= minimum(maximum(IndexCnt+SLVBit1Count(BusyPart)-1,-1),TIME_PARTITION_CODE_MAX);
    end if;

    part_data <= LinkPipe(0).PartData;
    part_code <= LinkPipe(0).PartCode;
    part_time <= LinkPipe(0).PartTime;
    end_data  <= LinkPipe(0).EndData;

  end process;

end behaviour;

-------------------------------------------------------------------
-- lcoder module
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
use work.std_logic_1164_ktp.all;
use work.tridaq.all;
use work.link.all;

entity fcoder is
  port (
    resetN				:in  TSL;
    clock				:in  TSL;
    data_in				:in  TChanOut;
    part_data				:out TPartData;
    part_code				:out TPartCode;
    part_time				:out TPartTime;
    end_data				:out TSL
  );
end fcoder;

architecture behaviour of fcoder is

  subtype TPipePos			is TSLV(data_in'range);
  type	  TPipe				is array(TIME_PARTITION_CODE_MAX downto 0) of TPipePos;
  subtype TPipeTimePos			is TN range 0 to TIME_PARTITION_CODE_MAX;
  type	  TPipeTime			is array(TIME_PARTITION_CODE_MAX downto 0) of TPipeTimePos;
  type    TPartDataVec			is array (DATA_PARTITION_CODE_MAX downto 0) of TPartData;

  signal  DataInReg			:TChanOut;
  signal  Pipe				:TPipe;
  signal  PipeTime			:TPipeTime;
  signal  PipeIndex			:TN range 0 to TIME_PARTITION_CODE_MAX;
  signal  PartDataSig			:TPartDataVec;
  signal  BusyPartReg			:TSLV(DATA_PARTITION_CODE_MAX downto 0);
  signal  BusyNextPartsReg		:TSLV(DATA_PARTITION_CODE_MAX-1 downto 0);
  signal  PartDataReg			:TPartData;
  signal  PartCodeReg			:TPartCode;
  signal  PartTimeReg			:TPartTime;
  signal  EndDataReg			:TSL;

begin

  process(resetN,Pipe)
    variable FirstPos			:TI range 0 to LDEMUX_DATA_OUT-1;
  begin
    for part_pos in PartDataSig'range loop
      FirstPos:=part_pos*DATA_PARTITION_SIZE;
      PartDataSig(part_pos) <= Pipe(0)(FirstPos+DATA_PARTITION_SIZE-1 downto FirstPos);
    end loop;
  end process;

  process(clock, resetN)
    variable PipeIndexVar		:TN range 0 to TIME_PARTITION_CODE_MAX;
    variable ShiftVar			:TL;
    variable FirstPos			:TI range 0 to LDEMUX_DATA_OUT-1;
  begin

    if (resetN='0') then

      DataInReg        <= (others => '0');
      Pipe             <= (Pipe'range => (others => '0'));
      PipeTime         <= (PipeTime'range => 0);
      PipeIndex        <= 0;
      BusyPartReg      <= (others => '0');
      BusyNextPartsReg <= (others => '0');
      PartDataReg      <= (others => '0');
      PartCodeReg      <= (others => '0');
      PartTimeReg      <= (others => '0');
      EndDataReg       <= '0';

    elsif (clock'event and clock='1') then
    
      PipeIndexVar := PipeIndex;
      ShiftVar := FALSE;
      
      DataInReg <= data_in;
    
      -- partitioning
      if(PipeIndexVar=0) then
        PartDataReg      <= (others => '0');
        PartCodeReg      <= (others => '1');
        PartTimeReg      <= (others => '1');
        BusyPartReg      <= (others => '0');
        BusyNextPartsReg <= (others => '0');
      else
        for part_pos in DATA_PARTITION_CODE_MAX downto 0 loop
          if(BusyPartReg(part_pos)='1') then
            PartDataReg <= PartDataSig(part_pos);
            PartCodeReg <= TSLVconv(part_pos,PartCodeReg'length);
            PartTimeReg <= TSLVconv(PipeTime(0),PartTimeREg'length);
	    if (PipeTime(0)=TIME_PARTITION_CODE_MAX) then
	      ShiftVar := TRUE;
	      if (part_pos=0) then
                EndDataReg  <= '0';
	      else
                EndDataReg  <= BusyNextPartsReg(part_pos-1);
	      end if;
	    elsif ((part_pos=0) or (BusyNextPartsReg(maximum(part_pos-1,0))='0')) then
	      ShiftVar := TRUE;
              EndDataReg  <= '0';
	    else
	      BusyPartReg(part_pos) <= '0';
              EndDataReg  <= '0';
	    end if;
	    exit;
	  else
	    if (part_pos=0) then
	      ShiftVar := TRUE;
	    end if;
          end if;
        end loop;
      end if;

      -- buffering
      if(ShiftVar=TRUE) then
        if(PipeIndexVar>1) then
          for time_pos in TIME_PARTITION_CODE_MAX downto 1 loop
	    if (time_pos<=PipeIndexVar) then
	      Pipe(time_pos-1) <= Pipe(time_pos);
	      PipeTime(time_pos-1) <= PipeTime(time_pos)+1;
	       if(time_pos=1) then
                 for part_pos in DATA_PARTITION_CODE_MAX downto 0 loop
                   FirstPos:=part_pos*DATA_PARTITION_SIZE;
                   BusyPartReg(part_pos) <= OR_REDUCE(Pipe(1)(FirstPos+DATA_PARTITION_SIZE-1 downto FirstPos));
                 end loop;
                 for part_pos in DATA_PARTITION_CODE_MAX-1 downto 0 loop
                   FirstPos:=part_pos*DATA_PARTITION_SIZE;
                   BusyNextPartsReg(part_pos) <= OR_REDUCE(Pipe(1)(FirstPos+DATA_PARTITION_SIZE-1 downto 0));
                 end loop;
	       end if;
	    end if;
	  end loop;
        end if;
        PipeIndexVar := PipeIndexVar-1;
      else
        if(PipeIndexVar/=0) then
          for time_pos in TIME_PARTITION_CODE_MAX downto 0 loop
	    if (time_pos<PipeIndexVar) then
              PipeTime(time_pos) <= PipeTime(time_pos)+1;
	    end if;
	  end loop;
        end if;
      end if;
      
      if (OR_REDUCE(DataInReg)='1') then
	Pipe(PipeIndexVar) <= DataInReg;
	PipeTime(PipeIndexVar) <= 0;
        if(PipeIndexVar=0) then
          for part_pos in DATA_PARTITION_CODE_MAX downto 0 loop
            FirstPos:=part_pos*DATA_PARTITION_SIZE;
            BusyPartReg(part_pos) <= OR_REDUCE(DataInReg(FirstPos+DATA_PARTITION_SIZE-1 downto FirstPos));
          end loop;
          for part_pos in DATA_PARTITION_CODE_MAX-1 downto 0 loop
            FirstPos:=part_pos*DATA_PARTITION_SIZE;
            BusyNextPartsReg(part_pos) <= OR_REDUCE(DataInReg(FirstPos+DATA_PARTITION_SIZE-1 downto 0));
          end loop;
        end if;
        PipeIndexVar := PipeIndexVar+1;
      end if;

      PipeIndex <= PipeIndexVar;

    end if;

  end process;

  part_data <= PartDataReg;
  part_code <= PartCodeReg;
  part_time <= PartTimeReg;
  end_data  <= EndDataReg;
end behaviour;

-------------------------------------------------------------------
-- link data synchronisation module
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.std_logic_1164_ktp.all;
use work.tridaq.all;

entity synchro is
  port(
    resetN				:in  TSL;
    clock				:in  TSL;
    bcn0				:in  TSL;
    data_in				:in  TSLV(LB_SYNCHRO_DATA-1 downto 0);
    tmark				:in  TSL;
    dphase				:in  TSL;
    data_out				:out TSLV(LINK_DATA_SIZE-1 downto 0);
    clock_quality			:out TSL;
    phase_quality			:out TSL;
    data_parity				:out TSL;
    BCN0_synch				:out TSL;
    BCN_synch				:out TSL;
    latency				:out TSLV(TVLcreate(SYN_PIPE_MAX)-1 downto 0)
  );
end synchro;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use work.std_logic_1164_ktp.all;
use work.vcomponent.all;
use work.LPMComponent.all;
use work.LPMSynchro.all;
use work.tridaq.all;

architecture behaviour of synchro is

  signal   Demux_DataOut	:TSLV(GLA(GLA'left).l downto 0);
  signal   Demux_ClkQuality	:TSL;
  signal   Demux_PhaseQuality	:TSL;
  signal   ClkSynch_DataIn	:TSLV((Demux_DataOut'length+2)-1 downto 0);
  signal   ClkSynch_DataOut	:TSLV(ClkSynch_DataIn'length-1 downto 0);
  signal   ParityReg		:TSL;
  signal   BCN0InSig		:TSL;
  signal   BCN0Synch_DataIn	:TSLV(GLA(GLA_ITEM_BCN).l downto 0);
  signal   BCN0Synch_DataOut	:TSLV(GLA(GLA_ITEM_BCN).l downto 0);
  signal   BCNInSig		:TSLV(LB_BCN_SIZE-1 downto 0);
  signal   BCNCnt		:TSLV(LB_BCN_SIZE-1 downto 0);
  signal   BCNSynchReg		:TSL;
  
begin

  Demux: component STREAM_DEMUX2
    generic map(
      LPM_DATA_WIDTH		=> data_in'length
    )
    port map(
      resetN			=> resetN,
      clock			=> tmark,
      data_in			=> data_in,
      phase_in			=> dphase,
      data_out			=> Demux_DataOut,
      clock_quality		=> Demux_ClkQuality,
      phase_quality		=> Demux_PhaseQuality
    );

  ClkSynch_DataIn <= TSLVconv(Demux_PhaseQuality) & (TSLVconv(Demux_ClkQuality) & Demux_DataOut);

  ClkSynch: component CLOCK_SYNCHRO
    generic map(
      LPM_DATA_WIDTH		=> ClkSynch_DataIn'length,
      INPUT_REGISTERED		=> FALSE,
      OUTPUT_REGISTERED		=> TRUE
    )
    port map(
      resetN			=> resetN,
      clock_in			=> tmark,
      data_in			=> ClkSynch_DataIn,
      clock_out			=> clock,
      data_out			=> ClkSynch_DataOut
    );

  phase_quality <= ClkSynch_DataOut(ClkSynch_DataOut'length-1);
  clock_quality <= ClkSynch_DataOut(ClkSynch_DataOut'length-2);
  BCN0InSig     <= ClkSynch_DataOut(GLA(GLA_ITEM_BCN0).l);
  BCN0Synch_DataIn <= ClkSynch_DataOut(GLA(GLA_ITEM_BCN).l downto 0);

  process(ResetN, clock) begin
    if (ResetN='0') then
      ParityReg <= '0';
    elsif (clock'event and clock='1') then
      ParityReg <= XOR_REDUCE(ClkSynch_DataOut(GLA(GLA'left).l downto 0));
    end if;
  end process;
  
  data_parity <= ParityReg;
  
  BCN0Synch: component BCN0_SYNCHRO
    generic map(
      LPM_DATA_WIDTH		=> BCN0Synch_DataIn'length,
      LPM_PIPE_LEN		=> SYN_PIPE_MAX,
      INPUT_REGISTERED		=> FALSE,
      BCN0_REGISTERED		=> FALSE,
      OUTPUT_REGISTERED		=> TRUE
    )
    port map(
      resetN			=> resetN,
      clock			=> clock,
      data_in			=> BCN0Synch_DataIn,
      bcn0_in			=> BCN0InSig,
      bcn0			=> bcn0,
      data_out			=> BCN0Synch_DataOut,
      pos_out			=> latency,
      synch_out			=> bcn0_synch
    );

  BCNInSig <= TSLVResize(BCN0Synch_DataOut,GLA(GLA_ITEM_BCN));
  data_out <= BCN0Synch_DataOut(GL(GL'left).l downto 0);

  process(ResetN, clock) begin
    if (ResetN='0') then
      BCNCnt      <= (others => '0');
      BCNSynchReg <= '0';
    elsif (clock'event and clock='1') then
      if (bcn0='1') then
        BCNCnt <= (others => '0');
      else
        BCNCnt <= BCNCnt+1;
      end if;
      BCNSynchReg <= TSLconv(BCNInSig=BCNCnt);
    end if;
  end process;
  
  bcn_synch <= BCNSynchReg;

end behaviour;

-------------------------------------------------------------------
-- ldecoder module
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.std_logic_1164_ktp.all;
use work.tridaq.all;
use work.link.all;

entity decoder is
  port (
    resetN				:in  TSL;
    clock				:in  TSL;
    part_data				:in  TPartData;
    part_code				:in  TPartCode;
    part_time				:in  TPartTime;
    end_data				:in  TSL;
    data_out				:out TChanOut;
    end_data_out			:out TSL
  );
end decoder;

architecture behaviour of decoder is

  type TLinkPos is record
    PartData				:TPartData;
    PartCode				:TPartCode;    
    PartTime				:TPartTime;
    EndData				:TSL;
  end record;

  type TLinkVec is array (TIME_PARTITION_CODE_MAX downto 0) of TLinkPos;
  
  signal LinkPipe			:TLinkVec;
  signal DataOutSig			:TChanOut;
  signal EndDataSig			:TSL;
  signal DataOutReg			:TChanOut;
  signal EndDataReg			:TSL;
   
begin

  process(clock, resetN)
  
  begin

    if (resetN='0') then

      for index in LinkPipe'range loop
        LinkPipe(index).PartData <= (others => '0');
        LinkPipe(index).PartCode <= (others => '0');
        LinkPipe(index).PartTime <= (others => '0');
        LinkPipe(index).EndData  <= '0';
      end loop;
      DataOutReg <= (others =>'0');
      EndDataReg <= '0';

    elsif (clock'event and clock='1') then

      LinkPipe(TIME_PARTITION_CODE_MAX).PartData <= part_data;
      LinkPipe(TIME_PARTITION_CODE_MAX).PartCode <= part_code;
      LinkPipe(TIME_PARTITION_CODE_MAX).PartTime <= part_time;
      LinkPipe(TIME_PARTITION_CODE_MAX).EndData  <= end_data;
      LinkPipe(TIME_PARTITION_CODE_MAX-1 downto 0) <= LinkPipe(TIME_PARTITION_CODE_MAX downto 1);

      DataOutReg <= DataOutSig;
      EndDataReg <= EndDataSig;
    end if;
     
  end process;

  process(LinkPipe)
  
    variable DataOutVar			:TChanOut;
    variable FirstPos			:natural range 0 to LDEMUX_DATA_OUT-1;
    variable EndDataVar			:TSL;
    
  begin

    DataOutVar := (others => '1');
    EndDataVar := '1';
    for time_pos in TIME_PARTITION_CODE_MAX downto 0 loop
      if (conv_integer(LinkPipe(time_pos).PartTime)=time_pos) then
        for part_pos in DATA_PARTITION_CODE_MAX downto 0 loop
          if (conv_integer(LinkPipe(time_pos).PartCode)=part_pos) then
            FirstPos:=part_pos*DATA_PARTITION_SIZE;
            DataOutVar(FirstPos+DATA_PARTITION_SIZE-1 downto FirstPos)		-->
            := DataOutVar(FirstPos+DATA_PARTITION_SIZE-1 downto FirstPos) and	-->
            LinkPipe(time_pos).PartData;
            EndDataVar := EndDataVar and LinkPipe(time_pos).EndData;
          end if;
        end loop;
      end if;
    end loop;
      
    DataOutSig <= DataOutVar;
    EndDataSig <= EndDataVar;
     
  end process;

  data_out     <= DataOutReg;
  end_data_out <= EndDataReg;
       
end behaviour;
