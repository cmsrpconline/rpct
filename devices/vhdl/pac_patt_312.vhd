library ieee;
use ieee.std_logic_1164.all;
use work.std_logic_1164_ktp.all;
use work.pac_packet6.all;

package pac_patt6 is

  constant PattTable    :TPattTable := (
--	blk	dir	lA	lB	C	lD	lE	code	 	l1	l2	l3	l4	pt
 (	0,	-1,	1,	0,	0,	0,	0,	31 )	--	140.0
,(	0,	-1,	0,	0,	0,	1,	1,	22 )	--	40.0
,(	0,	-1,	0,	-1,	-1,	-1,	-1,	20 )	--	30.0
,(	0,	-1,	1,	-1,	-1,	-2,	-2,	19 )	--	25.0
,(	0,	-1,	1,	0,	0,	-1,	-1,	18 )	--	20.0
,(	0,	-1,	1,	0,	0,	1,	1,	17 )	--	18.0
,(	0,	-1,	0,	1,	1,	2,	3,	16 )	--	16.0
,(	0,	-1,	1,	-1,	-2,	-3,	-4,	15 )	--	14.0
,(	0,	-1,	2,	-1,	-1,	-2,	-2,	14 )	--	12.0
,(	0,	-1,	1,	-1,	-2,	-4,	-5,	13 )	--	10.0
,(	0,	-1,	2,	-1,	-2,	-4,	-5,	12 )	--	8.0
,(	0,	-1,	2,	-1,	-2,	-3,	-3,	11 )	--	7.0
,(	0,	-1,	3,	-1,	-2,	-4,	-5,	10 )	--	6.0
,(	0,	-1,	3,	-2,	-3,	-4,	-5,	9 )	--	5.0
,(	0,	-1,	4,	-1,	-2,	-3,	-4,	8 )	--	4.5
,(	0,	-1,	4,	-3,	-3,	-4,	-5,	7 )	--	4.0
,(	0,	-1,	5,	-1,	-2,	-3,	-3,	7 )	--	4.0
,(	0,	-1,	6,	4,	4,	3,	2,	7 )	--	4.0
,(	0,	-1,	6,	5,	5,	5,	2,	7 )	--	4.0
,(	0,	1,	0,	0,	0,	0,	0,	31 )	--	140.0
,(	0,	1,	-1,	0,	0,	0,	0,	31 )	--	140.0
,(	0,	1,	0,	0,	0,	-1,	-1,	22 )	--	40.0
,(	0,	1,	0,	1,	1,	1,	1,	20 )	--	30.0
,(	0,	1,	-1,	1,	1,	2,	2,	19 )	--	25.0
,(	0,	1,	-1,	0,	0,	1,	1,	18 )	--	20.0
,(	0,	1,	-1,	0,	0,	-1,	-1,	17 )	--	18.0
,(	0,	1,	0,	-1,	-1,	-2,	-3,	16 )	--	16.0
,(	0,	1,	-1,	1,	2,	3,	4,	15 )	--	14.0
,(	0,	1,	-2,	1,	1,	2,	2,	14 )	--	12.0
,(	0,	1,	-1,	1,	2,	4,	5,	13 )	--	10.0
,(	0,	1,	-2,	1,	2,	4,	5,	12 )	--	8.0
,(	0,	1,	-2,	1,	2,	3,	3,	11 )	--	7.0
,(	0,	1,	-3,	1,	2,	4,	5,	10 )	--	6.0
,(	0,	1,	-3,	2,	3,	4,	5,	9 )	--	5.0
,(	0,	1,	-4,	1,	2,	3,	4,	8 )	--	4.5
,(	0,	1,	-4,	3,	3,	4,	5,	7 )	--	4.0
,(	0,	1,	-5,	1,	2,	3,	3,	7 )	--	4.0
,(	0,	1,	-6,	-4,	-4,	-3,	-2,	7 )	--	4.0
,(	0,	1,	-6,	-5,	-5,	-5,	-2,	7 )	--	4.0
,(	1,	-1,	1,	0,	0,	0,	0,	31 )	--	140.0
,(	1,	-1,	0,	0,	0,	1,	1,	22 )	--	40.0
,(	1,	-1,	0,	-1,	-1,	-1,	-1,	20 )	--	30.0
,(	1,	-1,	1,	-1,	-1,	-2,	-2,	19 )	--	25.0
,(	1,	-1,	1,	0,	0,	-1,	-1,	18 )	--	20.0
,(	1,	-1,	1,	0,	0,	1,	1,	17 )	--	18.0
,(	1,	-1,	0,	1,	1,	2,	3,	16 )	--	16.0
,(	1,	-1,	1,	-1,	-2,	-3,	-4,	15 )	--	14.0
,(	1,	-1,	2,	-1,	-1,	-2,	-2,	14 )	--	12.0
,(	1,	-1,	1,	-1,	-2,	-4,	-5,	13 )	--	10.0
,(	1,	-1,	2,	-1,	-2,	-4,	-5,	12 )	--	8.0
,(	1,	-1,	2,	-1,	-2,	-3,	-3,	11 )	--	7.0
,(	1,	-1,	3,	-1,	-2,	-4,	-5,	10 )	--	6.0
,(	1,	-1,	3,	-2,	-3,	-4,	-5,	9 )	--	5.0
,(	1,	-1,	4,	-1,	-2,	-3,	-4,	8 )	--	4.5
,(	1,	-1,	4,	-3,	-3,	-4,	-5,	7 )	--	4.0
,(	1,	-1,	5,	-1,	-2,	-3,	-3,	7 )	--	4.0
,(	1,	-1,	6,	4,	4,	3,	2,	7 )	--	4.0
,(	1,	-1,	6,	5,	5,	5,	2,	7 )	--	4.0
,(	1,	1,	0,	0,	0,	0,	0,	31 )	--	140.0
,(	1,	1,	-1,	0,	0,	0,	0,	31 )	--	140.0
,(	1,	1,	0,	0,	0,	-1,	-1,	22 )	--	40.0
,(	1,	1,	0,	1,	1,	1,	1,	20 )	--	30.0
,(	1,	1,	-1,	1,	1,	2,	2,	19 )	--	25.0
,(	1,	1,	-1,	0,	0,	1,	1,	18 )	--	20.0
,(	1,	1,	-1,	0,	0,	-1,	-1,	17 )	--	18.0
,(	1,	1,	0,	-1,	-1,	-2,	-3,	16 )	--	16.0
,(	1,	1,	-1,	1,	2,	3,	4,	15 )	--	14.0
,(	1,	1,	-2,	1,	1,	2,	2,	14 )	--	12.0
,(	1,	1,	-1,	1,	2,	4,	5,	13 )	--	10.0
,(	1,	1,	-2,	1,	2,	4,	5,	12 )	--	8.0
,(	1,	1,	-2,	1,	2,	3,	3,	11 )	--	7.0
,(	1,	1,	-3,	1,	2,	4,	5,	10 )	--	6.0
,(	1,	1,	-3,	2,	3,	4,	5,	9 )	--	5.0
,(	1,	1,	-4,	1,	2,	3,	4,	8 )	--	4.5
,(	1,	1,	-4,	3,	3,	4,	5,	7 )	--	4.0
,(	1,	1,	-5,	1,	2,	3,	3,	7 )	--	4.0
,(	1,	1,	-6,	-4,	-4,	-3,	-2,	7 )	--	4.0
,(	1,	1,	-6,	-5,	-5,	-5,	-2,	7 )	--	4.0
,(	2,	-1,	1,	0,	0,	0,	0,	31 )	--	140.0
,(	2,	-1,	0,	0,	0,	1,	1,	22 )	--	40.0
,(	2,	-1,	0,	-1,	-1,	-1,	-1,	20 )	--	30.0
,(	2,	-1,	1,	-1,	-1,	-2,	-2,	19 )	--	25.0
,(	2,	-1,	1,	0,	0,	-1,	-1,	18 )	--	20.0
,(	2,	-1,	1,	0,	0,	1,	1,	17 )	--	18.0
,(	2,	-1,	0,	1,	1,	2,	3,	16 )	--	16.0
,(	2,	-1,	1,	-1,	-2,	-3,	-4,	15 )	--	14.0
,(	2,	-1,	2,	-1,	-1,	-2,	-2,	14 )	--	12.0
,(	2,	-1,	1,	-1,	-2,	-4,	-5,	13 )	--	10.0
,(	2,	-1,	2,	-1,	-2,	-4,	-5,	12 )	--	8.0
,(	2,	-1,	2,	-1,	-2,	-3,	-3,	11 )	--	7.0
,(	2,	-1,	3,	-1,	-2,	-4,	-5,	10 )	--	6.0
,(	2,	-1,	3,	-2,	-3,	-4,	-5,	9 )	--	5.0
,(	2,	-1,	4,	-1,	-2,	-3,	-4,	8 )	--	4.5
,(	2,	-1,	4,	-3,	-3,	-4,	-5,	7 )	--	4.0
,(	2,	-1,	5,	-1,	-2,	-3,	-3,	7 )	--	4.0
,(	2,	-1,	6,	4,	4,	3,	2,	7 )	--	4.0
,(	2,	-1,	6,	5,	5,	5,	2,	7 )	--	4.0
,(	2,	1,	0,	0,	0,	0,	0,	31 )	--	140.0
,(	2,	1,	-1,	0,	0,	0,	0,	31 )	--	140.0
,(	2,	1,	0,	0,	0,	-1,	-1,	22 )	--	40.0
,(	2,	1,	0,	1,	1,	1,	1,	20 )	--	30.0
,(	2,	1,	-1,	1,	1,	2,	2,	19 )	--	25.0
,(	2,	1,	-1,	0,	0,	1,	1,	18 )	--	20.0
,(	2,	1,	-1,	0,	0,	-1,	-1,	17 )	--	18.0
,(	2,	1,	0,	-1,	-1,	-2,	-3,	16 )	--	16.0
,(	2,	1,	-1,	1,	2,	3,	4,	15 )	--	14.0
,(	2,	1,	-2,	1,	1,	2,	2,	14 )	--	12.0
,(	2,	1,	-1,	1,	2,	4,	5,	13 )	--	10.0
,(	2,	1,	-2,	1,	2,	4,	5,	12 )	--	8.0
,(	2,	1,	-2,	1,	2,	3,	3,	11 )	--	7.0
,(	2,	1,	-3,	1,	2,	4,	5,	10 )	--	6.0
,(	2,	1,	-3,	2,	3,	4,	5,	9 )	--	5.0
,(	2,	1,	-4,	1,	2,	3,	4,	8 )	--	4.5
,(	2,	1,	-4,	3,	3,	4,	5,	7 )	--	4.0
,(	2,	1,	-5,	1,	2,	3,	3,	7 )	--	4.0
,(	2,	1,	-6,	-4,	-4,	-3,	-2,	7 )	--	4.0
,(	2,	1,	-6,	-5,	-5,	-5,	-2,	7 )	--	4.0	
,(	3,	-1,	1,	0,	0,	0,	0,	31 )	--	140.0
,(	3,	-1,	0,	0,	0,	1,	1,	22 )	--	40.0
,(	3,	-1,	0,	-1,	-1,	-1,	-1,	20 )	--	30.0
,(	3,	-1,	1,	-1,	-1,	-2,	-2,	19 )	--	25.0
,(	3,	-1,	1,	0,	0,	-1,	-1,	18 )	--	20.0
,(	3,	-1,	1,	0,	0,	1,	1,	17 )	--	18.0
,(	3,	-1,	0,	1,	1,	2,	3,	16 )	--	16.0
,(	3,	-1,	1,	-1,	-2,	-3,	-4,	15 )	--	14.0
,(	3,	-1,	2,	-1,	-1,	-2,	-2,	14 )	--	12.0
,(	3,	-1,	1,	-1,	-2,	-4,	-5,	13 )	--	10.0
,(	3,	-1,	2,	-1,	-2,	-4,	-5,	12 )	--	8.0
,(	3,	-1,	2,	-1,	-2,	-3,	-3,	11 )	--	7.0
,(	3,	-1,	3,	-1,	-2,	-4,	-5,	10 )	--	6.0
,(	3,	-1,	3,	-2,	-3,	-4,	-5,	9 )	--	5.0
,(	3,	-1,	4,	-1,	-2,	-3,	-4,	8 )	--	4.5
,(	3,	-1,	4,	-3,	-3,	-4,	-5,	7 )	--	4.0
,(	3,	-1,	5,	-1,	-2,	-3,	-3,	7 )	--	4.0
,(	3,	-1,	6,	4,	4,	3,	2,	7 )	--	4.0
,(	3,	-1,	6,	5,	5,	5,	2,	7 )	--	4.0
,(	3,	1,	0,	0,	0,	0,	0,	31 )	--	140.0
,(	3,	1,	-1,	0,	0,	0,	0,	31 )	--	140.0
,(	3,	1,	0,	0,	0,	-1,	-1,	22 )	--	40.0
,(	3,	1,	0,	1,	1,	1,	1,	20 )	--	30.0
,(	3,	1,	-1,	1,	1,	2,	2,	19 )	--	25.0
,(	3,	1,	-1,	0,	0,	1,	1,	18 )	--	20.0
,(	3,	1,	-1,	0,	0,	-1,	-1,	17 )	--	18.0
,(	3,	1,	0,	-1,	-1,	-2,	-3,	16 )	--	16.0
,(	3,	1,	-1,	1,	2,	3,	4,	15 )	--	14.0
,(	3,	1,	-2,	1,	1,	2,	2,	14 )	--	12.0
,(	3,	1,	-1,	1,	2,	4,	5,	13 )	--	10.0
,(	3,	1,	-2,	1,	2,	4,	5,	12 )	--	8.0
,(	3,	1,	-2,	1,	2,	3,	3,	11 )	--	7.0
,(	3,	1,	-3,	1,	2,	4,	5,	10 )	--	6.0
,(	3,	1,	-3,	2,	3,	4,	5,	9 )	--	5.0
,(	3,	1,	-4,	1,	2,	3,	4,	8 )	--	4.5
,(	3,	1,	-4,	3,	3,	4,	5,	7 )	--	4.0
,(	3,	1,	-5,	1,	2,	3,	3,	7 )	--	4.0
,(	3,	1,	-6,	-4,	-4,	-3,	-2,	7 )	--	4.0
,(	3,	1,	-6,	-5,	-5,	-5,	-2,	7 )	--	4.0
,(	4,	-1,	1,	0,	0,	0,	0,	31 )	--	140.0
,(	4,	-1,	0,	0,	0,	1,	1,	22 )	--	40.0
,(	4,	-1,	0,	-1,	-1,	-1,	-1,	20 )	--	30.0
,(	4,	-1,	1,	-1,	-1,	-2,	-2,	19 )	--	25.0
,(	4,	-1,	1,	0,	0,	-1,	-1,	18 )	--	20.0
,(	4,	-1,	1,	0,	0,	1,	1,	17 )	--	18.0
,(	4,	-1,	0,	1,	1,	2,	3,	16 )	--	16.0
,(	4,	-1,	1,	-1,	-2,	-3,	-4,	15 )	--	14.0
,(	4,	-1,	2,	-1,	-1,	-2,	-2,	14 )	--	12.0
,(	4,	-1,	1,	-1,	-2,	-4,	-5,	13 )	--	10.0
,(	4,	-1,	2,	-1,	-2,	-4,	-5,	12 )	--	8.0
,(	4,	-1,	2,	-1,	-2,	-3,	-3,	11 )	--	7.0
,(	4,	-1,	3,	-1,	-2,	-4,	-5,	10 )	--	6.0
,(	4,	-1,	3,	-2,	-3,	-4,	-5,	9 )	--	5.0
,(	4,	-1,	4,	-1,	-2,	-3,	-4,	8 )	--	4.5
,(	4,	-1,	4,	-3,	-3,	-4,	-5,	7 )	--	4.0
,(	4,	-1,	5,	-1,	-2,	-3,	-3,	7 )	--	4.0
,(	4,	-1,	6,	4,	4,	3,	2,	7 )	--	4.0
,(	4,	-1,	6,	5,	5,	5,	2,	7 )	--	4.0
,(	4,	1,	0,	0,	0,	0,	0,	31 )	--	140.0
,(	4,	1,	-1,	0,	0,	0,	0,	31 )	--	140.0
,(	4,	1,	0,	0,	0,	-1,	-1,	22 )	--	40.0
,(	4,	1,	0,	1,	1,	1,	1,	20 )	--	30.0
,(	4,	1,	-1,	1,	1,	2,	2,	19 )	--	25.0
,(	4,	1,	-1,	0,	0,	1,	1,	18 )	--	20.0
,(	4,	1,	-1,	0,	0,	-1,	-1,	17 )	--	18.0
,(	4,	1,	0,	-1,	-1,	-2,	-3,	16 )	--	16.0
,(	4,	1,	-1,	1,	2,	3,	4,	15 )	--	14.0
,(	4,	1,	-2,	1,	1,	2,	2,	14 )	--	12.0
,(	4,	1,	-1,	1,	2,	4,	5,	13 )	--	10.0
,(	4,	1,	-2,	1,	2,	4,	5,	12 )	--	8.0
,(	4,	1,	-2,	1,	2,	3,	3,	11 )	--	7.0
,(	4,	1,	-3,	1,	2,	4,	5,	10 )	--	6.0
,(	4,	1,	-3,	2,	3,	4,	5,	9 )	--	5.0
,(	4,	1,	-4,	1,	2,	3,	4,	8 )	--	4.5
,(	4,	1,	-4,	3,	3,	4,	5,	7 )	--	4.0
,(	4,	1,	-5,	1,	2,	3,	3,	7 )	--	4.0
,(	4,	1,	-6,	-4,	-4,	-3,	-2,	7 )	--	4.0
,(	4,	1,	-6,	-5,	-5,	-5,	-2,	7 )	--	4.0		 
,(	5,	-1,	1,	0,	0,	0,	0,	31 )	--	140.0
,(	5,	-1,	0,	0,	0,	1,	1,	22 )	--	40.0
,(	5,	-1,	0,	-1,	-1,	-1,	-1,	20 )	--	30.0
,(	5,	-1,	1,	-1,	-1,	-2,	-2,	19 )	--	25.0
,(	5,	-1,	1,	0,	0,	-1,	-1,	18 )	--	20.0
,(	5,	-1,	1,	0,	0,	1,	1,	17 )	--	18.0
,(	5,	-1,	0,	1,	1,	2,	3,	16 )	--	16.0
,(	5,	-1,	1,	-1,	-2,	-3,	-4,	15 )	--	14.0
,(	5,	-1,	2,	-1,	-1,	-2,	-2,	14 )	--	12.0
,(	5,	-1,	1,	-1,	-2,	-4,	-5,	13 )	--	10.0
,(	5,	-1,	2,	-1,	-2,	-4,	-5,	12 )	--	8.0
,(	5,	-1,	2,	-1,	-2,	-3,	-3,	11 )	--	7.0
,(	5,	-1,	3,	-1,	-2,	-4,	-5,	10 )	--	6.0
,(	5,	-1,	3,	-2,	-3,	-4,	-5,	9 )	--	5.0
,(	5,	-1,	4,	-1,	-2,	-3,	-4,	8 )	--	4.5
,(	5,	-1,	4,	-3,	-3,	-4,	-5,	7 )	--	4.0
,(	5,	-1,	5,	-1,	-2,	-3,	-3,	7 )	--	4.0
,(	5,	-1,	6,	4,	4,	3,	2,	7 )	--	4.0
,(	5,	-1,	6,	5,	5,	5,	2,	7 )	--	4.0
,(	5,	1,	0,	0,	0,	0,	0,	31 )	--	140.0
,(	5,	1,	-1,	0,	0,	0,	0,	31 )	--	140.0
,(	5,	1,	0,	0,	0,	-1,	-1,	22 )	--	40.0
,(	5,	1,	0,	1,	1,	1,	1,	20 )	--	30.0
,(	5,	1,	-1,	1,	1,	2,	2,	19 )	--	25.0
,(	5,	1,	-1,	0,	0,	1,	1,	18 )	--	20.0
,(	5,	1,	-1,	0,	0,	-1,	-1,	17 )	--	18.0
,(	5,	1,	0,	-1,	-1,	-2,	-3,	16 )	--	16.0
,(	5,	1,	-1,	1,	2,	3,	4,	15 )	--	14.0
,(	5,	1,	-2,	1,	1,	2,	2,	14 )	--	12.0
,(	5,	1,	-1,	1,	2,	4,	5,	13 )	--	10.0
,(	5,	1,	-2,	1,	2,	4,	5,	12 )	--	8.0
,(	5,	1,	-2,	1,	2,	3,	3,	11 )	--	7.0
,(	5,	1,	-3,	1,	2,	4,	5,	10 )	--	6.0
,(	5,	1,	-3,	2,	3,	4,	5,	9 )	--	5.0
,(	5,	1,	-4,	1,	2,	3,	4,	8 )	--	4.5
,(	5,	1,	-4,	3,	3,	4,	5,	7 )	--	4.0
,(	5,	1,	-5,	1,	2,	3,	3,	7 )	--	4.0
,(	5,	1,	-6,	-4,	-4,	-3,	-2,	7 )	--	4.0
,(	5,	1,	-6,	-5,	-5,	-5,	-2,	7 )	--	4.0
,(	6,	-1,	1,	0,	0,	0,	0,	31 )	--	140.0
,(	6,	-1,	0,	0,	0,	1,	1,	22 )	--	40.0
,(	6,	-1,	0,	-1,	-1,	-1,	-1,	20 )	--	30.0
,(	6,	-1,	1,	-1,	-1,	-2,	-2,	19 )	--	25.0
,(	6,	-1,	1,	0,	0,	-1,	-1,	18 )	--	20.0
,(	6,	-1,	1,	0,	0,	1,	1,	17 )	--	18.0
,(	6,	-1,	0,	1,	1,	2,	3,	16 )	--	16.0
,(	6,	-1,	1,	-1,	-2,	-3,	-4,	15 )	--	14.0
,(	6,	-1,	2,	-1,	-1,	-2,	-2,	14 )	--	12.0
,(	6,	-1,	1,	-1,	-2,	-4,	-5,	13 )	--	10.0
,(	6,	-1,	2,	-1,	-2,	-4,	-5,	12 )	--	8.0
,(	6,	-1,	2,	-1,	-2,	-3,	-3,	11 )	--	7.0
,(	6,	-1,	3,	-1,	-2,	-4,	-5,	10 )	--	6.0
,(	6,	-1,	3,	-2,	-3,	-4,	-5,	9 )	--	5.0
,(	6,	-1,	4,	-1,	-2,	-3,	-4,	8 )	--	4.5
,(	6,	-1,	4,	-3,	-3,	-4,	-5,	7 )	--	4.0
,(	6,	-1,	5,	-1,	-2,	-3,	-3,	7 )	--	4.0
,(	6,	-1,	6,	4,	4,	3,	2,	7 )	--	4.0
,(	6,	-1,	6,	5,	5,	5,	2,	7 )	--	4.0
,(	6,	1,	0,	0,	0,	0,	0,	31 )	--	140.0
,(	6,	1,	-1,	0,	0,	0,	0,	31 )	--	140.0
,(	6,	1,	0,	0,	0,	-1,	-1,	22 )	--	40.0
,(	6,	1,	0,	1,	1,	1,	1,	20 )	--	30.0
,(	6,	1,	-1,	1,	1,	2,	2,	19 )	--	25.0
,(	6,	1,	-1,	0,	0,	1,	1,	18 )	--	20.0
,(	6,	1,	-1,	0,	0,	-1,	-1,	17 )	--	18.0
,(	6,	1,	0,	-1,	-1,	-2,	-3,	16 )	--	16.0
,(	6,	1,	-1,	1,	2,	3,	4,	15 )	--	14.0
,(	6,	1,	-2,	1,	1,	2,	2,	14 )	--	12.0
,(	6,	1,	-1,	1,	2,	4,	5,	13 )	--	10.0
,(	6,	1,	-2,	1,	2,	4,	5,	12 )	--	8.0
,(	6,	1,	-2,	1,	2,	3,	3,	11 )	--	7.0
,(	6,	1,	-3,	1,	2,	4,	5,	10 )	--	6.0
,(	6,	1,	-3,	2,	3,	4,	5,	9 )	--	5.0
,(	6,	1,	-4,	1,	2,	3,	4,	8 )	--	4.5
,(	6,	1,	-4,	3,	3,	4,	5,	7 )	--	4.0
,(	6,	1,	-5,	1,	2,	3,	3,	7 )	--	4.0
,(	6,	1,	-6,	-4,	-4,	-3,	-2,	7 )	--	4.0
,(	6,	1,	-6,	-5,	-5,	-5,	-2,	7 )	--	4.0	 
,(	7,	-1,	1,	0,	0,	0,	0,	31 )	--	140.0
,(	7,	-1,	0,	0,	0,	1,	1,	22 )	--	40.0
,(	7,	-1,	0,	-1,	-1,	-1,	-1,	20 )	--	30.0
,(	7,	-1,	1,	-1,	-1,	-2,	-2,	19 )	--	25.0
,(	7,	-1,	1,	0,	0,	-1,	-1,	18 )	--	20.0
,(	7,	-1,	1,	0,	0,	1,	1,	17 )	--	18.0
,(	7,	-1,	0,	1,	1,	2,	3,	16 )	--	16.0
,(	7,	-1,	1,	-1,	-2,	-3,	-4,	15 )	--	14.0
,(	7,	-1,	2,	-1,	-1,	-2,	-2,	14 )	--	12.0
,(	7,	-1,	1,	-1,	-2,	-4,	-5,	13 )	--	10.0
,(	7,	-1,	2,	-1,	-2,	-4,	-5,	12 )	--	8.0
,(	7,	-1,	2,	-1,	-2,	-3,	-3,	11 )	--	7.0
,(	7,	-1,	3,	-1,	-2,	-4,	-5,	10 )	--	6.0
,(	7,	-1,	3,	-2,	-3,	-4,	-5,	9 )	--	5.0
,(	7,	-1,	4,	-1,	-2,	-3,	-4,	8 )	--	4.5
,(	7,	-1,	4,	-3,	-3,	-4,	-5,	7 )	--	4.0
,(	7,	-1,	5,	-1,	-2,	-3,	-3,	7 )	--	4.0
,(	7,	-1,	6,	4,	4,	3,	2,	7 )	--	4.0
,(	7,	-1,	6,	5,	5,	5,	2,	7 )	--	4.0
,(	7,	1,	0,	0,	0,	0,	0,	31 )	--	140.0
,(	7,	1,	-1,	0,	0,	0,	0,	31 )	--	140.0
,(	7,	1,	0,	0,	0,	-1,	-1,	22 )	--	40.0
,(	7,	1,	0,	1,	1,	1,	1,	20 )	--	30.0
,(	7,	1,	-1,	1,	1,	2,	2,	19 )	--	25.0
,(	7,	1,	-1,	0,	0,	1,	1,	18 )	--	20.0
,(	7,	1,	-1,	0,	0,	-1,	-1,	17 )	--	18.0
,(	7,	1,	0,	-1,	-1,	-2,	-3,	16 )	--	16.0
,(	7,	1,	-1,	1,	2,	3,	4,	15 )	--	14.0
,(	7,	1,	-2,	1,	1,	2,	2,	14 )	--	12.0
,(	7,	1,	-1,	1,	2,	4,	5,	13 )	--	10.0
,(	7,	1,	-2,	1,	2,	4,	5,	12 )	--	8.0
,(	7,	1,	-2,	1,	2,	3,	3,	11 )	--	7.0
,(	7,	1,	-3,	1,	2,	4,	5,	10 )	--	6.0
,(	7,	1,	-3,	2,	3,	4,	5,	9 )	--	5.0
,(	7,	1,	-4,	1,	2,	3,	4,	8 )	--	4.5
,(	7,	1,	-4,	3,	3,	4,	5,	7 )	--	4.0
,(	7,	1,	-5,	1,	2,	3,	3,	7 )	--	4.0
,(	7,	1,	-6,	-4,	-4,	-3,	-2,	7 )	--	4.0
,(	7,	1,	-6,	-5,	-5,	-5,	-2,	7 )	--	4.0
  );
end pac_patt6;
