//  #undef IIDEC_IDEN_VAL
#define IIDEC_IDEN_VAL(id,type,value,comment) \
    static const type id ;
//  #undef IIDEC_IDEN_ENU
#define IIDEC_IDEN_ENU(id,type,comment) \
    static const type id ;
//  #undef IIDEC_COM_LINE
#define IIDEC_COM_LINE(text)
//  #undef IIDEC_ITEM_BEG
#define IIDEC_ITEM_BEG(type,id,width,num,parent,write,read,name,function,descr)
//  #undef IIDEC_ITEM_CON
#define IIDEC_ITEM_CON(type,id,width,num,parent,write,read,name,function,descr)
//  #undef IIDEC_ITEM_END
#define IIDEC_ITEM_END(type,id,width,num,parent,write,read,name,function,descr)


#include IID_FILE

  
#undef IIDEC_IDEN_VAL 
#undef IIDEC_IDEN_ENU  
#undef IIDEC_COM_LINE 
#undef IIDEC_ITEM_BEG 
#undef IIDEC_ITEM_CON    
#undef IIDEC_ITEM_END
#undef IID_FILE

