library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;
library lpm;
use lpm.lpm_components.all;
use work.std_logic_1164_ktp.all;
use work.vcomponent.all;
use work.LPMcomponent.all;
use work.tridaq.all;
package lb_coder_iis is
--
-- internal interface
--
constant   RATE_MEM_DATA_SIZE  :TN  :=minimum(64,LDEMUX_DATA_OUT); --  
constant   HIST_TIME_SIZE  :TN  :=40; --  
constant   HIST_MEM_COUNT_SIZE  :TN  :=32; --  
constant   HIST_PART_SIZE  :TN  :=DATA_PARTITION_SIZE; --  
constant   HIST_PART_NUMBERS  :TN  :=pow2(HIST_PART_SIZE); --  
constant   HIST_PART_ADDR_SIZE  :TVL  :=HIST_PART_SIZE+SLVPartAddrExpand(HIST_MEM_COUNT_SIZE,II_DATA_SIZE); --  
constant   HIST_CODE_SIZE  :TN  :=TIME_PARTITION_CODE_SIZE+DATA_PARTITION_CODE_SIZE+1; --  
constant   HIST_CODE_NUMBERS  :TN  :=pow2(HIST_CODE_SIZE); --  
constant   HIST_CODE_ADDR_SIZE  :TVL  :=HIST_CODE_SIZE+SLVPartAddrExpand(HIST_MEM_COUNT_SIZE,II_DATA_SIZE); --  
constant   LD_IDENTIFIER  :TS  :="LD"; --  
constant   LD_VERSION  :THV  :="0200"; --  
constant   PAGE_REGISTERS  :TN  := 18; --    
constant   PAGE_MEM_HIST_PART  :TN  := 19; --    
constant   PAGE_MEM_HIST_CODE  :TN  := 20; --    
constant   WORD_IDENTIFIER  :TN  := 22; --    
constant   WORD_VERSION  :TN  := 23; --    
constant   VECT_STATUS  :TN  := 25; --    
constant   BITS_STATUS_RATE_START  :TN  := 26; --    
constant   BITS_STATUS_RATE_STOP  :TN  := 27; --    
constant   BITS_STATUS_RATE_ENA  :TN  := 28; --    
constant   BITS_STATUS_PROC_REQ  :TN  := 29; --    
constant   BITS_STATUS_PROC_ACK  :TN  := 30; --    
constant   BITS_STATUS_CHAN_SEL  :TN  := 31; --    
constant   VECT_SYN0  :TN  := 33; --    
constant   BITS_SYN0_CLK_QUALITY  :TN  := 34; --    
constant   BITS_SYN0_PHASE_QUALITY  :TN  := 35; --    
constant   BITS_SYN0_DATA_PARITY  :TN  := 36; --    
constant   BITS_SYN0_BCN0_SYNCH  :TN  := 37; --    
constant   BITS_SYN0_BCN_SYNCH  :TN  := 38; --    
constant   BITS_SYN0_LATENCY  :TN  := 39; --    
constant   VECT_SYN1  :TN  := 41; --    
constant   BITS_SYN1_CLK_QUALITY  :TN  := 42; --    
constant   BITS_SYN1_PHASE_QUALITY  :TN  := 43; --    
constant   BITS_SYN1_DATA_PARITY  :TN  := 44; --    
constant   BITS_SYN1_BCN0_SYNCH  :TN  := 45; --    
constant   BITS_SYN1_BCN_SYNCH  :TN  := 46; --    
constant   BITS_SYN1_LATENCY  :TN  := 47; --    
constant   WORD_HIST_TIME_LIMIT  :TN  := 49; --    
constant   WORD_HIST_TIME_COUNT  :TN  := 50; --    
constant   AREA_MEM_HIST_PART  :TN  := 52; --    
constant   AREA_MEM_HIST_CODE  :TN  := 53; --    
--  " item type                  item ID                width                num           parent ID     write type     read type               name                        type     description"  
--
constant VIIItemDeclList	:TVIIItemDeclList :=(  
(    VII_PAGE  ,              PAGE_REGISTERS     ,                     0  ,                   0    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv(" ")   ,                    VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,            WORD_IDENTIFIER     ,          II_DATA_SIZE  ,                   1    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("IDENTIFIER")   ,           VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,               WORD_VERSION     ,          II_DATA_SIZE  ,                   1    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("VERSION")   ,              VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_VECT  ,                VECT_STATUS     ,                     0  ,                   0    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv("STATUS")   ,               VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,    BITS_STATUS_RATE_START     ,                     1  ,                   1    ,          VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("STATUS_RATE_START")   ,    VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_STATUS_RATE_STOP     ,                     1  ,                   1    ,          VECT_STATUS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("STATUS_RATE_STOP")   ,     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,      BITS_STATUS_RATE_ENA     ,                     1  ,                   1    ,          VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("STATUS_RATE_ENA")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,      BITS_STATUS_PROC_REQ     ,                     1  ,                   1    ,          VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("STATUS_PROC_REQ")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,      BITS_STATUS_PROC_ACK     ,                     1  ,                   1    ,          VECT_STATUS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("STATUS_PROC_ACK")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,      BITS_STATUS_CHAN_SEL     ,          TVLcreate(1)  ,                   1    ,          VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("STATUS_CHAN_SEL")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_VECT  ,                  VECT_SYN0     ,                     0  ,                   0    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv("SYN0")   ,                 VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_SYN0_CLK_QUALITY     ,                     1  ,                   1    ,            VECT_SYN0         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("SYN0_CLK_QUALITY")   ,     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_SYN0_PHASE_QUALITY     ,                     1  ,                   1    ,            VECT_SYN0         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("SYN0_PHASE_QUALITY")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_SYN0_DATA_PARITY     ,                     1  ,                   1    ,            VECT_SYN0         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("SYN0_DATA_PARITY")   ,     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,      BITS_SYN0_BCN0_SYNCH     ,                     1  ,                   1    ,            VECT_SYN0         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("SYN0_BCN0_SYNCH")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,       BITS_SYN0_BCN_SYNCH     ,                     1  ,                   1    ,            VECT_SYN0         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("SYN0_BCN_SYNCH")   ,       VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,         BITS_SYN0_LATENCY     ,         SYN_PIPE_SIZE  ,                   1    ,            VECT_SYN0         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("SYN0_LATENCY")   ,         VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_VECT  ,                  VECT_SYN1     ,                     0  ,                   0    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv("SYN1")   ,                 VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_SYN1_CLK_QUALITY     ,                     1  ,                   1    ,            VECT_SYN1         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("SYN1_CLK_QUALITY")   ,     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_SYN1_PHASE_QUALITY     ,                     1  ,                   1    ,            VECT_SYN1         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("SYN1_PHASE_QUALITY")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_SYN1_DATA_PARITY     ,                     1  ,                   1    ,            VECT_SYN1         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("SYN1_DATA_PARITY")   ,     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,      BITS_SYN1_BCN0_SYNCH     ,                     1  ,                   1    ,            VECT_SYN1         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("SYN1_BCN0_SYNCH")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,       BITS_SYN1_BCN_SYNCH     ,                     1  ,                   1    ,            VECT_SYN1         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("SYN1_BCN_SYNCH ")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,         BITS_SYN1_LATENCY     ,         SYN_PIPE_SIZE  ,                   1    ,            VECT_SYN1         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("SYN1_LATENCY")   ,         VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,       WORD_HIST_TIME_LIMIT     ,        HIST_TIME_SIZE  ,                   1    ,       PAGE_REGISTERS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("HIST_TIME_LIMIT")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,       WORD_HIST_TIME_COUNT     ,        HIST_TIME_SIZE  ,                   1    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("HIST_TIME_COUNT")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(    VII_PAGE  ,          PAGE_MEM_HIST_PART     ,                     0  ,                   0    ,   PAGE_MEM_HIST_PART         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv(" ")   ,                    VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_AREA  ,         AREA_MEM_HIST_PART     ,   HIST_MEM_COUNT_SIZE  ,   HIST_PART_NUMBERS    ,   PAGE_MEM_HIST_PART         ,     VII_WACCESS  ,   VII_REXTERNAL   ,   VIINameConv("MEM_PART_HIST")   ,        VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(    VII_PAGE  ,          PAGE_MEM_HIST_CODE     ,                     0  ,                   0    ,   PAGE_MEM_HIST_CODE         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv(" ")   ,                    VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_AREA  ,         AREA_MEM_HIST_CODE     ,   HIST_MEM_COUNT_SIZE  ,   HIST_CODE_NUMBERS    ,   PAGE_MEM_HIST_CODE         ,     VII_WACCESS  ,   VII_REXTERNAL   ,   VIINameConv("MEM_CODE_HIST")   ,        VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ) 
);
end lb_coder_iis;
