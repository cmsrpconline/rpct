library ieee;
use ieee.std_logic_1164.all;

use work.std_logic_1164_ktp.all;

package KTPComponent is

  component KTP_DFF is
    port(
      resetN			:in  TSL := '1';
      setN			:in  TSL := '1';
      clk			:in  TSL;
      ena			:in  TSL := '1';
      d				:in  TSL;
      q				:out TSL
    );
  end component;

  component KTP_LPM_DFF is
    generic (
      LPM_WIDTH			:in natural := 0
    );
    port(
      resetN			:in  TSLV(LPM_WIDTH-1 downto 0) := (others => '1');
      setN			:in  TSLV(LPM_WIDTH-1 downto 0) := (others => '1');
      clk			:in  TSLV(LPM_WIDTH-1 downto 0);
      ena			:in  TSLV(LPM_WIDTH-1 downto 0) := (others => '1');
      d				:in  TSLV(LPM_WIDTH-1 downto 0);
      q				:out TSLV(LPM_WIDTH-1 downto 0)
    );
  end component;

  component KTP_LPM_REG is
    generic (
      LPM_WIDTH			:in natural := 0
    );
    port(
      resetN			:in  TSL := '1';
      setN			:in  TSL := '1';
      clk			:in  TSL;
      ena			:in  TSL;
      d				:in  TSLV(LPM_WIDTH-1 downto 0);
      q				:out TSLV(LPM_WIDTH-1 downto 0)
    );
  end component;

  component KTP_LPM_PULSE is
    generic (
      LPM_WIDTH			:in natural := 0;
      LPM_DELAY			:in natural := 0
    );
    port(
      resetN			:in  TSL := '1';
      clk			:in  TSL;
      pulse			:out TSL
    );
  end component;

end KTPComponent;

library ieee;
use ieee.std_logic_1164.all;
use work.std_logic_1164_ktp.all;

entity KTP_DFF is
  port(
    resetN			:in  TSL := '1';
    setN			:in  TSL := '1';
    clk				:in  TSL;
    ena				:in  TSL := '1';
    d				:in  TSL;
    q				:out TSL
  );
end KTP_DFF;

architecture behaviour of KTP_DFF is

  signal   reg			:TSL;

begin

  process(clk, resetN, setN, ena) begin
    if (resetN='0') then
      reg <= '0';
    elsif (setN='0') then
      reg <= '1';
    elsif (clk'event and clk='1') then
      if (ena='1') then
        reg <= d;
      end if;
    end if;
  end process;
  
  q <= reg;

end behaviour;			   

--------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.std_logic_1164_ktp.all;
use work.KTPComponent.all;

entity KTP_LPM_DFF is
  generic (
    LPM_WIDTH			:in natural := 4
  );
  port(
      resetN			:in  TSLV(LPM_WIDTH-1 downto 0) := (others => '1');
      setN			:in  TSLV(LPM_WIDTH-1 downto 0) := (others => '1');
      clk			:in  TSLV(LPM_WIDTH-1 downto 0);
      ena			:in  TSLV(LPM_WIDTH-1 downto 0) := (others => '1');
      d				:in  TSLV(LPM_WIDTH-1 downto 0);
      q				:out TSLV(LPM_WIDTH-1 downto 0)
  );
end KTP_LPM_DFF;

architecture behaviour of KTP_LPM_DFF is
begin

  l1:
  for index in 0 to LPM_WIDTH-1 generate
    creg: component  KTP_DFF
      port map(
        resetN => resetN(index),
        setN   => setN(index),
        clk    => clk(index),
        ena    => ena(index),
        d      => d(index),
        q      => q(index)
      );
  end generate;

end behaviour;			   

--------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.std_logic_1164_ktp.all;

entity KTP_LPM_REG is
  generic (
    LPM_WIDTH			:in natural := 4
  );
  port(
      resetN			:in  TSL := '1';
      setN			:in  TSL := '1';
      clk			:in  TSL;
      ena			:in  TSL;
      d				:in  TSLV(LPM_WIDTH-1 downto 0);
      q				:out TSLV(LPM_WIDTH-1 downto 0)
  );
end KTP_LPM_REG;

architecture behaviour of KTP_LPM_REG is

  signal reg			:TSLV(LPM_WIDTH-1 downto 0);

begin

  process(clk, resetN, setN, ena) begin
    if (resetN='0') then
      reg <= (others => '0');
    elsif (setN='0') then
      reg <= (others => '1');
    elsif (clk'event and clk='1') then
      if (ena='1') then
        reg <= d;
      end if;
    end if;
  end process;
  
  q <= reg;

end behaviour;			   

--------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use work.std_logic_1164_ktp.all;
use work.KTPComponent.all;

entity KTP_LPM_PULSE is
  generic (
    LPM_WIDTH			:in natural := 2;
    LPM_DELAY			:in natural := 0
  );
  port(
    resetN			:in  TSL := '1';
    clk				:in  TSL;
    pulse			:out TSL
  );
end KTP_LPM_PULSE;

architecture behaviour of KTP_LPM_PULSE is

  signal H			:TSL;
  signal ClkSig			:TSLV(LPM_WIDTH-1 downto 0);
  signal QSig			:TSLV(LPM_WIDTH-1 downto 0);
  signal ResetNSig		:TSL;

begin

  H <= '1';
  ClkSig(0) <= clk;
  l1:
  for index in 0 to LPM_WIDTH-1 generate
    creg: component  KTP_DFF
      port map(
        resetN => ResetNSig,
        setN   => H,
        clk    => ClkSig(index),
        ena    => H,
        d      => H,
        q      => QSig(index)
      );
  end generate;

  l2:
  if (LPM_WIDTH>1) generate
     ClkSig(LPM_WIDTH-1 downto 1) <= QSig(LPM_WIDTH-2 downto 0);
  end generate;

  ResetNSig <= ResetN and (not QSig(LPM_WIDTH-1));
  pulse <= OR_REDUCE( QSig(LPM_WIDTH-1 downto LPM_DELAY));

end behaviour;			   
