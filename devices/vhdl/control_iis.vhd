library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;
library lpm;
use lpm.lpm_components.all;
use work.std_logic_1164_ktp.all;
use work.vcomponent.all;
use work.LPMcomponent.all;
use work.tridaq.all;
package control_iis is
--
-- internal interface
--
constant   CONTR_IDENTIFIER  :TS  :="CR"; --  
constant   CONTR_VERSION  :THV  :="0200"; --  
--  "predefined constant for I2C interface"  
constant   I2C_DATA_SIZE  :TN  :=8; --  
--  "predefined constants of Altera boot loader"  
constant   ALT_PGM_DATA_SIZE  :TVL  :=15; --  
--  "clock source select"  
constant   CLOCK_SEL_TTC_CLK  :TN  :=VEC_INDEX_MIN; --  
constant   CLOCK_SEL_TTC_DES1  :TN  :=CLOCK_SEL_TTC_CLK+1; --  
constant   CLOCK_SEL_TTC_DES2  :TN  :=CLOCK_SEL_TTC_DES1+1; --  
constant   CLOCK_SEL_INT  :TN  :=CLOCK_SEL_TTC_DES2+1; --  
constant   CLOCK_SEL_EXT  :TN  :=CLOCK_SEL_INT+1; --  
constant   CLOCK_SEL_NUM  :TN  :=CLOCK_SEL_EXT+1; --  
constant   CLOCK_SEL_SIZE  :TN  :=TVLcreate(CLOCK_SEL_NUM-1); --  
constant   PAGE_REGISTERS  :TN  := 19; --    
constant   PAGE_BOOT  :TN  := 20; --    
constant   WORD_IDENTIFIER  :TN  := 22; --    
constant   WORD_VERSION  :TN  := 23; --    
constant   VECT_STATUS  :TN  := 25; --    
constant   BITS_STATUS_CLOCK_SEL  :TN  := 26; --    
constant   BITS_STATUS_NCONF  :TN  := 27; --    
constant   BITS_STATUS_NSTATUS  :TN  := 28; --    
constant   BITS_STATUS_CONFDONE  :TN  := 29; --    
constant   BITS_STATUS_INITDONE  :TN  := 30; --    
constant   BITS_STATUS_ISA_ENA  :TN  := 31; --    
constant   BITS_STATUS_NUMBER  :TN  := 32; --    
constant   WORD_DELAY  :TN  := 34; --    
constant   WORD_RESET  :TN  := 35; --    
constant   WORD_I2C_EXECUTE  :TN  := 37; --    
constant   WORD_I2C_DATA_OUT  :TN  := 38; --    
constant   WORD_I2C_DATA_IN  :TN  := 39; --    
constant   AREA_BOOT  :TN  := 41; --    
--  " item type                item ID              width                        num       parent ID     write type     read type               name                        type     description"  
--
constant VIIItemDeclList	:TVIIItemDeclList :=(  
(    VII_PAGE  ,            PAGE_REGISTERS     ,                   0  ,                           0    ,   PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv(" ")   ,                       VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,          WORD_IDENTIFIER     ,        II_DATA_SIZE  ,                           1    ,   PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("WORD_IDENTIFIER")   ,         VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,             WORD_VERSION     ,        II_DATA_SIZE  ,                           1    ,   PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("WORD_VERSION")   ,            VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_VECT  ,              VECT_STATUS     ,                   0  ,                           0    ,   PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv("VECT_STATUS")   ,             VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_STATUS_CLOCK_SEL     ,      CLOCK_SEL_SIZE  ,                           1    ,      VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("BITS_STATUS_CLOCK_SEL")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,       BITS_STATUS_NCONF     ,                   1  ,                           1    ,      VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("BITS_STATUS_NCONF")   ,       VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_STATUS_NSTATUS     ,                   1  ,                           1    ,      VECT_STATUS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("BITS_STATUS_NSTATUS")   ,     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,    BITS_STATUS_CONFDONE     ,                   1  ,                           1    ,      VECT_STATUS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("BITS_STATUS_CONFDONE")   ,    VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,    BITS_STATUS_INITDONE     ,                   1  ,                           1    ,      VECT_STATUS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("BITS_STATUS_INITDONE")   ,    VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_STATUS_ISA_ENA     ,                   1  ,                           1    ,      VECT_STATUS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("BITS_STATUS_ISA_ENA")   ,     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,               WORD_DELAY     ,     DELAY_DATA_SIZE  ,                           1    ,   PAGE_REGISTERS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("WORD_DELAY")   ,              VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,               WORD_RESET     ,        II_DATA_SIZE  ,                           1    ,   PAGE_REGISTERS         ,     VII_WACCESS  ,   VII_REXTERNAL   ,   VIINameConv("WORD_RESET")   ,              VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,         WORD_I2C_EXECUTE     ,                   1  ,                           1    ,   PAGE_REGISTERS         ,     VII_WACCESS  ,   VII_REXTERNAL   ,   VIINameConv("WORD_I2C_EXECUTE")   ,        VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,        WORD_I2C_DATA_OUT     ,       I2C_DATA_SIZE  ,                           1    ,   PAGE_REGISTERS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("WORD_I2C_DATA_OUT")   ,       VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,         WORD_I2C_DATA_IN     ,       I2C_DATA_SIZE  ,                           1    ,   PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("WORD_I2C_DATA_IN")   ,        VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(    VII_PAGE  ,                 PAGE_BOOT     ,                   0  ,                           0    ,        PAGE_BOOT         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv(" ")   ,                       VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_AREA  ,                AREA_BOOT     ,   ALT_PGM_DATA_SIZE  ,   pow2(II_ADDR_USER_SIZE-1)    ,        PAGE_BOOT         ,     VII_WACCESS  ,   VII_REXTERNAL   ,   VIINameConv("AREA_BOOT")   ,               VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ) 
);
end control_iis;
