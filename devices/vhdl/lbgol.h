//---------------------------------------------------------------------------
               
//
//  Michal Pietrusinski 2001
//  Warsaw University
//

#ifndef lbgolH
#define lbgolH
//---------------------------------------------------------------------------

#include "IIHisto.h"
#include "THistoMgr.h"
#include "TIII2CMasterCore.h"
#include "TDiagCtrl.h" 
#include "TCrate.h"
#include "i2c.h"
#include "gol.h"


               

class TLBGolDevice: public TIIDevice, public IIVMEDevice {
protected:
  int VMEBoardBaseAddr;
public:
  TLBGolDevice(std::string name,
            TID name_id,
            std::string desc,
            std::string ver,
            TID ver_id,
            uint32_t base_address,
            IBoard& board,
            TMemoryMap* memmap,
            int vme_board_address)
    : TIIDevice(0 ,name, name_id, desc, ver, ver_id, 0, base_address, board, memmap),
      VMEBoardBaseAddr(vme_board_address) {}

  virtual ~TLBGolDevice() {}

  virtual int GetVMEBaseAddrShift()
    { return 16; }

  virtual int GetVMEBoardBaseAddr()
    { return VMEBoardBaseAddr; }
};

   

class TLBGolControl: public TLBGolDevice {
private:
  TIII2CMasterCore* I2C;    
  TDiagCtrl DiagCtrl;
public:
  #define IID_FILE "vhdl/lb_control.iid"
  #include "iid2c1.h"

  TLBGolControl(int vme_board_address, IBoard& board);
  virtual ~TLBGolControl() {}


  TIII2CMasterCore& GetI2C()
    {
      if (I2C == NULL) {
        I2C = new TIII2CMasterCore(*this, AREA_I2C);
        I2C->SetPrescaleValue(100);
        I2C->WriteControlReg(TI2CMasterCore::BIT_EN);
      };

      return *I2C;
    }
    
  TDiagCtrl& GetDiagCtrl()
    {
      return DiagCtrl;
    }
};


class TLBGolSrc: public TLBGolDevice {
private:

  //TIIHistoRunCtrl* RunCtrl;
  TDiagCtrl* DiagCtrl;

public:

  #define IID_FILE "vhdl/lb_golsrc.iid"
  #include "iid2c1.h"

  TDiagCtrl& GetDiagCtrl()
    {
      if (DiagCtrl == NULL)
        DiagCtrl = new TDiagCtrl(*this,
                                  VECT_STATUS,
                                  BITS_STATUS_PROC_REQ,
                                  BITS_STATUS_PROC_ACK,
                                  BITS_STATUS_TIMER_LOC_ENA,
                                  BITS_STATUS_TIMER_GLO_ENA,
                                  BITS_STATUS_TIMER_START,
                                  BITS_STATUS_TIMER_STOP,
                                  WORD_TIMER_LIMIT,
                                  WORD_TIMER_COUNT,
                                  false);

      return *DiagCtrl;
    }

  /*TIIHistoRunCtrl& GetRunCtrl()
    {
      if (RunCtrl == NULL)
        RunCtrl = new TIIHistoRunCtrl(*this,
                              VECT_STATUS,
                              BITS_STATUS_PROC_REQ,
                              BITS_STATUS_PROC_ACK,
                              BITS_STATUS_RATE_ENA,
                              BITS_STATUS_RATE_START,
                              BITS_STATUS_RATE_STOP,
                              WORD_TEST_TIME_LIMIT,
                              WORD_TEST_TIME_COUNT);

      return *RunCtrl;
    }*/


  TLBGolSrc(int vme_board_address, IBoard& board);
  virtual ~TLBGolSrc();
};


class TLBGolDest: public TLBGolDevice {
private:
  TIIHistoCtrl* ErrHisto;
  THistoMgr* ErrHistoMgr;

  //TIIHistoRunCtrl* RunCtrl;
  TDiagCtrl* DiagCtrl;

  IHistoCtrl& GetErrHistoCtrl()
    {
      if (ErrHisto == NULL) {
        /*(TIIHistoCtrlMask::TMaskedBins maskedBins;
        mskedBins.push_back(0);
        ErrHisto = new TIIHistoCtrlMask(*this, AREA_MEM_RATE_ERR, maskedBins);*/
        ErrHisto = new TIIHistoCtrl(*this, AREA_MEM_RATE_ERR);
      }

      return *ErrHisto;
    }

public:

  #define IID_FILE "vhdl/lb_goldst.iid"
  #include "iid2c1.h"

  TDiagCtrl& GetDiagCtrl()
    {
      if (DiagCtrl == NULL)
        DiagCtrl = new TDiagCtrl(*this,
                                  VECT_STATUS,
                                  BITS_STATUS_PROC_REQ,
                                  BITS_STATUS_PROC_ACK,
                                  BITS_STATUS_TIMER_LOC_ENA,
                                  BITS_STATUS_TIMER_GLO_ENA,
                                  BITS_STATUS_TIMER_START,
                                  BITS_STATUS_TIMER_STOP,
                                  WORD_TIMER_LIMIT,
                                  WORD_TIMER_COUNT,
                                  false);

      return *DiagCtrl;
    }

  THistoMgr& GetErrHistoMgr()
    {
      if (ErrHistoMgr == NULL)
        ErrHistoMgr = new THistoMgr(&GetErrHistoCtrl());

      return *ErrHistoMgr;
    }


  TLBGolDest(int vme_board_address, IBoard& board);
  virtual ~TLBGolDest();
};




class TLinkBoardGol: public IBoard {
private:
  TLBGolControl LBControl;
  TLBGolSrc  LBGolSrc;   
  TGolI2C    GolI2C;
  TLBGolDest LBGolDest;
  int VMEAddress;
  TDevices Devices;
  int ID;
  std::string Name;
public:
  TLinkBoardGol(TVMEInterface* vme, int vme_board_address);

  TLBGolControl& GetLBControl()
    { return LBControl; }

  TLBGolSrc& GetLBGolSrc()
    { return LBGolSrc; }

  TLBGolDest& GetLBGolDest()
    { return LBGolDest; }

  TGolI2C& GetGolI2C()
    { return GolI2C; }

  int GetVMEAddress()
    { return VMEAddress; } 
    
  virtual const TDevices& GetDevices()
    {
      return Devices;
    }

  virtual TCrate* GetCrate()
    { return NULL; }  
  virtual int GetID()
    { return ID; }       
  virtual const std::string& GetName()
    { return Name; }
};



#endif
