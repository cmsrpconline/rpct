library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;

library lpm;
use lpm.lpm_components.all;

use work.std_logic_1164_ktp.all;
use work.vcomponent.all;
use work.LPMcomponent.all;
use work.tridaq.all;
use work.link.all;
use work.lsynchro_iis.all;

entity lsynchro is
  port(
    --link channel 0
    LX0_data					:in    TSLV(LB_SYNCHRO_DATA-1 downto 0);
    LX0_phase					:in    TSL;
    LX0_clk					:in    TSL;
    LDMUX0					:out   TSLV(LINK_DATA_SIZE-1 downto 0);
    --link channel 1
    LX1_data					:in    TSLV(LB_SYNCHRO_DATA-1 downto 0);
    LX1_phase					:in    TSL;
    LX1_clk					:in    TSL;
    LDMUX1					:out   TSLV(LINK_DATA_SIZE-1 downto 0);
    -- Master DAQ interface
    MDAQ_clock					:in    TSL;
    MDAQ_trans_clock				:in    TSL;
    MDAQ_triggerN				:in    TSL;
    MDAQ_trg_page				:in    TSLV(TRIGGER_PAGE_CODE_SIZE-1 downto 0);
    MDAQ_cnt_resetN				:in    TSL;
    MDAQ_addr_slave				:in    TSLV(MDAQ_ADDR_SLAVE_SIZE-1 downto 0);
    MDAQ_addr_page				:in    TSLV(TRIGGER_PAGE_CODE_SIZE-1 downto 0);
    MDAQ_addr_pos				:in    TSLV(DATA_PARTITION_CODE_SIZE-1 downto 0);
    MDAQ_data					:out   TSLV(DPM_DATA_SIZE-1 downto 0);
    MDAQ_cnt					:out   TSLV(DATA_COUNTER_CODE_SIZE-1 downto 0);
    MDAQ_status					:out   TSLV(SDAQ_STATUS_SIZE-1 downto 0);
    MDAQ_ena					:out   TSL;
    -- fast signal interface
    clock					:in    TSL;
    l1a						:in    TSL;
    pretrg0					:in    TSL;
    pretrg1					:in    TSL;
    bcn0					:in    TSL;
    -- internal bus interface
    II_base					:in    TSLV(2 downto 1);
    II_addr					:in    TSLV(II_ADDR_SIZE-1 downto 0);
    II_data					:inout TSLV(II_DATA_SIZE-1 downto 0);
    II_oper					:in    TSL;
    II_write					:in    TSL;
    II_save					:in    TSL;
    ii_int					:inout TSL;
    II_reset					:in    TSL;
    -- diagnostics signal
    led						:out   TSLV(3 downto 0);
    conf_led					:out   TSL
  );
end lsynchro;

architecture behaviour of lsynchro is

  component synchro
    port(
      resetN			:in  TSL;
      clock			:in  TSL;
      bcn0			:in  TSL;
      data_in			:in  TSLV(LB_SYNCHRO_DATA-1 downto 0);
      tmark			:in  TSL;
      dphase			:in  TSL;
      data_out			:out TSLV(LINK_DATA_SIZE-1 downto 0);
      clock_quality		:out TSL;
      phase_quality		:out TSL;
      data_parity		:out TSL;
      BCN0_synch		:out TSL;
      BCN_synch			:out TSL;
      latency			:out TSLV(SYN_PIPE_SIZE-1 downto 0)
    );
  end component;

  component daq_slave
    port(
      resetN			:in  TSL;
      clk			:in  TSL;
      tclk			:in  TSL;
      chamber			:in  TSLV(CHAMBER_ADDRESS_SIZE-1 downto 0);
      time			:in  TSLV(TIME_PARTITION_CODE_SIZE-1 downto 0);
      partition			:in  TSLV(DATA_PARTITION_CODE_SIZE-1 downto 0);
      data			:in  TSLV(DATA_PARTITION_SIZE-1 downto 0);
      end_data			:in  TSL;
      half_part			:in  TSL;
      triggerN			:in  TSL;
      trg_page			:in  TSLV(TRIGGER_PAGE_CODE_SIZE-1 downto 0);
      cnt_resetN		:in  TSL;
      addr_position		:in  TSLV(DATA_PARTITION_CODE_SIZE-1 downto 0);
      addr_page			:in  TSLV(TRIGGER_PAGE_CODE_SIZE-1 downto 0);
      slave_data		:out TSLV(DPM_DATA_SIZE-1 downto 0);
      cnt_data			:out TSLV(DATA_COUNTER_CODE_SIZE-1 downto 0);
      slave_status		:out TSLV(SDAQ_STATUS_SIZE-1 downto 0);
      setup_reqN		:in  TSL;
      setup_ackN		:out TSL;
      delay			:in  TSLV(DELAY_PIPELINE_SIZE-1 downto 0);
      delay_mem_addr		:in  TSLV(DPM_DELAY_ADDR_SIZE-1 downto 0);
      delay_mem_data_in		:in  TSLV(DPM_DELAY_DATA_SIZE-1 downto 0);
      delay_mem_data_out	:out TSLV(DPM_DELAY_DATA_SIZE-1 downto 0);
      delay_mem_wr		:in  TSL;
      data_mem_addr		:in  TSLV(DPM_DATA_ADDR_SIZE-1 downto 0);
      data_mem_data_in		:in  TSLV(DPM_DATA_DATA_SIZE-1 downto 0);
      data_mem_data_out		:out TSLV(DPM_DATA_DATA_SIZE-1 downto 0);
      data_mem_wr		:in  TSL;
      cnt_mem_addr		:in  TSLV(DPM_CNT_ADDR_SIZE-1 downto 0);
      cnt_mem_data_in		:in  TSLV(DPM_CNT_DATA_SIZE-1 downto 0);
      cnt_mem_data_out		:out TSLV(DPM_CNT_DATA_SIZE-1 downto 0);
      cnt_mem_wr		:in  TSL
    );
  end component;

  constant IIPar				:TVII := TVIICreate(VIIItemDeclList,II_ADDR_SIZE,II_DATA_SIZE);
  signal   IIVecInt,IIVecExt,IIVecEna,IIVecSet	:TSLV(VII(IIPar)'high downto VEC_INDEX_MIN);
  signal   LIIEnableSig				:TSL;
  signal   LIIAddrLocSig			:TSLV(II_ADDR_USER_SIZE-1 downto 0);
  signal   IIDataSig				:TSLV(II_DATA_SIZE-1 downto 0);
  signal   LIIDataExport			:TSLV(II_DATA_SIZE-1 downto 0);
  signal   LIIDataExportEnable			:TSL;
  --
  signal   L, H					:TSL;
  signal   SelLinkChan				:TVI;
  --
  signal   L1AReg				:TSL;
  signal   BCN0Reg				:TSL;
  signal   Pretrg0Reg				:TSL;
  signal   Pretrg1Reg				:TSL;

  signal   Syn0_DataOutSig			:TSLV(LINK_DATA_SIZE-1 downto 0);
  signal   Syn0_ClockQualitySig			:TSL;
  signal   Syn0_PhaseQualitySig			:TSL;
  signal   Syn0_DataParitySig			:TSL;
  signal   Syn0_BCN0SynchSig			:TSL;
  signal   Syn0_BCNSynchSig			:TSL;
  signal   Syn0_LatencySig			:TSLV(SYN_PIPE_SIZE-1 downto 0);
  signal   Syn1_DataOutSig			:TSLV(LINK_DATA_SIZE-1 downto 0);
  signal   Syn1_ClockQualitySig			:TSL;
  signal   Syn1_PhaseQualitySig			:TSL;
  signal   Syn1_DataParitySig			:TSL;
  signal   Syn1_BCN0SynchSig			:TSL;
  signal   Syn1_BCNSynchSig			:TSL;
  signal   Syn1_LatencySig			:TSLV(SYN_PIPE_SIZE-1 downto 0);
  --
  signal   DAQ_ChamberSig			:TSLV(CHAMBER_ADDRESS_SIZE-1 downto 0);
  signal   DAQ_TimeSig				:TSLV(TIME_PARTITION_CODE_SIZE-1 downto 0);
  signal   DAQ_PartSig				:TSLV(DATA_PARTITION_CODE_SIZE-1 downto 0);
  signal   DAQ_DataSig				:TSLV(DATA_PARTITION_SIZE-1 downto 0);
  signal   DAQ_EndDataSig			:TSL;
  signal   DAQ_HalfPartSig			:TSL;
  signal   DAQ_SetupReqNSig			:TSL;
  signal   DAQ_SetupAckNSig			:TSL;
  signal   DAQ_DelaySig				:TSLV(DELAY_PIPELINE_SIZE-1 downto 0);
  signal   DAQ_DelayMemWrSig			:TSL;
  signal   DAQ_DelayMemDataOutSig		:TSLV(DPM_DELAY_DATA_SIZE-1 downto 0);
  signal   DAQ_DataMemWrSig			:TSL;
  signal   DAQ_DataMemDataOutSig		:TSLV(DPM_DATA_DATA_SIZE-1 downto 0);
  signal   DAQ_CntMemWrSig			:TSL;
  signal   DAQ_CntMemDataOutSig			:TSLV(DPM_CNT_DATA_SIZE-1 downto 0);
  signal   DAQ_EnaReg1, DAQ_EnaReg2		:TSL;

begin

  syn0: component synchro
    port map(
      resetN			=> II_reset,
      clock			=> clock,
      bcn0			=> BCN0Reg,
      data_in			=> LX0_data,
      tmark			=> LX0_clk,
      dphase			=> LX0_phase,
      data_out			=> Syn0_DataOutSig,
      clock_quality		=> Syn0_ClockQualitySig,
      phase_quality		=> Syn0_PhaseQualitySig,
      data_parity		=> Syn0_DataParitySig,
      BCN0_synch		=> Syn0_BCN0SynchSig,
      BCN_synch			=> Syn0_BCNSynchSig,
      latency			=> Syn0_LatencySig
    );

  LDMUX0 <= Syn0_DataOutSig;

  syn1: component synchro
    port map(
      resetN			=> II_reset,
      clock			=> clock,
      bcn0			=> BCN0Reg,
      data_in			=> LX1_data,
      tmark			=> LX1_clk,
      dphase			=> LX1_phase,
      data_out			=> Syn1_DataOutSig,
      clock_quality		=> Syn1_ClockQualitySig,
      phase_quality		=> Syn1_PhaseQualitySig,
      data_parity		=> Syn1_DataParitySig,
      BCN0_synch		=> Syn1_BCN0SynchSig,
      BCN_synch			=> Syn1_BCNSynchSig,
      latency			=> Syn1_LatencySig
    );

  LDMUX1 <= Syn1_DataOutSig;

  DAQ_ChamberSig  <= TSLVResize(Syn0_DataOutSig, GL(GL_ITEM_CHAMBER))   when SelLinkChan=0 else
                     TSLVResize(Syn1_DataOutSig, GL(GL_ITEM_CHAMBER));
  DAQ_TimeSig     <= TSLVResize(Syn0_DataOutSig, GL(GL_ITEM_TIME))      when SelLinkChan=0 else
                     TSLVResize(Syn1_DataOutSig, GL(GL_ITEM_TIME));
  DAQ_PartSig     <= TSLVResize(Syn0_DataOutSig, GL(GL_ITEM_PARTITION)) when SelLinkChan=0 else
                     TSLVResize(Syn1_DataOutSig, GL(GL_ITEM_PARTITION));
  DAQ_DataSig     <= TSLVResize(Syn0_DataOutSig, GL(GL_ITEM_DATA))      when SelLinkChan=0 else
                     TSLVResize(Syn1_DataOutSig, GL(GL_ITEM_DATA));
  DAQ_EndDataSig  <=  TSLconv(Syn0_DataOutSig, GL(GL_ITEM_END_DATA))  when SelLinkChan=0 else
                      TSLconv(Syn1_DataOutSig, GL(GL_ITEM_END_DATA));
  DAQ_HalfPartSig <=  TSLconv(Syn0_DataOutSig, GL(GL_ITEM_HALF_PART)) when SelLinkChan=0 else
                      TSLconv(Syn1_DataOutSig, GL(GL_ITEM_HALF_PART));

  DAQ_DelaySig      <= IIConnGetWordData(IIVecInt,IIPar,WORD_DAQ_DELAY,0);
  DAQ_SetupReqNSig  <= TSLconv(IIConnGetBitsData(IIVecInt,IIPar,BITS_STATUS_PROC_REQ,0));
  DAQ_DelayMemWrSig <= IIConnGetAreaWrite(IIVecEna,IIPar,AREA_DAQ_MEM_DELAY,ii_save);
  DAQ_DataMemWrSig  <= IIConnGetAreaWrite(IIVecEna,IIPar,AREA_DAQ_MEM_DATA,ii_save);
  DAQ_CntMemWrSig   <= IIConnGetAreaWrite(IIVecEna,IIPar,AREA_DAQ_MEM_CNT,ii_save);
  daq: component daq_slave
    port map(
      resetN			=> II_reset,
      clk			=> MDAQ_clock,
      tclk			=> MDAQ_trans_clock,
      chamber			=> DAQ_ChamberSig,
      time			=> DAQ_TimeSig,
      partition			=> DAQ_PartSig,
      data			=> DAQ_DataSig,
      end_data			=> DAQ_EndDataSig,
      half_part			=> DAQ_HalfPartSig,
      triggerN			=> MDAQ_triggerN,
      trg_page			=> MDAQ_trg_page,
      cnt_resetN		=> MDAQ_cnt_resetN,
      addr_position		=> MDAQ_addr_pos,
      addr_page			=> MDAQ_addr_page,
      slave_data		=> MDAQ_data,
      cnt_data			=> MDAQ_cnt,
      slave_status		=> MDAQ_status,
      setup_reqN		=> DAQ_SetupReqNSig,
      setup_ackN		=> DAQ_SetupAckNSig,
      delay			=> DAQ_DelaySig,
      delay_mem_addr		=> ii_addr(DPM_DELAY_ADDR_SIZE-1 downto 0),
      delay_mem_data_in		=> ii_data(DPM_DELAY_DATA_SIZE-1 downto 0),
      delay_mem_data_out	=> DAQ_DelayMemDataOutSig,
      delay_mem_wr		=> DAQ_DelayMemWrSig,
      data_mem_addr		=> ii_addr(DPM_DATA_ADDR_SIZE-1 downto 0),
      data_mem_data_in		=> ii_data(DPM_DATA_DATA_SIZE-1 downto 0),
      data_mem_data_out		=> DAQ_DataMemDataOutSig,
      data_mem_wr		=> DAQ_DataMemWrSig,
      cnt_mem_addr		=> ii_addr(DPM_CNT_ADDR_SIZE-1 downto 0),
      cnt_mem_data_in		=> ii_data(DPM_CNT_DATA_SIZE-1 downto 0),
      cnt_mem_data_out		=> DAQ_CntMemDataOutSig,
      cnt_mem_wr		=> DAQ_CntMemWrSig
    );

  --
  -- fast interface
  --
  process(ii_reset, clock)
  begin
    if(ii_reset='0') then
      L1AReg <= '0';
      BCN0Reg <= '0';
      Pretrg0Reg <= '0';
      Pretrg1Reg <= '0';
    elsif(clock'event and clock='1') then
      L1AReg <= l1a;
      BCN0Reg <= bcn0;
      Pretrg0Reg <= pretrg0;
      Pretrg1Reg <= pretrg1;
    end if;
  end process;
  --
  -- MDAQ interface
  --
  process (ii_reset, MDAQ_trans_clock) is
    variable PacketEmptyVar :TSL;
  begin
    if (ii_reset='0') then
      DAQ_EnaReg1 <= '0';
      DAQ_EnaReg2 <= '0';
    elsif (MDAQ_trans_clock'event and MDAQ_trans_clock='1') then
      DAQ_EnaReg1 <= TSLconv(MDAQ_addr_slave=TSLVResize(II_base,MDAQ_ADDR_SLAVE_SIZE));
      DAQ_EnaReg2 <= DAQ_EnaReg1;
    end if;
  end process;
  MDAQ_Ena <= DAQ_EnaReg2;
  --
  -- internal interface
  --
  LIIEnableSig <= not(TSLconv(  ii_addr(II_ADDR_SIZE-1 downto II_ADDR_USER_SIZE)
                          =
			  (II_LDEMUX0_BASE_ADDR+(2*TNconv(II_base)))
			and
			  ii_oper='0'
		        ));
  LIIAddrLocSig <= ii_addr(II_ADDR_USER_SIZE-1 downto 0);
  --
  process(ii_reset, ii_save)
  begin
    if(ii_reset='0') then
      IIVecInt <= IIReset(IIVecInt,IIPar);
    elsif(ii_save'event and ii_save='1') then
      if(LIIEnableSig='0') then
        IIVecInt <= IISaveAccess(IIVecInt,IIPar,LIIAddrLocSig,IIDataSig);
      end if;
    end if;
  end process;
  --
  IIVecEna <= IIWriteEnable(IIVecInt,IIPar,LIIEnableSig,ii_write,LIIAddrLocSig,IIDataSig);

  IIVecExt <= (IIWriteAccess(IIVecInt,IIPar,LIIEnableSig,ii_write,LIIAddrLocSig,IIDataSig)
            or IIConnPutWordData(IIVecInt,IIPar,WORD_IDENTIFIER,        0,TSLVconv(SYN_IDENTIFIER,II_DATA_SIZE))
            or IIConnPutWordData(IIVecInt,IIPar,WORD_VERSION,           0,TSLVconv(SYN_VERSION,II_DATA_SIZE))
            or IIConnPutBitsData(IIVecInt,IIPar,BITS_STATUS_PROC_ACK,   0,TSLVconv(DAQ_SetupAckNSig))
            or IIConnPutBitsData(IIVecInt,IIPar,BITS_SYN0_LATENCY,      0,Syn0_LatencySig)
            or IIConnPutBitsData(IIVecInt,IIPar,BITS_SYN1_LATENCY,      0,Syn1_LatencySig)
            or IIConnPutBitsData(IIVecInt,IIPar,BITS_SYN0_CLK_QUALITY,  0,TSLVconv(Syn0_ClockQualitySig))
            or IIConnPutBitsData(IIVecInt,IIPar,BITS_SYN0_PHASE_QUALITY,0,TSLVconv(Syn0_PhaseQualitySig))
            or IIConnPutBitsData(IIVecInt,IIPar,BITS_SYN0_DATA_PARITY,  0,TSLVconv(Syn0_DataParitySig))
            or IIConnPutBitsData(IIVecInt,IIPar,BITS_SYN0_BCN0_SYNCH,   0,TSLVconv(Syn0_BCN0SynchSig))
            or IIConnPutBitsData(IIVecInt,IIPar,BITS_SYN0_BCN_SYNCH,    0,TSLVconv(Syn0_BCNSynchSig))
            or IIConnPutBitsData(IIVecInt,IIPar,BITS_SYN1_CLK_QUALITY,  0,TSLVconv(Syn1_ClockQualitySig))
            or IIConnPutBitsData(IIVecInt,IIPar,BITS_SYN1_PHASE_QUALITY,0,TSLVconv(Syn1_PhaseQualitySig))
            or IIConnPutBitsData(IIVecInt,IIPar,BITS_SYN1_DATA_PARITY,  0,TSLVconv(Syn1_DataParitySig))
            or IIConnPutBitsData(IIVecInt,IIPar,BITS_SYN1_BCN0_SYNCH,   0,TSLVconv(Syn1_BCN0SynchSig))
            or IIConnPutBitsData(IIVecInt,IIPar,BITS_SYN1_BCN_SYNCH,    0,TSLVconv(Syn1_BCNSynchSig))

            or IIConnPutAreaData(IIVecInt,IIPar,AREA_DAQ_MEM_DELAY, DAQ_DelayMemDataOutSig)
            or IIConnPutAreaData(IIVecInt,IIPar,AREA_DAQ_MEM_DATA,  DAQ_DataMemDataOutSig)
            or IIConnPutAreaData(IIVecInt,IIPar,AREA_DAQ_MEM_CNT,   DAQ_CntMemDataOutSig)
	    );
  LIIDataExport <= IIReadAccess(IIVecExt,IIPar,LIIEnableSig,ii_write,LIIAddrLocSig);
  --
  LIIDataExportEnable <= not(LIIEnableSig) and ii_write;
  CIIData: ALTERA_BUSTRI
    generic map (LPM_WIDTH => II_DATA_SIZE)
    port map    (data_in   => LIIDataExport,
                 data_out  => IIDataSig,
                 data_tri  => ii_data,
                 ena       => LIIDataExportEnable);
  CIIIrq: ALTERA_TRI
    port map    (data_in   => L,
                 data_tri  => ii_int,
		 ena       => Pretrg0Reg);
  --
  -- diagnostic interface
  --
  led(0) <= II_oper and II_reset;
  led(1) <= LIIEnableSig and II_reset;
  led(2) <= II_write and II_reset;
  led(3) <= II_save and II_reset;

  conf_led <= '0';

end behaviour;

