library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;

package std_logic_1164_ktp is
-- this package was created by Krzysztof Pozniak(pozniak@ise.pw.edu.pl)

  -------------------------------------------------------------------
  -- standard package extentions
  -------------------------------------------------------------------
  subtype   TI is integer;
  subtype   TN is natural;
  subtype   TP is positive;
  subtype   TL is boolean;
  subtype   TC is character;
  subtype   TS is string;

  type      TIV is array(TN range<>) of integer;
  type      TNV is array(TN range<>) of natural;
  type      TPV is array(TN range<>) of positive;
  type      TLV is array(TN range<>) of boolean;


  function  minimum(a, b :TS) return TS;						-- returns less value from 'a' or 'b'
  function  maximum(a, b :TS) return TS;						-- returns biger value from 'a' or 'b'
  function  sel(t, f :TS; c :TL) return TS;				-- returns 't' for 'c'=TRUE or 'f' for 'c'=FALSE

  -------------------------------------------------------------------
  -- std_logic_arith package extentions
  -------------------------------------------------------------------
  function  minimum(a, b :TI) return TI;						-- returns less value from 'a' or 'b'
  function  maximum(a, b :TI) return TI;						-- returns biger value from 'a' or 'b'
  function  sel(t, f :TI; c :TL) return TI;				-- returns 't' for 'c'=TRUE or 'f' for 'c'=FALSE
  
  function  minimum(v :TIV) return TI;						-- returns smallest value from vector 'v' of integers
  function  maximum(v :TIV) return TI;						-- returns bigest value from vector 'v' of integers
  function  minimum(v :TNV) return TN;						-- returns smallest value from vector 'v' of naturals
  function  maximum(v :TNV) return TN;						-- returns bigest value from vector 'v' of naturals
  function  minimum(v :TPV) return TP;						-- returns smallest value from vector 'v' of positives
  function  maximum(v :TPV) return TP;						-- returns bigest value from vector 'v' of positives
  function  pow2(v :TN) return TN;						-- returns 2^'v'

  -------------------------------------------------------------------
  -- vectors range family
  -------------------------------------------------------------------
  subtype   TVL is TN;							-- 'TVL' defines vector length range
  constant  NO_VEC_LEN :TVL := 0;						-- 'NO_VEC_LEN' occurs not defined vector length

  subtype   TVI is TI range -1 to TVL'high;				-- 'TVI' defines vector index range
  constant  NO_VEC_INDEX :TVI := -1;					-- 'NO_VEC_INDEX' occurs not defined vector index
  constant  VEC_INDEX_MIN :TVI := 0;					-- 'VEC_INDEX_MIN' occurs minimal vector index

  type      TVR								-- 'TVR' type covers vector range parameters(l,r)
	    is record
	      l :TVI;
	      r :TVI;
	    end record;
  
  type      TVLV is array(TN range<>) of TVL;
  type      TVIV is array(TN range<>) of TVI;
  type      TVRV is array(TN range<>) of TVR;


  function  TVRcreate(l, r :TVI) return TVR;			-- generates 'TVR' for 'l' and 'r' input variables
  function  TVRcreate(len :TVL) return TVR;				-- generates 'TVR': 'l'=len-1 and 'r'=VEC_INDEX_MIN
  function  TVLcreate(l, r :TVI) return TVL;				-- computes length:abs('l'-'r')+1
  function  TVLcreate(par :TVR) return TVL;				-- computes length:abs('par.l'-'par.r')+1
  function  minimum(v :TVLV) return TVL;					-- returns smallest value from vector 'v' of TVL
  function  maximum(v :TVLV) return TVL;					-- returns bigest value from vector 'v' of TVL
  function  minimum(v :TVIV) return TVI;					-- returns smallest value from vector 'v' of TVI
  function  maximum(v :TVIV) return TVI;					-- returns bigest value from vector 'v' of TVI
  function  "**" (l, r :TVR) return TVR;				-- returns maximal common 'TVR' size for 'l' and 'r'

  function  TVRVconv(arg :TVR) return TVRV;			-- generates 'TVIV' vector(0 downto 0) = (arg)
  function  TVLcreate(arg :TVRV) return TVL;				-- returns length of vector from 'arg';
  function  "+" (l, r :TVRV) return TVRV;				-- adds 'l' to 'r' and generates common 'TVIV' vec.
  function  "+" (l, r :TVR) return TVRV;				-- adds 'l' to 'r' and generates common 'TVIV' vec.
  function  "+" (l :TVRV; r :TVR) return TVRV;			-- adds 'l' to 'r' and generates common 'TVIV' vec.
  function  "+" (l :TVR; r :TVRV) return TVRV;			-- adds 'l' to 'r' and generates common 'TVIV' vec.

  -------------------------------------------------------------------
  -- TSL and TSLV types families
  -------------------------------------------------------------------
  subtype   TSL is std_logic;
  subtype   TSLV is std_logic_vector;

  function  TSLconv(arg :TN) return TSL;						-- 'TN' type converts to 'TSL' type
  function  TSLconv(arg :TL) return TSL;						-- 'TL' type converts to 'TSL' type
  function  TSLconv(arg:TSLV) return TSL;						-- returns arg(arg'low) as 'TSL' type
  function  TNconv(arg :TSL) return TN;					-- 'TSL' type converts to 'TN' type
  function  TLconv(arg :TSL) return TL;					-- returns result of logical condition :(arg='1')

  function  TSLVconv(arg :TN; len :TVL) return TSLV;				-- converts 'TN' to TSLV(len-1 downto VEC_INDEX_MIN)
  function  TSLVconv(arg :TN) return TSLV;					-- converts 'TN' to TSLV with minimal size
  function  TSLVconv(arg :TL; len :TVL) return TSLV;				-- converts 'TL' to TSLV(len-1 downto VEC_INDEX_MIN)
  function  TSLVconv(arg :TL) return TSLV;					-- converts 'TL' to TSLV with minimal size
  function  TSLVconv(arg:TSL; len :TVL) return TSLV;				-- converts 'TSL' to TSLV(len-1 downto VEC_INDEX_MIN)
  function  TSLVconv(arg:TSL) return TSLV;						-- converts 'TSL' to TSLV with minimal size
  function  TNconv(arg :TSLV) return TN;					-- 'TSLV' type converts to 'TN' type
  function  TLconv(arg :TSLV) return TL;					-- returns FALSE if 'arg' contains only '0' bits

  procedure TSLVputS(signal vec :inout TSLV; dst, src :TSLV);			-- performs:vec(dst'range)=src; returns 'vec'
  procedure TSLVput(vec :inout TSLV; dst, src :TSLV);				-- performs:vec(dst'range)=src; returns 'vec'
  procedure TSLVputS(signal vec :inout TSLV; dst :TSLV; src :TSL);		-- performs:vec(dst'right)=src; returns 'vec'
  procedure TSLVput(vec :inout TSLV; dst :TSLV; src :TSL);			-- performs:vec(dst'right)=src; returns 'vec'
  procedure TSLVputS(signal dst :inout TSLV; src :TSLV);			-- performs:dst(src'range)=src; returns 'dst'
  procedure TSLVput(dst :inout TSLV; src :TSLV);				-- performs:dst(src'range)=src; returns 'dst'
  procedure TSLVputS(signal dst :inout TSLV; l,r :TVI; src :TSLV);		-- performs:vec(l to/downto r)=src; returns 'vec'
  procedure TSLVput(dst :inout TSLV; l,r :TVI; src :TSLV);			-- performs:vec(l to/downto r)=src; returns 'vec'
  procedure TSLVfillS(signal dst :inout TSLV; f :TSL);				-- fills dst of 'f'; returns 'dst'
  procedure TSLVfill(dst :inout TSLV; f :TSL);					-- fills dst of 'f'; returns 'dst'

  function  TSLVnew(l,r :TVI; f:TSL) return TSLV;			-- creates TSLV(l to/downto r) and fills 'f'
  function  TSLVnew(l,r :TVI) return TSLV;				-- creates TSLV(l to/downto r) and fills '0'
  function  TSLVnew(length :TVL; f:TSL) return TSLV;			-- creates TSLV(length-1 downto VEC_INDEX_MIN) + fills 'f'
  function  TSLVnew(length :TVL) return TSLV;				-- creates TSLV(length-1 downto VEC_INDEX_MIN) + fills '0'

  function  TVLcreate(arg:TN) return TVL;					-- returns minimal TSLV size to store 'arg'
  function  SLVMax(arg:TN) return TN;					-- returns maximum. value for min. TSLV size to store 'arg'

  function  SLVBitCount(arg:TSLV; b :TSL) return TN;				-- returns number of 'b' bits from 'arg'
  function  SLVBit0Count(arg:TSLV) return TN;					-- returns number of '0' bits from 'arg'
  function  SLVBit1Count(arg:TSLV) return TN;					-- returns number of '1' bits from 'arg'
  
  function  "**"(a, b :TSLV) return TSLV;					-- returns TSLV(a'length + b'length-1 downto 0)
  function  "&"(a, b :TSLV) return TSLV;						-- returns TSLV(a'length + b'length-1 downto 0)

  function  minimum(a, b :TSLV) return TSLV;						-- returns less value from 'a' or 'b'
  function  maximum(a, b :TSLV) return TSLV;						-- returns biger value from 'a' or 'b'
  function  sel(t, f :TSL; c :TL) return TSL;					-- returns 't' for 'c'=TRUE or 'f' for 'c'=FALSE
  function  sel(t, f :TSLV; c :TL) return TSLV;				-- returns 't' for 'c'=TRUE or 'f' for 'c'=FALSE

  function  TNconv(arg:TC) return TN;					-- 'TC' type convertion to 'TI' (as ANSII code)
  function  TSLVconv(arg:TC; len :TVL) return TSLV;				-- 'TC' type convertion to 'TSLV' with 'len' size
  function  TSLVconv(arg:TC) return TSLV;					-- 'TC' type convertion to 'TSLV' with 8 bit size
  function  TNconv(arg:TS) return TN;					-- 'TS' type convertion to 'TI' (as ANSII word)
  function  TSLVconv(arg:TS; len :TVL) return TSLV;				-- 'TS' type convertion to 'TSLV' with 'len' size
  function  TSLVconv(arg:TS) return TSLV;					-- 'TS' type convertion to 'TSLV' with 8*'arg' size
  function  TSLVconv(arg:TS; dst :TSLV) return TSLV;				-- 'TS' converts to TSLV with dst'lenght size

  -------------------------------------------------------------------
  -- vectors translate family
  -------------------------------------------------------------------
  type      TVT								-- 'TVT' type covers line function  parameters
            is record
	      a :TI;
	      b :TI;
	    end record;

  function  TVTcreate(sl, sr, dl, dr :TVI) return TVT;		-- generates 'TVT' where dl=a*dr+b, dr=a*sr+b
  function  VTindex(vt :TVT; index :TVI) return TVI;	-- counts y=vt.a*index+vt.b
  function  VTindex(sl, sr, dl, dr, index :TVI) return TVI;	-- generates 'TVT' for sl,..,dr and count y
  
  -------------------------------------------------------------------
  -- vectors translate family for SLV
  -------------------------------------------------------------------
  function  TVTcreate(src :TSLV; dl, dr :TVI) return TVT;		-- generates 'TVT' for src'left,src'right,dl,dr
  function  TVTcreate(src, dst :TSLV) return TVT;			-- generates 'TVT' for 'src' and 'dst' ranges
  function  VTnorm(src :TSLV) return TVT;				-- 'TVT' tranlates to(src'lenght-1 downto VEC_INDEX_MIN)
  function  VTrevNorm(src :TSLV) return TVT;				-- 'TVT' tranlates to(VEC_INDEX_MIN to src'lenght-1)
  function  TSLVtrans(src, dst :TSLV) return TSLV;				-- converts 'src' to TSLV(dst'left to/downto dst'right)    
  function  SLVNorm(src :TSLV) return TSLV;					-- converts 'src' to TSLV(src'lenght-1 downto VEC_INDEX_MIN)    
  function  SLVrevNorm(src :TSLV) return TSLV;					-- converts 'src' to TSLV(VEC_INDEX_MIN to src'lenght-1)    
  function  SLVrevRange(src :TSLV) return TSLV;					-- converts 'src' to TSLV(src'reverse_range)

  -------------------------------------------------------------------
  -- vectors resize family for TSLV
  -------------------------------------------------------------------
  function  TSLVResize(src :TSLV; l,r :TVI; f :TSL) return TSLV;		-- resize 'src' to TSLV(l to/downto r), fills 'f' new bits
  function  TSLVResize(src :TSLV; l,r :TVI) return TSLV;			-- resize 'src' to TSLV(l to/downto r), fills '0' new bits
  function  TSLVResize(src :TSLV; len :TVL; f :TSL) return TSLV;		-- resize 'src' to TSLV(len-1 downto VEC_INDEX_MIN) + fill 'f'
  function  TSLVResize(src :TSLV; len :TVL) return TSLV;			-- resize 'src' to TSLV(len-1 downto VEC_INDEX_MIN) + fill '0'
  function  TSLVResize(src, dst :TSLV; f :TSL) return TSLV;			-- resize 'src' to TSLV(dst'left .. dst'right) + fill 'f'
  function  TSLVResize(src, dst :TSLV) return TSLV;				-- resize 'src' to TSLV(dst'left .. dst'right) + fill '0'

  -------------------------------------------------------------------
  -- HEX type family
  -------------------------------------------------------------------
  type      TH is (								-- 'TH' definition
	      '0', '1', '2', '3', '4', '5', '6', '7',
              '8', '9',	'A', 'B', 'C', 'D', 'E', 'F'
	    );
  type      THV is array(TN range<>) of TH;
  
  function  TNconv(arg:TH) return TN;					-- 'TH' type convertion to 'TI' 
  function  TSLVconv(arg:TH; len :TVL) return TSLV;				-- 'TH' type convertion to 'TSLV' with 'len' size
  function  TSLVconv(arg:TH) return TSLV;					-- 'TH' type convertion to 'TSLV' with 4 bit size
  function  TNconv(arg:THV) return TN;					-- 'THVec' type convertion to 'TI'
  function  TSLVconv(arg:THV; len :TVL) return TSLV;			-- 'THVec' type convertion to 'TSLV' with 'len' size
  function  TSLVconv(arg:THV) return TSLV;					-- 'THVec' type convertion to 'TSLV' with 4*'arg' size
  function  TSLVconv(arg:THV; dst :TSLV) return TSLV;				-- 'THVec' converts to TSLV with dst'lenght size

  -------------------------------------------------------------------
  -- vectors range family for TSLV
  -------------------------------------------------------------------
  function  TVRcreate(src:TSLV) return TVR;				-- generates 'TVR' for v'left and v'right
  function  TSLVnew(par :TVR; f :TSL) return TSLV;			-- generates TSLV(par.l to/downto par.r) + fill 'f'
  function  TSLVnew(par :TVR) return TSLV;				-- generates TSLV(par.l to/downto par.r) + fill '0'
  function  TSLVResize(src :TSLV; par :TVR; f :TSL) return TSLV;		-- resize 'src' to TSLV(par.l .. par.r) + fill 'f'
  function  TSLVResize(src :TSLV; par :TVR) return TSLV;			-- resize 'src' to TSLV(par.l .. par.r) + fill '0'
  function  TSLconv(src:TSLV; vr :TVR) return TSL;				-- converts 'src' to TSL as TSLV(vr.r)
  function  TSLVresize(src:TSL; vr :TVR) return TSLV;				-- converts 'src' to TSLV(vr.l to/downto vr.r)
  function  TSLVresize(src:TSLV; vt :TVT; vr :TVR) return TSLV;		-- converts 'src' to TSLV(vt(vr.l) to/downto vt(vr.r))
  function  TSLconv(src:TSLV; vt :TVT; vr :TVR) return TSL;		-- converts 'src' to TSL as TSLV(vt(vr.r))
  function  TSLVresize(src:TSL; vt :TVT; vr :TVR) return TSLV;		-- converts 'src' to TSLV(vt(vr.l) to/downto vt(vr.r))
  function  TSLVtrans(src :TSLV; par :TVR) return TSLV;			-- converts 'src' to TSLV(par.l .. par.r)
  function  TSLVtrans(src :TSL; par :TVR) return TSLV;			-- converts 'src' to TSLV(par.l .. par.r)
  procedure TSLVputS(signal dst :inout TSLV; par :TVR; src :TSLV);	-- performs:vec(par.l to/downto par.r)=src; returns 'vec'
  procedure TSLVput(dst :inout TSLV; par :TVR; src :TSLV);			-- performs:vec(par.l to/downto par.r)=src; returns 'vec'
  procedure TSLVputS(signal dst :inout TSLV; par :TVR; src :TSL);	-- performs:vec(par.l)=src; returns 'vec'
  procedure TSLVput(dst :inout TSLV; par :TVR; src :TSL);			-- performs:vec(par.l)=src; returns 'vec'

  -------------------------------------------------------------------
  -- vectors partitioning
  -------------------------------------------------------------------
  function  SLVPartNum(vlen, plen :TVL) return TP;				-- calculates part numbers (len:'plen') in vector (len:'vlen')
  function  SLVPartNum(v :TSLV; plen :TVL) return TP;			-- calculates part numbers (len:'plen') in vector 'v'
  function  SLVPartSize(vlen, plen :TVL; index :TVI) return TVL;	-- calculates size of part number 'index' in vector
  function  SLVPartSize(v :TSLV; plen :TVL; index :TVI) return TVL;-- calculates size of part number 'index' in vector 'v'
  function  SLVPartSize(v :TSLV; plen :TVL; vi :TSLV) return TVL;	-- calculates size of part selected by 'vi' in vector 'v'
  function  SLVPartLastSize(vlen, plen :TVL) return TVL;			-- calculates size of last part in vector
  function  SLVPartLastSize(v :TSLV; plen :TVL) return TVL;		-- calculates size of last part in vector 'v'
  function  SLVPartGet(v :TSLV; plen :TVL; index :TVI) return TSLV;	-- returns 'v' part number 'index'
  function  SLVPartGet(v :TSLV; plen :TVL; vi :TSLV) return TSLV;		-- returns 'v' part selected by 'vi' (
  function  SLVPartAddrExpand(vlen, plen :TVL) return TN;			-- calculates address size selecting parts
  function  SLVPartAddrExpand(v :TSLV; plen :TVL) return TN;		-- calculates address size selecting parts in vectopr 'v'

  -------------------------------------------------------------------
  -- vectors multiplexing
  -------------------------------------------------------------------
  function  SLVDemux(src:TSLV;level:TVL; pos: TVI) return TSLV;	-- demuxing on 'level' vectors, output: 'pos' vector
  function  SLVMux(src0, src1 :TSLV) return TSLV;				-- muxing two vectors
  function  SLVMux(src0, src1, src2 :TSLV) return TSLV;				-- muxing three vectors
  function  SLVMux(src0, src1, src2, src3 :TSLV) return TSLV;			-- muxing four vectors

  -------------------------------------------------------------------
  -- two dimentions arrays emulation
  -------------------------------------------------------------------
  type      TA2D									-- 'TA2D' type covers column(y) and row(x) area sizes
            is record
              y :TVL;
              x :TVL;
            end record;
  
  function  TA2Dcreate(ya, xa :TVL) return TA2D;					-- generates 'TA2D' type where:y=ya and x=xa => A2D[ya,xa]
  function  A2DSize(ya, xa :TVL) return TVL;				-- returns TSLV size for A2D[ya,xa]
  function  A2DSize(ad :TA2D) return TVL;					-- returns TSLV size for A2D[ad]
  function  A2DIndex(ya, xa :TVL; y, x :TVI) return TVI;		-- returns TSLV position in A2D[ya,xa](x,y)
  function  A2DIndex(ad :TA2D; y, x :TVI) return TVI;		-- returns TSLV position in A2D[ad](x,y)

  -------------------------------------------------------------------
  -- two dimentions arrays emulation for TSLV
  -------------------------------------------------------------------
  function  A2DInitArea(ya, xa :TVL; d :TSL) return TSLV;			-- returns TSLV emulates A2D[ya,xa] => TSLV[ya,xa]
  function  A2DInitArea(ad :TA2D; d :TSL) return TSLV;				-- returns TSLV[ad.y,ad.x] => TSLV[ya,xa]

  function  A2DGetCell(v :TSLV; ya, xa :TVL; y, x :TVI) return TSL;	-- returns v[ya,xa](y,x)
  function  A2DGetCell(v :TSLV; ad :TA2D; y, x :TVI) return TSL;		-- returns v[ad](y,x)
  function  A2DGetRange(v :TSLV; ya, xa :TVL; y, xh, xl :TVI) return TSLV; -- returns v[ya,xa](y,xh downto xl)
  function  A2DGetRange(v :TSLV; ad :TA2D; y, xh, xl :TVI) return TSLV;	-- returns v[ad](y,xh downto xl)
  function  A2DGetRow(v :TSLV; ya, xa :TVL; y :TVI) return TSLV;	-- returns v[ya,xa](y,xa-1 downto VEC_INDEX_MIN)
  function  A2DGetRow(v :TSLV; ad :TA2D; y :TVI) return TSLV;		-- returns v[ad](y,ad.x-1 downto VEC_INDEX_MIN)
  function  A2DGetLastRow(v :TSLV; ya, xa :TVL) return TSLV;			-- returns v[ya,xa](ya,xa-1 downto VEC_INDEX_MIN)
  function  A2DGetLastRow(v :TSLV; ad :TA2D) return TSLV;			-- returns v[ad](ad.y,ad.x-1 downto VEC_INDEX_MIN)

  function  A2DSetCell(v :TSLV; ya, xa :TVL; y, x :TVI; d :TSL) return TSLV; -- set v[ya,xa](y,x)=d; returns v
  function  A2DSetCell(v :TSLV; ad :TA2D; y, x :TVI; d :TSL) return TSLV;	-- set v[ad](y,x)=d; returns v
  function  A2DSetRange(v :TSLV; ya, xa :TVL; y, xh, xl :TVI; d :TSLV) return TSLV; -- set v[ya,xa](y,xh downto xl)=d; returns v
  function  A2DSetRange(v :TSLV; ad :TA2D; y, xh, xl :TVI; d :TSLV) return TSLV; -- set v[ad](y,xh downto xl)=d; returns v
  function  A2DSetRow(v :TSLV; ya, xa :TVL; y :TVI; d :TSLV) return TSLV; -- set v[ya,xa](y,xa-1 downto VEC_INDEX_MIN)=d; returns v
  function  A2DSetRow(v :TSLV; ad :TA2D; y :TVI; d :TSLV) return TSLV;	-- set v[ad](y,ad.x-1 downto VEC_INDEX_MIN)=d; returns v

  function  A2DFillRange(v :TSLV; ya, xa :TVL; y, xh, xl :TVI; d :TSL) return TSLV; -- fill v[ya,xa](y,xh downto xl)=d; returns v
  function  A2DFillRange(v :TSLV; ad :TA2D; y, xh, xl :TVI; d :TSL) return TSLV; -- fill v[ad](y,xh downto xl)=d; returns v
  function  A2DFillRow(v :TSLV; ya, xa :TVL; y :TVI; d :TSL) return TSLV; -- fill v[ya,xa](y,xa-1 downto VEC_INDEX_MIN)=d; returns v
  function  A2DFillRow(v :TSLV; ad :TA2D; y :TVI; d :TSL) return TSLV;	-- fill v[ad](y,ad.x-1 downto VEC_INDEX_MIN)=d; returns v

end std_logic_1164_ktp;

package body std_logic_1164_ktp is

  -------------------------------------------------------------------
  -- standard package extentions
  -------------------------------------------------------------------
  function minimum(a, b :TS) return TS is
  begin
    if (a<b) then
      return(a);
    else
      return(b);
    end if;
  end function;
  
  -------------------------------------------------------------------

  function maximum(a, b :TS) return TS is
  begin
    if (a>b) then
      return(a);
    else
      return(b);
    end if;
  end function; 

  -------------------------------------------------------------------

  function sel(t, f :TS; c :TL) return TS is
  begin
    if (c=TRUE) then
      return(t);
    else
      return(f);
    end if;
  end function;  

  -------------------------------------------------------------------
  -- std_logic_arith package extensions
  -------------------------------------------------------------------
  function minimum(a, b :TI) return TI is
  begin
    if (a<b) then
      return(a);
    else
      return(b);
    end if;
  end function;
  
  -------------------------------------------------------------------

  function maximum(a, b :TI) return TI is
  begin
    if (a>b) then
      return(a);
    else
      return(b);
    end if;
  end function; 

  -------------------------------------------------------------------

  function sel(t, f :TI; c :TL) return TI is
  begin
    if (c=TRUE) then
      return(t);
    else
      return(f);
    end if;
  end function;  

  -------------------------------------------------------------------

  function minimum(v :TIV) return TI is
    variable var :TI;
  begin
    var := v(v'left);
    for index in v'range loop
      if (v(index)<var) then
        var := v(index);
      end if;
    end loop;
    return(var);
  end function; 

  -------------------------------------------------------------------

  function maximum(v :TIV) return TI is
    variable var :TI;
  begin
    var := v(v'left);
    for index in v'range loop
      if (v(index)>var) then
        var := v(index);
      end if;
    end loop;
    return(var);
  end function; 

  -------------------------------------------------------------------

  function minimum(v :TNV) return TN is
    variable var :TN;
  begin
    var := v(v'left);
    for index in v'range loop
      if (v(index)<var) then
        var := v(index);
      end if;
    end loop;
    return(var);
  end function; 

  -------------------------------------------------------------------

  function maximum(v :TNV) return TN is
    variable var :TN;
  begin
    var := v(v'left);
    for index in v'range loop
      if (v(index)>var) then
        var := v(index);
      end if;
    end loop;
    return(var);
  end function; 

  -------------------------------------------------------------------

  function minimum(v :TPV) return TP is
    variable var :TP;
  begin
    var := v(v'left);
    for index in v'range loop
      if (v(index)<var) then
        var := v(index);
      end if;
    end loop;
    return(var);
  end function; 

  -------------------------------------------------------------------

  function maximum(v :TPV) return TP is
    variable var :TP;
  begin
    var := v(v'left);
    for index in v'range loop
      if (v(index)>var) then
        var := v(index);
      end if;
    end loop;
    return(var);
  end function; 

  -------------------------------------------------------------------

  function  pow2(v :TN) return TN is
  begin
    return (2 ** v);
  end function;

  -------------------------------------------------------------------
  -- vectors range family
  -------------------------------------------------------------------

  function TVRcreate(l, r :TVI) return TVR is
    variable vr :TVR;
  begin
    vr.l := l;
    vr.r := r;
    return(vr);
  end function;

  -------------------------------------------------------------------

  function TVRcreate(len :TVL) return TVR is
  begin
    return(TVRcreate(len-1,VEC_INDEX_MIN));
  end function;

  -------------------------------------------------------------------

  function TVLcreate(l, r :TVI) return TVL is
  begin
    return(maximum(l,r)-minimum(l,r)+1);
  end function;

  -------------------------------------------------------------------

  function TVLcreate(par :TVR) return TVL is
  begin
    return(TVLcreate(par.l,par.r));
  end function;

  -------------------------------------------------------------------

  function minimum(v :TVLV) return TVL is
    variable var :TVL;
  begin
    var := v(v'left);
    for index in v'range loop
      if (v(index)<var) then
        var := v(index);
      end if;
    end loop;
    return(var);
  end function; 

  -------------------------------------------------------------------

  function maximum(v :TVLV) return TVL is
    variable var :TVL;
  begin
    var := v(v'left);
    for index in v'range loop
      if (v(index)>var) then
        var := v(index);
      end if;
    end loop;
    return(var);
  end function; 

  -------------------------------------------------------------------

  function minimum(v :TVIV) return TVI is
    variable var :TVI;
  begin
    var := v(v'left);
    for index in v'range loop
      if (v(index)<var) then
        var := v(index);
      end if;
    end loop;
    return(var);
  end function; 

  -------------------------------------------------------------------

  function maximum(v :TVIV) return TVI is
    variable var :TVI;
  begin
    var := v(v'left);
    for index in v'range loop
      if (v(index)>var) then
        var := v(index);
      end if;
    end loop;
    return(var);
  end function; 

  -------------------------------------------------------------------

  function "**" (l, r :TVR) return TVR is
    variable vr :TVR;
    variable vmin, vmax :TVI;
  begin
    vmin := minimum(minimum(l.l,l.r),minimum(r.l,r.r));
    vmax := maximum(maximum(l.l,l.r),maximum(r.l,r.r));
    if ((l.l<l.r) and (r.l<r.r)) then
      vr.l := vmin;
      vr.r := vmax;
    else
      vr.l := vmax;
      vr.r := vmin;
    end if;
    return(vr);
  end function; 

  -------------------------------------------------------------------

  function TVRVconv(arg :TVR) return TVRV is
    variable vec :TVRV(VEC_INDEX_MIN to VEC_INDEX_MIN);
  begin
    vec(VEC_INDEX_MIN) := arg;
    return(vec);
  end function; 

  -------------------------------------------------------------------
			    
  function TVLcreate(arg :TVRV) return TVL is
  begin
    return(arg(arg'left).l-arg(arg'right).r+1);
  end function;

  -------------------------------------------------------------------
			    
  function "+" (l, r :TVRV) return TVRV is
    variable vec :TVRV(l'length+r'length-1 downto VEC_INDEX_MIN);
    variable base :TVI;
  begin
    vec(r'length-1 downto VEC_INDEX_MIN) := r;
    vec(vec'left downto r'length) := l;
    if (r(r'left).l>=r(r'right).r) then
      base := r(r'left).l+1;
      for index in vec'left downto r'length loop
        vec(index).l := vec(index).l + base;
        vec(index).r := vec(index).r + base;
      end loop;
    else
      base := l(r'right).r+1;
      for index in r'length-1 downto VEC_INDEX_MIN loop
        vec(index).l := vec(index).l + base;
        vec(index).r := vec(index).r + base;
      end loop;
    end if;
    return(vec);
  end function; 

  -------------------------------------------------------------------
			    
  function "+" (l, r :TVR) return TVRV is
  begin
    return(TVRVconv(l)+TVRVconv(r));
  end function; 

  -------------------------------------------------------------------

  function "+" (l :TVRV; r :TVR) return TVRV is
  begin
    return(l+TVRVconv(r));
  end function; 

  -------------------------------------------------------------------
			    
  function "+" (l :TVR; r :TVRV) return TVRV is
  begin
    return(TVRVconv(l)+r);
  end function; 
 

  -------------------------------------------------------------------
  -- TSLV family
  -------------------------------------------------------------------
  function TSLconv(arg :TN) return TSL is
  begin
    if (arg = 0) then
      return('0');
    else
      return('1');
    end if;
  end function;
  
  -------------------------------------------------------------------
  function TSLconv(arg :TL) return TSL is
  begin
    if (arg = TRUE) then
      return('1');
    else
      return('0');
    end if;
  end function;
  
  -------------------------------------------------------------------

  function TSLconv(arg :TSLV) return TSL is
  begin
    return(arg(arg'low));
  end function;
  
  -------------------------------------------------------------------

  function TNconv(arg :TSL) return TN is
  begin
    if (arg = '0') then
      return(0);
    else
      return(1);
    end if;
  end function;
    
  -------------------------------------------------------------------

  function TLconv(arg :TSL) return TL is
  begin
    if (arg = '0') then
      return(FALSE);
    else
      return(TRUE);
    end if;
  end function;  
  
  -------------------------------------------------------------------

  function TSLVconv(arg :TN; len :TVL) return TSLV is
  begin
    return(TSLV(CONV_UNSIGNED(arg,len)));
  end function;  
  
  -------------------------------------------------------------------

  function TSLVconv(arg :TN) return TSLV is
  begin
    return(TSLVconv(arg,TVLcreate(arg)));
  end function;  
  
  -------------------------------------------------------------------

  function TSLVconv(arg :TL; len :TVL) return TSLV is
  begin
    return(TSLVconv(TSLconv(arg),len));
  end function;  
  
  -------------------------------------------------------------------

  function TSLVconv(arg :TL) return TSLV is
  begin
    return(TSLVconv(arg,1));
  end function;  
  
  -------------------------------------------------------------------

  function TSLVconv(arg:TSL; len :TVL) return TSLV is
    variable vec :TSLV(len-1 downto VEC_INDEX_MIN);
  begin
    vec := (others => '0');
    vec(VEC_INDEX_MIN) := arg;
    return(vec);
  end function;  
  
  -------------------------------------------------------------------

  function TSLVconv(arg:TSL) return TSLV is 
  begin
    return(TSLVconv(arg,1));
  end function;  
  
  -------------------------------------------------------------------

  function TNconv(arg :TSLV) return TN is
  begin
    return(CONV_INTEGER(UNSIGNED(arg)));
  end function;  
  
  -------------------------------------------------------------------

  function TLconv(arg :TSLV) return TL is
  begin
    return(TLconv(OR_REDUCE(arg)));
  end function;  
  
  -------------------------------------------------------------------

  procedure TSLVputS(signal vec :inout TSLV; dst, src :TSLV) is
  begin
    if (dst'length>1) then
      vec(dst'range)<=src;
    else
      vec(dst'right)<=TSLconv(src);
    end if;
  end procedure;
  
  -------------------------------------------------------------------

  procedure TSLVput(vec :inout TSLV; dst, src :TSLV) is
  begin
    if (dst'length>1) then
      vec(dst'range):=src;
    else
      vec(dst'right):=TSLconv(src);
    end if;
  end procedure;
  
  -------------------------------------------------------------------

  procedure TSLVputS(signal vec :inout TSLV; dst :TSLV; src :TSL) is
  begin
    vec(dst'right)<=src;
  end procedure;
  
  -------------------------------------------------------------------

  procedure TSLVput(vec :inout TSLV; dst :TSLV; src :TSL) is
  begin
    vec(dst'right):=src;
  end procedure;
  
  -------------------------------------------------------------------

  procedure TSLVputS(signal dst :inout TSLV; src :TSLV) is
  begin
    dst(src'range)<=src;
  end procedure;

  -------------------------------------------------------------------

  procedure TSLVput(dst :inout TSLV; src :TSLV) is
  begin
    dst(src'range):=src;
  end procedure;

  -------------------------------------------------------------------

  procedure TSLVputS(signal dst :inout TSLV; l,r :TVI; src :TSLV) is
  begin
    TSLVputS(dst,TSLVnew(l,r),src);
  end procedure;

  -------------------------------------------------------------------

  procedure TSLVput(dst :inout TSLV; l,r :TVI; src :TSLV) is
  begin
    TSLVput(dst,TSLVnew(l,r),src);
  end procedure;

  -------------------------------------------------------------------

  procedure TSLVfillS(signal dst :inout TSLV; f :TSL) is
  begin
    for index in dst'range loop
      dst(index)<=f;
    end loop;
  end procedure;

  -------------------------------------------------------------------

  procedure TSLVfill(dst :inout TSLV; f :TSL) is
  begin
    for index in dst'range loop
      dst(index):=f;
    end loop;
  end procedure;

  -------------------------------------------------------------------

  function TSLVnew(l,r :TVI; f:TSL) return TSLV is
    variable hvec :TSLV(maximum(l,r) downto minimum(l,r));
    variable lvec :TSLV(minimum(l,r) to maximum(l,r));
  begin
    if (l>=r) then
      hvec := (others => f);
      return(hvec);
    else
      lvec := (others => f);
      return(lvec);
    end if;
  end function;
  
  -------------------------------------------------------------------

  function TSLVnew(l,r :TVI) return TSLV is
  begin
    return(TSLVnew(l,r,'0'));
  end function;
  
  -------------------------------------------------------------------

  function TSLVnew(length :TVL; f:TSL) return TSLV is
  begin
    return(TSLVnew(length-1,VEC_INDEX_MIN,f));
  end function;
  
  -------------------------------------------------------------------

  function TSLVnew(length :TVL) return TSLV is
  begin
    return(TSLVnew(length-1,VEC_INDEX_MIN,'0'));
  end function;
  
  -------------------------------------------------------------------

  function TVLcreate(arg :TN) return TVL is
    constant BIT_RANGE :TVL := 32;  -- arbitrary !!!
    variable sum :TVL;
  begin
    sum := 1;
    for index in 1 to BIT_RANGE loop
      sum := 2*sum;
      if (sum>arg) then
        return(index);
      end if;
    end loop;
    return(BIT_RANGE);
  end function;
  
  -------------------------------------------------------------------

  function SLVMax(arg :TN) return TN is
  begin
    return((2**TVLcreate(arg))-1);
  end function;
  
  -------------------------------------------------------------------

  function SLVBitCount(arg :TSLV; b :TSL) return TN is
    variable sum:TN;
  begin
    sum:=0;
    for index in arg'range loop
      if (arg(index)=b) then
        sum:=sum+1;
      end if;
    end loop;
    return(sum);
  end function;  
  
  -------------------------------------------------------------------

  function SLVBit0Count(arg :TSLV) return TN is
  begin
    return(SLVBitCount(arg,'0'));
  end function;  
  
  -------------------------------------------------------------------

  function SLVBit1Count(arg :TSLV) return TN is
  begin
    return(SLVBitCount(arg,'1'));
  end function;  

  -------------------------------------------------------------------

  function "**"(a, b :TSLV) return TSLV is
    function \_**_\(v, a, b :TSLV) return TSLV is
      variable vec :TSLV(v'range);
    begin
      vec := v;
      for index in a'range loop
        vec(index) := a(index);
      end loop;
      for index in b'range loop
        vec(index) := b(index);
      end loop;
      return(vec);
    end function;
  begin
    return(\_**_\(TSLVnew(TVRcreate(a) ** TVRcreate(b)),a,b));
  end function;  

  -------------------------------------------------------------------

  function "&"(a, b :TSLV) return TSLV is
    variable vec :TSLV(a'length+b'length-1 downto 0);
  begin
    vec(b'length-1 downto 0) := b;
    vec(vec'length-1 downto b'length) := a;
    return(vec);
  end function;  

  -------------------------------------------------------------------

  function minimum(a, b :TSLV) return TSLV is
  begin
    if (a<b) then
      return(a);
    else
      return(b);
    end if;
  end function;
  
  -------------------------------------------------------------------

  function maximum(a, b :TSLV) return TSLV is
  begin
    if (a>b) then
      return(a);
    else
      return(b);
    end if;
  end function;
  
  -------------------------------------------------------------------

  function sel(t, f :TSL; c :TL) return TSL is
  begin
    if (c=TRUE) then
      return(t);
    else
      return(f);
    end if;
  end function;  

  -------------------------------------------------------------------

  function sel(t, f :TSLV; c :TL) return TSLV is
  begin
    if (c=TRUE) then
      return(t);
    else
      return(f);
    end if;
  end function;  

  -------------------------------------------------------------------

  function TNconv(arg:TC) return TN is
    type TCharTab is array(0 to 255) of  character;
    constant chr :TCharTab := (
      nul, soh, stx, etx, eot, enq, ack, bel,		-- 00
      bs,  ht,  lf,  vt,  ff,  cr,  so,  si,		-- 08
      dle, dc1, dc2, dc3, dc4, nak, syn, etb,		-- 10
      can, em,  sub, esc, fsp, gsp, rsp, usp,		-- 18
      ' ', '!', '"', '#', '$', '%', '&', ''',		-- 20
      '(', ')', '*', '+', ',', '-', '.', '/',		-- 28
      '0', '1', '2', '3', '4', '5', '6', '7',		-- 30
      '8', '9', ':', ';', '<', '=', '>', '?',		-- 38
      '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G',		-- 40
      'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',		-- 48
      'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',		-- 50
      'X', 'Y', 'Z', '[', '\', ']', '^', '_',		-- 58
      '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g',		-- 60
      'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',		-- 68
      'p', 'q', 'r', 's', 't', 'u', 'v', 'w',		-- 70
      'x', 'y', 'z', '{', '|', '}', '~', del,		-- 78
      c128, c129, c130, c131, c132, c133, c134, c135,	-- 80
      c136, c137, c138, c139, c140, c141, c142, c143,	-- 88
      c144, c145, c146, c147, c148, c149, c150, c151,	-- 90
      c152, c153, c154, c155, c156, c157, c158, c159,	-- 98
      '�', '�', '�', '�', '�', '�', '�', '�',		-- A0
      '�', '�', '�', '�', '�', '�', '�', '�',		-- A8
      '�', '�', '�', '�', '�', '�', '�', '�',		-- B0
      '�', '�', '�', '�', '�', '�', '�', '�',		-- B8
      '�', '�', '�', '�', '�', '�', '�', '�',		-- C0
      '�', '�', '�', '�', '�', '�', '�', '�',		-- C8
      '�', '�', '�', '�', '�', '�', '�', '�',		-- D0
      '�', '�', '�', '�', '�', '�', '�', '�',		-- D8
      '�', '�', '�', '�', '�', '�', '�', '�',		-- E0
      '�', '�', '�', '�', '�', '�', '�', '�',		-- E8
      '�', '�', '�', '�', '�', '�', '�', '�',		-- F0
      '�', '�', '�', '�', '�', '�', '�', '�' );		-- F8
  begin
    for index in chr'range loop
      if (chr(index)=arg) then
        return(index);
      end if;
    end loop;
    return(0);
  end;

  -------------------------------------------------------------------

  function TSLVconv(arg:TC; len:TVL) return TSLV is
  begin
    return(TSLVconv(TNconv(arg),len));  
  end function;

  -------------------------------------------------------------------

  function TSLVconv(arg:TC) return TSLV is
  begin
    return(TSLVconv(arg,8));  
  end function;

  -------------------------------------------------------------------

  function TNconv(arg:TS) return TN is
    constant tv :TVT := TVTcreate(arg'left, arg'right, arg'length, 1);	
    variable val :TN;
  begin
    val := 0;
    for index in arg'range loop
      val:=val+(tv.a*index+tv.b)*TNconv(arg(index));
    end loop;
    return(val);
  end function;

  -------------------------------------------------------------------

  function TSLVconv(arg:TS) return TSLV is
    constant tv :TVT := TVTcreate(arg'left,arg'right,arg'length-1,VEC_INDEX_MIN);
    variable val :TSLV((arg'length*8)-1 downto VEC_INDEX_MIN);
    variable pos :TVI;
  begin
    for index in arg'range loop
      pos := 8*(tv.a*index+tv.b);
      val(pos+7 downto pos) := TSLVconv(TNconv(arg(index)),8);
    end loop;
    return(val);  
  end function;

  -------------------------------------------------------------------

  function TSLVconv(arg:TS; len :TVL) return TSLV is
  begin
    return(TSLVResize(TSLVconv(arg),len));
  end function;

  -------------------------------------------------------------------

  function TSLVconv(arg:TS; dst :TSLV) return TSLV is
  begin
    return(TSLVconv(arg,dst'length));
  end function;


  -------------------------------------------------------------------
  -- vectors translate family
  -------------------------------------------------------------------

  function TVTcreate(sl, sr, dl, dr :TVI) return TVT is
    variable tv :TVT;
  begin
    tv := (0,0);
    if (abs(sl-sr)=abs(dl-dr)) then
      if(dl/=dr) then
        tv.a := (dl-dr)/(sl-sr);
        tv.b := dl-(tv.a*sl);
      else
	tv := (0,dl);
      end if;
    end if;
    return(tv);
  end function;

  -------------------------------------------------------------------

  function VTindex(vt :TVT; index :TVI) return TVI is
  begin
    return(vt.a*index+vt.b);
  end function;
    
  -------------------------------------------------------------------

  function VTindex(sl, sr, dl, dr, index :TVI) return TVI is
    constant vt :TVT := TVTcreate(sl,sr,dl,dr);
  begin
    return(VTindex(vt,index));
  end function;


  -------------------------------------------------------------------
  -- vectors translate family for SLV
  -------------------------------------------------------------------

  function TVTcreate(src :TSLV; dl, dr :TVI) return TVT is
  begin
    return(TVTcreate(src'left,src'right,dl,dr));
  end function;

  -------------------------------------------------------------------

  function TVTcreate(src, dst :TSLV) return TVT is
  begin
    return(TVTcreate(src'left,src'right,dst'left,dst'right));
  end function;

  -------------------------------------------------------------------

  function VTnorm(src :TSLV) return TVT is
  begin
    return(TVTcreate(src'left,src'right,src'length-1,VEC_INDEX_MIN));
  end function;

  -------------------------------------------------------------------

  function VTrevNorm(src :TSLV) return TVT is
  begin
    return(TVTcreate(src'left,src'right,VEC_INDEX_MIN,src'length-1));
  end function;

  -------------------------------------------------------------------

  function TSLVtrans(src, dst :TSLV) return TSLV is
    constant tv :TVT := TVTcreate(src,dst);
    variable vec :TSLV(dst'range);
  begin
    for index in src'range loop
      vec(VTindex(tv,index)):=src(index);
    end loop;
    return(vec);
  end function;

  -------------------------------------------------------------------

  function SLVNorm(src :TSLV) return TSLV is
    variable dst :TSLV(src'length-1 downto VEC_INDEX_MIN);
  begin
    return(TSLVtrans(src,dst));
  end function;

  -------------------------------------------------------------------

  function SLVrevNorm(src :TSLV) return TSLV is
    variable dst :TSLV(VEC_INDEX_MIN to src'length-1);
  begin
    return(TSLVtrans(src,dst));
  end function;

  -------------------------------------------------------------------

  function SLVrevRange(src :TSLV) return TSLV is
    variable dst :TSLV(src'reverse_range);
  begin
    return(TSLVtrans(src,dst));
  end function;


  -------------------------------------------------------------------
  -- vectors resize family for TSLV
  -------------------------------------------------------------------

  function TSLVResize(src :TSLV; l,r :TVI; f :TSL) return TSLV is
    function \_TSLVResize_\(v, src :TSLV) return TSLV is
      variable vec :TSLV(v'range);
    begin
      vec := v;
      for index in vec'range loop
        if ((index>=src'low) and (index<=src'high)) then
          vec(index) := src(index);
        end if;
      end loop;
      return(vec);
    end function;
  begin
    return(\_TSLVResize_\(TSLVnew(l,r,f),src));
  end function;

  -------------------------------------------------------------------

  function TSLVResize(src :TSLV; l,r :TVI) return TSLV is
  begin
    return(TSLVResize(src,l,r,'0'));
  end function;

  -------------------------------------------------------------------

  function TSLVResize(src :TSLV; len :TVL; f :TSL) return TSLV is
  begin
    return(TSLVResize(src,len-1,VEC_INDEX_MIN,f));
  end function;

  -------------------------------------------------------------------

  function TSLVResize(src :TSLV; len :TVL) return TSLV is
  begin
    return(TSLVResize(src,len,'0'));
  end function;

  -------------------------------------------------------------------

  function TSLVResize(src, dst :TSLV; f :TSL) return TSLV is
  begin
    return(TSLVResize(src,dst'left,dst'right,f));
  end function;

  -------------------------------------------------------------------

  function TSLVResize(src, dst:TSLV) return TSLV is
  begin
    return(TSLVResize(src,dst'left,dst'right,'0'));
  end function;


  -------------------------------------------------------------------
  -- HEX type family
  -------------------------------------------------------------------

  function TNconv(arg:TH) return TN is
    variable val :TN;
  begin
    case(arg) is
      when TH'('0') => val := 0;
      when TH'('1') => val := 1;
      when TH'('2') => val := 2;
      when TH'('3') => val := 3;
      when TH'('4') => val := 4;
      when TH'('5') => val := 5;
      when TH'('6') => val := 6;
      when TH'('7') => val := 7;
      when TH'('8') => val := 8;
      when TH'('9') => val := 9;
      when TH'('A') => val := 10;
      when TH'('B') => val := 11;
      when TH'('C') => val := 12;
      when TH'('D') => val := 13;
      when TH'('E') => val := 14;
      when TH'('F') => val := 15;
    end case;
    return(val);
  end function;
  
  -------------------------------------------------------------------

  function TSLVconv(arg:TH; len:TVL) return TSLV is
  begin
    return(TSLVconv(TNconv(arg),len));  
  end function;

  -------------------------------------------------------------------

  function TSLVconv(arg:TH) return TSLV is
  begin
    return(TSLVconv(arg,4));  
  end function;

  -------------------------------------------------------------------

  function TNconv(arg:THV) return TN is
    constant tv :TVT := TVTcreate(arg'left, arg'right, arg'length, 1);	
    variable val :TN;
  begin
    val := 0;
    for index in arg'range loop
      val:=val+(tv.a*index+tv.b)*TNconv(arg(index));
    end loop;
    return(val);
  end function;

  -------------------------------------------------------------------

  function TSLVconv(arg:THV) return TSLV is
    constant tv :TVT :=TVTcreate(arg'left,arg'right,arg'length-1,VEC_INDEX_MIN);
    variable val :TSLV((arg'length*4)-1 downto VEC_INDEX_MIN);
    variable pos :TVI;
  begin
    for index in arg'range loop
      pos := 4*(tv.a*index+tv.b);
      val(pos+3 downto pos) := TSLVconv(arg(index));
    end loop;
    return(val);  
  end function;

  -------------------------------------------------------------------

  function TSLVconv(arg:THV; len :TVL) return TSLV is
  begin
    return(TSLVResize(TSLVconv(arg),len));
  end function;

  -------------------------------------------------------------------

  function TSLVconv(arg:THV; dst :TSLV) return TSLV is
  begin
    return(TSLVconv(arg,dst'length));
  end function;


  -------------------------------------------------------------------
  -- vectors range family for TSLV
  -------------------------------------------------------------------

  function TVRcreate(src:TSLV) return TVR is
  begin
    return(TVRcreate(src'left, src'right));
  end function;

  -------------------------------------------------------------------

  function TSLVnew(par :TVR; f :TSL) return TSLV is
  begin
    return(TSLVnew(par.l,par.r,f));
  end function;

  -------------------------------------------------------------------

  function TSLVnew(par :TVR) return TSLV is
  begin
    return(TSLVnew(par.l,par.r));
  end function;

  -------------------------------------------------------------------

  function TSLVResize(src :TSLV; par :TVR; f :TSL) return TSLV is
  begin
    return(TSLVResize(src,par.l,par.r,f));
  end function;

  -------------------------------------------------------------------

  function TSLVResize(src :TSLV; par :TVR) return TSLV is
  begin
    return(TSLVResize(src,par.l,par.r));
  end function;

  -------------------------------------------------------------------

  function TSLconv(src:TSLV; vr :TVR) return TSL is
  begin
    return(TSLconv(TSLVresize(src,vr)));
  end function;
  
  -------------------------------------------------------------------

  function TSLVresize(src :TSL; vr :TVR) return TSLV is
  begin
    return(TSLVResize(TSLVconv(src),vr.l,vr.r));
  end function;

  -------------------------------------------------------------------

  function TSLVresize(src :TSLV; vt : TVT; vr :TVR) return TSLV is
  begin
    return(TSLVResize(src,VTindex(vt,vr.l),VTindex(vt,vr.r)));
  end function;

  -------------------------------------------------------------------

  function TSLconv(src:TSLV; vt : TVT; vr :TVR) return TSL is
  begin
    return(TSLconv(TSLVResize(src,vt,vr)));
  end function;
  
  -------------------------------------------------------------------

  function TSLVresize(src :TSL; vt : TVT; vr :TVR) return TSLV is
  begin
    return(TSLVresize(TSLVconv(src),vt,vr));
  end function;


  -------------------------------------------------------------------

  function TSLVtrans(src :TSLV; par :TVR) return TSLV is
  begin
    return(TSLVtrans(src,TSLVnew(par)));
  end function;

  -------------------------------------------------------------------

  function TSLVtrans(src :TSL; par :TVR) return TSLV is
  begin
    return(TSLVtrans(TSLVconv(src),TSLVnew(par)));
  end function;

  -------------------------------------------------------------------

  procedure TSLVputS(signal dst :inout TSLV; par :TVR; src :TSLV) is
  begin
    TSLVputS(dst,TSLVResize(dst,par),src);
  end procedure;

  -------------------------------------------------------------------

  procedure TSLVput(dst :inout TSLV; par :TVR; src :TSLV) is
  begin
    TSLVput(dst,TSLVResize(dst,par),src);
  end procedure;

  -------------------------------------------------------------------

  procedure TSLVputS(signal dst :inout TSLV; par :TVR; src :TSL) is
  begin
    TSLVputS(dst,TSLVResize(dst,par),TSLVconv(src));
  end procedure;

  -------------------------------------------------------------------

  procedure TSLVput(dst :inout TSLV; par :TVR; src :TSL) is
  begin
    TSLVput(dst,TSLVResize(dst,par),TSLVconv(src));
  end procedure;

  -------------------------------------------------------------------
  -- vectors partitioning
  -------------------------------------------------------------------

  function SLVPartNum(vlen, plen :TVL) return TP is
  begin
    return(((vlen-1)/plen)+1);
  end function;

  -------------------------------------------------------------------

  function SLVPartNum(v :TSLV; plen :TVL) return TP is
  begin
    return(SLVPartNum(v'length,plen));
  end function;

  -------------------------------------------------------------------

  function SLVPartSize(vlen, plen :TVL; index :TVI) return TVL is
    variable num :TP;
  begin
    num := SLVPartNum(vlen,plen);
    if (index<num-1) then
      return(plen);
    end if;
    return(vlen-(num-1)*plen);
  end function;

  -------------------------------------------------------------------

  function SLVPartSize(v :TSLV; plen :TVL; index :TVI) return TVL is
  begin
    return(SLVPartSize(v'length,plen,index));
  end function;

  -------------------------------------------------------------------

  function SLVPartSize(v :TSLV; plen :TVL; vi :TSLV) return TVL is
  begin
    return(SLVPartSize(v'length,plen,TNconv(vi)));
  end function;

  -------------------------------------------------------------------

  function SLVPartLastSize(vlen, plen :TVL) return TVL is
  begin
    return(SLVPartSize(vlen,plen,SLVPartNum(vlen,plen)-1));
  end function;

  -------------------------------------------------------------------

  function SLVPartLastSize(v :TSLV; plen :TVL) return TVL is
  begin
    return(SLVPartLastSize(v'length,plen));
  end function;

  -------------------------------------------------------------------

  function SLVPartGet(v :TSLV; plen :TVL; index :TVI) return TSLV is
    variable vt :TVT;
    variable vr :TVR;
  begin
    vt := TVTcreate(v,v'length-1,0);
    vr.r := index*plen;
    vr.l := vr.r + SLVPartSize(v,plen,index)-1;
    return(TSLVResize(v,vt,vr));
  end function;

  -------------------------------------------------------------------

  function SLVPartGet(v :TSLV; plen :TVL; vi :TSLV) return TSLV is
  begin
    return(SLVPartGet(v,plen,TNconv(vi)));
  end function;

  -------------------------------------------------------------------

  function SLVPartAddrExpand(vlen, plen :TVL) return TN is
    variable num :TP;
  begin
    num := SLVPartNum(vlen,plen);
    if (num=1) then
      return(0);
    end if;
    return(TVLcreate(num-1));
  end function;

  -------------------------------------------------------------------

  function SLVPartAddrExpand(v :TSLV; plen :TVL) return TN is
  begin
    return(SLVPartAddrExpand(v'length,plen));
  end function;


  -------------------------------------------------------------------
  -- vectors multiplexing
  -------------------------------------------------------------------
  function SLVDemux(src:TSLV; level:TVL; pos: TVI) return TSLV is
    variable dst :TSLV((src'length+level-1)/level-1 downto 0);
  begin
    for index in dst'range loop
      if(index<dst'length) then
        dst(index) := src(index*level+pos);
      else
        dst(index) := '0';
      end if;
    end loop;
    return(dst);
  end function;

  function SLVMux(src0, src1 :TSLV) return TSLV is
    variable dst :TSLV((src0'length*2)-1 downto 0);
  begin
    for index in src0'range loop
      dst(2*index+0) := src0(index);
      dst(2*index+1) := src1(index);
    end loop;
    return(dst);
  end function;

  function SLVMux(src0, src1, src2 :TSLV) return TSLV is
    variable dst :TSLV(src0'length*3-1 downto 0);
  begin
    for index in src0'range loop
      dst(3*index+0) := src0(index);
      dst(3*index+1) := src1(index);
      dst(3*index+2) := src2(index);
    end loop;
    return(dst);
  end function;

  function SLVMux(src0, src1, src2, src3 :TSLV) return TSLV is
    variable dst :TSLV(src0'length*4-1 downto 0);
  begin
    for index in src0'range loop
      dst(4*index+0) := src0(index);
      dst(4*index+1) := src1(index);
      dst(4*index+2) := src2(index);
      dst(4*index+3) := src3(index);
    end loop;
    return(dst);
  end function;

  -------------------------------------------------------------------
  -- two dimentions arrays emulation
  -------------------------------------------------------------------

  function TA2Dcreate(ya, xa :TVL) return TA2D is
    variable ad :TA2D;
  begin
    ad.y := ya;
    ad.x := xa;
    return(ad);
  end function;

  -------------------------------------------------------------------

  function A2DSize(ya, xa :TVL) return TVL is
  begin
    return(ya*xa);
  end function;

  -------------------------------------------------------------------

  function A2DSize(ad :TA2D) return TVL is
  begin
    return(A2DSize(ad.y,ad.x));
  end function;

  -------------------------------------------------------------------

  function A2DIndex(ya, xa :TVL; y, x :TVI) return TVI is
  begin
    return(y*xa+x);
  end function;

  -------------------------------------------------------------------

  function A2DIndex(ad :TA2D; y, x :TVI) return TVI is
  begin
    return(A2DIndex(ad.y,ad.x,y,x));
  end function;


  -------------------------------------------------------------------
  -- two dimentions arrays emulation for TSLV
  -------------------------------------------------------------------

  function A2DInitArea(ya, xa :TVL; d :TSL) return TSLV is
    variable vec :TSLV(A2DSize(ya,xa)-1 downto VEC_INDEX_MIN);
  begin
    vec := (others => d);
    return(vec);
  end function;

  -------------------------------------------------------------------

  function A2DInitArea(ad :TA2D; d :TSL) return TSLV is
  begin
    return(A2DInitArea(ad.y,ad.x,d));
  end function;

  -------------------------------------------------------------------

  function A2DGetCell(v :TSLV; ya, xa :TVL; y, x :TVI) return TSL is
  begin
    return(SLVNorm(v)(A2DIndex(ya,xa,y,x)));
  end function;

  -------------------------------------------------------------------

  function A2DGetCell(v :TSLV; ad :TA2D; y, x :TVI) return TSL is
  begin
    return(A2DGetCell(v,ad.y,ad.x,y,x));
  end function;

  -------------------------------------------------------------------

  function A2DGetRange(v :TSLV; ya, xa :TVL; y, xh, xl :TVI) return TSLV is
  begin
    return(SLVNorm(v)(A2DIndex(ya,xa,y,xh) downto A2DIndex(ya,xa,y,xl)));
  end function;

  -------------------------------------------------------------------

  function A2DGetRange(v :TSLV; ad :TA2D; y, xh, xl :TVI) return TSLV is
  begin
    return(A2DGetRange(v,ad.y,ad.x,y,xh,xl));
  end function;

  -------------------------------------------------------------------

  function A2DGetRow(v :TSLV; ya, xa :TVL; y :TVI) return TSLV is
  begin
    return(A2DGetRange(v,ya,xa,y,xa-1,VEC_INDEX_MIN));
  end function;

  -------------------------------------------------------------------

  function A2DGetRow(v :TSLV; ad :TA2D; y :TVI) return TSLV is
  begin
    return(A2DGetRow(v,ad.y,ad.x,y));
  end function;

  -------------------------------------------------------------------

  function A2DGetLastRow(v :TSLV; ya, xa :TVL) return TSLV is
  begin
    return(A2DGetRow(v,ya,xa,ya-1));
  end function;

  -------------------------------------------------------------------

  function A2DGetLastRow(v :TSLV; ad :TA2D) return TSLV is
  begin
    return(A2DGetLastRow(v,ad.y,ad.x));
  end function;

  -------------------------------------------------------------------

  function A2DSetCell(v :TSLV; ya, xa :TVL; y, x :TVI; d :TSL) return TSLV is
    variable vec :TSLV(v'length-1 downto VEC_INDEX_MIN);
  begin
    vec := v;
    vec(A2DIndex(ya,xa,y,x)) := d;
    return(vec);
  end function;

  -------------------------------------------------------------------

  function A2DSetCell(v :TSLV; ad :TA2D; y, x :TVI; d :TSL) return TSLV is
  begin
    return(A2DSetCell(v,ad.y,ad.x,y,x,d));
  end function;

  -------------------------------------------------------------------

  function A2DSetRange(v :TSLV; ya, xa :TVL; y, xh, xl :TVI; d :TSLV) return TSLV is
    variable vec :TSLV(v'length-1 downto VEC_INDEX_MIN);
  begin
    vec := v;
    vec(A2DIndex(ya,xa,y,xh) downto A2DIndex(ya,xa,y,xl)) := d;
    return(vec);
  end function;

  -------------------------------------------------------------------

  function A2DSetRange(v :TSLV; ad :TA2D; y, xh, xl :TVI; d :TSLV) return TSLV is
  begin
    return(A2DSetRange(v,ad.y,ad.x,y,xh,xl,d));
  end function;

  -------------------------------------------------------------------

  function A2DSetRow(v :TSLV; ya, xa :TVL; y :TVI; d :TSLV) return TSLV is
  begin
    return(A2DSetRange(v,ya,xa,y,xa-1,VEC_INDEX_MIN,d));
  end function;

  -------------------------------------------------------------------

  function A2DSetRow(v :TSLV; ad :TA2D; y :TVI; d :TSLV) return TSLV is
   begin
    return(A2DSetRow(v,ad.y,ad.x,y,d));
  end function;

  -------------------------------------------------------------------

  function A2DFillRange(v :TSLV; ya, xa :TVL; y, xh, xl :TVI; d :TSL) return TSLV is
    variable vec :TSLV(v'length-1 downto VEC_INDEX_MIN);
  begin
    vec := v;
    for index in xh downto xl loop
      vec(index) := d;
    end loop;
    return(vec);
  end function;

  -------------------------------------------------------------------

  function A2DFillRange(v :TSLV; ad :TA2D; y, xh, xl :TVI; d :TSL) return TSLV is
  begin
    return(A2DFillRange(v,ad.y,ad.x,y,xh,xl,d));
  end function;

  -------------------------------------------------------------------

  function A2DFillRow(v :TSLV; ya, xa :TVL; y :TVI; d :TSL) return TSLV is
  begin
    return(A2DFillRange(v,ya,xa,y,xa-1,VEC_INDEX_MIN,d));
  end function;

  -------------------------------------------------------------------

  function A2DFillRow(v :TSLV; ad :TA2D; y :TVI; d :TSL) return TSLV is
  begin
    return(A2DFillRange(v,ad.y,ad.x,y,ad.x-1,VEC_INDEX_MIN,d));
  end function;


end std_logic_1164_ktp;

