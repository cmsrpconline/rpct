library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;

library lpm;
use lpm.lpm_components.all;

use work.std_logic_1164_ktp.all;
use work.vcomponent.all;
use work.LPMcomponent.all;
use work.LPMsynchro.all;
use work.LPMdiagnostics.all;
use work.tridaq.all;
use work.link.all;
use work.ldemux_iis.all;

entity ldemux is
  port(
    data1_in					:in TSLV(LINK_DATA_SIZE-1 downto 0);
    data1_out					:out TSLV(LDEMUX_OUTH_SIZE-1 downto 0);
    data2_in					:in TSLV(LINK_DATA_SIZE-1 downto 0);
    data2_out					:out TSLV(LDEMUX_OUTH_SIZE-1 downto 0);
    -- fast signal interface
    clock					:in    TSL;
    l1a						:in    TSL;
    pretrg0					:in    TSL;
    pretrg1					:in    TSL;
    bcn0					:in    TSL;
    -- internal bus interface
    ii_base					:in    TSLV(2 downto 1);
    ii_addr					:in    TSLV(II_ADDR_SIZE-1 downto 0);
    ii_data					:inout TSLV(II_DATA_SIZE-1 downto 0);
    ii_oper					:in    TSL;
    ii_write					:in    TSL;
    ii_save					:in    TSL;
    ii_int					:inout TSL;
    ii_reset					:in    TSL;
    -- diagnostics signal
    led						:out   TSLV(3 downto 0);
    conf_led					:out   TSL
  );
end ldemux;

architecture behavioural of ldemux is

  component decoder
    port(
      resetN					:in  TSL;
      clock					:in  TSL;
      part_data					:in  TPartData;
      part_code					:in  TPartCode;
      part_time					:in  TPartTime;
      end_data					:in  TSL;
      data_out					:out TChanOut;
      end_data_out				:out TSL
    );
  end component;

  constant IIPar				:TVII := TVIICreate(VIIItemDeclList,II_ADDR_SIZE,II_DATA_SIZE);
  signal   IIVecInt,IIVecExt,IIVecEna		:TSLV(VII(IIPar)'high downto VEC_INDEX_MIN);
  signal   LIIEnableSig				:TSL;
  signal   LIIAddrLocSig			:TSLV(II_ADDR_USER_SIZE-1 downto 0);
  signal   IIDataSig				:TSLV(II_DATA_SIZE-1 downto 0);
  signal   LIIDataExport			:TSLV(II_DATA_SIZE-1 downto 0);
  signal   LIIDataExportEnable			:TSL;
  --
  signal   L, H					:TSL;
  signal   Data1InReg				:TSLV(data1_in'range);
  signal   Data2InReg				:TSLV(data2_in'range);
  signal   DEC1_ChamAddrBaseSig			:TChanAddr;
  signal   DEC1_PartDataSig			:TPartData;
  signal   DEC1_PartCodeSig			:TPartCode;
  signal   DEC1_PartTimeSig			:TPartTime;
  signal   DEC1_EndDataSig			:TSL;
  signal   DEC1_DataOutSig			:TChanOut;
  signal   DEC1_EndDataOutSig			:TSL;
  signal   DEC2_ChamAddrBaseSig			:TChanAddr;
  signal   DEC2_PartDataSig			:TPartData;
  signal   DEC2_PartCodeSig			:TPartCode;
  signal   DEC2_PartTimeSig			:TPartTime;
  signal   DEC2_EndDataSig			:TSL;
  signal   DEC2_DataOutSig			:TChanOut;
  signal   DEC2_EndDataOutSig			:TSL;
  --
  signal   ProcReqSig				:TSL;
  signal   ProcRateAckSig			:TSL;
  signal   ProcAckSig				:TSL;
  --
  signal   HistTimerLimitSig			:TSLV(HIST_TIME_SIZE-1 downto 0);
  signal   HistTimerCountSig			:TSLV(HIST_TIME_SIZE-1 downto 0);
  signal   HistTimerStartSig			:TSL;
  signal   HistTimerStopSig			:TSL;
  signal   HistTimerClkEnaSig			:TSL;
  signal   TimerEngineClkEnaSig			:TSL;
  signal   DemuxDataSig				:TSLV(RATE_MEM_DATA_SIZE-1 downto 0);
  signal   RateMemWrSig				:TSL;
  signal   RateMemDataOutSig			:TSLV(II_DATA_SIZE-1 downto 0);
  --
  signal   L1AReg				:TSL;
  signal   BCN0Reg				:TSL;
  signal   Pretrg0Reg				:TSL;
  signal   Pretrg1Reg				:TSL;

begin	  

  L <= '0'; H <= '1';

  DEC1_PartTimeSig <= TSLVResize(Data1InReg, GL(GL_ITEM_TIME));
  DEC1_PartCodeSig <= TSLVResize(Data1InReg, GL(GL_ITEM_PARTITION));
  DEC1_PartDataSig <= TSLVResize(Data1InReg, GL(GL_ITEM_DATA));
  DEC1_EndDataSig  <=  TSLconv(Data1InReg, GL(GL_ITEM_END_DATA))
                   and TSLconv(Data1InReg, GL(GL_ITEM_HALF_PART));
  decod1: decoder
    port map(
      resetN       =>ii_reset,
      clock        =>clock,
      part_data    =>DEC1_PartDataSig,
      part_code    =>DEC1_PartCodeSig,
      part_time    =>DEC1_PartTimeSig,
      end_data     =>DEC1_EndDataSig,
      data_out     =>DEC1_DataOutSig,
      end_data_out =>DEC1_EndDataOutSig
    );

  data1_out <=   SLVDemux(DEC1_DataOutSig(LDEMUX_OUT_SIZE-1 downto 0),2,0) when clock='0' else
                 SLVDemux(DEC1_DataOutSig(LDEMUX_OUT_SIZE-1 downto 0),2,1);

  DEC2_PartTimeSig <= TSLVResize(Data2InReg, GL(GL_ITEM_TIME));
  DEC2_PartCodeSig <= TSLVResize(Data2InReg, GL(GL_ITEM_PARTITION));
  DEC2_PartDataSig <= TSLVResize(Data2InReg, GL(GL_ITEM_DATA));
  DEC2_EndDataSig  <=  TSLconv(Data2InReg, GL(GL_ITEM_END_DATA))
                   and TSLconv(Data2InReg, GL(GL_ITEM_HALF_PART));
  decod2: decoder
    port map(
      resetN       =>ii_reset,
      clock        =>clock,
      part_data    =>DEC2_PartDataSig,
      part_code    =>DEC2_PartCodeSig,
      part_time    =>DEC2_PartTimeSig,
      end_data     =>DEC2_EndDataSig,
      data_out     =>DEC2_DataOutSig,
      end_data_out =>DEC2_EndDataOutSig
    );

  data2_out <=   SLVDemux(DEC2_DataOutSig(LDEMUX_OUT_SIZE-1 downto 0),2,0) when clock='0' else
                 SLVDemux(DEC2_DataOutSig(LDEMUX_OUT_SIZE-1 downto 0),2,1);

  --
  -- timer engine
  --
  ProcReqSig <= TSLconv(IIConnGetBitsData(IIVecInt,IIPar,BITS_STATUS_PROC_REQ,0));
  ProcAckSig <= ProcRateAckSig;

  HistTimerClkEnaSig <= TSLconv(IIConnGetBitsData(IIVecInt,IIPar,BITS_STATUS_RATE_ENA,0));
  HistTimerStartSig  <= TSLconv(IIConnGetBitsData(IIVecInt,IIPar,BITS_STATUS_RATE_START,0));
  HistTimerLimitSig  <= IIConnGetWordData(IIVecInt,IIPar,WORD_HIST_TIME_LIMIT,0);

  HistTimer: LPM_TIMER_ENGINE 
    generic map (
      LPM_TIMER_SIZE	=> HIST_TIME_SIZE
    )
    port map (
      resetN		=> ii_reset,
      clock		=> clock,
      clk_ena_in	=> HistTimerClkEnaSig,
      clk_ena_out	=> TimerEngineClkEnaSig,
      start		=> HistTimerStartSig,
      stop		=> HistTimerStopSig,
      limit		=> HistTimerLimitSig,
      count		=> HistTimerCountSig
    );

  --
  -- rates
  --
  DemuxDataSig    <= DEC1_DataOutSig(RATE_MEM_DATA_SIZE-1 downto 0) when IIConnGetBitsData(IIVecInt,IIPar,BITS_STATUS_CHAN_SEL,0)="0" else
                     DEC2_DataOutSig(RATE_MEM_DATA_SIZE-1 downto 0);

  RateMemWrSig <= IIConnGetAreaWrite(IIVecEna,IIPar,AREA_MEM_RATE,ii_save);
  RateMem: RATE
    generic map(
      LPM_DATA_WIDTH	=> RATE_MEM_DATA_SIZE,
      LPM_COUNT_WIDTH	=> RATE_MEM_COUNT_SIZE,
      LPM_MDATA_WIDTH	=> II_DATA_SIZE
    )
    port map(
      resetN		=> ii_reset,
      clock		=> clock,
      data_in		=> DemuxDataSig,
      clk_ena		=> TimerEngineClkEnaSig,
      sim_loop		=> L,
      proc_req		=> ProcReqSig,
      proc_ack		=> ProcRateAckSig,
      memory_address_in	=> ii_addr(RATE_MEM_ADDR_SIZE-1 downto 0),
      memory_data_in	=> ii_data,
      memory_data_out	=> RateMemDataOutSig,
      memory_wr		=> RateMemWrSig
  );

  --
  -- fast interface
  --
  process(ii_reset, clock)
  begin
    if(ii_reset='0') then
      Data1InReg  <= (others => '0');
      Data2InReg  <= (others => '0');
      L1AReg     <= '0';
      BCN0Reg    <= '0';
      Pretrg0Reg <= '0';
      Pretrg1Reg <= '0';
    elsif(clock'event and clock='1') then
      if (TSLVResize(Data1InReg, GL(GL_ITEM_CHAMBER))=DEC1_ChamAddrBaseSig) then
        Data1InReg  <= data1_in;
      else
        Data1InReg <= (others =>'1');
      end if;
      if (TSLVResize(Data2InReg, GL(GL_ITEM_CHAMBER))=DEC2_ChamAddrBaseSig) then
        Data2InReg  <= data2_in;
      else
        Data2InReg <= (others =>'1');
      end if;
      L1AReg     <= l1a;
      BCN0Reg    <= bcn0;
      Pretrg0Reg <= pretrg0;
      Pretrg1Reg <= pretrg1;
    end if;
  end process;

  --
  -- internal interface
  --
  LIIEnableSig <= not(TSLconv(  ii_addr(II_ADDR_SIZE-1 downto II_ADDR_USER_SIZE)
                          =
			  (II_LDEMUX0_BASE_ADDR+(2*TNconv(II_base))+1)
			and
			  ii_oper='0'
		        ));
  LIIAddrLocSig <= ii_addr(II_ADDR_USER_SIZE-1 downto 0);
  --
  process(ii_reset, ii_save)
  begin
    if(ii_reset='0') then
      IIVecInt <= IIReset(IIVecInt,IIPar);
    elsif(ii_save'event and ii_save='1') then
      if(LIIEnableSig='0') then
        IIVecInt <= IISaveAccess(IIVecInt,IIPar,LIIAddrLocSig,IIDataSig);
      end if;
    end if;
  end process;
  --
  IIVecEna <= IIWriteEnable(IIVecInt,IIPar,LIIEnableSig,ii_write,LIIAddrLocSig,IIDataSig);
  IIVecExt <= (  IIWriteAccess(IIVecInt,IIPar,LIIEnableSig,ii_write,LIIAddrLocSig,IIDataSig)
              or IIConnPutWordData(IIVecInt,IIPar,WORD_IDENTIFIER,0,TSLVconv(LDEMUX_IDENTIFIER,II_DATA_SIZE))
              or IIConnPutWordData(IIVecInt,IIPar,WORD_VERSION,0,TSLVconv(LDEMUX_VERSION,II_DATA_SIZE))
              or IIConnPutBitsData(IIVecInt,IIPar,BITS_STATUS_DEC1_EOD,0,TSLVconv(DEC1_EndDataOutSig))
              or IIConnPutBitsData(IIVecInt,IIPar,BITS_STATUS_DEC2_EOD,0,TSLVconv(DEC2_EndDataOutSig))

              or IIConnPutAreaData(IIVecInt,IIPar,AREA_MEM_RATE, RateMemDataOutSig)
	      );
  LIIDataExport <= IIReadAccess(IIVecExt,IIPar,LIIEnableSig,ii_write,LIIAddrLocSig);
  --
  LIIDataExportEnable <= not(LIIEnableSig) and ii_write;
  CIIData: ALTERA_BUSTRI
    generic map (LPM_WIDTH => II_DATA_SIZE)
    port map    (data_in   => LIIDataExport,
                 data_out  => IIDataSig,
                 data_tri  => ii_data,
                 ena       => LIIDataExportEnable);
  CIIIrq: ALTERA_TRI
    port map    (data_in   => L,
                 data_tri  =>  ii_int,
		 ena       => Pretrg0Reg);
  --
  -- diagnostic interface
  --
  led(0) <= II_oper and II_reset;
  led(1) <= LIIEnableSig and II_reset;
  led(2) <= II_write and II_reset;
  led(3) <= II_save and II_reset;

  conf_led <= '0';

end behavioural;
