library ieee;
use ieee.std_logic_1164.all;
use work.std_logic_1164_ktp.all;

package pac_packet6 is

  -- input types
  subtype  Tms1	is std_logic_vector(31 downto 0); 
  subtype  Tms2	is std_logic_vector(7 downto 0); 
  subtype  Tms3	is std_logic_vector(23 downto 0); 
  subtype  Tms4	is std_logic_vector(23 downto 0); 
  subtype  Tms5	is std_logic_vector(31 downto 0); 
  subtype  Tms6	is std_logic_vector(31 downto 0); 
  
  subtype  Tms1h is std_logic_vector(Tms1'Length/2-1 downto 0); 
  subtype  Tms2h is std_logic_vector(Tms2'Length/2-1 downto 0); 
  subtype  Tms3h is std_logic_vector(Tms3'Length/2-1 downto 0); 
  subtype  Tms4h is std_logic_vector(Tms4'Length/2-1 downto 0); 
  subtype  Tms5h is std_logic_vector(Tms5'Length/2-1 downto 0); 
  subtype  Tms6h is std_logic_vector(Tms6'Length/2-1 downto 0);
  
  -- output types
  subtype  TCode			is std_logic_vector(31 downto 0);
  subtype  TCodeVal			is natural range TCode'range;
  subtype  TQuality			is std_logic_vector(3 downto 0);
  subtype  TQualityVal		is natural range TQuality'range;
  subtype  TBlockType		is std_logic_vector(7 downto 0);
  subtype  TBlockTypeVal	is natural range TBlockType'range;

  -- pattern table format
  type     TPattVector		is record
  				  blk_type	:TBlockTypeVal;
  				  sign		:integer;
  				  LayerA	:integer;
  				  LayerB	:integer;
  				  LayerC	:integer;
  				  LayerD	:integer;
  				  LayerE	:integer;
  				  code		:TCodeVal;
  				end record;
  type     TPattTable	is array (natural range <>) of TPattVector;

end pac_packet6;
