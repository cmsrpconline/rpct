/* test.dds 1: */
/* test.dds 2: */
/* test.dds 3: */
/* test.dds 4: */
/* test.dds 5: */constant SYN_IDENTIFIER : TS := "SC"; -- 
/* test.dds 6: */constant SYN_VERSION : THV := "0100"; -- 
/* test.dds 7: */constant PAGE_REGISTERS : TN := VEC_INDEX_MIN; -- 
/* test.dds 8: */constant PAGE_DAQ_MEM_DELAY : TN := PAGE_REGISTERS+1; -- 
/* test.dds 9: */constant PAGE_DAQ_MEM_DATA : TN := PAGE_DAQ_MEM_DELAY+1; -- 
/* test.dds 10: */constant PAGE_DAQ_MEM_CNT : TN := PAGE_DAQ_MEM_DATA+1; -- 
/* test.dds 11: */constant WORD_IDENTIFIER : TN := VEC_INDEX_MIN+10; -- 
/* test.dds 12: */constant WORD_VERSION : TN := WORD_IDENTIFIER+1; -- 
/* test.dds 13: */constant VECT_STATUS : TN := WORD_VERSION+1; -- 
/* test.dds 14: */constant VECT_SYN0 : TN := VECT_STATUS+1; -- 
/* test.dds 15: */
/* test.dds 16: */
/* test.dds 17: */