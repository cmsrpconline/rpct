library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;

library lpm;
use lpm.lpm_components.all;

use work.std_logic_1164_ktp.all;
use work.vcomponent.all;
use work.LPMcomponent.all;
use work.tridaq.all;
use work.link.all;

  --
  -- internal interface
  --




















constant   SYN_IDENTIFIER         :   TS          :=   "SC"; --    
constant   SYN_VERSION            :   THV         :=   "0100"; --    
constant   PAGE_REGISTERS                                 :   TN          := 6; --    
constant   PAGE_DAQ_MEM_DELAY                             :   TN          := 7; --    
constant   PAGE_DAQ_MEM_DATA                              :   TN          := 8; --    
constant   PAGE_DAQ_MEM_CNT                               :   TN          := 9; --    
constant   WORD_IDENTIFIER                                :   TN          := 10; --    
constant   WORD_VERSION	                               :   TN          := 11; --    
constant   VECT_STATUS	                               :   TN          := 12; --    
constant   VECT_SYN0	                               :   TN          := 13; --    


 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 




  --
  constant VIIItemDeclList	:TVIIItemDeclList :=(  


















 




 
 
 
 
 
 
 
 
 
 


(    VII_PAGE  ,   PAGE_REGISTERS     ,                              0  ,                     0    ,       PAGE_REGISTERS         ,   VII_NOACCESS  ,   VII_NOACCESS   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ), 
(    VII_WORD  ,   WORD_IDENTIFIER     ,                  II_DATA_SIZE  ,                     1    ,       PAGE_REGISTERS         ,   VII_NOACCESS  ,   VII_EXTERNAL   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ), 
(    VII_WORD  ,   WORD_VERSION     ,                     II_DATA_SIZE  ,                     1    ,       PAGE_REGISTERS         ,   VII_NOACCESS  ,   VII_EXTERNAL   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ), 
(    VII_VECT  ,   VECT_STATUS     ,                                 0  ,                     0    ,       PAGE_REGISTERS         ,   VII_NOACCESS  ,   VII_NOACCESS   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ), 
(    VII_BITS  ,   BITS_STATUS_CHAN_SEL     ,             TVLcreate(1)  ,                     1    ,          VECT_STATUS         ,     VII_ACCESS  ,   VII_INTERNAL   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ), 
(    VII_BITS  ,   BITS_STATUS_PROC_REQ     ,                        1  ,                     1    ,          VECT_STATUS         ,     VII_ACCESS  ,   VII_INTERNAL   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ), 
(    VII_BITS  ,   BITS_STATUS_PROC_ACK     ,                        1  ,                     1    ,          VECT_STATUS         ,   VII_NOACCESS  ,   VII_EXTERNAL   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ), 
(    VII_VECT  ,   VECT_SYN0     ,                                   0  ,                     0    ,       PAGE_REGISTERS         ,   VII_NOACCESS  ,   VII_NOACCESS   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ), 
(    VII_BITS  ,   BITS_SYN0_CLK_QUALITY     ,                       1  ,                     1    ,            VECT_SYN0         ,   VII_NOACCESS  ,   VII_EXTERNAL   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ), 
(    VII_BITS  ,   BITS_SYN0_PHASE_QUALITY     ,                     1  ,                     1    ,            VECT_SYN0         ,   VII_NOACCESS  ,   VII_EXTERNAL   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ), 
(    VII_BITS  ,   BITS_SYN0_DATA_PARITY     ,                       1  ,                     1    ,            VECT_SYN0         ,   VII_NOACCESS  ,   VII_EXTERNAL   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ), 
(    VII_BITS  ,   BITS_SYN0_BCN0_SYNCH     ,                        1  ,                     1    ,            VECT_SYN0         ,   VII_NOACCESS  ,   VII_EXTERNAL   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ), 
(    VII_BITS  ,   BITS_SYN0_BCN_SYNCH     ,                         1  ,                     1    ,            VECT_SYN0         ,   VII_NOACCESS  ,   VII_EXTERNAL   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ), 
(    VII_BITS  ,   BITS_SYN0_LATENCY     ,               SYN_PIPE_SIZE  ,                     1    ,            VECT_SYN0         ,   VII_NOACCESS  ,   VII_EXTERNAL   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ), 
(    VII_VECT  ,   VECT_SYN1     ,                                   0  ,                     0    ,       PAGE_REGISTERS         ,   VII_NOACCESS  ,   VII_NOACCESS   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ), 
(    VII_BITS  ,   BITS_SYN1_CLK_QUALITY     ,                       1  ,                     1    ,            VECT_SYN1         ,   VII_NOACCESS  ,   VII_EXTERNAL   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ), 
(    VII_BITS  ,   BITS_SYN1_PHASE_QUALITY     ,                     1  ,                     1    ,            VECT_SYN1         ,   VII_NOACCESS  ,   VII_EXTERNAL   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ), 
(    VII_BITS  ,   BITS_SYN1_DATA_PARITY     ,                       1  ,                     1    ,            VECT_SYN1         ,   VII_NOACCESS  ,   VII_EXTERNAL   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ), 
(    VII_BITS  ,   BITS_SYN1_BCN0_SYNCH     ,                        1  ,                     1    ,            VECT_SYN1         ,   VII_NOACCESS  ,   VII_EXTERNAL   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ), 
(    VII_BITS  ,   BITS_SYN1_BCN_SYNCH     ,                         1  ,                     1    ,            VECT_SYN1         ,   VII_NOACCESS  ,   VII_EXTERNAL   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ), 
(    VII_BITS  ,   BITS_SYN1_LATENCY     ,               SYN_PIPE_SIZE  ,                     1    ,            VECT_SYN1         ,   VII_NOACCESS  ,   VII_EXTERNAL   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ), 
(    VII_WORD  ,   WORD_DAQ_DELAY     ,            DELAY_PIPELINE_SIZE  ,                     1    ,       PAGE_REGISTERS         ,     VII_ACCESS  ,   VII_INTERNAL   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ), 
(    VII_PAGE  ,   PAGE_DAQ_MEM_DELAY     ,                          0  ,                     0    ,   PAGE_DAQ_MEM_DELAY         ,   VII_NOACCESS  ,   VII_NOACCESS   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ), 
(    VII_AREA  ,   AREA_DAQ_MEM_DELAY     ,        DPM_DELAY_DATA_SIZE  ,   DPM_DELAY_ADDR_SIZE    ,   PAGE_DAQ_MEM_DELAY         ,   VII_NOACCESS  ,   VII_EXTERNAL   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ), 
(    VII_PAGE  ,   PAGE_DAQ_MEM_DATA     ,                           0  ,                     0    ,   PAGE_DAQ_MEM_DELAY         ,   VII_NOACCESS  ,   VII_NOACCESS   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ), 
(    VII_AREA  ,   AREA_DAQ_MEM_DATA     ,          DPM_DATA_DATA_SIZE  ,    DPM_DATA_ADDR_SIZE    ,   PAGE_DAQ_MEM_DELAY         ,   VII_NOACCESS  ,   VII_EXTERNAL   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ), 
(    VII_PAGE  ,   PAGE_DAQ_MEM_CNT     ,                            0  ,                     0    ,   PAGE_DAQ_MEM_DELAY         ,   VII_NOACCESS  ,   VII_NOACCESS   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ), 
(    VII_AREA  ,   AREA_DAQ_MEM_CNT     ,            DPM_CNT_DATA_SIZE  ,     DPM_CNT_ADDR_SIZE    ,   PAGE_DAQ_MEM_DELAY         ,   VII_NOACCESS  ,   VII_EXTERNAL   ,   VIINameConv(" ")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")     ) 



);


