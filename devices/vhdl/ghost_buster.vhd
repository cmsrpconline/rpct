library ieee;
use ieee.std_logic_1164.all;
--use ieee.std_logic_unsigned.all;

package local_package is
-- no neighbours codes in wedge solution
  constant CODE_SIZE :Positive :=7;
  constant IN_NUM :Positive :=12;
  constant IN_NUM_tb2001 :Positive :=5;
  constant ADDR_SIZE :Positive :=4;	-- sign of muon
  constant GB_NUM :Positive :=6;
  constant SOR_NUM :Positive :=4;
  subtype code_bus is std_logic_vector(CODE_SIZE-1 downto 0);  
  subtype address_bus is std_logic_vector(ADDR_SIZE-1 downto 0);  
  type in_code_bus is array(0 to IN_NUM-1) of code_bus;  
  type in_sign_bus is array(0 to IN_NUM-1) of std_logic;  
  type in_code_bus_tb2001 is array(0 to IN_NUM_tb2001-1) of code_bus;  
  type in_sign_bus_tb2001 is array(0 to IN_NUM_tb2001-1) of std_logic;  
  type gb_code_bus is array(0 to GB_NUM-1) of code_bus;
  type gb_addr_bus is array(0 to GB_NUM-1) of std_logic;
  type gb_sign_bus is array(0 to GB_NUM-1) of std_logic;
  type gb_addr_out_bus is array(0 to GB_NUM-1) of address_bus;
  type gb_addr_out_eta_bus is array(0 to GB_NUM-1) of std_logic;
  type s_code_bus is array(0 to SOR_NUM-1) of code_bus;
  type s_addr_bus is array(0 to SOR_NUM-1) of address_bus;
  type s_addr_eta_bus is array(0 to SOR_NUM-1) of std_logic;
  type s_sign_bus is array(0 to SOR_NUM-1) of std_logic;
  type rank is array(0 to 1) of code_bus; -- rank(0) = rank0, rank(1) = rankB,
  type rank_set is array(0 to 1) of rank; -- rank_set(0) = (rank0,rankB) for row a,
                                          -- rank_set(1) = (rank0,rankB) for row b,
end local_package;
--
library ieee;
use ieee.std_logic_1164.all;
--use ieee.std_logic_unsigned.all;
use work.local_package.all;
entity gb_tb_reg IS
  port(	 clock : in std_logic;
	    resetN :in	std_logic;
	    in_code_a : in in_code_bus;
	    in_sign_a : in in_sign_bus;  --  will be treated like address bits
		out_code : out s_code_bus;
	    out_addr : out s_addr_bus;
	    out_sign : out s_sign_bus);
end gb_tb_reg;
--
library ieee;
use ieee.std_logic_1164.all;
--use ieee.std_logic_unsigned.all;
use work.local_package.all;
architecture behavioural of gb_tb_reg is
-- rank opozniony aby lepsze latencje
  signal in_code_a_reg,in_code_b_reg: in_code_bus;
  signal in_sign_a_reg,in_sign_b_reg: in_sign_bus;
  signal gb_code,gb_code_a_temp,gb_code_b_temp,gb_code_out,gb_code_out_reg,s_code : gb_code_bus;
  signal gb_addr,gb_addr_a_temp,gb_addr_b_temp : gb_addr_bus;
  signal gb_addr_eta,gb_addr_eta_reg,s_addr_eta : gb_addr_out_eta_bus;
  signal gb_sign,gb_sign_reg,gb_sign_a_temp,gb_sign_b_temp,s_sign : gb_sign_bus;
  signal gb_addr_out,gb_addr_out_reg,s_addr : gb_addr_out_bus;
  signal s_addr_eta_temp : s_addr_eta_bus;
  signal s_code_temp : s_code_bus;
  signal s_addr_temp : s_addr_bus;
  signal s_sign_temp : s_sign_bus;
 --
 begin 
--
-- KP!!!!
--r1:process(clock)
-- input registers
--  begin
--    if (clock'event and clock='1') then
--		if resetN = '1' then
--		  in_code_a_reg <= in_code_a;
--		  in_sign_a_reg <= in_sign_a;			
--		else 
--		  in_code_a_reg <= (others => (others => '0'));
--		  in_sign_a_reg <= (others => '0');			
--		end if;	
--    end if;
--  end process r1;
-- 
-- KP!!!!

  in_code_a_reg <= in_code_a; --KP!!!
  in_sign_a_reg <= in_sign_a; --KP!!!

  gb_phi_a:
  process(in_code_a_reg,in_sign_a_reg)
-- phi ghost busting for row a, no registers here
begin
-- first - different - no left neighbour
	if ((in_code_a_reg(0)>in_code_a_reg(1)) or
       ((in_code_a_reg(0)=in_code_a_reg(1)) and  
		  (in_code_a_reg(1)>in_code_a_reg(2)))) then
	 gb_code_a_temp(0) <= in_code_a_reg(0);	  
	 gb_addr_a_temp(0) <= '0';
	 gb_sign_a_temp(0) <= in_sign_a_reg(0);
	elsif (((in_code_a_reg(1)>in_code_a_reg(2)) and 
			  (in_code_a_reg(1)>in_code_a_reg(0))) or
          ((in_code_a_reg(1)=in_code_a_reg(2)) and 
          	(in_code_a_reg(1)>=in_code_a_reg(0)) and 
		  	(in_code_a_reg(2)>in_code_a_reg(3)))) then 
	 gb_code_a_temp(0) <= in_code_a_reg(1);
	 gb_addr_a_temp(0) <= '1';
	 gb_sign_a_temp(0) <= in_sign_a_reg(1);
	else
	 gb_code_a_temp(0) <= (others => '0');
	 gb_addr_a_temp(0) <= '0';	
	 gb_sign_a_temp(0) <= '0';
	end if;	 
-- 
	for j in 1 to GB_NUM-2 loop
          if (((in_code_a_reg(2*j)>in_code_a_reg(2*j+1)) and 
			  (in_code_a_reg(2*j)>in_code_a_reg(2*j-1))) or
              ((in_code_a_reg(2*j)=in_code_a_reg(2*j+1)) and 
              (in_code_a_reg(2*j)>=in_code_a_reg(2*j-1)) and 
			  (in_code_a_reg(2*j+1)>in_code_a_reg(2*j+2)))) then		
				 gb_code_a_temp(j) <= in_code_a_reg(2*j);	  
				 gb_addr_a_temp(j) <= '0';
				 gb_sign_a_temp(j) <= in_sign_a_reg(2*j);
          elsif (((in_code_a_reg(2*j+1)>in_code_a_reg(2*j+2)) and 
			  (in_code_a_reg(2*j+1)>in_code_a_reg(2*j))) or
              ((in_code_a_reg(2*j+1)=in_code_a_reg(2*j+2)) and 
              (in_code_a_reg(2*j+1)>=in_code_a_reg(2*j)) and 
			  (in_code_a_reg(2*j+2)>in_code_a_reg(2*j+3)))) then
				 gb_code_a_temp(j) <= in_code_a_reg(2*j+1);
				 gb_addr_a_temp(j) <= '1';
				 gb_sign_a_temp(j) <= in_sign_a_reg(2*j+1);
	      else 
				 gb_code_a_temp(j) <= (others => '0');
				 gb_addr_a_temp(j) <= '0';	
				 gb_sign_a_temp(j) <= '0';
		  end if;
      end loop;  
-- last - different - no right neighbours
	if (((in_code_a_reg(2*(GB_NUM-1))>in_code_a_reg(2*(GB_NUM-1)+1)) and 
			  (in_code_a_reg(2*(GB_NUM-1))>in_code_a_reg(2*(GB_NUM-1)-1))) or
              ((in_code_a_reg(2*(GB_NUM-1))=in_code_a_reg(2*(GB_NUM-1)+1)) and 
              (in_code_a_reg(2*(GB_NUM-1))>=in_code_a_reg(2*GB_NUM-1)))) then
				 gb_code_a_temp(GB_NUM-1) <= in_code_a_reg(2*(GB_NUM-1));	  
				 gb_addr_a_temp(GB_NUM-1) <= '0';
				 gb_sign_a_temp(GB_NUM-1) <= in_sign_a_reg(2*(GB_NUM-1));
	elsif (((in_code_a_reg(2*(GB_NUM-1)+1)>in_code_a_reg(2*(GB_NUM-1)))) or
              ((in_code_a_reg(2*(GB_NUM-1)+1)>=in_code_a_reg(2*(GB_NUM-1))))) then 
				 gb_code_a_temp(GB_NUM-1) <= in_code_a_reg(2*(GB_NUM-1)+1);
				 gb_addr_a_temp(GB_NUM-1) <= '1';
				 gb_sign_a_temp(GB_NUM-1) <= in_sign_a_reg(2*(GB_NUM-1)+1);
	else
				 gb_code_a_temp(GB_NUM-1) <= (others => '0');
				 gb_addr_a_temp(GB_NUM-1) <= '0';	
				 gb_sign_a_temp(GB_NUM-1) <= '0';
	end if;
  end process gb_phi_a;
--  

	gb_eta: for j in 0 to GB_NUM-1 generate	 
		gb_code(j) <= gb_code_a_temp(j);
		gb_addr(j) <= gb_addr_a_temp(j);
		gb_sign(j) <= gb_sign_a_temp(j);
    end generate;  
 -- addresses for all 12 codes, LSB comes from gb selection	
		gb_code_out <= gb_code;
		gb_addr_out(5)(3 downto 1) <= "101";
		gb_addr_out(4)(3 downto 1) <= "100";
		gb_addr_out(3)(3 downto 1) <= "011";
		gb_addr_out(2)(3 downto 1) <= "010";
		gb_addr_out(1)(3 downto 1) <= "001";
		gb_addr_out(0)(3 downto 1) <= "000";
		gb_addr_out(5)(0) <= gb_addr(5);
		gb_addr_out(4)(0) <= gb_addr(4);
		gb_addr_out(3)(0) <= gb_addr(3);
		gb_addr_out(2)(0) <= gb_addr(2);
		gb_addr_out(1)(0) <= gb_addr(1);
		gb_addr_out(0)(0) <= gb_addr(0);
r2_1:process(clock)
-- intermediate registers
  begin
    if (clock'event and clock='1') then
		if resetN = '1' then 
			gb_code_out_reg	<= gb_code_out;
			gb_addr_out_reg	<= gb_addr_out; 	  
			gb_sign_reg	<= gb_sign; 
		else 
			gb_code_out_reg <= (others => (others => '0'));
			gb_addr_out_reg <= (others => (others => '0'));
			gb_sign_reg <= (others => '0');			
		end if;	
    end if;
  end process r2_1;
-- sorting 6 -> 4
  ss:process(gb_code_out_reg,gb_addr_out_reg,gb_sign_reg)
    variable E_code_sum_var  :natural range 0 to GB_NUM-1;
	begin
      for row in 0 to GB_NUM-1 loop
        E_code_sum_var  := 0;
        for col in 0 to GB_NUM-1 loop
          if( (gb_code_out_reg(row)>gb_code_out_reg(col))
            or ((gb_code_out_reg(row)=gb_code_out_reg(col)) and (col>row))) then
            E_code_sum_var := E_code_sum_var+1;
          end if;
        end loop;
        s_code(E_code_sum_var) <= gb_code_out_reg(row);
        s_addr(E_code_sum_var) <= gb_addr_out_reg(row);
        s_sign(E_code_sum_var) <= gb_sign_reg(row);
      end loop;
	end process ss; 
-- only 4 selected
  sel: process(s_code,s_sign,s_addr,s_addr_eta)
	begin
	    for i in 0 to SOR_NUM-1 loop
	      s_code_temp(i) <= s_code(GB_NUM-1-i);
	      s_sign_temp(i) <= s_sign(GB_NUM-1-i);
		  if (s_code(GB_NUM-1-i) = "0000000") then			  
		      s_addr_temp(i) <= (others => '0'); 
		      s_addr_eta_temp(i) <= '0'; 
		  else
		      s_addr_temp(i) <= s_addr(GB_NUM-1-i); 
		      s_addr_eta_temp(i) <= s_addr_eta(GB_NUM-1-i); 
		  end if;
	    end loop;
	end process sel;
-- output registers
  r3: process(clock)
	begin
		if (CLOCK'event and CLOCK='1') then	 
			out_code <= s_code_temp;
			out_addr <= s_addr_temp;
			out_sign <= s_sign_temp;
		end if;
	end process r3;
end behavioural;

--********************************************************************
--* Gb and Sorter for Wedge type trigger Board - gb_tb_half_2001.vhd *
--* By Maciek Kudla                                                  *
--* 5 inputs-> 4 outputs    (code, sign, phi and eta address)        *
--* based on design (component)                - gb_tb_reg.vhd       *
--* 12 inputs-> 4 outputs (code, sign, phi and eta address)          *
--* Test Bench                                 - gb_tb_2001_tb.vhd   *
--* Date 2001.05.02                                                  *
--********************************************************************

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;

library lpm;
use lpm.lpm_components.all;

use work.std_logic_1164_ktp.all;
use work.vcomponent.all;
use work.LPMcomponent.all;
use work.LPMDiagnostics.all;
use work.tridaq.all;

use work.local_package.all;

entity ghost_buster is
  port(
    in_code					:in    in_code_bus_tb2001;
    in_signs					:in    in_sign_bus_tb2001;
    out_code					:out   s_code_bus;
    out_addr					:out   s_addr_bus;
    out_sign					:out   s_sign_bus;
    -- fast signal interface
    clock					:in    TSL;
    l1a						:in    TSL;
    pretrg0					:in    TSL;
    pretrg1					:in    TSL;
    bcn0					:in    TSL;
    local_trg					:out   TSL;
    -- internal bus interface
    ii_addr					:in    TSLV(II_ADDR_SIZE-1 downto 0);
    ii_data					:inout TSLV(II_DATA_SIZE-1 downto 0);
    ii_oper					:in    TSL;
    ii_write					:in    TSL;
    ii_save					:in    TSL;
    ii_int					:inout TSL;
    ii_reset					:in    TSL;
    -- diagnostics signal
    led						:out   TSLV(3 downto 0);
    conf_led					:out   TSL
  );
end ghost_buster;

architecture behavioural of ghost_buster is

  component gb_tb_reg
    port(
      clock			:in  TSL;
      resetN			:in  TSL;
      in_code_a			:in  in_code_bus;
      in_sign_a			:in  in_sign_bus;
      out_code			:out s_code_bus;
      out_addr			:out s_addr_bus;
      out_sign			:out s_sign_bus);
  end component;
  --
  -- internal interface
  --
  constant GB_IDENTIFIER			:TS := "GB";
  constant GB_VERSION				:THV := "1000";
  --
  constant PAGE_REGISTERS			:TN := VEC_INDEX_MIN;
  constant PAGE_MEM_HIST			:TN := PAGE_REGISTERS+1;
  --
  constant WORD_IDENTIFIER			:TN := VEC_INDEX_MIN+10;
  constant WORD_VERSION				:TN := WORD_IDENTIFIER+1;
  constant VECT_STATUS				:TN := WORD_VERSION+1;
  constant AREA_MEM_HIST			:TN := VECT_STATUS+1;
  -- bit slaces in WORD_STATUS
  constant BITS_STATUS_CHAN_ENA			:TN := VEC_INDEX_MIN+100;
  constant BITS_STATUS_HIST_ENA			:TN := BITS_STATUS_CHAN_ENA+1;
  constant BITS_STATUS_PROC_REQ			:TN := BITS_STATUS_HIST_ENA+1;
  constant BITS_STATUS_PROC_ACK			:TN := BITS_STATUS_PROC_REQ+1;
  --
  constant VIIItemDeclList	:TVIIItemDeclList :=(
  -- item type,        item ID,                    width,                num,       parent ID,   write type,    read type,             name,          type,          description
  (   VII_PAGE, PAGE_REGISTERS,                        0,                   0, PAGE_REGISTERS, VII_WNOACCESS, VII_RNOACCESS, VIINameConv("a"), VII_FUN_UNDEF, VIIDescrConv("ele")),
   (  VII_WORD, WORD_IDENTIFIER,            II_DATA_SIZE,                   1, PAGE_REGISTERS, VII_WNOACCESS, VII_REXTERNAL, VIINameConv("b"), VII_FUN_UNDEF, VIIDescrConv("mele")),
   (  VII_WORD, WORD_VERSION,               II_DATA_SIZE,                   1, PAGE_REGISTERS, VII_WNOACCESS, VII_REXTERNAL, VIINameConv("c"), VII_FUN_UNDEF, VIIDescrConv("dudki")),
   (  VII_VECT, VECT_STATUS,                           0,                   0, PAGE_REGISTERS, VII_WNOACCESS, VII_RNOACCESS, VIINameConv("d"), VII_FUN_UNDEF, VIIDescrConv("gospodarz")),
    ( VII_BITS, BITS_STATUS_CHAN_ENA,                  1,       IN_NUM_tb2001,    VECT_STATUS,   VII_WACCESS, VII_RINTERNAL, VIINameConv(" "), VII_FUN_UNDEF, VIIDescrConv("malutki")),
    ( VII_BITS, BITS_STATUS_HIST_ENA,                  1,                   1,    VECT_STATUS,   VII_WACCESS, VII_RINTERNAL, VIINameConv(" "), VII_FUN_UNDEF, VIIDescrConv(" ")),
    ( VII_BITS, BITS_STATUS_PROC_REQ,                  1,                   1,    VECT_STATUS,   VII_WACCESS, VII_RINTERNAL, VIINameConv(" "), VII_FUN_UNDEF, VIIDescrConv(" ")),
    ( VII_BITS, BITS_STATUS_PROC_ACK,                  1,                   1,    VECT_STATUS, VII_WNOACCESS, VII_REXTERNAL, VIINameConv(" "), VII_FUN_UNDEF, VIIDescrConv(" ")),
  (   VII_PAGE, PAGE_MEM_HIST,                         0,                   0,  PAGE_MEM_HIST, VII_WNOACCESS, VII_RNOACCESS, VIINameConv(" "), VII_FUN_UNDEF, VIIDescrConv(" ")),
   (  VII_AREA, AREA_MEM_HIST,              II_DATA_SIZE,     code_bus'length,  PAGE_MEM_HIST,   VII_WACCESS, VII_REXTERNAL, VIINameConv(" "), VII_FUN_UNDEF, VIIDescrConv(" "))
  );
  constant IIPar				:TVII := TVIICreate(VIIItemDeclList,II_ADDR_SIZE,II_DATA_SIZE);
  signal   IIVecInt,IIVecExt,IIVecEna		:TSLV(VII(IIPar)'high downto VEC_INDEX_MIN);
  signal   LIIEnableSig				:TSL;
  signal   LIIAddrLocSig			:TSLV(II_ADDR_USER_SIZE-1 downto 0);
  signal   IIDataSig				:TSLV(II_DATA_SIZE-1 downto 0);
  signal   LIIDataExport			:TSLV(II_DATA_SIZE-1 downto 0);
  signal   LIIDataExportEnable			:TSL;
  signal   ProcReqSig				:TSL;
  signal   ProcAckSig				:TSL;
  signal   HistClkEnaSig			:TSL;
  signal   HistMemWrSig				:TSL;
  signal   HistMemDataOutSig			:TSLV(II_DATA_SIZE-1 downto 0);
  --
  signal   L, H					:TSL;
  signal   in_code_a_reg			:in_code_bus;
  signal   in_sign_a_reg			:in_sign_bus;
  signal   OutCodeBusSig			:s_code_bus;
  signal   OutCodeSig				:code_bus;
  --
  signal   L1AReg				:TSL;
  signal   BCN0Reg				:TSL;
  signal   Pretrg0Reg				:TSL;
  signal   Pretrg1Reg				:TSL;
  signal   LocalTrgReg				:TSL;

begin	  

  L <= '0'; H <= '1';
  l1:
  for index in IN_NUM_tb2001 to IN_NUM-1 generate
    in_code_a_reg(index) <= (others => '0');	
    in_sign_a_reg(index) <= '0';
  end generate;
  
  process (ii_reset,clock)
  begin
    if (ii_reset='0') then
      for index in 0 to IN_NUM_tb2001-1 loop
        in_code_a_reg(index) <= (others => '0');
        in_sign_a_reg(index) <= '0';
      end loop;
    elsif (clock'event and clock='1') then
      for index in 0 to IN_NUM_tb2001-1 loop
        if(IIConnGetBitsData(IIVecExt,IIPar,BITS_STATUS_CHAN_ENA,index)="1") then
          in_code_a_reg(index) <= in_code(index);	
          in_sign_a_reg(index) <= in_signs(index);
	else
          in_code_a_reg(index) <= (others => '0');
          in_sign_a_reg(index) <= '0';
	end if;
      end loop;
    end if;
  end process;
  --
  gb: gb_tb_reg
    port map(
      clock => clock,
      resetN => ii_reset,
      in_code_a => in_code_a_reg,
      in_sign_a => in_sign_a_reg,
      out_code => OutCodeBusSig,
      out_addr => out_addr,
      out_sign => out_sign
    );
  out_code <= OutCodeBusSig;
  --
  -- fast interface
  --
  process(ii_reset, clock)
  begin
    if(ii_reset='0') then
      L1AReg <= '0';
      BCN0Reg <= '0';
      Pretrg0Reg <= '0';
      Pretrg1Reg <= '0';
      LocalTrgReg <= '0';
    elsif(clock'event and clock='1') then
      L1AReg <= l1a;
      BCN0Reg <= bcn0;
      Pretrg0Reg <= pretrg0;
      Pretrg1Reg <= pretrg1;
      LocalTrgReg <= (Pretrg0Reg and Pretrg1Reg);
    end if;
  end process;
  
  local_trg <= LocalTrgReg;
  
  --
  -- diagnostics interface
  --
  OutCodeSig <= OutCodeBusSig(0);
  HistMemWrSig <= IIConnGetAreaWrite(IIVecEna,IIPar,AREA_MEM_HIST,ii_save);
  ProcReqSig  <= TSLconv(IIConnGetBitsData(IIVecInt,IIPar,BITS_STATUS_PROC_REQ,0));
  HistClkEnaSig  <= TSLconv(IIConnGetBitsData(IIVecInt,IIPar,BITS_STATUS_HIST_ENA,0));

  hist_out_code: DPM_PROG_HIST
    generic map(
      LPM_DATA_WIDTH	=> code_bus'length,
      LPM_COUNT_WIDTH	=> II_DATA_SIZE,
      LPM_MDATA_WIDTH	=> II_DATA_SIZE
    )
    port map(
      resetN		=> ii_reset,
      clock		=> clock,
      data_in		=> OutCodeSig,
      clk_ena		=> HistClkEnaSig,
      sim_loop		=> L,
      proc_req		=> ProcReqSig,
      proc_ack		=> ProcAckSig,
      memory_address_in	=> ii_addr(code_bus'length-1 downto 0),
      memory_data_in	=> ii_data,
      memory_data_out	=> HistMemDataOutSig,
      memory_wr		=> HistMemWrSig
  );
  --
  -- internal interface
  --
  LIIEnableSig <= TSLconv((ii_addr(II_ADDR_SIZE-1 downto II_ADDR_USER_SIZE)=II_GB_BASE_ADDR) and (ii_oper='0'));
  LIIAddrLocSig <= ii_addr(II_ADDR_USER_SIZE-1 downto 0);
  --
  process(ii_reset, ii_save)
  begin
    if(ii_reset='0') then
      IIVecInt <= IIReset(IIVecInt,IIPar);
    elsif(ii_save'event and ii_save='1') then
      if(LIIEnableSig='0') then
        IIVecInt <= IISaveAccess(IIVecInt,IIPar,LIIAddrLocSig,IIDataSig);
      end if;	    
    end if;
  end process;
  --
  IIVecEna <= IIWriteEnable(IIVecInt,IIPar,LIIEnableSig,ii_write,LIIAddrLocSig,IIDataSig);
  IIVecExt <= (  IIWriteAccess(IIVecInt,IIPar,LIIEnableSig,ii_write,LIIAddrLocSig,IIDataSig)
              or IIConnPutWordData(IIVecInt,IIPar,WORD_IDENTIFIER,0,TSLVconv(GB_IDENTIFIER,II_DATA_SIZE))
              or IIConnPutWordData(IIVecInt,IIPar,WORD_VERSION,0,TSLVconv(GB_VERSION,II_DATA_SIZE))
              or IIConnPutBitsData(IIVecInt,IIPar,BITS_STATUS_PROC_ACK,0,TSLVconv(ProcAckSig))

              or IIConnPutAreaData(IIVecInt,IIPar,AREA_MEM_HIST, HistMemDataOutSig)
	      );
  LIIDataExport <= IIReadAccess(IIVecExt,IIPar,LIIEnableSig,ii_write,LIIAddrLocSig);
  --
  LIIDataExportEnable <= not(LIIEnableSig) and ii_write;
  CIIData: ALTERA_BUSTRI
    generic map (LPM_WIDTH => II_DATA_SIZE)
    port map    (data_in   => LIIDataExport,
                 data_out  => IIDataSig,
                 data_tri  => ii_data,
                 ena       => LIIDataExportEnable);
  CIIIrq: ALTERA_TRI
    port map    (data_in   => L,
                 data_tri  => ii_int,
		 ena       => Pretrg0Reg);
  --
  -- diagnostic interface
  --
  led <= (others=>'0') when ((L1AReg=BCN0Reg) and (Pretrg0Reg=Pretrg1Reg)) else
         (others=>'1');

  conf_led <= '0';

end behavioural;
