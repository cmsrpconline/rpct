/***********************************************************************\
*                                                                       *
* This file was created by Component Internal Interface Engine software *
*  Copyright(c) 2000-2006 by Krzysztof Pozniak (pozniak@ise.pw.edu.pl)  *
*                           All Rights Reserved.                        *
*                                                                       *
\***********************************************************************/


// constants
const TN IID_CLASS :: LHC_CLK_FREQ = 40000000;
const TN IID_CLASS :: TTC_BCN_EVT_WIDTH = 12;
const TN IID_CLASS :: TTC_BCN_WIDTH = 12;
const TN IID_CLASS :: TTC_EVN_WIDTH = 24;
const TN IID_CLASS :: TTC_BROADCAST1_WIDTH = 4;
const TN IID_CLASS :: TTC_BROADCAST2_WIDTH = 2;
const TN IID_CLASS :: TTC_BROADCAST_SIZE = 6;
const TN IID_CLASS :: TTC_DOUT_ID_WIDTH = 8;
const TN IID_CLASS :: TTC_SUBADDR_WIDTH = 8;
const TN IID_CLASS :: TTC_DQ_WIDTH = 4;
const TN IID_CLASS :: TTC_ID_MODE_WIDTH = 16;
const TN IID_CLASS :: RPC_FEBRE11_DATA_WIDTH = 128;
const TN IID_CLASS :: RPC_FEBRE11_TEST_WIDTH = 32;
const TN IID_CLASS :: RPC_FEBSTD_DATA_WIDTH = 96;
const TN IID_CLASS :: RPC_FEBSTD_TEST_WIDTH = 24;
const TN IID_CLASS :: RPC_LB_DATA_PART_WIDTH = 8;
const TN IID_CLASS :: RPC_LB_TIME_MAX = 7;
const TN IID_CLASS :: RPC_LB_CHAMBER_MAX = 2;
const TN IID_CLASS :: RPC_LB_CHAMBER_NUM = 3;
const TN IID_CLASS :: RPC_LB_FAST_DATA_WIDTH = 5;
const TN IID_CLASS :: RPC_LB_TEST_PART_WIDTH = 8;
const TN IID_CLASS :: GOL_DATA_SIZE = 32;
const TN IID_CLASS :: RPC_LB_TEST_PART_NUM = 4;
const TN IID_CLASS :: RPC_LBRE11_DATA_POS_MAX = 15;
const TN IID_CLASS :: RPC_LBRE11_DATA_POS_WIDTH = 4;
const TN IID_CLASS :: RPC_LBRE11_TIME_WIDTH = 3;
const TN IID_CLASS :: RPC_LBRE11_CHAMBER_WIDTH = 2;
const TN IID_CLASS :: RPC_LBRE11_CODE_WIDTH = 17;
const TN IID_CLASS :: RPC_LBRE11_LMUX_WIDTH = 19;
const TN IID_CLASS :: RPC_LBRE11_BCN_WIDTH = 7;
const TN IID_CLASS :: RPC_LBSTD_DATA_POS_MAX = 11;
const TN IID_CLASS :: RPC_LBSTD_DATA_POS_WIDTH = 4;
const TN IID_CLASS :: RPC_LBSTD_TIME_WIDTH = 3;
const TN IID_CLASS :: RPC_LBSTD_CHAMBER_WIDTH = 2;
const TN IID_CLASS :: RPC_LBSTD_CODE_WIDTH = 17;
const TN IID_CLASS :: RPC_LBSTD_LMUX_WIDTH = 19;
const TN IID_CLASS :: RPC_LBSTD_BCN_WIDTH = 7;
const TN IID_CLASS :: RPC_LINK_STRUC_PART_DATA = 0;
const TN IID_CLASS :: RPC_LINK_STRUC_PART_CODE = 1;
const TN IID_CLASS :: RPC_LINK_STRUC_PART_TIME = 2;
const TN IID_CLASS :: RPC_LINK_STRUC_END_DATA = 3;
const TN IID_CLASS :: RPC_LINK_STRUC_HALF_PART = 4;
const TN IID_CLASS :: RPC_LINK_STRUC_CHAN_NUM = 5;
const TN IID_CLASS :: RPC_LINK_STRUC_FAST_DATA = 6;
const TN IID_CLASS :: RPC_PAC_QUALITY_NUM_MAX = 7;
const TN IID_CLASS :: RPC_PAC_QUALITY_WIDTH = 3;
const TN IID_CLASS :: RPC_PAC_CODE_NUM_MAX = 31;
const TN IID_CLASS :: RPC_PAC_CODE_WIDTH = 5;
const TN IID_CLASS :: RPC_PAC_DATA_WIDTH = 9;
const TN IID_CLASS :: RPC_PAC_SORT_NUM = 4;
const TN IID_CLASS :: RPC_PAC_STRUC_SIGN = 0;
const TN IID_CLASS :: RPC_PAC_STRUC_CODE = 1;
const TN IID_CLASS :: RPC_PAC_STRUC_QUALITY = 2;
const TN IID_CLASS :: I2C_AREA_ADDR_SIZE = 2;
const TN IID_CLASS :: I2C_AREA_ADDR_ITEM = 4;
const TN IID_CLASS :: I2C_AREA_DATA_SIZE = 16;
const TN IID_CLASS :: I2C_AREA_SEL_SIZE = 2;
const TN IID_CLASS :: TB_BASE_CNTRL = 0;
const TN IID_CLASS :: TB_BASE_SORTER = 1;
const TN IID_CLASS :: TB_BASE_OPTO = 2;
const TN IID_CLASS :: TB_BASE_LDPAC = 8;
const TN IID_CLASS :: TB_BASE_RMB = 12;
const TN IID_CLASS :: TB_BASE_NUM = 13;
const TN IID_CLASS :: TB_BASE_SIZE = 4;
const TN IID_CLASS :: TB_II_ADDR_WIDTH_HARD = 20;
const TN IID_CLASS :: TB_II_ADDR_WIDTH_SOFT = 18;
const TN IID_CLASS :: TB_II_BASE_WIDTH = 4;
const TN IID_CLASS :: TB_II_ADDR_WIDTH_USER = 14;
const TN IID_CLASS :: TB_II_DATA_WIDTH = 16;
const TN IID_CLASS :: TB_II_INT_WIDTH = 3;
const TN IID_CLASS :: TB_MUX_MULTIPL = 8;
const TN IID_CLASS :: TB_MUX_MULTIPL_WIDTH = 3;
const TN IID_CLASS :: TB_FAST_BUS_WIDTH = 3;
const TN IID_CLASS :: TB_LINK_NUM = 18;
const TN IID_CLASS :: TB_OPTO_CHAN_NUM = 3;
const TN IID_CLASS :: TB_OPTO_BOARD_NUM = 6;
const TN IID_CLASS :: TB_OPTO_MUX_MULTIPL = 8;
const TN IID_CLASS :: TB_OPTO_MUX_DATA_WIDTH = 3;
const TN IID_CLASS :: TB_OPTO_DATA_WIDTH = 24;
const TN IID_CLASS :: TB_OPTO_BCN_WIDTH = 4;
const TN IID_CLASS :: TB_OPTO_MUX_DELAY_WIDTH = 3;
const TN IID_CLASS :: TB_PAC_BOARD_NUM = 4;
const TN IID_CLASS :: TB_PAC_NUM = 12;
const TN IID_CLASS :: TB_PAC_MUX_MULTIPL = 8;
const TN IID_CLASS :: TB_PAC_MUX_DATA_WIDTH = 14;
const TN IID_CLASS :: TB_PAC_DATA_WIDTH = 112;
const TN IID_CLASS :: TB_PAC_BCN_WIDTH = 3;
const TN IID_CLASS :: TB_PAC_MUX_DELAY_WIDTH = 3;
const TN IID_CLASS :: TB_PAC_GB_DATA_WIDTH = 3;
const TN IID_CLASS :: RPC_GB_DATA_SIZE = 2;
const TN IID_CLASS :: RPC_PAC_GB_PHI_ADDR_SIZE = 4;
const TN IID_CLASS :: RPC_PAC_GB_ETA_ADDR_SIZE = 2;
const TN IID_CLASS :: RPC_PAC_GB_DATA_SIZE = 17;
const TN IID_CLASS :: TB_SORT_MUX_MULTIPL = 8;
const TN IID_CLASS :: TB_SORT_MUX_DATA_WIDTH = 9;
const TN IID_CLASS :: TB_SORT_DATA_WIDTH = 72;
const TN IID_CLASS :: TB_SORT_BCN_WIDTH = 3;
const TN IID_CLASS :: TB_SORT_MUX_DELAY_WIDTH = 3;
const TN IID_CLASS :: TB_BOARD_NUM = 9;
const TS IID_CLASS :: TB_BOARD_IDENTIFIER = "TB";
const TN IID_CLASS :: TB_TIMER_SIZE = 40;
const TN IID_CLASS :: TB_TRIGGER_SEL_L1A = 0;
const TN IID_CLASS :: TB_TRIGGER_SEL_PRETRG0 = 1;
const TN IID_CLASS :: TB_TRIGGER_SEL_PRETRG1 = 2;
const TN IID_CLASS :: TB_TRIGGER_SEL_PRETRG2 = 3;
const TN IID_CLASS :: TB_TRIGGER_SEL_NUM = 4;
const TN IID_CLASS :: TB_TRIGGER_SEL_SIZE = 2;
const TS IID_CLASS :: TB_LDPAC_IDENTIFIER = "LP";
const THV IID_CLASS :: TB_LDPAC_VERSION = "0301";
const TN IID_CLASS :: TB_OPTO_MUX_LINE_NUM = 54;
const TN IID_CLASS :: TB_LDPAC_DATA_DELAY_WIDTH = 2;
const TN IID_CLASS :: TB_LDPAC_BCN0_DELAY_SIZE = 12;
const TN IID_CLASS :: TB_LDPAC_TRG_DELAY_SIZE = 7;
const TN IID_CLASS :: TB_LDPAC_OR_DATA_MAX = 3;
const TN IID_CLASS :: TB_LDPAC_OR_DATA_SIZE = 2;
const TN IID_CLASS :: TB_LDPAC_TRIG_COUNT_SIZE = 32;
const TN IID_CLASS :: CLOCK_TEST_SIZE = 4;
const TN IID_CLASS :: RECEIVER_ERROR_COUNT_SIZE = 32;
const TN IID_CLASS :: DAQ_DIAG_INDATA_SIZE = 432;
const TN IID_CLASS :: DAQ_DIAG_DATA_SIZE = 563;
const TN IID_CLASS :: DAQ_DIAG_DATA_DELAY_SIZE = 8;
const TN IID_CLASS :: DAQ_DIAG_TRIG_NUM = 1;
const TN IID_CLASS :: DAQ_DIAG_TIMER_SIZE = 36;
const TN IID_CLASS :: DAQ_DIAG_MASK_WIDTH = 16;
const TN IID_CLASS :: DAQ_DIAG_ADDR_WIDTH = 7;
const TN IID_CLASS :: DAQ_DIAG_ADDR_POS = 128;
const TN IID_CLASS :: DAQ_DIAG_MEM_DATA_SIZE = 569;
const TN IID_CLASS :: DAQ_MEM_ADDR_SIZE = 13;
const TN IID_CLASS :: PULSE_POS_NUM = 256;
const TN IID_CLASS :: PULSE_POS_SIZE = 8;
const TN IID_CLASS :: PULSE_DATA_SIZE = 108;
const TN IID_CLASS :: PULSE_MEM_ADDR_SIZE = 11;
const TN IID_CLASS :: TRG_COND_SEL_NONE = 0;
const TN IID_CLASS :: TRG_COND_SEL_L1A = 1;
const TN IID_CLASS :: TRG_COND_SEL_PRETRG0 = 2;
const TN IID_CLASS :: TRG_COND_SEL_PRETRG1 = 3;
const TN IID_CLASS :: TRG_COND_SEL_PRETRG2 = 4;
const TN IID_CLASS :: TRG_COND_SEL_BCN0 = 5;
const TN IID_CLASS :: TRG_COND_SEL_LOCAL = 6;
const TN IID_CLASS :: TRG_COND_SEL_NUM = 7;
const TN IID_CLASS :: TRG_COND_SEL_SIZE = 3;

// parameters
const TN IID_CLASS :: II_ADDR_SIZE  = 18;
const TN IID_CLASS :: II_DATA_SIZE  = 16;
const TN IID_CLASS :: CHECK_SUM = 610530694;

// interfaces
const TN IID_CLASS :: PAGE_UNIVERSAL = 0;
const TN IID_CLASS :: WORD_CHECKSUM = 1;
const TN IID_CLASS :: WORD_BOARD = 2;
const TN IID_CLASS :: WORD_IDENTIFIER = 3;
const TN IID_CLASS :: WORD_VERSION = 4;
const TN IID_CLASS :: WORD_USER_REG1 = 5;
const TN IID_CLASS :: WORD_USER_REG2 = 6;
const TN IID_CLASS :: VECT_STATUS = 7;
const TN IID_CLASS :: BITS_STATUS_PLL_UNLOCK = 8;
const TN IID_CLASS :: BITS_STATUS_STROBE_UNLOCK = 9;
const TN IID_CLASS :: BITS_STATUS_TTC_DATA_DELAY = 10;
const TN IID_CLASS :: BITS_STATUS_PAC_DATA_OR = 11;
const TN IID_CLASS :: BITS_STATUS_TRG_DATA_SEL = 12;
const TN IID_CLASS :: VECT_TEST_CNT = 13;
const TN IID_CLASS :: BITS_TEST_CNT_CLK40 = 14;
const TN IID_CLASS :: BITS_TEST_CNT_BCN0 = 15;
const TN IID_CLASS :: WORD_REC_MUX_CLK90 = 16;
const TN IID_CLASS :: WORD_REC_MUX_CLK_INV = 17;
const TN IID_CLASS :: WORD_REC_MUX_REG_ADD = 18;
const TN IID_CLASS :: WORD_REC_MUX_DELAY = 19;
const TN IID_CLASS :: WORD_REC_DATA_DELAY = 20;
const TN IID_CLASS :: WORD_REC_CHECK_ENA = 21;
const TN IID_CLASS :: WORD_REC_CHECK_DATA_ENA = 22;
const TN IID_CLASS :: WORD_REC_TEST_ENA = 23;
const TN IID_CLASS :: WORD_REC_TEST_RND_ENA = 24;
const TN IID_CLASS :: WORD_REC_TEST_DATA = 25;
const TN IID_CLASS :: WORD_REC_TEST_OR_DATA = 26;
const TN IID_CLASS :: WORD_REC_ERROR_COUNT = 27;
const TN IID_CLASS :: WORD_MPAC_CFG_ID = 28;
const TN IID_CLASS :: WORD_PAC_ENA = 29;
const TN IID_CLASS :: WORD_SEND_CLK_INV = 30;
const TN IID_CLASS :: WORD_SEND_CHECK_ENA = 31;
const TN IID_CLASS :: WORD_SEND_CHECK_DATA_ENA = 32;
const TN IID_CLASS :: WORD_SEND_TEST_ENA = 33;
const TN IID_CLASS :: WORD_SEND_TEST_RND_ENA = 34;
const TN IID_CLASS :: WORD_SEND_TEST_DATA = 35;
const TN IID_CLASS :: WORD_TTC_BCN = 36;
const TN IID_CLASS :: WORD_TTC_EVN = 37;
const TN IID_CLASS :: WORD_TTC_TEST_DATA = 38;
const TN IID_CLASS :: WORD_BCN0_DELAY = 39;
const TN IID_CLASS :: WORD_DATA_TRG_DELAY = 40;
const TN IID_CLASS :: WORD_STAT_TRIG_COUNT = 41;
const TN IID_CLASS :: VECT_DAQ = 42;
const TN IID_CLASS :: BITS_DAQ_TIMER_START = 43;
const TN IID_CLASS :: BITS_DAQ_TIMER_STOP = 44;
const TN IID_CLASS :: BITS_DAQ_TIMER_LOC_ENA = 45;
const TN IID_CLASS :: BITS_DAQ_TIMER_TRIG_SEL = 46;
const TN IID_CLASS :: BITS_DAQ_PROC_REQ = 47;
const TN IID_CLASS :: BITS_DAQ_PROC_ACK = 48;
const TN IID_CLASS :: BITS_DAQ_READY = 49;
const TN IID_CLASS :: WORD_DAQ_TIMER_LIMIT = 50;
const TN IID_CLASS :: WORD_DAQ_TIMER_COUNT = 51;
const TN IID_CLASS :: WORD_DAQ_TIMER_TRG_DELAY = 52;
const TN IID_CLASS :: WORD_DAQ_DATA_DELAY = 53;
const TN IID_CLASS :: VECT_PULSER = 54;
const TN IID_CLASS :: BITS_PULSER_OUT_ENA = 55;
const TN IID_CLASS :: BITS_PULSER_LOOP_ENA = 56;
const TN IID_CLASS :: BITS_PULSER_TIMER_START = 57;
const TN IID_CLASS :: BITS_PULSER_TIMER_STOP = 58;
const TN IID_CLASS :: BITS_PULSER_TIMER_LOC_ENA = 59;
const TN IID_CLASS :: BITS_PULSER_TIMER_TRIG_SEL = 60;
const TN IID_CLASS :: BITS_PULSER_REPEAT_ENA = 61;
const TN IID_CLASS :: BITS_PULSER_PROC_REQ = 62;
const TN IID_CLASS :: BITS_PULSER_PROC_ACK = 63;
const TN IID_CLASS :: WORD_PULSER_LENGTH = 64;
const TN IID_CLASS :: WORD_PULSER_TIMER_LIMIT = 65;
const TN IID_CLASS :: WORD_PULSER_TIMER_COUNT = 66;
const TN IID_CLASS :: WORD_PULSER_TIMER_TRG_DELAY = 67;
const TN IID_CLASS :: AREA_MEM_PULSE = 68;
const TN IID_CLASS :: AREA_MEM_DAQ_DIAG = 69;

TMemoryMap* IID_CLASS ::BuildMemoryMap() {
  const TVIIItemDecl declItems[] =
  {
    { VII_WORD, WORD_CHECKSUM, 16, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("CHECKSUM"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_BOARD, 16, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("BOARD"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_IDENTIFIER, 16, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("IDENTIFIER"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_VERSION, 16, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("VERSION"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_USER_REG1, 16, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("USER_REG1"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_USER_REG2, 16, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("USER_REG2"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_VECT, VECT_STATUS, 0, 0, PAGE_UNIVERSAL, VII_WNOACCESS, VII_RNOACCESS, VIINameConv("STATUS"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_STATUS_PLL_UNLOCK, 1, 1, VECT_STATUS, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("PLL_UNLOCK"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_STATUS_STROBE_UNLOCK, 1, 1, VECT_STATUS, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("STROBE_UNLOCK"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_STATUS_TTC_DATA_DELAY, 3, 1, VECT_STATUS, VII_WACCESS, VII_RINTERNAL,VIINameConv("TTC_DATA_DELAY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_STATUS_PAC_DATA_OR, 2, 1, VECT_STATUS, VII_WACCESS, VII_RINTERNAL,VIINameConv("PAC_DATA_OR"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_STATUS_TRG_DATA_SEL, 3, 1, VECT_STATUS, VII_WACCESS, VII_RINTERNAL,VIINameConv("TRG_DATA_SEL"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_VECT, VECT_TEST_CNT, 0, 0, PAGE_UNIVERSAL, VII_WNOACCESS, VII_RNOACCESS, VIINameConv("TEST_CNT"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_TEST_CNT_CLK40, 4, 1, VECT_TEST_CNT, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("CLK40"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_TEST_CNT_BCN0, 4, 1, VECT_TEST_CNT, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("BCN0"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_MUX_CLK90, 54, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_MUX_CLK90"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_MUX_CLK_INV, 54, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_MUX_CLK_INV"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_MUX_REG_ADD, 54, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_MUX_REG_ADD"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_MUX_DELAY, 3, 18, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_MUX_DELAY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_DATA_DELAY, 2, 18, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_DATA_DELAY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_CHECK_ENA, 18, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_CHECK_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_CHECK_DATA_ENA, 18, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_CHECK_DATA_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_TEST_ENA, 18, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_TEST_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_TEST_RND_ENA, 18, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_TEST_RND_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_TEST_DATA, 24, 18, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("REC_TEST_DATA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_TEST_OR_DATA, 3, 18, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("REC_TEST_OR_DATA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_ERROR_COUNT, 32, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_REXTERNAL,VIINameConv("REC_ERROR_COUNT"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_MPAC_CFG_ID, 16, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("MPAC_CFG_ID"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_PAC_ENA, 12, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("PAC_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_SEND_CLK_INV, 1, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("SEND_CLK_INV"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_SEND_CHECK_ENA, 1, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("SEND_CHECK_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_SEND_CHECK_DATA_ENA, 1, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("SEND_CHECK_DATA_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_SEND_TEST_ENA, 1, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("SEND_TEST_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_SEND_TEST_RND_ENA, 1, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("SEND_TEST_RND_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_SEND_TEST_DATA, 112, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("SEND_TEST_DATA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_TTC_BCN, 12, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("TTC_BCN"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_TTC_EVN, 24, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("TTC_EVN"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_TTC_TEST_DATA, 6, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("TTC_TEST_DATA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_BCN0_DELAY, 12, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("BCN0_DELAY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_DATA_TRG_DELAY, 7, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("DATA_TRG_DELAY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_STAT_TRIG_COUNT, 32, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_REXTERNAL,VIINameConv("STAT_TRIG_COUNT"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_VECT, VECT_DAQ, 0, 0, PAGE_UNIVERSAL, VII_WNOACCESS, VII_RNOACCESS, VIINameConv("DAQ"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_TIMER_START, 1, 1, VECT_DAQ, VII_WACCESS, VII_RINTERNAL,VIINameConv("TIMER_START"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_TIMER_STOP, 1, 1, VECT_DAQ, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("TIMER_STOP"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_TIMER_LOC_ENA, 1, 1, VECT_DAQ, VII_WACCESS, VII_RINTERNAL,VIINameConv("TIMER_LOC_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_TIMER_TRIG_SEL, 3, 1, VECT_DAQ, VII_WACCESS, VII_RINTERNAL,VIINameConv("TIMER_TRIG_SEL"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_PROC_REQ, 1, 1, VECT_DAQ, VII_WACCESS, VII_RINTERNAL,VIINameConv("PROC_REQ"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_PROC_ACK, 1, 1, VECT_DAQ, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("PROC_ACK"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_READY, 1, 1, VECT_DAQ, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("READY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_DAQ_TIMER_LIMIT, 40, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("DAQ_TIMER_LIMIT"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_DAQ_TIMER_COUNT, 40, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("DAQ_TIMER_COUNT"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_DAQ_TIMER_TRG_DELAY, 7, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("DAQ_TIMER_TRG_DELAY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_DAQ_DATA_DELAY, 8, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("DAQ_DATA_DELAY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_VECT, VECT_PULSER, 0, 0, PAGE_UNIVERSAL, VII_WNOACCESS, VII_RNOACCESS, VIINameConv("PULSER"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_OUT_ENA, 1, 1, VECT_PULSER, VII_WACCESS, VII_RINTERNAL,VIINameConv("OUT_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_LOOP_ENA, 1, 1, VECT_PULSER, VII_WACCESS, VII_RINTERNAL,VIINameConv("LOOP_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_TIMER_START, 1, 1, VECT_PULSER, VII_WACCESS, VII_RINTERNAL,VIINameConv("TIMER_START"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_TIMER_STOP, 1, 1, VECT_PULSER, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("TIMER_STOP"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_TIMER_LOC_ENA, 1, 1, VECT_PULSER, VII_WACCESS, VII_RINTERNAL,VIINameConv("TIMER_LOC_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_TIMER_TRIG_SEL, 3, 1, VECT_PULSER, VII_WACCESS, VII_RINTERNAL,VIINameConv("TIMER_TRIG_SEL"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_REPEAT_ENA, 1, 1, VECT_PULSER, VII_WACCESS, VII_RINTERNAL,VIINameConv("REPEAT_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_PROC_REQ, 1, 1, VECT_PULSER, VII_WACCESS, VII_RINTERNAL,VIINameConv("PROC_REQ"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_PROC_ACK, 1, 1, VECT_PULSER, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("PROC_ACK"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_PULSER_LENGTH, 8, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("PULSER_LENGTH"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_PULSER_TIMER_LIMIT, 40, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("PULSER_TIMER_LIMIT"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_PULSER_TIMER_COUNT, 40, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("PULSER_TIMER_COUNT"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_PULSER_TIMER_TRG_DELAY, 7, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("PULSER_TIMER_TRG_DELAY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_AREA, AREA_MEM_PULSE, 108, 256, PAGE_UNIVERSAL, VII_WACCESS, VII_REXTERNAL,VIINameConv("MEM_PULSE"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_AREA, AREA_MEM_DAQ_DIAG, 569, 128, PAGE_UNIVERSAL, VII_WACCESS, VII_REXTERNAL,VIINameConv("MEM_DAQ_DIAG"), VII_FUN_UNDEF, VIIDescrConv(" ")}
  };
  const TVIIItem viiItems[] =
  {
    { VII_WORD, 1, -1, 16, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 0, 1}, //WORD_CHECKSUM
    { VII_WORD, 2, -1, 16, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 1, 1}, //WORD_BOARD
    { VII_WORD, 3, -1, 16, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 2, 1}, //WORD_IDENTIFIER
    { VII_WORD, 4, -1, 16, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 3, 1}, //WORD_VERSION
    { VII_WORD, 5, -1, 16, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 4, 1}, //WORD_USER_REG1
    { VII_WORD, 6, -1, 16, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 5, 1}, //WORD_USER_REG2
    { VII_BITS, 8, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 6, 0}, //BITS_STATUS_PLL_UNLOCK
    { VII_BITS, 9, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 6, 1}, //BITS_STATUS_STROBE_UNLOCK
    { VII_BITS, 10, -1, 3, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 6, 2}, //BITS_STATUS_TTC_DATA_DELAY
    { VII_BITS, 11, -1, 2, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 6, 5}, //BITS_STATUS_PAC_DATA_OR
    { VII_BITS, 12, -1, 3, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 6, 7}, //BITS_STATUS_TRG_DATA_SEL
    { VII_BITS, 14, -1, 4, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 7, 0}, //BITS_TEST_CNT_CLK40
    { VII_BITS, 15, -1, 4, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 7, 4}, //BITS_TEST_CNT_BCN0
    { VII_WORD, 16, -1, 54, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 8, 4}, //WORD_REC_MUX_CLK90
    { VII_WORD, 17, -1, 54, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 12, 4}, //WORD_REC_MUX_CLK_INV
    { VII_WORD, 18, -1, 54, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 16, 4}, //WORD_REC_MUX_REG_ADD
    { VII_WORD, 19, -1, 3, 18, VII_WACCESS, -1, VII_RINTERNAL, -1, 20, 1}, //WORD_REC_MUX_DELAY
    { VII_WORD, 20, -1, 2, 18, VII_WACCESS, -1, VII_RINTERNAL, -1, 38, 1}, //WORD_REC_DATA_DELAY
    { VII_WORD, 21, -1, 18, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 56, 2}, //WORD_REC_CHECK_ENA
    { VII_WORD, 22, -1, 18, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 58, 2}, //WORD_REC_CHECK_DATA_ENA
    { VII_WORD, 23, -1, 18, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 60, 2}, //WORD_REC_TEST_ENA
    { VII_WORD, 24, -1, 18, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 62, 2}, //WORD_REC_TEST_RND_ENA
    { VII_WORD, 25, -1, 24, 18, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 64, 2}, //WORD_REC_TEST_DATA
    { VII_WORD, 26, -1, 3, 18, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 100, 1}, //WORD_REC_TEST_OR_DATA
    { VII_WORD, 27, -1, 32, 1, VII_WACCESS, -1, VII_REXTERNAL, -1, 118, 2}, //WORD_REC_ERROR_COUNT
    { VII_WORD, 28, -1, 16, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 120, 1}, //WORD_MPAC_CFG_ID
    { VII_WORD, 29, -1, 12, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 121, 1}, //WORD_PAC_ENA
    { VII_WORD, 30, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 122, 1}, //WORD_SEND_CLK_INV
    { VII_WORD, 31, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 123, 1}, //WORD_SEND_CHECK_ENA
    { VII_WORD, 32, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 124, 1}, //WORD_SEND_CHECK_DATA_ENA
    { VII_WORD, 33, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 125, 1}, //WORD_SEND_TEST_ENA
    { VII_WORD, 34, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 126, 1}, //WORD_SEND_TEST_RND_ENA
    { VII_WORD, 35, -1, 112, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 127, 7}, //WORD_SEND_TEST_DATA
    { VII_WORD, 36, -1, 12, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 134, 1}, //WORD_TTC_BCN
    { VII_WORD, 37, -1, 24, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 135, 2}, //WORD_TTC_EVN
    { VII_WORD, 38, -1, 6, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 137, 1}, //WORD_TTC_TEST_DATA
    { VII_WORD, 39, -1, 12, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 138, 1}, //WORD_BCN0_DELAY
    { VII_WORD, 40, -1, 7, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 139, 1}, //WORD_DATA_TRG_DELAY
    { VII_WORD, 41, -1, 32, 1, VII_WACCESS, -1, VII_REXTERNAL, -1, 140, 2}, //WORD_STAT_TRIG_COUNT
    { VII_BITS, 43, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 142, 0}, //BITS_DAQ_TIMER_START
    { VII_BITS, 44, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 142, 1}, //BITS_DAQ_TIMER_STOP
    { VII_BITS, 45, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 142, 2}, //BITS_DAQ_TIMER_LOC_ENA
    { VII_BITS, 46, -1, 3, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 142, 3}, //BITS_DAQ_TIMER_TRIG_SEL
    { VII_BITS, 47, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 142, 6}, //BITS_DAQ_PROC_REQ
    { VII_BITS, 48, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 142, 7}, //BITS_DAQ_PROC_ACK
    { VII_BITS, 49, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 142, 8}, //BITS_DAQ_READY
    { VII_WORD, 50, -1, 40, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 143, 3}, //WORD_DAQ_TIMER_LIMIT
    { VII_WORD, 51, -1, 40, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 146, 3}, //WORD_DAQ_TIMER_COUNT
    { VII_WORD, 52, -1, 7, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 149, 1}, //WORD_DAQ_TIMER_TRG_DELAY
    { VII_WORD, 53, -1, 8, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 150, 1}, //WORD_DAQ_DATA_DELAY
    { VII_BITS, 55, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 151, 0}, //BITS_PULSER_OUT_ENA
    { VII_BITS, 56, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 151, 1}, //BITS_PULSER_LOOP_ENA
    { VII_BITS, 57, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 151, 2}, //BITS_PULSER_TIMER_START
    { VII_BITS, 58, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 151, 3}, //BITS_PULSER_TIMER_STOP
    { VII_BITS, 59, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 151, 4}, //BITS_PULSER_TIMER_LOC_ENA
    { VII_BITS, 60, -1, 3, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 151, 5}, //BITS_PULSER_TIMER_TRIG_SEL
    { VII_BITS, 61, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 151, 8}, //BITS_PULSER_REPEAT_ENA
    { VII_BITS, 62, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 151, 9}, //BITS_PULSER_PROC_REQ
    { VII_BITS, 63, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 151, 10}, //BITS_PULSER_PROC_ACK
    { VII_WORD, 64, -1, 8, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 152, 1}, //WORD_PULSER_LENGTH
    { VII_WORD, 65, -1, 40, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 153, 3}, //WORD_PULSER_TIMER_LIMIT
    { VII_WORD, 66, -1, 40, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 156, 3}, //WORD_PULSER_TIMER_COUNT
    { VII_WORD, 67, -1, 7, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 159, 1}, //WORD_PULSER_TIMER_TRG_DELAY
    { VII_AREA, 68, -1, 108, 256, VII_WACCESS, -1, VII_REXTERNAL, -1, 2048, 7}, //AREA_MEM_PULSE
    { VII_AREA, 69, -1, 569, 128, VII_WACCESS, -1, VII_REXTERNAL, -1, 8192, 36} //AREA_MEM_DAQ_DIAG
  };
  return new TMemoryMap(declItems, sizeof declItems / sizeof declItems[0],
    viiItems, sizeof viiItems / sizeof viiItems[0], II_ADDR_SIZE, II_DATA_SIZE, CHECK_SUM);
}
