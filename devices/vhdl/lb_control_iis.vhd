library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;
library lpm;
use lpm.lpm_components.all;
use work.std_logic_1164_ktp.all;
use work.vcomponent.all;
use work.LPMcomponent.all;
use work.i2c.all;
use work.tridaq.all;
package lb_control_iis is
--
-- internal interface
--
constant   LB_CONTR_IDENTIFIER  :TS  :="LR"; --chip name  
constant   LB_CONTR_VERSION  :THV  :="0202"; --chip version  
constant   TIME_DELAY_SIZE  :TN  :=16; --delay value  
constant   PAGE_REGISTERS  :TN  := 6; --    
constant   WORD_IDENTIFIER  :TN  := 8; --    
constant   WORD_VERSION  :TN  := 9; --    
constant   VECT_STATUS  :TN  := 11; --    
constant   BITS_STATUS_CLOCK_SEL  :TN  := 12; --    
constant   BITS_STATUS_TRG_SEL  :TN  := 13; --    
constant   BITS_STATUS_PRETRG0_SEL  :TN  := 14; --    
constant   BITS_STATUS_PRETRG1_SEL  :TN  := 15; --    
constant   VECT_GOL  :TN  := 17; --    
constant   BITS_GOL_LASER  :TN  := 18; --    
constant   BITS_GOL_NEDGE  :TN  := 19; --    
constant   BITS_GOL_TX_ENA  :TN  := 20; --    
constant   BITS_GOL_TX_ERR  :TN  := 21; --    
constant   BITS_GOL_READY  :TN  := 22; --    
constant   WORD_FEB_TEST_DATA  :TN  := 24; --    
constant   WORD_DELAY_BCN0  :TN  := 25; --    
constant   WORD_DELAY_L1A  :TN  := 26; --    
constant   WORD_DELAY_PRETRG0  :TN  := 27; --    
constant   WORD_DELAY_PRETRG1  :TN  := 28; --    
constant   AREA_I2C  :TN  := 30; --    
--   predefined  for I2C interface  
--   clock source select  
constant   CLOCK_SEL_TTC_CLK  :TN  :=VEC_INDEX_MIN; --  
constant   CLOCK_SEL_TTC_DES1  :TN  :=CLOCK_SEL_TTC_CLK+1; --  
constant   CLOCK_SEL_TTC_DES2  :TN  :=CLOCK_SEL_TTC_DES1+1; --  
constant   CLOCK_SEL_INT  :TN  :=CLOCK_SEL_TTC_DES2+1; --  
constant   CLOCK_SEL_EXT  :TN  :=CLOCK_SEL_INT+1; --  
constant   CLOCK_SEL_NONE  :TN  :=CLOCK_SEL_EXT+1; --  
constant   CLOCK_SEL_NUM  :TN  :=CLOCK_SEL_NONE+1; --  
constant   CLOCK_SEL_SIZE  :TN  :=TVLcreate(CLOCK_SEL_NUM-1); --  
--   trigger source select  
constant   TRG_SEL_TTC_L1A  :TN  :=VEC_INDEX_MIN; --  
constant   TRG_SEL_EXT  :TN  :=TRG_SEL_TTC_L1A+1; --  
constant   TRG_SEL_NUM  :TN  :=TRG_SEL_EXT+1; --  
constant   TRG_SEL_SIZE  :TN  :=TVLcreate(TRG_SEL_NUM-1); --  
--  pretrigger source select  
constant   PRETRG_SEL_EXT_TRG  :TN  :=VEC_INDEX_MIN; --  
constant   PRETRG_SEL_EXT_CLK  :TN  :=PRETRG_SEL_EXT_TRG+1; --  
constant   PRETRG_SEL_TTC_L1A  :TN  :=PRETRG_SEL_EXT_CLK+1; --  
constant   PRETRG_SEL_BROADCAST1  :TN  :=PRETRG_SEL_TTC_L1A+1; --  
constant   PRETRG_SEL_NUM  :TN  :=PRETRG_SEL_BROADCAST1+TTC_BROADCAST1_DATA_SIZE; --  
constant   PRETRG_SEL_SIZE  :TN  :=TVLcreate(PRETRG_SEL_NUM-1); --  
--   " item type                item ID              width                 num       parent ID    write type     read type                 name                         type    description"  
--
constant VIIItemDeclList	:TVIIItemDeclList :=(  
(    VII_PAGE  ,              PAGE_REGISTERS     ,                    0  ,                    0    ,   PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv(" ")   ,                    VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,            WORD_IDENTIFIER     ,         II_DATA_SIZE  ,                    1    ,   PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("IDENTIFIER ")   ,          VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,               WORD_VERSION     ,         II_DATA_SIZE  ,                    1    ,   PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("VERSION ")   ,             VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_VECT  ,                VECT_STATUS     ,                    0  ,                    0    ,   PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv("STATUS ")   ,              VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_STATUS_CLOCK_SEL     ,       CLOCK_SEL_SIZE  ,                    1    ,      VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("STATUS_CLOCK_SEL")   ,     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,       BITS_STATUS_TRG_SEL     ,         TRG_SEL_SIZE  ,                    1    ,      VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("STATUS_TRG_SEL")   ,       VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_STATUS_PRETRG0_SEL     ,      PRETRG_SEL_SIZE  ,                    1    ,      VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("STATUS_PRETRG0_SEL")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_STATUS_PRETRG1_SEL     ,      PRETRG_SEL_SIZE  ,                    1    ,      VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("STATUS_PRETRG1_SEL")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_VECT  ,                   VECT_GOL     ,                    0  ,                    0    ,   PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv("GOL ")   ,                 VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,            BITS_GOL_LASER     ,                    1  ,                    1    ,         VECT_GOL         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("GOL_LASER ")   ,           VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,            BITS_GOL_NEDGE     ,                    1  ,                    1    ,         VECT_GOL         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("GOL_NEDGE ")   ,           VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,           BITS_GOL_TX_ENA     ,                    1  ,                    1    ,         VECT_GOL         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("GOL_TX_ENA ")   ,          VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,           BITS_GOL_TX_ERR     ,                    1  ,                    1    ,         VECT_GOL         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("GOL_TX_ERR ")   ,          VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,            BITS_GOL_READY     ,                    1  ,                    1    ,         VECT_GOL         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("GOL_READY ")   ,           VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,            WORD_DELAY_BCN0     ,      TIME_DELAY_SIZE  ,                    1    ,   PAGE_REGISTERS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("DELAY_BCN0 ")   ,          VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,             WORD_DELAY_L1A     ,      TIME_DELAY_SIZE  ,                    1    ,   PAGE_REGISTERS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("DELAY_L1A ")   ,           VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,         WORD_DELAY_PRETRG0     ,      TIME_DELAY_SIZE  ,                    1    ,   PAGE_REGISTERS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("DELAY_PRETRG0, ")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,         WORD_DELAY_PRETRG1     ,      TIME_DELAY_SIZE  ,                    1    ,   PAGE_REGISTERS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("DELAY_PRETRG1, ")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,         WORD_FEB_TEST_DATA     ,        FEB_TEST_DATA  ,                    1    ,   PAGE_REGISTERS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("FEB_TEST_DATA ")   ,       VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_AREA  ,                   AREA_I2C     ,   AREA_I2C_DATA_SIZE  ,   AREA_I2C_ADDR_ITEM    ,   PAGE_REGISTERS         ,     VII_WACCESS  ,   VII_REXTERNAL   ,   VIINameConv("I2C_ACCESS_AREA")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ) 
);
end lb_control_iis;
