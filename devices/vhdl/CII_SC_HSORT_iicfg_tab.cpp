/***********************************************************************\
*                                                                       *
* This file was created by Component Internal Interface Engine software *
*  Copyright(c) 2000-2006 by Krzysztof Pozniak (pozniak@ise.pw.edu.pl)  *
*                           All Rights Reserved.                        *
*                                                                       *
\***********************************************************************/


// constants
const TN IID_CLASS :: LHC_CLK_FREQ = 40000000;
const TN IID_CLASS :: TTC_BCN_EVT_WIDTH = 12;
const TN IID_CLASS :: TTC_BCN_WIDTH = 12;
const TN IID_CLASS :: TTC_EVN_WIDTH = 24;
const TN IID_CLASS :: TTC_BROADCAST1_WIDTH = 4;
const TN IID_CLASS :: TTC_BROADCAST2_WIDTH = 2;
const TN IID_CLASS :: TTC_BROADCAST_SIZE = 6;
const TN IID_CLASS :: TTC_DOUT_ID_WIDTH = 8;
const TN IID_CLASS :: TTC_SUBADDR_WIDTH = 8;
const TN IID_CLASS :: TTC_DQ_WIDTH = 4;
const TN IID_CLASS :: TTC_ID_MODE_WIDTH = 16;
const TN IID_CLASS :: RPC_FEBRE11_DATA_WIDTH = 128;
const TN IID_CLASS :: RPC_FEBRE11_TEST_WIDTH = 32;
const TN IID_CLASS :: RPC_FEBSTD_DATA_WIDTH = 96;
const TN IID_CLASS :: RPC_FEBSTD_TEST_WIDTH = 24;
const TN IID_CLASS :: RPC_LB_DATA_PART_WIDTH = 8;
const TN IID_CLASS :: RPC_LB_TIME_MAX = 7;
const TN IID_CLASS :: RPC_LB_CHAMBER_MAX = 2;
const TN IID_CLASS :: RPC_LB_CHAMBER_NUM = 3;
const TN IID_CLASS :: RPC_LB_FAST_DATA_WIDTH = 5;
const TN IID_CLASS :: RPC_LB_TEST_PART_WIDTH = 8;
const TN IID_CLASS :: GOL_DATA_SIZE = 32;
const TN IID_CLASS :: RPC_LB_TEST_PART_NUM = 4;
const TN IID_CLASS :: RPC_LBRE11_DATA_POS_MAX = 15;
const TN IID_CLASS :: RPC_LBRE11_DATA_POS_WIDTH = 4;
const TN IID_CLASS :: RPC_LBRE11_TIME_WIDTH = 3;
const TN IID_CLASS :: RPC_LBRE11_CHAMBER_WIDTH = 2;
const TN IID_CLASS :: RPC_LBRE11_CODE_WIDTH = 17;
const TN IID_CLASS :: RPC_LBRE11_LMUX_WIDTH = 19;
const TN IID_CLASS :: RPC_LBRE11_BCN_WIDTH = 7;
const TN IID_CLASS :: RPC_LBSTD_DATA_POS_MAX = 11;
const TN IID_CLASS :: RPC_LBSTD_DATA_POS_WIDTH = 4;
const TN IID_CLASS :: RPC_LBSTD_TIME_WIDTH = 3;
const TN IID_CLASS :: RPC_LBSTD_CHAMBER_WIDTH = 2;
const TN IID_CLASS :: RPC_LBSTD_CODE_WIDTH = 17;
const TN IID_CLASS :: RPC_LBSTD_LMUX_WIDTH = 19;
const TN IID_CLASS :: RPC_LBSTD_BCN_WIDTH = 7;
const TN IID_CLASS :: RPC_LINK_STRUC_PART_DATA = 0;
const TN IID_CLASS :: RPC_LINK_STRUC_PART_CODE = 1;
const TN IID_CLASS :: RPC_LINK_STRUC_PART_TIME = 2;
const TN IID_CLASS :: RPC_LINK_STRUC_END_DATA = 3;
const TN IID_CLASS :: RPC_LINK_STRUC_HALF_PART = 4;
const TN IID_CLASS :: RPC_LINK_STRUC_CHAN_NUM = 5;
const TN IID_CLASS :: RPC_LINK_STRUC_FAST_DATA = 6;
const TN IID_CLASS :: RPC_PAC_QUALITY_NUM_MAX = 7;
const TN IID_CLASS :: RPC_PAC_QUALITY_WIDTH = 3;
const TN IID_CLASS :: RPC_PAC_CODE_NUM_MAX = 31;
const TN IID_CLASS :: RPC_PAC_CODE_WIDTH = 5;
const TN IID_CLASS :: RPC_PAC_DATA_WIDTH = 9;
const TN IID_CLASS :: RPC_PAC_SORT_NUM = 4;
const TN IID_CLASS :: RPC_PAC_STRUC_SIGN = 0;
const TN IID_CLASS :: RPC_PAC_STRUC_CODE = 1;
const TN IID_CLASS :: RPC_PAC_STRUC_QUALITY = 2;
const TN IID_CLASS :: I2C_AREA_ADDR_SIZE = 2;
const TN IID_CLASS :: I2C_AREA_ADDR_ITEM = 4;
const TN IID_CLASS :: I2C_AREA_DATA_SIZE = 16;
const TN IID_CLASS :: I2C_AREA_SEL_SIZE = 2;
const TN IID_CLASS :: TB_BASE_CNTRL = 0;
const TN IID_CLASS :: TB_BASE_SORTER = 1;
const TN IID_CLASS :: TB_BASE_OPTO = 2;
const TN IID_CLASS :: TB_BASE_LDPAC = 8;
const TN IID_CLASS :: TB_BASE_RMB = 12;
const TN IID_CLASS :: TB_BASE_NUM = 13;
const TN IID_CLASS :: TB_BASE_SIZE = 4;
const TN IID_CLASS :: TB_II_ADDR_WIDTH_HARD = 20;
const TN IID_CLASS :: TB_II_ADDR_WIDTH_SOFT = 18;
const TN IID_CLASS :: TB_II_BASE_WIDTH = 4;
const TN IID_CLASS :: TB_II_ADDR_WIDTH_USER = 14;
const TN IID_CLASS :: TB_II_DATA_WIDTH = 16;
const TN IID_CLASS :: TB_II_INT_WIDTH = 3;
const TN IID_CLASS :: TB_MUX_MULTIPL = 8;
const TN IID_CLASS :: TB_MUX_MULTIPL_WIDTH = 3;
const TN IID_CLASS :: TB_FAST_BUS_WIDTH = 3;
const TN IID_CLASS :: TB_LINK_NUM = 18;
const TN IID_CLASS :: TB_OPTO_CHAN_NUM = 3;
const TN IID_CLASS :: TB_OPTO_BOARD_NUM = 6;
const TN IID_CLASS :: TB_OPTO_MUX_MULTIPL = 8;
const TN IID_CLASS :: TB_OPTO_MUX_DATA_WIDTH = 3;
const TN IID_CLASS :: TB_OPTO_DATA_WIDTH = 24;
const TN IID_CLASS :: TB_OPTO_BCN_WIDTH = 4;
const TN IID_CLASS :: TB_OPTO_MUX_DELAY_WIDTH = 3;
const TN IID_CLASS :: TB_PAC_BOARD_NUM = 4;
const TN IID_CLASS :: TB_PAC_NUM = 12;
const TN IID_CLASS :: TB_PAC_MUX_MULTIPL = 8;
const TN IID_CLASS :: TB_PAC_MUX_DATA_WIDTH = 14;
const TN IID_CLASS :: TB_PAC_DATA_WIDTH = 112;
const TN IID_CLASS :: TB_PAC_BCN_WIDTH = 3;
const TN IID_CLASS :: TB_PAC_MUX_DELAY_WIDTH = 3;
const TN IID_CLASS :: TB_PAC_GB_DATA_WIDTH = 3;
const TN IID_CLASS :: RPC_GB_DATA_SIZE = 2;
const TN IID_CLASS :: RPC_PAC_GB_PHI_ADDR_SIZE = 4;
const TN IID_CLASS :: RPC_PAC_GB_ETA_ADDR_SIZE = 2;
const TN IID_CLASS :: RPC_PAC_GB_DATA_SIZE = 17;
const TN IID_CLASS :: TB_SORT_MUX_MULTIPL = 8;
const TN IID_CLASS :: TB_SORT_MUX_DATA_WIDTH = 9;
const TN IID_CLASS :: TB_SORT_DATA_WIDTH = 72;
const TN IID_CLASS :: TB_SORT_BCN_WIDTH = 3;
const TN IID_CLASS :: TB_SORT_MUX_DELAY_WIDTH = 3;
const TN IID_CLASS :: TB_BOARD_NUM = 9;
const TS IID_CLASS :: TB_BOARD_IDENTIFIER = "TB";
const TN IID_CLASS :: TB_TIMER_SIZE = 40;
const TN IID_CLASS :: TB_TRIGGER_SEL_L1A = 0;
const TN IID_CLASS :: TB_TRIGGER_SEL_PRETRG0 = 1;
const TN IID_CLASS :: TB_TRIGGER_SEL_PRETRG1 = 2;
const TN IID_CLASS :: TB_TRIGGER_SEL_PRETRG2 = 3;
const TN IID_CLASS :: TB_TRIGGER_SEL_NUM = 4;
const TN IID_CLASS :: TB_TRIGGER_SEL_SIZE = 2;
const TN IID_CLASS :: TC_ADDR_ETA_SIZE = 6;
const TN IID_CLASS :: TC_GB_DATA_SIZE = 21;
const TN IID_CLASS :: TC_SORT_CHAN_OUT_NUM = 2;
const TN IID_CLASS :: TC_SORT_CHAN_NUM = 2;
const TN IID_CLASS :: TC_SORT_MUX_MULTIPL = 2;
const TN IID_CLASS :: TC_SORT_MUX_DATA_WIDTH_HARD = 29;
const TN IID_CLASS :: TC_SORT_MUX_DATA_WIDTH = 24;
const TN IID_CLASS :: TC_SORT_MUX_DATA_WIDTH_FREE = 5;
const TN IID_CLASS :: TC_SORT_TEST_DATA_WIDTH = 48;
const TN IID_CLASS :: TC_SORT_DATA_WIDTH = 42;
const TN IID_CLASS :: TC_SORT_BCN_WIDTH = 5;
const TN IID_CLASS :: TC_SORT_PART_SIZE = 2;
const TN IID_CLASS :: TC_SORT_PART_NUM = 24;
const TN IID_CLASS :: TC_SORT_IIADDR_WIDTH = 16;
const TN IID_CLASS :: TC_SORT_IIDATA_WIDTH = 16;
const TN IID_CLASS :: HSORT_ADDR_WIDTH = 16;
const TN IID_CLASS :: HSORT_DATA_WIDTH = 16;
const TN IID_CLASS :: HSORT_TC_CRATE_NUM = 8;
const TN IID_CLASS :: HSORT_DCHAN_NUM = 16;
const TN IID_CLASS :: HSORT_BCN_SIZE = 8;
const TN IID_CLASS :: HSORT_ADDR_PHI_SIZE = 7;
const TN IID_CLASS :: HSORT_GB_DATA_SIZE = 22;
const TN IID_CLASS :: HSORT_DATA_SIZE = 88;
const TN IID_CLASS :: FSORT_ADDR_WIDTH = 16;
const TN IID_CLASS :: FSORT_DATA_WIDTH = 16;
const TN IID_CLASS :: FSORT_HALF_SORT_NUM = 2;
const TN IID_CLASS :: FSORT_CHAN_NUM = 8;
const TN IID_CLASS :: FSORT_ADDR_PHI_SIZE = 8;
const TN IID_CLASS :: FSORT_GB_DATA_SIZE = 23;
const TN IID_CLASS :: FSORT_DATA_SIZE = 92;
const TN IID_CLASS :: FSORT_BCN_SIZE = 3;
const TN IID_CLASS :: FSORT_GMT_SIZE = 31;
const TN IID_CLASS :: HSORT_DCHAN_DELAY_SIZE = 2;
const TN IID_CLASS :: RECEIVER_CLOCK_MULTIPL = 2;
const TN IID_CLASS :: RECEIVER_FAST_DELAY_WIDTH = 2;
const TN IID_CLASS :: RECEIVER_DELAY_WIDTH = 2;
const TN IID_CLASS :: RECEIVER_ERROR_COUNT_SIZE = 32;
const TN IID_CLASS :: CLOCK_TEST_SIZE = 4;
const TN IID_CLASS :: DAQ_DIAG_SIN_DATA_SIZE = 768;
const TN IID_CLASS :: DAQ_DIAG_DATA_SIZE = 966;
const TN IID_CLASS :: DAQ_DIAG_DATA_DELAY_SIZE = 8;
const TN IID_CLASS :: DAQ_DIAG_TRIG_NUM = 1;
const TN IID_CLASS :: DAQ_DIAG_TIMER_SIZE = 36;
const TN IID_CLASS :: DAQ_DIAG_MASK_WIDTH = 8;
const TN IID_CLASS :: DAQ_DIAG_ADDR_WIDTH = 8;
const TN IID_CLASS :: DAQ_DIAG_ADDR_POS = 256;
const TN IID_CLASS :: DAQ_DIAG_MEM_DATA_SIZE = 971;
const TN IID_CLASS :: DAQ_MEM_ADDR_SIZE = 14;
const TN IID_CLASS :: RATE_DIAG_DATA_SIZE = 32;
const TN IID_CLASS :: RATE_DIAG_COUNT_SIZE = 24;
const TN IID_CLASS :: RATE_MEM_ADDR_SIZE = 6;
const TN IID_CLASS :: PULSE_POS_NUM = 256;
const TN IID_CLASS :: PULSE_POS_SIZE = 8;
const TN IID_CLASS :: PULSE_INDATA_SIZE = 672;
const TN IID_CLASS :: PULSE_DATA_SIZE = 672;
const TN IID_CLASS :: PULSE_MEM_ADDR_SIZE = 14;
const TN IID_CLASS :: HSORT_TIMER_SIZE = 40;
const TN IID_CLASS :: HSORT_DELAY_SIZE = 7;
const TN IID_CLASS :: HSORT_TRIG_COUNT_SIZE = 32;
const TN IID_CLASS :: TRG_COND_SEL_NONE = 0;
const TN IID_CLASS :: TRG_COND_SEL_L1A = 1;
const TN IID_CLASS :: TRG_COND_SEL_PRETRG0 = 2;
const TN IID_CLASS :: TRG_COND_SEL_PRETRG1 = 3;
const TN IID_CLASS :: TRG_COND_SEL_PRETRG2 = 4;
const TN IID_CLASS :: TRG_COND_SEL_BCN0 = 5;
const TN IID_CLASS :: TRG_COND_SEL_LOCAL = 6;
const TN IID_CLASS :: TRG_COND_SEL_NUM = 7;
const TN IID_CLASS :: TRG_COND_SEL_SIZE = 3;
const TS IID_CLASS :: HSORT_BOARD_IDENTIFIER = "HS";
const TS IID_CLASS :: HSORT_IDENTIFIER = "BS";
const THV IID_CLASS :: HSORT_VERSION = "0001";
const TN IID_CLASS :: QPLL_SELECT_WIDTH = 6;

// parameters
const TN IID_CLASS :: II_ADDR_SIZE  = 16;
const TN IID_CLASS :: II_DATA_SIZE  = 16;
const TN IID_CLASS :: CHECK_SUM = 1394706760;

// interfaces
const TN IID_CLASS :: PAGE_UNIVERSAL = 0;
const TN IID_CLASS :: WORD_CHECKSUM = 1;
const TN IID_CLASS :: WORD_BOARD = 2;
const TN IID_CLASS :: WORD_IDENTIFIER = 3;
const TN IID_CLASS :: WORD_VERSION = 4;
const TN IID_CLASS :: WORD_USER_REG1 = 5;
const TN IID_CLASS :: WORD_USER_REG2 = 6;
const TN IID_CLASS :: WORD_REC_CHAN_ENA = 7;
const TN IID_CLASS :: WORD_REC_FAST_CLK_INV = 8;
const TN IID_CLASS :: WORD_REC_FAST_REG_ADD = 9;
const TN IID_CLASS :: WORD_REC_FAST_DATA_DELAY = 10;
const TN IID_CLASS :: WORD_REC_DATA_DELAY = 11;
const TN IID_CLASS :: WORD_REC_PART_ENA = 12;
const TN IID_CLASS :: WORD_REC_CHECK_ENA = 13;
const TN IID_CLASS :: WORD_REC_CHECK_DATA_ENA = 14;
const TN IID_CLASS :: WORD_REC_TEST_ENA = 15;
const TN IID_CLASS :: WORD_REC_TEST_DATA = 16;
const TN IID_CLASS :: WORD_REC_TEST_OR_DATA = 17;
const TN IID_CLASS :: WORD_REC_ERROR_COUNT = 18;
const TN IID_CLASS :: VECT_STATUS = 19;
const TN IID_CLASS :: BITS_STATUS_PLL_RESET = 20;
const TN IID_CLASS :: BITS_STATUS_PLL_UNLOCK = 21;
const TN IID_CLASS :: BITS_STATUS_TRG_DATA_SEL = 22;
const TN IID_CLASS :: BITS_STATUS_TTC_CLK_INV = 23;
const TN IID_CLASS :: BITS_STATUS_TTC_INIT_ENA = 24;
const TN IID_CLASS :: BITS_STATUS_TTC_RESET = 25;
const TN IID_CLASS :: BITS_STATUS_TTC_READY = 26;
const TN IID_CLASS :: VECT_TEST_CNT = 27;
const TN IID_CLASS :: BITS_TEST_CNT_MAIN = 28;
const TN IID_CLASS :: BITS_TEST_CNT_DES1 = 29;
const TN IID_CLASS :: BITS_TEST_CNT_DES2 = 30;
const TN IID_CLASS :: BITS_TEST_CNT_BCN0 = 31;
const TN IID_CLASS :: WORD_TTC_BROADCAST = 32;
const TN IID_CLASS :: WORD_TTC_DATA = 33;
const TN IID_CLASS :: WORD_TTC_SUBADDR = 34;
const TN IID_CLASS :: WORD_TTC_BCN = 35;
const TN IID_CLASS :: WORD_TTC_EVN = 36;
const TN IID_CLASS :: WORD_TTC_DQ = 37;
const TN IID_CLASS :: WORD_TTC_ID_MODE = 38;
const TN IID_CLASS :: WORD_BCN0_DELAY = 39;
const TN IID_CLASS :: WORD_DATA_TRG_DELAY = 40;
const TN IID_CLASS :: WORD_RATE_TRG_DELAY = 41;
const TN IID_CLASS :: WORD_STAT_TRIG_COUNT = 42;
const TN IID_CLASS :: WORD_DELAY_DCHAN = 43;
const TN IID_CLASS :: VECT_DAQ = 44;
const TN IID_CLASS :: BITS_DAQ_EMPTY = 45;
const TN IID_CLASS :: BITS_DAQ_EMPTY_ACK = 46;
const TN IID_CLASS :: BITS_DAQ_LOST = 47;
const TN IID_CLASS :: BITS_DAQ_LOST_ACK = 48;
const TN IID_CLASS :: BITS_DAQ_TIMER_START = 49;
const TN IID_CLASS :: BITS_DAQ_TIMER_STOP = 50;
const TN IID_CLASS :: BITS_DAQ_TIMER_LOC_ENA = 51;
const TN IID_CLASS :: BITS_DAQ_TIMER_TRIG_SEL = 52;
const TN IID_CLASS :: BITS_DAQ_PROC_REQ = 53;
const TN IID_CLASS :: BITS_DAQ_PROC_ACK = 54;
const TN IID_CLASS :: BITS_DAQ_NON_EMPTY_ENA = 55;
const TN IID_CLASS :: VECT_DAQ_WR = 56;
const TN IID_CLASS :: BITS_DAQ_WR_ADDR = 57;
const TN IID_CLASS :: BITS_DAQ_WR_ACK = 58;
const TN IID_CLASS :: VECT_DAQ_RD = 59;
const TN IID_CLASS :: BITS_DAQ_RD_ADDR = 60;
const TN IID_CLASS :: BITS_DAQ_RD_ACK = 61;
const TN IID_CLASS :: WORD_DAQ_TIMER_LIMIT = 62;
const TN IID_CLASS :: WORD_DAQ_TIMER_COUNT = 63;
const TN IID_CLASS :: WORD_DAQ_TIMER_TRG_DELAY = 64;
const TN IID_CLASS :: WORD_DAQ_DATA_DELAY = 65;
const TN IID_CLASS :: WORD_DAQ_MASK = 66;
const TN IID_CLASS :: VECT_RATE = 67;
const TN IID_CLASS :: BITS_RATE_TIMER_START = 68;
const TN IID_CLASS :: BITS_RATE_TIMER_STOP = 69;
const TN IID_CLASS :: BITS_RATE_TIMER_LOC_ENA = 70;
const TN IID_CLASS :: BITS_RATE_TIMER_TRIG_SEL = 71;
const TN IID_CLASS :: BITS_RATE_PROC_REQ = 72;
const TN IID_CLASS :: BITS_RATE_PROC_ACK = 73;
const TN IID_CLASS :: WORD_RATE_TIMER_LIMIT = 74;
const TN IID_CLASS :: WORD_RATE_TIMER_COUNT = 75;
const TN IID_CLASS :: VECT_PULSER = 76;
const TN IID_CLASS :: BITS_PULSER_OUT_ENA = 77;
const TN IID_CLASS :: BITS_PULSER_OUT_IN_SEL = 78;
const TN IID_CLASS :: BITS_PULSER_TIMER_START = 79;
const TN IID_CLASS :: BITS_PULSER_TIMER_STOP = 80;
const TN IID_CLASS :: BITS_PULSER_TIMER_LOC_ENA = 81;
const TN IID_CLASS :: BITS_PULSER_TIMER_TRIG_SEL = 82;
const TN IID_CLASS :: BITS_PULSER_REPEAT_ENA = 83;
const TN IID_CLASS :: BITS_PULSER_PROC_REQ = 84;
const TN IID_CLASS :: BITS_PULSER_PROC_ACK = 85;
const TN IID_CLASS :: WORD_PULSER_LENGTH = 86;
const TN IID_CLASS :: WORD_PULSER_TIMER_LIMIT = 87;
const TN IID_CLASS :: WORD_PULSER_TIMER_COUNT = 88;
const TN IID_CLASS :: WORD_PULSER_TIMER_TRG_DELAY = 89;
const TN IID_CLASS :: VECT_QPLL = 90;
const TN IID_CLASS :: BITS_QPLL_MODE = 91;
const TN IID_CLASS :: BITS_QPLL_CTRL = 92;
const TN IID_CLASS :: BITS_QPLL_SELECT = 93;
const TN IID_CLASS :: BITS_QPLL_LOCKED = 94;
const TN IID_CLASS :: BITS_QPLL_ERROR = 95;
const TN IID_CLASS :: BITS_QPLL_CLK_LOCK = 96;
const TN IID_CLASS :: AREA_I2C = 97;
const TN IID_CLASS :: AREA_MEM_RATE = 98;
const TN IID_CLASS :: AREA_MEM_DAQ = 99;
const TN IID_CLASS :: AREA_MEM_PULSE = 100;

TMemoryMap* IID_CLASS ::BuildMemoryMap() {
  const TVIIItemDecl declItems[] =
  {
    { VII_WORD, WORD_CHECKSUM, 16, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("CHECKSUM"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_BOARD, 16, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("BOARD"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_IDENTIFIER, 16, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("IDENTIFIER"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_VERSION, 16, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("VERSION"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_USER_REG1, 16, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("USER_REG1"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_USER_REG2, 16, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("USER_REG2"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_CHAN_ENA, 8, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_CHAN_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_FAST_CLK_INV, 24, 16, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_FAST_CLK_INV"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_FAST_REG_ADD, 24, 16, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_FAST_REG_ADD"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_FAST_DATA_DELAY, 32, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_FAST_DATA_DELAY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_DATA_DELAY, 32, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_DATA_DELAY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_PART_ENA, 16, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_PART_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_CHECK_ENA, 16, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_CHECK_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_CHECK_DATA_ENA, 16, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_CHECK_DATA_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_TEST_ENA, 16, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_TEST_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_TEST_DATA, 48, 16, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("REC_TEST_DATA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_TEST_OR_DATA, 24, 16, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("REC_TEST_OR_DATA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_ERROR_COUNT, 32, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_REXTERNAL,VIINameConv("REC_ERROR_COUNT"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_VECT, VECT_STATUS, 0, 0, PAGE_UNIVERSAL, VII_WNOACCESS, VII_RNOACCESS, VIINameConv("STATUS"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_STATUS_PLL_RESET, 1, 1, VECT_STATUS, VII_WACCESS, VII_RINTERNAL,VIINameConv("PLL_RESET"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_STATUS_PLL_UNLOCK, 1, 1, VECT_STATUS, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("PLL_UNLOCK"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_STATUS_TRG_DATA_SEL, 3, 1, VECT_STATUS, VII_WACCESS, VII_RINTERNAL,VIINameConv("TRG_DATA_SEL"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_STATUS_TTC_CLK_INV, 1, 1, VECT_STATUS, VII_WACCESS, VII_RINTERNAL,VIINameConv("TTC_CLK_INV"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_STATUS_TTC_INIT_ENA, 1, 1, VECT_STATUS, VII_WACCESS, VII_RINTERNAL,VIINameConv("TTC_INIT_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_STATUS_TTC_RESET, 1, 1, VECT_STATUS, VII_WACCESS, VII_RINTERNAL,VIINameConv("TTC_RESET"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_STATUS_TTC_READY, 1, 1, VECT_STATUS, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("TTC_READY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_VECT, VECT_TEST_CNT, 0, 0, PAGE_UNIVERSAL, VII_WNOACCESS, VII_RNOACCESS, VIINameConv("TEST_CNT"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_TEST_CNT_MAIN, 4, 1, VECT_TEST_CNT, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("MAIN"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_TEST_CNT_DES1, 4, 1, VECT_TEST_CNT, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("DES1"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_TEST_CNT_DES2, 4, 1, VECT_TEST_CNT, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("DES2"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_TEST_CNT_BCN0, 4, 1, VECT_TEST_CNT, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("BCN0"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_TTC_BROADCAST, 6, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("TTC_BROADCAST"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_TTC_DATA, 8, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("TTC_DATA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_TTC_SUBADDR, 8, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("TTC_SUBADDR"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_TTC_BCN, 12, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("TTC_BCN"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_TTC_EVN, 24, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("TTC_EVN"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_TTC_DQ, 4, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("TTC_DQ"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_TTC_ID_MODE, 16, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("TTC_ID_MODE"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_BCN0_DELAY, 12, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("BCN0_DELAY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_DATA_TRG_DELAY, 7, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("DATA_TRG_DELAY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_RATE_TRG_DELAY, 7, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("RATE_TRG_DELAY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_STAT_TRIG_COUNT, 32, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_REXTERNAL,VIINameConv("STAT_TRIG_COUNT"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_DELAY_DCHAN, 2, 16, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("DELAY_DCHAN"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_VECT, VECT_DAQ, 0, 0, PAGE_UNIVERSAL, VII_WNOACCESS, VII_RNOACCESS, VIINameConv("DAQ"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_EMPTY, 1, 1, VECT_DAQ, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("EMPTY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_EMPTY_ACK, 1, 1, VECT_DAQ, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("EMPTY_ACK"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_LOST, 1, 1, VECT_DAQ, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("LOST"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_LOST_ACK, 1, 1, VECT_DAQ, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("LOST_ACK"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_TIMER_START, 1, 1, VECT_DAQ, VII_WACCESS, VII_RINTERNAL,VIINameConv("TIMER_START"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_TIMER_STOP, 1, 1, VECT_DAQ, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("TIMER_STOP"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_TIMER_LOC_ENA, 1, 1, VECT_DAQ, VII_WACCESS, VII_RINTERNAL,VIINameConv("TIMER_LOC_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_TIMER_TRIG_SEL, 3, 1, VECT_DAQ, VII_WACCESS, VII_RINTERNAL,VIINameConv("TIMER_TRIG_SEL"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_PROC_REQ, 1, 1, VECT_DAQ, VII_WACCESS, VII_RINTERNAL,VIINameConv("PROC_REQ"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_PROC_ACK, 1, 1, VECT_DAQ, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("PROC_ACK"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_NON_EMPTY_ENA, 1, 1, VECT_DAQ, VII_WACCESS, VII_RINTERNAL,VIINameConv("NON_EMPTY_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_VECT, VECT_DAQ_WR, 0, 0, PAGE_UNIVERSAL, VII_WNOACCESS, VII_RNOACCESS, VIINameConv("DAQ_WR"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_WR_ADDR, 8, 1, VECT_DAQ_WR, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("ADDR"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_WR_ACK, 1, 1, VECT_DAQ_WR, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("ACK"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_VECT, VECT_DAQ_RD, 0, 0, PAGE_UNIVERSAL, VII_WNOACCESS, VII_RNOACCESS, VIINameConv("DAQ_RD"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_RD_ADDR, 8, 1, VECT_DAQ_RD, VII_WACCESS, VII_REXTERNAL,VIINameConv("ADDR"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_RD_ACK, 1, 1, VECT_DAQ_RD, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("ACK"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_DAQ_TIMER_LIMIT, 40, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("DAQ_TIMER_LIMIT"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_DAQ_TIMER_COUNT, 40, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("DAQ_TIMER_COUNT"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_DAQ_TIMER_TRG_DELAY, 7, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("DAQ_TIMER_TRG_DELAY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_DAQ_DATA_DELAY, 8, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("DAQ_DATA_DELAY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_DAQ_MASK, 8, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("DAQ_MASK"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_VECT, VECT_RATE, 0, 0, PAGE_UNIVERSAL, VII_WNOACCESS, VII_RNOACCESS, VIINameConv("RATE"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_RATE_TIMER_START, 1, 1, VECT_RATE, VII_WACCESS, VII_RINTERNAL,VIINameConv("TIMER_START"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_RATE_TIMER_STOP, 1, 1, VECT_RATE, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("TIMER_STOP"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_RATE_TIMER_LOC_ENA, 1, 1, VECT_RATE, VII_WACCESS, VII_RINTERNAL,VIINameConv("TIMER_LOC_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_RATE_TIMER_TRIG_SEL, 3, 1, VECT_RATE, VII_WACCESS, VII_RINTERNAL,VIINameConv("TIMER_TRIG_SEL"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_RATE_PROC_REQ, 1, 1, VECT_RATE, VII_WACCESS, VII_RINTERNAL,VIINameConv("PROC_REQ"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_RATE_PROC_ACK, 1, 1, VECT_RATE, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("PROC_ACK"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_RATE_TIMER_LIMIT, 40, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("RATE_TIMER_LIMIT"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_RATE_TIMER_COUNT, 40, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("RATE_TIMER_COUNT"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_VECT, VECT_PULSER, 0, 0, PAGE_UNIVERSAL, VII_WNOACCESS, VII_RNOACCESS, VIINameConv("PULSER"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_OUT_ENA, 1, 1, VECT_PULSER, VII_WACCESS, VII_RINTERNAL,VIINameConv("OUT_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_OUT_IN_SEL, 1, 1, VECT_PULSER, VII_WACCESS, VII_RINTERNAL,VIINameConv("OUT_IN_SEL"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_TIMER_START, 1, 1, VECT_PULSER, VII_WACCESS, VII_RINTERNAL,VIINameConv("TIMER_START"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_TIMER_STOP, 1, 1, VECT_PULSER, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("TIMER_STOP"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_TIMER_LOC_ENA, 1, 1, VECT_PULSER, VII_WACCESS, VII_RINTERNAL,VIINameConv("TIMER_LOC_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_TIMER_TRIG_SEL, 3, 1, VECT_PULSER, VII_WACCESS, VII_RINTERNAL,VIINameConv("TIMER_TRIG_SEL"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_REPEAT_ENA, 1, 1, VECT_PULSER, VII_WACCESS, VII_RINTERNAL,VIINameConv("REPEAT_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_PROC_REQ, 1, 1, VECT_PULSER, VII_WACCESS, VII_RINTERNAL,VIINameConv("PROC_REQ"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_PROC_ACK, 1, 1, VECT_PULSER, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("PROC_ACK"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_PULSER_LENGTH, 8, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("PULSER_LENGTH"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_PULSER_TIMER_LIMIT, 40, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("PULSER_TIMER_LIMIT"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_PULSER_TIMER_COUNT, 40, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("PULSER_TIMER_COUNT"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_PULSER_TIMER_TRG_DELAY, 7, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("PULSER_TIMER_TRG_DELAY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_VECT, VECT_QPLL, 0, 0, PAGE_UNIVERSAL, VII_WNOACCESS, VII_RNOACCESS, VIINameConv("QPLL"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_QPLL_MODE, 1, 1, VECT_QPLL, VII_WACCESS, VII_RINTERNAL,VIINameConv("MODE"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_QPLL_CTRL, 1, 1, VECT_QPLL, VII_WACCESS, VII_RINTERNAL,VIINameConv("CTRL"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_QPLL_SELECT, 6, 1, VECT_QPLL, VII_WACCESS, VII_RINTERNAL,VIINameConv("SELECT"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_QPLL_LOCKED, 1, 1, VECT_QPLL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("LOCKED"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_QPLL_ERROR, 1, 1, VECT_QPLL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("ERROR"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_QPLL_CLK_LOCK, 1, 1, VECT_QPLL, VII_WACCESS, VII_RINTERNAL,VIINameConv("CLK_LOCK"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_AREA, AREA_I2C, 16, 4, PAGE_UNIVERSAL, VII_WACCESS, VII_REXTERNAL,VIINameConv("I2C"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_AREA, AREA_MEM_RATE, 24, 32, PAGE_UNIVERSAL, VII_WACCESS, VII_REXTERNAL,VIINameConv("MEM_RATE"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_AREA, AREA_MEM_DAQ, 971, 256, PAGE_UNIVERSAL, VII_WACCESS, VII_REXTERNAL,VIINameConv("MEM_DAQ"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_AREA, AREA_MEM_PULSE, 672, 256, PAGE_UNIVERSAL, VII_WACCESS, VII_REXTERNAL,VIINameConv("MEM_PULSE"), VII_FUN_UNDEF, VIIDescrConv(" ")}
  };
  const TVIIItem viiItems[] =
  {
    { VII_WORD, 1, -1, 16, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 0, 1}, //WORD_CHECKSUM
    { VII_WORD, 2, -1, 16, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 1, 1}, //WORD_BOARD
    { VII_WORD, 3, -1, 16, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 2, 1}, //WORD_IDENTIFIER
    { VII_WORD, 4, -1, 16, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 3, 1}, //WORD_VERSION
    { VII_WORD, 5, -1, 16, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 4, 1}, //WORD_USER_REG1
    { VII_WORD, 6, -1, 16, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 5, 1}, //WORD_USER_REG2
    { VII_WORD, 7, -1, 8, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 6, 1}, //WORD_REC_CHAN_ENA
    { VII_WORD, 8, -1, 24, 16, VII_WACCESS, -1, VII_RINTERNAL, -1, 7, 2}, //WORD_REC_FAST_CLK_INV
    { VII_WORD, 9, -1, 24, 16, VII_WACCESS, -1, VII_RINTERNAL, -1, 39, 2}, //WORD_REC_FAST_REG_ADD
    { VII_WORD, 10, -1, 32, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 71, 2}, //WORD_REC_FAST_DATA_DELAY
    { VII_WORD, 11, -1, 32, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 73, 2}, //WORD_REC_DATA_DELAY
    { VII_WORD, 12, -1, 16, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 75, 1}, //WORD_REC_PART_ENA
    { VII_WORD, 13, -1, 16, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 76, 1}, //WORD_REC_CHECK_ENA
    { VII_WORD, 14, -1, 16, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 77, 1}, //WORD_REC_CHECK_DATA_ENA
    { VII_WORD, 15, -1, 16, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 78, 1}, //WORD_REC_TEST_ENA
    { VII_WORD, 16, -1, 48, 16, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 79, 3}, //WORD_REC_TEST_DATA
    { VII_WORD, 17, -1, 24, 16, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 127, 2}, //WORD_REC_TEST_OR_DATA
    { VII_WORD, 18, -1, 32, 1, VII_WACCESS, -1, VII_REXTERNAL, -1, 159, 2}, //WORD_REC_ERROR_COUNT
    { VII_BITS, 20, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 161, 0}, //BITS_STATUS_PLL_RESET
    { VII_BITS, 21, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 161, 1}, //BITS_STATUS_PLL_UNLOCK
    { VII_BITS, 22, -1, 3, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 161, 2}, //BITS_STATUS_TRG_DATA_SEL
    { VII_BITS, 23, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 161, 5}, //BITS_STATUS_TTC_CLK_INV
    { VII_BITS, 24, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 161, 6}, //BITS_STATUS_TTC_INIT_ENA
    { VII_BITS, 25, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 161, 7}, //BITS_STATUS_TTC_RESET
    { VII_BITS, 26, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 161, 8}, //BITS_STATUS_TTC_READY
    { VII_BITS, 28, -1, 4, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 162, 0}, //BITS_TEST_CNT_MAIN
    { VII_BITS, 29, -1, 4, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 162, 4}, //BITS_TEST_CNT_DES1
    { VII_BITS, 30, -1, 4, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 162, 8}, //BITS_TEST_CNT_DES2
    { VII_BITS, 31, -1, 4, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 162, 12}, //BITS_TEST_CNT_BCN0
    { VII_WORD, 32, -1, 6, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 163, 1}, //WORD_TTC_BROADCAST
    { VII_WORD, 33, -1, 8, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 164, 1}, //WORD_TTC_DATA
    { VII_WORD, 34, -1, 8, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 165, 1}, //WORD_TTC_SUBADDR
    { VII_WORD, 35, -1, 12, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 166, 1}, //WORD_TTC_BCN
    { VII_WORD, 36, -1, 24, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 167, 2}, //WORD_TTC_EVN
    { VII_WORD, 37, -1, 4, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 169, 1}, //WORD_TTC_DQ
    { VII_WORD, 38, -1, 16, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 170, 1}, //WORD_TTC_ID_MODE
    { VII_WORD, 39, -1, 12, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 171, 1}, //WORD_BCN0_DELAY
    { VII_WORD, 40, -1, 7, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 172, 1}, //WORD_DATA_TRG_DELAY
    { VII_WORD, 41, -1, 7, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 173, 1}, //WORD_RATE_TRG_DELAY
    { VII_WORD, 42, -1, 32, 1, VII_WACCESS, -1, VII_REXTERNAL, -1, 174, 2}, //WORD_STAT_TRIG_COUNT
    { VII_WORD, 43, -1, 2, 16, VII_WACCESS, -1, VII_RINTERNAL, -1, 176, 1}, //WORD_DELAY_DCHAN
    { VII_BITS, 45, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 192, 0}, //BITS_DAQ_EMPTY
    { VII_BITS, 46, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 192, 1}, //BITS_DAQ_EMPTY_ACK
    { VII_BITS, 47, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 192, 2}, //BITS_DAQ_LOST
    { VII_BITS, 48, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 192, 3}, //BITS_DAQ_LOST_ACK
    { VII_BITS, 49, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 192, 4}, //BITS_DAQ_TIMER_START
    { VII_BITS, 50, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 192, 5}, //BITS_DAQ_TIMER_STOP
    { VII_BITS, 51, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 192, 6}, //BITS_DAQ_TIMER_LOC_ENA
    { VII_BITS, 52, -1, 3, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 192, 7}, //BITS_DAQ_TIMER_TRIG_SEL
    { VII_BITS, 53, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 192, 10}, //BITS_DAQ_PROC_REQ
    { VII_BITS, 54, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 192, 11}, //BITS_DAQ_PROC_ACK
    { VII_BITS, 55, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 192, 12}, //BITS_DAQ_NON_EMPTY_ENA
    { VII_BITS, 57, -1, 8, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 193, 0}, //BITS_DAQ_WR_ADDR
    { VII_BITS, 58, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 193, 8}, //BITS_DAQ_WR_ACK
    { VII_BITS, 60, -1, 8, 1, VII_WACCESS, -1, VII_REXTERNAL, -1, 194, 0}, //BITS_DAQ_RD_ADDR
    { VII_BITS, 61, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 194, 8}, //BITS_DAQ_RD_ACK
    { VII_WORD, 62, -1, 40, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 195, 3}, //WORD_DAQ_TIMER_LIMIT
    { VII_WORD, 63, -1, 40, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 198, 3}, //WORD_DAQ_TIMER_COUNT
    { VII_WORD, 64, -1, 7, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 201, 1}, //WORD_DAQ_TIMER_TRG_DELAY
    { VII_WORD, 65, -1, 8, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 202, 1}, //WORD_DAQ_DATA_DELAY
    { VII_WORD, 66, -1, 8, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 203, 1}, //WORD_DAQ_MASK
    { VII_BITS, 68, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 204, 0}, //BITS_RATE_TIMER_START
    { VII_BITS, 69, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 204, 1}, //BITS_RATE_TIMER_STOP
    { VII_BITS, 70, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 204, 2}, //BITS_RATE_TIMER_LOC_ENA
    { VII_BITS, 71, -1, 3, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 204, 3}, //BITS_RATE_TIMER_TRIG_SEL
    { VII_BITS, 72, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 204, 6}, //BITS_RATE_PROC_REQ
    { VII_BITS, 73, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 204, 7}, //BITS_RATE_PROC_ACK
    { VII_WORD, 74, -1, 40, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 205, 3}, //WORD_RATE_TIMER_LIMIT
    { VII_WORD, 75, -1, 40, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 208, 3}, //WORD_RATE_TIMER_COUNT
    { VII_BITS, 77, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 211, 0}, //BITS_PULSER_OUT_ENA
    { VII_BITS, 78, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 211, 1}, //BITS_PULSER_OUT_IN_SEL
    { VII_BITS, 79, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 211, 2}, //BITS_PULSER_TIMER_START
    { VII_BITS, 80, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 211, 3}, //BITS_PULSER_TIMER_STOP
    { VII_BITS, 81, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 211, 4}, //BITS_PULSER_TIMER_LOC_ENA
    { VII_BITS, 82, -1, 3, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 211, 5}, //BITS_PULSER_TIMER_TRIG_SEL
    { VII_BITS, 83, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 211, 8}, //BITS_PULSER_REPEAT_ENA
    { VII_BITS, 84, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 211, 9}, //BITS_PULSER_PROC_REQ
    { VII_BITS, 85, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 211, 10}, //BITS_PULSER_PROC_ACK
    { VII_WORD, 86, -1, 8, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 212, 1}, //WORD_PULSER_LENGTH
    { VII_WORD, 87, -1, 40, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 213, 3}, //WORD_PULSER_TIMER_LIMIT
    { VII_WORD, 88, -1, 40, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 216, 3}, //WORD_PULSER_TIMER_COUNT
    { VII_WORD, 89, -1, 7, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 219, 1}, //WORD_PULSER_TIMER_TRG_DELAY
    { VII_BITS, 91, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 220, 0}, //BITS_QPLL_MODE
    { VII_BITS, 92, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 220, 1}, //BITS_QPLL_CTRL
    { VII_BITS, 93, -1, 6, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 220, 2}, //BITS_QPLL_SELECT
    { VII_BITS, 94, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 220, 8}, //BITS_QPLL_LOCKED
    { VII_BITS, 95, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 220, 9}, //BITS_QPLL_ERROR
    { VII_BITS, 96, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 220, 10}, //BITS_QPLL_CLK_LOCK
    { VII_AREA, 97, -1, 16, 4, VII_WACCESS, -1, VII_REXTERNAL, -1, 224, 1}, //AREA_I2C
    { VII_AREA, 98, -1, 24, 32, VII_WACCESS, -1, VII_REXTERNAL, -1, 256, 2}, //AREA_MEM_RATE
    { VII_AREA, 99, -1, 971, 256, VII_WACCESS, -1, VII_REXTERNAL, -1, 16384, 61}, //AREA_MEM_DAQ
    { VII_AREA, 100, -1, 672, 256, VII_WACCESS, -1, VII_REXTERNAL, -1, 32768, 42} //AREA_MEM_PULSE
  };
  return new TMemoryMap(declItems, sizeof declItems / sizeof declItems[0],
    viiItems, sizeof viiItems / sizeof viiItems[0], II_ADDR_SIZE, II_DATA_SIZE, CHECK_SUM);
}
