library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.std_logic_1164_ktp.all;
use work.tridaq.all;

entity daq_slave is
  port(
    --
    -- global control signals
    resetN			:in TSL;
    clk				:in TSL;
    tclk			:in TSL;
    --
    -- input data
    --
    chamber			:in TSLV(CHAMBER_ADDRESS_SIZE-1 downto 0);
    time			:in TSLV(TIME_PARTITION_CODE_SIZE-1 downto 0);
    partition			:in TSLV(DATA_PARTITION_CODE_SIZE-1 downto 0);
    data			:in TSLV(DATA_PARTITION_SIZE-1 downto 0);
    end_data			:in TSL;
    half_part			:in TSL;
    --
    -- DAQ-master signal control
    --
    triggerN			:in TSL;
    trg_page			:in TSLV(TRIGGER_PAGE_CODE_SIZE-1 downto 0);
    cnt_resetN			:in TSL;
    --
    -- DAQ-slave signal control
    --
    addr_position		:in  TSLV(DATA_PARTITION_CODE_SIZE-1 downto 0);
    addr_page			:in  TSLV(TRIGGER_PAGE_CODE_SIZE-1 downto 0);
    slave_data			:out TSLV(DPM_DATA_SIZE-1 downto 0);
    cnt_data			:out TSLV(DATA_COUNTER_CODE_SIZE-1 downto 0);
    slave_status		:out TSLV(SDAQ_STATUS_SIZE-1 downto 0);
    --
    -- internal interface
    --
    setup_reqN			:in  TSL;
    setup_ackN			:out TSL;
    -- pipeline
    delay			:in  TSLV(DELAY_PIPELINE_SIZE-1 downto 0);
    delay_mem_addr		:in  TSLV(DPM_DELAY_ADDR_SIZE-1 downto 0);
    delay_mem_data_in		:in  TSLV(DPM_DELAY_DATA_SIZE-1 downto 0);
    delay_mem_data_out		:out TSLV(DPM_DELAY_DATA_SIZE-1 downto 0);
    delay_mem_wr		:in  TSL;
    -- data memory
    data_mem_addr		:in  TSLV(DPM_DATA_ADDR_SIZE-1 downto 0);
    data_mem_data_in		:in  TSLV(DPM_DATA_DATA_SIZE-1 downto 0);
    data_mem_data_out		:out TSLV(DPM_DATA_DATA_SIZE-1 downto 0);
    data_mem_wr			:in  TSL;
    -- counter memory
    cnt_mem_addr		:in  TSLV(DPM_CNT_ADDR_SIZE-1 downto 0);
    cnt_mem_data_in		:in  TSLV(DPM_CNT_DATA_SIZE-1 downto 0);
    cnt_mem_data_out		:out TSLV(DPM_CNT_DATA_SIZE-1 downto 0);
    cnt_mem_wr			:in  TSL
  );
end daq_slave;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use work.std_logic_1164_ktp.all;
use work.vcomponent.all;
use work.LPMComponent.all;
use work.tridaq.all;

architecture behaviour of daq_slave is

  type TLinkPipeItem is record
    TrigPage				:TSLV(trg_page'range);
    TrigEna				:TSL;
    PacketValid				:TSL;
    Chamber				:TSLV(chamber'range);
    Time				:TSLV(time'range);
    Partition				:TSLV(partition'range);
    Data				:TSLV(Data'range);
    EndData				:TSL;
    HalfPart				:TSL;
  end record;
  type TLinkPipe is array(TIME_PARTITION_CODE_MAX+1 downto VEC_INDEX_MIN) of TLinkPipeItem;
  --
  -- signal definitions
  signal L,H, clkN			:TSL;
  signal proc_req			:TSL;
  signal proc_ack			:TSL;
  signal proc_ack_pipe			:TSL;
  signal proc_ack_DPMdata		:TSL;
  signal proc_ack_DPMcnt		:TSL;
  signal delay_data_in			:TSLV(LINK_DATA_SIZE-1 downto VEC_INDEX_MIN);
  signal delay_data_out			:TSLV(LINK_DATA_SIZE-1 downto VEC_INDEX_MIN);
  signal trigg_reg			:TSL;
  signal trigg_page_reg			:TSLV(trg_page'range);
  signal link_pipe			:TLinkPipe;
  signal dpm_cnt_reg			:TSLV(TIME_PARTITION_CODE_SIZE-1 downto 0);
  signal dpm_enableN			:TSL;
  signal dpm_data_addr			:TSLV(DPM_ADDR_SIZE-1 downto 0);
  signal dpm_data_in			:TSLV(DPM_DATA_SIZE-1 downto 0);
  signal addr_pos_reg			:TSLV(DATA_PARTITION_CODE_SIZE-1 downto 0);
  signal addr_page_reg			:TSLV(TRIGGER_PAGE_CODE_SIZE-1 downto 0);
  signal addr_dpm			:TSLV(DPM_ADDR_SIZE-1 downto 0);
  signal slave_data_reg,slave_data_sig	:TSLV(DPM_DATA_SIZE-1 downto 0);
  signal cnt_data_reg,cnt_data_sig	:TSLV(DATA_COUNTER_CODE_SIZE-1 downto 0);
  signal cnt_busy_page_reg		:TSLV(2**TRIGGER_PAGE_CODE_SIZE-1 downto 0);
  signal cnt_resetN_reg			:TSL;
  
begin

  L <= '0'; H <= '1'; clkN <= not(clk);

  setup_ackN <= not(proc_ack);
  proc_req <= setup_reqN;
  proc_ack <= proc_ack_pipe or proc_ack_DPMdata;

  --
  -- input link data delay based on DPM with external access
  --
  delay_data_in <=  TSLVtrans(time,      GL(GL_ITEM_TIME))
                ** (TSLVtrans(partition, GL(GL_ITEM_PARTITION))
                ** (TSLVtrans(data,      GL(GL_ITEM_DATA))
                ** (TSLVtrans(chamber,   GL(GL_ITEM_CHAMBER))
                ** (TSLVtrans(end_data,  GL(GL_ITEM_END_DATA))
                **  TSLVtrans(half_part, GL(GL_ITEM_HALF_PART))))));
  pipe: component DPM_PROG_PIPE
  generic map(
    LPM_DATA_WIDTH  => LINK_DATA_SIZE,
    LPM_DELAY_WIDTH => DELAY_PIPELINE_SIZE,
    LPM_MDATA_WIDTH => DPM_DELAY_DATA_SIZE
  )
  port map(
    resetN          => resetN,
    clock           => clk,
    clk_ena         => H,
    delay           => delay,
    data_in         => delay_data_in,
    data_out        => delay_data_out,
    sim_loop        => L,
    proc_req        => proc_req,
    proc_ack        => proc_ack_pipe,
    memory_addr     => delay_mem_addr,
    memory_data_in  => delay_mem_data_in,
    memory_data_out => delay_mem_data_out,
    memory_wr       => delay_mem_wr
  );
  --
  -- data packet selecting with trigger
  --
  process (resetN, clk) is
    variable PacketEmptyVar :TSL;
  begin
    if (resetN='0') then
      trigg_reg <= '0';
      trigg_page_reg <= (others=>'0');
      for index in link_pipe'range loop
        link_pipe(index).TrigPage    <= (others =>'0');
        link_pipe(index).TrigEna     <= '0';
        link_pipe(index).PacketValid <= '0';
        link_pipe(index).Chamber     <= (others =>'0');
        link_pipe(index).Time        <= (others =>'0');
        link_pipe(index).Partition   <= (others =>'0');
        link_pipe(index).Data        <= (others =>'0');
        link_pipe(index).EndData     <= '0';
        link_pipe(index).HalfPart    <= '0';
      end loop;
      dpm_cnt_reg <=  (others=>'0');
      cnt_busy_page_reg <= (others=>'0');

    elsif (clk'event and clk='1') then
      -- data readout from DPM delay
      -- data saving into link pipeline
      PacketEmptyVar:= AND_REDUCE(TSLVResize(delay_data_out, GL(GL_ITEM_PARTITION)))
                       and
	               AND_REDUCE(TSLVResize(delay_data_out, GL(GL_ITEM_DATA)));
      link_pipe(VEC_INDEX_MIN).TrigEna     <= '0';
      link_pipe(VEC_INDEX_MIN).TrigPage    <= (others => '0');
      link_pipe(VEC_INDEX_MIN).PacketValid <= not PacketEmptyVar;
      link_pipe(VEC_INDEX_MIN).Chamber     <= TSLVResize(delay_data_out, GL(GL_ITEM_CHAMBER));
      link_pipe(VEC_INDEX_MIN).Time        <= TSLVResize(delay_data_out, GL(GL_ITEM_TIME));
      link_pipe(VEC_INDEX_MIN).Partition   <= TSLVResize(delay_data_out, GL(GL_ITEM_PARTITION));
      link_pipe(VEC_INDEX_MIN).Data        <= TSLVResize(delay_data_out, GL(GL_ITEM_DATA));
      link_pipe(VEC_INDEX_MIN).EndData     <= TSLconv (delay_data_out, GL(GL_ITEM_END_DATA));
      link_pipe(VEC_INDEX_MIN).HalfPart    <= TSLconv (delay_data_out, GL(GL_ITEM_HALF_PART));
      -- data partitions selecting
      trigg_reg <= not(triggerN);
      trigg_page_reg <= trg_page;
      for index in VEC_INDEX_MIN to TIME_PARTITION_CODE_MAX loop
        if((trigg_reg='1')
           and
	   ((link_pipe(index).Time=(TIME_PARTITION_CODE_MAX-index))
           and
	   (link_pipe(index).PacketValid='1'))
        ) then
	  link_pipe(index+1).TrigPage <= trigg_page_reg;
	  link_pipe(index+1).TrigEna  <= '1';
	else
	  link_pipe(index+1).TrigPage <= link_pipe(index).TrigPage;
	  link_pipe(index+1).TrigEna  <= link_pipe(index).TrigEna;
	end if;
        link_pipe(index+1).PacketValid <= link_pipe(index).PacketValid;
        link_pipe(index+1).Chamber     <= link_pipe(index).Chamber;
        link_pipe(index+1).Time        <= link_pipe(index).Time;
        link_pipe(index+1).Partition   <= link_pipe(index).Partition;
        link_pipe(index+1).Data        <= link_pipe(index).Data;
        link_pipe(index+1).EndData     <= link_pipe(index).EndData;
        link_pipe(index+1).HalfPart    <= link_pipe(index).HalfPart;
      end loop;
      -- data saving to data DPM
      if (((link_pipe(link_pipe'left).TrigEna = '1')
	  and
	  (link_pipe(link_pipe'left-1).TrigEna = '1'))
          and
	  (link_pipe(link_pipe'left).TrigPage = link_pipe(link_pipe'left-1).TrigPage))
      then
	dpm_cnt_reg <= dpm_cnt_reg+1;
      else
	dpm_cnt_reg <= (others => '0');
      end if;
      -- DPMs signal registering
      if (link_pipe(link_pipe'left).TrigEna='1') then
        cnt_busy_page_reg(conv_integer(link_pipe(link_pipe'left).TrigPage))<='1';
      end if;
      if (cnt_resetN_reg='0' and cnt_busy_page_reg(conv_integer(addr_page_reg))='1') then
        cnt_busy_page_reg(conv_integer(addr_page_reg))<='0';
      end if;
    end if;
  end process;

  process (resetN, tclk) is
    variable PacketEmptyVar :TSL;
  begin
    if (resetN='0') then
      addr_pos_reg <=  (others=>'0');
      addr_page_reg <=  (others=>'0');
      slave_data_reg <=  (others=>'0');
      cnt_data_reg <=  (others=>'0');
      cnt_resetN_reg <= '0';

    elsif (tclk'event and tclk='1') then
      -- DPMs signal registering
      addr_pos_reg <=  addr_position;
      addr_page_reg <=  addr_page;
      slave_data_reg <= slave_data_sig;
      cnt_resetN_reg <= cnt_resetN;
      if (cnt_busy_page_reg(conv_integer(addr_page_reg))='1') then
        cnt_data_reg <=  TSLVResize(cnt_data_sig,cnt_data'length);
      else
	cnt_data_reg <= (others => '1'); 
      end if;
    end if;
  end process;

  dpm_data_in <=  TSLVtrans(link_pipe(link_pipe'left).Partition, GL(GL_ITEM_PARTITION))
              ** (TSLVtrans(link_pipe(link_pipe'left).Data,      GL(GL_ITEM_DATA))
              ** (TSLVtrans(link_pipe(link_pipe'left).Chamber,   GL(GL_ITEM_CHAMBER))
              ** (TSLVtrans(link_pipe(link_pipe'left).EndData,   GL(GL_ITEM_END_DATA))
              **  TSLVtrans(link_pipe(link_pipe'left).HalfPart,  GL(GL_ITEM_HALF_PART)))));
  dpm_data_addr <= link_pipe(link_pipe'left).TrigPage & dpm_cnt_reg;
  addr_dpm <=  addr_page_reg & addr_pos_reg;
  --
  -- Slave DPM
  --
  DPMdata: component DPM_PROG
  generic map(
    LPM_DATA_WIDTH  => DPM_DATA_SIZE,
    LPM_ADDR_WIDTH  => DPM_ADDR_SIZE,
    LPM_MDATA_WIDTH => DPM_DATA_DATA_SIZE
  )
  port map(
    addr_in          => dpm_data_addr,
    data_in          => dpm_data_in,
    wr               => clkN,
    wr_ena           => link_pipe(link_pipe'left).TrigEna,
    addr_out         => addr_dpm,
    data_out         => slave_data_sig,
    rd               => H,
    rd_ena           => H,
    simulate         => L,
    proc_req         => proc_req,
    proc_ack         => proc_ack_DPMdata,
    memory_addr      => data_mem_addr,
    memory_data_in   => data_mem_data_in,
    memory_data_out  => data_mem_data_out,
    memory_wr        => data_mem_wr
  );
  --
  DPMcount: component DPM_PROG
  generic map(
    LPM_DATA_WIDTH  => DATA_COUNTER_CODE_SIZE,
    LPM_ADDR_WIDTH  => TRIGGER_PAGE_CODE_SIZE,
    LPM_MDATA_WIDTH => DPM_CNT_DATA_SIZE
  )
  port map(
    addr_in          => link_pipe(link_pipe'left).TrigPage,
    data_in          => dpm_cnt_reg,
    wr               => clkN,
    wr_ena           => link_pipe(link_pipe'left).TrigEna,
    addr_out         => addr_page_reg,
    data_out         => cnt_data_sig,
    rd               => H,
    rd_ena           => H,
    simulate         => L,
    proc_req         => proc_req,
    proc_ack         => proc_ack_DPMcnt,
    memory_addr      => cnt_mem_addr,
    memory_data_in   => cnt_mem_data_in,
    memory_data_out  => cnt_mem_data_out,
    memory_wr        => cnt_mem_wr
  );
  slave_data <= slave_data_reg;
  cnt_data <= cnt_data_reg;
  slave_status <= (others => '1');
  
end behaviour;

