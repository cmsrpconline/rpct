library ieee;
use ieee.std_logic_1164.all;

use work.std_logic_1164_ktp.all;

package LPMComponent is

  component DPM
    generic (
      LPM_DATA_WIDTH		:in natural := 0;
      LPM_ADDR_WIDTH		:in natural := 0;
      INPUT_SYNCHRO		:in boolean := TRUE;
      OUTPUT_SYNCHRO		:in boolean := TRUE
    );
    port(
      wr			:in  TSL := '0';
      wr_ena			:in  TSL := '1';
      addr_in			:in  TSLV(LPM_ADDR_WIDTH-1 downto 0);
      data_in			:in  TSLV(LPM_DATA_WIDTH-1 downto 0);
      rd				:in  TSL := '1';
      rd_ena			:in  TSL := '1';
      addr_out			:in  TSLV(LPM_ADDR_WIDTH-1 downto 0);
      data_out			:out TSLV(LPM_DATA_WIDTH-1 downto 0)
    );
  end component;

  component DPM_UNREG
    generic (
      LPM_DATA_WIDTH		:in natural := 0;
      LPM_ADDR_WIDTH		:in natural := 0
    );
    port(
      wr			:in  TSL := '0';
      wr_ena			:in  TSL := '1';
      addr_in			:in  TSLV(LPM_ADDR_WIDTH-1 downto 0);
      data_in			:in  TSLV(LPM_DATA_WIDTH-1 downto 0);
      rd				:in  TSL := '1';
      rd_ena			:in  TSL := '1';
      addr_out			:in  TSLV(LPM_ADDR_WIDTH-1 downto 0);
      data_out			:out TSLV(LPM_DATA_WIDTH-1 downto 0)
    );
  end component;

  component DPM_PROG is
    generic (
      constant LPM_DATA_WIDTH	:in natural := 0;
      constant LPM_ADDR_WIDTH	:in natural := 0;
      constant LPM_DELAY_LEVEL	:in natural := 0;
      constant LPM_MDATA_WIDTH	:in natural := 0
    );
    port(
      addr_in			:in  TSLV(LPM_ADDR_WIDTH-1 downto 0);
      data_in			:in  TSLV(LPM_DATA_WIDTH-1 downto 0);
      wr			:in  TSL := '0';
      wr_ena			:in  TSL := '0';
      addr_out			:in  TSLV(LPM_ADDR_WIDTH-1 downto 0);
      data_out			:out TSLV(LPM_DATA_WIDTH-1 downto 0);
      rd			:in  TSL := '0';
      rd_ena			:in  TSL := '0';
      simulate			:in  TSL := '0';
      proc_req			:in  TSL := '0';
      proc_ack			:out TSL;
      memory_addr		:in  TSLV(LPM_ADDR_WIDTH+SLVPartAddrExpand(LPM_DATA_WIDTH,LPM_MDATA_WIDTH)-1 downto 0);
      memory_data_in		:in  TSLV(LPM_MDATA_WIDTH-1 downto 0);
      memory_data_out		:out TSLV(LPM_MDATA_WIDTH-1 downto 0);
      memory_wr			:in  TSL := '0'
  );
  end component;

  component DPM_PIPE
    generic (
      constant LPM_DATA_WIDTH	:in natural := 0;
      constant LPM_DELAY_WIDTH	:in natural := 0

    );
    port(
      resetN			:in  TSL := '0';  
      clock			:in  TSL := '0';
      clk_ena			:in  TSL := '1';
      delay			:in  TSLV(LPM_DELAY_WIDTH-1 downto 0);
      data_in			:in  TSLV(LPM_DATA_WIDTH-1 downto 0);
      data_out			:out TSLV(LPM_DATA_WIDTH-1 downto 0)
    );
  end component;

  component DPM_PROG_PIPE
    generic (
      constant LPM_DATA_WIDTH	:in natural := 0;
      constant LPM_DELAY_WIDTH	:in natural := 0;
      constant LPM_MDATA_WIDTH	:in natural := 0
    );
    port(
      resetN			:in  TSL := '0';
      clock			:in  TSL := '0';
      clk_ena			:in  TSL := '1';
      delay			:in  TSLV(LPM_DELAY_WIDTH-1 downto 0);
      data_in			:in  TSLV(LPM_DATA_WIDTH-1 downto 0);
      data_out			:out TSLV(LPM_DATA_WIDTH-1 downto 0);
      sim_loop			:in  TSL := '0';
      proc_req			:in  TSL := '0';
      proc_ack			:out TSL;
      memory_addr		:in  TSLV(LPM_DELAY_WIDTH+SLVPartAddrExpand(LPM_DATA_WIDTH,LPM_MDATA_WIDTH)-1 downto 0);
      memory_data_in		:in  TSLV(LPM_MDATA_WIDTH-1 downto 0);
      memory_data_out		:out TSLV(LPM_MDATA_WIDTH-1 downto 0);
      memory_wr			:in  TSL := '0'
    );
  end component;

  component ALTERA_BUSTRI is
    generic (
      LPM_WIDTH			:natural := 0
    );
    port(
      data_in			:in  TSLV(LPM_WIDTH-1 downto 0);
      data_out			:out TSLV(LPM_WIDTH-1 downto 0);
      data_tri			:inout TSLV(LPM_WIDTH-1 downto 0);
      ena			:in TSL
    );
  end component;

  component ALTERA_TRI is
    port(
      data_in			:in  TSL;
      data_out			:out TSL;
      data_tri			:inout TSL;
      ena			:in TSL
    );
  end component;

  component LPM_PULSE_GENER is
    generic (
      LPM_PULSE_WIDTH		:natural := 0;
      LPM_PULSE_INVERT		:boolean := FALSE
    );
    port(
      resetN			:in  TSL;
      clock			:in  TSL;
      trigger			:in  TSL;
      pulse_out			:out TSL
    );
  end component;

  component LPM_PROG_PULSE_GENER is
    generic (
      LPM_DATA_PULSE_WIDTH	:natural := 0;
      LPM_PULSE_INVERT		:boolean := FALSE
    );
    port(
      resetN			:in  TSL;
      clock			:in  TSL;
      trigger			:in  TSL;
      pulse_len			:in  TSLV(LPM_DATA_PULSE_WIDTH-1 downto 0);
      pulse_out			:out TSL
    );
  end component;

  component LPM_CLOCK_DIVIDER is
    generic (
      LPM_DIVIDE_PAR		:natural := 0;
      LPM_STROBE_MODE		:boolean := TRUE -- else: wave
    );
    port(
      resetN			:in  TSL;
      clock			:in  TSL;
      div_clock			:out TSL
    );
  end component;

  component LPM_CLOCK_PULSER is
    generic (
      LPM_DIVIDE_PAR		:natural := 0;
      LPM_DATA_PULSE_WIDTH	:natural := 0;
      LPM_PULSE_INVERT		:boolean := FALSE
    );
    port(
      resetN			:in  TSL;
      clock			:in  TSL;
      pulse			:out TSL
    );
  end component;

  component LPM_TIMER is
    generic (
      LPM_RANGE_MAX		:natural := 0
    );
    port(
      resetN			:in  TSL;
      clock			:in  TSL;
      clk_ena			:in  TSL;
      init			:in  TSL;
      count			:out TSLV(TVLcreate(LPM_RANGE_MAX)-1 downto 0);
      stop			:out TSL
    );
  end component;

  component LPM_PROG_TIMER is
    generic (
      LPM_DATA_SIZE		:natural := 0
    );
    port(
      resetN			:in  TSL;
      clock			:in  TSL;
      clk_ena			:in  TSL;
      init			:in  TSL;
      limit			:in  TSLV(LPM_DATA_SIZE-1 downto 0);
      count			:out TSLV(LPM_DATA_SIZE-1 downto 0);
      stop			:out TSL
    );
  end component;

  component LPM_MPROG_TIMER is
    generic (
      LPM_DATA_SIZE		:natural := 0;
      LPM_MODULE_SIZE		:natural := 0
  );
    port(
      resetN			:in  TSL;
      clock			:in  TSL;
      clk_ena			:in  TSL;
      init			:in  TSL;
      limit			:in  TSLV(LPM_DATA_SIZE-1 downto 0);
      count			:out TSLV(LPM_DATA_SIZE-1 downto 0);
      stop			:out TSL
    );
  end component;

  component LPM_PULSE_DELAY is
    generic (
      LPM_DELAY_SIZE		:natural := 4
    );
    port(
      resetN			:in  TSL;
      clock			:in  TSL;
      clk_ena			:in  TSL;
      pulse_in			:in  TSL;
      pulse_out			:out TSL;
      limit			:in  TSLV(LPM_DELAY_SIZE-1 downto 0)
    );
  end component;

end LPMComponent;

-------------------------------------------------------------------
-- DPM
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.std_logic_1164_ktp.all;

entity DPM is
  generic (
    constant LPM_DATA_WIDTH	:in natural := 8;
    constant LPM_ADDR_WIDTH	:in natural := 4;
    constant INPUT_SYNCHRO	:in boolean := TRUE;
    constant OUTPUT_SYNCHRO	:in boolean := TRUE
  );
  port(
    wr				:in  TSL := '0';
    wr_ena			:in  TSL := '1';
    addr_in			:in  TSLV(LPM_ADDR_WIDTH-1 downto 0);
    data_in			:in  TSLV(LPM_DATA_WIDTH-1 downto 0);
    rd				:in  TSL := '1';
    rd_ena			:in  TSL := '1';
    addr_out			:in  TSLV(LPM_ADDR_WIDTH-1 downto 0);
    data_out			:out TSLV(LPM_DATA_WIDTH-1 downto 0)
  );
end DPM;

library ieee;
use ieee.std_logic_1164.all;
library lpm;
use lpm.lpm_components.all;
use work.std_logic_1164_ktp.all;

architecture behaviour of DPM is

  constant LPM_INDATA_VAR                  :TS := sel("REGISTERED","UNREGISTERED",INPUT_SYNCHRO);
  constant LPM_OUTDATA_VAR                 :TS := sel("REGISTERED","UNREGISTERED",OUTPUT_SYNCHRO);
  constant LPM_RDADDRESS_CONTROL_VAR       :TS := sel("REGISTERED","UNREGISTERED",INPUT_SYNCHRO);
  constant LPM_WRADDRESS_CONTROL_VAR       :TS := sel("REGISTERED","UNREGISTERED",OUTPUT_SYNCHRO);
  signal   WrEnSig, WrClockSig, WrClkEnSig :TSL;
  signal   RdEnSig, RdClockSig, RdClkEnSig :TSL;

begin
	
  WrEnSig    <= '1'    when INPUT_SYNCHRO  else (wr and wr_ena);
  WrClockSig <= wr     when INPUT_SYNCHRO  else '1';
  WrClkEnSig <= wr_ena when INPUT_SYNCHRO  else '1';

  RdEnSig    <= '1'    when OUTPUT_SYNCHRO else (rd and rd_ena);
  RdClockSig <= rd     when OUTPUT_SYNCHRO else '1';
  RdClkEnSig <= rd_ena when OUTPUT_SYNCHRO else '1';


  mem :component LPM_RAM_DP
    generic map(
      LPM_WIDTH             => LPM_DATA_WIDTH,
      LPM_WIDTHAD           => LPM_ADDR_WIDTH,
      LPM_NUMWORDS          => 2**LPM_ADDR_WIDTH,
      LPM_INDATA            => LPM_INDATA_VAR,
      LPM_OUTDATA           => LPM_OUTDATA_VAR,
      LPM_RDADDRESS_CONTROL => LPM_RDADDRESS_CONTROL_VAR,
      LPM_WRADDRESS_CONTROL => LPM_WRADDRESS_CONTROL_VAR,
      LPM_FILE              => "UNUSED",
      LPM_TYPE              => L_RAM_DP,
      LPM_HINT              => "UNUSED"
    )
    port map(
      RDCLOCK => RdClockSig,
      RDCLKEN => RdClkEnSig,
      RDADDRESS =>addr_out,
      RDEN => RdEnSig,
      DATA => data_in,
      WRADDRESS =>addr_in,
      WREN => WrEnSig,
      WRCLOCK => WrClockSig,
      WRCLKEN => WrClkEnSig,
      Q => data_out
    );

end behaviour;

-------------------------------------------------------------------
-- DPM_UNREG
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.std_logic_1164_ktp.all;

entity DPM_UNREG is
  generic (
    constant LPM_DATA_WIDTH	:in natural := 8;
    constant LPM_ADDR_WIDTH	:in natural := 4
  );
  port(
    wr				:in  TSL := '0';
    wr_ena			:in  TSL := '1';
    addr_in			:in  TSLV(LPM_ADDR_WIDTH-1 downto 0);
    data_in			:in  TSLV(LPM_DATA_WIDTH-1 downto 0);
    rd				:in  TSL := '1';
    rd_ena			:in  TSL := '1';
    addr_out			:in  TSLV(LPM_ADDR_WIDTH-1 downto 0);
    data_out			:out TSLV(LPM_DATA_WIDTH-1 downto 0)
  );
end DPM_UNREG;

library ieee;
use ieee.std_logic_1164.all;
library lpm;
use lpm.lpm_components.all;
use work.std_logic_1164_ktp.all;

architecture behaviour of DPM_UNREG is

  signal   WrEnSig :TSL;
  signal   RdEnSig :TSL;

begin
	
  WrEnSig    <= (wr and wr_ena);
  RdEnSig    <= (rd and rd_ena);

  mem :component LPM_RAM_DP
    generic map(
      LPM_WIDTH             => LPM_DATA_WIDTH,
      LPM_WIDTHAD           => LPM_ADDR_WIDTH,
      LPM_NUMWORDS          => 2**LPM_ADDR_WIDTH,
      LPM_INDATA            => "UNREGISTERED",
      LPM_OUTDATA           => "UNREGISTERED",
      LPM_RDADDRESS_CONTROL => "UNREGISTERED",
      LPM_WRADDRESS_CONTROL => "UNREGISTERED",
      LPM_FILE              => "UNUSED",
      LPM_TYPE              => L_RAM_DP,
      LPM_HINT              => "UNUSED"
    )
    port map(
      RDADDRESS =>addr_out,
      RDEN => RdEnSig,
      DATA => data_in,
      WRADDRESS =>addr_in,
      WREN => WrEnSig,
      Q => data_out
    );

end behaviour;

-------------------------------------------------------------------
-- DPM_PIPE
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.std_logic_1164_ktp.all;

entity DPM_PIPE is
  generic (
    constant LPM_DATA_WIDTH	:in natural := 8;
    constant LPM_DELAY_WIDTH	:in natural := 4
  );
  port(
    resetN			:in  TSL := '0';  
    clock			:in  TSL := '0';
    clk_ena			:in  TSL := '1';
    delay			:in  TSLV(LPM_DELAY_WIDTH-1 downto 0);
    data_in			:in  TSLV(LPM_DATA_WIDTH-1 downto 0);
    data_out			:out TSLV(LPM_DATA_WIDTH-1 downto 0)
  );
end DPM_PIPE;

library ieee;
use ieee.std_logic_1164.all;
library lpm;
use lpm.lpm_components.all;
use work.std_logic_1164_ktp.all;

architecture behaviour of DPM_PIPE is

  signal H :TSL;
  signal count, count_reg :TSLV(delay'range);

begin
	
  process(clock, resetN)
  begin
    if(resetN='0') then
      count <= (others => '0');
      count_reg <= (others => '0');
    elsif(clock'event and clock='1') then
      if(clk_ena='1') then
        count_reg <= count;
        if(count=delay)then
          count <= (others => '0');
        else
          count <= count + 1;
        end if;
      end if;
    end if;
  end process;

  H <= '1';
  mem :component LPM_RAM_DP
    generic map(
      LPM_WIDTH             => LPM_DATA_WIDTH,
      LPM_WIDTHAD           => LPM_DELAY_WIDTH,
      LPM_NUMWORDS          => (2**LPM_DELAY_WIDTH)-1,
      LPM_INDATA            => "REGISTERED",
      LPM_OUTDATA           => "REGISTERED",
      LPM_RDADDRESS_CONTROL => "REGISTERED",
      LPM_WRADDRESS_CONTROL => "REGISTERED",
      LPM_FILE              => "UNUSED",
      LPM_TYPE              => L_RAM_DP,
      LPM_HINT              => "UNUSED"
    )
    port map(
      RDCLOCK => clock,
      RDCLKEN => clk_ena,
      RDADDRESS =>count,
      RDEN => H,
      DATA => data_in,
      WRADDRESS =>count_reg,
      WREN => H,
      WRCLOCK => clock,
      WRCLKEN => clk_ena,
      Q => data_out
    );

end behaviour;

-------------------------------------------------------------------
-- DPM_PROG
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.std_logic_1164_ktp.all;

entity DPM_PROG is
  generic (
    constant LPM_DATA_WIDTH	:in natural := 32;
    constant LPM_ADDR_WIDTH	:in natural := 4;
    constant LPM_DELAY_LEVEL	:in natural := 2;
    constant LPM_MDATA_WIDTH	:in natural := 8
  );
  port(
    addr_in			:in  TSLV(LPM_ADDR_WIDTH-1 downto 0);
    data_in			:in  TSLV(LPM_DATA_WIDTH-1 downto 0);
    wr				:in  TSL := '0';
    wr_ena			:in  TSL := '0';
    addr_out			:in  TSLV(LPM_ADDR_WIDTH-1 downto 0);
    data_out			:out TSLV(LPM_DATA_WIDTH-1 downto 0);
    rd				:in  TSL := '0';
    rd_ena			:in  TSL := '0';
    simulate			:in  TSL := '0';
    proc_req			:in  TSL := '0';
    proc_ack			:out TSL;
    memory_addr			:in  TSLV(LPM_ADDR_WIDTH+SLVPartAddrExpand(LPM_DATA_WIDTH,LPM_MDATA_WIDTH)-1 downto 0);
    memory_data_in		:in  TSLV(LPM_MDATA_WIDTH-1 downto 0);
    memory_data_out		:out TSLV(LPM_MDATA_WIDTH-1 downto 0);
    memory_wr			:in  TSL := '0'
  );
end DPM_PROG;

library ieee;
use ieee.std_logic_1164.all;
library lpm;
use lpm.lpm_components.all;
use work.std_logic_1164_ktp.all;
use work.KTPComponent.all;
use work.LPMComponent.all;

architecture behaviour of DPM_PROG is

  function \vr_arr_init\(vlen, plen :TVL) return TVRV is
    variable vr_arr :TVRV(SLVPartNum(vlen,plen)-1 downto 0);
    variable pos :TVI;
  begin
    pos := VEC_INDEX_MIN;
    for index in VEC_INDEX_MIN to vr_arr'left loop
      vr_arr(index).r := pos;
      vr_arr(index).l := pos+SLVPartSize(vlen,plen,index)-1;
      pos := vr_arr(index).l+1;
    end loop;
    return(vr_arr);
  end function;

  constant DPM_PART_NUM :TP := SLVPartNum(LPM_DATA_WIDTH,LPM_MDATA_WIDTH);
  constant ADDR_EXPAND_SIZE :TVL := SLVPartAddrExpand(LPM_DATA_WIDTH,LPM_MDATA_WIDTH);
  constant vr_arr :TVRV(DPM_PART_NUM-1 downto 0) := \vr_arr_init\(LPM_DATA_WIDTH,LPM_MDATA_WIDTH);

  signal   H, L :TSL;
  signal   wr_del, wr_sig :TSL;
  signal   dpm_wr, dpm_rd, dpm_rd_ena :TSL;
  signal   dpm_wr_ena :TSLV(DPM_PART_NUM-1 downto 0);
  signal   dpm_addr_in, dpm_addr_out :TSLV(addr_in'range);
  signal   dpm_data_in, dpm_data_out :TSLV(data_in'range);
  signal   mem_index :TN range 0 to DPM_PART_NUM-1;

begin
	
  H <= '1'; L <= '0';
	
  delay: KTP_LPM_PULSE
  generic map(
    LPM_WIDTH => maximum(LPM_DELAY_LEVEL,1),
    LPM_DELAY => 0
  )
  port map(
    resetN    => proc_req,
    clk       => wr,
    pulse     => wr_del
  );

  wr_sig <= wr     when LPM_DELAY_LEVEL= 0 else
            wr_del;

  dpm_wr       <= wr_sig                   when proc_req='1' else memory_wr;
  dpm_addr_in  <= addr_in                  when proc_req='1' else memory_addr(LPM_ADDR_WIDTH-1 downto 0);
  dpm_rd       <= rd                       when proc_req='1' else H;
  dpm_rd_ena   <= rd_ena                   when proc_req='1' else H;
  dpm_addr_out <= addr_out                 when proc_req='1' else memory_addr(LPM_ADDR_WIDTH-1 downto 0);
  
  mem_index <= 0                                                                            when (DPM_PART_NUM=1) else
  	       TNconv(memory_addr(ADDR_EXPAND_SIZE+LPM_ADDR_WIDTH-1 downto LPM_ADDR_WIDTH));

  l1:
  for index in 0 to DPM_PART_NUM-1 generate

    dpm_data_in(vr_arr(index).l downto vr_arr(index).r) <= data_in(vr_arr(index).l downto vr_arr(index).r) when proc_req='1' else
                                                           memory_data_in(vr_arr(index).l-vr_arr(index).r downto 0);
    dpm_wr_ena(index)  <= wr_ena and not(simulate) when proc_req='1'    else
		          H                        when mem_index=index else
		          L;

    dpmprog: component DPM_UNREG
      generic map(
        LPM_DATA_WIDTH => SLVPartSize(LPM_DATA_WIDTH,LPM_MDATA_WIDTH,index),
        LPM_ADDR_WIDTH => LPM_ADDR_WIDTH
      )
      port map(
        wr             => dpm_wr,
        wr_ena         => dpm_wr_ena(index),
        addr_in        => dpm_addr_in,
        data_in        => dpm_data_in(vr_arr(index).l downto vr_arr(index).r),
        rd             => dpm_rd,
        rd_ena         => dpm_rd_ena,
        addr_out       => dpm_addr_out,
        data_out       => dpm_data_out(vr_arr(index).l downto vr_arr(index).r)
      );


  end generate;

  process(dpm_data_out,memory_addr,mem_index) is
    variable data_var : TSLV(memory_data_out'range);
  begin
    data_var := (others => '0');
    if (DPM_PART_NUM=1) then
      data_var := dpm_data_out;
    else
     for index in 0 to DPM_PART_NUM-1 loop
       if(mem_index=index) then
         data_var := TSLVResize(SLVNorm(dpm_data_out(vr_arr(index).l downto vr_arr(index).r)),LPM_MDATA_WIDTH);
	 exit;
       end if;
     end loop;
    end if;
    memory_data_out <= data_var;
  end process;

  data_out <= dpm_data_out;
  proc_ack <= proc_req;

end behaviour;

-------------------------------------------------------------------
-- DPM_PROG_PIPE
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.std_logic_1164_ktp.all;

entity DPM_PROG_PIPE is
  generic (
    constant LPM_DATA_WIDTH	:in natural := 8;
    constant LPM_DELAY_WIDTH	:in natural := 8;
    constant LPM_MDATA_WIDTH	:in natural := 4
  );
  port(
    resetN			:in  TSL := '0';
    clock			:in  TSL := '0';
    clk_ena			:in  TSL := '1';
    delay			:in  TSLV(LPM_DELAY_WIDTH-1 downto 0);
    data_in			:in  TSLV(LPM_DATA_WIDTH-1 downto 0);
    data_out			:out TSLV(LPM_DATA_WIDTH-1 downto 0);
    sim_loop			:in  TSL := '0';
    proc_req			:in  TSL := '0';
    proc_ack			:out TSL;
    memory_addr			:in  TSLV(LPM_DELAY_WIDTH+SLVPartAddrExpand(LPM_DATA_WIDTH,LPM_MDATA_WIDTH)-1 downto 0);
    memory_data_in		:in  TSLV(LPM_MDATA_WIDTH-1 downto 0);
    memory_data_out		:out TSLV(LPM_MDATA_WIDTH-1 downto 0);
    memory_wr			:in  TSL := '0'
  );
end DPM_PROG_PIPE;

library ieee;
use ieee.std_logic_1164.all;
library lpm;
use lpm.lpm_components.all;
use work.std_logic_1164_ktp.all;
use work.LPMComponent.all;

architecture behaviour of DPM_PROG_PIPE is

  signal H, inv_clock			:TSL;
  signal pipe_data_out			:TSLV(data_in'range);
  signal data_in_reg, data_out_reg	:TSLV(data_in'range);
  signal count, count_reg		:TSLV(delay'range);
  signal proc_req_reg, sim_loop_reg	:TSL;

begin
	
  process(clock, resetN)
  begin
    if(resetN='0') then
      count <= (others => '0');
      count_reg <= (others => '0');
      data_in_reg <= (others => '0');
      data_out_reg <= (others => '0');
      proc_req_reg <= '0';
      sim_loop_reg <= '0';
    elsif(clock'event and clock='1') then
      proc_req_reg <= proc_req;
      sim_loop_reg <= sim_loop;
      data_in_reg <= data_in;
      if(clk_ena='1') then
        data_out_reg <= pipe_data_out;
        count_reg <= count;
        if((count=delay) or (proc_req_reg='0'))then
          count <= (others => '0');
        else
          count <= count + 1;
        end if;
      end if;
    end if;
  end process;

  H <= '1';
  inv_clock <= not clock;

  pipe: component DPM_PROG
    generic map (
      LPM_DATA_WIDTH	=> LPM_DATA_WIDTH,
      LPM_ADDR_WIDTH	=> LPM_DELAY_WIDTH,
      LPM_MDATA_WIDTH	=> LPM_MDATA_WIDTH
    )
    port map(
      addr_in		=> count_reg,
      data_in		=> data_in_reg,
      wr		=> inv_clock,
      wr_ena		=> H,
      addr_out		=> count,
      data_out		=> pipe_data_out,
      rd		=> H,
      rd_ena		=> H,
      simulate		=> sim_loop_reg,
      proc_req		=> proc_req_reg,
      proc_ack		=> proc_ack,
      memory_addr	=> memory_addr,
      memory_data_in	=> memory_data_in,
      memory_data_out	=> memory_data_out,
      memory_wr		=> memory_wr
  );

  data_out        <= data_out_reg;

end behaviour;

-------------------------------------------------------------------
-- DPM_PROG_FIFO
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.std_logic_1164_ktp.all;

entity DPM_PROG_FIFO is
  generic (
    constant LPM_DATA_WIDTH	:in natural := 8;
    constant LPM_ADDR_WIDTH	:in natural := 5;
    constant LPM_MDATA_WIDTH	:in natural := 4

  );
  port(
    resetN			:in  TSL := '0';
    clock			:in  TSL := '0';
    clk_ena			:in  TSL := '1';
    data_in			:in  TSLV(LPM_DATA_WIDTH-1 downto 0);
    block_end			:in  TSL := '0';
    last			:out TSL;
    full			:out TSL;
    addr_out			:in  TSLV(LPM_ADDR_WIDTH-1 downto 0);
    data_out			:out TSLV(LPM_DATA_WIDTH-1 downto 0);
    empty			:out TSL;
    last_addr_out		:out TSLV(LPM_ADDR_WIDTH-1 downto 0);
    last_addr_out_ena		:in  TSL := '0';
    free_addr_out		:in  TSLV(LPM_ADDR_WIDTH-1 downto 0);
    free_addr_out_wr		:in  TSL := '0';
    free_addr_test		:out  TSLV(LPM_ADDR_WIDTH-1 downto 0);
    sim_loop			:in  TSL := '0';
    proc_req			:in  TSL := '0';
    proc_ack			:out TSL;
    memory_addr			:in  TSLV(LPM_ADDR_WIDTH+SLVPartAddrExpand(LPM_DATA_WIDTH,LPM_MDATA_WIDTH)-1 downto 0);
    memory_data_in		:in  TSLV(LPM_MDATA_WIDTH-1 downto 0);
    memory_data_out		:out TSLV(LPM_MDATA_WIDTH-1 downto 0);
    memory_wr			:in  TSL := '0'
  );
end DPM_PROG_FIFO;

library ieee;
use ieee.std_logic_1164.all;
library lpm;
use lpm.lpm_components.all;
use work.std_logic_1164_ktp.all;
use work.LPMComponent.all;

architecture behaviour of DPM_PROG_FIFO is

  signal H, inv_clock			:TSL;
  signal wr_cnt				:TSLV(LPM_ADDR_WIDTH-1 downto 0);
  signal wr_cnt_next			:TSLV(LPM_ADDR_WIDTH-1 downto 0);
  signal last_reg			:TSL;
  signal full_reg			:TSL;
  signal last_addr_out_ena_reg		:TSL;
  signal last_addr_out_reg		:TSLV(LPM_ADDR_WIDTH-1 downto 0);
  signal last_addr_out_read_reg		:TSLV(LPM_ADDR_WIDTH-1 downto 0);
  signal empty_reg			:TSL;
  signal empty_read_reg			:TSL;
  signal free_addr_out_save_reg		:TSLV(LPM_ADDR_WIDTH-1 downto 0);
  signal free_addr_out_reg		:TSLV(LPM_ADDR_WIDTH-1 downto 0);
  signal free_addr_out_wr_reg		:TSL;
  signal proc_req_reg0,proc_req_reg1	:TSL;
  signal sim_loop_reg 			:TSL;

begin

  H <= '1';
  inv_clock <= not(clock);

  process(clock, resetN)
  begin
    if(resetN='0') then
      wr_cnt <= (others => '0');
      wr_cnt_next <= (others => '0');
      last_reg <= '0';
      full_reg <= '0';
      last_addr_out_reg <= (others => '0');
      last_addr_out_read_reg <= (others => '0');
      free_addr_out_reg <= (others => '0');
      proc_req_reg0 <= '0';
      proc_req_reg1 <= '0';
      sim_loop_reg <= '0';
    elsif(clock'event and clock='1') then
      proc_req_reg0 <= proc_req;
      proc_req_reg1 <= proc_req_reg0;
      sim_loop_reg <= sim_loop;
      if ((clk_ena='1') and (last_reg='0')) then
        wr_cnt <= wr_cnt_next;
        wr_cnt_next <= wr_cnt_next+1;
      else
        wr_cnt_next <= wr_cnt+1;
      end if;
      if (wr_cnt=free_addr_out_reg) then
        if (last_reg='1') then
	  full_reg <= '1';
	  last_reg <= '1';
	else
	  empty_reg <= '1'; 
	end if;
      elsif (wr_cnt_next=free_addr_out_reg) then
        last_reg <= '1';
      else
        full_reg <= '0';
	last_reg <= '0';
	empty_reg <= '0'; 
      end if;
      if (block_end='1') then
	last_addr_out_reg <= wr_cnt; 
      end if;
      if (last_addr_out_ena_reg='0') then
	last_addr_out_read_reg <= last_addr_out_reg; 
	empty_read_reg <= empty_reg;
      end if;
      if (free_addr_out_wr_reg='0') then
	free_addr_out_reg <= free_addr_out_save_reg; 
      end if;
    end if;
  end process;

  process(clock, resetN)
  begin
    if(resetN='0') then
      last_addr_out_ena_reg <= '0';
      free_addr_out_wr_reg <= '0';
    elsif(clock'event and clock='0') then
      last_addr_out_ena_reg <= last_addr_out_ena;
      free_addr_out_wr_reg <= free_addr_out_wr;
    end if;
  end process;

  process(free_addr_out_wr, resetN)
  begin
    if (resetN='0') then
      free_addr_out_save_reg <= (others => '0');
    elsif (free_addr_out_wr'event and free_addr_out_wr='1') then
      free_addr_out_save_reg <= free_addr_out;
    end if;
  end process;
    
  fifo: component DPM_PROG
    generic map (
      LPM_DATA_WIDTH	=> LPM_DATA_WIDTH,
      LPM_ADDR_WIDTH	=> LPM_ADDR_WIDTH,
      LPM_MDATA_WIDTH	=> LPM_MDATA_WIDTH
    )
    port map(
      addr_in		=> wr_cnt,
      data_in		=> data_in,
      wr		=> inv_clock,
      wr_ena		=> clk_ena,
      addr_out		=> addr_out,
      data_out		=> data_out,
      rd		=> H,
      rd_ena		=> H,
      simulate		=> sim_loop_reg,
      proc_req		=> proc_req_reg1,
      proc_ack		=> proc_ack,
      memory_addr	=> memory_addr,
      memory_data_in	=> memory_data_in,
      memory_data_out	=> memory_data_out,
      memory_wr		=> memory_wr
    );

  last_addr_out <= last_addr_out_read_reg;
  empty <= empty_read_reg;
  free_addr_test <= free_addr_out_save_reg;
  last <= last_reg;
  full <= full_reg;

end behaviour;			   


-------------------------------------------------------------------
-- ALTERA ELEMENTS
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.std_logic_1164_ktp.all;

entity ALTERA_BUSTRI is
  generic (
    LPM_WIDTH			:natural
  );
  port(
    data_in			:in  TSLV(LPM_WIDTH-1 downto 0);
    data_out			:out TSLV(LPM_WIDTH-1 downto 0);
    data_tri			:inout TSLV(LPM_WIDTH-1 downto 0);
    ena				:in TSL
  );
end ALTERA_BUSTRI;

library ieee;
use ieee.std_logic_1164.all;
library lpm;
use lpm.lpm_components.all;
use work.std_logic_1164_ktp.all;

architecture behaviour of ALTERA_BUSTRI is

  signal   H			:TSL;

begin

  H <= '1';

  TriGate: LPM_BUSTRI 
    generic map (
      LPM_WIDTH => LPM_WIDTH,
      LPM_TYPE =>  L_BUSTRI,
      LPM_HINT =>  "UNUSED")
    port map (
      DATA =>      data_in,
      ENABLEDT =>  ena,
      ENABLETR =>  H,
      RESULT =>    data_out,
      TRIDATA =>   data_tri
  );
  
end behaviour;			   

library ieee;
use ieee.std_logic_1164.all;
use work.std_logic_1164_ktp.all;
use work.LPMComponent.all;

entity ALTERA_TRI is
  port(
    data_in			:in  TSL;
    data_out			:out TSL;
    data_tri			:inout TSL;
    ena				:in TSL
  );
end ALTERA_TRI;

library ieee;
use ieee.std_logic_1164.all;
use work.std_logic_1164_ktp.all;
use work.LPMComponent.all;

architecture behaviour of ALTERA_TRI is

  signal   InSig		:TSLV(0 downto 0);
  signal   OutSig		:TSLV(0 downto 0);
  signal   TriSig		:TSLV(0 downto 0);

begin

  InSig(0) <= data_in;
  data_out <= OutSig(0);
  data_tri <= TriSig(0);

  BusTri: ALTERA_BUSTRI
    generic map (
      LPM_WIDTH => 1
    )
    port map (
      data_in => InSig,
      data_out =>OutSig,
      data_tri => TriSig,
      ena => ena
  );
  
end behaviour;			   


-------------------------------------------------------------------
-- PRIVATE KTP ELEMENTS
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.std_logic_1164_ktp.all;
use work.LPMComponent.all;

entity LPM_PULSE_GENER is
  generic (
    LPM_PULSE_WIDTH		:natural := 1;
    LPM_PULSE_INVERT		:boolean := FALSE
  );
  port(
    resetN			:in  TSL;
    clock			:in  TSL;
    trigger			:in  TSL;
    pulse_out			:out TSL
  );
end LPM_PULSE_GENER;

architecture behaviour of LPM_PULSE_GENER is

  signal   CountReg		:TN range 0 to LPM_PULSE_WIDTH;
  signal   TrgOutReg		:TSL;

begin

  process(clock, resetN)
  begin
    if(resetN='0') then
      CountReg <= 0;
      TrgOutReg <= '0';
    elsif(clock'event and clock='1') then
      if (CountReg <= 1) then
        if(trigger='1') then
          CountReg <= LPM_PULSE_WIDTH;
	  TrgOutReg <= '1';
	else
          CountReg <= 0;
	  TrgOutReg <= '0';
	end if;
      else
        CountReg <= CountReg - 1;
      end if;
    end if;
  end process;
  
  pulse_out <= TrgOutReg when (LPM_PULSE_INVERT = FALSE) else
               not(TrgOutReg);

end behaviour;			   


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.std_logic_1164_ktp.all;
use work.LPMComponent.all;

entity LPM_PROG_PULSE_GENER is
  generic (
    LPM_DATA_PULSE_WIDTH	:natural := 3;
    LPM_PULSE_INVERT		:boolean := FALSE
  );
  port(
    resetN			:in  TSL;
    clock			:in  TSL;
    trigger			:in  TSL;
    pulse_len			:in  TSLV(LPM_DATA_PULSE_WIDTH-1 downto 0);
    pulse_out			:out TSL
  );
end LPM_PROG_PULSE_GENER;

architecture behaviour of LPM_PROG_PULSE_GENER is

  signal   CountReg		:TSLV(LPM_DATA_PULSE_WIDTH-1 downto 0);
  signal   TrgOutReg		:TSL;

begin

  process(clock, resetN)
  begin
    if(resetN='0') then
      CountReg <= (others => '0');
      TrgOutReg <= '0';
    elsif(clock'event and clock='1') then
      if (CountReg <= TSLVconv(1,LPM_DATA_PULSE_WIDTH)) then
        if(trigger='1') then
          CountReg <= pulse_len;
	  TrgOutReg <= '1';
	else
          CountReg <= (others => '0');
	  TrgOutReg <= '0';
	end if;
      else
        CountReg <= CountReg - 1;
      end if;
    end if;
  end process;
  
  pulse_out <= TrgOutReg when (LPM_PULSE_INVERT = FALSE) else
               not(TrgOutReg);

end behaviour;			   


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.std_logic_1164_ktp.all;
use work.LPMComponent.all;

entity LPM_CLOCK_DIVIDER is
  generic (
    LPM_DIVIDE_PAR		:natural := 4;
    LPM_STROBE_MODE		:boolean := FALSE -- else: wave
  );
  port(
    resetN			:in  TSL;
    clock			:in  TSL;
    div_clock			:out TSL
  );
end LPM_CLOCK_DIVIDER;

architecture behaviour of LPM_CLOCK_DIVIDER is

  signal   CountReg		:TN range 0 to LPM_DIVIDE_PAR-1;
  signal   HCountReg		:TN range 0 to LPM_DIVIDE_PAR/2;
  signal   ClkOutReg		:TSL;

begin

  process(clock, resetN)
  begin
    if(resetN='0') then
      CountReg <= 0;
      HCountReg <= 0;
      ClkOutReg <= '0';
    elsif(clock'event and clock='1') then
      if (CountReg = 0) then
        CountReg <= LPM_DIVIDE_PAR-1;
	HCountReg <= LPM_DIVIDE_PAR/2;
        if (LPM_STROBE_MODE = FALSE) then
          ClkOutReg <= '1';
        end if;
      else
        CountReg <= CountReg - 1;
      end if;
      if (HCountReg /= 0) then
        HCountReg <= HCountReg - 1;
        if (LPM_STROBE_MODE = FALSE and HCountReg = 1) then
          ClkOutReg <= '0';
        end if;
      else
      end if;
      if (LPM_STROBE_MODE = TRUE) then
        ClkOutReg <= TSLconv(CountReg = 0);
      end if;
    end if;
  end process;
  
  div_clock <= ClkOutReg;

end behaviour;			   


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.std_logic_1164_ktp.all;
use work.LPMComponent.all;

entity LPM_CLOCK_PULSER is
  generic (
    LPM_DIVIDE_PAR		:natural := 10;
    LPM_DATA_PULSE_WIDTH	:natural := 3;
    LPM_PULSE_INVERT		:boolean := FALSE
  );
  port(
    resetN			:in  TSL;
    clock			:in  TSL;
    pulse			:out TSL
  );
end LPM_CLOCK_PULSER;

architecture behaviour of LPM_CLOCK_PULSER is

  signal   div_clock		:TSL; 

begin

  divider: LPM_CLOCK_DIVIDER
    generic map (
      LPM_DIVIDE_PAR		=> LPM_DIVIDE_PAR,
      LPM_STROBE_MODE		=> TRUE
    )
    port map (
      resetN			=> resetN,
      clock			=> clock,
      div_clock			=> div_clock
    );

  pulse_gen: LPM_PULSE_GENER
    generic map (
      LPM_PULSE_WIDTH		=> LPM_DATA_PULSE_WIDTH,
      LPM_PULSE_INVERT		=> LPM_PULSE_INVERT 
    )
    port map (
      resetN			=> resetN,
      clock			=> clock,
      trigger			=> div_clock,
      pulse_out			=> pulse
    );

end behaviour;			   


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.std_logic_1164_ktp.all;
use work.LPMComponent.all;

entity LPM_TIMER is
  generic (
    LPM_RANGE_MAX		:natural := 4
  );
  port(
    resetN			:in  TSL;
    clock			:in  TSL;
    clk_ena			:in  TSL;
    init			:in  TSL;
    count			:out TSLV(TVLcreate(LPM_RANGE_MAX)-1 downto 0);
    stop			:out TSL
  );
end LPM_TIMER;

architecture behaviour of LPM_TIMER is

  signal   CountReg		:TN range 0 to LPM_RANGE_MAX;
  signal   StopSig		:TSL;

begin

  StopSig <= TSLconv(CountReg = LPM_RANGE_MAX);
  
  process(clock, resetN)
  begin
    if(resetN='0') then
      CountReg <= 0;
    elsif(clock'event and clock='1') then
      if (clk_ena='1') then
        if (init='1') then
          CountReg <= 0;
        elsif (StopSig='0') then
          CountReg <= CountReg + 1;
        end if;
      end if;
    end if;
  end process;
  
  stop <= StopSig;
  count <= TSLVconv(CountReg,count'length);

end behaviour;			   


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.std_logic_1164_ktp.all;
use work.LPMComponent.all;

entity LPM_PROG_TIMER is
  generic (
    LPM_DATA_SIZE		:natural := 40
  );
  port(
    resetN			:in  TSL;
    clock			:in  TSL;
    clk_ena			:in  TSL;
    init			:in  TSL;
    limit			:in  TSLV(LPM_DATA_SIZE-1 downto 0);
    count			:out TSLV(LPM_DATA_SIZE-1 downto 0);
    stop			:out TSL
  );
end LPM_PROG_TIMER;

architecture behaviour of LPM_PROG_TIMER is

  signal   CountReg		:TSLV(LPM_DATA_SIZE-1 downto 0);
  signal   StopSig		:TSL;

begin

  StopSig <= TSLconv(CountReg = limit);
  
  process(clock, resetN)
  begin
    if(resetN='0') then
      CountReg <= (others =>'0');
    elsif(clock'event and clock='1') then
      if (clk_ena='1') then
        if (init='1') then
          CountReg <= (others =>'0');
        elsif (StopSig='0') then
          CountReg <= CountReg + 1;
	end if;
      end if;
    end if;
  end process;
  
  stop <= StopSig;
  count <= CountReg;

end behaviour;			   


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
use work.std_logic_1164_ktp.all;
use work.LPMComponent.all;

entity LPM_MPROG_TIMER is
  generic (
    LPM_DATA_SIZE		:natural := 40;
    LPM_MODULE_SIZE		:natural := 4
  );
  port(
    resetN			:in  TSL;
    clock			:in  TSL;
    clk_ena			:in  TSL;
    init			:in  TSL;
    limit			:in  TSLV(LPM_DATA_SIZE-1 downto 0);
    count			:out TSLV(LPM_DATA_SIZE-1 downto 0);
    stop			:out TSL
  );
end LPM_MPROG_TIMER;

architecture behaviour of LPM_MPROG_TIMER is

  constant MODELES_NUM		:TN := ((LPM_DATA_SIZE-1)/LPM_MODULE_SIZE)+1;
  constant DATA_SIZE		:TN := MODELES_NUM*LPM_MODULE_SIZE;

  signal   CountSig		:TSLV(DATA_SIZE-1 downto 0);
  signal   LimitSig		:TSLV(DATA_SIZE-1 downto 0);

  signal   ModLimitSig		:TSLV(LPM_MODULE_SIZE-1 downto 0);
  signal   ModInitSig		:TSLV(MODELES_NUM-1 downto 0);
  signal   ModClkEnaSig		:TSLV(MODELES_NUM-1 downto 0);
  signal   ModStopSig		:TSLV(MODELES_NUM-1 downto 0);
  signal   StopSig		:TSL;

begin

  LimitSig <= TSLVresize(limit,DATA_SIZE);
  ModLimitSig <= (others => '1');

  ModClkEnaSig(0) <= clk_ena and (not StopSig);
  
  l1:
  for index in 0 to MODELES_NUM-1 generate
    ModInitSig(index) <= ModStopSig(index) or init;
    time_mod		: LPM_PROG_TIMER
      generic map (
        LPM_DATA_SIZE	=> LPM_MODULE_SIZE
      )
      port map (
        resetN		=> resetN,
        clock		=> clock,
        clk_ena		=> ModClkEnaSig(index),
	init		=> ModInitSig(index),
        limit		=> ModLimitSig,
        count		=> CountSig(index*LPM_MODULE_SIZE+LPM_MODULE_SIZE-1 downto index*LPM_MODULE_SIZE),
        stop		=> ModStopSig(index)
      );
  end generate;

  l2:
  for index in 1 to MODELES_NUM-1 generate
    ModClkEnaSig(index) <= AND_REDUCE(ModStopSig(index-1 downto 0)) and ModClkEnaSig(0);
  end generate;

  StopSig <= TSLconv(CountSig=LimitSig);
  stop <= StopSig;

  count <= CountSig(LPM_DATA_SIZE-1 downto 0);

end behaviour;			   

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.std_logic_1164_ktp.all;
use work.LPMComponent.all;

entity LPM_PULSE_DELAY is
  generic (
    LPM_DELAY_SIZE		:natural := 4
  );
  port(
    resetN			:in  TSL;
    clock			:in  TSL;
    clk_ena			:in  TSL;
    pulse_in			:in  TSL;
    pulse_out			:out TSL;
    limit			:in  TSLV(LPM_DELAY_SIZE-1 downto 0)
  );
end LPM_PULSE_DELAY;

architecture behaviour of LPM_PULSE_DELAY is

  signal   WorkReg		:TSL;
  signal   TimerInitSig		:TSL;
  signal   TimerStopSig		:TSL;

begin

  process(clock, resetN)
  begin
    if(resetN='0') then
      WorkReg <= '0';
    elsif(clock'event and clock='1') then
      if (WorkReg='0' and pulse_in='1') then
        WorkReg <= '1';
      elsif (TimerStopSig='1') then
        WorkReg <= '0';
      end if;
    end if;
  end process;

  TimerInitSig <= not(WorkReg);
  pulse_out <= TimerStopSig and WorkReg;

  timer			: LPM_PROG_TIMER
    generic map (
      LPM_DATA_SIZE	=> LPM_DELAY_SIZE
    )
    port map (
      resetN		=> resetN,
      clock		=> clock,
      clk_ena		=> clk_ena,
      init		=> TimerInitSig,
      limit		=> limit,
      stop		=> TimerStopSig
    );
end behaviour;			   

