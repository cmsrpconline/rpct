library ieee;
use ieee.std_logic_1164.all;

use work.std_logic_1164_ktp.all;

package LPMSynchro is

  component STREAM_DEMUX2 is
    generic (
      LPM_DATA_WIDTH		:in natural := 4
    );
    port(
      resetN			:in  TSL := '0';
      clock			:in  TSL := '0';
      data_in			:in  TSLV(LPM_DATA_WIDTH-1 downto 0);
      phase_in			:in  TSL := '0';
      data_out			:out TSLV((2*LPM_DATA_WIDTH)-1 downto 0);
      clock_quality		:out TSL;
      phase_quality		:out TSL
    );
  end component;

  component CLOCK_SYNCHRO is
    generic (
      LPM_DATA_WIDTH		:in natural := 4;
      INPUT_REGISTERED		:in boolean := TRUE;
      OUTPUT_REGISTERED		:in boolean := TRUE
    );
    port(
      resetN			:in  TSL := '0';
      clock_in			:in  TSL := '0';
      data_in			:in  TSLV(LPM_DATA_WIDTH-1 downto 0);
      clock_out			:in  TSL := '0';
      data_out			:out TSLV(LPM_DATA_WIDTH-1 downto 0);
      phase_out			:out TSL
    );
  end component;

  component WINDOW_SYNCHRO is
    generic (
      LPM_DATA_WIDTH		:in natural := 96;
      OUTPUT_REGISTERED		:in boolean := TRUE
    );
    port(
      resetN			:in  TSL := '0';
      win_open			:in  TSL := '0';
      win_close			:in  TSL := '0';
      data_in			:in  TSLV(LPM_DATA_WIDTH-1 downto 0);
      clock_out			:in  TSL := '0';
      data_out			:out TSLV(LPM_DATA_WIDTH-1 downto 0);
      phase_out			:out TSL
    );
  end component;

  component BCN0_SYNCHRO is
    generic (
      LPM_DATA_WIDTH		:in natural := 4;
      LPM_PIPE_LEN		:in natural := 4;
      INPUT_REGISTERED		:in boolean := TRUE;
      BCN0_REGISTERED		:in boolean := TRUE;
      OUTPUT_REGISTERED		:in boolean := TRUE
    );
    port(
      resetN			:in  TSL := '0';
      clock			:in  TSL := '0';
      data_in			:in  TSLV(LPM_DATA_WIDTH-1 downto 0);
      bcn0_in			:in  TSL := '0';
      bcn0			:in  TSL := '0';
      data_out			:out TSLV(LPM_DATA_WIDTH-1 downto 0);
      pos_out			:out TSLV(TVLcreate(LPM_PIPE_LEN)-1 downto 0);
      synch_out			:out TSL
    );
  end component;

  component FFDELAY is
    port(
      data_in			:in  TSL;
      data_out			:out TSL;
      delay_strobe		:out TSL
    );
  end component;

  component STREAM_DELAY is
    generic (
      LPM_DATA_WIDTH		:in natural := 4
    );
    port(
      data_in			:in  TSLV(LPM_DATA_WIDTH-1 downto 0);
      data_out			:out TSLV(LPM_DATA_WIDTH-1 downto 0);
      delay_strobe		:out TSL
    );
  end component;

  component LPM_TIMER_ENGINE is
    generic (
      LPM_TIMER_SIZE		:in natural := 0
    );
    port(
      resetN			:in  TSL;
      clock			:in  TSL;
      clk_ena_in		:in  TSL;
      clk_ena_out		:out TSL;
      start			:in  TSL;
      stop			:out TSL;
      limit			:in  TSLv(LPM_TIMER_SIZE-1 downto 0);
      count			:out TSLv(LPM_TIMER_SIZE-1 downto 0)
    );
  end component;

  component LPM_FILL_SPS2001_ENGINE is
    generic (
      SPS_START_DELAY_SIZE	:natural := 0;
      SPS_FILL_COUNT_LIMIT	:natural := 0;
      SPS_REVOL_COUNT_SIZE	:natural := 0;
      SPS_ACTIVE_COUNT_SIZE	:natural := 0
    );
    port(
      resetN			:in  TSL;
      clock			:in  TSL;
      start			:in  TSL;
      start_delay		:in  TSLV(SPS_START_DELAY_SIZE-1 downto 0);
      revol_limit		:in  TSLV(SPS_REVOL_COUNT_SIZE-1 downto 0);
      active_limit		:in  TSLV(SPS_ACTIVE_COUNT_SIZE-1 downto 0);
      fill_ena			:out TSL;
      fill_pos			:out TSLV(TVLcreate(SPS_FILL_COUNT_LIMIT)-1 downto 0)
    );
  end component;

end LPMSynchro;


-------------------------------------------------------------------
-- STREAM_DEMUX2
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.std_logic_1164_ktp.all;

entity STREAM_DEMUX2 is
  generic (
    LPM_DATA_WIDTH		:in natural := 4
  );
  port(
    resetN			:in  TSL := '0';
    clock			:in  TSL := '0';
    data_in			:in  TSLV(LPM_DATA_WIDTH-1 downto 0);
    phase_in			:in  TSL := '0';
    data_out			:out TSLV((2*LPM_DATA_WIDTH)-1 downto 0);
    clock_quality		:out TSL;
    phase_quality		:out TSL
  );
end STREAM_DEMUX2;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
library lpm;
use lpm.lpm_components.all;
use work.std_logic_1164_ktp.all;
use work.LPMComponent.all;
use work.LPMSynchro.all;

architecture behaviour of STREAM_DEMUX2 is

  signal   InPosDataReg			:TSLV(data_in'range);
  signal   InInvDataReg			:TSLV(data_in'range);
  signal   InInvPosDataReg		:TSLV(data_in'range);
  signal   OutDataReg			:TSLV(data_out'range);
  signal   InPosPhaseReg		:TSL;
  signal   InInvPhaseReg		:TSL;
  signal   InInvPosPhaseReg		:TSL;

  signal   InTestSig			:TSL;
  signal   InPosTestReg			:TSL;
  signal   InInvTestReg			:TSL;
  signal   InInvPosTestReg		:TSL;
  signal   ClockQualityReg		:TSL;
  signal   PhaseQualityReg		:TSL;


begin

  DelayCmp: component STREAM_DELAY
    generic map(
      LPM_DATA_WIDTH  => LPM_DATA_WIDTH
    )
    port map(
      data_in	      => data_in,
      delay_strobe    => InTestSig
    );

  process(clock, resetN) begin
    if (resetN='0') then
      InInvDataReg     <= (others => '0');
      InInvPhaseReg    <= '0';
      InInvTestReg     <= '0';
    elsif (clock'event and clock='0') then
      InInvDataReg     <= data_in;
      InInvPhaseReg    <= phase_in;
      InInvTestReg     <= InTestSig;
    end if;
  end process;  

  process(clock, resetN)
  
    variable QualityVar			:TSL;

  begin
    if (resetN='0') then
      InPosDataReg     <= (others => '0');
      InInvPosDataReg  <= (others => '0');
      InInvPosPhaseReg <= '0';
      InPosTestReg     <= '0';
      InInvPosTestReg  <= '0';
      ClockQualityReg  <= '0';
      PhaseQualityReg  <= '0';
    elsif (clock'event and clock='1') then
      InPosDataReg     <= data_in;
      InInvPosDataReg  <= InInvDataReg;
      InPosPhaseReg    <= phase_in;
      InInvPosPhaseReg <= InInvPhaseReg;
      OutDataReg       <= SLVMux(InInvPosDataReg,InPosDataReg);
      InPosTestReg     <= InTestSig;
      InInvPosTestReg  <= InInvTestReg;
      ClockQualityReg  <= TSLconv((InInvPosTestReg ='0') and (InPosTestReg = '0'));
      PhaseQualityReg  <= TSLconv((InInvPosPhaseReg = '0') and (InPosPhaseReg = '1'));
    end if;

  end process;

  data_out             <= OutDataReg;
  clock_quality        <= ClockQualityReg;
  phase_quality        <= PhaseQualityReg;

end behaviour;			   

-------------------------------------------------------------------
-- CLOCK_SYNCHRO
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.std_logic_1164_ktp.all;

entity CLOCK_SYNCHRO is
  generic (
    LPM_DATA_WIDTH		:in natural := 4;
    INPUT_REGISTERED		:in boolean := TRUE;
    OUTPUT_REGISTERED		:in boolean := TRUE
  );
  port(
    resetN			:in  TSL := '0';
    clock_in			:in  TSL := '0';
    data_in			:in  TSLV(LPM_DATA_WIDTH-1 downto 0);
    clock_out			:in  TSL := '0';
    data_out			:out TSLV(LPM_DATA_WIDTH-1 downto 0);
    phase_out			:out TSL
  );
end CLOCK_SYNCHRO;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
library lpm;
use lpm.lpm_components.all;
use work.std_logic_1164_ktp.all;
use work.LPMComponent.all;

architecture behaviour of CLOCK_SYNCHRO is

  signal   InPosDataReg			:TSLV(data_in'range);
  signal   InInvDataReg			:TSLV(data_in'range);
  signal   OutPosDataReg		:TSLV(data_in'range);
  signal   OutInvDataReg		:TSLV(data_in'range);
  signal   OutDataReg			:TSLV(data_in'range);
  signal   ClockReg			:TSL;

begin

  l1:if (INPUT_REGISTERED=TRUE) generate
    process(clock_in, resetN) begin
      if (resetN='0') then
        InPosDataReg <= (others => '0');
      elsif (clock_in'event and clock_in='1') then
        InPosDataReg <= data_in;
      end if;
    end process;
  end generate;
  
  l2:if (INPUT_REGISTERED=FALSE) generate
    InPosDataReg <= data_in;
  end generate;

  process(clock_in, resetN) begin
    if (resetN='0') then
      InInvDataReg <= (others => '0');
    elsif (clock_in'event and clock_in='0') then
      InInvDataReg <= InPosDataReg;
    end if;
  end process;  

  process(clock_out, resetN) begin
    if (resetN='0') then
      OutPosDataReg <= (others => '0');
      OutInvDataReg <= (others => '0');
      ClockReg      <= '0';
    elsif (clock_out'event and clock_out='0') then
      OutPosDataReg <= InPosDataReg;
      OutInvDataReg <= InInvDataReg;
      ClockReg      <= clock_in;
    end if;
  end process;
  
  l3:if (OUTPUT_REGISTERED=TRUE) generate
    process(clock_out, resetN) begin
      if (resetN='0') then
        OutDataReg    <= (others => '0');
      elsif (clock_out'event and clock_out='1') then
        if (ClockReg='0') then
          OutDataReg  <= OutPosDataReg;
        else
          OutDataReg  <= OutInvDataReg;
        end if;
      end if;
    end process;
  end generate;

  l4:if (OUTPUT_REGISTERED=FALSE) generate
    OutDataReg  <= OutPosDataReg when ClockReg='0' else OutInvDataReg;
  end generate;

  data_out <= OutDataReg;
  phase_out <= not(ClockReg);

end behaviour;			   

-------------------------------------------------------------------
-- WINDOW_SYNCHRO
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.std_logic_1164_ktp.all;

entity WINDOW_SYNCHRO is
  generic (
    LPM_DATA_WIDTH		:in natural := 96;
    OUTPUT_REGISTERED		:in boolean := TRUE
  );
  port(
    resetN			:in  TSL := '0';
    win_open			:in  TSL := '0';
    win_close			:in  TSL := '0';
    data_in			:in  TSLV(LPM_DATA_WIDTH-1 downto 0);
    clock_out			:in  TSL := '0';
    data_out			:out TSLV(LPM_DATA_WIDTH-1 downto 0);
    phase_out			:out TSL
  );
end WINDOW_SYNCHRO;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
library lpm;
use lpm.lpm_components.all;
use work.std_logic_1164_ktp.all;
use work.LPMSynchro.all;

architecture behaviour of WINDOW_SYNCHRO is

  signal   OpenDataReg		:TSLV(data_in'range);
  signal   CloseDataReg		:TSLV(data_in'range);

begin

  process(win_open, resetN) begin
    if (resetN='0') then
      OpenDataReg <= (others => '0');
    elsif (win_open'event and win_open='1') then
      OpenDataReg <= data_in;
    end if;
  end process;
  
  process(win_close, resetN) begin
    if (resetN='0') then
      CloseDataReg <= (others => '0');
    elsif (win_close'event and win_close='1') then
      CloseDataReg <= data_in and (not OpenDataReg);
    end if;
  end process;
  
  clk_syn: CLOCK_SYNCHRO
    generic map (
      LPM_DATA_WIDTH		=> LPM_DATA_WIDTH,
      INPUT_REGISTERED		=> FALSE,
      OUTPUT_REGISTERED		=> OUTPUT_REGISTERED
    )
    port map (
      resetN			=> resetN,
      clock_in			=> win_close,
      data_in			=> CloseDataReg,
      clock_out			=> clock_out,
      data_out			=> data_out,
      phase_out			=> phase_out
    );

end behaviour;			   

-------------------------------------------------------------------
-- BCN0_SYNCHRO
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.std_logic_1164_ktp.all;

entity BCN0_SYNCHRO is
  generic (
    LPM_DATA_WIDTH		:in natural := 4;
    LPM_PIPE_LEN		:in natural := 4;
    INPUT_REGISTERED		:in boolean := TRUE;
    BCN0_REGISTERED		:in boolean := TRUE;
    OUTPUT_REGISTERED		:in boolean := TRUE
  );
  port(
    resetN			:in  TSL := '0';
    clock			:in  TSL := '0';
    data_in			:in  TSLV(LPM_DATA_WIDTH-1 downto 0);
    bcn0_in			:in  TSL := '0';
    bcn0			:in  TSL := '0';
    data_out			:out TSLV(LPM_DATA_WIDTH-1 downto 0);
    pos_out			:out TSLV(TVLcreate(LPM_PIPE_LEN)-1 downto 0);
    synch_out			:out TSL
  );
end BCN0_SYNCHRO;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
library lpm;
use lpm.lpm_components.all;
use work.std_logic_1164_ktp.all;
use work.LPMComponent.all;

architecture behaviour of BCN0_SYNCHRO is

  subtype  TDataVec             is TSLV(data_in'range);
  type     TDataArr		is array (LPM_PIPE_LEN-1 downto 0) of TDataVec;

  procedure \sel\(signal BCN0     :in  TSL;
                  signal BCN0In   :in  TSL;
                  signal BCN0Pipe :in  TSLV;
                  signal DataIn   :in  TSLV;
		  signal DataPipe :in  TDataArr;
		  signal DataOut  :out TSLV;
		  signal PosOut   :out TSLV;
		  signal SynchOut :out TSL) is
  begin
    if(BCN0='1') then
      for index in BCN0Pipe'range loop
        if (BCN0Pipe(index)='1') then
          DataOut  <= DataPipe(index);
	  PosOut   <= TSLVconv(index+1,pos_out'length);
	  SynchOut <= '1';
	  return;
        end if;
      end loop;
    end if;
    DataOut  <= DataIn;
    PosOut   <= TSLVconv(0,pos_out'length);
    SynchOut <= BCN0 and BCN0In;
  end procedure;

  signal   DataInReg		:TSLV(data_in'range);
  signal   BCN0InReg		:TSL;
  signal   BCN0Reg		:TSL;
  signal   BCN0InPipe		:TSLV(LPM_PIPE_LEN-1 downto 0);
  signal   DataInPipe		:TDataArr;
  signal   DataOutSig		:TSLV(data_in'range);
  signal   DataOutReg		:TSLV(data_in'range);
  signal   PosOutReg		:TSLV(pos_out'range);
  signal   SynchOutReg		:TSL;

begin

  l1:if (INPUT_REGISTERED=TRUE) generate
    process(clock, resetN) begin
      if (resetN='0') then
        DataInReg <= (others => '0');
        BCN0InReg <= '0';
      elsif (clock'event and clock='1') then
        DataInReg <= data_in;
	BCN0InReg <= bcn0_in;
      end if;
    end process;
  end generate;
  
  l2:if (INPUT_REGISTERED=FALSE) generate
    DataInReg <= data_in;
    BCN0InReg <= bcn0_in;
  end generate;

  l3:if (BCN0_REGISTERED=TRUE) generate
    process(clock, resetN) begin
      if (resetN='0') then
        BCN0Reg <= '0';
      elsif (clock'event and clock='1') then
        BCN0Reg <= bcn0;
      end if;
    end process;
  end generate;
  
  l4:if (INPUT_REGISTERED=FALSE) generate
    BCN0Reg <= bcn0;
  end generate;

  process(clock, resetN)
    variable PosOutVar		:TSLV(pos_out'range);
    variable SynchOutVar	:TSL;
  begin
    if (resetN='0') then
      BCN0InPipe  <= (others => '0');
      DataInPipe  <= (TDataArr'range => (others => '0'));
      PosOutReg   <= (others => '0');
      SynchOutReg <= '0';
    elsif (clock'event and clock='1') then
      BCN0InPipe(0) <= BCN0InReg;
      BCN0InPipe(LPM_PIPE_LEN-1 downto 1) <= BCN0InPipe(LPM_PIPE_LEN-2 downto 0);
      DataInPipe(0) <= DataInReg;
      DataInPipe(LPM_PIPE_LEN-1 downto 1) <= DataInPipe(LPM_PIPE_LEN-2 downto 0);
      if(BCN0Reg='1') then
	PosOutVar   := TSLVconv(0,pos_out'length);
	SynchOutVar := BCN0InReg;
        for index in BCN0InPipe'range loop
          if (BCN0InPipe(index)='1') then
	    PosOutVar   := TSLVconv(index+1,pos_out'length);
	    SynchOutVar := '1';
	    exit;
          end if;
        end loop;
        PosOutReg   <= PosOutVar;
        SynchOutReg <= SynchOutVar;
      end if;
    end if;
  end process;
  
  DataOutSig <= DataInReg when (TNconv(PosOutReg)=0) else DataInPipe(TNconv(PosOutReg)-1); 
  
  l5:if (OUTPUT_REGISTERED=TRUE) generate
    process(clock, resetN) begin
      if (resetN='0') then
        DataOutReg  <= (others => '0');
      elsif (clock'event and clock='1') then
        DataOutReg  <= DataOutSig;
      end if;
    end process;
  end generate;

  l6:if (OUTPUT_REGISTERED=FALSE) generate
    DataOutReg  <= DataOutSig;
  end generate;

  data_out  <= DataOutReg;
  pos_out   <= PosOutReg;
  synch_out <= SynchOutReg;

end behaviour;			   

-------------------------------------------------------------------
-- FFDELAY
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.std_logic_1164_ktp.all;

entity FFDELAY is
  port(
    data_in			:in  TSL;
    data_out			:out TSL;
    delay_strobe		:out TSL
  );
end FFDELAY;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
library lpm;
use work.std_logic_1164_ktp.all;
use work.LPMComponent.all;

architecture behaviour of FFDELAY is

  signal   PosReg		:TSL;
  signal   NegReg		:TSL;
  signal   ResetSig		:TSL;

begin

  process(ResetSig, data_in)
  begin
    if(ResetSig='0') then
      NegReg <= '0';
    elsif(data_in'event and data_in='0') then
      NegReg <= '1';
    end if;
  end process;

  process(ResetSig, data_in)
  begin
    if(ResetSig='0') then
      PosReg <= '0';
    elsif(data_in'event and data_in='1') then
      PosReg <= '1';
    end if;
  end process;

  ResetSig <= not (NegReg or PosReg);

  delay_strobe <= NegReg or PosReg;
  data_out <= (NegReg or PosReg) xor data_in;

end behaviour;			   

-------------------------------------------------------------------
-- STREAM_DELAY
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.std_logic_1164_ktp.all;

entity STREAM_DELAY is
  generic (
    LPM_DATA_WIDTH		:in natural := 4
  );
  port(
    data_in			:in  TSLV(LPM_DATA_WIDTH-1 downto 0);
    data_out			:out TSLV(LPM_DATA_WIDTH-1 downto 0);
    delay_strobe		:out TSL
  );
end STREAM_DELAY;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use work.std_logic_1164_ktp.all;
use work.LPMSynchro.all;

architecture behaviour of STREAM_DELAY is

  signal   StrobeSig		:TSLV(LPM_DATA_WIDTH-1 downto 0);

begin

  l1:for index in LPM_DATA_WIDTH-1 downto 0 generate
    FFDelayCmp: component FFDELAY
      port map(
        data_in	     => data_in(index),
        data_out     => data_out(index),
        delay_strobe => StrobeSig(index)
      );
  end generate;
  
  delay_strobe <= OR_REDUCE(StrobeSig);

end behaviour;			   

-------------------------------------------------------------------
-- STREAM_DELAY
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.std_logic_1164_ktp.all;

entity LPM_TIMER_ENGINE is
  generic (
    LPM_TIMER_SIZE		:in natural := 4
  );
  port(
    resetN			:in  TSL;
    clock			:in  TSL;
    clk_ena_in			:in  TSL;
    clk_ena_out			:out TSL;
    start			:in  TSL;
    stop			:out TSL;
    limit			:in  TSLv(LPM_TIMER_SIZE-1 downto 0);
    count			:out TSLv(LPM_TIMER_SIZE-1 downto 0)
  );
end LPM_TIMER_ENGINE;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use work.std_logic_1164_ktp.all;
use work.LPMComponent.all;
use work.LPMSynchro.all;

architecture behaviour of LPM_TIMER_ENGINE is

  signal   ClkEnaOutReg		:TSL;
  signal   TimerClkEnaReg	:TSL;
  signal   TimerInitReg		:TSL;
  signal   TimerStopSig		:TSL;
  signal   TimerStopReg		:TSL;
  
begin

  process(resetN, clock) is
  begin
    if(resetN='0') then
      ClkEnaOutReg   <= '0';
      TimerClkEnaReg <= '0';
      TimerInitReg   <= '0';
    elsif(clock'event and clock='1') then
      TimerStopReg   <= TimerStopSig;
      if (start='0') then
        ClkEnaOutReg   <= '0';
        TimerClkEnaReg <= '1';
        TimerInitReg   <= '1';
      else
        TimerInitReg   <= '0';
        if (TimerStopSig='0') then
          ClkEnaOutReg   <= clk_ena_in;
          TimerClkEnaReg <= clk_ena_in;
	else
          ClkEnaOutReg   <= '0';
          TimerClkEnaReg <= '0';
	end if;
      end if;
    end if;
  end process;

  Timer: LPM_PROG_TIMER
    generic map(
      LPM_DATA_SIZE	=> LPM_TIMER_SIZE
    )
    port map(
      resetN		=> resetN,
      clock		=> clock,
      clk_ena		=> TimerClkEnaReg,
      init		=> TimerInitReg,
      limit		=> limit,
      count		=> count,
      stop		=> TimerStopSig
    );
    
  clk_ena_out <= ClkEnaOutReg;
  stop <= TimerStopReg;

end behaviour;			   

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.std_logic_1164_ktp.all;

entity LPM_FILL_SPS2001_ENGINE is
  generic (
    SPS_START_DELAY_SIZE	:natural := 2;
    SPS_FILL_COUNT_LIMIT	:natural := 4;
    SPS_REVOL_COUNT_SIZE	:natural := 4;
    SPS_ACTIVE_COUNT_SIZE	:natural := 3
  );
  port(
    resetN			:in  TSL;
    clock			:in  TSL;
    start			:in  TSL;
    start_delay			:in  TSLV(SPS_START_DELAY_SIZE-1 downto 0);
    revol_limit			:in  TSLV(SPS_REVOL_COUNT_SIZE-1 downto 0);
    active_limit		:in  TSLV(SPS_ACTIVE_COUNT_SIZE-1 downto 0);
    fill_ena			:out TSL;
    fill_pos			:out TSLV(TVLcreate(SPS_FILL_COUNT_LIMIT)-1 downto 0)
  );
end LPM_FILL_SPS2001_ENGINE;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use work.std_logic_1164_ktp.all;
use work.LPMComponent.all;
use work.LPMSynchro.all;

architecture behaviour of LPM_FILL_SPS2001_ENGINE is

  constant SPS_FILL_COUNT_SIZE : TN := TVLcreate(SPS_FILL_COUNT_LIMIT);

  type     TSpsFillMachine is
           (
             SpsFillMachine_Idle,
             SpsFillMachine_Delay,
             SpsFillMachine_Fill,
             SpsFillMachine_Wait
	   );

  signal   H				:TSL;
  signal   SpsFillMachineCurState	:TSpsFillMachine;
  signal   SpsFillMachineNewState	:TSpsFillMachine;

  signal   SpsStartDelCountInitSig	:TSL;
  signal   SpsStartDelCountStopSig	:TSL;
  signal   SpsFillCountInitSig		:TSL;
  signal   SpsFillCountStopSig		:TSL;
  signal   SpsFillCountDataSig		:TSLV(SPS_FILL_COUNT_SIZE-1 downto 0);
  signal   SpsRevolCountInitSig		:TSL;
  signal   SpsRevolCountStopSig		:TSL;
  signal   SpsActiveCountEnaSig		:TSL;
  signal   SpsActiveCountInitSig	:TSL;
  signal   SpsActiveCountStopSig	:TSL;

begin

  H <= '1';

  SpsStartDelCount: LPM_PROG_TIMER
    generic map(
      LPM_DATA_SIZE	=> SPS_START_DELAY_SIZE
    )
    port map(
      resetN		=> resetN,
      clock		=> clock,
      clk_ena		=> H,
      init		=> SpsStartDelCountInitSig,
      limit		=> start_delay,
      stop		=> SpsStartDelCountStopSig
    );

  SpsFillCount: LPM_TIMER
    generic map(
      LPM_RANGE_MAX	=> SPS_FILL_COUNT_LIMIT
    )
    port map(
      resetN		=> resetN,
      clock		=> clock,
      clk_ena		=> H,
      init		=> SpsFillCountInitSig,
      count		=> SpsFillCountDataSig,
      stop		=> SpsFillCountStopSig
    );

  SpsRevolCount: LPM_PROG_TIMER
    generic map(
      LPM_DATA_SIZE	=> SPS_REVOL_COUNT_SIZE
    )
    port map(
      resetN		=> resetN,
      clock		=> clock,
      clk_ena		=> H,
      init		=> SpsRevolCountInitSig,      
      limit		=> revol_limit,
      stop		=> SpsRevolCountStopSig
    );

  SpsActivesCount: LPM_PROG_TIMER
    generic map(
      LPM_DATA_SIZE	=> SPS_ACTIVE_COUNT_SIZE
    )
    port map(
      resetN		=> resetN,
      clock		=> clock,
      clk_ena		=> SpsActiveCountEnaSig,
      init		=> SpsActiveCountInitSig,      
      limit		=> active_limit,
      stop		=> SpsActiveCountStopSig
    );
  
  process(resetN, clock) is
  begin
    if(resetN='0') then
      SpsFillMachineCurState <= SpsFillMachine_Idle;
    elsif(clock'event and clock='1') then
      SpsFillMachineCurState <= SpsFillMachineNewState;
    end if;
  end process;

  process(SpsFillMachineCurState,
          start,
          SpsStartDelCountStopSig,
	  SpsFillCountStopSig,
	  SpsRevolCountStopSig,
	  SpsActiveCountStopSig) is
  begin
    case SpsFillMachineCurState is
      when SpsFillMachine_Idle =>
        SpsFillCountInitSig        <= '1';
        SpsRevolCountInitSig       <= '1';
        SpsActiveCountEnaSig       <= '1';
        SpsActiveCountInitSig      <= '1';
	if (start='0') then
          SpsStartDelCountInitSig  <= '1';
	  SpsFillMachineNewState   <= SpsFillMachine_Idle;
	else
          SpsStartDelCountInitSig  <= '0';
	  SpsFillMachineNewState   <= SpsFillMachine_Delay;
        end if;
      when SpsFillMachine_Delay =>
        SpsFillCountInitSig        <= '1';
        SpsRevolCountInitSig       <= '1';
        SpsActiveCountEnaSig       <= '1';
        SpsActiveCountInitSig      <= '1';
        if (SpsStartDelCountStopSig='0') then
          SpsStartDelCountInitSig  <= '0';
	  SpsFillMachineNewState   <= SpsFillMachine_Delay;
	else
          SpsStartDelCountInitSig  <= '1';
	  SpsFillMachineNewState   <= SpsFillMachine_Fill;
	end if;
      when SpsFillMachine_Fill =>
        SpsStartDelCountInitSig    <= '1';
        SpsRevolCountInitSig       <= '0';
        SpsActiveCountEnaSig       <= '0';
        SpsActiveCountInitSig      <= '0';
	if (SpsFillCountStopSig='0') then
	  SpsFillMachineNewState   <= SpsFillMachine_Fill;
          SpsFillCountInitSig      <= '0';
	else
	  SpsFillMachineNewState   <= SpsFillMachine_Wait;
          SpsFillCountInitSig      <= '1';
	end if;
      when SpsFillMachine_Wait =>
        SpsStartDelCountInitSig    <= '1';
        SpsFillCountInitSig        <= '1';
        SpsActiveCountInitSig      <= '0';
        if (SpsRevolCountStopSig='0') then
          SpsFillMachineNewState   <= SpsFillMachine_Wait;
          SpsRevolCountInitSig     <= '0';
          SpsActiveCountEnaSig     <= '0';
	else
          SpsRevolCountInitSig     <= '1';
          SpsActiveCountEnaSig     <= '1';
          if (SpsActiveCountStopSig='0') then
	    SpsFillMachineNewState <= SpsFillMachine_Fill;
	  else
	    SpsFillMachineNewState <= SpsFillMachine_Idle;
	  end if;
	end if;
    end case;
  end process;

  fill_ena <= not(SpsFillCountInitSig) or SpsFillCountStopSig;
  fill_pos <= SpsFillCountDataSig;

end behaviour;			   
