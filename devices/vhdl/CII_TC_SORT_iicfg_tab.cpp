/***********************************************************************\
*                                                                       *
* This file was created by Component Internal Interface Engine software *
*  Copyright(c) 2000-2006 by Krzysztof Pozniak (pozniak@ise.pw.edu.pl)  *
*                           All Rights Reserved.                        *
*                                                                       *
\***********************************************************************/


// constants
const TN IID_CLASS :: LHC_CLK_FREQ = 40000000;
const TN IID_CLASS :: TTC_BCN_EVT_WIDTH = 12;
const TN IID_CLASS :: TTC_BCN_WIDTH = 12;
const TN IID_CLASS :: TTC_EVN_WIDTH = 24;
const TN IID_CLASS :: TTC_BROADCAST1_WIDTH = 4;
const TN IID_CLASS :: TTC_BROADCAST2_WIDTH = 2;
const TN IID_CLASS :: TTC_BROADCAST_SIZE = 6;
const TN IID_CLASS :: TTC_DOUT_ID_WIDTH = 8;
const TN IID_CLASS :: TTC_SUBADDR_WIDTH = 8;
const TN IID_CLASS :: TTC_DQ_WIDTH = 4;
const TN IID_CLASS :: TTC_ID_MODE_WIDTH = 16;
const TN IID_CLASS :: RPC_FEBRE11_DATA_WIDTH = 128;
const TN IID_CLASS :: RPC_FEBRE11_TEST_WIDTH = 32;
const TN IID_CLASS :: RPC_FEBSTD_DATA_WIDTH = 96;
const TN IID_CLASS :: RPC_FEBSTD_TEST_WIDTH = 24;
const TN IID_CLASS :: RPC_LB_DATA_PART_WIDTH = 8;
const TN IID_CLASS :: RPC_LB_TIME_MAX = 7;
const TN IID_CLASS :: RPC_LB_CHAMBER_MAX = 2;
const TN IID_CLASS :: RPC_LB_CHAMBER_NUM = 3;
const TN IID_CLASS :: RPC_LB_FAST_DATA_WIDTH = 5;
const TN IID_CLASS :: RPC_LB_TEST_PART_WIDTH = 8;
const TN IID_CLASS :: GOL_DATA_SIZE = 32;
const TN IID_CLASS :: RPC_LB_TEST_PART_NUM = 4;
const TN IID_CLASS :: RPC_LBRE11_DATA_POS_MAX = 15;
const TN IID_CLASS :: RPC_LBRE11_DATA_POS_WIDTH = 4;
const TN IID_CLASS :: RPC_LBRE11_TIME_WIDTH = 3;
const TN IID_CLASS :: RPC_LBRE11_CHAMBER_WIDTH = 2;
const TN IID_CLASS :: RPC_LBRE11_CODE_WIDTH = 17;
const TN IID_CLASS :: RPC_LBRE11_LMUX_WIDTH = 19;
const TN IID_CLASS :: RPC_LBRE11_BCN_WIDTH = 7;
const TN IID_CLASS :: RPC_LBSTD_DATA_POS_MAX = 11;
const TN IID_CLASS :: RPC_LBSTD_DATA_POS_WIDTH = 4;
const TN IID_CLASS :: RPC_LBSTD_TIME_WIDTH = 3;
const TN IID_CLASS :: RPC_LBSTD_CHAMBER_WIDTH = 2;
const TN IID_CLASS :: RPC_LBSTD_CODE_WIDTH = 17;
const TN IID_CLASS :: RPC_LBSTD_LMUX_WIDTH = 19;
const TN IID_CLASS :: RPC_LBSTD_BCN_WIDTH = 7;
const TN IID_CLASS :: RPC_LINK_STRUC_PART_DATA = 0;
const TN IID_CLASS :: RPC_LINK_STRUC_PART_CODE = 1;
const TN IID_CLASS :: RPC_LINK_STRUC_PART_TIME = 2;
const TN IID_CLASS :: RPC_LINK_STRUC_END_DATA = 3;
const TN IID_CLASS :: RPC_LINK_STRUC_HALF_PART = 4;
const TN IID_CLASS :: RPC_LINK_STRUC_CHAN_NUM = 5;
const TN IID_CLASS :: RPC_LINK_STRUC_FAST_DATA = 6;
const TN IID_CLASS :: RPC_PAC_QUALITY_NUM_MAX = 7;
const TN IID_CLASS :: RPC_PAC_QUALITY_WIDTH = 3;
const TN IID_CLASS :: RPC_PAC_CODE_NUM_MAX = 31;
const TN IID_CLASS :: RPC_PAC_CODE_WIDTH = 5;
const TN IID_CLASS :: RPC_PAC_DATA_WIDTH = 9;
const TN IID_CLASS :: RPC_PAC_SORT_NUM = 4;
const TN IID_CLASS :: RPC_PAC_STRUC_SIGN = 0;
const TN IID_CLASS :: RPC_PAC_STRUC_CODE = 1;
const TN IID_CLASS :: RPC_PAC_STRUC_QUALITY = 2;
const TN IID_CLASS :: I2C_AREA_ADDR_SIZE = 2;
const TN IID_CLASS :: I2C_AREA_ADDR_ITEM = 4;
const TN IID_CLASS :: I2C_AREA_DATA_SIZE = 16;
const TN IID_CLASS :: I2C_AREA_SEL_SIZE = 2;
const TN IID_CLASS :: TB_BASE_CNTRL = 0;
const TN IID_CLASS :: TB_BASE_SORTER = 1;
const TN IID_CLASS :: TB_BASE_OPTO = 2;
const TN IID_CLASS :: TB_BASE_LDPAC = 8;
const TN IID_CLASS :: TB_BASE_RMB = 12;
const TN IID_CLASS :: TB_BASE_NUM = 13;
const TN IID_CLASS :: TB_BASE_SIZE = 4;
const TN IID_CLASS :: TB_II_ADDR_WIDTH_HARD = 20;
const TN IID_CLASS :: TB_II_ADDR_WIDTH_SOFT = 18;
const TN IID_CLASS :: TB_II_BASE_WIDTH = 4;
const TN IID_CLASS :: TB_II_ADDR_WIDTH_USER = 14;
const TN IID_CLASS :: TB_II_DATA_WIDTH = 16;
const TN IID_CLASS :: TB_II_INT_WIDTH = 3;
const TN IID_CLASS :: TB_MUX_MULTIPL = 8;
const TN IID_CLASS :: TB_MUX_MULTIPL_WIDTH = 3;
const TN IID_CLASS :: TB_FAST_BUS_WIDTH = 3;
const TN IID_CLASS :: TB_LINK_NUM = 18;
const TN IID_CLASS :: TB_OPTO_CHAN_NUM = 3;
const TN IID_CLASS :: TB_OPTO_BOARD_NUM = 6;
const TN IID_CLASS :: TB_OPTO_MUX_MULTIPL = 8;
const TN IID_CLASS :: TB_OPTO_MUX_DATA_WIDTH = 3;
const TN IID_CLASS :: TB_OPTO_DATA_WIDTH = 24;
const TN IID_CLASS :: TB_OPTO_BCN_WIDTH = 4;
const TN IID_CLASS :: TB_OPTO_MUX_DELAY_WIDTH = 3;
const TN IID_CLASS :: TB_PAC_BOARD_NUM = 4;
const TN IID_CLASS :: TB_PAC_NUM = 12;
const TN IID_CLASS :: TB_PAC_MUX_MULTIPL = 8;
const TN IID_CLASS :: TB_PAC_MUX_DATA_WIDTH = 14;
const TN IID_CLASS :: TB_PAC_DATA_WIDTH = 112;
const TN IID_CLASS :: TB_PAC_BCN_WIDTH = 3;
const TN IID_CLASS :: TB_PAC_MUX_DELAY_WIDTH = 3;
const TN IID_CLASS :: TB_PAC_GB_DATA_WIDTH = 3;
const TN IID_CLASS :: RPC_GB_DATA_SIZE = 2;
const TN IID_CLASS :: RPC_PAC_GB_PHI_ADDR_SIZE = 4;
const TN IID_CLASS :: RPC_PAC_GB_ETA_ADDR_SIZE = 2;
const TN IID_CLASS :: RPC_PAC_GB_DATA_SIZE = 17;
const TN IID_CLASS :: TB_SORT_MUX_MULTIPL = 8;
const TN IID_CLASS :: TB_SORT_MUX_DATA_WIDTH = 9;
const TN IID_CLASS :: TB_SORT_DATA_WIDTH = 72;
const TN IID_CLASS :: TB_SORT_BCN_WIDTH = 3;
const TN IID_CLASS :: TB_SORT_MUX_DELAY_WIDTH = 3;
const TN IID_CLASS :: TB_BOARD_NUM = 9;
const TS IID_CLASS :: TB_BOARD_IDENTIFIER = "TB";
const TN IID_CLASS :: TB_TIMER_SIZE = 40;
const TN IID_CLASS :: TB_TRIGGER_SEL_L1A = 0;
const TN IID_CLASS :: TB_TRIGGER_SEL_PRETRG0 = 1;
const TN IID_CLASS :: TB_TRIGGER_SEL_PRETRG1 = 2;
const TN IID_CLASS :: TB_TRIGGER_SEL_PRETRG2 = 3;
const TN IID_CLASS :: TB_TRIGGER_SEL_NUM = 4;
const TN IID_CLASS :: TB_TRIGGER_SEL_SIZE = 2;
const TN IID_CLASS :: TC_ADDR_ETA_SIZE = 6;
const TN IID_CLASS :: TC_GB_DATA_SIZE = 21;
const TN IID_CLASS :: TC_SORT_CHAN_OUT_NUM = 2;
const TN IID_CLASS :: TC_SORT_CHAN_NUM = 2;
const TN IID_CLASS :: TC_SORT_MUX_MULTIPL = 2;
const TN IID_CLASS :: TC_SORT_MUX_DATA_WIDTH_HARD = 29;
const TN IID_CLASS :: TC_SORT_MUX_DATA_WIDTH = 24;
const TN IID_CLASS :: TC_SORT_MUX_DATA_WIDTH_FREE = 5;
const TN IID_CLASS :: TC_SORT_TEST_DATA_WIDTH = 48;
const TN IID_CLASS :: TC_SORT_DATA_WIDTH = 42;
const TN IID_CLASS :: TC_SORT_BCN_WIDTH = 5;
const TN IID_CLASS :: TC_SORT_PART_SIZE = 2;
const TN IID_CLASS :: TC_SORT_PART_NUM = 24;
const TN IID_CLASS :: TC_SORT_IIADDR_WIDTH = 16;
const TN IID_CLASS :: TC_SORT_IIDATA_WIDTH = 16;
const TN IID_CLASS :: TC_SORT_MUX_LINE_NUM = 81;
const TN IID_CLASS :: TC_SORT_DATA_DELAY_WIDTH = 2;
const TN IID_CLASS :: TC_SORT_BCN0_DELAY_SIZE = 12;
const TN IID_CLASS :: TC_SORT_TRG_DELAY_SIZE = 7;
const TN IID_CLASS :: TC_SORT_TRIG_COUNT_SIZE = 32;
const TN IID_CLASS :: RECEIVER_ERROR_COUNT_SIZE = 32;
const TN IID_CLASS :: DAQ_DIAG_DATA_INSIZE = 648;
const TN IID_CLASS :: DAQ_DIAG_DATA_OUTSIZE = 84;
const TN IID_CLASS :: DAQ_DIAG_DATA_SIZE = 745;
const TN IID_CLASS :: DAQ_DIAG_DATA_DELAY_SIZE = 8;
const TN IID_CLASS :: DAQ_DIAG_TRIG_NUM = 1;
const TN IID_CLASS :: DAQ_DIAG_TIMER_SIZE = 36;
const TN IID_CLASS :: DAQ_DIAG_MASK_WIDTH = 16;
const TN IID_CLASS :: DAQ_DIAG_ADDR_WIDTH = 7;
const TN IID_CLASS :: DAQ_DIAG_ADDR_POS = 128;
const TN IID_CLASS :: DAQ_DIAG_MEM_DATA_SIZE = 751;
const TN IID_CLASS :: DAQ_MEM_ADDR_SIZE = 13;
const TN IID_CLASS :: PULSE_POS_NUM = 256;
const TN IID_CLASS :: PULSE_POS_SIZE = 8;
const TN IID_CLASS :: PULSE_INDATA_SIZE = 612;
const TN IID_CLASS :: PULSE_DATA_SIZE = 612;
const TN IID_CLASS :: PULSE_MEM_ADDR_SIZE = 14;
const TN IID_CLASS :: TRG_COND_SEL_NONE = 0;
const TN IID_CLASS :: TRG_COND_SEL_L1A = 1;
const TN IID_CLASS :: TRG_COND_SEL_PRETRG0 = 2;
const TN IID_CLASS :: TRG_COND_SEL_PRETRG1 = 3;
const TN IID_CLASS :: TRG_COND_SEL_PRETRG2 = 4;
const TN IID_CLASS :: TRG_COND_SEL_BCN0 = 5;
const TN IID_CLASS :: TRG_COND_SEL_LOCAL = 6;
const TN IID_CLASS :: TRG_COND_SEL_NUM = 7;
const TN IID_CLASS :: TRG_COND_SEL_SIZE = 3;
const TS IID_CLASS :: TC_BOARD_IDENTIFIER = "TC";
const TS IID_CLASS :: TC_SORT_IDENTIFIER = "GS";
const THV IID_CLASS :: TC_SORT_VERSION = "0001";
const TN IID_CLASS :: QPLL_SELECT_WIDTH = 6;
const TN IID_CLASS :: CLOCK_TEST_SIZE = 4;

// parameters
const TN IID_CLASS :: II_ADDR_SIZE  = 16;
const TN IID_CLASS :: II_DATA_SIZE  = 16;
const TN IID_CLASS :: CHECK_SUM = 413383870;

// interfaces
const TN IID_CLASS :: PAGE_UNIVERSAL = 0;
const TN IID_CLASS :: WORD_CHECKSUM = 1;
const TN IID_CLASS :: WORD_BOARD = 2;
const TN IID_CLASS :: WORD_IDENTIFIER = 3;
const TN IID_CLASS :: WORD_VERSION = 4;
const TN IID_CLASS :: WORD_USER_REG1 = 5;
const TN IID_CLASS :: WORD_USER_REG2 = 6;
const TN IID_CLASS :: VECT_STATUS = 7;
const TN IID_CLASS :: BITS_STATUS_PLL_UNLOCK = 8;
const TN IID_CLASS :: BITS_STATUS_STROBE_UNLOCK = 9;
const TN IID_CLASS :: BITS_STATUS_TRG_DATA_SEL = 10;
const TN IID_CLASS :: WORD_REC_CHAN_ENA = 11;
const TN IID_CLASS :: WORD_REC_MUX_CLK90 = 12;
const TN IID_CLASS :: WORD_REC_MUX_CLK_INV = 13;
const TN IID_CLASS :: WORD_REC_MUX_REG_ADD = 14;
const TN IID_CLASS :: WORD_REC_MUX_DELAY = 15;
const TN IID_CLASS :: WORD_REC_DATA_DELAY = 16;
const TN IID_CLASS :: WORD_REC_CHECK_ENA = 17;
const TN IID_CLASS :: WORD_REC_CHECK_DATA_ENA = 18;
const TN IID_CLASS :: WORD_REC_TEST_ENA = 19;
const TN IID_CLASS :: WORD_REC_TEST_RND_ENA = 20;
const TN IID_CLASS :: WORD_REC_TEST_DATA = 21;
const TN IID_CLASS :: WORD_REC_TEST_OR_DATA = 22;
const TN IID_CLASS :: WORD_REC_ERROR_COUNT = 23;
const TN IID_CLASS :: WORD_SEND_CLK_INV = 24;
const TN IID_CLASS :: WORD_SEND_CHECK_ENA = 25;
const TN IID_CLASS :: WORD_SEND_CHECK_DATA_ENA = 26;
const TN IID_CLASS :: WORD_SEND_TEST_ENA = 27;
const TN IID_CLASS :: WORD_SEND_TEST_DATA = 28;
const TN IID_CLASS :: VECT_QPLL = 29;
const TN IID_CLASS :: BITS_QPLL_MODE = 30;
const TN IID_CLASS :: BITS_QPLL_CTRL = 31;
const TN IID_CLASS :: BITS_QPLL_SELECT = 32;
const TN IID_CLASS :: BITS_QPLL_LOCKED = 33;
const TN IID_CLASS :: BITS_QPLL_ERROR = 34;
const TN IID_CLASS :: BITS_QPLL_CLK_LOCK = 35;
const TN IID_CLASS :: VECT_TEST_CNT = 36;
const TN IID_CLASS :: BITS_TEST_CNT_MAIN = 37;
const TN IID_CLASS :: BITS_TEST_CNT_DES1 = 38;
const TN IID_CLASS :: BITS_TEST_CNT_DES2 = 39;
const TN IID_CLASS :: BITS_TEST_CNT_BCN0 = 40;
const TN IID_CLASS :: VECT_TTC = 41;
const TN IID_CLASS :: BITS_TTC_CLK_INV = 42;
const TN IID_CLASS :: BITS_TTC_INIT_ENA = 43;
const TN IID_CLASS :: BITS_TTC_RESET = 44;
const TN IID_CLASS :: BITS_TTC_READY = 45;
const TN IID_CLASS :: BITS_TTC_TEST_ENA = 46;
const TN IID_CLASS :: WORD_TTC_BROADCAST = 47;
const TN IID_CLASS :: WORD_TTC_DATA = 48;
const TN IID_CLASS :: WORD_TTC_SUBADDR = 49;
const TN IID_CLASS :: WORD_TTC_DQ = 50;
const TN IID_CLASS :: WORD_TTC_ID_MODE = 51;
const TN IID_CLASS :: WORD_TTC_TEST_DATA = 52;
const TN IID_CLASS :: WORD_TTC_BCN = 53;
const TN IID_CLASS :: WORD_TTC_EVN = 54;
const TN IID_CLASS :: WORD_BCN0_DELAY = 55;
const TN IID_CLASS :: WORD_DATA_TRG_DELAY = 56;
const TN IID_CLASS :: WORD_TRG_DELAY = 57;
const TN IID_CLASS :: WORD_STAT_TRIG_COUNT = 58;
const TN IID_CLASS :: VECT_DAQ = 59;
const TN IID_CLASS :: BITS_DAQ_EMPTY = 60;
const TN IID_CLASS :: BITS_DAQ_EMPTY_ACK = 61;
const TN IID_CLASS :: BITS_DAQ_LOST = 62;
const TN IID_CLASS :: BITS_DAQ_LOST_ACK = 63;
const TN IID_CLASS :: BITS_DAQ_TIMER_START = 64;
const TN IID_CLASS :: BITS_DAQ_TIMER_STOP = 65;
const TN IID_CLASS :: BITS_DAQ_TIMER_LOC_ENA = 66;
const TN IID_CLASS :: BITS_DAQ_TIMER_TRIG_SEL = 67;
const TN IID_CLASS :: BITS_DAQ_PROC_REQ = 68;
const TN IID_CLASS :: BITS_DAQ_PROC_ACK = 69;
const TN IID_CLASS :: BITS_DAQ_NON_EMPTY_ENA = 70;
const TN IID_CLASS :: VECT_DAQ_WR = 71;
const TN IID_CLASS :: BITS_DAQ_WR_ADDR = 72;
const TN IID_CLASS :: BITS_DAQ_WR_ACK = 73;
const TN IID_CLASS :: VECT_DAQ_RD = 74;
const TN IID_CLASS :: BITS_DAQ_RD_ADDR = 75;
const TN IID_CLASS :: BITS_DAQ_RD_ACK = 76;
const TN IID_CLASS :: WORD_DAQ_TIMER_LIMIT = 77;
const TN IID_CLASS :: WORD_DAQ_TIMER_COUNT = 78;
const TN IID_CLASS :: WORD_DAQ_TIMER_TRG_DELAY = 79;
const TN IID_CLASS :: WORD_DAQ_DATA_DELAY = 80;
const TN IID_CLASS :: WORD_DAQ_MASK = 81;
const TN IID_CLASS :: VECT_PULSER = 82;
const TN IID_CLASS :: BITS_PULSER_OUT_ENA = 83;
const TN IID_CLASS :: BITS_PULSER_OUT_IN_SEL = 84;
const TN IID_CLASS :: BITS_PULSER_LOOP_ENA = 85;
const TN IID_CLASS :: BITS_PULSER_TIMER_START = 86;
const TN IID_CLASS :: BITS_PULSER_TIMER_STOP = 87;
const TN IID_CLASS :: BITS_PULSER_TIMER_LOC_ENA = 88;
const TN IID_CLASS :: BITS_PULSER_TIMER_TRIG_SEL = 89;
const TN IID_CLASS :: BITS_PULSER_REPEAT_ENA = 90;
const TN IID_CLASS :: BITS_PULSER_PROC_REQ = 91;
const TN IID_CLASS :: BITS_PULSER_PROC_ACK = 92;
const TN IID_CLASS :: WORD_PULSER_LENGTH = 93;
const TN IID_CLASS :: WORD_PULSER_TIMER_LIMIT = 94;
const TN IID_CLASS :: WORD_PULSER_TIMER_COUNT = 95;
const TN IID_CLASS :: WORD_PULSER_TIMER_TRG_DELAY = 96;
const TN IID_CLASS :: AREA_I2C = 97;
const TN IID_CLASS :: AREA_MEM_PULSE = 98;
const TN IID_CLASS :: AREA_MEM_DAQ_DIAG = 99;

TMemoryMap* IID_CLASS ::BuildMemoryMap() {
  const TVIIItemDecl declItems[] =
  {
    { VII_WORD, WORD_CHECKSUM, 16, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("CHECKSUM"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_BOARD, 16, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("BOARD"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_IDENTIFIER, 16, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("IDENTIFIER"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_VERSION, 16, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("VERSION"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_USER_REG1, 16, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("USER_REG1"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_USER_REG2, 16, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("USER_REG2"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_VECT, VECT_STATUS, 0, 0, PAGE_UNIVERSAL, VII_WNOACCESS, VII_RNOACCESS, VIINameConv("STATUS"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_STATUS_PLL_UNLOCK, 1, 1, VECT_STATUS, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("PLL_UNLOCK"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_STATUS_STROBE_UNLOCK, 1, 1, VECT_STATUS, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("STROBE_UNLOCK"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_STATUS_TRG_DATA_SEL, 3, 1, VECT_STATUS, VII_WACCESS, VII_RINTERNAL,VIINameConv("TRG_DATA_SEL"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_CHAN_ENA, 9, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_CHAN_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_MUX_CLK90, 81, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_MUX_CLK90"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_MUX_CLK_INV, 81, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_MUX_CLK_INV"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_MUX_REG_ADD, 81, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_MUX_REG_ADD"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_MUX_DELAY, 3, 9, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_MUX_DELAY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_DATA_DELAY, 2, 9, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_DATA_DELAY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_CHECK_ENA, 9, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_CHECK_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_CHECK_DATA_ENA, 9, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_CHECK_DATA_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_TEST_ENA, 9, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_TEST_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_TEST_RND_ENA, 9, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("REC_TEST_RND_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_TEST_DATA, 72, 9, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("REC_TEST_DATA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_TEST_OR_DATA, 9, 9, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("REC_TEST_OR_DATA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_REC_ERROR_COUNT, 32, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_REXTERNAL,VIINameConv("REC_ERROR_COUNT"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_SEND_CLK_INV, 2, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("SEND_CLK_INV"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_SEND_CHECK_ENA, 2, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("SEND_CHECK_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_SEND_CHECK_DATA_ENA, 2, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("SEND_CHECK_DATA_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_SEND_TEST_ENA, 2, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("SEND_TEST_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_SEND_TEST_DATA, 48, 2, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("SEND_TEST_DATA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_VECT, VECT_QPLL, 0, 0, PAGE_UNIVERSAL, VII_WNOACCESS, VII_RNOACCESS, VIINameConv("QPLL"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_QPLL_MODE, 1, 1, VECT_QPLL, VII_WACCESS, VII_RINTERNAL,VIINameConv("MODE"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_QPLL_CTRL, 1, 1, VECT_QPLL, VII_WACCESS, VII_RINTERNAL,VIINameConv("CTRL"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_QPLL_SELECT, 6, 1, VECT_QPLL, VII_WACCESS, VII_RINTERNAL,VIINameConv("SELECT"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_QPLL_LOCKED, 1, 1, VECT_QPLL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("LOCKED"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_QPLL_ERROR, 1, 1, VECT_QPLL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("ERROR"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_QPLL_CLK_LOCK, 1, 1, VECT_QPLL, VII_WACCESS, VII_RINTERNAL,VIINameConv("CLK_LOCK"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_VECT, VECT_TEST_CNT, 0, 0, PAGE_UNIVERSAL, VII_WNOACCESS, VII_RNOACCESS, VIINameConv("TEST_CNT"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_TEST_CNT_MAIN, 4, 1, VECT_TEST_CNT, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("MAIN"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_TEST_CNT_DES1, 4, 1, VECT_TEST_CNT, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("DES1"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_TEST_CNT_DES2, 4, 1, VECT_TEST_CNT, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("DES2"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_TEST_CNT_BCN0, 4, 1, VECT_TEST_CNT, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("BCN0"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_VECT, VECT_TTC, 0, 0, PAGE_UNIVERSAL, VII_WNOACCESS, VII_RNOACCESS, VIINameConv("TTC"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_TTC_CLK_INV, 1, 1, VECT_TTC, VII_WACCESS, VII_RINTERNAL,VIINameConv("CLK_INV"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_TTC_INIT_ENA, 1, 1, VECT_TTC, VII_WACCESS, VII_RINTERNAL,VIINameConv("INIT_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_TTC_RESET, 1, 1, VECT_TTC, VII_WACCESS, VII_RINTERNAL,VIINameConv("RESET"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_TTC_READY, 1, 1, VECT_TTC, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("READY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_TTC_TEST_ENA, 1, 1, VECT_TTC, VII_WACCESS, VII_RINTERNAL,VIINameConv("TEST_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_TTC_BROADCAST, 6, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("TTC_BROADCAST"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_TTC_DATA, 8, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("TTC_DATA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_TTC_SUBADDR, 8, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("TTC_SUBADDR"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_TTC_DQ, 4, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("TTC_DQ"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_TTC_ID_MODE, 16, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("TTC_ID_MODE"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_TTC_TEST_DATA, 6, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("TTC_TEST_DATA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_TTC_BCN, 12, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("TTC_BCN"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_TTC_EVN, 24, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("TTC_EVN"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_BCN0_DELAY, 12, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("BCN0_DELAY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_DATA_TRG_DELAY, 7, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("DATA_TRG_DELAY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_TRG_DELAY, 7, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("TRG_DELAY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_STAT_TRIG_COUNT, 32, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_REXTERNAL,VIINameConv("STAT_TRIG_COUNT"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_VECT, VECT_DAQ, 0, 0, PAGE_UNIVERSAL, VII_WNOACCESS, VII_RNOACCESS, VIINameConv("DAQ"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_EMPTY, 1, 1, VECT_DAQ, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("EMPTY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_EMPTY_ACK, 1, 1, VECT_DAQ, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("EMPTY_ACK"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_LOST, 1, 1, VECT_DAQ, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("LOST"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_LOST_ACK, 1, 1, VECT_DAQ, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("LOST_ACK"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_TIMER_START, 1, 1, VECT_DAQ, VII_WACCESS, VII_RINTERNAL,VIINameConv("TIMER_START"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_TIMER_STOP, 1, 1, VECT_DAQ, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("TIMER_STOP"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_TIMER_LOC_ENA, 1, 1, VECT_DAQ, VII_WACCESS, VII_RINTERNAL,VIINameConv("TIMER_LOC_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_TIMER_TRIG_SEL, 3, 1, VECT_DAQ, VII_WACCESS, VII_RINTERNAL,VIINameConv("TIMER_TRIG_SEL"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_PROC_REQ, 1, 1, VECT_DAQ, VII_WACCESS, VII_RINTERNAL,VIINameConv("PROC_REQ"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_PROC_ACK, 1, 1, VECT_DAQ, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("PROC_ACK"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_NON_EMPTY_ENA, 1, 1, VECT_DAQ, VII_WACCESS, VII_RINTERNAL,VIINameConv("NON_EMPTY_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_VECT, VECT_DAQ_WR, 0, 0, PAGE_UNIVERSAL, VII_WNOACCESS, VII_RNOACCESS, VIINameConv("DAQ_WR"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_WR_ADDR, 7, 1, VECT_DAQ_WR, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("ADDR"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_WR_ACK, 1, 1, VECT_DAQ_WR, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("ACK"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_VECT, VECT_DAQ_RD, 0, 0, PAGE_UNIVERSAL, VII_WNOACCESS, VII_RNOACCESS, VIINameConv("DAQ_RD"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_RD_ADDR, 7, 1, VECT_DAQ_RD, VII_WACCESS, VII_REXTERNAL,VIINameConv("ADDR"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_DAQ_RD_ACK, 1, 1, VECT_DAQ_RD, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("ACK"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_DAQ_TIMER_LIMIT, 40, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("DAQ_TIMER_LIMIT"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_DAQ_TIMER_COUNT, 40, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("DAQ_TIMER_COUNT"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_DAQ_TIMER_TRG_DELAY, 7, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("DAQ_TIMER_TRG_DELAY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_DAQ_DATA_DELAY, 8, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("DAQ_DATA_DELAY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_DAQ_MASK, 16, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("DAQ_MASK"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_VECT, VECT_PULSER, 0, 0, PAGE_UNIVERSAL, VII_WNOACCESS, VII_RNOACCESS, VIINameConv("PULSER"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_OUT_ENA, 1, 1, VECT_PULSER, VII_WACCESS, VII_RINTERNAL,VIINameConv("OUT_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_OUT_IN_SEL, 1, 1, VECT_PULSER, VII_WACCESS, VII_RINTERNAL,VIINameConv("OUT_IN_SEL"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_LOOP_ENA, 1, 1, VECT_PULSER, VII_WACCESS, VII_RINTERNAL,VIINameConv("LOOP_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_TIMER_START, 1, 1, VECT_PULSER, VII_WACCESS, VII_RINTERNAL,VIINameConv("TIMER_START"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_TIMER_STOP, 1, 1, VECT_PULSER, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("TIMER_STOP"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_TIMER_LOC_ENA, 1, 1, VECT_PULSER, VII_WACCESS, VII_RINTERNAL,VIINameConv("TIMER_LOC_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_TIMER_TRIG_SEL, 3, 1, VECT_PULSER, VII_WACCESS, VII_RINTERNAL,VIINameConv("TIMER_TRIG_SEL"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_REPEAT_ENA, 1, 1, VECT_PULSER, VII_WACCESS, VII_RINTERNAL,VIINameConv("REPEAT_ENA"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_PROC_REQ, 1, 1, VECT_PULSER, VII_WACCESS, VII_RINTERNAL,VIINameConv("PROC_REQ"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_BITS, BITS_PULSER_PROC_ACK, 1, 1, VECT_PULSER, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("PROC_ACK"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_PULSER_LENGTH, 8, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("PULSER_LENGTH"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_PULSER_TIMER_LIMIT, 40, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("PULSER_TIMER_LIMIT"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_PULSER_TIMER_COUNT, 40, 1, PAGE_UNIVERSAL, VII_WNOACCESS, VII_REXTERNAL,VIINameConv("PULSER_TIMER_COUNT"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_WORD, WORD_PULSER_TIMER_TRG_DELAY, 7, 1, PAGE_UNIVERSAL, VII_WACCESS, VII_RINTERNAL,VIINameConv("PULSER_TIMER_TRG_DELAY"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_AREA, AREA_I2C, 16, 4, PAGE_UNIVERSAL, VII_WACCESS, VII_REXTERNAL,VIINameConv("I2C"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_AREA, AREA_MEM_PULSE, 612, 256, PAGE_UNIVERSAL, VII_WACCESS, VII_REXTERNAL,VIINameConv("MEM_PULSE"), VII_FUN_UNDEF, VIIDescrConv(" ")},
    { VII_AREA, AREA_MEM_DAQ_DIAG, 751, 128, PAGE_UNIVERSAL, VII_WACCESS, VII_REXTERNAL,VIINameConv("MEM_DAQ_DIAG"), VII_FUN_UNDEF, VIIDescrConv(" ")}
  };
  const TVIIItem viiItems[] =
  {
    { VII_WORD, 1, -1, 16, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 0, 1}, //WORD_CHECKSUM
    { VII_WORD, 2, -1, 16, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 1, 1}, //WORD_BOARD
    { VII_WORD, 3, -1, 16, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 2, 1}, //WORD_IDENTIFIER
    { VII_WORD, 4, -1, 16, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 3, 1}, //WORD_VERSION
    { VII_WORD, 5, -1, 16, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 4, 1}, //WORD_USER_REG1
    { VII_WORD, 6, -1, 16, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 5, 1}, //WORD_USER_REG2
    { VII_BITS, 8, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 6, 0}, //BITS_STATUS_PLL_UNLOCK
    { VII_BITS, 9, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 6, 1}, //BITS_STATUS_STROBE_UNLOCK
    { VII_BITS, 10, -1, 3, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 6, 2}, //BITS_STATUS_TRG_DATA_SEL
    { VII_WORD, 11, -1, 9, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 7, 1}, //WORD_REC_CHAN_ENA
    { VII_WORD, 12, -1, 81, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 8, 6}, //WORD_REC_MUX_CLK90
    { VII_WORD, 13, -1, 81, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 14, 6}, //WORD_REC_MUX_CLK_INV
    { VII_WORD, 14, -1, 81, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 20, 6}, //WORD_REC_MUX_REG_ADD
    { VII_WORD, 15, -1, 3, 9, VII_WACCESS, -1, VII_RINTERNAL, -1, 26, 1}, //WORD_REC_MUX_DELAY
    { VII_WORD, 16, -1, 2, 9, VII_WACCESS, -1, VII_RINTERNAL, -1, 35, 1}, //WORD_REC_DATA_DELAY
    { VII_WORD, 17, -1, 9, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 44, 1}, //WORD_REC_CHECK_ENA
    { VII_WORD, 18, -1, 9, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 45, 1}, //WORD_REC_CHECK_DATA_ENA
    { VII_WORD, 19, -1, 9, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 46, 1}, //WORD_REC_TEST_ENA
    { VII_WORD, 20, -1, 9, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 47, 1}, //WORD_REC_TEST_RND_ENA
    { VII_WORD, 21, -1, 72, 9, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 48, 5}, //WORD_REC_TEST_DATA
    { VII_WORD, 22, -1, 9, 9, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 93, 1}, //WORD_REC_TEST_OR_DATA
    { VII_WORD, 23, -1, 32, 1, VII_WACCESS, -1, VII_REXTERNAL, -1, 102, 2}, //WORD_REC_ERROR_COUNT
    { VII_WORD, 24, -1, 2, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 104, 1}, //WORD_SEND_CLK_INV
    { VII_WORD, 25, -1, 2, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 105, 1}, //WORD_SEND_CHECK_ENA
    { VII_WORD, 26, -1, 2, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 106, 1}, //WORD_SEND_CHECK_DATA_ENA
    { VII_WORD, 27, -1, 2, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 107, 1}, //WORD_SEND_TEST_ENA
    { VII_WORD, 28, -1, 48, 2, VII_WACCESS, -1, VII_RINTERNAL, -1, 108, 3}, //WORD_SEND_TEST_DATA
    { VII_BITS, 30, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 114, 0}, //BITS_QPLL_MODE
    { VII_BITS, 31, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 114, 1}, //BITS_QPLL_CTRL
    { VII_BITS, 32, -1, 6, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 114, 2}, //BITS_QPLL_SELECT
    { VII_BITS, 33, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 114, 8}, //BITS_QPLL_LOCKED
    { VII_BITS, 34, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 114, 9}, //BITS_QPLL_ERROR
    { VII_BITS, 35, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 114, 10}, //BITS_QPLL_CLK_LOCK
    { VII_BITS, 37, -1, 4, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 115, 0}, //BITS_TEST_CNT_MAIN
    { VII_BITS, 38, -1, 4, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 115, 4}, //BITS_TEST_CNT_DES1
    { VII_BITS, 39, -1, 4, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 115, 8}, //BITS_TEST_CNT_DES2
    { VII_BITS, 40, -1, 4, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 115, 12}, //BITS_TEST_CNT_BCN0
    { VII_BITS, 42, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 116, 0}, //BITS_TTC_CLK_INV
    { VII_BITS, 43, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 116, 1}, //BITS_TTC_INIT_ENA
    { VII_BITS, 44, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 116, 2}, //BITS_TTC_RESET
    { VII_BITS, 45, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 116, 3}, //BITS_TTC_READY
    { VII_BITS, 46, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 116, 4}, //BITS_TTC_TEST_ENA
    { VII_WORD, 47, -1, 6, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 117, 1}, //WORD_TTC_BROADCAST
    { VII_WORD, 48, -1, 8, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 118, 1}, //WORD_TTC_DATA
    { VII_WORD, 49, -1, 8, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 119, 1}, //WORD_TTC_SUBADDR
    { VII_WORD, 50, -1, 4, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 120, 1}, //WORD_TTC_DQ
    { VII_WORD, 51, -1, 16, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 121, 1}, //WORD_TTC_ID_MODE
    { VII_WORD, 52, -1, 6, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 122, 1}, //WORD_TTC_TEST_DATA
    { VII_WORD, 53, -1, 12, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 123, 1}, //WORD_TTC_BCN
    { VII_WORD, 54, -1, 24, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 124, 2}, //WORD_TTC_EVN
    { VII_WORD, 55, -1, 12, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 126, 1}, //WORD_BCN0_DELAY
    { VII_WORD, 56, -1, 7, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 127, 1}, //WORD_DATA_TRG_DELAY
    { VII_WORD, 57, -1, 7, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 128, 1}, //WORD_TRG_DELAY
    { VII_WORD, 58, -1, 32, 1, VII_WACCESS, -1, VII_REXTERNAL, -1, 129, 2}, //WORD_STAT_TRIG_COUNT
    { VII_BITS, 60, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 131, 0}, //BITS_DAQ_EMPTY
    { VII_BITS, 61, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 131, 1}, //BITS_DAQ_EMPTY_ACK
    { VII_BITS, 62, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 131, 2}, //BITS_DAQ_LOST
    { VII_BITS, 63, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 131, 3}, //BITS_DAQ_LOST_ACK
    { VII_BITS, 64, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 131, 4}, //BITS_DAQ_TIMER_START
    { VII_BITS, 65, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 131, 5}, //BITS_DAQ_TIMER_STOP
    { VII_BITS, 66, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 131, 6}, //BITS_DAQ_TIMER_LOC_ENA
    { VII_BITS, 67, -1, 3, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 131, 7}, //BITS_DAQ_TIMER_TRIG_SEL
    { VII_BITS, 68, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 131, 10}, //BITS_DAQ_PROC_REQ
    { VII_BITS, 69, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 131, 11}, //BITS_DAQ_PROC_ACK
    { VII_BITS, 70, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 131, 12}, //BITS_DAQ_NON_EMPTY_ENA
    { VII_BITS, 72, -1, 7, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 132, 0}, //BITS_DAQ_WR_ADDR
    { VII_BITS, 73, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 132, 7}, //BITS_DAQ_WR_ACK
    { VII_BITS, 75, -1, 7, 1, VII_WACCESS, -1, VII_REXTERNAL, -1, 133, 0}, //BITS_DAQ_RD_ADDR
    { VII_BITS, 76, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 133, 7}, //BITS_DAQ_RD_ACK
    { VII_WORD, 77, -1, 40, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 134, 3}, //WORD_DAQ_TIMER_LIMIT
    { VII_WORD, 78, -1, 40, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 137, 3}, //WORD_DAQ_TIMER_COUNT
    { VII_WORD, 79, -1, 7, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 140, 1}, //WORD_DAQ_TIMER_TRG_DELAY
    { VII_WORD, 80, -1, 8, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 141, 1}, //WORD_DAQ_DATA_DELAY
    { VII_WORD, 81, -1, 16, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 142, 1}, //WORD_DAQ_MASK
    { VII_BITS, 83, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 143, 0}, //BITS_PULSER_OUT_ENA
    { VII_BITS, 84, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 143, 1}, //BITS_PULSER_OUT_IN_SEL
    { VII_BITS, 85, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 143, 2}, //BITS_PULSER_LOOP_ENA
    { VII_BITS, 86, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 143, 3}, //BITS_PULSER_TIMER_START
    { VII_BITS, 87, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 143, 4}, //BITS_PULSER_TIMER_STOP
    { VII_BITS, 88, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 143, 5}, //BITS_PULSER_TIMER_LOC_ENA
    { VII_BITS, 89, -1, 3, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 143, 6}, //BITS_PULSER_TIMER_TRIG_SEL
    { VII_BITS, 90, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 143, 9}, //BITS_PULSER_REPEAT_ENA
    { VII_BITS, 91, -1, 1, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 143, 10}, //BITS_PULSER_PROC_REQ
    { VII_BITS, 92, -1, 1, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 143, 11}, //BITS_PULSER_PROC_ACK
    { VII_WORD, 93, -1, 8, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 144, 1}, //WORD_PULSER_LENGTH
    { VII_WORD, 94, -1, 40, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 145, 3}, //WORD_PULSER_TIMER_LIMIT
    { VII_WORD, 95, -1, 40, 1, VII_WNOACCESS, -1, VII_REXTERNAL, -1, 148, 3}, //WORD_PULSER_TIMER_COUNT
    { VII_WORD, 96, -1, 7, 1, VII_WACCESS, -1, VII_RINTERNAL, -1, 151, 1}, //WORD_PULSER_TIMER_TRG_DELAY
    { VII_AREA, 97, -1, 16, 4, VII_WACCESS, -1, VII_REXTERNAL, -1, 152, 1}, //AREA_I2C
    { VII_AREA, 98, -1, 612, 256, VII_WACCESS, -1, VII_REXTERNAL, -1, 16384, 39}, //AREA_MEM_PULSE
    { VII_AREA, 99, -1, 751, 128, VII_WACCESS, -1, VII_REXTERNAL, -1, 32768, 47} //AREA_MEM_DAQ_DIAG
  };
  return new TMemoryMap(declItems, sizeof declItems / sizeof declItems[0],
    viiItems, sizeof viiItems / sizeof viiItems[0], II_ADDR_SIZE, II_DATA_SIZE, CHECK_SUM);
}
