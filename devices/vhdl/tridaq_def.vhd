-- **************************************************************************************
-- *											*
-- *			  TriDAQ board definitions, 2001				*
-- *											*
-- **************************************************************************************

library ieee;
use ieee.std_logic_1164.all;
use work.std_logic_1164_ktp.all;

package tridaq is

  -- predefined constants of clock and time
  constant CLK_FREQ			:TN := 40000000;-- frequency = 40 MHz
  constant LED_PULSE_FREQ		:TN := 1;	-- repeat time = 1s
  constant LED_PULSE_WIDTH		:TN := (CLK_FREQ/10); -- pulse time = 100ms
  -- predefined constants of links compressions
  constant DELAY_PIPELINE_SIZE		:TVL := 7;	-- delay pileline length maximum
  constant TIME_PARTITION_CODE_SIZE 	:TVL := 4;	-- time latency bus width  in bits
  constant DATA_PARTITION_CODE_SIZE 	:TVL := 4;	-- partition number bus width in bits
  constant DATA_PARTITION_SIZE 		:TVL := 8; 	-- data bus width in bits
  constant CHAMBER_ADDRESS_SIZE		:TVL := 2;	-- chamber number bus width in bits
  constant TIME_PARTITION_CODE_MAX	:TVL := 7;	-- maximum: 2^TIME_PARTITION_CODE_SIZE-1
  constant DATA_PARTITION_CODE_MAX      :TVL := 11;	-- maximum:	2^DATA_PARTITION_CODE_SIZE-1
  constant TRIGGER_PAGE_CODE_SIZE	:TVL := 6;	-- page bus width in bits
  constant VME_ADDRESS_SIZE		:TVL := 16;	-- VME address bus width in bits
  constant VME_DATA_SIZE		:TVL := 32;	-- VME address bus width in bits
  -- predefined constants of link board data format
  constant LB_BCN_SIZE			:TVL := 6;	-- size of BCN less bits
  constant SYN_PIPE_MAX			:TVL := 6;	-- size of BCN auto-synchro pipeline
  -- predefined constants of Master DAQ board
  constant MDAQ_ADDR_SLAVE_SIZE		:TVL := 8;	-- size of Slave DAQ address board
  constant SDAQ_STATUS_SIZE		:TVL := 4;	-- size of Slave DAQ status word
  -- predefined constants of Internal Interface
  constant II_ADDR_SIZE			:TVL := 16;
  constant II_ADDR_BASE_SIZE		:TVL := 4;
  constant II_DATA_SIZE			:TVL := 16;
  constant II_CONTROL_BASE_ADDR		:TN  :=  0;
  constant II_JTAG_BASE_ADDR		:TN  :=  1;
  constant II_PAC0_BASE_ADDR		:TN  :=  2;	-- range from 2 to 6
  constant II_GB_BASE_ADDR		:TN  :=  7;
  constant II_LDEMUX0_BASE_ADDR		:TN  :=  8;	-- range from 8 to 15
  constant LDEMUX_OUT_SIZE		:TN  := 64;
  constant IILB_ADDR_SIZE		:TVL := 14;
  constant FEB_TEST_DATA		:TVL := 24;
  constant LB_GOL_DATA			:TVL := 32;
  --
  -- Controller board
  --
  -- predefined constants of Altera boot loader
  constant ALT_PGM_DATA_SIZE		:TVL := 15;
  -- predefined constants of TTC
  constant TTC_BROADCAST1_DATA_SIZE	:TVL := 4;
  constant TTC_BROADCAST2_DATA_SIZE	:TVL := 2;
  -- delay control bus
  constant DELAY_DATA_SIZE		:TVL := 6;
  --
  -- abrreviations
  --
  constant LDEMUX_DATA_OUT              :TVL := (DATA_PARTITION_CODE_MAX+1)*DATA_PARTITION_SIZE;
  constant DPM_DATA_SIZE 		:TVL := DATA_PARTITION_CODE_SIZE+DATA_PARTITION_SIZE+CHAMBER_ADDRESS_SIZE+2;
  constant DPM_ADDR_SIZE 		:TVL := DATA_PARTITION_CODE_SIZE+TRIGGER_PAGE_CODE_SIZE;
  constant LINK_DATA_SIZE               :TVL := TIME_PARTITION_CODE_SIZE+DPM_DATA_SIZE;
  constant DATA_COUNTER_CODE_SIZE 	:TVL := TVLcreate(TIME_PARTITION_CODE_MAX+1);
  constant II_ADDR_USER_SIZE		:TVL := II_ADDR_SIZE-II_ADDR_BASE_SIZE;
  constant DPM_DELAY_ADDR_SIZE		:TVL := DELAY_PIPELINE_SIZE+SLVPartAddrExpand(LINK_DATA_SIZE,II_DATA_SIZE);
  constant DPM_DELAY_DATA_SIZE		:TVL := minimum(LINK_DATA_SIZE,II_DATA_SIZE);
  constant DPM_DATA_ADDR_SIZE		:TVL := DPM_ADDR_SIZE+SLVPartAddrExpand(DPM_DATA_SIZE,II_DATA_SIZE);
  constant DPM_DATA_DATA_SIZE		:TVL := minimum(DPM_DATA_SIZE,II_DATA_SIZE);
  constant DPM_CNT_ADDR_SIZE		:TVL := TRIGGER_PAGE_CODE_SIZE+SLVPartAddrExpand(DATA_COUNTER_CODE_SIZE,II_DATA_SIZE);
  constant DPM_CNT_DATA_SIZE		:TVL := minimum(DATA_COUNTER_CODE_SIZE,II_DATA_SIZE);
  constant LDEMUX_OUTH_SIZE		:TN  := LDEMUX_OUT_SIZE/2;
  constant SYN_PIPE_SIZE		:TP  := TVLcreate(SYN_PIPE_MAX);
 

  -- bit slaces for link
  constant GL_ITEM_HALF_PART		:TN := 0;
  constant GL_ITEM_END_DATA		:TN := GL_ITEM_HALF_PART+1;
  constant GL_ITEM_CHAMBER		:TN := GL_ITEM_END_DATA+1;
  constant GL_ITEM_DATA			:TN := GL_ITEM_CHAMBER+1;
  constant GL_ITEM_PARTITION		:TN := GL_ITEM_DATA+1;
  constant GL_ITEM_TIME			:TN := GL_ITEM_PARTITION+1;
  constant GL_ITEM_NUM			:TN := GL_ITEM_TIME+1;
  constant GLA_ITEM_BCN			:TN := GL_ITEM_NUM;
  constant GLA_ITEM_BCN0		:TN := GLA_ITEM_BCN+1;
  constant GLA_ITEM_PARITY		:TN := GLA_ITEM_BCN0+1;
  constant GLA_ITEM_NUM			:TN := GLA_ITEM_PARITY+1;

  constant GL				:TVRV(GL_ITEM_NUM-1 downto 0) := (
    TVRcreate(TIME_PARTITION_CODE_SIZE)+
    TVRcreate(DATA_PARTITION_CODE_SIZE)+
    TVRcreate(DATA_PARTITION_SIZE)+
    TVRcreate(CHAMBER_ADDRESS_SIZE)+
    TVRcreate(1)+
    TVRcreate(1)
  );
  
  constant GLA				:TVRV(GLA_ITEM_NUM-1 downto 0) := (
    TVRcreate(1)+
    TVRcreate(1)+
    TVRcreate(LB_BCN_SIZE)+
    GL
  );
  
   constant LB_SYNCHRO_DATA		:TVL := (GLA(GLA'left).l+1+(1))/2;

end tridaq;
