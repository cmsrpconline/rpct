#undef II_DECL_IDENT
#define II_DECL_IDENT(type,name,value,comment)

#undef II_DECL_IDENT_SET
#define II_DECL_IDENT_SET(type,name,value,comment)

#undef II_DECL_IDENT_ENUM
#define II_DECL_IDENT_ENUM(type,name,comment)

#undef II_DECL_ITEM_BEGIN
#define II_DECL_ITEM_BEGIN(type,ID,width,num,parent,write,read,name,fun,description) \
(  type , ID    , width , num   , parent        , write , read  , name  , fun   , description   ),

#undef II_DECL_ITEM
#define II_DECL_ITEM(type,ID,width,num,parent,write,read,name,fun,description)  \
(  type , ID    , width , num   , parent        , write , read  , name  , fun   , description   ),

#undef II_DECL_ITEM_END
#define II_DECL_ITEM_END(type,ID,width,num,parent,write,read,name,fun,description) \
(  type , ID    , width , num   , parent        , write , read  , name  , fun   , description   )

 