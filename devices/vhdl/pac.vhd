library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;

library lpm;
use lpm.lpm_components.all;

use work.std_logic_1164_ktp.all;
use work.vcomponent.all;
use work.LPMcomponent.all;
use work.tridaq.all;
use work.pac_packet6.all;

entity pac is
  port(
    ms1a_inh					:in    Tms1h; 
    ms1b_inh					:in    Tms1h; 
    ms2a_inh					:in    Tms2h; 
    ms2b_inh					:in    Tms2h; 
    ms3_inh					:in    Tms3h; 
    ms4_inh					:in    Tms4h; 
    ms5_inh					:in    Tms5h;
    ms6_inh					:in    Tms6h;
    quality_out					:out   TQualityVal;
    code_out					:out   TCodeVal; 
    sign_out					:out   TSL; 
    -- fast signal interface
    clock					:in    TSL;
    l1a						:in    TSL;
    pretrg0					:in    TSL;
    pretrg1					:in    TSL;
    bcn0					:in    TSL;
    -- internal bus interface
    ii_base					:in    TSLV(2 downto 0);
    ii_addr					:in    TSLV(II_ADDR_SIZE-1 downto 0);
    ii_data					:inout TSLV(II_DATA_SIZE-1 downto 0);
    ii_oper					:in    TSL;
    ii_write					:in    TSL;
    ii_save					:in    TSL;
    ii_int					:inout TSL;
    ii_reset					:in    TSL;
    -- diagnostics signal
    led						:out   TSLV(3 downto 0);
    conf_led					:out   TSL
  );
end pac;

architecture behavioural of pac is

  component pac_all is
    port(
      ResetN		:in  TSL;
      Clk		:in  TSL;
      ms1a_in		:in  Tms1; 
      ms1b_in		:in  Tms1; 
      ms2a_in		:in  Tms2; 
      ms2b_in		:in  Tms2; 
      ms3_in		:in  Tms3; 
      ms4_in		:in  Tms4; 
      ms5_in		:in  Tms5;
      ms6_in		:in  Tms6;
      quality_out	:out TQualityVal;
      code_out		:out TCodeVal; 
      sign_out		:out TSL 
    );
  end component;
  --
  -- internal interface
  --
  constant PAC_IDENTIFIER			:TS := "PC";
  constant PAC_VERSION				:THV := "0100";
  --
  constant PAGE_REGISTERS			:TN := VEC_INDEX_MIN;
  --
  constant WORD_IDENTIFIER			:TN := VEC_INDEX_MIN;
  constant WORD_VERSION				:TN := WORD_IDENTIFIER+1;
  constant WORD_DUMMY				:TN := WORD_VERSION+1;
  --
  constant VIIItemDeclList	:TVIIItemDeclList :=(
  -- item type,        item ID,                    width,                num,       parent ID,   write type,    read type,             name,          type,        description
  (   VII_PAGE, PAGE_REGISTERS,                        0,                   0, PAGE_REGISTERS, VII_WNOACCESS, VII_RNOACCESS, VIINameConv(" "), VII_FUN_UNDEF, VIIDescrConv(" ")),
   (  VII_WORD, WORD_IDENTIFIER,            II_DATA_SIZE,                   1, PAGE_REGISTERS, VII_WNOACCESS, VII_REXTERNAL, VIINameConv(" "), VII_FUN_UNDEF, VIIDescrConv(" ")),
   (  VII_WORD, WORD_DUMMY,                 II_DATA_SIZE,                   1, PAGE_REGISTERS,   VII_WACCESS, VII_RINTERNAL, VIINameConv(" "), VII_FUN_UNDEF, VIIDescrConv(" "))
  );
  constant IIPar				:TVII := TVIIcreate(VIIItemDeclList,II_ADDR_SIZE,II_DATA_SIZE);
  signal   IIVecInt,IIVecExt,IIVecEna		:TSLV(VII(IIPar)'high downto VEC_INDEX_MIN);
  signal   LIIEnableSig				:TSL;
  signal   LIIAddrLocSig			:TSLV(II_ADDR_USER_SIZE-1 downto 0);
  signal   IIDataSig				:TSLV(II_DATA_SIZE-1 downto 0);
  signal   LIIDataExport			:TSLV(II_DATA_SIZE-1 downto 0);
  signal   LIIDataExportEnable			:TSL;
  --
  signal   L, H					:TSL;
  --
  signal   MS1aRegNeg				:Tms1h; 
  signal   MS1bRegNeg				:Tms1h; 
  signal   MS2aRegNeg				:Tms2h; 
  signal   MS2bRegNeg				:Tms2h; 
  signal   MS3RegNeg				:Tms3h; 
  signal   MS4RegNeg				:Tms4h; 
  signal   MS5RegNeg				:Tms5h;
  signal   MS6RegNeg				:Tms6h;
  signal   MS1aReg				:Tms1; 
  signal   MS1bReg				:Tms1; 
  signal   MS2aReg				:Tms2; 
  signal   MS2bReg				:Tms2; 
  signal   MS3Reg				:Tms3; 
  signal   MS4Reg				:Tms4; 
  signal   MS5Reg				:Tms5;
  signal   MS6Reg				:Tms6;
  --
  signal   L1AReg				:TSL;
  signal   BCN0Reg				:TSL;
  signal   Pretrg0Reg				:TSL;
  signal   Pretrg1Reg				:TSL;

begin	  

  L <= '0'; H <= '1';
  --
  process (ii_reset,clock)
  begin
    if (ii_reset='0') then
      MS1aRegNeg <= (others =>'0'); 
      MS1bRegNeg <= (others =>'0'); 
      MS2aRegNeg <= (others =>'0'); 
      MS2bRegNeg <= (others =>'0'); 
      MS3RegNeg  <= (others =>'0'); 
      MS4RegNeg  <= (others =>'0'); 
      MS5RegNeg  <= (others =>'0');
      MS6RegNeg  <= (others =>'0');
    elsif (clock'event and clock='0') then
      MS1aRegNeg <= ms1a_inh; 
      MS1bRegNeg <= ms1b_inh; 
      MS2aRegNeg <= ms2a_inh; 
      MS2bRegNeg <= ms2b_inh; 
      MS3RegNeg  <= ms3_inh; 
      MS4RegNeg  <= ms4_inh; 
      MS5RegNeg  <= ms5_inh;
      MS6RegNeg  <= ms6_inh;
    end if;
  end process;
  process (ii_reset,clock)
  begin
    if (ii_reset='0') then
      MS1aReg <= (others =>'0'); 
      MS1bReg <= (others =>'0'); 
      MS2aReg <= (others =>'0'); 
      MS2bReg <= (others =>'0'); 
      MS3Reg  <= (others =>'0'); 
      MS4Reg  <= (others =>'0'); 
      MS5Reg  <= (others =>'0');
      MS6Reg  <= (others =>'0');
    elsif (clock'event and clock='1') then
      MS1aReg <= SLVMux(MS1aRegNeg,ms1a_inh); 
      MS1bReg <= SLVMux(MS1bRegNeg,ms1b_inh); 
      MS2aReg <= SLVMux(MS2aRegNeg,ms2a_inh); 
      MS2bReg <= SLVMux(MS2bRegNeg,ms2b_inh); 
      MS3Reg  <= SLVMux(MS3RegNeg, ms3_inh); 
      MS4Reg  <= SLVMux(MS4RegNeg, ms4_inh); 
      MS5Reg  <= SLVMux(MS5RegNeg, ms5_inh);
      MS6Reg  <= SLVMux(MS6RegNeg, ms6_inh);
    end if;
  end process;
  pac_comp: pac_all
    port map(
      ResetN		=> ii_reset,
      Clk		=> clock,
      ms1a_in		=> MS1aReg,
      ms1b_in		=> MS1bReg, 
      ms2a_in		=> MS2aReg, 
      ms2b_in		=> MS2bReg, 
      ms3_in		=> MS3Reg, 
      ms4_in		=> MS4Reg, 
      ms5_in		=> MS5Reg,
      ms6_in		=> MS6Reg,
      quality_out	=> quality_out,
      code_out		=> code_out,
      sign_out		=> sign_out 
    );
  --
  -- fast interface
  --
  process(ii_reset, clock)
  begin
    if(ii_reset='0') then
      L1AReg <= '0';
      BCN0Reg <= '0';
      Pretrg0Reg <= '0';
      Pretrg1Reg <= '0';
    elsif(clock'event and clock='1') then
      L1AReg <= l1a;
      BCN0Reg <= bcn0;
      Pretrg0Reg <= pretrg0;
      Pretrg1Reg <= pretrg1;
    end if;
  end process;
  --
  -- internal interface
  --
  LIIEnableSig <= TSLconv((ii_addr(II_ADDR_SIZE-1 downto II_ADDR_USER_SIZE)=(II_PAC0_BASE_ADDR
                  +TNconv(ii_base))) and ii_oper='0');
  LIIAddrLocSig <= ii_addr(II_ADDR_USER_SIZE-1 downto 0);
  --
  process(ii_reset, ii_save)
  begin
    if(ii_reset='0') then
      IIVecInt <= IIReset(IIVecInt,IIPar);
    elsif(ii_save'event and ii_save='1') then
      if(LIIEnableSig='0') then
        IIVecInt <= IISaveAccess(IIVecInt,IIPar,LIIAddrLocSig,IIDataSig);
      end if;
    end if;
  end process;
  --
  IIVecEna <= IIWriteEnable(IIVecInt,IIPar,LIIEnableSig,ii_write,LIIAddrLocSig,IIDataSig);
  IIVecExt <= (  IIWriteAccess(IIVecInt,IIPar,LIIEnableSig,ii_write,LIIAddrLocSig,IIDataSig)
              or IIConnPutWordData(IIVecInt,IIPar,WORD_IDENTIFIER,0,TSLVconv(PAC_IDENTIFIER,II_DATA_SIZE))
              or IIConnPutWordData(IIVecInt,IIPar,WORD_VERSION,0,TSLVconv(PAC_VERSION,II_DATA_SIZE))
	      );
  LIIDataExport <= IIReadAccess(IIVecExt,IIPar,LIIEnableSig,ii_write,LIIAddrLocSig);
  --
  LIIDataExportEnable <= not(LIIEnableSig) and ii_write;
  CIIData: ALTERA_BUSTRI
    generic map (LPM_WIDTH => II_DATA_SIZE)
    port map    (data_in   => LIIDataExport,
                 data_out  => IIDataSig,
                 data_tri  => ii_data,
                 ena       => LIIDataExportEnable);
  CIIInt: ALTERA_TRI
    port map    (data_in   => L,
                 data_tri  => ii_int,
                 ena       => Pretrg0Reg);
  --
  -- diagnostic interface
  --
  led <= (others=>'0') when ((L1AReg=BCN0Reg) and (Pretrg0Reg=Pretrg1Reg)) else
         (others=>'1');

  conf_led <= '0';

end behavioural;
