library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;
library lpm;
use lpm.lpm_components.all;
use work.std_logic_1164_ktp.all;
use work.vcomponent.all;
use work.LPMcomponent.all;
use work.tridaq.all;
package lb_golsrc_iis is
--
-- internal interface
--
constant   TEST_TIME_SIZE  :TN  :=40; --  
constant   GENER_POS_NUM  :TN  :=1024; --  
constant   GENER_POS_SIZE  :TN  :=TVLcreate(GENER_POS_NUM-1); --  
constant   GENER_MEM_ADDR_SIZE  :TVL  :=GENER_POS_SIZE+SLVPartAddrExpand(LB_GOL_DATA,II_DATA_SIZE); --  
constant   RATE_DATA_COUNT_SIZE  :TN  :=32; --  
constant   RATE_DATA_MEM_ADDR_SIZE  :TVL  :=TVLcreate(LB_GOL_DATA-1)+SLVPartAddrExpand(RATE_DATA_COUNT_SIZE,II_DATA_SIZE); --  
constant   GS_IDENTIFIER  :TS  :="GS"; --  
constant   GS_VERSION  :THV  :="0100"; --  
constant   PAGE_REGISTERS  :TN  := 14; --    
constant   PAGE_MEM_GENER  :TN  := 15; --    
constant   PAGE_MEM_RATE_DATA  :TN  := 16; --    
constant   WORD_IDENTIFIER  :TN  := 18; --    
constant   WORD_VERSION  :TN  := 19; --    
constant   VECT_STATUS  :TN  := 21; --    
constant   BITS_STATUS_RATE_START  :TN  := 22; --    
constant   BITS_STATUS_RATE_STOP  :TN  := 23; --    
constant   BITS_STATUS_RATE_ENA  :TN  := 24; --    
constant   BITS_STATUS_PROC_REQ  :TN  := 25; --    
constant   BITS_STATUS_PROC_ACK  :TN  := 26; --    
constant   WORD_TEST_TIME_LIMIT  :TN  := 28; --    
constant   WORD_TEST_TIME_COUNT  :TN  := 29; --    
constant   AREA_MEM_GENER  :TN  := 31; --    
constant   AREA_MEM_RATE_DATA  :TN  := 32; --    
--  " item type                 item ID                 width            num           parent ID     write type     read type               name                        type     description"  
--
constant VIIItemDeclList	:TVIIItemDeclList :=(  
(    VII_PAGE  ,             PAGE_REGISTERS     ,                      0  ,               0    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv(" ")   ,                   VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,           WORD_IDENTIFIER     ,           II_DATA_SIZE  ,               1    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("IDENTIFIER")   ,          VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,              WORD_VERSION     ,           II_DATA_SIZE  ,               1    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("VERSION")   ,             VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_VECT  ,               VECT_STATUS     ,                      0  ,               0    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv("STATUS")   ,              VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_STATUS_RATE_START     ,                      1  ,               1    ,          VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("STATUS_RATE_START")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,    BITS_STATUS_RATE_STOP     ,                      1  ,               1    ,          VECT_STATUS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("STATUS_RATE_STOP")   ,    VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_STATUS_RATE_ENA     ,                      1  ,               1    ,          VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("STATUS_RATE_ENA")   ,     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_STATUS_PROC_REQ     ,                      1  ,               1    ,          VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("STATUS_PROC_REQ")   ,     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_STATUS_PROC_ACK     ,                      1  ,               1    ,          VECT_STATUS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("STATUS_PROC_ACK")   ,     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,      WORD_TEST_TIME_LIMIT     ,         TEST_TIME_SIZE  ,               1    ,       PAGE_REGISTERS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("HIST_TIME_LIMIT")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,      WORD_TEST_TIME_COUNT     ,         TEST_TIME_SIZE  ,               1    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("HIST_TIME_COUNT")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(    VII_PAGE  ,             PAGE_MEM_GENER     ,                      0  ,               0    ,       PAGE_MEM_GENER         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv(" ")   ,                   VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_AREA  ,            AREA_MEM_GENER     ,            LB_GOL_DATA  ,   GENER_POS_NUM    ,       PAGE_MEM_GENER         ,     VII_WACCESS  ,   VII_REXTERNAL   ,   VIINameConv("MEM_GENER")   ,           VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(    VII_PAGE  ,         PAGE_MEM_RATE_DATA     ,                      0  ,               0    ,   PAGE_MEM_RATE_DATA         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv(" ")   ,                   VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_AREA  ,        AREA_MEM_RATE_DATA     ,   RATE_DATA_COUNT_SIZE  ,     LB_GOL_DATA    ,   PAGE_MEM_RATE_DATA         ,     VII_WACCESS  ,   VII_REXTERNAL   ,   VIINameConv("MEM_RATE_DATA")   ,       VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ) 
);
end lb_golsrc_iis;
