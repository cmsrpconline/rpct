library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;

use work.std_logic_1164_ktp.all;
use work.tridaq.all;
use work.lb_golsrc_iis.all;

entity lb_golsrc is
  port(
    data_in					:in    TSLV(LDEMUX_DATA_OUT-1 downto 0);
    data_out					:out   TSLV(LDEMUX_DATA_OUT-1 downto 0);
    -- fast signal interface
    win_open					:in    TSL;
    win_close					:in    TSL;
    clock					:in    TSL;
    l1a						:in    TSL;
    pretrg0					:in    TSL;
    pretrg1					:in    TSL;
    bcn0					:in    TSL;
    -- internal bus interface
    II_addr					:in    TSLV(IILB_ADDR_SIZE-1 downto 0);
    II_data					:inout TSLV(II_DATA_SIZE-1 downto 0);
    II_oper					:in    TSL;
    II_write					:in    TSL;
    II_save					:in    TSL;
    II_reset					:in    TSL;
    II_ack					:inout TSL;
    -- diagnostics signal
    led						:out   TSLV(5 downto 0)
  );
end lb_golsrc;

library lpm;
use lpm.lpm_components.all;

use work.vcomponent.all;
use work.LPMcomponent.all;
use work.LPMsynchro.all;
use work.LPMdiagnostics.all;

architecture behaviour of lb_golsrc is

  constant IIPar				:TVII := TVIICreate(VIIItemDeclList,IILB_ADDR_SIZE,II_DATA_SIZE);

  signal   IIVecInt,IIVecExt,IIVecEna		:TSLV(VII(IIPar)'high downto VEC_INDEX_MIN);
  signal   IIDataSig				:TSLV(II_DATA_SIZE-1 downto 0);
  signal   IIAckInvSig				:TSL;
  signal   LIIDataExport			:TSLV(II_DATA_SIZE-1 downto 0);
  signal   LIIDataExportEnable			:TSL;

  signal   ProcReqSig				:TSL;
  signal   ProcGenerAckSig			:TSL;
  signal   ProcRateDataAckSig			:TSL;
  signal   ProcAckSig				:TSL;

  signal   HistTimerLimitSig			:TSLV(TEST_TIME_SIZE-1 downto 0);
  signal   HistTimerCountSig			:TSLV(TEST_TIME_SIZE-1 downto 0);
  signal   HistTimerStartSig			:TSL;
  signal   HistTimerStopSig			:TSL;
  signal   HistTimerClkEnaSig			:TSL;
  signal   TimerEngineClkEnaSig			:TSL;
  signal   TriggerEnaSelect			:TN;
  signal   TriggerSig				:TSL;
  signal   HistClkEnaSig			:TSL;
  --
  signal   GenerDataInSig			:TSLV(LB_GOL_DATA-1 downto 0);
  signal   GenerDataOutSig			:TSLV(LB_GOL_DATA-1 downto 0);
  signal   GenerMemWrSig			:TSL;
  signal   GenerMemDataOutSig			:TSLV(II_DATA_SIZE-1 downto 0);
  signal   GenerDelaySig			:TSLV(GENER_POS_SIZE-1 downto 0);
  --
  signal   RateDataMemWrSig			:TSL;
  signal   RateDataMemDataOutSig		:TSLV(II_DATA_SIZE-1 downto 0);
  --
  signal   L, H					:TSL;
  signal   ClockInvSig				:TSL;
  --
  signal   L1AReg				:TSL;
  signal   BCN0Reg				:TSL;
  signal   Pretrg0Reg				:TSL;
  signal   Pretrg1Reg				:TSL;
  signal   LocalTrgReg				:TSL;

begin	  

  L <= '0'; H <= '1';
  ClockInvSig <= not(clock);

  --
  -- timer engine
  --
  ProcReqSig <= TSLconv(IIConnGetBitsData(IIVecInt,IIPar,BITS_STATUS_PROC_REQ,0));
  ProcAckSig <= ProcGenerAckSig and ProcRateDataAckSig;

  HistTimerClkEnaSig <= TSLconv(IIConnGetBitsData(IIVecInt,IIPar,BITS_STATUS_RATE_ENA,0));
  HistTimerStartSig  <= TSLconv(IIConnGetBitsData(IIVecInt,IIPar,BITS_STATUS_RATE_START,0));
  HistTimerLimitSig  <= IIConnGetWordData(IIVecInt,IIPar,WORD_TEST_TIME_LIMIT,0);

  HistTimer: LPM_TIMER_ENGINE 
    generic map (
      LPM_TIMER_SIZE	=> TEST_TIME_SIZE
    )
    port map (
      resetN		=> ii_reset,
      clock		=> clock,
      clk_ena_in	=> HistTimerClkEnaSig,
      clk_ena_out	=> TimerEngineClkEnaSig,
      start		=> HistTimerStartSig,
      stop		=> HistTimerStopSig,
      limit		=> HistTimerLimitSig,
      count		=> HistTimerCountSig
    );

  --
  -- rates
  --
  GenerDelaySig  <= (others => '1');
  GenerDataInSig <= (others => '1');
  GenerMemWrSig     <= IIConnGetAreaWrite(IIVecEna,IIPar,AREA_MEM_GENER,ii_save);
  GenerMem: DPM_PROG_PIPE
    generic map(
      LPM_DATA_WIDTH	=> LB_GOL_DATA,
      LPM_DELAY_WIDTH	=> GENER_POS_SIZE,
      LPM_MDATA_WIDTH	=> II_DATA_SIZE
    )
    port map(
      resetN		=> ii_reset,
      clock		=> clock,
      delay		=> GenerDelaySig,
      data_in		=> GenerDataInSig,
      data_out		=> GenerDataOutSig,
      clk_ena		=> TimerEngineClkEnaSig,
      sim_loop		=> H,
      proc_req		=> ProcReqSig,
      proc_ack		=> ProcGenerAckSig,
      memory_addr	=> ii_addr(GENER_MEM_ADDR_SIZE-1 downto 0),
      memory_data_in	=> ii_data,
      memory_data_out	=> GenerMemDataOutSig,
      memory_wr		=> GenerMemWrSig
  );

  RateDataMemWrSig <= IIConnGetAreaWrite(IIVecEna,IIPar,AREA_MEM_RATE_DATA,ii_save);
  RateDataMem: RATE
    generic map(
      LPM_DATA_WIDTH	=> LB_GOL_DATA,
      LPM_COUNT_WIDTH	=> RATE_DATA_COUNT_SIZE,
      LPM_MDATA_WIDTH	=> II_DATA_SIZE
    )
    port map(
      resetN		=> ii_reset,
      clock		=> clock,
      data_in		=> GenerDataOutSig,
      clk_ena		=> TimerEngineClkEnaSig,
      sim_loop		=> L,
      proc_req		=> ProcReqSig,
      proc_ack		=> ProcRateDataAckSig,
      memory_address_in	=> ii_addr(RATE_DATA_MEM_ADDR_SIZE-1 downto 0),
      memory_data_in	=> ii_data,
      memory_data_out	=> RateDataMemDataOutSig,
      memory_wr		=> RateDataMemWrSig
  );

  --
  -- fast interface
  --
  process(ii_reset, clock) is
  begin
    if(ii_reset='0') then
      L1AReg <= '0';
      BCN0Reg <= '0';
      Pretrg0Reg <= '0';
      Pretrg1Reg <= '0';
    elsif(clock'event and clock='1') then
      L1AReg <= l1a;
      BCN0Reg <= bcn0;
      Pretrg0Reg <= pretrg0;
      Pretrg1Reg <= pretrg1;
    end if;
  end process;

  --
  -- internal interface
  --
  
  process(ii_reset, ii_save)
  begin
    if(ii_reset='0') then
      IIVecInt <= IIReset(IIVecInt,IIPar);
    elsif(ii_save'event and ii_save='1') then
      if(ii_oper='0') then
        IIVecInt <= IISaveAccess(IIVecInt,IIPar,ii_addr,IIDataSig);
      end if;
    end if;
  end process;
  --
  IIVecEna <= IIWriteEnable(IIVecInt,IIPar,ii_oper,ii_write,ii_addr,IIDataSig);

  IIVecExt <= (IIWriteAccess(IIVecInt,IIPar,ii_oper,ii_write,ii_addr,IIDataSig)
              or IIConnPutWordData(IIVecInt,IIPar,WORD_IDENTIFIER,0,TSLVconv(GS_IDENTIFIER,II_DATA_SIZE))
              or IIConnPutWordData(IIVecInt,IIPar,WORD_VERSION,0,TSLVconv(GS_VERSION,II_DATA_SIZE))
              or IIConnPutBitsData(IIVecInt,IIPar,BITS_STATUS_RATE_STOP,0,TSLVconv(HistTimerStopSig))
              or IIConnPutBitsData(IIVecInt,IIPar,BITS_STATUS_PROC_ACK,0,TSLVconv(ProcAckSig))
              or IIConnPutWordData(IIVecInt,IIPar,WORD_TEST_TIME_COUNT,0,HistTimerCountSig)

              or IIConnPutAreaData(IIVecInt,IIPar,AREA_MEM_GENER, GenerMemDataOutSig)
              or IIConnPutAreaData(IIVecInt,IIPar,AREA_MEM_RATE_DATA, RateDataMemDataOutSig)
	    );
  LIIDataExport <= IIReadAccess(IIVecExt,IIPar,ii_oper,ii_write,ii_addr);
  --
  LIIDataExportEnable <= not(ii_oper) and ii_write;
  CIIData: ALTERA_BUSTRI
    generic map (LPM_WIDTH => II_DATA_SIZE)
    port map    (data_in   => LIIDataExport,
                 data_out  => IIDataSig,
                 data_tri  => ii_data,
                 ena       => LIIDataExportEnable);
  --
  IIAckInvSig <= not(ii_oper);
  CIIAck: ALTERA_TRI
    port map(data_in  => L, 
             data_tri => ii_ack,
             ena      => IIAckInvSig
    );

  led(0) <= not(OR_REDUCE(GenerDataOutSig));
  led(1) <= not(TimerEngineClkEnaSig);
  led(2) <= not(ii_oper);
  led_clock: LPM_CLOCK_PULSER
    generic map (
      LPM_DIVIDE_PAR		=> CLK_FREQ/LED_PULSE_FREQ,
      LPM_DATA_PULSE_WIDTH	=> LED_PULSE_WIDTH,
      LPM_PULSE_INVERT		=> TRUE	
    )
    port map (
      resetN			=> II_reset,
      clock			=> clock,
      pulse			=> led(3)
    );


end behaviour;

