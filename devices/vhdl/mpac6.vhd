library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use work.std_logic_1164_ktp.all;
use work.pac_packet6.all;
use work.pac_patt6.all;

entity pac_side is
  generic(
    Sign_side	:integer := 0
  );
  port(
    ResetN		:IN  std_logic;
    Clk			:IN  std_logic;
    ms1_in		:IN  Tms1; 
    ms2_in		:IN  Tms2; 
    ms3_in		:IN  Tms3; 
    ms4_in		:IN  Tms4; 
    ms5_in		:IN  Tms5;
    ms6_in		:IN  Tms6;
    quality_out	:OUT TQualityVal;
    code_out	:OUT TCodeVal 
  );
end pac_side;
architecture behaviour of pac_side is

  type     TCQualityBus	is array (TCode'range) of TQuality;
  type     TQCodeBus	is array (TQuality'range) of TCode;
--

  -- muon track pattern decodes function for OR1
  function GetPatternCodeOR1(
    ms1_or1	:IN  Tms1; 
    ms2_or1	:IN  Tms2; 
    ms3_or1	:IN  Tms3; 
    ms4_or1	:IN  Tms4; 
    ms5_or1	:IN  Tms5;
    ms6_or1	:IN  Tms6;
    index	:IN  natural;
    LayerA	:IN  integer;
    LayerB	:IN  integer;
    LayerC	:IN  integer;
    LayerD	:IN  integer;
    LayerE	:IN  integer
  ) return TQuality is

    subtype  TLayer1	is std_logic_vector(12 downto 0);
    subtype  TLayer3	is std_logic_vector(10 downto 0);
    subtype  TLayer4	is std_logic_vector(10 downto 0);
    subtype  TLayer5	is std_logic_vector(10 downto 0);
    subtype  TLayer6	is std_logic_vector(10 downto 0);
  
    variable Layer1		:TLayer1;
    variable Layer3		:TLayer3;
    variable Layer4		:TLayer4;
    variable Layer5		:TLayer5;
    variable Layer6		:TLayer6;
    variable l1,l2,l3,l4,l5,l6:std_logic;
    variable hit_number	:natural;
    variable quality	:TQuality;
--
  begin
	-- local buses construction
    Layer1(0) := OR_REDUCE(ms1_or1(3+index downto index));  
    Layer1(1) := OR_REDUCE(ms1_or1(6+index downto 4+index));  
    Layer1(2) := OR_REDUCE(ms1_or1(8+index downto 7+index));
    Layer1(9 downto 3) := ms1_or1(15+index downto 9+index);   
    Layer1(10) := OR_REDUCE(ms1_or1(17+index downto 16+index));
    Layer1(11) := OR_REDUCE(ms1_or1(20+index downto 18+index));
    Layer1(12) := OR_REDUCE(ms1_or1(24+index downto 21+index));

    Layer3(0) := OR_REDUCE(ms3_or1(2+index downto index));  
    Layer3(1) := OR_REDUCE(ms3_or1(4+index downto 3+index));
    Layer3(8 downto 2) := ms3_or1(11+index downto 5+index);   
    Layer3(9) := OR_REDUCE(ms3_or1(13+index downto 12+index));
    Layer3(10) := OR_REDUCE(ms3_or1(16+index downto 14+index));

    Layer4(0) := OR_REDUCE(ms4_or1(2+index downto index));  
    Layer4(1) := OR_REDUCE(ms4_or1(4+index downto 3+index));
    Layer4(8 downto 2) := ms4_or1(11+index downto 5+index);   
    Layer4(9) := OR_REDUCE(ms4_or1(13+index downto 12+index));
    Layer4(10) := OR_REDUCE(ms4_or1(16+index downto 14+index));

    Layer5(0) := OR_REDUCE(ms5_or1(5+index downto index));  
    Layer5(1) := OR_REDUCE(ms5_or1(8+index downto 6+index));
    Layer5(8 downto 2) := ms5_or1(15+index downto 9+index);   
    Layer5(9) := OR_REDUCE(ms5_or1(18+index downto 16+index));
    Layer5(10) := OR_REDUCE(ms5_or1(24+index downto 19+index));

    Layer6(0) := OR_REDUCE(ms6_or1(5+index downto index));  
    Layer6(1) := OR_REDUCE(ms6_or1(8+index downto 6+index));
    Layer6(8 downto 2) := ms6_or1(15+index downto 9+index);   
    Layer6(9) := OR_REDUCE(ms6_or1(18+index downto 16+index));
    Layer6(10) := OR_REDUCE(ms6_or1(24+index downto 19+index));

    -- track bits selection
    l1 := Layer1(6+LayerA);
    l2 := ms2_or1(index);
    l3 := Layer3(5+LayerB);
    l4 := Layer4(5+LayerC);
    l5 := Layer5(5+LayerD);
    l6 := Layer6(5+LayerE);

    -- track finder
	hit_number:=SLVBit1Count((l1,l2,l3,l4,l5,l6));
	quality(3) := '0'; if (hit_number=6) then quality(3) := '1'; end if;
	quality(2) := '0'; if (hit_number=5) then quality(2) := '1'; end if;
	quality(1) := '0'; if (hit_number=4) then quality(1) := '1'; end if;
	quality(0) := '0'; if (hit_number=3) then quality(0) := '1'; end if;
    return(quality);

  end;

  -- higest quality bus selector
  function CodeBusSelect(val :TQCodeBus) return TQualityVal is
  begin
    for index in TQCodeBus'range loop
      if(OR_REDUCE(val(index))='1') then
        return(index);
      end if;
    end loop;
    return(0);
  end;
      
  -- encoder function
  function CodeBusEncode(val :TCode) return TCodeVal is
  begin
    for position in TCode'range loop
      if (val(position)='1') then
        return(position);
      end if;
    end loop;
    return(0);
  end;
            
  signal   ms1_reg		:Tms1; 
  signal   ms2_reg		:Tms2; 
  signal   ms3_reg		:Tms3; 
  signal   ms4_reg		:Tms4; 
  signal   ms5_reg		:Tms5;
  signal   ms6_reg		:Tms6;
  signal   QCodeBus		:TQCodeBus;
  signal   quality_reg	:TQualityVal;
  signal   code_reg		:TCodeVal;

begin

  process(Clk, ResetN)

    variable CQualityBus:TCQualityBus;
    variable CodeBus	:TCode;
    variable Quality	:TQualityVal;
    variable Code		:TCodeVal;

  begin

    if (ResetN='0') then

      ms1_reg  <= (others => '0'); 
      ms2_reg  <= (others => '0'); 
      ms3_reg  <= (others => '0'); 
      ms4_reg  <= (others => '0'); 
      ms5_reg  <= (others => '0');
      ms6_reg  <= (others => '0');
      QCodeBus     <= (TQuality'range => (others => '0'));
      quality_reg <= 0; 
      code_reg    <= 0; 

    elsif (Clk'event and Clk='1') then

      ms1_reg <= ms1_in; 
      ms2_reg <= ms2_in; 
      ms3_reg <= ms3_in; 
      ms4_reg <= ms4_in; 
      ms5_reg <= ms5_in;
      ms6_reg <= ms6_in;

      -- patterns finder
      CQualityBus := (TCode'range => (others => '0'));
      for index in PattTable'range loop
        case (PattTable(index).blk_type) is
          when 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 => 
            if(PattTable(index).sign=Sign_side) then
              CQualityBus(PattTable(index).code) := CQualityBus(PattTable(index).code) or
              GetPatternCodeOR1(ms1_reg,ms2_reg,ms3_reg,ms4_reg,ms5_reg,ms6_reg,
                                PattTable(index).blk_type,
                                PattTable(index).LayerA,
    			        PattTable(index).LayerB,
   			        PattTable(index).LayerC,
    			        PattTable(index).LayerD,
    			        PattTable(index).LayerE);
    	     end if;
        end case;
      end loop;
      -- buses conversion
      for qindex in TQuality'range loop
        for cindex in TCode'range loop
          QCodeBus(qindex)(cindex) <= CQualityBus(cindex)(qindex);
        end loop;
      end loop;

      -- highest code finder
      Quality := CodeBusSelect(QCodeBus);
      Code    := CodeBusEncode(QCodeBus(Quality));
      
      -- output laches
      quality_reg <= Quality;
      code_reg    <= Code;      

    end if;
 
  end process;

  quality_out <= quality_reg;
  code_out    <= code_reg;
  
end behaviour;

------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use work.std_logic_1164_ktp.all;
use work.pac_packet6.all;
use work.pac_patt6.all;

entity pac_sort is

  port(
    ResetN			:IN  std_logic;
    Clk				:IN  std_logic;
    Pquality_in		:IN  TQualityVal;
    Pcode_in		:IN  TCodeVal; 
    Nquality_in		:IN  TQualityVal;
    Ncode_in		:IN  TCodeVal; 
    quality_out		:OUT TQualityVal;
    code_out		:OUT TCodeVal; 
    sign_out		:OUT std_logic 
  );
end pac_sort;
architecture behaviour of pac_sort is

  signal quality_reg	:TQualityVal;
  signal code_reg		:TCodeVal; 
  signal sign_reg		:std_logic;

begin

  process(Clk, ResetN)

  begin

    if (ResetN='0') then

      quality_reg <= 0; 
      code_reg    <= 0; 
      sign_reg    <= '0';

    elsif (Clk'event and Clk='1') then

      -- output laches
      if(NQuality_in>PQuality_in) then
        quality_reg <= NQuality_in;
        code_reg    <= NCode_in;      
        sign_reg    <= '1';
      else
        quality_reg <= PQuality_in;
        code_reg    <= PCode_in;      
        sign_reg    <= '0';
      end if;

    end if;
  
  end process;

  quality_out <= quality_reg;
  code_out    <= code_reg;
  sign_out    <= sign_reg;

end behaviour;

------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use work.std_logic_1164_ktp.all;
use work.pac_packet6.all;
use work.pac_patt6.all;

entity pac_pos is

  port(
    ResetN			:IN  std_logic;
    Clk				:IN  std_logic;
    ms1_in			:IN  Tms1; 
    ms2_in			:IN  Tms2; 
    ms3_in			:IN  Tms3; 
    ms4_in			:IN  Tms4; 
    ms5_in			:IN  Tms5;
    ms6_in			:IN  Tms6;
    quality_out		:OUT TQualityVal;
    code_out		:OUT TCodeVal
  );
end pac_pos;
architecture behaviour of pac_pos is

  component pac_side is
    generic(
      Sign_side		:integer := 1
    );
    port(
      ResetN		:IN  std_logic;
      Clk			:IN  std_logic;
      ms1_in		:IN  Tms1; 
      ms2_in		:IN  Tms2; 
      ms3_in		:IN  Tms3; 
      ms4_in		:IN  Tms4; 
      ms5_in		:IN  Tms5;
      ms6_in		:IN  Tms6;
      quality_out	:OUT TQualityVal;
      code_out		:OUT TCodeVal
    );
  end component;

begin

  Pac_pos_code : pac_side
    port map(
      ResetN  => ResetN,
      Clk	  => Clk,
      ms1_in  => ms1_in, 
      ms2_in  => ms2_in, 
      ms3_in  => ms3_in, 
      ms4_in  => ms4_in, 
      ms5_in  => ms5_in,
      ms6_in  => ms6_in,
      quality_out => quality_out,
      code_out    => code_out
    );

end behaviour;

------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use work.std_logic_1164_ktp.all;
use work.pac_packet6.all;
use work.pac_patt6.all;

entity pac_neg is

  port(
    ResetN			:IN  std_logic;
    Clk				:IN  std_logic;
    ms1_in			:IN  Tms1; 
    ms2_in			:IN  Tms2; 
    ms3_in			:IN  Tms3; 
    ms4_in			:IN  Tms4; 
    ms5_in			:IN  Tms5;
    ms6_in			:IN  Tms6;
    Pquality_in		:IN  TQualityVal;
    Pcode_in		:IN  TCodeVal; 
    quality_out		:OUT TQualityVal;
    code_out		:OUT TCodeVal; 
    sign_out		:OUT std_logic 
  );
end pac_neg;
architecture behaviour of pac_neg is

  component pac_side is
    generic(
      Sign_side		:integer := -1
    );
    port(
      ResetN		:IN  std_logic;
      Clk			:IN  std_logic;
      ms1_in		:IN  Tms1; 
      ms2_in		:IN  Tms2; 
      ms3_in		:IN  Tms3; 
      ms4_in		:IN  Tms4; 
      ms5_in		:IN  Tms5;
      ms6_in		:IN  Tms6;
      quality_out	:OUT TQualityVal;
      code_out		:OUT TCodeVal
    );
  end component;

  component pac_sort is
    port(
      ResetN		:IN  std_logic;
      Clk			:IN  std_logic;
      Pquality_in	:IN  TQualityVal;
      Pcode_in		:IN  TCodeVal; 
      Nquality_in	:IN  TQualityVal;
      Ncode_in		:IN  TCodeVal; 
      quality_out	:OUT TQualityVal;
      code_out		:OUT TCodeVal; 
      sign_out		:OUT std_logic 
    );
  end component;

  signal Nquality	:TQualityVal;
  signal Ncode		:TCodeVal; 

begin

  Pac_neg_code : pac_side
    port map(
      ResetN  => ResetN,
      Clk	  => Clk,
      ms1_in  => ms1_in, 
      ms2_in  => ms2_in, 
      ms3_in  => ms3_in, 
      ms4_in  => ms3_in, 
      ms5_in  => ms5_in,
      ms6_in  => ms5_in,
      quality_out => Nquality,
      code_out    => Ncode
    );

  Pac_sort_block : pac_sort
    port map(
      ResetN  => ResetN,
      Clk	  => Clk,
      Pquality_in => Pquality_in,
      Pcode_in    => Pcode_in,
      Nquality_in => Nquality,
      Ncode_in    => Ncode,
      quality_out => quality_out,
      code_out    => code_out,
      sign_out    => sign_out 
  );

end behaviour;

------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use work.std_logic_1164_ktp.all;
use work.pac_packet6.all;
use work.pac_patt6.all;

entity pac_allh is

  port(
    ResetN		:IN  std_logic;
    Clk			:IN  std_logic;
    ms1a_inh	:IN  Tms1h; 
    ms1b_inh	:IN  Tms1h; 
    ms2a_inh	:IN  Tms2h; 
    ms2b_inh	:IN  Tms2h; 
    ms3_inh		:IN  Tms3h; 
    ms4_inh		:IN  Tms4h; 
    ms5_inh		:IN  Tms5h;
    ms6_inh		:IN  Tms6h;
    quality_out	:OUT TQualityVal;
    code_out	:OUT TCodeVal; 
    sign_out	:OUT std_logic 
  );
end pac_allh;
architecture behaviour of pac_allh is
	signal ms1_in	:Tms1; 
	signal ms2_in	:Tms2; 
	signal ms3_in	:Tms3; 
	signal ms4_in	:Tms4; 
	signal ms5_in	:Tms5;
	signal ms6_in	:Tms6;
	signal ms1_inh	:Tms1h; 
	signal ms2_inh	:Tms2h; 
	signal ms1_inhd	:Tms1h; 
	signal ms2_inhd	:Tms2h; 
	signal ms3_inhd	:Tms3h; 
	signal ms4_inhd	:Tms4h; 
	signal ms5_inhd	:Tms5h;
	signal ms6_inhd	:Tms6h;

  component pac_side is
    generic(
      Sign_side	:integer :=0
    );
    port(
      ResetN	:IN  std_logic;
      Clk		:IN  std_logic;
      ms1_in	:IN  Tms1; 
      ms2_in	:IN  Tms2; 
      ms3_in	:IN  Tms3; 
      ms4_in	:IN  Tms4; 
      ms5_in	:IN  Tms5;
      ms6_in	:IN  Tms6;
      quality_out	:OUT TQualityVal;
      code_out		:OUT TCodeVal
    );
  end component;

  component pac_sort is
    port(
      ResetN		:IN  std_logic;
      Clk			:IN  std_logic;
      Pquality_in	:IN  TQualityVal;
      Pcode_in		:IN  TCodeVal; 
      Nquality_in	:IN  TQualityVal;
      Ncode_in		:IN  TCodeVal; 
      quality_out	:OUT TQualityVal;
      code_out		:OUT TCodeVal; 
      sign_out		:OUT std_logic 
    );
  end component;

  signal Pquality	:TQualityVal;
  signal Pcode		:TCodeVal; 
  signal Nquality	:TQualityVal;
  signal Ncode		:TCodeVal; 

begin
	ms1_inh	 <= ms1a_inh OR ms1b_inh;
	ms2_inh	 <= ms2a_inh OR ms2b_inh;
--	
x1ab:for i in 0 to Tms1h'Left generate
	 process(Clk)
	  begin
	    if (Clk'event and Clk='1') then
			ms1_in(2*i) <= ms1_inhd(i);
			ms1_in(2*i+1) <= ms1_inh(i);
	    end if;
	 end process;
end generate;
	 process(Clk)
	  begin
	  	if (Clk'event and Clk='0') then
			 ms1_inhd <= ms1_inh;
	    end if;
	 end process;
x2ab:for i in 0 to Tms2h'Left generate
	 process(Clk)
	  begin
	    if (Clk'event and Clk='1') then
			ms2_in(2*i) <= ms2_inhd(i);
			ms2_in(2*i+1) <= ms2_inh(i);
	    end if;
	 end process;
end generate;
	 process(Clk)
	  begin
	  	if (Clk'event and Clk='0') then
			 ms2_inhd <= ms2_inh;
	    end if;
	 end process;
x3:for i in 0 to Tms3h'Left generate
	 process(Clk)
	  begin
	    if (Clk'event and Clk='1') then
			ms3_in(2*i) <= ms3_inhd(i);
			ms3_in(2*i+1) <= ms3_inh(i);
	    end if;
	 end process;
end generate;
	 process(Clk)
	  begin
	  	if (Clk'event and Clk='0') then
			 ms3_inhd <= ms3_inh;
	    end if;
	 end process;
x4:for i in 0 to Tms4h'Left generate
	 process(Clk)
	  begin
	    if (Clk'event and Clk='1') then
			ms4_in(2*i) <= ms4_inhd(i);
			ms4_in(2*i+1) <= ms4_inh(i);
	    end if;
	 end process;
end generate;
	 process(Clk)
	  begin
	  	if (Clk'event and Clk='0') then
			 ms4_inhd <= ms4_inh;
	    end if;
	 end process;
x5:for i in 0 to Tms5h'Left generate
	 process(Clk)
	  begin
	    if (Clk'event and Clk='1') then
			ms5_in(2*i) <= ms5_inhd(i);
			ms5_in(2*i+1) <= ms5_inh(i);
	    end if;
	 end process;
end generate;
	 process(Clk)
	  begin
	  	if (Clk'event and Clk='0') then
			 ms5_inhd <= ms5_inh;
	    end if;
	 end process;
x6:for i in 0 to Tms6h'Left generate
	 process(Clk)
	  begin
	    if (Clk'event and Clk='1') then
			ms6_in(2*i) <= ms6_inhd(i);
			ms6_in(2*i+1) <= ms6_inh(i);
	    end if;
	 end process;
end generate;
	 process(Clk)
	  begin
	  	if (Clk'event and Clk='0') then
			 ms6_inhd <= ms6_inh;
	    end if;
	 end process;

  Pac_neg_code : pac_side
    generic map(
      Sign_side	  => -1
    )
    port map(
      ResetN  => ResetN,
      Clk	  => Clk,
      ms1_in  => ms1_in, 
      ms2_in  => ms2_in, 
      ms3_in  => ms3_in, 
      ms4_in  => ms4_in, 
      ms5_in  => ms5_in,
      ms6_in  => ms6_in,
      quality_out => Nquality,
      code_out    => Ncode
    );

  Pac_pos_code : pac_side
    generic map(
      Sign_side	  => 1
    )
    port map(
      ResetN  => ResetN,
      Clk	  => Clk,
      ms1_in  => ms1_in, 
      ms2_in  => ms2_in, 
      ms3_in  => ms3_in, 
      ms4_in  => ms4_in, 
      ms5_in  => ms5_in,
      ms6_in  => ms6_in,
      quality_out => Pquality,
      code_out    => Pcode
    );

  Pac_sort_block : pac_sort
    port map(
      ResetN  => ResetN,
      Clk	  => Clk,
      Pquality_in => Pquality,
      Pcode_in    => Pcode,
      Nquality_in => Nquality,
      Ncode_in    => Ncode,
      quality_out => quality_out,
      code_out    => code_out,
      sign_out    => sign_out 
  );

end behaviour;

------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use work.std_logic_1164_ktp.all;
use work.pac_packet6.all;
use work.pac_patt6.all;

entity pac_all is

  port(
    ResetN		:IN  std_logic;
    Clk			:IN  std_logic;
    ms1a_in		:IN  Tms1; 
    ms1b_in		:IN  Tms1; 
    ms2a_in		:IN  Tms2; 
    ms2b_in		:IN  Tms2; 
    ms3_in		:IN  Tms3; 
    ms4_in		:IN  Tms4; 
    ms5_in		:IN  Tms5;
    ms6_in		:IN  Tms6;
    quality_out		:OUT TQualityVal;
    code_out		:OUT TCodeVal; 
    sign_out		:OUT std_logic 
  );
end pac_all;
architecture behaviour of pac_all is

  component pac_side is
    generic(
      Sign_side	:integer :=0
    );
    port(
      ResetN		:IN  std_logic;
      Clk		:IN  std_logic;
      ms1_in		:IN  Tms1; 
      ms2_in		:IN  Tms2; 
      ms3_in		:IN  Tms3; 
      ms4_in		:IN  Tms4; 
      ms5_in		:IN  Tms5;
      ms6_in		:IN  Tms6;
      quality_out	:OUT TQualityVal;
      code_out		:OUT TCodeVal
    );
  end component;

  component pac_sort is
    port(
      ResetN		:IN  std_logic;
      Clk		:IN  std_logic;
      Pquality_in	:IN  TQualityVal;
      Pcode_in		:IN  TCodeVal; 
      Nquality_in	:IN  TQualityVal;
      Ncode_in		:IN  TCodeVal; 
      quality_out	:OUT TQualityVal;
      code_out		:OUT TCodeVal; 
      sign_out		:OUT std_logic 
    );
  end component;

  signal ms1_in		:Tms1; 
  signal ms2_in		:Tms2; 
  signal Pquality	:TQualityVal;
  signal Pcode		:TCodeVal; 
  signal Nquality	:TQualityVal;
  signal Ncode		:TCodeVal; 

begin

  ms1_in <= (ms1a_in or ms1b_in);
  ms2_in <= (ms2a_in or ms2b_in);

  Pac_neg_code : pac_side
    generic map(
      Sign_side	  => -1
    )
    port map(
      ResetN  => ResetN,
      Clk	  => Clk,
      ms1_in  => ms1_in, 
      ms2_in  => ms2_in, 
      ms3_in  => ms3_in, 
      ms4_in  => ms4_in, 
      ms5_in  => ms5_in,
      ms6_in  => ms6_in,
      quality_out => Nquality,
      code_out    => Ncode
    );

  Pac_pos_code : pac_side
    generic map(
      Sign_side	  => 1
    )
    port map(
      ResetN  => ResetN,
      Clk	  => Clk,
      ms1_in  => ms1_in, 
      ms2_in  => ms2_in, 
      ms3_in  => ms3_in, 
      ms4_in  => ms4_in, 
      ms5_in  => ms5_in,
      ms6_in  => ms6_in,
      quality_out => Pquality,
      code_out    => Pcode
    );

  Pac_sort_block : pac_sort
    port map(
      ResetN  => ResetN,
      Clk	  => Clk,
      Pquality_in => Pquality,
      Pcode_in    => Pcode,
      Nquality_in => Nquality,
      Ncode_in    => Ncode,
      quality_out => quality_out,
      code_out    => code_out,
      sign_out    => sign_out 
  );

end behaviour;