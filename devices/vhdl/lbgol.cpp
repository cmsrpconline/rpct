//---------------------------------------------------------------------------

//
//  Michal Pietrusinski 2001
//  Warsaw University
//

#include "precompiled.h"
#pragma hdrstop

#include "lbgol.h"
#include "tridaq_def.h"

   
#pragma package(smart_init)

//---------------------------------------------------------------------------  

/////////////////////////////////////////////////////////////////////////////
//////////////////   lb_control   ///////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

#define IID_FILE "vhdl/lb_control.iid"
#define IID_CLASS TLBGolControl
#include "iid2c2.h"     

TLBGolControl::TLBGolControl(int vme_board_address, IBoard& board): TLBGolDevice(
     LB_CONTR_IDENTIFIER.c_str(),
     WORD_IDENTIFIER,
     "LB_CONTROL",
     LB_CONTR_VERSION.c_str(),
     WORD_VERSION,
     0,
     board,
     BuildMemoryMap(),
     vme_board_address
     ),
     I2C(NULL),
     DiagCtrl(*this,
              VECT_STATUS,
              BITS_STATUS_PROC_REQ,
              BITS_STATUS_PROC_ACK,
              BITS_STATUS_TIMER_ENA,
              BITS_STATUS_TIMER_ENA,
              BITS_STATUS_TIMER_START,
              BITS_STATUS_TIMER_STOP,
              WORD_TIMER_LIMIT,
              WORD_TIMER_COUNT,
              false)
{
}


/////////////////////////////////////////////////////////////////////////////
//////////////////   TLBGolSrc     ///////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

#define IID_FILE "vhdl/lb_golsrc.iid"
#define IID_CLASS TLBGolSrc
#include "iid2c2.h"


TLBGolSrc::TLBGolSrc(int vme_board_address, IBoard& board): TLBGolDevice(
     GS_IDENTIFIER.c_str(),
     WORD_IDENTIFIER,
     "LB_GOLSRC",
     GS_VERSION.c_str(),
     WORD_VERSION,
     1,
     board,
     BuildMemoryMap(),
     vme_board_address
     ), DiagCtrl(NULL) //RunCtrl(NULL)
{
}

TLBGolSrc::~TLBGolSrc()
{
  //if (RunCtrl != NULL)
  //  delete RunCtrl;

  if (DiagCtrl != NULL)
    delete DiagCtrl;

}




/////////////////////////////////////////////////////////////////////////////
//////////////////   lb_goldst     ///////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

                 
#define IID_FILE "vhdl/lb_goldst.iid"
#define IID_CLASS TLBGolDest
#include "iid2c2.h"

TLBGolDest::TLBGolDest(int vme_board_address, IBoard& board): TLBGolDevice(
     GS_IDENTIFIER.c_str(),
     WORD_IDENTIFIER,
     "LB_GOLDST",
     GS_VERSION.c_str(),
     WORD_VERSION,
     2,
     board,
     BuildMemoryMap(),
     vme_board_address
     ), ErrHisto(NULL), ErrHistoMgr(NULL), DiagCtrl(NULL)
{
}

TLBGolDest::~TLBGolDest()
{
  if (ErrHistoMgr != NULL)
    delete ErrHistoMgr;

  if (ErrHisto != NULL)
    delete ErrHisto;

  //if (RunCtrl != NULL)
  //  delete RunCtrl;

  if (DiagCtrl != NULL)
    delete DiagCtrl;
}



#undef pow2




TLinkBoardGol::TLinkBoardGol(TVMEInterface* vme, int vme_board_address)
: LBControl(vme_board_address, *this), LBGolSrc(vme_board_address, *this),
  LBGolDest(vme_board_address, *this),
  GolI2C(31), VMEAddress(vme_board_address)
{   
  std::ostringstream ost;
  ost << "Link Board GOL " << std::hex << VMEAddress;
  Name = ost.str();

  LBControl.getMemoryMap().SetHardwareIfc(
          new TIIVMEAccess(
                LBControl.GetVMEBoardBaseAddr(),
                LBControl.getBaseAddress() << LBControl.GetVMEBaseAddrShift(),
                vme,
                3 << 18),
          true);
  Devices.push_back(&LBControl);

  LBGolSrc.getMemoryMap().SetHardwareIfc(
          new TIIVMEAccess(
                LBGolSrc.GetVMEBoardBaseAddr(),
                LBGolSrc.getBaseAddress() << LBGolSrc.GetVMEBaseAddrShift(),
                vme,
                3 << 18),
          true);
  Devices.push_back(&LBGolSrc);

  LBGolDest.getMemoryMap().SetHardwareIfc(
          new TIIVMEAccess(
                LBGolDest.GetVMEBoardBaseAddr(),
                LBGolDest.getBaseAddress() << LBGolDest.GetVMEBaseAddrShift(),
                vme,
                3 << 18),
          true);
  Devices.push_back(&LBGolDest);
}