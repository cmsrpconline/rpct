library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;
library lpm;
use lpm.lpm_components.all;
use work.std_logic_1164_ktp.all;
use work.vcomponent.all;
use work.LPMcomponent.all;
use work.tridaq.all;
package lsynchro_iis is
--
-- internal interface
--
constant   SYN_IDENTIFIER  :TS  :="SC"; --  
constant   SYN_VERSION	  :THV  :="0200"; --  
constant   PAGE_REGISTERS  :TN  := 4; --    
constant   PAGE_DAQ_MEM_DELAY  :TN  := 5; --    
constant   PAGE_DAQ_MEM_DATA  :TN  := 6; --    
constant   PAGE_DAQ_MEM_CNT  :TN  := 7; --    
constant   WORD_IDENTIFIER  :TN  := 9; --    
constant   WORD_VERSION  :TN  := 10; --    
constant   VECT_STATUS  :TN  := 12; --    
constant   BITS_STATUS_CHAN_SEL  :TN  := 13; --    
constant   BITS_STATUS_PROC_REQ  :TN  := 14; --    
constant   BITS_STATUS_PROC_ACK  :TN  := 15; --    
constant   VECT_SYN0  :TN  := 17; --    
constant   BITS_SYN0_CLK_QUALITY  :TN  := 18; --    
constant   BITS_SYN0_PHASE_QUALITY  :TN  := 19; --    
constant   BITS_SYN0_DATA_PARITY  :TN  := 20; --    
constant   BITS_SYN0_BCN0_SYNCH  :TN  := 21; --    
constant   BITS_SYN0_BCN_SYNCH  :TN  := 22; --    
constant   BITS_SYN0_LATENCY  :TN  := 23; --    
constant   VECT_SYN1  :TN  := 25; --    
constant   BITS_SYN1_CLK_QUALITY  :TN  := 26; --    
constant   BITS_SYN1_PHASE_QUALITY  :TN  := 27; --    
constant   BITS_SYN1_DATA_PARITY  :TN  := 28; --    
constant   BITS_SYN1_BCN0_SYNCH  :TN  := 29; --    
constant   BITS_SYN1_BCN_SYNCH  :TN  := 30; --    
constant   BITS_SYN1_LATENCY  :TN  := 31; --    
constant   WORD_DAQ_DELAY  :TN  := 33; --    
constant   AREA_DAQ_MEM_DELAY  :TN  := 35; --    
constant   AREA_DAQ_MEM_DATA  :TN  := 36; --    
constant   AREA_DAQ_MEM_CNT  :TN  := 37; --    
--  " item type                  item ID          width                  num       parent ID     write type     read type               name                        type     description"  
--
constant VIIItemDeclList	:TVIIItemDeclList :=(  
(    VII_PAGE  ,     PAGE_REGISTERS     ,                              0  ,                     0    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv(" ")   ,                         VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,    WORD_IDENTIFIER     ,                  II_DATA_SIZE  ,                     1    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("WORD_IDENTIFIER")   ,           VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,    WORD_VERSION     ,                     II_DATA_SIZE  ,                     1    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("WORD_VERSION")   ,              VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_VECT  ,    VECT_STATUS     ,                                 0  ,                     0    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv("VECT_STATUS")   ,               VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_STATUS_CHAN_SEL     ,             TVLcreate(1)  ,                     1    ,          VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("BITS_STATUS_CHAN_SEL")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_STATUS_PROC_REQ     ,                        1  ,                     1    ,          VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("BITS_STATUS_PROC_REQ")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_STATUS_PROC_ACK     ,                        1  ,                     1    ,          VECT_STATUS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("BITS_STATUS_PROC_ACK")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_VECT  ,    VECT_SYN0     ,                                   0  ,                     0    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv("VECT_SYN0 ")   ,                VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_SYN0_CLK_QUALITY     ,                       1  ,                     1    ,            VECT_SYN0         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("BITS_SYN0_CLK_QUALITY")   ,     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_SYN0_PHASE_QUALITY     ,                     1  ,                     1    ,            VECT_SYN0         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("BITS_SYN0_PHASE_QUALITY")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_SYN0_DATA_PARITY     ,                       1  ,                     1    ,            VECT_SYN0         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("BITS_SYN0_DATA_PARITY")   ,     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_SYN0_BCN0_SYNCH     ,                        1  ,                     1    ,            VECT_SYN0         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("BITS_SYN0_BCN0_SYNCH")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_SYN0_BCN_SYNCH     ,                         1  ,                     1    ,            VECT_SYN0         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("BITS_SYN0_BCN_SYNCH")   ,       VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_SYN0_LATENCY     ,               SYN_PIPE_SIZE  ,                     1    ,            VECT_SYN0         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("BITS_SYN0_LATENCY")   ,         VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_VECT  ,    VECT_SYN1     ,                                   0  ,                     0    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv("VECT_SYN1")   ,                 VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_SYN1_CLK_QUALITY     ,                       1  ,                     1    ,            VECT_SYN1         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("BITS_SYN1_CLK_QUALITY")   ,     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_SYN1_PHASE_QUALITY     ,                     1  ,                     1    ,            VECT_SYN1         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("BITS_SYN1_PHASE_QUALITY")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_SYN1_DATA_PARITY     ,                       1  ,                     1    ,            VECT_SYN1         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("BITS_SYN1_DATA_PARITY")   ,     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_SYN1_BCN0_SYNCH     ,                        1  ,                     1    ,            VECT_SYN1         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("BITS_SYN1_BCN0_SYNCH")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_SYN1_BCN_SYNCH     ,                         1  ,                     1    ,            VECT_SYN1         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("BITS_SYN1_BCN_SYNCH ")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_SYN1_LATENCY     ,               SYN_PIPE_SIZE  ,                     1    ,            VECT_SYN1         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("BITS_SYN1_LATENCY")   ,         VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,    WORD_DAQ_DELAY     ,            DELAY_PIPELINE_SIZE  ,                     1    ,       PAGE_REGISTERS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("WORD_DAQ_DELAY")   ,            VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(    VII_PAGE  ,     PAGE_DAQ_MEM_DELAY     ,                          0  ,                     0    ,   PAGE_DAQ_MEM_DELAY         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv(" ")   ,                         VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_AREA  ,    AREA_DAQ_MEM_DELAY     ,        DPM_DELAY_DATA_SIZE  ,   DPM_DELAY_ADDR_SIZE    ,   PAGE_DAQ_MEM_DELAY         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("AREA_DAQ_MEM_DELAY")   ,        VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(    VII_PAGE  ,     PAGE_DAQ_MEM_DATA     ,                           0  ,                     0    ,   PAGE_DAQ_MEM_DELAY         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv(" ")   ,                         VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_AREA  ,    AREA_DAQ_MEM_DATA     ,          DPM_DATA_DATA_SIZE  ,    DPM_DATA_ADDR_SIZE    ,   PAGE_DAQ_MEM_DELAY         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("AREA_DAQ_MEM_DATA")   ,         VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(    VII_PAGE  ,     PAGE_DAQ_MEM_CNT     ,                            0  ,                     0    ,   PAGE_DAQ_MEM_DELAY         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv(" ")   ,                         VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_AREA  ,    AREA_DAQ_MEM_CNT     ,            DPM_CNT_DATA_SIZE  ,     DPM_CNT_ADDR_SIZE    ,   PAGE_DAQ_MEM_DELAY         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("AREA_DAQ_MEM_CNT")   ,          VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ) 
);
end lsynchro_iis;
