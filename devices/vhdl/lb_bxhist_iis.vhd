library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;
library lpm;
use lpm.lpm_components.all;
use work.std_logic_1164_ktp.all;
use work.vcomponent.all;
use work.LPMcomponent.all;
use work.tridaq.all;
package lb_bxhist_iis is
--
-- internal interface
--
constant   RATE_MEM_DATA_SIZE  :TN  :=minimum(64,LDEMUX_DATA_OUT); --  
constant   HIST_TIME_SIZE  :TN  :=40; --  
constant   BX_HIST_DATA_NUMBERS  :TN  :=64; --  
constant   BX_HIST_DATA_SIZE  :TN  :=TVLcreate(BX_HIST_DATA_NUMBERS-1); --  
constant   BX_HIST_MEM_COUNT_SIZE  :TN  :=32; --  
constant   BX_HIST_MEM_ADDR_SIZE  :TVL  :=BX_HIST_DATA_SIZE+SLVPartAddrExpand(BX_HIST_MEM_COUNT_SIZE,II_DATA_SIZE); --  
constant   SPS_START_DELAY_SIZE  :TN  :=16; --  
constant   SPS_REVOL_COUNT_SIZE  :TN  :=11; --  
constant   SPS_ACTIVE_COUNT_SIZE  :TN  :=18; --  
constant   TRG_COND_SEL_NON  :TN  :=VEC_INDEX_MIN; --  
constant   TRG_COND_SEL_PRETRG0  :TN  :=TRG_COND_SEL_NON+1; --  
constant   TRG_COND_SEL_PRETRG1  :TN  :=TRG_COND_SEL_PRETRG0+1; --  
constant   TRG_COND_SEL_NUM  :TN  :=TRG_COND_SEL_PRETRG1+1; --  
constant   TRG_COND_SEL_SIZE  :TN  :=TVLcreate(TRG_COND_SEL_NUM-1); --  
constant   SPS_COND_SEL_NON  :TN  :=VEC_INDEX_MIN; --  
constant   SPS_COND_SEL_BCN0  :TN  :=SPS_COND_SEL_NON+1; --  
constant   SPS_COND_SEL_PRETRG0  :TN  :=SPS_COND_SEL_BCN0+1; --  
constant   SPS_COND_SEL_PRETRG1  :TN  :=SPS_COND_SEL_PRETRG0+1; --  
constant   SPS_COND_SEL_NUM  :TN  :=SPS_COND_SEL_PRETRG1+1; --  
constant   SPS_COND_SEL_SIZE  :TN  :=TVLcreate(SPS_COND_SEL_NUM-1); --  
constant   LD_IDENTIFIER  :TS  :="LB"; --  
constant   LD_VERSION  :THV  :="0200"; --  
constant   PAGE_REGISTERS  :TN  := 30; --    
constant   PAGE_MEM_RATE  :TN  := 31; --    
constant   PAGE_MEM_BX_HIST0  :TN  := 32; --    
constant   PAGE_MEM_BX_HIST1  :TN  := 33; --    
constant   PAGE_MEM_BX_HIST2  :TN  := 34; --    
constant   WORD_IDENTIFIER  :TN  := 36; --    
constant   WORD_VERSION  :TN  := 37; --    
constant   VECT_STATUS  :TN  := 39; --    
constant   BITS_STATUS_RATE_START  :TN  := 40; --    
constant   BITS_STATUS_RATE_STOP  :TN  := 41; --    
constant   BITS_STATUS_RATE_ENA  :TN  := 42; --    
constant   BITS_STATUS_PROC_REQ  :TN  := 43; --    
constant   BITS_STATUS_PROC_ACK  :TN  := 44; --    
constant   BITS_STATUS_CHAN_SEL  :TN  := 45; --    
constant   VECT_SELECT  :TN  := 47; --    
constant   BITS_SELECT_HIST0_CHAN0  :TN  := 48; --    
constant   BITS_SELECT_HIST0_CHAN1  :TN  := 49; --    
constant   BITS_SELECT_HIST0_CHAN2  :TN  := 50; --    
constant   BITS_SELECT_HIST0_TRG  :TN  := 51; --    
constant   BITS_SELECT_HIST1_CHAN0  :TN  := 52; --    
constant   BITS_SELECT_HIST1_CHAN1  :TN  := 53; --    
constant   BITS_SELECT_HIST1_CHAN2  :TN  := 54; --    
constant   BITS_SELECT_HIST1_TRG  :TN  := 55; --    
constant   BITS_SELECT_HIST2_CHAN0  :TN  := 56; --    
constant   BITS_SELECT_HIST2_CHAN1  :TN  := 57; --    
constant   BITS_SELECT_HIST2_CHAN2  :TN  := 58; --    
constant   BITS_SELECT_HIST2_TRG  :TN  := 59; --    
constant   VECT_CONDITION  :TN  := 61; --    
constant   BITS_COND_TRG_SEL  :TN  := 62; --    
constant   BITS_COND_SPS_SEL  :TN  := 63; --    
constant   VECT_SYN0  :TN  := 65; --    
constant   BITS_SYN0_CLK_QUALITY  :TN  := 66; --    
constant   BITS_SYN0_PHASE_QUALITY  :TN  := 67; --    
constant   BITS_SYN0_DATA_PARITY  :TN  := 68; --    
constant   BITS_SYN0_BCN0_SYNCH  :TN  := 69; --    
constant   BITS_SYN0_BCN_SYNCH  :TN  := 70; --    
constant   BITS_SYN0_LATENCY  :TN  := 71; --    
constant   VECT_SYN1  :TN  := 73; --    
constant   BITS_SYN1_CLK_QUALITY  :TN  := 74; --    
constant   BITS_SYN1_PHASE_QUALITY  :TN  := 75; --    
constant   BITS_SYN1_DATA_PARITY  :TN  := 76; --    
constant   BITS_SYN1_BCN0_SYNCH  :TN  := 77; --    
constant   BITS_SYN1_BCN_SYNCH  :TN  := 78; --    
constant   BITS_SYN1_LATENCY  :TN  := 79; --    
constant   WORD_HIST_TIME_LIMIT  :TN  := 81; --    
constant   WORD_HIST_TIME_COUNT  :TN  := 82; --    
constant   WORD_SPS_START_DELAY  :TN  := 83; --    
constant   WORD_SPS_REVOL_COUNT  :TN  := 84; --    
constant   WORD_SPS_ACTIVE_COUNT  :TN  := 85; --    
constant   WORD_HIST0_CHAN_ENA  :TN  := 87; --    
constant   WORD_HIST1_CHAN_ENA  :TN  := 88; --    
constant   WORD_HIST2_CHAN_ENA  :TN  := 89; --    
constant   AREA_MEM_BX_HIST0  :TN  := 91; --    
constant   AREA_MEM_BX_HIST1  :TN  := 92; --    
constant   AREA_MEM_BX_HIST2  :TN  := 93; --    
--  " item type                  item ID          width                  num       parent ID     write type     read type               name                        type     description"  
--
constant VIIItemDeclList	:TVIIItemDeclList :=(  
(    VII_PAGE  ,              PAGE_REGISTERS     ,                        0  ,                      0    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv(" ")   ,                    VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,            WORD_IDENTIFIER     ,             II_DATA_SIZE  ,                      1    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("IDENTIFIER")   ,           VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,               WORD_VERSION     ,             II_DATA_SIZE  ,                      1    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("VERSION")   ,              VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_VECT  ,                VECT_STATUS     ,                        0  ,                      0    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv("STATUS")   ,               VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,    BITS_STATUS_RATE_START     ,                        1  ,                      1    ,          VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("STATUS_RATE_START")   ,    VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_STATUS_RATE_STOP     ,                        1  ,                      1    ,          VECT_STATUS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("STATUS_RATE_STOP")   ,     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,      BITS_STATUS_RATE_ENA     ,                        1  ,                      1    ,          VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("STATUS_RATE_ENA")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,      BITS_STATUS_PROC_REQ     ,                        1  ,                      1    ,          VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("STATUS_PROC_REQ")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,      BITS_STATUS_PROC_ACK     ,                        1  ,                      1    ,          VECT_STATUS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("STATUS_PROC_ACK")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,      BITS_STATUS_CHAN_SEL     ,             TVLcreate(1)  ,                      1    ,          VECT_STATUS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("STATUS_CHAN_SEL")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_VECT  ,                VECT_SELECT     ,                        0  ,                      0    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv("SELECT")   ,               VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_SELECT_HIST0_CHAN0     ,                        1  ,                      1    ,          VECT_SELECT         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("SELECT_HIST0_CHAN0")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_SELECT_HIST0_CHAN1     ,                        1  ,                      1    ,          VECT_SELECT         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("SELECT_HIST0_CHAN1")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_SELECT_HIST0_CHAN2     ,                        1  ,                      1    ,          VECT_SELECT         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("SELECT_HIST0_CHAN2")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_SELECT_HIST0_TRG     ,                        1  ,                      1    ,          VECT_SELECT         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("SELECT_HIST0_TRG")   ,     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_SELECT_HIST1_CHAN0     ,                        1  ,                      1    ,          VECT_SELECT         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("SELECT_HIST1_CHAN0")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_SELECT_HIST1_CHAN1     ,                        1  ,                      1    ,          VECT_SELECT         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("SELECT_HIST1_CHAN1")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_SELECT_HIST1_CHAN2     ,                        1  ,                      1    ,          VECT_SELECT         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("SELECT_HIST1_CHAN2")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_SELECT_HIST1_TRG     ,                        1  ,                      1    ,          VECT_SELECT         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("SELECT_HIST1_TRG")   ,     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_SELECT_HIST2_CHAN0     ,                        1  ,                      1    ,          VECT_SELECT         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("SELECT_HIST2_CHAN0")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_SELECT_HIST2_CHAN1     ,                        1  ,                      1    ,          VECT_SELECT         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("SELECT_HIST2_CHAN1")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_SELECT_HIST2_CHAN2     ,                        1  ,                      1    ,          VECT_SELECT         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("SELECT_HIST2_CHAN2")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_SELECT_HIST2_TRG     ,                        1  ,                      1    ,          VECT_SELECT         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("SELECT_HIST2_TRG")   ,     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_VECT  ,             VECT_CONDITION     ,                        0  ,                      0    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv("SELECT")   ,               VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,         BITS_COND_TRG_SEL     ,        TRG_COND_SEL_SIZE  ,                      1    ,       VECT_CONDITION         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("COND_TRG_SEL")   ,         VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,         BITS_COND_SPS_SEL     ,        SPS_COND_SEL_SIZE  ,                      1    ,       VECT_CONDITION         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("COND_SPS_SEL")   ,         VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_VECT  ,                  VECT_SYN0     ,                        0  ,                      0    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv("SYN0")   ,                 VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_SYN0_CLK_QUALITY     ,                        1  ,                      1    ,            VECT_SYN0         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("SYN0_CLK_QUALITY")   ,     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_SYN0_PHASE_QUALITY     ,                        1  ,                      1    ,            VECT_SYN0         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("SYN0_PHASE_QUALITY")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_SYN0_DATA_PARITY     ,                        1  ,                      1    ,            VECT_SYN0         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("SYN0_DATA_PARITY")   ,     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,      BITS_SYN0_BCN0_SYNCH     ,                        1  ,                      1    ,            VECT_SYN0         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("SYN0_BCN0_SYNCH")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,       BITS_SYN0_BCN_SYNCH     ,                        1  ,                      1    ,            VECT_SYN0         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("SYN0_BCN_SYNCH")   ,       VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,         BITS_SYN0_LATENCY     ,            SYN_PIPE_SIZE  ,                      1    ,            VECT_SYN0         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("SYN0_LATENCY")   ,         VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_VECT  ,                  VECT_SYN1     ,                        0  ,                      0    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv("SYN1")   ,                 VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_SYN1_CLK_QUALITY     ,                        1  ,                      1    ,            VECT_SYN1         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("SYN1_CLK_QUALITY")   ,     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,   BITS_SYN1_PHASE_QUALITY     ,                        1  ,                      1    ,            VECT_SYN1         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("SYN1_PHASE_QUALITY")   ,   VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,     BITS_SYN1_DATA_PARITY     ,                        1  ,                      1    ,            VECT_SYN1         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("SYN1_DATA_PARITY")   ,     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,      BITS_SYN1_BCN0_SYNCH     ,                        1  ,                      1    ,            VECT_SYN1         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("SYN1_BCN0_SYNCH")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,       BITS_SYN1_BCN_SYNCH     ,                        1  ,                      1    ,            VECT_SYN1         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("SYN1_BCN_SYNCH ")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(      VII_BITS  ,         BITS_SYN1_LATENCY     ,            SYN_PIPE_SIZE  ,                      1    ,            VECT_SYN1         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("SYN1_LATENCY")   ,         VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,       WORD_HIST_TIME_LIMIT     ,           HIST_TIME_SIZE  ,                      1    ,       PAGE_REGISTERS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("HIST_TIME_LIMIT")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,       WORD_HIST_TIME_COUNT     ,           HIST_TIME_SIZE  ,                      1    ,       PAGE_REGISTERS         ,   VII_WNOACCESS  ,   VII_REXTERNAL   ,   VIINameConv("HIST_TIME_COUNT")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,       WORD_SPS_START_DELAY     ,     SPS_START_DELAY_SIZE  ,                      1    ,       PAGE_REGISTERS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("SPS_START_DELAY")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,       WORD_SPS_REVOL_COUNT     ,     SPS_REVOL_COUNT_SIZE  ,                      1    ,       PAGE_REGISTERS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("SPS_REVOL_COUNT")   ,      VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,      WORD_SPS_ACTIVE_COUNT     ,    SPS_ACTIVE_COUNT_SIZE  ,                      1    ,       PAGE_REGISTERS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("SPS_ACTIVE_COUNT")   ,     VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,        WORD_HIST0_CHAN_ENA     ,       RATE_MEM_DATA_SIZE  ,                      1    ,       PAGE_REGISTERS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("HIST0_CHAN_ENA")   ,       VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,        WORD_HIST1_CHAN_ENA     ,       RATE_MEM_DATA_SIZE  ,                      1    ,       PAGE_REGISTERS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("HIST1_CHAN_ENA")   ,       VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_WORD  ,        WORD_HIST2_CHAN_ENA     ,       RATE_MEM_DATA_SIZE  ,                      1    ,       PAGE_REGISTERS         ,     VII_WACCESS  ,   VII_RINTERNAL   ,   VIINameConv("HIST2_CHAN_ENA")   ,       VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(    VII_PAGE  ,           PAGE_MEM_BX_HIST0     ,                        0  ,                      0    ,    PAGE_MEM_BX_HIST0         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv(" ")   ,                    VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_AREA  ,          AREA_MEM_BX_HIST0     ,   BX_HIST_MEM_COUNT_SIZE  ,   BX_HIST_DATA_NUMBERS    ,    PAGE_MEM_BX_HIST0         ,     VII_WACCESS  ,   VII_REXTERNAL   ,   VIINameConv("MEM_BX_HIST0")   ,         VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(    VII_PAGE  ,           PAGE_MEM_BX_HIST1     ,                        0  ,                      0    ,    PAGE_MEM_BX_HIST1         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv(" ")   ,                    VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_AREA  ,          AREA_MEM_BX_HIST1     ,   BX_HIST_MEM_COUNT_SIZE  ,   BX_HIST_DATA_NUMBERS    ,    PAGE_MEM_BX_HIST1         ,     VII_WACCESS  ,   VII_REXTERNAL   ,   VIINameConv("MEM_BX_HIST1")   ,         VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(    VII_PAGE  ,           PAGE_MEM_BX_HIST2     ,                        0  ,                      0    ,    PAGE_MEM_BX_HIST2         ,   VII_WNOACCESS  ,   VII_RNOACCESS   ,   VIINameConv(" ")   ,                    VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ), 
(     VII_AREA  ,          AREA_MEM_BX_HIST2     ,   BX_HIST_MEM_COUNT_SIZE  ,   BX_HIST_DATA_NUMBERS    ,    PAGE_MEM_BX_HIST2         ,     VII_WACCESS  ,   VII_REXTERNAL   ,   VIINameConv("MEM_BX_HIST2")   ,         VII_FUN_UNDEF    ,   VIIDescrConv(" ")    ) 
);
end lb_bxhist_iis;
