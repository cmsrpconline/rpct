library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.std_logic_1164_ktp.all;

package LPMDiagnostics is

  component DPM_PROG_HIST is
    generic (
      constant LPM_DATA_WIDTH		:natural := 0; -- szer szyny adresowej
      constant LPM_COUNT_WIDTH		:natural := 0; -- szer szyny danych
      constant LPM_MDATA_WIDTH		:natural := 0  -- szer szyny danych
    );
    port(
      resetN				:in  TSL := '0';
      clock				:in  TSL := '1';
      data_in				:in  TSLV(LPM_DATA_WIDTH-1 DOWNTO 0);  --dane wchodzace do ukladu i podlegajace histogramowaniu
      clk_ena				:in  TSL := '1';
      sim_loop				:in  TSL := '0';
      proc_req				:in  TSL := '0';
      proc_ack				:out TSL;
      memory_address_in			:in  TSLV(LPM_DATA_WIDTH+SLVPartAddrExpand(LPM_COUNT_WIDTH,LPM_MDATA_WIDTH)-1 DOWNTO 0);
      memory_data_in			:in  TSLV (LPM_MDATA_WIDTH-1 DOWNTO 0);
      memory_data_out			:out TSLV (LPM_MDATA_WIDTH-1 DOWNTO 0);
      memory_wr				:in  TSL
  );
  end component;


  component DPM_PROG_HIST1 is
    generic (
      constant LPM_DATA_WIDTH		:natural := 0; -- szer szyny adresowej
      constant LPM_COUNT_WIDTH		:natural := 0; -- szer szyny danych
      constant LPM_MDATA_WIDTH		:natural := 0  -- szer szyny danych
    );
    port(
      resetN				:in  TSL := '0';
      clock				:in  TSL := '1';
      data_in				:in  TSLV(LPM_DATA_WIDTH-1 DOWNTO 0);  --dane wchodzace do ukladu i podlegajace histogramowaniu
      clk_ena				:in  TSL := '1';
      sim_loop				:in  TSL := '0';
      proc_req				:in  TSL := '0';
      proc_ack				:out TSL;
      memory_address_in			:in  TSLV(LPM_DATA_WIDTH+SLVPartAddrExpand(LPM_COUNT_WIDTH,LPM_MDATA_WIDTH)-1 DOWNTO 0);
      memory_data_in			:in  TSLV (LPM_MDATA_WIDTH-1 DOWNTO 0);
      memory_data_out			:out TSLV (LPM_MDATA_WIDTH-1 DOWNTO 0);
      memory_wr				:in  TSL
  );
  end component;


  component RATE is
    generic(
      constant LPM_DATA_WIDTH			:integer:=8;
      constant LPM_COUNT_WIDTH			:integer:=8;
      constant LPM_MDATA_WIDTH			:natural := 8  -- szer szyny danych
    );
    port (
      resetN				:in  TSL := '0';
      clock				:in  TSL := '1';
      data_in				:in  TSLV(LPM_DATA_WIDTH-1 DOWNTO 0);  --dane wchodzace do ukladu i podlegajace histogramowaniu
      clk_ena				:in  TSL := '1';
      sim_loop				:in  TSL := '0';
      proc_req				:in  TSL := '0';
      proc_ack				:out TSL;
      memory_address_in			:in  TSLV(TVLcreate(LPM_DATA_WIDTH-1)+SLVPartAddrExpand(LPM_COUNT_WIDTH,LPM_MDATA_WIDTH)-1 DOWNTO 0);
      memory_data_in			:in  TSLV (LPM_MDATA_WIDTH-1 DOWNTO 0);
      memory_data_out			:out TSLV (LPM_MDATA_WIDTH-1 DOWNTO 0);
      memory_wr				:in  TSL     
    );
  end component; 


end LPMDiagnostics;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.std_logic_1164_ktp.all;
-----------------------------------------------------------
entity DPM_PROG_HIST is
  generic (
    constant LPM_DATA_WIDTH		:natural := 8; -- szer szyny adresowej
    constant LPM_COUNT_WIDTH		:natural :=16; -- szer szyny danych
    constant LPM_MDATA_WIDTH		:natural :=16 -- szer szyny danych
  );
  port(
    resetN				:in  TSL := '0';
    clock				:in  TSL;          --zegar
    data_in				:in  TSLV(LPM_DATA_WIDTH-1 DOWNTO 0);  --dane wchodzace do ukladu i podlegajace histogramowaniu
    clk_ena				:in  TSL;
    sim_loop				:in  TSL := '0';
    proc_req				:in  TSL;
    proc_ack				:out TSL;
    memory_address_in			:in  TSLV(LPM_DATA_WIDTH+SLVPartAddrExpand(LPM_COUNT_WIDTH,LPM_MDATA_WIDTH)-1 DOWNTO 0);
    memory_data_in			:in  TSLV (LPM_MDATA_WIDTH-1 DOWNTO 0);
    memory_data_out			:out TSLV (LPM_MDATA_WIDTH-1 DOWNTO 0);
    memory_wr				:in  TSL
);
end DPM_PROG_HIST;
-----------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.std_logic_1164_ktp.all;
use work.VComponent.all;
use work.LPMComponent.all;
 
architecture behaviour of DPM_PROG_HIST is
-----------------------------------------------------------
  signal nclock				:TSL;
  signal L,H				:TSL;
  signal ack				:TSL;
  signal data_input			:TSLV(LPM_COUNT_WIDTH-1 DOWNTO 0);
  signal data_address			:TSLV(LPM_DATA_WIDTH-1 DOWNTO 0);
  signal data_output			:TSLV(LPM_COUNT_WIDTH-1 DOWNTO 0);
  signal data_process			:TSLV(LPM_COUNT_WIDTH-1 DOWNTO 0);
  signal mem_proc_req			:TSL;
  signal wr_en				:TSL;


begin
  ----------------------------------------------------------------------------- 
  --zapis na zboczu narastajacym (clock) odczyt na opadajacym (nclock)
  nclock <= not clock;

  L <='0'; H <= '1';

  ack<=proc_req;-- or wr_en;
  data_input<= data_process + 1;--TNconv(wr_en);
  -----------------------------------------------------------------------------
  
  
--pobieranie danych
  process(clock,resetN)
  begin
    if (resetN = '0') then
      data_address <= (others => '0');
wr_en    <= '0';
         elsif (clock='1' and clock'event) then  
      data_address <=data_in;
wr_en    <=  clk_ena;
         end if;
  end process;





----------------------------------------------------------------------------- 
  process(clock,resetN)
  begin 
    if (resetN = '0') then
      data_process <= (others => '0');
    elsif clock='0' and clock'event then

	      data_process <= data_output;
	    end if;
  end process;
  ----------------------------------------------------------------------------- 
  mem_proc_req <= proc_req or ack;
  -- pamiec
  Pam_dpm: DPM_PROG 
    generic map(
      LPM_DATA_WIDTH  => LPM_COUNT_WIDTH,
      LPM_ADDR_WIDTH  => LPM_DATA_WIDTH,
      LPM_DELAY_LEVEL => 2,
      LPM_MDATA_WIDTH => LPM_MDATA_WIDTH
    )
    port map(
      addr_in         => data_address,
      data_in         => data_input,
      wr              => nclock,
      wr_ena          => wr_en,
      addr_out        => data_address,
      data_out        => data_output,
      rd              => H,
      rd_ena          => H,
      simulate        => sim_loop,
      proc_req        => mem_proc_req,
      proc_ack        => proc_ack ,
      memory_addr     => memory_address_in,
      memory_data_in  => memory_data_in,
      memory_data_out => memory_data_out,
      memory_wr       => memory_wr
    );

end behaviour;







--------------------------------------------------
-- liczy cykle zegara
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_1164.all;	
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_misc.all;
use work.std_logic_1164_ktp.all;
use work.VComponent.all;
use work.LPMComponent.all;


entity SYN_CNT is
  generic(
    constant A				: integer :=8
  );
  port (
    clock				:in  std_logic; -- wspolny zegar
    clock_ena				:in  std_logic; -- clock enable - tutaj sygnal wejsciowy
    rstN				:in  std_logic; --  nie wspolny reset
    data_count				:out std_logic_vector(A-1 downto 0) -- wyjscie danych
  );
end SYN_CNT;

architecture SYN_CNT_arch of SYN_CNT is
 signal count_reg			:std_logic_vector(A-1 downto 0);
begin

  process(clock, rstN,clock_ena)
  begin
    if clock'event and clock='1' then
      if rstN='0' then
 	count_reg<=TSLVResize(TSLVconv(clock_ena), A); 
      elsif clock_ena='1' then       
        count_reg <= count_reg+1;
      end if;
    end if;
  end process;
  
  data_count	<= count_reg;
end SYN_CNT_arch; 
-------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_1164.all;
use work.std_logic_1164_ktp.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_misc.all;
use work.VComponent.all;
use work.LPMComponent.all;
--use work.ustawienia_p.all;

entity SYN_CNT_ARRAY is
  generic(
    constant B				:integer:=8;
    constant C				:integer:=8
  );
  port (
    clock				:in  std_logic; -- wspolny zegar
    clock_ena				:in  std_logic_vector(C-1 downto 0); -- clock enable - tutaj sygnal wejsciowy
    rstN				:in  std_logic; --NIE wspolny reset
    data_count_array			:out std_logic_vector((C*B)-1 downto 0) -- wyjscie danych
  );
end SYN_CNT_ARRAY;

architecture SYN_CNT_ARRAY_arch of SYN_CNT_ARRAY is

  component SYN_CNT is
    generic(
      constant A: integer--:=8
    );
    port (
      clock				:in  std_logic; -- wspolny zegar
      clock_ena				:in  std_logic; -- clock enable - tutaj sygnal wejsciowy
      rstN				:in  std_logic; -- wspolny reset
      data_count				:out std_logic_vector(A-1 downto 0) -- wyjscie danych
    );
  end component;

begin

  LOOP1: for I in 0 to C-1 generate
    LICZNIK: SYN_CNT
      generic map(B)
      port map (
        clock			=>	clock,
        clock_ena		=>	clock_ena(I),
        rstN			=>	rstN,
        data_count		=>	data_count_array(((I+1)*B)-1 downto I*B)
      );
  end generate;
end architecture;
-------------------------------------------------
  
  
  
  
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_misc.all;
use IEEE.std_logic_arith.all;
use work.std_logic_1164_ktp.all;
use work.VComponent.all;
use work.LPMComponent.all;


entity RATE_TN is
  generic(
    constant LPM_DATA_WIDTH			:integer:=8;
    constant LPM_COUNT_WIDTH			:integer:=8;
    constant LPM_MDATA_WIDTH			:natural := 8  -- szer szyny danych
  );
  port (
    resetN				:in  TSL := '0';
    clock				:in  TSL := '1';
    data_in				:in  TSLV(LPM_DATA_WIDTH-1 DOWNTO 0);  --dane wchodzace do ukladu i podlegajace histogramowaniu
    clk_ena				:in  TSL := '1';
    sim_loop				:in  TSL := '0';
    proc_req				:in  TSL := '0';
    proc_ack				:out TSL;
    memory_address_in			:in  TSLV(TVLcreate(LPM_DATA_WIDTH-1)+SLVPartAddrExpand(LPM_COUNT_WIDTH,LPM_MDATA_WIDTH)-1 DOWNTO 0);
    memory_data_in			:in  TSLV (LPM_MDATA_WIDTH-1 DOWNTO 0);
    memory_data_out			:out TSLV (LPM_MDATA_WIDTH-1 DOWNTO 0);
    memory_wr				:in  TSL     
  );
end RATE_TN; 

architecture RATE_arch of RATE_TN is
  component SYN_CNT_ARRAY is 
    generic(
      constant B: integer;
      constant C: integer
    );
    port (
      clock				:in  std_logic; -- wspolny zegar
      clock_ena				:in  std_logic_vector(C-1 downto 0); -- clock enable - tutaj sygnal wejsciowy
      rstN				:in  std_logic; --NIE wspolny reset
      data_count_array			:out std_logic_vector((B*C)-1 downto 0) -- wyjscie danych
    );
  end component;

  constant COUNT_WIDTH		:integer:=TVLcreate(LPM_DATA_WIDTH);
  constant MEM_ADDR_WIDTH	:integer:=TVLcreate(LPM_DATA_WIDTH-1);
  
  signal int_data_out			:std_logic_vector((COUNT_WIDTH*LPM_DATA_WIDTH)-1 downto 0);
  signal int_data			:std_logic_vector((COUNT_WIDTH*LPM_DATA_WIDTH)-1 downto 0);
  signal counter			:integer range LPM_DATA_WIDTH-1 downto 0;
  signal data_cnt			:std_logic_vector(LPM_DATA_WIDTH-1 downto 0); -- wyjscie danych
  signal data_tmp			:std_logic_vector(COUNT_WIDTH-1 downto 0); -- wyjscie danych
  signal data_output			:std_logic_vector((LPM_COUNT_WIDTH)-1 downto 0); -- wyjscie danych
  signal data_input			:std_logic_vector((LPM_COUNT_WIDTH)-1 downto 0); -- wyjscie danych
  signal local_resetN			:std_logic;
  signal wr_enable			:std_logic;
  signal rd_enable			:std_logic;
  signal rd_wr				:std_logic;
  signal write_addr			:std_logic_vector((MEM_ADDR_WIDTH)-1 downto 0);  
  signal wr_addr			:std_logic_vector((MEM_ADDR_WIDTH)-1 downto 0);
  signal read_addr			:std_logic_vector((MEM_ADDR_WIDTH)-1 downto 0);
  signal rd_address			:std_logic_vector((MEM_ADDR_WIDTH)-1 downto 0);
  signal write_data			:std_logic_vector((LPM_COUNT_WIDTH)-1 downto 0);
  signal read_data			:std_logic_vector((LPM_COUNT_WIDTH)-1 downto 0);
  signal wr_data			:std_logic_vector((LPM_COUNT_WIDTH)-1 downto 0);
  signal proc_ackn			:std_logic; --process acknowledge

begin

--odczytywanie danych z zewnatrz
  process(clock,proc_ackn,read_data,read_addr,write_data,data_input)
  begin
    if proc_ackn='1' then 
      data_output(LPM_COUNT_WIDTH-1 downto 0)<=read_data;
      wr_addr <= write_addr;
      rd_address<=read_addr;
      wr_data<=write_data;	
      rd_wr<=not clock;
    else
      rd_wr<='0';
   end if;
  end process;

  wr_enable<='1';
  rd_enable<='1';

--licznik do zbierania przyrostow z licznikow
  process(clock,resetN,read_data,clk_ena,data_in)
  begin
    if clk_ena='0' then 
      data_cnt<=(others=>'0');
    else
      data_cnt<=data_in;
    end if;
  end process;
----proces sterujacy sygnalami request i acknowledge
  process(clock,resetN,proc_req)
  begin
    if clock = '1' and clock'event then 
      if proc_req='1'  then 
        proc_ackn<='1';
      else
        if local_resetN='0' then
          proc_ackn<='0';
        end if;
      end if;
    end if;
  end process;
------------------------------------------------
  process(clock,resetN,proc_req,proc_ackn,counter)
  begin
    if resetN='0' or proc_ackn='0' then 
      counter<=0;
    elsif clock = '1' and clock'event then 
      if proc_ackn='1' then
      write_addr<=read_addr;
      write_data<=read_data+data_tmp; 
      read_addr<=CONV_STD_LOGIC_VECTOR(counter,MEM_ADDR_WIDTH);
      
         if counter = LPM_DATA_WIDTH-1 then 
	   counter <=0;
	   local_resetN<='0';
         else
            counter<=counter+1;
	    local_resetN<='1';
	 end if;
      end if;
    end if;
  end process;


------------------------------------------------
  process(clock,resetN,counter,data_tmp,read_data,int_data_out)
  begin
    if resetN='0' then 
      int_data<=(others=>'0');
    elsif clock='1' and clock'event then
      if counter=0 then
        int_data<=int_data_out;
      else
        int_data(LPM_DATA_WIDTH*COUNT_WIDTH-1 downto COUNT_WIDTH)<=int_data((LPM_DATA_WIDTH-1)*COUNT_WIDTH-1 downto 0);	
      end if;      
    end if;
  end process;
  
  data_tmp<=int_data(LPM_DATA_WIDTH*COUNT_WIDTH-1 downto (LPM_DATA_WIDTH-1)*COUNT_WIDTH);
------------------------------------------------
  lsa1: SYN_CNT_ARRAY 
    generic map(COUNT_WIDTH ,LPM_DATA_WIDTH )
    port map(
      clock => clock,
      clock_ena => data_cnt,
      rstN =>local_resetN,
      data_count_array =>int_data_out
     );
  Pam_dpm: DPM_PROG 
    generic map(
      LPM_DATA_WIDTH  => LPM_COUNT_WIDTH,
      LPM_ADDR_WIDTH  => MEM_ADDR_WIDTH,
      LPM_DELAY_LEVEL => 2,
      LPM_MDATA_WIDTH => LPM_MDATA_WIDTH
    )
    port map(
      addr_in         => wr_addr,
      data_in         => wr_data,
      wr              => rd_wr,
      wr_ena          => wr_enable,
      addr_out        => rd_address,
      data_out        => read_data,
      rd              => rd_wr,
      rd_ena          => rd_enable,
      simulate        => sim_loop,
      proc_req        => proc_ackn,
      proc_ack        => proc_ack ,
      memory_addr     => memory_address_in,
      memory_data_in  => memory_data_in,
      memory_data_out => memory_data_out,
      memory_wr       => memory_wr
    );
end architecture;


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.std_logic_1164_ktp.all;

entity DPM_PROG_HIST1 is
  generic (
    constant LPM_DATA_WIDTH		:natural := 4;
    constant LPM_COUNT_WIDTH		:natural := 32;
    constant LPM_MDATA_WIDTH		:natural := 16
  );
  port(
    resetN				:in  TSL := '0';
    clock				:in  TSL;
    data_in				:in  TSLV(LPM_DATA_WIDTH-1 DOWNTO 0);
    clk_ena				:in  TSL;
    sim_loop				:in  TSL := '0';
    proc_req				:in  TSL;
    proc_ack				:out TSL;
    memory_address_in			:in  TSLV(LPM_DATA_WIDTH+SLVPartAddrExpand(LPM_COUNT_WIDTH,LPM_MDATA_WIDTH)-1 DOWNTO 0);
    memory_data_in			:in  TSLV (LPM_MDATA_WIDTH-1 DOWNTO 0);
    memory_data_out			:out TSLV (LPM_MDATA_WIDTH-1 DOWNTO 0);
    memory_wr				:in  TSL
);
end DPM_PROG_HIST1;

library ieee;
use ieee.std_logic_1164.all;
use work.std_logic_1164_ktp.all;
use work.VComponent.all;
use work.LPMComponent.all;
 
architecture behaviour of DPM_PROG_HIST1 is

  signal L,H				:TSL;
  signal clockN				:TSL;
  signal ProcReqReg1			:TSL;
  signal ProcReqReg2			:TSL;
  signal ClkEnaReg1			:TSL;
  signal ClkEnaReg2			:TSL;
  signal MemDataSig			:TSLV(LPM_COUNT_WIDTH-1 DOWNTO 0);
  signal MemDataReg			:TSLV(LPM_COUNT_WIDTH-1 DOWNTO 0);
  signal HistDataSig			:TSLV(LPM_COUNT_WIDTH-1 DOWNTO 0);
  signal MemAddrRdReg			:TSLV(LPM_DATA_WIDTH-1 DOWNTO 0);
  signal MemAddrWrReg			:TSLV(LPM_DATA_WIDTH-1 DOWNTO 0);
  signal DiffAddrSig			:TSL;
  signal WorkStartSig			:TSL;
  signal WorkNormSig			:TSL;
  signal WorkStopSig			:TSL;
  signal MemProcReqSig			:TSL;


begin

  L <='0'; H <= '1';
  clockN       <= not clock;

  WorkStartSig <=      ProcReqReg1  and (not ProcReqReg2);
  WorkNormSig  <=      ProcReqReg1  and      ProcReqReg2;
  WorkStopSig  <= (not ProcReqReg1) and      ProcReqReg2;

  DiffAddrSig  <= '1' when MemAddrRdReg/=MemAddrWrReg else '0';

  process(clock,resetN)
  begin

    if (resetN = '0') then

      ProcReqReg1  <= '0';
      ProcReqReg2  <= '0';
      ClkEnaReg1   <= '0';
      ClkEnaReg2   <= '0';
      MemAddrRdReg <= (others =>'0');
      MemAddrWrReg <= (others =>'0');
      MemDataReg   <= (others =>'0');

    elsif (clock'event and clock='1') then

      ProcReqReg1  <= proc_req;
      ProcReqReg2  <= ProcReqReg1;

      ClkEnaReg1   <= clk_ena;
      ClkEnaReg2   <= ClkEnaReg1;

      MemAddrRdReg <= data_in;
      MemAddrWrReg <= MemAddrRdReg;

      if ((WorkStartSig or (WorkNormSig and DiffAddrSig))='1') then
        MemDataReg <= MemDataSig;
      else
        MemDataReg <= HistDataSig;
      end if;

    end if;

  end process;

  HistDataSig <= MemDataReg+1 when (ClkEnaReg2='1' and (WorkNormSig='1' or WorkStopSig='1')) else
                 MemDataReg;

  MemProcReqSig	<= ProcReqReg1 or ProcReqReg2;
  mem: DPM_PROG 
    generic map(
      LPM_DATA_WIDTH  => LPM_COUNT_WIDTH,
      LPM_ADDR_WIDTH  => LPM_DATA_WIDTH,
      LPM_DELAY_LEVEL => 2,
      LPM_MDATA_WIDTH => LPM_MDATA_WIDTH
    )
    port map(
      addr_in         => MemAddrWrReg,
      data_in         => HistDataSig,
      wr              => clockN,
      wr_ena          => ProcReqReg2, --H
      addr_out        => MemAddrRdReg,
      data_out        => MemDataSig,
      rd              => H,
      rd_ena          => H,
      simulate        => sim_loop,
      proc_req        => MemProcReqSig,
      proc_ack        => proc_ack,
      memory_addr     => memory_address_in,
      memory_data_in  => memory_data_in,
      memory_data_out => memory_data_out,
      memory_wr       => memory_wr
    );
    
end behaviour;







library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_1164.all;	
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_misc.all;
use work.std_logic_1164_ktp.all;
use work.VComponent.all;
use work.LPMComponent.all;


entity RATE_SHORT_CNT is
  generic(
    constant LPM_DATA_WIDTH		: integer :=8
  );
  port (
    resetN				:in  TSL;
    clock				:in  TSL;
    data_in				:in  TSL;
    set					:in  TSL;
    data_out				:out TSLV(LPM_DATA_WIDTH-1 downto 0)
  );
end RATE_SHORT_CNT;

architecture behaviour of RATE_SHORT_CNT is
 signal CountReg			:TSLV(LPM_DATA_WIDTH-1 downto 0);
begin
  process(resetN, clock) is
  begin
    if (resetN='0') then
      CountReg <= (others =>'0'); 
    elsif clock'event and clock='1' then
      if set='1' then
        if (data_in='1') then
          CountReg <= TSLVconv(1,LPM_DATA_WIDTH);
	else
          CountReg <= (others =>'0');
	end if;
      elsif (data_in='1') then
        CountReg <= CountReg+1;
      end if;
    end if;
  end process;
  data_out <= CountReg;
end behaviour; 


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_misc.all;
use IEEE.std_logic_arith.all;
use work.std_logic_1164_ktp.all;
use work.VComponent.all;
use work.LPMComponent.all;

entity RATE is
  generic(
    constant LPM_DATA_WIDTH			:integer:=6;
    constant LPM_COUNT_WIDTH			:integer:=8;
    constant LPM_MDATA_WIDTH			:natural := 8  -- szer szyny danych
  );
  port (
    resetN				:in  TSL := '0';
    clock				:in  TSL := '1';
    data_in				:in  TSLV(LPM_DATA_WIDTH-1 DOWNTO 0);  --dane wchodzace do ukladu i podlegajace histogramowaniu
    clk_ena				:in  TSL := '1';
    sim_loop				:in  TSL := '0';
    proc_req				:in  TSL := '0';
    proc_ack				:out TSL;
    memory_address_in			:in  TSLV(TVLcreate(LPM_DATA_WIDTH-1)+SLVPartAddrExpand(LPM_COUNT_WIDTH,LPM_MDATA_WIDTH)-1 DOWNTO 0);
    memory_data_in			:in  TSLV (LPM_MDATA_WIDTH-1 DOWNTO 0);
    memory_data_out			:out TSLV (LPM_MDATA_WIDTH-1 DOWNTO 0);
    memory_wr				:in  TSL     
  );
end RATE; 

architecture behaviour of RATE is

  component RATE_SHORT_CNT
    generic (
      constant LPM_DATA_WIDTH		: integer :=0
    );
    port (
      resetN				:in  TSL;
      clock				:in  TSL;
      data_in				:in  TSL;
      set				:in  TSL;
      data_out				:out TSLV(LPM_DATA_WIDTH-1 downto 0)
    );
  end component;

  constant DATA_ADDR_SIZE		:TN  := TVLcreate(LPM_DATA_WIDTH-1);
  constant CNT_DATA_SIZE		:TN  := TVLcreate(LPM_DATA_WIDTH);
  constant MEM_ADDR_SIZE		:TN  := DATA_ADDR_SIZE+SLVPartAddrExpand(LPM_COUNT_WIDTH,LPM_MDATA_WIDTH);
  
  subtype  TShortCount			is TSLV(CNT_DATA_SIZE-1 downto 0);
  type	   TShortCountVec		is array (LPM_DATA_WIDTH-1 downto 0) of TShortCount;
  
  signal   L,H				:TSL;
  signal   clockN			:TSL;
  signal   DataInSig			:TSLV(LPM_DATA_WIDTH-1 downto 0);
  signal   ShortCountVec		:TShortCountVec;
  signal   ShortCountPipe		:TShortCountVec;
  signal   CountSel			:TSLV(DATA_ADDR_SIZE-1 downto 0);
  signal   MemAddrRdSig			:TSLV(DATA_ADDR_SIZE-1 downto 0);
  signal   MemAddrWrSig			:TSLV(DATA_ADDR_SIZE-1 downto 0);
  signal   SetCountSig			:TSL;
  signal   AddReg       		:TSLV(LPM_COUNT_WIDTH-1 downto 0);
  signal   CellDataOut       		:TSLV(LPM_COUNT_WIDTH-1 downto 0);
  signal   ProcReqReg1			:TSL;
  signal   ProcReqReg2			:TSL;
  signal   ProcReqReg3			:TSL;
  signal   DPMReqSig			:TSL;
  
begin

  L <='0'; H <= '1';
  clockN <= not(clock);
  
  DataInSig <= data_in when clk_ena='1' else
               (others => '0');
  SetCountSig <= TSLconv(CountSel=0);

  cnt_loop: for index in 0 to LPM_DATA_WIDTH-1 generate
    ShortCount: RATE_SHORT_CNT
      generic map(
        LPM_DATA_WIDTH => CNT_DATA_SIZE
      )
      port map (
        resetN         => resetN,
        clock          => clock,
        data_in        => DataInSig(index),
        set            => SetCountSig,
        data_out       => ShortCountVec(index)
      );
  end generate;

  process(clock,resetN)
  begin

    if (resetN = '0') then
      
      ShortCountPipe <= (ShortCountVec'range => (others => '0'));
      CountSel	     <= (others => '0');
      MemAddrRdSig   <= (others => '0');
      MemAddrWrSig   <= (others => '0');
      AddReg         <= (others => '0');
      ProcReqReg1    <= '0';
      ProcReqReg2    <= '0';
      ProcReqReg3    <= '0';

    elsif (clock'event and clock='1') then

      if (CountSel=LPM_DATA_WIDTH-1 or ProcReqReg1='0') then
        CountSel <= (others => '0');
      else
        CountSel <= CountSel+1;
      end if;
      MemAddrRdSig <= CountSel;
      MemAddrWrSig <= MemAddrRdSig;

      if (CountSel=0) then
        ShortCountPipe <= ShortCountVec;
      else
        ShortCountPipe(LPM_DATA_WIDTH-2 downto 0) <= ShortCountPipe(LPM_DATA_WIDTH-1 downto 1);
      end if;
      
      AddReg <= CellDataOut+ShortCountPipe(0);
      
      if (ProcReqReg1 ='0') then
        ProcReqReg1 <= proc_req;
      else
        if (CountSel=0) then
          ProcReqReg1 <= proc_req;
	end if;
      end if;	
      ProcReqReg2 <= ProcReqReg1;
      ProcReqReg3 <= ProcReqReg2;

    end if;

  end process;

  DPMReqSig <= ProcReqReg1 or ProcReqReg2 or ProcReqReg3;

  DPM_rate: DPM_PROG 
    generic map(
      LPM_DATA_WIDTH  => LPM_COUNT_WIDTH,
      LPM_ADDR_WIDTH  => DATA_ADDR_SIZE,
      LPM_DELAY_LEVEL => 2,
      LPM_MDATA_WIDTH => LPM_MDATA_WIDTH
    )
    port map(
      addr_in         => MemAddrWrSig,
      data_in         => AddReg,
      wr              => clockN,
      wr_ena          => ProcReqReg3,
      addr_out        => MemAddrRdSig,
      data_out        => CellDataOut,
      rd              => H,
      rd_ena          => H,
      simulate        => sim_loop,
      proc_req        => DPMReqSig,
      proc_ack        => proc_ack ,
      memory_addr     => memory_address_in,
      memory_data_in  => memory_data_in,
      memory_data_out => memory_data_out,
      memory_wr       => memory_wr
    ); 
    
end behaviour;
