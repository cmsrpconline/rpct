library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;
library lpm;
use lpm.lpm_components.all;
use work.std_logic_1164_ktp.all;
use work.vcomponent.all;
use work.LPMcomponent.all;
use work.i2c.all;
use work.tridaq.all;
use work.lb_control_iis.all;

entity lb_control is
  port(
    -- FEB test signal
    FEB_test					:out   TSLV(FEB_TEST_DATA-1 downto 0);
    -- GOL control signal
    GOL_laser					:out   TSL;
    GOL_nedge					:out   TSL;
    GOL_tx_ena					:out   TSL;
    GOL_tx_err					:out   TSL;
    GOL_ready					:in    TSL;
    -- i2c interface
    i2c_sda					:inout TSL;
    i2c_scl					:inout TSL;
    -- ttc interface
    ttc_clk					:in    TSL;
    ttc_clk_des1				:in    TSL;
    ttc_clk_des2				:in    TSL;
    ttc_l1a					:in    TSL;
    ttc_bcn0					:in    TSL;
    ttc_brcst1_data				:in    TSLV(TTC_BROADCAST1_DATA_SIZE-1 downto 0);
    ttc_brcst2_data				:in    TSLV(TTC_BROADCAST2_DATA_SIZE-1 downto 0);
    ttc_brcst_str1				:in    TSL;
    ttc_brcst_str2				:in    TSL;
    -- fast signal interface
    clock					:in    TSL;
    fs_clock					:out   TSL;
    fs_l1a					:out   TSL;
    fs_pretrg0					:out   TSL;
    fs_pretrg1					:out   TSL;
    fs_bcn0					:out   TSL;
    -- additional fast signals
    ext_clock					:in    TSL;
    ext_trigger					:in    TSL;
    int_clock					:in    TSL;
    -- internal bus interface
    II_addr					:in    TSLV(IILB_ADDR_SIZE-1 downto 0);
    II_data					:inout TSLV(II_DATA_SIZE-1 downto 0);
    II_oper					:in    TSL;
    II_write					:in    TSL;
    II_save					:in    TSL;
    II_reset					:in    TSL;
    II_ack					:inout TSL;
    -- diagnostics signal
    led						:out   TSLV(5 downto 0)
);
end lb_control;    

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;
library lpm;
use lpm.lpm_components.all;
use work.std_logic_1164_ktp.all;
use work.vcomponent.all;
use work.LPMcomponent.all;
use work.tridaq.all;

architecture behaviour of lb_control is

  signal L, H					:TSL;

  signal   I2C_AreaEnaSig			:TSL;
  signal   I2C_AreaWrSig			:TSLV(AREA_I2C_SEL_SIZE-1 downto 0);
  signal   I2C_DataOut				:TSLV(AREA_I2C_DATA_SIZE-1 downto 0);
  signal   I2C_SCLin				:TSL;
  signal   I2C_SCLout				:TSL;
  signal   I2C_SCLoutN				:TSL;
  signal   I2C_SDAin				:TSL;
  signal   I2C_SDAout				:TSL;
  signal   I2C_SDAoutN				:TSL;

  signal   ClkSelSig				:TVI;
  signal   ClkSig				:TSL;
  signal   TrgSelSig				:TVI;
  signal   TrgSig				:TSL;
  signal   PreTrg0Sig				:TSL;
  signal   PreTrg1Sig				:TSL;
  signal   BCN0DelSig				:TSL;
  signal   TrgDelSig				:TSL;
  signal   PreTrg0DelSig			:TSL;
  signal   PreTrg1DelSig			:TSL;
  signal   Brcst1DataReg			:TSLV(TTC_BROADCAST1_DATA_SIZE-1 downto 0);
  signal   Brcst2DataReg			:TSLV(TTC_BROADCAST2_DATA_SIZE-1 downto 0);

  constant IIPar				:TVII := TVIICreate(VIIItemDeclList,IILB_ADDR_SIZE,II_DATA_SIZE);
  signal   IIVecInt,IIVecExt,IIVecEna		:TSLV(VII(IIPar)'high downto VEC_INDEX_MIN);
  signal   IIDataSig				:TSLV(II_DATA_SIZE-1 downto 0);
  signal   IIAckInvSig				:TSL;
  signal   LIIDataExport			:TSLV(II_DATA_SIZE-1 downto 0);
  signal   LIIDataExportEnable			:TSL;
  signal   BCN0DelayValSig			:TSLV(TIME_DELAY_SIZE-1 downto 0);
  signal   TrgDelayValSig			:TSLV(TIME_DELAY_SIZE-1 downto 0);
  signal   PreTrg0DelayValSig			:TSLV(TIME_DELAY_SIZE-1 downto 0);
  signal   PreTrg1DelayValSig			:TSLV(TIME_DELAY_SIZE-1 downto 0);

begin

  L <= '0'; H <= '1';

  -- fast signal interface
  ClkSelSig <= TNconv(IIConnGetBitsData(IIVecExt,IIPar,BITS_STATUS_CLOCK_SEL,0));
  ClkSig <= ttc_clk      when (ClkSelSig=CLOCK_SEL_TTC_CLK)  else
            ttc_clk_des1 when (ClkSelSig=CLOCK_SEL_TTC_DES1) else
            ttc_clk_des2 when (ClkSelSig=CLOCK_SEL_TTC_DES2) else
            int_clock    when (ClkSelSig=CLOCK_SEL_INT)      else
            ext_clock    when (ClkSelSig=CLOCK_SEL_EXT)      else
            '0'          when (ClkSelSig=CLOCK_SEL_NONE)     else
	    '0';
  fs_clock <= ClkSig;
  --
  TrgSelSig <= TNconv(IIConnGetBitsData(IIVecExt,IIPar,BITS_STATUS_TRG_SEL,0));
  TRgSig <= ttc_l1a      when (TrgSelSig=TRG_SEL_TTC_L1A)  else
            ext_trigger  when (TrgSelSig=TRG_SEL_EXT)      else
            '0';

  process(II_reset, clock)
--    variable IIParV				:TVII(100 downto 0);
    variable PreTrg0SelVar	:TN;
    variable PreTrg1SelVar	:TN;
  begin
--    IIParV := TVIICreate(VIIItemDeclList,IILB_ADDR_SIZE,II_DATA_SIZE);
    PreTrg0SelVar := TNconv(IIConnGetBitsData(IIVecExt,IIPar,BITS_STATUS_PRETRG0_SEL,0));
    PreTrg1SelVar := TNconv(IIConnGetBitsData(IIVecExt,IIPar,BITS_STATUS_PRETRG1_SEL,0));
    if(II_reset='0') then
      Brcst1DataReg <= (others => '0');
      Brcst2DataReg <= (others => '0');
    elsif(clock'event and clock='1') then
      if (PreTrg0SelVar=PRETRG_SEL_EXT_TRG) then
        PreTrg0Sig <= TRgSig;
      elsif (PreTrg0SelVar=PRETRG_SEL_EXT_CLK) then
        PreTrg0Sig <= ext_clock;
      elsif (PreTrg0SelVar<PRETRG_SEL_NUM) then
        PreTrg0Sig <= ttc_brcst1_data(PreTrg0SelVar-PRETRG_SEL_BROADCAST1) and ttc_brcst_str1;
      else
        PreTrg0Sig <= '0';
      end if;
      if (PreTrg1SelVar=PRETRG_SEL_EXT_TRG) then
        PreTrg1Sig <= TRgSig;
      elsif (PreTrg1SelVar=PRETRG_SEL_EXT_CLK) then
        PreTrg1Sig <= ext_clock;
      elsif (PreTrg1SelVar<PRETRG_SEL_NUM) then
        PreTrg1Sig <= ttc_brcst1_data(PreTrg0SelVar-PRETRG_SEL_BROADCAST1) and ttc_brcst_str1;
      else
        PreTrg1Sig <= '0';
      end if;
      if (ttc_brcst_str1='1') then
        Brcst1DataReg <= ttc_brcst1_data;
      end if;
      if (ttc_brcst_str2='1') then
        Brcst2DataReg <= ttc_brcst2_data;
      end if;
    end if;
  end process;
  
  BCN0DelayValSig <= IIConnGetWordData(IIVecInt,IIPar,WORD_DELAY_BCN0,0);
  bcn0_delay: LPM_PULSE_DELAY
    generic map (
      LPM_DELAY_SIZE	=> TIME_DELAY_SIZE
    )
    port map(
      resetN		=> ii_reset,
      clock		=> clock,
      clk_ena		=> H,
      pulse_in		=> ttc_bcn0,
      pulse_out		=> BCN0DelSig,
      limit		=> BCN0DelayValSig
    );

  TrgDelayValSig <= IIConnGetWordData(IIVecInt,IIPar,WORD_DELAY_L1A,0);
  trg_delay: LPM_PULSE_DELAY
    generic map (
      LPM_DELAY_SIZE	=> TIME_DELAY_SIZE
    )
    port map(
      resetN		=> ii_reset,
      clock		=> clock,
      clk_ena		=> H,
      pulse_in		=> TrgSig,
      pulse_out		=> TrgDelSig,
      limit		=> TrgDelayValSig
    );

  PreTrg0DelayValSig <= IIConnGetWordData(IIVecInt,IIPar,WORD_DELAY_PRETRG0,0);
  pretrg1_delay: LPM_PULSE_DELAY
    generic map (
      LPM_DELAY_SIZE	=> TIME_DELAY_SIZE
    )
    port map(
      resetN		=> ii_reset,
      clock		=> clock,
      clk_ena		=> H,
      pulse_in		=> PreTrg0Sig,
      pulse_out		=> PreTrg0DelSig,
      limit		=> PreTrg0DelayValSig
    );

  PreTrg1DelayValSig <= IIConnGetWordData(IIVecInt,IIPar,WORD_DELAY_PRETRG0,0);
  pretrg2_delay: LPM_PULSE_DELAY
    generic map (
      LPM_DELAY_SIZE	=> TIME_DELAY_SIZE
    )
    port map(
      resetN		=> ii_reset,
      clock		=> clock,
      clk_ena		=> H,
      pulse_in		=> PreTrg1Sig,
      pulse_out		=> PreTrg1DelSig,
      limit		=> PreTrg1DelayValSig
    );

  fs_bcn0    <= BCN0DelSig;
  fs_l1a     <= TrgDelSig;
  fs_pretrg0 <= PreTrg0DelSig;
  fs_pretrg1 <= PreTrg1DelSig;
  
  --
  -- FEB test interface
  --
  FEB_test <= IIConnGetWordData(IIVecInt,IIPar,WORD_FEB_TEST_DATA,0);

  --
  -- GOL interface
  --
  GOL_laser   <= TSLconv(IIConnGetBitsData(IIVecInt,IIPar,BITS_GOL_LASER,0));
  GOL_nedge   <= TSLconv(IIConnGetBitsData(IIVecInt,IIPar,BITS_GOL_NEDGE,0));
  GOL_tx_ena  <= TSLconv(IIConnGetBitsData(IIVecInt,IIPar,BITS_GOL_TX_ENA,0));
  GOL_tx_err  <= TSLconv(IIConnGetBitsData(IIVecInt,IIPar,BITS_GOL_TX_ERR,0));

  --
  -- I2C interface
  --
  I2C_SCLoutN <= not(I2C_SCLout);
  I2C_SDAoutN <= not(I2C_SDAout);
  CI2Cscl: ALTERA_TRI port map (data_in => L, data_out => I2C_SCLin, data_tri => i2c_scl, ena => I2C_SCLoutN);
  CI2Csda: ALTERA_TRI port map (data_in => L, data_out => I2C_SDAin, data_tri => i2c_sda, ena => I2C_SDAoutN);
  --CI2Cscl: ALTERA_TRI port map (data_in => L, data_out => I2C_SCLin, data_tri => i2c_scl, ena => L);
  --CI2Csda: ALTERA_TRI port map (data_in => L, data_out => I2C_SDAin, data_tri => i2c_sda, ena => L);
  
  I2C_AreaEnaSig <= IIConnGetAreaEnable(IIVecEna,IIPar,AREA_I2C);
  I2C_AreaWrSig <= (others => '1') when IIConnGetAreaWrite(IIVecEna,IIPar,AREA_I2C,ii_save)='1' else
                   (others => '0');

  I2C: wishbone_i2c_master
    port map (
      -- wishbone signals
      CLK_I	=> clock,
      RST_I	=> L,
      nRESET	=> II_reset,
      ADR_I	=> ii_addr(AREA_I2C_ADDR_SIZE-1 downto 0),
      DAT_I	=> ii_data(AREA_I2C_DATA_SIZE-1 downto 0),
      DAT_O	=> I2C_DataOut,
      SEL_I	=> I2C_AreaWrSig,
      WE_I	=> I2C_AreaEnaSig,
      STB_I	=> I2C_AreaEnaSig,
      CYC_I	=> I2C_AreaEnaSig,

      -- I2C signals
      SCLi	=> I2C_SCLin,
      SCLo	=> I2C_SCLout,
      SDAi	=> I2C_SDAin,
      SDAo	=> I2C_SDAout
    );

  --
  -- internal interface
  --
  process(ii_reset, ii_save)
  begin
    if(ii_reset='0') then
      IIVecInt <= IIReset(IIVecInt,IIPar);
    elsif(ii_save'event and ii_save='1') then
      if(ii_oper='0') then
        IIVecInt <= IISaveAccess(IIVecInt,IIPar,ii_addr,IIDataSig);
      end if;
    end if;
  end process;
  --
  IIVecEna <= IIWriteEnable(IIVecInt,IIPar,ii_oper,ii_write,ii_addr,IIDataSig);

  IIVecExt <= (IIWriteAccess(IIVecInt,IIPar,ii_oper,ii_write,ii_addr,IIDataSig)
              or IIConnPutWordData(IIVecInt,IIPar,WORD_IDENTIFIER,0,TSLVconv(LB_CONTR_IDENTIFIER,II_DATA_SIZE))
              or IIConnPutWordData(IIVecInt,IIPar,WORD_VERSION,0,TSLVconv(LB_CONTR_VERSION,II_DATA_SIZE))
  
              or IIConnPutBitsData(IIVecInt,IIPar,BITS_GOL_READY,0,TSLVconv(GOL_ready))
  
              or IIConnPutAreaData(IIVecInt,IIPar,AREA_I2C, I2C_DataOut)
	      );
  LIIDataExport <= IIReadAccess(IIVecExt,IIPar,ii_oper,ii_write,ii_addr);
  --
  LIIDataExportEnable <= not(ii_oper) and ii_write;
  CIIData: ALTERA_BUSTRI
    generic map (LPM_WIDTH => II_DATA_SIZE)
    port map    (data_in   => LIIDataExport,
                 data_out  => IIDataSig,
                 data_tri  => ii_data,
                 ena       => LIIDataExportEnable);
  --
  IIAckInvSig <= not(ii_oper);
  CIIAck: ALTERA_TRI
    port map(data_in  => L, 
             data_tri => ii_ack,
             ena      => IIAckInvSig
    );
  --
  -- diagnostic interface
  --
  led_clock: LPM_CLOCK_PULSER
    generic map (
      LPM_DIVIDE_PAR		=> CLK_FREQ/LED_PULSE_FREQ,
      LPM_DATA_PULSE_WIDTH	=> LED_PULSE_WIDTH,
      LPM_PULSE_INVERT		=> TRUE	
    )
    port map (
      resetN			=> II_reset,
      clock			=> int_clock,
      pulse			=> led(0)
    );

  -- led(0) <= '0';
  led(1) <= not(BCN0DelSig);
  led(2) <= not(PreTrg0DelSig);
  led(3) <= not(PreTrg1DelSig);

end behaviour;
