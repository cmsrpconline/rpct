#include "rpct/devices/TMemoryMapLBWrapper.h"

using namespace std;

namespace rpct {

void TMemoryMapLBWrapper::writeArea(int id, void* data, int start_idx, int n) {
    const TItemInfo& item = CheckWriteAccess(id);

    //cout << "TMemoryMapLBWrapper::start_idx = " << start_idx << " n = " << n << endl;
    
    
    if ((start_idx < 0) || (start_idx >= item.Number))
        throw TException("TMemoryMap::writeArea: start_idx out of bounds");

    if (n == npos) {
        n = getItemDesc(id).Number - start_idx;
    } else if ((n < 0) || ((n + start_idx) > item.Number)) {
        throw TException("TMemoryMap::writeArea: n out of scope");
    }

    int width = (item.Width-1)/8+1; // width in bytes

    int blockLen = 1;
    while (blockLen < item.Number) {
        blockLen <<= 1;
    }
    
    int dataLen = n * width; 
    //cout << "dataLen = " << dataLen << endl;
    
    /*cout << "TMemoryMapLBWrapper::writeArea ";
    for (int i = 0; i < n * width; i++) {
        int val = *(((unsigned char*)data) + i);
        cout << hex << val;
    }
    cout << endl;*/

    if (item.AddrLen > 1) {
        unsigned char* cdata = (unsigned char*) data;
        unsigned short s;
        unsigned short sTmp;
        int cdataIndex;
        for (int i = 0; i < n; i++) {
            for (int block = 0; block < item.AddrLen; block++) {
                cdataIndex = i * width + block * GetDataSize();

                // first copy the higher order byte if it is accessible  
                s = *(cdata + cdataIndex);
                //cout << "cdataIndex = " << cdataIndex << " s " << s << endl;
                
                if ((cdataIndex + 1) < dataLen) {
                   sTmp = *(cdata + cdataIndex + 1);
                    s |= (sTmp << 8) & 0xff00;
                    //cout <<  " poprawka s " << s << endl;
                } 
                GetHardwareIfc()->Write(item.AddrPos + block*blockLen + start_idx + i, &s, //cdata + i*width + block*DataSize,
                        1, GetDataSize());
            }
        }
    } else {
        int size = GetDataSize() * n * item.AddrLen; // how many bytes needed to store the data
        unsigned char* buffer = new unsigned char[size];
        try {

            memset(buffer, 0, size);

            int bytes = (width >= GetDataSize()) ? GetDataSize() : width;

            for (int i = 0; i < n; i++) {
                for (int byte = 0; byte < bytes; byte++) {
                    buffer[i*GetDataSize() + byte] = ((unsigned char*)data)[i*width + byte];
                }
            }

            GetHardwareIfc()->Write(item.AddrPos + start_idx,
                    buffer,
                    n,
                    GetDataSize());

            delete [] buffer;
        } catch(...) {
            delete [] buffer;
            throw;
        }
    }

}

void TMemoryMapLBWrapper::writeArea(int id, uint32_t* data, int start_idx, int n) {
    const TItemInfo& item = CheckWriteAccess(id);

    if ((start_idx < 0) || (start_idx >= item.Number))
        throw TException("TMemoryMap::writeArea: start_idx out of bounds");

    if (n == npos) {
        n = getItemDesc(id).Number - start_idx;
    } else if ((n < 0) || ((n + start_idx) > item.Number)) {
        throw TException("TMemoryMap::writeArea: n out of scope");
    }

    int width = (item.Width-1)/8+1; // width in bytes

    int blockLen = 1;
    while (blockLen < item.Number) {
        blockLen <<= 1;
    }

    if (item.AddrLen > 1) {
        unsigned char* cdata = (unsigned char*) data;
        for (int i = 0; i < n; i++) {
            for (int block = 0; block < item.AddrLen; block++) {
                GetHardwareIfc()->Write(item.AddrPos + block*blockLen + start_idx + i, cdata + i * sizeof(uint32_t)
                        + block * GetDataSize(), 1, GetDataSize());
            }
        }
    } else {
        int size = GetDataSize() * n * item.AddrLen; // how many bytes needed to store the data
        unsigned char* buffer = new unsigned char[size];
        try {

            memset(buffer, 0, size);

            int bytes = (width >= GetDataSize()) ? GetDataSize() : width;

            for (int i = 0; i < n; i++) {
                for (int byte = 0; byte < bytes; byte++) {
                    buffer[i * GetDataSize() + byte] = ((unsigned char*)data)[i * sizeof(uint32_t) + byte];
                }
            }

            GetHardwareIfc()->Write(item.AddrPos + start_idx,
                    buffer,
                    n,
                    GetDataSize());

            delete [] buffer;
        } catch(...) {
            delete [] buffer;
            throw;
        }
    }

}

/*
 void TMemoryMapLB::writeArea(int id, uint32_t* data, int start_idx, int n)
 {
 TItemInfo& item = CheckWriteAccess(id);

 if (item.Width > sizeof(uint32_t)*8)
 throw TException("TMemoryMap::writeArea: area too wide to fit data to uint32_t");

 if ((start_idx < 0) || (start_idx >= item.Number))
 throw TException("TMemoryMap::writeArea: start_idx out of bounds");

 if (n == npos)
 n = getItemDesc(id).Number - start_idx;
 else
 if ((n < 0) || ((n + start_idx) > item.Number))
 throw TException("TMemoryMap::writeArea: n out of scope");

 // create temporary buffer
 int size = DataSize * item.Number * item.AddrLen;
 char* buffer = new char[size];
 int width = (item.Width-1)/8+1;  

 int blockLen = 1;
 while (blockLen  < item.Number)
 blockLen <<= 1;

 try {

 // reorganize byte order according to DataSize
 for(int block=0; block<item.AddrLen; block++) {

 // copy data appropriate for current block
 memset(buffer, 0, size);

 //int bytes = ((width - block*DataSize) - 1)/DataSize + 1;
 int bytes = width - block*DataSize;
 bytes = (bytes >= DataSize) ? DataSize : bytes;

 for(int i=0; i<n; i++)
 for(int byte=0; byte<bytes; byte++)
 buffer[i*DataSize + byte] = ((char*)data)[i*sizeof(uint32_t) + block*DataSize + byte];

 HardwareIfc->Write(item.AddrPos + block * blockLen + start_idx,
 buffer,
 n,
 DataSize);

 };

 delete [] buffer;
 } catch(...) {
 delete [] buffer;
 throw;
 }
 }      */

}
