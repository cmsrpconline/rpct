#include "rpct/devices/TTUTrigBxData.h"
#include "rpct/devices/cmssw/RPCConst.h"
#include "rpct/std/bitcpy.h"
#include "rpct/std/tbutil.h"

#include <iomanip>
#include<sstream>

using namespace std;

namespace rpct {

TTUTrigBxDataFactory TTUTrigBxDataFactory::instance_;

TTUTrigBxData::TTUTrigBxData(IDiagnosticReadout::BxData& bxData)
        : StandardBxData(bxData) {
    unsigned int pos = 0;

	unsigned int pacLogConeCnt = 12;
 	unsigned int pacLogConeOutBitsCnt = RPCPacOutMuon::getMuonBitsCnt();

    unsigned int bcn0Bit = 0; //1 bit

    unsigned int bcnBitsCnt = 4;
    unsigned int bcnData = 0;

    unsigned int channelValidBitsCnt = 18;

    unsigned int linksCnt = 18;
    unsigned int linkBitsCnt = 24;
    unsigned int linkData = 0;

    ostringstream ostr;

	/*for(unsigned int iLC = 0, index = 0; iLC < pacLogConeCnt; iLC++, index++) {
		unsigned int muonData = 0;
		bitcpy(&muonData, 0, bxData_.getData(), pos, pacLogConeOutBitsCnt);          pos += pacLogConeOutBitsCnt;	
		RPCPacOutMuon * muon = new RPCPacOutMuon(muonData, index);
		dataStr_ = muon->toString(2) + " | " + dataStr_;
		if(muonData != 0)
			muonVector_.push_back(muon); 
		else 
			delete muon;
	}
*/

    //needs to updated!!!!!!!!!!!!!!!!!!!!!!!!!!!
    bitcpy(&bcn0Bit,    0, bxData_.getData(), pos, 1);                                pos += 1;
    bitcpy(&bcnData,    0, bxData_.getData(), pos, bcnBitsCnt);                       pos += bcnBitsCnt;
    bitcpy(&channelsValid_,    0, bxData_.getData(), pos, channelValidBitsCnt);       pos += channelValidBitsCnt;

    for(int iO = 0, index = 0; iO < 6; iO++) {
        for(int iL = 0; iL < 3; iL++, index++) {
            bitcpy(&linkData, 0, bxData_.getData(), pos, linkBitsCnt);                pos += linkBitsCnt;
            dataStr_ = dataToHex(&linkData, linkBitsCnt) + " " + dataStr_;            
            if(linkData != 0) {
            	LmuxBxData* lmuxBxData = new LmuxBxData(linkData, index);            
            	lmuxBxDataVector_.push_back(lmuxBxData);
            }
        }
        dataStr_ = " | " + dataStr_ ;
    }
    
	ostr.clear();
	ostr<<hex<<setw(3)<<bcnData<<" "<<bcn0Bit<<" "<<setw(5)<<channelsValid_;
	dataStr_ = ostr.str() + " | " + dataStr_; 
}



}
