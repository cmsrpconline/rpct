#include "rpct/std/NullPointerException.h"
#include "rpct/devices/hsb.h"
#include "rpct/devices/VmeCrate.h"
#include "rpct/devices/HalfSortSettings.h"
#include "rpct/devices/FixedHardwareSettings.h"
#include "rpct/devices/HalfSortBxData.h"
#include "rpct/ii/ConfigurationFlags.h"


//---------------------------------------------------------------------------


using namespace std;
using namespace log4cplus;

namespace rpct {

Logger THsbDevice::logger = Logger::getInstance("THsbDevice");
void THsbDevice::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}
//---------------------------------------------------------------------------
#define IID_FILE "RPC_system_def.iid"
#define IID_CLASS THsbDevice
#include "iid2cdef2.h"

#define IID_FILE "RPC_TB3_def.iid"
#define IID_CLASS THsbDevice
#include "iid2cdef2.h"

#define IID_FILE "RPC_TC_SORT_def.iid"
#define IID_CLASS THsbDevice
#include "iid2cdef2.h"

#define IID_FILE "RPC_SORT_def.iid"
#define IID_CLASS THsbDevice
#include "iid2cdef2.h"

//---------------------------------------------------------------------------


#define IID_FILE "RPC_TB3_vme.iid"
#define IID_CLASS THsbVme
#include "iid2c2.h"

const HardwareItemType THsbVme::TYPE(HardwareItemType::cDevice, "HSBVME");

THsbVme::THsbVme(int vme_board_address, IBoard& board) : THsbDevice(
        board.getId() * 1000,
        VME_IDENTIFIER.c_str(),
        WORD_IDENTIFIER,
        "VME",
        TYPE,
        VME_VERSION.c_str(),
        WORD_VERSION,
        WORD_CHECKSUM,
        0,
        board,
        -1,
        BuildMemoryMap(),
        vme_board_address
), ABootContr(0), JtagIO(0)
{
}

void THsbVme::SetJtagEnable(bool value)
{
    writeBits(BITS_STATUS_JTAG_ENA, value ? 1 : 0);
}

THsbVme::~THsbVme()
{
    delete ABootContr;
    delete JtagIO;
}

//---------------------------------------------------------------------------

#ifdef CII_HS
#define IID_CLASS THsbSortHalf
#include "CII_SC_HSORT_iicfg_tab.cpp"
#undef IID_CLASS
#else
#define IID_FILE "RPC_SORT_half.iid"
#define IID_CLASS THsbSortHalf
#include "iid2c2.h"
const int THsbSortHalf::II_ADDR_SIZE = THsbSortHalf::HSORT_ADDR_WIDTH;
const int THsbSortHalf::II_DATA_SIZE = THsbSortHalf::HSORT_DATA_WIDTH;
#endif

const HardwareItemType THsbSortHalf::TYPE(HardwareItemType::cDevice, "HALFSORT");

THsbSortHalf::THsbSortHalf(int id, int vme_board_address, int ii_addr, const char* desc, IBoard& board) :
    THsbDevice(id, HSORT_IDENTIFIER.c_str(), WORD_IDENTIFIER, desc, TYPE, HSORT_VERSION.c_str(), WORD_VERSION,
            WORD_CHECKSUM, ii_addr, board, 0, BuildMemoryMap(), vme_board_address), DaqDiagCtrl(0),
            DiagnosticReadout(0), PulserDiagCtrl(0), Pulser(0), statisticsDiagCtrl_(0), I2C(0), TTCrxI2C(7, board.getDescription()),
            qpll_(*this, logger,
            		VECT_QPLL,
            		BITS_QPLL_MODE,
            		BITS_QPLL_CTRL,
            		BITS_QPLL_SELECT,
            		BITS_QPLL_LOCKED,
            		BITS_QPLL_ERROR,
            		BITS_QPLL_CLK_LOCK),
            EnabledInputs(16, 0ul), EnabledInChannles(8, 0ul),
            monRecErrorAnalyzer_(MonitorItemName::RECEIVER, 1, 1, 1000, 0xffffff)
{
    monRecErrorAnalyzer_.setMaxCounterVal((1ull << memoryMap_->GetItemInfo(WORD_REC_ERROR_COUNT).Width) - 1);

    monitorItems_.push_back(MonitorItemName::FIRMWARE);
    monitorItems_.push_back(MonitorItemName::QPLL);
    monitorItems_.push_back(MonitorItemName::TTCRX);
    monitorItems_.push_back(MonitorItemName::RECEIVER);
}

THsbSortHalf::~THsbSortHalf() {
    delete DaqDiagCtrl;
    delete DiagnosticReadout;
    delete PulserDiagCtrl;
    delete Pulser;
    delete I2C;

    for (unsigned int i = 0; i < histoManagers_.size(); i++) {
        delete histoManagers_[i];
    }
}


TDiagCtrl& THsbSortHalf::GetDaqDiagCtrl() {
    if (DaqDiagCtrl == NULL) {
        DaqDiagCtrl = new TDiagCtrl(*this,
                VECT_DAQ,
                BITS_DAQ_PROC_REQ,
                BITS_DAQ_PROC_ACK,
                BITS_DAQ_TIMER_LOC_ENA,
                0,
                BITS_DAQ_TIMER_START,
                BITS_DAQ_TIMER_STOP,
                BITS_DAQ_TIMER_TRIG_SEL,
                WORD_DAQ_TIMER_LIMIT,
                WORD_DAQ_TIMER_COUNT,
                WORD_DAQ_TIMER_TRG_DELAY,
                "DAQ_DIAG_CTRL");
    }

    return *DaqDiagCtrl;
}


TDiagCtrl& THsbSortHalf::GetPulserDiagCtrl() {
    if (PulserDiagCtrl == NULL) {
        PulserDiagCtrl = new TDiagCtrl(*this,
                VECT_PULSER,
                BITS_PULSER_PROC_REQ,
                BITS_PULSER_PROC_ACK,
                BITS_PULSER_TIMER_LOC_ENA,
                0,
                BITS_PULSER_TIMER_START,
                BITS_PULSER_TIMER_STOP,
                BITS_PULSER_TIMER_TRIG_SEL,
                WORD_PULSER_TIMER_LIMIT,
                WORD_PULSER_TIMER_COUNT,
                WORD_PULSER_TIMER_TRG_DELAY,
                "PULSER_DIAG_CTRL");
    }

    return *PulserDiagCtrl;
}

TPulser& THsbSortHalf::GetPulser() {
    if (Pulser == NULL) {
        Pulser = new HalfSortPulser(*this,
                "PULSER",
                GetPulserDiagCtrl(),
                AREA_MEM_PULSE,
                WORD_PULSER_LENGTH,
                BITS_PULSER_REPEAT_ENA,
                BITS_PULSER_OUT_ENA);
    }
    return *Pulser;
}

TStandardDiagnosticReadout& THsbSortHalf::GetDiagnosticReadout() {
    if (DiagnosticReadout == 0) {
        DiagnosticReadout = new TStandardDiagnosticReadout(*this,
                "READOUT",
                &HalfSortBxDataFactory::getInstance(),
                GetDaqDiagCtrl(),
                VECT_DAQ,
                BITS_DAQ_EMPTY,
                BITS_DAQ_EMPTY_ACK,
                BITS_DAQ_LOST,
                BITS_DAQ_LOST_ACK,
                VECT_DAQ_WR,
                BITS_DAQ_WR_ADDR,
                BITS_DAQ_WR_ACK,
                VECT_DAQ_RD,
                BITS_DAQ_RD_ADDR,
                BITS_DAQ_RD_ACK,
                WORD_DAQ_MASK,
                WORD_DAQ_DATA_DELAY,
                AREA_MEM_DAQ,
                DAQ_DIAG_DATA_SIZE,
                DAQ_DIAG_TRIG_NUM,
                DAQ_DIAG_TIMER_SIZE,
                TTC_BCN_EVT_WIDTH);
        /*GetDaqDiagCtrl(),
        VECT_DAQ,
        BITS_DAQ_EMPTY,
        BITS_DAQ_EMPTY_ACK,
        BITS_DAQ_LOST,
        BITS_DAQ_LOST_ACK,
        VECT_DAQ_WR,
        BITS_DAQ_WR_ADDR,
        BITS_DAQ_WR_ACK,
        VECT_DAQ_RD,
        BITS_DAQ_RD_ADDR,
        BITS_DAQ_RD_ACK,
        WORD_DAQ_MASK,
        0,
        AREA_MEM_DAQ,
        DAQ_DIAG_DATA_SIZE,
        DAQ_DIAG_TRIG_NUM,
        DAQ_DIAG_TIMER_SIZE);*/
    }
    return *DiagnosticReadout;
}

THsbSortHalf::TDiagCtrlVector& THsbSortHalf::GetDiagCtrls() {
    if (DiagCtrls.empty()) {
        DiagCtrls.push_back(&GetDaqDiagCtrl());
        DiagCtrls.push_back(&GetPulserDiagCtrl());
        DiagCtrls.push_back(&getStatisticsDiagCtrl());
    }
    return DiagCtrls;
}

THsbSortHalf::TDiagVector& THsbSortHalf::GetDiags() {
    if (Diags.empty()) {
        Diags.push_back(&GetDiagnosticReadout());
        Diags.push_back(&GetPulser());

        getHistoManagers();
        for (unsigned int i = 0; i < histoManagers_.size(); i++) {
            Diags.push_back(histoManagers_[i]);
        }
    }
    return Diags;
}


void THsbSortHalf::ConfigureDiagnosable(TTriggerDataSel triggerDataSel, uint32_t dataTrgDelay) {
    writeBits(BITS_STATUS_TRG_DATA_SEL, triggerDataSel);
    writeWord(WORD_DATA_TRG_DELAY, 0, dataTrgDelay);
}

HistoMgrVector& THsbSortHalf::getHistoManagers() {
    if (histoManagers_.empty()) {
        histoManagers_.push_back(new TIIHistoMgr(*this, "HS_INPUT_RATE", getStatisticsDiagCtrl(), AREA_MEM_RATE));
    }

    return histoManagers_;
}

TDiagCtrl& THsbSortHalf::getStatisticsDiagCtrl() {
    if (statisticsDiagCtrl_ == 0) {
        statisticsDiagCtrl_ = new TDiagCtrl(*this,
                                           VECT_RATE,
                                           BITS_RATE_PROC_REQ,
                                           BITS_RATE_PROC_ACK,
                                           BITS_RATE_TIMER_LOC_ENA,
                                           0,
                                           BITS_RATE_TIMER_START,
                                           BITS_RATE_TIMER_STOP,
                                           BITS_RATE_TIMER_TRIG_SEL,
                                           WORD_RATE_TIMER_LIMIT,
                                           WORD_RATE_TIMER_COUNT,
                                           0, //WORD_RATE_TIMER_TRG_DELAY,
                                           "STATISTICS_DIAG_CTRL");
    }
    return *statisticsDiagCtrl_;
}

void THsbSortHalf::ResetQPLL() {
	qpll_.reset();
}

TIII2CMasterCore& THsbSortHalf::GetI2C() {
    if (I2C == NULL) {

        I2C = new TIII2CMasterCore(*this, AREA_I2C);
        I2C->SetPrescaleValue(100);
        I2C->WriteControlReg(TI2CMasterCore::BIT_EN);
    }

    return *I2C;
}

void THsbSortHalf::ResetTTC() {
    writeBits(BITS_STATUS_TTC_RESET, 1);
    tbsleep(3000);
    if (readBits(BITS_STATUS_TTC_READY) != 0) {
        throw TException("TFsbSortFinal::ResetTTC: BITS_TTC_READY did not go down after BITS_TTC_RESET, 1");
        //LOG4CPLUS_ERROR(logger, "TTCSortTCSort::ResetTTC: BITS_TTC_READY did not go down after BITS_TTC_RESET, 1");
    }
    writeBits(BITS_STATUS_TTC_RESET, 0);
    tbsleep(500);
    if (readBits(BITS_STATUS_TTC_READY) != 1)
        throw TException("TFsbSortFinal::ResetTTC: BITS_TTC_READY did not go up");
}

void THsbSortHalf::ResetPLL() {
    writeBits(BITS_STATUS_PLL_RESET, 1);
    tbsleep(100);
    writeBits(BITS_STATUS_PLL_RESET, 0);
    tbsleep(100);
    readBits(BITS_STATUS_PLL_UNLOCK);
    if (readBits(BITS_STATUS_PLL_UNLOCK) == 1) {
        //throw TException(getName() + "THsbSortHalf::ResetPLL: BITS_STATUS_PLL_UNLOCK did not go down");
        LOG4CPLUS_WARN(logger, getBoard().getDescription() + " ResetPLL: BITS_STATUS_PLL_UNLOCK did not go down");
    }
}

void THsbSortHalf::InitTTC() {
    writeWord(WORD_TTC_ID_MODE, 0, TTCrxI2C.GetAddress());
    writeBits(BITS_STATUS_TTC_INIT_ENA, 1);
    ResetTTC();
    writeBits(BITS_STATUS_TTC_INIT_ENA, 0);

    // Enable Clock40Des2 output
    LOG4CPLUS_WARN(logger, "THsbSortHalf: Clock40Des2 not enabled ");
    //GetTTCrxI2C().WriteControlReg(GetTTCrxI2C().ReadControlReg() | 0x8);

    GetTTCrxI2C().WriteFineDelay1(0);
    GetTTCrxI2C().WriteFineDelay2(0);
    GetTTCrxI2C().WriteCoarseDelay(0, 0);
}



void THsbSortHalf::SetEnabledInputs(const boost::dynamic_bitset<>& enabledInputs) {
	//enabledInputs can have smaller size then 16 bits - or even 0 if non bit is set
	EnabledInputs.reset();
	for (unsigned int i = 0; i < enabledInputs.size(); i++) {
		EnabledInputs[i] = enabledInputs[i];
	}
    /*if(enabledInputs.size() != 8) {
        EnabledInputs.resize(8, false);
    }*/

    /*for(unsigned int iB = 0; iB < EnabledInputs.size(); iB++) {
        EnabledInChannles[2 * iB] = EnabledInputs[iB];
        EnabledInChannles[2 * iB + 1] = EnabledInputs[iB];
    }*/
    //EnabledInChannles = EnabledInputs;

	EnabledInChannles.reset();
    unsigned int recChanEna = 0;
    for (int i = 0; i < 8; i++) {
        if(EnabledInputs[2 * i] || EnabledInputs[2 * i +1]) {
            recChanEna |=  1 << i;
            EnabledInChannles[i] = true;
        }
    }

    writeWord(WORD_REC_CHAN_ENA, 0, recChanEna);
    //writeWord(WORD_REC_CHAN_ENA, 0, (GetEnabledInputs().to_ulong()));

    writeWord(WORD_REC_TEST_ENA, 0,  (~(GetEnabledInputs().to_ulong())) & 0xffff );
    //writeWord(WORD_REC_TEST_ENA, 0, uint32_t(0));//
    LOG4CPLUS_INFO(logger, "THsbSortHalf: SetEnabledInputs: EnabledInputs "<<EnabledInputs);
    LOG4CPLUS_INFO(logger, "THsbSortHalf: SetEnabledInputs: EnabledInChannles "<<EnabledInChannles);
}

bool THsbSortHalf::checkResetNeeded() {
    return (readBits(BITS_STATUS_TTC_READY) == 0) || (readBits(BITS_QPLL_LOCKED) == 0);
}

void THsbSortHalf::reset() {
    //    writeWord(WORD_REC_TEST_RND_ENA,0, uint32_t(0));
    /*    WriteWord(WORD_SEND_TEST_ENA, 0, uint32_t(0));
    WriteWord(WORD_SEND_TEST_RND_ENA, 0, uint32_t(0));
    WriteWord(WORD_SEND_TEST_DATA, 0, uint32_t(0));*/
    writeWord(WORD_BCN0_DELAY, 0, FixedHardwareSettings::HSB_BC0_DELAY);
    writeBits(BITS_PULSER_OUT_ENA, uint32_t(0));
}

void THsbSortHalf::configure(DeviceSettings* settings, int flags) {
	if (settings == 0) {
		throw NullPointerException("THsbSortHalf::configure: settings == 0");
	}
	HalfSortSettings* s = dynamic_cast<HalfSortSettings*>(settings);
	if (s == 0) {
		throw IllegalArgumentException("HalfSortSettings::configure: invalid cast for settings");
	}

	bool coldSetup = flags & ConfigurationFlags::FORCE_CONFIGURE;
    if(!coldSetup) {
    	if( (s->getConfigurationId() & 0xffff) != readWord(WORD_USER_REG1, 0) ) {
    		coldSetup = true;
    	}
    }

    LOG4CPLUS_INFO(logger, getDescription() << " configure: coldSetup = " << (coldSetup ? "true" : "false"));

    if (coldSetup) {
        reset();

        SetEnabledInputs(s->getRecChanEna());

        EnableTransmissionCheck(true, true);

        writeWord(WORD_REC_ERROR_COUNT, 0, uint32_t(0));

        for(unsigned int i = 0; i < 16; i++) {
            writeWord(WORD_REC_FAST_CLK_INV, i, s->getRecFastClkInv()[i]);
           // writeWord(WORD_REC_FAST_CLK90, i, s->getRecFastClk90()[i]);
            writeWord(WORD_REC_FAST_REG_ADD, i, s->getRecFastRegAdd()[i]);
        }
        //writeWord(WORD_REC_FAST_DATA_DELAY, 0, s->getRecFastDataDelay().to_ulong());

        boost::dynamic_bitset<> copyRecFastDataDelay = s->getRecFastDataDelay(); //make a copy
        copyRecFastDataDelay.resize(getItemDesc(WORD_REC_FAST_DATA_DELAY).Width, false);
        char* bits = bitsetToBits(copyRecFastDataDelay);
        writeWord(WORD_REC_FAST_DATA_DELAY, 0, bits);

        writeWord(WORD_REC_DATA_DELAY, 0, s->getRecDataDelay().to_ulong());
        delete bits;

        writeWord(WORD_USER_REG1, 0, s->getConfigurationId() & 0xffff);
    }
    else {
        if (settings == 0) {
            throw NullPointerException("THsbSortHalf::configure: settings == 0");
        }
        HalfSortSettings* s = dynamic_cast<HalfSortSettings*>(settings);
        SetEnabledInputs(s->getRecChanEna());
    }
}


void THsbSortHalf::enable() {
    writeWord(WORD_REC_ERROR_COUNT, 0, uint32_t(0));
    monRecErrorAnalyzer_.reset();
}

void THsbSortHalf::EnableTransmissionCheck(bool enableBCNcheck, bool enableDataCheck) {
    bool checkDataEna = enableBCNcheck;
    bool checkEna = enableDataCheck;

    uint32_t zero = 0;
    if(checkDataEna) {
        writeWord(THsbSortHalf::WORD_REC_CHECK_DATA_ENA, 0, GetEnabledInputs().to_ulong());//0x1ff
        //writeWord(THsbSortHalf::WORD_SEND_CHECK_DATA_ENA, 0, 0x1);
    }
    else {
        writeWord(THsbSortHalf::WORD_REC_CHECK_DATA_ENA, 0, zero);
        //writeWord(THsbSortHalf::WORD_SEND_CHECK_DATA_ENA, 0, zero);
    }
    if(checkEna) {
        writeWord(THsbSortHalf::WORD_REC_CHECK_ENA, 0, GetEnabledInputs().to_ulong());
        //writeWord(THsbSortHalf::WORD_SEND_CHECK_ENA, 0, 0x1);
    }
    else {
        writeWord(THsbSortHalf::WORD_REC_CHECK_ENA, 0, zero);
        //writeWord(THsbSortHalf::WORD_SEND_CHECK_ENA, 0, zero);
    }
}


void THsbSortHalf::monitor(volatile bool *stop) {
    monitor(monitorItems_);
}

void THsbSortHalf::monitor(MonitorItems& items) {
    warningStatusList_.clear();
    for (MonitorItems::iterator iItem = items.begin(); iItem != items.end(); ++iItem) {
        if ((*iItem) == MonitorItemName::FIRMWARE) {
            string msg = monitorVersion();
            if (msg.size() != 0) {
                warningStatusList_.push_back(MonitorableStatus(MonitorItemName::FIRMWARE, MonitorableStatus::ERROR, msg, 0));
            }
        }
        else if ((*iItem) == MonitorItemName::QPLL) {
            if (readBits(BITS_QPLL_LOCKED) != 1) {
                if (readBits(BITS_QPLL_LOCKED) != 1) {
                    warningStatusList_.push_back(MonitorableStatus(MonitorItemName::QPLL, MonitorableStatus::ERROR, "BITS_QPLL_LOCKED is not up", 0));
                }
                else {
                    warningStatusList_.push_back(MonitorableStatus(MonitorItemName::QPLL, MonitorableStatus::WARNING, "BITS_QPLL_LOCKED is not up", 0));
                }
            }
        }
        else if ((*iItem) == MonitorItemName::TTCRX) {
            if (readBits(BITS_STATUS_TTC_READY) != 1) {
                warningStatusList_.push_back(MonitorableStatus(MonitorItemName::TTCRX, MonitorableStatus::ERROR,
                        "BITS_TTC_READY is not up", 0));
            }
        } else if ((*iItem) == MonitorItemName::RECEIVER) {
            uint32_t recErrCnt = readWord(WORD_REC_ERROR_COUNT, 0);
            if (monRecErrorAnalyzer_.newValue(recErrCnt) != MonitorableStatus::OK) {
                warningStatusList_.push_back(monRecErrorAnalyzer_.getStatus());
            }
        }
    }
}
//---------------------------------------------------------------------------

Logger THsb::logger = Logger::getInstance("THsb");
void THsb::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}
const HardwareItemType THsb::TYPE(HardwareItemType::cDevice, "HSB");

THsb::THsb(int ident, const char* desc, TVMEInterface* vme, int vme_board_address, VmeCrate* c, int pos, int hsbSortHalfChipId)
: BoardBase(ident, desc, TYPE, c, pos), VME(vme), HsbVme(vme_board_address, *this),
HsbSortHalf(hsbSortHalfChipId, vme_board_address, 0, "HSB_SORT_HALF", *this), VMEAddress(vme_board_address)
{
    Init();
    HsbVme.getMemoryMap().SetHardwareIfc(
            new TIIVMEAccess(
                    HsbVme.GetVMEBoardBaseAddr(),
                    HsbVme.getBaseAddress() << HsbVme.GetVMEBaseAddrShift(),
                    vme,
                    0xf8000),
                    true);
    HsbSortHalf.getMemoryMap().SetHardwareIfc(
            new TIIVMEAccess(
                    HsbSortHalf.GetVMEBoardBaseAddr(),
                    HsbSortHalf.getBaseAddress() << HsbSortHalf.GetVMEBaseAddrShift(),
                    vme,
                    0), //0xf8000),
                    true);

    if( string(desc) == string("HSB_1") ){
        number_ = 1;
    }
    else {
        number_ = 0;
    }
}

THsb::~THsb() {
}

void THsb::Init() {
    devices_.push_back(&HsbVme);
    devices_.push_back(&HsbSortHalf);
}

void THsb::configure(ConfigurationSet* configurationSet, int configureFlags) {
/*    if(HsbSortHalf.readWord(THsbSortHalf::WORD_TTC_ID_MODE, 0) == 0 ||
       HsbSortHalf.readBits(THsbSortHalf::BITS_STATUS_TTC_READY) != 1 ||
       HsbSortHalf.readBits(THsbSortHalf::BITS_QPLL_LOCKED) != 1 ||
       HsbSortHalf.readBits(THsbSortHalf::BITS_STATUS_PLL_UNLOCK) == 1 ) {
        HsbSortHalf.InitTTC();
        HsbSortHalf.ResetQPLL();
        HsbSortHalf.ResetPLL();
        LOG4CPLUS_INFO(logger, "THsb::configure: InitTTC(), ResetQPLL(), ResetPLL() was  executed");
    }
    else {
        LOG4CPLUS_INFO(logger, "THsb::configure: the TTCrx, QPLL and PLL was OK, the InitTTC() nor ResetQPLL() nor ResetPLL() was not executed");
    }*/
    BoardBase::configure(configurationSet, configureFlags);
}

void THsb::selfTest() {
    bool ok = false;
    unsigned int value = HsbSortHalf.readBits(THsbSortHalf::BITS_TEST_CNT_DES1);
    for(int i = 0; i < 100; i++) {
        if(value != HsbSortHalf.readBits(THsbSortHalf::BITS_TEST_CNT_DES1) ) {
            ok = true;
            break;
        }
    }
    if(!ok) {
        throw TException("THsb::selfTest: BITS_TEST_CNT_DES1 is not changing");
    }

    ok = false;
    value = HsbSortHalf.readBits(THsbSortHalf::BITS_TEST_CNT_BCN0);
    for(int i = 0; i < 100; i++) {
        if(value != HsbSortHalf.readBits(THsbSortHalf::BITS_TEST_CNT_BCN0) ) {
            ok = true;
            break;
        }
    }
    if(!ok) {
        //throw TException("THsb::selfTest: BITS_TEST_CNT_BCN0 is not changing");
        LOG4CPLUS_WARN(logger, "THsb::selfTest: BITS_TEST_CNT_BCN0 is not changing - no BCN0?");
    }

    /*	if (HsbSortHalf.readBits(THsbSortHalf::BITS_STATUS_PLL_LOCKED) != 1) {
        throw TException("THsb::selfTest: BITS_STATUS_PLL_LOCKED is not 1");
    }
    else {
 		LOG4CPLUS_INFO(logger, "THsb::selfTest: BITS_STATUS_PLL_LOCKED is 1");
    }*/
}

}
