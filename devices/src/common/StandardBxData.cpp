#include "rpct/devices/StandardBxData.h"

using namespace log4cplus;

namespace rpct {

Logger StandardBxData::logger = Logger::getInstance("StandardBxData");
void StandardBxData::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}

unsigned int StandardBxData::checkAndAddMuon(RPCHardwareMuon* muon) {
	unsigned int status = 0;
	if (muon->getPtCode() != 0 || muon->getSign() != 0 || muon->getQuality() != 0 || 
			muon->getEtaAddr() != 0 || muon->getPhiAddr() != 0 )  
	  	muonVector_.push_back(muon); 
	else {
	    delete muon;
	    status = 0; 
	} 
	return status;
} 

}
