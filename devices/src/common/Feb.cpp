//---------------------------------------------------------------------------
//
//  Michal Pietrusinski 2001
//  Warsaw University
//
#include "rpct/std/NullPointerException.h"
#include "rpct/devices/Feb.h"
#include <math.h>

//---------------------------------------------------------------------------

#include "log4cplus/loggingmacros.h"

using namespace log4cplus;


namespace rpct {


//int Feb::Delay = 100;


Logger Feb::logger = Logger::getInstance("Feb");
void Feb::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}

const float Feb::MAX_VTH_DIFF = 5./1.024;
const float Feb::MAX_VMON_DIFF = 50;

Feb::Feb(DistributionBoard& db, unsigned int ln)
 : distributionBoard(db), localNumber(ln), dacEnabled(false), autoCorrection(true) {
    convertedLocalNumber = convertLocalNumber(localNumber);
    LOG4CPLUS_DEBUG(logger, "localNumber  = " << localNumber);
    LOG4CPLUS_DEBUG(logger, "convertedLocalNumber  = " << convertedLocalNumber);
}

Feb::~Feb() {
}

void Feb::sleep() {
    //tbsleep(Delay);
    //tbsleep(50);
}


TI2CInterface& Feb::getI2C() {
    if (distributionBoard.getI2C() == NULL) {
        throw NullPointerException("Feb::I2C == NULL");
    }
    return *distributionBoard.getI2C();
}


unsigned int Feb::convertLocalNumber(unsigned int localNumber) {
    return localNumber;
}

float Feb::readTemperature() {
    //unsigned int convAddr = convertAddr(addr);
    //return AD7417_RT(addr);
    // Write Address point Reg, for CONF_REG Write &  T Select
    // Write 2 bytes "0x1" and "0x0" to the addres "0x28+nboard"
    // (where 0 <= nboard <= 5))

    //getI2C().Write8(0x28 + addr, 0x1);
    //getI2C().Write8(0x28 + addr, 0x0); // xx
    getI2C().WriteADD (ADDR_AD7417 + convertedLocalNumber, 0x1, 0x0);
    // ADC Channel Selection: Write Address point Reg, for T-READ
    //Write the byte "0x0" to the addres "0x28+nboard"
    getI2C().Write8 (ADDR_AD7417 + convertedLocalNumber, 0x0);


    //Read T (2 Bytes) from "0x28+nboard" and convert into degrees:
    //Word_read ="0xXXXX"    T= ((Word_read & 0xFFC0) >> 6)*0.25;
    unsigned char val1, val2;
    getI2C().ReadADD(ADDR_AD7417 + convertedLocalNumber, val1, val2);

    unsigned int val = val1;
    //val = (val << 8) && 0xff;
    val = (val2 & 0xff) | ((val << 8) & 0xff00);

    return ((val >> 6) & 0x3ff) * 0.25;
}


float Feb::readAD7417_ADC(unsigned int chan) {
    //checkI2C();
    //unsigned int convAddr = convertAddr(addr);
    getI2C().WriteADD(ADDR_AD7417 + convertedLocalNumber, 0x1, chan);
    sleep();
    getI2C().Write8(ADDR_AD7417 + convertedLocalNumber, 0x4);
    sleep();


    unsigned char val1, val2;
    getI2C().ReadADD(ADDR_AD7417 + convertedLocalNumber, val1, val2);
    sleep();

    unsigned int val = val1;
    //val = (val << 8) && 0xff;
    val = (val2 & 0xff) | ((val << 8) & 0xff00);

    return ((val >> 6) & 0x3ff) * 2.5/1.024;
}



void Feb::writeAD5316_DAC(unsigned int wthp, float value) {
    LOG4CPLUS_DEBUG(logger, "writeAD5316_DAC wthp = " << wthp << " value = " << value);
    //selectDAC(true);

    unsigned int ivalue = (unsigned int) (value * 1.024/5);
    unsigned char data[3];
    data[0] = wthp;
    data[1] = ((ivalue & 0x3c0) >> 6) + 0x70; //((svalue >> 6) & 0xf) + 0x70;
    data[2] = (ivalue & 0x3f) << 2; //(svalue << 2) & 0xfc;
    getI2C().Write(0xC, data, 3);
    //EnableDAC(nboard, false);
    //selectDAC(false);
    /*
    wthmsb = ((nvth & 0x3c0) >> 6) + 0x70;
    wthlsb = (nvth & 0x3f) << 2;
    */
}




float Feb::readVTH1() {
    return readAD7417_ADC(CHAN_VTH1);
}

void Feb::writeVTH1(float val) {
	selectDAC(true);
	try {
	    writeAD5316_DAC(WTHP_VTH1, val);
	    if (autoCorrection) {
	    	float rvalue = readVTH1();
	        writeAD5316_DAC(WTHP_VTH1, 2 * val - rvalue);
	    }
    	selectDAC(false);
	}
	catch(...) {
    	selectDAC(false);
    	throw;
	}
}

float Feb::readVTH2() {
    //checkI2C();
    //return AD7417_RADC(addr,1); /* Read VTH2 */
    return readAD7417_ADC(CHAN_VTH2);
}

void Feb::writeVTH2(float val) {
    selectDAC(true);
	try {
	    writeAD5316_DAC(WTHP_VTH2, val);
	    if (autoCorrection) {
	    	float rvalue = readVTH2();
	        writeAD5316_DAC(WTHP_VTH2, 2 * val - rvalue);
	    }
	    selectDAC(false);
	}
	catch(...) {
    	selectDAC(false);
    	throw;
	}
}



float Feb::readVMon1() {
    //checkI2C();
    //return AD7417_RADC(addr,2); /* Read VMON1 */
    return readAD7417_ADC(CHAN_VMON1) * 2;
}

void Feb::writeVMon1(float val) {
    selectDAC(true);
    try {
	    writeAD5316_DAC(WTHP_VMON1, val);
	    if (autoCorrection) {
	    	float rvalue = readVMon1();
	        writeAD5316_DAC(WTHP_VMON1, 2 * val - rvalue);
	    }
	    selectDAC(false);
    }
	catch(...) {
    	selectDAC(false);
    	throw;
	}
}

float Feb::readVMon2() {
    //checkI2C();
    //return AD7417_RADC(addr,3); /* Read VMON2 */
    return readAD7417_ADC(CHAN_VMON2) * 2;
}

void Feb::writeVMon2(float val) {
    selectDAC(true);
    try {
	    writeAD5316_DAC(WTHP_VMON2, val);
	    if (autoCorrection) {
	    	float rvalue = readVMon2();
	        writeAD5316_DAC(WTHP_VMON2, 2 * val - rvalue);
	    }
	    selectDAC(false);
    }
	catch(...) {
    	selectDAC(false);
    	throw;
	}
}



unsigned char Feb::readPCF8574A_CR() {
    //checkI2C();
    //unsigned int convertedLocalNumber = convertAddr(addr);
    return getI2C().Read8(ADDR_PCF8574A_CR + convertedLocalNumber);
}

void Feb::writePCF8574A_CR(unsigned char value) {
    getI2C().Write8(ADDR_PCF8574A_CR + convertedLocalNumber, value);
}

void Feb::enableDAC(bool val) {
    unsigned char value = readPCF8574A_CR();
    if (val) {
        writePCF8574A_CR(value & 0x1);
    }
    else {
        writePCF8574A_CR(value | 0x2);
    }
    dacEnabled = val;
}


void Feb::selectDAC(bool val) {
    unsigned char value = readPCF8574A_CR();
    if (val)
        writePCF8574A_CR(value & 0x2);
    else
        writePCF8574A_CR(value | 0x1);
}


void Feb::write(float* vth1, float* vth2, float* vmon1, float* vmon2) {
    selectDAC(true);
    try {
        if (vth1 != 0) {
            writeAD5316_DAC(WTHP_VTH1, *vth1);
            if (autoCorrection) {
                float rvalue = readVTH1();
                if (fabs(rvalue - *vth1) > MAX_VTH_DIFF) {
                    writeAD5316_DAC(WTHP_VTH1, 2 * *vth1 - rvalue);
                }
            }
        }
        if (vth2 != 0) {
            writeAD5316_DAC(WTHP_VTH2, *vth2);
            if (autoCorrection) {
                float rvalue = readVTH2();
                if (fabs(rvalue - *vth2) > MAX_VTH_DIFF) {
                    writeAD5316_DAC(WTHP_VTH2, 2 * *vth2 - rvalue);
                }
            }
        }
        if (vmon1 != 0) {
            writeAD5316_DAC(WTHP_VMON1, *vmon1);
            if (autoCorrection) {
                float rvalue = readVMon1();
                if (fabs(rvalue - *vmon1) > MAX_VMON_DIFF) {
                    writeAD5316_DAC(WTHP_VMON1, 2 * *vmon1 - rvalue);
                }
            }
        }
        if (vmon2 != 0) {
            writeAD5316_DAC(WTHP_VMON2, *vmon2);
            if (autoCorrection) {
                float rvalue = readVMon2();
                if (fabs(rvalue - *vmon2) > MAX_VMON_DIFF) {
                    writeAD5316_DAC(WTHP_VMON2, 2 * *vmon2 - rvalue);
                }
            }
        }
        selectDAC(false);
    }
    catch(...) {
        selectDAC(false);
        throw;
    }
}

void Feb::writeRead(float* vth1, float* vth2, float* vmon1, float* vmon2) {
	selectDAC(true);
	try {
		if (vth1 != 0) {
		    writeAD5316_DAC(WTHP_VTH1, *vth1);
		    if (autoCorrection) {
		    	float rvalue = readVTH1();
		    	if (fabs(rvalue - *vth1) > MAX_VTH_DIFF) {
		        	writeAD5316_DAC(WTHP_VTH1, 2 * *vth1 - rvalue);
		        	rvalue = readVTH1();
		    	}
		    	*vth1 = rvalue;
		    }
		    else {
		    	*vth1 = readVTH1();
		    }
		}
		if (vth2 != 0) {
		    writeAD5316_DAC(WTHP_VTH2, *vth2);
		    if (autoCorrection) {
		    	float rvalue = readVTH2();
		    	if (fabs(rvalue - *vth2) > MAX_VTH_DIFF) {
		        	writeAD5316_DAC(WTHP_VTH2, 2 * *vth2 - rvalue);
		        	rvalue = readVTH2();
		    	}
		    	*vth2 = rvalue;
		    }
		    else {
		    	*vth2 = readVTH2();
		    }
		}
		if (vmon1 != 0) {
		    writeAD5316_DAC(WTHP_VMON1, *vmon1);
		    if (autoCorrection) {
		    	float rvalue = readVMon1();
		    	if (fabs(rvalue - *vmon1) > MAX_VMON_DIFF) {
		        	writeAD5316_DAC(WTHP_VMON1, 2 * *vmon1 - rvalue);
		        	rvalue = readVMon1();
		    	}
		    	*vmon1 = rvalue;
		    }
		    else {
		    	*vmon1 = readVMon1();
		    }
		}
		if (vmon2 != 0) {
		    writeAD5316_DAC(WTHP_VMON2, *vmon2);
		    if (autoCorrection) {
		    	float rvalue = readVMon2();
		    	if (fabs(rvalue - *vmon2) > MAX_VMON_DIFF) {
		        	writeAD5316_DAC(WTHP_VMON2, 2 * *vmon2 - rvalue);
		        	rvalue = readVMon2();
		    	}
		    	*vmon2 = rvalue;
		    }
		    else {
		    	*vmon2 = readVMon2();
		    }
		}
    	selectDAC(false);
	}
	catch(...) {
    	selectDAC(false);
    	throw;
	}
}

    void Feb::writeReadOffset(float & vth1, float & vth2
                              , float & vtho1, float & vtho2
                              , float & vmon1, float & vmon2
                              , float & vmono1, float & vmono2
                              , const bool & correct_vth, const bool & correct_vmon
                              , const bool & read_vth, const bool & read_vmon)
    {
        selectDAC(true);
        try {
            float rvalue, diff;
            if (vth1 > 0) {
                writeAD5316_DAC(WTHP_VTH1, vth1 + vtho1);
                if (correct_vth) {
                    rvalue = readVTH1();
                    if (fabs(diff = vth1 - rvalue) > MAX_VTH_DIFF) {
                        vtho1 += diff;
                        writeAD5316_DAC(WTHP_VTH1, vth1 + vtho1);
                        if (read_vth)
                            vth1 = readVTH1();
                    }
                    else if (read_vth)
                        vth1 = rvalue;
                }
                else if (read_vth)
                    vth1 = readVTH1();
            }
            if (vth2 > 0) {
                writeAD5316_DAC(WTHP_VTH2, vth2 + vtho2);
                if (correct_vth) {
                    rvalue = readVTH2();
                    if (fabs(diff = vth2 - rvalue) > MAX_VTH_DIFF) {
                        vtho2 += diff;
                        writeAD5316_DAC(WTHP_VTH2, vth2 + vtho2);
                        if (read_vth)
                            vth2 = readVTH2();
                    }
                    else if (read_vth)
                        vth2 = rvalue;
                }
                else if (read_vth)
                    vth2 = readVTH2();
            }
            if (vmon1 > 0) {
                writeAD5316_DAC(WTHP_VMON1, vmon1 + vmono1);
                if (correct_vmon) {
                    rvalue = readVMon1();
                    if (fabs(diff = vmon1 - rvalue) > MAX_VMON_DIFF) {
                        vmono1 += diff;
                        writeAD5316_DAC(WTHP_VMON1, vmon1 + vmono1);
                        if (read_vmon)
                            vmon1 = readVMon1();
                    }
                    else if (read_vmon)
                        vmon1 = rvalue;
                }
                else if (read_vmon)
                    vmon1 = readVMon1();
            }
            if (vmon2 > 0) {
                writeAD5316_DAC(WTHP_VMON2, vmon2 + vmono2);
                if (correct_vmon) {
                    rvalue = readVMon2();
                    if (fabs(diff = vmon2 - rvalue) > MAX_VMON_DIFF) {
                        vmono2 += diff;
                        writeAD5316_DAC(WTHP_VMON2, vmon2 + vmono2);
                        if (read_vmon)
                            vmon2 = readVMon2();
                    }
                    else if (read_vmon)
                        vmon2 = rvalue;
                }
                else if (read_vmon)
                    vmon2 = readVMon2();
            }
            selectDAC(false);
	}
	catch(...) {
            selectDAC(false);
            throw;
	}
    }
} // namespace
