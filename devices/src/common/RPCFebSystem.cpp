#include "rpct/devices/RPCFebSystem.h"

#include "rpct/devices/IFebAccessPoint.h"
#include "rpct/devices/RPCFebAccessPoint.h"

#include "rpct/devices/cb.h"

namespace rpct {

log4cplus::Logger RPCFebSystem::logger_ = log4cplus::Logger::getInstance("RPCFebSystem");
void RPCFebSystem::setupLogger(std::string namePrefix) {
	logger_ = log4cplus::Logger::getInstance(namePrefix + "." + logger_.getName());
}

HardwareItemType * RPCFebSystem::type_ = 0;

RPCFebSystem::RPCFebSystem(LinkSystem & linksystem)
    : FebSystem()
    , linksystem_(linksystem)
{
    if (!type_)
        type_ = new HardwareItemType(HardwareItemType::cBoard
                                     , type::feb::rpcfebsystem_);
}

RPCFebSystem::~RPCFebSystem()
{
    reset();
}
void RPCFebSystem::reset()
{
    FebSystem::reset();
}

IFebAccessPoint & RPCFebSystem::addRPCFebAccessPoint(int cb_id, bool disabled)
    throw (rpct::exception::SystemException)
{
    faps_type::iterator it = faps_.find(cb_id);
    if (it == faps_.end())
        {
            try {
                IBoard & ib = linksystem_.getBoardById(cb_id);
                TCB * tcb = dynamic_cast<TCB *>(&ib);
                if (tcb)
                    return *(faps_.insert(std::pair<int, IFebAccessPoint *>
                                          (cb_id, new RPCFebAccessPoint(*this, *tcb, disabled))
                                          ).first->second);
                else
                    throw rpct::exception::SystemException("the linksystem returned a board of the wrong type");
            } catch(TException & e) {
                throw rpct::exception::SystemException(std::string("could not retrieve controlboard from linksystem, ") + e.what());
            }
        }
    else
        return *(it->second);
}

} // namespace rpct
