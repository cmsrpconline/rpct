#include "rpct/devices/TIIJtagIO.h"

using namespace std;
using namespace log4cplus;

namespace rpct {
Logger TIIJtagIO::logger_ = Logger::getInstance("TIIJtagIO");
void TIIJtagIO::setupLogger(std::string namePrefix) {
	logger_ = Logger::getInstance(namePrefix + "." + logger_.getName());
}

bool TIIJtagIO::IsCaching() {
	return Caching;
}

void TIIJtagIO::SetCaching(bool caching) {
	Caching = caching;
	if (caching) {
		if (cache == 0) {
			cache = new uint32_t[cacheSize];
			tids = new TID[cacheSize];
			for (unsigned int i = 0; i < cacheSize; i++) {
				tids[i] = VECT_JTAG;
			}
		}
		nextCachePos = 0;
	}
}

void TIIJtagIO::Flush() {
	LOG4CPLUS_DEBUG(logger_, __FUNCTION__<<" called. wordsCnt " << std::dec << nextCachePos);
	/*for (size_t i = 0; i < nextCachePos; i++) {
		LOG4CPLUS_DEBUG(logger_, __FUNCTION__<<" data[" << i << "] = " << cache[i]);
	}*/
	Owner.writeVector(tids, cache, nextCachePos);
	nextCachePos = 0;
	//cout << "Flush...done" << endl;
}

bool TIIJtagIO::AddToCache(uint32_t val) {
	bool flush = false;
	if (nextCachePos == cacheSize) {
		Flush();
		flush = true;
	}
	cache[nextCachePos] = val;
	nextCachePos++;
	return flush;
}
	
/*
 bool TIIJtagIO::JtagIO(bool tms, bool tdi, bool read_tdo)
 {
 bool tdo = false;
 uint32_t data = 0;

 Owner.setBitsInVector(BITS_JTAG_TDO, tdi ? 1 : 0, data);
 Owner.setBitsInVector(BITS_JTAG_TMS, tms ? 1 : 0, data);
 if (AutoClock)
 Owner.setBitsInVector(BITS_JTAG_TCK, 1, data);
 Owner.writeVector(VECT_JTAG, data);

 if (read_tdo)
 tdo = Owner.readBits(BITS_JTAG_TDI) == 1;

 if (!AutoClock) {
 Owner.setBitsInVector(BITS_JTAG_TCK, 1, data);
 Owner.writeVector(VECT_JTAG, data);      
 Owner.setBitsInVector(BITS_JTAG_TCK, 0, data);
 Owner.writeVector(VECT_JTAG, data);
 }

 return tdo;
 }   */

void TIIJtagIO::ProcessData(uint32_t data) {
	//cout << "TIIJtagIO::ProcessData data = " << hex << data << endl;
	if (Caching) {
		AddToCache(data);
	}
	else {
		Owner.writeVector(VECT_JTAG, data);		
	}
}

bool TIIJtagIO::JtagIO(bool tms, bool tdo, bool read_tdi) {
	LOG4CPLUS_DEBUG(logger_, __FUNCTION__<<" called. tms "<<tms << ". tdo "<<tdo<< ". read_tdi " <<read_tdi);
	bool tdi = false; //input of the JTAG controller, i.e. output of the chain
	uint32_t data = 0;
	uint32_t one = 1 << Channel;
	
	Owner.setBitsInVector(BITS_JTAG_TDO, tdo ? one : 0, data);
	Owner.setBitsInVector(BITS_JTAG_TMS, tms ? one : 0, data);
	if (AutoClock) {
		Owner.setBitsInVector(BITS_JTAG_TCK, one, data);
	}
	ProcessData(data);
/*	if(dumpFile)
		dumpFile<<"w "<<data<<endl;*/

	if (read_tdi) {
		if (Caching) {
			Flush();
		}
		tdi = Owner.readBits(BITS_JTAG_TDI) & one;
		/*if(dumpFile)
			dumpFile<<"r "<<tdi<<endl;*/
		LOG4CPLUS_DEBUG(logger_, __FUNCTION__<<" called. read tdi "<<tdi);
	}

	if (!AutoClock) {
		Owner.setBitsInVector(BITS_JTAG_TCK, one, data);
		ProcessData(data);
		/*if(dumpFile)
			dumpFile<<"w "<<data<<endl;*/
		Owner.setBitsInVector(BITS_JTAG_TCK, 0, data);
		ProcessData(data);
		/*if(dumpFile)
			dumpFile<<"w "<<data<<endl;*/
	}

	if(dumpFile)
		(*dumpFile)<<tms<<" "<<tdo<<" "<<read_tdi<<" "<<tdi<<endl;

	return tdi;
}

/*
void TIIJtagIO::JtagIO(bool* tms, bool* tdi, int count) {

	uint32_t data = 0;
	uint32_t one = 1 << Channel;
	
	int writeCount = AutoClock ? count : 3 * count;
	TID tids[writeCount];
	unsigned int values[writeCount];
	int writePos = 0;
	
	for (int i = 0; i < count; i++) {
		Owner.setBitsInVector(BITS_JTAG_TDO, tdi ? one : 0, data);
		Owner.setBitsInVector(BITS_JTAG_TMS, tms ? one : 0, data);
		if (AutoClock) {
			Owner.setBitsInVector(BITS_JTAG_TCK, one, data);
		}
		//Owner.writeVector(VECT_JTAG, data);
		tids[writePos] = VECT_JTAG;
		values[writePos] = data;
		writePos++;
	
		if (!AutoClock) {
			Owner.setBitsInVector(BITS_JTAG_TCK, one, data);
			//Owner.writeVector(VECT_JTAG, data);
			tids[writePos] = VECT_JTAG;
			values[writePos] = data;
			writePos++;
			
			
			Owner.setBitsInVector(BITS_JTAG_TCK, 0, data);
			//Owner.writeVector(VECT_JTAG, data);
			tids[writePos] = VECT_JTAG;
			values[writePos] = data;
			writePos++;
		}
	}
}*/

bool TIIJtagIO::JtagIOLoop(bool tms) {
	if (Channel > 0)
		throw TException("JtagIOLoop not implemented for Channel > 0");
	uint32_t data = 0;
	bool tdo = Owner.readBits(BITS_JTAG_TCK) == 1;
	Owner.setBitsInVector(BITS_JTAG_TDO, tdo ? 1 : 0, data);
	Owner.setBitsInVector(BITS_JTAG_TMS, tms ? 1 : 0, data);
	Owner.setBitsInVector(BITS_JTAG_TCK, 1, data);
	Owner.writeVector(VECT_JTAG, data);

	// reading tck gives us not cached value of tdo
	//bool tdo = Owner.readBits(BITS_JTAG_TCK) == 1;
	//return tdo;

	if(dumpFile)
		(*dumpFile)<<"JtagIOLoop"<<endl;

	return Owner.readBits(BITS_JTAG_TCK) == 1;
}



}
