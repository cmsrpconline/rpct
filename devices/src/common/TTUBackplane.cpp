#include "rpct/devices/TTUBackplane.h"
//#include "rpct/devices/TriggerBoard.h"
#include "rpct/devices/FixedHardwareSettings.h"
#include "rpct/devices/TcGbSortBxData.h"
#include "rpct/devices/TcSortSettings.h"
#include "rpct/std/NullPointerException.h"
#include "rpct/ii/ConfigurationFlags.h"
#include "rpct/devices/TtuBackplaneSettings.h"

//---------------------------------------------------------------------------


using namespace std;
using namespace log4cplus;

namespace rpct {

Logger TTTUBackplaneDevice::logger = Logger::getInstance("TTTUBackplaneDevice");
void TTTUBackplaneDevice::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}

//---------------------------------------------------------------------------
#define IID_FILE "RPC_system_def.iid"
#define IID_CLASS TTTUBackplaneDevice
#include "iid2cdef2.h"

#define IID_FILE "RPC_TB3_def.iid"
#define IID_CLASS TTTUBackplaneDevice
#include "iid2cdef2.h"

#define IID_FILE "RPC_TC_SORT_def.iid"
#define IID_CLASS TTTUBackplaneDevice
#include "iid2cdef2.h"

//---------------------------------------------------------------------------


#define IID_FILE "RPC_TB3_vme.iid"
#define IID_CLASS TTTUBackplaneVme
#include "iid2c2.h"

const HardwareItemType TTTUBackplaneVme::TYPE(HardwareItemType::cDevice,
		"TTUBACKPLANEVME");

TTTUBackplaneVme::TTTUBackplaneVme(int vme_board_address, IBoard& board) :
	TTTUBackplaneDevice(board.getId() * 1000, VME_IDENTIFIER.c_str(),
			WORD_IDENTIFIER, "VME", TYPE, VME_VERSION.c_str(), WORD_VERSION,
			WORD_CHECKSUM, 0, board, -1, BuildMemoryMap(), vme_board_address),
			ABootContr(NULL) {
}

TTTUBackplaneVme::~TTTUBackplaneVme() {
	delete ABootContr;
}

//---------------------------------------------------------------------------

#define IID_CLASS TTTUBackplaneFinal
#include "CII_TTU_final_iicfg_tab.cpp"
#undef IID_CLASS

const HardwareItemType TTTUBackplaneFinal::TYPE(HardwareItemType::cDevice,
		"TTTUBackplaneFinal");

TTTUBackplaneFinal::TTTUBackplaneFinal(int id, int vme_board_address,
		int ii_addr, const char* desc, IBoard& board) :
	TTTUBackplaneDevice(id, TTU_FINAL_IDENTIFIER.c_str(), WORD_IDENTIFIER,
			desc, TYPE, TTU_FINAL_VERSION.c_str(), WORD_VERSION, WORD_CHECKSUM,
			ii_addr, board, 0, BuildMemoryMap(), vme_board_address),
			DiagCtrl(0), DiagnosticReadout(0), PulserDiagCtrl(0), Pulser(0),
			I2C(0), TTCrxI2C(7, board.getDescription()),
			qpll_(*this, logger,
					VECT_QPLL,
					BITS_QPLL_MODE,
					BITS_QPLL_CTRL,
					BITS_QPLL_SELECT,
					BITS_QPLL_LOCKED,
					BITS_QPLL_ERROR,
					BITS_QPLL_CLK_LOCK)
{
    monitorItems_.push_back(MonitorItemName::QPLL);
    monitorItems_.push_back(MonitorItemName::TTCRX);
    monitorItems_.push_back(MonitorItemName::RECEIVER);
}

TTTUBackplaneFinal::~TTTUBackplaneFinal() {
	delete DiagCtrl;
	delete DiagnosticReadout;
	delete Pulser;
	delete PulserDiagCtrl;
	delete I2C;
}

/*    writeWord(TTTUBackplaneFinal::WORD_TTC_ID_MODE, 0, 7);
 writeBits(TTTUBackplaneFinal::BITS_TTC_INIT_ENA, 1);
 writeBits(TTTUBackplaneFinal::BITS_TTC_RESET, 1);
 tbsleep(2000);
 writeBits(TTTUBackplaneFinal::BITS_TTC_RESET, 0);
 writeBits(TTTUBackplaneFinal::BITS_TTC_INIT_ENA, 0);
 tbsleep(500);
 if (readBits(TTTUBackplaneFinal::BITS_TTC_READY) != 1) {
 throw TException("TTTUBackplaneFinal::Initialize: BITS_TTC_READY did not go up");
 }

 writeBits(TTTUBackplaneFinal::BITS_QPLL_MODE, 1);
 writeBits(TTTUBackplaneFinal::BITS_QPLL_SELECT, 0x30);
 tbsleep(1000);
 if (readBits(TTTUBackplaneFinal::BITS_QPLL_LOCKED) != 1) {
 throw TException("TTTUBackplaneFinal::Initialize: BITS_QPLL_LOCKED did not go up");
 }
 */

TIII2CMasterCore& TTTUBackplaneFinal::GetI2C() {
	if (I2C == NULL) {

		I2C = new TIII2CMasterCore(*this, AREA_I2C);
		I2C->SetPrescaleValue(100);
		I2C->WriteControlReg(TI2CMasterCore::BIT_EN);
	}

	return *I2C;
}

void TTTUBackplaneFinal::ResetTTC() {
	for(int i = 0; i < 3; i++) {
		int resetTime = 4000 + i * 100;
		LOG4CPLUS_INFO(logger, getFullName() <<" ResetTTC() stated, resetTime = "<<resetTime<<" BITS_STATUS2_TTC_RESET = 1");
		writeBits(BITS_TTC_RESET, 1);
		tbsleep(resetTime);
		if (readBits(BITS_TTC_READY) != 0) {
			throw TException(getFullName() + " ResetTTC: BITS_TTC_READY did not go down after BITS_TTC_RESET = 1");
			//LOG4CPLUS_ERROR(logger, "TTCSortTCSort::ResetTTC: BITS_TTC_READY did not go down after BITS_TTC_RESET, 1");
		}
		writeBits(BITS_TTC_RESET, 0);
		LOG4CPLUS_INFO(logger, getFullName() <<" BITS_STATUS2_TTC_RESET = 0");

	    tbsleep(100);
	    if (readBits(BITS_TTC_READY) != 1) {
	    	LOG4CPLUS_ERROR(logger, getFullName() + " InitTTC: BITS_TTC_READY did not go up, repeating TTCrx reset");
	    }
	    else {
	    	return;
	    }
	}
	throw TException(getFullName() + " InitTTC: BITS_TTC_READY did not go up");
}

void TTTUBackplaneFinal::InitTTC() {
	writeWord(WORD_TTC_ID_MODE, 0, TTCrxI2C.GetAddress());
	writeBits(BITS_TTC_INIT_ENA, 1);
	ResetTTC();
	writeBits(BITS_TTC_INIT_ENA, 0);

	//GetTTCrxI2C().WriteFineDelay1(0);
	//GetTTCrxI2C().WriteFineDelay2(0);
	//GetTTCrxI2C().WriteCoarseDelay(0, 0);
}

void TTTUBackplaneFinal::ResetQPLL() {
	qpll_.reset();
}

bool TTTUBackplaneFinal::checkResetNeeded() {
	return (readBits(BITS_TTC_READY) == 0) || (readBits(BITS_QPLL_LOCKED) == 0);
}

TDiagCtrl& TTTUBackplaneFinal::GetDiagCtrl() {
	if (DiagCtrl == 0) {
		DiagCtrl
				= new TDiagCtrl(*this, VECT_DAQ, BITS_DAQ_PROC_REQ, BITS_DAQ_PROC_ACK, BITS_DAQ_TIMER_LOC_ENA, 0, BITS_DAQ_TIMER_START, BITS_DAQ_TIMER_STOP, BITS_DAQ_TIMER_TRIG_SEL, WORD_DAQ_TIMER_LIMIT, WORD_DAQ_TIMER_COUNT, WORD_DAQ_TIMER_TRG_DELAY, "DAQ_DIAG_CTRL");
	}
	return *DiagCtrl;
}

TStandardDiagnosticReadout& TTTUBackplaneFinal::GetDiagnosticReadout() {
	if (DiagnosticReadout == 0) {
		DiagnosticReadout
				= new TStandardDiagnosticReadout(*this, "READOUT", &TcGbSortBxDataFactory::getInstance(), GetDiagCtrl(), VECT_DAQ, BITS_DAQ_EMPTY, BITS_DAQ_EMPTY_ACK, BITS_DAQ_LOST, BITS_DAQ_LOST_ACK, VECT_DAQ_WR, BITS_DAQ_WR_ADDR, BITS_DAQ_WR_ACK, VECT_DAQ_RD, BITS_DAQ_RD_ADDR, BITS_DAQ_RD_ACK, WORD_DAQ_MASK, WORD_DAQ_DATA_DELAY, AREA_MEM_DAQ_DIAG, DAQ_DIAG_DATA_SIZE, DAQ_DIAG_TRIG_NUM, DAQ_DIAG_TIMER_SIZE, TTC_BCN_EVT_WIDTH);
	}
	return *DiagnosticReadout;
}

TDiagCtrl& TTTUBackplaneFinal::GetPulserDiagCtrl() {
	if (PulserDiagCtrl == NULL) {
		PulserDiagCtrl
				= new TDiagCtrl(*this, VECT_PULSER, BITS_PULSER_PROC_REQ, BITS_PULSER_PROC_ACK, BITS_PULSER_TIMER_LOC_ENA, 0, BITS_PULSER_TIMER_START, BITS_PULSER_TIMER_STOP, BITS_PULSER_TIMER_TRIG_SEL, WORD_PULSER_TIMER_LIMIT, WORD_PULSER_TIMER_COUNT, WORD_PULSER_TIMER_TRG_DELAY, "PULSER_DIAG_CTRL");
	}

	return *PulserDiagCtrl;
}

TPulser& TTTUBackplaneFinal::GetPulser() {
	if (Pulser == NULL) {
		Pulser
				= new TcGbSortPulser(*this, "PULSER", GetPulserDiagCtrl(), AREA_MEM_PULSE, WORD_PULSER_LENGTH, BITS_PULSER_REPEAT_ENA, BITS_PULSER_OUT_ENA);
	}
	return *Pulser;
}
TTTUBackplaneFinal::TDiagCtrlVector& TTTUBackplaneFinal::GetDiagCtrls() {
	if (DiagCtrls.empty()) {
		DiagCtrls.push_back(&GetDiagCtrl());
		DiagCtrls.push_back(&GetPulserDiagCtrl());
	}
	return DiagCtrls;
}

TTTUBackplaneFinal::TDiagVector& TTTUBackplaneFinal::GetDiags() {
	if (Diags.empty()) {
		Diags.push_back(&GetDiagnosticReadout());
		Diags.push_back(&GetPulser());
	}
	return Diags;
}

void TTTUBackplaneFinal::ConfigureDiagnosable(TTriggerDataSel triggerDataSel,
		uint32_t dataTrgDelay) {
	writeBits(BITS_STATUS_TRG_DATA_SEL, triggerDataSel);
	writeWord(WORD_DATA_TRG_DELAY, 0, dataTrgDelay);
}

void TTTUBackplaneFinal::configure(DeviceSettings* settings, int flags) {
	if (settings == 0) {
		/* if (flags & ConfigurationFlags::RESET_ONLY_WHEN_NO_CONFIG_DATA) {
			 return;
			 }*/
		throw NullPointerException(
				"TTTUBackplaneFinal::configure: settings is null");
	}
	//TcSortSettings* s = dynamic_cast<TcSortSettings*>(settings);
	TtuBackplaneSettings* s =
			dynamic_cast<TtuBackplaneSettings*> (settings);
	if (s == 0) {
		throw IllegalArgumentException(
				"TTTUBackplaneFinal::configure: invalid cast for settings");
	}

	bool coldSetup = flags & ConfigurationFlags::FORCE_CONFIGURE;
    if(!coldSetup) {
    	if( (s->getConfigurationId() & 0xfffful) != readWord(WORD_USER_REG1, 0) ) {
    		coldSetup = true;
    	}
    }

    //LOG4CPLUS_INFO(logger, getDescription() << " configure: coldSetup = " << (coldSetup ? "true" : "false"));

	if (coldSetup) {
		reset();
		writeWord(WORD_MASK_TTU, 0, s->getMaskTtu());
		writeWord(WORD_MASK_POINTING, 0, s->getMaskPointing());
		writeWord(WORD_MASK_BLAST, 0, s->getMaskBlast());
		writeWord(WORD_DELAY_TTU, 0, s->getDelayTtu());
		writeWord(WORD_DELAY_POINTING, 0, s->getDelayPointing());
		writeWord(WORD_DELAY_BLAST, 0, s->getDelayBlast());
		writeWord(WORD_SHAPE_TTU, 0, s->getShapeTtu());
		writeWord(WORD_SHAPE_POINTING, 0, s->getShapePointing());
		writeWord(WORD_SHAPE_BLAST, 0, s->getShapeBlast());

		//writeWord( WORD_TRIG_CONFIG,    0, s->getTrigConfig() ); TODO !!
		//AO: CONFIG & CONFIG2 were missing, added may08-10

		vector<int>& trigConfig = s->getTrigConfig();
		unsigned int max_vec_size = trigConfig.size();
		for (vector<int>::size_type i = 0; i < max_vec_size; i++) {
			writeWord(WORD_TRIG_CONFIG, i, trigConfig[i]);
		}

		vector<int>& trigConfig2 = s->getTrigConfig2();
		max_vec_size = trigConfig2.size();
		for (vector<int>::size_type i = 0; i < max_vec_size; i++) {
			writeWord(WORD_TRIG_CONFIG2, i, trigConfig2[i]);
		}

		writeWord(WORD_USER_REG1, 0, s->getConfigurationId() & 0xffff);
	}
}

/*
 boost::dynamic_bitset<>& TTTUBackplaneFinal::getUsedTBs() {
 if (UsedTBs.size() == 0) {
 UsedTBs = boost::dynamic_bitset<>(9, 0ul);
 for(unsigned int iB = 0; iB < getBoard().getCrate()->getBoards().size(); iB++) {
 TriggerBoard* tb = dynamic_cast<TriggerBoard*>(getBoard().getCrate()->getBoards()[iB]);
 if(tb != NULL ) {
 UsedTBs[tb->getNumber()] = true;
 }
 }
 }
 return UsedTBs;
 }*/

void TTTUBackplaneFinal::reset() {
	writeWord(BITS_TTC_TEST_ENA, 0, uint32_t(0));

	/*writeWord(WORD_REC_CHAN_ENA, 0, (getUsedTBs().to_ulong()));

	 writeWord(WORD_REC_TEST_ENA, 0, uint32_t(0));
	 writeWord(WORD_REC_TEST_RND_ENA,0, uint32_t(0));

	 writeWord(WORD_SEND_TEST_ENA, 0, uint32_t(0));
	 writeWord(WORD_SEND_TEST_DATA, 0, uint32_t(0));*/

	writeBits(BITS_PULSER_OUT_ENA, uint32_t(0));

	writeWord(WORD_BCN0_DELAY, 0, FixedHardwareSettings::TCSORT_BC0_DELAY);

	EnableTransmissionCheck(true, true);
	//writeWord(WORD_REC_ERROR_COUNT, 0, uint32_t(0));
}

void TTTUBackplaneFinal::EnableTransmissionCheck(bool enableBCNcheck,
		bool enableDataCheck) {
	/*bool checkDataEna = enableBCNcheck;
	 bool checkEna = enableDataCheck;

	 uint32_t zero = 0;
	 if(checkDataEna) {
	 writeWord(TTTUBackplaneFinal::WORD_REC_CHECK_DATA_ENA, 0, getUsedTBs().to_ulong());//0x1ff
	 writeWord(TTTUBackplaneFinal::WORD_SEND_CHECK_DATA_ENA, 0, 0x3);
	 }
	 else {
	 writeWord(TTTUBackplaneFinal::WORD_REC_CHECK_DATA_ENA, 0, zero);
	 writeWord(TTTUBackplaneFinal::WORD_SEND_CHECK_DATA_ENA, 0, zero);
	 }
	 if(checkEna) {
	 writeWord(TTTUBackplaneFinal::WORD_REC_CHECK_ENA, 0, getUsedTBs().to_ulong());
	 writeWord(TTTUBackplaneFinal::WORD_SEND_CHECK_ENA, 0, 0x3);
	 }
	 else {
	 writeWord(TTTUBackplaneFinal::WORD_REC_CHECK_ENA, 0, zero);
	 writeWord(TTTUBackplaneFinal::WORD_SEND_CHECK_ENA, 0, zero);
	 }*/
}

void TTTUBackplaneFinal::monitor(volatile bool *stop) {
	monitor(monitorItems_);
}

void TTTUBackplaneFinal::monitor(MonitorItems& items) {
    warningStatusList_.clear();
    for (MonitorItems::iterator iItem = items.begin(); iItem != items.end(); ++iItem) {
        if ((*iItem) == MonitorItemName::QPLL) {
            if (readBits(BITS_QPLL_LOCKED) != 1) {
                if (readBits(BITS_QPLL_LOCKED) != 1) {
                    warningStatusList_.push_back(MonitorableStatus(MonitorItemName::QPLL, MonitorableStatus::ERROR, "BITS_QPLL_LOCKED is not up", 0));
                }
                else {
                    warningStatusList_.push_back(MonitorableStatus(MonitorItemName::QPLL, MonitorableStatus::WARNING, "BITS_QPLL_LOCKED is not up", 0));
                }
            }
        } else if ((*iItem) == MonitorItemName::TTCRX) {
            if (readBits(BITS_TTC_READY) != 1) {
                //throw MonitorableException(*this, MON_TTCRX, "BITS_TTC_READY is not up");
                warningStatusList_.push_back(MonitorableStatus(MonitorItemName::TTCRX, MonitorableStatus::ERROR,
                        "BITS_TTC_READY is not up", 0));
            }
        } /*else if ((*iItem) == MonitorItemName::RECEIVER) {
            uint32_t recErrCnt = readWord(WORD_REC_ERROR_COUNT, 0);
            if (monRecErrorAnalyzer_.newValue(recErrCnt) != MonitorableStatus::OK) {
                warningStatusList_.push_back(monRecErrorAnalyzer_.getStatus());
            }
        }*/
    }
}
//---------------------------------------------------------------------------
const HardwareItemType
		TTTUBackplane::TYPE(HardwareItemType::cDevice, "TTUSORT");

TTTUBackplane::TTTUBackplane(int ident, const char* desc, TVMEInterface* vme,
		int vme_board_address, VmeCrate* c, int pos, int ttuSortFinalChipId) :
	BoardBase(ident, desc, TYPE, c, pos), VME(vme), ttuBackplaneVme(
			vme_board_address, *this), ttuBackplaneFinal(ttuSortFinalChipId,
			vme_board_address, 0, "TTU_SORT", *this), VMEAddress(
			vme_board_address) {
	ttuBackplaneVme.getMemoryMap().SetHardwareIfc(
			new TIIVMEAccess(ttuBackplaneVme.GetVMEBoardBaseAddr(), ttuBackplaneVme.getBaseAddress()
					<< ttuBackplaneVme.GetVMEBaseAddrShift(), vme, 0xf8000),
			true);
	ttuBackplaneFinal.getMemoryMap().SetHardwareIfc(
			new TIIVMEAccess(ttuBackplaneFinal.GetVMEBoardBaseAddr(), ttuBackplaneFinal.getBaseAddress()
					<< ttuBackplaneFinal.GetVMEBaseAddrShift(), vme, 0), //0xf8000),
			true);
	devices_.push_back(&ttuBackplaneVme);
	devices_.push_back(&ttuBackplaneFinal);
}

TTTUBackplane::~TTTUBackplane() {
}

void TTTUBackplane::init() {
	ttuBackplaneFinal.InitTTC();
	ttuBackplaneFinal.ResetQPLL();
}

}
