#include "rpct/devices/VmeCrate.h"
#include "rpct/devices/System.h"
#include "rpct/devices/DummyVMEDevice.h"
#include "rpct/ii/DeviceSettings.h"
#include "rpct/std/NullPointerException.h"



using namespace std;
using namespace log4cplus;

namespace rpct {

Logger VmeCrate::logger = Logger::getInstance("VmeCrate");
void VmeCrate::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}

const HardwareItemType VmeCrate::TYPE(HardwareItemType::cCrate, "VMECRATE");

VmeCrate::VmeCrate(int ident, const char* desc, std::string const & _label)
    : Crate(ident, desc, _label),/* id_(ident), description_(desc),*/ vme_(0), boardVector_(21) {
}

void VmeCrate::setDefaultVme() {
    vme_ = TVMEFactory().Create();
    //TVMEEpp* vmeepp = dynamic_cast<TVMEEpp*>(VME);
    //if (vmeepp != NULL) {
    //  vmeepp->SetAutoOptimize(true);
    //}
}

void VmeCrate::setVme(TVMEInterface* vme) {
    vme_ = vme;
}


VmeCrate::~VmeCrate()
{
   // for (Boards::iterator iBoard = boards_.begin(); iBoard != boards_.end(); ++iBoard)
   //     delete *iBoard;
}


void VmeCrate::addBoard(IBoard& board)  {
    LOG4CPLUS_DEBUG(logger, "adding board " << board.getDescription());
    boards_.push_back(&board);
    int pos = board.getPosition();
    if (pos != 0) {
        if (pos < 1 || pos >= (int)boardVector_.size()) {
            throw IllegalArgumentException("Invalid pos " + toString(pos) + " of board " + board.getDescription());
        }

        if (boardVector_[pos] != 0) {
            throw IllegalArgumentException("Invalid pos " + toString(pos) + " of board " + board.getDescription() + ". There is already a board at this position");
        }
        boardVector_[pos] = &board;
    }
}


void VmeCrate::rememberConfiguration(ConfigurationSet* configurationSet) {

}

/*
void VmeCrate::checkVersions() {
    typedef IBoard::Devices Devices;
    bool error = false;
    std::ostringstream ost;

    for (Boards::iterator iBoard = boards_.begin(); iBoard != boards_.end(); ++iBoard) {
        const Devices& devices = (*iBoard)->getDevices();
        for (Devices::const_iterator iDevice = devices.begin(); iDevice != devices.end(); ++iDevice) {
            TIIDevice* iidev = dynamic_cast<TIIDevice*>(*iDevice);
            if (iidev != NULL) {
                try {
                    iidev->checkName();
                    iidev->checkVersion();
                    iidev->checkChecksum();
                }
                catch(TException& e) {
                    error = true;
                    ost << e.what() << "\n";
                }
            }
	  else {
	      TDummyVMEDevice* vmedev = dynamic_cast<TDummyVMEDevice*>(*iDevice);
	      if (vmedev !=  NULL) {//for DCC and CCS
                    try {
		    vmedev->checkName();
                        vmedev->checkVersion();
                        //vmedev->checkChecksum();
		}
		catch(TException& e) {
                        error = true;
                        ost << e.what() << "\n";
                    }
	      }
	  }
        }
    }
    if (error) {
        throw EBadDeviceVersion(ost.str());
    }
}

void VmeCrate::init() {
    Boards& boards = getBoards();
    for (Boards::iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {
        (*iBoard)->init();
    }
}


void VmeCrate::configure(ConfigurationSet* configurationSet, int configureFlags) {

    bool resetOnlyWhenNoConfigData = configureFlags & RESET_ONLY_WHEN_NO_CONFIG_DATA;
    if (configurationSet == 0 && !resetOnlyWhenNoConfigData) {
        throw NullPointerException("VmeCrate::configure: configurationSet is null");
    }

    init();

    for (Boards::iterator iBoard = boards_.begin(); iBoard != boards_.end(); ++iBoard) {
        IBoard* board = *iBoard;
        LOG4CPLUS_INFO(logger, "Configuring board " << board->getDescription());
        board->selfTest();
        const IBoard::Devices& devices = board->getDevices();
        for (IBoard::Devices::const_iterator iDevice = devices.begin(); iDevice != devices.end(); iDevice++) {
            IDevice* device = *iDevice;

            if (configurationSet == 0) {
                device->reset();
                LOG4CPLUS_INFO(logger, "configure: " << device->getDescription()<< " no deviceSettings for chip, chip was reset, but not configured");
            }
            else {
                DeviceSettings* deviceSettings = configurationSet->getDeviceSettings(*device);
                if (resetOnlyWhenNoConfigData) {
                    if(deviceSettings != 0)
                        device->configure(deviceSettings);
                    else {
                        device->reset();
                        LOG4CPLUS_INFO(logger, "configure: " << device->getDescription()<< " no deviceSettings for chip, chip was reset, but not configured");
                    }
                }
                else {
                    device->configure(deviceSettings);
                }
            }
        }
    }
}

*/
} // namespace

