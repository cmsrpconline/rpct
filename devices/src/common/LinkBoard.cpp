//---------------------------------------------------------------------------

//
//  Michal Pietrusinski 2001
//  Warsaw University
//

#include "rpct/devices/LinkBoard.h"
#include "rpct/devices/TIICCUAccess.h"
#include "rpct/devices/TMemoryMapLB.h"
#include "rpct/devices/SynCoderSettings.h"
#include "rpct/devices/LmuxBxData.h"
#include "rpct/devices/LinkBoardBxData.h"
#include "rpct/devices/FixedHardwareSettings.h"
#include "rpct/lboxaccess/lbc_const.h"
#include "rpct/std/IllegalArgumentException.h"
#include "rpct/std/NullPointerException.h"
#include "rpct/ii/ConfigurationFlags.h"


#ifdef CII_LB
# include "rpct/devices/TMemoryMapLBWrapper.h"
#else
# include "rpct/devices/TMemoryMapLB.h"
#endif


using namespace std;
using namespace log4cplus;

namespace rpct {

//---------------------------------------------------------------------------

#ifdef CII_LB
    #define IID_CLASS SynCoder
    #include "CII_LB_STD_iicfg_tab.cpp"
    #undef IID_CLASS
#else
	#define IID_FILE "RPC_LB_def.iid"
	#define IID_CLASS SynCoder
	#include "iid2cdef2.h"
	#define IID_FILE "RPC_system_def.iid"
	#define IID_CLASS SynCoder
	#include "iid2cdef2.h"

	const int SynCoder::II_ADDR_SIZE = SynCoder::LB_II_ADDR_WIDTH;
	const int SynCoder::II_DATA_SIZE = SynCoder::LB_II_DATA_WIDTH;

	#define IID_FILE "RPC_LBSTD_syncoder.iid"
	#define IID_CLASS SynCoder
	#include "iid2c2.h"
#endif

const HardwareItemType SynCoder::TYPE(HardwareItemType::cDevice, "SYNCODER");

Logger SynCoder::logger = Logger::getInstance("SynCoder");
void SynCoder::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}

void SynCoder::LBPulser::writeTarget(int target) {
	target_ = target;
	if (target < 4) {
    	GetOwner().writeBits(SynCoder::BITS_PULSER_LOOP_SEL, target_);
    	//GetOwner().writeBits(SynCoder::BITS_GOL_DATA_SEL, 0);
	}
	else {
		throw TException("LBPulser: unknow pulser target value");
	}
}

/*void SynCoder::LBPulser::Start() {
    GetOwner().writeBits(SynCoder::BITS_PULSER_OUT_ENA, 1);
	writeTarget(target_);
}

void SynCoder::LBPulser::Stop() {
	GetOwner().writeBits(SynCoder::BITS_PULSER_OUT_ENA, 0);
	if(target_ == 2) {
		if (dynamic_cast<LinkBoard&>(GetOwner().getBoard()).getMasterSlaveType() == msMaster)
			GetOwner().writeBits(SynCoder::BITS_GOL_DATA_SEL, 0);
		else
			GetOwner().writeBits(SynCoder::BITS_GOL_DATA_SEL, 5);
	}
}*/

SynCoder::SynCoder(int id, IBoard& board) : TIIDevice(
            id,
            LS_IDENTIFIER.c_str(),
            WORD_IDENTIFIER,
            "STDLB_SYNCODER",
            TYPE,
            LS_VERSION.c_str(),
            WORD_VERSION,
            WORD_CHECKSUM,
            0,
            board,
            0,
#ifdef CII_LB
            new TMemoryMapLBWrapper(BuildMemoryMap())
#else
            new TMemoryMapLB(*auto_ptr<TMemoryMap::TDeclItems>(GetDeclItems()), II_ADDR_SIZE, II_DATA_SIZE)
#endif
        ),
        winHistoMgr(0), fullHistoMgr(0), bxHistoMgr(0), rateHistoMgr(0),
        flashMgr(0), diagnosticReadout(0), pulser(0),
        statisticsDiagCtrl(0), daqDiagCtrl(0), pulserDiagCtrl(0),
        monMuxChkErr0Analyzer_(MonitorItemName::RECEIVER, 1, 1, 1000, 0xffffff), monMuxChkErr1Analyzer_(MonitorItemName::RECEIVER, 1, 1, 1000, 0xffffff)
{
    monMuxChkErr0Analyzer_.setMaxCounterVal((1ull << memoryMap_->GetItemInfo(WORD_SLAVE0_CHK_ERR_COUNT).Width) - 1);
    monMuxChkErr1Analyzer_.setMaxCounterVal((1ull << memoryMap_->GetItemInfo(WORD_SLAVE1_CHK_ERR_COUNT).Width) - 1);

    monitorItems_.push_back(MonitorItemName::CONTROL_BUS);
	monitorItems_.push_back(MonitorItemName::FIRMWARE);
	 monitorItems_.push_back(MonitorItemName::CONFIG_LOST);
    monitorItems_.push_back(MonitorItemName::QPLL);
    monitorItems_.push_back(MonitorItemName::TTCRX);

    //cout <<" __LINE__ " <<dec<< __LINE__ << endl;
    if (dynamic_cast<LinkBoard&>(board).getMasterSlaveType() == msMaster) {
        monitorItems_.push_back(MonitorItemName::GOL);
        monitorItems_.push_back(MonitorItemName::RECEIVER);
    }

    //monitorItems_.push_back(MON_TTCRX_CONF);
}

SynCoder::~SynCoder() {

    delete winHistoMgr;
    delete fullHistoMgr;
    delete rateHistoMgr;

    delete flashMgr;
    delete diagnosticReadout;
    delete pulser;

    delete statisticsDiagCtrl;
    delete daqDiagCtrl;
    delete pulserDiagCtrl;
}

LinkBoard& SynCoder::getLinkBoard() {
	return dynamic_cast<LinkBoard&>(getBoard());
}

void SynCoder::resetTTC() {
	LOG4CPLUS_DEBUG(logger, "ResetTTC");

#ifndef LB_AUTO_INIT
	for(int i = 0; i < 3; i++) {
		int resetTime = 200 + i * 500;
		LOG4CPLUS_INFO(logger, getFullName() <<" SynCoder::ResetTTC() stated, resetTime = "<<resetTime);
		//LOG4CPLUS_INFO(logger, getFullName() <<" BITS_STATUS2_TTC_RESET, 1");
		writeBits(BITS_STATUS2_TTC_RESET, 1);
		tbsleep(resetTime);

		if (readBits(BITS_MONIT_TTC_READY) != 0) {
			throw TException(getFullName() + " SynCoder::ResetTTC: BITS_MONIT_TTC_READY did not go down after BITS_STATUS2_TTC_RESET = 1");
		}
		writeBits(BITS_STATUS2_TTC_RESET, 0);
		//LOG4CPLUS_INFO(logger, getFullName() <<" BITS_STATUS2_TTC_RESET, 0");

		tbsleep(200);
		if (readBits(BITS_MONIT_TTC_READY) != 1) {
			LOG4CPLUS_WARN(logger, "SynCoder::InitTTC: BITS_MONIT_TTC_READY did not go up, repeating TTCrx reset");
		}
		else {
			LOG4CPLUS_INFO(logger, getFullName() + " ResetTTC Done");  //kb
			return;
		}
	}
	throw TException(getFullName() + " SynCoder::ResetTTC: BITS_STATUS2_TTC_READY did not go up");
#else
	writeWord(WORD_BOOT_INIT, 0, (uint32_t)0);
	writeWord(WORD_BOOT_INIT, 0, (uint32_t)0x13);*/
	tbsleep(1000);
	tbsleep(500);
	if (readBits(BITS_MONIT_TTC_READY) != 1) {
		throw TException(getFullName() + " SynCoder::ResetTTC: BITS_STATUS2_TTC_READY did not go up");
	}
	LOG4CPLUS_DEBUG(logger, getFullName() + " SynCoder::ResetTTC():  automatic TTC Reset done");
#endif
}

void SynCoder::resetQPLL() {
  #ifndef LB_AUTO_INIT
    LOG4CPLUS_DEBUG(logger, "ResetQPLL");
    writeBits(BITS_QPLL_MODE, 1);
    writeBits(BITS_QPLL_RESET_ENA, 1);
    tbsleep(5);
    writeBits(BITS_QPLL_RESET_ENA, 0);
  #endif
    for(int i = 1; i < 10; i++) {
    	tbsleep(100);
    	if (readBits(BITS_MONIT_QPLL_LOCKED) == 1) {
    		LOG4CPLUS_INFO(logger, getFullName()<<" ResetQPLL: QPLL_LOCKED after "<<i * 100<<" ms");
    		return;
    	}
    }
    throw TException(getFullName() + " SynCoder::ResetQPLL: BITS_QPLL_LOCKED did not go up");

}

void SynCoder::resetGOL() {
    LOG4CPLUS_DEBUG(logger, getFullName() + " ResetGOL");
    uint32_t gol = 0;
    setBitsInVector(BITS_GOL_RESET_ENA, 1, gol);
    writeVector(VECT_GOL, gol);

    setBitsInVector(BITS_GOL_RESET_ENA, 0, gol);
    setBitsInVector(BITS_GOL_TX_ENA, 1, gol);
    setBitsInVector(BITS_GOL_DATA_SEL, GOL_COND_SEL_LMUX, gol);
    setBitsInVector(BITS_GOL_LASER_ENA, 1, gol);

    writeVector(VECT_GOL, gol);
    //gol = readVector(VECT_GOL); not needed KB
    if (readBits(BITS_MONIT_GOL_READY) == 0) {
        for (int i = 0; true; i++) {
            tbsleep(100);
            if (readBits(BITS_MONIT_GOL_READY) == 0) {
                if (i > 10) {
                    throw TException( getFullName() + " GOL not ready");
                    LOG4CPLUS_ERROR(logger, getFullName() + " GOL not ready !!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                    break;
                }
            }
            else
                break;
        }
    }
    LOG4CPLUS_DEBUG(logger, getFullName() + " ResetGOL Done");
}


void SynCoder::synchGOL() {
    LOG4CPLUS_DEBUG(logger, getFullName() + " SynchGOL");
    writeBits(BITS_GOL_TX_ENA, 0);
    tbsleep(100);
    writeBits(BITS_GOL_TX_ENA, 1);
    LOG4CPLUS_DEBUG(logger,  getFullName() + " SynchGOL Done");
}



TDiagCtrl& SynCoder::getStatisticsDiagCtrl() {
    if (statisticsDiagCtrl == NULL) {
        statisticsDiagCtrl = new TDiagCtrl(*this,
                                           VECT_STAT,
                                           BITS_STAT_PROC_REQ,
                                           BITS_STAT_PROC_ACK,
                                           BITS_STAT_TIMER_LOC_ENA,
                                           0,
                                           BITS_STAT_TIMER_START,
                                           BITS_STAT_TIMER_STOP,
                                           BITS_STAT_TIMER_TRIG_SEL,
                                           WORD_STAT_TIMER_LIMIT,
                                           WORD_STAT_TIMER_COUNT,
                                           WORD_STAT_TIMER_TRG_DELAY,
                                           "STATISTICS_DIAG_CTRL");
    }
    return *statisticsDiagCtrl;
}


TDiagCtrl& SynCoder::getDaqDiagCtrl() {
    if (daqDiagCtrl == NULL) {
        daqDiagCtrl = new TDiagCtrl(*this,
                                    VECT_DAQ,
                                    BITS_DAQ_PROC_REQ,
                                    BITS_DAQ_PROC_ACK,
                                    BITS_DAQ_TIMER_LOC_ENA,
                                    0,
                                    BITS_DAQ_TIMER_START,
                                    BITS_DAQ_TIMER_STOP,
                                    BITS_DAQ_TIMER_TRIG_SEL,
                                    WORD_DAQ_TIMER_LIMIT,
                                    WORD_DAQ_TIMER_COUNT,
                                    WORD_DAQ_TIMER_TRG_DELAY,
                                    "DAQ_DIAG_CTRL");
    }

    return *daqDiagCtrl;
}


TDiagCtrl& SynCoder::getPulserDiagCtrl() {
    if (pulserDiagCtrl == NULL) {
        pulserDiagCtrl = new TDiagCtrl(*this,
                                       VECT_PULSER,
                                       BITS_PULSER_PROC_REQ,
                                       BITS_PULSER_PROC_ACK,
                                       BITS_PULSER_TIMER_LOC_ENA,
                                       0,
                                       BITS_PULSER_TIMER_START,
                                       BITS_PULSER_TIMER_STOP,
                                       BITS_PULSER_TIMER_TRIG_SEL,
                                       WORD_PULSER_TIMER_LIMIT,
                                       WORD_PULSER_TIMER_COUNT,
                                       WORD_PULSER_TIMER_TRG_DELAY,
                                       "PULSER_DIAG_CTRL");
    }

    return *pulserDiagCtrl;
}


IHistoMgr& SynCoder::getWinRateHistoMgr() {
    if (winHistoMgr == NULL) {
        //WinHistoMgr = new THistoMgr(&getWinRateHistoCtrl());
        winHistoMgr = new TIIHistoMgr(
                          *this, "RATE_WIND", getStatisticsDiagCtrl(), AREA_MEM_RATE_WIND);
    }

    return *winHistoMgr;
}

IHistoMgr& SynCoder::getFullRateHistoMgr() {
    if (fullHistoMgr == NULL) {
        //FullHistoMgr = new THistoMgr(&getFullRateHistoCtrl());
        fullHistoMgr = new TIIHistoMgr(
                           *this, "RATE", getStatisticsDiagCtrl(), AREA_MEM_RATE_DIAG);
    }

    return *fullHistoMgr;
}

IHistoMgr& SynCoder::getBXHistoMgr() {
    if (bxHistoMgr == NULL) {
        //BXHistoMgr = new THistoMgr(&getBXHistoCtrl());
        bxHistoMgr = new TIIHistoMgr(
                         *this, "BX_HIST", getStatisticsDiagCtrl(), AREA_MEM_BX_HIST);
    }

    return *bxHistoMgr;
}


SynCoder::LBDiagnosticReadout& SynCoder::getDiagnosticReadout() {
    if (diagnosticReadout == 0) {
        diagnosticReadout = new LBDiagnosticReadout(*this,
                            "READOUT",
                            &LinkBoardBxDataFactory::getInstance(),
                            getDaqDiagCtrl(),
                            VECT_DAQ,
                            BITS_DAQ_EMPTY,
                            BITS_DAQ_EMPTY_ACK,
                            BITS_DAQ_LOST,
                            BITS_DAQ_LOST_ACK,
                            VECT_DAQ_WR,
                            BITS_DAQ_WR_ADDR,
                            BITS_DAQ_WR_ACK,
                            VECT_DAQ_RD,
                            BITS_DAQ_RD_ADDR,
                            BITS_DAQ_RD_ACK,
                            BITS_DAQ_DATA_SEL,
                            BITS_DAQ_EXT_SEL,
                            WORD_DAQ_MASK,
                            WORD_DATA_DAQ_DELAY,
                            AREA_MEM_DAQ_DIAG,
                            DAQ_DIAG_DATA_WIDTH,
                            DAQ_DIAG_TRIG_NUM,
                            DAQ_DIAG_TIMER_SIZE,
                            TTC_BCN_EVT_WIDTH);
    }
    return *diagnosticReadout;
}


TPulser& SynCoder::getPulser() {
    if (pulser == NULL) {
        pulser = new LBPulser(*this,
                             "PULSER",
                             getPulserDiagCtrl(),
                             AREA_MEM_PULSE,
                             WORD_PULSER_LENGTH,
                             BITS_PULSER_REPEAT_ENA,
                             BITS_PULSER_OUT_ENA);
    }
    return *pulser;
}



void SynCoder::setLMuxInEna(bool masterEna, bool slave0Ena, bool slave1Ena) {
    writeBits(SynCoder::BITS_LMUX_MASTER_ENA, masterEna);
    writeBits(SynCoder::BITS_LMUX_SLAVE0_ENA, slave0Ena);
    writeBits(SynCoder::BITS_LMUX_SLAVE1_ENA, slave1Ena);
}

void SynCoder::setLMuxInDelay(int masterDelay, int slave0Delay, int slave1Delay) {
    writeBits(SynCoder::BITS_LMUX_MASTER_DELAY, masterDelay);
    writeBits(SynCoder::BITS_LMUX_SLAVE0_DELAY, slave0Delay);
    writeBits(SynCoder::BITS_LMUX_SLAVE1_DELAY, slave1Delay);
}

void SynCoder::setLMuxInDelay(LinkBoardMasterSlaveType masterSlaveType, int delay) {
    LOG4CPLUS_DEBUG(logger, "setLMuxInDelay masterSlaveType = " << masterSlaveType);
    if (masterSlaveType == msMaster) {
        writeBits(SynCoder::BITS_LMUX_MASTER_DELAY, delay);
    }
    else if (masterSlaveType == msLeft) {
        writeBits(SynCoder::BITS_LMUX_SLAVE0_DELAY, delay);
    }
    else if (masterSlaveType == msRight) {
        writeBits(SynCoder::BITS_LMUX_SLAVE1_DELAY, delay);
    }
    else {
        LOG4CPLUS_ERROR(logger, "setLMuxInDelay masterSlaveType - not proper value " << masterSlaveType);
        throw IllegalArgumentException("LinkBoard::setLMuxInDelay: masterSlaveType - not proper value ");
    }
    LOG4CPLUS_DEBUG(logger, "setLMuxInDelay done");
}

/** @param slaveOrMaster 1 - slave, 0 - master*/
void SynCoder::configureAsSlaveOrMaster() {
    LOG4CPLUS_DEBUG(logger, "configureAsSlaveOrMaster");
	if (dynamic_cast<LinkBoard&>(getBoard()).getMasterSlaveType() == msMaster) {

	    LOG4CPLUS_DEBUG(logger, "configuring as master");

    	writeBits(SynCoder::BITS_STATUS1_SLAVE_ENA, 0); //cannot be one operation, because the BITS_STATUS1_INV_CLOCK is also here!!!!
    	writeBits(SynCoder::BITS_STATUS1_SLAVE_DATA_ENA, 0);

    	writeBits(SynCoder::BITS_LMUX_MASTER_ENA, 1);
    	MasterLinkBoard& master = dynamic_cast<MasterLinkBoard&>(getBoard());
    	for (unsigned int iSLB = 0; iSLB < master.getSlaves().size(); iSLB++) {
    		if (master.getSlaves()[iSLB] != 0) {
    			if (master.getSlaves()[iSLB]->getMasterSlaveType() == msLeft) {
    		        LOG4CPLUS_DEBUG(logger, "configuring left slave");
    				writeBits(SynCoder::BITS_LMUX_SLAVE0_ENA, 1); //TODO LMUX_MASTER_DELAY is also (???)
    				writeBits(SynCoder::BITS_LMUX_SLAVE0_CHECK_ENA, 1);
    				writeWord(WORD_SLAVE0_CHK_ERR_COUNT, 0, uint32_t(0));
    			}
    			else if (master.getSlaves()[iSLB]->getMasterSlaveType() == msRight) {
                    LOG4CPLUS_DEBUG(logger, "configuring right slave");
    				writeBits(SynCoder::BITS_LMUX_SLAVE1_ENA, 1);
    				writeBits(SynCoder::BITS_LMUX_SLAVE1_CHECK_ENA, 1);
    				writeWord(WORD_SLAVE1_CHK_ERR_COUNT, 0, uint32_t(0));
    			}
    			else {
                    LOG4CPLUS_ERROR(logger, "illegal value of MasterSlaveTypes");
    				throw TException("SynCoder::configureAsSlaveOrMaster(): illegal value of MasterSlaveTypes");
    			}
    		}
    	}
//    	writeBits(SynCoder::BITS_SEND_CHECK_DATA_ENA, 1);
//    	writeBits(SynCoder::BITS_SEND_CHECK_ENA, 1);
    	uint32_t sendBits = 0;
    	setBitsInVector(BITS_SEND_CHECK_DATA_ENA, 1, sendBits);
    	setBitsInVector(BITS_SEND_CHECK_ENA, 1, sendBits);
    	writeVector(VECT_SEND, sendBits);
	}
	else {
        LOG4CPLUS_DEBUG(logger, "configuring as slave");
    	writeBits(SynCoder::BITS_STATUS1_SLAVE_ENA, 1);//cannot be one operation, because the BITS_STATUS1_INV_CLOCK is also here!!!!
    	writeBits(SynCoder::BITS_STATUS1_SLAVE_DATA_ENA, 1);

    	//setLMuxInEna(0, 0, 0); does not matter
    	uint32_t golBits = 0;
    	setBitsInVector(SynCoder::BITS_GOL_DATA_SEL, GOL_COND_SEL_MASTER, golBits);
    	writeVector(VECT_GOL, golBits);
	}
    LOG4CPLUS_DEBUG(logger, "configureAsSlaveOrMaster done");
}

void SynCoder::setChannelsEna(const boost::dynamic_bitset<> &channelsEna) {
    LOG4CPLUS_DEBUG(logger, "setChannelsEna " << channelsEna);
    char* chanEnaBits = 0;
    unsigned int channelsNum = getItemDesc(WORD_CHAN_ENA).Width;
    if (channelsEna.size() == channelsNum) {
        chanEnaBits = bitsetToBits(channelsEna);
    }
    else if (channelsEna.size() < channelsNum) {
        boost::dynamic_bitset<> ce = channelsEna; //make a copy as channelsEna is const
        ce.resize(channelsNum, false);
        chanEnaBits = bitsetToBits(ce);
    }
    else  {
        throw IllegalArgumentException("LinkBoard::setChannelsEna: channelsEna size exceeds channels number");
    }
    writeWord(WORD_CHAN_ENA, 0, chanEnaBits);
    delete [] chanEnaBits;
}

SynCoder::TDiagCtrlVector& SynCoder::GetDiagCtrls() {
    if (diagCtrls.empty()) {
        diagCtrls.push_back(&getStatisticsDiagCtrl());
        diagCtrls.push_back(&getDaqDiagCtrl());
        diagCtrls.push_back(&getPulserDiagCtrl());
    }
    return diagCtrls;
}

SynCoder::TDiagVector& SynCoder::GetDiags() {
    if (diags.empty()) {
        diags.push_back(&getWinRateHistoMgr());
        diags.push_back(&getFullRateHistoMgr());
        diags.push_back(&getBXHistoMgr());
        diags.push_back(&getDiagnosticReadout());
        diags.push_back(&getPulser());
    }
    return diags;
}

void SynCoder::ConfigureDiagnosable(TTriggerDataSel triggerDataSel, uint32_t dataTrgDelay) {
    writeBits(BITS_STATUS1_TRG_DATA_SEL, triggerDataSel);
    writeWord(WORD_DATA_TRG_DELAY, 0, dataTrgDelay);
}

void SynCoder::ConfigureDiagnosable(TTriggerDataSel triggerDataSel, uint32_t dataTrgDelay,
        uint32_t triggerAreaEna, bool synFullOutSynchWindow, bool synPulseEdgeSynchWindow) {
    uint32_t status = readVector(VECT_STATUS1);
    setBitsInVector(BITS_STATUS1_TRG_DATA_SEL, triggerDataSel, status);
    setBitsInVector(BITS_STATUS1_TRG_AREA_ENA, triggerAreaEna, status);
    setBitsInVector(BITS_STATUS1_FULL_OUT, synFullOutSynchWindow ? 1 : 0, status);
    setBitsInVector(BITS_STATUS1_EDGE_PULSE, synPulseEdgeSynchWindow ? 1 : 0, status);
    writeVector(VECT_STATUS1, status);
    writeWord(WORD_DATA_TRG_DELAY, 0, dataTrgDelay);
}

bool SynCoder::checkResetNeeded() {
	uint32_t monitorBits = readVector(VECT_MONIT);
	bool resetNeeded = false;
	LinkBoard& lb = (dynamic_cast<LinkBoard&>(getBoard()));
	if (getBitsInVector(BITS_MONIT_QPLL_LOCKED, monitorBits) != 1) {
		LOG4CPLUS_INFO(logger, "checkResetNeeded: BITS_QPLL_LOCKED is not up");
		resetNeeded = true;
	}
	if (getBitsInVector(BITS_MONIT_TTC_READY, monitorBits) != 1) {
		LOG4CPLUS_INFO(logger, "checkResetNeeded: BITS_MONIT_TTC_READY is not up");
		resetNeeded = true;
	}

	try {
		unsigned int controlReg = lb.getTtcRxI2c().ReadControlReg();
		if (controlReg != FixedHardwareSettings::LB_TTC_RX_CONTROL_REG) {
			LOG4CPLUS_INFO(logger, "checkResetNeeded: TTCrx controlReg is "<<rpct::toString(controlReg)<<" (should 0x9b)");
			resetNeeded = true;
		}
	}
	catch(EI2C& e) {
		LOG4CPLUS_WARN(logger, getDescription() << "checkResetNeeded: error accessing TTCrx via I2C. " << e.what());
		resetNeeded = true;
	}

	if( lb.getMasterSlaveType() == msMaster) {
		if (getBitsInVector(BITS_MONIT_GOL_READY, monitorBits) != 1) {
			LOG4CPLUS_INFO(logger, "checkResetNeeded: BITS_GOL_READY is not up");
			resetNeeded = true;
		}
	}

    return resetNeeded;
}

void SynCoder::reset() {
    LOG4CPLUS_DEBUG(logger, "Reset");
    /*getStatisticsDiagCtrl().StopCounting();
    getDaqDiagCtrl().StopCounting();
    getPulserDiagCtrl().StopCounting();
    getStatisticsDiagCtrl().SetCounterEna(false);
    getDaqDiagCtrl().SetCounterEna(false);
    getPulserDiagCtrl().SetCounterEna(false);
    getWinRateHistoMgr().Reset();
    getFullRateHistoMgr().Reset();
    getBXHistoMgr().Reset();
    getDiagnosticReadout().Reset(); */

/*
    writeBits(BITS_STATUS1_EDGE_PULSE, 0);
    writeBits(BITS_STATUS1_FULL_OUT, 0);
    writeBits(BITS_STATUS1_RBC_EXPAND, 0); //RBC puls lenght - + 1 bx = 2 bxs
*/

/*    uint32_t status1 = 0ul;
    writeVector(VECT_STATUS1, status1);*/
    //writeBits(BITS_PULSER_LOOP_SEL, 0); //TODO - maybe should be rather set here
    //writeBits(BITS_PULSER_OUT_ENA, 0);
    uint32_t pulser = 0;
    writeVector(VECT_PULSER, pulser);

    LOG4CPLUS_DEBUG(logger, "Reset Done");
}

/*void SynCoder::configure(DeviceSettings* settings, int flags) {

    bool coldSetup = flags & ConfigurationFlags::COLD_SETUP;

    LOG4CPLUS_DEBUG(logger, "configure: coldSetup = " << (coldSetup ? "true" : "false"));

    //lbx.ccu_read16(0x7000 + nr*0x100 + lbc.LBC_TESTREG)

    LinkBoard& lb = dynamic_cast<LinkBoard&>(getBoard());

    lb.getHalfBox()->getLBoxAccess().memoryWrite16(0x7000 + (getBoard().getPosition() - 1)%10 * 0x100 + LBC::LBC_TESTREG, 0xbeef);

    reset();

    if (settings == 0) {
        if (flags & ConfigurationFlags::RESET_ONLY_WHEN_NO_CONFIG_DATA) {
            return;
        }
        throw NullPointerException("SynCoder::configure: settings == 0");
    }
    SynCoderSettings* scs = dynamic_cast<SynCoderSettings*>(settings);
    if (scs == 0) {
        throw IllegalArgumentException("SynCoder::configure: invalid cast for settings");
    }

    configureAsSlaveOrMaster();


    lb.setWindow(scs->getWindowOpen(), scs->getWindowClose());
    writeBits(BITS_STATUS1_INV_CLOCK, scs->getInvertClock());

    if (lb.getMaster() == 0) {
        throw TException("SynCoder::configure: getMaster() returns 0. Check if master is not disabled in the database");
    }

    lb.getMaster()->getSynCoder().setLMuxInDelay(lb.getMasterSlaveType(), scs->getLMuxInDelay());

    writeWord(WORD_BCN0_DELAY, 0, scs->getBcn0Delay());
    //writeWord(WORD_DATA_TRG_DELAY, 0, scs->getDataTrgDelay()); //DataTrgDelay used for storing the hits timing used in that setting
    writeWord(WORD_DATA_TRG_DELAY, 0, 0ul);

    //writeBits(BITS_STATUS1_TRG_DATA_SEL, 0); //trigger for data for counters and readout

    writeWord(WORD_DATA_WIN_DELAY, 0, 0ul);
    writeWord(WORD_DATA_DIAG_DELAY, 0, 0ul);
    //writeWord(WORD_DATA_DAQ_DELAY, 0, scs->getDataDaqDelay()); //DataDaqDelay is used for storing SEND_DELAY
    writeWord(WORD_SEND_DELAY, 0, scs->getDataDaqDelay());

    writeWord(WORD_PULSER_TIMER_TRG_DELAY, 0, scs->getPulserTimerTrgDelay()); ////scs->bcn0Delay<<<<<<<<<<<<<<<<<<<<<<<<<<<<<!!!!

    writeBits(BITS_STATUS2_CSC_RBC_DELAY, scs->getRbcDelay()); //RBC puls delay

    //writeBits(BITS_STATUS2_RESET_DET, 0);
    readBits(BITS_MONIT_RESET_DET);

    if (scs->getInChannelsEna().size() > 0) {
        setChannelsEna(scs->getInChannelsEna());
    }

    if(lb.getDescription().find("LB_RE") != string::npos) {
         writeBits(BITS_STATUS2_CSC_OR_SEL, 3);  //or = 2^(CSC_OR_SEL)
         writeBits(BITS_STATUS2_CSC_SHIFT, 0ul);
         writeBits(BITS_STATUS2_CSC_ENA, 1);
    }
    else if(lb.getDescription().find("LB_RB") != string::npos) {
         writeBits(BITS_STATUS2_CSC_ENA, 0ul);
    }
    else {
        LOG4CPLUS_ERROR(logger, "LB getDescription() not contains LB_RE or LB_RB !!!!!!!!!!!!!!");
    }
}*/



void SynCoder::configure(DeviceSettings* settings, int flags) {
    bool coldSetup = flags & ConfigurationFlags::FORCE_CONFIGURE;

    LinkBoard& lb = dynamic_cast<LinkBoard&>(getBoard());

    lb.memoryWrite16(0x7000 + (getBoard().getPosition() - 1)%10 * 0x100 + LBC::LBC_TESTREG, 0xbeef);

    uint32_t pulserBits = 0;
    writeVector(VECT_PULSER, pulserBits); //to reset the pulser, if it was used before and xdaq crashed not reseting the pulser

    if (settings == 0) {
        throw NullPointerException("SynCoder::configure: settings == 0");
    }
    SynCoderSettings* scs = dynamic_cast<SynCoderSettings*>(settings);
    if (scs == 0) {
        throw IllegalArgumentException("SynCoder::configure: invalid cast for settings");
    }

    if(!coldSetup) {
    	if( uint32_t(scs->getConfigurationId() & 0xffff) != readWord(WORD_USER_REG1, 0) ) {
    		coldSetup = true;
    	}
    }

    LOG4CPLUS_INFO(logger, getDescription() << " configure: coldSetup = " << (coldSetup ? "true" : "false"));

    if (lb.getMaster() == 0) {
        throw TException("SynCoder::configure: getMaster() returns 0. Check if master is not disabled in the database");
    }

    if(coldSetup) {
        lb.setWindow(scs->getWindowOpen(), scs->getWindowClose());

    	uint32_t status1Bits = 0;
    	//setBitsInVector(BITS_STATUS1_FULL_OUT, 0, status1Bits);
    	//setBitsInVector(BITS_STATUS1_EDGE_PULSE, 0, status1Bits);
    	setBitsInVector(BITS_STATUS1_INV_CLOCK, scs->getInvertClock(), status1Bits);
    	//setBitsInVector(BITS_STATUS1_TRG_DATA_SEL, 0, status1Bits);
    	//setBitsInVector(BITS_STATUS1_TRG_AREA_ENA, 0, status1Bits);
    	if(lb.getMasterSlaveType() == msMaster) {
    		//setBitsInVector(BITS_STATUS1_SLAVE_ENA, 0, status1Bits);
    		//setBitsInVector(BITS_STATUS1_SLAVE_DATA_ENA, 0, status1Bits);
    	}
    	else {
    		setBitsInVector(BITS_STATUS1_SLAVE_ENA, 1, status1Bits);
    		setBitsInVector(BITS_STATUS1_SLAVE_DATA_ENA, 1, status1Bits);
    	}
    	setBitsInVector(BITS_STATUS1_RBC_EXPAND, 0, status1Bits);
    	writeVector(VECT_STATUS1, status1Bits);

    	uint32_t status2Bits = 0;
    	if(lb.getDescription().find("LB_RE") != string::npos) {
    		setBitsInVector(BITS_STATUS2_CSC_ENA, 1, status2Bits);
    		setBitsInVector(BITS_STATUS2_CSC_OR_SEL, 3, status2Bits);  //or = 2^(CSC_OR_SEL)
    		setBitsInVector(BITS_STATUS2_CSC_SHIFT, 0, status2Bits);
    	}
    	else if(lb.getDescription().find("LB_RB") != string::npos) {
    		setBitsInVector(BITS_STATUS2_CSC_ENA, 0, status2Bits);
    	}
    	else {
    		LOG4CPLUS_ERROR(logger, "LB getDescription() not contains LB_RE or LB_RB !!!!!!!!!!!!!!");
    	}
    	setBitsInVector(BITS_STATUS2_CSC_RBC_DELAY, scs->getRbcDelay(), status2Bits); //RBC puls delay
    	writeVector(VECT_STATUS2, status2Bits);
    }

    //this must be done always, because the enabled/disabled slaves (this is changing regardless of the SynCoderSettings)
    uint32_t lmuxBits = 0;
    if(lb.getMasterSlaveType() == msMaster) {
    	setBitsInVector(BITS_LMUX_MASTER_ENA, 1, lmuxBits);
    	setBitsInVector(SynCoder::BITS_LMUX_MASTER_DELAY, scs->getLMuxInDelay(), lmuxBits);
    	MasterLinkBoard& master = dynamic_cast<MasterLinkBoard&>(getBoard());
    	for (unsigned int iSLB = 0; iSLB < master.getSlaves().size(); iSLB++) {
    		if (master.getSlaves()[iSLB] != 0) {
    			if (master.getSlaves()[iSLB]->getMasterSlaveType() == msLeft) {
    				LOG4CPLUS_DEBUG(logger, "configuring left slave");
    				setBitsInVector(SynCoder::BITS_LMUX_SLAVE0_ENA, 1, lmuxBits);
    				setBitsInVector(SynCoder::BITS_LMUX_SLAVE0_CHECK_ENA, 1, lmuxBits);
    				setBitsInVector(SynCoder::BITS_LMUX_SLAVE0_DELAY, master.getSlaveODelay(), lmuxBits);
    				writeWord(WORD_SLAVE0_CHK_ERR_COUNT, 0, uint32_t(0)); //TODO maybe not needed here
    			}
    			else if (master.getSlaves()[iSLB]->getMasterSlaveType() == msRight) {
    				LOG4CPLUS_DEBUG(logger, "configuring right slave");
    				setBitsInVector(SynCoder::BITS_LMUX_SLAVE1_ENA, 1, lmuxBits);
    				setBitsInVector(SynCoder::BITS_LMUX_SLAVE1_CHECK_ENA, 1, lmuxBits);
    				setBitsInVector(SynCoder::BITS_LMUX_SLAVE1_DELAY, master.getSlave1Delay(), lmuxBits);
    				writeWord(WORD_SLAVE1_CHK_ERR_COUNT, 0, uint32_t(0)); //TODO maybe not needed here
    			}
    			else {
    				LOG4CPLUS_ERROR(logger, "illegal value of MasterSlaveTypes");
    				throw TException("SynCoder::configure(): illegal value of MasterSlaveTypes");
    			}
    		}
    	}
    }
    writeVector(VECT_LMUX, lmuxBits); //0 in case of the slave

    if(coldSetup) {
    	if(lb.getMasterSlaveType() == msMaster) {
    		uint32_t sendBits = 0;
    		setBitsInVector(BITS_SEND_CHECK_DATA_ENA, 1, sendBits);
    		setBitsInVector(BITS_SEND_CHECK_ENA, 1, sendBits);
    		writeVector(VECT_SEND, sendBits);
    	}
    	else {
    		uint32_t golBits = 0;
    		setBitsInVector(SynCoder::BITS_GOL_DATA_SEL, GOL_COND_SEL_MASTER, golBits);
    		writeVector(VECT_GOL, golBits);
    	}

    	writeWord(WORD_BCN0_DELAY, 0, scs->getBcn0Delay());
    	writeWord(WORD_SEND_DELAY, 0, scs->getDataDaqDelay());

    	setChannelsEna(scs->getInChannelsEna());


    	writeWord(WORD_USER_REG1, 0, scs->getConfigurationId() & 0xffff);
    	writeWord(WORD_SEND_TEST_DATA, 0, CONTROL_BUS_CHECK_VALUE);
    }
    //writeBits(BITS_STATUS2_RESET_DET, 0);
    //readBits(BITS_MONIT_RESET_DET); //TODO read all MONIT bits and check
}



void SynCoder::enable() {
	//cannot be done in constructor, as the whole LBOX must be build already
    if (dynamic_cast<LinkBoard&>(getBoard()).getMasterSlaveType() == msMaster) {
        writeWord(WORD_SLAVE0_CHK_ERR_COUNT, 0, uint32_t(0));
        writeWord(WORD_SLAVE1_CHK_ERR_COUNT, 0, uint32_t(0));
        monMuxChkErr0Analyzer_.reset();
        monMuxChkErr1Analyzer_.reset();
    }
    monitoringIteration_ = 0;
}

void SynCoder::monitor(volatile bool *stop) {
    monitor(monitorItems_);
}

const uint32_t SynCoder::CONTROL_BUS_CHECK_VALUE = 0x5555aaaa;

void SynCoder::monitor(MonitorItems& items) {
    monitoringIteration_++;
/*    if(monitoringIteration_%2 == 0) //pre-scaling
        return;*/ //cannot have a prescale here, because the histograming rely on the SEU errors detection here

    warningStatusList_.clear();

    LinkBoard& lb = dynamic_cast<LinkBoard&>(getBoard());

    uint32_t monitorBits = readVector(VECT_MONIT);

    for (MonitorItems::iterator iItem = items.begin(); iItem != items.end(); ++iItem) {
    	if ((*iItem) == MonitorItemName::CONTROL_BUS) {
    		TID testReg = WORD_SEND_TEST_DATA;
    		uint32_t regValue = readWord(testReg, 0);

    		std::ostringstream ostr;
    		uint64_t one = 1;
    		if(regValue == ((one << memoryMap_->GetItemInfo(testReg).Width) -1) ) {
    			ostr << std::hex <<": test register value is " << regValue<<" firmware was most probably lost; ";
    			warningStatusList_.push_back(MonitorableStatus(MonitorItemName::FIRMWARE, MonitorableStatus::ERROR, ostr.str(), 0));
    		}
    		else if (regValue != CONTROL_BUS_CHECK_VALUE) {
    			uint32_t diff = regValue ^ CONTROL_BUS_CHECK_VALUE;
    			boost::dynamic_bitset<> diffBitset(32, diff);

    			ostr<<std::hex <<": control bus not working correctly: test register expected value: "<<std::hex <<CONTROL_BUS_CHECK_VALUE<<" read: "<<regValue
    					<<" flip bits count: "<<std::dec<<diffBitset.count()<<"; ";

    			warningStatusList_.push_back(MonitorableStatus(MonitorItemName::CONTROL_BUS, MonitorableStatus::WARNING, ostr.str(), regValue));
    		}
    	}
    	else if ((*iItem) == MonitorItemName::FIRMWARE) {
            string msg = monitorVersion();
            if (msg.size() != 0) {
            	if(warningStatusList_.size() == 1) {//was MonitorItemName::CONTROL_BUS error before
            		warningStatusList_.back().message += msg;
            	}
            	else {
            		warningStatusList_.push_back(MonitorableStatus(MonitorItemName::FIRMWARE, MonitorableStatus::ERROR, msg, 0));
            	}
            }
        }
        else if ((*iItem) == MonitorItemName::CONFIG_LOST) {
        	string message;
        	if(readWord(WORD_USER_REG1, 0) == 0) {
        		message = "Syncoder USER_REG1 is 0";
        	}

        	/* disabled, because it triggers false alarm when reloading from flash
        	 * maciekz 20120301*/
        	/*
                    unsigned int val = lb.memoryRead16(0x7000 + (getBoard().getPosition() -1)%10 * 0x100 + LBC::LBC_TESTREG);
                    if (val != 0xbeef) {
                    	message = "LBC_TESTREG is not 0xbeef, but: " + toHexString(val) + ". ";
                    }
        	 */

        	if (getBitsInVector(BITS_MONIT_RESET_DET, monitorBits) == 1) {
        		message = message + " SynCoder reset was detected";
        	}

        	if(message.size() != 0) {
        		if(warningStatusList_.size() == 1) {//was MonitorItemName::CONTROL_BUS error before
        			warningStatusList_.back().message += message;
        		}
        		else {
        			warningStatusList_.push_back(MonitorableStatus(MonitorItemName::CONFIG_LOST, MonitorableStatus::ERROR, message, 0));
        		}
        	}
        }
        else if ((*iItem) == MonitorItemName::QPLL) {
            if (getBitsInVector(BITS_MONIT_QPLL_LOCKED, monitorBits) != 1) {
            	usleep(500000);
            	uint32_t monitorBits1 = readVector(VECT_MONIT);
            	if (getBitsInVector(BITS_MONIT_QPLL_LOCKED, monitorBits1) != 1) {
            		warningStatusList_.push_back(MonitorableStatus(MonitorItemName::QPLL, MonitorableStatus::ERROR, "BITS_QPLL_LOCKED is not up", 0)); //, MonErrorType::PERSISTENT
            	}
            	else {
            		warningStatusList_.push_back(MonitorableStatus(MonitorItemName::QPLL, MonitorableStatus::WARNING, "BITS_QPLL_LOCKED was not up for a moment", 0)); //, MonErrorType::TRANSIENT
            	}
            }
        }
        else if ((*iItem) == MonitorItemName::TTCRX) {
            if (getBitsInVector(BITS_MONIT_TTC_READY, monitorBits) != 1) {
                warningStatusList_.push_back(MonitorableStatus(MonitorItemName::TTCRX, MonitorableStatus::ERROR, "BITS_MONIT_TTC_READY is not up", 0));
            }

            //if(monitoringIteration_%5 == 0) //to not block the CCU channel to much
            {
            	unsigned int controlReg = 0;
            	/*try { lb.initI2C() moved to HalfBox::prepareAccess()
            		controlReg = lb.getTtcRxI2c().ReadControlReg();
            		if (controlReg != FixedHardwareSettings::LB_TTC_RX_CONTROL_REG)
            			LOG4CPLUS_WARN(logger, "readout TTC_RX_CONTROL_REG is "<<controlReg);
            	}
            	catch(EI2C& e) {
            		LOG4CPLUS_WARN(logger, "I2C error while reading the TTCrx control register");
            		controlReg = 0;
            	}
            	if (controlReg != FixedHardwareSettings::LB_TTC_RX_CONTROL_REG) {
            		LOG4CPLUS_WARN(logger, "trying lb.initI2C()");
            		lb.initI2C();
            		try {
            			controlReg = lb.getTtcRxI2c().ReadControlReg();
            			if (controlReg != FixedHardwareSettings::LB_TTC_RX_CONTROL_REG) {
            				warningStatusList_.push_back(MonitorableStatus(MonitorItemName::TTCRX, MonitorableStatus::ERROR, "TTCrx controlReg is " + rpct::toHexString(controlReg) + " (should 0x9b)", 0)); //TODO  no tgood slolution
            			}
            		}
            		catch(EI2C& e) {
            			warningStatusList_.push_back(MonitorableStatus(MonitorItemName::TTCRX, MonitorableStatus::ERROR, "I2C error while reading the TTCrx control register", 0));
            		}
            	}
*/
            	try {
            		controlReg = lb.getTtcRxI2c().ReadControlReg();
            		if (controlReg != FixedHardwareSettings::LB_TTC_RX_CONTROL_REG) {
            			LOG4CPLUS_WARN(logger, "readout TTC_RX_CONTROL_REG is "<<controlReg);
            			warningStatusList_.push_back(MonitorableStatus(MonitorItemName::TTCRX, MonitorableStatus::ERROR, "TTCrx controlReg is " + rpct::toHexString(controlReg) + " (should 0x9b)", 0));
            		}
            	}
            	catch(EI2C& e) {
            		LOG4CPLUS_WARN(logger, "I2C error while reading the TTCrx control register");
            		controlReg = 0;
            	}
            }
        }
        else if ((*iItem) == MonitorItemName::GOL) { //jesli LB nie jest master, MON_GOL nie jest dodawany do listy items
        	if (getBitsInVector(BITS_MONIT_GOL_READY, monitorBits) != 1) {
                warningStatusList_.push_back(MonitorableStatus(MonitorItemName::GOL, MonitorableStatus::ERROR, "BITS_GOL_READY is not up", 0));
        	}
        }
        else if ((*iItem) == MonitorItemName::RECEIVER) {
        	if( (dynamic_cast<MasterLinkBoard&>(lb)).getLeftSlave() != 0) {
        		//if (getBitsInVector(BITS_MONIT_SLAVE0_ERR_TEST, monitorBits) != 0) //looks that it is always 1, see savanah
        		{
        			uint32_t errCnt = readWord(WORD_SLAVE0_CHK_ERR_COUNT, 0);
        			if (monMuxChkErr0Analyzer_.newValue(errCnt) != MonitorableStatus::OK) {
        				warningStatusList_.push_back(monMuxChkErr0Analyzer_.getStatus());
        			}
        			if(errCnt == monMuxChkErr0Analyzer_.getMaxCounterVal()) {
        				writeWord(WORD_SLAVE0_CHK_ERR_COUNT, 0, uint32_t(0));
        			}
        			if(errCnt != 0)
        				LOG4CPLUS_WARN(logger, getDescription()<<" WORD_SLAVE0_CHK_ERR_COUNT = "<<errCnt);
        		}
        	}
        	if ( (dynamic_cast<MasterLinkBoard&>(lb)).getRightSlave() != 0 ) {
        		//if (getBitsInVector(BITS_MONIT_SLAVE1_ERR_TEST, monitorBits) != 0)
        		{
        			uint32_t errCnt = readWord(WORD_SLAVE1_CHK_ERR_COUNT, 0);
        			if (monMuxChkErr1Analyzer_.newValue(errCnt) != MonitorableStatus::OK) {
        				warningStatusList_.push_back(monMuxChkErr1Analyzer_.getStatus());
        			}
        			if(errCnt == monMuxChkErr1Analyzer_.getMaxCounterVal()) {
        				writeWord(WORD_SLAVE1_CHK_ERR_COUNT, 0, uint32_t(0));
        			}
        			if(errCnt != 0)
        				LOG4CPLUS_WARN(logger, getDescription()<<" WORD_SLAVE1_CHK_ERR_COUNT = "<<errCnt);
        		}
        	}
        }
    }
    if(checkWarningExist(MonitorItemName::CONTROL_BUS)) {
    	LOG4CPLUS_INFO(logger, "CONTROL_BUS error detected, downgrading other warnings to the SOFT_WARNING");
    	for (Monitorable::MonitorStatusList::iterator iStat = getWarningStatusList().begin();
    			iStat != getWarningStatusList().end(); ++iStat) {
    		if(iStat->monitorItem != MonitorItemName::CONTROL_BUS) {
    			iStat->level = MonitorableStatus::SOFT_WARNING;
    		}
    	}
    }
}



/////////////////////////////////////////////////////////////////////////////////////



Logger LinkBoard::logger = Logger::getInstance("LinkBoard");
void LinkBoard::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}
const HardwareItemType LinkBoard::TYPE(HardwareItemType::cBoard, "LINKBOARD");

LinkBoard::LinkBoard(int ident, const char* desc, LinkBox* lbox, int bn, LinkBoardMasterSlaveType mst,
    int syncoderId)
        : BoardBase(ident, desc, TYPE, lbox, bn), linkBox(lbox), halfBox(0), masterSlaveType(mst),
synCoder(syncoderId, *this), i2c(0), ttcRxI2c(7, getDescription()) {
    LOG4CPLUS_DEBUG(logger, "LinkBoard::LinkBoard: 1");
    halfBox = linkBox->getHalfBox(getPosition());
    LOG4CPLUS_DEBUG(logger, "LinkBoard::LinkBoard: 2");
    if (halfBox == 0) {
        throw TException("LinkBoard::LinkBoard: No halfBox found for linkBoard " + string(desc));
    }
    ////int mod = boardNum % 3;
    //masterSlaveType = (mod == 0) ? msRight : (mod == 1) ? msMaster : msLeft;
    synCoder.getMemoryMap().SetHardwareIfc(
        new TIILBCCUAccess(&halfBox->getLBoxAccess(), *this),
        true);
    devices_.push_back(&synCoder);
}

LinkBoard::~LinkBoard() {
    delete i2c;
}

unsigned short LinkBoard::memoryRead(uint32_t address) {
    try {
        return halfBox->getLBoxAccess().memoryRead(address);
    }
    catch (CCUException& e) {
        // try once again
        LOG4CPLUS_WARN(logger, "CCUException during memoryRead from address " << address << " reseting FEC and trying once again");
        halfBox->getLBoxAccess().getFecManager().resetPlxFecForAutoRecovery();
        return halfBox->getLBoxAccess().memoryRead(address);
    }
}

void LinkBoard::memoryWrite(uint32_t address, unsigned short data) {
    try {
    	halfBox->getLBoxAccess().memoryWrite(address, data);
    }
    catch (CCUException& e) {
        // try once again
        LOG4CPLUS_WARN(logger, "CCUException during memoryWrite from address " << address << " reseting FEC and trying once again");
        halfBox->getLBoxAccess().getFecManager().resetPlxFecForAutoRecovery();
        halfBox->getLBoxAccess().memoryWrite(address, data);
    }
}

unsigned short LinkBoard::memoryRead16(uint32_t address) {
    try {
        return halfBox->getLBoxAccess().memoryRead16(address);
    }
    catch (CCUException& e) {
        // try once again
        LOG4CPLUS_WARN(logger, "CCUException during memoryRead16 from address " << address << " reseting FEC and trying once again");
        halfBox->getLBoxAccess().getFecManager().resetPlxFecForAutoRecovery();
        return halfBox->getLBoxAccess().memoryRead16(address);
    }
}

void LinkBoard::memoryWrite16(uint32_t address, unsigned short data) {
    try {
    	halfBox->getLBoxAccess().memoryWrite16(address, data);
    }
    catch (CCUException& e) {
        // try once again
        LOG4CPLUS_WARN(logger, "CCUException during memoryWrite16 from address " << address << " reseting FEC and trying once again");
        halfBox->getLBoxAccess().getFecManager().resetPlxFecForAutoRecovery();
        halfBox->getLBoxAccess().memoryWrite16(address, data);
    }
}


TRPCCCUI2CMasterCore& LinkBoard::getI2C() {
    if (i2c == NULL) {
    	LOG4CPLUS_DEBUG(logger, "creating i2c");

        if (linkBox == NULL) {
            throw TException("LinkBoard::getI2C: linkBox == NULL");
        }


        // enable I2C on control board
        int halfBoxPos = (getPosition() % 10) - 1;
/*        uint32_t address = 0x7000 + LBC::LBC_INTERF + 0x100 * halfBoxPos;
        unsigned int value = memoryRead16(address);
        memoryWrite16(address, (1 << LBC::INT_I2C) | value);*/

        i2c = new TRPCCCUI2CMasterCore(
                  halfBox->getLBoxAccess(),
                  0x7000 + LBC::LBC_I2C + 0 + 0x100 * halfBoxPos,
                  0x7000 + LBC::LBC_I2C + 1 + 0x100 * halfBoxPos,
                  0x7000 + LBC::LBC_I2C + 2 + 0x100 * halfBoxPos,
                  0x7000 + LBC::LBC_I2C + 3 + 0x100 * halfBoxPos,
                  0x7000 + LBC::LBC_I2C + 4 + 0x100 * halfBoxPos,
                  false);
/*        i2c->SetPrescaleValue(128);
        i2c->WriteControlReg(TI2CMasterCore::BIT_EN);*/

    	//LOG4CPLUS_DEBUG(logger, "creating i2c done");
    }

    return *i2c;
}

void LinkBoard::initI2C() {
    getI2C();

    // enable I2C on control board
    int halfBoxPos = (getPosition() % 10) - 1;
    uint32_t address = 0x7000 + LBC::LBC_INTERF + 0x100 * halfBoxPos;
    unsigned int value = memoryRead16(address);
    memoryWrite16(address, (1 << LBC::INT_I2C) | value);

    i2c->SetPrescaleValue(128);
    i2c->WriteControlReg(TI2CMasterCore::BIT_EN);
}

void LinkBoard::setTtcRxI2cAddress(unsigned char addr) {
    ttcRxI2c.SetAddress(addr);
    synCoder.writeWord(SynCoder::WORD_TTC_ID_MODE, 0, addr);

}

void LinkBoard::setTtcRxI2cAddress() {
	setTtcRxI2cAddress(ttcRxI2c.GetAddress());
}

void LinkBoard::intiTTCrx() {
	LOG4CPLUS_INFO(logger, getDescription()<<" intiTTCrx started");
	setTtcRxI2cAddress(ttcRxI2c.GetAddress());

    synCoder.writeBits(SynCoder::BITS_STATUS2_TTC_INIT_ENA, 1);
    synCoder.resetTTC();
    synCoder.writeBits(SynCoder::BITS_STATUS2_TTC_INIT_ENA, 0); //TODO according to KTP this it not needed

    getTtcRxI2c().WriteControlReg(FixedHardwareSettings::LB_TTC_RX_CONTROL_REG);

    unsigned int controlReg = getTtcRxI2c().ReadControlReg();
    if (controlReg != FixedHardwareSettings::LB_TTC_RX_CONTROL_REG) {
        throw TException(getDescription() + " SynCoder::intiTTCrx: TTCrx controlReg is " + rpct::toString(controlReg) + " (should 0x9b)");
    }
    LOG4CPLUS_INFO(logger, getDescription()<<" intiTTCrx finished");
}

void LinkBoard::intiTTCrxStep1() {
	setTtcRxI2cAddress();
	getSynCoder().writeBits(SynCoder::BITS_STATUS2_TTC_INIT_ENA, 1); //TODO maybe it can be done in one operation
	getSynCoder().writeBits(SynCoder::BITS_STATUS2_TTC_RESET, 1); //but it is possiblem that the TTC_INIT_ENA should be set before the reset
}

void LinkBoard::intiTTCrxStep2() {
	if (getSynCoder().readBits(SynCoder::BITS_MONIT_TTC_READY) != 0) {
		throw TException(getDescription() + " SynCoder::ResetTTC: BITS_MONIT_TTC_READY did not go down after BITS_STATUS_TTC_RESET = 1");
	}
	getSynCoder().writeBits(SynCoder::BITS_STATUS2_TTC_RESET, 0);
	getSynCoder().writeBits(SynCoder::BITS_STATUS2_TTC_INIT_ENA, 0);//TODO this rather CANNOT be done in one operation, //TODO accorinng to KTP this it not needed
	LOG4CPLUS_DEBUG(logger, getDescription() + " ResetTTC Done");
}

void LinkBoard::intiTTCrxStep3() {
	if (getSynCoder().readBits(SynCoder::BITS_MONIT_TTC_READY) != 1) {
		LOG4CPLUS_WARN(logger, getDescription() << "LinkBoard::intiTTCrxStep3(): BITS_MONIT_TTC_READY did not go up after reset, Reseting once again");
		synCoder.resetTTC();
	}

	try {
		int controlRegValue = FixedHardwareSettings::LB_TTC_RX_CONTROL_REG; //lb->getTtcRxI2c().ReadControlReg() | 0x8;
		LOG4CPLUS_DEBUG(logger, getDescription() + " controlRegValue " << hex << controlRegValue);
		getTtcRxI2c().WriteControlReg(controlRegValue);
		//getTtcRxI2c().WriteCoarseDelay(0, 0); //TODO not neede, after reset should be 0, KB

		unsigned int controlReg = getTtcRxI2c().ReadControlReg();
		if (controlReg != FixedHardwareSettings::LB_TTC_RX_CONTROL_REG) {
		    throw TException(getDescription() + " SynCoder::intiTTCrxStep3: TTCrx controlReg is " + rpct::toString(controlReg) + " (should 0x9b)");
		}
	}
	catch(EI2C& e) {
		int rep = 3;
		for(int i = 0; i < rep; i++) {
			LOG4CPLUS_WARN(logger, getDescription() << " error accessing TTCrx via I2C. " << e.what() << ". Reseting the TTCrx and Trying once again");
			try {
				intiTTCrx(); //repeat the reset procedure, sets also the ControlReg
				break;
			}
			catch(EI2C& e) {
				if(i == rep-1) {
					throw e;
				}
			}
		}
		//getTtcRxI2c().WriteCoarseDelay(0, 0); //TODO not neede, after reset should be 0, KB
	}
}


void LinkBoard::enableAllChannels() {
    unsigned char masks[16];
    memset(masks, 0xff, 16);
    synCoder.writeWord(SynCoder::WORD_CHAN_ENA, 0, masks);
}

void LinkBoard::resetQPLL() {
    synCoder.resetQPLL();
}

void LinkBoard::resetQPLLStep1() {
	getSynCoder().writeBits(SynCoder::BITS_QPLL_MODE, 1); //mode = 1: 160 MHz frequency multiplication mode (160 MHz quartz crystal required).
	getSynCoder().writeBits(SynCoder::BITS_QPLL_RESET_ENA, 1);
}

void LinkBoard::resetQPLLStep2() {
	getSynCoder().writeBits(SynCoder::BITS_QPLL_RESET_ENA, 0);
}

void LinkBoard::resetQPLLStep3() {
	for(int i = 0; i < 5; i++) {
		if (getSynCoder().readBits(SynCoder::BITS_MONIT_QPLL_LOCKED) == 1) {
			return;
		}
		try {
			LOG4CPLUS_INFO(logger, getDescription()<<" ResetQPLL: QPLL not LOCKED, trying once again");
			synCoder.resetQPLL();
		}
		catch(TException& e) {

		}
	}
	throw TException(getDescription() + " SynCoder::ResetQPLL: QPLL_LOCKED did not go up");
}

TTTCrxI2C& LinkBoard::getTtcRxI2c() {
    if (ttcRxI2c.GetI2CInterface() == 0) {
        ttcRxI2c.SetI2CInterface(getI2C());
    }
    return ttcRxI2c;
}

void LinkBoard::setTTCrxDelays(int fineDelay1, int fineDelay2, int coarseDelay1, int coarseDelay2) {
    getTtcRxI2c().WriteFineDelay1(fineDelay1);
    getTtcRxI2c().WriteFineDelay2(fineDelay2);
    getTtcRxI2c().WriteCoarseDelay(coarseDelay1, coarseDelay2);
}

void LinkBoard::setWindow(int winO, int winC) {
    LOG4CPLUS_DEBUG(logger, "setWindow");
    getTtcRxI2c().WriteFineDelay1(winC);
    getTtcRxI2c().WriteFineDelay2(winO);
    LOG4CPLUS_DEBUG(logger, "setWindow done");
}

void LinkBoard::initDelays() {
    synCoder.writeWord(SynCoder::WORD_DATA_WIN_DELAY, 0, (uint32_t)0);
    synCoder.writeWord(SynCoder::WORD_DATA_DIAG_DELAY, 0, (uint32_t)0);
    synCoder.writeWord(SynCoder::WORD_DATA_DAQ_DELAY, 0, (uint32_t)0);
    setTTCrxDelays(220, 10, 0, 0);
}

void LinkBoard::resetGOL() {
    synCoder.resetGOL();
}

void LinkBoard::testClocks() { /*
    int regName = SynCoder::BITS_CLOCK_MAIN_TEST;
    getSynCoder().readBits(regName);
    unsigned int clkTest = getSynCoder().readBits(regName);
    bool ok = false;
    for(int i = 0; i < 100; i++) {
        if(clkTest != getSynCoder().readBits(regName)) {
            ok = true;
            break;
        }
        tbsleep(10);
    }
    if(!ok)  {
    	//throw TException(getDescription() + " testClocks(): BITS_CLOCK_MAIN_TEST do not change!!!! ");
    	LOG4CPLUS_ERROR(logger, getDescription() + " testClocks(): BITS_CLOCK_MAIN_TEST do not change!!!!");
    }

    regName = SynCoder::BITS_CLOCK_WIN_OPEN_TEST;
    getSynCoder().readBits(regName);
    clkTest = getSynCoder().readBits(regName);
    ok = false;
    for(int i = 0; i < 100; i++) {
        if(clkTest != getSynCoder().readBits(regName)) {
            ok = true;
            break;
        }
        tbsleep(10);
    }
    if(!ok) {
         //throw TException(getDescription() + " testClocks(): BITS_CLOCK_WIN_OPEN_TEST do not change!!!! ");//temoprary!!!!!!!!
    	LOG4CPLUS_ERROR(logger, getDescription() + " testClocks(): BITS_CLOCK_WIN_OPEN_TEST do not change!!!!");
    }

    regName = SynCoder::BITS_CLOCK_WIN_CLOSE_TEST;
    getSynCoder().readBits(regName);
    clkTest = getSynCoder().readBits(regName);
    ok = false;
    for(int i = 0; i < 100; i++) {
        if(clkTest != getSynCoder().readBits(regName)) {
            ok = true;
            break;
        }
        tbsleep(10);
    }
    if(!ok) {
         //throw TException(getDescription() + " testClocks(): BITS_CLOCK_WIN_CLOSE_TEST do not change!!!! ");
         LOG4CPLUS_ERROR(logger, getDescription() + " testClocks(): BITS_CLOCK_WIN_CLOSE_TEST do not change!!!!");
    }*/
}


/*
//this function is not call by the LinkBox::inti()
//LinkBox::inti() is doing the same by its own!!!!!
//in case of changes here, apply them also in the LinkBox::inti()
/void LinkBoard::init() {
    LOG4CPLUS_DEBUG(logger, getDescription() + " Initialize");
    try {
    	//LOG4CPLUS_DEBUG(logger, "synCoder.reset()");
	    //synCoder.reset();
	    LOG4CPLUS_DEBUG(logger, "intiTTCrx()");
	    intiTTCrx();

	    //LOG4CPLUS_DEBUG(logger, "enableAllChannels()");
	    //enableAllChannels();
	    LOG4CPLUS_DEBUG(logger, "resetQPLL()");
	    resetQPLL();
	    //LOG4CPLUS_DEBUG(logger, "setTTCrxDelays(220, 10, 0, 0)");
	    //setTTCrxDelays(220, 10, 0, 0);
    }
    catch(TException& e) {
    	throw TException("TException during init in " + getDescription() + ": " + e.what());
    }
    LOG4CPLUS_DEBUG(logger, getDescription() + " Initialize Done");
}*/


string LinkBoard::readTestRegs(bool verbose) {


    /*

lbx.lbox=int(sys.argv[1])
lbx.half=int(sys.argv[2])
cbicid=lbx.ccu_read8(cbic.CBIC_ID0)
if cbicid != 0xcb:
   print "Wrong CBIC ID:"+hex(cbicid)+"!"
   raise "Wrong CBIC ID:"+hex(cbicid)+"!"
cbicver=lbx.ccu_read8(cbic.CBIC_ID_VER_MJR)*256+lbx.ccu_read8(cbic.CBIC_ID_VER)
#cbpcver=lbx.ccu_read8(cbpc.CBPC_VER_MJR)*256+lbx.ccu_read8(cbpc.CBPC_VER_ID)
print "CBIC Version:"+hex(cbicver)
#print "CBPC Version:"+hex(cbpcver)
cbpcid=lbx.ccu_read8(cbpc.CBPC_IDENT)
if cbpcid != 0xcc:
   print "Wrong CBPC ID:"+hex(cbpcid)+"!"
   raise "Wrong CBPC ID:"+hex(cbpcid)+"!"
for lbnr in range(0,9):
   lbcid=lbx.ccu_read16(0x7000+0x100*lbnr+lbc.LBC_IDREG)
   lbver=lbx.ccu_read16(0x7000+0x100*lbnr+lbc.LBC_VER_ID)
   lbvermjr=lbx.ccu_read16(0x7000+0x100*lbnr+lbc.LBC_VER_MJR)
   if lbcid != 0x4442:
     print "Wrong LBC nr "+str(lbnr)+" ID:"+hex(lbcid)+"!"
     print "Skip LB nr"+hex(lbnr)+"!"
   else:
     print "LBC OK nr"+hex(lbnr)+" ver."+hex(lbvermjr)+"-"+hex(lbver)
     ktpid=lbx.ccu_read16(0x800*lbnr+1)
     print "KTP nr:"+str(lbnr)+" ID:"+hex(ktpid)
     */

    unsigned int lbcid;
    unsigned int lbver;
    unsigned int lbvermjr;
    unsigned int testreg;
    unsigned int lbcir0;
    unsigned int lbcir1;
    unsigned int ktpid0;
    unsigned int ktpid1;
    unsigned int ktpid2;
    // when verbose=true, do full dump of LBC registers having addresses from 0x00 to 0x4f
    const int lbcFullDump_minAddr=0x00;
    const int lbcFullDump_maxAddr=0x4f;
    unsigned int lbcFullDump[1+lbcFullDump_maxAddr-lbcFullDump_minAddr];

    try {
        unsigned int lbnr = (getPosition() - 1) % 10;
        unsigned int baseAddr = 0x7000 + lbnr * 0x100;
        lbcid = memoryRead16(baseAddr + LBC::LBC_IDREG);
        lbver = memoryRead16(baseAddr + LBC::LBC_VER_ID);
        lbvermjr = memoryRead16(baseAddr + LBC::LBC_VER_MJR);
        testreg = memoryRead16(baseAddr + LBC::LBC_TESTREG);
        lbcir0 = memoryRead16(baseAddr + LBC::LBC_INITS);
        lbcir1 = memoryRead16(baseAddr + LBC::LBC_INITS + 1);
        ktpid0 = memoryRead16(0x800 * lbnr + 0);
        ktpid1 = memoryRead16(0x800 * lbnr + 1);
        ktpid2 = memoryRead16(0x800 * lbnr + 2);
		if(verbose) {
            for(int i=lbcFullDump_minAddr; i<=lbcFullDump_maxAddr; i++) {
                lbcFullDump[i-lbcFullDump_minAddr] = memoryRead16(baseAddr + i);
            }
        }
    }
    catch(TException& e) {
        std::ostringstream ostr;
        ostr << "Error while checking device version: " << e.what()
        << ". Device id = " << getId() << " description = " <<  getDescription()
        << " board name = " << getDescription();
        throw TException(ostr.str().c_str());
    };

    std::ostringstream ostr;
    ostr << getDescription() <<  hex << " LBC:" << " LBC_VER_ID=0x" << lbver << " LBC_VER_MJR=0x" << lbvermjr<< " LBC_TESTREG=0x" << testreg << " (exp=0xbeef)\n";
    ostr << getDescription() <<  hex << " LBC:" << " LBC_IDREG=0x" << lbcid << " LBC_IR0=0x" << lbcir0<< " LBC_IR1=0x" << lbcir1 << "\n";
    ostr << getDescription() <<  hex << " KTP:" << " KTP_ID0=0x" << ktpid0 << " KTP_ID1=0x" << ktpid1 << " KTP_ID2=0x" << ktpid2;
    if(verbose) {
        for(int i=lbcFullDump_minAddr; i<=lbcFullDump_maxAddr; i++) {
            ostr << "\n";
	    ostr << getDescription() <<  hex << " LBC:" << " addr=0x" << i << " val=0x" << lbcFullDump[i-lbcFullDump_minAddr];
        }
    }

    return ostr.str();
}

void LinkBoard::configure(ConfigurationSet* configurationSet, int configureFlags) {
    BoardBase::configure(configurationSet, configureFlags);
}



void LinkBoard::dataToCodedDataItem(uint32_t data, LinkBoard::CodedDataItem& item) {
    item.BCN  = data & 0xf;
    item.BCN0 = (data >> 4) & 0x1;
    item.HP   = (data >> 5) & 0x1;
    item.EoD  = (data >> 6) & 0x1;
    item.Data = (data >> 7) & 0xff;
    item.Part = (data >> 15) & 0x3;
    item.Time = (data >> 17) & 0x7;
    item.ChNo = (data >> 20) & 0x3;
}

LinkBoard::CodedDataItem LinkBoard::dataToCodedDataItem(uint32_t data) {
    CodedDataItem item;
    dataToCodedDataItem(data, item);
    return item;
}


void LinkBoard::decodeData(uint32_t* srcData, size_t srcCount,
                           uint32_t* dstData, size_t dstCount) {
    //G_Log.out() <<  "DecodeData" << endl;
    CodedDataItem item;
    int dstIndex;
    //unsigned char* charData = (unsigned char*)dstData;
    for (uint32_t i = 0; i < srcCount; i++) {
        //G_Log.out() <<  "\ni = " << i << " ";
        //G_Log.out() <<  "srcData[i] = " << (uint32_t)srcData[i] << " ";
        dataToCodedDataItem(srcData[i], item);
        if (i < dstCount)
            dstData[i] = 0;
        dstIndex = i - item.Time;
        //G_Log.out() <<  "dstIndex = " << dstIndex << " ";
        if (dstIndex < 0)
            continue;
        if (dstIndex >= (int)dstCount)
            break;

        //G_Log.out() <<  " charIndex = " << (sizeof(uint32_t) * dstIndex + item.Part) << " ";
        //charData[sizeof(uint32_t) * dstIndex + item.Part] = item.Data;
        dstData[dstIndex] |= (item.Data  << (item.Part * 8));
        //G_Log.out() << " data = " << (uint32_t)item.Data << endl;
    }
}




MasterLinkBoard::MasterLinkBoard(int ident, const char* desc, LinkBox* lbox, int boardNum,
    int syncoderId)
: LinkBoard(ident, desc, lbox, boardNum, msMaster, syncoderId), leftSlave(0),
rightSlave(0), slaves(0) {
}

MasterLinkBoard::~MasterLinkBoard() {
	delete slaves;
}

void MasterLinkBoard::configureMaster(ConfigurationSet* configurationSet) {
	for (unsigned int iSLB = 0; iSLB < getSlaves().size(); iSLB++) {
		if (getSlaves()[iSLB] != 0) {
			IDevice* device = &(getSlaves()[iSLB]->getSynCoder() );
			DeviceSettings* settings = (configurationSet == 0) ? 0 : configurationSet->getDeviceSettings(*device);
			SynCoderSettings* scs = dynamic_cast<SynCoderSettings*>(settings);
		    if (scs == 0) {
		        throw IllegalArgumentException("SynCoder::configure: invalid cast for settings");
		    }

			if (getSlaves()[iSLB]->getMasterSlaveType() == msLeft) {
				slaveODelay_ = scs->getLMuxInDelay();
			}
			else if (getSlaves()[iSLB]->getMasterSlaveType() == msRight) {
				slave1Delay_ = scs->getLMuxInDelay();
			}
			else {
				//LOG4CPLUS_ERROR(logger, "illegal value of MasterSlaveTypes");
				throw TException("SynCoder::configureMaster(): illegal value of MasterSlaveTypes");
			}
		}
	}
}

void MasterLinkBoard::configure(ConfigurationSet* configurationSet, int configureFlags) {
	configureMaster(configurationSet);
    LinkBoard::configure(configurationSet, configureFlags);
}

void MasterLinkBoard::setSlaves() {
	slaves = new SlaveLinkBoards();

	LinkBoard* lb = getHalfBox()->getLinkBoardByPosition(getPosition() - 1);
	if (lb != 0) {
		leftSlave = dynamic_cast<SlaveLinkBoard*>(lb);
		if (leftSlave == 0) {
			throw IllegalArgumentException("MasterLinkBoard::setSlaves(): invalid LinkBoard type for slave at pos " + toString(lb->getPosition()));
		}
		slaves->push_back(leftSlave);
	}

	lb = getHalfBox()->getLinkBoardByPosition(getPosition() + 1);
	if (lb != 0) {
		rightSlave = dynamic_cast<SlaveLinkBoard*>(lb);
		if (rightSlave == 0) {
			throw IllegalArgumentException("MasterLinkBoard::setSlaves(): invalid LinkBoard type for slave at pos " + toString(lb->getPosition()));
		}
		slaves->push_back(rightSlave);
	}
}

SlaveLinkBoard* MasterLinkBoard::getLeftSlave() {
	if (slaves == 0) {
		setSlaves();
	}
    return leftSlave;
}
SlaveLinkBoard* MasterLinkBoard::getRightSlave() {
	if (slaves == 0) {
		setSlaves();
	}
    return rightSlave;
}

MasterLinkBoard::SlaveLinkBoards& MasterLinkBoard::getSlaves() {
	if (slaves == 0) {
		setSlaves();
	}
    return *slaves;
}

/*
void MasterLinkBoard::addSlave(SlaveLinkBoard* slave) {
    if ((slave->getPosition() + 1) == getPosition()) {
        if (slave->getMasterSlaveType() != msLeft) {
            throw IllegalArgumentException("MasterLinkBoard::addSlave: invalid MasterSlaveType, expecting msLeft");
        }
        if (leftSlave == 0) {
            slaves.push_back(slave);
        }
        else {
            throw IllegalArgumentException("MasterLinkBoard::addSlave: left slave already set");
        }
        leftSlave = slave;
    }
    else if ((slave->getPosition() - 1) == getPosition()) {
        if (slave->getMasterSlaveType() != msRight) {
            throw IllegalArgumentException("MasterLinkBoard::addSlave: invalid MasterSlaveType, expecting msRight");
        }
        if (rightSlave == 0) {
            slaves.push_back(slave);
        }
        else {
            throw IllegalArgumentException("MasterLinkBoard::addSlave: right slave already set");
        }
        rightSlave = slave;
    }
    else {
        throw IllegalArgumentException("MasterLinkBoard::addSlave: illegal board number");
    }
}*/


void MasterLinkBoard::synchronizeLink() {
    getSynCoder().writeBits(SynCoder::BITS_GOL_TX_ENA, 0);
}

void MasterLinkBoard::normalLinkOperation() {
    getSynCoder().writeBits(SynCoder::BITS_GOL_TX_ENA, 1);
}

void MasterLinkBoard::enableFindOptLinksSynchrDelay(bool enable, unsigned int testData) {
    if(enable) {
        getSynCoder().writeBits(SynCoder::BITS_SEND_CHECK_DATA_ENA, 0);
        getSynCoder().writeBits(SynCoder::BITS_SEND_CHECK_ENA, 0);
            //MLBVec[iLink]->getSynCoder().writeBits(SynCoder::BITS_SENDER_PART_ENA, 0);
        getSynCoder().writeBits(SynCoder::BITS_SEND_TEST_RND_ENA, 0);
        getSynCoder().writeBits(SynCoder::BITS_SEND_TEST_ENA, 1);
        getSynCoder().writeWord(SynCoder::WORD_SEND_TEST_DATA, 0, testData);
    }
    else {
        getSynCoder().writeBits(SynCoder::BITS_SEND_TEST_ENA, uint32_t(0));
        getSynCoder().writeWord(SynCoder::WORD_SEND_TEST_DATA, 0, uint32_t(0));
    }
}

void MasterLinkBoard::enableTransmissionCheck(bool enableBCNcheck, bool enableDataCheck) {
    bool checkDataEna = enableBCNcheck;
    bool checkEna = enableDataCheck;

    if(checkDataEna) {
        getSynCoder().writeBits(SynCoder::BITS_SEND_CHECK_DATA_ENA, 1);
    }
    else {
        getSynCoder().writeBits(SynCoder::BITS_SEND_CHECK_DATA_ENA, uint32_t(0));
    }

    if(checkEna) {
        getSynCoder().writeBits(SynCoder::BITS_SEND_CHECK_ENA, uint32_t(1));
    }
    else {
        getSynCoder().writeBits(SynCoder::BITS_SEND_CHECK_ENA, uint32_t(0));
    }
}


SlaveLinkBoard::SlaveLinkBoard(int ident, const char* desc, LinkBox* lbox, int boardNum, LinkBoardMasterSlaveType masterSlaveType, int syncoderId)
: LinkBoard(ident, desc, lbox, boardNum, masterSlaveType, syncoderId), master(0) {
}

SlaveLinkBoard::~SlaveLinkBoard() {
}


MasterLinkBoard* SlaveLinkBoard::getMaster() {
	if (master == 0) {
		LinkBoard* lb = getHalfBox()->getLinkBoardByPosition(getPosition() + ((getMasterSlaveType() == msLeft ? 1 : -1)));
		if (lb != 0) {
			master = dynamic_cast<MasterLinkBoard*>(lb);
			if (master == 0) {
				throw IllegalArgumentException("SlaveLinkBoard::getMaster(): invalid LinkBoard type for slave at pos " + toString(getPosition()));
			}
		}
	}
	return master;
}

/* for preparing configuration for flash */
void SynCoder::prepareForFlash_write(uint16_t byte)
{
	flashEnc.putWord(byte);
	LOG4CPLUS_DEBUG(logger, "SynCoder::prepareForFlash_write("<<hex<<byte<<")");
}

void SynCoder::prepareForFlash_write(uint16_t addr, uint16_t cmd)
{
	prepareForFlash_write(addr);
	prepareForFlash_write(cmd);
}

void SynCoder::prepareForFlash_cancelWatchdog()
{
	prepareForFlash_write(0xfff8);
}

//cycle = 65535 * 25 ns = 0xffff * 25 ns = 1 638 375 ns = 1.638375 ms = 0.0016 s
void SynCoder::prepareForFlash_setWatchdog(uint16_t cycles)
{
	/* each long delay cycle is 65535*25ns long */
	prepareForFlash_write(0xfff7, cycles);
}

//cycle = 65535 * 25 ns = 0xffff * 25 ns = 1 638 375 ns = 1.638375 ms = 0.0016 s
void SynCoder::prepareForFlash_ldelay(uint16_t cycles)
{
	/* each long delay cycle is 65535*25ns long */
	prepareForFlash_write(0xfffa, cycles);
}

//cycle = 25 ns = 1 BX
void SynCoder::prepareForFlash_delay(uint16_t cycles)
{
	prepareForFlash_write(0xfffb, cycles);
}

void SynCoder::prepareForFlash_verify(uint16_t ii_address, uint16_t ii_bit_mask, uint16_t ii_desired_value, uint16_t timeout)
{
	/* example: prepareForFlash_verify(0xe,0x2,0,0xf000) =  Check TTC ready = 0  */
	prepareForFlash_write(0xfffc);
	prepareForFlash_write(ii_address);
	prepareForFlash_write(ii_bit_mask);
	prepareForFlash_write(ii_desired_value);
	prepareForFlash_write(timeout);
}

/* writes data to TTCrx via I2C */
void SynCoder::prepareForFlash_i2c(uint8_t addr, uint8_t val)
{
	prepareForFlash_write(0xfffe);
	unsigned char taddr = TTCrxI2CFlashAddr <<1;
	// prepareForFlash_write((addr<<8) | val);
	prepareForFlash_write((((uint16_t)taddr)<<8) | (uint16_t)addr);
	prepareForFlash_write(0xfffe);
	prepareForFlash_write((((uint16_t)taddr+1)<<8) | (uint16_t)val);
}

void SynCoder::writeOneBitFlash(int id, uint32_t data) {
    const MemoryMap::TItemInfo& item = CheckWriteAccess(id, 0);

    // temporarily we will not handle the following
    if ((size_t) memoryMap_->GetDataSize() > sizeof(uint32_t))
        throw TException("TMemoryMap::WriteBit DataSize > sizeof(uint32_t)");

    uint32_t mask = (1 << item.Width) - 1;//item.Width is count of item's bits in the vector
    uint32_t data1;
    data1 = ((data & mask) << item.AddrLen); //item.AddrLen is position of item's bits in the vector

    writeFlash(item.AddrPos,
                       &data1,
                       1,
                       memoryMap_->GetDataSize());

    firmwareInitWrites[item.ParentID] = (uint16_t) data1;
}

uint32_t SynCoder::prepareBitsFlash_first(int id, uint32_t data)
{
    const MemoryMap::TItemInfo& item = CheckWriteAccess(id, 0);

    // temporarily we will not handle the following
    if ((size_t) memoryMap_->GetDataSize() > sizeof(uint32_t))
        throw TException("TMemoryMap::WriteBit DataSize > sizeof(uint32_t)");

    uint32_t mask = (1 << item.Width) - 1;
    uint32_t data1;
    data1 = ((data & mask) << item.AddrLen); //item.AddrLen is position of the bits in the vector
    return data1;
}

uint32_t SynCoder::prepareBitsFlash_next(int id, uint32_t data, uint32_t previousResult)
/* WARNING: when preparing next bit, you have to check manually that it is part
 * of the same 16-bit word as the prevoius bits. Otherwise the result shall be corrupted.
 */
{
    const MemoryMap::TItemInfo& item = CheckWriteAccess(id, 0);

    // temporarily we will not handle the following
    if ((size_t) memoryMap_->GetDataSize() > sizeof(uint32_t))
        throw TException("TMemoryMap::WriteBit DataSize > sizeof(uint32_t)");

    uint32_t mask = (1 << item.Width) - 1; //item.Width is count of item's bits in the vector
    uint32_t data1 = previousResult;
    data1 &= ~ (mask << item.AddrLen);
    data1 |= ((data & mask) << item.AddrLen);
    return data1;
}

uint32_t SynCoder::writePreparedBitsFlash(int id, uint32_t previousResult)
{
	const MemoryMap::TItemInfo& item = CheckWriteAccess(id, 0);
	writeFlash(item.AddrPos,
	                       &previousResult,
	                       1,
	                       memoryMap_->GetDataSize());

	firmwareInitWrites[item.ParentID] =  (uint16_t) previousResult;
	return previousResult;
}

void SynCoder::writeTwoBitsFlash(int id, uint32_t data, int id2, uint32_t data2) {
	uint32_t val = prepareBitsFlash_first(id, data);
	val = prepareBitsFlash_next(id2, data2, val);
	writePreparedBitsFlash(id, val);
}


void SynCoder::writeVectorFlash(int id, uint32_t data) {
    //G_Debug.out() << "writeVectorMZ " << id << ", " << data << endl;
    writeWordFlash(id, 0, data);
}

/* OK */ void SynCoder::writeWordFlash(int id, int idx, void* data) {
    const MemoryMap::TItemInfo& item = CheckWriteAccess(id, idx);

    /*
    HardwareIfc->Write(item.AddrPos + idx * item.AddrLen,
                       data,
                       item.AddrLen,
                       memoryMap_->GetDataSize());
                       */
    /* let's assume DataSize==2 and see if there will be errors */
    if ((size_t) memoryMap_->GetDataSize() != 2)
    	throw TException("writeWordMZ: DataSize != 2");
    if (idx != 0)
    	throw TException("writeWordMZ: idx != 0");


    for (int i=0; i<item.AddrLen; ++i)
    {
    	if (item.AddrLen==1 && id != WORD_CHAN_ENA)
    		firmwareInitWrites[id] =  *((uint16_t*)data+i);
    	/* WORD_CHAN_ENA is handled as a special case due to large number of bits
    	 * if you need to verify another value with more than 16 bits, you should
    	 * implement similar workaround. Otherwise the verification is guaranteed
    	 * to fail. <maciekz>, 2012-02-28
    	 */

    	prepareForFlash_write(item.AddrPos+i, *((uint16_t*)data+i));
    	//if (item.AddrLen==1) firmwareInitWrites[id] =  *((uint16_t*)data+i);
    }
}

/* OK */ void SynCoder::writeWordFlash(int id, int idx, uint32_t data) {
	writeWordFlash(id, idx, &data);
}

void SynCoder::writeFlash(uint32_t address, void* data,
                         size_t word_count, size_t word_size) {
    assert(word_size == 2);

	for (size_t i=0; i<word_count; i++) {
		prepareForFlash_write(address+i, *((unsigned short*)data+i));
		//firmwareInitWrites[address+i] =  *((unsigned short*)data+i);
	}
}

/* OK */ void SynCoder::configureForFlash(ConfigurationSet* configurationSet) {
	//DeviceSettings* settings = configurationSet->getDeviceSettings(*this);
	DeviceSettings* settings = configurationSet->getDeviceSettings(this->getId());

//	int flags = ConfigurationFlags::FORCE_CONFIGURE;
    LinkBoard& lb = dynamic_cast<LinkBoard&>(getBoard());

//    lb.memoryWrite16(0x7000 + (getBoard().getPosition() - 1)%10 * 0x100 + LBC::LBC_TESTREG, 0xbeef);
//    prepareForFlash_write(0x7000 + (getBoard().getPosition() - 1)%10 * 0x100 + LBC::LBC_TESTREG, 0xbeef);
    // FIXME

    uint32_t pulserBits = 0;
    writeVectorFlash(VECT_PULSER, pulserBits); //to reset the pulser, if it was used before and xdaq crashed not reseting the pulser

    if (settings == 0) {
        throw NullPointerException("SynCoder::configure: settings == 0");
    }
    SynCoderSettings* scs = dynamic_cast<SynCoderSettings*>(settings);
    if (scs == 0) {
        throw IllegalArgumentException("SynCoder::configure: invalid cast for settings");
    }

    if (lb.getMaster() == 0) {
        throw TException("SynCoder::configure: getMaster() returns 0. Check if master is not disabled in the database");
    }

	setWindowFlash(scs->getWindowOpen(), scs->getWindowClose());
	uint32_t status1Bits = 0;
	//setBitsInVector(BITS_STATUS1_FULL_OUT, 0, status1Bits);
	//setBitsInVector(BITS_STATUS1_EDGE_PULSE, 0, status1Bits);
	setBitsInVector(BITS_STATUS1_INV_CLOCK, scs->getInvertClock(), status1Bits);
	//setBitsInVector(BITS_STATUS1_TRG_DATA_SEL, 0, status1Bits);
	//setBitsInVector(BITS_STATUS1_TRG_AREA_ENA, 0, status1Bits);
	if(lb.getMasterSlaveType() == msMaster) {
		//setBitsInVector(BITS_STATUS1_SLAVE_ENA, 0, status1Bits);
		//setBitsInVector(BITS_STATUS1_SLAVE_DATA_ENA, 0, status1Bits);
	}
	else {
		setBitsInVector(BITS_STATUS1_SLAVE_ENA, 1, status1Bits);
		setBitsInVector(BITS_STATUS1_SLAVE_DATA_ENA, 1, status1Bits);
	}
	setBitsInVector(BITS_STATUS1_RBC_EXPAND, 0, status1Bits);
	writeVectorFlash(VECT_STATUS1, status1Bits);

	uint32_t status2Bits = 0;
	if(lb.getDescription().find("LB_RE") != string::npos) {
		setBitsInVector(BITS_STATUS2_CSC_ENA, 1, status2Bits);
		setBitsInVector(BITS_STATUS2_CSC_OR_SEL, 3, status2Bits);  //or = 2^(CSC_OR_SEL)
		setBitsInVector(BITS_STATUS2_CSC_SHIFT, 0ul, status2Bits);
	}
	else if(lb.getDescription().find("LB_RB") != string::npos) {
		setBitsInVector(BITS_STATUS2_CSC_ENA, 0ul, status2Bits);
	}
	else {
		LOG4CPLUS_ERROR(logger, "LB getDescription() not contains LB_RE or LB_RB !!!!!!!!!!!!!!");
	}
	setBitsInVector(BITS_STATUS2_CSC_RBC_DELAY, scs->getRbcDelay(), status2Bits); //RBC puls delay
	writeVectorFlash(VECT_STATUS2, status2Bits);

    //this must be done always, because the enabled/disabled slaves (this is changing regardless of the SynCoderSettings)
    uint32_t lmuxBits = 0;
    if(lb.getMasterSlaveType() == msMaster) {
    	setBitsInVector(BITS_LMUX_MASTER_ENA, 1, lmuxBits);
    	setBitsInVector(SynCoder::BITS_LMUX_MASTER_DELAY, scs->getLMuxInDelay(), lmuxBits);
    	MasterLinkBoard& master = dynamic_cast<MasterLinkBoard&>(getBoard());
    	master.configureMaster(configurationSet);
    	for (unsigned int iSLB = 0; iSLB < master.getSlaves().size(); iSLB++) {
    		if (master.getSlaves()[iSLB] != 0) {
    			if (master.getSlaves()[iSLB]->getMasterSlaveType() == msLeft) {
    				setBitsInVector(SynCoder::BITS_LMUX_SLAVE0_ENA, 1, lmuxBits);
    				setBitsInVector(SynCoder::BITS_LMUX_SLAVE0_CHECK_ENA, 1, lmuxBits);
    				setBitsInVector(SynCoder::BITS_LMUX_SLAVE0_DELAY, master.getSlaveODelay(), lmuxBits);
    				writeWordFlash(WORD_SLAVE0_CHK_ERR_COUNT, 0, uint32_t(0)); //TODO maybe not needed here
    			}
    			else if (master.getSlaves()[iSLB]->getMasterSlaveType() == msRight) {
    				setBitsInVector(SynCoder::BITS_LMUX_SLAVE1_ENA, 1, lmuxBits);
    				setBitsInVector(SynCoder::BITS_LMUX_SLAVE1_CHECK_ENA, 1, lmuxBits);
    				setBitsInVector(SynCoder::BITS_LMUX_SLAVE1_DELAY, master.getSlave1Delay(), lmuxBits);
    				writeWordFlash(WORD_SLAVE1_CHK_ERR_COUNT, 0, uint32_t(0)); //TODO maybe not needed here
    			}
    			else {
    				LOG4CPLUS_ERROR(logger, "illegal value of MasterSlaveTypes");
    				throw TException("SynCoder::configureForFlash(): illegal value of MasterSlaveTypes");
    			}
    		}
    	}
    }
    writeVectorFlash(VECT_LMUX, lmuxBits); //0 in case of the slave

	if(lb.getMasterSlaveType() == msMaster) {
		uint32_t sendBits = 0;
		setBitsInVector(BITS_SEND_CHECK_DATA_ENA, 1, sendBits);
		setBitsInVector(BITS_SEND_CHECK_ENA, 1, sendBits);
		writeVectorFlash(VECT_SEND, sendBits);
	}
	else {
		uint32_t golBits = 0;
		setBitsInVector(SynCoder::BITS_GOL_DATA_SEL, GOL_COND_SEL_MASTER, golBits);
		writeVectorFlash(VECT_GOL, golBits);
	}

	writeWordFlash(WORD_BCN0_DELAY, 0, scs->getBcn0Delay());
	writeWordFlash(WORD_SEND_DELAY, 0, scs->getDataDaqDelay());

	setChannelsEnaFlash(scs->getInChannelsEna());


	writeWordFlash(WORD_USER_REG1, 0, scs->getConfigurationId() & 0xfffful);
    //writeBits(BITS_STATUS2_RESET_DET, 0);
    //readBits(BITS_MONIT_RESET_DET); //TODO read all MONIT bits and check
}

/* OK */ void SynCoder::resetGOLFlash() {
    uint32_t gol = 0;
    setBitsInVector(BITS_GOL_RESET_ENA, 1, gol);
    writeVectorFlash(VECT_GOL, gol);
    prepareForFlash_ldelay(30); //30 = 48 ms

    setBitsInVector(BITS_GOL_RESET_ENA, 0, gol);
    setBitsInVector(BITS_GOL_TX_ENA, 0, gol);
    setBitsInVector(BITS_GOL_DATA_SEL, GOL_COND_SEL_LMUX, gol);
    setBitsInVector(BITS_GOL_LASER_ENA, 0, gol);

    writeVectorFlash(VECT_GOL, gol);
    /**
     * According to the GOL manual:
     * after the reset the INITIALISATION PROCEDURE is executed (page 20):
     * the PLL_lock_time (table 21) should be 25.6 us
     * assuming the CONFIG 1 has default-after-reset value i.e. 0x1F
     * and "slow mode" (but I am not sure if we are in slow or fast mode)
     *
     * then the GOL waits the time defined in the COnnfig O ("wait time")
     * assuming the CONFIG 0 has default-after-reset value i.e. 0x13 (dec 19)
     * the wait tmie accrding o the table 20 should be 13.12 ms (max is 25.24 ms)
     * after that the transmitter enters the "READY" state
     *
     */
    prepareForFlash_ldelay(10); //10 = 16 ms

    setBitsInVector(BITS_GOL_LASER_ENA, 1, gol); //here BITS_GOL_TX_ENA is 1, so synchronization word is started be send
    writeVectorFlash(VECT_GOL, gol);

	prepareForFlash_ldelay(1);

	setBitsInVector(BITS_GOL_TX_ENA, 1, gol);
    writeVectorFlash(VECT_GOL, gol);
}

/* OK */ void SynCoder::setChannelsEnaFlash(const boost::dynamic_bitset<> &channelsEna) {
    char* chanEnaBits = 0;
    unsigned int channelsNum = getItemDesc(WORD_CHAN_ENA).Width;
    if (channelsEna.size() == channelsNum) {
        chanEnaBits = bitsetToBits(channelsEna);
        firmwareInitChanEna = channelsEna;
    }
    else if (channelsEna.size() < channelsNum) {
        boost::dynamic_bitset<> ce = channelsEna; //make a copy as channelsEna is const
        ce.resize(channelsNum, false);
        chanEnaBits = bitsetToBits(ce);
        firmwareInitChanEna = ce;
    }
    else  {
        throw IllegalArgumentException("LinkBoard::setChannelsEna: channelsEna size exceeds channels number");
    }
    writeWordFlash(WORD_CHAN_ENA, 0, chanEnaBits);
    delete [] chanEnaBits;
}


void SynCoder::setWindowFlash(int winO, int winC) {
    //getTtcRxI2c().WriteFineDelay1(winC);
    //synCoder.prepareForFlash_i2c(getTtcRxI2c().FineDelay1Addr, winC);

    prepareForFlash_i2c(TTTCrxBase::FineDelay1Addr, getLinkBoard().getTtcRxI2c().Convert(winC));
//    getTtcRxI2c().WriteFineDelay2(winO);
    prepareForFlash_i2c(TTTCrxBase::FineDelay2Addr, getLinkBoard().getTtcRxI2c().Convert(winO));
    getLinkBoard().getTtcRxI2c().SetFineDelay1(winC);
    getLinkBoard().getTtcRxI2c().SetFineDelay2(winO);
}

void SynCoder::resetQPLLFlash() {
	writeOneBitFlash(SynCoder::BITS_QPLL_MODE, 1); //mode = 1: 160 MHz frequency multiplication mode (160 MHz quartz crystal required).
	writeTwoBitsFlash(SynCoder::BITS_QPLL_MODE, 1, SynCoder::BITS_QPLL_RESET_ENA, 1);
	prepareForFlash_ldelay(2);//3.2 ms
	writeOneBitFlash(SynCoder::BITS_QPLL_MODE, 1); //reset the BITS_QPLL_RESET_ENA
	prepareForFlash_ldelay(167); //167 = 267 ms
	//ori getSynCoder().writeOneBitMZ(SynCoder::BITS_QPLL_RESET_ENA, 0);
	//getSynCoder().prepareForFlash_ldelay(350*46);

	/*	for(int i = 0; i < 5; i++) {
		if (getSynCoder().readBits(SynCoder::BITS_MONIT_QPLL_LOCKED) == 1) {
			return;
		}
		synCoder.resetQPLL();
	}*/
	/* MZ: this was found out not to be needed in practice, and cannot be implemented in
	 * initialization from flash */
}

void SynCoder::setTtcRxI2cAddressFlash(unsigned char addr) {
	getLinkBoard().getTtcRxI2c().SetAddress(addr); //TODO not sure it should be here (KB). in principle the correct address is assigned in the LinkBoard constructor
    writeWordFlash(SynCoder::WORD_TTC_ID_MODE, 0, addr);
    setTTCrxI2CFlashAddr(addr);
}

void SynCoder::intiTTCrxFlash() {
	setTtcRxI2cAddressFlash(getLinkBoard().getTtcRxI2c().GetAddress());

	uint32_t val = prepareBitsFlash_first(SynCoder::BITS_STATUS2_TTC_INIT_ENA, 1);
	writePreparedBitsFlash(SynCoder::BITS_STATUS2_TTC_INIT_ENA, val);

	val = prepareBitsFlash_next(SynCoder::BITS_STATUS2_TTC_RESET, 1, val );
	writePreparedBitsFlash(SynCoder::BITS_STATUS2_TTC_INIT_ENA, val); //BITS_STATUS2_TTC_INIT_ENA gives the vector address, as thisis the first item in the vector, why not use VECT_STATUS2???

	prepareForFlash_ldelay(34); //34 = 54 ms

	/* FIXME  if (getSynCoder().readBits(SynCoder::BITS_MONIT_TTC_READY) != 0) {
		throw TException(getDescription() + " SynCoder::ResetTTC: BITS_MONIT_TTC_READY did not go down after BITS_STATUS_TTC_RESET = 1");
	} */

	val = prepareBitsFlash_next(SynCoder::BITS_STATUS2_TTC_RESET, 0, val );
	writePreparedBitsFlash(SynCoder::BITS_STATUS2_TTC_INIT_ENA, val);

	val = prepareBitsFlash_next(SynCoder::BITS_STATUS2_TTC_INIT_ENA, 0, val );
	writePreparedBitsFlash(SynCoder::BITS_STATUS2_TTC_INIT_ENA, val);

	prepareForFlash_ldelay(34);

	//in the software I2C the waiting for the acknowledge is 200 us = 0.2 ms
	prepareForFlash_setWatchdog(30); //10 = 16 ms
												// random value inserted -- maciekz

	int controlRegValue = FixedHardwareSettings::LB_TTC_RX_CONTROL_REG; //lb->getTtcRxI2c().ReadControlReg() | 0x8;
	prepareForFlash_i2c(TTTCrxBase::ControlRegisterAddr, controlRegValue);
	prepareForFlash_cancelWatchdog();
}

const MemoryMap::TItemInfo& SynCoder::CheckWriteAccess(int id, int idx)
{
	if (dynamic_cast<TMemoryMap*>(memoryMap_) != 0)
		return (dynamic_cast<TMemoryMap*>(memoryMap_))->CheckWriteAccess(id, idx);
	else if (dynamic_cast<TMemoryMapLBWrapper*>(memoryMap_) != 0)
		return (dynamic_cast<TMemoryMapLBWrapper*>(memoryMap_))->CheckWriteAccess(id, idx);
	else
		throw TException("Syncoder::CheckWriteAccess failed!");
}

void SynCoder::finalizeFlashConfig() {
	writeWordFlash(rpct::SynCoder::WORD_SEND_TEST_DATA, 0, CONTROL_BUS_CHECK_VALUE);
	writeWordFlash(rpct::SynCoder::WORD_USER_REG2, 0, 0xffaa);
	ostringstream file;
	file << "/tmp/lbflash_maciekz";
	file << id;
	ofstream out(file.str().c_str());
	if (!out.good())
		throw TException("SynCoder::finalizeFlashConfig can't open file!");

	out<<flashEnc.end();
	out.close();

	file << ".unenc";
	out.open(file.str().c_str());
	out<<flashEnc.getUnencoded();
	out.close();
}

std::string SynCoder::getFlashConfig() {
	return flashEnc.end();
}

void SynCoder::prepareForFlashConfig() {
	flashEnc.reset();
	//prepareForFlash_write(0x0013);
	writeWordFlash(rpct::SynCoder::WORD_USER_REG2, 0, 0xffba);
}

void SynCoder::setTTCrxI2CFlashAddr(unsigned char _addr) {
	TTCrxI2CFlashAddr = _addr;
}
unsigned char SynCoder::getTTCrxI2CFlashAddr() {
	return TTCrxI2CFlashAddr;
}

//this is done in the resetGOLFLash
void SynCoder::synchronizeLinkFlash() {
	LinkBoard& lb = dynamic_cast<LinkBoard&>(getBoard());

	if (lb.getMasterSlaveType() != rpct::msMaster)
		return;

    uint32_t gol = 0;
    setBitsInVector(BITS_GOL_TX_ENA, 1, gol);
    setBitsInVector(BITS_GOL_DATA_SEL, GOL_COND_SEL_LMUX, gol);
    setBitsInVector(BITS_GOL_LASER_ENA, 1, gol);
	setBitsInVector(BITS_GOL_TX_ENA, 0, gol);
    writeVectorFlash(VECT_GOL, gol);

	prepareForFlash_ldelay(152);

	setBitsInVector(BITS_GOL_TX_ENA, 1, gol);
    writeVectorFlash(VECT_GOL, gol);
}

void SynCoder::enableHistoFlash() {
	LinkBoard& lb = dynamic_cast<LinkBoard&>(getBoard());
	if (lb.getMasterSlaveType() == rpct::msMaster)
	{
		writeWordFlash(rpct::SynCoder::WORD_SLAVE0_CHK_ERR_COUNT, 0, uint32_t(0));
		writeWordFlash(rpct::SynCoder::WORD_SLAVE1_CHK_ERR_COUNT, 0, uint32_t(0));
	}

	uint64_t countersLimit = 0xffffffffffull;//10 * secBx;

	writeVectorFlash(VECT_STAT, 0);
	writeWordFlash(WORD_STAT_TIMER_LIMIT, 0, (void*) &countersLimit);
	//writeVectorFlash(VECT_STAT, 0); has no sense
	writeWordFlash(WORD_STAT_TIMER_TRG_DELAY, 0, uint32_t(0));
	//writeVectorMZ(VECT_STATUS1, 0);
	writeWordFlash(WORD_DATA_TRG_DELAY, 0, uint32_t(0));
	//writeVectorFlash(VECT_STAT, 0); has no sense
	writeVectorFlash(VECT_STAT, 0x40); //PROC_REQ = 1
	prepareForFlash_ldelay(1);
	writeVectorFlash(VECT_STAT, 0x41); //PROC_REQ = 1, TIMER_START = 1
	//writeVectorFlash(VECT_STAT, 0xc1); //PROC_ACK changing to 1, has no sense to write
	writeVectorFlash(VECT_STAT, 0xc5); //PROC_REQ = 1, TIMER_START = 1, TIMER_LOC_ENA = 1 - starting counting
	// check TIMER_LOC_ENA = 1 ?

	/* VECT_STAT is not to be verified */
	firmwareInitWrites.erase(VECT_STAT);
	//fmswfirmwareInitWrites.erase(VECT_STATUS1);
	firmwareInitWrites.erase(WORD_SLAVE0_CHK_ERR_COUNT);
	firmwareInitWrites.erase(WORD_SLAVE1_CHK_ERR_COUNT);
}

void SynCoder::generateFlashConfig(ConfigurationSet* configurationSet) {
	getLinkBoard().getI2C();

	prepareForFlashConfig();
	intiTTCrxFlash();
	resetQPLLFlash();

	configureForFlash(configurationSet);
	if(getLinkBoard().getMasterSlaveType() == msMaster)
		resetGOLFlash(); //it must be done after the BCO delay is set; resetGOLFlash does the synchronizeLink
	//(*iLB)->getSynCoder().synchronizeLinkFlash();
	enableHistoFlash();
	finalizeFlashConfig();
}

bool SynCoder::verifyFirmawreWrites() {
	LinkBoard& lb = dynamic_cast<LinkBoard&>(getBoard());
	for (map<int, uint16_t>::const_iterator i = firmwareInitWrites.begin(); i!= firmwareInitWrites.end(); ++i)
	{
		const MemoryMap::TItemInfo& item = CheckWriteAccess((*i).first, 0);
		int width = item.Width;
		volatile uint16_t mask = 0xffff;
		mask = (mask<<(16-width));
		mask = mask>>(16-width);
		if (item.ID == VECT_PULSER)
			mask &= 0xffdf;

		/* FIXME: hack because BITS_QPLL_ERROR is up on one board
		 * should be hopefully chcecked and removed
		 * maciekz 20120306
		 */
		if (item.ID == VECT_QPLL)
			mask &= 0xfff7;

		//			LOG4CPLUS_ERROR(logger, getFullName()<<" unexpected Width "<<item.Width<<" in item "<<(*i).first<<" mask "<<hex<<mask);


		if  ((readWord((*i).first, 0) & uint32_t(mask)) != ((*i).second & uint32_t(mask)))
		{
			LOG4CPLUS_ERROR(logger, __FUNCTION__<<" "<<getFullName()<<" expected "<<hex<<(*i).second<<" but found "<<readWord((*i).first, 0)<<" at address "<<(*i).first<<" in verifyFirmwareWrites().");
			return false;
		}
	}

	char *chanEnaBits = 0, *chanEnaExp = 0;
	chanEnaBits = bitsetToBits(firmwareInitChanEna); //to create the table with proper size
	chanEnaExp  = bitsetToBits(firmwareInitChanEna);
	readWord(WORD_CHAN_ENA, 0, chanEnaBits);
	/*LOG4CPLUS_INFO(logger, __FUNCTION__<<" "<<getFullName()<<" firmwareInitChanEna.size() "<<firmwareInitChanEna.size()<<" bits "<<firmwareInitChanEna);
	LOG4CPLUS_INFO(logger, __FUNCTION__<<" "<<getFullName()<<" WORD_CHAN_ENA: expected "<<dataToHex(chanEnaExp, firmwareInitChanEna.size())
					<<" found in hardware "<<dataToHex(chanEnaBits, firmwareInitChanEna.size()));*/
	if (!std::equal(chanEnaBits, chanEnaBits+(firmwareInitChanEna.size()/8), chanEnaExp))
	{
		LOG4CPLUS_ERROR(logger, __FUNCTION__<<" "<<getFullName()<<" WORD_CHAN_ENA: expected "<<dataToHex(chanEnaExp, firmwareInitChanEna.size())
				<<" found in hardware "<<hex<<dataToHex(chanEnaBits, firmwareInitChanEna.size()));
		delete[] chanEnaBits;
		delete[] chanEnaExp;
		return false;
	}
	delete[] chanEnaBits;
	delete[] chanEnaExp;

	rpct::TTTCrxI2C& i2c = lb.getTtcRxI2c();
	if ((i2c.GetFineDelay1() != i2c.ReadFineDelay1()) || (i2c.GetFineDelay2() != i2c.ReadFineDelay2()))
	{
		LOG4CPLUS_ERROR(logger, __FUNCTION__<<" "<<getFullName()<<" fine delays expected: "<<i2c.GetFineDelay1()<<" ,"<<i2c.GetFineDelay2()
				<<", found "<<" "<<i2c.ReadFineDelay1()<<", "<<i2c.ReadFineDelay2());
		return false;
	}

	/*
	for (int i=0; i<firmwareInitChanEna.size(); ++i)
	{
		LOG4CPLUS_INFO(logger, getFullName()<<" idx= "<<i<<".");

		if (readWord(WORD_CHAN_ENA, i) != firmwareInitChanEna[i])
		{
			LOG4CPLUS_ERROR(logger, getFullName()<<" expected "<<hex<<firmwareInitChanEna[i]<<" but found "<<readWord(WORD_CHAN_ENA, i)<<" at WORD_CHAN_ENA, idx= "<<i<<".");
			return false;
		}
	}*/
	return true;
}

void LinkBoard::checkVersions() {
	unsigned int lbnr = (getPosition() - 1) % 10;
	unsigned int baseAddr = 0x7000 + lbnr * 0x100;

	unsigned int lbcid = 0;
	unsigned int lbver = 0;
	unsigned int lbvermjr = 0;
	try {
		//lbcid = memoryRead16(baseAddr + LBC::LBC_IDREG);
		lbver = memoryRead16(baseAddr + LBC::LBC_VER_ID);
		lbvermjr = memoryRead16(baseAddr + LBC::LBC_VER_MJR);
	}
	catch(TException& e) {
		std::ostringstream ostr;
		ostr << "Error while checking device version: " << e.what()
        				<< ". Device id = " << getId() << " description = " <<  getDescription()
        				<< " board name = " << getDescription();
		throw TException(ostr.str().c_str());
	};

	EBadDeviceVersion::VersionInfoList errorList;
	ostringstream err;

	if(lbvermjr != LBC::LBC_VERSION_MAJOR || lbver != LBC::LBC_VERSION) {
		err << std::hex << getDescription() << " bad LBC version " << lbvermjr<<"."<<lbver
				<< " expected " << LBC::LBC_VERSION_MAJOR<<"."<<LBC::LBC_VERSION;

		ostringstream ostrExp;
		ostrExp<< LBC::LBC_VERSION_MAJOR<<"."<<LBC::LBC_VERSION;
		ostringstream ostrRead;
		ostrRead<<lbvermjr<<"."<<lbver;
		errorList.push_back(EBadDeviceVersion::VersionInfo(this, "LBC_VERSION", ostrExp.str(), ostrRead.str()));
	}

	BoardBase::checkVersions();
}

void LinkBoard::fixTTCrxSettings() {
	if(getDescription() == "LB_RB+1_S1_BP1A_CH2") {//TODO removed when fixed !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		LOG4CPLUS_WARN(logger, __FUNCTION__<<" "<<getDescription() << " fixTTCrxSettings skipped for the board");
		return;
	}
	LOG4CPLUS_WARN(logger, __FUNCTION__<<" "<<getDescription() << " Trying to reset the TTCrx to recover from the wrong deskew clock settings");
	if(getMasterSlaveType() == msMaster) {
		getSynCoder().writeBits(SynCoder::BITS_GOL_TX_ENA, 0); //should disable the link receiver
	}
	else if (getMasterSlaveType() == msLeft) {
		LOG4CPLUS_WARN(logger, "disabling left slave on the master");
		getMaster()->getSynCoder().writeBits(SynCoder::BITS_LMUX_SLAVE0_ENA, 0);
		//getMaster()->getSynCoder().writeBits(SynCoder::BITS_LMUX_SLAVE0_CHECK_ENA, 0);
	}
	else if (getMasterSlaveType() == msRight) {
		LOG4CPLUS_WARN(logger, "disabling right slave on the master");
		getMaster()->getSynCoder().writeBits(SynCoder::BITS_LMUX_SLAVE1_ENA, 0);
		//getMaster()->getSynCoder().writeBits(SynCoder::BITS_LMUX_SLAVE1_CHECK_ENA, 0);
	}


	int rep = 3;
	for(int i = 0; i < rep; i++) {
		try {
			intiTTCrx(); //repeat the reset procedure, sets also the ControlReg

			getTtcRxI2c().WriteFineDelay1( getTtcRxI2c().GetFineDelay1() );
			getTtcRxI2c().WriteFineDelay2( getTtcRxI2c().GetFineDelay2() );
			break;
		}
		catch(TException& e) {
			if(i == rep-1) {
				LOG4CPLUS_WARN(logger, __FUNCTION__<<" "<<getDescription() << " error: "
						<< e.what() << ". Recovery failed. The LB will be disabled on the Master input or on the GOL");
				getSynCoder().addWarning(MonitorableStatus(MonitorItemName::TTCRX, MonitorableStatus::ERROR, "TTCrx error during fixTTCrxSettings", 0));
				return;
			}
			else {
				LOG4CPLUS_WARN(logger, __FUNCTION__<<" "<<getDescription() << " error: " << e.what() << ". Reseting the TTCrx and trying once again");
			}
		}
	}

	try {
		synCoder.resetQPLL();
	}
	catch(TException& e) {
		getSynCoder().addWarning(MonitorableStatus(MonitorItemName::QPLL, MonitorableStatus::ERROR, "QPLL not LOCKED after fixTTCrxSettings", 0));
		LOG4CPLUS_WARN(logger, __FUNCTION__<<" "<<getDescription() << " error: "
				<< e.what() << ". Recovery failed. The LB will be disabled on the Master input");
		return;
	}

	if(getMasterSlaveType() == msMaster) {
		try {
			resetGOL();
			getSynCoder().writeWord(SynCoder::WORD_SLAVE0_CHK_ERR_COUNT, 0, uint32_t(0));
			getSynCoder().writeWord(SynCoder::WORD_SLAVE1_CHK_ERR_COUNT, 0, uint32_t(0));
		}
		catch(TException& e) {
			getSynCoder().addWarning(MonitorableStatus(MonitorItemName::QPLL, MonitorableStatus::ERROR, "QPLL not LOCKED after fixTTCrxSettings", 0));
			LOG4CPLUS_WARN(logger, __FUNCTION__<<" "<<getDescription() << " error: "
					<< e.what() << ". Recovery failed. The LB will be disabled on the Master input");
			return;
		}
		tbsleep(100);
		getSynCoder().writeBits(SynCoder::BITS_GOL_TX_ENA, 1);
	}
	else if (getMasterSlaveType() == msLeft) {
		LOG4CPLUS_WARN(logger, "enabling left slave on the master");
		getMaster()->getSynCoder().writeBits(SynCoder::BITS_LMUX_SLAVE0_ENA, 1);
		//getMaster()->getSynCoder().writeBits(SynCoder::BITS_LMUX_SLAVE0_CHECK_ENA, 1);
		getMaster()->getSynCoder().writeWord(SynCoder::WORD_SLAVE0_CHK_ERR_COUNT, 0, uint32_t(0));
	}
	else if (getMasterSlaveType() == msRight) {
		LOG4CPLUS_WARN(logger, "enabling right slave on the master");
		getMaster()->getSynCoder().writeBits(SynCoder::BITS_LMUX_SLAVE1_ENA, 1);
		//getMaster()->getSynCoder().writeBits(SynCoder::BITS_LMUX_SLAVE1_CHECK_ENA, 1);
		getMaster()->getSynCoder().writeWord(SynCoder::WORD_SLAVE1_CHK_ERR_COUNT, 0, uint32_t(0));
	}

	LOG4CPLUS_WARN(logger, __FUNCTION__<<" "<<getDescription() <<" - done ");

}
}
