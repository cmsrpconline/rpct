#include "rpct/devices/TIIDummyAccess.h"
#include <assert.h>

using namespace log4cplus;

namespace rpct {

Logger TIIDummyAccess::logger = Logger::getInstance("TIIDummyAccess");
void TIIDummyAccess::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}

void TIIDummyAccess::Write(uint32_t address, void* data,
        size_t word_count, size_t word_size) {
    assert(word_size == 2);

    for (size_t i = 0; i < word_count; i++) {
        LOG4CPLUS_INFO(logger, "II write to " << deviceName_ << " addr "
                << std::hex << (address + i) << " data " << *(((unsigned short*)data) + 1));
    }
}

void TIIDummyAccess::Read(uint32_t address, void* data,
        size_t word_count, size_t word_size) {
    assert(word_size == 2);

    for (size_t i = 0; i < word_count; i++) {
        LOG4CPLUS_INFO(logger, "II read from " << deviceName_ << " addr "
                << std::hex << (address + i));
    }
}

}
