#include "rpct/devices/TIICCUAccess.h"
#include "rpct/lboxaccess/CCUException.h"
#include <assert.h>


using namespace log4cplus;
using namespace std;

namespace rpct {

Logger TIICCUAccess::logger = Logger::getInstance("TIICCUAccess");
void TIICCUAccess::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}
Logger TIILBCCUAccess::logger = Logger::getInstance("TIILBCCUAccess");
void TIILBCCUAccess::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}

void TIICCUAccess::Write(uint32_t address, void* data,
                         size_t word_count, size_t word_size) {
    assert(word_size == 2);
    
    for (size_t i = 0; i < word_count; i++) {
        LOG4CPLUS_DEBUG(logger, "II write lbox addr 0x" << std::hex << LBox->getCcuAddress() 
                << " addr " << (address + i) << " data " << *(((unsigned short*)data) + 1));
    }

    if (word_count == 1) {
        try {
            LBox->memoryWrite16(address, *(unsigned short*)data);
        }
        catch (CCUException& e) {
            // try once again                
            LOG4CPLUS_WARN(logger, "CCUException during Write at address " << address << " reseting FEC and trying once again");
            LBox->getFecManager().resetPlxFecForAutoRecovery();
            LBox->memoryWrite16(address, *(unsigned short*)data);                
        }
    }
    else {
        unsigned short* ccudata = new unsigned short[word_count];

        try {
            for (size_t i=0; i<word_count; i++) {
                ccudata[i] = *((unsigned short*)data+i);
            }

            try {
                LBox->memoryWrite16Block(address, ccudata, word_count);
            }
            catch (CCUException& e) {
                // try once again                
                LOG4CPLUS_WARN(logger, "CCUException during Write at address " << address << " reseting FEC and trying once again");
                LBox->getFecManager().resetPlxFecForAutoRecovery();
                LBox->memoryWrite16Block(address, ccudata, word_count);                
            }
                
        }
        catch(...) {
            delete [] ccudata;
            throw;
        };
        delete [] ccudata;
    }
}


void TIICCUAccess::Read(uint32_t address, void* data,
                        size_t word_count, size_t word_size) {
    assert(word_size == 2);

    for (size_t i = 0; i < word_count; i++) {
        LOG4CPLUS_DEBUG(logger, "II read lbox addr 0x" << std::hex << LBox->getCcuAddress() 
                << " addr " << (address + i));
    }
    
    
    if (word_count == 1) {
        try {
            *(unsigned short*)data = LBox->memoryRead16(address);
        }
        catch (CCUException& e) {
            // try once again                
            LOG4CPLUS_WARN(logger, "CCUException during Write at address " << address << " reseting FEC and  trying once again");
            LBox->getFecManager().resetPlxFecForAutoRecovery();
            *(unsigned short*)data = LBox->memoryRead16(address);                
        }
    }
    else {
        unsigned short* ccudata = new unsigned short[word_count];

        try {
            try {
                LBox->memoryRead16Block(address, ccudata, word_count);
            }
            catch (CCUException& e) {
                // try once again                
                LOG4CPLUS_WARN(logger, "CCUException during Read from address " << address << " reseting FEC and  trying once again");
                LBox->getFecManager().resetPlxFecForAutoRecovery();
                LBox->memoryRead16Block(address, ccudata, word_count);
            }
            
            for (size_t i = 0; i < word_count; i++) {
                *(((unsigned short*) data)+i) = ccudata[i];
            }
        }
        catch(...) {
            delete [] ccudata;
            throw;
        };
        delete [] ccudata;
    }
}




void TIILBCCUAccess::Write(uint32_t address, void* data,
                           size_t word_count, size_t word_size)
{
    assert(word_size == 2);

    LOG4CPLUS_DEBUG(logger, "Write address=" << hex << address << " word_count=" << word_count << " word_size=" << word_size);
    for (size_t i = 0; i < word_count; i++) {
        LOG4CPLUS_DEBUG(logger, "II write lb  " << std::hex << LB.getDescription() 
                << " addr " << (address + i) << " data " << *(((unsigned short*)data) + i));
    }
    
    /*for (size_t i = 0; i < word_count * word_size; i++) {
        LOG4CPLUS_DEBUG(logger, "II write lb by char" << std::hex << LB.getDescription() 
                << " index " << i << " data " << ((unsigned int)*(((unsigned char*)data) + i)));
    }*/
    
    if (word_count == 1) {
        try {
            LBox->lbWrite(ModuleAddr, address, *(unsigned short*)data);
        }
        catch (CCUException& e) {
            // try once again                
            LOG4CPLUS_WARN(logger, "CCUException during TIILBCCUAccess::Write from address " << address << " reseting FEC and trying once again");
            LBox->getFecManager().resetPlxFecForAutoRecovery();
            LBox->lbWrite(ModuleAddr, address, *(unsigned short*)data);
        }
    }
    else {
        unsigned short* ccudata = new unsigned short[word_count];

        try {
            for (size_t i=0; i<word_count; i++) {
                ccudata[i] = *(((unsigned short*)data)+i); // TODO sprawdzic czy tu nie powinnismy skakac co 2?
            }


            uint32_t pagePos;
            uint32_t words;
            unsigned short* ccudataptr = ccudata;
            for (uint32_t wc = word_count; wc > 0; wc -= words) {
                pagePos = address % PageSize;
                words = ((pagePos + wc) > PageSize) ? (PageSize - pagePos) : wc;
                try {
                    LBox->lbWriteBlock(ModuleAddr, address, ccudataptr, words);
                }
                catch (CCUException& e) {
                    // try once again                
                    LOG4CPLUS_WARN(logger, "CCUException during TIILBCCUAccess::Write from address " << address << " reseting FEC and trying once again");
                    LBox->getFecManager().resetPlxFecForAutoRecovery();
                    LBox->lbWriteBlock(ModuleAddr, address, ccudataptr, words);
                }
                
                address += words;
                ccudataptr += words;
            }
        }
        catch(...) {
            delete [] ccudata;
            throw;
        };
        delete [] ccudata;
    }
}


void TIILBCCUAccess::Read(uint32_t address, void* data,
                          size_t word_count, size_t word_size)
{
    assert(word_size == 2);

    for (size_t i = 0; i < word_count; i++) {
        LOG4CPLUS_DEBUG(logger, "II read lb " << std::hex << LB.getDescription() 
                << " addr " << (address + i));
    }
    
    
    if (word_count == 1) {
        try {
            *(unsigned short*)data = LBox->lbRead(ModuleAddr, address);
        }
        catch (CCUException& e) {
            // try once again                
            LOG4CPLUS_WARN(logger, "CCUException during TIILBCCUAccess::Read from address " << address << " reseting FEC and trying once again");
            LBox->getFecManager().resetPlxFecForAutoRecovery();
            *(unsigned short*)data = LBox->lbRead(ModuleAddr, address);
        }        
    }
    else {
        unsigned short* ccudata = new unsigned short[word_count];

        try {

            uint32_t pagePos;
            uint32_t words;
            unsigned short* ccudataptr = ccudata;
            for (uint32_t wc = word_count; wc > 0; wc -= words) {
                pagePos = address % PageSize;
                words = ((pagePos + wc) > PageSize) ? (PageSize - pagePos) : wc;
                try {
                    LBox->lbReadBlock(ModuleAddr, address, ccudataptr, words);
                }
                catch (CCUException& e) {
                    // try once again                
                    LOG4CPLUS_WARN(logger, "CCUException during TIILBCCUAccess::Read from address " << address << " reseting FEC and trying once again");
                    LBox->getFecManager().resetPlxFecForAutoRecovery();
                    LBox->lbReadBlock(ModuleAddr, address, ccudataptr, words);
                }  
                address += words;
                ccudataptr += words;
            }

            for (size_t i = 0; i < word_count; i++) {
                *(((unsigned short*) data)+i) = ccudata[i]; // TODO sprawdzic czy tu nie powinnismy skakac co 2?
            }
        }
        catch(...) {
            delete [] ccudata;
            throw;
        };
        delete [] ccudata;
    }
}


}
