#include "rpct/devices/LinkBoxConfigurator.h"


using namespace std;
using namespace log4cplus;

namespace rpct {

Logger LinkBoxConfigurator::logger = Logger::getInstance("LinkBoxConfigurator");
void LinkBoxConfigurator::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}

}
