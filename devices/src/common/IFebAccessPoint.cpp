#include "rpct/devices/IFebAccessPoint.h"

#include "rpct/ii/feb_const.h"
#include "rpct/devices/FebSystem.h"

#include "rpct/ii/i2c/II2cAccess.h"

#include "rpct/devices/feb_const.h"

namespace rpct {

log4cplus::Logger IFebAccessPoint::logger_ = log4cplus::Logger::getInstance("IFebAccessPoint");
void IFebAccessPoint::setupLogger(std::string namePrefix) {
	logger_ = log4cplus::Logger::getInstance(namePrefix + "." + logger_.getName());
}
HardwareItemType * IFebAccessPoint::type_ = 0;
IMonitorable::MonitorItems * IFebAccessPoint::mitems_ = 0;

IFebAccessPoint::IFebAccessPoint(FebSystem & system
                                 , rpct::i2c::II2cAccess & i2c_access
                                 , int id
                                 , const std::string & description
                                 , int position
                                 , bool disabled)
    : FebSystemItem(system, position, &system)
    , id_(id)
    , description_(description)
    , i2c_access_(i2c_access)
    , position_(position)
{
    disable(disabled);
    if (!type_)
        type_ = new HardwareItemType(HardwareItemType::cBoard
                                     , type::feb::febaccesspoint_);
    if (!mitems_)
        {
            mitems_ = new MonitorItems();
            mitems_->push_back(monitoritem::feb::pcf8574a_);
            mitems_->push_back(monitoritem::feb::temp_);
            mitems_->push_back(monitoritem::feb::vth_);
            mitems_->push_back(monitoritem::feb::vmon_);
        }

    addToSystem();
}
IFebAccessPoint::~IFebAccessPoint()
{
    removeFromSystem();
    fdbs_type::iterator iFDBsEnd = fdbs_.end();
    for (fdbs_type::iterator iFDB = fdbs_.begin()
             ; iFDB != iFDBsEnd
             ; ++iFDB)
        delete iFDB->second;
}

void IFebAccessPoint::reset()
{
    FebSystemItem::reset();
    fdbs_type::iterator iFDBsEnd = fdbs_.end();
    for (fdbs_type::iterator iFDB = fdbs_.begin()
             ; iFDB != iFDBsEnd
             ; ++iFDB)
        iFDB->second->reset();
}

void IFebAccessPoint::configure(ConfigurationSet* configurationSet, int configureFlags)
{
    LOG4CPLUS_DEBUG(logger_, "configure()");
    if (!isDisabled())
        {
            fdbs_type::iterator iFdbsEnd = fdbs_.end();
            for (fdbs_type::iterator iFdb = fdbs_.begin()
                     ; iFdb != iFdbsEnd
                     ; ++iFdb)
                iFdb->second->configure(configurationSet, configureFlags);
            configured();
            log("configured");
        }
    else
        {
            LOG4CPLUS_DEBUG(logger_, *this << " was not configured, it's disabled.");
        }
}
void IFebAccessPoint::configure()
{
    LOG4CPLUS_DEBUG(logger_, "configure()");
    if (!isDisabled())
        {
            fdbs_type::iterator iFdbsEnd = fdbs_.end();
            for (fdbs_type::iterator iFdb = fdbs_.begin()
                     ; iFdb != iFdbsEnd
                     ; ++iFdb)
                iFdb->second->configure();
            configured();
            log("configured");
        }
    else
        {
            LOG4CPLUS_DEBUG(logger_, *this << " was not configured, it's disabled.");
        }
}

void IFebAccessPoint::loadConfiguration(ConfigurationSet* configurationSet, int configureFlags)
{
    LOG4CPLUS_DEBUG(logger_, "loadConfiguration()");
    if (!isDisabled())
        {
            fdbs_type::iterator iFdbsEnd = fdbs_.end();
            for (fdbs_type::iterator iFdb = fdbs_.begin()
                     ; iFdb != iFdbsEnd
                     ; ++iFdb)
                iFdb->second->loadConfiguration(configurationSet, configureFlags);
            configured(0);
            unlog("configured");
        }
    else
        {
            LOG4CPLUS_DEBUG(logger_, "configuration for " << *this << " was not loaded, it's disabled.");
        }
}

void IFebAccessPoint::monitor(MonitorItems & items)
{
    if (!isDisabled())
        {
            fdbs_type::iterator iFdbsEnd = fdbs_.end();
            for (fdbs_type::iterator iFdb = fdbs_.begin()
                     ; iFdb != iFdbsEnd
                     ; ++iFdb)
                iFdb->second->monitor(items);
            log("monitored");
        }
    else
        {
            LOG4CPLUS_DEBUG(logger_, *this << " was not monitored, it's disabled.");
        }
}

void IFebAccessPoint::printParameters(tools::Parameters & parameters) const
{
    parameters.add("position", position_);
}

FebDistributionBoard & IFebAccessPoint::addFebDistributionBoard(int id
                                                                , const std::string & description
                                                                , unsigned short channel
                                                                , bool dt
                                                                , unsigned char pca9544_address_offset
                                                                , unsigned char dt_channel
                                                                , bool disabled)
{
    int position = FebDistributionBoard::makePosition(channel, pca9544_address_offset, dt_channel);
    fdbs_type::iterator iFdb = fdbs_.find(position);
    if (iFdb == fdbs_.end())
        return *((fdbs_.insert(std::pair<int, FebDistributionBoard * >
                               (position
                                , new FebDistributionBoard(*this
                                                           , i2c_access_
                                                           , id, description
                                                           , channel
                                                           , dt
                                                           , pca9544_address_offset
                                                           , dt_channel
                                                           , disabled)
                                )
                               )).first->second);
    else
        return *(iFdb->second);
}

} // namespace rpct
