//---------------------------------------------------------------------------
//
//  Michal Pietrusinski 2001
//  Warsaw University
//
#include "rpct/std/NullPointerException.h"
#include "rpct/devices/FebEndcap.h"
#include "rpct/std/ENotImplemented.h"
#include <math.h>
//---------------------------------------------------------------------------



namespace rpct {


FebEndcap::FebEndcap(DistributionBoard& distributionBoard, unsigned int ln)
        : Feb(distributionBoard, ln), dac2Enabled(false) {
    // the following is previously called in the base constructor, 
    // but FebEndcap is not yet accessible, so 
    // Feb::convertLocalNumber is used instead of FebEndcap::convertLocalNumber
    convertedLocalNumber = convertLocalNumber(localNumber);
}

FebEndcap::~FebEndcap() {
}

unsigned int FebEndcap::convertLocalNumber(unsigned int localNumber) {
    return localNumber << 1;
}

float FebEndcap::readTemperature2() {
    //checkI2C();
    //unsigned int convertedLocalNumber = convertAddr(addr);
    //return AD7417_RT(addr);
    // Write Address point Reg, for CONF_REG Write &  T Select
    // Write 2 bytes "0x1" and "0x0" to the addres "0x28+nboard"
    // (where 0 <= nboard <= 5))

    //getI2C().Write8(0x28 + addr, 0x1);
    //getI2C().Write8(0x28 + addr, 0x0); // xx
    getI2C().WriteADD (ADDR_AD7417_2 + convertedLocalNumber, 0x1, 0x0);
    // ADC Channel Selection: Write Address point Reg, for T-READ
    //Write the byte "0x0" to the addres "0x28+nboard"
    getI2C().Write8 (ADDR_AD7417_2 + convertedLocalNumber, 0x0);


    //Read T (2 Bytes) from "0x28+nboard" and convert into degrees:
    //Word_read ="0xXXXX"    T= ((Word_read & 0xFFC0) >> 6)*0.25;
    unsigned char val1, val2;
    getI2C().ReadADD(ADDR_AD7417_2 + convertedLocalNumber, val1, val2);

    unsigned int val = val1;
    //val = (val << 8) && 0xff;
    val = (val2 & 0xff) | ((val << 8) & 0xff00);

    return ((val >> 6) & 0x3ff) * 0.25;
}

float FebEndcap::readAD7417_2_ADC(unsigned int chan) {
    //checkI2C();
    //unsigned int convertedLocalNumber = convertAddr(addr);
    getI2C().WriteADD(ADDR_AD7417_2 + convertedLocalNumber, 0x1, chan);
    sleep();
    getI2C().Write8(ADDR_AD7417_2 + convertedLocalNumber, 0x4);
    sleep();


    unsigned char val1, val2;
    getI2C().ReadADD(ADDR_AD7417_2 + convertedLocalNumber, val1, val2);
    sleep();

    unsigned int val = val1;
    //val = (val << 8) && 0xff;
    val = (val2 & 0xff) | ((val << 8) & 0xff00);

    return ((val >> 6) & 0x3ff) * 2.5/1.024;
}


void FebEndcap::writeAD5316_2_DAC(unsigned int wthp, float value) {
    //selectDAC2(true);

    unsigned int ivalue = (unsigned int) (value * 1.024/5);
    unsigned char data[3];
    data[0] = wthp;
    data[1] = ((ivalue & 0x3c0) >> 6) + 0x70; //((svalue >> 6) & 0xf) + 0x70;
    data[2] = (ivalue & 0x3f) << 2; //(svalue << 2) & 0xfc;
    getI2C().Write(0xC, data, 3);
    //EnableDAC(nboard, false);
    //selectDAC2(false);
    /*
    wthmsb = ((nvth & 0x3c0) >> 6) + 0x70;
    wthlsb = (nvth & 0x3f) << 2;
    */
}


float FebEndcap::readVTH3() {
    //checkI2C();
    return readAD7417_2_ADC(CHAN_VTH1);
}

void FebEndcap::writeVTH3(float val) {
    //checkI2C();
    /*set_controlreg(convertedLocalNumber,2,2,0);
    AD5316_WDAC_FB(addr,2,val);
    set_controlreg(addr,2,2,1);*/
    selectDAC2(true);
    writeAD5316_2_DAC(WTHP_VTH1, val);
    if (autoCorrection) {
    	float rvalue = readVTH3();
        writeAD5316_2_DAC(WTHP_VTH1, 2 * val - rvalue);
    }
    selectDAC2(false);
}

float FebEndcap::readVTH4() {
    //checkI2C();
    return readAD7417_2_ADC(CHAN_VTH2);
}

void FebEndcap::writeVTH4(float val) {
    //checkI2C();
    /*set_controlreg(addr,2,2,0);
    AD5316_WDAC_FB(addr,2,val);
    set_controlreg(addr,2,2,1);*/
    selectDAC2(true);
    writeAD5316_2_DAC(WTHP_VTH2, val);
    if (autoCorrection) {
    	float rvalue = readVTH4();
        writeAD5316_2_DAC(WTHP_VTH2, 2 * val - rvalue);
    }
    selectDAC2(false);
}


float FebEndcap::readVMon3() {
    //checkI2C();
    //return AD7417_RADC(addr,3); /* Read VMON2 */
    return readAD7417_2_ADC(CHAN_VMON1) * 2;
}

void FebEndcap::writeVMon3(float val) {
    //checkI2C();
    /*set_controlreg(addr,2,2,0);
    AD5316_WDAC_FB(addr,4,val);
    set_controlreg(addr,2,2,1);*/ 
    selectDAC2(true);
    writeAD5316_2_DAC(WTHP_VMON1, val);
    if (autoCorrection) {
    	float rvalue = readVMon3();
        writeAD5316_2_DAC(WTHP_VMON1, 2 * val - rvalue);
    }
    selectDAC2(false);
}


float FebEndcap::readVMon4() {
    //checkI2C();
    //return AD7417_RADC(addr,3); /* Read VMON2 */
    return readAD7417_2_ADC(CHAN_VMON2) * 2;
}

void FebEndcap::writeVMon4(float val) {
    //checkI2C();
    /*set_controlreg(addr,2,2,0);
    AD5316_WDAC_FB(addr,4,val);
    set_controlreg(addr,2,2,1);*/ 
    selectDAC2(true);
    writeAD5316_2_DAC(WTHP_VMON2, val);
    if (autoCorrection) {
    	float rvalue = readVMon4();
        writeAD5316_2_DAC(WTHP_VTH2, 2 * val - rvalue);
    }
    selectDAC2(false);
}



unsigned char FebEndcap::readPCF8574A_CR() {
    //checkI2C();
    //unsigned int convertedLocalNumber = convertAddr(addr);
    return getI2C().Read8(ADDR_PCF8574A_CR + convertedLocalNumber);
}

void FebEndcap::writePCF8574A_CR(unsigned int value) {
    //checkI2C();
    //unsigned int convertedLocalNumber = convertAddr(addr);
    getI2C().Write8(ADDR_PCF8574A_CR + convertedLocalNumber, value);
}

void FebEndcap::enableDAC(bool val) {
    //checkI2C();
    //en_thres(addr,(val == 0) ? 1 : 0);    /* Enable/disable DAC */
    unsigned char value = readPCF8574A_CR();
    if (val) {
        writePCF8574A_CR(value & (~EN_DAC1));
    }
    else {
        writePCF8574A_CR(value | EN_DAC1);
    }
    dacEnabled = value;
}

void FebEndcap::enableDAC2(bool val) {
    //checkI2C();
    //en_thres(addr,(val == 0) ? 1 : 0);    /* Enable/disable DAC */
    unsigned char value = readPCF8574A_CR();
    if (val) {
        writePCF8574A_CR(value & (~EN_DAC2));
    }
    else {
        writePCF8574A_CR(value | EN_DAC2);
    }
    dac2Enabled = value;
}

void FebEndcap::selectDAC(bool val) {
    //checkI2C();
    unsigned char value = readPCF8574A_CR();
    if (val) {
        writePCF8574A_CR(value & (~SEL_DAC1));
    }
    else {
        writePCF8574A_CR(value | SEL_DAC1);
    }
}

void FebEndcap::selectDAC2(bool val) {
    //checkI2C();
    unsigned char value = readPCF8574A_CR();
    if (val) {
        writePCF8574A_CR(value & (~SEL_DAC2));
    }
    else {
        writePCF8574A_CR(value | SEL_DAC2);
    }
}


void FebEndcap::write2(float* vth3, float* vth4, float* vmon3, float* vmon4) {
    selectDAC2(true);
    try {
        if (vth3 != 0) {
            writeAD5316_2_DAC(WTHP_VTH1, *vth3);
            if (autoCorrection) {               
                float rvalue = readVTH3();
                if (fabs(rvalue - *vth3) > MAX_VTH_DIFF) {
                    writeAD5316_2_DAC(WTHP_VTH1, 2 * *vth3 - rvalue);
                }
            } 
        }
        if (vth4 != 0) {
            writeAD5316_2_DAC(WTHP_VTH2, *vth4);
            if (autoCorrection) {               
                float rvalue = readVTH4();
                if (fabs(rvalue - *vth4) > MAX_VTH_DIFF) {
                    writeAD5316_2_DAC(WTHP_VTH2, 2 * *vth4 - rvalue);
                }
            }  
        }
        if (vmon3 != 0) {
            writeAD5316_2_DAC(WTHP_VMON1, *vmon3);
            if (autoCorrection) {               
                float rvalue = readVMon3();
                if (fabs(rvalue - *vmon3) > MAX_VMON_DIFF) {
                    writeAD5316_2_DAC(WTHP_VMON1, 2 * *vmon3 - rvalue);
                }
            }    
        }
        if (vmon4 != 0) {
            writeAD5316_2_DAC(WTHP_VMON2, *vmon4);
            if (autoCorrection) {               
                float rvalue = readVMon4();
                if (fabs(rvalue - *vmon4) > MAX_VMON_DIFF) {
                    writeAD5316_2_DAC(WTHP_VMON2, 2 * *vmon4 - rvalue);
                }
            } 
        }
        selectDAC2(false);
    }
    catch(...) {
        selectDAC2(false);
        throw;
    }
}


void FebEndcap::writeRead2(float* vth3, float* vth4, float* vmon3, float* vmon4) {
    selectDAC2(true);
    try {
        if (vth3 != 0) {
            writeAD5316_2_DAC(WTHP_VTH1, *vth3);
            if (autoCorrection) {               
                float rvalue = readVTH3();
                if (fabs(rvalue - *vth3) > MAX_VTH_DIFF) {
                    writeAD5316_2_DAC(WTHP_VTH1, 2 * *vth3 - rvalue);
                    rvalue = readVTH3();                    
                }
                *vth3 = rvalue;
            }   
            else {
                *vth3 = readVTH3();
            }
        }
        if (vth4 != 0) {
            writeAD5316_2_DAC(WTHP_VTH2, *vth4);
            if (autoCorrection) {               
                float rvalue = readVTH4();
                if (fabs(rvalue - *vth4) > MAX_VTH_DIFF) {
                    writeAD5316_2_DAC(WTHP_VTH2, 2 * *vth4 - rvalue);
                    rvalue = readVTH4();
                }
                *vth4 = rvalue;
            }     
            else {
                *vth4 = readVTH4();
            }
        }
        if (vmon3 != 0) {
            writeAD5316_2_DAC(WTHP_VMON1, *vmon3);
            if (autoCorrection) {               
                float rvalue = readVMon3();
                if (fabs(rvalue - *vmon3) > MAX_VMON_DIFF) {
                    writeAD5316_2_DAC(WTHP_VMON1, 2 * *vmon3 - rvalue);
                    rvalue = readVMon3();
                }
                *vmon3 = rvalue;
            }    
            else {
                *vmon3 = readVMon3();
            } 
        }
        if (vmon4 != 0) {
            writeAD5316_2_DAC(WTHP_VMON2, *vmon4);
            if (autoCorrection) {               
                float rvalue = readVMon4();
                if (fabs(rvalue - *vmon4) > MAX_VMON_DIFF) {
                    writeAD5316_2_DAC(WTHP_VMON2, 2 * *vmon4 - rvalue);
                    rvalue = readVMon4();
                }
                *vmon4 = rvalue;
            }  
            else {
                *vmon4 = readVMon4();
            }  
        }
        selectDAC2(false);
    }
    catch(...) {
        selectDAC2(false);
        throw;
    }
}
    
    void FebEndcap::writeReadOffset2(float & vth3, float & vth4
                                     , float & vtho3, float & vtho4
                                     , float & vmon3, float & vmon4
                                     , float & vmono3, float & vmono4
                                     , const bool & correct_vth, const bool & correct_vmon
                                     , const bool & read_vth, const bool & read_vmon)
    {
        selectDAC(true);
        try {
            float rvalue, diff;
            if (vth3 > 0) {
                writeAD5316_2_DAC(WTHP_VTH1, vth3 + vtho3);
                if (correct_vth) {
                    rvalue = readVTH3();
                    if (fabs(diff = vth3 - rvalue) > MAX_VTH_DIFF) {
                        vtho3 += diff;
                        writeAD5316_2_DAC(WTHP_VTH1, vth3 + vtho3);
                        if (read_vth)
                            vth3 = readVTH3();
                    }
                    else if (read_vth)
                        vth3 = rvalue;
                }
                else if (read_vth)
                    vth3 = readVTH3();
            }
            if (vth4 > 0) {
                writeAD5316_2_DAC(WTHP_VTH2, vth4 + vtho4);
                if (correct_vth) {
                    rvalue = readVTH4();
                    if (fabs(diff = vth4 - rvalue) > MAX_VTH_DIFF) {
                        vtho4 += diff;
                        writeAD5316_2_DAC(WTHP_VTH2, vth4 + vtho4);
                        if (read_vth)
                            vth4 = readVTH4();
                    }
                    else if (read_vth)
                        vth4 = rvalue;
                }
                else if (read_vth)
                    vth4 = readVTH4();
            }
            if (vmon3 > 0) {
                writeAD5316_2_DAC(WTHP_VMON1, vmon3 + vmono3);
                if (correct_vmon) {
                    rvalue = readVMon3();
                    if (fabs(diff = vmon3 - rvalue) > MAX_VMON_DIFF) {
                        vmono3 += diff;
                        writeAD5316_2_DAC(WTHP_VMON1, vmon3 + vmono3);
                        if (read_vmon)
                            vmon3 = readVMon3();
                    }
                    else if (read_vmon)
                        vmon3 = rvalue;
                }
                else if (read_vmon)
                    vmon3 = readVMon3();
            }
            if (vmon4 > 0) {
                writeAD5316_2_DAC(WTHP_VMON2, vmon4 + vmono4);
                if (correct_vmon) {
                    rvalue = readVMon4();
                    if (fabs(diff = vmon4 - rvalue) > MAX_VMON_DIFF) {
                        vmono4 += diff;
                        writeAD5316_2_DAC(WTHP_VMON2, vmon4 + vmono4);
                        if (read_vmon)
                            vmon4 = readVMon4();
                    }
                    else if (read_vmon)
                        vmon4 = rvalue;
                }
                else if (read_vmon)
                    vmon4 = readVMon4();
            }
            selectDAC(false);
	}
	catch(...) {
            selectDAC(false);
            throw;
	}
    }

} // namespace
