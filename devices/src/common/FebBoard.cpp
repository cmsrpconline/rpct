#include "rpct/devices/FebBoard.h"

#include <sstream>

#include "rpct/ii/feb_const.h"
#include "rpct/devices/FebSystem.h"

#include "rpct/ii/ConfigurationFlags.h"

#include "rpct/ii/i2c/II2cAccess.h"

#include "rpct/devices/feb_const.h"
#include "rpct/devices/IFebAccessPoint.h"
#include "rpct/devices/FebDistributionBoard.h"

namespace rpct {

log4cplus::Logger FebBoard::logger_ = log4cplus::Logger::getInstance("Feb.FebBoard");
void FebBoard::setupLogger(std::string namePrefix) {
	logger_ = log4cplus::Logger::getInstance(namePrefix + "." + logger_.getName());
}

HardwareItemType * FebBoard::type_ = 0;
IMonitorable::MonitorItems * FebBoard::mitems_ = 0;

FebBoard::FebBoard(FebDistributionBoard & febdistributionboard
                   , rpct::i2c::II2cAccess & i2c_access
                   , int id
                   , const std::string & description
                   , unsigned short channel
                   , bool febtype
                   , unsigned char position
                   , bool disabled)
    : FebSystemItem(febdistributionboard.getSystem(), position, &febdistributionboard)
    , id_(id)
    , description_(description)
    , i2c_access_(i2c_access)
    , febdistributionboard_(febdistributionboard)
    , channel_(channel)
    , febtype_(febtype?1:0)
    , address_(pos2address(position, febtype))
    , pcf8574a_register_(febtype)
    , is_hardware_accessed_(false)
{
    disable(disabled);
    if (!type_)
        type_ = new HardwareItemType(HardwareItemType::cBoard
                                     , type::feb::febboard_);
    if (!mitems_)
        {
            mitems_ = new MonitorItems();
            mitems_->push_back(monitoritem::feb::pcf8574a_);
            mitems_->push_back(monitoritem::feb::temp_);
            mitems_->push_back(monitoritem::feb::vth_);
            mitems_->push_back(monitoritem::feb::vmon_);
        }
    pcf8574a_read_response_ = new rpct::i2c::TI2cResponse<FebBoard>(this
                                                                    , &FebBoard::i2c_read_pcf8574a_ready
                                                                    , &FebBoard::i2c_read_pcf8574a_error);
    pcf8574a_write_response_ = new rpct::i2c::TI2cResponse<FebBoard>(this
                                                                     , &FebBoard::i2c_write_pcf8574a_ready
                                                                     , &FebBoard::i2c_write_pcf8574a_error);
    fps_[0] = 0;
    fps_[1] = 0;

    addToSystem();
}

FebBoard::~FebBoard()
{
    removeFromSystem();
    delete pcf8574a_read_response_;
    delete pcf8574a_write_response_;
    for (int part = 0 ; part < nparts() ; ++part)
        if (fps_[part])
            delete fps_[part];
}

void FebBoard::reset()
{
    FebSystemItem::reset();
    IRecursive::reset();
    pcf8574a_register_.reset();
    for (int part = 0 ; part < nparts() ; ++part)
        if (fps_[part])
            fps_[part]->reset();
    is_hardware_accessed_ = false;
}

void FebBoard::configure(ConfigurationSet* configurationSet, int configureFlags)
{
    LOG4CPLUS_DEBUG(logger_, "configure()");
    if (!getSystem().getInterrupt() && ((configureFlags & ConfigurationFlags::INCLUDE_DISABLED)  || !isDisabled()))
        {
            bool only_cold = (configureFlags & rpct::ConfigurationFlags::ONLY_COLD);

            is_hardware_accessed_ = false;
            {
                RecursiveAction ra(this);

                if (only_cold && !febdistributionboard_.isRecursive())
                    { // for recursive, this is done by the FDB to profit from the bi2c
                        readPCF8574a();
                        i2c_access_.execute();
                    }
                if (only_cold)
                    pcf8574a_register_.out2in();
                // Confusing behaviour that's actually not used:
                //else
                //    pcf8574a_register_.reset_in();

                if (!only_cold || !isWarm())
                    {
                        for (int part = 0 ; part < nparts() ; ++part)
                            if (fps_[part])
                                fps_[part]->configure(configurationSet, configureFlags);
                        log("configured");
                    }
            }
            disableFebAccess();
        }
    else
        LOG4CPLUS_DEBUG(logger_, *this << " was not configured, it's disabled.");
}

void FebBoard::configure(int configureFlags)
{
    LOG4CPLUS_DEBUG(logger_, "configure()");
    if (!getSystem().getInterrupt() && ((configureFlags & ConfigurationFlags::INCLUDE_DISABLED) || !isDisabled()))
        {
            is_hardware_accessed_ = false;
            {
                RecursiveAction ra(this);

                pcf8574a_register_.reset_in();

                for (int part = 0 ; part < nparts() ; ++part)
                    if (fps_[part])
                        fps_[part]->configure();
            }
            disableFebAccess();
            log("configured");
        }
    else
        LOG4CPLUS_DEBUG(logger_, *this << " was not configured, it's disabled.");
}

void FebBoard::loadConfiguration(ConfigurationSet* configurationSet, int configureFlags)
{
    LOG4CPLUS_DEBUG(logger_, "loadConfiguration()");
    if (!getSystem().getInterrupt() && ((configureFlags & ConfigurationFlags::INCLUDE_DISABLED) || !isDisabled()))
        {
            for (int part = 0 ; part < nparts() ; ++part)
                if (fps_[part])
                    fps_[part]->loadConfiguration(configurationSet, configureFlags);
            unlog("configured");
        }
    else
        LOG4CPLUS_DEBUG(logger_, "Configuration for " << *this << " was not loaded, it's disabled.");
}

void FebBoard::monitor(MonitorItems & items, int configureFlags)
{
    LOG4CPLUS_DEBUG(logger_, "FebBoard::monitor()");
    if (!getSystem().getInterrupt() && ((configureFlags & ConfigurationFlags::INCLUDE_DISABLED) || !isDisabled()))
        {
            is_hardware_accessed_ = false;
            {
                RecursiveAction ra(this);

                MonitorItems::iterator ImiEnd = items.end();
                MonitorItems::iterator Imi = std::find(items.begin(), ImiEnd, monitoritem::feb::pcf8574a_);
                if (Imi != ImiEnd)
                    readPCF8574a();
                for (int part = 0 ; part < nparts() ; ++part)
                    if (fps_[part])
                        fps_[part]->monitor(items, configureFlags);
            }
            disableFebAccess();
            log("monitored");
        }
    else
        LOG4CPLUS_DEBUG(logger_, *this << " not monitored, it's disabled.");
}

void FebBoard::printParameters(tools::Parameters & parameters) const
{
    parameters.add("channel/port", channel_);
    parameters.add("febtype", (febtype_?"endcap":"barrel"));
    parameters.hexadd("address", address_);
    parameters.hexadd("pcf8574a write", *(pcf8574a_register_.in()));
    parameters.hexadd("pcf8574a read", *(pcf8574a_register_.out()));
}

FebPart & FebBoard::addFebPart(int id
                               , unsigned char position
                               , bool disabled)
    throw (rpct::exception::SystemException)
{
    LOG4CPLUS_DEBUG(logger_, "FebBoard::addFebPart " << id << " " << (int)position);
    if (position < nparts())
        {
            if (!fps_[position])
                fps_[position] = new FebPart(*this, i2c_access_, position, id, disabled);
            return *(fps_[position]);
        }
    else
        {
            std::stringstream error("FebPart ", std::ios_base::out | std::ios_base::ate);
            error << position << " out of range for " << *this;
            throw rpct::exception::SystemException(error.str());
        }
}

FebChip & FebBoard::addFebChip(int id, unsigned char chip, bool disabled)
    throw (rpct::exception::SystemException)
{
    LOG4CPLUS_DEBUG(logger_, "FebBoard::addFebChip");
    return addFebPart(id, chip2part(chip), isDisabled()).addFebChip(id, chip2position(chip), disabled);
}

void FebBoard::enableFebAccess(bool force)
{
    febdistributionboard_.enableFebAccess(force);
}

void FebBoard::disableFebAccess(bool force)
{
    if (force || !recursive_)
        {
            if (is_hardware_accessed_)
                {
                    is_hardware_accessed_ = false;
                    enableFebAccess();
                    i2c_access_.write(channel_, rpct::devices::feb::preset::pcf8574a_ + address_
                                      , &pcf8574a_register_, pcf8574a_write_response_);
                }
            febdistributionboard_.disableFebAccess(force);
        }
}

void FebBoard::selectPart(unsigned char position, bool select)
    throw (rpct::exception::SystemException)
{
    if (position < nparts())
        {
            is_hardware_accessed_ = true;
            pcf8574a_register_.select(position, select);
        }
    else
        {
            std::stringstream error("FebPart ", std::ios_base::out | std::ios_base::ate);
            error << position << " out of range for " << *this;
            throw rpct::exception::SystemException(error.str());
        }
}
void FebBoard::enablePart(unsigned char position, bool enable)
    throw (rpct::exception::SystemException)
{
    if (position < nparts())
        {
            is_hardware_accessed_ = true;
            pcf8574a_register_.enable(position, enable);
        }
    else
        {
            std::stringstream error("FebPart ", std::ios_base::out | std::ios_base::ate);
            error << position << " out of range for " << *this;
            throw rpct::exception::SystemException(error.str());
        }
}

bool FebBoard::readPCF8574a()
{
    bool returnvalue = false;
    {
        RecursiveAction ra(this);

        enableFebAccess();
        if (is_hardware_accessed_)
            writePCF8574a(true);
        returnvalue = i2c_access_.read(channel_, rpct::devices::feb::preset::pcf8574a_ + address_
                                       , &pcf8574a_register_, pcf8574a_read_response_);
    }
    disableFebAccess();

    return returnvalue;
}
bool FebBoard::writePCF8574a(bool force)
{
    bool returnvalue = false;
    if (force || !recursive_)
        {
            is_hardware_accessed_ = false;
            enableFebAccess();
            returnvalue = i2c_access_.write(channel_, rpct::devices::feb::preset::pcf8574a_ + address_
                                            , &pcf8574a_register_, pcf8574a_write_response_);
            disableFebAccess();
        }
    return returnvalue;
}

void FebBoard::i2c_read_pcf8574a_error(rpct::exception::Exception const * e)
    throw ()
{
    // we don't know if it's warm
    warm(0, 0);
    for (int part = 0 ; part < nparts() ; ++part)
        if (fps_[part])
            fps_[part]->warm(0, 0);
    log("i2c_read_pcf8574a");
    if (e)
        {
            LOG4CPLUS_WARN(logger_, "Error in i2c access to pcf8574a for "
                           << *this << ": " << e->what());
        }
    else
        {
            LOG4CPLUS_WARN(logger_, "Unknown error in i2c access to pcf8574a for " << *this);
        }
}
void FebBoard::i2c_read_pcf8574a_ready()
    throw ()
{
    unlog("i2c_read_pcf8574a");
    warm();
    for (int part = 0 ; part < nparts() ; ++part)
        if (fps_[part])
            {
                fps_[part]->warm(pcf8574a_register_.enabled(part));
                if (isWarm())
                    warm(fps_[part]->isWarm());
                if (!fps_[part]->isWarm())
                    fps_[part]->log("febpart_dac_disabled");
                else
                    fps_[part]->unlog("febpart_dac_disabled");
            }
    if (!isWarm())
        log("febboard_dac_disabled");
    else
        unlog("febboard_dac_disabled");
}
void FebBoard::i2c_write_pcf8574a_error(rpct::exception::Exception const * e)
    throw ()
{
    // we don't know if it's warm
    warm(0, 0);
    for (int part = 0 ; part < nparts() ; ++part)
        if (fps_[part])
            {
                fps_[part]->warm(0, 0);
                fps_[part]->unlog("febpart_dac_disabled");
            }
    log("i2c_write_pcf8574a");
    if (e)
        {
            LOG4CPLUS_WARN(logger_, "Error in i2c access to pcf8574a for "
                           << *this << ": " << e->what());
        }
    else
        {
            LOG4CPLUS_WARN(logger_, "Unknown error in i2c access to pcf8574a for " << *this);
        }
}
void FebBoard::i2c_write_pcf8574a_ready()
    throw ()
{
    for (int part = 0 ; part < nparts() ; ++part)
        if (fps_[part])
            fps_[part]->unlog("febpart_dac_disabled");
    unlog("i2c_write_pcf8574a");
}

} // namespace rpct
