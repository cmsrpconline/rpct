#ifdef __BORLANDC__
#include "precompiled.h"
#pragma hdrstop
#endif


#include "rpct/devices/System.h"
#include "rpct/devices/VmeCrate.h"
#include "rpct/devices/LinkBox.h"
#include "rpct/ii/ConfigurationFlags.h"
#include "rpct/std/IllegalArgumentException.h"


using namespace std;
//using namespace xercesc;

namespace rpct {



System::Instance System::instance;
log4cplus::Logger System::logger = log4cplus::Logger::getInstance("System");
void System::setupLogger(std::string namePrefix) {
	logger = log4cplus::Logger::getInstance(namePrefix + "." + logger.getName());
}

const System::HardwareItemList System::emptyHardwareItemList;

/*System& System::getInstance() {
    if (instance.get() == 0) {
        instance.reset(new System());
    }

    return *instance.get();
}*/

System::System() {
}


System::~System() {
    LOG4CPLUS_DEBUG(logger, "Destroying System"
            <<": Start" );

    for (VmeCrateList::iterator i = vmeCrateList.begin(); i != vmeCrateList.end(); i++) {
        delete *i;
    }
    LOG4CPLUS_DEBUG(logger, "Destroing VmeCrates"
            <<": Done" );

    LOG4CPLUS_DEBUG(logger, "Destroying System"
            <<": Done" );
}

void System::checkVersions() {
    LOG4CPLUS_DEBUG(logger, "System::checkVersions: starting to check versions");
	std::ostringstream ost;
    EBadDeviceVersion::VersionInfoList errorList;
	for (CrateList::iterator iCrate = crateList.begin(); iCrate != crateList.end(); ++iCrate) {
		try {
			(*iCrate)->checkVersions();
		}
		catch(EBadDeviceVersion& e) {
			ost << e.what() << "\n";
            errorList.insert(errorList.end(), e.getVersionInfoList().begin(), e.getVersionInfoList().end());
		}
	}


    LOG4CPLUS_DEBUG(logger, "System::checkVersions: versions checked. " << ost.str());

    if (errorList.size() > 0) {
        throw EBadDeviceVersion(ost.str(), errorList);
    }
}

void System::registerItem(IHardwareItem& item) {
    LOG4CPLUS_DEBUG(logger, "System::RegisterItem: registering item with id = " << item.getId()
                    << " description = " << item.getDescription());

    if (dynamic_cast<ICrate*>(&item) != 0) {
        ICrate* crate = dynamic_cast<ICrate*>(&item);
        if (crateMap.find(crate->getId()) != crateMap.end()) {
            throw IllegalArgumentException("System::registerItem: Duplicate item " + toString(crate->getId())
                    + " name = " + crate->getDescription());
        }
        crateMap.insert(CrateMap::value_type(crate->getId(), crate));

        if (crateByNameMap.find(crate->getDescription()) != crateByNameMap.end()) {
            LOG4CPLUS_WARN(logger, "System::registerItem: Duplicate item " << crate->getDescription());
        }
        crateByNameMap.insert(CrateByNameMap::value_type(crate->getDescription(), crate));
    }
    else if (dynamic_cast<IBoard*>(&item) != 0) {
        IBoard* board = dynamic_cast<IBoard*>(&item);
        if (boardMap.find(board->getId()) != boardMap.end()) {
            throw IllegalArgumentException("System::registerItem: Duplicate item " + toString(board->getId())
                    + " name = " + board->getDescription());
        }
        boardMap.insert(BoardMap::value_type(board->getId(), board));
    }
    else if (dynamic_cast<IDevice*>(&item) != 0) {
        IDevice* device = dynamic_cast<IDevice*>(&item);
        if (deviceMap.find(device->getId()) != deviceMap.end()) {
            throw IllegalArgumentException("System::registerItem: Duplicate item " + toString(device->getId())
                    + " name = " + device->getBoard().getDescription() + "." + device->getDescription());
        }
        deviceMap.insert(DeviceMap::value_type(device->getId(), device));
    }
    else {
        throw IllegalArgumentException("System::registerItem");
    }
    hardwareItemByTypeMap[item.getType()].push_back(&item);
}

/*
IHardwareItem& System::getItemById(int id) {
    ItemMap::iterator iItem = itemMap.find(id);
    if (iItem == itemMap.end()) {
        throw TException("System::GetItemByID: item not found");
    }
    return *iItem->second;
}*/

/*
const System::ItemMap& System::getItemMap() {
    return itemMap;
}
*/

const System::CrateMap& System::getCrateMap() {
    return crateMap;
}
const System::BoardMap& System::getBoardMap() {
    return boardMap;
}
const System::DeviceMap& System::getDeviceMap() {
    return deviceMap;
}

const System::HardwareItemList& System::getHardwareItemsByType(HardwareItemType type) {
    HardwareItemByTypeMap::iterator iList = hardwareItemByTypeMap.find(type);
    if (iList == hardwareItemByTypeMap.end()) {
        return emptyHardwareItemList;
    }
    return iList->second;
}

ICrate& System::getCrateById(int id) {
    CrateMap::iterator iItem = crateMap.find(id);
    if (iItem == crateMap.end()) {
        throw TException("System::getCrateById: item '" + toString(id) + "'not found ");
    }
    return *iItem->second;
}

ICrate& System::getCrateByName(string name) {
    CrateByNameMap::iterator iItem = crateByNameMap.find(name);
    if (iItem == crateByNameMap.end()) {
        throw TException("System::getCrateByName: item '" + name + "'not found ");
    }
    return *iItem->second;
}

IBoard& System::getBoardById(int id) {
    BoardMap::iterator iItem = boardMap.find(id);
    if (iItem == boardMap.end()) {
        throw TException("System::getBoardById: item '" + toString(id) + "'not found ");
    }
    return *iItem->second;
}

IDevice& System::getDeviceById(int id) {
    DeviceMap::iterator iItem = deviceMap.find(id);
    if (iItem == deviceMap.end()) {
        throw TException("System::getDeviceById: item '" + toString(id) + "'not found ");
    }
    return *iItem->second;
}

void System::registerCrate(ICrate& crate) {
    crateList.push_back(&crate);
    registerItem(crate);
}

void System::addVmeInterface(TVMEInterface& vmeInterface) {
    vmeInterfaceList.push_back(&vmeInterface);
}

void System::addVmeCrate(VmeCrate& vmeCrate) {
    vmeCrateList.push_back(&vmeCrate);
    registerCrate(vmeCrate);
}

/*checkResetNeeded and then reset for each crate separately!*/
bool System::resetHardware(bool forceReset) {
    CrateList& crates = getCrates();
    bool wasReset = false;
    for (CrateList::iterator iCrate = crates.begin(); iCrate != crates.end(); ++iCrate) {
        ICrate* crate = *iCrate;
        LOG4CPLUS_INFO(logger, "reseting crate " << crate->getDescription());

        bool resetNeeded = forceReset;
        if (!forceReset) {
        	resetNeeded = crate->checkResetNeeded();
        }

        crate->reset(resetNeeded); //reset is implemented in the inherited crates
        wasReset = resetNeeded;
    }
    return wasReset;
}

void System::setupHardware(ConfigurationSet* configurationSet, bool forceSetup) {
	int configFlags = forceSetup ? ConfigurationFlags::FORCE_CONFIGURE : 0;

    CrateList& crates = getCrates();
    bool wasCold = false;
    for (CrateList::iterator iCrate = crates.begin(); iCrate != crates.end(); ++iCrate) {
        ICrate* crate = *iCrate;
        wasCold = crate->configure(configurationSet, configFlags);
    }
}

void System::configureSelected(ConfigurationSet& configurationSet) {
    CrateList& crates = getCrates();
    for (CrateList::iterator iCrate = crates.begin(); iCrate != crates.end(); ++iCrate) {
        ICrate* crate = *iCrate;
        LOG4CPLUS_INFO(logger, "Configuring crate " << crate->getDescription());
        crate->configure(&configurationSet, 0);//ConfigurationFlags::RESET_ONLY_WHEN_NO_CONFIG_DATA
        /*crate->init();

        ICrate::Boards& boards = crate->getBoards();
        for (ICrate::Boards::iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {
            IBoard* board = *iBoard;
            LOG4CPLUS_INFO(logger, "Configuring board " << board->getDescription());
//            board->init();
            board->selfTest();
            const IBoard::Devices& devices = board->getDevices();
            for (IBoard::Devices::const_iterator iDevice = devices.begin(); iDevice != devices.end(); iDevice++) {
                IDevice* device = *iDevice;
                DeviceSettings* deviceSettings = configurationSet.getDeviceSettings(*device);
                if(deviceSettings != 0)
                	device->configure(deviceSettings);
                else {
                	device->reset();
                	LOG4CPLUS_INFO(logger, "configureSelected: " << device->getDescription()<< " no deviceSettings for chip, chip was reset, but not configured");
                }
            }
        }*/
    }
}

void System::enable() {
    CrateList& crates = getCrates();
    for (CrateList::iterator iCrate = crates.begin(); iCrate != crates.end(); ++iCrate) {
        ICrate* crate = *iCrate;
        LOG4CPLUS_INFO(logger, "Enabling crate " << crate->getDescription());
        crate->enable();
    }
}

void System::disable() {
    CrateList& crates = getCrates();
    for (CrateList::iterator iCrate = crates.begin(); iCrate != crates.end(); ++iCrate) {
        ICrate* crate = *iCrate;
        LOG4CPLUS_INFO(logger, "Disabling crate " << crate->getDescription());
        crate->disable();
    }
}

} // namespace rpct
