#include "rpct/devices/LmuxBxData.h"
#include "rpct/devices/LinkBoard.h"
#include <iomanip>
#include <sstream>
#include <iostream>

using namespace std;

namespace rpct {
	
LmuxBxData::LmuxBxData() {
    partitionData_  = 0;
    partitionNum_   = 0;
    partitionDelay_ = 0;
    endOfData_      = 0;
    halfPartition_  = 0;
    lbNum_          = 0;
    index_          = 0;
}

LmuxBxData::LmuxBxData(unsigned int rawData, int index) {
    unsigned int shift = 0;

    partitionData_  =  rawData &    0xff          ; shift += 8;
    partitionNum_   = (rawData &   0xf00) >> shift; shift += 4;
    partitionDelay_ = (rawData &  0x7000) >> shift; shift += 3;
    endOfData_      = (rawData &  0x8000) >> shift; shift += 1;
    halfPartition_  = (rawData & 0x10000) >> shift; shift += 1;
    lbNum_          = (rawData & 0x60000) >> shift; shift += 2;
    
    raw_ = rawData;
    
    index_          = index;
}

LmuxBxData::LmuxBxData(unsigned short partData,
                       unsigned char partNum,
                       unsigned char partDelay,
                       unsigned char eOfData,
                       unsigned char halfPart,
                       unsigned char lbNum,
                       int index)  {
    partitionData_  =  partData;
    partitionNum_   =  partNum;
    partitionDelay_ =  partDelay;
    endOfData_      =  eOfData;
    halfPartition_  =  halfPart;
    lbNum_          =  lbNum;
    index_          =  index;
}

unsigned int LmuxBxData::toRaw() {
    unsigned int rawData = 0;
    unsigned int shift = 0;

    rawData = partitionData_                                 ; shift += 8;
    rawData = rawData | ((partitionNum_  << shift) &   0xf00); shift += 4;
    rawData = rawData | ((partitionDelay_<< shift )&  0x7000); shift += 3;
    rawData = rawData | ((endOfData_     << shift )&  0x8000); shift += 1;
    rawData = rawData | ((halfPartition_ << shift )& 0x10000); shift += 1;
    rawData = rawData | ((lbNum_         << shift )& 0x60000); shift += 2;

    return rawData;
}

bool LmuxBxData::operator == (const LmuxBxData& right) const {
    if( partitionData_  == right.getPartitionData()    &&
            partitionNum_   == right.getPartitionNum()     &&
            partitionDelay_ == right.getPartitionDelay()   &&
            endOfData_      == right.getEndOfData()        &&
            halfPartition_  == right.getHalfPartition()    &&
            lbNum_          == right.getLbNum()	            )
        return true;
    else
        return false;
}

bool LmuxBxData::operator != (const LmuxBxData& right) const {
    return !(*this == right);
}

string LmuxBxData::toString() {
    ostringstream ostr;

    ostr 
    << (unsigned int)lbNum_ << " "
    << (unsigned int)halfPartition_ << " "
    << (unsigned int)endOfData_ << " "
    << (unsigned int)partitionDelay_<< " "
    << setw(2)<< dec
    << (unsigned int)partitionNum_ << " "
    << setw(2)<< hex << setfill('0')
    << partitionData_;

    return ostr.str();
}

LmuxBxDataVec2 demutliplex(const LmuxBxDataVec& muxedDataVec) {
    cout<<"LDeMux "<<endl;
    LmuxBxDataVec2 deMUXdata(3, LmuxBxDataVec(muxedDataVec.size()));
    vector<int> bxOfLast(3, -1);
    cout<<"LDeMux 1 muxedDataVec.size() "<<muxedDataVec.size()<<endl;
    for(unsigned int iBx = 0; iBx < muxedDataVec.size(); iBx++) {
        LmuxBxData muxedData = muxedDataVec[iBx];
        if(muxedData.getPartitionData() != 0) {
            int lbNum = muxedData.getLbNum();
            muxedData.setLbNum(0);
            int bx = iBx - muxedData.getPartitionDelay();
            if(bx <= bxOfLast[lbNum]) {
                bx = bxOfLast[lbNum] + 1;
            }

            muxedData.setPartitionDelay(muxedData.getPartitionDelay() - (iBx - bx) );
            muxedData.setLbNum(0);
            deMUXdata[lbNum][bx] = muxedData;

            bxOfLast[lbNum] = bx;
        }
    }
    return deMUXdata;
}

std::vector<LmuxBxData*> code(void* stripData) {
	unsigned int chamberWidth = 96; //SynCoder::RPC_LBSTD_CHAMBER_WIDTH - tp daje 2, czyli jakos bzdure;
	unsigned int bitsPerPart = 8;
	
	std::vector<LmuxBxData*> lmuxBxDataVec;
	for(int iPart = (chamberWidth/bitsPerPart) -1, partDelay = 0; iPart >= 0; iPart--) {
		unsigned int partData = 0;
		bitcpy(&partData, 0, stripData, iPart * bitsPerPart, bitsPerPart);
		if(partData != 0) {
			LmuxBxData* lmuxBxData = new LmuxBxData(partData, iPart, partDelay, 0, 0, 0, 0);
			lmuxBxDataVec.push_back(lmuxBxData);
			partDelay++;
		}
	}
	
	return lmuxBxDataVec;
}


}
