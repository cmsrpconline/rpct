#include "rpct/devices/LinkBoardBxData.h"
#include "rpct/devices/LinkBoard.h"
#include "rpct/devices/cmssw/RPCConst.h"
#include "rpct/std/bitcpy.h"
#include "rpct/std/tbutil.h"
#include "rpct/std/TException.h"

#include <iomanip>
#include<sstream>

using namespace std;

namespace rpct {

LinkBoardBxDataFactory LinkBoardBxDataFactory::instance_;

LinkBoardBxData::LinkBoardBxData(IDiagnosticReadout::BxData& bxData, IDiagnosticReadout& ownerReadout)
        : StandardBxData(bxData) {
	
	SynCoder::LBDiagnosticReadout& lbOwnerReadout = dynamic_cast<SynCoder::LBDiagnosticReadout&> (ownerReadout);
/*	if(lbOwnerReadout == 0)
		throw TException("LinkBoardBxData: ownerReadout ins not LBDiagnosticReadout");*/
			
    unsigned int chamberWidth = SynCoder::RPC_LBSTD_CHAMBER_WIDTH;
    
    chamberWidth = 96; 	
 //----------   	
 	int stripDataSize = chamberWidth /8;
	char stripData[stripDataSize];
	
	bitcpy(stripData, 0, bxData_.getData(), 0, chamberWidth);
	string stripDataStr = dataToHex(stripData, chamberWidth);
	
	//strip data are coded and put into the lmuxBxDataVector_ with LmuxBxData::index 0
	lmuxBxDataVector_ = code(stripData);
	
	uint32_t codedData = 0;
	bitcpy(&codedData, 0, bxData_.getData(), chamberWidth, 20);
		
	ILBDiagnosticReadout::ExtDataSel extSel = lbOwnerReadout.getExtSource();
	if(extSel != ILBDiagnosticReadout::edsNone && extSel != ILBDiagnosticReadout::edsCSC) {
		//if the ext data are from coder or lmux, the LmuxBxData::index denotes the source of the extended data
		LmuxBxData* lmuxBxData = new LmuxBxData(codedData, extSel);
		if(codedData != 0)
			lmuxBxDataVector_.push_back(lmuxBxData);
		dataStr_ = lmuxBxData->toString() + " | " + stripDataStr;
	}
	else {
		dataStr_ = dataToHex(&codedData, 20) + " | " + stripDataStr;
	}
}



}
