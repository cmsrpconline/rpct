#include "rpct/devices/tcsort.h"
#include "rpct/devices/TriggerBoard.h"
#include "rpct/devices/FixedHardwareSettings.h"
#include "rpct/devices/TcGbSortBxData.h"
#include "rpct/devices/TcSortSettings.h"
#include "rpct/std/NullPointerException.h"
#include "rpct/ii/ConfigurationFlags.h"
//#include "rpct/devices/System.h"


//---------------------------------------------------------------------------


using namespace std;
using namespace log4cplus;

namespace rpct {

Logger TTCSortDevice::logger = Logger::getInstance("TTCSortDevice");
void TTCSortDevice::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}

//---------------------------------------------------------------------------
#define IID_FILE "RPC_system_def.iid"
#define IID_CLASS TTCSortDevice
#include "iid2cdef2.h"

#define IID_FILE "RPC_TB3_def.iid"
#define IID_CLASS TTCSortDevice
#include "iid2cdef2.h"

#define IID_FILE "RPC_TC_SORT_def.iid"
#define IID_CLASS TTCSortDevice
#include "iid2cdef2.h"

//---------------------------------------------------------------------------


#define IID_FILE "RPC_TB3_vme.iid"
#define IID_CLASS TTCSortVme
#include "iid2c2.h"

const HardwareItemType TTCSortVme::TYPE(HardwareItemType::cDevice, "TCSORTVME");

TTCSortVme::TTCSortVme(int vme_board_address, IBoard& board) :
    TTCSortDevice(board.getId() * 1000, VME_IDENTIFIER.c_str(), WORD_IDENTIFIER, "VME", TYPE, VME_VERSION.c_str(),
            WORD_VERSION, WORD_CHECKSUM, 0, board, -1, BuildMemoryMap(), vme_board_address), ABootContr(NULL) {
}

TTCSortVme::~TTCSortVme() {
    delete ABootContr;
}

//---------------------------------------------------------------------------

#ifdef CII_TC
#define IID_CLASS TTCSortTCSort
#include "CII_TC_SORT_iicfg_tab.cpp"
#undef IID_CLASS
#else
#define IID_FILE "RPC_TC_SORT.iid"
#define IID_CLASS TTCSortTCSort
#include "iid2c2.h"
const int TTCSortTCSort::II_ADDR_SIZE = TTCSortTCSort::TC_SORT_IIADDR_WIDTH;
const int TTCSortTCSort::II_DATA_SIZE = TTCSortTCSort::TC_SORT_IIDATA_WIDTH;
#endif

const HardwareItemType TTCSortTCSort::TYPE(HardwareItemType::cDevice, "TCSORTTCSORT");

TTCSortTCSort::TTCSortTCSort(int id, int vme_board_address, int ii_addr, const char* desc, IBoard& board) :
    TTCSortDevice(id, TC_SORT_IDENTIFIER.c_str(), WORD_IDENTIFIER, desc, TYPE, TC_SORT_VERSION.c_str(), WORD_VERSION,
            WORD_CHECKSUM, ii_addr, board, 0, BuildMemoryMap(), vme_board_address), DiagCtrl(0), DiagnosticReadout(0),
            PulserDiagCtrl(0), Pulser(0), I2C(0), TTCrxI2C(7, board.getDescription()),
            qpll_(*this, logger,
            		VECT_QPLL,
            		BITS_QPLL_MODE,
            		BITS_QPLL_CTRL,
            		BITS_QPLL_SELECT,
            		BITS_QPLL_LOCKED,
            		BITS_QPLL_ERROR,
            		BITS_QPLL_CLK_LOCK),
            monRecErrorAnalyzer_(MonitorItemName::RECEIVER, 1, 1, 1000, 0xffffff)
{
    monRecErrorAnalyzer_.setMaxCounterVal((1ull << memoryMap_->GetItemInfo(WORD_REC_ERROR_COUNT).Width) - 1);
    monitorItems_.push_back(MonitorItemName::FIRMWARE);
    monitorItems_.push_back(MonitorItemName::QPLL);
    monitorItems_.push_back(MonitorItemName::TTCRX);
    monitorItems_.push_back(MonitorItemName::RECEIVER);
}

TTCSortTCSort::~TTCSortTCSort() {
    delete DiagCtrl;
    delete DiagnosticReadout;
    delete Pulser;
    delete PulserDiagCtrl;
    delete I2C;
}

/*    writeWord(TTCSortTCSort::WORD_TTC_ID_MODE, 0, 7);
 writeBits(TTCSortTCSort::BITS_TTC_INIT_ENA, 1);
 writeBits(TTCSortTCSort::BITS_TTC_RESET, 1);
 tbsleep(2000);
 writeBits(TTCSortTCSort::BITS_TTC_RESET, 0);
 writeBits(TTCSortTCSort::BITS_TTC_INIT_ENA, 0);
 tbsleep(500);
 if (readBits(TTCSortTCSort::BITS_TTC_READY) != 1) {
 throw TException("TTCSortTCSort::Initialize: BITS_TTC_READY did not go up");
 }

 writeBits(TTCSortTCSort::BITS_QPLL_MODE, 1);
 writeBits(TTCSortTCSort::BITS_QPLL_SELECT, 0x30);
 tbsleep(1000);
 if (readBits(TTCSortTCSort::BITS_QPLL_LOCKED) != 1) {
 throw TException("TTCSortTCSort::Initialize: BITS_QPLL_LOCKED did not go up");
 }
 */

TIII2CMasterCore& TTCSortTCSort::GetI2C() {
    if (I2C == NULL) {

        I2C = new TIII2CMasterCore(*this, AREA_I2C);
        I2C->SetPrescaleValue(100);
        I2C->WriteControlReg(TI2CMasterCore::BIT_EN);
    }

    return *I2C;
}

void TTCSortTCSort::ResetTTC() {
	for(int i = 0; i < 3; i++) {
		int resetTime = 4000 + i * 100;
		LOG4CPLUS_INFO(logger, getFullName() <<" TTCSortTCSort::ResetTTC() stated, resetTime = "<<resetTime);
		LOG4CPLUS_INFO(logger, getFullName() <<" BITS_STATUS2_TTC_RESET, 1");
		writeBits(BITS_TTC_RESET, 1);
		tbsleep(resetTime);
		if (readBits(BITS_TTC_READY) != 0) {
			throw TException("TTCSortTCSort::ResetTTC: BITS_TTC_READY did not go down after BITS_TTC_RESET, 1");
			//LOG4CPLUS_ERROR(logger, "TTCSortTCSort::ResetTTC: BITS_TTC_READY did not go down after BITS_TTC_RESET, 1");
		}
		writeBits(BITS_TTC_RESET, 0);
		LOG4CPLUS_INFO(logger, getFullName() <<" BITS_STATUS2_TTC_RESET, 0");

	    tbsleep(100);
	    if (readBits(BITS_TTC_READY) != 1) {
	    	LOG4CPLUS_ERROR(logger, "TTCSortTCSort::InitTTC: BITS_TTC_READY did not go up, repeating TTCrx reset");
	    }
	    else {
	    	return;
	    }
	}
	throw TException("TTCSortTCSort::InitTTC: BITS_TTC_READY did not go up");
}

void TTCSortTCSort::InitTTC() {
    writeWord(WORD_TTC_ID_MODE, 0, TTCrxI2C.GetAddress());
    writeBits(BITS_TTC_INIT_ENA, 1);
    ResetTTC();
    writeBits(BITS_TTC_INIT_ENA, 0);
    LOG4CPLUS_INFO(logger, "TTCSortTCSort::InitTTC: done");
//    tbsleep(500);
//    if (readBits(BITS_TTC_READY) != 1) {
//        throw TException("TTCSortTCSort::InitTTC: BITS_TTC_READY did not go up");
//        //LOG4CPLUS_ERROR(logger, "TTCSortTCSort::InitTTC: BITS_TTC_READY did not go up");
//
//    }

    //GetTTCrxI2C().WriteFineDelay1(0);
    //GetTTCrxI2C().WriteFineDelay2(0);
    //GetTTCrxI2C().WriteCoarseDelay(0, 0);
}

void TTCSortTCSort::ResetQPLL() {
	qpll_.reset();
}

TDiagCtrl& TTCSortTCSort::GetDiagCtrl() {
    if (DiagCtrl == 0) {
        DiagCtrl
                = new TDiagCtrl(*this, VECT_DAQ, BITS_DAQ_PROC_REQ, BITS_DAQ_PROC_ACK, BITS_DAQ_TIMER_LOC_ENA, 0, BITS_DAQ_TIMER_START, BITS_DAQ_TIMER_STOP, BITS_DAQ_TIMER_TRIG_SEL, WORD_DAQ_TIMER_LIMIT, WORD_DAQ_TIMER_COUNT, WORD_DAQ_TIMER_TRG_DELAY, "DAQ_DIAG_CTRL");
    }
    return *DiagCtrl;
}

TStandardDiagnosticReadout& TTCSortTCSort::GetDiagnosticReadout() {
    if (DiagnosticReadout == 0) {
        DiagnosticReadout
                = new TStandardDiagnosticReadout(*this, "READOUT", &TcGbSortBxDataFactory::getInstance(), GetDiagCtrl(), VECT_DAQ, BITS_DAQ_EMPTY, BITS_DAQ_EMPTY_ACK, BITS_DAQ_LOST, BITS_DAQ_LOST_ACK, VECT_DAQ_WR, BITS_DAQ_WR_ADDR, BITS_DAQ_WR_ACK, VECT_DAQ_RD, BITS_DAQ_RD_ADDR, BITS_DAQ_RD_ACK, WORD_DAQ_MASK, WORD_DAQ_DATA_DELAY, AREA_MEM_DAQ_DIAG, DAQ_DIAG_DATA_SIZE, DAQ_DIAG_TRIG_NUM, DAQ_DIAG_TIMER_SIZE, TTC_BCN_EVT_WIDTH);
    }
    return *DiagnosticReadout;
}

TDiagCtrl& TTCSortTCSort::GetPulserDiagCtrl() {
    if (PulserDiagCtrl == NULL) {
        PulserDiagCtrl
                = new TDiagCtrl(*this, VECT_PULSER, BITS_PULSER_PROC_REQ, BITS_PULSER_PROC_ACK, BITS_PULSER_TIMER_LOC_ENA, 0, BITS_PULSER_TIMER_START, BITS_PULSER_TIMER_STOP, BITS_PULSER_TIMER_TRIG_SEL, WORD_PULSER_TIMER_LIMIT, WORD_PULSER_TIMER_COUNT, WORD_PULSER_TIMER_TRG_DELAY, "PULSER_DIAG_CTRL");
    }

    return *PulserDiagCtrl;
}

TPulser& TTCSortTCSort::GetPulser() {
    if (Pulser == NULL) {
        Pulser
                = new TcGbSortPulser(*this, "PULSER", GetPulserDiagCtrl(), AREA_MEM_PULSE, WORD_PULSER_LENGTH, BITS_PULSER_REPEAT_ENA, BITS_PULSER_OUT_ENA);
    }
    return *Pulser;
}
TTCSortTCSort::TDiagCtrlVector& TTCSortTCSort::GetDiagCtrls() {
    if (DiagCtrls.empty()) {
        DiagCtrls.push_back(&GetDiagCtrl());
        DiagCtrls.push_back(&GetPulserDiagCtrl());
    }
    return DiagCtrls;
}

TTCSortTCSort::TDiagVector& TTCSortTCSort::GetDiags() {
    if (Diags.empty()) {
        Diags.push_back(&GetDiagnosticReadout());
        Diags.push_back(&GetPulser());
    }
    return Diags;
}

void TTCSortTCSort::ConfigureDiagnosable(TTriggerDataSel triggerDataSel, uint32_t dataTrgDelay) {
    writeBits(BITS_STATUS_TRG_DATA_SEL, triggerDataSel);
    writeWord(WORD_DATA_TRG_DELAY, 0, dataTrgDelay);
}

bool TTCSortTCSort::checkResetNeeded() {
    return (readBits(BITS_TTC_READY) == 0) || (readBits(BITS_QPLL_LOCKED) == 0);
}

bool TTCSortTCSort::checkWarm(DeviceSettings* s) {
    //return (readBits(BITS_QPLL_LOCKED) == 1) && (readBits(BITS_TTC_READY) == 1);
    return true;
}

void TTCSortTCSort::configure(DeviceSettings* settings, int flags) {
    if (flags & ConfigurationFlags::FORCE_CONFIGURE) {
        reset();

        if (settings == 0) {
           /* if (flags & ConfigurationFlags::RESET_ONLY_WHEN_NO_CONFIG_DATA) {
                return;
            }*/
            throw NullPointerException("TTCSortTCSort::configure: settings is null");
        }
        TcSortSettings* s = dynamic_cast<TcSortSettings*> (settings);
        if (s == 0) {
            throw IllegalArgumentException("TTCSortTCSort::configure: invalid cast for settings");
        }

        writeWord(WORD_REC_MUX_CLK_INV, 0, s->getRecMuxClkInv());
        writeWord(WORD_REC_MUX_CLK90, 0, s->getRecMuxClk90());
        writeWord(WORD_REC_MUX_REG_ADD, 0, s->getRecMuxRegAdd());

        vector<int>& recMuxDelay = s->getRecMuxDelay();
        for (vector<int>::size_type i = 0; i < recMuxDelay.size(); i++) {
            writeWord(WORD_REC_MUX_DELAY, i, recMuxDelay[i]);
        }

        vector<int>& recDataDelay = s->getRecDataDelay();
        for (vector<int>::size_type i = 0; i < recDataDelay.size(); i++) {
            writeWord(WORD_REC_DATA_DELAY, i, recDataDelay[i]);
        }
    }
}

void TTCSortTCSort::enable() {
    writeWord(WORD_REC_ERROR_COUNT, 0, uint32_t(0));
}

boost::dynamic_bitset<>& TTCSortTCSort::getUsedTBs() {
    if (UsedTBs.size() == 0) {
        UsedTBs = boost::dynamic_bitset<>(9, 0ul);
        for (unsigned int iB = 0; iB < getBoard().getCrate()->getBoards().size(); iB++) {
            TriggerBoard* tb = dynamic_cast<TriggerBoard*> (getBoard().getCrate()->getBoards()[iB]);
            if (tb != NULL) {
                UsedTBs[tb->getNumber()] = true;
            }
        }
    }
    return UsedTBs;
}

void TTCSortTCSort::reset() {
    writeWord(BITS_TTC_TEST_ENA, 0, uint32_t(0));

    writeWord(WORD_REC_CHAN_ENA, 0, (getUsedTBs().to_ulong()));

    writeWord(WORD_REC_TEST_ENA, 0, uint32_t(0));
    writeWord(WORD_REC_TEST_RND_ENA, 0, uint32_t(0));

    writeWord(WORD_SEND_TEST_ENA, 0, uint32_t(0));
    writeWord(WORD_SEND_TEST_DATA, 0, uint32_t(0));

    writeBits(BITS_PULSER_OUT_ENA, uint32_t(0));

    writeWord(WORD_BCN0_DELAY, 0, FixedHardwareSettings::TCSORT_BC0_DELAY);

    writeWord(WORD_TRG_DELAY, 0, FixedHardwareSettings::TCSORT_TRG_DELAY);

    EnableTransmissionCheck(true, true);
    writeWord(WORD_REC_ERROR_COUNT, 0, uint32_t(0));
}

void TTCSortTCSort::EnableTransmissionCheck(bool enableBCNcheck, bool enableDataCheck) {
    bool checkDataEna = enableBCNcheck;
    bool checkEna = enableDataCheck;

    uint32_t zero = 0;
    if (checkDataEna) {
        writeWord(TTCSortTCSort::WORD_REC_CHECK_DATA_ENA, 0, getUsedTBs().to_ulong());//0x1ff
        writeWord(TTCSortTCSort::WORD_SEND_CHECK_DATA_ENA, 0, 0x3);
    } else {
        writeWord(TTCSortTCSort::WORD_REC_CHECK_DATA_ENA, 0, zero);
        writeWord(TTCSortTCSort::WORD_SEND_CHECK_DATA_ENA, 0, zero);
    }
    if (checkEna) {
        writeWord(TTCSortTCSort::WORD_REC_CHECK_ENA, 0, getUsedTBs().to_ulong());
        writeWord(TTCSortTCSort::WORD_SEND_CHECK_ENA, 0, 0x3);
    } else {
        writeWord(TTCSortTCSort::WORD_REC_CHECK_ENA, 0, zero);
        writeWord(TTCSortTCSort::WORD_SEND_CHECK_ENA, 0, zero);
    }
}

void TTCSortTCSort::monitor(volatile bool *stop) {
    monitor(monitorItems_);
}

void TTCSortTCSort::monitor(MonitorItems& items) {
    warningStatusList_.clear();
    for (MonitorItems::iterator iItem = items.begin(); iItem != items.end(); ++iItem) {
        if ((*iItem) == MonitorItemName::FIRMWARE) {
            string msg = monitorVersion();
            if (msg.size() != 0) {
                warningStatusList_.push_back(MonitorableStatus(MonitorItemName::FIRMWARE, MonitorableStatus::ERROR, msg, 0));
            }
        }
        else if ((*iItem) == MonitorItemName::QPLL) {
            if (readBits(BITS_QPLL_LOCKED) != 1) {
                if (readBits(BITS_QPLL_LOCKED) != 1) {
                    warningStatusList_.push_back(MonitorableStatus(MonitorItemName::QPLL, MonitorableStatus::ERROR, "BITS_QPLL_LOCKED is not up", 0));
                }
                else {
                    warningStatusList_.push_back(MonitorableStatus(MonitorItemName::QPLL, MonitorableStatus::WARNING, "BITS_QPLL_LOCKED is not up", 0));
                }
            }
        } else if ((*iItem) == MonitorItemName::TTCRX) {
            if (readBits(BITS_TTC_READY) != 1) {
                //throw MonitorableException(*this, MON_TTCRX, "BITS_TTC_READY is not up");
                warningStatusList_.push_back(MonitorableStatus(MonitorItemName::TTCRX, MonitorableStatus::ERROR,
                        "BITS_TTC_READY is not up", 0));
            }
        } else if ((*iItem) == MonitorItemName::RECEIVER) {
            uint32_t recErrCnt = readWord(WORD_REC_ERROR_COUNT, 0);
            if (monRecErrorAnalyzer_.newValue(recErrCnt) != MonitorableStatus::OK) {
                warningStatusList_.push_back(monRecErrorAnalyzer_.getStatus());
            }
        }
        /*else if ((*iItem) == MON_TTCRX_CONF) {
            unsigned int controlReg = GetTTCrxI2C().ReadControlReg(); //I think the I@C must be setup, thats why it is not working
            if (controlReg != FixedHardwareSettings::LB_TTC_RX_CONTROL_REG) {
                warningStatusList_.push_back(MonitorableStatus(MON_TTCRX_CONF, MonitorableStatus::SOFT_WARNING, "TTCrx controlReg is " + rpct::toString(controlReg) + " (should 0x9b)", 0));
            }
        }*/
    }
}
//---------------------------------------------------------------------------
const HardwareItemType TTCSort::TYPE(HardwareItemType::cDevice, "TCSORT");

TTCSort::TTCSort(int ident, const char* desc, TVMEInterface* vme, int vme_board_address, VmeCrate* c, int pos,
        int tcSortChipId) :
    BoardBase(ident, desc, TYPE, c, pos), VME(vme), TCSortVme(vme_board_address, *this), TCSortTCSort(tcSortChipId,
            vme_board_address, 0, "TC_SORT", *this), VMEAddress(vme_board_address) {
    TCSortVme.getMemoryMap().SetHardwareIfc(
            new TIIVMEAccess(TCSortVme.GetVMEBoardBaseAddr(), TCSortVme.getBaseAddress()
                    << TCSortVme.GetVMEBaseAddrShift(), vme, 0xf8000), true);
    TCSortTCSort.getMemoryMap().SetHardwareIfc(
            new TIIVMEAccess(TCSortTCSort.GetVMEBoardBaseAddr(), TCSortTCSort.getBaseAddress()
                    << TCSortTCSort.GetVMEBaseAddrShift(), vme, 0), //0xf8000),
            true);
    devices_.push_back(&TCSortVme);
    devices_.push_back(&TCSortTCSort);
}

TTCSort::~TTCSort() {
}

void TTCSort::init() {
    TCSortTCSort.InitTTC();
    TCSortTCSort.ResetQPLL();
}

}
