//---------------------------------------------------------------------------
#ifdef __BORLANDC__
#include "precompiled.h"
#pragma hdrstop
#endif

#include "rpct/devices/TGolI2C.h"

namespace rpct {

void TGolI2C::SetAddress(unsigned char addr) {
    unsigned int a = addr;
    a <<= 1;
    a &= 0xfe;
    I2CPointer = a;
    I2CData = a | 1;
    //I2CPointer = (addr << 1) & 0xfe;
    //I2CData = (addr << 1) | 1;
}

unsigned char TGolI2C::GetAddress() {
    return I2CPointer >> 1;
}

void TGolI2C::SetI2CInterface(TI2CInterface& i2c) {
    I2C = &i2c;
}

void TGolI2C::WriteConfig0(unsigned char wait_time, unsigned char loss_of_lock_time) {
    if ((wait_time > 0x1f) || (loss_of_lock_time > 0x7))
        throw TException("WriteConfig0: invalid parameter");

    unsigned char value = wait_time | (loss_of_lock_time << 5);
    WriteReg(Config0, value);
    if (ReadReg(Config0) != value)
        throw TException("Could not set config 0");
}

void TGolI2C::ReadConfig0(unsigned char& wait_time, unsigned char& loss_of_lock_time) {
    unsigned char value = ReadReg(Config0);
    wait_time = value & 0x1f;
    loss_of_lock_time = (value >> 5) & 0x7;
}

void TGolI2C::WriteConfig1(unsigned char pll_lock_time, bool en_soft_loss_of_lock,
        bool en_loss_of_lock_count, bool en_force_lock, bool en_self_test) {
    if (pll_lock_time > 0xf)
        throw TException("WriteConfig1: invalid parameter");

    unsigned char value = pll_lock_time | (en_soft_loss_of_lock ? (1 << 4) : 0)
            | (en_loss_of_lock_count ? (1 << 5) : 0) | (en_force_lock ? (1 << 6) : 0)
            | (en_self_test ? (1 << 7) : 0);

    WriteReg(Config1, value);
    if (ReadReg(Config1) != value)
        throw TException("Could not set config 1");
}

void TGolI2C::ReadConfig1(unsigned char& pll_lock_time, bool& en_soft_loss_of_lock,
        bool& en_loss_of_lock_count, bool& en_force_lock, bool& en_self_test) {
    unsigned char value = ReadReg(Config1);
    pll_lock_time = value & 0xf;
    en_soft_loss_of_lock = (value & (1 << 4)) != 0;
    en_loss_of_lock_count = (value & (1 << 5)) != 0;
    en_force_lock = (value & (1 << 6)) != 0;
    en_self_test = (value & (1 << 7)) != 0;
}

void TGolI2C::WriteConfig2(unsigned char pll_current, bool en_flag) {
    if ((pll_current > 0x1f))
        throw TException("WriteConfig2: invalid parameter");

    unsigned char value = pll_current | (en_flag ? (1 << 7) : 0);
    WriteReg(Config2, value);
    if ((ReadReg(Config2) & 0x9F) != value)
        throw TException("Could not set config 2");
}

void TGolI2C::ReadConfig2(unsigned char& pll_current, bool& en_flag) {
    unsigned char value = ReadReg(Config2);
    pll_current = value & 0x1f;
    en_flag = (value & (1 << 7)) != 0;
}

void TGolI2C::WriteConfig3(unsigned char ld_current, bool use_conf_regs) {
    if ((ld_current > 0x3f))
        throw TException("WriteConfig3: invalid parameter");

    unsigned char value = ld_current | (use_conf_regs ? (1 << 7) : 0);
    WriteReg(Config3, value);
    if (ReadReg(Config3) != value)
        throw TException("Could not set config 3");
}

void TGolI2C::ReadConfig3(unsigned char& ld_current, bool& use_conf_regs) {
    unsigned char value = ReadReg(Config3);
    ld_current = value & 0x3f;
    use_conf_regs = (value & (1 << 7)) != 0;
}

void TGolI2C::ReadStatus0(unsigned char& loss_of_lock_count) {
    loss_of_lock_count = ReadReg(Status0);
}

void TGolI2C::ReadStatus1(bool& conf_wmode16, bool& conf_glink,
        TLinkControlState& link_control_state_C, TLinkControlState& link_control_state_B,
        TLinkControlState& link_control_state_A) {
    unsigned char value = ReadReg(Status1);
    conf_wmode16 = (value & (1 << 0)) != 0;
    conf_glink = (value & (1 << 1)) != 0;
    link_control_state_C = (value >> 2) & 0x3;
    link_control_state_B = (value >> 4) & 0x3;
    link_control_state_A = (value >> 6) & 0x3;
}

}
