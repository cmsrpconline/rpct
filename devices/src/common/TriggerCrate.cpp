#include "rpct/devices/TriggerCrate.h"
#include "rpct/devices/System.h"
#include "rpct/ii/ConfigurationFlags.h"



using namespace std;
using namespace log4cplus;

namespace rpct {


Logger TriggerCrate::logger = Logger::getInstance("TriggerCrate");
void TriggerCrate::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}

const HardwareItemType TriggerCrate::TYPE(HardwareItemType::cCrate, "TRIGGERCRATE");

TriggerBoard* TriggerCrate::getTb(int pos) {
    IBoard* board = getBoard(pos);
    if (board == 0) {
        return 0;
    }
    TriggerBoard* tb3 = dynamic_cast<TriggerBoard*>(board);
    if (tb3 == 0) {
        throw IllegalArgumentException("Not a Tb3 at pos " + toString(pos) + " in the crate");
    }
    return tb3;
}

std::list<TriggerBoard*>& TriggerCrate::getTriggerBoards() {
    if(triggerBoards_.size() == 0) {
        Boards& boards = getBoards();
        for (Boards::iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {
            TriggerBoard* tb = dynamic_cast<TriggerBoard*>(*iBoard);
            if (tb != 0) {
                triggerBoards_.push_back(tb);
            }
        }
    }
    return triggerBoards_;
}

TTCSort* TriggerCrate::getTcSort() {
    if (tcSort_ == 0) {
        Boards& boards = getBoards();
        for (Boards::iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {
            tcSort_ = dynamic_cast<TTCSort*>(*iBoard);
            if (tcSort_ != 0) {
                break;
            }
        }
    }
    return tcSort_;
}

void TriggerCrate::reset(bool forceReset) {
    if (forceReset)  {
        TTCSort* tcSort = getTcSort();
        if (tcSort == 0) {
            throw TException("TriggerCrate::beforeConfigure(): tcSort not found");
        }
        tcSort->init();

        const Boards& boards = getBoards();
            for (Boards::const_iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {
                TriggerBoard* tb = dynamic_cast<TriggerBoard*>(*iBoard);
                if (tb != 0) {
                    tb->GetControl().ResetQPLL();
                }
            }
    }
}


bool TriggerCrate::checkWarm(ConfigurationSet* configurationSet) {
    unsigned int configId = configurationSet->getId();
    if (configId == 0) {
        return false;
    }
    TTCSort* tcSort = getTcSort();
    if (tcSort == 0) {
        return false;
    }
    TTCSortTCSort& chip = tcSort->GetTCSortTCSort();
    unsigned int readConfigId;
    readConfigId = (chip.readWord(TTCSortTCSort::WORD_USER_REG1, 0) << 16) & 0xffff0000;
    readConfigId |= chip.readWord(TTCSortTCSort::WORD_USER_REG2, 0) & 0xffff;
    LOG4CPLUS_INFO(logger, "checkWarm " << getDescription() << hex << " configId = " << configId << ", readConfigId = " << readConfigId
    		<< ": " << (configId == readConfigId ? "WARM" : "COLD" ));

    bool warm = configId == readConfigId;
    if (!warm) {
        return false;
    }

    return VmeCrate::checkWarm(configurationSet);
}

void TriggerCrate::rememberConfiguration(ConfigurationSet* configurationSet) {
    unsigned int configId = configurationSet->getId();
    TTCSort* tcSort = getTcSort();
    if (tcSort == 0) {
        return;
    }
    TTCSortTCSort& chip = tcSort->GetTCSortTCSort();
    chip.writeWord(TTCSortTCSort::WORD_USER_REG1, 0, (configId >> 16) & 0xffff);
    chip.writeWord(TTCSortTCSort::WORD_USER_REG2, 0, configId & 0xffff);
}


void TriggerCrate::checkConfigId(ConfigurationSet* configurationSet) {

    std::ostringstream ost;
    EBadDeviceVersion::VersionInfoList errorList;

    const Boards& boards = getBoards();
    for (Boards::const_iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {
        TriggerBoard* tb = dynamic_cast<TriggerBoard*>(*iBoard);
        if (tb != 0) {
            try {
                tb->checkConfigId(configurationSet);
            }
            catch (EBadDeviceVersion& e) {
                ost << e.what() << "\n";
                errorList.insert(errorList.end(), e.getVersionInfoList().begin(), e.getVersionInfoList().end());
            }
        }
    }


    if (errorList.size() > 0) {
        throw EBadDeviceVersion(ost.str(), errorList);
    }
}

} // namespace

