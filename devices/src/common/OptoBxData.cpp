#include "rpct/devices/OptoBxData.h"
#include "rpct/devices/cmssw/RPCConst.h"
#include "rpct/std/bitcpy.h"
#include "rpct/std/tbutil.h"
#include "rpct/std/TException.h"

#include <iomanip>
#include<sstream>

using namespace std;

namespace rpct {

OptoBxDataFactory OptoBxDataFactory::instance_;

OptoBxData::OptoBxData(IDiagnosticReadout::BxData& bxData)
        : StandardBxData(bxData) {
	//throw TException("OptoBxData nie zaimplementowane!!!!");	
    unsigned int pos = 0;

	unsigned int channelValidBitsCnt = 3;
	
    unsigned int bcn0Bit = 0; //1 bit

    unsigned int bcnBitsCnt = 3;
    unsigned int bcnData = 0;
 
    unsigned int linksCnt = 3;
    unsigned int linkBitsCnt = 32;
    unsigned int linkData = 0;

    ostringstream ostr;
    string logConeOutStr;    
    
    for(unsigned int iL = 0, index = 0; iL < linksCnt; iL++, index++) {
        bitcpy(&linkData, 0, bxData_.getData(), pos, linkBitsCnt);
        pos += linkBitsCnt;
        dataStr_ = dataToHex(&linkData, linkBitsCnt) + " " + dataStr_;
                
        if(linkData != 0) {
        	LmuxBxData* lmuxBxData = new LmuxBxData(linkData, index);
        	lmuxBxDataVector_.push_back(lmuxBxData);
        }
        
        //RPC LBs
        unsigned int bcn0Bit = (linkData & 0x01000000) >> 24;
        unsigned int bcnData = (linkData & 0xfe000000) >> 25;
        
/*        //HO
        unsigned int bcn0Bit = (linkData & 0x08000000) >> 27;
        unsigned int bcnData = (linkData & 0x07000000) >> 24;*/
                
        ostringstream ostr;
        ostr.clear();
        ostr<<hex<<setw(3)<<bcnData<<" "<<bcn0Bit<<" ";
        dataStr_ = " | " + ostr.str() + dataStr_ ;
    }
    
    bitcpy(&channelsValid_,    0, bxData_.getData(), pos, channelValidBitsCnt);       pos += channelValidBitsCnt;
    bitcpy(&bcn0Bit,    0, bxData_.getData(), pos, 1);                                pos += 1;
    bitcpy(&bcnData,    0, bxData_.getData(), pos, bcnBitsCnt);                       pos += bcnBitsCnt;

	ostr.clear();
	ostr<<hex<<setw(3)<<bcnData<<" "<<bcn0Bit<<" "<<setw(5)<<channelsValid_;
	dataStr_ = ostr.str() + " | " + dataStr_;     
}



}
