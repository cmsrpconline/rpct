#include "rpct/devices/FebPart.h"

#include <sstream>

#include <cmath>

#include "rpct/ii/feb_const.h"
#include "rpct/devices/FebSystem.h"

#include "rpct/ii/const.h"
#include "rpct/ii/ConfigurationFlags.h"

#include "rpct/ii/i2c/II2cAccess.h"

#include "rpct/devices/feb_const.h"
#include "rpct/devices/FebBoard.h"
#include "rpct/devices/RecursiveAction.h"

namespace rpct {

log4cplus::Logger FebPart::logger_ = log4cplus::Logger::getInstance("Feb.FebPart");
void FebPart::setupLogger(std::string namePrefix) {
	logger_ = log4cplus::Logger::getInstance(namePrefix + "." + logger_.getName());
}
HardwareItemType * FebPart::type_ = 0;
IMonitorable::MonitorItems * FebPart::mitems_ = 0;

unsigned short FebPart::channel() const
{
    return febboard_.getChannel();
}
unsigned char FebPart::febtype() const
{
    return febboard_.getFebType();
}
unsigned char FebPart::address() const
{
    return febboard_.getAddress();
}

void FebPart::readTemperature()
{
    LOG4CPLUS_TRACE(logger_, "FebPart::readTemperature for " << *this << ".");

    unsigned char laddress = (rpct::devices::feb::preset::ad7417_[position_ + febtype()]
                              + address());
    unsigned char data[3] = {0x01, rpct::devices::feb::preset::adc_temp_, 0x00};

    febboard_.enableFebAccess();

    unsigned short _channel(channel());
    i2c::EnsureGroup e_group(&i2c_access_);
    i2c_access_.write(_channel, laddress
                      , &(data[0]), 2);
    i2c_access_.write(_channel, laddress
                      , &(data[2]), 1);
    i2c_access_.delay(33000);
    i2c_access_.read(_channel, laddress
                     , &adc_register_temperature_, temperature_read_response_);
    e_group.end();

    febboard_.disableFebAccess();
}
void FebPart::readVTH(unsigned char chip)
{
    if (chip < preset::feb::nchips_)
        {
            LOG4CPLUS_TRACE(logger_, "FebPart::readVTH for " << *this << "." << (int)chip);

            FebChip & fchip = *(fcs_[chip]);

            unsigned char laddress = (rpct::devices::feb::preset::ad7417_[position_ + febtype()] + address());
            unsigned char data[3] = {0x01, rpct::devices::feb::preset::adc_vth_[chip], 0x04};

            febboard_.enableFebAccess();

            unsigned short _channel(channel());
            i2c::EnsureGroup e_group(&i2c_access_);
            i2c_access_.write(_channel, laddress
                              , &(data[0]), 2);
            i2c_access_.write(_channel, laddress
                              , &(data[2]), 1);
            i2c_access_.delay(18000);
            i2c_access_.read(_channel, laddress
                             , &(fchip.adc_register_vth_), (fchip.vth_read_response_));
            e_group.end();

            febboard_.disableFebAccess();
        }
    else
        LOG4CPLUS_WARN(logger_, "Requested readVTH for out of range chip for " << *this << "." << (int)chip);
}
void FebPart::readVMON(unsigned char chip)
{
    if (chip < preset::feb::nchips_)
        {
            LOG4CPLUS_TRACE(logger_, "FebPart::readVMON for " << *this << "." << (int)chip);

            FebChip & fchip = *(fcs_[chip]);

            unsigned char laddress = (rpct::devices::feb::preset::ad7417_[position_ + febtype()] + address());
            unsigned char data[3] = {0x01, rpct::devices::feb::preset::adc_vmon_[chip], 0x04};

            febboard_.enableFebAccess();

            unsigned short _channel(channel());
            i2c::EnsureGroup e_group(&i2c_access_);
            i2c_access_.write(_channel, laddress
                              , &(data[0]), 2);
            i2c_access_.write(_channel, laddress
                              , &(data[2]), 1);
            i2c_access_.delay(18000);
            i2c_access_.read(_channel, laddress
                             , &(fchip.adc_register_vmon_), (fchip.vmon_read_response_));
            e_group.end();

            febboard_.disableFebAccess();
        }
    else
        LOG4CPLUS_WARN(logger_, "Requested readVMON for out of range chip for " << *this << "." << (int)chip);
}
void FebPart::writeVTH(unsigned char chip)
{
    if (chip < preset::feb::nchips_)
        {
            LOG4CPLUS_TRACE(logger_, "FebPart::writeVTH for " << *this << "." << (int)chip);

            FebChip & fchip = *(fcs_[chip]);
            dac_register_.ptr(rpct::devices::feb::preset::dac_vth_[chip]);
            dac_register_.value(fchip.getVTHSet());

            i2c_access_.write(channel(), rpct::devices::feb::preset::ad5316_
                              , &dac_register_, (fchip.vth_write_response_));
        }
    else
        LOG4CPLUS_WARN(logger_, "Requested writeVTH for out of range chip for " << *this << "." << (int)chip);
}
void FebPart::writeVTH()
{
    uint16_t vth[2] = {fcs_[0]->getVTHSet(), fcs_[1]->getVTHSet()};
    if (fabs(vth[0] - vth[1]) < preset::feb::vth_max_diff_)
        {
            LOG4CPLUS_TRACE(logger_, "FebPart::writeVTH combined for " << *this << ".");

            uint16_t vth_set = (vth[0] + vth[1] + 1) >> 1;
            fcs_[0]->vth_offset_ = vth_set - fcs_[0]->vth_;
            fcs_[1]->vth_offset_ = vth_set - fcs_[1]->vth_;

            dac_register_.ptr(rpct::devices::feb::preset::dac_vth_[0]);
            dac_register_.add_ptr(rpct::devices::feb::preset::dac_vth_[1]);
            dac_register_.value(vth_set);

            i2c_access_.write(channel(), rpct::devices::feb::preset::ad5316_
                              , &dac_register_, vth_write_response_);
        }
    else
        {
            writeVTH(0);
            writeVTH(1);
        }
}
void FebPart::writeVMON(unsigned char chip)
{
    if (chip < preset::feb::nchips_)
        {
            LOG4CPLUS_TRACE(logger_, "FebPart::writeVMON for " << *this << "." << (int)chip);

            FebChip & fchip = *(fcs_[chip]);
            dac_register_.ptr(rpct::devices::feb::preset::dac_vmon_[chip]);
            dac_register_.value(fchip.getVMONSet());

            i2c_access_.write(channel(), rpct::devices::feb::preset::ad5316_
                              , &dac_register_, (fchip.vmon_write_response_));
        }
    else
        LOG4CPLUS_WARN(logger_, "Requested writeVMON for out of range chip for " << *this << "." << (int)chip);
}
void FebPart::writeVMON()
{
    uint16_t vmon[2] = {fcs_[0]->getVMONSet(), fcs_[1]->getVMONSet()};
    if (fabs(vmon[0] - vmon[1]) < preset::feb::vmon_max_diff_)
        {
            LOG4CPLUS_TRACE(logger_, "FebPart::writeVMON combined for " << *this << ".");

            uint16_t vmon_set = (vmon[0] + vmon[1] + 1) >> 1;
            fcs_[0]->vmon_offset_ = vmon_set - fcs_[0]->vmon_;
            fcs_[1]->vmon_offset_ = vmon_set - fcs_[1]->vmon_;

            dac_register_.ptr(rpct::devices::feb::preset::dac_vmon_[0]);
            dac_register_.add_ptr(rpct::devices::feb::preset::dac_vmon_[1]);
            dac_register_.value(vmon_set);

            i2c_access_.write(channel(), rpct::devices::feb::preset::ad5316_
                              , &dac_register_, vmon_write_response_);
        }
    else
        {
            writeVMON(0);
            writeVMON(1);
        }
}

FebPart::FebPart(FebBoard & febboard
                 , rpct::i2c::II2cAccess & i2c_access
                 , unsigned char position
                 , int id
                 , bool disabled)
    : FebSystemItem(febboard.getSystem(), position, &febboard)
    , id_(id)
    , i2c_access_(i2c_access)
    , febboard_(febboard)
    , position_(position)
{
    disable(disabled);
    if (!type_)
        type_ = new HardwareItemType(HardwareItemType::cBoard
                                     , type::feb::febpart_);
    if (!mitems_)
        {
            mitems_ = new MonitorItems();
            mitems_->push_back(monitoritem::feb::temp_);
            mitems_->push_back(monitoritem::feb::vth_);
            mitems_->push_back(monitoritem::feb::vmon_);
        }
    temperature_read_response_ = new rpct::i2c::TI2cResponse<FebPart>(this
                                                                      , &FebPart::i2c_read_temp_ready
                                                                      , &FebPart::i2c_read_temp_error);
    vth_write_response_ = new rpct::i2c::TI2cResponse<FebPart>(this
                                                               , &FebPart::i2c_write_vth_ready
                                                               , &FebPart::i2c_write_vth_error);
    vmon_write_response_ = new rpct::i2c::TI2cResponse<FebPart>(this
                                                                , &FebPart::i2c_write_vmon_ready
                                                                , &FebPart::i2c_write_vmon_error);
    fcs_[0] = new FebChip(*this, 0, -1, true);
    fcs_[1] = new FebChip(*this, 1, -1, true);

    addToSystem();
}

FebPart::~FebPart()
{
    removeFromSystem();
    delete temperature_read_response_;
    delete vth_write_response_;
    delete vmon_write_response_;
    delete fcs_[0];
    delete fcs_[1];
}

void FebPart::reset()
{
    FebSystemItem::reset();
    dac_register_.reset_in();
    adc_register_temperature_.reset_out();
    fcs_[0]->reset();
    fcs_[1]->reset();
}

const std::string & FebPart::getDescription() const
{
    return febboard_.getDescription();
}

void FebPart::configure(ConfigurationSet* configurationSet, int configureFlags)
{
    LOG4CPLUS_DEBUG(logger_, "FebPart::configure()");
    if ((configureFlags & ConfigurationFlags::INCLUDE_DISABLED) || !isDisabled())
        {
            fcs_[0]->configure(configurationSet, configureFlags);
            fcs_[1]->configure(configurationSet, configureFlags);

            if (fcs_[0]->to_be_configured_ || fcs_[1]->to_be_configured_)
                {
                    fcs_[0]->configured();
                    fcs_[1]->configured();
                    fcs_[0]->log("fcconfigured");
                    fcs_[1]->log("fcconfigured");
                    configured();
                    febboard_.enableFebAccess();
                    {
                        RecursiveAction ra(&febboard_);
                        i2c::EnsureGroup eg(&i2c_access_);
                        febboard_.enablePart(position_, false);
                        febboard_.selectPart(position_, true);
                        febboard_.writePCF8574a(true);
                        write();
                        febboard_.enablePart(position_, true);
                        febboard_.selectPart(position_, false);
                        febboard_.writePCF8574a();
                        eg.end();
                    }
                    febboard_.disableFebAccess();
                    log("configured");
                }
            else
                LOG4CPLUS_DEBUG(logger_, *this << "not configured, chips don't need configuration.");
        }
    else
        LOG4CPLUS_DEBUG(logger_, *this << "not configured, it's disabled.");
}

void FebPart::configure(int configureFlags)
{
    LOG4CPLUS_DEBUG(logger_, "FebPart::configure()");
    if ((configureFlags & ConfigurationFlags::INCLUDE_DISABLED) || !isDisabled())
        {
            fcs_[0]->configure();
            fcs_[1]->configure();

            if (fcs_[0]->to_be_configured_ || fcs_[1]->to_be_configured_)
                {
                    fcs_[0]->configured();
                    fcs_[1]->configured();
                    fcs_[0]->log("fcconfigured");
                    fcs_[1]->log("fcconfigured");
                    configured();
                    febboard_.enableFebAccess();
                    {
                        RecursiveAction ra(&febboard_);
                        i2c::EnsureGroup eg(&i2c_access_);
                        febboard_.enablePart(position_, false);
                        febboard_.selectPart(position_, true);
                        febboard_.writePCF8574a(true);
                        write();
                        febboard_.enablePart(position_, true);
                        febboard_.selectPart(position_, false);
                        febboard_.writePCF8574a();
                        eg.end();
                    }
                    febboard_.disableFebAccess();
                    log("configured");
                }
            else
                LOG4CPLUS_DEBUG(logger_, *this << "not configured, chips don't need configuration.");
        }
    else
        LOG4CPLUS_DEBUG(logger_, *this << "not configured, it's disabled.");
}

void FebPart::loadConfiguration(ConfigurationSet* configurationSet, int configureFlags)
{
    LOG4CPLUS_DEBUG(logger_, "FebPart::loadConfiguration()");
    if ((configureFlags & ConfigurationFlags::INCLUDE_DISABLED) || !isDisabled())
        {
            fcs_[0]->loadConfiguration(configurationSet, configureFlags);
            fcs_[1]->loadConfiguration(configurationSet, configureFlags);

            fcs_[0]->configured(0);
            fcs_[1]->configured(0);
            fcs_[0]->unlog("fcconfigured");
            fcs_[1]->unlog("fcconfigured");
            configured(0);
            unlog("configured");
        }
    else
        LOG4CPLUS_DEBUG(logger_, "Configuration for " << *this << " was not loaded, it's disabled.");
}

void FebPart::monitor(MonitorItems & items, int configureFlags)
{
    LOG4CPLUS_DEBUG(logger_, "FebPart::monitor()");
    if ((configureFlags & ConfigurationFlags::INCLUDE_DISABLED) || !isDisabled())
        {
            {
                RecursiveAction ra(&febboard_);

                MonitorItems::iterator ImiEnd = items.end();
                MonitorItems::iterator ImiTemp = std::find(items.begin(), ImiEnd, monitoritem::feb::temp_);
                MonitorItems::iterator ImiVTH = std::find(items.begin(), ImiEnd, monitoritem::feb::vth_);
                MonitorItems::iterator ImiVMON = std::find(items.begin(), ImiEnd, monitoritem::feb::vmon_);

                if (ImiTemp != ImiEnd)
                    readTemperature();
                bool vth = (ImiVTH != ImiEnd);
                bool vmon = (ImiVMON != ImiEnd);
                if (vth || vmon)
                    {
                        fcs_[0]->monitor(vth, vmon, configureFlags);
                        fcs_[1]->monitor(vth, vmon, configureFlags);
                    }
            }
            febboard_.disableFebAccess();
            log("monitored");
        }
    else
        LOG4CPLUS_DEBUG(logger_, *this << "not monitored, it's disabled.");
}

void FebPart::printParameters(tools::Parameters & parameters) const
{
    parameters.add("position", (int)(position_));
    parameters.add("temperature", adc_register_temperature_.temperature());
}

FebChip & FebPart::addFebChip(int id
                              , unsigned char position
                              , bool disabled)
    throw (rpct::exception::SystemException)
{
    LOG4CPLUS_DEBUG(logger_, "FebBoard::addFebChip");
    if (position < preset::feb::nchips_)
        {
            delete fcs_[position];
            fcs_[position] = new FebChip(*this, position, id, disabled);
            disable(fcs_[0]->isDisabled() || fcs_[1]->isDisabled()); // 20110307: OR of the FebChip disabling
            return *(fcs_[position]);
        }
    else
        {
            std::stringstream error("FebChip ");
            error << position << " out of range for " << *this;
            throw rpct::exception::SystemException(error.str());
        }
}

void FebPart::i2c_read_temp_error(rpct::exception::Exception const * e)
    throw ()
{
    LOG4CPLUS_TRACE(logger_, "FebPart::readTemperature error for " << *this << ", " << std::hex << (int)(adc_register_temperature_.out()[0]) << " " << (int)(adc_register_temperature_.out()[1]) << std::dec);
    log("i2c_rd_febpart_temp");
    unlog("febpart_temp_diff");
    if (e)
        {
            LOG4CPLUS_WARN(logger_, "Error in i2c access to "
                           << *this << ": " << e->what());
        }
    else
        {
            LOG4CPLUS_ERROR(logger_, "Unknown error in i2c access to " << *this);
        }
}
void FebPart::i2c_read_temp_ready()
    throw ()
{
    LOG4CPLUS_TRACE(logger_, "FebPart::readTemperature ready for " << *this << ", " << std::hex << (int)(adc_register_temperature_.out()[0]) << " " << (int)(adc_register_temperature_.out()[1]) << std::dec);
    unlog("i2c_rd_febpart_temp");
    float value(getTemperature());
    if (value > rpct::preset::feb::max_temp_ || value < rpct::preset::feb::min_temp_)
        log("febpart_temp_diff");
    else
        unlog("febpart_temp_diff");
    getSystem().publish(getId(), rpct::publish::type::feb::temp_, value);
}

void FebPart::i2c_write_vth_error(rpct::exception::Exception const * e)
    throw ()
{
    fcs_[0]->i2c_write_vth_error(e);
    fcs_[1]->i2c_write_vth_error(e);
}
void FebPart::i2c_write_vth_ready()
    throw ()
{
    fcs_[0]->i2c_write_vth_ready();
    fcs_[1]->i2c_write_vth_ready();
}
void FebPart::i2c_write_vmon_error(rpct::exception::Exception const * e)
    throw ()
{
    fcs_[0]->i2c_write_vmon_error(e);
    fcs_[1]->i2c_write_vmon_error(e);
}
void FebPart::i2c_write_vmon_ready()
    throw ()
{
    fcs_[0]->i2c_write_vmon_ready();
    fcs_[1]->i2c_write_vmon_ready();
}

} // namespace rpct
