#include "rpct/devices/feb_const.h"

namespace rpct {
namespace devices {
namespace feb {
namespace preset {

const unsigned int monitorcycle_ = 1800;

// we want 1000mV as max, all offsets are about within 50mV=20u, which would make the minimum maximum about 360u=880mV
const uint16_t vth_max_ = 380;

const float dac_unit_ = 2.;
const float inv_dac_unit_ = .5;
// vth, vmon
const float adc_unit_[2] = {1., 2.};
const float inv_adc_unit_[2] = {1., .5};

// single feb, double feb part 0, double feb part 1
const unsigned char sel_dac_[3] = {0x01, 0x01, 0x02};
const unsigned char en_dac_ [3] = {0x02, 0x04, 0x08};

const unsigned char ad7417_[3]  = {0x28, 0x28, 0x29}; // + address_
const unsigned char pcf8574a_   = 0x38; // + address_
const unsigned char pca9544_    = 0x70;
const unsigned char pca9544_dt_ = 0x71;
const unsigned char ad5316_     = 0x0c;

const unsigned char adc_temp_ = 0x00;
// part 0, part 1
const unsigned char adc_vth_ [4] = {0x20, 0x40, 0x20, 0x40};
const unsigned char adc_vmon_[4] = {0x60, 0x80, 0x60, 0x80};
const unsigned char dac_vth_ [4] = {0x01, 0x02, 0x01, 0x02};
const unsigned char dac_vmon_[4] = {0x04, 0x08, 0x04, 0x08};
const unsigned char dac_vth_combined_ = 0x03; // = dac_vth_[0] + dac_vth_[1]
const unsigned char dac_vmon_combined_ = 0x0c; // = dac_vmon_[0] + dac_vmon_[1]

} // namespace preset
} // namespace feb
} // namespace devices
} // namespace rpct
