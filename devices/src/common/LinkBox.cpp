#include "rpct/devices/LinkBox.h"
#include "rpct/devices/Rbc.h"
#include "rpct/devices/LinkBoard.h"
#include "rpct/devices/cb.h"
#include "rpct/devices/test/cbMock.h"
#include "rpct/devices/LinkBoxFlashConfigurator.h"
#include "rpct/devices/LinkSystem.h"
#include "rpct/std/IllegalArgumentException.h"
#include "rpct/std/NullPointerException.h"
#include "rpct/ii/DeviceSettings.h"
#include "rpct/devices/FixedHardwareSettings.h"
#include "rpct/devices/IFebAccessPoint.h"
#include <xercesc/parsers/SAXParser.hpp>
#include <sstream>

#include "xoap/domutils.h"
#include "xdaq/exception/Exception.h"

using namespace std;
using namespace xercesc;
using namespace log4cplus;

namespace rpct {

Logger LinkBox::logger = Logger::getInstance("LinkBox");
void LinkBox::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}
Logger HalfBox::logger = Logger::getInstance("HalfBox");
void HalfBox::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}
//unsigned int HalfBox::monitoringIteration = 0;

void HalfBox::addBoard(IBoard& board) {
    boards.push_back(&board);
    if (dynamic_cast<MasterLinkBoard*>(&board) != 0) {
        masterLinkBoards.push_back(dynamic_cast<MasterLinkBoard*>(&board));
    }
    if (dynamic_cast<LinkBoard*>(&board) != 0) {
        linkBoards.push_back(dynamic_cast<LinkBoard*>(&board));
    }
    if (dynamic_cast<Rbc*>(&board) != 0) {
        rbc=dynamic_cast<Rbc*>(&board);
    }
}

LinkBoard* HalfBox::getLinkBoardByPosition(int pos) {
    for (LinkBoards::iterator iLinkBoard = linkBoards.begin(); iLinkBoard
            != linkBoards.end(); ++iLinkBoard) {
        if ((*iLinkBoard)->getPosition() == pos) {
            return *iLinkBoard;
        }
    }
    return 0;
}

HalfBox::~HalfBox() {
    LOG4CPLUS_DEBUG(logger, "Destroying HalfBox!!!!!!");
/*
    int id = getLinkBox().getId();
    string desc = getLinkBox().getDescription();
    LOG4CPLUS_DEBUG(logger, "Destroying HalfBox for LinkBox id = "<< id << " desc = "<< desc
            << ": Start" );
*/
    for (LinkBoards::iterator iLinkBoard = linkBoards.begin(); iLinkBoard != linkBoards.end(); ++iLinkBoard) {
//        int bid = (*iLinkBoard)->getId();
        //if(*iLinkBoard) { delete *iLinkBoard; *iLinkBoard=0; }
/*
        LOG4CPLUS_DEBUG(logger, "Destroying LinkBoard id = " << bid
                << ": Done" );
*/
    }
    for (MasterLinkBoards::iterator iMasterLinkBoard = masterLinkBoards.begin(); iMasterLinkBoard != masterLinkBoards.end(); ++iMasterLinkBoard) {
//        int bid = (*iMasterLinkBoard)->getId();
        //if(*iMasterLinkBoard) { delete *iMasterLinkBoard; *iMasterLinkBoard=0; }
/*
        LOG4CPLUS_DEBUG(logger, "Destroying MasterLinkBoard id = " << bid
                << ": Done" );
*/
    }
/*
    LOG4CPLUS_DEBUG(logger, "Destroying HalfBox for LinkBox id = "<< id << " desc = "<< desc
            << ": Done" );
*/
}

bool HalfBox::prepareAccess() {
	if(lboxAccess.isTTCHardResetEnabled() == false)
		return false;
	LOG4CPLUS_INFO(logger, "HalfBox::"<<__FUNCTION__<<" started, Hard Reset disabled");
	lboxAccess.piaWrite(1, 0xcf);
	usleep(FixedHardwareSettings::CONFIGURE_FROM_FLASH_TIME * 2);//to wait until the Hard Reset started just before a moment ended
	TRPCCCUI2CMasterCore& i2c =  dynamic_cast<TRPCCCUI2CMasterCore&> (getCb().getI2C());
	if(i2c.ReadControlReg() == 0) {
		LOG4CPLUS_INFO(logger, "HalfBox::"<<__FUNCTION__<<": Hard Reset detected on "<<getCb().getDescription()<<" i2c.ReadControlReg() == 0. Initializing the I2Cs ");
		getCb().initI2C();

		for (LinkBoards::iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); iLB++) {
			(*iLB)->initI2C();
		}
	}
        return true;
}

bool HalfBox::finaliseAccess() {
	if(lboxAccess.isTTCHardResetEnabled() == false)
		return false;
	lboxAccess.piaWrite(1, 0xc7);
	LOG4CPLUS_INFO(logger, "HalfBox::"<<__FUNCTION__<<" Hard Reset enabled");
        return true;
}

void HalfBox::monitor(volatile bool *stop) {
	if(*stop) {
		LOG4CPLUS_WARN(logger, "monitor() stopped before any boards monitored");
		return;
	}

	LOG4CPLUS_INFO(logger, "monitoring HalfBox "<<getCb().getDescription()<<" monitoringIteration "<<monitoringIteration);
	prepareAccess();

	for (ICrate::Boards::iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {
		if(*stop) {
			LOG4CPLUS_WARN(logger, "HalfBox::monitor stopped before all boards monitored");
			break;
		}

		IBoard* board = (*iBoard);
		IMonitorable* monitorable = dynamic_cast<IMonitorable*> (board);
		if (monitorable != 0) {
			monitorable->monitor(stop);
		}
		else {
			LOG4CPLUS_WARN(logger, board->getDescription() <<"is not IMonitorable !!!!!!!!!!!!!!!!!!!!!!!!!!!");
		}
	}

	if(*stop) {
		LOG4CPLUS_WARN(logger, "monitor() stopped before FEBs monitored");
	}
	else {
		if(getLinkBox().getLinkSystem()->getFebSystem() != 0) {
			//LOG4CPLUS_INFO(logger, "HalfBox::"<<__FUNCTION__<<" "<<__LINE__);
			rpct::IFebAccessPoint * fap = getLinkBox().getLinkSystem()->getFebSystem()->getFebAccessPoint(cb.getId());
			if (fap) {
				if(monitoringIteration%12 == getNumberInCCUring() ) {
					LOG4CPLUS_INFO(logger, "HalfBox::"<<__FUNCTION__<<" Monitoring FEBs from "<<fap->getDescription()<<" monitoringIteration "<<monitoringIteration);
					fap->monitor(stop);
				}
			}
		}
	}

	finaliseAccess();
	monitoringIteration++;
        // _access_handler.finalise();
}

unsigned int HalfBox::getNumberInCCUring() {
	unsigned int ccuAddress = lboxAccess.getCcuAddress();
	unsigned int num = (ccuAddress >> 4) & 0xf;
	num = (num - 1) * 2;
	if( (ccuAddress & 0xf) == 0x5)
		num+=1;

	LOG4CPLUS_INFO(logger, __FUNCTION__<<" "<<getCb().getDescription()<<" CcuAddress "<<hex<<lboxAccess.getCcuAddress()<<" numberInCCUring "<<num);
	return num;
}
////////////////////////////////////////////////////////// MC - 3 Feb 2010 (start)
/* these are now obsolete:
void HalfBox::select(bool state) {
    skipPositions_.clear();
    if(!state) for(ICrate::Boards::iterator ibd=boards.begin(); ibd!=boards.end(); ibd++) {
        skipPositions_.insert((*ibd)->getPosition());
    }
}
bool HalfBox::isSelected() {
    LOG4CPLUS_DEBUG(logger, __FUNCTION__<<": started (Halfbox="<< getCb().getDescription()
                                       << " skipped="<<skipPositions_.size()<<" all="<<boards.size()
                                       <<" isSelected="<<(skipPositions_.size()!=boards.size())<<")");
    return (skipPositions_.size()!=boards.size());
}
void HalfBox::selectPosition(int pos, bool state) {
    LOG4CPLUS_DEBUG(logger, __FUNCTION__<<": started (Halfbox="<< getCb().getDescription() << " pos="<<pos<<" state="<<state<<")");
    for(ICrate::Boards::iterator ibd=boards.begin(); ibd!=boards.end(); ibd++) {
        if(pos==(*ibd)->getPosition()) {
            if(state) skipPositions_.erase(pos);   // use this board
            else      skipPositions_.insert(pos);  // skip this board
        }
    }
    LOG4CPLUS_DEBUG(logger, __FUNCTION__<<": done (Halfbox="<< getCb().getDescription() << " pos="<<pos<<" state="<<state<<")");
}
bool HalfBox::isPositionSelected(int pos) {
    return (find(skipPositions_.begin(), skipPositions_.end(), pos)==skipPositions_.end());
}

IBoard* HalfBox::getBoardByPosition(int pos) {
    for (ICrate::Boards::iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {
        if ((*iBoard)->getPosition() == pos) {
            return *iBoard;
        }
    }
    return 0;
}
*/
////////////////////////////////////////////////////////// MC - 3 Feb 2010 (end)

HalfBoxAccessHandler::HalfBoxAccessHandler(HalfBox & _halfbox, bool _prepare)
    : halfbox_(_halfbox)
    , initiated_access_(false)
{
    if (_prepare)
        initiated_access_ = halfbox_.prepareAccess();
}

HalfBoxAccessHandler::HalfBoxAccessHandler(HalfBoxAccessHandler const & _halfboxaccesshandler)
    : halfbox_(_halfboxaccesshandler.halfbox_)
    , initiated_access_(_halfboxaccesshandler.initiated_access_)
{
    _halfboxaccesshandler.initiated_access_ = false;
}

HalfBoxAccessHandler::~HalfBoxAccessHandler()
{
    if (initiated_access_)
        halfbox_.finaliseAccess();
}

bool HalfBoxAccessHandler::initiatedAccess() const
{
    return initiated_access_;
}

bool HalfBoxAccessHandler::prepare()
{
    if (!initiated_access_)
        initiated_access_ = halfbox_.prepareAccess();
    return initiated_access_;
}

bool HalfBoxAccessHandler::finalise()
{
    if (initiated_access_)
        {
            halfbox_.finaliseAccess();
            initiated_access_ = false;
            return true;
        }
    return false;
}

const HardwareItemType LinkBox::TYPE(HardwareItemType::cCrate, "LINKBOX");

LinkBox::LinkBox(int ident, const char* desc, LinkSystem* linkSystem, StdLinkBoxAccess* lboxaccess10,
                 StdLinkBoxAccess* lboxaccess20, int cb10Id, int cb20Id, bool mock, std::string const & _label)
    : Crate(ident, desc, _label), linkSystem_(linkSystem), halfBox10(0), halfBox20(0) {
 //   cout << "creating LinkBox id = "<< ident << " desc = "<< desc << endl;
    LOG4CPLUS_DEBUG(logger, "Creating LinkBox id = "<< ((int)ident)<< " desc = "<< ((char*)desc)
            <<": Started");
    if (lboxaccess10 != 0) {
        ICB* cb;
        if (mock) {
            cb = new TCBMock(cb10Id, (getDescription() + "_CB_10").c_str(), this);
        } else {
            cb = new TCB(cb10Id, (getDescription() + "_CB_10").c_str(), this, 10);
        }
        lboxaccess10->setName(cb->getDescription());
        boards_.push_back(cb);
        LOG4CPLUS_DEBUG(logger, "Creating LinkBox id = "<< ((int)ident)<< " desc = "<< ((char*)desc)
                << ": HalfBox 10" );
        halfBox10 = new HalfBox(*this, *lboxaccess10, *cb);
        ////////////////////////////////////////////////////////// MC - 17 Aug 2009
        halfBox10->addBoard(*cb);
        ///////////////////////////////////////////////////////////////////////////
    }
    if (lboxaccess20 != 0) {
        ICB* cb;
        if (mock) {
            cb = new TCBMock(cb20Id, (getDescription() + "_CB_20").c_str(), this);
        } else {
            cb = new TCB(cb20Id, (getDescription() + "_CB_20").c_str(), this, 20);
        }
        lboxaccess20->setName(cb->getDescription());
        boards_.push_back(cb);
        LOG4CPLUS_DEBUG(logger, "Creating LinkBox id = "<< ((int)ident)<< " desc = "<< ((char*)desc)
                << ": HalfBox 20" );
        halfBox20 = new HalfBox(*this, *lboxaccess20, *cb);
        ////////////////////////////////////////////////////////// MC - 17 Aug 2009
        halfBox20->addBoard(*cb);
        ///////////////////////////////////////////////////////////////////////////
    }
    LOG4CPLUS_DEBUG(logger, "Creating LinkBox id = "<< ((int)ident)<< " desc = "<< ((char*)desc)
            << ": Done" );
}

LinkBox::~LinkBox() {
    int id = getId();
    string desc = getDescription();
    LOG4CPLUS_DEBUG(logger, "Destroying LinkBox id = "<< id << " desc = "<< desc << ": Start" );
    for (Boards::iterator iBoard = boards_.begin(); iBoard != boards_.end(); ++iBoard) {
        int id = (*iBoard)->getId();
        if (*iBoard) { delete *iBoard; *iBoard=0; }
        LOG4CPLUS_DEBUG(logger, "Destroying Board id = "<< id <<": Done" );
    }
    if(halfBox10) {
        LOG4CPLUS_DEBUG(logger, "Destroying HalfBox 10: Start");
        delete halfBox10; halfBox10=0;
        LOG4CPLUS_DEBUG(logger, "Destroying HalfBox 10: Done" );
    }
    if(halfBox20) {
        LOG4CPLUS_DEBUG(logger, "Before destroying HalfBox 20: Start");
        delete halfBox20; halfBox20=0;
        LOG4CPLUS_DEBUG(logger, "Destroying HalfBox 20: Done" );
    }
    LOG4CPLUS_DEBUG(logger, "Destroying LinkBox id = "<< id << " desc = "<< desc << ": Done" );
}
/*
LinkBox::~LinkBox() {
    for (Boards::iterator iBoard = boards_.begin(); iBoard != boards_.end(); ++iBoard) {
        delete *iBoard;
    }
}
void LinkBox::checkVersions() {
    typedef IBoard::Devices Devices;
    bool error = false;
    std::ostringstream ost;
    for (Boards::iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {
        const Devices& devices = (*iBoard)->getDevices();
        for (Devices::const_iterator iDevice = devices.begin(); iDevice
                != devices.end(); ++iDevice) {
            TIIDevice* iidev = dynamic_cast<TIIDevice*>(*iDevice);
            if (iidev != NULL) {
                try {
                    iidev->checkName();
                    iidev->checkVersion();
                    iidev->checkChecksum();
                }
                catch(TException& e) {
                    error = true;
                    ost << e.what() << "\n";
                }
            }
        }
    }
    if (error) {
        throw EBadDeviceVersion(ost.str());
    }
}*/

LinkBoard& LinkBox::addLinkBoard(int pos, int id, const char* desc,
        int syncoderId) {
    //cout << "LinkBoard& LinkBox::addLinkBoard pos = "<< pos << ", id = "<< id
    //        << ", desc = "<< desc << endl;
    LinkBoard* linkBoard;
    switch (pos) {
    case 1:
    case 4:
    case 7:
    case 11:
    case 14:
    case 17:
        linkBoard = new SlaveLinkBoard(id, desc, this, pos, msLeft, syncoderId);
        break;
    case 3:
    case 6:
    case 9:
    case 13:
    case 16:
    case 19:
        linkBoard = new SlaveLinkBoard(id, desc, this, pos, msRight, syncoderId);
        break;
    case 2:
    case 5:
    case 8:
    case 12:
    case 15:
    case 18:
        linkBoard = new MasterLinkBoard(id, desc, this, pos, syncoderId);
        break;
    default:
        throw IllegalArgumentException("LinkBox::addLinkBoard: invalid position " + rpct::toString(pos));
    }
    //cout << "LinkBoard created"<< endl;
    boards_.push_back(linkBoard);
    HalfBox* halfBox = getHalfBox(pos);
    if (halfBox == 0) {
        throw rpct::IllegalArgumentException("Invalid position " + toString(id));
    }
    halfBox->addBoard(*linkBoard);
    return *linkBoard;
}

////////////////////////////////////////////////////// MC - 3 Feb 2010 (start)
/* these are now obsolete:
void LinkBox::intiTTCrxs() {
	LOG4CPLUS_INFO(logger, getDescription() <<": intiTTCrxs");
    Boards& boards = getBoards();
    for (Boards::iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {
    	LinkBoard* lb = dynamic_cast<LinkBoard*>(*iBoard);
        ////////////////////////////////////////////////////////// MC - 17 Aug 2009
        if(lb && !(lb->getHalfBox()->isSelected())) {
            LOG4CPLUS_INFO(logger, __FUNCTION__<<": skipped board "<<lb->getDescription());
            continue;
        }
        ////////////////////////////////////////////////////////////////////////////
        if(lb != 0) {
    		lb->setTtcRxI2cAddress();
    		lb->getSynCoder().writeBits(SynCoder::BITS_STATUS2_TTC_INIT_ENA, 1); //TODO maybe it can be done in one operation
    		lb->getSynCoder().writeBits(SynCoder::BITS_STATUS2_TTC_RESET, 1); //but it is possiblem that the TTC_INIT_ENA should be set before the reset
    	}
    }

    tbsleep(3000);

    for (Boards::iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {
    	LinkBoard* lb = dynamic_cast<LinkBoard*>(*iBoard);
        ////////////////////////////////////////////////////////// MC - 17 Aug 2009
        if(lb && !(lb->getHalfBox()->isSelected())) {
            LOG4CPLUS_INFO(logger, __FUNCTION__<<": skipped board "<<lb->getDescription());
            continue;
        }
        ////////////////////////////////////////////////////////////////////////////
        if(lb != 0) {
    	    if (lb->getSynCoder().readBits(SynCoder::BITS_MONIT_TTC_READY) != 0) {
    	        throw TException(lb->getDescription() + " SynCoder::ResetTTC: BITS_STATUS_TTC_READY did not go down after BITS_STATUS_TTC_RESET = 1");
    	    }
    	    lb->getSynCoder().writeBits(SynCoder::BITS_STATUS2_TTC_RESET, 0);
    	    lb->getSynCoder().writeBits(SynCoder::BITS_STATUS2_TTC_INIT_ENA, 0);//TODO this rather CANNOT be done in one operation
    	    LOG4CPLUS_DEBUG(logger, lb->getDescription() + " ResetTTC Done");
    	}
    }

    tbsleep(500);

    for (Boards::iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {
    	LinkBoard* lb = dynamic_cast<LinkBoard*>(*iBoard);
        ////////////////////////////////////////////////////////// MC - 17 Aug 2009
        if(lb && !(lb->getHalfBox()->isSelected())) {
            LOG4CPLUS_INFO(logger, __FUNCTION__<<": skipped board "<<lb->getDescription());
            continue;
        }
        ////////////////////////////////////////////////////////////////////////////
        if(lb != 0) {
    	    if (lb->getSynCoder().readBits(SynCoder::BITS_MONIT_TTC_READY) != 1) {
    	        throw TException(lb->getDescription() + " SynCoder::ResetTTC: BITS_STATUS_TTC_READY did not go up after reset");
    	    }

    	    try {
    	        int controlRegValue = FixedHardwareSettings::LB_TTC_RX_CONTROL_REG; //lb->getTtcRxI2c().ReadControlReg() | 0x8;
    	        LOG4CPLUS_DEBUG(logger, lb->getDescription() + " controlRegValue " << hex << controlRegValue);
    	        lb->getTtcRxI2c().WriteControlReg(controlRegValue);
    	        //lb->getTtcRxI2c().WriteCoarseDelay(0, 0); //TODO not neede, after reset should be 0, KB
    	    }
    	    catch(EI2C& e) {
                LOG4CPLUS_WARN(logger, lb->getDescription() << " error accessing TTCrx via I2C. " << e.what() << ". Reseting the TTCrx and Trying one again");
                lb->intiTTCrx(); //repeat the reset procedure, sets also the ControlReg
                //lb->getTtcRxI2c().WriteCoarseDelay(0, 0); //TODO not neede, after reset should be 0, KB
    	    }
    	}
    }

    LOG4CPLUS_INFO(logger, getDescription() <<": intiTTCrxs done");
}

void LinkBox::resetQPLLs() {
    Boards& boards = getBoards();
    LOG4CPLUS_INFO(logger, getDescription() <<": resetQPLL");
    // The pin autoRestart is connected to '+' on the PCB
    // The pin externalControl is connected to '0' on the PCB
    // The pins f0selct <0..3> are not connected, internal pull ups and downs
    for (Boards::iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {
    	LinkBoard* lb = dynamic_cast<LinkBoard*>(*iBoard);
        ////////////////////////////////////////////////////////// MC - 17 Aug 2009
        if(lb && !(lb->getHalfBox()->isSelected())) {
            LOG4CPLUS_INFO(logger, __FUNCTION__<<": skipped board "<<lb->getDescription());
            continue;
        }
        ////////////////////////////////////////////////////////////////////////////
        if(lb != 0) {
    		lb->getSynCoder().writeBits(SynCoder::BITS_QPLL_MODE, 1); //mode = 1: 160 MHz frequency multiplication mode (160 MHz quartz crystal required).
    		lb->getSynCoder().writeBits(SynCoder::BITS_QPLL_RESET_ENA, 1);
    	}
    }

    tbsleep(500);

    for (Boards::iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {
    	LinkBoard* lb = dynamic_cast<LinkBoard*>(*iBoard);
        ////////////////////////////////////////////////////////// MC - 17 Aug 2009
        if(lb && !(lb->getHalfBox()->isSelected())) {
            LOG4CPLUS_INFO(logger, __FUNCTION__<<": skipped board "<<lb->getDescription());
            continue;
        }
        ////////////////////////////////////////////////////////////////////////////
        if(lb != 0) {
    		lb->getSynCoder().writeBits(SynCoder::BITS_QPLL_RESET_ENA, 0);
    	}
    }

    tbsleep(500);

    for (Boards::iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {
    	LinkBoard* lb = dynamic_cast<LinkBoard*>(*iBoard);
        ////////////////////////////////////////////////////////// MC - 17 Aug 2009
        if(lb && !(lb->getHalfBox()->isSelected())) {
            LOG4CPLUS_INFO(logger, __FUNCTION__<<": skipped board "<<lb->getDescription());
            continue;
        }
        ////////////////////////////////////////////////////////////////////////////
        if(lb != 0) {
    	    if (lb->getSynCoder().readBits(SynCoder::BITS_MONIT_QPLL_LOCKED) != 1) {
    	        throw TException(lb->getDescription() + " SynCoder::ResetQPLL: BITS_QPLL_LOCKED did not go up");
    	    }
    	}
    }

    LOG4CPLUS_INFO(logger, getDescription() <<": resetQPLL Done");
}

void LinkBox::beforeConfigure(bool cold) {
	LOG4CPLUS_INFO(logger, getDescription() <<": beforeConfigure");
	//Boards& boards = getBoards();
	//for (Boards::iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {
	//	LinkBoard* lb = dynamic_cast<LinkBoard*>(*iBoard);
	//	if (lb == 0) { //boards other than LB
	//		(*iBoard)->init();
	//	}
	//}

	//for LBs only
	//intiTTCrxs();
	//resetQPLLs();

    for (Boards::iterator iBoard = boards_.begin(); iBoard != boards_.end(); ++iBoard) {
    	MasterLinkBoard* mlb = dynamic_cast<MasterLinkBoard*>(*iBoard);
        ////////////////////////////////////////////////////////// MC - 17 Aug 2009
        if(mlb && !(mlb->getHalfBox()->isSelected())) {
            LOG4CPLUS_INFO(logger, __FUNCTION__<<": skipped board "<<mlb->getDescription());
            continue;
        }
        ////////////////////////////////////////////////////////////////////////////
        if(mlb != 0) {
    		mlb->resetGOL();
    	}
    }
    LOG4CPLUS_INFO(logger, getDescription() <<": resetGOLs Done");

    for (Boards::iterator iBoard = boards_.begin(); iBoard != boards_.end(); ++iBoard) {
    	Rbc* rbc = dynamic_cast<Rbc*>(*iBoard);

        //if(rbc && !(rbc->getHalfBox()->isSelected())) { //TODO - correct somehow
    	//	LOG4CPLUS_INFO(logger, __FUNCTION__<<": skipped board "<<rbc->getDescription());
    	//	continue;
    	//}
    	////////////////////////////////////////////////////////////////////////////
    	if(rbc != 0) {
    		rbc->reset();
    		LOG4CPLUS_INFO(logger, getDescription() <<": RBC reset done")
    	}
    }

	LOG4CPLUS_INFO(logger, getDescription() <<": beforeConfigure done");
}

void LinkBox::programFlash() {
    LinkBoxFlashConfigurator flashConf(*this, "flash");
    flashConf.configStart();
    try {
        flashConf.intiTTCrxs();
        flashConf.configEnd();
    }
    catch(...) {
        flashConf.configEnd();
        throw;
    }
}

SynCoder& LinkBox::getCheckWarmSyncoder() {
    for (int i = 1; i < 20; i++) {
        HalfBox* hb = getHalfBox(i);
        if (hb != 0) {
            LinkBoard* lb = hb->getLinkBoardByPosition(i);
            ////////////////////////////////////////////////////////// MC - 17 Aug 2009
            if(lb && !(lb->getHalfBox()->isSelected())) {
                LOG4CPLUS_INFO(logger, __FUNCTION__<<": skipped board "<<lb->getDescription());
                continue;
            }
            ////////////////////////////////////////////////////////////////////////////
            if(lb != 0) {
                return lb->getSynCoder();
            }
        }
    }
    throw TException("LinkBox::getCheckWarmSyncoder(): syncoder not found");
}
*/
////////////////////////////////////////////////////// MC - 3 Feb 2010 (end)

bool LinkBox::checkWarm(ConfigurationSet* configurationSet) {
    //unsigned int configId = configurationSet->getId();
    //SynCoder& sc = getCheckWarmSyncoder();
    //sc.writeWord(SynCoder::WORD_)
    return false;
}

void LinkBox::rememberConfiguration(ConfigurationSet* configurationSet) {
    //unsigned int configId = configurationSet->getId();
    //SynCoder& sc = getCheckWarmSyncoder();
    //sc.writeWord(SynCoder::WORD_)
}

/*
void LinkBox::configure(ConfigurationSet* configurationSet, int configureFlags) {
    bool resetOnlyWhenNoConfigData = configureFlags & RESET_ONLY_WHEN_NO_CONFIG_DATA;
    if (configurationSet == 0 && !resetOnlyWhenNoConfigData) {
        throw NullPointerException("VmeCrate::configure: configurationSet is null");
    }

    init();

    for (Boards::iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {
        IBoard* board = *iBoard;
        LOG4CPLUS_INFO(logger, "Configuring board " << board->getDescription());
        board->selfTest();
        const IBoard::Devices& devices = board->getDevices();
        for (IBoard::Devices::const_iterator iDevice = devices.begin(); iDevice != devices.end(); iDevice++) {
            IDevice* device = *iDevice;

            if (configurationSet == 0) {
                device->reset();
                LOG4CPLUS_INFO(logger, "configure: " << device->getDescription()<< " no deviceSettings for chip, chip was reset, but not configured");
            }
            else {
                DeviceSettings* deviceSettings = configurationSet->getDeviceSettings(*device);
                if (resetOnlyWhenNoConfigData) {
                    if(deviceSettings != 0)
                        device->configure(deviceSettings);
                    else {
                        device->reset();
                        LOG4CPLUS_INFO(logger, "configure: " << device->getDescription()<< " no deviceSettings for chip, chip was reset, but not configured");
                    }
                }
                else {
                    device->configure(deviceSettings);
                }
            }
        }
    }
}
*/

IBoard* LinkBox::getBoardByPosition(int pos) {
    for (Boards::iterator iBoard = boards_.begin(); iBoard != boards_.end(); ++iBoard) {
        if ((*iBoard)->getPosition() == pos) {
            return *iBoard;
        }
    }
    return 0;
}

bool LinkBox::boardSortPredicate(rpct::IBoard *b1, rpct::IBoard *b2)
{
    try {
        if(b1==0 || b2==0) throw TException("b1 or b2 is null");
        int pos1 = b1->getPosition();
        int pos2 = b2->getPosition();
        if(pos1==0 || pos2==0) throw TException("pos1 or pos2 is 0");
        LinkBox* lbb1 = dynamic_cast<LinkBox*> (b1->getCrate());
        LinkBox* lbb2 = dynamic_cast<LinkBox*> (b2->getCrate());
        if(lbb1==0 || lbb2==0) throw TException("lbb1 or lbb2 is null");
        HalfBox* hb1 = lbb1->getHalfBox(pos1);
        HalfBox* hb2 = lbb2->getHalfBox(pos2);
        if(hb1==0 || hb2==0) throw TException("hb1 or hb2 is null");
        // boards are in different halfboxes
        if( (hb1->getLBoxAccess()).getCcuAddress() < (hb2->getLBoxAccess()).getCcuAddress() ) return true;
        if( (hb1->getLBoxAccess()).getCcuAddress() > (hb2->getLBoxAccess()).getCcuAddress() ) return false;
        // boards are in the same halfbox
        bool isCb1 = ( dynamic_cast<TCB*>(b1) !=0 );
        bool isCb2 = ( dynamic_cast<TCB*>(b2) !=0 );
        // for {LB,CB} or {CB,LB} pairs put CB before LB
        if ( isCb1 && !isCb2 ) return true;
        if ( !isCb1 && isCb2 ) return false;
        // for {LB,LB} or {CB,CB} pairs order according to ascending position
        return (pos1<pos2);
    } catch (std::exception& e) {
        LOG4CPLUS_ERROR(logger, __FUNCTION__<<": "<<e.what());
        return false;
    }
}

}
