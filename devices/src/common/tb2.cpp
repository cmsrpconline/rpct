//---------------------------------------------------------------------------
#ifdef __BORLANDC__
#include "precompiled.h"
#pragma hdrstop
#endif

#include "rpct/devices/tb2.h"
#include "rpct/devices/VmeCrate.h" 
#include "rpct/devices/i2c.h"   
#include "rpct/devices/System.h" 

//---------------------------------------------------------------------------


using namespace std;
using namespace log4cplus;

namespace rpct {

Logger TTb2Device::logger = Logger::getInstance("TTb2Device");
void TTb2Device::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}
//---------------------------------------------------------------------------
#define IID_FILE "RPC_system_def.iid"
#define IID_CLASS TTb2Device
#include "iid2cdef2.h"

#define IID_FILE "RPC_TB2_def.iid"
#define IID_CLASS TTb2Device
#include "iid2cdef2.h"


//---------------------------------------------------------------------------

#define IID_FILE "RPC_TB2_vme.iid"
#define IID_CLASS TTb2Vme
#include "iid2c2.h"


const HardwareItemType TTb2Vme::TYPE(HardwareItemType::cDevice, "TB2VME");

TTb2Vme::TTb2Vme(int vme_board_address, IBoard& board) : TTb2Device(
            board.getId() + 100000,
            VME_IDENTIFIER.c_str(),
            WORD_IDENTIFIER,
            "VME",
            TYPE,
            VME_VERSION.c_str(),
            WORD_VERSION,
            WORD_CHECKSUM,
            0,
            board,
            -1,
            BuildMemoryMap(),
            vme_board_address
        ), ABootContr(0) {
}

TTb2Vme::~TTb2Vme()
{
    delete ABootContr;
}


//---------------------------------------------------------------------------

const int TTb2Control::II_ADDR_SIZE = TTb2Device::TB_II_ADDR_WIDTH_SOFT;
const int TTb2Control::II_DATA_SIZE = TTb2Device::TB_II_DATA_WIDTH;

const HardwareItemType TTb2Control::TYPE(HardwareItemType::cDevice, "TB2CONTROL");


#define IID_FILE "RPC_TB2_ctrl.iid"
#define IID_CLASS TTb2Control
#include "iid2c2.h"


TTb2Control::TTb2Control(int vme_board_address, IBoard& board, int position) 
: TTb2Device(
            board.getId() + 100001,
            TB_CTRL_IDENTIFIER.c_str(),
            WORD_IDENTIFIER,
            TB_CTRL_IDENTIFIER,
            TYPE,
            TB_CTRL_VERSION.c_str(),
            WORD_VERSION,
            WORD_CHECKSUM,
            TB_BASE_CNTRL,
            board,
            position,
            BuildMemoryMap(),
            vme_board_address
), ABootContr(0), I2C(0), TTCrxI2C(7) {
}

TTb2Control::~TTb2Control(){
    delete ABootContr;
}

void TTb2Control::ResetQPLL() {
    writeBits(BITS_QPLL_MODE, 1);
    writeBits(BITS_QPLL_SELECT, 0x30);
    tbsleep(1000);
    if (readBits(BITS_QPLL_LOCKED) != 1) {
        throw TException("TTb2Control::ResetQPLL: BITS_QPLL_LOCKED did not go up");
    }
}

TIII2CMasterCore& TTb2Control::GetI2C() {
    if (I2C == NULL) {

        I2C = new TIII2CMasterCore(*this, AREA_I2C);
        I2C->SetPrescaleValue(100);
        I2C->WriteControlReg(TI2CMasterCore::BIT_EN);
    }

    return *I2C;
}

void TTb2Control::SetTTCrxI2CAddress(unsigned char addr) {
    TTCrxI2C.SetAddress(addr);
    writeWord(WORD_TTC_ID_MODE, 0, addr);
    writeBits(BITS_TTC_INIT_ENA, 1);
    ResetTTC();
    writeBits(BITS_TTC_INIT_ENA, 0);
}

void TTb2Control::ResetTTC() {
    writeBits(BITS_TTC_RESET, 1);
    tbsleep(3000);
    if (readBits(BITS_TTC_READY) != 0) {
        throw TException("TTb2Control::ResetTTC: BITS_TTC_READY did not go down after BITS_TTC_RESET, 1");
    }
    writeBits(BITS_TTC_RESET, 0);
}

void TTb2Control::InitTTC() {
    SetTTCrxI2CAddress(TTCrxI2C.GetAddress());
    tbsleep(100);
    if (readBits(BITS_TTC_READY) != 1) {
        throw TException("TTb2Control::InitTTC: BITS_TTC_READY did not go up");
    }
    // Enable Clock40Des2 output
    LOG4CPLUS_WARN(logger, "tb2: Clock40Des2 not enabled ");
    //GetTTCrxI2C().WriteControlReg(GetTTCrxI2C().ReadControlReg() | 0x8);
}
//---------------------------------------------------------------------------

const int TTb2Opto::II_ADDR_SIZE = TTb2Device::TB_II_ADDR_WIDTH_SOFT;
const int TTb2Opto::II_DATA_SIZE = TTb2Device::TB_II_DATA_WIDTH;

const HardwareItemType TTb2Opto::TYPE(HardwareItemType::cDevice, "TB2OPTO");

#define IID_FILE "RPC_TB2_opto.iid"
#define IID_CLASS TTb2Opto
#include "iid2c2.h"


TTb2Opto::TChannel::TChannel(TTb2Opto& owner, int channel)
        : Owner(owner), Channel(channel)/*, Tlk(owner,
            (Channel == 0) ? VECT_TLK1          : VECT_TLK2,
            (Channel == 0) ? BITS_TLK1_OPTO_SIG : BITS_TLK2_OPTO_SIG,
            (Channel == 0) ? BITS_TLK1_ENABLE   : BITS_TLK2_ENABLE,
            (Channel == 0) ? BITS_TLK1_LCK_REFN : BITS_TLK2_LCK_REFN,
            (Channel == 0) ? BITS_TLK1_LOOP_ENA : BITS_TLK2_LOOP_ENA,
            (Channel == 0) ? BITS_TLK1_PRBS_ENA : BITS_TLK2_PRBS_ENA,
            (Channel == 0) ? BITS_TLK1_RX_ERR   : BITS_TLK2_RX_ERR,
            (Channel == 0) ? BITS_TLK1_RX_DV    : BITS_TLK2_RX_DV,
            (Channel == 0) ? BITS_TLK1_TEST_ENA : BITS_TLK2_TEST_ENA,
            (Channel == 0) ? BITS_TLK1_TX_ENA   : BITS_TLK2_TX_ENA,
            (Channel == 0) ? BITS_TLK1_TX_ERR   : BITS_TLK2_TX_ERR,
            (Channel == 0) ? BITS_TLK1_INV_CLK  : BITS_TLK2_INV_CLK,
    (Channel == 0) ? WORD_TLK1_RXDATA   : WORD_TLK2_RXDATA)*/ {
}

TTb2Opto::TChannel::~TChannel() {
}

TTb2Opto::TTb2Opto(int id, int vme_board_address, int ii_addr, const char* desc,
                   IBoard& board, int position)
        : TTb2Device(
            id,
            TB_OPTO_IDENTIFIER.c_str(),
            WORD_IDENTIFIER,
            desc,
            TYPE,
            TB_OPTO_VERSION.c_str(),
            WORD_VERSION,
            WORD_CHECKSUM,
            ii_addr,
            board,
            position,
            BuildMemoryMap(),
            vme_board_address),
DiagCtrl(0), DiagnosticReadout(0), PulserDiagCtrl(0), Pulser(0) {
    for (int i = 0; i < GetChannelCount(); i++) {
        Channels.push_back(new TChannel(*this, i));
    }

    for(unsigned int iLink = 0; iLink < GetOptLinksCount(); iLink++) {
        OptLinks.push_back(TOptLink(this, iLink));
    }
    EnabledOptLinks = boost::dynamic_bitset<>(3, 0ul);
}

TTb2Opto::~TTb2Opto() {
    for (int i = 0; i < GetChannelCount(); i++) {
        delete Channels.at(i);
    }
    delete DiagCtrl;
    delete DiagnosticReadout;
    delete Pulser;
    delete PulserDiagCtrl;
}


TDiagCtrl& TTb2Opto::GetDiagCtrl() {
    if (DiagCtrl == 0) {
        DiagCtrl = new TDiagCtrl(*this,
                                 VECT_DAQ,
                                 BITS_DAQ_PROC_REQ,
                                 BITS_DAQ_PROC_ACK,
                                 BITS_DAQ_TIMER_LOC_ENA,
                                 0,
                                 BITS_DAQ_TIMER_START,
                                 BITS_DAQ_TIMER_STOP,
                                 BITS_DAQ_TIMER_TRIG_SEL,
                                 WORD_DAQ_TIMER_LIMIT,
                                 WORD_DAQ_TIMER_COUNT,
                                 "DAQ_DIAG_CTRL");
    }
    return *DiagCtrl;
}


TStandardDiagnosticReadout& TTb2Opto::GetDiagnosticReadout() {
    if (DiagnosticReadout == 0) {
        DiagnosticReadout = new TStandardDiagnosticReadout(*this,
                            "READOUT",
                            GetDiagCtrl(),
                            VECT_DAQ,
                            BITS_DAQ_EMPTY,
                            BITS_DAQ_EMPTY_ACK,
                            BITS_DAQ_LOST,
                            BITS_DAQ_LOST_ACK,
                            VECT_DAQ_WR,
                            BITS_DAQ_WR_ADDR,
                            BITS_DAQ_WR_ACK,
                            VECT_DAQ_RD,
                            BITS_DAQ_RD_ADDR,
                            BITS_DAQ_RD_ACK,
                            WORD_DAQ_MASK,
                            0,
                            AREA_MEM_DAQ_DIAG,
                            DAQ_DIAG_DATA_SIZE,
                            DAQ_DIAG_TRIG_NUM,
                            DAQ_DIAG_TIMER_SIZE,
                            TTC_BCN_EVT_WIDTH);
    }
    return *DiagnosticReadout;
}

TDiagCtrl& TTb2Opto::GetPulserDiagCtrl() {
    if (PulserDiagCtrl == NULL) {
        PulserDiagCtrl = new TDiagCtrl(*this,
                                       VECT_PULSER,
                                       BITS_PULSER_PROC_REQ,
                                       BITS_PULSER_PROC_ACK,
                                       BITS_PULSER_TIMER_LOC_ENA,
                                       0,
                                       BITS_PULSER_TIMER_START,
                                       BITS_PULSER_TIMER_STOP,
                                       BITS_PULSER_TIMER_TRIG_SEL,
                                       WORD_PULSER_TIMER_LIMIT,
                                       WORD_PULSER_TIMER_COUNT,
                                       "PULSER_DIAG_CTRL");
    }

    return *PulserDiagCtrl;
}

TPulser& TTb2Opto::GetPulser() {
    if (Pulser == NULL) {
        Pulser = new TPulser(*this,
                             "PULSER",
                             GetPulserDiagCtrl(),
                             AREA_MEM_PULSE,
                             WORD_PULSER_LENGTH,
                             BITS_PULSER_REPEAT_ENA,
                             BITS_PULSER_OUT_ENA);
    }
    return *Pulser;
}



TTb2Opto::TDiagCtrlVector& TTb2Opto::GetDiagCtrls() {
    if (DiagCtrls.empty()) {
        DiagCtrls.push_back(&GetDiagCtrl());
        DiagCtrls.push_back(&GetPulserDiagCtrl());
    }
    return DiagCtrls;
}

TTb2Opto::TDiagVector& TTb2Opto::GetDiags() {
    if (Diags.empty()) {
        Diags.push_back(&GetDiagnosticReadout());
        Diags.push_back(&GetPulser());
    }
    return Diags;
}


void TTb2Opto::ConfigureDiagnosable(TTriggerDataSel triggerDataSel) {
    writeBits(BITS_STATUS_TRG_DATA_SEL, triggerDataSel);
}

void TTb2Opto::Reset() {
    uint32_t zero = 0;
    writeBits(TTb2Opto::BITS_STATUS_TTC_DATA_DELAY, 4);	//<<<<<<<< may change after firmware chnge!!!!
    writeWord(TTb2Opto::WORD_TLK_ENABLE, 0, zero);
    writeWord(TTb2Opto::WORD_TLK_LCK_REFN, 0, zero);


    writeWord(TTb2Opto::WORD_RECEIVER_TEST_ENA, 0, zero);
    writeWord(TTb2Opto::WORD_RECEIVER_TEST_RND_ENA, 0, zero);
    for(unsigned int iLink = 0; iLink < 3; iLink++) {
        writeWord(TTb2Opto::WORD_LINK_DELAY, iLink, zero);

        EnabledOptLinks[iLink] = false;

        writeWord(TTb2Opto::WORD_SENDER_TEST_DATA, iLink, zero);
    }

    writeWord(TTb2Opto::WORD_SENDER_TEST_ENA, 0, zero);
    writeWord(TTb2Opto::WORD_SENDER_TEST_RND_ENA, 0, zero);

    writeBits(TTb2Opto::BITS_STATUS_PULSER_OUT_ENA, zero);
    writeBits(TTb2Opto::BITS_PULSER_OUT_ENA, zero);
}

TTb2Opto::TOptLink::TOptLink(TTb2Opto* opto, int optLinkNum) : Opto(opto), Number(optLinkNum) {

}

TTb2Opto::TOptLink::~TOptLink() {}

void TTb2Opto::TOptLink::EnableOptoLink(bool enable) {
    uint32_t tlkEnable = GetOpto().readWord(TTb2Opto::WORD_TLK_ENABLE, 0);
    if(enable) {
        tlkEnable =  tlkEnable | 1<<Number;
        GetOpto().EnabledOptLinks[Number] = true;
    }
    else {
        tlkEnable =  tlkEnable & (~(1<<Number));
        GetOpto().EnabledOptLinks[Number] = false;
    }
    GetOpto().writeWord(TTb2Opto::WORD_TLK_ENABLE, 0, tlkEnable);
    GetOpto().writeWord(TTb2Opto::WORD_TLK_LCK_REFN, 0, tlkEnable);
}

bool TTb2Opto::TOptLink::FindOptLinksSynchrDelay(unsigned int testData) {
    vector<unsigned int> recTestsData;
    for(uint32_t del = 0; del < 8; del++) {
        GetOpto().writeWord(TTb2Opto::WORD_TLK_DELAY, Number, del);
        //cout<<"WORD_TLK_RXDATA "<<GetTBOpto().readWord(TTb2Opto::WORD_TLK_RXDATA, iLink)<<endl;
        recTestsData.push_back(GetOpto().readWord(TTb2Opto::WORD_TLK_RXDATA, Number));
        //cout<<"del "<<del<<" "<<hex<<recTestsData[del]<<endl;
    }
    int firsGood = -1;
    int lastGood = -1;
    int foundDel = -1;
    if(recTestsData[0] == testData ) {
        firsGood = 0;
    }
    for(int del = 0; del < 7; del++) {
        if(recTestsData[del] != testData && recTestsData[del+1] == testData)
            firsGood = del + 1;
        else if(recTestsData[del] == testData && recTestsData[del+1] != testData)
            lastGood = del;
    }
    if(lastGood == -1 && recTestsData[7] == testData ) {
        lastGood = 7;
    }

    if(firsGood != -1 && lastGood != -1) {
        if(firsGood == 0 && lastGood == 7) {
            foundDel = 3;
            LOG4CPLUS_DEBUG(logger, GetOpto().getDescription() << " SynchronizeOptLink: linkNum " << Number <<" foundDel "<<foundDel);

            GetOpto().writeWord(TTb2Opto::WORD_TLK_DELAY, Number, foundDel);
        }
        else {
            if(lastGood >= firsGood)
                foundDel = (firsGood + lastGood ) / 2;
            else {
                foundDel = ((firsGood + lastGood + 8) / 2) % 8;
            }
            LOG4CPLUS_DEBUG(logger, GetOpto(). getDescription() << " SynchronizeOptLink: linkNum " << Number <<" foundDel "<<foundDel);

            GetOpto().writeWord(TTb2Opto::WORD_TLK_DELAY, Number, foundDel);
        }
        /*			if(foundDel <= 3) {
        				GetTBOpto().writeWord(TTb2Opto::WORD_LINK_DELAY, iLink, 1);	
        				cout<<"WORD_LINK_DELAY iLink"<<iLink<<" "<<1<<endl;
        			}*/
        return true;
    }
    else {
        LOG4CPLUS_ERROR(logger, GetOpto().getDescription() << " SynchronizeOptLink: linkNum " << Number <<" delay not found, synchronization not succided!!!");
        unsigned int zer = 0;
        GetOpto().writeWord(TTb2Opto::WORD_TLK_DELAY, Number, zer);
        return false;
    }
}

/*void TTb2Opto::EnableOptoLink(int linkNum, bool enable) {
	uint32_t tlkEnable = readWord(TTb2Opto::WORD_TLK_ENABLE, 0);
	if(enable) {
		tlkEnable =  tlkEnable | 1<<linkNum;
		EnabledOptLinks[linkNum] = true;
	}	
	else {
		tlkEnable =  tlkEnable & (~(1<<linkNum));	
		EnabledOptLinks[linkNum] = false;
	}	 
	writeWord(TTb2Opto::WORD_TLK_ENABLE, 0, tlkEnable); 	
	writeWord(TTb2Opto::WORD_TLK_LCK_REFN, 0, tlkEnable);
}*/

bool TTb2Opto::CheckLinksSynchronization() {
    bool good = true;
    int dv = readWord(TTb2Opto::WORD_TLK_RX_DV, 0); //data valid, should be 0
    int err = readWord(TTb2Opto::WORD_TLK_RX_ERR, 0); //should be 0
    //LOG4CPLUS_DEBUG(logger, getDescription()<<" CheckLinksSynchronization: TLK_RX_DV "<<dv<<" TLK_RX_ERR "<<err );

    if(EnabledOptLinks.to_ulong() & dv != 0 ||
            EnabledOptLinks.to_ulong() & err != 0   ) {
        good = false;
        LOG4CPLUS_ERROR(logger, getDescription()<<" CheckLinksSynchronization: TLK_RX_DV "<<dv<<" TLK_RX_ERR "<<err );
    }

    for(unsigned int iLink = 0; iLink < 3; iLink++) {
        if(EnabledOptLinks[iLink] == false)
            continue;

        unsigned int rxdata = readWord(TTb2Opto::WORD_TLK_RXDATA, iLink);
        cout<<"iLink "<<iLink<<" WORD_TLK_RXDATA "<<rxdata<<endl;

        if(rxdata != 0x50bc50bc) {
            good = false;
            LOG4CPLUS_ERROR(logger, getDescription()<<" CheckLinksSynchronization: iLink "<<iLink<<" WORD_TLK_RXDATA "<<rxdata<<"  != 0x50bc50bc" );
        }
    }
    return good;
}

bool TTb2Opto::CheckLinksOperation() {
    bool good = true;
    unsigned int  dv = readWord(TTb2Opto::WORD_TLK_RX_DV, 0); //data valid, should be 0
    unsigned int  err = readWord(TTb2Opto::WORD_TLK_RX_ERR, 0); //should be 0
    //LOG4CPLUS_DEBUG(logger, getDescription()<<" CheckLinksSynchronization: TLK_RX_DV "<<dv<<" TLK_RX_ERR "<<err );

    if(EnabledOptLinks.to_ulong() & dv != EnabledOptLinks.to_ulong()  ||
            EnabledOptLinks.to_ulong() & err != 0) {
        good = false;
        LOG4CPLUS_ERROR(logger, getDescription()<<" CheckLinksOperation: TLK_RX_DV "<<dv<<" TLK_RX_ERR "<<err );
    }

    return good;
}

bool TTb2Opto::FindOptLinksSynchrDelay(unsigned int testData) {
    bool good = true;
    for(unsigned int iLink = 0; iLink < OptLinks.size(); iLink++) {
        if(EnabledOptLinks[iLink] == false)
            continue;

        if(OptLinks[iLink].FindOptLinksSynchrDelay(testData) == false)
            good = false;
    }
    return good;
}
//---------------------------------------------------------------------------

const int TTb2LdPac::II_ADDR_SIZE = TTb2Device::TB_II_ADDR_WIDTH_SOFT;
const int TTb2LdPac::II_DATA_SIZE = TTb2Device::TB_II_DATA_WIDTH;
const HardwareItemType TTb2LdPac::TYPE(HardwareItemType::cDevice, "TB2LDPAC");


#define IID_FILE "RPC_TB2_ldpac.iid"
#define IID_CLASS TTb2LdPac
#include "iid2c2.h"



TTb2LdPac::TTb2LdPac(int id, int vme_board_address, int ii_addr, const char* desc,
                     IBoard& board, int position): TTb2Device(
                                 id,
                                 TB_LDPAC_IDENTIFIER.c_str(),
                                 WORD_IDENTIFIER,
                                 desc,
                                 TYPE,
                                 TB_LDPAC_VERSION.c_str(),
                                 WORD_VERSION,
                                 WORD_CHECKSUM,
                                 ii_addr,
                                 board,
                                 position,
                                 BuildMemoryMap(),
                                 vme_board_address), DiagCtrl(0), DiagnosticReadout(0)
{
}

TTb2LdPac::~TTb2LdPac()
{
    delete DiagCtrl;
    delete DiagnosticReadout;
}


TDiagCtrl& TTb2LdPac::GetDiagCtrl() {
    if (DiagCtrl == 0) {
        DiagCtrl = new TDiagCtrl(*this,
                                 VECT_DAQ,
                                 BITS_DAQ_PROC_REQ,
                                 BITS_DAQ_PROC_ACK,
                                 BITS_DAQ_TIMER_LOC_ENA,
                                 0,
                                 BITS_DAQ_TIMER_START,
                                 BITS_DAQ_TIMER_STOP,
                                 BITS_DAQ_TIMER_TRIG_SEL,
                                 WORD_DAQ_TIMER_LIMIT,
                                 WORD_DAQ_TIMER_COUNT,
                                 "DAQ_DIAG_CTRL");
    }
    return *DiagCtrl;
}


TStandardDiagnosticReadout& TTb2LdPac::GetDiagnosticReadout() {
    if (DiagnosticReadout == 0) {
        DiagnosticReadout = new TStandardDiagnosticReadout(*this,
                            "READOUT",
                            GetDiagCtrl(),
                            VECT_DAQ,
                            BITS_DAQ_EMPTY,
                            BITS_DAQ_EMPTY_ACK,
                            BITS_DAQ_LOST,
                            BITS_DAQ_LOST_ACK,
                            VECT_DAQ_WR,
                            BITS_DAQ_WR_ADDR,
                            BITS_DAQ_WR_ACK,
                            VECT_DAQ_RD,
                            BITS_DAQ_RD_ADDR,
                            BITS_DAQ_RD_ACK,
                            WORD_DAQ_MASK,
                            0,
                            AREA_MEM_DAQ_DIAG,
                            DAQ_DIAG_DATA_SIZE,
                            DAQ_DIAG_TRIG_NUM,
                            DAQ_DIAG_TIMER_SIZE,
                            TTC_BCN_EVT_WIDTH);
    }
    return *DiagnosticReadout;
}

TTb2LdPac::TDiagCtrlVector& TTb2LdPac::GetDiagCtrls() {
    if (DiagCtrls.empty()) {
        DiagCtrls.push_back(&GetDiagCtrl());
    }
    return DiagCtrls;
}

TTb2LdPac::TDiagVector& TTb2LdPac::GetDiags() {
    if (Diags.empty()) {
        Diags.push_back(&GetDiagnosticReadout());
    }
    return Diags;
}



void TTb2LdPac::ConfigureDiagnosable(TTriggerDataSel triggerDataSel) {
    writeBits(BITS_STATUS_TRG_DATA_SEL, triggerDataSel);
}

void TTb2LdPac::Reset() {
    writeBits(TTb2LdPac::BITS_STATUS_TTC_DATA_DELAY, 4); //<<<may chnge after firmewe chnged

    unsigned int zero = 0;
    writeWord(TTb2LdPac::WORD_RECEIVER_TEST_ENA, 0, ~((dynamic_cast<TTb2&>(getBoard())).GetUsedOptos().to_ulong()));
    writeWord(TTb2LdPac::WORD_RECEIVER_TEST_RND_ENA, 0, zero);

    writeWord(TTb2LdPac::WORD_SENDER_TEST_ENA, 0, zero);
    writeWord(TTb2LdPac::WORD_SENDER_TEST_RND_ENA, 0, zero);
    writeWord(TTb2LdPac::WORD_SENDER_TEST_DATA, 0, zero);

    writeBits(TTb2LdPac::BITS_PULSER_OUT_ENA, zero);
}
//---------------------------------------------------------------------------

const int TTb2GbSort::II_ADDR_SIZE = TTb2Device::TB_II_ADDR_WIDTH_SOFT;
const int TTb2GbSort::II_DATA_SIZE = TTb2Device::TB_II_DATA_WIDTH;
const HardwareItemType TTb2GbSort::TYPE(HardwareItemType::cDevice, "TB2GBSORT");

#define IID_FILE "RPC_TB2_gbsort.iid"
#define IID_CLASS TTb2GbSort
#include "iid2c2.h"



TTb2GbSort::TTb2GbSort(int id, int vme_board_address, int ii_addr, const char* desc,
                       IBoard& board, int position): TTb2Device(
                                   id,
                                   TB_GBSORT_IDENTIFIER.c_str(),
                                   WORD_IDENTIFIER,
                                   desc,
                                   TYPE,
                                   TB_GBSORT_VERSION.c_str(),
                                   WORD_VERSION,
                                   WORD_CHECKSUM,
                                   ii_addr,
                                   board,
                                   position,
                                   BuildMemoryMap(),
                                   vme_board_address), DiagCtrl(0), DiagnosticReadout(0)
{
}

TTb2GbSort::~TTb2GbSort()
{
    delete DiagCtrl;
    delete DiagnosticReadout;
}



TDiagCtrl& TTb2GbSort::GetDiagCtrl() {
    if (DiagCtrl == 0) {
        DiagCtrl = new TDiagCtrl(*this,
                                 VECT_DAQ,
                                 BITS_DAQ_PROC_REQ,
                                 BITS_DAQ_PROC_ACK,
                                 BITS_DAQ_TIMER_LOC_ENA,
                                 0,
                                 BITS_DAQ_TIMER_START,
                                 BITS_DAQ_TIMER_STOP,
                                 BITS_DAQ_TIMER_TRIG_SEL,
                                 WORD_DAQ_TIMER_LIMIT,
                                 WORD_DAQ_TIMER_COUNT,
                                 "DAQ_DIAG_CTRL");
    }
    return *DiagCtrl;
}


TStandardDiagnosticReadout& TTb2GbSort::GetDiagnosticReadout() {
    if (DiagnosticReadout == 0) {
        DiagnosticReadout = new TStandardDiagnosticReadout(*this,
                            "READOUT",
                            GetDiagCtrl(),
                            VECT_DAQ,
                            BITS_DAQ_EMPTY,
                            BITS_DAQ_EMPTY_ACK,
                            BITS_DAQ_LOST,
                            BITS_DAQ_LOST_ACK,
                            VECT_DAQ_WR,
                            BITS_DAQ_WR_ADDR,
                            BITS_DAQ_WR_ACK,
                            VECT_DAQ_RD,
                            BITS_DAQ_RD_ADDR,
                            BITS_DAQ_RD_ACK,
                            WORD_DAQ_MASK,
                            0,
                            AREA_MEM_DAQ_DIAG,
                            DAQ_DIAG_DATA_SIZE,
                            DAQ_DIAG_TRIG_NUM,
                            DAQ_DIAG_TIMER_SIZE,
                            TTC_BCN_EVT_WIDTH);
    }
    return *DiagnosticReadout;
}

TTb2GbSort::TDiagCtrlVector& TTb2GbSort::GetDiagCtrls() {
    if (DiagCtrls.empty()) {
        DiagCtrls.push_back(&GetDiagCtrl());
    }
    return DiagCtrls;
}

TTb2GbSort::TDiagVector& TTb2GbSort::GetDiags() {
    if (Diags.empty()) {
        Diags.push_back(&GetDiagnosticReadout());
    }
    return Diags;
}



void TTb2GbSort::ConfigureDiagnosable(TTriggerDataSel triggerDataSel) {
    writeBits(BITS_STATUS_TRG_DATA_SEL, triggerDataSel);
}

void TTb2GbSort::Reset() {
    writeBits(TTb2GbSort::BITS_STATUS_TTC_DATA_DELAY, 2);//may change after firmware chnge!!!!

    unsigned int zero = 0;
    writeWord(TTb2GbSort::WORD_RECEIVER_TEST_ENA, 0, ~((dynamic_cast<TTb2&>(getBoard())).GetUsedPacs().to_ulong()));
    writeWord(TTb2GbSort::WORD_RECEIVER_TEST_RND_ENA, 0, zero);

    writeWord(TTb2GbSort::WORD_SENDER_TEST_ENA, 0, zero);
    writeWord(TTb2GbSort::WORD_SENDER_TEST_RND_ENA, 0, zero);
    writeWord(TTb2GbSort::WORD_SENDER_TEST_DATA, 0, zero);

    writeBits(TTb2GbSort::BITS_PULSER_OUT_ENA, zero);
}
//---------------------------------------------------------------------------

const int TTb2Rmb::II_ADDR_SIZE = TTb2Device::TB_II_ADDR_WIDTH_SOFT;
const int TTb2Rmb::II_DATA_SIZE = TTb2Device::TB_II_DATA_WIDTH;
const HardwareItemType TTb2Rmb::TYPE(HardwareItemType::cDevice, "TB2RMB");


#define IID_FILE "RPC_TB2_rmb.iid"
#define IID_CLASS TTb2Rmb
#include "iid2c2.h"



TTb2Rmb::TTb2Rmb(int id, int vme_board_address, int ii_addr, const char* desc,
                 IBoard& board, int position): TTb2Device(
                             id,
                             TB_RMB_IDENTIFIER.c_str(),
                             WORD_IDENTIFIER,
                             desc,
                             TYPE,
                             TB_RMB_VERSION.c_str(),
                             WORD_VERSION,
                             WORD_CHECKSUM,
                             ii_addr,
                             board,
                             position,
                             BuildMemoryMap(),
                             vme_board_address), DiagCtrl(0), DiagnosticReadout(0)
{
}

TTb2Rmb::~TTb2Rmb()
{
    delete DiagCtrl;
    delete DiagnosticReadout;
}

TDiagCtrl& TTb2Rmb::GetDiagCtrl() {
    if (DiagCtrl == 0) {
        DiagCtrl = new TDiagCtrl(*this,
                                 VECT_DAQ,
                                 BITS_DAQ_PROC_REQ,
                                 BITS_DAQ_PROC_ACK,
                                 BITS_DAQ_TIMER_LOC_ENA,
                                 0,
                                 BITS_DAQ_TIMER_START,
                                 BITS_DAQ_TIMER_STOP,
                                 BITS_DAQ_TIMER_TRIG_SEL,
                                 WORD_DAQ_TIMER_LIMIT,
                                 WORD_DAQ_TIMER_COUNT,
                                 "DAQ_DIAG_CTRL");
    }
    return *DiagCtrl;
}


TStandardDiagnosticReadout& TTb2Rmb::GetDiagnosticReadout() {
    if (DiagnosticReadout == 0) {
        DiagnosticReadout = new TStandardDiagnosticReadout(*this,
                            "READOUT",
                            GetDiagCtrl(),
                            VECT_DAQ,
                            BITS_DAQ_EMPTY,
                            BITS_DAQ_EMPTY_ACK,
                            BITS_DAQ_LOST,
                            BITS_DAQ_LOST_ACK,
                            VECT_DAQ_WR,
                            BITS_DAQ_WR_ADDR,
                            BITS_DAQ_WR_ACK,
                            VECT_DAQ_RD,
                            BITS_DAQ_RD_ADDR,
                            BITS_DAQ_RD_ACK,
                            WORD_DAQ_MASK,
                            0,
                            AREA_MEM_DAQ_DIAG,
                            DAQ_DIAG_DATA_SIZE,
                            DAQ_DIAG_TRIG_NUM,
                            DAQ_DIAG_TIMER_SIZE,
                            TTC_BCN_EVT_WIDTH);
    }
    return *DiagnosticReadout;
}


TTb2Rmb::TDiagCtrlVector& TTb2Rmb::GetDiagCtrls() {
    if (DiagCtrls.empty()) {
        DiagCtrls.push_back(&GetDiagCtrl());
    }
    return DiagCtrls;
}

TTb2Rmb::TDiagVector& TTb2Rmb::GetDiags() {
    if (Diags.empty()) {
        Diags.push_back(&GetDiagnosticReadout());
    }
    return Diags;
}

void TTb2Rmb::ConfigureDiagnosable(TTriggerDataSel triggerDataSel) {
    writeBits(BITS_STATUS_TRG_DATA_SEL, triggerDataSel);
}
//---------------------------------------------------------------------------

Logger TTb2::logger = Logger::getInstance("TTb2");
void TTb2::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}

const HardwareItemType TTb2::TYPE(HardwareItemType::cBoard, "TB2");

TTb2::TTb2(int ident, const char* desc, TVMEInterface* vme, int vme_board_address, VmeCrate* c, int pos)
        : BoardBase(ident, desc, TYPE, c, pos), VME(vme), Vme(vme_board_address, *this), Control(vme_board_address, *this, 0),
        VMEAddress(vme_board_address)
{
    cout << vme_board_address << endl;
    Init(true);
    Vme.getMemoryMap().SetHardwareIfc(
        new TIIVMEAccess(
            Vme.GetVMEBoardBaseAddr(),
            Vme.getBaseAddress() << Vme.GetVMEBaseAddrShift(),
            vme,
            0xf8000),
        true);
    Control.getMemoryMap().SetHardwareIfc(
        new TIIVMEAccess(
            Control.GetVMEBoardBaseAddr(),
            Control.getBaseAddress() << Control.GetVMEBaseAddrShift(),
            vme,
            0),
        true);

    Optos.assign(6, NULL);
    //LdPacs.assign(4, NULL);
    GbSort = NULL;
    Rmb = NULL;

    UsedOptos = boost::dynamic_bitset<>(6, 0ul);
    UsedPacs = boost::dynamic_bitset<>(4, 0ul);;
}

/*
TTb2::TTb2(const char* address, int port, int vme_board_address, TCrate* crate)
: TrgVme(vme_board_address, *this), TrgControl(vme_board_address, *this),
  VMEAddress(vme_board_address), Crate(crate)
{
  Init(false);
  TrgControl.getMemoryMap().SetHardwareIfc(
          new TIITcpAccess(
                TrgControl.getBaseAddress() << TrgControl.GetVMEBaseAddrShift(),
                address,
                port),
          true);
} */

TTb2::~TTb2()
{
    for (Devices::iterator iDevice = devices_.begin(); iDevice != devices_.end(); ++iDevice)
        delete dynamic_cast<TTb2Opto*>(*iDevice);
}


void TTb2::Init(bool use_vme) {
    //std::ostringstream ost;
    //ost << "TRG " << std::hex << VMEAddress;
    //Description = ost.str();

    /* IE_RESET_II = "Reset II";
     IE_INIT_TTC = "Init TTC";
     //IE_SET_CONTROL_CLOCK = "Set UNI_CONTROL clock and STATUS_TRG_SEL";
     //IE_SET_OPTO_CLOCK = "Set UNI_OPTO clock";
     IE_INIT_TLK = "Initialize TLK"; 
     //IE_RESET_DELAY = "Reset delay";
     IE_INIT_QPLL = "Init QPLL";
     //IE_INIT_OPTO = "Init Opto";
     InitElements.push_back(IE_RESET_II);
     InitElements.push_back(IE_INIT_TTC);
     //InitElements.push_back(IE_SET_CONTROL_CLOCK);
     //InitElements.push_back(IE_SET_OPTO_CLOCK);
     //InitElements.push_back(IE_INIT_TLK); <<!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!!!!!!!!! KB
     //InitElements.push_back(IE_RESET_DELAY);
     InitElements.push_back(IE_INIT_QPLL);
     //InitElements.push_back(IE_INIT_OPTO);
    */
    if (use_vme) {
        devices_.push_back(&Vme);
        devices_.push_back(&Control);
    }
    else
        devices_.push_back(&Control);
}

IDevice& TTb2::addChip(std::string type, int pos, int id) {
    transform(type.begin(), type.end(), type.begin(), ::toupper);
    ostringstream ostr;
    ostr << type << ' ' << pos;
    string chipName = ostr.str(); 
    if (type == string("OPTO")) {
        TTb2Opto* chip = new TTb2Opto(id, VMEAddress, pos, chipName.c_str(), *this, pos);

        chip->getMemoryMap().SetHardwareIfc(
            new TIIVMEAccess(
                chip->GetVMEBoardBaseAddr(),
                chip->getBaseAddress() << chip->GetVMEBaseAddrShift(),
                VME,
                0),
            true);
        devices_.push_back(chip);
        Optos[pos - 2] = chip;
        UsedOptos[pos - 2] = true;
        return *chip;
    }

    if (type == string("LDPAC")) {
        TTb2LdPac* chip = new TTb2LdPac(id, VMEAddress, pos, chipName.c_str(), *this, pos);

        chip->getMemoryMap().SetHardwareIfc(
            new TIIVMEAccess(
                chip->GetVMEBoardBaseAddr(),
                chip->getBaseAddress() << chip->GetVMEBaseAddrShift(),
                VME,
                0),
            true);
        devices_.push_back(chip);
        //LdPacs[pos - 8] = chip;
        LdPacs.push_back(chip);
        UsedPacs[pos - 8] = true;
        return *chip;
    }

    if (type == string("GBSORT")) {
        TTb2GbSort* chip = new TTb2GbSort(id, VMEAddress, pos, chipName.c_str(), *this, pos);

        chip->getMemoryMap().SetHardwareIfc(
            new TIIVMEAccess(
                chip->GetVMEBoardBaseAddr(),
                chip->getBaseAddress() << chip->GetVMEBaseAddrShift(),
                VME,
                0),
            true);
        devices_.push_back(chip);
        GbSort = chip;
        return *chip;
    }

    if (type == string("RMB")) {
        TTb2Rmb* chip = new TTb2Rmb(id, VMEAddress, pos, chipName.c_str(), *this, pos);

        chip->getMemoryMap().SetHardwareIfc(
            new TIIVMEAccess(
                chip->GetVMEBoardBaseAddr(),
                chip->getBaseAddress() << chip->GetVMEBaseAddrShift(),
                VME,
                0),
            true);
        devices_.push_back(chip);
        Rmb = chip;
        return *chip;
    }

    throw TException(string("Ivalid chip type '") + type + "'");
}
/*
void TTb2::InitializeElement(std::string elem) {     
    LOG4CPLUS_DEBUG(logger, "Initializing " <<  getDescription() << " element " << elem);
  if (elem == IE_RESET_II) {
    Vme.writeWord(TTb2Vme::WORD_RESET, 0, 0x13);
  }
  else if (elem == IE_INIT_TTC) {
    Control.InitTTC();
  }
  else if (elem == IE_INIT_TLK) {
    for (Devices::iterator iDevice = devices.begin(); iDevice != devices.end(); ++iDevice) {
      TTb2Opto* opto = dynamic_cast<TTb2Opto*>(*iDevice);
      if (opto != 0) {
        for (int i = 0; i < opto->GetChannelCount(); i++) {
          opto->GetChannel(i).GetTlk().Init();
        }
      }
    }
  }
  else if (elem == IE_INIT_QPLL) {
    Control.ResetQPLL();
  }
  else
    throw TException("Unknown init element '" + elem + "'.");
}
*/

void TTb2::init() {
    Vme.writeWord(TTb2Vme::WORD_RESET, 0, 0x13);
    Control.InitTTC();
    for (Devices::iterator iDevice = devices_.begin(); iDevice != devices_.end(); ++iDevice) {
        TTb2Opto* opto = dynamic_cast<TTb2Opto*>(*iDevice);
        if (opto != 0) {
            for (int i = 0; i < opto->GetChannelCount(); i++) {
                opto->GetChannel(i).GetTlk().Init();
            }
        }
    }
    Control.ResetQPLL();
    Control.writeBits(TTb2Control::BITS_TTC_TC_ENA, 1); //we are using the TTC signals and clock from the TC, not from the TB TTCrx, bacause it does not work
}

bool TTb2::CheckOptLinksSynchronization() {
    bool good = true;
    for(TTb2Optos::iterator optoIt = Optos.begin(); optoIt != Optos.end(); optoIt++) {
        if((*optoIt) == NULL)
            continue;
        if(	(*optoIt)->CheckLinksSynchronization() == false )
            good = false;
        //throw TException(" opto links synchronization failed");
        LOG4CPLUS_ERROR(logger, "opto links synchronization failed on: tb " <<  getId() << " opto " << (*optoIt)->getId());

    }
    return false;
}

bool TTb2::CheckLinksOperation() {
    bool good = true;
    for(TTb2Optos::iterator optoIt = Optos.begin(); optoIt != Optos.end(); optoIt++) {
        if((*optoIt) == NULL)
            continue;
        if(	(*optoIt)->CheckLinksOperation() == false )
            good = false;
        //throw TException(" CheckLinksOperation failed");
        LOG4CPLUS_ERROR(logger, "CheckLinksOperation failed on: tb " <<  getId() << " opto " << (*optoIt)->getId());

    }
    return false;
}

bool TTb2::FindOptLinksSynchrDelay(unsigned int testData) {
    bool good = true;
    for(TTb2Optos::iterator optoIt = Optos.begin(); optoIt != Optos.end(); optoIt++) {
        if((*optoIt) == NULL)
            continue;
        if(	(*optoIt)->FindOptLinksSynchrDelay(testData) == false )
            good = false;
        //throw TException(" opto links synchronization failed");
        LOG4CPLUS_ERROR(logger, "FindOptLinksSynchrDelay failed on: tb " <<  getId() << " opto " << (*optoIt)->getId());

    }
    return false;
}


void TTb2::EnableTransmissionCheck(bool enable) {
    bool checkDataEna = enable;
    bool checkEna = enable;


    uint32_t zero = 0;
    for(TTb2Optos::iterator optoIt = Optos.begin(); optoIt != Optos.end(); optoIt++) {
        if((*optoIt) == NULL)
            continue;
        (*optoIt)->writeBits(TTb2Opto::BITS_ERROR_RX_ER , (*optoIt)->GetEnabledOptLinks().to_ulong()); //jak 1 to liczy blady z TLK
        (*optoIt)->writeBits(TTb2Opto::BITS_ERROR_RX_DV , (*optoIt)->GetEnabledOptLinks().to_ulong()); //jak 1 to liczy brak DV  (Data Valid) z TLK
        (*optoIt)->writeBits(TTb2Opto::BITS_ERROR_REC_ERR , (*optoIt)->GetEnabledOptLinks().to_ulong());  //jak 1 to liczy bledy analizy danych

        if(checkDataEna) {
            (*optoIt)->writeWord(TTb2Opto::WORD_RECEIVER_CHECK_DATA_ENA, 0, (*optoIt)->GetEnabledOptLinks().to_ulong());
            (*optoIt)->writeWord(TTb2Opto::WORD_SENDER_CHECK_DATA_ENA, 0, 7);
        }
        else {
            (*optoIt)->writeWord(TTb2Opto::WORD_RECEIVER_CHECK_DATA_ENA, 0, zero);
            (*optoIt)->writeWord(TTb2Opto::WORD_SENDER_CHECK_DATA_ENA, 0, zero);
        }

        if(checkEna) {
            (*optoIt)->writeWord(TTb2Opto::WORD_RECEIVER_CHECK_ENA, 0, (*optoIt)->GetEnabledOptLinks().to_ulong());
            (*optoIt)->writeWord(TTb2Opto::WORD_SENDER_CHECK_ENA, 0, 7);
        }
        else {
            (*optoIt)->writeWord(TTb2Opto::WORD_RECEIVER_CHECK_ENA, 0, zero);
            (*optoIt)->writeWord(TTb2Opto::WORD_SENDER_CHECK_ENA, 0, zero);
        }
    }

    for(TTb2LdPacs::iterator pacIt = LdPacs.begin(); pacIt != LdPacs.end(); pacIt++) {
        if((*pacIt) == NULL)
            continue;
        if(checkDataEna) {
            (*pacIt)->writeWord(TTb2LdPac::WORD_RECEIVER_CHECK_DATA_ENA, 0, UsedOptos.to_ulong());
            (*pacIt)->writeWord(TTb2LdPac::WORD_SENDER_CHECK_DATA_ENA, 0, 1);
        }
        else {
            (*pacIt)->writeWord(TTb2LdPac::WORD_RECEIVER_CHECK_DATA_ENA, 0, zero);
            (*pacIt)->writeWord(TTb2LdPac::WORD_SENDER_CHECK_DATA_ENA, 0, zero);
        }


        if(checkEna) {
            (*pacIt)->writeWord(TTb2LdPac::WORD_RECEIVER_CHECK_ENA, 0, UsedOptos.to_ulong());
            (*pacIt)->writeWord(TTb2LdPac::WORD_SENDER_CHECK_ENA, 0, 1);
        }
        else {
            (*pacIt)->writeWord(TTb2LdPac::WORD_RECEIVER_CHECK_ENA, 0, zero);
            (*pacIt)->writeWord(TTb2LdPac::WORD_SENDER_CHECK_ENA, 0, zero);
        }
    }

    if(Rmb != NULL) {
        if(checkDataEna) {
            Rmb->writeWord(TTb2Rmb::WORD_RECEIVER_CHECK_DATA_ENA, 0, UsedOptos.to_ulong());
        }
        else {
            Rmb->writeWord(TTb2Rmb::WORD_RECEIVER_CHECK_DATA_ENA, 0, zero);
        }

        if(checkEna) {
            Rmb->writeWord(TTb2Rmb::WORD_RECEIVER_CHECK_ENA, 0, UsedOptos.to_ulong());
        }
        else {
            Rmb->writeWord(TTb2Rmb::WORD_RECEIVER_CHECK_ENA, 0, zero);
        }
    }

    if(GbSort != NULL) {
        if(checkDataEna)  {
            GbSort->writeWord(TTb2GbSort::WORD_RECEIVER_CHECK_DATA_ENA, 0, UsedPacs.to_ulong());
            GbSort->writeWord(TTb2GbSort::WORD_SENDER_CHECK_DATA_ENA, 0, 0x1);
        }
        else {
            GbSort->writeWord(TTb2GbSort::WORD_RECEIVER_CHECK_DATA_ENA, 0, zero);
            GbSort->writeWord(TTb2GbSort::WORD_SENDER_CHECK_DATA_ENA, 0, zero);
        }
        if(checkEna) {
            GbSort->writeWord(TTb2GbSort::WORD_RECEIVER_CHECK_ENA, 0, UsedPacs.to_ulong());
            GbSort->writeWord(TTb2GbSort::WORD_SENDER_CHECK_ENA, 0, 0x1);
        }
        else {
            GbSort->writeWord(TTb2GbSort::WORD_RECEIVER_CHECK_ENA, 0, zero);
            GbSort->writeWord(TTb2GbSort::WORD_SENDER_CHECK_ENA, 0, zero);
        }
    }
}

void TTb2::EnableOptLink(int linkNum, bool enable) {
    if(Optos[linkNum/TTb2Opto::GetOptLinksCount()] != NULL)
        Optos[linkNum/TTb2Opto::GetOptLinksCount()]->EnableOptoLink(linkNum%TTb2Opto::GetOptLinksCount(), enable);
    else {
        throw TException("TTb2::EnableOptLink: no Opto for required optLink input");
    }
}

void TTb2::Reset() {
    for(TTb2Optos::iterator optoIt = Optos.begin(); optoIt != Optos.end(); optoIt++) {
        if((*optoIt) == NULL)
            continue;
        (*optoIt)->Reset();
    }

    for(TTb2LdPacs::iterator pacIt = LdPacs.begin(); pacIt != LdPacs.end(); pacIt++) {
        if((*pacIt) == NULL)
            continue;
        (*pacIt)->Reset();
    }

    if(Rmb != NULL) {
        //Rmb->Reset();
    }

    if(GbSort != NULL) {
        GbSort->Reset();
    }
}
}
