#include "rpct/devices/FebChip.h"

#include <cmath>

#include "rpct/devices/FebSystem.h"
#include "rpct/ii/Monitorable.h"
#include "rpct/ii/ConfigurationFlags.h"

#include "rpct/ii/const.h"

#include "rpct/devices/FebBoard.h"
#include "rpct/devices/FebPart.h"

#include "rpct/devices/IFebChipConfiguration.h"

namespace rpct {

log4cplus::Logger FebChip::logger_ = log4cplus::Logger::getInstance("Feb.FebChip");
void FebChip::setupLogger(std::string namePrefix) {
	logger_ = log4cplus::Logger::getInstance(namePrefix + "." + logger_.getName());
}
HardwareItemType * FebChip::type_ = 0;

const std::string & FebChip::getDescription() const
{
    return febpart_.getDescription();
}

void FebChip::configure(ConfigurationSet* configurationSet, int configureFlags)
{
    LOG4CPLUS_DEBUG(logger_, "FebChip::configure()");
    to_be_configured_ = false;
    if ((configureFlags & ConfigurationFlags::INCLUDE_DISABLED) || !isDisabled())
        {
            if (configurationSet)
                {
                    rpct::IFebChipConfiguration const * configuration =
                        dynamic_cast<rpct::IFebChipConfiguration const *> (configurationSet->getDeviceSettings(id_));
                    if (configuration)
                        {
                            setValues(configuration->getVTH()
                                      , configuration->getVMON()
                                      , configuration->getVTHOffset()
                                      , configuration->getVMONOffset());

                            configuration_id_ = configuration->getId();
                            to_be_configured_ = true;
                        }
                    else
                        LOG4CPLUS_DEBUG(logger_, *this << " was not configured, not in configurationset.");
                }
            else
                LOG4CPLUS_DEBUG(logger_, *this << " was not configured, no configurationset.");
        }
    else
        LOG4CPLUS_DEBUG(logger_, *this << " was not configured, it's disabled.");
}

void FebChip::configure(int configureFlags)
{
    LOG4CPLUS_DEBUG(logger_, "FebChip::configure()");
    to_be_configured_ = ((configureFlags & ConfigurationFlags::INCLUDE_DISABLED) || !isDisabled());
}

void FebChip::configure()
{
    LOG4CPLUS_DEBUG(logger_, "FebChip::configure()");
    to_be_configured_ = !isDisabled();
}

void FebChip::loadConfiguration(ConfigurationSet* configurationSet, int configureFlags)
{
    LOG4CPLUS_DEBUG(logger_, "FebChip::loadConfiguration()");
    if ((configureFlags & ConfigurationFlags::INCLUDE_DISABLED) || !isDisabled())
        {
            if (configurationSet)
                {
                    rpct::IFebChipConfiguration const * configuration =
                        dynamic_cast<rpct::IFebChipConfiguration const *> (configurationSet->getDeviceSettings(id_));
                    if (configuration)
                        {
                            setValues(configuration->getVTH()
                                      , configuration->getVMON()
                                      , configuration->getVTHOffset()
                                      , configuration->getVMONOffset());

                            configuration_id_ = configuration->getId();
                        }
                    else
                        LOG4CPLUS_DEBUG(logger_, "Configuration for " << *this << " was not loaded, not in configurationset.");
                }
            else
                LOG4CPLUS_DEBUG(logger_, "Configuration for " << *this << " was not loaded, no configurationset.");
        }
    else
        LOG4CPLUS_DEBUG(logger_, "Configuration for " << *this << " was not loaded, it's disabled.");
}

void FebChip::monitor(bool vth, bool vmon, int configureFlags)
{
    LOG4CPLUS_DEBUG(logger_, "monitor()");
    if ((configureFlags & ConfigurationFlags::INCLUDE_DISABLED) || !isDisabled())
        {
            if (vth)
                febpart_.readVTH(position_);
            if (vmon)
                febpart_.readVMON(position_);
            log("fcmonitored");
        }
    else
        LOG4CPLUS_DEBUG(logger_, *this << "not monitored, it's disabled.");
}

FebChip::FebChip(FebPart & febpart
                 , unsigned char position
                 , int id
                 , bool disabled)
    : FebSystemItem(febpart.getSystem(), id, &febpart)
    , id_(id)
    , febpart_(febpart)
    , position_(position)
    , configuration_id_(-1)
    , to_be_configured_(false)
    , vth_(preset::feb::vth_)
    , vmon_(preset::feb::vmon_)
    , vth_offset_(preset::feb::offset_)
    , vmon_offset_(preset::feb::offset_)
    , vth_offset_set_(preset::feb::offset_)
    , vmon_offset_set_(preset::feb::offset_)
    , vth_ok_(false)
    , vmon_ok_(false)
{
    disable(disabled);
    if (!type_)
        type_ = new HardwareItemType(HardwareItemType::cDevice
                                     , type::feb::febchip_);

    vth_read_response_ = new rpct::i2c::TI2cResponse<FebChip>(this, &FebChip::i2c_read_vth_ready, &FebChip::i2c_read_vth_error);
    vmon_read_response_ = new rpct::i2c::TI2cResponse<FebChip>(this, &FebChip::i2c_read_vmon_ready, &FebChip::i2c_read_vmon_error);
    vth_write_response_ = new rpct::i2c::TI2cResponse<FebChip>(this, &FebChip::i2c_write_vth_ready, &FebChip::i2c_write_vth_error);
    vmon_write_response_ = new rpct::i2c::TI2cResponse<FebChip>(this, &FebChip::i2c_write_vmon_ready, &FebChip::i2c_write_vmon_error);

    addToSystem();
}
FebChip::~FebChip()
{
    removeFromSystem();
    delete vth_read_response_;
    delete vmon_read_response_;
    delete vth_write_response_;
    delete vmon_write_response_;
}

void FebChip::reset()
{
    FebSystemItem::reset();
    configuration_id_ = -1;
    to_be_configured_ = false;

    vth_ = preset::feb::vth_;
    vmon_ = preset::feb::vmon_;
    vth_offset_ = preset::feb::offset_;
    vmon_offset_ = preset::feb::offset_;

    vth_offset_set_ = preset::feb::offset_;
    vmon_offset_set_ = preset::feb::offset_;

    adc_register_vth_.reset_out();
    adc_register_vmon_.reset_out();

    vth_ok_ = false;
    vmon_ok_ = false;
}

void FebChip::printParameters(tools::Parameters & parameters) const
{
    parameters.add("configuration id", configuration_id_);
    parameters.add("vth  read", (adc_register_vth_.vth() * preset::feb::unit_));
    parameters.add("vmon read", (adc_register_vmon_.vmon() * preset::feb::unit_));
    parameters.add("vth  in", (vth_ * preset::feb::unit_));
    parameters.add("vmon in", (vmon_ * preset::feb::unit_));
    parameters.add("vth  offset", (vth_offset_ * preset::feb::unit_));
    parameters.add("vmon offset", (vmon_offset_ * preset::feb::unit_));
    parameters.add("vth  offset in", (vth_offset_set_ * preset::feb::unit_));
    parameters.add("vmon offset in", (vmon_offset_set_ * preset::feb::unit_));
}

void FebChip::setValues(uint16_t vth, uint16_t vmon, int16_t vthoffset, int16_t vmonoffset)
{
    LOG4CPLUS_DEBUG(logger_, "setValues(" << vth << " " << vmon << " " << vth_offset_ << " " << vmonoffset << ')');

    setVTH(vth);
    setVMON(vmon);
    setVTHOffset(vthoffset);
    setVMONOffset(vmonoffset);
}

void FebChip::setVTH(uint16_t value)
{
    if (value != 0)
        {
            vth_ = value;
            vth_ok_ = false;
            configured(0);
            unlog("fcconfigured");
            configuration_id_ = -1;
        }
}
void FebChip::setVMON(uint16_t value)
{
    if (value != 0)
        {
            vmon_ = value;
            vmon_ok_ = false;
            configured(0);
            unlog("fcconfigured");
            configuration_id_ = -1;
        }
}
void FebChip::setVTHOffset(int16_t value)
{
    if (value != preset::feb::offset_keep_)
        {
            vth_offset_set_ = value;
            vth_offset_ = value;
            vth_ok_ = false;
            configured(0);
            unlog("fcconfigured");
            configuration_id_ = -1;
        }
    else
        vth_offset_set_ = vth_offset_;
}
void FebChip::setVMONOffset(int16_t value)
{
    if (value != preset::feb::offset_keep_)
        {
            vmon_offset_set_ = value;
            vmon_offset_ = value;
            vmon_ok_ = false;
            configured(0);
            unlog("fcconfigured");
            configuration_id_ = -1;
        }
    else
        vmon_offset_set_ = vmon_offset_;
}

void FebChip::i2c_read_vth_ready()
    throw()
{
    LOG4CPLUS_TRACE(logger_, "FebChip::readVTH ready for " << *this << ", " << std::hex << (int)(adc_register_vth_.out()[0]) << " " << (int)(adc_register_vth_.out()[1]) << std::dec);
    unlog("i2c_rd_febchip_vth");
    getSystem().publish(getId(), rpct::publish::type::feb::vth_set_, getVTH(true));
    getSystem().publish(getId(), rpct::publish::type::feb::vth_, getVTH());
    if (vth_ok_ && febpart_.isWarm() && isConfigured())
        {
            vth_ok_ = false;
            uint16_t vth_set = (vth_ + vth_offset_set_ + 1) & 0x7fe;
            vth_offset_ = (int16_t)vth_set - adc_register_vth_.vth();
            getSystem().publish(getId(), rpct::publish::type::feb::vth_offset_set_, vth_offset_set_);
            getSystem().publish(getId(), rpct::publish::type::feb::vth_offset_, vth_offset_);
            if (fabs(vth_offset_ - vth_offset_set_) > rpct::preset::feb::vth_max_offset_offset_)
                log("febchip_vth_offset_diff");
            else
                unlog("febchip_vth_offset_diff");
        }
    if (fabs(getVTH(true) - getVTH(false)) > preset::feb::vth_max_offset_error_)
        {
            unlog("febchip_vth_diff");
            log("febchip_vth_diff_e");
        }
    else if (fabs(getVTH(true) - getVTH(false)) > preset::feb::vth_max_offset_)
        {
            unlog("febchip_vth_diff_e");
            log("febchip_vth_diff");
        }
    else
        {
            unlog("febchip_vth_diff");
            unlog("febchip_vth_diff_e");
        }
}
void FebChip::i2c_read_vth_error(rpct::exception::Exception const * e)
    throw()
{
    LOG4CPLUS_TRACE(logger_, "FebChip::readVTH error for " << *this << ", " << std::hex << (int)(adc_register_vth_.out()[0]) << " " << (int)(adc_register_vth_.out()[1]) << std::dec);
    configured(0);
    log("i2c_rd_febchip_vth");
    if (e)
        {
            LOG4CPLUS_WARN(logger_, "Read error in i2c access to "
                           << *this << ": " << e->what());
        }
    else
        {
            LOG4CPLUS_WARN(logger_, "Unknown read error in i2c access to " << *this);
        }
}

void FebChip::i2c_read_vmon_ready()
    throw()
{
    LOG4CPLUS_TRACE(logger_, "FebChip::readVMON ready for " << *this << ", " << std::hex << (int)(adc_register_vmon_.out()[0]) << " " << (int)(adc_register_vmon_.out()[1]) << std::dec);
    unlog("i2c_rd_febchip_vmon");
    getSystem().publish(getId(), rpct::publish::type::feb::vmon_set_, getVMON(true));
    getSystem().publish(getId(), rpct::publish::type::feb::vmon_, getVMON());
    if (vmon_ok_ && febpart_.isWarm() && isConfigured())
        {
            vmon_ok_ = false;
            uint16_t vmon_set = (vmon_ + vmon_offset_set_ + 1) & 0x7fe;
            vmon_offset_ = (int16_t)vmon_set - adc_register_vmon_.vmon();
            getSystem().publish(getId(), rpct::publish::type::feb::vmon_offset_set_, vmon_offset_set_);
            getSystem().publish(getId(), rpct::publish::type::feb::vmon_offset_, vmon_offset_);
            if (fabs(vmon_offset_ - vmon_offset_set_) > rpct::preset::feb::vmon_max_offset_offset_)
                log("febchip_vmon_offset_diff");
            else
                unlog("febchip_vmon_offset_diff");
        }

    if (fabs(getVMON(true) - getVMON(false)) > preset::feb::vmon_max_offset_error_)
        {
            unlog("febchip_vmon_diff");
            log("febchip_vmon_diff_e");
        }
    else if (fabs(getVMON(true) - getVMON(false)) > preset::feb::vmon_max_offset_)
        {
            unlog("febchip_vmon_diff_e");
            log("febchip_vmon_diff");
        }
    else
        {
            unlog("febchip_vmon_diff");
            unlog("febchip_vmon_diff_e");
        }
}
void FebChip::i2c_read_vmon_error(rpct::exception::Exception const * e)
    throw()
{
    LOG4CPLUS_TRACE(logger_, "FebChip::readVMON error for " << *this << ", " << std::hex << (int)(adc_register_vmon_.out()[0]) << " " << (int)(adc_register_vmon_.out()[1]) << std::dec);
    configured(0);
    log("i2c_rd_febchip_vmon");
    if (e)
        {
            LOG4CPLUS_WARN(logger_, "Read error in i2c access to "
                           << *this << ": " << e->what());
        }
    else
        {
            LOG4CPLUS_WARN(logger_, "Unknown read error in i2c access to " << *this);
        }
}

void FebChip::i2c_write_vth_ready()
    throw()
{
    unlog("i2c_wr_febchip_vth");
    vth_ok_ = true;
}
void FebChip::i2c_write_vth_error(rpct::exception::Exception const * e)
    throw()
{
    vth_ok_ = false;
    configured(0);
    unlog("fcconfigured");
    log("i2c_wr_febchip_vth");
    if (e)
        {
            LOG4CPLUS_WARN(logger_, "Write error in i2c access to "
                           << *this << ": " << e->what());
        }
    else
        {
            LOG4CPLUS_WARN(logger_, "Unknown write error in i2c access to " << *this);
        }
}

void FebChip::i2c_write_vmon_ready()
    throw()
{
    unlog("i2c_wr_febchip_vmon");
    vmon_ok_ = true;
}
void FebChip::i2c_write_vmon_error(rpct::exception::Exception const * e)
    throw()
{
    vmon_ok_ = false;
    configured(0);
    unlog("fcconfigured");
    log("i2c_wr_febchip_vmon");
    if (e)
        {
            LOG4CPLUS_WARN(logger_, "Write error in i2c access to "
                           << *this << ": " << e->what());
        }
    else
        {
            LOG4CPLUS_WARN(logger_, "Unknown write error in i2c access to " << *this);
        }
}

} // namespace rpct
