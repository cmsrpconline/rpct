#include "rpct/devices/FebDistributionBoard.h"

#include <sstream>

#include "rpct/ii/feb_const.h"

#include "rpct/ii/i2c/II2cAccess.h"
#include "rpct/devices/IFebAccessPoint.h"

#include "rpct/ii/ConfigurationFlags.h"
#include "rpct/devices/FebSystem.h"

namespace rpct {

log4cplus::Logger FebDistributionBoard::logger_ = log4cplus::Logger::getInstance("Feb.FebDistributionBoard");
void FebDistributionBoard::setupLogger(std::string namePrefix) {
	logger_ = log4cplus::Logger::getInstance(namePrefix + "." + logger_.getName());
}

HardwareItemType * FebDistributionBoard::type_ = 0;
IMonitorable::MonitorItems * FebDistributionBoard::mitems_ = 0;

bool FebDistributionBoard::readPCA9544()
{
    return i2c_access_.read(channel_, devices::feb::preset::pca9544_ + pca9544_address_offset_
                            , &pca9544_register_, pca9544_response_);
}
bool FebDistributionBoard::writePCA9544()
{
    return i2c_access_.write(channel_, devices::feb::preset::pca9544_ + pca9544_address_offset_
                             , &pca9544_register_, pca9544_response_);
}
bool FebDistributionBoard::readPCA9544_DT()
{
    if (dt_)
        return i2c_access_.read(channel_, devices::feb::preset::pca9544_dt_
                                , &pca9544_dt_register_, pca9544_dt_response_);
    else
        return false;
}
bool FebDistributionBoard::writePCA9544_DT()
{
    if (dt_)
        return i2c_access_.write(channel_, devices::feb::preset::pca9544_dt_
                                 , &pca9544_dt_register_, pca9544_dt_response_);
    else
        return false;
}

FebDistributionBoard::FebDistributionBoard(IFebAccessPoint & febaccesspoint
                                           , rpct::i2c::II2cAccess & i2c_access
                                           , int id
                                           , const std::string & description
                                           , unsigned short channel
                                           , bool dt
                                           , unsigned char pca9544_address_offset
                                           , unsigned char dt_channel
                                           , bool disabled)
    : FebSystemItem(febaccesspoint.getSystem()
                    , makePosition(channel, pca9544_address_offset, dt_channel)
                    , &febaccesspoint)
    , id_(id)
    , description_(description)
    , febaccesspoint_(febaccesspoint)
    , i2c_access_(i2c_access)
    , channel_(channel)
    , pca9544_address_offset_(pca9544_address_offset)
    , dt_channel_(dt_channel)
    , dt_(dt)
    , is_hardware_accessed_(false)
{
    disable(disabled);
    if (!type_)
        type_ = new HardwareItemType(HardwareItemType::cBoard
                                     , type::feb::febdistributionboard_);
    if (!mitems_)
        {
            mitems_ = new MonitorItems();
            mitems_->push_back(monitoritem::feb::pcf8574a_);
            mitems_->push_back(monitoritem::feb::temp_);
            mitems_->push_back(monitoritem::feb::vth_);
            mitems_->push_back(monitoritem::feb::vmon_);
        }
    pca9544_response_ = new rpct::i2c::TI2cResponse<FebDistributionBoard>
        (this, &FebDistributionBoard::i2c_pca9544_ready, &FebDistributionBoard::i2c_pca9544_error);
    pca9544_dt_response_ = new rpct::i2c::TI2cResponse<FebDistributionBoard>
        (this, &FebDistributionBoard::i2c_pca9544_dt_ready, &FebDistributionBoard::i2c_pca9544_dt_error);

    addToSystem();
}
FebDistributionBoard::~FebDistributionBoard()
{
    removeFromSystem();
    delete pca9544_response_;
    delete pca9544_dt_response_;
    fbs_type::iterator iFBsEnd = fbs_.end();
    for (fbs_type::iterator iFB = fbs_.begin()
             ; iFB != iFBsEnd
             ; ++iFB)
        delete iFB->second;
}

void FebDistributionBoard::reset()
{
    FebSystemItem::reset();
    IRecursive::reset();
    pca9544_register_.reset();
    pca9544_dt_register_.reset();
    fbs_type::iterator iFBsEnd = fbs_.end();
    for (fbs_type::iterator iFB = fbs_.begin()
             ; iFB != iFBsEnd
             ; ++iFB)
        iFB->second->reset();
    is_hardware_accessed_ = false;
}
void FebDistributionBoard::configure(ConfigurationSet* configurationSet, int configureFlags)
{
    LOG4CPLUS_DEBUG(logger_, "configure()");
    if ((configureFlags & ConfigurationFlags::INCLUDE_DISABLED) || !isDisabled())
        {
            bool only_cold = (configureFlags & rpct::ConfigurationFlags::ONLY_COLD);

            is_hardware_accessed_ = false;
            {
                RecursiveAction ra(this);
                fbs_type::iterator iFBsEnd = fbs_.end();

                if (only_cold)
                    {
                        for (fbs_type::iterator iFB = fbs_.begin()
                                 ; iFB != iFBsEnd
                                 ; ++iFB)
                            iFB->second->readPCF8574a();
                        i2c_access_.execute();
                    }

                configured();
                for (fbs_type::iterator iFB = fbs_.begin()
                         ; iFB != iFBsEnd
                         ; ++iFB)
                    iFB->second->configure(configurationSet, configureFlags);
            }
            disableFebAccess();
            log("configured");
        }
    else
        LOG4CPLUS_DEBUG(logger_, *this << " was not configured, it's disabled.");
}
void FebDistributionBoard::configure(int configureFlags)
{
    LOG4CPLUS_DEBUG(logger_, "configure()");
    if ((configureFlags & ConfigurationFlags::INCLUDE_DISABLED) || !isDisabled())
        {
            is_hardware_accessed_ = false;
            configured();
            {
                RecursiveAction ra(this);
                fbs_type::iterator iFBsEnd = fbs_.end();
                for (fbs_type::iterator iFB = fbs_.begin()
                         ; iFB != iFBsEnd
                         ; ++iFB)
                    iFB->second->configure();
            }
            disableFebAccess();
            log("configured");
        }
    else
        LOG4CPLUS_DEBUG(logger_, *this << " was not configured, it's disabled.");
}

void FebDistributionBoard::loadConfiguration(ConfigurationSet* configurationSet, int configureFlags)
{
    LOG4CPLUS_DEBUG(logger_, "loadConfiguration()");
    if ((configureFlags & ConfigurationFlags::INCLUDE_DISABLED) || !isDisabled())
        {
            fbs_type::iterator iFBsEnd = fbs_.end();
            configured(0);
            for (fbs_type::iterator iFB = fbs_.begin()
                     ; iFB != iFBsEnd
                     ; ++iFB)
                iFB->second->loadConfiguration(configurationSet, configureFlags);
            unlog("configured");
        }
    else
        LOG4CPLUS_DEBUG(logger_, "Configuration for " << *this << " was not loaded, it's disabled.");
}

void FebDistributionBoard::monitor(MonitorItems & items, int configureFlags)
{
    LOG4CPLUS_DEBUG(logger_, "monitor()");
    if ((configureFlags & ConfigurationFlags::INCLUDE_DISABLED) || !isDisabled())
        {
            is_hardware_accessed_ = false;
            {
                RecursiveAction ra(this);

                MonitorItems::iterator ImiEnd = items.end();
                MonitorItems::iterator Imi = std::find(items.begin(), ImiEnd, monitoritem::feb::pca9544_);
                if (Imi != ImiEnd)
                    readPCA9544();
                if (dt_)
                    {
                        Imi = std::find(items.begin(), ImiEnd, monitoritem::feb::pca9544_dt_);
                        if (Imi != ImiEnd)
                            readPCA9544_DT();
                    }

                fbs_type::iterator iFBsEnd = fbs_.end();
                for (fbs_type::iterator iFB = fbs_.begin()
                         ; iFB != iFBsEnd
                         ; ++iFB)
                    iFB->second->monitor(items, configureFlags);
            }
            disableFebAccess();
            log("monitored");
        }
    else
        LOG4CPLUS_DEBUG(logger_, *this << " not monitored, it's disabled.");
}

void FebDistributionBoard::printParameters(tools::Parameters & parameters) const
{
    parameters.add("channel/port", channel_);
    parameters.hexadd("address offset", pca9544_address_offset_);
    if (dt_)
        parameters.hexadd("dt channel", dt_channel_);
    parameters.hexadd("pca9544 in", *(pca9544_register_.in()));
    parameters.hexadd("pca9544 out", *(pca9544_register_.out()));
    if (dt_) {
        parameters.hexadd("pca9544 DT in", *(pca9544_dt_register_.in()));
        parameters.hexadd("pca9544 DT out", *(pca9544_dt_register_.out()));
    }
}

FebBoard & FebDistributionBoard::addFebBoard(int id
                                             , const std::string & description
                                             , unsigned char febtype
                                             , unsigned char position
                                             , bool disabled)
{
    LOG4CPLUS_DEBUG(logger_, "addFebBoard()");
    fbs_type::iterator iFB = fbs_.find(position);
    if (iFB != fbs_.end())
        return *(iFB->second);
    else
        return *((fbs_.insert(std::pair<unsigned char, FebBoard *>
                              (position
                               , new FebBoard(*this
                                              , i2c_access_
                                              , id, description
                                              , channel_
                                              , febtype
                                              , position, disabled)))
                  ).first->second);
}

void FebDistributionBoard::enableFebAccess(bool force)
{
    if (force || !is_hardware_accessed_)
        {
            is_hardware_accessed_ = true;
            pca9544_register_.enable();
            writePCA9544();
            if (dt_) {
                pca9544_dt_register_.enable(dt_channel_);
                writePCA9544_DT();
            }
        }
}
void FebDistributionBoard::disableFebAccess(bool force, bool i2c_execute)
{
    if (force || (!recursive_ && is_hardware_accessed_)) // keep from disabling if acting recursively
        {
            if (dt_) {
                pca9544_dt_register_.disable();
                writePCA9544_DT();
            }
            pca9544_register_.disable();
            is_hardware_accessed_ = false;
            writePCA9544();
            if (i2c_execute)
                i2c_access_.execute();
        }
}

void FebDistributionBoard::i2c_pca9544_error(rpct::exception::Exception const * e)
    throw ()
{
    log("i2c_pca9544");
    if (e)
        {
            LOG4CPLUS_WARN(logger_, "Error in i2c access to pca9544 for "
                           << *this << ": " << e->what());
        }
    else
        {
            LOG4CPLUS_WARN(logger_, "Unknown error in i2c access to pca9544 for " << *this);
        }
}
void FebDistributionBoard::i2c_pca9544_ready()
    throw ()
{
    unlog("i2c_pca9544");
}


void FebDistributionBoard::i2c_pca9544_dt_error(rpct::exception::Exception const * e)
    throw ()
{
    log("i2c_pca9544_dt");
    if (e)
        {
            LOG4CPLUS_WARN(logger_, "Error in i2c access to pca9544_dt for "
                           << *this << ": " << e->what());
        }
    else
        {
            LOG4CPLUS_WARN(logger_, "Unknown error in i2c access to pca9544_dt for " << *this);
        }
}
void FebDistributionBoard::i2c_pca9544_dt_ready()
    throw ()
{
    unlog("i2c_pca9544_dt");
}

} // namespace rpct
