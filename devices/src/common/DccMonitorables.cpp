#include "rpct/devices/DccMonitorables.h"
#include <sstream>
#include <bitset>
#include "cgicc/Cgicc.h"
#include "rpct/devices/VDcc_pkgDef.h"
#include "rpct/devices/Dcc.h"

using namespace std;
using namespace rpct;

DccMonitorables::DccMonitorables(): 
    fedId_(0), ebStatus_(0), ttsStatus_(0), ebOosDbg_(-1), ebEvtCnt_(-1) {}


HardwareItemType DccMonitorables::getDeviceType() { return Dcc::TYPE; }

std::string DccMonitorables::printInfo() const
{
  std::ostringstream str;
  str << "<Huge> ";

  str << "<large> FedId= </large> <b>" << std::dec <<getFedId() << " </b>";
  str << "<large> &nbsp;&nbsp;&nbsp;  Ev= </large>" << getEbEvtCnt();

  

  str << " &nbsp;&nbsp;&nbsp;  Status: ";

//  bitset<32> oosDbg(getEbOosDbg());
//  str <<" OssDbg: "<<oosDbg;

  bitset<32> status(getEbStatus());
  string color;
  color = (status[EBB_STATUS_QPLL_LOCKED] && !status[EBB_STATUS_QPLL_LOCK_LOST] ) 
           ? "green" : "red"; 
  str <<"<font color="<<color<<">QPLL</font>_"; 
  color = (status[EBB_STATUS_TTC_READY] && !status[EBB_STATUS_TTC_READY_LOST] ) 
           ? "green" : "red"; 
  str <<"<font color="<<color<<">TTCrx</font>_"; 
  //IH
  color = "green";
  if (status[EBB_STATUS_IH_ALMOST_FULL]) color = "orange";
  if (status[EBB_STATUS_IH_FULL] || status[EBB_STATUS_IH_OVERRUN]) color = "red";
  str <<"<font color="<<color<<">IH</font>_"; 
  //BC0
  color = (status[EBB_STATUS_BX_NOT_SET]) ? "red" : "green"; 
  str <<"<font color="<<color<<">BC0</font>_"; 
  //EC0
  color = (status[EBB_STATUS_EV_NOT_SET]) ? "red" : "green"; 
  str <<"<font color="<<color<<">EC0</font>_"; 
  //Run 
  color = (status[EBB_STATUS_RUN]) ? "green" : "red"; 
  str <<"<font color="<<color<<">Run</font>"; 

 
  str << " <large>  &nbsp;&nbsp;&nbsp; TTS:</large> ";
  switch (getTtsStatus() ) {
  case (1) : str << "<font color=orange> Warning</font>"; break;
  case (2) : str << "<font color=red> OutOfSync </font>"; break;
  case (4) : str << "<font color=red> Busy </font>"; break;
  case (8) : str << "<font color=green> Ready</font>"; break;
  case (0) : case (15): str << "Disconnected"; break;
  default: str << "Unknown:" << bitset<32>(getTtsStatus());
  };

  str << "</Huge>";
  return str.str();
}
