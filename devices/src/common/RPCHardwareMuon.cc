#include "rpct/devices/RPCHardwareMuon.h"
#include "rpct/std/TException.h"

#include <sstream>
#include <iomanip>

using namespace std;
RPCHardwareMuon::RPCHardwareMuon(int ptCode, int quality, int sign, MuonType type):
    RPCGBSMuon(ptCode, quality, sign, 0, 0), type_(type), index_(0)  {
    m_Killed = false;

    m_GBData = 0;

    m_EtaAddress = 0;
    m_PhiAddress = 0;
}

RPCHardwareMuon* RPCHardwareMuon::createMuon(int ptCode, int quality, int sign, MuonType type) {
	if(type == mtPacOut) 
		return new RPCPacOutMuon(ptCode, quality, sign);
	else if(type == mtTBSortOut) 
		return new RPCTbGbSortOutMuon(ptCode, quality, sign);
	else if(type == mtTCSortOut) 
		return new RPCTcSortOutMuon(ptCode, quality, sign);
	else if(type == mtHalfSortOut) 
		return new RPCHalfSortOutMuon(ptCode, quality, sign);
	else if(type == mtFinalSortOut) 
		return new RPCFinalSortOutMuon(ptCode, quality, sign);
		
	throw TException("RPCHardwareMuon::createMuon: unknow muon type");
}

RPCPacOutMuon::RPCPacOutMuon(int ptCode, int quality, int sign): RPCHardwareMuon(ptCode, quality, sign, RPCHardwareMuon::mtPacOut) {

} 

RPCPacOutMuon::RPCPacOutMuon(unsigned int value, unsigned int index): RPCHardwareMuon(RPCHardwareMuon::mtPacOut, index) {
  unsigned int shift = 0;
  m_Sign       = (value & (m_signBitsMask<<shift))  >> shift;  shift += m_signBitsCnt;  
  m_PtCode     = (value & (m_ptBitsMask<<shift))    >> shift;  shift += m_ptBitsCnt;
  m_Quality    = (value & (m_qualBitsMask<<shift))  >> shift;  shift += m_qualBitsCnt;
}

unsigned int RPCPacOutMuon::toBits() {
  unsigned int value = 0;
  unsigned int shift = 0;
  value =         (m_Sign<<shift);       shift += m_signBitsCnt;
  value = value | (m_PtCode<<shift);     shift += m_ptBitsCnt;
  value = value | (m_Quality<<shift);    shift += m_qualBitsCnt;
   
  return value;
}

//-----------------------
RPCTbGbSortOutMuon::RPCTbGbSortOutMuon(int ptCode, int quality, int sign): RPCHardwareMuon(ptCode, quality, sign, RPCHardwareMuon::mtTBSortOut) {

} 

RPCTbGbSortOutMuon::RPCTbGbSortOutMuon(unsigned int value, unsigned int index): RPCHardwareMuon(RPCHardwareMuon::mtTBSortOut, index) {
  unsigned int shift = 0;
  m_Sign       = (value & (m_signBitsMask<<shift))  >> shift;  shift += m_signBitsCnt;  
  m_PtCode     = (value & (m_ptBitsMask<<shift))    >> shift;  shift += m_ptBitsCnt;
  m_Quality    = (value & (m_qualBitsMask<<shift))  >> shift;  shift += m_qualBitsCnt;
  m_PhiAddress = (value & (m_phiBitsMask<<shift))   >> shift;  shift += m_phiBitsCnt;
  m_EtaAddress = (value & (m_etaBitsMask<<shift))   >> shift;  shift += m_etaBitsCnt;
  m_GBData     = (value & (m_gbDataBitsMask<<shift))>> shift;  shift += m_gbDataBitsCnt;       
}

unsigned int RPCTbGbSortOutMuon::toBits() {
  unsigned int value = 0;
  unsigned int shift = 0;
  value =         (m_Sign<<shift);       shift += m_signBitsCnt;
  value = value | (m_PtCode<<shift);     shift += m_ptBitsCnt;
  value = value | (m_Quality<<shift);    shift += m_qualBitsCnt;
  value = value | (m_PhiAddress<<shift); shift += m_phiBitsCnt;
  value = value | (m_EtaAddress<<shift); shift += m_etaBitsCnt; 
  value = value | (m_GBData<<shift);     shift += m_gbDataBitsCnt;
  return value;
}

//-----------------------
RPCTcSortOutMuon::RPCTcSortOutMuon(int ptCode, int quality, int sign): RPCHardwareMuon(ptCode, quality, sign, RPCHardwareMuon::mtTCSortOut) {

} 

RPCTcSortOutMuon::RPCTcSortOutMuon(unsigned int value, unsigned int index): RPCHardwareMuon(RPCHardwareMuon::mtTCSortOut, index) {
  unsigned int shift = 0;
  m_Sign       = (value & (m_signBitsMask<<shift))  >> shift;  shift += m_signBitsCnt; 
  m_PtCode     = (value & (m_ptBitsMask<<shift))    >> shift;  shift += m_ptBitsCnt;
  m_Quality    = (value & (m_qualBitsMask<<shift))  >> shift;  shift += m_qualBitsCnt;
  m_PhiAddress = (value & (m_phiBitsMask<<shift))   >> shift;  shift += m_phiBitsCnt;
  m_EtaAddress = (value & (m_etaBitsMask<<shift))   >> shift;  shift += m_etaBitsCnt;
  m_GBData     = (value & (m_gbDataBitsMask<<shift))>> shift;  shift += m_gbDataBitsCnt;     
}

unsigned int RPCTcSortOutMuon::toBits() {
  unsigned int value = 0;
  unsigned int shift = 0;
  value =         (m_Sign<<shift);       shift += m_signBitsCnt;
  value = value | (m_PtCode<<shift);     shift += m_ptBitsCnt;
  value = value | (m_Quality<<shift);    shift += m_qualBitsCnt;
  value = value | (m_PhiAddress<<shift); shift += m_phiBitsCnt;  
  value = value | (m_EtaAddress<<shift); shift += m_etaBitsCnt; 
  value = value | (m_GBData<<shift);     shift += m_gbDataBitsCnt;
  return value;
}
//-------------------------------
RPCHalfSortOutMuon::RPCHalfSortOutMuon(int ptCode, int quality, int sign): RPCHardwareMuon(ptCode, quality, sign, RPCHardwareMuon::mtHalfSortOut) {

} 

RPCHalfSortOutMuon::RPCHalfSortOutMuon(unsigned int value, unsigned int index): RPCHardwareMuon(RPCHardwareMuon::mtHalfSortOut, index) {
  unsigned int shift = 0;
  m_Sign       = (value & (m_signBitsMask<<shift)) >> shift;  shift += m_signBitsCnt;
  m_PtCode     = (value & (m_ptBitsMask<<shift))   >> shift;  shift += m_ptBitsCnt;
  m_Quality    = (value & (m_qualBitsMask<<shift)) >> shift;  shift += m_qualBitsCnt;
  m_PhiAddress = (value & (m_phiBitsMask<<shift))  >> shift;  shift += m_phiBitsCnt;
  m_EtaAddress = (value & (m_etaBitsMask<<shift))  >> shift;  shift += m_etaBitsCnt;
}

unsigned int RPCHalfSortOutMuon::toBits() {
  unsigned int value = 0;

  unsigned int shift = 0;
  value = value | (m_Sign<<shift);       shift += m_signBitsCnt;
  value = value | (m_PtCode<<shift);     shift += m_ptBitsCnt;
  value = value | (m_Quality<<shift);    shift += m_qualBitsCnt;
  value = value | (m_PhiAddress<<shift); shift += m_phiBitsCnt;
  value = value | (m_EtaAddress<<shift); shift += m_etaBitsCnt; 
   
  return value;
}
//------------------------------------------------------------------------------------
RPCFinalSortOutMuon::RPCFinalSortOutMuon(int ptCode, int quality, int sign): RPCHardwareMuon(ptCode, quality, sign, RPCHardwareMuon::mtFinalSortOut) {

} 

RPCFinalSortOutMuon::RPCFinalSortOutMuon(unsigned int value, unsigned int index): RPCHardwareMuon(RPCHardwareMuon::mtFinalSortOut, index) {
  unsigned int shift = 0;
  m_PhiAddress =  value &  m_phiBitsMask;                     shift += m_phiBitsCnt;
  m_PtCode     = (value & (m_ptBitsMask<<shift))   >> shift;  shift += m_ptBitsCnt;
  m_Quality    = (value & (m_qualBitsMask<<shift)) >> shift;  shift += m_qualBitsCnt;
  m_EtaAddress = (value & (m_etaBitsMask<<shift))  >> shift;  shift += m_etaBitsCnt + 1; //+1 beacouse H/F bits, unused in RPC:
  m_Sign       = (value & (m_signBitsMask<<shift)) >> shift;  shift += m_signBitsCnt;
  signValid_   = (value & (m_signValidBitsMask<<shift)) >> shift;  shift += m_signValidBitsCnt;
  bcn_         = (value & (m_bcnBitsMask<<shift)) >> shift;  shift += m_bcnBitsCnt;	
  bc0_         = (value & (m_bc0BitsMask<<shift)) >> shift;  shift += m_bc0BitsCnt;
  
  par_  = (value & (m_parityBitsMask<<shift)) >> shift; shift += m_parityBitsCnt;  
  
  m_PtCode = (~(m_PtCode)) & m_ptBitsMask;
  m_Quality = (~(m_Quality)) & m_qualBitsMask;
}

/*RPCFinalSortOutMuon::RPCFinalSortOutMuon(unsigned int value, unsigned int index): RPCHardwareMuon(RPCHardwareMuon::mtFinalSortOut, index) {
  unsigned int shift = 0;
  m_Sign       = (value & (m_signBitsMask<<shift)) >> shift;  shift += m_signBitsCnt;  
  m_PtCode     = (value & (m_ptBitsMask<<shift))   >> shift;  shift += m_ptBitsCnt;
  m_Quality    = (value & (m_qualBitsMask<<shift)) >> shift;  shift += m_qualBitsCnt;
  m_PhiAddress =  value &  m_phiBitsMask           >> shift;  shift += m_phiBitsCnt;
  m_EtaAddress = (value & (m_etaBitsMask<<shift))  >> shift;  shift += m_etaBitsCnt; 

  bc0_       = (value & (m_bc0BitsMask<<shift)) >> shift;  shift += m_bc0BitsCnt;
  bcn_       = (value & (m_bcnBitsMask<<shift)) >> shift;  shift += m_bcnBitsCnt;
  cntrBits_  = (value & (m_cntrBitsMask<<shift)) >> shift; shift += m_cntrBitsCnt;  
  
  //m_PtCode = (~(m_PtCode)) & m_ptBitsMask;
  //m_Quality = (~(m_Quality)) & m_qualBitsMask;
}*/

/*unsigned int RPCFinalSortOutMuon::toBits() {
  unsigned int value = 0;
    	
  unsigned int shift = 0;
  //bita are reversed, to be zero when cable not connected
  unsigned int ptCode =  (~(m_PtCode)) & m_ptBitsMask; 
  unsigned int quality = (~(m_Quality)) & m_qualBitsMask;

  value = value |  m_PhiAddress;              shift += m_phiBitsCnt;    
  value = value | (ptCode<<shift);            shift += m_ptBitsCnt;
  value = value | (quality<<shift);           shift += m_qualBitsCnt;
  
  //+1 beacouse H/F bits, unused in RPC:
  value = value | (m_EtaAddress<<shift); shift += m_etaBitsCnt + 1; 
  value = value | (m_Sign<<shift);       shift += m_signBitsCnt;
  
  value = value | (1<<shift);       shift += m_signValidBitsCnt; //sign always valid
  return value;
}*/

unsigned int RPCFinalSortOutMuon::toBits() {
  unsigned int value = 0;

  unsigned int shift = 0;
  value = value | (m_Sign<<shift);       shift += m_signBitsCnt;
  value = value | (m_PtCode<<shift);     shift += m_ptBitsCnt;
  value = value | (m_Quality<<shift);    shift += m_qualBitsCnt;
  value = value | (m_PhiAddress<<shift); shift += m_phiBitsCnt;
  value = value | (m_EtaAddress<<shift); shift += m_etaBitsCnt; 
   
  return value;
}

std::string RPCFinalSortOutMuon::toString(int format) const {
  ostringstream ostr;
  if(format == 0) {
  	 ostr<<"qu "<<m_Quality<<", pt "<<setw(2)<<m_PtCode<<", sig "<<m_Sign
         <<", phi "<<setw(3)<<m_PhiAddress<<", eta "<<setw(2)<<m_EtaAddress;
  }
  else if( format == 1) {
  	ostr<<" "<<bcn_<<" "<<bc0_<<" . "<<m_Quality<<" "<<setw(2)<<m_PtCode<<" "<<m_Sign
        <<" "<<setw(3)<<m_PhiAddress<<" "<<setw(2)<<m_EtaAddress;
  }    
  else if( format == 2) {
  	ostr<<" "<<m_Quality<<" "<<setw(2)<<m_PtCode<<" "<<m_Sign;
  }    
  
  return ostr.str();
}
