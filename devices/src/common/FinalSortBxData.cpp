#include "rpct/devices/FinalSortBxData.h"
#include "rpct/devices/cmssw/RPCConst.h"
#include "rpct/std/bitcpy.h"
#include "rpct/std/tbutil.h"

#include <iomanip>
#include <sstream>

using namespace std;

namespace rpct {

FinalSortBxDataFactory FinalSortBxDataFactory::instance_;

FinalSortBxData::FinalSortBxData(IDiagnosticReadout::BxData& bxData): StandardBxData(bxData) {
	  	unsigned int pos = 0;
	  	
	  	unsigned int fsbOutMuonsCnt = 8; 
	  	unsigned int fsbMuonBitsCnt = RPCFinalSortOutMuon::getDaqMuonBitsCnt();// 31;	
			  							
		unsigned int hsbOutMuonBitsCnt = RPCHalfSortOutMuon::getMuonBitsCnt(); //22;

		unsigned int index = 0;
		for(unsigned int iMu = 0; iMu < fsbOutMuonsCnt; iMu++) {
			unsigned int muonData = 0;
    		bitcpy(&muonData, 0, bxData_.getData(), pos, fsbMuonBitsCnt);     
    		pos += fsbMuonBitsCnt;	
    		RPCFinalSortOutMuon* muon = new RPCFinalSortOutMuon(muonData, index);
    		dataStr_ = muon->toString(1)+ " | " + dataStr_;
    		checkAndAddMuon(muon);
    		//dataStr_ = dataToHex(&muonData, fsbMuonBitsCnt) + " | " + dataStr_;;
    		index++;
    	}
    	
		dataStr_ = "out| " + dataStr_;

		for(unsigned int iBe = 0; iBe < 4; iBe++) { //8 muons from barrel (4 from HSB 0 + 4 HSB 1), then 8 from endcap (4 from HSB 0 + 4 HSB 1),
			for(unsigned int iMu = 0; iMu < 4; iMu++) {  
				unsigned int muonData = 0;
	    		bitcpy(&muonData, 0, bxData_.getData(), pos, hsbOutMuonBitsCnt);     
	    		pos += hsbOutMuonBitsCnt;	
	    		
    			RPCHalfSortOutMuon* muon =  new RPCHalfSortOutMuon(muonData, index);
    			dataStr_ = muon->toString(1)+ " | " + dataStr_;
    			
    			if(muonData != 0)
    				muonVector_.push_back(muon); 		
    			else 
    				delete muon;				
				
				index++;
	    	}	
			if(iBe == 0)
				dataStr_ = "0b| " + dataStr_;	
			else if(iBe == 1)
				dataStr_ = "1b| " + dataStr_;	
			else if(iBe == 2)
				dataStr_ = "0e| " + dataStr_;	
			else if(iBe == 3)
				dataStr_ = "1e| " + dataStr_;				
		}
			
		unsigned int bcn0Bit = 0; //1 bit
			
		unsigned int bcnBitsCnt = 8; //recived bcn
		unsigned int bcnData = 0;	
		
		bitcpy(&bcn0Bit,    0, bxData_.getData(), pos, 1);                                pos += 1;
		bitcpy(&bcnData,    0, bxData_.getData(), pos, bcnBitsCnt);                       pos += bcnBitsCnt;
		ostringstream ostr;
  		ostr<<dec<<setw(4)<<bcnData<<" "<<bcn0Bit;
    	dataStr_ = ostr.str() + " | " + dataStr_;   
		
		ostr.clear();
		
		bitcpy(&bcn0Bit,    0, bxData_.getData(), pos, 1);                                pos += 1;
		bitcpy(&bcnData,    0, bxData_.getData(), pos, bcnBitsCnt);                       pos += bcnBitsCnt;	
		
		ostr<<dec<<setw(4)<<bcnData<<" "<<bcn0Bit;
    	dataStr_ = ostr.str() + " | " + dataStr_; 
    	
    	//dataStr_ = dataToHex(bxData_.getData(), 615); //TODO - remove!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				    	  
}

}
