#include "rpct/devices/TbGbSortBxData.h"
#include "rpct/devices/cmssw/RPCConst.h"
#include "rpct/std/bitcpy.h"
#include "rpct/std/tbutil.h"
#include "rpct/devices/TriggerBoard.h"

#include <iomanip>
#include<sstream>

using namespace std;

namespace rpct {

TbGbSortBxDataFactory TbGbSortBxDataFactory::instance_;

TbGbSortBxData::TbGbSortBxData(IDiagnosticReadout::BxData& bxData, IDiagnosticReadout& ownerReadout)
        : StandardBxData(bxData) {
	
	boost::dynamic_bitset<>& usedPacs = (dynamic_cast<TriggerBoard&>(ownerReadout.GetOwner().getBoard())).GetUsedPacs(); 
	
  	unsigned int pos = 0;
  	
  	//unsigned int gbSortOutBitsCnt = 68;
	
	unsigned int channelValidBitsCnt = 4;
	
	unsigned int tbMuonsCnt = 4;
	unsigned int tbMuonBitsCnt = RPCTbGbSortOutMuon::getMuonBitsCnt(); //
		
	unsigned int pacsCnt = 4;	  	
	unsigned int pacLogConeCnt = 12;

 	unsigned int pacLogConeOutBitsCnt = RPCPacOutMuon::getMuonBitsCnt();

	unsigned int bcn0Bit = 0; //1 bit
		
	unsigned int bcnBitsCnt = 3;
	unsigned int bcnData = 0;	 	
    
    unsigned int index = 0;

	for(unsigned int iMu = 0; iMu < tbMuonsCnt; iMu++, index++) {
		unsigned int muonData = 0;
		bitcpy(&muonData, 0, bxData_.getData(), pos, tbMuonBitsCnt);                 pos += tbMuonBitsCnt;	
		RPCTbGbSortOutMuon* muon = new RPCTbGbSortOutMuon(muonData, index);
  		dataStr_ = muon->toString(1) + " | " + dataStr_;
  		if(muonData != 0)
  			muonVector_.push_back(muon);
  		else
  			delete muon;
	}

    dataStr_ = " |out| " + dataStr_;
    
	bitcpy(&bcn0Bit,    0, bxData_.getData(), pos, 1);                                pos += 1;
	bitcpy(&bcnData,    0, bxData_.getData(), pos, bcnBitsCnt);                       pos += bcnBitsCnt;
	
	bitcpy(&channelsValid_,    0, bxData_.getData(), pos, channelValidBitsCnt);       pos += channelValidBitsCnt;			  
		
	for(unsigned int iPac = 0; iPac < pacsCnt; iPac++) {				  
		for(unsigned int iLC = 0; iLC < pacLogConeCnt; iLC++, index++) {
			unsigned int muonData = 0;
			bitcpy(&muonData, 0, bxData_.getData(), pos, pacLogConeOutBitsCnt);       pos += pacLogConeOutBitsCnt;	
			RPCPacOutMuon* muon = new RPCPacOutMuon(muonData, index);
  			dataStr_ = muon->toString(2) + " | " + dataStr_;
  			if(usedPacs[iPac] && muonData != 0)
	  			muonVector_.push_back(muon);
  	  		else
  	  			delete muon;
    	}
    	
    	unsigned int bcn0Bit = 0; //1 bit
		unsigned int bcnData = 0;	
    	
		bitcpy(&bcn0Bit,    0, bxData_.getData(), pos, 1);                              pos += 1;
		bitcpy(&bcnData,    0, bxData_.getData(), pos, bcnBitsCnt);                     pos += bcnBitsCnt;	
		
		ostringstream ostr;
		ostr<<hex<<setw(3)<<bcnData<<" "<<bcn0Bit;
		dataStr_ = ostr.str() + " |-| " + dataStr_;  
	}
	ostringstream ostr;
	ostr<<hex<<setw(3)<<bcnData<<" "<<bcn0Bit<<" "<<channelsValid_;
	dataStr_ = ostr.str() + " | " + dataStr_; 
}



}
