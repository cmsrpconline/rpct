#include "rpct/devices/DccInputHandlerStatus.h"
#include "rpct/devices/DccInputHandler.h"
#include "rpct/devices/VDcc_pkgDef.h"

#include <sstream>
#include <bitset>
#include <iostream>
#include <iomanip>


using namespace rpct;
using namespace std;

DccInputHandlerStatus::DccInputHandlerStatus(DccInputHandler &ih)
  : inputErrorCounters(nHandlerInput)
{
   theId = ih.id();
   theStatus = ih.read(IHA_STATUS);
   theQueueStatus = ih.read(IHA_QUEUE_STATUS);
   theEmergencyMask = ih.read(IHA_EMERGENCY_MASK);
   theSyncAdjastment = ih.read(IHA_SYNC_ADJUST);
   if(0x11==theStatus) 
       theEventCounter = ih.read(IHA_EVCNT_L)+(ih.read(IHA_EVCNT_H)<<16); else theEventCounter = -1;

   for (int input = 0; input < nHandlerInput; ++input) {
     ErrorCounter errs;
     errs.NOTINTABLE = ih.read(IHA_CNT_RXNOTINTABLE+input);
     errs.DISPERR    = ih.read(IHA_CNT_RXDISPERR+input);
     errs.LOSSOFSYNC = ih.read(IHA_CNT_RXLOSSOFSYNC+input);
     inputErrorCounters[input] = errs; 
   }
}

string DccInputHandlerStatus::print(bool forHtml)
{
  ostringstream str;
  bool setRed = false; 
  str<<"IH:"<<theId
     <<"    Status 0x"<<std::hex<<theStatus;
  int b1 = theQueueStatus & 0xf;
  int b2 = (theQueueStatus >> 4) & 0xf;
  int b3 = (theQueueStatus >> 8) & 0xf;
  int b4 = (theQueueStatus >>12) & 0xf;
  if ( (b1!=0 && b1!=8) || (b2!=0 && b2!=8) || (b3!=0 && b3!=8) || (b4!=0 && b4 !=8) ){ str << "<font color=red>";  setRed = true;}
  str<<"    Queue  0x"<<std::hex<<b4<<b3<<b2<<b1; if (setRed) {setRed=false;  str<<"</font>";}
  str<<"    Evnts: "<<std::dec<<theEventCounter ;
  if (theEmergencyMask != 0) { str << "<font color=red>";  setRed = true;}
  str <<"    EmMask "<<std::bitset<nHandlerInput>(theEmergencyMask); if (setRed) {setRed=false;  str<<"</font>";}
//  str<<std::dec <<"    SyncAdj: "<<theSyncAdjastment;
  str<<"    ErCnt ";
  int isMaskedQueue = theQueueStatus;
  str<<"<code>";
  for(int inn=nHandlerInput-1; inn >= 0; inn--) {
    unsigned int numtmp = inputErrorCounters[inn].sum();
    unsigned int myModulo = (theId*4+(3-inn))%9; 
    bool expectedBigCounter = (myModulo==0 || myModulo==8);
    int queueMask = (theQueueStatus >> (inn*4)) & 0xf;

    setRed = false;
    if (    numtmp>50 && forHtml && !expectedBigCounter && (queueMask!=8) ) {setRed=true;   str<<"<font color=red>";}
    str.fill('_');
    str<<" in"<<inn <<":{"        <<setw(6)<<inputErrorCounters[inn].NOTINTABLE
                             <<","<<setw(6)<<inputErrorCounters[inn].DISPERR
                             <<","<<setw(6)<<inputErrorCounters[inn].LOSSOFSYNC
                             <<"} ";
    if (setRed) {setRed=false;  str<<"</font>";}
  } 
  str<<"</code>";
  return str.str();
}

