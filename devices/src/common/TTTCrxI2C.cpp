//---------------------------------------------------------------------------
#ifdef __BORLANDC__
#include "precompiled.h"
#pragma hdrstop
#endif

#include "rpct/devices/TTTCrxI2C.h"

//---------------------------------------------------------------------------
using namespace log4cplus;

namespace rpct {

Logger TTTCrxI2C::logger = Logger::getInstance("TTTCrxI2C");
void TTTCrxI2C::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}

void TTTCrxI2C::SetAddress(unsigned char addr)
{
  I2CPointer = (addr << 1) & 0xfe;
  I2CData = (addr << 1) | 1;
}

unsigned char TTTCrxI2C::GetAddress()
{
  return I2CPointer >> 1;
}

void TTTCrxI2C::SetI2CInterface(TI2CInterface& i2c)
{
  I2C = &i2c;
}

}
