#include "rpct/devices/FebSystemItem.h"

#include <fstream>
#include <sstream>

#include "rpct/ii/const.h"
#include "rpct/devices/FebSystem.h"

namespace rpct {

FebSystemItem::FebSystemItem(FebSystem & febsystem
                             , int position
                             , FebSystemItem * parent)
    : tools::TTreeLogState<FebSystemItem *>(position, parent)
    , febsystem_(febsystem)
{}

FebSystemItem::~FebSystemItem()
{}

void FebSystemItem::reset()
{
    HardwareFlags::reset();
    tools::LogState::reset();
}

void FebSystemItem::addToSystem()
{
    febsystem_.addItem(*this);
}
void FebSystemItem::removeFromSystem()
{
    febsystem_.removeItem(*this);
}

std::ostream & operator<<(std::ostream & outstream, const FebSystemItem & fsi)
{
    outstream << fsi.getType().getType() << '(' << fsi.getId() << ':' << fsi.getDescription() << ')';
    return outstream;
}

} // namespace rpct
