#include "rpct/devices/Dcc.h"
#include "rpct/devices/DccSettingsImpl.h"
#include "rpct/devices/DccMonitorables.h"
#include "rpct/devices/System.h" 
#include "rpct/std/IllegalArgumentException.h"
#include "rpct/std/NullPointerException.h"
#include "rpct/ii/ConfigurationFlags.h"

using namespace std;
using namespace log4cplus;

namespace rpct {

Logger Dcc::logger = Logger::getInstance("Dcc");
void Dcc::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}
const HardwareItemType Dcc::TYPE(HardwareItemType::cBoard, "DCC");

Dcc::~Dcc() {}

Dcc::Dcc(int ident, const char* desc, TVMEInterface* vme,
        int vme_board_address, VmeCrate* c, int pos) 
  : 
    BoardBase(ident, desc, TYPE, c, pos), vme_(vme),
    vmeAddress_(vme_board_address), preTrigger_(3), postTrigger_(3),
    dccDevice(vme_board_address,*this,vme) 
{
    vme_->SetDefaultAddressingMode(TVMEInterface::am32);
    Init();
    LOG4CPLUS_INFO(logger, desc << " " << hex << vme_board_address);     
}


void Dcc::Init(){
    devices_.push_back(&dccDevice);
}

void Dcc::reset(){
    dccDevice.reset();
}

void Dcc::initialize(){
    dccDevice.initialize();
}

void Dcc::configure(DeviceSettings* settings){
    dccDevice.configure(settings, ConfigurationFlags::FORCE_CONFIGURE);
}

void Dcc::run(){
    dccDevice.run();
}

void Dcc::start(DeviceSettings* settings) { 
    reset();
    initialize();
    //Set pre-, post-trigger BXes and unmasks inupts
    configure(settings);
    //run
    run();
}

void Dcc::setBXRange(unsigned int pre, unsigned int post){
    dccDevice.setBXRange(pre, post);
}

void Dcc::forceTTS(unsigned int status=0x8){
    dccDevice.forceTTS(status);
}

void Dcc::freeTTS(){
    dccDevice.freeTTS();
}

int Dcc::checkTTS(){
    return dccDevice.checkTTS();
}

DccMonitorables Dcc::checkBoard(){//Check status of the board
    return dccDevice.checkDevice();
}

}
