#include "rpct/devices/RPCFebAccessPoint.h"

#include "rpct/devices/cb.h"
#include "rpct/devices/RPCFebSystem.h"

namespace rpct {

RPCFebAccessPoint::RPCFebAccessPoint(RPCFebSystem & system
                                     , TCB & controlboard
                                     , bool disabled)
    : IFebAccessPoint(system
                      , controlboard.getI2cAccess()
                      , controlboard.getId()
                      , controlboard.getDescription()
                      , controlboard.getPosition()
                      , disabled)
    , controlboard_(controlboard)
{}

} // namespace rpct
