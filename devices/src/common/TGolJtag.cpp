//---------------------------------------------------------------------------
#ifdef __BORLANDC__
#include "precompiled.h"
#pragma hdrstop
#endif

#include "TGolJtag.h"

//---------------------------------------------------------------------------

using namespace bs;
using namespace std;


namespace rpct {

unsigned char TGolJtag::WriteReg(std::string name, unsigned char value)
{

  TScanVector valVector = ValToScanVector(value);

  G_Log.out() << "TGolJtag::WriteReg(" << name << ", " << (int)value << "), valVector = " << valVector << endl;


  TScanVector drseq, drcap, ircap;


  
  // test
  TScanVector testcap;
  Contr.ProgramStart();
  Contr.TAPtoReset();
  GOL.SetState("BYPASS");
  Contr.IRTransmitRec(Board.GenerateIRSequence(), ircap);
  Contr.DRTransmitRec("101010101010101", testcap);  
  GOL.SetState("IDCODE");
  Contr.IRTransmitRec(Board.GenerateIRSequence(), ircap);
  Contr.DRLoopRec(Board.GetDRLength(), drcap);
  Contr.ProgramEnd();
  Contr.ProgramExecute();                       
  G_Log.out() << "    testcap = " << testcap << endl;
  G_Log.out() << "    drcap = " << drcap << endl;

  // retrieve current CONF register contents
  GOL.SetState("CONF_R");
  Contr.ProgramStart();
  Contr.TAPtoReset();
  Contr.IRTransmitRec(Board.GenerateIRSequence(), ircap);
  Contr.DRLoopRec(Board.GetDRLength(), drcap);
  Contr.ProgramEnd();
  Contr.ProgramExecute();
  G_Log.out() << "    drcap = " << drcap << endl;
  Board.InterpreteDRSequence(drcap, IElement::btSEND);


  ConfReg[name].SetStateSeq(valVector);

  // send new values to the CONF register  
  GOL.SetState("CONF_RW");
  Contr.ProgramStart();
  Contr.TAPtoReset();                 
  G_Log.out() << "    CONF_RW = " << Board.GenerateIRSequence() << endl;
  Contr.IRTransmitRec(Board.GenerateIRSequence(), ircap);
  drseq = Board.GenerateDRSequence();
  G_Log.out() << "    drseg = " << drseq << endl;
  Contr.DRTransmit(drseq);
  //Contr.DRLoopRec(Board.GetDRLength(), drcap);
  Contr.ProgramEnd();
  Contr.ProgramExecute();
                    
  // retrieve current CONF register contents   
  GOL.SetState("CONF_R");
  Contr.ProgramStart();
  Contr.TAPtoReset();
  Contr.IRTransmitRec(Board.GenerateIRSequence(), ircap);
  Contr.DRLoopRec(Board.GetDRLength(), drcap);
  Contr.ProgramEnd();
  Contr.ProgramExecute();
  G_Log.out() << "    drcap = " << drcap << endl;
  Board.InterpreteDRSequence(drcap);
  
  G_Log.out() << "    result = " << ConfReg[name].GetStateSeq() << endl;

  return ScanVectorToVal(ConfReg[name].GetStateSeq());
}

unsigned char TGolJtag::ReadReg(std::string name)
{
  TScanVector irseq, drcap, ircap;

  Contr.ProgramStart();
  Contr.TAPtoReset();
  GOL.SetState("CONF_R");
  irseq = Board.GenerateIRSequence();
  Contr.IRTransmitRec(irseq, ircap);
  Contr.DRLoopRec(Board.GetDRLength(), drcap);
  Contr.ProgramEnd();
  Contr.ProgramExecute();
  Board.InterpreteDRSequence(drcap);

  return ScanVectorToVal(ConfReg[name].GetStateSeq());
}


void TGolJtag::WriteConfig0(unsigned char wait_time, unsigned char loss_of_lock_time)
{
  if ((wait_time > 0x1f) || (loss_of_lock_time > 0x7))
    throw TException("WriteConfig0: invalid parameter");     

  unsigned char value = wait_time | (loss_of_lock_time << 5);

  if (WriteReg("config0", value) != value)
    throw TException("Could not set config 0");
}
   

void TGolJtag::ReadConfig0(unsigned char& wait_time, unsigned char& loss_of_lock_time)
{
  unsigned char value = ReadReg("config0");
  wait_time = value & 0x1f;
  loss_of_lock_time = (value >> 5) & 0x7;
}


void TGolJtag::WriteConfig1(unsigned char pll_lock_time,
                bool en_soft_loss_of_lock,
                bool en_loss_of_lock_count,
                bool en_force_lock,
                bool en_self_test)
{
  if (pll_lock_time > 0xf)
    throw TException("WriteConfig1: invalid parameter");

  unsigned char value = pll_lock_time
                     | (en_soft_loss_of_lock  ? (1 << 4) : 0)
                     | (en_loss_of_lock_count ? (1 << 5) : 0)
                     | (en_force_lock         ? (1 << 6) : 0)
                     | (en_self_test          ? (1 << 7) : 0);


  if (WriteReg("config1", value) != value)
    throw TException("Could not set config 1");
}

void TGolJtag::ReadConfig1(unsigned char& pll_lock_time,
               bool& en_soft_loss_of_lock,
               bool& en_loss_of_lock_count,
               bool& en_force_lock,
               bool& en_self_test)
{
  unsigned char value = ReadReg("config1");
  pll_lock_time = value & 0xf;
  en_soft_loss_of_lock  = (value & (1 << 4)) != 0;
  en_loss_of_lock_count = (value & (1 << 5)) != 0;
  en_force_lock         = (value & (1 << 6)) != 0;
  en_self_test          = (value & (1 << 7)) != 0;
}


void TGolJtag::WriteConfig2(unsigned char pll_current, bool en_flag)
{
  if ((pll_current > 0x1f))
    throw TException("WriteConfig2: invalid parameter");

  unsigned char value = pll_current
                     | (en_flag ? (1 << 7) : 0);

  if ((WriteReg("config2", value) & 0x9F) != value)
    throw TException("Could not set config 2");
}

void TGolJtag::ReadConfig2(unsigned char& pll_current, bool& en_flag)
{
  unsigned char value = ReadReg("config2");
  pll_current = value & 0x1f;
  en_flag = (value & (1 << 7)) != 0;
}

void TGolJtag::WriteConfig3(unsigned char ld_current, bool use_conf_regs)
{
  if ((ld_current > 0x3f))
    throw TException("WriteConfig3: invalid parameter");

  unsigned char value = ld_current
                     | (use_conf_regs ? (1 << 7) : 0);

  if (WriteReg("config3", value) != value)
    throw TException("Could not set config 3");
}

void TGolJtag::ReadConfig3(unsigned char& ld_current, bool& use_conf_regs)
{
  unsigned char value = ReadReg("config3");
  ld_current = value & 0x3f;
  use_conf_regs = (value & (1 << 7)) != 0;
}

void TGolJtag::ReadStatus0(unsigned char& loss_of_lock_count)
{
  loss_of_lock_count = ReadReg("status0");
}

void TGolJtag::ReadStatus1(bool& conf_wmode16,
               bool& conf_glink,
               TLinkControlState& link_control_state_C,
               TLinkControlState& link_control_state_B,
               TLinkControlState& link_control_state_A)
{
  unsigned char value = ReadReg("status1");
  conf_wmode16 = (value & (1 << 0)) != 0;
  conf_glink   = (value & (1 << 1)) != 0;
  link_control_state_C = (value >> 2) & 0x3;
  link_control_state_B = (value >> 4) & 0x3;
  link_control_state_A = (value >> 6) & 0x3;
}

}
