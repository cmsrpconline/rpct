#include "rpct/devices/XmlSystemBuilder.h"
#include "rpct/lboxaccess/FecManagerImpl.h"
#include "rpct/lboxaccess/test/FecManagerMock.h"
#include "rpct/devices/TriggerCrate.h"
#include "rpct/std/IllegalArgumentException.h"

#include "xoap/domutils.h" 
#include "xdaq/exception/Exception.h" 

#include "rpct/devices/LinkBoard.h"
#include "rpct/devices/cb.h" 
//#include "rpct/devices/tb2.h"
#include "rpct/devices/TriggerBoard.h"    
#include "rpct/devices/TTUBoard.h"    
#include "rpct/devices/tcsort.h" 
#include "rpct/devices/hsb.h"  
#include "rpct/devices/fsb.h"  
#include "rpct/devices/Dcc.h"  
#include "rpct/devices/Ccs.h"

#include <xercesc/parsers/SAXParser.hpp>  
#include <sstream>

using namespace xoap;
using namespace std;
using namespace log4cplus;

namespace rpct {

Logger XmlSystemBuilder::logger = Logger::getInstance("XmlSystemBuilder");
void XmlSystemBuilder::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}

XmlSystemBuilder::XmlSystemBuilder(LinkSystem& s, bool m) :
    system(s), mock(m) {
}

void XmlSystemBuilder::build() {
    build(string("system.xml"));
}

void XmlSystemBuilder::build(string filename) {
    this->filename = filename;
    resetErrors();
    SAXParser parser;
    parser.setDocumentHandler(this);
    parser.setErrorHandler(this);
    parser.parse(filename.c_str());
    if (sawErrors) {
        throw TException("Problems found during building system from file " + filename);
    }
}

string XmlSystemBuilder::getAttribute(const char* attrName,
        xercesc::AttributeList& attributes) {
    return XMLCh2String(attributes.getValue(attrName));
}

bool XmlSystemBuilder::getHexAttribute(const char* attrName,
        xercesc::AttributeList& attributes, unsigned int& value) {
    string attr = XMLCh2String(attributes.getValue(attrName));
    if (!attr.empty()) {
        istringstream istr(attr);
        istr >> hex >> value;
        return true;
    }
    return false;
}

bool XmlSystemBuilder::getDecAttribute(const char* attrName,
        xercesc::AttributeList& attributes, unsigned int& value) {
    string attr = XMLCh2String(attributes.getValue(attrName));
    if (!attr.empty()) {
        istringstream istr(attr);
        istr >> value;
        return true;
    }
    return false;
}

bool XmlSystemBuilder::getDecAttribute(const char* attrName,
        xercesc::AttributeList& attributes, int& value) {
    string attr = XMLCh2String(attributes.getValue(attrName));
    if (!attr.empty()) {
        istringstream istr(attr);
        istr >> value;
        return true;
    }
    return false;
}

void XmlSystemBuilder::startElement(const XMLCh* const name,
        AttributeList& attributes) {
    string sname = XMLCh2String(name);
    unsigned int id = 0;
    getDecAttribute("id", attributes, id);
    LOG4CPLUS_DEBUG(logger, "XmlSystemBuilder: startElement begin " << sname);

    if (sname == "ring") {
        unsigned int pciSlot;
        unsigned int fecAddr;
        unsigned int ringAddr;
        if (!getHexAttribute("pciSlot", attributes, pciSlot)) {
            throw rpct::IllegalArgumentException("pciSlot");
        }
        if (!getHexAttribute("fecAddr", attributes, fecAddr)) {
            throw rpct::IllegalArgumentException("fecAddr");
        }
        if (!getHexAttribute("ringAddr", attributes, ringAddr)) {
            throw rpct::IllegalArgumentException("ringAddr");
        }
        if (mock) {
            curFecManager = new FecManagerMock(pciSlot, fecAddr, ringAddr);
        } else {
            curFecManager = new FecManagerImpl(pciSlot, fecAddr, ringAddr);
        }
        system.addFecManager(*curFecManager);
    } else if (sname == "linkBox") {
        cout << "linkBox - started" << endl; 
        unsigned int ccuAddr;
        unsigned int ccuAddr2;
        bool ccu10 = getHexAttribute("ccuAddr", attributes, ccuAddr);
        bool ccu20 = getHexAttribute("ccuAddr2", attributes, ccuAddr2);
        LinkBox* lbox = new LinkBox(id, getAttribute("desc", attributes).c_str(),
        		&system,
                ccu10 ? &curFecManager->addLinkBoxAccess(ccuAddr) : 0,
                ccu20 ? &curFecManager->addLinkBoxAccess(ccuAddr2) : 0,
                id + 1, id + 2, mock);
        cout << "asdf 2" << endl;
        system.addLinkBox(*lbox);
        cout << "linkBox - added to system" << endl; 
        curCrate = lbox;
        cout << "linkBox - ok" << endl; 
    } else if (sname == "linkBoard") {
        LinkBox* linkBox = dynamic_cast<LinkBox*>(curCrate);
        if (linkBox == 0) {
            throw TException("Problems found during building system from file " + filename
                    + ". Unexpected 'linkBoard' element.");
        }
        unsigned int pos;
        getDecAttribute("pos", attributes, pos);
        LinkBoard& stdlb = linkBox->addLinkBoard(pos, id, getAttribute("desc", attributes).c_str(), id + 1);
        system.registerItem(stdlb);
        system.registerItem(stdlb.getSynCoder());
    } else if (sname == "crate") {
        if (getAttribute("type", attributes) == "vme") {
            VmeCrate* vme = new VmeCrate(id, getAttribute("desc", attributes).c_str());
            system.addVmeCrate(*vme);
            curCrate = vme;
        } else {
            throw TException("Unknown crate type");
        }
    } else if (sname == "triggerCrate") {
        if (getAttribute("type", attributes) == "vme") {

            unsigned int logSector;
            getDecAttribute("logSector", attributes, logSector);

            VmeCrate* vme = new TriggerCrate(id, getAttribute("desc", attributes).c_str(),
                    logSector);
            system.addVmeCrate(*vme);
            curCrate = vme;
        } else {
            throw TException("No vme attribute");
        }
    } else if (sname == "vme") {
        VmeCrate* vme = dynamic_cast<VmeCrate*>(curCrate);
        if (vme == 0) {
            throw TException("Problems found during building system from file " + filename
                    + ". Unexpected 'vme' element.");
        }
        if (mock) {
            vme->setVme(new TVMESimul());
        } else {
            string vmeInterface = getAttribute("interface", attributes);
            string vmeUnit = getAttribute("unit", attributes);

            if (vmeUnit.empty()) {
                vme->setVme(TVMEFactory().Create(vmeInterface));
            } else {
                vme->setVme(TVMEFactory().Create(vmeInterface, vmeUnit));
            }
        }
    } else if (sname == "board") {
        VmeCrate* vme = dynamic_cast<VmeCrate*>(curCrate);
        if (vme == 0) {
            throw TException("Problems found during building system from file " + filename
                    + ". Unexpected 'board' element.");
        }
        string type = getAttribute("type", attributes);
        unsigned int addr;
        getHexAttribute("addr", attributes, addr);
        string desc = getAttribute("desc", attributes);
        int pos = -1;
        getDecAttribute("pos", attributes, pos);

        /*if (type == "tb2") {            
         TTb2* tb2 = new TTb2(id, desc.c_str(), vme->getVme(), addr, vme, pos);
         vme->addBoard(*tb2);     
         curItem = tb2;
         system.registerItem(*tb2);
         }
         else*/if (type == "tb3") {
            TriggerBoard* tb3 = new TriggerBoard(id, desc.c_str(), vme->getVme(), addr, vme, pos);
            vme->addBoard(*tb3);
            curItem = tb3;
            system.registerItem(*tb3);
            system.registerItem(tb3->GetControl());
        } else if (type == "tcsort") {
            TTCSort* tcsort = new TTCSort(id, desc.c_str(), vme->getVme(), addr, vme, pos, id + 98);
            vme->addBoard(*tcsort);
            curItem = tcsort;
            system.registerItem(*tcsort);
            system.registerItem(tcsort->GetTCSortTCSort());
        } else if (type == "hsb") {
            THsb* hsb = new THsb(id, desc.c_str(), vme->getVme(), addr, vme, pos, id + 98);
            vme->addBoard(*hsb);
            curItem = hsb;
            system.registerItem(*hsb);
            system.registerItem(hsb->GetHsbSortHalf());
        } else if (type == "fsb") {
            TFsb* fsb = new TFsb(id, desc.c_str(), vme->getVme(), addr, vme, pos, id + 98);
            vme->addBoard(*fsb);
            curItem = fsb;
            system.registerItem(*fsb);
            system.registerItem(fsb->GetFsbSortFinal());
        } else if (type == "dcc") {
            Dcc* dcc = new Dcc(id, desc.c_str(), vme->getVme(), addr, vme, pos);
            vme->addBoard(*dcc);
            curItem = dcc;
            system.registerItem(*dcc);
            system.registerItem(dcc->getDccDevice());
        } else if (type == "ccs") {
            Ccs* ccs = new Ccs(id, desc.c_str(), vme->getVme(), addr, vme, pos);
            vme->addBoard(*ccs);
            curItem = ccs;
            system.registerItem(*ccs);
            system.registerItem(ccs->getTriggerFPGA());
        } else if (type == "ttu") {
            TTUBoard* ttu = new TTUBoard(id, desc.c_str(), vme->getVme(), addr, vme, pos);
            vme->addBoard(*ttu);
            curItem = ttu;
            system.registerItem(*ttu);
            system.registerItem(ttu->GetControl());
        }
    }
    /*else if (sname == "mezz") {
     TUni* uni = dynamic_cast<TUni*>(CurrItem);
     if (uni != NULL) {
     istringstream istr(XMLCh2String(attributes.getValue("pos")));
     int pos;
     istr >> hex >> pos;
     uni->AddMezz(XMLCh2String(attributes.getValue("type")).c_str(), pos, id, XMLCh2String(attributes.getValue("desc")).c_str());
     }
     }  */
    else if (sname == "chip") {
        IBoard* board = dynamic_cast<IBoard*>(curItem);
        if (board != 0) {
            unsigned int pos;
            getHexAttribute("pos", attributes, pos);
            IDevice& chip = board->addChip(getAttribute("type", attributes),
                    pos, id);
            system.registerItem(chip);
        }
    }
    /*else if (sname == "link") {
     TLink* link = new TLink();
     link->startElement(name, attributes, *System);
     System->RegisterItem(*link);
     }
     else if (CurrCrate != NULL) {
     // we are inside 'crate' tag
     IHardwareBuilder* builder = dynamic_cast<IHardwareBuilder*>(CurrCrate);
     if (builder != NULL)
     builder->startElement(name, attributes, *System);
     else
     throw TException("Crate does not support IHardwareBuilder interface");
     } */
    LOG4CPLUS_DEBUG(logger, "XmlSystemBuilder: startElement end " << sname);
}

void XmlSystemBuilder::endElement(const XMLCh* const name) {
    string sname = XMLCh2String(name);
    if (sname == "ring") {
        curFecManager = 0;
    } else if (sname == "linkBox") {
        //curLinkBox = 0;
        curCrate = 0;
    } else if (sname == "crate") {
        //curVmeCrate = 0;
        curCrate = 0;
    } else if (sname == "board") {
        curItem = 0;
    }
}

void XmlSystemBuilder::error(const xercesc::SAXParseException& e) {
    sawErrors = true;
    LOG4CPLUS_ERROR(logger, "Error at file " << XMLCh2String(e.getSystemId())
            << ", line " << e.getLineNumber()
            << ", char " << e.getColumnNumber()
            << "\n  Message: " << XMLCh2String(e.getMessage()));
}

void XmlSystemBuilder::fatalError(const xercesc::SAXParseException& e) {
    sawErrors = true;
    LOG4CPLUS_ERROR(logger, "Fatal Error at file " << XMLCh2String(e.getSystemId())
            << ", line " << e.getLineNumber()
            << ", char " << e.getColumnNumber()
            << "\n  Message: " << XMLCh2String(e.getMessage()));
}

void XmlSystemBuilder::warning(const xercesc::SAXParseException& e) {
    LOG4CPLUS_WARN(logger, "Warning at file " << XMLCh2String(e.getSystemId())
            << ", line " << e.getLineNumber()
            << ", char " << e.getColumnNumber()
            << "\n  Message: " << XMLCh2String(e.getMessage()));
}

void XmlSystemBuilder::resetErrors() {
    sawErrors = false;
}

}
