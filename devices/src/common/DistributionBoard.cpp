//---------------------------------------------------------------------------
//
//  Michal Pietrusinski 2001
//  Warsaw University
//
#include "rpct/std/NullPointerException.h"
#include "rpct/devices/DistributionBoard.h"

//---------------------------------------------------------------------------



namespace rpct {


//int Feb::Delay = 100;


DistributionBoard::DistributionBoard(TI2CInterface* i)
 : i2c(i), febAccessEnabled(false) {
}

DistributionBoard::~DistributionBoard() {
}

void DistributionBoard::sleep() {
    //tbsleep(Delay);
}

TI2CInterface* DistributionBoard::getI2C() {
    return i2c;
}

void DistributionBoard::setI2C(TI2CInterface* i2c) {
    this->i2c = i2c;
}

void DistributionBoard::checkI2C() {
    if (i2c == NULL) {
        throw NullPointerException("DistributionBoard::I2C == NULL");
    }
}

void DistributionBoard::enableFebAccess(bool value) {    // 1)
    checkI2C();
    i2c->Write8(0x70, value ? 0x4 : 0x0);
    febAccessEnabled = value;
}



} // namespace
