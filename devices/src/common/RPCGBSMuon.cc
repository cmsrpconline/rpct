//---------------------------------------------------------------------------
//#include "L1Trigger/RPCTrigger/interface/RPCGBSMuon.h"
#include "rpct/devices/cmssw/RPCGBSMuon.h"
//#include "L1Trigger/RPCTrigger/interface/RPCException.h"
#ifndef _STAND_ALONE
#include "FWCore/MessageLogger/interface/MessageLogger.h"
#endif // _STAND_ALONE
//#include "L1Trigger/RPCTrigger/interface/RPCException.h"

#include <sstream>
#include <iomanip>
#include <iostream>

using namespace std;
//---------------------------------------------------------------------------
RPCGBSMuon::RPCGBSMuon(): RPCMuon() {
    m_Killed = false;

    m_GBData = 0;

    m_EtaAddress = 0;
    m_PhiAddress = 0;
}
//---------------------------------------------------------------------------
RPCGBSMuon::RPCGBSMuon(int ptCode, int quality, int sign,
                         int patternNum, unsigned short firedPlanes):
    RPCMuon(ptCode, quality, sign, patternNum, firedPlanes) 
{
    m_Killed = false;

    m_GBData = 0;

    m_EtaAddress = 0;
    m_PhiAddress = 0;
}

/**
*
* \brief Gives debuging info in human readable format (1) or technicall format (2)
* \note Possible debugFormat codes (1,2) are defined in RPCTrigger constructor
*
*/
std::string RPCGBSMuon::printDebugInfo(int debugFormat) const {

  std::ostringstream sDebugInfo;
  if (debugFormat==1){  // Human readable

    sDebugInfo << "TBMuon: code: " << getPtCode()
               << " etaAddr: " << getEtaAddr()
               << " phiAddr: " << getPhiAddr()
               << " sgAddr: " << getSegmentAddr()
               << " scAddr: " << getSectorAddr()
               << " gbData: " << getGBData();

  }
  else {        //technicall
   sDebugInfo << "TBMuon pt "<< getPtCode() 
              <<   " ql " <<getQuality() 
              <<   " sgn " << getSign()
              <<   " tw " << getTower()
              <<   " sc " << getLogSector()
              <<   " sg " << getLogSegment();
  }

  return sDebugInfo.str();

}
//---------------------------------------------------------------------------
// Simple setters and getters

///Combined quality and ptCode, 8 bits [7...5 m_Quality, 4...0 m_PtCode], used in GhoustBusters
int RPCGBSMuon::getCode() const {  return (m_Quality<<5 | m_PtCode); }

///Sets combined code: 8 bits [7...5 m_Quality, 4...0 m_PtCode].
void RPCGBSMuon::setCode(int code) {
    m_Quality = (code & (3<<5))>>5;
    m_PtCode = code & 31;
}


void RPCGBSMuon::setPhiAddr(int phiAddr) { m_PhiAddress = phiAddr;}

void RPCGBSMuon::setSectorAddr(int sectorAddr){ m_PhiAddress = m_PhiAddress | sectorAddr<<4;}

void RPCGBSMuon::setEtaAddr(int etaAddr) { m_EtaAddress = etaAddr;}
  
void RPCGBSMuon::setAddress(int etaAddr, int phiAddr) { 
     m_EtaAddress = etaAddr;
     m_PhiAddress = phiAddr;
}

void RPCGBSMuon::setAddress(int tbNumber, int tbTower, int phiAddr) {
    m_EtaAddress = (tbNumber<<2) | tbTower;
    m_PhiAddress = phiAddr;
}

void RPCGBSMuon::setGBData(unsigned int gbData) {
	m_GBData = gbData;
}

int RPCGBSMuon::getEtaAddr() const { return m_EtaAddress; }

int RPCGBSMuon::getPhiAddr() const { return m_PhiAddress; }

int RPCGBSMuon::getSegmentAddr() const { return m_PhiAddress & 15; }

int RPCGBSMuon::getSectorAddr() const { return (m_PhiAddress & 0xF0)>>4; }

int RPCGBSMuon::getContinSegmAddr() const { return getSectorAddr()*12 + getSegmentAddr();}

void RPCGBSMuon::setCodeAndPhiAddr(int code, int phiAddr) {
    setCode(code);
    m_PhiAddress = phiAddr;
}

void RPCGBSMuon::setCodeAndEtaAddr(int code, int etaAddr) {
    setCode(code);
    m_EtaAddress = etaAddr;
}
  
int RPCGBSMuon::getGBData() const { return m_GBData;}

std::string RPCGBSMuon::getGBDataBitStr() const {
    std::string str = "00";
    if (m_GBData == 1)
      str = "01";
    else if (m_GBData == 2)
      str = "10";
    else if (m_GBData == 3)
      str = "11";
    return str;  
}

void RPCGBSMuon::setGBDataKilledFirst() { m_GBData = m_GBData | 1;}

void RPCGBSMuon::setGBDataKilledLast() { m_GBData = m_GBData | 2; }

bool RPCGBSMuon::gBDataKilledFirst() const { return (m_GBData & 1);}

bool RPCGBSMuon::gBDataKilledLast() const { return (m_GBData & 2);}


//---------------------------------------------------------------------------
void RPCGBSMuon::kill() { m_Killed = true; }

/** @return true = was non-empty muon and was killed
  * false = was not killed or is zero */
bool RPCGBSMuon::wasKilled() const {
    if(m_PtCode > 0 && m_Killed)
      return true;
    else return false;
}

/** @return true = was no-zero muon and was not killed
  * false = is killed or is zero */
bool RPCGBSMuon::isLive() const {
    if(m_PtCode > 0 && !m_Killed)
      return true;
    else return false;
}

//---------------------------------------------------------------------------
#ifndef _STAND_ALONE
RPCGBSMuon::RPCGBSMuon(const RPCPacMuon& pacMuon):
    RPCMuon(pacMuon) 
{
    m_Killed = false;

    m_GBData = 0;

    m_EtaAddress = 0;
    m_PhiAddress = 0;
}
#endif //_STAND_ALONE

std::string RPCGBSMuon::toString(int format) const {
  ostringstream ostr;
  if(format == 0) {
  	 ostr<<"sig "<<m_Sign<<", qu "<<m_Quality<<", pt "<<setw(2)<<m_PtCode
         <<", phi "<<setw(3)<<m_PhiAddress<<", eta "<<setw(2)<<m_EtaAddress;
  }
  else if( format == 1) {
  	ostr<<" "<<m_Sign<<" "<<m_Quality<<" "<<setw(2)<<m_PtCode
        <<" "<<setw(3)<<m_PhiAddress<<" "<<setw(2)<<m_EtaAddress;
  }    
  else if( format == 2) {
  	ostr<<" "<<m_Quality<<" "<<setw(2)<<m_PtCode<<" "<<m_Sign;
  }    
  
  return ostr.str();
}
//---------------------------------------------------------------------------
