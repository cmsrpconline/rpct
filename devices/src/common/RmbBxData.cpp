#include "rpct/devices/RmbBxData.h"
#include "rpct/devices/cmssw/RPCConst.h"
#include "rpct/std/bitcpy.h"
#include "rpct/std/tbutil.h"

#include <iomanip>
#include<sstream>

using namespace std;

namespace rpct {

RmbBxDataFactory RmbBxDataFactory::instance_;

RmbBxData::RmbBxData(IDiagnosticReadout::BxData& bxData)
        : StandardBxData(bxData) {
    unsigned int pos = 0;
    unsigned int rmbOutBitsCnt = 16;
    unsigned short rmbOutData;

    unsigned int trigBit = 0;
    unsigned int bcn0Bit = 0;

    unsigned int bcnBitsCnt = 4;
    unsigned int bcnData = 0;

    unsigned int channelValidBitsCnt = 18;

    unsigned int linkBitsCnt = 24;
    unsigned int linkData = 0;

    ostringstream ostr;

    bitcpy(&rmbOutData, 0, bxData_.getData(), pos, rmbOutBitsCnt);  pos += rmbOutBitsCnt;


    unsigned int youngerByte = (rmbOutData & 0xff00) >> 8;
    unsigned int olderByte = rmbOutData & 0xff;

    //ostr<<setw(4)<<olderByte<<youngerByte;
    unsigned int dt = (olderByte<<8) + youngerByte;

    ostr<<" "<<setw(4)<<hex<<dt;

    if((dt & 0xe000) == 0xe000) {
        ostr<<" SOF evn "<<setw(2)<<((dt/8) & 0x3ff)<<" bxn "<<(dt & 7);
    }
    else if((dt & 0xdfe0) == 0xdfe0) {
        ostr<<" SLD link "<<setw(2)<<(dt & 0x1f);
    }
    else if(dt  == 0xc969) {
        ostr<<" EOF ";
    }
    else if(dt == 0xdf9f) {
        ostr<<" DDM - whole event";
    }
    else if(dt == 0xdf9e) {
        ostr<<" DDM - catasthropy";
    }
    else if((dt & 0xffe0) == 0xdf80) {
        ostr<<" DDM link "<<setw(2)<<(dt & 0x1f);
    }
    else if((olderByte & 0xc0) != 0xc0) { //link data
    	unsigned int lbPartByte = (olderByte>>2);
        //ostr<<"  "<<setw(2)<<(olderByte>>2)<<" "<<youngerByte;
    	ostr<<"  "<<(lbPartByte >> 4)<<" "<<(lbPartByte & 0xf)<<" "<<youngerByte;
    }
	dataStr_ = ostr.str(); 
	
    bitcpy(&trigBit,    0, bxData_.getData(), pos, 1);              pos += 1;
    bitcpy(&bcn0Bit,    0, bxData_.getData(), pos, 1);              pos += 1;
    bitcpy(&bcnData,    0, bxData_.getData(), pos, bcnBitsCnt);     pos += bcnBitsCnt;
    bitcpy(&channelsValid_,    0, bxData_.getData(), pos, channelValidBitsCnt);       pos += channelValidBitsCnt;

    for(int iO = 0, index = 0; iO < 6; iO++) {
        for(int iL = 0; iL < 3; iL++, index++) {
            bitcpy(&linkData, 0, bxData_.getData(), pos, linkBitsCnt);                pos += linkBitsCnt;
            dataStr_ = dataToHex(&linkData, linkBitsCnt) + " " + dataStr_;            
            if(linkData != 0) {
            	LmuxBxData* lmuxBxData = new LmuxBxData(linkData, index);            
            	lmuxBxDataVector_.push_back(lmuxBxData);
            }
        }
        dataStr_ = " | " + dataStr_ ;
    }
    	
    ostringstream ostr1;
    ostr1<<hex<<setw(4)<<bcnData<<" "<<bcn0Bit<<" "<<trigBit<<" "<<setw(5)<<channelsValid_<<" ";
	dataStr_ = ostr1.str() + " | " + dataStr_; 
}



}
