#include "rpct/devices/DccDevice.h"
#include "rpct/devices/DccSettingsImpl.h"
#include "rpct/devices/DccMonitorables.h"
#include "rpct/devices/System.h"
#include "rpct/std/IllegalArgumentException.h"
#include "rpct/std/NullPointerException.h"
#include "rpct/ii/ConfigurationFlags.h"

using namespace std;
using namespace log4cplus;
using namespace rpct;


Logger DccDevice::logger = Logger::getInstance("DccDevice");
void DccDevice::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}
const HardwareItemType DccDevice::TYPE(HardwareItemType::cDevice, "DccDevice");

DccDevice::DccDevice(int vme_board_address,
        IBoard& board, TVMEInterface* vme)
: TDummyVMEDevice(board.getId(), "DccDevice", "DccDevice",//deviceId=boardId
        TYPE, "1.1", 0, vme_board_address, board, 0, vme),
        settings_(0)
{
    vmeAddress_=BoardAddress+BaseAddress;//niepotrzebne bo BaseAddress=0
    ebAddress_=0xa00000;
    eventBuilder = new DccChip("EB",vme_,vmeAddress_,ebAddress_,4);
    emAddress_=0xb00000;
    eventMerger = new DccChip("EM",vme_,vmeAddress_,emAddress_,2);
    for(int i=0; i<9; i++){
//        ihAddress_[i]=0x100000*(i+1);
//        char name[3]="IH";
//        sprintf(name+2, "%i", i);
//        inputHandlerVec.push_back(  new DccChip(name,vme_,vmeAddress_,ihAddress_[i],2) );
//          inputHandlerVec.push_back( new DccInputHandler(i, name, vme_,vmeAddress_,ihAddress_[i]) );
        inputHandlerVec.push_back( new DccInputHandler(i, vme_,vmeAddress_) );
    }
}

DccDevice::~DccDevice(){
    delete eventBuilder;
    delete eventMerger;
    for(int i=0; i<9; i++){ delete inputHandlerVec[i]; }
    if(settings_ != 0) delete settings_;
}

void DccDevice::configure(DeviceSettings* settings, int flags){
    //Check settings
    if (settings == 0) {
        /*if (flags & ConfigurationFlags::RESET_ONLY_WHEN_NO_CONFIG_DATA) {
            reset();
            return;
        }*/
        throw NullPointerException("DccDevice::configure: settings is null");
    }
    DccSettingsImpl* s = dynamic_cast<DccSettingsImpl*>(settings);
    if (s == 0) {
        throw IllegalArgumentException("DccDevice::configure: invalid cast for settings");
    }
    settings_ = new DccSettingsImpl(*s);

    //Set BX range
    setBXRange(settings_->getPreTriggerVal(),settings_->getPostTriggerVal());
    //std::cout<<"BXRange ["<<s->getPreTriggerVal()<<","<<s->getPostTriggerVal()<<"]"<<std::endl;
    //Set resync command and mask
    eventBuilder->write(EB_RESYNC_MASK_CMD,settings_->getResyncCmd()+0x100*(settings_->getResyncMask()));
    //Enable spy (moved to run() method)
    //Ignore SLink status (LFF) (moved to run() method)

    // set DCC fedId
    unsigned int fedId = settings_->getFedId();
    for (unsigned int ih=0; ih<9; ++ih) inputHandlerVec[ih]->write(IHA_SRC_ID,fedId);
    for (unsigned int ih=0; ih<9; ++ih) inputHandlerVec[ih]->write(IHA_SYNC_ADJUST,6);

    if(settings_->getEmulMode()){
        LOG4CPLUS_INFO(logger, "WARNING!!! Emul mode is on");
        eventBuilder->setBits(EB_CTRL_REG,vector<unsigned int>(1,EBB_CTRL_USE_EMUL));
        //Configure IHs
        int iId=0;
        vector<unsigned int> ihEmulBits(2);
        ihEmulBits[0]=IHB_CFG_EMUL; ihEmulBits[1]=IHB_CFG_TEST_RATE;
        for(int i=0; i<70; ++i){
            if((i%8-2<4)&&(i%8-2>=0)){
                int ihNum = i/8;
                int ihInNum = i%8-2;
                //Set IDs for all channels
                inputHandlerVec[ihNum]->write((IHA_RMB_ID+ihInNum),iId);
                //Unmask all channels
                inputHandlerVec[ihNum]->clearBits(IHA_INPUT_MASK,vector<unsigned int>(1,ihInNum));
                //Switch on emulation on all IHs
                //Switch on emulation on all IHs
                inputHandlerVec[ihNum]->setBits(IHA_CONFIG,ihEmulBits);
                //std::cout<<"IH = "<<ihNum<<" ihInNum = "<<ihInNum<<" RMBId = "<<inputiId<<std::endl;
                iId++;
            }
        }
    }
    else{
        //Configure IHs
        unsigned int * inputId = settings_->getInputIds(); //int array[70]
        for(int i=0; i<70; ++i){
            if(inputId[i]!=99){
                if((i%8-2<4)&&(i%8-2>=0)){
                    int ihNum = i/8;
                    int ihInNum = 3-(i%8-2);//inverted inputs numbering
                    //Set IDs for connected channels
                    inputHandlerVec[ihNum]->write((IHA_RMB_ID+ihInNum),inputId[i]);
                    //Unmask connected channels
                    inputHandlerVec[ihNum]->clearBits(IHA_INPUT_MASK,vector<unsigned int>(1,ihInNum));
                    std::cout<<"IH = "<<ihNum<<" ihInNum = "<<ihInNum<<" RMBId = "<<inputId[i]<<std::endl;
                }
                else{
                    LOG4CPLUS_INFO(logger, "DCC: invalid input number " << i <<"IH/IH_in"<<i/8<<"/"<<i%8-2);
                }
            }
        }
    }
  resetErrorCounters();
}


void DccDevice::resetErrorCounters()
{
   eventBuilder->setBits(EB_CTRL_REG,vector<unsigned int>(1,EBB_CTRL_OOS_LATCH_CLEAR));
  for(std::vector<DccInputHandler*>::iterator ih = inputHandlerVec.begin(); ih < inputHandlerVec.end(); ++ih)
       (**ih).write(IHA_RESET_CNT_RX,0);

}

void DccDevice::reset()
{
  LOG4CPLUS_INFO(logger,"DccDevice, reset called for FED: " << inputHandlerVec[0]->read(IHA_SRC_ID));

  //Stop DCC, wait 100ms
  eventBuilder->write(EB_CTRL_REG,0);
  tbsleep((unsigned int)100);
  bool ttcReady1 = bitset<32>(eventBuilder->read(EB_STATUS))[EBB_STATUS_TTC_READY];

  //Reset board (set hw reset), wait 1sek
  vme_->Write32(vmeAddress_+0xffffd0,1);
  tbsleep((unsigned int)1000);
  bool ttcReady2 = bitset<32>(eventBuilder->read(EB_STATUS))[EBB_STATUS_TTC_READY];

  //unset hw reset, wait 1 sek
  vme_->Write32(vmeAddress_+0xffffd0,0);
  tbsleep((unsigned int)1000);
  bool ttcReady3 = bitset<32>(eventBuilder->read(EB_STATUS))[EBB_STATUS_TTC_READY];

  //Second "soft" reset, MK add
  for (int i=1; i<=3; i++) {
    eventBuilder->write(0x0,0x12345678);
    tbsleep((unsigned int)500);
  }
  bool ttcReady4 = bitset<32>(eventBuilder->read(EB_STATUS))[EBB_STATUS_TTC_READY];
  LOG4CPLUS_INFO(logger,"DccDevice, TTC status during reset procedure: " << ttcReady1<<ttcReady2<<ttcReady3<<ttcReady4);

  resetErrorCounters();
}

void DccDevice::initialize()
{
  for(int i=0; i<9; ++i){
    //write DAMAGE mask in IH
    inputHandlerVec[i]->write(IHA_DAMAGE,0x0);
    //clear CONFIG registers
    inputHandlerVec[i]->write(IHA_CONFIG,0);

    vector<unsigned int> ihReqs;
    //reset requests
    inputHandlerVec[i]->write(IHA_REQUESTS,0);
    if(i%3==0){// 1st IH on bus
      ihReqs.push_back(IHB_REQ_SOD);
    }
    if(i%3==2){// last IH on bus
      ihReqs.push_back(IHB_REQ_EOD);
    }
    if(i==0){// 1st IH
      ihReqs.push_back(IHB_REQ_SOE);
      ihReqs.push_back(IHB_REQ_SBX);
    }
    if(i==8){// last IH
      ihReqs.push_back(IHB_REQ_EOE);
    }
    //program requests
    inputHandlerVec[i]->setBits(IHA_REQUESTS,ihReqs);
    //mask all inputs
    inputHandlerVec[i]->write(IHA_INPUT_MASK,0xff);
    //set timeouts
    inputHandlerVec[i]->write(IHA_TIMEOUT_SOD1,0xff);
    inputHandlerVec[i]->write(IHA_TIMEOUT_SOD,0xff);
    inputHandlerVec[i]->write(IHA_TIMEOUT_DATA,0x200);
  }

  //clear TTS register
  eventBuilder->write(EB_TTS_REG,0);

  //swap TTS bits
  eventBuilder->write(EB_TTS_REG,0x200);//obsolete, not needeed and have no impact

  resetErrorCounters();
}

void DccDevice::run(){
    //some settings which could be reseted
    if(settings_ != 0){
        //Enable spy
        if(settings_->getEnaSpy()){
            LOG4CPLUS_INFO(logger, "Spy mem is enabled");
            enableSpyMem();
        }
        //Ignore SLink status (LFF)
        if(settings_->getIgnoreLFF()){
            LOG4CPLUS_INFO(logger, "WARNING!!! LFF status is ignored");
            eventBuilder->setBits(EB_CTRL_REG,vector<unsigned int>(1,EBB_CTRL_IGN_LFFn));
        }
        if(settings_->getEmulMode()){
            LOG4CPLUS_INFO(logger, "WARNING!!! Emul mode is on");
            eventBuilder->setBits(EB_CTRL_REG,vector<unsigned int>(1,EBB_CTRL_USE_EMUL));
        }
    }

    //Switch on the trigger queque in the 1st IH
    inputHandlerVec[0]->setBits(IHA_CONFIG,vector<unsigned int>(1,IHB_CFG_USE_TRIG_QUEUE));
    LOG4CPLUS_INFO(logger, "Trigger queque in the 1st IH is on");

    //Run DCC
    eventBuilder->setBits(EB_CTRL_REG,vector<unsigned int>(1,EBB_CTRL_DCC_RUN));
    LOG4CPLUS_INFO(logger, "DCC is ready to run");
}

void DccDevice::start(DeviceSettings* settings) {
    reset();
    initialize();
    //Set pre-, post-trigger BXes and unmasks inupts
    configure(settings, ConfigurationFlags::FORCE_CONFIGURE);
    //run
    run();
}

void DccDevice::setBXRange(unsigned int pre, unsigned int post){
    if(pre<0 || pre>3){
        throw TException("Dcc::setBXRange: PreTrigger value out of range");
    }
    if(post<3 || pre>7){
        throw TException("Dcc::setBXRange: PostTrigger value out of range");
    }
    preTrigger_=pre;
    postTrigger_=post;
    eventBuilder->write(EB_PRETR_VAL,preTrigger_);
    eventBuilder->write(EB_POSTTR_VAL,postTrigger_);
}

void DccDevice::forceTTS(unsigned int status=0x8){
    //check if status value is in valid range (4 bits);
    if((status>0xf)||(status<0x0)){
        LOG4CPLUS_INFO(logger, "DCC:" << " invalid TTS status " << status);
        return;
    }
    unsigned int forcedStatus = (eventBuilder->read(EB_TTS_REG)&0x200);
    //switch on force mode
    forcedStatus |= 0x100;
    //force TTS status
    forcedStatus |= (status<<4);
    eventBuilder->write(EB_TTS_REG,forcedStatus);
    //checkTTS();
}

void DccDevice::freeTTS(){
    unsigned int currentStatus = eventBuilder->read(EB_TTS_REG);;
    //switch off force mode
    currentStatus &= 0xff;
    eventBuilder->write(EB_TTS_REG,currentStatus);
    //checkTTS();
}


int DccDevice::checkTTS(){
    unsigned int status = eventBuilder->read(EB_TTS_REG);
    LOG4CPLUS_INFO(logger, "DCC:" << " set TTS: Force " <<((status & 0x100)>>8)
            <<             ", forced state " << bitset<4>((status & 0xf0) >> 4)
            <<             ", DCC status " << bitset<4>(status & 0xf));
    return status;
}

DccMonitorables DccDevice::checkDevice(){
    DccMonitorables monitorables;
    monitorables.setTtsStatus(eventBuilder->read(EB_TTS_REG));
    monitorables.setEbStatus(eventBuilder->read(EB_STATUS));
    monitorables.setEbEvtCnt(eventBuilder->read(EB_EVCNT));
    monitorables.setFedId(inputHandlerVec[0]->read(IHA_SRC_ID));
    monitorables.setEbOosDbg(eventBuilder->read(EB_OOS_DBG_LATCH));
    monitorables.setEbEqlzStatus(eventBuilder->read(EB_EQLZ_STAT));
    for(unsigned int i=0; i<9; ++i) monitorables.addInputHandlerStatus(inputHandlerVec[i]->status());
    lastMonitorables_ = monitorables;
    return monitorables;
}

void DccDevice::checkName(){
    unsigned int iname=0;
    try{
        for(int i=0; i<9; ++i){
            inputHandlerVec[i]->read(IHA_ID);
            iname = i;
        }
        eventBuilder->read(EB_ID);
        iname = 9;
        eventMerger->read(EM_ID);
        iname = 10;
    }
    catch(TException& e) {
        std::ostringstream ostr;
        ostr << "Error while checking device name: " << e.what()
        << ". Device id = " << id << " description = " <<  description;
        if(iname<9)
            ostr << " " << inputHandlerVec[iname]->getChipDesc();
        else if(iname==9)
            ostr << " " << eventBuilder->getChipDesc();
        else if(iname==10)
            ostr << " " << eventMerger->getChipDesc();
        else
            ostr << " unknown DCC chip !?";
        ostr << " board name = " << getBoard().getDescription();
        throw TException(ostr.str().c_str());
    }

    unsigned int name, expName;
    for(int i=0; i<9; ++i){
        name=inputHandlerVec[i]->read(IHA_ID);
        expName=IH_ID_VAL;
        //std::cout << "IH" << i << " name = " << std::hex << name << " expName = " << expName << std::endl;
        if (expName != name) {
            std::ostringstream ostr;
            //ostr << "Bad device version: " << std::hex
            ostr << std::hex
            << "expected name: " << expName << ", read: " << name
            << ". Device id = " << std::dec << id << " description = " <<  description
            << " " << inputHandlerVec[i]->getChipDesc()
            << " board name = " << getBoard().getDescription();
            throw EBadDeviceVersion(ostr.str().c_str(), EBadDeviceVersion::VersionInfo(this, "IHA_ID", rpct::toHexString(expName), rpct::toHexString(name)));
        }
    }
    name=eventBuilder->read(EB_ID);
    expName=EB_ID_VAL;
    //std::cout << "EB   name = " << std::hex << name << " expName = " << expName << std::endl;
    if (expName != name) {
        std::ostringstream ostr;
        //ostr << "Bad device version: " << std::hex
        ostr << std::hex
        << "expected name: " << expName << ", read: " << name
        << ". Device id = " << std::dec << id << " description = " <<  description
        << " " << eventBuilder->getChipDesc()
        << " board name = " << getBoard().getDescription();
        //throw EBadDeviceVersion(ostr.str().c_str());
        throw EBadDeviceVersion(ostr.str().c_str(), EBadDeviceVersion::VersionInfo(this, "EB_ID", rpct::toHexString(expName), rpct::toHexString(name)));
    }
    name=eventMerger->read(EM_ID);
    expName=EM_ID_VAL;
    //std::cout << "EM   name = " << std::hex << name << " expName = " << expName << std::endl;
    if (expName != name) {
        std::ostringstream ostr;
        //ostr << "Bad device version: " << std::hex
        ostr << std::hex
        << "expected name: " << expName << ", read: " << name
        << ". Device id = " << std::dec << id << " description = " <<  description
        << " " << eventMerger->getChipDesc()
        << " board name = " << getBoard().getDescription();
        //throw EBadDeviceVersion(ostr.str().c_str());
        throw EBadDeviceVersion(ostr.str().c_str(), EBadDeviceVersion::VersionInfo(this, "EM_ID", rpct::toHexString(expName), rpct::toHexString(name)));
    }
}

void DccDevice::checkVersion()
{
    unsigned int version, expVer, iver=0;
    try{
        for(int i=0; i<9; ++i){
            version = inputHandlerVec[i]->read(IHA_VERSION);
            iver = i;
        }
        version = eventBuilder->read(EB_VERSION);
        iver = 9;
        version = 256*(eventMerger->read(EM_VERSION2))+(eventMerger->read(EM_VERSION1));
        iver = 10;
    }
    catch(TException& e) {
        std::ostringstream ostr;
        ostr << "Error while checking device version: " << e.what()
        << ". Device id = " << id << " description = " <<  description;
        if(iver<9)
            ostr << " " << inputHandlerVec[iver]->getChipDesc();
        else if(iver==9)
            ostr << " " << eventBuilder->getChipDesc();
        else if(iver==10)
            ostr << " " << eventMerger->getChipDesc();
        else
            ostr << " unknown DCC chip !?";
        ostr << " board name = " << getBoard().getDescription();
        throw TException(ostr.str().c_str());
    }

    for(int i=0; i<9; ++i){
        version = inputHandlerVec[i]->read(IHA_VERSION);
        expVer = IH_VERSION_VAL;
        if (version != expVer) {
            std::ostringstream ostr;
            ostr << std::hex
            << "expected version of chip '" << expVer
            << ", read: " << version
            << ". Device id = " << std::dec << id << " description = " <<  description
            << " " << inputHandlerVec[i]->getChipDesc()
            << " board name = " << getBoard().getDescription();
            //throw TException(ostr.str().c_str());
            //throw EBadDeviceVersion(ostr.str().c_str());
            throw EBadDeviceVersion(ostr.str().c_str(), EBadDeviceVersion::VersionInfo(this, "IHA_VERSION", rpct::toHexString(expVer), rpct::toHexString(version)));
        }
    }
    version = eventBuilder->read(EB_VERSION);
    expVer = EB_VERSION_VAL;
    //std::cout << "EB ver = " << std::hex << version << " expVer = " << expVer << std::endl;
    if (version != expVer) {
        std::ostringstream ostr;
        //ostr << "Bad device version: " << std::hex
        ostr << std::hex
        << "expected version of chip '" << expVer
        << ", read: " << version
        << ". Device id = " << std::dec << id << " description = " <<  description
        << " " << eventBuilder->getChipDesc()
        << " board name = " << getBoard().getDescription();
        //throw TException(ostr.str().c_str());
        //throw EBadDeviceVersion(ostr.str().c_str());
        throw EBadDeviceVersion(ostr.str().c_str(), EBadDeviceVersion::VersionInfo(this, "EB_VERSION", rpct::toHexString(expVer), rpct::toHexString(version)));
    }
    version = 256*(eventMerger->read(EM_VERSION2))+(eventMerger->read(EM_VERSION1));
    expVer = 256*EM_VERSION2_VAL+EM_VERSION1_VAL;
    //std::cout << "EM ver = " << std::hex << version << " expVer = " << expVer << std::endl;
    if (version != expVer) {
        std::ostringstream ostr;
        //ostr << "Bad device version: " << std::hex
        ostr << std::hex
        << "expected version of chip '" << expVer
        << ", read: " << version
        << ". Device id = " << std::dec << id << " description = " <<  description
        << " " << eventBuilder->getChipDesc()
        << " board name = " << getBoard().getDescription();
        //throw TException(ostr.str().c_str());
        //throw EBadDeviceVersion(ostr.str().c_str());
        throw EBadDeviceVersion(ostr.str().c_str(), EBadDeviceVersion::VersionInfo(this, "EM_VERSION", rpct::toHexString(expVer), rpct::toHexString(version)));
    }
    
    LOG4CPLUS_INFO(logger," FIRMWARE VERSION IHA_VER: 0x"<<std::hex<<IH_VERSION_VAL<<", EB_VER: 0x"<<EB_VERSION_VAL<<", EM_VER: 0x"<<256*EM_VERSION2_VAL+EM_VERSION1_VAL);

//    unsigned int addr = vmeAddress_ + 0xffffc0;
//    unsigned int vmeVersion = vme_->Read32(addr);
//    LOG4CPLUS_INFO(logger," FIRMWARE VERSION VME: address 0x"<<std::hex<<addr<<" version 0x"<<vmeVersion);
}

void DccDevice::enableSpyMem()
{
  eventBuilder->setBits(EB_CTRL_REG,std::vector<unsigned int>(1,EBB_CTRL_SPY_WRITE_MODE));
  eventBuilder->setBits(EB_CTRL_REG,std::vector<unsigned int>(1,EBB_CTRL_SPY_CIRCULAR_MODE));
}
