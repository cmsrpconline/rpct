#include "rpct/devices/DccChip.h"

using namespace rpct;
using namespace std;

#include "vme/TVMEInterface.h"

DccChip::DccChip( const char* desc, TVMEInterface* vme, int vme_board_address,
    int chip_address, size_t word_size) 
{
  init(desc,vme,vme_board_address,chip_address,word_size);
}

void DccChip::init ( const char* desc, TVMEInterface* vme, int vme_board_address,
    int chip_address, size_t word_size)
{
  vme_ = vme;
  vmeAddress_ = vme_board_address+chip_address;
  chipAddress_ = chip_address;
  desc_ = desc;
  word_size_ = word_size;
}

//  : vme_(vme), vmeAddress_(vme_board_address+chip_address),
//    chipAddress_(chip_address), desc_(desc), word_size_(word_size) 
//{ }

unsigned int DccChip::read(int addr)
{
  int faddr=vmeAddress_+2*addr;
  if(word_size_ == 2) return vme_->Read16(faddr);
  else if(word_size_ == 4) {
//   std::cout<<std::hex<<"read addr = 0x"<<faddr<<" Value is: 0x"<<vme_->Read32(faddr)<<" addr is: vmeAddress_(0x"<<vmeAddress_<<")+2*addr(0x"<<addr<<")" <<std::endl;
   return vme_->Read32(faddr);
  }
  else throw TException("DccChip::read: Not supported word size");
}

void DccChip::write(int addr, unsigned int val)
    // pythonowy oryginal
    //   def setbits32(self,adr,bits):
    //      val=self.read32(adr)
    //      for i in bits:
    //         val |= (1L << i)
    //      self.write32(adr,val)
{
        //std::cout<<std::hex<<"write addr = 0x"<<addr<<" Val = 0x"<<val<<std::endl;
        addr=vmeAddress_+2*addr;
        if(word_size_ == 2)
            vme_->Write16(addr, (unsigned short)val);
        else if(word_size_ == 4)
            vme_->Write32(addr, val);
        else
            throw TException("DccChip::write: Not supported word size");
}

void DccChip::setBits(int addr, std::vector<unsigned int> bits)
{
        addr=vmeAddress_+2*addr;
        if(word_size_ == 2){
            unsigned int val = vme_->Read16(addr);
            for(unsigned int i=0; i<bits.size(); ++i)
                val |= (1 << (unsigned int)bits[i]);
            vme_->Write16(addr,(unsigned short)val);
        }
        else if(word_size_ == 4){
            unsigned int val = vme_->Read32(addr);
            for(unsigned int i = 0; i<bits.size(); ++i)
                val |= (1 << (unsigned int)bits[i]);
            vme_->Write32(addr,val);
        }
        else
            throw TException("DccChip::write: Not supported word size");
}

void DccChip::clearBits(int addr, std::vector<unsigned int> bits)
// pythonowy oryginal
// def clrbits32(self,adr,bits):
//    val=self.read32(adr)
//    for i in bits:
//       val &= ~(1L << i)
//    self.write32(adr,val)
{
  addr=vmeAddress_+2*addr;
  if(word_size_ == 2){
    unsigned int val = vme_->Read16(addr);
    for(unsigned int i=0 ; i<bits.size(); ++i)
      val &= ~(1 << (unsigned int) bits[i]);
      vme_->Write16(addr,(unsigned short)val);
    }
    else if(word_size_ == 4){
      unsigned int val = vme_->Read32(addr);
      for(unsigned int i=0 ; i<bits.size(); ++i)
        val &= ~(1 << (unsigned int)bits[i]);
      vme_->Write32(addr,val);
    }
    else throw TException("DccChip::write: Not supported word size");
}




