#include "rpct/devices/SorterCrate.h"
#include "rpct/devices/System.h"
#include "rpct/ii/ConfigurationFlags.h"

using namespace std;
using namespace log4cplus;

namespace rpct {

Logger SorterCrate::logger = Logger::getInstance("SorterCrate");
void SorterCrate::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}
const HardwareItemType SorterCrate::TYPE(HardwareItemType::cCrate, "SorterCrate");

SorterCrate::SorterCrate(int ident, const char* desc, std::string const & _label) :
    VmeCrate(ident, desc, _label), fsb_(0), ttuBackplane_(0) {
    LOG4CPLUS_INFO(logger, "Constructing SorterCrate " << getDescription());


/*    Boards& boards = getBoards();
    for (Boards::iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {
        if (dynamic_cast<TFsb*> (*iBoard) != 0) {
            fsb_ = dynamic_cast<TFsb*> (*iBoard);
        }
        else if (dynamic_cast<THsb*> (*iBoard) != 0) {
            hsbs_.push_back(dynamic_cast<THsb*> (*iBoard) );
        }
        else if (dynamic_cast<TTTUBackplane*> (*iBoard) != 0) {
            ttuBackplane_ = dynamic_cast<TTTUBackplane*> (*iBoard);
        }
        else if (dynamic_cast<TTUBoard*> (*iBoard) != 0) {
            ttus_.push_back(dynamic_cast<TTUBoard*> (*iBoard) );
        }
    }

   if(hsbs_.size() == 2 && hsbs_[0]->getDescription() == "HSB_1") {
       THsb* hsb1 = hsbs_[0];
       hsbs_[0] =  hsbs_[1];
       hsbs_[1] =  hsb1;
   }*/

}

TFsb* SorterCrate::getFsb() {
    if (fsb_ == 0) {
        Boards& boards = getBoards();
        for (Boards::iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {
            fsb_ = dynamic_cast<TFsb*> (*iBoard);
            if (fsb_ != 0) {
                break;
            }
        }
    }
    return fsb_;
}

vector<THsb*>& SorterCrate::getHSBs() {
    if(hsbs_.size() == 0) {
        Boards& boards = getBoards();
        for (Boards::iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {
            if (dynamic_cast<THsb*> (*iBoard) != 0) {
                hsbs_.push_back(dynamic_cast<THsb*> (*iBoard) );
            }
        }

        if(hsbs_.size() == 2 && hsbs_[0]->getDescription() == "HSB_1") {
            THsb* hsb1 = hsbs_[0];
            hsbs_[0] =  hsbs_[1];
            hsbs_[1] =  hsb1;
        }
    }
    return hsbs_;
}

TTTUBackplane* SorterCrate::getTTUBackplane() {
    if (ttuBackplane_ == 0) {
        Boards& boards = getBoards();
        for (Boards::iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {
            ttuBackplane_ = dynamic_cast<TTTUBackplane*> (*iBoard);
            if (ttuBackplane_ != 0) {
                break;
            }
        }
    }
    return ttuBackplane_;
}

vector<TTUBoard*>& SorterCrate::getTTUs() {
    if(ttus_.size() == 0) {
        Boards& boards = getBoards();
        for (Boards::iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {
            if (dynamic_cast<TTUBoard*> (*iBoard) != 0) {
                ttus_.push_back(dynamic_cast<TTUBoard*> (*iBoard) );
            }
        }
    }

    return ttus_;
}

void SorterCrate::reset(bool forceReset) {
    getFsb();
    getHSBs();
    getTTUBackplane();
    getTTUs();

    if (forceReset) {
        TTTUBackplane* ttuBackplane = getTTUBackplane();
        if (ttuBackplane == 0) {
            //throw TException("SorterCrate::beforeConfigure(): ttuBackplane not found");
        	 LOG4CPLUS_WARN(logger, "SorterCrate::beforeConfigure(): ttuBackplane not found");
        }
        else
        	ttuBackplane->init();

        if(fsb_ != 0) {
            fsb_->GetFsbSortFinal().InitTTC();
            fsb_->GetFsbSortFinal().ResetQPLL();
        }

        for(unsigned int i = 0; i < hsbs_.size(); i++) {
            if(hsbs_[i] != 0) {
                hsbs_[i]->GetHsbSortHalf().InitTTC();
                hsbs_[i]->GetHsbSortHalf().ResetQPLL();
                hsbs_[i]->GetHsbSortHalf().ResetPLL();
            }
        }

        for(unsigned int i = 0; i < ttus_.size(); i++) {
            if(ttus_[i] != 0) {
                ttus_[i]->GetControl().ResetQPLL();
            }
        }
    }
}

bool SorterCrate::checkWarm(ConfigurationSet* configurationSet) {
	return false;
/*    LOG4CPLUS_INFO(logger, "checkingWarm " << getDescription());
    unsigned int configId = configurationSet->getId();
    if (configId == 0) {
        return false;
    }
    TFsb* fsb = getFsb();
    if (fsb == 0) {
        return false;
    }
    TFsbSortFinal& chip = fsb->GetFsbSortFinal();
    unsigned int readConfigId;
    readConfigId = (chip.readWord(TFsbSortFinal::WORD_USER_REG1, 0) << 16) & 0xffff0000;
    readConfigId |= chip.readWord(TFsbSortFinal::WORD_USER_REG2, 0) & 0xffff;
    LOG4CPLUS_INFO(logger, "checkWarm " << getDescription() << hex << " configId = " << configId << ", readConfigId = "
            << readConfigId << ": " << (configId == readConfigId ? "WARM" : "COLD"));

    bool warm = configId == readConfigId;
    if (!warm) {
        return false;
    }*/

    //return VmeCrate::checkWarm(configurationSet); in principle it had no effect
}

void SorterCrate::rememberConfiguration(ConfigurationSet* configurationSet) {
    unsigned int configId = configurationSet->getId();
    TFsb* fsb = getFsb();
    if (fsb == 0) {
        return;
    }
    TFsbSortFinal& chip = fsb->GetFsbSortFinal();
    chip.writeWord(TFsbSortFinal::WORD_USER_REG1, 0, (configId >> 16) & 0xffff);
    chip.writeWord(TFsbSortFinal::WORD_USER_REG2, 0, configId & 0xffff);
}

} // namespace

