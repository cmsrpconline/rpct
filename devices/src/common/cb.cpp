//---------------------------------------------------------------------------

//
//  Michal Pietrusinski 2001
//  Warsaw University
//

#include "rpct/devices/cb.h"
#include "rpct/lboxaccess/cbic_const.h"
#include "rpct/lboxaccess/cbpc_const.h"
#include "rpct/lboxaccess/lbc_const.h"



using namespace std;
using namespace log4cplus;


namespace rpct {

Logger TCB::logger = Logger::getInstance("TCB");
void TCB::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}

const HardwareItemType TCB::TYPE(HardwareItemType::cBoard, "CONTROLBOARD");

TCB::TCB(int ident, const char* desc, LinkBox* b, int bnum)
    : BoardBase(ident, desc, TYPE, b, bnum), lbox(b), i2c(0)
    , command_i2c_(0), block_i2c_(0), bi2c_client_(0) {
    //monitorItems_.push_back(MonitorItemName::CONFIG_LOST);
    monitorItems_.push_back(MonitorItemName::RELOAD_DETECTED);
    monitorItems_.push_back(MonitorItemName::TTCRX);
}

TCB::~TCB() {
    if (i2c)
        delete i2c;
    if (command_i2c_)
        delete command_i2c_;
    if (block_i2c_)
        delete block_i2c_;
    if (bi2c_client_)
        delete bi2c_client_;
}

/*
void TCB::SetTTCrxI2CAddress(unsigned char addr)
{
  TTCrxI2C.SetAddress(addr);
  LBox->GetLBoxAccess()->MemoryWrite(0xfe01, addr);
  LBox->GetLBoxAccess()->ResetTTC();
}*/

unsigned short TCB::memoryRead(uint32_t address) {
    try {
        return getHalfBox()->getLBoxAccess().memoryRead(address);
    }
    catch (CCUException& e) {
        // try once again
        LOG4CPLUS_WARN(logger, "CCUException during memoryRead from address " << address << " reseting FEC and trying once again");
        getHalfBox()->getLBoxAccess().getFecManager().resetPlxFecForAutoRecovery();
        return getHalfBox()->getLBoxAccess().memoryRead(address);
    }
}

void TCB::memoryWrite(uint32_t address, unsigned short data) {
    try {
    	getHalfBox()->getLBoxAccess().memoryWrite(address, data);
    }
    catch (CCUException& e) {
        // try once again
        LOG4CPLUS_WARN(logger, "CCUException during memoryWrite from address " << address << " reseting FEC and trying once again");
        getHalfBox()->getLBoxAccess().getFecManager().resetPlxFecForAutoRecovery();
        getHalfBox()->getLBoxAccess().memoryWrite(address, data);
    }
}

unsigned short TCB::memoryRead16(uint32_t address) {
    try {
        return getHalfBox()->getLBoxAccess().memoryRead16(address);
    }
    catch (CCUException& e) {
        // try once again
        LOG4CPLUS_WARN(logger, "CCUException during memoryRead16 from address " << address << " reseting FEC and trying once again");
        getHalfBox()->getLBoxAccess().getFecManager().resetPlxFecForAutoRecovery();
        return getHalfBox()->getLBoxAccess().memoryRead16(address);
    }
}

void TCB::memoryWrite16(uint32_t address, unsigned short data) {
    try {
    	getHalfBox()->getLBoxAccess().memoryWrite16(address, data);
    }
    catch (CCUException& e) {
        // try once again
        LOG4CPLUS_WARN(logger, "CCUException during memoryWrite16 from address " << address << " reseting FEC and trying once again");
        getHalfBox()->getLBoxAccess().getFecManager().resetPlxFecForAutoRecovery();
        getHalfBox()->getLBoxAccess().memoryWrite16(address, data);
    }
}

TI2CInterface& TCB::getI2C() {
    if (i2c == 0) {
        if (lbox == 0) {
            throw TException("TCB::GetCBLBSTDI2C: LBox == NULL");
        }

        // enable I2C on control board
        //LBox->GetLBoxAccess()->MemoryWrite16(0x700d, 2);

        i2c = new TRPCCCUI2CMasterCore(
                  lbox->getHalfBox(getPosition())->getLBoxAccess(),
                  CBPC::CBPC_I2C, CBPC::CBPC_I2C + 1, CBPC::CBPC_I2C + 2, CBPC::CBPC_I2C + 3, CBPC::CBPC_I2C + 4, true);
/*        i2c->WriteControlReg(0);
        //I2C->SetPrescaleValue(128);
        i2c->SetPrescaleValue(prescaler);
        i2c->WriteControlReg(TI2CMasterCore::BIT_EN); call initI2C() instead*/
    }

    return *i2c;
}

void TCB::initI2C() {
    getI2cAccess().init();
}

void TCB::selectI2CChannel(int channel) {
    LOG4CPLUS_DEBUG(logger, "Selecting i2c channel " << channel);
    memoryWrite(CBPC::CBPC_I2C_CHAN, channel);
}


/*
Feb& TCB::getFeb(int channel, unsigned int localNumber, bool endcap) {
    LOG4CPLUS_DEBUG(logger, "searching febs for channel " << channel);
    FebMap& febMap = febPerChannelMap[channel];
    if (febMap.empty()) {
        // there is not feb for this channel
        DistributionBoard* db = new DistributionBoard(&getI2C());
        Feb* feb = endcap ? new FebEndcap(*db, localNumber) : new Feb(*db, localNumber);
        febMap.insert(FebMap::value_type(localNumber, feb));
        return *feb;
    }

    FebMap::iterator iFeb = febMap.find(localNumber);
    if (iFeb == febMap.end()) {
        DistributionBoard& db = febMap.begin()->second->getDistributionBoard();
        Feb* feb = endcap ? new FebEndcap(db, localNumber) : new Feb(db, localNumber);
        febMap.insert(FebMap::value_type(localNumber, feb));
        return *feb;
    }
    return *iFeb->second;
}*/

Feb& TCB::getFeb(int channel, unsigned int localNumber) {
    return febInstanceManager.getFeb(channel, localNumber, &getI2C());
}

FebEndcap& TCB::getFebEndcap(int channel, unsigned int localNumber) {
    return febInstanceManager.getFebEndcap(channel, localNumber, &getI2C());
}

void TCB::checkVersions() {

    unsigned int cbicid;
    unsigned int cbicver;
    unsigned int cbicver_mjr;
    unsigned int cbpcid;
    unsigned int cbpcver;
    unsigned int cbpcver_mjr;
    //unsigned int fl1dil;

    try {
        cbicid = memoryRead(CBIC::CBIC_ID0);

        cbicver_mjr = memoryRead(CBIC::CBIC_ID_VER_MJR);
        cbicver     = memoryRead(CBIC::CBIC_ID_VER);

        cbpcver_mjr = memoryRead(CBPC::CBPC_VER_MJR);
        cbpcver     = memoryRead(CBPC::CBPC_VER_ID);

        cbpcid = memoryRead(CBPC::CBPC_IDENT);
        //fl1dil = lboxAccess.memoryRead(CBPC::CBPC_FL1_DIL);
    }
    catch(TException& e) {
        std::ostringstream ostr;
        ostr << "Error while checking device version: " << e.what()
        << ". Device id = " << getId() << " description = " <<  getDescription()
        << " board name = " << getDescription();
        throw TException(ostr.str().c_str());
    };

    //std::ostringstream ostr;
    //ostr << getDescription() <<  hex << " CBIC:" << " CBIC_ID0=0x" << cbicid << " CBIC_VER=0x" << cbicver << "\n";
    //ostr << getDescription() <<  hex << " CBPC:" << " CBPC_ID0=0x" << cbpcid << " CBPC_VER=0x" << cbpcver << " CBPC_FL1_DIL=0x" << fl1dil;

    EBadDeviceVersion::VersionInfoList errorList;
    ostringstream err;
    if (cbicid != 0xcb) {
        err << std::hex << getDescription() << " bad CBIC_ID0 value " << cbicid << " expected " << 0xcb;
        errorList.push_back(EBadDeviceVersion::VersionInfo(this, "CBIC_ID0", rpct::toHexString(0xcb),
                        rpct::toHexString(cbicid)));
    }
    if (cbpcid != 0xcc) {
        err << std::hex << getDescription() << " bad CBPC_IDENT value " << cbpcid << " expected " << 0xcc;
        errorList.push_back(EBadDeviceVersion::VersionInfo(this, "CBPC_IDENT", rpct::toHexString(0xcc),
                                rpct::toHexString(cbpcid)));
    }
    if(cbicver_mjr != CBIC::CBIC_VERSION_MJR || cbicver != CBIC::CBIC_VERSION) {
    	err << std::hex << getDescription() << " bad CBIC version " << cbicver_mjr<<"."<<cbicver
    	    << " expected " << CBIC::CBIC_VERSION_MJR<<"."<<CBIC::CBIC_VERSION;

    	ostringstream ostrExp;
    	ostrExp<< CBIC::CBIC_VERSION_MJR<<"."<<CBIC::CBIC_VERSION;
    	ostringstream ostrRead;
    	ostrRead<<cbicver_mjr<<"."<<cbicver;
    	errorList.push_back(EBadDeviceVersion::VersionInfo(this, "CBIC_VERSION", ostrExp.str(), ostrRead.str()));
    }
    if(cbpcver_mjr != CBPC::CBPC_VERSION_MAJOR || cbpcver != CBPC::CBPC_VERSION) {
    	err << std::hex << getDescription() << " bad CBPC version " << cbpcver_mjr<<"."<<cbpcver
    			<< " expected " << CBPC::CBPC_VERSION_MAJOR<<"."<<CBPC::CBPC_VERSION;

    	ostringstream ostrExp;
    	ostrExp<< CBPC::CBPC_VERSION_MAJOR<<"."<<CBPC::CBPC_VERSION;
    	ostringstream ostrRead;
    	ostrRead<<cbpcver_mjr<<"."<<cbpcver;
    	errorList.push_back(EBadDeviceVersion::VersionInfo(this, "CBPC_VERSION", ostrExp.str(), ostrRead.str()));
    }
    if (err.str().size() > 0) {
        LOG4CPLUS_ERROR(logger, err.str());
        throw EBadDeviceVersion(err.str().c_str(), errorList);
    }

    BoardBase::checkVersions();
}



string TCB::readTestRegs(bool verbose) {

    unsigned int cbicid;
    unsigned int cbicver;
    unsigned int cbpcid;
    unsigned int cbpcver;
    unsigned int fl1dil;

    try {
        cbicid = memoryRead(CBIC::CBIC_ID0);
        cbicver = memoryRead(CBIC::CBIC_ID_VER_MJR) * 256 + memoryRead(CBIC::CBIC_ID_VER);
        cbpcver = memoryRead(CBPC::CBPC_VER_MJR) * 256 + memoryRead(CBPC::CBPC_VER_ID);
        cbpcid = memoryRead(CBPC::CBPC_IDENT);
        fl1dil = memoryRead(CBPC::CBPC_FL1_DIL);
        if(verbose) {
            // read more registers...
        }
    }
    catch(TException& e) {
        std::ostringstream ostr;
        ostr << "Error while checking device version: " << e.what()
        << ". Device id = " << getId() << " description = " <<  getDescription()
        << " board name = " << getDescription();
        throw TException(ostr.str().c_str());
    };

    std::ostringstream ostr;
    ostr << getDescription() <<  hex << " CBIC:" << " CBIC_ID0=0x" << cbicid << " CBIC_VER=0x" << cbicver << "\n";
    ostr << getDescription() <<  hex << " CBPC:" << " CBPC_ID0=0x" << cbpcid << " CBPC_VER=0x" << cbpcver << " CBPC_FL1_DIL=0x" << fl1dil << " (exp=0xed)";
    if(verbose) {
        // print out more registers...
    }

    return ostr.str();
}


void TCB::monitor(volatile bool *stop) {
    monitor(monitorItems_);
}

void TCB::monitor(MonitorItems& items) {
	warningStatusList_.clear();

	LOG4CPLUS_DEBUG(logger, "BlockI2c::getStatus() (rea)   " << (int)(getBlockI2c().getStatus()));
	LOG4CPLUS_DEBUG(logger, "BlockI2c::getExtendedStatus() " << (int)(getBlockI2c().getExtendedStatus()));

	for (MonitorItems::iterator iItem = items.begin(); iItem != items.end(); ++iItem) {

		if ((*iItem) == MonitorItemName::CONFIG_LOST) {
			MonitorableStatus monStatus(MonitorItemName::CONFIG_LOST, MonitorableStatus::OK, " ", 0) ;

			unsigned int val = memoryRead(CBPC::CBPC_RST_COUNT);
			if (val != 0) {
				monStatus.level = MonitorableStatus::ERROR;
				monStatus.message += "CBPC_RST_COUNT is " + toString(val) + ". ";
			}

			/* disabled, because it triggers false alarm when reloading from flash
			 * maciekz 20120301*/
			/*
            val = memoryRead(CBPC::CBPC_FL1_DIL);
            if (val != 0xed) {
            	monStatus.level = MonitorableStatus::ERROR;
            	monStatus.message += "CBPC_FL1_DIL is not 0xed: " + toHexString(val) + ". ";
            }
			 */

			if(monStatus.level != MonitorableStatus::OK)
				warningStatusList_.push_back(monStatus);
		}
		else if ((*iItem) == MonitorItemName::RELOAD_DETECTED) {
			if(checkAndUpdateLastCbicReloadCounter(false)) {
				warningStatusList_.push_back(MonitorableStatus(MonitorItemName::RELOAD_DETECTED, MonitorableStatus::SOFT_WARNING, "CBIC::CBIC_RELOAD_CNT increased", 0));
			}
		}
		else if ((*iItem) == MonitorItemName::TTCRX) {
			unsigned int ttcrxRead = getHalfBox()->getLBoxAccess().piaRead(CBIC::CCU_PA_TTCReady);
			if( (ttcrxRead & CBIC::CBICI_TTC_READY) != CBIC::CBICI_TTC_READY ) {
				warningStatusList_.push_back(MonitorableStatus(MonitorItemName::TTCRX, MonitorableStatus::SOFT_WARNING, "TTCrx not ready", 0));
			}
		}
	}

}

void TCB::configure(ConfigurationSet* configurationSet, int configureFlags) {
    memoryWrite(CBPC::CBPC_RST_COUNT, 0x0);
    memoryWrite(CBPC::CBPC_FL1_DIL, 0xed);
    BoardBase::configure(configurationSet, configureFlags);
    getI2cAccess().init();
}

HalfBox* TCB::getHalfBox() {
    return lbox->getHalfBox(getPosition());
}

StdLinkBoxAccess * TCB::getLBoxAccess() const
    throw (rpct::exception::SystemException)
{
    if (lbox == 0)
        throw rpct::exception::SystemException("TCB::getLBoxAccess(): lbox is nullpointer");
    else
        {
            rpct::HalfBox * halfbox = lbox->getHalfBox(const_cast<TCB*>(this)->getPosition());
            if (halfbox != 0)
                return &(halfbox->getLBoxAccess());
            else
                throw rpct::exception::SystemException("TCB::getLBoxAccess(): halfbox is nullpointer");
        }
}
rpct::devices::i2c::CommandI2c & TCB::getCommandI2c()
{
    if (!command_i2c_)
        {
            LOG4CPLUS_DEBUG(logger, "creating command_i2c_ for cb " << id_ << " " << description_ << " " << position_);
            command_i2c_ = new rpct::devices::i2c::CommandI2c(*this);
        }
    return *command_i2c_;
}
rpct::devices::i2c::BlockI2c & TCB::getBlockI2c()
{
    if (!block_i2c_)
        {
            LOG4CPLUS_DEBUG(logger, "creating block_i2c_ for cb " << id_ << " " << description_ << " " << position_);
            block_i2c_ = new rpct::devices::i2c::BlockI2c(*this);
        }
    return *block_i2c_;
}
rpct::devices::i2c::BI2cClient & TCB::getBI2cClient()
{
    if (!bi2c_client_)
        {
            LOG4CPLUS_DEBUG(logger, "creating bi2c_client_ for cb " << id_ << " " << description_ << " " << position_);
            bi2c_client_ = new rpct::devices::i2c::BI2cClient(*this);
        }
    return *bi2c_client_;
}

void TCB::enable()
{
	BoardBase::enable();
	checkAndUpdateLastCbicReloadCounter(true);
}

bool TCB::checkAndUpdateLastCbicReloadCounter(bool saveOnly)
{
	StdLinkBoxAccess& LBA = getHalfBox()->getLBoxAccess();
	uint16_t res = LBA.memoryRead(CBIC::CBIC_RELOAD_CNT);
	if (!saveOnly) {
		if (res != lastCbicReloadCounter) {
			LOG4CPLUS_INFO(logger, "CBIC_RELOAD_CNT changed from "<<lastCbicReloadCounter<<" to "<<res<<" on "<<getDescription());
			lastCbicReloadCounter = res;
			return true;
		}
	}
	lastCbicReloadCounter = res;
	return false;
}

} // namespace
