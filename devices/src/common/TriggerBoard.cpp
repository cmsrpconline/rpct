#include <algorithm>
#include <cctype>

#include "rpct/devices/TriggerBoard.h"
#include "rpct/devices/VmeCrate.h"
#include "rpct/devices/i2c.h"
#include "rpct/devices/System.h"
#include "rpct/devices/PacSettings.h"
#include "rpct/devices/TbGbSortSettings.h"
#include "rpct/devices/OptoBxData.h"
#include "rpct/devices/PacBxData.h"
#include "rpct/devices/RmbBxData.h"
#include "rpct/devices/TbGbSortBxData.h"
#include "rpct/devices/FixedHardwareSettings.h"
#include "rpct/std/IllegalArgumentException.h"
#include "rpct/std/NullPointerException.h"
#include "rpct/std/tbutil.h"
#include "rpct/ii/ConfigurationFlags.h"

//---------------------------------------------------------------------------


using namespace std;
using namespace log4cplus;

namespace rpct {

Logger TriggerBoardDevice::logger = Logger::getInstance("TriggerBoardDevice");
void TriggerBoardDevice::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}



//---------------------------------------------------------------------------
#define IID_FILE "RPC_system_def.iid"
#define IID_CLASS TriggerBoardDevice
#include "iid2cdef2.h"

#define IID_FILE "RPC_TB3_def.iid"
#define IID_CLASS TriggerBoardDevice
#include "iid2cdef2.h"

//---------------------------------------------------------------------------

#define IID_FILE "RPC_TB3_vme.iid"
#define IID_CLASS TriggerBoardVme
#include "iid2c2.h"

const HardwareItemType TriggerBoardVme::TYPE(HardwareItemType::cDevice, "TB3VME");

TriggerBoardVme::TriggerBoardVme(int vme_board_address, IBoard& board) :
    TriggerBoardDevice(board.getId() * 1000, VME_IDENTIFIER.c_str(), WORD_IDENTIFIER, "VME", TYPE,
            VME_VERSION.c_str(), WORD_VERSION, WORD_CHECKSUM, 0, board, -1, BuildMemoryMap(),
            vme_board_address), ABootContr(0) {
}

TriggerBoardVme::~TriggerBoardVme() {
    delete ABootContr;
}


//---------------------------------------------------------------------------

const HardwareItemType TriggerBoardControl::TYPE(HardwareItemType::cDevice, "TB3CONTROL");

#ifdef CII
#define IID_CLASS TriggerBoardControl
#include "CII_TB3_CTRL_iicfg_tab.cpp"
#undef IID_CLASS
#else
#define IID_FILE "RPC_TB3_ctrl.iid"
#define IID_CLASS TriggerBoardControl
#include "iid2c2.h"
const int TriggerBoardControl::II_ADDR_SIZE = TriggerBoardDevice::TB_II_ADDR_WIDTH_SOFT;
const int TriggerBoardControl::II_DATA_SIZE = TriggerBoardDevice::TB_II_DATA_WIDTH;
#endif

TriggerBoardControl::TriggerBoardControl(int vme_board_address, IBoard& board, int position) :
    		TriggerBoardDevice(board.getId() * 1000 + 1, TB_CTRL_IDENTIFIER.c_str(), WORD_IDENTIFIER,
    				TB_CTRL_IDENTIFIER, TYPE, TB_CTRL_VERSION.c_str(), WORD_VERSION, WORD_CHECKSUM,
    				TB_BASE_CNTRL, board, position, BuildMemoryMap(), vme_board_address), ABootContr(0),
    				I2C(0), TTCrxI2C(7, board.getDescription() ),
    				qpll_(*this, logger,
    						VECT_QPLL,
    						BITS_QPLL_MODE,
    						BITS_QPLL_CTRL,
    						BITS_QPLL_SELECT,
    						BITS_QPLL_LOCKED,
    						BITS_QPLL_ERROR,
    						BITS_QPLL_CLK_LOCK)
{
    monitorItems_.push_back(MonitorItemName::FIRMWARE);
	monitorItems_.push_back(MonitorItemName::QPLL);
	//monitorItems_.push_back(MON_TTCRX);
}

TriggerBoardControl::~TriggerBoardControl() {
    delete ABootContr;
}

void TriggerBoardControl::ResetQPLL() {
    writeBits(BITS_TTC_TC_ENA, 1); //we are using the TTC signals and clock from the TC, not from the TB TTCrx, bacause it does not work

    qpll_.reset();
}

TIII2CMasterCore& TriggerBoardControl::GetI2C() {
    if (I2C == NULL) {

        I2C = new TIII2CMasterCore(*this, AREA_I2C);
        I2C->SetPrescaleValue(100);
        I2C->WriteControlReg(TI2CMasterCore::BIT_EN);
    }

    return *I2C;
}

void TriggerBoardControl::SetTTCrxI2CAddress(unsigned char addr) {
    TTCrxI2C.SetAddress(addr);
    writeWord(WORD_TTC_ID_MODE, 0, addr);
    writeBits(BITS_TTC_INIT_ENA, 1);
    ResetTTC();
    writeBits(BITS_TTC_INIT_ENA, 0);
}

void TriggerBoardControl::ResetTTC() {
    writeBits(BITS_TTC_RESET, 1);
    tbsleep(3000);
    if (readBits(BITS_TTC_READY) != 0) {
        //throw TException("TriggerBoardControl::ResetTTC: BITS_TTC_READY did not go down after BITS_TTC_RESET, 1");
        LOG4CPLUS_ERROR(logger, "TriggerBoardControl::ResetTTC: BITS_TTC_READY did not go down after BITS_TTC_RESET, 1");
    }
    writeBits(BITS_TTC_RESET, 0);
}

void TriggerBoardControl::InitTTC() {
    SetTTCrxI2CAddress(TTCrxI2C.GetAddress());
    tbsleep(100);
    if (readBits(BITS_TTC_READY) != 1) {
        //throw TException("TriggerBoardControl::InitTTC: BITS_TTC_READY did not go up");
        LOG4CPLUS_ERROR(logger, "TriggerBoardControl::InitTTC: BITS_TTC_READY did not go up");

    }
    // Enable Clock40Des2 output
    LOG4CPLUS_WARN(logger, "tb3: Clock40Des2 not enabled ");
    //GetTTCrxI2C().WriteControlReg(GetTTCrxI2C().ReadControlReg() | 0x8);
}

void TriggerBoardControl::reset() {
    writeBits(BITS_TTC_TEST_ENA, uint32_t(0));
    writeBits(BITS_TTC_TC_ENA, 1);
    writeBits(BITS_TTC_TC_DELAY, FixedHardwareSettings::TBCONTROL_TTC_DATA_DELAY);
}

bool TriggerBoardControl::checkResetNeeded() {
    return !(readBits(BITS_QPLL_LOCKED) );
}

bool TriggerBoardControl::checkWarm(DeviceSettings* s) {
    //return readBits(BITS_QPLL_LOCKED) == 1;
    return true;
}

void TriggerBoardControl::configure(DeviceSettings* settings, int flags) {
    if (flags & ConfigurationFlags::FORCE_CONFIGURE) {
        reset();
    }
}

void TriggerBoardControl::monitor(volatile bool *stop) {
    monitor(monitorItems_);
}

void TriggerBoardControl::monitor(MonitorItems& items) {
    warningStatusList_.clear();
    for (MonitorItems::iterator iItem = items.begin(); iItem != items.end(); ++iItem) {
        if ((*iItem) == MonitorItemName::FIRMWARE) {
            string msg = monitorVersion();
            if (msg.size() != 0) {
                warningStatusList_.push_back(MonitorableStatus(MonitorItemName::FIRMWARE, MonitorableStatus::ERROR, msg, 0));
            }
        }
        else if ((*iItem) == MonitorItemName::QPLL) {
            if (readBits(BITS_QPLL_LOCKED) != 1) {
            	if (readBits(BITS_QPLL_LOCKED) != 1) {
            		warningStatusList_.push_back(MonitorableStatus(MonitorItemName::QPLL, MonitorableStatus::ERROR, "BITS_QPLL_LOCKED is not up", 0));
            	}
            	else {
            		warningStatusList_.push_back(MonitorableStatus(MonitorItemName::QPLL, MonitorableStatus::WARNING, "BITS_QPLL_LOCKED was not up for a while", 0));
            	}
            }
        }
        /*else if ((*iItem) == MON_TTCRX) {
         if (readBits(BITS_TTC_READY) != 0) {
         throw MonitorableException(*this, MON_TTCRX, "BITS_TTC_READY is not down");
         }
         }*/
    }
}

//---------------------------------------------------------------------------

//const int Opto::II_ADDR_SIZE = TriggerBoardDevice::TB_II_ADDR_WIDTH_SOFT;
//const int Opto::II_DATA_SIZE = TriggerBoardDevice::TB_II_DATA_WIDTH;
const HardwareItemType Opto::TYPE(HardwareItemType::cDevice, "TB3OPTO");

#ifdef CII
#define IID_CLASS Opto
#include "CII_TB3_OPTO_iicfg_tab.cpp"
#undef IID_CLASS
#else
#define IID_FILE "RPC_TB3_opto.iid"
#define IID_CLASS Opto
#include "iid2c2.h"
const int Opto::II_ADDR_SIZE = TriggerBoardDevice::TB_II_ADDR_WIDTH_SOFT;
const int Opto::II_DATA_SIZE = TriggerBoardDevice::TB_II_DATA_WIDTH;
#endif

Opto::Opto(int id, int vme_board_address, int ii_addr, const char* desc, IBoard& board, int position) :
    TriggerBoardDevice(id, TB_OPTO_IDENTIFIER.c_str(), WORD_IDENTIFIER, desc, TYPE,
            TB_OPTO_VERSION.c_str(), WORD_VERSION, WORD_CHECKSUM, ii_addr, board, position,
            BuildMemoryMap(), vme_board_address),
            optLinkStatus_(3, OFF),
            DiagCtrl(0), DiagnosticReadout(0),
            PulserDiagCtrl(0), Pulser(0),
            EnabledOptLinks(3, 0ul),
            monRecErrorAnalyzer_("ERR_CNT", 1, 2, 1000, 0xffff)
{
    monRecErrorAnalyzer_.setMaxCounterVal( (1ull << memoryMap_->GetItemInfo(WORD_REC_ERROR_COUNT).Width) - 1);

    for (unsigned int iLink = 0; iLink < GetOptLinksCount(); iLink++) {
        OptLinks.push_back(TOptLink(this, iLink));
    }

    monitorItems_.push_back(MonitorItemName::FIRMWARE);
    monitorItems_.push_back(MonitorItemName::OPT_LINK);
}

Opto::~Opto() {
    delete DiagCtrl;
    delete DiagnosticReadout;
    delete Pulser;
    delete PulserDiagCtrl;
}

TDiagCtrl& Opto::GetDiagCtrl() {
    if (DiagCtrl == 0) {
        DiagCtrl = new TDiagCtrl(*this,
                VECT_DAQ,
                BITS_DAQ_PROC_REQ,
                BITS_DAQ_PROC_ACK,
                BITS_DAQ_TIMER_LOC_ENA,
                0,
                BITS_DAQ_TIMER_START,
                BITS_DAQ_TIMER_STOP,
                BITS_DAQ_TIMER_TRIG_SEL,
                WORD_DAQ_TIMER_LIMIT,
                WORD_DAQ_TIMER_COUNT,
                WORD_DAQ_TIMER_TRG_DELAY,
                "DAQ_DIAG_CTRL");
    }
    return *DiagCtrl;
}

TStandardDiagnosticReadout& Opto::GetDiagnosticReadout() {
    if (DiagnosticReadout == 0) {
        DiagnosticReadout = new TStandardDiagnosticReadout(*this,
                "READOUT",
                &OptoBxDataFactory::getInstance(),
                GetDiagCtrl(),
                VECT_DAQ,
                BITS_DAQ_EMPTY,
                BITS_DAQ_EMPTY_ACK,
                BITS_DAQ_LOST,
                BITS_DAQ_LOST_ACK,
                VECT_DAQ_WR,
                BITS_DAQ_WR_ADDR,
                BITS_DAQ_WR_ACK,
                VECT_DAQ_RD,
                BITS_DAQ_RD_ADDR,
                BITS_DAQ_RD_ACK,
                WORD_DAQ_MASK,
                WORD_DAQ_DATA_DELAY,
                AREA_MEM_DAQ_DIAG,
                DAQ_DIAG_DATA_SIZE,
                DAQ_DIAG_TRIG_NUM,
                DAQ_DIAG_TIMER_SIZE,
                TTC_BCN_EVT_WIDTH);
    }
    return *DiagnosticReadout;
}

TDiagCtrl& Opto::GetPulserDiagCtrl() {
    if (PulserDiagCtrl == NULL) {
        PulserDiagCtrl = new TDiagCtrl(*this,
                VECT_PULSER,
                BITS_PULSER_PROC_REQ,
                BITS_PULSER_PROC_ACK,
                BITS_PULSER_TIMER_LOC_ENA,
                0,
                BITS_PULSER_TIMER_START,
                BITS_PULSER_TIMER_STOP,
                BITS_PULSER_TIMER_TRIG_SEL,
                WORD_PULSER_TIMER_LIMIT,
                WORD_PULSER_TIMER_COUNT,
                WORD_PULSER_TIMER_TRG_DELAY,
                "PULSER_DIAG_CTRL");
    }

    return *PulserDiagCtrl;
}

TPulser& Opto::GetPulser() {
    if (Pulser == NULL) {
        Pulser = new OptoPulser(*this,
                "PULSER",
                GetPulserDiagCtrl(),
                AREA_MEM_PULSE,
                WORD_PULSER_LENGTH,
                BITS_PULSER_REPEAT_ENA,
                BITS_PULSER_OUT_ENA);
    }
    return *Pulser;
}

Opto::TDiagCtrlVector& Opto::GetDiagCtrls() {
    if (DiagCtrls.empty()) {
        DiagCtrls.push_back(&GetDiagCtrl());
        DiagCtrls.push_back(&GetPulserDiagCtrl());
    }
    return DiagCtrls;
}

Opto::TDiagVector& Opto::GetDiags() {
    if (Diags.empty()) {
        Diags.push_back(&GetDiagnosticReadout());
        Diags.push_back(&GetPulser());
    }
    return Diags;
}

void Opto::ConfigureDiagnosable(TTriggerDataSel triggerDataSel, uint32_t dataTrgDelay) {
    writeBits(BITS_STATUS_TRG_DATA_SEL, triggerDataSel);
    writeWord(WORD_DATA_TRG_DELAY, 0, dataTrgDelay);
}

void Opto::enableTransmissionCheck() {
    bool checkDataEna = true;
    bool checkEna = true;
    if(checkDataEna) {
        writeWord(WORD_REC_CHECK_DATA_ENA, 0, GetEnabledOptLinks().to_ulong());
        writeWord(WORD_SEND_CHECK_DATA_ENA, 0, 7);
    }
    else {
        writeWord(WORD_REC_CHECK_DATA_ENA, 0, uint32_t(0));
        writeWord(WORD_SEND_CHECK_DATA_ENA, 0, uint32_t(0));
    }

    if(checkEna) {
        writeWord(WORD_REC_CHECK_ENA, 0, GetEnabledOptLinks().to_ulong());
        writeWord(WORD_SEND_CHECK_ENA, 0, 7);
    }
    else {
        writeWord(WORD_REC_CHECK_ENA, 0, uint32_t(0));
        writeWord(WORD_SEND_CHECK_ENA, 0, uint32_t(0));
    }
}

void Opto::resetErrorCounters() {
    writeWord(WORD_REC_ERROR_COUNT, 0, uint32_t(0));
    for(unsigned int iLink = 0; iLink < optLinkStatus_.size(); iLink++) {
        if(EnabledOptLinks[iLink]) {// && (optLinkStatus_[iLink] != RX_ERR) ) { what for I did it??? KB
            readWord(WORD_REC_TEST_OR_DATA, iLink);
        }
    }
    readBits(Opto::BITS_TLK_RX_ERROR);
    monRecErrorAnalyzer_.reset();
}

void Opto::reset() {
    writeBits(BITS_STATUS_TTC_DATA_DELAY, FixedHardwareSettings::OPTO_TTC_DATA_DELAY); // may change after firmware chnge!!!!

/*    writeBits(BITS_TLK_ENABLE, 0, 0);
    writeBits(BITS_TLK_LCK_REFN, 0, uint32_t(0));
    writeWord(WORD_REC_TEST_ENA, 0, 7ul);
    LOG4CPLUS_INFO(logger, getBoard().getDescription()<<" "<<getDescription()<<" all opt links inputs disabled");*/

    writeWord(WORD_REC_TEST_RND_ENA, 0, uint32_t(0));
    for(unsigned int iLink = 0; iLink < 3; iLink++) {
        setRecDataDelay(iLink, uint32_t(0));
        writeWord(WORD_SEND_TEST_DATA, iLink, uint32_t(0));
    }

    writeWord(WORD_SEND_TEST_ENA, 0, uint32_t(0));
    writeWord(WORD_SEND_TEST_RND_ENA, 0, uint32_t(0));

    writeBits(BITS_PULSER_OUT_ENA, uint32_t(0));

    writeWord(WORD_BCN0_DELAY, 0, FixedHardwareSettings::OPTO_BC0_DELAY);

    writeWord(WORD_SEND_CHECK_DATA_ENA, 0, 7);
    writeWord(WORD_SEND_CHECK_ENA, 0, 7);

    writeWord(WORD_REC_CHECK_DATA_ENA, 0, uint32_t(0)); //because all links disabled
    writeWord(WORD_REC_CHECK_ENA, 0, uint32_t(0));

    writeBits(BITS_STATUS_REC_HRESET_ENA, 1);
    writeWord(WORD_HRESET_TIMER, 0, FixedHardwareSettings::CONFIGURE_FROM_FLASH_TIME * 40); //[us]*40 BX/us
    //resetErrorCounters();
}


void Opto::configure(DeviceSettings* settings, int flags) {
    if (flags & ConfigurationFlags::FORCE_CONFIGURE) {
        reset();
    }

    if (settings == 0) {
        throw NullPointerException("Opto::configure: settings == 0");
    }
    OptoSettings* s = dynamic_cast<OptoSettings*>(settings);
    if (s == 0) {
        throw IllegalArgumentException("Opto::configure: invalid cast for settings");
    }
    unsigned int enable = s->isEnableLink0() + (s->isEnableLink1() << 1) + (s->isEnableLink2() << 2);
    EnabledOptLinks = boost::dynamic_bitset<>(3, enable);
    for(unsigned int i = 0; i < EnabledOptLinks.size(); i++) {
        if(EnabledOptLinks[i])
            optLinkStatus_[i] = ON;
        else
            optLinkStatus_[i] = OFF;
    }
    EnableOptoLink(enable);
    LOG4CPLUS_INFO(logger, getBoard().getDescription()<<" "<<getDescription()<<" enabled opt links "<<enable);
    enableTransmissionCheck();
    resetErrorCounters();
}

void Opto::enable() {
    //writeWord(WORD_REC_ERROR_COUNT, 0, uint32_t(0));
    resetErrorCounters();
}

void Opto::setRecFastDelay(unsigned int linkNum, unsigned int delay) {
/*    unsigned int value = readWord(WORD_REC_FAST_DATA_DELAY, 0);
    value &= ~(7 << (linkNum * 3));
    value |= delay << (linkNum * 3);
    writeWord(WORD_REC_FAST_DATA_DELAY, 0, value);*/
}

void Opto::setRecDataDelay(unsigned int linkNum, unsigned int delay) {
/*    unsigned int value = readWord(WORD_REC_DATA_DELAY, 0);
    value &= ~(7 << (linkNum * 3));
    value |= delay << (linkNum * 3);
    writeWord(WORD_REC_DATA_DELAY, 0, value);*/
}

Opto::TOptLink::TOptLink(Opto* opto, int optLinkNum)
: opto_(opto), number_(optLinkNum) {
}

Opto::TOptLink::~TOptLink() {
}

void Opto::TOptLink::EnableOptoLink(bool enable) {
    uint32_t tlkEnable = GetOpto().readBits(Opto::BITS_TLK_LCK_REFN);
    if(enable) {
        tlkEnable = tlkEnable | 1 << number_;
        GetOpto().EnabledOptLinks[number_] = true;
    }
    else {
        tlkEnable = tlkEnable & (~(1<<number_));
        GetOpto().EnabledOptLinks[number_] = false;
    }
    GetOpto().writeBits(Opto::BITS_TLK_ENABLE, tlkEnable);
    GetOpto().writeBits(Opto::BITS_TLK_LCK_REFN, tlkEnable);
    GetOpto().writeWord(WORD_REC_TEST_ENA, 0, (~tlkEnable) & 7);
}

bool Opto::TOptLink::FindOptLinksTLKDelay(unsigned int testData) {
    vector<unsigned int> recTestsData;
    for(uint32_t del = 0; del < 8; del++) {
        GetOpto().setRecFastDelay(number_, del);
        recTestsData.push_back(GetOpto().readWord(Opto::WORD_REC_TEST_DATA, number_));
        //recTestsData.push_back(GetOpto().readWord(Opto::WORD_TLK_RXDATA, number_));
        //cout<<"del "<<del<<" "<<hex<<recTestsData[del]<<endl;
    }
    int firsGood = -1;
    int lastGood = -1;
    int foundDel = -1;
    if(recTestsData[0] == testData ) {
        firsGood = 0;
    }
    for(int del = 0; del < 7; del++) {
        if(recTestsData[del] != testData && recTestsData[del+1] == testData)
            firsGood = del + 1;
        else if(recTestsData[del] == testData && recTestsData[del+1] != testData)
            lastGood = del;
    }
    if(lastGood == -1 && recTestsData[7] == testData ) {
        lastGood = 7;
    }

    if(firsGood != -1 && lastGood != -1) {
        if(firsGood == 0 && lastGood == 7) {
            foundDel = 3;
            LOG4CPLUS_DEBUG(logger, GetOpto().getBoard().getDescription()
                    << " " << GetOpto().getBoard().getId() << " "
                    << GetOpto().getDescription() << " FindOptLinksTLKDelay: linkNum " << number_
                    << " foundDel " << foundDel);
            GetOpto().setRecFastDelay(number_, foundDel);
            //GetOpto().writeWord(Opto::WORD_TLK_DELAY, number_, foundDel);
        }
        else {
            if(lastGood >= firsGood)
                foundDel = (firsGood + lastGood ) / 2;
            else {
                foundDel = ((firsGood + lastGood + 8) / 2) % 8;
            }
            LOG4CPLUS_INFO(logger, GetOpto().getBoard().getDescription()<<" "<<GetOpto().getBoard().getId()<<" "<<GetOpto().getDescription() << " FindOptLinksTLKDelay: linkNum " << number_ <<" foundDel "<<foundDel);

            GetOpto().setRecFastDelay(number_, foundDel);
        }

        return true;
    }
    else {
        LOG4CPLUS_ERROR(logger, GetOpto().getBoard().getDescription()<<" "<<GetOpto().getBoard().getId()<<" "<<GetOpto().getDescription() << " FindOptLinksTLKDelay: linkNum " << number_ <<" delay not found, synchronization not succided!!!");
        GetOpto().setRecFastDelay(number_, uint32_t(0));
        return false;
    }
}

bool Opto::TOptLink::FindOptLinkDelay() {
    for(unsigned int del = 0; del < 8; del++) {
        GetOpto().setRecDataDelay(number_, del);
        GetOpto().readWord(Opto::WORD_REC_TEST_OR_DATA, number_);
        tbsleep(500);
        uint32_t recTestOr = GetOpto().readWord(Opto::WORD_REC_TEST_OR_DATA, number_);
        if (recTestOr == 0) {
            LOG4CPLUS_INFO(logger, GetOpto().getBoard().getDescription()<<" "<<GetOpto().getBoard().getId()<<" "<<GetOpto().getDescription() << " FindOptLinkDelay: linkNum " << number_ <<" delay "<<del);
            return true;
        }
        /*        else if (recTestOr < 100) {
                 LOG4CPLUS_INFO(logger, GetOpto().getBoard().getDescription()<<" "<<GetOpto().getBoard().getId()<<" "<<GetOpto().getDescription() << " FindOptLinkDelay: linkNum " << number_ <<" delay "<<del<<" but WORD_REC_ERROR_COUNT = "<<recTestOr);
                 return true;
                 }*/
    }
    LOG4CPLUS_ERROR(logger, GetOpto().getBoard().getDescription()<<" "<<GetOpto().getBoard().getId()<<" "<<GetOpto().getDescription() << " FindOptLinkDelay: linkNum " << number_ <<" delay not found, synchronization not succided!!!");
    return false;
}

/*void Opto::EnableOptoLink(int linkNum, bool enable) {
         uint32_t tlkEnable = readWord(Opto::WORD_TLK_ENABLE, 0);
         if(enable) {
         tlkEnable =  tlkEnable | 1<<linkNum;
         EnabledOptLinks[linkNum] = true;
         }
         else {
         tlkEnable =  tlkEnable & (~(1<<linkNum));
         EnabledOptLinks[linkNum] = false;
         }
         writeWord(Opto::WORD_TLK_ENABLE, 0, tlkEnable);
         writeWord(Opto::WORD_TLK_LCK_REFN, 0, tlkEnable);
         }*/

void Opto::EnableOptoLink(uint32_t enable) {
    writeBits(Opto::BITS_TLK_ENABLE, enable);
    writeBits(Opto::BITS_TLK_LCK_REFN, enable);
    writeBits(Opto::BITS_STATUS_REC_SYNCH_REQ, enable);

    writeWord(WORD_REC_TEST_ENA, 0, (~enable) & 7);

    /*
    EnabledOptLinks = boost::dynamic_bitset<>(3, enable);
    for(unsigned int i = 0; i < EnabledOptLinks.size(); i++) {
        if(EnabledOptLinks[i])
            optLinkStatus_[i] = ON;
        else
            optLinkStatus_[i] = OFF;
    }*/
}

bool Opto::CheckLinksSynchronization() {
    bool good = true;
    int noData = readBits(Opto::BITS_TLK_RX_NO_DATA); //link synchronization error? should be 0; 1 if error appierd on given link
    int err = readBits(Opto::BITS_TLK_RX_ERROR); //should be 0
    //LOG4CPLUS_DEBUG(logger, getDescription()<<" CheckLinksSynchronization: TLK_RX_DV "<<noData<<" TLK_RX_ERR "<<err );

    if( (EnabledOptLinks.to_ulong() & noData) != 0 ||
            (EnabledOptLinks.to_ulong() & err) != 0 ) {
        good = false;
        LOG4CPLUS_ERROR(logger, getBoard().getDescription()<<" "<<getBoard().getId()<<" "<<getDescription()<<" CheckLinksSynchronization: TLK_RX_DV "<<noData<<" TLK_RX_ERR "<<err );
    }

    for(unsigned int iLink = 0; iLink < 3; iLink++) {
        if(EnabledOptLinks[iLink] == false)
            continue;

        unsigned int rxdata = readWord(Opto::WORD_REC_TEST_DATA, iLink);
        cout<<"iLink "<<iLink<<" WORD_TLK_RXDATA "<<rxdata<<endl;

        if(rxdata != 0x50bc50bc) {
            good = false;
            LOG4CPLUS_ERROR(logger, getBoard().getDescription()<<" "<<getBoard().getId()<<" "<<getBoard().getDescription()<<" "<<getDescription()<<" CheckLinksSynchronization: iLink "<<iLink<<" WORD_TLK_RXDATA "<<rxdata<<"  != 0x50bc50bc" );
        }
    }
    return good;
}

bool Opto::CheckLinksOperation() {
    bool good = true;
    unsigned int noData = readBits(Opto::BITS_TLK_RX_NO_DATA); //data valid, should be 0
    unsigned int err = readBits(Opto::BITS_TLK_RX_ERROR); //should be 0
    //LOG4CPLUS_DEBUG(logger, getDescription()<<" CheckLinksSynchronization: TLK_RX_DV "<<noData<<" TLK_RX_ERR "<<err );

    if( (EnabledOptLinks.to_ulong() & noData) != EnabledOptLinks.to_ulong() ||
            (EnabledOptLinks.to_ulong() & err) != 0) {
        good = false;
        LOG4CPLUS_ERROR(logger, getBoard().getDescription()<<" "<<getBoard().getId()<<" "<<getBoard().getDescription()<<" "<<getDescription()<<" CheckLinksOperation: TLK_RX_DV "<<noData<<" TLK_RX_ERR "<<err );
    }

    return good;
}

bool Opto::FindOptLinksTLKDelay(unsigned int testData) {
    bool good = true;
    for(unsigned int iLink = 0; iLink < OptLinks.size(); iLink++) {
        if(EnabledOptLinks[iLink] == false)
            continue;

        if(OptLinks[iLink].FindOptLinksTLKDelay(testData) == false)
            good = false;
    }
    return good;
}

bool Opto::FindOptLinksDelay() {
    bool good = true;
    for(unsigned int iLink = 0; iLink < OptLinks.size(); iLink++) {
        if(EnabledOptLinks[iLink] == false)
            continue;

        if(OptLinks[iLink].FindOptLinkDelay() == false)
            good = false;
    }
    return good;
}

void Opto::monitor(volatile bool *stop) {
    monitor(monitorItems_);
}

void Opto::monitor(MonitorItems& items) {
    warningStatusList_.clear();
    for(unsigned int iLink = 0; iLink < optLinkStatus_.size(); iLink++) {
        if( optLinkStatus_[iLink] != OFF) {
            optLinkStatus_[iLink] = OK; //clearing the messages
        }
    }
    unsigned int enabled = EnabledOptLinks.to_ulong();
    for (MonitorItems::iterator iItem = items.begin(); iItem != items.end(); ++iItem) {
        if ((*iItem) == MonitorItemName::FIRMWARE) {
            string msg = monitorVersion();
            if (msg.size() != 0) {
                warningStatusList_.push_back(MonitorableStatus(MonitorItemName::FIRMWARE, MonitorableStatus::ERROR, msg, 0));
            }
        }
        else if ((*iItem) == MonitorItemName::OPT_LINK) {
            unsigned int err1 = readBits(BITS_TLK_RX_ERROR);
            unsigned int err2 = 0;
            if ((err1 & enabled) != 0) {
            	usleep(10000);
            	err2 = readBits(BITS_TLK_RX_ERROR);
            }

            uint32_t recErrCnt = readWord(WORD_REC_ERROR_COUNT, 0);
            monRecErrorAnalyzer_.newValue(recErrCnt);
            MonitorableStatus& recErrorMonStatus = monRecErrorAnalyzer_.getStatus();
            if(recErrCnt == monRecErrorAnalyzer_.getMaxCounterVal()) {
            	writeWord(WORD_REC_ERROR_COUNT, 0, uint32_t(0));
            }

            for(unsigned int iLink = 0; iLink < optLinkStatus_.size(); iLink++) {
            	MonitorableStatus monStatus(MonitorItemName::OPT_LINK, MonitorableStatus::OK, "optLink " + toString(getGlobalLinkNum(iLink)) + ": ", 0) ;

            	if(EnabledOptLinks[iLink]) {
            		if( (err2 & (1 << iLink)) != 0 ) {
            			optLinkStatus_[iLink] = RX_ERR;
            			monStatus.level = MonitorableStatus::ERROR;
            			monStatus.message +=  "persistent RX_ERROR, ";
            		}
            		else if( (err1 & (1 << iLink)) != 0 ) {
            			monStatus.level = MonitorableStatus::SOFT_WARNING;
            			monStatus.message +=  "transient RX_ERROR, ";
            		}

            		if (recErrorMonStatus.level != MonitorableStatus::OK) {
            			uint32_t recTestOr1 = readWord(WORD_REC_TEST_OR_DATA, iLink);
            			usleep(10000);
            			uint32_t recTestOr2 = readWord(WORD_REC_TEST_OR_DATA, iLink);
            			if(recTestOr2 != 0) {
            				if(optLinkStatus_[iLink] != RX_ERR) {
            					optLinkStatus_[iLink] = recErrorMonStatus.level + 2;
            				}

            				if(monStatus.level < recErrorMonStatus.level) {
            					monStatus.level = recErrorMonStatus.level;
            				}
            				monStatus.message += recErrorMonStatus.message;
            				monStatus.value = recErrorMonStatus.value;
            			}
            			else if(recTestOr1 != 0 ) {
            				if(optLinkStatus_[iLink] != RX_ERR) {
            					monStatus.level = MonitorableStatus::SOFT_WARNING;
            					monStatus.message +=  "transient receiver error " + recErrorMonStatus.message;
            				}
            			}
            		}

            		if(monStatus.level != MonitorableStatus::OK)
            			warningStatusList_.push_back(monStatus);
            	}
            }
        }
    }
}
//---------------------------------------------------------------------------

//const int Pac::II_ADDR_SIZE = TriggerBoardDevice::TB_II_ADDR_WIDTH_SOFT;
//const int Pac::II_DATA_SIZE = TriggerBoardDevice::TB_II_DATA_WIDTH;
const HardwareItemType Pac::TYPE(HardwareItemType::cDevice, "TB3LDPAC");

#ifdef CII
#define IID_CLASS Pac
#include "CII_TB3_LDPAC_iicfg_tab.cpp"
#undef IID_CLASS
#else
#define IID_FILE "RPC_TB3_ldpac.iid"
#define IID_CLASS Pac
#include "iid2c2.h"
const int Pac::II_ADDR_SIZE = TriggerBoardDevice::TB_II_ADDR_WIDTH_SOFT;
const int Pac::II_DATA_SIZE = TriggerBoardDevice::TB_II_DATA_WIDTH;
#endif

Pac::Pac(int id, int vme_board_address, int ii_addr, const char* desc,
        IBoard& board, int position): TriggerBoardDevice(
                id,
                TB_LDPAC_IDENTIFIER.c_str(),
                WORD_IDENTIFIER,
                desc,
                TYPE,
                TB_LDPAC_VERSION.c_str(),
                WORD_VERSION,
                WORD_CHECKSUM,
                ii_addr,
                board,
                position,
                BuildMemoryMap(),
                vme_board_address), DiagCtrl(0), DiagnosticReadout(0), PulserDiagCtrl(0), Pulser(0),
                monRecErrorAnalyzer_(MonitorItemName::RECEIVER, 1, 0xffffffff, 0xffffffff,  5, 1000, 0xffffff)
                                   //std::string name, minimumPeriod, warningCntThreshold, errorCntThreshold, warningRateThreshold, errorRateThreshold, maxCounterVal
{
    monRecErrorAnalyzer_.setMaxCounterVal((1ull << memoryMap_->GetItemInfo(WORD_REC_ERROR_COUNT).Width) - 1);
    monitorItems_.push_back(MonitorItemName::FIRMWARE);
    monitorItems_.push_back(MonitorItemName::RECEIVER);
    towerNum_ = (dynamic_cast<TriggerBoard&>(getBoard())).getTowerNum(getNumber());
    //cout<<getBoard().getDescription()<<dec<<" pac "<<getNumber()<<" tbNum "<<tbNum<<" towerNum_ "<<towerNum_ <<endl;
}

Pac::~Pac() {
    delete DiagCtrl;
    delete DiagnosticReadout;
    delete PulserDiagCtrl;
    delete Pulser;
}

TDiagCtrl& Pac::GetDiagCtrl() {
    if (DiagCtrl == 0) {
        DiagCtrl = new TDiagCtrl(*this,
                VECT_DAQ,
                BITS_DAQ_PROC_REQ,
                BITS_DAQ_PROC_ACK,
                BITS_DAQ_TIMER_LOC_ENA,
                0,
                BITS_DAQ_TIMER_START,
                BITS_DAQ_TIMER_STOP,
                BITS_DAQ_TIMER_TRIG_SEL,
                WORD_DAQ_TIMER_LIMIT,
                WORD_DAQ_TIMER_COUNT,
                WORD_DAQ_TIMER_TRG_DELAY,
                "DAQ_DIAG_CTRL");
    }
    return *DiagCtrl;
}

TStandardDiagnosticReadout& Pac::GetDiagnosticReadout() {
/*    if (DiagnosticReadout == 0) {
        DiagnosticReadout = new TStandardDiagnosticReadout(*this,
                "READOUT",
                &PacBxDataFactory::getInstance(),
                GetDiagCtrl(),
                VECT_DAQ,
                BITS_DAQ_EMPTY,
                BITS_DAQ_EMPTY_ACK,
                BITS_DAQ_LOST,
                BITS_DAQ_LOST_ACK,
                VECT_DAQ_WR,
                BITS_DAQ_WR_ADDR,
                BITS_DAQ_WR_ACK,
                VECT_DAQ_RD,
                BITS_DAQ_RD_ADDR,
                BITS_DAQ_RD_ACK,
                WORD_DAQ_MASK,
                WORD_DAQ_DATA_DELAY,
                AREA_MEM_DAQ_DIAG,
                DAQ_DIAG_DATA_SIZE,
                DAQ_DIAG_TRIG_NUM,
                DAQ_DIAG_TIMER_SIZE,
                TTC_BCN_EVT_WIDTH);
    }*/
    return *DiagnosticReadout;
}

TDiagCtrl& Pac::GetPulserDiagCtrl() {
    if (PulserDiagCtrl == NULL) {
        PulserDiagCtrl = new TDiagCtrl(*this,
                VECT_PULSER,
                BITS_PULSER_PROC_REQ,
                BITS_PULSER_PROC_ACK,
                BITS_PULSER_TIMER_LOC_ENA,
                0,
                BITS_PULSER_TIMER_START,
                BITS_PULSER_TIMER_STOP,
                BITS_PULSER_TIMER_TRIG_SEL,
                WORD_PULSER_TIMER_LIMIT,
                WORD_PULSER_TIMER_COUNT,
                WORD_PULSER_TIMER_TRG_DELAY,
                "PULSER_DIAG_CTRL");
    }

    return *PulserDiagCtrl;
}

TPulser& Pac::GetPulser() {
    if (Pulser == NULL) {
        Pulser = new TPulser(*this,
                "PULSER",
                GetPulserDiagCtrl(),
                AREA_MEM_PULSE,
                WORD_PULSER_LENGTH,
                BITS_PULSER_REPEAT_ENA,
                BITS_PULSER_OUT_ENA);
    }
    return *Pulser;
}

Pac::TDiagCtrlVector& Pac::GetDiagCtrls() {
    if (DiagCtrls.empty()) {
        //DiagCtrls.push_back(&GetDiagCtrl()); TODO
        DiagCtrls.push_back(&GetPulserDiagCtrl());
    }
    return DiagCtrls;
}

Pac::TDiagVector& Pac::GetDiags() {
    if (Diags.empty()) {
        //Diags.push_back(&GetDiagnosticReadout()); TODO
        Diags.push_back(&GetPulser());
    }
    return Diags;
}

void Pac::ConfigureDiagnosable(TTriggerDataSel triggerDataSel, uint32_t dataTrgDelay) {
    writeBits(BITS_STATUS_TRG_DATA_SEL, triggerDataSel);
    writeWord(WORD_DATA_TRG_DELAY, 0, dataTrgDelay);
}

void Pac::enableTransmissionCheck() {
    bool checkDataEna = true;
    bool checkEna = true;
    if(checkDataEna) {
        writeWord(WORD_REC_CHECK_DATA_ENA, 0, 0x3ffff);
        writeWord(WORD_SEND_CHECK_DATA_ENA, 0, 1);
    }
    else {
        writeWord(WORD_REC_CHECK_DATA_ENA, 0, uint32_t(0));
        writeWord(WORD_SEND_CHECK_DATA_ENA, 0, uint32_t(0));
    }

    if(checkEna) {
        writeWord(WORD_REC_CHECK_ENA, 0, 0x3ffff);
        writeWord(WORD_SEND_CHECK_ENA, 0, uint32_t(1));
    }
    else {
        writeWord(WORD_REC_CHECK_ENA, 0, uint32_t(0));
        writeWord(WORD_SEND_CHECK_ENA, 0, uint32_t(0));
    }
}

void Pac::resetErrorCounters() {
    writeWord(WORD_REC_ERROR_COUNT, 0, uint32_t(0));
    monRecErrorAnalyzer_.reset();
}

void Pac::reset() {
    writeBits(BITS_STATUS_TTC_DATA_DELAY, FixedHardwareSettings::PAC_TTC_DATA_DELAY); //<<<may chnge after firmewe chnged
    //writeBits(BITS_STATUS_PAC_DATA_OR, 0);

    //writeWord(Pac::WORD_REC_TEST_ENA, 0, ~((dynamic_cast<TriggerBoard&>(getBoard())).GetUsedOptos().to_ulong()));
    writeWord(WORD_REC_TEST_ENA, 0, uint32_t(0));

    writeWord(WORD_REC_TEST_RND_ENA, 0, uint32_t(0));

    writeWord(WORD_SEND_TEST_ENA, 0, uint32_t(0));
    writeWord(WORD_SEND_TEST_RND_ENA, 0, uint32_t(0));
    writeWord(WORD_SEND_TEST_DATA, 0, uint32_t(0));

    writeBits(BITS_PULSER_OUT_ENA, uint32_t(0));

    //writeWord(WORD_PAC_ENA, 0, 0xfff);

    writeWord(WORD_BCN0_DELAY, 0, FixedHardwareSettings::PAC_BC0_DELAY);

    enableTransmissionCheck();
    resetErrorCounters();
}

void Pac::configure(DeviceSettings* settings, int flags) {
    if (flags & ConfigurationFlags::FORCE_CONFIGURE) {
        if (settings == 0) {
            /*if (flags & ConfigurationFlags::RESET_ONLY_WHEN_NO_CONFIG_DATA) {
                reset(); //!!!!! tu jest nietypowo, bo reset() jest na koncu configure() - Karol, nie wiem, czy to tak specjalnie
                return;
            }*/
            throw NullPointerException("Pac::configure: settings == 0");
        }
        PacSettings* s = dynamic_cast<PacSettings*>(settings);
        if (s == 0) {
            throw IllegalArgumentException("Pac::configure: invalid cast for settings");
        }

        writeWord(WORD_REC_MUX_CLK_INV, 0, s->getRecMuxClkInv());
        writeWord(WORD_REC_MUX_CLK90, 0, s->getRecMuxClk90());
        writeWord(WORD_REC_MUX_REG_ADD, 0, s->getRecMuxRegAdd());

        vector<int>& recMuxDelay = s->getRecMuxDelay();
        for (vector<int>::size_type i = 0; i < recMuxDelay.size(); i++) {
            writeWord(WORD_REC_MUX_DELAY, i, recMuxDelay[i]);
        }

        vector<int>& recDataDelay = s->getRecDataDelay();
        for (vector<int>::size_type i = 0; i < recDataDelay.size(); i++) {
            writeWord(WORD_REC_DATA_DELAY, i, recDataDelay[i]);
        }

        writeBits(BITS_STATUS_PAC_DATA_OR, s->getBxOfCoincidence(), true);
        writeWord(WORD_PAC_ENA, 0, s->getPacEna());
        reset();
    }
}


void Pac::enable() {
    resetErrorCounters();
}


void Pac::checkConfigId(DeviceSettings* settings) {
    if (settings == 0) {
        throw NullPointerException("Pac::checkConfigId: settings == 0");
    }
    PacSettings* s = dynamic_cast<PacSettings*>(settings);
    if (s == 0) {
        throw IllegalArgumentException("Pac::checkConfigId: invalid cast for settings");
    }

    uint32_t configId;
    try {
        configId = readWord(WORD_MPAC_CFG_ID, 0);
    } catch (TException& e) {
        std::ostringstream ostr;
        ostr << "Error while reading configId: " << e.what() << ". Device id = " << id << " description = "
        << description << " board name = " << getBoard().getDescription();
        throw TException(ostr.str().c_str());
    }

    uint32_t mask = (1 << memoryMap_->GetItemInfo(WORD_MPAC_CFG_ID).Width) - 1;
    uint32_t expected = s->getPacConfigId() & mask;
    if (expected != (configId & mask)) {
        std::ostringstream ostr;
        ostr << std::hex << "expected configId of chip '" << Name << "': " << expected << ", read: " << configId
        << ". Device id = " << id << " description = " << description << " board name = "
        << getBoard().getDescription();
        throw EBadDeviceVersion(ostr.str(), EBadDeviceVersion::VersionInfo(this, "CONFIG_ID", rpct::toHexString(expected),
                rpct::toHexString(configId)));
    }
    else {
        LOG4CPLUS_DEBUG(logger, "checkConfigId " <<  getDescription() << " expected PacConfigId: " << expected << " read is the same");
        //cout<<"checkConfigId " <<  getDescription() << " expected PacConfigId: " << expected << " read is the same"<<endl;
    }
}


void Pac::monitor(volatile bool *stop) {
    monitor(monitorItems_);
}

void Pac::monitor(MonitorItems& items) {
    warningStatusList_.clear();
    for (MonitorItems::iterator iItem = items.begin(); iItem != items.end(); ++iItem) {
        if ((*iItem) == MonitorItemName::FIRMWARE) {
            string msg = monitorVersion();
            if (msg.size() != 0) {
                warningStatusList_.push_back(MonitorableStatus(MonitorItemName::FIRMWARE, MonitorableStatus::ERROR, msg, 0));
            }
        }
        else if ((*iItem) == MonitorItemName::RECEIVER) {
            uint32_t recErrCnt = readWord(WORD_REC_ERROR_COUNT, 0);
            if (monRecErrorAnalyzer_.newValue(recErrCnt) != MonitorableStatus::OK) {
                warningStatusList_.push_back(monRecErrorAnalyzer_.getStatus());
            }
        }
    }
}

//---------------------------------------------------------------------------
const HardwareItemType GbSort::TYPE(HardwareItemType::cDevice, "TB3GBSORT");

#ifdef CII
#define IID_CLASS GbSort
#include "CII_TB3_GBSORT_iicfg_tab.cpp"
#undef IID_CLASS
#else
#define IID_FILE "RPC_TB3_gbsort.iid"
#define IID_CLASS GbSort
#include "iid2c2.h"
const int GbSort::II_ADDR_SIZE = TriggerBoardDevice::TB_II_ADDR_WIDTH_SOFT;
const int GbSort::II_DATA_SIZE = TriggerBoardDevice::TB_II_DATA_WIDTH;
#endif

GbSort::GbSort(int id, int vme_board_address, int ii_addr, const char* desc,
        IBoard& board, int position): TriggerBoardDevice(
                id,
                TB_GBSORT_IDENTIFIER.c_str(),
                WORD_IDENTIFIER,
                desc,
                TYPE,
                TB_GBSORT_VERSION.c_str(),
                WORD_VERSION,
                WORD_CHECKSUM,
                ii_addr,
                board,
                position,
                BuildMemoryMap(),
                vme_board_address), DiagCtrl(0), DiagnosticReadout(0), PulserDiagCtrl(0), Pulser(0), statisticsDiagCtrl_(0),
                monRecErrorAnalyzer_(MonitorItemName::RECEIVER, 1, 0xffffffff, 0xffffffff,  5, 1000, 0xffffff)
{
    monRecErrorAnalyzer_.setMaxCounterVal((1ull << memoryMap_->GetItemInfo(WORD_REC_ERROR_COUNT).Width) - 1);
    monitorItems_.push_back(MonitorItemName::FIRMWARE);
    monitorItems_.push_back(MonitorItemName::RECEIVER);
}

GbSort::~GbSort()
{
    delete DiagCtrl;
    delete DiagnosticReadout;
    delete PulserDiagCtrl;
    delete Pulser;

    for(size_t i = 0; i < histoManagers_.size(); i++) {
        delete histoManagers_[i];
    }
}

TDiagCtrl& GbSort::GetDiagCtrl() {
    if (DiagCtrl == 0) {
        DiagCtrl = new TDiagCtrl(*this,
                VECT_DAQ,
                BITS_DAQ_PROC_REQ,
                BITS_DAQ_PROC_ACK,
                BITS_DAQ_TIMER_LOC_ENA,
                0,
                BITS_DAQ_TIMER_START,
                BITS_DAQ_TIMER_STOP,
                BITS_DAQ_TIMER_TRIG_SEL,
                WORD_DAQ_TIMER_LIMIT,
                WORD_DAQ_TIMER_COUNT,
                WORD_DAQ_TIMER_TRG_DELAY,
                "DAQ_DIAG_CTRL");
    }
    return *DiagCtrl;
}

TStandardDiagnosticReadout& GbSort::GetDiagnosticReadout() {
    if (DiagnosticReadout == 0) {
        DiagnosticReadout = new TStandardDiagnosticReadout(*this,
                "READOUT",
                &TbGbSortBxDataFactory::getInstance(),
                GetDiagCtrl(),
                VECT_DAQ,
                BITS_DAQ_EMPTY,
                BITS_DAQ_EMPTY_ACK,
                BITS_DAQ_LOST,
                BITS_DAQ_LOST_ACK,
                VECT_DAQ_WR,
                BITS_DAQ_WR_ADDR,
                BITS_DAQ_WR_ACK,
                VECT_DAQ_RD,
                BITS_DAQ_RD_ADDR,
                BITS_DAQ_RD_ACK,
                WORD_DAQ_MASK,
                WORD_DAQ_DATA_DELAY,
                AREA_MEM_DAQ_DIAG,
                DAQ_DIAG_DATA_SIZE,
                DAQ_DIAG_TRIG_NUM,
                DAQ_DIAG_TIMER_SIZE,
                TTC_BCN_EVT_WIDTH);
    }
    return *DiagnosticReadout;
}

TDiagCtrl& GbSort::GetPulserDiagCtrl() {
    if (PulserDiagCtrl == NULL) {
        PulserDiagCtrl = new TDiagCtrl(*this,
                VECT_PULSER,
                BITS_PULSER_PROC_REQ,
                BITS_PULSER_PROC_ACK,
                BITS_PULSER_TIMER_LOC_ENA,
                0,
                BITS_PULSER_TIMER_START,
                BITS_PULSER_TIMER_STOP,
                BITS_PULSER_TIMER_TRIG_SEL,
                WORD_PULSER_TIMER_LIMIT,
                WORD_PULSER_TIMER_COUNT,
                WORD_PULSER_TIMER_TRG_DELAY,
                "PULSER_DIAG_CTRL");
    }

    return *PulserDiagCtrl;
}

TPulser& GbSort::GetPulser() {
    if (Pulser == NULL) {
        Pulser = new GbSortPulser(*this,
                "PULSER",
                GetPulserDiagCtrl(),
                AREA_MEM_PULSE,
                WORD_PULSER_LENGTH,
                BITS_PULSER_REPEAT_ENA,
                BITS_PULSER_OUT_ENA);
    }
    return *Pulser;
}

HistoMgrVector& GbSort::getHistoManagers() {
    if (histoManagers_.empty()) {
        histoManagers_.push_back(new TIIHistoMgr(*this, "RMB_LINKS_RATE", getStatisticsDiagCtrl(), AREA_MEM_RATE));
    }

    return histoManagers_;
}

TDiagCtrl& GbSort::getStatisticsDiagCtrl() {
    if (statisticsDiagCtrl_ == 0) {
        statisticsDiagCtrl_ = new TDiagCtrl(*this,
                VECT_RATE,
                BITS_RATE_PROC_REQ,
                BITS_RATE_PROC_ACK,
                BITS_RATE_TIMER_LOC_ENA,
                0,
                BITS_RATE_TIMER_START,
                BITS_RATE_TIMER_STOP,
                BITS_RATE_TIMER_TRIG_SEL,
                WORD_RATE_TIMER_LIMIT,
                WORD_RATE_TIMER_COUNT,
                0, //WORD_HIST_TIMER_TRG_DELAY <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                "STATISTICS_DIAG_CTRL");
    }
    return *statisticsDiagCtrl_;
}

GbSort::TDiagCtrlVector& GbSort::GetDiagCtrls() {
    if (DiagCtrls.empty()) {
        DiagCtrls.push_back(&GetDiagCtrl());
        DiagCtrls.push_back(&GetPulserDiagCtrl());
        DiagCtrls.push_back(&getStatisticsDiagCtrl());
    }
    return DiagCtrls;
}

GbSort::TDiagVector& GbSort::GetDiags() {
    if (Diags.empty()) {
        Diags.push_back(&GetDiagnosticReadout());
        Diags.push_back(&GetPulser());

        for (unsigned int i = 0; i < histoManagers_.size(); i++) {
            Diags.push_back(histoManagers_[i]);
        }
    }
    return Diags;
}

void GbSort::ConfigureDiagnosable(TTriggerDataSel triggerDataSel, uint32_t dataTrgDelay) {
    writeBits(BITS_STATUS_TRG_DATA_SEL, triggerDataSel);
    writeWord(WORD_DATA_TRG_DELAY, 0, dataTrgDelay);
}

void GbSort::enableTransmissionCheck() {
    bool checkDataEna = true;
    bool checkEna = true;
    if(checkDataEna) {
        writeWord(WORD_REC_CHECK_DATA_ENA, 0, (dynamic_cast<TriggerBoard&>(getBoard())).GetUsedPacs().to_ulong());
        writeWord(WORD_SEND_CHECK_DATA_ENA, 0, uint32_t(1));
    }
    else {
        writeWord(WORD_REC_CHECK_DATA_ENA, 0, uint32_t(0));
        writeWord(WORD_SEND_CHECK_DATA_ENA, 0, uint32_t(0));
    }

    if(checkEna) {
        writeWord(WORD_REC_CHECK_ENA, 0, (dynamic_cast<TriggerBoard&>(getBoard())).GetUsedPacs().to_ulong());
        writeWord(WORD_SEND_CHECK_ENA, 0, uint32_t(1));
    }
    else {
        writeWord(WORD_REC_CHECK_ENA, 0, uint32_t(0));
        writeWord(WORD_SEND_CHECK_ENA, 0, uint32_t(0));
    }
}

void GbSort::resetErrorCounters() {
    writeWord(WORD_REC_ERROR_COUNT, 0, uint32_t(0));
    monRecErrorAnalyzer_.reset();
}

void GbSort::reset() {
    writeBits(GbSort::BITS_STATUS_TTC_DATA_DELAY, FixedHardwareSettings::TBSORT_TTC_DATA_DELAY);//may change after firmware chnge!!!!

    writeWord(GbSort::WORD_REC_CHAN_ENA, 0, (dynamic_cast<TriggerBoard&>(getBoard())).GetUsedPacs().to_ulong());
    writeWord(GbSort::WORD_REC_TEST_ENA, 0, uint32_t(0));
    writeWord(GbSort::WORD_REC_TEST_RND_ENA, 0, uint32_t(0));

    writeWord(GbSort::WORD_SEND_TEST_ENA, 0, uint32_t(0));
    writeWord(GbSort::WORD_SEND_TEST_RND_ENA, 0, uint32_t(0));
    writeWord(GbSort::WORD_SEND_TEST_DATA, 0, uint32_t(0));

    writeBits(GbSort::BITS_PULSER_OUT_ENA, uint32_t(0));

    writeWord(WORD_BCN0_DELAY, 0, FixedHardwareSettings::TBSORT_BC0_DELAY);

    enableTransmissionCheck();
    resetErrorCounters();
}

void GbSort::configure(DeviceSettings* settings, int flags) {
    if (flags & ConfigurationFlags::FORCE_CONFIGURE) {
        if (settings == 0) {
            /*if (flags & ConfigurationFlags::RESET_ONLY_WHEN_NO_CONFIG_DATA) {
                reset();
                return;
            }*/
            throw NullPointerException("GbSort::configure: settings == 0");
        }
        TbGbSortSettings* s = dynamic_cast<TbGbSortSettings*>(settings);
        if (s == 0) {
            throw IllegalArgumentException("GbSort::configure: invalid cast for settings");
        }

        writeWord(WORD_REC_MUX_CLK_INV, 0, s->getRecMuxClkInv());
        writeWord(WORD_REC_MUX_CLK90, 0, s->getRecMuxClk90());
        writeWord(WORD_REC_MUX_REG_ADD, 0, s->getRecMuxRegAdd());

        vector<int>& recMuxDelay = s->getRecMuxDelay();
        for (vector<int>::size_type i = 0; i < recMuxDelay.size(); i++) {
            writeWord(WORD_REC_MUX_DELAY, i, recMuxDelay[i]);
        }

        vector<int>& recDataDelay = s->getRecDataDelay();
        for (vector<int>::size_type i = 0; i < recDataDelay.size(); i++) {
            writeWord(WORD_REC_DATA_DELAY, i, recDataDelay[i]);
        }

        reset();
    }
}

void GbSort::enable() {
    resetErrorCounters();
}

void GbSort::monitor(volatile bool *stop) {
    monitor(monitorItems_);
}

void GbSort::monitor(MonitorItems& items) {
    warningStatusList_.clear();
    for (MonitorItems::iterator iItem = items.begin(); iItem != items.end(); ++iItem) {
        if ((*iItem) == MonitorItemName::FIRMWARE) {
            string msg = monitorVersion();
            if (msg.size() != 0) {
                warningStatusList_.push_back(MonitorableStatus(MonitorItemName::FIRMWARE, MonitorableStatus::ERROR, msg, 0));
            }
        }
        else if ((*iItem) == MonitorItemName::RECEIVER) {
            uint32_t recErrCnt = readWord(WORD_REC_ERROR_COUNT, 0);
            //if (recErrCnt != 0) {
                //    warningStatusList_.push_back(MonitorableStatus(MON_REC_ERR, MonitorableStatus::WARNING, "REC_ERROR = " + rpct::toString(recErrCnt), recErrCnt));
            //}
            if (monRecErrorAnalyzer_.newValue(recErrCnt) != MonitorableStatus::OK) {
                warningStatusList_.push_back(monRecErrorAnalyzer_.getStatus());
            }
        }
    }
}


//---------------------------------------------------------------------------

const HardwareItemType Rmb::TYPE(HardwareItemType::cDevice, "TB3RMB");

const char* Rmb::EVENT_CNT = "EVENT_CNT";

#ifdef CII
#define IID_CLASS Rmb
#include "CII_TB3_RMB_iicfg_tab.cpp"
#undef IID_CLASS
#else
#define IID_FILE "RPC_TB3_rmb.iid"
#define IID_CLASS Rmb
#include "iid2c2.h"
const int Rmb::II_ADDR_SIZE = TriggerBoardDevice::TB_II_ADDR_WIDTH_SOFT;
const int Rmb::II_DATA_SIZE = TriggerBoardDevice::TB_II_DATA_WIDTH;
#endif

Rmb::Rmb(int id, int vme_board_address, int ii_addr, const char* desc,
        IBoard& board, int position)
: TriggerBoardDevice(
        id,
        TB_RMB_IDENTIFIER.c_str(),
        WORD_IDENTIFIER,
        desc,
        TYPE,
        TB_RMB_VERSION.c_str(),
        WORD_VERSION,
        WORD_CHECKSUM,
        ii_addr,
        board,
        position,
        BuildMemoryMap(),
        vme_board_address), DiagCtrl(0), DiagnosticReadout(0), i2c_(0), golI2c_(0), statisticsDiagCtrl_(0),
        monRecErrorAnalyzer_(MonitorItemName::RECEIVER, 1, 0xffffffff, 0xffffffff,  5, 1000, 0xffffff)
{
    monRecErrorAnalyzer_.setMaxCounterVal((1ull << memoryMap_->GetItemInfo(WORD_REC_ERROR_COUNT).Width) - 1);
    monitorItems_.push_back(MonitorItemName::FIRMWARE);
    monitorItems_.push_back(MonitorItemName::RECEIVER);
    monitorItems_.push_back(MonitorItemName::RMB_DATA);
    monitorItems_.push_back(MonitorItemName::RMB_ALGORITHM);
    monitorItems_.push_back(EVENT_CNT);
}

Rmb::~Rmb()
{
    delete DiagCtrl;
    delete DiagnosticReadout;
    delete golI2c_;
    delete i2c_;

    for(size_t i = 0; i < histoManagers_.size(); i++) {
        delete histoManagers_[i];
    }
}

TDiagCtrl& Rmb::GetDiagCtrl() {
    if (DiagCtrl == 0) {
        DiagCtrl = new TDiagCtrl(*this,
                VECT_DAQ,
                BITS_DAQ_PROC_REQ,
                BITS_DAQ_PROC_ACK,
                BITS_DAQ_TIMER_LOC_ENA,
                0,
                BITS_DAQ_TIMER_START,
                BITS_DAQ_TIMER_STOP,
                BITS_DAQ_TIMER_TRIG_SEL,
                WORD_DAQ_TIMER_LIMIT,
                WORD_DAQ_TIMER_COUNT,
                WORD_DAQ_TIMER_TRG_DELAY,
                "DAQ_DIAG_CTRL");
    }
    return *DiagCtrl;
}

TStandardDiagnosticReadout& Rmb::GetDiagnosticReadout() {
    if (DiagnosticReadout == 0) {
        DiagnosticReadout = new TStandardDiagnosticReadout(*this,
                "READOUT",
                &RmbBxDataFactory::getInstance(),
                GetDiagCtrl(),
                VECT_DAQ,
                BITS_DAQ_EMPTY,
                BITS_DAQ_EMPTY_ACK,
                BITS_DAQ_LOST,
                BITS_DAQ_LOST_ACK,
                VECT_DAQ_WR,
                BITS_DAQ_WR_ADDR,
                BITS_DAQ_WR_ACK,
                VECT_DAQ_RD,
                BITS_DAQ_RD_ADDR,
                BITS_DAQ_RD_ACK,
                WORD_DAQ_MASK,
                WORD_DAQ_DATA_DELAY,
                AREA_MEM_DAQ_DIAG,
                DAQ_DIAG_DATA_SIZE,
                DAQ_DIAG_TRIG_NUM,
                DAQ_DIAG_TIMER_SIZE,
                TTC_BCN_EVT_WIDTH);
    }
    return *DiagnosticReadout;
}

HistoMgrVector& Rmb::getHistoManagers() {
    if (histoManagers_.empty()) {
        histoManagers_.push_back(new TIIHistoMgr(*this, "RMB_LINKS_RATE", getStatisticsDiagCtrl(), AREA_MEM_RATE));
    }

    return histoManagers_;
}

TDiagCtrl& Rmb::getStatisticsDiagCtrl() {
    if (statisticsDiagCtrl_ == 0) {
        statisticsDiagCtrl_ = new TDiagCtrl(*this,
                VECT_RATE,
                BITS_RATE_PROC_REQ,
                BITS_RATE_PROC_ACK,
                BITS_RATE_TIMER_LOC_ENA,
                0,
                BITS_RATE_TIMER_START,
                BITS_RATE_TIMER_STOP,
                BITS_RATE_TIMER_TRIG_SEL,
                WORD_RATE_TIMER_LIMIT,
                WORD_RATE_TIMER_COUNT,
                0, //WORD_HIST_TIMER_TRG_DELAY <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                "STATISTICS_DIAG_CTRL");
    }
    return *statisticsDiagCtrl_;
}

Rmb::TDiagCtrlVector& Rmb::GetDiagCtrls() {
    if (DiagCtrls.empty()) {
        DiagCtrls.push_back(&GetDiagCtrl());
        DiagCtrls.push_back(&getStatisticsDiagCtrl());
    }
    return DiagCtrls;
}

Rmb::TDiagVector& Rmb::GetDiags() {
    if (Diags.empty()) {
        Diags.push_back(&GetDiagnosticReadout());

        for (unsigned int i = 0; i < histoManagers_.size(); i++) {
            Diags.push_back(histoManagers_[i]);
        }
    }
    return Diags;
}

void Rmb::ConfigureDiagnosable(TTriggerDataSel triggerDataSel, uint32_t dataTrgDelay) {
    writeBits(BITS_STATUS_TRG_DATA_SEL, triggerDataSel);
    writeWord(WORD_DATA_TRG_DELAY, 0, dataTrgDelay);
}

void Rmb::enableTransmissionCheck() {
    bool checkDataEna = true;
    bool checkEna = true;
    if(checkDataEna) {
        writeWord(WORD_REC_CHECK_DATA_ENA, 0, 0x3ffff);
    }
    else {
        writeWord(WORD_REC_CHECK_DATA_ENA, 0, uint32_t(0));
    }

    if(checkEna) {
        writeWord(WORD_REC_CHECK_ENA, 0, 0x3ffff);
    }
    else {
        writeWord(WORD_REC_CHECK_ENA, 0, uint32_t(0));
    }
}

void Rmb::resetErrorCounters() {
    writeWord(WORD_REC_ERROR_COUNT, 0, uint32_t(0));
    monRecErrorAnalyzer_.reset();

/*    readWord(WORD_RMB_CHMB3_ERR_CNT, 0);
    readWord(WORD_RMB_DDM30_ERR_CNT, 0);
    readWord(WORD_RMB_DDM31_ERR_CNT, 0);

    for(int iLink = 0; iLink < 18; iLink++) {
        readWord(WORD_RMB_DEL_ERR_CNT, iLink);
        readWord(WORD_RMB_DDM_ERR_CNT, iLink);
    }*/
}

void Rmb::resetRmb() {
    writeBits(BITS_STATUS_RMB_RESET , 1);
    tbsleep(100);
    writeBits(BITS_STATUS_RMB_RESET , 0);
}

void Rmb::reset() {
    writeBits(Rmb::BITS_STATUS_TTC_DATA_DELAY, FixedHardwareSettings::RMB_TTC_DATA_DELAY); //<<<may chnge after firmewe chnged

    //writeWord(Rmb::WORD_REC_TEST_ENA, 0, ~((dynamic_cast<TriggerBoard&>(getBoard())).GetUsedOptos().to_ulong()));
    writeWord(Rmb::WORD_REC_TEST_ENA, 0, uint32_t(0));
    writeWord(Rmb::WORD_REC_TEST_RND_ENA, 0, uint32_t(0));

    writeBits(BITS_STATUS_RMB_TEST_MODE, 0);
    writeBits(BITS_STATUS_BER_TEST_MODE, 0);
    writeBits(BITS_STATUS_BER_TEST_RUN, 0);

    writeWord(WORD_RMB_BCN0_DELAY, 0, 1ul);

    writeWord(WORD_BCN0_DELAY, 0, FixedHardwareSettings::RMB_BC0_DELAY);

    writeBits(BITS_STATUS_TRG_RMB_SEL, IDiagnosable::tdsL1A);

    writeBits(BITS_STATUS_GOH_CLK_INV , 1);

    writeBits(BITS_STATUS_RMB_RESET , 1);
    tbsleep(100);
    writeBits(BITS_STATUS_RMB_RESET , 0);
    tbsleep(100);
    writeBits(BITS_STATUS_GOH_RESET, 1);
    tbsleep(200);
    writeBits(BITS_STATUS_GOH_RESET, 0);
    tbsleep(200);
    if(readBits(BITS_STATUS_GOH_READY) != 1)
        throw TException(string("BITS_STATUS_GOH_READY is not 1 after goh reset on ") + getDescription());

    const unsigned char LD_CURRENT = 0x20;

    try {
    	if(golI2c_ != 0) {
    		delete golI2c_; //to force reinitialization of the golI2c_
    		golI2c_ = 0;

    		delete i2c_;
    		i2c_ = 0;
    	}
        getGolI2c().WriteConfig3(LD_CURRENT, true);
        // LOG4CPLUS_WARN(logger, "Setting GOL config3 disabled!!!!!!!!!!!!!. " );
    }
    catch (EI2C& e) {
        // try once again
        LOG4CPLUS_WARN(logger, "I2C error during setings GOL config3. " << e.what() << ". Trying once again.");
        getGolI2c().WriteConfig3(LD_CURRENT, true);
    }

    enableTransmissionCheck();
    resetErrorCounters();
}

void Rmb::configure(DeviceSettings* settings, int flags) {
    if (flags & ConfigurationFlags::FORCE_CONFIGURE) {
        if (settings == 0) {
            /*if (flags & ConfigurationFlags::RESET_ONLY_WHEN_NO_CONFIG_DATA) {
                writeWord(WORD_RMB_PRETRG_VAL, 0, 1ul); //defoult values
                writeWord(WORD_RMB_POSTTRG_VAL, 0, 5ul);
                writeWord(WORD_RMB_DATA_DELAY, 0, FixedHardwareSettings::RMB_DATA_DELAY);
                reset();
                return;
            }*/
            throw NullPointerException("Rmb::configure: settings is null");
        }
        RmbSettings* s = dynamic_cast<RmbSettings*>(settings);
        if (s == 0) {
            throw IllegalArgumentException("Rmb::configure: invalid cast for settings");
        }
        writeWord(WORD_RMB_TRG_DELAY, 0, s->getTrgDelay()); // to w zasadzie powinno byc zawsze 0
        writeWord(WORD_RMB_DATA_DELAY, 0, s->getDataDelay());
        writeWord(WORD_RMB_PRETRG_VAL, 0, s->getPreTriggerVal());
        writeWord(WORD_RMB_POSTTRG_VAL, 0, s->getPostTriggerVal());

        writeWord(WORD_RMB_CHAN_ENA, 0, s->getChanEna());
        LOG4CPLUS_INFO(logger, " rmbChanEna "<< getDescription()<<" "<<getId()<<" "<<hex<< s->getChanEna());

        writeWord(WORD_REC_MUX_CLK_INV, 0, s->getRecMuxClkInv());
        writeWord(WORD_REC_MUX_CLK90, 0, s->getRecMuxClk90());
        writeWord(WORD_REC_MUX_REG_ADD, 0, s->getRecMuxRegAdd());

        vector<int>& recMuxDelay = s->getRecMuxDelay();
        for (vector<int>::size_type i = 0; i < recMuxDelay.size(); i++) {
            writeWord(WORD_REC_MUX_DELAY, i, recMuxDelay[i]);
        }

        vector<int>& recDataDelay = s->getRecDataDelay();
        for (vector<int>::size_type i = 0; i < recDataDelay.size(); i++) {
            writeWord(WORD_REC_DATA_DELAY, i, recDataDelay[i]);
        }

        reset(); //as the rmb algorithm is reset here, it should be done after the setup
    }
    else {
        resetRmb();
    }
}

void Rmb::enable() {
    resetErrorCounters();
    //resetRmb();
}

void Rmb::monitor(volatile bool *stop) {
    monitor(monitorItems_);
}

void Rmb::monitor(MonitorItems& items) {
    warningStatusList_.clear();
    for (MonitorItems::iterator iItem = items.begin(); iItem != items.end(); ++iItem) {
        if ((*iItem) == MonitorItemName::FIRMWARE) {
            string msg = monitorVersion();
            if (msg.size() != 0) {
                warningStatusList_.push_back(MonitorableStatus(MonitorItemName::FIRMWARE, MonitorableStatus::ERROR, msg, 0));
            }
        }
        else if ((*iItem) == MonitorItemName::RECEIVER) {
            uint32_t errCnt = readWord(WORD_REC_ERROR_COUNT, 0);

            if (monRecErrorAnalyzer_.newValue(errCnt) != MonitorableStatus::OK) {
                warningStatusList_.push_back(monRecErrorAnalyzer_.getStatus());
            }
        }
        else if ((*iItem) == MonitorItemName::RMB_DATA) {
        	string msg = "";
        	int monStatus = MonitorableStatus::OK;

        	uint32_t errCnt = readWord(WORD_RMB_CHMB3_ERR_CNT, 0);
            if (errCnt != 0) {
                msg = "CHMB3_ERR = " + rpct::toString(errCnt) +";";
                if(errCnt > 10)
                	monStatus = MonitorableStatus::WARNING;
                else
                	monStatus = MonitorableStatus::SOFT_WARNING;
            }

            /* this monitorable have no sense, as the frames with not correct delay are detected just after the reset
             * one should record the errors cnt during enable, and then look if it is not growing
             * for(int iLink = 0; iLink < 18; iLink++) {
                errCnt = readWord(WORD_RMB_DEL_ERR_CNT, iLink);
                if (errCnt != 0) {
                    msg += " DEL_ERR [" + rpct::toString(iLink) + "] = " + rpct::toString(errCnt) +";";
                    if(errCnt > 10)
                    	monStatus = MonitorableStatus::WARNING;
                    else
                    	monStatus = MonitorableStatus::SOFT_WARNING;
                }
            }*/

            if(monStatus != MonitorableStatus::OK)
            	warningStatusList_.push_back(MonitorableStatus(MonitorItemName::RMB_DATA, monStatus, msg, 0));
        }
        else if ((*iItem) == MonitorItemName::RMB_ALGORITHM) {
        	string msg = "";
        	int monStatus = MonitorableStatus::OK;

        	/*DDM 30 to katastroficzne przepenienie kolejki triggerw, a DDM31 to dostanie triggera mimo wystawienia BUSY.
        	 DDM31  this means, that the chamber data for this trigger have been discarded, due to the
        		danger of trigger queue overflow. This marker is being sent in the appropriate BX data.
     	 	 DDM30  this value means, that the trigger queue in the RMB was filled completely, when
        		the new trigger arrived. So all the event information have been discarded, which leads
        		to the data corruption. This markes is sent as "out of band data", so it may appear in
        		the data stream in the middle of correct data associated with the previous events. It is
        		very likely however, that the link where the problem occurred will be blocked by DCC.
        		and what is DDM?
        	 * */
        	uint32_t errCnt = readWord(WORD_RMB_DDM30_ERR_CNT, 0);
            if (errCnt != 0) {
                msg += " DDM30_ERR = " + rpct::toString(errCnt) +";";
                if(errCnt > 10)
                	monStatus = MonitorableStatus::WARNING;
                else
                	monStatus = MonitorableStatus::SOFT_WARNING;
            }

            errCnt = readWord(WORD_RMB_DDM31_ERR_CNT, 0);
            if (errCnt != 0) {
                msg += " DDM31_ERR = " + rpct::toString(errCnt) +";";
                if(errCnt > 10)
                	monStatus = MonitorableStatus::WARNING;
                else
                	monStatus = MonitorableStatus::SOFT_WARNING;
            }

            for(int iLink = 0; iLink < 18; iLink++) {
                errCnt = readWord(WORD_RMB_DDM_ERR_CNT, iLink);
                if (errCnt != 0) {
                    msg += " DDM_ERR [" + rpct::toString(iLink) + "] = " + rpct::toString(errCnt) +";";
                    if(errCnt > 10)
                    	monStatus = MonitorableStatus::WARNING;
                    else
                    	monStatus = MonitorableStatus::SOFT_WARNING;
                }
            }

            if(monStatus != MonitorableStatus::OK)
            	warningStatusList_.push_back(MonitorableStatus(MonitorItemName::RMB_ALGORITHM, monStatus, msg, 0));

        }
        else if ((*iItem) == EVENT_CNT) {
            uint32_t evnCnt = readWord(WORD_TTC_EVN, 0);
            LOG4CPLUS_INFO(logger, getBoard().getDescription()<<" RMB EVENT_CNT "<<evnCnt);
        }
    }
}

TI2CInterface& Rmb::getI2c() {
    if (i2c_ == 0) {
        LOG4CPLUS_INFO(logger, "creating i2c");
        i2c_ = new TIII2CMasterCorev09(*this, AREA_I2C, false);
        //i2c_->SetPrescaleValue(2);
        i2c_->SetPrescaleValue(128);
        i2c_->Enable();
        LOG4CPLUS_INFO(logger, "created i2c");
    }
    return *i2c_;
}

TGolI2C& Rmb::getGolI2c() {
    if (golI2c_ == 0) {
        LOG4CPLUS_INFO(logger, "creating golI2c_");
        const uint32_t GOL_I2C_ADDR = 16;
        writeWord(WORD_I2C_ADDR, 0,GOL_I2C_ADDR);
        golI2c_ = new TGolI2C(GOL_I2C_ADDR);
        //golI2c_ = new TGolI2C(8);
        golI2c_->SetI2CInterface(getI2c());
        LOG4CPLUS_INFO(logger, "created golI2c_");

        // some tests
        LOG4CPLUS_INFO(logger, "Status0=" << hex << ((int)golI2c_->ReadStatus0())
                << " Config0=" << hex << ((int)golI2c_->ReadConfig0())
                << " Config1=" << hex << ((int)golI2c_->ReadConfig1()));

    }
    return *golI2c_;
}

//---------------------------------------------------------------------------

const HardwareItemType RmbMH::TYPE(HardwareItemType::cDevice, "TB3RMBMH");

#define IID_CLASS RmbMH
#include "CII_TB3_RMB_MH_iicfg_tab.cpp"
#include <rpct/ii/ConfigurationFlags.h>
using namespace rpct;
using namespace rpct;
#undef IID_CLASS

RmbMH::RmbMH(int id, int vme_board_address, int ii_addr, const char* desc,
        IBoard& board, int position): TriggerBoardDevice(
                id,
                TB_RMB_IDENTIFIER.c_str(),
                WORD_IDENTIFIER,
                desc,
                TYPE,
                TB_RMB_VERSION.c_str(),
                WORD_VERSION,
                WORD_CHECKSUM,
                ii_addr,
                board,
                position,
                BuildMemoryMap(),
                vme_board_address)
                {
                }

RmbMH::~RmbMH()
{
}

void RmbMH::reset() {

    /*    writeBits(Rmb::BITS_STATUS_TTC_DATA_DELAY, FixedHardwareSettings::RMB_TTC_DATA_DELAY); //<<<may chnge after firmewe chnged

                 writeWord(Rmb::WORD_REC_TEST_ENA, 0, uint32_t(0));
                 writeWord(Rmb::WORD_REC_TEST_RND_ENA, 0, uint32_t(0));

                 writeBits(BITS_STATUS_RMB_TEST_MODE, 0);
                 writeBits(BITS_STATUS_BER_TEST_MODE, 0);
                 writeBits(BITS_STATUS_BER_TEST_RUN, 0);

                 writeWord(WORD_RMB_BCN0_DELAY, 0, 1ul);

                 writeWord(WORD_BCN0_DELAY, 0, FixedHardwareSettings::RMB_BC0_DELAY);

                 writeBits(BITS_STATUS_TRG_RMB_SEL, IDiagnosable::tdsL1A);

                 writeBits(BITS_STATUS_RMB_RESET , 1);
                 tbsleep(100);
                 writeBits(BITS_STATUS_RMB_RESET , 0);
                 tbsleep(100);
                 writeBits(BITS_STATUS_GOH_RESET, 1);
                 tbsleep(100);
                 writeBits(BITS_STATUS_GOH_RESET, 0);

                 //trzeba bedize to stad wyrzucic, bo reset jest robiony w confiore  na koncu
                 writeWord(WORD_RMB_PRETRG_VAL, 0, 2ul);
                 writeWord(WORD_RMB_POSTTRG_VAL, 0, 4ul);
                 writeWord(WORD_RMB_DATA_DELAY, 0, 92); // 62 is the value for the trigger from the TC
     */
}

void RmbMH::configure(DeviceSettings* settings, int flags) {
    /*if (settings == 0) {
                 throw NullPointerException("Rmb::configure: settings is null");
                 }
                 RmbSettings* s = dynamic_cast<RmbSettings*>(settings);
                 if (s == 0) {
                 throw IllegalArgumentException("Rmb::configure: invalid cast for settings");
                 }
                 writeWord(WORD_RMB_TRG_DELAY, 0, s->getTrgDelay()); // to w zasadzie powinno byc zawsze 0
                 writeWord(WORD_RMB_DATA_DELAY, 0, s->getDataDelay());
                 writeWord(WORD_RMB_PRETRG_VAL, 0, s->getPreTriggerVal());
                 writeWord(WORD_RMB_POSTTRG_VAL, 0, s->getPostTriggerVal());

                 writeWord(WORD_RMB_CHAN_ENA, 0, s->getChanEna());
                 LOG4CPLUS_INFO(logger, " rmbChanEna "<< getDescription()<<" "<<getId()<<" "<<hex<< s->getChanEna());

                 writeWord(WORD_REC_MUX_CLK_INV, 0, s->getRecMuxClkInv());
                 writeWord(WORD_REC_MUX_CLK90, 0, s->getRecMuxClk90());
                 writeWord(WORD_REC_MUX_REG_ADD, 0, s->getRecMuxRegAdd());

                 vector<int>& recMuxDelay = s->getRecMuxDelay();
                 for (vector<int>::size_type i = 0; i < recMuxDelay.size(); i++) {
                 writeWord(WORD_REC_MUX_DELAY, i, recMuxDelay[i]);
                 }

                 vector<int>& recDataDelay = s->getRecDataDelay();
                 for (vector<int>::size_type i = 0; i < recDataDelay.size(); i++) {
                 writeWord(WORD_REC_DATA_DELAY, i, recDataDelay[i]);
                 }*/

    reset(); //as the rmb algorithm is reset here, it should be done after the setup
    //writeWord(WORD_BCN0_DELAY, 0, Globals::PAC_BCN0_DELAY);
}

//---------------------------------------------------------------------------

Logger TriggerBoard::logger = Logger::getInstance("TriggerBoard");
void TriggerBoard::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}
const HardwareItemType TriggerBoard::TYPE(HardwareItemType::cBoard, "TB3");

TriggerBoard::TriggerBoard(int ident, const char* desc, TVMEInterface* vme, int vme_board_address, VmeCrate* c, int pos)
: BoardBase(ident, desc, TYPE, c, pos), vme_(vme), triggerBoardVme_(vme_board_address, *this),
triggerBoardControl_(vme_board_address, *this, 0), vmeAddress_(vme_board_address) {
    cout << vme_board_address << endl;
    for(unsigned int i = 0; i < 3; i++)
    	pacs_.push_back(0);
    Init(true);
    triggerBoardVme_.getMemoryMap().SetHardwareIfc(
            new TIIVMEAccess(
                    triggerBoardVme_.GetVMEBoardBaseAddr(),
                    triggerBoardVme_.getBaseAddress() << triggerBoardVme_.GetVMEBaseAddrShift(),
                    vme,
                    0xf8000),
                    true);
    triggerBoardControl_.getMemoryMap().SetHardwareIfc(
            new TIIVMEAccess(
                    triggerBoardControl_.GetVMEBoardBaseAddr(),
                    triggerBoardControl_.getBaseAddress() << triggerBoardControl_.GetVMEBaseAddrShift(),
                    vme,
                    0),
                    true);

    optos_.assign(6, (Opto*)0);
    //LdPacs.assign(4, NULL);
    gbSort_ = 0;
    rmb_ = 0;

    usedOptos_ = boost::dynamic_bitset<>(6, 0ul);
    usedPacs_ = boost::dynamic_bitset<>(4, 0ul);;
}

/*
             TriggerBoard::TriggerBoard(const char* address, int port, int vme_board_address, TCrate* crate)
             : TrgVme(vme_board_address, *this), TrgControl(vme_board_address, *this),
             VMEAddress(vme_board_address), Crate(crate)
             {
             Init(false);
             TrgControl.getMemoryMap().SetHardwareIfc(
             new TIITcpAccess(
             TrgControl.getBaseAddress() << TrgControl.GetVMEBaseAddrShift(),
             address,
             port),
             true);
             } */

TriggerBoard::~TriggerBoard()
{
    for (Devices::iterator iDevice = devices_.begin(); iDevice != devices_.end(); ++iDevice)
        delete dynamic_cast<Opto*>(*iDevice);
}

int TriggerBoard::getTowerNum(unsigned int pacNum) {
    int tbNum = getNumber();
    int towerNum = 0;
    if(tbNum == 0) {
        towerNum = -16 + pacNum;
    }
    else if(tbNum == 1) {
    	towerNum = -12 + pacNum;
    }
    else if(tbNum == 2) {
    	towerNum = -8 + pacNum;
    }
    else if(tbNum == 3) {
    	towerNum = -4 + pacNum;
    }
    else if(tbNum == 4) {
    	towerNum = -1 + pacNum;
    }
    else if(tbNum == 5) {
    	towerNum =  2 + pacNum;
    }
    else if(tbNum == 6) {
    	towerNum =  5 + pacNum;
    }
    else if(tbNum == 7) {
    	towerNum =  9 + pacNum;
    }
    else if(tbNum == 8) {
    	towerNum = 13 + pacNum;
    }

    return towerNum;
}

void TriggerBoard::Init(bool use_vme) {
    //std::ostringstream ost;
    //ost << "TRG " << std::hex << VMEAddress;
    //Description = ost.str();

    /*IE_RESET_II = "Reset II";
                 IE_INIT_TTC = "Init TTC";
                 //IE_SET_CONTROL_CLOCK = "Set UNI_CONTROL clock and STATUS_TRG_SEL";
                 //IE_SET_OPTO_CLOCK = "Set UNI_OPTO clock";
                 IE_INIT_TLK = "Initialize TLK";
                 //IE_RESET_DELAY = "Reset delay";
                 IE_INIT_QPLL = "Init QPLL";
                 //IE_INIT_OPTO = "Init Opto";
                 InitElements.push_back(IE_RESET_II);
                 InitElements.push_back(IE_INIT_TTC);
                 //InitElements.push_back(IE_SET_CONTROL_CLOCK);
                 //InitElements.push_back(IE_SET_OPTO_CLOCK);
                 //InitElements.push_back(IE_INIT_TLK); <<!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!!!!!!!!! KB
                 //InitElements.push_back(IE_RESET_DELAY);
                 InitElements.push_back(IE_INIT_QPLL);
                 //InitElements.push_back(IE_INIT_OPTO);*/

    if (use_vme) {
        devices_.push_back(&triggerBoardVme_);
        devices_.push_back(&triggerBoardControl_);
    }
    else {
        devices_.push_back(&triggerBoardControl_);
    }
}

IDevice& TriggerBoard::addChip(string type, int pos, int id) {

    transform(type.begin(), type.end(), type.begin(), ::toupper);

    ostringstream ostr;
    ostr << type << ' ' << pos;
    string chipName = ostr.str();

    if (type == string("OPTO")) {
        Opto* chip = new Opto(id, vmeAddress_, pos, chipName.c_str(), *this, pos);

        chip->getMemoryMap().SetHardwareIfc(
                new TIIVMEAccess(
                        chip->GetVMEBoardBaseAddr(),
                        chip->getBaseAddress() << chip->GetVMEBaseAddrShift(),
                        vme_,
                        0),
                        true);
        devices_.push_back(chip);
        optos_[chip->getNumber()] = chip;
        usedOptos_[chip->getNumber()] = true;
        return *chip;
    }

    if (type == string("LDPAC") || type == string("PAC")) {
        Pac* chip = new Pac(id, vmeAddress_, pos, chipName.c_str(), *this, pos);

        chip->getMemoryMap().SetHardwareIfc(
                new TIIVMEAccess(
                        chip->GetVMEBoardBaseAddr(),
                        chip->getBaseAddress() << chip->GetVMEBaseAddrShift(),
                        vme_,
                        0),
                        true);
        devices_.push_back(chip);
        if(chip->getNumber() == 3 && pacs_.size() == 3) {
        	pacs_.push_back(chip);
        }
        else if (chip->getNumber() < 3) {
        	pacs_[chip->getNumber()] = chip;
        }
        else {
        	ostringstream ostr;
        	ostr<<__FILE__<<":"<<__LINE__<<" invalid PAC number "<<chip->getNumber();
        	throw TException(ostr.str());
        }
        usedPacs_[chip->getNumber()] = true;
        return *chip;
    }

    if (type == string("GBSORT")) {
        GbSort* chip = new GbSort(id, vmeAddress_, pos, chipName.c_str(), *this, pos);

        chip->getMemoryMap().SetHardwareIfc(
                new TIIVMEAccess(
                        chip->GetVMEBoardBaseAddr(),
                        chip->getBaseAddress() << chip->GetVMEBaseAddrShift(),
                        vme_,
                        0),
                        true);
        devices_.push_back(chip);
        gbSort_ = chip;
        return *chip;
    }

    if (type == string("RMB")) {
        Rmb* chip = new Rmb(id, vmeAddress_, pos, chipName.c_str(), *this, pos);

        chip->getMemoryMap().SetHardwareIfc(
                new TIIVMEAccess(
                        chip->GetVMEBoardBaseAddr(),
                        chip->getBaseAddress() << chip->GetVMEBaseAddrShift(),
                        vme_,
                        0),
                        true);
        devices_.push_back(chip);
        rmb_ = chip;
        return *chip;
    }

    if (type == string("RMBMH")) {
        RmbMH* chip = new RmbMH(id, vmeAddress_, pos, chipName.c_str(), *this, pos);

        chip->getMemoryMap().SetHardwareIfc(
                new TIIVMEAccess(
                        chip->GetVMEBoardBaseAddr(),
                        chip->getBaseAddress() << chip->GetVMEBaseAddrShift(),
                        vme_,
                        0),
                        true);
        devices_.push_back(chip);
        //rmb_ = chip;
        return *chip;
    }

    throw TException(string("Ivalid chip type '") + type + "'");
}
/*
             void TriggerBoard::InitializeElement(std::string elem) {
             LOG4CPLUS_DEBUG(logger, "Initializing " <<  getDescription() << " element " << elem);
             if (elem == IE_RESET_II) {
             Vme.writeWord(TriggerBoardVme::WORD_RESET, 0, 0x13);
             }
             else if (elem == IE_INIT_TTC) {
             Control.InitTTC();
             }
             else if (elem == IE_INIT_TLK) {
             for (Devices::iterator iDevice = devices.begin(); iDevice != devices.end(); ++iDevice) {
             Opto* opto = dynamic_cast<Opto*>(*iDevice);
             if (opto != 0) {
             for (int i = 0; i < opto->GetChannelCount(); i++) {
             opto->GetChannel(i).GetTlk().Init();
             }
             }
             }
             }
             else if (elem == IE_INIT_QPLL) {
             Control.ResetQPLL();
             }
             else
             throw TException("Unknown init element '" + elem + "'.");
             }*/

void TriggerBoard::configure(ConfigurationSet* configurationSet, int flags) {
    if (flags & ConfigurationFlags::FORCE_CONFIGURE) {
        //triggerBoardVme_.writeWord(TriggerBoardVme::WORD_RESET, 0, 0x13); // reset calej plyty TB
        //triggerBoardControl_.InitTTC(); TTCrx is not usd, /we are using the TTC signals and clock from the TC

        /*for (Devices::iterator iDevice = devices_.begin(); iDevice != devices_.end(); ++iDevice) {
                     Opto* opto = dynamic_cast<Opto*>(*iDevice);
                     if (opto != 0) {
                     for (int i = 0; i < opto->GetChannelCount(); i++) {
                     opto->GetChannel(i).GetTlk().Init();
                     }
                     }
                     }*/
        //triggerBoardControl_.ResetQPLL();
        //triggerBoardControl_.writeBits(TriggerBoardControl::BITS_TTC_TC_ENA, 1); //we are using the TTC signals and clock from the TC, not from the TB TTCrx, bacause it does not work
    }

    BoardBase::configure(configurationSet, flags);
}


void TriggerBoard::checkConfigId(ConfigurationSet* configurationSet) {
    std::ostringstream ost;
    EBadDeviceVersion::VersionInfoList errorList;

    const Devices& devices = getDevices();
    for (Devices::const_iterator iDevice = devices.begin(); iDevice != devices.end(); ++iDevice) {
        Pac* pac = dynamic_cast<Pac*>(*iDevice);
        if (pac != 0) {
            try {
                pac->checkConfigId(configurationSet->getDeviceSettings(*pac));
            }
            catch (EBadDeviceVersion& e) {
                ost << e.what() << "\n";
                errorList.insert(errorList.end(), e.getVersionInfoList().begin(), e.getVersionInfoList().end());
            }
        }
    }


    if (errorList.size() > 0) {
        throw EBadDeviceVersion(ost.str(), errorList);
    }
}

bool TriggerBoard::CheckOptLinksSynchronization() {
    bool good = true;
    for (Optos::iterator optoIt = optos_.begin(); optoIt != optos_.end(); optoIt++) {
        if((*optoIt) == NULL)
            continue;
        (*optoIt)->writeWord(Opto::WORD_REC_CHECK_DATA_ENA, 0, uint32_t(0));
        (*optoIt)->writeWord(Opto::WORD_REC_CHECK_ENA, 0, uint32_t(0));
        if( (*optoIt)->CheckLinksSynchronization() == false ) {
            good = false;
            //throw TException(" opto links synchronization failed");
            LOG4CPLUS_ERROR(logger, "opto links synchronization failed on: tb " << getId() << " opto " << (*optoIt)->getId());
        }
    }
    return false;
}

bool TriggerBoard::CheckLinksOperation() {
    bool good = true;
    for (Optos::iterator optoIt = optos_.begin(); optoIt != optos_.end(); optoIt++) {
        if((*optoIt) == NULL)
            continue;

        if( (*optoIt)->CheckLinksOperation() == false ) {
            good = false;
            //throw TException(" CheckLinksOperation failed");
            LOG4CPLUS_ERROR(logger, "CheckLinksOperation failed on: tb " << getId() << " opto " << (*optoIt)->getId());
        }
    }
    return false;
}

bool TriggerBoard::FindOptLinksTLKDelay(unsigned int testData) {
    bool good = true;
    for (Optos::iterator optoIt = optos_.begin(); optoIt != optos_.end(); optoIt++) {
        if((*optoIt) == NULL)
            continue;

        (*optoIt)->writeWord(Opto::WORD_REC_CHECK_DATA_ENA, 0, uint32_t(0));
        (*optoIt)->writeWord(Opto::WORD_REC_CHECK_ENA, 0, uint32_t(0));
        if( (*optoIt)->FindOptLinksTLKDelay(testData) == false ) {
            good = false;
            LOG4CPLUS_ERROR(logger, "FindOptLinksTLKDelay failed on: tb " << getId() << " opto " << (*optoIt)->getId());
        }
    }
    return false;
}

bool TriggerBoard::FindOptLinksDelay() {
    bool good = true;
    for (Optos::iterator optoIt = optos_.begin(); optoIt != optos_.end(); optoIt++) {
        if((*optoIt) == NULL)
            continue;

        //(*optoIt)->writeBits(Opto::BITS_ERROR_RX_ER , (*optoIt)->GetEnabledOptLinks().to_ulong()); //jak 1 to liczy blady z TLK
        //(*optoIt)->writeBits(Opto::BITS_ERROR_RX_DV , (*optoIt)->GetEnabledOptLinks().to_ulong()); //jak 1 to liczy brak DV  (Data Valid) z TLK
        //(*optoIt)->writeBits(Opto::BITS_ERROR_REC_ERR , (*optoIt)->GetEnabledOptLinks().to_ulong());  //jak 1 to liczy bledy analizy danych

        (*optoIt)->writeWord(Opto::WORD_REC_CHECK_DATA_ENA, 0, (*optoIt)->GetEnabledOptLinks().to_ulong());
        (*optoIt)->writeWord(Opto::WORD_REC_CHECK_ENA, 0, (*optoIt)->GetEnabledOptLinks().to_ulong());

        if( (*optoIt)->FindOptLinksDelay() == false ) {
            good = false;
            //throw TException(" opto links synchronization failed");
            LOG4CPLUS_ERROR(logger, "FindOptLinksDelay failed on: tb " << getId() << " opto " << (*optoIt)->getId());
        }
    }
    return false;
}

void TriggerBoard::EnableTransmissionCheck(bool enableBCNcheck, bool enableDataCheck) {
    bool checkDataEna = enableBCNcheck;
    bool checkEna = enableDataCheck;

    for (Optos::iterator optoIt = optos_.begin(); optoIt != optos_.end(); optoIt++) {
        if((*optoIt) == NULL)
            continue;
        //(*optoIt)->writeBits(Opto::BITS_ERROR_RX_ER , (*optoIt)->GetEnabledOptLinks().to_ulong()); //jak 1 to liczy blady z TLK
        //(*optoIt)->writeBits(Opto::BITS_ERROR_RX_DV , (*optoIt)->GetEnabledOptLinks().to_ulong()); //jak 1 to liczy brak DV  (Data Valid) z TLK
        //(*optoIt)->writeBits(Opto::BITS_ERROR_REC_ERR , (*optoIt)->GetEnabledOptLinks().to_ulong());  //jak 1 to liczy bledy analizy danych

        if(checkDataEna) {
            (*optoIt)->writeWord(Opto::WORD_REC_CHECK_DATA_ENA, 0, (*optoIt)->GetEnabledOptLinks().to_ulong());
            (*optoIt)->writeWord(Opto::WORD_SEND_CHECK_DATA_ENA, 0, 7);
        }
        else {
            (*optoIt)->writeWord(Opto::WORD_REC_CHECK_DATA_ENA, 0, uint32_t(0));
            (*optoIt)->writeWord(Opto::WORD_SEND_CHECK_DATA_ENA, 0, uint32_t(0));
        }

        if(checkEna) {
            (*optoIt)->writeWord(Opto::WORD_REC_CHECK_ENA, 0, (*optoIt)->GetEnabledOptLinks().to_ulong());
            (*optoIt)->writeWord(Opto::WORD_SEND_CHECK_ENA, 0, 7);
        }
        else {
            (*optoIt)->writeWord(Opto::WORD_REC_CHECK_ENA, 0, uint32_t(0));
            (*optoIt)->writeWord(Opto::WORD_SEND_CHECK_ENA, 0, uint32_t(0));
        }
    }

    for (Pacs::iterator pacIt = pacs_.begin(); pacIt != pacs_.end(); pacIt++) {
        if((*pacIt) == 0)
            continue;
        if(checkDataEna) {
            (*pacIt)->writeWord(Pac::WORD_REC_CHECK_DATA_ENA, 0, 0x3ffff); //UsedOptos.to_ulong());
            (*pacIt)->writeWord(Pac::WORD_SEND_CHECK_DATA_ENA, 0, 1);
        }
        else {
            (*pacIt)->writeWord(Pac::WORD_REC_CHECK_DATA_ENA, 0, uint32_t(0));
            (*pacIt)->writeWord(Pac::WORD_SEND_CHECK_DATA_ENA, 0, uint32_t(0));
        }

        if(checkEna) {
            (*pacIt)->writeWord(Pac::WORD_REC_CHECK_ENA, 0, 0x3ffff); //UsedOptos.to_ulong());
            (*pacIt)->writeWord(Pac::WORD_SEND_CHECK_ENA, 0, 1);
        }
        else {
            (*pacIt)->writeWord(Pac::WORD_REC_CHECK_ENA, 0, uint32_t(0));
            (*pacIt)->writeWord(Pac::WORD_SEND_CHECK_ENA, 0, uint32_t(0));
        }
    }

    if (rmb_ != 0) {
        if(checkDataEna) {
            rmb_->writeWord(Rmb::WORD_REC_CHECK_DATA_ENA, 0, 0x3ffff); //UsedOptos.to_ulong());
        }
        else {
            rmb_->writeWord(Rmb::WORD_REC_CHECK_DATA_ENA, 0, uint32_t(0));
        }

        if(checkEna) {
            rmb_->writeWord(Rmb::WORD_REC_CHECK_ENA, 0, 0x3ffff); //UsedOptos.to_ulong());
        }
        else {
            rmb_->writeWord(Rmb::WORD_REC_CHECK_ENA, 0, uint32_t(0));
        }
    }

    if(gbSort_ != 0) {
        if(checkDataEna) {
            gbSort_->writeWord(GbSort::WORD_REC_CHECK_DATA_ENA, 0, usedPacs_.to_ulong());
            gbSort_->writeWord(GbSort::WORD_SEND_CHECK_DATA_ENA, 0, 0x1);
        }
        else {
            gbSort_->writeWord(GbSort::WORD_REC_CHECK_DATA_ENA, 0, uint32_t(0));
            gbSort_->writeWord(GbSort::WORD_SEND_CHECK_DATA_ENA, 0, uint32_t(0));
        }
        if(checkEna) {
            gbSort_->writeWord(GbSort::WORD_REC_CHECK_ENA, 0, usedPacs_.to_ulong());
            gbSort_->writeWord(GbSort::WORD_SEND_CHECK_ENA, 0, 0x1);
        }
        else {
            gbSort_->writeWord(GbSort::WORD_REC_CHECK_ENA, 0, uint32_t(0));
            gbSort_->writeWord(GbSort::WORD_SEND_CHECK_ENA, 0, uint32_t(0));
        }
    }
}

void TriggerBoard::EnableOptLink(int linkNum, bool enable) {
    if(optos_[linkNum/Opto::GetOptLinksCount()] != NULL)
        optos_[linkNum/Opto::GetOptLinksCount()]->EnableOptoLink(linkNum%Opto::GetOptLinksCount(), enable);
    else {
        throw TException("TriggerBoard::EnableOptLink: no Opto for required optLink input");
    }
}

void TriggerBoard::Reset() {
    triggerBoardControl_.reset();

    for (Optos::iterator optoIt = optos_.begin(); optoIt != optos_.end(); optoIt++) {
        if((*optoIt) == NULL)
            continue;
        (*optoIt)->reset();
    }

    for (Pacs::iterator pacIt = pacs_.begin(); pacIt != pacs_.end(); pacIt++) {
        if((*pacIt) == NULL)
            continue;
        (*pacIt)->reset();
    }

    if(rmb_ != NULL) {
        rmb_->reset();
    }

    if(gbSort_ != NULL) {
        gbSort_->reset();
    }
}

std::vector<int> TriggerBoard::getOptLinkStatus() {
    std::vector<int> optLinkStatus(0, 18);
    for(unsigned int iO = 0; iO < optos_.size(); iO++) {
        for(unsigned int iLink = 0; iLink < optos_[iO]->getOptLinkStatus().size(); iLink++) {
            optLinkStatus[iO * 3 + iLink] = optos_[iO]->getOptLinkStatus()[iLink];
        }
    }
    return optLinkStatus;
}

}
