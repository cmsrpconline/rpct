//---------------------------------------------------------------------------

//
//  Michal Pietrusinski 2006
//  Warsaw University
//

#include "rpct/devices/FebInstanceManager.h"
   
#include "log4cplus/loggingmacros.h"

using namespace std;
using namespace log4cplus;


namespace rpct {
       
Logger FebInstanceManager::logger = Logger::getInstance("FebInstanceManager");
void FebInstanceManager::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}
         
FebInstanceManager::~FebInstanceManager() {
    for (FebPerChannelMap::iterator iFebMap = febPerChannelMap.begin(); iFebMap != febPerChannelMap.end(); ++iFebMap) {
        FebMap& febMap = iFebMap->second;
        DistributionBoard* db = &febMap.begin()->second->getDistributionBoard();
        delete db;
        for (FebMap::iterator iFeb = febMap.begin(); iFeb != febMap.end(); ++iFeb) {
            delete iFeb->second;
        }
    }
}
         
Feb& FebInstanceManager::getFeb(int channel, unsigned int localNumber, bool endcap, TI2CInterface* i2c) {
    LOG4CPLUS_DEBUG(logger, "searching febs for channel " << channel);
    FebMap& febMap = febPerChannelMap[channel];
    if (febMap.empty()) {
        // there is not feb for this channel
        DistributionBoard* db = new DistributionBoard(i2c);
        Feb* feb = endcap ? new FebEndcap(*db, localNumber) : new Feb(*db, localNumber);
        febMap.insert(FebMap::value_type(localNumber, feb));
        return *feb;
    }

    FebMap::iterator iFeb = febMap.find(localNumber);
    if (iFeb == febMap.end()) {
        DistributionBoard& db = febMap.begin()->second->getDistributionBoard();
        Feb* feb = endcap ? new FebEndcap(db, localNumber) : new Feb(db, localNumber);
        febMap.insert(FebMap::value_type(localNumber, feb));
        return *feb;        
    }
    return *iFeb->second;
}

Feb& FebInstanceManager::getFeb(int channel, unsigned int localNumber, TI2CInterface* i2c) {
    return getFeb(channel, localNumber, false, i2c);
}

FebEndcap& FebInstanceManager::getFebEndcap(int channel, unsigned int localNumber, TI2CInterface* i2c) {
    return dynamic_cast<FebEndcap&>(getFeb(channel, localNumber, true, i2c));
}



} // namespace
