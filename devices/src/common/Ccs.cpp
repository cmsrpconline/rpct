#include "rpct/devices/Ccs.h"
#include "rpct/devices/System.h" 
#include "rpct/devices/CcsSettings.h" 
#include "rpct/std/NullPointerException.h"
#include "rpct/ii/ConfigurationFlags.h"
#include "rpct/devices/CcsMonitorables.h"

using namespace std;
using namespace log4cplus;

namespace rpct {

Logger CcsTriggerFPGA::logger = Logger::getInstance("CcsTriggerFPGA");
void CcsTriggerFPGA::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
	//logger = Logger::getInstance(namePrefix + "." + "TCB");
}



const HardwareItemType CcsTriggerFPGA::TYPE(HardwareItemType::cDevice, "TriggerFPGA");

CcsTriggerFPGA::CcsTriggerFPGA(int vme_board_address, 
        IBoard& board, TVMEInterface* vme) 
: TDummyVMEDevice(board.getId(), "CCStriggerFPGA", "CCStriggerFPGA",//deviceId=boardId 
        TYPE, "1.4", 0, vme_board_address, board, 0, vme) {}

void CcsTriggerFPGA::setEnabledInputs(const boost::dynamic_bitset<>& enabledInputs){
    if(enabledInputs.size() != 5)
        throw TException("CcsTriggerFPGA::setEnabledInputs: enabledInputs.size() != 5");
    enabledInputs_=enabledInputs;
    uint32_t dccIn = ~(enabledInputs.to_ulong());
    writeWord(TTS_CONF,(0x3e & (dccIn<<1)));
}

void CcsTriggerFPGA::configure(DeviceSettings* settings, int flags) {
    if (settings == 0) {
        /*if (flags & ConfigurationFlags::RESET_ONLY_WHEN_NO_CONFIG_DATA) {
            return;
        }*/
        throw NullPointerException("CcsTriggerFPGA::configure: settings is null");
    }
    CcsSettings* s = dynamic_cast<CcsSettings*>(settings);
    if(s == 0)
        throw IllegalArgumentException("CcsTriggerFPGA::configure: invalid cast for settings");
    setEnabledInputs(s->getEnabledInputs());
    
    // mask L1 accespt
    writeWord(CONFIG1, 0xF);
    
    writeWord(CONFIG0, 0x00b8); //1011 1000 , in the initialize, which is called before is triggerFPGA.writeWord(CONFIG0,0x50b8);
    
    //0x80 - TTCrxready 1, QPLLlocked 0
    //0x00 - TTCrxready 0, QPLLlocked 0
    //0xc0 - TTCrxready 1, QPLLlocked 0 - but the LED is pink (blinking very fast???)    
    //0xa0 - TTCrxready 1, QPLLlocked 1 - the LED is green
    
    //TTCrxready 1 => TTCrx is ready
    
    
    /* CF0<4> Autorestart or 5th bit of the frequency select (depends on EXTCTRL????), should be 1
     * CF0<5> QPLL Reset (0 - reset, 1 - normal)
     * CF0<6> QPLL EXTCTRL (0 - external (TTC clock), 1 - internal (generates own clock)
     * CF0<7> TTCrx Reset ()
     * CF0<8> Force local mode (1 - force local, 0 - remote mode)
     * 
     * */
    /*the bit number 8 (0x100) must be 0, otherwise the DDC will not recive the TTC signals
     * see below
     * from that it yields, that we have to work with the external TTC signal, otherwise olse the
     * DCC will not receive the correct TTC signals!!!
     * 
     */
    
    /*
     * By default, upon power up, the card will check if there is an active TTC
signal at the input. If the TTC input signal lies in the correct frequency
range then both the TTCrx chip and the QPLL chip on-board are in-lock and
the card enters the REMOTE mode of operation.
In the REMOTE mode the input TTC signal is propagated to the ECAL backplane
bypassing the Trigger FPGA logic following the "red" path shown on the
schematic. The clock signal that goes to the mFECs is also synchronized to
the TTC input signal.

In this condition the user can FORCE the card to operate in the "LOCAL" mode
by setting the bit "FORCE_LOCAL_MODE" to "1". In the LOCAL operation mode
the card will switchover the TTC output multiplexer sending to the ECAL
backplane a TTC signal that is encoded localy in the Trigger FPGA.
At the same time the mFEC clock signal is also localy generated in the
Trigger FPGA.
A side effect of this is that the TTC output signal and the mFEC clock
signal will still be in-sync with the TTC input signal.
That is because the TTCrx and the QPLL are in-lock with the TTC input
signal. If the user sets the TTCrx in RESET mode then this locking condition
breaks and the QPLL works as a free running clock generator.
The TTC output on the ECAL bus and the mFEC clock signal will be in-sync but
they will loose sync with the TTC input signal.

In the FORSED LOCAL MODE the TTC front-panel LED will start blinking in red
color to warn the user that the card is in a non standard and potential
dangerous operating mode. The card will never go out of this mode unless you
power cycle it or write back the "correct" values in the confog0 register.

By default (FORCE_LOCAL_MODE bit = 0) the card will switchover between
REMOTE and LOCAL mode depending of the status of the TTC input signal.

The default value of the CF<3..0> that is preprogrammed in the Trigger FPGA
and is loaded in the Config0 register on power up is chosen such as the card
will lock on the 40.080MHz clock reference. To check the default values in
the Trigger FPGA just read back the Config0 register after powering up the
card.

Let me know if that information was helpful and if you still need
clarifications.

Best Regards,
Kostas
     */
}

void CcsTriggerFPGA::reset(){}

//---------------------------------------------------------------

Logger Ccs::logger = Logger::getInstance("Ccs");
void Ccs::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
	//logger = Logger::getInstance(namePrefix + "." + "TCB");
}

const HardwareItemType Ccs::TYPE(HardwareItemType::cBoard, "CCS");

Ccs::Ccs(int ident, const char* desc, TVMEInterface* vme, 
        int vme_board_address, VmeCrate* c, int pos)
: BoardBase(ident, desc, TYPE, c, pos), vme_(vme),
  vmeAddress_(vme_board_address), triggerFPGA(vme_board_address,*this,vme) 
{ 
    vme_->SetDefaultAddressingMode(TVMEInterface::am32);
    Init();
    LOG4CPLUS_INFO(logger, desc << " " << hex << vme_board_address);          
}

Ccs::~Ccs() { }

void Ccs::Init(){
    devices_.push_back(&triggerFPGA);
}

void Ccs::initialize(){
    //set SSID: 101(bin) for RPC, and TTCrx reset
    triggerFPGA.writeWord(CONFIG0,0x50b8);
    //some debug info
    uint32_t id_reg = triggerFPGA.readWord(ID_REG);
    LOG4CPLUS_INFO(logger, "CCS:" << " SSID " <<  bitset<3>((id_reg & 0xf000)>>12) << ", version " << ((id_reg & 0xf0)>>4) << "." << (id_reg & 0xf)); 
    uint32_t statTTC = triggerFPGA.readWord(STATUS0);
    LOG4CPLUS_INFO(logger, "CCS:" << " TTCrx Ready " << (statTTC & 0x1) << ", QPLL Locked " << (statTTC & 0x2) << ", QPLL Err " << (statTTC & 0x4)); 
    //mask all TTS inputs and switch off force mode
    triggerFPGA.writeWord(TTS_CONF,0x3e);
    //LOG4CPLUS_INFO(logger, "CCS:" << " Ready " );
}

void Ccs::configure(unsigned int dccIn = 0x1f ){
    //initialize();
    //dccIn = (0x3e & (dccIn<<1)); 
    //unmask used inputs (1-3)
    //triggerFPGA.writeWord(TTS_CONF,dccIn);
    boost::dynamic_bitset<> enIn(5,dccIn);
    enIn.flip();
    triggerFPGA.setEnabledInputs(enIn);
    checkTTS();
}

void Ccs::configure(DeviceSettings* settings){
    triggerFPGA.configure(settings, ConfigurationFlags::FORCE_CONFIGURE);
}

void Ccs::forceTTS(unsigned int status = 0x8){//default stat = 0x8 i.e. READY
    //check if status value is in valid range (4 bits);
    if((status>0xf)||(status<0x0)){
        LOG4CPLUS_INFO(logger, "CCS:" << " invalid TTS status " << status);
        return;
    }
    uint32_t currentStatus = (triggerFPGA.readWord(TTS_CONF) & 0x3f); //only 6 youngest bits
    //switch on force mode
    currentStatus |= (0x1 << 0 );
    //force TTS status
    currentStatus |= (status<<8);
    triggerFPGA.writeWord(TTS_CONF,currentStatus);
    //checkTTS();
}

void Ccs::freeTTS(){
    uint32_t currentStatus = (triggerFPGA.readWord(TTS_CONF) & 0x3f); //only 6 youngest bits
    //switch off force mode
    currentStatus &= 0x3e;
    triggerFPGA.writeWord(TTS_CONF,currentStatus);
    //checkTTS();
}

int Ccs::checkTTS(){
    uint32_t setTTS = triggerFPGA.readWord(TTS_CONF);
    uint32_t status = triggerFPGA.readWord(TTS_STAT);
//    LOG4CPLUS_INFO(logger, "CCS:" << " set TTS: Force " <<(setTTS & 0x1) 
//            << ", Disabled In "   << std::bitset<5>(setTTS >> 1) 
//            << ", Forced Out "    << std::bitset<4>(setTTS >> 8)); 

//    LOG4CPLUS_INFO(logger, "CCS:" << ", TTS status: In1 " << bitset<4>(status & 0xf)
//            <<             ", In2 " << bitset<4>((status & 0xf0) >> 4)
//            <<             ", In3 " << bitset<4>((status & 0xf00) >> 8)
//            <<             ", In4 " << bitset<4>((status & 0xf000) >> 12)
//            <<             ", In5 " << bitset<4>((status & 0xf0000) >> 16)
//            <<             ", Out " << bitset<4>((status & 0xf00000) >> 20)); 
    return status;
}

int Ccs::checkStatus() {
    uint32_t status = triggerFPGA.readWord(STATUS0);
//    LOG4CPLUS_INFO(logger, "CCS " << vmeAddress_ <<" Status: " <<hex<< status << " TTCrx READY " <<(status & 0x1) << ", QPLL LOCKED "   << ((status & 0x2)>>1) << ", QPLL ERROR "    << ((status & 0x4)>>2) ); 
   
    return status;
}

CcsMonitorables * Ccs::checkBoard() 
{
  monitorables_ = CcsMonitorables();
  monitorables_.setCcsStatus(checkStatus());
  monitorables_.setTtsStatus(checkTTS());
  monitorables_.setPosition(getPosition());
  return &monitorables_; 
}

}

