//---------------------------------------------------------------------------
#ifdef __BORLANDC__
#include "precompiled.h"
#pragma hdrstop
#endif

#include <algorithm>
#include <cctype>

#include "rpct/devices/TTUBoard.h"
#include "rpct/devices/VmeCrate.h"
#include "rpct/devices/i2c.h"
#include "rpct/devices/System.h"
#include "rpct/devices/RbcSettings.h"
#include "rpct/devices/FixedHardwareSettings.h"
#include "rpct/std/IllegalArgumentException.h"
#include "rpct/std/NullPointerException.h"
#include "rpct/ii/ConfigurationFlags.h"
#include "rpct/devices/TTUOptoBxData.h"
#include "rpct/devices/TTUTrigBxData.h"
#include "rpct/devices/TtuTriggerSettings.h"

//---------------------------------------------------------------------------


using namespace std;
using namespace log4cplus;

namespace rpct {

Logger TTUBoardDevice::logger = Logger::getInstance("TTUBoardDevice");
void TTUBoardDevice::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}
//---------------------------------------------------------------------------
#define IID_FILE "RPC_system_def.iid"
#define IID_CLASS TTUBoardDevice
#include "iid2cdef2.h"
//addendum to RPC_system_def.iid??//some redefinitions
#define IID_FILE "RBC_system_def.iid"
#define IID_CLASS TTUBoardDevice
#include "iid2cdef2.h"

#define IID_FILE "RPC_TB3_def.iid"
#define IID_CLASS TTUBoardDevice
#include "iid2cdef2.h"

//---------------------------------------------------------------------------

#define IID_FILE "RPC_TB3_vme.iid"
#define IID_CLASS TTUBoardVme
#include "iid2c2.h"

const HardwareItemType TTUBoardVme::TYPE(HardwareItemType::cDevice, "TTUVME");

TTUBoardVme::TTUBoardVme(int vme_board_address, IBoard& board) :
	TTUBoardDevice(board.getId() * 1000, VME_IDENTIFIER.c_str(),
			WORD_IDENTIFIER, "VME", TYPE, VME_VERSION.c_str(), WORD_VERSION,
			WORD_CHECKSUM, 0, board, -1, BuildMemoryMap(), vme_board_address),
			ABootContr(0) {
}

TTUBoardVme::~TTUBoardVme() {
	delete ABootContr;
}

//---------------------------------------------------------------------------

const HardwareItemType TTUBoardControl::TYPE(HardwareItemType::cDevice,
		"TTUCONTROL");

#ifdef CII
#define IID_CLASS TTUBoardControl
#include "CII_TB3_CTRL_iicfg_tab.cpp"
#undef IID_CLASS
#else
#define IID_FILE "RPC_TB3_ctrl.iid"
#define IID_CLASS TTUBoardControl
#include "iid2c2.h"
const int TTUBoardControl::II_ADDR_SIZE = TTUBoardDevice::TB_II_ADDR_WIDTH_SOFT;
const int TTUBoardControl::II_DATA_SIZE = TTUBoardDevice::TB_II_DATA_WIDTH;
#endif

TTUBoardControl::TTUBoardControl(int vme_board_address, IBoard& board,
		int position) :
	TTUBoardDevice(board.getId() * 1000 + 1, TB_CTRL_IDENTIFIER.c_str(),
			WORD_IDENTIFIER, TB_CTRL_IDENTIFIER, TYPE, TB_CTRL_VERSION.c_str(),
			WORD_VERSION, WORD_CHECKSUM, TB_BASE_CNTRL, board, position,
			BuildMemoryMap(), vme_board_address), ABootContr(0), I2C(0),
			TTCrxI2C(7, board.getDescription()),
			qpll_(*this, logger,
					VECT_QPLL,
					BITS_QPLL_MODE,
					BITS_QPLL_CTRL,
					BITS_QPLL_SELECT,
					BITS_QPLL_LOCKED,
					BITS_QPLL_ERROR,
					BITS_QPLL_CLK_LOCK)
{
    monitorItems_.push_back(MonitorItemName::FIRMWARE);
	monitorItems_.push_back(MonitorItemName::QPLL);
}

TTUBoardControl::~TTUBoardControl() {
	delete ABootContr;
}

void TTUBoardControl::ResetQPLL() {
	writeBits(BITS_TTC_TC_ENA, 1); //we are using the TTC signals and clock from the TC, not from the TB TTCrx, because it does not work

	qpll_.reset();
}

TIII2CMasterCore& TTUBoardControl::GetI2C() {
	if (I2C == NULL) {

		I2C = new TIII2CMasterCore(*this, AREA_I2C);
		I2C->SetPrescaleValue(100);
		I2C->WriteControlReg(TI2CMasterCore::BIT_EN);
	}

	return *I2C;
}

void TTUBoardControl::SetTTCrxI2CAddress(unsigned char addr) {
	TTCrxI2C.SetAddress(addr);
	writeWord(WORD_TTC_ID_MODE, 0, addr);
	writeBits(BITS_TTC_INIT_ENA, 1);
	ResetTTC();
	writeBits(BITS_TTC_INIT_ENA, 0);
}

void TTUBoardControl::ResetTTC() {
	writeBits(BITS_TTC_RESET, 1);
	tbsleep(3000);
	if (readBits(BITS_TTC_READY) != 0) {
		//throw TException("TTUBoardControl::ResetTTC: BITS_TTC_READY did not go down after BITS_TTC_RESET, 1");
		LOG4CPLUS_ERROR(logger, "TTUBoardControl::ResetTTC: BITS_TTC_READY did not go down after BITS_TTC_RESET, 1");
	}
	writeBits(BITS_TTC_RESET, 0);
}

void TTUBoardControl::InitTTC() {
	SetTTCrxI2CAddress(TTCrxI2C.GetAddress());
	tbsleep(100);
	if (readBits(BITS_TTC_READY) != 1) {
		//throw TException("TTUBoardControl::InitTTC: BITS_TTC_READY did not go up");
		LOG4CPLUS_ERROR(logger, "TTUBoardControl::InitTTC: BITS_TTC_READY did not go up");

	}
	// Enable Clock40Des2 output
	LOG4CPLUS_WARN(logger, "tb3: Clock40Des2 not enabled ");
	//GetTTCrxI2C().WriteControlReg(GetTTCrxI2C().ReadControlReg() | 0x8);
}

bool TTUBoardControl::checkResetNeeded() {
	return !(readBits(BITS_QPLL_LOCKED));
}

void TTUBoardControl::reset() {
	writeBits(BITS_TTC_TEST_ENA, uint32_t(0));
	writeBits(BITS_TTC_TC_ENA, 1);
	writeBits(BITS_TTC_TC_DELAY,
			FixedHardwareSettings::TBCONTROL_TTC_DATA_DELAY);
}

void TTUBoardControl::monitor(volatile bool *stop) {
	monitor(monitorItems_);
}

void TTUBoardControl::monitor(MonitorItems& items) {
    warningStatusList_.clear();
    for (MonitorItems::iterator iItem = items.begin(); iItem != items.end(); ++iItem) {
        if ((*iItem) == MonitorItemName::FIRMWARE) {
            string msg = monitorVersion();
            if (msg.size() != 0) {
                warningStatusList_.push_back(MonitorableStatus(MonitorItemName::FIRMWARE, MonitorableStatus::ERROR, msg, 0));
            }
        }
        else if ((*iItem) == MonitorItemName::QPLL) {
            if (readBits(BITS_QPLL_LOCKED) != 1) {
                if (readBits(BITS_QPLL_LOCKED) != 1) {
                    warningStatusList_.push_back(MonitorableStatus(MonitorItemName::QPLL, MonitorableStatus::ERROR, "BITS_QPLL_LOCKED is not up", 0));
                }
                else {
                    warningStatusList_.push_back(MonitorableStatus(MonitorItemName::QPLL, MonitorableStatus::WARNING, "BITS_QPLL_LOCKED is not up", 0));
                }
            }
        }
        /*else if ((*iItem) == MON_TTCRX) {
         if (readBits(BITS_TTC_READY) != 0) {
         throw MonitorableException(*this, MON_TTCRX, "BITS_TTC_READY is not down");
         }
         }*/
    }
}

//---------------------------------------------------------------------------

const HardwareItemType TTUOpto::TYPE(HardwareItemType::cDevice, "TTUOPTO");

#ifdef CII
#define IID_CLASS TTUOpto
#include "CII_TTU_opto_iicfg_tab.cpp"
#undef IID_CLASS
#else
#define IID_FILE "RBC_opto.iid"
#define IID_CLASS TTUOpto
#include "iid2c2.h"
const int TTUOpto::II_ADDR_SIZE = TTUBoardDevice::TB_II_ADDR_WIDTH_SOFT;
const int TTUOpto::II_DATA_SIZE = TTUBoardDevice::TB_II_DATA_WIDTH;
#endif

TTUOpto::TTUOpto(int id, int vme_board_address, int ii_addr, const char* desc,
		IBoard& board, int position) :
	TTUBoardDevice(id, TTU_OPTO_IDENTIFIER.c_str(), WORD_IDENTIFIER, desc,
			TYPE, TTU_OPTO_VERSION.c_str(), WORD_VERSION, WORD_CHECKSUM,
			ii_addr, board, position, BuildMemoryMap(), vme_board_address),
			optLinkStatus_(2, OFF), DiagCtrl(0), DiagnosticReadout(0),
			PulserDiagCtrl(0), Pulser(0), monRecErrorAnalyzer_(
					"MON_REC_ERROR_COUNT", 1, 0.001, 0.1, 0xffff) {

	for (unsigned int iLink = 0; iLink < GetOptLinksCount(); iLink++) {
		OptLinks.push_back(TOptLink(this, iLink));
	}
	EnabledOptLinks = boost::dynamic_bitset<>(3, 0ul);

	//.. monRecErrorAnalyzer_("MON_REC_ERROR_COUNT", 1, 1, 1000, 0xffffff);
	//.. minimumPeriod =
	//.. warningRateThreshold = 0.001 Hz
	//.. errorRateThreshold = 0.1 Hz
	//.. maxCounterVal =

	//TODO: 1. how to mask unsused optochips (TTU2: opto5,6,7)
	//2. check if all these are really needed  AO
	monitorItems_.push_back(MonitorItemName::FIRMWARE);
    monitorItems_.push_back(MonitorItemName::OPT_LINK);
}

TTUOpto::~TTUOpto() {
	delete DiagCtrl;
	delete DiagnosticReadout;
	delete Pulser;
	delete PulserDiagCtrl;
}

void TTUOpto::reset() {
	writeBits(BITS_STATUS_TTC_DATA_DELAY,
			FixedHardwareSettings::OPTO_TTC_DATA_DELAY); //<<<<<<<< may change after firmware change!!!!

	/*   writeBits(BITS_TLK_ENABLE, 0, 7);
	 writeBits(BITS_TLK_LCK_REFN, 0, uint32_t(0));
	 LOG4CPLUS_INFO(logger, getBoard().getDescription()<<" "<<getDescription()<<" all opt links inputs disabled");*/

	writeWord(WORD_REC_TEST_ENA, 0, uint32_t(0));
	writeWord(WORD_REC_TEST_RND_ENA, 0, uint32_t(0));
	for (unsigned int iLink = 0; iLink < (unsigned int) TTU_OPTO_CHAN_NUM; iLink++) {
		setRecDataDelay(iLink, uint32_t(0));
		writeWord(WORD_SEND_TEST_DATA, iLink, uint32_t(0));
	}

	writeWord(WORD_SEND_TEST_ENA, 0, uint32_t(0));
	writeWord(WORD_SEND_TEST_RND_ENA, 0, uint32_t(0));

	//writeBits(Opto::BITS_PULSER_OUT_ENA, uint32_t(0));
	//BCNO delay
	writeWord(WORD_BCN0_DELAY, 0, FixedHardwareSettings::TTUOPTO_BC0_DELAY);

    writeBits(BITS_STATUS_REC_HRESET_ENA, 1);
    writeWord(WORD_HRESET_TIMER, 0, FixedHardwareSettings::CONFIGURE_FROM_FLASH_TIME * 40 * 8); //[us]*40 BX/us, * 8 because there are looks that here longer time is neede to avoid the false triggers

	resetErrorCounters();

}

void TTUOpto::configure(DeviceSettings* settings, int flags) {

	LOG4CPLUS_INFO(logger, "configuring>TTUOptos");

	if (flags & ConfigurationFlags::FORCE_CONFIGURE) {
		LOG4CPLUS_INFO(logger, "configuring>TTUOptos : COLD_SETUP selected");
		reset();
	}

	if (settings == 0) {
		throw NullPointerException("TTUOpto::configure: settings == 0");
	}
	OptoSettings* s = dynamic_cast<OptoSettings*> (settings); //standard opto setting? Yes
	if (s == 0) {
		throw IllegalArgumentException("TTUOpto::configure: invalid cast for settings");
	}

	//... TTU only uses 2 optolinks
	unsigned int enable = s->isEnableLink0() + (s->isEnableLink1() << 1);
	EnabledOptLinks = boost::dynamic_bitset<>(2, enable);
	for (unsigned int i = 0; i < EnabledOptLinks.size(); i++) {
		std::cout << "TTUOptos::configure: " << i << " " << EnabledOptLinks[i]<< '\n';
		if (EnabledOptLinks[i])
			optLinkStatus_[i] = ON;
		else
			optLinkStatus_[i] = OFF;
	}

	EnableOptoLink(enable);
	LOG4CPLUS_INFO(logger, getBoard().getDescription()<<" "<<getDescription()<<" enabled opt links "<<enable);
	enableTransmissionCheck();
	resetErrorCounters();

}
void TTUOpto::enable() {
    //writeWord(WORD_REC_ERROR_COUNT, 0, uint32_t(0));
    resetErrorCounters();
}

void TTUOpto::setRecFastDelay(unsigned int linkNum, unsigned int delay) {
	/*obsolete, correction needed
	 * #ifdef CII
	 unsigned int value = readWord(WORD_REC_FAST_DATA_DELAY, 0);
	 #else
	 unsigned int value = readWord(WORD_RECEIVER_FAST_DATA_DELAY, 0);
	 #endif
	 value &= ~(7 << (linkNum * 3));
	 value |= delay << (linkNum * 3);
	 #ifdef CII
	 writeWord(WORD_REC_FAST_DATA_DELAY, 0, value);
	 #else
	 writeWord(WORD_RECEIVER_FAST_DATA_DELAY, 0, value);
	 #endif
	 */}

void TTUOpto::setRecDataDelay(unsigned int linkNum, unsigned int delay) {
	/*obsolete, correction needed
	 * #ifdef CII
	 unsigned int value = readWord(WORD_REC_DATA_DELAY, 0);
	 #else
	 unsigned int value = readWord(WORD_RECEIVER_DATA_DELAY, 0);
	 #endif
	 value &= ~(7 << (linkNum * 3));
	 value |= delay << (linkNum * 3);
	 #ifdef CII
	 writeWord(WORD_REC_DATA_DELAY, 0, value);
	 #else
	 writeWord(WORD_RECEIVER_DATA_DELAY, 0, value);
	 #endif
	 */}

TTUOpto::TOptLink::TOptLink(TTUOpto* opto, int optLinkNum) :
	opto_(opto), number_(optLinkNum) {
}

TTUOpto::TOptLink::~TOptLink() {
}

void TTUOpto::TOptLink::EnableOptoLink(bool enable) {
	uint32_t tlkEnable = GetOpto().readBits(TTUOpto::BITS_TLK_ENABLE);
	if (enable) {
		tlkEnable = tlkEnable | 1 << number_;
		GetOpto().EnabledOptLinks[number_] = true;
	} else {
		tlkEnable = tlkEnable & (~(1 << number_));
		GetOpto().EnabledOptLinks[number_] = false;
	}
	GetOpto().writeBits(TTUOpto::BITS_TLK_ENABLE, tlkEnable);
	GetOpto().writeBits(TTUOpto::BITS_TLK_LCK_REFN, tlkEnable);
	GetOpto().writeWord(WORD_REC_TEST_ENA, 0, (~tlkEnable) & 7);
}

void TTUOpto::EnableOptoLink(uint32_t enable) {
	writeBits(TTUOpto::BITS_TLK_ENABLE, enable);
	writeBits(TTUOpto::BITS_TLK_LCK_REFN, enable);
	writeBits(TTUOpto::BITS_STATUS_REC_SYNCH_REQ, enable);

    writeWord(WORD_REC_TEST_ENA, 0, (~enable) & 7);
}

void TTUOpto::enableTransmissionCheck() {
	bool checkDataEna = true;
	bool checkEna = true;
	if (checkDataEna) {
		writeWord(WORD_REC_CHECK_DATA_ENA, 0, GetEnabledOptLinks().to_ulong());
		writeWord(WORD_SEND_CHECK_DATA_ENA, 0, 7);
	} else {
		writeWord(WORD_REC_CHECK_DATA_ENA, 0, uint32_t(0));
		writeWord(WORD_SEND_CHECK_DATA_ENA, 0, uint32_t(0));
	}

	if (checkEna) {
		writeWord(WORD_REC_CHECK_ENA, 0, GetEnabledOptLinks().to_ulong());
		writeWord(WORD_SEND_CHECK_ENA, 0, 7);
	} else {
		writeWord(WORD_REC_CHECK_ENA, 0, uint32_t(0));
		writeWord(WORD_SEND_CHECK_ENA, 0, uint32_t(0));
	}
}

void TTUOpto::resetErrorCounters() {
    writeWord(WORD_REC_ERROR_COUNT, 0, uint32_t(0));
    for(unsigned int iLink = 0; iLink < optLinkStatus_.size(); iLink++) {
        if(EnabledOptLinks[iLink]) {// && (optLinkStatus_[iLink] != RX_ERR) ) { what for I did it??? KB
            readWord(WORD_REC_TEST_OR_DATA, iLink);
        }
    }
    readBits(BITS_TLK_RX_ERROR);
    monRecErrorAnalyzer_.reset();
}

bool TTUOpto::CheckLinksOperation() {
	bool good = true;
	unsigned int noData = readWord(TTUOpto::BITS_TLK_RX_NO_DATA, 0); //data valid, should be 0
	unsigned int err = readBits(TTUOpto::BITS_TLK_RX_ERROR); //should be 0
	//LOG4CPLUS_DEBUG(logger, getDescription()<<" CheckLinksSynchronization: TLK_RX_DV "<<noData<<" TLK_RX_ERR "<<err );

	if ((EnabledOptLinks.to_ulong() & noData) != EnabledOptLinks.to_ulong()
			|| (EnabledOptLinks.to_ulong() & err) != 0) {
		good = false;
		LOG4CPLUS_ERROR(logger, getBoard().getDescription()<<" "<<getBoard().getId()<<" "<<getBoard().getDescription()<<" "<<getDescription()<<" CheckLinksOperation: TLK_RX_DV "<<noData<<" TLK_RX_ERR "<<err );
	}

	return good;
}

TDiagCtrl& TTUOpto::GetDiagCtrl() {
	if (DiagCtrl == 0) {
		DiagCtrl
				= new TDiagCtrl(*this, VECT_DAQ, BITS_DAQ_PROC_REQ,
						BITS_DAQ_PROC_ACK, BITS_DAQ_TIMER_LOC_ENA, 0,
						BITS_DAQ_TIMER_START, BITS_DAQ_TIMER_STOP,
						BITS_DAQ_TIMER_TRIG_SEL, WORD_DAQ_TIMER_LIMIT,
						WORD_DAQ_TIMER_COUNT, WORD_DAQ_TIMER_TRG_DELAY,
						"DAQ_DIAG_CTRL");
	}
	return *DiagCtrl;
}

TStandardDiagnosticReadout& TTUOpto::GetDiagnosticReadout() {
	if (DiagnosticReadout == 0) {
		DiagnosticReadout = new TStandardDiagnosticReadout(*this, "READOUT",
				&TTUOptoBxDataFactory::getInstance(), GetDiagCtrl(), VECT_DAQ,
				BITS_DAQ_EMPTY, BITS_DAQ_EMPTY_ACK, BITS_DAQ_LOST,
				BITS_DAQ_LOST_ACK, VECT_DAQ_WR, BITS_DAQ_WR_ADDR,
				BITS_DAQ_WR_ACK, VECT_DAQ_RD, BITS_DAQ_RD_ADDR,
				BITS_DAQ_RD_ACK, WORD_DAQ_MASK, WORD_DAQ_DATA_DELAY,
				AREA_MEM_DAQ_DIAG, DAQ_DIAG_DATA_SIZE, DAQ_DIAG_TRIG_NUM,
				DAQ_DIAG_TIMER_SIZE, TTC_BCN_EVT_WIDTH);
	}
	return *DiagnosticReadout;
}

TDiagCtrl& TTUOpto::GetPulserDiagCtrl() {
	if (PulserDiagCtrl == NULL) {
		PulserDiagCtrl = new TDiagCtrl(*this, VECT_PULSER,
				BITS_PULSER_PROC_REQ, BITS_PULSER_PROC_ACK,
				BITS_PULSER_TIMER_LOC_ENA, 0, BITS_PULSER_TIMER_START,
				BITS_PULSER_TIMER_STOP, BITS_PULSER_TIMER_TRIG_SEL,
				WORD_PULSER_TIMER_LIMIT, WORD_PULSER_TIMER_COUNT,
				WORD_PULSER_TIMER_TRG_DELAY, "PULSER_DIAG_CTRL");
	}

	return *PulserDiagCtrl;
}

TPulser& TTUOpto::GetPulser() {
	if (Pulser == NULL) {
		Pulser = new OptoPulser(*this, "PULSER", GetPulserDiagCtrl(),
				AREA_MEM_PULSE, WORD_PULSER_LENGTH, BITS_PULSER_REPEAT_ENA,
				BITS_PULSER_OUT_ENA);
	}
	return *Pulser;
}

TTUOpto::TDiagCtrlVector& TTUOpto::GetDiagCtrls() {
	if (DiagCtrls.empty()) {
		DiagCtrls.push_back(&GetDiagCtrl());
		DiagCtrls.push_back(&GetPulserDiagCtrl());
	}
	return DiagCtrls;
}

TTUOpto::TDiagVector& TTUOpto::GetDiags() {
	if (Diags.empty()) {
		Diags.push_back(&GetDiagnosticReadout());
		Diags.push_back(&GetPulser());
	}
	return Diags;
}

void TTUOpto::ConfigureDiagnosable(TTriggerDataSel triggerDataSel,
		uint32_t dataTrgDelay) {
	writeBits(BITS_STATUS_TRG_DATA_SEL, triggerDataSel);
	writeWord(WORD_DATA_TRG_DELAY, 0, dataTrgDelay);
}

void TTUOpto::monitor(volatile bool *stop) {
	monitor(monitorItems_);
}

void TTUOpto::monitor(MonitorItems& items) {
    warningStatusList_.clear();
    for(unsigned int iLink = 0; iLink < optLinkStatus_.size(); iLink++) {
        if( optLinkStatus_[iLink] != OFF) {
            optLinkStatus_[iLink] = OK; //clearing the messages
        }
    }
    unsigned int enabled = EnabledOptLinks.to_ulong();

    for (MonitorItems::iterator iItem = items.begin(); iItem != items.end(); ++iItem) {
        if ((*iItem) == MonitorItemName::FIRMWARE) {
            string msg = monitorVersion();
            if (msg.size() != 0) {
                warningStatusList_.push_back(MonitorableStatus(MonitorItemName::FIRMWARE, MonitorableStatus::ERROR, msg, 0));
            }
        }
        else  if ((*iItem) == MonitorItemName::OPT_LINK) {
            unsigned int err1 = readBits(BITS_TLK_RX_ERROR);
            unsigned int err2 = 0;
            if ((err1 & enabled) != 0) {
            	usleep(10000);
                err2 = readBits(BITS_TLK_RX_ERROR);
            }

            uint32_t recErrCnt = readWord(WORD_REC_ERROR_COUNT, 0);
            monRecErrorAnalyzer_.newValue(recErrCnt);
            MonitorableStatus& recErrorMonStatus = monRecErrorAnalyzer_.getStatus();
            if(recErrCnt == monRecErrorAnalyzer_.getMaxCounterVal()) {
            	writeWord(WORD_REC_ERROR_COUNT, 0, uint32_t(0));
            }

            for(unsigned int iLink = 0; iLink < optLinkStatus_.size(); iLink++) {
                MonitorableStatus monStatus(MonitorItemName::OPT_LINK, MonitorableStatus::OK, "optLink " + toString(getGlobalLinkNum(iLink)) + ": ", 0) ;

                if(EnabledOptLinks[iLink]) {
                    if( (err2 & (1 << iLink)) != 0 ) {
                        optLinkStatus_[iLink] = RX_ERR;
                        monStatus.level = MonitorableStatus::ERROR;
                        monStatus.message +=  "persistent RX_ERROR, ";
                    }
                    else if( (err1 & (1 << iLink)) != 0 ) {
                    	monStatus.level = MonitorableStatus::SOFT_WARNING;
                    	monStatus.message +=  "transient RX_ERROR, ";
                    }

                    if (recErrorMonStatus.level != MonitorableStatus::OK) {
                    	uint32_t recTestOr1 = readWord(WORD_REC_TEST_OR_DATA, iLink);
                    	usleep(10000);
                    	uint32_t recTestOr2 = readWord(WORD_REC_TEST_OR_DATA, iLink);
                    	if(recTestOr2 != 0) {
                    		if(optLinkStatus_[iLink] != RX_ERR) {
                    			optLinkStatus_[iLink] = recErrorMonStatus.level + 2;
                    		}

                    		if(monStatus.level < recErrorMonStatus.level) {
                    			monStatus.level = recErrorMonStatus.level;
                    		}
                    		monStatus.message += recErrorMonStatus.message;
                    		monStatus.value = recErrorMonStatus.value;
                    	}
                    	else if(recTestOr1 != 0 ) {
                    		if(optLinkStatus_[iLink] != RX_ERR) {
                    			monStatus.level = MonitorableStatus::SOFT_WARNING;
                    			monStatus.message +=  "transient receiver error " + recErrorMonStatus.message;
                    		}
                    	}
                    }

                    if(monStatus.level != MonitorableStatus::OK)
                        warningStatusList_.push_back(monStatus);
                }
            }
        }
    }
}

//---------------------------------------------------------------------------

const HardwareItemType TTUTrig::TYPE(HardwareItemType::cDevice, "TTUTRIG");

#ifdef CII
#define IID_CLASS TTUTrig
#include "CII_TTU_trig_iicfg_tab.cpp"
#undef IID_CLASS
#else
#define IID_FILE "RBC_trig.iid" //RPC_TB3_ldpac.iid->RBC_trig.iid
#define IID_CLASS TTUTrig
#include "iid2c2.h"
const int TTUTrig::II_ADDR_SIZE = TTUBoardDevice::TB_II_ADDR_WIDTH_SOFT;
const int TTUTrig::II_DATA_SIZE = TTUBoardDevice::TB_II_DATA_WIDTH;
#endif

TTUTrig::TTUTrig(int id, int vme_board_address, int ii_addr, const char* desc,
        IBoard& board, int position) :
        TTUBoardDevice(id, TTU_TRIG_IDENTIFIER.c_str(), WORD_IDENTIFIER, desc,
                TYPE, TTU_TRIG_VERSION.c_str(), WORD_VERSION, WORD_CHECKSUM,
			ii_addr, board, position, BuildMemoryMap(), vme_board_address),
			DiagCtrl(0), DiagnosticReadout(0),
			monRecErrorAnalyzer_("MON_REC_ERROR_COUNT", 1, 0.001, 0.1, 0xffffff) {
    monitorItems_.push_back(MonitorItemName::FIRMWARE);
    monitorItems_.push_back(MonitorItemName::RECEIVER);
}

TTUTrig::~TTUTrig() {
	delete DiagCtrl;
	delete DiagnosticReadout;
}

void TTUTrig::reset() {

	writeBits(BITS_STATUS_TTC_DATA_DELAY,
			FixedHardwareSettings::PAC_TTC_DATA_DELAY + 1); //<<<may change after firmware changed
	//+ 1 tymczsowo!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	//writeWord(Pac::WORD_REC_TEST_ENA, 0, ~((dynamic_cast<TriggerBoard&>(getBoard())).GetUsedOptos().to_ulong()));

	writeWord(WORD_REC_TEST_ENA, 0, uint32_t(0));
	writeWord(WORD_REC_TEST_RND_ENA, 0, uint32_t(0));

	//writeWord(WORD_SEND_TEST_ENA, 0, uint32_t(0));
	//writeWord(WORD_SEND_TEST_RND_ENA, 0, uint32_t(0));
	//writeWord(WORD_SEND_TEST_DATA, 0, uint32_t(0));
	//writeBits(BITS_PULSER_OUT_ENA, uint32_t(0));
	//writeWord(WORD_PAC_ENA, 0, 0xfff);

	writeWord(WORD_BCN0_DELAY, 0, FixedHardwareSettings::TTUTRIG_BC0_DELAY);

}

void TTUTrig::configure(DeviceSettings* settings, int flags) {
	LOG4CPLUS_INFO(logger, "TTUTrig::configure starts.");

	LOG4CPLUS_INFO(logger, "TTUTrig> settings=" << settings );
	if (settings == 0) {
		throw NullPointerException("TTUTrig::configure: settings == 0");
	}

	TtuTriggerSettings* s = dynamic_cast<TtuTriggerSettings*> (settings);

	if (s == 0) {
		throw IllegalArgumentException(
				"TTUTrig::configure: invalid cast for settings");
	}

	bool coldSetup = flags & ConfigurationFlags::FORCE_CONFIGURE;
    if(!coldSetup) {
    	if( (s->getConfigurationId() & 0xfffful) != readWord(WORD_USER_REG1, 0) ) {
    		coldSetup = true;
    	}
    }

    LOG4CPLUS_INFO(logger, getDescription() << " configure: coldSetup = " << (coldSetup ? "true" : "false"));

    if (coldSetup) {
		reset();
		//... basic configuration
		writeWord(WORD_REC_MUX_CLK_INV, 0, s->getRecMuxClkInv());
		writeWord(WORD_REC_MUX_CLK90, 0, s->getRecMuxClk90());
		writeWord(WORD_REC_MUX_REG_ADD, 0, s->getRecMuxRegAdd());

		vector<int>& recMuxDelay = s->getRecMuxDelay();
		for (vector<int>::size_type i = 0; i < recMuxDelay.size(); i++) {
			writeWord(WORD_REC_MUX_DELAY, i, recMuxDelay[i]);
		}

		vector<int>& recDataDelay = s->getRecDataDelay();
		for (vector<int>::size_type i = 0; i < recDataDelay.size(); i++) {
			writeWord(WORD_REC_DATA_DELAY, i, recDataDelay[i]);
		}

		//... TTU-TA specific
		unsigned int ta_trgDelay = s->getTA_TrgDelay();
		writeWord(WORD_TA_TRG_DELAY, 0, ta_trgDelay);
		unsigned int ta_trgTOff = s->getTA_TrgTOff();
		writeWord(WORD_TA_TRG_TOFF, 0, ta_trgTOff);
		unsigned int ta_trgSel = s->getTA_TrgSel();
		writeWord(WORD_TA_TRG_SELECT, 0, ta_trgSel);
		unsigned int ta_trgCntrGate = s->getTA_TrgCntrGate();
		writeWord(WORD_TA_TRG_CNTR_GATE, 0, ta_trgCntrGate);
		unsigned int trg_Majority = s->getTrgMajority();
		writeWord(WORD_TRIG_MAJORITY, 0, trg_Majority);
		unsigned int sec_trgMask = s->getSectorTrigMask();
		writeWord(WORD_SECTOR_TRIG_MASK, 0, sec_trgMask);
		unsigned int sec_Threshold = s->getSectorThreshold();
		writeWord(WORD_SECTOR_THRESHOLD, 0, sec_Threshold);
		unsigned int tow_Threshold = s->getTowerThreshold();
		writeWord(WORD_TOWER_THRESHOLD, 0, tow_Threshold);
		unsigned int whe_Threshold = s->getWheelThreshold();
		writeWord(WORD_WHEEL_THRESHOLD, 0, whe_Threshold);
		unsigned int sec_trgSel = s->getSectorTrgSel();
		writeWord(WORD_SECT_TRG_SEL, 0, sec_trgSel);

		std::cout << "TTUTrig::configure: trgMajority= " << trg_Majority
				<< std::endl;

		//vector quantities (x10)

		vector<int>& taSectorDelay = s->getTA_SectorDelay();
		unsigned int max_vec_size = taSectorDelay.size();
		for (vector<int>::size_type i = 0; i < max_vec_size; i++) {
			writeWord(WORD_TA_SECTOR_DELAY, i, taSectorDelay[i]);
		}

		vector<int>& ta_ForceLogic_0 = s->getTA_ForceLogic0();
		vector<int>& ta_ForceLogic_1 = s->getTA_ForceLogic1();
		vector<int>& ta_ForceLogic_2 = s->getTA_ForceLogic2();
		vector<int>& ta_ForceLogic_3 = s->getTA_ForceLogic3();

		max_vec_size = ta_ForceLogic_0.size();
		for (vector<int>::size_type i = 0; i < max_vec_size; i++) {
			writeWord(WORD_TA_FORCE_LOGIC_0, i, ta_ForceLogic_0[i]);
			writeWord(WORD_TA_FORCE_LOGIC_1, i, ta_ForceLogic_1[i]);
			writeWord(WORD_TA_FORCE_LOGIC_2, i, ta_ForceLogic_2[i]);
			writeWord(WORD_TA_FORCE_LOGIC_3, i, ta_ForceLogic_3[i]);
			std::cout << "TTUTrig::configure: taForceLogic0_" << i << "= "
					<< ta_ForceLogic_0[i] << std::endl;
		}

		vector<int>& trg_Config = s->getTrgConfig();
		max_vec_size = trg_Config.size();
		for (vector<int>::size_type i = 0; i < max_vec_size; i++) {
			writeWord(WORD_TRIG_CONFIG, i, trg_Config[i]);
		}

		vector<int>& trg_Force = s->getTrgForce();
		max_vec_size = trg_Force.size();
		for (vector<int>::size_type i = 0; i < max_vec_size; i++) {
			writeWord(WORD_TRIG_FORCE, i, trg_Force[i]);
		}

		vector<int>& ta_SectorMajority = s->getSectorMajority();
		max_vec_size = ta_SectorMajority.size();
		for (vector<int>::size_type i = 0; i < max_vec_size; i++) {
			writeWord(WORD_SECTOR_MAJORITY, i, ta_SectorMajority[i]);
		}

		vector<int>& taConfig2 = s->getTA_Config2();
		max_vec_size = taConfig2.size();
		for (vector<int>::size_type i = 0; i < max_vec_size; i++) {
			writeWord(WORD_TA_CONFIG2, i, taConfig2[i]);
		}

		writeWord(WORD_REC_CHECK_DATA_ENA, 0, 0xfff);
		writeWord(WORD_REC_CHECK_ENA, 0, 0xfff);

		writeWord(WORD_USER_REG1, 0, s->getConfigurationId() & 0xffff);

		LOG4CPLUS_INFO(logger, "TTU Hardware configuration updated");
	}

	vector<int>& connectedRbcMask = s->getConnectedRbcMask();
	unsigned int max_vec_size = connectedRbcMask.size();
	for (vector<int>::size_type i = 0; i < max_vec_size; i++) {
		//writeWord(WORD_TA_CONFIG2, i, taConfig2[i]);
		std::cout << "Calculated RBC Mask: " << i << " " << connectedRbcMask[i]
				<< '\n';
	}

	vector<int>& trg_Mask = s->getTrgMask();
	max_vec_size = trg_Mask.size();
	for (vector<int>::size_type i = 0; i < max_vec_size; i++) {
		writeWord(WORD_TRIG_MASK, i, ( connectedRbcMask[i] | trg_Mask[i] ) ); //TODO check in db service
	}

	//... resetCounters();
	//reset();

}

TDiagCtrl& TTUTrig::GetDiagCtrl() {
	if (DiagCtrl == 0) {
		DiagCtrl
				= new TDiagCtrl(*this, VECT_DAQ, BITS_DAQ_PROC_REQ,
						BITS_DAQ_PROC_ACK, BITS_DAQ_TIMER_LOC_ENA, 0,
						BITS_DAQ_TIMER_START, BITS_DAQ_TIMER_STOP,
						BITS_DAQ_TIMER_TRIG_SEL, WORD_DAQ_TIMER_LIMIT,
						WORD_DAQ_TIMER_COUNT, WORD_DAQ_TIMER_TRG_DELAY,
						"DAQ_DIAG_CTRL");
	}
	return *DiagCtrl;
}

TStandardDiagnosticReadout& TTUTrig::GetDiagnosticReadout() {
	if (DiagnosticReadout == 0) {
		DiagnosticReadout = new TStandardDiagnosticReadout(*this, "READOUT",
				&TTUTrigBxDataFactory::getInstance(), GetDiagCtrl(), VECT_DAQ,
				BITS_DAQ_EMPTY, BITS_DAQ_EMPTY_ACK, BITS_DAQ_LOST,
				BITS_DAQ_LOST_ACK, VECT_DAQ_WR, BITS_DAQ_WR_ADDR,
				BITS_DAQ_WR_ACK, VECT_DAQ_RD, BITS_DAQ_RD_ADDR,
				BITS_DAQ_RD_ACK, WORD_DAQ_MASK, WORD_DAQ_DATA_DELAY,
				AREA_MEM_DAQ_DIAG, DAQ_DIAG_DATA_SIZE, DAQ_DIAG_TRIG_NUM,
				DAQ_DIAG_TIMER_SIZE, TTC_BCN_EVT_WIDTH);
	}
	return *DiagnosticReadout;
}

TTUTrig::TDiagCtrlVector& TTUTrig::GetDiagCtrls() {
	if (DiagCtrls.empty()) {
		DiagCtrls.push_back(&GetDiagCtrl());
	}
	return DiagCtrls;
}

TTUTrig::TDiagVector& TTUTrig::GetDiags() {
	if (Diags.empty()) {
		Diags.push_back(&GetDiagnosticReadout());
	}
	return Diags;
}

void TTUTrig::ConfigureDiagnosable(TTriggerDataSel triggerDataSel,
		uint32_t dataTrgDelay) {
	writeBits(BITS_STATUS_TRG_DATA_SEL, triggerDataSel);
	writeWord(WORD_DATA_TRG_DELAY, 0, dataTrgDelay);
}

void TTUTrig::resetErrorCounters() {
    writeWord(WORD_REC_ERROR_COUNT, 0, uint32_t(0));
    monRecErrorAnalyzer_.reset();
}

void TTUTrig::enable() {
    resetErrorCounters();
}

void TTUTrig::monitor(volatile bool *stop) {
	monitor(monitorItems_);
}

void TTUTrig::monitor(MonitorItems& items) {

    //...Note: the rates would be deal in a different way
    warningStatusList_.clear();

    for (MonitorItems::iterator iItem = items.begin(); iItem != items.end(); ++iItem) {
        if ((*iItem) == MonitorItemName::FIRMWARE) {
            string msg = monitorVersion();
            if (msg.size() != 0) {
                warningStatusList_.push_back(MonitorableStatus(MonitorItemName::FIRMWARE, MonitorableStatus::ERROR, msg, 0));
            }
        }
        else if ((*iItem) == MonitorItemName::RECEIVER) {
            uint32_t recErrCnt = readWord(WORD_REC_ERROR_COUNT, 0);
            if (monRecErrorAnalyzer_.newValue(recErrCnt) != MonitorableStatus::OK) {
                warningStatusList_.push_back(monRecErrorAnalyzer_.getStatus());
            }
        }
    }

}

//---------------------------------------------------------------------------

Logger TTUBoard::logger = Logger::getInstance("TTUBoard");
void TTUBoard::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}

const HardwareItemType TTUBoard::TYPE(HardwareItemType::cBoard, "TTUBOARD");

TTUBoard::TTUBoard(int ident, const char* desc, TVMEInterface* vme,
		int vme_board_address, VmeCrate* c, int pos) :
	BoardBase(ident, desc, TYPE, c, pos), vme_(vme), ttuBoardVme_(
			vme_board_address, *this), ttuBoardControl_(vme_board_address,
			*this, 0), vmeAddress_(vme_board_address) {
	cout << vme_board_address << endl;
	Init(true);
	ttuBoardVme_.getMemoryMap().SetHardwareIfc(new TIIVMEAccess(
			ttuBoardVme_.GetVMEBoardBaseAddr(), ttuBoardVme_.getBaseAddress()
					<< ttuBoardVme_.GetVMEBaseAddrShift(), vme, 0xf8000), true);
	ttuBoardControl_.getMemoryMap().SetHardwareIfc(new TIIVMEAccess(
			ttuBoardControl_.GetVMEBoardBaseAddr(),
			ttuBoardControl_.getBaseAddress()
					<< ttuBoardControl_.GetVMEBaseAddrShift(), vme, 0), true);

	optos_.assign(6, (TTUOpto*) 0);

	usedOptos_ = boost::dynamic_bitset<>(6, 0ul);
	usedTTUTrigs_ = boost::dynamic_bitset<>(4, 0ul);
	;
}

TTUBoard::~TTUBoard() {
	for (Devices::iterator iDevice = devices_.begin(); iDevice
			!= devices_.end(); ++iDevice)
		delete dynamic_cast<TTUOpto*> (*iDevice);
}

void TTUBoard::Init(bool use_vme) {
	//std::ostringstream ost;
	//ost << "TRG " << std::hex << VMEAddress;
	//Description = ost.str();

	/*IE_RESET_II = "Reset II";
	 IE_INIT_TTC = "Init TTC";
	 //IE_SET_CONTROL_CLOCK = "Set UNI_CONTROL clock and STATUS_TRG_SEL";
	 //IE_SET_OPTO_CLOCK = "Set UNI_OPTO clock";
	 IE_INIT_TLK = "Initialize TLK";
	 //IE_RESET_DELAY = "Reset delay";
	 IE_INIT_QPLL = "Init QPLL";
	 //IE_INIT_OPTO = "Init Opto";
	 InitElements.push_back(IE_RESET_II);
	 InitElements.push_back(IE_INIT_TTC);
	 //InitElements.push_back(IE_SET_CONTROL_CLOCK);
	 //InitElements.push_back(IE_SET_OPTO_CLOCK);
	 //InitElements.push_back(IE_INIT_TLK); <<!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!!!!!!!!! KB
	 //InitElements.push_back(IE_RESET_DELAY);
	 InitElements.push_back(IE_INIT_QPLL);
	 //InitElements.push_back(IE_INIT_OPTO);*/

	if (use_vme) {
		devices_.push_back(&ttuBoardVme_);
		devices_.push_back(&ttuBoardControl_);
	} else {
		devices_.push_back(&ttuBoardControl_);
	}
}

IDevice& TTUBoard::addChip(string type, int pos, int id) {

	transform(type.begin(), type.end(), type.begin(), ::toupper);

	ostringstream ostr;
	ostr << type << ' ' << pos;
	string chipName = ostr.str();

	if (type == string("TTUOPTO")) {
		TTUOpto* chip = new TTUOpto(id, vmeAddress_, pos, chipName.c_str(),
				*this, pos);

		chip->getMemoryMap().SetHardwareIfc(new TIIVMEAccess(
				chip->GetVMEBoardBaseAddr(), chip->getBaseAddress()
						<< chip->GetVMEBaseAddrShift(), vme_, 0), true);
		devices_.push_back(chip);
		optos_[chip->getNumber()] = chip;
		usedOptos_[chip->getNumber()] = true;
		return *chip;
	}

	if (type == string("TTUTRIG")) {
		TTUTrig* chip = new TTUTrig(id, vmeAddress_, pos, chipName.c_str(),
				*this, pos);

		chip->getMemoryMap().SetHardwareIfc(new TIIVMEAccess(
				chip->GetVMEBoardBaseAddr(), chip->getBaseAddress()
						<< chip->GetVMEBaseAddrShift(), vme_, 0), true);
		devices_.push_back(chip);
		ttuTrigs_.push_back(chip);
		usedTTUTrigs_[chip->getNumber()] = true;
		return *chip;
	}

	throw TException(string("Ivalid chip type '") + type + "'");
}

void TTUBoard::configure(ConfigurationSet* configurationSet, int configureFlags) {

	//ttuBoardVme_.writeWord(TTUBoardVme::WORD_RESET, 0, 0x13); I think it is not needed and might cause troubles KB
	//ttuBoardControl_.InitTTC(); //we are using the TTC signals and clock from the TC, not from the TB TTCrx, because it does not work
	//ttuBoardControl_.ResetQPLL();

	BoardBase::configure(configurationSet, configureFlags);

}

bool TTUBoard::CheckLinksOperation() {
	bool good = true;
	for (TTUOptos::iterator optoIt = optos_.begin(); optoIt != optos_.end(); optoIt++) {
		if ((*optoIt) == NULL)
			continue;

		if ((*optoIt)->CheckLinksOperation() == false) {
			good = false;
			//throw TException(" CheckLinksOperation failed");
			LOG4CPLUS_ERROR(logger, "CheckLinksOperation failed on: tb " << getId() << " opto " << (*optoIt)->getId());
		}
	}
	return false;
}

void TTUBoard::EnableOptLink(int linkNum, bool enable) {
	if (optos_[linkNum / TTUOpto::GetOptLinksCount()] != NULL)
		optos_[linkNum / TTUOpto::GetOptLinksCount()]->EnableOptoLink(linkNum
				% TTUOpto::GetOptLinksCount(), enable);
	else {
		throw TException(
				"TTUBoard::EnableOptLink: no Opto for required optLink input");
	}
}

void TTUBoard::Reset() {
	ttuBoardControl_.reset();

	for (TTUOptos::iterator optoIt = optos_.begin(); optoIt != optos_.end(); optoIt++) {
		if ((*optoIt) == NULL)
			continue;
		(*optoIt)->reset();
	}

	for (TTUTrigs::iterator ttuTrigIt = ttuTrigs_.begin(); ttuTrigIt
			!= ttuTrigs_.end(); ttuTrigIt++) {
		if ((*ttuTrigIt) == NULL)
			continue;
		(*ttuTrigIt)->reset();
	}
}

}
