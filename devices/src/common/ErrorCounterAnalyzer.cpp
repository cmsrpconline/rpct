/*
 * ErrorCounterAnalyzer.cpp
 *
 *  Created on: Oct 2, 2008
 *      Author: mpietrus
 */

#include "rpct/devices/ErrorCounterAnalyzer.h"
#include "rpct/std/tbutil.h"


#include <boost/date_time/posix_time/posix_time.hpp>
using namespace rpct;
namespace rpct {

using namespace std;
using namespace boost::posix_time;

ErrorCounterAnalyzer::ErrorCounterAnalyzer(string name)
: minimumPeriod_(1), warningCntThreshold_(1000), errorCntThreshold_(10000),
  warningRateThreshold_(5), errorRateThreshold_(1000), maxCounterVal_(0xffffffff), status_(name, MonitorableStatus::OK, "", 0) {
    reset();
}

ErrorCounterAnalyzer::ErrorCounterAnalyzer(string name, double minimumPeriod, double warningRateThreshold, double errorRateThreshold, uint32_t maxCounterVal)
: minimumPeriod_(minimumPeriod), warningCntThreshold_(1000), errorCntThreshold_(10000),
  warningRateThreshold_(warningRateThreshold), errorRateThreshold_(errorRateThreshold), maxCounterVal_(maxCounterVal),
  status_(name, MonitorableStatus::OK, "", 0) {
    reset();
}

ErrorCounterAnalyzer::ErrorCounterAnalyzer(std::string name, double minimumPeriod, unsigned int warningCntThreshold, unsigned int errorCntThreshold,
		double warningRateThreshold, double errorRateThreshold, uint32_t maxCounterVal)
: minimumPeriod_(minimumPeriod), warningCntThreshold_(warningCntThreshold), errorCntThreshold_(errorCntThreshold),
  warningRateThreshold_(warningRateThreshold), errorRateThreshold_(errorRateThreshold), maxCounterVal_(maxCounterVal),
  status_(name, MonitorableStatus::OK, "", 0) {
    reset();
}

ErrorCounterAnalyzer::~ErrorCounterAnalyzer() {
}

void ErrorCounterAnalyzer::reset() {
    lastTime_ = not_a_date_time;
    lastVal_ = 0;
    status_.level =  MonitorableStatus::OK;
    status_.message = "";
    status_.value = 0;
}


int ErrorCounterAnalyzer::newValue(uint32_t value) {

    ptime t = microsec_clock::local_time();
    if (lastTime_ == not_a_date_time) {
        lastTime_ = t;
        this->lastVal_ = value;
        return status_.level;
    }

    double period = (t - lastTime_).total_milliseconds() / 1000.0;
    if (period < minimumPeriod_) {
        return status_.level;
    }

    double rate = 0;
    if(value == maxCounterVal_ || (value < lastVal_))
    	rate = value / period;
    else
    	rate = (value - lastVal_) / period;

    if(value == maxCounterVal_) {
        status_.level =  MonitorableStatus::ERROR;
    }
    else if (value > errorCntThreshold_) {
        status_.level =  MonitorableStatus::ERROR;
    }
    else if (value > warningCntThreshold_) {
        status_.level =  MonitorableStatus::WARNING;
    }
    else if (value > 0) {
        status_.level =  MonitorableStatus::SOFT_WARNING;
    }

    else if (rate > errorRateThreshold_) {
        status_.level =  MonitorableStatus::ERROR;
    }
    else if (rate > warningRateThreshold_) {
        status_.level =  MonitorableStatus::WARNING;
    }
    else {
        status_.level =  MonitorableStatus::OK;
    }

    if(status_.level !=  MonitorableStatus::OK) {
    	status_.message = status_.monitorItem  + " count = " + rpct::toString(value);
    	if(value == maxCounterVal_ && lastVal_ == maxCounterVal_) {
    		status_.message += ", last rate: MAX";
    	}
    	else {
    		status_.message += ", last rate: " + rpct::toString(rate) + " Hz";
    	}
    	//status_.value = (uint32_t)(rate + 0.5); // round to integer values
    	status_.value = value;
    }

    this->lastTime_ = t;
    this->lastVal_ = value;
    return status_.level;
}


}
