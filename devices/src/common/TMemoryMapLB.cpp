#include "rpct/devices/TMemoryMapLB.h"

using namespace std;

namespace rpct {

void TMemoryMapLB::writeArea(int id, void* data, int start_idx, int n)
{
    //cout << "TMemoryMapLB::writeArea start_idx" << start_idx << " n " << n << endl;
    
    TItemInfo& item = CheckWriteAccess(id);

    if ((start_idx < 0) || (start_idx >= item.Number))
        throw TException("TMemoryMap::writeArea: start_idx out of bounds");

    if (n == npos) {
        n = getItemDesc(id).Number - start_idx;
    }
    else if ((n < 0) || ((n + start_idx) > item.Number)) {
        throw TException("TMemoryMap::writeArea: n out of scope");
    }

    int width = (item.Width-1)/8+1;   // width in bytes

    int blockLen = 1;
    while (blockLen  < item.Number) {
        blockLen <<= 1;
    }    

    if (item.AddrLen > 1) {    
        unsigned char* cdata = (unsigned char*) data;
        unsigned short s;
        for (int i = 0; i < n; i++) {
            for (int block = 0; block < item.AddrLen; block++) {
                //cout << "TMemoryMapLB::writeArea DataSize" << DataSize << endl;
                s = *(cdata + i*width + block*DataSize);
                HardwareIfc->Write(item.AddrPos + block*blockLen + start_idx + i,
                        &s, //cdata + i*width + block*DataSize,
                        1,
                        DataSize);
            }
        }
    }
    else {
        int size = DataSize * n * item.AddrLen;  // how many bytes needed to store the data
        unsigned char* buffer = new unsigned char[size];
        try {

            memset(buffer, 0, size);

            int bytes = (width >= DataSize) ? DataSize : width;

            for (int i = 0; i < n; i++) {
                for (int byte = 0; byte < bytes; byte++) {
                    buffer[i*DataSize + byte] = ((unsigned char*)data)[i*width + byte];
                }
            }
            
            //cout << "MemoryMapLB::writeArea: ";
            for (int i = 0; i < size; i++) {
                //cout << ((unsigned int) buffer[i]);
            }
            //cout << endl;

            HardwareIfc->Write(item.AddrPos + start_idx,
                    buffer,
                    n,
                    DataSize);

            delete [] buffer;
        } catch(...) {
            delete [] buffer;
            throw;
        }
    }

}




void TMemoryMapLB::writeArea(int id, uint32_t* data, int start_idx, int n)
{
    //cout << "TMemoryMapLB::writeArea ul start_idx" << start_idx << " n " << n << endl;
    
    TItemInfo& item = CheckWriteAccess(id);

    if ((start_idx < 0) || (start_idx >= item.Number))
        throw TException("TMemoryMap::writeArea: start_idx out of bounds");

    if (n == npos) {
        n = getItemDesc(id).Number - start_idx;
    }
    else if ((n < 0) || ((n + start_idx) > item.Number)) {
        throw TException("TMemoryMap::writeArea: n out of scope");
    }

    int width = (item.Width-1)/8+1;   // width in bytes

    int blockLen = 1;
    while (blockLen  < item.Number) {
        blockLen <<= 1;
    }    

    if (item.AddrLen > 1) {    
        unsigned char* cdata = (unsigned char*) data;
        for (int i = 0; i < n; i++) {
            for (int block = 0; block < item.AddrLen; block++) {
                HardwareIfc->Write(item.AddrPos + block*blockLen + start_idx + i,
                        cdata + i * sizeof(uint32_t) + block * DataSize,
                        1,
                        DataSize);
            }
        }
    }
    else {
        int size = DataSize * n * item.AddrLen;  // how many bytes needed to store the data
        unsigned char* buffer = new unsigned char[size];
        try {

            memset(buffer, 0, size);

            int bytes = (width >= DataSize) ? DataSize : width;

            for(int i = 0; i < n; i++)
                for(int byte = 0; byte < bytes; byte++)
                    buffer[i*DataSize + byte] = ((unsigned char*)data)[i*sizeof(uint32_t) + byte];

            HardwareIfc->Write(item.AddrPos + start_idx,
                    buffer,
                    n,
                    DataSize);

            delete [] buffer;
        } catch(...) {
            delete [] buffer;
            throw;
        }
    }

}

/*
void TMemoryMapLB::writeArea(int id, uint32_t* data, int start_idx, int n)
{
  TItemInfo& item = CheckWriteAccess(id);

  if (item.Width > sizeof(uint32_t)*8)
    throw TException("TMemoryMap::writeArea: area too wide to fit data to uint32_t");

  if ((start_idx < 0) || (start_idx >= item.Number))
    throw TException("TMemoryMap::writeArea: start_idx out of bounds");

  if (n == npos)
    n = getItemDesc(id).Number - start_idx;
  else
    if ((n < 0) || ((n + start_idx) > item.Number))
      throw TException("TMemoryMap::writeArea: n out of scope");

  // create temporary buffer
  int size = DataSize * item.Number * item.AddrLen;
  char* buffer = new char[size];
  int width = (item.Width-1)/8+1;  

  int blockLen = 1;
  while (blockLen  < item.Number)
      blockLen <<= 1;

  try {

    // reorganize byte order according to DataSize
    for(int block=0; block<item.AddrLen; block++) {

      // copy data appropriate for current block
      memset(buffer, 0, size);

      //int bytes = ((width - block*DataSize) - 1)/DataSize + 1;
      int bytes = width - block*DataSize;
      bytes = (bytes >= DataSize) ? DataSize : bytes;

      for(int i=0; i<n; i++)
        for(int byte=0; byte<bytes; byte++)
          buffer[i*DataSize + byte] = ((char*)data)[i*sizeof(uint32_t) + block*DataSize + byte];

      HardwareIfc->Write(item.AddrPos + block * blockLen + start_idx,
                         buffer,
                         n,
                         DataSize);

    };

    delete [] buffer;
  } catch(...) {
    delete [] buffer;
    throw;
  }
}      */

}
