/*
 * JTAGControllerImpl.cpp
 *
 *  Created on: Jun 13, 2012
 *      Author: Karol Bunkowski
 */
#include "rpct/devices/JTAGControllerImpl.h"
#include <exception>

#include <iostream>
#include <iomanip>
#include <stdint.h>

#include "log4cplus/loggingmacros.h"

using namespace std;
using namespace log4cplus;

namespace rpct {
Logger JTAGControllerImpl::logger_ = Logger::getInstance("JTAGControllerImpl");
void JTAGControllerImpl::setupLogger(std::string namePrefix) {
	logger_ = Logger::getInstance(namePrefix + "." + logger_.getName());
}

void JTAGControllerImpl::lock() {
	//TBD
};


void JTAGControllerImpl::unlock() {
	//TBD
};


/** shift bits through the active JTAG chain
 *
 * @param bitcount is the number of bits to shift
 * @param data_out contains the bits. The first bit to be shifted is the LSB of the first byte in the vector.
 *           if the vector contains less bits than specified by bitcount, then bits of zeros are shifted for the missing bits.
 * @param data_in is filled with the bits read from the JTAG chain, if the parameter doRead is true. If the vector
 *           is too small to contain all the bits, it is resized to contain all the bits. If bitcount is not a multiple of 8,
 *           then the last bit(s) scanned through the chain will be stored in the lowermost bits of the last byte in in_data.
 * @param doRead specifies that data is to be read back form the chain
 * @param autoTMS specifies that TMS should automatically go high while shifting
 *        the last bit in order to move the JTAG state machine to the EXIT1 state
 *
 */
void JTAGControllerImpl::shift(uint32_t num_bits,
		vector<uint8_t> const& data_out,
		vector<uint8_t> &  data_in,
		bool doRead,
		bool autoTMS)
throw(jal::HardwareException,
		jal::TimeoutException,
		jal::OutOfRangeException) {

	if (!_initialized) init();

	uint32_t num_bytes = (num_bits+7) / 8;


	if (_debug_flag >= 4) {
		LOG4CPLUS_DEBUG(logger_, __FUNCTION__<<" called. " << std::dec << num_bits << " bits, TMS_high=" << autoTMS << "; ");
		if (data_out.size() != 0) {
			LOG4CPLUS_DEBUG(logger_, "write = ");
			for (int i = num_bytes-1; i >= 0; i--) {
				LOG4CPLUS_DEBUG(logger_,std::setw(2) << std::setfill('0') << std::hex << (uint32_t)data_out[i]);
			}
		}
	}

	// grow the vector for response data if necessary
	if (doRead && data_in.capacity() < num_bytes)
		data_in.resize(num_bytes);

	for (uint32_t i = 0; i < num_bits; i++) {

		uint32_t byte_index = i/8;
		uint8_t bit_index = i%8;
		uint8_t bit_mask = 1 << bit_index;

		bool tms = false;
		bool tdo = false;

		if (byte_index < num_bytes &&  (data_out[byte_index] & bit_mask) == bit_mask  )
			tdo =  true;

		if (autoTMS && i == num_bits-1)
			tms =  true;

		bool tdi = jtagIO_->JtagIO(tms, tdo, doRead);

		if (doRead) {
			if (bit_index == 0)
				data_in[byte_index]=0;

			if (tdi) // remark: this does not care whether bit 0 or bit 7 is set ...
				data_in[byte_index] |= bit_mask;
		}

	}

	if(jtagIO_->IsCaching() && !doRead) //if doRead -  flush is done during every jtagIO_->JtagIO operation
		jtagIO_->Flush();

	if (doRead &&_debug_flag >= 4) {
		LOG4CPLUS_DEBUG(logger_, "read = ");
		for (int i=(num_bits+7)/8-1; i>=0; i--) {
			LOG4CPLUS_DEBUG(logger_, std::setw(2) << std::setfill('0') << std::hex << (uint32_t)data_in[i]);
		}
	}

	LOG4CPLUS_DEBUG(logger_, __FUNCTION__<<" ended");
}


/// send a TMS seqence on the active JTAG chain (max. 16 bits)
///
/// @param bitcount is the number of bits (max 16)
/// @param sequence contain the sequence to be shifted (in the lowermost bits). Shifting starts with the LSB.
void JTAGControllerImpl::sequenceTMS(uint32_t num_bits, uint32_t tms)
throw(jal::HardwareException,
		jal::TimeoutException,
		jal::OutOfRangeException) {

	if (!_initialized) init();

	if (_debug_flag >= 4) {
		LOG4CPLUS_DEBUG(logger_, __FUNCTION__<<" called. " << std::dec << tms << ", num=" << num_bits);
		for (int i=num_bits-1;i>=0; i--)
			LOG4CPLUS_DEBUG(logger_, "tms["<<i<<"] = "<<((tms & (1 << i)) ? "1" : "0" ));
	}

	if (num_bits > 16)
		XCEPT_RAISE(jal::OutOfRangeException, "sequenceTMS(): num_bits out of range");

	if (num_bits == 0) {
		if (_debug_flag >= 4)
			LOG4CPLUS_DEBUG(logger_, __FUNCTION__<<" numbits is zero. returning.");
		return;
	}

	for (uint32_t i=0; i < num_bits; i++) {
		bool tmsBit = false;
		bool tdo = false;

		uint32_t bit_mask = (1<<i);
		tmsBit = (   (tms & bit_mask)  ==  bit_mask  ) ? true : false;

		jtagIO_->JtagIO(tmsBit, tdo, false);
	}

	if(jtagIO_->IsCaching())
		jtagIO_->Flush();
}

/// pulse the JTAG clock for a number of clock cycles
///
/// @parma num_tcks is the number of JTAG clock cycles.
/// @param tmshigh indicates whether TMS should be held high while clocking.
///        (this is only needed for byteblaster style controllers which
///        steer TMS and CLK with a single register).
/// @param stage gives the stages to perform.
///        It is in the range (jal::PULSESTAGE_ALL, jal::PULSESTAGE_PRE, jal::PULSESTAGE_PAUSE).
///
///        If stage = jal::RTSTAGE_ALL, then the full command should be performed.
///        For the other stage options the behavior depends on the implementation:
///        If the implementation performs the pulseTCK by doing some preparation and then
///        executing a pause, then only the preparation should be performed when called with jal::PULSESTAGE_PRE
///        and only the pause should be performed when called with jal::PULSESTAGE_PAUSE.
///
///        If the implementation does not separate the pulseTCK into two operations, then all operations
///        should be performed when called with jal::PULSESTAGE_PRE,
///        and nothing should be done when called with jal::PULSESTAGE_PAUSE.
///
void JTAGControllerImpl::pulseTCK(uint32_t num_tcks, bool tmshigh, jal::PulseStage stage)
throw(jal::HardwareException,
		jal::TimeoutException,
		jal::OutOfRangeException) {

	if (!_initialized) init();

	if (_debug_flag >= 4)
		LOG4CPLUS_DEBUG(logger_, __FUNCTION__<<" called "<< std::dec << num_tcks );

	if (_desiredTCKfrequency == -1.)
		XCEPT_RAISE(jal::OutOfRangeException, "error: pulseTCK() was called before a desired frequency was specified.");

	//
	// do at least 2 clock cycles before sleeping
	//
	for (int i = 0;i < 2 && num_tcks > 0;i++) {

		if (stage == jal::PULSESTAGE_PRE || stage == jal::PULSESTAGE_ALL) {
			jtagIO_->JtagIO(tmshigh, false, false);
		}
		num_tcks--;
	}
	if(jtagIO_->IsCaching())
		jtagIO_->Flush();

	// return if there was only one clock to do
	if (num_tcks==0)
		return;

	if (_simulatepulsing) {
		XCEPT_RAISE(jal::OutOfRangeException, "error: pulseTCK(): _simulatepulsing  not implemnted");
		/*if (stage == jal::PULSESTAGE_PAUSE || stage == jal::PULSESTAGE_ALL) {
			double t_sleep = num_tcks / _desiredTCKfrequency;
			_timer.sleepMicros( (uint32_t) (t_sleep * 1.e6) );
		}*/

	}
	else {

		if (stage == jal::PULSESTAGE_PRE || stage == jal::PULSESTAGE_ALL) {
			// scale the number of clocks if both the system frequency and the desired frequency are known
			if ( _sck_frequency == -1.)
				XCEPT_RAISE(jal::OutOfRangeException, "error: controller is in pulsing mode, but system frequency was not set.");
			else
				num_tcks = (uint32_t) ( (double) num_tcks * _sck_frequency / _desiredTCKfrequency );

			for (uint32_t i=0; i<num_tcks; i++) {
				jtagIO_->JtagIO(tmshigh, false, false);
			}
		}

		if(jtagIO_->IsCaching())
			jtagIO_->Flush();

	}
}


void JTAGControllerImpl::init()
throw(jal::HardwareException) {

	try {
		//_device.write(_jtag_reg_prefix+"ENABLE", 0x1);
		_initialized = true;
	}
	catch (exception& e) {
		XCEPT_RAISE(jal::HardwareException, string ("error during TAGControllerImpl::init()") + e.what());
	}
}


}



