#include "rpct/devices/IFebChipConfiguration.h"

#include "rpct/ii/feb_const.h"

namespace rpct {

HardwareItemType * IFebChipConfiguration::type_ = 0;

IFebChipConfiguration::IFebChipConfiguration()
{
    if (!type_)
        type_ = new HardwareItemType(HardwareItemType::cDevice, std::string(rpct::type::feb::febchip_));
}
IFebChipConfiguration::~IFebChipConfiguration()
{}

} // namespace rpct
