#include "rpct/devices/Crate.h"
#include "rpct/devices/System.h"
#include "rpct/ii/DeviceSettings.h"
#include "rpct/ii/ConfigurationFlags.h"
#include "rpct/ii/BoardBase.h"
#include "rpct/std/NullPointerException.h"
#include "rpct/devices/LinkBoard.h"
#include "rpct/devices/cb.h"

using namespace rpct;

using namespace std;
using namespace log4cplus;

namespace rpct {

Logger Crate::logger = Logger::getInstance("Crate");
void Crate::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
	//logger = Logger::getInstance(namePrefix + "." + "TCB");
}


Crate::Crate(int ident, const char* desc, std::string const & _label) :
    id_(ident), description_(desc), label_(_label) {
}

Crate::~Crate() {
    for (Boards::iterator iBoard = boards_.begin(); iBoard != boards_.end(); ++iBoard)
        delete *iBoard;
}

bool Crate::checkResetNeeded() {
    for (Boards::iterator iBoard = boards_.begin(); iBoard != boards_.end(); ++iBoard) {
        BoardBase* b = dynamic_cast<BoardBase*>(*iBoard);
        if (b != 0) {
            if (b->checkResetNeeded() == true) {
                return true;
            }
        }
    }
    return false;
}

bool Crate::checkWarm(ConfigurationSet* configurationSet) {

    for (Boards::iterator iBoard = boards_.begin(); iBoard != boards_.end(); ++iBoard) {
        BoardBase* b = dynamic_cast<BoardBase*>(*iBoard);
        ////////////////////////////////////////////////////////// MC - 3 Feb 2010 (start)
        /* now it's obsolete
        // for Link Boards check if halfbox is selected
        LinkBoard* lb = dynamic_cast<LinkBoard*>(*iBoard);
        if(lb != 0 && !(lb->getHalfBox()->isPositionSelected(lb->getPosition()))) {
            LOG4CPLUS_INFO(logger, __FUNCTION__<<": skipped board "<<lb->getDescription());
            continue; // skip this Link Board
        }
        // for Control Boards check if halfbox is selected
        TCB* cb = dynamic_cast<TCB*>(*iBoard);
        LinkBox* lbb = (cb ? dynamic_cast<LinkBox*>(cb->getCrate()) : 0);
        if(cb != 0 && lbb!=0 && !(lbb->getHalfBox(cb->getPosition())->isPositionSelected(cb->getPosition()))) {
            LOG4CPLUS_INFO(logger, __FUNCTION__<<": skipped board "<<cb->getDescription());
            continue; // skip this Control Board
        }
        */
        ////////////////////////////////////////////////////////// MC - 3 Feb 2010 (end)
        if (b != 0) {
            if (b->checkWarm(configurationSet) == false) {
                return false;
            }
        }
    }
    return true;
}

void Crate::checkVersions() {
    std::ostringstream ost;
    EBadDeviceVersion::VersionInfoList errorList;

    for (Boards::iterator iBoard = boards_.begin(); iBoard != boards_.end(); ++iBoard) {
        try {
            (*iBoard)->checkVersions();
        }
        catch(EBadDeviceVersion& e) {
            ost << e.what() << "\n";
            errorList.insert(errorList.end(), e.getVersionInfoList().begin(), e.getVersionInfoList().end());
        }
    }
    if (errorList.size() > 0) {
        throw EBadDeviceVersion(ost.str(), errorList);
    }
}



/*bool Crate::configure(ConfigurationSet* configurationSet, int configureFlags) {

    LOG4CPLUS_INFO(logger, "Configuring crate " << getDescription());

    if (configurationSet == 0 ) {
        throw NullPointerException("Crate::configure: configurationSet is null");
    }

    bool forceReset = configureFlags & ConfigurationFlags::FORCE_RESET;
    bool forceConfigure = configureFlags & ConfigurationFlags::FORCE_CONFIGURE;

    if (!forceReset) {
    	forceReset = checkResetNeeded();
    	configureFlags |= ConfigurationFlags::FORCE_RESET;
    }

    reset(forceReset);

    if (!forceConfigure) {
        forceConfigure = !checkWarm(configurationSet);
        if(forceConfigure)
        	configureFlags |= ConfigurationFlags::FORCE_CONFIGURE;
    }
    LOG4CPLUS_INFO(logger, "forceReset " <<forceReset <<" forceConfigure "<<forceConfigure<<" (configureFlags "<<hex<<configureFlags<<")");

    int boardConfigFlags =  configureFlags;

    for (Boards::iterator iBoard = boards_.begin(); iBoard != boards_.end(); ++iBoard) {
        IBoard* board = *iBoard;
        board->configure(configurationSet, boardConfigFlags);
        board->selfTest();
    }

    afterConfigure(forceConfigure);
    rememberConfiguration(configurationSet);
    return forceConfigure;
}*/

bool Crate::configure(ConfigurationSet* configurationSet, int configureFlags) {
    LOG4CPLUS_INFO(logger, "Configuring crate " << getDescription());

    if (configurationSet == 0 ) {
        throw NullPointerException("Crate::configure: configurationSet is null");
    }

    bool forceConfigure = configureFlags & ConfigurationFlags::FORCE_CONFIGURE;

    if (!forceConfigure) {
        forceConfigure = !checkWarm(configurationSet);
        if(forceConfigure)
        	configureFlags |= ConfigurationFlags::FORCE_CONFIGURE;
    }
    LOG4CPLUS_INFO(logger, "forceConfigure "<<forceConfigure<<" (configureFlags "<<hex<<configureFlags<<")");

    for (Boards::iterator iBoard = boards_.begin(); iBoard != boards_.end(); ++iBoard) {
        IBoard* board = *iBoard;
        board->configure(configurationSet, configureFlags);
        board->selfTest();
    }

    afterConfigure(forceConfigure);
    rememberConfiguration(configurationSet);
    return forceConfigure;
}

void Crate::enable() {
    LOG4CPLUS_INFO(logger, "Enabling crate " << getDescription());

    for (Boards::iterator iBoard = boards_.begin(); iBoard != boards_.end(); ++iBoard) {
        IBoard* board = *iBoard;
        board->enable();
    }
}

void Crate::disable() {
    LOG4CPLUS_INFO(logger, "Disabling crate " << getDescription());

    for (Boards::iterator iBoard = boards_.begin(); iBoard != boards_.end(); ++iBoard) {
        IBoard* board = *iBoard;
        board->disable();
    }
}

void Crate::monitor(volatile bool *stop) {
	//TODO use stop if needed
	for (Boards::iterator iBoard = boards_.begin(); iBoard != boards_.end(); ++iBoard) {
		IBoard* board = (*iBoard);
		IMonitorable* monitorable = dynamic_cast<IMonitorable*> (board);
		if (monitorable != 0) {
			monitorable->monitor(stop);
		}
	}
}

} // namespace

