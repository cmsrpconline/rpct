#include "rpct/devices/DccInputHandler.h"

#include <string>
#include <ostream>
#include <sstream>

using namespace rpct;
using namespace std;


DccInputHandler::DccInputHandler(unsigned int id, TVMEInterface* vme, int vme_board_address)
  : DccChip()
{
  id_ = id;
  int ihAddress = 0x100000*(id+1);
  std::ostringstream oname; oname<<"IH"<<id;
  init( oname.str().c_str(), vme, vme_board_address, ihAddress, 2); 
}
