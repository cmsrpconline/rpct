#include "rpct/devices/LinkBoxFlashConfigurator.h"
#include "rpct/std/tbutil.h"
#include <errno.h>

using namespace std;
using namespace log4cplus;

namespace rpct {


void LinkBoxFlashConfigurator::setCurLinkBoard(LinkBoard* lb) {
    curLinkBoard_ = lb;
    if (curLinkBoard_ != 0) {
        curFile_ = filesMap_[lb->getId()];
        if (curFile_ == 0) {
            string filename = outputDir_ + "/" + linkBox_.getDescription() + "_" + toString(lb->getPosition()) + ".lbflash";
            curFile_ = fopen(filename.c_str(), "w");
            if (curFile_ == 0) {
                throw TException("Could not create file " + filename + ". " + strerror(errno));
            }
            filesMap_[lb->getId()] = curFile_;
        }
    }
}

void LinkBoxFlashConfigurator::writeCommand(FILE* file, unsigned short word1, unsigned short word2) {

    if (file == 0) {
        throw TException("LinkBoxFlashConfigurator::writeCommand: file == 0");
    }
    unsigned char buf[4];
    buf[0] = word1 & 0xff;
    buf[1] = (word1 & 0xff00) >> 8;
    buf[2] = word2 & 0xff;
    buf[3] = (word2 & 0xff00) >> 8;
    
    fwrite(buf, 4, 1, file);
    
    if (logger.isEnabledFor(log4cplus::DEBUG_LOG_LEVEL)) {
        uint32_t val = buf[0];
        val <<= 8;
        val |= buf[1];
        val <<= 8;
        val |= buf[2];
        val <<= 8;
        val |= buf[3];
        
        LOG4CPLUS_DEBUG(logger, linkBox_.getDescription() <<": writeCommand " << hex << val);
    }
}

void LinkBoxFlashConfigurator::writeCommand(unsigned short word1, unsigned short word2) {
    writeCommand(curFile_, word1, word2);
}

void LinkBoxFlashConfigurator::configStart() {
    
}

void LinkBoxFlashConfigurator::configEnd() {
    for (TFilesMap::iterator iEntry = filesMap_.begin(); iEntry != filesMap_.end(); ++iEntry) {
        writeCommand(iEntry->second, 0xffff, 0xffff);
        fclose(iEntry->second);
    }
}


void LinkBoxFlashConfigurator::intiTTCrxs() {

    LOG4CPLUS_INFO(logger, linkBox_.getDescription() <<": intiTTCrxs");
    LinkBox::Boards& boards = linkBox_.getBoards();
    for (LinkBox::Boards::iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {
        setCurLinkBoard(dynamic_cast<LinkBoard*>(*iBoard));
        if(curLinkBoard_ != 0) {
            
            IHardwareIfc* origIfc = curLinkBoard_->getSynCoder().getMemoryMap().GetHardwareIfc();
            bool origOwner = curLinkBoard_->getSynCoder().getMemoryMap().IsHardwareIfcOwner();
            curLinkBoard_->getSynCoder().getMemoryMap().SetHardwareIfc(this, false);
            
            TI2CInterface* origI2c = curLinkBoard_->getTtcRxI2c().GetI2CInterface();
            curLinkBoard_->getTtcRxI2c().SetI2CInterface(*this);
            
            try {            
                curLinkBoard_->setTtcRxI2cAddress();
                uint32_t ttc_status = 0;
                
                //curLinkBoard_->getSynCoder().writeBits(SynCoder::BITS_STATUS_TTC_INIT_ENA, 1);
                //curLinkBoard_->getSynCoder().writeBits(SynCoder::BITS_STATUS_TTC_RESET, 1);
                curLinkBoard_->getSynCoder().setBitsInVector(SynCoder::BITS_STATUS2_TTC_INIT_ENA, 1, ttc_status);
                curLinkBoard_->getSynCoder().setBitsInVector(SynCoder::BITS_STATUS2_TTC_RESET, 1, ttc_status);
                curLinkBoard_->getSynCoder().writeVector(SynCoder::VECT_STATUS2, ttc_status);
    
                waitBit(0, 0);
                /*tbsleep(3000);
                if (curLinkBoard_->getSynCoder().readBits(SynCoder::BITS_STATUS2_TTC_READY) != 0) {
                    throw TException(curLinkBoard_->getDescription() + " SynCoder::ResetTTC: BITS_STATUS2_TTC_READY did not go down after BITS_STATUS2_TTC_RESET = 1");
                }*/
                
                
                //curLinkBoard_->getSynCoder().writeBits(SynCoder::BITS_STATUS2_TTC_RESET, 0);
                //curLinkBoard_->getSynCoder().writeBits(SynCoder::BITS_STATUS2_TTC_INIT_ENA, 0);
                curLinkBoard_->getSynCoder().writeVector(SynCoder::VECT_STATUS2, uint32_t(0));
                LOG4CPLUS_DEBUG(logger, curLinkBoard_->getDescription() + " ResetTTC Done");

                waitBit(0, 1);
                /*tbsleep(500);    
                if (curLinkBoard_->getSynCoder().readBits(SynCoder::BITS_STATUS2_TTC_READY) != 1) {
                    throw TException(curLinkBoard_->getDescription() + " SynCoder::ResetTTC: BITS_STATUS2_TTC_READY did not go up after reset");
                }*/
                //curLinkBoard_->getTtcRxI2c().WriteControlReg(curLinkBoard_->getTtcRxI2c().ReadControlReg() | 0x8);
                curLinkBoard_->getTtcRxI2c().WriteControlReg(0x9b);

                curLinkBoard_->getSynCoder().getMemoryMap().SetHardwareIfc(origIfc, origOwner);  
                curLinkBoard_->getTtcRxI2c().SetI2CInterface(*origI2c);
                
            }
            catch (TException& e) {
                LOG4CPLUS_ERROR(logger, linkBox_.getDescription() <<": intiTTCrxs " << e.what());
                curLinkBoard_->getSynCoder().getMemoryMap().SetHardwareIfc(origIfc, origOwner);  
                curLinkBoard_->getTtcRxI2c().SetI2CInterface(*origI2c);
                throw;
            }
        } 
    }
    
    LOG4CPLUS_INFO(logger, linkBox_.getDescription() <<": intiTTCrxs done");
}



void LinkBoxFlashConfigurator::writeII(LinkBoard& lb, uint32_t address, uint32_t data) {

    LOG4CPLUS_INFO(logger, linkBox_.getDescription() <<": writeII address=" << hex << address << ", data=" << data);
    writeCommand(address, data);
}

void LinkBoxFlashConfigurator::writeI2C(LinkBoard& lb, unsigned char address, unsigned char value) {
    LOG4CPLUS_INFO(logger, linkBox_.getDescription() <<": writeI2C address=" << hex << address << ", data=" << ((int)value));
    unsigned short word2 = address;
    word2 = (word2 & 0xe) << 8;
    word2 |= value;
    writeCommand(0xfffe, word2);
}

void LinkBoxFlashConfigurator::waitBit(int bitNum, int value) {
    LOG4CPLUS_INFO(logger, linkBox_.getDescription() <<": waitBit bitNum=" << hex << bitNum << ", value=" << value);
    writeCommand(0xfffd, ((bitNum << 1) & 0xfffe) | (value & 0x1));
}

// IHardwareIfc interface
void LinkBoxFlashConfigurator::Write(uint32_t address, void* data,
                   size_t word_count, size_t word_size) {

    if (curLinkBoard_ == 0) {
        throw NullPointerException("LinkBoxFlashConfigurator::Write: curLinkBoard_ == 0");
    }
    if (word_size != 2) {
        throw TException("LinkBoxFlashConfigurator::Write: illegal word_size " + toString(word_size));
    }

    for (size_t i = 0; i < word_count; i++) {
        writeII(*curLinkBoard_, address + i, *(((unsigned short*)data) + i));
    }    
}


}
