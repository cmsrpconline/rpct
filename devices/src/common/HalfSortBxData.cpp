#include "rpct/devices/HalfSortBxData.h"
#include "rpct/devices/cmssw/RPCConst.h"
#include "rpct/std/bitcpy.h"
#include "rpct/std/tbutil.h"
#include "rpct/devices/hsb.h"

#include <iomanip>
#include <sstream>

using namespace std;
using namespace rpct;

HalfSortBxDataFactory HalfSortBxDataFactory::instance_;


HalfSortBxData::HalfSortBxData(IDiagnosticReadout::BxData& bxData, IDiagnosticReadout& ownerReadout)
  : StandardBxData(bxData) {

  	const boost::dynamic_bitset<>& enabledInputs = (dynamic_cast<THsbSortHalf&>(ownerReadout.GetOwner())).GetEnabledInputs(); //cables inputs

	unsigned int bcn0Bit = 0; //1 bit
	unsigned int bcnBitsCnt = 5; //
	unsigned int bcnData = 0;

	unsigned int channelValidBitsCnt = 16;

	  //
	  // pack HS muons
	  //
	unsigned int pos = 0;
	unsigned int hsbOutMuonsCnt = 8;
	unsigned int hsbMuonBitsCnt = RPCHalfSortOutMuon::getMuonBitsCnt();

    unsigned int index=0;
  	for(unsigned int iMu = 0; iMu < hsbOutMuonsCnt; iMu++, ++index) {
	    unsigned int muonData = 0;
	    bitcpy(&muonData, 0, bxData_.getData(), pos, hsbMuonBitsCnt);
	    pos += hsbMuonBitsCnt;
	    RPCHalfSortOutMuon* muon = new RPCHalfSortOutMuon(muonData, index);
	    dataStr_ = muon->toString(1) + " | " + dataStr_;
	    if(muonData != 0)
	    	muonVector_.push_back(muon);
	    else
	    	delete muon;
  	}

  	dataStr_ = "out| " + dataStr_;

  	bitcpy(&bcn0Bit,    0, bxData_.getData(), pos, 1);                                pos += 1;
  	bitcpy(&bcnData,    0, bxData_.getData(), pos, bcnBitsCnt);                       pos += bcnBitsCnt;
	bitcpy(&channelsValid_,    0, bxData_.getData(), pos, channelValidBitsCnt);       pos += channelValidBitsCnt;
	  //
	  // pack TC muons
	  //
	unsigned int tcsCnt = 8;
	unsigned int tcOutMuonsCnt = 4;
	unsigned int tcMuonBitsCnt = RPCTcSortOutMuon::getMuonBitsCnt(); // 21

  	for (unsigned int iTc = 0; iTc < tcsCnt; ++iTc) {
   		for (unsigned int iMu = 0; iMu < tcOutMuonsCnt; ++iMu, ++index) {
	      	unsigned int muonData = 0;
	      	bitcpy(&muonData, 0, bxData_.getData(), pos, tcMuonBitsCnt);           pos += tcMuonBitsCnt;
	      	RPCTcSortOutMuon * muon = new RPCTcSortOutMuon(muonData,index);
	      	//dataStr_ = muon->toString(1) + " | " + dataStr_;
	      	ostringstream ostr;
	      	ostr<<hex<<setfill('0')<<setw(tcMuonBitsCnt/4 + 1)<<muonData;
	      	dataStr_ = ostr.str() + " | " + dataStr_;

	      	cerr<<"enabledInputs "<<enabledInputs<<" size "<<enabledInputs.size()<<endl;
	      	cout<<"iTc*2 + iMu/2 "<<(iTc*2 + iMu/2)<<endl;
	      	if(enabledInputs[iTc*2 + iMu/2] && muonData != 0)
	      		muonVector_.push_back(muon);
	      	else
	      		delete muon;

	      	if(iMu%2 == 1) {
	    		bitcpy(&bcn0Bit,    0, bxData_.getData(), pos, 1);                 pos += 1;
				bitcpy(&bcnData,    0, bxData_.getData(), pos, bcnBitsCnt);        pos += bcnBitsCnt;
				ostringstream ostr;
				ostr<<"|"<<setw(3)<<bcnData<<" "<<bcn0Bit;
				dataStr_ = ostr.str() +  " | " + dataStr_;
	      	}
    	}
   		ostringstream ostr;
   		ostr<<iTc;
		dataStr_ = ostr.str() + dataStr_;
  	}
  	ostringstream ostr;
  	ostr<<hex<<setw(3)<<bcnData<<" "<<bcn0Bit<<" "<<channelsValid_;
    dataStr_ = ostr.str() + " | " + dataStr_;
}



