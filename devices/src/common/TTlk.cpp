//---------------------------------------------------------------------------
#ifdef __BORLANDC__
#include "precompiled.h"
#pragma hdrstop
#endif

#include "rpct/devices/TTlk.h"


namespace rpct {


TTlk::TState TTlk::GetTxState()
{
  uint32_t data = Owner.readVector(VECT_TLK);
  uint32_t txena = Owner.getBitsInVector(BITS_TLK_TX_ENA, data);
  uint32_t txerr = Owner.getBitsInVector(BITS_TLK_TX_ERR, data);
  int state = txerr + 2 * txena;
  if (state == 0)
    return stIdle;
  else if (state == 1)
    return stCarrier;
  else if (state == 2)
    return stNormal;
  else if (state == 3)
    return stError;
  throw TException("Unknown state");
}

void TTlk::SetTxState(TState state)
{
  uint32_t data = Owner.readVector(VECT_TLK);
  if (state == stIdle) {
    Owner.setBitsInVector(BITS_TLK_TX_ENA, 0, data);
    Owner.setBitsInVector(BITS_TLK_TX_ERR, 0, data);
  }
  else if (state == stCarrier) {
    Owner.setBitsInVector(BITS_TLK_TX_ENA, 0, data);
    Owner.setBitsInVector(BITS_TLK_TX_ERR, 1, data);
  }
  else if (state == stNormal) {
    Owner.setBitsInVector(BITS_TLK_TX_ENA, 1, data);
    Owner.setBitsInVector(BITS_TLK_TX_ERR, 0, data);
  }                                         
  else if (state == stError) {
    Owner.setBitsInVector(BITS_TLK_TX_ENA, 1, data);
    Owner.setBitsInVector(BITS_TLK_TX_ERR, 1, data);
  }
  else
    throw TException("Unknown state");

  Owner.writeVector(VECT_TLK, data);
}


TTlk::TState TTlk::GetRxState()
{
  uint32_t data = Owner.readVector(VECT_TLK);
  uint32_t rxena = Owner.getBitsInVector(BITS_TLK_RX_DV, data);
  uint32_t rxerr = Owner.getBitsInVector(BITS_TLK_RX_ERR, data);
  int state = rxerr + 2 * rxena;
  if (state == 0)
    return stIdle;
  else if (state == 1)
    return stCarrier;
  else if (state == 2)
    return stNormal;
  else if (state == 3)
    return stError;
  throw TException("Unknown state");
}


void TTlk::Init()
{
  SetFlags(false, true, false, false);
  tbsleep(100);
  SetFlags(true, true, false, false);
}

}
