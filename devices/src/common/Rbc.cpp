//---------------------------------------------------------------------------
//
//  Karol Bunkowski 2009
//  Warsaw University
//  Andres Osorio 2009
//  Universidad de los Andes

#include "rpct/devices/cb.h"   // MC, 20 Feb 2010
#include "rpct/devices/Rbc.h"
#include "rpct/devices/RbcDeviceSettings.h"

#include "rpct/std/NullPointerException.h"
#include "rpct/ii/ConfigurationFlags.h"
#include <bitset>

#include "rpct/lboxaccess/cbpc_const.h"

#include <cstdlib>


using namespace std;
using namespace log4cplus;

namespace rpct {

//........... RbcDevice definition .................

const HardwareItemType RbcDevice::TYPE(HardwareItemType::cDevice, "RBCDEVICE");

Logger RbcDevice::logger = Logger::getInstance("Rbc");
void RbcDevice::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}
// ... for monitoring
const char* RbcDevice::MON_STATUS_REG = "STATUS_REG";
const char* RbcDevice::MON_BCO_oddA_LATCH = "BCO_oddA_latch";
const char* RbcDevice::MON_BCO_oddB_LATCH = "BCO_oddB_latch";
const char* RbcDevice::MON_BCO_evenA_LATCH = "BCO_evenA_latch";
const char* RbcDevice::MON_BCO_evenB_LATCH = "BCO_evenB_latch";
const char* RbcDevice::MON_BCO_oddA_ERR = "BCO_oddA_err";
const char* RbcDevice::MON_BCO_oddB_ERR = "BCO_oddB_err";
const char* RbcDevice::MON_BCO_evenA_ERR = "BCO_evenA_err";
const char* RbcDevice::MON_BCO_evenB_ERR = "BCO_evenB_err";

const uint8_t RbcDevice::WORD_GOL = 0x07;
const uint8_t RbcDevice::WORD_QPLL = 0x08;
const uint8_t RbcDevice::WORD_STATUS_REG = 0x02;
const uint8_t RbcDevice::WORD_BCO_oddA_LATCH = 0x3A;
const uint8_t RbcDevice::WORD_BCO_oddB_LATCH = 0x3B;
const uint8_t RbcDevice::WORD_BCO_evenA_LATCH = 0x3C;
const uint8_t RbcDevice::WORD_BCO_evenB_LATCH = 0x3D;
const uint8_t RbcDevice::WORD_BCO_oddA_ERR = 0x50;
const uint8_t RbcDevice::WORD_BCO_oddB_ERR = 0x51;
const uint8_t RbcDevice::WORD_BCO_evenA_ERR = 0x52;
const uint8_t RbcDevice::WORD_BCO_evenB_ERR = 0x53;
const uint8_t RbcDevice::WORD_MASK_ODD_A = 0x10;
const uint8_t RbcDevice::WORD_MASK_ODD_B = 0x11;
const uint8_t RbcDevice::WORD_MASK_EVEN_A = 0x12;
const uint8_t RbcDevice::WORD_MASK_EVEN_B = 0x13;

// ... for configuration
const uint8_t RbcDevice::WORD_CTRL = 0x00;
const uint8_t RbcDevice::WORD_REC_CONF = 0x01;
const uint8_t RbcDevice::WORD_REC_MAJ_REG = 0x03;
const uint8_t RbcDevice::WORD_SHAPE = 0x05;
const uint8_t RbcDevice::WORD_REC_CONFIG_IN = 0x06;
const uint8_t RbcDevice::WORD_REC_CONFIG_VER = 0x0E;

const uint8_t RbcDevice::WORD_PROM_REG = 0x24;

RbcDevice::RbcDevice(int id, IBoard& board, ICB* cb) :
	id_(id), i2cId_(8), description_("RBCDEVICE"), cb_(cb),
			monRecErrorAnalyzer_("MON_BCO_ERR_CNTR", 1, 1, 1000, 0xffffff) {
    rbc_ =  &(dynamic_cast<Rbc&> (board) );
	monitorItems_.push_back(MON_BCO_oddA_LATCH);
	monitorItems_.push_back(MON_BCO_oddB_LATCH);
	monitorItems_.push_back(MON_BCO_evenA_LATCH);
	monitorItems_.push_back(MON_BCO_evenB_LATCH);
	monitorItems_.push_back(MON_STATUS_REG); //<- the order matters.

	//monitorItems_.push_back(MON_BCO_oddA_ERR);
	//monitorItems_.push_back(MON_BCO_oddB_ERR);
	//monitorItems_.push_back(MON_BCO_evenA_ERR);
	//monitorItems_.push_back(MON_BCO_evenB_ERR);

	setName(board.getDescription() + " " + this->getDescription());
}

TI2CInterface& RbcDevice::prepareI2C() {
	LOG4CPLUS_DEBUG(logger, "preparing I2C for " << getDescription());
	//cb_->selectI2CChannel(8);
	cb_->selectI2CChannel(i2cId_);
	return cb_->getI2C();
}

//TODO check
//TODO check types!!!!!
uint8_t RbcDevice::read(uint8_t address) {
    int rep = 2;
    for(int i = 0; i < rep; i++) {
        try {
            TI2CInterface& i2c = prepareI2C();
            i2c.Write8(0x0, (int) address); /* Write the reg. Address into the Address Pointer */
            return i2c.Read8(0x1);
        }
        catch(EI2C& e) {
            if(i == rep-1) {
                throw EI2CRbc(string(" exception during RbcDevice::read(): ") + string(e.what()) + " operation unsuccessful", rbc_);
            }
            LOG4CPLUS_WARN(logger, rbcName_ <<" exception during RbcDevice::read(): "<<e.what()<< ". Trying to repeat");
            unsigned short channel = (dynamic_cast<TCB*>(cb_))->memoryRead(CBPC::CBPC_I2C_CHAN);
            LOG4CPLUS_WARN(logger, rbcName_ <<"readback of the CBPC::CBPC_I2C_CHAN: is :"<<channel<<" should be "<<i2cId_);
        }
    }
    return 0;
}

//TODO check; add the error handling
//TODO check types!!!!!
void RbcDevice::write(uint8_t address, uint8_t data) {
/*    if((rand() % 10) == 0) {
        throw EI2CRbc(rbcName_ + string(" test exception during RbcDevice::write(): "), rbc_);
    }*/

    int rep = 3;
    for(int i = 0; i < rep; i++) {
        try {
            TI2CInterface& i2c = prepareI2C();
            i2c.Write8(0x0, address); /* Write the reg. Address into the Address Pointer */
            i2c.Write8(0x1, data);
        }
        catch(EI2C& e) {
            if(i == rep-1) {
                throw EI2CRbc(string(" exception during RbcDevice::write(): ") + string(e.what()), rbc_);
            }
            LOG4CPLUS_WARN(logger, rbcName_ <<" exception during RbcDevice::write(): "<<e.what()<< ". Trying to repeat");
            unsigned short channel = (dynamic_cast<TCB*>(cb_))->memoryRead(CBPC::CBPC_I2C_CHAN);
            LOG4CPLUS_WARN(logger, rbcName_ <<"readback of the CBPC::CBPC_I2C_CHAN: is :"<<channel<<" should be "<<i2cId_);
        }
    }
}

//this function probably will not be needed
bool RbcDevice::checkResetNeeded() {
	return false;
}

void RbcDevice::reset() {
    LOG4CPLUS_INFO(logger, rbcName_<<" RbcDevice::reset() started");

/*    if((rand() % 5) == 0) {
        throw EI2CRbc(rbcName_ + string(" test exception during RbcDevice::write(): "), rbc_);
    }*/

	//.... reset both GOL and QPLL
	this->write(WORD_QPLL, 0x80);
	this->write(WORD_GOL, 0x80);
	//this->write(WORD_CTRL, 0x80);
	LOG4CPLUS_INFO(logger, rbcName_<<" RbcDevice::reset() done");
}

void RbcDevice::setName(const std::string & name) {
	this->rbcName_ = name;
}

void RbcDevice::enable() {
	try {
		//. read the value in the CTRL register
		uint8_t ctrl_value = this->read(WORD_CTRL);

		//CRTL[7]=1 resets all other counters in RBCs
		this->write(WORD_CTRL, 0x80 | ctrl_value);
	}
	catch(EI2CRbc& e) {
		LOG4CPLUS_ERROR(logger, "Error during "<<__FUNCTION__<<" in "<<rbcName_<<": "<<e.what()<<" RBC is not good, but we must live with that");
		//TODO - remove if problems with the I2C communication after the firmware reloading are solved
	}
}

void RbcDevice::configure(DeviceSettings* settings, int configureFlags) {
	LOG4CPLUS_INFO(logger, "configuring "<< rbcName_ );
	if (settings == 0) {
		throw NullPointerException("RbcDevice::configure: settings == 0");
	}
	else {
		RbcDeviceSettings* s = dynamic_cast<RbcDeviceSettings*> (settings);
		if (s == 0) {
			throw IllegalArgumentException(
					"RbcDevice::configure: invalid cast for settings");
		}

		LOG4CPLUS_INFO(logger, "Configuring RBC");

		//bool coldNeeded = false;
		int config_id = s->getConfigurationId();
		uint8_t readVersionId = this->read(WORD_REC_CONFIG_VER);

		if (configureFlags & ConfigurationFlags::FORCE_CONFIGURE || readVersionId != (config_id & 0xff)) {
			LOG4CPLUS_INFO(logger, "RBC Cold setup");

			unsigned int config = s->getRbcDeviceConfig();
			unsigned int config_in = s->getRbcDeviceConfigIn();
			unsigned int majority = s->getRbcDeviceMajority();
			unsigned int shape = s->getRbcDeviceShape();
			unsigned int ctrl = s->getRbcCtrl();

			std::cout << "RBC::configure: registers read from DB: "
					<< config << '\t' << config_in << '\t' << majority
					<< '\t' << shape << '\t' << ctrl << '\n';

			channelMask_ = s->getRbcMask();
			if(channelMask_ != 0)
				LOG4CPLUS_INFO(logger, "channelMask: 0x"<<hex<<channelMask_);

			this->write(WORD_REC_CONF, config);
			this->write(WORD_REC_CONFIG_IN, config_in); ///this changes the phase of the clock
			this->write(WORD_REC_MAJ_REG, majority);
			this->write(WORD_REC_CONFIG_VER, config_id); ///update with the new config_id value
			this->write(WORD_SHAPE, shape);
			this->write(WORD_CTRL, ctrl);

			unsigned int maskChannel = (channelMask_ & 0xff);
			this->write(WORD_MASK_ODD_A, maskChannel);
			maskChannel = (channelMask_ & 0xff00) >> 8;
			this->write(WORD_MASK_ODD_B, maskChannel);
			maskChannel = (channelMask_ & 0xff0000) >> 16;
			this->write(WORD_MASK_EVEN_A, maskChannel);
			maskChannel = (channelMask_ & 0xff000000) >> 24;
			this->write(WORD_MASK_EVEN_B, maskChannel);

			//send the sync word - be sure the link boards are ready (if RBC are last then weloadFromFlash could safely send the sync)
			LOG4CPLUS_INFO(logger, "RBC Hardware configuration updated");
		}
	}

	LOG4CPLUS_DEBUG(logger, rbcName_<<" channelMask_ " <<channelMask_);
	enable();
}

void RbcDevice::checkConfiguration(ConfigurationSet* configurationSet) {
	DeviceSettings* settings = configurationSet->getDeviceSettings(getId());

	LOG4CPLUS_INFO(logger, "RbcDevice::checkConfiguration "<< rbcName_ );
	if (settings == 0) {
		throw NullPointerException("RbcDevice::configure: settings == 0");
	}
	RbcDeviceSettings* s = dynamic_cast<RbcDeviceSettings*> (settings);
	if (s == 0) {
		throw IllegalArgumentException(
				"RbcDevice::configure: invalid cast for settings");
	}

	unsigned int config_id = s->getConfigurationId();

	unsigned int config = s->getRbcDeviceConfig();
	unsigned int config_in = s->getRbcDeviceConfigIn();
	unsigned int majority = s->getRbcDeviceMajority();
	unsigned int shape = s->getRbcDeviceShape();
	unsigned int ctrl = s->getRbcCtrl();

	unsigned int config_id_read = this->read(WORD_REC_CONFIG_VER);
	if( config_id_read !=  config_id)
		LOG4CPLUS_INFO(logger, "  config_id_read: 0x"<<hex<< config_id_read<<"  config_id from db: 0x"<<hex<< config_id);

	unsigned int config_read = this->read(WORD_REC_CONF);
	if(config_read != config)
		LOG4CPLUS_INFO(logger, "  config_read: 0x"<<hex<<config_read<<" config from db: 0x"<<hex<<config);

	unsigned int config_in_read = this->read(WORD_REC_CONFIG_IN);
	if(config_in_read != config_in)
			LOG4CPLUS_INFO(logger, "  config_in_read: 0x"<<hex<<config_in_read<<" config_in from db: 0x"<<hex<<config_in);

	unsigned int majority_read = this->read(WORD_REC_MAJ_REG);
	if(majority_read != majority)
			LOG4CPLUS_INFO(logger, "  majority_read: 0x"<<hex<<majority_read<<" majority from db: 0x"<<hex<<majority);

	unsigned int shape_read = this->read(WORD_SHAPE);
	if( shape_read !=  shape)
			LOG4CPLUS_INFO(logger, "  shape_read: 0x"<<hex<< shape_read<<"  shape from db: 0x"<<hex<< shape);

	unsigned int ctrl_read = this->read(WORD_CTRL);
	if(ctrl_read != ctrl)
			LOG4CPLUS_INFO(logger, "  ctrl_read: 0x"<<hex<<ctrl_read<<" ctrl from db: 0x"<<hex<<ctrl);


	channelMask_ = s->getRbcMask();

	unsigned int maskChannel_read = 0;
	maskChannel_read = this->read(WORD_MASK_ODD_A);
	maskChannel_read |= ((unsigned int)this->read(WORD_MASK_ODD_B)) << 8;
	maskChannel_read |= ((unsigned int)this->read(WORD_MASK_EVEN_A)) << 16;
	maskChannel_read |= ((unsigned int)this->read(WORD_MASK_EVEN_B)) << 24;

	if(channelMask_ != maskChannel_read)
		LOG4CPLUS_INFO(logger, "  maskChannel_read "<<maskChannel_read<<" channelMask from DB: 0x"<<hex<<channelMask_);
}

void RbcDevice::synchronizeLink() {
	this->write(WORD_GOL, 0x40);
	LOG4CPLUS_INFO(logger, "RBCDevice: synchronization command executed");
}

void RbcDevice::monitor(volatile bool *stop) {
	monitor(monitorItems_);
}

/*void RbcDevice::monitor(MonitorItems& items) {
	warningStatusList_.clear();

	bool hasSyncProblem = false;

    MonitorableStatus monStatus(MonitorItemName::RBC, MonitorableStatus::OK, "", 0) ;
	for (MonitorItems::iterator iItem = items.begin(); iItem != items.end(); ++iItem) {

		 * MON_BCO_odd/even_LATCH: all bits should be at 11111111
		 * Counters BC0 no properly latched: they should be at 0: if !=0 expert intervention is needed
		 * sending the sync command will reset the counter
		 *
		string warnigMessage;
		if ((*iItem) == MON_BCO_oddA_LATCH) {
			unsigned int status = this->read(WORD_BCO_oddA_LATCH);
			LOG4CPLUS_DEBUG(logger, "RBCDevice MON> status: " << status );
			if ((status & 0xff) != (~channelMask_ & 0xff)) {
				hasSyncProblem = true;

				int recErrCnt = this->read(WORD_BCO_oddA_ERR);

				monStatus.level = MonitorableStatus::ERROR;
				monStatus.message +=  string(MON_BCO_oddA_LATCH) + " not good, error cnt: " + toString(recErrCnt);
			}

		} else if ((*iItem) == MON_BCO_oddB_LATCH) {
			unsigned int status = this->read(WORD_BCO_oddB_LATCH);
			LOG4CPLUS_DEBUG(logger, "RBCDevice MON> status: " << status );
			if ( ( status & 0xff ) != ((~channelMask_ & 0xff00) >> 8)) {
				hasSyncProblem = true;

				int recErrCnt = this->read(WORD_BCO_oddB_ERR);

                monStatus.level = MonitorableStatus::ERROR;
                monStatus.message +=  string(MON_BCO_oddB_LATCH) + " not good, error cnt: " + toString(recErrCnt);
			}
		} else if ((*iItem) == MON_BCO_evenA_LATCH) {
			unsigned int status = this->read(WORD_BCO_evenA_LATCH);
			LOG4CPLUS_DEBUG(logger, "RBCDevice MON> status: " << status );
			if ( ( status & 0xff ) != ((~channelMask_ & 0xff0000) >> 16)) {
				hasSyncProblem = true;

				int recErrCnt = this->read(WORD_BCO_evenA_ERR);

				monStatus.level = MonitorableStatus::ERROR;
				monStatus.message +=  string(MON_BCO_evenA_LATCH) + " not good, error cnt: " + toString(recErrCnt);
			}
		} else if ((*iItem) == MON_BCO_evenB_LATCH) {
			unsigned int status = this->read(WORD_BCO_evenB_LATCH);
			LOG4CPLUS_DEBUG(logger, "RBCDevice MON> status: " << status );
			if ( ( status & 0xff ) != ((~channelMask_ & 0xff000000) >> 24)) {
				hasSyncProblem = true;

				int recErrCnt = this->read(WORD_BCO_evenB_ERR);

				monStatus.level = MonitorableStatus::ERROR;
				monStatus.message +=  string(MON_BCO_evenB_LATCH) + " not good, error cnt: " + toString(recErrCnt);
			}
		} else if ((*iItem) == MON_STATUS_REG) {
			int status = this->read(WORD_STATUS_REG);
			if (status != 3 && hasSyncProblem) {
				std::bitset<8> statusBits(status);
				if (statusBits[0] == 1 && statusBits[1] == 1 && statusBits[2] == 0) {
					if (statusBits[6]) {
					    monStatus.level = MonitorableStatus::ERROR;
					    monStatus.message +=  "Sync. Problem in Odd sector, status reg is " + statusBits.to_string<char,char_traits<char>,allocator<char> >();
					}
					if (statusBits[7]) {
					    monStatus.level = MonitorableStatus::ERROR;
					    monStatus.message +=  "Sync. Problem in Even sector, status reg is " + statusBits.to_string<char,char_traits<char>,allocator<char> >();
					}
				} else {
                    monStatus.level = MonitorableStatus::ERROR;
                    monStatus.message +=  "QPLL or GOL sync problem, status reg is " + statusBits.to_string<char,char_traits<char>,allocator<char> >();
				}
			}
		}
	}

    if(monStatus.level != MonitorableStatus::OK)
        warningStatusList_.push_back(monStatus);
}*/

void RbcDevice::monitor(MonitorItems& items) {
	warningStatusList_.clear();

	MonitorableStatus monStatus(MonitorItemName::RBC, MonitorableStatus::OK, "", 0) ;

	try {
		for (MonitorItems::iterator iItem = items.begin(); iItem != items.end(); ++iItem) {
			/*
			 * MON_BCO_odd/even_LATCH: all bits should be at 11111111
			 * Counters BC0 no properly latched: they should be at 0: if !=0 expert intervention is needed
			 * sending the sync command will reset the counter
			 *
			 * the description of STATUS_REG is the following:

            STATUS_REG[0] <= #1 GOL_READY ;      // If 1, OK.  If '0', problem in GOL
            STATUS_REG[1] <= #1 QPLL_LOCKED ;  // If 1, OK.  If '0', QPLL is not locked
            STATUS_REG[2] <= #1 QPLL_ERR ;         // If 0, OK.  If '1', error in QPLL
            STATUS_REG[3] <= #1 0;

            STATUS_REG[4] <= #1 TX_FAULT;      //  If 1, error in the optical driver
            STATUS_REG[5] <= #1 0
            STATUS_REG[6] <= #1 err_BC0_odd;     //  If '1',  BC0 sync of odd sector failed
            STATUS_REG[7] <= #1 err_BC0_even;    // If '1',  BC0 sync of even sector failed

            The last 2 bits are triggered to '1' if a wrong synchronization of BC0 bit happens (in this case, the words MON_BC0_odd and MON_BC0_even start to count the number of errors).
            It could happen that there is a temporary problem in the clock and these bits are triggered to '1'.
            After sometime, the problem is automatically recovered and MON_BC0_xxx stop to increment their value: so the RBC is working well now, but the STATUS_REG still keeps tracks of the previous problem.
            The 2 bits  STATUS_REG[6] and STATUS_REG[7]), together with the MON_BC0_xxx error counters are reset with the sync procedure of RBC.

            Flavio
			 *
			 * */
			string warnigMessage;
			if ((*iItem) == MON_STATUS_REG) {
				int status = this->read(WORD_STATUS_REG);
				if (status != 3) {
					std::bitset<8> statusBits(status);
					monStatus.message +=  "STATUS_REG is " + statusBits.to_string<char,char_traits<char>,allocator<char> >() + ". ";

					if (statusBits[0] == 0) {
						monStatus.level = MonitorableStatus::ERROR;
						monStatus.message +=  "GOL problem. ";
					}
					if (statusBits[1] == 0) {
						monStatus.level = MonitorableStatus::ERROR;
						monStatus.message +=  "QPLL is not locked. ";
					}
					if (statusBits[2] == 1) {
						monStatus.level = MonitorableStatus::ERROR;
						monStatus.message +=  "Error in QPLL. ";
					}
					if (statusBits[4] == 1) {
						monStatus.level = MonitorableStatus::ERROR;
						monStatus.message +=  "Error in the optical driver. ";
					}
					if (statusBits[6]) {
						monStatus.message +=  "Sync. Problem in Odd sector. ";
						{
							unsigned int status = this->read(WORD_BCO_oddA_LATCH);
							//LOG4CPLUS_WARN(logger,  rbcName_<<" "<< monStatus.message<<" BCO_oddA_LATCH: "<<hex<< status );
							//the worning is printed, even though it is due to the connections that are masked, therefore the monitoring error is not generated
							if ((status & 0xff) != (~channelMask_ & 0xff)) {
								monStatus.level = MonitorableStatus::ERROR;
								monStatus.message += "BCO_oddA_LATCH " + toHexString(status);

								int recErrCnt = this->read(WORD_BCO_oddA_ERR);
								monStatus.message +=  " BCO_oddA_ERR error cnt: " + toString(recErrCnt);
							}
						}
						{
							unsigned int status = this->read(WORD_BCO_oddB_LATCH);
							//LOG4CPLUS_WARN(logger,  rbcName_<<" "<< monStatus.message<<" BCO_oddB_LATCH: "<<hex<< status );
							if ( ( status & 0xff ) != ((~channelMask_ & 0xff00) >> 8)) {
								monStatus.level = MonitorableStatus::ERROR;
								monStatus.message += "BCO_oddB_LATCH " + toHexString(status);

								int recErrCnt = this->read(WORD_BCO_oddB_ERR);
								monStatus.message +=  " BCO_oddB_ERR error cnt: " + toString(recErrCnt);
							}
						}
					}
					if (statusBits[7]) {
						monStatus.message +=  "Sync. Problem in Even sector. ";
						{
							unsigned int status = this->read(WORD_BCO_evenA_LATCH);
							//LOG4CPLUS_WARN(logger, rbcName_<<" "<< monStatus.message<<" BCO_evenA_LATCH: " <<hex<< status );
							if ( ( status & 0xff ) != ((~channelMask_ & 0xff0000) >> 16)) {
								monStatus.level = MonitorableStatus::ERROR;
								monStatus.message += " BCO_evenA_LATCH " + toHexString(status);

								int recErrCnt = this->read(WORD_BCO_evenA_ERR);
								monStatus.message +=  "BCO_evenA_ERR cnt: " + toString(recErrCnt);
							}
						}
						{
							unsigned int status = this->read(WORD_BCO_evenB_LATCH);
							//LOG4CPLUS_WARN(logger, rbcName_<<" "<< monStatus.message<<" BCO_evenB_LATCH: " <<hex<< status );
							if ( ( status & 0xff ) != ((~channelMask_ & 0xff000000) >> 24)) {
								monStatus.level = MonitorableStatus::ERROR;
								monStatus.message += "BCO_evenB_LATCH " + toHexString(status);

								int recErrCnt = this->read(WORD_BCO_evenB_ERR);
								monStatus.message +=  " BCO_evenB_ERR error cnt: " + toString(recErrCnt);
							}
						}
					}
				}
			}
		}
	}
	catch(EI2CRbc& e) {
		LOG4CPLUS_ERROR(logger, "Error during "<<__FUNCTION__<<" in "<<rbcName_<<": "<<e.what()<<" RBC is not good, but we must live with that");
		//TODO - remove if problems with the I2C communication after the firmware reloading are solved
		monStatus.level = MonitorableStatus::SOFT_WARNING;
		monStatus.message +=  string(" ") + e.what();
	}
	if(monStatus.level != MonitorableStatus::OK)
		warningStatusList_.push_back(monStatus);
}
//........... Rbc definition .................

Logger Rbc::logger = Logger::getInstance("Rbc");
void Rbc::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}
const HardwareItemType Rbc::TYPE(HardwareItemType::cBoard, "RBCBOARD");

Rbc::Rbc(int id, std::string description, LinkBox* lbox, int position, ICB* cb,
		int rbcId) :
	BoardBase(id, description, TYPE, lbox, position), rbcDevice_(rbcId, *this,
			cb) {
}

Rbc::~Rbc() {
}

bool Rbc::checkResetNeeded() {
	return rbcDevice_.checkResetNeeded();
}

void Rbc::reset() {
	return rbcDevice_.reset();
}

void Rbc::enable() {
	rbcDevice_.enable();
}

void Rbc::configure(ConfigurationSet* configurationSet, int configureFlags) {
	//BoardBase::configure(configurationSet, configureFlags);
	rbcDevice_.configure(
			configurationSet->getDeviceSettings(rbcDevice_.getId()),
			configureFlags);
}

bool Rbc::checkConfiguration(DeviceSettings* settings) {
	uint8_t promReg = rbcDevice_.read(RbcDevice::WORD_PROM_REG);
	//TODO check types!!!!!

	unsigned int readConfigId = rbcDevice_.read(RbcDevice::WORD_REC_CONFIG_VER);

	RbcDeviceSettings* s = dynamic_cast<RbcDeviceSettings*> (settings);
	if (s == 0) {
		throw IllegalArgumentException(
				"RbcDevice::checkConfiguration: invalid cast for settings");
	}

	if(promReg != 0x80 || readConfigId != (s->getConfigurationId() & 0xff) ) { //TODO add mask on the confID
		LOG4CPLUS_INFO(logger, getDescription() <<" Rbc::checkConfiguration() : wrong configuration: WORD_PROM_REG = 0x"<<hex<< (unsigned int)promReg
				<<", readConfigId = 0x"<< readConfigId << " while configurationId from DB = "<<s->getConfigurationId() );
		return false;
	}

	return true;
}

void Rbc::putConfigurationToProm(DeviceSettings* settings) {
	rbcDevice_.configure(settings, ConfigurationFlags::FORCE_CONFIGURE);

	rbcDevice_.write(RbcDevice::WORD_CTRL, 0x01); //Enable automatic resync //TODO see https://savannah.cern.ch/task/?25521

	rbcDevice_.write(RbcDevice::WORD_PROM_REG, 0x80); //Enable automatic load

	rbcDevice_.write(RbcDevice::WORD_PROM_REG, 0x82); //Erase block, Store internal registers into PROM_BUFFER and
	//Write full content of internal PROM_BUFFER into EPROM

}

bool Rbc::putConfigurationToProm(ConfigurationSet* configurationSet, bool force) {
	DeviceSettings* settings = configurationSet->getDeviceSettings(rbcDevice_.getId());
	if (settings == 0) {
		throw NullPointerException("RbcDevice::putConfigurationToProm: settings == 0");
	}

	if(force == false) {
		force = !checkConfiguration(settings);
	}

	if(force) {
		putConfigurationToProm(settings);
	}

	return force;
}

//////Rbc Rbc::rbc(0, "rbc", 0, 101, 0, 123); //TODO only to check compilation, remove

bool Rbc::parseLBoxName(const std::string & name, int & sector) {
	size_t found;
	bool inBarrel(false);

	found = name.find("RB");
	if (found != std::string::npos) {
		inBarrel = true;
	} else
		return false;

	unsigned int iStart = name.rfind('_') + 2;
	unsigned int iEnd = name.size();
	std::istringstream secStr(name.substr(iStart, (iEnd - iStart)),
			std::istringstream::in);
	secStr >> sector;

	return inBarrel;

}

HalfBox* Rbc::getHalfBox() {
    return (dynamic_cast<TCB*>(rbcDevice_.getCb()))->getHalfBox();
}

} // namespace
