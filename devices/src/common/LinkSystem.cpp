#ifdef __BORLANDC__
#include "precompiled.h"
#pragma hdrstop
#endif


#include "rpct/devices/LinkSystem.h"
#include "rpct/devices/VmeCrate.h"
#include "rpct/devices/LinkBox.h"
#include "rpct/ii/ConfigurationFlags.h"
#include "rpct/std/IllegalArgumentException.h"
#include "rpct/devices/SynCoderSettings.h"


using namespace std;
//using namespace xercesc;

namespace rpct {

log4cplus::Logger LinkSystem::logger = log4cplus::Logger::getInstance("LinkSystem");
void LinkSystem::setupLogger(std::string namePrefix) {
	logger = log4cplus::Logger::getInstance(namePrefix + "." + logger.getName());
}
/*System& System::getInstance() {
    if (instance.get() == 0) {
        instance.reset(new System());
    }

    return *instance.get();
}*/

LinkSystem::LinkSystem(): System(), febSystem_(0) {

}


LinkSystem::~LinkSystem() {
    LOG4CPLUS_DEBUG(logger, "Destroying System"
            <<": Start" );
    for (FecManagerList::iterator i = fecManagerList_.begin(); i != fecManagerList_.end(); i++) {
        delete *i;
    }
    LOG4CPLUS_DEBUG(logger, "Destroying FecManagers"
            <<": Done" );

    for (LinkBoxList::iterator i = linkBoxList_.begin(); i != linkBoxList_.end(); i++) {
        delete *i;
    }
    LOG4CPLUS_DEBUG(logger, "Destroing LinkBoxes"
            <<": Done" );
    LOG4CPLUS_DEBUG(logger, "Destroying System"
            <<": Done" );
}

void LinkSystem::addFecManager(FecManager& fecManager) {
    FecManagerKey key(fecManager.getPciSlot(), fecManager.getVmeSlot(),
                      fecManager.getRingNumber());
    if (fecManagerMap_.find(key) != fecManagerMap_.end()) {
        throw TException("System::addFecManager: Duplicate fecManager");
    };
    fecManagerList_.push_back(&fecManager);
    fecManagerMap_.insert(FecManagerMap::value_type(key, &fecManager));
}


void LinkSystem::addHalfBox(HalfBox& halfBox) {
    LOG4CPLUS_DEBUG(logger, "adding HalfBox - Started");
/*
    try{
        LinkBox& linkBox = halfBox.getLinkBox();
        LOG4CPLUS_DEBUG(logger, "adding HalfBox - getLinkBox ok");

        StdLinkBoxAccess& lboxAccess = halfBox.getLBoxAccess();
        LOG4CPLUS_DEBUG(logger, "adding HalfBox - getLBoxAccess ok");

        FecManager& fm = halfBox.getLBoxAccess().getFecManager();
        LOG4CPLUS_DEBUG(logger, "adding HalfBox - getFecManager ok");
    }
    catch(...) {
        LOG4CPLUS_DEBUG(logger, "adding HalfBox - fm something wrong!!!!");
        ostringstream ostr;
        ostr << "System::addHalfBox - something wrong!!!!" << endl;
        throw TException(ostr.str());
    };
*/
    FecManager& fm = halfBox.getLBoxAccess().getFecManager();
    HalfBoxKey key(fm.getPciSlot(), fm.getVmeSlot(), fm.getRingNumber(),
            halfBox.getLBoxAccess().getCcuAddress());
    LOG4CPLUS_DEBUG(logger, "adding HalfBox"
            << " linkBox id = " << halfBox.getLinkBox().getId()
            << " linkBox name = " << halfBox.getLinkBox().getDescription()
            << " pciSlot = " << fm.getPciSlot()
            << " vmeSlot = " << fm.getVmeSlot()
            << " ringNumber = " << fm.getRingNumber()
            << " ccuAddress = " << halfBox.getLBoxAccess().getCcuAddress());

    if (halfBoxMap_.find(key) != halfBoxMap_.end()) {
            ostringstream ostr;
            ostr << "System::addHalfBox: Duplicate halfBox"
            << " linkBox id = " << halfBox.getLinkBox().getId()
            << " linkBox name = " << halfBox.getLinkBox().getDescription()
            << " pciSlot = " << fm.getPciSlot()
            << " vmeSlot = " << fm.getVmeSlot()
            << " ringNumber = " << fm.getRingNumber()
            << " ccuAddress = " << halfBox.getLBoxAccess().getCcuAddress()
            << endl;
            throw TException(ostr.str());
    };
    halfBoxMap_.insert(HalfBoxMap::value_type(key, &halfBox));
    halfBoxList_.push_back(&halfBox);
    LOG4CPLUS_DEBUG(logger, "adding HalfBox - Ended");
}

void LinkSystem::addLinkBox(LinkBox& linkBox) {

    LOG4CPLUS_DEBUG(logger, "adding LinkBox - Started");
    if (linkBox.getHalfBox10() != 0) {
        addHalfBox(*linkBox.getHalfBox10());
    }
    if (linkBox.getHalfBox20() != 0) {
        addHalfBox(*linkBox.getHalfBox20());
    }

    linkBoxList_.push_back(&linkBox);
    registerCrate(linkBox);

    LOG4CPLUS_DEBUG(logger, "adding LinkBox - Ended");
}

void LinkSystem::registerFebSystem(FebSystem* febSystem) {
	if(febSystem != 0) {
		febSystem_ = febSystem;
		LOG4CPLUS_INFO(logger, __FUNCTION__<<" adding febSystem");
		return;
	}
	else {
		LOG4CPLUS_ERROR(logger, __FUNCTION__<<"  febSystem is 0");
	}
}

void LinkSystem::registerItem(IHardwareItem& item) {
	/*FebSystem* febSystem = dynamic_cast<FebSystem*> (&item);
	if(febSystem != 0) {
		febSystem_ = febSystem;
		LOG4CPLUS_INFO(logger, __FUNCTION__<<"adding febSystem "<<febSystem_);
		return;
	}*/

	System::registerItem(item);

	if (dynamic_cast<IBoard*>(&item) == 0)
		return;

	LinkBoard* lb = dynamic_cast<LinkBoard*> (&item);
	if (lb != 0) {
		linkBoards_.push_back(lb);
		LOG4CPLUS_DEBUG(logger, "adding LB "<<lb->getDescription());
		return;
	}

	ICB* cb = dynamic_cast<ICB*> (&item);
	if (cb != 0) {
		controlBoards_.push_back(cb);
		return;
	}

	Rbc* rbc = dynamic_cast<Rbc*> (&item);
	if (rbc != 0) {
		rbcBoards_.push_back(rbc);
		return;
	}

	LOG4CPLUS_ERROR(logger, "Item " << item.getDescription() << " is of unknown type");
}

FecManager& LinkSystem::getFecManager() {
    LinkSystem::FecManagerList& fecManagerList = getFecManagers();
    if (fecManagerList.empty()) {
        throw TException("No ccu ring found in the system");
    }

    if (fecManagerList.size() > 1) {
        throw TException("More than one ccu ring found in the system, don't know which shpoudl I give");
    }

    FecManager* fm = fecManagerList.front();
    if (fm == 0) {
        throw TException("No fecManager found");
    }
    return *fm;
}

FecManager& LinkSystem::getFecManager(int pciSlot, int vmeSlot, int ringNumber) {
    FecManagerKey key(pciSlot, vmeSlot, ringNumber);
    FecManagerMap::iterator iFec = fecManagerMap_.find(key);
    if (iFec == fecManagerMap_.end()) {
        throw TException("System::getFecManager: item not found (pciSlot = " + toString(pciSlot)
        	+ ", vmeSlot = " + toString(vmeSlot) + ", ringNumber = " + toString(ringNumber));
    };
    return *iFec->second;
}

HalfBox& LinkSystem::getHalfBox(int pciSlot, int vmeSlot, int ringNumber,
                             unsigned int ccuAddress) {
    HalfBoxKey key(pciSlot, vmeSlot, ringNumber, ccuAddress);
    LOG4CPLUS_DEBUG(logger, "searching HalfBox " << key.toString());
    HalfBoxMap::iterator iHalfBox = halfBoxMap_.find(key);
    if (iHalfBox == halfBoxMap_.end()) {
        throw TException("System::getHalfBox " + key.toString() + " not found.");
    };
    return *iHalfBox->second;
}

bool LinkSystem::checkResetNeeded(ControlBoards controlBoards, LinkBoards linkBoards, RbcBoards rbcBoards) {
	bool resetNeeded = false;
	for (LinkBoards::iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); iLB++) {
		if( (*iLB)->checkResetNeeded() )
			resetNeeded = true;
	}
	LOG4CPLUS_DEBUG(logger, "checkResetNeeded " << resetNeeded);
	return resetNeeded;
}

void LinkSystem::initI2CinControlBoards(ControlBoards controlBoards) {
    for (ControlBoards::iterator iCB = controlBoards.begin(); iCB != controlBoards.end(); iCB++) {
        (*iCB)->initI2C();
    }
}

void LinkSystem::resetLinkBoards(LinkBoards linkBoards) {
    LOG4CPLUS_INFO(logger, " start intiTTCrxs");
    for (LinkBoards::iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); iLB++) {
        (*iLB)->initI2C();
    }

	LOG4CPLUS_INFO(logger, " start intiTTCrxs");
	for (LinkBoards::iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); iLB++) {
		(*iLB)->intiTTCrxStep1();
	}
	tbsleep(200); //with 100 the not ready state is more likely afetr the reset, with 10 it looks that the I2C errors are more likely
	for (LinkBoards::iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); iLB++) {
		(*iLB)->intiTTCrxStep2();
	}
	tbsleep(200); //with 100 the not ready state is more likely
	for (LinkBoards::iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); iLB++) {
		(*iLB)->intiTTCrxStep3();
	}
	LOG4CPLUS_INFO(logger, ": intiTTCrxs done");


	LOG4CPLUS_INFO(logger, " start resetQPLLS");
	for (LinkBoards::iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); iLB++) {
		(*iLB)->resetQPLLStep1();
	}
	tbsleep(5);
	for (LinkBoards::iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); iLB++) {
		(*iLB)->resetQPLLStep2();
	}
	tbsleep(500);//according to the QPLL manual the reset cycle last ~180 ms, measurements shows 400 ms
	for (LinkBoards::iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); iLB++) {
		(*iLB)->resetQPLLStep3();
	}
	LOG4CPLUS_INFO(logger, ": resetQPLLS done");

	LOG4CPLUS_INFO(logger, " start resetGOL");
	for (LinkBoards::iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); iLB++) {
		if((*iLB)->getMasterSlaveType() == msMaster)
			(*iLB)->resetGOL();
	}
	LOG4CPLUS_INFO(logger, ": resetGOL done");
}

/*void LinkSystem::resetLinkBoardsMZ(LinkBoards linkBoards, ConfigurationSet* configurationSet) {
    for (LinkBoards::iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); iLB++) {
        (*iLB)->initI2CFlash();
    }

	for (LinkBoards::iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); iLB++) {
		(*iLB)->getSynCoder().prepareForFlashConfig();
		(*iLB)->intiTTCrxFlash();
		(*iLB)->resetQPLLFlash();
		//DeviceSettings* settings = configurationSet->getDeviceSettings(*(*iLB));
		(*iLB)->getSynCoder().configureForFlash(configurationSet);
		if((*iLB)->getMasterSlaveType() == msMaster)
			(*iLB)->getSynCoder().resetGOLFlash(); //it must be done after the BCO delay is set; resetGOLFlash does the synchronizeLink
		//(*iLB)->getSynCoder().synchronizeLinkFlash();
		(*iLB)->getSynCoder().enableHistoFlash();
		(*iLB)->getSynCoder().finalizeFlashConfig();
	}
}*/

void LinkSystem::resetRbcBoards(RbcBoards rbcBoards) {
	for (RbcBoards::iterator iRbc = rbcBoards.begin(); iRbc != rbcBoards.end(); iRbc++) {
		(*iRbc)->reset();
	}
}

void LinkSystem::setupBoards(ConfigurationSet* configurationSet, int configFlags, ControlBoards controlBoards, LinkBoards linkBoards, RbcBoards rbcBoards) {
	//cout<<"controlBoards.size() "<<controlBoards.size()<<" !!!!!!!!!!!!!!!!!!!!!!!!!!!!1"<<endl;

    LOG4CPLUS_INFO(logger, ": setupBoards: ControlBoards configure started");
	for (ControlBoards::iterator iCB = controlBoards.begin(); iCB != controlBoards.end(); iCB++) {
		(*iCB)->configure(configurationSet, configFlags);
	}
    LOG4CPLUS_INFO(logger, ": setupBoards: ControlBoards configure finished");

	time_t timer = time(NULL);
	tm* tblock = localtime(&timer);
	tblock->tm_mon += 1;
    std::stringstream fileName;

    fileName<<"/rpctdata/lbMasksDump/" + towerName_ + "_"
    << tblock->tm_year + 1900<< '_'
    << tblock->tm_mon << '_' << tblock->tm_mday << "__" << tblock->tm_hour << '_'
    << tblock->tm_min << '_' << tblock->tm_sec<<".txt";
    ofstream outFile;
    outFile.open(fileName.str().c_str());
    LOG4CPLUS_INFO(logger, ": setupBoards: opened file "<<fileName.str());

    LOG4CPLUS_INFO(logger, ": setupBoards: LinkBoards configure started");
	for (LinkBoards::iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); iLB++) {
		(*iLB)->configure(configurationSet, configFlags);
		if(configurationSet != 0) {
			DeviceSettings* settings = configurationSet->getDeviceSettings((*iLB)->getSynCoder());
			SynCoderSettings* scs = dynamic_cast<SynCoderSettings*>(settings);
//			outFile<<(*iLB)->getDescription()<<" "<<dataToHex(bitsetToBits(scs->getInChannelsEna()), (*iLB)->getChannelCnt())<<endl;
			outFile<<(*iLB)->getChamberName()<<" "<<dataToHex(bitsetToBits(scs->getInChannelsEna()), (*iLB)->getChannelCnt())<<endl;
		}
	}
	LOG4CPLUS_INFO(logger, ": setupBoards: LinkBoards configure finished");
	outFile.close();

	LOG4CPLUS_INFO(logger, ": setupBoards: RbcBoards configure started");
	for (RbcBoards::iterator iRbc = rbcBoards.begin(); iRbc != rbcBoards.end(); iRbc++) {
		(*iRbc)->configure(configurationSet, configFlags);
	}
	LOG4CPLUS_INFO(logger, ": setupBoards: RbcBoards configure finished");
}

bool LinkSystem::resetHardware(bool forceReset) {
	bool resetNeeded = forceReset;
	if (!forceReset) {
		resetNeeded = checkResetNeeded();
	}
	LOG4CPLUS_INFO(logger, "LinkSystem::resetHardware: resetNeeded="<<resetNeeded);
	if(resetNeeded) {
		resetBoards();
	}

	return resetNeeded;
}

void LinkSystem::setupHardware(ConfigurationSet* configurationSet, bool forceSetup) {
	int configFlags = forceSetup ? ConfigurationFlags::FORCE_CONFIGURE : 0;

	setupBoards(configurationSet, configFlags);
}

void LinkSystem::synchronizeLinksLBs(LinkBoards linkBoards) {
	for (LinkBoards::iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); iLB++) {
		if ((*iLB)->getMasterSlaveType() == rpct::msMaster) {
			(*iLB)->getSynCoder().writeBits(rpct::SynCoder::BITS_GOL_TX_ENA, 0);
		}
	}
	tbsleep(500);
	for (LinkBoards::iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); iLB++) {
		if ((*iLB)->getMasterSlaveType() == rpct::msMaster) {
			(*iLB)->getSynCoder().writeBits(rpct::SynCoder::BITS_GOL_TX_ENA, 1);
		}
	}

/*	for (LinkBoards::iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); iLB++) {
		if ((*iLB)->getMasterSlaveType() == rpct::msMaster) {
			(*iLB)->getSynCoder().writeBits(rpct::SynCoder::BITS_GOL_TX_ENA, 0);
			tbsleep(10);
			(*iLB)->getSynCoder().writeBits(rpct::SynCoder::BITS_GOL_TX_ENA, 1);
		}
	}*/
}

void LinkSystem::synchronizeLinksRbcs(RbcBoards rbcBoards) {
	for (RbcBoards::iterator iRbc = rbcBoards.begin(); iRbc != rbcBoards.end(); iRbc++) {
		(*iRbc)->synchronizeLink();
	}
}

void LinkSystem::disableLinksLBs(LinkBoards linkBoards) {
    for (LinkBoards::iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); iLB++) {
        if ((*iLB)->getMasterSlaveType() == rpct::msMaster) {
            //(*iLB)->getSynCoder().writeBits(rpct::SynCoder::BITS_GOL_TX_ENA, 0);
            (*iLB)->getSynCoder().writeBits(rpct::SynCoder::BITS_SEND_TEST_ENA, 1);
        }
    }
    LOG4CPLUS_WARN(logger, " LinkSystem::disableLinksLBs() executed");
}

void  LinkSystem::readLbChamberMap() {
	typedef std::map<std::string, std::string> LbChamberMap;
	LbChamberMap lbChamberMap;

    string towerName = towerName_;
    size_t pos = towerName.find('+');
    if( pos != string::npos ) {
    	towerName[pos] = 'p';
    }
    pos = towerName.find('-');
    if( pos != string::npos ) {
    	towerName[pos] = 'n';
    }

    ifstream inFile;

    char* tmp = getenv("RPCT_DATA_PATH");
    if (tmp == 0) {
    	throw TException(string(__FUNCTION__) + string(": Environment variable RPCT_DATA_PATH not defined"));
    }
    string fileDirName =  string(tmp) + "/config/lbChamberMap_" + towerName + ".txt";

    inFile.open(fileDirName.c_str());
    if(!inFile) {
        LOG4CPLUS_WARN(logger, "the lbChamberMap file could not be opened: "<<fileDirName);
        return;
    }

    //cout<<"File "<<fileDirName<<" opened, reading the data"<<endl;
    string lbName;
    string lbPosition;
    string chamberName;
    string stripSurface;
    while ( !inFile.eof() ) {
        inFile >> lbName>>lbPosition>>chamberName>>stripSurface;
        lbChamberMap[lbName] = chamberName;
    }
    inFile.close();

    for (LinkBoards::iterator iLB = linkBoards_.begin(); iLB != linkBoards_.end(); iLB++) {
    	LbChamberMap::iterator it = lbChamberMap.find((*iLB)->getDescription());
    	if(it != lbChamberMap.end()) {
    		(*iLB)->setChamberName(it->second);
    	}
    }
}

string LinkSystem::readBoardsLabels(string fileDirName) {
	typedef std::map<std::string, std::string> BoardLabelMap;
	BoardLabelMap boardLabelMap;

    ifstream inFile;

    inFile.open(fileDirName.c_str());
    if(!inFile) {
        LOG4CPLUS_WARN(logger, "the BoardsLabels file could not be opened: "<<fileDirName);
        return "";
    }

    //cout<<"File "<<fileDirName<<" opened, reading the data"<<endl;
    string boardName;
    string label;
    while ( !inFile.eof() ) {
        inFile >> boardName>>label;
        if(label != "0")
        	boardLabelMap[boardName] = label;
    }
    inFile.close();

    for (LinkBoards::iterator iLB = linkBoards_.begin(); iLB != linkBoards_.end(); iLB++) {
    	BoardLabelMap::iterator it = boardLabelMap.find((*iLB)->getDescription());
    	if(it != boardLabelMap.end()) {
    		(*iLB)->setLabel(it->second);
    	}
    }

    for (ControlBoards::iterator iCB = controlBoards_.begin(); iCB != controlBoards_.end(); iCB++) {
    	BoardLabelMap::iterator it = boardLabelMap.find((*iCB)->getDescription());
    	if(it != boardLabelMap.end()) {
    		(dynamic_cast<BoardBase*>(*iCB))->setLabel(it->second);
    	}
    }

    return boardLabelMap["backplane"];
}

} // namespace rpct
