#include "rpct/devices/FebSystem.h"

#include <sstream>

#include "rpct/tools/Chrono.h"

#include "rpct/devices/IFebAccessPoint.h"
#include "rpct/devices/FebChip.h"

#include "rpct/ii/feb_const.h"

namespace rpct {

log4cplus::Logger FebSystem::logger_ = log4cplus::Logger::getInstance("Feb.FebSystem");
void FebSystem::setupLogger(std::string namePrefix) {
    logger_ = log4cplus::Logger::getInstance(namePrefix + "." + logger_.getName());
}
HardwareItemType * FebSystem::type_ = 0;
IMonitorable::MonitorItems * FebSystem::mitems_ = 0;

FebSystem::FebSystem()
    : FebSystemItem(*this, 0)
    , id_(-1)
    , description_("FebSystem")
    , monitorcycle_(-1)
    , configuration_time_(0)
    , monitoring_time_(0)
    , monitorcycle_timer_(rpct::devices::feb::preset::monitorcycle_)
    , interrupt_(false)
{
    if (!type_)
        type_ = new HardwareItemType(HardwareItemType::cBoard
                                     , type::feb::febsystem_);
    if (!mitems_)
        {
            mitems_ = new MonitorItems();
            mitems_->push_back(monitoritem::feb::pcf8574a_);
            mitems_->push_back(monitoritem::feb::temp_);
            mitems_->push_back(monitoritem::feb::vth_);
            mitems_->push_back(monitoritem::feb::vmon_);
        }

    // TODO: change location of the file
    logcenter_->loadCategories("/nfshome0/rpcpro/rpct/devices/feb_log_categories.txt");
    logcenter_->loadMessages("/nfshome0/rpcpro/rpct/devices/feb_log_messages.txt");
    logcenter_->setPublisherLogType(rpct::publish::type::log_);
    logcenter_->setPublisherUnLogType(rpct::publish::type::unlog_);
    logcenter_->setPublisher("febchip_i2c", *this);
    logcenter_->setPublisher("febchip_vth", *this);
    logcenter_->setPublisher("febchip_vmon", *this);
    logcenter_->setPublisher("febchip_offset", *this);
    logcenter_->setPublisher("febchip_temp", *this);
    logcenter_->setPublisher("febchip", *this);

    fsis_["empty"];
}

FebSystem::~FebSystem()
{
    logcenter_->unsetPublisher("febchip_i2c");
    logcenter_->unsetPublisher("febchip_vth");
    logcenter_->unsetPublisher("febchip_vmon");
    logcenter_->unsetPublisher("febchip_offset");
    logcenter_->unsetPublisher("febchip_temp");
    logcenter_->unsetPublisher("febchip");

    reset();
}
void FebSystem::reset()
{
    FebSystemItem::reset();
    fsis_.clear();
    fsis_["empty"];
    faps_type::iterator IfapsEnd = faps_.end();
    for (faps_type::iterator Ifaps = faps_.begin()
             ; Ifaps != IfapsEnd
             ; ++Ifaps)
        delete Ifaps->second;
    faps_.clear();
    mus_.clear();
    monitorcycle_ = -1;
}

void FebSystem::addItem(FebSystemItem & fsi)
{
    if (fsi.getType().getType() == rpct::type::feb::febmonitorunit_)
        mus_.insert(std::pair<int, IMonitorable *>(fsi.getId(), dynamic_cast<IMonitorable *>(&fsi)));
    fsis_[fsi.getType().getType()].insert(std::pair<int, FebSystemItem *>(fsi.getId(), &fsi));
}
void FebSystem::removeItem(FebSystemItem & fsi)
{
    if (fsi.getType().getType() == rpct::type::feb::febmonitorunit_)
        mus_.erase(fsi.getId());
    std::map<int, FebSystemItem *> & fsis_t = fsis_[fsi.getType().getType()];
    std::map<int, FebSystemItem *>::iterator Iitem = fsis_t.find(fsi.getId());
    if (Iitem != fsis_t.end())
        fsis_t.erase(Iitem);
}

FebSystemItem * FebSystem::getItem(const std::string & type, int id) const
{
    fsis_type::const_iterator fsis_t = fsis_.find(type);
    if (fsis_t != fsis_.end())
        {
            std::map<int, FebSystemItem *>::const_iterator Iitem = fsis_t->second.find(id);
            if (Iitem != fsis_t->second.end())
                return Iitem->second;
            else
                return 0;
        }
    else
        return 0;
}

std::vector<int> FebSystem::getChipIds() const
{
    std::vector<int> chipids;

    fsis_type::const_iterator febchips = fsis_.find(rpct::type::feb::febchip_);
    if (febchips != fsis_.end())
        {
            std::map<int, FebSystemItem *>::const_iterator IfcEnd = febchips->second.end();
            for (std::map<int, FebSystemItem *>::const_iterator Ifc = febchips->second.begin()
                     ; Ifc != IfcEnd
                     ; ++Ifc)
                if (!Ifc->second->isDisabled() && Ifc->first >= 0)
                    chipids.push_back(Ifc->first);
        }
    return chipids;
}

void FebSystem::monitor(MonitorItems & items)
{
    rpct::tools::Chrono _chrono;
    faps_type::iterator IfapsEnd = faps_.end();
    for (faps_type::iterator Ifaps = faps_.begin()
             ; Ifaps != IfapsEnd
             ; ++Ifaps)
        Ifaps->second->monitor(items);
    monitoring_time_ = _chrono.duration();
    log("monitored");
}
void FebSystem::monitorcycle()
{
    if (!mus_.empty())
        {
            monitorunit_type::iterator ImusEnd = mus_.end();
            monitorunit_type::iterator Imus = mus_.find(monitorcycle_);
            if (Imus == ImusEnd)
                Imus = mus_.begin();
            else
                {
                    ++Imus;
                    if (Imus == ImusEnd)
                        {
                            if (!(monitorcycle_timer_.ready()))
                                return;
                            else
                                {
                                    monitorcycle_timer_.reset();
                                    Imus = mus_.begin();
                                }
                        }
                }
            monitorcycle_ = Imus->first;
            if (Imus->second)
                Imus->second->monitor(0); //TODO pass stop if needed
        }
}

void FebSystem::configure(ConfigurationSet* configurationSet, int configureFlags)
{
    rpct::tools::Chrono _chrono;
    faps_type::iterator IfapsEnd = faps_.end();
    for (faps_type::iterator Ifaps = faps_.begin()
             ; Ifaps != IfapsEnd
             ; ++Ifaps)
        Ifaps->second->configure(configurationSet, configureFlags);
    configuration_time_ = _chrono.duration();
    configured();
    log("configured");
}
void FebSystem::configure()
{
    rpct::tools::Chrono _chrono;
    faps_type::iterator IfapsEnd = faps_.end();
    for (faps_type::iterator Ifaps = faps_.begin()
             ; Ifaps != IfapsEnd
             ; ++Ifaps)
        Ifaps->second->configure();
    configuration_time_ = _chrono.duration();
    configured();
    log("configured");
}

void FebSystem::loadConfiguration(ConfigurationSet * configurationSet, int configureFlags)
{
    faps_type::iterator IfapsEnd = faps_.end();
    for (faps_type::iterator Ifaps = faps_.begin()
             ; Ifaps != IfapsEnd
             ; ++Ifaps)
        Ifaps->second->loadConfiguration(configurationSet, configureFlags);
    configured(0);
    unlog("configured");
}

void FebSystem::printParameters(tools::Parameters & parameters) const
{}

std::string FebSystem::getStatusSummary() const
{
    std::map<std::string, std::map<int, FebSystemItem *> > fsis_type;
    int warn(0), error(0), disabled(0), configured(0), n(0);
    fsis_type::const_iterator febchips = fsis_.find(rpct::type::feb::febchip_);
    if (febchips != fsis_.end())
        {
            std::map<int, FebSystemItem *>::const_iterator IfebchipEnd = febchips->second.end();
            for (std::map<int, FebSystemItem *>::const_iterator Ifebchip = febchips->second.begin()
                     ; Ifebchip != IfebchipEnd
                     ; ++Ifebchip)
                {
                    ++n;
                    if (Ifebchip->second->isWarn())
                        ++warn;
                    else if (Ifebchip->second->isError())
                        ++error;
                    if (Ifebchip->second->isDisabled())
                        ++disabled;
                    if (Ifebchip->second->isConfigured())
                        ++configured;
                }
            std::stringstream out;
            out << "Febsystem " << getDescription() << ": " << n << " FebChips, " << disabled << " disabled, " << configured << " configured, " << warn << " in warning state, " << error << " in error state; " << configuration_time_ << " for configuration, " << monitoring_time_ << " for monitoring.";
            return out.str();
        }
    else
        return "FebSystem has no FebChips";
}

} // namespace rpct
