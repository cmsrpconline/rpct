/*
 * QPLL.cpp
 *
 *  Created on: Nov 16, 2010
 *      Author: tb
 */

#include "rpct/devices/QPLL.h"
#include "rpct/std/tbutil.h"

using namespace std;
using namespace log4cplus;


namespace rpct {

void QPLL::setup() {
    uint32_t qpll = owner_.readVector(VECT_QPLL);
    owner_.setBitsInVector(BITS_QPLL_MODE, 1, qpll); //mode = 1: 160 MHz frequency multiplication mode (160 MHz quartz crystal required).
    owner_.setBitsInVector(BITS_QPLL_CLK_LOCK, 0, qpll); //inCMOS
    owner_.setBitsInVector(BITS_QPLL_CTRL, 0, qpll); //externalControl
    owner_.setBitsInVector(BITS_QPLL_SELECT, 0x30, qpll); //0x10 autoRestart, 0x20 ~reset ;
    owner_.writeVector(VECT_QPLL, qpll);
}

bool QPLL::reset() {
	setup();

    for(int i = 0; i < 3; i++) {
    	owner_.writeBits(BITS_QPLL_SELECT, 0x10); // reset start
    	tbsleep(5);
    	owner_.writeBits(BITS_QPLL_SELECT, 0x30); // reset stop
    	tbsleep(400);
    	for(int iWait = 0; iWait < 3; iWait++) {
    		if (owner_.readBits(BITS_QPLL_LOCKED) == 1) {
    			LOG4CPLUS_INFO(logger_, owner_.getBoard().getDescription() <<" "<< owner_.getDescription()
    					<< " ResetTTC: QPLL is LOCKED, reset repeated: "<< i<<", locked after " << 400 + iWait * 100 <<" ns");
    			return true;
    		}
    		tbsleep(100);
    	}
    }
    throw TException(owner_.getBoard().getDescription() + owner_.getDescription() + " ResetQPLL: BITS_QPLL_LOCKED did not go up");
    return false;
}

}
