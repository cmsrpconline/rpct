#include "rpct/devices/CcsMonitorables.h"
#include <sstream>
#include <bitset>
#include "cgicc/Cgicc.h"
#include "rpct/devices/VCcs_pkgDef.h"
#include "rpct/devices/CcsSettings.h"

using namespace rpct;
using namespace std;

HardwareItemType CcsMonitorables::getDeviceType() { return Ccs::TYPE; }

bool CcsMonitorables::hasProblem()
{
  unsigned int outTTS = (ttsStatus_ & 0xf00000) >> 20;
  if (outTTS!=1 ||  outTTS !=8) return true;
  return false;
}

std::string CcsMonitorables::printTTS() 
{
  std::ostringstream str;
  
  str << " <h2>outgoing sTTS status:";
  unsigned int outTTS = (ttsStatus_ & 0xf00000) >> 20; 
  switch (outTTS) {
  case (1) : str << "<font color=orange> Warning</font>"; break;
  case (2) : str << "<font color=red> OutOfSync </font>"; break;
  case (4) : str << "<font color=red> Busy </font>"; break;
  case (8) : str << "<font color=green> Ready</font>"; break;
  case (0) : case (15): str << "Disconnected"; break;
  default: str << "Unknown";
  };
  str <<"</h2>";
  return str.str();
}

std::string CcsMonitorables::printInfo()
{
  std::ostringstream str;
  bool qpllLocked = (ccsStatus_ & 0x2)>>1;
  bool qpllError  = (ccsStatus_ & 0x4)>>2;
  bool ttcrxReady = ccsStatus_ & 0x1;
  string err;
  if (!qpllLocked) err += " !qpllLocked";
  if (qpllError) err += " qpllError";
  if (!ttcrxReady) err += " !ttcrxReady";
  if (err !="") str << "<h2> <font color=red> CCS  pos:" 
                    << position_<< "error: "<<err
                    <<"</font></h2>"; 
  return str.str();
}

