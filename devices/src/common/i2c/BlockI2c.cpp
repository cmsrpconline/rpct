#include "rpct/devices/i2c/BlockI2c.h"

#include <unistd.h> // usleep(microseconds)

#include <sstream>

#include "rpct/lboxaccess/StdLinkBoxAccess.h"

#include "rpct/devices/cb.h"

#include "rpct/devices/i2c/exception/Bi2cException.h"

#include "rpct/ii/i2c/I2cResponse.h"

namespace rpct {
namespace devices {
namespace i2c {

log4cplus::Logger BlockI2c::logger_ = log4cplus::Logger::getInstance("BlockI2c");
void BlockI2c::setupLogger(std::string namePrefix) {
	logger_ = log4cplus::Logger::getInstance(namePrefix + "." + logger_.getName());
}

rpct::StdLinkBoxAccess * BlockI2c::getLBoxAccess() const
    throw (rpct::exception::SystemException)
{
    return getControlBoard().getLBoxAccess();
}

void BlockI2c::iRecoverCCU()
    throw (rpct::exception::CCUException)
{
    LOG4CPLUS_DEBUG(logger_, "iRecoverCCU()");
    try {
        getLBoxAccess()->getFecManager().resetPlxFecForAutoRecovery();
    } catch(rpct::CCUException & e) {
        reset();
        throw rpct::exception::CCUException(e.what());
    } catch(TException & e) {
        reset();
        throw rpct::exception::CCUException(e.what());
    }
}

unsigned short BlockI2c::memoryRead(uint32_t address)
{
    LOG4CPLUS_DEBUG(logger_, "memoryRead()");
    rpct::StdLinkBoxAccess * lboxAccess = getLBoxAccess();
    try {
        return lboxAccess->memoryRead(address);
    } catch (rpct::CCUException & e) {
        LOG4CPLUS_WARN(logger_, "CCUException in BlockI2c::memoryRead, trying again after attempt to recover.");
        iRecoverCCU();
        return lboxAccess->memoryRead(address);
    } catch (TException & e) {
        LOG4CPLUS_WARN(logger_, "TException in BlockI2c::memoryRead, trying again.");
        return lboxAccess->memoryRead(address);
    }
}
void BlockI2c::memoryWrite(uint32_t address, unsigned short data)
{
    LOG4CPLUS_DEBUG(logger_, "memoryWrite()");
    rpct::StdLinkBoxAccess * lboxAccess = getLBoxAccess();
    try {
        lboxAccess->memoryWrite(address, data);
    } catch (rpct::CCUException & e) {
        LOG4CPLUS_WARN(logger_, "CCUException in BlockI2c::memoryWrite, trying again after attempt to recover.");
        iRecoverCCU();
        lboxAccess->memoryWrite(address, data);
    } catch (TException & e) {
        LOG4CPLUS_WARN(logger_, "TException in BlockI2c::memoryWrite, trying again.");
        lboxAccess->memoryWrite(address, data);
    }
}
void BlockI2c::memoryReadBlock(uint32_t address
                               , unsigned char * data
                               , uint32_t count)
{
    LOG4CPLUS_DEBUG(logger_, "memoryReadBlock()");
    rpct::StdLinkBoxAccess * lboxAccess = getLBoxAccess();
    try {
        lboxAccess->memoryReadBlock(address, data, count);
    } catch (rpct::CCUException & e) {
        LOG4CPLUS_WARN(logger_, "CCUException in BlockI2c::memoryReadBlock, trying again.");
        iRecoverCCU();
        lboxAccess->memoryReadBlock(address, data, count);
    } catch (TException & e) {
        LOG4CPLUS_WARN(logger_, "TException in BlockI2c::memoryReadBlock, trying again.");
        lboxAccess->memoryReadBlock(address, data, count);
    }
}
void BlockI2c::memoryWriteBlock(uint32_t address
                                , const unsigned char * data
                                , uint32_t count)
{
    LOG4CPLUS_DEBUG(logger_, "memoryWriteBlock()");
    rpct::StdLinkBoxAccess * lboxAccess = getLBoxAccess();
    try {
        lboxAccess->memoryWriteBlock(address, const_cast<unsigned char *>(data), count); // sorry for the const_cast.
    } catch (rpct::CCUException & e) {
        LOG4CPLUS_WARN(logger_, "CCUException in BlockI2c::memoryWriteBlock, trying again after attempt to recover.");
        iRecoverCCU();
        lboxAccess->memoryWriteBlock(address, const_cast<unsigned char *>(data), count);
    } catch (TException & e) {
        LOG4CPLUS_WARN(logger_, "TException in BlockI2c::memoryWriteBlock, trying again.");
        lboxAccess->memoryWriteBlock(address, const_cast<unsigned char *>(data), count);
    }
}

bool BlockI2c::waitForReady(int max_timesteps)
    throw (rpct::exception::Exception)
{
    LOG4CPLUS_DEBUG(logger_, "waitForReady()");
    readStatus();
    while (!isReady() && max_timesteps-- > 0)
        {
            ::usleep(bi2c::preset::sleep_timestep_useconds_);
            readStatus();
        }
    return (isReady());
}
void BlockI2c::writeBlock(bool readstatus)
    throw (rpct::exception::Exception)
{
    LOG4CPLUS_DEBUG(logger_, "writeBlock()");
    if ((readstatus && !waitForReady()) || !isReady())
        throw rpct::exception::Exception
            ("error executing bi2c command: ready bit not set");
    else
        {
            // this one doesn't use BlockI2c::memoryWriteBlock cause we want an init() if there was a ccu-exception.
            try {
                rpct::StdLinkBoxAccess * lboxAccess = getLBoxAccess();
                try {
                    LOG4CPLUS_DEBUG(logger_, "lboxAccess->memoryWriteBlock");
                    lboxAccess->memoryWriteBlock(rpct::CBPC::CBPC_BI2C_BUFE - block_.getInputSize() + 1
                                                 , block_.getInput()
                                                 , block_.getInputSize());
                } catch (rpct::CCUException & e) {
                    LOG4CPLUS_WARN(logger_, "CCUException in BlockI2c::writeBlock, trying again after attempt to recover + init.");
                    iRecoverCCU();
                    init();
                    waitForReady();
                    LOG4CPLUS_DEBUG(logger_, "lboxAccess->memoryWriteBlock");
                    lboxAccess->memoryWriteBlock(rpct::CBPC::CBPC_BI2C_BUFE - block_.getInputSize() + 1
                                                 , block_.getInput()
                                                 , block_.getInputSize());
                } catch (TException & e) {
                    LOG4CPLUS_WARN(logger_, "TException in BlockI2c::writeBlock, trying again.");
                    LOG4CPLUS_DEBUG(logger_, "lboxAccess->memoryWriteBlock");
                    lboxAccess->memoryWriteBlock(rpct::CBPC::CBPC_BI2C_BUFE - block_.getInputSize() + 1
                                                 , block_.getInput()
                                                 , block_.getInputSize());
                }
            } catch(rpct::exception::SystemException & e) {
                e.what((std::string("error writing bi2c commands: ") + e.what()).c_str());
                throw e;
            } catch(TException & e) {
                throw rpct::exception::AccessException
                    (std::string("error accessing bi2c: ") + e.what());
            }
        }
}

bool BlockI2c::waitForExecuted(int max_timesteps)
    throw (rpct::exception::Exception)
{
    LOG4CPLUS_DEBUG(logger_, "waitForExecuted()");
    readStatus();
    while (!(isAck() || isErr()) && max_timesteps-- > 0)
        {
            ::usleep(bi2c::preset::sleep_timestep_useconds_);
            readStatus();
        }
    return (isAck() || isErr());
}
void BlockI2c::readBlock(bool readstatus)
    throw (rpct::exception::Exception)
{
    LOG4CPLUS_DEBUG(logger_, "readBlock()");
    if ((readstatus && !waitForExecuted()) || !(isAck() || isErr()))
        throw rpct::exception::Exception
            ("error before reading block: no ack bit set.");
    else if (isErr())
        throw exception::Bi2cErrorException
            ("error before reading block: error bit set");
    else
        {
            try {
                memoryReadBlock(rpct::CBPC::CBPC_BI2C_BUFB
                                , block_.getOutput()
                                , block_.getOutputSize());
            } catch(rpct::exception::SystemException & e) {
                e.what((std::string("error reading bi2c output: ") + e.what()).c_str());
                throw e;
            } catch(TException & e) {
                throw rpct::exception::AccessException
                    (std::string("error reading bi2c output: ") + e.what());
            }
        }
}

void BlockI2c::idelay(unsigned int nseconds)
    throw (rpct::exception::Exception)
{
    try {
        block_.delay(nseconds);
    } catch (exception::BufferException & e) {
        execute();
        block_.delay(nseconds);
    }
}
void BlockI2c::iread(unsigned short channel
                     , unsigned char address
                     , unsigned char * data
                     , unsigned char count
                     , rpct::i2c::II2cResponse * i2c_response)
    throw (rpct::exception::Exception)
{
    try {
        block_.read(channel, address, data, count, i2c_response);
    } catch (exception::BufferException & e) {
        execute();
        block_.read(channel, address, data, count, i2c_response);
    }
}
void BlockI2c::iwrite(unsigned short channel
                      , unsigned char address
                      , const unsigned char * data
                      , unsigned char count
                      , rpct::i2c::II2cResponse * i2c_response)
    throw (rpct::exception::Exception)
{
    try {
        block_.write(channel, address, data, count, i2c_response);
    } catch (exception::BufferException & e) {
        execute();
        block_.write(channel, address, data, count, i2c_response);
    }
}

BlockI2c::BlockI2c(rpct::TCB & controlboard, unsigned short prescaler)
    : controlboard_(controlboard)
    , prescaler_(prescaler)
    , block_()
    , status_(0)
    , extended_status_(0)
    , error_command_(0)
    , init_(0)
{}

void BlockI2c::init()
    throw (rpct::exception::Exception)
{
    LOG4CPLUS_DEBUG(logger_, "init()");
    try {
        init_ = 0;
        disable();
        writePrescaler();
        enable();
        init_ = 1;
    } catch(rpct::exception::SystemException & e) {
        e.what((std::string("error initializing bi2c: ") + e.what()).c_str());
        throw e;
    } catch(TException & e) {
        throw rpct::exception::AccessException
            (std::string("error initializing bi2c: ") + e.what());
    }
}
void BlockI2c::reset()
{
    init_ = 0;
    status_ = 0;
    extended_status_ = 0;
    error_command_ = 0;
    block_.reset(false);
}

void BlockI2c::enable()
    throw (rpct::exception::Exception)
{
    LOG4CPLUS_DEBUG(logger_, "enable()");
    try {
        memoryWrite(rpct::CBPC::CBPC_I2C_CHAN, 0xfe);
    } catch(rpct::exception::SystemException & e) {
        e.what((std::string("error enabling bi2c: ") + e.what()).c_str());
        throw e;
    } catch(TException & e) {
        throw rpct::exception::AccessException
            (std::string("error enabling bi2c: ") + e.what());
    }
}
void BlockI2c::disable()
    throw (rpct::exception::Exception)
{
    LOG4CPLUS_DEBUG(logger_, "disable()");
    try {
        init_ = 0;
        memoryWrite(rpct::CBPC::CBPC_I2C_CHAN, 0x00);
        // sleep to keep SDA/SCL at 1 a while
        usleep(1000);
    } catch(rpct::exception::SystemException & e) {
        e.what((std::string("error disabling bi2c: ") + e.what()).c_str());
        throw e;
    } catch(TException & e) {
        throw rpct::exception::AccessException
            (std::string("error disabling bi2c: ") + e.what());
    }
}
void BlockI2c::writePrescaler()
    throw (rpct::exception::Exception)
{
    LOG4CPLUS_DEBUG(logger_, "writePrescaler()");
    try {
        memoryWrite(rpct::CBPC::CBPC_BI2C_PRESCALER, (prescaler_ > 0x0ff0 ?
                                                      0xff :
                                                      ((prescaler_ >> 4) & 0x00ff)
                                                      ));
    } catch(rpct::exception::SystemException & e) {
        e.what((std::string("error writing bi2c prescaler: ") + e.what()).c_str());
        throw e;
    } catch(TException & e) {
        throw rpct::exception::AccessException
            (std::string("error writing bi2c prescaler: ") + e.what());
    }
}

void BlockI2c::execute(bool recover_group
                       , bool postprocess_error)
    throw (rpct::exception::Exception)
{
    LOG4CPLUS_DEBUG(logger_, "execute()");
    LOG4CPLUS_DEBUG(logger_, block_);
    if (groupMode() && block_.getInputSize(0) == 0)
        { // in this situation a group is just too big for one block or someone forgot to close a group
            endGroup();
            startGroup();
            LOG4CPLUS_WARN(logger_, "A group was too big for one block or wasn't closed before execution.  Please check the code using BlockI2c.  Ended group.");
        }
    if (block_.getInputSize(0) > 0)
        {
            if (!init_ || !waitForReady())
                {
                    init();
                    waitForReady();
                }
            try {
                try {
                    writeBlock(false);
                    if (!waitForExecuted() || isErr())
                        {
                            LOG4CPLUS_WARN(logger_, (isErr() ? "error" : "timeout") << " executing bi2c.  Trying once more");
                            init();
                            writeBlock(true);
                            waitForExecuted();
                        }
                    if (!isAck() || isErr())
                        throw rpct::exception::Exception
                            ("error after writing block: no ack bit set or err bit set.");
                    else if (block_.getOutputSize())
                        readBlock(false); // throws if isErr(), so postprocess is safe
                    block_.postprocess();
                } catch(rpct::exception::Exception & e) {
                    e.what((std::string("error executing bi2c: ") + e.what()).c_str());
                    throw e;
                } catch(TException & e) {
                    throw rpct::exception::AccessException
                        (std::string("error executing bi2c: ") + e.what());
                }
            } catch(rpct::exception::Exception & e) {
                if (postprocess_error)
                    block_.postprocess(&e);
                else
                    throw e;
            }
            block_.reset(recover_group);
        }
}
void BlockI2c::readStatus()
    throw (rpct::exception::Exception)
{
    try {
        status_ = memoryRead(rpct::CBPC::CBPC_BI2C_STATUS);
        if (isErr())
            {
                readExtendedStatus();
                readErrorCommand();
                LOG4CPLUS_WARN(logger_, "error bit set, extended status " << std::hex << std::showbase << (int)getExtendedStatus() << ", error command " << (int)getErrorCommand() << std::noshowbase << std::dec);
            }

    } catch(rpct::exception::SystemException & e) {
        e.what((std::string("error reading bi2c status: ") + e.what()).c_str());
        throw e;
    } catch(TException & e) {
        throw rpct::exception::AccessException
            (std::string("error reading bi2c status: ") + e.what());
    }
}
void BlockI2c::readExtendedStatus()
    throw (rpct::exception::Exception)
{
    try {
        extended_status_ = memoryRead(rpct::CBPC::CBPC_BI2C_EXTSTATUS);
    } catch(rpct::exception::SystemException & e) {
        e.what((std::string("error reading extended bi2c status: ") + e.what()).c_str());
        throw e;
    } catch(TException & e) {
        throw rpct::exception::AccessException
            (std::string("error reading extended bi2c status: ") + e.what());
    }
}
void BlockI2c::readErrorCommand()
    throw (rpct::exception::Exception)
{
    try {
        error_command_ = memoryRead(rpct::CBPC::CBPC_BI2C_RD_NMBR);
    } catch(rpct::exception::SystemException & e) {
        e.what((std::string("error bi2c error command: ") + e.what()).c_str());
        throw e;
    } catch(TException & e) {
        throw rpct::exception::AccessException
            (std::string("error bi2c error command: ") + e.what());
    }
}

std::ostream & operator<< (std::ostream& outstream, const BlockI2c& blocki2c)
{
    std::stringstream ss;
    ss << "BlockI2c(" << "cb_" << blocki2c.controlboard_.getId() << ",prescaler_" << (int)(blocki2c.prescaler_)  << ")";
    outstream << ss.str();
    return outstream;
}

} // namespace i2c
} // namespace devices
} // namespace rpct
