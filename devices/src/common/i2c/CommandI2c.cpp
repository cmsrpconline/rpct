#include "rpct/devices/i2c/CommandI2c.h"

#include "rpct/tools/Time.h"

#include "rpct/i2c/TI2CMasterCorev09.h"
#include "rpct/devices/cb.h"

#include "rpct/ii/i2c/I2cResponse.h"

namespace rpct {
namespace devices {
namespace i2c {

log4cplus::Logger CommandI2c::logger_ = log4cplus::Logger::getInstance("CommandI2c");
void CommandI2c::setupLogger(std::string namePrefix) {
	logger_ = log4cplus::Logger::getInstance(namePrefix + "." + logger_.getName());
}

void CommandI2c::selectChannel(unsigned char channel)
    throw (rpct::exception::Exception)
{
    if (channel > max_channel_)
        throw rpct::exception::InputException("invalid i2c channel");
    if (channel != channel_)
        {
            try {
                getI2c(); // ensure initialization
                controlboard_.selectI2CChannel(channel);
                channel_ = channel;
            } catch(TException & e) {
                error_ = 1;
                throw rpct::exception::AccessException
                    (std::string("problem setting i2c channel: ") + e.what());
            }
        }
}

rpct::TI2CMasterCorev09 * CommandI2c::getI2c()
    throw (rpct::exception::SystemException)
{
    try {
        if (!i2c_)
            init();
        return i2c_;
    } catch(rpct::exception::SystemException & e) {
        error_ = 1;
        throw e;
    } catch(TException & e) {
        error_ = 1;
        throw rpct::exception::SystemException
            (std::string("error getting controlboard i2c: ") + e.what());
    }
}
void CommandI2c::idelay(unsigned int nseconds)
    throw (rpct::exception::Exception)
{
    if (group_mode_ && error_)
        throw rpct::exception::Exception
            ("command i2c in group mode and an error occured before.");
    else if (nseconds)
        {
            rpct::tools::Time _time(0, nseconds);
            _time.sleep();
        }
}
void CommandI2c::iread(unsigned short channel
                       , unsigned char address
                       , unsigned char * data
                       , unsigned char count
                       , rpct::i2c::II2cResponse * i2c_response)
    throw (rpct::exception::Exception)
{
    if (group_mode_ && error_)
        throw rpct::exception::Exception
            ("command i2c in group mode and an error occured before.");
    else if (!count)
        { // do nothing
        }
    else if (count > 2)
        {
            error_ = 1;
            throw rpct::exception::InputException
                ("at most 2 byte reading per command is implemented");
        }
    else
        {
            rpct::TI2CMasterCorev09 * i2c = getI2c();
            selectChannel(channel); // checks channel value
            try {
                if (count == 1)
                    *data = i2c->Read8(address);
                else // == 2
                    i2c->ReadADD(address, *(data), *(data + 1));
            } catch(TException & e) {
                error_ = 1;
                throw rpct::exception::AccessException
                    (std::string("problem reading: ") + e.what());
            }
        }
    if (i2c_response)
        i2c_response->i2c_ready();
}
void CommandI2c::iwrite(unsigned short channel
                        , unsigned char address
                        , unsigned char const * data
                        , unsigned char count
                        , rpct::i2c::II2cResponse * i2c_response)
    throw (rpct::exception::Exception)
{
    if (group_mode_ && error_)
        throw rpct::exception::Exception
            ("command i2c in group mode and an error occured before.");
    else if (!count)
        { // do nothing
        }
    else
        {
            rpct::TI2CMasterCorev09 * i2c = getI2c();
            selectChannel(channel); // checks channel value
            try {
                i2c->Write(address, const_cast<unsigned char *>(data), count);
                // sorry for the const_cast, checked the code and it's safe.
            } catch(TException & e) {
                error_ = 1;
                throw rpct::exception::AccessException
                    (std::string("problem writing: ") + e.what());
            }
        }
    if (i2c_response)
        i2c_response->i2c_ready();
}

CommandI2c::CommandI2c(TCB & controlboard, unsigned short prescaler)
    : controlboard_(controlboard)
    , i2c_(0)
    , prescaler_(prescaler)
    , channel_(max_channel_+1)
    , group_mode_(0)
    , error_(0)
{}

void CommandI2c::init()
    throw (rpct::exception::Exception)
{
    try {
        i2c_ = dynamic_cast<rpct::TI2CMasterCorev09 *>(&(controlboard_.getI2C()));
        if (!i2c_)
            throw TException("0 pointer to i2c");
        writePrescaler(); // since getI2c for cb no longer initializes
        channel_ = max_channel_ + 1;
    } catch(TException & e) {
        error_ = 1;
        i2c_ = 0;
        throw rpct::exception::SystemException
            (std::string("error getting controlboard i2c for init: ") + e.what());
    }
}

void CommandI2c::enable()
    throw (rpct::exception::Exception)
{
    try {
        getI2c()->WriteControlReg(TI2CMasterCore::BIT_EN);
    } catch(TException & e) {
        throw rpct::exception::AccessException
            (std::string("error enabling: ") + e.what());
    }
}
void CommandI2c::disable()
    throw (rpct::exception::Exception)
{
    try {
        getI2c()->WriteControlReg(0);
    } catch(TException & e) {
        throw rpct::exception::AccessException
            (std::string("error disabling: ") + e.what());
    }
}

void CommandI2c::writePrescaler()
    throw (rpct::exception::Exception)
{
    rpct::TI2CMasterCorev09 * i2c = getI2c();
    try {
        i2c->WriteControlReg(0);
        i2c->SetPrescaleValue(prescaler_);
        i2c->WriteControlReg(TI2CMasterCore::BIT_EN);
    } catch(TException & e) {
        throw rpct::exception::AccessException
            (std::string("error writing prescaler: ") + e.what());
    }
}

} // namespace i2c
} // namespace devices
} // namespace rpct
