#include "rpct/devices/i2c/Block.h"

#include <sstream>

#include "rpct/devices/i2c/exception/Bi2cException.h"

#include "rpct/ii/i2c/I2cResponse.h"

namespace rpct {
namespace devices {
namespace i2c {

BI2cCommand::BI2cCommand(bool read
                         , int group
                         , int in
                         , int out
                         , unsigned char length
                         , unsigned char * data
                         , rpct::i2c::II2cResponse * i2c_response)
    : length_(length)
    , data_(data)
    , read_(read)
    , i2c_response_(i2c_response)
    , group_(group)
    , in_(in)
    , out_(out)
{}

void BlockProperties::reset()
{
    channel_ = max_channel_+1;
    group_ = 0;
    input_size_ = 0;
    output_size_ = 0;
    nbi2c_commands_ = 0;
}

Block::Block()
    : group_mode_(0)
{
    properties_[0].reset();
    properties_[1].reset();
}
unsigned char const * Block::getInput(int start) const
    throw (rpct::exception::Exception)
{
    if (start == 0 || (start >= 0 && start < input_size()))
        return (input_ + start);
    else
        throw rpct::exception::InputException
            ("requesting out-of-range element in block input.");
}
unsigned char * Block::getInput(int start)
    throw (rpct::exception::Exception)
{
    if (start == 0 || (start >= 0 && start < input_size()))
        return (input_ + start);
    else
        throw rpct::exception::InputException
            ("requesting out-of-range element in block input.");
}
unsigned char const * Block::getOutput(int start) const
    throw (rpct::exception::Exception)
{
    if (start == 0 || (start >= 0 && start < output_size()))
        return (output_ + start);
    else
        throw rpct::exception::InputException
            ("requesting out-of-range element in block output.");
}
unsigned char * Block::getOutput(int start)
    throw (rpct::exception::Exception)
{
    if (start == 0 || (start >= 0 && start < output_size()))
        return (output_ + start);
    else
        throw rpct::exception::InputException
            ("requesting out-of-range element in block output.");
}
BI2cCommand * Block::getBI2cCommand(int start)
    throw (rpct::exception::Exception)
{
    if (start == 0 || (start >= 0 && start < nbi2c_commands()))
        return (bi2c_commands_ + start);
    else
        throw rpct::exception::InputException
            ("requesting out-of-range bi2c command.");
}
const BI2cCommand * Block::getBI2cCommand(int start) const
    throw (rpct::exception::Exception)
{
    if (start == 0 || (start >= 0 && start < nbi2c_commands()))
        return (bi2c_commands_ + start);
    else
        throw rpct::exception::InputException
            ("requesting out-of-range bi2c command.");
}

void Block::postprocess(rpct::exception::Exception const * e)
{
    int max = properties_[0].nbi2c_commands_;
    if (e)
        {
            for (int i = 0 ; i < max ; ++i)
                if (bi2c_commands_[i].i2c_response_)
                    bi2c_commands_[i].i2c_response_->i2c_error(e);
        }
    else
        {
            for (int i = 0 ; i < max ; ++i)
                {
                    if (bi2c_commands_[i].read_ && bi2c_commands_[i].data_)
                        memcpy(bi2c_commands_[i].data_, (output_ + bi2c_commands_[i].out_), bi2c_commands_[i].length_);
                    if (bi2c_commands_[i].i2c_response_)
                        bi2c_commands_[i].i2c_response_->i2c_ready();
                }
        }
}
void Block::reset(bool recover_group)
{
    if (recover_group && group_mode_ && properties_[1].input_size_ != properties_[0].input_size_)
        {
            int din  = properties_[0].input_size_;
            int dout = properties_[0].output_size_;
            int dgroup = properties_[0].group_;

            unsigned char count = 1;
            if (input_[din] ==  0xff) // special command
                {
                    unsigned char tchannel = ((input_[din+1] & 0xf0) >> 4);
                    if (tchannel != 0xf)
                        properties_[0].channel_ = tchannel;
                    count = input_[din+1] & 0x0f;
                    din += bi2c::special_command_size_; // now skip the special command
                }

            // add a special command
            unsigned char * out = input_;
            unsigned char gchannel = properties_[1].channel_;
            properties_[1].channel_ = max_channel_+1; // force a channel set in the special command
            specialcommand(out, properties_[0].channel_, count);
            properties_[1].channel_ = gchannel;

            // copy what was there
            memmove(input_ + bi2c::special_command_size_
                    , (input_ + din)
                    , (properties_[1].input_size_-din));

            // the actual moving distance instead of the start position
            din -= bi2c::special_command_size_;

            memmove(bi2c_commands_
                    , (bi2c_commands_ + properties_[0].nbi2c_commands_)
                    , (properties_[1].nbi2c_commands_-properties_[0].nbi2c_commands_) * sizeof(BI2cCommand));

            properties_[1].group_ -= - dgroup;
            properties_[1].input_size_ -= din;
            properties_[1].output_size_ -= dout;
            properties_[1].nbi2c_commands_ -= properties_[0].nbi2c_commands_;

            properties_[0].reset();
            for (int i = 0 ; i < properties_[0].nbi2c_commands_ ; ++i)
                bi2c_commands_[i].move(-din, -dout, -dgroup);
        }
    else
        {
            properties_[0].reset();
            properties_[1].reset();
            if (!recover_group)
                group_mode_ = false;
            else if (group_mode_)
                ++group();
        }
}

bool Block::startGroup()
{
    if (!group_mode_)
        {
            properties_[1] = properties_[0];
            group_mode_ = 1;
            ++group();
            return true;
        }
    return false;
}
void Block::cancelGroup()
{
    group_mode_ = 0;
}
void Block::endGroup()
{
    if (group_mode_)
        {
            properties_[0] = properties_[1];
            group_mode_ = 0;
        }
}

void Block::delay(unsigned int nseconds)
    throw (rpct::exception::Exception)
{
    if (nseconds)
        {
            int totalcommandsize = bi2c::delay_command_size_ + input_size();

            if (totalcommandsize > bi2c::buffer_size_)
                throw exception::InputBufferException
                    ("adding this command would exceed the input buffer size.");
            // check output buffer size vs input buffer size
            bool valid = true;
            for (int i = 0 ; valid && i < nbi2c_commands() ; ++i)
                valid = bi2c_commands_[i].valid(bi2c::delay_command_size_);
            if (valid)
                {
                    nseconds /= 25;
                    if (nseconds > 0xff0)
                        nseconds = 0xff0;
                    else if (nseconds & 0x0f > 0x08) // round
                        nseconds += 0x10;
                    unsigned char time = ((nseconds>>4) & 0xff);

                    // constructing the command
                    unsigned char * out = input_ + input_size();
                    delaycommand(out, time);
                    input_size() += bi2c::delay_command_size_;
                }
            else
                throw exception::BufferOverRunException
                    ("adding this command would cause the output to overrun the input buffer.");
        }
}
void Block::read(unsigned char _channel
                 , unsigned char address
                 , unsigned char * data
                 , unsigned char count
                 , rpct::i2c::II2cResponse * i2c_response)
    throw (rpct::exception::Exception)
{
    if (_channel > max_channel_)
        throw rpct::exception::InputException("invalid i2c channel");
    if (address > 0x7e) // to keep it from becoming a special command
        throw rpct::exception::InputException("invalid i2c address");
    if (!count)
        return;
    else if (count > 0xf)
        throw rpct::exception::InputException
            ("impossible to read more than 15 bytes or less than 1 byte in one command");

    int commandsize = bi2c::command_size_;
    if (_channel != channel() || count != 1)
        commandsize += bi2c::special_command_size_;

    int totalcommandsize = commandsize + input_size();

    if (totalcommandsize > bi2c::buffer_size_)
        throw exception::InputBufferException
            ("adding this command would exceed the input buffer size.");
    if (output_size() + count > bi2c::buffer_size_)
        throw exception::OutputBufferException
            ("adding this command would exceed the output buffer size.");
    // check output buffer size vs input buffer size
    bi2c_commands_[nbi2c_commands()] = BI2cCommand(true
                                                   , (group_mode_?group() : 0)
                                                   , input_size()
                                                   , output_size()
                                                   , count, data, i2c_response);
    bool valid = true;
    for (int i = 0 ; valid && i <= nbi2c_commands() ; ++i)
        valid = bi2c_commands_[i].valid(totalcommandsize);
    if (valid)
        {
            ++nbi2c_commands();
            output_size() += count;

            // constructing the command
            unsigned char * out = input_ + input_size();
            if (_channel != channel() || count != 1)
                specialcommand(out, _channel, count);
            readcommand(out, address);

            input_size() += commandsize;
        }
    else
        throw exception::BufferOverRunException
            ("adding this command would cause the output to overrun the input buffer.");
}
void Block::write(unsigned char _channel
                  , unsigned char address
                  , unsigned char const * data
                  , unsigned char count
                  , rpct::i2c::II2cResponse * i2c_response)
    throw (rpct::exception::Exception)
{
    if (_channel > max_channel_)
        throw rpct::exception::InputException("invalid i2c channel");
    if (address > 0x7e) // to keep it from becoming a delay command
        throw rpct::exception::InputException("invalid i2c address");
    if (!count)
        return;
    else if (count > 0xf)
        throw rpct::exception::InputException
            ("impossible to write more than 15 bytes or less than 1 byte in one command");

    int commandsize = bi2c::command_size_ + count;
    if (_channel != channel() || count != 1)
        commandsize += bi2c::special_command_size_;

    int totalcommandsize = commandsize + input_size();

    if (totalcommandsize > bi2c::buffer_size_)
        throw exception::InputBufferException
            ("adding this command would exceed the input buffer size.");
    // check output buffer size vs input buffer size
    bi2c_commands_[nbi2c_commands()] = BI2cCommand(true
                                                   , (group_mode_?group() : 0)
                                                   , input_size()
                                                   , output_size()
                                                   , count, (output_ + output_size()), i2c_response);
    bool valid = true;
    for (int i = 0 ; valid && i < nbi2c_commands() ; ++i)
        valid = bi2c_commands_[i].valid(totalcommandsize);
    if (valid)
        {
            ++nbi2c_commands();

            // constructing the command
            unsigned char * out = &(input_[input_size()]);
            if (_channel != channel() || count != 1)
                specialcommand(out, _channel, count);
            writecommand(out, address, data, count);

            input_size() += commandsize;
        }
    else
        throw exception::BufferOverRunException
            ("adding this command would cause the output to overrun the input buffer.");
}
std::ostream & operator<< (std::ostream& outstream, const Block& block)
{
    std::stringstream ss("Block:", std::ios_base::out | std::ios_base::ate);
    ss << block.input_size() << ':' << block.output_size() << ':' << block.nbi2c_commands() << ':';
    int size = 1;
    int max = block.properties_[block.group_mode_].input_size_;
    int gmax = block.properties_[0].input_size_;
    for (int i = 0 ; i < max ; ++i)
        {
            if (i == gmax)
                ss << "gm:";
            if (block.input_[i] == 0xff) // special command
                {
                    size = (int)(block.input_[i+1] & 0x0f);
                    ss << "s(" << (int)((block.input_[i+1] & 0xf0) >> 4) << ',' << size << ");";
                    ++i;
                }
            else if (block.input_[i] == 0x7f) // delay command
                {
                    ss << "d(" << (int)(((block.input_[i+1]<<4) & 0xff0) * 25) << ");";
                    ++i;
                }
            else if (block.input_[i] < 0x80) // write command
                {
                    ss << "w(" << std::showbase << std::hex << (unsigned int)(block.input_[i] & 0x7f) << ',';
                    ss << (unsigned int)(block.input_[i+1]);
                    for (int j = 1 ; j < size ; ++j)
                        ss << ':' << (unsigned int)(block.input_[i+j+1]);
                    ss << std::dec << std::noshowbase << ");";
                    i += size;
                    size = 1;
                }
            else // read command
                {
                    ss << "r(" << std::showbase << std::hex << (unsigned int)(block.input_[i] & 0x7f) <<
                        std::dec << std::noshowbase << ',' << size << ");";
                    size = 1;
                }
        }
    outstream << ss.str();
    return outstream;
}

} // namespace i2c
} // namespace devices
} // namespace rpct
