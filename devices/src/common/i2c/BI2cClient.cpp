#include "rpct/devices/i2c/BI2cClient.h"

#include "rpct/devices/cb.h"

#include "rpct/devices/i2c/Block.h"
#include "rpct/devices/i2c/exception/Bi2cException.h"

#include "rpct/ii/i2c/I2cResponse.h"

namespace rpct {
namespace devices {
namespace i2c {

log4cplus::Logger BI2cClient::logger_ = log4cplus::Logger::getInstance("BI2cClient");
void BI2cClient::setupLogger(std::string namePrefix) {
	logger_ = log4cplus::Logger::getInstance(namePrefix + "." + logger_.getName());
}

void BI2cClient::parseBlockCommands(Block & block)
{
    commandi2c_.reset();

    unsigned char count = 1;
    unsigned short channel = max_channel_ + 1;

    int max = block.getInputSize(0);
    int bc = 0;
    int group = 0;

    const unsigned char * input = block.getInput();
    unsigned char * output = block.getOutput();
    BI2cCommand * bi2c_commands = block.getBI2cCommand();

    for (int i = 0 ; i < max; ++i)
        {
            if (input[i] == 0xff) // special command
                {
                    count = (input[i+1] & 0x0f);
                    if ((input[i+1] & 0xf0) != 0xf0)
                        channel = ((input[i+1] & 0xf0) >> 4);
                    ++i;
                }
            else if (input[i] == 0x7f) // delay command
                {
                    try {
                        commandi2c_.delay(((input[i+1]<<4) & 0xff0) * 25);
                        ++i;
                    } catch (rpct::exception::Exception & e) {
                    }
                }
            else if (input[i] < 0x80) // write command
                {
                    BI2cCommand & command = bi2c_commands[bc++];
                    if (group != command.group_)
                        {
                            if (group)
                                commandi2c_.endGroup();
                            if (command.group_)
                                commandi2c_.startGroup();
                        }
                    group = command.group_;
                    try {
                        commandi2c_.write(channel, input[i] & 0x7f, &(input[i+1]), count, command.i2c_response_);
                    } catch (rpct::exception::Exception & e) {
                        LOG4CPLUS_ERROR(logger_, "in BI2cClient::parseBlockCommands: could not write ("
                                        << (input[i] & 0x7f) << "): " << e.what());
                    }
                    i += count;
                    count = 1;
                }
            else // read command
                {
                    BI2cCommand & command = bi2c_commands[bc++];
                    if (group != command.group_)
                        {
                            if (group)
                                commandi2c_.endGroup();
                            if (command.group_)
                                commandi2c_.startGroup();
                        }
                    group = command.group_;
                    try {
                        commandi2c_.read(channel, input[i] & 0x7f, command.data_, count, command.i2c_response_);
                    } catch (rpct::exception::Exception & e) {
                        LOG4CPLUS_ERROR(logger_, "in BI2cClient::parseBlockCommands: could not read ("
                                        << (input[i] & 0x7f) << "): " << e.what());
                    }
                    output += count;
                    count = 1;
                }
        }
    block.reset(true);
}

BI2cClient::BI2cClient(TCB & controlboard
                       , bool bi2c_mode
                       , bool ci2c_fallback)
    : controlboard_(controlboard)
    , commandi2c_(controlboard.getCommandI2c())
    , blocki2c_(controlboard.getBlockI2c())
    , init_(0)
    , bi2c_mode_(bi2c_mode)
    , ci2c_fallback_(ci2c_fallback)
{}

void BI2cClient::init() throw (rpct::exception::Exception)
{
    LOG4CPLUS_DEBUG(logger_, "init()");
    if (!bi2c_mode_ || ci2c_fallback_)
        {
            try {
                init_ = 0;
                commandi2c_.init();
                init_ = 1;
            } catch(rpct::exception::SystemException & e) {
                LOG4CPLUS_WARN(logger_, e.what());
                throw e;
            }
        }
    if (bi2c_mode_)
        {
            try {
                init_ = 0;
                blocki2c_.init();
                init_ = 1;
            } catch(rpct::exception::Exception & e) {
                LOG4CPLUS_WARN(logger_, e.what());
                throw e;
            }
        }
}

void BI2cClient::reset()
{
    commandi2c_.reset();
    blocki2c_.reset();
    init_ = 0;
}

void BI2cClient::execute() throw (rpct::exception::Exception)
{
    LOG4CPLUS_DEBUG(logger_, "execute()");
    if (bi2c_mode_)
        {
            if (ci2c_fallback_)
                {
                    try {
                        blocki2c_.execute(true, false);
                    } catch (rpct::exception::Exception & e) {
                        LOG4CPLUS_WARN(logger_, "in execute(): "
                                       << e.what() << "; falling back to command i2c.");
                        parseBlockCommands(blocki2c_.getBlock());
                        blocki2c_.init();
                    }
                }
            else
                blocki2c_.execute();
        }
}

void BI2cClient::enable()
    throw (rpct::exception::Exception)
{
    if (bi2c_mode_)
        {
            if (ci2c_fallback_)
                commandi2c_.enable();
            blocki2c_.enable();
        }
    else
        commandi2c_.enable();
}
void BI2cClient::disable()
    throw (rpct::exception::Exception)
{
    init_ = 0;
    if (bi2c_mode_)
        {
            if (ci2c_fallback_)
                commandi2c_.disable();
            blocki2c_.disable();
        }
    else
        commandi2c_.disable();
}
void BI2cClient::setPrescaler(unsigned short prescaler)
{
    if (bi2c_mode_)
        {
            if (ci2c_fallback_)
                commandi2c_.setPrescaler(prescaler);
            blocki2c_.setPrescaler(prescaler);
        }
    else
        commandi2c_.setPrescaler(prescaler);
}
unsigned short BI2cClient::getPrescaler() const
{
    if (bi2c_mode_)
        return blocki2c_.getPrescaler();
    else
        return commandi2c_.getPrescaler();
}
void BI2cClient::writePrescaler()
    throw (rpct::exception::Exception)
{
    if (bi2c_mode_)
        {
            if (ci2c_fallback_)
                commandi2c_.writePrescaler();
            blocki2c_.writePrescaler();
        }
    else
        commandi2c_.writePrescaler();
}


void BI2cClient::idelay(unsigned int nseconds)
    throw (rpct::exception::Exception)
{
    if (bi2c_mode_)
        {
            try {
                blocki2c_.getBlock().delay(nseconds);
            } catch (exception::BufferException & e) {
                execute();
                blocki2c_.getBlock().delay(nseconds);
            }
        }
    else
        commandi2c_.delay(nseconds);
}
void BI2cClient::iread(unsigned short channel
                       , unsigned char address
                       , unsigned char * data
                       , unsigned char count
                       , rpct::i2c::II2cResponse * i2c_response)
    throw (rpct::exception::Exception)
{
    if (bi2c_mode_)
        {
            try {
                blocki2c_.getBlock().read(channel, address, data, count, i2c_response);
            } catch (exception::BufferException & e) {
                execute();
                blocki2c_.getBlock().read(channel, address, data, count, i2c_response);
            }
        }
    else
        commandi2c_.read(channel, address, data, count, i2c_response);
}
void BI2cClient::iwrite(unsigned short channel
                        , unsigned char address
                        , const unsigned char * data
                        , unsigned char count
                        , rpct::i2c::II2cResponse * i2c_response)
    throw (rpct::exception::Exception)
{
    if (bi2c_mode_)
        {
            try {
                blocki2c_.getBlock().write(channel, address, data, count, i2c_response);
            } catch (exception::BufferException & e) {
                execute();
                blocki2c_.getBlock().write(channel, address, data, count, i2c_response);
            }
        }
    else
        commandi2c_.write(channel, address, data, count, i2c_response);
}
void BI2cClient::setBi2cMode(bool bi2c_mode)
    throw (rpct::exception::Exception)
{
    if (bi2c_mode != bi2c_mode_)
        {
            try {
                if (bi2c_mode_) // changing mode implies executing all commands
                    endGroup();
                execute();
            } catch(rpct::exception::Exception & e) {
                LOG4CPLUS_WARN(logger_, "in setBi2cMode(): " << e.what());
                throw e;
            }
            bi2c_mode_ = bi2c_mode;

            if (init_)
                {
                    if (!bi2c_mode_ && !ci2c_fallback_)
                        {
                            try {
                                commandi2c_.init();
                            } catch(rpct::exception::SystemException & e) {
                                LOG4CPLUS_WARN(logger_, "in setBi2cMode(): " << e.what());
                                throw e;
                            }
                        }
                    else if (bi2c_mode_)
                        {
                            try {
                                blocki2c_.init();
                            } catch(rpct::exception::Exception & e) {
                                LOG4CPLUS_WARN(logger_, "in setBi2cMode(): " << e.what());
                                throw e;
                            }
                        }
                }
        }
}
void BI2cClient::setCi2cFallback(bool ci2c_fallback)
    throw (rpct::exception::Exception)
{
    if (init_ && bi2c_mode_ && !ci2c_fallback_ && ci2c_fallback)
        {
            try {
                commandi2c_.init();
            } catch(rpct::exception::Exception & e) {
                LOG4CPLUS_WARN(logger_, "in setCi2cFallback(): " << e.what());
                throw e;
            }
        }
    ci2c_fallback_ = ci2c_fallback;
}

} // namespace i2c
} // namespace devices
} // namespace rpct
