#include "rpct/devices/i2c/const.h"

#include "rpct/lboxaccess/cbpc_const.h"

namespace rpct {
namespace devices {
namespace i2c {

namespace bi2c {

//const int buffer_size_ = rpct::CBPC::CBPC_BI2C_BUFE - rpct::CBPC::CBPC_BI2C_BUFB + 1;
//const int command_size_ = 1;
//const int special_command_size_ = 2;

namespace preset {
unsigned short prescaler_ = 0x0ff0;
bool bi2c_mode_ = false;
bool ci2c_fallback_ = true;

int sleep_timestep_useconds_ = 250000;
int timeout_timesteps_ = 10;
} // namespace preset

} // namespace bi2c

const unsigned char max_channel_ = 0x0f;

namespace preset {
unsigned short prescaler_ = 0x0200;
} // namespace preset

} // namespace i2c
} // namespace devices
} // namespace rpct
