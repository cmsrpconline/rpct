#include "rpct/devices/fsb.h"
#include "rpct/devices/VmeCrate.h"
#include "rpct/devices/FinalSortBxData.h"
#include "rpct/devices/FixedHardwareSettings.h"
#include "rpct/ii/ConfigurationFlags.h"


//---------------------------------------------------------------------------


using namespace std;
using namespace log4cplus;

namespace rpct {

Logger TFsbDevice::logger = Logger::getInstance("TFsbDevice");
void TFsbDevice::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}
//---------------------------------------------------------------------------
#define IID_FILE "RPC_system_def.iid"
#define IID_CLASS TFsbDevice
#include "iid2cdef2.h"


#define IID_FILE "RPC_TB3_def.iid"
#define IID_CLASS TFsbDevice
#include "iid2cdef2.h"

#define IID_FILE "RPC_TC_SORT_def.iid"
#define IID_CLASS TFsbDevice
#include "iid2cdef2.h"

#define IID_FILE "RPC_SORT_def.iid"
#define IID_CLASS TFsbDevice
#include "iid2cdef2.h"

//---------------------------------------------------------------------------


#define IID_FILE "RPC_TB3_vme.iid"
#define IID_CLASS TFsbVme
#include "iid2c2.h"

const HardwareItemType TFsbVme::TYPE(HardwareItemType::cDevice, "FSBVME");

TFsbVme::TFsbVme(int vme_board_address, IBoard& board) : TFsbDevice(
            board.getId() * 1000,
            VME_IDENTIFIER.c_str(),
            WORD_IDENTIFIER,
            "VME",
            TYPE,
            VME_VERSION.c_str(),
            WORD_VERSION,
            WORD_CHECKSUM,
            0,
            board,
            -1,
            BuildMemoryMap(),
            vme_board_address
        ), ABootContr(0), JtagIO(0)
{
}

void TFsbVme::SetJtagEnable(bool value)
{
    writeBits(BITS_STATUS_JTAG_ENA, value ? 1 : 0);
}

TFsbVme::~TFsbVme()
{
    delete ABootContr;
    delete JtagIO;
}

//---------------------------------------------------------------------------
#ifdef CII_FS
    #define IID_CLASS TFsbSortFinal
    #include "CII_SC_FSORT_iicfg_tab.cpp"
    #undef IID_CLASS
#else
    #define IID_FILE "RPC_SORT_final.iid"
    #define IID_CLASS TFsbSortFinal
    #include "iid2c2.h"
	const int TFsbSortFinal::II_ADDR_SIZE = TFsbSortFinal::FSORT_ADDR_WIDTH;
	const int TFsbSortFinal::II_DATA_SIZE = TFsbSortFinal::FSORT_DATA_WIDTH;
#endif

const HardwareItemType TFsbSortFinal::TYPE(HardwareItemType::cDevice, "FINALSORT");

TFsbSortFinal::TFsbSortFinal(int id, int vme_board_address, int ii_addr, const char* desc,
                             IBoard& board): TFsbDevice(
                                         id,
                                         FSORT_IDENTIFIER.c_str(),
                                         WORD_IDENTIFIER,
                                         desc,
                                         TYPE,
                                         FSORT_VERSION.c_str(),
                                         WORD_VERSION,
                                         WORD_CHECKSUM,
                                         ii_addr,
                                         board,
                                         0,
                                         BuildMemoryMap(),
                                         vme_board_address), DaqDiagCtrl(0), PulserDiagCtrl(0), DiagnosticReadout(0), Pulser(0),
        statisticsDiagCtrl_(0), I2C(0), TTCrxI2C(7, board.getDescription()),
        qpll_(*this, logger,
        		VECT_QPLL,
        		BITS_QPLL_MODE,
        		BITS_QPLL_CTRL,
        		BITS_QPLL_SELECT,
        		BITS_QPLL_LOCKED,
        		BITS_QPLL_ERROR,
        		BITS_QPLL_CLK_LOCK)
{
    monitorItems_.push_back(MonitorItemName::FIRMWARE);
    monitorItems_.push_back(MonitorItemName::QPLL);
    monitorItems_.push_back(MonitorItemName::TTCRX);
}

TFsbSortFinal::~TFsbSortFinal()
{
    delete DaqDiagCtrl;
    delete PulserDiagCtrl;
    delete Pulser;
    delete DiagnosticReadout;
    delete I2C;

    for(unsigned int i = 0; i < histoManagers_.size(); i++) {
        delete histoManagers_[i];
    }
}


TDiagCtrl& TFsbSortFinal::GetDaqDiagCtrl() {
    if (DaqDiagCtrl == NULL) {
        DaqDiagCtrl = new TDiagCtrl(*this,
                                    VECT_DAQ,
                                    BITS_DAQ_PROC_REQ,
                                    BITS_DAQ_PROC_ACK,
                                    BITS_DAQ_TIMER_LOC_ENA,
                                    0,
                                    BITS_DAQ_TIMER_START,
                                    BITS_DAQ_TIMER_STOP,
                                    BITS_DAQ_TIMER_TRIG_SEL,
                                    WORD_DAQ_TIMER_LIMIT,
                                    WORD_DAQ_TIMER_COUNT,
                                    WORD_DAQ_TIMER_TRG_DELAY,
                                    "DAQ_DIAG_CTRL");
    }

    return *DaqDiagCtrl;
}

TPulser& TFsbSortFinal::GetPulser() {
    if (Pulser == NULL) {
        Pulser = new FinalSortPulser(*this,
                             "PULSER",
                             GetPulserDiagCtrl(),
                             AREA_MEM_PULSE,
                             WORD_PULSER_LENGTH,
                             BITS_PULSER_REPEAT_ENA,
                             BITS_PULSER_OUT_ENA);
    }
    return *Pulser;
}

TDiagCtrl& TFsbSortFinal::GetPulserDiagCtrl() {
    if (PulserDiagCtrl == NULL) {
        PulserDiagCtrl = new TDiagCtrl(*this,
                                       VECT_PULSER,
                                       BITS_PULSER_PROC_REQ,
                                       BITS_PULSER_PROC_ACK,
                                       BITS_PULSER_TIMER_LOC_ENA,
                                       0,
                                       BITS_PULSER_TIMER_START,
                                       BITS_PULSER_TIMER_STOP,
                                       BITS_PULSER_TIMER_TRIG_SEL,
                                       WORD_PULSER_TIMER_LIMIT,
                                       WORD_PULSER_TIMER_COUNT,
                                       WORD_PULSER_TIMER_TRG_DELAY,
                                       "PULSER_DIAG_CTRL");
    }

    return *PulserDiagCtrl;
}


TStandardDiagnosticReadout& TFsbSortFinal::GetDiagnosticReadout() {
    if (DiagnosticReadout == 0) {
        DiagnosticReadout = new TStandardDiagnosticReadout(*this,
                            "READOUT",
                            &FinalSortBxDataFactory::getInstance(),
                            GetDaqDiagCtrl(),
                            VECT_DAQ,
                            BITS_DAQ_EMPTY,
                            BITS_DAQ_EMPTY_ACK,
                            BITS_DAQ_LOST,
                            BITS_DAQ_LOST_ACK,
                            VECT_DAQ_WR,
                            BITS_DAQ_WR_ADDR,
                            BITS_DAQ_WR_ACK,
                            VECT_DAQ_RD,
                            BITS_DAQ_RD_ADDR,
                            BITS_DAQ_RD_ACK,
                            WORD_DAQ_MASK,
                            WORD_DAQ_DATA_DELAY,
                            AREA_MEM_DAQ_DIAG,
                            DAQ_DIAG_DATA_SIZE,
                            DAQ_DIAG_TRIG_NUM,
                            DAQ_DIAG_TIMER_SIZE,
                            TTC_BCN_EVT_WIDTH);
    }
    return *DiagnosticReadout;
}

TFsbSortFinal::TDiagCtrlVector& TFsbSortFinal::GetDiagCtrls() {
    if (DiagCtrls.empty()) {
        DiagCtrls.push_back(&GetDaqDiagCtrl());
        DiagCtrls.push_back(&GetPulserDiagCtrl());
        DiagCtrls.push_back(&getStatisticsDiagCtrl());
    }
    return DiagCtrls;
}

TFsbSortFinal::TDiagVector& TFsbSortFinal::GetDiags() {
    if (Diags.empty()) {
        Diags.push_back(&GetDiagnosticReadout());
        Diags.push_back(&GetPulser());

        getHistoManagers();
        for (unsigned int i = 0; i < histoManagers_.size(); i++) {
            Diags.push_back(histoManagers_[i]);
        }
    }
    return Diags;
}

void TFsbSortFinal::ConfigureDiagnosable(TTriggerDataSel triggerDataSel, uint32_t dataTrgDelay) {
    writeBits(BITS_STATUS_TRG_DATA_SEL, triggerDataSel);
    writeWord(WORD_DATA_TRG_DELAY, 0, dataTrgDelay);
}

HistoMgrVector& TFsbSortFinal::getHistoManagers() {
    if (histoManagers_.empty()) {
        histoManagers_.push_back(new TIIHistoMgr(*this, "FS_HIST_CODE_B0", getStatisticsDiagCtrl(), AREA_MEM_HIST_CODE_B0));
        histoManagers_.push_back(new TIIHistoMgr(*this, "FS_HIST_CODE_B1", getStatisticsDiagCtrl(), AREA_MEM_HIST_CODE_B1));
        histoManagers_.push_back(new TIIHistoMgr(*this, "FS_HIST_CODE_B2", getStatisticsDiagCtrl(), AREA_MEM_HIST_CODE_B2));
        histoManagers_.push_back(new TIIHistoMgr(*this, "FS_HIST_CODE_B3", getStatisticsDiagCtrl(), AREA_MEM_HIST_CODE_B3));
        histoManagers_.push_back(new TIIHistoMgr(*this, "FS_HIST_CODE_E0", getStatisticsDiagCtrl(), AREA_MEM_HIST_CODE_E0));
        histoManagers_.push_back(new TIIHistoMgr(*this, "FS_HIST_CODE_E1", getStatisticsDiagCtrl(), AREA_MEM_HIST_CODE_E1));
        histoManagers_.push_back(new TIIHistoMgr(*this, "FS_HIST_CODE_E2", getStatisticsDiagCtrl(), AREA_MEM_HIST_CODE_E2));
        histoManagers_.push_back(new TIIHistoMgr(*this, "FS_HIST_CODE_E3", getStatisticsDiagCtrl(), AREA_MEM_HIST_CODE_E3));
    }

    return histoManagers_;
}

TDiagCtrl& TFsbSortFinal::getStatisticsDiagCtrl() {
    if (statisticsDiagCtrl_ == 0) {
        statisticsDiagCtrl_ = new TDiagCtrl(*this,
                                           VECT_HIST,
                                           BITS_HIST_PROC_REQ,
                                           BITS_HIST_PROC_ACK,
                                           BITS_HIST_TIMER_LOC_ENA,
                                           0,
                                           BITS_HIST_TIMER_START,
                                           BITS_HIST_TIMER_STOP,
                                           BITS_HIST_TIMER_TRIG_SEL,
                                           WORD_HIST_TIMER_LIMIT,
                                           WORD_HIST_TIMER_COUNT,
                                           WORD_HIST_TIMER_TRG_DELAY,
                                           "STATISTICS_DIAG_CTRL");
    }
    return *statisticsDiagCtrl_;
}

void TFsbSortFinal::ResetQPLL() {
	qpll_.reset();
}

TIII2CMasterCore& TFsbSortFinal::GetI2C() {
    if (I2C == NULL) {

        I2C = new TIII2CMasterCore(*this, AREA_I2C);
        I2C->SetPrescaleValue(100);
        I2C->WriteControlReg(TI2CMasterCore::BIT_EN);
    }

    return *I2C;
}

void TFsbSortFinal::ResetTTC() {
	int resetTime = 1;
	LOG4CPLUS_INFO(logger, "TFsbSortHalf: ResetTTC started. resetTime = "<<resetTime<<" msec");
	LOG4CPLUS_INFO(logger, "TFsbSortHalf: ResetTTC. BITS_STATUS_TTC_RESET = 1");
    writeBits(BITS_STATUS_TTC_RESET, 1);
    tbsleep(resetTime);
    if (readBits(BITS_STATUS_TTC_READY) != 0) {
        throw TException("TFsbSortFinal::ResetTTC: BITS_TTC_READY did not go down after BITS_TTC_RESET, 1");
        //LOG4CPLUS_ERROR(logger, "TTCSortTCSort::ResetTTC: BITS_TTC_READY did not go down after BITS_TTC_RESET, 1");
    }
    writeBits(BITS_STATUS_TTC_RESET, 0);
    LOG4CPLUS_INFO(logger, "TFsbSortHalf: ResetTTC. BITS_STATUS_TTC_RESET = 0");

    int i = 0;
    for(; i < 5; i++) {
    	if (readBits(BITS_STATUS_TTC_READY) == 1) {
    		LOG4CPLUS_INFO(logger, "TFsbSortHalf: ResetTTC: BITS_TTC_READY went up after "<<i * 10<<" msec");
    		break;
    	}
    	tbsleep(10);

    }
    if(i == 5)
    	throw TException("TFsbSortFinal::ResetTTC: BITS_TTC_READY did not go up");

	LOG4CPLUS_INFO(logger, "TFsbSortHalf: ResetTTC finished");
}

void TFsbSortFinal::InitTTC() {
	LOG4CPLUS_INFO(logger, "TFsbSortHalf: InitTTC started ");
    writeWord(WORD_TTC_ID_MODE, 0, TTCrxI2C.GetAddress());
    writeBits(BITS_STATUS_TTC_INIT_ENA, 1);
    ResetTTC();
    writeBits(BITS_STATUS_TTC_INIT_ENA, 0);

    // Enable Clock40Des2 output
    LOG4CPLUS_WARN(logger, "TFsbSortFinal: Clock40Des2 not enabled ");
    //GetTTCrxI2C().WriteControlReg(GetTTCrxI2C().ReadControlReg() | 0x8);

    GetTTCrxI2C().WriteFineDelay1(0);
    GetTTCrxI2C().WriteFineDelay2(0);
    GetTTCrxI2C().WriteCoarseDelay(0, 0);
    LOG4CPLUS_INFO(logger, "TFsbSortHalf: InitTTC finished ");
}

bool TFsbSortFinal::checkResetNeeded() {
    return (readBits(BITS_STATUS_TTC_READY) == 0) || (readBits(BITS_QPLL_LOCKED) == 0);
}

void TFsbSortFinal::reset() {
	writeWord(WORD_BCN0_DELAY, 0, FixedHardwareSettings::FSB_BC0_DELAY);
	SorterChannelsEna(0xff, 0xff);
    writeBits(BITS_PULSER_OUT_ENA, uint32_t(0));
}

void TFsbSortFinal::configure(DeviceSettings* settings, int flags) {
	InitTTC(); //TODO ~!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!111
    reset();
}

void TFsbSortFinal::SorterChannelsEna(unsigned int barrel, unsigned int endcap) {
    writeWord(WORD_SORTB_CHAN_ENA, 0, barrel);
    writeWord(WORD_SORTE_CHAN_ENA, 0, endcap);
}

void TFsbSortFinal::monitor(volatile bool *stop) {
    monitor(monitorItems_);
}

void TFsbSortFinal::monitor(MonitorItems& items) {
    warningStatusList_.clear();
    for (MonitorItems::iterator iItem = items.begin(); iItem != items.end(); ++iItem) {
        if ((*iItem) == MonitorItemName::FIRMWARE) {
            string msg = monitorVersion();
            if (msg.size() != 0) {
                warningStatusList_.push_back(MonitorableStatus(MonitorItemName::FIRMWARE, MonitorableStatus::ERROR, msg, 0));
            }
        }
        else if ((*iItem) == MonitorItemName::QPLL) {
            if (readBits(BITS_QPLL_LOCKED) != 1) {
                if (readBits(BITS_QPLL_LOCKED) != 1) {
                    warningStatusList_.push_back(MonitorableStatus(MonitorItemName::QPLL, MonitorableStatus::ERROR, "BITS_QPLL_LOCKED is not up", 0));
                }
                else {
                    warningStatusList_.push_back(MonitorableStatus(MonitorItemName::QPLL, MonitorableStatus::WARNING, "BITS_QPLL_LOCKED is not up", 0));
                }
            }
        }
        else if ((*iItem) == MonitorItemName::RECEIVER) {
            if (readBits(BITS_STATUS_TTC_READY) != 1) {
                warningStatusList_.push_back(MonitorableStatus(MonitorItemName::TTCRX, MonitorableStatus::ERROR, "BITS_TTC_READY is not up", 0));
            }
        }
    }
}
//---------------------------------------------------------------------------

Logger TFsb::logger = Logger::getInstance("TFsb");
void TFsb::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}
const HardwareItemType TFsb::TYPE(HardwareItemType::cBoard, "FSB");


TFsb::TFsb(int ident, const char* desc, TVMEInterface* vme, int vme_board_address, VmeCrate* c, int pos, int fsbSortFinalId)
        : BoardBase(ident, desc, TYPE, c, pos), VME(vme), FsbVme(vme_board_address, *this),
        FsbSortFinal(fsbSortFinalId, vme_board_address, 0, "FSB_SORT_FINAL", *this), VMEAddress(vme_board_address)
{
    Init();
    FsbVme.getMemoryMap().SetHardwareIfc(
        new TIIVMEAccess(
            FsbVme.GetVMEBoardBaseAddr(),
            FsbVme.getBaseAddress() << FsbVme.GetVMEBaseAddrShift(),
            vme,
            0xf8000),
        true);
    FsbSortFinal.getMemoryMap().SetHardwareIfc(
        new TIIVMEAccess(
            FsbSortFinal.GetVMEBoardBaseAddr(),
            FsbSortFinal.getBaseAddress() << FsbSortFinal.GetVMEBaseAddrShift(),
            vme,
            0), //0xf8000),
        true);
}


TFsb::~TFsb()
{
}



void TFsb::Init()
{
    devices_.push_back(&FsbVme);
    devices_.push_back(&FsbSortFinal);
}

void TFsb::configure(ConfigurationSet* configurationSet, int configureFlags) {
    //FsbSortFinal.InitTTC();
    //FsbSortFinal.ResetQPLL();
    BoardBase::configure(configurationSet, configureFlags);
}


}
