#include "rpct/devices/LinkBoardFlashEncoder.h"
#include <stdexcept>
#include <algorithm>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>

//Ten program wyznacza zawarto�� flasha z korekcj�
//Poni�sza funkcja wyznacza sum� kontroln� zer
//w s�owie. Najstarsze 27 bit�w to dane
//Najm�odsze 5 bit�w - kontrola
//Program nale�y radykalnie przeorganizowa� - musi by� zrobiony obiektowo - obiekt
//filtr wyj�ciowy przyjmuje nowe s�owo, i kiedy trzeba generuje dane wyj�ciowe.
namespace rpct {

void LinkBoardFlashEncoder::initialize()
{
	std::fill(d1, d1+5, 0xff);
	std::fill(d2, d2+5, 0xff);
//	d1 = {0xff,0xff,0xff,0xff,0xff};
//	d2 = {0xff,0xff,0xff,0xff,0xff};
	dptr = 0;
	out.str(std::string(""));
	unencodedOut.str(std::string(""));
	hasByte = false;
	finish = false;
}

void LinkBoardFlashEncoder::reset()
{
	initialize();
}

uint32_t LinkBoardFlashEncoder::sumzer(uint32_t dt)
{
	uint32_t maska=0x1;
	int i;
	uint32_t suma=0x1;
	for(i=0;i<27;i++) {
		if(!(dt & maska)) suma++;
		maska <<= 1;
	}
	dt=(dt<<5) & 0xffffffe0;
	dt=dt | (suma);
	return dt;
}

void LinkBoardFlashEncoder::flush()
{
	//Mamy odczytane dane ze zbior�w wej�ciowych, teraz przekazujemy je na wyj�cie
	//Je�li danych wej�ciowych by�o za ma�o, dopychamy ff-ami (kt�re zosta�y
	//po inicjalizacji
	//Budujemy dane wyj�ciowe
	//S�owo przetwarzane przez nasz system to s�owo 80-bitowe (z jednym bitem nadmiarowym)
	//Sk�ada si� ono z trzech pods��w 27 bitowych, dope�nionych sum� kontroln�
	//Najpierw wpisujemy bity danych
	int nb=0,i;
	for(i=0;i<5;i++) {
		int j;
		unsigned char mask=0x01;
		for(j=0;j<8;j++) {
			lb[nb++]=(d1[i] & mask) ? 1 : 0;
			mask <<= 1;
		}
		mask=0x01;
		for(j=0;j<8;j++) {
			lb[nb++]=(d2[i] & mask) ? 1 : 0;
			mask <<= 1;
		}
	}
	if(nb!=80) {
		fprintf(stderr,"Powa�ny b��d programu!!! nb!=80 tylko %d\n",nb);
		exit(1);
	}
	lb[80]=1;
	//Teraz pakujemy bity do s��w wyj�ciowych
	nb=0;
	for(i=0;i<3;i++) {
		uint32_t mask=0x1;
		int j;
		l[i]=0l;
		for(j=0;j<27;j++) {
	 	      if(lb[nb++]) l[i] |= mask;
	 	      mask <<= 1;
		}
	}
	//Liczymy sum� kontroln�
	l[3]=l[0] ^ l[1] ^ l[2];
	//Teraz dodajemy sumy kontrolne zer
	for(i=0;i<4;i++) {
		l[i]=sumzer(l[i]);
	}
	//Wreszcie wyrzucamy wynik do pliku
	for(i=0;i<4;i++) {
		// stringstream nie umie normalnie radzi� sobie z zapisywaniem ci�g�w znak�w z d�ugiego inta
		out.put( (char) ((l[i] & 0xff)));
		out.put( (char) ((l[i] & 0xff00)>>8));
		out.put( (char) ((l[i] & 0xff0000)>>16));
		out.put( (char) ((l[i] & 0xff000000)>>24));
	}
}


void LinkBoardFlashEncoder::addwrd(unsigned short int d)
{
	unsigned int pos; // Pozycja zapisu w buforze
	pos=dptr/2;
	d1[pos] = (d & 0xff);
	d2[pos] = (d & 0xff00) >> 8;
	dptr+=2;
	if (dptr==10) { //bufor wype�niony
		flush();
		dptr=0;
	}
	unencodedOut.put(d & 0xffff);
	unencodedOut.put(d >> 8);
}

void LinkBoardFlashEncoder::putByte(unsigned char _byte)
{
	if (finish)
		throw std::logic_error("Encoding of this data was already finished.");

	if (hasByte)
	{
		addwrd(((unsigned short)byte <<8) | (unsigned short) _byte);
		hasByte = false;
	} else {
		byte = _byte;
		hasByte = true;
	}
}

void LinkBoardFlashEncoder::putWord(unsigned short int w)
{
	if (finish)
		throw std::logic_error("Encoding of this data was already finished.");

	if (hasByte)
	{
		putByte(w & 0xff);
		putByte((w & 0xff00) >> 8);
	} else
		addwrd(w);
}

std::string LinkBoardFlashEncoder::end()
{
	if (!finish)
	{
		if (hasByte)
			putByte(0xff);
		putWord(0xffff);
		putWord(0xffff);
		flush();
		finish = true;
	}
	return out.str();
}

std::string LinkBoardFlashEncoder::getUnencoded()
{
	return unencodedOut.str();
}

LinkBoardFlashEncoder::LinkBoardFlashEncoder()
{
	initialize();
}

} /* namespace rpct */
