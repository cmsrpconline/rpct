//---------------------------------------------------------------------------

#ifndef TGolJtagH
#define TGolJtagH
//---------------------------------------------------------------------------

#include "gol.h"
#include "tb_bs.h"
#include <bitset>

namespace rpct {

class TGolJtag : public TGolInterface {
private:
  bs::TBoard& Board;
  bs::TBSContr& Contr;
  bs::TChip& GOL;
  bs::TFragmentedRegister& ConfReg;
  typedef std::bitset<8> TBitSet;
  unsigned char ScanVectorToVal(bs::TScanVector seq)
    {
      TBitSet bitSet(seq);
      return bitSet.to_ulong();
    }
  bs::TScanVector ValToScanVector(unsigned char value)
    {
      TBitSet bitSet(value);
      return bitSet.to_string<char, std::char_traits<char>, std::allocator<char> >();
      //return bitSet.to_string();
    }
  unsigned char WriteReg(std::string name, unsigned char value);
  unsigned char ReadReg(std::string name);
public:
  TGolJtag(bs::TBoard& board,
           bs::TBSContr& contr,
           bs::TChip& gol)
    : Board(board), Contr(contr), GOL(gol),
      ConfReg(dynamic_cast<bs::TFragmentedRegister&>((gol)["CONF"])) {}

  void WriteConfig0(unsigned char wait_time, unsigned char loss_of_lock_time);

  void ReadConfig0(unsigned char& wait_time, unsigned char& loss_of_lock_time);

  void WriteConfig1(unsigned char pll_lock_time,
                    bool en_soft_loss_of_lock,
                    bool en_loss_of_lock_count,
                    bool en_force_lock,
                    bool en_self_test);

  void ReadConfig1(unsigned char& pll_lock_time,
                   bool& en_soft_loss_of_lock,
                   bool& en_loss_of_lock_count,
                   bool& en_force_lock,
                   bool& en_self_test);

  void WriteConfig2(unsigned char pll_current, bool en_flag);

  void ReadConfig2(unsigned char& pll_current, bool& en_flag);

  void WriteConfig3(unsigned char ld_current, bool use_conf_regs);

  void ReadConfig3(unsigned char& ld_current, bool& use_conf_regs);
  void ReadStatus0(unsigned char& loss_of_lock_count);
  void ReadStatus1(bool& conf_wmode16,
                   bool& conf_glink,
                   TLinkControlState& link_control_state_C,
                   TLinkControlState& link_control_state_B,
                   TLinkControlState& link_control_state_A);
};

}

#endif
