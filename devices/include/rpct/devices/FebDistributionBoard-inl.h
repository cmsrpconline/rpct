/** Filip Thyssen */

#ifndef rpct_devices_FebDistributionBoard_inl_h_
#define rpct_devices_FebDistributionBoard_inl_h_

#include "rpct/devices/FebDistributionBoard.h"

namespace rpct {

inline int FebDistributionBoard::getId() const
{
    return id_;
}
inline const std::string & FebDistributionBoard::getDescription() const
{
    return description_;
}
inline const HardwareItemType & FebDistributionBoard::getType() const
{
    return *type_;
}

inline const IMonitorable::MonitorItems & FebDistributionBoard::getMonitorItems()
{
    return *mitems_;
}
inline void FebDistributionBoard::configure()
{
    configure(0x0);
}
inline void FebDistributionBoard::monitor(volatile bool *stop)
{
    monitor(*mitems_, 0x0);
}
inline void FebDistributionBoard::monitor(volatile bool *stop, int configureFlags)
{
    monitor(*mitems_, configureFlags);
}

inline int FebDistributionBoard::makePosition(unsigned short channel
                                              , unsigned char pca9544_address_offset
                                              , unsigned char dt_channel)
{
    return (int)(
                 (((unsigned int)channel << 16) & 0xffff0000) |
                 ((pca9544_address_offset << 8) & 0x0000ff00) |
                 (dt_channel                    & 0x000000ff)
                 );
}

inline rpct::i2c::II2cAccess & FebDistributionBoard::getI2cAccess()
{
    return i2c_access_;
}
inline IFebAccessPoint & FebDistributionBoard::getFebAccessPoint() const
{
    return febaccesspoint_;
}
inline unsigned short FebDistributionBoard::getChannel() const
{
    return channel_;
}
inline unsigned char FebDistributionBoard::getDTChannel() const
{
    return dt_channel_;
}
inline bool FebDistributionBoard::isDT() const
{
    return dt_;
}

} // namespace rpct

#endif // rpct_devices_FebDistributionBoard_inl_h_
