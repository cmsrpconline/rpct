#ifndef RPCTTHALFSORTDATA_H_
#define RPCTTHALFSORTDATA_H_

#include "rpct/diag/IDiagnosticReadout.h"
#include "rpct/devices/StandardBxData.h"
#include "rpct/devices/cmssw/RPCTBMuon.h"

namespace rpct {
    

class FinalSortBxData : public StandardBxData { 
public:
    FinalSortBxData(IDiagnosticReadout::BxData& bxData);
    
    virtual IDiagnosticReadout::BxData& getBxData() {
        return bxData_;
    }
    
    virtual std::string toString() {
    	return dataStr_;
    }; 
};

class FinalSortBxDataFactory : virtual public IDiagnosticReadout::ConvertedBxDataFactory {
private:
    static FinalSortBxDataFactory instance_;
    FinalSortBxDataFactory() {
    }
    
public:    
    virtual IDiagnosticReadout::ConvertedBxData* create(IDiagnosticReadout::BxData& bxData, IDiagnosticReadout& ownerReadout) {
        return new FinalSortBxData(bxData);
    }       
    
    static FinalSortBxDataFactory& getInstance() {
        return instance_;
    }
    
};

}

#endif 
