#ifndef i2cH
#define i2cH

#include "tb_ii.h"

namespace rpct {

const TN AREA_I2C_ADDR_SIZE =  2;
const TN AREA_I2C_ADDR_ITEM =  4;
const TN AREA_I2C_DATA_SIZE = 16;
const TN AREA_I2C_SEL_SIZE  =  2;

}

#endif
