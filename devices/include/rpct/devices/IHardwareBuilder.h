#ifndef IHardwareBuilderH
#define IHardwareBuilderH

#include <xercesc/sax/HandlerBase.hpp>   
#include <xercesc/sax/AttributeList.hpp>
#include "System.h"

namespace rpct {

class IHardwareBuilder  {
public:
  //virtual ~ICrateBuilder() {}
  //virtual void startElement(const XMLCh* const name, xercesc::AttributeList& attributes) = 0;
  //virtual void endElement(const XMLCh* const name) = 0;
    
  virtual void startElement(const XMLCh* const name, xercesc::AttributeList& attributes, System& system) = 0;
};

}

#endif
