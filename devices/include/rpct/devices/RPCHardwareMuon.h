#ifndef RPCHardwareMuon_h
#define RPCHardwareMuon_h
#include "rpct/devices/cmssw/RPCGBSMuon.h"

class RPCHardwareMuon: public RPCGBSMuon {
public:
  enum MuonType {
    mtPacOut,
    mtTBSortOut,
    mtTCSortOut,
    mtHalfSortOut,
    mtFinalSortOut
  };
protected:
	MuonType type_;
	
	int index_; //position in the diagnostic readout, separatly counted for input and output	
public:
	RPCHardwareMuon(int ptCode, int quality, int sign, MuonType type);
	
	RPCHardwareMuon(MuonType type, int index): type_(type), index_(index) {
	}

	virtual ~RPCHardwareMuon() {
	}

	MuonType getType() {
		return type_;	
	}
	
	int getIndex() {
		return index_;
	}	
	
	virtual unsigned int toBits() = 0;
	
	static RPCHardwareMuon* createMuon(int ptCode, int quality, int sign, MuonType type);
};

class RPCPacOutMuon : public RPCHardwareMuon {
private:
   	static const int m_qualBitsCnt = 3;  static const unsigned int m_qualBitsMask = 0x7;
  	static const int m_ptBitsCnt   = 5;  static const unsigned int m_ptBitsMask   = 0x1f;
  	static const int m_signBitsCnt = 1;  static const unsigned int m_signBitsMask = 0x1;	
public:
	RPCPacOutMuon(int ptCode, int quality, int sign);
	
	RPCPacOutMuon(unsigned int bits, unsigned int index);

	virtual ~RPCPacOutMuon() {
	}

  	virtual unsigned int toBits();
  	
  	static const int getMuonBitsCnt() {
  		return m_qualBitsCnt + m_ptBitsCnt + m_signBitsCnt;
  	}
};

class RPCTbGbSortOutMuon: public RPCHardwareMuon {
private:  	
  	static const int m_qualBitsCnt = 3;  static const unsigned int m_qualBitsMask = 0x7;  
  	static const int m_ptBitsCnt   = 5;  static const unsigned int m_ptBitsMask   = 0x1f;	
  	static const int m_signBitsCnt = 1;  static const unsigned int m_signBitsMask = 0x1;
  	static const int m_phiBitsCnt  = 4;  static const unsigned int m_phiBitsMask  = 0xf;
  	static const int m_etaBitsCnt  = 2;  static const unsigned int m_etaBitsMask  = 0x3;
  	static const int m_gbDataBitsCnt=2;  static const unsigned int m_gbDataBitsMask = 0x3;
  	
  public:	  
  	RPCTbGbSortOutMuon(int ptCode, int quality, int sign);
  	
  	RPCTbGbSortOutMuon(unsigned int bits, unsigned int index);
  
  	virtual ~RPCTbGbSortOutMuon(){
  	}

  	virtual unsigned int toBits();  
  	
  	static const int getMuonBitsCnt() {
  		return m_qualBitsCnt + m_ptBitsCnt + m_signBitsCnt + m_phiBitsCnt + m_etaBitsCnt + m_gbDataBitsCnt;
  	}
};

class RPCTcSortOutMuon: public RPCHardwareMuon {
private:
  	static const int m_gbDataBitsCnt=2;  static const unsigned int m_gbDataBitsMask = 0x3;
  	static const int m_etaBitsCnt  = 6;  static const unsigned int m_etaBitsMask  = 0x3f;
  	static const int m_phiBitsCnt  = 4;  static const unsigned int m_phiBitsMask  = 0xf;
  	static const int m_qualBitsCnt = 3;  static const unsigned int m_qualBitsMask = 0x7;
  	static const int m_ptBitsCnt   = 5;  static const unsigned int m_ptBitsMask   = 0x1f;
  	static const int m_signBitsCnt = 1;  static const unsigned int m_signBitsMask = 0x1;
public:
	RPCTcSortOutMuon(int ptCode, int quality, int sign);
	
	RPCTcSortOutMuon(unsigned int bits, unsigned int index);
	
	virtual ~RPCTcSortOutMuon() {
	}

  	virtual unsigned int toBits();
  	
  	static const int getMuonBitsCnt() {
  		return m_qualBitsCnt + m_ptBitsCnt + m_signBitsCnt + m_phiBitsCnt + m_etaBitsCnt + m_gbDataBitsCnt;
  	}
};

class RPCHalfSortOutMuon: public RPCHardwareMuon {
private:
  	static const int m_signBitsCnt = 1;  static const unsigned int m_signBitsMask = 0x1;
  	static const int m_ptBitsCnt   = 5;  static const unsigned int m_ptBitsMask   = 0x1f;
  	static const int m_qualBitsCnt = 3;  static const unsigned int m_qualBitsMask = 0x7;  	
  	static const int m_phiBitsCnt  = 7;  static const unsigned int m_phiBitsMask  = 0x7f;  	  	
  	static const int m_etaBitsCnt  = 6;  static const unsigned int m_etaBitsMask  = 0x3f;
public:	  
	RPCHalfSortOutMuon(int ptCode, int quality, int sign);
	
	RPCHalfSortOutMuon(unsigned int bits, unsigned int index);
		
	virtual ~RPCHalfSortOutMuon() {
	}

  	virtual unsigned int toBits();
  	  	
  	static const int getMuonBitsCnt() {
  		return m_qualBitsCnt + m_ptBitsCnt + m_signBitsCnt + m_phiBitsCnt + m_etaBitsCnt;
  	}
};

class RPCFinalSortOutMuon: public RPCHardwareMuon {
private:
	unsigned int bc0_;
	unsigned int bcn_;
	unsigned int signValid_;
	unsigned int par_;
private:
  	static const int m_phiBitsCnt  = 8;  static const unsigned int m_phiBitsMask  = 0xff;
  	static const int m_ptBitsCnt   = 5;  static const unsigned int m_ptBitsMask   = 0x1f;
  	static const int m_qualBitsCnt = 3;  static const unsigned int m_qualBitsMask = 0x7;
  	static const int m_etaBitsCnt  = 6;  static const unsigned int m_etaBitsMask  = 0x3f;  	
  	static const int m_hvBitsCnt = 1;    static const unsigned int m_hvBitsMask = 0x1; //H/F - CSC/DT bit  	 
  	static const int m_signBitsCnt = 1;  static const unsigned int m_signBitsMask = 0x1;  	
  	static const int m_signValidBitsCnt = 1;static const unsigned int m_signValidBitsMask = 0x1;  	
  	static const int m_bcnBitsCnt = 3;   static const unsigned int m_bcnBitsMask  = 0x7;
  	static const int m_bc0BitsCnt = 1;   static const unsigned int m_bc0BitsMask  = 0x1;  	
  	static const int m_synchErrBitsCnt = 1;  static const unsigned int m_synchErrBitsMask = 0x1; 
  	static const int m_parityBitsCnt = 1; static const unsigned int m_parityBitsMask = 0x1; 
public:	
	RPCFinalSortOutMuon(int ptCode, int quality, int sign);
	
	RPCFinalSortOutMuon(unsigned int bits, unsigned int index);
	
	virtual ~RPCFinalSortOutMuon() {
	}

  	virtual unsigned int toBits();
  	
  	static const int getPulserMuonBitsCnt() {
  		return m_qualBitsCnt + m_ptBitsCnt + m_signBitsCnt + m_phiBitsCnt + m_etaBitsCnt;
  	}
  	
  	static const int getDaqMuonBitsCnt() {
  		return m_qualBitsCnt + m_ptBitsCnt + m_signBitsCnt + m_phiBitsCnt + m_etaBitsCnt + m_bc0BitsCnt + m_bcnBitsCnt + m_signValidBitsCnt + m_synchErrBitsCnt + m_parityBitsCnt + m_hvBitsCnt;
  	}
  	
  	std::string toString(int format) const;
};

#endif

