//---------------------------------------------------------------------------
//
//  Michal Pietrusinski 2001
//  Warsaw University
//
#ifndef fsbH
#define fsbH
//---------------------------------------------------------------------------

#include "tb_ii.h"
#include "i2c.h"
#include "TIIABootContr.h"
#include "TIIJtagIO.h"
#include "TTTCrxI2C.h"
#include "rpct/diag/TDiagCtrl.h"
#include "rpct/diag/TStandardDiagnosticReadout.h"
#include "rpct/devices/VmeCrate.h"
#include "rpct/i2c/TIII2CMasterCore.h"
#include "rpct/ii/BoardBase.h"
#include "rpct/ii/memmap.h"
#include "rpct/std/ENotImplemented.h"
#include "rpct/diag/TPulser.h"
#include "rpct/diag/TIIHistoMgr.h"
#include "rpct/ii/Monitorable.h"
#include "rpct/devices/QPLL.h"

#define CII_FS

namespace rpct {

class TFsbDevice: public TIIDevice, public IIVMEDevice {
protected:
    int VMEBoardBaseAddr;
    static log4cplus::Logger logger;
public:
  static void setupLogger(std::string namePrefix);
  #define IID_FILE "RPC_system_def.iid"
  #include "iid2cdef1.h"

  #define IID_FILE "RPC_TB3_def.iid"
  #include "iid2cdef1.h"

  #define IID_FILE "RPC_TC_SORT_def.iid"
  #include "iid2cdef1.h"

  #define IID_FILE "RPC_SORT_def.iid"
  #include "iid2cdef1.h"

    TFsbDevice(int id,
               std::string name,
               TID name_id,
               std::string desc,
               const HardwareItemType& type,
               std::string ver,
               TID ver_id,
               TID checksum_id,
               uint32_t base_address,
               IBoard& board,
               int position,
               TMemoryMap* memmap,
               int vme_board_address)
            : TIIDevice(id, name, name_id, desc, type, ver, ver_id, checksum_id, base_address,
            board, position, memmap),
    VMEBoardBaseAddr(vme_board_address) {}

    virtual ~TFsbDevice() {}

    virtual int GetVMEBaseAddrShift()
    { return 16; }

    virtual int GetVMEBoardBaseAddr()
    { return VMEBoardBaseAddr; }
};




class TFsbVme : public TFsbDevice {
private:
    TIIABootContr* ABootContr;
    TIIJtagIO* JtagIO;
public:

  #define IID_FILE "RPC_TB3_vme.iid"
  #include "iid2c1.h"

    const static int II_ADDR_SIZE = 20;
    const static int II_DATA_SIZE = 32;
    const static HardwareItemType TYPE;

    TFsbVme(int vme_board_address, IBoard& board);
    virtual ~TFsbVme();


    TIIABootContr& GetABootContr() {
        if (ABootContr == NULL)
            ABootContr = new TIIABootContr(*this,
                                           VECT_ALTERA,
                                           BITS_ALTERA_NCONF,
                                           BITS_ALTERA_NSTATUS,
                                           BITS_ALTERA_CONFDONE,
                                           BITS_ALTERA_INITDONE,
                                           WORD_RESET,
                                           AREA_ALTERA_BOOT);
        return *ABootContr;
    }

    TIIJtagIO& GetJTagIO() {
        if (JtagIO == NULL)
            JtagIO = new TIIJtagIO(*this,
                                   VECT_JTAG,
                                   BITS_JTAG_TDI,
                                   BITS_JTAG_TDO,
                                   BITS_JTAG_TCK,
                                   BITS_JTAG_TMS);
        return *JtagIO;
    }

    void SetJtagEnable(bool value);

    virtual void reset() {
    }

    virtual void configure(DeviceSettings* settings, int flags) {
        reset();
    }
};



class TFsbSortFinal : public TFsbDevice, virtual public IDiagnosable, virtual public Monitorable {
private:
	class FinalSortPulser : public TPulser {
	public:
		FinalSortPulser(IDiagnosable& owner, const char* name, IDiagCtrl& diagCtrl,
            TID areaMemPulse, TID wordPulserLength, TID bitsRepeatEna, TID bitsPulserOutEna):
            TPulser(owner, name, diagCtrl, areaMemPulse, wordPulserLength, bitsRepeatEna, bitsPulserOutEna)
            {};

        virtual void writeTarget(int target) {
        	if (target > 1) {
            	throw TException("TPulser::WriteTarget: target > 1");
        	}
        	GetOwner().writeBits(TFsbSortFinal::BITS_PULSER_OUT_IN_SEL, target);
    	}
	};

private:
    TDiagCtrl* DaqDiagCtrl;
    TDiagCtrl* PulserDiagCtrl;
    TStandardDiagnosticReadout* DiagnosticReadout;
    TPulser* Pulser;
    TDiagCtrlVector DiagCtrls;
    TDiagVector Diags;

    HistoMgrVector histoManagers_;
    TDiagCtrl* statisticsDiagCtrl_;

    TIII2CMasterCore* I2C;
    TTTCrxI2C TTCrxI2C;

    QPLL qpll_;

    //static const char* MON_REC_ERR;
public:

#ifdef CII_FS
    #include "CII_SC_FSORT_iicfg_tab.h"
#else
    #define IID_FILE "RPC_SORT_final.iid"
    #include "iid2c1.h"
    const static int II_ADDR_SIZE;
    const static int II_DATA_SIZE;
#endif

    const static HardwareItemType TYPE;


    TFsbSortFinal(int id, int vme_board_address, int ii_addr, const char* desc, IBoard& board);
    virtual ~TFsbSortFinal();

	TStandardDiagnosticReadout& GetDiagnosticReadout();
    TDiagCtrl& GetDaqDiagCtrl();

    TPulser& GetPulser();
    TDiagCtrl& GetPulserDiagCtrl();

    HistoMgrVector& getHistoManagers();
    TDiagCtrl& getStatisticsDiagCtrl();

    virtual TDiagCtrlVector& GetDiagCtrls();
    virtual TDiagVector& GetDiags();
    virtual void ConfigureDiagnosable(TTriggerDataSel triggerDataSel, uint32_t dataTrgDelay);

    void ResetQPLL();
    void ResetTTC();
    void InitTTC();

    TTTCrxI2C& GetTTCrxI2C()
    {
        if (TTCrxI2C.GetI2CInterface() == 0) {
            TTCrxI2C.SetI2CInterface(GetI2C());
        }
        return TTCrxI2C;
    }

    TIII2CMasterCore& GetI2C();

    bool checkResetNeeded();

    virtual void reset();

    virtual void configure(DeviceSettings* settings, int flags);

    void SorterChannelsEna(unsigned int barrel, unsigned int endcap);

    virtual void monitor(volatile bool *stop);
    virtual void monitor(MonitorItems& items);
};



class TFsb: public BoardBase, virtual public IBoard {
private:
    static log4cplus::Logger logger;
    TVMEInterface* VME;
    TFsbVme FsbVme;
    TFsbSortFinal FsbSortFinal;

    int VMEAddress;

    void Init();

public:
    static void setupLogger(std::string namePrefix);
    const static HardwareItemType TYPE;
    TFsb(int ident, const char* desc, TVMEInterface* vme, int vme_board_address, VmeCrate* crate, int pos, int fsbSortFinalId);
    virtual ~TFsb();

    TFsbVme& GetFsbVme() {
        return FsbVme;
    }

    TFsbSortFinal& GetFsbSortFinal()
    { return FsbSortFinal; }

    int GetVMEAddress()
    { return VMEAddress; }

    virtual IDevice& addChip(std::string type, int pos, int id) {
        throw ENotImplemented("TFsb::addChip: not implemented");
    }

    TIIJtagIO& GetJTagIO() {
        return FsbVme.GetJTagIO();
    }

    virtual void configure(ConfigurationSet* configurationSet, int configureFlags);
    virtual void selfTest() {
    }
};

}


#endif
