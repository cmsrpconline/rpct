/** Filip Thyssen */

#ifndef rpct_devices_feb_const_h_
#define rpct_devices_feb_const_h_

#include <stdint.h>

namespace rpct {
namespace devices {
namespace feb {
namespace preset {

extern const unsigned int monitorcycle_;

// a limit for vth at about 1000mV
extern const uint16_t vth_max_;

extern const float dac_unit_;
extern const float inv_dac_unit_;
// vth, vmon
extern const float adc_unit_[2];
extern const float inv_adc_unit_[2];

// single feb, double feb part 0, double feb part 1
extern const unsigned char sel_dac_[3];
extern const unsigned char en_dac_ [3];

extern const unsigned char ad7417_[3]; // + address_
extern const unsigned char pcf8574a_; // + address_
extern const unsigned char pca9544_;
extern const unsigned char pca9544_dt_;
extern const unsigned char ad5316_;

extern const unsigned char adc_temp_;
// part 0 chip 0, part 0 chip 1, part 1 chip 0, part 1 chip 1
extern const unsigned char adc_vth_ [4];
extern const unsigned char adc_vmon_[4];
extern const unsigned char dac_vth_ [4];
extern const unsigned char dac_vmon_[4];
extern const unsigned char dac_vth_combined_; // = dac_vth_[0] + dac_vth_[1]
extern const unsigned char dac_vmon_combined_; // = dac_vmon_[0] + dac_vmon_[1]

} // namespace preset
} // namespace feb
} // namespace devices
} // namespace rpct

#endif // rpct_devices_feb_const_h_
