#ifndef RPCTOPTOSETTINGS_H_
#define RPCTOPTOSETTINGS_H_

#include "rpct/ii/DeviceSettings.h"

namespace rpct {
    

class OptoSettings : virtual public DeviceSettings {
public:
    virtual ~OptoSettings() {
    }
    
    virtual bool isEnableLink0() = 0;  
    virtual bool isEnableLink1() = 0;  
    virtual bool isEnableLink2() = 0;

    //TODO: andres
    //add here the getters to DB configurations
    //virtual unsigned int getTlkEnable()          = 0;
    //virtual unsigned int getTlkRefn()            = 0;
    //virtual unsigned int getRecSynchRequest()    = 0;
    //virtual unsigned int getRecCheckEnable()     = 0;
    //virtual unsigned int getRecCheckDataEnable() = 0;
    //virtual unsigned int getBCN0Delay()          = 0;


};

}

#endif 
