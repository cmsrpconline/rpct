/*
 * QPLL.h
 *
 *  Created on: Nov 16, 2010
 *      Author: Karol Bunkowski
 */

#ifndef QPLL_H_
#define QPLL_H_

#include "rpct/ii/IIDevice.h"
#include <log4cplus/logger.h>

namespace rpct {

class QPLL {
public:
	typedef IDevice::TID TID;
private:
	TIIDevice& owner_;

	log4cplus::Logger& logger_;

	TID VECT_QPLL;
	TID BITS_QPLL_MODE;
	TID BITS_QPLL_CTRL;
	TID BITS_QPLL_SELECT;
	TID BITS_QPLL_LOCKED;
	TID BITS_QPLL_ERROR;
	TID BITS_QPLL_CLK_LOCK;


public:
	QPLL(TIIDevice& owner, log4cplus::Logger& logger,
			TID vectQpll,
			TID bitsQpllMode,
			TID bitsQpllCtrl,
			TID bitsQpllSelect,
			TID bitsQpllLocked,
			TID bitsQpllError,
			TID bitsQpllClkLock) :
				owner_(owner), logger_(logger),
				VECT_QPLL(vectQpll),
				BITS_QPLL_MODE(bitsQpllMode),
				BITS_QPLL_CTRL(bitsQpllCtrl),
				BITS_QPLL_SELECT(bitsQpllSelect),
				BITS_QPLL_LOCKED(bitsQpllLocked),
				BITS_QPLL_ERROR(bitsQpllError),
				BITS_QPLL_CLK_LOCK(bitsQpllClkLock)
				{};

	virtual ~QPLL() {};

	void setup();

	bool reset();
};

}
#endif /* QPLL_H_ */
