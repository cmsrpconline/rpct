#ifndef RPCTOPTOSETTINGSIMPL_H_
#define RPCTOPTOSETTINGSIMPL_H_

#include "rpct/devices/OptoSettings.h"
#include "rpct/devices/TriggerBoard.h"

namespace rpct {
    

class OptoSettingsImpl : virtual public OptoSettings {
private:
    bool enabledLink0_;
    bool enabledLink1_;
    bool enabledLink2_;
public:
    OptoSettingsImpl(bool enabledLink0, bool enabledLink1, bool enabledLink2)
    : enabledLink0_(enabledLink0), enabledLink1_(enabledLink1), enabledLink2_(enabledLink2) {
    }
    
    virtual bool isEnableLink0() {
        return enabledLink0_;
    }
    virtual bool isEnableLink1() {
        return enabledLink1_;
    }  
    virtual bool isEnableLink2() {
        return enabledLink2_;
    } 
    virtual HardwareItemType getDeviceType() {
        return Opto::TYPE;
    }
};

}

#endif 
