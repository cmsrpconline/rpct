//---------------------------------------------------------------------------

//
//  Michal Pietrusinski 2001
//  Warsaw University
//
#ifndef tcsortH
#define tcsortH
//---------------------------------------------------------------------------

#include "tb_ii.h"
#include "i2c.h"
#include "TIIABootContr.h"
#include "rpct/devices/VmeCrate.h"
#include "rpct/diag/TDiagCtrl.h"
#include "rpct/diag/TStandardDiagnosticReadout.h"
#include "rpct/diag/TPulser.h"
#include "rpct/ii/BoardBase.h"
#include "rpct/ii/memmap.h"
#include "rpct/ii/Monitorable.h"
#include "rpct/std/ENotImplemented.h"
#include "rpct/devices/ErrorCounterAnalyzer.h"
#include "TTTCrxI2C.h"
#include "rpct/i2c/TIII2CMasterCore.h"
#include <boost/dynamic_bitset.hpp>
#include <log4cplus/logger.h>
#include "rpct/devices/QPLL.h"

#define CII_TC

namespace rpct {

class TTCSortDevice: public TIIDevice, public IIVMEDevice {
protected:
    int VMEBoardBaseAddr;
    static log4cplus::Logger logger;
public:
    static void setupLogger(std::string namePrefix);
#define IID_FILE "RPC_system_def.iid"
#include "iid2cdef1.h"

#define IID_FILE "RPC_TB3_def.iid"
#include "iid2cdef1.h"

#define IID_FILE "RPC_TC_SORT_def.iid"
#include "iid2cdef1.h"

    TTCSortDevice(int id, std::string name, TID name_id, std::string desc, const HardwareItemType& type,
            std::string ver, TID ver_id, TID checksum_id, uint32_t base_address, IBoard& board, int position,
            TMemoryMap* memmap, int vme_board_address) :
        TIIDevice(id, name, name_id, desc, type, ver, ver_id, checksum_id, base_address, board, position, memmap),
                VMEBoardBaseAddr(vme_board_address) {
    }

    virtual ~TTCSortDevice() {
    }

    virtual int GetVMEBaseAddrShift() {
        return 16;
    }

    virtual int GetVMEBoardBaseAddr() {
        return VMEBoardBaseAddr;
    }
};

class TTCSortVme: public TTCSortDevice {
private:
    TIIABootContr* ABootContr;
public:

#define IID_FILE "RPC_TB3_vme.iid"
#include "iid2c1.h"

    const static int II_ADDR_SIZE = 20;
    const static int II_DATA_SIZE = 32;

    const static HardwareItemType TYPE;

    TTCSortVme(int vme_board_address, IBoard& board);
    virtual ~TTCSortVme();

    TIIABootContr& GetABootContr() {
        if (ABootContr == NULL)
            ABootContr
                    = new TIIABootContr(*this, VECT_ALTERA, BITS_ALTERA_NCONF, BITS_ALTERA_NSTATUS, BITS_ALTERA_CONFDONE, BITS_ALTERA_INITDONE, WORD_RESET, AREA_ALTERA_BOOT);
        return *ABootContr;
    }

    virtual void reset() {
    }

    virtual void configure(DeviceSettings* settings, int flags) {
        reset();
    }
};

/*class TTCSortTCSort : public TTCSortDevice {
 private:
 public:

 #define IID_FILE "RPC_TC_SORT.iid"
 #include "iid2c1.h"

 const static int II_ADDR_SIZE;
 const static int II_DATA_SIZE;


 TTCSortTCSort(int id, int vme_board_address, int ii_addr, const char* desc, IBoard& board);
 virtual ~TTCSortTCSort();
 };*/

class TTCSortTCSort: public TTCSortDevice, virtual public IDiagnosable, virtual public Monitorable {
private:
    class TcGbSortPulser: public TPulser {
    public:
        TcGbSortPulser(IDiagnosable& owner, const char* name, IDiagCtrl& diagCtrl, TID areaMemPulse,
                TID wordPulserLength, TID bitsRepeatEna, TID bitsPulserOutEna) :
            TPulser(owner, name, diagCtrl, areaMemPulse, wordPulserLength, bitsRepeatEna, bitsPulserOutEna) {
        }
        ;

        virtual void writeTarget(int target) {
            if (target > 1) {
                throw TException("TPulser::WriteTarget: target > 1");
            }
            GetOwner().writeBits(TTCSortTCSort::BITS_PULSER_OUT_IN_SEL, target);
        }
    };

private:
    TDiagCtrl* DiagCtrl;
    TStandardDiagnosticReadout* DiagnosticReadout;

    TDiagCtrl* PulserDiagCtrl;
    TPulser* Pulser;

    TDiagCtrlVector DiagCtrls;
    TDiagVector Diags;

    TIII2CMasterCore* I2C;
    TTTCrxI2C TTCrxI2C;

    QPLL qpll_;

    boost::dynamic_bitset<> UsedTBs;
    ErrorCounterAnalyzer monRecErrorAnalyzer_;
public:

#ifdef CII_TC
#include "CII_TC_SORT_iicfg_tab.h"
#else
#define IID_FILE "RPC_TC_SORT.iid"
#include "iid2c1.h"
    const static int II_ADDR_SIZE;
    const static int II_DATA_SIZE;
#endif

    const static HardwareItemType TYPE;

    TTCSortTCSort(int id, int vme_board_address, int ii_addr, const char* desc, IBoard& board);
    virtual ~TTCSortTCSort();

    void ResetQPLL();
    void ResetTTC();
    void InitTTC();

    TDiagCtrl& GetDiagCtrl();
    TStandardDiagnosticReadout& GetDiagnosticReadout();

    TDiagCtrl& GetPulserDiagCtrl();
    TPulser& GetPulser();

    virtual TDiagCtrlVector& GetDiagCtrls();
    virtual TDiagVector& GetDiags();
    virtual void ConfigureDiagnosable(TTriggerDataSel triggerDataSel, uint32_t dataTrgDelay);

    boost::dynamic_bitset<>& getUsedTBs();

    void EnableTransmissionCheck(bool enableBCNcheck, bool enableDataCheck);

    //powinno byc robione automatycznie podczas inicjalizacji
    void SetUsedTBs(const boost::dynamic_bitset<>& usedTBs) {
        UsedTBs = usedTBs;
    }

    virtual void reset();

    virtual bool checkResetNeeded(); //if reset of the TTCrx and QPLL is needed, not above reset()
    virtual bool checkWarm(DeviceSettings* s);
    virtual void configure(DeviceSettings* settings, int flags);
    virtual void enable();

    TTTCrxI2C& GetTTCrxI2C() {
        if (TTCrxI2C.GetI2CInterface() == 0) {
            TTCrxI2C.SetI2CInterface(GetI2C());
        }
        return TTCrxI2C;
    }

    TIII2CMasterCore& GetI2C();

    virtual void monitor(volatile bool *stop);
    virtual void monitor(MonitorItems& items);
};

class TTCSort: public BoardBase, virtual public IBoard {
private:
    TVMEInterface* VME;
    TTCSortVme TCSortVme;
    TTCSortTCSort TCSortTCSort;

    int VMEAddress;
    //VmeCrate* crate;


public:
    const static HardwareItemType TYPE;
    TTCSort(int ident, const char* desc, TVMEInterface* vme, int vme_board_address, VmeCrate* crate, int pos,
            int tcSortChipId);
    virtual ~TTCSort();

    TTCSortVme& GetTCSortVme() {
        return TCSortVme;
    }

    TTCSortTCSort& GetTCSortTCSort() {
        return TCSortTCSort;
    }

    //void AddChip(const char* type, int pos, int id, const char* desc);

    int GetVMEAddress() {
        return VMEAddress;
    }

    virtual IDevice& addChip(std::string type, int pos, int id) {
        throw ENotImplemented("TTCSort::addChip: not implemented");
    }

    void init();

    virtual void selfTest() {
    }
};

}

#endif
