#ifndef RPCTHALFSORTSETTINGSIMPL_H_
#define RPCTHALFSORTSETTINGSIMPL_H_

#include <boost/dynamic_bitset.hpp>
#include "rpct/devices/hsb.h"
#include "rpct/devices/HalfSortSettings.h"

namespace rpct {

class HalfSortSettingsImpl : public HalfSortSettings {
private:
    boost::dynamic_bitset<> recChanEna_;
    std::vector<int> recFastClkInv_;
    std::vector<int> recFastClk90_;
    std::vector<int> recFastRegAdd_;
    boost::dynamic_bitset<> recFastDataDelay_;
    boost::dynamic_bitset<> recDataDelay_;

public:
    HalfSortSettingsImpl() :
        recChanEna_(THsbSortHalf::HSORT_DCHAN_NUM), recFastClkInv_(16, 0), recFastClk90_(16, 0),
                recFastRegAdd_(16, 0), recFastDataDelay_(48), recDataDelay_(32) {
    }

    HalfSortSettingsImpl(boost::dynamic_bitset<>& recChanEna_,
            std::vector<int>& recFastClkInv_,
            std::vector<int>& recFastClk90_,
            std::vector<int>& recFastRegAdd_,
            boost::dynamic_bitset<>& recFastDataDelay_,
            boost::dynamic_bitset<>& recDataDelay_) :
        enabledInputs_(enabledInputs) {
        if ((int) recChanEna.size() != THsbSortHalf::HSORT_DCHAN_NUM) {
            throw IllegalArgumentException("Invalid length of recChanEna_");
        }
        if ((int) recFastClkInv.size() != 16) {
            throw IllegalArgumentException("Invalid length of recFastClkInv");
        }
        if ((int) recFastClk90.size() != 16) {
            throw IllegalArgumentException("Invalid length of recFastClk90");
        }
        if ((int) recFastRegAdd.size() != 16) {
            throw IllegalArgumentException("Invalid length of recFastRegAdd");
        }
        if ((int) recFastDataDelay.size() != 48) {
            throw IllegalArgumentException("Invalid length of recFastDataDelay");
        }
        if ((int) recDataDelay.size() != 32) {
            throw IllegalArgumentException("Invalid length of recDataDelay");
        }
    }

    boost::dynamic_bitset<>& getRecChanEna() {
        return recChanEna_;
    }
    
    std::vector<int>& getRecFastClkInv() {
        return recFastClkInv_;
    }
    
    std::vector<int>& getRecFastClk90() {
        return recFastClk90_;
    }
    
    std::vector<int>& getRecFastRegAdd() {
        return recFastRegAdd_;
    }
    
    boost::dynamic_bitset<>& getRecFastDataDelay() {
        return recFastDataDelay_;
    }
    
    boost::dynamic_bitset<>& getRecDataDelay() {
        return recDataDelay_;
    }    
    
    virtual HardwareItemType getDeviceType() {
        return THsbSortHalf::TYPE;
    }
};

}

#endif 
