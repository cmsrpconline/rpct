//---------------------------------------------------------------------------

#ifndef TTTCrxI2CH
#define TTTCrxI2CH
//---------------------------------------------------------------------------
#include "ttccontr/TTTCrxBase.h"  
#include "rpct/i2c/TI2CInterface.h"
#include <log4cplus/logger.h>
#include "log4cplus/loggingmacros.h"

namespace rpct {

class TTTCrxI2C : public TTTCrxBase {
protected:
	static log4cplus::Logger logger;
private:
  TI2CInterface* I2C;
  unsigned char I2CPointer;
  unsigned char I2CData;

  void WriteReg(unsigned char reg, unsigned char value)
  {
	  int rep = 2;
	  for(int i = 0; i < rep; i++) {
		  try {
			  I2C->Write8(I2CPointer, reg);
			  I2C->Write8(I2CData, value);
			  return;
		  }
		  catch(EI2C& e) {
			  LOG4CPLUS_WARN(logger, boardName_ + " WriteReg() error accessing TTCrx via I2C: " << e.what() <<". Trying once more");
			  if(i == rep-1) {
				  throw e;
			  }
		  }
	  }
  }

  unsigned char ReadReg(unsigned char reg)
  {
	  int rep = 2;
	  for(int i = 0; i < rep; i++) {
		  try {
			  I2C->Write8(I2CPointer, reg);
			  return I2C->Read8(I2CData);
		  }
		  catch(EI2C& e) {
			  LOG4CPLUS_WARN(logger, boardName_ + " ReadReg() error accessing TTCrx via I2C: " << e.what() <<". Trying once more");
			  if(i == rep-1) {
				  throw e;
			  }
		  }
	  }
	  return 0;
  }

public:
  static void setupLogger(std::string namePrefix);
  TTTCrxI2C(unsigned char addr, std::string boardName)
    : TTTCrxBase(true, boardName), I2C(NULL), I2CPointer((addr << 1) & 0xfe), I2CData((addr << 1) | 1) {}

  void SetAddress(unsigned char addr);

  unsigned char GetAddress();

  void SetI2CInterface(TI2CInterface& i2c);
  TI2CInterface* GetI2CInterface()
    {
      return I2C;
    }
};

}

#endif
