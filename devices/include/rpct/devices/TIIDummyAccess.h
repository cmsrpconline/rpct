//---------------------------------------------------------------------------

#ifndef TIIDummyAccessH
#define TIIDummyAccessH
//---------------------------------------------------------------------------

#include "rpct/ii/TIIAccess.h"
#include "rpct/devices/LinkBoard.h" 
#include <log4cplus/logger.h>


namespace rpct {

class TIIDummyAccess : public TIIAccess {
private:
    static log4cplus::Logger logger;
    std::string deviceName_;
public:
    static void setupLogger(std::string namePrefix);
    TIIDummyAccess(uint32_t module_addr, std::string deviceName)
            : TIIAccess(module_addr), deviceName_(deviceName) {        
    }
    
    TIIDummyAccess(LinkBoard& lb)
            : TIIAccess((lb.getPosition() % 10) - 1) {
        deviceName_ = lb.getDescription() + ".SYNCODER";        
    }

    virtual ~TIIDummyAccess() {}

    virtual void Write(uint32_t address, void* data,
                       size_t word_count, size_t word_size);

    virtual void Read(uint32_t address, void* data,
                      size_t word_count, size_t word_size);
};


}

#endif
