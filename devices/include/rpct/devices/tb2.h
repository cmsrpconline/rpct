//---------------------------------------------------------------------------

//
//  Michal Pietrusinski 2001
//  Warsaw University
//
#ifndef tb2H
#define tb2H
//---------------------------------------------------------------------------

#include "tb_ii.h"  
//#include "tb_bs.h"  
#include "TIIABootContr.h"    
#include "TIIJtagIO.h"  
#include "TTlk.h"     
#include "TTTCrxI2C.h"      
#include "rpct/diag/TDiagCtrl.h" 
#include "rpct/diag/TStandardDiagnosticReadout.h" 
#include "rpct/diag/TPulser.h"
#include "rpct/devices/VmeCrate.h"
#include "rpct/i2c/TIII2CMasterCore.h"  
#include "rpct/ii/BoardBase.h"  
#include <log4cplus/logger.h>
#include <boost/dynamic_bitset.hpp>


namespace rpct {

class TTb2Device: public TIIDevice, public IIVMEDevice {
protected:
    int VMEBoardBaseAddr;
    static log4cplus::Logger logger;
public:
  static void setupLogger(std::string namePrefix);
  #define IID_FILE "RPC_system_def.iid"
  #include "iid2cdef1.h"

  #define IID_FILE "RPC_TB2_def.iid"
  #include "iid2cdef1.h"


    TTb2Device(int id,
               std::string name,
               TID name_id,
               std::string desc,
               const HardwareItemType& type,
               std::string ver,
               TID ver_id,
               TID checksum_id,
               uint32_t base_address,
               IBoard& board,
               int position,
               TMemoryMap* memmap,
               int vme_board_address)
            : TIIDevice(id, name, name_id, desc, type, ver, ver_id, checksum_id, 
            base_address, board, position, memmap),
    VMEBoardBaseAddr(vme_board_address) {
    }

    virtual ~TTb2Device() {}

    virtual int GetVMEBaseAddrShift()  { 
        return 16; 
    }

    virtual int GetVMEBoardBaseAddr() { 
        return VMEBoardBaseAddr; 
    }
};




class TTb2Vme : public TTb2Device {
private:
    TIIABootContr* ABootContr;
public:

  #define IID_FILE "RPC_TB2_vme.iid"
  #include "iid2c1.h"

    const static int II_ADDR_SIZE = 20;
    const static int II_DATA_SIZE = 32;
    
    const static HardwareItemType TYPE;  

    TTb2Vme(int vme_board_address, IBoard& board);
    virtual ~TTb2Vme();


    TIIABootContr& GetABootContr()
    {
        if (ABootContr == 0)
            ABootContr = new TIIABootContr(*this,
                                           VECT_ALTERA,
                                           BITS_ALTERA_NCONF,
                                           BITS_ALTERA_NSTATUS,
                                           BITS_ALTERA_CONFDONE,
                                           BITS_ALTERA_INITDONE,
                                           WORD_RESET,
                                           AREA_ALTERA_BOOT);
        return *ABootContr;
    }
};


class TTb2Control : public TTb2Device {
private:
    TIIABootContr* ABootContr;

    TIII2CMasterCore* I2C;
    TTTCrxI2C TTCrxI2C;
public:

  #define IID_FILE "RPC_TB2_ctrl.iid"
  #include "iid2c1.h"

    const static int II_ADDR_SIZE;
    const static int II_DATA_SIZE;
    const static HardwareItemType TYPE;  


    TTb2Control(int vme_board_address, IBoard& board, int position);
    virtual ~TTb2Control();


    TIIABootContr& GetABootContr()
    {
        if (ABootContr == 0)
            ABootContr = new TIIABootContr(*this,
                                           VECT_ALTERA,
                                           BITS_ALTERA_NCONF,
                                           BITS_ALTERA_NSTATUS,
                                           BITS_ALTERA_CONFDONE,
                                           BITS_ALTERA_INITDONE,
                                           0,
                                           AREA_ALTERA_BOOT);
        return *ABootContr;
    }

    void ResetQPLL();
    void ResetTTC();
    void InitTTC();
    TTTCrxI2C& GetTTCrxI2C()
    {
        if (TTCrxI2C.GetI2CInterface() == 0) {
            TTCrxI2C.SetI2CInterface(GetI2C());
        }
        return TTCrxI2C;
    }

    void SetTTCrxI2CAddress(unsigned char addr);

    TIII2CMasterCore& GetI2C();
};



class TTb2Opto : public TTb2Device, virtual public IDiagnosable {
public:
    class TChannel {
    private:
        TTb2Opto& Owner;
        int Channel; // 0 or 1 or 2
        /*TIIHistoCtrl* ErrHisto;
        THistoMgr* ErrHistoMgr;
        TFlashMgr* FlashMgr;
        TUniSelectiveReadout* SelectiveReadout;

        IHistoCtrl& GetErrHistoCtrl()
          {
            if (ErrHisto == 0) {
              ErrHisto = new TIIHistoCtrl(Owner,
                   (Channel == 0) ? TUni2Opto::AREA_MEM1_RATE_ERR : TUni2Opto::AREA_MEM2_RATE_ERR);
            }

            return *ErrHisto;
          } */
        //TTlk Tlk;
    public:
        TChannel(TTb2Opto& owner, int channel);
        virtual ~TChannel();

        TTlk& GetTlk()
        {
            //return Tlk;
            throw TException("GetTlk() not implemented");
        }

        TTb2Opto& GetOwner()
        {
            return Owner;
        }

        /*void ReadMemBuffer(uint32_t* destData, uint32_t size)
          {
            Owner.readArea((Channel == 0) ? TUni2Opto::AREA_MEM1_BUFOR : TUni2Opto::AREA_MEM2_BUFOR,
                         destData, 0, size);
          }

        void WriteMemGener(uint32_t* pulseData, uint32_t size)
          {
            Owner.writeArea((Channel == 0) ? TUni2Opto::AREA_MEM1_GENER : TUni2Opto::AREA_MEM2_GENER,
                             pulseData, 0, size);
          }

        void WriteDataEna(uint32_t masks)
          {
            Owner.writeWord((Channel == 0) ? TUni2Opto::WORD_DATA_ENA1 : TUni2Opto::WORD_DATA_ENA2,
                            0, masks);
          }

        void ReadDataEna(void* masks)
          {
            Owner.readWord((Channel == 0) ? TUni2Opto::WORD_DATA_ENA1 : TUni2Opto::WORD_DATA_ENA2, 0, masks);
          }

        void WriteDataDelay(uint32_t value)
          {
            Owner.writeWord((Channel == 0) ? TUni2Opto::WORD_DATA_DELAY1 : TUni2Opto::WORD_DATA_DELAY2,
                            0, value);
          }

        uint32_t ReadDataDelay()
          {
            return Owner.readWord((Channel == 0) ? TUni2Opto::WORD_DATA_DELAY1 : TUni2Opto::WORD_DATA_DELAY2, 0);
          }

        THistoMgr& GetErrHistoMgr()
          {
            if (ErrHistoMgr == 0)
              ErrHistoMgr = new THistoMgr(&GetErrHistoCtrl());

            return *ErrHistoMgr;
          }

        TFlashMgr& GetFlashMgr()
          {
            if (FlashMgr == 0) {
              FlashMgr = new TFlashMgr(Owner,
                                       (Channel == 0) ? AREA_MEM1_DAQ_FLASH : AREA_MEM2_DAQ_FLASH,
                                       (Channel == 0) ? BITS_DAQ1_FLASH_READY : BITS_DAQ2_FLASH_READY,
                                       (Channel == 0) ? WORD_DAQ1_FLASH_LEN : WORD_DAQ2_FLASH_LEN);
            }

            return *FlashMgr;
          }

        TUniSelectiveReadout& GetSelectiveReadout()
          {
            if (SelectiveReadout == 0) {
              if (Channel == 0)
                SelectiveReadout = new TUniSelectiveReadout(Owner,
                                      TUni2Opto::VECT_DAQ1_DIAG,
                                      TUni2Opto::BITS_DAQ1_DIAG_EMPTY,
                                      TUni2Opto::BITS_DAQ1_DIAG_EMPTY_ACK,
                                      TUni2Opto::BITS_DAQ1_DIAG_LOST,
                                      TUni2Opto::BITS_DAQ1_DIAG_LOST_ACK,
                                      TUni2Opto::VECT_DAQ1_DIAG_WR,
                                      TUni2Opto::BITS_DAQ1_DIAG_WR_ADDR,
                                      TUni2Opto::BITS_DAQ1_DIAG_WR_ACK,
                                      TUni2Opto::VECT_DAQ1_DIAG_RD,
                                      TUni2Opto::BITS_DAQ1_DIAG_RD_ADDR,
                                      TUni2Opto::BITS_DAQ1_DIAG_RD_ACK,
                                      TUni2Opto::WORD_DAQ1_DIAG_MASK,
                                      TUni2Opto::AREA_MEM1_DAQ_DIAG,
                                      TUni2Opto::RPC_FEBSTD_DATA_WIDTH,
                                      TUni2Opto::DAQ_DIAG_TRIG_NUM,
                                      TUni2Opto::DAQ_DIAG_TIMER_SIZE,
                                      TUni2Opto::TTC_BCN_EVT_WIDTH);
              else
                SelectiveReadout = new TUniSelectiveReadout(Owner,
                                      TUni2Opto::VECT_DAQ2_DIAG,
                                      TUni2Opto::BITS_DAQ2_DIAG_EMPTY,
                                      TUni2Opto::BITS_DAQ2_DIAG_EMPTY_ACK,
                                      TUni2Opto::BITS_DAQ2_DIAG_LOST,
                                      TUni2Opto::BITS_DAQ2_DIAG_LOST_ACK,
                                      TUni2Opto::VECT_DAQ2_DIAG_WR,
                                      TUni2Opto::BITS_DAQ2_DIAG_WR_ADDR,
                                      TUni2Opto::BITS_DAQ2_DIAG_WR_ACK,
                                      TUni2Opto::VECT_DAQ2_DIAG_RD,
                                      TUni2Opto::BITS_DAQ2_DIAG_RD_ADDR,
                                      TUni2Opto::BITS_DAQ2_DIAG_RD_ACK,
                                      TUni2Opto::WORD_DAQ2_DIAG_MASK,
                                      TUni2Opto::AREA_MEM2_DAQ_DIAG,
                                      TUni2Opto::RPC_FEBSTD_DATA_WIDTH,
                                      TUni2Opto::DAQ_DIAG_TRIG_NUM,
                                      TUni2Opto::DAQ_DIAG_TIMER_SIZE,
                                      TUni2Opto::TTC_BCN_EVT_WIDTH);
            }
            return *SelectiveReadout;
          }

        void Reset()
          {
            //GetDiagCtrl().StopCounting();
            GetErrHistoMgr().Reset();
            GetFlashMgr().Reset();
            GetFlashMgr().WriteFlashLen(DAQ_FLASH_ADDR_POS-1);
            GetSelectiveReadout().Reset();
          } */

        //disables all TLKs, zeros other registres,
    };

    class TOptLink {
    private:
        TTb2Opto* Opto;
        int Number; //0, 2, 3
    public:
        TOptLink(TTb2Opto* opto, int optLinkNum);

        virtual ~TOptLink();

        TTb2Opto& GetOpto() {
            return *Opto;
        }

        void EnableOptoLink(bool enable);

        bool FindOptLinksSynchrDelay(unsigned int testData);
    };

    friend class TOptLink;
    typedef std::vector<TOptLink> TOptLinks;
private:
    TDiagCtrl* DiagCtrl;
    TStandardDiagnosticReadout* DiagnosticReadout;
    TDiagCtrl* PulserDiagCtrl;
    TPulser* Pulser;

    typedef std::vector<TChannel*> TChannels;
    TChannels Channels;
    TDiagCtrlVector DiagCtrls;
    TDiagVector Diags;

    boost::dynamic_bitset<> EnabledOptLinks;
    TOptLinks OptLinks;
public:

  #define IID_FILE "RPC_TB2_opto.iid"
  #include "iid2c1.h"

    const static int II_ADDR_SIZE;
    const static int II_DATA_SIZE;
    const static HardwareItemType TYPE;  


    TTb2Opto(int id, int vme_board_address, int ii_addr, const char* desc, IBoard& board, 
        int position);
    virtual ~TTb2Opto();


    TDiagCtrl& GetDiagCtrl();
    TStandardDiagnosticReadout& GetDiagnosticReadout();
    TDiagCtrl& GetPulserDiagCtrl();
    TPulser& GetPulser();

    TChannel& GetChannel(int chan)
    {
        return *Channels.at(chan);
    }

    static int GetChannelCount()
    {
        return 3;
    }

    static unsigned int GetOptLinksCount()
    {
        return 3ul;
    }

    virtual TDiagCtrlVector& GetDiagCtrls();
    virtual TDiagVector& GetDiags();
    virtual void ConfigureDiagnosable(TTriggerDataSel triggerDataSel);

    void Reset();

    //linkNum = 0...2
    void EnableOptoLink(int linkNum, bool enable) {
        OptLinks[linkNum].EnableOptoLink(enable);
    }

    boost::dynamic_bitset<>&  GetEnabledOptLinks() {
        return EnabledOptLinks;
    }

    //the MLBs must be configured first: synchronizeLink()!!!!!
    bool CheckLinksSynchronization();

    //the MLBs must be configured first: EnableFindOptLinksSynchrDelay()!!!!!
    bool FindOptLinksSynchrDelay( unsigned int testData);

    bool CheckLinksOperation();
};

typedef std::vector<TTb2Opto*> TTb2Optos;

class TTb2LdPac : public TTb2Device, virtual public IDiagnosable {
private:
    TDiagCtrl* DiagCtrl;
    TStandardDiagnosticReadout* DiagnosticReadout;

    TDiagCtrlVector DiagCtrls;
    TDiagVector Diags;
public:

  #define IID_FILE "RPC_TB2_ldpac.iid"
  #include "iid2c1.h"

    const static int II_ADDR_SIZE;
    const static int II_DATA_SIZE;
    const static HardwareItemType TYPE;  


    TTb2LdPac(int id, int vme_board_address, int ii_addr, const char* desc, IBoard& board, int position);
    virtual ~TTb2LdPac();
    TDiagCtrl& GetDiagCtrl();
    TStandardDiagnosticReadout& GetDiagnosticReadout();
    virtual TDiagCtrlVector& GetDiagCtrls();
    virtual TDiagVector& GetDiags();
    virtual void ConfigureDiagnosable(TTriggerDataSel triggerDataSel);

    void Reset();
};

typedef std::vector<TTb2LdPac*> TTb2LdPacs;

class TTb2GbSort : public TTb2Device, virtual public IDiagnosable{
private:
    TDiagCtrl* DiagCtrl;
    TStandardDiagnosticReadout* DiagnosticReadout;

    TDiagCtrlVector DiagCtrls;
    TDiagVector Diags;
public:

  #define IID_FILE "RPC_TB2_gbsort.iid"
  #include "iid2c1.h"

    const static int II_ADDR_SIZE;
    const static int II_DATA_SIZE;
    const static HardwareItemType TYPE;  


    TTb2GbSort(int id, int vme_board_address, int ii_addr, const char* desc, IBoard& board, int position);
    virtual ~TTb2GbSort();
    TDiagCtrl& GetDiagCtrl();
    TStandardDiagnosticReadout& GetDiagnosticReadout();
    virtual TDiagCtrlVector& GetDiagCtrls();
    virtual TDiagVector& GetDiags();
    virtual void ConfigureDiagnosable(TTriggerDataSel triggerDataSel);

    void Reset();
};

class TTb2Rmb : public TTb2Device, virtual public IDiagnosable  {
private:
    TDiagCtrl* DiagCtrl;
    TStandardDiagnosticReadout* DiagnosticReadout;

    TDiagCtrlVector DiagCtrls;
    TDiagVector Diags;
public:

  #define IID_FILE "RPC_TB2_rmb.iid"
  #include "iid2c1.h"

    const static int II_ADDR_SIZE;
    const static int II_DATA_SIZE;
    const static HardwareItemType TYPE;  


    TTb2Rmb(int id, int vme_board_address, int ii_addr, const char* desc, IBoard& board, int position);
    virtual ~TTb2Rmb();
    TDiagCtrl& GetDiagCtrl();
    TStandardDiagnosticReadout& GetDiagnosticReadout();
    virtual TDiagCtrlVector& GetDiagCtrls();
    virtual TDiagVector& GetDiags();
    virtual void ConfigureDiagnosable(TTriggerDataSel triggerDataSel);
};


class TTb2: public BoardBase, virtual public IBoard {
private:
    static log4cplus::Logger logger;
    TVMEInterface* VME;
    TTb2Vme Vme;
    TTb2Control Control;

    int VMEAddress;;

    TTb2Optos Optos;
    TTb2LdPacs LdPacs;
    TTb2GbSort* GbSort;
    TTb2Rmb* Rmb;

    boost::dynamic_bitset<> UsedOptos;
    boost::dynamic_bitset<> UsedPacs;


    void Init(bool use_vme);

public:
    static void setupLogger(std::string namePrefix);
    const static HardwareItemType TYPE;  
    TTb2(int ident, const char* desc, TVMEInterface* vme, int vme_board_address, VmeCrate* crate, int pos);
    virtual ~TTb2();

    TTb2Vme& GetVme()
    { return Vme; }

    TTb2Control& GetControl()
    { return Control; }

    virtual IDevice& addChip(std::string type, int pos, int id);

    int GetVMEAddress()
    { return VMEAddress; }


    TTb2Optos& GetOptos() {
        return Optos;
    }

    TTb2LdPacs& GetLdPacs() {
        return LdPacs;
    }

    TTb2GbSort* GetGbSort() {
        return GbSort;
    }
    TTb2Rmb* GetRmb() {
        return Rmb;
    }

    void Reset();

    bool CheckOptLinksSynchronization();

    bool CheckLinksOperation();

    bool FindOptLinksSynchrDelay(unsigned int testData);

    void EnableTransmissionCheck(bool enable);

    //link num = 0...17
    void EnableOptLink(int linkNum, bool enable);
  
    virtual void init();

    virtual void selfTest() {
    }

    boost::dynamic_bitset<>& GetUsedPacs() {
        return UsedPacs;
    }

    boost::dynamic_bitset<>& GetUsedOptos() {
        return UsedOptos;
    }
};

}


#endif
