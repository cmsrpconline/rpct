#ifndef RPCTTBGBSORTSETTINGSIMPL_H_
#define RPCTTBGBSORTSETTINGSIMPL_H_

#include <boost/dynamic_bitset.hpp>
#include "rpct/devices/TriggerBoard.h"
#include "rpct/devices/TbGbSortSettings.h"


namespace rpct {
    

class TbGbSortSettingsImpl : public TbGbSortSettings {
private:
    boost::dynamic_bitset<> recMuxClkInv_;
    boost::dynamic_bitset<> recMuxClk90_;
    boost::dynamic_bitset<> recMuxRegAdd_;
    std::vector<int> recMuxDelay_;
    std::vector<int> recDataDelay_;
public:
	TbGbSortSettingsImpl() 
    : recMuxClkInv_(GbSort::TB_LDPAC_MUX_LINE_NUM), recMuxClk90_(GbSort::TB_LDPAC_MUX_LINE_NUM), 
    recMuxRegAdd_(GbSort::TB_LDPAC_MUX_LINE_NUM), 
    recMuxDelay_(GbSort::TB_PAC_BOARD_NUM, 0), recDataDelay_(GbSort::TB_PAC_BOARD_NUM, 0) {
    }

	TbGbSortSettingsImpl(
        boost::dynamic_bitset<> recMuxClkInv,
        boost::dynamic_bitset<> recMuxClk90,
        boost::dynamic_bitset<> recMuxRegAdd,
        std::vector<int> recMuxDelay,
        std::vector<int> recDataDelay)
    : recMuxClkInv_(recMuxClkInv), recMuxClk90_(recMuxClk90), 
    recMuxRegAdd_(recMuxRegAdd), recMuxDelay_(recMuxDelay), recDataDelay_(recDataDelay) {
        if ((int) recMuxClkInv.size() != GbSort::TB_LDPAC_MUX_LINE_NUM) {
            throw IllegalArgumentException("Invalid length of recMuxClkInv");
        }
        if ((int) recMuxClk90.size() != GbSort::TB_LDPAC_MUX_LINE_NUM) {
            throw IllegalArgumentException("Invalid length of recMuxClk90");
        }
        if ((int) recMuxRegAdd.size() != GbSort::TB_LDPAC_MUX_LINE_NUM) {
            throw IllegalArgumentException("Invalid length of recMuxRegAdd");
        }
        if ((int) recMuxDelay.size() != GbSort::TB_PAC_BOARD_NUM) {
            throw IllegalArgumentException("Invalid length of recMuxDelay");
        }
        if ((int) recDataDelay.size() != GbSort::TB_PAC_BOARD_NUM) {
            throw IllegalArgumentException("Invalid length of recDataDelay");
        }
    }
    
    boost::dynamic_bitset<>& getRecMuxClkInv() {
        return recMuxClkInv_;
    }
    
    boost::dynamic_bitset<>& getRecMuxClk90() {
        return recMuxClk90_;
    }
    
    boost::dynamic_bitset<>& getRecMuxRegAdd() {
        return recMuxRegAdd_;
    }
    
    std::vector<int>& getRecMuxDelay() {
        return recMuxDelay_;
    }
    
    std::vector<int>& getRecDataDelay() {
        return recDataDelay_;
    }
    
    virtual HardwareItemType getDeviceType() {
        return GbSort::TYPE;
    }
};

}

#endif 
