/** Filip Thyssen */

#ifndef rpct_devices_FebBoard_h_
#define rpct_devices_FebBoard_h_

#include <log4cplus/logger.h>

#include "rpct/devices/FebSystemItem.h"
#include "rpct/ii/HardwareItemBase.h"
#include "rpct/ii/Configurable.h"
#include "rpct/ii/Monitorable.h"

#include "rpct/ii/exception/Exception.h"

#include "rpct/ii/i2c/I2cResponse.h"

#include "rpct/devices/PCF8574a.h"
#include "rpct/devices/FebPart.h"
#include "rpct/devices/RecursiveAction.h"

namespace rpct {

namespace i2c {
class II2cAccess;
} // namespace i2c

class FebSystem;
class FebDistributionBoard;
class FebChip;

class FebBoard
    : public FebSystemItem
    , public Configurable
    , public IMonitorable
    , public IRecursive
{
protected:
    typedef FebPart * fps_type[2];

    static unsigned char pos2address(unsigned char position, bool febtype);
    static unsigned char chip2part(unsigned char chip);
    static unsigned char chip2position(unsigned char chip);
    int nparts() const;

public:
    static void setupLogger(std::string namePrefix);
    FebBoard(FebDistributionBoard & febdistributionboard
             , rpct::i2c::II2cAccess & i2c_access
             , int id
             , const std::string & description
             , unsigned short channel
             , bool febtype
             , unsigned char position
             , bool disabled = false);
    ~FebBoard();

    // FebSystemItem
    void reset();

    // THardwareItem
    int getId() const;
    const std::string & getDescription() const;
    const HardwareItemType & getType() const;

    // Configurable
    void configure(ConfigurationSet* configurationSet, int configureFlags);
    void configure(int configureFlags);
    void configure();

    void loadConfiguration(ConfigurationSet * configurationSet, int configureFlags);
    // Monitorable
    const MonitorItems & getMonitorItems();
    void monitor(volatile bool *stop);
    void monitor(volatile bool *stop, int configureFlags);
    void monitor(MonitorItems & items, int configureFlags = 0x0);

    // IPrintable
    void printParameters(tools::Parameters & parameters) const;

    // other
    rpct::i2c::II2cAccess & getI2cAccess() const;
    FebDistributionBoard & getFebDistributionBoard() const;

    unsigned short getChannel() const;
    int getFebType() const;
    unsigned char getAddress() const;

    // system
    FebPart & addFebPart(int id
                         , unsigned char position = 0
                         , bool disabled = false)
        throw (rpct::exception::SystemException);

    FebChip & addFebChip(int id
                         , unsigned char chip
                         , bool disabled = false)
        throw (rpct::exception::SystemException);

    // hardware
    void enableFebAccess(bool force = false);
    void disableFebAccess(bool force = false);
    void selectPart(unsigned char position, bool select = true)
        throw (rpct::exception::SystemException);
    void enablePart(unsigned char position, bool enable = true)
        throw (rpct::exception::SystemException);

    bool readPCF8574a();
    bool writePCF8574a(bool force = false);

    void i2c_read_pcf8574a_error(rpct::exception::Exception const * e = 0)
        throw();
    void i2c_read_pcf8574a_ready()
        throw();
    void i2c_write_pcf8574a_error(rpct::exception::Exception const * e = 0)
        throw();
    void i2c_write_pcf8574a_ready()
        throw();
private:
    static log4cplus::Logger logger_;
protected:
    static HardwareItemType * type_;
    static MonitorItems * mitems_;

    int id_;
    std::string description_;

    rpct::i2c::II2cAccess & i2c_access_;
    FebDistributionBoard & febdistributionboard_;

    const unsigned short channel_; //< as given by febdistributionboard
    const int febtype_; //< barrel or endcap, number of parts - 1
    const unsigned char address_; // as derived from the position

    fps_type fps_;

    PCF8574aRegister pcf8574a_register_;
    rpct::i2c::TI2cResponse<FebBoard> * pcf8574a_read_response_;
    rpct::i2c::TI2cResponse<FebBoard> * pcf8574a_write_response_;
    bool is_hardware_accessed_;
};

} // namespace rpct

#include "rpct/devices/FebBoard-inl.h"

#endif // rpct_devices_FebBoard_h_
