#ifndef RPCTSTANDARDBXDATA_H_
#define RPCTSTANDARDBXDATA_H_

#include "rpct/diag/IDiagnosticReadout.h"
#include "rpct/devices/LmuxBxData.h"
#include "rpct/devices/RPCHardwareMuon.h"
#include "rpct/std/tbutil.h"

#include <vector>
#include <algorithm>

#include <log4cplus/logger.h>

namespace rpct {
    
//the base class for storing data of bx of diagnostic readout
class StandardBxData : virtual public IDiagnosticReadout::ConvertedBxData {
public:
	static void setupLogger(std::string namePrefix);
    typedef std::vector<LmuxBxData*> LmuxBxDataVector;
    typedef std::vector<RPCHardwareMuon*> MuonVector;
protected:
    static log4cplus::Logger logger;
    IDiagnosticReadout::BxData& bxData_;    
    LmuxBxDataVector lmuxBxDataVector_;
    MuonVector muonVector_;
    unsigned int channelsValid_;    
    std::string dataStr_;
public:
    StandardBxData(IDiagnosticReadout::BxData& bxData) : bxData_(bxData), channelsValid_(0) {
    } 
    
    virtual ~StandardBxData() {
    	std::for_each(lmuxBxDataVector_.begin(), lmuxBxDataVector_.end(), FDelete<LmuxBxData*>());
    	std::for_each(muonVector_.begin(), muonVector_.end(), FDelete<RPCHardwareMuon*>());
    }
    
    virtual int getIndex() {
    	return bxData_.getIndex();
    }
    
    virtual IDiagnosticReadout::BxData& getBxData() {
        return bxData_;
    }    
    
    virtual LmuxBxDataVector& getLmuxBxDataVector() {
        return lmuxBxDataVector_;
    }
    
    virtual MuonVector& getMuonVector() {
        return muonVector_;
    }
    
    virtual unsigned int getChannelsValid() {
        return channelsValid_;
    }
    
    virtual std::string toString() {
    	return dataStr_;
    };
    
    virtual bool isNotEmpty() {
        if(lmuxBxDataVector_.size() > 0)
            return true;
        if(muonVector_.size() > 0)
            return true;
        
        return false;
    }
    unsigned int checkAndAddMuon(RPCHardwareMuon* muon);
};


}

#endif 
