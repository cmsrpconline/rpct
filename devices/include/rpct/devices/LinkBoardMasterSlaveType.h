#ifndef LINKBOARDMASTERSLAVETYPE_H_
#define LINKBOARDMASTERSLAVETYPE_H_

namespace rpct {

enum LinkBoardMasterSlaveType { msMaster = 0, msRight = 1, msLeft = 2 };

}

#endif /*LINKBOARDMASTERSLAVETYPE_H_*/
