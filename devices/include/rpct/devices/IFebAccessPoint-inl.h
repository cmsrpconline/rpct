/** Filip Thyssen */

#ifndef rpct_devices_IFebAccessPoint_inl_h_
#define rpct_devices_IFebAccessPoint_inl_h_

#include "rpct/devices/IFebAccessPoint.h"

namespace rpct {

inline int IFebAccessPoint::getPosition() const
{
    return position_;
}
inline int IFebAccessPoint::getId() const
{
    return id_;
}
inline const std::string & IFebAccessPoint::getDescription() const
{
    return description_;
}
inline const HardwareItemType & IFebAccessPoint::getType() const
{
    return *type_;
}
inline const IMonitorable::MonitorItems & IFebAccessPoint::getMonitorItems()
{
    return *mitems_;
}
inline void IFebAccessPoint::monitor(volatile bool *stop)
{
    monitor(*mitems_);
}
inline rpct::i2c::II2cAccess & IFebAccessPoint::getI2cAccess() const
{
    return i2c_access_;
}

} // namespace rpct

#endif // rpct_devices_IFebAccessPoint_inl_h_
