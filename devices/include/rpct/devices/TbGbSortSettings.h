#ifndef RPCTTBGBSORTSETTINGS_H_
#define RPCTTBGBSORTSETTINGS_H_

#include "rpct/ii/DeviceSettings.h"
#include <boost/dynamic_bitset.hpp>



namespace rpct {
    

class TbGbSortSettings : virtual public DeviceSettings  {
public:
    virtual ~TbGbSortSettings() {
    }
    
    virtual boost::dynamic_bitset<>& getRecMuxClkInv() = 0;  
    virtual boost::dynamic_bitset<>& getRecMuxClk90() = 0;    
    virtual boost::dynamic_bitset<>& getRecMuxRegAdd() = 0;    
    virtual std::vector<int>& getRecMuxDelay() = 0;    
    virtual std::vector<int>& getRecDataDelay() = 0;
};

}

#endif 
