#ifndef _RPCTCONFIGURATIONSETIMPL_H_
#define _RPCTCONFIGURATIONSETIMPL_H_


#include "rpct/ii/ConfigurationSet.h"
#include <map>
#include "rpct/std/ENotImplemented.h"

namespace rpct {

class ConfigurationSetImpl : public virtual ConfigurationSet {
public:
    typedef std::map<int, DeviceSettingsPtr> DeviceSettingsByChipId;
    typedef std::map<HardwareItemType, DeviceSettingsPtr> DeviceSettingsByType;
private:
    unsigned int id_;
    DeviceSettingsByChipId deviceSettings_;
    DeviceSettingsByType defaultDeviceSettings_;
public:
    ConfigurationSetImpl() : id_(0) {
    }

    ConfigurationSetImpl(unsigned int id) : id_(id) {
    }

    virtual unsigned int getId() {
        return id_;
    }

    void addDeviceSettings(int deviceId, DeviceSettingsPtr settings) {
        deviceSettings_[deviceId] = settings;
    }

    void addDeviceSettings(DeviceSettingsPtr settings) {
        defaultDeviceSettings_[settings->getDeviceType()] = settings;
    }

    virtual DeviceSettings* getDeviceSettings(IDevice& device) {
        DeviceSettingsByChipId::iterator iDS = deviceSettings_.find(device.getId());
        if (iDS != deviceSettings_.end()) {
            return iDS->second.get();
        }

        DeviceSettingsByType::iterator iDS1 = defaultDeviceSettings_.find(device.getType());
        if (iDS1 != defaultDeviceSettings_.end()) {
            return iDS1->second.get();
        }
        return 0;
    }

    //not giving the default settings
    virtual DeviceSettings* getDeviceSettings(int id) {
        DeviceSettingsByChipId::iterator iDS = deviceSettings_.find(id);
        if (iDS != deviceSettings_.end()) {
            return iDS->second.get();
        }

        return 0;
    }

    virtual DeviceSettingsPtr getDeviceSettingsPtr(IDevice& device) {
      DeviceSettingsByChipId::iterator iDS = deviceSettings_.find(device.getId());
      if (iDS != deviceSettings_.end()) {
	return iDS->second;
      }

      DeviceSettingsByType::iterator iDS1 = defaultDeviceSettings_.find(device.getType());
      if (iDS1 != defaultDeviceSettings_.end()) {
	return iDS1->second;
      }
      return DeviceSettingsPtr();
    }

    virtual ISynCoderConfInFlash* getSynCoderConfInFlash(int synCoderId) {
    	throw ENotImplemented("ConfigurationSetImpl::getSynCoderConfInFlash");
    	return 0;
    }
};

}

#endif
