// M. Bluj 01/02/07 
// CCS registers
static const int ID_REG = 0x300;
static const int CONFIG0 = 0x304;
static const int CONFIG1 = 0x308;
static const int STATUS0 = 0x30C;
static const int TTS_STAT = 0x310;
static const int TTS_CONF = 0x314;
// reserved adress 0x318;
static const int LOC_STRB = 0x31C;
static const int LOC_L1 = 0x320;
static const int LOC_B101B = 0x324;
static const int LOC_B110B = 0x328;
static const int LOC_B111B = 0x32C;
static const int LOC_OUT1 = 0x330;
static const int LOC_OUT2 = 0x334;
static const int LOC_OUT3 = 0x338;
static const int LOC_OUT4 = 0x33C;
static const int LOC_EXITIN0 = 0x340;
static const int LOC_EXITIN1 = 0x344;
static const int LOC_FREQ = 0x348;
static const int LOC_BCMD = 0x34C;
