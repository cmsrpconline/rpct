//---------------------------------------------------------------------------

//
//  Michal Pietrusinski 2006
//  Warsaw University
//

#ifndef rpctfebH
#define rpctfebH
//---------
/*   
Based on:
File I2C.c    10 July 2001
Created by F. Loddo
Isituto Nazionale di Fisica Nucleare  Sez. di BARI

This code contains the main Routines to drive I2C lines 
on RPC FEB via the PC-Parallel Port

10-7-2001 Write threshold with feedback
------------------------------------------------------------------
*/     
#include "rpct/devices/DistributionBoard.h"   
#include <log4cplus/logger.h>

namespace rpct {

class Feb {
protected:
    static log4cplus::Logger logger;
    DistributionBoard& distributionBoard;
    unsigned int localNumber;
    unsigned int convertedLocalNumber;
    bool dacEnabled;
    bool autoCorrection;
    
    static const unsigned char ADDR_AD7417 = 0x28;   
    static const unsigned char ADDR_PCF8574A_CR = 0x38;

    static const unsigned char CHAN_VTH1 = 0x20;
    static const unsigned char CHAN_VTH2 = 0x40;
    static const unsigned char CHAN_VMON1 = 0x60;
    static const unsigned char CHAN_VMON2 = 0x80;
    
    static const float MAX_VTH_DIFF; // = 5./1.024;
    static const float MAX_VMON_DIFF;// = 50;

    virtual float readAD7417_ADC(unsigned int chan);
    
    static const unsigned char WTHP_VTH1 = 1;
    static const unsigned char WTHP_VTH2 = 2;
    static const unsigned char WTHP_VMON1 = 4;
    static const unsigned char WTHP_VMON2 = 8;
    virtual void writeAD5316_DAC(unsigned int wthp, float value);
    
    virtual unsigned char readPCF8574A_CR();
    virtual void writePCF8574A_CR(unsigned char value);
    virtual TI2CInterface& getI2C();
    virtual void sleep();
    
    virtual unsigned int convertLocalNumber(unsigned int localNumber);
public:
    static void setupLogger(std::string namePrefix);
    //static int Delay;
    Feb(DistributionBoard& distributionBoard, unsigned int localNumber);
    virtual ~Feb();
    
    virtual DistributionBoard& getDistributionBoard() {
        return distributionBoard;
    }
    
    virtual bool isAutoCorrection() {
    	return autoCorrection;
    }
    
    virtual void setAutoCorrection(bool autoCorrection) {
    	this->autoCorrection = autoCorrection;
    }
    
    virtual float readTemperature();     // 2a)

    virtual float readVTH1();            // 2b)
    virtual void writeVTH1(float value);

    virtual float readVTH2();            // 2b)
    virtual void writeVTH2(float value);
    
    virtual float readVMon1();             // 2b)
    virtual void writeVMon1(float value);

    virtual float readVMon2();             // 2b)
    virtual void writeVMon2(float value);
                                       
    virtual void enableDAC(bool value);
    virtual bool isDacEnabled() {
        return dacEnabled;
    }
    
    virtual void selectDAC(bool value);
    
    virtual void write(float* vth1, float* vth2, float* vmon1, float* vmon2);
    
    virtual void writeRead(float* vth1, float* vth2, float* vmon1, float* vmon2);
    
    /** Filip, 2009.10.16: Function to check speed gain with/without autocorrect, readback and predefined offsets */
    virtual void writeReadOffset(float & vth1, float & vth2
                                 , float & vtho1, float & vtho2
                                 , float & vmon1, float & vmon2
                                 , float & vmono1, float & vmono2
                                 , const bool & correct_vth = true, const bool & correct_vmon = true
                                 , const bool & read_vth = true, const bool & read_vmon = true);
};


}

#endif
