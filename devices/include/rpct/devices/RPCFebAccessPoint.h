/** Filip Thyssen */

#ifndef rpct_devices_RPCFebAccessPoint_h_
#define rpct_devices_RPCFebAccessPoint_h_

#include "rpct/devices/IFebAccessPoint.h"

namespace rpct {

class TCB;
class RPCFebSystem;

class RPCFebAccessPoint
    : public IFebAccessPoint
{
public:
    RPCFebAccessPoint(RPCFebSystem & system
                      , TCB & controlboard
                      , bool disabled = false);

    // other
    TCB & getControlBoard() const;

protected:
    TCB & controlboard_;
};

inline TCB & RPCFebAccessPoint::getControlBoard() const
{
    return controlboard_;
}

} // namespace rpct

#endif // rpct_devices_RPCFebAccessPoint_h_
