#ifndef RPCTPACSETTINGS_H_
#define RPCTPACSETTINGS_H_

#include "rpct/ii/DeviceSettings.h"
#include <boost/dynamic_bitset.hpp>



namespace rpct {


class PacSettings : virtual public DeviceSettings  {
public:
    virtual ~PacSettings() {
    }

    virtual boost::dynamic_bitset<>& getRecMuxClkInv() = 0;
    virtual boost::dynamic_bitset<>& getRecMuxClk90() = 0;
    virtual boost::dynamic_bitset<>& getRecMuxRegAdd() = 0;
    virtual std::vector<int>& getRecMuxDelay() = 0;
    virtual std::vector<int>& getRecDataDelay() = 0;
    virtual unsigned int getBxOfCoincidence() = 0;
    virtual unsigned int getPacEna() = 0;
    virtual unsigned int getPacConfigId() = 0;
};

}

#endif
