#ifndef RPCT_CCSSETTINGS_H
#define RPCT_CCSSETTINGS_H

#include <boost/dynamic_bitset.hpp>
#include "rpct/ii/DeviceSettings.h"
#include "rpct/devices/Ccs.h"

namespace rpct {
    

class CcsSettings : virtual public DeviceSettings {
 private:
  boost::dynamic_bitset<> enabledInputs_;
 public:
  CcsSettings() 
    : enabledInputs_(5) {
  }

  CcsSettings(unsigned int enabledIns) 
    : enabledInputs_(5,enabledIns) {
  }
    
  virtual ~CcsSettings() {
  }
    
  boost::dynamic_bitset<>& getEnabledInputs() {
    return enabledInputs_;
  }

  virtual HardwareItemType getDeviceType() {
    return Ccs::TYPE;
  }
};

};

#endif 
