#ifndef RPCTTTUTRIGBXDATA_H_
#define RPCTTTUTRIGBXDATA_H_

#include "rpct/diag/IDiagnosticReadout.h"
#include "rpct/devices/StandardBxData.h"

namespace rpct {
    

class TTUTrigBxData : public StandardBxData {
public:
    TTUTrigBxData(IDiagnosticReadout::BxData& bxData);
};

class TTUTrigBxDataFactory : virtual public IDiagnosticReadout::ConvertedBxDataFactory {
private:
    static TTUTrigBxDataFactory instance_;
    TTUTrigBxDataFactory() {
    }
    
public:    
    virtual IDiagnosticReadout::ConvertedBxData* create(IDiagnosticReadout::BxData& bxData, IDiagnosticReadout& ownerReadout) {
        return new TTUTrigBxData(bxData);
    }       
    
    static TTUTrigBxDataFactory& getInstance() {
        return instance_;
    }
    
};

}

#endif 
