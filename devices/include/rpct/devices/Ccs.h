#ifndef RPCT_CCS_H
#define RPCT_CCS_H

#include "tb_ii.h" 
#include "rpct/devices/VCcs_pkgDef.h"
#include "rpct/devices/VmeCrate.h"
#include "rpct/ii/BoardBase.h"
#include "rpct/devices/DummyVMEDevice.h"
#include "rpct/std/ENotImplemented.h"  
#include <log4cplus/logger.h>
#include <boost/dynamic_bitset.hpp>
#include <bitset>
#include <vector>
#include "rpct/devices/CcsMonitorables.h"

namespace rpct {


class CcsTriggerFPGA: public TDummyVMEDevice {
public:
    static void setupLogger(std::string namePrefix);
private:
    boost::dynamic_bitset<> enabledInputs_;
protected:
    static log4cplus::Logger logger;
public:
    const static HardwareItemType TYPE;

    CcsTriggerFPGA(int vme_board_address, IBoard& board, TVMEInterface* vme);
    ~CcsTriggerFPGA() {}

    virtual void reset();

    // low level access utils
    int readWord(int addr){
        return vme_->Read32(addr+BoardAddress+BaseAddress);	
    }

    void writeWord(int addr, unsigned int val){
        vme_->Write32(addr+BoardAddress+BaseAddress, val);	
    }

    //** pythonowy oryginal
    //   def setbits32(self,adr,bits):
    //      val=self.read32(adr)
    //      for i in bits:
    //         val |= (1L << i)
    //      self.write32(adr,val)
    //**
    void setBits(int addr, std::vector<unsigned int> bits){
        addr+=BoardAddress+BaseAddress;
        unsigned int val = vme_->Read32(addr);
        for(unsigned int i = 0; i<bits.size(); ++i)
            val |= (1 << bits[i]);
        vme_->Write32(addr,val);
    }

    //** pythonowy oryginal
    // def clrbits32(self,adr,bits):
    //    val=self.read32(adr)
    //    for i in bits:
    //       val &= ~(1L << i)
    //    self.write32(adr,val)
    //**
    void clearBits(int addr, std::vector<unsigned int> bits){
        addr+=BoardAddress+BaseAddress;
        unsigned int val = vme_->Read32(addr);
        for(unsigned int i = 0; i<bits.size(); ++i)
            val &= ~(1 << bits[i]);
        vme_->Write32(addr,val);
    }    

    void setEnabledInputs(const boost::dynamic_bitset<>& enabledInputs);

    virtual void configure(DeviceSettings* settings, int flags);
    
    virtual void enable() {        
    }
    
    virtual void disable() {        
    }

    void checkName(){}

    void checkVersion(){}

};

//---------------------------------------------------------------

class Ccs: public BoardBase, virtual public IBoard {
public:
    static void setupLogger(std::string namePrefix);
private:
    static log4cplus::Logger logger;
    TVMEInterface* vme_;
    int vmeAddress_;
    CcsTriggerFPGA triggerFPGA;
    void Init();
    CcsMonitorables monitorables_;

public:
    const static HardwareItemType TYPE; 
    Ccs(int ident, const char* desc, TVMEInterface* vme, int vme_board_address, VmeCrate* crate, int pos);
    virtual ~Ccs();

    int getVmeAddress() { 
        return vmeAddress_; 
    }

    virtual IDevice& addChip(std::string type, int pos, int id) {
        throw ENotImplemented("Ccs::addChip");
    }

    CcsTriggerFPGA& getTriggerFPGA(){
        return triggerFPGA;
    }

    void initialize();

    void configure(unsigned int dccIn);
    virtual void configure(DeviceSettings* settings);

    void forceTTS(unsigned int status);

    void freeTTS();

    int checkTTS();
    
    int checkStatus();

    CcsMonitorables * checkBoard();

    virtual void selfTest() {
    }

};

}

#endif
