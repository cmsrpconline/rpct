#ifndef RPCTTTUBACKPLANESETTINGS_H_
#define RPCTTTUBACKPLANESETTINGS_H_

#include "rpct/ii/DeviceSettings.h"
//#include <boost/dynamic_bitset.hpp>



namespace rpct {

class TtuBackplaneSettings : virtual public DeviceSettings  {
public:
    virtual ~TtuBackplaneSettings() {
    }

    //"MASK_TTU" regValue="0"
    //"MASK_POINTING"  regValue="0" />
    //"MASK_BLAST"  regValue="0" />
    //"DELAY_TTU" regValue="0" />
    //"DELAY_POINTING"  regValue="0" />
    //"DELAY_BLAST"  regValue="2" />
    //"SHAPE_TTU" regValue="0" />
    //"SHAPE_POINTING"  regValue="0" />
    //"SHAPE_BLAST"  regValue="2" />
    //"TRIG_CONFIG" size="6" >
    //"TRIG_CONFIG2" size="6" >

	virtual int getConfigurationId() = 0;
    virtual unsigned int  getMaskTtu()              = 0;
    virtual unsigned int  getMaskPointing()         = 0;
    virtual unsigned int  getMaskBlast()            = 0;
    virtual unsigned int  getDelayTtu()             = 0;
    virtual unsigned int  getDelayPointing()        = 0;
    virtual unsigned int  getDelayBlast()           = 0;
    virtual unsigned int  getShapeTtu()             = 0;
    virtual unsigned int  getShapePointing()        = 0;
    virtual unsigned int  getShapeBlast()           = 0;
    virtual std::vector<int>&  getTrigConfig()           = 0;
    virtual std::vector<int>&  getTrigConfig2()          = 0;
    
};

}

#endif
