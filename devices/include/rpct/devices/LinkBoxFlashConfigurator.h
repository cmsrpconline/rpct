#ifndef RPCT_LINKBOXFLASHCONFIGURATOR_H
#define RPCT_LINKBOXFLASHCONFIGURATOR_H

#include "rpct/devices/LinkBoxConfigurator.h"
#include "rpct/devices/LinkBoard.h"
#include "rpct/i2c/TI2CInterface.h"
#include "rpct/std/NullPointerException.h"
#include <stdlib.h>

namespace rpct {

class LinkBoxFlashConfigurator : public LinkBoxConfigurator, virtual public TI2CInterface, virtual public IHardwareIfc {
protected:
    std::string outputDir_;
    typedef std::map<int, FILE*> TFilesMap;
    TFilesMap filesMap_;
    LinkBoard* curLinkBoard_;
    FILE* curFile_;
    
    void setCurLinkBoard(LinkBoard* lb);
    void writeCommand(unsigned short word1, unsigned short word2);
    void writeCommand(FILE* file, unsigned short word1, unsigned short word2);
public:
    LinkBoxFlashConfigurator(LinkBox& linkBox, const std::string outputDir) :
        LinkBoxConfigurator(linkBox), outputDir_(outputDir), curLinkBoard_(0) {
    }

    virtual ~LinkBoxFlashConfigurator() {
    }
    
    virtual void configStart();
    virtual void configEnd();
    
    //virtual void writeII(LinkBoard& lb, IDevice::TID id, uint32_t value) = 0;
    virtual void writeII(LinkBoard& lb, uint32_t address, uint32_t data);
    virtual void writeI2C(LinkBoard& lb, unsigned char address, unsigned char value);
    virtual void waitBit(int bitNum, int value);
    virtual void intiTTCrxs();

    // TI2CInterface
    virtual void Write8(unsigned char address, unsigned char value) {
        if (curLinkBoard_ == 0) {
            throw NullPointerException("LinkBoxFlashConfigurator::Write8: curLinkBoard_ == 0");
        }
        writeI2C(*curLinkBoard_, address, value);
    }
    virtual unsigned char Read8(unsigned char address, bool ack = true) {
        throw TException("LinkBoxFlashConfigurator::Read8 not allowed");
    }

    virtual void WriteADD(unsigned char address, unsigned char value1, unsigned char value2) {
        throw TException("LinkBoxFlashConfigurator::WriteADD not allowed");
    }
    virtual void ReadADD(unsigned char address, unsigned char& value1, unsigned char& value2) {
        throw TException("LinkBoxFlashConfigurator::ReadADD not allowed");
    }

    virtual void Write(unsigned char address, unsigned char* data, size_t count) {
        throw TException("LinkBoxFlashConfigurator::Write not allowed");
    }

    
    // IHardwareIfc interface
    virtual void Write(uint32_t address, void* data,
                       size_t word_count, size_t word_size);

    virtual void Write(uint32_t* addresses, void** data,
                       size_t* word_counts, size_t word_size, size_t count) {
        throw TException("LinkBoxFlashConfigurator::Read not allowed");
    }

    virtual void Read(uint32_t address, void* data,
                      size_t word_count, size_t word_size) {
        throw TException("LinkBoxFlashConfigurator::Read not allowed");
    }
    
    
};

}

#endif
