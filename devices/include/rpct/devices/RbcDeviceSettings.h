#ifndef RPCTRBCDEVICESETTINGS_H_
#define RPCTRBCDEVICESETTINGS_H_

#include "rpct/ii/DeviceSettings.h"
#include <boost/dynamic_bitset.hpp>



namespace rpct {

class RbcDeviceSettings : virtual public DeviceSettings  {
public:
    virtual ~RbcDeviceSettings() {
    }

    //... List of registers: CONF, CONFIG_IN, MAJ_REG, CONFIG_VER(=Nsync_pulse), MASK, SHAPE, CTRL

    virtual int getConfigurationId() = 0; //same id as in the DB
    
    virtual unsigned int getRbcDeviceConfig()   = 0;
    virtual unsigned int getRbcDeviceConfigIn() = 0;
    virtual unsigned int getRbcDeviceMajority() = 0;
    virtual unsigned int getRbcDeviceShape()    = 0;
    virtual unsigned int getRbcDeviceConfVer()  = 0;
    virtual unsigned int getRbcMask()           = 0;
    virtual unsigned int getRbcCtrl()           = 0;


};

}

#endif
