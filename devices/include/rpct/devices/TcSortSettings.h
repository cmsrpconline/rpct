#ifndef RPCTTCSORTSETTINGS_H_
#define RPCTTCSORTSETTINGS_H_

#include "rpct/ii/DeviceSettings.h"
#include <boost/dynamic_bitset.hpp>



namespace rpct {


class TcSortSettings : virtual public DeviceSettings  {
public:
    virtual ~TcSortSettings() {
    }

    virtual boost::dynamic_bitset<>& getRecMuxClkInv() = 0;
    virtual boost::dynamic_bitset<>& getRecMuxClk90() = 0;
    virtual boost::dynamic_bitset<>& getRecMuxRegAdd() = 0;
    virtual std::vector<int>& getRecMuxDelay() = 0;
    virtual std::vector<int>& getRecDataDelay() = 0;
    virtual std::string getBootScript() = 0;
};

}

#endif
