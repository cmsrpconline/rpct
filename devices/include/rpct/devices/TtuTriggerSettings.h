#ifndef RPCTTTUTRIGGERSETTINGS_H_
#define RPCTTTUTRIGGERSETTINGS_H_

#include "rpct/ii/DeviceSettings.h"
#include <boost/dynamic_bitset.hpp>



namespace rpct {

class TtuTriggerSettings : virtual public DeviceSettings  {
public:
    virtual ~TtuTriggerSettings() {
    }

	virtual int getConfigurationId() = 0;
    virtual boost::dynamic_bitset<>& getRecMuxClkInv() = 0;
    virtual boost::dynamic_bitset<>& getRecMuxClk90()  = 0;
    virtual boost::dynamic_bitset<>& getRecMuxRegAdd() = 0;

    virtual std::vector<int>& getRecMuxDelay()         = 0;
    virtual std::vector<int>& getRecDataDelay()        = 0;

    virtual unsigned int  getTA_TrgDelay()             = 0;
    virtual unsigned int  getTA_TrgTOff()              = 0;
    virtual unsigned int  getTA_TrgSel()               = 0;
    virtual unsigned int  getTA_TrgCntrGate()          = 0;

    virtual std::vector<int>&  getTA_SectorDelay()     = 0;
    virtual std::vector<int>&  getTA_ForceLogic0()     = 0;
    virtual std::vector<int>&  getTA_ForceLogic1()     = 0;
    virtual std::vector<int>&  getTA_ForceLogic2()     = 0;
    virtual std::vector<int>&  getTA_ForceLogic3()     = 0;
    virtual std::vector<int>&  getTrgConfig()          = 0;
    virtual std::vector<int>&  getTrgMask()            = 0;
    virtual std::vector<int>&  getTrgForce()           = 0;
    virtual std::vector<int>&  getSectorMajority()     = 0;
    virtual std::vector<int>&  getTA_Config2()         = 0;
    virtual std::vector<int>&  getConnectedRbcMask()   = 0;

    virtual unsigned int  getTrgMajority()             = 0;
    virtual unsigned int  getSectorTrigMask()          = 0;
    virtual unsigned int  getSectorThreshold()         = 0;
    virtual unsigned int  getTowerThreshold()          = 0;
    virtual unsigned int  getWheelThreshold()          = 0;
    virtual unsigned int  getSectorTrgSel()            = 0;

    //get configVer
    //virtual unsigned int  getConfigVer()               = 0;

};

}

#endif
