#ifndef RPCTSYNCODERSETTINGS_H_
#define RPCTSYNCODERSETTINGS_H_

#include "rpct/ii/DeviceSettings.h"
#include <boost/dynamic_bitset.hpp>

namespace rpct {
    

class SynCoderSettings : virtual public DeviceSettings {
public:
	virtual int getConfigurationId() = 0;
    virtual unsigned int getWindowOpen() = 0;
    virtual unsigned int getWindowClose() = 0;
    virtual bool getInvertClock() = 0;
    virtual unsigned int getLMuxInDelay() = 0;
    virtual unsigned int getRbcDelay() = 0;
    virtual unsigned int getBcn0Delay() = 0;
    virtual unsigned int getDataTrgDelay() = 0;
    virtual unsigned int getDataDaqDelay() = 0;
    virtual unsigned int getPulserTimerTrgDelay() = 0;
    virtual boost::dynamic_bitset<>& getInChannelsEna() = 0;
};

}

#endif 
