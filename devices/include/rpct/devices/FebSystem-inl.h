/** Filip Thyssen */

#ifndef rpct_devices_FebSystem_inl_h_
#define rpct_devices_FebSystem_inl_h_

#include "rpct/devices/FebSystem.h"

namespace rpct {

inline std::map<int, FebSystemItem *> const & FebSystem::getFebSystemItems(std::string const & _type) const
{
    std::map<std::string, std::map<int, FebSystemItem *> >::const_iterator _it(fsis_.find(_type));
    if (_it != fsis_.end())
        return _it->second;
    else
        return fsis_.find("empty")->second;
}

inline IFebAccessPoint * FebSystem::getFebAccessPoint(int id) const
{
    faps_type::const_iterator it_faps = faps_.find(id);
    if (it_faps != faps_.end())
        return it_faps->second;
    else
        return 0;
}

inline void FebSystem::publish(int id, int type, double value)
{}

inline int FebSystem::getId() const
{
    return id_;
}
inline const std::string & FebSystem::getDescription() const
{
    return description_;
}
inline void FebSystem::setDescription(const std::string & description)
{
    description_ = description;
}
inline const HardwareItemType & FebSystem::getType() const
{
    return *type_;
}

inline const IMonitorable::MonitorItems & FebSystem::getMonitorItems()
{
    return *mitems_;
}
inline void FebSystem::monitor(volatile bool *stop)
{
    monitor(*mitems_);
}

inline rpct::tools::Time & FebSystem::getConfigurationTime()
{
    return configuration_time_;
}

inline rpct::tools::Time & FebSystem::getMonitoringTime()
{
    return monitoring_time_;
}

inline rpct::tools::Timer & FebSystem::getMonitorCycleTimer()
{
    return monitorcycle_timer_;
}

inline bool FebSystem::getInterrupt() const
{
    return interrupt_;
}
inline void FebSystem::setInterrupt(bool interrupt)
{
    interrupt_ = interrupt;
}

} // namespace rpct

#endif // rpct_devices_FebSystem_inl_h_
