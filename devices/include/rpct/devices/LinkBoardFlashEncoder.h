#ifndef LinkBoardFlashEncoderH
#define LinkBoardFlashEncoderH
#include <sstream>
#include <string>
#include <stdint.h>

/* A rewrite of corlbc2 for encoding initialization data in flash for KTP chip.
 * maciekz 2012 
 *
 * To encode data: create a FlashEncoder, use putByte or putWord to put your 
 * data, call end() when ready to get encoded contents.
 *
 * To encode next set of data call reset().
 * No data can be added between calling end() and reset() (this is due to
 * injection of end-of-data markers).
 */

//Ten program wyznacza zawarto�� flasha z korekcj�
//Poni�sza funkcja wyznacza sum� kontroln� zer
//w s�owie. Najstarsze 27 bit�w to dane
//Najm�odsze 5 bit�w - kontrola
//Program nale�y radykalnie przeorganizowa� - musi by� zrobiony obiektowo - obiekt
//filtr wyj�ciowy przyjmuje nowe s�owo, i kiedy trzeba generuje dane wyj�ciowe.

namespace rpct {

class LinkBoardFlashEncoder{
	private:
		//Bufor wyj�ciowy
		std::stringstream out;
		std::stringstream unencodedOut;
		bool finish;// = false;
		
		//Bufor na dane generowane
		unsigned char d1[5];//={0xff,0xff,0xff,0xff,0xff};
		unsigned char d2[5];//={0xff,0xff,0xff,0xff,0xff};
		unsigned char lb[81]; //Tu b�dzie wektor bitowy
		uint32_t l[4];
		//Licznik bufora
		unsigned char dptr;//=0;
		//Bufor na 1 bajt
		bool hasByte;// = false;
		unsigned char byte;// = 0;

		void initialize();
		uint32_t sumzer(uint32_t dt);
		void flush();
		void addwrd(unsigned short int d);
	
	public:
		void putByte(unsigned char _byte);
		void putWord(unsigned short int w);
		void reset();
		std::string end();
		std::string getUnencoded();
		LinkBoardFlashEncoder();
};
}
#endif
