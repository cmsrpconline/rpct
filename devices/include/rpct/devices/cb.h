//---------------------------------------------------------------------------

//
//  Michal Pietrusinski 2001
//  Warsaw University
//

#ifndef cbH
#define cbH
//---------------------------------------------------------------------------
#include "ICB.h"
#include "rpct/i2c/TIII2CMasterCore.h"
#include "rpct/i2c/TRPCCCUI2CMasterCore.h"
#include "LinkBox.h"
#include "rpct/devices/FebInstanceManager.h"
#include "rpct/std/ENotImplemented.h"
#include "rpct/ii/BoardBase.h"
#include "rpct/ii/Monitorable.h"
#include <log4cplus/logger.h>

#include "rpct/devices/i2c/CommandI2c.h"
#include "rpct/devices/i2c/BlockI2c.h"
#include "rpct/devices/i2c/BI2cClient.h"
#include "rpct/ii/exception/Exception.h"

namespace rpct {

class LinkBox; // MC, 20 Feb 2010
class HalfBox; // MC, 20 Feb 2010

class TCB: virtual public BoardBase, virtual public ICB, virtual public Monitorable {
public:
    static void setupLogger(std::string namePrefix);

private:
    static log4cplus::Logger logger;
    LinkBox* lbox;

    // I2C
    TRPCCCUI2CMasterCore* i2c;

    // TTCrx
    //TTTCrxI2C TTCrxI2C;

    FebInstanceManager febInstanceManager;

    rpct::devices::i2c::CommandI2c * command_i2c_;
    rpct::devices::i2c::BlockI2c * block_i2c_;
    rpct::devices::i2c::BI2cClient * bi2c_client_;
    uint16_t lastCbicReloadCounter;
    bool checkAndUpdateLastCbicReloadCounter(bool saveOnly);
public:
    const static HardwareItemType TYPE;
    TCB(int ident, const char* desc, LinkBox* LBox, int boardNum);
    virtual ~TCB();

    //functions with CCU errors recovery (reserFec)
    void memoryWrite(uint32_t address, unsigned short data);
    unsigned short memoryRead(uint32_t address);
    virtual void memoryWrite16(uint32_t address, unsigned short data);
    virtual unsigned short memoryRead16(uint32_t address);

    TI2CInterface& getI2C();

    virtual void initI2C();

    void selectI2CChannel(int channel);

    virtual IDevice& addChip(std::string type, int pos, int id) {
        throw ENotImplemented("TCB::addChip: not implemented");
    }

    virtual Feb& getFeb(int channel, unsigned int localNumber);
    virtual FebEndcap& getFebEndcap(int channel, unsigned int localNumber);

    virtual void init() {
    }

    virtual void selfTest() {
    }

    virtual void checkVersions();
    virtual std::string readTestRegs(bool verbose=false);

    virtual void configure(ConfigurationSet* configurationSet, int configureFlags);

    virtual void monitor(volatile bool *stop);
    virtual void monitor(MonitorItems& items);

    virtual void enable();

    virtual HalfBox* getHalfBox();

    StdLinkBoxAccess * getLBoxAccess() const
    throw (rpct::exception::SystemException);
    rpct::devices::i2c::CommandI2c & getCommandI2c();
    rpct::devices::i2c::BlockI2c & getBlockI2c();
    rpct::devices::i2c::BI2cClient & getBI2cClient();
    rpct::i2c::II2cAccess & getI2cAccess() {
        return getBI2cClient();
    }
};

}

#endif
