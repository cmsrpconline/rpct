#ifndef RPCTRMBSETTINGS_H_
#define RPCTRMBSETTINGS_H_

#include "rpct/ii/DeviceSettings.h"

namespace rpct {
    

class RmbSettings : virtual public DeviceSettings {
public:
    virtual ~RmbSettings() {
    }
    
    virtual boost::dynamic_bitset<>& getRecMuxClkInv() = 0;  
    virtual boost::dynamic_bitset<>& getRecMuxClk90() = 0;    
    virtual boost::dynamic_bitset<>& getRecMuxRegAdd() = 0;    
    virtual std::vector<int>& getRecMuxDelay() = 0;    
    virtual std::vector<int>& getRecDataDelay() = 0;
    
    virtual int getChanEna() = 0;  
    virtual int getPreTriggerVal() = 0;    
    virtual int getPostTriggerVal() = 0;    
    virtual int getDataDelay() = 0;    
    virtual int getTrgDelay() = 0;
};

}

#endif 
