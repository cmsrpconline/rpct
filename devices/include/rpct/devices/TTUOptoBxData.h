#ifndef RPCTTTUOPTOBXDATA_H_
#define RPCTTTUOPTOBXDATA_H_

#include "rpct/diag/IDiagnosticReadout.h"
#include "rpct/devices/StandardBxData.h"

namespace rpct {
    

class TTUOptoBxData : public StandardBxData {   
public:
	TTUOptoBxData(IDiagnosticReadout::BxData& bxData);  
};

class TTUOptoBxDataFactory : virtual public IDiagnosticReadout::ConvertedBxDataFactory {
private:
    static TTUOptoBxDataFactory instance_;
    TTUOptoBxDataFactory() {
    }
    
public:    
    virtual IDiagnosticReadout::ConvertedBxData* create(IDiagnosticReadout::BxData& bxData, IDiagnosticReadout& ownerReadout) {
        return new TTUOptoBxData(bxData);
    }       
    
    static TTUOptoBxDataFactory& getInstance() {
        return instance_;
    }
    
};

}

#endif 
