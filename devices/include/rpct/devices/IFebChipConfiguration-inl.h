/** Filip Thyssen */

#ifndef rpct_devices_IFebChipConfiguration_inl_h_
#define rpct_devices_IFebChipConfiguration_inl_h_

#include "rpct/devices/IFebChipConfiguration.h"

namespace rpct {

inline HardwareItemType IFebChipConfiguration::getDeviceType()
{
    return *type_;
}

} // namespace rpct

#endif // rpct_devices_FebChipConfiguration_inl_h_
