/** Filip Thyssen */

#ifndef rpct_devices_IFebAccessPoint_h_
#define rpct_devices_IFebAccessPoint_h_

#include <map>

#include <log4cplus/logger.h>

#include "rpct/devices/FebSystemItem.h"
#include "rpct/ii/HardwareItemBase.h"
#include "rpct/ii/Configurable.h"
#include "rpct/ii/Monitorable.h"

#include "rpct/ii/exception/Exception.h"

#include "rpct/devices/FebDistributionBoard.h"

namespace rpct {


namespace i2c {
class II2cAccess;
} // namespace i2c

class FebSystem;

class IFebAccessPoint
    : public FebSystemItem
    , public Configurable
    , public IMonitorable
{
protected:
    typedef std::map<int, FebDistributionBoard*> fdbs_type;
public:
    static void setupLogger(std::string namePrefix);
    IFebAccessPoint(FebSystem & febsystem
                    , rpct::i2c::II2cAccess & i2c_access
                    , int id
                    , const std::string & description
                    , int position
                    , bool disabled = false);
    virtual ~IFebAccessPoint();

    int getPosition() const;

    // FebSystemItem
    void reset();

    // THardwareItem
    int getId() const;
    const std::string & getDescription() const;
    const HardwareItemType & getType() const;

    // Configurable
    void configure(ConfigurationSet* configurationSet, int configureFlags);
    void configure();

    void loadConfiguration(ConfigurationSet * configurationSet, int configureFlags);
    // Monitorable
    const MonitorItems & getMonitorItems();
    void monitor(volatile bool *stop);
    void monitor(MonitorItems & items);

    // Printable
    void printParameters(tools::Parameters & parameters) const;

    // other
    rpct::i2c::II2cAccess & getI2cAccess() const;

    FebDistributionBoard & addFebDistributionBoard(int id
                                                   , const std::string & description
                                                   , unsigned short channel
                                                   , bool dt = 0
                                                   , unsigned char pca9544_address_offset = 0
                                                   , unsigned char dt_channel = 0
                                                   , bool disabled = false);
private:
    static log4cplus::Logger logger_;
    static HardwareItemType * type_;
protected:
    static MonitorItems * mitems_;

    int id_;
    std::string description_;

    rpct::i2c::II2cAccess & i2c_access_;

    const int position_;

    fdbs_type fdbs_;
};

} // namespace rpct

#include "rpct/devices/IFebAccessPoint-inl.h"

#endif // rpct_devices_IFebAccessPoint_h_
