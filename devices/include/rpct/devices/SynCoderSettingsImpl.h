#ifndef RPCTSYNCODERSETTINGSIMPL_H_
#define RPCTSYNCODERSETTINGSIMPL_H_

#include "rpct/devices/SynCoderSettings.h"
#include "rpct/devices/LinkBoard.h"
#include <boost/dynamic_bitset.hpp>

namespace rpct {
    
//obsolete, do not use!!!!
class SynCoderSettingsImpl : virtual public SynCoderSettings {
private:
    unsigned int winO;
    unsigned int winC;
    bool invClock;
    unsigned int lMuxInDelay;
    unsigned int rbcDelay;
    unsigned int bcn0Delay;
    unsigned int dataTrgDelay;
    unsigned int dataDaqDelay;
    unsigned int pulserTimerTrgDelay;
    boost::dynamic_bitset<> inChannelsEna;
public:
    SynCoderSettingsImpl() {
        winO = 0;
        winC = 0;
        invClock = false;
        lMuxInDelay = 0;
        rbcDelay = 0;
        bcn0Delay = 0;
        dataTrgDelay = 0;
        dataDaqDelay = 0;
        pulserTimerTrgDelay = 0;
        //inChannelsEna;
    }

    SynCoderSettingsImpl(
        unsigned int _winO,
        unsigned int _winC,
        bool _invClock,
        unsigned int _lMuxInDelay,
        unsigned int _rbcDelay,
        unsigned int _bcn0Delay,
        unsigned int _dataTrgDelay,
        unsigned int _dataDaqDelay,
        unsigned int _pulserTimerTrgDelay,
        boost::dynamic_bitset<> _inChannelsEna)
            : winO(_winO),
            winC(_winC),
            invClock(_invClock),
            lMuxInDelay(_lMuxInDelay),
            rbcDelay(_rbcDelay),
            bcn0Delay(_bcn0Delay),
            dataTrgDelay(_dataTrgDelay),
            dataDaqDelay(_dataDaqDelay),
            pulserTimerTrgDelay(_pulserTimerTrgDelay),
            inChannelsEna(_inChannelsEna)  {
    }

    virtual int getConfigurationId() {
    	return 0; //!!!!!!!!!!!!!!!!!!!!!!!!
    }

    virtual unsigned int getWindowOpen() {
        return winO;
    }
    virtual unsigned int getWindowClose() {
        return winC;
    }
    virtual bool getInvertClock() {
        return invClock;
    }
    virtual unsigned int getLMuxInDelay() {
        return lMuxInDelay;
    }
    virtual unsigned int getRbcDelay() {
        return rbcDelay;
    }
    virtual unsigned int getBcn0Delay() {
        return bcn0Delay;
    }
    virtual unsigned int getDataTrgDelay() {
        return dataTrgDelay;
    }
    virtual unsigned int getDataDaqDelay() {
        return dataDaqDelay;
    }
    virtual unsigned int getPulserTimerTrgDelay() {
        return pulserTimerTrgDelay;
    }
    virtual boost::dynamic_bitset<>& getInChannelsEna() {
        return inChannelsEna;
    }
    
    virtual HardwareItemType getDeviceType() {
        return SynCoder::TYPE;
    }
};

}

#endif 
