#ifndef RPCTTCGBSORTDATA_H_
#define RPCTTCGBSORTDATA_H_

#include "rpct/diag/IDiagnosticReadout.h"
#include "rpct/devices/StandardBxData.h"

namespace rpct {
    

class TcGbSortBxData : public StandardBxData {
public:
    TcGbSortBxData(IDiagnosticReadout::BxData& bxData, IDiagnosticReadout& ownerReadout);
};

class TcGbSortBxDataFactory : virtual public IDiagnosticReadout::ConvertedBxDataFactory {
private:
    static TcGbSortBxDataFactory instance_;
    TcGbSortBxDataFactory() {
    }
    
public:    
    virtual IDiagnosticReadout::ConvertedBxData* create(IDiagnosticReadout::BxData& bxData, IDiagnosticReadout& ownerReadout) {
        return new TcGbSortBxData(bxData, ownerReadout);
    }       
    
    static TcGbSortBxDataFactory& getInstance() {
        return instance_;
    }
    
};

}

#endif 
