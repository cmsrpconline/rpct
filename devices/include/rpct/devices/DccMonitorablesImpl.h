#ifndef RPCTDCCMONITORABLESIMPL_H
#define RPCTDCCMONITORABLESIMPL_H

#include "rpct/devices/DccMonitorables.h"

namespace rpct {
    
class DccMonitorablesImpl : virtual public DccMonitorables {
 private:
  unsigned int fedId_;
  unsigned int ebStatus_;
  unsigned int ttsStatus_;
  int ebEvtCnt_;
//  std::vector<unsigned int> ihStatusVec_;
//  std::vector<unsigned int> ihEmergencyMask_;
//  std::vector<int> ihEvtCntVec_;

 public:
  DccMonitorablesImpl(): fedId_(0), ebStatus_(0), ttsStatus_(0), ebEvtCnt_(-1) 
//                     ,ihStatusVec_(9,0), ihEmergencyMask_(9,-1), ihEvtCntVec_(9,-1) 
{}
  virtual ~DccMonitorablesImpl() {}
    
  unsigned int getFedId() { return fedId_; }
  unsigned int getEbStatus() { return ebStatus_; }    

  unsigned int getTtsStatus() { return ttsStatus_; }
  void setTtsStatus(unsigned int s) { ttsStatus_ = s; }

  int getEbEvtCnt() { return ebEvtCnt_; }    

//  int getIhEmergencyMask(unsigned int ihNum) {
//    if((ihNum>=0)&&(ihNum<=8)) return ihEmergencyMask_[ihNum]; else return -1;
//  }

//  int getIhStatus(unsigned int ihNum) {
//   if((ihNum>=0)&&(ihNum<=8))
//    return ihStatusVec_[ihNum];
//   else
//    return -1;
//  }    

//  int getIhEvtCnt(unsigned int ihNum) {
//   if((ihNum>=0)&&(ihNum<=8))
//    return ihEvtCntVec_[ihNum];
//   else
//    return -10;
//  }    

//  std::vector<unsigned int> getIhStatusVec() {
//    return ihStatusVec_;
//  }

//  std::vector<int> getIhEvtCntVec() {
//    return ihEvtCntVec_;
//  }

  void setFedId(unsigned int fedId) { fedId_ = fedId; }

  void setEbStatus(unsigned int ebStat) {
   ebStatus_ = ebStat;
  }    

  void setEbEvtCnt(unsigned int ebEvtCnt) {
   ebEvtCnt_ = ebEvtCnt;
  }    

/*
  bool setIhEmergencyMask(unsigned int ihEMask, unsigned int ihNum) {
    if((ihNum>=0)&&(ihNum<=8)){
      ihEmergencyMask_[ihNum] = ihEMask;
      return true;
    } else return false;
  }

  bool setIhStatus(unsigned int ihStat, unsigned int ihNum) {
   if((ihNum>=0)&&(ihNum<=8)){
    ihStatusVec_[ihNum] = ihStat;
    return true;
   }
   else
    return false;
  }

  bool setIhEvtCnt(int ihEvtCnt, unsigned int ihNum) {
   if((ihNum>=0)&&(ihNum<=8)){
    ihEvtCntVec_[ihNum] = ihEvtCnt;
    return true;
   }
   else
    return false;
  }
*/

  HardwareItemType getDeviceType() {
    return Dcc::TYPE;
  }

};

}

#endif 
