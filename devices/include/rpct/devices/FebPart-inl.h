/** Filip Thyssen */

#ifndef rpct_devices_FebPart_inl_h_
#define rpct_devices_FebPart_inl_h_

#include "rpct/devices/FebPart.h"

namespace rpct {

inline int FebPart::getId() const
{
    return id_;
}

inline const HardwareItemType & FebPart::getType() const
{
    return *type_;
}
inline const IMonitorable::MonitorItems & FebPart::getMonitorItems()
{
    return *mitems_;
}
inline void FebPart::configure()
{
    configure(0x0);
}
inline void FebPart::monitor(volatile bool *stop)
{
    monitor(*mitems_, 0x0);
}
inline void FebPart::monitor(volatile bool *stop, int configureFlags)
{
    monitor(*mitems_, configureFlags);
}

inline rpct::i2c::II2cAccess & FebPart::getI2cAccess() const
{
    return i2c_access_;
}
inline FebBoard & FebPart::getFebBoard() const
{
    return febboard_;
}

inline float FebPart::getTemperature() const
{
    return adc_register_temperature_.temperature();
}

inline void FebPart::readVTH()
{
    readVTH(0);
    readVTH(1);
}
inline void FebPart::readVMON()
{
    readVMON(0);
    readVMON(1);
}
inline void FebPart::read()
{
    readVTH();
    readVMON();
}
inline void FebPart::write()
{
    writeVTH();
    writeVMON();
}

} // namespace rpct

#endif // rpct_devices_FebPart_inl_h_
