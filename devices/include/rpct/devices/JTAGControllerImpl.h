/**
 *     @class JTAGControllerImpl
 *
 *     @short jal::JTAGController implementation wrapping the bs::IJtagIO
 *
 *    @author Karol Bunkowski
 * $Revision: 1.1 $
 *     $Date: 2007/03/27 07:59:06 $
 *
 *
 **/

#ifndef JTAGCONTROLLERIMPL_H
#define JTAGCONTROLLERIMPL_H
#include "jal/jtagController/JTAGController.h"
//#include "jal/jtagController/Timer.h"
#include "../bs/contr/IJtagIO.h"

#include <log4cplus/logger.h>
#include <string>
#include <stdint.h>

namespace rpct {

class JTAGControllerImpl : public jal::JTAGController {
private:
	static log4cplus::Logger logger_;
	IJtagIO* jtagIO_;
public:
	static void setupLogger(std::string namePrefix);
	JTAGControllerImpl(IJtagIO* jtagIO, bool simulatePulsing, double SCKFrequency):
		jtagIO_(jtagIO),
		_simulatepulsing(simulatePulsing),
		_sck_frequency(SCKFrequency),
		_desiredTCKfrequency(-1.),
		_debug_flag(0),
		_initialized(false) //, _timer()
	{

	}

	virtual ~JTAGControllerImpl() {

	}
	/// return the number of JTAG chains supportes by the JTAG controller.
	virtual uint32_t numberOfChains() {
		//jtagIO_->GetChannlesCnt();
		return 1;
	};

	/// select the JTAG chain active for subsequent commands.
	virtual void selectChain(int ichain)
	throw(jal::HardwareException,
			jal::OutOfRangeException)
	{
		jtagIO_->SetChannel(ichain);
	};

	/// Set the desired TCK frequency (in Hz) for subsequent commands.
	///
	/// This command has to be used (by the JTAGChain) before the first pulseTCK command
	/// so that the controller knows how to scale the number ot pulses or to convert
	/// them into a time delay.
	///
	/// The other commands (shift, sequenceTMS) ignore the desired frequency as it is assumed
	/// that this implementation is always slower than the desired speed.
	///
	/// @param f is the desired frequency in Hz.
	virtual void setDesiredTCKFrequency (double f)
	throw(jal::HardwareException,
			jal::OutOfRangeException) { _desiredTCKfrequency = f; };

	/// send a sequence on the TMS line of the active chain (max. 16 bits)
	///
	/// @param bitcount is the number of bist (max 16).
	/// @param sequence contains teh bits. The LSB is shifted first.
	virtual void sequenceTMS(uint32_t bitcount, uint32_t sequence)
	throw(jal::HardwareException,
			jal::TimeoutException,
			jal::OutOfRangeException);

	/// shift bits through the active JTAG chain
	///
	/// @param bitcount is the number of bits to shift.
	/// @param data_out is the date to shift to teh chain.
	/// @param data_in is a reference to the data read from the chain (if doRead is true)
	/// @param doRead specifies that data is to be read back form the chain.
	/// @param autoTMS specifies that TMS should automatically go high while shifting
	///        the last bit in order to move the JTAG state machine to the EXIT1 state
	///
	virtual void shift(uint32_t bitcount,
			std::vector<uint8_t> const & data_out,
			std::vector <uint8_t> & data_in,
			bool doRead,
			bool autoTMS = true)
	throw(jal::HardwareException,
			jal::TimeoutException,
			jal::OutOfRangeException);


	/// pulse the JTAG clock num_tck times
	///
	/// @param num_tcks is the number of ticks
	/// @param tmshigh indicates whether TMS should be high while pulsing the clock.
	///        needed for running a RUNTEST command in RESET state
	/// @param stage gives the stages to perform.
	///        It is in the range (jal::PULSESTAGE_ALL, jal::PULSESTAGE_PRE, jal::PULSESTAGE_PAUSE).
	///
	///?????????????????????????????????
	///        If stage = jal::RTSTAGE_ALL, then the full command is performed.
	///        For the other stage options the behavior depends the simulatepulsing optin in the constuctor.
	///        If simulatepulsing is true, then only two JTAG clocks are pulsed when called with jal::PULSESTAGE_PRE
	///        and only a pause statemant is issued  when called with jal::PULSESTAGE_PAUSE.
	///
	///        If simulatepulsing is false, then all operations
	///        are performed when called with jal::PULSESTAGE_PRE,
	///        and nothing is donewhen called with jal::PULSESTAGE_PAUSE.
	///
	virtual void pulseTCK(uint32_t num_tcks,
			bool tmshigh = false,
			jal::PulseStage stage = jal::PULSESTAGE_ALL)
	throw(jal::HardwareException,
			jal::TimeoutException,
			jal::OutOfRangeException);


	/// lock the controller
	virtual void lock();

	/// unlock the controller
	virtual void unlock();

	/// get frequency of the JTAG System Clock in Hz.
	/// may be needed by the chain in order to convert microseconds into cycles
	virtual double getSCKFrequency()
	throw(jal::HardwareException,
			jal::TimeoutException,
			jal::OutOfRangeException) { return _sck_frequency; };

	/// set debug flag (0..3 no debug, 4: debug, 5: debug & no hardware access)
	virtual void setDebugFlag (int f) { _debug_flag = f; };

	/// initialize the CMSDAQJTAGController
	/// this method is automatically called by all methods if not done before
	virtual void init()
	throw(jal::HardwareException);


private:
    bool _simulatepulsing;
    double _sck_frequency;

	double _desiredTCKfrequency;
	int _debug_flag;
	bool _initialized;

	//jal::Timer _timer;
};

}
#endif 
