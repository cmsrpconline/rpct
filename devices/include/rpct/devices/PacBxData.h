#ifndef RPCTPACBXDATA_H_
#define RPCTPACBXDATA_H_

#include "rpct/diag/IDiagnosticReadout.h"
#include "rpct/devices/StandardBxData.h"

namespace rpct {
    

class PacBxData : public StandardBxData {
public:
    PacBxData(IDiagnosticReadout::BxData& bxData);
};

class PacBxDataFactory : virtual public IDiagnosticReadout::ConvertedBxDataFactory {
private:
    static PacBxDataFactory instance_;
    PacBxDataFactory() {
    }
    
public:    
    virtual IDiagnosticReadout::ConvertedBxData* create(IDiagnosticReadout::BxData& bxData, IDiagnosticReadout& ownerReadout) {
        return new PacBxData(bxData);
    }       
    
    static PacBxDataFactory& getInstance() {
        return instance_;
    }
    
};

}

#endif 
