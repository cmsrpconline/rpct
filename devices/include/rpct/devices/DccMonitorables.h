#ifndef RPCTDCCMONITORABLES_H
#define RPCTDCCMONITORABLES_H

#include "rpct/devices/DeviceMonitorables.h"
#include <vector>
#include "rpct/devices/DccInputHandlerStatus.h"

namespace rpct {
    
class DccMonitorables : virtual public DeviceMonitorables {
 public:
  DccMonitorables();
  virtual ~DccMonitorables() {}

  void setFedId(unsigned int fedId) { fedId_ = fedId; }
  unsigned int getFedId() const { return fedId_; }

  void setEbStatus(unsigned int ebStat) { ebStatus_ = ebStat; }
  unsigned int getEbStatus() const { return ebStatus_; }

  unsigned int getTtsStatus() const { return ttsStatus_; }
  void setTtsStatus(unsigned int s) { ttsStatus_ = s; }

  unsigned int getEbEvtCnt() const { return ebEvtCnt_; }
  void setEbEvtCnt(unsigned int ebEvtCnt) { ebEvtCnt_ = ebEvtCnt; }

  void setEbOosDbg(unsigned int ebOosDbg) { ebOosDbg_ = ebOosDbg; }
  unsigned int getEbOosDbg() const { return ebOosDbg_; }

  void setEbEqlzStatus(unsigned int  ebEqlzStatus) { ebEqlzStatus_ = ebEqlzStatus; }
  unsigned int  getEbEqlzStatus() const { return ebEqlzStatus_; }

  virtual std::string printInfo() const;

  const std::vector<DccInputHandlerStatus> & inputHandlerStatuses() const {return theDccInputHandlerStatuses;}
  void addInputHandlerStatus(const DccInputHandlerStatus & stat) {
    theDccInputHandlerStatuses.push_back(stat); 
  }

  HardwareItemType getDeviceType();
  
private:
  unsigned int fedId_;
  unsigned int ebStatus_;
  unsigned int ttsStatus_;
  unsigned int ebOosDbg_;
  unsigned int ebEvtCnt_;
  unsigned int ebEqlzStatus_; 

  std::vector<DccInputHandlerStatus> theDccInputHandlerStatuses;

};

}

#endif 
