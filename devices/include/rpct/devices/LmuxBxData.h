#ifndef RPCTLMUXBXDATA_H_
#define RPCTLMUXBXDATA_H_
#include <string>
#include <vector>

namespace rpct {

class LmuxBxData {
public:
	static const unsigned int BITS_CNT = 19; 
    LmuxBxData();

    LmuxBxData(unsigned int rawData, int index);

    LmuxBxData(unsigned short partitionData,
               unsigned char partitionNum,
               unsigned char partitionDelay,
               unsigned char endOfData,
               unsigned char halfPartition,
               unsigned char lbNum,
               int index);

    unsigned short getPartitionData() const { return partitionData_; }
    unsigned char getPartitionNum() const { return partitionNum_; }
    unsigned char getPartitionDelay() const { return partitionDelay_; }
    unsigned char getEndOfData() const { return endOfData_; }
    unsigned char getHalfPartition() const { return halfPartition_; }
    unsigned char getLbNum() const { return lbNum_; }
    int getIndex() const { return index_; }
    unsigned int getRaw() {return raw_; }

    void setPartitionData(unsigned short partData) { partitionData_=partData; }
    void setPartitionNum(unsigned char partNo) { partitionNum_=partNo; }
    void setPartitionDelay(unsigned char partDelay) { partitionDelay_=partDelay; }
    void setEndOfData(unsigned char eofData) { 
    	endOfData_=eofData; 
    }
    void setHalfPartition(unsigned char halfPart) { halfPartition_=halfPart; }
    void setLbNum(unsigned char lbNo) { lbNum_=lbNo; }
    //void setRaw(unsigned char raw) { raw_ = raw; }
    
    void fromRaw(unsigned short rawData);

    unsigned int toRaw();

    std::string toString();

    bool operator == (const LmuxBxData& right) const;

    bool operator != (const LmuxBxData& right) const;

	struct More : public std::less<LmuxBxData> {
        bool operator() (const LmuxBxData& dataL,
                         const LmuxBxData& dataR) const {
            if(dataL.getPartitionDelay() == dataR.getPartitionDelay() ) {
	            if(dataL.getLbNum() == dataR.getLbNum() ) {
	            	return dataL.getPartitionNum() > dataR.getPartitionNum();
	            }
	            else {
	            	return dataL.getLbNum() < dataR.getLbNum();
	            }
            }
            else 
            	return dataL.getPartitionDelay() < dataR.getPartitionDelay();           
        }
    };

private:
    unsigned short partitionData_;
    unsigned char partitionNum_;
    unsigned char partitionDelay_;
    unsigned char endOfData_;
    unsigned char halfPartition_;
    unsigned char lbNum_;
    
    unsigned char raw_;
    int index_;
};





typedef std::vector<LmuxBxData> LmuxBxDataVec;

typedef std::vector<LmuxBxDataVec> LmuxBxDataVec2;

LmuxBxDataVec2 demutliplex(const LmuxBxDataVec& muxedDataVec);

std::vector<LmuxBxData*> code(void* stripData);

}
#endif /*TLMUXBXDATA_H_*/
