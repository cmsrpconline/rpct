#ifndef ILBDIAGNOSABLE_H_
#define ILBDIAGNOSABLE_H_

#include "rpct/diag/IDiagnosable.h"


namespace rpct {

class ILBDiagnosable : virtual public IDiagnosable {
public:
    virtual void ConfigureDiagnosable(TTriggerDataSel triggerDataSel, 
            uint32_t dataTrgDelay,
            uint32_t triggerAreaEna, bool synFullOutSynchWindow,
            bool synPulseEdgeSynchWindow) = 0;
};

}

#endif /*ILBDIAGNOSTICREADOUT_H_*/
