#ifndef RPCTTRIGGERCRATE_H
#define RPCTTRIGGERCRATE_H

#include "rpct/devices/VmeCrate.h"
#include "rpct/devices/TriggerBoard.h"
#include "rpct/devices/tcsort.h"



namespace rpct {

class TriggerCrate : public VmeCrate {
private:
    static log4cplus::Logger logger;
    int logSector_;
    TTCSort* tcSort_;
    std::list<TriggerBoard*> triggerBoards_;
protected:
    //virtual bool checkResetNeeded(); //if reset of the TTCrx and QPLL is needed
    virtual bool checkWarm(ConfigurationSet* configurationSet);
    virtual void rememberConfiguration(ConfigurationSet* configurationSet);
    virtual void reset(bool forceReset);
public:
    static void setupLogger(std::string namePrefix);
    const static HardwareItemType TYPE;
    TriggerCrate(int ident, const char* desc, int logSector)
        : VmeCrate(ident, desc), logSector_(logSector), tcSort_(0) {
    }
    TriggerCrate(int ident, const char* desc, std::string const & _label, int logSector)
        : VmeCrate(ident, desc, _label), logSector_(logSector), tcSort_(0) {
    }
    virtual ~TriggerCrate() {
    }

    virtual const HardwareItemType& getType() const {
        return TriggerCrate::TYPE;
    }

    int getLogSector() {
        return logSector_;
    }

    TriggerBoard* getTb(int pos);
    std::list<TriggerBoard*>& getTriggerBoards();

    TTCSort* getTcSort();

    virtual void checkConfigId(ConfigurationSet* configurationSet);
    //virtual void configure(ConfigurationSet* configurationSet, int configureFlags);
};


}


#endif
