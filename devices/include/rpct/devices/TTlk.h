//---------------------------------------------------------------------------

#ifndef TTlkH
#define TTlkH
//---------------------------------------------------------------------------

#include "tb_ii.h"


namespace rpct {

class TTlk {
public:
  typedef IDevice::TID TID;
  enum TState { stIdle = 0, stCarrier, stNormal, stError };
private:
  TIIDevice& Owner;
  TID VECT_TLK;
  TID BITS_TLK_OPTO_SIG;
  TID BITS_TLK_ENABLE;
  TID BITS_TLK_LCK_REFN;
  TID BITS_TLK_LOOP_ENA;
  TID BITS_TLK_PRBS_ENA;
  TID BITS_TLK_RX_ERR;
  TID BITS_TLK_RX_DV;
  TID BITS_TLK_TEST_ENA;
  TID BITS_TLK_TX_ENA;
  TID BITS_TLK_TX_ERR;
  TID BITS_TLK_INV_CLK;
  TID WORD_TLK_RXDATA;
public:
  TTlk(TIIDevice& owner,
       TID vectTlk,
       TID bitTlkOptoSig,
       TID bitTlkEnable,
       TID bitTlkLckRefn,
       TID bitTlkLoopEna,
       TID bitTlkPrbsEna,
       TID bitTlkRxErr,
       TID bitTlkRxDv,
       TID bitTlkTestEna,
       TID bitTlkTxEna,
       TID bitTlkTxErr,
       TID bitTlkInvClk,
       TID wordTlkRxData)
    : Owner(owner),
      VECT_TLK(vectTlk),
      BITS_TLK_OPTO_SIG(bitTlkOptoSig),
      BITS_TLK_ENABLE(bitTlkEnable),
      BITS_TLK_LCK_REFN(bitTlkLckRefn),
      BITS_TLK_LOOP_ENA(bitTlkLoopEna),
      BITS_TLK_PRBS_ENA(bitTlkPrbsEna),
      BITS_TLK_RX_ERR(bitTlkRxErr),
      BITS_TLK_RX_DV(bitTlkRxDv),
      BITS_TLK_TEST_ENA(bitTlkTestEna),
      BITS_TLK_TX_ENA(bitTlkTxEna),
      BITS_TLK_TX_ERR(bitTlkTxErr),
      BITS_TLK_INV_CLK(bitTlkInvClk),
      WORD_TLK_RXDATA(wordTlkRxData) {}

  TState GetTxState();
  void SetTxState(TState state);
  TState GetRxState();

  void GetFlags(bool& enable, bool& lckRefn, bool& loopEna, bool& prbsEna)
    {
      uint32_t data = Owner.readVector(VECT_TLK);
      enable = Owner.getBitsInVector(BITS_TLK_ENABLE, data) == 1;
      lckRefn = Owner.getBitsInVector(BITS_TLK_LCK_REFN, data) == 1;
      loopEna = Owner.getBitsInVector(BITS_TLK_LOOP_ENA, data) == 1;
      prbsEna = Owner.getBitsInVector(BITS_TLK_PRBS_ENA, data) == 1;
    }
  void SetFlags(bool enable, bool lckRefn, bool loopEna, bool prbsEna)
    {
      uint32_t data = Owner.readVector(VECT_TLK);
      Owner.setBitsInVector(BITS_TLK_ENABLE, enable ? 1 : 0, data);
      Owner.setBitsInVector(BITS_TLK_LCK_REFN, lckRefn ? 1 : 0, data);
      Owner.setBitsInVector(BITS_TLK_LOOP_ENA, loopEna ? 1 : 0, data);
      Owner.setBitsInVector(BITS_TLK_PRBS_ENA, prbsEna ? 1 : 0, data);  
      Owner.writeVector(VECT_TLK, data);
    }
  uint32_t ReadRxData()
    {
      return Owner.readWord(WORD_TLK_RXDATA, 0);
    }
  void Init();
};

}

#endif
