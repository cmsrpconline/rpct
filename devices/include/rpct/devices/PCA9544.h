/** Filip Thyssen */

#ifndef rpct_devices_PCA9544_h_
#define rpct_devices_PCA9544_h_

#include "rpct/ii/i2c/Register.h"

namespace rpct {

class PCA9544Register : public rpct::i2c::TRegister<1, 0x00, 1, 0x00>
{
public:
    bool enabled(unsigned char channel) const;
    bool enabled() const;
    bool disabled(unsigned char channel) const;
    bool disabled() const;
    void enable(unsigned char channel = 0);
    void disable();
};

inline bool PCA9544Register::enabled(unsigned char channel) const
{
    return ((out_[0] & 0x07) == (0x04 | channel));
}
inline bool PCA9544Register::enabled() const
{
    return (out_[0] & 0x04);
}

inline bool PCA9544Register::disabled(unsigned char channel) const
{
    return !enabled(channel);
}
inline bool PCA9544Register::disabled() const
{
    return !enabled();
}

inline void PCA9544Register::enable(unsigned char channel)
{
    in_[0] = (0x04 | channel);
}

inline void PCA9544Register::disable()
{
    in_[0] = 0;
}

} // namespace rpct

#endif // rpct_devices_PCA9544_h_
