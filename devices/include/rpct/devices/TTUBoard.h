//---------------------------------------------------------------------------
//
//  Karol Bunkowski 2007
//  Warsaw University
//  Andres Osorio 2009
//  Universidad de los Andes
//
#ifndef RPCTTTUBOARD_H
#define RPCTTTUBOARD_H
//---------------------------------------------------------------------------

#include "tb_ii.h"
//#include "tb_bs.h"
#include "TIIABootContr.h"
#include "TIIJtagIO.h"
#include "TTlk.h"
#include "TTTCrxI2C.h"
#include "rpct/diag/TDiagCtrl.h"
#include "rpct/diag/TStandardDiagnosticReadout.h"
#include "rpct/diag/TPulser.h"
#include "rpct/devices/VmeCrate.h"
#include "rpct/devices/OptoSettings.h"//standard opto setting??
#include "rpct/devices/RbcSettings.h"//RBC settings should be implemented
#include "rpct/devices/ErrorCounterAnalyzer.h"
#include "rpct/i2c/TIII2CMasterCore.h"
#include "rpct/ii/BoardBase.h"
#include "rpct/ii/memmap.h"
#include <log4cplus/logger.h>
#include <boost/dynamic_bitset.hpp>
#include "rpct/std/ENotImplemented.h"
#include "rpct/devices/QPLL.h"

#define CII

namespace rpct {

//A copy of TriggerBoardDevice
class TTUBoardDevice: public TIIDevice, public IIVMEDevice {
protected:
	int VMEBoardBaseAddr;
	static log4cplus::Logger logger;
public:
	static void setupLogger(std::string namePrefix);
#define IID_FILE "RPC_system_def.iid"//bez zmian??
#include "iid2cdef1.h"

#define IID_FILE "RBC_system_def.iid"//dodatek do RPC_system_def.iid??
#include "iid2cdef1.h"

#define IID_FILE "RPC_TB3_def.iid"//bez zmian??
#include "iid2cdef1.h"


	TTUBoardDevice(int id,
			std::string name,
			TID name_id,
			std::string desc,
			const HardwareItemType& type,
			std::string ver,
			TID ver_id,
			TID checksum_id,
			uint32_t base_address,
			IBoard& board,
			int position,
			TMemoryMap* memmap,
			int vme_board_address)
	: TIIDevice(id, name, name_id, desc, type, ver, ver_id, checksum_id, base_address,
			board, position, memmap),
			VMEBoardBaseAddr(vme_board_address) {}

	virtual ~TTUBoardDevice() {}

	virtual int GetVMEBaseAddrShift()
	{ return 16; }

	virtual int GetVMEBoardBaseAddr()
	{ return VMEBoardBaseAddr; }
};



//A copy of TriggerBoardVme
class TTUBoardVme : public TTUBoardDevice {
private:
	TIIABootContr* ABootContr;
public:

#define IID_FILE "RPC_TB3_vme.iid"//bez zmian
#include "iid2c1.h"

	const static int II_ADDR_SIZE = 20;
	const static int II_DATA_SIZE = 32;
	const static HardwareItemType TYPE;

	TTUBoardVme(int vme_board_address, IBoard& board);
	virtual ~TTUBoardVme();


	TIIABootContr& GetABootContr() {
		if (ABootContr == 0) {
			ABootContr = new TIIABootContr(*this,
					VECT_ALTERA,
					BITS_ALTERA_NCONF,
					BITS_ALTERA_NSTATUS,
					BITS_ALTERA_CONFDONE,
					BITS_ALTERA_INITDONE,
					WORD_RESET,
					AREA_ALTERA_BOOT);
		}

		return *ABootContr;
	}

	virtual void reset() {
	}

	virtual void configure(DeviceSettings* settings, int flags) {
		reset();
	}
};

//A copy of TriggerBoardControl
class TTUBoardControl : public TTUBoardDevice, virtual public Monitorable  {
private:
	TIIABootContr* ABootContr;

	TIII2CMasterCore* I2C;
	TTTCrxI2C TTCrxI2C;
	QPLL qpll_;

public:

#ifdef CII
#include "CII_TB3_CTRL_iicfg_tab.h"
#else
#define IID_FILE "RPC_TB3_ctrl.iid"//bez zmian
#include "iid2c1.h"
	const static int II_ADDR_SIZE;
	const static int II_DATA_SIZE;
#endif

	const static HardwareItemType TYPE;


	TTUBoardControl(int vme_board_address, IBoard& board, int position);
	virtual ~TTUBoardControl();


	TIIABootContr& GetABootContr() {
		if (ABootContr == 0)
			ABootContr = new TIIABootContr(*this,
					VECT_ALTERA,
					BITS_ALTERA_NCONF,
					BITS_ALTERA_NSTATUS,
					BITS_ALTERA_CONFDONE,
					BITS_ALTERA_INITDONE,
					0,
					AREA_ALTERA_BOOT);
		return *ABootContr;
	}

	void ResetQPLL();
	void ResetTTC();
	void InitTTC();
	TTTCrxI2C& GetTTCrxI2C() {
		if (TTCrxI2C.GetI2CInterface() == 0) {
			TTCrxI2C.SetI2CInterface(GetI2C());
		}
		return TTCrxI2C;
	}

	void SetTTCrxI2CAddress(unsigned char addr);

	TIII2CMasterCore& GetI2C();

    bool checkResetNeeded();

	virtual void reset();

	virtual void configure(DeviceSettings* settings, int flags) {
		reset();
	}

    virtual void monitor(volatile bool *stop);
    virtual void monitor(MonitorItems& items);
};


//A copy of Opto from TriggerBoard, iid changed
class TTUOpto : public TTUBoardDevice, virtual public IDiagnosable, virtual public Monitorable {
public:
    //rather obsolete class
	class TOptLink {
	private:
		TTUOpto* opto_;
		int number_; //0, 1, 2

	public:
		TOptLink(TTUOpto* opto, int optLinkNum);

		virtual ~TOptLink();

		TTUOpto& GetOpto() {
			return *opto_;
		}

		void EnableOptoLink(bool enable);

	};

	std::vector<int> optLinkStatus_;

	//...this enum might move to some other class
	enum OptLinkStatus {
	        OFF = 0,
	        ON = 1, //after configure and before monitoring started
	        OK = 2,
	        WARNING = 3,
	        ERROR = 4,
	        RX_ERR =5
	};

	friend class TOptLink; //rather obsolete
	typedef std::vector<TOptLink> TOptLinks; //rather obsolete

private:
	boost::dynamic_bitset<> EnabledOptLinks;
	TOptLinks OptLinks; //rather obsolete

public:
#ifdef CII
#include "CII_TTU_opto_iicfg_tab.h"
#else
#define IID_FILE "RBC_opto.iid" //RPC_TB3_opto.iid->RBC_opto.iid
#include "iid2c1.h"
	const static int II_ADDR_SIZE;
	const static int II_DATA_SIZE;
#endif
	const static HardwareItemType TYPE;

	TTUOpto(int id, int vme_board_address, int ii_addr, const char* desc, IBoard& board, int position);
	virtual ~TTUOpto();

	static unsigned int GetOptLinksCount() {
		return 2ul;
	}

	virtual void reset();
	virtual void configure(DeviceSettings* settings, int flags);
	virtual void enable();

	void enableTransmissionCheck();
	void resetErrorCounters();

	//linkNum = 0...1 //rather obsolete
	void EnableOptoLink(int linkNum, bool enable) {
		OptLinks[linkNum].EnableOptoLink(enable);
	}

	void EnableOptoLink(uint32_t enable);

	boost::dynamic_bitset<>&  GetEnabledOptLinks() {
		return EnabledOptLinks;
	}

	//rather obsolete
	void setRecFastDelay(unsigned int linkNum, unsigned int delay);

	//rather obsolete
	void setRecDataDelay(unsigned int linkNum, unsigned int delay);

	//rather obsolete
	bool CheckLinksOperation();

	unsigned int getNumber() {
		return getPosition() - 2;
	}


    unsigned int getGlobalLinkNum(unsigned int localLinkNum) {
    	return localLinkNum + getNumber() * GetOptLinksCount();
    }

private:
	class OptoPulser : public TPulser {
	public:
		OptoPulser(IDiagnosable& owner, const char* name, IDiagCtrl& diagCtrl,
				TID areaMemPulse, TID wordPulserLength, TID bitsRepeatEna, TID bitsPulserOutEna):
					TPulser(owner, name, diagCtrl, areaMemPulse, wordPulserLength, bitsRepeatEna, bitsPulserOutEna)
					{};

					virtual void writeTarget(int target) {
						if (target > 1) {
							throw TException("TPulser::WriteTarget: target > 1");
						}
						GetOwner().writeBits(BITS_PULSER_LOOP_ENA, target);
					}
	};

	TDiagCtrl* DiagCtrl;
	TStandardDiagnosticReadout* DiagnosticReadout;
	TDiagCtrl* PulserDiagCtrl;
	TPulser* Pulser;

	TDiagCtrlVector DiagCtrls;
	TDiagVector Diags;

    ErrorCounterAnalyzer monRecErrorAnalyzer_;

public:
	TDiagCtrl& GetDiagCtrl();
	TStandardDiagnosticReadout& GetDiagnosticReadout();
	TDiagCtrl& GetPulserDiagCtrl();
	TPulser& GetPulser();

    virtual TDiagCtrlVector& GetDiagCtrls();
    virtual TDiagVector& GetDiags();
    virtual void ConfigureDiagnosable(TTriggerDataSel triggerDataSel, uint32_t dataTrgDelay);

    virtual void monitor(volatile bool *stop);
    virtual void monitor(MonitorItems& items);
};

typedef std::vector<TTUOpto*> TTUOptos;

//A copy of Pac from TriggerBoard, iid changed
class TTUTrig : public TTUBoardDevice, virtual public IDiagnosable , virtual public Monitorable {
private:

public:
#ifdef CII
#include "CII_TTU_trig_iicfg_tab.h"
#else
#define IID_FILE "RBC_trig.iid" //RPC_TB3_ldpac.iid->RBC_trig.iid
#include "iid2c1.h"
	const static int II_ADDR_SIZE;
	const static int II_DATA_SIZE;
#endif

    const static HardwareItemType TYPE;

	TTUTrig(int id, int vme_board_address, int ii_addr, const char* desc, IBoard& board,
			int position);
	virtual ~TTUTrig();

	virtual void reset();
	virtual void configure(DeviceSettings* settings, int flags);
	virtual void enable();

	//number of PAC/TTUTrig slot on the TB (0...3)
	unsigned int getNumber() {
		return getPosition() - 8;
	}

private:
	TDiagCtrl* DiagCtrl;
	TStandardDiagnosticReadout* DiagnosticReadout;

	TDiagCtrlVector DiagCtrls;
	TDiagVector Diags;

    ErrorCounterAnalyzer monRecErrorAnalyzer_;

public:
	TDiagCtrl& GetDiagCtrl();
	TStandardDiagnosticReadout& GetDiagnosticReadout();

    virtual TDiagCtrlVector& GetDiagCtrls();
    virtual TDiagVector& GetDiags();
    virtual void ConfigureDiagnosable(TTriggerDataSel triggerDataSel, uint32_t dataTrgDelay);

    virtual void monitor(volatile bool *stop);
    virtual void monitor(MonitorItems& items);

    void resetErrorCounters();
};

typedef std::vector<TTUTrig*> TTUTrigs;

class TTUBoard: public BoardBase, virtual public IBoard {
private:
	static log4cplus::Logger logger;
	TVMEInterface* vme_;
	TTUBoardVme ttuBoardVme_;
	TTUBoardControl ttuBoardControl_;

	int vmeAddress_;

	TTUOptos optos_;
	TTUTrigs ttuTrigs_;

	boost::dynamic_bitset<> usedOptos_;
	boost::dynamic_bitset<> usedTTUTrigs_;


	void Init(bool use_vme);

public:
	static void setupLogger(std::string namePrefix);
	const static HardwareItemType TYPE;

	//SOMETHING to CONF-MONITOR? here

	TTUBoard(int ident, const char* desc, TVMEInterface* vme, int vme_board_address, VmeCrate* crate, int pos);
	virtual ~TTUBoard();

	TTUBoardVme& GetVme() {
		return ttuBoardVme_;
	}

	TTUBoardControl& GetControl() {
		return ttuBoardControl_;
	}

	virtual IDevice& addChip(std::string type, int pos, int id);

	//the number of TB in the TC backplane slot (0...8)
	unsigned int getNumber() {
		return getPosition() - 1;
	}

	int GetVMEAddress() {
		return vmeAddress_;
	}

	TTUOptos& GetOptos() {
		return optos_;
	}

	TTUTrigs& GetTTUTrigs() {
		return ttuTrigs_;
	}

	void Reset();

	bool CheckLinksOperation();

	//link num = 0...17
	void EnableOptLink(int linkNum, bool enable);

	boost::dynamic_bitset<>& GetUsedTTUTrigs() {
		return usedTTUTrigs_;
	}

	boost::dynamic_bitset<>& GetUsedOptos() {
		return usedOptos_;
	}

	//virtual void init();
	virtual void configure(ConfigurationSet* configurationSet, int configureFlags);

	//* self Test
	virtual void selfTest() {

	}

	void resetErrorCounters();
};

}


#endif
