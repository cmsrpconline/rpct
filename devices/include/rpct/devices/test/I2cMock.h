#ifndef _RPCTTI2CMOCK_H_
#define _RPCTTI2CMOCK_H_

namespace rpct {
class I2cMock : public TI2CInterface {  
private:
	unsigned int randqd1() 
	{ 
		static unsigned long u; 
		return (u = u*1664525u + 1013904223u); 
	}
public:
    I2cMock() { 
    }
    
    virtual ~I2cMock() {}

    virtual void Write8(unsigned char address, unsigned char value) {
    }
    
    virtual unsigned char Read8(unsigned char address, bool ack = true)  {
        return randqd1();
    }


    virtual void WriteADD(unsigned char address, unsigned char value1,
                        unsigned char value2) {
    }
                        
    virtual void ReadADD(unsigned char address, unsigned char& value1,
                        unsigned char& value2) {
        value1 = randqd1();
        value2 = randqd1();
    }

    virtual void Write(unsigned char address, unsigned char* data, size_t count) {
    }
    /*virtual void Write32 (unsigned char address, unsigned long int  value)  = 0;
    virtual void Write16 (unsigned char address, unsigned short int value)  = 0;

    virtual void Read (unsigned char address, char * data, size_t count)  = 0;
    virtual unsigned long  Read32 (unsigned char address)  = 0;
    virtual unsigned short Read16 (unsigned char address)  = 0;*/
};


}

#endif /*_TI2CMOCK_H_*/
