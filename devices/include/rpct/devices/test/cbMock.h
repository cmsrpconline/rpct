//---------------------------------------------------------------------------
    
//
//  Michal Pietrusinski 2001
//  Warsaw University
//

#ifndef cbMockH
#define cbMockH
//--------------------------------------------------------------------------- 
#include "rpct/devices/ICB.h"
#include "rpct/devices/LinkBox.h"
#include "rpct/devices/FebInstanceManager.h"
#include "rpct/devices/test/I2cMock.h"
#include "rpct/ii/BoardBase.h"  
#include "rpct/devices/cb.h"



namespace rpct {


class TCBMock: public BoardBase, virtual public ICB {
private:
  	I2cMock i2c;

    FebInstanceManager febInstanceManager;
public:
  	TCBMock(int ident, const char* desc, LinkBox* lbox)
  	: BoardBase(ident, desc, TCB::TYPE, lbox, -1) {
  	}
  
  	virtual ~TCBMock() {
  	}
  
  	TI2CInterface& getI2C() {
        return i2c;
   	}

  	virtual void initI2C() {

  	}

  	void selectI2CChannel(int channel) {         
        G_Log.out() << description_ << ": selecting " << channel << " channel" << std::endl; 
    }    
    
    virtual IDevice& addChip(std::string type, int pos, int id) {
        throw ENotImplemented("TCBMock::addChip: not implemented");
    }
    
    virtual Feb& getFeb(int channel, unsigned int localNumber) {
        return febInstanceManager.getFeb(channel, localNumber, &getI2C());
    }
    
    virtual FebEndcap& getFebEndcap(int channel, unsigned int localNumber) {
        return febInstanceManager.getFebEndcap(channel, localNumber, &getI2C());
    }
    
    virtual void init() {
    }
    
    virtual void selfTest() {
    }
};

}

#endif
