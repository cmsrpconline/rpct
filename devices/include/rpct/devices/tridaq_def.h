//---------------------------------------------------------------------------

#ifndef tridaq_defH
#define tridaq_defH
//---------------------------------------------------------------------------

#include "tb_ii.h"

namespace rpct {
              
// predefined constants of clock and time
const TN CLK_FREQ = 40000000;   // frequency = 40 MHz
const TN LED_PULSE_FREQ = 1;	// repeat time = 1s
const TN LED_PULSE_WIDTH = CLK_FREQ / 10; // pulse time = 100ms

// predefined constants of links compressions
const TVL DELAY_PIPELINE_SIZE	   = 7;	// delay pileline length maximum
const TVL TIME_PARTITION_CODE_SIZE = 4;	// time latency bus width  in bits
const TVL DATA_PARTITION_CODE_SIZE = 4;	// partition number bus width in bits
const TVL DATA_PARTITION_SIZE 	   = 8; // data bus width in bits
const TVL CHAMBER_ADDRESS_SIZE	   = 2;	// chamber number bus width in bits
const TVL TIME_PARTITION_CODE_MAX  = 7; // maximum: 2^TIME_PARTITION_CODE_SIZE-1
const TVL DATA_PARTITION_CODE_MAX  = 11;// maximum:	2^DATA_PARTITION_CODE_SIZE-1
const TVL TRIGGER_PAGE_CODE_SIZE   = 6;	// page bus width in bits
const TVL VME_ADDRESS_SIZE	  	   = 16;// VME address bus width in bits
const TVL VME_DATA_SIZE		       = 32;// VME address bus width in bits

// predefined consts of link board data format
const TVL LB_BCN_SIZE  = 6;	// size of BCN less bits
const TVL SYN_PIPE_MAX = 6;	// size of BCN auto-synchro pipeline

// predefined consts of Master DAQ board
const TVL MDAQ_ADDR_SLAVE_SIZE = 8;	// size of Slave DAQ address board
const TVL SDAQ_STATUS_SIZE	   = 4;	// size of Slave DAQ status word

// predefined contants of pac board
const TN PAC_BOARD_NUM        = 5;
const TN GHOST_BUSTER_OUT_NUM = 4;

// predefined consts of Internal Interface
const TVL II_ADDR_SIZE			= 16;
const TVL II_ADDR_BASE_SIZE		=  4;
const TVL II_DATA_SIZE			= 16;
const TN  II_CONTROL_BASE_ADDR	=  0;
const TN  II_JTAG_BASE_ADDR		=  1;
const TN  II_PAC0_BASE_ADDR		=  2;	// range from 2 to 6
const TN  II_GB_BASE_ADDR		=  7;
const TN  II_LDEMUX0_BASE_ADDR	=  8;	// range from 8 to 15
const TN  LDEMUX_OUT_SIZE		= 64;
const TVL IILB_ADDR_SIZE		= 14;
const TVL FEB_TEST_DATA		    = 24;
const TVL LB_GOL_DATA			= 32;
//
// Controller board
//
// predefined consts of Altera boot loader
const TVL ALT_PGM_DATA_SIZE		= 15;

// predefined consts of TTC
const TVL TTC_BROADCAST1_DATA_SIZE = 4;
const TVL TTC_BROADCAST2_DATA_SIZE = 2;

// delay control bus
const TVL DELAY_DATA_SIZE		   = 6;


//
// abrreviations
//

const TVL LDEMUX_DATA_OUT        = (DATA_PARTITION_CODE_MAX+1)*DATA_PARTITION_SIZE;
const TVL DPM_DATA_SIZE 	     = DATA_PARTITION_CODE_SIZE+DATA_PARTITION_SIZE+CHAMBER_ADDRESS_SIZE+2;
const TVL DPM_ADDR_SIZE 		 = DATA_PARTITION_CODE_SIZE+TRIGGER_PAGE_CODE_SIZE;
const TVL LINK_DATA_SIZE         = TIME_PARTITION_CODE_SIZE+DPM_DATA_SIZE;
const TVL DATA_COUNTER_CODE_SIZE = TVLcreate(TIME_PARTITION_CODE_MAX+1);
const TVL LB_SYNCHRO_DATA        = (LB_BCN_SIZE+LINK_DATA_SIZE+4+(1))/2;
const TVL II_ADDR_USER_SIZE		 = II_ADDR_SIZE-II_ADDR_BASE_SIZE;
const TVL DPM_DELAY_ADDR_SIZE    = DELAY_PIPELINE_SIZE+SLVPartAddrExpand(LINK_DATA_SIZE,II_DATA_SIZE);
const TVL DPM_DELAY_DATA_SIZE    = minimum(LINK_DATA_SIZE,II_DATA_SIZE);
const TVL DPM_DATA_ADDR_SIZE     = DPM_ADDR_SIZE+SLVPartAddrExpand(DPM_DATA_SIZE,II_DATA_SIZE);
const TVL DPM_DATA_DATA_SIZE     = minimum(DPM_DATA_SIZE,II_DATA_SIZE);
const TVL DPM_CNT_ADDR_SIZE		 = TRIGGER_PAGE_CODE_SIZE+SLVPartAddrExpand(DATA_COUNTER_CODE_SIZE,II_DATA_SIZE);
const TVL DPM_CNT_DATA_SIZE		 = minimum(DATA_COUNTER_CODE_SIZE,II_DATA_SIZE);
const TN LDEMUX_OUTH_SIZE		 = LDEMUX_OUT_SIZE/2;
const TP SYN_PIPE_SIZE		     = TVLcreate(SYN_PIPE_MAX);
const TN GHOST_BUSTER_CHAN_NUM   = PAC_BOARD_NUM;


// bit slaces for link
const TN GL_ITEM_HALF_PART = 0;
const TN GL_ITEM_END_DATA  = GL_ITEM_HALF_PART+1;
const TN GL_ITEM_CHAMBER   = GL_ITEM_END_DATA+1;
const TN GL_ITEM_DATA	   = GL_ITEM_CHAMBER+1;
const TN GL_ITEM_PARTITION = GL_ITEM_DATA+1;
const TN GL_ITEM_TIME	   = GL_ITEM_PARTITION+1;
const TN GL_ITEM_NUM	   = GL_ITEM_TIME+1;
const TN GLA_ITEM_BCN	   = GL_ITEM_NUM;
const TN GLA_ITEM_BCN0	   = GLA_ITEM_BCN+1;
const TN GLA_ITEM_PARITY   = GLA_ITEM_BCN0+1;
const TN GLA_ITEM_NUM      = GLA_ITEM_PARITY+1;

}

#endif
