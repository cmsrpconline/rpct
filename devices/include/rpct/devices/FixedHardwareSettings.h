#ifndef FIXEDHARDWARESETTINGS_H_
#define FIXEDHARDWARESETTINGS_H_

namespace rpct {

class FixedHardwareSettings {
public:
/*	static const unsigned int OPTO_BC0_DELAY = 65;
	static const unsigned int PAC_BC0_DELAY;
	static const unsigned int TBSORT_BC0_DELAY;
	static const unsigned int TCSORT_BC0_DELAY;
	static const unsigned int HSB_BC0_DELAY;
	static const unsigned int FSB_BC0_DELAY;

	static const unsigned int OPTO_TTC_DATA_DELAY;
	static const unsigned int PAC_TTC_DATA_DELAY;
	static const unsigned int TBSORT_TTC_DATA_DELAY;*/

    static const unsigned int TTUOPTO_BC0_DELAY = 0x32; //fo the max fibers lenght as in W+2 near
    static const unsigned int TTUTRIG_BC0_DELAY = TTUOPTO_BC0_DELAY + 5;


	static const unsigned int OPTO_BC0_DELAY = 27 + 6 + 2 + 10 -1;
	//static const unsigned int OPTO_BC0_DELAY = 8; //PASTEURA
	//static const unsigned int OPTO_BC0_DELAY = 10; //904
	static const unsigned int PAC_BC0_DELAY = OPTO_BC0_DELAY + 5;
	static const unsigned int RMB_BC0_DELAY = PAC_BC0_DELAY; // + 2 for new RMB firmware handling properly the overlap events, 2011 03 16
	static const unsigned int TBSORT_BC0_DELAY = PAC_BC0_DELAY + 6;
	static const unsigned int TCSORT_BC0_DELAY = TBSORT_BC0_DELAY + 9 + 3 -1;
	static const unsigned int HSB_BC0_DELAY = TCSORT_BC0_DELAY + 5 +2;
	//+1 goes together with the REC_DATA_DELAY= 0x5555555 to compensate the HSB lower latency (i.e. 2 BX) in the the firmware  Aug  9 17:16 cii_sc_hsort.svf checksum 0x8948
	//+2 goes together with the REC_DATA_DELAY= 0xaaaaaaa to compensate the PAC lower latency in the the firmware 0x1049, done on July  5 2015 7:56

	static const unsigned int FSB_BC0_DELAY = 3522; //should be good on the GMT now

	//these delays may change after firmware recompilation!
	static const unsigned int TBCONTROL_TTC_DATA_DELAY = 7;
	static const unsigned int OPTO_TTC_DATA_DELAY = 3;
	static const unsigned int PAC_TTC_DATA_DELAY = 2;
	static const unsigned int RMB_TTC_DATA_DELAY = 3;
	static const unsigned int TBSORT_TTC_DATA_DELAY = 3;

	//it is taken from the DB now, so this number does not matter
	static const unsigned int RMB_DATA_DELAY = 84 - 2; //for new RMB firmware handling properly the overlap events, 2011 03 16

	static const unsigned int TCSORT_TRG_DELAY = 10;

	static const unsigned int PT_CODE_MAX = 31;
	static const unsigned int QUALITY_MAX = 7;

	static const unsigned int LB_TTC_RX_CONTROL_REG = 0x9b;

	static const unsigned int CONFIGURE_FROM_FLASH_TIME = 500000;//us
};

}
#endif /*FIXEDHARDWARESETTINGS_H_*/
