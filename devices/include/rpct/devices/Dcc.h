#ifndef RPCT_DCC_H
#define RPCT_DCC_H

#include "tb_ii.h" 
#include "rpct/ii/BoardBase.h"

#include "vme/TVMEInterface.h"
#include "rpct/devices/VDcc_pkgDef.h"
#include "rpct/devices/VmeCrate.h"
#include "rpct/devices/DummyVMEDevice.h"
#include "rpct/devices/DccSettings.h"
#include "rpct/devices/DccMonitorables.h"
#include "rpct/devices/DccDevice.h"
#include "rpct/ii/memmap.h"
#include "rpct/std/ENotImplemented.h"  
#include <log4cplus/logger.h>
// #include <vector>

namespace rpct {

//---------------------------------------------------------------------------

class Dcc: public BoardBase, virtual public IBoard {
private:
    static log4cplus::Logger logger;
    TVMEInterface* vme_;
    int vmeAddress_;
    int preTrigger_, postTrigger_;
    DccDevice dccDevice;
    void Init();

public:
    static void setupLogger(std::string namePrefix);
    const static HardwareItemType TYPE; 
    Dcc(int ident, const char* desc, TVMEInterface* vme, int vme_board_address, VmeCrate* crate, int pos);
    virtual ~Dcc();

    int getVmeAddress() { return vmeAddress_; }

    virtual IDevice& addChip(std::string type, int pos, int id) { throw ENotImplemented("Dcc::addChip"); }

    DccDevice& getDccDevice(){ return dccDevice; }

    virtual void reset();

    virtual void initialize();

    virtual void configure(DeviceSettings* settings);

    virtual void run();

    virtual void start(DeviceSettings* settings);

    void setBXRange(unsigned int pre, unsigned int post);

    void forceTTS(unsigned int status);

    void freeTTS();

    int checkTTS();

    DccMonitorables checkBoard();

// virtual void configure(ConfigurationSet* configurationSet, int configureFlags) { }

    virtual void selfTest() { }

};

}

#endif
