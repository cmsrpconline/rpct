/** Filip Thyssen */

#ifndef rpct_devices_IFebChipConfiguration_h_
#define rpct_devices_IFebChipConfiguration_h_

#include "rpct/ii/DeviceSettings.h"

namespace rpct {

class IFebChipConfiguration : public virtual DeviceSettings
{
public:
    IFebChipConfiguration();
    virtual ~IFebChipConfiguration();

    // DeviceSettings
    HardwareItemType getDeviceType();

    virtual void setId(int id) = 0;
    virtual int getId() const = 0;

    virtual void setVTH(unsigned int vth) = 0;
    virtual void setVMON(unsigned int vmon) = 0;
    virtual void setVTHOffset(int vth_offset) = 0;
    virtual void setVMONOffset(int vmon_offset) = 0;

    virtual unsigned int getVTH() const = 0;
    virtual unsigned int getVMON() const = 0;
    virtual int getVTHOffset() const = 0;
    virtual int getVMONOffset() const = 0;
protected:
    static HardwareItemType * type_;
};

} // namespace rpct

#include "rpct/devices/IFebChipConfiguration-inl.h"

#endif // rpct_devices_IFebChipConfiguration_h_
