#ifndef RPCTLINKBOX
#define RPCTLINKBOX

#include <memory>
#include <vector>
#include <list>
#include <set>
#include "tb_vme.h"
#include "tb_ii.h"
#include "rpct/lboxaccess/StdLinkBoxAccess.h"
#include "rpct/devices/Crate.h"
#include "rpct/devices/ICB.h"
#include "rpct/devices/Rbc.h"
#include <log4cplus/logger.h>

namespace rpct {

class LinkBoard;
class MasterLinkBoard;
class SlaveLinkBoard;
class Rbc;
class LinkBox;
class SynCoder;
class LinkSystem;

class HalfBox {
public:
    typedef std::vector<MasterLinkBoard*> MasterLinkBoards;
    typedef std::vector<LinkBoard*> LinkBoards;
private:
    LinkBox& linkBox;
    StdLinkBoxAccess& lboxAccess;
    ICrate::Boards boards;
    ICB& cb;
    MasterLinkBoards masterLinkBoards;
    LinkBoards linkBoards;
    Rbc* rbc;
    static log4cplus::Logger logger;

    unsigned int monitoringIteration;

public:
    static void setupLogger(std::string namePrefix);
    HalfBox(LinkBox& lbox, StdLinkBoxAccess& lboxAcc, ICB& contrb)
            : linkBox(lbox), lboxAccess(lboxAcc), cb(contrb), rbc(0), monitoringIteration(0) {
    }

    virtual ~HalfBox();

    void addBoard(IBoard& board);

    LinkBox& getLinkBox() {
        return linkBox;
    }

    ICB& getCb() {
        return cb;
    }
    Rbc* getRbc() {
        return rbc;
    }

    StdLinkBoxAccess& getLBoxAccess() {
        return lboxAccess;
    }

    MasterLinkBoards& getMasters() {
        return masterLinkBoards;
    }

    LinkBoards& getLinkBoards() {
        return linkBoards;
    }
    LinkBoard* getLinkBoardByPosition(int pos);
    IBoard* getBoardByPosition(int pos);

    //0 to 11, calculated based on the ccuAddress
    unsigned int getNumberInCCUring();

    /**
     * Disables the TTC Hard Reset triggered firmware reloading,
     * initializes the CB and LB I2C if needed (if the firmware was reloaded)
     * should be called on the beginning of the monitoring process of given half box or link boards
     */
    bool prepareAccess();

    /**
     * enables the TTC Hard Reset triggered firmware reloading
     * should be called on the end of the monitoring process of given half box or link boards
     */
    bool finaliseAccess();

    void monitor(volatile bool *stop);

    virtual void enable() {
    	monitoringIteration = 0;
    }
};

class HalfBoxAccessHandler
{
public:
    HalfBoxAccessHandler(HalfBox & _halfbox, bool _prepare = true);
    HalfBoxAccessHandler(HalfBoxAccessHandler const & _halfboxaccesshandler);
    ~HalfBoxAccessHandler();

    bool initiatedAccess() const;
    bool prepare();
    bool finalise();

protected:
    HalfBox & halfbox_;
    mutable bool initiated_access_;
};

class LinkBox : public Crate {
public:
    friend class HalfBox;
    const static HardwareItemType TYPE;
private:
    LinkSystem* linkSystem_;

    HalfBox* halfBox10;
    HalfBox* halfBox20;

    static log4cplus::Logger logger;
protected:
    //virtual bool checkResetNeeded();
    virtual bool checkWarm(ConfigurationSet* configurationSet);
    virtual void rememberConfiguration(ConfigurationSet* configurationSet);
public:
    static void setupLogger(std::string namePrefix);
    LinkBox(int ident, const char* desc, LinkSystem* linkSystem, StdLinkBoxAccess* lboxaccess10, StdLinkBoxAccess* lboxaccess20,
            int cb10Id, int cb20Id, bool mock, std::string const & _label = std::string(""));
    virtual ~LinkBox();

    virtual const HardwareItemType& getType() const {
        return LinkBox::TYPE;
    }

   // virtual void checkVersions();

    LinkSystem* getLinkSystem() {
    	return linkSystem_;
    }

    HalfBox* getHalfBox10() {
        return halfBox10;
    }

    HalfBox* getHalfBox20() {
        return halfBox20;
    }

    HalfBox* getHalfBox(int boardPos) {
        return (boardPos <= 10) ? halfBox10 : halfBox20;
    }

    LinkBoard& addLinkBoard(int pos, int id, const char* desc, int syncoderId);

public:
    IBoard* getBoardByPosition(int pos);
    static bool boardSortPredicate(rpct::IBoard *b1, rpct::IBoard *b2);

    virtual void monitor(volatile bool *stop){
    	if(halfBox10) {
    		halfBox10->monitor(stop);
    	}

    	if(halfBox20){
    		halfBox20->monitor(stop);
    	}
    }

    virtual void enable() {
    	if(halfBox10) {
    		halfBox10->enable();
    	}
    	if(halfBox20) {
    		halfBox20->enable();
    	}
    	Crate::enable();
    }
};

}



#endif
