#ifndef RPCTDEVICEMONITORABLES_H
#define RPCTDEVICEMONITORABLES_H

#include "rpct/ii/HardwareItemType.h"
#include <boost/shared_ptr.hpp>

namespace rpct {

class DeviceMonitorables {
public:
    virtual ~DeviceMonitorables() {
    }
    
    virtual HardwareItemType getDeviceType() = 0;
};

typedef boost::shared_ptr<DeviceMonitorables> DeviceMonitorablesPtr;

}

#endif
