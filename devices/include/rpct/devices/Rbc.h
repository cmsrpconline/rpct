//---------------------------------------------------------------------------

//
//  Karol Bunkowski 2009
//  Warsaw University
//

#ifndef rpctrbcH
#define rpctrbcH
//---------------------------------------------------------------------------
#include "ICB.h"
#include "rpct/devices/FebInstanceManager.h"
#include "rpct/std/ENotImplemented.h"
#include "rpct/ii/BoardBase.h"
#include "rpct/devices/LinkBox.h"
#include "rpct/ii/Monitorable.h"
#include <log4cplus/logger.h>
#include "rpct/devices/ErrorCounterAnalyzer.h"
#include <sstream>

namespace rpct {

class LinkBox;
class HalfBox;  // MC, 20 Feb 2010
class Rbc;

class RbcDevice: virtual public IHardwareItem, virtual public Monitorable {
private:
    static log4cplus::Logger logger;

    int id_;

    const int i2cId_;

    std::string description_;

    std::string rbcName_;

    unsigned int channelMask_;

    Rbc* rbc_;

    ICB* cb_;

    static const char* MON_STATUS_REG;
    static const char* MON_BCO_oddA_LATCH;
    static const char* MON_BCO_oddB_LATCH;
    static const char* MON_BCO_evenA_LATCH;
    static const char* MON_BCO_evenB_LATCH;
    static const char* MON_BCO_oddA_ERR;
    static const char* MON_BCO_oddB_ERR;
    static const char* MON_BCO_evenA_ERR;
    static const char* MON_BCO_evenB_ERR;

    TI2CInterface& prepareI2C();
public:
    static void setupLogger(std::string namePrefix);
    const static HardwareItemType TYPE;

    //addresses of the RBC Monitor registers
    const static uint8_t WORD_STATUS_REG;
    const static uint8_t WORD_BCO_oddA_LATCH;
    const static uint8_t WORD_BCO_oddB_LATCH;
    const static uint8_t WORD_BCO_evenA_LATCH;
    const static uint8_t WORD_BCO_evenB_LATCH;
    const static uint8_t WORD_BCO_oddA_ERR;
    const static uint8_t WORD_BCO_oddB_ERR;
    const static uint8_t WORD_BCO_evenA_ERR;
    const static uint8_t WORD_BCO_evenB_ERR;

    //addreses of the RBC Configuration registers
    const static uint8_t WORD_REC_CONF;
    const static uint8_t WORD_REC_CONFIG_IN;
    const static uint8_t WORD_REC_MAJ_REG;
    const static uint8_t WORD_REC_CONFIG_VER;
    const static uint8_t WORD_CTRL;
    const static uint8_t WORD_GOL;
    const static uint8_t WORD_QPLL;
    const static uint8_t WORD_SHAPE;
    const static uint8_t WORD_MASK_ODD_A;
    const static uint8_t WORD_MASK_ODD_B;
    const static uint8_t WORD_MASK_EVEN_A;
    const static uint8_t WORD_MASK_EVEN_B;

    const static uint8_t WORD_PROM_REG;

    ErrorCounterAnalyzer monRecErrorAnalyzer_;

    RbcDevice(int id, IBoard& board, ICB* cb);

    virtual ~RbcDevice() {

    }

    virtual int getId() const {
        return id_;
    }

    virtual const std::string& getDescription() const {
        return description_;
    }

    virtual const HardwareItemType& getType() const {
        return TYPE;
    }

    uint8_t read(uint8_t address);

    void write(uint8_t address, uint8_t data);

    bool checkResetNeeded(); //this function probably will not be needed

    void reset();

    void enable();

    void configure(DeviceSettings* settings, int configureFlags);

    void checkConfiguration(ConfigurationSet* configurationSet);

    virtual void monitor(volatile bool *stop);
    virtual void monitor(MonitorItems& items);

	void synchronizeLink();

    void setName( const std::string & );

    virtual Rbc* getRbc() {
        return rbc_;
    }

    virtual ICB* getCb() {
        return cb_;
    }
};

class Rbc: virtual public BoardBase, virtual public Monitorable  {
private:
    static log4cplus::Logger logger;

    RbcDevice rbcDevice_;

    //ICB* cb_;
    //add if needed
    //LinkBox* linkBox_;
    //HalfBox* halfBox_;

public:
    static void setupLogger(std::string namePrefix);
    const static HardwareItemType TYPE;

    Rbc(int id, std::string description, LinkBox* lbox, int position, ICB* cb, int rbcId);

    virtual ~Rbc();

    virtual IDevice& addChip(std::string type, int pos, int id) {
        throw ENotImplemented("TCB::addChip: not implemented");
    }

    virtual void init() {
    }

    virtual void selfTest() {
    }

    virtual bool checkResetNeeded(); //this function probably will not be needed

    void reset();

    virtual void enable();

    virtual void configure(ConfigurationSet* configurationSet, int configureFlags);

    RbcDevice& getRbcDevice() {
        return rbcDevice_;
    }

    void synchronizeLink() {
    	rbcDevice_.synchronizeLink();
    }

    virtual void monitor(volatile bool *stop) {
    	rbcDevice_.monitor(stop);
    };

    virtual void monitor(MonitorItems& items) {
    	rbcDevice_.monitor(items);
    };

    virtual const MonitorItems& getMonitorItems() {
    	return rbcDevice_.getMonitorItems();
    };

    virtual MonitorStatusList& getWarningStatusList() {
    	return rbcDevice_.getWarningStatusList();
    };

    ///////static Rbc rbc; //only to check compilation, remove

    static bool parseLBoxName( const std::string & name , int & sector );

    virtual HalfBox* getHalfBox();

    /**
     * checks the configId value in the WORD_REC_CONFIG_VER and if WORD_PROM_REG = 0x80,
     * i.e. if RBC has  automatic load enabled
     * return true if is OK
     */
    bool checkConfiguration(DeviceSettings* settings);

    void putConfigurationToProm(DeviceSettings* settings);

    /**
     * if force == false, calls checkConfiguration and updates the PROM only if checkConfiguration returns false
     * if force == true just updates the PROM
     *
     * returns true iff prom has been updated
     */
    bool putConfigurationToProm(ConfigurationSet* configurationSet, bool force);
};

class EI2CRbc : public EI2C {
private:
    Rbc* rbc_;
public:
    EI2CRbc(const std::string& msg, Rbc* rbc) throw()
    : EI2C(" on: " +  rbc->getDescription() + " " + msg), rbc_(rbc) {}
    EXCFASTCALL virtual ~EI2CRbc() throw() {}

    Rbc* getRbc() {
        return rbc_;
    }
};
}

#endif
