#ifndef RPCTRMBBXDATA_H_
#define RPCTRMBBXDATA_H_

#include "rpct/diag/IDiagnosticReadout.h"
#include "rpct/devices/StandardBxData.h"

namespace rpct {
    

class RmbBxData : public StandardBxData {    
public:
    RmbBxData(IDiagnosticReadout::BxData& bxData);
};

class RmbBxDataFactory : virtual public IDiagnosticReadout::ConvertedBxDataFactory {
private:
    static RmbBxDataFactory instance_;
    RmbBxDataFactory() {
    }
    
public:    
    virtual IDiagnosticReadout::ConvertedBxData* create(IDiagnosticReadout::BxData& bxData, IDiagnosticReadout& ownerReadout) {
        return new RmbBxData(bxData);
    }       
    
    static RmbBxDataFactory& getInstance() {
        return instance_;
    }
    
};

}

#endif 
