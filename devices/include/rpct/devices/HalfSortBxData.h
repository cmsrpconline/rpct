#ifndef RPCTTHALFSORTDATA_H_
#define RPCTTHALFSORTDATA_H_

#include "rpct/devices/StandardBxData.h"

#include "rpct/diag/IDiagnosticReadout.h"
#include "rpct/devices/LmuxBxData.h"

namespace rpct {
    

class HalfSortBxData : public StandardBxData {
public:
	HalfSortBxData( IDiagnosticReadout::BxData& bxData, IDiagnosticReadout& ownerReadout);
};

class HalfSortBxDataFactory : virtual public IDiagnosticReadout::ConvertedBxDataFactory {
private:
    static HalfSortBxDataFactory instance_;
    HalfSortBxDataFactory() { }

public:
    virtual IDiagnosticReadout::ConvertedBxData* create(IDiagnosticReadout::BxData& bxData, IDiagnosticReadout& ownerReadout) {
        return new HalfSortBxData(bxData, ownerReadout);
    }

    static HalfSortBxDataFactory& getInstance() { return instance_; }
};


}

#endif 
