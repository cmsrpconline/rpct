#ifndef VMECRATE_H
#define VMECRATE_H

#include <vector>
#include "rpct/std/IllegalArgumentException.h"
#include "tb_vme.h"
#include "tb_ii.h"
#include "rpct/devices/Crate.h"
#include <log4cplus/logger.h>



namespace rpct {

class VmeCrate: public Crate {
private:
	static log4cplus::Logger logger;
    TVMEInterface* vme_;
    typedef std::vector<IBoard*> BoardVector;
    BoardVector boardVector_;
protected:
    virtual void rememberConfiguration(ConfigurationSet* configurationSet);
public:
    static void setupLogger(std::string namePrefix);
    const static HardwareItemType TYPE;

    VmeCrate(int ident, const char* desc, std::string const & _label = std::string(""));
    virtual ~VmeCrate();

    TVMEInterface* getVme() {
        return vme_;
    }

    void setVme(TVMEInterface* vme);

    void setDefaultVme();

    virtual const HardwareItemType& getType() const {
        return VmeCrate::TYPE;
    }

    virtual IBoard* getBoard(int position) {
        if (position < 1 || position >= (int)boardVector_.size()) {
            throw IllegalArgumentException("Invalid position " + toString(position));
        }
        return boardVector_[position];
    }

    virtual void addBoard(IBoard& board);
};


}


#endif
