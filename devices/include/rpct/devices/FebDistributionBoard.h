/** Filip Thyssen */

#ifndef rpct_devices_FebDistributionBoard_h_
#define rpct_devices_FebDistributionBoard_h_

#include <map>

#include <log4cplus/logger.h>

#include "rpct/devices/FebSystemItem.h"
#include "rpct/ii/HardwareItemBase.h"
#include "rpct/ii/Configurable.h"
#include "rpct/ii/Monitorable.h"

#include "rpct/ii/exception/Exception.h"

#include "rpct/ii/i2c/I2cResponse.h"

#include "rpct/devices/feb_const.h"
#include "rpct/devices/PCA9544.h"
#include "rpct/devices/FebBoard.h"
#include "rpct/devices/RecursiveAction.h"

namespace rpct {

namespace i2c {
class II2cAccess;
} // namespace i2c

class FebSystem;
class IFebAccessPoint;

class FebDistributionBoard
    : public FebSystemItem
    , public Configurable
    , public IMonitorable
    , public IRecursive
{
protected:
    typedef std::map<unsigned char, FebBoard *> fbs_type;

    bool readPCA9544();
    bool writePCA9544();
    bool readPCA9544_DT();
    bool writePCA9544_DT();
public:
    static void setupLogger(std::string namePrefix);
    FebDistributionBoard(IFebAccessPoint & febaccesspoint
                         , rpct::i2c::II2cAccess & i2c_access
                         , int id
                         , const std::string & description
                         , unsigned short channel
                         , bool dt = false
                         , unsigned char pca9544_address_offset = 0
                         , unsigned char dt_channel = 0
                         , bool disabled = false);
    ~FebDistributionBoard();

    // FebSystemItem
    void reset();

    // THardwareItem
    int getId() const;
    const std::string & getDescription() const;
    const HardwareItemType & getType() const;

    // Configurable
    void configure(ConfigurationSet* configurationSet, int configureFlags);
    void configure(int configureFlags);
    void configure();

    void loadConfiguration(ConfigurationSet * configurationSet, int configureFlags);
    // Monitorable
    const MonitorItems & getMonitorItems();
    void monitor(volatile bool *stop);
    void monitor(volatile bool *stop, int configureFlags);
    void monitor(MonitorItems & items, int configureFlags = 0x0);

    // IPrintable
    void printParameters(tools::Parameters & parameters) const;

    // other
    static int makePosition(unsigned short channel
                            , unsigned char pca9544_address_offset
                            , unsigned char dt_channel);

    rpct::i2c::II2cAccess & getI2cAccess();
    IFebAccessPoint & getFebAccessPoint() const;

    unsigned short getChannel() const;
    unsigned char getDTChannel() const;
    bool isDT() const;
    // system
    FebBoard & addFebBoard(int id
                           , const std::string & description
                           , unsigned char febtype
                           , unsigned char position
                           , bool disabled = false);

    // hardware
    void enableFebAccess(bool force = false);
    void disableFebAccess(bool force = false, bool i2c_execute = true);

    void i2c_pca9544_error(rpct::exception::Exception const * e = 0)
        throw();
    void i2c_pca9544_ready()
        throw();
    void i2c_pca9544_dt_error(rpct::exception::Exception const * e = 0)
        throw();
    void i2c_pca9544_dt_ready()
        throw();
private:
    static log4cplus::Logger logger_;
protected:
    static HardwareItemType * type_;
    static MonitorItems * mitems_;

    int id_;
    std::string description_;

    /** IFebAccessPoint
     *  RPC controlboard
     *  DT minicrate
     */
    IFebAccessPoint & febaccesspoint_;
    rpct::i2c::II2cAccess & i2c_access_;
    /** channel in the "FebAccessPoint"
     *  RPC controlboard channel
     *  DT minicrate port
     */
    const unsigned short channel_;
    /** address offset for the PCA9544
     *  RPC line 0x00
     *  DT  line 0x06 for out, 0x07 elsewhere
     */
    const unsigned char pca9544_address_offset_;
    /** channel within the second pca9544 of the line */
    const unsigned char dt_channel_;
    const bool dt_;

    fbs_type fbs_;

    PCA9544Register pca9544_register_;
    PCA9544Register pca9544_dt_register_;
    rpct::i2c::TI2cResponse<FebDistributionBoard> * pca9544_response_;
    rpct::i2c::TI2cResponse<FebDistributionBoard> * pca9544_dt_response_;
    bool is_hardware_accessed_;
};

} // namespace rpct

#include "rpct/devices/FebDistributionBoard-inl.h"

#endif // rpct_devices_FebDistributionBoard_h_
