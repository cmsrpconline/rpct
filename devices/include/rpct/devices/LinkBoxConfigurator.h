#ifndef RPCT_LINKBOXCONFIGURATOR_H
#define RPCT_LINKBOXCONFIGURATOR_H

#include "rpct/devices/LinkBox.h"
#include <log4cplus/logger.h>
#include <bitset>
#include <vector>

namespace rpct {

class LinkBoxConfigurator {
protected:
    static log4cplus::Logger logger;
    LinkBox& linkBox_;
public:
    static void setupLogger(std::string namePrefix);
    LinkBoxConfigurator(LinkBox& linkBox) :
        linkBox_(linkBox) {
    }
    virtual ~LinkBoxConfigurator() {
    }

    virtual void configStart() = 0;
    virtual void configEnd() = 0;
    
    //virtual void writeII(LinkBoard& lb, IDevice::TID id, uint32_t value) = 0;
    virtual void writeII(LinkBoard& lb, uint32_t address, uint32_t data) = 0;
    virtual void writeI2C(LinkBoard& lb, unsigned char address, unsigned char value) = 0;
    virtual void waitBit(int bitNum, int value) = 0;
    virtual void intiTTCrxs() = 0;
    
};

}

#endif
