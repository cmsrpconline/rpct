#ifndef RPCTPACSETTINGSIMPL_H_
#define RPCTPACSETTINGSIMPL_H_

#include <boost/dynamic_bitset.hpp>
#include "rpct/devices/TriggerBoard.h"
#include "rpct/devices/PacSettings.h"
#include "rpct/devices/TriggerBoard.h"


namespace rpct {


class PacSettingsImpl : public PacSettings {
private:
    boost::dynamic_bitset<> recMuxClkInv_;
    boost::dynamic_bitset<> recMuxClk90_;
    boost::dynamic_bitset<> recMuxRegAdd_;
    std::vector<int> recMuxDelay_;
    std::vector<int> recDataDelay_;
    unsigned int bxOfCoincidence_;
    unsigned int pacEna_;
    unsigned int pacConfigId_;
public:
    PacSettingsImpl()
    : recMuxClkInv_(Pac::TB_OPTO_MUX_LINE_NUM), recMuxClk90_(Pac::TB_OPTO_MUX_LINE_NUM),
    recMuxRegAdd_(Pac::TB_OPTO_MUX_LINE_NUM), recMuxDelay_(Pac::TB_LINK_NUM, 0),
    recDataDelay_(Pac::TB_LINK_NUM, 0), bxOfCoincidence_(0), pacEna_(0), pacConfigId_(0) {
    }

    PacSettingsImpl(
        boost::dynamic_bitset<>& recMuxClkInv,
        boost::dynamic_bitset<>& recMuxClk90,
        boost::dynamic_bitset<>& recMuxRegAdd,
        std::vector<int>& recMuxDelay,
        std::vector<int>& recDataDelay,
        unsigned int bxOfCoincidence,
        unsigned int pacEna,
        unsigned int pacConfigId)
    : recMuxClkInv_(recMuxClkInv), recMuxClk90_(recMuxClkInv),
    recMuxRegAdd_(recMuxRegAdd), recMuxDelay_(recMuxDelay), recDataDelay_(recDataDelay) {
        if ((int) recMuxClkInv.size() != Pac::TB_OPTO_MUX_LINE_NUM) {
            throw IllegalArgumentException("Invalid length of recMuxClkInv");
        }
        if ((int) recMuxClk90.size() != Pac::TB_OPTO_MUX_LINE_NUM) {
            throw IllegalArgumentException("Invalid length of recMuxClk90");
        }
        if ((int) recMuxRegAdd.size() != Pac::TB_OPTO_MUX_LINE_NUM) {
            throw IllegalArgumentException("Invalid length of recMuxRegAdd");
        }
        if ((int) recMuxDelay.size() != Pac::TB_LINK_NUM) {
            throw IllegalArgumentException("Invalid length of recMuxDelay");
        }
        if ((int) recDataDelay.size() != Pac::TB_LINK_NUM) {
            throw IllegalArgumentException("Invalid length of recDataDelay");
        }
        bxOfCoincidence_ = bxOfCoincidence;
        pacEna_ = pacEna;
        pacConfigId_ = pacConfigId;
    }

    boost::dynamic_bitset<>& getRecMuxClkInv() {
        return recMuxClkInv_;
    }

    boost::dynamic_bitset<>& getRecMuxClk90() {
        return recMuxClk90_;
    }

    boost::dynamic_bitset<>& getRecMuxRegAdd() {
        return recMuxRegAdd_;
    }

    std::vector<int>& getRecMuxDelay() {
        return recMuxDelay_;
    }

    std::vector<int>& getRecDataDelay() {
        return recDataDelay_;
    }

    virtual HardwareItemType getDeviceType() {
        return Pac::TYPE;
    }

    virtual unsigned int getBxOfCoincidence() {
        return bxOfCoincidence_;
    }

    virtual unsigned int getPacEna() {
        return pacEna_;
    }

    virtual unsigned int getPacConfigId() {
        return pacConfigId_;
    }
};

}

#endif
