//---------------------------------------------------------------------------

//
//  Michal Pietrusinski 2006
//  Warsaw University
//

#ifndef rpctfebinstancemanagerh
#define rpctfebinstancemanagerh
//---------------------------------------------------------------------------
#include "rpct/devices/FebEndcap.h"
#include <log4cplus/logger.h>

#include <map>

namespace rpct {


class FebInstanceManager {
private:    
    static log4cplus::Logger logger;
    typedef std::map<unsigned int, Feb*> FebMap;
    typedef std::map<int, FebMap> FebPerChannelMap;
    FebPerChannelMap febPerChannelMap;
    virtual Feb& getFeb(int channel, unsigned int localNumber, bool endcap, TI2CInterface* i2c); 
public:
    static void setupLogger(std::string namePrefix);
    virtual ~FebInstanceManager();
    virtual Feb& getFeb(int channel, unsigned int localNumber, TI2CInterface* i2c);
    virtual FebEndcap& getFebEndcap(int channel, unsigned int localNumber, TI2CInterface* i2c);

};

}

#endif
