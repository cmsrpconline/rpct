//---------------------------------------------------------------------------

//
//  Michal Pietrusinski 2001
//  Warsaw University
//

#ifndef rpctlinkboardH
#define rpctlinkboardH
//---------------------------------------------------------------------------
#include "tb_ii.h"
#include "rpct/diag/TIIHistoMgr.h"
#include "rpct/diag/TFlashMgr.h"
#include "rpct/diag/TStandardDiagnosticReadout.h"
#include "rpct/diag/TPulser.h"
#include "rpct/devices/ILBDiagnosable.h"
#include "rpct/devices/ILBDiagnosticReadout.h"
#include "rpct/devices/LinkBoardMasterSlaveType.h"
#include "rpct/devices/LinkBoardFlashEncoder.h"
#include "rpct/devices/LinkBox.h"
#include "rpct/devices/System.h"
#include "rpct/devices/TGolI2C.h"
//#include "rpct/devices/TGolJtag.h"
#include "rpct/devices/TIIJtagIO.h"
#include "rpct/devices/TTTCrxI2C.h"
#include "rpct/devices/ErrorCounterAnalyzer.h"
#include "rpct/i2c/TIII2CMasterCore.h"
#include "rpct/i2c/TRPCCCUI2CMasterCore.h"
#include "rpct/std/bitcpy.h"
#include "rpct/ii/BoardBase.h"
#include "rpct/ii/memmap.h"
#include "rpct/ii/Monitorable.h"
#include <log4cplus/logger.h>
#include <boost/dynamic_bitset.hpp>
#include <map>

#define CII_LB

namespace rpct {


class SynCoder : public TIIDevice, virtual public ILBDiagnosable, virtual public Monitorable {
private:
	class LBPulser : public TPulser {
	private :
		int target_;
	public:
		LBPulser(IDiagnosable& owner, const char* name, IDiagCtrl& diagCtrl,
            TID areaMemPulse, TID wordPulserLength, TID bitsRepeatEna, TID bitsPulserOutEna):
            TPulser(owner, name, diagCtrl, areaMemPulse, wordPulserLength, bitsRepeatEna, bitsPulserOutEna) {
				target_ = -1;
			}

        virtual void writeTarget(int target);
        //virtual void Start();
        //virtual void Stop();
	};

public:

    class LBDiagnosticReadout : public TStandardDiagnosticReadout, virtual public ILBDiagnosticReadout {
    private:
        TID BITS_DAQ_DATA_SEL;
        TID BITS_DAQ_EXT_SEL;

        ILBDiagnosticReadout::ExtDataSel extSource;
    public:
        LBDiagnosticReadout(IDiagnosable& owner,
                             const char* name,
                             ConvertedBxDataFactory* convertedBxDataFactory,
                             IDiagCtrl& diagCtrl,
                             TID vectDiagDaq,
                             TID bitsDiagDaqEmpty,
                             TID bitsDiagDaqEmptyAck,
                             TID bitsDiagDaqLost,
                             TID bitsDiagDaqLostAck,
                             TID vectDiagDaqWr,
                             TID bitsDiagDaqWrAddr,
                             TID bitsDiagDaqWrAck,
                             TID vectDiagDaqRd,
                             TID bitsDiagDaqRdAddr,
                             TID bitsDiagDaqRdAck,
                             TID bitsDaqDataSel,
                             TID bitsDaqExtSel,
                             TID wordDiagDaqMask,
                             TID wordDataDaqDelay,
                             TID areaDiagDaq,
                             int dataWidth,
                             int trgNum,
                             int timerWidth,
                             int bcnWidth)
                : TStandardDiagnosticReadout(owner, name, convertedBxDataFactory, diagCtrl, vectDiagDaq, bitsDiagDaqEmpty,
                                             bitsDiagDaqEmptyAck, bitsDiagDaqLost, bitsDiagDaqLostAck,
                                             vectDiagDaqWr, bitsDiagDaqWrAddr, bitsDiagDaqWrAck, vectDiagDaqRd,
                                             bitsDiagDaqRdAddr, bitsDiagDaqRdAck, wordDiagDaqMask, wordDataDaqDelay,
                                             areaDiagDaq,  dataWidth, trgNum, timerWidth, bcnWidth),
        BITS_DAQ_DATA_SEL(bitsDaqDataSel), BITS_DAQ_EXT_SEL(bitsDaqExtSel) {
        	extSource = ILBDiagnosticReadout::edsNone;
        }

		void configureDataSource(ILBDiagnosticReadout::DataSel dataSel, ILBDiagnosticReadout::ExtDataSel extSel) {
            uint32_t status = Owner.readVector(VECT_DIAG_DAQ);
            Owner.setBitsInVector(BITS_DAQ_DATA_SEL, dataSel, status);
            Owner.setBitsInVector(BITS_DAQ_EXT_SEL, extSel, status);
            Owner.writeVector(VECT_DIAG_DAQ, status);

            extSource = extSel;
		}

        virtual void Configure(TMasks& masks, unsigned int daqDelay, ILBDiagnosticReadout::DataSel dataSel,
        		ILBDiagnosticReadout::ExtDataSel  extSel) {
            TDiagnosticReadout::Configure(masks, daqDelay);
            configureDataSource(dataSel, extSel);
        }

        virtual void Configure(unsigned int daqDelay, ILBDiagnosticReadout::DataSel dataSel, ILBDiagnosticReadout::ExtDataSel extSel) {
            TDiagnosticReadout::Configure(daqDelay);
            configureDataSource(dataSel, extSel);
        }

        ILBDiagnosticReadout::ExtDataSel getExtSource() {
        	return extSource;
        }
    };

    const static HardwareItemType TYPE;

private:
    static log4cplus::Logger logger;
    IHistoMgr* winHistoMgr;
    IHistoMgr* fullHistoMgr;
    IHistoMgr* bxHistoMgr;
    IHistoMgr* rateHistoMgr;
    TFlashMgr* flashMgr;
    LBDiagnosticReadout* diagnosticReadout;
    TPulser* pulser;

    TDiagCtrl* statisticsDiagCtrl;
    TDiagCtrl* daqDiagCtrl;
    TDiagCtrl* pulserDiagCtrl;

    TDiagCtrlVector diagCtrls;
    TDiagVector diags;

    ErrorCounterAnalyzer monMuxChkErr0Analyzer_;
    ErrorCounterAnalyzer monMuxChkErr1Analyzer_;

    std::map<int, uint16_t> firmwareInitWrites; //map which includes all TItem id's which are written to during init from flash and their respective values
    boost::dynamic_bitset<> firmwareInitChanEna; //vector which holds enabled channels, as seen by initialization from flash

    int monitoringIteration_;

    unsigned char TTCrxI2CFlashAddr;
public:
    static void setupLogger(std::string namePrefix);
#ifdef CII_LB
    #include "CII_LB_STD_iicfg_tab.h"
#else
    #define IID_FILE "RPC_LB_def.iid"
    #include "iid2cdef1.h"
    #define IID_FILE "RPC_system_def.iid"
    #include "iid2cdef1.h"
    #define IID_FILE "RPC_LBSTD_syncoder.iid"
    #include "iid2c1.h"

    static const int II_ADDR_SIZE;
    static const int II_DATA_SIZE;
#endif

    SynCoder(int id, IBoard& board);
    virtual ~SynCoder();

    LinkBoard& getLinkBoard();

    TDiagCtrl& getStatisticsDiagCtrl();
    TDiagCtrl& getDaqDiagCtrl();
    TDiagCtrl& getPulserDiagCtrl();

    IHistoMgr& getWinRateHistoMgr();
    IHistoMgr& getFullRateHistoMgr();
    IHistoMgr& getBXHistoMgr();

    LBDiagnosticReadout& getDiagnosticReadout();
    TPulser& getPulser();

    void resetTTC();
    void resetQPLL();
    void resetGOL();
    void synchGOL();

    void setLMuxInEna(bool masterEna, bool slave0Ena, bool slave1Ena);

    void setLMuxInDelay(int masterDelay, int slave0Delay, int slave1Delay);

    //slaveMastedNum: 0 master (this board), 1 - SLAVE0 (left), 2 SLAVE1 (right)
    void setLMuxInDelay(LinkBoardMasterSlaveType masterSlaveType, int delay);

    /** @param slaveOrMaster 1 - slave, 0 - master*/
    void configureAsSlaveOrMaster();

    void setChannelsEna(const boost::dynamic_bitset<> &channelsEna);

    /*
     * @returns the channels enabled mask remembered during last configureForFlash
     */
    boost::dynamic_bitset<> getRememberedChannelsEna() {
    	return firmwareInitChanEna;
    }

    virtual TDiagCtrlVector& GetDiagCtrls();
    virtual TDiagVector& GetDiags();
    virtual void ConfigureDiagnosable(TTriggerDataSel triggerDataSel, uint32_t dataTrgDelay);
    virtual void ConfigureDiagnosable(TTriggerDataSel triggerDataSel, uint32_t dataTrgDelay,
            uint32_t triggerAreaEna, bool synFullOutSynchWindow, bool synPulseEdgeSynchWindow);


    virtual bool checkResetNeeded();

    virtual void reset();
    virtual void configure(DeviceSettings* settings, int configureFlags);
    virtual void enable();

    virtual void monitor(volatile bool *stop);
    virtual void monitor(MonitorItems& items);

    static const uint32_t CONTROL_BUS_CHECK_VALUE;

    /* The following functions are used for preparing in-flash initialization data */
private:
    LinkBoardFlashEncoder flashEnc;
public:
    void prepareForFlash_write(uint16_t byte);
    void prepareForFlash_write(uint16_t addr, uint16_t cmd);
    void prepareForFlash_cancelWatchdog();
    void prepareForFlash_setWatchdog(uint16_t cycles);
    void prepareForFlash_ldelay(uint16_t cycles);
    void prepareForFlash_delay(uint16_t cycles);
    void prepareForFlash_verify(uint16_t ii_address, uint16_t ii_bit_mask, uint16_t ii_desired_value, uint16_t timeout);
    void prepareForFlash_i2c(uint8_t addr, uint8_t val);
    void writeOneBitFlash(int id, uint32_t data);
    uint32_t prepareBitsFlash_first(int id, uint32_t data);
    uint32_t prepareBitsFlash_next(int id, uint32_t data, uint32_t previousResult);
    uint32_t writePreparedBitsFlash(int id, uint32_t previousResult);
    void writeTwoBitsFlash(int id, uint32_t data, int id2, uint32_t data2);
    void writeVectorFlash(int id, uint32_t data);
    void writeWordFlash(int id, int idx, void* data);
    void writeWordFlash(int id, int idx, uint32_t data);
    void writeFlash(uint32_t address, void* data, size_t word_count, size_t word_size);
    void configureForFlash(ConfigurationSet* configurationSet);
    void resetGOLFlash();
    void setChannelsEnaFlash(const boost::dynamic_bitset<> &channelsEna);
    const MemoryMap::TItemInfo& CheckWriteAccess(int id, int idx = 0);
    void finalizeFlashConfig();
    void prepareForFlashConfig();
    std::string getFlashConfig();
    void setTTCrxI2CFlashAddr(unsigned char _addr);
    unsigned char getTTCrxI2CFlashAddr();
    bool verifyFirmawreWrites();
    void synchronizeLinkFlash();
    void enableHistoFlash();

    void setWindowFlash(int winO, int winC);
    void resetQPLLFlash();
    void setTtcRxI2cAddressFlash(unsigned char addr);
    void setTtcRxI2cAddressFlash();
    void intiTTCrxFlash();

    void generateFlashConfig(ConfigurationSet* configurationSet);
};


class LinkBox;
class HalfBox;

class LinkBoard: public BoardBase, virtual public IBoard {
public:
    struct CodedDataItem {
        unsigned int BCN;
        unsigned int BCN0;
        unsigned int HP;
        unsigned int EoD;
        unsigned int Data;
        unsigned int Part;
        unsigned int Time;
        unsigned int ChNo;
    };
    const static HardwareItemType TYPE;
private:
    static log4cplus::Logger logger;
    LinkBox* linkBox;
    HalfBox* halfBox;
    LinkBoardMasterSlaveType masterSlaveType;

    SynCoder synCoder;

    // I2C
    TRPCCCUI2CMasterCore* i2c;

    // TTCrx
    TTTCrxI2C ttcRxI2c;

    std::string chamberName_;

public:
    static void setupLogger(std::string namePrefix);
    LinkBoard(int ident, const char* desc, LinkBox* lbox, int boardNum, LinkBoardMasterSlaveType masterSlaveType,
        int syncoderId);
    virtual ~LinkBoard();

    int getChannelCnt() {
    	return SynCoder::RPC_FEBSTD_DATA_WIDTH;//96
    }

    typedef SynCoder::LBDiagnosticReadout Readout;

    TRPCCCUI2CMasterCore& getI2C();

    void initI2C();

    SynCoder& getSynCoder() {
        return synCoder;
    }

    LinkBoardMasterSlaveType getMasterSlaveType() {
        return masterSlaveType;
    }

    std::string getChamberName() {
    	return chamberName_;
    }

    void setChamberName(std::string& chmaberName) {
    	chamberName_ = chmaberName;
    }

    //functions with CCU errors recovery (reserFec)
    void memoryWrite(uint32_t address, unsigned short data);
    unsigned short memoryRead(uint32_t address);
    virtual void memoryWrite16(uint32_t address, unsigned short data);
    virtual unsigned short memoryRead16(uint32_t address);

    TTTCrxI2C& getTtcRxI2c();

    void setTtcRxI2cAddress(unsigned char addr);
    void setTtcRxI2cAddress();

    void intiTTCrx();
    /* there must be 3 s delay between intiTTCrxStep1 and intiTTCrxStep2
     * and 0.5 s between intiTTCrxStep2 and intiTTCrxStep3 */
    void intiTTCrxStep1();
    void intiTTCrxStep2();
    void intiTTCrxStep3();

    void enableAllChannels();

    void resetQPLL();
    void resetQPLLStep1();
    void resetQPLLStep2();
    void resetQPLLStep3();


    void setTTCrxDelays(int fineDelay1, int fineDelay2, int coarseDelay1, int coarseDelay2);
    void setWindow(int winO, int winC);
    void initDelays();
    void resetGOL();

    void testClocks();

    virtual std::string readTestRegs(bool verbose=false);

    //virtual void init();
    virtual void configure(ConfigurationSet* configurationSet, int configureFlags);
    virtual void selfTest() {
        testClocks();
    }

    //void configure(LinkBoardSettings& settings);

    virtual HalfBox* getHalfBox() {
        return halfBox;
    }
    virtual void dataToCodedDataItem(uint32_t data, CodedDataItem& item);
    virtual CodedDataItem dataToCodedDataItem(uint32_t data);

    virtual void decodeData(uint32_t* srcData, size_t srcCount,
                            uint32_t* dstData, size_t dstCount);

    virtual IDevice& addChip(std::string type, int pos, int id) {
        throw ENotImplemented("LinkBoard::addChip: not implemented");
    }

    virtual MasterLinkBoard* getMaster() = 0;

    virtual void checkVersions();

    virtual void fixTTCrxSettings();
};

class SlaveLinkBoard;

class MasterLinkBoard : public LinkBoard {
    typedef std::vector<SlaveLinkBoard*> SlaveLinkBoards;
private:
    SlaveLinkBoard* leftSlave;
    SlaveLinkBoard* rightSlave;
    SlaveLinkBoards* slaves;

    unsigned int slaveODelay_;
    unsigned int slave1Delay_;
    void setSlaves();
public:
    MasterLinkBoard(int ident, const char* desc, LinkBox* lbox, int boardNum, int syncoderId);
    virtual ~MasterLinkBoard();

    //virtual void init();
    virtual void configure(ConfigurationSet* configurationSet, int flags);

    virtual MasterLinkBoard* getMaster() {
        return this;
    }
    SlaveLinkBoard* getLeftSlave();
    SlaveLinkBoard* getRightSlave();
    SlaveLinkBoards& getSlaves();

    //void addSlave(SlaveLinkBoard* slave);

    void synchronizeLink();

    void normalLinkOperation();

    void enableFindOptLinksSynchrDelay(bool enable, unsigned int testData);

    void enableTransmissionCheck(bool enableBCNcheck, bool enableDataCheck);

    unsigned int getSlaveODelay() {
    	return slaveODelay_;
    }
    unsigned int getSlave1Delay() {
    	return slave1Delay_;
    }
    void configureMaster(ConfigurationSet* configurationSet);
};


class SlaveLinkBoard : public LinkBoard {
private:
    MasterLinkBoard* master;
public:
    SlaveLinkBoard(int ident, const char* desc, LinkBox* lbox, int boardNum, LinkBoardMasterSlaveType masterSlaveType, int syncoderId);
    virtual ~SlaveLinkBoard();

    virtual MasterLinkBoard* getMaster();
};

}



#endif
