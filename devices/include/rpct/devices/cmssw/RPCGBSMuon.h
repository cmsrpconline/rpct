#ifndef L1Trigger_RPCGBSMuon_h
#define L1Trigger_RPCGBSMuon_h
//#include "L1Trigger/RPCTrigger/interface/RPCMuon.h"
//#include "L1Trigger/RPCTrigger/interface/RPCPacMuon.h"
#include "rpct/devices/cmssw/RPCMuon.h"


//---------------------------------------------------------------------------
//output of the Pac (one LogCone),
//used in m_TBGhostBuster
//input and output of the m_TBGhostBuster

/** \class RPCTBMuon
  * Used in Ghoust Busters and sorters.
  * Has additionall filds: m_Killed, m_GBData, m_EtaAddress, m_PhiAddress need in these algorithms.
  * \author Karol Bunkowski (Warsaw)
  */


class RPCGBSMuon: public RPCMuon {
public:
  ///Empty muon.
  RPCGBSMuon();

  RPCGBSMuon(int ptCode, int quality, int sign, int patternNum, unsigned short firedPlanes); 

  //RPCGBSMuon(const RPCPacMuon& pacMuon);

  int getCode() const;

  void setCode(int code);

  void setPhiAddr(int phiAddr);

  void setSectorAddr(int sectorAddr);

  void setEtaAddr(int etaAddr);
  
  void setAddress(int etaAddr, int phiAddr);

  void setAddress(int tbNumber, int tbTower, int phiAddr);
  
  void setGBData(unsigned int gbData);

  int getEtaAddr() const;

  int getPhiAddr() const;

  int getSegmentAddr() const;

  int getSectorAddr() const;

  int getContinSegmAddr() const;

  void setCodeAndPhiAddr(int code, int phiAddr);

  void setCodeAndEtaAddr(int code, int etaAddr);

  int getGBData() const;

  std::string getGBDataBitStr() const;
  
  std::string printDebugInfo(int debugFormat) const;

  std::string printExtDebugInfo(int, int, int) const;

  void setGBDataKilledFirst();

  void setGBDataKilledLast();

  bool gBDataKilledFirst() const;

  bool gBDataKilledLast() const;

//------need to perform ghost - busting ------------
  void kill();

  /** @return true = was non-empty muon and was killed
    * false = was not killed or is zero */
  bool wasKilled() const;

  /** @return true = was no-zero muon and was not killed
    * false = is killed or is zero */
  bool isLive() const;
//aaa
  ///Used in sorting.
  struct TMuonMore : public std::less<RPCGBSMuon> {
    bool operator()(const RPCGBSMuon& muonL,
                     const RPCGBSMuon& muonR) const {
      return muonL.getCode() > muonR.getCode();
    }
  };
  	
  unsigned int toBits() const;  
  
  std::string toString(int format) const;

protected:
//------ hardware signals------------------------
  unsigned int m_EtaAddress;

  unsigned int m_PhiAddress;

  /** 2 bits,
    * 0 00 - this muon did not kill nothing on the sector edge
    * 1 01 - this muon killed muon from segment 0 (the "right" sector edge), or is in segment 0
    * 2 10 - this muon killed muon from segment 11 (the "left" sector edge), or is in segment 11
    * 3 11 - this muon killed muon from segment 0 and 11 */
  unsigned int m_GBData; 

//------- need to perform ghost - busting ---------
  bool m_Killed; //!< true means that muon was killed during GB	
};


typedef std::vector<RPCGBSMuon> L1RpcGBSMuonsVec;
typedef std::vector<L1RpcGBSMuonsVec> L1RpcGBSMuonsVec2;

#endif

