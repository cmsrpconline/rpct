/** Filip Thyssen */

#ifndef rpct_devices_RecursiveAction_h_
#define rpct_devices_RecursiveAction_h_

namespace rpct {

class IRecursive
{
protected:
    bool recursive_; // is this in recursive mode?
public:
    IRecursive();
    virtual ~IRecursive() {}
    bool isRecursive() const;
    virtual void startRecursive();
    virtual void stopRecursive();
    virtual void reset();
};
class RecursiveAction
{
protected:
    IRecursive * const ir_;
    bool recursive_; // did this action start the recursive action?
public:
    RecursiveAction(IRecursive * const ir);
    ~RecursiveAction();
    void stop();
};
            
inline IRecursive::IRecursive()
  : recursive_(false)
{}

inline bool IRecursive::isRecursive() const
{
    return recursive_;
}
inline void IRecursive::startRecursive()
{
    recursive_ = true;
}
inline void IRecursive::stopRecursive()
{
    recursive_ = false;
}
inline void IRecursive::reset()
{
    recursive_ = false;
}

inline RecursiveAction::RecursiveAction(IRecursive * const ir)
    : ir_(ir)
    , recursive_(false)
{
    if (ir_ && 
        (recursive_ = !(ir_->isRecursive()))
        )
        ir_->startRecursive();
}
inline void RecursiveAction::stop()
{
    if (recursive_)
        ir_->stopRecursive();
    recursive_ = false;
}
inline RecursiveAction::~RecursiveAction()
{
    stop();
}

} // namespace rpct

#endif // rpct_devices_RecursiveAction_h_
