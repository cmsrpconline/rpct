/** Filip Thyssen */

#ifndef rpct_devices_RPCFebSystem_h_
#define rpct_devices_RPCFebSystem_h_

#include "rpct/devices/FebSystem.h"
#include "rpct/devices/LinkSystem.h"

namespace rpct {

class RPCFebAccessPoint;

class RPCFebSystem
    : public virtual FebSystem
{
public:
    static void setupLogger(std::string namePrefix);
    RPCFebSystem(LinkSystem & linksystem);
    ~RPCFebSystem();

    void reset();

    // THardwareItem
    const HardwareItemType & getType() const;

    IFebAccessPoint & addRPCFebAccessPoint(int cb_id, bool disabled = false)
    throw (rpct::exception::SystemException);

private:
    static log4cplus::Logger logger_;
    static HardwareItemType * type_;

protected:
    LinkSystem & linksystem_;
};

} // namespace rpct

#include "rpct/devices/RPCFebSystem-inl.h"

#endif // rpct_devices_RPCFebSystem_h_
