/** Filip Thyssen */

#ifndef rpct_devices_FebChip_inl_h_
#define rpct_devices_FebChip_inl_h_

#include "rpct/devices/FebChip.h"

#include "rpct/devices/feb_const.h"

namespace rpct {

inline int FebChip::getId() const
{
    return id_;
}
inline const HardwareItemType & FebChip::getType() const
{
    return *type_;
}

inline FebPart & FebChip::getFebPart() const
{
    return febpart_;
}

inline uint16_t FebChip::getVTH(bool in) const
{
    return (in ? vth_ : adc_register_vth_.vth());
}
inline uint16_t FebChip::getVTHSet() const
{
    uint16_t value = (vth_ + vth_offset_set_ + 1) & 0x7fe;
    if (value > rpct::devices::feb::preset::vth_max_)
        value = rpct::devices::feb::preset::vth_max_;
    return value;
}

inline uint16_t FebChip::getVMON(bool in) const
{
    return (in ? vmon_ : adc_register_vmon_.vmon());
}
inline uint16_t FebChip::getVMONSet() const
{
    return (vmon_ + vmon_offset_set_ + 1) & 0x7fe;
}
inline int16_t FebChip::getVTHOffset(bool in) const
{
    return (in ? vth_offset_set_ : vth_offset_);
}
inline int16_t FebChip::getVMONOffset(bool in) const
{
    return (in ? vmon_offset_set_ : vmon_offset_);
}

} // namespace rpct

#endif // rpct_devices_FebChip_inl_h_
