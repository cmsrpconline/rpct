#ifndef RPCTSORTERCRATE_H
#define RPCTSORTERCRATE_H

#include "rpct/devices/VmeCrate.h"
#include "rpct/devices/TriggerBoard.h"
#include "rpct/devices/fsb.h"
#include "rpct/devices/hsb.h"
#include "rpct/devices/TTUBackplane.h"
#include "rpct/devices/TTUBoard.h"



namespace rpct {

class SorterCrate : public VmeCrate {
private:
    static log4cplus::Logger logger;
    TFsb* fsb_;
    std::vector<THsb*> hsbs_;
    std::vector<TTUBoard*> ttus_;
    TTTUBackplane* ttuBackplane_;
protected:
    //virtual bool checkResetNeeded();
    virtual bool checkWarm(ConfigurationSet* configurationSet);
    virtual void rememberConfiguration(ConfigurationSet* configurationSet);
    virtual void reset(bool forceReset);
public:
    static void setupLogger(std::string namePrefix);
    const static HardwareItemType TYPE;
    SorterCrate(int ident, const char* desc, std::string const & _label = std::string(""));
    virtual ~SorterCrate() {
    }

    virtual const HardwareItemType& getType() const {
        return TYPE;
    }

    TFsb* getFsb();
    std::vector<THsb*>& getHSBs();

    TTTUBackplane* getTTUBackplane();
    std::vector<TTUBoard*>& getTTUs();
};


}


#endif
