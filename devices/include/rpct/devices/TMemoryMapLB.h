//---------------------------------------------------------------------------

#ifndef TMemoryMapLBH
#define TMemoryMapLBH
//---------------------------------------------------------------------------

#include "rpct/ii/memmap.h"

namespace rpct {

class TMemoryMapLB : public TMemoryMap {
public:
  TMemoryMapLB(TDeclItems& items, int addr_width, int data_width)
    : TMemoryMap(items, addr_width, data_width) {}      
    
  virtual void writeArea(int id, void* data, int start_idx = 0, int n = npos);
  virtual void writeArea(int id, uint32_t* data, int start_idx = 0, int n = npos);
};


}

#endif
