#ifndef ICB_H_
#define ICB_H_

#include "tb_ii.h"
#include "rpct/i2c/TI2CInterface.h"
#include "rpct/devices/Feb.h"
#include "rpct/devices/FebEndcap.h"

namespace rpct {
    
class ICB: virtual public IBoard {
public:  
    virtual ~ICB() {
    }
  
    virtual TI2CInterface& getI2C() = 0;

    virtual void initI2C() = 0;

    virtual void selectI2CChannel(int channel) = 0;
    virtual Feb& getFeb(int channel, unsigned int localNumber) = 0;
    virtual FebEndcap& getFebEndcap(int channel, unsigned int localNumber) = 0;
};

}


#endif /*ICB_H_*/
