/** Filip Thyssen */

#ifndef rpct_devices_PCF8574a_h_
#define rpct_devices_PCF8574a_h_

#include "rpct/ii/i2c/Register.h"
#include "rpct/devices/feb_const.h"

namespace rpct {

class PCF8574aRegister : public rpct::i2c::TRegister<1, 0xff>
{
public:
    PCF8574aRegister(bool type = 0);

    /** warning: calling this issues a reset_in. */
    void setType(bool type);
    bool getType() const;

    void out2in();

    bool enabled(int part) const;
    bool disabled(int part) const;
    void enable(int part, bool _enable = true);
    void disable(int part);

    bool selected(int part) const;
    bool deselected(int part) const;
    void select(int part, bool _select = true);
    void deselect(int part);
protected:
    /** safe input */
    void safepart(int & part) const;

    int type_;
};

inline PCF8574aRegister::PCF8574aRegister(bool type)
    : type_(type?1:0)
{}

inline void PCF8574aRegister::setType(bool type)
{
    type_ = (type?1:0);
    reset_in();
}
inline bool PCF8574aRegister::getType() const
{
    return type_;
}

inline void PCF8574aRegister::out2in()
{
    in_[0] = out_[0];
}

inline bool PCF8574aRegister::enabled(int part) const
{
    safepart(part);
    return !(out_[0] & rpct::devices::feb::preset::en_dac_[part + type_]);
}
inline bool PCF8574aRegister::disabled(int part) const
{
    safepart(part);
    return (out_[0] & rpct::devices::feb::preset::en_dac_[part + type_]);
}
inline void PCF8574aRegister::enable(int part, bool _enable)
{
    safepart(part);
    if (_enable)
        in_[0] &= ~(rpct::devices::feb::preset::en_dac_[part + type_]);
    else
        in_[0] |= rpct::devices::feb::preset::en_dac_[part + type_];
}
inline void PCF8574aRegister::disable(int part)
{
    safepart(part);
    in_[0] |= rpct::devices::feb::preset::en_dac_[part + type_];
}

inline bool PCF8574aRegister::selected(int part) const
{
    safepart(part);
    return !(out_[0] & rpct::devices::feb::preset::sel_dac_[part + type_]);
}
inline bool PCF8574aRegister::deselected(int part) const
{
    safepart(part);
    if (part > type_)
        part = type_;
    return (out_[0] & rpct::devices::feb::preset::sel_dac_[part + type_]);
}
inline void PCF8574aRegister::select(int part, bool _select)
{
    safepart(part);
    if (_select)
        in_[0] &= ~(rpct::devices::feb::preset::sel_dac_[part + type_]);
    else
        in_[0] |= rpct::devices::feb::preset::sel_dac_[part + type_];
}
inline void PCF8574aRegister::deselect(int part)
{
    safepart(part);
    in_[0] |= rpct::devices::feb::preset::sel_dac_[part + type_];
}

inline void PCF8574aRegister::safepart(int & part) const
{
    if (part > type_)
        part = type_;
    if (part < 0)
        part = 0;
}

} // namespace rpct

#endif // rpct_devices_PCF8574a_h_
