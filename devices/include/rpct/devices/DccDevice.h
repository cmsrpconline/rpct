#ifndef RPCT_DCC_DEVICE_H
#define RPCT_DCC_DEVICE_H

#include "tb_ii.h"
#include "vme/TVMEInterface.h"
#include "rpct/devices/VDcc_pkgDef.h"
#include "rpct/devices/VmeCrate.h"
#include "rpct/devices/DummyVMEDevice.h"
#include "rpct/devices/DccChip.h"
#include "rpct/devices/DccInputHandler.h"
#include "rpct/devices/DccSettings.h"
#include "rpct/devices/DccMonitorables.h"
#include "rpct/ii/BoardBase.h"
#include "rpct/ii/memmap.h"
#include "rpct/std/ENotImplemented.h"
#include <log4cplus/logger.h>
#include <bitset>
#include <vector>

namespace rpct {

class DccDevice: public TDummyVMEDevice {
private:
    int vmeAddress_;
    int ebAddress_, emAddress_;
//  int ihAddress_[9];
    int preTrigger_, postTrigger_;
    DccChip* eventBuilder;
    DccChip* eventMerger;
//    std::vector<DccChip*> inputHandlerVec;
    std::vector<DccInputHandler*> inputHandlerVec;
    DccSettings* settings_;
    DccMonitorables lastMonitorables_;

protected:
    static log4cplus::Logger logger;

public:
    static void setupLogger(std::string namePrefix);
    const static HardwareItemType TYPE;

    DccDevice(int vme_board_address, IBoard& board, TVMEInterface* vme);
    virtual ~DccDevice();

    //DeviceSettings* getSettings(){ return settings_; }
    DccSettings* getSettings(){ return settings_; }

    void resetErrorCounters();

    virtual void reset();

    virtual void initialize();

    virtual void configure(DeviceSettings* settings, int flags);
    
    virtual void enable() {        
    }
    
    virtual void disable() {        
    }

    virtual void run();

    virtual void start(DeviceSettings* settings);//??

    void setBXRange(unsigned int pre, unsigned int post);

    void forceTTS(unsigned int status);

    void freeTTS();

    int checkTTS();

    int ebEventNumber() { return eventBuilder->read(EB_EVCNT); }

    DccMonitorables checkDevice();

    DeviceMonitorables* getMonitorables(){
        checkDevice();//read monitorables
        return &lastMonitorables_;
    }
    void checkName();

    virtual void  checkVersion();

    void enableSpyMem();

//    void getSpyMemData(std::vector<unsigned long long> spyData){//size of (unsigned long long)=64 bits
//        unsigned int dataLen=eventBuilder->read(EB_SPY_MEM_ADDW);
//        eventBuilder->clearBits(EB_CTRL_REG,std::vector<unsigned int>(1,EBB_CTRL_SPY_WRITE_MODE));
//        unsigned long long word=0;
//        for(unsigned int adr=0; adr<dataLen; ++adr){
//            eventBuilder->write(EB_SPY_MEM_ADDR,adr+1);
//            word = 0x100000000ll*(eventBuilder->read(EB_SPY_MEM_DATAH))+eventBuilder->read(EB_SPY_MEM_DATAL);
//            spyData.push_back(word);
//        }
//    }

};

}
#endif
