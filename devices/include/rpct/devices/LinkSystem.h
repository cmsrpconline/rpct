//---------------------------------------------------------------------------

#ifndef rpctLinkSystemH
#define rpctLinkSystemH
//---------------------------------------------------------------------------

#include <memory>
#include <list>
#include <map>
#include "tb_ii.h"
#include "rpct/devices/System.h"
#include "rpct/lboxaccess/FecManager.h"
#include "rpct/devices/LinkBox.h"
#include "rpct/devices/LinkBoard.h"
#include "rpct/devices/Rbc.h"
#include "rpct/devices/FebSystem.h"
#include "rpct/ii/ConfigurationSet.h"
#include <log4cplus/logger.h>
#include <sstream>

namespace rpct {


class VmeCrate;
//class LinkBox;
//class HalfBox;

class LinkSystem: public System {
public:
	static void setupLogger(std::string namePrefix);
    typedef std::list<FecManager*> FecManagerList;

    typedef std::list<LinkBox*> LinkBoxList;

    typedef std::vector<LinkBoard*> LinkBoards;

    typedef std::vector<ICB*> ControlBoards;

    typedef std::vector<Rbc*> RbcBoards;


private:
    static log4cplus::Logger logger;
    std::string towerName_;

    FecManagerList fecManagerList_;

    LinkBoxList linkBoxList_;

    LinkBoards linkBoards_;

    ControlBoards controlBoards_;

    RbcBoards rbcBoards_;

    FebSystem* febSystem_;

    struct FecManagerKey {
        int pciSlot;
        int vmeSlot;
        int ringNumber;

        FecManagerKey(int pci, int vme, int ring)
        : pciSlot(pci), vmeSlot(vme), ringNumber(ring){
        }
        bool operator< (const FecManagerKey& other) const {
            if (pciSlot < other.pciSlot) {
                return true;
            }
            if (pciSlot > other.pciSlot) {
                return false;
            }
            if (vmeSlot < other.vmeSlot) {
                return true;
            }
            if (vmeSlot > other.vmeSlot) {
                return false;
            }
            if (ringNumber < other.ringNumber) {
                return true;
            }
            return false;
        }
        std::string toString() {
            std::ostringstream ostr;
            ostr << "LinkBoxKey(" << pciSlot << ", "
                << vmeSlot << ", " << ringNumber << ")";
            return ostr.str();
        }
    };
    typedef std::map<FecManagerKey, FecManager*> FecManagerMap;
    FecManagerMap fecManagerMap_;

    struct HalfBoxKey {
        int pciSlot;
        int vmeSlot;
        int ringNumber;
        unsigned int ccuAddress;

        HalfBoxKey(int pci, int vme, int ring, unsigned int ccu)
        : pciSlot(pci), vmeSlot(vme), ringNumber(ring), ccuAddress(ccu) {
        }
        bool operator< (const HalfBoxKey& other) const {
            if (pciSlot < other.pciSlot) {
                return true;
            }
            if (pciSlot > other.pciSlot) {
                return false;
            }
            if (vmeSlot < other.vmeSlot) {
                return true;
            }
            if (vmeSlot > other.vmeSlot) {
                return false;
            }
            if (ringNumber < other.ringNumber) {
                return true;
            }
            if (ringNumber > other.ringNumber) {
                return false;
            }
            if (ccuAddress < other.ccuAddress) {
                return true;
            }
            return false;
        }

        std::string toString() {
            std::ostringstream ostr;
            ostr << "HalfBoxKey(" << pciSlot << ", "
                << vmeSlot << ", " << ringNumber << ", " << ccuAddress << ")";
            return ostr.str();
        }
    };
    typedef std::map<HalfBoxKey, HalfBox*> HalfBoxMap;
    std::vector<HalfBox*> halfBoxList_;
    HalfBoxMap halfBoxMap_;
    void addHalfBox(HalfBox& halfBox);

public:
    LinkSystem();
    virtual ~LinkSystem();

    void setTowerName(std::string towerName) {
    	towerName_ = towerName;
    }

    virtual FecManagerList& getFecManagers() {
        return fecManagerList_;
    }

    //assuming that there is only one FecManager
    virtual rpct::FecManager& getFecManager();

    virtual void addFecManager(FecManager& fecManager);
    virtual void addLinkBox(LinkBox& linkBox);
    virtual void registerItem(IHardwareItem& item);

    FecManager& getFecManager(int pciSlot, int vmeSlot, int ringNumber);
    HalfBox& getHalfBox(int pciSlot, int vmeSlot, int ringNumber, unsigned int ccuAddress);

    std::vector<HalfBox*>& getHalfBoxes() {
        return halfBoxList_;
    }

    LinkBoards& getLinkBoards() {
    	return linkBoards_;
    }

    LinkBoxList& getLinkBoxes() {
        return linkBoxList_;
    }

    ControlBoards& getControlBoards() {
    	return controlBoards_;
    }

    RbcBoards& getRbcBoards() {
    	return rbcBoards_;
    }

    void registerFebSystem(FebSystem* febSystem);

    FebSystem* getFebSystem() {
    	return febSystem_;
    }

    virtual bool checkResetNeeded() {
    	return checkResetNeeded(controlBoards_, linkBoards_, rbcBoards_);
    }

    bool checkResetNeeded(ControlBoards controlBoards, LinkBoards linkBoards, RbcBoards rbcBoards);

    void initI2CinControlBoards(ControlBoards controlBoards);

    void resetLinkBoards(LinkBoards linkBoards);

    void resetRbcBoards(RbcBoards rbcBoards);

    void resetBoards(ControlBoards controlBoards, LinkBoards linkBoards, RbcBoards rbcBoards) {
    	initI2CinControlBoards(controlBoards);
    	resetLinkBoards(linkBoards);
    	resetRbcBoards(rbcBoards);
    }

    //all boards at once
    void resetBoards() {
    	initI2CinControlBoards(controlBoards_);
    	resetLinkBoards(linkBoards_);
    	resetRbcBoards(rbcBoards_);
    }

    void setupBoards(ConfigurationSet* configurationSet, int configFlags, ControlBoards controlBoards, LinkBoards linkBoards, RbcBoards rbcBoards);

    void setupBoards(ConfigurationSet* configurationSet, int configFlags) {
    	setupBoards(configurationSet, configFlags, controlBoards_, linkBoards_, rbcBoards_);
    }

    void synchronizeLinksLBs(LinkBoards linkBoards);
    void synchronizeLinksRbcs(RbcBoards rbcBoards);
    void synchronizeLinks(LinkBoards linkBoards, RbcBoards rbcBoards) {
        synchronizeLinksLBs(linkBoards);
        synchronizeLinksRbcs(rbcBoards);
    }
    void synchronizeLinks() {
    	synchronizeLinks(linkBoards_, rbcBoards_);
    }

    void disableLinksLBs(LinkBoards linkBoards);

    void disableLinksLBs() {
        disableLinksLBs(linkBoards_);
    }

    void readLbChamberMap();


    /*
     * updates the board description with the name labels read from the file
     * @returns the ID of the backplane
     */
    std::string readBoardsLabels(std::string fileDirName);


    /* Checks, if the reset is needed (i.e. some devices are not ready), and if needed reset all hardware.
     * If forceRest == true, reset without checking. */
    virtual bool resetHardware(bool forceReset);

    /* Checks, if the proper configuration parameters are in the hardware,
     * and if setup (applies configuration parameters from the configurationSet) the hardware.
     * If forceSetup == true, setups without checking. */
    virtual void setupHardware(ConfigurationSet* configurationSet, bool forceSetup);
};

}

#endif
