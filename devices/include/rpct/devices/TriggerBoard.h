//---------------------------------------------------------------------------

//
//  Michal Pietrusinski 2001
//  Warsaw University
//
#ifndef RPCTTRIGGERBOARD_H
#define RPCTTRIGGERBOARD_H
//---------------------------------------------------------------------------

#include "tb_ii.h"
#include "TIIABootContr.h"
#include "TIIJtagIO.h"
#include "TTlk.h"
#include "TTTCrxI2C.h"
#include "rpct/diag/TDiagCtrl.h"
#include "rpct/diag/TStandardDiagnosticReadout.h"
#include "rpct/diag/TPulser.h"
#include "rpct/devices/VmeCrate.h"
#include "rpct/devices/OptoSettings.h"
#include "rpct/devices/PacSettings.h"
#include "rpct/devices/RmbSettings.h"
#include "rpct/devices/TGolI2C.h"
#include "rpct/devices/ErrorCounterAnalyzer.h"
#include "rpct/i2c/TIII2CMasterCorev09.h"
#include "rpct/i2c/TIII2CMasterCore.h"
#include "rpct/ii/BoardBase.h"
#include "rpct/ii/memmap.h"
#include "rpct/ii/Monitorable.h"
#include "rpct/diag/TIIHistoMgr.h"
#include "rpct/devices/QPLL.h"

#include <log4cplus/logger.h>
#include <boost/dynamic_bitset.hpp>

#define CII

namespace rpct {

class TriggerBoardDevice: public TIIDevice, public IIVMEDevice {
protected:
    int VMEBoardBaseAddr;
    static log4cplus::Logger logger;

public:
    static void setupLogger(std::string namePrefix);

#define IID_FILE "RPC_system_def.iid"
#include "iid2cdef1.h"

#define IID_FILE "RPC_TB3_def.iid"
#include "iid2cdef1.h"


    TriggerBoardDevice(int id,
            std::string name,
            TID name_id,
            std::string desc,
            const HardwareItemType& type,
            std::string ver,
            TID ver_id,
            TID checksum_id,
            uint32_t base_address,
            IBoard& board,
            int position,
            TMemoryMap* memmap,
            int vme_board_address)
    : TIIDevice(id, name, name_id, desc, type, ver, ver_id, checksum_id, base_address,
            board, position, memmap),
            VMEBoardBaseAddr(vme_board_address) {}

    virtual ~TriggerBoardDevice() {}

    virtual int GetVMEBaseAddrShift()
    { return 16; }

    virtual int GetVMEBoardBaseAddr()
    { return VMEBoardBaseAddr; }
};


class TriggerBoardVme : public TriggerBoardDevice {
private:
    TIIABootContr* ABootContr;
public:

#define IID_FILE "RPC_TB3_vme.iid"
#include "iid2c1.h"

    const static int II_ADDR_SIZE = 20;
    const static int II_DATA_SIZE = 32;
    const static HardwareItemType TYPE;

    TriggerBoardVme(int vme_board_address, IBoard& board);
    virtual ~TriggerBoardVme();


    TIIABootContr& GetABootContr() {
        if (ABootContr == 0) {
            ABootContr = new TIIABootContr(*this,
                    VECT_ALTERA,
                    BITS_ALTERA_NCONF,
                    BITS_ALTERA_NSTATUS,
                    BITS_ALTERA_CONFDONE,
                    BITS_ALTERA_INITDONE,
                    WORD_RESET,
                    AREA_ALTERA_BOOT);
        }

        return *ABootContr;
    }

    virtual void configure(DeviceSettings* settings, int flags) {
    }
};


class TriggerBoardControl : public TriggerBoardDevice, virtual public Monitorable {
private:
    TIIABootContr* ABootContr;

    TIII2CMasterCore* I2C;
    TTTCrxI2C TTCrxI2C;
    QPLL qpll_;
    //static const char* MON_TTCRX;
public:

#ifdef CII
#include "CII_TB3_CTRL_iicfg_tab.h"
#else
#define IID_FILE "RPC_TB3_ctrl.iid"
#include "iid2c1.h"
    const static int II_ADDR_SIZE;
    const static int II_DATA_SIZE;
#endif

    const static HardwareItemType TYPE;

    TriggerBoardControl(int vme_board_address, IBoard& board, int position);
    virtual ~TriggerBoardControl();


    TIIABootContr& GetABootContr() {
        if (ABootContr == 0)
            ABootContr = new TIIABootContr(*this,
                    VECT_ALTERA,
                    BITS_ALTERA_NCONF,
                    BITS_ALTERA_NSTATUS,
                    BITS_ALTERA_CONFDONE,
                    BITS_ALTERA_INITDONE,
                    0,
                    AREA_ALTERA_BOOT);
        return *ABootContr;
    }

    void ResetQPLL();
    void ResetTTC();
    void InitTTC();
    TTTCrxI2C& GetTTCrxI2C() {
        if (TTCrxI2C.GetI2CInterface() == 0) {
            TTCrxI2C.SetI2CInterface(GetI2C());
        }
        return TTCrxI2C;
    }

    void SetTTCrxI2CAddress(unsigned char addr);

    TIII2CMasterCore& GetI2C();

    virtual void reset();

    virtual bool checkResetNeeded(); //if reset of the TTCrx and QPLL is needed, not above reset()
    virtual bool checkWarm(DeviceSettings* s);
    virtual void configure(DeviceSettings* settings, int flags);


    virtual void monitor(volatile bool *stop);
    virtual void monitor(MonitorItems& items);

};



class Opto : public TriggerBoardDevice, virtual public IDiagnosable, virtual public Monitorable {
public:
    class TOptLink {
    private:
        Opto* opto_;
        int number_; //0, 1, 2
    public:
        TOptLink(Opto* opto, int optLinkNum);

        virtual ~TOptLink();

        Opto& GetOpto() {
            return *opto_;
        }

        void EnableOptoLink(bool enable);

        bool FindOptLinksTLKDelay(unsigned int testData);

        bool FindOptLinkDelay();
    };

    friend class TOptLink;
    typedef std::vector<TOptLink> TOptLinks;

    enum OptLinkStatus {
        OFF = 0,
        ON = 1, //after configure and before monitoring started
        OK = 2,
        WARNING = 3,
        ERROR = 4,
        RX_ERR =5
    };


private:
    std::vector<int> optLinkStatus_;
public:
    std::vector<int> getOptLinkStatus() {
        return optLinkStatus_;
    }
private:
    class OptoPulser : public TPulser {
    public:
        OptoPulser(IDiagnosable& owner, const char* name, IDiagCtrl& diagCtrl,
                TID areaMemPulse, TID wordPulserLength, TID bitsRepeatEna, TID bitsPulserOutEna):
                    TPulser(owner, name, diagCtrl, areaMemPulse, wordPulserLength, bitsRepeatEna, bitsPulserOutEna)
                    {};

                    virtual void writeTarget(int target) {
                        if (target > 1) {
                            throw TException("TPulser::WriteTarget: target > 1");
                        }
                        GetOwner().writeBits(Opto::BITS_PULSER_LOOP_ENA, target);
                    }
    };

    TDiagCtrl* DiagCtrl;
    TStandardDiagnosticReadout* DiagnosticReadout;
    TDiagCtrl* PulserDiagCtrl;
    TPulser* Pulser;

    TDiagCtrlVector DiagCtrls;
    TDiagVector Diags;

    boost::dynamic_bitset<> EnabledOptLinks;
    TOptLinks OptLinks;

    ErrorCounterAnalyzer monRecErrorAnalyzer_;

    public:

#ifdef CII
#include "CII_TB3_OPTO_iicfg_tab.h"
#else
#define IID_FILE "RPC_TB3_opto.iid"
#include "iid2c1.h"
    const static int II_ADDR_SIZE;
    const static int II_DATA_SIZE;
#endif

    const static HardwareItemType TYPE;


    Opto(int id, int vme_board_address, int ii_addr, const char* desc, IBoard& board, int position);
    virtual ~Opto();


    TDiagCtrl& GetDiagCtrl();
    TStandardDiagnosticReadout& GetDiagnosticReadout();
    TDiagCtrl& GetPulserDiagCtrl();
    TPulser& GetPulser();

    static unsigned int GetOptLinksCount() {
        return 3ul;
    }

    virtual TDiagCtrlVector& GetDiagCtrls();
    virtual TDiagVector& GetDiags();
    virtual void ConfigureDiagnosable(TTriggerDataSel triggerDataSel, uint32_t dataTrgDelay);

    virtual void reset();
    virtual void configure(DeviceSettings* settings, int flags);
    virtual void enable();


    //linkNum = 0...2
    void EnableOptoLink(int linkNum, bool enable) {
        OptLinks[linkNum].EnableOptoLink(enable);
    }

    void EnableOptoLink(uint32_t enable);

    boost::dynamic_bitset<>&  GetEnabledOptLinks() {
        return EnabledOptLinks;
    }

    void setRecFastDelay(unsigned int linkNum, unsigned int delay);

    void setRecDataDelay(unsigned int linkNum, unsigned int delay);

    //the MLBs must be configured first: synchronizeLink()!!!!!
    bool CheckLinksSynchronization();

    //the MLBs must be configured first: EnableFindOptLinksTLKDelay()!!!!!
    bool FindOptLinksTLKDelay( unsigned int testData);

    //the MLBs must be configured first, trannsmision check must be enabled
    bool FindOptLinksDelay();

    bool CheckLinksOperation();

    unsigned int getNumber() {
        return getPosition() - 2;
    }

    unsigned int getGlobalLinkNum(unsigned int localLinkNum) {
    	return localLinkNum + getNumber() * GetOptLinksCount();
    }

    virtual void monitor(volatile bool *stop);
    virtual void monitor(MonitorItems& items);

    void enableTransmissionCheck();
    void resetErrorCounters();
};

typedef std::vector<Opto*> Optos;


class Pac : public TriggerBoardDevice, virtual public IDiagnosable, virtual public Monitorable {
private:
    TDiagCtrl* DiagCtrl;
    TStandardDiagnosticReadout* DiagnosticReadout;
    TDiagCtrl* PulserDiagCtrl;
    TPulser* Pulser;

    TDiagCtrlVector DiagCtrls;
    TDiagVector Diags;
    ErrorCounterAnalyzer monRecErrorAnalyzer_;

    int towerNum_;
public:

#ifdef CII
#include "CII_TB3_LDPAC_iicfg_tab.h"
#else
#define IID_FILE "RPC_TB3_ldpac.iid"
#include "iid2c1.h"
    const static int II_ADDR_SIZE;
    const static int II_DATA_SIZE;
#endif

    const static HardwareItemType TYPE;

    Pac(int id, int vme_board_address, int ii_addr, const char* desc, IBoard& board,
            int position);
    virtual ~Pac();
    TDiagCtrl& GetDiagCtrl();
    TStandardDiagnosticReadout& GetDiagnosticReadout();
    TDiagCtrl& GetPulserDiagCtrl();
    TPulser& GetPulser();
    virtual TDiagCtrlVector& GetDiagCtrls();
    virtual TDiagVector& GetDiags();
    virtual void ConfigureDiagnosable(TTriggerDataSel triggerDataSel, uint32_t dataTrgDelay);

    virtual void reset();
    virtual void configure(DeviceSettings* settings, int flags);
    virtual void enable();

    virtual void checkConfigId(DeviceSettings* settings);

    //numer of PAC slot on the TB (0...3)
    unsigned int getNumber() {
        return getPosition() - 8;
    }

    int getTowerNum() {
        return towerNum_;
    }

    virtual void monitor(volatile bool *stop);
    virtual void monitor(MonitorItems& items);

    void enableTransmissionCheck();
    void resetErrorCounters();
};

typedef std::vector<Pac*> Pacs;

class GbSort : public TriggerBoardDevice, virtual public IDiagnosable, virtual public Monitorable {
private:
    class GbSortPulser : public TPulser {
    public:
        GbSortPulser(IDiagnosable& owner, const char* name, IDiagCtrl& diagCtrl,
                TID areaMemPulse, TID wordPulserLength, TID bitsRepeatEna, TID bitsPulserOutEna)
        : TPulser(owner, name, diagCtrl, areaMemPulse, wordPulserLength, bitsRepeatEna, bitsPulserOutEna) {
        }

        virtual void writeTarget(int target) {
            if (target > 1) {
                throw TException("TPulser::WriteTarget: target > 1");
            }
            GetOwner().writeBits(GbSort::BITS_PULSER_OUT_IN_SEL, target);
        }
    };

private:
    TDiagCtrl* DiagCtrl;
    TStandardDiagnosticReadout* DiagnosticReadout;

    TDiagCtrl* PulserDiagCtrl;
    TPulser* Pulser;

    HistoMgrVector histoManagers_;
    TDiagCtrl* statisticsDiagCtrl_;

    TDiagCtrlVector DiagCtrls;
    TDiagVector Diags;
    ErrorCounterAnalyzer monRecErrorAnalyzer_;
public:

#ifdef CII
#include "CII_TB3_GBSORT_iicfg_tab.h"
#else
#define IID_FILE "RPC_TB3_gbsort.iid"
#include "iid2c1.h"
    const static int II_ADDR_SIZE;
    const static int II_DATA_SIZE;
#endif

    const static HardwareItemType TYPE;

    GbSort(int id, int vme_board_address, int ii_addr, const char* desc, IBoard& board,
            int position);
    virtual ~GbSort();
    TDiagCtrl& GetDiagCtrl();
    TStandardDiagnosticReadout& GetDiagnosticReadout();

    HistoMgrVector& getHistoManagers();
    TDiagCtrl& getStatisticsDiagCtrl();

    virtual TDiagCtrlVector& GetDiagCtrls();
    virtual TDiagVector& GetDiags();
    TDiagCtrl& GetPulserDiagCtrl();
    TPulser& GetPulser();
    virtual void ConfigureDiagnosable(TTriggerDataSel triggerDataSel, uint32_t dataTrgDelay);

    virtual void reset();

    virtual void configure(DeviceSettings* settings, int flags);
    virtual void enable();

    virtual void monitor(volatile bool *stop);
    virtual void monitor(MonitorItems& items);

    void enableTransmissionCheck();
    void resetErrorCounters();
};

class Rmb : public TriggerBoardDevice, virtual public IDiagnosable, virtual public Monitorable  {
private:
    TDiagCtrl* DiagCtrl;
    TStandardDiagnosticReadout* DiagnosticReadout;
    TIII2CMasterCorev09* i2c_;
    TGolI2C* golI2c_;

    HistoMgrVector histoManagers_;
    TDiagCtrl* statisticsDiagCtrl_;

    TDiagCtrlVector DiagCtrls;
    TDiagVector Diags;

    ErrorCounterAnalyzer monRecErrorAnalyzer_;
    static const char* EVENT_CNT;
public:

#ifdef CII
#include "CII_TB3_RMB_iicfg_tab.h"
#else
#define IID_FILE "RPC_TB3_rmb.iid"
#include "iid2c1.h"
    const static int II_ADDR_SIZE;
    const static int II_DATA_SIZE;
#endif

    const static HardwareItemType TYPE;

    Rmb(int id, int vme_board_address, int ii_addr, const char* desc, IBoard& board,
            int position);
    virtual ~Rmb();
    TDiagCtrl& GetDiagCtrl();
    TStandardDiagnosticReadout& GetDiagnosticReadout();

    HistoMgrVector& getHistoManagers();
    TDiagCtrl& getStatisticsDiagCtrl();

    virtual TDiagCtrlVector& GetDiagCtrls();
    virtual TDiagVector& GetDiags();
    virtual void ConfigureDiagnosable(TTriggerDataSel triggerDataSel, uint32_t dataTrgDelay);

    virtual void resetRmb(); //reset the RMB algorithm and algorithm error counters
    virtual void reset(); //sets the hardcoded settings
    virtual void configure(DeviceSettings* settings, int flags);
    virtual void enable();

    virtual void monitor(volatile bool *stop);
    virtual void monitor(MonitorItems& items);

    void enableTransmissionCheck();
    void resetErrorCounters();

    TI2CInterface& getI2c();
    TGolI2C& getGolI2c();
};


class RmbMH : public TriggerBoardDevice {
public:
#include "CII_TB3_RMB_MH_iicfg_tab.h"

    const static HardwareItemType TYPE;

    RmbMH(int id, int vme_board_address, int ii_addr, const char* desc, IBoard& board,
            int position);
    virtual ~RmbMH();

    virtual void reset();
    virtual void configure(DeviceSettings* settings, int flags);
};



class TriggerBoard: public BoardBase, virtual public IBoard {
private:
    static log4cplus::Logger logger;
    TVMEInterface* vme_;
    TriggerBoardVme triggerBoardVme_;
    TriggerBoardControl triggerBoardControl_;

    int vmeAddress_;

    Optos optos_;
    Pacs pacs_;
    GbSort* gbSort_;
    Rmb* rmb_;

    boost::dynamic_bitset<> usedOptos_;
    boost::dynamic_bitset<> usedPacs_;


    void Init(bool use_vme);

public:
    static void setupLogger(std::string namePrefix);
    const static HardwareItemType TYPE;
    TriggerBoard(int ident, const char* desc, TVMEInterface* vme, int vme_board_address, VmeCrate* crate, int pos);
    virtual ~TriggerBoard();

    TriggerBoardVme& GetVme() {
        return triggerBoardVme_;
    }

    TriggerBoardControl& GetControl() {
        return triggerBoardControl_;
    }

    virtual IDevice& addChip(std::string type, int pos, int id);

    //the number of TB in the TC backplane slot (0...8)
    unsigned int getNumber() {
        return getPosition() - 11;
    }

    int getTowerNum(unsigned int pacNum);

    int GetVMEAddress() {
        return vmeAddress_;
    }

    Optos& GetOptos() {
        return optos_;
    }

    Pacs& GetLdPacs() {
        return pacs_;
    }

    GbSort* GetGbSort() {
        return gbSort_;
    }
    Rmb* GetRmb() {
        return rmb_;
    }

    void Reset();

    bool CheckOptLinksSynchronization();

    bool CheckLinksOperation();

    bool FindOptLinksTLKDelay(unsigned int testData);

    //enabling transsmison check, on MLBs transmission check must be enabled
    bool FindOptLinksDelay();

    void EnableTransmissionCheck(bool enableBCNcheck, bool enableDataCheck);

    //link num = 0...17
    void EnableOptLink(int linkNum, bool enable);

    boost::dynamic_bitset<>& GetUsedPacs() {
        return usedPacs_;
    }

    boost::dynamic_bitset<>& GetUsedOptos() {
        return usedOptos_;
    }

    virtual void configure(ConfigurationSet* configurationSet, int configureFlags);

    virtual void checkConfigId(ConfigurationSet* configurationSet);

    virtual void selfTest() {
    }

    std::vector<int> getOptLinkStatus();
};

}


#endif
