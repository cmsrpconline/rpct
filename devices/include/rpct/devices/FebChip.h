/** Filip Thyssen */

#ifndef rpct_devices_FebChip_h_
#define rpct_devices_FebChip_h_

#include <log4cplus/logger.h>

#include "rpct/ii/feb_const.h"

#include "rpct/devices/FebSystemItem.h"
#include "rpct/ii/HardwareItemBase.h"
#include "rpct/ii/Configurable.h"

#include "rpct/ii/exception/Exception.h"

#include "rpct/ii/i2c/I2cResponse.h"
#include "rpct/devices/AD7417.h"

namespace rpct {

class FebSystem;
class FebPart;

class FebChip
    : public FebSystemItem
    , protected Configurable
{
protected:
    friend class FebPart;

    void configure(ConfigurationSet* configurationSet, int configureFlags);
    void configure(int configureFlags);
    void configure();

    void loadConfiguration(ConfigurationSet * configurationSet, int configureFlags);
    void monitor(bool vth = true, bool vmon = true, int configureFlags = 0x0);
public:
    static void setupLogger(std::string namePrefix);
    FebChip(FebPart & febpart
            , unsigned char position
            , int id = -1
            , bool disabled = false);
    ~FebChip();

    // FebSystemItem
    void reset();

    // IHardwareItem
    int getId() const;
    const std::string & getDescription() const;
    const HardwareItemType & getType() const;

    // IPrintable
    void printParameters(tools::Parameters & parameters) const;

    // other
    FebPart & getFebPart() const;

    // values
    void setValues(uint16_t vth = rpct::preset::feb::vth_
                   , uint16_t vmon = rpct::preset::feb::vmon_
                   , int16_t vthoffset = rpct::preset::feb::offset_keep_
                   , int16_t vmonoffset = rpct::preset::feb::offset_keep_);

    void setVTH(uint16_t value = rpct::preset::feb::vth_);
    void setVMON(uint16_t value = rpct::preset::feb::vmon_);
    void setVTHOffset(int16_t value = rpct::preset::feb::offset_);
    void setVMONOffset(int16_t value = rpct::preset::feb::offset_);

    uint16_t getVTH(bool in = false) const;
    /** get the VTH to be set, if not in a newly calculated offset will be used */
    uint16_t getVTHSet() const;
    int16_t getVTHOffset(bool in = false) const;

    uint16_t getVMON(bool in = false) const;
    /** get the VMON to be set, if not in a newly calculated offset will be used */
    uint16_t getVMONSet() const;
    int16_t getVMONOffset(bool in = false) const;

    void i2c_read_vth_ready()
        throw ();
    void i2c_read_vth_error(rpct::exception::Exception const * e = 0)
        throw ();
    void i2c_read_vmon_ready()
        throw ();
    void i2c_read_vmon_error(rpct::exception::Exception const * e = 0)
        throw ();
    void i2c_write_vth_ready()
        throw ();
    void i2c_write_vth_error(rpct::exception::Exception const * e = 0)
        throw ();
    void i2c_write_vmon_ready()
        throw ();
    void i2c_write_vmon_error(rpct::exception::Exception const * e = 0)
        throw ();

private:
    static log4cplus::Logger logger_;
protected:
    static HardwareItemType * type_;

    int id_;

    FebPart & febpart_;

    const unsigned char position_;

    int configuration_id_;
    bool to_be_configured_;

    ADCRegister adc_register_vth_, adc_register_vmon_;
    uint16_t vth_, vmon_;
    int16_t vth_offset_, vmon_offset_;
    int16_t vth_offset_set_, vmon_offset_set_;

    rpct::i2c::TI2cResponse<FebChip> * vth_read_response_, * vmon_read_response_;
    rpct::i2c::TI2cResponse<FebChip> * vth_write_response_, * vmon_write_response_;
    bool vth_ok_, vmon_ok_;
};

} // namespace rpct

#include "rpct/devices/FebChip-inl.h"

#endif // rpct_devices_FebChip_h_
