#ifndef RPCLINKBOARDBXDATA_H_
#define RPCLINKBOARDBXDATA_H_

#include "rpct/diag/IDiagnosticReadout.h"
#include "rpct/devices/StandardBxData.h"

namespace rpct {
    

class LinkBoardBxData : public StandardBxData  {
public:
    LinkBoardBxData(IDiagnosticReadout::BxData& bxData, IDiagnosticReadout& ownerReadout);   
};

class LinkBoardBxDataFactory : virtual public IDiagnosticReadout::ConvertedBxDataFactory {
private:
    static LinkBoardBxDataFactory instance_;
    LinkBoardBxDataFactory() {
    }
    
public:    
    virtual IDiagnosticReadout::ConvertedBxData* create(IDiagnosticReadout::BxData& bxData, IDiagnosticReadout& ownerReadout) {
        return new LinkBoardBxData(bxData, ownerReadout);
    }       
    
    static LinkBoardBxDataFactory& getInstance() {
        return instance_;
    }
    
};

}

#endif 
