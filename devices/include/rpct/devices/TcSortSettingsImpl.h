#ifndef RPCTTCSORTSETTINGSIMPL_H_
#define RPCTTCSORTSETTINGSIMPL_H_

#include <boost/dynamic_bitset.hpp>
#include "rpct/devices/tcsort.h"
#include "rpct/devices/TcSortSettings.h"


namespace rpct {
    

class TcSortSettingsImpl : public TcSortSettings {
private:
    boost::dynamic_bitset<> recMuxClkInv_;
    boost::dynamic_bitset<> recMuxClk90_;
    boost::dynamic_bitset<> recMuxRegAdd_;
    std::vector<int> recMuxDelay_;
    std::vector<int> recDataDelay_;
public:
	TcSortSettingsImpl() 
    : recMuxClkInv_(TTCSortTCSort::TC_SORT_MUX_LINE_NUM), recMuxClk90_(TTCSortTCSort::TC_SORT_MUX_LINE_NUM), 
    recMuxRegAdd_(TTCSortTCSort::TC_SORT_MUX_LINE_NUM), 
    recMuxDelay_(TTCSortTCSort::TB_BOARD_NUM, 0), recDataDelay_(TTCSortTCSort::TB_BOARD_NUM, 0) {
    }

	TcSortSettingsImpl(
        boost::dynamic_bitset<> recMuxClkInv,
        boost::dynamic_bitset<> recMuxClk90,
        boost::dynamic_bitset<> recMuxRegAdd,
        std::vector<int> recMuxDelay,
        std::vector<int> recDataDelay)
    : recMuxClkInv_(recMuxClkInv), recMuxClkInv_(recMuxClk90), recMuxRegAdd_(recMuxRegAdd),
    recMuxDelay_(recMuxDelay), recDataDelay_(recDataDelay) {
        if ((int) recMuxClkInv.size() != TTCSortTCSort::TC_SORT_MUX_LINE_NUM) {
            throw IllegalArgumentException("Invalid length of recMuxClkInv");
        }
        if ((int) recMuxClk90.size() != TTCSortTCSort::TC_SORT_MUX_LINE_NUM) {
            throw IllegalArgumentException("Invalid length of recMuxClk90");
        }
        if ((int) recMuxRegAdd.size() != TTCSortTCSort::TC_SORT_MUX_LINE_NUM) {
            throw IllegalArgumentException("Invalid length of recMuxRegAdd");
        }
        if ((int) recMuxDelay.size() != TTCSortTCSort::TB_BOARD_NUM) {
            throw IllegalArgumentException("Invalid length of recMuxDelay");
        }
        if ((int) recDataDelay.size() != TTCSortTCSort::TB_BOARD_NUM) {
            throw IllegalArgumentException("Invalid length of recDataDelay");
        }
    }
    
    boost::dynamic_bitset<>& getRecMuxClkInv() {
        return recMuxClkInv_;
    }
    
    boost::dynamic_bitset<>& getRecMuxClk90() {
        return recMuxClk90_;
    }
    
    boost::dynamic_bitset<>& getRecMuxRegAdd() {
        return recMuxRegAdd_;
    }
    
    std::vector<int>& getRecMuxDelay() {
        return recMuxDelay_;
    }
    
    std::vector<int>& getRecDataDelay() {
        return recDataDelay_;
    }
    
    virtual HardwareItemType getDeviceType() {
        return TTCSortTCSort::TYPE;
    }
};

}

#endif 
