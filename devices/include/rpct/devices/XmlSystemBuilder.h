#ifndef RPCTXMLSYSTEMBUILDER_H_
#define RPCTXMLSYSTEMBUILDER_H_


#include "rpct/ii/IHardwareItem.h"
#include "rpct/ii/ICrate.h"
#include "rpct/devices/LinkSystem.h"
#include "rpct/std/TXMLObjectMgr.h"
#include <xercesc/sax/HandlerBase.hpp>   
#include <xercesc/sax/AttributeList.hpp>
#include <string>
#include <log4cplus/logger.h>

namespace rpct {
    
class FecManager;
class VmeCrate;
class LinkBox;

class XmlSystemBuilder : public TXMLObjectMgr, public xercesc::HandlerBase {
public:
    static void setupLogger(std::string namePrefix);
    ICrate* curCrate;
    FecManager* curFecManager;
    IHardwareItem* curItem;
    bool sawErrors;
    std::string filename;
    LinkSystem& system;
    bool mock;
private:
    static log4cplus::Logger logger;
    virtual void startElement(const XMLCh* const name, 
                            xercesc::AttributeList& attributes);
    virtual void endElement(const XMLCh* const name);
    std::string getAttribute(const char* attrName, xercesc::AttributeList& attributes);
        
    void warning(const xercesc::SAXParseException& exception);
    void error(const xercesc::SAXParseException& exception);
    void fatalError(const xercesc::SAXParseException& exception);
    void resetErrors();
    
    
    bool getHexAttribute(const char* attrName, xercesc::AttributeList& attributes, unsigned int& value);
    bool getDecAttribute(const char* attrName, xercesc::AttributeList& attributes, unsigned int& value);
    bool getDecAttribute(const char* attrName, xercesc::AttributeList& attributes, int& value);
public:
    XmlSystemBuilder(LinkSystem& system, bool mock = false);
    virtual ~XmlSystemBuilder() {}
    void build();
    void build(std::string filename);
};

}

#endif 
