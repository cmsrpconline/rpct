#ifndef RPCTCCSMONITORABLES_H
#define RPCTCCSMONITORABLES_H

#include "rpct/devices/DeviceMonitorables.h"

namespace rpct {

class CcsMonitorables : virtual public DeviceMonitorables {
public:
  CcsMonitorables() : position_(0), ttsStatus_(0), ccsStatus_(0) {}
  virtual ~CcsMonitorables() { }

  void setPosition(unsigned int p) { position_ = p; }

  void setTtsStatus(unsigned int s) { ttsStatus_ = s; }
  unsigned int getTtsStatus() { return ttsStatus_; }

  void setCcsStatus(unsigned int s) { ccsStatus_ = s; }
  unsigned int getCcsStatus() { return ttsStatus_; }

  virtual HardwareItemType getDeviceType();

  virtual std::string printInfo();
  virtual std::string printTTS();

  virtual bool hasProblem();

private:
  unsigned int position_;
  unsigned int ttsStatus_;
  unsigned int ccsStatus_;  
};
}

#endif
