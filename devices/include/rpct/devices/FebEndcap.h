//---------------------------------------------------------------------------

//
//  Michal Pietrusinski 2001
//  Warsaw University
//

#ifndef rpctfebEndcapH
#define rpctfebEndcapH

#include "rpct/devices/Feb.h"

namespace rpct {

class FebEndcap : public Feb {
private:
    bool dac2Enabled;
    static const unsigned char ADDR_AD7417_2 = 0x29;   
    static const unsigned int SEL_DAC1 = 0x1;
    static const unsigned int SEL_DAC2 = 0x2;
    static const unsigned int EN_DAC1 = 0x4;
    static const unsigned int EN_DAC2 = 0x8;

    float readAD7417_2_ADC(unsigned int chan);
    
    void writeAD5316_2_DAC(unsigned int wthp, float value);
    
    virtual unsigned char readPCF8574A_CR();
    virtual void writePCF8574A_CR(unsigned int value);
    
    virtual unsigned int convertLocalNumber(unsigned int localNumber);
public:                
    FebEndcap(DistributionBoard& distributionBoard, unsigned int localNumber);
    virtual ~FebEndcap();
    
    virtual float readTemperature2();     // 2a)
    
    virtual float readVTH3();            // 2b)
    virtual void writeVTH3(float value);

    virtual float readVTH4();            // 2b)
    virtual void writeVTH4(float value);
    
    virtual float readVMon3();             // 2b)
    virtual void writeVMon3(float value);
    
    virtual float readVMon4();             // 2b)
    virtual void writeVMon4(float value);
           
    virtual void enableDAC(bool value);
    virtual void enableDAC2(bool value);
    virtual bool isDac2Enabled() {
        return dac2Enabled;
    }
    
    virtual void selectDAC(bool value);
    virtual void selectDAC2(bool value);
    
    virtual void write2(float* vth3, float* vth4, float* vmon3, float* vmon4);
    
    virtual void writeRead2(float* vth3, float* vth4, float* vmon3, float* vmon4);
    
    /** Filip, 2009.10.16: Function to check speed gain with/without autocorrect, readback and predefined offsets */
    virtual void writeReadOffset2(float & vth3, float & vth4
                                  , float & vtho3, float & vtho4
                                  , float & vmon3, float & vmon4
                                  , float & vmono3, float & vmono4
                                  , const bool & correct_vth = true, const bool & correct_vmon = true
                                  , const bool & read_vth = true, const bool & read_vmon = true);
    
};


}

#endif
