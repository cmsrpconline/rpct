//---------------------------------------------------------------------------


//
//  Michal Pietrusinski 2001
//  Warsaw University
//

#ifndef iii2cH
#define iii2cH
//---------------------------------------------------------------------------

#include "rpct/i2c/TI2CInterface.h"


namespace rpct {


class TIII2C {  
public:
    TIII2C(int board_address) {}
    virtual ~TIII2C() {}

    virtual void Reset() = 0;

    virtual void Write (uint32_t address, char * data, size_t  count)  = 0;
    virtual void Write32 (uint32_t address, uint32_t int  value)  = 0;
    virtual void Write16 (uint32_t address, unsigned short int value)  = 0;

    virtual void Read (uint32_t address, char * data, size_t count)  = 0;
    virtual uint32_t  Read32 (uint32_t address)  = 0;
    virtual unsigned short Read16 (uint32_t address)  = 0;
};


}




#endif
