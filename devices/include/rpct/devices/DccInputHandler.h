#ifndef RPCT_DccInputHandler_H
#define RPCT_DccInputHandler_H

#include "rpct/devices/DccChip.h"
#include "rpct/devices/VDcc_pkgDef.h"
#include "rpct/devices/DccInputHandlerStatus.h"

class TVMEInterface;

namespace rpct {

class DccInputHandler : public DccChip {

public:

   DccInputHandler( unsigned int id,
    const char* desc, TVMEInterface* vme, int vme_board_address, int chip_address)
  :  DccChip(desc, vme, vme_board_address, chip_address, 2), id_(id) {}

  DccInputHandler(unsigned int id, TVMEInterface* vme, int vme_board_address);

  DccInputHandlerStatus status() { return DccInputHandlerStatus(*this); }

  unsigned int id() {return id_;}

private:
  unsigned int id_;
};
} 
#endif
