/** Filip Thyssen */

#ifndef rpct_devices_AD7417_h_
#define rpct_devices_AD7417_h_

#include "rpct/ii/i2c/Register.h"

namespace rpct {

/**
 * bits adc register: vvvv vvvv vv00 0000
 * v: 10 bit value, 2s complement for temperature
 **/
class ADCRegister : public rpct::i2c::TRRegister<2>
{ // AD7417
public:
    uint16_t value() const;
    uint16_t vth() const;
    uint16_t vmon() const;
    float temperature() const;
};

inline uint16_t ADCRegister::value() const
{
    return ((0x0003 & (out_[1] >> 6)) | (0x03fc & (out_[0] << 2)));
}
inline uint16_t ADCRegister::vth() const
{
    return value();
}
inline uint16_t ADCRegister::vmon() const
{
    // different unit for ADC, factor 2
    return (value() << 1) & 0x7fe;
}
inline float ADCRegister::temperature() const
{
    uint16_t val = value();
    if (val < 0x200) // 10 bits
        return .25 * val;
    else
        return -.25 * ((~val & 0x03FF) + 1);
}

} // namespace rpct

#endif // rpct_devices_AD7417_h_
