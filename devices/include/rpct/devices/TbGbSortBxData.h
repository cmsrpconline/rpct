#ifndef RPCTTBGBSORTDATA_H_
#define RPCTTBGBSORTDATA_H_

#include "rpct/diag/IDiagnosticReadout.h"
#include "rpct/devices/StandardBxData.h"

namespace rpct {
    

class TbGbSortBxData : public StandardBxData {   
public:
    TbGbSortBxData(IDiagnosticReadout::BxData& bxData, IDiagnosticReadout& ownerReadout);
};

class TbGbSortBxDataFactory : virtual public IDiagnosticReadout::ConvertedBxDataFactory {
private:
    static TbGbSortBxDataFactory instance_;
    TbGbSortBxDataFactory() {
    }
    
public:    
    virtual IDiagnosticReadout::ConvertedBxData* create(IDiagnosticReadout::BxData& bxData, IDiagnosticReadout& ownerReadout) {
        return new TbGbSortBxData(bxData, ownerReadout);
    }       
    
    static TbGbSortBxDataFactory& getInstance() {
        return instance_;
    }
    
};

}

#endif 
