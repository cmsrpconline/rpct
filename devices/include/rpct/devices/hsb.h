//---------------------------------------------------------------------------
//
//  Michal Pietrusinski 2001
//  Warsaw University
//
#ifndef hsbH
#define hsbH
//---------------------------------------------------------------------------

#include "tb_ii.h"
#include "i2c.h"
#include "TIIABootContr.h"
#include "TIIJtagIO.h"
#include "TTTCrxI2C.h"
#include "rpct/diag/TDiagCtrl.h"
#include "rpct/diag/TStandardDiagnosticReadout.h"
#include "rpct/diag/TPulser.h"
#include "rpct/diag/TIIHistoMgr.h"
#include "rpct/i2c/TIII2CMasterCore.h"
#include "rpct/ii/BoardBase.h"
#include "rpct/ii/memmap.h"
#include "rpct/ii/Monitorable.h"
#include "rpct/std/ENotImplemented.h"
#include "rpct/devices/tcsort.h"
#include "rpct/devices/ErrorCounterAnalyzer.h"
#include "rpct/devices/VmeCrate.h"
#include <boost/dynamic_bitset.hpp>
#include "rpct/devices/QPLL.h"

#define CII_HS

namespace rpct {

class THsbDevice: public TIIDevice, public IIVMEDevice {
protected:
    int VMEBoardBaseAddr;
    static log4cplus::Logger logger;
public:
    static void setupLogger(std::string namePrefix);
#define IID_FILE "RPC_system_def.iid"
#include "iid2cdef1.h"

#define IID_FILE "RPC_TB3_def.iid"
#include "iid2cdef1.h"

#define IID_FILE "RPC_TC_SORT_def.iid"
#include "iid2cdef1.h"

#define IID_FILE "RPC_SORT_def.iid"
#include "iid2cdef1.h"

    THsbDevice(int id, std::string name, TID name_id, std::string desc, const HardwareItemType& type, std::string ver,
            TID ver_id, TID checksum_id, uint32_t base_address, IBoard& board, int position, TMemoryMap* memmap,
            int vme_board_address) :
        TIIDevice(id, name, name_id, desc, type, ver, ver_id, checksum_id, base_address, board, position, memmap),
                VMEBoardBaseAddr(vme_board_address) {
    }

    virtual ~THsbDevice() {
    }

    virtual int GetVMEBaseAddrShift() {
        return 16;
    }

    virtual int GetVMEBoardBaseAddr() {
        return VMEBoardBaseAddr;
    }
};

class THsbVme: public THsbDevice {
private:
    TIIABootContr* ABootContr;
    TIIJtagIO* JtagIO;
public:

#define IID_FILE "RPC_TB3_vme.iid"
#include "iid2c1.h"

    const static int II_ADDR_SIZE = 20;
    const static int II_DATA_SIZE = 32;
    const static HardwareItemType TYPE;

    THsbVme(int vme_board_address, IBoard& board);
    virtual ~THsbVme();

    TIIABootContr& GetABootContr() {
        if (ABootContr == NULL)
            ABootContr = new TIIABootContr(*this, VECT_ALTERA, BITS_ALTERA_NCONF, BITS_ALTERA_NSTATUS,
                    BITS_ALTERA_CONFDONE, BITS_ALTERA_INITDONE, WORD_RESET, AREA_ALTERA_BOOT);
        return *ABootContr;
    }

    TIIJtagIO& GetJTagIO() {
        if (JtagIO == NULL)
            JtagIO = new TIIJtagIO(*this, VECT_JTAG, BITS_JTAG_TDI, BITS_JTAG_TDO, BITS_JTAG_TCK, BITS_JTAG_TMS);
        return *JtagIO;
    }

    void SetJtagEnable(bool value);

    virtual void reset() {
    }

    virtual void configure(DeviceSettings* settings, int flags) {
        reset();
    }
};

class THsbSortHalf: public THsbDevice, virtual public IDiagnosable, virtual public Monitorable {
private:
    class HalfSortPulser: public TPulser {
    public:
        HalfSortPulser(IDiagnosable& owner, const char* name, IDiagCtrl& diagCtrl, TID areaMemPulse,
                TID wordPulserLength, TID bitsRepeatEna, TID bitsPulserOutEna) :
            TPulser(owner, name, diagCtrl, areaMemPulse, wordPulserLength, bitsRepeatEna, bitsPulserOutEna) {
        }
        ;

        virtual void writeTarget(int target) {
            if (target > 1) {
                throw TException("TPulser::WriteTarget: target > 1");
            }
            GetOwner().writeBits(THsbSortHalf::BITS_PULSER_OUT_IN_SEL, target);
        }
    };

private:
    TDiagCtrl* DaqDiagCtrl;
    TStandardDiagnosticReadout* DiagnosticReadout;

    TDiagCtrl* PulserDiagCtrl;
    TPulser* Pulser;

    TDiagCtrlVector DiagCtrls;
    TDiagVector Diags;

    HistoMgrVector histoManagers_;
    TDiagCtrl* statisticsDiagCtrl_;

    TIII2CMasterCore* I2C;
    TTTCrxI2C TTCrxI2C;

    QPLL qpll_;

    boost::dynamic_bitset<> EnabledInputs; // one input = one TC = 2 cables (transmission channels)
    boost::dynamic_bitset<> EnabledInChannles;

    ErrorCounterAnalyzer monRecErrorAnalyzer_;

public:

#ifdef CII_HS
#include "CII_SC_HSORT_iicfg_tab.h"
#else
#define IID_FILE "RPC_SORT_half.iid"
#include "iid2c1.h"
    const static int II_ADDR_SIZE;
    const static int II_DATA_SIZE;
#endif

    const static HardwareItemType TYPE;

    THsbSortHalf(int id, int vme_board_address, int ii_addr, const char* desc, IBoard& board);
    virtual ~THsbSortHalf();

    TDiagCtrl& GetDaqDiagCtrl();
    TStandardDiagnosticReadout& GetDiagnosticReadout();

    TDiagCtrl& GetPulserDiagCtrl();
    TPulser& GetPulser();

    HistoMgrVector& getHistoManagers();
    TDiagCtrl& getStatisticsDiagCtrl();

    virtual TDiagCtrlVector& GetDiagCtrls();
    virtual TDiagVector& GetDiags();
    virtual void ConfigureDiagnosable(TTriggerDataSel triggerDataSel, uint32_t dataTrgDelay);

    unsigned int getNumber() {
    	/*if (getBoard().getPosition() == 1)
            return 0;
        if (getBoard().getPosition() == 3)
            return 1;*/
    	//throw TException("THsbSortHalf::getNumber - getPosition is not 1 nor 3");

    	if (getBoard().getDescription() == "HSB_0")
    		return 0;
    	if (getBoard().getDescription() == "HSB_1")
    		return 1;
    	throw TException("THsbSortHalf::getNumber - getDescription is not HSB_0 nor HSB_1");
    }

    void ResetQPLL();
    void ResetTTC();
    void InitTTC();
    void ResetPLL();

    bool checkResetNeeded();

    virtual void reset();
    virtual void configure(DeviceSettings* settings, int flags);
    virtual void enable();

    void EnableTransmissionCheck(bool enableBCNcheck, bool enableDataCheck);

    void SetEnabledInputs(const boost::dynamic_bitset<>& enabledInputs);

/*    const boost::dynamic_bitset<>& GetEnabledInputs() const {
        return EnabledInputs;
    }*/

    /**
     * returns the enabled input channels, where input channel is one Trigger Crate = 4 muons = 2 cables
     */
    const boost::dynamic_bitset<>& GetEnabledInChannles() const {
        return EnabledInChannles;
    }

    /**
     * returns the enabled cables input, where one input channel = 2 muons = 1 cable
     */
    const boost::dynamic_bitset<>& GetEnabledInputs() const {
        return EnabledInputs;
    }

    TTTCrxI2C& GetTTCrxI2C() {
        if (TTCrxI2C.GetI2CInterface() == 0) {
            TTCrxI2C.SetI2CInterface(GetI2C());
        }
        return TTCrxI2C;
    }

    TIII2CMasterCore& GetI2C();

    virtual void monitor(volatile bool *stop);
    virtual void monitor(MonitorItems& items);
};

class THsb: public BoardBase, virtual public IBoard {
private:
    static log4cplus::Logger logger;
    TVMEInterface* VME;
    THsbVme HsbVme;
    THsbSortHalf HsbSortHalf;

    int VMEAddress;

    int number_;

    void Init();

public:
    static void setupLogger(std::string namePrefix);
    const static HardwareItemType TYPE;
    THsb(int ident, const char* desc, TVMEInterface* vme, int vme_board_address, VmeCrate* crate, int pos,
            int hsbSortHalfChipId);
    virtual ~THsb();

    THsbVme& GetHsbVme() {
        return HsbVme;
    }

    THsbSortHalf& GetHsbSortHalf() {
        return HsbSortHalf;
    }

    int getNumber() {
        return number_;
    }

    int GetVMEAddress() {
        return VMEAddress;
    }

    virtual IDevice& addChip(std::string type, int pos, int id) {
        throw ENotImplemented("THsb::addChip: not implemented");
    }

    TIIJtagIO& GetJTagIO() {
        return HsbVme.GetJTagIO();
    }

    virtual void configure(ConfigurationSet* configurationSet, int configureFlags);
    virtual void selfTest();
};

}

#endif
