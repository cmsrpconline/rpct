//---------------------------------------------------------------------------

#ifndef TIICCUAccessH
#define TIICCUAccessH
//---------------------------------------------------------------------------

#include "rpct/lboxaccess/StdLinkBoxAccess.h"
#include "rpct/ii/TIIAccess.h"
#include "rpct/devices/LinkBoard.h" 
#include <log4cplus/logger.h>

#include <stdint.h>
namespace rpct {

class TIICCUAccess : public TIIAccess {
private:
    static log4cplus::Logger logger;
    StdLinkBoxAccess* LBox;
public:
    static void setupLogger(std::string namePrefix);
    TIICCUAccess(StdLinkBoxAccess* lbox, uint32_t module_addr)
            : TIIAccess(module_addr), LBox(lbox) {}

    virtual ~TIICCUAccess() {}

    virtual void Write(uint32_t address, void* data,
                       size_t word_count, size_t word_size);

    virtual void Read(uint32_t address, void* data,
                      size_t word_count, size_t word_size);
};

/*
class TIILBCCUAccess : public TIIAccess {
public:                    
  typedef IDevice::TID TID;
private:
  //TRPCCCU* CCU;   
  IStdLBoxAccess* LBox;
  LinkBoard& LB;
  TIIDevice& SynCoder;
  TID WORD_II_ADDR_EXT;
  int LBSTD_II_ADDR_BUS_WIDTH;
  int LBSTD_II_ADDR_EXT_WIDTH;
  uint32_t CurIIAddrExt;
  uint32_t ExtMask;
  uint32_t AddrMask;
  uint32_t UsePagingMask;
  uint32_t PageSize;
  
  void WriteIIAddrExt(uint32_t address)
    {                             
      uint32_t addrExt = (address >> (LBSTD_II_ADDR_BUS_WIDTH - 1)) & ExtMask;
      if (addrExt == 0) {
        UsePagingMask = 0;
      }
      else {
        if (addrExt != CurIIAddrExt) {
          SynCoder.writeWord(WORD_II_ADDR_EXT, 0, addrExt);
          CurIIAddrExt = addrExt;
        }   
        UsePagingMask = PageSize;
      }
    }
public:
    TIILBCCUAccess(IStdLBoxAccess* lbox, LinkBoard& lb,
                   TID wordIIAdrrExt,
                   int lbstdIIAddrBusWidth,
                   int lbstdIIAddrExtWidth)
      : TIIAccess(lb.getBoardNum()), LBox(lbox), LB(lb),
        SynCoder(lb.getSynCoder()),
        WORD_II_ADDR_EXT(wordIIAdrrExt),
        LBSTD_II_ADDR_BUS_WIDTH(lbstdIIAddrBusWidth),
        LBSTD_II_ADDR_EXT_WIDTH(lbstdIIAddrExtWidth),
        CurIIAddrExt(0xffffffff)
      {                               
        PageSize = 1 << (LBSTD_II_ADDR_BUS_WIDTH - 1);
        AddrMask = PageSize - 1;
        ExtMask = (1 << LBSTD_II_ADDR_EXT_WIDTH) - 1;
      }

    virtual ~TIILBCCUAccess() {}

    virtual void Write(uint32_t int address, void* data,
                       size_t word_count, size_t word_size);

    virtual void Read(uint32_t int address, void* data,
                      size_t word_count, size_t word_size);
};    */


class TIILBCCUAccess : public TIIAccess {
private:
    static log4cplus::Logger logger;
    StdLinkBoxAccess* LBox;
    LinkBoard& LB;
    static const uint32_t PageSize = 2048;
public:
    static void setupLogger(std::string namePrefix);
    TIILBCCUAccess(StdLinkBoxAccess* lbox, LinkBoard& lb)
            : TIIAccess((lb.getPosition() % 10) - 1), LBox(lbox), LB(lb) {}

    virtual ~TIILBCCUAccess() {}

    virtual void Write(uint32_t address, void* data,
                       size_t word_count, size_t word_size);

    virtual void Read(uint32_t address, void* data,
                      size_t word_count, size_t word_size);
};

}

#endif
