#ifndef CRATE_H
#define CRATE_H

#include <vector>
#include "rpct/std/IllegalArgumentException.h"
#include "rpct/std/ENotImplemented.h"

//#include "tb_vme.h"
//#include "tb_ii.h"
#include "rpct/ii/ICrate.h"
#include "rpct/ii/Monitorable.h"
#include <log4cplus/logger.h>

namespace rpct {

class Crate : public ICrate, virtual public IMonitorable {
protected:
    int id_;
    std::string description_;
    std::string label_;
    Boards boards_;
    //typedef std::vector<IBoard*> BoardVector;
    //BoardVector boardVector_;
    virtual bool checkResetNeeded();
    virtual bool checkWarm(ConfigurationSet* configurationSet);
    virtual void rememberConfiguration(ConfigurationSet* configurationSet) = 0;
    virtual void reset(bool cold) {
    	throw ENotImplemented("Crate::reset");
    }
    virtual void afterConfigure(bool cold) {
    }
public:
    static void setupLogger(std::string namePrefix);
    static log4cplus::Logger logger;

    Crate(int ident, const char* desc, std::string const & _label = std::string(""));
    virtual ~Crate();

    virtual int getId() const {
        return id_;
    }

    std::string const & getLabel() const {
        return label_;
    }

    virtual const std::string& getDescription() const {
        return description_;
    }

    virtual Boards& getBoards() {
        return boards_;
    }

    /*virtual IBoard* getBoard(int position) {
     if (position < 1 || position >= (int)boardVector_.size()) {
     throw IllegalArgumentException("Invalid position " + toString(position));
     }
     return boardVector_[position];
     }*/

    //virtual void addBoard(IBoard& board);

    virtual void checkVersions();
    virtual bool configure(ConfigurationSet* configurationSet, int configureFlags);

    virtual void enable();
    virtual void disable();

    virtual void monitor(volatile bool *stop);
};

}

#endif
