#ifndef RPCT_DccInputHandlerStatus_H
#define RPCT_DccInputHandlerStatus_H

#include <vector> 
#include <string>

//#include "rpct/devices/DccInputHandler.h"



namespace rpct {

class DccInputHandler;

class DccInputHandlerStatus {
public:
  static const int nHandlerInput = 4;
  struct ErrorCounter { 
    unsigned int NOTINTABLE; unsigned int DISPERR; unsigned int LOSSOFSYNC; 
    unsigned int sum() { return NOTINTABLE+DISPERR+LOSSOFSYNC;}
  };
  unsigned int ih() { return theId;}
  unsigned int status() { return theStatus; }
  unsigned int queueStatus() { return theQueueStatus; }
  int eventCounter() { return theEventCounter; }
  std::string print(bool forHtml=true);


private:
  friend class DccInputHandler;
  DccInputHandlerStatus(DccInputHandler& ih);

  unsigned int theId;
  int theEventCounter; 
  int theSyncAdjastment;
  unsigned int theStatus;
  unsigned int theEmergencyMask; 
  unsigned int theQueueStatus;
  
  std::vector<ErrorCounter> inputErrorCounters;

};

}
#endif
