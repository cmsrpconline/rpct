//---------------------------------------------------------------------------

#ifndef golH
#define golH
//---------------------------------------------------------------------------

namespace rpct {

class TGolInterface {
public:
  TGolInterface() : OutOfLock(0), Locked(1), Ready(2), TXLolc(3) {}

  virtual ~TGolInterface() {}

  virtual void WriteConfig0(unsigned char wait_time, unsigned char loss_of_lock_time) = 0;

  virtual void ReadConfig0(unsigned char& wait_time, unsigned char& loss_of_lock_time) = 0;

  virtual void WriteConfig1(unsigned char pll_lock_time,
                    bool en_soft_loss_of_lock,
                    bool en_loss_of_lock_count,
                    bool en_force_lock,
                    bool en_self_test) = 0;

  virtual void ReadConfig1(unsigned char& pll_lock_time,
                   bool& en_soft_loss_of_lock,
                   bool& en_loss_of_lock_count,
                   bool& en_force_lock,
                   bool& en_self_test) = 0;

  virtual void WriteConfig2(unsigned char pll_current, bool en_flag) = 0;

  virtual void ReadConfig2(unsigned char& pll_current, bool& en_flag) = 0;

  virtual void WriteConfig3(unsigned char ld_current, bool use_conf_regs) = 0;

  virtual void ReadConfig3(unsigned char& ld_current, bool& use_conf_regs) = 0;

  virtual void ReadStatus0(unsigned char& loss_of_lock_count) = 0;

  typedef unsigned char TLinkControlState;
  const TLinkControlState OutOfLock;
  const TLinkControlState Locked;
  const TLinkControlState Ready;
  const TLinkControlState TXLolc;

  virtual void ReadStatus1(bool& conf_wmode16,
                   bool& conf_glink,
                   TLinkControlState& link_control_state_C,
                   TLinkControlState& link_control_state_B,
                   TLinkControlState& link_control_state_A) = 0;
};

}

#endif
