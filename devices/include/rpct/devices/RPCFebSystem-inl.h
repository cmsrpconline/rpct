/** Filip Thyssen */

#ifndef rpct_devices_RPCFebSystem_inl_h_
#define rpct_devices_RPCFebSystem_inl_h_

#include "rpct/devices/RPCFebSystem.h"

namespace rpct {

inline const HardwareItemType & RPCFebSystem::getType() const
{
    return *type_;
}

} // namespace rpct

#endif // rpct_devices_RPCFebSystem_inl_h_
