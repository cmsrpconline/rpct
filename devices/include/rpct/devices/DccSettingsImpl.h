#ifndef RPCTDCCSETTINGSIMPL_H_
#define RPCTDCCSETTINGSIMPL_H_

#include <vector>
#include "rpct/devices/Dcc.h"
#include "rpct/devices/DccSettings.h"

namespace rpct {    

class DccSettingsImpl : public DccSettings {
 private:
  unsigned int preTriggerVal_;
  unsigned int postTriggerVal_;
  unsigned int fedId_;
  //unsigned int dataDelay_;
  //unsigned int trgDelay_;
  unsigned int resyncCmd_;
  unsigned int resyncMask_;
  unsigned int inputIds_[70];
  bool emulMode_, enaSpy_, ignoreLFF_;

 public:
  //Default CTor
  DccSettingsImpl() : preTriggerVal_(3), postTriggerVal_(3), 
    resyncCmd_(0x10), resyncMask_(0), 
    emulMode_(0), enaSpy_(1), ignoreLFF_(0) {
    for(int i=0; i<70; ++i)
      inputIds_[i] = 99;//all inputs disabled
  }


//  DccSettingsImpl(unsigned int preTriggerVal, unsigned int postTriggerVal, 
//		  unsigned int fedId, unsigned int resyncCmd, unsigned int resyncMask, 
//	  unsigned int *inputIds)
//    : preTriggerVal_(preTriggerVal), postTriggerVal_(postTriggerVal),
//    fedId_(fedId), resyncCmd_(resyncCmd), resyncMask_(resyncMask),
//    emulMode_(0), enaSpy_(1), ignoreLFF_(0) {
//    for(int i=0; i<70; ++i)
//      inputIds_[i] = *(inputIds+i);
//  }


  DccSettingsImpl(unsigned int preTriggerVal, unsigned int postTriggerVal, 
		  unsigned int fedId, unsigned int resyncCmd, unsigned int resyncMask,
		  bool emulMode, bool enaSpy, bool ignoreLFF,
		  unsigned int *inputIds)
    : preTriggerVal_(preTriggerVal), postTriggerVal_(postTriggerVal),
    fedId_(fedId), resyncCmd_(resyncCmd), resyncMask_(resyncMask),
    emulMode_(emulMode), enaSpy_(enaSpy), ignoreLFF_(ignoreLFF) 
 {
    for(int i=0; i<70; ++i) inputIds_[i] = *(inputIds+i);
  }
    
  unsigned int getPreTriggerVal() {
    return preTriggerVal_;
  }
    
  unsigned int getPostTriggerVal() {
    return postTriggerVal_;
  }

  unsigned int getResyncCmd() {
    return resyncCmd_;
  }

  unsigned int getResyncMask() {
    return resyncMask_;
  }
  /*
  unsigned int getDataDelay() {
    return dataDelay_;
  }
    
  virtual unsigned int getTrgDelay() {
    return trgDelay_;
  }
  */
  bool getEmulMode() {
    return emulMode_;
  }

  bool getEnaSpy() {
    return enaSpy_;
  }

  bool getIgnoreLFF() { return ignoreLFF_; }

  unsigned int *getInputIds() {
    return inputIds_;
  }

  unsigned int getFedId() { return fedId_; }
  void setFedId(unsigned int aFedId) { fedId_ = aFedId; }

  void setPreTriggerVal(unsigned int preTrg) {
    preTriggerVal_ = preTrg;
  }
    
  void setPostTriggerVal(unsigned int postTrg) {
    postTriggerVal_ = postTrg;
  }
  /*
  void setDataDelay(unsigned int dataDelay) {
    dataDelay_ = dataDelay;
  }
    
  void setTrgDelay(unsigned int trgDelay) {
    trgDelay_ = trgDelay;
  }
  */
  void setResyncCmd(unsigned int resyncCmd) {
    resyncCmd_ = resyncCmd;
  }

  void setResyncMask(unsigned int resyncMask) {
    resyncMask_ = resyncMask;
  }

  void setInputId(unsigned int in, unsigned int id) {
    //    *(inputIds_+in-1) = id;
    inputIds_[in-1] = id;
  }

  void setInputIds(unsigned int * inIds) {
    for(int i=0; i<70; ++i)
      inputIds_[i] = *(inIds+i);
  }

  HardwareItemType getDeviceType() {
    return Dcc::TYPE;
  }
};

}

#endif 
