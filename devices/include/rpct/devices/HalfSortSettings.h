#ifndef RPCTHALFSORTSETTINGS_H_
#define RPCTHALFSORTSETTINGS_H_

#include "rpct/ii/DeviceSettings.h"

namespace rpct {
    

class HalfSortSettings : virtual public DeviceSettings {
public:
    virtual ~HalfSortSettings() {
    }

	virtual int getConfigurationId() = 0;
    virtual boost::dynamic_bitset<>& getRecChanEna() = 0;   
    virtual std::vector<int>& getRecFastClkInv() = 0;   
    virtual std::vector<int>& getRecFastClk90() = 0;    
    virtual std::vector<int>& getRecFastRegAdd() = 0;
    virtual boost::dynamic_bitset<>& getRecFastDataDelay() = 0;   
    virtual boost::dynamic_bitset<>& getRecDataDelay() = 0;   
};

}

#endif 
