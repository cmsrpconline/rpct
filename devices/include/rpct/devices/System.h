//---------------------------------------------------------------------------

#ifndef rpctSystemH
#define rpctSystemH
//---------------------------------------------------------------------------

#include <memory>
#include <list>
#include <map>
#include "tb_ii.h"
#include "rpct/lboxaccess/FecManager.h"
#include "rpct/devices/LinkBox.h"
#include "rpct/ii/ConfigurationSet.h"
#include <log4cplus/logger.h>
#include <sstream>

namespace rpct {


class VmeCrate;

class System {
public:
	static void setupLogger(std::string namePrefix);
    typedef std::list<ICrate*> CrateList;
    typedef std::map<int, ICrate*> CrateMap;
    typedef std::map<int, IBoard*> BoardMap;
    typedef std::map<int, IDevice*> DeviceMap;
    typedef std::list<IHardwareItem*> HardwareItemList;
    typedef std::list<TVMEInterface*> VmeInterfaceList;

    typedef std::map<std::string, ICrate*> CrateByNameMap;
private:
    static log4cplus::Logger logger;

    typedef std::auto_ptr<System> Instance;
    static Instance instance;

    VmeInterfaceList vmeInterfaceList;

    CrateList crateList;

    typedef std::list<VmeCrate*> VmeCrateList;
    VmeCrateList vmeCrateList;

    CrateMap crateMap;
    BoardMap boardMap;
    DeviceMap deviceMap;

    CrateByNameMap crateByNameMap;

    typedef std::map<HardwareItemType, HardwareItemList> HardwareItemByTypeMap;
    HardwareItemByTypeMap hardwareItemByTypeMap;
    const static HardwareItemList emptyHardwareItemList;

protected:
    virtual void registerCrate(ICrate& crate);

public:
    System();
    virtual ~System();

    static System& getInstance();

    virtual CrateList& getCrates() {
        return crateList;
    }

    virtual void addVmeInterface(TVMEInterface& vmeInterface);
    virtual void addVmeCrate(VmeCrate& vmeCrate);

    virtual void registerItem(IHardwareItem& item);

    virtual ICrate& getCrateById(int id);
    virtual ICrate& getCrateByName(std::string name);
    virtual IBoard& getBoardById(int id);
    virtual IDevice& getDeviceById(int id);
    virtual const CrateMap& getCrateMap();
    virtual const BoardMap& getBoardMap();
    virtual const DeviceMap& getDeviceMap();

    virtual const HardwareItemList& getHardwareItemsByType(HardwareItemType type);

    virtual void checkVersions();

    /* Checks, if the reset is needed (i.e. some devices are not ready), and if needed reset all hardware.
     * The idea is that even one device in nor proper state should trigger reset of all devices
     * If forceRest == true, reset without checking. */
    virtual bool resetHardware(bool forceReset);

    /* Setup hardware (applies configuration parameters from the configurationSet) the hardware.
     * checking if the setup is needed depends on the actual implementation
     * If forceSetup == true, setups without checking. */
    virtual void setupHardware(ConfigurationSet* configurationSet, bool forceSetup);

    //virtual bool configure(ConfigurationSet& configurationSet, int configFlags = 0);

    //checks if there is DeviceSettings for a given device in the configurationSet,
    //if not the device configuration is skipped
    virtual void configureSelected(ConfigurationSet& configurationSet);


    // start run - clear error counters etc.
    virtual void enable();
    virtual void disable();
};

}

#endif
