#ifndef RPCTOPTOBXDATA_H_
#define RPCTOPTOBXDATA_H_

#include "rpct/diag/IDiagnosticReadout.h"
#include "rpct/devices/StandardBxData.h"

namespace rpct {
    

class OptoBxData : public StandardBxData {   
public:
    OptoBxData(IDiagnosticReadout::BxData& bxData);  
};

class OptoBxDataFactory : virtual public IDiagnosticReadout::ConvertedBxDataFactory {
private:
    static OptoBxDataFactory instance_;
    OptoBxDataFactory() {
    }
    
public:    
    virtual IDiagnosticReadout::ConvertedBxData* create(IDiagnosticReadout::BxData& bxData, IDiagnosticReadout& ownerReadout) {
        return new OptoBxData(bxData);
    }       
    
    static OptoBxDataFactory& getInstance() {
        return instance_;
    }
    
};

}

#endif 
