#ifndef RPCTDCCSETTINGS_H_
#define RPCTDCCSETTINGS_H_

#include "rpct/ii/DeviceSettings.h"

namespace rpct {
    
class DccSettings : virtual public DeviceSettings {
 public:
  virtual ~DccSettings() {}
    
  virtual unsigned int getPreTriggerVal() = 0;    
  virtual unsigned int getPostTriggerVal() = 0;    
  //virtual unsigned int getDataDelay() = 0;    
  //virtual unsigned int getTrgDelay() = 0;
  virtual unsigned int getResyncCmd() = 0;    
  virtual unsigned int getResyncMask() = 0;
  virtual bool getEmulMode() = 0;
  virtual bool getEnaSpy() = 0;
  virtual bool getIgnoreLFF() = 0;
  virtual unsigned int *getInputIds() = 0;
  virtual  unsigned int getFedId() = 0;

  virtual void setPreTriggerVal(unsigned int) = 0;    
  virtual void setPostTriggerVal(unsigned int) = 0;    
  //virtual void setDataDelay(unsigned int) = 0;    
  //virtual void setTrgDelay(unsigned int) = 0;
  virtual void setResyncCmd(unsigned int) = 0;    
  virtual void setResyncMask(unsigned int) = 0;
  virtual void setInputIds(unsigned int*) = 0;
  virtual void setInputId(unsigned int, unsigned int) = 0;
  virtual void setFedId(unsigned int) = 0;
};

}

#endif 
