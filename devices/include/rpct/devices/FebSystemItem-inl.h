/** Filip Thyssen */

#ifndef rpct_devices_FebSystemItem_inl_h_
#define rpct_devices_FebSystemItem_inl_h_

#include "rpct/devices/FebSystemItem.h"

namespace rpct {

inline FebSystem & FebSystemItem::getSystem() const
{
    return febsystem_;
}

} // namespace rpct

#endif // rpct_devices_FebSystemItem_inl_h_
