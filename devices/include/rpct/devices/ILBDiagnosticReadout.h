#ifndef ILBDIAGNOSTICREADOUT_H_
#define ILBDIAGNOSTICREADOUT_H_

#include "rpct/diag/IDiagnosticReadout.h"


namespace rpct {
    
class ILBDiagnosticReadout : virtual public IDiagnosticReadout {
public:
	//data from synchronizator, first 96 bits of readout
	enum DataSel {
		dsSynchWinData = 0,
		dsDiagSynchData
	};
	//"extended" data for readout, bits 96...
	enum ExtDataSel {
		edsNone = 0,
		edsCSC,
		edsLMUX,
		edsCoder,
		edsSlave0,
		edsSlave1
	};
		
    virtual void Configure(TMasks& masks, unsigned int daqDelay, DataSel dataSel, 
    		ExtDataSel  extSel) = 0;
        
    virtual void Configure(unsigned int daqDelay, DataSel dataSel, 
    		ExtDataSel extSel) = 0;    
};

}

#endif /*ILBDIAGNOSTICREADOUT_H_*/
