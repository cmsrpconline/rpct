//---------------------------------------------------------------------------

#ifndef TGolI2CH
#define TGolI2CH
//---------------------------------------------------------------------------

#include "gol.h"

#include "rpct/i2c/TI2CInterface.h"

namespace rpct {

class TGolI2C : public TGolInterface {
private:
    TI2CInterface* I2C;
    unsigned char I2CPointer;
    unsigned char I2CData;

    static const unsigned char Config0 = 0;
    static const unsigned char Config1 = 1;
    static const unsigned char Config2 = 2;
    static const unsigned char Config3 = 3;
    static const unsigned char Status0 = 4;
    static const unsigned char Status1 = 5;

    void WriteReg(unsigned char reg, unsigned char value) {
        I2C->Write8(I2CPointer, reg);
        I2C->Write8(I2CData, value);
    }

    unsigned char ReadReg(unsigned char reg) {
        I2C->Write8(I2CPointer, reg);
        return I2C->Read8(I2CData, false);
    }

public:
    TGolI2C(unsigned char addr) :
        I2C(NULL), I2CPointer((addr << 1) & 0xfe), I2CData((addr << 1) | 1) {
    }

    void SetAddress(unsigned char addr);

    unsigned char GetAddress();

    void SetI2CInterface(TI2CInterface& i2c);

    void WriteConfig0(unsigned char wait_time, unsigned char loss_of_lock_time);

    void ReadConfig0(unsigned char& wait_time, unsigned char& loss_of_lock_time);

    unsigned char ReadConfig0() {
        return ReadReg(Config0);
    }

    void WriteConfig1(unsigned char pll_lock_time, bool en_soft_loss_of_lock,
            bool en_loss_of_lock_count, bool en_force_lock, bool en_self_test);

    unsigned char ReadConfig1() {
        return ReadReg(Config1);
    }

    void ReadConfig1(unsigned char& pll_lock_time, bool& en_soft_loss_of_lock,
            bool& en_loss_of_lock_count, bool& en_force_lock, bool& en_self_test);

    void WriteConfig2(unsigned char pll_current, bool en_flag);

    unsigned char ReadConfig2() {
        return ReadReg(Config2);
    }

    void ReadConfig2(unsigned char& pll_current, bool& en_flag);

    void WriteConfig3(unsigned char ld_current, bool use_conf_regs);

    unsigned char ReadConfig3() {
        return ReadReg(Config3);
    }
    
    void ReadConfig3(unsigned char& ld_current, bool& use_conf_regs);

    unsigned char ReadStatus0() {
        return ReadReg(Status0);
    }

    void ReadStatus0(unsigned char& loss_of_lock_count);

    unsigned char ReadStatus1() {
        return ReadReg(Status1);
    }

    void ReadStatus1(bool& conf_wmode16, bool& conf_glink, TLinkControlState& link_control_state_C,
            TLinkControlState& link_control_state_B, TLinkControlState& link_control_state_A);
};

}

#endif
