/** Filip Thyssen */

#ifndef rpct_devices_FebPart_h_
#define rpct_devices_FebPart_h_

#include <log4cplus/logger.h>

#include "rpct/devices/FebSystemItem.h"
#include "rpct/ii/HardwareItemBase.h"
#include "rpct/ii/Configurable.h"
#include "rpct/ii/Monitorable.h"

#include "rpct/ii/exception/Exception.h"

#include "rpct/ii/i2c/I2cResponse.h"

#include "rpct/devices/AD5316.h"
#include "rpct/devices/AD7417.h"
#include "rpct/devices/FebChip.h"

namespace rpct {

namespace i2c {
class II2cAccess;
} // namespace i2c

class FebSystem;
class FebBoard;

class FebPart
    : public FebSystemItem
    , public IMonitorable
    , public Configurable
{
protected:
    typedef FebChip * fcs_type[2];

    unsigned short channel() const;
    unsigned char febtype() const;
    unsigned char address() const;

    void writeVTH(unsigned char chip);
    void writeVTH();
    void writeVMON(unsigned char chip);
    void writeVMON();
    void write();

public:
    static void setupLogger(std::string namePrefix);
    FebPart(FebBoard & febboard
            , rpct::i2c::II2cAccess & i2c_access
            , unsigned char position
            , int id
            , bool disabled = false);
    ~FebPart();

    // FebSystemItem
    void reset();

    // IHardwareItem
    int getId() const;
    const std::string & getDescription() const;
    const HardwareItemType & getType() const;

    // Configurable
    void configure(ConfigurationSet* configurationSet, int configureFlags);
    void configure(int configureFlags);
    void configure();

    void loadConfiguration(ConfigurationSet * configurationSet, int configureFlags);
    // Monitorable
    const MonitorItems & getMonitorItems();
    void monitor(volatile bool *stop);
    void monitor(volatile bool *stop, int configureFlags);
    void monitor(MonitorItems & items, int configureFlags = 0x0);

    // IPrintable
    void printParameters(tools::Parameters & parameters) const;

    // other
    rpct::i2c::II2cAccess & getI2cAccess() const;
    FebBoard & getFebBoard() const;

    // values
    float getTemperature() const;

    FebChip & addFebChip(int id
                         , unsigned char position
                         , bool disabled = false)
        throw (rpct::exception::SystemException);

    // hardware
    void readTemperature();

    void readVTH(unsigned char chip);
    void readVTH();
    void readVMON(unsigned char chip);
    void readVMON();
    void read();

    void i2c_read_temp_error(rpct::exception::Exception const * e = 0)
        throw ();
    void i2c_read_temp_ready()
        throw ();
    void i2c_write_vth_error(rpct::exception::Exception const * e = 0)
        throw ();
    void i2c_write_vth_ready()
        throw ();
    void i2c_write_vmon_error(rpct::exception::Exception const * e = 0)
        throw ();
    void i2c_write_vmon_ready()
        throw ();
private:
    static log4cplus::Logger logger_;
protected:
    static HardwareItemType * type_;
    static MonitorItems * mitems_;

    int id_;

    rpct::i2c::II2cAccess & i2c_access_;

    FebBoard & febboard_;

    const unsigned char position_;

    fcs_type fcs_;

    DACRegister dac_register_;
    ADCRegister adc_register_temperature_;

    rpct::i2c::TI2cResponse<FebPart>
    * temperature_read_response_
        , * vth_write_response_
        , * vmon_write_response_;
};

} // namespace rpct

#include "rpct/devices/FebPart-inl.h"

#endif // rpct_devices_FebPart_h_
