/** Filip Thyssen */

#ifndef rpct_devices_FebSystemItem_h_
#define rpct_devices_FebSystemItem_h_

#include <ostream>
#include <map>
#include <string>
#include <utility>

#include "rpct/ii/IHardwareItem.h"
#include "rpct/tools/Printable.h"
#include "rpct/ii/HardwareFlags.h"
#include "rpct/tools/LogState.h"

namespace rpct {

class FebSystem;

class FebSystemItem
    : public virtual IHardwareItem
    , public virtual tools::Printable
    , public HardwareFlags
    , public tools::TTreeLogState<FebSystemItem *> // both the treelogstate and the treenode logic
{
public:
    FebSystemItem(FebSystem & febsystem
                  , int position
                  , FebSystemItem * parent = 0);
    virtual ~FebSystemItem();

    FebSystem & getSystem() const;

    virtual void reset();

    void addToSystem();
    void removeFromSystem();

protected:
    FebSystem & febsystem_;
};

std::ostream & operator<<(std::ostream & outstream, const FebSystemItem & fsi);

} // namespace rpct

#include "rpct/devices/FebSystemItem-inl.h"

#endif // rpct_devices_FebSystemItem_h_
