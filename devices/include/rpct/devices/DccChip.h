#ifndef RPCT_DCC_CHIP_H
#define RPCT_DCC_CHIP_H

#include <vector>
#include <string>
class TVMEInterface;

namespace rpct {

class DccChip {

public:


    DccChip(const char* desc, TVMEInterface* vme, int vme_board_address,
            int chip_address, size_t word_size);

    ~DccChip(){}

    int getChipAddress(){ return chipAddress_; }

    std::string getChipDesc(){ return desc_; }

    size_t getChipWordSize(){ return word_size_; }

    unsigned int read(int addr);

    void write(int addr, unsigned int val);

    void setBits(int addr, std::vector<unsigned int> bits);

    void clearBits(int addr, std::vector<unsigned int> bits);
  
protected:
    DccChip() : vme_(0), vmeAddress_(0), chipAddress_(0), desc_(" "), word_size_(2) {}
    void init( const char* desc, TVMEInterface* vme, int vme_board_address,
        int chip_address, size_t word_size);

private:

    TVMEInterface* vme_;
    int vmeAddress_, chipAddress_;
    std::string desc_;
    size_t word_size_;

};

}
#endif
