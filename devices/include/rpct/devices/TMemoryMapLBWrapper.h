//---------------------------------------------------------------------------

#ifndef TMemoryMapLBWrapperH
#define TMemoryMapLBWrapperH
//---------------------------------------------------------------------------

#include "rpct/ii/memmap.h"

namespace rpct {

class TMemoryMapLBWrapper : public MemoryMap {
private:
	TMemoryMap* memoryMap_;
    virtual const TItemInfo& CheckReadAccess(int id, int idx=0) {
        if (GetHardwareIfc() == 0)
            throw TException("TMemoryMapLBWrapper: HardwareIfc==NULL");

        const TItemInfo& item = GetItemInfo(id);

        if (item.RdType == VII_RNOACCESS)
            throw TException("TMemoryMapLBWrapper: no read access");

        if (idx >= item.Number)
            throw TException("TMemoryMapLBWrapper: idx out of range");

        return item;
    }
public: /* needed for generating data for flash initialization */
    virtual const TItemInfo& CheckWriteAccess(int id, int idx=0)
    {
        if (GetHardwareIfc() == 0)
            throw TException("TMemoryMapLBWrapper: HardwareIfc==NULL");

        const TItemInfo& item = GetItemInfo(id);

        if (item.WrType == VII_WNOACCESS)
            throw TException("TMemoryMapLBWrapper: no write access");

        if (idx >= item.Number)
            throw TException("TMemoryMapLBWrapper: idx out of range");

        return item;
    }
public:
	TMemoryMapLBWrapper(TMemoryMap* memoryMap)
    : memoryMap_(memoryMap) {		
	}      
	
	virtual ~TMemoryMapLBWrapper() {
		delete memoryMap_;
	}	

    // word operations
    virtual uint32_t readWord(int id, int idx) {
    	return memoryMap_->readWord(id, idx);
    }
    virtual void readWord(int id, int idx, void* data) {
    	return memoryMap_->readWord(id, idx, data);
    }

    virtual void writeWord(int id, int idx, uint32_t data) {
    	memoryMap_->writeWord(id, idx, data);
    }
    virtual void writeWord(int* id, int* idx, uint32_t* data, size_t count) {
    	memoryMap_->writeWord(id, idx, data, count);
    }
    virtual void writeWord(int id, int idx, void* data) {
    	memoryMap_->writeWord(id, idx, data);
    }

    // area operations
    // !!!! this one has different behaviour
    virtual void writeArea(int id, void* data, int start_idx = 0, int n = npos);
    virtual void readArea(int id, void* data, int start_idx = 0, int n = npos) {
    	return memoryMap_->readArea(id, data, start_idx, n);
    }

    // !!!! this one has different behaviour
    virtual void writeArea(int id, uint32_t* data, int start_idx = 0, int n = npos);
    virtual void readArea(int id, uint32_t* data, int start_idx = 0, int n = npos) {
    	return memoryMap_->readArea(id, data, start_idx, n);
    }

    //vector operations
    virtual uint32_t readVector(int id){
    	return memoryMap_->readVector(id);
    }
    virtual void readVector(int id, void* data){
    	return memoryMap_->readVector(id, data);
    }

    virtual void writeVector(int id, uint32_t data) {
    	memoryMap_->writeVector(id, data);
    }
    virtual void writeVector(int* id, uint32_t* data, size_t count) {
    	memoryMap_->writeVector(id, data, count);
    }
    virtual void writeVector(int id, void* data) {
    	memoryMap_->writeVector(id, data);
    }

    // bit operations
    virtual uint32_t readBits(int id){
    	return memoryMap_->readBits(id);
    }
    virtual void writeBits(int id, uint32_t data, bool preread = true)  {
    	memoryMap_->writeBits(id, data, preread);
    }

    virtual uint32_t getBitsInVector(int bits_id, uint32_t vector_data){
    	return memoryMap_->getBitsInVector(bits_id, vector_data);
    }
    virtual void setBitsInVector(int bits_id, uint32_t bits_data, uint32_t& vector_data)  {
    	memoryMap_->setBitsInVector(bits_id, bits_data, vector_data);
    }

    virtual uint32_t getBitsInVector(int bits_id, void* vector_data){
    	return memoryMap_->getBitsInVector(bits_id, vector_data);
    }
    virtual void setBitsInVector(int bits_id, uint32_t bits_data, void* vector_data)  {
    	memoryMap_->setBitsInVector(bits_id, bits_data, vector_data);
    }


    // interfacing with the hardware
    virtual void SetHardwareIfc(IHardwareIfc* ifc, bool owner) {
    	memoryMap_->SetHardwareIfc(ifc, owner);
    }
    virtual IHardwareIfc* GetHardwareIfc(){
    	return memoryMap_->GetHardwareIfc();
    }
    

    virtual bool IsHardwareIfcOwner()  {
        return memoryMap_->IsHardwareIfcOwner();
    }
    
    virtual void SetHardwareIfcOwner(bool owner) {
        memoryMap_->SetHardwareIfcOwner(owner);
    }

    virtual const TItemsMap* GetItems() {
    	return memoryMap_->GetItems();
    }

    virtual const TItemDesc& getItemDesc(TID id){
    	return memoryMap_->getItemDesc(id);
    }

    virtual const TItemInfo& GetItemInfo(TID id) const {
    	return memoryMap_->GetItemInfo(id);
    }

    virtual int GetDataSize() {
    	return memoryMap_->GetDataSize();
    }

    virtual uint32_t GetChecksum() {
    	return memoryMap_->GetChecksum();
    }
};


}

#endif
