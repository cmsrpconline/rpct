/** Filip Thyssen */

#ifndef rpct_devices_FebBoard_inl_h_
#define rpct_devices_FebBoard_inl_h_

#include "rpct/devices/FebBoard.h"

namespace rpct {

inline unsigned char FebBoard::pos2address(unsigned char position
                                           , bool febtype)
{
    return (position << (febtype?1:0));
}
inline unsigned char FebBoard::chip2part(unsigned char chip)
{
    return (chip>1?1:0);
}
inline unsigned char FebBoard::chip2position(unsigned char chip)
{
    return chip%2;
}
inline int FebBoard::nparts() const
{
    return (febtype_?2:1);
}

inline int FebBoard::getId() const
{
    return id_;
}
inline const std::string & FebBoard::getDescription() const
{
    return description_;
}
inline const HardwareItemType & FebBoard::getType() const
{
    return *type_;
}
inline const IMonitorable::MonitorItems & FebBoard::getMonitorItems()
{
    return *mitems_;
}
inline void FebBoard::configure()
{
    configure(0x0);
}
inline void FebBoard::monitor(volatile bool *stop)
{
    monitor(*mitems_, 0x0);
}
inline void FebBoard::monitor(volatile bool *stop, int configureFlags)
{
    monitor(*mitems_, configureFlags);
}

inline rpct::i2c::II2cAccess & FebBoard::getI2cAccess() const
{
    return i2c_access_;
}
inline FebDistributionBoard & FebBoard::getFebDistributionBoard() const
{
    return febdistributionboard_;
}
inline unsigned short FebBoard::getChannel() const
{
    return channel_;
}
inline int FebBoard::getFebType() const
{
    return febtype_;
}
inline unsigned char FebBoard::getAddress() const
{
    return address_;
}

} // namespace rpct

#endif // rpct_devices_FebBoard_inl_h_
