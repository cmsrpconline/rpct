/** Filip Thyssen */

#ifndef rpct_devices_i2c_Block_inl_h_
#define rpct_devices_i2c_Block_inl_h_
#include <cstring>
#include "rpct/devices/i2c/Block.h"

namespace rpct {
namespace devices {
namespace i2c {

inline bool BI2cCommand::valid(int size) const
{
    if (read_)
        return (bi2c::buffer_size_ - size + in_) >= (out_ + length_);
    else
        return true;
}
inline void BI2cCommand::move(int in_distance, int out_distance, int group_distance)
{
    in_ += in_distance;
    if (read_)
        out_ += out_distance;
    if (group_ != 0) // not no group
        group_ += group_distance;
}

inline unsigned char & Block::channel()
{
    return properties_[group_mode_].channel_;
}
inline int & Block::group()
{
    return properties_[group_mode_].group_;
}
inline int & Block::input_size()
{
    return properties_[group_mode_].input_size_;
}
inline int & Block::output_size()
{
    return properties_[group_mode_].output_size_;
}
inline int & Block::nbi2c_commands()
{
    return properties_[group_mode_].nbi2c_commands_;
}

inline unsigned char Block::channel() const
{
    return properties_[group_mode_].channel_;
}
inline int Block::group() const
{
    return properties_[group_mode_].group_;
}
inline int Block::input_size() const
{
    return properties_[group_mode_].input_size_;
}
inline int Block::output_size() const
{
    return properties_[group_mode_].output_size_;
}
inline int Block::nbi2c_commands() const
{
    return properties_[group_mode_].nbi2c_commands_;
}

inline void Block::specialcommand(unsigned char * & out
                                  , unsigned char _channel
                                  , unsigned char count)
{
    out[0] = 0xff;
    if (_channel != channel())
        {
            out[1] = (((_channel << 4) & 0xf0) | (count & 0x0f));
            channel() = _channel;
        }
    else
        out[1] = (0xf0 | (count & 0x0f));
    out += 2;
}
inline void Block::delaycommand(unsigned char * & out
                                , unsigned char time)
{
    out[0] = 0x7f;
    out[1] = time;
    out += 2;
}
inline void Block::readcommand(unsigned char * & out
                               , unsigned char address)
{
    out[0] = (0x80 | address);
    ++out;
}
inline void Block::writecommand(unsigned char * & out
                                , unsigned char address
                                , unsigned char const * data
                                , unsigned char count)
{
    out[0] = (0x7f & address);
    ++out;
    memcpy(out, data, count*sizeof(unsigned char));
    out += count;
}

inline int Block::getGroup(int mode) const
{
    if (mode < 0 || mode > 1)
        return properties_[group_mode_].group_;
    else
        return properties_[mode].group_;
}
inline unsigned char Block::getChannel(int mode) const
{
    if (mode < 0 || mode > 1)
        return properties_[group_mode_].channel_;
    else
        return properties_[mode].channel_;
}
inline int Block::getInputSize(int mode) const
{
    if (mode < 0 || mode > 1)
        return properties_[group_mode_].input_size_;
    else
        return properties_[mode].input_size_;
}
inline int Block::getOutputSize(int mode) const
{
    if (mode < 0 || mode > 1)
        return properties_[group_mode_].output_size_;
    else
        return properties_[mode].output_size_;
}
inline int Block::getNBI2cCommands(int mode) const
{
    if (mode < 0 || mode > 1)
        return properties_[group_mode_].nbi2c_commands_;
    else
        return properties_[mode].nbi2c_commands_;
}

inline bool Block::groupMode() const
{
    return group_mode_;
}
} // namespace i2c
} // namespace devices
} // namespace rpct

#endif // rpct_devices_i2c_Block_inl_h_
