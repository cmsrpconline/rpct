/** Filip Thyssen */

#ifndef rpct_devices_i2c_BlockI2c_h_
#define rpct_devices_i2c_BlockI2c_h_

#include <log4cplus/logger.h>
#include <stdint.h>
#include "rpct/lboxaccess/CCUException.h"

#include "rpct/devices/i2c/const.h"
#include "rpct/devices/i2c/Block.h"

#include "rpct/ii/exception/Exception.h"
#include "rpct/ii/i2c/II2cAccess.h"

namespace rpct {

class TCB;
class StdLinkBoxAccess;

namespace devices {
namespace i2c {

class BlockI2c : public rpct::i2c::II2cAccess
{
private:
    /** recover from CCU exception */
    void iRecoverCCU()
        throw (rpct::exception::CCUException);

    unsigned short memoryRead(uint32_t address);
    void memoryWrite(uint32_t address, unsigned short data);
    void memoryReadBlock(uint32_t address, unsigned char * data,
                         uint32_t count);
    void memoryWriteBlock(uint32_t address, const unsigned char * data,
                          uint32_t count);

    /** wait for the bi2c to be ready, returns if it is
     * \param max_timesteps maximum number of timesteps to wait for ready bit
     */
    bool waitForReady(int max_timesteps = bi2c::preset::timeout_timesteps_)
        throw (rpct::exception::Exception);
    /** write the command block
     * \param readstatus read the status,
     *        only to be disabled if it is explicitly read before
     */
    void writeBlock(bool readstatus = true)
        throw (rpct::exception::Exception);

    /** wait for the last command block to end execution
     * \param max_timesteps maximum number of timesteps to wait for ack/err
     */
    bool waitForExecuted(int max_timesteps = bi2c::preset::timeout_timesteps_)
        throw (rpct::exception::Exception);

    /** read the buffer
     * \param readstatus read the status,
     *        only to be disabled if it is explicitly read before
     */
    void readBlock(bool readstatus = false)
        throw (rpct::exception::Exception);
    /** execute the commands, from start to end
     * \param readstatus read the status,
     *        only to be disabled if it is explicitly read before
     * \param recover_group
     * \param postprocess_error
     */
protected:
    void idelay(unsigned int nseconds)
        throw (rpct::exception::Exception);
    void iread(unsigned short channel
               , unsigned char address
               , unsigned char * data = 0
               , unsigned char count = 1
               , rpct::i2c::II2cResponse * i2c_response = 0)
        throw (rpct::exception::Exception);
    void iwrite(unsigned short channel
                , unsigned char address
                , unsigned char const * data
                , unsigned char count = 1
                , rpct::i2c::II2cResponse * i2c_response = 0)
        throw (rpct::exception::Exception);
public:
    static void setupLogger(std::string namePrefix);
    BlockI2c(rpct::TCB & controlboard, unsigned short prescaler = bi2c::preset::prescaler_);

    // II2cAccess
    void log(rpct::exception::Exception const * e) const;

    void init() throw (rpct::exception::Exception);
    void reset();
    void execute() throw (rpct::exception::Exception);

    void enable() throw (rpct::exception::Exception);
    void disable() throw (rpct::exception::Exception);

    void setPrescaler(unsigned short prescaler);
    unsigned short getPrescaler() const;
    void writePrescaler() throw (rpct::exception::Exception);

    bool startGroup();
    void cancelGroup();
    void endGroup();
    bool groupMode() const;

    // other
    rpct::TCB & getControlBoard() const;
    StdLinkBoxAccess * getLBoxAccess() const
        throw (rpct::exception::SystemException);

    Block & getBlock();
    const Block & getBlock() const;

    // status
    unsigned char getStatus() const;
    unsigned char getExtendedStatus() const;
    unsigned char getErrorCommand() const;
    bool isReady() const;
    bool isAck() const;
    bool isErr() const;

    void execute(bool recover_group
                 , bool postprocess_error = true)
        throw (rpct::exception::Exception);
    /** read the bi2c status */
    void readStatus()
        throw (rpct::exception::Exception);
    /** read the bi2c extended status */
    void readExtendedStatus()
        throw (rpct::exception::Exception);
    /** read the bi2c extended status */
    void readErrorCommand()
        throw (rpct::exception::Exception);

    friend std::ostream& operator<< (std::ostream& outstream, const BlockI2c& in);
private:
    static log4cplus::Logger logger_;
    rpct::TCB & controlboard_;

    unsigned short prescaler_;

    Block block_;

    unsigned char status_;
    unsigned char extended_status_;
    unsigned char error_command_;

    bool init_;
};
std::ostream& operator<< (std::ostream& outstream, const BlockI2c& in);

} // namespace i2c
} // namespace devices
} // namespace rpct

#include "rpct/devices/i2c/BlockI2c-inl.h"

#endif // rpct_devices_i2c_BlockI2c_h_
