/** Filip Thyssen */

#ifndef rpct_devices_i2c_const_h_
#define rpct_devices_i2c_const_h_

#include "rpct/ii/i2c/const.h"
#include "rpct/lboxaccess/cbpc_const.h"

namespace rpct {
namespace devices {
namespace i2c {

namespace bi2c {

const int buffer_size_ = rpct::CBPC::CBPC_BI2C_BUFE - rpct::CBPC::CBPC_BI2C_BUFB + 1;
const int command_size_ = 1;
const int delay_command_size_ = 2;
const int special_command_size_ = 2;

namespace preset {
extern unsigned short prescaler_;

extern bool bi2c_mode_;
extern bool ci2c_fallback_;

extern int sleep_timestep_useconds_;
extern int timeout_timesteps_;
} // namespace preset

} // namespace bi2c

extern const unsigned char max_channel_;

namespace preset {
extern unsigned short prescaler_;
} // namespace preset

} // namespace i2c
} // namespace devices
} // namespace rpct

#endif // rpct_devices_i2c_const_h_
