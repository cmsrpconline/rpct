/** Filip Thyssen */

#ifndef rpct_devices_i2c_Block_h_
#define rpct_devices_i2c_Block_h_

#include <ostream>

#include "rpct/ii/exception/Exception.h"
#include "rpct/devices/i2c/const.h"

namespace rpct {

namespace i2c {
class II2cResponse;
} // namespace i2c

namespace devices {
namespace i2c {

class BI2cCommand
{
public:
    BI2cCommand(bool read = true
                , int group = 0
                , int in = 0
                , int out = 0
                , unsigned char length = 1
                , unsigned char * data = 0
                , rpct::i2c::II2cResponse * i2c_response = 0);

    /** checks for buffer overruns in case the input buffer grows
     * \param size the size by which the input buffer would grow
     */
    bool valid(int size) const;
    /** moves the command's in and out positions */
    void move(int in_distance, int out_distance, int group_distance);
public:
    unsigned char length_;
    unsigned char * data_;
    bool read_; //< read = 1, write = 0
    rpct::i2c::II2cResponse * i2c_response_;
    int group_;
    int in_;
    int out_; // position in output buffer
};
struct BlockProperties
{
public:
    unsigned char channel_;
    int group_;
    int input_size_, output_size_, nbi2c_commands_;
    void reset();
};
class Block
{
protected:
    // internal functions that take into account group mode
    unsigned char & channel();
    int & group();
    int & input_size();
    int & output_size();
    int & nbi2c_commands();

    unsigned char channel() const;
    int group() const;
    int input_size() const;
    int output_size() const;
    int nbi2c_commands() const;

    /** create a special command
     * \param out      pointer to the unsigned char array to write to
     * \param _channel the channel of the next i2c commands
     * \param count    the transfer length of the next command
     */
    void specialcommand(unsigned char * & out
                        , unsigned char _channel
                        , unsigned char count = 1);
    /** create a delay command
     * \param out      pointer to the unsigned char array to write to
     * \param time     the time in unit 25ns * time * 16
     */
    void delaycommand(unsigned char * & out
                      , unsigned char time = 1);
    /** create a read command
     * \param out     pointer to the unsigned char array to write to
     * \param address the address for the i2c commands
     */
    static void readcommand(unsigned char * & out
                            , unsigned char address);
    /** create a read command
     * \param out     pointer to the unsigned char array to write to
     * \param address the address for the i2c command
     * \param data    the data to be written in the command
     * \param count   the transfer length of the command
     */
    static void writecommand(unsigned char * & out
                             , unsigned char address
                             , unsigned char const * data
                             , unsigned char count = 1);

public:
    Block();

    int getGroup(int mode = -1) const;
    unsigned char getChannel(int mode = -1) const;
    int getInputSize(int mode = -1) const;
    int getOutputSize(int mode = -1) const;
    int getNBI2cCommands(int mode = -1) const;

    unsigned char * getInput(int start = 0)
        throw (rpct::exception::Exception);
    unsigned char const * getInput(int start = 0) const
        throw (rpct::exception::Exception);
    unsigned char * getOutput(int start = 0)
        throw (rpct::exception::Exception);
    unsigned char const * getOutput(int start = 0) const
        throw (rpct::exception::Exception);
    BI2cCommand * getBI2cCommand(int start = 0)
        throw (rpct::exception::Exception);
    BI2cCommand const * getBI2cCommand(int start = 0) const
        throw (rpct::exception::Exception);

    /** postprocess the block */
    void postprocess(rpct::exception::Exception const * e = 0);
    /** start a  new command block */
    void reset(bool recover_group = false);

    /** start a new group */
    bool startGroup();
    /** cancel the started group - it won't be added to the actual block */
    void cancelGroup();
    /** end a group - adds it to the actual block */
    void endGroup();
    /** in group mode ? */
    bool groupMode() const;

    /** add a delay command to the block / started group
     * \param nseconds the delay in nanoseconds; will be rounded.
     */
    void delay(unsigned int nseconds = rpct::i2c::preset::delay_nseconds_)
        throw (rpct::exception::Exception);
    /** add a read command to the block / started group
     * \param _channel the channel
     * \param address the address
     * \param data    pointer to location to store read data
     * \param count   the transfer length
     */
    void read(unsigned char _channel
              , unsigned char address
              , unsigned char * data = 0
              , unsigned char count = 1
              , rpct::i2c::II2cResponse * i2c_response = 0)
        throw (rpct::exception::Exception);
    /** add a write command to the block / started group
     * \param _channel the channel
     * \param address the address
     * \param data    the data
     * \param count   the transfer length
     */
    void write(unsigned char _channel
               , unsigned char address
               , unsigned char const * data
               , unsigned char count = 1
               , rpct::i2c::II2cResponse * i2c_response = 0)
        throw (rpct::exception::Exception);

    friend std::ostream & operator<< (std::ostream& outstream, const Block& in);
protected:
    int group_mode_;
    BlockProperties properties_[2]; // 0: normal, 1: groupmode

    unsigned char input_[bi2c::buffer_size_], output_[bi2c::buffer_size_];
    BI2cCommand bi2c_commands_[bi2c::buffer_size_];
};
std::ostream& operator<< (std::ostream& outstream, const Block& in);

} // namespace i2c
} // namespace devices
} // namespace rpct

#include "rpct/devices/i2c/Block-inl.h"

#endif // rpct_devices_i2c_Block_h_
