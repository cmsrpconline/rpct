/** Filip Thyssen */

#ifndef rpct_devices_i2c_CommandI2c_h_
#define rpct_devices_i2c_CommandI2c_h_

#include <log4cplus/logger.h>

#include "rpct/devices/i2c/const.h"

#include "rpct/ii/exception/Exception.h"
#include "rpct/ii/i2c/II2cAccess.h"

namespace rpct {

class TCB;
class TI2CInterface;
class TI2CMasterCorev09;

namespace devices {
namespace i2c {

class CommandI2c : public rpct::i2c::II2cAccess
{
protected:
    void selectChannel(unsigned char channel)
        throw (rpct::exception::Exception);

    TCB & getControlBoard() const;
    rpct::TI2CMasterCorev09 * getI2c()
        throw (rpct::exception::SystemException);

    void idelay(unsigned int nseconds)
        throw (rpct::exception::Exception);
    void iread(unsigned short channel
               , unsigned char address
               , unsigned char * data = 0
               , unsigned char count = 1
               , rpct::i2c::II2cResponse * i2c_response = 0)
        throw (rpct::exception::Exception);
    void iwrite(unsigned short channel
                , unsigned char address
                , unsigned char const * data
                , unsigned char count = 1
                , rpct::i2c::II2cResponse * i2c_response = 0)
        throw (rpct::exception::Exception);
public:
    static void setupLogger(std::string namePrefix);
    CommandI2c(TCB & controlboard, unsigned short prescaler = preset::prescaler_);

    // II2cAccess
    void log(rpct::exception::Exception const * e) const;

    void init() throw (rpct::exception::Exception);
    void reset();

    void enable() throw (rpct::exception::Exception);
    void disable() throw (rpct::exception::Exception);

    void setPrescaler(unsigned short prescaler);
    unsigned short getPrescaler() const;
    void writePrescaler() throw (rpct::exception::Exception);

    bool startGroup();
    void cancelGroup();
    void endGroup();
    bool groupMode() const;
protected:
    static log4cplus::Logger logger_;

    TCB & controlboard_;
    rpct::TI2CMasterCorev09 * i2c_;
    unsigned short prescaler_;
    unsigned char channel_;

    bool group_mode_;
    bool error_;
};

} // namespace i2c
} // namespace devices
} // namespace rpct

#include "rpct/devices/i2c/CommandI2c-inl.h"

#endif // rpct_devices_i2c_CommandI2c_h_
