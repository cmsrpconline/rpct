/** Filip Thyssen */

#ifndef rpct_devices_i2c_CommandI2c_inl_h_
#define rpct_devices_i2c_CommandI2c_inl_h_

#include "rpct/devices/i2c/CommandI2c.h"

#include "log4cplus/loggingmacros.h"

namespace rpct {
namespace devices {
namespace i2c {

inline void CommandI2c::log(rpct::exception::Exception const * e) const
{
    if (e)
        {
            LOG4CPLUS_WARN(logger_, e->what());
        }
    else
        {
            LOG4CPLUS_WARN(logger_, "CommandI2c log called without exception description.");
        }
}

inline void CommandI2c::reset()
{
    channel_ = max_channel_ + 1;
    group_mode_ = 0;
    error_ = 0;
}

inline void CommandI2c::setPrescaler(unsigned short prescaler)
{
    prescaler_ = prescaler;
}
inline unsigned short CommandI2c::getPrescaler() const
{
    return prescaler_;
}

inline bool CommandI2c::startGroup()
{
    if (group_mode_)
        return false;
    else
        {
            error_ = 0;
            return group_mode_ = true;
        }
}
inline void CommandI2c::cancelGroup()
{
    group_mode_ = false;
    error_ = 0;
}
inline void CommandI2c::endGroup()
{
    group_mode_ = false;
    error_ = 0;
}

inline bool CommandI2c::groupMode() const
{
    return group_mode_;
}

} // namespace i2c
} // namespace devices
} // namespace rpct

#endif // rpct_devices_i2c_CommandI2c_inl_h_
