/** Filip Thyssen */

#ifndef rpct_devices_i2c_BlockI2c_inl_h_
#define rpct_devices_i2c_BlockI2c_inl_h_

#include "rpct/devices/i2c/BlockI2c.h"

#include "log4cplus/loggingmacros.h"

namespace rpct {
namespace devices {
namespace i2c {

inline void BlockI2c::log(rpct::exception::Exception const * e) const
{
    if (e)
        {
            LOG4CPLUS_WARN(logger_, e->what());
        }
    else
        {
            LOG4CPLUS_WARN(logger_, "BlockI2c log called without exception description.");
        }
}

inline void BlockI2c::execute() throw (rpct::exception::Exception)
{
    execute(true);
}

inline unsigned short BlockI2c::getPrescaler() const
{
    return prescaler_;
}
inline void BlockI2c::setPrescaler(unsigned short prescaler)
{
    prescaler_ = prescaler;
}

inline bool BlockI2c::startGroup()
{
    return block_.startGroup();
}
inline void BlockI2c::cancelGroup()
{
    block_.cancelGroup();
}
inline void BlockI2c::endGroup()
{
    block_.endGroup();
}
inline bool BlockI2c::groupMode() const
{
    return block_.groupMode();
}

inline rpct::TCB & BlockI2c::getControlBoard() const
{
    return controlboard_;
}

inline Block & BlockI2c::getBlock()
{
    return block_;
}
inline const Block & BlockI2c::getBlock() const
{
    return block_;
}

inline unsigned char BlockI2c::getStatus() const
{
    return status_;
}
inline unsigned char BlockI2c::getExtendedStatus() const
{
    return extended_status_;
}
inline unsigned char BlockI2c::getErrorCommand() const
{
    return error_command_;
}
inline bool BlockI2c::isReady() const
{
    return (status_ >> rpct::CBPC::BI2C_ST_READY) & 0x1;
}
inline bool BlockI2c::isAck() const
{
    return (status_ >> rpct::CBPC::BI2C_ST_ACK) & 0x1;
}
inline bool BlockI2c::isErr() const
{
    return (status_ >> rpct::CBPC::BI2C_ST_ERR) & 0x1;
}

} // namespace i2c
} // namespace devices
} // namespace rpct

#endif // rpct_devices_i2c_BlockI2c_inl_h_
