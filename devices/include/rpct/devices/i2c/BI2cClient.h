/** Filip Thyssen */

#ifndef rpct_devices_i2c_BI2cClient_h_
#define rpct_devices_i2c_BI2cClient_h_

#include <log4cplus/logger.h>

#include "rpct/devices/i2c/const.h"

#include "rpct/ii/exception/Exception.h"
#include "rpct/ii/i2c/II2cAccess.h"

namespace rpct {

class TCB;

namespace devices {
namespace i2c {

class CommandI2c;
class BlockI2c;
class Block;

class BI2cClient : public rpct::i2c::II2cAccess
{
protected:
    void parseBlockCommands(Block & block);

    void idelay(unsigned int nseconds)
        throw (rpct::exception::Exception);
    void iread(unsigned short channel
               , unsigned char address
               , unsigned char * data = 0
               , unsigned char count = 1
               , rpct::i2c::II2cResponse * i2c_response = 0)
        throw (rpct::exception::Exception);
    void iwrite(unsigned short channel
                , unsigned char address
                , unsigned char const * data
                , unsigned char count = 1
                , rpct::i2c::II2cResponse * i2c_response = 0)
        throw (rpct::exception::Exception);

public:
    static void setupLogger(std::string namePrefix);
    BI2cClient(TCB & controlboard
               , bool bi2c_mode = bi2c::preset::bi2c_mode_
               , bool ci2c_fallback = bi2c::preset::ci2c_fallback_);

    // II2cAccess
    void log(rpct::exception::Exception const * e) const;

    void init() throw (rpct::exception::Exception);
    void reset();
    void execute() throw (rpct::exception::Exception);

    void enable() throw (rpct::exception::Exception);
    void disable() throw (rpct::exception::Exception);

    void setPrescaler(unsigned short prescaler);
    unsigned short getPrescaler() const;
    void writePrescaler() throw (rpct::exception::Exception);

    bool startGroup();
    void cancelGroup();
    void endGroup();
    bool groupMode() const;

    // other
    bool getBi2cMode() const;
    /** executes remaining block, changes mode and inits if necessary */
    void setBi2cMode(bool bi2c_mode = bi2c::preset::bi2c_mode_)
        throw (rpct::exception::Exception);
    bool getCi2cFallback() const;
    /** inits if necessary */
    void setCi2cFallback(bool ci2c_fallback = bi2c::preset::ci2c_fallback_)
        throw (rpct::exception::Exception);
protected:
    static log4cplus::Logger logger_;

    TCB & controlboard_;
    CommandI2c & commandi2c_;
    BlockI2c & blocki2c_;

    bool init_;
    bool bi2c_mode_;
    bool ci2c_fallback_;
};

} // namespace i2c
} // namespace devices
} // namespace rpct

#include "rpct/devices/i2c/BI2cClient-inl.h"

#endif // rpct_devices_i2c_BI2cClient_h_
