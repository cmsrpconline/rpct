/** Filip Thyssen */

#ifndef rpct_devices_i2c_BI2cClient_inl_h_
#define rpct_devices_i2c_BI2cClient_inl_h_

#include "rpct/devices/i2c/BI2cClient.h"

#include "rpct/devices/i2c/CommandI2c.h"
#include "rpct/devices/i2c/BlockI2c.h"

namespace rpct {
namespace devices {
namespace i2c {

class BlockI2c;

inline void BI2cClient::log(rpct::exception::Exception const * e) const
{
    if (e)
        {
            LOG4CPLUS_WARN(logger_, e->what());
        }
    else
        {
            LOG4CPLUS_WARN(logger_, "BI2cClient log called without exception description.");
        }
}

inline bool BI2cClient::startGroup()
{
    if (bi2c_mode_)
        return blocki2c_.startGroup();
    else
        return commandi2c_.startGroup();
}
inline void BI2cClient::cancelGroup()
{
    if (bi2c_mode_)
        blocki2c_.cancelGroup();
    else
        commandi2c_.cancelGroup();
}
inline void BI2cClient::endGroup()
{
    if (bi2c_mode_)
        blocki2c_.endGroup();
    else
        commandi2c_.endGroup();
}
inline bool BI2cClient::groupMode() const
{
    if (bi2c_mode_)
        return blocki2c_.groupMode();
    else
        return commandi2c_.groupMode();
}
inline bool BI2cClient::getBi2cMode() const
{
    return bi2c_mode_;
}

inline bool BI2cClient::getCi2cFallback() const
{
    return ci2c_fallback_;
}

} // namespace i2c
} // namespace devices
} // namespace rpct

#endif // rpct_devices_i2c_BI2cClient_inl_h_
