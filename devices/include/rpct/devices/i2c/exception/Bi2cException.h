/** Filip Thyssen */

#ifndef rpct_devices_i2c_exception_Bi2cException_h_
#define rpct_devices_i2c_exception_Bi2cException_h_

#include "rpct/ii/exception/Exception.h"

namespace rpct {
namespace devices {
namespace i2c {
namespace exception {

class Bi2cException : public virtual rpct::exception::Exception
{
public:
    Bi2cException(const std::string & msg = "") throw()
        : rpct::exception::Exception("bi2c: " + msg)
    {}
    ~Bi2cException() throw() {}
};

class Bi2cErrorException : public virtual Bi2cException
{
public:
    Bi2cErrorException(const std::string & msg = "") throw()
        : Bi2cException("error: " + msg)
    {}
    ~Bi2cErrorException() throw() {}
};
/** Block buffer problems */
class BufferException : public virtual Bi2cException
{
public:
    BufferException(const std::string & msg = "") throw()
        : Bi2cException("buffer: " + msg)
    {}
    ~BufferException() throw() {}
};
class InputBufferException : public virtual BufferException
{
public:
    InputBufferException(const std::string & msg = "") throw()
        : BufferException("input: " + msg)
    {}
    ~InputBufferException() throw() {}
};
class OutputBufferException : public virtual BufferException
{
public:
    OutputBufferException(const std::string & msg = "") throw()
        : BufferException("output: " + msg)
    {}
    ~OutputBufferException() throw() {}
};
class BufferOverRunException : public virtual BufferException
{
public:
    BufferOverRunException(const std::string & msg = "") throw()
        : BufferException("overrun: " + msg)
    {}
    ~BufferOverRunException() throw() {}
};

} // namespace exception
} // namespace i2c
} // namespace devices
} // namespace rpct

#endif // rpct_devices_i2c_exception_Bi2cException_h_
