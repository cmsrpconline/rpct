/*
 * ErrorCounterAnalyzer.h
 *
 *  Created on: Oct 2, 2008
 *      Author: mpietrus
 */

#ifndef RPCTERRORCOUNTERANALYZER_H_
#define RPCTERRORCOUNTERANALYZER_H_

#include "rpct/ii/Monitorable.h"
#include <boost/date_time/posix_time/ptime.hpp>

namespace rpct {

class ErrorCounterAnalyzer {
public:
    ErrorCounterAnalyzer(std::string name);
    ErrorCounterAnalyzer(std::string name, double minimumPeriod, double warningRateThreshold, double errorRateThreshold, uint32_t maxCounterVal);
    ErrorCounterAnalyzer(std::string name, double minimumPeriod, unsigned int warningCntThreshold, unsigned int errorCntThreshold, double warningRateThreshold, double errorRateThreshold, uint32_t maxCounterVal);
    virtual ~ErrorCounterAnalyzer();

    double getMinimumPeriod() const {
        return minimumPeriod_;
    }

    double getWarningRateThreshold() const {
        return warningRateThreshold_;
    }

    double getErrorRateThreshold() const {
        return errorRateThreshold_;
    }

    void setMaxCounterVal(uint32_t maxCounterVal) {
        maxCounterVal_ = maxCounterVal;
    }

    uint32_t getMaxCounterVal() {
        return maxCounterVal_;
    }

    const boost::posix_time::ptime& getLastTime() const {
        return lastTime_;
    }

    uint32_t getLastVal() const {
        return lastVal_;
    }

    int newValue(uint32_t value);


    MonitorableStatus& getStatus() {
        return status_;
    }

    void reset();

private:
    double minimumPeriod_;
    unsigned int warningCntThreshold_;
    unsigned int errorCntThreshold_;
    double warningRateThreshold_;
    double errorRateThreshold_;
    uint32_t maxCounterVal_;
    boost::posix_time::ptime lastTime_;
    uint32_t lastVal_;
    MonitorableStatus status_;
};

}

#endif /* ERRORCOUNTERANALYZER_H_ */
