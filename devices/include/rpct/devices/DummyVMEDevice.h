#ifndef Dummy_VMEDevice_H
#define Dummy_VMEDevice_H
//---------------------------------------------------------------------------
//
//  Michal Bluj 2007, Warsaw
//
//---------------------------------------------------------------------------

#include "rpct/ii/IDevice.h"
#include "rpct/ii/IBoard.h"
#include "vme/TVMEInterface.h"
#include "rpct/std/IllegalArgumentException.h"
#include "rpct/std/ENotImplemented.h"
#include <sstream>
#include <log4cplus/logger.h>

namespace rpct {

class TDummyVMEDevice : virtual public IDevice {
 private:
  static log4cplus::Logger logger;
 protected:
  int id;
  std::string Name;
  std::string description;
  HardwareItemType type_;
  uint32_t Version;
  uint32_t BaseAddress;
  uint32_t BoardAddress;
  IBoard& Board;
  int position_;
  TVMEInterface* vme_;
  std::string FullName;

 public:
  TDummyVMEDevice(int ident,
		  std::string name,
		  std::string desc,
		  HardwareItemType type,
		  std::string ver,
		  uint32_t base_address,
		  uint32_t board_address,
		  IBoard& board,
		  int position,
		  TVMEInterface* vme)
    : id(ident), Name(name), description(desc), type_(type), 
    BaseAddress(base_address), BoardAddress(board_address), Board(board), 
    position_(position), vme_(vme) {
    if (vme_==NULL)
      throw TException("TDummyVMEDevice::TDummyVMEDevice: vme+==NULL");
    sscanf(ver.c_str(), "%ix", &Version);
    FullName = Board.getDescription() + "/" + Name;
  }

  virtual ~TDummyVMEDevice(){
    delete vme_;//??
  }

  virtual uint32_t getBaseAddress() {
    return BaseAddress;
  }

  virtual int getId() const {
    return id;
  }

  virtual const std::string& getName() {
    return Name;
  }

  virtual const std::string& getDescription() const {
    return description;
  }

  virtual const HardwareItemType & getType() const {
    return type_;
  }

  virtual uint32_t getVersion() {
    return Version;
  }

  virtual TID getVersionId() {
    throw ENotImplemented("TDummyVMEDevice::getVersionId()");
  }

  virtual IBoard& getBoard() {
    return Board;
  }

  virtual int getPosition() {
    return position_;
  }

  virtual const TItemsMap& getItems() {
    throw ENotImplemented("TDummyVMEDevice::getItems()");
  }

  virtual const TItemDesc& getItemDesc(TID id) {
    throw ENotImplemented("TDummyVMEDevice::getItemDesc(TID)");
  }

  TVMEInterface* getVME(){
    return vme_;
  }

  virtual void checkName() = 0;

  virtual void checkVersion() = 0;

  //
  // hardware operations
  //

  // word operations
  virtual uint32_t readWord(TID id, int idx) {
    throw ENotImplemented("TDummyVMEDevice::readWord(TID, int)");
  }

  virtual void readWord(TID id, int idx, void* data) {
    throw ENotImplemented("TDummyVMEDevice::readWord(TID, int, void*)");
  }

  virtual void writeWord(TID id, int idx, uint32_t data) {
    throw ENotImplemented("TDummyVMEDevice::writeWord(TID, int, uint32_t)");
  }
  virtual void writeWord(TID* id, int* idx, uint32_t* data, int count) {
	    throw ENotImplemented("TDummyVMEDevice::writeWord(TID, int, uint32_t)");
  }

  virtual void writeWord(TID id, int idx, void* data) {
    throw ENotImplemented("TDummyVMEDevice::writeWord(TID, int, void*)");
  }

  virtual void writeWord(TID id, int idx, boost::dynamic_bitset<>& data) {
    throw ENotImplemented("TDummyVMEDevice::writeWord(TID, int, boost::dynamic_bitset<>&)");
  }

  // area operations
  virtual void writeArea(TID id, void* data, int start_idx = 0, int n = npos) {
    throw ENotImplemented("TDummyVMEDevice::writeArea(TID, void*, int, int)");
  }

  virtual void readArea(TID id, void* data, int start_idx = 0, int n = npos) {
    throw ENotImplemented("TDummyVMEDevice::readArea(TID, void*, int, int)");
  }

  virtual void writeArea(TID id, uint32_t* data, int start_idx = 0, int n = npos) {
    throw ENotImplemented("TDummyVMEDevice::writeArea(TID, uint32_t*, int, int)");
  }

  virtual void readArea(TID id, uint32_t* data, int start_idx = 0, int n = npos)  {
    throw ENotImplemented("TDummyVMEDevice::readArea(TID, uint32_t*, int, int)");
  }

  //vector operations
  virtual uint32_t readVector(TID id) {
    throw ENotImplemented("TDummyVMEDevice::readVector(TID)");
  }

  virtual void readVector(TID id, void* data) {
    throw ENotImplemented("TDummyVMEDevice::readVector(TID, void*)");
  }

  virtual void writeVector(TID id, uint32_t data) {
    throw ENotImplemented("TDummyVMEDevice::writeVector(TID, uint32_t)");
  }
  
  virtual void writeVector(TID* id, uint32_t* data, int count) {
    throw ENotImplemented("TDummyVMEDevice::writeVector(TID, uint32_t)");
  }

  virtual void writeVector(TID id, void* data) {
    throw ENotImplemented("TDummyVMEDevice::writeVector(TID, void*)");
  }

  // bit operations
  virtual uint32_t readBits(TID id) {
    throw ENotImplemented("TDummyVMEDevice::readBits(TID)");
  }

  virtual void writeBits(TID id, uint32_t data, bool preread = true) {
    throw ENotImplemented("TDummyVMEDevice::writeBits(TID, uint32_t, bool)");
  }

  virtual uint32_t getBitsInVector(TID bits_id, uint32_t vector_data) {
    throw ENotImplemented("TDummyVMEDevice::getBitsInVector(TID, uint32_t)");
  }

  virtual void setBitsInVector(TID bits_id, uint32_t bits_data, uint32_t& vector_data) {
    throw ENotImplemented("TDummyVMEDevice::setBitsInVector(TID, uint32_t, uint32_t&)");
  }

  virtual uint32_t getBitsInVector(TID bits_id, void* vector_data) {
    throw ENotImplemented("TDummyVMEDevice::getBitsInVector(TID, void*)");
  }

  virtual void setBitsInVector(TID bits_id, uint32_t bits_data, void* vector_data) {
    throw ENotImplemented("TDummyVMEDevice::setBitsInVector(TID, uint32_t, void*)");
  }

  virtual void registerObserver(IObserver& obs, TID id = -1) {
    throw ENotImplemented("TDummyVMEDevice::registerObserver(IObserver&, TID)");
  }

  virtual void unregisterObserver(IObserver& obs, TID id = -1) {
    throw ENotImplemented("TDummyVMEDevice::unregisterObserver(IObserver&, TID)");
  }

};

}

#endif
