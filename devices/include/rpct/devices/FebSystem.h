/** Filip Thyssen */

#ifndef rpct_devices_FebSystem_h_
#define rpct_devices_FebSystem_h_

#include <map>
#include <string>
#include <vector>

#include <log4cplus/logger.h>

#include "rpct/tools/Publisher.h"
#include "rpct/tools/Time.h"
#include "rpct/tools/Timer.h"
#include "rpct/tools/Mutex.h"

#include "rpct/ii/HardwareItemBase.h"
#include "rpct/ii/Monitorable.h"
#include "rpct/ii/Configurable.h"

#include "rpct/devices/FebSystemItem.h"

#include "rpct/ii/exception/Exception.h"

namespace rpct {

class IFebAccessPoint;

class FebSystem
    : public FebSystemItem
    , public tools::Publisher
    , public IMonitorable
    , public Configurable
{
protected:
    typedef std::map<std::string, std::map<int, FebSystemItem *> > fsis_type;
    typedef std::map<int, IFebAccessPoint *> faps_type;
    typedef std::map<int, IMonitorable *> monitorunit_type;
public:
    static void setupLogger(std::string namePrefix);
    FebSystem();
    ~FebSystem();

    void reset();

    void addItem(FebSystemItem & fsi);
    void removeItem(FebSystemItem & fsi);

    std::map<int, FebSystemItem *> const & getFebSystemItems(std::string const & _type) const;
    FebSystemItem * getItem(const std::string & type, int id) const;

    IFebAccessPoint * getFebAccessPoint(int id) const;

    std::vector<int> getChipIds() const;

    // Publisher
    // Placeholder when the effective implementation (xdaqFebSystem)
    // is destroyed before this can remove itself as a publisher from the LogCenter
    void publish(int id, int type, double value);

    // HardwareItem
    int getId() const;
    const std::string & getDescription() const;
    const HardwareItemType & getType() const;

    void setDescription(const std::string & description);

    // Monitorable
    const MonitorItems & getMonitorItems();
    void monitor(volatile bool *stop);
    void monitor(MonitorItems & items);

    void monitorcycle();
    // Configurable
    void configure(ConfigurationSet * configurationSet, int configureFlags);
    void configure();

    void loadConfiguration(ConfigurationSet * configurationSet, int configureFlags);
    // Printable
    void printParameters(tools::Parameters & parameters) const;

    rpct::tools::Time & getConfigurationTime();
    rpct::tools::Time & getMonitoringTime();
    rpct::tools::Timer & getMonitorCycleTimer();
    
    virtual std::string getStatusSummary() const;

    bool getInterrupt() const;
    void setInterrupt(bool interrupt);

private:
    static log4cplus::Logger logger_;
    static HardwareItemType * type_;
protected:
    static MonitorItems * mitems_;

    int id_;
    std::string description_;

    int monitorcycle_;

    fsis_type fsis_;
    faps_type faps_;
    monitorunit_type mus_;

    rpct::tools::Time configuration_time_, monitoring_time_;
    rpct::tools::Timer monitorcycle_timer_;

    volatile bool interrupt_;
};

} // namespace rpct

#include "rpct/devices/FebSystem-inl.h"

#endif // rpct_devices_FebSystem_h_
