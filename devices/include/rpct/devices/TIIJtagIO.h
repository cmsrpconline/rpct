//---------------------------------------------------------------------------
#ifndef TIIJTAGIOH
#define TIIJTAGIOH
//---------------------------------------------------------------------------

#include "tb_bs.h"
#include "tb_ii.h"
#include <log4cplus/logger.h>

namespace rpct {

class TIIJtagIO : public IJtagIO {
private:
	static log4cplus::Logger logger_;

	typedef IDevice::TID TID;

	TIIDevice& Owner;
	TID VECT_JTAG;
	TID BITS_JTAG_TDI;
	TID BITS_JTAG_TDO;
	TID BITS_JTAG_TCK;
	TID BITS_JTAG_TMS;

	bool AutoClock;
	int Channel;
	bool Caching;
	const size_t cacheSize;
	TID* tids;
	uint32_t* cache;
	size_t nextCachePos;
	
	std::ofstream* dumpFile;

	// returns true if cache is full and flush is done before caching
	bool AddToCache(uint32_t val);
	
	void ProcessData(uint32_t data);
public:
	static void setupLogger(std::string namePrefix);
	TIIJtagIO(TIIDevice& owner, TID vectJtag, TID bitsJtagTDI, TID bitsJtagTDO,
			TID bitsJtagTCK, TID bitsJtagTMS, bool caching = false,
			bool autoclock = true, int channel = 0) :
		Owner(owner), VECT_JTAG(vectJtag), BITS_JTAG_TDI(bitsJtagTDI),
				BITS_JTAG_TDO(bitsJtagTDO), BITS_JTAG_TCK(bitsJtagTCK),
				BITS_JTAG_TMS(bitsJtagTMS), AutoClock(autoclock),
				Channel(channel), cacheSize(100), tids(0), cache(0), nextCachePos(0), dumpFile(0) {
		SetCaching(caching);
	}

	virtual ~TIIJtagIO() {
		delete [] tids;
		delete [] cache;
	}

	virtual bool IsCaching();
	virtual void SetCaching(bool caching);
	virtual void Flush();

	virtual bool JtagIO(bool tms, bool tdo, bool read_tdi);
	//virtual void JtagIO(bool* tms, bool* tdi, int count);
	virtual bool JtagIOLoop(bool tms);
	virtual int GetChannel() {
		return Channel;
	}
	virtual void SetChannel(int channel) {
		Channel = channel;
	}

	void setDumpFile(std::ofstream* file) {
		dumpFile = file;
	}
};

}

#endif
