/** Filip Thyssen */

#ifndef rpct_devices_AD5316_h_
#define rpct_devices_AD5316_h_

#include "rpct/ii/i2c/Register.h"

namespace rpct {

/**
 * bits dac register: xx00 aaaa 0111 vvvv vvvv vv00
 * x: undefined, a: addresspointer, v: 10 bit value
 **/
class DACRegister : public rpct::i2c::TWRegister<3>
{ // AD5316
public:
    void ptr(unsigned char ptr);
    void add_ptr(unsigned char ptr);
    void value(uint16_t value);
};

inline void DACRegister::ptr(unsigned char ptr)
{
    in_[0] = (0x0f & ptr);
}
inline void DACRegister::add_ptr(unsigned char ptr)
{
    in_[0] |= (0x0f & ptr);
}
inline void DACRegister::value(uint16_t value)
{
    ++value; // correct "round"
    // 10 bit limit with 1 bit shift
    if (value > 0x7fe)
        value = 0x7fe;
    // only half the accuracy of the default unit (2.5/1.024)mV,
    // so these two imply a >> 1 (>>6 and <<2)
    in_[1] = 0x70 + ((value >> 7) & 0x0f);
    in_[2] = ((value << 1) & 0xfc);
}

} // namespace rpct

#endif // rpct_devices_AD5316_h_
