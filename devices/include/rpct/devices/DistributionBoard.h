//---------------------------------------------------------------------------

//
//  Michal Pietrusinski 2006
//  Warsaw University
//

#ifndef rpctdistributionboardH
#define rpctdistributionboardH

#include "rpct/i2c/TI2CInterface.h"

namespace rpct {

class DistributionBoard {
protected:
    TI2CInterface* i2c;
    bool febAccessEnabled;
    virtual void checkI2C();
    virtual void sleep();
    
public:                
    //static int Delay;
    DistributionBoard(TI2CInterface* i2c);
    virtual ~DistributionBoard();
    
    TI2CInterface* getI2C();
    virtual void setI2C(TI2CInterface* i2c);

    virtual void enableFebAccess(bool value);    
     
    virtual bool isFebAccessEnabled() {
        return febAccessEnabled;
    }
};


}

#endif
