#ifndef RPCTRMBSETTINGSIMPL_H_
#define RPCTRMBSETTINGSIMPL_H_

#include <boost/dynamic_bitset.hpp>
#include "rpct/devices/RmbSettings.h"
#include "rpct/devices/TriggerBoard.h"

namespace rpct {
    

class RmbSettingsImpl : virtual public RmbSettings {
private:
    boost::dynamic_bitset<> recMuxClkInv_;
    boost::dynamic_bitset<> recMuxClk90_;
    boost::dynamic_bitset<> recMuxRegAdd_;
    std::vector<int> recMuxDelay_;
    std::vector<int> recDataDelay_;
    
    int chanEna_;
    int preTriggerVal_;
    int postTriggerVal_;
    int dataDelay_;
    int trgDelay_;
public:
    RmbSettingsImpl()
    : recMuxClkInv_(Rmb::TB_OPTO_MUX_LINE_NUM), recMuxClk90_(Rmb::TB_OPTO_MUX_LINE_NUM),
    recMuxRegAdd_(Rmb::TB_OPTO_MUX_LINE_NUM), 
    recMuxDelay_(Rmb::TB_LINK_NUM, 0), recDataDelay_(Rmb::TB_LINK_NUM, 0),
    chanEna_(0), preTriggerVal_(0), postTriggerVal_(0),  dataDelay_(0), trgDelay_(0) {
    }
    
    RmbSettingsImpl(
            boost::dynamic_bitset<> recMuxClkInv,
            boost::dynamic_bitset<> recMuxClk90,
            boost::dynamic_bitset<> recMuxRegAdd,
            std::vector<int> recMuxDelay,
            std::vector<int> recDataDelay,
            int chanEna, int preTriggerVal, int postTriggerVal, int dataDelay, int trgDelay)
    : recMuxClkInv_(recMuxClkInv), recMuxClk90_(recMuxClk90), 
    recMuxRegAdd_(recMuxRegAdd), recMuxDelay_(recMuxDelay), recDataDelay_(recDataDelay),
    chanEna_(chanEna), preTriggerVal_(preTriggerVal), postTriggerVal_(postTriggerVal),
    dataDelay_(dataDelay), trgDelay_(trgDelay) {
        if ((int) recMuxClkInv.size() != Rmb::TB_OPTO_MUX_LINE_NUM) {
            throw IllegalArgumentException("Invalid length of recMuxClkInv");
        }
        if ((int) recMuxClk90.size() != Rmb::TB_OPTO_MUX_LINE_NUM) {
            throw IllegalArgumentException("Invalid length of recMuxClk90");
        }
        if ((int) recMuxRegAdd.size() != Rmb::TB_OPTO_MUX_LINE_NUM) {
            throw IllegalArgumentException("Invalid length of recMuxRegAdd");
        }
        if ((int) recMuxDelay.size() != Rmb::TB_LINK_NUM) {
            throw IllegalArgumentException("Invalid length of recMuxDelay");
        }
        if ((int) recDataDelay.size() != Rmb::TB_LINK_NUM) {
            throw IllegalArgumentException("Invalid length of recDataDelay");
        }
    }
    
    virtual ~RmbSettingsImpl() {
    }

    boost::dynamic_bitset<>& getRecMuxClkInv() {
        return recMuxClkInv_;
    }

    boost::dynamic_bitset<>& getRecMuxClk90() {
        return recMuxClk90_;
    }
    
    boost::dynamic_bitset<>& getRecMuxRegAdd() {
        return recMuxRegAdd_;
    }
    
    std::vector<int>& getRecMuxDelay() {
        return recMuxDelay_;
    }
    
    std::vector<int>& getRecDataDelay() {
        return recDataDelay_;
    }
    
    virtual int getChanEna() {
        return chanEna_;
    }
    
    virtual int getPreTriggerVal() {
        return preTriggerVal_;
    }
    
    virtual int getPostTriggerVal() {
        return postTriggerVal_;
    }
        
    virtual int getDataDelay() {
        return dataDelay_;
    }
    
    virtual int getTrgDelay() {
        return trgDelay_;
    }
    
    virtual HardwareItemType getDeviceType() {
        return Rmb::TYPE;
    }
};

}

#endif 
