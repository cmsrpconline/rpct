//---------------------------------------------------------------------------

//
//  Michal Pietrusinski 2001
//  Warsaw University
//
#ifndef TIIABootContrH
#define TIIABootContrH
//---------------------------------------------------------------------------

#include "rpct/aboot2/IABootContr.h"


namespace rpct {


class TIIABootContr : public IABootContr {
public:                    
  typedef IDevice::TID TID;
private:                   
  TIIDevice& Owner;
  TID VECT_ALTERA;
  TID BITS_ALTERA_NCONF;
  TID BITS_ALTERA_NSTATUS;
  TID BITS_ALTERA_CONFDONE;
  TID BITS_ALTERA_INITDONE;
  TID WORD_RESET;
  TID AREA_BOOT;
public:
  TIIABootContr(TIIDevice& owner,
            TID vectAltera,
            TID bitsAlteraNConf,
            TID bitsAlteraNStatus,
            TID bitsAlteraConfDone,
            TID bitsAlteraInitDone,
            TID wordReset,
            TID areaBoot)
    : Owner(owner),
      VECT_ALTERA(vectAltera),
      BITS_ALTERA_NCONF(bitsAlteraNConf),
      BITS_ALTERA_NSTATUS(bitsAlteraNStatus),
      BITS_ALTERA_CONFDONE(bitsAlteraConfDone),
      BITS_ALTERA_INITDONE(bitsAlteraInitDone),
      WORD_RESET(wordReset),
      AREA_BOOT(areaBoot) {}   
      
  virtual ~TIIABootContr() {}

  virtual void Reset()
    {
      if (WORD_RESET != 0)
        Owner.writeWord(WORD_RESET, 0,  0x13ul);
    }

  virtual int GetStatusFlag(TID bit_id)
    {
      return Owner.readBits(bit_id);
    }

  virtual void SetStatusFlag(int level, TID bit_id)
    {
      uint32_t value;

      if(level!=0 && level!=1)
        throw TException("SetStatusFlag: illegal prameter");

      value = Owner.readVector(VECT_ALTERA);
      Owner.setBitsInVector(bit_id, level, value);
      Owner.writeVector(VECT_ALTERA, value);
    }

  virtual void TestStatusFlag(int level, int count, TID bit_id)
    {      
      if (level!=0 && level!=1)
        throw TException("TestStatusFlag: illegal prameter");
      if (count<1)
        throw TException("TestStatusFlag: illegal prameter");

      for (int k=0; k<count; k++)
        if (level == GetStatusFlag(bit_id))
          return;

      throw TException("incorrect level");
    }

  virtual void TSetStatusFlag(int level, int count, TID bit_id)
    {
      SetStatusFlag(level, bit_id);
      TestStatusFlag(level, count, bit_id);
    }

  /* Altera's boot utility */

  virtual int GetNConfig()
    {
      return GetStatusFlag(BITS_ALTERA_NCONF);
    }

  virtual void SetNConfig(int level)
    {
      SetStatusFlag(level, BITS_ALTERA_NCONF);
    }

  virtual void TestNConfig(int level, int count)
    {
      TestStatusFlag(level, count, BITS_ALTERA_NCONF);
    }

  virtual void TSetNConfig(int level, int count)
    {
      TSetStatusFlag(level, count, BITS_ALTERA_NCONF);
    }

  virtual int GetNStatus()
    {
      return GetStatusFlag(BITS_ALTERA_NSTATUS);
    }

  virtual void TestNStatus(int level, int count)
    {
      TestStatusFlag(level, count, BITS_ALTERA_NSTATUS);
    }

  virtual int GetConfDone()
    {
      return GetStatusFlag(BITS_ALTERA_CONFDONE);
    }


  virtual void TestConfDone(int level, int count)
    {
      TestStatusFlag(level, count, BITS_ALTERA_CONFDONE);
    }

  virtual void SendData(uint32_t data)
    {
      Owner.writeArea(AREA_BOOT, &data, 0, 1);
    }


  virtual void SendData(uint32_t *buf, int number)
    {
      Owner.writeArea(AREA_BOOT, buf, 0, number);
    }
};

}

#endif
