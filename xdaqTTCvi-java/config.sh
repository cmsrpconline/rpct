RPCT_ROOT=${BUILD_HOME}/rpct
HOMEPATH=${RPCT_ROOT}/xdaqTTCvi-java

CASTORLIB=${HOMEPATH}/castor-0.9.4.3/castor-0.9.4.3.jar:${HOMEPATH}/castor-0.9.4.3/castor-0.9.4.3-xml.jar

SRCPATH=${HOMEPATH}/src
LIBDIR=${HOMEPATH}/lib

CLASSPATH=${LIBDIR}/log4j.jar:${LIBDIR}/activation.jar:${LIBDIR}/dom.jar:${LIBDIR}/jaxm-api.jar:${LIBDIR}/junit.jar:${LIBDIR}/mail.jar:${LIBDIR}/saaj-api.jar:${LIBDIR}/saaj-impl.jar:${LIBDIR}/xalan.jar:${LIBDIR}/xdaqctl.jar:${LIBDIR}/xerces.jar:${LIBDIR}/xercesImpl.jar:${LIBDIR}/axis.jar:${LIBDIR}/jaxrpc.jar:${LIBDIR}/commons-logging-1.0.4.jar:${LIBDIR}/log4j-1.2.8.jar:${LIBDIR}/commons-discovery-0.2.jar:${LIBDIR}/wsdl4j-1.5.1.jar:${CASTORLIB}:${SRCPATH}
