package rpct.ttccontrol;

import java.math.BigInteger;
import java.rmi.RemoteException;

import javax.xml.namespace.QName;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.rpc.ServiceException;
import javax.xml.soap.SOAPException;
import javax.xml.transform.TransformerException;

import net.hep.cms.xdaqctl.XDAQDocument;
import net.hep.cms.xdaqctl.XDAQException;
import net.hep.cms.xdaqctl.XDAQMessage;
import net.hep.cms.xdaqctl.XDAQParameter;
import net.hep.cms.xdaqctl.XDAQTimeoutException;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class XdaqAccess {
    
    private static Log LOG = LogFactory.getLog(XdaqAccess.class);
    
    private static XdaqAccess instance;
    
    private XDAQDocument xdaqDocument;
    private String endpoint;
    private String xdaqTTCviClassName;
    private int xdaqTTCviInstance;
    private XDAQParameter xdaqParameter;

    private static final String XDAQ_NS_URI = "urn:xdaq-soap:3.0";
    private final Service service = new Service();
    private String soapActionURI;    
       
    private XdaqAccess() throws XdaqAccessException {    
        try {
            xdaqDocument = new XDAQDocument(ApplicationResources.getXdaqConfigurationFile());
            xdaqDocument.configureHost();
            Thread.sleep(1000);
            endpoint = (String) xdaqDocument.getContextList().firstElement();
            xdaqTTCviClassName = (String) xdaqDocument.getApplicationList().firstElement();
            xdaqTTCviInstance = ((Integer)xdaqDocument.getInstanceList(xdaqTTCviClassName).firstElement()).intValue();            
            (new XDAQMessage("config")).send(endpoint, xdaqTTCviClassName, xdaqTTCviInstance);            
            xdaqParameter = xdaqDocument.getParameter(xdaqTTCviClassName, xdaqTTCviInstance);
            soapActionURI = "urn:xdaq-application:class=" + xdaqTTCviClassName + ",instance=" + xdaqTTCviInstance;
            
        } catch (Exception e) {
            throw new XdaqAccessException(e);
        }
    }
    
    public static XdaqAccess getInstance() throws XdaqAccessException {
        if (instance == null) {
            instance = new XdaqAccess();
        }
        return instance;
    } 

        
    public synchronized String getParamValue(String name) throws XdaqAccessException {
        try {
            LOG.info("getParamValue " + name);
            return xdaqParameter.getValue(name);
        } catch (Exception e) {
            throw new XdaqAccessException(e);
        }        
    }
    
    public synchronized void setParamValue(String name, String value) throws XdaqAccessException {
        try {
            LOG.info("setParamValue " + name + ", " + value);
            xdaqParameter.setValue(name, value);
            if (!xdaqParameter.send()) {
                throw new XdaqAccessException("Could not set parameter '" + name + 
                        "' in xdaq application class=" + xdaqTTCviClassName 
                        + " instance=" + xdaqTTCviInstance);
            }
        } catch (XDAQTimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XDAQException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    }

    private Call prepareCall(String name) throws ServiceException {
        Call call = (Call) service.createCall();  
        call.setTargetEndpointAddress(endpoint);        
        call.setSOAPActionURI(soapActionURI);
        call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS,
                Boolean.FALSE); 
        call.setOperationName(new QName(XDAQ_NS_URI, name)); 
        return call;        
    }
    
    public long read(String name) throws XdaqAccessException {  
        try {
            Call call = prepareCall("read");
            call.addParameter(new QName(XDAQ_NS_URI, "name"), 
                    org.apache.axis.Constants.XSD_STRING,
                    javax.xml.rpc.ParameterMode.IN);
            call.setReturnType(org.apache.axis.Constants.XSD_INTEGER);
            return ((BigInteger) call.invoke(new Object[] { name })).longValue();
        } catch (ServiceException e) {
            throw new XdaqAccessException(e);
        } catch (RemoteException e) {
            throw new XdaqAccessException(e);
        }
    }
    
    public void write(String name, long value) throws XdaqAccessException {  
        try {
            Call call = prepareCall("write");
            call.addParameter(new QName(XDAQ_NS_URI, "name"), 
                    org.apache.axis.Constants.XSD_STRING,
                    javax.xml.rpc.ParameterMode.IN);
            call.addParameter(new QName(XDAQ_NS_URI, "value"), 
                    org.apache.axis.Constants.XSD_INTEGER,
                    javax.xml.rpc.ParameterMode.IN);
            call.setReturnType(org.apache.axis.Constants.XSD_INTEGER);
            call.invoke(new Object[] { name, BigInteger.valueOf(value) });
        } catch (ServiceException e) {
            throw new XdaqAccessException(e);
        } catch (RemoteException e) {
            throw new XdaqAccessException(e);
        }
    }
}
