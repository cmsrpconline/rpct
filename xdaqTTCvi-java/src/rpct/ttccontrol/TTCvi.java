package rpct.ttccontrol;

import java.util.BitSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * <p>Title: TTC Control</p>
 * <p>Description: Aplication that gives full control over TTCvi and connected to it TTCrx's</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Warsaw University</p>
 * @author Karol Bunkowski
 * @version 1.0
 */
/** @todo !!!! resolve problems with sign bits in short and int send to VME */
/** @todo check US changes */
//------------------------------------------------------------------------------
class TTCviException extends Exception {
    TTCviException() {
        super();
    }
    
    TTCviException(String msg) {
        super("TTCvi Exception: " + msg);
    }
}

//------------------------------------------------------------------------------
/*class TVMEInterface {
 /** @todo do something */
/*  TVMEInterface() {
 }
 }*/
//------------------------------------------------------------------------------
class Inhibit {
    private short address; //dalay address; duration addres = delay address + 2
    private String delayRegister;
    private String durationRegister;
    private short delay;
    private short duration;
    private TTCvi ttcvi;
    
    public Inhibit(TTCvi t, String delayRegister, String durationRegister,
            short addr) {
        ttcvi = t;
        this.delayRegister = delayRegister;
        this.durationRegister = durationRegister;
        address = addr;
    }
    
    void setInhibit(short del, short dur) throws XdaqAccessException {
        delay = del;
        duration = dur;
        //ttcvi.write16(address, delay);
        ttcvi.writeRegister(delayRegister, delay);
        
        //ttcvi.write16( (short) (address + 2), duration);
        ttcvi.writeRegister(durationRegister, duration);
    }
    
    void setDelay(short del) throws XdaqAccessException {
        delay = del;
        //ttcvi.write16( (short) (address), delay);
        ttcvi.writeRegister(delayRegister, delay);
    }
    
    void setDuration(short dur) throws XdaqAccessException {
        duration = dur;
        //ttcvi.write16( (short) (address + 2), duration);
        ttcvi.writeRegister(durationRegister, duration);
    }
    
    short getDelay() {
        return delay;
    }
    
    short getDuration() {
        return duration;
    }
}

//------------------------------------------------------------------------------
class BGo {
    private short dataAddres, settingsAddres, b_GoSignalAddres;
    private String dataAddressReg;
    private String settingsAddressReg;
    private String b_GoSignalAddressReg;
    private TTCvi ttcvi;
    private boolean[] settings = new boolean[5];
    // [0] enable, [1] sync, [2] single, [3] fifo, [4] calibration
    
    BGo(TTCvi t, String dataAddressReg, String settingsAddressReg,
            String b_GoSignalAddressReg, short dAddr, short sAddr, short b_GoSAddr) {
        ttcvi = t;
        this.dataAddressReg = dataAddressReg;
        this.settingsAddressReg = settingsAddressReg;
        this.b_GoSignalAddressReg = b_GoSignalAddressReg;
        dataAddres = dAddr;
        settingsAddres = sAddr;
        b_GoSignalAddres = b_GoSAddr;
    }
    
    void setMode(boolean enable, boolean sync, boolean single, boolean fifo,
            boolean calibration) throws XdaqAccessException {
        settings[0] = enable;
        settings[1] = sync;
        settings[2] = single;
        settings[3] = fifo;
        settings[4] = calibration;
        short word = (short) MyUtil.boolTabToInt(settings);
        //ttcvi.write16(settingsAddres, word);
        ttcvi.writeRegister(settingsAddressReg, word);
    }
    
    void setMode(boolean[] sets) throws XdaqAccessException {
        for (int i = 0; i < settings.length; i++) {
            settings[i] = sets[i];
        }
        short word = (short) MyUtil.boolTabToInt(settings);
        //ttcvi.write16(settingsAddres, word);
        ttcvi.writeRegister(settingsAddressReg, word);
    }
    
    void setModeEnable(boolean enable) throws XdaqAccessException {
        settings[0] = enable;
        short word = (short) MyUtil.boolTabToInt(settings);
        //ttcvi.write16(settingsAddres, word);
        ttcvi.writeRegister(settingsAddressReg, word);
    }
    
    void setModeSync(boolean sync) throws XdaqAccessException {
        settings[1] = sync;
        short word = (short) MyUtil.boolTabToInt(settings);
        //ttcvi.write16(settingsAddres, word);
        ttcvi.writeRegister(settingsAddressReg, word);
    }
    
    void setModeSingle(boolean single) throws XdaqAccessException {
        settings[2] = single;
        short word = (short) MyUtil.boolTabToInt(settings);
        //ttcvi.write16(settingsAddres, word);
        ttcvi.writeRegister(settingsAddressReg, word);
    }
    
    void setModeFifo(boolean fifo) throws XdaqAccessException {
        settings[3] = fifo;
        short word = (short) MyUtil.boolTabToInt(settings);
        //ttcvi.write16(settingsAddres, word);
        ttcvi.writeRegister(settingsAddressReg, word);
    }
    
    void setModeCalibration(boolean calibration) throws XdaqAccessException {
        settings[4] = calibration;
        short word = (short) MyUtil.boolTabToInt(settings);
        //ttcvi.write16(settingsAddres, word);
        ttcvi.writeRegister(settingsAddressReg, word);
    }
    
    boolean[] getMode() {
        return settings;
    }
    
    boolean[] readMode() throws XdaqAccessException {
        boolean[] b = new boolean[5];
        //b = MyUtil.shortToBoolTab(ttcvi.read16(settingsAddres), 5);
        b = MyUtil.shortToBoolTab((short)ttcvi.readRegister(settingsAddressReg), 5);
        return b;
    }
    
    void bGoSignal() throws XdaqAccessException {
        //ttcvi.write16(b_GoSignalAddres, (short) 1);
        ttcvi.writeRegister(b_GoSignalAddressReg, 1);
    }
    
    void WriteDataLong(short rxAddr, boolean e, short subaddr, short data)
    throws XdaqAccessException {
        BitSet word = new BitSet(32);
        BitSet brxAddr = MyUtil.intToBitSet(rxAddr, 14);
        BitSet bsubaddr = MyUtil.intToBitSet(subaddr, 8);
        BitSet bdata = MyUtil.intToBitSet(data, 8);
        
        word.set(31); //must be set if long cycle
        
        for (int i = 0; i < 14; i++) {
            MyUtil.setBit(word, i + 17, brxAddr.get(i));
            
        }
        MyUtil.setBit(word, 16, e);
        
        for (int i = 0; i < 8; i++) {
            MyUtil.setBit(word, i + 8, bsubaddr.get(i));
            
        }
        for (int i = 0; i < 8; i++) {
            MyUtil.setBit(word, i, bdata.get(i));
            
        }
        //ttcvi.write32(dataAddres, MyUtil.bitSetToInt(word));
        ttcvi.writeRegister(dataAddressReg, MyUtil.bitSetToInt(word));
    }
    
    void WriteDataShort(short command) throws XdaqAccessException {
        BitSet word = new BitSet(32);
        BitSet bcommand = MyUtil.intToBitSet(command, 8);
        word.clear(31); //must be unset if short cycle
        for (int i = 0; i < 8; i++) {
            MyUtil.setBit(word, i + 23, bcommand.get(i));
        }
        //ttcvi.write32(dataAddres, MyUtil.bitSetToInt(word));
        ttcvi.writeRegister(dataAddressReg, MyUtil.bitSetToInt(word));
    }
    
    void WriteData(boolean[] word) {
        /** @todo  write, if necessary*/
    }
}

//------------------------------------------------------------------------------
public class TTCvi {
    private static Log LOG = LogFactory.getLog(TTCvi.class);
    // Fields
    static public int MAX_INHIBIT_DELAY = 4095;
    static public int MAX_INHIBIT_DURATION = 255;
    
    static private String url; //= "http://212.87.7.87:40001";
    static private String targetAddr; //= "0";//"91";
    
    //private VMEInterface vme; no-xdaq version
    //private int boardAddress; no-xdaq version
    private boolean[] csr1 = new boolean[16];
    private boolean[] csr2 = new boolean[16];
    private Inhibit[] inhibit = new Inhibit[4];
    private BGo[] bGo = new BGo[4];
    
    short tRIGWORDad;
    short tRIGWORDsad;
    boolean tRIGWORDie;
    boolean tRIGWORDsize;
    
    // Constructor
    /* no-xdaq version
     public TTCvi(VMEInterface v, int bA ) {
     vme = v;
     boardAddress = bA;
     inhibit[0] = new Inhibit(this, "Inhibit0Delay", "Inhibit0Duration", (short) 0x92);
     inhibit[1] = new Inhibit(this, "Inhibit1Delay", "Inhibit1Duration", (short) 0x9A);
     inhibit[2] = new Inhibit(this, "Inhibit2Delay", "Inhibit2Duration", (short) 0xA2);
     inhibit[3] = new Inhibit(this, "Inhibit3Delay", "Inhibit3Duration", (short) 0xAA);
     
     bGo[0] = new BGo(this, "BData0Data", "BGo0Mode", "BGo0Send", (short) 0xB0, (short) 0x90, (short) 0x96);
     bGo[1] = new BGo(this, "BData1Data", "BGo1Mode", "BGo1Send", (short) 0xB4, (short) 0x98, (short) 0x9E);
     bGo[2] = new BGo(this, "BData2Data", "BGo2Mode", "BGo2Send", (short) 0xB8, (short) 0xA0, (short) 0xA6);
     bGo[3] = new BGo(this, "BData3Data", "BGo3Mode", "BGo3Send", (short) 0xBC, (short) 0xA8, (short) 0xAE);
     }*/
    
    public TTCvi() {
        inhibit[0] = new Inhibit(this, "Inhibit0Delay", "Inhibit0Duration", (short) 0x92);
        inhibit[1] = new Inhibit(this, "Inhibit1Delay", "Inhibit1Duration", (short) 0x9A);
        inhibit[2] = new Inhibit(this, "Inhibit2Delay", "Inhibit2Duration", (short) 0xA2);
        inhibit[3] = new Inhibit(this, "Inhibit3Delay", "Inhibit3Duration", (short) 0xAA);
        
        bGo[0] = new BGo(this, "BData0Data", "BGo0Mode", "BGo0Send", (short) 0xB0, (short) 0x90, (short) 0x96);
        bGo[1] = new BGo(this, "BData1Data", "BGo1Mode", "BGo1Send", (short) 0xB4, (short) 0x98, (short) 0x9E);
        bGo[2] = new BGo(this, "BData2Data", "BGo2Mode", "BGo2Send", (short) 0xB8, (short) 0xA0, (short) 0xA6);
        bGo[3] = new BGo(this, "BData3Data", "BGo3Mode", "BGo3Send", (short) 0xBC, (short) 0xA8, (short) 0xAE);
    }
    
    // Methods
    
    void setXdaqParameters(String _url, String _targetAddr) {
        url = _url;
        targetAddr = _targetAddr;
    }
    
    BGo getBGo(int bGoNumber) {
        return bGo[bGoNumber];
    }
    
    Inhibit getInhibit(int inhibitNumber) {
        return inhibit[inhibitNumber];
    }
    
    /*final void write16(short address, short word) {
        try {
            // vme.write16(boardAddress + address, word); no-xdaq version
            String strAddress = Integer.toHexString((int)address);
            if (strAddress.length() > 4)
                strAddress = strAddress.substring(4);
            
            String strWord = Integer.toHexString((int)word);
            if (strWord.length() > 4)
                strWord = strWord.substring(4);
            
            LOG.info("write16 address = " + strAddress + " data = " + strWord); 
            
        }
        catch (Throwable e) {
            e.printStackTrace();
            System.err.println(e.getMessage());
        }
    }*/
    
    void writeRegister(String name, int value) throws XdaqAccessException {
        //AccessParams.getInstance().setParam(url, targetAddr, name, "unsigned long",
        //    Long.toString(((long)value) & 0xffffffff ));
        LOG.info("writeRegister name = " + name + " value = " + Integer.toHexString(value)); 
        XdaqAccess.getInstance().write(name, ((long)value) & 0xffffffff);
    }
    
    void writeRegister(String name, short value) throws XdaqAccessException  {
        //AccessParams.getInstance().setParam(url, targetAddr, name, "unsigned long",
        //    Integer.toString(((int)value) & 0xffff ));
        LOG.info("writeRegister name = " + name + " value = " + Integer.toHexString(value)); 
        XdaqAccess.getInstance().write(name, ((long)value) & 0xffff);
    }
    
    int readRegister(String name) throws XdaqAccessException  {
        //return (int)Long.parseLong(AccessParams.getInstance().getParam(
        //    url, targetAddr, name, "unsigned long"));
        return (int)XdaqAccess.getInstance().read(name);  
    }
    
    /*final void write32(short address, int word) {
        try {
            //vme.write32(boardAddress + address, word); no-xdaq version
            LOG.info(Integer.toHexString(address) + " " +
                    Integer.toHexString(word)); //for test pourpose
        }
        catch (Throwable e) {
            e.printStackTrace();
            System.err.println(e.getMessage());
        }
    }*/
    
    /*final short read16(short address) {
        short value = 0;
        try {
            //value = vme.read16(boardAddress + address); no-xdaq version
        }
        
        catch (Throwable e) {
            e.printStackTrace();
            System.err.println(e.getMessage());
        }
        return value;
    }*/
    
    final int read32(short address) {
        int value = 0;
        try {
            //value = vme.read32(boardAddress + address); no-xdaq version
        }
        catch (Throwable e) {
            e.printStackTrace();
            System.err.println(e.getMessage());
        }
        return value;
    }
    
    //CSR1----------------------------------------
    private final void writeCSR1(short word) throws XdaqAccessException {
        //write16( (short) (0x80), word);
        writeRegister("CSR1", word);
    }
    
    void setCSR1(short word) throws XdaqAccessException {
        csr1 = MyUtil.shortToBoolTab(word);
        writeCSR1(word);
    }
    
    String getCSR1String() {
        return MyUtil.boolTabToString(csr1);
    }
    
    short readCSR1() throws XdaqAccessException {
        return (short)readRegister("CSR1"); //read16( (short) 0x80);
    }
    
    void setL1ATriggerSource(int s) throws XdaqAccessException { //s= 0 - 7
        BitSet buf = MyUtil.intToBitSet(s, 8);
        
        csr1[2] = buf.get(2);
        csr1[1] = buf.get(1);
        csr1[0] = buf.get(0);
        writeCSR1( (short) MyUtil.boolTabToInt(csr1));
    }
    
    short getL1ATriggerSource() {
        boolean[] buf = new boolean[3];
        
        buf[2] = csr1[2];
        buf[1] = csr1[1];
        buf[0] = csr1[0];
        return MyUtil.boolTabToShort(buf);
    }
    
    void setRanTrigRate(int s) throws XdaqAccessException { //s= 0 - 7
        BitSet buf = MyUtil.intToBitSet(s, 8);
        
        csr1[14] = buf.get(2);
        csr1[13] = buf.get(1);
        csr1[12] = buf.get(0);
        
        writeCSR1( (short) MyUtil.boolTabToInt(csr1));
    }
    
    void setOrbitSignalSel(boolean s) throws XdaqAccessException {
        csr1[3] = s;
        writeCSR1( (short) MyUtil.boolTabToInt(csr1));
    }
    
    boolean getOrbitSignalSel() {
        return csr1[3];
    }
    
    void eventOrbitCountSel(boolean s) throws XdaqAccessException {
        csr1[15] = s;
        writeCSR1( (short) MyUtil.boolTabToInt(csr1));
    }
    
    void resetL1AFIFO() throws XdaqAccessException {
        csr1[6] = true;
        writeCSR1( (short) MyUtil.boolTabToInt(csr1));
        csr1[6] = false; //sets this bit again on 0, so fifo isn't reseted, when CSR1 is writen by other functions
    }
    
    void sendL1ASignal() throws XdaqAccessException { //sets CSR1<0...2> to proper value (=4) and generates L1A signal
        setL1ATriggerSource(4);
        //write16( (short) 0x86, (short) 0x000f);
        writeRegister("L1ASend", 0);
    }
    
//  CSR2  -----------------------------------
    private final void writeCSR2(short word) throws XdaqAccessException {
        //write16( (short) (0x82), word);
        writeRegister("CSR2", word);
    }
    
    void setCSR2(short word) throws XdaqAccessException {
        csr2 = MyUtil.shortToBoolTab(word);
        writeCSR2(word);
    }
    
    String getCSR2String() {
        return MyUtil.boolTabToString(csr2);
    }
    
    short readCSR2() throws XdaqAccessException {
        return (short)readRegister("CSR2");//read16( (short) (0x82));
    }
    
    void resetBGoFIFO(boolean[] b) throws XdaqAccessException { //see page 9 on TTC doc, colling this function with b[i] set for 1 resets b-go fifo i
        for (int i = 0; i < 4; i++) {
            csr2[i + 12] = b[i];
        }
        writeCSR2( (short) MyUtil.boolTabToInt(csr2));
        for (int i = 0; i < 4; i++) {
            csr2[i + 12] = false;
        }
    }
    
    void resetBGoFIFO(int bGoNumb) throws XdaqAccessException {
        csr2[bGoNumb + 12] = true;
        writeCSR2( (short) MyUtil.boolTabToInt(csr2));
        csr2[bGoNumb + 12] = false;
    }
    
    void retransmiteBGoFIFO(boolean[] b) throws XdaqAccessException {
        for (int i = 0; i < 4; i++) {
            csr2[i + 8] = b[i];
        }
        writeCSR2( (short) MyUtil.boolTabToInt(csr2));
    }
    
    void retransmiteBGoFIFO(int bGoNumb, boolean retr) throws XdaqAccessException {
        csr2[bGoNumb + 8] = retr;
        writeCSR2( (short) MyUtil.boolTabToInt(csr2));
    }
    
    boolean getRetransmiteBGoFIFO(int bGoNumb) {
        return csr2[bGoNumb + 8];
    }
    
    String getFIFOStatus(int bGoNumb) throws XdaqAccessException {
        boolean[] csr = MyUtil.shortToBoolTab(readCSR2());
        String status;
        status = "LOADED";
        if (csr[2 * bGoNumb]) {
            status = "EMPTY";
        }
        if (csr[1 + 2 * bGoNumb]) {
            status = "FULL";
        }
        return status;
    }
    
//  other ----------------------------------
    void setTRIGWORD(short ad, short sad, boolean ie, boolean size) throws
    XdaqAccessException { //only for TTCvi MKII
        //seting size to 0 inhibit after-trigger transfer
        //setting ie to 0 is nonsens, it may change TTCrx setings
        tRIGWORDad = ad;
        tRIGWORDsad = sad;
        tRIGWORDie = ie;
        tRIGWORDsize = size;
        //write16( (short) 0xC8, ad);
        writeRegister("TrigWordAddress", ad);
        
        BitSet word = new BitSet(10);
        BitSet bsad = MyUtil.intToBitSet(sad, 10);
        
        word.clear(0);
        word.clear(1); //bits set by TTCvi to mark cycle number
        
        for (int i = 0; i < 6; i++) {
            MyUtil.setBit(word, i + 2, bsad.get(i));
            
        }
        MyUtil.setBit(word, 8, ie);
        MyUtil.setBit(word, 9, size);
        
        //write16( (short) 0xCA, (short) MyUtil.bitSetToInt(word));
        writeRegister("TrigWordLSW", MyUtil.bitSetToInt(word));
    }
    
    short getTRIGWORDad() {
        return tRIGWORDad;
    }
    
    short getTRIGWORDsad() {
        return tRIGWORDsad;
    }
    
    boolean getTRIGWORDie() {
        return tRIGWORDie;
    }
    
    boolean getTRIGWORDsize() {
        return tRIGWORDsize;
    }
    
    void reset() throws XdaqAccessException { //resets TTCvi
        //write16( (short) 0x84, (short) 0);
        writeRegister("SoftReset", 0);
    }
    
    long readL1ACounter() throws XdaqAccessException { //Event/Orbit Counter
        short msb = (short) readRegister("CounterMSB"); //read16((short)0x88);
        short lsb = (short) readRegister("CounterLSB"); //read16((short)0x8a);
        int counter = (msb << 16) + lsb;
        return counter; /** @todo resolve problems with sign bit */
    }
    
    void writeL1ACounter(short msb, short lsb) throws XdaqAccessException {
        //write16( (short) 0x88, (short) msb);
        writeRegister("CounterMSB", msb);
        
        //write16( (short) 0x8a, (short) lsb);
        writeRegister("CounterLSB", lsb);
        /** @todo resolve problems with sign bit */
    }
    
    // TTCrx - asynchronous cycle
    void writeRX(short rxAddr, short subaddr, short data) throws XdaqAccessException { //long asynchronous cycle with commad for rx
        //write16( (short) 0xc0, (short) (0x8000 + ( (rxAddr & 0x3fff) << 1)));
        writeRegister("BChannelLongMSB", 0x8000 + ( (rxAddr & 0x3fff) << 1));
        
        //write16( (short) 0xc2, (short) ( ( (subaddr & 0xff) << 8) + (data & 0xff)));
        writeRegister("BChannelLongLSB", ( (subaddr & 0xff) << 8) + (data & 0xff));
    }
    
    void writeExternal(short rxAddr, short subaddr, short data) throws
    XdaqAccessException { //long asynchronous cycle with commad for pararel exterenal bus on rx
        //write16( (short) 0xc0, (short) (0x8001 + ( (rxAddr & 0x3fff) << 1)));
        writeRegister("BChannelLongMSB", 0x8001 + ( (rxAddr & 0x3fff) << 1));
        
        //write16( (short) 0xc2, (short) ( ( (subaddr & 0xff) << 8) + (data & 0xff)));
        writeRegister("BChannelLongLSB", ( (subaddr & 0xff) << 8) + (data & 0xff));
    }
    
    void writeShortAsynchronous(short command) throws XdaqAccessException {
        //write16( (short) 0xc4, command); // command - 8 bits
        writeRegister("BChannelShortBroadcast", command);
    }
    
}