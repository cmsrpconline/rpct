package rpct.ttccontrol;

/**
 * <p>Title: TTC Control</p>
 * <p>Description: Aplication that gives full control over TTCvi and connected to it TTCrx's</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Warsaw University</p>
 * @author Karol Bunkowski
 * @version 1.0
 */
class TTCrxException extends Exception {
  TTCrxException() {
    super();
  }

  TTCrxException(String msg) {
    super("TTCrx Exception: " + msg);
  }
}

public class TTCrx {
  // Fields
  static public int MAX_DESKEW = 239;
  static public int MAX_COAR_DELAY = 15;
  private String name;
  private short address;
  private TTCvi ttcvi;
  private boolean[] controlRegister = new boolean[8];
  private short deskew1, deskew2, coarDelay1, coarDelay2;

  // Constructor
  public TTCrx(String n, short addr, TTCvi vi) {
    name = new String(n);
    address = addr;
    ttcvi = vi;           //after reset state
    controlRegister[0] = true;
    controlRegister[1] = true;
    controlRegister[2] = false;
    controlRegister[3] = false;
    controlRegister[4] = true;
    controlRegister[5] = false;
    controlRegister[6] = false;
    controlRegister[7] = true;
  }

  // Methods
  public String toString() {
    String str = name + " @ 0x" + Integer.toHexString(address) ;
    return str;
  }

  short getAddress() {
    return address;
  }

  String getName() {
    return name;
  }

  short convertDescew(short k) throws TTCrxException {
  //converts descew in ps for proper numbers
  //see TTCrx manual Appendix A
    if(k > MAX_DESKEW)
      throw new TTCrxException("Descew value to big.");
    short n, m;
    n = (short)(k % 15);
    m = (short)((k/15 - n + 14) % 16);
    return (short)(16*n + m);
  }


  void Reset() throws Exception {
    ttcvi.writeRX(address, (short)0x6, (short)1);
  }

  boolean[] getControlRegister() {
    return controlRegister;
  }

  void setControlRegister(boolean[] cReg) throws Exception {
    controlRegister = (boolean[])cReg.clone();
    ttcvi.writeRX(address, (short)0x3, MyUtil.boolTabToShort(controlRegister));
  }

  void setDeskew1(short d) throws TTCrxException, Exception {
    deskew1 = d;
    ttcvi.writeRX(address, (short)0x0, convertDescew(deskew1));
  }

  short getDeskew1() {
    return deskew1;
  }

  void setDeskew2(short d) throws TTCrxException, Exception {
    deskew2 = d;
    ttcvi.writeRX(address, (short)0x1, convertDescew(deskew2));
  }

  short getDeskew2() {
    return deskew2;
  }

  void setCoarDelay1(short d) throws TTCrxException, Exception {
    if(d > MAX_COAR_DELAY)
      throw new TTCrxException("Coar Delay value to big.");
    coarDelay1 = d;
    ttcvi.writeRX(address, (short)0x2, (short)(coarDelay1 + (coarDelay2 << 4)) );
  }

  short getCoarDelay1() {
    return coarDelay1;
  }

  void setCoarDelay2(short d) throws TTCrxException, Exception {
    if(d > MAX_COAR_DELAY)
      throw new TTCrxException("Coar Delay value to big.");
    coarDelay2 = d;
    ttcvi.writeRX(address, (short)0x2, (short)(coarDelay1 + (coarDelay2 << 4)) );
  }

  short getCoarDelay2() {
    return coarDelay2;
  }

  void getSettings() {
  /** @todo think and write */
  }
}