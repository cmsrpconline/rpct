package rpct.ttccontrol;
import java.util.BitSet;
/**
 * <p>Title: TTC Control</p>
 * <p>Description: Aplication that gives full control over TTCvi and connected to it TTCrx's</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Warsaw University</p>
 * @author Karol Bunkowski
 * @version 1.0
 */

public class MyUtil  {

  public static final int bitSetToInt(BitSet bits) throws IndexOutOfBoundsException {
    if(bits.length() > 32 )
      throw new IndexOutOfBoundsException("BitSet is to long for representing in int");
    int word =0 ;
    int pow2 = 1;
    for(int i = 0; i < bits.length(); i++) {
      if (bits.get(i))
        word = word + pow2;
      pow2 *= 2;
    }
    return word;
  }

  public static final BitSet intToBitSet(int word) { /*returns bitSet containg bit
   representation of word */
    int mask =1;
    BitSet bitSet = new BitSet(32);
    for (int i = 0; i < 32; i++) {
      if ( (word & mask) == mask)
        bitSet.set(i);
      mask <<= 1;
    }
    return bitSet;
  }

  public static final BitSet intToBitSet(int word, int length) {/*returns bitSet of lenght
  "length" containg bit representation of word*/
    int mask =1;
    BitSet bitSet = new BitSet(length);
    for (int i = 0; i < length; i++) {
      if ( (word & mask) == mask)
        bitSet.set(i);
      mask = mask << 1;
      //System.out.println("  in intToBitSet " + mask);
    }
    return bitSet;
  }

  public static final void setBit(BitSet bitSet, int pos, boolean value) {
    if (value)
      bitSet.set(pos);
    else
      bitSet.clear(pos);
  }


  public static final String bitSetToString(BitSet bitSet) {
    StringBuffer strBuf = new StringBuffer();
    for (int i = 0; i < bitSet.length(); i++)
      if(bitSet.get(i) )
        strBuf.append('1');
      else
        strBuf.append('0');

    strBuf.reverse();
    return strBuf.toString();
  }

  public static int charBoolToInt(char c) throws TTCviException {
  /** @todo think if it is necesery */
    if (c == '\u0030')
      return 0;
    else if (c == '\u0031')
      return 1;
    else throw new TTCviException();
  }

  public static final int boolTabToInt(boolean[] bits) throws IndexOutOfBoundsException {
    if(bits.length > 32 )
      throw new IndexOutOfBoundsException("BitSet is to long for representing in int");
    int word =0 ;
    int pow2 = 1;
    for(int i = 0; i < bits.length; i++) {
      if (bits[i])
        word = word + pow2;
      pow2 *= 2;
    }
    return word;
  }

  public static final short boolTabToShort(boolean[] bits) throws IndexOutOfBoundsException {
    if(bits.length > 16 )
      throw new IndexOutOfBoundsException("BitSet is to long for representing in int");
    short word =0 ;
    short pow2 = 1;
    for(int i = 0; i < bits.length; i++) {
      if (bits[i])
        word = (short)(word + pow2);
      pow2 *= 2;
    }
    return word;
  }

  public static final boolean[] intToBoolTab(int word) { /*returns bitSet containg bit
  representation of word */
    int mask =1;
    boolean[] boolTab = new boolean[32];
    for (int i = 0; i < 32; i++) {
      if ( (word & mask) == mask)
        boolTab[i] = true;
      mask <<= 1;
    }
    return boolTab;
  }

  public static final boolean[] shortToBoolTab(short word) { /*returns bitSet containg bit
  representation of word */
    int mask =1;
    boolean[] boolTab = new boolean[16];
    for (int i = 0; i < 16; i++) {
      if ( (word & mask) == mask)
        boolTab[i] = true;
      mask <<= 1;
    }
    return boolTab;
  }

  public static final boolean[] intToBoolTab(int word, int lenght) { /*returns bitSet containg bit
  representation of word */
    int mask =1;
    boolean[] boolTab = new boolean[lenght];
    for (int i = 0; i < lenght; i++) {
      if ( (word & mask) == mask)
        boolTab[i] = true;
      mask <<= 1;
    }
    return boolTab;
  }

  public static final boolean[] shortToBoolTab(short word, int lenght) { /*returns bitSet containg bit
  representation of word */
    int mask =1;
    boolean[] boolTab = new boolean[lenght];
    for (int i = 0; i < lenght; i++) {
      if ( (word & mask) == mask)
        boolTab[i] = true;
      mask <<= 1;
    }
    return boolTab;
  }

  public static final String  boolTabToString(boolean[] boolTab) {
    StringBuffer strBuf = new StringBuffer();
    for (int i = boolTab.length -1; i >= 0; i--) {
      if(boolTab[i])
        strBuf.append('1');
      else
        strBuf.append('0');
    }
    return strBuf.toString();
  }

  public static final boolean[] stringToBoolTab(String str) throws Exception {
    int strLenght = str.length();
    boolean[] boolTabBuf = new boolean[strLenght];
    for (int i = 0; i < strLenght; i++) {
      if (str.charAt(i) == '1')
        boolTabBuf[strLenght - i -1] = true;
      else if (str.charAt(i) == '0')
        boolTabBuf[strLenght - i -1] = false;
      else throw new Exception("character can't be convert to bit, its not '1' or '0'");
    }
    return boolTabBuf;
  }
}
