package rpct.ttccontrol;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 * <p>Title: TTC Control</p>
 * <p>Description: Aplication that gives full control over TTCvi and connected to it TTCrx's</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Warsaw University</p>
 * @author Karol Bunkowski
 * @version 1.0
 */

public class PanelScrolEdit extends JPanel{
  private int maxValue;
  private int value;
  private boolean changeBySetValue;
  //GUI objects
  BorderLayout borderLayout1 = new BorderLayout();
  JTextField jTextField1 = new JTextField();
  JScrollBar jScrollBar1 = new JScrollBar();

  public PanelScrolEdit() {
    try {
      maxValue = 100;
      jbInit();
    }
    catch(Exception ex) {
      ex.printStackTrace();
    }
  }
  void jbInit() throws Exception {
    jTextField1.setText("0");
    jTextField1.setHorizontalAlignment(SwingConstants.RIGHT);
    jTextField1.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jTextField1_actionPerformed(e);
      }
    });
    this.setLayout(borderLayout1);
    jScrollBar1.setMaximum(maxValue + 5);
    jScrollBar1.setOrientation(JScrollBar.HORIZONTAL);
    jScrollBar1.setVisibleAmount(5);
    jScrollBar1.addAdjustmentListener(new java.awt.event.AdjustmentListener() {
      public void adjustmentValueChanged(AdjustmentEvent e) {
        jScrollBar1_adjustmentValueChanged(e);
      }
    });
    this.setBorder(BorderFactory.createEtchedBorder());
    this.add(jTextField1, BorderLayout.NORTH);
    this.add(jScrollBar1, BorderLayout.SOUTH);
  }

  private transient Vector adjustmentListeners;

  void jScrollBar1_adjustmentValueChanged(AdjustmentEvent e) {
    value = jScrollBar1.getValue();
    jTextField1.setText(String.valueOf(value));
    if (!changeBySetValue)
      fireAdjustmentValueChanged(e);
  }

  void jTextField1_actionPerformed(ActionEvent e) {
    try {
      value = Integer.valueOf(jTextField1.getText()).intValue();
      if (value > maxValue) {
        value = maxValue;
        jTextField1.setText(String.valueOf(value));
      }
      jScrollBar1.setValue(value);
    }
    catch (java.lang.NumberFormatException ex) {
      JOptionPane.showMessageDialog(null, jTextField1.getText() +
      " is not proper integer value","NumberFormatException",
      JOptionPane.ERROR_MESSAGE);
      jTextField1.setText(String.valueOf(value));
    }
  }

  void setMax(int mV) {
    maxValue = mV;
    jScrollBar1.setMaximum(maxValue + 5); //5 = VisibleAmount
  }

  int getValue() {
    return value;
  }
  //ActionEvent
  public void actionPerformed(ActionEvent e) {
  }

  public void setValue(int value) {
    this.value = value;
    changeBySetValue = true;
    jTextField1.setText(String.valueOf(value));
    jScrollBar1.setValue(value);
    changeBySetValue = false;
  }
  public synchronized void removeAdjustmentListener(AdjustmentListener l) {
    if (adjustmentListeners != null && adjustmentListeners.contains(l)) {
      Vector v = (Vector) adjustmentListeners.clone();
      v.removeElement(l);
      adjustmentListeners = v;
    }
  }
  public synchronized void addAdjustmentListener(AdjustmentListener l) {
    Vector v = adjustmentListeners == null ? new Vector(2) : (Vector) adjustmentListeners.clone();
    if (!v.contains(l)) {
      v.addElement(l);
      adjustmentListeners = v;
    }
  }
  protected void fireAdjustmentValueChanged(AdjustmentEvent e) {
    if (adjustmentListeners != null) {
      Vector listeners = adjustmentListeners;
      int count = listeners.size();
      for (int i = 0; i < count; i++) {
        ((AdjustmentListener) listeners.elementAt(i)).adjustmentValueChanged(e);
      }
    }
  }

}
