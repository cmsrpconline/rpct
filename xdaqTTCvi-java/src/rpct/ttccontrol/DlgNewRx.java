package rpct.ttccontrol;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * <p>Title: TTC Control</p>
 * <p>Description: Aplication that gives full control over TTCvi and connected to it TTCrx's</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Warsaw University</p>
 * @author Karol Bunkowski
 * @version 1.0
 */

public class DlgNewRx extends JDialog {
  /** Return value form class method if CANCEL is chosen. */
  public static final int  CANCEL_OPTION = 2;
  /** Return value form class method if OK is chosen. */
  public static final int  OK_OPTION = 0;
  private int result;
  private short address;
  private String name;
  //GUI Objects
  JPanel panel1 = new JPanel();
  JPanel jPanel1 = new JPanel();
  JLabel jLabel1 = new JLabel();
  JLabel jLabel2 = new JLabel();
  JTextField jTxtFldName = new JTextField();
  JTextField jTxtFldAddress = new JTextField();
  JButton jBtOK = new JButton();
  JButton jBtCancel = new JButton();
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private GridBagLayout gridBagLayout2 = new GridBagLayout();

  public DlgNewRx(Frame frame, String title, boolean modal) {
    super(frame, title, modal);
    try {
      jbInit();
      pack();
    }
    catch(Exception ex) {
      ex.printStackTrace();
    }
  }

  public DlgNewRx() {
    this(null, "", false);
  }
  void jbInit() throws Exception {
    panel1.setLayout(gridBagLayout1);
    jPanel1.setBorder(BorderFactory.createEtchedBorder());
    jPanel1.setPreferredSize(new Dimension(165, 80));
    jPanel1.setLayout(gridBagLayout2);
    jLabel1.setText("Name");
    jLabel2.setText("Address");
    this.setTitle("New TTCrx");
    this.addWindowListener(new DlgNewRx_this_windowAdapter(this));
    jBtOK.setPreferredSize(new Dimension(80, 27));
    jBtOK.setText("OK");
    jBtOK.addActionListener(new DlgNewRx_jBtOK_actionAdapter(this));
    jBtOK.addActionListener(new DlgNewRx_jBtOK_actionAdapter(this));
    jBtCancel.setPreferredSize(new Dimension(80, 27));
    jBtCancel.setText("Cancel");
    jBtCancel.addActionListener(new DlgNewRx_jBtCancel_actionAdapter(this));
    jTxtFldAddress.setMinimumSize(new Dimension(40, 27));
    jTxtFldAddress.setPreferredSize(new Dimension(40, 27));
    jTxtFldAddress.setText("3CCC");
    jTxtFldName.setMinimumSize(new Dimension(80, 27));
    jTxtFldName.setPreferredSize(new Dimension(80, 27));
    panel1.setMinimumSize(new Dimension(250, 150));
    panel1.setPreferredSize(new Dimension(200, 151));
    getContentPane().add(panel1, null);
    jPanel1.add(jLabel1,        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 5, 0, 24), 0, 0));
    jPanel1.add(jTxtFldName,              new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 0, 0, 0), -6, 0));
    jPanel1.add(jLabel2,   new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(12, 5, 19, 0), 0, 0));
    jPanel1.add(jTxtFldAddress,      new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 0, 19, 0), 0, 0));
    panel1.add(jPanel1,        new GridBagConstraints(0, 0, 2, 1, 1.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(9, 9, 9, 9), 0, 0));
    panel1.add(jBtCancel,                    new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(1, 0, 9, 15), 0, 0));
    panel1.add(jBtOK,                                    new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(1, 15, 9, 0), 0, 0));
  }

  String getRxName() {
    return name;
  }

  short getAddress() {
    return address;
  }

  int getResult() {
    return result;
  }

  void this_windowActivated(WindowEvent e) {
    result = CANCEL_OPTION;
  }

  void jBtOK_actionPerformed(ActionEvent e) {
    try {
      name = jTxtFldName.getText();
      address = Short.parseShort(jTxtFldAddress.getText(), 16);
      if (address < 0 || address > 0x3FFF )
        throw new Exception();
      result = OK_OPTION;
      this.hide();
    }
    catch (Exception ex) {
      JOptionPane.showMessageDialog(null,
      "Rx address must be number in hex format from range 0 - 3FFF",
      "NumberFormatException",
      JOptionPane.ERROR_MESSAGE);
    }
  }

  void jBtCancel_actionPerformed(ActionEvent e) {
    result = CANCEL_OPTION;
    this.hide();
  }

}

class DlgNewRx_this_windowAdapter extends java.awt.event.WindowAdapter {
  DlgNewRx adaptee;

  DlgNewRx_this_windowAdapter(DlgNewRx adaptee) {
    this.adaptee = adaptee;
  }
  public void windowActivated(WindowEvent e) {
    adaptee.this_windowActivated(e);
  }
}

class DlgNewRx_jBtOK_actionAdapter implements java.awt.event.ActionListener {
  DlgNewRx adaptee;

  DlgNewRx_jBtOK_actionAdapter(DlgNewRx adaptee) {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e) {
    adaptee.jBtOK_actionPerformed(e);
  }
}

class DlgNewRx_jBtCancel_actionAdapter implements java.awt.event.ActionListener {
  DlgNewRx adaptee;

  DlgNewRx_jBtCancel_actionAdapter(DlgNewRx adaptee) {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e) {
    adaptee.jBtCancel_actionPerformed(e);
  }
}