package rpct.ttccontrol;

import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
import java.awt.event.*;

/**
 * <p>Title: TTC Control</p>
 * <p>Description: Aplication that gives full control over TTCvi and connected to it TTCrx's</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Warsaw University</p>
 * @author Karol Bunkowski
 * @version 1.0
 */

public class DlgStartOptions extends JDialog {
  JPanel jPanel1 = new JPanel();
  JPanel jPanel2 = new JPanel();
  GridBagLayout gridBagLayout1 = new GridBagLayout();
  TitledBorder titledBorder1;
  JButton jBtOK = new JButton();
  GridBagLayout gridBagLayout2 = new GridBagLayout();
  JTextField jTxtFldUrl = new JTextField();
  JLabel jLabel1 = new JLabel();
  JLabel jLabel2 = new JLabel();
  JTextField jTxtFldTargetAddr = new JTextField();

  public DlgStartOptions(Frame frame, String title, boolean modal) {
    super(frame, title, modal);
    try {
      jbInit();
      pack();
    }
    catch(Exception ex) {
      ex.printStackTrace();
    }
  }

  public DlgStartOptions() {
    this(null, "", false);
  }

  void jbInit() throws Exception {
    titledBorder1 = new TitledBorder("");
    this.setTitle("TTCControl - Start Options");
    jPanel2.setLayout(gridBagLayout2);
    jPanel1.setLayout(gridBagLayout1);
    jPanel2.setBorder(BorderFactory.createEtchedBorder());
    jBtOK.setMinimumSize(new Dimension(80, 25));
    jBtOK.setPreferredSize(new Dimension(80, 25));
    jBtOK.setText("OK");
    jBtOK.addActionListener(new DlgStartOptions_jBtOK_actionAdapter(this));
    jTxtFldUrl.setPreferredSize(new Dimension(250, 19));
    jTxtFldUrl.setRequestFocusEnabled(true);
    jTxtFldUrl.setText("http://212.87.7.87:40001");
    jLabel1.setText("xdaq server url");
    jPanel1.setPreferredSize(new Dimension(400, 200));
    jLabel2.setText("target address");
    jTxtFldTargetAddr.setPreferredSize(new Dimension(50, 19));
    jTxtFldTargetAddr.setText("91");
    this.getContentPane().add(jPanel1, BorderLayout.CENTER);
    jPanel1.add(jPanel2,              new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 0, 10), 400, 300));
    jPanel2.add(jTxtFldUrl,       new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    jPanel2.add(jLabel1,        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 4), 0, 0));
    jPanel2.add(jLabel2,  new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(10, 0, 0, 0), 0, 0));
    jPanel2.add(jTxtFldTargetAddr,     new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 0, 0, 0), 0, 0));
    jPanel1.add(jBtOK,      new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(10, 0, 10, 0), 0, 0));

  }

  void jBtOK_actionPerformed(ActionEvent e) {
    this.hide();
  }


}

class DlgStartOptions_jBtOK_actionAdapter implements java.awt.event.ActionListener {
  DlgStartOptions adaptee;

  DlgStartOptions_jBtOK_actionAdapter(DlgStartOptions adaptee) {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e) {
    adaptee.jBtOK_actionPerformed(e);
  }
}