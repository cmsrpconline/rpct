package rpct.ttccontrol;

public class XdaqAccessException extends Exception {

    private static final long serialVersionUID = -7879128392078247204L;

    public XdaqAccessException() {
        super();
    }

    public XdaqAccessException(String message) {
        super(message);
    }

    public XdaqAccessException(String message, Throwable cause) {
        super(message, cause);
    }

    public XdaqAccessException(Throwable cause) {
        super(cause);
    }

}
