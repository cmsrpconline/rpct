package rpct.ttccontrol;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

/**
 * <p>Title: TTC Control</p>
 * <p>Description: Aplication that gives full control over TTCvi and connected to it TTCrx's</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Warsaw University</p>
 * @author Karol Bunkowski
 * @version 1.0
 */

public class DlgRxControlRegister extends JDialog {
  /** Return value form class method if CANCEL is chosen. */
  public static final int  CANCEL_OPTION = 2;
  /** Return value form class method if OK is chosen. */
  public static final int  OK_OPTION = 0;
  private int result;
  private boolean[] settings = new boolean[8];

  //GUI Objects
  JPanel panel1 = new JPanel();
  JPanel jPanel1 = new JPanel();
  JCheckBox jCheckBox1 = new JCheckBox();
  JCheckBox jCheckBox2 = new JCheckBox();
  JCheckBox jCheckBox3 = new JCheckBox();
  JCheckBox jCheckBox4 = new JCheckBox();
  JCheckBox jCheckBox5 = new JCheckBox();
  JCheckBox jCheckBox6 = new JCheckBox();
  JCheckBox jCheckBox7 = new JCheckBox();
  JCheckBox jCheckBox8 = new JCheckBox();
  GridLayout gridLayout1 = new GridLayout();
  JPanel jPanel2 = new JPanel();
  JButton jBtCancel = new JButton();
  JButton jBtOK = new JButton();
  FlowLayout flowLayout1 = new FlowLayout();
  Border border1;
  BorderLayout borderLayout1 = new BorderLayout();
  TitledBorder titledBorder1;
  TitledBorder titledBorder2;

  public DlgRxControlRegister(Frame frame, String title, boolean modal) {
    super(frame, title, modal);
    try {
      jbInit();
      pack();
    }
    catch(Exception ex) {
      ex.printStackTrace();
    }
  }

  public DlgRxControlRegister() {
    this(null, "", false);
  }
  void jbInit() throws Exception {
    border1 = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,new Color(148, 145, 140)),BorderFactory.createEmptyBorder(0,5,0,0));
    titledBorder1 = new TitledBorder("");
    titledBorder2 = new TitledBorder("");
    panel1.setLayout(borderLayout1);
    jPanel1.setBorder(border1);
    jPanel1.setPreferredSize(new Dimension(300, 200));
    jPanel1.setLayout(gridLayout1);
    jCheckBox1.setAlignmentX((float) 1.0);
    jCheckBox1.setSelected(true);
    jCheckBox1.setText("Enable Bunch Counter operation");
    jCheckBox2.setAlignmentX((float) 1.0);
    jCheckBox2.setSelected(true);
    jCheckBox2.setText("Enable Event Counter operation");
    jCheckBox3.setText("Select Clock40Des2");
    jCheckBox4.setText("Enable Clock40Des2 output");
    jCheckBox5.setSelected(true);
    jCheckBox5.setText("Enable L1AClock output");
    jCheckBox6.setText("Enable Parallel Output Bus");
    jCheckBox7.setText("Enable Serial B output");
    jCheckBox8.setSelected(true);
    jCheckBox8.setText("Enable (non-descewed) Clock 40 output");
    this.setTitle("Rx Control Regiser");
    this.addWindowListener(new DlgRxControlRegister_this_windowAdapter(this));
    gridLayout1.setColumns(1);
    gridLayout1.setHgap(5);
    gridLayout1.setRows(0);
    jBtCancel.setPreferredSize(new Dimension(80, 27));
    jBtCancel.setText("Cancel");
    jBtCancel.addActionListener(new DlgRxControlRegister_jBtCancel_actionAdapter(this));
    jBtOK.setPreferredSize(new Dimension(80, 27));
    jBtOK.setText("OK");
    jBtOK.addActionListener(new DlgRxControlRegister_jBtOK_actionAdapter(this));
    jPanel2.setLayout(flowLayout1);
    jPanel2.setPreferredSize(new Dimension(300, 40));
    panel1.setBorder(titledBorder2);
    getContentPane().add(panel1);
    panel1.add(jPanel1, BorderLayout.CENTER);
    jPanel1.add(jCheckBox1, null);
    jPanel1.add(jCheckBox2, null);
    jPanel1.add(jCheckBox3, null);
    jPanel1.add(jCheckBox4, null);
    jPanel1.add(jCheckBox5, null);
    jPanel1.add(jCheckBox6, null);
    jPanel1.add(jCheckBox7, null);
    jPanel1.add(jCheckBox8, null);
    panel1.add(jPanel2, BorderLayout.SOUTH);
    jPanel2.add(jBtCancel, null);
    jPanel2.add(jBtOK, null);
  }

  int getResult() {
    return result;
  }

  boolean[] getSettings() {
    settings[0] = jCheckBox1.isSelected();
    settings[1] = jCheckBox2.isSelected();
    settings[2] = jCheckBox3.isSelected();
    settings[3] = jCheckBox4.isSelected();
    settings[4] = jCheckBox5.isSelected();
    settings[5] = jCheckBox6.isSelected();
    settings[6] = jCheckBox7.isSelected();
    settings[7] = jCheckBox8.isSelected();
    return settings;
  }

  void setSettings(boolean[] controlRegister) {
    jCheckBox1.setSelected(controlRegister[0]);
    jCheckBox2.setSelected(controlRegister[1]);
    jCheckBox3.setSelected(controlRegister[2]);
    jCheckBox4.setSelected(controlRegister[3]);
    jCheckBox5.setSelected(controlRegister[4]);
    jCheckBox6.setSelected(controlRegister[5]);
    jCheckBox7.setSelected(controlRegister[6]);
    jCheckBox8.setSelected(controlRegister[7]);
  }

  void jBtOK_actionPerformed(ActionEvent e) {
    result = OK_OPTION;
    this.hide();
  }

  void jBtCancel_actionPerformed(ActionEvent e) {
    result = CANCEL_OPTION;
    this.hide();
  }

  void this_windowActivated(WindowEvent e) {
    result = CANCEL_OPTION;
  }
}

class DlgRxControlRegister_jBtOK_actionAdapter implements java.awt.event.ActionListener {
  DlgRxControlRegister adaptee;

  DlgRxControlRegister_jBtOK_actionAdapter(DlgRxControlRegister adaptee) {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e) {
    adaptee.jBtOK_actionPerformed(e);
  }
}

class DlgRxControlRegister_jBtCancel_actionAdapter implements java.awt.event.ActionListener {
  DlgRxControlRegister adaptee;

  DlgRxControlRegister_jBtCancel_actionAdapter(DlgRxControlRegister adaptee) {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e) {
    adaptee.jBtCancel_actionPerformed(e);
  }
}

class DlgRxControlRegister_this_windowAdapter extends java.awt.event.WindowAdapter {
  DlgRxControlRegister adaptee;

  DlgRxControlRegister_this_windowAdapter(DlgRxControlRegister adaptee) {
    this.adaptee = adaptee;
  }
  public void windowActivated(WindowEvent e) {
    adaptee.this_windowActivated(e);
  }
}