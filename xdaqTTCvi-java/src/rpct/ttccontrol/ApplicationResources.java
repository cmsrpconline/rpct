package rpct.ttccontrol;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ApplicationResources {    
    
	private final static String xdaqConfigurationFile; 
    
    private final static String PROPERTIES_FILE_NAME = "xdaqTTCvi.properties";
    
    
    static {
        Properties properties = new Properties();
        try {            
            InputStream is = ApplicationResources.class.getClassLoader().getResourceAsStream(PROPERTIES_FILE_NAME);
            if (is != null) {
                try {
                    properties.load(is);
                }
                finally {
                    is.close();
                }
                xdaqConfigurationFile = properties.getProperty("xdaqTTCvi.xdaqConfigurationFile");
            }
            else {
                throw new ExceptionInInitializerError("Could not load " + PROPERTIES_FILE_NAME);
            }
        } catch (IOException e) {
            throw new ExceptionInInitializerError(e);
        }        
    }
	
	public final static String getXdaqConfigurationFile() {
		return xdaqConfigurationFile;
	}
}
