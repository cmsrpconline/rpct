package rpct.ttccontrol;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;

/**
 * <p>Title: TTC Control</p>
 * <p>Description: Aplication that gives full control over TTCvi and connected to it TTCrx's</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Warsaw University</p>
 * @author Karol Bunkowski
 * @version 1.0
 */

public class DlgBGoDataForLoad extends JDialog {
  private JPanel panel1 = new JPanel();
  private BorderLayout borderLayout1 = new BorderLayout();
  private JPanel jPanel1 = new JPanel();
  private JPanel jPanel2 = new JPanel();
  private Border border1;
  private JButton jBtOK = new JButton();
  private JList jLstBGoData = new JList();
  private JLabel jLabel12 = new JLabel();
  private BorderLayout borderLayout2 = new BorderLayout();
  private JLabel jLabel1 = new JLabel();
  private Border border2;
  private int[] bGoDataNum = new int[4];
  private int okClickedCount;

  public DlgBGoDataForLoad(Frame frame, String title, boolean modal) {
    super(frame, title, modal);
    try {
      jbInit();
      pack();
    }
    catch(Exception ex) {
      ex.printStackTrace();
    }
  }

  public DlgBGoDataForLoad() {
    this(null, "", false);
  }
  private void jbInit() throws Exception {
    border1 = BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED,Color.white,Color.white,new Color(103, 101, 98),new Color(148, 145, 140)),BorderFactory.createEmptyBorder(5,5,5,5));
    border2 = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,new Color(148, 145, 140)),BorderFactory.createEmptyBorder(5,5,5,5));
    panel1.setLayout(borderLayout1);
    panel1.setBorder(border1);
    panel1.setPreferredSize(new Dimension(200, 300));
    jPanel1.setBorder(border2);
    jPanel1.setPreferredSize(new Dimension(10, 100));
    jPanel1.setLayout(borderLayout2);
    jPanel2.setPreferredSize(new Dimension(10, 35));
    jBtOK.setPreferredSize(new Dimension(80, 27));
    jBtOK.setText("OK");
    jBtOK.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jBtOK_actionPerformed(e);
      }
    });
    jLstBGoData.setBorder(BorderFactory.createLoweredBevelBorder());
    jLstBGoData.setPreferredSize(new Dimension(105, 150));
    jLabel12.setPreferredSize(new Dimension(86, 20));
    jLabel12.setText("Select BGo Data to be load");
    jLabel1.setText("in BGo number 0");
    getContentPane().add(panel1);
    panel1.add(jPanel1,  BorderLayout.CENTER);
    jPanel1.add(jLstBGoData, BorderLayout.SOUTH);
    jPanel1.add(jLabel1, BorderLayout.CENTER);
    jPanel1.add(jLabel12, BorderLayout.NORTH);
    panel1.add(jPanel2,  BorderLayout.SOUTH);
    jPanel2.add(jBtOK, null);
  }
  void jLstBGoData_mousePressed(MouseEvent e) {

  }
  void jLstBGoData_mouseReleased(MouseEvent e) {

  }
  void jLstBGoData_mouseEntered(MouseEvent e) {

  }
  void jLstBGoData_mouseExited(MouseEvent e) {

  }

  public void setData(Vector bGoDataNames ,int[] bGoDaNm) {
    jLstBGoData.setListData(bGoDataNames);
    bGoDataNum = bGoDaNm;
    jLstBGoData.setSelectedIndex(0);
    okClickedCount = 0;
    jLabel1.setText("in BGo number " + Integer.toString(okClickedCount));
  }

  void jBtOK_actionPerformed(ActionEvent e) {
    bGoDataNum[okClickedCount] = jLstBGoData.getSelectedIndex() - 1;
    okClickedCount++;
    jLabel1.setText("in BGo number " + Integer.toString(okClickedCount));
    if(okClickedCount == 4)
      this.hide();
  }
}