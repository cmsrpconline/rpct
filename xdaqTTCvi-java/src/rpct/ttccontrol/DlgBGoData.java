package rpct.ttccontrol;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import rpct.ttccontrol.*;
/**
 * <p>Title: TTC Control</p>
 * <p>Description: Aplication that gives full control over TTCvi and connected to it TTCrx's</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Warsaw University</p>
 * @author Karol Bunkowski
 * @version 1.0
 */

public class DlgBGoData extends JDialog {
  /** Return value form class method if CANCEL is chosen. */
  public static final int  CANCEL_OPTION = 2;
  /** Return value form class method if OK is chosen. */
  public static final int  OK_OPTION = 0;
  private int result;
  BGoData bGoData;
  TTCManager ttcManager;
  /*Vector bGoCycles = new Vector(); object created here is unusfull,
  it is here only because designer got mad if bGoCycles, which is inserted into
  jLstycles, is null. bGoCycles keeps vector chosen in jLstBGoData on mainFrame*/

  //GUI objects
  JPanel panel1 = new JPanel();
  JPanel jPanel1 = new JPanel();
  JPanel jPanel5 = new JPanel();
  JTextField jTxtFldName = new JTextField();
  JLabel jLabel1 = new JLabel();
  FlowLayout flowLayout6 = new FlowLayout();
  JPanel jPanel6 = new JPanel();
  JButton jBtDelete = new JButton();
  JList jLstCycles = new JList();
  JPanel jPanel7 = new JPanel();
  JButton jBtAddLongCyle = new JButton();
  JButton jBtAddShortCycle = new JButton();
  JPanel jPanel8 = new JPanel();
  JLabel jLabel18 = new JLabel();
  JTextField jTxtFldCommand = new JTextField();
  JLabel jLabel17 = new JLabel();
  JLabel jLabel16 = new JLabel();
  JLabel jLabel15 = new JLabel();
  JLabel jLabel14 = new JLabel();
  JLabel jLabel13 = new JLabel();
  JPanel jPanel12 = new JPanel();
  JPanel jPanel11 = new JPanel();
  JCheckBox jChBxExsternal = new JCheckBox();
  JComboBox jCbBxRxAddress;
  JTextField jTxtFldSubAddress = new JTextField();
  JTextField jTxtFldData = new JTextField();
  BorderLayout borderLayout1 = new BorderLayout();
  BorderLayout borderLayout2 = new BorderLayout();
  JPanel jPanel9 = new JPanel();
  JButton jBtCancel = new JButton();
  JButton jBtOK = new JButton();
  FlowLayout flowLayout9 = new FlowLayout();
  Border border1;
  BorderLayout borderLayout3 = new BorderLayout();
  TitledBorder titledBorder1;
  Border border2;
  private JLabel jLabel3 = new JLabel();
  private JLabel jLabel2 = new JLabel();
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private GridBagLayout gridBagLayout2 = new GridBagLayout();
  private GridBagLayout gridBagLayout3 = new GridBagLayout();
  private GridBagLayout gridBagLayout4 = new GridBagLayout();

  public DlgBGoData(Frame frame, String title, boolean modal, TTCManager _ttcManager) {
    super(frame, title, modal);
    try {
      ttcManager = _ttcManager;
      jbInit();
      pack();
    }
    catch(Exception ex) {
      ex.printStackTrace();
    }
  }

  public DlgBGoData() {
    this(null, "", false, null);
  }
  void jbInit() throws Exception {
    jCbBxRxAddress = new JComboBox(ttcManager.getTTCrxsVec());

    border1 = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,new Color(148, 145, 140)),BorderFactory.createEmptyBorder(0,0,0,5));
    titledBorder1 = new TitledBorder("");
    border2 = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,new Color(148, 145, 140)),BorderFactory.createEmptyBorder(5,5,5,5));
    panel1.setLayout(borderLayout3);
    this.setModal(true);
    this.addWindowListener(new DlgBGoData_this_windowAdapter(this));
    jPanel1.setBorder(border2);
    jPanel1.setPreferredSize(new Dimension(430, 280));
    jPanel1.setLayout(borderLayout2);
    jTxtFldName.setMinimumSize(new Dimension(60, 21));
    jTxtFldName.setPreferredSize(new Dimension(70, 21));
    jTxtFldName.setText("data");
    jLabel1.setText("Name");
    jPanel5.setLayout(flowLayout6);
    flowLayout6.setAlignment(FlowLayout.LEFT);
    jBtDelete.setPreferredSize(new Dimension(80, 30));
    jBtDelete.setMargin(new Insets(2, 4, 2, 4));
    jBtDelete.setText("Delete");
    jBtDelete.addActionListener(new DlgBGoData_jBtDelete_actionAdapter(this));
    jLstCycles.setBorder(BorderFactory.createLoweredBevelBorder());
    jLstCycles.setMinimumSize(new Dimension(140, 130));
    jLstCycles.setPreferredSize(new Dimension(140, 150));
    jPanel6.setLayout(gridBagLayout4);
    jPanel6.setMinimumSize(new Dimension(144, 220));
    jPanel6.setPreferredSize(new Dimension(145, 220));
    jBtAddLongCyle.setMargin(new Insets(2, 4, 2, 4));
    jBtAddLongCyle.setText("<<Add");
    jBtAddLongCyle.addActionListener(new DlgBGoData_jBtAddLongCyle_actionAdapter(this));
    jBtAddShortCycle.setText("<<Add");
    jBtAddShortCycle.addActionListener(new DlgBGoData_jBtAddShortCycle_actionAdapter(this));
    jBtAddShortCycle.setMargin(new Insets(2, 4, 2, 4));
    jLabel18.setToolTipText("");
    jLabel18.setText("TTCrx");
    jTxtFldCommand.setPreferredSize(new Dimension(20, 21));
    jTxtFldCommand.setToolTipText("in hex format");
    jTxtFldCommand.setText("00");
    jLabel17.setPreferredSize(new Dimension(70, 17));
    jLabel17.setText("Data");
    jLabel16.setPreferredSize(new Dimension(70, 17));
    jLabel16.setText("Subaddress");
    jLabel15.setPreferredSize(new Dimension(79, 17));
    jLabel15.setText("Command");
    jLabel14.setFont(new java.awt.Font("Dialog", 1, 10));
    jLabel14.setText("Short-format asynchronous cycle");
    jLabel13.setFont(new java.awt.Font("Dialog", 1, 10));
    jLabel13.setPreferredSize(new Dimension(195, 19));
    jLabel13.setText("Long-format asynchronous cycle");
    jPanel12.setLayout(gridBagLayout2);
    jPanel12.setBorder(BorderFactory.createEtchedBorder());
    jPanel12.setPreferredSize(new Dimension(220, 235));
    jPanel11.setBorder(BorderFactory.createEtchedBorder());
    jPanel11.setPreferredSize(new Dimension(220, 160));
    jPanel11.setLayout(gridBagLayout1);
    jChBxExsternal.setText("External Subaddress");
    jTxtFldSubAddress.setMinimumSize(new Dimension(23, 21));
    jTxtFldSubAddress.setPreferredSize(new Dimension(23, 21));
    jTxtFldSubAddress.setToolTipText("in hex format");
    jTxtFldSubAddress.setText("CC");
    jTxtFldData.setMinimumSize(new Dimension(23, 21));
    jTxtFldData.setPreferredSize(new Dimension(23, 21));
    jTxtFldData.setToolTipText("in hex format");
    jTxtFldData.setText("00");
    jPanel8.setLayout(borderLayout1);
    jPanel7.setMinimumSize(new Dimension(30, 169));
    jPanel7.setPreferredSize(new Dimension(30, 290));
    jPanel7.setLayout(gridBagLayout3);
    jPanel8.setPreferredSize(new Dimension(220, 380));
    borderLayout1.setHgap(5);
    borderLayout1.setVgap(5);
    jBtCancel.setPreferredSize(new Dimension(80, 30));
    jBtCancel.setText("Cancel");
    jBtCancel.addActionListener(new DlgBGoData_jBtCancel_actionAdapter(this));
    jBtOK.setPreferredSize(new Dimension(80, 30));
    jBtOK.setText("OK");
    jBtOK.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jBtOK_actionPerformed(e);
      }
    });
    jBtOK.addActionListener(new DlgBGoData_jBtOK_actionAdapter(this));
    jPanel9.setLayout(flowLayout9);
    borderLayout2.setVgap(2);
    panel1.setBorder(titledBorder1);
    panel1.setPreferredSize(new Dimension(460, 330));
    jLabel3.setFont(new java.awt.Font("Dialog", 1, 10));
    jLabel3.setPreferredSize(new Dimension(100, 17));
    jLabel3.setText("Command");
    jLabel2.setFont(new java.awt.Font("Dialog", 1, 10));
    jLabel2.setText("RxAdd, SubAdd, E, Data");
    jCbBxRxAddress.setMaximumSize(new Dimension(150, 32));
    jCbBxRxAddress.setMinimumSize(new Dimension(130, 32));
    jCbBxRxAddress.setPreferredSize(new Dimension(110, 32));
    getContentPane().add(panel1);
    panel1.add(jPanel1, BorderLayout.CENTER);
    jPanel5.add(jLabel1, null);
    jPanel5.add(jTxtFldName, null);
    jPanel1.add(jPanel6,  BorderLayout.WEST);
    jPanel6.add(jLabel3,     new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 5, 0), 0, 0));
    jPanel6.add(jLabel2,   new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    jPanel6.add(jLstCycles,        new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
    jPanel6.add(jBtDelete,       new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 0, 5, 0), 0, 0));
    jPanel1.add(jPanel7,  BorderLayout.CENTER);
    jPanel7.add(jBtAddLongCyle,  new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(70, 9, 0, 10), 0, 0));
    jPanel7.add(jBtAddShortCycle,  new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(70, 9, 30, 10), 0, 0));
    jPanel1.add(jPanel8,  BorderLayout.EAST);
    jPanel12.add(jLabel14,            new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 0), 0, 0));
    jPanel12.add(jLabel15,   new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 0), 0, 0));
    jPanel12.add(jTxtFldCommand,         new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 0), 0, 0));
    jPanel1.add(jPanel5, BorderLayout.NORTH);
    jPanel8.add(jPanel11,  BorderLayout.NORTH);
    jPanel11.add(jLabel13,                new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
    jPanel11.add(jLabel16,               new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 16, 0));
    jPanel11.add(jTxtFldSubAddress,           new GridBagConstraints(1, 2, 2, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 5, 0), 0, 0));
    jPanel11.add(jCbBxRxAddress,                     new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 5, 0), 0, 0));
    jPanel11.add(jLabel18,         new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
    jPanel11.add(jTxtFldData,      new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 5, 0), 0, 0));
    jPanel11.add(jLabel17,    new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
    jPanel11.add(jChBxExsternal,    new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
    jPanel8.add(jPanel12, BorderLayout.CENTER);
    panel1.add(jPanel9, BorderLayout.SOUTH);
    jPanel9.add(jBtOK, null);
    jPanel9.add(jBtCancel, null);
  }

  void jBtOK_actionPerformed(ActionEvent e) {
    result = OK_OPTION;
    bGoData.setName(jTxtFldName.getText());
    this.hide();
  }

  void jBtCancel_actionPerformed(ActionEvent e) {
    result = CANCEL_OPTION;
    this.hide();
  }

  void this_windowActivated(WindowEvent e) {
    result = CANCEL_OPTION;
    jTxtFldName.setText(bGoData.getName());
    jLstCycles.setListData((Vector)bGoData.getBGoCyclesVec());
    //jCbBxRxAddress.setL
  }

  int getResult() {
    return result;
  }

  void jBtAddLongCyle_actionPerformed(ActionEvent e) {
    LongTTCviCycle cycle = new LongTTCviCycle(
      ((TTCrx)jCbBxRxAddress.getSelectedItem()).getAddress(),
      Short.parseShort(jTxtFldSubAddress.getText(), 16),
      jChBxExsternal.isSelected(),
      Short.parseShort(jTxtFldData.getText(), 16));
    bGoData.addCycle(cycle);
    jLstCycles.setListData(bGoData.getBGoCyclesVec());
  }

  void jBtAddShortCycle_actionPerformed(ActionEvent e) {
    ShortTTCviCycle cycle =
      new ShortTTCviCycle( Short.parseShort(jTxtFldCommand.getText(), 16));
    bGoData.addCycle(cycle);
    jLstCycles.setListData(bGoData.getBGoCyclesVec());
  }

  void jBtDelete_actionPerformed(ActionEvent e) {
    bGoData.removeCycle(jLstCycles.getSelectedIndex());
    jLstCycles.setListData(bGoData.getBGoCyclesVec());
  }
}

class DlgBGoData_jBtOK_actionAdapter implements java.awt.event.ActionListener {
  DlgBGoData adaptee;

  DlgBGoData_jBtOK_actionAdapter(DlgBGoData adaptee) {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e) {
    adaptee.jBtOK_actionPerformed(e);
  }
}

class DlgBGoData_jBtCancel_actionAdapter implements java.awt.event.ActionListener {
  DlgBGoData adaptee;

  DlgBGoData_jBtCancel_actionAdapter(DlgBGoData adaptee) {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e) {
    adaptee.jBtCancel_actionPerformed(e);
  }
}

class DlgBGoData_this_windowAdapter extends java.awt.event.WindowAdapter {
  DlgBGoData adaptee;

  DlgBGoData_this_windowAdapter(DlgBGoData adaptee) {
    this.adaptee = adaptee;
  }
  public void windowActivated(WindowEvent e) {
    adaptee.this_windowActivated(e);
  }
}

class DlgBGoData_jBtAddLongCyle_actionAdapter implements java.awt.event.ActionListener {
  DlgBGoData adaptee;

  DlgBGoData_jBtAddLongCyle_actionAdapter(DlgBGoData adaptee) {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e) {
    adaptee.jBtAddLongCyle_actionPerformed(e);
  }
}

class DlgBGoData_jBtAddShortCycle_actionAdapter implements java.awt.event.ActionListener {
  DlgBGoData adaptee;

  DlgBGoData_jBtAddShortCycle_actionAdapter(DlgBGoData adaptee) {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e) {
    adaptee.jBtAddShortCycle_actionPerformed(e);
  }
}

class DlgBGoData_jBtDelete_actionAdapter implements java.awt.event.ActionListener {
  DlgBGoData adaptee;

  DlgBGoData_jBtDelete_actionAdapter(DlgBGoData adaptee) {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e) {
    adaptee.jBtDelete_actionPerformed(e);
  }
}