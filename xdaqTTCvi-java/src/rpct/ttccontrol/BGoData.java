package rpct.ttccontrol;
import java.util.Vector;
/**
 * <p>Title: TTC Control</p>
 * <p>Description: Aplication that gives full control over TTCvi and connected to it TTCrx's</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Warsaw University</p>
 * @author Karol Bunkowski
 * @version 1.0
 */

class TTCviCycle {
}

class LongTTCviCycle extends TTCviCycle {
  private short rxAddress;
  private short subAddress;
  private boolean e;
  private short data;

  LongTTCviCycle(short rxAddr, short subAddr, boolean ext,
                 short dt) {
  /*  if(rxAddr > 0x4000) //2^14
      throw new Exception("rxAddress is to big");*/ /** @todo add exception hendling */
    rxAddress = rxAddr;
    subAddress = subAddr;
    e = ext;
    data = dt;
  }

  public String toString() {
    String str;
    str = "0x" + Integer.toHexString(rxAddress).toUpperCase() +
          " ,0x" + Integer.toHexString(subAddress).toUpperCase() + ", " +
          String.valueOf(e) + ", 0x" + Integer.toHexString(data).toUpperCase();
    return str;
  }

  short getRxAddress() {
    return rxAddress;
  }

  short getSubAddress() {
    return subAddress;
  }

  boolean getE() {
    return e;
  }
  short getData() {
    return data;
  }

}

class ShortTTCviCycle extends TTCviCycle {
  private short command;

  ShortTTCviCycle(short cmmnd) {
    command = cmmnd;
  }

  public String toString() {
    String str;
    str = "0x" + Integer.toHexString(command).toUpperCase();
    return str;
  }

  short getCommand() {
    return command;
  }
}

public class BGoData {
  private String name;
  private Vector bGoCyclesVec = new Vector(); //vector of TTCviCycle

  BGoData(String nam) {
    name = nam;
  }

  BGoData duplicate() {
    BGoData bGoData= new BGoData(this.name.toString());
    bGoData.bGoCyclesVec = (Vector)this.bGoCyclesVec.clone();
    return bGoData;
  }

  void addCycle(TTCviCycle cycle) {
    bGoCyclesVec.add(cycle);
  }

  String getName() {
    return name;
  }

  void setName(String nam) {
    name = nam;
  }

  Vector getBGoCyclesVec() {
    return bGoCyclesVec;
  }

  void removeCycle(int indx) {
    bGoCyclesVec.remove(indx);
  }

  public String toString() {
    return name;
  }
}