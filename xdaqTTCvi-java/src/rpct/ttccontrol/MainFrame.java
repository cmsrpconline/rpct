package rpct.ttccontrol;

import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;

import java.awt.*;
import java.awt.event.*;

/**
 * <p>Title: TTC Control</p>
 * <p>Description: Aplication that gives full control over TTCvi and connected to it TTCrx's</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Warsaw University</p>
 * @author Karol Bunkowski
 * @version 1.0
 */

public class MainFrame
    extends JFrame {
  TTCManager ttcManager;
  String[] trgRateStrings = {
      "1 Hz", "100 Hz", "1 kHz", "5 kHz", "10 kHz",
      "25 kHz", "50 kHz", "100 kHz"};
  //graphic UI objects
  JPanel contentPane;
  ButtonGroup buttonGroupTrgSrs = new ButtonGroup();

  ButtonGroup buttonGroupBGo = new ButtonGroup();
  Border border1;
  DlgBGoData dlgBGoData;
  DlgNewRx dlgNewRx = new DlgNewRx(this, "New TTCrx", true);
  DlgBGoDataForLoad dlgBGoDataForLoad = new DlgBGoDataForLoad(this,
      "BGo Data load when prest applayed", true);
  DlgRxControlRegister dlgRxControlRegister = new DlgRxControlRegister(this,
      "Rx Control Register", true);
  DlgStartOptions dlgStartOptions = new DlgStartOptions(this, "TTCCOntrol - Start Options", true);
  private Border border2;
  private Border border3;
  private Border border4;
  private Border border5;
  private Border border6;
  private Border border7;
  private JScrollPane jScrollPane1 = new JScrollPane();
  private JPanel jPanel21 = new JPanel();
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private JTextField jTxtFldCSR2 = new JTextField();
  private JTextField jTxtFldCSR1 = new JTextField();
  private JButton jBtL1AFIFOReset = new JButton();
  private BorderLayout borderLayout14 = new BorderLayout();
  private BorderLayout borderLayout13 = new BorderLayout();
  private FlowLayout flowLayout3 = new FlowLayout();
  private JRadioButton jRBtTrgSrc6 = new JRadioButton();
  private JRadioButton jRBtTrgSrc5 = new JRadioButton();
  private JRadioButton jRBtTrgSrc4 = new JRadioButton();
  private JButton jBtReadCSR2 = new JButton();
  private JRadioButton jRBtTrgSrc3 = new JRadioButton();
  private JButton jBtReadCSR1 = new JButton();
  private JRadioButton jRBtTrgSrc2 = new JRadioButton();
  private JRadioButton jRBtTrgSrc1 = new JRadioButton();
  private JRadioButton jRBtTrgSrc0 = new JRadioButton();
  private JCheckBox jChBOrbitSignal = new JCheckBox();
  private JPanel jPanel23 = new JPanel();
  private JPanel jPanel22 = new JPanel();
  private JPanel jPanel20 = new JPanel();
  private JComboBox jCbBxTrgRate = new JComboBox(trgRateStrings);
  private JLabel jLabel9 = new JLabel();
  private JPanel jPanel5 = new JPanel();
  private JPanel jPanel4 = new JPanel();
  private JLabel jLabel4 = new JLabel();
  private JPanel jPanel3 = new JPanel();
  private JLabel jLabel3 = new JLabel();
  private JPanel jPanel2 = new JPanel();
  private JLabel jLabel2 = new JLabel();
  private JPanel jPanel1 = new JPanel();
  private JButton jBtResetBGoFIFOs = new JButton();
  private JLabel jLabel1 = new JLabel();
  private JButton jBtTrigger = new JButton();
  private BorderLayout borderLayout2 = new BorderLayout();
  private BorderLayout borderLayout1 = new BorderLayout();
  private JPanel jPanel19 = new JPanel();
  private JPanel jPanel18 = new JPanel();
  private GridLayout gridLayout6 = new GridLayout();
  private GridLayout gridLayout5 = new GridLayout();
  private GridLayout gridLayout3 = new GridLayout();
  private JTextField jTxtFldCommand = new JTextField();
  private JLabel jLabel15 = new JLabel();
  private JButton jBtResetFIFO = new JButton();
  private JLabel jLabel14 = new JLabel();
  private JLabel jLabel13 = new JLabel();
  private JButton jBtSendShortCycle = new JButton();
  private JLabel jLabel12 = new JLabel();
  private JLabel jLabel11 = new JLabel();
  private JPanel jPanel30 = new JPanel();
  private FlowLayout flowLayout9 = new FlowLayout();
  private JCheckBox jChBAsynchron = new JCheckBox();
  private JButton jBtSendLongCycle = new JButton();
  private JCheckBox jChBRepetetive = new JCheckBox();
  private JList jLstBGoData = new JList();
  private JCheckBox jChBCalibration = new JCheckBox();
  private JButton jButton5 = new JButton();
  private JButton jButton4 = new JButton();
  private JPanel jPanel29 = new JPanel();
  private JCheckBox jChBEnable = new JCheckBox();
  private JPanel jPanel24 = new JPanel();
  private JPanel jPanel9 = new JPanel();
  private JPanel jPanel8 = new JPanel();
  private JPanel jPanel7 = new JPanel();
  private JPanel jPanel6 = new JPanel();
  private JButton jBtAddBGoData = new JButton();
  private JCheckBox jChBRetrensmit = new JCheckBox();
  private JCheckBox jChBFIFO = new JCheckBox();
  private JLabel jLabel28 = new JLabel();
  private BorderLayout borderLayout3 = new BorderLayout();
  private JButton jBtDelete = new JButton();
  private JTextField jTxtFldBGoSettings = new JTextField();
  private JButton jBtBGoSignal = new JButton();
  private JButton jBtResetTTCvi = new JButton();
  private JCheckBox jChBxExternal = new JCheckBox();
  private JPanel jPanel17 = new JPanel();
  private JButton jBtReadBGoSettings = new JButton();
  private JPanel jPanel12 = new JPanel();
  private JPanel jPanel11 = new JPanel();
  private JPanel jPanel10 = new JPanel();
  private JToggleButton jTBtBGo3 = new JToggleButton();
  private JButton jBtFIFOStatus = new JButton();
  private JToggleButton jTBtBGo2 = new JToggleButton();
  private JToggleButton jTBtBGo1 = new JToggleButton();
  private JButton jBtLoadFIFO = new JButton();
  private JToggleButton jTBtBGo0 = new JToggleButton();
  private GridLayout gridLayout1 = new GridLayout();
  private JButton jBtDeleteRx = new JButton();
  private JComboBox jCbBxPresets;// = new JComboBox(TTCManager.getManager().getPresets());
  private JLabel jLabel19 = new JLabel();
  private FlowLayout flowLayout19 = new FlowLayout();
  private JPanel jPanel38 = new JPanel();
  private JPanel jPanel37 = new JPanel();
  private JPanel jPanel36 = new JPanel();
  private JTextField jTxtFldSub = new JTextField();
  private JComboBox jCbBxRxTR; //= new JComboBox(TTCManager.getManager().getTTCrxsVec());
  private JComboBox jCbBxRxs; //= new JComboBox(TTCManager.getManager().getTTCrxsVec());
  private JButton jBtApplayPreset = new JButton();
  private JLabel jLabel111 = new JLabel();
  private JLabel jLabel110 = new JLabel();
  private JCheckBox jChBxExtSub = new JCheckBox();
  private JLabel jLabel27 = new JLabel();
  private FlowLayout flowLayout26 = new FlowLayout();
  private JLabel jLabel21 = new JLabel();
  private JButton jBtAddPreset = new JButton();
  private FlowLayout flowLayout25 = new FlowLayout();
  private JPanel jPanel16 = new JPanel();
  private FlowLayout flowLayout24 = new FlowLayout();
  private JLabel jLabel20 = new JLabel();
  private JPanel jPanel15 = new JPanel();
  private JButton jBtDeletePreset = new JButton();
  private FlowLayout flowLayout23 = new FlowLayout();
  private JPanel jPanel14 = new JPanel();
  private JPanel jPanel13 = new JPanel();
  private JTextField jTxtFldNewPresetName = new JTextField();
  private JButton jBtSetTrigReg = new JButton();
  private JCheckBox jChBxEnableATT = new JCheckBox();
  private JButton jBtAddRx = new JButton();
  private Border border8;
  private GridBagLayout gridBagLayout2 = new GridBagLayout();
  private JLabel jLabel7 = new JLabel();
  private JLabel jLabel6 = new JLabel();
  private PanelScrolEdit pSEdtInhDel2 = new PanelScrolEdit();
  private PanelScrolEdit pSEdtInhDur2 = new PanelScrolEdit();
  private JLabel jLabel10 = new JLabel();
  private PanelScrolEdit pSEdtInhDel3 = new PanelScrolEdit();
  private PanelScrolEdit pSEdtInhDur3 = new PanelScrolEdit();
  private PanelScrolEdit pSEdtInhDel0 = new PanelScrolEdit();
  private JLabel jLabel5 = new JLabel();
  private PanelScrolEdit pSEdtInhDur0 = new PanelScrolEdit();
  private PanelScrolEdit pSEdtInhDel1 = new PanelScrolEdit();
  private JLabel jLabel8 = new JLabel();
  private PanelScrolEdit pSEdtInhDur1 = new PanelScrolEdit();
  private GridBagLayout gridBagLayout3 = new GridBagLayout();
  private JLabel jLabel17 = new JLabel();
  private JTextField jTxtFldData = new JTextField();
  private JLabel jLabel16 = new JLabel();
  private JTextField jTxtFldSubAddress = new JTextField();
  private JLabel jLabel18 = new JLabel();
  private JComboBox jCbBxRx;// = new JComboBox(TTCManager.getManager().getTTCrxsVec());
  private GridBagLayout gridBagLayout4 = new GridBagLayout();
  private GridBagLayout gridBagLayout5 = new GridBagLayout();
  private GridBagLayout gridBagLayout6 = new GridBagLayout();
  private GridBagLayout gridBagLayout7 = new GridBagLayout();
  private GridBagLayout gridBagLayout8 = new GridBagLayout();
  private GridBagLayout gridBagLayout9 = new GridBagLayout();
  private GridBagLayout gridBagLayout10 = new GridBagLayout();
  private JLabel jLabel25 = new JLabel();
  private JLabel jLabel24 = new JLabel();
  private JLabel jLabel23 = new JLabel();
  private JLabel jLabel22 = new JLabel();
  private JLabel jLabel26 = new JLabel();
  private PanelScrolEdit pSEdtDskw1 = new PanelScrolEdit();
  private PanelScrolEdit pSEdtCoarDel2 = new PanelScrolEdit();
  private PanelScrolEdit pSEdtCoarDel1 = new PanelScrolEdit();
  private JButton jBtCntrlReg = new JButton();
  private PanelScrolEdit pSEdtDskw2 = new PanelScrolEdit();
  private GridBagLayout gridBagLayout11 = new GridBagLayout();
  private GridBagLayout gridBagLayout12 = new GridBagLayout();
  GridBagLayout gridBagLayout13 = new GridBagLayout();
  //Construct the frame
  public MainFrame() {
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try {
      ttcManager = new TTCManager();
      //dlgStartOptions.jTxtFldUrl.setText(ttcManager.getIniUrl());
      //dlgStartOptions.jTxtFldTargetAddr.setText(ttcManager.getIniTargetAddr());

      //dlgStartOptions.show();

      //ttcManager.saveIniXdaqParameters(dlgStartOptions.jTxtFldUrl.getText(),
      //                            dlgStartOptions.jTxtFldTargetAddr.getText());

      ttcManager.init(dlgStartOptions.jTxtFldUrl.getText(),
                                  dlgStartOptions.jTxtFldTargetAddr.getText());
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  //Component initialization
  private void jbInit() throws Exception {
    //setIconImage(Toolkit.getDefaultToolkit().createImage(MainFrame.class.getResource("[Your Icon]")));

    dlgBGoData = new DlgBGoData(this, "B-Go Data", true, ttcManager);
    jCbBxPresets = new JComboBox(ttcManager.getPresets());
    jCbBxRxTR = new JComboBox(ttcManager.getTTCrxsVec());
    jCbBxRxs = new JComboBox(ttcManager.getTTCrxsVec());
    jCbBxRx = new JComboBox(ttcManager.getTTCrxsVec());

    contentPane = (JPanel)this.getContentPane();
    border8 = BorderFactory.createBevelBorder(BevelBorder.RAISED, Color.white,
                                              Color.white,
                                              new Color(124, 124, 0),
                                              new Color(178, 178, 0));
    jBtAddRx.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jBtAddRx_actionPerformed(e);
      }
    });
    jBtAddRx.setText("Add");
    jBtAddRx.setMargin(new Insets(2, 4, 2, 4));
    jBtAddRx.setPreferredSize(new Dimension(60, 27));
    jChBxEnableATT.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jChBxEnableATT_actionPerformed(e);
      }
    });
    jChBxEnableATT.setText("Enable after-trigger transfer");
    jChBxEnableATT.setSelected(true);
    jChBxEnableATT.setBackground(Color.yellow);
    jBtSetTrigReg.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jBtSetTrigReg_actionPerformed(e);
      }
    });
    jBtSetTrigReg.setText("Set");
    jBtSetTrigReg.setBackground(SystemColor.info);
    jTxtFldNewPresetName.setText("NewPresetName");
    jTxtFldNewPresetName.setPreferredSize(new Dimension(100, 21));
    jTxtFldNewPresetName.setMinimumSize(new Dimension(100, 21));
    jPanel13.setLayout(flowLayout25);
    jPanel13.setPreferredSize(new Dimension(246, 160));
    jPanel13.setBorder(border8);
    jPanel13.setMinimumSize(new Dimension(240, 160));
    jPanel13.setBackground(Color.yellow);
    jPanel14.setLayout(gridBagLayout10);
    jPanel14.setPreferredSize(new Dimension(246, 350));
    jPanel14.setBorder(BorderFactory.createRaisedBevelBorder());
    jPanel14.setMinimumSize(new Dimension(240, 339));
    flowLayout23.setAlignment(FlowLayout.LEFT);
    jBtDeletePreset.setMargin(new Insets(2, 4, 2, 4));
    jBtDeletePreset.setPreferredSize(new Dimension(70, 27));
    jBtDeletePreset.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jBtDeletePreset_actionPerformed(e);
      }
    });
    jBtDeletePreset.setText("Delete");
    jPanel15.setLayout(flowLayout19);
    jPanel15.setPreferredSize(new Dimension(230, 70));
    jPanel15.setBorder(BorderFactory.createEtchedBorder());
    jPanel15.setMinimumSize(new Dimension(230, 70));
    jLabel20.setText("Triggword register");
    jLabel20.setPreferredSize(new Dimension(200, 27));
    jLabel20.setFont(new java.awt.Font("Dialog", 1, 14));
    flowLayout24.setAlignment(FlowLayout.LEFT);
    jPanel16.setPreferredSize(new Dimension(246, 125));
    jPanel16.setBorder(BorderFactory.createRaisedBevelBorder());
    jPanel16.setMinimumSize(new Dimension(240, 120));
    jPanel16.setLayout(flowLayout26);
    flowLayout25.setVgap(2);
    flowLayout25.setAlignment(FlowLayout.LEFT);
    jBtAddPreset.setMargin(new Insets(2, 4, 2, 4));
    jBtAddPreset.setPreferredSize(new Dimension(70, 27));
    jBtAddPreset.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jBtAddPreset_actionPerformed(e);
      }
    });
    jBtAddPreset.setText("Add");
    jLabel21.setText("TTCrx\'s conntrol");
    jLabel21.setHorizontalTextPosition(SwingConstants.CENTER);
    jLabel21.setHorizontalAlignment(SwingConstants.CENTER);
    jLabel21.setPreferredSize(new Dimension(158, 30));
    jLabel21.setFont(new java.awt.Font("Dialog", 1, 14));
    flowLayout26.setAlignment(FlowLayout.LEFT);
    jLabel27.setText("Presettings");
    jLabel27.setPreferredSize(new Dimension(163, 17));
    jLabel27.setFont(new java.awt.Font("Dialog", 1, 14));
    jChBxExtSub.setText("External Subaddress");
    jChBxExtSub.setBackground(Color.yellow);
    jLabel110.setText("Subaddress");
    jLabel111.setText("TTCrx");
    jBtApplayPreset.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jBtApplayPreset_actionPerformed(e);
      }
    });
    jBtApplayPreset.setText("Apply");
    jCbBxRxs.setPreferredSize(new Dimension(145, 21));
    jCbBxRxs.setMinimumSize(new Dimension(145, 21));
    jCbBxRxs.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jCbBxRxs_actionPerformed(e);
      }
    });
    jCbBxRxTR.setPreferredSize(new Dimension(145, 21));
    jCbBxRxTR.setMinimumSize(new Dimension(145, 21));
    jTxtFldSub.setText("000");
    jPanel36.setLayout(flowLayout23);
    jPanel36.setBackground(Color.yellow);
    jPanel37.setLayout(flowLayout24);
    jPanel37.setBackground(Color.yellow);
    jPanel38.setLayout(gridBagLayout11);
    jPanel38.setMinimumSize(new Dimension(180, 230));
    jPanel38.setPreferredSize(new Dimension(210, 230));
    flowLayout19.setVgap(7);
    jLabel19.setText("TTCrx");
    jCbBxPresets.setPreferredSize(new Dimension(120, 21));
    jCbBxPresets.setMinimumSize(new Dimension(140, 21));
    jBtDeleteRx.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jBtDeleteRx_actionPerformed(e);
      }
    });
    jBtDeleteRx.setText("Delete");
    jBtDeleteRx.setMargin(new Insets(2, 4, 2, 4));
    jBtDeleteRx.setPreferredSize(new Dimension(60, 27));
    jTBtBGo0.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jTBtBGo0_actionPerformed(e);
      }
    });
    jTBtBGo0.setText(" B-Go 0");
    jTBtBGo0.setSelected(true);
    jTBtBGo0.setBorder(border1);
    jBtLoadFIFO.setText("Load FIFO");
    jBtLoadFIFO.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jBtLoadFIFO_actionPerformed(e);
      }
    });
    jBtLoadFIFO.setMargin(new Insets(2, 4, 2, 4));
    jBtLoadFIFO.setMinimumSize(new Dimension(100, 27));
    jBtLoadFIFO.setPreferredSize(new Dimension(100, 27));
    jTBtBGo1.setBorder(BorderFactory.createLoweredBevelBorder());
    jTBtBGo1.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jTBtBGo1_actionPerformed(e);
      }
    });
    jTBtBGo1.setText("B-Go 1");
    jTBtBGo2.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jTBtBGo2_actionPerformed(e);
      }
    });
    jTBtBGo2.setText("B-Go 2");
    jTBtBGo2.setBorder(BorderFactory.createLoweredBevelBorder());
    jBtFIFOStatus.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jBtFIFOStatus_actionPerformed(e);
      }
    });
    jBtFIFOStatus.setText("FIFO Status");
    jBtFIFOStatus.setMargin(new Insets(2, 4, 2, 4));
    jBtFIFOStatus.setMinimumSize(new Dimension(100, 27));
    jBtFIFOStatus.setPreferredSize(new Dimension(100, 27));
    jTBtBGo3.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jTBtBGo3_actionPerformed(e);
      }
    });
    jTBtBGo3.setText("B-Go 3");
    jTBtBGo3.setBorder(BorderFactory.createLoweredBevelBorder());
    jPanel10.setLayout(gridBagLayout6);
    jPanel10.setPreferredSize(new Dimension(312, 233));
    jPanel10.setBorder(BorderFactory.createRaisedBevelBorder());
    jPanel10.setMinimumSize(new Dimension(300, 235));
    jPanel11.setLayout(gridBagLayout3);
    jPanel11.setPreferredSize(new Dimension(300, 180));
    jPanel11.setBorder(BorderFactory.createEtchedBorder());
    jPanel12.setLayout(gridBagLayout5);
    jPanel12.setPreferredSize(new Dimension(300, 70));
    jPanel12.setBorder(BorderFactory.createEtchedBorder());
    jBtReadBGoSettings.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jBtReadBGoSettings_actionPerformed(e);
      }
    });
    jBtReadBGoSettings.setText("Read");
    jBtReadBGoSettings.setMargin(new Insets(2, 4, 2, 4));
    jBtReadBGoSettings.setAlignmentY( (float) 0.0);
    jPanel17.setPreferredSize(new Dimension(300, 30));
    jPanel17.setBorder(BorderFactory.createRaisedBevelBorder());
    jPanel17.setBackground(Color.yellow);
    jChBxExternal.setText("External Subaddress");
    jChBxExternal.setPreferredSize(new Dimension(240, 25));
    jChBxExternal.setMaximumSize(new Dimension(200, 25));
    jBtResetTTCvi.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jBtResetTTCvi_actionPerformed(e);
      }
    });
    jBtResetTTCvi.setMaximumSize(new Dimension(90, 27));
    jBtResetTTCvi.setPreferredSize(new Dimension(100, 27));
    jBtResetTTCvi.setText("Reset TCvi");
    jBtBGoSignal.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jBtBGoSignal_actionPerformed(e);
      }
    });
    jBtBGoSignal.setText("B-Go Signal");
    jBtBGoSignal.setMargin(new Insets(2, 4, 2, 4));
    jBtBGoSignal.setMinimumSize(new Dimension(100, 27));
    jBtBGoSignal.setPreferredSize(new Dimension(100, 27));
    jTxtFldBGoSettings.setText("00000");
    jTxtFldBGoSettings.setEditable(false);
    jBtDelete.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jBtDelete_actionPerformed(e);
      }
    });
    jBtDelete.setText("Delete");
    jBtDelete.setMargin(new Insets(2, 4, 2, 4));
    borderLayout3.setHgap(3);
    jLabel28.setText("Yellow things available only in TTCvi Mk II ");
    jLabel28.setBackground(Color.yellow);
    jChBFIFO.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jChBFIFO_actionPerformed(e);
      }
    });
    jChBFIFO.setText("FIFO");
    jChBFIFO.setPreferredSize(new Dimension(81, 25));
    jChBFIFO.setMinimumSize(new Dimension(81, 25));
    jChBRetrensmit.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jChBRetrensmit_actionPerformed(e);
      }
    });
    jChBRetrensmit.setText("Retransmit");
    jBtAddBGoData.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jBtAddBGoData_actionPerformed(e);
      }
    });
    jBtAddBGoData.setText("Add");
    jBtAddBGoData.setMargin(new Insets(2, 4, 2, 4));
    jBtAddBGoData.setPreferredSize(new Dimension(51, 27));
    jPanel6.setLayout(gridBagLayout8);
    jPanel6.setPreferredSize(new Dimension(312, 350));
    jPanel6.setBorder(BorderFactory.createRaisedBevelBorder());
    jPanel6.setMinimumSize(new Dimension(300, 339));
    jPanel7.setLayout(gridBagLayout9);
    jPanel7.setPreferredSize(new Dimension(145, 220));
    jPanel7.setBorder(BorderFactory.createEtchedBorder());
    jPanel7.setMinimumSize(new Dimension(140, 220));
    jPanel7.setAlignmentX( (float) 1.0);
    jPanel8.setLayout(flowLayout9);
    jPanel8.setPreferredSize(new Dimension(145, 220));
    jPanel8.setBorder(BorderFactory.createEtchedBorder());
    jPanel8.setMinimumSize(new Dimension(140, 220));
    jPanel8.setAlignmentX( (float) 1.0);
    jPanel9.setLayout(gridLayout1);
    jPanel24.setLayout(gridBagLayout4);
    jPanel24.setMinimumSize(new Dimension(300, 68));
    jPanel24.setPreferredSize(new Dimension(312, 70));
    jChBEnable.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jChBEnable_actionPerformed(e);
      }
    });
    jChBEnable.setText("Enable");
    jPanel29.setMinimumSize(new Dimension(57, 27));
    jPanel29.setLayout(borderLayout3);
    jButton4.setText("Help");
    jButton4.setMaximumSize(new Dimension(72, 27));
    jButton4.setMinimumSize(new Dimension(72, 25));
    jButton4.setPreferredSize(new Dimension(95, 27));
    jButton5.setText("About");
    jButton5.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jButton5_actionPerformed(e);
      }
    });
    jButton5.setMaximumSize(new Dimension(72, 27));
    jButton5.setPreferredSize(new Dimension(95, 27));
    jChBCalibration.setText("Calibration");
    jChBCalibration.setEnabled(false);
    jChBCalibration.setBackground(Color.yellow);
    jLstBGoData.addMouseListener(new java.awt.event.MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        jLstBGoData_mouseClicked(e);
      }
    });
    jLstBGoData.setPreferredSize(new Dimension(105, 130));
    jLstBGoData.setBorder(BorderFactory.createLoweredBevelBorder());
    jChBRepetetive.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jChBRepetetive_actionPerformed(e);
      }
    });
    jChBRepetetive.setText("Repetetive");
    jBtSendLongCycle.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jBtSendLongCycle_actionPerformed(e);
      }
    });
    jBtSendLongCycle.setText("Send");
    jChBAsynchron.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jChBAsynchron_actionPerformed(e);
      }
    });
    jChBAsynchron.setText("Asynchron");
    flowLayout9.setAlignment(FlowLayout.LEFT);
    jPanel30.setPreferredSize(new Dimension(310, 80));
    jPanel30.setAlignmentY( (float) 2.0);
    jPanel30.setAlignmentX( (float) 2.0);
    jPanel30.setLayout(gridBagLayout7);
    jLabel11.setText("Settings");
    jLabel11.setPreferredSize(new Dimension(85, 17));
    jLabel12.setText("Data");
    jLabel12.setPreferredSize(new Dimension(86, 17));
    jBtSendShortCycle.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jBtSendShortCycle_actionPerformed(e);
      }
    });
    jBtSendShortCycle.setText("Send");
    jLabel13.setText("Long-format asynchronous cycle");
    jLabel14.setText("Short-format asynchronous cycle");
    jLabel14.setPreferredSize(new Dimension(240, 17));
    jBtResetFIFO.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jBtResetFIFO_actionPerformed(e);
      }
    });
    jBtResetFIFO.setText("Reset FIFO");
    jBtResetFIFO.setMargin(new Insets(2, 4, 2, 4));
    jBtResetFIFO.setMinimumSize(new Dimension(100, 27));
    jBtResetFIFO.setPreferredSize(new Dimension(100, 27));
    jLabel15.setText("Command");
    jTxtFldCommand.setMinimumSize(new Dimension(20, 21));
    jTxtFldCommand.setPreferredSize(new Dimension(20, 21));
    jTxtFldCommand.setText("00");
    gridLayout3.setVgap(1);
    gridLayout3.setRows(8);
    gridLayout3.setColumns(1);
    gridLayout3.setHgap(2);
    gridLayout5.setColumns(2);
    gridLayout6.setHgap(3);
    gridLayout6.setColumns(2);
    jPanel18.setPreferredSize(new Dimension(200, 29));
    jPanel18.setLayout(borderLayout13);
    jPanel19.setPreferredSize(new Dimension(182, 25));
    jPanel19.setLayout(borderLayout14);
    borderLayout1.setHgap(3);
    borderLayout2.setHgap(3);
    jBtTrigger.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jBtTrigger_actionPerformed(e);
      }
    });
    jBtTrigger.setText("Trigger");
    jBtTrigger.setPreferredSize(new Dimension(90, 29));
    jBtTrigger.setEnabled(false);
    jLabel1.setText("CSR1");
    jLabel1.setFont(new java.awt.Font("Dialog", 1, 14));
    jBtResetBGoFIFOs.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jBtResetBGoFIFOs_actionPerformed(e);
      }
    });
    jBtResetBGoFIFOs.setText("Reset B-Go FIFOs");
    jBtResetBGoFIFOs.setMargin(new Insets(2, 4, 2, 4));
    jPanel1.setLayout(gridBagLayout12);
    jPanel1.setToolTipText("CSR1");
    jPanel1.setPreferredSize(new Dimension(217, 360));
    jPanel1.setBorder(BorderFactory.createRaisedBevelBorder());
    jLabel2.setText("Trigger Source");
    jLabel2.setPreferredSize(new Dimension(130, 17));
    jLabel2.setMinimumSize(new Dimension(100, 17));
    jPanel2.setPreferredSize(new Dimension(210, 220));
    jPanel2.setBorder(BorderFactory.createEtchedBorder());
    jPanel2.setLayout(gridLayout3);
    jLabel3.setText("Orbit Signal");
    jPanel3.setBorder(border7);
    jPanel3.setLayout(gridLayout6);
    jLabel4.setText("CSR2");
    jLabel4.setPreferredSize(new Dimension(200, 17));
    jLabel4.setFont(new java.awt.Font("Dialog", 1, 14));
    jPanel4.setLayout(flowLayout3);
    jPanel4.setPreferredSize(new Dimension(217, 90));
    jPanel4.setBorder(BorderFactory.createRaisedBevelBorder());
    jPanel4.setMinimumSize(new Dimension(217, 90));
    jPanel5.setLayout(gridBagLayout2);
    jPanel5.setPreferredSize(new Dimension(217, 220));
    jPanel5.setBorder(BorderFactory.createRaisedBevelBorder());
    jPanel5.setMinimumSize(new Dimension(217, 210));
    jLabel9.setText("Inhibit 2");
    jCbBxTrgRate.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jCbBxTrgRate_actionPerformed(e);
      }
    });
    jCbBxTrgRate.setEnabled(true);
    jPanel20.setLayout(gridLayout5);
    jPanel22.setLayout(borderLayout1);
    jPanel23.setLayout(borderLayout2);
    jChBOrbitSignal.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jChBOrbitSignal_actionPerformed(e);
      }
    });
    jChBOrbitSignal.setText("internal");
    jRBtTrgSrc0.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jRBtTrgSrc0_actionPerformed(e);
      }
    });
    jRBtTrgSrc0.setText("0 L1A ECL");
    jRBtTrgSrc0.setSelected(true);
    jRBtTrgSrc0.setPreferredSize(new Dimension(130, 25));
    jRBtTrgSrc1.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jRBtTrgSrc1_actionPerformed(e);
      }
    });
    jRBtTrgSrc1.setText("1 L1A NIM");
    jRBtTrgSrc1.setPreferredSize(new Dimension(130, 25));
    jRBtTrgSrc2.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jRBtTrgSrc2_actionPerformed(e);
      }
    });
    jRBtTrgSrc2.setText("2 L1A NIM");
    jRBtTrgSrc2.setPreferredSize(new Dimension(130, 25));
    jBtReadCSR1.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jBtReadCSR1_actionPerformed(e);
      }
    });
    jBtReadCSR1.setText("Read CSR1");
    jBtReadCSR1.setMargin(new Insets(2, 4, 2, 4));
    jBtReadCSR1.setHorizontalTextPosition(SwingConstants.LEFT);
    jRBtTrgSrc3.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jRBtTrgSrc3_actionPerformed(e);
      }
    });
    jRBtTrgSrc3.setText("3 L1A NIM");
    jRBtTrgSrc3.setPreferredSize(new Dimension(130, 25));
    jBtReadCSR2.setHorizontalTextPosition(SwingConstants.LEFT);
    jBtReadCSR2.setMargin(new Insets(2, 4, 2, 4));
    jBtReadCSR2.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jBtReadCSR2_actionPerformed(e);
      }
    });
    jBtReadCSR2.setText("Read CSR2");
    jRBtTrgSrc4.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jRBtTrgSrc4_actionPerformed(e);
      }
    });
    jRBtTrgSrc4.setText("4 VME Send");
    jRBtTrgSrc5.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jRBtTrgSrc5_actionPerformed(e);
      }
    });
    jRBtTrgSrc5.setText("5 Random");
    jRBtTrgSrc6.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jRBtTrgSrc6_actionPerformed(e);
      }
    });
    jRBtTrgSrc6.setText("6 Calibration");
    jRBtTrgSrc6.setBackground(Color.yellow);
    flowLayout3.setVgap(3);
    flowLayout3.setHgap(3);
    flowLayout3.setAlignment(FlowLayout.LEFT);
    jBtL1AFIFOReset.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jBtL1AFIFOReset_actionPerformed(e);
      }
    });
    jBtL1AFIFOReset.setText("L1A FIFO Reset");
    jTxtFldCSR1.setText("1000000000000001");
    jTxtFldCSR1.setEditable(false);
    jTxtFldCSR1.setFont(new java.awt.Font("Dialog", 0, 10));
    jTxtFldCSR2.setEditable(false);
    jTxtFldCSR2.setFont(new java.awt.Font("Dialog", 0, 10));
    jTxtFldCSR2.setText("1000000000000001");
    border1 = BorderFactory.createLineBorder(new Color(204, 204, 204), 2);
    border2 = BorderFactory.createCompoundBorder(BorderFactory.
                                                 createBevelBorder(BevelBorder.
        RAISED, Color.white, Color.white, new Color(99, 99, 99),
        new Color(142, 142, 142)),
                                                 BorderFactory.
                                                 createEmptyBorder(0, 5, 5, 5));
    border3 = BorderFactory.createCompoundBorder(BorderFactory.
                                                 createBevelBorder(BevelBorder.
        RAISED, Color.white, Color.white, new Color(99, 99, 99),
        new Color(142, 142, 142)),
                                                 BorderFactory.
                                                 createEmptyBorder(0, 5, 5, 5));
    border4 = BorderFactory.createEmptyBorder(5, 5, 5, 5);
    border5 = BorderFactory.createCompoundBorder(BorderFactory.
                                                 createEtchedBorder(Color.white,
        new Color(142, 142, 142)), BorderFactory.createEmptyBorder(0, 5, 2, 5));
    border6 = BorderFactory.createCompoundBorder(BorderFactory.
                                                 createBevelBorder(BevelBorder.
        RAISED, Color.white, Color.white, new Color(99, 99, 99),
        new Color(142, 142, 142)),
                                                 BorderFactory.
                                                 createEmptyBorder(5, 5, 5, 5));
    border7 = BorderFactory.createCompoundBorder(BorderFactory.
                                                 createEtchedBorder(Color.white,
        new Color(142, 142, 142)), BorderFactory.createEmptyBorder(0, 5, 0, 5));
    contentPane.setLayout(gridBagLayout13);
    this.setSize(new Dimension(802, 712));
    this.setTitle("TTC Control");

    contentPane.setBorder(border4);
    contentPane.setMinimumSize(new Dimension(320, 320));
    contentPane.setPreferredSize(new Dimension(700, 600));
    jPanel21.setLayout(gridBagLayout1);
    jLabel7.setText("Duration");
    jLabel7.setHorizontalTextPosition(SwingConstants.RIGHT);
    jLabel7.setToolTipText("");
    jLabel7.setPreferredSize(new Dimension(80, 17));
    jLabel6.setText("Delay");
    jLabel6.setHorizontalTextPosition(SwingConstants.CENTER);
    jLabel6.setHorizontalAlignment(SwingConstants.CENTER);
    jLabel6.setPreferredSize(new Dimension(100, 17));
    pSEdtInhDel2.setMinimumSize(new Dimension(70, 40));
    pSEdtInhDel2.setPreferredSize(new Dimension(71, 40));
    pSEdtInhDur2.setMinimumSize(new Dimension(70, 40));
    pSEdtInhDur2.setPreferredSize(new Dimension(71, 40));
    jLabel10.setText("Inhibit 3");
    pSEdtInhDel3.setMinimumSize(new Dimension(70, 40));
    pSEdtInhDel3.setPreferredSize(new Dimension(71, 40));
    pSEdtInhDur3.setMinimumSize(new Dimension(70, 40));
    pSEdtInhDur3.setPreferredSize(new Dimension(71, 40));
    pSEdtInhDel0.setMinimumSize(new Dimension(70, 40));
    pSEdtInhDel0.setPreferredSize(new Dimension(71, 40));
    jLabel5.setText("Inhibit 0");
    pSEdtInhDur0.setMinimumSize(new Dimension(70, 40));
    pSEdtInhDur0.setPreferredSize(new Dimension(71, 40));
    pSEdtInhDel1.setMinimumSize(new Dimension(70, 40));
    pSEdtInhDel1.setPreferredSize(new Dimension(71, 40));
    jLabel8.setText("Inhibit 1");
    pSEdtInhDur1.setMinimumSize(new Dimension(70, 40));
    pSEdtInhDur1.setPreferredSize(new Dimension(71, 40));
    jLabel17.setText("Data");
    jTxtFldData.setText("00");
    jTxtFldData.setMinimumSize(new Dimension(20, 21));
    jTxtFldData.setPreferredSize(new Dimension(20, 21));
    jTxtFldData.setToolTipText("in hex format");
    jLabel16.setText("Subaddress");
    jTxtFldSubAddress.setText("00");
    jTxtFldSubAddress.setMinimumSize(new Dimension(20, 21));
    jTxtFldSubAddress.setPreferredSize(new Dimension(20, 21));
    jTxtFldSubAddress.setToolTipText("in hex format");
    jLabel18.setText("TTCrx");
    jLabel18.setToolTipText("");
    jCbBxRx.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jCbBxRx_actionPerformed(e);
      }
    });
    jCbBxRx.setToolTipText("");
    jCbBxRx.setPreferredSize(new Dimension(130, 21));
    jCbBxRx.setMinimumSize(new Dimension(130, 21));
    jPanel9.setPreferredSize(new Dimension(310, 21));
    jLabel25.setText("Coar Delay 2");
    jLabel24.setText("Coar Delay 1");
    jLabel23.setText("Deskew 2");
    jLabel22.setText("Deskew 1");
    jLabel26.setText("Control Register");
    pSEdtDskw1.setMinimumSize(new Dimension(70, 41));
    pSEdtDskw1.setPreferredSize(new Dimension(70, 41));
    pSEdtCoarDel2.setMinimumSize(new Dimension(70, 41));
    pSEdtCoarDel2.setPreferredSize(new Dimension(70, 41));
    pSEdtCoarDel1.setMinimumSize(new Dimension(70, 41));
    pSEdtCoarDel1.setPreferredSize(new Dimension(70, 41));
    jBtCntrlReg.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jBtCntrlReg_actionPerformed(e);
      }
    });
    jBtCntrlReg.setText("1001001");
    jBtCntrlReg.setMargin(new Insets(2, 4, 2, 4));
    jBtCntrlReg.setPreferredSize(new Dimension(70, 27));
    jBtCntrlReg.setFont(new java.awt.Font("Dialog", 1, 10));
    pSEdtDskw2.setMinimumSize(new Dimension(70, 41));
    pSEdtDskw2.setPreferredSize(new Dimension(70, 41));
    jScrollPane1.setAutoscrolls(false);
    jScrollPane1.setPreferredSize(new Dimension(700, 600));
    jPanel21.setMinimumSize(new Dimension(772, 600));
    jPanel21.setPreferredSize(new Dimension(785, 700));
    jPanel5.add(jLabel6, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
                                                , GridBagConstraints.CENTER,
                                                GridBagConstraints.NONE,
                                                new Insets(0, 0, 5, 0), 0, 0));
    jPanel5.add(jLabel7, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0
                                                , GridBagConstraints.CENTER,
                                                GridBagConstraints.NONE,
                                                new Insets(0, 0, 5, 0), 0, 0));
    jPanel5.add(jLabel9, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
                                                , GridBagConstraints.CENTER,
                                                GridBagConstraints.NONE,
                                                new Insets(0, 5, 0, 5), 0, 0));
    jPanel5.add(pSEdtInhDel2, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE,
        new Insets(0, 0, 7, 10), 0, 0));
    jPanel5.add(pSEdtInhDur2, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE,
        new Insets(0, 0, 7, 5), 0, 0));
    jPanel5.add(jLabel10, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
                                                 , GridBagConstraints.CENTER,
                                                 GridBagConstraints.NONE,
                                                 new Insets(0, 5, 0, 5), 0, 0));
    jPanel5.add(pSEdtInhDel3, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE,
        new Insets(0, 0, 7, 10), 0, 0));
    jPanel5.add(pSEdtInhDur3, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE,
        new Insets(0, 0, 7, 5), 0, 0));
    jPanel5.add(pSEdtInhDel0, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE,
        new Insets(0, 0, 7, 10), 0, 0));
    jPanel5.add(jLabel5, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
                                                , GridBagConstraints.CENTER,
                                                GridBagConstraints.NONE,
                                                new Insets(0, 5, 0, 5), 0, 0));
    jPanel5.add(pSEdtInhDur0, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE,
        new Insets(0, 0, 7, 5), 0, 0));
    jPanel5.add(pSEdtInhDel1, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE,
        new Insets(0, 0, 7, 10), 0, 0));
    jPanel5.add(jLabel8, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
                                                , GridBagConstraints.CENTER,
                                                GridBagConstraints.NONE,
                                                new Insets(0, 5, 0, 5), 0, 0));
    jPanel5.add(pSEdtInhDur1, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE,
        new Insets(0, 0, 7, 5), 0, 0));
    jPanel21.add(jPanel16,  new GridBagConstraints(2, 3, 1, 2, 0.0, 0.0
            ,GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
    jPanel4.add(jLabel4, null);
    jPanel4.add(jBtResetBGoFIFOs, null);
    jPanel4.add(jPanel23, null);
    jPanel23.add(jBtReadCSR2, BorderLayout.WEST);
    jPanel23.add(jTxtFldCSR2, BorderLayout.EAST);
    jPanel21.add(jPanel1,   new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 0, 5, 5), 0, 0));
    jPanel1.add(jLabel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
                                                , GridBagConstraints.WEST,
                                                GridBagConstraints.NONE,
                                                new Insets(3, 3, 0, 173), 0, 0));
    jPanel1.add(jPanel2, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0
                                                , GridBagConstraints.CENTER,
                                                GridBagConstraints.BOTH,
                                                new Insets(0, 5, 5, 5), 0, 0));
    jPanel2.add(jLabel2, null);
    jPanel2.add(jRBtTrgSrc0, null);
    jPanel2.add(jRBtTrgSrc1, null);
    jPanel2.add(jRBtTrgSrc2, null);
    jPanel2.add(jRBtTrgSrc3, null);
    jPanel2.add(jPanel18, null);
    jPanel18.add(jRBtTrgSrc4, BorderLayout.WEST);
    jPanel18.add(jBtTrigger, BorderLayout.EAST);
    jPanel2.add(jPanel19, null);
    jPanel19.add(jRBtTrgSrc5, BorderLayout.WEST);
    jPanel19.add(jCbBxTrgRate, BorderLayout.EAST);
    jPanel2.add(jPanel20, null);
    jPanel20.add(jRBtTrgSrc6, null);
    jPanel1.add(jPanel3, new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0
                                                , GridBagConstraints.CENTER,
                                                GridBagConstraints.BOTH,
                                                new Insets(0, 5, 5, 5), 0, 0));
    jPanel3.add(jLabel3, null);
    jPanel3.add(jChBOrbitSignal, null);
    jPanel1.add(jBtL1AFIFOReset, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE,
        new Insets(0, 5, 5, 5), 0, 0));
    jPanel1.add(jPanel22, new GridBagConstraints(0, 4, 1, 1, 1.0, 1.0
                                                 , GridBagConstraints.WEST,
                                                 GridBagConstraints.BOTH,
                                                 new Insets(0, 5, 5, 25), 0, 0));
    jPanel22.add(jBtReadCSR1, BorderLayout.WEST);
    jPanel22.add(jTxtFldCSR1, BorderLayout.EAST);
    jPanel21.add(jPanel6,  new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(5, 0, 5, 5), 0, 0));
    jPanel24.add(jButton5,     new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));
    jPanel24.add(jBtResetTTCvi, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE,
        new Insets(0, 0, 0, 0), 0, 0));
    jPanel24.add(jButton4,    new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    jPanel24.add(jPanel17, new GridBagConstraints(0, 1, 3, 1, 0.0, 0.0
                                                  , GridBagConstraints.NORTH,
                                                  GridBagConstraints.VERTICAL,
                                                  new Insets(10, 0, 0, 0), 0, 0));
    jPanel17.add(jLabel28, null);
    jPanel21.add(jPanel14,   new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(5, 0, 5, 5), 0, 0));
    jPanel30.add(jBtFIFOStatus, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE,
        new Insets(5, 51, 5, 5), 0, 0));
    jPanel30.add(jBtLoadFIFO, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE,
        new Insets(5, 5, 5, 52), 0, 0));
    jPanel30.add(jBtResetFIFO, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE,
        new Insets(0, 51, 5, 5), 0, 0));
    jPanel30.add(jBtBGoSignal, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE,
        new Insets(0, 5, 5, 52), 0, 0));
    jPanel6.add(jPanel7, new GridBagConstraints(0, 1, 1, 1, 0.5, 0.0
                                                , GridBagConstraints.NORTHWEST,
                                                GridBagConstraints.HORIZONTAL,
                                                new Insets(10, 5, 5, 5), 0, 0));
    jPanel6.add(jPanel9, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0
                                                , GridBagConstraints.CENTER,
                                                GridBagConstraints.HORIZONTAL,
                                                new Insets(0, 0, 0, 0), 0, 0));
    jPanel9.add(jTBtBGo0, null);
    jPanel9.add(jTBtBGo1, null);
    jPanel9.add(jTBtBGo2, null);
    jPanel9.add(jTBtBGo3, null);
    jPanel6.add(jPanel30, new GridBagConstraints(0, 2, 2, 1, 0.0, 1.0
                                                 , GridBagConstraints.NORTH,
                                                 GridBagConstraints.NONE,
                                                 new Insets(10, 0, 0, 0), -49,
                                                 0));
    jPanel7.add(jLabel11, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
                                                 , GridBagConstraints.WEST,
                                                 GridBagConstraints.NONE,
                                                 new Insets(0, 5, 0, 41), 0, 0));
    jPanel7.add(jChBEnable, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE,
        new Insets(0, 5, 0, 67), 0, 0));
    jPanel7.add(jChBAsynchron, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE,
        new Insets(0, 5, 0, 49), 0, 0));
    jPanel7.add(jChBRepetetive, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE,
        new Insets(0, 5, 0, 48), 0, 0));
    jPanel7.add(jChBFIFO, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
                                                 , GridBagConstraints.WEST,
                                                 GridBagConstraints.NONE,
                                                 new Insets(0, 5, 0, 50), 0, 0));
    jPanel7.add(jChBCalibration, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE,
        new Insets(0, 5, 0, 41), 0, 0));
    jPanel7.add(jChBRetrensmit, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE,
        new Insets(0, 5, 0, 41), 0, 0));
    jPanel7.add(jPanel29, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0
                                                 , GridBagConstraints.CENTER,
                                                 GridBagConstraints.BOTH,
                                                 new Insets(0, 5, 1, 41), 0, 0));
    jPanel29.add(jBtReadBGoSettings, BorderLayout.WEST);
    jPanel29.add(jTxtFldBGoSettings, BorderLayout.EAST);
    jPanel6.add(jPanel8, new GridBagConstraints(1, 1, 1, 1, 0.5, 0.0
                                                , GridBagConstraints.NORTHEAST,
                                                GridBagConstraints.NONE,
                                                new Insets(10, 0, 5, 5), 0, 0));
    jPanel8.add(jLabel12, null);
    jPanel8.add(jLstBGoData, null);
    jPanel8.add(jBtDelete, null);
    jPanel8.add(jBtAddBGoData, null);
    jPanel21.add(jPanel10, new GridBagConstraints(1, 1, 1, 3, 0.0, 0.0
                                                  , GridBagConstraints.NORTH,
                                                  GridBagConstraints.NONE,
                                                  new Insets(0, 0, 5, 5), 0, 0));
    jPanel12.add(jLabel14, new GridBagConstraints(0, 0, 3, 1, 0.0, 0.0
                                                  , GridBagConstraints.WEST,
                                                  GridBagConstraints.NONE,
                                                  new Insets(5, 5, 0, 0), 0, 0));
    jPanel12.add(jLabel15, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
                                                  , GridBagConstraints.WEST,
                                                  GridBagConstraints.NONE,
                                                  new Insets(10, 5, 10, 0), 0,
                                                  0));
    jPanel12.add(jTxtFldCommand, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE,
        new Insets(10, 21, 12, 0), 0, 0));
    jPanel12.add(jBtSendShortCycle, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE,
        new Insets(10, 81, 12, 0), 0, 0));
    jPanel21.add(jPanel24,   new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
    jPanel10.add(jPanel11, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0
                                                  , GridBagConstraints.CENTER,
                                                  GridBagConstraints.BOTH,
                                                  new Insets(5, 5, 5, 5), 0, 0));
    jPanel11.add(jLabel13, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0
                                                  , GridBagConstraints.WEST,
                                                  GridBagConstraints.NONE,
                                                  new Insets(5, 5, 5, 0), 0, 0));
    jPanel11.add(jChBxExternal, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE,
        new Insets(0, 5, 5, 0), 0, 0));
    jPanel11.add(jBtSendLongCycle, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE,
        new Insets(0, 0, 5, 0), 0, 0));
    jPanel11.add(jLabel17, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
                                                  , GridBagConstraints.WEST,
                                                  GridBagConstraints.NONE,
                                                  new Insets(0, 5, 5, 0), 0, 0));
    jPanel11.add(jTxtFldData, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE,
        new Insets(0, 10, 0, 0), 0, 0));
    jPanel11.add(jLabel16, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
                                                  , GridBagConstraints.WEST,
                                                  GridBagConstraints.NONE,
                                                  new Insets(0, 5, 5, 0), 0, 0));
    jPanel11.add(jTxtFldSubAddress, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE,
        new Insets(0, 10, 0, 0), 0, 0));
    jPanel11.add(jLabel18, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
                                                  , GridBagConstraints.WEST,
                                                  GridBagConstraints.NONE,
                                                  new Insets(0, 5, 5, 0), 0, 0));
    jPanel11.add(jCbBxRx, new GridBagConstraints(1, 1, 2, 1, 0.0, 0.0
                                                 , GridBagConstraints.WEST,
                                                 GridBagConstraints.NONE,
                                                 new Insets(0, 10, 5, 0), 0, 0));
    jPanel10.add(jPanel12, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0
                                                  , GridBagConstraints.CENTER,
                                                  GridBagConstraints.BOTH,
                                                  new Insets(0, 5, 5, 5), 0, 0));
    jPanel16.add(jLabel27, null);
    jPanel16.add(jCbBxPresets, null);
    jPanel16.add(jBtApplayPreset, null);
    jPanel16.add(jBtAddPreset, null);
    jPanel16.add(jBtDeletePreset, null);
    jPanel16.add(jTxtFldNewPresetName, null);
    jPanel21.add(jPanel4, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
                                                 , GridBagConstraints.WEST,
                                                 GridBagConstraints.NONE,
                                                 new Insets(0, 0, 5, 5), 0, 0));
    jPanel14.add(jLabel21, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
                                                  , GridBagConstraints.WEST,
                                                  GridBagConstraints.NONE,
                                                  new Insets(0, 0, 0, 0), 84, 0));
    jPanel14.add(jPanel38, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
                                                  , GridBagConstraints.CENTER,
                                                  GridBagConstraints.BOTH,
                                                  new Insets(0, 5, 5, 5), 0, 0));
    jPanel38.add(jLabel25, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
                                                  , GridBagConstraints.WEST,
                                                  GridBagConstraints.NONE,
                                                  new Insets(0, 5, 5, 0), 0, 0));
    jPanel38.add(jLabel23, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
                                                  , GridBagConstraints.WEST,
                                                  GridBagConstraints.NONE,
                                                  new Insets(0, 5, 5, 0), 0, 0));
    jPanel38.add(jLabel24, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
                                                  , GridBagConstraints.WEST,
                                                  GridBagConstraints.NONE,
                                                  new Insets(0, 5, 5, 0), 0, 0));
    jPanel38.add(pSEdtCoarDel2, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE,
        new Insets(0, 5, 10, 0), 0, 0));
    jPanel38.add(pSEdtCoarDel1, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE,
        new Insets(0, 5, 10, 0), 0, 0));
    jPanel38.add(pSEdtDskw2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE,
        new Insets(0, 5, 10, 0), 0, 0));
    jPanel38.add(jBtCntrlReg, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE,
        new Insets(0, 5, 5, 0), 0, 0));
    jPanel38.add(jLabel22, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
                                                  , GridBagConstraints.WEST,
                                                  GridBagConstraints.NONE,
                                                  new Insets(0, 5, 5, 0), 0, 0));
    jPanel38.add(pSEdtDskw1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE,
        new Insets(0, 5, 10, 0), 0, 0));
    jPanel38.add(jLabel26, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
                                                  , GridBagConstraints.WEST,
                                                  GridBagConstraints.NONE,
                                                  new Insets(0, 5, 5, 0), 0, 0));
    jPanel14.add(jPanel15, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
                                                  , GridBagConstraints.CENTER,
                                                  GridBagConstraints.BOTH,
                                                  new Insets(0, 5, 5, 5), 0, 0));
    jPanel15.add(jLabel111, null);
    jPanel15.add(jCbBxRxs, null);
    jPanel15.add(jBtAddRx, null);
    jPanel15.add(jBtDeleteRx, null);
    jPanel21.add(jPanel13,  new GridBagConstraints(2, 1, 1, 2, 0.0, 0.0
            ,GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
    jPanel13.add(jLabel20, null);
    jPanel13.add(jPanel36, null);
    jPanel36.add(jLabel19, null);
    jPanel36.add(jCbBxRxTR, null);
    jPanel13.add(jChBxExtSub, null);
    jPanel13.add(jChBxEnableATT, null);
    jPanel13.add(jPanel37, null);
    jPanel37.add(jLabel110, null);
    jPanel37.add(jTxtFldSub, null);
    jPanel37.add(jBtSetTrigReg, null);
    jPanel21.add(jPanel5, new GridBagConstraints(0, 2, 1, 4, 0.0, 0.0
                                                 , GridBagConstraints.NORTHWEST,
                                                 GridBagConstraints.NONE,
                                                 new Insets(0, 0, 0, 5), 0, 0));
    pSEdtInhDel2.addAdjustmentListener(new java.awt.event.AdjustmentListener() {
      public void adjustmentValueChanged(AdjustmentEvent e) {
        pSEdtInhDel2_adjustmentValueChanged(e);
      }
    });
    pSEdtInhDel2.setMax(TTCvi.MAX_INHIBIT_DELAY);
    pSEdtInhDur2.addAdjustmentListener(new java.awt.event.AdjustmentListener() {
      public void adjustmentValueChanged(AdjustmentEvent e) {
        pSEdtInhDur2_adjustmentValueChanged(e);
      }
    });
    pSEdtInhDur2.setMax(TTCvi.MAX_INHIBIT_DURATION);
    pSEdtInhDel3.addAdjustmentListener(new java.awt.event.AdjustmentListener() {
      public void adjustmentValueChanged(AdjustmentEvent e) {
        pSEdtInhDel3_adjustmentValueChanged(e);
      }
    });
    pSEdtInhDel3.setMax(TTCvi.MAX_INHIBIT_DELAY);
    pSEdtInhDur3.addAdjustmentListener(new java.awt.event.AdjustmentListener() {
      public void adjustmentValueChanged(AdjustmentEvent e) {
        pSEdtInhDur3_adjustmentValueChanged(e);
      }
    });
    pSEdtInhDur3.setMax(TTCvi.MAX_INHIBIT_DURATION);
    pSEdtInhDel0.setMax(TTCvi.MAX_INHIBIT_DELAY);
    pSEdtInhDel0.addAdjustmentListener(new java.awt.event.AdjustmentListener() {
      public void adjustmentValueChanged(AdjustmentEvent e) {
        pSEdtInhDel0_adjustmentValueChanged(e);
      }
    });
    pSEdtInhDur0.addAdjustmentListener(new java.awt.event.AdjustmentListener() {
      public void adjustmentValueChanged(AdjustmentEvent e) {
        pSEdtInhDur0_adjustmentValueChanged(e);
      }
    });
    pSEdtInhDur0.setMax(TTCvi.MAX_INHIBIT_DURATION);
    pSEdtInhDel1.addAdjustmentListener(new java.awt.event.AdjustmentListener() {
      public void adjustmentValueChanged(AdjustmentEvent e) {
        pSEdtInhDel1_adjustmentValueChanged(e);
      }
    });
    pSEdtInhDel1.setMax(TTCvi.MAX_INHIBIT_DELAY);
    pSEdtInhDur1.addAdjustmentListener(new java.awt.event.AdjustmentListener() {
      public void adjustmentValueChanged(AdjustmentEvent e) {
        pSEdtInhDur1_adjustmentValueChanged(e);
      }
    });
    pSEdtInhDur1.setMax(TTCvi.MAX_INHIBIT_DURATION);
    buttonGroupBGo.add(jTBtBGo0);
    buttonGroupBGo.add(jTBtBGo1);
    buttonGroupBGo.add(jTBtBGo2);
    buttonGroupBGo.add(jTBtBGo3);
    buttonGroupTrgSrs.add(jRBtTrgSrc0);
    buttonGroupTrgSrs.add(jRBtTrgSrc1);
    buttonGroupTrgSrs.add(jRBtTrgSrc2);
    buttonGroupTrgSrs.add(jRBtTrgSrc3);
    buttonGroupTrgSrs.add(jRBtTrgSrc4);
    buttonGroupTrgSrs.add(jRBtTrgSrc5);
    buttonGroupTrgSrs.add(jRBtTrgSrc6);
    contentPane.add(jScrollPane1,     new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    jScrollPane1.getViewport().add(jPanel21, null);
    pSEdtDskw1.addAdjustmentListener(new java.awt.event.AdjustmentListener() {
      public void adjustmentValueChanged(AdjustmentEvent e) {
        pSEdtDskw1_adjustmentValueChanged(e);
      }
    });
    pSEdtDskw1.setMax(TTCrx.MAX_DESKEW);
    pSEdtCoarDel2.addAdjustmentListener(new java.awt.event.AdjustmentListener() {
      public void adjustmentValueChanged(AdjustmentEvent e) {
        pSEdtCoarDel2_adjustmentValueChanged(e);
      }
    });
    pSEdtCoarDel2.setMax(TTCrx.MAX_COAR_DELAY);
    pSEdtCoarDel1.addAdjustmentListener(new java.awt.event.AdjustmentListener() {
      public void adjustmentValueChanged(AdjustmentEvent e) {
        pSEdtCoarDel1_adjustmentValueChanged(e);
      }
    });
    pSEdtCoarDel1.setMax(TTCrx.MAX_COAR_DELAY);
    pSEdtDskw2.addAdjustmentListener(new java.awt.event.AdjustmentListener() {
      public void adjustmentValueChanged(AdjustmentEvent e) {
        pSEdtDskw2_adjustmentValueChanged(e);
      }
    });
    pSEdtDskw2.setMax(TTCrx.MAX_DESKEW);

    if ("MkI".equals(ttcManager.GetTTCviType())) {
      jCbBxRxTR.setEnabled(false);
      jChBxExtSub.setEnabled(false);
      jChBxEnableATT.setEnabled(false);
      jTxtFldSub.setEnabled(false);
      jBtSetTrigReg.setEnabled(false);
      jRBtTrgSrc6.setEnabled(false);
      jBtSetTrigReg.setEnabled(false);
    }
  } //end jInit

  //Overridden so we can exit when window is closed
  protected void processWindowEvent(WindowEvent e) {
    super.processWindowEvent(e);
    if (e.getID() == WindowEvent.WINDOW_CLOSING) {
      System.exit(0);
    }
  }

  private void showErrorMessage(String message) {
    JOptionPane.showInternalMessageDialog(contentPane,
                                          message,
                                          "Error",
                                          JOptionPane.ERROR_MESSAGE);
  }

  void jTBtBGo0_actionPerformed(ActionEvent e) {
    bGoSelectedChanged();
  }

  void jTBtBGo1_actionPerformed(ActionEvent e) {
    bGoSelectedChanged();
  }

  void jTBtBGo2_actionPerformed(ActionEvent e) {
    bGoSelectedChanged();
  }

  void jTBtBGo3_actionPerformed(ActionEvent e) {
    bGoSelectedChanged();
  }

  void jBtFIFOStatus_actionPerformed(ActionEvent e) {
    try {
      int bGoNo = bGoSelected();
      JOptionPane.showInternalMessageDialog(
          contentPane,
          "B-Go FIFO[" + bGoNo + "] " +
          ttcManager.getTTCvi().getFIFOStatus(bGoNo),
          "FIFO Status",
          JOptionPane.INFORMATION_MESSAGE);
    }
    catch(XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }
  }

  int bGoSelected() {
    if (jTBtBGo0.isSelected()) {
      return 0;
    }
    if (jTBtBGo1.isSelected()) {
      return 1;
    }
    if (jTBtBGo2.isSelected()) {
      return 2;
    }
    if (jTBtBGo3.isSelected()) {
      return 3;
    }
    return 0;
  }

  void bGoSelectedChanged() {
    int bGoSelected = bGoSelected();
    jTBtBGo0.setBorder(BorderFactory.createLoweredBevelBorder());
    jTBtBGo1.setBorder(BorderFactory.createLoweredBevelBorder());
    jTBtBGo2.setBorder(BorderFactory.createLoweredBevelBorder());
    jTBtBGo3.setBorder(BorderFactory.createLoweredBevelBorder());
    jChBCalibration.setEnabled(false);
    if (bGoSelected == 0) {
      jTBtBGo0.setBorder(border1);
    }
    else if (bGoSelected == 1) {
      jTBtBGo1.setBorder(border1);
    }
    else if (bGoSelected == 2) {
      jTBtBGo2.setBorder(border1);
      if ("MkII".equals(ttcManager.GetTTCviType()))
        jChBCalibration.setEnabled(true);
    }
    else if (bGoSelected == 3) {
      jTBtBGo3.setBorder(border1);

    }
    jChBEnable.setSelected(ttcManager.getTTCvi().getBGo(bGoSelected).getMode()[
                           0]);
    jChBAsynchron.setSelected(ttcManager.getTTCvi().getBGo(bGoSelected).getMode()[
                              1]);
    jChBRepetetive.setSelected(ttcManager.getTTCvi().getBGo(bGoSelected).
                               getMode()[2]);
    jChBFIFO.setSelected(ttcManager.getTTCvi().getBGo(bGoSelected).getMode()[3]);
    jChBCalibration.setSelected(ttcManager.getTTCvi().getBGo(bGoSelected).
                                getMode()[4]);
    jChBRetrensmit.setSelected(ttcManager.getTTCvi().getRetransmiteBGoFIFO(
        bGoSelected));
  }

  //trigger soucre
  int trgSrcSelected() {
    if (jRBtTrgSrc0.isSelected()) {
      return 0;
    }
    if (jRBtTrgSrc1.isSelected()) {
      return 1;
    }
    if (jRBtTrgSrc2.isSelected()) {
      return 2;
    }
    if (jRBtTrgSrc3.isSelected()) {
      return 3;
    }
    if (jRBtTrgSrc4.isSelected()) {
      return 4;
    }
    if (jRBtTrgSrc5.isSelected()) {
      return 5;
    }
    if (jRBtTrgSrc6.isSelected()) {
      return 6;
    }
    return 0;
  }

  void trgSrcChanged() {
    try {
      int trgSrc = trgSrcSelected();
      if (trgSrc == 4) {
        jBtTrigger.setEnabled(true);
        //jCbBxTrgRate.setEnabled(false);
      }
      else if (trgSrc == 5) {
        //jCbBxTrgRate.setEnabled(true);
        jBtTrigger.setEnabled(false);
      }
      else {
        jBtTrigger.setEnabled(false);
        //jCbBxTrgRate.setEnabled(false);
      }
      ttcManager.getTTCvi().setL1ATriggerSource(trgSrc);
    }
    catch (XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }
  }

  void setTriggerSourceRbts(short trgSrc) {
    if (trgSrc == 0) {
      jRBtTrgSrc0.setSelected(true);
    }
    else if (trgSrc == 1) {
      jRBtTrgSrc1.setSelected(true);
    }
    else if (trgSrc == 2) {
      jRBtTrgSrc2.setSelected(true);
    }
    else if (trgSrc == 3) {
      jRBtTrgSrc3.setSelected(true);
    }
    else if (trgSrc == 6) {
      jRBtTrgSrc6.setSelected(true);

    }
    if (trgSrc == 4) {
      jRBtTrgSrc4.setSelected(true);
      jBtTrigger.setEnabled(true);
      //jCbBxTrgRate.setEnabled(false);
    }
    else if (trgSrc == 5) {
      jRBtTrgSrc3.setSelected(true);
      //jCbBxTrgRate.setEnabled(true);
      jBtTrigger.setEnabled(false);
    }
    else {
      jBtTrigger.setEnabled(false);
      //jCbBxTrgRate.setEnabled(false);
    }
  }

  void jRBtTrgSrc0_actionPerformed(ActionEvent e) {
    trgSrcChanged();
  }

  void jRBtTrgSrc1_actionPerformed(ActionEvent e) {
    trgSrcChanged();
  }

  void jRBtTrgSrc2_actionPerformed(ActionEvent e) {
    trgSrcChanged();
  }

  void jRBtTrgSrc3_actionPerformed(ActionEvent e) {
    trgSrcChanged();
  }

  void jRBtTrgSrc4_actionPerformed(ActionEvent e) {
    trgSrcChanged();
  }

  void jRBtTrgSrc5_actionPerformed(ActionEvent e) {
    trgSrcChanged();
  }

  void jRBtTrgSrc6_actionPerformed(ActionEvent e) {
    trgSrcChanged();
  }

  void jBtTrigger_actionPerformed(ActionEvent e) {
    try {
      ttcManager.getTTCvi().sendL1ASignal();
    }
    catch (XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }
  }

  void jCbBxTrgRate_actionPerformed(ActionEvent e) {
    try {
      int ranTrigRate = jCbBxTrgRate.getSelectedIndex();
      ttcManager.getTTCvi().setRanTrigRate(ranTrigRate);
    }
    catch (XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }

  }

  //end trigger source

  void jLstBGoData_mouseClicked(MouseEvent e) {
    if ( (e.getClickCount() == 2) && (ttcManager.getTTCrxsVec().size() != 0)) {
      dlgBGoData.bGoData = (BGoData) jLstBGoData.getSelectedValue();
      //we will work on a copy
      dlgBGoData.bGoData = dlgBGoData.bGoData.duplicate();

      dlgBGoData.show();

      if (dlgBGoData.getResult() == DlgBGoData.OK_OPTION) {
        int selectedIndex = jLstBGoData.getSelectedIndex();
        ttcManager.setBGoData(dlgBGoData.bGoData, selectedIndex);
        //jLstBGoData.setListData(ttcManager.getBGoDataVec());
      }
    }
  }

  void jChBOrbitSignal_actionPerformed(ActionEvent e) {
    try {
      ttcManager.getTTCvi().setOrbitSignalSel(jChBOrbitSignal.isSelected());
    }
    catch (XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }

  }

  void jBtL1AFIFOReset_actionPerformed(ActionEvent e) {
    try {
      ttcManager.getTTCvi().resetL1AFIFO();
    }
    catch (XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }

  }

  void jBtReadCSR1_actionPerformed(ActionEvent e) {
    try {
      int csr1 = ttcManager.getTTCvi().readCSR1();
      String str = MyUtil.boolTabToString(MyUtil.intToBoolTab(csr1, 16));
      jTxtFldCSR1.setText(str);
    }
    catch (XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }

  }

  void jBtReadCSR2_actionPerformed(ActionEvent e) {
    try {
      int csr2 = ttcManager.getTTCvi().readCSR2();
      String str = MyUtil.boolTabToString(MyUtil.intToBoolTab(csr2, 16));
      jTxtFldCSR2.setText(str);
    }
    catch(XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }

  }

  void jBtResetBGoFIFOs_actionPerformed(ActionEvent e) {
    try {
      boolean[] b = new boolean[4];
      for (int i = 0; i < 4; i++) {
        b[i] = true;
      }
      ttcManager.getTTCvi().resetBGoFIFO(b);
    }
    catch(XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }

  }

  void jChBEnable_actionPerformed(ActionEvent e) {
    try {
      ttcManager.getTTCvi().getBGo(bGoSelected()).setModeEnable(jChBEnable.
          isSelected());
    }
    catch(XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }
  }

  void jChBAsynchron_actionPerformed(ActionEvent e) {
    try {
      ttcManager.getTTCvi().getBGo(bGoSelected()).setModeSync(jChBAsynchron.
          isSelected());
    }
    catch(XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }

  }

  void jChBRepetetive_actionPerformed(ActionEvent e) {
    try {
      ttcManager.getTTCvi().getBGo(bGoSelected()).setModeSingle(jChBRepetetive.
          isSelected());
    }
    catch(XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }

  }

  void jChBFIFO_actionPerformed(ActionEvent e) {
    try {
      ttcManager.getTTCvi().getBGo(bGoSelected()).setModeFifo(jChBFIFO.
          isSelected());
    }
    catch(XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }

  }

  void jChBRetrensmit_actionPerformed(ActionEvent e) {
    try {
      ttcManager.getTTCvi().retransmiteBGoFIFO(bGoSelected(),
                                               jChBRetrensmit.isSelected());
    }
    catch(XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }
  }

  void jBtReadBGoSettings_actionPerformed(ActionEvent e) {
    try {
      String mode = MyUtil.boolTabToString(ttcManager.getTTCvi().getBGo(
          bGoSelected()).readMode());
      jTxtFldBGoSettings.setText(mode);
    }
    catch(XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }

  }

  void jBtResetFIFO_actionPerformed(ActionEvent e) {
    try {
      ttcManager.getTTCvi().resetBGoFIFO(bGoSelected());
    }
    catch(XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }
  }

  void jBtBGoSignal_actionPerformed(ActionEvent e) {
    try {
      ttcManager.getTTCvi().getBGo(bGoSelected()).bGoSignal();
    }
    catch(XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }

  }

  void jBtLoadFIFO_actionPerformed(ActionEvent e) {
    if (jLstBGoData.getSelectedIndex() == -1)
      showErrorMessage("No BGo data selected");
    else if(jLstBGoData.getModel().getSize() == 0)
      showErrorMessage("There is no BGo data");
    else
      ttcManager.loadBGoData(bGoSelected(), jLstBGoData.getSelectedIndex());
  }

  void jBtAddBGoData_actionPerformed(ActionEvent e) {
    dlgBGoData.bGoData = new BGoData("data");
    dlgBGoData.show();
    if (dlgBGoData.getResult() == DlgBGoData.OK_OPTION) {
      ttcManager.addBGoData(dlgBGoData.bGoData);
      jLstBGoData.setListData(ttcManager.getBGoDataVec());
      jLstBGoData.setSelectedIndex(0);
    }
  }

  void jBtDelete_actionPerformed(ActionEvent e) {
    ttcManager.removeBGoData(jLstBGoData.getSelectedIndex());
    jLstBGoData.setListData(ttcManager.getBGoDataVec());
    jLstBGoData.setSelectedIndex(0);
  }

  void jBtSendLongCycle_actionPerformed(ActionEvent e) {
    short rxAddress = 0; /** @todo why dosn't work??? */

    if (jCbBxRx.getSelectedItem().getClass().getName() == "java.lang.String") {
      try {
        rxAddress = Short.parseShort( (String) jCbBxRx.getSelectedItem(), 16);
      }
      catch (NumberFormatException ex) {
        JOptionPane.showInternalMessageDialog(contentPane,
                                              "Write Rx address in hex format",
                                              "NumberFormatException",
                                              JOptionPane.ERROR_MESSAGE);
      }
    }
    else {
      rxAddress = ( (TTCrx) jCbBxRx.getSelectedItem()).getAddress();

    }

    try {

      if (jChBxExternal.isSelected()) {
        ttcManager.getTTCvi().writeExternal(
            rxAddress,
            Short.parseShort(jTxtFldSubAddress.getText(), 16),
            Short.parseShort(jTxtFldData.getText(), 16));
      }
      else {
        ttcManager.getTTCvi().writeRX(
            rxAddress,
            Short.parseShort(jTxtFldSubAddress.getText(), 16),
            Short.parseShort(jTxtFldData.getText(), 16));
      }
    }
    catch (XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }
  }

  void jBtSendShortCycle_actionPerformed(ActionEvent e) {
    try {
      ttcManager.getTTCvi().writeShortAsynchronous(
          Short.parseShort(jTxtFldCommand.getText(), 16));
    }
    catch (XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }
  }

  void jBtAddRx_actionPerformed(ActionEvent e) {
    dlgNewRx.show();
    if (dlgNewRx.getResult() == dlgNewRx.OK_OPTION) {
      ttcManager.addTTCrx(dlgNewRx.getRxName(), dlgNewRx.getAddress());
      jCbBxRxs.setSelectedIndex(jCbBxRxs.getItemCount() - 1);
    }
  }

  void jBtDeleteRx_actionPerformed(ActionEvent e) {
    if (jCbBxRxs.getModel().getSize() > 1) {
      ttcManager.removeTTCrx(jCbBxRxs.getSelectedIndex());
      jCbBxRxs.setSelectedIndex(0);
      jCbBxRx.setSelectedIndex(0);
      jCbBxRxTR.setSelectedIndex(0);
    }
  }

  void jBtCntrlReg_actionPerformed(ActionEvent e) {
    dlgRxControlRegister.setSettings(
        ( (TTCrx) jCbBxRxs.getSelectedItem()).getControlRegister());
    dlgRxControlRegister.show();
    if (dlgRxControlRegister.getResult() == dlgRxControlRegister.OK_OPTION) {
      try {
        ( (TTCrx) jCbBxRxs.getSelectedItem()).setControlRegister(
            dlgRxControlRegister.getSettings());
        jBtCntrlReg.setText(MyUtil.boolTabToString(
            dlgRxControlRegister.getSettings()));
      }
      catch (Exception ex) {
        showErrorMessage(ex.getMessage());
      }
    }
  }

  void pSEdtInhDel0_adjustmentValueChanged(AdjustmentEvent e) {
    try {
      ttcManager.getTTCvi().getInhibit(0).setDelay( (short) pSEdtInhDel0.
          getValue());
    }
    catch (XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }
  }

  void pSEdtInhDel1_adjustmentValueChanged(AdjustmentEvent e) {
    try {
      ttcManager.getTTCvi().getInhibit(1).setDelay( (short) pSEdtInhDel1.
          getValue());
    }
    catch (XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }

  }

  void pSEdtInhDel2_adjustmentValueChanged(AdjustmentEvent e) {
    try {
      ttcManager.getTTCvi().getInhibit(2).setDelay( (short) pSEdtInhDel2.
          getValue());
    }
    catch (XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }

  }

  void pSEdtInhDel3_adjustmentValueChanged(AdjustmentEvent e) {
    try {
      ttcManager.getTTCvi().getInhibit(3).setDelay( (short) pSEdtInhDel3.
          getValue());
    }
    catch (Exception ex) {
      showErrorMessage(ex.getMessage());
    }

  }

  void pSEdtInhDur0_adjustmentValueChanged(AdjustmentEvent e) {
    try {
      ttcManager.getTTCvi().getInhibit(0).setDuration(
          (short) pSEdtInhDur0.getValue());
    }
    catch (XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }

  }

  void pSEdtInhDur1_adjustmentValueChanged(AdjustmentEvent e) {
    try {
      ttcManager.getTTCvi().getInhibit(1).setDuration(
          (short) pSEdtInhDur1.getValue());
    }
    catch (XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }

  }

  void pSEdtInhDur2_adjustmentValueChanged(AdjustmentEvent e) {
    try {
      ttcManager.getTTCvi().getInhibit(2).setDuration(
          (short) pSEdtInhDur2.getValue());
    }
    catch (XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }

  }

  void pSEdtInhDur3_adjustmentValueChanged(AdjustmentEvent e) {
    try {
      ttcManager.getTTCvi().getInhibit(3).setDuration(
          (short) pSEdtInhDur3.getValue());
    }
    catch (XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }

  }

  void pSEdtDskw1_adjustmentValueChanged(AdjustmentEvent e) {
    try {
      ( (TTCrx) jCbBxRxs.getSelectedItem()).setDeskew1( (short) pSEdtDskw1.
          getValue());
    }
    catch (Exception ex) {
      JOptionPane.showInternalMessageDialog(contentPane,
                                            ex.getMessage(),
                                            "Error",
                                            JOptionPane.ERROR_MESSAGE);
    }
  }

  void pSEdtDskw2_adjustmentValueChanged(AdjustmentEvent e) {
    try {
      ( (TTCrx) jCbBxRxs.getSelectedItem()).setDeskew2( (short) pSEdtDskw2.
          getValue());
    }
    catch (Exception ex) {
      JOptionPane.showInternalMessageDialog(contentPane,
                                            ex.getMessage(),
                                            "Error",
                                            JOptionPane.ERROR_MESSAGE);
    }
  }

  void pSEdtCoarDel1_adjustmentValueChanged(AdjustmentEvent e) {
    try {
      ( (TTCrx) jCbBxRxs.getSelectedItem()).setCoarDelay1( (short)
          pSEdtCoarDel1.getValue());
    }
    catch (Exception ex) {
      JOptionPane.showInternalMessageDialog(contentPane,
                                            ex.getMessage(),
                                            "Error",
                                            JOptionPane.ERROR_MESSAGE);
    }
  }

  void pSEdtCoarDel2_adjustmentValueChanged(AdjustmentEvent e) {
    try {
      ( (TTCrx) jCbBxRxs.getSelectedItem()).setCoarDelay2( (short)
          pSEdtCoarDel2.getValue());
    }
    catch (Exception ex) {
      JOptionPane.showInternalMessageDialog(contentPane,
                                            ex.getMessage(),
                                            "Error",
                                            JOptionPane.ERROR_MESSAGE);
    }
  }

  void jCbBxRxs_actionPerformed(ActionEvent e) {
    TTCrx rxSelected = (TTCrx) jCbBxRxs.getSelectedItem();
    pSEdtDskw1.setValue(rxSelected.getDeskew1());
    pSEdtDskw2.setValue(rxSelected.getDeskew2());
    pSEdtCoarDel1.setValue(rxSelected.getCoarDelay1());
    pSEdtCoarDel2.setValue(rxSelected.getCoarDelay2());
    jBtCntrlReg.setText(MyUtil.boolTabToString(rxSelected.getControlRegister()));
  }

  void jChBxEnableATT_actionPerformed(ActionEvent e) {
    try {
      boolean aTTEnable = jChBxEnableATT.isSelected();
      jCbBxRxTR.setEnabled(aTTEnable);
      jChBxExtSub.setEnabled(aTTEnable);
      jTxtFldSub.setEnabled(aTTEnable);
      jBtSetTrigReg.setEnabled(aTTEnable);
      ttcManager.getTTCvi().setTRIGWORD(
          ( (TTCrx) jCbBxRxTR.getSelectedItem()).getAddress(),
          Short.parseShort(jTxtFldSub.getText(), 16),
          jChBxExtSub.isSelected(),
          aTTEnable);
    }
    catch (XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }
  }

  void jBtResetTTCvi_actionPerformed(ActionEvent e) {
    try {
      ttcManager.getTTCvi().reset();
    }
    catch (XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }
  }

  void jBtSetTrigReg_actionPerformed(ActionEvent e) {
    try {
      ttcManager.getTTCvi().setTRIGWORD(
          ( (TTCrx) jCbBxRxTR.getSelectedItem()).getAddress(),
          Short.parseShort(jTxtFldSub.getText(), 16),
          jChBxExtSub.isSelected(),
          true);
    }
    catch (XdaqAccessException ex) {
      showErrorMessage(ex.getMessage());
    }
  }

  void jBtApplayPreset_actionPerformed(ActionEvent e) {
    try {
      ttcManager.applyPreset(jCbBxPresets.getSelectedIndex());
    }
    catch (Exception ex) {
      JOptionPane.showInternalMessageDialog(contentPane, ex.getMessage() +
                                            ", TTCini.xml file may be corupted",
                                            "Exception during loading preset",
                                            JOptionPane.ERROR_MESSAGE);
      showErrorMessage(ex.getMessage());
      ex.printStackTrace();
    }

    //changeing interface
    setTriggerSourceRbts(ttcManager.getTTCvi().getL1ATriggerSource());
    jChBOrbitSignal.setSelected(ttcManager.getTTCvi().getOrbitSignalSel());
    pSEdtInhDel0.setValue(ttcManager.getTTCvi().getInhibit(0).getDelay());
    pSEdtInhDel1.setValue(ttcManager.getTTCvi().getInhibit(1).getDelay());
    pSEdtInhDel2.setValue(ttcManager.getTTCvi().getInhibit(2).getDelay());
    pSEdtInhDel3.setValue(ttcManager.getTTCvi().getInhibit(3).getDelay());
    pSEdtInhDur0.setValue(ttcManager.getTTCvi().getInhibit(0).getDuration());
    pSEdtInhDur1.setValue(ttcManager.getTTCvi().getInhibit(1).getDuration());
    pSEdtInhDur2.setValue(ttcManager.getTTCvi().getInhibit(2).getDuration());
    pSEdtInhDur3.setValue(ttcManager.getTTCvi().getInhibit(3).getDuration());
    bGoSelectedChanged();
    jLstBGoData.setListData(ttcManager.getBGoDataVec());
    jLstBGoData.setSelectedIndex(0);
    jCbBxRxs.setSelectedIndex(0);
    jCbBxRx.setSelectedIndex(0);
    jCbBxRxTR.setSelectedIndex(0);

    boolean aTTEnable = ttcManager.getTTCvi().getTRIGWORDsize();
    jChBxEnableATT.setSelected(aTTEnable);
    jCbBxRxTR.setEnabled(aTTEnable);
    jChBxExtSub.setEnabled(aTTEnable);
    jTxtFldSub.setEnabled(aTTEnable);
    jBtSetTrigReg.setEnabled(aTTEnable);
    if (aTTEnable) {
      try {
        jCbBxRxTR.setSelectedItem(ttcManager.getTTCrxByAddress(ttcManager.
            getTTCvi().getTRIGWORDad()));
      }
      catch (TTCManagerException ex) {
        ex.printStackTrace();
      }
      jChBxExtSub.setSelected(ttcManager.getTTCvi().getTRIGWORDie());
      jTxtFldSub.setText(Integer.toHexString(ttcManager.getTTCvi().
                                             getTRIGWORDsad()));

    }

  }

  void jBtAddPreset_actionPerformed(ActionEvent e) {
    int[] bGoDataNum = new int[4];
//    bGoDataNum[0] = 0; bGoDataNum[1] = -1; bGoDataNum[2] = -1; bGoDataNum[3] = -1;

    Vector bGoDataNames = new Vector();
    bGoDataNames.add("none");
    for (int i = 0; i < ttcManager.getBGoDataVec().size(); i++) {
      bGoDataNames.add(ttcManager.getBGoDataVec().get(i).toString());

    }
    dlgBGoDataForLoad.setData(bGoDataNames, bGoDataNum);
    dlgBGoDataForLoad.show();
    try {
      ttcManager.savePreset(jTxtFldNewPresetName.getText(), bGoDataNum);
      jCbBxPresets.setSelectedIndex(jCbBxPresets.getItemCount() - 1);
    }
    catch (Exception ex) {
      showErrorMessage(ex.getMessage());
    }
  }

  void jBtDeletePreset_actionPerformed(ActionEvent e) {
    try {
      if (jCbBxPresets.getModel().getSize() == 1) {
        showErrorMessage("Last preset can't be deleted");
      }
      else  {
        ttcManager.removePreset(jCbBxPresets.getSelectedIndex());
        jCbBxPresets.setSelectedIndex(jCbBxPresets.getItemCount() - 1);
      }
    }
    catch (Exception ex) {
      showErrorMessage(ex.getMessage());
      ex.printStackTrace();
    }
  }

  void jLstBGoData_mousePressed(MouseEvent e) {

  }

  void jLstBGoData_mouseReleased(MouseEvent e) {

  }

  void jLstBGoData_mouseEntered(MouseEvent e) {

  }

  void jLstBGoData_mouseExited(MouseEvent e) {

  }

  void jCbBxRx_actionPerformed(ActionEvent e) {

  }

  void jButton5_actionPerformed(ActionEvent e) {
    JOptionPane.showInternalMessageDialog(contentPane,"TTCControl v.1.0 \nAuthors:\n  Karol Bunkowski" +
    " kbunkow@fuw.edu.pl\n  Michal Pietrusinski mpietrus@fuw.edu.pl\n  Warsaw University","About",
                                      JOptionPane.INFORMATION_MESSAGE);

  }
  // jTextArea1.append(String.valueOf("dur 3"+pSEdtInhDur3.getValue())+"\n");
}