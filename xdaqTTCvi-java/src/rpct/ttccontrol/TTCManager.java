package rpct.ttccontrol;
import java.io.FileReader;
import java.util.Hashtable;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import rpct.castor.XmlBGo;
import rpct.castor.XmlBGoCyclesVec;
import rpct.castor.XmlBGoData;
import rpct.castor.XmlBGoDataVec;
import rpct.castor.XmlBGos;
import rpct.castor.XmlCycle;
import rpct.castor.XmlInhibit;
import rpct.castor.XmlInhibits;
import rpct.castor.XmlTTCPreset;
import rpct.castor.XmlTTCPresets;
import rpct.castor.XmlTTCini;
import rpct.castor.XmlTTCrx;
import rpct.castor.XmlTTCrxs;
/**
 * <p>Title: TTC Control</p>
 * <p>Description: Aplication that gives full control over TTCvi and connected to it TTCrx's</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Warsaw University</p>
 * @author Karol Bunkowski
 * @version 1.0
 */

class TTCManagerException extends Exception {
    TTCManagerException() {
        super();
    }
    
    TTCManagerException(String msg) {
        super("TTCManagerException: " + msg);
    }
}

public class TTCManager {
    private static Log LOG = LogFactory.getLog(TTCManager.class);
    // Fields
    private TTCvi ttcvi;
    private String TTCviType;
    private Vector ttcrxs = new Vector();
    private Hashtable presets = new Hashtable();
    private Vector bGoDataVec = new Vector(); //vector of BGoData
    private XmlTTCini xmlTTCini;
    String iniFileName = "TTCini.xml";
    Vector presetsNames = new Vector();
    
    // Constructor - creates ttcvi
    public TTCManager() {/*
    try {
    createDefoultIni();
    }
    catch (Exception ex) {
    ex.printStackTrace();
    }*/
        readIniFile();
    }
    
    public void init(String url, String targetAddr) {
        //ttcvi = new TTCvi(vme, Integer.parseInt(xmlTTCini.getXmlBoardAddress(), 16)); no-xdaq version
        ttcvi = new TTCvi();
        ttcvi.setXdaqParameters(url, targetAddr);
        
        try {
            //TTCviType = AccessParams.getInstance().getParam(url, targetAddr, "ttcviType", "string");
            TTCviType = XdaqAccess.getInstance().getParamValue("ttcviType");
        }
        catch(XdaqAccessException ex) {
            ex.printStackTrace();
        }
        addTTCrx("all", (short)0);
        
    }
    
    String getIniUrl() {
        return xmlTTCini.getXmlUrl();
    }
    
    String getIniTargetAddr() {
        return xmlTTCini.getXmlTargetAddr();
    }
    
    void saveIniXdaqParameters(String url, String targetAddr) throws TTCManagerException {
        xmlTTCini.setXmlUrl(url);
        xmlTTCini.setXmlTargetAddr(targetAddr);
        try {
            java.io.FileWriter file = new java.io.FileWriter(iniFileName);
            xmlTTCini.marshal(file);
            file.close();
        }
        catch (Exception ex) {
            throw new TTCManagerException("Error during saving IniXdaqParameters:" +  ex.getMessage());
        }
    }
    
    
    TTCvi getTTCvi() {
        return ttcvi;
    }
    
    String GetTTCviType() {
        return TTCviType;
    }
    
    
    void readIniFile() {
        try {
            LOG.info("unmarshalling \"" + iniFileName + "\"");
            //Unmarshal XML file.
            FileReader file = new FileReader(iniFileName);
            xmlTTCini = XmlTTCini.unmarshal(file);
            xmlTTCini.validate();
            file.close();
            LOG.info("unmarshalling \"" + iniFileName + "\" done");
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    Vector getPresets() {
        int presetCount = xmlTTCini.getXmlTTCPresets().getXmlTTCPresetCount();
        for (int i = 0; i < presetCount; i++) {
            presetsNames.add(xmlTTCini.getXmlTTCPresets().getXmlTTCPreset(i).getXmlPresetName());
        }
        return presetsNames;
    }
    
    void applyPreset(int presetNum) throws Exception {
        XmlTTCPreset xmlTTCPreset = xmlTTCini.getXmlTTCPresets().getXmlTTCPreset(presetNum);
        
        ttcvi.setCSR1(Short.parseShort(xmlTTCPreset.getXmlCSR1(), 2));
        ttcvi.setCSR2(Short.parseShort(xmlTTCPreset.getXmlCSR2(), 2));
        
        XmlInhibits xmlInhibits = xmlTTCPreset.getXmlInhibits();
        for(int i = 0; i < 4; i++) {
            ttcvi.getInhibit(i).setInhibit(xmlInhibits.getXmlInhibit(i).getXmlDelay(),
                    xmlInhibits.getXmlInhibit(i).getXmlDuration());
        }
        
        if ("MkII".equals(TTCviType)) {
            ttcvi.setTRIGWORD(Short.parseShort(xmlTTCPreset.getXmlTRIGWORDad(), 16),
                    Short.parseShort(xmlTTCPreset.getXmlTRIGWORDsad(), 16),
                    xmlTTCPreset.getXmlTRIGWORDie(),
                    xmlTTCPreset.getXmlTRIGWORDsize());
        }
        //XmlBGoDataVec
        bGoDataVec.clear();
        int bGoDataCount = xmlTTCPreset.getXmlBGoDataVec().getXmlBGoDataCount();
        for (int i = 0; i < bGoDataCount; i++) {
            XmlBGoData xmlBGoData = xmlTTCPreset.getXmlBGoDataVec().getXmlBGoData(i);
            BGoData bGoData = new BGoData(xmlBGoData.getXmlName());
            int cycleCount = xmlBGoData.getXmlBGoCyclesVec().getXmlCycleCount();
            for (int j = 0; j < cycleCount; j++) {
                XmlCycle xmlCycle = xmlBGoData.getXmlBGoCyclesVec().getXmlCycle(j);
                if (xmlCycle.getXmlType() == rpct.castor.types.XmlTypeType.LONG) {
                    LongTTCviCycle cycle =
                        new LongTTCviCycle(Short.parseShort(xmlCycle.getXmlRxAddress(), 16),
                                Short.parseShort(xmlCycle.getXmlSubAddress(), 16),
                                Boolean.valueOf(xmlCycle.getXmlE()).booleanValue(),
                                Short.parseShort(xmlCycle.getXmlData(), 16));
                    bGoData.addCycle(cycle);
                }
                else if (xmlCycle.getXmlType() == rpct.castor.types.XmlTypeType.SHORT) {
                    ShortTTCviCycle cycle = new ShortTTCviCycle(Short.parseShort(xmlCycle.getXmlData(), 16));
                    bGoData.addCycle(cycle);
                }
                else throw new TTCManagerException("XmlType in XmlCycle number "+ Integer.toString(i)+" is not Long or Short");
            }
            addBGoData(bGoData);
        }
        //XmlBGos
        XmlBGos xmlBGos = xmlTTCPreset.getXmlBGos();
        for (int i = 0; i < 4; i++) {
            ttcvi.getBGo(i).setMode(MyUtil.stringToBoolTab(xmlBGos.getXmlBGo(i).getXmlSettings()));
            int dataNum = Short.parseShort(xmlBGos.getXmlBGo(i).getXmlDataNum());
            if (dataNum != -1)
                loadBGoData(i, dataNum);
        }
        
        //TTCrx
        ttcrxs.clear();
        XmlTTCrxs xmlTTCrxs = xmlTTCPreset.getXmlTTCrxs();
        int TTCrxCount = xmlTTCrxs.getXmlTTCrxCount();
        for (int i = 0; i < TTCrxCount; i++) {
            XmlTTCrx xmlTTCrx = xmlTTCrxs.getXmlTTCrx(i);
            TTCrx rx = new TTCrx(xmlTTCrx.getXmlRxName(), Short.parseShort(xmlTTCrx.getXmlAddress(), 16), ttcvi);
            rx.setControlRegister(MyUtil.stringToBoolTab(xmlTTCrx.getXmlControlRegister()) );
            rx.setDeskew1(Short.parseShort(xmlTTCrx.getXmlDeskew1(), 16) );
            rx.setDeskew2(Short.parseShort(xmlTTCrx.getXmlDeskew2(), 16) );
            rx.setCoarDelay1(Short.parseShort(xmlTTCrx.getXmlCoarDelay1(), 16));
            rx.setCoarDelay1(Short.parseShort(xmlTTCrx.getXmlCoarDelay1(), 16));
            addTTCrx(rx);
        }
    }
    
    void createDefoultIni() throws TTCManagerException {
        xmlTTCini = new XmlTTCini();
        
        //xmlTTCini.setXmlBoardAddress("00000");
        xmlTTCini.setXmlUrl("http");
        xmlTTCini.setXmlTargetAddr("0");
        
        XmlTTCPreset xmlTTCPreset = new XmlTTCPreset();
        
        xmlTTCPreset.setXmlPresetName("defoult");
        
        xmlTTCPreset.setXmlCSR1("0000000000000100");
        xmlTTCPreset.setXmlCSR2("0000000000000000");
        
        XmlInhibits xmlInhibits = new XmlInhibits();
        for (int i = 0; i < 4; i++)  {
            XmlInhibit xmlInhibit = new XmlInhibit();
            xmlInhibit.setXmlDelay((short)0);
            xmlInhibit.setXmlDuration((short)0);
            xmlInhibits.addXmlInhibit(xmlInhibit);
        }
        xmlTTCPreset.setXmlInhibits(xmlInhibits);
        
        xmlTTCPreset.setXmlTRIGWORDad(Integer.toHexString(0) );
        xmlTTCPreset.setXmlTRIGWORDsad(Integer.toHexString(0) );
        xmlTTCPreset.setXmlTRIGWORDie(false);
        xmlTTCPreset.setXmlTRIGWORDsize(false);
        
        XmlBGoDataVec xmlBGoDataVec = new XmlBGoDataVec();
        //for (int i = 0; i < bGoDataVec.size(); i++)
        {
            XmlBGoData xmlBGoData = new XmlBGoData();
            xmlBGoData.setXmlName("foo");
            XmlBGoCyclesVec xmlBGoCyclesVec = new XmlBGoCyclesVec();
            //for (int j = 0; j < bGoCycles.size(); j++)
            {
                XmlCycle xmlCycle = new XmlCycle();
                //if (bGoCycles.get(j) instanceof rpct.ttccontrol.ShortTTCviCycle)
                {
                    xmlCycle.setXmlType(rpct.castor.types.XmlTypeType.SHORT);
                    xmlCycle.setXmlRxAddress("0");
                    xmlCycle.setXmlSubAddress("0");
                    xmlCycle.setXmlE("false");
                    xmlCycle.setXmlData(Integer.toHexString(0));
                }
                xmlBGoCyclesVec.addXmlCycle(xmlCycle);
                xmlBGoData.setXmlBGoCyclesVec(xmlBGoCyclesVec);
            }
            xmlBGoDataVec.addXmlBGoData(xmlBGoData);
        }
        xmlTTCPreset.setXmlBGoDataVec(xmlBGoDataVec);
        
        //XmlBGos
        XmlBGos xmlBGos = new XmlBGos();
        for (int i = 0; i < 4; i++) {
            XmlBGo xmlBGo = new XmlBGo();
            //MyUtil.boolTabToString(ttcvi.getBGo(i).getMode())
            xmlBGo.setXmlSettings("00000");
            xmlBGo.setXmlDataNum("0");
            xmlBGos.addXmlBGo(xmlBGo);
        }
        xmlTTCPreset.setXmlBGos(xmlBGos);
        
        XmlTTCrxs xmlTTCrxs = new XmlTTCrxs();
        //for (int i = 0; i < ttcrxs.size(); i++)
        {
            XmlTTCrx xmlTTCrx = new XmlTTCrx();
            xmlTTCrx.setXmlRxName("foo");
            xmlTTCrx.setXmlAddress("0");
            xmlTTCrx.setXmlControlRegister("00000000");
            xmlTTCrx.setXmlDeskew1("0");
            xmlTTCrx.setXmlDeskew2("0");
            xmlTTCrx.setXmlCoarDelay1("0");
            xmlTTCrx.setXmlCoarDelay2("0");
            xmlTTCrxs.addXmlTTCrx(xmlTTCrx);
        }
        xmlTTCPreset.setXmlTTCrxs(xmlTTCrxs);
        
        XmlTTCPresets xmlTTCPresets = new XmlTTCPresets();
        xmlTTCPresets.addXmlTTCPreset(xmlTTCPreset);
        xmlTTCini.setXmlTTCPresets(xmlTTCPresets);
        try {
            java.io.FileWriter file = new java.io.FileWriter(iniFileName);
            xmlTTCini.marshal(file);
            file.close();
            presetsNames.add("defoult");
        }
        catch (Exception ex) {
            throw new TTCManagerException("Error during writing the preset:" +  ex.getMessage());
        }
    }
    
    
    void savePreset(String presetName, int[] bGoDataNum) throws TTCManagerException {
        if(bGoDataVec.size() == 0) {
            throw new TTCManagerException("To save preset at list one BGo Data should exist");
        }
        if (presetsNames.contains(presetName)) {
            throw new TTCManagerException("The preset with name " + presetName +
            " already exist. Choose other name");
        }
        XmlTTCPreset xmlTTCPreset = new XmlTTCPreset();
        
        xmlTTCPreset.setXmlPresetName(presetName);
        
        xmlTTCPreset.setXmlCSR1(ttcvi.getCSR1String());
        xmlTTCPreset.setXmlCSR2(ttcvi.getCSR2String());
        
        XmlInhibits xmlInhibits = new XmlInhibits();
        for (int i = 0; i < 4; i++)  {
            XmlInhibit xmlInhibit = new XmlInhibit();
            xmlInhibit.setXmlDelay(ttcvi.getInhibit(i).getDelay());
            xmlInhibit.setXmlDuration(ttcvi.getInhibit(i).getDuration());
            xmlInhibits.addXmlInhibit(xmlInhibit);
        }
        xmlTTCPreset.setXmlInhibits(xmlInhibits);
        
        xmlTTCPreset.setXmlTRIGWORDad(Integer.toHexString(ttcvi.getTRIGWORDad()) );
        xmlTTCPreset.setXmlTRIGWORDsad(Integer.toHexString(ttcvi.getTRIGWORDsad()) );
        xmlTTCPreset.setXmlTRIGWORDie(ttcvi.getTRIGWORDie());
        xmlTTCPreset.setXmlTRIGWORDsize(ttcvi.getTRIGWORDsize());
        
        XmlBGoDataVec xmlBGoDataVec = new XmlBGoDataVec();
        for (int i = 0; i < bGoDataVec.size(); i++) {
            XmlBGoData xmlBGoData = new XmlBGoData();
            BGoData bGoData = (BGoData)bGoDataVec.get(i);
            xmlBGoData.setXmlName(bGoData.getName());
            XmlBGoCyclesVec xmlBGoCyclesVec = new XmlBGoCyclesVec();
            Vector bGoCycles = bGoData.getBGoCyclesVec();
            for (int j = 0; j < bGoCycles.size(); j++) {
                XmlCycle xmlCycle = new XmlCycle();
                if (bGoCycles.get(j) instanceof rpct.ttccontrol.ShortTTCviCycle) {
                    xmlCycle.setXmlType(rpct.castor.types.XmlTypeType.SHORT);
                    xmlCycle.setXmlRxAddress("0");
                    xmlCycle.setXmlSubAddress("0");
                    xmlCycle.setXmlE("false");
                    xmlCycle.setXmlData(Integer.toHexString( ((ShortTTCviCycle)bGoCycles.get(j)).getCommand() ));
                }
                if (bGoCycles.get(j) instanceof rpct.ttccontrol.LongTTCviCycle) {
                    xmlCycle.setXmlType(rpct.castor.types.XmlTypeType.LONG);
                    LongTTCviCycle longTTCviCycle = (LongTTCviCycle)bGoCycles.get(j);
                    xmlCycle.setXmlRxAddress(Integer.toHexString(longTTCviCycle.getRxAddress()));
                    xmlCycle.setXmlE(String.valueOf(longTTCviCycle.getE()));
                    xmlCycle.setXmlSubAddress(Integer.toHexString( longTTCviCycle.getSubAddress()));
                    xmlCycle.setXmlData(Integer.toHexString(longTTCviCycle.getData()));
                }
                xmlBGoCyclesVec.addXmlCycle(xmlCycle);
                xmlBGoData.setXmlBGoCyclesVec(xmlBGoCyclesVec);
            }
            xmlBGoDataVec.addXmlBGoData(xmlBGoData);
        }
        xmlTTCPreset.setXmlBGoDataVec(xmlBGoDataVec);
        
        //XmlBGos
        XmlBGos xmlBGos = new XmlBGos();
        for (int i = 0; i < 4; i++) {
            XmlBGo xmlBGo = new XmlBGo();
            xmlBGo.setXmlSettings(MyUtil.boolTabToString(ttcvi.getBGo(i).getMode()));
            xmlBGo.setXmlDataNum(Integer.toString(bGoDataNum[i]));
            xmlBGos.addXmlBGo(xmlBGo);
        }
        xmlTTCPreset.setXmlBGos(xmlBGos);
        
        XmlTTCrxs xmlTTCrxs = new XmlTTCrxs();
        for (int i = 0; i < ttcrxs.size(); i++) {
            XmlTTCrx xmlTTCrx = new XmlTTCrx();
            TTCrx ttcrx = (TTCrx)ttcrxs.get(i);
            xmlTTCrx.setXmlRxName(ttcrx.getName());
            xmlTTCrx.setXmlAddress(Integer.toHexString(ttcrx.getAddress()));
            xmlTTCrx.setXmlControlRegister(MyUtil.boolTabToString(ttcrx.getControlRegister()));
            xmlTTCrx.setXmlDeskew1(Integer.toHexString(ttcrx.getDeskew1()));
            xmlTTCrx.setXmlDeskew2(Integer.toHexString(ttcrx.getDeskew2()));
            xmlTTCrx.setXmlCoarDelay1(Integer.toHexString(ttcrx.getCoarDelay1()));
            xmlTTCrx.setXmlCoarDelay2(Integer.toHexString(ttcrx.getCoarDelay2()));
            xmlTTCrxs.addXmlTTCrx(xmlTTCrx);
        }
        xmlTTCPreset.setXmlTTCrxs(xmlTTCrxs);
        
        xmlTTCini.getXmlTTCPresets().addXmlTTCPreset(xmlTTCPreset);
        try {
            java.io.FileWriter file = new java.io.FileWriter(iniFileName);
            xmlTTCini.marshal(file);
            file.close();
            presetsNames.add(presetName);
        }
        catch (Exception ex) {
            throw new TTCManagerException("Error during writing the preset:" +  ex.getMessage());
        }
    }
    
    void removePreset(int presetNum) throws TTCManagerException {
        xmlTTCini.getXmlTTCPresets().removeXmlTTCPreset(presetNum);
        try {
            java.io.FileWriter file = new java.io.FileWriter(iniFileName);
            xmlTTCini.marshal(file);
            file.close();
            presetsNames.remove(presetNum);
        }
        catch (Exception ex) {
            throw new TTCManagerException("Error during removing the preset:" +  ex.getMessage());
        }
    }
    
    void addTTCrx(TTCrx rx) throws TTCManagerException {
        // if (ttcrxs.put(rx.name, rx) != null)
        //  throw new TTCManagerException("TTCrx with name " + rx.name + " already exist");
        ttcrxs.add(rx);
    }
    
    void addTTCrx(String rxName, short rxAddr) { ///** @todo  */ throws TTCManagerException
        /*  for (int i =0; ttcrxs.size(); i++)
         if((TTCrx)(ttcrxs.get(i)).name == rxName)
         throw new TTCManagerException("TTCrx with name " + rx.name + " already exist");*/
        /** @todo think if it is necesery */
        TTCrx rx = new TTCrx(rxName, rxAddr, ttcvi);
        ttcrxs.add(rx);
    }
    
    /*void removeTTCrx(String rxName) throws TTCManagerException {
     if (ttcrxs.remove(rxName) == null)
     throw new TTCManagerException("TTCrx with name " + rxName + " not exist");
     }*/
    
    void removeTTCrx(int indx) {
        ttcrxs.remove(indx);
    }
    
    /*TTCrx getTTCrx(String rxName) throws TTCManagerException {
     TTCrx rx = (TTCrx)ttcrxs.get(rxName);
     if (rx == null)
     throw new TTCManagerException("TTCrx with name " + rxName + " not exist");
     return rx;
     }*/
    
    TTCrx getTTCrx(int indx) {
        return (TTCrx)ttcrxs.get(indx);
    }
    
    TTCrx getTTCrxByAddress(short address) throws TTCManagerException {
        TTCrx ttcrx = null;
        for(int i = 0; i < ttcrxs.size(); i++) {
            ttcrx = (TTCrx)ttcrxs.get(i);
            if ( ttcrx.getAddress() == address)
                return ttcrx;
        }
        throw new TTCManagerException("TTCrx with adderes " + String.valueOf(address) + " not exist");
    }
    
    Vector getTTCrxsVec() {
        return ttcrxs;
    }
    
    void addBGoData(BGoData bGoData) {
        bGoDataVec.add(bGoData);
    }
    
    void addBGoData(BGoData bGoData, int indx) {
        bGoDataVec.add(indx, bGoData);
    }
    
    void setBGoData(BGoData bGoData, int indx) { //Replaces the element at the specified position in this Vector with the specified element.
        bGoDataVec.set(indx, bGoData);
    }
    
    void removeBGoData(int indx) {
        bGoDataVec.remove(indx);
    }
    
    void clearBGoData() {
        bGoDataVec.clear();
    }
    
    void loadBGoData(int bGoNmbr, int indx) {//loads BGo[bGoNmbr] with data in bGoDataVec[indx]
        try {
            Vector bGoCycles = ( (BGoData) bGoDataVec.get(indx)).getBGoCyclesVec();
            for (int i = 0; i < bGoCycles.size(); i++) {
                if (bGoCycles.get(i) instanceof rpct.ttccontrol.ShortTTCviCycle)
                    ttcvi.getBGo(bGoNmbr).WriteDataShort(
                            ( (ShortTTCviCycle) bGoCycles.get(i)).getCommand());
                if (bGoCycles.get(i) instanceof rpct.ttccontrol.LongTTCviCycle)
                    ttcvi.getBGo(bGoNmbr).WriteDataLong(
                            ( (LongTTCviCycle) bGoCycles.get(i)).getRxAddress(),
                            ( (LongTTCviCycle) bGoCycles.get(i)).getE(),
                            ( (LongTTCviCycle) bGoCycles.get(i)).getSubAddress(),
                            ( (LongTTCviCycle) bGoCycles.get(i)).getData());
            }
        }
        catch(XdaqAccessException ex) {
            javax.swing.JOptionPane.showMessageDialog(
                    null,
                    ex.getMessage(),
                    "Error",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
        }
    }
    
    Vector getBGoDataVec() {
        return bGoDataVec;
    }
}