/*
 * This class was automatically generated with 
 * <a href="http://castor.exolab.org">Castor 0.9.4</a>, using an
 * XML Schema.
 * $Id: XmlCycleType.java,v 1.1 2005/06/29 16:33:03 tb Exp $
 */

package rpct.castor;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.*;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;
import rpct.castor.types.XmlTypeType;

/**
 * 
 * 
 * @version $Revision: 1.1 $ $Date: 2005/06/29 16:33:03 $
**/
public abstract class XmlCycleType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    private rpct.castor.types.XmlTypeType _xmlType;

    private java.lang.String _xmlRxAddress;

    private java.lang.String _xmlSubAddress;

    private java.lang.String _xmlE;

    private java.lang.String _xmlData;


      //----------------/
     //- Constructors -/
    //----------------/

    public XmlCycleType() {
        super();
    } //-- rpct.castor.XmlCycleType()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'xmlData'.
     * 
     * @return the value of field 'xmlData'.
    **/
    public java.lang.String getXmlData()
    {
        return this._xmlData;
    } //-- java.lang.String getXmlData() 

    /**
     * Returns the value of field 'xmlE'.
     * 
     * @return the value of field 'xmlE'.
    **/
    public java.lang.String getXmlE()
    {
        return this._xmlE;
    } //-- java.lang.String getXmlE() 

    /**
     * Returns the value of field 'xmlRxAddress'.
     * 
     * @return the value of field 'xmlRxAddress'.
    **/
    public java.lang.String getXmlRxAddress()
    {
        return this._xmlRxAddress;
    } //-- java.lang.String getXmlRxAddress() 

    /**
     * Returns the value of field 'xmlSubAddress'.
     * 
     * @return the value of field 'xmlSubAddress'.
    **/
    public java.lang.String getXmlSubAddress()
    {
        return this._xmlSubAddress;
    } //-- java.lang.String getXmlSubAddress() 

    /**
     * Returns the value of field 'xmlType'.
     * 
     * @return the value of field 'xmlType'.
    **/
    public rpct.castor.types.XmlTypeType getXmlType()
    {
        return this._xmlType;
    } //-- rpct.castor.types.XmlTypeType getXmlType() 

    /**
    **/
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * 
     * 
     * @param out
    **/
    public abstract void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException;

    /**
     * 
     * 
     * @param handler
    **/
    public abstract void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException;

    /**
     * Sets the value of field 'xmlData'.
     * 
     * @param xmlData the value of field 'xmlData'.
    **/
    public void setXmlData(java.lang.String xmlData)
    {
        this._xmlData = xmlData;
    } //-- void setXmlData(java.lang.String) 

    /**
     * Sets the value of field 'xmlE'.
     * 
     * @param xmlE the value of field 'xmlE'.
    **/
    public void setXmlE(java.lang.String xmlE)
    {
        this._xmlE = xmlE;
    } //-- void setXmlE(java.lang.String) 

    /**
     * Sets the value of field 'xmlRxAddress'.
     * 
     * @param xmlRxAddress the value of field 'xmlRxAddress'.
    **/
    public void setXmlRxAddress(java.lang.String xmlRxAddress)
    {
        this._xmlRxAddress = xmlRxAddress;
    } //-- void setXmlRxAddress(java.lang.String) 

    /**
     * Sets the value of field 'xmlSubAddress'.
     * 
     * @param xmlSubAddress the value of field 'xmlSubAddress'.
    **/
    public void setXmlSubAddress(java.lang.String xmlSubAddress)
    {
        this._xmlSubAddress = xmlSubAddress;
    } //-- void setXmlSubAddress(java.lang.String) 

    /**
     * Sets the value of field 'xmlType'.
     * 
     * @param xmlType the value of field 'xmlType'.
    **/
    public void setXmlType(rpct.castor.types.XmlTypeType xmlType)
    {
        this._xmlType = xmlType;
    } //-- void setXmlType(rpct.castor.types.XmlTypeType) 

    /**
    **/
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
