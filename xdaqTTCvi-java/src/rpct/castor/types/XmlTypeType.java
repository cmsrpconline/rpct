/*
 * This class was automatically generated with 
 * <a href="http://castor.exolab.org">Castor 0.9.4</a>, using an
 * XML Schema.
 * $Id: XmlTypeType.java,v 1.1 2005/06/29 16:33:04 tb Exp $
 */

package rpct.castor.types;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Hashtable;
import org.exolab.castor.xml.*;

/**
 * 
 * 
 * @version $Revision: 1.1 $ $Date: 2005/06/29 16:33:04 $
**/
public class XmlTypeType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * The Short type
    **/
    public static final int SHORT_TYPE = 0;

    /**
     * The instance of the Short type
    **/
    public static final XmlTypeType SHORT = new XmlTypeType(SHORT_TYPE, "Short");

    /**
     * The Long type
    **/
    public static final int LONG_TYPE = 1;

    /**
     * The instance of the Long type
    **/
    public static final XmlTypeType LONG = new XmlTypeType(LONG_TYPE, "Long");

    private static java.util.Hashtable _memberTable = init();

    private int type = -1;

    private java.lang.String stringValue = null;


      //----------------/
     //- Constructors -/
    //----------------/

    private XmlTypeType(int type, java.lang.String value) {
        super();
        this.type = type;
        this.stringValue = value;
    } //-- rpct.castor.types.XmlTypeType(int, java.lang.String)


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns an enumeration of all possible instances of
     * XmlTypeType
    **/
    public static java.util.Enumeration enumerate()
    {
        return _memberTable.elements();
    } //-- java.util.Enumeration enumerate() 

    /**
     * Returns the type of this XmlTypeType
    **/
    public int getType()
    {
        return this.type;
    } //-- int getType() 

    /**
    **/
    private static java.util.Hashtable init()
    {
        Hashtable members = new Hashtable();
        members.put("Short", SHORT);
        members.put("Long", LONG);
        return members;
    } //-- java.util.Hashtable init() 

    /**
     * Returns the String representation of this XmlTypeType
    **/
    public java.lang.String toString()
    {
        return this.stringValue;
    } //-- java.lang.String toString() 

    /**
     * Returns a new XmlTypeType based on the given String value.
     * 
     * @param string
    **/
    public static rpct.castor.types.XmlTypeType valueOf(java.lang.String string)
    {
        java.lang.Object obj = null;
        if (string != null) obj = _memberTable.get(string);
        if (obj == null) {
            String err = "'" + string + "' is not a valid XmlTypeType";
            throw new IllegalArgumentException(err);
        }
        return (XmlTypeType) obj;
    } //-- rpct.castor.types.XmlTypeType valueOf(java.lang.String) 

}
