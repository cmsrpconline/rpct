/*
 * This class was automatically generated with 
 * <a href="http://castor.exolab.org">Castor 0.9.4</a>, using an
 * XML Schema.
 * $Id: XmlTTCrxType.java,v 1.1 2005/06/29 16:33:03 tb Exp $
 */

package rpct.castor;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.*;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * 
 * 
 * @version $Revision: 1.1 $ $Date: 2005/06/29 16:33:03 $
**/
public abstract class XmlTTCrxType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    private java.lang.String _xmlRxName;

    private java.lang.String _xmlAddress;

    private java.lang.String _xmlControlRegister;

    private java.lang.String _xmlDeskew1;

    private java.lang.String _xmlDeskew2;

    private java.lang.String _xmlCoarDelay1;

    private java.lang.String _xmlCoarDelay2;


      //----------------/
     //- Constructors -/
    //----------------/

    public XmlTTCrxType() {
        super();
    } //-- rpct.castor.XmlTTCrxType()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'xmlAddress'.
     * 
     * @return the value of field 'xmlAddress'.
    **/
    public java.lang.String getXmlAddress()
    {
        return this._xmlAddress;
    } //-- java.lang.String getXmlAddress() 

    /**
     * Returns the value of field 'xmlCoarDelay1'.
     * 
     * @return the value of field 'xmlCoarDelay1'.
    **/
    public java.lang.String getXmlCoarDelay1()
    {
        return this._xmlCoarDelay1;
    } //-- java.lang.String getXmlCoarDelay1() 

    /**
     * Returns the value of field 'xmlCoarDelay2'.
     * 
     * @return the value of field 'xmlCoarDelay2'.
    **/
    public java.lang.String getXmlCoarDelay2()
    {
        return this._xmlCoarDelay2;
    } //-- java.lang.String getXmlCoarDelay2() 

    /**
     * Returns the value of field 'xmlControlRegister'.
     * 
     * @return the value of field 'xmlControlRegister'.
    **/
    public java.lang.String getXmlControlRegister()
    {
        return this._xmlControlRegister;
    } //-- java.lang.String getXmlControlRegister() 

    /**
     * Returns the value of field 'xmlDeskew1'.
     * 
     * @return the value of field 'xmlDeskew1'.
    **/
    public java.lang.String getXmlDeskew1()
    {
        return this._xmlDeskew1;
    } //-- java.lang.String getXmlDeskew1() 

    /**
     * Returns the value of field 'xmlDeskew2'.
     * 
     * @return the value of field 'xmlDeskew2'.
    **/
    public java.lang.String getXmlDeskew2()
    {
        return this._xmlDeskew2;
    } //-- java.lang.String getXmlDeskew2() 

    /**
     * Returns the value of field 'xmlRxName'.
     * 
     * @return the value of field 'xmlRxName'.
    **/
    public java.lang.String getXmlRxName()
    {
        return this._xmlRxName;
    } //-- java.lang.String getXmlRxName() 

    /**
    **/
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * 
     * 
     * @param out
    **/
    public abstract void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException;

    /**
     * 
     * 
     * @param handler
    **/
    public abstract void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException;

    /**
     * Sets the value of field 'xmlAddress'.
     * 
     * @param xmlAddress the value of field 'xmlAddress'.
    **/
    public void setXmlAddress(java.lang.String xmlAddress)
    {
        this._xmlAddress = xmlAddress;
    } //-- void setXmlAddress(java.lang.String) 

    /**
     * Sets the value of field 'xmlCoarDelay1'.
     * 
     * @param xmlCoarDelay1 the value of field 'xmlCoarDelay1'.
    **/
    public void setXmlCoarDelay1(java.lang.String xmlCoarDelay1)
    {
        this._xmlCoarDelay1 = xmlCoarDelay1;
    } //-- void setXmlCoarDelay1(java.lang.String) 

    /**
     * Sets the value of field 'xmlCoarDelay2'.
     * 
     * @param xmlCoarDelay2 the value of field 'xmlCoarDelay2'.
    **/
    public void setXmlCoarDelay2(java.lang.String xmlCoarDelay2)
    {
        this._xmlCoarDelay2 = xmlCoarDelay2;
    } //-- void setXmlCoarDelay2(java.lang.String) 

    /**
     * Sets the value of field 'xmlControlRegister'.
     * 
     * @param xmlControlRegister the value of field
     * 'xmlControlRegister'.
    **/
    public void setXmlControlRegister(java.lang.String xmlControlRegister)
    {
        this._xmlControlRegister = xmlControlRegister;
    } //-- void setXmlControlRegister(java.lang.String) 

    /**
     * Sets the value of field 'xmlDeskew1'.
     * 
     * @param xmlDeskew1 the value of field 'xmlDeskew1'.
    **/
    public void setXmlDeskew1(java.lang.String xmlDeskew1)
    {
        this._xmlDeskew1 = xmlDeskew1;
    } //-- void setXmlDeskew1(java.lang.String) 

    /**
     * Sets the value of field 'xmlDeskew2'.
     * 
     * @param xmlDeskew2 the value of field 'xmlDeskew2'.
    **/
    public void setXmlDeskew2(java.lang.String xmlDeskew2)
    {
        this._xmlDeskew2 = xmlDeskew2;
    } //-- void setXmlDeskew2(java.lang.String) 

    /**
     * Sets the value of field 'xmlRxName'.
     * 
     * @param xmlRxName the value of field 'xmlRxName'.
    **/
    public void setXmlRxName(java.lang.String xmlRxName)
    {
        this._xmlRxName = xmlRxName;
    } //-- void setXmlRxName(java.lang.String) 

    /**
    **/
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
