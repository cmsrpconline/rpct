/*
 * This class was automatically generated with 
 * <a href="http://castor.exolab.org">Castor 0.9.4</a>, using an
 * XML Schema.
 * $Id: XmlTTCrxs.java,v 1.1 2005/06/29 16:33:03 tb Exp $
 */

package rpct.castor;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.*;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * 
 * 
 * @version $Revision: 1.1 $ $Date: 2005/06/29 16:33:03 $
**/
public class XmlTTCrxs implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    private java.util.Vector _xmlTTCrxList;


      //----------------/
     //- Constructors -/
    //----------------/

    public XmlTTCrxs() {
        super();
        _xmlTTCrxList = new Vector();
    } //-- rpct.castor.XmlTTCrxs()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vXmlTTCrx
    **/
    public void addXmlTTCrx(XmlTTCrx vXmlTTCrx)
        throws java.lang.IndexOutOfBoundsException
    {
        _xmlTTCrxList.addElement(vXmlTTCrx);
    } //-- void addXmlTTCrx(XmlTTCrx) 

    /**
     * 
     * 
     * @param index
     * @param vXmlTTCrx
    **/
    public void addXmlTTCrx(int index, XmlTTCrx vXmlTTCrx)
        throws java.lang.IndexOutOfBoundsException
    {
        _xmlTTCrxList.insertElementAt(vXmlTTCrx, index);
    } //-- void addXmlTTCrx(int, XmlTTCrx) 

    /**
    **/
    public java.util.Enumeration enumerateXmlTTCrx()
    {
        return _xmlTTCrxList.elements();
    } //-- java.util.Enumeration enumerateXmlTTCrx() 

    /**
     * 
     * 
     * @param index
    **/
    public XmlTTCrx getXmlTTCrx(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _xmlTTCrxList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (XmlTTCrx) _xmlTTCrxList.elementAt(index);
    } //-- XmlTTCrx getXmlTTCrx(int) 

    /**
    **/
    public XmlTTCrx[] getXmlTTCrx()
    {
        int size = _xmlTTCrxList.size();
        XmlTTCrx[] mArray = new XmlTTCrx[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (XmlTTCrx) _xmlTTCrxList.elementAt(index);
        }
        return mArray;
    } //-- XmlTTCrx[] getXmlTTCrx() 

    /**
    **/
    public int getXmlTTCrxCount()
    {
        return _xmlTTCrxList.size();
    } //-- int getXmlTTCrxCount() 

    /**
    **/
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * 
     * 
     * @param out
    **/
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * 
     * 
     * @param handler
    **/
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
    **/
    public void removeAllXmlTTCrx()
    {
        _xmlTTCrxList.removeAllElements();
    } //-- void removeAllXmlTTCrx() 

    /**
     * 
     * 
     * @param index
    **/
    public XmlTTCrx removeXmlTTCrx(int index)
    {
        java.lang.Object obj = _xmlTTCrxList.elementAt(index);
        _xmlTTCrxList.removeElementAt(index);
        return (XmlTTCrx) obj;
    } //-- XmlTTCrx removeXmlTTCrx(int) 

    /**
     * 
     * 
     * @param index
     * @param vXmlTTCrx
    **/
    public void setXmlTTCrx(int index, XmlTTCrx vXmlTTCrx)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _xmlTTCrxList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _xmlTTCrxList.setElementAt(vXmlTTCrx, index);
    } //-- void setXmlTTCrx(int, XmlTTCrx) 

    /**
     * 
     * 
     * @param xmlTTCrxArray
    **/
    public void setXmlTTCrx(XmlTTCrx[] xmlTTCrxArray)
    {
        //-- copy array
        _xmlTTCrxList.removeAllElements();
        for (int i = 0; i < xmlTTCrxArray.length; i++) {
            _xmlTTCrxList.addElement(xmlTTCrxArray[i]);
        }
    } //-- void setXmlTTCrx(XmlTTCrx) 

    /**
     * 
     * 
     * @param reader
    **/
    public static rpct.castor.XmlTTCrxs unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (rpct.castor.XmlTTCrxs) Unmarshaller.unmarshal(rpct.castor.XmlTTCrxs.class, reader);
    } //-- rpct.castor.XmlTTCrxs unmarshal(java.io.Reader) 

    /**
    **/
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
