/*
 * This class was automatically generated with 
 * <a href="http://castor.exolab.org">Castor 0.9.4</a>, using an
 * XML Schema.
 * $Id: XmlBGos.java,v 1.1 2005/06/29 16:33:03 tb Exp $
 */

package rpct.castor;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.*;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * 
 * 
 * @version $Revision: 1.1 $ $Date: 2005/06/29 16:33:03 $
**/
public class XmlBGos implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    private java.util.Vector _xmlBGoList;


      //----------------/
     //- Constructors -/
    //----------------/

    public XmlBGos() {
        super();
        _xmlBGoList = new Vector();
    } //-- rpct.castor.XmlBGos()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vXmlBGo
    **/
    public void addXmlBGo(XmlBGo vXmlBGo)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_xmlBGoList.size() < 4)) {
            throw new IndexOutOfBoundsException();
        }
        _xmlBGoList.addElement(vXmlBGo);
    } //-- void addXmlBGo(XmlBGo) 

    /**
     * 
     * 
     * @param index
     * @param vXmlBGo
    **/
    public void addXmlBGo(int index, XmlBGo vXmlBGo)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_xmlBGoList.size() < 4)) {
            throw new IndexOutOfBoundsException();
        }
        _xmlBGoList.insertElementAt(vXmlBGo, index);
    } //-- void addXmlBGo(int, XmlBGo) 

    /**
    **/
    public java.util.Enumeration enumerateXmlBGo()
    {
        return _xmlBGoList.elements();
    } //-- java.util.Enumeration enumerateXmlBGo() 

    /**
     * 
     * 
     * @param index
    **/
    public XmlBGo getXmlBGo(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _xmlBGoList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (XmlBGo) _xmlBGoList.elementAt(index);
    } //-- XmlBGo getXmlBGo(int) 

    /**
    **/
    public XmlBGo[] getXmlBGo()
    {
        int size = _xmlBGoList.size();
        XmlBGo[] mArray = new XmlBGo[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (XmlBGo) _xmlBGoList.elementAt(index);
        }
        return mArray;
    } //-- XmlBGo[] getXmlBGo() 

    /**
    **/
    public int getXmlBGoCount()
    {
        return _xmlBGoList.size();
    } //-- int getXmlBGoCount() 

    /**
    **/
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * 
     * 
     * @param out
    **/
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * 
     * 
     * @param handler
    **/
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
    **/
    public void removeAllXmlBGo()
    {
        _xmlBGoList.removeAllElements();
    } //-- void removeAllXmlBGo() 

    /**
     * 
     * 
     * @param index
    **/
    public XmlBGo removeXmlBGo(int index)
    {
        java.lang.Object obj = _xmlBGoList.elementAt(index);
        _xmlBGoList.removeElementAt(index);
        return (XmlBGo) obj;
    } //-- XmlBGo removeXmlBGo(int) 

    /**
     * 
     * 
     * @param index
     * @param vXmlBGo
    **/
    public void setXmlBGo(int index, XmlBGo vXmlBGo)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _xmlBGoList.size())) {
            throw new IndexOutOfBoundsException();
        }
        if (!(index < 4)) {
            throw new IndexOutOfBoundsException();
        }
        _xmlBGoList.setElementAt(vXmlBGo, index);
    } //-- void setXmlBGo(int, XmlBGo) 

    /**
     * 
     * 
     * @param xmlBGoArray
    **/
    public void setXmlBGo(XmlBGo[] xmlBGoArray)
    {
        //-- copy array
        _xmlBGoList.removeAllElements();
        for (int i = 0; i < xmlBGoArray.length; i++) {
            _xmlBGoList.addElement(xmlBGoArray[i]);
        }
    } //-- void setXmlBGo(XmlBGo) 

    /**
     * 
     * 
     * @param reader
    **/
    public static rpct.castor.XmlBGos unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (rpct.castor.XmlBGos) Unmarshaller.unmarshal(rpct.castor.XmlBGos.class, reader);
    } //-- rpct.castor.XmlBGos unmarshal(java.io.Reader) 

    /**
    **/
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
