/*
 * This class was automatically generated with 
 * <a href="http://castor.exolab.org">Castor 0.9.4</a>, using an
 * XML Schema.
 * $Id: XmlBGoType.java,v 1.1 2005/06/29 16:33:03 tb Exp $
 */

package rpct.castor;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.*;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * 
 * 
 * @version $Revision: 1.1 $ $Date: 2005/06/29 16:33:03 $
**/
public abstract class XmlBGoType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    private java.lang.String _xmlSettings;

    private java.lang.String _xmlDataNum;


      //----------------/
     //- Constructors -/
    //----------------/

    public XmlBGoType() {
        super();
    } //-- rpct.castor.XmlBGoType()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'xmlDataNum'.
     * 
     * @return the value of field 'xmlDataNum'.
    **/
    public java.lang.String getXmlDataNum()
    {
        return this._xmlDataNum;
    } //-- java.lang.String getXmlDataNum() 

    /**
     * Returns the value of field 'xmlSettings'.
     * 
     * @return the value of field 'xmlSettings'.
    **/
    public java.lang.String getXmlSettings()
    {
        return this._xmlSettings;
    } //-- java.lang.String getXmlSettings() 

    /**
    **/
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * 
     * 
     * @param out
    **/
    public abstract void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException;

    /**
     * 
     * 
     * @param handler
    **/
    public abstract void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException;

    /**
     * Sets the value of field 'xmlDataNum'.
     * 
     * @param xmlDataNum the value of field 'xmlDataNum'.
    **/
    public void setXmlDataNum(java.lang.String xmlDataNum)
    {
        this._xmlDataNum = xmlDataNum;
    } //-- void setXmlDataNum(java.lang.String) 

    /**
     * Sets the value of field 'xmlSettings'.
     * 
     * @param xmlSettings the value of field 'xmlSettings'.
    **/
    public void setXmlSettings(java.lang.String xmlSettings)
    {
        this._xmlSettings = xmlSettings;
    } //-- void setXmlSettings(java.lang.String) 

    /**
    **/
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
