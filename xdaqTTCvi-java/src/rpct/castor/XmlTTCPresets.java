/*
 * This class was automatically generated with 
 * <a href="http://castor.exolab.org">Castor 0.9.4</a>, using an
 * XML Schema.
 * $Id: XmlTTCPresets.java,v 1.1 2005/06/29 16:33:03 tb Exp $
 */

package rpct.castor;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.*;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * 
 * 
 * @version $Revision: 1.1 $ $Date: 2005/06/29 16:33:03 $
**/
public class XmlTTCPresets implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    private java.util.Vector _xmlTTCPresetList;


      //----------------/
     //- Constructors -/
    //----------------/

    public XmlTTCPresets() {
        super();
        _xmlTTCPresetList = new Vector();
    } //-- rpct.castor.XmlTTCPresets()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vXmlTTCPreset
    **/
    public void addXmlTTCPreset(XmlTTCPreset vXmlTTCPreset)
        throws java.lang.IndexOutOfBoundsException
    {
        _xmlTTCPresetList.addElement(vXmlTTCPreset);
    } //-- void addXmlTTCPreset(XmlTTCPreset) 

    /**
     * 
     * 
     * @param index
     * @param vXmlTTCPreset
    **/
    public void addXmlTTCPreset(int index, XmlTTCPreset vXmlTTCPreset)
        throws java.lang.IndexOutOfBoundsException
    {
        _xmlTTCPresetList.insertElementAt(vXmlTTCPreset, index);
    } //-- void addXmlTTCPreset(int, XmlTTCPreset) 

    /**
    **/
    public java.util.Enumeration enumerateXmlTTCPreset()
    {
        return _xmlTTCPresetList.elements();
    } //-- java.util.Enumeration enumerateXmlTTCPreset() 

    /**
     * 
     * 
     * @param index
    **/
    public XmlTTCPreset getXmlTTCPreset(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _xmlTTCPresetList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (XmlTTCPreset) _xmlTTCPresetList.elementAt(index);
    } //-- XmlTTCPreset getXmlTTCPreset(int) 

    /**
    **/
    public XmlTTCPreset[] getXmlTTCPreset()
    {
        int size = _xmlTTCPresetList.size();
        XmlTTCPreset[] mArray = new XmlTTCPreset[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (XmlTTCPreset) _xmlTTCPresetList.elementAt(index);
        }
        return mArray;
    } //-- XmlTTCPreset[] getXmlTTCPreset() 

    /**
    **/
    public int getXmlTTCPresetCount()
    {
        return _xmlTTCPresetList.size();
    } //-- int getXmlTTCPresetCount() 

    /**
    **/
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * 
     * 
     * @param out
    **/
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * 
     * 
     * @param handler
    **/
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
    **/
    public void removeAllXmlTTCPreset()
    {
        _xmlTTCPresetList.removeAllElements();
    } //-- void removeAllXmlTTCPreset() 

    /**
     * 
     * 
     * @param index
    **/
    public XmlTTCPreset removeXmlTTCPreset(int index)
    {
        java.lang.Object obj = _xmlTTCPresetList.elementAt(index);
        _xmlTTCPresetList.removeElementAt(index);
        return (XmlTTCPreset) obj;
    } //-- XmlTTCPreset removeXmlTTCPreset(int) 

    /**
     * 
     * 
     * @param index
     * @param vXmlTTCPreset
    **/
    public void setXmlTTCPreset(int index, XmlTTCPreset vXmlTTCPreset)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _xmlTTCPresetList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _xmlTTCPresetList.setElementAt(vXmlTTCPreset, index);
    } //-- void setXmlTTCPreset(int, XmlTTCPreset) 

    /**
     * 
     * 
     * @param xmlTTCPresetArray
    **/
    public void setXmlTTCPreset(XmlTTCPreset[] xmlTTCPresetArray)
    {
        //-- copy array
        _xmlTTCPresetList.removeAllElements();
        for (int i = 0; i < xmlTTCPresetArray.length; i++) {
            _xmlTTCPresetList.addElement(xmlTTCPresetArray[i]);
        }
    } //-- void setXmlTTCPreset(XmlTTCPreset) 

    /**
     * 
     * 
     * @param reader
    **/
    public static rpct.castor.XmlTTCPresets unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (rpct.castor.XmlTTCPresets) Unmarshaller.unmarshal(rpct.castor.XmlTTCPresets.class, reader);
    } //-- rpct.castor.XmlTTCPresets unmarshal(java.io.Reader) 

    /**
    **/
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
