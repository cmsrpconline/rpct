/*
 * This class was automatically generated with 
 * <a href="http://castor.exolab.org">Castor 0.9.4</a>, using an
 * XML Schema.
 * $Id: XmlInhibits.java,v 1.1 2005/06/29 16:33:03 tb Exp $
 */

package rpct.castor;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.*;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * 
 * 
 * @version $Revision: 1.1 $ $Date: 2005/06/29 16:33:03 $
**/
public class XmlInhibits implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    private java.util.Vector _xmlInhibitList;


      //----------------/
     //- Constructors -/
    //----------------/

    public XmlInhibits() {
        super();
        _xmlInhibitList = new Vector();
    } //-- rpct.castor.XmlInhibits()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vXmlInhibit
    **/
    public void addXmlInhibit(XmlInhibit vXmlInhibit)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_xmlInhibitList.size() < 4)) {
            throw new IndexOutOfBoundsException();
        }
        _xmlInhibitList.addElement(vXmlInhibit);
    } //-- void addXmlInhibit(XmlInhibit) 

    /**
     * 
     * 
     * @param index
     * @param vXmlInhibit
    **/
    public void addXmlInhibit(int index, XmlInhibit vXmlInhibit)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_xmlInhibitList.size() < 4)) {
            throw new IndexOutOfBoundsException();
        }
        _xmlInhibitList.insertElementAt(vXmlInhibit, index);
    } //-- void addXmlInhibit(int, XmlInhibit) 

    /**
    **/
    public java.util.Enumeration enumerateXmlInhibit()
    {
        return _xmlInhibitList.elements();
    } //-- java.util.Enumeration enumerateXmlInhibit() 

    /**
     * 
     * 
     * @param index
    **/
    public XmlInhibit getXmlInhibit(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _xmlInhibitList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (XmlInhibit) _xmlInhibitList.elementAt(index);
    } //-- XmlInhibit getXmlInhibit(int) 

    /**
    **/
    public XmlInhibit[] getXmlInhibit()
    {
        int size = _xmlInhibitList.size();
        XmlInhibit[] mArray = new XmlInhibit[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (XmlInhibit) _xmlInhibitList.elementAt(index);
        }
        return mArray;
    } //-- XmlInhibit[] getXmlInhibit() 

    /**
    **/
    public int getXmlInhibitCount()
    {
        return _xmlInhibitList.size();
    } //-- int getXmlInhibitCount() 

    /**
    **/
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * 
     * 
     * @param out
    **/
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * 
     * 
     * @param handler
    **/
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
    **/
    public void removeAllXmlInhibit()
    {
        _xmlInhibitList.removeAllElements();
    } //-- void removeAllXmlInhibit() 

    /**
     * 
     * 
     * @param index
    **/
    public XmlInhibit removeXmlInhibit(int index)
    {
        java.lang.Object obj = _xmlInhibitList.elementAt(index);
        _xmlInhibitList.removeElementAt(index);
        return (XmlInhibit) obj;
    } //-- XmlInhibit removeXmlInhibit(int) 

    /**
     * 
     * 
     * @param index
     * @param vXmlInhibit
    **/
    public void setXmlInhibit(int index, XmlInhibit vXmlInhibit)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _xmlInhibitList.size())) {
            throw new IndexOutOfBoundsException();
        }
        if (!(index < 4)) {
            throw new IndexOutOfBoundsException();
        }
        _xmlInhibitList.setElementAt(vXmlInhibit, index);
    } //-- void setXmlInhibit(int, XmlInhibit) 

    /**
     * 
     * 
     * @param xmlInhibitArray
    **/
    public void setXmlInhibit(XmlInhibit[] xmlInhibitArray)
    {
        //-- copy array
        _xmlInhibitList.removeAllElements();
        for (int i = 0; i < xmlInhibitArray.length; i++) {
            _xmlInhibitList.addElement(xmlInhibitArray[i]);
        }
    } //-- void setXmlInhibit(XmlInhibit) 

    /**
     * 
     * 
     * @param reader
    **/
    public static rpct.castor.XmlInhibits unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (rpct.castor.XmlInhibits) Unmarshaller.unmarshal(rpct.castor.XmlInhibits.class, reader);
    } //-- rpct.castor.XmlInhibits unmarshal(java.io.Reader) 

    /**
    **/
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
