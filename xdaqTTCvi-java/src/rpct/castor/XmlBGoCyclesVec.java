/*
 * This class was automatically generated with 
 * <a href="http://castor.exolab.org">Castor 0.9.4</a>, using an
 * XML Schema.
 * $Id: XmlBGoCyclesVec.java,v 1.1 2005/06/29 16:33:03 tb Exp $
 */

package rpct.castor;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.*;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * 
 * 
 * @version $Revision: 1.1 $ $Date: 2005/06/29 16:33:03 $
**/
public class XmlBGoCyclesVec implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    private java.util.Vector _xmlCycleList;


      //----------------/
     //- Constructors -/
    //----------------/

    public XmlBGoCyclesVec() {
        super();
        _xmlCycleList = new Vector();
    } //-- rpct.castor.XmlBGoCyclesVec()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vXmlCycle
    **/
    public void addXmlCycle(XmlCycle vXmlCycle)
        throws java.lang.IndexOutOfBoundsException
    {
        _xmlCycleList.addElement(vXmlCycle);
    } //-- void addXmlCycle(XmlCycle) 

    /**
     * 
     * 
     * @param index
     * @param vXmlCycle
    **/
    public void addXmlCycle(int index, XmlCycle vXmlCycle)
        throws java.lang.IndexOutOfBoundsException
    {
        _xmlCycleList.insertElementAt(vXmlCycle, index);
    } //-- void addXmlCycle(int, XmlCycle) 

    /**
    **/
    public java.util.Enumeration enumerateXmlCycle()
    {
        return _xmlCycleList.elements();
    } //-- java.util.Enumeration enumerateXmlCycle() 

    /**
     * 
     * 
     * @param index
    **/
    public XmlCycle getXmlCycle(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _xmlCycleList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (XmlCycle) _xmlCycleList.elementAt(index);
    } //-- XmlCycle getXmlCycle(int) 

    /**
    **/
    public XmlCycle[] getXmlCycle()
    {
        int size = _xmlCycleList.size();
        XmlCycle[] mArray = new XmlCycle[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (XmlCycle) _xmlCycleList.elementAt(index);
        }
        return mArray;
    } //-- XmlCycle[] getXmlCycle() 

    /**
    **/
    public int getXmlCycleCount()
    {
        return _xmlCycleList.size();
    } //-- int getXmlCycleCount() 

    /**
    **/
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * 
     * 
     * @param out
    **/
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * 
     * 
     * @param handler
    **/
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
    **/
    public void removeAllXmlCycle()
    {
        _xmlCycleList.removeAllElements();
    } //-- void removeAllXmlCycle() 

    /**
     * 
     * 
     * @param index
    **/
    public XmlCycle removeXmlCycle(int index)
    {
        java.lang.Object obj = _xmlCycleList.elementAt(index);
        _xmlCycleList.removeElementAt(index);
        return (XmlCycle) obj;
    } //-- XmlCycle removeXmlCycle(int) 

    /**
     * 
     * 
     * @param index
     * @param vXmlCycle
    **/
    public void setXmlCycle(int index, XmlCycle vXmlCycle)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _xmlCycleList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _xmlCycleList.setElementAt(vXmlCycle, index);
    } //-- void setXmlCycle(int, XmlCycle) 

    /**
     * 
     * 
     * @param xmlCycleArray
    **/
    public void setXmlCycle(XmlCycle[] xmlCycleArray)
    {
        //-- copy array
        _xmlCycleList.removeAllElements();
        for (int i = 0; i < xmlCycleArray.length; i++) {
            _xmlCycleList.addElement(xmlCycleArray[i]);
        }
    } //-- void setXmlCycle(XmlCycle) 

    /**
     * 
     * 
     * @param reader
    **/
    public static rpct.castor.XmlBGoCyclesVec unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (rpct.castor.XmlBGoCyclesVec) Unmarshaller.unmarshal(rpct.castor.XmlBGoCyclesVec.class, reader);
    } //-- rpct.castor.XmlBGoCyclesVec unmarshal(java.io.Reader) 

    /**
    **/
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
