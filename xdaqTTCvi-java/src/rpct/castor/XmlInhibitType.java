/*
 * This class was automatically generated with 
 * <a href="http://castor.exolab.org">Castor 0.9.4</a>, using an
 * XML Schema.
 * $Id: XmlInhibitType.java,v 1.1 2005/06/29 16:33:03 tb Exp $
 */

package rpct.castor;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.*;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * 
 * 
 * @version $Revision: 1.1 $ $Date: 2005/06/29 16:33:03 $
**/
public abstract class XmlInhibitType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    private short _xmlDelay;

    /**
     * keeps track of state for field: _xmlDelay
    **/
    private boolean _has_xmlDelay;

    private short _xmlDuration;

    /**
     * keeps track of state for field: _xmlDuration
    **/
    private boolean _has_xmlDuration;


      //----------------/
     //- Constructors -/
    //----------------/

    public XmlInhibitType() {
        super();
    } //-- rpct.castor.XmlInhibitType()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'xmlDelay'.
     * 
     * @return the value of field 'xmlDelay'.
    **/
    public short getXmlDelay()
    {
        return this._xmlDelay;
    } //-- short getXmlDelay() 

    /**
     * Returns the value of field 'xmlDuration'.
     * 
     * @return the value of field 'xmlDuration'.
    **/
    public short getXmlDuration()
    {
        return this._xmlDuration;
    } //-- short getXmlDuration() 

    /**
    **/
    public boolean hasXmlDelay()
    {
        return this._has_xmlDelay;
    } //-- boolean hasXmlDelay() 

    /**
    **/
    public boolean hasXmlDuration()
    {
        return this._has_xmlDuration;
    } //-- boolean hasXmlDuration() 

    /**
    **/
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * 
     * 
     * @param out
    **/
    public abstract void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException;

    /**
     * 
     * 
     * @param handler
    **/
    public abstract void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException;

    /**
     * Sets the value of field 'xmlDelay'.
     * 
     * @param xmlDelay the value of field 'xmlDelay'.
    **/
    public void setXmlDelay(short xmlDelay)
    {
        this._xmlDelay = xmlDelay;
        this._has_xmlDelay = true;
    } //-- void setXmlDelay(short) 

    /**
     * Sets the value of field 'xmlDuration'.
     * 
     * @param xmlDuration the value of field 'xmlDuration'.
    **/
    public void setXmlDuration(short xmlDuration)
    {
        this._xmlDuration = xmlDuration;
        this._has_xmlDuration = true;
    } //-- void setXmlDuration(short) 

    /**
    **/
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
