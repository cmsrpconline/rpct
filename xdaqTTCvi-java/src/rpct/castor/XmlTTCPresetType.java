/*
 * This class was automatically generated with 
 * <a href="http://castor.exolab.org">Castor 0.9.4</a>, using an
 * XML Schema.
 * $Id: XmlTTCPresetType.java,v 1.1 2005/06/29 16:33:03 tb Exp $
 */

package rpct.castor;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.*;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * 
 * 
 * @version $Revision: 1.1 $ $Date: 2005/06/29 16:33:03 $
**/
public abstract class XmlTTCPresetType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    private java.lang.String _xmlPresetName;

    private java.lang.String _xmlCSR1;

    private java.lang.String _xmlCSR2;

    private XmlInhibits _xmlInhibits;

    private java.lang.String _xmlTRIGWORDad;

    private java.lang.String _xmlTRIGWORDsad;

    private boolean _xmlTRIGWORDie;

    /**
     * keeps track of state for field: _xmlTRIGWORDie
    **/
    private boolean _has_xmlTRIGWORDie;

    private boolean _xmlTRIGWORDsize;

    /**
     * keeps track of state for field: _xmlTRIGWORDsize
    **/
    private boolean _has_xmlTRIGWORDsize;

    private XmlBGoDataVec _xmlBGoDataVec;

    private XmlBGos _xmlBGos;

    private XmlTTCrxs _xmlTTCrxs;


      //----------------/
     //- Constructors -/
    //----------------/

    public XmlTTCPresetType() {
        super();
    } //-- rpct.castor.XmlTTCPresetType()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'xmlBGoDataVec'.
     * 
     * @return the value of field 'xmlBGoDataVec'.
    **/
    public XmlBGoDataVec getXmlBGoDataVec()
    {
        return this._xmlBGoDataVec;
    } //-- XmlBGoDataVec getXmlBGoDataVec() 

    /**
     * Returns the value of field 'xmlBGos'.
     * 
     * @return the value of field 'xmlBGos'.
    **/
    public XmlBGos getXmlBGos()
    {
        return this._xmlBGos;
    } //-- XmlBGos getXmlBGos() 

    /**
     * Returns the value of field 'xmlCSR1'.
     * 
     * @return the value of field 'xmlCSR1'.
    **/
    public java.lang.String getXmlCSR1()
    {
        return this._xmlCSR1;
    } //-- java.lang.String getXmlCSR1() 

    /**
     * Returns the value of field 'xmlCSR2'.
     * 
     * @return the value of field 'xmlCSR2'.
    **/
    public java.lang.String getXmlCSR2()
    {
        return this._xmlCSR2;
    } //-- java.lang.String getXmlCSR2() 

    /**
     * Returns the value of field 'xmlInhibits'.
     * 
     * @return the value of field 'xmlInhibits'.
    **/
    public XmlInhibits getXmlInhibits()
    {
        return this._xmlInhibits;
    } //-- XmlInhibits getXmlInhibits() 

    /**
     * Returns the value of field 'xmlPresetName'.
     * 
     * @return the value of field 'xmlPresetName'.
    **/
    public java.lang.String getXmlPresetName()
    {
        return this._xmlPresetName;
    } //-- java.lang.String getXmlPresetName() 

    /**
     * Returns the value of field 'xmlTRIGWORDad'.
     * 
     * @return the value of field 'xmlTRIGWORDad'.
    **/
    public java.lang.String getXmlTRIGWORDad()
    {
        return this._xmlTRIGWORDad;
    } //-- java.lang.String getXmlTRIGWORDad() 

    /**
     * Returns the value of field 'xmlTRIGWORDie'.
     * 
     * @return the value of field 'xmlTRIGWORDie'.
    **/
    public boolean getXmlTRIGWORDie()
    {
        return this._xmlTRIGWORDie;
    } //-- boolean getXmlTRIGWORDie() 

    /**
     * Returns the value of field 'xmlTRIGWORDsad'.
     * 
     * @return the value of field 'xmlTRIGWORDsad'.
    **/
    public java.lang.String getXmlTRIGWORDsad()
    {
        return this._xmlTRIGWORDsad;
    } //-- java.lang.String getXmlTRIGWORDsad() 

    /**
     * Returns the value of field 'xmlTRIGWORDsize'.
     * 
     * @return the value of field 'xmlTRIGWORDsize'.
    **/
    public boolean getXmlTRIGWORDsize()
    {
        return this._xmlTRIGWORDsize;
    } //-- boolean getXmlTRIGWORDsize() 

    /**
     * Returns the value of field 'xmlTTCrxs'.
     * 
     * @return the value of field 'xmlTTCrxs'.
    **/
    public XmlTTCrxs getXmlTTCrxs()
    {
        return this._xmlTTCrxs;
    } //-- XmlTTCrxs getXmlTTCrxs() 

    /**
    **/
    public boolean hasXmlTRIGWORDie()
    {
        return this._has_xmlTRIGWORDie;
    } //-- boolean hasXmlTRIGWORDie() 

    /**
    **/
    public boolean hasXmlTRIGWORDsize()
    {
        return this._has_xmlTRIGWORDsize;
    } //-- boolean hasXmlTRIGWORDsize() 

    /**
    **/
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * 
     * 
     * @param out
    **/
    public abstract void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException;

    /**
     * 
     * 
     * @param handler
    **/
    public abstract void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException;

    /**
     * Sets the value of field 'xmlBGoDataVec'.
     * 
     * @param xmlBGoDataVec the value of field 'xmlBGoDataVec'.
    **/
    public void setXmlBGoDataVec(XmlBGoDataVec xmlBGoDataVec)
    {
        this._xmlBGoDataVec = xmlBGoDataVec;
    } //-- void setXmlBGoDataVec(XmlBGoDataVec) 

    /**
     * Sets the value of field 'xmlBGos'.
     * 
     * @param xmlBGos the value of field 'xmlBGos'.
    **/
    public void setXmlBGos(XmlBGos xmlBGos)
    {
        this._xmlBGos = xmlBGos;
    } //-- void setXmlBGos(XmlBGos) 

    /**
     * Sets the value of field 'xmlCSR1'.
     * 
     * @param xmlCSR1 the value of field 'xmlCSR1'.
    **/
    public void setXmlCSR1(java.lang.String xmlCSR1)
    {
        this._xmlCSR1 = xmlCSR1;
    } //-- void setXmlCSR1(java.lang.String) 

    /**
     * Sets the value of field 'xmlCSR2'.
     * 
     * @param xmlCSR2 the value of field 'xmlCSR2'.
    **/
    public void setXmlCSR2(java.lang.String xmlCSR2)
    {
        this._xmlCSR2 = xmlCSR2;
    } //-- void setXmlCSR2(java.lang.String) 

    /**
     * Sets the value of field 'xmlInhibits'.
     * 
     * @param xmlInhibits the value of field 'xmlInhibits'.
    **/
    public void setXmlInhibits(XmlInhibits xmlInhibits)
    {
        this._xmlInhibits = xmlInhibits;
    } //-- void setXmlInhibits(XmlInhibits) 

    /**
     * Sets the value of field 'xmlPresetName'.
     * 
     * @param xmlPresetName the value of field 'xmlPresetName'.
    **/
    public void setXmlPresetName(java.lang.String xmlPresetName)
    {
        this._xmlPresetName = xmlPresetName;
    } //-- void setXmlPresetName(java.lang.String) 

    /**
     * Sets the value of field 'xmlTRIGWORDad'.
     * 
     * @param xmlTRIGWORDad the value of field 'xmlTRIGWORDad'.
    **/
    public void setXmlTRIGWORDad(java.lang.String xmlTRIGWORDad)
    {
        this._xmlTRIGWORDad = xmlTRIGWORDad;
    } //-- void setXmlTRIGWORDad(java.lang.String) 

    /**
     * Sets the value of field 'xmlTRIGWORDie'.
     * 
     * @param xmlTRIGWORDie the value of field 'xmlTRIGWORDie'.
    **/
    public void setXmlTRIGWORDie(boolean xmlTRIGWORDie)
    {
        this._xmlTRIGWORDie = xmlTRIGWORDie;
        this._has_xmlTRIGWORDie = true;
    } //-- void setXmlTRIGWORDie(boolean) 

    /**
     * Sets the value of field 'xmlTRIGWORDsad'.
     * 
     * @param xmlTRIGWORDsad the value of field 'xmlTRIGWORDsad'.
    **/
    public void setXmlTRIGWORDsad(java.lang.String xmlTRIGWORDsad)
    {
        this._xmlTRIGWORDsad = xmlTRIGWORDsad;
    } //-- void setXmlTRIGWORDsad(java.lang.String) 

    /**
     * Sets the value of field 'xmlTRIGWORDsize'.
     * 
     * @param xmlTRIGWORDsize the value of field 'xmlTRIGWORDsize'.
    **/
    public void setXmlTRIGWORDsize(boolean xmlTRIGWORDsize)
    {
        this._xmlTRIGWORDsize = xmlTRIGWORDsize;
        this._has_xmlTRIGWORDsize = true;
    } //-- void setXmlTRIGWORDsize(boolean) 

    /**
     * Sets the value of field 'xmlTTCrxs'.
     * 
     * @param xmlTTCrxs the value of field 'xmlTTCrxs'.
    **/
    public void setXmlTTCrxs(XmlTTCrxs xmlTTCrxs)
    {
        this._xmlTTCrxs = xmlTTCrxs;
    } //-- void setXmlTTCrxs(XmlTTCrxs) 

    /**
    **/
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
