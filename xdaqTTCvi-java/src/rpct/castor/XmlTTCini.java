/*
 * This class was automatically generated with 
 * <a href="http://castor.exolab.org">Castor 0.9.4</a>, using an
 * XML Schema.
 * $Id: XmlTTCini.java,v 1.1 2005/06/29 16:33:03 tb Exp $
 */

package rpct.castor;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.*;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * 
 * 
 * @version $Revision: 1.1 $ $Date: 2005/06/29 16:33:03 $
**/
public class XmlTTCini implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    private java.lang.String _xmlUrl;

    private java.lang.String _xmlTargetAddr;

    private XmlTTCPresets _xmlTTCPresets;


      //----------------/
     //- Constructors -/
    //----------------/

    public XmlTTCini() {
        super();
    } //-- rpct.castor.XmlTTCini()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'xmlTTCPresets'.
     * 
     * @return the value of field 'xmlTTCPresets'.
    **/
    public XmlTTCPresets getXmlTTCPresets()
    {
        return this._xmlTTCPresets;
    } //-- XmlTTCPresets getXmlTTCPresets() 

    /**
     * Returns the value of field 'xmlTargetAddr'.
     * 
     * @return the value of field 'xmlTargetAddr'.
    **/
    public java.lang.String getXmlTargetAddr()
    {
        return this._xmlTargetAddr;
    } //-- java.lang.String getXmlTargetAddr() 

    /**
     * Returns the value of field 'xmlUrl'.
     * 
     * @return the value of field 'xmlUrl'.
    **/
    public java.lang.String getXmlUrl()
    {
        return this._xmlUrl;
    } //-- java.lang.String getXmlUrl() 

    /**
    **/
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * 
     * 
     * @param out
    **/
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * 
     * 
     * @param handler
    **/
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'xmlTTCPresets'.
     * 
     * @param xmlTTCPresets the value of field 'xmlTTCPresets'.
    **/
    public void setXmlTTCPresets(XmlTTCPresets xmlTTCPresets)
    {
        this._xmlTTCPresets = xmlTTCPresets;
    } //-- void setXmlTTCPresets(XmlTTCPresets) 

    /**
     * Sets the value of field 'xmlTargetAddr'.
     * 
     * @param xmlTargetAddr the value of field 'xmlTargetAddr'.
    **/
    public void setXmlTargetAddr(java.lang.String xmlTargetAddr)
    {
        this._xmlTargetAddr = xmlTargetAddr;
    } //-- void setXmlTargetAddr(java.lang.String) 

    /**
     * Sets the value of field 'xmlUrl'.
     * 
     * @param xmlUrl the value of field 'xmlUrl'.
    **/
    public void setXmlUrl(java.lang.String xmlUrl)
    {
        this._xmlUrl = xmlUrl;
    } //-- void setXmlUrl(java.lang.String) 

    /**
     * 
     * 
     * @param reader
    **/
    public static rpct.castor.XmlTTCini unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (rpct.castor.XmlTTCini) Unmarshaller.unmarshal(rpct.castor.XmlTTCini.class, reader);
    } //-- rpct.castor.XmlTTCini unmarshal(java.io.Reader) 

    /**
    **/
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
