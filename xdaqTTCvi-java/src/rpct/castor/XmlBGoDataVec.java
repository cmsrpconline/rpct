/*
 * This class was automatically generated with 
 * <a href="http://castor.exolab.org">Castor 0.9.4</a>, using an
 * XML Schema.
 * $Id: XmlBGoDataVec.java,v 1.1 2005/06/29 16:33:03 tb Exp $
 */

package rpct.castor;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.*;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * 
 * 
 * @version $Revision: 1.1 $ $Date: 2005/06/29 16:33:03 $
**/
public class XmlBGoDataVec implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    private java.util.Vector _xmlBGoDataList;


      //----------------/
     //- Constructors -/
    //----------------/

    public XmlBGoDataVec() {
        super();
        _xmlBGoDataList = new Vector();
    } //-- rpct.castor.XmlBGoDataVec()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vXmlBGoData
    **/
    public void addXmlBGoData(XmlBGoData vXmlBGoData)
        throws java.lang.IndexOutOfBoundsException
    {
        _xmlBGoDataList.addElement(vXmlBGoData);
    } //-- void addXmlBGoData(XmlBGoData) 

    /**
     * 
     * 
     * @param index
     * @param vXmlBGoData
    **/
    public void addXmlBGoData(int index, XmlBGoData vXmlBGoData)
        throws java.lang.IndexOutOfBoundsException
    {
        _xmlBGoDataList.insertElementAt(vXmlBGoData, index);
    } //-- void addXmlBGoData(int, XmlBGoData) 

    /**
    **/
    public java.util.Enumeration enumerateXmlBGoData()
    {
        return _xmlBGoDataList.elements();
    } //-- java.util.Enumeration enumerateXmlBGoData() 

    /**
     * 
     * 
     * @param index
    **/
    public XmlBGoData getXmlBGoData(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _xmlBGoDataList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (XmlBGoData) _xmlBGoDataList.elementAt(index);
    } //-- XmlBGoData getXmlBGoData(int) 

    /**
    **/
    public XmlBGoData[] getXmlBGoData()
    {
        int size = _xmlBGoDataList.size();
        XmlBGoData[] mArray = new XmlBGoData[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (XmlBGoData) _xmlBGoDataList.elementAt(index);
        }
        return mArray;
    } //-- XmlBGoData[] getXmlBGoData() 

    /**
    **/
    public int getXmlBGoDataCount()
    {
        return _xmlBGoDataList.size();
    } //-- int getXmlBGoDataCount() 

    /**
    **/
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * 
     * 
     * @param out
    **/
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * 
     * 
     * @param handler
    **/
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
    **/
    public void removeAllXmlBGoData()
    {
        _xmlBGoDataList.removeAllElements();
    } //-- void removeAllXmlBGoData() 

    /**
     * 
     * 
     * @param index
    **/
    public XmlBGoData removeXmlBGoData(int index)
    {
        java.lang.Object obj = _xmlBGoDataList.elementAt(index);
        _xmlBGoDataList.removeElementAt(index);
        return (XmlBGoData) obj;
    } //-- XmlBGoData removeXmlBGoData(int) 

    /**
     * 
     * 
     * @param index
     * @param vXmlBGoData
    **/
    public void setXmlBGoData(int index, XmlBGoData vXmlBGoData)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _xmlBGoDataList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _xmlBGoDataList.setElementAt(vXmlBGoData, index);
    } //-- void setXmlBGoData(int, XmlBGoData) 

    /**
     * 
     * 
     * @param xmlBGoDataArray
    **/
    public void setXmlBGoData(XmlBGoData[] xmlBGoDataArray)
    {
        //-- copy array
        _xmlBGoDataList.removeAllElements();
        for (int i = 0; i < xmlBGoDataArray.length; i++) {
            _xmlBGoDataList.addElement(xmlBGoDataArray[i]);
        }
    } //-- void setXmlBGoData(XmlBGoData) 

    /**
     * 
     * 
     * @param reader
    **/
    public static rpct.castor.XmlBGoDataVec unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (rpct.castor.XmlBGoDataVec) Unmarshaller.unmarshal(rpct.castor.XmlBGoDataVec.class, reader);
    } //-- rpct.castor.XmlBGoDataVec unmarshal(java.io.Reader) 

    /**
    **/
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
