/*
 * This class was automatically generated with 
 * <a href="http://castor.exolab.org">Castor 0.9.4</a>, using an
 * XML Schema.
 * $Id: XmlBGoDataType.java,v 1.1 2005/06/29 16:33:03 tb Exp $
 */

package rpct.castor;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.*;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * 
 * 
 * @version $Revision: 1.1 $ $Date: 2005/06/29 16:33:03 $
**/
public abstract class XmlBGoDataType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    private java.lang.String _xmlName;

    private XmlBGoCyclesVec _xmlBGoCyclesVec;


      //----------------/
     //- Constructors -/
    //----------------/

    public XmlBGoDataType() {
        super();
    } //-- rpct.castor.XmlBGoDataType()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'xmlBGoCyclesVec'.
     * 
     * @return the value of field 'xmlBGoCyclesVec'.
    **/
    public XmlBGoCyclesVec getXmlBGoCyclesVec()
    {
        return this._xmlBGoCyclesVec;
    } //-- XmlBGoCyclesVec getXmlBGoCyclesVec() 

    /**
     * Returns the value of field 'xmlName'.
     * 
     * @return the value of field 'xmlName'.
    **/
    public java.lang.String getXmlName()
    {
        return this._xmlName;
    } //-- java.lang.String getXmlName() 

    /**
    **/
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * 
     * 
     * @param out
    **/
    public abstract void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException;

    /**
     * 
     * 
     * @param handler
    **/
    public abstract void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException;

    /**
     * Sets the value of field 'xmlBGoCyclesVec'.
     * 
     * @param xmlBGoCyclesVec the value of field 'xmlBGoCyclesVec'.
    **/
    public void setXmlBGoCyclesVec(XmlBGoCyclesVec xmlBGoCyclesVec)
    {
        this._xmlBGoCyclesVec = xmlBGoCyclesVec;
    } //-- void setXmlBGoCyclesVec(XmlBGoCyclesVec) 

    /**
     * Sets the value of field 'xmlName'.
     * 
     * @param xmlName the value of field 'xmlName'.
    **/
    public void setXmlName(java.lang.String xmlName)
    {
        this._xmlName = xmlName;
    } //-- void setXmlName(java.lang.String) 

    /**
    **/
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
