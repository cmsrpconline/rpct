//---------------------------------------------------------------------------

//
//  Michal Pietrusinski 2007
//  Warsaw University
//

#ifndef RPCT_MEMORYMAP_H
#define RPCT_MEMORYMAP_H

#include "rpct/ii/VComponent.h"
#include "rpct/ii/IDevice.h"
#include "rpct/ii/IHardwareIfc.h"
#include <list>


namespace rpct {



class MemoryMap {
public:
	virtual ~MemoryMap() {		
	}

    typedef std::list<TVIIItemDecl> TDeclItems;
    static const int npos = IDevice::npos;

    // word operations
    virtual uint32_t readWord(int id, int idx) = 0;
    virtual void readWord(int id, int idx, void* data) = 0;

    virtual void writeWord(int id, int idx, uint32_t data) = 0;
    virtual void writeWord(int* id, int* idx, uint32_t* data, size_t count) = 0;
    virtual void writeWord(int id, int idx, void* data) = 0;

    // area operations
    virtual void writeArea(int id, void* data, int start_idx = 0, int n = npos) = 0;
    virtual void readArea(int id, void* data, int start_idx = 0, int n = npos) = 0;

    virtual void writeArea(int id, uint32_t* data, int start_idx = 0, int n = npos) = 0;
    virtual void readArea(int id, uint32_t* data, int start_idx = 0, int n = npos) = 0;

    //vector operations
    virtual uint32_t readVector(int id) = 0;
    virtual void readVector(int id, void* data) = 0;

    virtual void writeVector(int id, uint32_t data) = 0;
    virtual void writeVector(int* id, uint32_t* data, size_t count) = 0;
    virtual void writeVector(int id, void* data) = 0;

    // bit operations
    virtual uint32_t readBits(int id) = 0;
    virtual void writeBits(int id, uint32_t data, bool preread = true) = 0;

    virtual uint32_t getBitsInVector(int bits_id, uint32_t vector_data) = 0;
    virtual void setBitsInVector(int bits_id, uint32_t bits_data, uint32_t& vector_data) = 0;

    virtual uint32_t getBitsInVector(int bits_id, void* vector_data) = 0;
    virtual void setBitsInVector(int bits_id, uint32_t bits_data, void* vector_data) = 0;


    // interfacing with the hardware
    virtual void SetHardwareIfc(IHardwareIfc* ifc, bool owner) = 0;
    virtual IHardwareIfc* GetHardwareIfc() = 0;
    
    virtual bool IsHardwareIfcOwner() = 0;
    virtual void SetHardwareIfcOwner(bool owner) = 0;

    typedef IDevice::TItemsMap TItemsMap;
    virtual const TItemsMap* GetItems() = 0;

    typedef IDevice::TID TID;

    typedef IDevice::TItemDesc TItemDesc;
    virtual const TItemDesc& getItemDesc(TID) = 0;


    struct TItemInfo {
        // declaration information
        TVIIItemType Type;
        int ID;
        std::string Desciption;
        int Width;
        int Number;
        int ParentID;
        TVIIItemWrType WrType;
        TVIIItemRdType RdType;

        // layout information
        TVI Page;
        TVI WrPos;
        TVI RdPos;
        TVI AddrPos;
        int AddrLen;
    };
    virtual const TItemInfo& GetItemInfo(TID) const = 0;

    virtual int GetDataSize() = 0;
    virtual uint32_t GetChecksum() = 0;
};

}

#endif
