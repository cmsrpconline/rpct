/*
 * ISynCoderConfInFlash.h
 *
 *  Created on: Feb 3, 2012
 *      Author: Karol Bunkowski
 */

#ifndef ISYNCODERCONFINFLASH_H_
#define ISYNCODERCONFINFLASH_H_
#include "xdata/Integer.h"

namespace rpct {

class ISynCoderConfInFlash {
public:
	virtual ~ISynCoderConfInFlash() {};
	virtual int getChipId() = 0;
	virtual int getSynCoderConfId() = 0;
	virtual int getBeginAddress() = 0;
	virtual int getEndAddress() = 0;
	virtual void setChipId(xdata::Integer& chipId) = 0;
	virtual void setSynCoderConfId(xdata::Integer& synCoderConfId) = 0;
	virtual void setBeginAddress(xdata::Integer& beginAddress) = 0;
	virtual void setEndAddress(xdata::Integer& endAddress) = 0;
	virtual void setChipId(xdata::Integer chipId) = 0;
	virtual void setSynCoderConfId(xdata::Integer synCoderConfId) = 0;
	virtual void setBeginAddress(xdata::Integer beginAddress) = 0;
	virtual void setEndAddress(xdata::Integer endAddress) = 0;
};
}
#endif /* ISYNCODERCONFINFLASH_H_ */
