/** Filip Thyssen */

#ifndef rpct_ii_const_h_
#define rpct_ii_const_h_

namespace rpct {

namespace publish {
namespace type {
const int unknown_ = 0;
const int error_   = 1;
const int state_   = 2;

const int log_   = 3;
const int unlog_ = 4;

namespace feb {
const int temp_ = 11;
const int vth_  = 12;
const int vmon_ = 13;
const int vth_offset_  = 21;
const int vmon_offset_ = 22;

const int vth_set_  = 31;
const int vmon_set_ = 32;
const int vth_offset_set_  = 33;
const int vmon_offset_set_ = 34;
} // namespace feb
} // namespace type
} // namespace publish

} // namespace rpct

#endif // rpct_ii_const_h_
