/** Filip Thyssen */

#ifndef rpct_ii_FebChipLocationId_h_
#define rpct_ii_FebChipLocationId_h_

#include "rpct/tools/LocationId.h"

namespace rpct {

namespace febchiplocationid {

extern const int nfields_;
extern const int pos_[];
extern const int mask_[];
extern const int offset_[];

} // namespace febchiplocationid

class FebChipLocationId :
        public rpct::tools::LocationId<9, febchiplocationid::pos_, febchiplocationid::mask_, febchiplocationid::offset_>
{
public:
    FebChipLocationId(uint32_t id = 0x0);

    int getBoE() const;
    int getWheel() const;
    int getDisc() const;
    int getLayer() const;
    int getRing() const;
    int getSector() const;
    int getSubsector() const;
    int getRoll() const;
    int getPartition() const;
    int getChannel() const;
    int getAddress() const;
    int getPosition() const;

    void setBoE(int val = tools::locationid::wildcard_);
    void setWheel(int val = tools::locationid::wildcard_);
    void setDisc(int val = tools::locationid::wildcard_);
    void setLayer(int val = tools::locationid::wildcard_);
    void setRing(int val = tools::locationid::wildcard_);
    void setSector(int val = tools::locationid::wildcard_);
    void setSubsector(int val = tools::locationid::wildcard_);
    void setRoll(int val = tools::locationid::wildcard_);
    void setPartition(int val = tools::locationid::wildcard_);
    void setChannel(int val = tools::locationid::wildcard_);
    void setAddress(int val = tools::locationid::wildcard_);
    void setPosition(int val = tools::locationid::wildcard_);

    std::string getName() const;
public:
    enum Field {
        fields_boe_ = 0     // :(0),   (1)
        , fields_wheel_     // :(-2-2),(-4-4)
        , fields_layer_     // :(1-6), (1-3)
        , fields_sector_    // :(1-12),(1-36)
        , fields_subsector_ // :(0/1-6), (0), wildcard=0=none
        , fields_roll_      // :(1-3), (1-4)
        , fields_channel_   // :(1-8)
        , fields_address_   // :(0-7), (0-3)
        , fields_position_  // :(0-3)
    };

    static const char * boe_[4];
    static const char * layer_[4][8];
    static const char * subsector_[4][8];
    static const char * roll_[4][8];
};

} // namespace rpct

#include "rpct/ii/FebChipLocationId-inl.h"

#endif // rpct_ii_FebChipLocationId_h_
