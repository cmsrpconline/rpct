#ifndef IHARDWAREIFC_H
#define IHARDWAREIFC_H


//#include "tb_std.h"
#include <cstring>
#include <stdint.h>

namespace rpct { 
    
    
class IHardwareIfc {
public:
    virtual ~IHardwareIfc() {}
    
    virtual void Write(uint32_t address, void* data,
                       std::size_t word_count, std::size_t word_size) = 0;

    virtual void Write(uint32_t* addresses, void** data,
    		std::size_t* word_counts, std::size_t word_size, std::size_t count) = 0;

    virtual void Read(uint32_t address, void* data,
    		std::size_t word_count, std::size_t word_size) = 0;
};

}
#endif
