// Borland C++ Builder
// Copyright (c) 1995, 1999 by Borland International
// All rights reserved

// (DO NOT EDIT: machine generated header) 'std_logic_1164.pas' rev: 5.00

#ifndef std_logic_1164HPP
#define std_logic_1164HPP

#pragma delphiheader begin
#pragma option push -w-
#pragma option push -Vx
#include <SysInit.hpp>	// Pascal unit
#include <System.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Std_logic_1164
{
//-- type declarations -------------------------------------------------------
typedef char std_logic;

typedef DynamicArray<char >  std_logic_vector;

//-- var, const, procedure ---------------------------------------------------

}	/* namespace Std_logic_1164 */
#if !defined(NO_IMPLICIT_NAMESPACE_USE)
using namespace Std_logic_1164;
#endif
#pragma option pop	// -w-
#pragma option pop	// -Vx

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// std_logic_1164
