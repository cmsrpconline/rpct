#ifndef _RPCTCONFIGURATIONSET_H_
#define _RPCTCONFIGURATIONSET_H_

#include "rpct/ii/DeviceSettings.h"
#include "rpct/ii/IDevice.h"
#include "rpct/ii/ISynCoderConfInFlash.h"

namespace rpct {

class ConfigurationSet {
public:
    virtual ~ConfigurationSet() {
    }

    virtual unsigned int getId() = 0;

    virtual DeviceSettings* getDeviceSettings(IDevice& device) = 0;

    virtual DeviceSettings* getDeviceSettings(int id) = 0;

    virtual ISynCoderConfInFlash* getSynCoderConfInFlash(int synCoderId) = 0;
};

}

#endif /*_RPCTFEBSTATEINFO_H_*/
