/** Filip Thyssen */

#ifndef rpct_ii_feb_const_h_
#define rpct_ii_feb_const_h_

#include <stdint.h>

namespace rpct {

namespace type {
namespace feb {

extern const char * febchip_;
extern const char * febboard_;
extern const char * febpart_;
extern const char * febdistributionboard_;
extern const char * febaccesspoint_;
extern const char * rpcfebaccesspoint_;
extern const char * dtfebaccesspoint_;
extern const char * febsystem_;
extern const char * rpcfebsystem_;
extern const char * dtfebsystem_;

extern const char * febmonitorunit_;

} // namespace feb
} // namespace type

namespace preset {
namespace feb {

extern const float unit_;
extern const float inv_unit_;

extern const uint16_t vth_;
extern const uint16_t vmon_;
// maximum difference between two values to be written at once
extern const uint16_t vth_max_diff_;
extern const uint16_t vmon_max_diff_;

// maximum difference between requested and desired value
extern const uint16_t vth_max_offset_;
extern const uint16_t vmon_max_offset_;

extern const uint16_t vth_max_offset_error_;
extern const uint16_t vmon_max_offset_error_;

extern const uint16_t vth_max_offset_offset_;
extern const uint16_t vmon_max_offset_offset_;

extern const int16_t offset_keep_;
extern const int16_t offset_;

extern const unsigned int nchips_;
extern const unsigned int nparts_[];

extern const float min_temp_;
extern const float max_temp_;

} // namespace feb
} // namepace preset

namespace monitoritem {
namespace feb {

extern const char * pca9544_;
extern const char * pca9544_dt_;
extern const char * pcf8574a_;
extern const char * vth_;
extern const char * vth_offset_;
extern const char * vmon_;
extern const char * vmon_offset_;
extern const char * temp_;

} // namespace feb
} // namespace preset

} // namespace rpct

#endif // rpct_ii_feb_const_h_
