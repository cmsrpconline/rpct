/** Filip Thyssen */

#ifndef rpct_ii_i2c_Register_inl_h_
#define rpct_ii_i2c_Register_inl_h_

#include <string.h>

#include "rpct/ii/i2c/Register.h"

namespace rpct {
namespace i2c {

template<unsigned char size_out_, unsigned char default_out_>
inline TRRegister<size_out_, default_out_>::TRRegister()
{
    reset_out();
}
template<unsigned char size_out_, unsigned char default_out_>
inline unsigned char TRRegister<size_out_, default_out_>::size_out() const
{
    return size_out_;
}
template<unsigned char size_out_, unsigned char default_out_>
inline const unsigned char * TRRegister<size_out_, default_out_>::out() const
{
    return out_;
}
template<unsigned char size_out_, unsigned char default_out_>
inline unsigned char * TRRegister<size_out_, default_out_>::out()
{
    return out_;
}
template<unsigned char size_out_, unsigned char default_out_>
inline void TRRegister<size_out_, default_out_>::reset_out()
{
    memset(out_, default_out_, size_out_);
}

template<unsigned char size_in_, unsigned char default_in_>
inline TWRegister<size_in_, default_in_>::TWRegister()
{
    reset_in();
}
template<unsigned char size_in_, unsigned char default_in_>
inline unsigned char TWRegister<size_in_, default_in_>::size_in() const
{
    return size_in_;
}
template<unsigned char size_in_, unsigned char default_in_>
inline const unsigned char * TWRegister<size_in_, default_in_>::in() const
{
    return in_;
}
template<unsigned char size_in_, unsigned char default_in_>
inline unsigned char * TWRegister<size_in_, default_in_>::in()
{
    return in_;
}
template<unsigned char size_in_, unsigned char default_in_>
inline void TWRegister<size_in_, default_in_>::reset_in()
{
    memset(in_, default_in_, size_in_);
}

template<unsigned char size_in_, unsigned char default_in_, unsigned char size_out_, unsigned char default_out_>
inline TRegister<size_in_, default_in_, size_out_, default_out_>::TRegister()
    : TRRegister<size_out_, default_out_>::TRRegister()
    , TWRegister<size_in_, default_in_>::TWRegister()
{}

} // namespace i2c
} // namespace rpct

#endif // rpct_ii_i2c_Register_inl_h_
