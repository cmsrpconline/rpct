/** Filip Thyssen */

#ifndef rpct_ii_i2c_const_h_
#define rpct_ii_i2c_const_h_

namespace rpct {
namespace i2c {

namespace preset {
extern unsigned short delay_nseconds_;
} // namespace preset

} // namespace i2c
} // namespace rpct

#endif // rpct_ii_i2c_const_h_
