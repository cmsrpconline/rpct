/** Filip Thyssen */

#ifndef rpct_ii_i2c_I2cResponse_h_
#define rpct_ii_i2c_I2cResponse_h_

#include "rpct/ii/exception/Exception.h"

namespace rpct {
namespace i2c {

class II2cResponse
{
public:
    virtual ~II2cResponse();

    virtual void i2c_ready() const throw ();
    virtual void i2c_error(rpct::exception::Exception const * e = 0) const throw ();
};
template<typename T>
class TI2cResponse : public II2cResponse
{
public:
    TI2cResponse(T * obj = 0
                 , void (T::* ready)() throw () = 0
                 , void (T::* error)(rpct::exception::Exception const *) throw () = 0);
    void i2c_ready() const throw();
    void i2c_error(rpct::exception::Exception const * e = 0) const throw();

    void setI2cResponse(T * obj = 0
                        , void (T::* ready)() throw () = 0
                        , void (T::* error)(rpct::exception::Exception const *) throw () = 0);
    void setObject(T * obj = 0);
    void setI2cReady(void (T::* ready)() throw () = 0);
    void setI2cError(void (T::* error)(rpct::exception::Exception const *) throw () = 0);
protected:
    T * obj_;
    void (T::* ready_)() throw ();
    void (T::* error_)(rpct::exception::Exception const *) throw ();
};

} // namespace i2c
} // namespace rpct

#include "rpct/ii/i2c/I2cResponse-inl.h"

#endif // rpct_ii_i2c_I2cResponse_h_
