/** Filip Thyssen */

#ifndef rpct_ii_i2c_II2cAccess_h_
#define rpct_ii_i2c_II2cAccess_h_

#include "rpct/ii/exception/Exception.h"
#include "rpct/ii/i2c/const.h"

namespace rpct {
namespace i2c {

class II2cResponse;
class II2cRRegister;
class II2cWRegister;
class IRRegister;
class IWRegister;

class II2cAccess
{
protected:
    virtual void idelay(unsigned int nseconds)
        throw (rpct::exception::Exception) = 0;
    virtual void iread(unsigned short channel
                       , unsigned char address
                       , unsigned char * data = 0
                       , unsigned char count = 1
                       , II2cResponse * i2c_response = 0)
        throw (rpct::exception::Exception) = 0;
    virtual void iwrite(unsigned short channel
                        , unsigned char address
                        , unsigned char const * data
                        , unsigned char count = 1
                        , II2cResponse * i2c_response = 0)
        throw (rpct::exception::Exception) = 0;
public:
    virtual ~II2cAccess();

    virtual void log(rpct::exception::Exception const * e) const;

    virtual void init() throw (rpct::exception::Exception);
    virtual void reset();
    virtual void execute() throw (rpct::exception::Exception);

    virtual void enable() throw (rpct::exception::Exception);
    virtual void disable() throw (rpct::exception::Exception);

    // prescaler
    virtual void setPrescaler(unsigned short prescaler);
    virtual unsigned short getPrescaler() const;
    virtual void writePrescaler() throw (rpct::exception::Exception);

    // groups
    virtual bool startGroup();
    virtual void cancelGroup();
    virtual void endGroup();
    virtual bool groupMode() const;

    // read/write
    virtual bool delay(unsigned int nseconds = preset::delay_nseconds_);
    virtual bool read(unsigned short channel
                      , unsigned char address
                      , II2cRRegister * i2c_rregister);
    virtual bool read(unsigned short channel
                      , unsigned char address
                      , IRRegister * rregister
                      , II2cResponse * i2c_response = 0);
    virtual bool read(unsigned short channel
                      , unsigned char address
                      , unsigned char * data = 0
                      , unsigned char count = 1
                      , II2cResponse * i2c_response = 0);
    virtual bool write(unsigned short channel
                       , unsigned char address
                       , II2cWRegister * i2c_wregister);
    virtual bool write(unsigned short channel
                       , unsigned char address
                       , IWRegister * wregister
                       , II2cResponse * i2c_response = 0);
    virtual bool write(unsigned short channel
                       , unsigned char address
                       , unsigned char const * data
                       , unsigned char count = 1
                       , II2cResponse * i2c_response = 0);
};
class EnsureGroup
{
public:
    EnsureGroup(II2cAccess * i2c_access, bool end = false);
    void end();
    ~EnsureGroup();
protected:
    II2cAccess * i2c_access_;
    bool started_;
    bool end_;
};

} // namespace i2c
} // namespace rpct

#include "rpct/ii/i2c/II2cAccess-inl.h"

#endif // rpct_ii_i2c_II2cAccess_h_
