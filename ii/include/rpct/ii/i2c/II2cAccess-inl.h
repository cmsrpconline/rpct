/** Filip Thyssen */

#ifndef rpct_ii_i2c_II2cAccess_inl_h_
#define rpct_ii_i2c_II2cAccess_inl_h_

#include "rpct/ii/i2c/II2cAccess.h"

namespace rpct {
namespace i2c {

inline void II2cAccess::log(rpct::exception::Exception const * e) const
{}

inline void II2cAccess::init() throw (rpct::exception::Exception)
{}
inline void II2cAccess::reset()
{}
inline void II2cAccess::execute() throw (rpct::exception::Exception)
{}
inline void II2cAccess::enable()
    throw (rpct::exception::Exception)
{}
inline void II2cAccess::disable()
    throw (rpct::exception::Exception)
{}
inline void II2cAccess::setPrescaler(unsigned short prescaler)
{}
inline unsigned short II2cAccess::getPrescaler() const
{
    return 0;
}
inline void II2cAccess::writePrescaler()
    throw (rpct::exception::Exception)
{}

inline bool II2cAccess::startGroup()
{
    return false;
}
inline void II2cAccess::cancelGroup()
{}
inline void II2cAccess::endGroup()
{}

inline bool II2cAccess::groupMode() const
{
    return false;
}

inline EnsureGroup::EnsureGroup(II2cAccess * i2c_access, bool end)
    : i2c_access_(i2c_access)
    , started_(false)
    , end_(end)
{
    if (i2c_access_)
        started_ = i2c_access_->startGroup();
}
inline void EnsureGroup::end()
{
    end_ = true;
}
inline EnsureGroup::~EnsureGroup()
{
    if (i2c_access_ && started_)
        {
            if (end_)
                i2c_access_->endGroup();
            else
                i2c_access_->cancelGroup();
        }
}

} // namespace i2c
} // namespace rpct

#endif // rpct_ii_i2c_II2cAccess_inl_h_

