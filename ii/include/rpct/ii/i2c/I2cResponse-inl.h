/** Filip Thyssen */

#ifndef rpct_ii_i2c_I2cResponse_inl_h_
#define rpct_ii_i2c_I2cResponse_inl_h_

#include "rpct/ii/i2c/I2cResponse.h"

namespace rpct {
namespace i2c {

inline II2cResponse::~II2cResponse()
{}
inline void II2cResponse::i2c_ready() const throw ()
{}
inline void II2cResponse::i2c_error(rpct::exception::Exception const * e) const throw ()
{}

template<typename T>
inline TI2cResponse<T>::TI2cResponse(T * obj
                                     , void (T::* ready)() throw ()
                                     , void (T::* error)(rpct::exception::Exception const *) throw ())
    : obj_(obj)
    , ready_(ready)
    , error_(error)
{}
template<typename T>
inline void TI2cResponse<T>::i2c_ready() const throw()
{
    if (obj_ && ready_)
        (obj_->*ready_)();
}
template<typename T>
inline void TI2cResponse<T>::i2c_error(rpct::exception::Exception const * e) const throw()
{
    if (obj_ && error_)
        (obj_->*error_)(e);
}
template<typename T>
inline void TI2cResponse<T>::setI2cResponse(T * obj
                                            , void (T::* ready)() throw ()
                                            , void (T::* error)(rpct::exception::Exception const *) throw ())
{
    obj_ = obj;
    ready_ = ready;
    error_ = error;
}
template<typename T>
inline void TI2cResponse<T>::setObject(T * obj)
{
    obj_ = obj;
}
template<typename T>
inline void TI2cResponse<T>::setI2cReady(void (T::* ready)() throw ())
{
    ready_ = ready;
}
template<typename T>
inline void TI2cResponse<T>::setI2cError(void (T::* error)(rpct::exception::Exception const *) throw ())
{
    error_ = error;
}

} // namespace i2c
} // namespace rpct

#endif // rpct_ii_i2c_I2cResponse_inl_h_
