/** Filip Thyssen */

#ifndef rpct_ii_i2c_Register_h_
#define rpct_ii_i2c_Register_h_

namespace rpct {
namespace i2c {

class IRRegister
{
public:
    virtual ~IRRegister();
    virtual unsigned char size_out() const = 0;
    virtual const unsigned char * out() const = 0;
    virtual unsigned char * out() = 0;

    virtual void out(const unsigned char * _out);

    virtual void reset_out() = 0;
};
class IWRegister
{
public:
    virtual ~IWRegister();
    virtual unsigned char size_in() const = 0;
    virtual const unsigned char * in() const = 0;
    virtual unsigned char * in() = 0;

    virtual void in(const unsigned char * _in);

    virtual void reset_in() = 0;
};
class IRegister
    : public virtual IRRegister
    , public virtual IWRegister
{
public:
    virtual ~IRegister();
    virtual void reset();
};

template<unsigned char size_out_ = 1, unsigned char default_out_ = 0x00>
class TRRegister : public virtual IRRegister
{
public:
    TRRegister();

    unsigned char size_out() const;
    const unsigned char * out() const;
    unsigned char * out();

    void reset_out();
protected:
    unsigned char out_[size_out_];
};
template<unsigned char size_in_ = 1, unsigned char default_in_ = 0x00>
class TWRegister : public virtual IWRegister
{
public:
    TWRegister();

    unsigned char size_in() const;
    const unsigned char * in() const;
    unsigned char * in();

    void reset_in();
protected:
    unsigned char in_[size_in_];
};
template<unsigned char size_in_ = 1, unsigned char default_in_ = 0x00, unsigned char size_out_ = size_in_, unsigned char default_out_ = default_in_>
class TRegister
    : public virtual IRegister
    , public TRRegister<size_out_, default_out_>
    , public TWRegister<size_in_, default_in_>
{
public:
    TRegister();
};

} // namespace i2c
} // namespace rpct

#include "rpct/ii/i2c/Register-inl.h"

#endif // rpct_ii_i2c_Register_h_
