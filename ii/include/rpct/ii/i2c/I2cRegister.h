/** Filip Thyssen */

#ifndef rpct_ii_i2c_I2cRegister_h_
#define rpct_ii_i2c_I2cRegister_h_

#include "rpct/ii/i2c/Register.h"
#include "rpct/ii/i2c/I2cResponse.h"

namespace rpct {
namespace i2c {

class II2cRRegister
    : public virtual II2cResponse
    , public virtual IRRegister
{
public:
    virtual ~II2cRRegister();
};
class II2cWRegister
    : public virtual II2cResponse
    , public virtual IWRegister
{
public:
    virtual ~II2cWRegister();
};
class II2cRegister
    : public virtual IRegister
    , public virtual II2cRRegister
    , public virtual II2cWRegister
{
public:
    virtual ~II2cRegister();
};

} // namespace i2c
} // namespace rpct

#endif // rpct_ii_i2c_I2cRegister_h_
