#ifndef RPCT_II_MONITORABLE_H
#define RPCT_II_MONITORABLE_H

#include <list>
#include <string>
#include "IHardwareItem.h"
#include "rpct/std/TException.h"
#include <stdint.h>

namespace rpct {

class MonitorItemName {
public:
	static const char* CONTROL_BUS;
	static const char* FIRMWARE;
	static const char* QPLL;
	static const char* TTCRX;
	static const char* GOL;
	static const char* RECEIVER;
	static const char* OPT_LINK;
	static const char* CONFIG_LOST; //means also "was reset"
	static const char* RMB_DATA;
	static const char* RMB_ALGORITHM;
	static const char* RBC;
	static const char* CCU_RING;
	static const char* VME;
	static const char* DB_SERVICE;
	static const char* EXCEPTION;
	static const char* RATE;
	static const char* WIN_FULL_RATIO;
	static const char* MESSAGE;
	static const char* RELOAD_DETECTED;
	static const char* HISTOGRAMS_HALTED;
};

typedef std::string MonitorItem;

/*class MonErrorType {
public:
	static const char* TRANSIENT;
	static const char* PERSISTENT;
};*/

class MonitorableStatus {
public:
    const static int OK = 0;
    const static int SOFT_WARNING = 1;
    const static int WARNING = 2;
    const static int ERROR = 3;
    
    static const char* STATUS_STRINGS[];
private:


public:
    MonitorItem monitorItem;
    uint32_t value;
    int level;
    std::string message;
    //std::string type;

    MonitorableStatus(MonitorItem monitorItem, int level, std::string message,
            uint32_t value) :
            	monitorItem(monitorItem), value(value), level(level), message(message) {
    };

/*    MonitorableStatus(MonitorItem monitorItem, int level, std::string message,
            uint32_t value, std::string type) :
            	monitorItem(monitorItem), value(value), level(level), message(message), type(type) {
    };*/

    static  std::string getStatusStr(int level);

    std::string getStatusStr() {
    	return getStatusStr(level);
    }

};

class IMonitorable : virtual public  IHardwareItem
{
public:
	typedef std::list<MonitorItem> MonitorItems;

    virtual void monitor(volatile bool *stop) = 0;
    //virtual void monitor(MonitorItems & items) = 0;
    //virtual const MonitorItems& getMonitorItems() = 0;
};

class Monitorable : virtual public IMonitorable {
public:
    typedef std::list<MonitorableStatus> MonitorStatusList;

protected:
    MonitorItems monitorItems_;
    MonitorStatusList warningStatusList_;
public:
    virtual const MonitorItems& getMonitorItems() {
    	return (monitorItems_);
    }

    virtual MonitorStatusList& getWarningStatusList() {
    	return (warningStatusList_);
    }

    virtual void addWarning(MonitorableStatus monitorStatus) {
    	warningStatusList_.push_back(monitorStatus);
    }

    virtual bool checkWarningExist(MonitorItem monitorItem);
};

/*
 class MonitorableException : public TException {
 private:
 Monitorable& monitorable_;
 Monitorable::MonitorItem item_;
 std::string error_;
 public:
 MonitorableException(Monitorable& monitorable, const std::string item, const std::string& error) throw() :
 TException("MonitorableException: " + monitorable.getDescription() + ", item: " + item + ", error: "+ error),
 monitorable_(monitorable), item_(item), error_(error) {
 }
 
 virtual ~MonitorableException() throw() {        
 }
 
 Monitorable& getMonitorable() {
 return monitorable_;
 }

 const std::string& getItem() {
 return item_;
 }

 const std::string& getError() {
 return error_;
 }
 };*/

}

#endif
