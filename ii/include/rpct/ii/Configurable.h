/** Filip Thyssen */

#ifndef rpct_ii_Configurable_h_
#define rpct_ii_Configurable_h_

#include "rpct/ii/IHardwareItem.h"
#include "rpct/ii/ConfigurationSet.h"

namespace rpct {

class Configurable : public virtual IHardwareItem
{
protected:
public:
    virtual ~Configurable() {}
    virtual void configure(ConfigurationSet * configurationSet, int configureFlags) = 0;
    virtual void configure() {}
};

} // namespace rpct

#endif // rpct_ii_Configurable_h_
