#ifndef VComponentH
#define VComponentH

#include <string>
#include <vector>
#include "std_logic_1164_ktp.h"	


enum TVIIItemType { VII_PAGE, VII_AREA, VII_WORD, VII_VECT, VII_BITS };
enum TVIIItemWrType { VII_WNOACCESS, VII_WACCESS };
enum TVIIItemRdType { VII_RNOACCESS, VII_REXTERNAL, VII_RINTERNAL };
enum TVIIItemFun { VII_FUN_UNDEF, VII_FUN_HIST, VII_FUN_RATE };

struct TVIIItemDecl
{
	TVIIItemType ItemType;
	TN ItemID;
	TVL ItemWidth;
	TN ItemNumber;
	TN ItemParentID;
	TVIIItemWrType ItemWrType;
	TVIIItemRdType ItemRdType;
	std::string ItemName;
	TVIIItemFun ItemFun;
	std::string ItemDescr;
};

typedef std::vector<TVIIItemDecl> TVIIItemDeclList;

struct TVIIItem
{
	TVIIItemType ItemType;
	TN ItemID;
	TVI ItemParentID;
	TVL ItemWidth;
	TN ItemNumber;
	TVIIItemWrType ItemWrType;
	TVI ItemWrPos;
	TVIIItemRdType ItemRdType;
	TVI ItemRdPos;
	TVI ItemAddrPos;
	TVL ItemAddrLen;
};

typedef std::vector<TVIIItem> TVII;
               
const TP VII_ITEM_NAME_LEN	= 32;
const TP VII_ITEM_DESCR_LEN	= 64;
const TVL LPM_ADDR_WIDTH = 4;
const TVL LPM_DATA_WIDTH = 4;
const TVL LPM_TEST_WIDTH = 8;

TS  VIINameConv(TS name);
TS VIIDescrConv(TS name);
TVII TVIICreate(TVIIItemDeclList list, TVL addr_width, TVL data_width);
TN VIICheckSumGet(TVII par);

#endif 
