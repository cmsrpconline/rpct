#ifndef rpctHardwareItemBaseH
#define rpctHardwareItemBaseH

#include "rpct/ii/IHardwareItem.h"

namespace rpct {

class HardwareItemBase : virtual public IHardwareItem {
protected:
    int id_;
    std::string description_;
    const HardwareItemType type_;
public:
    HardwareItemBase(int id, std::string description, const HardwareItemType& type)
    : id_(id), description_(description), type_(type) {
    }
    
    virtual int getId() const {
        return id_;
    }
    
    virtual const std::string& getDescription() const {
        return description_;
    }

    virtual const HardwareItemType& getType() const {
        return type_;
    }
};

}

#endif
