#ifndef IDEVICEITEM_H
#define IDEVICEITEM_H

class IDevice;

namespace rpct {

class IDeviceItem {	
private:
	IDevice& Device;
	
public:
	IDeviceItem() {}
	virtual ~IDeviceItem() {}
};
}



#endif


