#ifndef IBOARD_HH
#define IBOARD_HH

#include <vector>
#include "rpct/ii/IDevice.h"
#include "rpct/ii/IHardwareItem.h"
#include "rpct/ii/Configurable.h"

namespace rpct {

class ICrate;

class IBoard : virtual public IHardwareItem
             , public virtual Configurable {
public:
    typedef std::vector<IDevice*> Devices;
    virtual const Devices& getDevices() = 0;

    virtual ICrate* getCrate() = 0;

    //virtual std::string getType() = 0;
    virtual int getPosition() = 0;

    virtual IDevice& addChip(std::string type, int pos, int id) = 0;

    //virtual void init() = 0;
    virtual void selfTest() = 0;

    virtual void enable() = 0;
    virtual void disable() = 0
    ;
    virtual void checkVersions() = 0;
};

}

#endif
