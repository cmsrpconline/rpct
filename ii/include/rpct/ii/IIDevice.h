//---------------------------------------------------------------------------

//
//  Michal Pietrusinski 2001
//  Warsaw University
//

#ifndef IIDeviceH
#define IIDeviceH
//---------------------------------------------------------------------------

#include "rpct/ii/IDevice.h"
#include "rpct/ii/MemoryMap.h"
#include "rpct/ii/IBoard.h"
#include "rpct/std/IllegalArgumentException.h"
#include <sstream>
#include <log4cplus/logger.h>
#include "log4cplus/loggingmacros.h"

namespace rpct {

class IIVMEDevice {
public:
    virtual ~IIVMEDevice() {
    }
    virtual int GetVMEBaseAddrShift() = 0;
    virtual int GetVMEBoardBaseAddr() = 0;
};

class IIUSBDevice {
public:
    virtual ~IIUSBDevice() {
    }
    virtual int GetUSBBaseAddrShift() = 0;
    virtual int GetUSBBoardBaseAddr() = 0;
};

class EBadDeviceVersion: public TException {
public:

    struct VersionInfo {
        rpct::IHardwareItem* item;
        std::string element;
        std::string expectedVal;
        std::string val;
        VersionInfo() {
        }
        VersionInfo(rpct::IHardwareItem* _item, std::string _element, std::string _expectedVal, std::string _val) :
            item(_item), element(_element), expectedVal(_expectedVal), val(_val) {
        }
    };

    typedef std::list<VersionInfo> VersionInfoList;

    /*EBadDeviceVersion(const std::string& msg) throw() :
     TException("Bad device version: " + msg) {
     }*/

    EBadDeviceVersion(const std::string& msg, VersionInfo versionInfo) throw() :
        TException("Bad device version: " + msg) {
        versionInfoList_.push_back(versionInfo);
    }

    EBadDeviceVersion(const std::string& msg, VersionInfoList& versionInfoList) throw() :
        TException("Bad device version: " + msg), versionInfoList_(versionInfoList) {
    }

    virtual ~EBadDeviceVersion() throw() {
    }

    VersionInfoList& getVersionInfoList() {
        return versionInfoList_;
    }

    void setVersionInfoList(VersionInfoList& versionInfoList) {
        versionInfoList_ = versionInfoList;
    }
private:
    VersionInfoList versionInfoList_;
};

class TIIDevice: virtual public IDevice {
private:
    static log4cplus::Logger logger;
protected:
    int id;
    std::string Name;
    TID NameID;
    std::string description;
    HardwareItemType type_;
    uint32_t Version;
    TID VersionID;
    TID ChecksumID;
    uint32_t BaseAddress;
    IBoard& Board;
    int position_;
    MemoryMap* memoryMap_;
    typedef std::multimap<TID, IObserver*> TObserverMap;
    TObserverMap ObserverMap;
    std::string FullName;

    typedef std::pair<TObserverMap::iterator, TObserverMap::iterator> TObserverRange;

    TObserverRange NotifyObserversBeforeWrite(TID id) {
        TObserverRange range = ObserverMap.equal_range(id);
        TObserverMap::iterator iObserver = range.first;
        for (; iObserver != range.second; ++iObserver)
            iObserver->second->DeviceAttributeBeforeWrite(this, id);
        return range;
    }

    void NotifyObserversAfterWrite(TObserverRange p, TID id) {
        TObserverMap::iterator iObserver = p.first;
        for (; iObserver != p.second; ++iObserver)
            iObserver->second->DeviceAttributeAfterWrite(this, id);
    }

    std::string GetIDName(TID id) {
        std::ostringstream ostr;
        ostr << FullName << '.' << memoryMap_->getItemDesc(id).Name;
        return ostr.str();
    }

    /*ostream& AppendItemInfo(std::ostream& ost, TID id)
     {
     TItemDesc& itemDesc = MemoryMap->getItemDesc(id);

     ost << "boardId=\"" << Board.GetID()
     << "\" boardDesc=\"" << Board.GetDescription()
     << "\" devId=\"" << GetID()
     << "\" devDesc=\"" << GetDescription()
     << "\" itemName=\"" << itemDesc.Name;
     << "\" itemName=\"" << itemDesc.Name;
     }

     void LogReadOperation(TID id, const char* operation, uint32_t address, uint32_t value)
     {
     G_Log.out() << "<iia oper=\"" << operation << "\" addr=\"" << address << "\" >"
     << value << "</iia>\n";

     //<< Board.GetDescription() << " " << GetIDName(id) << "[" << idx << "] = " << std::flush;
     //  uint32_t value = MemoryMap->readWord(id, idx);

     }  */

public:
    static void setupLogger(std::string namePrefix);
    TIIDevice(int ident, std::string name, TID name_id, std::string desc, HardwareItemType type, std::string ver,
            TID ver_id, TID checksum_id, uint32_t base_address, IBoard& board, int position, MemoryMap* memmap) :
                id(ident), Name(name), NameID(name_id), description(desc), type_(type), VersionID(ver_id), ChecksumID(
                        checksum_id), BaseAddress(base_address), Board(board), position_(position), memoryMap_(memmap) {
        if (memoryMap_ == NULL)
            throw TException("TIIDevice::TIIDevice: memmap==NULL");

        sscanf(ver.c_str(), "%x", &Version);
        //std::istringstream istr(ver.c_str());
        //istr >> std::hex;
        //istr >> Version;
        FullName = Board.getDescription() + "/" + Name;
    }

    virtual ~TIIDevice() {
        delete memoryMap_;
    }

    virtual uint32_t getBaseAddress() {
        return BaseAddress;
    }

    virtual int getId() const {
        return id;
    }

    virtual const std::string& getName() {
        return Name;
    }

    virtual const std::string& getFullName() {
        return FullName;
    }

    virtual const std::string& getDescription() const {
        return description;
    }

    virtual const HardwareItemType& getType() const {
        return type_;
    }

    virtual uint32_t getVersion() {
        return Version;
    }

    virtual TID getVersionId() {
        return VersionID;
    }

    virtual IBoard& getBoard() {
        return Board;
    }

    virtual int getPosition() {
        return position_;
    }

    virtual const TItemsMap& getItems() {
        return *memoryMap_->GetItems();
    }

    virtual const TItemDesc& getItemDesc(TID id) {
        return memoryMap_->getItemDesc(id);
    }

    virtual MemoryMap& getMemoryMap() {
        return *memoryMap_;
    }

    virtual void checkName();

    virtual void checkVersion();

    virtual void checkChecksum();

    /**
     * if version is not correct, does not give exception checkVersion(),
     * but returns non empty string with message
     */
    virtual std::string monitorVersion();

    //
    // hardware operations
    //

    // word operations
    virtual uint32_t readWord(TID id, int idx) {
        uint32_t value = memoryMap_->readWord(id, idx);
        LOG4CPLUS_DEBUG(logger, "Read " << GetIDName(id) << "[" << idx << "] = " << std::hex << value);
        return value;
    }

    virtual void readWord(TID id, int idx, void* data) {
        memoryMap_->readWord(id, idx, data);
        LOG4CPLUS_DEBUG(logger, "Read " << GetIDName(id) << "[" << idx << "] = some data");
    }

    virtual void writeWord(TID id, int idx, uint32_t data) {
        TObserverRange range = NotifyObserversBeforeWrite(id);
        LOG4CPLUS_DEBUG(logger, "Write " << GetIDName(id) << "[" << idx << "] = " << std::hex << data);
        memoryMap_->writeWord(id, idx, data);
        NotifyObserversAfterWrite(range, id);
    }

    virtual void writeWord(TID* id, int* idx, uint32_t* data, int count) {
        TObserverRange ranges[count];
        for (int i = 0; i < count; i++) {
            ranges[i] = NotifyObserversBeforeWrite(id[i]);
        }
        LOG4CPLUS_DEBUG(logger, "Write multiple");
        memoryMap_->writeWord(id, idx, data, count);
        for (int i = 0; i < count; i++) {
            NotifyObserversAfterWrite(ranges[i], id[i]);
        }
    }

    virtual void writeWord(TID id, int idx, void* data) {
        TObserverRange range = NotifyObserversBeforeWrite(id);
        LOG4CPLUS_DEBUG(logger, "Write " << GetIDName(id) << "[" << idx << "] = some data");
        memoryMap_->writeWord(id, idx, data);
        NotifyObserversAfterWrite(range, id);
    }

    virtual void writeWord(TID id, int idx, boost::dynamic_bitset<>& data);

    // area operations
    virtual void writeArea(TID id, void* data, int start_idx = 0, int n = npos) {
        TObserverRange range = NotifyObserversBeforeWrite(id);
        LOG4CPLUS_DEBUG(logger, "Write " << GetIDName(id) << " from " << start_idx
                << " size " << n << " = some data");
        memoryMap_->writeArea(id, data, start_idx, n);
        NotifyObserversAfterWrite(range, id);
    }

    virtual void readArea(TID id, void* data, int start_idx = 0, int n = npos) {
        LOG4CPLUS_DEBUG(logger, "Read " << GetIDName(id) << " from " << start_idx
                << " size " << n << " = some data");
        memoryMap_->readArea(id, data, start_idx, n);
    }

    virtual void writeArea(TID id, uint32_t* data, int start_idx = 0, int n = npos) {
        TObserverRange range = NotifyObserversBeforeWrite(id);
        LOG4CPLUS_DEBUG(logger, "Write " << GetIDName(id) << " from " << start_idx
                << " size " << n << " = some data");
        memoryMap_->writeArea(id, data, start_idx, n);
        NotifyObserversAfterWrite(range, id);
    }

    virtual void readArea(TID id, uint32_t* data, int start_idx = 0, int n = npos) {
        LOG4CPLUS_DEBUG(logger, "Read " << GetIDName(id) << " from " << start_idx
                << " size " << n << " = some data");
        memoryMap_->readArea(id, data, start_idx, n);
    }

    //vector operations
    virtual uint32_t readVector(TID id) {
        uint32_t value = memoryMap_->readVector(id);
        LOG4CPLUS_DEBUG(logger, "Read " << GetIDName(id) << " = " << std::hex << value);
        return value;
    }

    virtual void readVector(TID id, void* data) {
        LOG4CPLUS_DEBUG(logger, "Read " << GetIDName(id) << " = some data");
        memoryMap_->readVector(id, data);
    }

    virtual void writeVector(TID id, uint32_t data) {
        TObserverRange range = NotifyObserversBeforeWrite(id);
        LOG4CPLUS_DEBUG(logger, "Write " << GetIDName(id) << " = " << std::hex << data);
        memoryMap_->writeVector(id, data);
        NotifyObserversAfterWrite(range, id);
    }

    virtual void writeVector(TID* id, uint32_t* data, int count) {
        TObserverRange ranges[count];
        for (int i = 0; i < count; i++) {
            ranges[i] = NotifyObserversBeforeWrite(id[i]);
        }
        LOG4CPLUS_DEBUG(logger, "Write multiple vector");
        memoryMap_->writeVector(id, data, count);
        for (int i = 0; i < count; i++) {
            NotifyObserversAfterWrite(ranges[i], id[i]);
        }
    }

    virtual void writeVector(TID id, void* data) {
        TObserverRange range = NotifyObserversBeforeWrite(id);
        LOG4CPLUS_DEBUG(logger, "Write " << GetIDName(id) << " = some data");
        memoryMap_->writeVector(id, data);
        NotifyObserversAfterWrite(range, id);
    }

    // bit operations
    virtual uint32_t readBits(TID id) {
        uint32_t value = memoryMap_->readBits(id);
        LOG4CPLUS_DEBUG(logger, "Read " << GetIDName(id) << " = " << std::hex << value);
        return value;
    }

    virtual void writeBits(TID id, uint32_t data, bool preread = true) {
        TObserverRange range = NotifyObserversBeforeWrite(id);
        LOG4CPLUS_DEBUG(logger, "Write " << GetIDName(id) << " = " << std::hex << data
                << (preread ? " preread" : ""));
        memoryMap_->writeBits(id, data, preread);
        NotifyObserversAfterWrite(range, id);
    }

    virtual uint32_t getBitsInVector(TID bits_id, uint32_t vector_data) {
        return memoryMap_->getBitsInVector(bits_id, vector_data);
    }

    virtual void setBitsInVector(TID bits_id, uint32_t bits_data, uint32_t& vector_data) {
        memoryMap_->setBitsInVector(bits_id, bits_data, vector_data);
    }

    virtual uint32_t getBitsInVector(TID bits_id, void* vector_data) {
        return memoryMap_->getBitsInVector(bits_id, vector_data);
    }

    virtual void setBitsInVector(TID bits_id, uint32_t bits_data, void* vector_data) {
        memoryMap_->setBitsInVector(bits_id, bits_data, vector_data);
    }

    virtual void registerObserver(IObserver& obs, TID id = -1) {
        ObserverMap.insert(TObserverMap::value_type(id, &obs));
    }

    virtual void unregisterObserver(IObserver& obs, TID id = -1) {
        std::pair<TObserverMap::iterator, TObserverMap::iterator> p = ObserverMap.equal_range(id);
        for (; p.first != p.second; ++p.first) {
            if (p.first->second == &obs)
                ObserverMap.erase(p.first);
        }
    }

    // return true if cold reset must be done (QPLL not locked, TTCrx not ready), otherwise false
    virtual bool checkResetNeeded() {
        return false;
    }

    // return false if cold configure must be done (e.g. the config version identifier does not match, otherwise true
    virtual bool checkWarm(DeviceSettings* s) {
        return true;
    }

    virtual void enable() {
    }

    virtual void disable() {
    }

    // helper function to simplify conversion from VHDL
    static TVIIItemDecl createTVIIItemDecl(TVIIItemType ItemType, int ItemID, int ItemWidth, int ItemNumber,
            int ItemParentID, TVIIItemWrType ItemWrType, TVIIItemRdType ItemRdType, std::string ItemName,
            TVIIItemFun ItemFun, std::string ItemDescr);
};

}

#endif
