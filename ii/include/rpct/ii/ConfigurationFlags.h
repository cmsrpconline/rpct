#ifndef RPCT_CONFIGURATIONFLAGS_H_
#define RPCT_CONFIGURATIONFLAGS_H_

namespace rpct {

class ConfigurationFlags {
private:
    ConfigurationFlags() {}
public:
    static const int DEFAULT = 0x00;

    //static const int FORCE_RESET = 0x1;
    static const int FORCE_CONFIGURE = 0x2;
    static const int ONLY_COLD = 0x4;
    static const int INCLUDE_DISABLED = 0x8;
};

}

#endif 
