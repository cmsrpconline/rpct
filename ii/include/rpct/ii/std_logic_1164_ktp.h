//---------------------------------------------------------------------------

#ifndef std_logic_1164_ktpH
#define std_logic_1164_ktpH
//---------------------------------------------------------------------------

#include <string>
#include <vector>


  //////////////////////////////////////////////////////////////////-
  // standard package extentions
  //////////////////////////////////////////////////////////////////-
typedef int TI;
typedef int TN;
typedef int TP;
typedef bool TL;
typedef char TC;
typedef std::string TS;
typedef int TI;

  /*
  type TIV = array of integer;
  type TNV = array of integer;
  type TPV = array of integer;
  type TLV = array of boolean;


  function  minimum(a, b :TS) : TS; overload;	// returns less value from 'a' or 'b'
  function  maximum(a, b :TS) : TS; overload;  	// returns biger value from 'a' or 'b'
  function  sel(t, f :TS; c :TL) : TS; overload;// returns 't' for 'c'=TRUE or 'f' for 'c'=FALSE

  //////////////////////////////////////////////////////////////////-
  // std_logic_arith package extentions
  //////////////////////////////////////////////////////////////////-
  */
  TI  minimum(TI a, TI b);    // returns less value from 'a' or 'b'
  TI  maximum(TI a, TI b);	// returns biger value from 'a' or 'b'
  /*
  function  sel(t, f :TI; c :TL): TI; overload;	// returns 't' for 'c'=TRUE or 'f' for 'c'=FALSE

  function  minimum(v :TIV): TI; overload; 	// returns smallest value from vector 'v' of integers
  function  maximum(v :TIV): TI; overload; 	// returns bigest value from vector 'v' of integers
  function  minimum(v :TNV): TN; overload; 	// returns smallest value from vector 'v' of naturals
  function  maximum(v :TNV): TN; overload; 	// returns bigest value from vector 'v' of naturals
  function  minimum(v :TPV): TP; overload; 	// returns smallest value from vector 'v' of positives
  function  maximum(v :TPV): TP; overload; 	// returns bigest value from vector 'v' of positives

  //////////////////////////////////////////////////////////////////-
  // vectors range family
  //////////////////////////////////////////////////////////////////-
  */
  typedef TN TVL;	       	// 'TVL' defines vector length range
  const TVL NO_VEC_LEN = 0;  	// 'NO_VEC_LEN' occurs not defined vector length
  typedef int TVI;	// 'TVI' defines vector index range

  const TVI NO_VEC_INDEX  = -1;	// 'NO_VEC_INDEX' occurs not defined vector index
  const TVI VEC_INDEX_MIN = 0;	// 'VEC_INDEX_MIN' occurs minimal vector index
  /*

  type      TVR	= record	// 'TVR' type covers vector range parameters(l,r)
	      l :TVI;
	      r :TVI;
  end;

  type      TVLV = array of TVL;
  */
  typedef std::vector<TVI>  TVIV;
  /*
  type      TVRV = array of TVR;


  function  TVRcreate(l, r :TVI): TVR; overload; 	// generates 'TVR' for 'l' and 'r' input variables
  function  TVRcreate(len :TVL): TVR; overload;	 	// generates 'TVR': 'l'=len-1 and 'r'=VEC_INDEX_MIN
  function  TVLcreate(l, r :TVI): TVL; overload; 	// computes length:abs('l'-'r')+1

  function  TVLcreate(par :TVR): TVL; overload;	        // computes length:abs('par.l'-'par.r')+1
  function  minimum(v :TVLV): TVL; overload;	   	// returns smallest value from vector 'v' of TVL
  function  maximum(v :TVLV): TVL; overload;	   	// returns bigest value from vector 'v' of TVL
  function  minimum(v :TVIV): TVI; overload;	   	// returns smallest value from vector 'v' of TVI
  */
  TVI maximum(TVIV v);	   	// returns bigest value from vector 'v' of TVI
  /*

  function  oper_star_star(l, r :TVR): TVR; overload;	        // returns maximal common 'TVR' size for 'l' and 'r'

  function  TVRVconv(arg :TVR): TVRV;			// generates 'TVIV' vector(0 downto 0) = (arg)
  function  TVLcreate(arg :TVRV): TVL; overload;     	// returns length of vector from 'arg';
  function  oper_plus(l, r :TVRV): TVRV; overload;   	// adds 'l' to 'r' and generates common 'TVIV' vec.
  function  oper_plus(l, r :TVR): TVRV; overload;    	// adds 'l' to 'r' and generates common 'TVIV' vec.
  function  oper_plus(l :TVRV; r :TVR): TVRV; overload;	// adds 'l' to 'r' and generates common 'TVIV' vec.
  function  oper_plus(l :TVR; r :TVRV): TVRV; overload;	// adds 'l' to 'r' and generates common 'TVIV' vec.

  //////////////////////////////////////////////////////////////////-
  // TSL and TSLV types families
  //////////////////////////////////////////////////////////////////-
  type   TSL = std_logic;
  type   TSLV = std_logic_vector;

  function  TSLconv(arg :TN): TSL; overload;   	// 'TN' type converts to 'TSL' type
  function  TSLconv(arg :TL): TSL; overload;   	// 'TL' type converts to 'TSL' type
  function  TSLconv(arg:TSLV): TSL; overload;  	// returns arg(arg'low) as 'TSL' type
  function  TNconv(arg :TSL): TN; overload;     // 'TSL' type converts to 'TN' type
  function  TLconv(arg :TSL): TL; overload;	// returns result of logical condition :(arg='1')

  function  TSLVconv(arg :TN; len :TVL): TSLV; overload;// converts 'TN' to TSLV(len-1 downto VEC_INDEX_MIN)
  function  TSLVconv(arg :TN): TSLV; overload;		// converts 'TN' to TSLV with minimal size
  function  TSLVconv(arg :TL; len :TVL): TSLV; overload;// converts 'TL' to TSLV(len-1 downto VEC_INDEX_MIN)
  function  TSLVconv(arg :TL): TSLV; overload;		// converts 'TL' to TSLV with minimal size
  function  TSLVconv(arg:TSL; len :TVL): TSLV; overload;// converts 'TSL' to TSLV(len-1 downto VEC_INDEX_MIN)
  function  TSLVconv(arg:TSL): TSLV; overload;		// converts 'TSL' to TSLV with minimal size
  function  TNconv(arg :TSLV): TN; overload;		// 'TSLV' type converts to 'TN' type
  function  TLconv(arg :TSLV): TL; overload;		// returns FALSE if 'arg' contains only '0' bits

  procedure TSLVputS (var vec :TSLV; dst, src :TSLV); overload;	   	// performs:vec(dst'range)=src; returns 'vec'
  procedure TSLVput  (var vec :TSLV; dst, src :TSLV); overload;		   	// performs:vec(dst'range)=src; returns 'vec'
  procedure TSLVputS (var vec :TSLV; dst :TSLV; src :TSL); overload; 	// performs:vec(dst'right)=src; returns 'vec'
  procedure TSLVput  (var vec :TSLV; dst :TSLV; src :TSL); overload;	   	// performs:vec(dst'right)=src; returns 'vec'
  procedure TSLVputS (var dst :TSLV; src :TSLV); overload;	   	// performs:dst(src'range)=src; returns 'dst'
  procedure TSLVput  (var dst :TSLV; src :TSLV); overload;		   	// performs:dst(src'range)=src; returns 'dst'
  procedure TSLVputS (var dst :TSLV; l,r :TVI; src :TSLV); overload; 	// performs:vec(l to/downto r)=src; returns 'vec'
  procedure TSLVput  (var dst :TSLV; l,r :TVI; src :TSLV); overload;	   	// performs:vec(l to/downto r)=src; returns 'vec'
  procedure TSLVfillS(var dst :TSLV; f :TSL); overload;		   	// fills dst of 'f'; returns 'dst'
  procedure TSLVfill (var dst :TSLV; f :TSL); overload;			   	// fills dst of 'f'; returns 'dst'

  function  TSLVnew(l,r :TVI; f:TSL): TSLV; overload;	 // creates TSLV(l to/downto r) and fills 'f'
  function  TSLVnew(l,r :TVI): TSLV; overload;		 // creates TSLV(l to/downto r) and fills '0'
  function  TSLVnew(length :TVL; f:TSL): TSLV; overload; // creates TSLV(length-1 downto VEC_INDEX_MIN) + fills 'f'
  function  TSLVnew(length :TVL): TSLV; overload;	 // creates TSLV(length-1 downto VEC_INDEX_MIN) + fills '0'
  */
  TVL TVLcreate(TN arg);	 // returns minimal TSLV size to store 'arg'

  TN SLVMax(TN arg);		 // returns maximum. value for min. TSLV size to store 'arg'
  /*

  function  SLVBitCount(arg:TSLV; b :TSL): TN;		 // returns number of 'b' bits from 'arg'
  function  SLVBit0Count(arg:TSLV): TN;			 // returns number of '0' bits from 'arg'
  function  SLVBit1Count(arg:TSLV): TN;			 // returns number of '1' bits from 'arg'

  function  oper_star_star(a, b :TSLV): TSLV; overload;	 // returns TSLV(a'length + b'length-1 downto 0)
  function  oper_amper(a, b :TSLV): TSLV; overload;	 // returns TSLV(a'length + b'length-1 downto 0)

  function  minimum(a, b :TSLV): TSLV; overload;	 // returns less value from 'a' or 'b'
  function  maximum(a, b :TSLV): TSLV; overload;	 // returns biger value from 'a' or 'b'
  function  sel(t, f :TSL; c :TL): TSL; overload;	 // returns 't' for 'c'=TRUE or 'f' for 'c'=FALSE
  function  sel(t, f :TSLV; c :TL): TSLV; overload;	 // returns 't' for 'c'=TRUE or 'f' for 'c'=FALSE

  //function  TNconv(arg:TC): TN; overload;		 // 'TC' type convertion to 'TI' (as ANSII code)
  //function  TSLVconv(arg:TC; len :TVL): TSLV; overload;	 // 'TC' type convertion to 'TSLV' with 'len' size
  //function  TSLVconv(arg:TC): TSLV; overload;		 // 'TC' type convertion to 'TSLV' with 8 bit size
  function  TNconv(arg:TS): TN; overload;		 // 'TS' type convertion to 'TI' (as ANSII word)
  function  TSLVconv(arg:TS; len :TVL): TSLV; overload;	 // 'TS' type convertion to 'TSLV' with 'len' size
  function  TSLVconv(arg:TS): TSLV; overload;		 // 'TS' type convertion to 'TSLV' with 8*'arg' size
  function  TSLVconv(arg:TS; dst :TSLV): TSLV; overload; // 'TS' converts to TSLV with dst'lenght size

  //////////////////////////////////////////////////////////////////
  // vectors translate family
  //////////////////////////////////////////////////////////////////
  type      TVT = record				// 'TVT' type covers line function  parameters
	      a :TI;
	      b :TI;
  end;

  function  TVTcreate(sl, sr, dl, dr :TVI): TVT; overload;		// generates 'TVT' where dl=a*dr+b, dr=a*sr+b
  function  VTindex(vt :TVT; index :TVI): TVI; overload;	// counts y=vt.a*index+vt.b
  function  VTindex(sl, sr, dl, dr, index :TVI): TVI; overload;	// generates 'TVT' for sl,..,dr and count y

  //////////////////////////////////////////////////////////////////-
  // vectors translate family for SLV
  //////////////////////////////////////////////////////////////////-
  function  TVTcreate(src :TSLV; dl, dr :TVI): TVT; overload;		// generates 'TVT' for src'left,src'right,dl,dr
  function  TVTcreate(src, dst :TSLV): TVT; overload;			// generates 'TVT' for 'src' and 'dst' ranges
  function  VTnorm(src :TSLV): TVT; overload;				// 'TVT' tranlates to(src'lenght-1 downto VEC_INDEX_MIN)
  function  VTrevNorm(src :TSLV): TVT; overload;				// 'TVT' tranlates to(VEC_INDEX_MIN to src'lenght-1)
  function  TSLVtrans(src, dst :TSLV): TSLV; overload;				// converts 'src' to TSLV(dst'left to/downto dst'right)
  function  SLVNorm(src :TSLV): TSLV; overload;					// converts 'src' to TSLV(src'lenght-1 downto VEC_INDEX_MIN)
  function  SLVrevNorm(src :TSLV): TSLV; overload;					// converts 'src' to TSLV(VEC_INDEX_MIN to src'lenght-1)
  function  SLVrevRange(src :TSLV): TSLV; overload;					// converts 'src' to TSLV(src'reverse_range)

  //////////////////////////////////////////////////////////////////-
  // vectors resize family for TSLV
  //////////////////////////////////////////////////////////////////-
  function  TSLVResize(src :TSLV; l,r :TVI; f :TSL): TSLV; overload;		// resize 'src' to TSLV(l to/downto r), fills 'f' new bits
  function  TSLVResize(src :TSLV; l,r :TVI): TSLV; overload;			// resize 'src' to TSLV(l to/downto r), fills '0' new bits
  function  TSLVResize(src :TSLV; len :TVL; f :TSL): TSLV; overload;		// resize 'src' to TSLV(len-1 downto VEC_INDEX_MIN) + fill 'f'
  function  TSLVResize(src :TSLV; len :TVL): TSLV; overload;			// resize 'src' to TSLV(len-1 downto VEC_INDEX_MIN) + fill '0'
  function  TSLVResize(src, dst :TSLV; f :TSL): TSLV; overload;			// resize 'src' to TSLV(dst'left .. dst'right) + fill 'f'
  function  TSLVResize(src, dst :TSLV): TSLV; overload;				// resize 'src' to TSLV(dst'left .. dst'right) + fill '0'

  //////////////////////////////////////////////////////////////////-
  // HEX type family
  //////////////////////////////////////////////////////////////////-
  //type      TH = (								// 'TH' definition
  //	      '0', '1', '2', '3', '4', '5', '6', '7',
  //            '8', '9',	'A', 'B', 'C', 'D', 'E', 'F'
  //	    );

  type      TH = (
                th0, th1, th2, th3, th4, th5, th6, th7,
                th8, th9, thA, thB, thC, thD, thE, thF
            );
  type      THV = array of TH;
  */
  typedef std::string THV;

  /*
  function  TNconv(arg:TH): TN; overload;					// 'TH' type convertion to 'TI'
  function  TSLVconv(arg:TH; len :TVL): TSLV; overload;				// 'TH' type convertion to 'TSLV' with 'len' size
  function  TSLVconv(arg:TH): TSLV; overload;					// 'TH' type convertion to 'TSLV' with 4 bit size
  function  TNconv(arg:THV): TN; overload;					// 'THVec' type convertion to 'TI'
  function  TSLVconv(arg:THV; len :TVL): TSLV; overload;			// 'THVec' type convertion to 'TSLV' with 'len' size
  function  TSLVconv(arg:THV): TSLV; overload;					// 'THVec' type convertion to 'TSLV' with 4*'arg' size
  function  TSLVconv(arg:THV; dst :TSLV): TSLV; overload;				// 'THVec' converts to TSLV with dst'lenght size

  //////////////////////////////////////////////////////////////////-
  // vectors range family for TSLV
  //////////////////////////////////////////////////////////////////-
  function  TVRcreate(src:TSLV): TVR; overload;				// generates 'TVR' for v'left and v'right
  function  TSLVnew(par :TVR; f :TSL): TSLV; overload;			// generates TSLV(par.l to/downto par.r) + fill 'f'
  function  TSLVnew(par :TVR): TSLV; overload;				// generates TSLV(par.l to/downto par.r) + fill '0'
  function  TSLVResize(src :TSLV; par :TVR; f :TSL): TSLV; overload;		// resize 'src' to TSLV(par.l .. par.r) + fill 'f'
  function  TSLVResize(src :TSLV; par :TVR): TSLV; overload;			// resize 'src' to TSLV(par.l .. par.r) + fill '0'
  function  TSLconv(src:TSLV; vr :TVR): TSL; overload;				// converts 'src' to TSL as TSLV(vr.r)
  function  TSLVresize(src:TSL; vr :TVR): TSLV; overload;				// converts 'src' to TSLV(vr.l to/downto vr.r)
  function  TSLVresize(src:TSLV; vt :TVT; vr :TVR): TSLV; overload;		// converts 'src' to TSLV(vt(vr.l) to/downto vt(vr.r))
  function  TSLconv(src:TSLV; vt :TVT; vr :TVR): TSL; overload;		// converts 'src' to TSL as TSLV(vt(vr.r))
  function  TSLVresize(src:TSL; vt :TVT; vr :TVR): TSLV; overload;		// converts 'src' to TSLV(vt(vr.l) to/downto vt(vr.r))
  function  TSLVtrans(src :TSLV; par :TVR): TSLV; overload;			// converts 'src' to TSLV(par.l .. par.r)
  function  TSLVtrans(src :TSL; par :TVR): TSLV; overload;			// converts 'src' to TSLV(par.l .. par.r)
  procedure TSLVputS(var dst :TSLV; par :TVR; src :TSLV); overload;	// performs:vec(par.l to/downto par.r)=src; returns 'vec'
  procedure TSLVput(var dst :TSLV; par :TVR; src :TSLV); overload;			// performs:vec(par.l to/downto par.r)=src; returns 'vec'
  procedure TSLVputS(var dst :TSLV; par :TVR; src :TSL); overload;	// performs:vec(par.l)=src; returns 'vec'
  procedure TSLVput(var dst :TSLV; par :TVR; src :TSL); overload;			// performs:vec(par.l)=src; returns 'vec'

  //////////////////////////////////////////////////////////////////-
  // vectors partitioning
  //////////////////////////////////////////////////////////////////-
  */
  TP SLVPartNum(TVL vlen, TVL plen);				// calculates part numbers (len:'plen') in vector (len:'vlen')
  /*function  SLVPartNum(v :TSLV; plen :TVL): TP; overload;			// calculates part numbers (len:'plen') in vector 'v'
  function  SLVPartSize(vlen, plen :TVL; index :TVI): TVL; overload;	// calculates size of part number 'index' in vector
  function  SLVPartSize(v :TSLV; plen :TVL; index :TVI): TVL; overload;// calculates size of part number 'index' in vector 'v'
  function  SLVPartSize(v :TSLV; plen :TVL; vi :TSLV): TVL; overload;	// calculates size of part selected by 'vi' in vector 'v'
  function  SLVPartLastSize(vlen, plen :TVL): TVL; overload;			// calculates size of last part in vector
  function  SLVPartLastSize(v :TSLV; plen :TVL): TVL; overload;		// calculates size of last part in vector 'v'
  function  SLVPartGet(v :TSLV; plen :TVL; index :TVI): TSLV; overload;	// returns 'v' part number 'index'
  function  SLVPartGet(v :TSLV; plen :TVL; vi :TSLV): TSLV; overload;		// returns 'v' part selected by 'vi' (
  */
  TN SLVPartAddrExpand(TVL vlen, TVL plen);			// calculates address size selecting parts
  /*
  function  SLVPartAddrExpand(v :TSLV; plen :TVL): TN; overload;		// calculates address size selecting parts in vectopr 'v'

  //////////////////////////////////////////////////////////////////-
  // vectors multiplexing
  //////////////////////////////////////////////////////////////////-
  function  SLVDemux(src:TSLV;level:TVL; pos: TVI): TSLV; overload;	// demuxing on 'level' vectors, output: 'pos' vector
  function  SLVMux(src0, src1 :TSLV): TSLV; overload;				// muxing two vectors
  function  SLVMux(src0, src1, src2 :TSLV): TSLV; overload;				// muxing three vectors
  function  SLVMux(src0, src1, src2, src3 :TSLV): TSLV; overload;			// muxing four vectors
  */

#endif
