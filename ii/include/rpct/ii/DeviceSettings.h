#ifndef RPCTDEVICESETTINGS_H
#define RPCTDEVICESETTINGS_H

#include "rpct/ii/HardwareItemType.h"
#include <boost/shared_ptr.hpp>

namespace rpct {

class DeviceSettings {
public:
    virtual ~DeviceSettings() {
    }
    
    virtual HardwareItemType getDeviceType() = 0;
};

typedef boost::shared_ptr<DeviceSettings> DeviceSettingsPtr;

}

#endif
