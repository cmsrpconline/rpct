//---------------------------------------------------------------------------

#ifndef LPMDiagnosticsH
#define LPMDiagnosticsH
//---------------------------------------------------------------------------

#include "std_logic_1164_ktp.h"

TN DIAG_DAQ_MemDataWidth(TN data, TN trg, TN mask);
TN DIAG_DAQ_MemAddrWidth(TN data, TN trg, TN mask, TN abus,
	TN dbus);



#endif
