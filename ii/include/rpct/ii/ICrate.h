#ifndef ICRATE_H
#define ICRATE_H

#include <vector>
#include "IHardwareItem.h"
#include "IBoard.h"
#include "rpct/ii/ConfigurationSet.h"


namespace rpct {
    
class ICrate : public IHardwareItem {
public:    
  typedef std::vector<IBoard*> Boards;
  virtual ~ICrate() {}
  
  virtual std::string const & getLabel() const = 0;

  virtual Boards& getBoards() = 0;
  
  virtual bool checkResetNeeded() = 0;

  virtual void checkVersions() = 0;   
  
  virtual void reset(bool forceReset) = 0;
  virtual bool configure(ConfigurationSet* configSet, int configureFlags) = 0;

  virtual void enable() = 0;
  virtual void disable() = 0;
};

}

#endif
