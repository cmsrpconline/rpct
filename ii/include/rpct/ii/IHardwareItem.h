#ifndef IHARDWAREITEM_H
#define IHARDWAREITEM_H

#include "rpct/ii/HardwareItemType.h"
#include "rpct/tools/Item.h"

namespace rpct {

class IHardwareItem : public virtual rpct::tools::Item {
public:
  virtual ~IHardwareItem() {}                        
  virtual const std::string& getDescription() const = 0;
  virtual const HardwareItemType& getType() const = 0;
};

bool operator<(const IHardwareItem & ihi_a, const IHardwareItem & ihi_b);
bool operator==(const IHardwareItem & ihi_a, const IHardwareItem & ihi_b);

}

#endif
