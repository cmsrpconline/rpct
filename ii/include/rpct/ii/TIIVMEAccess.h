//---------------------------------------------------------------------------

#ifndef TIIVMEAccessH
#define TIIVMEAccessH
//---------------------------------------------------------------------------

#include "rpct/ii/TIIAccess.h"
#include "tb_vme.h"


namespace rpct {
    
    
class TIIVMEAccess: public TIIAccess {
private:
  uint32_t BoardAddr;
  TVMEInterface* VME;
  uint32_t Mask;
public:
    TIIVMEAccess(uint32_t board_addr, uint32_t module_addr,
                 TVMEInterface* vme, uint32_t mask);

    virtual ~TIIVMEAccess() {}

    virtual void Write(uint32_t address, void* data,
                       size_t word_count, size_t word_size);

    virtual void Write(uint32_t* addresses, void** data,
                       size_t* word_counts, size_t word_size, size_t count);

    virtual void Read(uint32_t address, void* data,
                      size_t word_count, size_t word_size);
};
 
}

#endif
