#ifndef TII_ACCESS_H
#define TII_ACCESS_H

#include "IHardwareIfc.h"

namespace rpct {
    
class TIIAccess: public IHardwareIfc {
protected:
  uint32_t ModuleAddr;
public:
    TIIAccess(uint32_t module_addr)
      : ModuleAddr(module_addr) {}

    virtual ~TIIAccess() {}

    virtual void Write(uint32_t address, void* data,
                       size_t word_count, size_t word_size) = 0;

    virtual void Write(uint32_t* addresses, void** data,
                       size_t* word_counts, size_t word_size, size_t count) {
    	for (size_t i = 0; i < count; i++) {
    		Write(addresses[i], data[i], word_counts[i], word_size);
    	}
    }

    virtual void Read(uint32_t address, void* data,
                      size_t word_count, size_t word_size) = 0;
};

}

#endif
