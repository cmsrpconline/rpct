#ifndef RPCT_HARDWAREITEMTYPE_H
#define RPCT_HARDWAREITEMTYPE_H

#include <string>

namespace rpct {

class HardwareItemType {
public:
    enum Category { cCrate, cBoard, cDevice, cDeviceItem };   
private:
    Category category_;
    std::string type_;
public:     
    HardwareItemType(Category category, const std::string & type) 
    : category_(category), type_(type) {
    }   
                        
    const Category& getCategory() const {
        return category_;
    }
    
    const std::string& getType() const {
        return type_;
    }
    
    friend bool operator==(const HardwareItemType& h1, const HardwareItemType& h2);
    friend bool operator<(const HardwareItemType& h1, const HardwareItemType& h2);
};

bool operator==(const HardwareItemType& h1, const HardwareItemType& h2);
bool operator<(const HardwareItemType& h1, const HardwareItemType& h2);

}

#endif
