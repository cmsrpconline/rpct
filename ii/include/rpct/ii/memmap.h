//---------------------------------------------------------------------------

//
//  Michal Pietrusinski 2001
//  Warsaw University
//

#ifndef memmapH
#define memmapH

#include <map>
#include <list>
#include <ostream>
#include "rpct/ii/MemoryMap.h"
#include "rpct/ii/VComponent.h"
#include "rpct/ii/IDevice.h"
#include "rpct/ii/TIIAccess.h"
#include "rpct/std/TException.h"

#ifdef TB_USB
#include "TUSBInterface.h"
#endif
//---------------------------------------------------------------------------

namespace rpct {

#ifdef TB_USB
class TIIUSBAccess: public TIIAccess {
private:
    TUSBInterface* USB;
    uint32_t BoardAddr;
    uint32_t Mask;
public:
    TIIUSBAccess(uint32_t board_addr, uint32_t module_addr,
                 TUSBInterface* usb)
            :BoardAddr(board_addr), TIIAccess(module_addr), USB(usb)
    { Mask = ModuleAddr | (BoardAddr << 20); }

    virtual ~TIIUSBAccess() {}

    virtual void Write(uint32_t int address, void* data,
                       size_t word_count, size_t word_size)
    {
        assert(word_size == 4);

        uint32_t* usbdata = new uint32_t[word_count];

        try {
            for(size_t i=0; i<word_count; i++) {
                usbdata[i] = *((uint32_t*)data+i);
            };

            USB->Write( Mask | address, (char*)usbdata, word_count * sizeof(uint32_t) );
        }
        catch(...) {
            delete [] usbdata;
            throw;
        };
        delete [] usbdata;
    }

    virtual void Read(uint32_t int address, void* data,
                      size_t word_count, size_t word_size)
    {
        assert(word_size == 4);

        uint32_t* usbdata = new uint32_t[word_count];

        try {
            USB->Read( Mask | address, (char*)usbdata, word_count * sizeof(uint32_t) );
            for(size_t i=0; i<word_count; i++) {
                *(((uint32_t*) data)+i) = usbdata[i];
            }
        }
        catch(...) {
            delete [] usbdata;
            throw;
        };
        delete [] usbdata;
    }
};

#endif



class TMemoryMap : virtual public MemoryMap {
public:

    //typedef std::list<TVIIItemDecl> TDeclItems;

    TMemoryMap(TDeclItems& items, int addr_width, int data_width);

    TMemoryMap(const TVIIItemDecl* declItems, int declItemsCount, const TVIIItem* viiItems, int viiItemsCount, int addr_width, int data_width,
               uint32_t checkSum);

    virtual ~TMemoryMap()
    {
        if (HardwareIfcOwner && (HardwareIfc != NULL))
            delete HardwareIfc;
    }

    //static const int npos = IDevice::npos;

    // word operations
    virtual uint32_t readWord(int id, int idx);
    virtual void readWord(int id, int idx, void* data);

    virtual void writeWord(int id, int idx, uint32_t data);
    virtual void writeWord(int* id, int* idx, uint32_t* data, size_t count);
    virtual void writeWord(int id, int idx, void* data);

    // area operations
    virtual void writeArea(int id, void* data, int start_idx = 0, int n = npos);
    virtual void readArea(int id, void* data, int start_idx = 0, int n = npos);

    virtual void writeArea(int id, uint32_t* data, int start_idx = 0, int n = npos);
    virtual void readArea(int id, uint32_t* data, int start_idx = 0, int n = npos);

    //vector operations
    virtual uint32_t readVector(int id);
    virtual void readVector(int id, void* data);

    virtual void writeVector(int id, uint32_t data);
    virtual void writeVector(int* id, uint32_t* data, size_t count);
    virtual void writeVector(int id, void* data);

    // bit operations
    virtual uint32_t readBits(int id);
    virtual void writeBits(int id, uint32_t data, bool preread = true);

    virtual uint32_t getBitsInVector(int bits_id, uint32_t vector_data);
    virtual void setBitsInVector(int bits_id, uint32_t bits_data, uint32_t& vector_data);

    virtual uint32_t getBitsInVector(int bits_id, void* vector_data);
    virtual void setBitsInVector(int bits_id, uint32_t bits_data, void* vector_data);


    // interfacing with the hardware
    virtual void SetHardwareIfc(IHardwareIfc* ifc, bool owner)
    {
        if (HardwareIfcOwner && (HardwareIfc != NULL))
            delete HardwareIfc;

        HardwareIfc = ifc;
        HardwareIfcOwner = owner;
    }

    virtual IHardwareIfc* GetHardwareIfc()
    {
        return HardwareIfc;
    }

    virtual bool IsHardwareIfcOwner()  {
        return HardwareIfcOwner;
    }

    virtual void SetHardwareIfcOwner(bool owner) {
        HardwareIfcOwner = owner;
    }

    //typedef IDevice::TItemsMap TItemsMap;
    virtual const TItemsMap* GetItems();

    //typedef IDevice::TID TID;

    //typedef IDevice::TItemDesc TItemDesc;
    virtual const TItemDesc& getItemDesc(TID);


    /*struct TItemInfo {
        // declaration information
        TVIIItemType Type;
        int ID;
        std::string Desciption;
        int Width;
        int Number;
        int ParentID;
        TVIIItemWrType WrType;
        TVIIItemRdType RdType;

        // layout information
        TVI Page;
        TVI WrPos;
        TVI RdPos;
        TVI AddrPos;
        int AddrLen;
    };*/
    const TItemInfo& GetItemInfo(TID) const;

    virtual int GetDataSize() {
    	return DataSize;
    }

    uint32_t GetChecksum()
    {
        return Checksum;
    }


    friend std::ostream& operator<<(std::ostream&, TMemoryMap&);

protected:
    TVII MemoryLayout;
    uint32_t Checksum;

    TItemsMap ItemsMap;

    typedef std::map<int, TItemInfo> TItemInfoMap;
    TItemInfoMap ItemInfoMap;

    struct TBitInfo {
        int ID;
        int Width;
        int Addr;
        int Pos;
        int PosInVector;
        int VectorID;
    };
    typedef std::map<int, TBitInfo> TBitInfoMap;
    TBitInfoMap BitInfoMap;

    int AddrWidth; // in bits
    int DataWidth; // in bits
    int AddrSize; // in bytes
    int DataSize; // in bytes


    IHardwareIfc* HardwareIfc;
    bool HardwareIfcOwner;

    virtual TItemInfo& CheckReadAccess(int id, int idx=0)
    {
        if (HardwareIfc==NULL)
            throw TException("TMemoryMap: HardwareIfc==NULL");

        TItemInfoMap::iterator iItem = ItemInfoMap.find(id);
        if (iItem == ItemInfoMap.end())
            throw TException("TMemoryMap: item id not found");

        TItemInfo& item = iItem->second;

        if (item.RdType == VII_RNOACCESS)
            throw TException("TMemoryMap: no read access to item " + item.Desciption);

        if (idx >= item.Number)
            throw TException("TMemoryMap: idx out of range");

        return item;
    }

public: /* needed for generating data for flash initialization */
    virtual TItemInfo& CheckWriteAccess(int id, int idx=0)
    {
        if (HardwareIfc==NULL)
            throw TException("TMemoryMap: HardwareIfc==NULL");

        TItemInfoMap::iterator iItem = ItemInfoMap.find(id);
        if (iItem == ItemInfoMap.end())
            throw TException("TMemoryMap: item id not found");

        TItemInfo& item = iItem->second;

        if (item.WrType == VII_WNOACCESS)
            throw TException("TMemoryMap: no write access to item " + item.Desciption);

        if (idx >= item.Number)
            throw TException("TMemoryMap: idx out of range");

        return item;
    }
};

}

#endif
