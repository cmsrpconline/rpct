#ifndef _RPCT_ii_CHIPCONFIGURATION_H_
#define _RPCT_ii_CHIPCONFIGURATION_H_


#include "rpct/ii/DeviceSettings.h"

namespace rpct {

class ChipConfiguration : virtual public DeviceSettings  {    
public:
    virtual ~ChipConfiguration() {
    }
    
    virtual int getConfigurationId() = 0;
};

}

#endif 
