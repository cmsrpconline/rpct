/** Filip Thyssen */

#ifndef rpct_ii_exception_Exception_h_
#define rpct_ii_exception_Exception_h_

#include <string>

#include "tb_std.h"

namespace rpct {
namespace exception {

/** exceptions */
class Exception : public virtual TException
{
public:
    Exception(const std::string & msg = "") throw()
        : TException(std::string("Exception: ") + msg)
    {}
    virtual ~Exception() throw() {}
};

/** general null pointer exception */
class NotImplementedException : public virtual Exception
{
public:
    NotImplementedException(const std::string & msg = "") throw()
        : Exception("not implemented: " + msg)
    {}
    ~NotImplementedException() throw() {}
};
/** general null pointer exception */
class NullPointerException : public virtual Exception
{
public:
    NullPointerException(const std::string & msg = "") throw()
        : Exception("nullpointer: " + msg)
    {}
    ~NullPointerException() throw() {}
};
/** wrong function input */
class InputException : public virtual Exception
{
public:
    InputException(const std::string & msg = "") throw()
        : Exception("input: " + msg)
    {}
    ~InputException() throw() {}
};
/** system pointer problems: non existing device pointers etc. */
class SystemException : public virtual Exception
{
public:
    SystemException(const std::string & msg = "") throw()
        : Exception("system: " + msg)
    {}
    ~SystemException() throw() {}
};
/** hardware access problems */
class AccessException : public virtual Exception
{
public:
    AccessException(const std::string & msg = "") throw()
        : Exception("access: " + msg)
    {}
    ~AccessException() throw() {}
};
/** ccu exception */
class CCUException : public virtual AccessException
{
public:
    CCUException(const std::string & msg = "") throw()
        : AccessException("ccu exception: " + msg)
    {}
    ~CCUException() throw() {}
};
/** time out */
class TimeOutException : public virtual Exception
{
public:
    TimeOutException(const std::string & msg = "") throw()
        : Exception("timeout: " + msg)
    {}
    ~TimeOutException() throw() {}
};

} // namespace exception
} // namespace rpct

#endif // rpct_ii_exception_Exception_h_
