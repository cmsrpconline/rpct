#ifndef rpctBoardBaseH
#define rpctBoardBaseH

#include <vector>
#include "rpct/ii/IBoard.h"
#include "rpct/ii/HardwareItemBase.h"
#include "rpct/ii/ConfigurationSet.h"
#include "rpct/ii/Monitorable.h"
#include <log4cplus/logger.h>

namespace rpct {

class BoardBase : public HardwareItemBase, virtual public IBoard, virtual public IMonitorable {
private:
    static log4cplus::Logger logger;
protected:
    Devices devices_;
    ICrate* crate_;
    int position_;
    std::string label_;
public:
    static void setupLogger(std::string namePrefix);
    BoardBase(int id, std::string description, const HardwareItemType& type, ICrate* crate, int position)
    : HardwareItemBase(id, description, type), crate_(crate), position_(position) {
    	label_ = description_;
    }

    virtual const Devices& getDevices() {
        return devices_;
    }

    virtual ICrate* getCrate() {
        return crate_;
    }

    virtual int getPosition() {
        return position_;
    }

    // return true if cold reset must be done (e.g. QPLL not locked), otherwise false
    virtual bool checkResetNeeded();

     // return false if cold configure must be done (e.g. the config version identifier does not match, otherwise true
    virtual bool checkWarm(ConfigurationSet* configurationSet);

    virtual void configure(ConfigurationSet* configurationSet, int configureFlags);

    virtual void enable();
    virtual void disable();

    virtual void monitor(volatile bool *stop);

    virtual void checkVersions();

    virtual void setLabel(std::string label) {
    	label_ = description_ + "_" + label;
    }

    virtual const std::string& getDescription() const {
    	return label_;
    }
};

}

#endif
