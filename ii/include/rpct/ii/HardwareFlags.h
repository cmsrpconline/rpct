/** Filip Thyssen */

#ifndef rpct_ii_HardwareFlags_h_
#define rpct_ii_HardwareFlags_h_

#include "rpct/tools/Flags.h"
#include "rpct/tools/Printable.h"

#include "rpct/ii/const.h"

namespace rpct {

class HardwareFlags : public rpct::tools::Flags<4, 0x0>
                    , public virtual rpct::tools::Printable
{
public:
    void enable(bool _enable = true, bool _set = true);
    void disable(bool _disable = true);
    bool isEnabled() const;
    bool isDisabled() const;

    void working(bool _working = true, bool _set = true);
    void broken(bool _broken = true);
    bool isWorking() const;
    bool isBroken() const;

    void warm(bool _warm = true, bool _set = true);
    void cold(bool _cold = true);
    bool isWarm() const;
    bool isCold() const;

    void configured(bool _configured = true, bool _set = true);
    void notconfigured(bool _notconfigured = true);
    bool isConfigured() const;
    bool isNotConfigured() const;

    std::string getName() const;
    void printParameters(rpct::tools::Parameters & parameters) const;
public:
    enum Flag {
        flag_enabled_ = 0
        , flag_working_
        , flag_warm_
        , flag_configured_
    }; // defaults 0x0 to the normal situation

    static const int nflags_;
    static const char * setting_[4];
    static const char * name_[4][4];
};

} // namespace rpct

#include "rpct/ii/HardwareFlags-inl.h"

#endif // rpct_ii_HardwareFlags_h_
