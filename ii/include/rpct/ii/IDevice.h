//---------------------------------------------------------------------------

//
//  Michal Pietrusinski 2001
//  Warsaw University
//

#ifndef IDeviceH
#define IDeviceH
//---------------------------------------------------------------------------

#include <string>
#include <map>
#include <sstream>
#include <iomanip>
#include <boost/dynamic_bitset.hpp>
#include "rpct/ii/IHardwareItem.h"
#include "rpct/ii/DeviceSettings.h"
#include <stdint.h>

namespace rpct {

class IBoard;

class IDevice : virtual public IHardwareItem {
public:
    enum TItemType {itPage, itArea, itWord, itVect, itBits};
    enum TAccessType {atNone, atRead, atWrite, atAll};
    enum TFunction {fnStatus, fnCounter, fnHisto, fnRaw};
    typedef int TID;
    class TItemDesc {
    public:
        TItemType Type;
        TID ID;
        std::string Name;
        std::string Description;
        int Width;
        int Number;
        TID ParentID;
        TAccessType AccessType;
        TFunction Function;
    };

    class IObserver {
    public:
        virtual ~IObserver() {
        }
        virtual void DeviceAttributeBeforeWrite(IDevice* device, TID id) {
        }
        virtual void DeviceAttributeAfterWrite(IDevice* device, TID id) {
        }
    };

    virtual void registerObserver(IObserver& obs, TID id = -1) = 0;
    virtual void unregisterObserver(IObserver& obs, TID id = -1) = 0;

    virtual ~IDevice() {
    }

    virtual const std::string& getName() = 0;
    virtual uint32_t getVersion() = 0;
    virtual TID getVersionId() = 0;
    virtual IBoard& getBoard() = 0;
    virtual int getPosition() = 0;

    typedef std::map<TID, TItemDesc> TItemsMap;
    virtual const TItemsMap& getItems() = 0;
    virtual const TItemDesc& getItemDesc(TID) = 0;

    //
    // hardware operations
    //

    // word operations
    virtual uint32_t readWord(TID id, int idx) = 0;
    virtual void readWord(TID id, int idx, void* data) = 0;

    virtual void writeWord(TID id, int idx, uint32_t data) = 0;
    virtual void writeWord(TID* id, int* idx, uint32_t* data, int count) = 0;
    virtual void writeWord(TID id, int idx, void* data) = 0;
    virtual void writeWord(TID id, int idx, boost::dynamic_bitset<>& data) = 0;

    // area operations  
    static const int npos = -1;

    virtual void writeArea(TID id, void* data, int start_idx = 0, int n = npos) = 0;
    virtual void readArea(TID id, void* data, int start_idx = 0, int n = npos) = 0;
    virtual void writeArea(TID id, uint32_t* data, int start_idx = 0, int n = npos) = 0;
    virtual void readArea(TID id, uint32_t* data, int start_idx = 0, int n = npos) = 0;

    //vector operations
    virtual uint32_t readVector(TID id) = 0;
    virtual void readVector(TID id, void* data) = 0;

    virtual void writeVector(TID id, uint32_t data) = 0;
    virtual void writeVector(TID* id, uint32_t* data, int count) = 0;
    virtual void writeVector(TID id, void* data) = 0;

    // bit operations
    virtual uint32_t readBits(TID id) = 0;
    virtual void writeBits(TID id, uint32_t data, bool preread = true) = 0;

    virtual uint32_t getBitsInVector(TID bits_id, uint32_t vector_data) = 0;
    virtual void setBitsInVector(TID bits_id, uint32_t bits_data, uint32_t& vector_data) = 0;

    virtual uint32_t getBitsInVector(TID bits_id, void* vector_data) = 0;
    virtual void setBitsInVector(TID bits_id, uint32_t bits_data, void* vector_data) = 0;

    //virtual void reset() = 0;
    virtual void configure(DeviceSettings* settings, int configureFlags) = 0;
    virtual void enable() = 0;
    virtual void disable() = 0;
};

}

#endif
