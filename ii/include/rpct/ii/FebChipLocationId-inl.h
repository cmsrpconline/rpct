/** Filip Thyssen */

#ifndef rpct_ii_FebChipLocationId_inl_h_
#define rpct_ii_FebChipLocationId_inl_h_

#include "rpct/ii/FebChipLocationId.h"

namespace rpct {

inline int FebChipLocationId::getBoE() const
{
    return get(fields_boe_);
}
inline int FebChipLocationId::getWheel() const
{
    return get(fields_wheel_);
}
inline int FebChipLocationId::getDisc() const
{
    return get(fields_wheel_);
}
inline int FebChipLocationId::getLayer() const
{
    return get(fields_layer_);
}
inline int FebChipLocationId::getRing() const
{
    return get(fields_layer_);
}
inline int FebChipLocationId::getSector() const
{
    return get(fields_sector_);
}
inline int FebChipLocationId::getSubsector() const
{
    return get(fields_subsector_);
}
inline int FebChipLocationId::getRoll() const
{
    return get(fields_roll_);
}
inline int FebChipLocationId::getPartition() const
{
    return get(fields_roll_);
}
inline int FebChipLocationId::getChannel() const
{
    return get(fields_channel_);
}
inline int FebChipLocationId::getAddress() const
{
    return get(fields_address_);
}
inline int FebChipLocationId::getPosition() const
{
    return get(fields_position_);
}

inline void FebChipLocationId::setBoE(int val)
{
    set(fields_boe_, val);
}
inline void FebChipLocationId::setWheel(int val)
{
    set(fields_wheel_, val);
}
inline void FebChipLocationId::setDisc(int val)
{
    set(fields_wheel_, val);
}
inline void FebChipLocationId::setLayer(int val)
{
    set(fields_layer_, val);
}
inline void FebChipLocationId::setRing(int val)
{
    set(fields_layer_, val);
}
inline void FebChipLocationId::setSector(int val)
{
    set(fields_sector_, val);
}
inline void FebChipLocationId::setSubsector(int val)
{
    set(fields_subsector_, val);
}
inline void FebChipLocationId::setRoll(int val)
{
    set(fields_roll_, val);
}
inline void FebChipLocationId::setPartition(int val)
{
    set(fields_roll_, val);
}
inline void FebChipLocationId::setChannel(int val)
{
    set(fields_channel_, val);
}
inline void FebChipLocationId::setAddress(int val)
{
    set(fields_address_, val);
}
inline void FebChipLocationId::setPosition(int val)
{
    set(fields_position_, val);
}

} // namespace rpct

#endif // rpct_ii_FebChipLocationId_inl_h_
