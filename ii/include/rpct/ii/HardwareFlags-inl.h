/** Filip Thyssen */

#ifndef rpct_ii_HardwareFlags_inl_h_
#define rpct_ii_HardwareFlags_inl_h_

#include "rpct/ii/HardwareFlags.h"
#include "rpct/ii/const.h"

namespace rpct {

inline void HardwareFlags::enable(bool _enable, bool _set)
{
    if (_set)
        set(flag_enabled_, _enable);
    else
        unset(flag_enabled_);
}
inline void HardwareFlags::disable(bool _disable)
{
    set(flag_enabled_, !_disable);
}
inline bool HardwareFlags::isEnabled() const
{
    return is(flag_enabled_, 1);
}
inline bool HardwareFlags::isDisabled() const
{
    return is(flag_enabled_, 0);
}
inline void HardwareFlags::working(bool _working, bool _set)
{
    if (_set)
        set(flag_working_, _working);
    else
        unset(flag_working_);
}
inline void HardwareFlags::broken(bool _broken)
{
    set(flag_working_, !_broken);
}
inline bool HardwareFlags::isWorking() const
{
    return is(flag_working_, 1);
}
inline bool HardwareFlags::isBroken() const
{
    return is(flag_working_, 0);
}
inline void HardwareFlags::warm(bool _warm, bool _set)
{
    if (_set)
        set(flag_warm_, _warm);
    else
        unset(flag_warm_);
}
inline void HardwareFlags::cold(bool _cold)
{
    set(flag_warm_, !_cold);
}
inline bool HardwareFlags::isWarm() const
{
    return is(flag_warm_, 1);
}
inline bool HardwareFlags::isCold() const
{
    return is(flag_warm_, 0);
}
inline void HardwareFlags::configured(bool _configured, bool _set)
{
    if (_set)
        set(flag_configured_, _configured);
    else
        unset(flag_configured_);
}
inline void HardwareFlags::notconfigured(bool _notconfigured)
{
    set(flag_configured_, !_notconfigured);
}
inline bool HardwareFlags::isConfigured() const
{
    return is(flag_configured_, 1);
}
inline bool HardwareFlags::isNotConfigured() const
{
    return !is(flag_configured_, 0);
}

} // namespace rpct

#endif // rpct_ii_HardwareFlags_inl_h_
