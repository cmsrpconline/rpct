#include "rpct/ii/i2c/II2cAccess.h"

#include "rpct/ii/i2c/Register.h"
#include "rpct/ii/i2c/I2cResponse.h"
#include "rpct/ii/i2c/I2cRegister.h"

namespace rpct {
namespace i2c {

II2cAccess::~II2cAccess()
{}

bool II2cAccess::delay(unsigned int nseconds)
{
    try {
        this->idelay(nseconds);
        return true;
    } catch(rpct::exception::Exception & e) {
        log(&e);
        return false;
    }
}
bool II2cAccess::read(unsigned short channel
                      , unsigned char address
                      , II2cRRegister * i2c_rregister)
{
    return this->read(channel, address
                      , i2c_rregister->out(), i2c_rregister->size_out(), i2c_rregister);
}
bool II2cAccess::read(unsigned short channel
                      , unsigned char address
                      , IRRegister * rregister
                      , II2cResponse * i2c_response)
{
    return this->read(channel, address
                      , rregister->out(), rregister->size_out(), i2c_response);
}
bool II2cAccess::read(unsigned short channel
                      , unsigned char address
                      , unsigned char * data
                      , unsigned char count
                      , II2cResponse * i2c_response)
{
    try {
        this->iread(channel, address, data, count, i2c_response);
        return true;
    } catch(rpct::exception::Exception & e) {
        if (i2c_response)
            i2c_response->i2c_error(&e);
        else
            log(&e);
        return false;
    }
}
bool II2cAccess::write(unsigned short channel
                       , unsigned char address
                       , II2cWRegister * i2c_wregister)
{
    return this->write(channel, address
                       , i2c_wregister->in(), i2c_wregister->size_in(), i2c_wregister);
}
bool II2cAccess::write(unsigned short channel
                       , unsigned char address
                       , IWRegister * wregister
                       , II2cResponse * i2c_response)
{
    return this->write(channel, address
                       , wregister->in(), wregister->size_in(), i2c_response);
}
bool II2cAccess::write(unsigned short channel
                       , unsigned char address
                       , unsigned char const * data
                       , unsigned char count
                       , II2cResponse * i2c_response)
{
    try {
        this->iwrite(channel, address, data, count, i2c_response);
        return true;
    } catch(rpct::exception::Exception & e) {
        if (i2c_response)
            i2c_response->i2c_error(&e);
        else
            log(&e);
        return false;
    }
}

} // namespace i2c
} // namespace rpct
