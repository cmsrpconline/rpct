#include "rpct/ii/i2c/Register.h"

namespace rpct {
namespace i2c {

IRRegister::~IRRegister()
{}
IWRegister::~IWRegister()
{}
IRegister::~IRegister()
{}

void IRRegister::out(const unsigned char * _out)
{
    memcpy(out(), _out, size_out());
}
void IWRegister::in(const unsigned char * _in)
{
    memcpy(in(), _in, size_in());
}
void IRegister::reset()
{
    reset_in();
    reset_out();
}

} // namespace i2c
} // namespace rpct
