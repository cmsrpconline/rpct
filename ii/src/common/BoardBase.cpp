//---------------------------------------------------------------------------

//
//  Michal Pietrusinski 2006
//  Warsaw University
//

#include "rpct/ii/BoardBase.h"
#include "rpct/ii/IIDevice.h"
#include "rpct/devices/DummyVMEDevice.h" //TODO should not be like that
//---------------------------------------------------------------------------

#include "log4cplus/loggingmacros.h"

using namespace log4cplus;

namespace rpct {

Logger BoardBase::logger = Logger::getInstance("BoardBase");
void BoardBase::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}

bool BoardBase::checkResetNeeded() {

    for (Devices::iterator iDevice = devices_.begin(); iDevice != devices_.end(); iDevice++) {
        TIIDevice* d = dynamic_cast<TIIDevice*>(*iDevice);
        if (d != 0) {
            if (d->checkResetNeeded() == true) {
                return true;
            }
        }
    }
    return false;
}

bool BoardBase::checkWarm(ConfigurationSet* configurationSet) {

    for (Devices::iterator iDevice = devices_.begin(); iDevice != devices_.end(); iDevice++) {
        TIIDevice* d = dynamic_cast<TIIDevice*>(*iDevice);
        if (d != 0) {
            if (d->checkWarm(configurationSet->getDeviceSettings(*d)) == false) {
                return false;
            }
        }
    }
    return true;
}

void BoardBase::configure(ConfigurationSet* configurationSet, int configureFlags) {

    LOG4CPLUS_DEBUG(logger, "Board self test " << getDescription());
    selfTest();

    LOG4CPLUS_DEBUG(logger, "Configuring board " << getDescription());

    for (Devices::iterator iDevice = devices_.begin(); iDevice != devices_.end(); iDevice++) {
        IDevice* device = *iDevice;

        DeviceSettings* settings = (configurationSet == 0) ? 0 : configurationSet->getDeviceSettings(*device);
        LOG4CPLUS_DEBUG(logger, "Device settings for " << device->getDescription() << ": " << (settings == 0 ? "null" : "not null"));
        device->configure(settings, configureFlags);
    }
}

void BoardBase::enable() {

    LOG4CPLUS_DEBUG(logger, "Enabling board " << getDescription());

    for (Devices::iterator iDevice = devices_.begin(); iDevice != devices_.end(); iDevice++) {
        IDevice* device = *iDevice;
        device->enable();
    }
}

void BoardBase::disable() {

    LOG4CPLUS_DEBUG(logger, "Disabling board " << getDescription());

    for (Devices::iterator iDevice = devices_.begin(); iDevice != devices_.end(); iDevice++) {
        IDevice* device = *iDevice;
        device->disable();
    }
}

void BoardBase::monitor(volatile bool *stop) {
	//TODO ues the stop if needed
	for (Devices::const_iterator iDevice = devices_.begin(); iDevice != devices_.end(); ++iDevice) {
		IDevice* device = *iDevice;
		IMonitorable* monitorable = dynamic_cast<IMonitorable*> (device);
		if (monitorable != 0) {
			monitorable->monitor(stop);
		}
	}
}

void BoardBase::checkVersions() {
    std::ostringstream ost;
    EBadDeviceVersion::VersionInfoList errorList;

    const Devices& devices = getDevices();
    for (Devices::const_iterator iDevice = devices.begin(); iDevice != devices.end(); ++iDevice) {
        TIIDevice* iidev = dynamic_cast<TIIDevice*>(*iDevice);
        if (iidev != 0) {
            try {
                iidev->checkName();
                iidev->checkVersion();
                iidev->checkChecksum();
            }
            catch (EBadDeviceVersion& e) {
                //error = true;
                ost << e.what() << "\n";
                errorList.insert(errorList.end(), e.getVersionInfoList().begin(), e.getVersionInfoList().end());
            }
        } else {//TODO find other solution
            TDummyVMEDevice* vmedev = dynamic_cast<TDummyVMEDevice*>(*iDevice);
            if (vmedev != NULL) {//for DCC and CCS
                try {
                    vmedev->checkName();
                    vmedev->checkVersion();
                    //vmedev->checkChecksum();
                }
                catch(EBadDeviceVersion& e) {
                    //error = true;
                    ost << e.what() << "\n";
                    errorList.insert(errorList.end(), e.getVersionInfoList().begin(), e.getVersionInfoList().end());
                }
            }
        }
    }


    if (errorList.size() > 0) {
        throw EBadDeviceVersion(ost.str(), errorList);
    }
}

}
