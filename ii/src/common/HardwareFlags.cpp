#include "rpct/ii/HardwareFlags.h"

#include <sstream>

namespace rpct {

const int HardwareFlags::nflags_ = 4;
const char * HardwareFlags::setting_[4] = 
    {"not set","no","yes","both?"};
const char * HardwareFlags::name_[4][4] = 
    {
        {"?", "disabled", "enabled", "both?"}
        ,{"?", "broken", "working", "both?"}
        ,{"?", "cold", "warm", "both?"}
        ,{"?", "not_configured", "configured", "both?"}
};

std::string HardwareFlags::getName() const
{
    std::stringstream name;
    unsigned char * parts = split();
    if (parts)
        {
            for (int flag = 0 ; flag < nflags_ ; ++flag)
                name << ':' << name_[flag][parts[flag]];
            name << ':';
            delete[] parts;
        }
    else
        return flags_type::getName();
    return name.str();;

}

void HardwareFlags::printParameters(rpct::tools::Parameters & parameters) const
{
    unsigned char * parts = split();
    if (parts)
        {
            for (int flag = 0 ; flag < nflags_ ; ++flag)
                if (parts[flag])
                    parameters.add(name_[flag][2], setting_[parts[flag]]);
            delete[] parts;
        }
}

} // namespace rpct
