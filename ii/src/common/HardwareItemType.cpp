//---------------------------------------------------------------------------
  
//
//  Michal Pietrusinski 2006
//  Warsaw University
//

#include "rpct/ii/HardwareItemType.h"

//---------------------------------------------------------------------------

namespace rpct {
 
bool operator==(const HardwareItemType& h1, const HardwareItemType& h2) {
    return h1.category_ == h2.category_ && h1.type_ == h2.type_;
} 


bool operator<(const HardwareItemType& h1, const HardwareItemType& h2) {
    return h1.category_ < h2.category_ ||
        (!(h2.category_ < h1.category_) && h1.type_ < h2.type_);
} 

}
