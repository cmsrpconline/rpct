//---------------------------------------------------------------------------

//
//  Michal Pietrusinski 2001
//  Warsaw University
//

#include "rpct/ii/IIDevice.h"
#include "rpct/std/tbutil.h"
//---------------------------------------------------------------------------


using namespace log4cplus;

namespace rpct {

//bool TIIDevice::Tracking = false;
Logger TIIDevice::logger = Logger::getInstance("TIIDevice");
void TIIDevice::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}

std::string& rtrim(std::string& str) {
    int i;
    for (i = (str.size() - 1); i >= 0; i--) {
        if (str[i] != ' ')
            break;
    }
    if (i >= 0)
        return str.erase(i + 1);
    else
        return str;
}

TVIIItemDecl TIIDevice::createTVIIItemDecl(TVIIItemType ItemType, int ItemID, int ItemWidth, int ItemNumber,
        int ItemParentID, TVIIItemWrType ItemWrType, TVIIItemRdType ItemRdType, std::string ItemName,
        TVIIItemFun ItemFun, std::string ItemDescr) {
    TVIIItemDecl item;
    item.ItemType = ItemType;
    item.ItemID = ItemID;
    item.ItemWidth = ItemWidth;
    item.ItemNumber = ItemNumber;
    item.ItemParentID = ItemParentID;
    item.ItemWrType = ItemWrType;
    item.ItemRdType = ItemRdType;
    item.ItemName = rtrim(ItemName);
    item.ItemFun = ItemFun;
    item.ItemDescr = rtrim(ItemDescr);

    return item;
}

void TIIDevice::checkName() {
    unsigned short iname;
    try {
        iname = readWord(NameID, 0);
    } catch (TException& e) {
        std::ostringstream ostr;
        ostr << "Error while checking device name: " << e.what() << ". Device id = " << id << " description = "
                << description << " board name = " << getBoard().getDescription();
        throw TException(ostr.str().c_str());
    };

    int namelen = getItemDesc(NameID).Width / 8;
    char name[8] = { 0 };
    for (int i = 0; i < namelen; i += 2) {
        name[i] = ((char*) &iname)[i + 1];
        name[i + 1] = ((char*) &iname)[i];
    }
    std::string expName = Name.substr(Name.size() - namelen);
    //std::cout << "namelen = " << namelen << " expName = " << expName << std::endl;
    if (expName != name) {
        std::ostringstream ostr;
        ostr << "expected name: " << expName << ", read: " << name << ". Device id = " << id << " description = "
                << description << " board name = " << getBoard().getDescription();
        throw EBadDeviceVersion(ostr.str(), EBadDeviceVersion::VersionInfo(this, "NAME", expName, name));
    }
}

void TIIDevice::checkVersion() {
    uint32_t version;
    try {
        version = readWord(VersionID, 0);
    } catch (TException& e) {
        std::ostringstream ostr;
        ostr << "Error while checking device version: " << e.what() << ". Device id = " << id << " description = "
                << description << " board name = " << getBoard().getDescription();
        throw TException(ostr.str().c_str());
    };

    if (Version != version) {
        std::ostringstream ostr;
        ostr << std::hex << "expected version of chip '" << Name << "': 0x"<< Version << ", read: 0x" << version
                << ". Device id = " << std::dec << id << " description = " << description << " board name = "
                << getBoard().getDescription();
        throw EBadDeviceVersion(ostr.str(), EBadDeviceVersion::VersionInfo(this, "VERSION", rpct::toHexString(Version),
                rpct::toHexString(version)));
    }
}

void TIIDevice::checkChecksum() {
    if (ChecksumID == 0) {
        return;
    }

    uint32_t checksum;
    try {
        checksum = readWord(ChecksumID, 0);
    } catch (TException& e) {
        std::ostringstream ostr;
        ostr << "Error while reading checksum: " << e.what() << ". Device id = " << id << " description = "
                << description << " board name = " << getBoard().getDescription();
        throw TException(ostr.str().c_str());
    };

    uint32_t mask = (1 << memoryMap_->GetItemInfo(ChecksumID).Width) - 1;
    uint32_t expected = memoryMap_->GetChecksum() & mask;
    if (expected != (checksum & mask)) {
        std::ostringstream ostr;
        ostr << std::hex << "expected checksum of chip '" << Name << "': " << expected << ", read: " << checksum
                << ". Device id = " << id << " description = " << description << " board name = "
                << getBoard().getDescription();
        //throw EBadDeviceVersion(ostr.str().c_str());
        throw EBadDeviceVersion(ostr.str(), EBadDeviceVersion::VersionInfo(this, "CHECKSUM", rpct::toHexString(expected),
                rpct::toHexString(checksum)));
    }
}

std::string TIIDevice::monitorVersion() {
    std::ostringstream ostr;
    uint32_t version = readWord(VersionID, 0);
    if (Version != version) {
    	uint32_t one = 1;
        memoryMap_->GetItemInfo(VersionID).Width;
        if(version == ((one << memoryMap_->GetItemInfo(VersionID).Width) -1) ) {
            ostr << std::hex << "Read firmware version is " << version<<" firmware was most probably lost";
        }
        else
            ostr << std::hex << "Firmware version not correct, expected version is "<< Version << ", read: "<< version;
    }
    return ostr.str();
}

void TIIDevice::writeWord(TID id, int idx, boost::dynamic_bitset<>& data) {
    //TObserverRange range = NotifyObserversBeforeWrite(id);
    LOG4CPLUS_DEBUG(logger, "Write " << GetIDName(id) << "[" << idx << "] = " << data);
    char* bits = 0;
    unsigned int w = getItemDesc(id).Width;
    if (data.size() == w) {
        bits = bitsetToBits(data);
    } else if (data.size() < w) {
        boost::dynamic_bitset<> ce = data; //make a copy as data is const
        ce.resize(w, false);
        bits = bitsetToBits(ce);
    } else {
        // check if bits which excced the size are set to '0'.
        // If this is the case, forget about them.
        for (size_t i = w - 1; i < data.size(); i++) {
            if (data[w] == 1) {
                throw IllegalArgumentException("IIDevice::WriteWord: data size exceeds the limit");
            }
        }
        bits = bitsetToBits(data);
    }
    try {
        writeWord(id, idx, bits);
        delete[] bits;
    } catch (...) {
        delete[] bits;
        throw ;
    }
    //NotifyObserversAfterWrite(range, id);
}

}
