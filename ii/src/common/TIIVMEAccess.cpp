//---------------------------------------------------------------------------

#include "rpct/ii/TIIVMEAccess.h" 
#include <assert.h>

//---------------------------------------------------------------------------


using namespace std;

namespace rpct {

TIIVMEAccess::TIIVMEAccess(uint32_t board_addr, uint32_t module_addr,
		TVMEInterface* vme, uint32_t mask) :
	TIIAccess(module_addr), BoardAddr(board_addr), VME(vme) {
	Mask = ModuleAddr | (BoardAddr << 20)| mask;
	/*cout << hex << "TIIVMEAccess::TIIVMEAccess: Mask = " << Mask
	 << ", BoardAddr = " << BoardAddr
	 << ", ModuleAddr = " << ModuleAddr << endl;*/
}

void TIIVMEAccess::Write(uint32_t address, void* data,
		size_t word_count, size_t word_size) {
	//assert(word_size == 2);
	/*G_Log.out() << "TIIVMEAccess::Write address = " << address 
	 << ", word_count = " << word_count 
	 << ", word_size = " << word_size 
	 << std::endl;*/

	if ((word_count == 1) && (word_size == 2)) {
		//VME->Write16( Mask | (address << 2), *(unsigned short*) data);
		uint32_t val = *(unsigned short*) data;
		VME->Write32( Mask | (address << 2), val);
	}
	else if ((word_count == 1) && (word_size == 4)) {
		VME->Write32( Mask | (address << 2), *(uint32_t*) data);
	}
	else {
		uint32_t* vmedata = new uint32_t[word_count];
		try {
			if (word_size == 2) {
				for (size_t i=0; i<word_count; i++)
				vmedata[i] = *((unsigned short*)data+i);
			}
			else if (word_size == 4) {
				for (size_t i=0; i<word_count; i++)
				vmedata[i] = *((uint32_t*)data+i);
			}
			else
			throw TException("Not supported word size");

			VME->Write( Mask | (address << 2), (char*)vmedata, word_count * sizeof(uint32_t) );
		}
		catch(...) {
			delete [] vmedata;
			throw;
		};
		delete [] vmedata;
	}
}

void TIIVMEAccess::Write(uint32_t* addresses, void** data,
size_t* word_counts, size_t word_size, size_t count) {	
	
	// TODO now we make multiple writes only in case when all word_count are equal 1.
	// this might be extended to other word_count values
	bool allWordCountsSingle = true;
	for (size_t i = 0; i < count; i++) {
		if (word_counts[i] != 1) {
			allWordCountsSingle = false;
			break;
		}
	}
	
	if (allWordCountsSingle && (word_size == 2 || word_size == 4)) {
		
		if (word_size == 2) {
			//VME->Write16( Mask | (address << 2), *(unsigned short*) data);
			uint32_t* addr = new uint32_t[count];
			uint32_t* vals = new uint32_t[count];
			try {
				for (size_t i = 0; i < count; i++) {
					addr[i] = Mask | (addresses[i] << 2);
					vals[i] = *(unsigned short*) data[i];
					//cout << "TIIVMEAccess::Write  1 vals[" << i << "] = " << vals[i] << endl;
				}
				VME->MultiWrite32(addr, vals, count);
				delete [] addr;
				delete [] vals;
			}
			catch (...) {
				delete [] addr;
				delete [] vals;
			}
		}
		else if (word_size == 4) {
			/*for (size_t i = 0; i < count; i++) {
				cout << "TIIVMEAccess::Write c data[" << i << "] = " << (*(uint32_t*)data[i]) << endl;
			}*/
			
			//VME->Write16( Mask | (address << 2), *(unsigned short*) data);
			uint32_t* addr = new uint32_t[count];
			uint32_t* vals = new uint32_t[count];
			
			try {
				for (size_t i = 0; i < count; i++) {
					addr[i] = Mask | (addresses[i] << 2);
					vals[i] = *(uint32_t*) data[i];
					//cout << "TIIVMEAccess::Write 2 vals[" << i << "] = " << vals[i] << endl;
				}
				VME->MultiWrite32(addr, vals, count);
				delete [] addr;
				delete [] vals;
			}
			catch (...) {
				delete [] addr;
				delete [] vals;
			}
		}
	}
	else {
		for (size_t i = 0; i < count; i++) {
			Write(addresses[i], data[i], word_counts[i], word_size);
		}		
	}
}

void TIIVMEAccess::Read(uint32_t address, void* data,
size_t word_count, size_t word_size) {
	//G_Log.out() << "TIIVMEAccess::Read address = " << address << std::endl;
	uint32_t* vmedata = new uint32_t[word_count];

	try {

		if ((word_count == 1) && (word_size == 2)) {
			//*(unsigned short*) data = VME->Read16(Mask | (address << 2));
			*(unsigned short*) data = VME->Read32(Mask | (address << 2));
		}
		else if ((word_count == 1) && (word_size == 4)) {
			*(uint32_t*) data = VME->Read32(Mask | (address << 2));
		}
		else {
			VME->Read( Mask | (address << 2), (char*)vmedata, word_count * sizeof(uint32_t) );
			if (word_size == 2) {
				for(size_t i=0; i<word_count; i++) {
					*(((unsigned short*) data)+i) = vmedata[i];
				}
			}
			else if (word_size == 4) {
				for(size_t i=0; i<word_count; i++) {
					*(((uint32_t*) data)+i) = vmedata[i];
				}
			}
			else {
				throw TException("Not supported word size");
			}
		}
	}
	catch(...) {
		delete [] vmedata;
		throw;
	};
	delete [] vmedata;
}

}
