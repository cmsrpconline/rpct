/*
 * Monitorable.cpp
 *
 *  Created on: Apr 28, 2010
 *      Author: Karol Bunkowski
 */
#include "rpct/ii/Monitorable.h"
namespace rpct {
const char* MonitorItemName::CONTROL_BUS = "CONTROL_BUS";
const char* MonitorItemName::FIRMWARE = "FIRMWARE";
const char* MonitorItemName::QPLL = "QPLL";
const char* MonitorItemName::TTCRX = "TTCrx";
const char* MonitorItemName::GOL = "GOL";
const char* MonitorItemName::RECEIVER = "RECEIVER_ERR";
const char* MonitorItemName::OPT_LINK = "OPT_LINK_ERR";
const char* MonitorItemName::CONFIG_LOST = "CONFIG_LOST";
const char* MonitorItemName::RMB_DATA = "RMB_DATA";
const char* MonitorItemName::RMB_ALGORITHM = "RMB_ALGORITHM";
const char* MonitorItemName::RBC = "RBC";
const char* MonitorItemName::CCU_RING ="CCU_RING";
const char* MonitorItemName::VME = "VME";
const char* MonitorItemName::DB_SERVICE = "DB_SERVICE";
const char* MonitorItemName::EXCEPTION = "EXCEPTION";
const char* MonitorItemName::RATE = "RATE";
const char* MonitorItemName::WIN_FULL_RATIO = "WIN_FULL_RATIO";
const char* MonitorItemName::MESSAGE = "MESSAGE";
const char* MonitorItemName::RELOAD_DETECTED = "RELOAD_DETECTED";
const char* MonitorItemName::HISTOGRAMS_HALTED = "HISTOGRAMS_HALTED";

const char* MonitorableStatus::STATUS_STRINGS[] = { "OK", "SOFT_WARNING", "WARNING", "ERROR" };

std::string MonitorableStatus::getStatusStr(int level) {
	if(level > ERROR)
		throw TException("MonitorableStatus::getStatusStr: level > ERROR");
	return STATUS_STRINGS[level];
}

bool Monitorable::checkWarningExist(MonitorItem monitorItem) {
	for (Monitorable::MonitorStatusList::iterator iStat = getWarningStatusList().begin();
			iStat != getWarningStatusList().end(); ++iStat) {
		if(iStat->monitorItem == monitorItem)
			return (true);
	}
		return (false);
}

}
