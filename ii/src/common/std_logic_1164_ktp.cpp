#include "rpct/ii/std_logic_1164_ktp.h"
#include <algorithm>

//---------------------------------------------------------------------------


TI minimum(TI a, TI b)
{
  return (a < b) ? a : b;
}

//////////////////////////////////////////////////////////////////-

TI maximum(TI a, TI b)
{
  return (a > b) ? a : b;
}
       
TVI maximum(TVIV v)
{
  return *std::max_element(v.begin(), v.end());
}


TVL TVLcreate(TN arg)
{
  const TVL BIT_RANGE = 32;
  TVL sum;
  int index;

  sum = 1;
  for (index = 1; index <= BIT_RANGE; index++) {
    sum *= 2;
    if (sum > arg)
      return index;
  }
  return BIT_RANGE;
}


TN SLVMax(TN arg)
{
  //Result := ((2**TVLcreate(arg))-1);  
  return (1 << TVLcreate(arg)) - 1;
}


TP SLVPartNum(TVL vlen, TVL plen)
{
  return (vlen-1) / plen + 1;
}


TN SLVPartAddrExpand(TVL vlen, TVL plen)
{
  TP num = SLVPartNum(vlen, plen);
  if (num == 1)
    return 0;
  else
    return TVLcreate(num-1);
}
