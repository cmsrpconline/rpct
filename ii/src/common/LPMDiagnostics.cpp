#include "rpct/ii/LPMDiagnostics.h"

//---------------------------------------------------------------------------
                       

TN DIAG_DAQ_MemDataWidth(TN data, TN trg, TN mask)
{
  return TVLcreate(mask) + trg + data;
}


TN DIAG_DAQ_MemAddrWidth(TN data, TN trg, TN mask, TN abus, TN dbus)
{
  return abus+SLVPartAddrExpand(DIAG_DAQ_MemDataWidth(data,trg,mask),dbus);
}
