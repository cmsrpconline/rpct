#include "rpct/ii/IHardwareItem.h"

namespace rpct {

bool operator<(const IHardwareItem & ihi_a, const IHardwareItem & ihi_b)
{
    return (ihi_a.getType() < ihi_b.getType() ||
            (!(ihi_b.getType() < ihi_a.getType()) &&  ihi_a.getId() < ihi_b.getId()));
}
bool operator==(const IHardwareItem & ihi_a, const IHardwareItem & ihi_b)
{
    return (ihi_a.getType() == ihi_b.getType() &&
            ihi_a.getId()   == ihi_b.getId());
}

} // namespace rpct
