//---------------------------------------------------------------------------

//
//  Michal Pietrusinski 2001
//  Warsaw University
//

#include <sstream>
#include "rpct/ii/memmap.h"


using namespace std;

namespace rpct {

IDevice::TItemDesc ItemDeclToItemDesc(TVIIItemDecl decl) {
    IDevice::TItemDesc desc;

    switch(decl.ItemType) {
    case VII_PAGE: desc.Type = IDevice::itPage; break;
    case VII_AREA: desc.Type = IDevice::itArea; break;
    case VII_WORD: desc.Type = IDevice::itWord; break;
    case VII_VECT: desc.Type = IDevice::itVect; break;
    case VII_BITS: desc.Type = IDevice::itBits; break;
    default: throw TException();
    };

    desc.ID = decl.ItemID;

    //if (Trim(decl.ItemName) == "") {
    if (decl.ItemName.empty()) {
        ostringstream str;
        str << hex << desc.ID;
        desc.Name = str.str();
    }
    else {
        desc.Name = decl.ItemName.c_str();
    };

    desc.Description = decl.ItemDescr.c_str();

    desc.Width = decl.ItemWidth;

    desc.Number = decl.ItemNumber;

    desc.ParentID = decl.ItemParentID;

    if (decl.ItemRdType == VII_RNOACCESS) {
        if (decl.ItemWrType == VII_WNOACCESS)
            desc.AccessType = IDevice::atNone;
        else
            desc.AccessType = IDevice::atWrite;
    }
    else {
        if (decl.ItemWrType == VII_WNOACCESS)
            desc.AccessType = IDevice::atRead;
        else
            desc.AccessType = IDevice::atAll;
    };

    desc.Function = IDevice::fnRaw;

    return desc;
}

TMemoryMap::TMemoryMap(TDeclItems& items, int addr_width, int data_width)
        : AddrWidth(addr_width), DataWidth(data_width), HardwareIfc(0), HardwareIfcOwner(false) {
    //ofstream fout("D:\\temp\\tbcc.log", ios::out | ios::app);
    //fout << "-------------------------------------------------------" << endl;
    AddrSize = (AddrWidth-1)/8 + 1;
    DataSize = (DataWidth-1)/8 + 1;

    TDeclItems::iterator iter = items.begin();

    TVIIItemDeclList VIIItemDeclList(items.size());

    for(int i=0; iter!=items.end(); ++iter, ++i) {
        VIIItemDeclList[i] = *iter;
        //ItemDeclMap.insert(  TItemDeclMap::value_type(iter->ItemID, *iter)  );
        if (ItemsMap.insert(  TItemsMap::value_type(
                                  iter->ItemID,
                                  ItemDeclToItemDesc(*iter)    )  ).second == false)
            throw TException("TMemoryMap: item id duplicate");

        TItemInfo iinfo;
        iinfo.Type = iter->ItemType;
        iinfo.ID = iter->ItemID;
        //iinfo.Desciption = iter->
        iinfo.Width = iter->ItemWidth;
        iinfo.Number = iter->ItemNumber;
        if (iinfo.Number == 0)
            iinfo.Number = 1;

        iinfo.ParentID = iter->ItemParentID;
        iinfo.WrType = iter->ItemWrType;
        iinfo.RdType = iter->ItemRdType;

        iinfo.Page = 0;
        iinfo.WrPos = 0;
        iinfo.RdPos = 0;
        iinfo.AddrPos = 0;
        iinfo.AddrLen = 0;
        iinfo.Desciption = iter->ItemName.c_str();

        ItemInfoMap.insert( TItemInfoMap::value_type(iinfo.ID, iinfo)  );
    }

    MemoryLayout = TVIICreate(VIIItemDeclList, addr_width, data_width);
    Checksum = VIICheckSumGet(MemoryLayout);
    //TVII memoryLayout = TVIICreate(VIIItemDeclList, addr_width, data_width);

    // convert MemoryLayout to internal data format
    for (size_t i = 0; i < MemoryLayout.size() - 1; i++) {
        //fout << "***************************** " << i << "****************" << endl;
        TVIIItem& item = MemoryLayout[i];
        //ItemMap.insert(  TItemMap::value_type(item.ItemID, item)  );

        TItemInfoMap::iterator iItem = ItemInfoMap.find(item.ItemID);
        if (iItem == ItemInfoMap.end())
            throw TException("TMemoryMap: item id not found");


        // add data describing position in Internal Interface
        iItem->second.Page = item.ItemParentID;
        iItem->second.WrPos = item.ItemWrPos;
        iItem->second.RdPos = item.ItemRdPos;
        iItem->second.AddrPos = item.ItemAddrPos;
        iItem->second.AddrLen = item.ItemAddrLen;
        iItem->second.Number = item.ItemNumber;

        // take special care of bit items
        if (item.ItemType == VII_BITS) {

            TItemInfoMap::iterator iParent = ItemInfoMap.find(iItem->second.ParentID);
            if (iParent == ItemInfoMap.end())
                throw TException("TMemoryMap: parent item id not found");
            TItemInfo& parentInfo = iParent->second;

            // update parent vector information - which is not included in MemoryLayout table
            //parentInfo.Width += item.ItemWidth * item.ItemNumber;
            if (parentInfo.AddrLen == 0) { // parent info was not yet set
                parentInfo.AddrPos = item.ItemAddrPos;
                parentInfo.AddrLen = 1;
                parentInfo.Number = 1; // initially it may have been set to 0
            }
            else {
                if (item.ItemAddrPos > parentInfo.AddrPos) {
                    parentInfo.AddrLen = item.ItemAddrPos - parentInfo.AddrPos + 1;
                }
                else
                    if (item.ItemAddrPos < parentInfo.AddrPos)
                        // bit with  ItemAddrLen == 0 goes first in the MemoryLayout table so we have an error here
                        throw TException("TMemoryMap: item.ItemAddrPos < parentInfo.AddrPos");
            }

            // add bit information to BitInfoMap
            TBitInfo bitInfo;
            bitInfo.ID = item.ItemID;
            bitInfo.Width = item.ItemWidth;
            bitInfo.Addr = item.ItemAddrPos;
            bitInfo.Pos = item.ItemAddrLen;

            //bitInfo.PosInVector = -1;
            bitInfo.PosInVector = bitInfo.Pos + (bitInfo.Addr - parentInfo.AddrPos) * DataWidth;
            if (parentInfo.Width <= (bitInfo.PosInVector + bitInfo.Width - 1)) {
                parentInfo.Width = bitInfo.PosInVector + bitInfo.Width;
                if (parentInfo.Width > (sizeof(uint32_t) * 8))
                    throw TException("Vectors of size greater than 32 are not currently supported");
            }

            bitInfo.VectorID = parentInfo.ID;
            /*if (parentInfo.Desciption == "CLOCK")
              fout << " ID " << bitInfo.ID
                            << " Width " << bitInfo.Width
                            << " Addr " << bitInfo.Addr
                            << " Pos " << bitInfo.Pos
                            << " PosInVector " << bitInfo.PosInVector
                            << endl;*/
            BitInfoMap.insert( TBitInfoMap::value_type(bitInfo.ID, bitInfo) );


            // update vector read and write access types: if any bit in vector can be read/written
            // then the whole vector can be read/written
            if (item.ItemWrType != VII_WNOACCESS)
                parentInfo.WrType = item.ItemWrType;


            if (item.ItemRdType != VII_RNOACCESS)
                parentInfo.RdType = item.ItemRdType;

            TItemsMap::iterator declParentItem = ItemsMap.find(parentInfo.ID);
            if (declParentItem == ItemsMap.end())
                throw TException();


            if (item.ItemRdType != VII_RNOACCESS)
                if (declParentItem->second.AccessType == IDevice::atNone)
                    declParentItem->second.AccessType = IDevice::atRead;
                else
                    if (declParentItem->second.AccessType == IDevice::atWrite)
                        declParentItem->second.AccessType = IDevice::atAll;


            if (item.ItemWrType != VII_WNOACCESS)
                if (declParentItem->second.AccessType == IDevice::atNone)
                    declParentItem->second.AccessType = IDevice::atWrite;
                else
                    if (declParentItem->second.AccessType == IDevice::atRead)
                        declParentItem->second.AccessType = IDevice::atAll;
        };

        // update PosInVector value
        /*TBitInfoMap::iterator iBitInfo = BitInfoMap.begin();
        for(; iBitInfo != BitInfoMap.end(); ++iBitInfo) {
          int addrLen = ItemInfoMap[iBitInfo->second.VectorID].AddrLen;
          iBitInfo->second.PosInVector = iBitInfo->second.Pos + (addrLen-1)*DataWidth;
          if (ItemInfoMap[iBitInfo->second.VectorID].Desciption == "CLOCK")
            fout << " PosInVector " << iBitInfo->second.PosInVector
                          << endl;
        } */
    }
    // update PosInVector value
    /*TBitInfoMap::iterator iBitInfo = BitInfoMap.begin();
    for(; iBitInfo != BitInfoMap.end(); ++iBitInfo) {
      int addrPos = ItemInfoMap[iBitInfo->second.VectorID].AddrPos;
      iBitInfo->second.PosInVector = iBitInfo->second.Pos + (iBitInfo->second.Addr - addrPos) * DataWidth;

      //if (ItemInfoMap[iBitInfo->second.VectorID].Desciption == "CLOCK")
      //  fout << " PosInVector " << iBitInfo->second.PosInVector
      //                << endl;
    } */
}


TMemoryMap::TMemoryMap(const TVIIItemDecl* declItems, int declItemsCount, const TVIIItem* viiItems, int viiItemsCount, int addr_width, int data_width,
                       uint32_t checkSum)
        : AddrWidth(addr_width), DataWidth(data_width), HardwareIfc(0), HardwareIfcOwner(false) {

    AddrSize = (AddrWidth-1)/8 + 1;
    DataSize = (DataWidth-1)/8 + 1;

    TVIIItemDeclList VIIItemDeclList(declItemsCount);

    for (int i = 0; i < declItemsCount; ++i) {
        const TVIIItemDecl& decl = declItems[i];
        VIIItemDeclList[i] = decl;
        if (ItemsMap.insert(TItemsMap::value_type(decl.ItemID, ItemDeclToItemDesc(decl))).second == false) {
            throw TException("TMemoryMap: item id duplicate");
        }

        TItemInfo iinfo;
        iinfo.Type = decl.ItemType;
        iinfo.ID = decl.ItemID;
        //iinfo.Desciption = decl.
        iinfo.Width = decl.ItemWidth;
        iinfo.Number = decl.ItemNumber;
        if (iinfo.Number == 0)
            iinfo.Number = 1;

        iinfo.ParentID = decl.ItemParentID;
        iinfo.WrType = decl.ItemWrType;
        iinfo.RdType = decl.ItemRdType;

        iinfo.Page = 0;
        iinfo.WrPos = 0;
        iinfo.RdPos = 0;
        iinfo.AddrPos = 0;
        iinfo.AddrLen = 0;
        iinfo.Desciption = decl.ItemName.c_str();

        ItemInfoMap.insert( TItemInfoMap::value_type(iinfo.ID, iinfo)  );
    }

    Checksum = checkSum;
    for (int i = 0; i < viiItemsCount; i++) {

        const TVIIItem& item = viiItems[i];
        MemoryLayout.push_back(item);
        //ItemMap.insert(  TItemMap::value_type(item.ItemID, item)  );

        TItemInfoMap::iterator iItem = ItemInfoMap.find(item.ItemID);
        if (iItem == ItemInfoMap.end())
            throw TException("TMemoryMap: item id not found");


        // add data describing position in Internal Interface
        iItem->second.Page = item.ItemParentID;
        iItem->second.WrPos = item.ItemWrPos;
        iItem->second.RdPos = item.ItemRdPos;
        iItem->second.AddrPos = item.ItemAddrPos;
        iItem->second.AddrLen = item.ItemAddrLen;
        iItem->second.Number = item.ItemNumber;

        // take special care of bit items
        if (item.ItemType == VII_BITS) {

            TItemInfoMap::iterator iParent = ItemInfoMap.find(iItem->second.ParentID);
            if (iParent == ItemInfoMap.end())
                throw TException("TMemoryMap: parent item id not found");
            TItemInfo& parentInfo = iParent->second;

            // update parent vector information - which is not included in MemoryLayout table
            //parentInfo.Width += item.ItemWidth * item.ItemNumber;
            if (parentInfo.AddrLen == 0) { // parent info was not yet set
                parentInfo.AddrPos = item.ItemAddrPos;
                parentInfo.AddrLen = 1;
                parentInfo.Number = 1; // initially it may have been set to 0
            }
            else {
                if (item.ItemAddrPos > parentInfo.AddrPos) {
                    parentInfo.AddrLen = item.ItemAddrPos - parentInfo.AddrPos + 1;
                }
                else
                    if (item.ItemAddrPos < parentInfo.AddrPos)
                        // bit with  ItemAddrLen == 0 goes first in the MemoryLayout table so we have an error here
                        throw TException("TMemoryMap: item.ItemAddrPos < parentInfo.AddrPos");
            }

            // add bit information to BitInfoMap
            TBitInfo bitInfo;
            bitInfo.ID = item.ItemID;
            bitInfo.Width = item.ItemWidth;
            bitInfo.Addr = item.ItemAddrPos;
            bitInfo.Pos = item.ItemAddrLen;

            //bitInfo.PosInVector = -1;
            bitInfo.PosInVector = bitInfo.Pos + (bitInfo.Addr - parentInfo.AddrPos) * DataWidth;
            if (parentInfo.Width <= (bitInfo.PosInVector + bitInfo.Width - 1)) {
                parentInfo.Width = bitInfo.PosInVector + bitInfo.Width;
                if (parentInfo.Width > (int)(sizeof(uint32_t) * 8))
                    throw TException("Vectors of size greater than 32 are not currently supported");
            }

            bitInfo.VectorID = parentInfo.ID;
            /*if (parentInfo.Desciption == "CLOCK")
              fout << " ID " << bitInfo.ID
                            << " Width " << bitInfo.Width
                            << " Addr " << bitInfo.Addr
                            << " Pos " << bitInfo.Pos
                            << " PosInVector " << bitInfo.PosInVector
                            << endl;*/
            BitInfoMap.insert( TBitInfoMap::value_type(bitInfo.ID, bitInfo) );


            // update vector read and write access types: if any bit in vector can be read/written
            // then the whole vector can be read/written
            if (item.ItemWrType != VII_WNOACCESS)
                parentInfo.WrType = item.ItemWrType;


            if (item.ItemRdType != VII_RNOACCESS)
                parentInfo.RdType = item.ItemRdType;

            TItemsMap::iterator declParentItem = ItemsMap.find(parentInfo.ID);
            if (declParentItem == ItemsMap.end())
                throw TException();


            if (item.ItemRdType != VII_RNOACCESS)
                if (declParentItem->second.AccessType == IDevice::atNone)
                    declParentItem->second.AccessType = IDevice::atRead;
                else
                    if (declParentItem->second.AccessType == IDevice::atWrite)
                        declParentItem->second.AccessType = IDevice::atAll;


            if (item.ItemWrType != VII_WNOACCESS)
                if (declParentItem->second.AccessType == IDevice::atNone)
                    declParentItem->second.AccessType = IDevice::atWrite;
                else
                    if (declParentItem->second.AccessType == IDevice::atRead)
                        declParentItem->second.AccessType = IDevice::atAll;
        }
    }
}



// word operations
uint32_t TMemoryMap::readWord(int id, int idx) {
    TItemInfo& item = CheckReadAccess(id, idx);

    uint32_t result = 0;

    int bytes = item.AddrLen * DataSize;
    //int word_count = ((bytes <= sizeof(uint32_t)) ? bytes : sizeof(uint32_t))/DataSize;
    int word_count = (bytes <= sizeof(uint32_t)) ? item.AddrLen : (sizeof(uint32_t)/DataSize);

    HardwareIfc->Read(item.AddrPos + idx * item.AddrLen,
                      (unsigned char*)&result,
                      word_count,
                      DataSize);

    /*G_Debug.out() << " Word width " << item.Width
                  << " data " << result
                  << " masked " << ((item.Width < sizeof(uint32_t) * 8) ? (result & ((1 << item.Width) - 1))
           : result) << endl;*/
    uint32_t one = 1;
    return (item.Width < sizeof(uint32_t) * 8) ? (result & ((one << item.Width) - 1))
           : result;
}

void TMemoryMap::readWord(int id, int idx, void* data) {
    //cout << "TMemoryMap::ReadWord id = " << id << ", idx = " << idx << flush;
    TItemInfo& item = CheckReadAccess(id, idx);

    // create temporary buffer
    int width = (item.Width-1)/8+1;
    int size = DataSize * item.AddrLen;

    //cout << ", item.AddrPos = " << item.AddrPos << endl;

    if (width == size) {
        HardwareIfc->Read(item.AddrPos + idx * item.AddrLen,
                          data,
                          item.AddrLen,
                          DataSize);
    }
    else{
        unsigned char* buffer = new unsigned char[size];
        try {
            HardwareIfc->Read(item.AddrPos + idx * item.AddrLen,
                              buffer,
                              item.AddrLen,
                              DataSize);

            memcpy(data, buffer, width);

            delete [] buffer;
        } catch(...) {
            delete [] buffer;
            throw;
        }
    };

    // mask if needed
    int used_bits = width * 8;
    if (used_bits != item.Width) {
        unsigned char* c_data = ((unsigned char*) data) + width -1;
        (*c_data) &= (0xff >> (used_bits - item.Width));
    };
}


void TMemoryMap::writeWord(int id, int idx, uint32_t data) {
    TItemInfo& item = CheckWriteAccess(id, idx);

    size_t bytes = item.AddrLen * DataSize;
    int word_count = (bytes <= sizeof(uint32_t)) ? item.AddrLen : (sizeof(uint32_t)/DataSize);

    HardwareIfc->Write(item.AddrPos + idx * item.AddrLen,
                       &data,
                       word_count,
                       DataSize);
}

void TMemoryMap::writeWord(int* id, int* idx, uint32_t* data, size_t count) {
	uint32_t* addresses = new uint32_t[count];
	void** pdata = new void*[count];
	size_t* word_counts = new size_t[count];
	try {

		for (size_t i = 0; i < count; i++) {
			int index = idx == 0 ? 0 : idx[i];
		    TItemInfo* item = &CheckWriteAccess(id[i], index);
		    size_t bytes = item->AddrLen * DataSize;
		    word_counts[i] = (bytes <= sizeof(uint32_t)) ? item->AddrLen : (sizeof(uint32_t)/DataSize);
		    addresses[i] = item->AddrPos + index * item->AddrLen;
		    pdata[i] = &data[i];

			//cout << "TMemoryMap::writeWord pdata[" << i << "] = " << (*(uint32_t*)pdata[i]) << endl;
		}
		HardwareIfc->Write(addresses,
		                       pdata,
		                       word_counts,
		                       DataSize,
		                       count);

		delete [] addresses;
		delete [] pdata;
		delete [] word_counts;
	}
	catch (...) {
			delete [] addresses;
			delete [] pdata;
			delete [] word_counts;
			throw;
	}
}

void TMemoryMap::writeWord(int id, int idx, void* data) {
    TItemInfo& item = CheckWriteAccess(id, idx);

    HardwareIfc->Write(item.AddrPos + idx * item.AddrLen,
                       data,
                       item.AddrLen,
                       DataSize);
}



// area operations
void TMemoryMap::writeArea(int id, void* data, int start_idx, int n) {
    TItemInfo& item = CheckWriteAccess(id);

    if ((start_idx < 0) || (start_idx >= item.Number))
        throw TException("TMemoryMap::writeArea: start_idx out of bounds");

    if (n == npos)
        n = getItemDesc(id).Number - start_idx;
    else
        if ((n < 0) || ((n + start_idx) > item.Number))
            throw TException("TMemoryMap::writeArea: n out of scope");

    // create temporary buffer
    int width = (item.Width-1)/8+1;
    int size = DataSize * item.Number * item.AddrLen;
    unsigned char* buffer = new unsigned char[size];

    int blockLen = 1;
    while (blockLen  < item.Number)
        blockLen <<= 1;
    try {

        // reorganize byte order according to DataSize
        for(int block=0; block<item.AddrLen; block++) {

            // copy data appropriate for current block
            memset(buffer, 0, size);

            //int bytes = ((width - block*DataSize) - 1)/DataSize + 1;
            int bytes = width - block*DataSize;
            bytes = (bytes >= DataSize) ? DataSize : bytes;

            for(int i=0; i<n; i++)
                for(int byte=0; byte<bytes; byte++)
                    buffer[i*DataSize + byte] = ((unsigned char*)data)[i*width + block*DataSize + byte];

            /*HardwareIfc->Write(item.AddrPos + block*item.Number + start_idx,
                              buffer,
                              n,
                              DataSize);*/
            HardwareIfc->Write(item.AddrPos + block*blockLen + start_idx,
                               buffer,
                               n,
                               DataSize);

        };

        delete [] buffer;
    } catch(...) {
        delete [] buffer;
        throw;
    }
}

void TMemoryMap::readArea(int id, void* data, int start_idx, int n) {
    TItemInfo& item = CheckReadAccess(id);

    if ((start_idx < 0) || (start_idx >= item.Number))
        throw TException("TMemoryMap::readArea: start_idx out of bounds");

    if (n == npos)
        n = getItemDesc(id).Number - start_idx;
    else
        if ((n < 0) || ((n + start_idx) > item.Number))
            throw TException("TMemoryMap::readArea: n out of scope");

    // create temporary buffer
    int width = (item.Width-1)/8+1;
    int size = DataSize * n * item.AddrLen;
    unsigned char* buffer = new unsigned char[size];

    int blockLen = 1;
    while (blockLen  < item.Number)
        blockLen <<= 1;

    try {

        memset(data, 0, width*n);

        // reorganize byte order according to DataSize
        for(int block=0; block<item.AddrLen; block++) {

            // copy data appropriate for current block
            /*HardwareIfc->Read(item.AddrPos + block * item.Number + start_idx,
                              buffer,
                              n,
                              DataSize); */

            HardwareIfc->Read(item.AddrPos + block * blockLen + start_idx,
                              buffer,
                              n,
                              DataSize);

            //int bytes = ((width - block*DataSize) - 1)/DataSize + 1;
            int bytes = width - block*DataSize;
            bytes = (bytes >= DataSize) ? DataSize : bytes;

            for(int i=0; i<n; i++)
                for(int byte=0; byte<bytes; byte++)
                    ((unsigned char*)data)[i*width + block*DataSize + byte] = buffer[i*DataSize + byte];


        };

        delete [] buffer;
    } catch(...) {
        delete [] buffer;
        throw;
    }
}


// area operations
void TMemoryMap::writeArea(int id, uint32_t* data, int start_idx, int n) {
    TItemInfo& item = CheckWriteAccess(id);

    if (item.Width > sizeof(uint32_t)*8)
        throw TException("TMemoryMap::writeArea: area too wide to fit data to uint32_t");

    if ((start_idx < 0) || (start_idx >= item.Number))
        throw TException("TMemoryMap::writeArea: start_idx out of bounds");

    if (n == npos)
        n = getItemDesc(id).Number - start_idx;
    else
        if ((n < 0) || ((n + start_idx) > item.Number))
            throw TException("TMemoryMap::writeArea: n out of scope");

    // create temporary buffer
    int size = DataSize * item.Number * item.AddrLen;
    unsigned char* buffer = new unsigned char[size];
    int width = (item.Width-1)/8+1;

    int blockLen = 1;
    while (blockLen  < item.Number)
        blockLen <<= 1;

    //cout << "writeArea: blockLen = " << dec << blockLen
    //     << "item.AddrLen = " << item.AddrLen << endl;

    try {

        // reorganize byte order according to DataSize
        for(int block=0; block<item.AddrLen; block++) {

            // copy data appropriate for current block
            memset(buffer, 0, size);

            //int bytes = ((width - block*DataSize) - 1)/DataSize + 1;
            int bytes = width - block*DataSize;
            bytes = (bytes >= DataSize) ? DataSize : bytes;

            for(int i=0; i<n; i++)
                for(int byte=0; byte<bytes; byte++)
                    buffer[i*DataSize + byte] = ((unsigned char*)data)[i*sizeof(uint32_t) + block*DataSize + byte];

            /*HardwareIfc->Write(item.AddrPos + block * item.Number + start_idx,
                               buffer,
                               n,
                               DataSize);*/
            HardwareIfc->Write(item.AddrPos + block * blockLen + start_idx,
                               buffer,
                               n,
                               DataSize);

        };

        delete [] buffer;
    } catch(...) {
        delete [] buffer;
        throw;
    }
}

void TMemoryMap::readArea(int id, uint32_t* data, int start_idx, int n) {
    TItemInfo& item = CheckReadAccess(id);

    if (item.Width > sizeof(uint32_t)*8)
        throw TException("TMemoryMap::writeArea: area too wide to fit data to uint32_t");

    if ((start_idx < 0) || (start_idx >= item.Number))
        throw TException("TMemoryMap::readArea: start_idx out of bounds");

    if (n == npos)
        n = getItemDesc(id).Number - start_idx;
    else
        if ((n < 0) || ((n + start_idx) > item.Number))
            throw TException("TMemoryMap::readArea: n out of scope");

    // create temporary buffer
    int width = (item.Width-1)/8+1;
    int size = DataSize * n * item.AddrLen;
    unsigned char* buffer = new unsigned char[size];

    int blockLen = 1;
    while (blockLen  < item.Number)
        blockLen <<= 1;

    try {

        // copy data appropriate for current block
        memset(data, 0, sizeof(uint32_t) * n);

        // reorganize byte order according to DataSize
        for(int block=0; block<item.AddrLen; block++) {

            /*HardwareIfc->Read(item.AddrPos + block*item.Number + start_idx,
                              buffer,
                              n,
                              DataSize);*/
            HardwareIfc->Read(item.AddrPos + block * blockLen + start_idx,
                              buffer,
                              n,
                              DataSize);

            //int bytes = ((width - block*DataSize) - 1)/DataSize + 1;
            int bytes = width - block*DataSize;
            bytes = (bytes >= DataSize) ? DataSize : bytes;

            for(int i=0; i<n; i++)
                for(int byte=0; byte<bytes; byte++)
                    ((unsigned char*)data)[i*sizeof(uint32_t) + block*DataSize + byte] = buffer[i*DataSize + byte];


        };

        delete [] buffer;
    } catch(...) {
        delete [] buffer;
        throw;
    }
}



//vector operations
uint32_t TMemoryMap::readVector(int id) {
    return readWord(id, 0);
}

void TMemoryMap::readVector(int id, void* data) {
    readWord(id, 0, data);
}


void TMemoryMap::writeVector(int id, uint32_t data) {
    //G_Debug.out() << "writeVector " << id << ", " << data << endl;
    writeWord(id, 0, data);
}

void TMemoryMap::writeVector(int* id, uint32_t* data, size_t count) {
    //G_Debug.out() << "writeVector " << id << ", " << data << endl;
	/*for (size_t i = 0; i < count; i++) {
		cout << "TMemoryMap::writeVector data[" << i << "] = " << data[i] << endl;
	}*/
    writeWord(id, 0, data, count);
}

void TMemoryMap::writeVector(int id, void* data) {
    writeWord(id, 0, data);
}


// bit operations
uint32_t TMemoryMap::readBits(int id) {
    TItemInfo& item = CheckReadAccess(id, 0);

    // temporarily we will not handle the following
    if (DataSize > sizeof(uint32_t))
        throw TException("TMemoryMap::ReadBit DataSize > sizeof(uint32_t)");

    uint32_t data;

    HardwareIfc->Read(item.AddrPos,
                      &data,
                      1,
                      DataSize);

    //return (data >> item.AddrLen) & ((1 << item.Width) -1);

    data >>= item.AddrLen;

    return (item.Width < sizeof(uint32_t) * 8) ? (data & ((1 << item.Width) - 1))
           : data;
}

void TMemoryMap::writeBits(int id, uint32_t data, bool preread) {
    TItemInfo& item = CheckWriteAccess(id, 0);

    // temporarily we will not handle the following
    if (DataSize > sizeof(uint32_t))
        throw TException("TMemoryMap::WriteBit DataSize > sizeof(uint32_t)");

    uint32_t mask = (1 << item.Width) - 1;
    uint32_t data1;
    if (preread) {
        HardwareIfc->Read(item.AddrPos,
                          &data1,
                          1,
                          DataSize);
        data1 &= ~ (mask << item.AddrLen);
        data1 |= ((data & mask) << item.AddrLen);
    }
    else
        data1 = ((data & mask) << item.AddrLen);

    HardwareIfc->Write(item.AddrPos,
                       &data1,
                       1,
                       DataSize);
}


uint32_t TMemoryMap::getBitsInVector(int bits_id, uint32_t vector_data) {
    // get the bit info
    TBitInfoMap::iterator iBitInfo = BitInfoMap.find(bits_id);
    if (iBitInfo == BitInfoMap.end())
        throw TException("TMemoryMap: item id not found");

    int pos = iBitInfo->second.PosInVector;
    int mask = ((1 << iBitInfo->second.Width) -1);
    /*G_Debug.out() << " ID " << iBitInfo->second.ID
                  << " pos " << pos
                  << " mask " << mask
                  << " vector data " << vector_data
                  << " moved " << (vector_data >> pos)
                  << " masked " << ((vector_data >> pos) & mask) << endl;*/
    return (vector_data >> pos) & mask;
}

void TMemoryMap::setBitsInVector(int bits_id, uint32_t bits_data, uint32_t& vector_data) {
    // get the bit info
    TBitInfoMap::iterator iBitInfo = BitInfoMap.find(bits_id);
    if (iBitInfo == BitInfoMap.end())
        throw TException("TMemoryMap: item id not found");

    int pos = iBitInfo->second.PosInVector;
    int mask = ((1 << iBitInfo->second.Width) -1);
    //int mask = ((1 << iBitInfo->second.Pos) -1);
    //G_Debug.out() << "pos " << pos << " mask " << mask << "bits_data" << bits_data << " data " << vector_data;
    vector_data &= ~(mask << pos);
    vector_data |= ((bits_data & mask) << pos);
    //G_Debug.out() << " data " << vector_data << endl;
}

uint32_t TMemoryMap::getBitsInVector(int bits_id, void* vector_data) {
    throw TException("TMemoryMap::getBitsInVector not implemented");
}


void TMemoryMap::setBitsInVector(int bits_id, uint32_t bits_data, void* vector_data) {
    throw TException("TMemoryMap::setBitsInVector not implemented");
}

/*

TVIIItemDecl& TMemoryMap::GetItemDeclInfo(int id)
{
      TItemDeclMap::iterator iItem = ItemDeclMap.find(id);
      if (iItem == ItemDeclMap.end())
        throw TException("TMemoryMap: item id not found");

      return iItem->second;
}

TMemoryMap::TItemDeclMap TMemoryMap::GetItemDecls()
{
  return ItemDeclMap;
}      */


const TMemoryMap::TItemsMap* TMemoryMap::GetItems() {
    return &ItemsMap;
}

const TMemoryMap::TItemDesc& TMemoryMap::getItemDesc(IDevice::TID id) {
    TItemsMap::iterator iItem = ItemsMap.find(id);
    if (iItem == ItemsMap.end())
        throw TException("TMemoryMap: item id not found");

    return iItem->second;
}

const TMemoryMap::TItemInfo& TMemoryMap::GetItemInfo(IDevice::TID id) const {
    TItemInfoMap::const_iterator iItem = ItemInfoMap.find(id);
    if (iItem == ItemInfoMap.end())
        throw TException("TMemoryMap: item id not found");

    return iItem->second;
}


ostream& operator<<(ostream& ost, TMemoryMap& map) {
    TVII* layout = &map.MemoryLayout;
    TVIIItem* item;

    for(int i=0; i < layout->size(); i++) {
        item = &((*layout)[i]);
        ost << i << ": "
        << " ItemType = " << item->ItemType
        << " ItemID = "   << item->ItemID
        << " ItemParentID = "   << item->ItemParentID
        << " ItemWidth = "   << item->ItemWidth
        << " ItemNumber = "   << item->ItemNumber
        << " ItemWrType = "   << item->ItemWrType
        << " ItemWrPos = "   << item->ItemWrPos
        << " ItemRdType = "   << item->ItemRdType
        << " ItemRdPos = "   << item->ItemRdPos
        << " ItemAddrPos = "   << item->ItemAddrPos
        << " ItemAddrLen = "   << item->ItemAddrLen
        << "\n";
    };

    for(TMemoryMap::TBitInfoMap::iterator iMap = map.BitInfoMap.begin(); iMap != map.BitInfoMap.end(); ++iMap){
        ost << " ID = " << iMap->second.ID
        << " Addr = " << iMap->second.Addr
        << " Pos = " << iMap->second.Pos
        << " PosInVector = " << iMap->second.PosInVector
        << " VectorID = " << iMap->second.VectorID
        << "\n";
    }

    return ost;
}


}
//---------------------------------------------------------------------------

