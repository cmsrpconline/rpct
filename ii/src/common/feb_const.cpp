#include "rpct/ii/feb_const.h"

namespace rpct {

namespace type {
namespace feb {

const char * febchip_   = "FebChip";
const char * febboard_  = "FebBoard";
const char * febpart_   = "FebPart";
const char * febdistributionboard_ = "FebDistributionBoard";
const char * febaccesspoint_    = "FebAccessPoint";
const char * rpcfebaccesspoint_ = "RPCFebAccessPoint";
const char * dtfebaccesspoint_  = "DTFebAccessPoint";
const char * febsystem_    = "FebSystem";
const char * rpcfebsystem_ = "RPCFebSystem";
const char * dtfebsystem_  = "DTFebSystem";

const char * febmonitorunit_ = "FebDistributionBoard";

} // namespace feb
} // namespace type

namespace preset {
namespace feb {

const float unit_ = 2.5/1.024;
const float inv_unit_ = 1.024 / 2.5;

const uint16_t vth_ = 90;
const uint16_t vmon_ = 1434;
// maximum difference between two values to be written at once
const uint16_t vth_max_diff_ = 1;
const uint16_t vmon_max_diff_ = 4;

// maximum difference between requested and desired value
const uint16_t vth_max_offset_ = 2;
const uint16_t vmon_max_offset_ = 20;

const uint16_t vth_max_offset_error_ = 10;
const uint16_t vmon_max_offset_error_ = 40;

const uint16_t vth_max_offset_offset_ = 2;
const uint16_t vmon_max_offset_offset_ = 4;

const int16_t offset_keep_ = 0x400;
const int16_t offset_ = 0;

const unsigned int nchips_ = 2;
const unsigned int nparts_[] = {1,2};

const float min_temp_ = 10;
const float max_temp_ = 32;

} // namespace feb
} // namepace preset

namespace monitoritem {
namespace feb {

const char * pca9544_    = "pca9544";
const char * pca9544_dt_ = "pca9544_dt";
const char * pcf8574a_   = "pcf8574a";
const char * vth_        = "vth";
const char * vth_offset_ = "vth_offset";
const char * vmon_       = "vmon";
const char * vmon_offset_= "vmon_offset";
const char * temp_       = "temp";

} // namespace feb
} // namespace preset

} // namespace rpct
