#include "rpct/ii/FebChipLocationId.h"

#include <sstream>
#include <iomanip>

namespace rpct {

namespace febchiplocationid {

const int nfields_ = 9;
const int pos_[]
= { 30, 26, 23,  17, 14, 11,  7,  3,  0};
const int mask_[]
= {0xC0000000
   , 0x3C000000
   , 0x03800000
   , 0x007E0000
   , 0x0001C000
   , 0x00003800
   , 0x00000780
   , 0x00000078
   , 0x00000007};
const int offset_[]
= {  1,  5,  0,   0,  0,  0,  0,  1,  1};

} // namespace febchiplocationid

const char * FebChipLocationId::boe_[4]
= {"*", "W", "RE", "x"};
const char * FebChipLocationId::layer_[4][8]
= {
    {  "*", "1"    , "2"     , "3"    , "4"     , "5"  , "6"   , "x"}
    , {"*", "RB1in", "RB1out", "RB2in", "RB2out", "RB3", "RB4" , "x"}
    , {"*", "1"    , "2"     , "3"    , "x"     , "x"  , "x"   , "x"}
    , {"*", "1"    , "2"     , "3"    , "4"     , "5"  , "6"   , "x"}
};
const char * FebChipLocationId::subsector_[4][8]
= {
    {  "*", "1", "2", "3" , "4", "5", "6" , "x"}
    , {"" , "-", "+", "--", "-", "+", "++", "x"}
    , {"" , "x", "x", "x" , "x", "x", "x" , "x"}
    , {"*", "1", "2", "3" , "4", "5", "6" , "x"}
};
const char * FebChipLocationId::roll_[4][8]
= {
    {  "*", "1"       , "2"      , "3"      , "4", "x", "x", "x"}
    , {"*", "Backward", "Central", "Forward", "x", "x", "x", "x"}
    , {"*", "A"       , "B"      , "C"      , "D", "x", "x", "x"}
    , {"*", "1"       , "2"      , "3"      , "4", "x", "x", "x"}
};

FebChipLocationId::FebChipLocationId(uint32_t id)
    : lid_type(id)
{}
std::string FebChipLocationId::getName() const
{
    std::stringstream name;
    uint32_t * parts = split();
    if (parts)
        {
            int boe = parts[fields_boe_];
            name << boe_[boe];
            if (parts[fields_wheel_])
	        {
                    int wheel = parts[fields_wheel_] - febchiplocationid::offset_[fields_wheel_];
                    if (wheel > 0)
                        name << '+';
                    name << wheel;
                }
            else
                name << '*';
            name << '/' << layer_[boe][parts[fields_layer_]] << '/';
            if (parts[fields_sector_])
                name << (parts[fields_sector_] - febchiplocationid::offset_[fields_sector_]);
            else
                name << '*';
            name << subsector_[boe][parts[fields_subsector_]];
            if (parts[fields_roll_])
                name << '_' << roll_[boe][parts[fields_roll_]];
            if (parts[fields_channel_] || parts[fields_address_])
                {
                    name << ':';
                    if (parts[fields_channel_])
                        name << std::hex << (parts[fields_channel_] - febchiplocationid::offset_[fields_channel_]);
                    else
                        name << '*';
                    name << '.';
                    if (parts[fields_address_])
                        name << std::hex << (parts[fields_address_] - febchiplocationid::offset_[fields_address_]);
                    else
                        name << '*';
                }
            if (parts[fields_position_])
                name << '#' << (parts[fields_position_] - febchiplocationid::offset_[fields_position_]);
            delete[] parts;
        }
    else
        return lid_type::getName();
    return name.str();
}

} // namespace rpct
