#include "rpct/ii/VComponent.h"

//---------------------------------------------------------------------------

/*int intpow(int x, int y)
{
  if (y == 0)
    return 1;

  for (int i = 1; i < y; i++)
    x *= x;
} */


TVII __TVIIItemListGen(TVIIItemDeclList list, TN addr_width, TN data_width,
                       TN page_num, TN item_num, TN vect_num)
{
  TVII par(item_num+1);
  TVI par_index;
  TVIV page_vec(page_num);
  TVIV page_len_vec(page_num);
  TVIV vect_vec(maximum(vect_num,1));
  TVIV vect_width(vect_vec.size());
  TVIV vect_addr(vect_vec.size());;
  TVI page_cnt;
  TVI vect_cnt;
  TN item_id;
  int index, vect_index, page_index;
  TVI tmp_val;

  // physical items (without pages) setting
  par_index = VEC_INDEX_MIN;
  for (index = 0; index < list.size(); index++) {
    if ((list[index].ItemType == VII_AREA)
        || (list[index].ItemType == VII_WORD)  
        || (list[index].ItemType == VII_BITS)) {
      par[par_index].ItemType     = list[index].ItemType;
      par[par_index].ItemID       = list[index].ItemID;
      par[par_index].ItemParentID = list[index].ItemParentID;
      par[par_index].ItemWidth    = list[index].ItemWidth;
      par[par_index].ItemNumber   = list[index].ItemNumber;
      par[par_index].ItemWrType   = list[index].ItemWrType;
      par[par_index].ItemRdType   = list[index].ItemRdType;
      par[par_index].ItemWrPos    = NO_VEC_INDEX;
      par[par_index].ItemRdPos    = NO_VEC_INDEX;
      par[par_index].ItemAddrPos  = NO_VEC_INDEX;
      par[par_index].ItemAddrLen  = NO_VEC_LEN;
      par_index = par_index +1;
    }
  }

  // interconnections for vectors
  if (vect_num > 0) {
    vect_cnt = VEC_INDEX_MIN;
    for (index = 0; index < list.size(); index++)
      if (list[index].ItemType == VII_VECT) {
        vect_vec[vect_cnt]   = index;
        vect_width[vect_cnt] = VEC_INDEX_MIN;
        vect_addr[vect_cnt]  = NO_VEC_INDEX;
        vect_cnt             = vect_cnt+1;
      }

    for (vect_index = 0; vect_index < vect_vec.size(); vect_index++) {
      item_id = list[vect_vec[vect_index]].ItemID;
      for (index = 0; index < (par.size() - 1); index++) {
        if (par[index].ItemType == VII_BITS) {
          if (par[index].ItemParentID == item_id) {
            par[index].ItemAddrLen = par[index].ItemNumber * par[index].ItemWidth;
            if (par[index].ItemAddrLen <= data_width) {
              par[index].ItemParentID = vect_index;
              par[index].ItemAddrLen = par[index].ItemAddrLen+vect_width[vect_index]-1;
              if (vect_width[vect_index] / data_width != par[index].ItemAddrLen / data_width)
                vect_width[vect_index] = (par[index].ItemAddrLen/data_width) * data_width;
              par[index].ItemAddrLen = vect_width[vect_index];
              vect_width[vect_index] = vect_width[vect_index] + par[index].ItemNumber*par[index].ItemWidth;
            }
            else
              par[index].ItemParentID = NO_VEC_INDEX; // ! can't divide common bits
          }
        }
      }
    }
  }

  // interconnection for pages;
  for (index = 0; index < page_len_vec.size(); index++)
    page_len_vec[index] = VEC_INDEX_MIN;

  page_cnt = VEC_INDEX_MIN;
  for (index = 0; index < list.size(); index++) {
    if (list[index].ItemType == VII_PAGE) {
        page_vec[page_cnt] = index;
        page_cnt = page_cnt+1;
    }
  }

  for (page_index = 0; page_index < page_vec.size(); page_index++) {
    item_id = list[page_vec[page_index]].ItemID;
    for (index = 0; index < (par.size() - 1); index++) {
      if (par[index].ItemAddrPos == NO_VEC_INDEX)
        if (par[index].ItemType == VII_WORD) {
            if (par[index].ItemParentID == item_id) {
              par[index].ItemAddrPos = page_len_vec[page_index];
              par[index].ItemAddrLen = (par[index].ItemWidth-1) / data_width + 1;
              page_len_vec[page_index] =  page_len_vec[page_index] + (par[index].ItemAddrLen*par[index].ItemNumber);
              par[index].ItemParentID = page_index;
            }
        }
        else if (par[index].ItemType == VII_BITS) {
          if (list[vect_vec[par[index].ItemParentID]].ItemParentID == item_id) {
            if (vect_addr[par[index].ItemParentID] == NO_VEC_INDEX) {
              vect_addr[par[index].ItemParentID] = page_len_vec[page_index];
              page_len_vec[page_index] =  page_len_vec[page_index] + (vect_width[par[index].ItemParentID]-1) / data_width + 1;
            }
            par[index].ItemAddrPos  = vect_addr[par[index].ItemParentID] + par[index].ItemAddrLen / data_width;
            par[index].ItemAddrLen  = par[index].ItemAddrLen - (par[index].ItemAddrLen / data_width) * data_width;
            par[index].ItemParentID = page_index;
          }
        }
        else if (par[index].ItemType == VII_AREA) {
          if (par[index].ItemParentID == item_id) {
            par[index].ItemAddrLen = (par[index].ItemWidth-1) / data_width + 1;
            //par[index].ItemNumber = SLVMax(par[index].ItemNumber - 1) + 1;
            //tmp_val = (SLVMax(par[index].ItemNumber-1)+1)*par[index].ItemAddrLen;
            tmp_val = SLVMax((SLVMax(par[index].ItemNumber-1)+1)*par[index].ItemAddrLen-1)+1;//!!2004-10A
                                        
            if (page_len_vec[page_index] == 0)                                                   // 2004-06
              par[index].ItemAddrPos = 0;                                                        // 2004-06
            else                                                                                 // 2004-06
              //par[index].ItemAddrPos = SLVPartNum(page_len_vec[page_index],                      // 2004-06
              //                                    par[index].ItemNumber)*par[index].ItemNumber;  // 2004-06
              par[index].ItemAddrPos = SLVPartNum(page_len_vec[page_index],
                                                   tmp_val)*tmp_val;

                              
            //page_len_vec[page_index] = par[index].ItemAddrPos +
            //       (par[index].ItemNumber*par[index].ItemAddrLen); // 2004-06
            page_len_vec[page_index] =  par[index].ItemAddrPos+tmp_val;

            // !! important: area can't be divided on parts
            par[index].ItemParentID = page_index;
          }
        }
    }                 
  }

  // calculates external addresses for each page
  //page_cnt := Round(IntPower(2, TVLcreate(maximum(page_len_vec)-1)));
  page_cnt = 1 << TVLcreate(maximum(page_len_vec)-1);
  for (page_index = 0; page_index < page_vec.size(); page_index++) {
    par_index = page_cnt * page_index;
    for (index = 0; index < (par.size() - 1); index++) {
      if ((par[index].ItemType == VII_AREA)
          || (par[index].ItemType == VII_WORD)
          || (par[index].ItemType == VII_BITS))
        if (par[index].ItemParentID == page_index)
          par[index].ItemAddrPos = par[index].ItemAddrPos + par_index;
    }
  }

  // calculates positions in vector and vector lenhth
  par_index = VEC_INDEX_MIN;
  for (index = 0; index < (par.size() - 1); index++) {
    if ((par[index].ItemType == VII_WORD)
        || (par[index].ItemType == VII_BITS)) {
      if (par[index].ItemWrType == VII_WACCESS) {
        par[index].ItemWrPos = par_index;
        par_index = par_index + par[index].ItemNumber * par[index].ItemWidth;
      }
      if (par[index].ItemRdType == VII_RINTERNAL)
        par[index].ItemRdPos = par[index].ItemWrPos;
      else if (par[index].ItemRdType == VII_REXTERNAL) {
        par[index].ItemRdPos = par_index;
        par_index = par_index + par[index].ItemNumber * par[index].ItemWidth;
      }
    }
    else if (par[index].ItemType == VII_AREA) {
          if (par[index].ItemWrType == VII_WACCESS) {
            par[index].ItemWrPos = par_index;
            par_index = par_index + minimum(par[index].ItemWidth, data_width);
          }
          if (par[index].ItemRdType == VII_REXTERNAL) {
            par[index].ItemRdPos = par_index;
            par_index = par_index + minimum(par[index].ItemWidth, data_width);
          }
    }
  }
  // special last page stores global parametres
  par[par.size()-1].ItemType = VII_PAGE;
  par[par.size()-1].ItemWidth = data_width;
  par[par.size()-1].ItemNumber = addr_width;
  par[par.size()-1].ItemAddrPos = par_index;
  return par;
}


TS VIINameConv(TS name)
{
  name.resize(VII_ITEM_NAME_LEN, ' ');
  return name;
}

TS VIIDescrConv(TS name)
{
  name.resize(VII_ITEM_DESCR_LEN, ' ');
  return name;
}



TVII TVIICreate(TVIIItemDeclList list, int addr_width, int data_width)
{
  TN page_num = 0;
  TN item_num = 0;
  TN vect_num = 0;
  for (int index = 0; index < list.size(); index++) {
    switch(list[index].ItemType) {
      case VII_PAGE:
        page_num = page_num+1;
        break;
      case VII_AREA:
      case VII_WORD:
      case VII_BITS:
        item_num = item_num+1;
        break;
      case VII_VECT:
        vect_num = vect_num+1;
    }
  }
  return __TVIIItemListGen(list,addr_width,data_width,page_num,item_num,vect_num);
}


TN VIICheckSumGet(TVII par)
{
    TN SumVar;
    TN CheckSumVar;

    CheckSumVar = 0;                    
    for (int index = 0; index < par.size()-1; index++) {
      switch (par[index].ItemType) {
        case VII_PAGE: SumVar = 0; break;
        case VII_AREA: SumVar = 1; break;
        case VII_WORD: SumVar = 2; break;
        case VII_BITS: SumVar = 3; break;
        case VII_VECT: SumVar = 0; break;
        default:       SumVar = 0;
      }

      SumVar = par[index].ItemWidth * par[index].ItemNumber * SumVar;
      
      switch (par[index].ItemWrType) {
        case VII_WNOACCESS:  SumVar = SumVar+123; break;
        case VII_WACCESS:    SumVar = SumVar+765; break;
        default:             SumVar = SumVar+0;
      }

      switch (par[index].ItemRdType) {
        case VII_RNOACCESS: SumVar = SumVar+432; break;
        case VII_REXTERNAL: SumVar = SumVar+326; break;
        case VII_RINTERNAL: SumVar = SumVar+739; break;
        default:            SumVar = SumVar+0;
      }

      SumVar = SumVar + par[index].ItemAddrPos * 13;
      SumVar = SumVar + par[index].ItemAddrLen * 7;
      CheckSumVar = CheckSumVar + SumVar;
    }
    return CheckSumVar;
}
