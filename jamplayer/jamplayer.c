/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * MAIN function (moved by MB from jamstub_wz.c)
 *
 */

#include "jamstub_wz.h"
#include "jamexprt.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>

/* GLobal variables */
char *error_text[] =
  {
    /* JAMC_SUCCESS            0 */ "success",
    /* JAMC_OUT_OF_MEMORY      1 */ "out of memory",
    /* JAMC_IO_ERROR           2 */ "file access error",
    /* JAMC_SYNTAX_ERROR       3 */ "syntax error",
    /* JAMC_UNEXPECTED_END     4 */ "unexpected end of file",
    /* JAMC_UNDEFINED_SYMBOL   5 */ "undefined symbol",
    /* JAMC_REDEFINED_SYMBOL   6 */ "redefined symbol",
    /* JAMC_INTEGER_OVERFLOW   7 */ "integer overflow",
    /* JAMC_DIVIDE_BY_ZERO     8 */ "divide by zero",
    /* JAMC_CRC_ERROR          9 */ "CRC mismatch",
    /* JAMC_INTERNAL_ERROR    10 */ "internal error",
    /* JAMC_BOUNDS_ERROR      11 */ "bounds error",
    /* JAMC_TYPE_MISMATCH     12 */ "type mismatch",
    /* JAMC_ASSIGN_TO_CONST   13 */ "assignment to constant",
    /* JAMC_NEXT_UNEXPECTED   14 */ "NEXT unexpected",
    /* JAMC_POP_UNEXPECTED    15 */ "POP unexpected",
    /* JAMC_RETURN_UNEXPECTED 16 */ "RETURN unexpected",
    /* JAMC_ILLEGAL_SYMBOL    17 */ "illegal symbol name",
    /* JAMC_VECTOR_MAP_FAILED 18 */ "vector signal name not found",
    /* JAMC_USER_ABORT        19 */ "execution cancelled",
    /* JAMC_STACK_OVERFLOW    20 */ "stack overflow",
    /* JAMC_ILLEGAL_OPCODE    21 */ "illegal instruction code",
    /* JAMC_PHASE_ERROR       22 */ "phase error",
    /* JAMC_SCOPE_ERROR       23 */ "scope error",
    /* JAMC_ACTION_NOT_FOUND  24 */ "action not found",
  };
#define MAX_ERROR_CODE (int)((sizeof(error_text)/sizeof(error_text[0]))+1)

/* Variables used in the caen.c module */
int dcc_number = -1;
int jtag_output_chain = -1;
verbose = FALSE;

int main(int argc, char **argv)
{

  BOOL help = FALSE;
  BOOL error = FALSE;
  char *filename = NULL;
  long offset = 0L;
  long error_line = 0L;
  JAM_RETURN_TYPE crc_result = JAMC_SUCCESS;
  JAM_RETURN_TYPE exec_result = JAMC_SUCCESS;
  unsigned short expected_crc = 0;
  unsigned short actual_crc = 0;
  char key[33] = {0};
  char value[257] = {0};
  int exit_status = 0; 
  int arg = 0;
  int exit_code = 0;
  int format_version = 0;
  time_t start_time = 0;
  time_t end_time = 0;
  int time_delta = 0;
  char *workspace = NULL;
  char *action = NULL;
  char *init_list[10];
  int init_count = 0;
  FILE *fp = NULL;
  struct stat sbuf;
  long workspace_size = 0;
  char *exit_string = NULL;
  int reset_jtag = 1;

  verbose = FALSE;

  init_list[0] = NULL;

  /* print out the version string and copyright message */
  fprintf(stderr, "Jam STAPL Player Version 2.5 (20040526)\nCopyright (C) 1997-2004 Altera Corporation\n\n");
  fprintf(stderr, "Modified by WZab for RPC DAQ 2006 \n\n");

  for (arg = 1; arg < argc; arg++)
    {
#if PORT == UNIX
      if (argv[arg][0] == '-')
#else
	if ((argv[arg][0] == '-') || (argv[arg][0] == '/'))
#endif
	  {
	    switch(toupper(argv[arg][1]))
	      {
	      case 'A':				/* set action name */
		action = &argv[arg][2];
		if (action[0] == '"') ++action;
		break;

#if PORT == WINDOWS || PORT == DOS
	      case 'C':				/* Use alternative ISP download cable */
		if(toupper(argv[arg][2]) == 'L')
		  alternative_cable_l = TRUE;
		else if(toupper(argv[arg][2]) == 'X')
		  alternative_cable_x = TRUE;
		break;
#endif

	      case 'D':				/* initialization list */
		if (argv[arg][2] == '"')
		  {
		    init_list[init_count] = &argv[arg][3];
		  }
		else
		  {
		    init_list[init_count] = &argv[arg][2];
		  }
		init_list[++init_count] = NULL;
		break;

#if PORT == WINDOWS || PORT == DOS
	      case 'P':				/* set LPT port address */
		specified_lpt_port = TRUE;
		if (sscanf(&argv[arg][2], "%d", &lpt_port) != 1) error = TRUE;
		if ((lpt_port < 1) || (lpt_port > 3)) error = TRUE;
		if (error)
		  {
		    if (sscanf(&argv[arg][2], "%x", &lpt_port) == 1)
		      {
			if ((lpt_port == 0x278) ||
			    (lpt_port == 0x27c) ||
			    (lpt_port == 0x378) ||
			    (lpt_port == 0x37c) ||
			    (lpt_port == 0x3b8) ||
			    (lpt_port == 0x3bc))
			  {
			    error = FALSE;
			    specified_lpt_addr = TRUE;
			    lpt_addr = (WORD) lpt_port;
			    lpt_port = 1;
			  }
		      }
		  }
		break;
#endif

	      case 'R':		/* don't reset the JTAG chain after use */
		reset_jtag = 0;
		break;

	      case 'S':				/* set serial port address */
		serial_port_name = &argv[arg][2];
		specified_com_port = TRUE;
		break;

	      case 'M':				/* set memory size */
		if (sscanf(&argv[arg][2], "%ld", &workspace_size) != 1)
		  error = TRUE;
		if (workspace_size == 0) error = TRUE;
		break;

	      case 'N':				/* set DCC number - added by WZab */
		dcc_number = -1;
		if (sscanf(&argv[arg][2], "%d", &dcc_number) != 1)
		  error = TRUE;
		if (dcc_number == -1) error = TRUE;
		break;

	      case 'O':				/* set SCANSTA111 output JTAG chain */
		jtag_output_chain = -1;
		if (sscanf(&argv[arg][2], "%d", &jtag_output_chain) != 1)
		  error = TRUE;
		if (jtag_output_chain == -1) error = TRUE;
		break;

	      case 'H':				/* help */
		help = TRUE;
		break;

	      case 'V':				/* verbose */
		verbose = TRUE;
		break;

	      default:
		error = TRUE;
		break;
	      }
	  }
	else
	  {
	    /* it's a filename */
	    if (filename == NULL)
	      {
		filename = argv[arg];
	      }
	    else
	      {
		/* error -- we already found a filename */
		error = TRUE;
	      }
	  }

      if (error)
	{
	  fprintf(stderr, "Illegal argument: \"%s\"\n", argv[arg]);
	  help = TRUE;
	  error = FALSE;
	}
    }

#if PORT == WINDOWS || PORT == DOS
  if (specified_lpt_port && specified_com_port)
    {
      fprintf(stderr, "Error:  -s and -p options may not be used together\n\n");
      help = TRUE;
    }
#endif

  if (help || (filename == NULL))
    {
      fprintf(stderr, "Usage:  jam [options] <filename>\n");
      fprintf(stderr, "\nAvailable options:\n");
      fprintf(stderr, "    -h          : show help message\n");
      fprintf(stderr, "    -v          : show verbose messages\n");
      fprintf(stderr, "    -a<action>  : specify action name (Jam STAPL)\n");
      fprintf(stderr, "    -d<var=val> : initialize variable to specified value (Jam 1.1)\n");
      fprintf(stderr, "    -d<proc=1>  : enable optional procedure (Jam STAPL)\n");
      fprintf(stderr, "    -d<proc=0>  : disable recommended procedure (Jam STAPL)\n");
#if PORT == WINDOWS || PORT == DOS
      fprintf(stderr, "    -p<port>    : parallel port number or address (for ByteBlaster)\n");
      fprintf(stderr, "    -c<cable>   : alternative download cable compatibility: -cl or -cx\n");
#endif
      fprintf(stderr, "    -s<port>    : serial port name (for BitBlaster)\n");
      fprintf(stderr, "    -r          : don't reset JTAG TAP after use\n");
      fprintf(stderr, " Options added specifically for RPC DAQ:\n");
      fprintf(stderr, "    -n<dcc>     : number of the slot of the DCC board (0 to 31)\n");
      fprintf(stderr, "    -o<chain>   : number of the JTAG chain in DCC we want to use (0 or 1)\n");
      exit_status = 1;
    }
  else if ((workspace_size > 0) &&
	   ((workspace = (char *) jam_malloc((size_t) workspace_size)) == NULL))
    {
      fprintf(stderr, "Error: can't allocate memory (%d Kbytes)\n",
	      (int) (workspace_size / 1024L));
      exit_status = 1;
    }
  else if (access(filename, 0) != 0)
    {
      fprintf(stderr, "Error: can't access file \"%s\"\n", filename);
      exit_status = 1;
   }
  else
    {
       /* get length of file */
       if (stat(filename, &sbuf) == 0) file_length = sbuf.st_size;

       if ((fp = fopen(filename, "rb")) == NULL)
	{
	  fprintf(stderr, "Error: can't open file \"%s\"\n", filename);
	  exit_status = 1;
	}
      else
	{
	  /*
	   *	Read entire file into a buffer
	   */
#if PORT == DOS
	  int pages = 1 + (int) (file_length >> 14L);
	  int page;
	  file_buffer = (char **) jam_malloc((size_t) (pages * sizeof(char *)));
	  for (page = 0; page < pages; ++page)
	    {
	      /* allocate enough 16K blocks to store the file */
	      file_buffer[page] = (char *) jam_malloc (0x4000);
	      if (file_buffer[page] == NULL)
		{
		  /* flag error and break out of loop */
		  file_buffer = NULL;
		  page = pages;
		}
	    }
#else
	  file_buffer = (char *) jam_malloc((size_t) file_length);
#endif
	  if (file_buffer == NULL)
	    {
	      fprintf(stderr, "Error: can't allocate memory (%d Kbytes)\n",
		      (int) (file_length / 1024L));
	      exit_status = 1;
	    }
	  else
	    {
#if PORT == DOS
	      int pages = 1 + (int) (file_length >> 14L);
	      int page;
	      size_t page_size = 0x4000;
	      for (page = 0; (page < pages) && (exit_status == 0); ++page)
		{
		  if (page == (pages - 1))
		    {
		      /* last page may not be full 16K bytes */
		      page_size = (size_t) (file_length & 0x3fffL);
		    }
		  if (fread(file_buffer[page], 1, page_size, fp) != page_size)
		    {
		      fprintf(stderr, "Error reading file \"%s\"\n", filename);
		      exit_status = 1;
		    }
		}
#else
	      if (fread(file_buffer, 1, (size_t) file_length, fp) !=
		  (size_t) file_length)
		{
		  fprintf(stderr, "Error reading file \"%s\"\n", filename);
		  exit_status = 1;
		}
#endif
	    }

	  fclose(fp);
	}

      if (exit_status == 0)
	{
	  /*
	   *	Get Operating System type
	   */
#if PORT == WINDOWS
	  windows_nt = !(GetVersion() & 0x80000000);
#endif

	  /*
	   *	Calibrate the delay loop function
	   */
	  calibrate_delay();

	  /*
	   *	Check CRC
	   */
	  crc_result = jam_check_crc(
#if PORT==DOS
				     0L, 0L,
#else
				     file_buffer, file_length,
#endif
				     &expected_crc, &actual_crc);

	  if (verbose || (crc_result == JAMC_CRC_ERROR))
	    {
	      switch (crc_result)
		{
		case JAMC_SUCCESS:
		  printf("CRC matched: CRC value = %04X\n", actual_crc);
		  break;

		case JAMC_CRC_ERROR:
		  printf("CRC mismatch: expected %04X, actual %04X\n",
			 expected_crc, actual_crc);
		  break;

		case JAMC_UNEXPECTED_END:
		  printf("Expected CRC not found, actual CRC value = %04X\n",
			 actual_crc);
		  break;

		default:
		  printf("CRC function returned error code %d\n", crc_result);
		  break;
		}
	    }

	  /*
	   *	Dump out NOTE fields
	   */
	  if (verbose)
	    {
	      while (jam_get_note(
#if PORT==DOS
				  0L, 0L,
#else
				  file_buffer, file_length,
#endif
				  &offset, key, value, 256) == 0)
		{
		  printf("NOTE \"%s\" = \"%s\"\n", key, value);
		}
	    }

	  /*
	   *	Execute the JAM program
	   */
	  time(&start_time);
	  exec_result = jam_execute(
#if PORT==DOS
				    0L, 0L,
#else
				    file_buffer, file_length,
#endif
				    workspace, workspace_size, action, init_list,
				    reset_jtag, &error_line, &exit_code, &format_version);
	  time(&end_time);

	  if (exec_result == JAMC_SUCCESS)
	    {
	      if (format_version == 2)
		{
		  switch (exit_code)
		    {
		    case  0: exit_string = "Success"; break;
		    case  1: exit_string = "Checking chain failure"; break;
		    case  2: exit_string = "Reading IDCODE failure"; break;
		    case  3: exit_string = "Reading USERCODE failure"; break;
		    case  4: exit_string = "Reading UESCODE failure"; break;
		    case  5: exit_string = "Entering ISP failure"; break;
		    case  6: exit_string = "Unrecognized device"; break;
		    case  7: exit_string = "Device revision is not supported"; break;
		    case  8: exit_string = "Erase failure"; break;
		    case  9: exit_string = "Device is not blank"; break;
		    case 10: exit_string = "Device programming failure"; break;
		    case 11: exit_string = "Device verify failure"; break;
		    case 12: exit_string = "Read failure"; break;
		    case 13: exit_string = "Calculating checksum failure"; break;
		    case 14: exit_string = "Setting security bit failure"; break;
		    case 15: exit_string = "Querying security bit failure"; break;
		    case 16: exit_string = "Exiting ISP failure"; break;
		    case 17: exit_string = "Performing system test failure"; break;
		    default: exit_string = "Unknown exit code"; break;
		    }
		}
	      else
		{
		  switch (exit_code)
		    {
		    case 0: exit_string = "Success"; break;
		    case 1: exit_string = "Illegal initialization values"; break;
		    case 2: exit_string = "Unrecognized device"; break;
		    case 3: exit_string = "Device revision is not supported"; break;
		    case 4: exit_string = "Device programming failure"; break;
		    case 5: exit_string = "Device is not blank"; break;
		    case 6: exit_string = "Device verify failure"; break;
		    case 7: exit_string = "SRAM configuration failure"; break;
		    default: exit_string = "Unknown exit code"; break;
		    }
		}

	      printf("Exit code = %d... %s\n", exit_code, exit_string);
	    }
	  else if ((format_version == 2) &&
		   (exec_result == JAMC_ACTION_NOT_FOUND))
	    {
	      if ((action == NULL) || (*action == '\0'))
		{
		  printf("Error: no action specified for Jam file.\nProgram terminated.\n");
		}
	      else
		{
		  printf("Error: action \"%s\" is not supported for this Jam file.\nProgram terminated.\n", action);
		}
	    }
	  else if (exec_result < MAX_ERROR_CODE)
	    {
	      printf("Error on line %ld: %s.\nProgram terminated.\n",
		     error_line, error_text[exec_result]);
	    }
	  else
	    {
	      printf("Unknown error code %ld\n", exec_result);
	    }

	  /*
	   *	Print out elapsed time
	   */
	  if (verbose)
	    {
	      time_delta = (int) (end_time - start_time);
	      printf("Elapsed time = %02u:%02u:%02u\n",
		     time_delta / 3600,			/* hours */
		     (time_delta % 3600) / 60,	/* minutes */
		     time_delta % 60);			/* seconds */
	    }
	}
    }

  if (jtag_hardware_initialized) close_jtag_hardware();

  if (workspace != NULL) jam_free(workspace);
  if (file_buffer != NULL) jam_free(file_buffer);

#if defined(MEM_TRACKER)
  if (verbose)
    {
#if defined(USE_STATIC_MEMORY)
      fprintf(stdout, "Memory Usage Info: static memory size = %ud (%dKB)\n", N_STATIC_MEMORY_BYTES, N_STATIC_MEMORY_KBYTES);
#endif /* USE_STATIC_MEMORY */
      fprintf(stdout, "Memory Usage Info: peak memory usage = %ud (%dKB)\n", peak_memory_usage, (peak_memory_usage + 1023) / 1024);
      fprintf(stdout, "Memory Usage Info: peak allocations = %d\n", peak_allocations);
#if defined(USE_STATIC_MEMORY)
      if ((n_bytes_allocated - n_bytes_not_recovered) != 0)
	{
	  fprintf(stdout, "Memory Usage Info: bytes still allocated = %d (%dKB)\n", (n_bytes_allocated - n_bytes_not_recovered), ((n_bytes_allocated - n_bytes_not_recovered) + 1023) / 1024);
	}
#else /* USE_STATIC_MEMORY */
      if (n_bytes_allocated != 0)
	{
	  fprintf(stdout, "Memory Usage Info: bytes still allocated = %d (%dKB)\n", n_bytes_allocated, (n_bytes_allocated + 1023) / 1024);
	}
#endif /* USE_STATIC_MEMORY */
      if (n_allocations != 0)
	{
	  fprintf(stdout, "Memory Usage Info: allocations not freed = %d\n", n_allocations);
	}
    }
#endif /* MEM_TRACKER */


  return (exit_status);
}
