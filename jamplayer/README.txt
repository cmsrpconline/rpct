Sterowanie SCANSTA111 - najlepiej wsadzi� do Jam Playera, do procedury inicjalizacji JTAG'a
trzeba jednak m�c przekaza� Jam Playerowi dwa parametry - numer DCC i numer chainu.

Gdzie wprowadzi�em zmiany - wmontowa�em obs�ug� SCANSTA111.
Doda�em grupowanie odwo�a� do JTAGa tam, gdzie to mo�liwe 
(czyli w sekwencjach zapis�w).
Rzeczywiste wykonanie operacji jes wymuszane tylko tam, gdzie jest odczyt,
lub tam, gdzie jest wym�g generacji op�nienia.

Wyd�u�ony ci�g bit�w daj�cy si� przes�a� przez DRSCAN?