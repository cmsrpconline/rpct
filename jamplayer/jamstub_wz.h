// Header file for jamstub_wz.c
#ifndef JAMSTUB_WZ_H
#define JAMSTUB_WZ_H

#include "jamport.h"

#ifndef NO_ALTERA_STDIO
#define NO_ALTERA_STDIO
#endif

#if ( _MSC_VER >= 800 )
#pragma warning(disable:4115)
#pragma warning(disable:4201)
#pragma warning(disable:4214)
#pragma warning(disable:4514)
#endif


#if PORT == WINDOWS
#include <windows.h>
#else
typedef int BOOL;
typedef unsigned char BYTE;
typedef unsigned short WORD;
typedef unsigned long DWORD;
#define TRUE 1
#define FALSE 0
#endif

#if defined(USE_STATIC_MEMORY)
#define N_STATIC_MEMORY_KBYTES ((unsigned int) USE_STATIC_MEMORY)
#define N_STATIC_MEMORY_BYTES (N_STATIC_MEMORY_KBYTES * 1024)
#define POINTER_ALIGNMENT sizeof(DWORD)
#else /* USE_STATIC_MEMORY */
#include <malloc.h>
#define POINTER_ALIGNMENT sizeof(BYTE)
#endif /* USE_STATIC_MEMORY */

#if PORT == DOS
#include <bios.h>
#endif

#if PORT == WINDOWS
#define PGDC_IOCTL_GET_DEVICE_INFO_PP 0x00166A00L
#define PGDC_IOCTL_READ_PORT_PP       0x00166A04L
#define PGDC_IOCTL_WRITE_PORT_PP      0x0016AA08L
#define PGDC_IOCTL_PROCESS_LIST_PP    0x0016AA1CL
#define PGDC_READ_INFO                0x0a80
#define PGDC_READ_PORT                0x0a81
#define PGDC_WRITE_PORT               0x0a82
#define PGDC_PROCESS_LIST             0x0a87
#define PGDC_HDLC_NTDRIVER_VERSION    2
#define PORT_IO_BUFFER_SIZE           256
#endif

#if PORT == WINDOWS
#ifdef __BORLANDC__
/* create dummy inp() and outp() functions for Borland 32-bit compile */
WORD inp(WORD address) { address = address; return(0); }
void outp(WORD address, WORD data) { address = address; data = data; }
#else
#pragma intrinsic (inp, outp)
#endif
#endif

/*
 *	For Borland C compiler (16-bit), set the stack size
 */
#if PORT == DOS
#ifdef __BORLANDC__
extern unsigned int _stklen = 50000;
#endif
#endif

/************************************************************************
 *
 *	Global variables
 */

/* file buffer for JAM input file */
#if PORT == DOS
extern char **file_buffer;
#else
extern char *file_buffer;
#endif
extern long file_pointer;
extern long file_length;

/* delay count for one millisecond delay */
extern long one_ms_delay;

/* delay count to reduce the maximum TCK frequency */
extern int tck_delay;

/* serial port interface available on all platforms */
extern BOOL jtag_hardware_initialized;
extern char *serial_port_name;
extern BOOL specified_com_port;
extern int com_port;
void initialize_jtag_hardware(void);
void close_jtag_hardware(void);

#if defined(USE_STATIC_MEMORY)
extern unsigned char static_memory_heap[N_STATIC_MEMORY_BYTES];
#endif /* USE_STATIC_MEMORY */

#if defined(USE_STATIC_MEMORY) || defined(MEM_TRACKER)
extern unsigned int n_bytes_allocated;
#endif /* USE_STATIC_MEMORY || MEM_TRACKER */

#if defined(MEM_TRACKER)
extern unsigned int peak_memory_usage;
extern unsigned int peak_allocations;
extern unsigned int n_allocations;
#if defined(USE_STATIC_MEMORY)
extern unsigned int n_bytes_not_recovered;
#endif /* USE_STATIC_MEMORY */
extern const DWORD BEGIN_GUARD;// = 0x01234567;
extern const DWORD END_GUARD;// = 0x76543210;
#endif /* MEM_TRACKER */

#if PORT == WINDOWS || PORT == DOS
/* parallel port interface available on PC only */
extern BOOL specified_lpt_port;
extern BOOL specified_lpt_addr;
extern int lpt_port = 1;
extern int initial_lpt_ctrl;
extern WORD lpt_addr;
extern WORD lpt_addr_table[3];
extern BOOL alternative_cable_l;
extern BOOL alternative_cable_x;
void write_byteblaster(int port, int data);
int read_byteblaster(int port);
#endif

#if PORT==WINDOWS
#ifndef __BORLANDC__
extern WORD lpt_addresses_from_registry[4];
#endif
#endif

#if PORT == WINDOWS
/* variables to manage cached I/O under Windows NT */
extern BOOL windows_nt;
extern int port_io_count;
extern HANDLE nt_device_handle;
struct PORT_IO_LIST_STRUCT
{
  USHORT command;
  USHORT data;
} port_io_buffer[PORT_IO_BUFFER_SIZE];
extern void flush_ports(void);
BOOL initialize_nt_driver(void);
#endif

/* Variables used in the caen.c module */
//int dcc_number = -1;
//int jtag_output_chain = -1;

////
/* function prototypes to allow forward reference */
extern void delay_loop(long count);

/*
 *	This structure stores information about each available vector signal
 */
struct VECTOR_LIST_STRUCT
{
  char *signal_name;
  int  hardware_bit;
  int  vector_index;
};

/*
 *	Vector signals for ByteBlaster:
 *
 *	tck (dclk)    = register 0, bit 0
 *	tms (nconfig) = register 0, bit 1
 *	tdi (data)    = register 0, bit 6
 *	tdo (condone) = register 1, bit 7 (inverted!)
 *	nstatus       = register 1, bit 4 (not inverted)
 */

BOOL verbose;// = FALSE;

/************************************************************************
 *
 *	Customized interface functions for JAM interpreter I/O:
 *
 *	jam_getc()
 *	jam_seek()
 *	jam_jtag_io()
 *	jam_message()
 *	jam_delay()
 */

int jam_getc(void);

int jam_seek(long offset);

int jam_jtag_io_orig(int tms, int tdi, int read_tdo);

void jam_message(char *message_text);

void jam_export_integer(char *key, long value);

#define HEX_LINE_CHARS 72
#define HEX_LINE_BITS (HEX_LINE_CHARS * 4)

char conv_to_hex(unsigned long value);

void jam_export_boolean_array(char *key, unsigned char *data, long count);

void jam_delay_orig(long microseconds);

int jam_vector_map(int signal_count, char **signals);

int jam_vector_io_orig(int signal_count, long *dir_vect, long *data_vect, long *capture_vect);

int jam_set_frequency(long hertz);

void *jam_malloc(unsigned int size);

void jam_free(void *ptr);

/************************************************************************
 *
 *	get_tick_count() -- Get system tick count in milliseconds
 *
 *	for DOS, use BIOS function _bios_timeofday()
 *	for WINDOWS use GetTickCount() function
 *	for UNIX use clock() system function
 */
DWORD get_tick_count(void);

#define DELAY_SAMPLES 10
#define DELAY_CHECK_LOOPS 10000

void calibrate_delay(void);

/************************************************************************/


#if PORT==WINDOWS
#ifndef __BORLANDC__
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *	SEARCH_DYN_DATA
 *
 *	Searches recursively in Windows 95/98 Registry for parallel port info
 *	under HKEY_DYN_DATA registry key.  Called by search_local_machine().
 */
void search_dyn_data(char *dd_path, char *hardware_key, int lpt);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *	SEARCH_LOCAL_MACHINE
 *
 *	Searches recursively in Windows 95/98 Registry for parallel port info
 *	under HKEY_LOCAL_MACHINE\Enum.  When parallel port is found, calls
 *	search_dyn_data() to get the port address.
 */
void search_local_machine(char *lm_path, char *dd_path);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *	GET_LPT_ADDRESSES_FROM_REGISTRY
 *
 *	Searches Win95/98 registry recursively to get I/O port addresses for
 *	parallel ports.
 */
void get_lpt_addresses_from_registry();
#endif
#endif

void initialize_jtag_hardware_orig();

void close_jtag_hardware_orig();

#if PORT == WINDOWS
/**************************************************************************/
/*                                                                        */

BOOL initialize_nt_driver();

/*                                                                        */
/*  Uses CreateFile() to open a connection to the Windows NT device       */
/*  driver.                                                               */
/*                                                                        */
/**************************************************************************/
#endif

#if PORT == WINDOWS || PORT == DOS
/**************************************************************************/
/*                                                                        */

void write_byteblaster(int port, int data);

/*                                                                        */
/**************************************************************************/

/**************************************************************************/
/*                                                                        */

int read_byteblaster(int port);

/*                                                                        */
/**************************************************************************/

#if PORT == WINDOWS
void flush_ports(void);
#endif /* PORT == WINDOWS */
#endif /* PORT == WINDOWS || PORT == DOS */

#if !defined (DEBUG)
#pragma optimize ("ceglt", off)
#endif

void delay_loop(long count);

#endif  /* JAMSTUB_WZ_H */
