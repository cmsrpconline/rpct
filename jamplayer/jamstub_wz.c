
// Modifed by WZab, split on *.c and *.h by MB
/****************************************************************************/
/*																			*/
/*	Module:			jamstub.c												*/
/*																			*/
/*					Copyright (C) Altera Corporation 1997-2000				*/
/*																			*/
/*	Description:	Main source file for stand-alone JAM test utility.		*/
/*																			*/
/*					Supports Altera ByteBlaster hardware download cable		*/
/*					on Windows 95 and Windows NT operating systems.			*/
/*					(A device driver is required for Windows NT.)			*/
/*																			*/
/*					Also supports BitBlaster hardware download cable on		*/
/*					Windows 95, Windows NT, and UNIX platforms.				*/
/*																			*/
/*	Revisions:		1.1	added dynamic memory allocation						*/
/*					1.11 added multi-page memory allocation for file_buffer */
/*                    to permit DOS version to read files larger than 64K   */
/*					1.2 fixed control port initialization for ByteBlaster	*/
/*					2.2 updated usage message, added support for alternate	*/
/*					  cable types, moved porting macros in jamport.h,		*/
/*					  fixed bug in delay calibration code for 16-bit port	*/
/*					2.3 added support for static memory						*/
/*						fixed /W4 warnings									*/
/*																			*/
/****************************************************************************/
#include "jamstub_wz.h"

#include "jamport.h"
#include "jamexprt.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
//#include <io.h>
#include <fcntl.h>
//#include <process.h>

#include <time.h>
//#include <conio.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>

/************************************************************************
 *
 *	Global variables
 */

/* file buffer for JAM input file */
#if PORT == DOS
char **file_buffer = NULL;
#else
char *file_buffer = NULL;
#endif
long file_pointer = 0L;
long file_length = 0L;

/* delay count for one millisecond delay */
long one_ms_delay = 0L;

/* delay count to reduce the maximum TCK frequency */
int tck_delay = 0;

/* serial port interface available on all platforms */
BOOL jtag_hardware_initialized = FALSE;
char *serial_port_name = NULL;
BOOL specified_com_port = FALSE;
int com_port = -1;

#if defined(USE_STATIC_MEMORY)
unsigned char static_memory_heap[N_STATIC_MEMORY_BYTES] = { 0 };
#endif /* USE_STATIC_MEMORY */

#if defined(USE_STATIC_MEMORY) || defined(MEM_TRACKER)
unsigned int n_bytes_allocated = 0;
#endif /* USE_STATIC_MEMORY || MEM_TRACKER */

#if defined(MEM_TRACKER)
unsigned int peak_memory_usage = 0;
unsigned int peak_allocations = 0;
unsigned int n_allocations = 0;
#if defined(USE_STATIC_MEMORY)
unsigned int n_bytes_not_recovered = 0;
#endif /* USE_STATIC_MEMORY */
const DWORD BEGIN_GUARD = 0x01234567;
const DWORD END_GUARD = 0x76543210;
#endif /* MEM_TRACKER */

#if PORT == WINDOWS || PORT == DOS
/* parallel port interface available on PC only */
BOOL specified_lpt_port = FALSE;
BOOL specified_lpt_addr = FALSE;
int lpt_port = 1;
int initial_lpt_ctrl = 0;
WORD lpt_addr = 0x3bc;
WORD lpt_addr_table[3] = { 0x3bc, 0x378, 0x278 };
BOOL alternative_cable_l = FALSE;
BOOL alternative_cable_x = FALSE;
#endif

#if PORT==WINDOWS
#ifndef __BORLANDC__
WORD lpt_addresses_from_registry[4] = { 0 };
#endif
#endif

#if PORT == WINDOWS
/* variables to manage cached I/O under Windows NT */
BOOL windows_nt = FALSE;
int port_io_count = 0;
HANDLE nt_device_handle = INVALID_HANDLE_VALUE;
#endif

struct VECTOR_LIST_STRUCT vector_list[] =
  {
    /* add a record here for each vector signal */
    { "**TCK**",   0, -1 },
    { "**TMS**",   1, -1 },
    { "**TDI**",   6, -1 },
    { "**TDO**",   7, -1 },
    { "TCK",       0, -1 },
    { "TMS",       1, -1 },
    { "TDI",       6, -1 },
    { "TDO",       7, -1 },
    { "DCLK",      0, -1 },
    { "NCONFIG",   1, -1 },
    { "DATA",      6, -1 },
    { "CONF_DONE", 7, -1 },
    { "NSTATUS",   4, -1 }
  };
#define VECTOR_SIGNAL_COUNT ((int)(sizeof(vector_list)/sizeof(vector_list[0])))

/************************************************************************
 *
 *	Customized interface functions for JAM interpreter I/O:
 *
 *	jam_getc()
 *	jam_seek()
 *	jam_jtag_io()
 *	jam_message()
 *	jam_delay()
 */

int jam_getc(void)
{
  int ch = EOF;

  if (file_pointer < file_length)
    {
#if PORT == DOS
      ch = (int) file_buffer[file_pointer >> 14L][file_pointer & 0x3fffL];
      ++file_pointer;
#else
      ch = (int) file_buffer[file_pointer++];
#endif
    }

  return (ch);
}

int jam_seek(long offset)
{
  int return_code = EOF;

  if ((offset >= 0L) && (offset < file_length))
    {
      file_pointer = offset;
      return_code = 0;
    }

  return (return_code);
}

int jam_jtag_io_orig(int tms, int tdi, int read_tdo)
{
  int data = 0;
  int tdo = 0;
  int i = 0;
  int result = 0;
  char ch_data = 0;

  if (!jtag_hardware_initialized)
    {
      initialize_jtag_hardware();
      jtag_hardware_initialized = TRUE;
    }

  if (specified_com_port)
    {
      ch_data = (char)
	((tdi ? 0x01 : 0) | (tms ? 0x02 : 0) | 0x60);

      write(com_port, &ch_data, 1);

      if (read_tdo)
	{
	  ch_data = 0x7e;
	  write(com_port, &ch_data, 1);
	  for (i = 0; (i < 100) && (result != 1); ++i)
	    {
	      result = read(com_port, &ch_data, 1);
	    }
	  if (result == 1)
	    {
	      tdo = ch_data & 0x01;
	    }
	  else
	    {
	      fprintf(stderr, "Error:  BitBlaster not responding\n");
	    }
	}

      ch_data = (char)
	((tdi ? 0x01 : 0) | (tms ? 0x02 : 0) | 0x64);

      write(com_port, &ch_data, 1);
    }
  else
    {
#if PORT == WINDOWS || PORT == DOS
      data = (alternative_cable_l ? ((tdi ? 0x01 : 0) | (tms ? 0x04 : 0)) :
	      (alternative_cable_x ? ((tdi ? 0x01 : 0) | (tms ? 0x04 : 0) | 0x10) :
	       ((tdi ? 0x40 : 0) | (tms ? 0x02 : 0))));

      write_byteblaster(0, data);

      if (read_tdo)
	{
	  tdo = read_byteblaster(1);
	  tdo = (alternative_cable_l ? ((tdo & 0x40) ? 1 : 0) :
		 (alternative_cable_x ? ((tdo & 0x10) ? 1 : 0) :
		  ((tdo & 0x80) ? 0 : 1)));
	}

      write_byteblaster(0, data | (alternative_cable_l ? 0x02 : (alternative_cable_x ? 0x02: 0x01)));

      write_byteblaster(0, data);
#else
      /* parallel port interface not available */
      tdo = 0;
#endif
    }

  if (tck_delay != 0) delay_loop(tck_delay);

  return (tdo);
}

void jam_message(char *message_text)
{
  puts(message_text);
  fflush(stdout);
}

void jam_export_integer(char *key, long value)
{
  if (verbose)
    {
      printf("Export: key = \"%s\", value = %ld\n", key, value);
      fflush(stdout);
    }
}

char conv_to_hex(unsigned long value)
{
  char c;

  if (value > 9)
    {
      c = (char) (value + ('A' - 10));
    }
  else
    {
      c = (char) (value + '0');
    }

  return (c);
}

void jam_export_boolean_array(char *key, unsigned char *data, long count)
{
  unsigned long size, line, lines, linebits, value, j, k;
  char string[HEX_LINE_CHARS + 1];
  long i, offset;

  if (verbose)
    {
      if (count > HEX_LINE_BITS)
	{
	  printf("Export: key = \"%s\", %ld bits, value = HEX\n", key, count);
	  lines = (count + (HEX_LINE_BITS - 1)) / HEX_LINE_BITS;

	  for (line = 0; line < lines; ++line)
	    {
	      if (line < (lines - 1))
		{
		  linebits = HEX_LINE_BITS;
		  size = HEX_LINE_CHARS;
		  offset = count - ((line + 1) * HEX_LINE_BITS);
		}
	      else
		{
		  linebits = count - ((lines - 1) * HEX_LINE_BITS);
		  size = (linebits + 3) / 4;
		  offset = 0L;
		}

	      string[size] = '\0';
	      j = size - 1;
	      value = 0;

	      for (k = 0; k < linebits; ++k)
		{
		  i = k + offset;
		  if (data[i >> 3] & (1 << (i & 7))) value |= (1 << (i & 3));
		  if ((i & 3) == 3)
		    {
		      string[j] = conv_to_hex(value);
		      value = 0;
		      --j;
		    }
		}
	      if ((k & 3) > 0) string[j] = conv_to_hex(value);

	      printf("%s\n", string);
	    }

	  fflush(stdout);
	}
      else
	{
	  size = (count + 3) / 4;
	  string[size] = '\0';
	  j = size - 1;
	  value = 0;

	  for (i = 0; i < count; ++i)
	    {
	      if (data[i >> 3] & (1 << (i & 7))) value |= (1 << (i & 3));
	      if ((i & 3) == 3)
		{
		  string[j] = conv_to_hex(value);
		  value = 0;
		  --j;
		}
	    }
	  if ((i & 3) > 0) string[j] = conv_to_hex(value);

	  printf("Export: key = \"%s\", %ld bits, value = HEX %s\n",
		 key, count, string);
	  fflush(stdout);
	}
    }
}

void jam_delay_orig(long microseconds)
{
#if PORT == WINDOWS
  /* if Windows NT, flush I/O cache buffer before delay loop */
  if (windows_nt && (port_io_count > 0)) flush_ports();
#endif

  delay_loop(microseconds *
	     ((one_ms_delay / 1000L) + ((one_ms_delay % 1000L) ? 1 : 0)));
}

int jam_vector_map
(
 int signal_count,
 char **signals
 )
{
  int signal, vector, ch_index, diff;
  int matched_count = 0;
  char l, r;

  for (vector = 0; (vector < VECTOR_SIGNAL_COUNT); ++vector)
    {
      vector_list[vector].vector_index = -1;
    }

  for (signal = 0; signal < signal_count; ++signal)
    {
      diff = 1;
      for (vector = 0; (diff != 0) && (vector < VECTOR_SIGNAL_COUNT);
	   ++vector)
	{
	  if (vector_list[vector].vector_index == -1)
	    {
	      ch_index = 0;
	      do
		{
		  l = signals[signal][ch_index];
		  r = vector_list[vector].signal_name[ch_index];
		  diff = (((l >= 'a') && (l <= 'z')) ? (l - ('a' - 'A')) : l)
		    - (((r >= 'a') && (r <= 'z')) ? (r - ('a' - 'A')) : r);
		  ++ch_index;
		}
	      while ((diff == 0) && (l != '\0') && (r != '\0'));

	      if (diff == 0)
		{
		  vector_list[vector].vector_index = signal;
		  ++matched_count;
		}
	    }
	}
    }

  return (matched_count);
}

int jam_vector_io_orig
(
 int signal_count,
 long *dir_vect,
 long *data_vect,
 long *capture_vect
 )
{
  int signal, vector, bit;
  int matched_count = 0;
  int data = 0;
  int mask = 0;
  int dir = 0;
  int i = 0;
  int result = 0;
  char ch_data = 0;

  if (!jtag_hardware_initialized)
    {
      initialize_jtag_hardware();
      jtag_hardware_initialized = TRUE;
    }

  /*
   *	Collect information about output signals
   */
  for (vector = 0; vector < VECTOR_SIGNAL_COUNT; ++vector)
    {
      signal = vector_list[vector].vector_index;

      if ((signal >= 0) && (signal < signal_count))
	{
	  bit = (1 << vector_list[vector].hardware_bit);

	  mask |= bit;
	  if (data_vect[signal >> 5] & (1L << (signal & 0x1f))) data |= bit;
	  if (dir_vect[signal >> 5] & (1L << (signal & 0x1f))) dir |= bit;

	  ++matched_count;
	}
    }

  /*
   *	Write outputs to hardware interface, if any
   */
  if (dir != 0)
    {
      if (specified_com_port)
	{
	  ch_data = (char) (((data >> 6) & 0x01) | (data & 0x02) |
			    ((data << 2) & 0x04) | ((data << 3) & 0x08) | 0x60);
	  write(com_port, &ch_data, 1);
	}
      else
	{
#if PORT == WINDOWS || PORT == DOS

	  write_byteblaster(0, data);

#endif
	}
    }

  /*
   *	Read the input signals and save information in capture_vect[]
   */
  if ((dir != mask) && (capture_vect != NULL))
    {
      if (specified_com_port)
	{
	  ch_data = 0x7e;
	  write(com_port, &ch_data, 1);
	  for (i = 0; (i < 100) && (result != 1); ++i)
	    {
	      result = read(com_port, &ch_data, 1);
	    }
	  if (result == 1)
	    {
	      data = ((ch_data << 7) & 0x80) | ((ch_data << 3) & 0x10);
	    }
	  else
	    {
	      fprintf(stderr, "Error:  BitBlaster not responding\n");
	    }
	}
      else
	{
#if PORT == WINDOWS || PORT == DOS

	  data = read_byteblaster(1) ^ 0x80; /* parallel port inverts bit 7 */

#endif
	}

      for (vector = 0; vector < VECTOR_SIGNAL_COUNT; ++vector)
	{
	  signal = vector_list[vector].vector_index;

	  if ((signal >= 0) && (signal < signal_count))
	    {
	      bit = (1 << vector_list[vector].hardware_bit);

	      if ((dir & bit) == 0)	/* if it is an input signal... */
		{
		  if (data & bit)
		    {
		      capture_vect[signal >> 5] |= (1L << (signal & 0x1f));
		    }
		  else
		    {
		      capture_vect[signal >> 5] &= ~(unsigned long)
			(1L << (signal & 0x1f));
		    }
		}
	    }
	}
    }

  return (matched_count);
}

int jam_set_frequency(long hertz)
{
  if (verbose)
    {
      printf("Frequency: %ld Hz\n", hertz);
      fflush(stdout);
    }

  if (hertz == -1)
    {
      /* no frequency limit */
      tck_delay = 0;
    }
  else if (hertz == 0)
    {
      /* stop the clock */
      tck_delay = -1;
    }
  else
    {
      /* set the clock delay to the period */
      /* corresponding to the selected frequency */
      tck_delay = (one_ms_delay * 1000) / hertz;
    }

  return (0);
}

void *jam_malloc(unsigned int size)
{	unsigned int n_bytes_to_allocate = 
#if defined(USE_STATIC_MEMORY) || defined(MEM_TRACKER)
    sizeof(unsigned int) +
#endif /* USE_STATIC_MEMORY || MEM_TRACKER */
#if defined(MEM_TRACKER)
    (2 * sizeof(DWORD)) +
#endif /* MEM_TRACKER */
    (POINTER_ALIGNMENT * ((size + POINTER_ALIGNMENT - 1) / POINTER_ALIGNMENT));

  unsigned char *ptr = 0;


#if defined(MEM_TRACKER)
  if ((n_bytes_allocated + n_bytes_to_allocate) > peak_memory_usage)
    {
      peak_memory_usage = n_bytes_allocated + n_bytes_to_allocate;
    }
  if ((n_allocations + 1) > peak_allocations)
    {
      peak_allocations = n_allocations + 1;
    }
#endif /* MEM_TRACKER */

#if defined(USE_STATIC_MEMORY)
  if ((n_bytes_allocated + n_bytes_to_allocate) <= N_STATIC_MEMORY_BYTES)
    {
      ptr = (&(static_memory_heap[n_bytes_allocated]));
    }
#else /* USE_STATIC_MEMORY */ 
  ptr = (unsigned char *) malloc(n_bytes_to_allocate);
#endif /* USE_STATIC_MEMORY */

#if defined(USE_STATIC_MEMORY) || defined(MEM_TRACKER)
  if (ptr != 0)
    {
      unsigned int i = 0;

#if defined(MEM_TRACKER)
      for (i = 0; i < sizeof(DWORD); ++i)
	{
	  *ptr = (unsigned char) (BEGIN_GUARD >> (8 * i));
	  ++ptr;
	}
#endif /* MEM_TRACKER */

      for (i = 0; i < sizeof(unsigned int); ++i)
	{
	  *ptr = (unsigned char) (size >> (8 * i));
	  ++ptr;
	}

#if defined(MEM_TRACKER)
      for (i = 0; i < sizeof(DWORD); ++i)
	{
	  *(ptr + size + i) = (unsigned char) (END_GUARD >> (8 * i));
	  /* don't increment ptr */
	}

      ++n_allocations;
#endif /* MEM_TRACKER */

      n_bytes_allocated += n_bytes_to_allocate;
    }
#endif /* USE_STATIC_MEMORY || MEM_TRACKER */

  return ptr;
}

void jam_free(void *ptr)
{
  if
    (
#if defined(MEM_TRACKER)
     (n_allocations > 0) &&
#endif /* MEM_TRACKER */
     (ptr != 0)
     )
    {
      unsigned char *tmp_ptr = (unsigned char *) ptr;

#if defined(USE_STATIC_MEMORY) || defined(MEM_TRACKER)
      unsigned int n_bytes_to_free = 0;
      unsigned int i = 0;
      unsigned int size = 0;
#endif /* USE_STATIC_MEMORY || MEM_TRACKER */
#if defined(MEM_TRACKER)
      DWORD begin_guard = 0;
      DWORD end_guard = 0;


      tmp_ptr -= sizeof(DWORD);
#endif /* MEM_TRACKER */
#if defined(USE_STATIC_MEMORY) || defined(MEM_TRACKER)
      tmp_ptr -= sizeof(unsigned int);
#endif /* USE_STATIC_MEMORY || MEM_TRACKER */
      ptr = tmp_ptr;

#if defined(MEM_TRACKER)
      for (i = 0; i < sizeof(DWORD); ++i)
	{
	  begin_guard |= (((DWORD)(*tmp_ptr)) << (8 * i));
	  ++tmp_ptr;
	}
#endif /* MEM_TRACKER */

#if defined(USE_STATIC_MEMORY) || defined(MEM_TRACKER)
      for (i = 0; i < sizeof(unsigned int); ++i)
	{
	  size |= (((unsigned int)(*tmp_ptr)) << (8 * i));
	  ++tmp_ptr;
	}
#endif /* USE_STATIC_MEMORY || MEM_TRACKER */

#if defined(MEM_TRACKER)
      tmp_ptr += size;

      for (i = 0; i < sizeof(DWORD); ++i)
	{
	  end_guard |= (((DWORD)(*tmp_ptr)) << (8 * i));
	  ++tmp_ptr;
	}

      if ((begin_guard != BEGIN_GUARD) || (end_guard != END_GUARD))
	{
	  fprintf(stderr, "Error: memory corruption detected for allocation #%d... bad %s guard\n",
		  n_allocations, (begin_guard != BEGIN_GUARD) ? "begin" : "end");
	}

      --n_allocations;
#endif /* MEM_TRACKER */

#if defined(USE_STATIC_MEMORY) || defined(MEM_TRACKER)
      n_bytes_to_free = 
#if defined(MEM_TRACKER)
	(2 * sizeof(DWORD)) +
#endif /* MEM_TRACKER */
	sizeof(unsigned int) +
	(POINTER_ALIGNMENT * ((size + POINTER_ALIGNMENT - 1) / POINTER_ALIGNMENT));
#endif /* USE_STATIC_MEMORY || MEM_TRACKER */

#if defined(USE_STATIC_MEMORY)
      if ((((unsigned long) ptr - (unsigned long) static_memory_heap) + n_bytes_to_free) == (unsigned long) n_bytes_allocated)
	{
	  n_bytes_allocated -= n_bytes_to_free;
	}
#if defined(MEM_TRACKER)
      else
	{
	  n_bytes_not_recovered += n_bytes_to_free;
	}
#endif /* MEM_TRACKER */
#else /* USE_STATIC_MEMORY */
#if defined(MEM_TRACKER)
      n_bytes_allocated -= n_bytes_to_free;
#endif /* MEM_TRACKER */
      free(ptr);
#endif /* USE_STATIC_MEMORY */
    }
#if defined(MEM_TRACKER)
  else
    {
      if (ptr != 0)
	{
	  fprintf(stderr, "Error: attempt to free unallocated memory\n");
	}
    }
#endif /* MEM_TRACKER */
}

/************************************************************************
 *
 *	get_tick_count() -- Get system tick count in milliseconds
 *
 *	for DOS, use BIOS function _bios_timeofday()
 *	for WINDOWS use GetTickCount() function
 *	for UNIX use clock() system function
 */
DWORD get_tick_count(void)
{
  return clock()/CLOCKS_PER_SEC;
}

void calibrate_delay(void)
{
  int sample = 0;
  int count = 0;
  DWORD tick_count1 = 0L;
  DWORD tick_count2 = 0L;

  one_ms_delay = 0L;

#if PORT == WINDOWS || PORT == DOS
  for (sample = 0; sample < DELAY_SAMPLES; ++sample)
    {
      count = 0;
      tick_count1 = get_tick_count();
      while ((tick_count2 = get_tick_count()) == tick_count1) {};
      do { delay_loop(DELAY_CHECK_LOOPS); count++; } while
	((tick_count1 = get_tick_count()) == tick_count2);
      one_ms_delay += ((DELAY_CHECK_LOOPS * (DWORD)count) /
		       (tick_count1 - tick_count2));
    }

  one_ms_delay /= DELAY_SAMPLES;
#else
  one_ms_delay = 1000L;
#endif
}

/************************************************************************/


#if PORT==WINDOWS
#ifndef __BORLANDC__
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *	SEARCH_DYN_DATA
 *
 *	Searches recursively in Windows 95/98 Registry for parallel port info
 *	under HKEY_DYN_DATA registry key.  Called by search_local_machine().
 */
void search_dyn_data
(
 char *dd_path,
 char *hardware_key,
 int lpt
 )
{
  DWORD index;
  DWORD size;
  DWORD type;
  LONG result;
  HKEY key;
  int length;
  WORD address;
  char buffer[1024];
  FILETIME last_write = {0};
  WORD *word_ptr;
  int i;

  length = strlen(dd_path);

  if (RegOpenKeyEx(
		   HKEY_DYN_DATA,
		   dd_path,
		   0L,
		   KEY_READ,
		   &key)
      == ERROR_SUCCESS)
    {
      size = 1023;

      if (RegQueryValueEx(
			  key,
			  "HardWareKey",
			  NULL,
			  &type,
			  (unsigned char *) buffer,
			  &size)
	  == ERROR_SUCCESS)
	{
	  if ((type == REG_SZ) && (stricmp(buffer, hardware_key) == 0))
	    {
	      size = 1023;

	      if (RegQueryValueEx(
				  key,
				  "Allocation",
				  NULL,
				  &type,
				  (unsigned char *) buffer,
				  &size)
		  == ERROR_SUCCESS)
		{
		  /*
		   *	By "inspection", I have found five cases: size 32, 48,
		   *	56, 60, and 80 bytes.  The port address seems to be
		   *	located at different offsets in the buffer for these
		   *	five cases, as shown below.  If a valid port address
		   *	is not found, or the size is not one of these known
		   *	sizes, then I search through the entire buffer and
		   *	look for a value which is a valid port address.
		   */

		  word_ptr = (WORD *) buffer;

		  if ((type == REG_BINARY) && (size == 32))
		    {
		      address = word_ptr[10];
		    }
		  else if ((type == REG_BINARY) && (size == 48))
		    {
		      address = word_ptr[18];
		    }
		  else if ((type == REG_BINARY) && (size == 56))
		    {
		      address = word_ptr[22];
		    }
		  else if ((type == REG_BINARY) && (size == 60))
		    {
		      address = word_ptr[24];
		    }
		  else if ((type == REG_BINARY) && (size == 80))
		    {
		      address = word_ptr[24];
		    }
		  else address = 0;

		  /* if not found, search through entire buffer */
		  i = 0;
		  while ((i < (int) (size / 2)) &&
			 (address != 0x278) &&
			 (address != 0x27C) &&
			 (address != 0x378) &&
			 (address != 0x37C) &&
			 (address != 0x3B8) &&
			 (address != 0x3BC))
		    {
		      if ((word_ptr[i] == 0x278) ||
			  (word_ptr[i] == 0x27C) ||
			  (word_ptr[i] == 0x378) ||
			  (word_ptr[i] == 0x37C) ||
			  (word_ptr[i] == 0x3B8) ||
			  (word_ptr[i] == 0x3BC))
			{
			  address = word_ptr[i];
			}
		      ++i;
		    }

		  if ((address == 0x278) ||
		      (address == 0x27C) ||
		      (address == 0x378) ||
		      (address == 0x37C) ||
		      (address == 0x3B8) ||
		      (address == 0x3BC))
		    {
		      lpt_addresses_from_registry[lpt] = address;
		    }
		}
	    }
	}

      index = 0;

      do
	{
	  size = 1023;

	  result = RegEnumKeyEx(
				key,
				index++,
				buffer,
				&size,
				NULL,
				NULL,
				NULL,
				&last_write);

	  if (result == ERROR_SUCCESS)
	    {
	      dd_path[length] = '\\';
	      dd_path[length + 1] = '\0';
	      strcpy(&dd_path[length + 1], buffer);

	      search_dyn_data(dd_path, hardware_key, lpt);
	
	      dd_path[length] = '\0';
	    }
	}
      while (result == ERROR_SUCCESS);

      RegCloseKey(key);
    }
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *	SEARCH_LOCAL_MACHINE
 *
 *	Searches recursively in Windows 95/98 Registry for parallel port info
 *	under HKEY_LOCAL_MACHINE\Enum.  When parallel port is found, calls
 *	search_dyn_data() to get the port address.
 */
void search_local_machine
(
 char *lm_path,
 char *dd_path
 )
{
  DWORD index;
  DWORD size;
  DWORD type;
  LONG result;
  HKEY key;
  int length;
  char buffer[1024];
  FILETIME last_write = {0};

  length = strlen(lm_path);

  if (RegOpenKeyEx(
		   HKEY_LOCAL_MACHINE,
		   lm_path,
		   0L,
		   KEY_READ,
		   &key)
      == ERROR_SUCCESS)
    {
      size = 1023;

      if (RegQueryValueEx(
			  key,
			  "PortName",
			  NULL,
			  &type,
			  (unsigned char *) buffer,
			  &size)
	  == ERROR_SUCCESS)
	{
	  if ((type == REG_SZ) &&
	      (size == 5) &&
	      (buffer[0] == 'L') &&
	      (buffer[1] == 'P') &&
	      (buffer[2] == 'T') &&
	      (buffer[3] >= '1') &&
	      (buffer[3] <= '4') &&
	      (buffer[4] == '\0'))
	    {
	      /* we found the entry in HKEY_LOCAL_MACHINE, now we need to */
	      /* find the corresponding entry under HKEY_DYN_DATA.  */
	      /* add 5 to lm_path to skip over "Enum" and backslash */
	      search_dyn_data(dd_path, &lm_path[5], (buffer[3] - '1'));
	    }
	}

      index = 0;

      do
	{
	  size = 1023;

	  result = RegEnumKeyEx(
				key,
				index++,
				buffer,
				&size,
				NULL,
				NULL,
				NULL,
				&last_write);

	  if (result == ERROR_SUCCESS)
	    {
	      lm_path[length] = '\\';
	      lm_path[length + 1] = '\0';
	      strcpy(&lm_path[length + 1], buffer);

	      search_local_machine(lm_path, dd_path);
	
	      lm_path[length] = '\0';
	    }
	}
      while (result == ERROR_SUCCESS);

      RegCloseKey(key);
    }
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *	GET_LPT_ADDRESSES_FROM_REGISTRY
 *
 *	Searches Win95/98 registry recursively to get I/O port addresses for
 *	parallel ports.
 */
void get_lpt_addresses_from_registry()
{
  char lm_path[1024];
  char dd_path[1024];

  strcpy(lm_path, "Enum");
  strcpy(dd_path, "Config Manager");
  search_local_machine(lm_path, dd_path);
}
#endif
#endif

void initialize_jtag_hardware_orig()
{
  if (specified_com_port)
    {
      com_port = open(serial_port_name, O_RDWR);
      if (com_port == -1)
	{
	  fprintf(stderr, "Error: can't open serial port \"%s\"\n",
		  serial_port_name);
	}
      else
	{
	  int i = 0, result = 0;
	  char data = 0;

	  data = 0x7e;
	  write(com_port, &data, 1);

	  for (i = 0; (i < 100) && (result != 1); ++i)
	    {
	      result = read(com_port, &data, 1);
	    }

	  if (result == 1)
	    {
	      data = 0x70; write(com_port, &data, 1); /* TDO echo off */
	      data = 0x72; write(com_port, &data, 1); /* auto LEDs off */
	      data = 0x74; write(com_port, &data, 1); /* ERROR LED off */
	      data = 0x76; write(com_port, &data, 1); /* DONE LED off */
	      data = 0x60; write(com_port, &data, 1); /* signals low */
	    }
	  else
	    {
	      fprintf(stderr, "Error: BitBlaster is not responding on %s\n",
		      serial_port_name);
	      close(com_port);
	      com_port = -1;
	    }
	}
    }
  else
    {
#if PORT == WINDOWS || PORT == DOS

#if PORT == WINDOWS
      if (windows_nt)
	{
	  initialize_nt_driver();
	}
      else
	{
#ifdef __BORLANDC__
	  fprintf(stderr, "Error: parallel port access is not available\n");
#else
	  if (!specified_lpt_addr)
	    {
	      get_lpt_addresses_from_registry();

	      lpt_addr = 0;

	      if (specified_lpt_port)
		{
		  lpt_addr = lpt_addresses_from_registry[lpt_port - 1];
		}

	      if (lpt_addr == 0)
		{
		  if (lpt_addresses_from_registry[3] != 0)
		    lpt_addr = lpt_addresses_from_registry[3];
		  if (lpt_addresses_from_registry[2] != 0)
		    lpt_addr = lpt_addresses_from_registry[2];
		  if (lpt_addresses_from_registry[1] != 0)
		    lpt_addr = lpt_addresses_from_registry[1];
		  if (lpt_addresses_from_registry[0] != 0)
		    lpt_addr = lpt_addresses_from_registry[0];
		}

	      if (lpt_addr == 0)
		{
		  if (specified_lpt_port)
		    {
		      lpt_addr = lpt_addr_table[lpt_port - 1];
		    }
		  else
		    {
		      lpt_addr = lpt_addr_table[0];
		    }
		}
	    }
	  initial_lpt_ctrl = windows_nt ? 0x0c : read_byteblaster(2);
#endif
	}
#endif

#if PORT == DOS
      /*
       *	Read word at specific memory address to get the LPT port address
       */
      WORD *bios_address = (WORD *) 0x00400008;

      if (!specified_lpt_addr)
	{
	  lpt_addr = bios_address[lpt_port - 1];

	  if ((lpt_addr != 0x278) &&
	      (lpt_addr != 0x27c) &&
	      (lpt_addr != 0x378) &&
	      (lpt_addr != 0x37c) &&
	      (lpt_addr != 0x3b8) &&
	      (lpt_addr != 0x3bc))
	    {
	      lpt_addr = lpt_addr_table[lpt_port - 1];
	    }
	}
      initial_lpt_ctrl = read_byteblaster(2);
#endif

      /* set AUTO-FEED low to enable ByteBlaster (value to port inverted) */
      /* set DIRECTION low for data output from parallel port */
      write_byteblaster(2, (initial_lpt_ctrl | 0x02) & 0xDF);
#endif
    }
}

void close_jtag_hardware_orig()
{
  if (specified_com_port)
    {
      if (com_port != -1) close(com_port);
    }
  else
    {
#if PORT == WINDOWS || PORT == DOS
      /* set AUTO-FEED high to disable ByteBlaster */
      write_byteblaster(2, initial_lpt_ctrl & 0xfd);

#if PORT == WINDOWS
      if (windows_nt && (nt_device_handle != INVALID_HANDLE_VALUE))
	{
	  if (port_io_count > 0) flush_ports();

	  CloseHandle(nt_device_handle);
	}
#endif
#endif
    }
}

#if PORT == WINDOWS
/**************************************************************************/
/*                                                                        */

BOOL initialize_nt_driver()

/*                                                                        */
/*  Uses CreateFile() to open a connection to the Windows NT device       */
/*  driver.                                                               */
/*                                                                        */
/**************************************************************************/
{
  BOOL status = FALSE;

  ULONG buffer[1];
  ULONG returned_length = 0;
  char nt_lpt_str[] = { '\\', '\\', '.', '\\',
			'A', 'L', 'T', 'L', 'P', 'T', '1', '\0' };


  nt_lpt_str[10] = (char) ('1' + (lpt_port - 1));

  nt_device_handle = CreateFile(
				nt_lpt_str,
				GENERIC_READ | GENERIC_WRITE,
				0,
				NULL,
				OPEN_EXISTING,
				FILE_ATTRIBUTE_NORMAL,
				NULL);

  if (nt_device_handle == INVALID_HANDLE_VALUE)
    {
      fprintf(stderr,
	      "I/O error:  cannot open device %s\nCheck port number and device driver installation",
	      nt_lpt_str);
    }
  else
    {
      if (DeviceIoControl(
			  nt_device_handle,			/* Handle to device */
			  PGDC_IOCTL_GET_DEVICE_INFO_PP,	/* IO Control code */
			  (ULONG *)NULL,					/* Buffer to driver. */
			  0,								/* Length of buffer in bytes. */
			  &buffer,						/* Buffer from driver. */
			  sizeof(ULONG),					/* Length of buffer in bytes. */
			  &returned_length,				/* Bytes placed in data_buffer. */
			  NULL))							/* Wait for operation to complete */
	{
	  if (returned_length == sizeof(ULONG))
	    {
	      if (buffer[0] == PGDC_HDLC_NTDRIVER_VERSION)
		{
		  status = TRUE;
		}
	      else
		{
		  fprintf(stderr,
			  "I/O error:  device driver %s is not compatible\n(Driver version is %lu, expected version %lu.\n",
			  nt_lpt_str,
			  (unsigned long) buffer[0],
			  (unsigned long) PGDC_HDLC_NTDRIVER_VERSION);
		}
	    }
	  else
	    {
	      fprintf(stderr, "I/O error:  device driver %s is not compatible.\n",
		      nt_lpt_str);
	    }
	}

      if (!status)
	{
	  CloseHandle(nt_device_handle);
	  nt_device_handle = INVALID_HANDLE_VALUE;
	}
    }

  if (!status)
    {
      /* error message already given */
      exit(1);
    }

  return (status);
}
#endif

#if PORT == WINDOWS || PORT == DOS
/**************************************************************************/
/*                                                                        */

void write_byteblaster
(
 int port,
 int data
 )

/*                                                                        */
/**************************************************************************/
{
#if PORT == WINDOWS
  BOOL status = FALSE;

  int returned_length = 0;
  int buffer[2];


  if (windows_nt)
    {
      /*
       *	On Windows NT, access hardware through device driver
       */
      if (port == 0)
	{
	  port_io_buffer[port_io_count].data = (USHORT) data;
	  port_io_buffer[port_io_count].command = PGDC_WRITE_PORT;
	  ++port_io_count;

	  if (port_io_count >= PORT_IO_BUFFER_SIZE) flush_ports();
	}
      else
	{
	  if (port_io_count > 0) flush_ports();

	  buffer[0] = port;
	  buffer[1] = data;

	  status = DeviceIoControl(
				   nt_device_handle,			/* Handle to device */
				   PGDC_IOCTL_WRITE_PORT_PP,	/* IO Control code for write */
				   (ULONG *)&buffer,			/* Buffer to driver. */
				   2 * sizeof(int),			/* Length of buffer in bytes. */
				   (ULONG *)NULL,				/* Buffer from driver.  Not used. */
				   0,							/* Length of buffer in bytes. */
				   (ULONG *)&returned_length,	/* Bytes returned.  Should be zero. */
				   NULL);						/* Wait for operation to complete */

	  if ((!status) || (returned_length != 0))
	    {
	      fprintf(stderr, "I/O error:  Cannot access ByteBlaster hardware\n");
	      CloseHandle(nt_device_handle);
	      exit(1);
	    }
	}
    }
  else
#endif
    {
      /*
       *	On Windows 95, access hardware directly
       */
      outp((WORD)(port + lpt_addr), (WORD)data);
    }
}

/**************************************************************************/
/*                                                                        */

int read_byteblaster
(
 int port
 )

/*                                                                        */
/**************************************************************************/
{
  int data = 0;

#if PORT == WINDOWS

  BOOL status = FALSE;

  int returned_length = 0;


  if (windows_nt)
    {
      /* flush output cache buffer before reading from device */
      if (port_io_count > 0) flush_ports();

      /*
       *	On Windows NT, access hardware through device driver
       */
      status = DeviceIoControl(
			       nt_device_handle,			/* Handle to device */
			       PGDC_IOCTL_READ_PORT_PP,	/* IO Control code for Read */
			       (ULONG *)&port,				/* Buffer to driver. */
			       sizeof(int),				/* Length of buffer in bytes. */
			       (ULONG *)&data,				/* Buffer from driver. */
			       sizeof(int),				/* Length of buffer in bytes. */
			       (ULONG *)&returned_length,	/* Bytes placed in data_buffer. */
			       NULL);						/* Wait for operation to complete */

      if ((!status) || (returned_length != sizeof(int)))
	{
	  fprintf(stderr, "I/O error:  Cannot access ByteBlaster hardware\n");
	  CloseHandle(nt_device_handle);
	  exit(1);
	}
    }
  else
#endif
    {
      /*
       *	On Windows 95, access hardware directly
       */
      data = inp((WORD)(port + lpt_addr));
    }

  return (data & 0xff);
}

#if PORT == WINDOWS
void flush_ports(void)
{
  ULONG n_writes = 0L;
  BOOL status;

  status = DeviceIoControl(
			   nt_device_handle,			/* handle to device */
			   PGDC_IOCTL_PROCESS_LIST_PP,	/* IO control code */
			   (LPVOID)port_io_buffer,		/* IN buffer (list buffer) */
			   port_io_count * sizeof(struct PORT_IO_LIST_STRUCT),/* length of IN buffer in bytes */
			   (LPVOID)port_io_buffer,	/* OUT buffer (list buffer) */
			   port_io_count * sizeof(struct PORT_IO_LIST_STRUCT),/* length of OUT buffer in bytes */
			   &n_writes,					/* number of writes performed */
			   0);							/* wait for operation to complete */

  if ((!status) || ((port_io_count * sizeof(struct PORT_IO_LIST_STRUCT)) != n_writes))
    {
      fprintf(stderr, "I/O error:  Cannot access ByteBlaster hardware\n");
      CloseHandle(nt_device_handle);
      exit(1);
    }

  port_io_count = 0;
}
#endif /* PORT == WINDOWS */
#endif /* PORT == WINDOWS || PORT == DOS */

void delay_loop(long count)
{
  while (count != 0L) count--;
}

