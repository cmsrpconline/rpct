#ifndef RPCT_DCCSTATEMACHINE_H
#define RPCT_DCCSTATEMACHINE_H
#include "Msgs.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStubImpl.h"
#include "xdaq/exception/Exception.h"
#include "xdaq/NamespaceURI.h"

#include "xdata/XStr.h"
#include "xdata/Float.h"
#include "xdata/Bag.h"
#include "xdata/Boolean.h"
#include "xdata/Integer.h"
  	
#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/domutils.h"
#include "xoap/Method.h"
  	
#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/fsm/FailedEvent.h"
#include <log4cplus/configurator.h>

class XdaqDccAccess;
class DccHwAccess;
class DccMonitorWatcher;

class DccStateMachine : public toolbox::lang::Class {
public:

  DccStateMachine(xdaq::Application* aXD, DccHwAccess* aHW, rpct::Msgs & aM);

  virtual ~DccStateMachine();

  xdata::String & stateName() { return theStateName; }

  void executeCommand(const std::string & command);
  bool isCommandAllowed(const std::string & command);
  void reset() { theFSM.reset(); stateChanged(theFSM); }
 
  // Finite State Machine action
  void ConfigureAction(toolbox::Event::Reference e)         throw(toolbox::fsm::exception::Exception);
  void EnableAction(toolbox::Event::Reference e)            throw(toolbox::fsm::exception::Exception);
  void DisableAction(toolbox::Event::Reference e)           throw(toolbox::fsm::exception::Exception);
  void SuspendAction(toolbox::Event::Reference e)           throw(toolbox::fsm::exception::Exception);
  void ResumeAction(toolbox::Event::Reference e)            throw(toolbox::fsm::exception::Exception);
  void HaltAction(toolbox::Event::Reference e)              throw(toolbox::fsm::exception::Exception);
  void TTSTestModeAction (toolbox::Event::Reference e)      throw(toolbox::fsm::exception::Exception);
  void TestTTSAction (toolbox::Event::Reference e)          throw(toolbox::fsm::exception::Exception);
  void failedTransition(toolbox::Event::Reference e)        throw(toolbox::fsm::exception::Exception);

protected:

  xdata::String  theStateName; 
  xdata::String  theGlobalConfKey;
  xdata::String  theRunType;
  xdata::Integer theTtsTestFedId;
  xdata::String  theTtsTestMode;
  xdata::Integer theTtsTestPattern;
  xdata::Integer theTtsTestSequenceRepeat;

  toolbox::fsm::FiniteStateMachine theFSM;

private:   
#include "FUNCTION_XXX.icc"

  void stateChanged(toolbox::fsm::FiniteStateMachine & fsm) throw(toolbox::fsm::exception::Exception);

  DccHwAccess * theHW;
  log4cplus::Logger theLogger;
  rpct::Msgs & theMsgs;
  DccMonitorWatcher * theMonitor;
};
#endif
