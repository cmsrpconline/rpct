#ifndef RPCT_MSGS_H
#define RPCT_MSGS_H

#include <sstream>
#include <toolbox/BSem.h> 

namespace rpct {
class Msgs {
public:

  Msgs() : messagesMutex_(toolbox::BSem::FULL) {}

  std::ostream& msgs() { return messages_; } 

  operator std::string() const { return messages_.str(); }
  std::string str()      const { return messages_.str(); }

  Msgs & operator=(const std::string &a) { messages_.str(a); return *this; }

  void info(const std::string& msg) {
    time_t timer = time(NULL); 
    std::string timeFormated(asctime(localtime(&timer))); 
    timeFormated.resize(timeFormated.size() -1); 
    messagesMutex_.take(); 
    msgs()<<timeFormated<<" " << msg << std::endl; 
    messagesMutex_.give(); 
  }

  void warn(const std::string& msg) {
    time_t timer = time(NULL); 
    std::string timeFormated(asctime(localtime(&timer))); 
    timeFormated.resize(timeFormated.size() -1); 
    messagesMutex_.take(); 
    msgs() << "<span style=\"color: blue;\">" <<timeFormated<<" "<< msg << "</span>"<< std::endl; 
    messagesMutex_.give(); 
  } 

  void error(const std::string& msg) {
    time_t timer = time(NULL); 
    std::string timeFormated(asctime(localtime(&timer))); 
    timeFormated.resize(timeFormated.size() -1); 
    messagesMutex_.take(); 
    msgs() << "<span style=\"color: red; font-weight: bold;\">" <<timeFormated<<" "<< msg << "</span>"<< std::endl; \
    messagesMutex_.give(); 
  }


private:
  toolbox::BSem messagesMutex_;
  std::ostringstream  messages_;
};
};
#endif
