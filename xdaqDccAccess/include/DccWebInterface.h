#ifndef RPCT_DccWebInterface
#define RPCT_DccWebInterface

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/exception/Exception.h"
#include "xdaq/NamespaceURI.h"

#include "xdaq/WebApplication.h"
#include "xgi/Method.h"
#include "cgicc/HTMLClasses.h"

#include <log4cplus/configurator.h>
#include "Msgs.h"

class DccHwAccess;
class DccStateMachine;

class DccWebInterface : public toolbox::lang::Class {
public:
  DccWebInterface(xdaq::Application* aXD, DccHwAccess* aHW, DccStateMachine* aFSM, rpct::Msgs & aM);
  virtual ~DccWebInterface(){}

  void Default(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
  void expert(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
  void status(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
  void setTTS(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);

private:
#include "FUNCTION_XXX.icc"

  // action callbacks passed to FSM
  void passToFSM( xgi::Input* in, xgi::Output* out, std::string command);
  void configure(xgi::Input* in, xgi::Output* out) throw(xgi::exception::Exception) { passToFSM(in, out, "Configure"); }
  void run(xgi::Input* in, xgi::Output* out) throw(xgi::exception::Exception)       { passToFSM(in, out, "Start"); }
  void stop(xgi::Input* in, xgi::Output* out) throw(xgi::exception::Exception)      { passToFSM(in, out, "Stop"); }
  void testTTS(xgi::Input* in, xgi::Output* out) throw(xgi::exception::Exception)   { passToFSM(in, out, "TTSTestMode"); }
  void halt(xgi::Input* in, xgi::Output* out) throw(xgi::exception::Exception)      { passToFSM(in, out, "Halt"); }

  // action callbacks not changeing FSM (status readout)
  void forceTTS(xgi::Input* in, xgi::Output* out) throw(xgi::exception::Exception);
  void checkBoard(xgi::Input* in, xgi::Output* out) throw(xgi::exception::Exception);
  void checkErrorCounters(xgi::Input* in, xgi::Output* out) throw(xgi::exception::Exception);


  cgicc::input createButton(std::string name, std::string url, bool enabled);
  void displayStatus(xgi::Output* out) throw(xgi::exception::Exception);
  void preparePage(xgi::Output* out,const std::string & pageName,  const std::string & refresh = std::string("OFF"));
  void printBoxMsgs(xgi::Output* out, bool clearButton = true) throw(xgi::exception::Exception); 
  void clearMsgs(xgi::Input* in, xgi::Output* out) throw(xgi::exception::Exception);
  void clearStatus(xgi::Input* in, xgi::Output* out) throw(xgi::exception::Exception);

  enum {Info, Expert, Status, TTS } thePage;
  void lastPage(xgi::Input* in, xgi::Output* out);

  xdaq::Application* theXD; 
  DccHwAccess* theHW;
  DccStateMachine* theFSM;
  log4cplus::Logger theLogger;
  rpct::Msgs & theMsgs;

  std::ostringstream osStatus; //temporary solution! 
};
#endif
