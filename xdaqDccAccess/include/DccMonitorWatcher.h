#ifndef _DccMonitorWatcher_H_
#define _DccMonitorWatcher_H_

#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/Action.h"
#include "toolbox/TimeInterval.h"

class DccHwAccess;
namespace log4cplus { class Logger; }
namespace xdaq { class Application; }

class DccMonitorWatcher : public toolbox::task::TimerListener {
public:
  DccMonitorWatcher(xdaq::Application* aXD, DccHwAccess *);
  virtual ~DccMonitorWatcher();
  virtual void timeExpired(toolbox::task::TimerEvent& e);
  void checkEnable();
  void checkDisable();
private:
  DccHwAccess * theHW;
  log4cplus::Logger & theLogger;
  toolbox::task::Timer * timer_; 
};

#endif 
