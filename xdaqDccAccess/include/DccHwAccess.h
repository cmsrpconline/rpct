#ifndef RPCT_DCCHWACCESS
#define RPCT_DCCHWACCESS

#include "xdaq/exception/Exception.h"
#include "xdaq/NamespaceURI.h"

#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/domutils.h"
#include "xoap/Method.h"

#include "xdata/exception/Exception.h"
#include "xdata/XStr.h"
#include "xdata/Float.h"
#include "xdata/Bag.h"
#include "xdata/Boolean.h"
#include "xdata/Integer.h"

#include "toolbox/lang/Class.h"
#include "toolbox/BSem.h"
#include <log4cplus/configurator.h>

#include "rpct/devices/LinkSystem.h"
#include "rpct/xdaqutils/XdaqDbServiceClient.h"
#include "rpct/devices/Dcc.h"
#include "rpct/devices/DccMonitorables.h"
#include "rpct/devices/Ccs.h"


#include <vector>

#include "Msgs.h"

namespace rpct { class CcsMonitorables; }

class DccHwAccess : public toolbox::lang::Class {

public:

  DccHwAccess( rpct::xdaqutils::XdaqDbServiceClient* aDB,
               log4cplus::Logger & aL, 
               rpct::Msgs & aM);

  virtual ~DccHwAccess();

  void clear();
  void configure();
  void setupBoards();
  void resetErrorCounters();
  std::vector< std::pair<int,int> > dccFedEventNumber();
  void forceTTS(unsigned int status=0x8, bool forceOnCCS=true);
  void checkBoard(std::vector<rpct::DccMonitorables> &dccInfo, std::vector<rpct::CcsMonitorables*> &ccsInfo);
  void checkBoard(std::ostringstream & str);

  void runDcc()   {  for(unsigned int iDcc = 0; iDcc < dccsVec.size(); iDcc++) dccsVec[iDcc]->run(); }
  void resetDcc() {  for(unsigned int iDcc = 0; iDcc < dccsVec.size(); iDcc++) dccsVec[iDcc]->reset(); }
  void initialiseCcs() { for(unsigned int iCcs = 0; iCcs < ccssVec.size(); iCcs++)  ccssVec[iCcs]->initialize(); }

  xdata::String & systemFileName() { return theSystemFileName; }
  xdata::String & settingsFileName() { return theSettingsFileName; }
  xdata::Boolean & mock() { return theMock; }

  xoap::MessageReference onGetErrorCounters(xoap::MessageReference msg) throw (xoap::exception::Exception);


private:
#include "FUNCTION_XXX.icc"

  rpct::xdaqutils::XdaqDbServiceClient* theDbServiceClient;
  log4cplus::Logger theLogger;

  xdata::Boolean theMock;
  xdata::String theSystemFileName;
  xdata::String theSettingsFileName;

  rpct::Msgs & theMsgs;
  rpct::LinkSystem* system;

  std::vector<rpct::Dcc*> dccsVec;
  std::vector<rpct::Ccs*> ccssVec;

  toolbox::BSem theHardwareMutex;

};
#endif
