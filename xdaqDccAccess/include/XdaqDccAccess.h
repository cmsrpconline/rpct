#ifndef _XDAQDCCACCESS_H_
#define _XDAQDCCACCESS_H_

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"
#include "xdaq/exception/Exception.h" 
#include "xoap/MessageReference.h"
#include "xdata/String.h"
#include "xgi/Method.h"

#include "Msgs.h"

class DccStateMachine;
class DccWebInterface;
class DccHwAccess;

class XdaqDccAccess: public xdaq::Application, public xgi::framework::UIManager {

public:

  XDAQ_INSTANTIATOR();    
 
  XdaqDccAccess(xdaq::ApplicationStub * s);      
  virtual ~XdaqDccAccess();
                   
  // Default Web callback
  void Default(xgi::Input* in, xgi::Output* out) throw(xgi::exception::Exception);

  // SOAP Callback trigger state change for L1FM 
  xoap::MessageReference fireEvent(xoap::MessageReference msg) throw(xoap::exception::Exception);
  xoap::MessageReference reset(xoap::MessageReference msg) throw(xoap::exception::Exception);
	
private:

  DccHwAccess * theHW;
  DccStateMachine * theFSM;
  DccWebInterface* theWebGUI;
  rpct::Msgs theMsgs;
  log4cplus::Logger & theLogger;

};

#endif /*_XDAQLBOXACCESS_H_*/
