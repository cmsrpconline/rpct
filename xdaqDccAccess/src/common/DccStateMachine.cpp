#include "DccStateMachine.h"
#include "DccHwAccess.h"
#include "DccMonitorWatcher.h"

using namespace std;

DccStateMachine::~DccStateMachine()
{ delete theMonitor; }

DccStateMachine::DccStateMachine(
    xdaq::Application* aXD, 
    DccHwAccess * aHW, 
    rpct::Msgs & aM)
  : theHW(aHW),
    theLogger(log4cplus::Logger::getInstance(aXD->getApplicationLogger().getName()+".DccStateMachine")),
    theMsgs(aM),
    theMonitor(new DccMonitorWatcher(aXD,aHW))
{
  FUNCTION_INFO("initialisation");

  try { 
  theFSM.addState('H', "Halted",      this, &DccStateMachine::stateChanged); 
  theFSM.addState('C', "Configured",  this, &DccStateMachine::stateChanged);
  theFSM.addState('R', "Running",     this, &DccStateMachine::stateChanged);
  theFSM.addState('P', "Paused",      this, &DccStateMachine::stateChanged);
  theFSM.addState('T', "TTSTestMode", this, &DccStateMachine::stateChanged);

  theFSM.addStateTransition('H', 'C', "Configure",  this, &DccStateMachine::ConfigureAction);
  theFSM.addStateTransition('C', 'R', "Start",      this, &DccStateMachine::EnableAction);
  theFSM.addStateTransition('R', 'P', "Pause",      this, &DccStateMachine::SuspendAction);
  theFSM.addStateTransition('P', 'R', "Resume",     this, &DccStateMachine::ResumeAction);
  theFSM.addStateTransition('H', 'T', "TTSTestMode",this, &DccStateMachine::TTSTestModeAction);
  theFSM.addStateTransition('T', 'T', "TestTTS",    this, &DccStateMachine::TestTTSAction);

  theFSM.addStateTransition('H', 'H', "Halt",       this, &DccStateMachine::HaltAction);
  theFSM.addStateTransition('C', 'H', "Halt",       this, &DccStateMachine::HaltAction);
  theFSM.addStateTransition('R', 'H', "Halt",       this, &DccStateMachine::HaltAction);
  theFSM.addStateTransition('P', 'H', "Halt",       this, &DccStateMachine::HaltAction);
  theFSM.addStateTransition('T', 'H', "Halt",       this, &DccStateMachine::HaltAction);
  theFSM.addStateTransition('R', 'C', "Stop",       this, &DccStateMachine::DisableAction);
  theFSM.addStateTransition('P', 'C', "Stop",       this, &DccStateMachine::DisableAction);

  // Failure state setting
  theFSM.setFailedStateTransitionAction(this,  &DccStateMachine::failedTransition);
  theFSM.setFailedStateTransitionChanged(this, &DccStateMachine::stateChanged);

  theFSM.setStateName('F', "Failed"); // give a name to the 'F' state

  theFSM.setInitialState('H'); 
  theFSM.reset();
  stateChanged(theFSM);

  aXD->getApplicationInfoSpace()->fireItemAvailable("stateName", &theStateName);
  aXD->getApplicationInfoSpace()->fireItemAvailable("GLOBAL_CONF_KEY", &theGlobalConfKey);
  aXD->getApplicationInfoSpace()->fireItemAvailable("RUN_TYPE", &theRunType);
  aXD->getApplicationInfoSpace()->fireItemAvailable("TTS_TEST_FED_ID", &theTtsTestFedId);
  aXD->getApplicationInfoSpace()->fireItemAvailable("TTS_TEST_MODE", &theTtsTestMode);
  aXD->getApplicationInfoSpace()->fireItemAvailable("TTS_TEST_PATTERN", &theTtsTestPattern);
  aXD->getApplicationInfoSpace()->fireItemAvailable("TTS_TEST_SEQUENCE_REPEAT", &theTtsTestSequenceRepeat);

  }
  catch(xcept::Exception& e) {
    FUNCTION_ERROR("Exception cached during FSM setup: " << e.what());
  }
  catch(std::exception& e) {
    FUNCTION_ERROR("Exception cached while calling stateChanged: " << e.what());
  }

  FUNCTION_INFO("initialised, state is: "<<(std::string)stateName() ) ;
}


void DccStateMachine::executeCommand(const std::string & command)
{
  FUNCTION_INFO(command);
  try {
    toolbox::Event::Reference e(new toolbox::Event(command, this));
    theFSM.fireEvent(e);
  }
  catch(xcept::Exception& e) {
    FUNCTION_ERROR(" "<<command<<" XCEPT exception catched" <<e.what());
  }
  catch(std::exception& e) {
    FUNCTION_ERROR(" "<<command<<" STD exception catched" <<e.what());
  }
}


void DccStateMachine::ConfigureAction(toolbox::Event::Reference e) throw(toolbox::fsm::exception::Exception) 
{
  FUNCTION_INFO("");
  try {
    theHW->configure();
    theHW->setupBoards();
  }
  catch (std::exception& e) {
    throw toolbox::fsm::exception::Exception(" ", e.what()," ", 0," "); 
  }
}

void DccStateMachine::EnableAction(toolbox::Event::Reference e) throw(toolbox::fsm::exception::Exception)
{
  FUNCTION_INFO("");
  theHW->runDcc();
  theMonitor->checkEnable();
}

void DccStateMachine::DisableAction(toolbox::Event::Reference e) throw(toolbox::fsm::exception::Exception) 
{
  FUNCTION_INFO("");
  theHW->resetDcc();
  theMonitor->checkDisable();
}

void DccStateMachine::SuspendAction(toolbox::Event::Reference e) throw(toolbox::fsm::exception::Exception) 
{
  FUNCTION_INFO("triggered by event:"<<e->type()<<" ( has nothing to do...)");
}

void DccStateMachine::ResumeAction(toolbox::Event::Reference e) throw(toolbox::fsm::exception::Exception) 
{
  FUNCTION_INFO("triggered by event:"<<e->type()<<" ( has nothing to do...)");
}

void DccStateMachine::HaltAction(toolbox::Event::Reference e) throw(toolbox::fsm::exception::Exception) {
  FUNCTION_INFO("triggered by event:"<<e->type());
  theHW->resetDcc();
  theHW->initialiseCcs();
  theMonitor->checkDisable();
}

void DccStateMachine::TTSTestModeAction (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception) 
{
  FUNCTION_INFO("");
  theHW->configure();
  theHW->setupBoards();
}

void  DccStateMachine::TestTTSAction (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception) 
{
  FUNCTION_ERROR(" this function is not yet implemented!");
//  theXD->TestTTSAction(e);
} 

void DccStateMachine::failedTransition(toolbox::Event::Reference e) throw(toolbox::fsm::exception::Exception) {
  toolbox::fsm::FailedEvent & fe = dynamic_cast<toolbox::fsm::FailedEvent&>(*e);
  FUNCTION_ERROR( "Failure occurred when performing transition from: " << fe.getFromState() <<  " to: " << fe.getToState() << " exception: " << fe.getException().what() );
}

void DccStateMachine::stateChanged(toolbox::fsm::FiniteStateMachine & fsm) throw(toolbox::fsm::exception::Exception) {
  theStateName = fsm.getStateName(fsm.getCurrentState());
  FUNCTION_INFO("state is:" << stateName().toString() );
}


bool DccStateMachine::isCommandAllowed(const std::string & command) {
  typedef std::map<std::string, toolbox::fsm::State, std::less<std::string> > Transitions;
  Transitions transitions = theFSM.getTransitions(theFSM.getCurrentState());
  return transitions.find(command) != transitions.end();
}

