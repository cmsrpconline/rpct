#include "DccMonitorWatcher.h"
#include "DccHwAccess.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStubImpl.h"
#include "xdaq/exception/Exception.h"
#include "xdaq/NamespaceURI.h"

#include <log4cplus/configurator.h>

using namespace std;
using namespace rpct;
using namespace rpct::xdaqutils;

DccMonitorWatcher::DccMonitorWatcher(xdaq::Application* aXD, DccHwAccess *aHW) 
  : theHW(aHW),  theLogger(aXD->getApplicationLogger()), timer_(0)
{
  timer_ = toolbox::task::getTimerFactory()->createTimer("DccDebugTimer." + aXD->getApplicationDescriptor()->getURN());
  timer_->stop();
}

DccMonitorWatcher::~DccMonitorWatcher()
{ }

void DccMonitorWatcher::timeExpired(toolbox::task::TimerEvent& e)
{
  std::vector< std::pair<int,int> > dccFedEventNumber;
  try {
     dccFedEventNumber = theHW->dccFedEventNumber();
  }
  catch(xcept::Exception& e) {
//     LOG4CPLUS_INFO(theLogger,"Exception catched, what=", e.what());
  }
  catch(std::exception& e) {
//     FUNCTION_ERROR("std::exception catched, what="<<e.what());
  }

  bool printNow = false;
  static std::vector<int> lastDccEvent(3,-1);
  static std::vector<int> lastDccPrint(3,-1);

  for (unsigned int iDcc=0; (iDcc < dccFedEventNumber.size() && iDcc <3) ; iDcc++) {

    int ebEventNumber = dccFedEventNumber[iDcc].second; 

    if ( (ebEventNumber == lastDccEvent[iDcc]) && (ebEventNumber != lastDccPrint[iDcc]) ) { printNow = true; }
    if (printNow) LOG4CPLUS_INFO(theLogger, "FED: "<< dccFedEventNumber[iDcc].first <<" Ev: "<<ebEventNumber<<" LastEv: "<<lastDccEvent[iDcc]<<" LastPrt: "<<lastDccPrint[iDcc]);
    lastDccEvent[iDcc] = ebEventNumber;
    if (printNow) lastDccPrint[iDcc] = ebEventNumber;
  }

  if (printNow) {
    std::ostringstream status;
    theHW->checkBoard(status);
    LOG4CPLUS_INFO(theLogger, status.str() );
  }
}
/*
void DccMonitorWatcher::timeExpired(toolbox::task::TimerEvent& e)
{
  std::vector<rpct::DccMonitorables > dccInfo;
  std::vector<rpct::CcsMonitorables * > ccsInfo;
  try {
     theHW->checkBoard(dccInfo, ccsInfo);
  }
  catch(xcept::Exception& e) {
//     LOG4CPLUS_INFO(theLogger,"Exception catched, what=", e.what());
  }
  catch(std::exception& e) {
//     FUNCTION_ERROR("std::exception catched, what="<<e.what());
  }

  bool printNow = false;
  static std::vector<int> lastDccEvent(3,-1);
  static std::vector<int> lastDccPrint(3,-1);

  for (int iDcc=0; iDcc < dccInfo.size(), iDcc <3 ; iDcc++) {
    if ( (dccInfo[iDcc].getEbEvtCnt() == lastDccEvent[iDcc]) && (lastDccPrint[iDcc] != dccInfo[iDcc].getEbEvtCnt()) ) { printNow = true; }
    lastDccEvent[iDcc] = dccInfo[iDcc].getEbEvtCnt();
    if (printNow) lastDccPrint[iDcc] = dccInfo[iDcc].getEbEvtCnt();
  }

  if (printNow) {
    std::ostringstream status;
    theHW->checkBoard(status);
    LOG4CPLUS_INFO(theLogger, status.str() );
  }
}
*/

/*
void DccMonitorWatcher::timeExpired(toolbox::task::TimerEvent& e)
{
  try {

    LOG4CPLUS_INFO(theLogger,e.getTimerTask()->name);
    std::ostringstream status;
    theHW->checkBoard(status);
    theHW->resetErrorCounters();
  }
  catch (...) {
    LOG4CPLUS_ERROR(theLogger,"DccMonitorWatcher - exception catched while checkBoard exectuted");
  }
}
*/

void DccMonitorWatcher::checkEnable()
{
  if (timer_ && !timer_->isActive()) {
    LOG4CPLUS_INFO(theLogger," DccMonitorWatcher - check enabled");
    timer_->start();
    if (timer_->getScheduledTasks().empty()) {
      toolbox::TimeInterval interval(5.0);   // delay [s] before reading monitorables for the next board
      toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
      timer_->scheduleAtFixedRate(start, this, interval, 0, "DccMonitorablesChecker");
    }
  }
}

void DccMonitorWatcher::checkDisable()
{
   if (timer_ && timer_->isActive()) {
     try {
       timer_->remove("DccMonitorablesChecker"); 
       LOG4CPLUS_INFO(theLogger," DccMonitorWatcher - checkdisabled!");
     } 
     catch (...) {
       LOG4CPLUS_ERROR(theLogger,"DccMonitorWatcher - faile do disable timer");
     }
     timer_->stop();
   }
}

