#include "DccHwAccess.h"

#include "rpct/xdaqutils/XdaqDbSystemBuilder.h"
#include "rpct/devices/XmlSystemBuilder.h"
#include "rpct/hardwareTests/XmlDevicesSettings.h"

#include "rpct/devices/CcsSettings.h"
#include "rpct/devices/DccDevice.h"
#include "rpct/devices/DccMonitorables.h"
#include "rpct/devices/CcsMonitorables.h"

using namespace std;
using namespace rpct;
using namespace rpct::xdaqutils;

DccHwAccess::DccHwAccess(
    rpct::xdaqutils::XdaqDbServiceClient* aDB, log4cplus::Logger & aL, rpct::Msgs & aM)
  : theDbServiceClient(aDB),
    theLogger(log4cplus::Logger::getInstance(aL.getName()+".DccHwAccess")),
    theMock(false),    
    theMsgs(aM),
    system(0),
    theHardwareMutex(toolbox::BSem::FULL)
{
}

DccHwAccess::~DccHwAccess()
{
  FUNCTION_INFO("");
  clear();
  delete theDbServiceClient;
}

void DccHwAccess::clear() 
{
  delete system; system = 0;
  ccssVec.clear();
  dccsVec.clear();
}

void DccHwAccess::configure()
{
  std::stringstream version; 
  version << " SOFTWARE COMPILED WITH FIRMWARE VERSION IHA_VER: 0x"<<std::hex<<IH_VERSION_VAL<<", EB_VER: 0x"<<EB_VERSION_VAL<<", EM_VER: 0x"<<256*EM_VERSION2_VAL+EM_VERSION1_VAL;
  
  FUNCTION_INFO(version.str()); 
  if(system != 0) {
    FUNCTION_WARNING(" system Has been prevously configured. Skipping");
    return;
  }

// rcmsStateNotifier_.findRcmsStateListener();
  system = new LinkSystem();
  try {
    if(systemFileName().toString().empty()) {
      // LOG4CPLUS_INFO(theLogger, "Configuring system from DB ");
      // XdaqDbSystemBuilder(this, *system, mock).buildTriggerCrate("pcrpct03", 1972, "XdaqDccAccess", getApplicationDescriptor()->getInstance()); // TODO pass real hostname, port instance //should be ControlCrate??
      ; 
    }
    else {
      FUNCTION_INFO( "Configuring system from file " << systemFileName().toString() )
      XmlSystemBuilder(*system, mock() ).build( systemFileName().toString() );
    }
    if(! (bool)mock()) {
      try {
        system->checkVersions();
      }
      catch(rpct::EBadDeviceVersion& e) {
        FUNCTION_ERROR(e.what());
        throw TException("BAD DEVICE VERSION");
      }
      const System::BoardMap& boardMap = system->getBoardMap();
      for(System::BoardMap::const_iterator itBoard = boardMap.begin(); itBoard != boardMap.end(); itBoard++) {
        if(dynamic_cast<Dcc*>(itBoard->second) != 0) {
          dccsVec.push_back(dynamic_cast<Dcc*>(itBoard->second));
          LOG4CPLUS_TRACE(theLogger, "added " <<  (itBoard->second)->getDescription()<<" "<<(itBoard->second)->getId());
        }
        else if(dynamic_cast<Ccs*>(itBoard->second) != 0) {
          ccssVec.push_back(dynamic_cast<Ccs*>(itBoard->second));
          LOG4CPLUS_TRACE(theLogger, "added " <<  (itBoard->second)->getDescription()<<" "<<(itBoard->second)->getId());
        }
      }
    }
  }
//  catch(TException& e) {
//    FUNCTION_ERROR("TException catched what="<<e.what());
//    std::cout <<"************* HEXP1 *************" << std::endl;
//    cout << "Press 'y' to continue" << endl; string str; cin >> str; if (str != "y") { clear(); throw; }
//    clear(); 
//    throw;
//  }
  catch(std::exception& e) {
    FUNCTION_ERROR("std::exception catched, what="<<e.what());
    std::cout <<"************* catched exception (HERE2) *************" << std::endl;
//        cout << "Press 'y' to continue" << endl; string str; cin >> str; if (str != "y") { clear(); throw; }
//    clear(); 
    throw;
  }
  catch(...) {
    FUNCTION_ERROR( "Unknown c++ exception");
    std::cout <<"************* HEXP3 *************" << std::endl;
    clear(); throw;
  }
  FUNCTION_INFO("...done");
}


void DccHwAccess::setupBoards() 
{
  try {
    for(unsigned int iCcs = 0; iCcs < ccssVec.size(); iCcs++) { ccssVec[iCcs]->initialize(); }
    for(unsigned int iDcc = 0; iDcc < dccsVec.size(); iDcc++){
      dccsVec[iDcc]->reset();
      LOG4CPLUS_INFO(theLogger, "DCC reseted");
      dccsVec[iDcc]->initialize();
      LOG4CPLUS_INFO(theLogger, "DCC initialised");
    }
    if(settingsFileName().toString().empty()) {//z DB
      FUNCTION_ERROR("Setting up boards from DB NOT YET IMPLEMENTED");
    }
    else {
      FUNCTION_INFO("Setting up boards from file " << ((string)settingsFileName()));
      LOG4CPLUS_INFO(theLogger,"start system building");
      XmlDevicesSettings xmlDeviceSettings;
      xmlDeviceSettings.readFile((string)settingsFileName());
      ConfigurationSetImpl confSet;
      const System::DeviceMap& deviceMap = system->getDeviceMap();
      LOG4CPLUS_TRACE(theLogger, "Device map size " << deviceMap.size());

      //CCS configuration retrievied from positions of Boards
      for(unsigned int iCcs=0; iCcs<ccssVec.size(); iCcs++){
        unsigned int dccIn = 0x1f;
        for(unsigned int iDcc=0; iDcc<dccsVec.size(); iDcc++){
          int posDiff = ((dccsVec[iDcc]->getPosition()-ccssVec[iCcs]->getPosition()));
          if((posDiff<=4)&&(posDiff>1)){//3,0?
            dccIn &= ~(1 << (4-posDiff));//najdalsza plyta na in 0
          }
        }
        //chipIds.push_back(ccssVec[iCcs]->getTriggerFPGA().getId());
        DeviceSettingsPtr ccsSettingsPtr(new CcsSettings(~dccIn & 0x1f));
        confSet.addDeviceSettings(ccssVec[iCcs]->getTriggerFPGA().getId(),ccsSettingsPtr);
        //ccssVec[iCcs]->configure(new CcsSettings(~dccIn & 0x1f));
      }

      //DCCs configuration from XML
      LOG4CPLUS_TRACE(theLogger, "HERE Device map size(2) "<<deviceMap.size());
      for(System::DeviceMap::const_iterator iDevice = deviceMap.begin();iDevice != deviceMap.end(); ++iDevice) {
        LOG4CPLUS_TRACE(theLogger, "HERE Device map size(3) "<<deviceMap.size());
        LOG4CPLUS_TRACE(theLogger," *** setupBoards(): system has following device: " << iDevice->second->getId());
        if (dynamic_cast<DccDevice*>(iDevice->second) != 0){
          //chipIds.push_back(iDevice->second->getId());
          //a bit complicated casting to construct proper DeviceSettingsPtr(boost::shared_ptr)
          //DeviceSettingsPtr dccSettingsPtr(new DccSettingsImpl(*(dynamic_cast<DccSettingsImpl*>(xmlDeviceSettings.getDeviceSettings(*(iDevice->second))))));
          DeviceSettingsPtr dccSettingsPtr = xmlDeviceSettings.getDeviceSettingsPtr(*(iDevice->second));
          confSet.addDeviceSettings(iDevice->second->getId(),dccSettingsPtr);
          LOG4CPLUS_INFO(theLogger, "Adding chip id " << iDevice->second->getId());
        }
        else {
          LOG4CPLUS_INFO(theLogger, "Skipping setup for " << iDevice->second->getId());
        }
      }
      system->configureSelected(confSet);
      LOG4CPLUS_INFO(theLogger, "configureSelected done");
    }
  }
  catch(xcept::Exception& e) {
    FUNCTION_ERROR("xcept::Exception catched "<<e.what()<<" rethrow!");
    XCEPT_RETHROW(xcept::Exception, e.what(), e);
  }
  catch(std::exception& e) {
    FUNCTION_ERROR("std::exception catched"<<e.what());
    throw e;
  }
  FUNCTION_INFO("... done");
  resetErrorCounters();
}

void DccHwAccess::resetErrorCounters()
{
  FUNCTION_INFO("resetting Error Counters");
  try {
    for(unsigned int iDcc = 0; iDcc < dccsVec.size(); iDcc++) dccsVec[iDcc]->getDccDevice().resetErrorCounters();
  }
  catch(xcept::Exception& e) {
    FUNCTION_ERROR("xcept::Exception catched what="<<e.what()<<", rethrow!");
    XCEPT_RETHROW(xcept::Exception, e.what(), e);
  }
  catch(std::exception& e) {
    FUNCTION_ERROR("std::exception catched"<<e.what());
    throw e;
  }
}

void DccHwAccess::forceTTS(unsigned int status, bool forceOnCCS) 
{
  if(forceOnCCS) for(unsigned int iCcs = 0; iCcs < ccssVec.size(); iCcs++) ccssVec[iCcs]->forceTTS(status); 
  else {
    for(unsigned int iCcs = 0; iCcs < ccssVec.size(); iCcs++) {
      ccssVec[iCcs]->freeTTS();
    }
    for(unsigned int iDcc = 0; iDcc < dccsVec.size(); iDcc++){
      dccsVec[iDcc]->forceTTS(status);
    }
  }
  FUNCTION_INFO("TTS status forced");
}

std::vector< std::pair<int,int> > DccHwAccess::dccFedEventNumber()
{
  std::vector< std::pair<int,int> > result;

  theHardwareMutex.take();
  for(unsigned int iDcc=0; iDcc<dccsVec.size(); iDcc++) {
    DccDevice& dcc = dccsVec[iDcc]->getDccDevice();
    result.push_back( std::make_pair( dcc.getSettings()->getFedId(), dcc.ebEventNumber() ) );
  }
  theHardwareMutex.give();

  return result;
}

void DccHwAccess::checkBoard(std::vector<DccMonitorables> &dccInfo, std::vector<CcsMonitorables*> &ccsInfo)
{
  theHardwareMutex.take();
  for(unsigned int iDcc=0; iDcc<dccsVec.size(); iDcc++) {
    DccMonitorables stat = dccsVec[iDcc]->checkBoard();
    dccInfo.push_back(stat);
  }
  for(unsigned int iCcs=0; iCcs<ccssVec.size(); iCcs++){
    CcsMonitorables* stat = ccssVec[iCcs]->checkBoard();
    ccsInfo.push_back(stat);
  }
  theHardwareMutex.give();
}

void DccHwAccess::checkBoard(std::ostringstream &str)
{
  theHardwareMutex.take();
  str.str("");
        for(unsigned int iDcc=0; iDcc<dccsVec.size(); iDcc++){
                DccMonitorables stat = dccsVec[iDcc]->checkBoard();
                str << "<b> Status of "<<dccsVec[iDcc]->getDescription()<<"  (FedId="<<std::dec<<stat.getFedId()<<") </b>"<<std::endl;
                str << "&nbsp; EvtNum "<<std::dec<<stat.getEbEvtCnt()<<std::endl;

                bitset<32> status((uint32_t)(stat.getEbStatus()));
                str << "&nbsp; Event Builder (EB)     status 0x"<<std::hex<<status.to_ulong()<<std::dec<<std::endl;

                str << "&nbsp;&nbsp;&nbsp; RUN: "<< status[EBB_STATUS_RUN] <<" LFFn: "<<status[EBB_STATUS_LFFn];
                str << "&nbsp;&nbsp;&nbsp; PLL1_LCK: "<< status[EBB_STATUS_PLL1_LOCKED]
                                       << ", DES1_LCK: "<< status[EBB_STATUS_DES1_LOCKED]
                                       << ", OUT_LCK: "<< status[EBB_STATUS_OUT_LOCKED]
                                       << ", &nbsp;&nbsp;&nbsp; QPLL_LCK: "<< status[EBB_STATUS_QPLL_LOCKED]
                                       << ", QPLL_LCK_LST: "<< status[EBB_STATUS_QPLL_LOCK_LOST]
                                       << ", &nbsp;&nbsp;&nbsp; TTC_RDY: "<< status[EBB_STATUS_TTC_READY]
                                       << ", TTC_RDY_LST: "<< status[EBB_STATUS_TTC_READY_LOST]
                      <<std::endl;

                str << "&nbsp;&nbsp;&nbsp; EVT_Not_Set: "<< status[EBB_STATUS_EV_NOT_SET]
                                       << ", BX_Not_Set: "<< status[EBB_STATUS_BX_NOT_SET];
                str << "&nbsp;&nbsp;&nbsp; IH_Almost_Full: "<< status[EBB_STATUS_IH_ALMOST_FULL]
                                       << ", IH_Full: "<< status[EBB_STATUS_IH_FULL]
                                       << ", IH_Over_Run: "<< status[EBB_STATUS_IH_OVERRUN];
                str << " &nbsp;&nbsp;&nbsp; " << "TRIGGQ_Almost_Full: "<< status[EBB_STATUS_TRIGQ_ALMOST_FULL]
                                                << ", TRIGGQ_Full: "<< status[EBB_STATUS_TRIGQ_FULL]
                                                << ", TRIGGQ_WRFull: "<< status[EBB_STATUS_TRIGQ_WRFULL]
                      <<std::endl;
                bitset<32> oosDbg(stat.getEbOosDbg());
                str <<"&nbsp;&nbsp;&nbsp; OssDbg: "<<oosDbg<<std::dec;
                bitset<32> eqlzStat(stat.getEbEqlzStatus());
                str <<"&nbsp;&nbsp;&nbsp; EQLZ_Stat: "<< eqlzStat.to_string() << std::endl;;

                str << "<small>" <<std::endl;
                std::vector<DccInputHandlerStatus> ihStatus = stat.inputHandlerStatuses();
                for(unsigned int iIh=0; iIh<9; iIh++){
                        str << ihStatus[iIh].print() << std::endl;
//              str << "&nbsp; Input Handler (IH) "<<iIh
//            << "&nbsp;&nbsp; Status 0x"<<std::hex<<stat->getIhStatus(iIh)
//                           << "&nbsp;&nbsp; EmMask "<<std::bitset<4>(stat->getIhEmergencyMask(iIh))
//            << "&nbsp;&nbsp; EvtNum "<<std::dec<<stat->getIhEvtCnt(iIh)<<std::endl;
                }
                str << "</small>";
        str << std::endl;
        }
        for(unsigned int iCcs=0; iCcs<ccssVec.size(); iCcs++){
                int ttsStatus = ccssVec[iCcs]->checkTTS();
                str << "TTS status from "<<ccssVec[iCcs]->getDescription()<<std::endl;
                str << "&nbsp;&nbsp; TTS_Stat: 0x"<<std::hex<<ttsStatus<<std::dec<<std::endl;
                str << "&nbsp;&nbsp;&nbsp; In1: " << bitset<4>(ttsStatus & 0xf)
                <<                 ",  In2: " << bitset<4>((ttsStatus & 0xf0) >> 4)
                <<                 ",  In3: " << bitset<4>((ttsStatus & 0xf00) >> 8)
                <<                 ",  In4: " << bitset<4>((ttsStatus & 0xf000) >> 12)
                <<                 ",  In5: " << bitset<4>((ttsStatus & 0xf0000) >> 16)
                <<                 " | Out: " << bitset<4>((ttsStatus & 0xf00000) >> 20)
                << std::endl;

                uint32_t status = ccssVec[iCcs]->checkStatus();
                str << "CCS " <<" Status: " <<hex<< (status & 0x7)
                                << ", TTCrx READY " <<(status & 0x1)
                            << ", QPLL LOCKED "   << ((status & 0x2)>>1)
                            << ", QPLL ERROR "    << ((status & 0x4)>>2) << std::endl ;

                str << "CCS CONFIG0 "<<hex << ccssVec[iCcs]->getTriggerFPGA().readWord(CONFIG0)<<std::endl;
                str << "CCS CONFIG1 "<<hex << ccssVec[iCcs]->getTriggerFPGA().readWord(CONFIG1)<<std::endl;
        }
        str << std::endl;
  theHardwareMutex.give();
//  LOG4CPLUS_INFO(theLogger, "Status checked: "<<std::endl<<str.str());
  LOG4CPLUS_INFO(theLogger, "Status checked: "<<std::endl);
  std::cout <<str.str()<< std::endl;

}


xoap::MessageReference DccHwAccess::onGetErrorCounters(xoap::MessageReference msg) throw (xoap::exception::Exception) {

    ostringstream str;
    configure();
    for(unsigned int iDcc=0; iDcc<dccsVec.size(); iDcc++){
        DccMonitorables stat = dccsVec[iDcc]->checkBoard();
	str << std::endl << dccsVec[iDcc]->getDescription() << ":" << std::endl;

        std::vector<DccInputHandlerStatus> ihStatus = stat.inputHandlerStatuses();
        for(unsigned int iIh=0; iIh<9; iIh++){
            str << ihStatus[iIh].print() << std::endl;
            
        }
    }
    xoap::MessageReference reply = xoap::createMessage();
    xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
    envelope.getBody().addTextNode(str.str());
    
    return reply;
}
