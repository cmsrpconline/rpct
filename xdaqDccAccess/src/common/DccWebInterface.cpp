#include "DccWebInterface.h"

#include "DccHwAccess.h"
#include "DccStateMachine.h"
#include "xgi/Utils.h"
#include "rpct/devices/DccMonitorables.h"
#include "rpct/devices/CcsMonitorables.h"


using namespace std;

DccWebInterface::DccWebInterface(xdaq::Application* aXD, DccHwAccess* aHW, DccStateMachine* aFSM,rpct::Msgs & aM)
  : theXD(aXD),
    theHW(aHW), 
    theFSM(aFSM),
    theLogger(log4cplus::Logger::getInstance(aXD->getApplicationLogger().getName()+".DccWebInterface")),
    theMsgs(aM)
{
  FUNCTION_INFO("initialisation")
  xgi::deferredbind(this, this, &DccWebInterface::Default, "Default");
  xgi::deferredbind(this, this, &DccWebInterface::expert, "Expert");
  xgi::deferredbind(this, this, &DccWebInterface::status, "Status");
  xgi::deferredbind(this, this, &DccWebInterface::setTTS, "setTTS");

  xgi::deferredbind(this, this, &DccWebInterface::configure, "configure");
  xgi::deferredbind(this, this, &DccWebInterface::run, "run");
  xgi::deferredbind(this, this, &DccWebInterface::stop, "stop");
  xgi::deferredbind(this, this, &DccWebInterface::halt, "halt");

  xgi::deferredbind(this, this, &DccWebInterface::testTTS, "testtts");
  xgi::deferredbind(this, this, &DccWebInterface::forceTTS, "forcetts");
  xgi::deferredbind(this, this, &DccWebInterface::checkBoard, "checkboard");
  xgi::deferredbind(this, this, &DccWebInterface::checkErrorCounters, "checkErrorCounters");

  xgi::deferredbind(this, this, &DccWebInterface::clearMsgs, "clearmessages");
  xgi::deferredbind(this, this, &DccWebInterface::clearStatus, "clearstatus");
}

void DccWebInterface::passToFSM( xgi::Input* in, xgi::Output* out, std::string command)
{
  FUNCTION_INFO(command);
  try { 
    theFSM->executeCommand(command); 
  }
  catch(xcept::Exception& e) { FUNCTION_ERROR("xcept::Exception catched, what="<<e.what()); }
  catch(std::exception& e) { FUNCTION_ERROR("std::Exception catched, what="<<e.what()); }
  lastPage(in, out);
}


void DccWebInterface::Default(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception)
{
  thePage = Info;
  std::string urlBase = "/" + theXD->getApplicationDescriptor()->getURN();
  preparePage(out,"DCC-Info","5");
  printBoxMsgs(out, false);

  // STATE MACHINE
  *out << cgicc::body()<<endl;
  *out << cgicc::h2("State: "+ (string)theFSM->stateName()) << cgicc::br();

  // GET STATUS OF HARDRARE
  std::vector<rpct::DccMonitorables > dccInfo;
  std::vector<rpct::CcsMonitorables * > ccsInfo;
  try {
     theHW->checkBoard(dccInfo, ccsInfo);
   }
   catch(xcept::Exception& e) {
     FUNCTION_ERROR("xcept::Exception catched, what="<<e.what());
   }
   catch(std::exception& e) {
     FUNCTION_ERROR("std::exception catched, what="<<e.what());
   }


  // PRINT FEDS
  if (ccsInfo.size() >0) *out << ccsInfo[0]->printTTS() << cgicc::br();
  for (std::vector<rpct::DccMonitorables>::const_iterator it = dccInfo.begin(); it != dccInfo.end(); ++it) *out << (*it).printInfo() << cgicc::br();
  for (std::vector<rpct::CcsMonitorables * >::const_iterator it = ccsInfo.begin(); it != ccsInfo.end(); ++it) *out << (*it)->printInfo() << cgicc::br();
  *out << cgicc::br();

  *out << "GoTo: ";
  *out << createButton(" Info ", urlBase + "/", false) <<" ";
  *out << createButton("Expert", urlBase + "/Expert", true) << " ";
  *out << createButton("Status", urlBase + "/Status", true) << " ";
  *out << createButton("setTTS", urlBase + "/setTTS", true) << " ";
  *out << cgicc::body();

  //xgi::Utils::getPageFooter(*out);
  *out << cgicc::html() << endl;;

//  unsigned int measTimeVal = 5;
//  rpct::tbsleep(measTimeVal*1000);
//  lastPage(in,out);
}

void DccWebInterface::expert(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception)
{
  thePage = Expert;
  std::string urlBase = "/" + theXD->getApplicationDescriptor()->getURN();
  preparePage(out,"DCC-Expert");
  printBoxMsgs(out);

  *out << cgicc::body()<<endl;
  *out << cgicc::h2("State: "+ (string)theFSM->stateName()) << cgicc::br();
  *out << "Commands: ";
  *out << createButton("Configure", urlBase + "/configure", theFSM->isCommandAllowed("Configure")) << " ";
  *out << createButton("Enable run", urlBase + "/run", theFSM->stateName() == "Configured")<<" ";
  *out << createButton("Disable run", urlBase + "/stop", theFSM->stateName() == "Running") <<" ";
  *out << createButton("Halt", urlBase + "/halt", true) << " ";
  *out << cgicc::br();
  *out << "GoTo:     ";
  *out << createButton(" Info ", urlBase + "/", true) <<" ";
  *out << createButton("Expert", urlBase + "/Expert", false) << " ";
  *out << createButton("Status", urlBase + "/Status", true) << " ";
  *out << createButton("setTTS", urlBase + "/setTTS", true) << " ";
  *out << cgicc::body();

  //xgi::Utils::getPageFooter(*out);
  *out << cgicc::html() << endl;;
}


void DccWebInterface::status(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception)
{
  thePage = Status;
  std::string urlBase = "/" + theXD->getApplicationDescriptor()->getURN();
  preparePage(out,"DCC-Status");
  printBoxMsgs(out);

  *out << cgicc::body()<<endl;
  *out << cgicc::h2("State: "+ (string)theFSM->stateName()) << cgicc::br();

  *out << createButton("Check Status", urlBase + "/checkboard", system !=0 );

        *out << cgicc::form().set("method","POST").set("action", urlBase + "/checkErrorCounters").set("enctype","multipart/form-data");
        *out << cgicc::input().set("type", "submit").set("align","center").set("name", "checkErrorCounters").set("value", "Error Measurm.").set("enabled");
        *out << "<font size=\"3\" face=\"Times New Roman, Times, serif\"> test time </font>"
        << "<select size=\"1\" name=\"measTime\" value=\"XXXX\">"
        << " <option value=\"0\">none</option>"
        << " <option value=\"10\">10 sec.</option>"
        << " <option value=\"60\"> 1 min.</option>"
        << " <option value=\"600\">10 min.</option>"
        << " <option value=\"3600\">1 hour</option>"
        << "</select> &nbsp;";
        *out << cgicc::form();

  *out << cgicc::br()<<cgicc::br();
  *out << "GoTo: ";
  *out << createButton("Info", urlBase + "/", true) <<" ";
  *out << createButton("Expert", urlBase + "/Expert", true) << " ";
  *out << createButton("Status", urlBase + "/Status", true) << " ";
  *out << createButton("setTTS", urlBase + "/setTTS", true) << " ";

  osStatus <<" ";
  displayStatus(out); 

  *out << cgicc::body();

  //xgi::Utils::getPageFooter(*out);
  *out << cgicc::html() << endl;;
}


void DccWebInterface::setTTS(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception)
{
  thePage = TTS;
  std::string urlBase = "/" + theXD->getApplicationDescriptor()->getURN();
  preparePage(out,"DCC-setTTS");
  printBoxMsgs(out);

  *out << cgicc::body()<<endl;
        *out << cgicc::br() <<  cgicc::br() << "Test TTS: &nbsp;";
        *out << createButton("Configure", urlBase + "/testtts", theFSM->isCommandAllowed("Configure")) << "&nbsp;";

        *out << cgicc::form().set("method","POST").set("action", urlBase + "/forcetts").set("enctype","multipart/form-data");
        *out << "<font size=\"3\" face=\"Times New Roman, Times, serif\"> sTTS bits </font>"
        << "<select size=\"1\" name=\"ttsBits\" value=\"1000\">"
        << " <option value=\"0001\">0001</option> <option value=\"0010\">0010</option>"
        << " <option value=\"0011\">0011</option> <option value=\"0100\">0100</option>"
        << " <option value=\"0101\">0101</option> <option value=\"0110\">0110</option>"
        << " <option value=\"0111\">0111</option> <option value=\"1000\">1000</option>"
        << " <option value=\"1001\">1001</option> <option value=\"1010\">1010</option>"
        << " <option value=\"1011\">1011</option> <option value=\"1100\">1100</option>"
        << " <option value=\"1101\">1101</option> <option value=\"1110\">1110</option>"
        << " <option value=\"1111\">1111</option> <option value=\"0000\">0000</option> "
        << "</select> &nbsp;";

        *out << "CCS <input type=\"radio\" name=\"ccsOrDcc\" value=\"CCS\" checked=\"checked\" />"
        << "DCCs <input type=\"radio\" name=\"ccsOrDcc\" value=\"DCCs\" &nbsp; /> ";
        std::string enabled = (theFSM->stateName() == "TTSTestMode") ? "enabled" : "disabled";
        *out << cgicc::input().set("type", "submit").set("align","center").set("name", "forceTTS").set("value", "Force TTS").set(enabled);
        *out << cgicc::form();
        *out << cgicc::br()  << cgicc::br() << "&nbsp;";



  *out << "GoTo: ";
  *out << createButton("Info", urlBase + "/", true) <<" ";
  *out << createButton("Expert", urlBase + "/Expert", true) << " ";
  *out << createButton("Status", urlBase + "/Status", true) << " ";
  *out << createButton("setTTS", urlBase + "/setTTS", true) << " ";
  *out << cgicc::body();
  //xgi::Utils::getPageFooter(*out);
  *out << cgicc::html() << endl;;
}

void DccWebInterface::forceTTS(xgi::Input* in, xgi::Output* out) throw(xgi::exception::Exception)
{
  try {
    cgicc::Cgicc formData(in);
    cgicc::form_iterator ttsBitsIt = formData.getElement("ttsBits");
    cgicc::form_iterator ccsOrDccIt = formData.getElement("ccsOrDcc");
    bool forceOnCCS = true;
    unsigned int ttsStatus = 0;

    if (!ttsBitsIt->isEmpty() && ttsBitsIt != (*formData).end()) {
      std::string ttsStatusStr = ttsBitsIt->getValue();
      if (ttsStatusStr.size()>4) FUNCTION_ERROR("You To many TTS bits, only 4 youngest taken");

      for (int i=ttsStatusStr.size()-1,j=0; i>=(int)ttsStatusStr.size()-4; --i,++j) {
        if(ttsStatusStr[i]=='1'){
          ttsStatus |= (1<<j);
        } 
        else if(ttsStatusStr[i]!='0'){
          FUNCTION_ERROR("Invalid TTS bits: " << ttsStatusStr << ", Use default (READY/1000/8)"); 
          ttsStatus = 0x8;
          break;
        }
      }
      FUNCTION_INFO("TTS: " << ttsStatus);
    }
    else { FUNCTION_WARNING("TTS state not entered. Use default (READY/1000/8)" ); }

    if( !ccsOrDccIt->isEmpty() && ccsOrDccIt != (*formData).end()) {
      std::string ccsOrDcc = ccsOrDccIt->getValue();
      FUNCTION_INFO("DCC: " << ccsOrDcc );
      if(ccsOrDcc != "CCS") forceOnCCS = false;
    }
    else { FUNCTION_INFO("CCS/DCC not choosen. Use default (CCS)" ); }

    theHW->forceTTS(ttsStatus, forceOnCCS);

  }
  catch(xcept::Exception& e) {
    FUNCTION_ERROR("xcept::Exception catched, what="<< e.what());
  }
  catch(std::exception& e) {
    FUNCTION_ERROR("std::Exception catched, what="<< e.what());
  }

  lastPage(in, out);
}


void DccWebInterface:: preparePage(xgi::Output* out, const std::string & pageName, const std::string & refresh)
{
  out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
  *out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
  *out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
  *out << cgicc::title(pageName) << std::endl;
  if (refresh != "OFF" ) *out << cgicc::meta().set("http-equiv", "Refresh").set("content", refresh);
/*  xgi::Utils::getPageHeader(out, "XdaqDccAccess - "+pageName,
      theXD->getApplicationDescriptor()->getContextDescriptor()->getURL(),
      theXD->getApplicationDescriptor()->getURN(),
      "/daq/xgi/images/Application.jpg");*/
}

void DccWebInterface::printBoxMsgs(xgi::Output* out, bool clearButton) throw (xgi::exception::Exception) 
{
    if (! theMsgs.str().empty()) {
        *out << "<div style=\"border:1px inset; background-color: #EFEFEF; padding-left: 10px; padding-right: 10px; \">";
        *out << "<div id=\"log\" style=\"max-height: 100px; overflow:auto; overflow-y:visible; font-family: monospace; \">";
        *out << cgicc::pre();
        *out << theMsgs.str();
        *out << cgicc::pre();
        *out << "</div>";
        *out << cgicc::hr();
        *out << createButton("Clear", "/"+(theXD->getApplicationDescriptor()->getURN())+"/clearmessages", clearButton);
        *out << "</div>";
        *out << "<script type=\"text/javascript\">";
        *out << "var objDiv = document.getElementById(\"log\");";
        *out << "objDiv.scrollTop = objDiv.scrollHeight;";
        *out << "</script>";
    }
}

void DccWebInterface::clearMsgs(xgi::Input* in, xgi::Output* out)  throw(xgi::exception::Exception) 
{
  theMsgs="";
  FUNCTION_INFO("");
  lastPage(in,out);
}

void DccWebInterface::clearStatus(xgi::Input* in, xgi::Output* out)  throw(xgi::exception::Exception) 
{
  osStatus.str("");
  lastPage(in,out);
}

void DccWebInterface::lastPage(xgi::Input* in, xgi::Output* out)
{
  switch (thePage) { 
    case Info:   { Default(in,out); break; }
    case Expert: { expert(in,out); break; }
    case Status: { status(in,out); break; }
    case TTS:    { setTTS(in,out); break; }
    default:       Default(in,out);
  }
}

cgicc::input DccWebInterface::createButton(std::string name, std::string url, bool enabled) {
    cgicc::input button;
    button.set("type", "button");
    button.set("value", name.c_str());
    button.set("onclick", "javascript:window.location ='" + url + "';");
    if (!enabled) button.set("disabled", "disabled"); 
    return button;
}

void DccWebInterface::checkBoard(xgi::Input* in, xgi::Output* out) throw(xgi::exception::Exception) {

        try {
                theHW->checkBoard(osStatus);
        }
        catch(xcept::Exception& e) {
          FUNCTION_ERROR("xcept::Exception catched, what="<<e.what());
        }
        catch(std::exception& e) {
          FUNCTION_ERROR("std::exception catched, what="<<e.what());
        }
        lastPage(in, out);
}


void DccWebInterface::checkErrorCounters(xgi::Input* in, xgi::Output* out) throw(xgi::exception::Exception)
{
        try {
          LOG4CPLUS_INFO(theLogger," *** checkErrorCounters *** ");
          cgicc::Cgicc formData(in);
          cgicc::form_iterator measTimeIt = formData.getElement("measTime");
          std::string measTimeStr;
          if( !measTimeIt->isEmpty() && measTimeIt != (*formData).end()) measTimeStr = measTimeIt->getValue();
          int measTimeVal =  std::atoi(measTimeStr.c_str());
          LOG4CPLUS_INFO(theLogger," *** checkErrorCounters, time (sek): *** " << measTimeVal);

          theHW->resetErrorCounters();
          rpct::tbsleep(measTimeVal*1000);
          theHW->checkBoard(osStatus);
        }
        catch(xcept::Exception& e) {
          FUNCTION_ERROR("xcept::Exception catched, what="<<e.what());
        }
        catch(std::exception& e) {
          FUNCTION_ERROR("std::exception catched, what="<<e.what());
        }
        lastPage(in, out);
}

void DccWebInterface::displayStatus(xgi::Output* out) throw(xgi::exception::Exception) {
        if(!osStatus.str().empty()) {
                *out << cgicc::br() << cgicc::br();
                *out << cgicc::div().set("style", "background-color: yellow;");
                std::istringstream istr(osStatus.str());
                std::string line;
                while(istr) {
                        getline(istr, line);
                        *out << line << cgicc::br();
                }
                *out << cgicc::hr();
                *out << createButton("Clear", "clearstatus", true);
                *out << line << cgicc::br();
                *out << cgicc::div();
        }
}
