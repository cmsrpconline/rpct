/*
 *
 *  Version: $Id: XdaqDccAccess.cpp,v 1.38 2008/11/21 11:39:49 tb Exp $
 *
 */

#include "XdaqDccAccess.h"

#include "xdata/exception/Exception.h"
#include "xdata/XStr.h"
#include "xdata/Float.h"
#include "xdata/Bag.h"
#include "xdata/Boolean.h"
#include "xdata/Integer.h"

#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"
#include "xoap/domutils.h"
#include "xdaq/NamespaceURI.h"

#include "xcept/tools.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "xdata/Binary.h"
#include "rpct/xdaqutils/XdaqDbServiceClient.h"
#include "rpct/xdaqutils/XdaqDbSystemBuilder.h"
#include "rpct/xdaqutils/LogUtils.h"
#include "rpct/devices/XmlSystemBuilder.h"

#include "DccHwAccess.h"
#include "DccStateMachine.h"
#include "DccWebInterface.h"

#include <log4cplus/configurator.h>

#include <algorithm>
#include <vector>

using namespace std;
using namespace rpct;
using namespace rpct::xdaqutils;

template <typename LISTENER>
void bind(xdaq::Application* app, LISTENER * obj, 
	  xoap::MessageReference (LISTENER::*func)(xoap::MessageReference) throw (xoap::exception::Exception), 
	  const std::string & messageName, 
	  const std::string & namespaceURI) 
{
  xoap::Method<LISTENER> * f = new xoap::Method<LISTENER>;
  f->obj_ = obj;
  f->func_ = func;
  f->namespaceURI_ = namespaceURI;
  f->name_ = messageName;
  std::string name = namespaceURI + ":";
  name += messageName;
  app->removeMethod(name);
  app->addMethod(f, name);
}


XDAQ_INSTANTIATOR_IMPL(XdaqDccAccess);

XdaqDccAccess::XdaqDccAccess(xdaq::ApplicationStub * s)
  : xdaq::Application(s), xgi::framework::UIManager(this), theHW(0), theFSM(0), theWebGUI(0), theLogger(getApplicationLogger())
{
  rpcLogSetup(getApplicationLogger(),this);
  LOG4CPLUS_INFO(theLogger,"Hello World!");

  theHW = new DccHwAccess(new XdaqDbServiceClient(this), this->getApplicationLogger(), theMsgs );
  theFSM = new DccStateMachine(    this, theHW, theMsgs );
  theWebGUI = new DccWebInterface( this, theHW, theFSM, theMsgs );

  typedef std::vector<toolbox::lang::Method* > METHODS;
  METHODS v =  theWebGUI->getMethods();
  for (METHODS::iterator i = v.begin(); i != v.end(); ++i) {
    if ((*i)->type() == "cgi") {
      std::string name = static_cast<xgi::MethodSignature*>( *i )->name();
//      theMsgs.msgs() << " (xgi) binding: " + name;
      xgi::deferredbind(this, this, &XdaqDccAccess::Default, name);
    }
  }

  // bind soap L1FSM callbacks 
  xoap::bind(this, &XdaqDccAccess::fireEvent, "Configure", XDAQ_NS_URI );
  xoap::bind(this, &XdaqDccAccess::fireEvent, "Start", XDAQ_NS_URI );
  xoap::bind(this, &XdaqDccAccess::fireEvent, "Stop", XDAQ_NS_URI );
  xoap::bind(this, &XdaqDccAccess::fireEvent, "Pause", XDAQ_NS_URI );
  xoap::bind(this, &XdaqDccAccess::fireEvent, "Resume", XDAQ_NS_URI );
  xoap::bind(this, &XdaqDccAccess::fireEvent, "Halt", XDAQ_NS_URI );
  xoap::bind(this, &XdaqDccAccess::fireEvent, "TTSTestMode", XDAQ_NS_URI );
  xoap::bind(this, &XdaqDccAccess::fireEvent, "TestTTS", XDAQ_NS_URI );
  xoap::bind(this, &XdaqDccAccess::reset, "Reset", XDAQ_NS_URI );
  bind(this, theHW, &DccHwAccess::onGetErrorCounters, "GetErrorCounters",XDAQ_NS_URI);  
  // Export a "State" variable that reflects the state of the state machine
  getApplicationInfoSpace()->fireItemAvailable("systemFileName", &(theHW->systemFileName()) );
  getApplicationInfoSpace()->fireItemAvailable("settingsFileName", &(theHW->settingsFileName()));
  getApplicationInfoSpace()->fireItemAvailable("mock", &(theHW->mock()));

}

XdaqDccAccess::~XdaqDccAccess() {
	LOG4CPLUS_DEBUG(this->getApplicationLogger(), __FUNCTION__);
        delete theWebGUI;
        delete theFSM;
        delete theHW;
}

void XdaqDccAccess::Default(xgi::Input* in, xgi::Output* out) throw(xgi::exception::Exception) 
{
  std::string name = in->getenv("PATH_INFO");
//   std::cout <<"XdaqDccAccess::Default called with name: " << name <<endl;
  if (name=="") name="Default";
  static_cast<xgi::MethodSignature*>(theWebGUI->getMethod(name))->invoke(in,out);
}

xoap::MessageReference XdaqDccAccess::fireEvent(xoap::MessageReference msg) throw(xoap::exception::Exception) 
{
	xoap::SOAPPart part = msg->getSOAPPart();
	xoap::SOAPEnvelope env = part.getEnvelope();
	xoap::SOAPBody body = env.getBody();
	DOMNode* node = body.getDOMNode();
	DOMNodeList* bodyList = node->getChildNodes();
	for(unsigned int i = 0; i < bodyList->getLength(); i++) {
	  DOMNode* command = bodyList->item(i);
	  if(command->getNodeType() == DOMNode::ELEMENT_NODE) {

	    std::string commandName = xoap::XMLCh2String(command->getLocalName());
            cout <<" fireEvent notified with command: " << commandName << endl;

            try {
	      toolbox::Event::Reference e(new toolbox::Event(commandName,this));
              theFSM->executeCommand(commandName);
	    }
	    catch(toolbox::fsm::exception::Exception & e)	{
	      XCEPT_RETHROW(xoap::exception::Exception, "invalid command", e);
	    }

            xoap::MessageReference reply = xoap::createMessage();
            xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
            xoap::SOAPName responseName = envelope.createName("response", "xdaq", XDAQ_NS_URI);
            xoap::SOAPBodyElement responseElement = envelope.getBody().addBodyElement( responseName );
            xoap::SOAPName stateName =  envelope.createName( "state", "xdaq", XDAQ_NS_URI);
            xoap::SOAPElement stateElement = responseElement.addChildElement( stateName );
            xoap::SOAPName attributeName = envelope.createName( "stateName", "xdaq", XDAQ_NS_URI);
            stateElement.addAttribute(attributeName, theFSM->stateName());
            return reply;
          }
	}

	XCEPT_RAISE(xcept::Exception, "command not found");
}

xoap::MessageReference XdaqDccAccess::reset(xoap::MessageReference msg) throw(xoap::exception::Exception)
{
  LOG4CPLUS_INFO(getApplicationLogger(), "A state before reset is: " << theFSM->stateName().toString() );
  theFSM->reset();
  

  xoap::MessageReference reply = xoap::createMessage();
  xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
  xoap::SOAPName responseName = envelope.createName("response", "xdaq", XDAQ_NS_URI);
  xoap::SOAPBodyElement responseElement = envelope.getBody().addBodyElement( responseName );
  xoap::SOAPName stateName =  envelope.createName( "state", "xdaq", XDAQ_NS_URI);
  xoap::SOAPElement stateElement = responseElement.addChildElement( stateName );
  xoap::SOAPName attributeName = envelope.createName( "stateName", "xdaq", XDAQ_NS_URI);
  stateElement.addAttribute(attributeName, theFSM->stateName());

             LOG4CPLUS_INFO(getApplicationLogger(), "New state after reset is: " << theFSM->stateName().toString());

  return reply;
}


/*

void XdaqDccAccess::TestTTSAction (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception) {
	LOG4CPLUS_INFO (getApplicationLogger(), e->type()
			<< " ttsTestFedId = " << ttsTestFedId.toString()
			<< " ttsTestMode = " << ttsTestMode.toString()
			<< " ttsTestPattern = " << ttsTestPattern.toString()
			<< " ttsTestSequenceRepeat = " << ttsTestSequenceRepeat.toString());
	if((int)ttsTestFedId<790||(int)ttsTestFedId>795){
		stringstream ss;
		ss << "Invalid FED id " << ttsTestFedId.toString();
		XCEPT_RAISE(toolbox::fsm::exception::Exception, ss.str());
	}
	else {
		bool forceOnCCS = true; // tylko na CCS
		string mode = ttsTestMode.toString();
		transform(mode.begin(),mode.end(),mode.begin(),(int(*)(int)) toupper);
		if(mode=="PATTERN"){
			forceTTS((unsigned int)ttsTestPattern, forceOnCCS);
		}
		else if(mode=="CYCLE"){
			for(int iCycle=0; iCycle<(int)ttsTestSequenceRepeat; iCycle++){
				for(unsigned int iStatus=0; iStatus<16; iStatus++){
					forceTTS(iStatus, forceOnCCS);
					//sleep((unsigned int)1); //sleep 1ms (frequency not exceed 5MHz)
				}
			}
		}
		else{
			stringstream ss;
			ss << "Invalid TTS test mode " << ttsTestMode.toString();
			XCEPT_RAISE(toolbox::fsm::exception::Exception, ss.str());
		}
	}
}
*/
