#!/bin/bash
mvn clean
rm -rfv ~/opt/tomcat_masking/webapps/iiaccess*
mvn package
cp -av target/iiaccess.war ~/opt/tomcat_masking/webapps/
mvn install