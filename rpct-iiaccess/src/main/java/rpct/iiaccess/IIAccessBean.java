package rpct.iiaccess;

import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.FacesException;
import javax.faces.convert.Converter;
import javax.faces.convert.NumberConverter;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.xml.rpc.ServiceException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.myfaces.trinidad.component.core.data.CoreTreeTable;
import org.apache.myfaces.trinidad.model.ChildPropertyTreeModel;
import org.apache.myfaces.trinidad.model.TreeModel;

import rpct.xdaq.XdaqApplicationInfo;
import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.AccessType;
import rpct.xdaqaccess.Board;
import rpct.xdaqaccess.Crate;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.DeviceDesc;
import rpct.xdaqaccess.DeviceItemArea;
import rpct.xdaqaccess.DeviceItemBits;
import rpct.xdaqaccess.DeviceItemType;
import rpct.xdaqaccess.DeviceItemVector;
import rpct.xdaqaccess.DeviceItemWord;
import rpct.xdaqaccess.HardwareItem;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.axis.DeviceImplAxis;

/**
 * Created on 2005-05-06
 *
 * @author Michal Pietrusinski
 * @version $Id: IIAccessBean.java,v 1.1 2008/06/19 18:51:13 tb Exp $
 */
public class IIAccessBean {
    private static Log LOG = LogFactory.getLog(IIAccessBean.class);
        
    private Crate[] crates; 
    private CoreTreeTable devicesTree;
    private TreeModel devicesTreeModel;
    private Map<String, Converter> convertersMap = new HashMap<String, Converter>();
    private Converter converter;
    private List<SelectItem> converterSelectItems = new ArrayList<SelectItem>();  
    
    public interface TreeItemInfo {
        String getName();
        String getType();
        Integer getWidth();
        Binary getReadValue();
        Binary getWriteValue();
        void setWriteValue(Binary writeValue);
        boolean isReadable();
        boolean isWriteable();
        public void read();
        public void write();
    }
    
    
    public static class HardwareItemInfo implements TreeItemInfo {   
        private HardwareItem hardwareItem;
        private String name;
        
        public HardwareItemInfo(HardwareItem hardwareItem) {
            this.hardwareItem = hardwareItem;
            if (hardwareItem == null) {
                this.name = "System";
            }
            else if (hardwareItem instanceof Board) {
                Board board = (Board) hardwareItem;
                this.name = board.getName() + " (" + hardwareItem.getId() +
                    ", " + board.getPosition() + ")";
            }
            else {
                this.name = hardwareItem.getName() + " (" + hardwareItem.getId() + ")";
            }
        }
        
        private HardwareItem getHardwareItem() {
            return hardwareItem;
        }    
        
        public String getName() {
            return name;
        }
        public String getType() { 
            if (hardwareItem == null) {
                return "";
            }
            return hardwareItem.getType();
        }
        public boolean isReadable() {
            return hardwareItem != null;
        }
        public boolean isWriteable() {
            return false;
        }
        public Integer getWidth() {
            return null;
        }
        public Binary getReadValue() {
            return null;
        }
        public Binary getWriteValue() {
            return null;
        }
        public void setWriteValue(Binary writeValue) {            
        }

        public void read() {           
        }

        public void write() {         
        }

    }    
    
    public static class DeviceItemData implements TreeItemInfo  {
        private Device device;
        private DeviceDesc.ItemDesc itemDesc;
        private int index; // used for indexed device items
        private Binary readValue;
        private Binary writeValue;
        private String name;
        public DeviceItemData(Device device, DeviceDesc.ItemDesc itemDesc, int index) {
            this.device = device;
            this.itemDesc = itemDesc;
            this.index = index;  
            StringBuilder nameBuilder = new StringBuilder(itemDesc.getName());
            
            if (itemDesc.getNumber() > 1 || itemDesc.getType().equals(DeviceItemType.AREA)) { 
                if (index == -1) {
                    nameBuilder.append("[]");                    
                }
                else {
                    nameBuilder.append('[').append(index).append(']');
                }
            }
            this.name = nameBuilder.toString();
        }
        /*public DeviceItem getDeviceItem() {
            return deviceItem;
        }*/
        public int getIndex() {
            return index;
        }
        public Binary getReadValue() {
            return readValue;
        }
        public void setReadValue(Binary readValue) {
            this.readValue = readValue;
            if (writeValue == null) {
                //writeValue = new Binary(this.readValue.getBytes());
                writeValue = readValue;
            }
        }
        public Binary getWriteValue() {
            return writeValue;
        }
        public void setWriteValue(Binary writeValue) {
            this.writeValue = writeValue;
        }
        public String getName() {
            return name;
        }    
        public String getType() {
            return itemDesc.getType().toString();
        }   
        public boolean isReadable() { 
            if (itemDesc.getType().equals(DeviceItemType.AREA) && index == -1) {
                return false;
            }
            AccessType accessType = itemDesc.getAccessType();
            return accessType == AccessType.READ || accessType == AccessType.BOTH;
        } 
        public boolean isNestedRead() {
            if (itemDesc.getType().equals(DeviceItemType.WORD) && index == -1) {
                return true;
            }
            return false;
        }
        public boolean isWriteable() { 
            if ((itemDesc.getType().equals(DeviceItemType.AREA)
                    || itemDesc.getType().equals(DeviceItemType.WORD)) 
                    && index == -1) {
                return false;
            }
            AccessType accessType = itemDesc.getAccessType(); 
            return accessType == AccessType.WRITE || accessType == AccessType.BOTH;
        }
        public void write() {
            if (!isWriteable()) {
                return;
            }
            if (writeValue == null) {
                return;
            }
            try {
                if (itemDesc.getType() == DeviceItemType.WORD) {
                    DeviceItemWord word = (DeviceItemWord) device.getItem(itemDesc);
                    word.writeBinary(index, writeValue);
                }
                else if (itemDesc.getType() == DeviceItemType.BITS) {
                    DeviceItemBits bits = (DeviceItemBits) device.getItem(itemDesc);
                    bits.writeBinary(index, writeValue);
                }
                else if (itemDesc.getType() == DeviceItemType.VECT) {
                    DeviceItemVector vector = (DeviceItemVector) device.getItem(itemDesc);
                    vector.writeBinary(writeValue);
                }
                else if (itemDesc.getType() == DeviceItemType.AREA) {
                    DeviceItemArea area = (DeviceItemArea) device.getItem(itemDesc);
                    area.writeAreaBinary(index, new Binary[] { writeValue } );
                }
            } catch (RemoteException e) {
                throw new FacesException(e);
            } catch (ServiceException e) {
                throw new FacesException(e);
            }
            
        }
        public void read() {
            if (!isReadable()) {
                return;
            }
            if (index == -1) {
            	return;            	
            }
            try {
                if (itemDesc.getType() == DeviceItemType.WORD) {
                    DeviceItemWord word = (DeviceItemWord) device.getItem(itemDesc);
                    setReadValue(word.readBinary(index));
                }
                else if (itemDesc.getType() == DeviceItemType.BITS) {
                    DeviceItemBits bits = (DeviceItemBits) device.getItem(itemDesc);
                    setReadValue(bits.readBinary(index));
                }
                else if (itemDesc.getType() == DeviceItemType.VECT) {
                    DeviceItemVector vector = (DeviceItemVector) device.getItem(itemDesc);
                    setReadValue(vector.readBinary());
                }
                else if (itemDesc.getType() == DeviceItemType.AREA) {
                    DeviceItemArea area = (DeviceItemArea) device.getItem(itemDesc);
                    setReadValue(area.readAreaBinary(index, 1)[0]);
                }
            } catch (RemoteException e) {
                throw new FacesException(e);
            } catch (ServiceException e) {
                throw new FacesException(e);
            }            
        }
        public Integer getWidth() {
            return itemDesc.getWidth();
        }
    }
    
    public static class TreeItem {
        private TreeItemInfo treeItemInfo;
        private List<TreeItem> items = null;        
        
        public TreeItem(TreeItemInfo treeItemInfo) {
            this.treeItemInfo = treeItemInfo;
        }

        public String getName() {
            return treeItemInfo.getName();
        }

        public Binary getReadValue() {
            return treeItemInfo.getReadValue();
        }

        public String getType() {
            return treeItemInfo.getType();
        }

        public Integer getWidth() {
            return treeItemInfo.getWidth();
        }

        public Binary getWriteValue() {
            return treeItemInfo.getWriteValue();
        }

        public boolean isReadable() {
            return treeItemInfo.isReadable();
        }

        public boolean isWriteable() {
            return treeItemInfo.isWriteable();
        }

        public void setWriteValue(Binary writeValue) {
            treeItemInfo.setWriteValue(writeValue);
        }        

        private static void addNodes(TreeItem parent, Device device, DeviceDesc.ItemDesc itemDesc) {
            if (itemDesc.getType() == DeviceItemType.AREA
                    || (itemDesc.getType() == DeviceItemType.WORD && itemDesc.getNumber() > 1)) {
                TreeItem groupNode = new TreeItem(new DeviceItemData(device, itemDesc, -1)); 
                parent.addItem(groupNode);
                for (int i = 0; i < itemDesc.getNumber(); i++) {
                    groupNode.addItem(new TreeItem(new DeviceItemData(device, itemDesc, i)));                
                }
            }
            else if (itemDesc.getNumber() > 1) {
                for (int i = 0; i < itemDesc.getNumber(); i++) {
                    parent.addItem(new TreeItem(new DeviceItemData(device, itemDesc, i)));
                }            
            }
            else {
                parent.addItem(new TreeItem(new DeviceItemData(device, itemDesc, 0)));
            }
        }

        public List<TreeItem> getItems() {            
            
            if (items == null) {
                if (treeItemInfo instanceof HardwareItemInfo) {
                    HardwareItemInfo hii = (HardwareItemInfo) treeItemInfo;
                    HardwareItem hi =  hii.getHardwareItem();
                    if (hi instanceof Crate) {
                        Crate crate = (Crate) hi;
                        for (Board board : crate.getBoards()) {
                            TreeItem boardNode = new TreeItem(new HardwareItemInfo(board));
                            addItem(boardNode);  
                        }                        
                    }
                    else if (hi instanceof Board) {
                        Board board = (Board) hi;                
                        for (Device device : board.getDevices()) {
                            TreeItem deviceNode = new TreeItem(new HardwareItemInfo(device));
                            addItem(deviceNode);
                        }    
                    }
                    else if (hi instanceof Device) {
                        Map<Integer, TreeItem> vectorNodesMap = new HashMap<Integer, TreeItem>();
                        Device device = (Device) hi;  
                        if (device.getDeviceDesc().getItems() == null) {
                            // force loading device items
                            ((DeviceImplAxis) device).loadItems();
                        }
                        for (DeviceDesc.ItemDesc item : device.getDeviceDesc().getItems()) {
                            DeviceItemType itemType = item.getType();
                            if (itemType == DeviceItemType.WORD) {
                                addNodes(this, device, item);
                            }
                            else if (itemType == DeviceItemType.AREA) {
                                addNodes(this, device, item);
                            }
                            else if (itemType == DeviceItemType.VECT) {
                                TreeItem node = new TreeItem(new DeviceItemData(device, item, 0));
                                addItem(node);
                                vectorNodesMap.put(item.getId(), node);
                                //for (DeviceItemBits bitsItem : vectorItem.getBits()) {
                                //    addNodes(vectorNode, device, bitsItem);
                                //}                                                             
                            }    
                            else if (itemType == DeviceItemType.BITS) {
                                TreeItem vectorNode = vectorNodesMap.get(item.getParent().getId());
                                if (vectorNode == null) {
                                    throw new FacesException("parent vector node not found");
                                };
                                addNodes(vectorNode, device, item);                     
                            }                                                       
                        }
                    }
                }
            }
            return items;
        }    
        
        public void addItem(TreeItem item) {
            if (items == null) {
                items = new ArrayList<TreeItem>();
            }
            items.add(item);
        }
        
        public void read() {
            if (isNestedRead()) {
                for (TreeItem item : items) {
                    item.read();
                }
            }
            else {
                treeItemInfo.read();
            }
        }
        
        public void write() {
            treeItemInfo.write();
        }
        
        public boolean isNestedRead() {
        	if (!isReadable()) {
        		return false;
        	}
        	if (treeItemInfo instanceof HardwareItemInfo) {
        		return true;
        	}
        	if (treeItemInfo instanceof DeviceItemData) {
        		DeviceItemData deviceItemData = (DeviceItemData) treeItemInfo;
        		return deviceItemData.isNestedRead();
        	}
        	return false;
        }
        
        /*public boolean isNestedWrite() {
            return (treeItemInfo instanceof DeviceInfo) && isWriteable();
        }*/
        
        public String getNodeType() {
          return "folder";
        }
    }

    public IIAccessBean() throws MalformedURLException, UnknownHostException, RemoteException, ServiceException {        
        
        converter = new HexConverter();
        convertersMap.put(HexConverter.CONVERTER_ID, converter);
        converterSelectItems.add(new SelectItem(HexConverter.CONVERTER_ID, "hex"));
        
        NumberConverter numberConverter = new NumberConverter();
        numberConverter.setType("number");        
        convertersMap.put(NumberConverter.CONVERTER_ID, numberConverter);        
        converterSelectItems.add(new SelectItem(NumberConverter.CONVERTER_ID, "dec"));
    }
    
    private void loadCrates() {    
        List<Crate> crateList = new ArrayList<Crate>();
    	
    	HardwareRegistry hardwareRegistry = new HardwareRegistry();
        for (XdaqApplicationInfo appInfo : ApplicationResources.getIIAccessApplications()) {
            System.out.println("IIAccessBean: connecting to " + appInfo.getUrl() + "," + 
                    appInfo.getClassName() + "," + appInfo.getInstance());
            hardwareRegistry.registerXdaqApplication(appInfo);
            try {
                Collections.addAll(crateList, 
                		hardwareRegistry.getXdaqApplicationAccess(appInfo).getCrates());
            }
            catch (Exception e) {
                LOG.warn("Error during loding crates", e);
            }
        }
        
        crates = crateList.toArray(new Crate[] {});
        TreeItem root = new TreeItem(new HardwareItemInfo(null));
        for (Crate crate : crates) {
            
            TreeItem crateNode = new TreeItem(new HardwareItemInfo(crate));
            root.addItem(crateNode);
            
            /*for (Board board : crate.getBoards()) {
                Device[] devices = board.getDevices();
                TreeItem boardNode = new TreeItem(new HardwareItemInfo(board));
                crateNode.addItem(boardNode);                
                for (Device device : devices) {
                    TreeItem deviceNode = new TreeItem(new HardwareItemInfo(device));
                    boardNode.addItem(deviceNode);
                    for (DeviceItem item : device.getItems()) {
                        DeviceItemType itemType = item.getInfo().getType();
                        if (itemType == DeviceItemType.WORD) {
                            addNodes(deviceNode, item);
                        }
                        else if (itemType == DeviceItemType.AREA) {
                            addNodes(deviceNode, item);
                        }
                        else if (itemType == DeviceItemType.VECT) {
                            DeviceItemVector vectorItem = (DeviceItemVector) item;
                            TreeItem vectorNode = new TreeItem(new DeviceItemData(item, 0));
                            deviceNode.addItem(vectorNode);
                            for (DeviceItemBits bitsItem : vectorItem.getBits()) {
                                addNodes(vectorNode, bitsItem);
                            }                                                             
                        }                                                       
                    }
                }    
            }*/
        }

        try {
            devicesTreeModel = new ChildPropertyTreeModel(root, "items");
        } catch (Exception e) {
            throw new FacesException(e);
        }
    }
    
    
    public void getCrates(ActionEvent event) {  
        loadCrates();
    }


    public void readTree(ActionEvent event) {  
        TreeItem treeItem = (TreeItem) devicesTree.getRowData();
        treeItem.read();
    }
    
    public void writeTree(ActionEvent event) {  
        TreeItem treeItem = (TreeItem) devicesTree.getRowData();
        treeItem.write();
    }
    
    public Converter getConverter() {
        return converter;
    }
    
    public void setConverter(Converter converter) {
        this.converter = converter;
    }
    
    public String getConverterId() {
        if (converter instanceof HexConverter) {
            return HexConverter.CONVERTER_ID;
        }
        return NumberConverter.CONVERTER_ID;
    }
    
    public void setConverterId(String converterId) {
        converter = convertersMap.get(converterId);
    }
    
    
    public List<SelectItem> getConverterSelectItems() {
        return converterSelectItems;
    }

    public TreeModel getDevicesTreeModel() {
        if (crates == null) {
            loadCrates();
        }
        return devicesTreeModel;
    }

    public CoreTreeTable getDevicesTree() {
        return devicesTree;
    }

    public void setDevicesTree(CoreTreeTable devicesTree) {
        this.devicesTree = devicesTree;
    }    
}
