#ifndef XmlDevicesSettings_H_
#define XmlDevicesSettings_H_
#include <string>
#include <log4cplus/logger.h>

#include "rpct/std/TXMLObjectMgr.h"

#include "rpct/devices/ConfigurationSetImpl.h"

#include <xercesc/sax/HandlerBase.hpp>   
#include <xercesc/sax/AttributeList.hpp>
//public TXMLObjectMgr,

class XmlDevicesSettings :  public xercesc::HandlerBase , public rpct::ConfigurationSetImpl {
public:
	static void setupLogger(std::string namePrefix);
    XmlDevicesSettings();
    virtual ~XmlDevicesSettings();
	
    void readFile(std::string filename);
	
    void adddefaultDeviceSettings();
	
private:
    static log4cplus::Logger logger;

    bool sawErrors;
    std::string filename;
    
    virtual void startElement(const XMLCh* const name, 
                            xercesc::AttributeList& attributes);
    virtual void endElement(const XMLCh* const name);
    std::string getAttribute(const char* attrName, xercesc::AttributeList& attributes);
        
    void warning(const xercesc::SAXParseException& exception);
    void error(const xercesc::SAXParseException& exception);
    void fatalError(const xercesc::SAXParseException& exception);
    void resetErrors();
    
    
    bool getHexAttribute(const char* attrName, xercesc::AttributeList& attributes, unsigned int& value);
    bool getDecAttribute(const char* attrName, xercesc::AttributeList& attributes, unsigned int& value);    
};

#endif /*XmlDevicesSettings_H_*/
