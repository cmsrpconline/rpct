#ifndef SCSTATISTICDIAGMANAGER_H_
#define SCSTATISTICDIAGMANAGER_H_

#include "rpct/hardwareTests/StatisticDiagManagerBase.h"
#include "rpct/devices/SorterCrate.h"

#include "TVectorT.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TLegendEntry.h"
#include "TLegend.h"
#include "TPaveText.h"
#include "TH2.h"

#include <vector>

namespace rpct {

class SCStatisticDiagManager : public StatisticDiagManagerBase {
private:
    class HSHistogramRecord: public HistogramRecordBase {
    public:
        HSHistogramRecord(IDiagCtrl* diagCtrl, HistoMgrVector& histoManagers): HistogramRecordBase(diagCtrl, histoManagers) {
            for(unsigned int i = 0; i < binsCnt; i++) {
                branchData_.binsHSMuonCounters_[i] = 10;
            }
        }
        virtual ~HSHistogramRecord() {};

        static const unsigned int binsCnt = 32; //HalfSort::RATE_DIAG_DATA_SIZE;
        struct BranchData {
            uint64_t timerValue_;
            unsigned int startTime_; //time_t start of histograming time slice
            unsigned int stopTime_; //time_t start of histograming time slice
            unsigned int binsHSMuonCounters_[binsCnt]; //32
        } ;

    private:
        BranchData branchData_;
    public:
        BranchData& getBranchData() {
            return branchData_;
        }

        virtual void createBranch(TTree& tree, std::string& name) {
           tree.Branch(name.c_str(), &(branchData_.timerValue_),
                    "timerValue/l:startTime_/i:stopTime_/i:binsHSMuonCounters[32]/i");
        }

        virtual uint64_t getTimerValue() {
            return branchData_.timerValue_;
        }
        virtual void setTimerValue(uint64_t timerValue) {
            branchData_.timerValue_ = timerValue;
        }

        virtual unsigned int getStartTime() {
            return branchData_.startTime_;
        }
        virtual void setStartTime(unsigned int startTime) {
            branchData_.startTime_ = startTime;
        }

        virtual unsigned int getStopTime() {
            return branchData_.stopTime_;
        }
        virtual void setStopTime(unsigned int stopTime) {
            branchData_.stopTime_ = stopTime;
        }
    };

    class FSHistogramRecord: public HistogramRecordBase {
        public:
            FSHistogramRecord(IDiagCtrl* diagCtrl, HistoMgrVector& histoManagers): HistogramRecordBase(diagCtrl, histoManagers) {
                for(unsigned int i = 0; i < binsCnt * histoCnt; i++) {
                    branchData_.binsFSMuonCounters[i] = 0;
                }
            };

            virtual ~FSHistogramRecord() {};

            static const unsigned int binsCnt = 512; //GbSort::RATE_DIAG_DATA_SIZE;
            static const unsigned int histoCnt = 8;
            struct BranchData {
                uint64_t timerValue_;
                unsigned int startTime_; //time_t start of histograming time slice
                unsigned int stopTime_; //time_t start of histograming time slice
                unsigned int binsFSMuonCounters[binsCnt * histoCnt]; //512 * 8, data from all histograms in one table
            };
            std::vector<uint64_t> fsMuonCountersInteg_;
        private:
            BranchData branchData_;
        public:
            BranchData& getBranchData() {
                return branchData_;
            }

            virtual void createBranch(TTree& tree, std::string& name) {
                tree.Branch(name.c_str(), &(branchData_.timerValue_),
                        "timerValue/l:startTime_/i:stopTime_/i:binsFSMuonCounters[4096]/i");;
            }

            virtual uint64_t getTimerValue() {
                return branchData_.timerValue_;
            }
            virtual void setTimerValue(uint64_t timerValue) {
                branchData_.timerValue_ = timerValue;
            }

            virtual unsigned int getStartTime() {
                return branchData_.startTime_;
            }
            virtual void setStartTime(unsigned int startTime) {
                branchData_.startTime_ = startTime;
            }

            virtual unsigned int getStopTime() {
                return branchData_.stopTime_;
            }
            virtual void setStopTime(unsigned int stopTime) {
                branchData_.stopTime_ = stopTime;
            }
        };

/*    class TBItems {
    public:
        TriggerBoard* tb_;
        //RMB
        RMBHistogramRecord* rmbHistRecord_;
        TH1D* rmbHist_;

        //GBS
        GBSHistogramRecord* gbsHistRecord_;
    };*/
private:
    SorterCrate* sc_;
    //std::vector<TBItems> tbItemsVec_;

    std::vector<HSHistogramRecord*> hsHistogramRecord_;
    //HSHistogramRecord* hsHistogramRecord0_;
    //HSHistogramRecord* hsHistogramRecord1_;
    FSHistogramRecord* fsHistogramRecord_;

    TH1D* hMaxRateHist_;
    TH1D* hsMeanRateHist_;

    std::vector<TGraph*> rateGraphs_;
    std::vector<TLegendEntry*> rateGraphLegendEntries_;

    std::vector<TH1I*> ptCodeHistos_;
    std::vector<TH1I*> qualityHistos_;

    //std::vector<double> integratedRateVec_;
    TLegend fsBarrelRateLegend1_;
    TLegend fsEndcapRateLegend1_;
    TLegend fsBarrelRateLegend2_;
    TLegend fsEndcapRateLegend2_;

    TPaveText hsTitlePaveText_;
    TLegend hsLegend1_;
    TLegend hsLegend2_;

    TPaveText barrelPaveText_;
    TPaveText endcapPaveText_;

    TCanvas* fsbRateCanvas_;
public:
    SCStatisticDiagManager(std::string name, std::string picturesDir, SorterCrate* sc);
    virtual ~SCStatisticDiagManager();

    virtual void createBranches(TTree& tree);

    //virtual void resetAndConfigHistos(uint64_t countersLimit, IDiagCtrl::TTriggerType triggerType, uint32_t triggerDelay);
    //virtual void start();
    //virtual void stop();
    virtual bool readout(bool startAfterReadout);
    virtual Monitorable::MonitorStatusList& analyse();

    virtual void initialize(time_t startTime);
    //virtual void finalize();

    virtual std::string getHtml();
};

}
#endif /*SCStatisticDiagManager_H_*/
