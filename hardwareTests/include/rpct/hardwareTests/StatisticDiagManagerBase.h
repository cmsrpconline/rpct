#ifndef STATISTICDIAGMANAGER_H_
#define STATISTICDIAGMANAGER_H_

#include "rpct/hardwareTests/IStatisticDiagManager.h"

#include "rpct/diag/TDiagCtrl.h"
#include "rpct/diag/TIIHistoMgr.h"
#include "rpct/diag/IDiagnosable.h"
#include "rpct/diag/IDiagCtrl.h"

#include "TH1.h"
#include "TFile.h"
#include "TObjArray.h"
#include "TTree.h"
#include "TCanvas.h"

#include <sys/time.h>
#include <map>

namespace rpct {

class HistogramRecordBase {
protected:
    time_t startTimeBuf_; //Buffer for start of histograming time slice
    uint64_t timerValuePrev_; //previous
    uint64_t timerValueIntegrated_;

    IDiagCtrl* diagCtrl_;
    HistoMgrVector& histoManagers_;
public:
    HistogramRecordBase(IDiagCtrl* diagCtrl, HistoMgrVector& histoManagers): diagCtrl_(diagCtrl),  histoManagers_(histoManagers) {}

    virtual ~HistogramRecordBase() {};

    IDiagCtrl* getDiagCtrl() {
        return diagCtrl_;
    }
    HistoMgrVector& getHistoManagers() {
        return histoManagers_;
    }

    time_t getStartTimeBuf() {
        return startTimeBuf_;
    }
    void setStartTimeBuf(time_t startTimeBuf) {
        startTimeBuf_ = startTimeBuf;
    }

    //void* getBranchDataAddress() = 0;
    virtual void createBranch(TTree& tree, std::string& name) = 0;

    virtual uint64_t getTimerValue() = 0;
    virtual void setTimerValue(uint64_t timerValue) = 0;

    virtual unsigned int getStartTime() = 0; //time_t start of histograming time slice
    virtual void setStartTime(unsigned int startTime) = 0;

    virtual unsigned int getStopTime() = 0; //time_t start of histograming time slice
    virtual void setStopTime(unsigned int stopTime) = 0;

    virtual uint64_t getTimerValuePrev() {//previous
        return timerValuePrev_;
    }
    virtual void setTimerValuePrev(uint64_t timerValuePrev) {//previous
        timerValuePrev_ = timerValuePrev;
    }

    virtual uint64_t getTimerValueIntegrated() {
        return timerValueIntegrated_;
    }
    virtual void setTimerValueIntegrated(uint64_t timerValueIntegrated) {
        timerValueIntegrated_ = timerValueIntegrated;
    }
};

class StatisticDiagManagerBase : public IStatisticDiagManager {
protected:
    std::string name_;

    std::string picturesDir_;

    //IDiagnosable::TDiagCtrlVector diagCtrls_;
    //HistoMgrVector histoManagers_;

    std::vector<HistogramRecordBase*> histogramRecords_;

    Monitorable::MonitorStatusList monitorStatusList_;

    time_t startTime_; //start time of the monitoring job

    std::vector<std::string> picturesDirs_;
    std::vector<std::string> picturesNames_;

    static log4cplus::Logger logger;

    unsigned int iteration_;

    std::vector<TCanvas*> canvases_;

    bool logScale_;
public:
    static void setupLogger(std::string namePrefix);
    StatisticDiagManagerBase(std::string name, std::string picturesDir) {
        name_ = name;
        picturesDir_ = picturesDir;
        iteration_ = 0;;
    }

    virtual ~StatisticDiagManagerBase();

    virtual void createBranches(TTree& tree) = 0;

    virtual void resetAndConfigHistos();
    virtual void start();
    virtual void stop();
    virtual bool readout(bool startAfterReadout);
    virtual Monitorable::MonitorStatusList& analyse() = 0;

    virtual void initialize(time_t startTime);
    virtual void finalize();

    virtual bool countingEnded(uint64_t coutingPeriod);


    // this feature is not implemented by default
    virtual void setStopHistoPtr(volatile bool *ptr) {};

    virtual std::string getHtml() = 0;

    //log scale ofor selected
    virtual void setLogScale(bool logScale) {
    	logScale_ = logScale;
    }
};

//typedef std::map<std::string, StatisticDiagManagerBase*> StatisticDiagManagersMap;
}
#endif /*STATISTICDIAGMANAGER_H_*/
