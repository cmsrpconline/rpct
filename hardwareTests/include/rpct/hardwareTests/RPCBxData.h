#ifndef RPCTBXDATA_H_
#define RPCTBXDATA_H_

#include "rpct/devices/RPCHardwareMuon.h"
#include "rpct/devices/LmuxBxData.h"
#include "rpct/devices/cmssw/RPCConst.h"
#include "rpct/std/TException.h"
#include <log4cplus/logger.h>

#include <map>
#include <vector>
#include <set>
#include <boost/shared_ptr.hpp>

typedef std::vector<boost::shared_ptr<RPCHardwareMuon> > MuonVector;

/*typedef boost::shared_ptr<LmuxBxData> LmuxBxDataPtr;
typedef std::vector<LmuxBxDataPtr> LmuxBxDataPtrVec;*/

/*struct LmuxBxDataPtrMore : public std::less<LmuxBxDataPtr> {
    bool operator() (const LmuxBxDataPtr& dataL,
                     const LmuxBxDataPtr& dataR) const {
        LmuxBxData::More more;             	
    	return more(*(dataL.get()), *(dataR.get()));                 
    }
};*/

class RPCBxData {
private:
	static log4cplus::Logger logger;
public:
	static void setupLogger(std::string namePrefix);
	typedef std::set<rpct::LmuxBxData, rpct::LmuxBxData::More > LmuxBxDataSet;
	typedef boost::shared_ptr<LmuxBxDataSet> LmuxBxDataSetPtr;

	typedef std::map<unsigned int, LmuxBxDataSetPtr> OptLinksData; //[optLinkNum]
	
	class LbData {
	private:
		unsigned int id_;
		
		unsigned int pulserData_;
		
		LmuxBxDataSetPtr coderData_;
		
	public:
		LbData(unsigned int id): id_(id), pulserData_(0), coderData_(new LmuxBxDataSet() )  {
		}
		
		unsigned int getPulserData() {
			return pulserData_;
		}	
		
		void setPulserData(unsigned int pulserData) {
			pulserData_ = pulserData;
		}		
			
		LmuxBxDataSetPtr getCoderData() {
			return coderData_;
		}		
	}; 
	
    typedef boost::shared_ptr<LbData> LbDataPtr; 
    typedef std::map<int, LbDataPtr> LbDataMap;	

    typedef boost::shared_ptr<MuonVector> MuonVecPtr; 
    
    class SimpleSourceMuons {
    private:        
        MuonVecPtr muons_;
    public:
        SimpleSourceMuons(int maxMuons): muons_(new MuonVector(maxMuons)) {
        	 ;
        }
        
        SimpleSourceMuons() {
        }
        
        virtual ~SimpleSourceMuons() {
        }
        
        
        MuonVector& getMuons() {
            return *muons_;
        } 
        
        MuonVecPtr getMuonVecPtr() {
        	return muons_;
        }
    };
    
    class PacData : public SimpleSourceMuons {
    private:
    	int pacNum_;
    public:
        PacData(int pacNum) : SimpleSourceMuons(12), pacNum_(pacNum)  {
        }
        
        int getPacNum() {
            return pacNum_;
        }
    };
    
    typedef boost::shared_ptr<PacData> PacDataPtr; 
    typedef std::map<int, PacDataPtr> PacDataMap;
    
    class TbData {
    private:    
        int tbNum_;
        OptLinksData optLinksData_;
        PacDataMap pacDataMap_; //pac output muons
        MuonVecPtr gbSortMuons_; //gbSort output muons
    public:
        TbData(int tbNum) : tbNum_(tbNum), gbSortMuons_(new MuonVector(RPCConst::m_GBETA_OUT_MUONS_CNT)) {
        }
        
        int getTbNum() {
            return tbNum_;
        }
              
        OptLinksData& getOptLinksDataMap() {
        	return optLinksData_;
        }
        
        
        LmuxBxDataSetPtr addOptLinkData(unsigned int optLinkNum) {
        	OptLinksData::iterator optDataIt = optLinksData_.find(optLinkNum);
        	if(optDataIt == optLinksData_.end()) {
	            LmuxBxDataSetPtr p(new LmuxBxDataSet());
	            optLinksData_.insert(OptLinksData::value_type(optLinkNum, p));
	            return p;
        	}
        	else {
        		return (optDataIt->second);
        	}
        }
        
/*       	void addOptLinkData(unsigned int optLinkNum, const rpct::LmuxBxData& lmuxBxData) {
        	if((optLinksData_[optLinkNum]->insert(lmuxBxData)).second == false) {
        		LOG4CPLUS_INFO(logger, "RPCBxData::TbData::addOptLinkData: the same lmuxBxData were already added to the link, the data are skipped");
        		//throw TException("RPCBxData::TbData::addOptLinkData: the same lmuxBxData were already added to the link ");
        	}		
        }*/
/*        LmuxBxDataSetPtr getOptLinkData(unsigned int optLinkNum) {
        	if(optLinksData_.find(optLinkNum) == optLinksData_.end() ) {
        		LmuxBxDataSetPtr = 
        	}
        	return optLinksData_[optLinkNum];
        }*/
        
        PacDataMap& getPacData() {
            return pacDataMap_;
        }
        
/*        PacData& getPacData(int pacNum) {
             PacDataMap::iterator pacIt = pacDataMap_.find(pacNum);
             if(pacIt != pacDataMap_.end() )
             	return *(pacIt->second());
             else {
             	PacDataPtr p(new PacData(pacNum));
             	return *p;
             }	             
        } chyba jest slisliskie*/
                
        PacDataPtr addPacData(int pacNum) {
        	PacDataMap::iterator pacDataIt = pacDataMap_.find(pacNum);
        	if(pacDataIt == pacDataMap_.end()) {
	            PacDataPtr p(new PacData(pacNum));
	            pacDataMap_.insert(PacDataMap::value_type(pacNum, p));
	            return p;
        	}
        	else {
        		return (pacDataIt->second);
        	}
        }
        
        MuonVector& getGbSortMuons() {
            return *gbSortMuons_;
        }
        
        MuonVecPtr getGbSortMuonsVecPtr() {
            return gbSortMuons_;
        }
    };
    typedef boost::shared_ptr<TbData> TbDataPtr;
    typedef std::map<int, boost::shared_ptr<TbData> > TbDataMap;
    
    class TcData {
    private:
        int tcNum_; //== LogSector
        TbDataMap tbDataMap_;
        MuonVecPtr gbSortMuons_;
    public:
        TcData(int tcNum) : tcNum_(tcNum), gbSortMuons_(new MuonVector(RPCConst::m_TCGB_OUT_MUONS_CNT)) {
        }        
                
        int getTcNum() {
            return tcNum_;
        }
        
        TbDataMap& getTbData() {
            return tbDataMap_;
        }
        
        TbDataPtr addTbData(int tbNum) {
        	TbDataMap::iterator tbDataIt = tbDataMap_.find(tbNum);
	        if(tbDataIt == tbDataMap_.end()) {
	            TbDataPtr t(new TbData(tbNum));
	            tbDataMap_.insert(TbDataMap::value_type(tbNum, t));
	            return t;
	        }
	        else
	        	return (tbDataIt->second);
        }
    
        
        MuonVector& getGbSortMuons() {
            return *gbSortMuons_;
        }
        
        MuonVecPtr getGbSortMuonsVecPtr() {
            return gbSortMuons_;
        }
    };
    typedef boost::shared_ptr<TcData> TcDataPtr;
    typedef std::map<int, TcDataPtr> TcDataMap; //[tcNum_]
    
    typedef std::vector<MuonVecPtr> MuonVecPtrVec;
    
    class HalfSorterData {
   	private:
        int hsbNum_; 
        MuonVecPtrVec gbSortMuons_;
        MuonVecPtr    gbSortMuonsFlat_;
    public:    
        int getHsbNum() {
            return hsbNum_;
        }
        
        HalfSorterData(int hsbNum) : hsbNum_(hsbNum) {
        	MuonVecPtr barrelMuons(new MuonVector(RPCConst::m_FINAL_OUT_MUONS_CNT));
        	gbSortMuons_.push_back(barrelMuons);	
        	MuonVecPtr endcapMuons(new MuonVector(RPCConst::m_FINAL_OUT_MUONS_CNT));
        	gbSortMuons_.push_back(endcapMuons);	
        }
        
         MuonVecPtrVec& getGbSortMuons() {
            return gbSortMuons_;
        }   
        
/*        MuonVecPtrVec getGbSortMuonsVecPtr() {
            return gbSortMuons_;
        } */   
        
        MuonVecPtr getGbSortMuonsPtr() {
        	gbSortMuonsFlat_.reset(new MuonVector(*(gbSortMuons_[0])));
        	gbSortMuonsFlat_->insert(gbSortMuonsFlat_->end(), gbSortMuons_[1]->begin(), gbSortMuons_[1]->end());
            return gbSortMuonsFlat_;
        } 
        
    };    
    typedef boost::shared_ptr<HalfSorterData> HalfSorterDataPtr; 
    typedef std::map<int, HalfSorterDataPtr> HalfSorterDataMap;

    class FinalSorterData {
   	private:
        MuonVecPtrVec gbSortMuons_;
        MuonVecPtr    gbSortMuonsFlat_;
    public:    
        FinalSorterData() {
        	MuonVecPtr barrelMuons(new MuonVector(RPCConst::m_FINAL_OUT_MUONS_CNT));
        	gbSortMuons_.push_back(barrelMuons);	
        	MuonVecPtr endcapMuons(new MuonVector(RPCConst::m_FINAL_OUT_MUONS_CNT));
        	gbSortMuons_.push_back(endcapMuons);	
        }
        
         MuonVecPtrVec& getGbSortMuons() {
            return gbSortMuons_;
        }     
        
        MuonVecPtr getGbSortMuonsPtr() {
        	gbSortMuonsFlat_.reset(new MuonVector(*(gbSortMuons_[0])));
        	gbSortMuonsFlat_->insert(gbSortMuonsFlat_->end(), gbSortMuons_[1]->begin(), gbSortMuons_[1]->end());
            return gbSortMuonsFlat_;
        }   
    };     
    
private:    
	unsigned int bxNum_;	
	LbDataMap lbData_;
    TcDataMap tcData_;
    HalfSorterDataMap halfSorterData_;
    FinalSorterData finalSorterData_;
    
public:
    RPCBxData(unsigned int bxNum) : bxNum_(bxNum) {
    }
    
   	unsigned int getBxNum() {
    	return bxNum_;
    }

	LbDataMap& getLbData() {
		return lbData_;
	}

    LbDataPtr addLbData(int lbId) {
    	LbDataMap::iterator lbDataIt = lbData_.find(lbId);
    	if(lbDataIt == lbData_.end() ) {
        	LbDataPtr t(new LbData(lbId));
        	lbData_.insert(LbDataMap::value_type(lbId, t));
        	return t;
    	}
    	else {
    		return (lbDataIt->second);
    	}
    }
    
    TcDataMap& getTcData() {
        return tcData_;
    }
    
    TcDataPtr addTcData(int tcNum) {
    	TcDataMap::iterator tcDataIt = tcData_.find(tcNum);
    	if(tcDataIt == tcData_.end() ) {
        	TcDataPtr t(new TcData(tcNum));
        	tcData_.insert(TcDataMap::value_type(tcNum, t));
        	return t;
    	}
    	else {
    		return (tcDataIt->second);
    	}
    }
    
    HalfSorterDataMap& getHalfSorterData() {
        return halfSorterData_;
    }
    
    HalfSorterDataPtr addHalfSorterData(int hsbNum) {
       	HalfSorterDataMap::iterator hsDataIt = halfSorterData_.find(hsbNum);
    	if(hsDataIt == halfSorterData_.end() ) {
        	HalfSorterDataPtr t(new HalfSorterData(hsbNum));
        	halfSorterData_.insert(HalfSorterDataMap::value_type(hsbNum, t));
        	return t;
    	}
    	else {
    		return (hsDataIt->second);
    	}
    }
    
    FinalSorterData& getFinalSorterData() {
        return finalSorterData_;
    }
};

typedef boost::shared_ptr<RPCBxData> BxDataPtr;
typedef std::vector<BxDataPtr> BxDataVector;
typedef std::map<unsigned int, BxDataPtr> BxDataMap; //[bxNum]

#endif /*BXDATA_H_*/
