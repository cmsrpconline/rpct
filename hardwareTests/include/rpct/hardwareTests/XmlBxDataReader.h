#ifndef RPCTXMLBXDATAREADER_H_
#define RPCTXMLBXDATAREADER_H_


#include "rpct/std/TXMLObjectMgr.h"
#include "rpct/hardwareTests/RPCDataStream.h"
#include "rpct/hardwareTests/RPCBxData.h"
#include "tb_std.h" 
#include <log4cplus/logger.h>



namespace rpct {
    

class XmlBxDataReader : public TXMLObjectMgr, public xercesc::HandlerBase {
public:
	static void setupLogger(std::string namePrefix);
private:
	static log4cplus::Logger logger;
    virtual void startElement(const XMLCh* const name, 
                            xercesc::AttributeList& attributes);
    virtual void endElement(const XMLCh* const name);
        
    void warning(const xercesc::SAXParseException& exception);
    void error(const xercesc::SAXParseException& exception);
    void fatalError(const xercesc::SAXParseException& exception);
    void resetErrors();
    bool sawErrors_;
    //std::string filename_;
    //BxDataVector* bxData_;    
    RPCDataStreamPtr dataStream_;
    BxDataPtr lastBxData_;
    RPCBxData::PacDataPtr lastPacData_;
    RPCBxData::TbDataPtr lastTbData_;
    RPCBxData::TcDataPtr lastTcData_;
    RPCBxData::MuonVecPtr lastMuonVector_;  
    RPCHardwareMuon::MuonType lastMuonType_;
    RPCBxData::LmuxBxDataSetPtr lastLmuxBxDataSet_;
    
    unsigned int lastOptLinkNum;
    
    RPCBxData& lastBxData() {
        if (lastBxData_.get() == 0) {
            throw TException("XmlBxDataReader::lastBxData_: lastBxData_ == 0");
        }
        return *lastBxData_;
    }
    
    RPCBxData::TcData& lastTcData() {
        if (lastTcData_.get() == 0) {
            throw TException("XmlBxDataReader::lastTcData: lastTcData == 0");
        }
        return *lastTcData_;
    }
    
    RPCBxData::TbData& lastTbData() {
        if (lastTbData_.get()  == 0) {
            throw TException("XmlBxDataReader::lastTbData: lastTbData == 0");
        }
        return *lastTbData_;
    }
    
    RPCBxData::PacData& lastPacData() {
        if (lastPacData_.get()  == 0) {
            throw TException("XmlBxDataReader::lastPacData: lastPacData == 0");
        }
        return *lastPacData_;
    }
    
    MuonVector& lastMuonVector() {
        if (lastMuonVector_.get() == 0) {
            throw TException("XmlBxDataReader::lastMuonVector: lastMuonVector == 0");
        }
        return *lastMuonVector_;
    }
    RPCHardwareMuon* createMuon(xercesc::AttributeList& attributes, RPCHardwareMuon::MuonType muonBitsType);
    
    LmuxBxData createLmuxBxData(xercesc::AttributeList& attributes);
    
    unsigned int mapBxNum(unsigned int num);
public:
    XmlBxDataReader();
    virtual ~XmlBxDataReader();
    RPCDataStreamPtr read(std::string filename);  
    RPCDataStreamPtr readString(const char* str);   
};

}

#endif 
