/*
 * LBStatisticDiagManager.h
 *
 *  Created on: Aug 8, 2008
 *      Author: Karol Bunkowswki
 */

#ifndef LBSTATISTICDIAGMANAGER_H_
#define LBSTATISTICDIAGMANAGER_H_

#include "rpct/hardwareTests/IStatisticDiagManager.h"

#include "rpct/devices/LinkBoard.h"
#include "rpct/diag/TDiagCtrl.h"
#include "rpct/diag/TIIHistoMgr.h"
#include "rpct/diag/IDiagnosable.h"
#include "rpct/diag/IDiagCtrl.h"

#include "TCanvas.h"
#include "TGraph.h"
#include "TTree.h"
#include "TLegend.h"
#include "TH1D.h"
#include "TPaveText.h"

#include <sys/time.h>
#include <map>
namespace rpct {

static const unsigned int LB_IN_CHANNELS_CNT = 96;
class LBStatisticDiagManager  : public IStatisticDiagManager {
protected:
    std::string name_;

    //std::string histosDir_;
    std::string picturesDir_;

    //IDiagnosable::TDiagCtrlVector diagCtrls_;
    //HistoMgrVector histoManagers_;

//    TFile outFile;
//    TObjArray histosArray_;

    time_t startTime_;

    std::vector<std::string> picturesDirs_;
    std::vector<std::string> picturesNames_;

    static log4cplus::Logger logger_;

    unsigned int iteration_;

public:
    static void setupLogger(std::string namePrefix);
    struct HistogramRecord {
    //public:
        //uint32_t - I believe it is 8 bytes
        uint64_t timerValue_;
        unsigned int startTime_; //time_t start of histograming time slice
        unsigned int stopTime_; //time_t start of histograming time slice
        unsigned int binsFull_[LB_IN_CHANNELS_CNT]; //96
        unsigned int binsWin_[LB_IN_CHANNELS_CNT]; //96

        time_t startTimeBuf_; //Buffer for start of histograming time slice
        uint64_t lastIntegratedFullCnt_;
        uint64_t lastIntegratedWinCnt_;
        uint64_t timerValuePrev_; //previous
        uint64_t timerValueIntegrated_;
    };
public:
    typedef std::vector<LinkBoard*> LinkBoards;
    LBStatisticDiagManager(std::string name, std::string picturesDir, LinkBoards& linkBoards, std::vector<HalfBox*> halfBoxes, bool takeFullHist, bool takeWinHist);
    virtual ~LBStatisticDiagManager();

    std::vector<HistogramRecord*>& getHistogramRecords() {
        return histogramRecords_;
    }

    virtual void createBranches(TTree& tree);

    virtual void resetAndConfigHistos(uint64_t countersLimit, IDiagCtrl::TTriggerType triggerType, uint32_t triggerDelay);
    virtual void start();
    virtual void stop();
    virtual bool readout(bool startAfterReadout);

    /* first call boardReloadedPrepare, and after reads call boardsReloadedCheck */
    virtual void boardsReloadedPrepare();

    enum ReloadState {
    	rsReloadDetected,
    	rsNoReloadDetected,
    	rsError //wrong value of the WORD_USER_REG2, most probably due to SEU

    };
    virtual ReloadState boardsReloadedCheck();

    void readoutSingleLB(bool startAfterReadout, unsigned int iLB);

    virtual Monitorable::MonitorStatusList& analyse();

    virtual void initialize(time_t startTime);
    virtual void finalize();

    std::string getHtml();

    //TODO implement

    virtual void resetAndConfigHistos();

    virtual bool countingEnded(uint64_t countingPeriod);

    virtual void setLogScale(bool logScale);

    unsigned int getLinkBoardsSize() {
    	return linkBoards_.size();
    }

private:
    std::vector<HistogramRecord*> histogramRecords_;

    Monitorable::MonitorStatusList monitorStatusList_;

    LinkBoards linkBoards_;
    std::vector<HalfBox*> halfBoxes_;
    bool takeFullHist_;
    bool takeWinHist_;

    std::vector<TCanvas*> canvases_;
    std::vector<TGraph*> rateGraphs_;

    TH1D* allLBsMeanRateHist_;
    TH1D* allLBsMaxRateHist_;

    std::vector<TH1D*> stripMeanRateHistos_;
    std::vector<TH1D*> stripMaxRateHistos_;

    TLegend legend1_;
    TLegend legend2_;

    TPaveText towerNameTextBox_;

    int lbsPerCanv_;

    static bool checkIfStatusIsBad(LinkBoard* lb);
    //////////////////////////////////////////////////////// MC - 17 Aug 2009
    // passing a non-null pointer to 'stopHisto_' of XdaqLBoxAccess class
    // allows one to abort faster: resetAndConfigHistos, readout, initialize, start, stop, analyse
private:
    volatile bool *stopHistoPtr_;
public:
    inline void setStopHistoPtr(volatile bool *ptr) {
        stopHistoPtr_=ptr;
    }
    /////////////////////////////////////////////////////////////////////////
};

}
#endif /* LBSTATISTICDIAGMANAGER_H_ */
