#ifndef TESTPULSESMANAGER_H_
#define TESTPULSESMANAGER_H_

#include "rpct/hardwareTests/RPCBxData.h"
#include "rpct/hardwareTests/RPCDataStream.h"
#include "tb_std.h" 
#include "rpct/devices/System.h"
#include <log4cplus/logger.h>

//obsolete
class TestPulsesManager {
private:
	//BxDataVector bxDataVec;
	
	RPCDataStreamPtr dataStream_;
	
	static log4cplus::Logger logger;
public:
	TestPulsesManager(std::string xmlBxdataFileName);
	virtual ~TestPulsesManager();
	
	//std::vector<rpct::BigInteger> getPacPulses(unsigned int tcNum, unsigned int tbNum, unsigned int pacNum);
	
	void configurePulsers(rpct::HardwareItemType type, unsigned int target, unsigned int pulseLength, uint64_t  pulserLimit, rpct::IDiagCtrl::TTriggerType pulserStartTrigger, const bool pulserRepeatEna);
	
	void configurePulsers(const rpct::System::HardwareItemList& chipList, unsigned int target, unsigned int pulseLength, uint64_t  pulserLimit, rpct::IDiagCtrl::TTriggerType pulserStartTrigger, const bool pulserRepeatEna);
	
	void startPulsers(rpct::HardwareItemType type); 
	
	void stopPulsers(rpct::HardwareItemType type);
	
	void configureReadouts(const rpct::System::HardwareItemList& chipList, int bxNum, unsigned int daqDataDelay, unsigned int readoutLimit, rpct::IDiagCtrl::TTriggerType diagStartTrigger, rpct::IDiagnosable::TTriggerDataSel triggerDataSel, uint32_t dataTrgDelay);
		
	void startReadouts(const rpct::System::HardwareItemList& chipList); 
	
	void stopReadouts(const rpct::System::HardwareItemList& chipList);
	
	bool checkCountingEnded(const rpct::System::HardwareItemList& chipList, std::string diagCntrType);
};

#endif /*TESTPULSESMANAGER_H_*/
