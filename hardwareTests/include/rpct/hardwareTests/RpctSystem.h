#ifndef RPCTSYSTEM_H_
#define RPCTSYSTEM_H_

#include "rpct/devices/LinkBox.h" 
#include "rpct/devices/LinkBoard.h"

#include "rpct/devices/TriggerBoard.h"
#include "rpct/devices/tcsort.h"
#include "rpct/devices/hsb.h"
#include "rpct/devices/fsb.h"
#include "rpct/devices/Dcc.h"
#include "rpct/devices/Ccs.h"

#include <log4cplus/logger.h>

//#include "rpct/hardwareTests/linkBoard.h"
//#include "rpct/hardwareTests/XmlBoardsSettings.h"
#/*include "rpct/hardwareTests/TTBOpto.h"
#include "rpct/hardwareTests/TPac.h"
#include "rpct/hardwareTests/TBGbSorter.h"
#include "rpct/hardwareTests/TCGbSorter.h"*/

#include "rpct/std/bitcpy.h"
#include <boost/dynamic_bitset.hpp>

//obsolete
class RpctSystem {
private:
	void init();
	
	static log4cplus::Logger logger;

	rpct::HalfBox::MasterLinkBoards masterLBsVec;
	rpct::HalfBox::LinkBoards       lbsVec;	
	
	//rpct::VmeCrate* triggerCrate;
	
	std::vector<rpct::TTCSortTCSort*> tcSortsVec;
	
    std::vector<rpct::THsbSortHalf*> hsbSortsVec;
    
    rpct::TFsbSortFinal* fsbSortFinal;
	
	std::vector<rpct::TriggerBoard*> tbsVec;
	
    //rpct::VmeCrate* controlCrate;
        
    std::vector<rpct::Dcc*> dccsVec;
        
    std::vector<rpct::Ccs*> ccssVec;
                

	//XmlBoardsSettings xmlBoardsSettings;
public:

	RpctSystem();
	
	
	~RpctSystem();
		
	rpct::HalfBox::LinkBoards&  getLBsVec() {
		return lbsVec;
	};
	
	rpct::HalfBox::MasterLinkBoards& getMasterLBsVec() {
		return masterLBsVec;
	};
		
	std::vector<rpct::TriggerBoard*>& GetTbsVec() {
		return tbsVec;
	};    
    
   	std::vector<rpct::TTCSortTCSort*>& getTcSortsVec() {
        return tcSortsVec; 
    }
    
    std::vector<rpct::THsbSortHalf*>& getHsbSortsVec() {
        return hsbSortsVec;
    }
    
    rpct::TFsbSortFinal* getFsbSortFinal() {
    	return fsbSortFinal;
    }
    
    std::vector<rpct::Dcc*>& getDccsVec() {
    	return dccsVec;
    }
    
    std::vector<rpct::Ccs*>& getCcssVec() {
        return ccssVec;
    }   
    
		
	void configureFromXml(std::string settingsFileName);
	
	void initLBs();
			
	void configureLBs();
	
	//void connectOptLinks();
	
	void initTCs();
	
	void resetTCs();
	
	void initSC();
	
	void resetSC();
	
	void initCC();
	
	void resetCC();
	
	void configureCC(int preTrig, int postTrig);    
	
	void configure();
	
	void enableAllRMBChannels(bool enalbe);
	
	void resetRMBs();
	
	void resetRecErrorCnt();
	
	void printRecErrorCnt();
	
	void synchronizeOptLinks();
	
	void enableTransmissionCheck(bool enableBCNcheck, bool enableDataCheck);	

	void printLinksConnection(std::ostream* ostr);
    
    void configureReadOutOnTBSorts(uint32_t  readoutLimit, rpct::IDiagCtrl::TTriggerType diagStartTrigger, rpct::IDiagnosable::TTriggerDataSel dataTrig);
    
    void configureReadOutOnTCSorts(uint32_t  readoutLimit, rpct::IDiagCtrl::TTriggerType diagStartTrigger, rpct::IDiagnosable::TTriggerDataSel dataTrig);
    
    void configureReadOutOnPacs(uint32_t  readoutLimit, rpct::IDiagCtrl::TTriggerType diagStartTrigger, rpct::IDiagnosable::TTriggerDataSel dataTrig);
    
    void configureReadOutOnHSBs(uint32_t  readoutLimit, rpct::IDiagCtrl::TTriggerType diagStartTrigger, rpct::IDiagnosable::TTriggerDataSel dataTrig);
    
};

#endif /*RpctSystem_H_*/
