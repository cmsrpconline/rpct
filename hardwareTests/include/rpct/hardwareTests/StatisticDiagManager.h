#ifndef STATISTICDIAGMANAGER_H_
#define STATISTICDIAGMANAGER_H_

#include "rpct/diag/TDiagCtrl.h"
#include "rpct/diag/TIIHistoMgr.h"
#include "rpct/diag/IDiagnosable.h"
#include "rpct/diag/IDiagCtrl.h"

#include "TH1.h"
#include "TFile.h"
#include "TObjArray.h"

#include <sys/time.h>
#include <map>

namespace rpct {

class StatisticDiagManager {
protected:
    std::string name_;

    std::string histosDir_;
    std::string picturesDir_;

    IDiagnosable::TDiagCtrlVector diagCtrls_;
    HistoMgrVector histoManagers_;

    TFile outFile;
    TObjArray histosArray_;

    timeval startTime;

    std::vector<std::string> picturesDirs_;
    std::vector<std::string> picturesNames_;

    static log4cplus::Logger logger;

    unsigned int interation_;

public:
    StatisticDiagManager(std::string name, std::string histosDir, std::string picturesDir, IDiagnosable::TDiagCtrlVector& diagCtrls, HistoMgrVector& histoManagers);

    virtual ~StatisticDiagManager();

    virtual void initialize(int runNumber);

    virtual void clear() {
        interation_ = 0;
    }; //clear the data containers here

    void resetAndConfigHistos(uint64_t countersLimit, IDiagCtrl::TTriggerType triggerType, uint32_t triggerDelay);

    void startHistos();

    void stopHistos();

    virtual void readoutAndAnalyse(double countingPeriod);

    std::vector<std::string>& getPicturesDirs() {
        return picturesDirs_;
    };

    IDiagnosable::TDiagCtrlVector& getDiagCtrls() {
        return diagCtrls_;;
    }

    virtual std::string getHtml() {
        return std::string("");
    }

};

//typedef std::map<std::string, StatisticDiagManager*> StatisticDiagManagersMap;
}
#endif /*STATISTICDIAGMANAGER_H_*/
