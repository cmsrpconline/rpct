#ifndef TCSTATISTICDIAGMANAGER_H_
#define TCSTATISTICDIAGMANAGER_H_

#include "rpct/hardwareTests/StatisticDiagManagerBase.h"
#include "rpct/devices/TriggerBoard.h"
#include "rpct/devices/TriggerCrate.h"

#include "TVectorT.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TLegendEntry.h"
#include "TLegend.h"
#include "TPaveText.h"
#include "TH2.h"

#include <vector>

namespace rpct {

class TCStatisticDiagManager: public StatisticDiagManagerBase {
private:
    class GBSHistogramRecord: public HistogramRecordBase {
    public:
        GBSHistogramRecord(IDiagCtrl* diagCtrl, HistoMgrVector& histoManagers) :
            HistogramRecordBase(diagCtrl, histoManagers) {
            for (unsigned int i = 0; i < binsCnt; i++) {
                branchData_.binsPacMuonCounters[i] = 0;
            }
        }
        static const unsigned int binsCnt = 48; //GbSort::RATE_DIAG_DATA_SIZE;
        struct BranchData {
            uint64_t timerValue_;
            unsigned int startTime_; //time_t start of histograming time slice
            unsigned int stopTime_; //time_t start of histograming time slice
            uint64_t binsPacMuonCounters[binsCnt]; //96
        };
    private:
        BranchData branchData_;
    public:
        BranchData& getBranchData() {
            return branchData_;
        }

        virtual void createBranch(TTree& tree, std::string& name) {
            tree.Branch(name.c_str(), &(branchData_.timerValue_),
                    "timerValue/l:startTime_/i:stopTime_/i:binsPacMuonCounters[48]/l");
        }

        virtual uint64_t getTimerValue() {
            return branchData_.timerValue_;
        }
        virtual void setTimerValue(uint64_t timerValue) {
            branchData_.timerValue_ = timerValue;
        }

        virtual unsigned int getStartTime() {
            return branchData_.startTime_;
        }
        virtual void setStartTime(unsigned int startTime) {
            branchData_.startTime_ = startTime;
        }

        virtual unsigned int getStopTime() {
            return branchData_.stopTime_;
        }
        virtual void setStopTime(unsigned int stopTime) {
            branchData_.stopTime_ = stopTime;
        }
    };

    class RMBHistogramRecord: public HistogramRecordBase {
    public:
        RMBHistogramRecord(IDiagCtrl* diagCtrl, HistoMgrVector& histoManagers) :
            HistogramRecordBase(diagCtrl, histoManagers) {
            for (unsigned int i = 0; i < binsCnt; i++) {
                branchData_.binsLinkFrameCounters[i] = 0;
            }
        };
        static const unsigned int binsCnt = 54; //GbSort::RATE_DIAG_DATA_SIZE;
        struct BranchData {
            uint64_t timerValue_;
            unsigned int startTime_; //time_t start of histograming time slice
            unsigned int stopTime_; //time_t start of histograming time slice
            uint64_t binsLinkFrameCounters[binsCnt]; //54
        };
    private:
        BranchData branchData_;
    public:
        BranchData& getBranchData() {
            return branchData_;
        }

        virtual void createBranch(TTree& tree, std::string& name) {
            tree.Branch(name.c_str(), &(branchData_.timerValue_),
                    "timerValue/l:startTime_/i:stopTime_/i:binsLinkFrameCounters[54]/l");
        }

        virtual uint64_t getTimerValue() {
            return branchData_.timerValue_;
        }
        virtual void setTimerValue(uint64_t timerValue) {
            branchData_.timerValue_ = timerValue;
        }

        virtual unsigned int getStartTime() {
            return branchData_.startTime_;
        }
        virtual void setStartTime(unsigned int startTime) {
            branchData_.startTime_ = startTime;
        }

        virtual unsigned int getStopTime() {
            return branchData_.stopTime_;
        }
        virtual void setStopTime(unsigned int stopTime) {
            branchData_.stopTime_ = stopTime;
        }
    };

    class TBItems {
    public:
        TriggerBoard* tb_;
        //RMB
        RMBHistogramRecord* rmbHistRecord_;
        TH1D* rmbMaxRateHist_;
        TH1D* rmbMeanRateHist_;
        TH1D* rmbMeanRateHist2_;
        TPaveText* rmbTitle_;
        TH2D* optLinksStatusHist_;
        //GBS
        GBSHistogramRecord* gbsHistRecord_;
        std::vector<uint64_t> previous;
    };
private:
    static const unsigned int tbMaxCnt_;
    TriggerCrate* tc_;
    std::vector<TBItems> tbItemsVec_;

    TH1D* gbsHist_;
    TH2D* gbsHist2_;
    std::vector< std::vector<TGraph*> > rateGraphs_;
    std::vector<TLegendEntry*> rateGraphLegendEntries_;
    TLegend towerRateLegend_;

public:
    TCStatisticDiagManager(std::string name, std::string picturesDir, TriggerCrate* tc);
    virtual ~TCStatisticDiagManager();

    virtual void createBranches(TTree& tree);

    //virtual void resetAndConfigHistos(uint64_t countersLimit, IDiagCtrl::TTriggerType triggerType, uint32_t triggerDelay);
    //virtual void start();
    //virtual void stop();
    //virtual bool readout(bool startAfterReadout);
    virtual Monitorable::MonitorStatusList& analyse();

    virtual void initialize(time_t startTime);
    //virtual void finalize();

    virtual std::string getHtml();
};

}
#endif /*TCSTATISTICDIAGMANAGER_H_*/
