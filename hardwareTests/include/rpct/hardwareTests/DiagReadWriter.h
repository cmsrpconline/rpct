#ifndef DIAGREADWRITER_H_
#define DIAGREADWRITER_H_
#include <sstream>
#include "rpct/diag/TStandardDiagnosticReadout.h"
//#include "rpct/hardwareTests/IDiagReadBXDataFormatter.h"

class DiagReadWriter {
private:
	std::ostream* ostr;	
	std::vector<rpct::TStandardDiagnosticReadout*> diagReadoutsVec;
	
	//std::vector<IDiagReadBXDataFormatter*> formattersVec;
public:
	DiagReadWriter(std::ostream* _ostr, std::vector<rpct::TStandardDiagnosticReadout*> _diagReadoutsVec);
	
/*	DiagReadWriter(std::ostream* _ostr, Options _options) 
	: ostr(_ostr), options(_options) {
	
	}*/
	
	virtual ~DiagReadWriter();
	
	void writeHeader(unsigned int runNum) {
	    (*ostr) <<"Beginning Header"<<std::endl;
		(*ostr) <<"Run "<<runNum<<std::endl;
		(*ostr)<<std::endl<<"End Header"<<std::endl;
	}
	
	void writeEvents();
	
	void writeEventsPararel();
	
	bool enableEVNCheck;
};

#endif /*DIAGREADWRITER_H_*/
