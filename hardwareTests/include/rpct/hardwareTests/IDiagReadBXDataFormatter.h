#ifndef IDIAGREADBXDATAFORMATTER_H_
#define IDIAGREADBXDATAFORMATTER_H_

class IDiagReadBXDataFormatter {
public:
    virtual std::string getLabel() = 0;
	virtual std::string format(void* _bxData) = 0;
};

#endif /*IDIAGREADBXDATAFORMATTER_H_*/
