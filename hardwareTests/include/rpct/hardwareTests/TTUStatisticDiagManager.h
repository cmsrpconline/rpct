/*
 * TTUStatisticDiagManager.h
 *
 *  Created on: Apr 27, 2010
 *      Author: aosorio
 */

#ifndef TTUSTATISTICDIAGMANAGER_H_
#define TTUSTATISTICDIAGMANAGER_H_

#include "rpct/hardwareTests/IStatisticDiagManager.h"
#include "rpct/hardwareTests/StatisticDiagManagerBase.h"
#include "rpct/devices/SorterCrate.h"

#include "rpct/diag/TDiagCtrl.h"
#include "rpct/diag/TIIHistoMgr.h"
#include "rpct/diag/IDiagnosable.h"
#include "rpct/diag/IDiagCtrl.h"

#include "TH1.h"
#include "TFile.h"
#include "TObjArray.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TLegendEntry.h"
#include "TLegend.h"
#include "TPaveText.h"

#include <sys/time.h>
#include <map>

namespace rpct {

class TTUStatisticDiagManager: public IStatisticDiagManager {
private:

	//................................................................................
	//... Time utilities + Tree data structure

	class TTUHistogramRecord {

	protected:
		time_t startTimeBuf_; //Buffer for start of histograming time slice
		uint64_t timerValuePrev_; //previous
		uint64_t timerValueIntegrated_;

	public:

		TTUHistogramRecord() {
		}

		virtual ~TTUHistogramRecord() {
		}

		struct TTUBranchData {
			uint64_t timerValue_;
			unsigned int startTime_; // time_t start of histograming time slice
			unsigned int stopTime_; // time_t start of histograming time slice
			unsigned int runNumber_;
			float wheelRates_[5];
			float sectorRateW0_[12];
			float sectorRateW1_[12];
			float sectorRateW2_[12];
			float sectorRateW3_[12];
			float sectorRateW4_[12];
		};

		TTUBranchData branchData_;

		TTUBranchData& getBranchData() {
			return branchData_;
		}

		void createBranch(TTree& tree, std::string& name) {
			tree.Branch(
					name.c_str(),
					&(branchData_.timerValue_),
					"timerValue/l:startTime_/i:stopTime_/i:runNumber_/i:wheelRates_[5]/f:sectorRateW0_[12]/f:sectorRateW1_[12]/f:sectorRateW2_[12]/f:sectorRateW3_[12]/f:sectorRateW4_[12]/f");
		}

		uint64_t getTimerValue() {
			return branchData_.timerValue_;
		}

		void setTimerValue(uint64_t timerValue) {
			branchData_.timerValue_ = timerValue;
		}

		unsigned int getStartTime() {
			return branchData_.startTime_;
		}

		void setStartTime(unsigned int startTime) {
			branchData_.startTime_ = startTime;
		}

		unsigned int getStopTime() {
			return branchData_.stopTime_;
		}

		void setStopTime(unsigned int stopTime) {
			branchData_.stopTime_ = stopTime;
		}

		time_t getStartTimeBuf() {
			return startTimeBuf_;
		}

		void setStartTimeBuf(time_t startTimeBuf) {
			startTimeBuf_ = startTimeBuf;
		}

		uint64_t getTimerValuePrev() {//previous
			return timerValuePrev_;
		}

		void setTimerValuePrev(uint64_t timerValuePrev) { //previous
			timerValuePrev_ = timerValuePrev;
		}

		uint64_t getTimerValueIntegrated() {
			return timerValueIntegrated_;
		}

		void setTimerValueIntegrated(uint64_t timerValueIntegrated) {
			timerValueIntegrated_ = timerValueIntegrated;
		}

	};

	//................................................................................

protected:

	static log4cplus::Logger logger;

	std::string name_;
	std::string picturesDir_;
	unsigned int iteration_;
	time_t startTime_; //start time of the monitoring job

	std::vector<std::string> picturesDirs_;
	std::vector<std::string> picturesNames_;

	TFile outFile;
	TObjArray histosArray_;
	TLegend * fsBarrelRateLegend1_;
	TPaveText * barrelPaveText_;

	std::vector<TPaveText *> sectorPaveText_;
	std::vector<TCanvas*> canvases_;
	std::vector<TGraph*> wheelRateGraphs_;
	std::vector<TGraph*> sectorRateGraphs_;
	std::vector<TLegend*> rateGraphLegendEntries_;

	TTUHistogramRecord* m_histogramRecords;

    Monitorable::MonitorStatusList monitorStatusList_;

public:
    static void setupLogger(std::string namePrefix);
	TTUStatisticDiagManager(std::string name, std::string picturesDir,
			SorterCrate*, bool);

	virtual ~TTUStatisticDiagManager();

	virtual void createBranches(TTree& tree);

	virtual void resetAndConfigHistos();
	virtual void start();
	virtual void stop();
	virtual bool readout(bool startAfterReadout);
	virtual Monitorable::MonitorStatusList& analyse();

	virtual void initialize(time_t startTime);
	virtual void finalize();

	virtual bool countingEnded(uint64_t countingPeriod);

	virtual std::string getHtml();

	virtual void setLogScale(bool logScale);

	virtual void setStopHistoPtr(volatile bool *ptr) {};

private:

	SorterCrate* m_SC;

	void initializeConstants();

	void setLegendOptions(TLegend *);

	void setAlarmOptions(TPaveText *);

	unsigned int m_maxWheels;
	unsigned int m_maxSectors;
	bool m_useLogScale;

	std::map<int, int> m_ttu_NWheel;
	std::map<int, int> m_ttu_WheelColour;
	std::map<int, int> m_ttu_WheelMarker;
	std::map<int, std::string> m_ttu_WheelTB;
	std::map<int, std::string> m_ttu_WheelPAC;

};

}

#endif /* TTUSTATISTICDIAGMANAGER_H_ */
