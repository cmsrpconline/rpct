#ifndef RPCTBXDATASTREAM_H_
#define RPCTBXDATASTREAM_H_

#include "rpct/hardwareTests/RPCBxData.h"
#include "rpct/devices/cmssw/RPCTBMuon.h"
#include "rpct/devices/RPCHardwareMuon.h"
#include "rpct/devices/LmuxBxData.h"
#include "rpct/devices/cmssw/RPCConst.h"
#include "rpct/std/TException.h"
#include "rpct/devices/System.h"
#include "rpct/std/BigInteger.h"
#include "rpct/diag/IDiagnosable.h"
#include <log4cplus/logger.h>

#include <map>
#include <vector>
#include <set>
#include <boost/shared_ptr.hpp>

class RPCDataStream {
private:
	static log4cplus::Logger logger;
public:
	static void setupLogger(std::string namePrefix);
	typedef std::map<unsigned int, rpct::LmuxBxData> OptLinkDataMap; //[bxNum]

	typedef std::map<unsigned int, RPCBxData::MuonVecPtr> MuonVecMap; //[bxNum][iMu]
private:
	BxDataMap bxDataMap_;
public:
	RPCDataStream() {};
	//RPCDataStream(BxDataVector bxDataVec): bxDataVec_(bxDataVec) {};
	
	std::vector<rpct::BigInteger> getLbPulses(unsigned int lbId, unsigned int pulseLength);
	
	//for Coder or LMUX out
	std::vector<rpct::BigInteger> getLbCodedPulses(unsigned int lbId, unsigned int pulseLength);
	
	OptLinkDataMap getOptLinkData(unsigned int tcNum, unsigned int tbNum, unsigned int optLiknNum);
	std::vector<OptLinkDataMap> getOptoData(unsigned int tcNum, unsigned int tbNum, unsigned int optoNum);
	
	
	OptLinkDataMap getTTUOptLinkData(unsigned int ttuNum, unsigned int optLiknNum);
	std::vector<RPCDataStream::OptLinkDataMap> getTTUOptoData(unsigned int ttuNum, unsigned int optoNum);
	
	
	//the returned vectors are the same as stored in the RPCBxData, therfore there are pointers in the map 
	//[bxNum][iMu]
	MuonVecMap getPacDataMap(unsigned int tcNum, unsigned int tbNum, unsigned int pacNum);

	MuonVecMap getTbSortInDataMap(unsigned int tcNum, unsigned int tbNum);
	
	MuonVecMap getTbSortDataMap(unsigned int tcNum, unsigned int tbNum);

	MuonVecMap getTcSortDataMap(unsigned int tcNum);
	
	MuonVecMap getTcSortInDataMap(unsigned int tcNum);
	
	MuonVecMap getHalfSortInDataMap(unsigned int hsNum);
	
	MuonVecMap getHalfSortOutDataMap(unsigned int hsNum);
	
	MuonVecMap getFinalSortInDataMap();
	
	MuonVecMap getFinalSortOutDataMap();
	
	BxDataPtr addBxData(unsigned int bxNum) {
    	BxDataMap::iterator bxDataIt = bxDataMap_.find(bxNum);
    	if(bxDataIt == bxDataMap_.end() ) {
        	BxDataPtr t(new RPCBxData(bxNum));
        	bxDataMap_.insert(BxDataMap::value_type(bxNum, t));
        	return t;
    	}
    	else {
    		return (bxDataIt->second);
    	}		
	};
	
	//void addPacData(unsigned int tcNum, unsigned int tbNum, unsigned int pacNum, MuonVecMap pacDataMap);
	
	//adds the bxData to the optLinkData, puts them in the position bxNum or first free, allowed, position, sets the proper partitioon daly 
	void multipelxLmuxData(OptLinkDataMap& optLinkData, const RPCBxData::LmuxBxDataSet& bxData, unsigned int bxNum);
	
	std::vector<rpct::BigInteger> getPulses(std::vector<OptLinkDataMap> optoData, unsigned int pulseLength);
	
	std::vector<rpct::BigInteger> getTTUPulses(std::vector<OptLinkDataMap> optoData, unsigned int pulseLength);
	
	std::vector<rpct::BigInteger> getPulses(MuonVecMap& muons,  unsigned int pulseWidth, unsigned int muonsCnt, unsigned int muonBitsCnt, unsigned int pulseLength);
	
	std::vector<rpct::BigInteger> getPuslsesFor(rpct::IDiagnosable* chip, unsigned int target, unsigned int pulseLength);
};

typedef boost::shared_ptr<RPCDataStream> RPCDataStreamPtr;

#endif /*RPCTBXDATASTREAM_H_*/
