#ifndef SORTERSSTATISTICDIAGMANAGER_H_
#define SORTERSSTATISTICDIAGMANAGER_H_

#include "rpct/hardwareTests/StatisticDiagManager.h"
#include "TVectorT.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TLegendEntry.h"
#include "TLegend.h"
#include "TPaveText.h"
#include <vector>

namespace rpct {
class SortersStatisticDiagManager : public StatisticDiagManager {
private:
    TCanvas* canvas1_;
    TCanvas* canvas2_;
    std::vector<TGraph*> rateGraphs_;
    std::vector<TLegendEntry*> rateGraphLegendEntries_;     
    
    //std::vector<float> timeXAxis_;
    //std::vector<std::vector<float> > ratesYAxis_;    
    
    std::string fsBarrelRateGif_;
    std::string fsEndcapRateGif_;
    std::string fsBarrelPtCodeGif_;
    std::string fsEndcapPtCodeGif_;
    std::string fsBarrelQualityGif_;
    std::string fsEndcapQualityGif_;
    
    TLegend fsBarrelRateLegend1_;
    TLegend fsEndcapRateLegend1_;
    TLegend fsBarrelRateLegend2_;
    TLegend fsEndcapRateLegend2_;
    
    TPaveText barrelPaveText_;
    TPaveText endcapPaveText_;
    
    std::vector<TH1I*> ptCodeHistos_;
    std::vector<TH1I*> qualityHistos_;
    
    std::vector<double> integratedRateVec_;
public:
    SortersStatisticDiagManager(std::string name, std::string histosDir, std::string picturesDir, IDiagnosable::TDiagCtrlVector& diagCtrls, HistoMgrVector& histoManagers);
    virtual ~SortersStatisticDiagManager();
    
    virtual void clear();
    
    virtual void readoutAndAnalyse(double countingPeriod);
    
    virtual std::string getHtml();
};

}
#endif /*SORTERSSTATISTICDIAGMANAGER_H_*/
