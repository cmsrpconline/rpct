#ifndef ISTATISTICDIAGMANAGER_H_
#define ISTATISTICDIAGMANAGER_H_

#include "TTree.h"
#include "rpct/ii/Monitorable.h"

#include <map>

namespace rpct {

class IStatisticDiagManager {
public:
    virtual void createBranches(TTree& tree) = 0;

    virtual void resetAndConfigHistos() = 0;
    virtual void start() = 0;
    virtual void stop() = 0;
    /**
     * return true if data are valid and should be analyzed and saved
     */
    virtual bool readout(bool startAfterReadout) = 0;
    virtual Monitorable::MonitorStatusList& analyse() = 0;

    virtual void initialize(time_t startTime) = 0;
    virtual void finalize() = 0;

    virtual bool countingEnded(uint64_t coutingPeriod) = 0;

    virtual std::string getHtml() = 0;

    virtual void setLogScale(bool logScale) = 0;

    // Some diagManagers will cancel their operations faster if this flag is passed
    // and set to false by other thread
    virtual void setStopHistoPtr(volatile bool *ptr) = 0;


    virtual ~IStatisticDiagManager() {}
};

typedef std::map<std::string, IStatisticDiagManager*> StatisticDiagManagersMap;
}
#endif /*ISTATISTICDIAGMANAGER_H_*/
