#ifndef TBDIAGREADEVENT_
#define TBDIAGREADEVENT_
#include <boost/shared_ptr.hpp>
#include <vector>

#include "rpct/hardwareTests/RPCBxData.h"

/*class TBDiagReadEvent {
private:
	unsigned int eventNum;
	unsigned int bxnNum; //the bxn of the L1A of this event

public:
	TBDiagReadEvent(unsigned int _eventNum, unsigned int _bxnNum);
	
	void addLinkRecord(unsigned int tbNum, unsigned int linkNum, unsigned int bxOfRecord, TLmuxBxData linkDataRecord);

};*/


class DiagReadBxData {
public:
	
};

class PacDiagReadBxData: public  DiagReadBxData {
public:
	
};

class TBSortDiagReadBxData: public  DiagReadBxData {
private:
	MuonVector outMuons_;
	std::vector<MuonVector>	pacsMuons_;
	unsigned int dataValid;
public:
	
};

typedef boost::shared_ptr<DiagReadBxData> DiagReadBxDataPtr; 

typedef std::vector<DiagReadBxDataPtr> DiagReadBxDataVec;

class DiagReadEvent {
private:
	unsigned int eventNum_;
	unsigned int bxnNum_; //the bxn of the L1A of this event
	
	DiagReadBxDataVec diagReadBxDataVec_;
public:
	void addDiagReadBxData(DiagReadBxDataPtr diagReadBxData) {
		diagReadBxDataVec_.push_back(diagReadBxData);	
	};
	
	void dupa() {
		DiagReadBxDataPtr p(new PacDiagReadBxData());
		addDiagReadBxData(p);
	};
};
#endif /*TBDIAGREADEVENT_*/
