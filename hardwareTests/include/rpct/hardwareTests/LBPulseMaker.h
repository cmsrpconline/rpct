#ifndef LBPulseMaker_h
#define LBPulseMaker_h
#include <stdint.h>
#include <vector>

class LBPulseMaker {

public:

  LBPulseMaker();
  ~LBPulseMaker(){};
  void setPulseTestPattern(int i) {testType= i;};
  std::vector<uint32_t> getTestPulse(int  iLB);



private:

  uint32_t testPulse(int clockTick, int iLB);
  uint32_t serialTest(int clockTick, int iLB);


  int testType;

};


#endif
