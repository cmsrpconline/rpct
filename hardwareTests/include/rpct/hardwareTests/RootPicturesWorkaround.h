/*
 * RootPicturesWorkaround.h
 *
 *  Created on: Sep 23, 2010
 *      Author: Karol Bunkowski
 */

#ifndef ROOTPICTURESWORKAROUND_H_
#define ROOTPICTURESWORKAROUND_H_
#include <vector>
#include <string>

#include <log4cplus/logger.h>

#include "TCanvas.h"

class RootPicturesWorkaround {
public:
	static void crateRootMacro(std::string& name, std::string& picturesDir);

	static void createPictures(std::string& name, std::string& picturesDir, std::vector<TCanvas*>& canvases, log4cplus::Logger& logger);
};

#endif /* ROOTPICTURESWORKAROUND_H_ */
