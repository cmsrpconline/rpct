/*
 * FEBsTimingTester.h
 *
 *  Created on: Oct 10, 2008
 *      Author: tb
 */

#ifndef FEBSTIMINGTESTER_H_
#define FEBSTIMINGTESTER_H_
#include "rpct/devices/System.h"
#include "rpct/devices/LinkSystem.h"
#include "rpct/devices/LinkBoard.h"
#include "rpct/devices/ConfigurationSetImpl.h"
#include "rpct/devices/SynCoderSettingsImpl.h"
#include "rpct/lboxaccess/FecManagerImpl.h"
#include "xdata/XStr.h"
#include "xdata/Float.h"
#include "xdata/Bag.h"
#include "xdata/Boolean.h"
#include "xdata/Integer.h"
#include "xdata/TableIterator.h"
#include "TH1.h"
#include "TH2.h"
#include "TFile.h"
#include "TObjArray.h"
#include <vector>
#include <log4cplus/logger.h>

namespace rpct {

class FEBsTimingTester {
public:
	static void setupLogger(std::string namePrefix);

    enum WindowTestType {
        testWinO,
        testWinC
    };
public:
	class IAnalyser {
	public:
		/**
		 * nPulses - count of the sent, thus expected pulses in the full window hits
		 */
		virtual ~IAnalyser() {

		}
		virtual std::string analyse(
				WindowTestType windowTestType, int nPulses,
				uint32_t pulsePattern,
				std::vector<int> winPos,
				std::vector<std::vector<int> > fulWinTab,
				std::vector<std::vector<int> > addWinTab ) = 0;
	};
protected:
static log4cplus::Logger logger;
public:
static log4cplus::Logger& getLogger() {
	return logger;
}
private:
	std::string outputDir;
	TFile outRootFile_;
    static const int winStep;

public:
    FEBsTimingTester(std::string outputDir);
    virtual ~FEBsTimingTester();
    std::string timingTest(double countingTime, WindowTestType windowTestType, uint32_t pulsePattern,
    				int winOInit, int winCInit, int pulsDistance, IAnalyser* analyser, rpct::LinkSystem::LinkBoards& linkBoards);
    void pulseTest(rpct::LinkSystem::LinkBoards& linkBoards);

    class AnalyserForTestBoard: public IAnalyser {
    private:
    public:
    	AnalyserForTestBoard() {
    	}

    	virtual ~AnalyserForTestBoard() {};

    	virtual std::string analyse(
    			WindowTestType windowTestType, int nPulses,
    			uint32_t pulsePattern,
    			std::vector<int> winPos,
    			std::vector<std::vector<int> > fulWinTab,
    			std::vector<std::vector<int> > addWinTab );

    	int pulseForOutChannel(uint32_t pulsePattern, int channelNum);
    };
};

}; //end of namespace rpct
#endif /* FEBSTIMINGTESTER_H_ */
