#ifndef LBTESTER_H_
#define LBTESTER_H_

#include "rpct/hardwareTests/RpctSystem.h"

#include "rpct/std/bitcpy.h"
#include <boost/dynamic_bitset.hpp>

//robi testy dla wszytkich LB z masterLBsVec jednoczesnie
//obsolete
class LBTester {
private:
	RpctSystem& rpctSystem;
public:
	enum RpcInputType {
		febs,
		testBoards,
		none
	};

	LBTester(RpcInputType _inputType, RpctSystem& _rpctSystem);		
		
	void fullTest(unsigned int readoutRepetitionsCnt);		
	
	void rpcInputsTestWithReadout(unsigned int readoutRepetitionsCnt);

	void rpcInputsTestWithCounters(unsigned int countingTime);
	
	enum WindowTestType {
		testWinO,
		testWinC
	};
	
	void windowTest(double countingTime, WindowTestType windowTestType, int winOInit, int winCInit, int winStep);
	
	void windowTestWithFEBs(double countingTime, WindowTestType windowTestType);
	
	void histosTest(double countingTime);
	
	void slaveMasterTest(unsigned int readoutRepetitionsCnt);
			
	boost::dynamic_bitset<> pulsToInput(boost::dynamic_bitset<> pulsePattern, RpcInputType rpcinputType);
	
	void preConfigure(std::string lbsSettingsFileName);
	
	void coutResults() {
		std::cout << resultsLog.str() << std::endl;
	}
	
	void configureForGolTest();
	
	void saveResults(std::string fileName) ;
	
private:
	std::string systemFileName;
	
	RpcInputType rpcInputType;
	
	std::ostringstream resultsLog;	
};

#endif /*LBTESTER_H_*/
