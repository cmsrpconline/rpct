#ifndef XMLBOARDSSETTINGS_H_
#define XMLBOARDSSETTINGS_H_
#include <string>

#include "rpct/std/TXMLObjectMgr.h"

#include <xercesc/sax/HandlerBase.hpp>   
#include <xercesc/sax/AttributeList.hpp>
//public TXMLObjectMgr,


//typedef std::map<unsigned int, OptLinkConnection> OptLinkConnections; //[mlbid]
//typedef std::vector<OptLinkConnection> OptLinkConnections;

class DccConnection {
public:
	unsigned int dccId;
	unsigned int tbId;
	unsigned int dccChannelNum, dccChannelId;
};

typedef std::vector<DccConnection> DccConnections;

class XmlBoardsSettings :  public xercesc::HandlerBase {
public:
	XmlBoardsSettings();
	virtual ~XmlBoardsSettings();
	
	void readFile(std::string filename);
	
	DccConnections& getDccConnections() {
		return dccConnections;
	}
private:
	static log4cplus::Logger logger;

	bool sawErrors;
    std::string filename;
    
    virtual void startElement(const XMLCh* const name, 
                            xercesc::AttributeList& attributes);
    virtual void endElement(const XMLCh* const name);
    std::string getAttribute(const char* attrName, xercesc::AttributeList& attributes);
        
    void warning(const xercesc::SAXParseException& exception);
    void error(const xercesc::SAXParseException& exception);
    void fatalError(const xercesc::SAXParseException& exception);
    void resetErrors();
    
    
    bool getHexAttribute(const char* attrName, xercesc::AttributeList& attributes, unsigned int& value);
    bool getDecAttribute(const char* attrName, xercesc::AttributeList& attributes, unsigned int& value);    
	
	DccConnections dccConnections;
};

#endif /*XMLBOARDSSETTINGS_H_*/
