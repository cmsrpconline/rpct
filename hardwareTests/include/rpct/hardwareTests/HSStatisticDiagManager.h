#ifndef HSSTATISTICDIAGMANAGER_H_
#define HSSTATISTICDIAGMANAGER_H_

#include "rpct/hardwareTests/StatisticDiagManager.h"
#include "TVectorT.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TLegendEntry.h"
#include "TLegend.h"
#include "TPaveText.h"
#include <vector>

namespace rpct {
class HSStatisticDiagManager : public StatisticDiagManager {
private:
    TCanvas* canvas1_;
    //TCanvas* canvas2_;
    
    std::vector<TH1F*> lastRateHistos_;

public:
    HSStatisticDiagManager(std::string name, std::string histosDir, std::string picturesDir, IDiagnosable::TDiagCtrlVector& diagCtrls, HistoMgrVector& histoManagers);
    virtual ~HSStatisticDiagManager();
    
    virtual void clear();
    
    virtual void readoutAndAnalyse(double countingPeriod);
    
    virtual std::string getHtml();
};

}
#endif /*HSSTATISTICDIAGMANAGER_H_*/
