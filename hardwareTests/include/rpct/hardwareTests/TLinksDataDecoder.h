//---------------------------------------------------------------------------

#ifndef TLinksDataDecoderH
#define TLinksDataDecoderH
//---------------------------------------------------------------------------
#include "TBitset.h"

//data transmited by one link, decoded, one bx, up to 3? Rpcs
class TLink1BxStrips { //state of strips in one BX
public:
  TLink1BxStrips(int rpcsCnt, int rpcStripsCnt) {
    RpcsVec = TBitsetsVec(rpcsCnt, TBitset(rpcStripsCnt, false));
  }

  TBitsetsVec RpcsVec;
  int EndOfData;
};

typedef std::vector<TLink1BxStrips> TLink1BxStripsVec; //[bx].RpcsVec[rpc][strip]
//----------------------------------------------------------------------------

class TLinksDataDecoder {
public:
  TLinksDataDecoder(const int &partitionDataWidth, const int &partitionCodeBitsCnt,
                    const int &partitionDelayBitsCnt, const int &rpcNumBitsCnt,
                    const int &bxNumBitsCnt, const int &endOfDataBitsCnt,
                    const int &rpcStripsCnt, const int &rpcsCnt):
    RpcNumBitsCnt(rpcNumBitsCnt),
    PartitionDelayBitsCnt(partitionDelayBitsCnt),
    PartitionCodeBitsCnt(partitionCodeBitsCnt),
    PartitionDataWidth(partitionDataWidth),
    EndOfDataBitsCnt(endOfDataBitsCnt),
    RpcStripsCnt(rpcStripsCnt),
    RpcsCnt(rpcsCnt),
    BxNumBitsCnt(bxNumBitsCnt)
    //DecodedData = TRpcsStripsVec();
  {}

  TLink1BxStripsVec Decode(const TBitsetsVec& codedData);

  std::string PrintCodedData(const TBitsetsVec& codedData);

private:
  const int RpcNumBitsCnt;
  const int PartitionDelayBitsCnt;
  const int PartitionCodeBitsCnt;
  const int PartitionDataWidth;
  const int EndOfDataBitsCnt;
  const int RpcStripsCnt; //in one bx
  const int RpcsCnt;
  const int BxNumBitsCnt;


  TLink1BxStripsVec DecodedData;  

  void AddPartitionData(TBitset& partitionData, int bx, int partitionNum, int rpcNum, int endOfData);
};
#endif
