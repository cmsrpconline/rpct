#include "rpct/hardwareTests/RpctSystem.h"
/*#include "rpct/devices/LinkBoard.h"
#include "rpct/lboxaccess/FecManagerImpl.h"*/
#include "rpct/devices/System.h"
#include "rpct/devices/XmlSystemBuilder.h"
#include <boost/dynamic_bitset.hpp>
#include "rpct/std/bitcpy.h"

#include <time.h>
#include <sys/time.h>

using namespace rpct;
using namespace std;
using namespace boost;
using namespace log4cplus;

Logger RpctSystem::logger = Logger::getInstance("RpctSystem");

RpctSystem::RpctSystem() {
    init();
}

void RpctSystem::init() {
/*    triggerCrate = 0;
    tcSort = 0;
    hsbSort = 0;*/
    
	System& system = System::getInstance();
    try {
        system.checkVersions(); ///<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    }
    catch (TException& e ) {
	//LOG4CPLUS_INFO(logger, "error during initialization of "<<lbsVec[iLB]->getDescription()<<" id "<< lbsVec[iLB]->getId());
	cerr<<"error  system.checkVersions()"<<endl;
	cerr << e.what() <<endl;		
	cout<<"continue? (y/n) ";
	char yn;
	cin>>yn;
	if(yn == 'n')
	    throw e; 
    }
	
	const System::CrateMap& crateMap = system.getCrateMap();

    for(System::CrateMap::const_iterator it = crateMap.begin(); it != crateMap.end(); it++) {
        if( dynamic_cast<LinkBox*>(it->second) != NULL) {
            if(dynamic_cast<LinkBox*>(it->second)->getHalfBox10() != NULL) {
        	lbsVec.insert(lbsVec.end(),
        	          dynamic_cast<LinkBox*>(it->second)->getHalfBox10()->getLinkBoards().begin(),
        	          dynamic_cast<LinkBox*>(it->second)->getHalfBox10()->getLinkBoards().end() );	
            
                masterLBsVec.insert(masterLBsVec.end(),
        	          dynamic_cast<LinkBox*>(it->second)->getHalfBox10()->getMasters().begin(),
        	          dynamic_cast<LinkBox*>(it->second)->getHalfBox10()->getMasters().end() );	
            }          

            if(dynamic_cast<LinkBox*>(it->second)->getHalfBox20() != NULL) {
        	lbsVec.insert(lbsVec.end(),
        	          dynamic_cast<LinkBox*>(it->second)->getHalfBox20()->getLinkBoards().begin(),
        	          dynamic_cast<LinkBox*>(it->second)->getHalfBox20()->getLinkBoards().end() );        	        	         	        	         

        	masterLBsVec.insert(masterLBsVec.end(),
        	          dynamic_cast<LinkBox*>(it->second)->getHalfBox20()->getMasters().begin(),
        	          dynamic_cast<LinkBox*>(it->second)->getHalfBox20()->getMasters().end() );
            }
  
        }        
    }  
    LOG4CPLUS_INFO(logger, "added " <<  lbsVec.size()<<" LBs including "<<masterLBsVec.size()<<" masters "); 
//--------------------------tc ------------------------------------------------------------------------  
    const System::HardwareItemList& tbList = system.getHardwareItemsByType(TriggerBoard::TYPE);
    for (System::HardwareItemList::const_iterator iItem = tbList.begin(); iItem != tbList.end(); ++iItem) {
        (*iItem)->getDescription();  
        tbsVec.push_back(dynamic_cast<TriggerBoard*>(*iItem));
        LOG4CPLUS_INFO(logger, "added " <<  (*iItem)->getDescription()<<" "<<(*iItem)->getId()); 
    }
    
    const System::HardwareItemList& tcList = system.getHardwareItemsByType(TTCSortTCSort::TYPE);
    for (System::HardwareItemList::const_iterator iItem = tcList.begin(); iItem != tcList.end(); ++iItem) {
        (*iItem)->getDescription();  
        tcSortsVec.push_back(dynamic_cast<TTCSortTCSort*>(*iItem));
        LOG4CPLUS_INFO(logger, "added " <<  (*iItem)->getDescription()<<" "<<(*iItem)->getId()); 
    }           
       
    const System::HardwareItemList& hsbList = system.getHardwareItemsByType(THsbSortHalf::TYPE);        
    for (System::HardwareItemList::const_iterator iItem = hsbList.begin(); iItem != hsbList.end(); ++iItem) {
        (*iItem)->getDescription();  
        hsbSortsVec.push_back(dynamic_cast<THsbSortHalf*>(*iItem));
        LOG4CPLUS_INFO(logger, "added " <<  (*iItem)->getDescription()<<" "<<(*iItem)->getId()); 
    }
     
    const System::HardwareItemList& fsbList = system.getHardwareItemsByType(TFsbSortFinal::TYPE);  
    if(fsbList.size() == 1) {
    	fsbSortFinal = dynamic_cast<TFsbSortFinal*>(*(fsbList.begin()));
    }
    
    const System::HardwareItemList& dccList = system.getHardwareItemsByType(Dcc::TYPE);        
    for (System::HardwareItemList::const_iterator iItem = dccList.begin(); iItem != dccList.end(); ++iItem) {
        (*iItem)->getDescription();  
        dccsVec.push_back(dynamic_cast<Dcc*>(*iItem));
        LOG4CPLUS_INFO(logger, "added " <<  (*iItem)->getDescription()<<" "<<(*iItem)->getId()); 
    }
    
    const System::HardwareItemList& ccsList = system.getHardwareItemsByType(Ccs::TYPE);        
    for (System::HardwareItemList::const_iterator iItem = ccsList.begin(); iItem != ccsList.end(); ++iItem) {
        (*iItem)->getDescription();  
        ccssVec.push_back(dynamic_cast<Ccs*>(*iItem));
        LOG4CPLUS_INFO(logger, "added " <<  (*iItem)->getDescription()<<" "<<(*iItem)->getId()); 
    }
        
/*    const System::BoardMap& boardMap = system.getBoardMap();      
    for(System::BoardMap::const_iterator itBoard = boardMap.begin(); itBoard != boardMap.end(); itBoard++) {
    	if(dynamic_cast<TTb3*>(itBoard->second) != 0) {
    		tbsVec.push_back(dynamic_cast<TTb3*>(itBoard->second));
    		LOG4CPLUS_INFO(logger, "added " <<  (itBoard->second)->getDescription()<<" "<<(itBoard->second)->getId());    		
    	}
    	
    	else if(dynamic_cast<TTCSort*>(itBoard->second) != 0) {
    		tcSortsVec.push_back(&(dynamic_cast<TTCSort*>(itBoard->second)->GetTCSortTCSort()));
    	}
    	
    	else if(dynamic_cast<THsb*>(itBoard->second) != 0) {
    		hsbSortsVec.push_back(&(dynamic_cast<THsb*>(itBoard->second)->GetHsbSortHalf()));
    	}	
    	
    	else if(dynamic_cast<TFsb*>(itBoard->second) != 0) {
    		fsbSortFinal = &(dynamic_cast<TFsb*>(itBoard->second)->GetFsbSortFinal());
    	}
//--------------------------cc ------------------------------------------------------------------------ 
         else if(dynamic_cast<Dcc*>(itBoard->second) != 0) {
               dccsVec.push_back(dynamic_cast<Dcc*>(itBoard->second));
               LOG4CPLUS_INFO(logger, "added " <<  (itBoard->second)->getDescription()<<" "<<(itBoard->second)->getId());              
         }
         
         else if(dynamic_cast<Ccs*>(itBoard->second) != 0) {
               ccssVec.push_back(dynamic_cast<Ccs*>(itBoard->second));
               LOG4CPLUS_INFO(logger, "added " <<  (itBoard->second)->getDescription()<<" "<<(itBoard->second)->getId());              
         }    	
    } */

//------------------------------------------------       
}

RpctSystem::~RpctSystem() {
}

void RpctSystem::configureFromXml(std::string settingsFileName) {
	//xmlBoardsSettings.readFile(settingsFileName.c_str());
}

/*void RpctSystem::initLBs() {
	for (unsigned int iLB = 0; iLB < lbsVec.size(); iLB++) {	
		//cout<<lbsVec[iLB]->getStdLb().getDescription()<<" id "<< lbsVec[iLB]->getStdLb().GetID()<<endl;
	    try {
		LOG4CPLUS_INFO(logger, lbsVec[iLB]->getDescription()<<" id "<< lbsVec[iLB]->getId());
		lbsVec[iLB]->initialize();
		//cout<<"    lb initilaized"<<endl;
		LOG4CPLUS_INFO(logger, "    lb initilaized");
		lbsVec[iLB]->testClocks();
	    //lbsVec[iLB]->configure();?????
	    }	    
	    catch (TException& e ) {
	        //LOG4CPLUS_INFO(logger, "error during initialization of "<<lbsVec[iLB]->getDescription()<<" id "<< lbsVec[iLB]->getId());
	        cerr<<"error during initialization of "<<lbsVec[iLB]->getDescription()<<" id "<< lbsVec[iLB]->getId()<<endl;
		cerr << e.what() <<endl;		
		cout<<"continue? (y/n) ";
		char yn;
		cin>>yn;
		if(yn == 'n')
		    throw e; 
	    }
	}
}

void RpctSystem::configureLBs() {	
	LinkBoardSettingsMap& lbSettings = xmlBoardsSettings.getLinkBoardSettingsMap();		
		
	for (unsigned int iLB = 0; iLB < lbsVec.size(); iLB++) {	
		unsigned int lbId = lbsVec[iLB]->getId();
		lbsVec[iLB]->getSynCoder().configureAsSlaveOrMaster();		
		lbsVec[iLB]->configure(lbSettings[lbId]); 	
		LOG4CPLUS_INFO(logger, lbsVec[iLB]->getDescription()<<" id "<< lbsVec[iLB]->getId()<<" configured");	
    }
}

void RpctSystem::initTCs() {
		for(unsigned int iTc = 0; iTc < tcSortsVec.size(); iTc++)
			tcSortsVec[iTc]->Initialize();
		
		for (unsigned int iTb = 0; iTb < tbsVec.size(); iTb++) {
			tbsVec[iTb]->Initialize();
		}
}

void RpctSystem::resetTCs() {
	for(unsigned int iTc = 0; iTc < tcSortsVec.size(); iTc++)
		tcSortsVec[iTc]->Reset();
	
	for (unsigned int iTb = 0; iTb < tbsVec.size(); iTb++) {
		tbsVec[iTb]->Reset();
	}	
} 

void RpctSystem::initSC() {
	for(unsigned int iHsb = 0; iHsb < hsbSortsVec.size(); iHsb++) {
		try {
			hsbSortsVec[iHsb]->Initialize();
		}	
		catch(TException& e) {
			cerr << e.what() <<endl;
			cout<<"contimue? ()y/n)";
			char yn = 'n';
			cin>>yn;
			if(yn == 'n') {
				throw e;
			}
		}
	} 
	if(fsbSortFinal != NULL) {
		try {
			fsbSortFinal->Initialize();
		}	
		catch(TException& e) {
			cerr << e.what() <<endl;
			cout<<"contimue? ()y/n)";
			char yn = 'n';
			cin>>yn;
			if(yn == 'n') {
				throw e;
			}
		}
	}	
}

void RpctSystem::resetSC() {
	for(unsigned int iHsb = 0; iHsb < hsbSortsVec.size(); iHsb++) {
		hsbSortsVec[iHsb]->Reset();
	} 
	if(fsbSortFinal != NULL) {
		//fsbSortFinal->Reset();
	}	
}*/

void RpctSystem::initCC() {
  for(unsigned int iDcc = 0; iDcc < dccsVec.size(); iDcc++){
    dccsVec[iDcc]->initialize();
  }               
  for (unsigned int iCcs = 0; iCcs < ccssVec.size(); iCcs++) {
    ccssVec[iCcs]->initialize();
  }
}

void RpctSystem::resetCC() {
  for(unsigned int iDcc = 0; iDcc < dccsVec.size(); iDcc++){
    dccsVec[iDcc]->reset();
  }
} 

void RpctSystem::configureCC(int preTrig = 3, int postTrig =3){//nie optymalne, warto poprawic
  for(unsigned int iCcs=0; iCcs<ccssVec.size(); iCcs++){
    bitset<6> dccIn(0x3e);
    for(unsigned int iDcc=0; iDcc<dccsVec.size(); iDcc++){
      int posDiff = ((dccsVec[iDcc]->getPosition()-ccssVec[iCcs]->getPosition()));
      if((posDiff<=3)&&(posDiff>0)){
		LOG4CPLUS_INFO(logger, "CCS: TTS_In<"<<posDiff-1<<"> unmasked");
		dccIn.set(posDiff,0);
      }
      else{
		LOG4CPLUS_INFO(logger, "WARNING!! Wrong distance between CCS and DCC boards "<<posDiff);
      }
    }
    ccssVec[iCcs]->configure(dccIn.to_ulong());
  }
  
/*  DccConnections& dccConnections = xmlBoardsSettings.getDccConnections();
  for(unsigned int iDcc=0; iDcc<dccsVec.size(); iDcc++){
    vector<pair<int,int> > dccInIds;
    for(DccConnections::iterator dcIt = dccConnections.begin(); dcIt != dccConnections.end(); dcIt++){
      if(dccsVec[iDcc]->getId() == dcIt->dccId){
	pair<int,int> inId(dcIt->dccChannelNum,dcIt->dccChannelId);
	dccInIds.push_back(inId);
      }
    }
    dccsVec[iDcc]->configure(preTrig,postTrig,dccInIds);
  }*/
}

/*void RpctSystem::connectOptLinks() {//trzeba poprawic!!!!!!!!!!!!!!!!!!!!!!!!
	OptLinkConnections& optLinkConnections = xmlBoardsSettings.getOptLinkConnections();
	for (OptLinkConnections::iterator olcIt = optLinkConnections.begin();
		  olcIt != optLinkConnections.end(); olcIt++) {		
		//unsigned int mlbId = masterLBsVec[iMLB]->getStdLb().getId(); 			
		for (unsigned int iTb = 0; iTb < tbsVec.size(); iTb++) {
			if(	tbsVec[iTb]->getId() == olcIt->tbId ) {
				tbsVec[iTb]->EnableOptLink(olcIt->tbInNum, true);
				LOG4CPLUS_INFO(logger, "the optLinkConnection: tb id "<<tbsVec[iTb]->getId()<<" tb inpunt num "<<olcIt->tbInNum);						
				break;
			}
		}						
	} 			
}*/

void RpctSystem::synchronizeOptLinks() {
 	for (unsigned int iMLB = 0; iMLB < masterLBsVec.size(); iMLB++) {
		masterLBsVec[iMLB]->synchronizeLink(); 	
		cout<<"masterLBsVec[iMLB]->synchronizeLink();"<<endl;
	} 
	
	for (unsigned int iTb = 0; iTb < tbsVec.size(); iTb++) {
		if(	tbsVec[iTb]->CheckOptLinksSynchronization() == false ) ;
				//throw TException(" opto links synchronization failed");
		cout<<"tbsVec[iTb]->CheckOptLinksSynchronization()"<<endl;
	} 			
	
 	for (unsigned int iMLB = 0; iMLB < masterLBsVec.size(); iMLB++) {
		masterLBsVec[iMLB]->normalLinkOperation(); 						
	} 
	 
	for (unsigned int iTb = 0; iTb < tbsVec.size(); iTb++) {
		if(	tbsVec[iTb]->CheckLinksOperation() == false ) ;
				//throw TException("CheckLinksOperation() == false ");
	}
	
	unsigned int testData = 0x12345678;
 	for (unsigned int iMLB = 0; iMLB < masterLBsVec.size(); iMLB++) {
		masterLBsVec[iMLB]->enableFindOptLinksSynchrDelay(true, testData); 						
	} 
	for (unsigned int iTb = 0; iTb < tbsVec.size(); iTb++) {
		if(	tbsVec[iTb]->FindOptLinksTLKDelay(testData) == false ) ;
				//throw TException(" opto links synchronization failed");
	}
	
/*	for (unsigned int iTb = 0; iTb < tbsVec.size(); iTb++) {
		if(	tbsVec[iTb]->CheckLinksOperation() == false ) ;
				//throw TException("CheckLinksOperation() == false ");
	}*/	
	
	for (unsigned int iMLB = 0; iMLB < masterLBsVec.size(); iMLB++) {
		masterLBsVec[iMLB]->enableFindOptLinksSynchrDelay(false, 0); 						
	} 
	
	for (unsigned int iMLB = 0; iMLB < masterLBsVec.size(); iMLB++) {
		masterLBsVec[iMLB]->enableTransmissionCheck(true, true); 						
	} 
	
	for (unsigned int iTb = 0; iTb < tbsVec.size(); iTb++) {
		if(	tbsVec[iTb]->FindOptLinksDelay() == false ) ;
				//throw TException(" opto links synchronization failed");
	}
}

/*void RpctSystem::configure() {
	unsigned int optosBcn0Delay = 65;
	//unsigned int optosBcn0Delay = 2;    
	unsigned int pacsBcn0Delay = optosBcn0Delay + 6;
	unsigned int tbSortBcn0Delay = optosBcn0Delay + 13;  
	unsigned int tcSortBcn0Delay = optosBcn0Delay + 22;
	unsigned int hsbBcn0Delay = tcSortBcn0Delay + 6;
	
	unsigned int rmbPreTriggerVal = 1;
	unsigned int rmbPostTriggerVal = 5;
	vector<unsigned int> rmbChanEna(tbsVec.size(), 0ul);
	dynamic_bitset<> maskedLinks(18, 0ul);
	maskedLinks[5] = true;
	maskedLinks[6] = true;
	rmbChanEna[0] = maskedLinks.to_ulong();
	
	maskedLinks.reset();
	maskedLinks[11] = true;
	maskedLinks[13] = true;
	maskedLinks[14] = true;
	rmbChanEna[1] = maskedLinks.to_ulong();
	
	//unsigned int rmbGohInvClk = 1;
	
	LOG4CPLUS_INFO(logger, "BC0 delays: optos " << optosBcn0Delay <<", PACs "<<pacsBcn0Delay<<", tbSort "<<tbSortBcn0Delay<<", tcSort "<<tcSortBcn0Delay);						
  	LOG4CPLUS_INFO(logger, "rmbPretriggerVal " << rmbPreTriggerVal);
  	LOG4CPLUS_INFO(logger, "rmbPostTriggerVal " << rmbPostTriggerVal); 
  	//LOG4CPLUS_INFO(logger, "rmbGohInvClk " << rmbGohInvClk);
  	
	connectOptLinks();
	    
	for (unsigned int iTB = 0; iTB < tbsVec.size(); iTB++) {							
		for(Optos::iterator optoIt=tbsVec[iTB]->GetOptos().begin(); 
								optoIt!=tbsVec[iTB]->GetOptos().end(); optoIt++){ 
			if((*optoIt) == NULL)
				continue
			
			(*optoIt)->writeWord(Opto::WORD_BCN0_DELAY, 0, optosBcn0Delay);	
			(*optoIt)->writeWord(Opto::WORD_DAQ_TIMER_TRG_DELAY, 0, 0ul);		
		}
		
		for(Pacs::iterator pacIt = tbsVec[iTB]->GetLdPacs().begin(); pacIt != tbsVec[iTB]->GetLdPacs().end(); pacIt++) {
			if((*pacIt) == NULL)
				continue;    		
				
			(*pacIt)->WriteWord(Pac::WORD_DAQ_TIMER_TRG_DELAY, 0, 0ul);
 			(*pacIt)->WriteWord(Pac::WORD_BCN0_DELAY, 0, pacsBcn0Delay); //UsedOptos.to_ulong());
		}
		if (tbsVec[iTB]->GetRmb() != NULL) {
	    	tbsVec[iTB]->GetRmb()->WriteWord(Rmb::WORD_DAQ_TIMER_TRG_DELAY, 0, 0ul);
			tbsVec[iTB]->GetRmb()->WriteWord(Rmb::WORD_RMB_PRETRG_VAL, 0, rmbPreTriggerVal);											
			tbsVec[iTB]->GetRmb()->WriteWord(Rmb::WORD_RMB_POSTTRG_VAL, 0,rmbPostTriggerVal);

			tbsVec[iTB]->GetRmb()->WriteWord(Rmb::WORD_RMB_CHAN_ENA, 0, rmbChanEna[iTB]);
			LOG4CPLUS_INFO(logger, " rmbChanEna "<< tbsVec[iTB]->getDescription()<<" "<<tbsVec[iTB]->getId()<<" "<< rmbChanEna[iTB]);
			
			tbsVec[iTB]->GetRmb()->WriteWord(Rmb::WORD_BCN0_DELAY, 0, pacsBcn0Delay); 
			tbsVec[iTB]->GetRmb()->WriteBits(Rmb::BITS_STATUS_TRG_RMB_SEL, IDiagnosable::tdsL1A); 			
		}
		
		///-----------------------GbSort-----------------
		if(tbsVec[iTB]->GetGbSort() != NULL) {
			tbsVec[iTB]->GetGbSort()->WriteWord(TTb3GbSort::WORD_BCN0_DELAY, 0, tbSortBcn0Delay);
			tbsVec[iTB]->GetGbSort()->WriteWord(TTb3GbSort::WORD_DAQ_TIMER_TRG_DELAY, 0, 0ul);
		}
	}
	
	for(unsigned int iTc = 0; iTc < tcSortsVec.size(); iTc++) {
		tcSortsVec[iTc]->WriteWord(TTCSortTCSort::WORD_BCN0_DELAY, 0, tcSortBcn0Delay);
		tcSortsVec[iTc]->WriteWord(TTCSortTCSort::WORD_DAQ_TIMER_TRG_DELAY, 0, 0ul);
	}
	
	
	for(unsigned int iHsb = 0; iHsb < hsbSortsVec.size(); iHsb++) {
		hsbSortsVec[iHsb]->WriteWord(THsbSortHalf::WORD_BCN0_DELAY, 0, hsbBcn0Delay);
		hsbSortsVec[iHsb]->WriteWord(THsbSortHalf::WORD_DAQ_TIMER_TRG_DELAY, 0, 0ul);
	} 
	if(fsbSortFinal != NULL) {
		//fsbSortFinal->Reset();
	}	
};*/

void RpctSystem::enableAllRMBChannels(bool enalbe) {
	for (unsigned int iTB = 0; iTB < tbsVec.size(); iTB++) {							
		if (tbsVec[iTB]->GetRmb() != NULL) {
			if(enalbe)
				tbsVec[iTB]->GetRmb()->writeWord(Rmb::WORD_RMB_CHAN_ENA, 0, 0ul); 
			else 
				tbsVec[iTB]->GetRmb()->writeWord(Rmb::WORD_RMB_CHAN_ENA, 0, 0x3ffff);
		}
	}
}

void RpctSystem::resetRMBs() {
	for (unsigned int iTB = 0; iTB < tbsVec.size(); iTB++) {							
		if (tbsVec[iTB]->GetRmb() != NULL) {
			tbsVec[iTB]->GetRmb()->writeBits(Rmb::BITS_STATUS_RMB_RESET, 0); 
			tbsleep(1000);
			tbsVec[iTB]->GetRmb()->writeBits(Rmb::BITS_STATUS_RMB_RESET, 1); 
		}
	}
}

void RpctSystem::enableTransmissionCheck(bool enableBCNcheck, bool enableDataCheck) {
	for (unsigned int iMLB = 0; iMLB < masterLBsVec.size(); iMLB++) {
		masterLBsVec[iMLB]->enableTransmissionCheck(enableBCNcheck, enableDataCheck); 						
	}
	for (unsigned int iTb = 0; iTb < tbsVec.size(); iTb++) {
		tbsVec[iTb]->EnableTransmissionCheck(enableBCNcheck, enableDataCheck);
	} 
	
	for(unsigned int iTc = 0; iTc < tcSortsVec.size(); iTc++) {
		tcSortsVec[iTc]->EnableTransmissionCheck(enableBCNcheck, enableDataCheck);
	}
	
	for(unsigned int iHsb = 0; iHsb < hsbSortsVec.size(); iHsb++) {
		hsbSortsVec[iHsb]->EnableTransmissionCheck(enableBCNcheck, enableDataCheck);
	} 
	if(fsbSortFinal != NULL) {
		//fsbSortFinal->Reset();
	}	
}

void RpctSystem::resetRecErrorCnt() {
	for (unsigned int iTB = 0; iTB < tbsVec.size(); iTB++) {							
		for(Optos::iterator optoIt=tbsVec[iTB]->GetOptos().begin(); 
								optoIt!=tbsVec[iTB]->GetOptos().end(); optoIt++){ 
			if((*optoIt) == NULL)
				continue;
			
			(*optoIt)->writeWord(Opto::WORD_REC_ERROR_COUNT, 0, 0ul);
			
		}
		
		for(Pacs::iterator pacIt = tbsVec[iTB]->GetLdPacs().begin(); pacIt != tbsVec[iTB]->GetLdPacs().end(); pacIt++) {
			if((*pacIt) == NULL)
				continue;    		
 			(*pacIt)->writeWord(Pac::WORD_REC_ERROR_COUNT, 0, 0ul); 
		}
		
		if (tbsVec[iTB]->GetRmb() != NULL) {
    		tbsVec[iTB]->GetRmb()->writeWord(Rmb::WORD_REC_ERROR_COUNT, 0, 0ul);

		
			tbsVec[iTB]->GetRmb()->writeWord(Rmb::WORD_REC_ERROR_COUNT, 0, 0ul);
			
			tbsVec[iTB]->GetRmb()->writeBits(Rmb::BITS_STATUS_RMB_CNT_RESET, 1);
			tbsleep(100);
			tbsVec[iTB]->GetRmb()->writeBits(Rmb::BITS_STATUS_RMB_CNT_RESET, 0);
		}  
		
		///-----------------------GbSort-----------------
		if(tbsVec[iTB]->GetGbSort() != NULL) {
			tbsVec[iTB]->GetGbSort()->writeWord(GbSort::WORD_REC_ERROR_COUNT, 0, 0ul);
		}
		
	}
	
	for(unsigned int iTc = 0; iTc < tcSortsVec.size(); iTc++) {
		tcSortsVec[iTc]->writeWord(TTCSortTCSort::WORD_REC_ERROR_COUNT, 0, 0ul);
	}
	
	for(unsigned int iHsb = 0; iHsb < hsbSortsVec.size(); iHsb++)
		hsbSortsVec[iHsb]->writeWord(THsbSortHalf::WORD_REC_ERROR_COUNT, 0, 0ul);
};

void RpctSystem::printRecErrorCnt() {   	
	for (unsigned int iTB = 0; iTB < tbsVec.size(); iTB++) {
		cout<<" WORD_REC_ERROR_COUNT: "<<tbsVec[iTB]->getDescription()<<" "<<tbsVec[iTB]->getId()<<endl;
		for(Optos::iterator optoIt=tbsVec[iTB]->GetOptos().begin(); 
								optoIt!=tbsVec[iTB]->GetOptos().end(); optoIt++){
			if((*optoIt) == NULL)
				continue;
			cout<<(*optoIt)->getDescription()<<" ";		
    		for(unsigned int iLink = 0; iLink < 3; iLink++) {
    			cout<<"link "<<iLink<<" "<<setw(12)<<(*optoIt)->readWord(Opto::WORD_REC_ERROR_COUNT, iLink)<<",      ";	    			    		
    		}    	
    		cout<<endl;
		}
		
		for(Pacs::iterator pacIt = tbsVec[iTB]->GetLdPacs().begin(); pacIt != tbsVec[iTB]->GetLdPacs().end(); pacIt++) {
			if((*pacIt) == NULL)
				continue;    		
 			cout<<(*pacIt)->getDescription()<<" "<<setw(12)<<(*pacIt)->readWord(Pac::WORD_REC_ERROR_COUNT, 0)<<endl;	    			    		
		}
		
		if (tbsVec[iTB]->GetRmb() != NULL) {
			cout<<tbsVec[iTB]->GetRmb()->getDescription()<<endl;
			cout<<"   RECEIVER_ERROR_COUNT  "<<tbsVec[iTB]->GetRmb()->readWord(Rmb::WORD_REC_ERROR_COUNT, 0)<<endl;
			cout<<"   RMB_CHMB3_ERR_CNT     "<<tbsVec[iTB]->GetRmb()->readWord(Rmb::WORD_RMB_CHMB3_ERR_CNT, 0)<<endl;
			cout<<"   RMB_DDM30_ERR_CNT     "<<tbsVec[iTB]->GetRmb()->readWord(Rmb::WORD_RMB_DDM30_ERR_CNT, 0)<<endl;
			cout<<"   RMB_DDM31_ERR_CNT     "<<tbsVec[iTB]->GetRmb()->readWord(Rmb::WORD_RMB_DDM31_ERR_CNT, 0)<<endl;
			cout<<"           RMB_DEL_ERR_CNT    RMB_DDM_ERR_CNT"<<endl;
			for(unsigned int iLinkNum = 0; iLinkNum < Rmb::TB_LINK_NUM; iLinkNum++) {
				cout<<"   "<<dec<<setw(2)<<iLinkNum<<" "<<setw(12)<<tbsVec[iTB]->GetRmb()->readWord(Rmb::WORD_RMB_DEL_ERR_CNT, iLinkNum);
				cout<<"   "<<setw(12)<<tbsVec[iTB]->GetRmb()->readWord(Rmb::WORD_RMB_DDM_ERR_CNT, iLinkNum)<<endl;
			}
		}		
		if(tbsVec[iTB]->GetGbSort() != NULL) {
			cout<<tbsVec[iTB]->GetGbSort()->getDescription()<<" "<<tbsVec[iTB]->GetGbSort()->readWord(GbSort::WORD_REC_ERROR_COUNT, 0)<<endl;
		}
				
	} 
	for(unsigned int iTc = 0; iTc < tcSortsVec.size(); iTc++) {		
		cout<<tcSortsVec[iTc]->getDescription()<<" "<<tcSortsVec[iTc]->getId()<<" "<<tcSortsVec[iTc]->readWord(TTCSortTCSort::WORD_REC_ERROR_COUNT, 0)<<endl;
	}	
	for(unsigned int iHsb = 0; iHsb < hsbSortsVec.size(); iHsb++) {
		cout<<hsbSortsVec[iHsb]->getDescription()<<" "<<hsbSortsVec[iHsb]->getId()<<" "<<hsbSortsVec[iHsb]->readWord(THsbSortHalf::WORD_REC_ERROR_COUNT, 0)<<endl;
		
	} 
	if(fsbSortFinal != NULL) {
		//fsbSortFinal->Reset();
	}		
}

void RpctSystem::printLinksConnection(std::ostream* ostr) {
	for (unsigned int iMLB = 0; iMLB < masterLBsVec.size(); iMLB++) {
		(*ostr)<<masterLBsVec[iMLB]->getCrate()->getDescription()<<" "<<masterLBsVec[iMLB]->getDescription()<<" id "<<masterLBsVec[iMLB]->getId()<<endl;
		//masterLBsVec[iMLB]->enableTransmissionCheck(enable); 		
		
		masterLBsVec[iMLB]->getSynCoder().writeBits(SynCoder::BITS_SEND_TEST_ENA, 1);
        masterLBsVec[iMLB]->getSynCoder().writeWord(SynCoder::WORD_SEND_TEST_DATA, 0, 0x12345678);		
        
        for (unsigned int iTB = 0; iTB < tbsVec.size(); iTB++) {        
			(*ostr)<<tbsVec[iTB]->getDescription()<<" "<<tbsVec[iTB]->getId()<<endl;
			int linkInNum = 0;
			for(Optos::iterator optoIt=tbsVec[iTB]->GetOptos().begin(); 
									optoIt!=tbsVec[iTB]->GetOptos().end(); optoIt++){
				if((*optoIt) == NULL)
					continue;
					
	    		for(unsigned int iLink = 0; iLink < 3; iLink++) {
	    			(*ostr)<<(*optoIt)->getDescription()<<" ";	
	    			(*ostr)<<"link "<<iLink<<setw(2)<<" linkInNum "<< linkInNum<<" "<<setw(12)<<(*optoIt)->readWord(Opto::WORD_REC_TEST_DATA, iLink)<<endl;
	    			linkInNum++;	    			    		
	    		}    	
			}
        }
        
        masterLBsVec[iMLB]->getSynCoder().writeBits(SynCoder::BITS_SEND_TEST_ENA, 0);
        masterLBsVec[iMLB]->getSynCoder().writeWord(SynCoder::WORD_SEND_TEST_DATA, 0, 0ul);			
	}
}

/*void RpctSystem::startReadOutOnHSBs() {
	for(unsigned int iHsb = 0; iHsb < hsbSortsVec.size(); iHsb++) {
		hsbSortsVec[iHsb]->GetDiagnosticReadout().GetDiagCtrl().Start();	
	}
}*/
