#include "rpct/hardwareTests/StatisticDiagManager.h"
#include "rpct/ii/IHardwareItem.h"
#include "rpct/std/tbutil.h"

#include "TStyle.h"

using namespace std;
using namespace log4cplus;

namespace rpct {

Logger StatisticDiagManager::logger = Logger::getInstance("StatisticDiagManager");

StatisticDiagManager::StatisticDiagManager(string name, string histosDir, std::string picturesDir, IDiagnosable::TDiagCtrlVector& diagCtrls, HistoMgrVector& histoManagers) {
    diagCtrls_ = diagCtrls;
    histoManagers_ = histoManagers;
    name_ = name;
    histosDir_ = histosDir;
    picturesDir_ = picturesDir;

    histosArray_.SetOwner();

    interation_ = 0;
}

StatisticDiagManager::~StatisticDiagManager() {
    //delete outFile;
}

void StatisticDiagManager::initialize(int runNumber) {
    time_t timer = time(NULL);
    tm* tblock = localtime(&timer);
    tblock->tm_mon += 1;

    std::stringstream sfile;
    sfile <<histosDir_<<"/Histos_"<<name_<<"_run_"<<runNumber<<"_"<<tblock->tm_year + 1900<< '_'
            << tblock->tm_mon << '_' << tblock->tm_mday << "__" << tblock->tm_hour << '_'
            << tblock->tm_min << '_' << tblock->tm_sec<<".root";

    LOG4CPLUS_INFO(logger, "\n monitorHistograms. Creating file: " << sfile.str() << endl);

    outFile.Open(sfile.str().c_str(), "new");
    gettimeofday(&startTime, 0);
    gStyle->SetTimeOffset(startTime.tv_sec);
}

void StatisticDiagManager::resetAndConfigHistos(uint64_t countersLimit, IDiagCtrl::TTriggerType triggerType, uint32_t triggerDelay) {
    for (IDiagnosable::TDiagCtrlVector::iterator iDC = diagCtrls_.begin(); iDC != diagCtrls_.end(); ++iDC) {
        (dynamic_cast<IDiagnosable&>((*iDC)->GetOwner())).ConfigureDiagnosable(IDiagnosable::tdsNone, 0ul);
        (*iDC)->Reset();
        (*iDC)->Configure(countersLimit, triggerType, 0);
    }
    LOG4CPLUS_WARN(logger, "resetAndConigHistos: Diagnosable was configured: ConfigureDiagnosable(IDiagnosable::tdsNone, 0ul)");

    for (HistoMgrVector::const_iterator iHM = histoManagers_.begin(); iHM != histoManagers_.end(); ++iHM) {
        (*iHM)->Reset();
    }
}

void StatisticDiagManager::startHistos() {
    for (IDiagnosable::TDiagCtrlVector::const_iterator iDC = diagCtrls_.begin(); iDC != diagCtrls_.end(); ++iDC) {
        (*iDC)->Start();
    }
    //gettimeofday(&startTime, 0);
}

void StatisticDiagManager::stopHistos() {
    for (IDiagnosable::TDiagCtrlVector::const_iterator iDC = diagCtrls_.begin(); iDC != diagCtrls_.end(); ++iDC) {
        (*iDC)->Stop();
    }
}

void StatisticDiagManager::readoutAndAnalyse(double countingPeriod) {
    timeval timv;
    gettimeofday(&timv, 0);
    for (HistoMgrVector::const_iterator iHM = histoManagers_.begin(); iHM != histoManagers_.end(); ++iHM) {
        int binCount = (*iHM)->GetBinCount();

        string name = (dynamic_cast<IDevice&>((*iHM)->GetOwner())).getBoard().getDescription() + "_" + (dynamic_cast<IDevice&>((*iHM)->GetOwner())).getDescription() + "_" + (*iHM)->GetName();
        string title = name + " " + rpct::toString(timv.tv_sec) + " " + rpct::toString(timv.tv_usec);

        TH1I* h1 = new TH1I(name.c_str(), title.c_str(), binCount , 0, binCount);
        histosArray_.Add(h1);
        (*iHM)->Refresh();

        for (int iCh = 0; iCh < binCount; iCh++) {
            int binVal = (*iHM)->GetLastData()[iCh];
            h1->Fill(iCh, binVal);
        }
    }

    histosArray_.Write();
    histosArray_.Clear();
}

}
