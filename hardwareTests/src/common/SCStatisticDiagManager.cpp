#include "rpct/hardwareTests/SCStatisticDiagManager.h"
#include "rpct/ii/IHardwareItem.h"
#include "rpct/std/tbutil.h"
#include "rpct/devices/FixedHardwareSettings.h"
#include "rpct/hardwareTests/RootPicturesWorkaround.h"
#include "TFrame.h"
#include "TROOT.h"
#include <sys/time.h>

using namespace std;
using namespace log4cplus;

namespace rpct {
//Logger SCStatisticDiagManager::logger = Logger::getInstance("StatisticDiagManager");

SCStatisticDiagManager::SCStatisticDiagManager(std::string name, std::string picturesDir, SorterCrate* sc):
    StatisticDiagManagerBase(name, picturesDir), sc_(sc),
    fsBarrelRateLegend1_(0.5, 0.88, 0.1, 1.),
    fsEndcapRateLegend1_(0.5, 0.88, 0.1, 1.),
    fsBarrelRateLegend2_(0.9, 0.88, 0.5, 1.),
    fsEndcapRateLegend2_(0.9, 0.88, 0.5, 1.),
    hsLegend1_(0.5, 0.92, 0.3, 1.), hsLegend2_(0.7, 0.92, 0.5, 1.),
    barrelPaveText_(0.98, 0.1, 0.99, 0.9, "BRNDC"),
    endcapPaveText_(0.98, 0.1, 0.99, 0.9, "BRNDC")
    {
    string canvasName = name_ + "_hsRate";
    TCanvas* canvas1 = new TCanvas(canvasName.c_str(), canvasName.c_str(), 300,10,1000,200);

    canvases_.push_back(canvas1);
    picturesNames_.push_back(canvasName + ".png");
    picturesDirs_.push_back(picturesDir_ + "/" + picturesNames_.back());
    //cout<<"debuuuuuuuug "<<dec<<__FILE__<<" "<<__LINE__<<" "<<__FUNCTION__<<" creating canvas "<<canvasName<<endl<<flush;
    LOG4CPLUS_INFO(logger,name_<<" creating canvas "<<canvasName<< endl);

    //canvas->SetFillColor(21);
    //    canvas1->SetLeftMargin(0.04);
    //    canvas1->SetRightMargin(0.01);
    //    canvas1->SetTopMargin(0.06);
    //    canvas1->SetBottomMargin(0.3);

    canvas1->cd();
    canvas1->SetGrid();
    canvas1->SetLeftMargin(0.05);
    canvas1->SetRightMargin(0.02);
    canvas1->SetTopMargin(0.1);

    int tcBinsCnt = 12 *4;
    hMaxRateHist_ = new TH1D("hMaxRateHist_", "", tcBinsCnt , -0.5, tcBinsCnt + 0.5);
    hsMeanRateHist_ = new TH1D("hsMeanRateHist_", "", tcBinsCnt , -0.5, tcBinsCnt + 0.5);;

    hMaxRateHist_->SetStats(kFALSE);
    hMaxRateHist_->GetXaxis()->SetLabelSize(0.12);
    hMaxRateHist_->GetYaxis()->SetLabelSize(0.06);
    hMaxRateHist_->SetFillColor(kRed);

    hsMeanRateHist_->SetStats(kFALSE);
    hsMeanRateHist_->GetXaxis()->SetLabelSize(0.12);
    hsMeanRateHist_->GetYaxis()->SetLabelSize(0.06);
    hsMeanRateHist_->SetFillColor(kBlue);

    int iCh = -2;
    //hMaxRateHist_->GetXaxis()->SetBinLabel(iCh+=4, "TC_11");
    hMaxRateHist_->GetXaxis()->SetBinLabel(iCh+=4, "TC_0");
    hMaxRateHist_->GetXaxis()->SetBinLabel(iCh+=4, "TC_1");
    hMaxRateHist_->GetXaxis()->SetBinLabel(iCh+=4, "TC_2");
    hMaxRateHist_->GetXaxis()->SetBinLabel(iCh+=4, "TC_3");
    hMaxRateHist_->GetXaxis()->SetBinLabel(iCh+=4, "TC_4");
    hMaxRateHist_->GetXaxis()->SetBinLabel(iCh+=4, "TC_5");
    //hMaxRateHist_->GetXaxis()->SetBinLabel(iCh+=4, "TC_6");

    //hMaxRateHist_->GetXaxis()->SetBinLabel(iCh+=4, "TC_5");
    hMaxRateHist_->GetXaxis()->SetBinLabel(iCh+=4, "TC_6");
    hMaxRateHist_->GetXaxis()->SetBinLabel(iCh+=4, "TC_7");
    hMaxRateHist_->GetXaxis()->SetBinLabel(iCh+=4, "TC_8");
    hMaxRateHist_->GetXaxis()->SetBinLabel(iCh+=4, "TC_9");
    hMaxRateHist_->GetXaxis()->SetBinLabel(iCh+=4, "TC_10");
    hMaxRateHist_->GetXaxis()->SetBinLabel(iCh+=4, "TC_11");
    //hMaxRateHist_->GetXaxis()->SetBinLabel(iCh+=4, "TC_0");
    hMaxRateHist_->LabelsOption("h", "X");
    hMaxRateHist_->Draw("bar2");
    hsMeanRateHist_->Draw("samebar2");

    hsLegend1_.AddEntry(hsMeanRateHist_, "mean rate [Hz]", "F");
    hsLegend1_.Draw();

    hsLegend2_.AddEntry(hMaxRateHist_, "max (peak) rate [Hz]", "F");
    hsLegend2_.Draw();

    HSHistogramRecord* null1 = 0;
    hsHistogramRecord_.push_back(null1);
    hsHistogramRecord_.push_back(null1);
    for(unsigned int iH = 0; iH < sc->getHSBs().size(); iH++) {
        HSHistogramRecord* hsHistogramRecord = new HSHistogramRecord(&sc->getHSBs()[iH]->GetHsbSortHalf().getStatisticsDiagCtrl(), sc->getHSBs()[iH]->GetHsbSortHalf().getHistoManagers());
        histogramRecords_.push_back(hsHistogramRecord);
        hsHistogramRecord_[sc->getHSBs()[iH]->getNumber()] = hsHistogramRecord;
    }

    //---------------------------------------FSB--------------
    canvasName = name_ + "_fsRateGraphs";
    canvas1 = new TCanvas(canvasName.c_str(), canvasName.c_str(), 300,10,1000,500);
    canvas1->Divide(1, 2, 0.001, 0.001); //<<<<<<<<<
    fsbRateCanvas_ = canvas1;

    canvas1->cd(1)->SetGrid();
    canvas1->cd(1)->SetLeftMargin(0.05);
    canvas1->cd(1)->SetRightMargin(0.02);
    canvas1->cd(1)->SetTopMargin(0.15);
    canvas1->cd(2)->SetGrid();
    canvas1->cd(2)->SetLeftMargin(0.05);
    canvas1->cd(2)->SetRightMargin(0.02);
    canvas1->cd(2)->SetTopMargin(0.15);

    canvases_.push_back(canvas1);
    picturesNames_.push_back(canvasName + ".png");
    picturesDirs_.push_back(picturesDir_ + "/" + picturesNames_.back());
    //cout<<"debuuuuuuuug "<<dec<<__FILE__<<" "<<__LINE__<<" "<<__FUNCTION__<<" creating canvas "<<canvasName<<endl<<flush;
    LOG4CPLUS_INFO(logger,name_<<" creating canvas "<<canvasName<< endl);

    canvasName = name_ + "_fsHisits";
    TCanvas* canvas2 = new TCanvas(canvasName.c_str(), canvasName.c_str(),200,10,1000,1000);
    canvas2->SetGrid();
    canvas2->Divide(4, 4, 0.001, 0.001); //<<<<<<<<<
    canvases_.push_back(canvas2);
    picturesNames_.push_back(canvasName + ".png");
    picturesDirs_.push_back(picturesDir_ + "/" + picturesNames_.back());
    //cout<<"debuuuuuuuug "<<dec<<__FILE__<<" "<<__LINE__<<" "<<__FUNCTION__<<" creating canvas "<<canvasName<<endl<<flush;

    for(unsigned int iHist = 0; iHist < sc->getFsb()->GetFsbSortFinal().getHistoManagers().size(); iHist++) {
        string nameH = sc->getFsb()->getDescription() + "_" + sc->getFsb()->GetFsbSortFinal().getHistoManagers()[iHist]->GetName();
        //unsigned int binCount = sc->getFsb()->GetFsbSortFinal().getHistoManagers()[iHist]->GetBinCount();
        //TH1I* h1 = new TH1I(nameH.c_str(), nameH.c_str(), binCount , -0.5, binCount-0.5);

        TGraph* graph = new TGraph();
        rateGraphs_.push_back(graph);
        graph->SetLineColor(2 + iHist);
        graph->SetLineWidth(1);
        graph->SetMarkerColor(2 + iHist);
        graph->SetMarkerStyle(23);
        graph->SetMarkerSize(0.4);
        //graph->GetXaxis()->SetTitle("time [s]");
        //graph->GetYaxis()->SetTitle("rate [Hz]");

        //graph->GetXaxis()->SetTimeDisplay(1);
        //graph->GetXaxis()->SetTimeFormat("%H:%M:%S");

        ostringstream title;
        if(iHist < 4) {
            ostringstream ostr;
            ostr<<"barrel rate, at least "<<iHist + 1<<" muons";
            graph->SetTitle(ostr.str().c_str());
            //graph->SetTitle("");
            if(iHist < 2) {
                rateGraphLegendEntries_.push_back(fsBarrelRateLegend1_.AddEntry(graph, "", "LP"));
            }
            else{
                rateGraphLegendEntries_.push_back(fsBarrelRateLegend2_.AddEntry(graph, "", "LP"));
            }
            title<<"barrel muon "<<iHist + 1;
        }
        else if(iHist < 8) {
            ostringstream ostr;
            ostr<<"endcap rate, at leas "<<iHist%4 +1<<" muons";
            graph->SetTitle(ostr.str().c_str());
            //graph->SetTitle("");
            if(iHist < 6) {
                rateGraphLegendEntries_.push_back(fsEndcapRateLegend1_.AddEntry(graph, "", "LP"));
            }
            else {
                rateGraphLegendEntries_.push_back(fsEndcapRateLegend2_.AddEntry(graph, "", "LP"));
            }

            title<<"endcap muon "<<iHist%4 + 1;
        }

        canvas1->cd(iHist/4 + 1);
        canvas1->cd(iHist/4 + 1)->SetLogy();
        if(iHist%4 == 0) { //we assume the order og the histograms in the histoManagers_, to avoid checking names (for better efficiency)
        	graph->Draw("AP");
        	graph->GetXaxis()->SetTitle("time");
            graph->GetYaxis()->SetTitle("rate [Hz]");
            graph->GetYaxis()->SetTitleOffset(0.5);
            graph->GetXaxis()->SetLabelSize(0.06);
            graph->GetHistogram()->SetTitle("");
            graph->SetMinimum(0.);
        }
        else {
            graph->Draw("P");
        }

        //legends
        if(iHist == 3) {
            canvas1->cd(1);
            fsBarrelRateLegend1_.Draw();
            fsBarrelRateLegend2_.Draw();
        }
        else if(iHist == 7) {
            canvas1->cd(2);
            fsEndcapRateLegend1_.Draw();
            fsEndcapRateLegend2_.Draw();
        }

        string namePt = sc->getFsb()->GetFsbSortFinal().getHistoManagers()[iHist]->GetName() + "_ptCode";
        string titlePt = "ptCode in " + title.str();
        ptCodeHistos_.push_back(new TH1I(namePt.c_str(), titlePt.c_str(), FixedHardwareSettings::PT_CODE_MAX +1, -0.5, FixedHardwareSettings::PT_CODE_MAX + 0.5));
        ptCodeHistos_.back()->SetFillColor(kBlue);
        ptCodeHistos_.back()->SetStats(kFALSE);
        ptCodeHistos_.back()->SetXTitle("ptCode");

        canvas2->cd(iHist + 1);
        ptCodeHistos_.back()->Draw();

        string nameQ = sc->getFsb()->GetFsbSortFinal().getHistoManagers()[iHist]->GetName() + "_quality";
        string titleQ = "quality in " + title.str();
        qualityHistos_.push_back(new TH1I(nameQ.c_str(), titleQ.c_str(), FixedHardwareSettings::QUALITY_MAX +1, -0.5, FixedHardwareSettings::QUALITY_MAX + 0.5));
        qualityHistos_.back()->SetFillColor(kGreen);
        qualityHistos_.back()->SetStats(kFALSE);
        qualityHistos_.back()->SetXTitle("quality");
        canvas2->cd(iHist + 8 + 1);
        qualityHistos_.back()->Draw();
    }

    barrelPaveText_.AddText(" ")->SetTextAlign(32);
    barrelPaveText_.AddText(" ")->SetTextAlign(32);
    barrelPaveText_.GetLine(1)->SetTextColor(kRed);
    barrelPaveText_.GetLine(1)->SetTextSize(0.2);

    endcapPaveText_.AddText(" ")->SetTextAlign(32);
    endcapPaveText_.AddText(" ")->SetTextAlign(32);
    endcapPaveText_.GetLine(1)->SetTextColor(kRed);
    endcapPaveText_.GetLine(1)->SetTextSize(0.2);

    fsHistogramRecord_ = new FSHistogramRecord(&sc->getFsb()->GetFsbSortFinal().getStatisticsDiagCtrl(), sc->getFsb()->GetFsbSortFinal().getHistoManagers());
    histogramRecords_.push_back(fsHistogramRecord_);

    RootPicturesWorkaround::crateRootMacro(name_, picturesDir_);
 }


SCStatisticDiagManager::~SCStatisticDiagManager() {
    for(unsigned int iC = 0; iC < canvases_.size(); iC++) {
        delete canvases_[iC];
    }
    canvases_.clear();

    for (unsigned int i = 0; i < histogramRecords_.size(); i++) {
        delete histogramRecords_[i];
    }

    hsHistogramRecord_.clear();

    delete hMaxRateHist_;
    delete hsMeanRateHist_;

    for(unsigned int iH = 0; iH < rateGraphs_.size(); iH++) {
        delete rateGraphs_[iH];
    }
    for(unsigned int iHist = 0; iHist < ptCodeHistos_.size(); iHist++) {
        delete ptCodeHistos_[iHist];
        delete qualityHistos_[iHist];
    }
}

void SCStatisticDiagManager::createBranches(TTree& tree) {
    for (unsigned int i = 0; i < histogramRecords_.size(); i++)  {
        std::string name = histogramRecords_[i]->getDiagCtrl()->GetOwner().getBoard().getDescription() + "_Hist";
        histogramRecords_[i]->createBranch(tree, name);
    }
}

/*
 * It is needed to clear the counters after each readout, as they are to short for the high luminosity rates
 * if the reset is done automatically or the counters are longer, remove this function and back to the StatisticDiagManagerBase::readout
 */
bool SCStatisticDiagManager::readout(bool startAfterReadout) {
    time_t timer = 0;time(NULL);
    for (unsigned int i = 0; i < histogramRecords_.size(); i++)  {
        //histogramRecords_[iLB]->timerValuePrev_ = histogramRecords_[iLB]->timerValue_;
        //w tej implementcji stop zatrzymuje counter, wiec wartosc countera pokazuje, ile bx histogram liiczyl
        //jesli krzysiu zrobi odczytuwanie bez zatrzymywania, trzeba bedzie do tej opcji  wrocic
        uint64_t counter = 0;
        unsigned char* p = (unsigned char*)(&counter);
        dynamic_cast<TDiagCtrl*>(histogramRecords_[i]->getDiagCtrl())->GetCounterValue(p);
        histogramRecords_[i]->setTimerValue(counter);
        histogramRecords_[i]->setTimerValueIntegrated(histogramRecords_[i]->getTimerValueIntegrated() + counter);

        histogramRecords_[i]->getDiagCtrl()->Stop();
        timer = time(NULL);
        histogramRecords_[i]->setStopTime(timer);

        for (unsigned int iM = 0; iM < histogramRecords_[i]->getHistoManagers().size(); iM++) {
            histogramRecords_[i]->getHistoManagers()[iM]->Refresh(); //refresh - counter value readout
            histogramRecords_[i]->getHistoManagers()[iM]->Reset(); //<<<<<<<<<<<<<<!!!!!!!!!!!!!!!
        }

        histogramRecords_[i]->setStartTime(histogramRecords_[i]->getStartTimeBuf());
        if (startAfterReadout) {
            histogramRecords_[i]->getDiagCtrl()->Start();
            timer = time(NULL);
            histogramRecords_[i]->setStartTimeBuf(timer);
        }
    }

    return true;
}

Monitorable::MonitorStatusList& SCStatisticDiagManager::analyse() {
	monitorStatusList_.clear();
	//--------------------------HS------------------------------
    for(unsigned int iH = 0; iH < hsHistogramRecord_.size(); iH++) {
        if(hsHistogramRecord_[iH] == 0) {
            continue;
        }
        unsigned int binCount = hsHistogramRecord_[iH]->getHistoManagers().front()->GetBinCount();
        double countingTime = hsHistogramRecord_[iH]->getTimerValue() / 40000000.;
        double countingTimeInt = (hsHistogramRecord_[iH]->getTimerValueIntegrated()) / 40000000.;
        for (unsigned int iCh = 0; iCh < binCount; iCh++) {
            unsigned int binVal = hsHistogramRecord_[iH]->getHistoManagers().front()->GetLastData()[iCh];

            if(binVal == 0xffffff) {
                LOG4CPLUS_WARN(logger, hsHistogramRecord_[iH]->getDiagCtrl()->GetOwner().getBoard().getDescription() + " hsb counter overflow in bin "<< iCh<< endl);
            }

            double lastRate = ((double)binVal) / countingTime;
            hsHistogramRecord_[iH]->getBranchData().binsHSMuonCounters_[iCh] += binVal;//binVal is now 24 bits, integrated value should fit into the 32 bit int with up to 10 kHz/TC during 24h

            //LOG4CPLUS_INFO(logger,name_<<" hsHistogramRecord_["<<iH<<"]["<<iCh<<"] "<<hsHistogramRecord_[iH]->getBranchData().binsHSMuonCounters_[iCh]<<" binVal "<<binVal<<endl);

            //int binNum = iCh+1 + iH * HSHistogramRecord::binsCnt;
            if(iH == 1 && iCh/4 == 0) {

            }
            else if(iH == 1 && iCh/4 == 7) {

            }

            int binNum = iCh -4;
            if(iH == 1) {
                binNum += 6 * 4;
            }
            binNum++; //biny liczone sa od 1
            if(iCh >= 4 && iCh < 28) {
                hsMeanRateHist_->SetBinContent(binNum, (hsHistogramRecord_[iH]->getBranchData().binsHSMuonCounters_[iCh]) / countingTimeInt );
                if(hMaxRateHist_->GetBinContent(binNum) < lastRate)
                    hMaxRateHist_->SetBinContent(binNum, lastRate);
            }
        }
    }

    //--------------------------FS------------------------------
    bool counterOverflow = false;
    for(unsigned int iHist = 0; iHist < fsHistogramRecord_->getHistoManagers().size(); iHist++)  {
        unsigned int binCount = fsHistogramRecord_->getHistoManagers()[iHist]->GetBinCount();

/*        for(unsigned  int iBin = 1; iBin <= FixedHardwareSettings::PT_CODE_MAX +1; iBin++) {
            ptCodeHistos_[iHist]->SetBinContent(iBin, 0);
        }
        for(unsigned  int iBin = 1; iBin <= FixedHardwareSettings::QUALITY_MAX +1; iBin++) {
            qualityHistos_[iHist]->SetBinContent(iBin, 0);
        } not needed as the counters are reset every readout*/

        uint64_t integralVal =  0;
        for (unsigned int iCh = 1; iCh < binCount; iCh++) { //in the iCh = 0 empty muons are counted
            unsigned int binVal = fsHistogramRecord_->getHistoManagers()[iHist]->GetLastData()[iCh];
            if(binVal == 0xffffff) {//in new FSB firmware this will be 32 bits
                LOG4CPLUS_WARN(logger, fsHistogramRecord_->getHistoManagers()[iHist]->GetName()<<" counter in channel "<<iCh<<" overflow!!!");
                counterOverflow = true;
            }

            fsHistogramRecord_->getBranchData().binsFSMuonCounters[iCh + iHist * FSHistogramRecord::binsCnt] += binVal;
            //binVal is now 24 bits, integrated value should fit into the 32 bit int with up to 10 kHz/bin during 24h

            ptCodeHistos_[iHist]->Fill( (iCh & 0x3e) >> 1, binVal);
            qualityHistos_[iHist]->Fill( (iCh & 0x1c0) >> 6, binVal);

            integralVal +=  binVal;
        }

        if(counterOverflow) {
            if(iHist < 4) {
                barrelPaveText_.GetLine(1)->SetText(0, 0, "Counters overflow!!!");
                fsbRateCanvas_->cd(1);
                barrelPaveText_.Draw();
            }
            else if(iHist < 8) {
                endcapPaveText_.GetLine(1)->SetText(0, 0, "Counters overflow!!!");
                fsbRateCanvas_->cd(2);
                endcapPaveText_.Draw();
            }
        }
       // double lastRate = ((double)(integralVal -  fsHistogramRecord_->fsMuonCountersInteg_[iHist]))/fsHistogramRecord_->getTimerValue()*40000000.;
        double lastRate = ((double)integralVal)/fsHistogramRecord_->getTimerValue()*40000000.;
        fsHistogramRecord_->fsMuonCountersInteg_[iHist] += integralVal;
        rateGraphs_[iHist]->SetPoint(iteration_, fsHistogramRecord_->getStopTime() - startTime_, lastRate + 0.011);

        if(iHist%4 == 0) {
        	if(lastRate > 200000) {
        		monitorStatusList_.push_back(MonitorableStatus(MonitorItemName::RATE, MonitorableStatus::WARNING, "barrel trigger rate too high", lastRate));
        	}
        }
        else if(iHist%4 == 0) {
        	if(lastRate > 200000) {
        		monitorStatusList_.push_back(MonitorableStatus(MonitorItemName::RATE, MonitorableStatus::WARNING, "endcap trigger rate too high", lastRate));
        	}
        }

        if(iHist%4 == 0) { //we assume the order of the histograms in the histoManagers_, to avoid checking names (for better efficiency)
            rateGraphs_[iHist]->GetXaxis()->SetTitle("time");
            rateGraphs_[iHist]->GetYaxis()->SetTitle("rate [Hz]");
            rateGraphs_[iHist]->GetYaxis()->SetTitleOffset(0.5);
            rateGraphs_[iHist]->GetXaxis()->SetLabelSize(0.06);
            rateGraphs_[iHist]->GetHistogram()->SetTitle("");

            if(logScale_) {
                rateGraphs_[iHist]->SetMinimum(0.01);
                rateGraphs_[iHist]->SetMaximum(1000000);
            }
            else {
                rateGraphs_[iHist]->SetMinimum(0);
                rateGraphs_[iHist]->SetMaximum();
            }
            rateGraphs_[iHist]->GetXaxis()->SetTimeDisplay(1);
            rateGraphs_[iHist]->GetXaxis()->SetTimeFormat("%H:%M:%S");
            //rateGraphs_[iHist]->Draw("ALP");
        }

        ostringstream ostr;
        ostr<<rateGraphs_[iHist]->GetTitle()<<", mean "<<((double)fsHistogramRecord_->fsMuonCountersInteg_[iHist])/fsHistogramRecord_->getTimerValueIntegrated()*40000000.<<" Hz, last "<<lastRate<<" Hz";
        rateGraphLegendEntries_[iHist]->SetLabel(ostr.str().c_str());
    }
    ///----------making pictures----------------
    canvases_[1]->cd(1)->SetLogy(logScale_);
    canvases_[1]->cd(2)->SetLogy(logScale_);
    //cout<<__FUNCTION__<<"logScale_ "<<logScale_<<endl;

    /*    for(unsigned int iC = 0; iC < canvases_.size(); iC++) {
        canvases_[iC]->Update();
        canvases_[iC]->Modified();
        canvases_[iC]->Print(picturesDirs_[iC].c_str(), "png");
    }*/

	RootPicturesWorkaround::createPictures(name_, picturesDir_, canvases_, logger);

    iteration_++;

    return monitorStatusList_;
}

void SCStatisticDiagManager::initialize(time_t startTime) {
    StatisticDiagManagerBase::initialize(startTime);

    for(unsigned int iH = 0; iH < hsHistogramRecord_.size(); iH++) {
    	if(hsHistogramRecord_[iH] == 0) {
    		continue;
    	}
        for (unsigned int iCh = 0; iCh < HSHistogramRecord::binsCnt; iCh++) { //in the iCh = 0 empty muons are counted
            hsHistogramRecord_[iH]->getBranchData().binsHSMuonCounters_[iCh] = 0;
        }
    }

    for(unsigned int iHist = 0; iHist < fsHistogramRecord_->getHistoManagers().size(); iHist++)  {
    	ptCodeHistos_[iHist]->Reset();
    	qualityHistos_[iHist]->Reset();

    }

    for (unsigned int iCh = 0; iCh < (FSHistogramRecord::binsCnt * FSHistogramRecord::histoCnt); iCh++) {
    	fsHistogramRecord_->getBranchData().binsFSMuonCounters[iCh] = 0;
    	//was forgotten up to the end of 2012, thus the first time slice in the root file
    	//contained data from the previous run + the data from the first iteration of this run
    }

    hMaxRateHist_->Reset();
    hsMeanRateHist_->Reset();

    for(unsigned int i = 0; i < rateGraphs_.size(); i++) {
        rateGraphs_[i]->Set(0);
    }

    barrelPaveText_.GetLine(1)->SetText(0, 0, " ");
    endcapPaveText_.GetLine(1)->SetText(0, 0, " ");

    fsHistogramRecord_->fsMuonCountersInteg_.assign(8, 0);

    barrelPaveText_.GetLine(1)->SetText(0, 0, "");
    endcapPaveText_.GetLine(1)->SetText(0, 0, "");
}

string SCStatisticDiagManager::getHtml() {
    ostringstream ostr;
    ostr << "<br>\n";
    for(unsigned int i = 0; i < picturesNames_.size(); i++ ) {
        ostr << "<img src=\""<<"/tmp/"<<picturesNames_[i]<<"\">";
        ostr << "<br>\n";
    }
    return ostr.str();
}

}
