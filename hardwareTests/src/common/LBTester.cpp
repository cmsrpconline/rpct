#include "rpct/hardwareTests/LBTester.h"
/*#include "rpct/devices/LinkBoard.h"
#include "rpct/lboxaccess/FecManagerImpl.h"*/
#include "rpct/devices/System.h"
#include "rpct/devices/XmlSystemBuilder.h"
#include "rpct/hardwareTests/XmlBoardsSettings.h"
#include <boost/dynamic_bitset.hpp>
#include "rpct/std/bitcpy.h"

#include <time.h>
#include <sys/time.h>

using namespace rpct;
using namespace std;
using namespace boost;


dynamic_bitset<> LBTester::pulsToInput(boost::dynamic_bitset<> pulsePattern, RpcInputType rpcinputType) {
    dynamic_bitset<> inputBits(96); //<<<<<<<<<<<<<<<<<<<<<,
    for(unsigned int iB = 0; iB < pulsePattern.size(); iB++) {
        if(rpcinputType == testBoards) {
            for(unsigned int i = 0; i < 4; i++) {
                inputBits[((iB - iB%2) * 4) + (i * 2) + iB%2] =  pulsePattern[iB];
            }
        }
        else if(rpcinputType == febs) {
            for(unsigned int i = 0; i < 4; i++) {
                inputBits[iB * 4 + i] =  pulsePattern[iB];
            }
        }
        else
            throw TException("LBTester::pulsToInput: unknown rpcinputType");		

    }

    return inputBits;
}

LBTester::LBTester(RpcInputType _inputType, RpctSystem& _rpctSystem):
    rpctSystem(_rpctSystem) {
    rpcInputType = _inputType;	
}

/*void LBTester::preConfigure(std::string lbsSettingsFileName) {
	XmlBoardsSettings xmlBoardsSettings;
	if(lbsSettingsFileName != "") {
		xmlBoardsSettings.readFile(lbsSettingsFileName.c_str());
		cout<<"lbs configured with setings from file "<<lbsSettingsFileName<<endl;
	}
	else {
		cout<<"lbs configured with defoult setings (windows and delays = 0)"<<endl;
	}	

	LinkBoardSettingsMap& lbSettings = xmlBoardsSettings.getLinkBoardSettingsMap();		

	for (unsigned int iLB = 0; iLB < lbsVec.size(); iLB++) {	
		unsigned int lbId = lbsVec[iLB]->getStdLb().getId();
		lbsVec[iLB]->getStdLb().SetWindow(lbSettings[lbId].winO, lbSettings[lbId].winC);
		lbsVec[iLB]->getSynCoder().WriteBits(SynCoder::BITS_STATUS_INV_CLOCK, lbSettings[lbId].invClock);

		lbsVec[iLB]->getMasterLB()->getSynCoder().SetLMuxInDelay(lbsVec[iLB]->getMasterSleveNum(), lbSettings[lbId].lMuxInDelay); 					
		uint32_t zero = 0;

        lbsVec[iLB]->getSynCoder().WriteWord(SynCoder::WORD_BCN0_DELAY, 0, lbSettings[lbId].bcn0Delay);
        lbsVec[iLB]->getSynCoder().WriteWord(SynCoder::WORD_DATA_TRG_DELAY, 0, lbSettings[lbId].dataTrgDelay);

        lbsVec[iLB]->getSynCoder().WriteWord(SynCoder::WORD_DATA_WIN_DELAY, 0, zero);
        lbsVec[iLB]->getSynCoder().WriteWord(SynCoder::WORD_DATA_DIAG_DELAY, 0, zero);
       	lbsVec[iLB]->getSynCoder().WriteWord(SynCoder::WORD_DATA_DAQ_DELAY, 0, lbSettings[lbId].dataDaqDelay);        

        lbsVec[iLB]->getSynCoder().WriteWord(SynCoder::WORD_DAQ_TIMER_TRG_DELAY, 0, zero);
        lbsVec[iLB]->getSynCoder().WriteWord(SynCoder::WORD_PULSER_TIMER_TRG_DELAY, 0, lbSettings[lbId].pulserTimerTrgDelay); ////lbSettings[lbId].bcn0Delay<<<<<<<<<<<<<<<<<<<<<<<<<<<<<!!!!

        lbsVec[iLB]->getSynCoder().WriteBits(SynCoder::BITS_STATUS_TRG_DATA_SEL, zero); //trigger for data for counters and readout
        lbsVec[iLB]->getSynCoder().WriteBits(SynCoder::BITS_STATUS_EDGE_PULSE, 0);
        lbsVec[iLB]->getSynCoder().WriteBits(SynCoder::BITS_STATUS_FULL_OUT, 0);

        lbsVec[iLB]->getSynCoder().WriteBits(SynCoder::BITS_PULSER_LOOP_ENA, 0);     

        lbsVec[iLB]->getSynCoder().WriteBits(SynCoder::BITS_STATUS_RBC_EXPAND, 0); //RBC puls lenght - + 1 bx = 2 bxs
        lbsVec[iLB]->getSynCoder().WriteBits(SynCoder::BITS_STATUS_RBC_DELAY, lbSettings[lbId].rbcDelay); //RBC puls delay - + 1 bx = 2 bxs                
    }
}*/

//void LBTester::rpcInputsTestWithReadout(unsigned int readoutRepetitionsCnt) {	
//    vector<LinkBoard*>&       allLBsVec = rpctSystem.getLBsVec();
//    vector<MasterLinkBoard*>& masterLBsVec = rpctSystem.getMasterLBsVec();
//	
//	int	delay = 10;
//	
//	int winO = 5;
//   	int winC = 222;	
//   	//unsigned int        golDataSel = 5;
//    //pulser
//    unsigned int        pulserRepeatEna = 1;    
//    unsigned int        pulserOutEna = 1; 
//    unsigned int        pulserLoopEna = 0;
//    uint32_t       pulseLen = 256; // lenght of pulse pattern 256
//    uint64_t  pulserLimit =  256; // how many clocks pulser will work
///*      ttManual = 0, 
//        ttL1A,
//        ttPretrigger0, bradcast 0x04
//        ttPretrigger1, bradcast 0x08
//        ttPretrigger2, bradcast 0x0c
//        ttBCN0,
//        ttLocal */
//    IDiagCtrl::TTriggerType pulserStartTrigger = IDiagCtrl::ttBCN0;     
//	int          daqExtSel = 3; 
//	unsigned int readoutLimit = 256; 
//    
//    IDiagCtrl::TTriggerType readStartTrigger = IDiagCtrl::ttBCN0;    
//    	
//	uint32_t pulseData[pulseLen];
//	uint32_t iBx = 0;
//	pulseData[iBx] = 0; 		iBx++;
//	pulseData[iBx] = 0xffffff;	iBx++;
//	pulseData[iBx] = 0x0;		iBx++;
//	pulseData[iBx] = 0xaaaaaa;	iBx++;
//	pulseData[iBx] = 0x0;		iBx++;
//	pulseData[iBx] = 0x555555;	iBx++;
//	pulseData[iBx] = 0x0;		iBx++;
//	uint32_t one = 1;
//    for (int i = 0; i < 24; iBx++, i++) {
//    	if(iBx >= pulseLen)
//    		break;
//    	pulseData[iBx] = one;
//    	one = one << 1;
//    }
//    pulseData[iBx] = 0x0;		iBx++;
//    for ( ;iBx < pulseLen; iBx++) {
//    	if(iBx%2)
//	    	pulseData[iBx] = random() & 0xffffff;
//	    else {
//	    	pulseData[iBx] = 0;
//	    }	
//    }
//	
//	//////////////////
//	for (unsigned int iLB = 0; iLB < lbsVec.size(); iLB++) {	
//		allLBsVec[iLB]->getSynCoder().SetTTCrxDelays(winC, winO, 0, 0);
//		
//		//allLBsVec[iLB]->getSynCoder().WriteBits(SynCoder::BITS_PULSER_OUT_ENA, pulserOutEna);
//		allLBsVec[iLB]->getSynCoder().WriteBits(SynCoder::BITS_PULSER_LOOP_ENA, pulserLoopEna);
//		
//		allLBsVec[iLB]->getSynCoder().GetPulser().Configure(data, pulseLength, pulserRepeatEna);
//		allLBsVec[iLB]->getSynCoder().GetPulserDiagCtrl().Reset(); //<<<<<<<<<<<<<<<<<<<<<<<?????????????????????????
//    	allLBsVec[iLB]->getSynCoder().GetPulserDiagCtrl().Configure(pulserLimit - 1, pulserStartTrigger);	
//							
//       	IDiagnosticReadout::TMask& mask = allLBsVec[iLB]->GetDiagnosticReadout().GetMask(0);
//        mask.clear();
//        for(int i = 1; i <= allLBsVec[iLB]->GetDiagnosticReadout().GetMaskWidth(); i++) {
//            mask.insert(i);
//        }
//        allLBsVec[iLB]->getSynCoder().GetDiagnosticReadout().WriteMasks();
//        allLBsVec[iLB]->getSynCoder().GetDiagnosticReadout().Reset();
//        allLBsVec[iLB]->getSynCoder().GetDiagnosticReadout().GetDiagCtrl().Reset();
//        allLBsVec[iLB]->getSynCoder().GetDiagnosticReadout().GetDiagCtrl().Configure(readoutLimit - 1, diagStartTrigger);
//        allLBsVec[iLB]->getSynCoder().WriteWord(SynCoder::WORD_DAQ_TIMER_TRG_DELAY, 0, 0ul);           
//        allLBsVec[iLB]->getSynCoder().WriteBits(SynCoder::BITS_DAQ_DATA_SEL, 0);
//		allLBsVec[iLB]->getSynCoder().WriteBits(SynCoder::BITS_DAQ_EXT_SEL, daqExtSel);
//
//		
//	}
//    ////////////////////////	
//    
//    
//    vector<dynamic_bitset<> > inputsFromPuls;
//    for (iBx = 0 ;iBx < pulseLen; iBx++) {
//    	inputsFromPuls.push_back(pulsToInput(dynamic_bitset<>(LinkBoard::stripsCnt/4, pulseData[iBx]), rpcInputType));
//    }
//    
//	for(unsigned int iRep = 0; iRep < readoutRepetitionsCnt; iRep++) {		
//		for (unsigned int iLB = 0; iLB < lbsVec.size(); iLB++) {			
//			allLBsVec[iLB]->getSynCoder().GetPulserDiagCtrl().Start();
//			allLBsVec[iLB]->getSynCoder().GetDiagnosticReadout().GetDiagCtrl().Start();
//    	}
//    	
//    	//ttc.sendPretrigger0();
//    	//::system("TTCciCommand.exe");
//    	usleep(1000);
//    	
//    	for (unsigned int iLB = 0; iLB < lbsVec.size(); iLB++) {	
//			allLBsVec[iLB]->getSynCoder().GetPulserDiagCtrl().Stop();
//			allLBsVec[iLB]->getSynCoder().GetDiagnosticReadout().GetDiagCtrl().Stop();
//    	}
//    	
//    	resultsLog<<endl<<"rpcInputsTestWithReadout"<<endl;
//    	for (unsigned int iLB = 0; iLB < lbsVec.size(); iLB++) {
//			//analiza wynikow
//			resultsLog<<allLBsVec[iLB]->getCrate()->getDescription()<<" "<<allLBsVec[iLB]->getDescription()<<" id "<<allLBsVec[iLB]->getId()<<" ";
//			bool ok = true;
//			LBReadoutEventVec readoutEventVec = allLBsVec[iLB]->getLBReadoutEventVec();
//			if(readoutEventVec.size() > 0) {
//				for(unsigned int iBx = 0; iBx < readoutEventVec[0].bxDataVec.size() - delay; iBx++) {
//					LBReadoutBxDataVec& readoutBxDataVec = readoutEventVec[0].bxDataVec;
//					if (readoutBxDataVec[iBx + delay].getRpcData() != inputsFromPuls[iBx]) {
//						resultsLog<<"iBx "<<dec<<iBx<<endl;
//						resultsLog<<"pulseData        "<<hex<<pulseData[iBx]<<endl;
//						resultsLog<<"inputsFromPuls   "<<inputsFromPuls[iBx]<<endl;
//						resultsLog<<"readoutBxDataVec "<<readoutBxDataVec[iBx + delay].getRpcData()<<endl;
//						ok = false;
//					}
//				}
//			}
//			if(ok)
//				resultsLog<<" OK"<<endl;
//    	}		
//	}
//}
////---------------------------------------------------------------------------------------
//void LBTester::rpcInputsTestWithCounters(unsigned int countingTime) {	
//	const uint32_t secBx = 40000000;
//	int winO = 5;
//   	int winC = 222;	
//   	//unsigned int        golDataSel = 5;
//    //pulser
//    unsigned int        pulserRepeatEna = 0;    
//    unsigned int        pulserOutEna = 1; 
//    unsigned int        pulserLoopEna = 0;
//    uint32_t       pulseLen = 256; // lenght of pulse pattern 256
//    uint64_t  pulserLimit =  countingTime * secBx  + 2 *  secBx; // how many clocks pulser will work
///*      ttManual = 0, 
//        ttL1A,
//        ttPretrigger0, bradcast 0x04
//        ttPretrigger1, bradcast 0x08
//        ttPretrigger2, bradcast 0x0c
//        ttBCN0,
//        ttLocal */
//    IDiagCtrl::TTriggerType pulserStartTrigger = IDiagCtrl::ttBCN0;     
//    IDiagCtrl::TTriggerType countersStartTrigger = IDiagCtrl::ttBCN0;
//    
//	unsigned int countersLimit = countingTime * secBx;   	
//	uint32_t pulseData[pulseLen];
//	uint32_t pulsePattern = 0x555555;
//    for (uint32_t iBx = 0;iBx < pulseLen; iBx++) {
//    	if(iBx % 8 == 4)
//    		pulseData[iBx] = pulsePattern;
//    	else
//    		pulseData[iBx] = 0;
//    }
//	//////////////////
//	for (unsigned int iLB = 0; iLB < lbsVec.size(); iLB++) {	
//		lbsVec[iLB]->getStdLb().SetTTCrxDelays(winC, winO, 0, 0);
//		
//		//lbsVec[iLB]->getSynCoder().WriteBits(SynCoder::BITS_GOL_DATA_SEL, golDataSel);	
//		
//		lbsVec[iLB]->getSynCoder().WriteBits(SynCoder::BITS_PULSER_OUT_ENA, pulserOutEna);
//		lbsVec[iLB]->getSynCoder().WriteBits(SynCoder::BITS_PULSER_LOOP_ENA, pulserLoopEna);					
//        lbsVec[iLB]->configurePulser(pulseData, pulseLen, pulserRepeatEna, pulserLimit, pulserStartTrigger);
// 		lbsVec[iLB]->getSynCoder().GetFullRateHistoMgr().GetDiagCtrl().Reset(); 		       	
//		lbsVec[iLB]->getSynCoder().GetFullRateHistoMgr().GetDiagCtrl().Configure(countersLimit, countersStartTrigger); 		
//	}
//    ////////////////////////	
///*    cout<<"reading AREA_MEM_PULSE masterLBsVec[0] "<<endl;
//    lbsVec[0]->getSynCoder().ReadArea(SynCoder::AREA_MEM_PULSE, pulseData, 0, pulseLen); 
//    for (uint32_t i = 0; i < pulseLen; i++) {
//        cout << "pulseData["<<setw(3)<<i<<"] = " << hex << pulseData[i] << endl;
//    }*/
//    
//    resultsLog<<endl<<"rpcInputsTestWithCounters "<<endl;       
//	for(unsigned int iRep = 0; iRep < 2; iRep++) {	
//		if(iRep == 1) {
//			pulsePattern = 0xaaaaaa;
//		    for (uint32_t iBx = 0; iBx < pulseLen; iBx++) {
//		    	if(iBx % 8 == 4)
//		    		pulseData[iBx] = pulsePattern;
//		    	else
//		    		pulseData[iBx] = 0;
//		    }
//		}	
//		resultsLog<<"pulsePattern "<<hex<<pulsePattern<<endl;   
//		 
//		for (unsigned int iLB = 0; iLB < lbsVec.size(); iLB++) {	
//			if(iRep == 1) {		
//				lbsVec[iLB]->getSynCoder().WriteArea(SynCoder::AREA_MEM_PULSE, pulseData, 0, pulseLen);
//			}
//			lbsVec[iLB]->getSynCoder().GetFullRateHistoMgr().Reset();
//		}
//		
//		for (unsigned int iLB = 0; iLB < lbsVec.size(); iLB++) {						
//			lbsVec[iLB]->getSynCoder().GetPulserDiagCtrl().Start();
//			lbsVec[iLB]->getSynCoder().GetFullRateHistoMgr().GetDiagCtrl().Start();
//    	}
//    	
//    	//ttc.sendPretrigger0();
//    	//::system("TTCciCommand.exe");
//    	
//    	//usleep(countingTime * 1000000);
//    	int i = 0;
//        cout<<"GetFullRateHistoMgr().GetDiagCtrl().CheckCountingEnded():"<<endl;
//        while (!lbsVec[0]->getSynCoder().GetFullRateHistoMgr().GetDiagCtrl().CheckCountingEnded()) {
//            usleep(1000000);
//            cout << "*" << flush;
//            i++;
//            if(i == 10) {
//            	cout<<"conting not ended, returnig"<<endl;
//            	return;
//            }	            
//        }
//                    	
//    	for (unsigned int iLB = 0; iLB < lbsVec.size(); iLB++) {	
//			lbsVec[iLB]->getSynCoder().GetPulserDiagCtrl().Stop();
//			lbsVec[iLB]->getSynCoder().GetFullRateHistoMgr().GetDiagCtrl().Stop();
//    	}
//    	
//    	dynamic_bitset<> inputsFromPuls = pulsToInput(dynamic_bitset<>(LinkBoard::stripsCnt/4, pulsePattern), rpcInputType);
//    	    	
//    	for (unsigned int iLB = 0; iLB < lbsVec.size(); iLB++) {
//			//analiza wynikow
//			resultsLog<<lbsVec[iLB]->getDecsr()<<" "<<endl;
//			bool ok = true;
//			lbsVec[iLB]->getSynCoder().GetFullRateHistoMgr().Refresh();
//            unsigned int binCount = lbsVec[iLB]->getSynCoder().GetFullRateHistoMgr().GetBinCount();
//			for (unsigned int iCh = 0; iCh < binCount; iCh++) {
//            	unsigned int fullBinCnt = lbsVec[iLB]->getSynCoder().GetFullRateHistoMgr().GetLastData()[iCh];
//            	if(fullBinCnt != countersLimit / 8 * inputsFromPuls[iCh]) 
//            	{
//            		resultsLog<<"iCh "<<dec<<iCh<<" fullBinCnt: "<<fullBinCnt<<" expexcted "<<countersLimit / 8 * inputsFromPuls[iCh]<<endl;
//            		ok = false;
//            	}
//			}
//			if(ok)
//				resultsLog<<" OK"<<endl;
//    	}		
//	}
//}

void LBTester::windowTest(double countingTime, WindowTestType windowTestType, int winOInit, int winCInit, int winStep) {
    HalfBox::LinkBoards& allLBsVec = rpctSystem.getLBsVec();
    //vector<MasterLinkBoard*>& masterLBsVec = rpctSystem.getMasterLBsVec();

    //int winStepsCnt = 50;
    unsigned int clkInv = 0;

    const uint32_t secBx = 40000000;
    //unsigned int        golDataSel = 5;
    //pulser
    bool                pulserRepeatEna = 0;    
    unsigned int        pulserOutEna = 1; 
    unsigned int        pulserLoopEna = 0;
    uint32_t       pulseLen = 256; // lenght of pulse pattern 256
    uint64_t  pulserLimit = 0xffffffffffll;
        //countingTime * 239 * secBx + 100 * secBx ; // how many clocks pulser will work
    /*      ttManual = 0, 
        ttL1A,
        ttPretrigger0, bradcast 0x04
        ttPretrigger1, bradcast 0x08
        ttPretrigger2, bradcast 0x0c
        ttBCN0,
        ttLocal */
    IDiagCtrl::TTriggerType pulserStartTrigger = IDiagCtrl::ttBCN0;     
    IDiagCtrl::TTriggerType countersStartTrigger = IDiagCtrl::ttBCN0;

    std::vector<BigInteger> dataVec;
    uint64_t  countersLimit = countingTime * secBx;
    uint32_t pulseData;
    uint32_t pulsePattern = 0xffffff;
    int bxPerPulse = 16;
    for (uint32_t iBx = 0;iBx < pulseLen; iBx++) {
        if(iBx % bxPerPulse < 4)
            pulseData = pulsePattern;
        else
            pulseData = 0;

        dataVec.push_back(BigInteger(pulseData));	
    }
    //////////////////
    for (unsigned int iLB = 0; iLB < allLBsVec.size(); iLB++) {								
        //lbsVec[iLB]->getStdLb().SetTTCrxDelays(winC, winO, 0, 0);
        allLBsVec[iLB]->getSynCoder().writeBits(SynCoder::BITS_STATUS1_INV_CLOCK, clkInv); 
        //lbsVec[iLB]->getSynCoder().writeBits(SynCoder::BITS_GOL_DATA_SEL, golDataSel);	

        allLBsVec[iLB]->getSynCoder().writeBits(SynCoder::BITS_PULSER_OUT_ENA, pulserOutEna);					
        allLBsVec[iLB]->getSynCoder().getPulser().Configure(dataVec, pulseLen, pulserRepeatEna, 0);
        allLBsVec[iLB]->getSynCoder().writeBits(SynCoder::BITS_PULSER_LOOP_SEL, pulserLoopEna);		
        allLBsVec[iLB]->getSynCoder().getPulserDiagCtrl().Reset(); //<<<<<<<<<<<<<<<<<<<<<<<?????????????????????????
        allLBsVec[iLB]->getSynCoder().getPulserDiagCtrl().Configure(pulserLimit - 1, pulserStartTrigger, 0);	             	

        allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().Reset();	       	
        allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().Configure(countersLimit, countersStartTrigger, 0); 		
    }	
    ///////////////////////////

    vector<vector<double> > fullWinEffTab; //[iWinPos][iChanel]
    vector<vector<double> > adjWinEffTab; //[iWinPos][iChanel]
    vector<int> winPos; //(winStepsCnt, 0); //[iWinPos]

    if(windowTestType == testWinO) {
        resultsLog<<endl<<"window open test"<<endl; 
        cout<<"window open test "<<endl;                      	
    }
    else {
        resultsLog<<endl<<"window close test"<<endl; 
        cout<<"window close test "<<endl;                      	
    }
    for (unsigned int iLB = 0; iLB < allLBsVec.size(); iLB++) {
        bool ok = true;
        resultsLog<<allLBsVec[iLB]->getCrate()->getDescription()<<" "<<allLBsVec[iLB]->getDescription()<<" id "<<allLBsVec[iLB]->getId()<<endl;
        cout<<allLBsVec[iLB]->getCrate()->getDescription()<<" "<<allLBsVec[iLB]->getDescription()<<" id "<<allLBsVec[iLB]->getId()<<endl;

        int winO = winOInit;
        int winC = winCInit;

        int transientAreaBeg = -1;
        int transientAreaEnd = -1;

        allLBsVec[iLB]->getSynCoder().getPulserDiagCtrl().Start();

        for (int iWin = 0; true; iWin++) { 
            allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().Reset();
            allLBsVec[iLB]->getSynCoder().getWinRateHistoMgr().Reset();                     

            fullWinEffTab.push_back(vector<double>(96, 0));
            adjWinEffTab.push_back(vector<double>(96, 0));
            //Desc1, Decs2             
            allLBsVec[iLB]->setTTCrxDelays(winC, winO, 0, 0);                

            if(windowTestType == testWinO)  
                winPos.push_back(winO);
            else if(windowTestType == testWinC)
                winPos.push_back(winC);

            cout<<"winO " <<winO<<" winC "<<winC<<endl;                                                            


            allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().Start();                                     

            //::system("TTCciCommand.exe");
            //::system("ttccommand.exe");


            while (!allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().CheckCountingEnded()) {
                usleep(100000);
                cout << "*" << flush;
            }

            allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().Stop();                     

            allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().Refresh();
            allLBsVec[iLB]->getSynCoder().getWinRateHistoMgr().Refresh();

            int binCount = allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().GetBinCount();
            cout<<"fullHisto "<<"winHisto "<<endl;
            for (int iCh = 0; iCh < binCount; iCh++) {
                int fullBinCnt = allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().GetLastData()[iCh];
                int winBinCnt = allLBsVec[iLB]->getSynCoder().getWinRateHistoMgr().GetLastData()[iCh];
                cout << dec << setw(3) << iCh << ": " << dec 
                << setw(8) << fullBinCnt << setw(8) << winBinCnt<<setw(8)<<" ";                                                            

                fullWinEffTab[iWin][iCh] = ((double)fullBinCnt)/(countersLimit / bxPerPulse);
                cout <<" "<<dec<<fullWinEffTab[iWin][iCh];     
                
                adjWinEffTab[iWin][iCh] = ((double)winBinCnt)/(countersLimit / bxPerPulse);
                cout <<"   "<<dec<<adjWinEffTab[iWin][iCh]<<endl; 
            }    

            if(windowTestType == testWinO) {                
                if(winO >= 238)
                    break;  
                winO = winO + winStep;
            }               
            else if(windowTestType == testWinC) {
                if(winC >= 238)
                    break; 
                winC = winC + winStep; //////?????????????
            }  
        }
        allLBsVec[iLB]->getSynCoder().getPulserDiagCtrl().Stop();
        cout << "Done" << endl;

        /*           resultsLog<<"transientAreaBeg "<<dec<<transientAreaBeg<<" transientAreaEnd "<<transientAreaEnd<<endl;
            if(ok)
            	resultsLog<<"OK"<<endl;
            else
            	resultsLog<<"NOT OK !!!!"<<endl;*/	

        ofstream outFileFullWin;
        ofstream outFileAdjWin;
        ostringstream ostr;      

        string testType;
        if(windowTestType == testWinO) {   
            testType = "winOscan";                           
        }
        else if(windowTestType == testWinC) {                
            testType = "winCscan";
        }

        //ostr<<"windowTests/"<<allLBsVec[iLB]->getDescription()<<"_winCScan_.txt";

        string outFileName = "windowTests/" + allLBsVec[iLB]->getDescription() + "_" + testType + "_" + "full.txt";                
        outFileFullWin.open(outFileName.c_str());

        //ostr<<"windowTests/"<<allLBsVec[iLB]->getDescription()<<"_winC.txt";
        outFileName= "windowTests/" + allLBsVec[iLB]->getDescription() + "_" + testType + "_" + "adj.txt";                
        outFileAdjWin.open(outFileName.c_str());

        if(windowTestType == testWinO) {   
            outFileFullWin<<"winC "<<winC<<" clkInv "<<clkInv<<endl;        
            outFileAdjWin<<"winC "<<winC<<" clkInv "<<clkInv<<endl;
        }
        else if(windowTestType == testWinC) {                
            outFileFullWin<<"winO "<<winO<<" clkInv "<<clkInv<<endl;
            outFileAdjWin<<"winC "<<winC<<" clkInv "<<clkInv<<endl;
        }

        outFileFullWin<<"\t";
        outFileAdjWin<<"\t";
        for (u_int iW = 0; iW < winPos.size(); iW++) {
            outFileFullWin<<setw(12)<<winPos[iW];
            outFileAdjWin<<setw(12)<<winPos[iW];
        }
        outFileFullWin<<endl;
        outFileAdjWin<<endl;
        for (u_int iCh = 0; iCh < fullWinEffTab[0].size(); iCh++) {
            outFileFullWin<<iCh<<"\t";
            outFileAdjWin<<iCh<<"\t";
            for (u_int iW = 0; iW < fullWinEffTab.size(); iW++) {
                outFileFullWin<<setw(12)<<fullWinEffTab[iW][iCh];       
                outFileAdjWin<<setw(12)<<adjWinEffTab[iW][iCh]; 
            }           
            outFileFullWin<<endl; 
            outFileAdjWin<<endl;
        }

        outFileFullWin.close();
        outFileAdjWin.close();
    }
}


void LBTester::windowTestWithFEBs(double countingTime, WindowTestType windowTestType) {
    vector<LinkBoard*>&       allLBsVec = rpctSystem.getLBsVec();
    //vector<MasterLinkBoard*>& masterLBsVec = rpctSystem.getMasterLBsVec();
    //int winStepsCnt = 50;
    int winOInit = 0;
    int winCInit = 239;
    int winStep = 5;

    int pulsDistance = 16;

    unsigned int clkInv = 0;

    const uint32_t secBx = 40000000;
    //unsigned int        golDataSel = 5;
    //pulser
    unsigned int        pulserRepeatEna = 0;    
    unsigned int        pulserOutEna = 1; 
    unsigned int        pulserLoopEna = 0;
    uint32_t       pulseLen = 256; // lenght of pulse pattern 256
    uint64_t  pulserLimit =  countingTime * secBx  + 0.2 * secBx; // how many clocks pulser will work
    /*      ttManual = 0, 
        ttL1A,
        ttPretrigger0, bradcast 0x04
        ttPretrigger1, bradcast 0x08
        ttPretrigger2, bradcast 0x0c
        ttBCN0,
        ttLocal */
    IDiagCtrl::TTriggerType pulserStartTrigger = IDiagCtrl::ttBCN0;     
    IDiagCtrl::TTriggerType countersStartTrigger = IDiagCtrl::ttBCN0;

    unsigned int  countersLimit = countingTime * secBx;     	
    uint32_t pulseData;
    uint32_t pulsePattern = 0xffffff;//0xffffff
    std::vector<BigInteger> dataVec;
    for (uint32_t iBx = 0;iBx < pulseLen; iBx++) {
        if(iBx % pulsDistance == 4)
            pulseData = 0;
        else if(iBx % pulsDistance == 5)
            pulseData = 0;
        else
            pulseData = 0xffffff;

        dataVec.push_back(BigInteger(pulseData));	
    }
    //////////////////
    for (unsigned int iLB = 0; iLB < allLBsVec.size(); iLB++) {								
        //lbsVec[iLB]->getStdLb().SetTTCrxDelays(winC, winO, 0, 0);
        allLBsVec[iLB]->getSynCoder().writeBits(SynCoder::BITS_STATUS1_INV_CLOCK, clkInv); 
        //lbsVec[iLB]->getSynCoder().writeBits(SynCoder::BITS_GOL_DATA_SEL, golDataSel);	

        allLBsVec[iLB]->getSynCoder().writeBits(SynCoder::BITS_PULSER_OUT_ENA, pulserOutEna);
        allLBsVec[iLB]->getSynCoder().writeBits(SynCoder::BITS_PULSER_LOOP_SEL, pulserLoopEna);					
        allLBsVec[iLB]->getSynCoder().getPulser().Configure(dataVec, pulseLen, pulserRepeatEna, 0);
        allLBsVec[iLB]->getSynCoder().getPulserDiagCtrl().Reset(); //<<<<<<<<<<<<<<<<<<<<<<<?????????????????????????
        allLBsVec[iLB]->getSynCoder().getPulserDiagCtrl().Configure(pulserLimit - 1, pulserStartTrigger, 0);	             	

        allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().Reset();	       	
        allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().Configure(countersLimit, countersStartTrigger, 0); 		
    }

    ///////////////////////////
    //finding FEBs noisy channels
    resultsLog<<" finding FEBs noisy channels "<<endl;
    vector<dynamic_bitset<> > noisyChannels(allLBsVec.size(), dynamic_bitset<>(96, 0) );
    for (unsigned int iLB = 0; iLB < allLBsVec.size(); iLB++) {
        allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().Reset();	
    }
    for (unsigned int iLB = 0; iLB < allLBsVec.size(); iLB++) {
        allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().Start(); 
    }
    while (!allLBsVec[allLBsVec.size() - 1]->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().CheckCountingEnded()) {
        usleep(100000);
        cout << "*" << flush;
    }
    for (unsigned int iLB = 0; iLB < allLBsVec.size(); iLB++) {
        allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().Stop();    
    }
    for (unsigned int iLB = 0; iLB < allLBsVec.size(); iLB++) {
        resultsLog<<allLBsVec[iLB]->getCrate()->getDescription()<<" "<<allLBsVec[iLB]->getDescription()<<" id "<<allLBsVec[iLB]->getId()<<endl;
        cout<<allLBsVec[iLB]->getCrate()->getDescription()<<" "<<allLBsVec[iLB]->getDescription()<<" id "<<allLBsVec[iLB]->getId()<<endl; 		

        allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().Refresh();
        int binCount = allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().GetBinCount();
        cout<<"fullHisto "<<endl;
        for (int iCh = 0; iCh < binCount; iCh++) {        	
            int fullBinCnt = allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().GetLastData()[iCh];
            cout << dec << setw(3) << iCh << ": " << dec << setw(8) << fullBinCnt <<" ";  
            if(fullBinCnt > countersLimit/1000) {///<<<<<<<<<<<<<<<<<<<<<<
                cout<<" noisy!!! ";
                noisyChannels[iLB][iCh] = true;
                resultsLog<<"chanel "<< dec<<setw(3)<< iCh<<" fullBinCnt "<<dec<<setw(8)<<fullBinCnt<<" marked as noisy "<<endl;	
            }	   
            cout << endl;                                      
        }
    }      
    ///////////////////////////	

    if(windowTestType == testWinO) {
        resultsLog<<endl<<"window open test"<<endl; 
        cout<<"window open test "<<endl;                      	
    }
    else {
        resultsLog<<endl<<"window close test"<<endl; 
        cout<<"window open test "<<endl;                      	
    }

    ofstream pulsPosOutFile("windowTestsMTCC/pulsPosition.txt");
    if(windowTestType == testWinO) {
        pulsPosOutFile<<"window open test"<<endl;                   	
    }
    else {
        pulsPosOutFile<<"window close test"<<endl;                   	
    }

    for (unsigned int iLB = 0; iLB < allLBsVec.size(); iLB++) {
        vector<vector<double> > winEffTab; //[iWinPos][iChanel]
        vector<vector<int> > fulWinTab;
        vector<vector<int> > addWinTab;

        vector<int> winPos; //(winStepsCnt, 0); //[iWinPos]
        dynamic_bitset<> badChannels(96, 0); //<<<<<<<<<<<<<<<<<<<<<<

        bool ok = true;

        resultsLog<<allLBsVec[iLB]->getCrate()->getDescription()<<" "<<allLBsVec[iLB]->getDescription()<<" id "<<allLBsVec[iLB]->getId()<<endl;
        cout<<allLBsVec[iLB]->getCrate()->getDescription()<<" "<<allLBsVec[iLB]->getDescription()<<" id "<<allLBsVec[iLB]->getId()<<endl;

        int winO = winOInit;
        int winC = winCInit;

        /*        int transientAreaBeg = -1;
        int transientAreaEnd = -1; */

        for (int iWin = 0; true; iWin++) { 
            allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().Reset();
            allLBsVec[iLB]->getSynCoder().getWinRateHistoMgr().Reset();                     

            winEffTab.push_back(vector<double>(96, 0));
            fulWinTab.push_back(vector<int>(96, 0));
            addWinTab.push_back(vector<int>(96, 0));

            //Desc1, Decs2             
            allLBsVec[iLB]->setTTCrxDelays(winC, winO, 0, 0);                

            if(windowTestType == testWinO)  
                winPos.push_back(winO);
            else if(windowTestType == testWinC)
                winPos.push_back(winC);

            cout<<"winO " <<winO<<" winC "<<winC<<endl; 
            if(iWin == 0)
                resultsLog<<"winO " <<winO<<" winC "<<winC<<endl;                                                            

            allLBsVec[iLB]->getSynCoder().getPulserDiagCtrl().Start();                                         
            allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().Start();                                     

            //::system("TTCciCommand.exe");
            //::system("ttccommand.exe");

            while (!allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().CheckCountingEnded()) {
                usleep(100000);
                cout << "*" << flush;
            }

            allLBsVec[iLB]->getSynCoder().getPulserDiagCtrl().Stop();                                         
            allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().Stop();                     

            allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().Refresh();
            allLBsVec[iLB]->getSynCoder().getWinRateHistoMgr().Refresh();

            int binCount = allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().GetBinCount();
            cout<<"fullHisto "<<"winHisto "<<endl;

            for (int iCh = 0; iCh < binCount; iCh++) {
                int fullBinCnt = allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().GetLastData()[iCh];
                int winBinCnt = allLBsVec[iLB]->getSynCoder().getWinRateHistoMgr().GetLastData()[iCh];

                fulWinTab[iWin][iCh] = fullBinCnt;
                addWinTab[iWin][iCh] = winBinCnt;

                if (fullBinCnt > 0) {
                    cout << dec << setw(3) << iCh << ": " << dec 
                    << setw(8) << fullBinCnt << setw(8) << winBinCnt<<setw(8)<<" ";

                    winEffTab[iWin][iCh] = ((double)winBinCnt) / fullBinCnt;
                    cout <<" "<<dec<<winEffTab[iWin][iCh]<<endl;                                               
                }   
                else {
                    cout << dec << setw(3) << iCh << ": " << dec 
                    << setw(8) << fullBinCnt << setw(8) << winBinCnt<<setw(8)<<" "<<endl;;                
                }
                if(iWin == 0) {
                    resultsLog << dec << setw(3) << iCh << ": " << dec 
                    << setw(8) << fullBinCnt << setw(8) << winBinCnt<<setw(8)<<" "
                    <<" "<<dec<<winEffTab[iWin][iCh]<<endl;                   	
                }

                if(fullBinCnt < countersLimit / pulsDistance * 0.95)
                    badChannels[iCh] = true;                                     
            }                
            if(windowTestType == testWinO) {
                winStep = 5;                		
                winO = winO + winStep;
                if(winO >= 215 && winC != 180) {
                    winC = 180;	
                    winO = 200;
                }
                if(winO >= 240) {   
                    winO = 0;  
                }		       
                if(winO == 55 && winPos.size() > (240 / winStep) )              
                    break;             	
            }               
            else if(windowTestType == testWinC) {
                winC = winC + winStep; //////?????????????
            }                                
        }                                      
        cout << "Done" << endl;

        /*       //analiza
       vector<int> pulsePosition(LinkBoard::stripsCnt, 0);
        if(noisyChannels[iLB][iCh] == false &&  badChannels[iCh] == false) {//skeaping noisy and dead channels
            if(windowTestType == testWinO) {             	
            	for (int iCh = 0; iCh < binCount; iCh++) {   
            		for (int iWin = 1; winEffTab.size(); iWin++) {	            		               	             
	            		if(winEffTab[iWin - 1][iCh] > 0.5 && winEffTab[iWin][iCh] <= 0.5) {
	            			if(
	            		}

		            	if(winO >= 5 && transientAreaBeg == -1 && winEffTab[iWin][iCh] < 0.95) {
		            		transientAreaBeg = winO;
		            	}

		    			if(winEffTab[iWin][iCh] > 0.05 && winO < 210 && winO > 10) {
		    				transientAreaEnd = winO;
		    			}	 
            		}
            	}
            }
        }

        resultsLog<<"transientAreaBeg "<<dec<<transientAreaBeg<<" transientAreaEnd "<<transientAreaEnd<<endl;
        pulsPosOutFile<<lbsVec[iLB]->getStdLb().getDescription()<<" "<<dec<<transientAreaBeg<<" transientAreaEnd "<<transientAreaEnd<<endl;
         */      
        if(ok)
            resultsLog<<"OK"<<endl;
        else
            resultsLog<<"NOT OK !!!!"<<endl;	

        ofstream outFile;
        ostringstream ostr;      
        if(windowTestType == testWinO) {
            ostr<<"windowTestsMTCC/"<<allLBsVec[iLB]->getId()<<"_"<<allLBsVec[iLB]->getDescription()<<"_winO.txt";
            outFile.open(ostr.str().c_str());
            outFile<<"lbId "<<allLBsVec[iLB]->getId()
            <<" winCInit "<<winCInit<<" winC "<<winC<<" clkInv "<<clkInv
            <<" winStep "<<winStep<<endl;
        }
        else if(windowTestType == testWinC) {
            ostr<<"windowTestsMTCC/"<<allLBsVec[iLB]->getDescription()<<"_winC.txt";
            outFile.open(ostr.str().c_str());
            outFile<<"lbId "<<allLBsVec[iLB]->getId()<<" winO "<<winO<<" clkInv "<<clkInv<<endl;
        }
        outFile<<"\t";
        for (u_int iW = 0; iW < winPos.size(); iW++) {
            outFile<<setw(22)<<winPos[iW];
        }
        outFile<<"\t-1"<<endl;
        for (u_int iCh = 0; iCh < winEffTab[0].size(); iCh++) {
            outFile<<iCh<<"\t";
            for (u_int iW = 0; iW < winEffTab.size(); iW++) {
                //outFile<<setw(12)<<winEffTab[iW][iCh];   
                outFile<<setw(11)<<fulWinTab[iW][iCh]<<setw(11)<<addWinTab[iW][iCh];           
            }   
            outFile<<"\t";

            if(noisyChannels[iLB][iCh]) 
                outFile<<"n";
            if(badChannels[iCh]) 
                outFile<<"b";	
            if(!noisyChannels[iLB][iCh] && !badChannels[iCh])
                outFile<<"g";		      
            outFile<<endl; 
        }

        outFile.close();			
    }
}


void LBTester::histosTest(double countingTime) {
    HalfBox::LinkBoards& allLBsVec = rpctSystem.getLBsVec();
    //vector<MasterLinkBoard*>& masterLBsVec = rpctSystem.getMasterLBsVec();

    //int winStepsCnt = 50;
    unsigned int clkInv = 0;

    const uint32_t secBx = 40000000;
    //unsigned int        golDataSel = 5;
    //pulser
    bool                pulserRepeatEna = 0;    
    unsigned int        pulserOutEna = 1; 
    unsigned int        pulserLoopEna = 0;
    uint32_t       pulseLen = 256; // lenght of pulse pattern 256
    uint64_t  pulserLimit =  countingTime * secBx + secBx ; // how many clocks pulser will work
    /*      ttManual = 0, 
        ttL1A,
        ttPretrigger0, bradcast 0x04
        ttPretrigger1, bradcast 0x08
        ttPretrigger2, bradcast 0x0c
        ttBCN0,
        ttLocal */
    IDiagCtrl::TTriggerType pulserStartTrigger = IDiagCtrl::ttPretrigger0;     
    IDiagCtrl::TTriggerType countersStartTrigger = IDiagCtrl::ttPretrigger0;

    std::vector<BigInteger> dataVec;
    unsigned int  countersLimit = countingTime * secBx;         
    uint32_t pulseData;
    uint32_t pulsePattern = 0xffffff;
    int bxPerPulse = 16;
    for (uint32_t iBx = 0;iBx < pulseLen; iBx++) {
        if(iBx % bxPerPulse < 4)
            pulseData = pulsePattern;
        else
            pulseData = 0;

        dataVec.push_back(BigInteger(pulseData));   
    }
    //////////////////
    for (unsigned int iLB = 0; iLB < allLBsVec.size(); iLB++) {                             
        //lbsVec[iLB]->getStdLb().SetTTCrxDelays(winC, winO, 0, 0);
        allLBsVec[iLB]->getSynCoder().writeBits(SynCoder::BITS_STATUS1_INV_CLOCK, clkInv); 
        //lbsVec[iLB]->getSynCoder().writeBits(SynCoder::BITS_GOL_DATA_SEL, golDataSel);    

        allLBsVec[iLB]->getSynCoder().writeBits(SynCoder::BITS_PULSER_OUT_ENA, pulserOutEna);                   
        allLBsVec[iLB]->getSynCoder().getPulser().Configure(dataVec, pulseLen, pulserRepeatEna, 0);
        allLBsVec[iLB]->getSynCoder().writeBits(SynCoder::BITS_PULSER_LOOP_SEL, pulserLoopEna);     
        allLBsVec[iLB]->getSynCoder().getPulserDiagCtrl().Reset(); //<<<<<<<<<<<<<<<<<<<<<<<?????????????????????????
        allLBsVec[iLB]->getSynCoder().getPulserDiagCtrl().Configure(pulserLimit - 1, pulserStartTrigger, 0);                    

        allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().Reset();          
        allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().Configure(countersLimit, countersStartTrigger, 0);        
    }   
    ///////////////////////////

    unsigned int winO, winC, incClk;
    char yn = 'y';
    while (yn == 'y') {
        cout<<"winO? ";
        cin>>winO;
        cout<<"winC? ";
        cin>>winC;
        cout<<"incClk? ";
        cin>>incClk;
        for (unsigned int iLB = 0; iLB < allLBsVec.size(); iLB++) {
            bool ok = true;            
            cout<<allLBsVec[iLB]->getCrate()->getDescription()<<" "<<allLBsVec[iLB]->getDescription()<<" id "<<allLBsVec[iLB]->getId()<<endl;

            allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().Reset();
            allLBsVec[iLB]->getSynCoder().getWinRateHistoMgr().Reset();                     

            //Desc1, Decs2             
            allLBsVec[iLB]->setTTCrxDelays(winC, winO, 0, 0);   
            allLBsVec[iLB]->getSynCoder().writeBits(SynCoder::BITS_STATUS1_INV_CLOCK, incClk);

            cout<<allLBsVec[iLB]->getDescription()<<" winO "<<
            allLBsVec[iLB]->getTtcRxI2c().ReadFineDelay2()<<" winC "<<
            allLBsVec[iLB]->getTtcRxI2c().ReadFineDelay1()<<endl;

            allLBsVec[iLB]->getSynCoder().getPulserDiagCtrl().Start();
            allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().Start();                                     

            //::system("TTCciCommand.exe");
            //::system("ttccommand.exe");

            while (!(allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().CheckCountingEnded()) && 
                    !(allLBsVec[iLB]->getSynCoder().getPulserDiagCtrl().CheckCountingEnded())) {
                usleep(100000);
                cout << "*" << flush;
            }

            allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().Stop();                     

            allLBsVec[iLB]->getSynCoder().getPulserDiagCtrl().Stop();

            allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().Refresh();
            allLBsVec[iLB]->getSynCoder().getWinRateHistoMgr().Refresh();

            int binCount = allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().GetBinCount();
            cout<<endl<<"fullHisto "<<"winHisto "<<endl;
            for (int iCh = 0; iCh < binCount; iCh++) {
                int fullBinCnt = allLBsVec[iLB]->getSynCoder().getFullRateHistoMgr().GetLastData()[iCh];
                int winBinCnt = allLBsVec[iLB]->getSynCoder().getWinRateHistoMgr().GetLastData()[iCh];
                cout << dec << setw(3) << iCh << ": " << dec 
                << setw(8) << fullBinCnt << setw(8) << winBinCnt<<setw(8)<<" ";

                if(fullBinCnt > 0)
                    cout <<" "<< ((double)winBinCnt)/fullBinCnt<<endl;
                else
                    cout <<endl;              
            }


        }
        cout<<"continue(y/n)?";
        cin>>yn;
    }
}
//void LBTester::slaveMasterTest(unsigned int readoutRepetitionsCnt) {
//	int delay = 6;
//	
//	int winO = 5;
//   	int winC = 222;	
//   	unsigned int        golDataSel = 2; //from resultsLogpulser, slave only
//    pulser
//    unsigned int        pulserRepeatEna = 1;    
//    unsigned int        pulserOutEna = 1; 
//    unsigned int        pulserLoopEna = 0;resultsLog
//    uint32_t       pulseLen = 256; // lenght of pulse pattern 256
//    uint64_t  pulserLimit =  256; // how many clocks pulser will work
///*      ttManual = 0, 
//        ttL1A,
//        ttPretrigger0, bradcast 0x04
//        ttPretrigger1, bradcast 0x08
//        ttPretrigger2, bradcast 0x0c
//        ttBCN0,
//        ttLocal */
//    IDiagCtrl::TTriggerType pulserStartTrigger = IDiagCtrl::ttBCN0;     
//	int          daqExtSel = 3; 
//	unsigned int readoutLimit = 256; 
//    
//    IDiagCtrl::TTriggerType readStartTrigger = IDiagCtrl::ttBCN0;    
//    
//	for (unsigned int iLB = 0; iLB < lbsVec.size(); iLB++) {
//		lbsVec[iLB]->getStdLb().SetTTCrxDelays(winC, winO, 0, 0);		
//	}
//			
//	uint32_t pulseData[pulseLen];
//	uint32_t iBx = 0;
//	pulseData[iBx] = 0; 		iBx++;resultsLog
//	pulseData[iBx] = 0xfffff;	iBx++;
//	pulseData[iBx] = 0x0;		iBx++;
//	pulseData[iBx] = 0xaaaaa;	iBx++;
//	pulseData[iBx] = 0x0;		iBx++;
//	pulseData[iBx] = 0x55555;	iBx++;
//	pulseData[iBx] = 0x0;		iBx++;
//	uint32_t one = 1;
//    for (int i = 0;i < 20; iBx++, i++) {
//    	if(iBx >= pulseLen)
//    		break;
//    	pulseData[iBx] = one;
//    	one = one << 1;
//    }
//    pulseData[iBx] = 0x0;		iBx++;
//    for ( ;iBx < pulseLen; iBx++) {
//    	pulseData[iBx] = random() & 0xfffff;
//    }
//	
//	////////////////
//	for (unsigned int iLB = 0; iLB < lbsVec.size(); iLB++) {	
//		lbsVec[iLB]->getSynCoder().WriteBits(SynCoder::BITS_PULSER_OUT_ENA, pulserOutEna);	
//		lbsVec[iLB]->getSynCoder().WriteBits(SynCoder::BITS_PULSER_LOOP_SEL, pulserLoopEna);
//		if(lbsVec[iLB]->isMaster())	
//			lbsVec[iLB]->configureReadout(daqExtSel, readoutLimit, readStartTrigger); 		
//		else {
//			lbsVec[iLB]->getSynCoder().WriteBits(SynCoder::BITS_GOL_DATA_SEL, golDataSel);
//			lbsVec[iLB]->configurePulser(pulseData, pulseLen, pulserRepeatEna, pulserLimit, pulserStartTrigger);							
//		}	
//	}
//    //////////////////////
//    
//	resultsLog<<endl<<"slaveMasterTest"<<endl;
//	for(unsigned int iRep = 0; iRep < readoutRepetitionsCnt; iRep++) {		
//		for (int iSlb = 1; iSlb <= 2; iSlb++) {
//			for (unsigned int iMLB = 0; iMLB < masterLBsVec.size(); iMLB++) {
//				if(masterLBsVec[iMLB]->isSLB(iSlb) == false) 
//					continue;
//					
//				masterLBsVec[iMLB]->getSynCoder().WriteBits(rpct::SynCoder::BITS_DAQ_EXT_SEL, 3 + iSlb);
//				
//				masterLBsVec[iMLB]->getSLB(iSlb)->getSynCoder().GetPulserDiagCtrl().Start();
//				masterLBsVec[iMLB]->getSynCoder().GetDiagnosticReadout().GetDiagCtrl().Start();
//	
//	    	}
//	    	
//	    	ttc.sendPretrigger0();
//	    	::system("TTCciCommand.exe");
//    		usleep(1000);
//	    	
//	    	for (unsigned int iMLB = 0; iMLB < masterLBsVec.size(); iMLB++) {
//				if(masterLBsVec[iMLB]->isSLB(iSlb) == false) 
//					continue;
//				
//				masterLBsVec[iMLB]->getSLB(iSlb)->getSynCoder().GetPulserDiagCtrl().Stop();
//				masterLBsVec[iMLB]->getSynCoder().GetDiagnosticReadout().GetDiagCtrl().Stop();
//	
//	    	}
//	    	
//	    	for (unsigned int iMLB = 0; iMLB < masterLBsVec.size(); iMLB++) {
//				if(masterLBsVec[iMLB]->isSLB(iSlb) == false) 
//					continue;
//					
//	    		resultsLog<<masterLBsVec[iMLB]->getDecsr()<<"  -  ";
//	    		resultsLog<<masterLBsVec[iMLB]->getSLB(iSlb)->getDecsr()<<"  -  ";				
//				analiza wynikow
//				bool ok = true;
//				LBReadoutEventVec readoutEventVec = lbsVec[iMLB]->getLBReadoutEventVec();
//				if(readoutEventVec.size() > 0) {
//					for(unsigned int iBx = 0; iBx < readoutEventVec[0].bxDataVec.size() - delay; iBx++) {
//						LBReadoutBxDataVec& readoutBxDataVec = readoutEventVec[0].bxDataVec;
//						if (readoutBxDataVec[iBx + delay].getExtData() != pulseData[iBx]) {
//							ok = false;
//							resultsLog<<"iBx "<<dec<<iBx<<endl;
//							resultsLog<<"pulseData        "<<hex<<pulseData[iBx]<<endl;
//							resultsLog<<"readoutBxDataVec "<<readoutBxDataVec[iBx + delay].getExtData()<<endl;
//						}
//					}
//				}
//				if(ok)
//					resultsLog<<" OK"<<endl;
//	    	}		
//		}
//	}
//}
//
//void LBTester::configureForGolTest() {	
//	int winO = 5;
//   	int winC = 222;	
//   	unsigned int        golDataSel = 2;
//    pulser
//    unsigned int        pulserRepeatEna = 1;    
//    unsigned int        pulserOutEna = 1; 
//    unsigned int        pulserLoopEna = 0;
//    uint32_t       pulseLen = 256; // lenght of pulse pattern 256
//    uint64_t  pulserLimit =  256; // how many clocks pulser will work
///*      ttManual = 0, 
//        ttL1A,
//        ttPretrigger0, bradcast 0x04
//        ttPretrigger1, bradcast 0x08
//        ttPretrigger2, bradcast 0x0c
//        ttBCN0,
//        ttLocal */
//    IDiagCtrl::TTriggerType pulserStartTrigger = IDiagCtrl::ttBCN0;     
//	int          daqExtSel = 3; 
//	unsigned int readoutLimit = 256; 
//    
//    IDiagCtrl::TTriggerType readStartTrigger = IDiagCtrl::ttBCN0;    
//    	
//	uint32_t pulseData[pulseLen];
//	uint32_t iBx = 0;
//	pulseData[iBx] = 0; 		iBx++;
//	pulseData[iBx] = 0xffff;	iBx++;
//	pulseData[iBx] = 0x0;		iBx++;
//	pulseData[iBx] = 0xaaaa;	iBx++;
//	pulseData[iBx] = 0x0;		iBx++;
//	pulseData[iBx] = 0x5555;	iBx++;
//	pulseData[iBx] = 0x0;		iBx++;
//	uint32_t one = 1;
//    for (;iBx < 20; iBx++) {
//    	if(iBx >= pulseLen)
//    		break;
//    	pulseData[iBx] = one;
//    	one = one << 1;
//    }
//    pulseData[iBx] = 0x0;		iBx++;
//    for ( ;iBx < pulseLen; iBx++) {
//    	if(iBx%2)
//	    	pulseData[iBx] = random();
//	    else {
//	    	pulseData[iBx] = 0;
//	    }	
//    }
//	
//	////////////////
//	for (unsigned int iLB = 0; iLB < lbsVec.size(); iLB++) {	
//		lbsVec[iLB]->getStdLb().SetTTCrxDelays(winC, winO, 0, 0);
//		
//		lbsVec[iLB]->getSynCoder().WriteBits(SynCoder::BITS_GOL_DATA_SEL, golDataSel);	
//		lbsVec[iLB]->getSynCoder().SynchGOL();
//		lbsVec[iLB]->getSynCoder().WriteBits(SynCoder::BITS_GOL_LASER_ENA, 1);  
//		lbsVec[iLB]->getSynCoder().WriteBits(SynCoder::BITS_GOL_TX_ERR, 0);
//			
//		lbsVec[iLB]->getSynCoder().WriteBits(SynCoder::BITS_PULSER_OUT_ENA, pulserOutEna);
//		lbsVec[iLB]->getSynCoder().WriteBits(SynCoder::BITS_PULSER_LOOP_SEL, pulserLoopEna);					
//        lbsVec[iLB]->configurePulser(pulseData, pulseLen, pulserRepeatEna, pulserLimit, pulserStartTrigger);	
//		lbsVec[iLB]->configureReadout(daqExtSel, readoutLimit, readStartTrigger); 		
//	}
//    //////////////////////	
//}

void LBTester::saveResults(std::string fileName) {
    std::ofstream outFile(fileName.c_str(), std::ofstream::app);
    time_t t = time(NULL);
    outFile<<endl<<endl<<ctime(&t)<<endl;
    outFile<<resultsLog.str();
}
