#include "rpct/hardwareTests/LBPulseMaker.h"
#include <cmath>

using namespace std;

LBPulseMaker::LBPulseMaker(){testType = 0;}


vector<uint32_t> LBPulseMaker::getTestPulse(int  iLB){

  vector<uint32_t> pulseData(256);
  for(int clockTick=0;clockTick<256;clockTick++) 
    pulseData[clockTick] = testPulse(clockTick,iLB);


  return pulseData;
}


uint32_t LBPulseMaker::testPulse(int clockTick, int iLB){
  if(testType==0) return serialTest(clockTick, iLB);
  else return 0;
}

uint32_t LBPulseMaker::serialTest(int clockTick, int iLB){
  return (uint32_t)pow(2.0,clockTick%24);
}





/*

		for (uint32_t iBx = 0;iBx < pulseLen; iBx++) {
    		if(iBx  == 4)
    			pulseData[iBx] = 0xfffeff;
    		else if(iBx == 5)
    			pulseData[iBx] = 0xfffeff;
    		else
    			pulseData[iBx] = 0xffffff;
    }
*/
