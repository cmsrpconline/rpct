#include "rpct/hardwareTests/RPCBxData.h"

using namespace std;
using namespace log4cplus;
//using namespace rpct;

Logger RPCBxData::logger = Logger::getInstance("RPCBxData");
void RPCBxData::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}
