#include "rpct/hardwareTests/XmlBoardsSettings.h"
#include "rpct/devices/LinkBoardSettings.h"
#include <log4cplus/logger.h>

#include <xercesc/parsers/SAXParser.hpp>
#include "xoap/domutils.h"

using namespace xoap;
using namespace xercesc;
using namespace std;
using namespace log4cplus;
using namespace rpct;

Logger XmlBoardsSettings::logger = Logger::getInstance("XmlBoardsSettings");

XmlBoardsSettings::XmlBoardsSettings()
{
}

XmlBoardsSettings::~XmlBoardsSettings()
{
}

void XmlBoardsSettings::readFile(string filename) { 
    this->filename = filename;
    resetErrors();
    SAXParser parser;
    parser.setDocumentHandler(this);
    parser.setErrorHandler(this);
    parser.parse(filename.c_str());
    if (sawErrors) {
        throw TException("Problems found during reading the file " + filename);
    }
}

string XmlBoardsSettings::getAttribute(const char* attrName, xercesc::AttributeList& attributes) {
    return XMLCh2String(attributes.getValue(attrName));
}

bool XmlBoardsSettings::getHexAttribute(const char* attrName, xercesc::AttributeList& attributes, 
        unsigned int& value) {        
    string attr = XMLCh2String(attributes.getValue(attrName));
    if (!attr.empty()) {
        istringstream istr(attr);
        istr >> hex >> value;
        return true;
    }    
    return false;
}
    
bool XmlBoardsSettings::getDecAttribute(const char* attrName, xercesc::AttributeList& attributes, 
        unsigned int& value) {        
    string attr = XMLCh2String(attributes.getValue(attrName));
    if (!attr.empty()) {
        istringstream istr(attr);
        istr >> value;
        return true;
    }    
    return false;
}

void XmlBoardsSettings::startElement(const XMLCh* const name, AttributeList& attributes) {
    string sname = XMLCh2String(name);
    unsigned int id = 0;
    getDecAttribute("id", attributes, id);
    //LOG4CPLUS_DEBUG(logger, "XmlBoardsSettings: startElement begin " << sname);
  
    if (sname == "linkBoard") {
    	
    	
    	unsigned int winO;
	    unsigned int winC;
	    unsigned int invClock;
	    unsigned int lMuxInDelay;
	    unsigned int rbcDelay;
	    unsigned int bcn0Delay;
	    unsigned int dataTrgDelay;
	    unsigned int dataDaqDelay;
	    unsigned int pulserTimerTrgDelay;
    
        getDecAttribute("winO", attributes, winO);
        getDecAttribute("winC", attributes, winC);
        getDecAttribute("invClock", attributes, invClock);
        getDecAttribute("lMuxInDelay", attributes, lMuxInDelay);
        getDecAttribute("rbcDelay", attributes, rbcDelay);
        getDecAttribute("bcn0Delay", attributes, bcn0Delay);
        getDecAttribute("dataTrgDelay", attributes, dataTrgDelay);
        getDecAttribute("dataDaqDelay", attributes, dataDaqDelay);        
        getDecAttribute("pulserTimerTrgDelay", attributes, pulserTimerTrgDelay);
        boost::dynamic_bitset<> channelsEna(96, 0ul);
        channelsEna.set(); 
        LinkBoardSettings linkBoardSettings(winO,
										    winC,
										    invClock,
										    lMuxInDelay,
										    rbcDelay,
										    bcn0Delay,
										    dataTrgDelay,
										    dataDaqDelay,
										    pulserTimerTrgDelay,
                                            channelsEna); // TODO  channelsEna not done          
        linkBoardSettingsMap[id] = linkBoardSettings;
        
/*		ostringstream ostr;
		ostr<<"winO "<<linkBoardSettings.winO;
		ostr<<" winC "<<linkBoardSettings.winC;
		ostr<<" invClock "<<linkBoardSettings.invClock;
		ostr<<" rbcDelay "<<linkBoardSettings.rbcDelay;
		LOG4CPLUS_DEBUG(logger, ostr.str());
		cout<<ostr.str()<<endl;*/
    }	
    else if(sname == "optLink") {
    	OptLinkConnection optLinkCon;
 		getDecAttribute("mlbId", attributes, optLinkCon.mlbId);
        getDecAttribute("tbId", attributes, optLinkCon.tbId);
        getDecAttribute("tbInNum", attributes, optLinkCon.tbInNum);  
        
        optLinkConnections.push_back(optLinkCon);  	
    }
	else if(sname == "dccConn") {
    	DccConnection dccConn;
 		getDecAttribute("tbId", attributes, dccConn.tbId);
        getDecAttribute("dccId", attributes, dccConn.dccId);
        getDecAttribute("dccChannNum", attributes, dccConn.dccChannelNum);  
        getDecAttribute("dccChannId", attributes, dccConn.dccChannelId); 
        
        dccConnections.push_back(dccConn);  	
    }
    //LOG4CPLUS_DEBUG(logger, "XmlBoardsSettings: startElement end " << sname);
}


void XmlBoardsSettings::endElement(const XMLCh* const name) {
    string sname = XMLCh2String(name);
}

void XmlBoardsSettings::error(const xercesc::SAXParseException& e) {
    sawErrors = true;
    LOG4CPLUS_ERROR(logger,  "Error at file " << XMLCh2String(e.getSystemId())
            << ", line " << e.getLineNumber()
            << ", char " << e.getColumnNumber()
            << "\n  Message: " << XMLCh2String(e.getMessage()));
}
 
void XmlBoardsSettings::fatalError(const xercesc::SAXParseException& e) {
    sawErrors = true;
    LOG4CPLUS_ERROR(logger,  "Fatal Error at file " << XMLCh2String(e.getSystemId())
            << ", line " << e.getLineNumber()
            << ", char " << e.getColumnNumber()
            << "\n  Message: " << XMLCh2String(e.getMessage()));
}
 
void XmlBoardsSettings::warning(const xercesc::SAXParseException& e) {
    LOG4CPLUS_WARN(logger,  "Warning at file " << XMLCh2String(e.getSystemId())
            << ", line " << e.getLineNumber()
            << ", char " << e.getColumnNumber()
            << "\n  Message: " << XMLCh2String(e.getMessage()));
}

void XmlBoardsSettings::resetErrors() {
    sawErrors = false;
}

