/*
 * FEBsTimingTester.cpp
 *
 *  Created on: Oct 10, 2008
 *      Author: tb
 */

#include "rpct/std/tbutil.h"
#include "rpct/hardwareTests/FEBsTimingTester.h"
#include "TCanvas.h"
#include "TStyle.h"


using namespace rpct;
using namespace std;
using namespace log4cplus;

const int FEBsTimingTester::winStep = 10; //TODO

Logger FEBsTimingTester::logger = Logger::getInstance("FEBsTimingTester");
void FEBsTimingTester::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}

FEBsTimingTester::FEBsTimingTester(std::string outputDir):
		outputDir(outputDir) {
    time_t timer = time(NULL);
    tm* tblock = localtime(&timer);
    tblock->tm_mon += 1;

    std::stringstream srfile;
    srfile<<outputDir<<"/testFEBsTiming_"
    << timeString() <<".root";

    outRootFile_.Open(srfile.str().c_str(), "RECREATE");
}

FEBsTimingTester::~FEBsTimingTester() {
	outRootFile_.Close();
	//LOG4CPLUS_INFO(FEBsTimingTester::logger, "FEBsTimingTester destructor");
}

std::string FEBsTimingTester::timingTest(double countingTime, FEBsTimingTester::WindowTestType windowTestType,
							      uint32_t pulsePattern, int winOInit, int winCInit, int pulsDistance, IAnalyser* analyser,
							      rpct::LinkSystem::LinkBoards& linkBoards) {

	gStyle->SetPalette(1);
	gStyle->SetOptStat(0);
    ostringstream resultsLog;
/*    int winOInit = 0; //TODO
    int winCInit = 100;
    int pulsDistance = 16;
*/
    unsigned int clkInv = 1;

    const unsigned int secBx = 40000000;

    unsigned int        pulserRepeatEna = 0;
    unsigned int       pulseLen = 256; // lenght of pulse pattern 256
    uint64_t  pulserLimit =  0xfffffffffffffffull; //countingTime * secBx  ; // how many clocks pulser will work

    IDiagCtrl::TTriggerType pulserStartTrigger = IDiagCtrl::ttManual;
    IDiagCtrl::TTriggerType countersStartTrigger = IDiagCtrl::ttManual;

    uint64_t  countersLimit = countingTime * secBx;
    countersLimit =  countersLimit - (countersLimit%pulseLen);

    //---------pulse pattern generation ----------------------------------------
    uint32_t pulseData;
    //uint32_t pulsePattern = 0xffffff;//0xffffff
    std::vector<BigInteger> dataVec;
    for (unsigned int iBx = 0;iBx < pulseLen; iBx++) {
        //if(iBx % pulsDistance >= 4 && iBx % pulsDistance<=7 )//)< 5
        if(iBx <= pulsDistance) {
    	//pulseData = 0;
            pulseData = pulsePattern;
            //pulseData = 0xdddddd;
            //pulseData = 0xffffff;
        }
        else {
            //pulseData = 0xffffff; //FEBS - the FEB pulses have reversed logic
        	//pulseData = 0xffffff&(~pulsePattern);
        	pulseData = 0; //test board
        }
        /*else {
        	pulseData = 0;
        }*/
        dataVec.push_back(BigInteger(pulseData));
    }
    //////////////////

    if(windowTestType == testWinO) {
        resultsLog<<endl<<"window open test"<<endl;
        cout<<"window open test "<<endl;
    }
    else {
        resultsLog<<endl<<"window close test"<<endl;
        cout<<"window close test "<<endl;
        winCInit = 0;
    }


    //-------------- main loop over every selected LB --------------------------------
    for (LinkSystem::LinkBoards::iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); ++iLB) {
        LinkBoard* lb = (*iLB); //
        //lbsVec[iLB]->getStdLb().SetTTCrxDelays(winC, winO, 0, 0);
        lb->getSynCoder().writeBits(SynCoder::BITS_STATUS1_INV_CLOCK, clkInv);
        //lbsVec[iLB]->getSynCoder().writeBits(SynCoder::BITS_GOL_DATA_SEL, golDataSel);

        lb->getSynCoder().getPulser().Configure(dataVec, pulseLen, pulserRepeatEna, 0);
        lb->getSynCoder().getPulserDiagCtrl().Reset(); //<<<<<<<<<<<<<<<<<<<<<<<?????????????????????????
        lb->getSynCoder().getPulserDiagCtrl().Configure(pulserLimit - 1, pulserStartTrigger, 0);

        lb->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().Reset();
        lb->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().Configure(countersLimit-1, countersStartTrigger, 0);


        vector<vector<double> > winEffTab; //[iWinPos][iChanel]
        vector<vector<int> > fulWinTab;
        vector<vector<int> > addWinTab;
        vector<int> winPos; //(winStepsCnt, 0); //[iWinPos]

        boost::dynamic_bitset<> badChannels(96, 0); //<<<<<<<<<<<<<<<<<<<<<<

        resultsLog<<lb->getCrate()->getDescription()<<" "<<lb->getDescription()<<" id "<<lb->getId()<<endl;
        cout<<lb->getCrate()->getDescription()<<" "<<lb->getDescription()<<" id "<<lb->getId()<<endl;

        int winO = winOInit;
        int winC = winCInit;


        lb->getSynCoder().getPulserDiagCtrl().Start();
        for (int iWin = 0; true; iWin++) {

            lb->getSynCoder().getFullRateHistoMgr().Reset();
            lb->getSynCoder().getWinRateHistoMgr().Reset();

            winEffTab.push_back(vector<double>(96, 0));
            fulWinTab.push_back(vector<int>(96, 0));
            addWinTab.push_back(vector<int>(96, 0));

            //Desc1, Decs2
            lb->setTTCrxDelays(winC, winO, 0, 0);

            if(windowTestType == testWinO)
                winPos.push_back(winO);
            else if(windowTestType == testWinC)
                winPos.push_back(winC);

            cout<<"winO " <<winO<<" winC "<<winC<<endl;

            lb->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().Start();

            //::system("TTCciCommand.exe");
            //::system("ttccommand.exe");

            while (!lb->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().CheckCountingEnded()) {
                usleep(100000);
                cout << "*" << flush;
            }

            lb->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().Stop();

            lb->getSynCoder().getFullRateHistoMgr().Refresh();
            lb->getSynCoder().getWinRateHistoMgr().Refresh();

            int binCount = lb->getSynCoder().getFullRateHistoMgr().GetBinCount();
            cout<<"fullHisto "<<"winHisto "<<endl;

            for (int iCh = 0; iCh < binCount; iCh++) {
                int fullBinCnt = lb->getSynCoder().getFullRateHistoMgr().GetLastData()[iCh];
                int winBinCnt = lb->getSynCoder().getWinRateHistoMgr().GetLastData()[iCh];

                fulWinTab[iWin][iCh] = fullBinCnt;
                addWinTab[iWin][iCh] = winBinCnt;

                if (fullBinCnt > 0) {
                    cout << dec << setw(3) << iCh << ": " << dec
                    << setw(8) << fullBinCnt << setw(8) << winBinCnt<<setw(8)<<" ";

                    winEffTab[iWin][iCh] = ((double)winBinCnt) / fullBinCnt;
                    cout <<" "<<dec<<winEffTab[iWin][iCh]<<endl;
                }
                else {
                    cout << dec << setw(3) << iCh << ": " << dec
                    << setw(8) << fullBinCnt << setw(8) << winBinCnt<<setw(8)<<" "<<endl;;
                }

                if(fullBinCnt < countersLimit / pulsDistance * 0.95)
                    badChannels[iCh] = true;
            }
            if(windowTestType == testWinO) {
              //  winStep = 5;
                winO = winO + winStep;
                if(winO > 239)//TODO
                	break;

                /*if(winO >= 215 && winC != 130) {
                    winC = 130; //if I use the test board to direct the pulses to the inputs, the pulse position is ~170-180
                    winO = 200;
                }
                if(winO >= 240) {
                    winO = 0;
                }
                if(winO == 55 && winPos.size() > (240 / winStep) ) {
                    break;
                }*/
            }
            else if(windowTestType == testWinC) {
                winC = winC + winStep; //////?????????????
                if(winC >= 240)
                    break;
            }
        }//end of    for(int iWin = 0; true; iWin++)

        lb->getSynCoder().getPulserDiagCtrl().Stop();
        /*TH1I *hNoise =  0;
        if(false) {
        	cout << "taking noise profile" << endl;
        	lb->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().Reset();
        	lb->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().Configure(2 * secBx, countersStartTrigger, 0);

        	lb->getSynCoder().getFullRateHistoMgr().Reset();
        	//lb->getSynCoder().getWinRateHistoMgr().Reset();

        	lb->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().Start();

        	//::system("TTCciCommand.exe");
        	//::system("ttccommand.exe");

        	while (!lb->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().CheckCountingEnded()) {
        		usleep(100000);
        		cout << "*" << flush;
        	}

        	lb->getSynCoder().getFullRateHistoMgr().GetDiagCtrl().Stop();

        	lb->getSynCoder().getFullRateHistoMgr().Refresh();
        	//lb->getSynCoder().getWinRateHistoMgr().Refresh();

        	int binCount = lb->getSynCoder().getFullRateHistoMgr().GetBinCount();

        	string name = lb->getDescription() + "_noise_FULL";
        	string title = name ;
        	hNoise =new TH1I(name.c_str(), title.c_str(), binCount,0, binCount) ;

        	for (int iCh = 0; iCh < binCount; iCh++) {
        		int fullBinCnt = lb->getSynCoder().getFullRateHistoMgr().GetLastData()[iCh];
        		hNoise->Fill(iCh, fullBinCnt);
        		//int winBinCnt = lb->getSynCoder().getWinRateHistoMgr().GetLastData()[iCh];
        	}

        	hNoise->Write();
        }*/
        cout << "Done" << endl;

        time_t timer = time(NULL);
        tm* tblock = localtime(&timer);
        tblock->tm_mon += 1;

        std::stringstream sfile;

        sfile << outputDir;

        // vector<vector<int> > fulWinTab;
       // vector<vector<int> > addWinTab;
        ostringstream name;
        //--------root name--------------
        name<<lb->getDescription()<<"_FULL_"<<hex<<pulsePattern<<dec<<"_winO_"<<winOInit<<dec<<"_winC_"<<winCInit;
        TH2I h1(name.str().c_str(), name.str().c_str(), (int)fulWinTab[0].size(),0,(int)fulWinTab[0].size(),(int)fulWinTab.size(),0,(int)fulWinTab.size()*winStep) ;

        name.str("");
        name<<lb->getDescription()<<"_WIN_"<<hex<<pulsePattern<<dec<<"_winO_"<<winOInit<<dec<<"_winC_"<<winCInit;
        TH2I h2(name.str().c_str(), name.str().c_str(), (int)addWinTab[0].size(),0,(int)addWinTab[0].size(),(int)addWinTab.size(),0,(int)addWinTab.size()*winStep) ;
        ofstream outFile;
        ostringstream ostr;
        if(windowTestType == testWinO) {

            sfile<<"/"<<lb->getDescription()<<"_0x"<<hex<<pulsePattern<<"_winO.txt";
            outFile.open(sfile.str().c_str());
            outFile<<"lbId "<<lb->getId()
            <<" winCInit "<<winCInit<<" winC "<<winC<<" clkInv "<<clkInv
            <<" winStep "<<winStep<<endl;
        }
        else if(windowTestType == testWinC) {
            sfile<<"/"<<lb->getId()<<"_"<<lb->getDescription()<<"_winC.txt";
            outFile.open(sfile.str().c_str());
            outFile<<"lbId "<<lb->getId()<<" winO "<<winO<<" clkInv "<<clkInv<<endl;
        }
        outFile<<"\t";

        for (u_int iW = 0; iW < winPos.size(); iW++) {
            outFile<<setw(22)<<winPos[iW];
        }
        outFile<<"\t-1"<<endl;

        for (u_int iCh = 0; iCh < winEffTab[0].size(); iCh++) {
            outFile<<iCh<<"\t";
            for (u_int iW = 0; iW < winEffTab.size(); iW++) {
                //outFile<<setw(12)<<winEffTab[iW][iCh];
                outFile<<setw(11)<<fulWinTab[iW][iCh]<<setw(11)<<addWinTab[iW][iCh];
                h1.Fill(iCh,iW*winStep,fulWinTab[iW][iCh]);
                h2.Fill(iCh,iW*winStep,addWinTab[iW][iCh]);

            }

            outFile<<"\t";

            outFile<<endl;
        }

        resultsLog<<analyser->analyse(windowTestType, countersLimit/pulseLen, pulsePattern, winPos, fulWinTab, addWinTab);
        h1.Write();
        h2.Write();

        ostringstream canvasName;
        canvasName<<lb->getDescription()<<"_0x"<<hex<<pulsePattern;

        TCanvas c1(canvasName.str().c_str(), canvasName.str().c_str(), 600, 1000);

        c1.Divide(1, 2);
        c1.cd(2);
        h1.SetMinimum(0);

        h1.Draw("colz");

        c1.cd(1);
        h2.SetMinimum(0);
        h2.Draw("colz");

        string canvasDir = outputDir + "/" +canvasName.str() + ".png";
        c1.Print(canvasDir.c_str());

        outRootFile_.Flush();

        outFile.close();
        /*
        if(hNoise)
        	delete hNoise;*/
        //break;
    }//-------------- end of the main loop over every selected LB --------------------------------

    return resultsLog.str();
}

void FEBsTimingTester::pulseTest(rpct::LinkSystem::LinkBoards& linkBoards) {
    //double countingTime = 20.;

    ostringstream resultsLog;

    int pulsDistance = 128;

    unsigned int clkInv = 1;
    int winO = 0;
    int winC = 220;

    //const unsigned int secBx = 40000000;

    unsigned int        pulserRepeatEna = 0;
    unsigned int       pulseLen = 256; // lenght of pulse pattern 256
    uint64_t pulserLimit =  0xfffffffffffffffull; //countingTime * secBx  ; // how many clocks pulser will work

    IDiagCtrl::TTriggerType pulserStartTrigger = IDiagCtrl::ttPretrigger0;

    //pulserLimit = countingTime * secBx;

    uint32_t pulseData;
    //uint32_t pulsePattern = 0xffffff;//0xffffff
    std::vector<BigInteger> dataVec;
    for (unsigned int iBx = 0;iBx < pulseLen; iBx++) {
    	//FEB
        if(iBx < pulsDistance) {
            pulseData = 0;
            //pulseData = 0x555555;
        	//pulseData = 0xaaaaaa;
        }
        else {
            pulseData = 0xffffff;
        }

/*//test board
        if(iBx >=10 && iBx < 14) {
        	//pulseData = 0;
        	//pulseData = 0x555555;
        	pulseData = 0xffffff;
        }
        else if(iBx >=210 && iBx < 214) {
        	//pulseData = 0;
        	//pulseData = 0xaaaaaa;
        	pulseData = 0xffffff;
        }
        else
        	pulseData = 0;
*/

        dataVec.push_back(BigInteger(pulseData));
    }

    for (rpct::LinkSystem::LinkBoards::iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); ++iLB) {
        LinkBoard* lb = dynamic_cast<LinkBoard*>(*iLB);

        lb->setTTCrxDelays(winC, winO, 0, 0);

        //lbsVec[iLB]->getStdLb().SetTTCrxDelays(winC, winO, 0, 0);
        lb->getSynCoder().writeBits(SynCoder::BITS_STATUS1_INV_CLOCK, clkInv);
        //lbsVec[iLB]->getSynCoder().writeBits(SynCoder::BITS_GOL_DATA_SEL, golDataSel);

        lb->getSynCoder().getPulser().Configure(dataVec, pulseLen, pulserRepeatEna, 0);
        lb->getSynCoder().getPulserDiagCtrl().Reset(); //<<<<<<<<<<<<<<<<<<<<<<<?????????????????????????
        lb->getSynCoder().getPulserDiagCtrl().Configure(pulserLimit - 1, pulserStartTrigger, 0);

        lb->getSynCoder().getPulserDiagCtrl().Start();
    }

/*    LinkBoard* lb = dynamic_cast<LinkBoard*>(*(linkBoards.begin()));
    while (!lb->getSynCoder().getPulserDiagCtrl().CheckCountingEnded()) {
        usleep(100000);
        cout << "*" << flush;
    }

    for (System::HardwareItemList::const_iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); ++iLB) {
        LinkBoard* lb = dynamic_cast<LinkBoard*>(*iLB);
        lb->getSynCoder().getPulserDiagCtrl().Stop();
    }*/

}

int FEBsTimingTester::AnalyserForTestBoard::pulseForOutChannel(uint32_t pulsePattern, int channelNum) {
	int pulseNum = channelNum%2 + 2*( (int)(channelNum/8) );
	return (pulsePattern & (1<<pulseNum))>>pulseNum;
}

string FEBsTimingTester::AnalyserForTestBoard::analyse(
    			WindowTestType windowTestType, int nPulses,
    			uint32_t pulsePattern,
    			std::vector<int> winPos,
    			std::vector<std::vector<int> > fulWinTab,
    			std::vector<std::vector<int> > addWinTab )
{
	ostringstream ostr;
	ostringstream summary;
	std::set<int> badChannels;

	int winPos1 = 90;
	int winPos2 = 110;

	//int winPos1 = 170; //for the old boards
	//int winPos2 = 180;
	int winPos3 = 230;




	for (u_int iCh = 0; iCh < fulWinTab[0].size(); iCh++) {
		//cout<<pulsePattern<<" iCh "<< iCh<<" pulseForOutChannel "<<pulseForOutChannel(pulsePattern, iCh)<<endl;
		for (u_int iW = 0; iW < fulWinTab.size(); iW++) {
			if(fulWinTab[iW][iCh] != (nPulses * pulseForOutChannel(pulsePattern, iCh)) ) {
				ostr<<"channel "<<iCh
						<<": not expected number of counts in the full window. Is "
						<<fulWinTab[iW][iCh]<<", should be "<<(nPulses * pulseForOutChannel(pulsePattern, iCh))<<
						" Window position "<<winPos[iW]<<endl;
				badChannels.insert(iCh);
			}

			if(winPos[iW] < winPos1) {
				if(addWinTab[iW][iCh] != (nPulses * pulseForOutChannel(pulsePattern, iCh))) {
					ostr<<"channel "<<iCh
							<<": not expected number of counts in the adjustable window. Is "
							<<addWinTab[iW][iCh]<<", should be "<<(nPulses * pulseForOutChannel(pulsePattern, iCh))<<
							" Window position "<<winPos[iW]<<endl;
					badChannels.insert(iCh);
				}
			}
			else if(winPos[iW] > winPos1 && winPos[iW] < winPos1) {
				if(addWinTab[iW][iCh] >= (nPulses * pulseForOutChannel(pulsePattern, iCh))) {
					ostr<<"channel "<<iCh
							<<": not expected number of counts in the adjustable window. Is "
							<<addWinTab[iW][iCh]<<", should be less than "<<(nPulses * pulseForOutChannel(pulsePattern, iCh))<<
							" Window position "<<winPos[iW]<<endl;
					badChannels.insert(iCh);
				}
			}
			else if(winPos[iW] > winPos2 && winPos[iW] < winPos3) {
				if(addWinTab[iW][iCh] != 0) {
					ostr<<"channel "<<iCh
							<<": not expected number of counts in the adjustable window. Is "
							<<addWinTab[iW][iCh]<<", should be "<<0<<
							" Window position "<<winPos[iW]<<endl;
					badChannels.insert(iCh);
				}
			}

		}

	}

	if(ostr.str().size() == 0) {
		ostr<<"OK";
	}
	if(badChannels.size() == 0) {
		summary<<"OK\n";
	}
	else {
		summary<<"BAD channels: ";
		for(set<int>::iterator it = badChannels.begin(); it != badChannels.end(); it++) {
			summary<<*it<<" ";
		}
		summary<<"\n";
	}
	LOG4CPLUS_WARN(FEBsTimingTester::logger, ostr.str());
	return summary.str();
}
