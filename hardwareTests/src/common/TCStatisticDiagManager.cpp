#include "rpct/hardwareTests/TCStatisticDiagManager.h"
#include "rpct/ii/IHardwareItem.h"
#include "rpct/std/tbutil.h"
#include "rpct/devices/FixedHardwareSettings.h"
#include "rpct/hardwareTests/RootPicturesWorkaround.h"
#include "TFrame.h"
#include "TROOT.h"
#include "TGaxis.h"
#include "TPaletteAxis.h"
#include <sys/time.h>

using namespace std;
using namespace log4cplus;

namespace rpct {
//Logger TCStatisticDiagManager::logger = Logger::getInstance("StatisticDiagManager");

const unsigned int TCStatisticDiagManager::tbMaxCnt_ = 9;

TCStatisticDiagManager::TCStatisticDiagManager(std::string name,
		std::string picturesDir, TriggerCrate* tc) :
	StatisticDiagManagerBase(name, picturesDir), tc_(tc) {
	string canvasName = name_ + "_rmbc1";
	TCanvas* canvas1 = new TCanvas(canvasName.c_str(), canvasName.c_str(), 300,
			10, 1150, 400);
	canvases_.push_back(canvas1);
	picturesNames_.push_back(canvasName + ".png");
	picturesDirs_.push_back(picturesDir_ + "/" + picturesNames_.back());

	canvas1->Divide(2 * tbMaxCnt_, 1, 0.001, 0.001); //<<<<<<<<<

	//canvas->SetFillColor(21);
	//    canvas1->SetLeftMargin(0.04);
	//    canvas1->SetRightMargin(0.01);
	//    canvas1->SetTopMargin(0.06);
	//    canvas1->SetBottomMargin(0.3);

	//canvas1->cd();

	for (list<TriggerBoard*>::iterator tbIt = tc->getTriggerBoards().begin(); tbIt
			!= tc->getTriggerBoards().end(); tbIt++) {
		TBItems tbItems;
		tbItems.tb_ = (*tbIt);

		tbItems.rmbHistRecord_ = new RMBHistogramRecord(
				&(*tbIt)->GetRmb()->getStatisticsDiagCtrl(),
				(*tbIt)->GetRmb()->getHistoManagers());
		histogramRecords_.push_back(tbItems.rmbHistRecord_);
		unsigned int binCount =
				(*tbIt)->GetRmb()->getHistoManagers().front()->GetBinCount();
		string name = (*tbIt)->getDescription() + "_"
				+ (*tbIt)->GetRmb()->getHistoManagers().front()->GetName();
		string title = (*tbIt)->getDescription();// + " total hits";

		tbItems.rmbTitle_ = new TPaveText(0.05, 0.93, 0.95, 1., "TRNDC");
		tbItems.rmbTitle_->SetBorderSize(1);
		tbItems.rmbTitle_->AddText((title + " rate [Hz]").c_str());
		tbItems.rmbTitle_->GetLine(0)->SetTextSize(0.1);

		tbItems.rmbMaxRateHist_ = new TH1D((name + "_maxRate").c_str(), "",
				binCount, -0.5, binCount - 0.5);
		tbItems.rmbMaxRateHist_->SetStats(kFALSE);
		tbItems.rmbMaxRateHist_->GetXaxis()->SetLabelSize(0.14);
		tbItems.rmbMaxRateHist_->GetYaxis()->SetLabelSize(0.1);
		tbItems.rmbMaxRateHist_->GetYaxis()->SetLabelOffset(0.000001); //nie dziala
		tbItems.rmbMaxRateHist_->GetXaxis()->SetLabelOffset(0.001); //nie dziala
		//tbItems.rmbMaxRateHist_->GetXaxis()->SetTitle(0.12);
		//tbItems.rmbMaxRateHist_->GetYaxis()->SetTitle("rate [Hz]");
		tbItems.rmbMaxRateHist_->SetFillColor(kRed);

		tbItems.rmbMaxRateHist_->SetMinimum(0.1);
		tbItems.rmbMaxRateHist_->SetMaximum(1000000);

		tbItems.rmbMeanRateHist_ = new TH1D((name + "_meanRate").c_str(),
				title.c_str(), binCount, -0.5, binCount - 0.5);
		tbItems.rmbMeanRateHist_->SetStats(kFALSE);
		//tbItems.rmbMeanRateHist_->GetXaxis()->SetLabelSize(0.12);
		//tbItems.rmbMeanRateHist_->GetYaxis()->SetLabelSize(0.08);
		tbItems.rmbMeanRateHist_->SetFillColor(kBlue-9);

		for (unsigned int iCh = 1; iCh < binCount; iCh += 3) {
			ostringstream binName;
			binName << "l " << iCh / 3;
			tbItems.rmbMaxRateHist_->GetXaxis()->SetBinLabel(binCount - iCh,
					binName.str().c_str());
		}

		//tbItems.rmbMaxRateHist_->GetXaxis()->SetNdivisions(2, false);does not work for the alphanumeric labels
		tbItems.rmbMaxRateHist_->GetYaxis()->SetNdivisions(3, true);
		TGaxis::SetMaxDigits(8);

		int tbNum = (*tbIt)->getNumber();

		TVirtualPad* curPad = canvas1->cd(2 * tbNum + 1);
		canvas1->cd(2 * tbNum + 1)->SetPad(curPad->GetXlowNDC(),
				curPad->GetYlowNDC(), curPad->GetXlowNDC() + curPad->GetWNDC()
						* 1.75, curPad->GetYlowNDC() + curPad->GetHNDC());
		curPad->SetBorderSize(1);
		canvas1->cd(2 * tbNum + 1)->SetLeftMargin(0.15);
		canvas1->cd(2 * tbNum + 1)->SetRightMargin(0.05);
		canvas1->cd(2 * tbNum + 1)->SetTopMargin(0.08);
		canvas1->cd(2 * tbNum + 1)->SetBottomMargin(0.12);

		canvas1->cd(2 * tbNum + 1)->SetLogx();
		//canvas1->cd(2 * tbNum + 1)->SetGrid();

		tbItems.rmbMaxRateHist_->Draw("hbar2");
		tbItems.rmbMeanRateHist_->Draw("samehbar2");
		tbItems.rmbMeanRateHist2_ = (TH1D*)tbItems.rmbMeanRateHist_->Clone((name + "_meanRate2").c_str());
		tbItems.rmbMeanRateHist2_->SetFillColor(kBlue);
		tbItems.rmbMeanRateHist2_->Draw("samehbar2");
		tbItems.rmbTitle_->Draw();

		//tbItems.rmbMaxRateHist_->GetYaxis()->LabelsOption("v"); dziala tylko dla labelek alfanumerycznych
		//tbItems.rmbMeanRateHist_->LabelsOption("v", "Y");
		//tbItems.rmbMaxRateHist_->GetYaxis()->SetNoExponent(kTRUE);

		canvas1->cd(2 * tbNum + 2);
		curPad = canvas1->cd(2 * tbNum + 2);
		canvas1->cd(2 * tbNum + 2)->SetPad(curPad->GetXlowNDC()
				+ curPad->GetWNDC() * 0.75, curPad->GetYlowNDC(),
				curPad->GetXlowNDC() + curPad->GetWNDC(), curPad->GetYlowNDC()
						+ curPad->GetHNDC());
		curPad->SetBorderSize(1);
		canvas1->cd(2 * tbNum + 2)->SetLeftMargin(0.01);
		canvas1->cd(2 * tbNum + 2)->SetRightMargin(0.15);
		canvas1->cd(2 * tbNum + 2)->SetTopMargin(0.08);
		canvas1->cd(2 * tbNum + 2)->SetBottomMargin(0.12);

		tbItems.optLinksStatusHist_ = new TH2D(
				(name + "_optLinksStatus").c_str(), "", 1, 0., 1., 18, 1, 18);

		tbItems.optLinksStatusHist_->SetStats(kFALSE);
		tbItems.optLinksStatusHist_->GetXaxis()->SetLabelSize(0);
		tbItems.optLinksStatusHist_->GetYaxis()->SetLabelSize(0);
		tbItems.optLinksStatusHist_->GetXaxis()->SetTickLength(0.);
		tbItems.optLinksStatusHist_->GetYaxis()->SetNdivisions(18, false);

		tbItems.optLinksStatusHist_->GetZaxis()->SetRangeUser(0, 5);
		tbItems.optLinksStatusHist_->Draw("COL");

		for (unsigned int localTowerNumber = 0; localTowerNumber < 4; localTowerNumber++)
			tbItems.previous.push_back(0);

		tbItemsVec_.push_back(tbItems);

	}

	//---------------------------------------GBS--------------
	canvasName = name_ + "_gbs1";
	canvas1 = new TCanvas(canvasName.c_str(), canvasName.c_str(), 300, 10,
			1150, 400);
	canvases_.push_back(canvas1);
	picturesNames_.push_back(canvasName + ".png");
	picturesDirs_.push_back(picturesDir_ + "/" + picturesNames_.back());
/*	cout << "debuuuuuuuug " << dec << __FILE__ << " " << __LINE__ << " "
			<< __FUNCTION__ << " creating canvas " << canvasName << endl
			<< flush;*/

	canvas1->Divide(1, 2, 0.001, 0.001); //<<<<<<<<<

	//canvas->SetFillColor(21);

	canvas1->cd(1)->SetLeftMargin(0.04);
	canvas1->cd(1)->SetRightMargin(0.09);
	canvas1->cd(1)->SetTopMargin(0.06);
	canvas1->cd(1)->SetBottomMargin(0.1);
	canvas1->cd(1)->SetGrid();

	canvas1->cd(2)->SetLeftMargin(0.04);
	canvas1->cd(2)->SetRightMargin(0.09);
	canvas1->cd(2)->SetTopMargin(0.06);
	canvas1->cd(2)->SetBottomMargin(0.1);
	canvas1->cd(2)->SetGrid();

	name = tc_->getDescription() + "_rateInTowers";
	string title = tc_->getDescription() + " rate in towers [Hz]";
	int towersCnt = 2 * 16 + 1;
	gbsHist_ = new TH1D(name.c_str(), title.c_str(), towersCnt, -0.5 - 16, 16
			+ 0.5);
	gbsHist_->SetStats(kFALSE);
	gbsHist_->GetXaxis()->SetLabelSize(0.07);
	gbsHist_->GetYaxis()->SetLabelSize(0.06);
	gbsHist_->GetXaxis()->SetTitle("tower");
	gbsHist_->GetXaxis()->SetTitleSize(0.07);
	gbsHist_->GetXaxis()->SetTitleOffset(0.5);

	canvas1->cd(2);
	gbsHist_->SetFillColor(kGreen);
	gbsHist_->Draw("bar1");

	canvas1->cd(1);
	name = tc_->getDescription() + "_rateInCones";
	title = tc_->getDescription() + " rate in logCones [Hz]";
	gbsHist2_ = new TH2D(name.c_str(), title.c_str(), towersCnt, -0.5 - 16, 16
			+ 0.5, 12, -0.5, 12 - 0.5);
	gbsHist2_->SetStats(kFALSE);
	gbsHist2_->GetXaxis()->SetLabelSize(0.07);
	gbsHist2_->GetYaxis()->SetLabelSize(0.06);

	gbsHist2_->GetXaxis()->SetTitle("tower");
	gbsHist2_->GetXaxis()->SetTitleSize(0.07);
	gbsHist2_->GetXaxis()->SetTitleOffset(0.5);

	gbsHist2_->GetYaxis()->SetTitle("log segment");
	gbsHist2_->GetYaxis()->SetTitleSize(0.1);
	gbsHist2_->GetYaxis()->SetTitleOffset(0.2);

	gbsHist2_->SetMinimum(0.);
	//gbsHist2_->SetMaximum(5);
	gbsHist2_->Draw("COLZ");
	/*TPaletteAxis* palette = (TPaletteAxis*)gbsHist2_->GetListOfFunctions()->FindObject("palette"); not exist yet,
	palette->SetLabelSize(0.05);*/
	for (unsigned int iTB = 0; iTB < tbItemsVec_.size(); iTB++) {
		tbItemsVec_[iTB].gbsHistRecord_ = new GBSHistogramRecord(
				&tbItemsVec_[iTB].tb_->GetGbSort()->getStatisticsDiagCtrl(),
				tbItemsVec_[iTB].tb_->GetGbSort()->getHistoManagers());
		histogramRecords_.push_back(tbItemsVec_[iTB].gbsHistRecord_);
	}

	//--------------TBRateStatistics----------------------------M. Wolszczak

	canvasName = name_ + "_TBRates";
	canvas1 = new TCanvas(canvasName.c_str(), canvasName.c_str(), 200, 10,
			1150, 1600);
	canvas1->Divide(1, tbMaxCnt_, 0.001, 0.001);
	canvases_.push_back(canvas1);
	picturesNames_.push_back(canvasName + ".png");
	picturesDirs_.push_back(picturesDir_ + "/" + picturesNames_.back());
	for (unsigned int iTB = 0; iTB < tbItemsVec_.size(); iTB++) {
		TriggerBoard* tb = tbItemsVec_[iTB].tb_;
		int tbNum = tb->getNumber();

		string name = tb->getDescription();
		canvas1->cd(tbNum +1)->SetLogy(1);
		canvas1->cd(tbNum +1)->SetGrid();
		canvas1->cd(tbNum +1)->SetLeftMargin(0.035); //*2
		canvas1->cd(tbNum +1)->SetRightMargin(0.09);
		canvas1->cd(tbNum +1)->SetTopMargin(0.06);
		canvas1->cd(tbNum +1)->SetBottomMargin(0.1);
		TLegend *leg = new TLegend(0.7738693, 0.8032056, 0.846231, 0.9916725,
				NULL, "brNDC");

		//leg->SetHeader(title.str().c_str());

		std::vector<TGraph*> towerRate; //
		for (unsigned int localTowerNumber = 0; localTowerNumber < tbItemsVec_[iTB].tb_->GetLdPacs().size(); localTowerNumber++) {
			TGraph *gr = new TGraph();
			towerRate.push_back(gr);
			towerRate[localTowerNumber]->SetLineColor(2*localTowerNumber + 2); //3-zielony
			towerRate[localTowerNumber]->SetLineWidth(1);
			towerRate[localTowerNumber]->SetMarkerColor(2*localTowerNumber + 2); //3-zielony
			towerRate[localTowerNumber]->SetMarkerStyle(23);
			towerRate[localTowerNumber]->SetMarkerSize(0.6);
			towerRate[localTowerNumber]->SetTitle("");
			towerRate[localTowerNumber]->GetXaxis()->SetTitle("time");
			towerRate[localTowerNumber]->GetYaxis()->SetTitle("rate [Hz]");
			towerRate[localTowerNumber]->GetXaxis()->SetLabelSize(0.05);
			towerRate[localTowerNumber]->GetYaxis()->SetLabelSize(0.05);
			towerRate[localTowerNumber]->GetXaxis()->SetTimeDisplay(1);
			towerRate[localTowerNumber]->GetXaxis()->SetTimeFormat("%H:%M:%S");

			int tower = tbItemsVec_[iTB].tb_->getTowerNum(localTowerNumber);
			ostringstream legend;
			legend << "Tower " << tower;
			leg->AddEntry(towerRate[localTowerNumber], legend.str().c_str(), "LP");

			if (localTowerNumber == 0) {
				towerRate[localTowerNumber]->Draw("AP");
			} else {
				towerRate[localTowerNumber]->Draw("P");
			}
		}

		ostringstream title;
		title << "Rate from Trigger Board " << name << " [Hz]";
		towerRate[0]->SetTitle(title.str().c_str());
		rateGraphLegendEntries_.push_back(towerRateLegend_.AddEntry(
				towerRate[0], "", "LP"));

		rateGraphs_.push_back(towerRate);
		//	gr->Draw("LP");
		leg->Draw();
	}

	//--------------TBRateStatistics----------------------------M. Wolszczak
	RootPicturesWorkaround::crateRootMacro(name_, picturesDir_);
}

TCStatisticDiagManager::~TCStatisticDiagManager() {
	for (unsigned int iC = 0; iC < canvases_.size(); iC++) {
		delete canvases_[iC];
	}
	canvases_.clear();

	for (unsigned int iTB = 0; iTB < tbItemsVec_.size(); iTB++) {
		delete tbItemsVec_[iTB].rmbMaxRateHist_;
		delete tbItemsVec_[iTB].rmbMeanRateHist_;
		delete tbItemsVec_[iTB].rmbMeanRateHist2_;
		delete tbItemsVec_[iTB].rmbTitle_;
		delete tbItemsVec_[iTB].optLinksStatusHist_;
	}

	for (unsigned int i = 0; i < histogramRecords_.size(); i++) {
		delete histogramRecords_[i];
	}

    for(unsigned int iH = 0; iH < rateGraphs_.size(); iH++) {
    	for(unsigned int iT = 0; iT < rateGraphs_[iH].size(); iT++) {
    		delete rateGraphs_[iH][iT];
    	}
    }

	delete gbsHist_;
	delete gbsHist2_;
}

void TCStatisticDiagManager::createBranches(TTree& tree) {
	for (unsigned int iTB = 0; iTB < tbItemsVec_.size(); iTB++) {
		std::string name = tbItemsVec_[iTB].tb_->getDescription() + "_rmbHist";
		tbItemsVec_[iTB].rmbHistRecord_->createBranch(tree, name);

		name = tbItemsVec_[iTB].tb_->getDescription() + "_gbsHist";
		tbItemsVec_[iTB].gbsHistRecord_->createBranch(tree, name);
	}
}

Monitorable::MonitorStatusList& TCStatisticDiagManager::analyse() {
	monitorStatusList_.clear();
	for (unsigned int iTB = 0; iTB < tbItemsVec_.size(); iTB++) {
		unsigned int
				binCount =
						tbItemsVec_[iTB].rmbHistRecord_->getHistoManagers().front()->GetBinCount();
		double countingTime = tbItemsVec_[iTB].rmbHistRecord_->getTimerValue()
				/ 40000000.;
		double countingTimeInt =
				(tbItemsVec_[iTB].rmbHistRecord_->getTimerValueIntegrated())
						/ 40000000.;

		for (unsigned int iCh = 0; iCh < binCount; iCh++) {
			unsigned int
					binVal =
							tbItemsVec_[iTB].rmbHistRecord_->getHistoManagers().front()->GetLastData()[iCh];

			if (binVal == 0xffffff) {
				//LOG4CPLUS_WARN(logger,tbItemsVec_[iTB].tb_->getDescription() + " rmb counter overflow in bin "<< iCh<< endl);
			}

			double lastRate = ((double) (binVal))/ countingTime;//- tbItemsVec_[iTB].rmbHistRecord_->getBranchData().binsLinkFrameCounters[iCh]))

			tbItemsVec_[iTB].rmbHistRecord_->getBranchData().binsLinkFrameCounters[iCh]
					+= binVal;

			double meanRate = tbItemsVec_[iTB].rmbHistRecord_->getBranchData().binsLinkFrameCounters[iCh] / countingTimeInt + 0.001;
			tbItemsVec_[iTB].rmbMeanRateHist_->SetBinContent(binCount - iCh, meanRate);

			if((binCount - iCh -1)%6 < 3 ){
				tbItemsVec_[iTB].rmbMeanRateHist2_->SetBinContent(binCount - iCh, meanRate);
			}
			else {
				tbItemsVec_[iTB].rmbMeanRateHist2_->SetBinContent(binCount - iCh,0.001);
			}

			if (tbItemsVec_[iTB].rmbMaxRateHist_->GetBinContent(binCount - iCh)
					<= lastRate)
				tbItemsVec_[iTB].rmbMaxRateHist_->SetBinContent(binCount - iCh,
						lastRate + 0.001);
		}

		//cout<<"debuuuuuuuug "<<dec<<__FILE__<<" "<<__LINE__<<" "<<__FUNCTION__<<endl<<flush;
		for (unsigned int iO = 0; iO < tbItemsVec_[iTB].tb_->GetOptos().size(); iO++) {
			//cout<<"debuuuuuuuug "<<dec<<__FILE__<<" "<<__LINE__<<" "<<__FUNCTION__<<endl<<flush;
			for (unsigned int iLink = 0; iLink
					< tbItemsVec_[iTB].tb_->GetOptos()[iO]->getOptLinkStatus().size(); iLink++) {
				//cout<<"debuuuuuuuug "<<dec<<__FILE__<<" "<<__LINE__<<" "<<__FUNCTION__<<" "<<iO * 3. + iLink<<"  "<<tbItemsVec_[iTB].tb_->GetOptos()[iO]->getOptLinkStatus()[iLink]<<endl<<flush;
				tbItemsVec_[iTB].optLinksStatusHist_->SetBinContent(
						1,
						18 - (iO * 3 + iLink),
						tbItemsVec_[iTB].tb_->GetOptos()[iO]->getOptLinkStatus()[iLink]);
				/*                if(iTB % 2)
				 tbItemsVec_[iTB].optLinksStatusHist_->SetBinContent(1 , iO * 3 + iLink +1, tbItemsVec_[iTB].tb_->GetOptos()[iO]->getOptLinkStatus()[iLink] + iO%(6));
				 else
				 tbItemsVec_[iTB].optLinksStatusHist_->SetBinContent(1 , iO * 3 + iLink +1, tbItemsVec_[iTB].tb_->GetOptos()[iO]->getOptLinkStatus()[iLink] + iO%(3));*/
			}
		}
	}

	for (unsigned int iTB = 0; iTB < tbItemsVec_.size(); iTB++) {
		unsigned int binCount = tbItemsVec_[iTB].gbsHistRecord_->getHistoManagers().front()->GetBinCount();

		unsigned int countInTower = 0;
		for (unsigned int iCh = 0; iCh < binCount; iCh++) {
			unsigned int binVal = tbItemsVec_[iTB].gbsHistRecord_->getHistoManagers().front()->GetLastData()[iCh];

			if (binVal == 0xffffffff) {
				LOG4CPLUS_WARN(logger, tbItemsVec_[iTB].tb_->getDescription()
						+ " gbs counter overflow in bin " << iCh << endl);
			}

			tbItemsVec_[iTB].gbsHistRecord_->getBranchData().binsPacMuonCounters[iCh] += binVal;

			int segment = iCh % 12;
			int tower = tbItemsVec_[iTB].tb_->getTowerNum(iCh / 12);

			double rate = (tbItemsVec_[iTB].gbsHistRecord_->getBranchData().binsPacMuonCounters[iCh]) * 40000000.
									/ tbItemsVec_[iTB].gbsHistRecord_->getTimerValueIntegrated();
			if (iCh / 12 < tbItemsVec_[iTB].tb_->GetLdPacs().size()) {
				gbsHist2_->SetBinContent(tower + 1 + 16, segment + 1, rate);

				countInTower += tbItemsVec_[iTB].gbsHistRecord_->getBranchData().binsPacMuonCounters[iCh];
				if (segment == 11) {
					rate = countInTower * 40000000. / tbItemsVec_[iTB].gbsHistRecord_->getTimerValueIntegrated();
					gbsHist_->SetBinContent(tower + 1 + 16, rate);
					countInTower = 0;
				}
			}

		}
	}

	//--------------TBRateStatistics----------------------------M. Wolszczak

	for (unsigned int iTB = 0; iTB < tbItemsVec_.size(); iTB++) {
		unsigned int binCount = tbItemsVec_[iTB].gbsHistRecord_->getHistoManagers().front()->GetBinCount();
		unsigned int countInTower = 0;

		for (unsigned int iCh = 0; iCh < binCount; iCh++) {
			unsigned int binVal = tbItemsVec_[iTB].gbsHistRecord_->getHistoManagers().front()->GetLastData()[iCh];

			if (binVal == 0xffffffff) {
				LOG4CPLUS_WARN(logger, tbItemsVec_[iTB].tb_->getDescription()
						+ " gbs counter overflow in bin " << iCh << endl);
			}

			for (unsigned int localTowerNumber = 0; localTowerNumber < tbItemsVec_[iTB].tb_->GetLdPacs().size(); localTowerNumber++) {
				//suma zliczen w wiezy nr tower+1+16
				if ((localTowerNumber * 12 <= iCh) && (iCh <= (localTowerNumber + 1) * 12)) {
					countInTower += binVal;
					if (iCh == (localTowerNumber + 1) * 12 - 1) {
						unsigned int time = tbItemsVec_[iTB].gbsHistRecord_->getStopTime() - startTime_;
						long double rate = ((double) (countInTower)) //- tbItemsVec_[iTB].previous[localTowerNumber])
												/ tbItemsVec_[iTB].gbsHistRecord_->getTimerValue()
												* 40000000.;
						tbItemsVec_[iTB].previous[localTowerNumber] += countInTower;
						countInTower = 0;
						rateGraphs_[iTB][localTowerNumber]->SetPoint(iteration_, time, rate + 0.011);
						rateGraphs_[iTB][localTowerNumber]->GetXaxis()->SetTitle("time");
						rateGraphs_[iTB][localTowerNumber]->GetYaxis()->SetTitle("rate [Hz]");
						rateGraphs_[iTB][localTowerNumber]->GetXaxis()->SetLabelSize(0.05);
						rateGraphs_[iTB][localTowerNumber]->GetYaxis()->SetLabelSize(0.05);
						rateGraphs_[iTB][localTowerNumber]->GetXaxis()->SetTimeDisplay(1);
						rateGraphs_[iTB][localTowerNumber]->GetXaxis()->SetTimeFormat("%H:%M:%S");
						if(logScale_) {
							rateGraphs_[iTB][localTowerNumber]->SetMinimum(0.01);
							rateGraphs_[iTB][localTowerNumber]->SetMaximum(100000000.);
						}
						else {
							rateGraphs_[iTB][localTowerNumber]->SetMinimum(0);
							rateGraphs_[iTB][localTowerNumber]->SetMaximum();
						}
					}
				}
			}
		}
	}

	//--------------TBRateStatistics----------------------------M. Wolszczak

	//cout<<__FUNCTION__<<"logScale_ "<<logScale_<<endl;
	for (unsigned int iTB = 0; iTB < tbMaxCnt_; iTB++) {
		canvases_[2]->cd(iTB + 1)->SetLogy(logScale_);
	}

	///----------making pictures----------------
	/*for (unsigned int iC = 0; iC < canvases_.size(); iC++) {
		canvases_[iC]->Modified();
		canvases_[iC]->Update();
		canvases_[iC]->Print(picturesDirs_[iC].c_str(), "png");
	}*/

	RootPicturesWorkaround::createPictures(name_, picturesDir_, canvases_, logger);
	iteration_++;

	return monitorStatusList_;
}

void TCStatisticDiagManager::initialize(time_t startTime) {
	StatisticDiagManagerBase::initialize(startTime);
	for (unsigned int iTB = 0; iTB < tbItemsVec_.size(); iTB++) {
		tbItemsVec_[iTB].rmbMaxRateHist_->Reset();
		tbItemsVec_[iTB].rmbMeanRateHist_->Reset();
		tbItemsVec_[iTB].rmbMeanRateHist2_->Reset();

		for (unsigned int iCh = 0; iCh < RMBHistogramRecord::binsCnt; iCh++) {
			tbItemsVec_[iTB].rmbHistRecord_->getBranchData().binsLinkFrameCounters[iCh] = 0;
		}

		for (unsigned int iCh = 0; iCh < GBSHistogramRecord::binsCnt; iCh++) {
			tbItemsVec_[iTB].gbsHistRecord_->getBranchData().binsPacMuonCounters[iCh] = 0;
		}
	}

    for(unsigned int iH = 0; iH < rateGraphs_.size(); iH++) {
    	for(unsigned int iT = 0; iT < rateGraphs_[iH].size(); iT++) {
    		rateGraphs_[iH][iT]->Set(0);
    	}
    }

}

string TCStatisticDiagManager::getHtml() {
	ostringstream ostr;
	ostr << "<br>\n";
	ostr
			<< "<thead>Left: Optical links rate [Hz], blue - mean, red - peak.<br>Right: Links status: white - OFF, blue - ON, green - OK, yellow - WARNING, orange - ERROR, red - RX_ERROR</thead>\n";
	for (unsigned int i = 0; i < picturesNames_.size(); i++) {
		ostr << "<img src=\"" << "/tmp/" << picturesNames_[i]
				<< "\">";
		ostr << "<br>\n";
	}
	return ostr.str();
}

}
