#include "rpct/hardwareTests/StatisticDiagManagerBase.h"
#include "rpct/ii/IHardwareItem.h"
#include "rpct/std/tbutil.h"

#include "TStyle.h"

using namespace std;
using namespace log4cplus;

namespace rpct {
Logger StatisticDiagManagerBase::logger = Logger::getInstance("StatisticDiagManagerBase");
void StatisticDiagManagerBase::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}

StatisticDiagManagerBase::~StatisticDiagManagerBase() {

}


void StatisticDiagManagerBase::resetAndConfigHistos() {
	//const uint64_t secBx = 40000000;
	const uint64_t countersLimit = 0xffffffffffull;//10 * secBx;

    const IDiagCtrl::TTriggerType triggerType = IDiagCtrl::ttManual;

	LOG4CPLUS_INFO(logger, "reseting And Configuring Histos"<< endl);

    for (unsigned int i = 0; i < histogramRecords_.size(); i++) {
        histogramRecords_[i]->getDiagCtrl()->Reset();
        histogramRecords_[i]->getDiagCtrl()->Configure(countersLimit, triggerType, 0);

        for (unsigned int iM = 0; iM < histogramRecords_[i]->getHistoManagers().size(); iM++) {
            histogramRecords_[i]->getHistoManagers()[iM]->Reset();
        }
    }

    LOG4CPLUS_INFO(logger, "reseting And Configuring Histos -  done"<< endl);
}

void StatisticDiagManagerBase::start() {
    timeval timv;
    for (unsigned int i = 0; i < histogramRecords_.size(); i++) {
        histogramRecords_[i]->getDiagCtrl()->Start();
        gettimeofday(&timv, 0);
        histogramRecords_[i]->setStartTimeBuf(timv.tv_sec);
    }
}

void StatisticDiagManagerBase::stop() {
    for (unsigned int i = 0; i < histogramRecords_.size(); i++) {
         histogramRecords_[i]->getDiagCtrl()->Stop();
    }
}

/*
 * It is needed to clear the counters after each readout, as they are to short for the high luminosity rates
 */
bool StatisticDiagManagerBase::readout(bool startAfterReadout) {
    time_t timer = 0;time(NULL);
    for (unsigned int i = 0; i < histogramRecords_.size(); i++)  {
        //histogramRecords_[iLB]->timerValuePrev_ = histogramRecords_[iLB]->timerValue_;
        //w tej implementcji stop zatrzymuje counter, wiec wartosc countera pokazuje, ile bx histogram liiczyl
        //jesli krzysiu zrobi odczytuwanie bez zatrzymywania, trzeba bedzie do tej opcji  wrocic
        uint64_t counter = 0;
        unsigned char* p = (unsigned char*)(&counter);
        dynamic_cast<TDiagCtrl*>(histogramRecords_[i]->getDiagCtrl())->GetCounterValue(p);
        histogramRecords_[i]->setTimerValue(counter);
        histogramRecords_[i]->setTimerValueIntegrated(histogramRecords_[i]->getTimerValueIntegrated() + counter);

        histogramRecords_[i]->getDiagCtrl()->Stop();
        timer = time(NULL);
        histogramRecords_[i]->setStopTime(timer);

        for (unsigned int iM = 0; iM < histogramRecords_[i]->getHistoManagers().size(); iM++) {
            histogramRecords_[i]->getHistoManagers()[iM]->Refresh();
            histogramRecords_[i]->getHistoManagers()[iM]->Reset(); //<<<<<<<<<<<<<<!!!!!!!!!!!!!!!if the reset is done automatically or the counters are longer, remove this
        }

        histogramRecords_[i]->setStartTime(histogramRecords_[i]->getStartTimeBuf());
        if (startAfterReadout) {
            histogramRecords_[i]->getDiagCtrl()->Start();
            timer = time(NULL);
            histogramRecords_[i]->setStartTimeBuf(timer);
        }
    }
    return true;
}

void StatisticDiagManagerBase::initialize(time_t startTime) {
    iteration_ = 0;
    startTime_ = startTime;
    for (unsigned int i = 0; i < histogramRecords_.size(); i++)  {
        histogramRecords_[i]->setStartTimeBuf(0); //Buffer for start of histograming time slice
        histogramRecords_[i]->setTimerValuePrev(0); //previous
        histogramRecords_[i]->setTimerValueIntegrated(0);
    }
}

void StatisticDiagManagerBase::finalize() {
    for(unsigned int iC = 0; iC < canvases_.size(); iC++) {
        canvases_[iC]->Write();
    }
}

bool StatisticDiagManagerBase::countingEnded(uint64_t coutingPeriod) {
    uint64_t counterValue = 0;

    unsigned char* p = (unsigned char*)(&counterValue);
    dynamic_cast<TDiagCtrl*>(histogramRecords_.front()->getDiagCtrl())->GetCounterValue(p);

    if(counterValue > coutingPeriod) {
        return true;
    }
    return false;
}

}
