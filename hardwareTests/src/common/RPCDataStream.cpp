#include "rpct/hardwareTests/RPCDataStream.h"
#include "rpct/devices/LinkBoard.h"
#include "rpct/devices/TriggerBoard.h"
#include "rpct/devices/TTUBoard.h"
#include "rpct/devices/TriggerCrate.h"
#include "rpct/devices/hsb.h"
#include "rpct/devices/fsb.h"

using namespace std;
using namespace log4cplus;
using namespace rpct;

Logger RPCDataStream::logger = Logger::getInstance("RPCDataStream");
void RPCDataStream::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}
/*vector<RPCDataStream::OptLinkDataMap> RPCDataStream::getLBData(unsigned int lbID) {
	vector<RPCDataStream::OptLinkDataMap> optoData;
	for(int i = 0; i < 3; i++) {
		optoData.push_back(getOptLinkData(tcNum, tbNum, optoNum * 3 + i));
	}
	return optoData;
}*/

std::vector<rpct::BigInteger> RPCDataStream::getLbPulses(unsigned int lbId, unsigned int pulseLength) {
    std::vector<rpct::BigInteger> pulsData;
    const unsigned int pulseWidth = 24; //bits
    const unsigned int pulseBitesWidth = pulseWidth/8;

    unsigned char pulseData[pulseBitesWidth];

    BxDataMap::iterator bxIt = bxDataMap_.begin();
    for(unsigned int iBx = 0; iBx < pulseLength; iBx++) {
        unsigned int dataBits = 0;
        bxIt = bxDataMap_.find(iBx);
        if(bxIt != bxDataMap_.end()) {
            RPCBxData::LbDataMap::iterator lbDataIt = bxIt->second->getLbData().find(lbId);
            if(lbDataIt != 	bxIt->second->getLbData().end()) {
                dataBits = lbDataIt->second->getPulserData();
            }
        }
        bitcpy(pulseData, 0, &dataBits, 0, pulseWidth);
        pulsData.push_back(rpct::BigInteger(pulseData, pulseBitesWidth));
    }

    return pulsData;
}

//for Coder or LMUX out
std::vector<rpct::BigInteger> RPCDataStream::getLbCodedPulses(unsigned int lbId, unsigned int pulseLength) {
    OptLinkDataMap codedData;
    for(BxDataMap::iterator bxIt = bxDataMap_.begin(); bxIt != bxDataMap_.end(); bxIt++) {
        RPCBxData::LbDataMap::iterator lbDataIt = bxIt->second->getLbData().find(lbId);
        if(lbDataIt != 	bxIt->second->getLbData().end()) {
            multipelxLmuxData(codedData, *(lbDataIt->second->getCoderData()), bxIt->second->getBxNum());
        }
    }

    std::vector<rpct::BigInteger> pulsData;
    const unsigned int pulseWidth = 24; //bits
    const unsigned int pulseBitesWidth = pulseWidth/8;

    unsigned char pulseData[pulseBitesWidth];
    //uint32_t pulseData = 0;

    for(unsigned int iBx = 0; iBx < pulseLength; iBx++) {
        RPCDataStream::OptLinkDataMap::iterator dataIt = codedData.find(iBx);
        unsigned int dataBits = 0;
        if(dataIt != codedData.end()) {
            dataBits = dataIt->second.toRaw();
            LOG4CPLUS_DEBUG(logger, "bx " << iBx << " lb "<<lbId<<": linkDat "<<hex<<dataBits);
        }
        bitcpy(&pulseData, 0, &dataBits, 0, LmuxBxData::BITS_CNT);

        pulsData.push_back(rpct::BigInteger(pulseData, pulseBitesWidth));
        //pulsData.push_back(rpct::BigInteger(dataBits));
    }
    return pulsData;
}

RPCDataStream::OptLinkDataMap RPCDataStream::getOptLinkData(unsigned int tcNum, unsigned int tbNum, unsigned int optLiknNum) {
    OptLinkDataMap optLinkData;
    for(BxDataMap::iterator bxIt = bxDataMap_.begin(); bxIt != bxDataMap_.end(); bxIt++) {
        //BxDataPtr bxDataPtr = bxIt->second;
        RPCBxData::TcDataMap::iterator tcDataIt = bxIt->second->getTcData().find(tcNum);
        if(tcDataIt != bxIt->second->getTcData().end() ) {
            RPCBxData::TbDataMap::iterator tbDataIt = tcDataIt->second->getTbData().find(tbNum);
            if(tbDataIt != tcDataIt->second->getTbData().end() ) {
                RPCBxData::OptLinksData::iterator optLinkIt = tbDataIt->second->getOptLinksDataMap().find(optLiknNum);
                if(optLinkIt != tbDataIt->second->getOptLinksDataMap().end()) {
                    multipelxLmuxData(optLinkData, *(optLinkIt->second), bxIt->second->getBxNum());
                }
            }
        }
    }
    return optLinkData;
}

vector<RPCDataStream::OptLinkDataMap> RPCDataStream::getOptoData(unsigned int tcNum, unsigned int tbNum, unsigned int optoNum) {
    vector<RPCDataStream::OptLinkDataMap> optoData;
    for(int i = 0; i < 3; i++) {
        optoData.push_back(getOptLinkData(tcNum, tbNum, optoNum * 3 + i));
    }
    return optoData;
}

RPCDataStream::OptLinkDataMap RPCDataStream::getTTUOptLinkData(unsigned int ttuNum, unsigned int optLiknNum) {
    OptLinkDataMap optLinkData;
   /* int tcNum == 12;
    for(BxDataMap::iterator bxIt = bxDataMap_.begin(); bxIt != bxDataMap_.end(); bxIt++) {
        //BxDataPtr bxDataPtr = bxIt->second;
        RPCBxData::TcDataMap::iterator tcDataIt = bxIt->second->getTcData().find(tcNum);
        if(tcDataIt != bxIt->second->getTcData().end() ) {
            RPCBxData::TbDataMap::iterator tbDataIt = tcDataIt->second->getTbData().find(ttuNum);
            if(tbDataIt != tcDataIt->second->getTbData().end() ) {
                RPCBxData::OptLinksData::iterator optLinkIt = tbDataIt->second->getOptLinksDataMap().find(optLiknNum);
                if(optLinkIt != tbDataIt->second->getOptLinksDataMap().end()) {
                    optLinkData.insert(optLinkData.end(), OptLinkDataMap::value_type(bxIt->second->getBxNum(), *dataIt));
                }
            }
        }
    }*/
    return optLinkData;
}

/*we are using anly 2 opt link input for each opto, one opt link input is unused.
 * */
vector<RPCDataStream::OptLinkDataMap> RPCDataStream::getTTUOptoData(unsigned int ttuNum, unsigned int optoNum) {
    vector<RPCDataStream::OptLinkDataMap> optoData;
    for(int i = 0; i < 2; i++) {
        optoData.push_back(getTTUOptLinkData(ttuNum, optoNum * 2 + i));
    }
    return optoData;
}

void RPCDataStream::multipelxLmuxData(OptLinkDataMap& optLinkData, const RPCBxData::LmuxBxDataSet& bxData, unsigned int bxNum) {
    unsigned int firstFreeBx = 0;
    unsigned int currPartDelay = 0;
    if(optLinkData.size() != 0 ) {
        firstFreeBx = optLinkData.rbegin()->first;
        firstFreeBx++;
        //currPartDelay = optLinkData.rbegin()->second.getPartitionDelay();
        //currPartDelay++;
        currPartDelay = firstFreeBx - bxNum;
    }
    if(firstFreeBx <= bxNum) {
        firstFreeBx = bxNum;
        currPartDelay = 0;
    }
    for(RPCBxData::LmuxBxDataSet::const_iterator dataIt = bxData.begin(); dataIt != bxData.end(); dataIt++) {
        if(currPartDelay >= 8) {
            optLinkData.rbegin()->second.setEndOfData(1ul);
            LOG4CPLUS_INFO(logger, "multipelxLmuxData endOfData reach while inserting data for bxNum" << bxNum <<" next records are not inserted ");
            break;
        }
        OptLinkDataMap::iterator lastIt = optLinkData.insert(optLinkData.end(), OptLinkDataMap::value_type(firstFreeBx, *dataIt));
        firstFreeBx++;
        lastIt->second.setPartitionDelay(currPartDelay);
        currPartDelay++;
    }
}

/*std::vector<rpct::BigInteger> RPCDataStream::getPulses(std::vector<RPCDataStream::OptLinkDataMap> optoData, unsigned int pulseLength) {
	std::vector<rpct::BigInteger> pulsData;
	const unsigned int pulseWidth = 24; //bits
   	const unsigned int pulseBitesWidth = pulseWidth/8 +1;

    unsigned char pulseData[pulseBitesWidth];
	for(unsigned int iBx = 0; iBx < pulseLength; iBx++) {
		for(unsigned int iOptLink = 0; iOptLink < 3; iOptLink++) {
			RPCDataStream::OptLinkDataMap::iterator dataIt = optoData[iOptLink].find(iBx);
			unsigned int dataBits = 0;
			if(dataIt != optoData[iOptLink].end()) {
				dataBits = dataIt->second.toRaw();
				LOG4CPLUS_INFO(logger, "bx " << iBx << " link "<<iOptLink<<": linkDat "<<dataIt->second.toString());
			}
			bitcpy(pulseData, iOptLink * LmuxBxData::BITS_CNT, &dataBits, 0, LmuxBxData::BITS_CNT);
		}
		pulsData.push_back(rpct::BigInteger(pulseData, pulseBitesWidth));
	}

	return pulsData;
}*/

std::vector<rpct::BigInteger> RPCDataStream::getPulses(std::vector<RPCDataStream::OptLinkDataMap> optoData, unsigned int pulseLength) {
    std::vector<rpct::BigInteger> pulsData;
    const unsigned int pulseWidth = 57; //bits
    const unsigned int pulseBitesWidth = (pulseWidth -1)/8 +1;

    unsigned char pulseData[pulseBitesWidth];
    for(unsigned int iBx = 0; iBx < pulseLength; iBx++) {
        for(unsigned int iOptLink = 0; iOptLink < 3; iOptLink++) {
            RPCDataStream::OptLinkDataMap::iterator dataIt = optoData[iOptLink].find(iBx);
            unsigned int dataBits = 0;
            if(dataIt != optoData[iOptLink].end()) {
                dataBits = dataIt->second.toRaw();
                LOG4CPLUS_DEBUG(logger, "bx " << iBx << " link "<<iOptLink<<": linkDat "<<dataIt->second.toString());
            }
            bitcpy(pulseData, iOptLink * LmuxBxData::BITS_CNT, &dataBits, 0, LmuxBxData::BITS_CNT);
        }
        pulsData.push_back(rpct::BigInteger(pulseData, pulseBitesWidth));
    }

    return pulsData;
}

std::vector<rpct::BigInteger> RPCDataStream::getTTUPulses(std::vector<RPCDataStream::OptLinkDataMap> optoData, unsigned int pulseLength) {
    std::vector<rpct::BigInteger> pulsData;
    const unsigned int pulseWidth = 60; //bits
    const unsigned int pulseBitesWidth = (pulseWidth -1)/8 +1;

    unsigned char pulseData[pulseBitesWidth];
    for(unsigned int iBx = 0; iBx < pulseLength; iBx++) {
        for(unsigned int iOptLink = 0; iOptLink < 3; iOptLink++) {
            RPCDataStream::OptLinkDataMap::iterator dataIt = optoData[iOptLink].find(iBx);
            unsigned int dataBits = 0;
            if(dataIt != optoData[iOptLink].end()) {
                dataBits = dataIt->second.getRaw();
                LOG4CPLUS_DEBUG(logger, "bx " << iBx << " link "<<iOptLink<<": linkDat "<<hex<<dataBits);
            }
            bitcpy(pulseData, iOptLink * 30, &dataBits, 0, 30);
        }
        pulsData.push_back(rpct::BigInteger(pulseData, pulseBitesWidth));
    }

    return pulsData;
}

RPCDataStream::MuonVecMap RPCDataStream::getPacDataMap(unsigned int tcNum, unsigned int tbNum, unsigned int pacNum) {
    MuonVecMap muonVecMap;

    for(BxDataMap::iterator bxIt = bxDataMap_.begin(); bxIt != bxDataMap_.end(); bxIt++) {
        RPCBxData::TcDataMap::iterator tcDataIt = bxIt->second->getTcData().find(tcNum);
        if(tcDataIt != bxIt->second->getTcData().end() ) {
            RPCBxData::TbDataMap::iterator tbDataIt = tcDataIt->second->getTbData().find(tbNum);
            if(tbDataIt != tcDataIt->second->getTbData().end() ) {
                RPCBxData::PacDataMap::iterator pacDataIt = tbDataIt->second->getPacData().find(pacNum);
                if(pacDataIt != tbDataIt->second->getPacData().end() ) {
                    muonVecMap.insert(MuonVecMap::value_type(bxIt->second->getBxNum(), pacDataIt->second->getMuonVecPtr()));
                }
            }
        }
    }

    return muonVecMap;
}

RPCDataStream::MuonVecMap RPCDataStream::getTbSortInDataMap(unsigned int tcNum, unsigned int tbNum) {
    MuonVecMap muonVecMap;
    for(unsigned int iPac = 0; iPac < RPCConst::m_TOWERS_ON_TB_CNT; iPac++) {
        MuonVecMap muons = getPacDataMap(tcNum, tbNum, iPac);
        for(MuonVecMap::iterator itBx = muons.begin(); itBx != muons.end(); itBx++) {
            MuonVecMap::iterator itOutBx = muonVecMap.find(itBx->first);
            if(itOutBx == muonVecMap.end() ) {
                RPCBxData::MuonVecPtr muVec(new MuonVector(RPCConst::m_TOWERS_ON_TB_CNT * RPCConst::m_SEGMENTS_IN_SECTOR_CNT));
                itOutBx = muonVecMap.insert(muonVecMap.end(), MuonVecMap::value_type(itBx->first, muVec));
            }
            for(unsigned int iMu = 0; iMu < itBx->second->size(); iMu++) {
                (*(itOutBx->second))[iPac * RPCConst::m_SEGMENTS_IN_SECTOR_CNT + iMu] = (*(itBx->second))[iMu];
            }
        }
    }

    return muonVecMap;
}

RPCDataStream::MuonVecMap RPCDataStream::getTbSortDataMap(unsigned int tcNum, unsigned int tbNum) {
    MuonVecMap muonVecMap;
    for(BxDataMap::iterator bxIt = bxDataMap_.begin(); bxIt != bxDataMap_.end(); bxIt++) {
        //BxDataPtr bxDataPtr = bxIt->second;
        RPCBxData::TcDataMap::iterator tcDataIt = bxIt->second->getTcData().find(tcNum);
        if(tcDataIt != bxIt->second->getTcData().end() ) {
            RPCBxData::TbDataMap::iterator tbDataIt = tcDataIt->second->getTbData().find(tbNum);
            if(tbDataIt != tcDataIt->second->getTbData().end() ) {
                muonVecMap.insert(MuonVecMap::value_type(bxIt->second->getBxNum(), tbDataIt->second->getGbSortMuonsVecPtr()));
            }
        }
    }

    return muonVecMap;
}

RPCDataStream::MuonVecMap RPCDataStream::getTcSortInDataMap(unsigned int tcNum) {
    MuonVecMap muonVecMap;
    for(unsigned int iTb = 0; iTb < 9; iTb++) {
        MuonVecMap muons = getTbSortDataMap(tcNum, iTb);
        for(MuonVecMap::iterator itBx = muons.begin(); itBx != muons.end(); itBx++) {
            MuonVecMap::iterator itOutBx = muonVecMap.find(itBx->first);
            if(itOutBx == muonVecMap.end() ) {
                RPCBxData::MuonVecPtr muVec(new MuonVector(9 * RPCConst::m_GBETA_OUT_MUONS_CNT));
                itOutBx = muonVecMap.insert(muonVecMap.end(), MuonVecMap::value_type(itBx->first, muVec));
            }
            for(unsigned int iMu = 0; iMu < itBx->second->size(); iMu++) {
                (*(itOutBx->second))[iTb * RPCConst::m_GBETA_OUT_MUONS_CNT + iMu] = (*(itBx->second))[iMu];
            }
        }
    }

    return muonVecMap;
}

RPCDataStream::MuonVecMap RPCDataStream::getTcSortDataMap(unsigned int tcNum) {
    MuonVecMap muonVecMap;
    for(BxDataMap::iterator bxIt = bxDataMap_.begin(); bxIt != bxDataMap_.end(); bxIt++) {
        //BxDataPtr bxDataPtr = bxIt->second;
        RPCBxData::TcDataMap::iterator tcDataIt = bxIt->second->getTcData().find(tcNum);
        if(tcDataIt != bxIt->second->getTcData().end() ) {
            muonVecMap.insert(MuonVecMap::value_type(bxIt->second->getBxNum(), tcDataIt->second->getGbSortMuonsVecPtr()));
        }
    }

    return muonVecMap;
}


RPCDataStream::MuonVecMap RPCDataStream::getHalfSortOutDataMap(unsigned int hsNum) {
    MuonVecMap muonVecMap;
    for(BxDataMap::iterator bxIt = bxDataMap_.begin(); bxIt != bxDataMap_.end(); bxIt++) {
        //BxDataPtr bxDataPtr = bxIt->second;
        RPCBxData::HalfSorterDataMap::iterator hsDataIt = bxIt->second->getHalfSorterData().find(hsNum);
        if(hsDataIt != bxIt->second->getHalfSorterData().end() ) {
            muonVecMap.insert(MuonVecMap::value_type(bxIt->second->getBxNum(), hsDataIt->second->getGbSortMuonsPtr()));
        }
    }

    return muonVecMap;
}

RPCDataStream::MuonVecMap RPCDataStream::getHalfSortInDataMap(unsigned int hsNum) {
    MuonVecMap tcMuons;
    unsigned int tcBegin;
    if(hsNum == 0)
        tcBegin = 11;
    else
        tcBegin = 5;

    unsigned int tcIndx = 0;
    for(unsigned int iTC = tcBegin; iTC < tcBegin + 8; iTC++) {
        unsigned int tcNum = iTC%12;
        MuonVecMap muons = getTcSortDataMap(tcNum);
        for(MuonVecMap::iterator itBx = muons.begin(); itBx != muons.end(); itBx++) {
            MuonVecMap::iterator itOutBx = tcMuons.find(itBx->first);
            if(itOutBx == tcMuons.end() ) {
                RPCBxData::MuonVecPtr muVec(new MuonVector(8 * RPCConst::m_TCGB_OUT_MUONS_CNT));
                itOutBx = tcMuons.insert(tcMuons.end(), MuonVecMap::value_type(itBx->first, muVec));
            }
            for(unsigned int iMu = 0; iMu < itBx->second->size(); iMu++) {
                (*(itOutBx->second))[tcIndx * RPCConst::m_TCGB_OUT_MUONS_CNT + iMu] = (*(itBx->second))[iMu];
            }
        }
        tcIndx++;
    }

    return tcMuons;
}

RPCDataStream::MuonVecMap RPCDataStream::getFinalSortInDataMap() {
    MuonVecMap muonVecMap;
    for(BxDataMap::iterator bxIt = bxDataMap_.begin(); bxIt != bxDataMap_.end(); bxIt++) {
        //BxDataPtr bxDataPtr = bxIt->second;
        if(bxIt->second->getHalfSorterData().size() > 0) {
            RPCBxData::MuonVecPtr muVec(new MuonVector(2 * 8));
            for(unsigned int hsNum = 0; hsNum < 2; hsNum++) {
                RPCBxData::HalfSorterDataMap::iterator hsDataIt = bxIt->second->getHalfSorterData().find(hsNum);
                if(hsDataIt != bxIt->second->getHalfSorterData().end() ) {
                    for(unsigned int be = 0; be < 2; be++) {
                        for(unsigned int iMu = 0; iMu < (hsDataIt->second->getGbSortMuons())[be]->size(); iMu++) {
                            (*muVec)[be * 8 + hsNum * 4 + iMu] = (*(hsDataIt->second->getGbSortMuons())[be])[iMu];
                        }
                    }
                }
            }
            muonVecMap.insert(muonVecMap.end(), MuonVecMap::value_type(bxIt->first, muVec));
        }
    }
    return muonVecMap;
}

RPCDataStream::MuonVecMap RPCDataStream::getFinalSortOutDataMap() {
    MuonVecMap muonVecMap;
    for(BxDataMap::iterator bxIt = bxDataMap_.begin(); bxIt != bxDataMap_.end(); bxIt++) {
        muonVecMap.insert(MuonVecMap::value_type(bxIt->second->getBxNum(), bxIt->second->getFinalSorterData().getGbSortMuonsPtr()));
    }

    return muonVecMap;
}

std::vector<rpct::BigInteger> RPCDataStream::getPulses(RPCDataStream::MuonVecMap& muons, unsigned int pulseWidth, unsigned int muonsCnt, unsigned int muonBitsCnt, unsigned int pulseLength) {
    std::vector<rpct::BigInteger> pulsData;
    const unsigned int pulseBitesWidth = (pulseWidth-1)/8 +1;

    unsigned char zeroPulseData[pulseBitesWidth];
    unsigned char pulseData[pulseBitesWidth];
    for(unsigned int i = 0; i < pulseBitesWidth; i++)
        zeroPulseData[i] = 0;

    rpct::BigInteger zeroData(zeroPulseData, pulseBitesWidth);
    //cout<<"muons.size() "<<muons.size()<<endl;
    RPCDataStream::MuonVecMap::iterator muonsIt = muons.begin();
    for(unsigned int iBx = 0; iBx < pulseLength; iBx++) {
        if(muonsIt != muons.end() && muonsIt->first == iBx) {
            bitcpy(pulseData, 0, zeroPulseData, 0, pulseWidth); //clearing pulseData
            //cout<<"muonsIt->second->size() "<<muonsIt->second->size()<<endl;
            for(unsigned int iMu = 0; iMu < muonsIt->second->size(); iMu++) {
                if(muonsIt->second->at(iMu).get() != 0) {
                    unsigned int muonBits = muonsIt->second->at(iMu)->toBits();
                    bitcpy(pulseData, iMu * muonBitsCnt, &muonBits, 0, muonBitsCnt);
                    LOG4CPLUS_DEBUG(logger, "bx " << iBx << " iMu "<<iMu<<" "<<muonsIt->second->at(iMu)->toString(0)<<" muBits "<<muonBits);
                }
            }
            pulsData.push_back(rpct::BigInteger(pulseData, pulseBitesWidth));
            if(muonsIt != muons.end())
                muonsIt++;
        }
        else
            pulsData.push_back(zeroData);
    }

    return pulsData;
}

std::vector<rpct::BigInteger> RPCDataStream::getPuslsesFor(IDiagnosable* chip, unsigned int target, unsigned int pulseLength) {
    unsigned int pulseWidth = 0;
    IDiagnosable::TDiagVector& diagVector = chip->GetDiags();
    for(unsigned int i = 0; i < diagVector.size(); i++) {
        TPulser* pulser = dynamic_cast<TPulser*>(diagVector[i]);
        if(pulser != 0) {
            pulseWidth = pulser->GetWidth();
        }
    }
    if(pulseWidth == 0) {
        throw TException("RPCDataStream::getPuslsesFor: no TPulser found on the chip");
    }


    if(dynamic_cast<SynCoder*>(chip) != 0) {
        SynCoder* synCoder = dynamic_cast<SynCoder*>(chip);
        unsigned int lbId = synCoder->getBoard().getId();
        if(target == 0 || target == 1) {
            return getLbPulses(lbId, pulseLength);
        }
        if(target == 2) {
            return getLbCodedPulses(lbId, pulseLength);
        }
    }
    else if(dynamic_cast<Opto*>(chip) != 0) {
        Opto* opto = dynamic_cast<Opto*>(chip);
        unsigned int tbNum = (dynamic_cast<TriggerBoard&>(opto->getBoard())).getNumber();
        unsigned int tcNum = (dynamic_cast<TriggerCrate*>(opto->getBoard().getCrate()))->getLogSector();
        LOG4CPLUS_DEBUG(logger, "getPuslsesFor "<<chip->getBoard().getDescription()<<" " <<chip->getDescription()<<" tbNum "<<tbNum<<" tcNum "<<tcNum );
        return getPulses(getOptoData(tcNum, tbNum, opto->getNumber() ), pulseLength);
    }
    else if(dynamic_cast<TTUOpto*>(chip) != 0) {
        TTUOpto* opto = dynamic_cast<TTUOpto*>(chip);
        unsigned int ttuNum = (dynamic_cast<TTUBoard&>(opto->getBoard())).getNumber();
        LOG4CPLUS_DEBUG(logger, "getPuslsesFor "<<chip->getBoard().getDescription()<<" " <<chip->getDescription()<<" ttuNum "<<ttuNum );
        return getPulses(getTTUOptoData(ttuNum, opto->getNumber() ), pulseLength);
    }

    else if(dynamic_cast<Pac*>(chip) != 0) {
        Pac* pac = dynamic_cast<Pac*>(chip);
        unsigned int tbNum = (dynamic_cast<TriggerBoard&>(pac->getBoard())).getNumber();
        unsigned int tcNum = (dynamic_cast<TriggerCrate*>(pac->getBoard().getCrate()))->getLogSector();
        RPCDataStream::MuonVecMap muons = getPacDataMap(tcNum, tbNum, pac->getNumber());
        LOG4CPLUS_DEBUG(logger, "getPuslsesFor "<<chip->getBoard().getDescription()<<" " <<chip->getDescription()<<" tbNum "<<tbNum<<" tcNum "<<tcNum );

        return getPulses(muons, pulseWidth, RPCConst::m_SEGMENTS_IN_SECTOR_CNT, RPCPacOutMuon::getMuonBitsCnt(), pulseLength);
    }

    else if(dynamic_cast<GbSort*>(chip) != 0) {
        GbSort* gbSort = dynamic_cast<GbSort*>(chip);
        unsigned int tbNum = (dynamic_cast<TriggerBoard&>(gbSort->getBoard())).getNumber();
        unsigned int tcNum = (dynamic_cast<TriggerCrate*>(gbSort->getBoard().getCrate()))->getLogSector();
        if(target == 0) {
            RPCDataStream::MuonVecMap muons = getTbSortDataMap(tcNum, tbNum);
            return getPulses(muons, pulseWidth, RPCConst::m_GBETA_OUT_MUONS_CNT, RPCTbGbSortOutMuon::getMuonBitsCnt(), pulseLength);
        }
        else if (target == 1) {
            RPCDataStream::MuonVecMap muons = getTbSortInDataMap(tcNum, tbNum);
            return getPulses(muons, pulseWidth, RPCConst::m_TOWERS_ON_TB_CNT * RPCConst::m_SEGMENTS_IN_SECTOR_CNT, RPCPacOutMuon::getMuonBitsCnt(), pulseLength);
        }
        else
            throw TException("RPCDataStream::getPuslsesFor: target in GbSort is not 0 or 1");
    }
    else if(dynamic_cast<TTCSortTCSort*>(chip) != 0) {
        TTCSortTCSort* tcSort = dynamic_cast<TTCSortTCSort*>(chip);
        //cout<<"aaaaaaaaaaaa"<<endl;
        tcSort->getBoard().getCrate();
        //cout<<"bbbbbbbbbbbb"<<endl;
        (dynamic_cast<TriggerCrate*>(tcSort->getBoard().getCrate()))->getLogSector();
        //cout<<"ccccccccccc"<<endl;
        unsigned int tcNum = (dynamic_cast<TriggerCrate*>(tcSort->getBoard().getCrate()))->getLogSector();
        //cout<<"------------- tcNum : "<<tcNum<<endl;
        if(target == 0) {
            RPCDataStream::MuonVecMap muons = getTcSortDataMap(tcNum);
            return getPulses(muons, pulseWidth, RPCConst::m_GBETA_OUT_MUONS_CNT, RPCTcSortOutMuon::getMuonBitsCnt(), pulseLength);
        }
        else if (target == 1) {
            RPCDataStream::MuonVecMap muons = getTcSortInDataMap(tcNum);
            return getPulses(muons, pulseWidth, 9 * RPCConst::m_GBETA_OUT_MUONS_CNT, RPCTbGbSortOutMuon::getMuonBitsCnt(), pulseLength);
        }
    }
    else if(dynamic_cast<THsbSortHalf*>(chip) != 0) {
        THsbSortHalf* halfSort = dynamic_cast<THsbSortHalf*>(chip);
        unsigned int hsbNum = halfSort->getNumber();
        if(target == 0) {
            RPCDataStream::MuonVecMap muons = getHalfSortOutDataMap(hsbNum);

            return getPulses(muons, pulseWidth, 8, RPCHalfSortOutMuon::getMuonBitsCnt(), pulseLength);
        }
        else if (target == 1) {
            RPCDataStream::MuonVecMap muons = getHalfSortInDataMap(hsbNum);

            return getPulses(muons, pulseWidth, RPCConst::m_GBETA_OUT_MUONS_CNT * 8, RPCTcSortOutMuon::getMuonBitsCnt(), pulseLength);
        }
        else
            throw TException("RPCDataStream::getPuslsesFor: target in THsbSortHalf is not 0 or 1");
    }
    else if(dynamic_cast<TFsbSortFinal*>(chip) != 0) {
        //TFsbSortFinal* finalSort = dynamic_cast<TFsbSortFinal*>(chip);        ;
        if(target == 0) {
            RPCDataStream::MuonVecMap muons = getFinalSortOutDataMap();

            return getPulses(muons, pulseWidth, 8, RPCFinalSortOutMuon::getPulserMuonBitsCnt(), pulseLength);
        }
        else if (target == 1) {
            RPCDataStream::MuonVecMap muons = getFinalSortInDataMap();
            return getPulses(muons, pulseWidth, 2 * 8, RPCHalfSortOutMuon::getMuonBitsCnt(), pulseLength);
        }
        else
            throw TException("RPCDataStream::getPuslsesFor: target in THsbSortHalf is not 0 or 1");
    }
    else {
        throw TException("RPCDataStream::getPuslsesFor not defined fot that type of chip");
    }
}
