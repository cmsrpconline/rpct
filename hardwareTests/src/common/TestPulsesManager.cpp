#include "rpct/hardwareTests/TestPulsesManager.h"
#include "rpct/devices/TriggerCrate.h"
#include "rpct/hardwareTests/XmlBxDataReader.h"
#include "xdaq/exception/Exception.h" 

using namespace std;
using namespace rpct;
using namespace log4cplus;

Logger TestPulsesManager::logger = Logger::getInstance("TestPulsesManager");

TestPulsesManager::TestPulsesManager(string xmlBxdataFileName) {
	XmlBxDataReader xmlBxDataReader;
	dataStream_ = xmlBxDataReader.read(xmlBxdataFileName);	
}

TestPulsesManager::~TestPulsesManager()
{
}

/*std::vector<rpct::BigInteger> getPacPulses(unsigned int tcNum, unsigned int tbNum, unsigned int pacNum, unsigned int pulseLength) {
	for(unsigned int iBx = 0; iBx < pulseLength; iBx++) {
		
	}
	
	
}*/

/*void TestPulsesManager::configurePulsersOnPacs(unsigned int pulseLength, uint64_t  pulserLimit, IDiagCtrl::TTriggerType pulserStartTrigger, const bool pulserRepeatEna) {
	System& system = System::getInstance();
	LOG4CPLUS_INFO(logger, "Configuring pulser on PACs");
	const System::HardwareItemList& chipList = system.getHardwareItemsByType(Pac::TYPE);
    for (System::HardwareItemList::const_iterator iItem = chipList.begin(); iItem != chipList.end(); ++iItem) { 
        Pac* pac = dynamic_cast<Pac*>(*iItem);        
        pac->getNumber();
        unsigned int tbNum = (dynamic_cast<TriggerBoard&>(pac->getBoard())).getNumber();
        unsigned int tcNum = (dynamic_cast<TriggerCrate*>(pac->getBoard().getCrate()))->getLogSector();
        RPCDataStream::MuonVecMap muons = dataStream_->getPacDataMap(tcNum, tbNum, pac->getNumber());
        
        std::vector<BigInteger> data = getPulses(muons, RPCConst::m_SEGMENTS_IN_SECTOR_CNT, RPCTBMuon::PACOut::getMuonBitsCnt(), pulseLength);
        pac->GetPulser().Configure(data, pulseLength, pulserRepeatEna);
		pac->GetPulserDiagCtrl().Reset(); //<<<<<<<<<<<<<<<<<<<<<<<?????????????????????????
        pac->GetPulserDiagCtrl().Configure(pulserLimit - 1, pulserStartTrigger);	
        
        LOG4CPLUS_INFO(logger, "pulser configured on " <<  (*iItem)->getDescription()<<" "<<(*iItem)->getId()); 
    }
}*/


void TestPulsesManager::configurePulsers(HardwareItemType type, unsigned int target, unsigned int pulseLength, uint64_t  pulserLimit, IDiagCtrl::TTriggerType pulserStartTrigger, const bool pulserRepeatEna) {
	System& system = System::getInstance();
	LOG4CPLUS_INFO(logger, "Configuring pulser on " <<  type.getType());
	const System::HardwareItemList& chipList = system.getHardwareItemsByType(type);
	configurePulsers(chipList, target, pulseLength, pulserLimit,pulserStartTrigger, pulserRepeatEna);
}

void TestPulsesManager::configurePulsers(const System::HardwareItemList& chipList, unsigned int target, unsigned int pulseLength, uint64_t  pulserLimit, IDiagCtrl::TTriggerType pulserStartTrigger, const bool pulserRepeatEna) {
    for (System::HardwareItemList::const_iterator iItem = chipList.begin(); iItem != chipList.end(); ++iItem) { 
    	IDiagnosable* chip = dynamic_cast<IDiagnosable*>(*iItem);    	
    	IDiagnosable::TDiagVector& diagVector = chip->GetDiags();
    	unsigned int i = 0;
    	for(; i < diagVector.size(); i++) {
    		TPulser* pulser = dynamic_cast<TPulser*>(diagVector[i]);
    		if(pulser != 0) {
    			std::vector<rpct::BigInteger> pulsData = dataStream_->getPuslsesFor(chip, target, pulseLength);
    			pulser->Configure(pulsData, pulseLength, pulserRepeatEna, target);
    			pulser->GetDiagCtrl().Reset();
    			pulser->GetDiagCtrl().Configure(pulserLimit - 1, pulserStartTrigger, 0);	
    			break;
    		}
    	}
    	if(i == diagVector.size()) {
    		throw TException("TestPulsesManager::configurePulsers: PULSER_DIAG_CTRL not foud on a chip");
    	}
    	        
        LOG4CPLUS_INFO(logger, "pulser configured on " <<  (*iItem)->getDescription()<<" "<<(*iItem)->getId()); 
    }
}

void TestPulsesManager::startPulsers(HardwareItemType type) {
	System& system = System::getInstance();
	LOG4CPLUS_INFO(logger, "Starting pulser on " <<  type.getType());
	const System::HardwareItemList& chipList = system.getHardwareItemsByType(type);
    for (System::HardwareItemList::const_iterator iItem = chipList.begin(); iItem != chipList.end(); ++iItem) {     	
    	IDiagnosable* chip = dynamic_cast<IDiagnosable*>(*iItem);    	
    	IDiagnosable::TDiagVector& diagVector = chip->GetDiags();
    	unsigned int i = 0;
    	for(; i < diagVector.size(); i++) {
    		TPulser* pulser = dynamic_cast<TPulser*>(diagVector[i]);
    		if(pulser != 0) {
    			pulser->GetDiagCtrl().Start();
    			break;
    		}
    	}
    	if(i == diagVector.size()) {
    		throw TException("TestPulsesManager::configurePulsers: PULSER_DIAG_CTRL not foud on a chip");
    	}
        //LOG4CPLUS_INFO(logger, "pulser started on " <<  (*iItem)->getDescription()<<" "<<(*iItem)->getId()); 
    }
}

void TestPulsesManager::stopPulsers(HardwareItemType type) {
	System& system = System::getInstance();
	LOG4CPLUS_INFO(logger, "Stoping pulsers");
	const System::HardwareItemList& chipList = system.getHardwareItemsByType(type);
    for (System::HardwareItemList::const_iterator iItem = chipList.begin(); iItem != chipList.end(); ++iItem) { 
    	IDiagnosable* chip = dynamic_cast<IDiagnosable*>(*iItem);    
    	IDiagnosable::TDiagVector& diagVector = chip->GetDiags();	
    	unsigned int i = 0;
    	for(; i < diagVector.size(); i++) {
    		TPulser* pulser = dynamic_cast<TPulser*>(diagVector[i]);
    		if(pulser != 0) {
    			pulser->GetDiagCtrl().Stop();
    			break;
    		}
    	}
    	if(i == diagVector.size()) {
    		throw TException("TestPulsesManager::configurePulsers: PULSER_DIAG_CTRL not foud on a chip");
    	}    
        //LOG4CPLUS_INFO(logger, "pulser started on " <<  (*iItem)->getDescription()<<" "<<(*iItem)->getId()); 
    }
}

void TestPulsesManager::startReadouts(const System::HardwareItemList& chipList) {
	LOG4CPLUS_INFO(logger, "Starting redouts");
    for (System::HardwareItemList::const_iterator iItem = chipList.begin(); iItem != chipList.end(); ++iItem) {     	
    	IDiagnosable* chip = dynamic_cast<IDiagnosable*>(*iItem);    	                  
        IDiagnosable::TDiagVector& diagVector = chip->GetDiags();	
    	unsigned int i = 0;
    	for(; i < diagVector.size(); i++) {
    		TStandardDiagnosticReadout* readout = dynamic_cast<TStandardDiagnosticReadout*>(diagVector[i]);
    		if(readout != 0) {
    			readout->GetDiagCtrl().Start();
    			break;
    		}
    	}
    	if(i == diagVector.size()) {
    		throw TException("TestPulsesManager::startReadouts: TStandardDiagnosticReadout not foud on a chip");
    	}          
    }
}

void TestPulsesManager::stopReadouts(const System::HardwareItemList& chipList) {
	LOG4CPLUS_INFO(logger, "Stoping redouts");
    for (System::HardwareItemList::const_iterator iItem = chipList.begin(); iItem != chipList.end(); ++iItem) { 
    	IDiagnosable* chip = dynamic_cast<IDiagnosable*>(*iItem);    	
    	IDiagnosable::TDiagVector& diagVector = chip->GetDiags();	
    	unsigned int i = 0;
    	for(; i < diagVector.size(); i++) {
    		TStandardDiagnosticReadout* readout = dynamic_cast<TStandardDiagnosticReadout*>(diagVector[i]);
    		if(readout != 0) {
    			readout->GetDiagCtrl().Stop();
    			break;
    		}
    	}
    	if(i == diagVector.size()) {
    		throw TException("TestPulsesManager::stopReadouts: TStandardDiagnosticReadout not foud on a chip");
    	}        
        //LOG4CPLUS_INFO(logger, "pulser started on " <<  (*iItem)->getDescription()<<" "<<(*iItem)->getId()); 
    }
}


void TestPulsesManager::configureReadouts(const System::HardwareItemList& chipList, int bxNum, unsigned int daqDataDelay, 
                                          unsigned int readoutLimit, IDiagCtrl::TTriggerType diagStartTrigger, 
                                          IDiagnosable::TTriggerDataSel triggerDataSel, uint32_t dataTrgDelay) {
	LOG4CPLUS_INFO(logger, "configuring redouts" <<", no of items on the list "<<chipList.size() );
    for (System::HardwareItemList::const_iterator iItem = chipList.begin(); iItem != chipList.end(); ++iItem) { 
    	IDiagnosable* chip = dynamic_cast<IDiagnosable*>(*iItem);    	   	
    	IDiagnosable::TDiagVector& diagVector = chip->GetDiags();	
    	unsigned int i = 0;
    	for(; i < diagVector.size(); i++) {
    		TStandardDiagnosticReadout* readout = dynamic_cast<TStandardDiagnosticReadout*>(diagVector[i]);
    		if(readout != 0) {
    			readout->Reset();//???????????
		    	readout->GetDiagCtrl().Reset();
		    	readout->SetMask0(bxNum);
		    	readout->Configure(daqDataDelay);
			    readout->GetDiagCtrl().Configure(readoutLimit - 1, diagStartTrigger, 0);
			    chip->ConfigureDiagnosable(triggerDataSel, dataTrgDelay);   
    			break;
    		}
    	}
    	if(i == diagVector.size()) {
    		throw TException("TestPulsesManager::stopReadouts: TStandardDiagnosticReadout not foud on a chip");
    	}    	
       
        LOG4CPLUS_INFO(logger, "readout configured on " <<  (*iItem)->getDescription()<<" "<<(*iItem)->getId()); 
    }
}

bool TestPulsesManager::checkCountingEnded(const System::HardwareItemList& chipList, string diagCntrType) {
	LOG4CPLUS_INFO(logger, "TestPulsesManager::checkCountingEnded " + diagCntrType);	
	bool ended = false;
    for (System::HardwareItemList::const_iterator iItem = chipList.begin(); iItem != chipList.end(); ++iItem) { 
    	IDiagnosable* chip = dynamic_cast<IDiagnosable*>(*iItem);    	
    	IDiagnosable::TDiagCtrlVector& diagCtrlVector = chip->GetDiagCtrls();    	
    	unsigned int i = 0;
    	for(; i < diagCtrlVector.size(); i++) {
	    	if(diagCtrlVector[i]->GetName() == diagCntrType) {  
	    		if(iItem == chipList.begin() ) {
	    			ended = diagCtrlVector[i]->CheckCountingEnded();
	    			if(ended == false)
	    				return false;
	    		}	    		 	    	
    			if(diagCtrlVector[i]->CheckCountingEnded() != ended) {
    				throw TException("TestPulsesManager::checkCountingEnded: counting not ended on" +
    				                  chip->getDescription()); 
    			}
    			break;
    		}
    	}   
    	if(i == diagCtrlVector.size()) {
    		throw TException("TestPulsesManager::checkCountingEnded: " + diagCntrType + " not foud on a chip");
    	}   
        //LOG4CPLUS_INFO(logger, "pulser started on " <<  (*iItem)->getDescription()<<" "<<(*iItem)->getId()); 
    }
    return ended; 
}
