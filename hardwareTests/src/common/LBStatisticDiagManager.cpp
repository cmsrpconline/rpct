/*
 * LBStatisticDiagManager.cpp
 *
 *  Created on: Aug 8, 2008
 *      Author: Karol Bunkowswki
 */

#include "rpct/hardwareTests/LBStatisticDiagManager.h"
#include "rpct/hardwareTests/RootPicturesWorkaround.h"
#include "rpct/devices/FixedHardwareSettings.h"


#include "TAxis.h"
#include "TFile.h"

#include <ctime>
#include <fstream>

using namespace std;
using namespace log4cplus;
namespace rpct {

Logger LBStatisticDiagManager::logger_ = Logger::getInstance("LBStatisticDiagManager");
void LBStatisticDiagManager::setupLogger(std::string namePrefix) {
	logger_ = Logger::getInstance(namePrefix + "." + logger_.getName());
}

bool LBStatisticDiagManager::checkIfStatusIsBad(LinkBoard* lb) {
	if(lb->getSynCoder().checkWarningExist(MonitorItemName::CONTROL_BUS)
			|| lb->getSynCoder().checkWarningExist(MonitorItemName::FIRMWARE)
			|| lb->getSynCoder().checkWarningExist(MonitorItemName::TTCRX)
			|| lb->getSynCoder().checkWarningExist(MonitorItemName::QPLL)
			|| lb->getSynCoder().checkWarningExist(MonitorItemName::HISTOGRAMS_HALTED) ){
		return (true);
	}
	return (false);
}

LBStatisticDiagManager::LBStatisticDiagManager(std::string name, std::string picturesDir, LinkBoards& linkBoards, std::vector<HalfBox*> halfBoxes, bool takeFullHist, bool takeWinHist) :
    linkBoards_(linkBoards), halfBoxes_(halfBoxes), takeFullHist_(takeFullHist), takeWinHist_(takeWinHist),
    legend1_(0.5, 0.94, 0.3, 1.), legend2_(0.7, 0.94, 0.5, 1.),
    towerNameTextBox_(0.3, 0.94, 0.1, 1. , "TRNDC"),
    stopHistoPtr_(0)
    {
    //    diagCtrls_ = diagCtrls;
    //    histoManagers_ = histoManagers;

    name_ = name;
    size_t pos = name_.find('+');
    if( pos != string::npos ) {
        name_[pos] = 'p';
    }
    pos = name_.find('-');
    if( pos != string::npos ) {
        name_[pos] = 'n';
    }

    picturesDir_ = picturesDir;

    iteration_ = 0;
    //-------------------------------------

    int canvasNum = 0;
    lbsPerCanv_ = 20;
    TCanvas* canvas = 0;

    //----------------
    string canvasName = name_ + "_" + string("allLBs");
    canvas = new TCanvas(canvasName.c_str(), canvasName.c_str(), 300,10,1150,250);
    canvases_.push_back(canvas);
    picturesNames_.push_back(canvasName + ".png");
    picturesDirs_.push_back(picturesDir_ + "/" + picturesNames_.back());
    //cout<<"debuuuuuuuug "<<dec<<__FILE__<<" "<<__LINE__<<" "<<__FUNCTION__<<" creating canvas "<<canvasName<<endl<<flush;
    canvas->cd();
    canvas->SetLeftMargin(0.06);
    canvas->SetRightMargin(0.01);
    canvas->SetTopMargin(0.06);
    canvas->SetBottomMargin(0.4);
    canvas->SetGrid();
    gPad->SetLogy();

    allLBsMeanRateHist_ = new TH1D("allLBsMeanRateHist_", "", linkBoards_.size(), -0.5, linkBoards_.size() - 0.5);
    allLBsMeanRateHist_->SetStats(kFALSE);
    //allLBsMeanRateHist_->GetXaxis()->SetLabelSize(0.12);
    //allLBsMeanRateHist_->GetYaxis()->SetLabelSize(0.06);
    allLBsMeanRateHist_->SetFillColor(kGreen);

    allLBsMaxRateHist_ = new TH1D("allLBsMaxRateHist_", "", linkBoards_.size(), -0.5, linkBoards_.size() - 0.5);
    allLBsMaxRateHist_->SetStats(kFALSE);

    allLBsMaxRateHist_->SetFillColor(kRed);

    for (unsigned int iLB = 0; iLB < linkBoards_.size(); iLB++) {
        string boardName = linkBoards_[iLB]->getDescription();
        if(linkBoards_[iLB]->getChamberName().size() != 0) {
            allLBsMaxRateHist_->GetXaxis()->SetBinLabel(iLB + 1, linkBoards_[iLB]->getChamberName().c_str());
        }
        else {
            allLBsMaxRateHist_->GetXaxis()->SetBinLabel(iLB + 1, boardName.c_str());
        }
    }
    allLBsMaxRateHist_->LabelsOption("v", "X");

    allLBsMaxRateHist_->GetXaxis()->SetLabelSize(0.07);
    allLBsMaxRateHist_->GetXaxis()->SetLabelOffset(0.00001);
    allLBsMaxRateHist_->GetYaxis()->SetLabelSize(0.04);

    allLBsMaxRateHist_->GetYaxis()->SetNoExponent(kTRUE);
    allLBsMaxRateHist_->SetMinimum(0.1);
    allLBsMaxRateHist_->SetMaximum(100000000);

    allLBsMaxRateHist_->Draw("bar1");
    allLBsMeanRateHist_->Draw("samebar1");

    std::time_t t = std::time(nullptr);
    char mbstr[50];
    std::strftime(mbstr, sizeof(mbstr), "%Y-%m-%d %H:%M:%S ", std::localtime(&t));

    towerNameTextBox_.AddText((mbstr + name_).c_str());
    towerNameTextBox_.Draw();

    legend1_.AddEntry(allLBsMeanRateHist_, "mean rate [Hz]", "F");
    legend1_.Draw();

    legend2_.AddEntry(allLBsMaxRateHist_, "max (peak) rate [Hz]", "F");
    legend2_.Draw();
    //----------------

    for (unsigned int iLB = 0; iLB < linkBoards_.size(); iLB++) {
        int panelNum = iLB%lbsPerCanv_ * 2;
        if(panelNum == 0) {
            string canvasName = name_ + "_" + string("C") + toString(canvasNum);
            canvas = new TCanvas(canvasName.c_str(), canvasName.c_str(), 300,10,1150,800);
            canvas->Divide(2, lbsPerCanv_, 0.0001, 0.0001);

            canvases_.push_back(canvas);

            picturesNames_.push_back(canvasName + ".png");
            picturesDirs_.push_back(picturesDir_ + "/" + picturesNames_.back());
            //cout<<"debuuuuuuuug "<<dec<<__FILE__<<" "<<__LINE__<<" "<<__FUNCTION__<<" creating canvas "<<canvasName<<endl<<flush;

            canvasNum++;
        }

        double division = 0.7;
        canvas->cd(panelNum +1)->SetPad(canvas->cd(panelNum +1)->GetXlowNDC() , canvas->cd(panelNum +1)->GetYlowNDC() , division, canvas->cd(panelNum +1)->GetYlowNDC() + canvas->cd(panelNum +1)->GetHNDC());
        canvas->cd(panelNum +2)->SetPad(division , canvas->cd(panelNum +1)->GetYlowNDC() , 1, canvas->cd(panelNum +1)->GetYlowNDC() + canvas->cd(panelNum +1)->GetHNDC());

        canvas->cd(panelNum +1);
        canvas->cd(panelNum +1)->SetLeftMargin(0.05);
        canvas->cd(panelNum +1)->SetRightMargin(0.1);
        canvas->cd(panelNum +1)->SetTopMargin(0.05);
        if(panelNum == 0 || iLB == (linkBoards_.size() -1)) {
            canvas->cd(panelNum +1)->SetBottomMargin(0.3);
        }
        else {
            canvas->cd(panelNum +1)->SetBottomMargin(0.07);
        }
        //canvas->cd(panelNum +1)->SetLogy();
        canvas->cd(panelNum +1)->SetGrid();

        string boardName = linkBoards_[iLB]->getDescription();
        HistogramRecord* histogramRecord = new HistogramRecord();
        histogramRecords_.push_back(histogramRecord);

        //graphs for winHists only
        TGraph* graph = new TGraph();
        rateGraphs_.push_back(graph);
        graph->SetLineColor(kRed);
        graph->SetLineWidth(1);
        graph->SetMarkerColor(kRed);
        graph->SetMarkerStyle(23);
        graph->SetMarkerSize(0.3);
        //graph->GetXaxis()->SetTitle("time [s]");
        //graph->GetYaxis()->SetTitle("rate [Hz]");

        if(iLB%lbsPerCanv_ == 0  || iLB == (linkBoards_.size() -1)) {
        	graph->GetXaxis()->SetLabelSize(0.3);
        }
        else {
        	graph->GetXaxis()->SetLabelSize(0.03);
        	//graph->GetXaxis()->SetOption("U");
        }
        //graph->GetYaxis()->SetTitle("rate [Hz]");
        //graph->GetYaxis()->SetTitleOffset(0.16);
        //graph->GetYaxis()->SetTitleSize(0.15);

        graph->GetYaxis()->SetNoExponent(kTRUE);
        graph->GetYaxis()->SetLabelSize(0.2);
        graph->GetYaxis()->SetNdivisions(4);

        graph->SetMinimum(0.);

        graph->GetXaxis()->SetTimeDisplay(1);
        graph->GetXaxis()->SetTimeFormat("%H:%M:%S");




        //graph->SetTitle((boardName + " winHist").c_str());
        graph->SetTitle("");

        TPaveText* hTitle = new TPaveText(0.88, 0.01, 0.999, 0.995, "TRNDC"); //kto to deletuje????????????? TODO
        hTitle->AddText((boardName).c_str()); // + " winHist

        if(linkBoards_[iLB]->getChamberName().size() != 0) {
            hTitle->AddText(linkBoards_[iLB]->getChamberName().c_str());
            hTitle->GetLine(0)->SetTextSize(0.25);
            hTitle->GetLine(1)->SetTextSize(0.25);
        }
        else {
            hTitle->AddText("winHist");
        }
        hTitle->SetTextAlign(23);
        hTitle->SetBorderSize(1);
        graph->Draw("ALP");
        hTitle->Draw();
        //-----------------------------
        canvas->cd(panelNum +2);
        canvas->cd(panelNum +2)->SetLeftMargin(0.15);
        canvas->cd(panelNum +2)->SetRightMargin(0.01);
        canvas->cd(panelNum +2)->SetTopMargin(0.05);
        canvas->cd(panelNum +2)->SetLogy();
        canvas->cd(panelNum +2)->SetGridx();

        string histName = "stripMaxRateHisto" + toString(iLB);
        TH1D* stripMaxRateHisto = new TH1D(histName.c_str(), "", LB_IN_CHANNELS_CNT, -0.5, LB_IN_CHANNELS_CNT - 0.5);
        stripMaxRateHistos_.push_back(stripMaxRateHisto);
        stripMaxRateHisto->SetStats(kFALSE);
        stripMaxRateHisto->GetYaxis()->SetNoExponent(kTRUE);
        stripMaxRateHisto->GetYaxis()->SetLabelSize(0.2);
        stripMaxRateHisto->GetYaxis()->SetNdivisions(4);
        stripMaxRateHisto->GetXaxis()->SetNdivisions(12, kFALSE);
        //stripMaxRateHisto->GetXaxis()->SetLabelSize(0.12);
        //stripMaxRateHisto->GetYaxis()->SetLabelSize(0.06);
        stripMaxRateHisto->SetFillColor(kRed);
        stripMaxRateHisto->SetMinimum(0.1);
        stripMaxRateHisto->SetMaximum(100000);
        stripMaxRateHisto->Draw("bar");//"bar1"

        histName = "stripMeanRateHisto" + toString(iLB);
        TH1D* stripMeanRateHisto = new TH1D(histName.c_str(), "", LB_IN_CHANNELS_CNT, -0.5, LB_IN_CHANNELS_CNT - 0.5);
        stripMeanRateHistos_.push_back(stripMeanRateHisto);
        stripMeanRateHisto->SetStats(kFALSE);
        //stripMeanRateHisto->GetYaxis()->SetNoExponent(kTRUE);
        //stripMeanRateHisto->GetYaxis()->SetLabelSize(0.2);
        //stripMeanRateHisto->GetYaxis()->SetNdivisions(4);
        //allLBsMeanRateHist_->GetXaxis()->SetLabelSize(0.12);
        //allLBsMeanRateHist_->GetYaxis()->SetLabelSize(0.06);
        stripMeanRateHisto->SetFillColor(kGreen);
        stripMeanRateHisto->Draw("samebar");//"bar1"
    }

/*    ofstream picturesMacro(makroDir_.c_str() );
    picturesMacro<<"{"<<endl;
    picturesMacro<<"TFile *f = TFile::Open(\""<<picturesDir_<<"/"<<name_<<"canvases.root"<< "\" );"<<endl;
    picturesMacro<<"TIter next(f->GetListOfKeys());"<<endl;
    picturesMacro<<"TKey *key;"<<endl;
    picturesMacro<<"while ((key = (TKey*)next())) {"<<endl;
    picturesMacro<<"  TClass *cl = gROOT->GetClass(key->GetClassName());"<<endl;
    picturesMacro<<"  if (!cl->InheritsFrom(\"TCanvas\"))"<<endl;
    picturesMacro<<"	continue;"<<endl;
    picturesMacro<<" TCanvas *c = (TCanvas*)key->ReadObj();"<<endl;
    picturesMacro<<" string pictureName = c->GetName();"<<endl;
    picturesMacro<<" pictureName = \"/data/MonitorPictures/\" + pictureName  + \".png\";"<<endl;
    picturesMacro<<" c->Print( pictureName.c_str(), \"png\");"<<endl;
    picturesMacro<<"}"<<endl;
    picturesMacro<<"};"<<endl;

    picturesMacro.close();*/

    RootPicturesWorkaround::crateRootMacro(name_, picturesDir_);

}

LBStatisticDiagManager::~LBStatisticDiagManager() {
    for (unsigned int iLB = 0; iLB < rateGraphs_.size(); iLB++) {
        delete rateGraphs_[iLB];
    }
    rateGraphs_.clear();

    for (unsigned int iLB = 0; iLB < histogramRecords_.size(); iLB++) {
        delete histogramRecords_[iLB];
    }
    histogramRecords_.clear();

    for(unsigned int iC = 0; iC < canvases_.size(); iC++) {
        delete canvases_[iC];
    }
    canvases_.clear();

    //allLBsMeanRateHist_->Delete();
    delete allLBsMeanRateHist_;
    delete allLBsMaxRateHist_;

    for (unsigned int iLB = 0; iLB < stripMeanRateHistos_.size(); iLB++) {
        delete stripMeanRateHistos_[iLB];
        delete stripMaxRateHistos_[iLB];
    }
    stripMeanRateHistos_.clear();

}

void LBStatisticDiagManager::createBranches(TTree& tree) {
    for (unsigned int iLB = 0; iLB < linkBoards_.size(); iLB++) {
        string boardName = linkBoards_[iLB]->getDescription();
        string bName = boardName;
        size_t pos = bName.find('+');
        if( pos != string::npos ) {
            bName[pos] = 'p';
        }
        pos = bName.find('-');
        if( pos != string::npos ) {
            bName[pos] = 'n';
        }

        tree.Branch(bName.c_str(), &(histogramRecords_[iLB]->timerValue_),
                "timerValue/l:startTime_/i:stopTime_/i:binsFull_[96]/i:binsWin_[96]/i");

        uintptr_t timerValueAdr = (uintptr_t)&(histogramRecords_[iLB]->timerValue_);
        uintptr_t startTimeAdr = (uintptr_t)&(histogramRecords_[iLB]->startTime_);
        uintptr_t stopTimeAdr = (uintptr_t)&(histogramRecords_[iLB]->stopTime_);
        uintptr_t binsFullAdr = (uintptr_t)&(histogramRecords_[iLB]->binsFull_);
        uintptr_t binsWinAdr = (uintptr_t)&(histogramRecords_[iLB]->binsWin_);
        if(     (timerValueAdr + 8ul) != startTimeAdr ||
                (startTimeAdr   + 4ul) != stopTimeAdr ||
                (stopTimeAdr    + 4ul) != binsFullAdr ||
                (binsFullAdr+ (4ul * 96ul))!= binsWinAdr )
            //if(iLB == 0)
        {
/*            cout<<"timerValue size "<<sizeof(histogramRecords_[iLB]->timerValue_)<<" adr "<<timerValueAdr<<endl;
            cout<<"startTime_ size "<<sizeof(histogramRecords_[iLB]->startTime_)<<" adr "<<startTimeAdr<<endl;
            cout<<"stopTime_ size "<<sizeof(histogramRecords_[iLB]->stopTime_)<<" adr "<<stopTimeAdr<<endl;
            cout<<"binsFull_ size "<<sizeof(histogramRecords_[iLB]->binsFull_)<<" adr "<<binsFullAdr<<endl;
            cout<<"binsWin_ size "<<sizeof(histogramRecords_[iLB]->binsWin_)<<" adr "<<binsWinAdr<<endl;*/

/*
            unsigned int a = 0;
            cout<<hex<<"size int 0x"<<sizeof(a)<<endl;
            uint32_t b = 0;
            cout<<"size long 0x"<<sizeof(b)<<endl;
            uint64_t c = 0;
            cout<<"size long long 0x"<<sizeof(c)<<endl;
            time_t d = 0;
            cout<<"size time_t 0x"<<sizeof(d)<<endl;
*/

            throw TException("the addresses in the histogramRecords_ are not aligned, tree.Branch created not properly");
        }
    }
}

void LBStatisticDiagManager::resetAndConfigHistos(uint64_t countersLimit, IDiagCtrl::TTriggerType triggerType, uint32_t triggerDelay) {
    /*if(linkBoards_.front()->getHalfBox()->getLBoxAccess().isTTCHardResetEnabled()) {
    	LOG4CPLUS_INFO(logger_, "reseting And Configuring Histos skipped as should be done from FLASH. "<< endl);
    	//in principle always when the loaded from FLASH, the full configuration is done (including histograms)
    	//- if it is stored in FLASH - but isTTCHardResetEnabled should assure that this is a case
    	return;
    }*/

	LOG4CPLUS_INFO(logger_, "reseting And Configuring Histos");
    for (unsigned int iLB = 0; iLB < linkBoards_.size(); iLB++) {
        if(stopHistoPtr_ && *stopHistoPtr_) {
            LOG4CPLUS_INFO(logger_, __FUNCTION__<<": STOP HISTO signal received after processing "<<iLB<<" / "<<linkBoards_.size()<<" boards");
            return;
        }

        if(checkIfStatusIsBad(linkBoards_[iLB])) {
        	LOG4CPLUS_WARN(logger_, __FUNCTION__<<": "<<linkBoards_[iLB]->getDescription()<<" monitoring errors exists, skipping this LB");
        	continue;
        }
        try {
        	linkBoards_[iLB]->getSynCoder().getStatisticsDiagCtrl().Reset();
        	LOG4CPLUS_DEBUG(logger_, "getSynCoder().getStatisticsDiagCtrl().Reset(); done"<< endl);
        	linkBoards_[iLB]->getSynCoder().getStatisticsDiagCtrl().Configure(countersLimit, triggerType, 0);
        	LOG4CPLUS_DEBUG(logger_, "getSynCoder().getStatisticsDiagCtrl().Configure(); done"<< endl);
        	linkBoards_[iLB]->getSynCoder().ConfigureDiagnosable(IDiagnosable::tdsNone, 0ul);
        	if (takeFullHist_) {
        		//linkBoards_[iLB]->getSynCoder().getFullRateHistoMgr().Reset();
        	}
        	if (takeWinHist_) {
        		//linkBoards_[iLB]->getSynCoder().getWinRateHistoMgr().Reset();
        	}
        }
    	catch(std::exception& e) {
    		usleep(FixedHardwareSettings::CONFIGURE_FROM_FLASH_TIME * 2);
    		//Assuming this was due to hard reset, wait to reload finished, * 2 to include the repeating TTCrx reset
    		LOG4CPLUS_WARN(logger_, __FUNCTION__<<":"<<__LINE__<<" "<<linkBoards_[iLB]->getDescription()
    				<<" exception: "<<e.what()<<", repeating getStatisticsDiagCtrl().resetAndConfigHistos()");
    		try {
    			linkBoards_[iLB]->getSynCoder().getStatisticsDiagCtrl().Reset();
    			linkBoards_[iLB]->getSynCoder().getStatisticsDiagCtrl().Configure(countersLimit, triggerType, 0);
    			linkBoards_[iLB]->getSynCoder().ConfigureDiagnosable(IDiagnosable::tdsNone, 0ul);
    		}
    		catch(std::exception& e) {
    			LOG4CPLUS_WARN(logger_, __FUNCTION__<<":"<<__LINE__<<" "<<linkBoards_[iLB]->getDescription()
    			    				<<" exception: "<<e.what()<<" could not reset and configure histograms");

    			std::ostringstream ostr;
    			ostr<<linkBoards_[iLB]->getDescription()<<" histograming halted due to exception during histograms configuration";
    			linkBoards_[iLB]->getSynCoder().addWarning(MonitorableStatus(MonitorItemName::HISTOGRAMS_HALTED, MonitorableStatus::SOFT_WARNING, ostr.str(), 0));
    		}
    	}
    }
    LOG4CPLUS_INFO(logger_, "reseting And Configuring Histos -  done"<< endl);
    //cout<<endl;
}

void LBStatisticDiagManager::start() {
    timeval timv;
    for (unsigned int iLB = 0; iLB < linkBoards_.size(); iLB++) {
        if(checkIfStatusIsBad(linkBoards_[iLB])) {
        	LOG4CPLUS_WARN(logger_, __FUNCTION__<<": "<<linkBoards_[iLB]->getDescription()<<" monitoring errors exists, skipping this LB");
        	continue;
        }

    	try {
    		linkBoards_[iLB]->getSynCoder().getStatisticsDiagCtrl().Start();
    	}
    	catch(std::exception& e) {
    		usleep(FixedHardwareSettings::CONFIGURE_FROM_FLASH_TIME * 2); //wait to reload finished
    		LOG4CPLUS_WARN(logger_, __FUNCTION__<<":"<<__LINE__
    				<<" exception: "<<e.what()<<", repeating getStatisticsDiagCtrl().Start()");

    		try {
    			linkBoards_[iLB]->getSynCoder().getStatisticsDiagCtrl().Start();
    		}
    		catch(std::exception& e) {
    			LOG4CPLUS_WARN(logger_, __FUNCTION__<<":"<<__LINE__<<" "<<linkBoards_[iLB]->getDescription()
    					<<" exception: "<<e.what()<<" could not start histograms");

    			std::ostringstream ostr;
    			ostr<<linkBoards_[iLB]->getDescription()<<" histograming halted due to exception during histograms configuration";
    			linkBoards_[iLB]->getSynCoder().addWarning(MonitorableStatus(MonitorItemName::HISTOGRAMS_HALTED, MonitorableStatus::SOFT_WARNING, ostr.str(), 0));
    		}
    	}

        gettimeofday(&timv, 0);
        histogramRecords_[iLB]->startTimeBuf_ = timv.tv_sec;
    }
}

void LBStatisticDiagManager::stop() {
/*    for (unsigned int iLB = 0; iLB < linkBoards_.size(); iLB++) {
    	if(linkBoards_[iLB]->getSynCoder().getStatisticsDiagCtrl().GetState() == IDiagCtrl::sRunning) {
    		linkBoards_[iLB]->getSynCoder().getStatisticsDiagCtrl().Stop();
    	}
    }*/ //to speed up the stop. the stopping of the histograms is performed also during the reset, so should be fine
}

bool LBStatisticDiagManager::readout(bool startAfterReadout) {
	time_t timer = 0;
	time(NULL);
	LOG4CPLUS_INFO(logger_, __FUNCTION__<<" started");

	boardsReloadedPrepare();
	for (unsigned int iLB = 0; iLB < linkBoards_.size(); iLB++) {
		if(stopHistoPtr_ && *stopHistoPtr_) {
			LOG4CPLUS_INFO(logger_, __FUNCTION__<<": STOP HISTO signal received after processing "<<iLB<<" / "<<linkBoards_.size()<<" boards");
			return (false);
		}
		LinkBoard* lb = linkBoards_[iLB];
		try {//we must handel here both SEUs problems and hard resets
			LOG4CPLUS_DEBUG(logger_, __FUNCTION__<<":"<<__LINE__<<" "<<lb->getDescription()<<" DiagCtrl().GetState "<<lb->getSynCoder().getStatisticsDiagCtrl().GetState());
			if(checkIfStatusIsBad(lb)) {
				LOG4CPLUS_WARN(logger_, __FUNCTION__<<": "<<lb->getDescription()<<" monitoring errors exists, skipping histograms readout");
				linkBoards_[iLB]->getSynCoder().addWarning(MonitorableStatus(MonitorItemName::HISTOGRAMS_HALTED, MonitorableStatus::SOFT_WARNING,
						" histograming halted due to existing errors", 0));
				histogramRecords_[iLB]->timerValue_ = 0;
				continue;
			} //if there was an error during resetAndConfigHistos(), the HISTOGRAMS_HALTED exist, so this board is skipped
			/////////////////////////////////////////////////////////////////////////
			//histogramRecords_[iLB]->timerValuePrev_ = histogramRecords_[iLB]->timerValue_;
			//w tej implementcji stop zatrzymuje counter, wiec wartosc countera pokazuje, ile bx histogram liiczyl
			//jesli krzysiu zrobi odczytywanie bez zatrzymywania, trzeba bedzie do tej opcji  wrocic
			//histogramRecords_[iLB]->timerValue_ =
			lb->getSynCoder().readWord(SynCoder::WORD_STAT_TIMER_COUNT, 0, &(histogramRecords_[iLB]->timerValue_));

			if(linkBoards_[iLB]->getSynCoder().getStatisticsDiagCtrl().GetState() == IDiagCtrl::sRunning) {
				lb->getSynCoder().getStatisticsDiagCtrl().Stop();//<<<<<<<<<<<<<<<<<<<<<<<<<<<<
			}
			else {
				/* here it should be in principle the case when:
				 * 1. in one of the previous iterations there was an exception after the stop()
				 * i.e. during readout or start() (e.g. due to SEU), as the SEU should gone now (due to hard reset)
				 * (checkIfStatusIsBad(lb) assure that), we can try simply to continue with the readout
				 * 2. the SEU was already during the resetAndConfigHistos, so the histograms were not configured on the LB,
				 * and the GetState() is sInit. Then hard reset should clean that problem and restart histogrmas,
				 * so the readout should be correct now. the trick is that the getStatisticsDiagCtrl().Start()
				 * will not throw the EIllegalStateTransition, since TDiagCtrl::CheckStateTransition allows for all
				 * transition from the sInit - whcih in principle is a bug, btut here it works as we want
				 */
			}

			timer = time(NULL);
			histogramRecords_[iLB]->stopTime_ = timer;

			if (takeFullHist_) {
				lb->getSynCoder().getFullRateHistoMgr().Refresh();
			}

			if(stopHistoPtr_ && *stopHistoPtr_) {
				LOG4CPLUS_INFO(logger_, __FUNCTION__<<": STOP HISTO signal received after processing "<<iLB<<" / "<<linkBoards_.size()<<" boards");
				return (false);
			}

			if (takeWinHist_) {
				lb->getSynCoder().getWinRateHistoMgr().Refresh();//<<<<<<<<<<<<<<<<<<<<<<<<<<<<
			}

			if(stopHistoPtr_ && *stopHistoPtr_) {
				LOG4CPLUS_INFO(logger_, __FUNCTION__<<": STOP HISTO signal received after processing "<<iLB<<" / "<<linkBoards_.size()<<" boards");
				return (false);
			}

			histogramRecords_[iLB]->startTime_ = histogramRecords_[iLB]->startTimeBuf_;
			if (startAfterReadout) {
				lb->getSynCoder().getStatisticsDiagCtrl().Start();//<<<<<<<<<<<<<<<<<<<<<<<<<<<<

				timer = time(NULL);
				histogramRecords_[iLB]->startTimeBuf_ = timer;
			}
		}
		catch(std::exception& e) {//Either there was hard reset or error due to SEU, first wee will check about hard reset
			usleep(FixedHardwareSettings::CONFIGURE_FROM_FLASH_TIME * 2); //wait to reload finished
			ReloadState boardsReloadedDetected = rsNoReloadDetected;
			LOG4CPLUS_WARN(logger_, __FUNCTION__<<":"<<__LINE__<<" "<<lb->getDescription()<<" exception during readout() "<<": "<<e.what()<<" starting recovery");
			try {
				boardsReloadedDetected =  boardsReloadedCheck();
			} catch(std::exception& e) {//exception here is rather unlikely,as here only USER_REG2 is readout
				LOG4CPLUS_INFO(logger_, __FUNCTION__<<":"<<__LINE__<<" exception during boardsReloadedCheck() "<<": "<<e.what());
			}

			if(boardsReloadedDetected == rsReloadDetected) {
				if(lb->getSynCoder().getStatisticsDiagCtrl().GetState() == IDiagCtrl::sIdle) {
					//stop was performed, and exception was after, hard reset is already done, we must assure the correct FSM state i.e. sRunning
					LOG4CPLUS_INFO(logger_, __FUNCTION__<<":"<<__LINE__<<" "<<lb->getDescription()<<" reload Detected, repeating getStatisticsDiagCtrl().Start()");
					try {
						lb->getSynCoder().getStatisticsDiagCtrl().Start();//<<<<<<<<<<<<<<<<<<<<<<<<<<<<
					} catch(std::exception& e) {
						LOG4CPLUS_WARN(logger_, __FUNCTION__<<":"<<__LINE__<<" "<<lb->getDescription()<<" exception during histograms restarting after LBs reload detected: "<<": "<<e.what());
					}
					LOG4CPLUS_INFO(logger_, __FUNCTION__<<":"<<__LINE__<<" "<<lb->getDescription()<<" getStatisticsDiagCtrl().Start() done");
				}
				LOG4CPLUS_INFO(logger_, __FUNCTION__<<":"<<__LINE__<<" breaking the readout as reload detected");
				return (false); //data not valid due to firmware reload, breaking the readout here
			}
			else if (boardsReloadedDetected == rsError) {
				//error (wrong value in the USER_REG2) on any LB in this tower, not necessary on this LB
			}

			//no reload detected, but some exception, including the one in during re-starting just above
			std::ostringstream ostr;
			ostr<<lb->getDescription()<<" histograming halted due to exception during histograms readout: "<<e.what();
			lb->getSynCoder().addWarning(MonitorableStatus(MonitorItemName::HISTOGRAMS_HALTED, MonitorableStatus::SOFT_WARNING, ostr.str(), 0));
		}
	}//end LB loop

	ReloadState boardsReloadedDetected = rsNoReloadDetected;
	try {
		boardsReloadedDetected =  boardsReloadedCheck();
		/* we must do it here, as the hard reset can be in the middle of the lb->getSynCoder().getWinRateHistoMgr().Refresh() or
		 * reading WORD_STAT_TIMER_COUNT.
		 */
		if(boardsReloadedDetected == rsReloadDetected) {
			LOG4CPLUS_INFO(logger_, __FUNCTION__<<":"<<__LINE__<<":  LBs reload detected during readout, data will be skipped");
			return false;
		}
	} catch(std::exception& e) {//exception here is rather unlikely,as here only USER_REG2 is readout
		LOG4CPLUS_INFO(logger_, __FUNCTION__<<":"<<__LINE__<<" exception during boardsReloadedCheck() "<<": "<<e.what());
	}

	LOG4CPLUS_DEBUG(logger_, __FUNCTION__<<" finished");
	return (true); //all data are valid
}

void LBStatisticDiagManager::boardsReloadedPrepare() {
	//we need to check in every HalfBox, as the TTC HardReset is sometimes not received in some half boxes....
	for(vector<HalfBox*>::const_iterator iHB = halfBoxes_.begin(); iHB!=halfBoxes_.end(); iHB++) {
		LinkBoard *lb = (*iHB)->getLinkBoards().at(0);
		lb->getSynCoder().writeWord(SynCoder::WORD_USER_REG2, 0, 0xabba);
		LOG4CPLUS_DEBUG(logger_, __FUNCTION__<<" setting WORD_USER_REG2 to 0xabba on "<<lb->getDescription());
	}
}

/**
 * three possible states are possible:
 * no reload detected
 * reload detected
 * wrong value of the WORD_USER_REG2 due to SEU
 */
LBStatisticDiagManager::ReloadState LBStatisticDiagManager::boardsReloadedCheck() {
	bool wasError = false;
	for(vector<HalfBox*>::const_iterator iHB = halfBoxes_.begin(); iHB!=halfBoxes_.end(); iHB++) {
		LinkBoard *lb = (*iHB)->getLinkBoards().at(0);
		uint16_t res = lb->getSynCoder().readWord(SynCoder::WORD_USER_REG2, 0);
		LOG4CPLUS_DEBUG(logger_, __FUNCTION__<<":"<<__LINE__<<" checking on the  "<<lb->getDescription()<<" WORD_USER_REG2 is "<<hex<<res);
		if (res == 0xabba)
			lb->getSynCoder().writeWord(SynCoder::WORD_USER_REG2, 0, 0xffaa);
		else {
			int i;
			for (i = 0; i < 5; ++i) {
				LOG4CPLUS_INFO(logger_, __FUNCTION__<<":"<<__LINE__<<" checking on the  "<<lb->getDescription()<<" WORD_USER_REG2 is "<<hex<<res<<" iteration "<<i);
				if (res == 0xffaa) { //0xffaa is written at the end of the configuration from flash
					LOG4CPLUS_INFO(logger_, __FUNCTION__<<":"<<__LINE__<<" reload detected on "<<lb->getDescription());
					return (rsReloadDetected);
				}
				usleep(FixedHardwareSettings::CONFIGURE_FROM_FLASH_TIME * 2);
				res = lb->getSynCoder().readWord(SynCoder::WORD_USER_REG2, 0);
			}
			if (i == 5) {
				LOG4CPLUS_ERROR(logger_, __FUNCTION__<<":"<<__LINE__<<" "<<lb->getDescription()
						<<" no correct value of the WORD_USER_REG2: read value "<<hex<<res<<" expected "<<0xffaa);
				wasError = true;
			}
		}
	}
	if(wasError)
		return (rsError);
    return (rsNoReloadDetected);
}

void LBStatisticDiagManager::readoutSingleLB(bool startAfterReadout, unsigned int iLB) {
	time_t timer = 0;
	time(NULL);
	//LOG4CPLUS_DEBUG(logger_, __FUNCTION__<<" started");
	if(iLB < 0 || iLB >= linkBoards_.size() ) {
		throw TException("LBStatisticDiagManager::readoutSingleLB: iLB < 0 || iLB >= linkBoards_.size()");
	}

	//histogramRecords_[iLB]->timerValuePrev_ = histogramRecords_[iLB]->timerValue_;
	//w tej implementcji stop zatrzymuje counter, wiec wartosc countera pokazuje, ile bx histogram liiczyl
	//jesli krzysiu zrobi odczytuwanie bez zatrzymywania, trzeba bedzie do tej opcji  wrocic
	//histogramRecords_[iLB]->timerValue_ =
	linkBoards_[iLB]->getSynCoder().readWord(SynCoder::WORD_STAT_TIMER_COUNT, 0, &(histogramRecords_[iLB]->timerValue_));
	linkBoards_[iLB]->getSynCoder().getStatisticsDiagCtrl().Stop();
	timer = time(NULL);
	histogramRecords_[iLB]->stopTime_ = timer;

	if (takeFullHist_) {
		linkBoards_[iLB]->getSynCoder().getFullRateHistoMgr().Refresh();
	}
	if (takeWinHist_) {
		linkBoards_[iLB]->getSynCoder().getWinRateHistoMgr().Refresh();
	}

	histogramRecords_[iLB]->startTime_ = histogramRecords_[iLB]->startTimeBuf_;
	if (startAfterReadout) {
		linkBoards_[iLB]->getSynCoder().getStatisticsDiagCtrl().Start();
		timer = time(NULL);
		histogramRecords_[iLB]->startTimeBuf_ = timer;
	}

	//LOG4CPLUS_DEBUG(logger_, __FUNCTION__<<" finshed");
}

Monitorable::MonitorStatusList& LBStatisticDiagManager::analyse() {
	monitorStatusList_.clear();
    for (unsigned int iLB = 0; iLB < linkBoards_.size(); iLB++) {
        LinkBoard* lb = linkBoards_[iLB];
        if(checkIfStatusIsBad(lb)) {
        	LOG4CPLUS_WARN(logger_, __FUNCTION__<<": "<<lb->getDescription()<<" monitoring errors exists, skipping histograms analysis");
        	Double_t x = 0;
        	Double_t y = 0;
        	rateGraphs_[iLB]->GetPoint(iteration_-1, x, y);//if the i is not correct it is handled inside
        	rateGraphs_[iLB]->SetPoint(iteration_, x, y);//in this way the number of points is the same in every LB
        	continue;
        }

        unsigned int binCount = lb->getSynCoder().getFullRateHistoMgr().GetBinCount();

        uint64_t integratedFullCnt = 0;
        uint64_t integratedFullCntEnabledOnly = 0;
        uint64_t integratedWinCnt = 0;
        double lastFullRate = 0;
        double lastWinRate = 0;

        //double countingTime = (histogramRecords_[iLB]->timerValue_ - histogramRecords_[iLB]->timerValuePrev_) / 40000000.;
        //patrz powyzej
        double countingTime = (histogramRecords_[iLB]->timerValue_) / 40000000.;
        histogramRecords_[iLB]->timerValueIntegrated_ += histogramRecords_[iLB]->timerValue_;//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        double countingTimeInt = (histogramRecords_[iLB]->timerValueIntegrated_) / 40000000.;
        if (takeFullHist_) {
            for (unsigned int iCh = 0; iCh < binCount; iCh++) {
                LinkBoard* lb = linkBoards_[iLB];
                unsigned int binVal = lb->getSynCoder().getFullRateHistoMgr().GetLastData()[iCh];
                if(binVal == 0xffffffff) {
                    LOG4CPLUS_WARN(logger_,lb->getDescription() + " full counter overflow in bin "<< iCh<< endl);
                }
                histogramRecords_[iLB]->binsFull_[iCh] += binVal;
                integratedFullCnt += binVal;

                /*LOG4CPLUS_INFO(logger_,lb->getDescription() + " "<< iCh
                		<<" enabled "<<lb->getSynCoder().getRememberedChannelsEna()[iCh]
                		<<" binsFull_ "<<histogramRecords_[iLB]->binsFull_[iCh]
                		<<" binsWin_ "<<histogramRecords_[iLB]->binsWin_[iCh]<< endl);*/
                if(lb->getSynCoder().getRememberedChannelsEna()[iCh]) {
                	integratedFullCntEnabledOnly += binVal;
                }
            }
            //lastFullRate = ((double)(integratedFullCnt - histogramRecords_[iLB]->lastIntegratedFullCnt_)) / countingTime;
            lastFullRate = ((double)(histogramRecords_[iLB]->lastIntegratedFullCnt_)) / countingTime;
            histogramRecords_[iLB]->lastIntegratedFullCnt_ = integratedFullCnt;
        }

        if (takeWinHist_) {
            for (unsigned int iCh = 0; iCh < binCount; iCh++) {
                LinkBoard* lb = linkBoards_[iLB];
                unsigned int binVal = lb->getSynCoder().getWinRateHistoMgr().GetLastData()[iCh];
                if(binVal == 0xffffffff) {
                    LOG4CPLUS_WARN(logger_,lb->getDescription() + " win counter overflow in bin "<< iCh<< endl);
                }

                histogramRecords_[iLB]->binsWin_[iCh] += binVal; //binsWin_ is unsigned int, it can be overflow

                stripMeanRateHistos_[iLB]->SetBinContent(iCh + 1, (histogramRecords_[iLB]->binsWin_[iCh]) / countingTimeInt  + 0.001);

                double stripRate = ((double)(binVal)) / countingTime;

                if(stripMaxRateHistos_[iLB]->GetBinContent(iCh + 1) <=  stripRate) {
                    stripMaxRateHistos_[iLB]->SetBinContent(iCh + 1, stripRate + 0.001); //o avoid 0 on the log scale
                }

                integratedWinCnt += binVal;
            }

            lastWinRate = ((double)(integratedWinCnt)) / countingTime;
            histogramRecords_[iLB]->lastIntegratedWinCnt_ += integratedWinCnt;
            //lastWinRate = 10;//8 * iteration_ %10 * (iteration_ %3 * 10) + 10;
            rateGraphs_[iLB]->SetPoint(iteration_, histogramRecords_[iLB]->stopTime_ - startTime_, lastWinRate);

            rateGraphs_[iLB]->SetMinimum(0.);
            if(iLB%lbsPerCanv_ == 0  || iLB == (linkBoards_.size() -1)) {
                rateGraphs_[iLB]->GetXaxis()->SetLabelSize(0.3);
            }
            else {
                rateGraphs_[iLB]->GetXaxis()->SetLabelSize(0.03);
                //graph->GetXaxis()->SetOption("U");
            }
            //graph->GetYaxis()->SetTitle("rate [Hz]");
            //graph->GetYaxis()->SetTitleOffset(0.16);
            //graph->GetYaxis()->SetTitleSize(0.15);

            rateGraphs_[iLB]->GetYaxis()->SetNoExponent(kTRUE);
            rateGraphs_[iLB]->GetYaxis()->SetLabelSize(0.2);

            rateGraphs_[iLB]->GetYaxis()->SetNdivisions(4);

            rateGraphs_[iLB]->GetXaxis()->SetTimeDisplay(1);
            rateGraphs_[iLB]->GetXaxis()->SetTimeFormat("%H:%M:%S");

            //rateGraphs_[iLB]->SetMaximum(rateGraphs_.back()->GetMaximum());

            //rateGraphs_[iLB]->GetHistogram()->GetXaxis()->SetRangeUser(rateGraphs_.back()->GetXaxis()->GetXmin(),
              //                                          rateGraphs_.back()->GetXaxis()->GetXmax());

            double meanRate = histogramRecords_[iLB]->lastIntegratedWinCnt_ / countingTimeInt ;
            allLBsMeanRateHist_->SetBinContent(iLB +1, meanRate + 0.001);
            if(allLBsMaxRateHist_->GetBinContent(iLB +1) <= lastWinRate) {
                allLBsMaxRateHist_->SetBinContent(iLB +1, lastWinRate + 0.001);
            }
            LOG4CPLUS_DEBUG(logger_, __FUNCTION__<<":"<<__LINE__<<": "<<linkBoards_[iLB]->getDescription()<<" iteration_ "<<iteration_<<" lastWinRate "<<lastWinRate
            		<<" MaxRate "<<allLBsMaxRateHist_->GetBinContent(iLB +1)<<" countingTimeInt "<<countingTimeInt);

            /*if(lastWinRate == 0) {
            	monitorStatusList_.push_back(MonitorableStatus(MonitorItemName::RATE, MonitorableStatus::WARNING, lb->getDescription()  + " rate too low", lastWinRate));
            }*/

            /*if(lastWinRate > 1e6) {
            	monitorStatusList_.push_back(MonitorableStatus(MonitorItemName::RATE, MonitorableStatus::WARNING, lb->getDescription() + " chamber " + lb ->getChamberName(), lastWinRate));
            }*/
            if(lastWinRate >= 1e6 && rateGraphs_[iLB]->GetN() > 3) {  //this should allow to skip the warning if it was spike, but do we really want this?
            	double lastWinRate1 = 0;
            	double lastWinRate2 = 0;
            	double x = 0;
            	rateGraphs_[iLB]->GetPoint( rateGraphs_[iLB]->GetN() - 2, x, lastWinRate1);
            	rateGraphs_[iLB]->GetPoint( rateGraphs_[iLB]->GetN() - 3, x, lastWinRate2);
            	if(lastWinRate1 >= 1e6 && lastWinRate2 >= 1e6) {
            		monitorStatusList_.push_back(MonitorableStatus(MonitorItemName::RATE, MonitorableStatus::WARNING, lb->getDescription() + " chamber " + lb ->getChamberName(), lastWinRate));
                    LOG4CPLUS_DEBUG(logger_, __FUNCTION__<<":"<<__LINE__<<": "<<linkBoards_[iLB]->getDescription()<<" "<<monitorStatusList_.back().message);
            	}
            }
        }


/*        LOG4CPLUS_INFO(logger_, __FUNCTION__<<": "<<lb->getDescription()<<" "<<lb ->getChamberName()
                				<<": winCnt "<<integratedWinCnt<<" fullCnt "<<integratedFullCntEnabledOnly
                				<<" ratio "<< (integratedWinCnt / (double)integratedFullCntEnabledOnly) );*/
        if(integratedFullCntEnabledOnly > 100)
        {
        	if( (integratedWinCnt < (0.9 * integratedFullCntEnabledOnly) ) ) {
        		LOG4CPLUS_WARN(logger_, __FUNCTION__<<": "<<lb->getDescription()<<" "<<lb ->getChamberName()
        				<<" win/full not correct: winCnt "<<integratedWinCnt<<" fullCnt "<<integratedFullCntEnabledOnly
        				<<" ratio "<< (integratedWinCnt / (double)integratedFullCntEnabledOnly) );

        		ostringstream ostr;
        		ostr<<" not correct win/full = "<<(integratedWinCnt / (double)integratedFullCntEnabledOnly);
        		try {
        			lb->getHalfBox()->prepareAccess();
        			lb->fixTTCrxSettings();

        			lb->getSynCoder().getStatisticsDiagCtrl().Stop();
        			lb->getSynCoder().getStatisticsDiagCtrl().Start();

        			lb->getHalfBox()->finaliseAccess();
        			ostr<<" - correction successful";
        		}
        		catch(std::exception& e) {
        			LOG4CPLUS_WARN(logger_, __FUNCTION__<<":"<<__LINE__<<": "<<linkBoards_[iLB]->getDescription()<<" error during fixing TTCrx settings: "<<e.what());
        			ostr<<" - error during correcting: "<<e.what();
        		}

        		lb->getSynCoder().addWarning(MonitorableStatus(MonitorItemName::WIN_FULL_RATIO, MonitorableStatus::SOFT_WARNING,
        				ostr.str(), 100 * (integratedWinCnt / (double)integratedFullCntEnabledOnly)));
        	}
        }
    }

    ///----------making pictures----------------

    /*if(iteration_%10 == 1) {
    	for(unsigned int iC = 0; iC < canvases_.size(); iC++) {
    		canvases_[iC]->Modified();
    		canvases_[iC]->Update();
    		canvases_[iC]->Print(picturesDirs_[iC].c_str(), "png");
    	}
    }*/

    std::time_t t = std::time(nullptr);
    char mbstr[50];
    std::strftime(mbstr, sizeof(mbstr), "%Y-%m-%d %H:%M:%S ", std::localtime(&t));
    towerNameTextBox_.Clear();
    towerNameTextBox_.AddText((mbstr + name_).c_str());

    RootPicturesWorkaround::createPictures(name_, picturesDir_, canvases_, logger_);
    iteration_++;

    return (monitorStatusList_);
}


void LBStatisticDiagManager::initialize(time_t startTime) {
    startTime_ = startTime;
    iteration_ = 0;
    for(unsigned int i = 0; i < rateGraphs_.size(); i++) {
        rateGraphs_[i]->Set(0);
    }

    allLBsMaxRateHist_->Reset();
    allLBsMeanRateHist_->Reset();

    for (unsigned int iLB = 0; iLB < stripMeanRateHistos_.size(); iLB++) {
        stripMeanRateHistos_[iLB]->Reset();
        stripMaxRateHistos_[iLB]->Reset();
    }

    for (unsigned int iLB = 0; iLB < linkBoards_.size(); iLB++) {
        histogramRecords_[iLB]->startTimeBuf_ = 0; //Buffer for start of histograming time slice
        histogramRecords_[iLB]->lastIntegratedFullCnt_ = 0;
        histogramRecords_[iLB]->lastIntegratedWinCnt_ = 0;
        histogramRecords_[iLB]->timerValuePrev_ = 0;; //previous
        histogramRecords_[iLB]->timerValueIntegrated_ = 0;
        histogramRecords_[iLB]->stopTime_ = 0;
        histogramRecords_[iLB]->startTime_ = 0;;

        unsigned int binCount = linkBoards_[iLB]->getSynCoder().getFullRateHistoMgr().GetBinCount();
        for (unsigned int iCh = 0; iCh < binCount; iCh++) {
            histogramRecords_[iLB]->binsWin_[iCh] = 0;
            histogramRecords_[iLB]->binsFull_[iCh] = 0;
        }
    }
}

void LBStatisticDiagManager::finalize() {
    for(unsigned int iC = 0; iC < canvases_.size(); iC++) {
        canvases_[iC]->Write();
    }
    //cout<<"debuuuuuuuug "<<dec<<__FILE__<<" "<<__LINE__<<" "<<__FUNCTION__<<endl;
}

string LBStatisticDiagManager::getHtml() {
    ostringstream ostr;
    ostr << "<br>\n";
    ostr << "<thead>Integrated rate [Hz] in chamber (window only):</thead>\n";
    for(unsigned int i = 0; i < picturesNames_.size(); i++ ) {
        ostr << "<img src=\""<<"/tmp/"<<picturesNames_[i]<<"\">";
        ostr << "<br>\n";
    }
    return ostr.str();
}




void LBStatisticDiagManager::resetAndConfigHistos() {

  const uint64_t countersLimit = 0xffffffffffull;//10 * secBx;
  
  const IDiagCtrl::TTriggerType triggerType = IDiagCtrl::ttManual;
  resetAndConfigHistos(countersLimit, triggerType, 0);
};

bool LBStatisticDiagManager::countingEnded(uint64_t countingPeriod) {
  uint64_t counterValue = 0;
  //LOG4CPLUS_INFO(logger_, __FUNCTION__<<" start");
      
  for (unsigned int iLB = 0; iLB < linkBoards_.size(); iLB++) {
	  LinkBoard* lb = linkBoards_[iLB];
	  if(checkIfStatusIsBad(lb)) {
		  LOG4CPLUS_WARN(logger_, __FUNCTION__<<": "<<lb->getDescription()<<" monitoring errors exists, skipping this LB");
		  continue;
	  }
	  lb->getSynCoder().readWord(SynCoder::WORD_STAT_TIMER_COUNT, 0, &(counterValue));
	  break;
  }

  if(counterValue > countingPeriod) {
    LOG4CPLUS_INFO(logger_, "countingEnded");
    return (true);
  }
  //LOG4CPLUS_INFO(logger_, __FUNCTION__<<" false");
  return (false);
};

void LBStatisticDiagManager::setLogScale(bool logScale) {

};
}
