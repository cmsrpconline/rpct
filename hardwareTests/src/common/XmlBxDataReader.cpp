
#include "rpct/hardwareTests/XmlBxDataReader.h"
#include "rpct/std/IllegalArgumentException.h"

#include "xoap/domutils.h" 
#include "xdaq/exception/Exception.h" 

#include "rpct/devices/LinkBoard.h"
#include "rpct/devices/TriggerBoard.h"    
#include "rpct/devices/tcsort.h" 
#include "rpct/devices/hsb.h"  
#include "rpct/devices/fsb.h"  


#include <xercesc/parsers/SAXParser.hpp> 
#include <xercesc/framework/MemBufInputSource.hpp>
 
#include <sstream>

using namespace xoap;
using namespace std;
using namespace log4cplus;


namespace rpct
{

Logger XmlBxDataReader::logger = Logger::getInstance("XmlBxDataReader");
void XmlBxDataReader::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}

XmlBxDataReader::XmlBxDataReader() : dataStream_(new RPCDataStream() ) {
}

XmlBxDataReader::~XmlBxDataReader()  {
    //delete bxData_;
}

RPCHardwareMuon* XmlBxDataReader::createMuon(xercesc::AttributeList& attributes, RPCHardwareMuon::MuonType muonType) {
    unsigned int ptCode = 0;
    unsigned int quality = 0;
    unsigned int sign = 0;
    getDecAttribute("pt", attributes, ptCode);
    getDecAttribute("qual", attributes, quality);
    getDecAttribute("sign", attributes, sign);
    RPCHardwareMuon* muon = RPCHardwareMuon::createMuon(ptCode, quality, sign, muonType);
    if (muonType != RPCHardwareMuon::mtPacOut) { // && muonType != RPCHardwareMuon::mbtUnset
        unsigned int phiAddress = 0;
        int etaAddress = 0;
        getDecAttribute("phi", attributes, phiAddress);
        getSignedDecAttribute("eta", attributes, etaAddress);//TODO not sure if this works fine, previously was getDecAttribute, but there was issue with the eta of the HSB muons, KB
        muon->setPhiAddr(phiAddress);
        muon->setEtaAddr(etaAddress);
    }
    if(muonType == RPCHardwareMuon::mtTBSortOut || muonType == RPCHardwareMuon::mtTCSortOut) {
        unsigned int gbData = 0;
        getDecAttribute("gbD", attributes, gbData);
        muon->setGBData(gbData);
    }

    return muon;
}

LmuxBxData XmlBxDataReader::createLmuxBxData(xercesc::AttributeList& attributes) {
	unsigned int partitionData = 0;
	unsigned int partitionNum = 0;
	unsigned int partitionDelay = 0;
	unsigned int endOfData = 0;
	unsigned int halfPartition = 0;
	unsigned int lbNum = 0;
			
	if(getHexAttribute("dat", attributes, partitionData)) {
	    getDecAttribute("par", attributes, partitionNum);
    	getDecAttribute("del", attributes, partitionDelay);
    	getDecAttribute("eod", attributes, endOfData);
    	getDecAttribute("hp", attributes, halfPartition);
    	getDecAttribute("lb", attributes, lbNum);
    	return LmuxBxData(partitionData, partitionNum, partitionDelay, endOfData, halfPartition, lbNum, 0);
	}
	unsigned int raw = 0;
	if(getHexAttribute("raw", attributes, raw) ) {
	    return LmuxBxData(raw, 0);  
	}
	
	return LmuxBxData(0, 0); 	
}

RPCDataStreamPtr XmlBxDataReader::read(string filename) {
    //filename_ = filename;
    //delete bxData_;
    resetErrors();
    SAXParser parser;
    parser.setDocumentHandler(this);
    parser.setErrorHandler(this);
    parser.parse(filename.c_str());
    if (sawErrors_) {
        throw TException("Problems found during reading file " + filename);
    }
    LOG4CPLUS_INFO(logger, "bxData_ succesfully read from " << filename);
    return dataStream_;
}

RPCDataStreamPtr XmlBxDataReader::readString(const char* str) {
    //filename_ = filename;
    //delete bxData_; 
    MemBufInputSource memBufIS((const XMLByte*)str, strlen(str), str);
    
    resetErrors();
    SAXParser parser;
    parser.setDocumentHandler(this);
    parser.setErrorHandler(this);
    
    
    parser.parse(memBufIS);
    
    if (sawErrors_) {
        throw TException("Problems found during reading string " + string(str).substr(0, 100) + ".....");
    }
    //LOG4CPLUS_INFO(logger, "bxData_ succesfully read from string");
    return dataStream_;
}


void XmlBxDataReader::startElement(const XMLCh* const name, AttributeList& attributes) {
    string sname = XMLCh2String(name);
    LOG4CPLUS_DEBUG(logger, "XmlBxDataReader: startElement begin " << sname);
    if (sname == "rpctDataStream") {
    }
    else if (sname == "event") {
        unsigned int num = 0;        
        getDecAttribute("num", attributes, num);     
    }
    else if (sname == "bxData") {
        unsigned int num = 0;        
        getDecAttribute("num", attributes, num);     
        num = mapBxNum(num);   
        lastBxData_ = dataStream_->addBxData(num);
    }
    else if (sname == "tc") {
        unsigned int num = 0;
        getDecAttribute("num", attributes, num);
        lastTcData_ = lastBxData().addTcData(num);
    }
    else if (sname == "tb") {
        unsigned int num = 0;
        getDecAttribute("num", attributes, num);
        lastTbData_ = lastTcData().addTbData(num);
    }
    else if (sname == "pac") {
        unsigned int num = 0;
        getDecAttribute("num", attributes, num);
        lastPacData_= lastTbData().addPacData(num);
        lastMuonVector_ = lastPacData_->getMuonVecPtr();
        lastMuonType_ = RPCHardwareMuon::mtPacOut;
    }
    else if (sname == "tbgs") {
        //unsigned int num = 0;
        //getDecAttribute("num", attributes, num);
        lastMuonVector_ = lastTbData().getGbSortMuonsVecPtr();
        lastMuonType_ = RPCHardwareMuon::mtTBSortOut;
    }
    else if (sname == "tcgs") {
        //unsigned int num = 0;
        //getDecAttribute("num", attributes, num);
        lastMuonVector_ = lastTcData().getGbSortMuonsVecPtr();
        lastMuonType_ = RPCHardwareMuon::mtTCSortOut;
    }
    else if (sname == "hs") {
        unsigned int num = 0;
        unsigned int be = 0;
        getDecAttribute("num", attributes, num);
        getDecAttribute("be", attributes, be);
        RPCBxData::HalfSorterDataPtr halfSorterData = lastBxData().addHalfSorterData(num);
        lastMuonVector_ = halfSorterData->getGbSortMuons()[be];
        lastMuonType_ = RPCHardwareMuon::mtHalfSortOut;
    }
    else if (sname == "fs") {
        unsigned int be = 0;
        getDecAttribute("be", attributes, be);
        lastMuonVector_ = lastBxData().getFinalSorterData().getGbSortMuons()[be];
        lastMuonType_ = RPCHardwareMuon::mtFinalSortOut;
    }
    else if (sname == "mu") {
        unsigned int num = 0;
        getDecAttribute("num", attributes, num);
        RPCHardwareMuon* muon = createMuon(attributes, lastMuonType_);
        if(num >= lastMuonVector().size() ) {
        	throw TException("XmlBxDataReader: num >= lastMuonVector().size()");
        }
        lastMuonVector().at(num).reset(muon);
    }
    else if (sname == "ol") {
        lastOptLinkNum = 0;
        getDecAttribute("num", attributes, lastOptLinkNum);
        lastLmuxBxDataSet_ = lastTbData().addOptLinkData(lastOptLinkNum); 
    }
    else if (sname == "lb") {
        unsigned int id = 0;
        getDecAttribute("id", attributes, id);
                       
        RPCBxData::LbDataPtr lbData = lastBxData().addLbData(id); 
        lastLmuxBxDataSet_ = lbData->getCoderData();

        unsigned int pulserData = 0;
        getHexAttribute("pulse", attributes, pulserData);         
        lbData->setPulserData(pulserData);
    }
    else if (sname == "lmd") {
        LmuxBxData lmuxBxData = createLmuxBxData(attributes);
       	if(lastLmuxBxDataSet_->insert(lmuxBxData).second == false) {    		
    		LOG4CPLUS_INFO(logger, "the same lmuxBxData were already added to the lastLmuxBxDataSet_, the data are skipped ");
    		//LOG4CPLUS_INFO(logger, "bx "<<lastBxData_->getBxNum()<<" tc "<<lastTcData_->getTcNum()<<" tb "<<lastTbData_->getTbNum()<<" ol "<<lastOptLinkNum<<" linkData "<<lmuxBxData.toString());
    		LOG4CPLUS_INFO(logger, "bx "<<lastBxData_->getBxNum()<<" linkData "<<lmuxBxData.toString());
    		//throw TException("RPCBxData::TbData::addOptLinkData: the same lmuxBxData were already added to the link ");
    	}        
    }
    else {
        throw TException("Unknown element '" + sname + "'");
    }
}


void XmlBxDataReader::endElement(const XMLCh* const name) {
    /*istring sname = XMLCh2String(name);
    f (sname == "ring") {
        curFecManager = 0;
    }
    else if (sname == "linkBox") {
        //curLinkBox = 0;
        curCrate = 0;
    }
    else if (sname == "crate") {
        //curVmeCrate = 0;
        curCrate = 0;
    }
    else if (sname == "board") {
        curItem = 0;
    }*/
}

void XmlBxDataReader::error(const xercesc::SAXParseException& e) {
    sawErrors_ = true;
    LOG4CPLUS_ERROR(logger,  "Error at file " << XMLCh2String(e.getSystemId())
                    << ", line " << e.getLineNumber()
                    << ", char " << e.getColumnNumber()
                    << "\n  Message: " << XMLCh2String(e.getMessage()));
}

void XmlBxDataReader::fatalError(const xercesc::SAXParseException& e) {
    sawErrors_ = true;
    LOG4CPLUS_ERROR(logger,  "Fatal Error at file " << XMLCh2String(e.getSystemId())
                    << ", line " << e.getLineNumber()
                    << ", char " << e.getColumnNumber()
                    << "\n  Message: " << XMLCh2String(e.getMessage()));
}

void XmlBxDataReader::warning(const xercesc::SAXParseException& e) {
    LOG4CPLUS_WARN(logger,  "Warning at file " << XMLCh2String(e.getSystemId())
                   << ", line " << e.getLineNumber()
                   << ", char " << e.getColumnNumber()
                   << "\n  Message: " << XMLCh2String(e.getMessage()));
}

void XmlBxDataReader::resetErrors() {
    sawErrors_ = false;
}

unsigned int XmlBxDataReader::mapBxNum(unsigned int num) {
	cout<<dec<<"bxIN "<<num<<" bxOut "<<num<<endl;
	return num;
	
}

}
