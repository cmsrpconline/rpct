#include "rpct/hardwareTests/HSStatisticDiagManager.h"
#include "rpct/ii/IHardwareItem.h"
#include "rpct/std/tbutil.h"
#include "rpct/devices/FixedHardwareSettings.h"
#include "TFrame.h"
#include <sys/time.h>
#include <string>
#include <vector>

using namespace std;
using namespace log4cplus;

namespace rpct {
//Logger HSStatisticDiagManager::logger = Logger::getInstance("StatisticDiagManager");

HSStatisticDiagManager::HSStatisticDiagManager(std::string name, std::string histosDir, std::string picturesDir, IDiagnosable::TDiagCtrlVector& diagCtrls, HistoMgrVector& histoManagers) :
    StatisticDiagManager(name, histosDir, picturesDir, diagCtrls, histoManagers)
{
    canvas1_ = new TCanvas("hsc1","",300,10,1000,200);
    //canvas2_ = new TCanvas("hsc2","",200,10,1000,250);

    canvas1_->Divide(2, 1, 0.001, 0.001); //<<<<<<<<<

    canvas1_->SetFillColor(21);
    //canvas2_->SetFillColor(21);
    canvas1_->SetGrid();
    //canvas2_->SetGrid();
    std::vector<std::string> hs0tcNames;
    std::vector<std::string> hs1tcNames;
    hs0tcNames.push_back("TC_11");
    hs0tcNames.push_back("TC_0");
    hs0tcNames.push_back("TC_1");
    hs0tcNames.push_back("TC_2");
    hs0tcNames.push_back("TC_3");
    hs0tcNames.push_back("TC_4");
    hs0tcNames.push_back("TC_5");
    hs0tcNames.push_back("TC_6");

    hs1tcNames.push_back("TC_5");
    hs1tcNames.push_back("TC_6");
    hs1tcNames.push_back("TC_7");
    hs1tcNames.push_back("TC_8");
    hs1tcNames.push_back("TC_9");
    hs1tcNames.push_back("TC_10");
    hs1tcNames.push_back("TC_11");
    hs1tcNames.push_back("TC_0");
    for(unsigned int iHist = 0; iHist < histoManagers_.size(); iHist++)  {
        unsigned int binCount = histoManagers_[iHist]->GetBinCount();

        string baordName = (dynamic_cast<IDevice&>(histoManagers_[iHist]->GetOwner())).getBoard().getDescription();
        string name = baordName + "_" + histoManagers_[iHist]->GetName();
        string title = baordName + " total hits";
        // + " " + rpct::toString(timv.tv_sec) + " " + rpct::toString(timv.tv_usec);

        TH1I* h1 = new TH1I(name.c_str(), title.c_str(), binCount , -0.5, binCount-0.5);
        h1->SetStats(kFALSE);
        h1->GetXaxis()->SetLabelSize(0.12);
        h1->GetYaxis()->SetLabelSize(0.06);
        h1->SetFillColor(kBlue);
        histosArray_.Add(h1);
        name = name + "_lastRate";
        title = title + " last Rate [Hz]";
        TH1F* h2 = new TH1F(name.c_str(), title.c_str(), binCount , -0.5, binCount-0.5);
        h2->SetStats(kFALSE);
        h2->GetXaxis()->SetLabelSize(0.12);
        h2->GetYaxis()->SetLabelSize(0.06);
        h2->SetFillColor(kGreen);
        lastRateHistos_.push_back(h2);
        if(baordName == "HSB_0") {
            for (unsigned int iCh = 1; iCh < binCount; iCh+=4) {
                h1->GetXaxis()->SetBinLabel(iCh + 1, hs0tcNames[iCh/4].c_str());
                h2->GetXaxis()->SetBinLabel(iCh + 1, hs0tcNames[iCh/4].c_str());
            }
            canvas1_->cd(1);
            canvas1_->cd(1)->SetLeftMargin(0.08);
            canvas1_->cd(1)->SetRightMargin(0.01);
            histosArray_[iHist]->Draw("bar1");

        }
        else if(baordName =="HSB_1") {
            for (unsigned int iCh = 1; iCh < binCount; iCh+=4) {
                h1->GetXaxis()->SetBinLabel(iCh + 1, hs1tcNames[iCh/4].c_str());
                h2->GetXaxis()->SetBinLabel(iCh + 1, hs1tcNames[iCh/4].c_str());
            }
            canvas1_->cd(2);
            canvas1_->cd(2)->SetLeftMargin(0.08);
            canvas1_->cd(2)->SetRightMargin(0.01);
            histosArray_[iHist]->Draw("bar1");
        }
        h1->LabelsOption("h", "X");
        h2->LabelsOption("h", "X");
    }
    picturesNames_.push_back(name_ + ".png");
    picturesDirs_.push_back(picturesDir + "/" + picturesNames_.back());
 }

HSStatisticDiagManager::~HSStatisticDiagManager() {
    delete canvas1_;
}

void HSStatisticDiagManager::clear() {
    StatisticDiagManager::clear();
}

void HSStatisticDiagManager::readoutAndAnalyse(double countingPeriod) {
    //outFile.cd();
    timeval timv;
    gettimeofday(&timv, 0);

    //double timeXVal = timv.tv_sec + timv.tv_usec/1000000.0 - startTime.tv_sec - startTime.tv_usec/1000000.0;
    //timeXAxis_.push_back((timv.tv_sec + timv.tv_usec/1000000.0 - startTime.tv_sec - startTime.tv_usec/1000000.0));


    //double yMax = 0;
    for(unsigned int iHist = 0; iHist < histoManagers_.size(); iHist++)  {
        unsigned int binCount = histoManagers_[iHist]->GetBinCount();
        histoManagers_[iHist]->Refresh();

        for (unsigned int iCh = 0; iCh < binCount; iCh++) { //in the iCh = 0 empty muons are counted
            unsigned int binVal = histoManagers_[iHist]->GetLastData()[iCh];
            lastRateHistos_[iHist]->SetBinContent(iCh+1, (binVal - ((TH1*)histosArray_[iHist])->GetBinContent(iCh+1)) / countingPeriod);
            ((TH1*)histosArray_[iHist])->SetBinContent(iCh+1, binVal);
        }
        string baordName = (dynamic_cast<IDevice&>(histoManagers_[iHist]->GetOwner())).getBoard().getDescription();
        string title = baordName + " " + rpct::toString(timv.tv_sec) + " " + rpct::toString(timv.tv_usec);
        ((TH1*)histosArray_[iHist])->SetTitle(title.c_str());
/*        if( ((TH1*)histosArray_[iHist])->GetMaximum()) {
            yMax = ((TH1*)histosArray_[iHist])->GetMaximum();
        }*/
    }

/*    for(unsigned int iHist = 0; iHist < histoManagers_.size(); iHist++)  {
        ((TH1*)histosArray_[iHist])->GetYaxis()->SetRangeUser(0, yMax);
    }*/

    //drawing histos
    canvas1_->Modified();
    canvas1_->Update();
    canvas1_->Print(picturesDirs_.back().c_str(), "png");

    //cout<<"debuuuuuuuug "<<dec<<__FILE__<<" "<<__LINE__<<" "<<__FUNCTION__<<endl;
    histosArray_.Write();
    //histosArray_.Clear();
    //cout<<"debuuuuuuuug "<<dec<<__FILE__<<" "<<__LINE__<<" "<<__FUNCTION__<<endl;

    interation_++;
}

string HSStatisticDiagManager::getHtml() {
    ostringstream ostr;
    ostr << "<br>\n";
    for(unsigned int i = 0; i < picturesNames_.size(); i++ ) {
        ostr << "<img src=\""<<"/tmp/"<<picturesNames_[i]<<"\">";
        ostr << "<br>\n";
    }
    return ostr.str();
}

}
