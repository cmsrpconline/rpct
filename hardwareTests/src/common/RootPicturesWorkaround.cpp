/*
 * RootPicturesWorkaround.cpp
 *
 *  Created on: Sep 23, 2010
 *      Author: tb
 */

#include "rpct/hardwareTests/RootPicturesWorkaround.h"

#include <fstream>

#include "TFile.h"

#include "log4cplus/loggingmacros.h"

using namespace std;

void RootPicturesWorkaround::crateRootMacro(string& name, string& picturesDir) {
	string makroDir = picturesDir + "/" + name + "_canvases.C";
    ofstream picturesMacro(makroDir.c_str() );
    picturesMacro<<"{"<<endl;
    picturesMacro<<"gStyle->SetPalette(1);"<<endl;
    picturesMacro<<"TFile *f = TFile::Open(\""<<picturesDir<<"/"<<name<<"_canvases.root"<< "\" );"<<endl;
    picturesMacro<<"TIter next(f->GetListOfKeys());"<<endl;
    picturesMacro<<"TKey *key;"<<endl;
    picturesMacro<<"while ((key = (TKey*)next())) {"<<endl;
    picturesMacro<<"  TClass *cl = gROOT->GetClass(key->GetClassName());"<<endl;
    picturesMacro<<"  if (!cl->InheritsFrom(\"TCanvas\"))"<<endl;
    picturesMacro<<"	continue;"<<endl;
    picturesMacro<<" TCanvas *c = (TCanvas*)key->ReadObj();"<<endl;
    picturesMacro<<" string pictureName = c->GetName();"<<endl;
    picturesMacro<<" pictureName = \"/tmp/\" + pictureName  + \".png\";"<<endl;
    picturesMacro<<" c->Print( pictureName.c_str(), \"png\");"<<endl;
    picturesMacro<<"}"<<endl;
    picturesMacro<<"};"<<endl;

    picturesMacro.close();

}

void RootPicturesWorkaround::createPictures(string& name, string& picturesDir, std::vector<TCanvas*>& canvases, log4cplus::Logger& logger) {
	string rootFileDir = picturesDir + "/" + name + "_canvases.root";

    TFile canvasesFile(rootFileDir.c_str(), "RECREATE");
    canvasesFile.cd();
    for(unsigned int iC = 0; iC < canvases.size(); iC++) {
        canvases[iC]->Update();
        canvases[iC]->Modified();
        //canvases_[iC]->Print(picturesDirs_[iC].c_str(), "png");
        canvases[iC]->Write();
    }

    string makroDir = picturesDir + "/" + name + "_canvases.C";
    if( system(("$ROOTSYS/bin/root -b -q " + makroDir + " >> /dev/null 2>&1").c_str()) !=0 ){
    //if( system(("$ROOTSYS/bin/root.exe -b -q " + makroDir).c_str()) !=0 ) {
        LOG4CPLUS_WARN(logger, "Cannot create picture with root macro "<<makroDir);
    }
}


/*void RootPicturesWorkaround::createPictures(string& name, string& picturesDir, std::vector<TCanvas*>& canvases, log4cplus::Logger& logger) {
    for(unsigned int iC = 0; iC < canvases.size(); iC++) {
        canvases[iC]->Update();
        canvases[iC]->Modified();
        canvases[iC]->Print((picturesDir+ "/" + canvases[iC]->GetName() + ".png").c_str(), "png");
    }
}*/

