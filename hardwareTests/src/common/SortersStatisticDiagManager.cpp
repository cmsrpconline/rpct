#include "rpct/hardwareTests/SortersStatisticDiagManager.h"
#include "rpct/ii/IHardwareItem.h"
#include "rpct/std/tbutil.h"
#include "rpct/devices/FixedHardwareSettings.h"
#include "TFrame.h"
#include "TStyle.h"
#include <sys/time.h>

using namespace std;
using namespace log4cplus;

namespace rpct {
//Logger SortersStatisticDiagManager::logger = Logger::getInstance("StatisticDiagManager");

SortersStatisticDiagManager::SortersStatisticDiagManager(std::string name, std::string histosDir, std::string picturesDir, IDiagnosable::TDiagCtrlVector& diagCtrls, HistoMgrVector& histoManagers) :
    StatisticDiagManager(name, histosDir, picturesDir, diagCtrls, histoManagers),
    //ratesYAxis_(histoManagers.size(), vector<float>()),
    fsBarrelRateLegend1_(0.5, 0.88, 0.1, 1.),
    fsEndcapRateLegend1_(0.5, 0.88, 0.1, 1.),
    fsBarrelRateLegend2_(0.9, 0.88, 0.5, 1.),
    fsEndcapRateLegend2_(0.9, 0.88, 0.5, 1.),
    barrelPaveText_(0.98, 0.1, 0.99, 0.9, "BRNDC"),
    endcapPaveText_(0.98, 0.1, 0.99, 0.9, "BRNDC"),
    integratedRateVec_(histoManagers.size(), 0.)
{
    canvas1_ = new TCanvas("fsC1","",300,10,1000,250);
    canvas2_ = new TCanvas("fsC2","",200,10,1000,250);

    canvas1_->SetLeftMargin(0.05);
    canvas1_->SetRightMargin(0.02);
    canvas1_->SetTopMargin(0.15);
    for(unsigned int iHist = 0; iHist < histoManagers_.size(); iHist++) {
        string nameH = (dynamic_cast<IHardwareItem&>(histoManagers_[iHist]->GetOwner())).getDescription() + "_" + histoManagers_[iHist]->GetName();
        unsigned int binCount = histoManagers_[iHist]->GetBinCount();
        TH1I* h1 = new TH1I(nameH.c_str(), nameH.c_str(), binCount , -0.5, binCount-0.5);
        histosArray_.Add(h1);

        TGraph* graph = new TGraph();
        rateGraphs_.push_back(graph);
        graph->SetLineColor(2 + iHist);
        graph->SetLineWidth(1);
        graph->SetMarkerColor(2 + iHist);
        graph->SetMarkerStyle(23);
        graph->SetMarkerSize(0.6);
        graph->GetXaxis()->SetTitle("time [s]");
        graph->GetYaxis()->SetTitle("rate [Hz]");

        graph->GetXaxis()->SetTimeDisplay(1);
        graph->GetXaxis()->SetTimeFormat("%H:%M:%S");

        ostringstream title;
        if(iHist < 4) {
            ostringstream ostr;
            ostr<<"barrel rate, at least "<<iHist + 1<<" muons";
            graph->SetTitle(ostr.str().c_str());
            if(iHist < 2) {
                rateGraphLegendEntries_.push_back(fsBarrelRateLegend1_.AddEntry(graph, "", "LP"));
            }
            else{
                rateGraphLegendEntries_.push_back(fsBarrelRateLegend2_.AddEntry(graph, "", "LP"));
            }
            title<<"barrel muon "<<iHist + 1;
        }
        else if(iHist < 8) {
            ostringstream ostr;
            ostr<<"endcap rate, at leas "<<iHist%4 +1<<" muons";
            graph->SetTitle(ostr.str().c_str());

            if(iHist < 6) {
                rateGraphLegendEntries_.push_back(fsEndcapRateLegend1_.AddEntry(graph, "", "LP"));
            }
            else {
                rateGraphLegendEntries_.push_back(fsEndcapRateLegend2_.AddEntry(graph, "", "LP"));
            }

            title<<"endcap muon "<<iHist%4 + 1;
        }

        string namePt = histoManagers_[iHist]->GetName() + "_ptCode";
        string titlePt = "ptCode in " + title.str();
        ptCodeHistos_.push_back(new TH1I(namePt.c_str(), titlePt.c_str(), FixedHardwareSettings::PT_CODE_MAX +1, -0.5, FixedHardwareSettings::PT_CODE_MAX + 0.5));
        ptCodeHistos_.back()->SetFillColor(kBlue);
        ptCodeHistos_.back()->SetStats(kFALSE);
        ptCodeHistos_.back()->SetXTitle("ptCode");

        string nameQ = histoManagers_[iHist]->GetName() + "_quality";
        string titleQ = "quality in " + title.str();
        qualityHistos_.push_back(new TH1I(nameQ.c_str(), titleQ.c_str(), FixedHardwareSettings::QUALITY_MAX +1, -0.5, FixedHardwareSettings::QUALITY_MAX + 0.5));
        qualityHistos_.back()->SetFillColor(kGreen);
        qualityHistos_.back()->SetStats(kFALSE);
        qualityHistos_.back()->SetXTitle("quality");

        barrelPaveText_.AddText(" ")->SetTextAlign(32);
        barrelPaveText_.AddText(" ")->SetTextAlign(32);
        barrelPaveText_.GetLine(1)->SetTextColor(kRed);
        barrelPaveText_.GetLine(1)->SetTextSize(0.2);

        endcapPaveText_.AddText(" ")->SetTextAlign(32);
        endcapPaveText_.AddText(" ")->SetTextAlign(32);
        endcapPaveText_.GetLine(1)->SetTextColor(kRed);
        endcapPaveText_.GetLine(1)->SetTextSize(0.2);
    }

    canvas2_->Divide(4, 1, 0.001, 0.001); //<<<<<<<<<

    canvas1_->SetFillColor(21);
    canvas2_->SetFillColor(21);
    canvas1_->SetGrid();
    canvas2_->SetGrid();

    picturesNames_.push_back("FsBarrelRate.png");
    fsBarrelRateGif_ = picturesDir + "/" + picturesNames_.back();

    picturesNames_.push_back("FsEndcapRate.png");
    fsEndcapRateGif_ = picturesDir + "/" + picturesNames_.back();

    picturesNames_.push_back("FsBarrelPtCode.png");
    fsBarrelPtCodeGif_ = picturesDir + "/" + picturesNames_.back();

    picturesNames_.push_back("FsEndcapPtCode.png");
    fsEndcapPtCodeGif_ = picturesDir + "/" + picturesNames_.back();

    picturesNames_.push_back("FsBarrelQuality.png");
    fsBarrelQualityGif_ = picturesDir + "/" + picturesNames_.back();

    picturesNames_.push_back("FsEndcapQuality.png");
    fsEndcapQualityGif_ = picturesDir + "/" + picturesNames_.back();

    picturesDirs_.push_back(fsBarrelRateGif_);
    picturesDirs_.push_back(fsEndcapRateGif_);

    picturesDirs_.push_back(fsBarrelPtCodeGif_);
    picturesDirs_.push_back(fsEndcapPtCodeGif_);
    picturesDirs_.push_back(fsBarrelQualityGif_);
    picturesDirs_.push_back(fsEndcapQualityGif_);
    }

SortersStatisticDiagManager::~SortersStatisticDiagManager() {
    delete canvas1_;
    delete canvas2_;
    for(unsigned int iH = 0; iH < rateGraphs_.size(); iH++) {
        delete rateGraphs_[iH];
    }
    for(unsigned int iHist = 0; iHist < ptCodeHistos_.size(); iHist++) {
        delete ptCodeHistos_[iHist];
        delete qualityHistos_[iHist];
    }
}

void SortersStatisticDiagManager::clear() {
    StatisticDiagManager::clear();
    /*timeXAxis_.clear();
    for(unsigned int i = 0; i < ratesYAxis_.size(); i++) {
        ratesYAxis_[i].clear();
    }*/

    for(unsigned int i = 0; i < rateGraphs_.size(); i++) {
        rateGraphs_[i]->Set(0);
    }
    for(unsigned int i = 0; i < integratedRateVec_.size(); i++) {
        integratedRateVec_[i] = 0.;
    }

    barrelPaveText_.GetLine(1)->SetText(0, 0, " ");
    endcapPaveText_.GetLine(1)->SetText(0, 0, " ");
}

void SortersStatisticDiagManager::readoutAndAnalyse(double countingPeriod) {
    //outFile.cd();
    timeval timv;
    gettimeofday(&timv, 0);

    double timeXVal = timv.tv_sec + timv.tv_usec/1000000.0 - startTime.tv_sec - startTime.tv_usec/1000000.0;
    //timeXAxis_.push_back((timv.tv_sec + timv.tv_usec/1000000.0 - startTime.tv_sec - startTime.tv_usec/1000000.0));

    bool counterOverflow = false;
    for(unsigned int iHist = 0; iHist < histoManagers_.size(); iHist++)  {
        unsigned int binCount = histoManagers_[iHist]->GetBinCount();

        string title = string(((TH1*)histosArray_[iHist])->GetName()) + " " + rpct::toString(timv.tv_sec) + " " + rpct::toString(timv.tv_usec);
        ((TH1*)histosArray_[iHist])->SetTitle(title.c_str());

        histoManagers_[iHist]->Refresh();

        for(unsigned  int iBin = 1; iBin <= FixedHardwareSettings::PT_CODE_MAX +1; iBin++) {
            ptCodeHistos_[iHist]->SetBinContent(iBin, 0);
        }
        for(unsigned  int iBin = 1; iBin <= FixedHardwareSettings::QUALITY_MAX +1; iBin++) {
            qualityHistos_[iHist]->SetBinContent(iBin, 0);
        }

        for (unsigned int iCh = 1; iCh < binCount; iCh++) { //in the iCh = 0 empty muons are counted
            unsigned int binVal = histoManagers_[iHist]->GetLastData()[iCh];
            if(binVal == 0xffffff) {
                LOG4CPLUS_WARN(logger, histoManagers_[iHist]->GetName()<<"counter in channel "<<iCh<<" overflow!!!");
                counterOverflow = true;
            }
            //h1->Fill(iCh, binVal);
            ((TH1*)histosArray_[iHist])->SetBinContent(iCh+1, binVal);

            ptCodeHistos_[iHist]->Fill( (iCh & 0x3e) >> 1, binVal);
            qualityHistos_[iHist]->Fill( (iCh & 0x1c0) >> 6, binVal);
        }
        if(counterOverflow) {
            if(iHist < 4) {
                barrelPaveText_.GetLine(1)->SetText(0, 0, "Counters overflow!!!");
            }
            else if(iHist < 8) {
                endcapPaveText_.GetLine(1)->SetText(0, 0, "Counters overflow!!!");
            }
        }
        double integralVal =  ((TH1*)histosArray_[iHist])->Integral();
        double lastRate = ((double)(integralVal - integratedRateVec_[iHist]))/countingPeriod;
        //ratesYAxis_[iHist].push_back(lastRate);
        integratedRateVec_[iHist] = integralVal;
        //cout<<"debuuuuuuuug "<<dec<<__FILE__<<" "<<__LINE__<<" "<<__FUNCTION__<<endl;
        rateGraphs_[iHist]->SetPoint(interation_, timeXVal, lastRate);
        //cout<<"debuuuuuuuug "<<dec<<__FILE__<<" "<<__LINE__<<" "<<__FUNCTION__<<endl;

        ostringstream ostr;
        ostr<<rateGraphs_[iHist]->GetTitle()<<", mean "<<((double)integralVal)/(countingPeriod * interation_)<<" Hz, last "<<lastRate<<" Hz"; //should be rather that way: !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //ostr<<rateGraphs_[iHist]->GetTitle()<<" mean "<<integralVal/(timeXAxis_.back())<<" last "<<lastRate; //TODO
        rateGraphLegendEntries_[iHist]->SetLabel(ostr.str().c_str());
    }

    //cout<<"debuuuuuuuug "<<dec<<__FILE__<<" "<<__LINE__<<" "<<__FUNCTION__<<endl;
    //drawing graphs
    canvas1_->cd();
    for(unsigned int iHist = 0; iHist < histoManagers_.size(); iHist++) {
        rateGraphs_[iHist]->GetXaxis()->SetTimeDisplay(1);
        rateGraphs_[iHist]->GetXaxis()->SetTimeFormat("%H:%M:%S");
        if(iHist%4 == 0) { //we assume the order og the histograms in the histoManagers_, to avoid checking names (for better efficiency)
            canvas1_->Clear();
            rateGraphs_[iHist]->Draw("ALP");
            rateGraphs_[iHist]->GetXaxis()->SetTitle("time");
            rateGraphs_[iHist]->GetYaxis()->SetTitle("rate [Hz]");
            rateGraphs_[iHist]->GetYaxis()->SetTitleOffset(0.5);
            rateGraphs_[iHist]->GetXaxis()->SetLabelSize(0.06);
            rateGraphs_[iHist]->GetHistogram()->SetTitle("");
            rateGraphs_[iHist]->SetMinimum(0.);
        }
        else {
            rateGraphs_[iHist]->Draw("LP");
        }

        if(iHist == 3) {
            fsBarrelRateLegend1_.Draw();
            fsBarrelRateLegend2_.Draw();
            if(counterOverflow) {
                barrelPaveText_.Draw();
            }
            canvas1_->Modified();
            canvas1_->Update();
            canvas1_->Print(fsBarrelRateGif_.c_str(), "png");
        }
        else if(iHist == 7) {
            fsEndcapRateLegend1_.Draw();
            fsEndcapRateLegend2_.Draw();
            if(counterOverflow) {
                endcapPaveText_.Draw();
            }
            canvas1_->Modified();
            canvas1_->Update();
            canvas1_->Print(fsEndcapRateGif_.c_str(), "png");
        }
    }
    //drawing histos
    //canvas2_->Clear();
    //cout<<"debuuuuuuuug "<<dec<<__FILE__<<" "<<__LINE__<<" "<<__FUNCTION__<<endl;
    for(unsigned int iHist = 0; iHist < ptCodeHistos_.size(); iHist++) {
        canvas2_->cd(iHist%4 +1);
        canvas2_->cd(iHist%4 +1)->Clear();
        ptCodeHistos_[iHist]->Draw();
        if(iHist == 3) {
            canvas2_->Modified();
            canvas2_->Update();
            canvas2_->Print(fsBarrelPtCodeGif_.c_str(), "png");
            //canvas2_->Clear();
        }
        if(iHist == 7) {
            canvas2_->Modified();
            canvas2_->Update();
            canvas2_->Print(fsEndcapPtCodeGif_.c_str(), "png");
            ///canvas2_->Clear();
        }
    }
    //cout<<"debuuuuuuuug "<<dec<<__FILE__<<" "<<__LINE__<<" "<<__FUNCTION__<<endl;
    for(unsigned int iHist = 0; iHist < qualityHistos_.size(); iHist++) {
        canvas2_->cd(iHist%4 +1);
        canvas2_->cd(iHist%4 +1)->Clear();
        qualityHistos_[iHist]->Draw();
        if(iHist == 3) {
            canvas2_->Modified();
            canvas2_->Update();
            canvas2_->Print(fsBarrelQualityGif_.c_str(), "png");
            //canvas2_->Clear();
        }
        if(iHist == 7) {
            canvas2_->Modified();
            canvas2_->Update();
            canvas2_->Print(fsEndcapQualityGif_.c_str(), "png");
            //canvas2_->Clear();
        }
    }

    //cout<<"debuuuuuuuug "<<dec<<__FILE__<<" "<<__LINE__<<" "<<__FUNCTION__<<endl;
    histosArray_.Write();
    //histosArray_.Clear();
    //cout<<"debuuuuuuuug "<<dec<<__FILE__<<" "<<__LINE__<<" "<<__FUNCTION__<<endl;

    interation_++;
}

string SortersStatisticDiagManager::getHtml() {
    ostringstream ostr;
    ostr << "<br>\n";
    for(unsigned int i = 0; i < picturesNames_.size(); i++ ) {
        ostr << "<img src=\""<<"/tmp/"<<picturesNames_[i]<<"\">";
        ostr << "<br>\n";
    }
    return ostr.str();
}

}
