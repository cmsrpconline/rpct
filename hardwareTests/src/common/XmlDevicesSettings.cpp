#include "rpct/hardwareTests/XmlDevicesSettings.h"
#include "rpct/ii/DeviceSettings.h"
#include "rpct/std/TException.h"
#include "rpct/devices/OptoSettingsImpl.h"
#include "rpct/devices/PacSettingsImpl.h"
#include "rpct/devices/RmbSettingsImpl.h"
#include "rpct/devices/SynCoderSettingsImpl.h"
#include "rpct/devices/DccSettingsImpl.h"
#include "rpct/devices/CcsSettings.h"

#include <log4cplus/logger.h>

#include <xercesc/parsers/SAXParser.hpp>
#include "xoap/domutils.h"
#include <sstream>

using namespace xoap;
using namespace xercesc;
using namespace std;
using namespace log4cplus;
using namespace rpct;

Logger XmlDevicesSettings::logger = Logger::getInstance("XmlDevicesSettings");
void XmlDevicesSettings::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}

XmlDevicesSettings::XmlDevicesSettings() : ConfigurationSetImpl(0)
{
}

XmlDevicesSettings::~XmlDevicesSettings()
{
}

void XmlDevicesSettings::readFile(string filename) { 
    this->filename = filename;
    resetErrors();
    SAXParser parser;
    parser.setDocumentHandler(this);
    parser.setErrorHandler(this);
    parser.parse(filename.c_str());
    if (sawErrors) {
        throw TException("Problems found during reading the file " + filename);
    }
}

string XmlDevicesSettings::getAttribute(const char* attrName, xercesc::AttributeList& attributes) {
    return XMLCh2String(attributes.getValue(attrName));
}

bool XmlDevicesSettings::getHexAttribute(const char* attrName, xercesc::AttributeList& attributes, 
        unsigned int& value) {        
    string attr = XMLCh2String(attributes.getValue(attrName));
    if (!attr.empty()) {
        istringstream istr(attr);
        istr >> hex >> value;
        return true;
    }    
    return false;
}
    
bool XmlDevicesSettings::getDecAttribute(const char* attrName, xercesc::AttributeList& attributes, 
        unsigned int& value) {        
    string attr = XMLCh2String(attributes.getValue(attrName));
    if (!attr.empty()) {
        istringstream istr(attr);
        istr >> value;
        return true;
    }    
    return false;
}

void XmlDevicesSettings::startElement(const XMLCh* const name, AttributeList& attributes) {
    string sname = XMLCh2String(name);
    unsigned int deviceId = 0;
    getDecAttribute("id", attributes, deviceId);
    //LOG4CPLUS_DEBUG(logger, "XmlDevicesSettings: startElement begin " << sname);
  
/*    if (sname == "linkBoard") {
    	unsigned int winO;
	    unsigned int winC;
	    unsigned int invClock;
	    unsigned int lMuxInDelay;
	    unsigned int rbcDelay;
	    unsigned int bcn0Delay;
	    unsigned int dataTrgDelay;
	    unsigned int dataDaqDelay;
	    unsigned int pulserTimerTrgDelay;
    
        getDecAttribute("winO", attributes, winO);
        getDecAttribute("winC", attributes, winC);
        getDecAttribute("invClock", attributes, invClock);
        getDecAttribute("lMuxInDelay", attributes, lMuxInDelay);
        getDecAttribute("rbcDelay", attributes, rbcDelay);
        getDecAttribute("bcn0Delay", attributes, bcn0Delay);
        getDecAttribute("dataTrgDelay", attributes, dataTrgDelay);
        getDecAttribute("dataDaqDelay", attributes, dataDaqDelay);        
        getDecAttribute("pulserTimerTrgDelay", attributes, pulserTimerTrgDelay);
        boost::dynamic_bitset<> channelsEna(96, 0ul);
        channelsEna.set(); 
        LinkBoardSettings linkBoardSettings(winO,
										    winC,
										    invClock,
										    lMuxInDelay,
										    rbcDelay,
										    bcn0Delay,
										    dataTrgDelay,
										    dataDaqDelay,
										    pulserTimerTrgDelay,
                                            channelsEna); // TODO  channelsEna not done          
        linkBoardSettingsMap[id] = linkBoardSettings;       
    }	
    else */ 
    if(sname == "opto") {
        unsigned int enabledLink0, enabledLink1, enabledLink2;
        getDecAttribute("link0", attributes, enabledLink0);
        getDecAttribute("link1", attributes, enabledLink1);
        getDecAttribute("link2", attributes, enabledLink2);
        
        DeviceSettingsPtr devSetPtr(new OptoSettingsImpl(enabledLink0, enabledLink1, enabledLink2));
        addDeviceSettings(deviceId, devSetPtr);
    }
    else if(sname == "dcc") {
      unsigned int preTrg = 3, postTrg = 3;
      unsigned int fedId = 790;
      unsigned int resyncCmd = 0x10, resyncMask = 0;
      unsigned int emulMode = 0, enaSpy = 1, ignoreLFF = 0;
      unsigned int inputIds[70];
      getDecAttribute("preTrig", attributes, preTrg);
      getDecAttribute("postTrig", attributes, postTrg);
      getDecAttribute("fed", attributes, fedId);
      getHexAttribute("resCmd", attributes, resyncCmd);
      getHexAttribute("resMask", attributes, resyncMask);
      getDecAttribute("emulMode", attributes, emulMode);
      getDecAttribute("enaSpy", attributes, enaSpy);
      getDecAttribute("ignoreLFF", attributes, ignoreLFF);
      for(int dccIn=0; dccIn<70; dccIn++){
	inputIds[dccIn]=99;//default value
	ostringstream ostr;
	ostr<<"in"<< setfill ('0') << setw (2)<<dccIn+1;
	getDecAttribute(ostr.str().c_str(), attributes, inputIds[dccIn]);
      }
      DeviceSettingsPtr devSetPtr(new DccSettingsImpl(preTrg, postTrg, fedId, 
						      resyncCmd, resyncMask, 
						      emulMode, enaSpy, ignoreLFF, 
						      inputIds));
      addDeviceSettings(deviceId, devSetPtr);
    }

    // LOG4CPLUS_DEBUG(logger, "XmlDevicesSettings: startElement end " << sname);
}


void XmlDevicesSettings::endElement(const XMLCh* const name) {
    string sname = XMLCh2String(name);
}

void XmlDevicesSettings::error(const xercesc::SAXParseException& e) {
    sawErrors = true;
    LOG4CPLUS_ERROR(logger,  "Error at file " << XMLCh2String(e.getSystemId())
            << ", line " << e.getLineNumber()
            << ", char " << e.getColumnNumber()
            << "\n  Message: " << XMLCh2String(e.getMessage()));
}
 
void XmlDevicesSettings::fatalError(const xercesc::SAXParseException& e) {
    sawErrors = true;
    LOG4CPLUS_ERROR(logger,  "Fatal Error at file " << XMLCh2String(e.getSystemId())
            << ", line " << e.getLineNumber()
            << ", char " << e.getColumnNumber()
            << "\n  Message: " << XMLCh2String(e.getMessage()));
}
 
void XmlDevicesSettings::warning(const xercesc::SAXParseException& e) {
    LOG4CPLUS_WARN(logger,  "Warning at file " << XMLCh2String(e.getSystemId())
            << ", line " << e.getLineNumber()
            << ", char " << e.getColumnNumber()
            << "\n  Message: " << XMLCh2String(e.getMessage()));
}

void XmlDevicesSettings::resetErrors() {
    sawErrors = false;
}


void XmlDevicesSettings::adddefaultDeviceSettings() {
    DeviceSettingsPtr opto(new OptoSettingsImpl(0, 0, 0)); 	        
    addDeviceSettings(opto);	

    DeviceSettingsPtr pac(new PacSettingsImpl()); 	        
    addDeviceSettings(pac);	
    
    DeviceSettingsPtr rmb(new RmbSettingsImpl()); 	        
    addDeviceSettings(rmb);	

    DeviceSettingsPtr lb(new SynCoderSettingsImpl()); 	        
    addDeviceSettings(lb);	    

    DeviceSettingsPtr dcc(new DccSettingsImpl()); 	        
    addDeviceSettings(dcc);

    DeviceSettingsPtr ccs(new CcsSettings()); 	        
    addDeviceSettings(ccs);
}

