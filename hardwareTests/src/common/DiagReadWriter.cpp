#include "rpct/hardwareTests/DiagReadWriter.h"
//#include "rpct/hardwareTests/PacDataFormaters.h"
//#include "rpct/hardwareTests/TRmb.h"
//#include "rpct/hardwareTests/TCGbSorter.h"
//#include "rpct/hardwareTests/TBGbSorter.h"
//#include "rpct/devices/tcsort.h"
//#include "rpct/devices/TriggerBoard.h"
//#include "rpct/devices/hsb.h"
//#include "rpct/hardwareTests/sortersReadoutBxData.h"

using namespace std;
using namespace rpct; 

DiagReadWriter::DiagReadWriter(std::ostream* _ostr, std::vector<rpct::TStandardDiagnosticReadout*> _diagReadoutsVec) 
	: ostr(_ostr), diagReadoutsVec(_diagReadoutsVec) 
{	
/*    PacReadoutBxDataFormatter::resetInstanceCnt();
    RMBReadoutBxDataFormatter::resetInstanceCnt();
    TBGbSorterReadoutBxDataFormatter::resetInstanceCnt();
    TCGbSorterReadoutBxDataFormatter::resetInstanceCnt();
	for(unsigned int iDiagRead = 0; iDiagRead < diagReadoutsVec.size(); iDiagRead++) { 
		IDiagnosable* device = &(diagReadoutsVec[iDiagRead]->GetOwner());
		if( dynamic_cast<SynCoder*>(device) ) {
            formattersVec.push_back(new LBReadoutBxDataFormatter());
        }   
        else if( dynamic_cast<Pac*>(device) ) {
            formattersVec.push_back(new PacReadoutBxDataFormatter());
        }   
        else if( dynamic_cast<Rmb*>(device) ) {
            formattersVec.push_back(new RMBReadoutBxDataFormatter());
        }   
        else if( dynamic_cast<GbSort*>(device) ) {
			formattersVec.push_back(new TBGbSorterReadoutBxDataFormatter());
		}	
		else if( dynamic_cast<TTCSortTCSort*>(device) ) {
			formattersVec.push_back(new TCGbSorterReadoutBxDataFormatter(dynamic_cast<TTCSortTCSort*>(device)->getUsedTBs()));
		}
		else if( dynamic_cast<THsbSortHalf*>(device) ) {
			formattersVec.push_back(new HSBReadoutBxDataFormatter());
		}
        else {
            throw TException("unknown diag readout"); 
        }
	}*/
}

DiagReadWriter::~DiagReadWriter() {
/*    for(unsigned int iF = 0; iF < formattersVec.size(); iF++) {
        delete formattersVec[iF];
    }*/
};
	
void DiagReadWriter::writeEvents() {
	std::vector<rpct::IDiagnosticReadout::Events::EventList::iterator> eventItersVec;    
    if(diagReadoutsVec.size() == 0) {
    	cout<<"diagReadoutsVec.size() == 0)"<<endl;
    	(*ostr)<<"diagReadoutsVec.size() == 0)"<<endl;
    	return;
    	
    }
	for(unsigned int iDiagRead = 0; iDiagRead < diagReadoutsVec.size(); iDiagRead++) {
		if(diagReadoutsVec[iDiagRead]->GetLastEvents().getEventList().size() == 0) {
        	cout << "No readout data." << endl; ///throw???????????????????????????????????????????
        	(*ostr)<<(dynamic_cast<IDevice&>(diagReadoutsVec[iDiagRead]->GetOwner())).getBoard().getDescription()<<" ";
        	(*ostr)<<(dynamic_cast<IDevice&>(diagReadoutsVec[iDiagRead]->GetOwner())).getDescription()<<" - NO EVENTS"<<endl;
        				
        	return;
    	} 			
		eventItersVec.push_back(diagReadoutsVec[iDiagRead]->GetLastEvents().getEventList().begin());
		//cout<<diagReadoutsVec[iDiagRead]->GetLastEvents().getEventList().size()<<endl;
	}
	
	while (1) {
		uint32_t bxNum = 0;
		uint32_t  evNum = 0;
		
		uint32_t bxNum0 = 0;
		uint32_t  evNum0 = 0;
		    	    	   	     
    	for(unsigned int iEventIter = 0; iEventIter < eventItersVec.size(); iEventIter++) {
    		if(eventItersVec[iEventIter] == diagReadoutsVec[iEventIter]->GetLastEvents().getEventList().end() )
    			return;
    		
    		rpct::IDiagnosticReadout::Events::EventList::iterator& iEvent = eventItersVec[iEventIter];
    		diagReadoutsVec[iEventIter]->ParseTimer(**iEvent, bxNum, evNum); 
    		if(iEventIter == 0) {			    	  	
		    	(*ostr)<<"Beginning Event ";
		    	(*ostr)<<"EvNum "<<dec<<setw(7)<<evNum<<
			         	 " BxNum "<<setw(4)<<bxNum<<" ";
	
				time_t t = time(NULL);
				(*ostr)<<" Time "<<ctime(&t);
				
				bxNum0 = bxNum;
				evNum0 = evNum;		    			
    		}
    		else if (enableEVNCheck) {
    			if(bxNum0 != bxNum) {
    				//throw TException("DiagReadWriter::writeEvents: bxNum0 != bxNum");
    			}
    			if(evNum0 != evNum) {
    				throw TException("DiagReadWriter::writeEvents: evNum0 != evNum");
    			}	    			
    		}
    		    	
    	   	//(*ostr)<<formattersVec[iEventIter]->getLabel()<<" "<<endl;
    	   	(*ostr)<<(dynamic_cast<IDevice&>(diagReadoutsVec[iEventIter]->GetOwner())).getBoard().getDescription()<<" ";
    	   	(*ostr)<<(dynamic_cast<IDevice&>(diagReadoutsVec[iEventIter]->GetOwner())).getBoard().getId()<<" ";
			(*ostr)<<(dynamic_cast<IDevice&>(diagReadoutsVec[iEventIter]->GetOwner())).getDescription()<<" ";			   
    		(*ostr)<<"EvNum "<<dec<<setw(7)<<evNum<<" BxNum "<<setw(4)<<bxNum<<endl;
			
	        IDiagnosticReadout::Event::BxDataList::const_iterator iBX = (*iEvent)->getBxDataList().begin();
	        int iBx = 0;	  
	        //cout<<(*iEvent)->getBxDataList().size()<<endl;      
	        for ( ;iBX != (*iEvent)->getBxDataList().end(); ++iBX, iBx++) {
	           //(*ostr) <<setw(2)<<dec<<iBx<<" "<<formattersVec[iEventIter]->format((*iBX)->getData())<<"\n";
	           (*ostr) <<setw(3)<<dec<<iBx<<" "<<(*iBX)->getConvertedData().toString()<<"\n";
	        }
	        eventItersVec[iEventIter]++;
    	}		
	}
}	

void DiagReadWriter::writeEventsPararel() {
	vector<rpct::IDiagnosticReadout::Events::EventList::iterator> eventItersVec;
	
	for(unsigned int iDiagRead = 0; iDiagRead < diagReadoutsVec.size(); iDiagRead++) {
		if(diagReadoutsVec[iDiagRead]->GetLastData().isEmpty() == true) {
        	cout << "No readout data." << endl; ///throw???????????????????????????????????????????
        	return;
    	} 			
		eventItersVec.push_back(diagReadoutsVec[iDiagRead]->GetLastEvents().getEventList().begin());
	}
				
	while (1) {
		uint32_t bxNum = 0;
		uint32_t  evNum = 0;
		
		uint32_t bxNum0 = 0;
		uint32_t  evNum0 = 0;
		
		vector<rpct::IDiagnosticReadout::Event::BxDataList::const_iterator> bxIterVec;   	    	   	     
    	for(unsigned int iEventIter = 0; iEventIter < eventItersVec.size(); iEventIter++) {
    		if(eventItersVec[iEventIter] == diagReadoutsVec[iEventIter]->GetLastEvents().getEventList().end() )
    			return;
    		
    		IDiagnosticReadout::Events::EventList::iterator& iEvent = eventItersVec[iEventIter];
    		diagReadoutsVec[iEventIter]->ParseTimer(**iEvent, bxNum, evNum); 
    		if(iEventIter == 0) {			    	  	
		    	(*ostr)<<"Beginning Event ";
		    	(*ostr)<<"EvNum "<<dec<<setw(7)<<evNum<<
			         	 " BxNum "<<setw(4)<<bxNum<<" ";
	
				time_t t = time(NULL);
				(*ostr)<<" Time "<<ctime(&t);
				
				bxNum0 = bxNum;
				evNum0 = evNum;		    			
    		}
    		else if (enableEVNCheck) {
    			if(bxNum0 != bxNum) {
    				//throw TException("DiagReadWriter::writeEvents: bxNum0 != bxNum");
    			}
    			if(evNum0 != evNum) {
    				throw TException("DiagReadWriter::writeEvents: evNum0 != evNum");
    			}	    			
    		}
    		
    	   	//(*ostr)<<formattersVec[iEventIter]->getLabel()<<" ";    	   	
    	   	(*ostr)<<(dynamic_cast<IDevice&>(diagReadoutsVec[iEventIter]->GetOwner())).getDescription()<<" "<<endl;
    	   		    	   	 	    	   	       
	        rpct::IDiagnosticReadout::Event::BxDataList::const_iterator iBX = (*iEvent)->getBxDataList().begin();
	        bxIterVec.push_back(iBX);	        	       
    	}
    	(*ostr)<<endl;
    	bool end = false;
   		for(int iBx = 0; 1; iBx++) {	
   			(*ostr) <<setw(2)<<dec<<iBx<<" ";
   			for(unsigned int iEventIter = 0; iEventIter < eventItersVec.size(); iEventIter++) {
   				if(bxIterVec[iEventIter] == (*(eventItersVec[iEventIter]))->getBxDataList().end()) {
   					end = true;
   					break;
   				} 
   				//(*ostr)<<formattersVec[iEventIter]->format((*(bxIterVec[iEventIter]) )->getData())<<" |-| ";   				
   				(*ostr) <<setw(2)<<dec<<iBx<<" "<<(*(bxIterVec[iEventIter]) )->getConvertedData().toString()<<" |-| ";
   				
   				bxIterVec[iEventIter]++;		
   			}	
   			(*ostr) <<"\n";   
   			if(end)
   				break;    		
   		}
   		for(unsigned int iEventIter = 0; iEventIter < eventItersVec.size(); iEventIter++) {
   			eventItersVec[iEventIter]++; 
   		}	     
	}
}
