#ifndef TBUTIL_H
#define TBUTIL_H


#include <functional>
#include <string>
#include <sstream>
#include <boost/dynamic_bitset.hpp>
#include "rpct/std/bitcpy.h"


namespace rpct {

void tbsleep(int msec);

template <class T>
class FDelete : public std::unary_function<T, void> {
public:
  void operator() (T& value)
    {
      delete value;
    }
};

template <typename T>
std::string toString(const T& obj) {
    std::ostringstream ostr;
    ostr << obj;
    return ostr.str();
}

template <typename T>
std::string toHexString(const T& obj) {
    std::ostringstream ostr;
    ostr << std::hex<<"0x" << obj;
    return ostr.str();
}

boost::dynamic_bitset<>* createBitsetFromBits(void *bits, size_t cnt);
boost::dynamic_bitset<> bitsetFromBits(void *bits, size_t cnt);
char* bitsetToBits(const boost::dynamic_bitset<> &inBitset);

std::string dataToHex(void* data, size_t bitnum);
void hexToData(std::string text, void* data, size_t size);

std::string memoryUsageInfo();

/**
 * returns the current time in a form 2013_5_29__19_20_48 - usefull for the file names
 */
std::string timeString();

}

#endif
