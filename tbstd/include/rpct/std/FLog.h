//---------------------------------------------------------------------------


#ifndef FLogH
#define FLogH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ostream>
#include <strstream>

using namespace std;

//---------------------------------------------------------------------------
class TfrLog : public TFrame
{
__published:	// IDE-managed Components
    TMemo *mmText;
        void __fastcall mmText1MouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
    void __fastcall mmTextDblClick(TObject *Sender);
private:	// User declarations
        strstream* Ostr;
        const int BUFF_LEN;
        char* Buffer;
        int ShowLast;
        int MaxLines;
        bool Active;
public:		// User declarations
        __fastcall TfrLog(TComponent* Owner);
        ostream& ostr();

        void SetActive(bool Value)
            {
                Active = Value;
            }

        bool GetActive()
            {
                return Active;
            }

        void Clear()
            {
                mmText->Clear();
            }

        void SetMaxLines(int Value)
            {
                MaxLines = Value;
            }

        int GetMaxLines()
            {
                return MaxLines;
            }

};
//---------------------------------------------------------------------------
extern PACKAGE TfrLog *frLog;
//---------------------------------------------------------------------------
#endif
