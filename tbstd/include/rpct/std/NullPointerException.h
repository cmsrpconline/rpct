#ifndef RPCTNULLPOINTEREXCEPTION_H_
#define RPCTNULLPOINTEREXCEPTION_H_

#include "TException.h"

namespace rpct
{

class NullPointerException : public TException {
public:
    NullPointerException() throw() 
    : TException("NullPointerException") {}
    
    NullPointerException(const std::string& msg) throw() 
    : TException("NullPointerException: " + msg ) {}
};


}

#endif /*RPCTNULLPOINTEREXCEPTION_H_*/
