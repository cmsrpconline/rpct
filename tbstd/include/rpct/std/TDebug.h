#ifndef TDEBUG_HH
#define TDEBUG_HH

#include <string>
#include <iostream>
#include <stdio.h>
//#include "DebugIO.h"
#include <fstream>


/*
#ifdef _DEBUG
#define TB_DEBUG
#endif
*/

//using namespace std;

class TDebug;

extern TDebug G_Debug;
//extern int G_Debug;


class TDebug {
private:
    int Level;
    int MAX_OFFSET;
    char OFFSET_CHAR;
    std::ostream* CurStream;
	std::ofstream NullStream;

    //basic_odstream<char> DOut;
public:
    static const int OFF = 10;
    static const int VME_OPER = 1;
                
    TDebug(): Level(OFF), MAX_OFFSET(10), OFFSET_CHAR('.'), CurStream(&std::cerr) {}

	std::ostream& out(int level = VME_OPER)
		{
          #ifdef TB_DEBUG
		    if (level >= Level) {
				(*CurStream) << std::string(MAX_OFFSET - level, OFFSET_CHAR);
				return *CurStream;
			}
			else 
				return NullStream;			

          #else
				return NullStream;
          #endif
		}
    
	void AddMessageLn(const std::string& message, int level)
    	{
          #ifdef TB_DEBUG
          if (level >= Level)
			  (*CurStream) << std::string(MAX_OFFSET - level, OFFSET_CHAR) << message << std::endl;
          #endif
        }

	void AddMessageContLn(const std::string& message, int level)
    	{
          #ifdef TB_DEBUG
          if (level >= Level)
          	(*CurStream) << message << std::endl;
          #endif
        }

	void AddMessage(const std::string& message, int level)
    	{
          #ifdef TB_DEBUG
          if (level >= Level)
          	(*CurStream) << std::string(MAX_OFFSET - level, OFFSET_CHAR) << message;
          #endif
        }

	void AddMessageCont(const std::string& message, int level)
    	{
          #ifdef TB_DEBUG
          if (level >= Level)
          	(*CurStream) << message;
          #endif
        }

	void AddMessageLn(char* message, int level)
    	{
          #ifdef TB_DEBUG
          if (level >= Level)
          	(*CurStream) << std::string(MAX_OFFSET - level, OFFSET_CHAR) << message << std::endl;
          #endif
        }
          
	void AddMessageContLn(char* message, int level)
    	{
          #ifdef TB_DEBUG
          if (level >= Level)
          	(*CurStream) << message << std::endl;
          #endif
        }

	void AddMessage(char* message, int level)
    	{
          #ifdef TB_DEBUG
          if (level >= Level)
          	(*CurStream) << std::string(MAX_OFFSET - level, OFFSET_CHAR) << message << std::flush;
          #endif
        }

	void AddMessageCont(char* message, int level)
    	{
          #ifdef TB_DEBUG
          if (level >= Level)
          	(*CurStream) << message;
          #endif
        }

    void AddHexCont(unsigned long number, int level)
        {
          #ifdef TB_DEBUG
              if (level >= Level)
                (*CurStream) << std::hex << number << std::flush;
          #endif
        }     

    void AddDecCont(unsigned long number, int level)
        {
          #ifdef TB_DEBUG
              if (level >= Level)
                (*CurStream) << std::dec << number << std::flush;
          #endif
        }
        
    void SetLevel(int level) { Level = level; }
    int  GetLevel()			 { return Level;  }

	void SetStream(std::ostream& ostr)
		{
			CurStream = &ostr;
		}

    static void Init() {}
};


  
class TLog {
private:
    std::ostream* CurStream;
	std::ofstream NullStream;
public:
    TLog()
      {
        CurStream = &NullStream;
      }

	std::ostream& out()
		{
          return *CurStream;
		}

	void out(std::ostream& ostr)
		{
			CurStream = &ostr;
		}
};



extern TLog G_Log;

#endif
