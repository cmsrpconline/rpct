//---------------------------------------------------------------------------

#ifndef TXMLObjectMgrH
#define TXMLObjectMgrH
//---------------------------------------------------------------------------

#include <xercesc/sax/HandlerBase.hpp>   
#include <xercesc/sax/AttributeList.hpp>
#include <string>

class TXMLObjectMgr {
protected:
    static int InstanceCount;

    std::string getAttribute(const char* attrName, xercesc::AttributeList& attributes);
    bool getHexAttribute(const char* attrName, xercesc::AttributeList& attributes, unsigned int& value);
    unsigned int getHexAttribute(const char* attrName, xercesc::AttributeList& attributes);
    bool getDecAttribute(const char* attrName, xercesc::AttributeList& attributes, unsigned int& value);
    bool getSignedDecAttribute(const char* attrName, xercesc::AttributeList& attributes, int& value);
    int getDecAttribute(const char* attrName, xercesc::AttributeList& attributes);
    
public:
    TXMLObjectMgr();
    virtual ~TXMLObjectMgr();
};



#endif
