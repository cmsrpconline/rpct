#ifndef DEBUGIO_H
#define DEBUGIO_H

#pragma once

#ifdef _DEBUG
#pragma comment(lib, "DebugIoD.lib")
#else
#pragma comment(lib, "DebugIo.lib")
#endif

#include <streambuf>
#include <iomanip>

#include <ostream>
#include <cstdio>
#include <cassert>
#include <string>

#ifndef ASSERT
#ifdef _DEBUG
#define ASSERT(b) do {assert(b);} while(false)
#else
#define ASSERT(b) (void(0))
#endif
#endif

int dprintf(const char* cszFormat, ...);
int wdprintf(const wchar_t* cwszFormat, ...);

#ifdef _UNICODE
#define tdprintf wdprintf
#else
#define tdprintf dprintf
#endif


#ifdef _DEBUG
#define DPRINTF dprintf
#define WDPRINTF wdprintf
#define TDPRINTF tdprintf
#else
#define DPRINTF 1 ? (void)0 : dprintf
#define WDPRINTF 1 ? (void)0 : wdprintf
#define TDPRINTF 1 ? (void)0 : tdprintf
#endif;


template<class _E, class _Tr = char_traits<_E> >
class basic_debugbuf : public std::basic_streambuf<_E, _Tr>
{
public:
        virtual ~basic_debugbuf()
        {}
protected:
        virtual int_type overflow(int_type c = _Tr::eof());

private:
        enum EPlatform
        {
                s_cePlatformWin32s,
                s_cePlatformWinXx,
                s_cePlatformWinNt
        };

        EPlatform GetPlatform();
};

template<class _E, class _Tr = char_traits<_E> >
inline basic_debugbuf<_E, _Tr>::int_type
basic_debugbuf<_E, _Tr>::overflow(int_type c)
{
        static const EPlatform _s_cePlatform = GetPlatform();

// conversion from 'int' to 'const char', possible loss of data
#pragma warning(disable:4244)
        const _E cArg[2] = {c, 0};
#pragma warning(default:4244)
        if (1 == sizeof(_E))
        {
                ::OutputDebugStringA(reinterpret_cast<const char*>(cArg));
        }
        else if (2 == sizeof(_E))
        {
                if (s_cePlatformWinNt == _s_cePlatform)
                {
                        ::OutputDebugStringW(reinterpret_cast<const wchar_t*>(cArg));
                }
                else
                {
                        char szArg[6] = {0};

                        ::WideCharToMultiByte(
                                CP_ACP,
                                0,
                                reinterpret_cast<const wchar_t*>(cArg),
                                -1,
                                szArg,
                                sizeof szArg,
                                0,
                                0);

                        ::OutputDebugStringA(szArg);
                }
        }

        return c;
}


template<class _E, class _Tr = char_traits<_E> >
inline basic_debugbuf<_E, _Tr>::EPlatform basic_debugbuf<_E, _Tr>::GetPlatform()
{
        EPlatform ePlatform = s_cePlatformWin32s;

        OSVERSIONINFO osVersionInfo;
        osVersionInfo.dwOSVersionInfoSize = sizeof OSVERSIONINFO;

        ::GetVersionEx(&osVersionInfo);

        switch (osVersionInfo.dwPlatformId)
        {
                case VER_PLATFORM_WIN32s:
                        ePlatform = s_cePlatformWin32s;
                        break;

                case VER_PLATFORM_WIN32_WINDOWS:
                        ePlatform = s_cePlatformWinXx;
                        break;

                case VER_PLATFORM_WIN32_NT:
                        ePlatform = s_cePlatformWinNt;
        }

        return ePlatform;
}


template<class _E, class _Tr = std::char_traits<_E> >
        class basic_odstream : public std::basic_ostream<_E, _Tr>
{
public:
        typedef basic_odstream<_E, _Tr> _Myt;
        typedef basic_debugbuf<_E, _Tr> _Mydb;
        basic_odstream()
                : std::basic_ostream<_E, _Tr>(&_Fb)
                {}
        virtual ~basic_odstream()
                {}
        _Mydb *rdbuf() const
                {return ((_Mydb *)&_Fb); }
private:
        _Mydb _Fb;
        };


class _CNullOut
{
private:
        typedef _CNullOut _Myt;
        typedef std::basic_ostream<char, std::char_traits<char> > _Mybos;
        typedef std::basic_ios<char, std::char_traits<char> > _Myios;

public:
        _Myt& operator<<(_Myt& (__cdecl *)(_Myt&))
                {  return *this; }

        _Myt& operator<<(_Mybos& (__cdecl *)(_Mybos&))
                {  return *this; }

        _Myt& operator<<(_Myios& (__cdecl *)(_Myios&))
                {  return *this; }

        _Myt& operator<<(std::ios_base& (__cdecl *)(std::ios_base&))
                {  return *this; }
};

template<typename T> inline
_CNullOut& __cdecl operator<< ( _CNullOut& _O, T& )
        {  return (_O); }

template<class _E> inline
_CNullOut& __cdecl operator<<(_CNullOut& _O, const std::_Fillobj<_E>&)
        {  return (_O); }

template<class _Tm> inline
_CNullOut& __cdecl operator<<(_CNullOut& _O, const std::_Smanip<_Tm>&)
        {  return (_O); }


extern basic_odstream<char> dout;
extern basic_odstream<wchar_t> wdout;

#ifdef _UNICODE
#define tdout wdout
#else
#define tdout dout
#endif

#ifdef _DEBUG
#define DOUT dout
#define WDOUT wdout
#define TDOUT tdout
#else
#define DOUT _CNullOut()
#define WDOUT _CNullOut()
#define TDOUT _CNullOut()
#endif


#endif