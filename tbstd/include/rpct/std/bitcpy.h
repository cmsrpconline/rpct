//---------------------------------------------------------------------------

#ifndef bitcpyH
#define bitcpyH
//---------------------------------------------------------------------------

#ifdef linux
#include <cstddef>
#endif

//void bitcpy(void *dest, int dst_ofs,
//            const void *source, int src_ofs, size_t cnt);
void bitcpy(void *dst, ptrdiff_t dst_ofs,
       const void *src, ptrdiff_t src_ofs, size_t cnt);

#endif
