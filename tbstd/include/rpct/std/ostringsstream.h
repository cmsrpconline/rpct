//---------------------------------------------------------------------------

#ifndef ostringsstreamH
#define ostringsstreamH

#include <iostream.h>
#include <Classes.hpp>
//---------------------------------------------------------------------------

class stringsbuf: public std::streambuf
{
private:
  TStrings*    text;

  void      put_buffer(void);
  void      put_char(int);

protected:
  int       overflow(int);
  int       sync();

public:
  stringsbuf(TStrings*, int = 0);
  ~stringsbuf();
};

class ostringsstream: public std::ostream
{
public:
  ostringsstream(TStrings* str, int bsize=0)
     : std::ostream(new stringsbuf(str, bsize)) {}
};


#endif
