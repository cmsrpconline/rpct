#ifndef ENOTIMPLEMENTED_H_
#define ENOTIMPLEMENTED_H_

#include "rpct/std/TException.h"

class ENotImplemented : public TException {
public:
    ENotImplemented(const std::string& msg) throw() 
	: TException( "Not implemented:" + msg ) {}
};

#endif /*ENOTIMPLEMENTED_H_*/
