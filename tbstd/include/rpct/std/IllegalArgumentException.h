#ifndef ILLEGALARGUMENTEXCEPTION_H_
#define ILLEGALARGUMENTEXCEPTION_H_

#include "TException.h"

namespace rpct
{

class IllegalArgumentException : public TException {
public:
    IllegalArgumentException(const std::string& msg) throw() 
    : TException( "Illegal argument: " + msg ) {}
};


}

#endif /*ILLEGALARGUMENTEXCEPTION_H_*/
