#ifndef _RPCT_BIG_INTEGER_H_
#define _RPCT_BIG_INTEGER_H_

#include "rpct/std/TException.h"
#include <stdint.h>

namespace rpct {
    
    
class BigInteger {
public: 
    BigInteger(unsigned char* value, std::size_t size);
    BigInteger(uint32_t value);
    BigInteger();
    BigInteger(const BigInteger& b);
                        
    virtual ~BigInteger();    
        
    void clear();
    operator uint32_t();
    operator uint64_t();
            
    BigInteger& operator=(const BigInteger& value);
    BigInteger& operator=(const uint32_t& value);
    
    int operator==(const BigInteger & b);
            
    int operator!=(const BigInteger & b);
        
    virtual std::string toString ();

    virtual void  fromString (std::string value);
              
    virtual size_t size() {
        return size_;
    }  
    
    virtual unsigned char* buffer() {
        return value_;
    }
    void copy(void* dest);
private:
    unsigned char* value_;
    size_t size_;
    std::string string_;
};
    
}

#endif /*_RPCT_BIG_INTEGER_H_*/
