#ifndef TEXCEPTION_HH
#define TEXCEPTION_HH
          
//
//  Michal Pietrusinski 2001
//  Warsaw University
//

#include <string>


#if (__BCPLUSPLUS__ >= 0x0550) 
#include <SysUtils.hpp>

#define EXCFASTCALL __fastcall

class TException: public Exception {
    std::string Message;
public:                                               
    TException();
    TException(const std::string& msg);

    __fastcall virtual ~TException() {}
    virtual void what(const char* msg)
    {
      Message = msg;
    }
    virtual const char* what() const
	{
	    return Message.c_str();
	}
};
#else
#include <exception>
#define EXCFASTCALL
class TException : public std::exception {
private:
    std::string message;
public:
    TException() throw()
    : message("") {
    }
    
    TException(const std::string& msg) throw()
    : message(msg) {
    }
    
    virtual ~TException() throw() {}   
    
    virtual void what(const char* msg) {
        message = msg;
    }
    
    virtual const char* what() const throw() {
	    return message.c_str();
	}
};
#endif



/*

class EOutOfMemory : public TException {
public:
    EOutOfMemory( string msg = "" ) : TException( "Error: out of memory" + msg ) {}
    virtual ~EOutOfMemory() {}
};
                 */
#endif
