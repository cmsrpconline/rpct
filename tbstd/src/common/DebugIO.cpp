#include <WINDOWS.H>
#include "DebugIo.h"

#include <io.h>
#include <fcntl.h>
#include <share.h>

#include <stdio.h>
#include <stdarg.h>
#include <assert.h>

int dprintf(const char* cszFormat, ...)
{
            va_list args;
            va_start(args, cszFormat);

        FILE *pNulDevice = fopen("\\\\.\\NUL",  "w");

        int nCharCount = vfprintf(pNulDevice, cszFormat, args);

        if (0 > nCharCount)
                return 0;

        std::string strOutput(nCharCount, '  ');

        int nTrueCharCount = vsprintf(&strOutput[0],  cszFormat,  args);
        
        if (nTrueCharCount > nCharCount)
        {
                fclose(pNulDevice);
                va_end(args);
                assert(false);
                /* THROW EXCEPTION */
                return 0;
        }

            OutputDebugStringA(strOutput.c_str());

        fclose(pNulDevice);
            va_end(args);

        return nTrueCharCount;
}

int wdprintf(const wchar_t* cwszFormat, ...)
{
            va_list args;
            va_start(args, cwszFormat);

        FILE *pNulDevice = fopen("\\\\.\\NUL", "w");

        int nCharCount = vfwprintf(pNulDevice, cwszFormat, args);

        if (0 > nCharCount)
                return 0;

        std::wstring wstrOutput(nCharCount, L' ');

        int nTrueCharCount = vswprintf(&wstrOutput[0], cwszFormat, args);
        
        if (nTrueCharCount > nCharCount)
        {
                fclose(pNulDevice);
                va_end(args);
                assert(false);
                /* THROW EXCEPTION */
                return 0;
        }

                ::OutputDebugStringW(wstrOutput.c_str());

        fclose(pNulDevice);
                va_end(args);

        return nTrueCharCount;
}


basic_odstream<char> dout;
basic_odstream<wchar_t> wdout;









