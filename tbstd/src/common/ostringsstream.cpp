//--------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "ostringsstream.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)




stringsbuf::stringsbuf(TStrings* str, int bsize)
  : streambuf(), text(str)
{
  if (bsize)
  {
    char *ptr = new char[bsize];
    setp(ptr, ptr + bsize);
  }
  else
    setp(0, 0);

  setg(0, 0, 0);
}


stringsbuf::~stringsbuf()
{
  sync();
  delete[] pbase();
}

int stringsbuf::overflow(int c)
{
  put_buffer();

  if (c != EOF)
    if (pbase() == epptr())
      put_char(c);
    else
      sputc(c);

  return 0;
}

int stringsbuf::sync()
{
  put_buffer();
  return 0;
}




void stringsbuf::put_char(int chr)
{
  char  app[2];

  app[0] = chr;
  app[1] = 0;

  int idx;

  if (!text->Count) {
    text->Add("");
    idx = 0;
  }
  else
    idx = text->Count-1;

  text->Strings[idx] = text->Strings[idx] + app;
  //XmTextInsert(text, XmTextGetLastPosition(text), app);
}

void stringsbuf::put_buffer()
{
  if (pbase() != pptr())
  {
    int len = (pptr() - pbase());
    char* buffer = new char[len + 1];

    strncpy(buffer, pbase(), len);
    buffer[len] = 0;

    int idx;

    if (!text->Count) {
      text->Add("");
      idx = 0;
    }
    else
      idx = text->Count-1;
      
    text->BeginUpdate();
    text->Strings[idx] = text->Strings[idx] + buffer;
    text->EndUpdate();
    //XmTextInsert(text, XmTextGetLastPosition(text), buffer);

    setp(pbase(), epptr());
    delete [] buffer;
  }
}