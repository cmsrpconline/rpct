#include "rpct/std/TXMLObjectMgr.h"
#include <xercesc/util/PlatformUtils.hpp>
#include "tb_std.h"

#include "xoap/domutils.h" 
#include "xdaq/exception/Exception.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

using namespace xercesc;
using namespace xoap;
using namespace std;


int TXMLObjectMgr::InstanceCount = 0;

TXMLObjectMgr::TXMLObjectMgr()
{
  if (InstanceCount == 0) { 
    try {
        XMLPlatformUtils::Initialize();
        //XPathEvaluator::initialize();
        InstanceCount++;
    }
    catch(const XMLException &toCatch)  {
      #ifdef __BORLANDC__
        throw Exception(WideString("Error during Xerces-c Initialization: ")
           + toCatch.getMessage());
      #else
        throw TException("Error during Xerces-c Initialization: "
           + XMLCh2String(toCatch.getMessage()));
      #endif
    }
    //catch(XalanDOMException& e) {
    //  throw Exception(WideString("XalanDOMException: ") + IntToStr(e.getExceptionCode()));
    //}
  }
}


TXMLObjectMgr::~TXMLObjectMgr()
{
  InstanceCount--;
  if (InstanceCount == 0) {
    //XPathEvaluator::terminate();
    //XMLPlatformUtils::Terminate(); //-- sometimes raises access violations
  }   
}


string TXMLObjectMgr::getAttribute(const char* attrName, xercesc::AttributeList& attributes) {
    return XMLCh2String(attributes.getValue(attrName));
}

bool TXMLObjectMgr::getHexAttribute(const char* attrName, xercesc::AttributeList& attributes, 
        unsigned int& value) {        
    string attr = XMLCh2String(attributes.getValue(attrName));
    if (!attr.empty()) {
        istringstream istr(attr);
        istr >> hex >> value;
        return true;
    }    
    return false;
}
    

unsigned int TXMLObjectMgr::getHexAttribute(const char* attrName, xercesc::AttributeList& attributes) {
    unsigned int result;
    if (!getHexAttribute(attrName, attributes, result)) {
        ostringstream ostr;
        ostr << "Attribute '" << attrName << "' not found";
        throw TException(ostr.str().c_str());
    }
    return result;
}
    
    
    
bool TXMLObjectMgr::getDecAttribute(const char* attrName, xercesc::AttributeList& attributes, 
		unsigned int& value) {
    string attr = XMLCh2String(attributes.getValue(attrName));
    if (!attr.empty()) {
        istringstream istr(attr);
        istr >> value;
        return true;
    }    
    return false;
}

bool TXMLObjectMgr::getSignedDecAttribute(const char* attrName, xercesc::AttributeList& attributes,
        int& value) {
    string attr = XMLCh2String(attributes.getValue(attrName));
    if (!attr.empty()) {
        istringstream istr(attr);
        istr >> value;
        return true;
    }
    return false;
}

int TXMLObjectMgr::getDecAttribute(const char* attrName, xercesc::AttributeList& attributes) {
    int result;
    if (!getSignedDecAttribute(attrName, attributes, result)) {//TODO not sure if this works fine, previously was getDecAttribute KB
        ostringstream ostr;
        ostr << "Attribute '" << attrName << "' not found";
        throw TException(ostr.str().c_str());
    }
    return result;
}
