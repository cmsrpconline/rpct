#include "rpct/std/TException.h"



#if (__BCPLUSPLUS__ >= 0x0550) 


TException::TException()
  : Message(""), Exception("")
{
}

TException::TException(const std::string& msg)
  : Message(msg), Exception(msg.c_str())
{
}

#else 



#endif
