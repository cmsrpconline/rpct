// $Id: BigInteger.cpp,v 1.2 2007/03/14 20:21:06 tb Exp $

#include "rpct/std/BigInteger.h"
#include <algorithm>
#include <sstream>
#include <cstring> 

using namespace std;

namespace rpct {

BigInteger::BigInteger(unsigned char* value, size_t size) {
    size_ = size;
    value_ = new unsigned char[size_];
    memcpy(value_, value, size_);
}


BigInteger::BigInteger(uint32_t value) {
    size_ = sizeof(uint32_t);
    value_ = new unsigned char[size_];
    memcpy(value_, &value, size_);
}

BigInteger::BigInteger() {
    size_ = 0;
    value_ = 0;
}

BigInteger::BigInteger(const BigInteger& b) {
    size_ = b.size_;
    value_ = new unsigned char[size_];
    memcpy(value_, b.value_, size_);    
}
    
                    
BigInteger::~BigInteger() {
    delete [] value_;
}


void BigInteger::clear() {
    delete [] value_;
    value_ = 0;
    size_ = 0;
    string_.clear();
}


BigInteger::operator uint32_t()
{ 
    uint32_t result = 0;
    int max = std::min(sizeof(uint32_t), size_);
    for (int i = 0; i < max; i++) {
        uint32_t val = *(value_ + i);
        result |= (val << (i * 8));
    }
        
	return result; 
}

BigInteger::operator uint64_t()
{ 
    uint64_t result = 0;
    int max = std::min(sizeof(uint64_t), size_);
    for (int i = 0; i < max; i++) {
        uint64_t val = *(value_ + i);
        result |= (val << (i * 8));
    }
        
	return result; 
}



BigInteger& BigInteger::operator=(const BigInteger& value)
{
	//std::cout << "BigInteger::operator=(const BigInteger& value)" << std::endl;
    clear();
    size_ = value.size_;
    value_ = new unsigned char[size_];
    memcpy(value_, value.value_, size_);    
	return *this;
}

BigInteger& BigInteger::operator=(const uint32_t& value)
{
    //std::cout << "BigInteger::operator=(const uint32_t& value)" << std::endl;
    clear();
    size_ = sizeof(uint32_t);
    value_ = new unsigned char[size_];
    memcpy(value_, &value, size_);    
    return *this;
}

// ------------------------------------------------------------------

int BigInteger::operator==(const BigInteger& b)
{
    return (size_ == b.size_) && !memcmp(value_, b.value_, size_);
}

int BigInteger::operator!=(const BigInteger& b)
{
	return (value_ != b.value_);
}

// ------------------------------------------------------------------

void BigInteger::copy(void* dest) {
    memcpy(dest, value_, size_);
}

string BigInteger::toString()
{
    if (string_.length() == 0) {
        ostringstream str;
        str.fill('0');
        str << hex << uppercase;
        int b = 0;
        for (int i = (size_ - 1); i >= 0; i--) {
            b = *(value_ + i);
            str.width(2);
            str << b;
        }
        string_ = str.str();
    }
    return string_;
}

void BigInteger::fromString(std::string value) 
{    
    clear();

    string::size_type firstNotZero = value.find_first_not_of('0');
    if (firstNotZero == string::npos) {
        return;
    }
    
    value = value.substr(firstNotZero);
    
    istringstream str;
    str >> hex >> uppercase;
    if (value.size()%2) {
        value.insert(value.begin(), '0');
    }

    //while (text.size()/2 < size)  {
    //    text.insert(text.begin(), 2, '0');
    //}
    
    size_ = value.size() / 2;
    value_ = new unsigned char[size_];

    unsigned short us;
    for (int i=size_-1; i>=0; i--) {
        // move back the input sequence to the beginning
        str.seekg(0);
        // clear the state flags
        str.clear();

        str.str(value.substr((size_-i-1)*2, 2));
        str >> us;
        ((unsigned char*)value_)[i] = us;
    }
}
    
    
}
