#include "rpct/std/tbutil.h"
#include <iomanip>
#ifdef __BORLANDC__
# include <vcl.h>
# pragma hdrstop
#else
# include <unistd.h>
#endif
#include <sys/resource.h>
#include <sys/types.h>
#include <unistd.h>
#include <fstream>


using namespace boost;
using namespace std;

namespace rpct {

void tbsleep(int msec)
{
  #ifdef __BORLANDC__
    Sleep(msec);
  #else
    usleep(msec * 1000);
  #endif
}




dynamic_bitset<>* createBitsetFromBits(void *bits, size_t cnt) {
    dynamic_bitset<>* resBitset = new dynamic_bitset<>();
    unsigned int i = 0;
    for(; i < cnt/dynamic_bitset<>::bits_per_block; i++) {
        dynamic_bitset<>::block_type block = 0;
        bitcpy(&block, 0, bits, i * dynamic_bitset<>::bits_per_block, dynamic_bitset<>::bits_per_block);
        resBitset->append(block);
    }

    unsigned int remBits = cnt % dynamic_bitset<>::bits_per_block;
    if (remBits != 0) {
        dynamic_bitset<>::block_type block = 0;
        bitcpy(&block, 0, bits, i * dynamic_bitset<>::bits_per_block, remBits);
        resBitset->append(block);
        resBitset->resize(cnt);
    }

    return resBitset;
}

dynamic_bitset<> bitsetFromBits(void *bits, size_t cnt) {
    dynamic_bitset<> resBitset;
    unsigned int i = 0;
    for(; i < cnt/dynamic_bitset<>::bits_per_block; i++) {
        dynamic_bitset<>::block_type block = 0;
        bitcpy(&block, 0, bits, i * dynamic_bitset<>::bits_per_block, dynamic_bitset<>::bits_per_block);
        resBitset.append(block);
    }

    unsigned int remBits = cnt%dynamic_bitset<>::bits_per_block;
    if(remBits != 0) {
        dynamic_bitset<>::block_type block = 0;
        bitcpy(&block, 0, bits, i * dynamic_bitset<>::bits_per_block, remBits);
        resBitset.append(block);
        resBitset.resize(cnt);
    }

    return resBitset;
}

char* bitsetToBits(const dynamic_bitset<> &inBitset) {
    unsigned int size = inBitset.size() / 8;
    if(inBitset.size() % 8 > 0)
        size++;
    char* bits = new char[size];
    //unsigned int ulong_width = sizeof(unsigned long);
    unsigned int ulong_width = std::numeric_limits<unsigned long>::digits; //copied from dynamic_bitset.hpp
    unsigned int i = 0;
    for(; i < inBitset.size() / ulong_width; i++) {
        dynamic_bitset<> buf = (inBitset >> (ulong_width * i));
        buf.resize(ulong_width);
        unsigned long bitsULong = buf.to_ulong();
        bitcpy(bits, ulong_width * i, &bitsULong, 0, ulong_width);
    }

    int remBits = inBitset.size() % ulong_width;
    if(remBits) {
        dynamic_bitset<> buf = inBitset >> (ulong_width * i);
        buf.resize(remBits);
        unsigned long bitsULong = buf.to_ulong();
        bitcpy(bits, ulong_width * i, &bitsULong, 0, remBits);
    }

    return bits;
}

std::string dataToHex(void* data, size_t bitnum) {
    using namespace std;
    ostringstream str;
    str << hex << uppercase << setfill('0');

    int size = (bitnum-1)/8+1;

    for(int i=size-1; i>=0; i--)
        if (i == size-1)
            str << setw(2-((bitnum/4)%2)) << (unsigned int)(((unsigned char*)data)[i]);
        else
            str << setw(2) << (unsigned int)(((unsigned char*)data)[i]);
    return str.str();
}


void hexToData(std::string text, void* data, size_t size) {
    using namespace std;
    istringstream str;
    str >> hex >> uppercase;

    if (text.size()%2)
        text.insert(text.begin(), '0');

    while (text.size()/2 < size)
        text.insert(text.begin(), 2, '0');


    unsigned short us;
    for(int i=size-1; i>=0; i--) {
        // move back the input sequence to the beginning
        str.seekg(0);
        // clear the state flags
        str.clear();

        str.str(text.substr((size-i-1)*2, 2));
        str >> us;
        ((unsigned char*)data)[i] = us;
    }
}

std::string memoryUsageInfo() {
	string outStr;
/*
	//not working
	int who = RUSAGE_SELF;
	struct rusage usage;

	if(getrusage(who, &usage) != 0)
	{
		strm<<"getrusage failed"<<endl ;
		return strm.str();
	}

	strm<<"Shared Memory = "<<usage.ru_ixrss<<endl ;
	strm<<"Unshared Data Size = "<<usage.ru_idrss<<endl ;
	strm<<"Unshared Stack Size = "<<usage.ru_isrss<<endl ;
*/

/*	strm<<"ps -p "<<getpid()<<" v";

	system(strm.str().c_str());*/
	ostringstream fileName;
	fileName<<"/proc/"<<getpid()<<"/status";
	ifstream  procStatusFile;
	procStatusFile.open(fileName.str().c_str());
	string line;
	while(getline(procStatusFile,line)) {
		if(line.find("VmSize:") !=string::npos || line.find("VmRSS:") !=string::npos) {
			outStr = outStr + line + " ";
		}
	}
	procStatusFile.close();
	return outStr;
}

std::string timeString() {
	time_t timer = time(NULL);
	tm* tblock = localtime(&timer);
	tblock->tm_mon += 1;

	std::stringstream srfile;
	srfile<< tblock->tm_year + 1900<< '_'
		  << tblock->tm_mon << '_' << tblock->tm_mday << "__" << tblock->tm_hour << '_'
		  << tblock->tm_min << '_' << tblock->tm_sec;

	return srfile.str();

}
}
