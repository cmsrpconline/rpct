#include "rpct/std/bitcpy.h"
#include <assert.h>
#include <limits.h>
#include <string.h>


//---------------------------------------------------------------------------

#pragma package(smart_init)

/* based on code taken from newsgroup
From: Mathew Hendry <mathew_hendry@my-deja.com>
Newsgroups: comp.lang.c,comp.lang.c++
Subject: Re: Bit copy function
Date: Sat, 08 Sep 2001 18:02:18 +0100
Organization: home
Lines: 75
Message-ID: <kdjkpts11soeqe9a7b947jm7e4rgm852ce@4ax.com>
*/

// utility macros

#define MIN(a, b)      ((a) < (b) ? (a) : (b))
/*
#define BSET(ofs, cnt) (((UCHAR_MAX >> (cnt)) ^ UCHAR_MAX) >> (ofs))
#define BSET0(cnt)     ((UCHAR_MAX >> (cnt)) ^ UCHAR_MAX)
#define BCLR(ofs, cnt) (BSET(ofs, cnt) ^ UCHAR_MAX)
#define BCLR0(cnt)     (UCHAR_MAX >> (cnt)) */ 
#define BSET(ofs, cnt) (((UCHAR_MAX << (cnt)) ^ UCHAR_MAX) << (ofs))
#define BSET0(cnt)     ((UCHAR_MAX << (cnt)) ^ UCHAR_MAX)
#define BCLR(ofs, cnt) (BSET(ofs, cnt) ^ UCHAR_MAX)
#define BCLR0(cnt)     (UCHAR_MAX << (cnt))

// core routine, assumes non-zero cnt and normalised bit-pointers
static void bitcpy_main(unsigned char *dst, int dst_ofs,
            const unsigned char *src, int src_ofs, size_t cnt)
{
  int ndst, nsrc;
  size_t bytes;

  assert(cnt != 0);
  assert(dst_ofs >= 0 && dst_ofs < CHAR_BIT);
  assert(src_ofs >= 0 && src_ofs < CHAR_BIT);

  /* byte-align destination */
  if (dst_ofs) {
    ndst = MIN(CHAR_BIT - dst_ofs, cnt);
    nsrc = MIN(CHAR_BIT - src_ofs, ndst);

    *dst &= BCLR(dst_ofs, ndst);
    //*dst |= ((src[0] << src_ofs) & BSET0(nsrc)) >> dst_ofs;
    *dst |= ((src[0] >> src_ofs) & BSET0(nsrc)) << dst_ofs;
    if (nsrc < ndst)
      //*dst |= (src[1] & BSET0(ndst - nsrc)) >> (dst_ofs + nsrc);
      *dst |= (src[1] & BSET0(ndst - nsrc)) << (dst_ofs + nsrc);

    cnt -= ndst;
    if (!cnt)
      return;

    ++dst;
    src_ofs += ndst;
    if (src_ofs >= CHAR_BIT) {
      ++src;
      src_ofs -= CHAR_BIT;
    }
  }

  /* bytewise copy */
  bytes = cnt / CHAR_BIT;
  if (bytes) {
    if (!src_ofs) {
      memcpy(dst, src, bytes);

      dst += bytes, src += bytes; 
    }
    else {
      nsrc = CHAR_BIT - src_ofs;
      do {
        //*dst = ((src[0] << src_ofs) & BSET0(nsrc)) | (src[1] >> nsrc);
        *dst = ((src[0] >> src_ofs) & BSET0(nsrc)) | (src[1] << nsrc);
      } while (++src, ++dst, --bytes);
    }

    cnt %= CHAR_BIT;
    if (!cnt)
      return;
  }

  /* deal with any trailing bits */
  ndst = cnt;
  nsrc = MIN(CHAR_BIT - src_ofs, ndst);
  *dst &= BCLR0(ndst);
  //*dst |= (src[0] << src_ofs) & BSET0(nsrc);
  *dst |= (src[0] >> src_ofs) & BSET0(nsrc);
  if (nsrc < ndst)
    //*dst |= (src[1] & BSET0(ndst - nsrc)) >> nsrc;
    *dst |= (src[1] & BSET0(ndst - nsrc)) << nsrc;
}


/* normalise bit-pointer such that 0 <= ofs < CHAR_BIT */
#define NORMALISE(ptr, ofs) \
  do { \
    if ((ofs) < 0) { \
      (ofs) = ~(size_t)(ofs); \
      (ptr) -= (ofs) / CHAR_BIT + 1; \
      (ofs) = CHAR_BIT - 1 - (ofs) % CHAR_BIT; \
    } else { \
      (ptr) += (ofs) / CHAR_BIT; \
      (ofs) = (ofs) % CHAR_BIT; \
    } \
  } while (0)

/* entry point, cleans input for passing to bitcpy_main */
void bitcpy(void *dst, ptrdiff_t dst_ofs,
       const void *src, ptrdiff_t src_ofs, size_t cnt)
{
  unsigned char *dstb;
  const unsigned char *srcb;

  assert(dst != 0);
  assert(src != 0);

  /* bail out if nothing to copy */
  if (!cnt)
    return;

  dstb = (unsigned char *)dst;
  srcb = (unsigned char *)src;

  /* normalise bit-pointers */
  NORMALISE(dstb, dst_ofs);
  NORMALISE(srcb, src_ofs);

  bitcpy_main(dstb, dst_ofs, srcb, src_ofs, cnt);
}
