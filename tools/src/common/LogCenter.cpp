#include "rpct/tools/LogCenter.h"

#include <fstream>
#include <sstream>
#include <iomanip>

#include "rpct/tools/LogState.h"

#include "rpct/tools/Publisher.h"

namespace rpct {
namespace tools {

log4cplus::Logger LogCenter::log4cplus_logger_ = log4cplus::Logger::getInstance("LogCenter");

LogCategory::LogCategory(int cat_id
                         , const std::string & name
                         , const std::string & description)
    : id_(cat_id)
    , name_(name)
    , description_(description)
{}

LogMessage::LogMessage(int id, int cat_id, unsigned char level
                       , const std::string & name
                       , const std::string & description)
    : id_(id)
    , cat_id_(cat_id)
    , level_(level)
    , name_(name)
    , description_(description)
{}

void LogMessage::print(std::ostream & os, time_t tv) const
{
    os << std::setw(8) << State::getStateName(level_) << ": ";
    if (tv)
        {
            std::string date(ctime(&tv));
            date.erase(date.size()-1, 1); // remove the \n
            os << date << ": ";
        }
    os << description_ << std::endl;
}
void LogMessage::printHTML(std::ostream & os, time_t tv) const
{
    os << "<li class=\"st_" << State::getStateShort(level_) << "\">";
    if (tv)
        os << "<span class=\"date\">" << ctime(&tv) << ": </span>"; // don't really care about the \n in html
    os << description_ << "</li>" << std::endl;
}

LogCenter::LogCenter()
    : logtype_(-1)
    , unlogtype_(-1)
    , publisher_(0)
    , id_counter_(-1)
    , cat_id_counter_(-1)
    , mutex_()
{}

LogCenter::~LogCenter()
{
    clear();
}

void LogCenter::clear()
{
    WLock lock(mutex_);
    {
        log_messages_type::iterator itEnd = log_messages_.end();
        for (log_messages_type::iterator it = log_messages_.begin()
                 ; it != itEnd
                 ; ++it)
            delete it->second;
    }
    {
        log_categories_type::iterator itEnd = log_categories_.end();
        for (log_categories_type::iterator it = log_categories_.begin()
                 ; it != itEnd
                 ; ++it)
            delete it->second;
    }
    log_messages_.clear();
    log_message_names_.clear();
    log_categories_.clear();
    log_category_names_.clear();
    log_publishers_.clear();
}

void LogCenter::loadCategories(std::string const & filename)
{
    std::ifstream logfile(filename.c_str());
    if (logfile.is_open())
        {
            WLock lock(mutex_);
            int cat_id;
            std::string line, name, description;
            while(std::getline(logfile, line))
                {
                    std::stringstream ssdescription;
                    ssdescription.str(line);
                    ssdescription >> std::skipws >> cat_id >> name;
                    ssdescription.get();
                    std::getline(ssdescription, description);
                    iaddCategory(cat_id, name, description);
                }
        }
    logfile.close();
}
void LogCenter::loadMessages(std::string const & filename)
{
    std::ifstream logfile(filename.c_str());
    if (logfile.is_open())
        {
            RWLock lock(RWLock::write_, mutex_);
            int id;
            int cat_id;
            unsigned int level;
            std::string line, cat_name, name, description;
            while(std::getline(logfile, line))
                {
                    std::stringstream ssdescription(line);
                    ssdescription >> std::skipws >> id >> cat_name >> name >> level;
                    ssdescription.get();
                    std::getline(ssdescription, description);
                    cat_id = igetCategory(lock, cat_name).getId();
                    iaddMessage(id, cat_id, level, name, description);
                }
        }
    logfile.close();
}

void LogCenter::log(LogState & logstate, const std::string & name)
{
    RWLock lock(RWLock::read_, mutex_);
    LogMessage const & lm = igetMessage(lock, name);
    logstate.log(lm.getLevel(), lm.getId(), lm.getCategoryId());
    if (logtype_ > 0)
        {
            if (publisher_)
                publisher_->publish(logstate.getId(), logtype_, lm.getId());
            Publisher * publisher;
            if ((publisher = log_publishers_[lm.getCategoryId()]))
                publisher->publish(logstate.getId(), logtype_, lm.getId());
        }
}
void LogCenter::unlog(LogState & logstate, const std::string & name)
{
    RWLock lock(RWLock::read_, mutex_);
    LogMessage const & lm = igetMessage(lock, name);
    bool unlogged = logstate.unlog(lm.getLevel(), lm.getId(), lm.getCategoryId());
    if (unlogtype_ > 0)
        {
            if (publisher_ && (unlogged | !(publisher_->synced())))
                publisher_->publish(logstate.getId(), unlogtype_, lm.getId());
            Publisher * publisher;
            if ((publisher = log_publishers_[lm.getCategoryId()]) && (unlogged || !publisher->synced()))
                publisher->publish(logstate.getId(), unlogtype_, lm.getId());
        }
}
void LogCenter::log(LogState & logstate, int id)
{
    RWLock lock(RWLock::read_, mutex_);
    LogMessage const & lm = igetMessage(lock, id);
    logstate.log(lm.getLevel(), lm.getId(), lm.getCategoryId());
    if (logtype_ > 0)
        {
            if (publisher_)
                publisher_->publish(logstate.getId(), logtype_, lm.getId());
            Publisher * publisher;
            if ((publisher = log_publishers_[lm.getCategoryId()]))
                publisher->publish(logstate.getId(), logtype_, lm.getId());
        }
}
void LogCenter::unlog(LogState & logstate, int id)
{
    RWLock lock(RWLock::read_, mutex_);
    LogMessage const & lm = igetMessage(lock, id);
    bool unlogged = logstate.unlog(lm.getLevel(), lm.getId(), lm.getCategoryId());
    if (unlogtype_ > 0)
        {
            if (publisher_ && (unlogged | !(publisher_->synced())))
                publisher_->publish(logstate.getId(), unlogtype_, lm.getId());
            Publisher * publisher;
            if ((publisher = log_publishers_[lm.getCategoryId()]) && (unlogged | !(publisher->synced())))
                publisher->publish(logstate.getId(), unlogtype_, lm.getId());
        }
}

void LogCenter::printLog(LogState const & logstate, std::ostream & os)
{
    RWLock rwlock(RWLock::read_, mutex_);
    LogState::log_type const & log(logstate.getLog());
    if (!log.empty())
        {
            LogState::log_type::const_iterator itEnd = log.end();
            for (LogState::log_type::const_iterator it = log.begin()
                     ; it != itEnd
                     ; ++it)
                igetMessage(rwlock, it->second.getId()).print(os, it->second.getTime());
        }
}

void LogCenter::printLogHTML(LogState const & logstate, std::ostream & os)
{
    RWLock rwlock(RWLock::read_, mutex_);
    LogState::log_type const & log(logstate.getLog());
    if (!log.empty())
        {
            os << "<ul class=\"log\">" << std::endl;
            LogState::log_type::const_iterator itEnd = log.end();
            for (LogState::log_type::const_iterator it = log.begin()
                     ; it != itEnd
                     ; ++it)
                igetMessage(rwlock, it->second.getId()).printHTML(os, it->second.getTime());
            os << "</ul>" << std::endl;
        }
}

LogCategory const &  LogCenter::iaddCategory(int cat_id
                                             , const std::string & name
                                             , const std::string & description)
{
    iremoveCategory(cat_id);
    iremoveCategory(name);
    log_publishers_.insert(std::pair<int, Publisher *>(cat_id, (Publisher *)(0)));
    LogCategory * lc = new LogCategory(cat_id, name, description);
    log_categories_.insert(std::pair<int, LogCategory * >(cat_id, lc));
    log_category_names_.insert(std::pair<std::string, LogCategory * >(name, lc));
    return *lc;
}

LogMessage const & LogCenter::iaddMessage(int id, int cat_id, unsigned char level
                                          , const std::string & name
                                          , const std::string & description)
{
    if (log_categories_.find(cat_id) == log_categories_.end())
        {
            std::stringstream sscategory;
            sscategory << "cat_" << cat_id;
            iaddCategory(cat_id, sscategory.str(), sscategory.str());
        }
    iremoveMessage(id);
    iremoveMessage(name);
    LogMessage * lm = new LogMessage(id, cat_id, level, name, description);
    log_messages_.insert(std::pair<int, LogMessage * >(id, lm));
    log_message_names_.insert(std::pair<std::string, LogMessage * >(name, lm));
    return *lm;
}

void LogCenter::iremoveCategory(int cat_id)
{
    log_categories_type::iterator it = log_categories_.find(cat_id);
    if (it != log_categories_.end())
        {
            std::string name = it->second->getName();
            delete it->second;
            log_categories_.erase(cat_id);
            log_category_names_.erase(name);
            log_publishers_.erase(cat_id);
        }
}
void LogCenter::iremoveCategory(const std::string & name)
{
    log_category_names_type::iterator it = log_category_names_.find(name);
    if (it != log_category_names_.end())
        {
            int id = it->second->getId();
            delete it->second;
            log_categories_.erase(id);
            log_category_names_.erase(name);
            log_publishers_.erase(id);
        }
}
void LogCenter::iremoveMessage(int id)
{
    log_messages_type::iterator it = log_messages_.find(id);
    if (it != log_messages_.end())
        {
            std::string name = it->second->getName();
            delete it->second;
            log_messages_.erase(id);
            log_message_names_.erase(name);
        }
}
void LogCenter::iremoveMessage(const std::string & name)
{
    log_message_names_type::iterator it = log_message_names_.find(name);
    if (it != log_message_names_.end())
        {
            int id = it->second->getId();
            delete it->second;
            log_messages_.erase(id);
            log_message_names_.erase(name);
        }
}

LogCategory const & LogCenter::igetCategory(RWLock & lock
                                            , const std::string & name)
{
    log_category_names_type::const_iterator it = log_category_names_.find(name);
    if (it != log_category_names_.end())
        return *(it->second);
    else
        {
            lock.change(RWLock::write_);
            return iaddCategory(--cat_id_counter_, name, name);
        }
}
LogCategory const & LogCenter::igetCategory(RWLock & lock
                                            , int cat_id)
{
    log_categories_type::const_iterator it = log_categories_.find(cat_id);
    if (it != log_categories_.end())
        return *(it->second);
    else
        {
            std::stringstream sscategory;
            sscategory << "cat_" << cat_id;
            lock.change(RWLock::write_);
            return iaddCategory(cat_id, sscategory.str(), sscategory.str());
        }
}
LogMessage const & LogCenter::igetMessage(RWLock & lock
                                          , const std::string & name)
{
    log_message_names_type::const_iterator it = log_message_names_.find(name);
    if (it != log_message_names_.end())
        return *(it->second);
    else
        {
            lock.change(RWLock::write_);
            std::stringstream ssmessage;
            ssmessage << name << " (this short name does not correspond to a log message.  Please report this.)";
            return iaddMessage(--id_counter_, -1, State::unknown_, name, ssmessage.str());
        }
}
LogMessage const & LogCenter::igetMessage(RWLock & lock
                                          , int id)
{
    log_messages_type::const_iterator it = log_messages_.find(id);
    if (it != log_messages_.end())
        return *(it->second);
    else
        {
            std::stringstream ssmessage;
            ssmessage << "log_" << id;
            std::string name(ssmessage.str());
            ssmessage << " (this id does not correspond as a log message.  Please report this.)";
            lock.change(RWLock::write_);
            return iaddMessage(id, -1, State::unknown_, name, ssmessage.str());
        }
}

} // namespace tools
} // namespace rpct
