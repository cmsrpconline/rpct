#include "rpct/tools/RWMutex.h"

namespace rpct {
namespace tools {

void RWMutex::cleanup_unlock(void * arg)
{
    pthread_mutex_unlock((pthread_mutex_t *)(arg));
}

RWMutex::RWMutex(size_t value
                 , size_t max)
    : max_(max)
    , rvalue_(max_)
    , wvalue_(1)
{
    pthread_mutexattr_init(&attr_);
    // protection against deadlock within one thread and "undefined behaviour"
    pthread_mutexattr_settype(&attr_, PTHREAD_MUTEX_ERRORCHECK);
    pthread_mutex_init(&smutex_, &attr_);
    pthread_mutex_init(&vmutex_, &attr_);

    pthread_cond_init(&max_reached_, 0);

    if (!value)
        wtake();
}

RWMutex::~RWMutex()
{
    pthread_cond_destroy(&max_reached_);
    pthread_mutex_destroy(&vmutex_);
    pthread_mutex_destroy(&smutex_);
    pthread_mutexattr_destroy(&attr_);
}

int RWMutex::rtake()
{
    int e = pthread_mutex_lock(&smutex_);
    if (!e)
        {
            pthread_cleanup_push(cleanup_unlock, &smutex_);

            pthread_mutex_lock(&vmutex_);
            pthread_cleanup_push(cleanup_unlock, &vmutex_);
            while (!(wvalue_ && (rvalue_ > 0)) && !e)
                e = pthread_cond_wait(&max_reached_, &vmutex_);
            if (!e)
                --rvalue_;
            pthread_cleanup_pop(1); // vmutex

            pthread_cleanup_pop(1); // smutex
        }
    return e;
}

int RWMutex::rtry_take()
{
    int e = pthread_mutex_trylock(&smutex_);
    if (!e)
        {
            pthread_mutex_lock(&vmutex_);
            if (wvalue_ && (rvalue_ > 0))
                --rvalue_;
            else
                e = -1;
            pthread_mutex_unlock(&vmutex_);

            pthread_mutex_unlock(&smutex_);
        }
    return e;
}

int RWMutex::rtake(timespec const & endtime)
{
    int e = pthread_mutex_timedlock(&smutex_, &endtime);
    if (!e)
        {
            pthread_cleanup_push(cleanup_unlock, &smutex_);

            pthread_mutex_lock(&vmutex_);
            pthread_cleanup_push(cleanup_unlock, &vmutex_);
            while (!(wvalue_ && (rvalue_ > 0)) && !e)
                e = pthread_cond_timedwait(&max_reached_, &vmutex_, &endtime);
            if (!e)
                --rvalue_;
            pthread_cleanup_pop(1); // vmutex

            pthread_cleanup_pop(1); // smutex
        }
    return e;
}

int RWMutex::rgive()
{
    pthread_mutex_lock(&vmutex_);
    if (rvalue_ < max_)
        ++rvalue_;
    pthread_cond_signal(&max_reached_);
    pthread_mutex_unlock(&vmutex_);
    return 0;
}

int RWMutex::wtake()
{
    int e = pthread_mutex_lock(&smutex_);
    if (!e)
        {
            pthread_cleanup_push(cleanup_unlock, &smutex_);

            pthread_mutex_lock(&vmutex_);
            pthread_cleanup_push(cleanup_unlock, &vmutex_);
            while (!(wvalue_ && (rvalue_ == max_)) && !e)
                e =  pthread_cond_wait(&max_reached_, &vmutex_);
            if (!e)
                wvalue_ = 0;
            pthread_cleanup_pop(1); // vmutex

            pthread_cleanup_pop(1); // smutex
        }
    return e;
}

int RWMutex::wtry_take()
{
    int e = pthread_mutex_trylock(&smutex_);
    if (!e)
        {
            pthread_mutex_lock(&vmutex_);
            if (wvalue_ && (rvalue_ == max_))
                wvalue_ = 0;
            else
                e = -1;
            pthread_mutex_unlock(&vmutex_);
            pthread_mutex_unlock(&smutex_);
        }
    return e;
}

int RWMutex::wtake(timespec const & endtime)
{
    int e = pthread_mutex_timedlock(&smutex_, &endtime);
    if (!e)
        {
            pthread_cleanup_push(cleanup_unlock, &smutex_);

            pthread_mutex_lock(&vmutex_);
            pthread_cleanup_push(cleanup_unlock, &vmutex_);
            while (!(wvalue_ && (rvalue_ == max_)) && !e)
                e = pthread_cond_timedwait(&max_reached_, &vmutex_, &endtime);
            if (!e)
                wvalue_ = 0;
            pthread_cleanup_pop(1); // vmutex

            pthread_cleanup_pop(1); // smutex
        }
    return e;
}

int RWMutex::wgive()
{
    pthread_mutex_lock(&vmutex_);
    wvalue_ = 1;
    pthread_cond_signal(&max_reached_);
    pthread_mutex_unlock(&vmutex_);
    return 0;
}

void RWMutex::max(const size_t & max)
{
    pthread_mutex_lock(&vmutex_);
    rvalue_ += (max - max_);
    max_ = max;
    pthread_cond_signal(&max_reached_);
    pthread_mutex_unlock(&vmutex_);
}

size_t RWMutex::max() const
{
    return (max_ > 0 ? (size_t)(max_) : 0);
}

size_t RWMutex::rvalue() const
{
    return (rvalue_ > 0 ? (size_t)(rvalue_) : 0);
}
size_t RWMutex::wvalue() const
{
    return wvalue_;
}

} // namespace tools
} // namespace rpct
