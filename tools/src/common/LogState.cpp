#include "rpct/tools/LogState.h"

namespace rpct {
namespace tools {

LogCenter * LogState::logcenter_(0);

void LogState::calculateLogState()
{
    if (log_.empty())
        setState(State::notset_);
    else
        setState(log_.rbegin()->first);
}

bool LogState::log(unsigned char level, int id, int cat_id)
{
    std::pair<log_type::iterator, bool> in = log_.insert(log_pair_type(level, LogEntry(id, cat_id)));
    if (in.second)
        {
            if (level > State::state_ || log_.size() == 1)
                setState(level);
        }
    else
        in.first->second.setTime();
    return in.second;
}

bool LogState::unlog(unsigned char level, int id, int cat_id)
{
    bool removed = log_.erase(log_pair_type(level, LogEntry(id, cat_id)));
    if (removed && level == State::state_)
        calculateLogState();
    return removed;
}

LogState::LogState()
{
    if (!logcenter_)
        logcenter_ = new LogCenter();
}

TreeLogState::TreeLogState(int position
                           , TreeNode< TreeLogState *, true> * parent
                           , TreeNode< TreeLogState *, true> * sibling)
    : TTreeLogState<TreeLogState *>(position, parent, sibling)
{}

} // namespace tools
} // namespace rpct
