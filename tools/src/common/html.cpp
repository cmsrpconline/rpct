#include "rpct/tools/html.h"

#include "rpct/tools/State.h"

namespace rpct {
namespace tools {
namespace html {

void header(std::ostream & os, const std::string & title, const std::string & subtitle, bool topleft)
{
    os << "<!DOCTYPE html>" << std::endl;
    os << "<html lang=\"en\" dir=\"ltr\">" << std::endl;
    os << "<head>" << std::endl;
    os << "<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\" /> " << std::endl;
    os << "<title>" << title << " - " << subtitle << "</title>" << std::endl;
    os << "<style type=\"text/css\">" << std::endl;
    os << "body{font-size:10pt;font-family:Helvetica, Verdana, Arial, Times New Roman, Times, Serif;background-color:#babdb6;}" << std::endl;
    if (topleft)
        os << "body{margin-left:180px;padding-left:0;}" << std::endl;
    os << "h1,h2,h3{color:#204a87;font-size:16pt;font-weight:bold;border-bottom:1px solid #204a87;}" << std::endl;
    os << "h2,h3{font-size:14pt;border-bottom:1px dotted #204a87;}" << std::endl;
    os << "h3{font-size:12pt;}" << std::endl;

    os << "table, embed, .box {background-color:#fff;margin:10px;padding:5px;border:0;"
       << "border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;"
       << "box-shadow: 5px 5px 5px #888a85;-moz-box-shadow: 5px 5px 5px #888a85;-webkit-box-shadow: 5px 5px 5px #888a85;}" << std::endl;
    os << ".box h1, .box h2, .box h3 {border:0;margin:5px;}" << std::endl;
    os << ".topleft {margin:0;width:150px;position:fixed;top:10px;left:10px;}" << std::endl;

    os << "a{color:#204a87;text-decoration:none;font-weight:normal;}" << std::endl;
    os << "a:hover{text-decoration:underline;}" << std::endl;

    os << "embed{margin:10px;}" << std::endl;

    os << "table {border-collapse:collapse;border-spacing:0;}" << std::endl;
    os << "th {background-color:#729fcf;border:1px solid #204a87;padding:4px;min-width:75px;}" << std::endl;
    os << "td {background-color:#eeeeec;border:1px solid #555753;padding:4px;min-width:75px;}" << std::endl;

    os << "fieldset{margin:10px;border:0;border-top:1px solid #888a85;border-left:1px solid #888a85;border-top-left-radius:5px;-moz-border-radius-topleft:5px;-webkit-border-top-left-radius:5px;}" << std::endl;
    os << "fieldset legend{font-weight:bold;}" << std::endl;
    os << "input, select {margin:5px;font-size:8pt;}" << std::endl;

    os << ".weblogger {font:8pt monospace;overflow:auto;height:120px;border:1px solid #888;margin:2px;background-color:#efefef;}" << std::endl;
    os << "ul.log {list-style-type:none;margin:10px;padding:5px;border:1px solid #000;}" << std::endl;
    os << "ul.states {list-style-type:none;margin:0;padding:2px;}" << std::endl;
    os << "ul.states li {margin:0;padding:2px 2px 2px 26px;background:url('/extern/icons/control_stop_blue.png') no-repeat 2px 2px;}" << std::endl;
    for (unsigned char level = 0 ; level < rpct::tools::State::max_ ; ++level)
        {
            os << ".bg_" << State::getStateShort(level) << ", ul.states .bg_" << State::getStateShort(level) << " {background-color:"
               << State::getStateBgColor(level) << "}" << std::endl;
            os << ".fg_" << State::getStateShort(level) << ", ul.states .fg_" << State::getStateShort(level) << " {background-image:"
               << "url('" << State::getStateImage(level)
               << "');margin:0;padding:2px 2px 2px 26px;background-repeat:no-repeat;background-position:2px 2px;}" << std::endl;
            os << ".st_" << State::getStateShort(level) << ", ul.states .st_" << State::getStateShort(level) << " {background:"
               << State::getStateBgColor(level)
               << " url('" << State::getStateImage(level)
               << "') no-repeat 2px 2px;margin:0;padding:2px 2px 2px 26px;}" << std::endl;
            os << "." << State::getStateName(level) << " {background:"
               << State::getStateBgColor(level)
               << " url('" << State::getStateImage(level)
               << "') no-repeat 4px 4px;font-size:8pt;font-style:italic;border:1px solid black;margin:5px;padding:4px 4px 4px 30px;}" << std::endl;
        }
    os << ".small {font-size:8pt;}" << std::endl;
    os << ".date {font-family:monospace;font-size:8pt;}" << std::endl;
    os << ".mono {font-family:monospace;}" << std::endl;
    os << "</style>" << std::endl;
    os << "</head>" << std::endl;
    os << "<body>" << std::endl;

    os << "<div class=\"box\"><h1>" << title << "</h1><h2>" << subtitle << "</h2></div>" << std::endl;
}

void footer(std::ostream & os)
{
    os << "</body>" << std::endl;
    os << "</html>" << std::endl;
}

} // namespace html
} // namespace tools
} // namespace rpct
