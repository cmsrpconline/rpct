#include "rpct/tools/Drawable.h"

namespace rpct {
namespace tools {

Drawable::~Drawable()
{}

void Drawable::printSVGEmbed(std::ostream & os, const std::string & location, int depth) const
{
    os << "<embed class=\"svg\" width=\"" << getSVGWidth() << "\" height=\"" << getSVGHeight()
       << "\" src=\"" << location << "\" type=\"image/svg+xml\" align=\"middle\" />" << std::endl;
}

void Drawable::printSVGHeader(std::ostream & os, int depth) const
{
    os << "<?xml version=\"1.0\" standalone=\"no\"?>" << std::endl;
    os << "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" "
       << "\"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">" << std::endl;
    os << "<svg width=\"100%\" height=\"100%\" version=\"1.1\" baseProfile=\"full\" "
       << "xmlns=\"http://www.w3.org/2000/svg\" "
       << "xmlns:xlink=\"http://www.w3.org/1999/xlink\" "
       << "xmlns:ev=\"http://www.w3.org/2001/xml-events\">" << std::endl;
    os << "<defs><style type=\"text/css\"><![CDATA[" << std::endl;
    printSVGCSS(os, depth);
    os << "]]></style></defs>" << std::endl;
}
void Drawable::printSVGCSS(std::ostream & os, int depth) const
{}
void Drawable::printSVGFooter(std::ostream & os, int depth) const
{
    os << "</svg>" << std::endl;
}

} // namespace tools
} // namespace rpct
