#include "rpct/tools/Mutex.h"

#include "rpct/tools/RunningTimeVal.h"

namespace rpct {
namespace tools {

Mutex::Mutex(size_t value, bool recursive)
    : value_(1)
{
    pthread_mutexattr_init(&attr_);
    if (recursive)
        pthread_mutexattr_settype(&attr_, PTHREAD_MUTEX_RECURSIVE);
    else // protection against deadlock within one thread and "undefined behaviour"
        pthread_mutexattr_settype(&attr_, PTHREAD_MUTEX_ERRORCHECK);
    pthread_mutex_init(&mutex_, &attr_);
    if (!value)
        take();
}

Mutex::~Mutex()
{
    pthread_mutex_destroy(&mutex_);
    pthread_mutexattr_destroy(&attr_);
}

int Mutex::take()
{
    int e = pthread_mutex_lock(&mutex_);
    if (!e)
        value_ = 0;
    return e;
}

int Mutex::try_take()
{
    int e = pthread_mutex_trylock(&mutex_);
    if (!e)
        value_ = 0;
    return e;
}

int Mutex::take(timespec const & endtime)
{
    int e = pthread_mutex_timedlock(&mutex_, &endtime);
    if (!e)
        value_ = 0;
    return e;
}

int Mutex::give()
{
    int e = pthread_mutex_unlock(&mutex_);
    if (!e)
        value_ = 1;
    return e;
}

} // namespace tools
} // namespace rpct
