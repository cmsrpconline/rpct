#include "rpct/tools/RunningTimeVal.h"

namespace rpct {
namespace tools {

RunningTimeVal::RunningTimeVal(int type, timeval const * tval)
    : type_(type)
{
    init(tval);
}
RunningTimeVal::RunningTimeVal(timeval const & tval)
    : type_(timeval_)
{
    init(&tval);
}
RunningTimeVal::RunningTimeVal(int type, time_t sec, suseconds_t usec)
    : type_(type)
{
    timeval tval;
    timeval_fill(tval, sec, usec);
    init(&tval);
}

void RunningTimeVal::sleep() const
{
    if (type_ <= 0)
        {
            timespec ts = getTimeSpec();
            ::nanosleep(&ts, 0);
        }
}

std::string RunningTimeVal::getDate() const
{
    timeval tval = getTimeVal();
    std::string date(ctime(&(tval.tv_sec)));
    date.erase(date.size()-1, 1); // remove the \n
    return date;
}

void RunningTimeVal::init(timeval const * tval)
{
    if (!tval)
        {
            gettimeofday(&tref_, 0);
            dt_ = tref_;
        }
    else
        {
            tref_ = *tval;
            dt_ = tref_;
            if (type_ < 0)
                {
                    gettimeofday(&tref_, 0);
                    tref_ += dt_;
                }
        }
}

std::ostream & operator << (std::ostream & outstream, const RunningTimeVal & rtval)
{
    timeval tval = rtval.getTimeVal();

    outstream << (rtval.getType() < 0 ? '-' : '+');
    outstream << tval.tv_sec << ".";
    char fill = outstream.fill('0');
    int width = outstream.width(6);
    outstream << tval.tv_usec << "s";
    outstream.fill(fill);
    outstream.width(width);
    return outstream;
}

} // namespace tools
} // namespace rpct
