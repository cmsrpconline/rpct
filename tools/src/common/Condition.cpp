#include "rpct/tools/Condition.h"

namespace rpct {
namespace tools {

void Condition::cleanup_substract(void * arg)
{
    --(*((size_t *)(arg)));
}

void Condition::cleanup_unlock(void * arg)
{
    pthread_mutex_unlock((pthread_mutex_t *)(arg));
}

Condition::Condition(size_t value)
    : Mutex(value, false)
    , nwaiting_(0)
    , nreleases_(0)
{
    pthread_cond_init(&condition_, 0);
}

Condition::~Condition()
{
    free_all();
    pthread_cond_destroy(&condition_);
}

void Condition::wait(bool take)
{
    int e = 0;
    if (take)
        pthread_mutex_lock(&mutex_);
    pthread_cleanup_push(cleanup_unlock, &mutex_);
    ++nwaiting_;
    pthread_cleanup_push(cleanup_substract, (void *) &nwaiting_);
    while (!nreleases_ && !e) // "spurious wakeups" protection
        e = pthread_cond_wait(&condition_, &mutex_);
    pthread_cleanup_pop(1);
    if (!e)
        --nreleases_;
    pthread_cleanup_pop(take ? 1 : 0);
}

int Condition::wait(timespec const & endtime, bool take)
{
    int e = 0;
    if (take)
        e = pthread_mutex_timedlock(&mutex_, &endtime);
    if (!e)
        {
            pthread_cleanup_push(cleanup_unlock, &mutex_);
            ++nwaiting_;
            pthread_cleanup_push(cleanup_substract, (void *) &nwaiting_);
            while (!nreleases_ && !e) // "spurious wakeups" protection
                e = pthread_cond_timedwait(&condition_, &mutex_, &endtime);
            pthread_cleanup_pop(1);
            if (!e)
                --nreleases_;
            pthread_cleanup_pop(take ? 1 : 0);
        }
    return e;
}

void Condition::free_one(bool take)
{
    if (take)
        pthread_mutex_lock(&mutex_);
    if (nwaiting_ > nreleases_)
        {
            ++nreleases_;
            pthread_cond_signal(&condition_);
        }
    if (take)
        pthread_mutex_unlock(&mutex_);
}

void Condition::free_all(bool take)
{
    if (take)
        pthread_mutex_lock(&mutex_);
    nreleases_ = nwaiting_;
    pthread_cond_broadcast(&condition_);
    if (take)
        pthread_mutex_unlock(&mutex_);
}

} // namespace tools
} // namespace rpct
