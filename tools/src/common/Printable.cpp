#include "rpct/tools/Printable.h"
#include <iomanip>

namespace rpct {
namespace tools {

void Parameters::add(const std::string & key, const std::string & value)
{
    parameters_.push_back(parameter_type(key, value));
    unsigned int length = key.length();
    if (keylength_ < length)
        keylength_ = length;
}
void Parameters::add(const std::string & key, const bool & value)
{
    parameters_.push_back(parameter_type(key, (value?"true":"false")));
    unsigned int length = key.length();
    if (keylength_ < length)
        keylength_ = length;
}

Printable::~Printable()
{}
void Parameters::print(std::ostream & os) const
{
    const_iterator IparameterEnd = parameters_.end();
    for (const_iterator Iparameter = parameters_.begin()
             ; Iparameter != IparameterEnd
             ; ++Iparameter)
        os << std::setw(keylength_) << Iparameter->first << ": " << Iparameter->second << std::endl;
}
void Parameters::printHTML(std::ostream & os) const
{
    if (parameters_.size() > 0)
        {
            os << "<table class=\"parameters\">" << std::endl;
            os << "<tr><th>key</th><th>value</th></tr>";
            const_iterator IparameterEnd = parameters_.end();
            for (const_iterator Iparameter = parameters_.begin()
                     ; Iparameter != IparameterEnd
                     ; ++Iparameter)
                os << "<tr><td>" << Iparameter->first
                   << "</td><td>" << Iparameter->second
                   << "</td></tr>" << std::endl;
            os << "</table>" << std::endl;
        }
}

void Printable::printParameters(Parameters & parameters) const
{}

} // namespace tools
} // namespace rpct
