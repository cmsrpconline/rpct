#include "rpct/tools/WebLogger.h"

#include "log4cplus/spi/loggingevent.h"

namespace rpct {
namespace tools {

std::map<log4cplus::LogLevel, unsigned char> * WebLogger::levels_ = 0;

LogEvent::LogEvent(std::string const & message, unsigned char level, time_t tv)
    : message_(message)
    , level_(level)
    , tv_(tv)
{
    if (!tv_)
        tv_ = time(0);
}

void LogEvent::print(std::ostream & os) const
{
    std::string date(ctime(&tv_));
    date.erase(date.size()-1, 1);
    os << State::getStateName(level_) << ": "
       << date << ": " << message_ << std::endl;
}
void LogEvent::printHTML(std::ostream & os) const
{
    os << "<li class=\"fg_" << State::getStateShort(level_) << "\">"
       << "<span class=\"date\">" << ctime(&tv_) << ": </span>"
       << message_ << "</li>" << std::endl;
}

void WebLogger::append(const log4cplus::spi::InternalLoggingEvent & event)
{
    WLock wlock(events_mutex_);

    if (events_.size() >= max_events_)
        events_.pop_back();
    unsigned char level = State::unknown_;
    std::map<log4cplus::LogLevel, unsigned char>::const_iterator it = levels_->find(event.getLogLevel());
    if (it != levels_->end())
        level = it->second;
    events_.push_front(LogEvent(event.getMessage(), level, event.getTimestamp().getTime()));
}

WebLogger::WebLogger(size_t max_events)
    : max_events_(max_events)
{
    if (!levels_)
        {
            levels_ = new std::map<log4cplus::LogLevel, unsigned char>();
            levels_->insert(std::pair<log4cplus::LogLevel, unsigned char>(log4cplus::NOT_SET_LOG_LEVEL, State::notset_));
            levels_->insert(std::pair<log4cplus::LogLevel, unsigned char>(log4cplus::ALL_LOG_LEVEL, State::notset_));
            levels_->insert(std::pair<log4cplus::LogLevel, unsigned char>(log4cplus::TRACE_LOG_LEVEL, State::notset_));
            levels_->insert(std::pair<log4cplus::LogLevel, unsigned char>(log4cplus::DEBUG_LOG_LEVEL, State::notset_));
            levels_->insert(std::pair<log4cplus::LogLevel, unsigned char>(log4cplus::INFO_LOG_LEVEL, State::info_));
            levels_->insert(std::pair<log4cplus::LogLevel, unsigned char>(log4cplus::WARN_LOG_LEVEL, State::warn_));
            levels_->insert(std::pair<log4cplus::LogLevel, unsigned char>(log4cplus::ERROR_LOG_LEVEL, State::error_));
            levels_->insert(std::pair<log4cplus::LogLevel, unsigned char>(log4cplus::FATAL_LOG_LEVEL, State::error_));
            levels_->insert(std::pair<log4cplus::LogLevel, unsigned char>(log4cplus::OFF_LOG_LEVEL, State::error_));
        }
}

void WebLogger::print(std::ostream & os) const
{
    RLock rlock(events_mutex_);

    std::deque<LogEvent>::const_iterator IeventEnd = events_.end();
    for (std::deque<LogEvent>::const_iterator Ievent = events_.begin()
             ; Ievent != IeventEnd
             ; ++Ievent)
        Ievent->print(os);
}
void WebLogger::printHTML(std::ostream & os) const
{
    RLock rlock(events_mutex_);

    os << "<div class=\"box\">" << std::endl;
    os << "<h3>Log</h3>" << std::endl;
    os << "<ul class=\"states weblogger\">" << std::endl;
    std::deque<LogEvent>::const_iterator IeventEnd = events_.end();
    for (std::deque<LogEvent>::const_iterator Ievent = events_.begin()
             ; Ievent != IeventEnd
             ; ++Ievent)
        Ievent->printHTML(os);
    os << "</ul></div>" << std::endl;
}

} // namespace tools
} // namespace rpct
