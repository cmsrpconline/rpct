#include "rpct/tools/Progress.h"

namespace rpct {
namespace tools {

Progress::Progress(log4cplus::Logger & _logger
                   , std::string const & _title
                   , rpct::tools::ActionSignature * _pause_action
                   , rpct::tools::ActionSignature * _resume_action)
    : logger_(_logger)
    , state_(idle_)
    , title_(_title), description_("")
    , pause_action_(_pause_action)
    , resume_action_(_resume_action)
    , value_(0), failed_(0), maximum_(0)
    , chrono_(), time_(), duration_()
{}

} // namespace tools
} // namespace rpct
