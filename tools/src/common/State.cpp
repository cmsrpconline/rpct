#include "rpct/tools/State.h"

namespace rpct {
namespace tools {

const unsigned char State::ok_      = 0;
const unsigned char State::notset_  = 1;
const unsigned char State::info_    = 2;
const unsigned char State::warn_    = 3;
const unsigned char State::error_   = 4;
const unsigned char State::unknown_ = 5;
const unsigned char State::default_ = State::notset_;
const unsigned char State::max_     = 5;

const char * State::name_[] = {"ok", "notset", "info", "warn", "error", "unknown"};
const char * State::short_[] = {"o", "n", "i", "w", "e", "u"};
const char * State::color_[] = {"#67e667", "#eee", "#6C8Ce5", "#ffd073", "#ff7373", "#ffd073"};
const char * State::bgcolor_[] = {"#dfd", "#efefef", "#ddf", "#ffb", "#fdd", "#ffb"};
const char * State::image_[] = {"/extern/icons/accept.png"
                                , "/extern/icons/control_stop.png"
                                , "/extern/icons/information.png"
                                , "/extern/icons/error.png"
                                , "/extern/icons/exclamation.png"
                                , "/extern/icons/lightning.png"};


const log4cplus::LogLevel State::loglevel_[] = {log4cplus::INFO_LOG_LEVEL, log4cplus::INFO_LOG_LEVEL, log4cplus::INFO_LOG_LEVEL, log4cplus::WARN_LOG_LEVEL, log4cplus::ERROR_LOG_LEVEL, log4cplus::INFO_LOG_LEVEL};

bool operator<(const State & state_a, const State & state_b)
{
    return (state_a.getState() < state_b.getState());
}

std::ostream & operator<<(std::ostream & outstream, const State & state)
{
    outstream << State::getStateName(state.getState());
    return outstream;
}

State::State()
    : state_(default_)
{}

State::~State()
{}

TreeState::TreeState(int position
                     , TreeNode< TreeState *, true > * parent
                     , TreeNode< TreeState *, true > * sibling)
    : TTreeState<TreeState *>(position, parent, sibling)
{}

} // namespace tools
} // namespace rpct
