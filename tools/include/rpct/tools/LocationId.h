/** Filip Thyssen */

#ifndef rpct_tools_LocationId_h_
#define rpct_tools_LocationId_h_

#include <stdint.h> // uint32_t
#include <string>
#include <set>
#include <ostream>

namespace rpct {
namespace tools {

namespace locationid {

const int wildcard_ = 0x7fffffff;

} // namespace locationid

template<int nfields_, const int * pos_, const int * masks_, const int * offset_>
class LocationId
{
protected:
    typedef LocationId<nfields_, pos_, masks_, offset_> lid_type;
public:
    LocationId(uint32_t id = 0x0);
    virtual ~LocationId();

    operator uint32_t() const;
    uint32_t getId() const;

    void setId(uint32_t id);

    void reset();

    bool matches(lid_type const & in) const;

    bool operator <(lid_type const & in) const;
    bool operator ==(lid_type const & in) const;

    virtual std::string getName() const;
protected:
    void mask();
    int get(int field) const;
    void set(int field, int value = locationid::wildcard_);

    // to be deleted by user
    uint32_t * split() const;

    uint32_t id_;
    uint32_t mask_;
};

template<int nfields_, const int * pos_, const int * masks_, const int * offset_>
std::ostream & operator<< (std::ostream & os, LocationId<nfields_, pos_, masks_, offset_> const & in);

template<typename T>
class LocationIdSelection
{
public:
    typedef typename std::set<T> selection;
    typedef typename std::set<T>::const_iterator const_iterator;
public:
    LocationIdSelection();
    ~LocationIdSelection();

    const_iterator select_begin() const;
    const_iterator select_end() const;
    const_iterator mask_begin() const;
    const_iterator mask_end() const;

    void select(T const & id);
    void deselect(T const & id);
    void mask(T const & id);
    void unmask(T const & id);

    void add(T const & id);
    void remove(T const & id);

    bool matches(T const & id) const;
protected:
    selection select_;
    selection mask_;
};

} // namespace tools
} // namespace rpct

#include "rpct/tools/LocationId-inl.h"

#endif // rpct_tools_LocationId_h_
