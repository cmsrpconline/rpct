/** Filip Thyssen */

#ifndef rpct_tools_Action_inl_h_
#define rpct_tools_Action_inl_h_

#include "rpct/tools/Action.h"

namespace rpct {
namespace tools {

inline ActionSignature::~ActionSignature()
{}

inline int ActionSignature::operator()() const
{
    return invoke();
}

inline std::string ActionSignature::name() const
{
    return "undefinedActionSignature";
}

template<typename Listener>
inline Action<Listener>::Action(Listener & listener
                                , int (Listener::*func)()
                                , const std::string & name)
    : listener_(listener)
    , func_(func)
    , name_(name)
{}

template<typename Listener>
inline Action<Listener>::~Action()
{}

template<typename Listener>
inline int Action<Listener>::invoke() const
{
    if (func_)
        return (listener_.*func_)();
    else
        return -1;
}

template<typename Listener>
inline std::string Action<Listener>::name() const
{
    return name_;
}

template<typename T>
inline ObjectActionSignature<T>::~ObjectActionSignature()
{}

template<typename T>
inline int ObjectActionSignature<T>::operator()(T obj) const
{
    return invoke(obj);
}

template<typename T>
inline std::string ObjectActionSignature<T>::name() const
{
    return "undefinedObjectActionSignature";
}

template<typename T, typename Listener>
inline ObjectAction<T, Listener>::ObjectAction(Listener & listener
                                               , int (Listener::*func)(T)
                                               , const std::string & name)
    : listener_(listener)
    , func_(func)
    , name_(name)
{}

template<typename T, typename Listener>
inline ObjectAction<T, Listener>::~ObjectAction()
{}

template<typename T, typename Listener>
inline int ObjectAction<T, Listener>::invoke(T obj) const
{
    if (func_)
        return (listener_.*func_)(obj);
    else
        return -1;
}

template<typename T, typename Listener>
inline std::string ObjectAction<T, Listener>::name() const
{
    return name_;
}

} // namespace tools
} // namespace rpct

#endif // rpct_tools_Action_inl_h_
