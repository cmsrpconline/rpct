/* Filip Thyssen */

#ifndef rpct_tools_Progress_inl_h
#define rpct_tools_Progress_inl_h

#include "rpct/tools/Progress.h"

#include "log4cplus/loggingmacros.h"
#include "rpct/tools/Action.h"

namespace rpct {
namespace tools {

inline bool Progress::pause()
{
    rpct::tools::Lock _lock(condition_);

    if (state_ & control_paused_)
        return false;

    LOG4CPLUS_TRACE(logger_, title_ << " pausing");

    state_ |= control_paused_;

    return true;
}

inline bool Progress::resume()
{
    rpct::tools::Lock _lock(condition_);

    if (!(state_ & control_paused_))
        return false;

    LOG4CPLUS_TRACE(logger_, title_ << " resuming");

    state_ &= ~control_paused_;

    if (state_ & process_paused_)
        condition_.free_one(false);

    return true;
}

inline bool Progress::terminate()
{
    rpct::tools::Lock _lock(condition_);

    if (!(state_ & control_active_))
        return false;

    LOG4CPLUS_TRACE(logger_, title_ << " terminating");

    state_ &= ~control_active_;

    if (state_ & process_paused_)
        condition_.free_one(false);

    return true;
}

inline std::string Progress::getTitle() const
{
    rpct::tools::Lock _lock(condition_);
    return title_ + std::string(" (") + getStateName() + std::string(")");
}

inline Progress::State Progress::getState() const
{
    rpct::tools::Lock _lock(condition_);
    return state_;
}

inline bool Progress::isControlActive() const
{
    rpct::tools::Lock _lock(condition_);
    return (state_ & control_active_);
}

inline bool Progress::isControlPaused() const
{
    rpct::tools::Lock _lock(condition_);
    return (state_ & control_paused_);
}

inline bool Progress::isProcessActive() const
{
    rpct::tools::Lock _lock(condition_);
    return (state_ & process_active_);
}

inline bool Progress::isProcessPaused() const
{
    rpct::tools::Lock _lock(condition_);
    return (state_ & process_paused_);
}

inline std::string Progress::getStateName() const
{
    switch (state_)
        {
        case (idle_):
            return "idle";
            break;
        case (                  control_paused_):
        case (control_active_ | control_paused_ | process_active_ | process_paused_):
            return "paused";
        break;
        case (control_active_ | control_paused_ | process_active_):
            return "pausing";
            break;
        case (                                    process_active_):
        case (                                    process_active_ | process_paused_):
            return "terminating";
        break;
        case (                  control_paused_ | process_active_):
        case (                  control_paused_ | process_active_ | process_paused_):
            return "terminating and paused";
        break;
        case (control_active_                   | process_active_):
            return "running";
            break;
        case (control_active_                   | process_active_ | process_paused_):
            return "resuming";
            break;
        default:
            break;
        }
    return "?";
}

inline std::string const & Progress::getDescription() const
{
    rpct::tools::Lock _lock(condition_);
    return description_;
}

inline Time Progress::getStart() const
{
    rpct::tools::Lock _lock(condition_);
    return time_;
}

inline Time Progress::getDuration() const
{
    rpct::tools::Lock _lock(condition_);
    return chrono_.duration();
}

inline Time Progress::getEndEstimate() const
{
    rpct::tools::Lock _lock(condition_);

    if (state_ == idle_)
        return time_ + chrono_.duration();

    float _progress = (maximum_ ? (float)(value_ + failed_) / (float)maximum_ : 1.);

    if (_progress > 0.)
        return time_ + duration_ / _progress;

    return time_;
}

inline std::size_t Progress::getValue() const
{
    rpct::tools::Lock _lock(condition_);
    return value_;
}

inline std::size_t Progress::getFailed() const
{
    rpct::tools::Lock _lock(condition_);
    return failed_;
}

inline std::size_t Progress::getMaximum() const
{
    rpct::tools::Lock _lock(condition_);
    return maximum_;
}

inline float Progress::getProgress() const
{
    rpct::tools::Lock _lock(condition_);
    return ((maximum_ - failed_) > value_ ? (float)(value_) / (float)(maximum_ - failed_) : 1.);
}

inline bool Progress::start()
{
    rpct::tools::Lock _lock(condition_);

    if (state_ & (control_active_ | process_active_))
        return false;

    chrono_.reset();
    time_.set();
    duration_.set(0);

    LOG4CPLUS_TRACE(logger_, title_ << " started");

    state_ |= (control_active_ | process_active_);

    return true;
}

inline bool Progress::checkPaused()
{
    rpct::tools::Lock _lock(condition_);

    // terminating has priority
    if (!(state_ & control_active_))
        return false;

    if (!(state_ & control_paused_))
        return false;

    if (pause_action_)
        pause_action_->invoke();

    chrono_.pause();
    state_ |= process_paused_;

    LOG4CPLUS_TRACE(logger_, title_ << " paused");

    condition_.wait(false);

    if (resume_action_)
        resume_action_->invoke();

    time_.set();
    time_ -= chrono_.duration();
    chrono_.resume();

    state_ &= ~(process_paused_);

    LOG4CPLUS_TRACE(logger_, title_ << " resumed");

    return true;
}

inline bool Progress::checkTerminated()
{
    rpct::tools::Lock _lock(condition_);

    if (state_ & control_active_)
        return false;

    LOG4CPLUS_TRACE(logger_, title_ << " stopping");

    return true;
}

inline bool Progress::stop()
{
    rpct::tools::Lock _lock(condition_);

    if (!(state_ & (control_active_ | process_active_)))
        return false;

    state_ &= ~(control_active_ | process_active_);
    chrono_.pause();

    LOG4CPLUS_TRACE(logger_, title_ << " stopped");

    return true;
}

inline bool Progress::fail()
{
    rpct::tools::Lock _lock(condition_);

    if (!(state_ & (control_active_ | process_active_)))
        return false;

    state_ &= ~(control_active_ | process_active_);
    chrono_.pause();

    failed_ = (maximum_ > value_ ? maximum_ - value_ : 0);

    LOG4CPLUS_TRACE(logger_, title_ << " failed");

    return true;
}

inline void Progress::init()
{
    rpct::tools::Lock _lock(condition_);

    chrono_.reset();
    time_.set();
    duration_.set(0);

    value_ = 0;
    failed_ = 0;
    maximum_ = 0;

    LOG4CPLUS_TRACE(logger_, title_ << " initiated");
}

inline void Progress::setValue(std::size_t _value)
{
    rpct::tools::Lock _lock(condition_);
    value_ = _value;
    duration_ = chrono_.duration();
}

inline void Progress::setFailed(std::size_t _failed)
{
    rpct::tools::Lock _lock(condition_);
    failed_ = _failed;
    duration_ = chrono_.duration();
}

inline void Progress::setMaximum(std::size_t _maximum)
{
    rpct::tools::Lock _lock(condition_);
    maximum_ = _maximum;
}

inline void Progress::setTitle(std::string const & _title)
{
    rpct::tools::Lock _lock(condition_);
    title_ = _title;
}

inline void Progress::setDescription(std::string const & _description)
{
    rpct::tools::Lock _lock(condition_);
    description_ = _description;
}

inline void Progress::addValue(std::size_t _value)
{
    rpct::tools::Lock _lock(condition_);
    value_ += _value;
    duration_ = chrono_.duration();
}

inline void Progress::addFailed(std::size_t _failed)
{
    rpct::tools::Lock _lock(condition_);
    failed_ += _failed;
    duration_ = chrono_.duration();
}

inline void Progress::addDescription(std::string const & _description)
{
    rpct::tools::Lock _lock(condition_);
    description_ += _description;
}


} // namespace tools
} // namespace rpct

#endif // rpct_tools_Progress_inl_h
