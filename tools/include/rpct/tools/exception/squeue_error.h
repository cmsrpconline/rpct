/** Filip Thyssen */

#ifndef rpct_tools_exception_squeue_error_h_
#define rpct_tools_exception_squeue_error_h_

#include <stdexcept>

namespace rpct {
namespace tools {
namespace exception {

class squeue_error: public std::runtime_error
{
public:
    squeue_error(std::string const & message)
        : std::runtime_error(message)
    {}

    squeue_error(std::exception const & e)
        : std::runtime_error(e.what())
    {}
};
class squeue_timeout_error: public squeue_error
{
public:
    squeue_timeout_error(std::string const & message)
        : squeue_error(message)
    {}

    squeue_timeout_error(std::exception const & e)
        : squeue_error(e.what())
    {}
};

} // namespace exception
} // namespace tools
} // namespace rpct

#endif // rpct_tools_exception_squeue_error_h_
