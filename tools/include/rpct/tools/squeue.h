/** Filip Thyssen */

#ifndef rpct_tools_squeue_h_
#define rpct_tools_squeue_h_

#include <sys/time.h>
#include <time.h>

#include <queue>
#include <string>

#include "rpct/tools/Condition.h"
#include "rpct/tools/exception/squeue_error.h"
#include "rpct/tools/Action.h"

namespace rpct {
namespace tools {

template<typename Object>
class squeue
{
public:
    /** squeue constructor
     * \param max_size maximum number of elements, 0 = system limit.
     * \param end_function function to run on an element before removing it
     *                      with this->remove, this->clear or the destructor
     */
    squeue(size_t max_size = 0
           , const std::string & name = std::string("undefined_squeue")
           , ObjectActionSignature<Object &> * end_function = 0);
    virtual ~squeue();

    const std::string & name() const;
    
    size_t max_size() const;
    void max_size(size_t max_size);

    size_t size();
    bool empty();

    void remove(const Object & element, bool end_function = true) throw (exception::squeue_error);
    void clear();
    /** stop every process waiting for a wait_pop */
    void free();

    Object pop() throw (exception::squeue_error);
    Object pop_front() throw (exception::squeue_error);
    Object pop_back() throw (exception::squeue_error);

    void push(const Object & element) throw (exception::squeue_error);
    void push_back(const Object & element) throw (exception::squeue_error);
    void push_front(const Object & element) throw (exception::squeue_error);

    Object wait_pop() throw (exception::squeue_error);
    Object wait_pop_front() throw (exception::squeue_error);
    Object wait_pop_back() throw (exception::squeue_error);

    Object wait_pop(timeval const & timeout) throw (exception::squeue_error);
    Object wait_pop_front(timeval const & timeout) throw (exception::squeue_error);
    Object wait_pop_back(timeval const & timeout) throw (exception::squeue_error);

    Object wait_pop(time_t sec, suseconds_t usec = 0) throw (exception::squeue_error);
    Object wait_pop_front(time_t sec, suseconds_t usec = 0) throw (exception::squeue_error);
    Object wait_pop_back(time_t sec, suseconds_t usec = 0) throw (exception::squeue_error);

    Object wait_pop(timespec const & endtime) throw (exception::squeue_error);
    Object wait_pop_front(timespec const & endtime) throw (exception::squeue_error);
    Object wait_pop_back(timespec const & endtime) throw (exception::squeue_error);

protected:
    std::deque<Object> queue_;

    Condition q_is_empty_;

    const std::string name_;

    ObjectActionSignature<Object &> * const end_function_;

    volatile size_t max_size_;
    volatile bool freeing_;
};

} // namespace tools
} // namespace rpct

#include "rpct/tools/squeue-inl.h"

#endif // rpct_tools_squeue_h_
