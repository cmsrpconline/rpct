/** Filip Thyssen */

#ifndef rpct_tools_RunningTimeVal_inl_h_
#define rpct_tools_RunningTimeVal_inl_h_

#include "rpct/tools/RunningTimeVal.h"

namespace rpct {
namespace tools {

inline void timeval_validate(timeval & tv)
{
    // no negative values
    while (tv.tv_usec < 0 && tv.tv_sec > 0)
        {
            --(tv.tv_sec);
            tv.tv_usec += tv_usec_sec_;
        }
    if (tv.tv_sec < 0  || tv.tv_usec < 0)
        {
            tv.tv_sec = 0;
            tv.tv_usec = 0;
        }
    // no seconds hidden in useconds
    if (tv.tv_usec >= tv_usec_sec_)
        {
            tv.tv_sec += (tv.tv_usec / tv_usec_sec_);
            tv.tv_usec %= tv_usec_sec_;
        }
}

inline void timeval_fill(timeval & tv, time_t sec, suseconds_t usec)
{
    tv.tv_sec = sec;
    tv.tv_usec = usec;
    timeval_validate(tv);
}

inline void timespec_validate(timespec & ts)
{
    // no negative values
    while (ts.tv_nsec < 0 && ts.tv_sec > 0)
        {
            --(ts.tv_sec);
            ts.tv_nsec += tv_nsec_sec_;
        }
    if (ts.tv_sec < 0  || ts.tv_nsec < 0)
        {
            ts.tv_sec = 0;
            ts.tv_nsec = 0;
        }
    // no seconds hidden in nseconds
    if (ts.tv_nsec >= tv_nsec_sec_)
        {
            ts.tv_sec += (ts.tv_nsec / tv_nsec_sec_);
            ts.tv_nsec %= tv_nsec_sec_;
        }
}

inline void timespec_fill(timespec & ts, time_t sec, suseconds_t usec, long nsec)
{
    ts.tv_sec = sec;
    ts.tv_nsec = usec * tv_nsec_usec_ + nsec;
    timespec_validate(ts);
}

inline timeval timeval_to_abs_timeval(timeval const & tv)
{
    timeval abs;
    gettimeofday(&abs, 0);
    abs += tv;
    return abs;
}

inline timespec timeval_to_timespec(timeval const & tv)
{
    timespec ts;
    ts.tv_sec = tv.tv_sec;
    ts.tv_nsec = tv.tv_usec * tv_nsec_usec_;
    return ts;
}

inline timespec timeval_to_abs_timespec(timeval const & tv)
{
    timeval abs;
    gettimeofday(&abs, 0);
    abs += tv;
    timespec ts;
    ts.tv_sec = abs.tv_sec;
    ts.tv_nsec = abs.tv_usec * tv_nsec_usec_;
    return ts;
}

inline timeval operator + (timeval const & a, timeval const & b)
{
    timeval out;
    out.tv_sec = a.tv_sec + b.tv_sec;
    int usec = a.tv_usec + b.tv_usec; // unless there's a guarantee suseconds_t can go above 1E6?
    if (usec >= tv_usec_sec_)
        {
            out.tv_sec += (usec / tv_usec_sec_);
            usec %= tv_usec_sec_;
        }
    out.tv_usec = usec;
    return out;
}

inline timeval & operator += (timeval & a, timeval const & b)
{
    a.tv_sec += b.tv_sec;
    int usec = a.tv_usec + b.tv_usec;
    if (usec >= tv_usec_sec_)
        {
            a.tv_sec += (usec / tv_usec_sec_);
            usec %= tv_usec_sec_;
        }
    a.tv_usec = usec;
    return a;
}

inline timeval operator - (timeval const & a, timeval const & b)
{
    timeval out;
    if (a.tv_sec < b.tv_sec ||
        (a.tv_sec == b.tv_sec && a.tv_usec < b.tv_usec))
        {
            out.tv_sec = 0;
            out.tv_usec = 0;
        }
    else if (a.tv_usec >= b.tv_usec)
        {
            out.tv_sec = a.tv_sec - b.tv_sec;
            out.tv_usec = a.tv_usec - b.tv_usec;
        }
    else
        {
            out.tv_sec = (a.tv_sec - 1 - b.tv_sec);
            out.tv_usec = (a.tv_usec + tv_usec_sec_ - b.tv_usec);
        }
    return out;
}

inline timeval & operator -= (timeval & a, timeval const & b)
{
    if (a.tv_sec < b.tv_sec ||
        (a.tv_sec == b.tv_sec && a.tv_usec < b.tv_usec))
        {
            a.tv_sec = 0;
            a.tv_usec = 0;
        }
    else if (a.tv_usec >= b.tv_usec)
        {
            a.tv_sec -= b.tv_sec;
            a.tv_usec -= b.tv_usec;
        }
    else
        {
            a.tv_sec -= (b.tv_sec + 1);
            a.tv_usec -= (b.tv_usec - tv_usec_sec_);
        }
    return a;
}

inline int RunningTimeVal::getType() const
{
    return type_;
}

inline timeval RunningTimeVal::getTimeVal() const
{
    if (!type_)
        return tref_;
    else
        {
            timeval tval;
            gettimeofday(&tval, 0);
            if (type_ < 0)
                tval = tref_ - tval;
            else
                tval -= tref_;
            return tval;
        }
}

inline timespec RunningTimeVal::getTimeSpec() const
{
    return timeval_to_timespec(getTimeVal());
}

inline timeval RunningTimeVal::getRefTimeVal() const
{
    return tref_;
}

inline timespec RunningTimeVal::getRefTimeSpec() const
{
    return timeval_to_timespec(tref_);
}

inline void RunningTimeVal::reset()
{
    if (type_ != 0)
        gettimeofday(&tref_, 0);
    if (type_ < 0)
        tref_ += dt_;
}

inline void RunningTimeVal::pause()
{
    if (type_ > 0)
        gettimeofday(&dt_, 0);
}

inline void RunningTimeVal::resume()
{
    if (type_ > 0)
        {
            timeval tval;
            gettimeofday(&tval, 0);
            tref_ += (tval - dt_);
        }
}

inline void RunningTimeVal::setDT(timeval const & tval)
{
    if (type_ < 0)
        dt_ = tval;
}

inline void RunningTimeVal::setDT(time_t sec, suseconds_t usec)
{
    if (type_ < 0)
        timeval_fill(dt_, sec, usec);
}

} // namespace tools
} // namespace rpct

#endif // rpct_tools_RunningTimeVal_inl_h_
