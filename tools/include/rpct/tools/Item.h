/** Filip Thyssen */

#ifndef rpct_tools_Item_h_
#define rpct_tools_Item_h_

namespace rpct {
namespace tools {

class Item
{
public:
    virtual ~Item();

    virtual int getId() const = 0;
};

inline Item::~Item()
{}

} // namespace tools
} // namespace rpct

#endif // rpct_tools_Item_h_
