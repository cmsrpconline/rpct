/** Filip Thyssen */

#ifndef rpct_tools_Action_h_
#define rpct_tools_Action_h_

#include <string>

namespace rpct {
namespace tools {

class ActionSignature
{
public:
    virtual ~ActionSignature();

    virtual int invoke() const = 0;
    virtual int operator()() const;

    virtual std::string name() const;
};

template<typename Listener>
class Action : public ActionSignature
{
public:
    Action(Listener & listener
           , int (Listener::*func)()
           , const std::string & name = "undefinedAction");
    ~Action();
    
    int invoke() const;
    std::string name() const;

protected:
    Listener & listener_;
    int (Listener::* const func_)();
    const std::string name_;
};

template<typename T>
class ObjectActionSignature
{
public:
    virtual ~ObjectActionSignature();

    virtual int invoke(T obj) const = 0;
    virtual int operator()(T obj) const;

    virtual std::string name() const;
};

template<typename T, typename Listener>
class ObjectAction
    : public ObjectActionSignature<T>
{
public:
    ObjectAction(Listener & listener
                 , int (Listener::*func)(T)
                 , const std::string & name = "undefinedObjectAction");
    ~ObjectAction();

    int invoke(T obj) const;
    std::string name() const;

protected:
    Listener & listener_;
    int (Listener::* const func_)(T);
    const std::string name_;
};

} // namespace tools
} // namespace rpct

#include "rpct/tools/Action-inl.h"

#endif // rpct_tools_Action_h_
