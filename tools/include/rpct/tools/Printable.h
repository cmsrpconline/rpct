/** Filip Thyssen */

#ifndef rpct_tools_Printable_h_
#define rpct_tools_Printable_h_

#include <ostream>
#include <vector>
#include <string>

namespace rpct {
namespace tools {

class Parameters
{
protected:
    friend class Printable;

    typedef std::pair<std::string, std::string> parameter_type;
    typedef std::vector<parameter_type> parameters_type;
public:
    typedef parameters_type::iterator iterator;
    typedef parameters_type::const_iterator const_iterator;
    typedef parameters_type::size_type size_type;

    void clear();
    template<typename T>
    void add(const std::string & key, const T & value);
    template<typename T>
    void hexadd(const std::string & key, const T & value);
    void add(const std::string & key, const std::string & value);
    void add(const std::string & key, const bool & value);

    size_type size() const;
    const_iterator begin() const;
    const_iterator end() const;
    iterator begin();
    iterator end();

    void print(std::ostream & os) const;
    void printHTML(std::ostream & os) const;
protected:
    parameters_type parameters_;
    unsigned int keylength_;
};

class Printable
{
public:
    virtual ~Printable();

    virtual void print(std::ostream & os) const;
    virtual void printHTML(std::ostream & os) const;
    virtual void printParameters(Parameters & parameters) const;
};

} // namespace tools
} // namespace rpct

#include "rpct/tools/Printable-inl.h"

#endif // rpct_tools_Printable_h_
