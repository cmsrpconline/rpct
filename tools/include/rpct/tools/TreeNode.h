/** Filip Thyssen */

#ifndef rpct_tools_TreeNode_h_
#define rpct_tools_TreeNode_h_

#include <iterator>
#include <cstddef>

namespace rpct {
namespace tools {

template<typename T, bool tp_>
class TreeNode;
template<typename T, bool tp_>
class Tree;

template<typename T, bool tp_ = false>
class TreeNodeIterator : public std::iterator<std::bidirectional_iterator_tag, T, ptrdiff_t, T *, T &>
{
    friend class Tree<T, tp_>;
    friend class TreeNode<T, tp_>;

public:
    typedef TreeNodeIterator<T, tp_> iterator;
    typedef TreeNodeIterator<T, tp_> sibling_iterator;

    typedef std::reverse_iterator<iterator> reverse_iterator;
    typedef std::reverse_iterator<sibling_iterator> reverse_sibling_iterator;

public:
    TreeNodeIterator(int max_depth = -1);
    TreeNodeIterator(const TreeNodeIterator<T, tp_> & it);
    TreeNodeIterator(TreeNode<T, tp_> & tn, int max_depth = -1);

    TreeNodeIterator<T, tp_> & operator=(const TreeNodeIterator<T, tp_> & it);

    TreeNodeIterator<T, tp_> & operator++();
    TreeNodeIterator<T, tp_>   operator++(int);
    TreeNodeIterator<T, tp_> & operator--();
    TreeNodeIterator<T, tp_>   operator--(int);

    bool operator==(const TreeNodeIterator<T, tp_> & it) const;
    bool operator!=(const TreeNodeIterator<T, tp_> & it) const;

    T & operator*();
    T * operator->();

    T const & operator*() const;
    T const * operator->() const;

    int depth() const;
    int max_depth() const;
    bool skip_children() const;
    void set_max_depth(int max_depth = -1);
    void set_skip_children(bool skip = true);

    iterator begin();
    iterator end();
    reverse_iterator rbegin();
    reverse_iterator rend();

    sibling_iterator s_begin();
    sibling_iterator s_end();
    reverse_sibling_iterator s_rbegin();
    reverse_sibling_iterator s_rend();

protected:
    TreeNode<T, tp_> * tn_;
    int depth_; ///< <0 means end was reached
    int max_depth_; ///< <0 means unlimited depth, 0 means sibling_iterator
    bool skip_children_;
};

/** The TreeNode will contain T;
 *    if T is in fact a pointer to the object itself, (this-pointer), tp_ should be true for correct copying
 */
template<typename T, bool tp_ = false>
class TreeNode
{
    friend class Tree<T, tp_>;
    friend class TreeNodeIterator<T, tp_>;
public:
    typedef TreeNodeIterator<T, tp_> iterator;
    typedef TreeNodeIterator<T, tp_> sibling_iterator;

    typedef std::reverse_iterator<iterator> reverse_iterator;
    typedef std::reverse_iterator<sibling_iterator> reverse_sibling_iterator;

public:
    TreeNode(T const & item
             , int position = 0
             , TreeNode<T, tp_> * parent = 0
             , TreeNode<T, tp_> * siblings = 0);
    /** there's a const_cast in this function to add the copy as a sibling and take the children*/
    TreeNode(TreeNode<T, tp_> const & t);

    /** there's a const_cast in this function to add the copy as a sibling and take the children*/
    TreeNode<T, tp_> & operator=(TreeNode<T, tp_> const & t);

    virtual ~TreeNode();

    virtual void removeFromTree();
    virtual void removeTreeChildren();

    bool operator==(const TreeNode<T, tp_> & tn) const;
    bool operator!=(const TreeNode<T, tp_> & tn) const;
    bool operator==(const T & t) const;
    bool operator!=(const T & t) const;

    T & getTreeItem();
    T const & getTreeItem() const;

    int getTreePosition() const;

    TreeNode<T, tp_> * getTreeParent() const;
    TreeNode<T, tp_> * getTreeSibling() const;
    TreeNode<T, tp_> * getTreeChild() const;

    virtual void addTreeSibling (TreeNode<T, tp_> & tree);
    void addTreeSiblings(TreeNode<T, tp_> & tree); // for ease of inheritance this uses addTreeSibling
    virtual void addTreeChild   (TreeNode<T, tp_> & tree);
    void addTreeChildren(TreeNode<T, tp_> & tree); // same remark as addTreeSiblings

    iterator tree_begin();
    iterator tree_end();
    reverse_iterator tree_rbegin();
    reverse_iterator tree_rend();

    sibling_iterator tree_s_begin();
    sibling_iterator tree_s_end();
    reverse_sibling_iterator tree_s_rbegin();
    reverse_sibling_iterator tree_s_rend();

protected:
    int ti_position_;
    TreeNode<T, tp_> * ti_parent_, * ti_rsibling_, * ti_lchild_;
    TreeNode<T, tp_> * ti_lsibling_, * ti_rchild_;
    T ti_item_;
};

template<typename T, bool tp_ = false>
class Tree
{
public:
    typedef TreeNodeIterator<T, tp_> iterator;
    typedef TreeNodeIterator<T, tp_> sibling_iterator;

    typedef std::reverse_iterator<iterator> reverse_iterator;
    typedef std::reverse_iterator<sibling_iterator> reverse_sibling_iterator;

protected:
    void erase(TreeNode<T, tp_> * tn);
    void rerase(TreeNode<T, tp_> * tn);

public:
    Tree();
    ~Tree();

    void clear();
    bool empty() const;

    iterator add(const T & t, int position = 0);

    iterator addChild(iterator const & it, const T & t, int position = 0);
    iterator addSibling(iterator const & it, const T & t, int position = 0);

    void erase(iterator const & it);

    iterator find(const T & t);

    iterator begin();
    iterator end();
    reverse_iterator rbegin();
    reverse_iterator rend();

    sibling_iterator s_begin();
    sibling_iterator s_end();
    reverse_sibling_iterator s_rbegin();
    reverse_sibling_iterator s_rend();

protected:
    TreeNode<T, tp_> * child_;
};

} // namespace tools
} // namespace rpct

#include "rpct/tools/TreeNode-inl.h"

#endif // rpct_tools_TreeNode_h_
