/** Filip Thyssen */

#ifndef rpct_tools_Flags_h_
#define rpct_tools_Flags_h_

#include <stdint.h> // uint32_t
#include <string>
#include <ostream>

namespace rpct {
namespace tools {

/** a set of 2 byte flags, 0=unset, 1=false, 2=true */
template<int nflags_, uint32_t preset_ = 0x0>
class Flags
{
protected:
    typedef Flags<nflags_, preset_> flags_type;
public:
    Flags(uint32_t flags = preset_);
    virtual ~Flags();

    operator uint32_t() const;
    uint32_t getFlags() const;

    void setFlags(uint32_t flags);

    void reset();

    bool matches(flags_type const & in) const;
    bool matches(flags_type const & in, uint32_t mask) const;

    bool operator <(flags_type const & in) const;
    bool operator ==(flags_type const & in) const;

    virtual std::string getName() const;
protected:
    void mask();
    bool has(int flag) const;
    bool is(int flag, bool _set = true) const;
    void set(int flag, bool _set = true);
    void unset(int flag);

    // to be deleted by user
    unsigned char * split() const;

    uint32_t flags_;
    uint32_t mask_;
};

template<int nflags_, uint32_t preset_>
std::ostream & operator<< (std::ostream & os, Flags<nflags_, preset_> const & in);

} // namespace tools
} // namespace rpct

#include "rpct/tools/Flags-inl.h"

#endif // rpct_tools_Flags_h_
