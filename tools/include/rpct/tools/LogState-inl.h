/** Filip Thyssen */

#ifndef rpct_tools_LogState_inl_h_
#define rpct_tools_LogState_inl_h_

#include "rpct/tools/LogState.h"

namespace rpct {
namespace tools {

inline LogCenter & LogState::getLogCenter()
{
    if (!logcenter_)
        logcenter_ = new LogCenter();
    return *logcenter_;
}

inline int LogState::getId() const
{
    return 0;
}

inline void LogState::reset()
{
    log_.clear();
    State::reset();
}

inline void LogState::log(const std::string & name)
{
    logcenter_->log(*this, name);
}

inline void LogState::unlog(const std::string & name)
{
    logcenter_->unlog(*this, name);
}

inline void LogState::log(int id)
{
    logcenter_->log(*this, id);
}

inline void LogState::unlog(int id)
{
    logcenter_->unlog(*this, id);
}

inline LogState::log_type const & LogState::getLog() const
{
    return log_;
}

inline void LogState::printLog(std::ostream & os) const
{
    logcenter_->printLog(*this, os);
}

inline void LogState::printLogHTML(std::ostream & os) const
{
    logcenter_->printLogHTML(*this, os);
}

template<typename T>
inline TTreeLogState<T>::TTreeLogState(int position
                                       , TreeNode<T, true> * parent
                                       , TreeNode<T, true> * sibling)
    : TTreeState<T>(position, parent, sibling)
{}

} // namespace tools
} // namespace rpct

#endif // rpct_tools_LogState_inl_h_
