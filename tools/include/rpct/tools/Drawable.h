/** Filip Thyssen */

#ifndef rpct_tools_Drawable_h_
#define rpct_tools_Drawable_h_

#include <ostream>

namespace rpct {
namespace tools {

class Drawable
{
public:
    virtual ~Drawable();

    void printSVGDocument(std::ostream & os, int depth = 0) const;

    void printSVGEmbed(std::ostream & os, const std::string & location, int depth = 0) const;

    virtual void printSVGHeader(std::ostream & os, int depth = 0) const;
    virtual void printSVGCSS(std::ostream & os, int depth = 0) const;
    virtual void printSVG(std::ostream & os, int depth = 0) const;
    virtual void printSVGFooter(std::ostream & os, int depth = 0) const;

    virtual int getSVGWidth(int depth = 0) const = 0;
    virtual int getSVGHeight(int depth = 0) const = 0;
};


} // namespace tools
} // namespace rpct

#include "rpct/tools/Drawable-inl.h"

#endif // rpct_tools_Drawable_h_
