/** Filip Thyssen */

#ifndef rpct_tools_LogCenter_h_
#define rpct_tools_LogCenter_h_

#include "time.h"

#include <map>
#include <string>

#include "log4cplus/logger.h"

#include "rpct/tools/RWMutex.h"
#include "rpct/tools/State.h"

namespace rpct {
namespace tools {

class LogState;
class Publisher;

class LogCategory
{
public:
    LogCategory(int cat_id
                , const std::string & name = std::string("unknown")
                , const std::string & description = std::string("unknown"));

    int getId() const;
    std::string const & getName() const;
    std::string const & getDescription() const;

protected:
    int id_;
    std::string name_;
    std::string description_;
};
class LogMessage
{
public:
    LogMessage(int id, int cat_id = -1, unsigned char level = State::unknown_
               , const std::string & name = std::string("unknown")
               , const std::string & description = std::string("unknown"));

    int getId() const;
    int getCategoryId() const;
    unsigned char getLevel() const;
    std::string const & getName() const;
    std::string const & getDescription() const;

    void print(std::ostream & os, time_t tv = 0) const;
    void printHTML(std::ostream & os, time_t tv = 0) const;

protected:
    int id_;
    int cat_id_;
    unsigned char level_;
    std::string name_;
    std::string description_;
};

class LogEntry
{
public:
    LogEntry(int id, int cat_id, time_t tv = 0);

    int getId() const;
    int getCategoryId() const;
    time_t getTime() const;

    void setTime(time_t tv = 0) const;

    bool operator<(LogEntry const & le) const;
protected:
    int id_, cat_id_;
    mutable time_t tv_;
};

class LogCenter
{
protected:
    friend class LogState;

    typedef std::map<int, LogCategory * > log_categories_type;
    typedef std::map<int, LogMessage * > log_messages_type;
    typedef std::map<std::string, LogCategory * > log_category_names_type;
    typedef std::map<std::string, LogMessage * > log_message_names_type;
    typedef std::map<int, Publisher * > log_publishers_type;

public:
    LogCenter();
    ~LogCenter();

    void clear();

    void loadCategories(std::string const & filename);
    void loadMessages(std::string const & filename);

    void addCategory(int cat_id
                     , const std::string & name = std::string("unknown")
                     , const std::string & description = std::string("unknown"));
    void addMessage(int id, int cat_id, unsigned char level
                    , const std::string & name = std::string("unknown")
                    , const std::string & description = std::string("unknown"));

    LogCategory const & getCategory(const std::string & name);
    LogCategory const & getCategory(int cat_id);

    LogMessage const & getMessage(const std::string & name);
    LogMessage const & getMessage(int id);

    void log(LogState & logstate, const std::string & name);
    void unlog(LogState & logstate, const std::string & name);

    void log(LogState & logstate, int id);
    void unlog(LogState & logstate, int id);

    void setPublisherLogType(int type);
    void setPublisherUnLogType(int type);

    void setPublisher(Publisher & publisher);
    void unsetPublisher();
    // set a publisher for a certain category
    void setPublisher(int cat_id, Publisher & publisher);
    void unsetPublisher(int cat_id);
    void setPublisher(const std::string & name, Publisher & publisher);
    void unsetPublisher(const std::string & name);

    void printLog(LogState const & logstate, std::ostream & os);
    void printLogHTML(LogState const & logstate, std::ostream & os);

protected:
    static log4cplus::Logger log4cplus_logger_;

    // i* functions don't take the mutex
    LogCategory const & iaddCategory(int cat_id
                                     , const std::string & name = std::string("unknown")
                                     , const std::string & description = std::string("unknown"));
    LogMessage const & iaddMessage(int id, int cat_id = -1, unsigned char level = State::unknown_
                                   , const std::string & name = std::string("unknown")
                                   , const std::string & description = std::string("unknown"));

    void iremoveCategory(int cat_id);
    void iremoveCategory(const std::string & name);

    void iremoveMessage(int id);
    void iremoveMessage(const std::string & name);

    LogCategory const & igetCategory(RWLock & rwlock
                                     , const std::string & name);
    LogCategory const & igetCategory(RWLock & rwlock
                                     , int cat_id);

    LogMessage const & igetMessage(RWLock & rwlock
                                   , const std::string & name);
    LogMessage const & igetMessage(RWLock & rwlock
                                   , int id);

    log_categories_type log_categories_;
    log_messages_type log_messages_;
    log_category_names_type log_category_names_;
    log_message_names_type log_message_names_;

    int logtype_, unlogtype_;

    log_publishers_type log_publishers_;
    Publisher * publisher_;

    int id_counter_;
    int cat_id_counter_;

    RWMutex mutex_;
};

} // namespace tools
} // namespace rpct

#include "rpct/tools/LogCenter-inl.h"

#endif // rpct_tools_LogCenter_h_
