/** Filip Thyssen */

#ifndef rpct_tools_State_inl_h_
#define rpct_tools_State_inl_h_

#include "rpct/tools/State.h"

namespace rpct {
namespace tools {

inline std::string State::getStateName(unsigned char level)
{
    if (level < max_)
        return name_[level];
    else
        return name_[unknown_];
}

inline std::string State::getStateShort(unsigned char level)
{
    if (level < max_)
        return short_[level];
    else
        return short_[unknown_];
}

inline std::string State::getStateColor(unsigned char level)
{
    if (level < max_)
        return color_[level];
    else
        return color_[unknown_];
}

inline std::string State::getStateBgColor(unsigned char level)
{
    if (level < max_)
        return bgcolor_[level];
    else
        return bgcolor_[unknown_];
}

inline std::string State::getStateImage(unsigned char level)
{
    if (level < max_)
        return image_[level];
    else
        return image_[unknown_];
}

inline log4cplus::LogLevel State::getStateLogLevel(unsigned char _level)
{
    if (_level < max_)
        return loglevel_[_level];
    else
        return loglevel_[unknown_];
}

inline unsigned char State::getState() const
{
    return state_;
}

inline void State::setState(unsigned char state)
{
    state_ = state;
}

inline void State::reset()
{
    setState(notset_);
}

inline void State::ok()
{
    setState(ok_);
}

inline bool State::isOk() const
{
    return (state_ == ok_);
}

inline void State::notset()
{
    setState(notset_);
}

inline bool State::isNotSet() const
{
    return (state_ == notset_);
}

inline void State::info()
{
    setState(info_);
}

inline bool State::isInfo() const
{
    return (state_ == info_);
}

inline void State::warn()
{
    setState(warn_);
}

inline bool State::isWarn() const
{
    return (state_ == warn_);
}

inline void State::error()
{
    setState(error_);
}

inline bool State::isError() const
{
    return (state_ == error_);
}

template<typename T>
inline void TTreeState<T>::setPStateUp(unsigned char state)
{
    if (state > pstate_)
        {
            pstate_ = state;
            if (ti_parent_)
                ti_parent_->getTreeItem()->setPStateUp(state);
        }
}

template<typename T>
inline void TTreeState<T>::setPStateDown()
{
    unsigned char old_pstate = pstate_;

    pstate_ = state_;
    if (ti_lchild_)
        for (typename TreeNode<T, true>::sibling_iterator child = ti_lchild_->tree_s_begin()
                 ; child.depth() == 0
                 ; ++child)
            if ((*child)->pstate_ > pstate_)
                pstate_ = (*child)->pstate_;

    if (old_pstate > pstate_ && ti_parent_ && ti_parent_->getTreeItem()->pstate_ == old_pstate)
        ti_parent_->getTreeItem()->setPStateDown();
}

template<typename T>
inline void TTreeState<T>::setState(unsigned char state)
{
    unsigned char old_state = state_;
    state_ = state;
    if (state > old_state)
        setPStateUp(state);
    else if (state < old_state && pstate_ == old_state)
        setPStateDown();
}

template<typename T>
inline unsigned char TTreeState<T>::getPState() const
{
    return pstate_;
}

template<typename T>
inline void TTreeState<T>::removeFromTree()
{
    TreeNode<T, true> * parent = ti_parent_;
    TreeNode<T, true>::removeFromTree();
    if (parent)
        parent->getTreeItem()->setPStateDown();
}

template<typename T>
inline void TTreeState<T>::removeTreeChildren()
{
    TreeNode<T, true>::removeTreeChildren();
    setPStateDown();
}

template<typename T>
inline void TTreeState<T>::addTreeSibling(TreeNode<T, true> & tree)
{
    TreeNode<T, true>::addTreeSibling(tree);
    if (ti_parent_ && tree.getTreeItem()->pstate_ > ti_parent_->getTreeItem()->pstate_)
        ti_parent_->getTreeItem()->setPStateUp(tree.getTreeItem()->pstate_);
}

template<typename T>
inline void TTreeState<T>::addTreeChild(TreeNode<T, true> & tree)
{
    TreeNode<T, true>::addTreeChild(tree);
    if (tree.getTreeItem()->pstate_ > pstate_)
        setPStateUp(tree.getTreeItem()->pstate_);
}

template<typename T>
inline TTreeState<T>::TTreeState(int position
                                 , TreeNode<T, true> * parent
                                 , TreeNode<T, true> * siblings)
    : TreeNode<T, true>(static_cast<T>(this), position, 0, 0)
    , pstate_(State::default_)
{
    if (parent)
        parent->addTreeChild(*this);
    else if (siblings)
        siblings->addTreeSibling(*this);
}

} // namespace tools
} // namespace rpct

#endif // rpct_tools_State_inl_h_
