/** Filip Thyssen */

#ifndef rpct_tools_WebLogger_inl_h_
#define rpct_tools_WebLogger_inl_h_

#include "rpct/tools/WebLogger.h"

namespace rpct {
namespace tools {

inline void WebLogger::close()
{}

} // namespace tools
} // namespace rpct

#endif // rpct_tools_WebLogger_inl_h_
