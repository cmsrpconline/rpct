/** Filip Thyssen */

#ifndef rpct_tools_Condition_h_
#define rpct_tools_Condition_h_

#include <sys/time.h> // timeval
#include <time.h>     // timespec

#include <pthread.h>

#include "rpct/tools/Mutex.h"

namespace rpct {
namespace tools {

/** A condition - the mutex should be locked and unlocked by the user unless bool take = true */
class Condition : public Mutex
{
protected:
    static void cleanup_substract(void * arg);
    static void cleanup_unlock(void * arg);
public:
    Condition(size_t value = 1);
    ~Condition();

    void wait(bool take = true);
    int  wait(timeval const & timeout, bool take = true);
    int  wait(time_t sec, suseconds_t usec = 0, bool take = true);
    int  wait(timespec const & endtime, bool take = true);

    void free_one(bool take = true);
    void free_all(bool take = true);

    size_t nwaiting () const;
    size_t nreleases() const;
protected:
    pthread_cond_t condition_;

    volatile size_t nwaiting_;
    volatile size_t nreleases_;
};

} // namespace tools
} // namespace rpct

#include "rpct/tools/Condition-inl.h"

#endif // rpct_tools_Condition_h_
