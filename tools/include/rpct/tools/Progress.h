/* Filip Thyssen */

#ifndef rpct_tools_Progress_h
#define rpct_tools_Progress_h

#include <cstddef>
#include <string>

#include "log4cplus/logger.h"

#include "rpct/tools/RWMutex.h"
#include "rpct/tools/Condition.h"

#include "rpct/tools/Chrono.h"
#include "rpct/tools/Time.h"

namespace rpct {
namespace tools {

class ActionSignature;

class Progress
{
public:
    typedef unsigned char State;

    static State const idle_     = 0x00;
    static State const active_   = 0x01;
    static State const paused_   = 0x02;

    static unsigned int const control_shift_ = 0;
    static unsigned int const process_shift_ = 2;

    static State const control_active_ = active_ << control_shift_;
    static State const control_paused_ = paused_ << control_shift_;
    static State const process_active_ = active_ << process_shift_;
    static State const process_paused_ = paused_ << process_shift_;

    static State const control_mask_ = control_active_ | control_paused_;
    static State const process_mask_ = process_active_ | process_paused_;

public:
    Progress(log4cplus::Logger & _logger
             , std::string const & _title = std::string("")
             , rpct::tools::ActionSignature * _pause_action = 0
             , rpct::tools::ActionSignature * _resume_action = 0);

    /**
     * \name User Functions
     * Functions for the user
     * @{
     */
    bool pause(); ///< Pause execution, return if valid
    bool resume(); ///< Resume execution, return if valid
    bool terminate(); ///< Terminate execution, return if valid

    std::string getTitle() const;
    State getState() const;

    bool isControlActive() const;
    bool isControlPaused() const;
    bool isProcessActive() const;
    bool isProcessPaused() const;

    std::string getStateName() const;
    std::string const & getDescription() const;

    Time getStart() const;
    Time getDuration() const;
    Time getEndEstimate() const;
    std::size_t getValue() const;
    std::size_t getFailed() const;
    std::size_t getMaximum() const;
    float getProgress() const;
    /** @} */

    /**
     * \name Executing Thread Functions
     * Functions for the executing thread
     * @{
     */
    bool start();
    bool checkPaused();
    bool checkTerminated();
    bool stop();
    bool fail(); // lazy stop + setFailed
    void init();

    void setValue(std::size_t _value);
    void setFailed(std::size_t _failed);
    void setMaximum(std::size_t _maximum);

    void setTitle(std::string const & _title);
    void setDescription(std::string const & _description);

    void addValue(std::size_t _value = 1);
    void addFailed(std::size_t _failed = 1);
    void addDescription(std::string const & _description);
    /** @} */


protected:
    log4cplus::Logger & logger_;

    State state_;
    std::string title_, description_;

    mutable Condition condition_;
    rpct::tools::ActionSignature * pause_action_, * resume_action_;

    std::size_t value_, failed_, maximum_;
    Chrono chrono_;
    Time time_, duration_;

};

} // namespace tools
} // namespace rpct

#include "rpct/tools/Progress-inl.h"

#endif // rpct_tools_Progress_h
