/** Filip Thyssen */

#ifndef rpct_tools_RunningTimeVal_h_
#define rpct_tools_RunningTimeVal_h_

#include "sys/time.h"
#include "time.h"
#include <string>
#include <ostream>

namespace rpct {
namespace tools {

/** the number of microseconds in a second */
const int tv_usec_sec_ = 1000000;
/** the number of nanoseconds in a second */
const int tv_nsec_sec_ = 1000000000;
/** the number of nanoseconds in a microsecond */
const int tv_nsec_usec_ = 1000;

void timeval_validate(timeval & tv);
void timeval_fill(timeval & tv, time_t sec = 0, suseconds_t usec = 0);

void timespec_validate(timespec & tc);
void timespec_fill(timespec & ts, time_t sec = 0, suseconds_t usec = 0, long nsec = 0);

timeval  timeval_to_abs_timeval (timeval const & tv);
timespec timeval_to_timespec    (timeval const & tv);
timespec timeval_to_abs_timespec(timeval const & tv);

timeval   operator +  (timeval const & a, timeval const & b);
timeval & operator += (timeval & a, timeval const & b);
timeval   operator -  (timeval const & a, timeval const & b);
timeval & operator -= (timeval & a, timeval const & b);

class RunningTimeVal
{
public:
    static const int chrono_ = 1;
    static const int timeval_ = 0;
    static const int countdown_ = -1;
    static const int default_ = 1;
public:
    /** RunningTimeVal
     * \param type  1: chrono, default
     *              0: timeval
     *             -1: countdown
     * \param tval 0:         chrono   : start time is now
     *                        timeval  : value is now
     *                        countdown: useless, timeval with value 0
     *             otherwise: chrono   : start time is tval
     *                        timeval  : value is tval
     *                        countdown: time to go is tval
     */
    RunningTimeVal(int type = default_, timeval const * tval = 0);
    /** RunningTimeVal
     * \param type cf previous constructor
     * \param sec  the seconds of timeval tval in the previous constructor
     * \param usec the microseconds of timeval tval in the previous constructor
     */
    RunningTimeVal(int type, time_t sec, suseconds_t usec = 0);
    /** RunningTimeVal of type timeval
     * \param tval the value
     */
    RunningTimeVal(timeval const & tval);

    /** returns the type of the RunningTimeVal */
    int getType() const;

    /** get the current timeval depending on the type */
    timeval getTimeVal() const;
    /** get the current timespec depending on the type */
    timespec getTimeSpec() const;

    /** get the reference timeval depending on the type */
    timeval getRefTimeVal() const;
    /** get the reference timespec depending on the type */
    timespec getRefTimeSpec() const;

    /** for chrono and countdown: start over */
    void reset();

    /** for chrono */
    void pause();
    /** for chrono */
    void resume();

    /** for countdown: set time to go after next reset */
    void setDT(timeval const & tval);
    /** for countdown: set time to go after next reset
     * \param sec  seconds
     * \param usec microseconds
     */
    void setDT(time_t sec, suseconds_t usec = 0);

    /** sleep until the countdown ends */
    void sleep() const;

    /** returns a string in the ctime date format, without \n */
    std::string getDate() const;

protected:
    void init(timeval const * tval = 0);

    /** the type_ can have values < 0, == 0, > 0, which correspond to countdown, timeval and chrono */
    int type_;
    timeval tref_, dt_;
};
std::ostream & operator << (std::ostream & outstream, const RunningTimeVal & rtval);

} // namespace tools
} // namespace rpct

#include "rpct/tools/RunningTimeVal-inl.h"

#endif // rpct_tools_RunningTimeVal_h_
