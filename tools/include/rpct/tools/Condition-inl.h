/** Filip Thyssen */

#ifndef rpct_tools_Condition_inl_h_
#define rpct_tools_Condition_inl_h_

#include "rpct/tools/Condition.h"

#include "rpct/tools/RunningTimeVal.h"

namespace rpct {
namespace tools {

inline int Condition::wait(timeval const & timeout, bool take)
{
    timespec endtime = timeval_to_abs_timespec(timeout);
    return wait(endtime, take);
}

inline int Condition::wait(time_t sec, suseconds_t usec, bool take)
{
    timeval tval;
    timeval_fill(tval, sec, usec);
    timespec endtime = timeval_to_abs_timespec(tval);
    return wait(endtime, take);
}

inline size_t Condition::nwaiting() const
{
    return nwaiting_;
}

inline size_t Condition::nreleases() const
{
    return nreleases_;
}

} // namespace tools
} // namespace rpct

#endif // rpct_tools_Condition_inl_h_
