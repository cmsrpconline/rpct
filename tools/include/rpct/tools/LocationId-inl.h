/** Filip Thyssen */

#ifndef rpct_tools_LocationId_inl_h_
#define rpct_tools_LocationId_inl_h_

#include "rpct/tools/LocationId.h"

#include <sstream>

namespace rpct {
namespace tools {

template<int nfields_, const int * pos_, const int * masks_, const int * offset_>
inline LocationId<nfields_, pos_, masks_, offset_>::LocationId(uint32_t id)
    : id_(id)
{
    mask();
}
template<int nfields_, const int * pos_, const int * masks_, const int * offset_>
inline LocationId<nfields_, pos_, masks_, offset_>::~LocationId()
{}
template<int nfields_, const int * pos_, const int * masks_, const int * offset_>
inline LocationId<nfields_, pos_, masks_, offset_>::operator uint32_t() const
{
    return id_;
}
template<int nfields_, const int * pos_, const int * masks_, const int * offset_>
inline uint32_t LocationId<nfields_, pos_, masks_, offset_>::getId() const
{
    return id_;
}

template<int nfields_, const int * pos_, const int * masks_, const int * offset_>
inline void LocationId<nfields_, pos_, masks_, offset_>::setId(uint32_t id)
{
    id_ = id;
    mask();
}
template<int nfields_, const int * pos_, const int * masks_, const int * offset_>
inline void LocationId<nfields_, pos_, masks_, offset_>::reset()
{
    id_   = 0x0;
    mask_ = 0x0;
}

template<int nfields_, const int * pos_, const int * masks_, const int * offset_>
inline bool LocationId<nfields_, pos_, masks_, offset_>::matches(lid_type const & in) const
{
    return ((id_ & in.mask_) == (in & mask_));
}

template<int nfields_, const int * pos_, const int * masks_, const int * offset_>
inline bool LocationId<nfields_, pos_, masks_, offset_>::operator <(lid_type const & in) const
{
    return (id_ < in.id_);
}
template<int nfields_, const int * pos_, const int * masks_, const int * offset_>
inline bool LocationId<nfields_, pos_, masks_, offset_>::operator ==(lid_type const & in) const
{
    return (id_ == in.id_);
}

template<int nfields_, const int * pos_, const int * masks_, const int * offset_>
inline std::string LocationId<nfields_, pos_, masks_, offset_>::getName() const
{
    std::stringstream out;
    uint32_t * parts = split();
    out << id_;
    if (parts)
        {
            out << ":";
            for (int field = 0 ; field < nfields_ ; ++field)
                {
                    if (parts[field])
                        out << ":" << parts[field] - offset_[field];
                    else
                        out << ":*";
                }
            delete[] parts;
        }
    return out.str();
}
template<int nfields_, const int * pos_, const int * masks_, const int * offset_>
inline void LocationId<nfields_, pos_, masks_, offset_>::mask()
{
    mask_ = 0x0;
    for (int field = 0 ; field < nfields_ ; ++field)
        if (id_ & masks_[field])
            mask_ |= masks_[field];
}
template<int nfields_, const int * pos_, const int * masks_, const int * offset_>
inline int LocationId<nfields_, pos_, masks_, offset_>::get(int field) const
{
    if (id_ & masks_[field])
        return ((id_ & masks_[field]) >> pos_[field]) - offset_[field];
    else
        return locationid::wildcard_;
}
template<int nfields_, const int * pos_, const int * masks_, const int * offset_>
inline void LocationId<nfields_, pos_, masks_, offset_>::set(int field, int value)
{
    id_ &= ~(masks_[field]);
    if (value != locationid::wildcard_)
        {
            id_   |= (((uint32_t)(value + offset_[field])) << pos_[field]) & masks_[field];
            mask_ |= masks_[field];
        }
    else
        mask_ &= ~(masks_[field]);
}

template<int nfields_, const int * pos_, const int * masks_, const int * offset_>
inline uint32_t * LocationId<nfields_, pos_, masks_, offset_>::split() const
{
    uint32_t * parts = new (std::nothrow) uint32_t[nfields_];
    if (parts)
        for (int field = 0 ; field < nfields_ ; ++field)
            parts[field] = ((id_ & masks_[field])>>pos_[field]);
    return parts;
}

template<int nfields_, const int * pos_, const int * masks_, const int * offset_>
inline std::ostream & operator<< (std::ostream & os, LocationId<nfields_, pos_, masks_, offset_> const & in)
{
    os << in.getName();
    return os;
}

template<typename T>
inline LocationIdSelection<T>::LocationIdSelection()
{}
template<typename T>
inline LocationIdSelection<T>::~LocationIdSelection()
{}

template<typename T>
inline typename LocationIdSelection<T>::const_iterator LocationIdSelection<T>::select_begin() const
{
    return select_.begin();
}
template<typename T>
inline typename LocationIdSelection<T>::const_iterator LocationIdSelection<T>::select_end() const
{
    return select_.end();
}
template<typename T>
inline typename LocationIdSelection<T>::const_iterator LocationIdSelection<T>::mask_begin() const
{
    return mask_.begin();
}
template<typename T>
inline typename LocationIdSelection<T>::const_iterator LocationIdSelection<T>::mask_end() const
{
    return mask_.end();
}

template<typename T>
inline void LocationIdSelection<T>::select(T const & id)
{
    select_.insert(id);
}
template<typename T>
inline void LocationIdSelection<T>::deselect(T const & id)
{
    select_.erase(id);
}
template<typename T>
inline void LocationIdSelection<T>::mask(T const & id)
{
    mask_.insert(id);
}
template<typename T>
inline void LocationIdSelection<T>::unmask(T const & id)
{
    mask_.erase(id);
}

template<typename T>
inline void LocationIdSelection<T>::add(T const & id)
{
    mask_.erase(id);
    select_.insert(id);
}
template<typename T>
inline void LocationIdSelection<T>::remove(T const & id)
{
    select.erase(id);
    mask_.insert(id);
}

template<typename T>
inline bool LocationIdSelection<T>::matches(T const & id) const
{
    const_iterator IidEnd = select_.end();
    const_iterator Iid = select_.begin();
    while(Iid != IidEnd && !(id.matches(*Iid)))
        ++Iid;
    if (Iid != IidEnd)
        {
            IidEnd = mask_.end();
            Iid = mask_.begin();
            while (Iid != IidEnd && !(id.matches(*Iid)))
                ++Iid;
            return (Iid == IidEnd);
        }
    else
        return 0;
}

} // namespace tools
} // namespace rpct

#endif // rpct_tools_LocationId_inl_h_
