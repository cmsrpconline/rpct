#ifndef rpct_tools_Publisher_inl_h_
#define rpct_tools_Publisher_inl_h_

#include "rpct/tools/Publisher.h"

namespace rpct {
namespace tools {

inline bool Publisher::synced() const
{
    return false;
}

} // namespace tools
} // namespace rpct

#endif // rpct_tools_Publisher_inl_h_
