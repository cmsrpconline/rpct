/** Filip Thyssen */

#ifndef rpct_tools_WebLogger_h_
#define rpct_tools_WebLogger_h_

#include <ostream>
#include <string>
#include <map>
#include <deque>

#include "time.h"

#include "log4cplus/logger.h"
#include "log4cplus/appender.h"

#include "rpct/tools/State.h"
#include "rpct/tools/RWMutex.h"

namespace rpct {
namespace tools {

class LogEvent
{
public:
    LogEvent(std::string const & message, unsigned char level = State::unknown_, time_t tv = 0);

    void print(std::ostream & os) const;
    void printHTML(std::ostream & os) const;
protected:
    std::string message_;
    unsigned char level_;
    time_t tv_;
};

class WebLogger : public virtual log4cplus::Appender
{
protected:
    void append(log4cplus::spi::InternalLoggingEvent const & event);

public:
    WebLogger(size_t max_events = 50);

    void close();

    void print(std::ostream & os) const;
    void printHTML(std::ostream & os) const;
protected:
    static std::map<log4cplus::LogLevel, unsigned char> * levels_;

    size_t max_events_;
    std::deque<LogEvent> events_;
    mutable RWMutex events_mutex_;
};

} // namespace tools
} // namespace rpct

#include "rpct/tools/WebLogger-inl.h"

#endif // rpct_tools_WebLogger_h_
