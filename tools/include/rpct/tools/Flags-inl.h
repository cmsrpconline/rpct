/** Filip Thyssen */

#ifndef rpct_tools_Flags_inl_h_
#define rpct_tools_Flags_inl_h_

#include "rpct/tools/Flags.h"

#include <sstream>

namespace rpct {
namespace tools {

template<int nflags_, uint32_t preset_>
inline Flags<nflags_, preset_>::Flags(uint32_t flags)
    : flags_(flags)
{
    mask();
}
template<int nflags_, uint32_t preset_>
inline Flags<nflags_, preset_>::~Flags()
{}

template<int nflags_, uint32_t preset_>
inline Flags<nflags_, preset_>::operator uint32_t() const
{
    return flags_;
}
template<int nflags_, uint32_t preset_>
inline uint32_t Flags<nflags_, preset_>::getFlags() const
{
    return flags_;
}
template<int nflags_, uint32_t preset_>
inline void Flags<nflags_, preset_>::setFlags(uint32_t flags)
{
    flags_ = flags;
    mask();
}
template<int nflags_, uint32_t preset_>
inline void Flags<nflags_, preset_>::reset()
{
    flags_ = preset_;
    mask();
}

template<int nflags_, uint32_t preset_>
inline bool Flags<nflags_, preset_>::matches(flags_type const & in) const
{
    return ((flags_ & in.mask_) == (in & mask_));
}

template<int nflags_, uint32_t preset_>
inline bool Flags<nflags_, preset_>::operator <(flags_type const & in) const
{
    return flags_ < in.getFlags();
}
template<int nflags_, uint32_t preset_>
inline bool Flags<nflags_, preset_>::operator ==(flags_type const & in) const
{
    return flags_ == in.getFlags();
}

template<int nflags_, uint32_t preset_>
inline std::string Flags<nflags_, preset_>::getName() const
{
    std::stringstream out;
    unsigned char * parts = split();
    if (parts)
        {
            for (int flag = 0 ; flag < nflags_ ; ++flag)
                {
                    if (parts[flag])
                        out << ":" << parts[flag] - 1;
                    else
                        out << ":*";
                }
            out << ":";
            delete[] parts;
        }
    else
        out << flags_;
    return out.str();

}
template<int nflags_, uint32_t preset_>
inline void Flags<nflags_, preset_>::mask()
{
    mask_ = 0x0;
    for (int flag = 0 ; flag < nflags_ ; ++flag)
        if (flags_ & (0x3 << (flag << 1)))
            mask_ |= (0x3 << (flag << 1));
}
template<int nflags_, uint32_t preset_>
inline bool Flags<nflags_, preset_>::has(int flag) const
{
    return (flags_ & (0x3 << (flag << 1)));
}
template<int nflags_, uint32_t preset_>
inline bool Flags<nflags_, preset_>::is(int flag, bool _set) const
{
    return (flags_ & ((_set ? 0x2:0x1) << (flag << 1)));
}
template<int nflags_, uint32_t preset_>
inline void Flags<nflags_, preset_>::set(int flag, bool _set)
{
    flags_ &= ~(0x3 << (flag << 1));
    flags_ |= ((_set ? 0x2:0x1) << (flag << 1));
    mask_  |= (0x3 << (flag << 1));
}
template<int nflags_, uint32_t preset_>
inline void Flags<nflags_, preset_>::unset(int flag)
{
    flags_ &= ~(0x3 << (flag << 1));
    mask_  &= ~(0x3 << (flag << 1));
}

template<int nflags_, uint32_t preset_>
inline unsigned char * Flags<nflags_, preset_>::split() const
{
    unsigned char * parts = new (std::nothrow) unsigned char[nflags_];
    if (parts)
        for (int flag = 0 ; flag < nflags_ ; ++flag)
            parts[flag] = ((flags_ >> (flag << 1)) & 0x3);
    return parts;
}

template<int nflags_, uint32_t preset_>
inline std::ostream & operator<< (std::ostream & os, Flags<nflags_, preset_> const & in)
{
    os << in.getName();
    return os;
}

} // namespace tools
} // namespace rpct

#endif // rpct_tools_Flags_inl_h_
