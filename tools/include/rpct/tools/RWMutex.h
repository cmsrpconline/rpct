/** Filip Thyssen */

#ifndef rpct_tools_RWMutex_h_
#define rpct_tools_RWMutex_h_

#include "rpct/tools/Mutex.h"

namespace rpct {
namespace tools {

/** A mutex allowing a fixed amount of readers, with equal priorities for readers and writers */
class RWMutex
{
protected:
    static void cleanup_unlock(void * arg);
public:
    /** Constructor
     * \param state the initial state for writing
     * \param max the maximum number of parallel readers
     */
    RWMutex(size_t value = 1
            , size_t max = 32);
    virtual ~RWMutex();

    /** ask to read */
    int rtake();
    int rtry_take();
    int rtake(timeval const & timeout);
    int rtake(time_t sec, suseconds_t usec = 0);
    int rtake(timespec const & endtime);
    int rgive();

    /** ask to write */
    int wtake();
    int wtry_take();
    int wtake(timeval const & timeout);
    int wtake(time_t sec, suseconds_t usec = 0);
    int wtake(timespec const & endtime);
    int wgive();

    /** ask to write */
    int take();
    int try_take();
    int take(timeval const & timeout);
    int take(time_t sec, suseconds_t usec = 0);
    int take(timespec const & endtime);
    int give();

    /** get the maximum number of readers */
    size_t max() const;
    /** set the maximum number of readers */
    void max(const size_t & max);

    /** returns the number of available writer-places: 0 or 1 */
    size_t value() const;
    /** returns the number of available writer-places: 0 or 1 */
    size_t wvalue() const;
    /** returns the number of available reader-places */
    size_t rvalue() const;

protected:
    pthread_mutexattr_t attr_;
    pthread_mutex_t smutex_   ///< shared mutex
        , vmutex_;            ///< value_ and max_ mutex

    volatile int max_;       ///< The maximum number of readers
    volatile int rvalue_;    ///< The number of available readers
    volatile size_t wvalue_; ///< The number of available writers
    pthread_cond_t max_reached_;
};

typedef TLock<RWMutex> WLock;

class RLock
{
public:
    RLock(RWMutex & lmutex) throw (int);
    RLock(RWMutex & lmutex, timeval const & timeout) throw (int);
    RLock(RWMutex & lmutex, time_t sec, suseconds_t usec = 0) throw (int);
    RLock(RWMutex & lmutex, timespec const & endtime) throw (int);
    RLock(RLock const & rlock);

    ~RLock();

    void keep();
    void unkeep();

    int give();
    int take();
protected:
    RWMutex & lmutex_;
    mutable int e_;
};

class RWLock
{
public:
    static const bool read_ = false;
    static const bool write_ = true;

public:
    RWLock(bool read_or_write, RWMutex & lmutex) throw (int);
    RWLock(bool read_or_write, RWMutex & lmutex, timeval const & timeout) throw (int);
    RWLock(bool read_or_write, RWMutex & lmutex, time_t sec, suseconds_t usec = 0) throw (int);
    RWLock(bool read_or_write, RWMutex & lmutex, timespec const & endtime) throw (int);
    RWLock(RWLock const & rwlock);

    ~RWLock();

    void keep();
    void unkeep();

    int change(bool read_or_write);

    int give();
    int take();

protected:
    RWMutex & lmutex_;
    bool read_or_write_;
    mutable int e_;
};

template<typename Obj>
class RLockPointer
{
public:
    RLockPointer(Obj & lobject, RWMutex & lmutex) throw (int);
    RLockPointer(Obj & lobject, RWMutex & lmutex, timeval const & timeout) throw (int);
    RLockPointer(Obj & lobject, RWMutex & lmutex, time_t sec, suseconds_t usec = 0) throw (int);
    RLockPointer(Obj & lobject, RWMutex & lmutex, timespec const & endtime) throw (int);
    RLockPointer(RLockPointer<Obj> const & lptr);

    ~RLockPointer();

    void keep();
    void unkeep();

    int give();

    Obj & operator*();
    Obj * operator->();
protected:
    Obj & lobject_;
    RWMutex & lmutex_;
    mutable int e_;
};

} // namespace tools
} // namespace rpct

#include "rpct/tools/RWMutex-inl.h"

#endif // rpct_tools_RWMutex_h_
