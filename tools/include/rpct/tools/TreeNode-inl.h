/** Filip Thyssen */

#ifndef rpct_tools_TreeNode_inl_h_
#define rpct_tools_TreeNode_inl_h_

#include "rpct/tools/TreeNode.h"

#include <ostream>
#include <stdexcept>

namespace rpct {
namespace tools {

template<typename T, bool tp_>
inline TreeNodeIterator<T, tp_>::TreeNodeIterator(int max_depth)
    : tn_(0)
    , depth_(-1)
    , max_depth_(max_depth)
    , skip_children_(0)
{}

template<typename T, bool tp_>
inline TreeNodeIterator<T, tp_>::TreeNodeIterator(const TreeNodeIterator<T, tp_> & it)
    : tn_(it.tn_)
    , depth_(it.depth_)
    , max_depth_(it.max_depth_)
    , skip_children_(it.skip_children_)
{}

template<typename T, bool tp_>
inline TreeNodeIterator<T, tp_>::TreeNodeIterator(TreeNode<T, tp_> & tn, int max_depth)
    : tn_(&tn)
    , depth_(0)
    , max_depth_(max_depth)
    , skip_children_(0)
{}

template<typename T, bool tp_>
inline TreeNodeIterator<T, tp_> & TreeNodeIterator<T, tp_>::operator=(const TreeNodeIterator<T, tp_> & it)
{
    if (this != &it)
        {
            tn_ = it.tn_;
            depth_ = it.depth_;
            max_depth_ = it.max_depth_;
            skip_children_ = it.skip_children_;
        }
    return *this;
}

template<typename T, bool tp_>
inline TreeNodeIterator<T, tp_> & TreeNodeIterator<T, tp_>::operator++()
{
    if (tn_ && depth_ >= 0)
        {
            if (!skip_children_ && tn_->ti_lchild_ && (max_depth_ < 0 || depth_ < max_depth_))
                {
                    ++depth_;
                    tn_ = tn_->ti_lchild_;
                }
            else if (tn_->ti_rsibling_)
                tn_ = tn_->ti_rsibling_;
            else
                {
                    while (depth_ > 0 && !(tn_->ti_rsibling_) && tn_->ti_parent_)
                        {
                            --depth_;
                            tn_ = tn_->ti_parent_;
                        }
                    if (tn_->ti_rsibling_)
                        tn_ = tn_->ti_rsibling_;
                    else
                        depth_ = -1; // end
                }
        }
    else
        throw std::out_of_range("operator++ on an end iterator.");
    skip_children_ = false;
    return *this;
}

template<typename T, bool tp_>
inline TreeNodeIterator<T, tp_> TreeNodeIterator<T, tp_>::operator++(int)
{
    TreeNodeIterator temp(*this);
    operator++();
    return temp;
}

template<typename T, bool tp_>
inline TreeNodeIterator<T, tp_> & TreeNodeIterator<T, tp_>::operator--()
{
    if (tn_ && depth_ >= 0)
        {
            if (tn_->ti_lsibling_)
                {
                    tn_ = tn_->ti_lsibling_;
                    while (!skip_children_ && tn_->ti_rchild_ && (max_depth_ < 0 || depth_ < max_depth_))
                        {
                            ++depth_;
                            tn_ = tn_->ti_rchild_;
                        }
                }
            else if (depth_ > 0 && tn_->ti_parent_)
                {
                    --depth_;
                    tn_ = tn_->ti_parent_;
                }
            else
                throw std::out_of_range("operator-- on a begin iterator.");
        }
    else if (tn_)
        depth_ = 0;
    skip_children_ = false;
    return *this;
}

template<typename T, bool tp_>
inline TreeNodeIterator<T, tp_> TreeNodeIterator<T, tp_>::operator--(int)
{
    TreeNodeIterator temp(*this);
    operator--();
    return temp;
}

template<typename T, bool tp_>
inline bool TreeNodeIterator<T, tp_>::operator==(const TreeNodeIterator<T, tp_> & it) const
{
    return (
            (depth_ < 0 && it.depth_ < 0)
            ||
            (tn_==it.tn_ && depth_ >= 0 && it.depth_ >= 0)
            );
}

template<typename T, bool tp_>
inline bool TreeNodeIterator<T, tp_>::operator!=(const TreeNodeIterator<T, tp_> & it) const
{
    return !(*this == it);
}

template<typename T, bool tp_>
inline T & TreeNodeIterator<T, tp_>::operator*()
{
    if (tn_ && depth_ >= 0)
        return tn_->getTreeItem();
    else
        throw std::out_of_range("dereferencing end iterator.");
}

template<typename T, bool tp_>
inline T * TreeNodeIterator<T, tp_>::operator->()
{
    if (tn_ && depth_ >= 0)
        return &(tn_->getTreeItem());
    else
        return 0;
}

template<typename T, bool tp_>
inline const T & TreeNodeIterator<T, tp_>::operator*() const
{
    if (tn_ && depth_ >= 0)
        return tn_->getTreeItem();
    else
        throw std::out_of_range("dereferencing end iterator.");
}

template<typename T, bool tp_>
inline const T * TreeNodeIterator<T, tp_>::operator->() const
{
    if (tn_ && depth_ >= 0)
        return &(tn_->getTreeItem());
    else
        return 0;
}

template<typename T, bool tp_>
inline int TreeNodeIterator<T, tp_>::depth() const
{
    return depth_;
}

template<typename T, bool tp_>
inline int TreeNodeIterator<T, tp_>::max_depth() const
{
    return max_depth_;
}

template<typename T, bool tp_>
inline void TreeNodeIterator<T, tp_>::set_max_depth(int max_depth)
{
    max_depth_ = max_depth;
}

template<typename T, bool tp_>
inline bool TreeNodeIterator<T, tp_>::skip_children() const
{
    return skip_children_;
}

template<typename T, bool tp_>
inline void TreeNodeIterator<T, tp_>::set_skip_children(bool skip_children)
{
    skip_children_ = skip_children;
}

template<typename T, bool tp_>
inline typename TreeNodeIterator<T, tp_>::iterator TreeNodeIterator<T, tp_>::begin()
{
    if (tn_)
        return tn_->tree_begin();
    else
        return iterator(-1);
}

template<typename T, bool tp_>
inline typename TreeNodeIterator<T, tp_>::iterator TreeNodeIterator<T, tp_>::end()
{
    if (tn_)
        return tn_->tree_end();
    else
        return iterator(-1);
}

template<typename T, bool tp_>
inline typename TreeNodeIterator<T, tp_>::reverse_iterator TreeNodeIterator<T, tp_>::rbegin()
{
    return reverse_iterator(end());
}

template<typename T, bool tp_>
inline typename TreeNodeIterator<T, tp_>::reverse_iterator TreeNodeIterator<T, tp_>::rend()
{
    return reverse_iterator(begin());
}

template<typename T, bool tp_>
inline typename TreeNodeIterator<T, tp_>::sibling_iterator TreeNodeIterator<T, tp_>::s_begin()
{
    if (tn_)
        return tn_->tree_s_begin();
    else
        return sibling_iterator(-1);
}

template<typename T, bool tp_>
inline typename TreeNodeIterator<T, tp_>::sibling_iterator TreeNodeIterator<T, tp_>::s_end()
{
    if (tn_)
        return tn_->tree_s_end();
    else
        return sibling_iterator(-1);
}

template<typename T, bool tp_>
inline typename TreeNodeIterator<T, tp_>::reverse_sibling_iterator TreeNodeIterator<T, tp_>::s_rbegin()
{
    return reverse_sibling_iterator(s_end());
}

template<typename T, bool tp_>
inline typename TreeNodeIterator<T, tp_>::reverse_sibling_iterator TreeNodeIterator<T, tp_>::s_rend()
{
    return reverse_sibling_iterator(s_begin());
}

template<typename T, bool tp_>
inline TreeNode<T, tp_>::TreeNode(T const & item
                                  , int position
                                  , TreeNode<T, tp_> * parent
                                  , TreeNode<T, tp_> * siblings)
    : ti_position_(position)
    , ti_parent_(0)
    , ti_rsibling_(0)
    , ti_lchild_(0)
    , ti_lsibling_(0)
    , ti_rchild_(0)
    , ti_item_(item)
{
    if (parent)
        parent->addTreeChild(*this);
    else if (siblings)
        siblings->addTreeSibling(*this);
}

template<typename T, bool tp_>
inline TreeNode<T, tp_>::TreeNode(TreeNode<T, tp_> const & t)
    : ti_position_(t.ti_position_)
    , ti_parent_(0)
    , ti_rsibling_(0)
    , ti_lchild_(0)
    , ti_lsibling_(0)
    , ti_rchild_(0)
    , ti_item_(tp_ ? static_cast<T>(this) : t.ti_item_)
{
    TreeNode<T, tp_> & nct = const_cast<TreeNode<T, tp_> & >(t);
    nct.addTreeSibling(*this);
    if (t.ti_lchild_)
        addTreeChildren(*(nct.ti_lchild_));
}

template<typename T, bool tp_>
inline TreeNode<T, tp_> & TreeNode<T, tp_>::operator=(TreeNode<T, tp_> const & t)
{
    if (this != &t)
        {
            removeFromTree();
            removeTreeChildren();

            ti_item_ = (tp_ ? static_cast<T>(this) : t.ti_item_);

            TreeNode<T, tp_> & nct = const_cast<TreeNode<T, tp_> & >(t);
            nct.addTreeSibling(*this);
            if (t.ti_lchild_)
                addTreeChildren(*(nct.ti_lchild_));
        }
    return *this;
}

template<typename T, bool tp_>
inline TreeNode<T, tp_>::~TreeNode()
{
    this->removeFromTree();
    this->removeTreeChildren();
}

template<typename T, bool tp_>
inline void TreeNode<T, tp_>::removeFromTree()
{
    if (ti_lsibling_)
        ti_lsibling_->ti_rsibling_ = ti_rsibling_;
    else if (ti_parent_)
        ti_parent_->ti_lchild_ = ti_rsibling_;
    if (ti_rsibling_)
        ti_rsibling_->ti_lsibling_ = ti_lsibling_;
    else if (ti_parent_)
        ti_parent_->ti_rchild_ = ti_lsibling_;

    ti_lsibling_ = 0;
    ti_rsibling_ = 0;
    ti_parent_ = 0;
}

template<typename T, bool tp_>
inline void TreeNode<T, tp_>::removeTreeChildren()
{
    if (ti_lchild_)
        do {
            ti_rchild_->ti_parent_ = 0;
        } while ((ti_rchild_ = ti_rchild_->ti_lsibling_));
    ti_lchild_ = 0;
    ti_rchild_ = 0;
}

template<typename T, bool tp_>
inline bool TreeNode<T, tp_>::operator==(const TreeNode<T, tp_> & it) const
{
    return (it.ti_position_ == ti_position_ && it.ti_item_ == ti_item_);
}

template<typename T, bool tp_>
inline bool TreeNode<T, tp_>::operator!=(const TreeNode<T, tp_> & it) const
{
    return !(*this == it);
}

template<typename T, bool tp_>
inline bool TreeNode<T, tp_>::operator==(const T & t) const
{
    return (ti_item_ == t);
}

template<typename T, bool tp_>
inline bool TreeNode<T, tp_>::operator!=(const T & t) const
{
    return (ti_item_ != t);
}

template<typename T, bool tp_>
inline T & TreeNode<T, tp_>::getTreeItem()
{
    return ti_item_;
}

template<typename T, bool tp_>
inline T const & TreeNode<T, tp_>::getTreeItem() const
{
    return ti_item_;
}

template<typename T, bool tp_>
inline int TreeNode<T, tp_>::getTreePosition() const
{
    return ti_position_;
}

template<typename T, bool tp_>
inline TreeNode<T, tp_> * TreeNode<T, tp_>::getTreeParent() const
{
    return ti_parent_;
}

template<typename T, bool tp_>
inline TreeNode<T, tp_> * TreeNode<T, tp_>::getTreeSibling() const
{
    return ti_rsibling_;
}

template<typename T, bool tp_>
inline TreeNode<T, tp_> * TreeNode<T, tp_>::getTreeChild() const
{
    return ti_lchild_;
}

template<typename T, bool tp_>
inline void TreeNode<T, tp_>::addTreeSibling(TreeNode<T, tp_> & tree)
{
    tree.removeFromTree();

    tree.ti_parent_ = ti_parent_;
    tree.ti_lsibling_ = this;

    while (tree.ti_lsibling_->ti_lsibling_ && tree.ti_lsibling_->ti_position_ > tree.ti_position_)
        tree.ti_lsibling_ = tree.ti_lsibling_->ti_lsibling_;
    while (tree.ti_lsibling_->ti_rsibling_ && tree.ti_lsibling_->ti_position_ <= tree.ti_position_)
        tree.ti_lsibling_ = tree.ti_lsibling_->ti_rsibling_;

    if (!tree.ti_lsibling_->ti_lsibling_ && tree.ti_lsibling_->ti_position_ > tree.ti_position_)
        {
            tree.ti_rsibling_ = tree.ti_lsibling_;
            tree.ti_rsibling_->ti_lsibling_ = &tree;
            tree.ti_lsibling_ = 0;
            if (ti_parent_)
                ti_parent_->ti_lchild_ = &tree;
        }
    else
        {
            tree.ti_rsibling_ = tree.ti_lsibling_->ti_rsibling_;
            tree.ti_lsibling_->ti_rsibling_ = &tree;
            if (tree.ti_rsibling_)
                tree.ti_rsibling_->ti_lsibling_ = &tree;
            else if (ti_parent_)
                ti_parent_->ti_rchild_ = &tree;
        }
}

template<typename T, bool tp_>
inline void TreeNode<T, tp_>::addTreeSiblings(TreeNode<T, tp_> & tree)
{
    TreeNode<T, tp_> * prev = this;
    TreeNode<T, tp_> * ins = &tree;

    if (tree.ti_parent_)
        {
            ins = tree.ti_parent_->ti_lchild_;
            tree.ti_parent_->removeTreeChildren();
        }
    else
        while (ins->ti_lsibling_)
            ins = ins->ti_lsibling_;

    TreeNode<T, tp_> * next;
    while (next = ins->ti_rsibling_)
        {
            prev->addTreeSibling(*ins);
            prev = ins;
            ins = next;
        }
    prev->addTreeSibling(*ins);
}

template<typename T, bool tp_>
inline void TreeNode<T, tp_>::addTreeChild(TreeNode<T, tp_> & tree)
{
    if (ti_lchild_)
        ti_lchild_->TreeNode<T, tp_>::addTreeSibling(tree);
    else
        {
            tree.removeFromTree();
            tree.ti_parent_ = this;
            ti_lchild_ = &tree;
            ti_rchild_ = &tree;
        }
}

template<typename T, bool tp_>
inline void TreeNode<T, tp_>::addTreeChildren(TreeNode<T, tp_> & tree)
{
    TreeNode<T, tp_> * prev = ti_lchild_;
    TreeNode<T, tp_> * ins = &tree;

    if (tree.ti_parent_)
        {
            ins = tree.ti_parent_->ti_lchild_;
            tree.ti_parent_->removeTreeChildren();
        }
    else
        while (ins->ti_lsibling_)
            ins = ins->ti_lsibling_;

    if (!prev)
        {
            prev = ins;
            ins = prev->ti_rsibling_;
            addTreeChild(*prev);
        }
    if (ins)
        {
            TreeNode<T, tp_> * next;
            while (next = ins->ti_rsibling_)
                {
                    prev->addTreeSibling(*ins);
                    prev = ins;
                    ins = next;
                }
            prev->addTreeSibling(*ins);
        }
}

template<typename T, bool tp_>
inline typename TreeNode<T, tp_>::iterator TreeNode<T, tp_>::tree_begin()
{
    iterator it(*this);
    while (it.tn_->ti_lsibling_)
        it.tn_ = it.tn_->ti_lsibling_;
    return it;
}

template<typename T, bool tp_>
inline typename TreeNode<T, tp_>::iterator TreeNode<T, tp_>::tree_end()
{
    iterator it(*this);
    if (ti_parent_)
        it.tn_ = ti_parent_->ti_rchild_;
    else
        while (it.tn_->ti_rsibling_)
            it.tn_ = it.tn_->ti_rsibling_;
    while (it.tn_->ti_rchild_)
        {
            ++(it.depth_);
            it.tn_ = it.tn_->ti_rchild_;
        }
    ++it;
    return it;
}

template<typename T, bool tp_>
inline typename TreeNode<T, tp_>::reverse_iterator TreeNode<T, tp_>::tree_rbegin()
{
    return reverse_iterator(tree_end());
}

template<typename T, bool tp_>
inline typename TreeNode<T, tp_>::reverse_iterator TreeNode<T, tp_>::tree_rend()
{
    return reverse_iterator(tree_begin());
}

template<typename T, bool tp_>
inline typename TreeNode<T, tp_>::sibling_iterator TreeNode<T, tp_>::tree_s_begin()
{
    sibling_iterator it(*this, 0);
    while (it.tn_->ti_lsibling_)
        it.tn_ = it.tn_->ti_lsibling_;
    return it;
}

template<typename T, bool tp_>
inline typename TreeNode<T, tp_>::sibling_iterator TreeNode<T, tp_>::tree_s_end()
{
    if (ti_parent_)
        return ++(sibling_iterator(ti_parent_->ti_rchild_, 0));
    else
        {
            sibling_iterator it(*this, 0);
            while (it.tn_->ti_rsibling_)
                it.tn_ = it.tn_->ti_rsibling_;
            ++it;
            return it;
        }
}

template<typename T, bool tp_>
inline typename TreeNode<T, tp_>::reverse_sibling_iterator TreeNode<T, tp_>::tree_s_rbegin()
{
    return reverse_sibling_iterator(tree_s_end());
}

template<typename T, bool tp_>
inline typename TreeNode<T, tp_>::reverse_sibling_iterator TreeNode<T, tp_>::tree_s_rend()
{
    return reverse_sibling_iterator(tree_s_begin());
}

template<typename T, bool tp_>
inline void Tree<T, tp_>::erase(TreeNode<T, tp_> * tn)
{
    if (tn)
        {
            while (tn->ti_lchild_)
                rerase(tn->ti_lchild_);
            if (tn == child_)
                child_ = tn->ti_rsibling_;
            delete tn;
        }
}

template<typename T, bool tp_>
inline void Tree<T, tp_>::rerase(TreeNode<T, tp_> * tn)
{
    while (tn->ti_lchild_)
        rerase(tn->ti_lchild_);
    delete tn;
}

template<typename T, bool tp_>
inline Tree<T, tp_>::Tree()
    : child_(0)
{}

template<typename T, bool tp_>
inline Tree<T, tp_>::~Tree()
{
    clear();
}

template<typename T, bool tp_>
inline void Tree<T, tp_>::clear()
{
    while (child_)
        erase(child_);
}

template<typename T, bool tp_>
inline bool Tree<T, tp_>::empty() const
{
    return child_;
}

template<typename T, bool tp_>
inline typename Tree<T, tp_>::iterator Tree<T, tp_>::add(const T & t, int position)
{
    TreeNode<T, tp_> * tn = new TreeNode<T, tp_>(t, position, 0, child_);
    if (!child_)
        child_ = tn;
    if (child_->ti_lsibling_)
        child_ = child_->ti_lsibling_;
    return iterator(*tn);
}

template<typename T, bool tp_>
inline typename Tree<T, tp_>::iterator Tree<T, tp_>::addChild(iterator const & it, const T & t, int position)
{
    if (it.depth_ >= 0)
        {
            TreeNode<T, tp_> * tn = new TreeNode<T, tp_>(t, position, it.tn_);
            return iterator(*tn);
        }
    else
        throw std::out_of_range("adding child to end iterator.");
}

template<typename T, bool tp_>
inline typename Tree<T, tp_>::iterator Tree<T, tp_>::addSibling(iterator const & it, const T & t, int position)
{
    if (it.depth_ >= 0)
        {
            TreeNode<T, tp_> * tn = new TreeNode<T, tp_>(t, position, 0, it.tn_);
            if (child_->ti_lsibling_)
                child_ = child_->ti_lsibling_;
            return iterator(*tn);
        }
    else
        throw std::out_of_range("adding child to end iterator.");
}

template<typename T, bool tp_>
inline void Tree<T, tp_>::erase(iterator const & it)
{
    if (it.depth_ >= 0)
        erase(it.tn_);
    else
        throw std::out_of_range("erasing end iterator.");
}

template<typename T, bool tp_>
inline typename Tree<T, tp_>::iterator Tree<T, tp_>::find(const T & t)
{
    iterator it = begin();
    while (it.depth_ >= 0 && *(it.tn_) != t)
        ++it;
    return it;
}

template<typename T, bool tp_>
inline typename Tree<T, tp_>::iterator Tree<T, tp_>::begin()
{
    if (child_)
        return child_->tree_begin();
    else
        return iterator();
}

template<typename T, bool tp_>
inline typename Tree<T, tp_>::iterator Tree<T, tp_>::end()
{
    if (child_)
        return child_->tree_end();
    else
        return iterator();
}

template<typename T, bool tp_>
inline typename Tree<T, tp_>::reverse_iterator Tree<T, tp_>::rbegin()
{
    return reverse_iterator(end());
}

template<typename T, bool tp_>
inline typename Tree<T, tp_>::reverse_iterator Tree<T, tp_>::rend()
{
    return reverse_iterator(begin());
}

template<typename T, bool tp_>
inline typename Tree<T, tp_>::sibling_iterator Tree<T, tp_>::s_begin()
{
    if (child_)
        return child_->tree_s_begin();
    else
        return sibling_iterator(0);
}

template<typename T, bool tp_>
inline typename Tree<T, tp_>::sibling_iterator Tree<T, tp_>::s_end()
{
    if (child_)
        return child_->tree_s_end();
    else
        return sibling_iterator(0);
}

template<typename T, bool tp_>
inline typename Tree<T, tp_>::reverse_sibling_iterator Tree<T, tp_>::s_rbegin()
{
    return reverse_sibling_iterator(s_end());
}

template<typename T, bool tp_>
inline typename Tree<T, tp_>::reverse_sibling_iterator Tree<T, tp_>::s_rend()
{
    return reverse_sibling_iterator(s_begin());
}

} // namespace tools
} // namespace rpct

#endif // rpct_tools_TreeNode_inl_h_
