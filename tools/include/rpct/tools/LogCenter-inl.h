/** Filip Thyssen */

#ifndef rpct_tools_LogCenter_inl_h_
#define rpct_tools_LogCenter_inl_h_

#include "rpct/tools/LogCenter.h"
#include "log4cplus/loggingmacros.h"

namespace rpct {
namespace tools {

inline int LogCategory::getId() const
{
    return id_;
}
inline std::string const & LogCategory::getName() const
{
    return name_;
}
inline std::string const & LogCategory::getDescription() const
{
    return description_;
}

inline int LogMessage::getId() const
{
    return id_;
}
inline int LogMessage::getCategoryId() const
{
    return cat_id_;
}
inline unsigned char LogMessage::getLevel() const
{
    return level_;
}
inline std::string const & LogMessage::getName() const
{
    return name_;
}
inline std::string const & LogMessage::getDescription() const
{
    return description_;
}

inline LogEntry::LogEntry(int id, int cat_id, time_t tv)
    : id_(id)
    , cat_id_(cat_id)
    , tv_(tv)
{
    if (!tv_)
        tv_ = time(0);
}

inline int LogEntry::getId() const
{
    return id_;
}

inline int LogEntry::getCategoryId() const
{
    return cat_id_;
}

inline time_t LogEntry::getTime() const
{
    return tv_;
}
inline void LogEntry::setTime(time_t tv) const
{
    if (tv)
        tv_ = tv;
    else
        tv_ = time(0);
}
inline bool LogEntry::operator<(LogEntry const & le) const
{
    return (cat_id_ < le.cat_id_ ||
            (!(le.cat_id_ < cat_id_) && id_ < le.id_));
}

inline void LogCenter::addCategory(int cat_id
                                   , const std::string & name
                                   , const std::string & description)
{
    WLock lock(mutex_);
    if (cat_id >= 0)
        iaddCategory(cat_id, name, description);
    else
        {
            LOG4CPLUS_ERROR(log4cplus_logger_, "Not adding log category " << name << " with negative id.");
        }
}

inline void LogCenter::addMessage(int id, int cat_id, unsigned char level
                                  , const std::string & name
                                  , const std::string & description)
{
    WLock lock(mutex_);
    if (id >= 0 && cat_id >= 0)
        iaddMessage(id, cat_id, level, name, description);
    else
        {
            LOG4CPLUS_ERROR(log4cplus_logger_, "Not adding log message " << name << " with negative id or category id.");
        }
}

inline LogCategory const & LogCenter::getCategory(const std::string & name)
{
    RWLock lock(false, mutex_);
    return igetCategory(lock, name);
}
inline LogCategory const & LogCenter::getCategory(int cat_id)
{
    RWLock lock(false, mutex_);
    return igetCategory(lock, cat_id);
}

inline LogMessage const & LogCenter::getMessage(const std::string & name)
{
    RWLock lock(false, mutex_);
    return igetMessage(lock, name);
}
inline LogMessage const & LogCenter::getMessage(int id)
{
    RWLock lock(false, mutex_);
    return igetMessage(lock, id);
}

inline void LogCenter::setPublisherLogType(int type)
{
    logtype_ = type;
}
inline void LogCenter::setPublisherUnLogType(int type)
{
    unlogtype_ = type;
}

inline void LogCenter::setPublisher(Publisher & publisher)
{
    RWLock lock(true, mutex_);
    publisher_ = &publisher;
}
inline void LogCenter::unsetPublisher()
{
    RWLock lock(true, mutex_);
    publisher_ = 0;
}
inline void LogCenter::setPublisher(int cat_id, Publisher & publisher)
{
    RWLock lock(true, mutex_);
    log_publishers_[cat_id] = &publisher;
}
inline void LogCenter::unsetPublisher(int cat_id)
{
    RWLock lock(true, mutex_);
    log_publishers_[cat_id] = 0;
}
inline void LogCenter::setPublisher(const std::string & name, Publisher & publisher)
{
    RWLock lock(true, mutex_);
    log_publishers_[igetCategory(lock, name).getId()] = &publisher;
}
inline void LogCenter::unsetPublisher(const std::string & name)
{
    RWLock lock(true, mutex_);
    log_publishers_[igetCategory(lock, name).getId()] = 0;
}



} // namespace tools
} // namespace rpct

#endif // rpct_tools_LogCenter_inl_h_
