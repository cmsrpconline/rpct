/** Filip Thyssen */

#ifndef rpct_tools_Mutex_inl_h_
#define rpct_tools_Mutex_inl_h_

#include "rpct/tools/Mutex.h"

#include "rpct/tools/RunningTimeVal.h"

namespace rpct {
namespace tools {

inline int Mutex::take(timeval const & timeout)
{
    timespec endtime = timeval_to_abs_timespec(timeout);
    return take(endtime);
}

inline int Mutex::take(time_t sec, suseconds_t usec)
{
    timeval tval;
    timeval_fill(tval, sec, usec);
    timespec endtime = timeval_to_abs_timespec(tval);
    return take(endtime);
}

inline size_t Mutex::value() const
{
    return value_;
}

template<typename T>
inline TLock<T>::TLock(T & lmutex) throw (int)
    : lmutex_(lmutex)
{
    e_ = lmutex_.take();
    if (e_)
        throw e_;
}

template<typename T>
inline TLock<T>::TLock(T & lmutex, timeval const & timeout) throw (int)
    : lmutex_(lmutex)
{
    e_ = lmutex_.take(timeout);
    if (e_)
        throw e_;
}

template<typename T>
inline TLock<T>::TLock(T & lmutex, time_t sec, suseconds_t usec) throw (int)
    : lmutex_(lmutex)
{
    e_ = lmutex_.take(sec, usec);
    if (e_)
        throw e_;
}

template<typename T>
inline TLock<T>::TLock(T & lmutex, timespec const & endtime) throw (int)
    : lmutex_(lmutex)
{
    e_ = lmutex_.take(endtime);
    if (e_)
        throw e_;
}

template<typename T>
inline TLock<T>::TLock(TLock<T> const & tlock)
    : lmutex_(tlock.lmutex_)
    , e_(tlock.e_)
{
    tlock.e_ = -2;
}

template<typename T>
inline void TLock<T>::keep()
{
    if (!e_)
        e_ = -1;
}

template<typename T>
inline void TLock<T>::unkeep()
{
    if (e_ == -1)
        e_ = 0;
}

template<typename T>
inline int TLock<T>::give()
{
    int e(e_);
    if (!e_ || e_ == -1)
        {
            e = lmutex_.give();
            e_ = -2;
        }
    return e;
}

template<typename T>
inline int TLock<T>::take()
{
    if (e_ && e_ != -1)
        e_ = lmutex_.take();
    return e_;
}

template<typename T>
inline TLock<T>::~TLock()
{
    if (!e_)
        lmutex_.give();
}

template<typename T, typename Obj>
inline TLockPointer<T, Obj>::TLockPointer(Obj & lobject, T & lmutex) throw (int)
    : lobject_(lobject)
    , lmutex_(lmutex)
{
    e_ = lmutex_.take();
    if (e_)
        throw e_;
}

template<typename T, typename Obj>
inline TLockPointer<T, Obj>::TLockPointer(Obj & lobject, T & lmutex, timeval const & timeout) throw (int)
    : lobject_(lobject)
    , lmutex_(lmutex)
{
    e_ = lmutex_.take(timeout);
    if (e_)
        throw e_;
}

template<typename T, typename Obj>
inline TLockPointer<T, Obj>::TLockPointer(Obj & lobject, T & lmutex, time_t sec, suseconds_t usec) throw (int)
    : lobject_(lobject)
    , lmutex_(lmutex)
{
    e_ = lmutex_.take(sec, usec);
    if (e_)
        throw e_;
}

template<typename T, typename Obj>
inline TLockPointer<T, Obj>::TLockPointer(Obj & lobject, T & lmutex, timespec const & endtime) throw (int)
    : lobject_(lobject)
    , lmutex_(lmutex)
{
    e_ = lmutex_.take(endtime);
    if (e_)
        throw e_;
}

template<typename T, typename Obj>
inline TLockPointer<T, Obj>::TLockPointer(TLockPointer<T, Obj> const & lptr)
    : lobject_(lptr.lobject_)
    , lmutex_(lptr.lmutex_)
    , e_(lptr.e_)
{
    lptr.e_ = -2;
}

template<typename T, typename Obj>
inline TLockPointer<T, Obj>::~TLockPointer()
{
    if (!e_)
        lmutex_.give();
}

template<typename T, typename Obj>
inline void TLockPointer<T, Obj>::keep()
{
    if (!e_)
        e_ = -1;
}

template<typename T, typename Obj>
inline void TLockPointer<T, Obj>::unkeep()
{
    if (e_ == -1)
        e_ = 0;
}

template<typename T, typename Obj>
inline int TLockPointer<T, Obj>::give()
{
    int e(e_);
    if (!e_ || e_ == -1)
        {
            e = lmutex_.give();
            e_ = -2;
        }
    return e;
}

template<typename T, typename Obj>
inline int TLockPointer<T, Obj>::take()
{
    if (e_ && e_ != -1)
        e_ = lmutex_.take();
    return e_;
}

template<typename T, typename Obj>
Obj & TLockPointer<T, Obj>:: operator*()
{
    return lobject_;
}

template<typename T, typename Obj>
Obj * TLockPointer<T, Obj>:: operator->()
{
    return &lobject_;
}

} // namespace tools
} // namespace rpct

#endif // rpct_tools_Mutex_inl_h_
