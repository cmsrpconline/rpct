/** Filip Thyssen */

#ifndef rpct_tools_RWMutex_inl_h_
#define rpct_tools_RWMutex_inl_h_

#include "rpct/tools/RWMutex.h"
#include "rpct/tools/RunningTimeVal.h"

namespace rpct {
namespace tools {

inline int RWMutex::rtake(timeval const & timeout)
{
    timespec endtime = timeval_to_abs_timespec(timeout);
    return rtake(endtime);
}

inline int RWMutex::rtake(time_t sec, suseconds_t usec)
{
    timeval tval;
    timeval_fill(tval, sec, usec);
    timespec endtime = timeval_to_abs_timespec(tval);
    return rtake(endtime);
}

inline int RWMutex::wtake(timeval const & timeout)
{
    timespec endtime = timeval_to_abs_timespec(timeout);
    return wtake(endtime);
}

inline int RWMutex::wtake(time_t sec, suseconds_t usec)
{
    timeval tval;
    timeval_fill(tval, sec, usec);
    timespec endtime = timeval_to_abs_timespec(tval);
    return wtake(endtime);
}

inline int RWMutex::take()
{
    return wtake();
}
inline int RWMutex::try_take()
{
    return wtry_take();
}
inline int RWMutex::take(timeval const & timeout)
{
    return wtake(timeout);
}
inline int RWMutex::take(time_t sec, suseconds_t usec)
{
    return wtake(sec, usec);
}
inline int RWMutex::take(timespec const & endtime)
{
    return wtake(endtime);
}
inline int RWMutex::give()
{
    return wgive();
}

inline size_t RWMutex::value() const
{
    return wvalue();
}

inline RLock::RLock(RWMutex & lmutex) throw (int)
    : lmutex_(lmutex)
{
    e_ = lmutex_.rtake();
    if (e_)
        throw e_;
}

inline RLock::RLock(RWMutex & lmutex, timeval const & timeout) throw (int)
    : lmutex_(lmutex)
{
    e_ = lmutex_.rtake(timeout);
    if (e_)
        throw e_;
}

inline RLock::RLock(RWMutex & lmutex, time_t sec, suseconds_t usec) throw (int)
    : lmutex_(lmutex)
{
    e_ = lmutex_.rtake(sec, usec);
    if (e_)
        throw e_;
}

inline RLock::RLock(RWMutex & lmutex, timespec const & endtime) throw (int)
    : lmutex_(lmutex)
{
    e_ = lmutex_.rtake(endtime);
    if (e_)
        throw e_;
}

inline RLock::RLock(RLock const & rlock)
    : lmutex_(rlock.lmutex_)
    , e_(rlock.e_)
{
    rlock.e_ = -2;
}

inline void RLock::keep()
{
    if (!e_)
        e_ = -1;
}

inline void RLock::unkeep()
{
    if (e_ == -1)
        e_ = 0;
}

inline int RLock::give()
{
    int e(e_);
    if (!e_ || e_ == -1)
        {
            e = lmutex_.rgive();
            e_ = -2;
        }
    return e;
}

inline int RLock::take()
{
    if (e_ && e_ != -1)
        e_ = lmutex_.rtake();
    return e_;
}

inline RLock::~RLock()
{
    if (!e_)
        lmutex_.rgive();
}

inline RWLock::RWLock(bool read_or_write, RWMutex & lmutex) throw (int)
    : lmutex_(lmutex)
    , read_or_write_(read_or_write)
{
    if (read_or_write_)
        e_ = lmutex_.wtake();
    else
        e_ = lmutex_.rtake();
    if (e_)
        throw e_;
}

inline RWLock::RWLock(bool read_or_write, RWMutex & lmutex, timeval const & timeout) throw (int)
    : lmutex_(lmutex)
    , read_or_write_(read_or_write)
{
    if (read_or_write_)
        e_ = lmutex_.wtake(timeout);
    else
        e_ = lmutex_.rtake(timeout);
    if (e_)
        throw e_;
}

inline RWLock::RWLock(bool read_or_write, RWMutex & lmutex, time_t sec, suseconds_t usec) throw (int)
    : lmutex_(lmutex)
    , read_or_write_(read_or_write)
{
    if (read_or_write_)
        e_ = lmutex_.wtake(sec, usec);
    else
        e_ = lmutex_.rtake(sec, usec);
    if (e_)
        throw e_;
}

inline RWLock::RWLock(bool read_or_write, RWMutex & lmutex, timespec const & endtime) throw (int)
    : lmutex_(lmutex)
    , read_or_write_(read_or_write)
{
    if (read_or_write_)
        e_ = lmutex_.wtake(endtime);
    else
        e_ = lmutex_.rtake(endtime);
    if (e_)
        throw e_;
}

inline RWLock::RWLock(RWLock const & rwlock)
    : lmutex_(rwlock.lmutex_)
    , read_or_write_(rwlock.read_or_write_)
    , e_(rwlock.e_)
{
    rwlock.e_ = -2;
}

inline void RWLock::keep()
{
    if (!e_)
        e_ = -1;
}

inline void RWLock::unkeep()
{
    if (e_ == -1)
        e_ = 0;
}

inline int RWLock::change(bool read_or_write)
{
    if (e_ < 1 && e_ != -2 && read_or_write != read_or_write_) // no errors, and not unlocked
        {
            e_ = give();
            read_or_write_ = read_or_write;
            if (!e_)
                {
                    if (read_or_write_)
                        e_ = lmutex_.wtake(); 
                    else
                        e_ = lmutex_.rtake();
                }
        }
    return e_;
}

inline int RWLock::give()
{
    int e(e_);
    if (!e_ || e_ == -1)
        {
            if (read_or_write_)
                e = lmutex_.wgive();
            else
                e = lmutex_.rgive();
            e_ = -2;
        }
    return e;
}

inline int RWLock::take()
{
    if (e_ && e_ != -1)
        {
            if (read_or_write_)
                e_ = lmutex_.wtake();
            else
                e_ = lmutex_.rtake();
        }
    return e_;
}

inline RWLock::~RWLock()
{
    if (!e_)
        {
            if (read_or_write_)
                lmutex_.wgive();
            else
                lmutex_.rgive();
        }
}

template<typename Obj>
inline RLockPointer<Obj>::RLockPointer(Obj & lobject, RWMutex & lmutex) throw (int)
    : lobject_(lobject)
    , lmutex_(lmutex)
{
    e_ = lmutex_.rtake();
    if (e_)
        throw e_;
}

template<typename Obj>
inline RLockPointer<Obj>::RLockPointer(Obj & lobject, RWMutex & lmutex, timeval const & timeout) throw (int)
    : lobject_(lobject)
    , lmutex_(lmutex)
{
    e_ = lmutex_.rtake(timeout);
    if (e_)
        throw e_;
}

template<typename Obj>
inline RLockPointer<Obj>::RLockPointer(Obj & lobject, RWMutex & lmutex, time_t sec, suseconds_t usec) throw (int)
    : lobject_(lobject)
    , lmutex_(lmutex)
{
    e_ = lmutex_.rtake(sec, usec);
    if (e_)
        throw e_;
}

template<typename Obj>
inline RLockPointer<Obj>::RLockPointer(Obj & lobject, RWMutex & lmutex, timespec const & endtime) throw (int)
    : lobject_(lobject)
    , lmutex_(lmutex)
{
    e_ = lmutex_.rtake(endtime);
    if (e_)
        throw e_;
}

template<typename Obj>
inline RLockPointer<Obj>::RLockPointer(RLockPointer<Obj> const & lptr)
    : lobject_(lptr.lobject_)
    , lmutex_(lptr.lmutex_)
    , e_(lptr.e_)
{
    lptr.e_ = -2;
}

template<typename Obj>
inline RLockPointer<Obj>::~RLockPointer()
{
    if (!e_)
        lmutex_.rgive();
}

template<typename Obj>
inline void RLockPointer<Obj>::keep()
{
    if (!e_)
        e_ = -1;
}

template<typename Obj>
inline void RLockPointer<Obj>::unkeep()
{
    if (e_ == -1)
        e_ = 0;
}

template<typename Obj>
inline int RLockPointer<Obj>::give()
{
    int e(e_);
    if (!e_ || e_ == -1)
        {
            e = lmutex_.rgive();
            e_ = -2;
        }
    return e;
}

template<typename Obj>
Obj & RLockPointer<Obj>:: operator*()
{
    return lobject_;
}

template<typename Obj>
Obj * RLockPointer<Obj>:: operator->()
{
    return &lobject_;
}

} // namespace tools
} // namespace rpct

#endif // rpct_tools_RWMutex_inl_h_
