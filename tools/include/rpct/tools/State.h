/** Filip Thyssen */

#ifndef rpct_tools_State_h_
#define rpct_tools_State_h_

#include <ostream>

#include "rpct/tools/TreeNode.h"

#include "log4cplus/loglevel.h"

namespace rpct {
namespace tools {

class State
{
public:
    static const unsigned char ok_;
    static const unsigned char notset_;
    static const unsigned char info_;
    static const unsigned char warn_;
    static const unsigned char error_;
    static const unsigned char unknown_;
    static const unsigned char max_;
    static const unsigned char default_;

    static const char * name_[];
    static const char * short_[];
    static const char * color_[];
    static const char * bgcolor_[];
    static const char * image_[];

    static std::string getStateName(unsigned char level);
    static std::string getStateShort(unsigned char level);
    static std::string getStateColor(unsigned char level);
    static std::string getStateBgColor(unsigned char level);
    static std::string getStateImage(unsigned char level);

    static const log4cplus::LogLevel loglevel_[];

    static log4cplus::LogLevel getStateLogLevel(unsigned char _level);

public:
    State();
    virtual ~State();

    unsigned char getState() const;
    virtual void setState(unsigned char state);
    virtual void reset();

    void ok();
    bool isOk() const;
    void notset();
    bool isNotSet() const;
    void info();
    bool isInfo() const;
    void warn();
    bool isWarn() const;
    void error();
    bool isError() const;

protected:
    unsigned char state_;
};

bool operator<(const State & state_a, const State & state_b);
std::ostream & operator<<(std::ostream & outstream, const State & state);

// for inheritance, takes a pointer
template<typename T>
class TTreeState : public virtual State
                 , public TreeNode<T, true>
{
protected:
    void setPStateUp(unsigned char state);
    void setPStateDown();

public:
    TTreeState(int position = 0
               , TreeNode<T, true> * parent = 0
               , TreeNode<T, true> * siblings = 0);

    void setState(unsigned char state);

    unsigned char getPState() const;

    void removeFromTree();
    void removeTreeChildren();

    void addTreeSibling(TreeNode<T, true> & tree);
    void addTreeChild  (TreeNode<T, true> & tree);

protected:
    unsigned char pstate_;

    using TreeNode<T, true>::ti_parent_;
    using TreeNode<T, true>::ti_lchild_;
};

/** for standalone use */
class TreeState : public TTreeState<TreeState *>
{
public:
    TreeState(int position = 0
              , TreeNode< TreeState *, true> * parent = 0
              , TreeNode< TreeState *, true> * siblings = 0);
};

} // namespace tools
} // namespace rpct

#include "rpct/tools/State-inl.h"

#endif // rpct_tools_State_h_
