/** Filip Thyssen */

#ifndef rpct_tools_LogState_h_
#define rpct_tools_LogState_h_

#include <ostream>
#include <set>
#include <utility>

#include "rpct/tools/State.h"
#include "rpct/tools/Item.h"

#include "rpct/tools/LogCenter.h"

namespace rpct {
namespace tools {

class LogState : public virtual State
               , public virtual Item
{
public:
    static LogCenter & getLogCenter();

protected:
    friend class LogCenter;

    typedef std::pair<unsigned char, LogEntry > log_pair_type; ///< level, entry
    typedef std::set<log_pair_type> log_type;

    void calculateLogState();

    virtual bool log(unsigned char level, int id, int cat_id);
    virtual bool unlog(unsigned char level, int id, int cat_id);

public:
    LogState();

    int getId() const;

    void reset();

    void log(const std::string & name);
    void unlog(const std::string & name);

    void log(int id);
    void unlog(int id);

    log_type const & getLog() const;

    void printLog(std::ostream & os) const;
    void printLogHTML(std::ostream & os) const;

protected:
    static LogCenter * logcenter_;

    log_type log_;
};

template<typename T>
class TTreeLogState : public LogState
                    , public TTreeState<T>
{
public:
    TTreeLogState(int position = 0
                  , TreeNode<T, true> * parent = 0
                  , TreeNode<T, true> * sibling = 0);
};

// for standalone use
class TreeLogState : public TTreeLogState<TreeLogState *>
{
public:
    TreeLogState(int position = 0
                 , TreeNode< TreeLogState *, true> * parent = 0
                 , TreeNode< TreeLogState *, true> * sibling = 0);
};

} // namespace tools
} // namespace rpct

#include "rpct/tools/LogState-inl.h"

#endif // rpct_tools_LogState_h_
