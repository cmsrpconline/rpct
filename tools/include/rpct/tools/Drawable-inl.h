/** Filip Thyssen */

#ifndef rpct_tools_Drawable_inl_h_
#define rpct_tools_Drawable_inl_h_

#include "rpct/tools/Drawable.h"

namespace rpct {
namespace tools {

inline void Drawable::printSVGDocument(std::ostream & os, int depth) const
{
    printSVGHeader(os, depth);
    printSVG(os, depth);
    printSVGFooter(os, depth);
}
inline void Drawable::printSVG(std::ostream & os, int depth) const
{}

} // namespace tools
} // namespace rpct

#endif // rpct_tools_Drawable_inl_h_
