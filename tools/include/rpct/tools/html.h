/** Filip Thyssen */

#ifndef rpct_tools_html_h_
#define rpct_tools_html_h_

#include <ostream>
#include <string>

namespace rpct {
namespace tools {
namespace html {

void header(std::ostream & os, const std::string & title, const std::string & subtitle, bool topleft = false);
void footer(std::ostream & os);

} // namespace html
} // namespace tools
} // namespace rpct

#endif // rpct_tools_html_h_
