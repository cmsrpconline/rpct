/** Filip Thyssen */

#ifndef rpct_tools_Publisher_h_
#define rpct_tools_Publisher_h_

namespace rpct {
namespace tools {

class Publisher
{
public:
    Publisher();
    virtual ~Publisher();

    virtual void publish(int id, int type, double value) = 0;

    // publisher usage can be optimized if the reciever is synced with the sender
    virtual bool synced() const;
};

} // namespace tools
} // namespace rpct

#include "rpct/tools/Publisher-inl.h"

#endif // rpct_tools_Publisher_h_
