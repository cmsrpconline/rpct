/** Filip Thyssen */

#ifndef rpct_tools_Printable_inl_h_
#define rpct_tools_Printable_inl_h_

#include "rpct/tools/Printable.h"

#include <sstream>

namespace rpct {
namespace tools {

inline void Parameters::clear()
{
    parameters_.clear();
    keylength_ = 0;
}

template<typename T>
inline void Parameters::add(const std::string & key, const T & value)
{
    std::stringstream temp;
    temp << value;
    parameters_.push_back(parameter_type(key, temp.str()));
    unsigned int length = key.length();
    if (keylength_ < length)
        keylength_ = length;
}
template<typename T>
inline void Parameters::hexadd(const std::string & key, const T & value)
{
    std::stringstream temp;
    temp << std::hex << std::showbase << (int)value << std::dec;
    parameters_.push_back(parameter_type(key, temp.str()));
    unsigned int length = key.length();
    if (keylength_ < length)
        keylength_ = length;
}

inline Parameters::size_type Parameters::size() const
{
    return parameters_.size();
}
inline Parameters::const_iterator Parameters::begin() const
{
    return parameters_.begin();
}
inline Parameters::const_iterator Parameters::end() const
{
    return parameters_.end();
}
inline Parameters::iterator Parameters::begin()
{
    return parameters_.begin();
}
inline Parameters::iterator Parameters::end()
{
    return parameters_.end();
}

inline void Printable::print(std::ostream & os) const
{
    Parameters parameters;
    printParameters(parameters);
    parameters.print(os);
}
inline void Printable::printHTML(std::ostream & os) const
{
    Parameters parameters;
    printParameters(parameters);
    if (parameters.size() > 0)
        parameters.printHTML(os);
    else
        {
            os << "<pre class=\"parameters\">" << std::endl;
            print(os);
            os << "</pre>" << std::endl;
        }
}

} // namespace tools
} // namespace rpct

#endif // rpct_tools_Printable_inl_h_
