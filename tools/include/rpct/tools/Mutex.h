/** Filip Thyssen */

#ifndef rpct_tools_Mutex_h_
#define rpct_tools_Mutex_h_

#include <sys/time.h> // timeval
#include <time.h>     // timespec
#include <stdint.h>   // size_t

#include <pthread.h>

namespace rpct {
namespace tools {

/** A simple mutex */
class Mutex
{
public:
    /** Constructor
     * \param value the initial free places, 0 or 1
     * \param recursive defines if a thread can lock more than once (requires the same number of unlocks)
     */
    Mutex(size_t value = 1, bool recursive = false);
    virtual ~Mutex();

    /** take the mutex */
    int take();
    /** try to take the mutex, returns 0 upon success */
    int try_take();
    /** try to take the mutex
     * \timeout a timeval specifying the time to wait before giving up
     */
    int take(timeval const & timeout);
    /** try to take the mutex
     * \param sec  the seconds of timeval tval in the previous constructor
     * \param usec the microseconds of timeval tval in the previous constructor
     */
    int take(time_t sec, suseconds_t usec = 0);
    /** try to take the mutex
     * \endtime a timespec specifying the moment to give up
     */
    int take(timespec const & endtime);    
    /** give the mutex */
    int give();

    /** returns the number of available places: 0 or 1 */
    size_t value() const;

protected:
    pthread_mutexattr_t attr_;
    pthread_mutex_t mutex_;
    volatile size_t value_;
};

template<typename T>
class TLock
{
public:
    TLock(T & lmutex) throw (int);
    TLock(T & lmutex, timeval const & timeout) throw (int);
    TLock(T & lmutex, time_t sec, suseconds_t usec = 0) throw (int);
    TLock(T & lmutex, timespec const & endtime) throw (int);
    TLock(TLock<T> const & tlock);

    ~TLock();

    void keep();
    void unkeep();

    int give();
    int take();
protected:
    T & lmutex_;
    mutable int e_;
};

typedef TLock<Mutex> Lock;

template<typename T, typename Obj>
class TLockPointer
{
public:
    TLockPointer(Obj & lobject, T & lmutex) throw (int);
    TLockPointer(Obj & lobject, T & lmutex, timeval const & timeout) throw (int);
    TLockPointer(Obj & lobject, T & lmutex, time_t sec, suseconds_t usec = 0) throw (int);
    TLockPointer(Obj & lobject, T & lmutex, timespec const & endtime) throw (int);
    TLockPointer(TLockPointer<T, Obj> const & lptr);

    ~TLockPointer();

    void keep();
    void unkeep();

    int give();
    int take();

    Obj & operator*();
    Obj * operator->();
protected:
    Obj & lobject_;
    T & lmutex_;
    mutable int e_;
};

} // namespace tools
} // namespace rpct

#include "rpct/tools/Mutex-inl.h"

#endif // rpct_tools_Mutex_h_
