/** Filip Thyssen */

#ifndef rpct_tools_squeue_inl_h_
#define rpct_tools_squeue_inl_h_

#include <unistd.h>

#include "rpct/tools/squeue.h"
#include "rpct/tools/RunningTimeVal.h"

namespace rpct {
namespace tools {

template <typename Object>
inline Object squeue<Object>::pop_front() throw (exception::squeue_error)
{
    return pop();
}

template <typename Object>
inline Object squeue<Object>::wait_pop_front() throw (exception::squeue_error)
{
    return wait_pop();
}

template <typename Object>
inline Object squeue<Object>::wait_pop_front(timeval const & timeout) throw (exception::squeue_error)
{
    return wait_pop(timeout);
}

template <typename Object>
inline Object squeue<Object>::wait_pop_front(time_t sec, suseconds_t usec) throw (exception::squeue_error)
{
    return wait_pop(sec, usec);
}

template <typename Object>
inline Object squeue<Object>::wait_pop_front(timespec const & endtime) throw (exception::squeue_error)
{
    return wait_pop(endtime);
}

template <typename Object>
inline squeue<Object>::squeue(size_t max_size, const std::string & name, ObjectActionSignature<Object &> * end_function)
    : name_(name)
    , end_function_(end_function)
    , max_size_(max_size)
    , freeing_(false)
{}

template <typename Object>
inline squeue<Object>::~squeue()
{
    if (end_function_)
        clear();
    free();
}

template <typename Object>
inline const std::string & squeue<Object>::name() const
{
    Lock clock(q_is_empty_);
    return name_;
}

template <typename Object>
inline size_t squeue<Object>::max_size() const
{
    Lock clock(q_is_empty_);
    return max_size_;
}

template <typename Object>
inline void squeue<Object>::max_size(size_t max_size)
{
    Lock clock(q_is_empty_);
    max_size_ = max_size;
}


template <typename Object>
inline void squeue<Object>::remove(const Object & element, bool end_function) throw (exception::squeue_error)
{
    Lock clock(q_is_empty_);
    typename std::deque<Object>::iterator position = find(queue_.begin(), queue_.end(), element);
    if (position != queue_.end())
        {
	    if (end_function && end_function_)
                end_function_->invoke(queue_[position]);
	    queue_.erase(position);
        }
    else
        {
	    std::string msg = "element not found in squeue " + name_;
	    throw exception::squeue_error(msg);
        }
}

template <typename Object>
inline bool squeue<Object>::empty()
{
    Lock clock(q_is_empty_);
    return queue_.empty();
}

template <typename Object>
inline Object squeue<Object>::pop() throw (exception::squeue_error)
{
    Lock clock(q_is_empty_);
    if (queue_.empty())
        {
	    std::string msg = "no element available in squeue " + name_;
	    throw exception::squeue_error(msg);
        }
    Object element = queue_.front();
    queue_.pop_front();
    return element;
}

template <typename Object>
inline Object squeue<Object>::pop_back() throw (exception::squeue_error)
{
    Lock clock(q_is_empty_);
    if (queue_.empty())
        {
	    std::string msg = "no element available in squeue " + name_;
	    throw exception::squeue_error(msg);
        }
    Object element = queue_.back();
    queue_.pop_back();
    return element;
}

template <typename Object>
inline void squeue<Object>::push(const Object & element) throw (exception::squeue_error)
{
    Lock clock(q_is_empty_);
    if (!max_size_ || (queue_.size() < max_size_ && queue_.size() < queue_.max_size()))
        {
            queue_.push_back(element);
            if (!freeing_)
                q_is_empty_.free_one(false);
        }
    else
        throw exception::squeue_error("Could not add an other element to the squeue, maximum size reached.");
}

template <typename Object>
inline void squeue<Object>::push_back(const Object & element) throw (exception::squeue_error)
{
    push(element);
}

template <typename Object>
inline void squeue<Object>::push_front(const Object & element) throw (exception::squeue_error)
{
    Lock clock(q_is_empty_);
    if (!max_size_ || (queue_.size() < max_size_ && queue_.size() < queue_.max_size()))
        {
            queue_.push_front(element);
            if (!freeing_)
                q_is_empty_.free_one(false);
        }
    else
        throw exception::squeue_error("Could not add an other element to the squeue, maximum size reached.");
}

template <typename Object>
inline size_t squeue<Object>::size()
{
    Lock clock(q_is_empty_);
    return queue_.size();
}

template <typename Object>
inline Object squeue<Object>::wait_pop() throw (exception::squeue_error)
{
    bool freeing(0);
    do {
        Lock clock(q_is_empty_);
        if (!queue_.empty())
            {
                Object element = queue_.front();
                queue_.pop_front();
                return element;
            }
        if (!(freeing = freeing_))
            {
                q_is_empty_.wait(false);
                freeing = freeing_;
            }
    } while (!freeing);
    throw exception::squeue_error("free() called during wait_pop.");
}

template <typename Object>
inline Object squeue<Object>::wait_pop_back() throw (exception::squeue_error)
{
    bool freeing(0);
    do {
        Lock clock(q_is_empty_);
        if (!queue_.empty())
            {
                Object element = queue_.back();
                queue_.pop_back();
                return element;
            }
        if (!(freeing = freeing_))
            {
                q_is_empty_.wait(false);
                freeing = freeing_;
            }
    } while (!freeing);
    throw exception::squeue_error("free() called during wait_pop_back.");
}

template <typename Object>
inline Object squeue<Object>::wait_pop(timeval const & timeout) throw (exception::squeue_error)
{
    timespec endtime = timeval_to_abs_timespec(timeout);
    return wait_pop(endtime);
}

template <typename Object>
inline Object squeue<Object>::wait_pop(time_t sec, suseconds_t usec) throw (exception::squeue_error)
{
    timeval timeout;
    timeval_fill(timeout, sec, usec);
    timespec endtime = timeval_to_abs_timespec(timeout);
    return wait_pop(endtime);
}

template <typename Object>
inline Object squeue<Object>::wait_pop(timespec const & endtime) throw (exception::squeue_error)
{
    int e(0);
    bool freeing(0);
    do {
        try {
            Lock clock(q_is_empty_, endtime);
            if (!queue_.empty())
                {
                    Object element = queue_.front();
                    queue_.pop_front();
                    return element;
                }
            if (!(freeing = freeing_))
                {
                    e = q_is_empty_.wait(endtime, false);
                    freeing = freeing_;
                }
        } catch(int & _e) {
            e = _e;
        }
    } while (!freeing && !e);
    if (freeing)
        throw exception::squeue_error("free() called during wait_pop.");
    else
        throw exception::squeue_timeout_error("timeout during wait_pop.");
}

template <typename Object>
inline Object squeue<Object>::wait_pop_back(timeval const & timeout) throw (exception::squeue_error)
{
    timespec endtime = timeval_to_abs_timespec(timeout);
    return wait_pop_back(endtime);
}

template <typename Object>
inline Object squeue<Object>::wait_pop_back(time_t sec, suseconds_t usec) throw (exception::squeue_error)
{
    timeval timeout;
    timeval_fill(timeout, sec, usec);
    timespec endtime = timeval_to_abs_timespec(timeout);
    return wait_pop_back(endtime);
}

template <typename Object>
inline Object squeue<Object>::wait_pop_back(timespec const & endtime) throw (exception::squeue_error)
{
    int e(0);
    bool freeing(0);
    do {
        try {
            Lock clock(q_is_empty_, endtime);
            if (!queue_.empty())
                {
                    Object element = queue_.back();
                    queue_.pop_back();
                    return element;
                }
            if (!(freeing = freeing_))
                {
                    e = q_is_empty_.wait(endtime, false);
                    freeing = freeing_;
                }
        } catch (int & _e) {
            e = _e;
        }
    } while (!freeing && !e);
    if (freeing)
        throw exception::squeue_error("free() called during wait_pop_back.");
    else
        throw exception::squeue_timeout_error("timeout during wait_pop_back.");
}

template <typename Object>
inline void squeue<Object>::clear()
{
    Lock clock(q_is_empty_);
    if (end_function_)
        while(!queue_.empty())
	    {
                end_function_->invoke(queue_.front());
                queue_.pop_front();
	    }
    else
        queue_.clear();
}

template <typename Object>
inline void squeue<Object>::free()
{
    {
        Lock clock(q_is_empty_);
        freeing_ = true;
        q_is_empty_.free_all(false);
    }
    // all previously waiting pops should get the mutex first
    while (q_is_empty_.nreleases())
        usleep(1000);
    Lock clock(q_is_empty_);
    freeing_ = false;
}

} // namespace tools
} // namespace rpct

#endif // rpct_tools_squeue_inl_h_
