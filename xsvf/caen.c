#include <CAENVMEtypes.h>
#include <CAENVMEoslib.h>
#include <CAENVMElib.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
//#include "ports.h"
#include "jtag.h"
#include <stdio.h>
#include <sched.h>

CVBoardTypes board_type=cvV2718;
long caen_handle=0;
extern int jtag_hardware_initialized; // Was type BOOL in jamstub.c!!!
extern int dcc_number;
extern int jtag_output_chain;
unsigned int jtag_addr=0xc0000000;
unsigned long int jtag_reg = (1<<JTG_ENA) | (1<<JTG_TRST);
int jam_jtag_io(int tms, int tdi, int read_tdo);
int low_jtag_io(int tms, int tdi, int read_tdo);
void flush_vme_cache(void);
void waitTime(long microsec);
void waitNano(long nanosec);

void caen_write(unsigned long int address, unsigned long int value)
{
  CVErrorCodes res;
  res=CAENVME_WriteCycle(caen_handle,address,&value,0x9,cvD32);
  if(res != cvSuccess) {
    fprintf(stderr,"CAENVME access error in caen_write: %d\n",res);
    exit(1);
  }
}

unsigned long int caen_read(unsigned long int address)
{
  CVErrorCodes res;
  unsigned long int value;
  res=CAENVME_ReadCycle(caen_handle,address,&value,0x9,cvD32);
  if(res != cvSuccess) {
    fprintf(stderr,"CAENVME access error in caen_READ: %d , addr=%lx\n",res,address);
    exit(1);
  }
  return value;
}

void jtag_scan_ir(unsigned char data)
{
  int i;
  unsigned char tdi=0;
  //We send the 8 bits from data
  //At the entry we are in the Select-DR-Scan state
  //Go to shift IR
  low_jtag_io(1,tdi,0); // go to Select-IR-Scan
  low_jtag_io(0,tdi,0); // go to Capture-IR
  low_jtag_io(0,tdi,0); // go Shift-IR
  for(i=0;i<8;i++) {
    tdi=data & 1;
    data >>= 1;
    low_jtag_io(i==7 ? 1 : 0 ,tdi, 0);
  }
  //We are in the Exit-IR state
  low_jtag_io(1,tdi,0); // go to Update-IR state
  low_jtag_io(1,tdi,0); // go to Select-DR-Scan state  
  //At the return we are also in the Select-DR-Scan state
  return;
}
void caen_init(void)
{
  unsigned char scansta_addr=0, scansta_chain=0;
  int i;
  CVErrorCodes res;
  /*
    struct sched_param schedp;
    schedp.sched_priority=99;  
    fprintf(stderr,"Set scheduling policy\n");
    if(sched_setscheduler(0, SCHED_RR, &schedp)) {
    fprintf(stderr,"Error setting the scheduling policy :-(\n");
    exit(1);
    };
  */
  fprintf(stderr,"CAENinit\n");
  if((dcc_number<0) || (dcc_number>31)) {
    fprintf(stderr,"Wrong DCC number: %d (must be between 0 and 31)\n",dcc_number);
    exit(1);
  }
  if((jtag_output_chain<1) || (jtag_output_chain>2)) {
    fprintf(stderr,"Wrong JTAG chain number: %d (must be between 1 and 2)\n",jtag_output_chain);
    exit(1);
  }
  scansta_addr=31-dcc_number; //(the address bits GA are negated!!!)
  scansta_chain=0xa0+jtag_output_chain;
  jtag_addr=0xc0ffffe0+dcc_number*0x1000000;
  res=CAENVME_Init(board_type,0,0,&caen_handle);
  if(res != cvSuccess) {
    fprintf(stderr,"CAENVME access error: %d\n",res);
    exit(1);
  }    
  fprintf(stderr,"JTAG reg: %lx\n",caen_read(jtag_addr));
  // We do reset JTAG!!!
  caen_write(jtag_addr,1<< JTG_ENA);
  fprintf(stderr,"JTAG reg: %lx\n",caen_read(jtag_addr));
  caen_write(jtag_addr,(1 << JTG_ENA) | (1 << JTG_TRST) );
  fprintf(stderr,"JTAG reg: %lx\n",caen_read(jtag_addr));
  //Now we can start with the setting of SCANSTA111
  //First soft reset of the TAP (well, it is not necessare, but I'm a little paranoid ;-) )
  for (i=0;i<10;i++) 
    low_jtag_io(1,0,0);
  low_jtag_io(0,0,0); // go to Run-Test-Idle
  low_jtag_io(1,0,0); // go to Select-DR-Scan
  jtag_scan_ir(scansta_addr); //Send 8-bits to IR, goo to Select-DR-Scan
  jtag_scan_ir(scansta_chain); //Send 8-bits to IR, goo to Select-DR-Scan
  //jam_jtag_io(?,0,0); // go to Run-Test-Idle
  //Reset the connected chain (SCANSTA111 does not get RESET, it needs the TRST reset!!!)
  for (i=0;i<10;i++) 
    low_jtag_io(1,0,0);
}

void caen_release(void)
{
  CAENVME_End(caen_handle);
}

void initialize_jtag_hardware()
{
  caen_init();
}
void close_jtag_hardware()
{
  //First we flush the VME cache
  flush_vme_cache();
  //Then we wait for a few miliseconds
  waitTime(1000);
  //And finally we set the reset line in active state
  caen_write(jtag_addr,(1 << JTG_ENA) );
  //Again we wait for a few miliseconds
  waitTime(1000);
  //And finally we release the caen
  caen_release();
}



/* read the TDO bit and store it in val */
unsigned char readTDOBit()
{
  unsigned long val;
  val=caen_read(jtag_addr);
  return val & (1<<JTG_TDO) ? 1 : 0;
}

//Buffers used for caching of write accesses
#define VME_CACHE_LEN (1024)
unsigned long addr[VME_CACHE_LEN];
unsigned long data[VME_CACHE_LEN];
CVAddressModifier am[VME_CACHE_LEN];
CVDataWidth dw[VME_CACHE_LEN];
CVErrorCodes ec[VME_CACHE_LEN];

//Pointer used to cache 
int vme_cache_cnt = 0;

void flush_vme_cache()
{
  CVErrorCodes res;
  if(vme_cache_cnt) {
    res=CAENVME_MultiWrite(caen_handle, addr, data,vme_cache_cnt,am,dw,ec);
    if(res != cvSuccess) {
      fprintf(stderr,"CAENVME access error in caen_multi_write: %d\n",res);
      exit(1);
    }
    vme_cache_cnt = 0;
  }
}

void vme_cache_write(unsigned long val)
{
  if(vme_cache_cnt>=VME_CACHE_LEN) flush_vme_cache();
  addr[vme_cache_cnt]=jtag_addr;
  data[vme_cache_cnt]=val;
  am[vme_cache_cnt]=0x9;
  dw[vme_cache_cnt]=cvD32;
  ec[vme_cache_cnt]=0;
  vme_cache_cnt++;
}

static void buffer_sync(struct udata_s)
{
  flush_vme_cache();
}

static void buffer_add(struct udata_s *u, int tms, int tdi, int tdo, int rmask)
{
  if(tms) {
    jtag_reg |= (1 << JTG_TMS);
  } else {
    jtag_reg &= 0xffffffff ^ (1 << JTG_TMS);
  }
  if(tdi==1) {
    jtag_reg |= (1 << JTG_TDI);
  } else if(tdi==0) {
    jtag_reg &= 0xffffffff ^ (1 << JTG_TDI);
  }
  jtag_reg &= 0xffffffff ^ (1 << JTG_TCK);
  vme_cache_write(jtag_reg);
  jtag_reg |= (1 << JTG_TCK);
  vme_cache_write(jtag_reg);
  if(tdo>=0) { //We have to check the TDO
    flush_vme_cache();
    u->last_tdo = readTDOBit();
    if (u->last_tdo != tdo) u->error_rc = -1;
  }
}

// W zasadzie to powyższe funkcje w pełni rozwiązują sprawę implementacji...
// Muszę tylko jakoś przekazać tu deklarację struktury udata_s
// Muszę też udostępnić inicjalizację sprzętu caenowego...

//More advanced version of the low_jtag_io - all write requests are cached, until read
//request is issued,
//the cache is emptied when 1) read of TDO is requested, 2) jam_delay is called
//3) the cache is full

int jam_jtag_io(int tms, int tdi, int read_tdo)
{
  int tdo=0;
  if (!jtag_hardware_initialized)
    {
      fprintf(stderr,"JTAG not initialized yet...\n");
      initialize_jtag_hardware();
      jtag_hardware_initialized = 1; //we must set it in advance
    }
  return low_jtag_io(tms, tdi, read_tdo); 
}

int jam_vector_io
(
 int signal_count,
 long *dir_vect,
 long *data_vect,
 long *capture_vect
 )
{
  fprintf(stderr,"VECTOR functions not implemented!!!\n");
  exit(1);
}

void waitTime(long microsec)
{
  waitNano(microsec*1000);
}

void waitNano(long nanosec)
{
  struct timespec rec;
  int res;
  rec.tv_sec=0;
  rec.tv_nsec=nanosec;
  for(;;) {
    res=nanosleep(&rec, &rec);
    if (res==0) break;
    else {
      if (errno==EINVAL){
	fprintf(stderr,"nanosleep error\n",res);
	exit(1);
      }
    }
  }
}
void jam_delay(long microseconds)
{
  if(jtag_hardware_initialized) flush_vme_cache();
  //usleep(microseconds);
  waitNano(1000*microseconds);
}
