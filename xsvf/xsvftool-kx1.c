/*
 *  Lib(X)SVF  -  A library for implementing SVF and XSVF JTAG players
 *
 *  Copyright (C) 2009  RIEGL Research ForschungsGmbH
 *  Copyright (C) 2009  Clifford Wolf <clifford@clifford.at>
 *  
 *  Permission to use, copy, modify, and/or distribute this software for any
 *  purpose with or without fee is hereby granted, provided that the above
 *  copyright notice and this permission notice appear in all copies.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 *
 *  A JTAG SVF/XSVF Player based on libxsvf for the FTDI FT232H, FT2232H and
 *  FT4232H High Speed USB to Multipurpose UART/FIFO ICs.
 *
 *  This also serves as an example program for using libxvsf with asynchonous
 *  hardware interfaces. Have a look at 'xsvftool-gpio.c' for a simple libxsvf
 *  example for synchonous interfaces (such as register mapped GPIOs).
 *
 *  IMPORTANT NOTE: You need libftdi [1] (version 0.16 or newer) installed
 *  to build this program.
 *
 *  To run it at full speed you need a version of libftdi that has been
 *  compiled with '--with-async-mode', and must enable all four defines
 *  BLOCK_WRITE, ASYNC_WRITE, BACKGROUND_READ and INTERLACED_READ_WRITE below.
 *  [1] http://www.intra2net.com/en/developer/libftdi/
 */

//#define uint8_t unsigned char

#include "libxsvf.h"
#include <ftdi.h>

#include <sys/time.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <errno.h>
#include <math.h>
#include <netinet/in.h>

#define BUFFER_SIZE (4096)
#define buffer_sync(x) buffer_flush(x)

static FILE *dumpfile = NULL;

struct buffer_s {
	unsigned int tms:1;
	unsigned int tdi:1;
	unsigned int tdi_enable:1;
	unsigned int tdo:1;
	unsigned int tdo_enable:1;
	unsigned int rmask:1;
};

struct udata_s {
	FILE *f;
	struct ftdi_context ftdic;
	int buffer_size;
	uint8_t buffer_out[BUFFER_SIZE*2];
	uint8_t buffer_in[BUFFER_SIZE];
	int buffer_i;
	int last_tdo;
	int error_rc;
	int verbose;
	int syncmode;
	int forcemode;
	int frequency;
	int retval_i;
	int retval[256];

};

void error_exit(char * msg)
{
    fprintf(stderr, msg);
     exit(1);
}

int cr2_blk_write(struct ftdi_context * ctx, uint16_t num, uint16_t * adr_val)
{
    int clen=1+5*num;
    unsigned char cmd[clen]; //This is the length of the command
    int i=0;
    int c=0;
    int res;
    cmd[c++]=0xa0;
    for (i=0;i<num;i++) {
        unsigned int adr=adr_val[2*i];
        unsigned int val=adr_val[2*i+1];
        if (i==num-1)
            cmd[c++]=0x10 | ((adr & 0xf000)>>12);
        else
            cmd[c++]=0x00 | ((adr & 0xf000)>>12);
        cmd[c++]=(adr & 0xfe0)>>5;
        cmd[c++]=((adr & 0x001f)<<2) | ((val & 0xc000)>>14);
        cmd[c++]=((val &0x3f80)>>7);
        cmd[c++]=(val & 0x007f);
    }
    //Command is ready, transfer it!
    res=ftdi_write_data(ctx,cmd,clen);
    if (res<0) {
        error_exit("Can't send command in cr2_blk_write\n");
    }
    while (1) {
        res=ftdi_read_data(ctx,cmd,1);
        if (res) break;
        else printf(".");
    }
    if (res<0) {
        error_exit("Can't read response in cr2_blk_write\n");
    }
    //Check the result...
    if ((cmd[0] & 0xfe)!=0x80) {
        printf("received : %x\n",(int) cmd[0]);
        error_exit("Wrong ACK in cr2_blk_write\n");
    }
    return cmd[0];
}

static void buffer_flush(struct udata_s *u)
{
  int res;
  int i;
  //printf("buffer flush. Len=%d\n",u->buffer_i);
  if(u->buffer_i) {
    //Mark the last set as end of buffer
    u->buffer_out[u->buffer_i-1]|=0x80;
    res=ftdi_write_data(&u->ftdic,u->buffer_out,u->buffer_i);
    //Read the response
    res=ftdi_read_data(&u->ftdic,u->buffer_in,u->buffer_i-1);
    if(res<u->buffer_i-1) { 
       error_exit("Can't read response in cr2_blk_read\n");
    }
    //Analyze the response
    for(i=0;i< u->buffer_i-1; i++) {
       //We look for an error in TDO
       uint8_t val = u->buffer_in[i];
       int tdo_received = (val & (1 << 0)) ? 1 : 0;
       if(val & (1<<4)) {
         //TDO was enabled
	 int tdo_expected = (val & (1 << 5)) ? 1 : 0;
         if (tdo_expected != tdo_received) 
 	    u->error_rc = -1;
       }
       u->last_tdo = tdo_received;
    }
    u->buffer_i = 0;
  }
}


static void buffer_add(struct udata_s *u, int tms, int tdi, int tdo, int rmask)
{
	uint8_t val = tms ? (1<<1) : 0;
        if(u->buffer_i == 0) {
          u->buffer_out[u->buffer_i]=0xa2;
          u->buffer_i ++;
        }
	if(tdi>=0) {
          val |= tdi ? (1<<2) : 0;
          val |= (1<<3);
        }
        if(tdo>=0) {
          val |= (1<<4);
          val |= tdo ? (1<<5) : 0;
        }
        val |= rmask ? (1<<6) : 0;
	u->buffer_out[u->buffer_i]=val;
        u->buffer_i ++ ;
	if (u->buffer_i >= BUFFER_SIZE)
		buffer_flush(u);
}



static int h_setup(struct libxsvf_host *h)
{
  int ret;
  struct udata_s *u = h->user_data;
  if (ftdi_init(&u->ftdic) < 0)
    {
      fprintf(stderr, "ftdi_init failed\n");
      exit(EXIT_FAILURE);
    }
  if ((ret = ftdi_set_interface(&u->ftdic,1))<0)
    {
      fprintf(stderr, "unable to open ftdi device: %d (%s)\n", ret, ftdi_get_error_string(&u->ftdic));
      exit(EXIT_FAILURE);
    }
  if ((ret = ftdi_usb_open(&u->ftdic, 0x0403, 0x6010)) < 0)
    {
      fprintf(stderr, "unable to open ftdi device: %d (%s)\n", ret, ftdi_get_error_string(&u->ftdic));
      exit(EXIT_FAILURE);
    }
  ftdi_usb_reset(&u->ftdic);
  if ((ret=ftdi_set_bitmode(&u->ftdic,0,BITMODE_MPSSE))<0)
    {
      fprintf(stderr, "unable to clear bitmode in ftdi device: %d (%s)\n", ret, ftdi_get_error_string(&u->ftdic));
      exit(EXIT_FAILURE);
    }
  if ((ret=ftdi_usb_purge_buffers(&u->ftdic))<0)
    {
      fprintf(stderr, "unable to purge buffers in ftdi device: %d (%s)\n", ret, ftdi_get_error_string(&u->ftdic));
      exit(EXIT_FAILURE);
    }
  //Set the GPIOH7 pin low and switch it to output
  printf("Trying to set pin LOW\n");
  ftdi_write_data(&u->ftdic,(uint8_t *)"\x82\x00\x80",3); //Reset the FPGA
  sleep(1);
  printf("Trying to set pin HIGH\n");
  ftdi_write_data(&u->ftdic,(uint8_t *)"\x82\xff\x80",3); //Reset the FPGA

  if ((ret=ftdi_set_bitmode(&u->ftdic,0,BITMODE_RESET))<0)
    {
      fprintf(stderr, "unable to clear bitmode in ftdi device: %d (%s)\n", ret, ftdi_get_error_string(&u->ftdic));
      exit(EXIT_FAILURE);
    }
  ftdi_write_data(&u->ftdic,(uint8_t *)"\xff",1); //Reset the FPGA
  sleep(2); //Wait until reset completes
  ftdi_write_data(&u->ftdic,(uint8_t *)"\xfe\xfd",2); //Wake UP the controller
  {
    uint16_t cmd[]={0x8000,0x0000,0xffe1,0x8400};
    cr2_blk_write(&u->ftdic,2,cmd);
  }
	u->last_tdo = -1;
	u->buffer_i = 0;
	u->error_rc = 0;
	u->retval_i = 0;
	return 0;
}

static int h_shutdown(struct libxsvf_host *h)
{
	struct udata_s *u = h->user_data;
	buffer_sync(u);
	ftdi_disable_bitbang(&u->ftdic);
	ftdi_usb_close(&u->ftdic);
	ftdi_deinit(&u->ftdic);
	return u->error_rc;
}


static void h_udelay(struct libxsvf_host *h, long usecs, int tms, long num_tck)
{
	struct udata_s *u = h->user_data;
	buffer_sync(u);
	if (num_tck > 0) {
		struct timeval tv1, tv2;
		gettimeofday(&tv1, NULL);
		while (num_tck > 0) {
			buffer_add(u, tms, -1, -1, 0);
			num_tck--;
		}
		buffer_sync(u);
		gettimeofday(&tv2, NULL);
		if (tv2.tv_sec > tv1.tv_sec) {
			usecs -= (1000000 - tv1.tv_usec) + (tv2.tv_sec - tv1.tv_sec - 1) * 1000000;
			tv1.tv_usec = 0;
		}
		usecs -= tv2.tv_usec - tv1.tv_usec;
	}
	if (usecs > 0) {
		usleep(usecs);
	}
}

static int h_getbyte(struct libxsvf_host *h)
{
	struct udata_s *u = h->user_data;
	return fgetc(u->f);
}

static int h_sync(struct libxsvf_host *h)
{
	struct udata_s *u = h->user_data;
	buffer_sync(u);
	int rc = u->error_rc;
	u->error_rc = 0;
	return rc;
}

static int h_pulse_tck(struct libxsvf_host *h, int tms, int tdi, int tdo, int rmask, int sync)
{
	struct udata_s *u = h->user_data;
	if (u->syncmode)
		sync = 1;
	buffer_add(u, tms, tdi, tdo, rmask);
	if (sync) {
		buffer_sync(u);
                //printf("TDO: %d\n",u->last_tdo);
		int rc = u->error_rc < 0 ? u->error_rc : u->last_tdo;
		u->error_rc = 0;
		return rc;
	}
	return u->error_rc < 0 ? u->error_rc : 1;
}

static int h_set_frequency(struct libxsvf_host *h, int v)
{
  fprintf(stderr, "WARNING: Setting JTAG clock frequency to %d ignored!\n", v);
  return 0;
}

static void h_report_tapstate(struct libxsvf_host *h)
{
	struct udata_s *u = h->user_data;
	if (u->verbose >= 2)
		printf("[%s]\n", libxsvf_state2str(h->tap_state));
}

static void h_report_device(struct libxsvf_host *h, unsigned long idcode)
{
	printf("idcode=0x%08lx, revision=0x%01lx, part=0x%04lx, manufactor=0x%03lx\n", idcode,
			(idcode >> 28) & 0xf, (idcode >> 12) & 0xffff, (idcode >> 1) & 0x7ff);
}

static void h_report_status(struct libxsvf_host *h, const char *message)
{
	struct udata_s *u = h->user_data;
	if (u->verbose >= 1)
		printf("[STATUS] %s\n", message);
}

static void h_report_error(struct libxsvf_host *h, const char *file, int line, const char *message)
{
	fprintf(stderr, "[%s:%d] %s\n", file, line, message);
}

static void *h_realloc(struct libxsvf_host *h, void *ptr, int size, enum libxsvf_mem which)
{
	return realloc(ptr, size);
}

static struct udata_s u = {
};

static struct libxsvf_host h = {
	.udelay = h_udelay,
	.setup = h_setup,
	.shutdown = h_shutdown,
	.getbyte = h_getbyte,
	.sync = h_sync,
	.pulse_tck = h_pulse_tck,
	.set_frequency = h_set_frequency,
	.report_tapstate = h_report_tapstate,
	.report_device = h_report_device,
	.report_status = h_report_status,
	.report_error = h_report_error,
	.realloc = h_realloc,
	.user_data = &u
};

const char *progname;

static void help()
{
	fprintf(stderr, "\n");
	fprintf(stderr, "A JTAG SVF/XSVF Player based on libxsvf for the FTDI FT232H, FT2232H and\n");
	fprintf(stderr, "FT4232H High Speed USB to Multipurpose UART/FIFO ICs.\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "xsvftool-ft2232h, part of Lib(X)SVF (http://www.clifford.at/libxsvf/).\n");
	fprintf(stderr, "Copyright (C) 2009  RIEGL Research ForschungsGmbH\n");
	fprintf(stderr, "Copyright (C) 2009  Clifford Wolf <clifford@clifford.at>\n");
	fprintf(stderr, "Lib(X)SVF is free software licensed under the ISC license.\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "Usage: %s [ -v[v..] ] [ -d dumpfile ] [ -L | -B ] [ -S ] [ -F ] \\\n", progname);
	fprintf(stderr, "      %*s [ -f freq[k|M] ] { -s svf-file | -x xsvf-file | -c } ...\n", (int)(strlen(progname)+1), "");
	fprintf(stderr, "\n");
	fprintf(stderr, "   -v\n");
	fprintf(stderr, "          Enable verbose output (repeat for incrased verbosity)\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "   -d dumpfile\n");
	fprintf(stderr, "          Write a logfile of all MPSSE comunication\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "   -L, -B\n");
	fprintf(stderr, "          Print RMASK bits as hex value (little or big endian)\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "   -S\n");
	fprintf(stderr, "          Run in synchronous mode (slow but report errors right away)\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "   -F\n");
	fprintf(stderr, "          Force mode (ignore all TDO mismatches)\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "   -f freq[k|M]\n");
	fprintf(stderr, "          Set maximum frequency in Hz, kHz or MHz\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "   -s svf-file\n");
	fprintf(stderr, "          Play the specified SVF file\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "   -x xsvf-file\n");
	fprintf(stderr, "          Play the specified XSVF file\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "   -c\n");
	fprintf(stderr, "          List devices in JTAG chain\n");
	fprintf(stderr, "\n");
	exit(1);
}

int main(int argc, char **argv)
{
	int rc = 0;
	int gotaction = 0;
	int hex_mode = 0;
	int opt, i, j;

	progname = argc >= 1 ? argv[0] : "xsvftool-ft232h";
	while ((opt = getopt(argc, argv, "vd:LBSFf:x:s:c")) != -1)
	{
		switch (opt)
		{
		case 'v':
			u.verbose++;
			break;
		case 'd':
			if (!strcmp(optarg, "-"))
				dumpfile = stdout;
			else
				dumpfile = fopen(optarg, "w");
			if (!dumpfile) {
				fprintf(stderr, "Can't open dumpfile `%s': %s\n", optarg, strerror(errno));
				rc = 1;
			}
			break;
		case 'f':
			u.frequency = strtol(optarg, &optarg, 10);
			while (*optarg != 0) {
				if (*optarg == 'k') {
					u.frequency *= 1000;
					optarg++;
					continue;
				}
				if (*optarg == 'M') {
					u.frequency *= 1000000;
					optarg++;
					continue;
				}
				if (optarg[0] == 'H' && optarg[1] == 'z') {
					optarg += 2;
					continue;
				}
				help();
			}
			break;
		case 'x':
		case 's':
			gotaction = 1;
			if (!strcmp(optarg, "-"))
				u.f = stdin;
			else
				u.f = fopen(optarg, "rb");
			if (u.f == NULL) {
				fprintf(stderr, "Can't open %s file `%s': %s\n", opt == 's' ? "SVF" : "XSVF", optarg, strerror(errno));
				rc = 1;
				break;
			}
			if (libxsvf_play(&h, opt == 's' ? LIBXSVF_MODE_SVF : LIBXSVF_MODE_XSVF) < 0) {
				fprintf(stderr, "Error while playing %s file `%s'.\n", opt == 's' ? "SVF" : "XSVF", optarg);
				rc = 1;
			}
			if (strcmp(optarg, "-"))
				fclose(u.f);
			break;
		case 'c':
			gotaction = 1;
			int old_frequency = u.frequency;
			if (u.frequency == 0)
				u.frequency = 10000;
			if (libxsvf_play(&h, LIBXSVF_MODE_SCAN) < 0) {
				fprintf(stderr, "Error while scanning JTAG chain.\n");
				rc = 1;
			}
			u.frequency = old_frequency;
			break;
		case 'L':
			hex_mode = 1;
			break;
		case 'B':
			hex_mode = 2;
			break;
		case 'S':
			if (u.frequency == 0)
				u.frequency = 10000;
			u.syncmode = 1;
			break;
		case 'F':
			u.forcemode = 1;
			break;
		default:
			help();
			break;
		}
	}

	if (!gotaction)
		help();

	if (u.retval_i) {
		if (hex_mode) {
			printf("0x");
			for (i=0; i < u.retval_i; i+=4) {
				int val = 0;
				for (j=i; j<i+4; j++)
					val = val << 1 | u.retval[hex_mode > 1 ? j : u.retval_i - j - 1];
				printf("%x", val);
			}
		} else {
			printf("%d rmask bits:", u.retval_i);
			for (i=0; i < u.retval_i; i++)
				printf(" %d", u.retval[i]);
		}
		printf("\n");
	}

	return rc;
}

