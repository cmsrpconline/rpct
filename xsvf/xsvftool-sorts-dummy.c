/*
 *  Lib(X)SVF  -  A library for implementing SVF and XSVF JTAG players
 *
 *  Copyright (C) 2009  RIEGL Research ForschungsGmbH
 *  Copyright (C) 2009  Clifford Wolf <clifford@clifford.at>
 *  
 *  Permission to use, copy, modify, and/or distribute this software for any
 *  purpose with or without fee is hereby granted, provided that the above
 *  copyright notice and this permission notice appear in all copies.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 *
 *  A JTAG SVF/XSVF Player based on libxsvf for the FTDI FT232H, FT2232H and
 *  FT4232H High Speed USB to Multipurpose UART/FIFO ICs.
 *
 *  This also serves as an example program for using libxvsf with asynchonous
 *  hardware interfaces. Have a look at 'xsvftool-gpio.c' for a simple libxsvf
 *  example for synchonous interfaces (such as register mapped GPIOs).
 *
 *  IMPORTANT NOTE: You need libftdi [1] (version 0.16 or newer) installed
 *  to build this program.
 *
 *  To run it at full speed you need a version of libftdi that has been
 *  compiled with '--with-async-mode', and must enable all four defines
 *  BLOCK_WRITE, ASYNC_WRITE, BACKGROUND_READ and INTERLACED_READ_WRITE below.
 *  [1] http://www.intra2net.com/en/developer/libftdi/
 */

//#define uint8_t unsigned char

#include "libxsvf.h"

#include <sys/time.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <errno.h>
#include <math.h>
#include <netinet/in.h>
#include <time.h>
#include <sched.h>

struct udata_s {
  FILE *f;
  int last_tdo;
  int error_rc;
  int verbose;
  int syncmode;
  int forcemode;
  int frequency;
  int retval_i;
  int retval[256];

};

/*
 *
 * CAEN related parts of the programming software
 *
 */
#define LINUX
#include "CAENVMEtypes.h"
#include "CAENVMEoslib.h"
#include "CAENVMElib.h"
#include "jtag.h"
CVBoardTypes board_type=cvV2718;
int32_t caen_handle=0;
int dcc_number=1;
int jtag_output_chain=1;
unsigned int jtag_addr=0xc0000000;
unsigned long int jtag_reg = (1<<JTG_ENA) | (1<<JTG_TRST);
void flush_vme_cache(void);
unsigned char readTDOBit();
void vme_cache_write(unsigned long val);
void waitTime(long microsec);
void waitNano(long nanosec);

void caen_write(unsigned long int address, unsigned long int value)
{
	//cout<<"CAENVME_WriteCycle: address "<<address<<" value "<<value<<endl;
	fprintf(stderr,"CAENVME_WriteCycle: %x\n",value);
/*  CVErrorCodes res;
  res=CAENVME_WriteCycle(caen_handle,address,&value,0x9,cvD32);
  if(res != cvSuccess) {
    fprintf(stderr,"CAENVME access error in caen_write: %d\n",res);
    exit(1);
  }*/
}

unsigned long int caen_read(unsigned long int address)
{
	//cout<<"CAENVME_ReadCycle: address "<<address<<endl;
	fprintf(stderr,"CAENVME_ReadCycle: address: %x\n",address);
	return 0;
/*  CVErrorCodes res;
  unsigned long int value;
  res=CAENVME_ReadCycle(caen_handle,address,&value,0x9,cvD32);
  if(res != cvSuccess) {
    fprintf(stderr,"CAENVME access error in caen_READ: %d , addr=%lx\n",res,address);
    exit(1);
  }
  return value;*/
}

int low_jtag_io(int tms, int tdi, int read_tdo)
{
  printf("low_jtag_io TMS:%d TDI:%d read_tdo:%d\n",tms,tdi,read_tdo);
  int tdo=0;
  if(tms) {
    jtag_reg |= (1 << JTG_TMS);
  } else {
    jtag_reg &= 0xffffffff ^ (1 << JTG_TMS);
  }
  if(tdi) {
    jtag_reg |= (1 << JTG_TDI);
  } else {
    jtag_reg &= 0xffffffff ^ (1 << JTG_TDI);
  }
  if(read_tdo) {
    //read is requested, flush the cached writes
    flush_vme_cache();
    caen_write(jtag_addr,jtag_reg);
    tdo = readTDOBit();
    //printf("TDO: %d\n",tdo);
  } else {
    vme_cache_write(jtag_reg);
  }
  jtag_reg |= (1 << JTG_TCK);
  vme_cache_write(jtag_reg);
  jtag_reg &= 0xffffffff ^ (1 << JTG_TCK);
  vme_cache_write(jtag_reg);
  return tdo;
}

void jtag_scan_ir(unsigned char data)
{
  int i;
  unsigned char tdi=0;
  //We send the 8 bits from data
  //At the entry we are in the Select-DR-Scan state
  //Go to shift IR
  low_jtag_io(1,tdi,0); // go to Select-IR-Scan
  low_jtag_io(0,tdi,0); // go to Capture-IR
  low_jtag_io(0,tdi,0); // go Shift-IR
  for(i=0;i<8;i++) {
    tdi=data & 1;
    data >>= 1;
    low_jtag_io(i==7 ? 1 : 0 ,tdi, 0);
  }
  //We are in the Exit-IR state
  low_jtag_io(1,tdi,0); // go to Update-IR state
  low_jtag_io(1,tdi,0); // go to Select-DR-Scan state  
  //At the return we are also in the Select-DR-Scan state
  return;
}
void caen_init(void)
{
  unsigned char scansta_addr=0, scansta_chain=0;
  int i;
  CVErrorCodes res;
  /*
    struct sched_param schedp;
    schedp.sched_priority=99;  
    fprintf(stderr,"Set scheduling policy\n");
    if(sched_setscheduler(0, SCHED_RR, &schedp)) {
    fprintf(stderr,"Error setting the scheduling policy :-(\n");
    exit(1);
    };
  */
  fprintf(stderr,"CAENinit\n");
  if((dcc_number<0) || (dcc_number>31)) {
    fprintf(stderr,"Wrong DCC number: %d (must be between 0 and 31)\n",dcc_number);
    exit(1);
  }
  if((jtag_output_chain<1) || (jtag_output_chain>2)) {
    fprintf(stderr,"Wrong JTAG chain number: %d (must be between 1 and 2)\n",jtag_output_chain);
    exit(1);
  }
  scansta_addr=31-dcc_number; //(the address bits GA are negated!!!)
  scansta_chain=0xa0+jtag_output_chain;
  jtag_addr=0xc0ffffe0+dcc_number*0x1000000;
/*  res=CAENVME_Init(board_type,0,0,&caen_handle);
  if(res != cvSuccess) {
    fprintf(stderr,"CAENVME access error: %d\n",res);
    exit(1);
  } */
  fprintf(stderr,"JTAG reg: %lx\n",caen_read(jtag_addr));
  // We do reset JTAG!!!
  caen_write(jtag_addr,1<< JTG_ENA);
  fprintf(stderr,"JTAG reg: %lx\n",caen_read(jtag_addr));
  caen_write(jtag_addr,(1 << JTG_ENA) | (1 << JTG_TRST) );
  fprintf(stderr,"JTAG reg: %lx\n",caen_read(jtag_addr));
  //Now we can start with the setting of SCANSTA111
  //First soft reset of the TAP (well, it is not necessare, but I'm a little paranoid ;-) )
  for (i=0;i<10;i++) 
    low_jtag_io(1,0,0);
  low_jtag_io(0,0,0); // go to Run-Test-Idle
  low_jtag_io(1,0,0); // go to Select-DR-Scan
  jtag_scan_ir(scansta_addr); //Send 8-bits to IR, goo to Select-DR-Scan
  jtag_scan_ir(scansta_chain); //Send 8-bits to IR, goo to Select-DR-Scan
  //jam_jtag_io(?,0,0); // go to Run-Test-Idle
  //Reset the connected chain (SCANSTA111 does not get RESET, it needs the TRST reset!!!)
  for (i=0;i<10;i++) 
    low_jtag_io(1,0,0);
}

void caen_release(void)
{
  //CAENVME_End(caen_handle);
}

void initialize_jtag_hardware()
{
  caen_init();
}
void close_jtag_hardware()
{
  //First we flush the VME cache
  flush_vme_cache();
  //Then we wait for a few miliseconds
  waitTime(1000);
  //And finally we set the reset line in active state
  caen_write(jtag_addr,(1 << JTG_ENA) );
  //Again we wait for a few miliseconds
  waitTime(1000);
  //And finally we release the caen
  caen_release();
}



/* read the TDO bit and store it in val */
unsigned char readTDOBit()
{
  fprintf(stderr,"readTDOBit\n");
  unsigned long val;
  val=caen_read(jtag_addr);
  return val & (1<<JTG_TDO) ? 1 : 0;
}

//Buffers used for caching of write accesses
#define VME_CACHE_LEN (1024)
uint32_t addr[VME_CACHE_LEN];
uint32_t data[VME_CACHE_LEN];
CVAddressModifier am[VME_CACHE_LEN];
CVDataWidth dw[VME_CACHE_LEN];
CVErrorCodes ec[VME_CACHE_LEN];

//Pointer used to cache 
int vme_cache_cnt = 0;

void flush_vme_cache(void)
{
  CVErrorCodes res;
  if(vme_cache_cnt) {
    /*res=CAENVME_MultiWrite(caen_handle, addr, data,vme_cache_cnt,am,dw,ec);
    if(res != cvSuccess) {
      fprintf(stderr,"CAENVME access error in caen_multi_write: %d\n",res);
      exit(1);
    }*/
	  fprintf(stderr,"flush_vme_cache\n");
	  int i = 0;
	  for (; i < vme_cache_cnt; i++) {
		  //cout<<__FUNCTION__<<" data[" << i << "] = " << data[i];

		  int tms = (data[i] & (1 << JTG_TMS))>>JTG_TMS;
		  int tdi = (data[i] & (1 << JTG_TDI))>>JTG_TDI;
		  int tck = (data[i] & (1 << JTG_TCK))>>JTG_TCK;

		  fprintf(stderr,"data[%x] %x TMS:%d TDI:%d \n", i, data[i], tms, tdi);
	  }

    vme_cache_cnt = 0;
  }
}

void vme_cache_write(unsigned long val)
{
  if(vme_cache_cnt>=VME_CACHE_LEN) flush_vme_cache();
  addr[vme_cache_cnt]=jtag_addr;
  data[vme_cache_cnt]=val;
  am[vme_cache_cnt]=0x9;
  dw[vme_cache_cnt]=cvD32;
  ec[vme_cache_cnt]=0;
  vme_cache_cnt++;
}

static void buffer_sync(struct udata_s *u)
{
  flush_vme_cache();
  u->last_tdo = readTDOBit();
  //printf("TDO: %d\n",u->last_tdo);
}

static void buffer_add(struct udata_s *u, int tms, int tdi, int tdo, int rmask)
{
	printf("buffer_add TMS:%d TDI:%d read_tdo:%d\n",tms,tdi,tdo);
  if(tms) {
    jtag_reg |= (1 << JTG_TMS);
  } else {
    jtag_reg &= 0xffffffff ^ (1 << JTG_TMS);
  }
  if(tdi==1) {
    jtag_reg |= (1 << JTG_TDI);
  } else if(tdi==0) {
    jtag_reg &= 0xffffffff ^ (1 << JTG_TDI);
  }
  jtag_reg &= 0xffffffff ^ (1 << JTG_TCK);
  vme_cache_write(jtag_reg);
  jtag_reg |= (1 << JTG_TCK);
  vme_cache_write(jtag_reg);
  if(tdo>=0) { //We have to check the TDO
    flush_vme_cache();
    u->last_tdo = readTDOBit();
    //printf("TDO: %d\n",u->last_tdo);
    if (u->last_tdo != tdo) u->error_rc = -1;
  }
}


void waitTime(long microsec)
{
  waitNano(microsec*1000);
}

void waitNano(long nanosec)
{
  struct timespec rec;
  int res;
  rec.tv_sec=0;
  rec.tv_nsec=nanosec;
  for(;;) {
    res=nanosleep(&rec, &rec);
    if (res==0) break;
    else {
      if (errno==EINVAL){
	fprintf(stderr,"nanosleep error %d\n",res);
	exit(1);
      }
    }
  }
}
/*
 * End of CAEN related part
 */

#define BUFFER_SIZE (4096)

static FILE *dumpfile = NULL;

struct buffer_s {
  unsigned int tms:1;
  unsigned int tdi:1;
  unsigned int tdi_enable:1;
  unsigned int tdo:1;
  unsigned int tdo_enable:1;
  unsigned int rmask:1;
};

static int h_setup(struct libxsvf_host *h)
{
  struct udata_s *u = h->user_data;
  initialize_jtag_hardware();
  u->last_tdo = 0;
  u->error_rc = 0;
  u->retval_i = 0;
  return 0;
}

static int h_shutdown(struct libxsvf_host *h)
{
  struct udata_s *u = h->user_data;
  buffer_sync(u);
  close_jtag_hardware();
  return u->error_rc;
}


static void h_udelay(struct libxsvf_host *h, long usecs, int tms, long num_tck)
{
  struct udata_s *u = h->user_data;
  buffer_sync(u);
  if (num_tck > 0) {
    struct timeval tv1, tv2;
    gettimeofday(&tv1, NULL);
    while (num_tck > 0) {
      buffer_add(u, tms, -1, -1, 0);
      num_tck--;
    }
    buffer_sync(u);
    gettimeofday(&tv2, NULL);
    if (tv2.tv_sec > tv1.tv_sec) {
      usecs -= (1000000 - tv1.tv_usec) + (tv2.tv_sec - tv1.tv_sec - 1) * 1000000;
      tv1.tv_usec = 0;
    }
    usecs -= tv2.tv_usec - tv1.tv_usec;
  }
  if (usecs > 0) {
    usleep(usecs);
  }
}

static int h_getbyte(struct libxsvf_host *h)
{
  struct udata_s *u = h->user_data;
  return fgetc(u->f);
}

static int h_sync(struct libxsvf_host *h)
{
  struct udata_s *u = h->user_data;
  buffer_sync(u);
  int rc = u->error_rc;
  u->error_rc = 0;
  return rc;
}

static int h_pulse_tck(struct libxsvf_host *h, int tms, int tdi, int tdo, int rmask, int sync)
{
  //printf("TMS:%d TDI:%d in_tdo:%d\n",tms,tdi,tdo);

  struct udata_s *u = h->user_data;
  if (u->syncmode)
    sync = 1;
  buffer_add(u, tms, tdi, tdo, rmask);
  if (sync) {
    buffer_sync(u);
    //printf("sync TDO: %d\n",u->last_tdo);
    int rc = u->error_rc < 0 ? u->error_rc : u->last_tdo;
    u->error_rc = 0;
    return rc;
  }
  return u->error_rc < 0 ? u->error_rc : 1;
}

static int h_set_frequency(struct libxsvf_host *h, int v)
{
  fprintf(stderr, "WARNING: Setting JTAG clock frequency to %d ignored!\n", v);
  return 0;
}

static void h_report_tapstate(struct libxsvf_host *h)
{
  struct udata_s *u = h->user_data;
  if (u->verbose >= 2)
    printf("[%s]\n", libxsvf_state2str(h->tap_state));
}

static void h_report_device(struct libxsvf_host *h, unsigned long idcode)
{
  printf("idcode=0x%08lx, revision=0x%01lx, part=0x%04lx, manufactor=0x%03lx\n", idcode,
	 (idcode >> 28) & 0xf, (idcode >> 12) & 0xffff, (idcode >> 1) & 0x7ff);
}

static void h_report_status(struct libxsvf_host *h, const char *message)
{
  struct udata_s *u = h->user_data;
  if (u->verbose >= 1)
    printf("[STATUS] %s\n", message);
}

static void h_report_error(struct libxsvf_host *h, const char *file, int line, const char *message)
{
  fprintf(stderr, "[%s:%d] %s\n", file, line, message);
}

static void *h_realloc(struct libxsvf_host *h, void *ptr, int size, enum libxsvf_mem which)
{
  return realloc(ptr, size);
}

static struct udata_s u = {
};

static struct libxsvf_host h = {
  .udelay = h_udelay,
  .setup = h_setup,
  .shutdown = h_shutdown,
  .getbyte = h_getbyte,
  .sync = h_sync,
  .pulse_tck = h_pulse_tck,
  .set_frequency = h_set_frequency,
  .report_tapstate = h_report_tapstate,
  .report_device = h_report_device,
  .report_status = h_report_status,
  .report_error = h_report_error,
  .realloc = h_realloc,
  .user_data = &u
};

const char *progname;

static void help()
{
  fprintf(stderr, "\n");
  fprintf(stderr, "A JTAG SVF/XSVF Player based on libxsvf for the FTDI FT232H, FT2232H and\n");
  fprintf(stderr, "FT4232H High Speed USB to Multipurpose UART/FIFO ICs.\n");
  fprintf(stderr, "\n");
  fprintf(stderr, "xsvftool-ft2232h, part of Lib(X)SVF (http://www.clifford.at/libxsvf/).\n");
  fprintf(stderr, "Copyright (C) 2009  RIEGL Research ForschungsGmbH\n");
  fprintf(stderr, "Copyright (C) 2009  Clifford Wolf <clifford@clifford.at>\n");
  fprintf(stderr, "Lib(X)SVF is free software licensed under the ISC license.\n");
  fprintf(stderr, "\n");
  fprintf(stderr, "Usage: %s [ -v[v..] ] [ -d dumpfile ] [ -L | -B ] [ -S ] [ -F ] \\\n", progname);
  fprintf(stderr, "      %*s [ -f freq[k|M] ] { -s svf-file | -x xsvf-file | -c } ...\n", (int)(strlen(progname)+1), "");
  fprintf(stderr, "      %*s [ -n DCC_number ] [ -o jtag_otput_chain] ...\n", (int)(strlen(progname)+1), "");
  fprintf(stderr, "\n");
  fprintf(stderr, "   -v\n");
  fprintf(stderr, "          Enable verbose output (repeat for incrased verbosity)\n");
  fprintf(stderr, "\n");
  fprintf(stderr, "   -d dumpfile\n");
  fprintf(stderr, "          Write a logfile of all MPSSE comunication\n");
  fprintf(stderr, "\n");
  fprintf(stderr, "   -L, -B\n");
  fprintf(stderr, "          Print RMASK bits as hex value (little or big endian)\n");
  fprintf(stderr, "\n");
  fprintf(stderr, "   -S\n");
  fprintf(stderr, "          Run in synchronous mode (slow but report errors right away)\n");
  fprintf(stderr, "\n");
  fprintf(stderr, "   -F\n");
  fprintf(stderr, "          Force mode (ignore all TDO mismatches)\n");
  fprintf(stderr, "\n");
  fprintf(stderr, "   -f freq[k|M]\n");
  fprintf(stderr, "          Set maximum frequency in Hz, kHz or MHz\n");
  fprintf(stderr, "\n");
  fprintf(stderr, "   -s svf-file\n");
  fprintf(stderr, "          Play the specified SVF file\n");
  fprintf(stderr, "\n");
  fprintf(stderr, "   -x xsvf-file\n");
  fprintf(stderr, "          Play the specified XSVF file\n");
  fprintf(stderr, "\n");
  fprintf(stderr, "   -c\n");
  fprintf(stderr, "          List devices in JTAG chain\n");
  fprintf(stderr, "   -n\n");
  fprintf(stderr, "          Select the slot of the DCC board (0 to 31)\n");
  fprintf(stderr, "   -o\n");
  fprintf(stderr, "          Number of the JTAG chain on DCC we want to use (0 or 1)\n");
  fprintf(stderr, "\n");
  exit(1);
}

int main(int argc, char **argv)
{
  int rc = 0;
  int gotaction = 0;
  int hex_mode = 0;
  int opt, i, j;

  progname = argc >= 1 ? argv[0] : "xsvftool-ft232h";
  while ((opt = getopt(argc, argv, "vd:LBSFf:x:s:co:n:")) != -1)
    {
      switch (opt)
	{
	case 'v':
	  u.verbose++;
	  break;
	case 'd':
	  if (!strcmp(optarg, "-"))
	    dumpfile = stdout;
	  else
	    dumpfile = fopen(optarg, "w");
	  if (!dumpfile) {
	    fprintf(stderr, "Can't open dumpfile `%s': %s\n", optarg, strerror(errno));
	    rc = 1;
	  }
	  break;
	case 'f':
	  u.frequency = strtol(optarg, &optarg, 10);
	  while (*optarg != 0) {
	    if (*optarg == 'k') {
	      u.frequency *= 1000;
	      optarg++;
	      continue;
	    }
	    if (*optarg == 'M') {
	      u.frequency *= 1000000;
	      optarg++;
	      continue;
	    }
	    if (optarg[0] == 'H' && optarg[1] == 'z') {
	      optarg += 2;
	      continue;
	    }
	    help();
	  }
	  break;
	case 'o':
	  jtag_output_chain = strtol(optarg, &optarg, 10);
	  break;
	case 'n':
	  dcc_number = strtol(optarg, &optarg, 10);
	  break;
	case 'x':
	case 's':
	  gotaction = 1;
	  if (!strcmp(optarg, "-"))
	    u.f = stdin;
	  else
	    u.f = fopen(optarg, "rb");
	  if (u.f == NULL) {
	    fprintf(stderr, "Can't open %s file `%s': %s\n", opt == 's' ? "SVF" : "XSVF", optarg, strerror(errno));
	    rc = 1;
	    break;
	  }
	  if (libxsvf_play(&h, opt == 's' ? LIBXSVF_MODE_SVF : LIBXSVF_MODE_XSVF) < 0) {
	    fprintf(stderr, "Error while playing %s file `%s'.\n", opt == 's' ? "SVF" : "XSVF", optarg);
	    rc = 1;
	  }
	  if (strcmp(optarg, "-"))
	    fclose(u.f);
	  break;
	case 'c':
	  gotaction = 1;
	  int old_frequency = u.frequency;
	  if (u.frequency == 0)
	    u.frequency = 10000;
	  if (libxsvf_play(&h, LIBXSVF_MODE_SCAN) < 0) {
	    fprintf(stderr, "Error while scanning JTAG chain.\n");
	    rc = 1;
	  }
	  u.frequency = old_frequency;
	  break;
	case 'L':
	  hex_mode = 1;
	  break;
	case 'B':
	  hex_mode = 2;
	  break;
	case 'S':
	  if (u.frequency == 0)
	    u.frequency = 10000;
	  u.syncmode = 1;
	  break;
	case 'F':
	  u.forcemode = 1;
	  break;
	default:
	  help();
	  break;
	}
    }

  if (!gotaction)
    help();

  if (u.retval_i) {
    if (hex_mode) {
      printf("0x");
      for (i=0; i < u.retval_i; i+=4) {
	int val = 0;
	for (j=i; j<i+4; j++)
	  val = val << 1 | u.retval[hex_mode > 1 ? j : u.retval_i - j - 1];
	printf("%x", val);
      }
    } else {
      printf("%d rmask bits:", u.retval_i);
      for (i=0; i < u.retval_i; i++)
	printf(" %d", u.retval[i]);
    }
    printf("\n");
  }

  return rc;
}

