package rpct.db.mtcc;

public class Constants {
    //public final static String DUMMY_CCS_NAME = "DUMMY_CCS";
    //public final static String DUMMY_VME_NAME = "DUMMY_VME";
    
    //public final static String CCS_NAME = "CCS_MTCC";
    //public final static String CCS_LABEL = "CCS_MTCC";
    //public final static int CCS_POSITION = 6;
    

    public final static String CONTROL_VME_CRATE_NAME = "VME_CONTROL";
    public final static String CONTROL_VME_CRATE_LABEL = "VME_CONTROL";
    
    public final static String XDAQ_HOST = "pccmsrpct";
    public final static int XDAQ_PORT = 1972;

    public final static String XDAQ_TC_HOST = "pccmsrpct";
    public final static int XDAQ_TC_PORT = 1973;
}