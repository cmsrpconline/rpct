package rpct.db;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import rpct.db.domain.configuration.XdaqExecutive;


public class TestEntityManager {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ConfCondDb");
        EntityManager em = emf.createEntityManager();
        List resultList = em.createQuery("select xe from XdaqExecutive xe").getResultList();
        for (Object object : resultList) {
            System.out.println(object);
        }
        System.out.println("22: " + em.find(XdaqExecutive.class, 22));        
    }

}
