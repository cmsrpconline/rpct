package rpct.db.fill;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import rpct.db.DataAccessException;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateDAO;
import rpct.db.domain.equipment.CrateDAOHibernate;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.FebConnector;
import rpct.db.domain.equipment.I2cCbChannel;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkBoardDAO;
import rpct.db.domain.equipment.LinkBoardDAOHibernate;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.equipment.ccsboard.CcsBoard;
import rpct.db.domain.equipment.chamberloaction.BarrelOrEndcap;
import rpct.db.domain.equipment.chamberloaction.ChamberLocation;
import rpct.db.domain.equipment.chamberloaction.ChamberLocationDAO;
import rpct.db.domain.equipment.chamberloaction.ChamberLocationDAOHibernate;
import rpct.db.domain.equipment.controlboard.ControlBoard;
import rpct.db.domain.equipment.distributionboard.DistributionBoardDAO;
import rpct.db.domain.equipment.distributionboard.DistributionBoardDAOHibernate;
import rpct.db.domain.equipment.feblocation.FebLocation;
import rpct.db.domain.equipment.feblocation.FebLocationDAO;
import rpct.db.domain.equipment.feblocation.FebLocationDAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.util.StringUtils;

/**
 * Created on 2005-03-01
 * 
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class DBGenerator {


    private EquipmentDAO daoEquipment;
    private CrateDAO daoCrate;
    private ChamberLocationDAO daoChamberLocation;
    private FebLocationDAO daoFebLocation;
    private DistributionBoardDAO daoDistributionBoard;
    private LinkBoardDAO daoLinkBoard;
    private CcsBoard dummyCCSBoard = null;
    private int errors = 0;

    static class LinkBoardInfo {
        private int masterPos;
        private int linkChannel;
        public LinkBoardInfo(int masterPos, int linkChannel) {
            this.linkChannel = linkChannel;
            this.masterPos = masterPos;
        }        
    }
    private Map<Integer, LinkBoardInfo> linkBoardMastersMap;

    public DBGenerator(HibernateContext hibernateContext) {
        daoEquipment = new EquipmentDAOHibernate(hibernateContext);
        daoCrate = new CrateDAOHibernate(hibernateContext);
        daoChamberLocation = new ChamberLocationDAOHibernate(hibernateContext);
        daoFebLocation =  new FebLocationDAOHibernate(hibernateContext);
        daoDistributionBoard = new DistributionBoardDAOHibernate(hibernateContext);
        daoLinkBoard = new LinkBoardDAOHibernate(hibernateContext);
        //daoFebCable = new CableDAOHibernate(hibernateContext);

        linkBoardMastersMap = new HashMap<Integer, LinkBoardInfo>();
        linkBoardMastersMap.put(1, new LinkBoardInfo(2, 1));
        linkBoardMastersMap.put(3, new LinkBoardInfo(2, 2));
        linkBoardMastersMap.put(4, new LinkBoardInfo(5, 1));
        linkBoardMastersMap.put(6, new LinkBoardInfo(5, 2));
        linkBoardMastersMap.put(7, new LinkBoardInfo(8, 1));
        linkBoardMastersMap.put(9, new LinkBoardInfo(8, 2));
        linkBoardMastersMap.put(11, new LinkBoardInfo(12, 1));
        linkBoardMastersMap.put(13, new LinkBoardInfo(12, 2));
        linkBoardMastersMap.put(14, new LinkBoardInfo(15, 1));
        linkBoardMastersMap.put(16, new LinkBoardInfo(15, 2));
        linkBoardMastersMap.put(17, new LinkBoardInfo(18, 1));
        linkBoardMastersMap.put(19, new LinkBoardInfo(18, 2));
        
    }

    /*public void generateLinkBoxes() throws DataAccessException {
        MessageFormat nameFormat = new MessageFormat("{0}_S{1}");
        for (Wheel wheel : Wheel.values()) {
            for (int sector = 1; sector <= 12; sector++) {
                LinkBox linkBox = new LinkBox();
                linkBox.setWheel(wheel);
                linkBox.setSector(sector);
                linkBox.setName(nameFormat.format(new Object[] {wheel, sector}));
                daoCrate.saveObject(linkBox);
            }
        }
    }*/

    /*private void updateCableDetails(ResultSet rs, Cable cable) throws DataAccessException, SQLException {
        cable.setDestination(StringUtils.trimToNull(rs.getString("CABLE_DEST")));
        cable.setLength(rs.getInt("CABLE_LEN"));
        cable.setType(StringUtils.trimToNull(rs.getString("CABLE_TYPE")));
        cable.setConnectorSrc(StringUtils.trimToNull(rs.getString("CABLE_CONN_IN")));
        cable.setConnectorDest(StringUtils.trimToNull(rs.getString("CABLE_CONN_OUT")));
        cable.setBuy(StringUtils.trimToNull(rs.getString("CABLE_BUY")));
        daoFebCable.saveObject(cable);                
    }    */

    /*public VmeCrate getDummyVme() throws DataAccessException {        
        
        VmeCrate vme = (VmeCrate) daoEquipment.getCrateByName(Constants.DUMMY_VME_NAME);
        if (vme == null) {
            vme = new VmeCrate();
            vme.setLabel(Constants.DUMMY_VME_NAME);
            vme.setName(Constants.DUMMY_VME_NAME);
            daoEquipment.saveObject(vme);
        }
        return vme;
    }*/
    
    /*public CcsBoard getDummyCCSBoard() throws DataAccessException {
        if (dummyCCSBoard == null) {
            dummyCCSBoard = (CcsBoard) daoEquipment.getBoardByName(Constants.DUMMY_CCS_NAME);
            if (dummyCCSBoard == null) {
                dummyCCSBoard = new CcsBoard();
                dummyCCSBoard.setLabel(Constants.DUMMY_CCS_NAME);
                dummyCCSBoard.setName(Constants.DUMMY_CCS_NAME);
                dummyCCSBoard.setPosition(0);
                dummyCCSBoard.setCrate(getDummyVme());
                daoEquipment.saveObject(dummyCCSBoard);
            }
        }
        return dummyCCSBoard;
    }*/
    
    public void generateLinkBoxes(String odbcSource) throws ClassNotFoundException, SQLException, DataAccessException {
        System.out.println("generateLinkBoxes");
        Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
        Connection con = DriverManager.getConnection("jdbc:odbc:" + odbcSource);            
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery("select * from LINKS where LBB_NAME <> ''");
        try {
            while (rs.next()) {
                String lbbName = "LBB_" + rs.getString("LBB_NAME");
                
                LinkBox linkBox = (LinkBox) daoEquipment.getCrateByName(lbbName);
                if (linkBox == null) {
                    linkBox = new LinkBox();
                    linkBox.setName(lbbName);                    
                }                
                linkBox.setLabel(rs.getString("LBB_LOCATION"));
                daoCrate.saveObject(linkBox);                    
            }
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
    }

    public void generateControlBoardsAndI2CConnections(String odbcSource, BarrelOrEndcap barrelOrEndcap) throws ClassNotFoundException, SQLException, DataAccessException {
        System.out.println("generateControlBoardsAndI2CConnections");
        Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
        Connection con = DriverManager.getConnection("jdbc:odbc:" + odbcSource);
        
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery("select * from LINKS");
        try {
            LinkBox linkBox = null;
            String wheelSector = null;
            ControlBoard controlBoard = null;
            while (rs.next()) {
                String tmp = rs.getString("LBB_NAME");                
                if (!StringUtils.isTrimmedEmpty(tmp)) {
                    wheelSector = tmp;
                    linkBox = (LinkBox) daoCrate.getByName("LBB_" + wheelSector.toUpperCase());                    
                    continue;
                }                
                                
                tmp = rs.getString("BOARD_NAME");
                if (tmp != null && "CB".equals(tmp.trim())) {
                                        
                    if (linkBox == null) {
                        throw new IllegalStateException("linkBox == null");
                    }
                    if (wheelSector == null) {
                        throw new IllegalStateException("wheelSector == null");                        
                    }
                    int lbbSlot = rs.getInt("LBB_SLOT");
                    if (lbbSlot != 10 && lbbSlot != 20) {
                        throw new IllegalStateException("Invalid lbbSlot of controlBoard " + lbbSlot);
                    }
                    controlBoard = (lbbSlot == 10) ? linkBox.getControlBoard10()
                            : linkBox.getControlBoard20();
                    if (controlBoard == null) {
                        controlBoard = new ControlBoard();
                        controlBoard.setCrate(linkBox);
                        controlBoard.setPosition(lbbSlot);
                        //controlBoard.setCcsBoard(getDummyCCSBoard());
                        String cbName = "CB_" + wheelSector + "_L" + lbbSlot;
                        controlBoard.setName(cbName);
                        controlBoard.setLabel(cbName);
                        //controlBoard.setPositionInCcuChain(posInCcuChain);
                        //cb.setRearPanelChannel(rearPanelChannel);
                        //cb.setCcsbInputConnector(ccsbInputConnector);
                        //System.out.println(cb.toString()); 
                        //System.out.println(ccsBoard.toString());
                        daoEquipment.saveObject(controlBoard);
                    }
                    continue;
                }
                
                tmp = rs.getString("CABLE_NAME");
                if (tmp != null && tmp.trim().length() > 0) {
                    String cableName = tmp;
                    StringTokenizer tokenizer = new StringTokenizer(tmp, "_");
                    
                    // skip RPC_ for the endcap
                    if (barrelOrEndcap == BarrelOrEndcap.Endcap) {
                        String skip = tokenizer.nextToken();
                        if (!skip.equals("RPC")) { 
                            throw new IllegalStateException("RPC_ not found");
                        }
                    }
                    
                    String cableType = tokenizer.nextToken();
                    if ("I2C".equals(cableType)) {
                        String wheel = tokenizer.nextToken();
                        String sector = tokenizer.nextToken();
                       
                        if (barrelOrEndcap == BarrelOrEndcap.Barrel) {

                            int diskOrWheel;
                            int layer;
                            int sect;
                            String station = tokenizer.nextToken(); 
                            String i2cRing = StringUtils.trimToNull(rs.getString("I2C_RING")); 
                            if (i2cRing != null) {
                                i2cRing = i2cRing.replace(" ", "");
                            }
                            
                            diskOrWheel = rbWheelStringToInt(wheel);
                            layer = rbStationStringToInt(station);
                            
                            if (sector.charAt(0) != 'S') {
                                throw new IllegalArgumentException("Illegal format of sector");
                            }
                            sect = Integer.parseInt(sector.substring(1));

                            List<FebLocation> febLocations;
                            
                            if (i2cRing == null) {
                                String subsector = null;
                                if (station.endsWith("+")) {
                                    subsector = "+";
                                }
                                else if (station.endsWith("-")) {
                                    subsector = "-";                                    
                                }
                                List<ChamberLocation> chLocations = daoChamberLocation.getByLocation(
                                        diskOrWheel, layer, sect, subsector, BarrelOrEndcap.Barrel);
                                if (chLocations.isEmpty() || chLocations.size() > 1) {
                                    throw new IllegalArgumentException("Invalid number (" + chLocations.size() 
                                            + ") of chamberLocations found for cable " + cableName);
                                }
                                String endCableName = rs.getString("END_CABLE_NAME");
                                char part = endCableName.charAt(3);
                                String etaPart;
                                switch (part) {
                                case 'F':
                                    etaPart = "Forward";
                                    break;
                                case 'B':
                                    etaPart = "Backward";
                                    break;
                                case 'M':
                                    etaPart = "Central";
                                    break;
                                default:
                                    throw new IllegalArgumentException("Invalid part " + part);
                                }    
                                febLocations = daoFebLocation.getByLocParameters(chLocations.get(0), etaPart, null);
                                if (febLocations.isEmpty()) {
                                    throw new IllegalArgumentException("No feb locations for chamberLocation " + chLocations.get(0).getChamberLocationName()
                                            + " and eta partition " + etaPart);                                
                                }                                                  
                            }
                            else if (i2cRing.equals("notused")) {
                                if (station.endsWith("+") || station.endsWith("-")) {  
                                    throw new IllegalArgumentException();
                                }
                                List<ChamberLocation> chLocations = daoChamberLocation.getByLocation(
                                        diskOrWheel, layer, sect, null, BarrelOrEndcap.Barrel);
                                if (chLocations.isEmpty() || chLocations.size() > 1) {
                                    throw new IllegalArgumentException("Invalid number (" + chLocations.size() 
                                            + ") of chamberLocations found for cable " + cableName);
                                }
                                String endCableName = rs.getString("END_CABLE_NAME");
                                char part = endCableName.charAt(3);
                                String etaPart;
                                switch (part) {
                                case 'F':
                                    etaPart = "Forward";
                                    break;
                                case 'B':
                                    etaPart = "Backward";
                                    break;
                                case 'M':
                                    etaPart = "Central";
                                    break;
                                default:
                                    throw new IllegalArgumentException("Invalid part " + part);
                                }
                                
                                febLocations = daoFebLocation.getByLocParameters(chLocations.get(0), etaPart, null);
                                if (febLocations.isEmpty()) {
                                    throw new IllegalArgumentException("No feb locations for chamberLocation " + chLocations.get(0).getChamberLocationName()
                                            + " and eta partition " + etaPart);              
                                } 
                                
                                // get febLocations from other part of the chamber - all febs from this chamber
                                // use the same i2c channel, and they should be ready for the other part.
                                String otherEtaPart;
                                if ("Forward".equals(etaPart)) {
                                    otherEtaPart = "Backward";
                                }
                                else if ("Backward".equals(etaPart)) {
                                    otherEtaPart = "Forward";
                                }
                                else {
                                    throw new IllegalArgumentException("Illegal eta partition for chamberLocation " + chLocations.get(0).getChamberLocationName()
                                            + " and eta partition " + etaPart);                                       
                                }

                                List<FebLocation> otherFebLocations = daoFebLocation.getByLocParameters(
                                        chLocations.get(0), otherEtaPart, null);
                                if (otherFebLocations.isEmpty()) {
                                    throw new IllegalArgumentException("No feb locations for chamberLocation " + chLocations.get(0).getChamberLocationName()
                                            + " and eta partition " + otherEtaPart);   
                                }
                                I2cCbChannel i2cCbChannel = otherFebLocations.get(0).getI2cCbChannel();
                                if (i2cCbChannel == null) {
                                    throw new IllegalArgumentException("No i2cCbChannel for other part of the chamber");
                                }
                                for (int i = 1; i < otherFebLocations.size(); i++) {
                                    if (otherFebLocations.get(i).getI2cCbChannel().getId() != i2cCbChannel.getId()) {
                                        throw new IllegalArgumentException("Different i2cCbChannel for other part of " +
                                                "the chamber for cable " + cableName);
                                    }
                                }
                                for (FebLocation febLocation : febLocations) {
                                    febLocation.setI2cCbChannel(i2cCbChannel);
                                    daoEquipment.saveObject(febLocation);
                                }
                                continue;
                            }
                            else {
                                String subsector = i2cRing;
                                List<ChamberLocation> chLocations = daoChamberLocation.getByLocation(
                                        diskOrWheel, layer, sect, subsector, BarrelOrEndcap.Barrel);
                                if (chLocations.isEmpty() || chLocations.size() > 1) {
                                    String errMsg = "Invalid number (" + chLocations.size() 
                                    + ") of chamberLocations found for cable " + cableName
                                    + " i2cRing " + i2cRing;
                                    System.err.println("Error:" + errMsg);
                                    errors++;
                                    continue;
                                    //throw new IllegalArgumentException(errMsg);
                                }
                                febLocations = daoFebLocation.getByLocParameters(chLocations.get(0), null, null);
                                if (febLocations.isEmpty()) {
                                    throw new IllegalArgumentException("No feb locations for chamberLocation " + chLocations.get(0).getChamberLocationName());                                
                                } 
                            }                                       
                          
                            I2cCbChannel i2cCbChannel = febLocations.get(0).getI2cCbChannel();
                            if (i2cCbChannel == null || i2cCbChannel.getId() < 0) {
                                i2cCbChannel = new I2cCbChannel();
                                febLocations.get(0).setI2cCbChannel(i2cCbChannel);
                            }
                            i2cCbChannel.setControlBoard(controlBoard);
                            i2cCbChannel.setCbChannel(rs.getInt("CABLE_POS"));
                            daoEquipment.saveObject(i2cCbChannel); 
                            daoEquipment.saveObject(febLocations.get(0)); 
                            
                            // make sure all febs in the list are connected to the same cb channel
                            for (int i = 1; i < febLocations.size(); i++) {
                                FebLocation fl = febLocations.get(i);
                                if (fl.getI2cCbChannel() == null || fl.getI2cCbChannel().getId() < 0) {
                                    fl.setI2cCbChannel(i2cCbChannel);
                                    daoEquipment.saveObject(fl);
                                }
                                else if (fl.getI2cCbChannel().getId() != i2cCbChannel.getId()) {
                                    throw new IllegalArgumentException("Not unique i2cCbChannel for " + cableName);
                                }
                            }                         
                        }
                        else {
                            String chamberName = tokenizer.nextToken().trim();
                            if (wheel.startsWith("YEN")) {
                                chamberName = chamberName.replace("RE", "RE-");
                            }
                            else if (wheel.startsWith("YEP")) {
                                chamberName = chamberName.replace("RE", "RE+");
                            }
                            else {
                                throw new IllegalArgumentException("Invalid wheel " + wheel);
                            }
                            ChamberLocation chamberLocation = daoChamberLocation.getByName(chamberName);
                            if (chamberLocation == null) {
                                throw new IllegalArgumentException("chamberLocation not found for name '" + chamberName + "'");
                            }
                            List<FebLocation> febLocations = daoFebLocation.getByLocParameters(chamberLocation, null, null);
                            if (febLocations.isEmpty()) {
                                String errMsg = "No feb locations for chamberLocation " 
                                    + chamberName + " for cable " + cableName;
                                System.err.println("Error: " + errMsg);
                                errors++;
                                continue;
                                //throw new IllegalArgumentException(errMsg);                                
                            }                            
                            I2cCbChannel i2cCbChannel = febLocations.get(0).getI2cCbChannel();
                            if (i2cCbChannel == null || i2cCbChannel.getId() < 0) {
                                i2cCbChannel = new I2cCbChannel();
                                febLocations.get(0).setI2cCbChannel(i2cCbChannel);
                            }
                            i2cCbChannel.setControlBoard(controlBoard);
                            i2cCbChannel.setCbChannel(rs.getInt("CABLE_POS"));
                            daoEquipment.saveObject(i2cCbChannel); 
                            daoEquipment.saveObject(febLocations.get(0)); 
                            
                            // make sure other febs in that chamber location are connected to the same
                            // cb channel
                            for (int i = 1; i < febLocations.size(); i++) {
                                FebLocation fl = febLocations.get(i);
                                if (fl.getI2cCbChannel() == null || fl.getI2cCbChannel().getId() < 0) {
                                    fl.setI2cCbChannel(i2cCbChannel);
                                    daoEquipment.saveObject(fl);
                                }
                                else if (fl.getI2cCbChannel().getId() != i2cCbChannel.getId()) {
                                    throw new IllegalArgumentException("Not unique i2cCbChannel for chamber " + chamberName);
                                }
                            }
                        }                        
                        continue;
                    }            
                }
            }
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
    }

    
    private void setMastersAndSaveLinkBoards(LinkBoard[] linkBoards) throws DataAccessException {
        for (int i = 1; i < linkBoards.length; i++) {
            LinkBoard linkBoard = linkBoards[i];
            if (linkBoard != null) {
                LinkBoardInfo info = linkBoardMastersMap.get(linkBoard.getPosition());
                if (info != null) {
                    LinkBoard master = linkBoards[info.masterPos];
                    linkBoard.setMasterBoard(master);
                    String masterName = master.getName();
                    linkBoard.setName(masterName.substring(0, masterName.length() - 1) + info.linkChannel);
                }
                throw new RuntimeException("Tu chyba trzeba ustawic master na samego siebie, ale teraz nie mam czasu sie w to bawic");
            }
        }
        for (int i = 1; i < linkBoards.length; i++) {
            LinkBoard linkBoard = linkBoards[i];
            if (linkBoard != null) {
                if (linkBoard.getName() == null) {
                    throw new NullPointerException("linkBoard name is null");
                }
                daoLinkBoard.saveObject(linkBoard);
                linkBoards[i] = null;
            }
        }
    }
    
    public void generateBarrelLinkBoards() throws ClassNotFoundException, SQLException, DataAccessException {
        System.out.println("generateBarrelLinkBoards");
        Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
        Connection con = DriverManager.getConnection("jdbc:odbc:YB_LBBoxes_sgn_trg");
        
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery("select * from LINKS");
        try {
            Crate linkBox = null;
            LinkBoard linkBoard = null;
            LinkBoard[] linkBoards = new LinkBoard[20]; // will be indexed from 1 by slot like in the excel file
            while (rs.next()) {
                String lbbName = rs.getString("LBB_NAME");
                if (!StringUtils.isTrimmedEmpty(lbbName)) {
                    setMastersAndSaveLinkBoards(linkBoards);
                    lbbName = "LBB_" + lbbName;
                    linkBox = daoCrate.getByName(lbbName.toUpperCase());
                    continue;
                }                
                
                int lbbSlot = rs.getInt("LBB_SLOT");
                if (lbbSlot != 0) {
                    
                    if (linkBox == null) {
                        throw new IllegalStateException("linkBox == null");
                    }
                    
                    linkBoard = new LinkBoard();
                    linkBoard.setCrate(linkBox);
                    linkBoard.setPosition(lbbSlot);
                    linkBoard.setLabel(linkBox.getName() + '_' + lbbSlot);
                    String linkName = StringUtils.toUpperCase(StringUtils.trimToNull(rs.getString("LINK_NAME")));
                    if (linkName != null) {
                        StringBuilder lbName = new StringBuilder("LB_");
                        StringTokenizer tokenizer = new StringTokenizer(linkName, "_");

                        String skip;
                        skip = tokenizer.nextToken(); 
                        if (!skip.equals("TRG")) { 
                            throw new IllegalStateException("TRG_ not found");
                        }
                        String wheel = tokenizer.nextToken();
                        lbName.append(wheel); // add wheel
                        lbName.append('_');
                        lbName.append(tokenizer.nextToken()); // add sector
                        lbName.append('_');
                        lbName.append(tokenizer.nextToken()); // add link name
                        lbName.append("_CH0"); // add link name
                        linkBoard.setName(lbName.toString());
                    }
                    
                    linkBoards[lbbSlot] = linkBoard;                                    
                    continue;
                }   
            }
            setMastersAndSaveLinkBoards(linkBoards);
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
    }
    
    private int rbWheelStringToInt(String rbString) {

        if ("RB+2".equals(rbString)) {
            return 2;
        }
        else if ("RB+1".equals(rbString)) {
            return 1;
        }
        else if ("RB0".equals(rbString)) {
            return 0;
        }
        else if ("RB-1".equals(rbString)) {
            return -1;
        }
        else if ("RB-2".equals(rbString)) {
            return -2;
        }
        else {
            throw new IllegalArgumentException("Illegal wheel " + rbString);
        }
    }
    
    public int rbStationStringToInt(String station) {        
        if ("RB1IN".equals(station)) {
            return 1;
        }
        else if ("RB1OUT".equals(station)) {
            return 2;
        }
        else if ("RB2IN".equals(station)) {
            return 3;
        }
        else if ("RB2/2IN".equals(station)) {
            return 3;
        }
        else if ("RB2/3IN".equals(station)) {
            return 3;
        }
        else if ("RB2OUT".equals(station)) {
            return 4;
        }
        else if ("RB2/2OUT".equals(station)) {
            return 4;
        }
        else if ("RB2/3OUT".equals(station)) {
            return 4;
        }
        else if ("RB3".equals(station)) {
            return 5;
        }
        else if ("RB4".equals(station)) {
            return 6;
        }
        else if ("RB4++".equals(station)) {
            return 6;
        }
        else if ("RB4+".equals(station)) {
            return 6;
        }
        else if ("RB4-".equals(station)) {
            return 6;
        }
        else if ("RB4--".equals(station)) {
            return 6;
        }
        else {
            throw new IllegalArgumentException("Illegal station " + station);
        }
    }
    
    public void generateSGNConnections(String odbcSource, BarrelOrEndcap barrelOrEndcap) throws ClassNotFoundException, SQLException, DataAccessException {
        System.out.println("generateSGNConnections");
        Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
        Connection con = DriverManager.getConnection("jdbc:odbc:" + odbcSource);
        
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery("select * from LINKS");
        try {
            Crate linkBox = null;
            LinkBoard linkBoard = null;
            LinkBoard[] linkBoards = new LinkBoard[20]; // will be indexed from 1 by slot like in the excel file
            while (rs.next()) {
                String lbbName = rs.getString("LBB_NAME");
                if (!StringUtils.isTrimmedEmpty(lbbName)) {
                    lbbName = "LBB_" + lbbName;
                    linkBox = daoCrate.getByName(lbbName.toUpperCase());
                    continue;
                }                
                
                int lbbSlot = rs.getInt("LBB_SLOT");
                if (lbbSlot != 0) {
                    
                    if (linkBox == null) {
                        throw new IllegalStateException("linkBox == null");
                    }
                    
                    linkBoard = (LinkBoard) linkBox.getBoardArray()[lbbSlot];
                    if (linkBoard == null) {
                        System.err.println("Warninng! No linkboard for " + linkBox.getName() 
                                + " slot " + lbbSlot);
                        continue;
                    }
                    if (linkBoard.getPosition() != lbbSlot) {
                        throw new IllegalStateException("linkBoard.getPosition() != lbbSlot");
                    }   
                    System.out.println(linkBoard.getLabel());
                    continue;
                }   
 
                String cableName = rs.getString("CABLE_NAME");
                if (cableName != null && cableName.trim().length() > 0) {
                    
                    if (linkBoard == null) {
                        throw new IllegalStateException("No linkboard for cable " + cableName);                        
                    }
                    
                    cableName = cableName.trim().toUpperCase();
                    
                    StringTokenizer tokenizer = new StringTokenizer(cableName, "_");
                    
                    // skip RPC_ for the endcap
                    if (barrelOrEndcap == BarrelOrEndcap.Endcap) {
                        String skip = tokenizer.nextToken();
                        if (!skip.equals("RPC")) { 
                            throw new IllegalStateException("RPC_ not found");
                        }
                    }
                    
                    String cableType = tokenizer.nextToken();
                    if ("SGN".equals(cableType)) {                        
                        String wheel = tokenizer.nextToken();
                        String sector = tokenizer.nextToken();
                        
                        if (barrelOrEndcap == BarrelOrEndcap.Barrel) {
                            int diskOrWheel;
                            int layer;
                            int sect;
                            int connectorNum;
                            String station = tokenizer.nextToken();       
                            //String i2cInfo = tokenizer.nextToken();  
                            String connector = tokenizer.nextToken();
                            
                            diskOrWheel = rbWheelStringToInt(wheel);
                            layer = rbStationStringToInt(station);
                            
                            if (sector.charAt(0) != 'S') {
                                throw new IllegalArgumentException("Illegal format of sector");
                            }
                            sect = Integer.parseInt(sector.substring(1));

                            List<ChamberLocation> chLocations = daoChamberLocation.getByLocation(
                                    diskOrWheel, layer, sect, null, BarrelOrEndcap.Barrel);
                            
                            if (chLocations.size() == 0) {
                                throw new IllegalArgumentException("No ChamberLocation found");
                            }
                            
                            if (connector.charAt(0) != 'C') {
                                throw new IllegalArgumentException("Illegal format of connector");
                            }
                            connectorNum = Integer.parseInt(connector.substring(1));
                            


                            boolean connectorFound = false;
                            for (ChamberLocation location : chLocations) {
                                List<FebLocation> febLocations = location.getFEBLocations();
                                for (FebLocation febLocation : febLocations) {
                                    List<FebConnector> febConnectors = febLocation.getFebConnectors();
                                    for (FebConnector febConnector : febConnectors) {
                                        if (febConnector.getFebConnectorNum() == connectorNum) {
                                            if (connectorFound) {
                                                String errMsg = "Several connectors with number " + connectorNum
                                                + " exist for location "
                                                + " diskOrWheel = " + diskOrWheel
                                                + " layer = " + layer
                                                + " sect = " + sect
                                                + " Barrel. Cable name = " + cableName;
                                                errors++;
                                                System.err.println(errMsg);
                                                //throw new IllegalArgumentException(errMsg);                                                
                                            }
                                            connectorFound = true;
                                            if (febLocation.getLinkBoard() != null
                                                    && febLocation.getLinkBoard().getId() != -1
                                                    && febLocation.getLinkBoard().getId() != linkBoard.getId()) {
                                                System.err.println("Warning! Changing LinkBoard in FebLocation " 
                                                        + febLocation.getId() + " from " + febLocation.getLinkBoard().getId()
                                                        + " to " + linkBoard.getId() + ". Cable name = " + cableName);
                                            }
                                            febLocation.setLinkBoard(linkBoard);
                                            febConnector.setFebConnectorNum(connectorNum);
                                            febConnector.setLinkBoardInputNum(rs.getInt("CABLE_POS"));
                                            daoEquipment.saveObject(febLocation);
                                            daoEquipment.saveObject(febConnector);
                                        }
                                    }
                                }
                            }

                            if (!connectorFound) {
                                String errMsg = "No connector number " + connectorNum 
                                + " for location diskOrWheel = " + diskOrWheel
                                + " layer = " + layer
                                + " sect = " + sect
                                + " Barrel. Cable name = " + cableName; 
                                errors++;
                                System.err.println(errMsg);
                                //throw new IllegalArgumentException();                                                
                            }    
                            
                        }
                        else { // endcap
                            

                            String chamberName = tokenizer.nextToken().trim();
                            String i2cInfo = tokenizer.nextToken();
                            String lbPos = tokenizer.nextToken();
                            String cabalePos = tokenizer.nextToken();
                            String connInfo = tokenizer.nextToken();
                            String febLocalEtaPartition;
                            int connectorNum;
                            if (wheel.startsWith("YEN")) {
                                chamberName = chamberName.replace("RE", "RE-");
                            }
                            else if (wheel.startsWith("YEP")) {
                                chamberName = chamberName.replace("RE", "RE+");
                            }
                            else {
                                throw new IllegalArgumentException("Invalid wheel " + wheel);
                            }
                            ChamberLocation chamberLocation = daoChamberLocation.getByName(chamberName);
                            if (chamberLocation == null) {
                                throw new IllegalArgumentException("chamberLocation not found for name '" + chamberName + "'");
                            }

                            febLocalEtaPartition = String.valueOf(connInfo.charAt(0));
                            if (!"A".equals(febLocalEtaPartition)
                                    && !"B".equals(febLocalEtaPartition)
                                    && !"C".equals(febLocalEtaPartition)) {
                                throw new IllegalArgumentException("Illegal febLocalEtaPartition " + febLocalEtaPartition);
                            }
                            
                            connectorNum = Integer.parseInt(connInfo.substring(1));

                            boolean connectorFound = false;
                            List<FebLocation> febLocations = chamberLocation.getFEBLocations();
                            for (FebLocation febLocation : febLocations) {                                    
                                if (febLocalEtaPartition.equals(febLocation.getFebLocalEtaPartition())) {
                                    List<FebConnector> febConnectors = febLocation.getFebConnectors();
                                    for (FebConnector febConnector : febConnectors) {
                                        if (febConnector.getFebConnectorNum() == connectorNum) {
                                            if (connectorFound) {
                                                String errMsg = "Several connectors with number " + connectorNum
                                                + " exist for chamber " + chamberName
                                                + " Endcap. Cable name = " + cableName;
                                                errors++;
                                                System.err.println(errMsg);
                                                //throw new IllegalArgumentException(errMsg);                                                
                                            }
                                            connectorFound = true;
                                            if (febLocation.getLinkBoard() != null
                                                    && febLocation.getLinkBoard().getId() != -1
                                                    && febLocation.getLinkBoard().getId() != linkBoard.getId()) {
                                                System.err.println("Warning! Changing LinkBoard in FebLocation " 
                                                        + febLocation.getId() + " from " + febLocation.getLinkBoard().getId()
                                                        + " to " + linkBoard.getId() + ". Cable name = " + cableName);
                                            }
                                            febLocation.setLinkBoard(linkBoard);
                                            febConnector.setFebConnectorNum(connectorNum);
                                            febConnector.setLinkBoardInputNum(rs.getInt("CABLE_POS"));
                                            daoEquipment.saveObject(febLocation);
                                            daoEquipment.saveObject(febConnector);
                                        }
                                    }
                                }


                            }

                            if (!connectorFound) {
                                String errMsg = "No connector number " + connectorNum 
                                + " exist for chamber " + chamberName
                                + " Endcap. Cable name = " + cableName; 
                                errors++;
                                System.err.println(errMsg);
                                //throw new IllegalArgumentException();                                                
                            }   
                            
                            /*int diskOrWheel;
                            int layer;
                            int sect;
                            String febLocalEtaPartition;
                            int connectorNum;
                            // RPC_SGN_YEN1_S1_RE1/2/36_AF0_L4_J1_A1 
                            String chamberName = tokenizer.nextToken();
                            String i2cInfo = tokenizer.nextToken();
                            String lbPos = tokenizer.nextToken();
                            String cabalePos = tokenizer.nextToken();
                            String connInfo = tokenizer.nextToken();
                            

                            if ("YEN1".equals(wheel)) {
                                diskOrWheel = -1;
                            }
                            else if ("YEP1".equals(wheel)) {
                                diskOrWheel = 1;
                            }
                            else {
                                throw new IllegalArgumentException("Illegal wheel " + wheel);
                            }
                            
                            
                            StringTokenizer chNameTokenizer = new StringTokenizer(chamberName, "/");
                            String chDisk = chNameTokenizer.nextToken();
                            layer = Integer.parseInt(chNameTokenizer.nextToken());
                            sect = Integer.parseInt(chNameTokenizer.nextToken());
                            
                            febLocalEtaPartition = String.valueOf(connInfo.charAt(0));
                            if (!"A".equals(febLocalEtaPartition)
                                    && !"B".equals(febLocalEtaPartition)
                                    && !"C".equals(febLocalEtaPartition)) {
                                throw new IllegalArgumentException("Illegal febLocalEtaPartition " + febLocalEtaPartition);
                            }
                            connectorNum = Integer.parseInt(connInfo.substring(1));
                            

                            List<ChamberLocation> chLocations = daoChamberLocation.getByLocation(
                                    diskOrWheel, layer, sect, null, BarrelOrEndcap.Endcap.toString());

                            if (chLocations.size() == 0) {
                                throw new IllegalArgumentException("No ChamberLocation found");
                            }

                            boolean connectorFound = false;
                            for (ChamberLocation location : chLocations) {
                                List<FebLocation> febLocations = location.getFEBLocations();
                                for (FebLocation febLocation : febLocations) {                                    
                                    if (febLocalEtaPartition.equals(febLocation.getFebLocalEtaPartition())) {
                                        List<FebConnector> febConnectors = febLocation.getFebConnectors();
                                        for (FebConnector febConnector : febConnectors) {
                                            if (febConnector.getFebConnectorNum() == connectorNum) {
                                                if (connectorFound) {
                                                    String errMsg = "Several connectors with number " + connectorNum
                                                    + " exist for location "
                                                    + " diskOrWheel = " + diskOrWheel
                                                    + " layer = " + layer
                                                    + " sect = " + sect
                                                    + " Endcap. Cable name = " + cableName;
                                                    errors++;
                                                    System.err.println(errMsg);
                                                    //throw new IllegalArgumentException(errMsg);                                                
                                                }
                                                connectorFound = true;
                                                if (febLocation.getLinkBoard() != null
                                                        && febLocation.getLinkBoard().getId() != -1
                                                        && febLocation.getLinkBoard().getId() != linkBoard.getId()) {
                                                    System.err.println("Warning! Changing LinkBoard in FebLocation " 
                                                            + febLocation.getId() + " from " + febLocation.getLinkBoard().getId()
                                                            + " to " + linkBoard.getId() + ". Cable name = " + cableName);
                                                }
                                                febLocation.setLinkBoard(linkBoard);
                                                febConnector.setFebConnectorNum(connectorNum);
                                                febConnector.setLinkBoardInputNum(rs.getInt("CABLE_POS"));
                                                daoEquipment.saveObject(febLocation);
                                                daoEquipment.saveObject(febConnector);
                                            }
                                        }
                                    }
                                    
                                    
                                }
                            }

                            if (!connectorFound) {
                                String errMsg = "No connector number " + connectorNum 
                                + " for location diskOrWheel = " + diskOrWheel
                                + " layer = " + layer
                                + " sect = " + sect
                                + " Endcap. Cable name = " + cableName; 
                                errors++;
                                System.err.println(errMsg);
                                //throw new IllegalArgumentException();                                                
                            }    */
                            
                        }                  
                        
                                          
                    }      
                    continue;
                }            
            }
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
    }
      

    
    public void generateEndcapLinkBoards(String odbcSource) throws ClassNotFoundException, SQLException, DataAccessException {
        System.out.println("generateEndcapLinkBoards");
        Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
        Connection con = DriverManager.getConnection("jdbc:odbc:" + odbcSource);
        
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery("select * from LINKS");
        try {
            Crate linkBox = null;
            LinkBoard linkBoard = null;
            LinkBoard[] linkBoards = new LinkBoard[20]; // will be indexed from 1 by slot like in the excel file
            while (rs.next()) {
                String lbbName = rs.getString("LBB_NAME");
                if (!StringUtils.isTrimmedEmpty(lbbName)) {
                    setMastersAndSaveLinkBoards(linkBoards);
                    lbbName = "LBB_" + lbbName;
                    linkBox = daoCrate.getByName(lbbName.toUpperCase());
                    continue;
                }                
                
                String tmp = rs.getString("LB_NAME");
                if ("MLB".equals(tmp) || "SLB".equals(tmp)) {
                    
                    if (linkBox == null) {
                        throw new IllegalStateException("linkBox == null");
                    }
                    int lbbSlot = rs.getInt("LBB_SLOT");
                    
                    linkBoard = new LinkBoard();
                    linkBoard.setCrate(linkBox);
                    linkBoard.setPosition(lbbSlot);
                    linkBoard.setLabel(linkBox.getName() + '_' + lbbSlot);
                    String linkName = StringUtils.toUpperCase(StringUtils.trimToNull(rs.getString("LINK_NAME")));
                    if (linkName != null) {
                        StringBuilder lbName = new StringBuilder("LB_");
                        StringTokenizer tokenizer = new StringTokenizer(linkName, "_");
                        
                        String skip;
                        skip = tokenizer.nextToken(); 
                        //  skip RPC_                        
                        if (!skip.equals("RPC")) { 
                            throw new IllegalStateException("RPC_ not found");
                        }
                        //  skip TRG_
                        skip = tokenizer.nextToken(); 
                        if (!skip.equals("TRG")) { 
                            throw new IllegalStateException("TRG_ not found");
                        }
                        String wheel = tokenizer.nextToken();
                        // convert wheel to convention RE... if needed
                        if (!wheel.startsWith("RE")) {
                            if (wheel.startsWith("YEN")) {
                                wheel = wheel.replaceFirst("YEN", "RE-");
                            }
                            else if (wheel.startsWith("YEP")) {
                                wheel = wheel.replaceFirst("YEP", "RE+");
                            }
                            else {
                                throw new IllegalArgumentException("Invalid wheel " + wheel);
                            }
                        }
                        lbName.append(wheel); // add wheel
                        lbName.append('_');
                        lbName.append(tokenizer.nextToken()); // add sector
                        lbName.append('_');
                        lbName.append(tokenizer.nextToken()); // add link name
                        lbName.append("_CH0"); // add link name
                        linkBoard.setName(lbName.toString());
                    }
                    
                    linkBoards[lbbSlot] = linkBoard;
                    // TODO linkBoard.setMaster("MLB".equals(rs.getString("LB_NAME").trim()));
                    /*linkBoard.setStripInversion("yes".equals(rs.getString("STRIP_INV").trim()));*/
                    
                    /*String linkName = StringUtils.toUpperCase(StringUtils.trimToNull(rs.getString("LINK_NAME")));
                     if (linkName != null) {
                     CableTRG link = new CableTRG();
                     link.setName(linkName);
                     updateCableDetails(rs, link);
                     linkBoard.setCableTRG(link);
                     //linkBoard.setLinkName(StringUtils.toUpperCase(
                      //        StringUtils.trimToNull(rs.getString("LINK_NAME"))));
                       }*/
                    //linkBoard.setReadOnly(true);                  
                    continue;
                }   
 
                
                /*int cablePos = rs.getInt("CABLE_POS");
                if (cablePos != 0) {
                    
                    if (linkBoard == null) {
                        throw new IllegalStateException("linkBoard == null");
                    }
                    
                    CableSGN cableSGN = new CableSGN();
                    cableSGN.setLinkBoard(linkBoard);
                    cableSGN.setLinkBoardPos(cablePos);                        
                    String cableName = StringUtils.toUpperCase(
                            StringUtils.trimToNull(rs.getString("CABLE_NAME")));
                    if (cableName == null) {
                        System.err.println("No cable name for cable:" 
                                + linkBox.getWheel().name() + "_S" + linkBox.getSector() 
                                + "_LB" + linkBoard.getLinkBoxSlot() 
                                + "_P" + cablePos );
                        continue;
                        //throw new IllegalStateException("cable name == null. ");
                    }
                    cableSGN.setName(cableName);
                    cableSGN.setMissChanStr(StringUtils.trimToNull(rs.getString("MISSING_CHANNELS")));
                    
                    int pos = cableName.indexOf("_A");
                    if (pos == -1) {
                        throw new IllegalStateException("_A not found in cable name");                            
                    }
                    cableSGN.setI2cAddr(Integer.parseInt(cableName.substring(pos+3, pos+4)));
                    
                    // check wheel and sector in cable name
                    String wheelSector = "_" + linkBox.getWheel().name() + "_S" + linkBox.getSector() + "_";
                    if (cableName.indexOf(wheelSector) == -1) {
                        System.err.println("Invalid wheel or sector for cable '" + cableName + "'. Expecting " + wheelSector);
                        continue;
                        //throw new IllegalStateException("Invalid wheel or sector for cable '" + cableName + "'. Expecting " + wheelSector);                            
                    }     
                    updateCableDetails(rs, cableSGN);
                }*/
            }
            setMastersAndSaveLinkBoards(linkBoards);
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
    }
    
    public void generateLinkBoardChips() throws DataAccessException {
        List linkBoards = daoEquipment.getObjects(LinkBoard.class);
        for (Object object : linkBoards) {
            LinkBoard linkBoard = (LinkBoard) object;
            Set<Chip> chips = linkBoard.getChips();
            if (chips == null || chips.isEmpty()) {
                Chip chip = new Chip();
                chip.setBoard(linkBoard);
                chip.setType(ChipType.SYNCODER);
                chip.setPosition(0);
                daoEquipment.saveObject(chip);
            }
        }
    }
    
    
    public static void main(String[] args) {
        try {
            System.out.println("Current directory: " + new File(".").getCanonicalPath());
                        
            HibernateContext hibernateContext = new SimpleHibernateContextImpl();
            DBGenerator dbGenerator = new DBGenerator(hibernateContext);    
            
//            dbGenerator.generateLinkBoxes("YB_LBBoxes_sgn_trg");
//            dbGenerator.generateLinkBoxes("YE1_LBBoxes_sgn_trg");
//            dbGenerator.generateLinkBoxes("YE3_LBBoxes_sgn_trg");
//            hibernateContext.currentSession().flush();
//            
//            dbGenerator.generateBarrelLinkBoards();
//            dbGenerator.generateEndcapLinkBoards("YE1_LBBoxes_sgn_trg");
//            dbGenerator.generateEndcapLinkBoards("YE3_LBBoxes_sgn_trg");
//            hibernateContext.currentSession().flush();
            
//            dbGenerator.generateSGNConnections("YB_LBBoxes_sgn_trg", BarrelOrEndcap.Barrel);
//            dbGenerator.generateSGNConnections("YE1_LBBoxes_sgn_trg", BarrelOrEndcap.Endcap);
            dbGenerator.generateSGNConnections("YE3_LBBoxes_sgn_trg", BarrelOrEndcap.Endcap);
//            hibernateContext.currentSession().flush();
            
            //dbGenerator.generateControlBoardsAndI2CConnections("YB_LBBoxes_i2c", BarrelOrEndcap.Barrel);
            //dbGenerator.generateControlBoardsAndI2CConnections("YE1_LBBoxes_i2c", BarrelOrEndcap.Endcap);
            //hibernateContext.currentSession().flush();
            
            //dbGenerator.generateLinkBoardChips();
            
            if (dbGenerator.getErrors() > 0) {
                System.err.println(dbGenerator.getErrors() + " errors found. Rolling back transaction"); 
                hibernateContext.rollback();
            }
            hibernateContext.closeSession();
        } catch(Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();            
        }
        System.out.println("Done");
    }

    public int getErrors() {
        return errors;
    }

}