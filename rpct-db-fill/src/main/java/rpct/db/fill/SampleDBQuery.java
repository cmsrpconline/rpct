package rpct.db.fill;

import java.util.List;

import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipDAO;
import rpct.db.domain.equipment.ChipDAOHibernate;
import rpct.db.domain.equipment.FebConnector;
import rpct.db.domain.equipment.FebConnectorDAO;
import rpct.db.domain.equipment.FebConnectorDAOHibernate;
import rpct.db.domain.equipment.chamberloaction.BarrelOrEndcap;
import rpct.db.domain.equipment.chamberloaction.ChamberLocation;
import rpct.db.domain.equipment.chamberloaction.ChamberLocationDAO;
import rpct.db.domain.equipment.chamberloaction.ChamberLocationDAOHibernate;
import rpct.db.domain.equipment.chamberstrip.ChamberStrip;
import rpct.db.domain.equipment.chamberstrip.ChamberStripDAO;
import rpct.db.domain.equipment.chamberstrip.ChamberStripDAOHibernate;
import rpct.db.domain.equipment.feblocation.FebLocation;
import rpct.db.domain.equipment.feblocation.FebLocationDAO;
import rpct.db.domain.equipment.feblocation.FebLocationDAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;

/**
 * Created on 2006-01-13
 * 
 * @author Michal Pietrusinski
 * @version $Id: SampleDBQuery.java,v 1.13 2007/07/09 13:27:15 tb Exp $
 */
public class SampleDBQuery {

    public SampleDBQuery(HibernateContext condHibernateContext) { 
    }
    
    public static void main(String[] args){ 
        try {                        
            HibernateContext hibernateContext = new SimpleHibernateContextImpl();
            ChipDAO chipDAO = new ChipDAOHibernate(hibernateContext);
            FebLocationDAO febLocationDAO = new FebLocationDAOHibernate(hibernateContext);
            FebConnectorDAO febConnectorDAO = new FebConnectorDAOHibernate(hibernateContext);
            ChamberLocationDAO locationDAO = new ChamberLocationDAOHibernate(hibernateContext);
            ChamberStripDAO chamberStripDAO = new ChamberStripDAOHibernate(hibernateContext);
            List<ChamberLocation> locations = locationDAO.getByLocation(2, 2, 10, "none", BarrelOrEndcap.Barrel);
            for (ChamberLocation location : locations) {
                System.out.println(location.getChamberLocationName());
                List<FebLocation> febLocations = febLocationDAO.getFebsByChamberLocation(location);  //location.getFEBLocations();                
                for (FebLocation febLocation : febLocations) {
                	List<FebConnector> febConnectors = febConnectorDAO.getConnectionsByFebLoc(febLocation);
//                	Chip synCoderChip = febLocation.getLinkBoard().getSynCoder();
                    Board board = febLocation.getLinkBoard();
                    if(board == null) continue;
                    int boardId = board.getId();
    				System.out.println("BoardId: " + boardId + "  " + "FEB Id: " + febLocation.getId());
                    Chip synCoderChip = chipDAO.getSynCoderChipByBoard(board);
                    for (FebConnector febConnector : febConnectors) {
                        System.out.println("Connector Number: " + febConnector.getLinkBoardInputNum() + "  ");
                    	List<ChamberStrip> chamberStrips = chamberStripDAO.getStripsByFebCon(febConnector);
                        for (ChamberStrip chamberStrip : chamberStrips) {
                            System.out.println("Chamber Strip Number: " + chamberStrip.getChamberStripNumber() + "  " +
                            		"Cable Channel Number: " + chamberStrip.getCableChannelNum());
                        }
                    }
    				System.out.println(location.getChamberLocationName() + "  " +
    						"Chip: " + synCoderChip.getId() + "  " +
    						"FEB Location: " + febLocation.getId() + "  " +
							"FEB Local Eta Partition: " + febLocation.getFebLocalEtaPartition() + "  " +
    						"FEB I2C Address: "	+ febLocation.getI2cLocalNumber());
                }
            }
/*        	HibernateContext condHibernateContext = new SimpleCondHibernateContextImpl();
        	ConditionDAO conditionDAO = new ConditionDAOHibernate(condHibernateContext);
        	conditionDAO.setFebCondition(7564, 24, 210,210,210,210,3200,3200,3200,3200,123456);
            condHibernateContext.closeSession();
*/        	
/*        	HibernateContext condHibernateContext = new SimpleCondHibernateContextImpl();
        	ConditionDAO conditionDAO = new ConditionDAOHibernate(condHibernateContext);
        	conditionDAO.setStripResponse(5, 24, 15, 10352);
            condHibernateContext.closeSession();
*/        	


/*        	HibernateContext condHibernateContext = new SimpleCondHibernateContextImpl();
        	ConditionDAO conditionDAO = new ConditionDAOHibernate(condHibernateContext);
//          	StripResponseDAO stripResponseDAO = new StripResponseDAOHibernate(condHibernateContext);
        	List<StripResponse> strips = conditionDAO.getStripResonseByChamberStripId(1);
        	if(strips.isEmpty()) System.out.println("Strips Empty");
            for (StripResponse strip : strips) {
                System.out.println("StripId: " + strip.getId() + "  " +
                		"CS_ChamberStripId: " + strip.getChamberStripId()  + "  " +
                		"Count: " + strip.getCount()  + "  " +
                		"Run: " + strip.getRun()  + "  " +
                		"Unix Time: " + strip.getUnixTime());
            }
            condHibernateContext.closeSession();
*/
/*
            HibernateContext hibernateContext = new SimpleHibernateContextImpl();
            FebLocationDAO febLocationDAO = new FebLocationDAOHibernate(hibernateContext);
            ChamberLocationDAO locationDAO = new ChamberLocationDAOHibernate(hibernateContext);
            //ChamberLocation location = locationDAO.getById(1);
            List<ChamberLocation> locations = locationDAO.getByLocation(2, 2, 34, null, BarrelOrEndcap.Endcap);
            for (ChamberLocation location : locations) {
                System.out.println(location.getChamberLocationName());
                List<FebLocation> febLocations = febLocationDAO.getByLocParameters(location, "B", 1);  //location.getFEBLocations();                
                for (FebLocation febLocation : febLocations) {
    				I2cCbChannel i2cCbChannel = febLocation.getI2cCbChannel();
    				ControlBoard controlBoard = i2cCbChannel.getControlBoard();
    				System.out.println(location.getChamberLocationName() + "  " +
    						"FEB Location: " + febLocation.getId() + "  " +
							"FEB Local Eta Partition: " + febLocation.getFebLocalEtaPartition() + "  " +
    						"FEB I2C Address: "	+ febLocation.getI2cLocalNumber() + "  " +
    						"I2cCbChannel: " + i2cCbChannel.getId() + "  " +
    						"ControlBoardChannel: " + i2cCbChannel.getCbChannel() + "  " +
    						"CCU Address:" + controlBoard.getCcuAddress());
                }
*/
/*            List<ChamberLocation> locations = locationDAO.getByDiskOrWheel(1, "Endcap");
            for (ChamberLocation location : locations) {
                //System.out.println(location.getChamberLocationName());
                List<FEBLocation> febLocations = location.getFEBLocations();
                for (FEBLocation febLocation : febLocations) {
    				DistributionBoard distributionBoard = febLocation.getDistributionBoard();
    				ControlBoard controlBoard = distributionBoard.getControlBoard();
    				System.out.println(location.getChamberLocationName() + "  " +
    						"FEB Location: " + febLocation.getId() + "  " +
    						"FEB I2C Address: "	+ febLocation.getI2CLocalNumber() + "  " +
    						"Distribution board: " + distributionBoard.getId() + "  " +
    						"ControlBoardChannel: " + distributionBoard.getCBChannel() + "  " +
    						"CCU Address:" + controlBoard.getCCUAddress());				
    			
			
            hibernateContext.closeSession();
*/        } catch(Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();            
        }
        System.out.println("Done");
    }

}
