package rpct.db.fill;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.log4j.PropertyConfigurator;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.ConnType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateDAO;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.FebBoard;
import rpct.db.domain.equipment.I2cCbChannel;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkBoardDAO;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.equipment.chamberloaction.BarrelOrEndcap;
import rpct.db.domain.equipment.chamberloaction.ChamberLocation;
import rpct.db.domain.equipment.chamberloaction.ChamberLocationDAO;
import rpct.db.domain.equipment.chamberloaction.ChamberLocationDAOHibernate;
import rpct.db.domain.equipment.chambertype.ChamberType;
import rpct.db.domain.equipment.controlboard.ControlBoard;
import rpct.db.domain.equipment.distributionboard.DistributionBoardDAO;
import rpct.db.domain.equipment.feblocation.FebLocation;
import rpct.db.domain.equipment.feblocation.FebLocationDAO;
import rpct.db.domain.equipment.feblocation.FebLocationDAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;

/**
 * Created on 2010-10-03
 * 
 * @author Karol Bunkowski
 * @version $Id$
 */
public class W0Rb1inToTower2Links {
    private FebLocationDAO daoFebLocation;
    private ChamberLocationDAO chamberLocationDAO;
    private LinkBoardDAO daoLinkBoard;
    
    
    private HibernateContext hibernateContext;
    private EquipmentDAO equipmentDAO;
    private ConfigurationDAO configurationDAO;
    private ConfigurationManager configurationManger;
    
    public W0Rb1inToTower2Links(HibernateContext hibernateContext) {
        this.hibernateContext = hibernateContext;
        this.equipmentDAO = new EquipmentDAOHibernate(this.hibernateContext);
        this.configurationDAO = new ConfigurationDAOHibernate(hibernateContext);
        this.configurationManger = new ConfigurationManager(hibernateContext, equipmentDAO, configurationDAO);
        this.daoFebLocation =  new FebLocationDAOHibernate(hibernateContext);
        this.chamberLocationDAO = new ChamberLocationDAOHibernate(hibernateContext);
    }
    
	void print() throws DataAccessException {
		List<ChamberLocation> chamberLocations = chamberLocationDAO.getByLayer(0, 1, BarrelOrEndcap.Barrel);
		for(ChamberLocation chamberLocation : chamberLocations) {
			System.out.println(chamberLocation.getChamberLocationName() + " FEBZOrnt " + chamberLocation.getFEBZOrnt());
			
			Set<String> febsStr = new TreeSet<String>();
			for(FebLocation febLocation : chamberLocation.getFEBLocations() ) {
				String str = ("	FEB LocalEta:\t" + febLocation.getFebLocalEtaPartition() + " CmsEta\t" + febLocation.getFebCmsEtaPartition() + " " + febLocation.getLinkBoard().getName() ); 
				febsStr.add(str);				
			}
			for(String str : febsStr ) {
				System.out.println(str);
			}
		}
	}
	
	
	void makeLinks(boolean saveToDb) throws DataAccessException {
		List<LinkConn> newlinkConns = new ArrayList<LinkConn>();  
		
		List<ChamberLocation> chamberLocations = chamberLocationDAO.getByLayer(0, 1, BarrelOrEndcap.Barrel);		
		for(ChamberLocation chamberLocation : chamberLocations) {
			System.out.println(chamberLocation.getChamberLocationName() + " FEBZOrnt " + chamberLocation.getFEBZOrnt());
			
			Set<String> febsStr = new TreeSet<String>();
			Map<String, LinkBoard> lbs = new TreeMap<String, LinkBoard>();
			for(FebLocation febLocation : chamberLocation.getFEBLocations() ) {
				String str = ("	FEB LocalEta:\t" + febLocation.getFebLocalEtaPartition() + " CmsEta\t" + febLocation.getFebCmsEtaPartition() + " " + febLocation.getLinkBoard().getName() ); 
				febsStr.add(str);	
				
				lbs.put(febLocation.getLinkBoard().getName(), febLocation.getLinkBoard());
			}
			for(String str : febsStr ) {
				System.out.println(str);
			}
			
			for(Map.Entry<String, LinkBoard> e : lbs.entrySet()) {
                LinkBoard lb = (LinkBoard)e.getValue();
				for(LinkConn linkConn : lb.getMaster().getLinkConns()) {
					TriggerCrate tc = linkConn.getTriggerBoard().getTriggerCrate();
					
					int newLinkInputNum = 0;
					if(lb.getChamberLocations().get(0).getSector() == tc.getLogSector() + 1)
						newLinkInputNum = 7;
					else if(lb.getChamberLocations().get(0).getSector() == (tc.getLogSector() + 2)%12)
						newLinkInputNum = 10;				
					else if(lb.getChamberLocations().get(0).getSector() == 12 && tc.getLogSector() == 10)
						newLinkInputNum = 10;
					
					String selectedTb = "null";
					if(lb.getFEBLocations().get(0).getFebCmsEtaPartition().equals("1")) {
						selectedTb = "n1";
					}
					else if(lb.getFEBLocations().get(0).getFebCmsEtaPartition().equals("3")) {
						selectedTb = "p1";
					}
					for(TriggerBoard tb : ((TriggerCrate)tc).getTriggerBoards()) {				
						if(tb.getName().contains(selectedTb)) {
							System.out.println("\n"+ tb.getName());

							LinkConn newLinkConn =  new LinkConn();
							newLinkConn.setBoard(lb.getMaster());
							//newLinkConn.setDisabled(true);
							newLinkConn.setTriggerBoard(tb);
							newLinkConn.setTriggerBoardInputNum(newLinkInputNum);
							newLinkConn.setType(ConnType.LINKCONN);

							newlinkConns.add(newLinkConn);
							if(saveToDb) {
								equipmentDAO.saveObject(newLinkConn);
								//configurationManger.enableLinkConn(newLinkConn, true);
							}
							
							for(int tbInputNum =  0; tbInputNum < 18; tbInputNum++) {
								System.out.println(tbInputNum + " " + tb.getLinkConn(tbInputNum) + " " + (tbInputNum == newLinkInputNum ? "<--" + lb.getName() : ""));
							}
						}
					}
				}
			}
			
			System.out.println("\n");
		}
		
		System.out.println("\n\n");
		for(LinkConn linkConn : newlinkConns) {
			System.out.println(linkConn);
		}
			
	}
	
	void printTBsLinksInputs() throws DataAccessException {
		for(Crate tc : equipmentDAO.getCratesByType(CrateType.TRIGGERCRATE, true)) {
			for(TriggerBoard tb : ((TriggerCrate)tc).getTriggerBoards()) {
				if(tb.getName().contains("p1") || tb.getName().contains("n1")) {
					System.out.println("\n"+ tb.getName());
					for(int tbInputNum =  0; tbInputNum < 18; tbInputNum++)
						System.out.println(tb.getLinkConn(tbInputNum));
				}
			}
		}
	}

	public static void main(String[] args) throws DataAccessException {
		PropertyConfigurator.configure("log4j.properties");
        HibernateContext hibernateContext = new SimpleHibernateContextImpl();
        
		try {
			W0Rb1inToTower2Links generator = new W0Rb1inToTower2Links(hibernateContext);
			//generator.print();
			
			//generator.printTBsLinksInputs();
			
			generator.makeLinks(false);
			
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
			hibernateContext.rollback();
		} finally {
			hibernateContext.closeSession();
		}
	}

}
