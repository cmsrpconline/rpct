package rpct.db.fill;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PerfTest {
    
    static class ElementBase {
        private int id;
        private String name;
        public ElementBase(int id, String name) {
            super();
            this.id = id;
            this.name = name;
        }
        public int getId() {
            return id;
        }
        public void setId(int id) {
            this.id = id;
        }
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
    }
    
    static class Element extends ElementBase {

        public Element(int id, String name) {
            super(id, name);
        }        
    }
    
    public static class Container {
        private int id;
        private String name;    
        private Map<String, ElementBase> map = new HashMap<String, ElementBase>();
        private List<ElementBase> list = new ArrayList<ElementBase>();
        public Container(int id, String name) {
            super();
            this.id = id;
            this.name = name;
        }
        public void addElement(int id, String name) {
        	Element e = new Element(id, name);
            map.put(name, e);
            list.add(e);
        }
        public int getId() {
            return id;
        }
        public void setId(int id) {
            this.id = id;
        }
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
        public Map<String, ElementBase> getMap() {
            return map;
        }
        public void setMap(Map<String, ElementBase> map) {
            this.map = map;
        }
        public ElementBase getElementByName(String name) {
            return map.get(name);
        }
		public List<ElementBase> getList() {
			return list;
		}
		public void setList(List<ElementBase> list) {
			this.list = list;
		}
    }
    
    public static void main(String[] args) {
        System.out.println("Start:" );
        long milis = System.currentTimeMillis();
        Container c = new Container(1, "c");
        for (int i = 0; i < 100000; i++) {
            c.addElement(i, Integer.toString(i));
        }
        for (int n = 0; n < 100; n++) { 
            int totalNameLength = 0;
            for (int i = 0; i < 100000; i++) {
                totalNameLength += c.getElementByName(Integer.toString(i)).getName().length();
            }
            /*for (ElementBase e : c.getList()) {
            	totalNameLength += e.getName().length();
            }*/
            //System.out.println("totalNameLength = " + totalNameLength);
            /*if ((n % 10) == 1) {
                System.out.println("Done " + (System.currentTimeMillis() - milis));
            }*/
        }
        System.out.println("Done " + (System.currentTimeMillis() - milis));
    }
}
