package rpct.db.fill;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.apache.axis.types.HexBinary;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ChipConfAssignment;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.SynCoderConf;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;

public class ImportLinkBoardSettingsXml {
    
    static Log log = LogFactory.getLog(ImportLinkBoardSettingsXml.class);
    
    final static QName SETTINGS_TAG = QName.valueOf("settings");
    final static QName LOCAL_CONFIG_KEY_ATTR = QName.valueOf("localConfigKey");
    
    final static QName LINKBOARD_TAG = QName.valueOf("linkBoard");
    //final static QName LINKBOARD_ID_ATTR = QName.valueOf("id");
    final static QName LINKBOARD_NAME_ATTR = QName.valueOf("name");
    final static QName LINKBOARD_WINO_ATTR = QName.valueOf("winO");
    final static QName LINKBOARD_WINC_ATTR = QName.valueOf("winC");
    final static QName LINKBOARD_INV_CLOCK_ATTR = QName.valueOf("invClock");
    final static QName LINKBOARD_LMUXIN_DELAY_ATTR = QName.valueOf("lMuxInDelay");
    final static QName LINKBOARD_RBC_DELAY_ATTR = QName.valueOf("rbcDelay");
    final static QName LINKBOARD_BCN0_DELAY_ATTR = QName.valueOf("bcn0Delay");
    final static QName LINKBOARD_DATATRG_DELAY_ATTR = QName.valueOf("dataTrgDelay");
    final static QName LINKBOARD_DATADAQ_DELAY_ATTR = QName.valueOf("dataDaqDelay");
    final static QName LINKBOARD_PULSER_TIMER_TRG_DELAY_ATTR = QName.valueOf("pulserTimerTrgDelay");
    final static QName LINKBOARD_IN_CHANNELS_ENA_ATTR = QName.valueOf("inChannelsEna");
    final static QName LINKBOARD_DESC_ATTR = QName.valueOf("desc");
        
    private XMLInputFactory inputFactory;
    private HibernateContext hibernateContext;
    private EquipmentDAO equipmentDAO;
    private ConfigurationDAO configurationDAO;

    private LocalConfigKey localConfigKey;
    private Map<String, String> descNameMap = new HashMap<String, String>();
    private boolean readOnly;
    private Map<LinkBoard, SynCoderConf> settings = new HashMap<LinkBoard, SynCoderConf>();
    
    public ImportLinkBoardSettingsXml(HibernateContext hibernateContext) {
        this.hibernateContext = hibernateContext;
        this.equipmentDAO = new EquipmentDAOHibernate(hibernateContext);
        this.configurationDAO =  new ConfigurationDAOHibernate(hibernateContext);
        inputFactory = XMLInputFactory.newInstance();
        inputFactory.setProperty("javax.xml.stream.isValidating", Boolean.FALSE);
        inputFactory.setProperty("javax.xml.stream.isCoalescing", Boolean.TRUE);
        //inputFactory.setProperty("javax.xml.stream.isNamespaceAware", Boolean.FALSE);

        descNameMap.put("lb RB4-_F", "");
    }    
    
    public boolean isReadOnly() {
        return readOnly;
    }
    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    public LocalConfigKey getLocalConfigKey() {
        return localConfigKey;
    }

    public Map<LinkBoard, SynCoderConf> getSettings() {
        return settings;
    }

    private void processSettingsElement(StartElement se) throws DataAccessException {  
        if (localConfigKey != null) {
            throw new IllegalStateException("<settings> found while localConfigKey is already initialized");
        }            
        String keyString = se.getAttributeByName(LOCAL_CONFIG_KEY_ATTR).getValue();
        log.info("localConfigKey = " + keyString);
        localConfigKey = configurationDAO.getLocalConfigKeyByName(keyString);
        if (localConfigKey == null) {
            log.info("Creating new localConfigKey in the database: " + keyString);
            localConfigKey = new LocalConfigKey();
            localConfigKey.setName(keyString);
            configurationDAO.saveObject(localConfigKey);
            configurationDAO.flush();            
        }
        log.info("localConfigKey = " + keyString);
    }

    private int getAttributeAsInt(StartElement se, QName attr) {
        return Integer.parseInt(se.getAttributeByName(attr).getValue());
    }
    private boolean getAttributeAsBoolean(StartElement se, QName attr) {
        return Boolean.parseBoolean(se.getAttributeByName(attr).getValue());
    }
    private byte[] getAttributeAsByteArray(StartElement se, QName attr) {        
        return HexBinary.decode(se.getAttributeByName(attr).getValue());
    }
    
    private void processLinkBoardElement(StartElement se) throws DataAccessException {
        String lbName;
        if (se.getAttributeByName(LINKBOARD_NAME_ATTR) != null) {
            lbName = se.getAttributeByName(LINKBOARD_NAME_ATTR).getValue();
        }
        else {
            String desc = se.getAttributeByName(LINKBOARD_DESC_ATTR).getValue(); 
            lbName = descNameMap.get(desc);
            if (lbName == null) {
                throw new IllegalArgumentException("LinkBoard name not found for description " + desc);
            }
        }
        LinkBoard linkBoard = (LinkBoard) equipmentDAO.getBoardByName(lbName);  
        if(linkBoard == null) {
            throw new RuntimeException("no lb found for the name " + lbName);
        }
        log.info("Importing settings for " + linkBoard.getName());
        Chip synCoder = linkBoard.getSynCoder();
        ChipConfAssignment chipConfAssignment = configurationDAO.getChipConfAssignment(synCoder, localConfigKey);
        if (chipConfAssignment == null) {
            chipConfAssignment = new ChipConfAssignment();
            chipConfAssignment.setChip(synCoder);
            chipConfAssignment.setLocalConfigKey(localConfigKey);
        }
        SynCoderConf synCoderConf = new SynCoderConf();
        synCoderConf.setWindowOpen(getAttributeAsInt(se, LINKBOARD_WINO_ATTR));
        synCoderConf.setWindowClose(getAttributeAsInt(se, LINKBOARD_WINC_ATTR));
        synCoderConf.setInvertClock(getAttributeAsBoolean(se, LINKBOARD_INV_CLOCK_ATTR));
        synCoderConf.setLmuxInDelay(getAttributeAsInt(se, LINKBOARD_LMUXIN_DELAY_ATTR));
        synCoderConf.setRbcDelay(getAttributeAsInt(se, LINKBOARD_RBC_DELAY_ATTR));
        synCoderConf.setBcn0Delay(getAttributeAsInt(se, LINKBOARD_BCN0_DELAY_ATTR));
        synCoderConf.setDataTrgDelay(getAttributeAsInt(se, LINKBOARD_DATATRG_DELAY_ATTR));
        synCoderConf.setDataDaqDelay(getAttributeAsInt(se, LINKBOARD_DATADAQ_DELAY_ATTR));
        synCoderConf.setPulserTimerTrgDelay(getAttributeAsInt(se, LINKBOARD_PULSER_TIMER_TRG_DELAY_ATTR));
        if (se.getAttributeByName(LINKBOARD_IN_CHANNELS_ENA_ATTR) != null) {
            synCoderConf.setInChannelsEna(getAttributeAsByteArray(se, LINKBOARD_IN_CHANNELS_ENA_ATTR));
        }
        else {
            synCoderConf.setInChannelsEna(new byte[] {});
        }
        chipConfAssignment.setStaticConfiguration(synCoderConf);
        configurationDAO.saveObject(synCoderConf);
        configurationDAO.flush();
        configurationDAO.saveObject(chipConfAssignment);  
        settings.put(linkBoard, synCoderConf);
    }
    
    public void importSettings(String fileName) throws FileNotFoundException, XMLStreamException, DataAccessException {  

        log.info("Importing file " + fileName);
        localConfigKey = null;
        settings.clear();
        XMLEventReader r = inputFactory.createXMLEventReader(fileName, new FileInputStream(fileName));
        while (r.hasNext()) {
            XMLEvent event = r.nextEvent();
            switch (event.getEventType()) {
            case XMLEvent.START_ELEMENT:
                StartElement se = event.asStartElement();                
                log.info(se.getName());
                if (SETTINGS_TAG.equals(se.getName())) {
                    processSettingsElement(se);
                }
                else if (LINKBOARD_TAG.equals(se.getName())) {
                    processLinkBoardElement(se);        
                }
                break;
            }
        }    
        if (readOnly) {
            hibernateContext.rollback();
        }
        else {
            hibernateContext.closeSession();
        }
        log.info("Impored file " + fileName + " successfully");
    }
    
    
    public static void main(String[] args) {
        try {
            /*if (args.length == 0) {
                throw new IllegalArgumentException("No input file specified");
            }

            HibernateContext hibernateContext = new SimpleHibernateContextImpl();
            ImportLinkBoardSettingsXml imp = new ImportLinkBoardSettingsXml(hibernateContext);
            
            for (String filename : args) {
                imp.importSettings(filename);           
            } */        
            
            HibernateContext hibernateContext = new SimpleHibernateContextImpl();
            ImportLinkBoardSettingsXml imp = new ImportLinkBoardSettingsXml(hibernateContext);
            
            String filename = "lbSettingsW-1.xml";
            //String filename = "lbSettingsW0.xml";
            ///String filename = "lbSettingsW+1.xml";
            imp.importSettings(filename);

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (XMLStreamException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
