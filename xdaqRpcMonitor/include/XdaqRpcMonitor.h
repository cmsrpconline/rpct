#ifndef _XdaqRpcMonitor_h_
#define _XdaqRpcMonitor_h_

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/exception/Exception.h" 
#include "xdaq/NamespaceURI.h"
 
#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"

#include "xdata/Serializable.h"
#include "xdata/UnsignedLong.h"
#include "xdata/rpctsoap/Serializer.h"
#include "xdata/String.h"
#include "xdata/Table.h"
#include "xdata/Float.h"
#include "xdata/Integer.h"
#include "xdata/Boolean.h"
#include "xdata/InfoSpace.h"
#include "xdata/Vector.h"

#include "xgi/Utils.h"
#include "xgi/Method.h"

#include "rpct/xdaqlboxaccess/XdaqLBoxAccessClient.h"
#include "rpct/xdaqutils/XdaqRpcSupervisorClient.h"
#include "rpct/xdaqutils/XdaqDbServiceClient.h"
#include "rpct/xdaqdiagaccess/XdaqDiagAccessClient.h"

#include "toolbox/task/Timer.h"

#include "cgicc/HTMLClasses.h"
class XdaqRpcMonitor: public xdaq::Application, public toolbox::task::TimerListener
{
	private:
	
    rpct::xdaqutils::XdaqDbServiceClient dbServiceClient;
    rpct::XdaqLBoxAccessClient lboxAccessClient;
    rpct::xdaqdiagaccess::XdaqDiagAccessClient diagAccessClient;
    rpct::XdaqLBoxAccessClient::MassiveReadResponseBag* monitorSystem(
    	rpct::xdaqutils::XdaqDbServiceClient::FebAccessInfoVector* febAccessInfoVector, xdata::Boolean& endcap);
    rpct::XdaqLBoxAccessClient::MassiveReadResponseBag* lastSelectiveReadResponseBag;
    rpct::XdaqLBoxAccessClient::MassiveReadResponseBag* lastSfotReadResponseBag;
    rpct::XdaqLBoxAccessClient::MassiveReadResponseBag* cumulativeReadResponseBag;
    void makeMenu(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
    void makeCell(xgi::Input* in, xgi::Output* out, xdata::Float& data,
            std::string& color, xdata::String& chamberLocationName, xdata::Integer& xdaqAppInstance,
    	xdata::String& febLocalEtaPartition, xdata::Integer& ccuAddress, xdata::Integer& channel,
    	xdata::Integer& address, xdata::Boolean& endcap) throw (xgi::exception::Exception);
    void makeCell(xgi::Input* in, xgi::Output* out, xdata::String& data) throw (xgi::exception::Exception);
    void makeCell(xgi::Input* in, xgi::Output* out, xdata::Integer& data) throw (xgi::exception::Exception);
    void makeCell(xgi::Input* in, xgi::Output* out, xdata::Float& data) throw (xgi::exception::Exception);
    void makeCell(xgi::Input* in, xgi::Output* out, xdata::UnsignedLong& data) throw (xgi::exception::Exception);
    void makeColHead(xgi::Input* in, xgi::Output* out, std::string header) throw (xgi::exception::Exception);
    rpct::xdaqutils::XdaqDbServiceClient::FebAccessInfoVector* getFebMap(int& grouping, int& disk, int& layer,
    	int& sector, std::string& subsector, std::string& barrelOrEndcap, std::string& febLocalEtaPartition, int& posInLocalEtaPartition);
	void setGraphLimits();
	void linkBoardCounters(int instance, int chipId, double rate[2][96]) throw (xgi::exception::Exception);

	int singleFebXdaqAppInstance, singleFebCcuAddress, singleFebChannel, singleFebAddress, sfotXdaqAppInstance, sfotCcuAddress, sfotChannel, sfotAddress;
	std::string singleFebChamberLocationName, singleFebLocalEtaPartition, sfotChamberLocationName, sfotLocalEtaPartition;
	xdata::Boolean singleFebEndcap, sfotEndcap;
    xdata::Integer limits[18];

    double upperTemp[20];
    double lowerTemp[20];
    double upperTh1[20];
    double lowerTh1[20];
    double upperTh2[20];
    double lowerTh2[20];
    double upperTh3[20];
    double lowerTh3[20];
    double upperTh4[20];
    double lowerTh4[20];
    double upperVmon1[20];
    double lowerVmon1[20];
    double upperVmon2[20];
    double lowerVmon2[20];
    double upperVmon3[20];
    double lowerVmon3[20];
    double upperVmon4[20];
    double lowerVmon4[20];

	double sfotTh1[20];
	double sfotTh2[20];
	double sfotTh3[20];
	double sfotTh4[20];
	double sfotVmon1[20];
	double sfotVmon2[20];
	double sfotVmon3[20];
	double sfotVmon4[20];
	double sfotTemp[20];
	double sfotTimes[20];
	
	std::map<int, int> histoData;
		
	void wait(int period){
		time_t start_time, cur_time;
		time(&start_time);
		do
		{
		  time(&cur_time);
		}
		while((cur_time - start_time) < period);
	}
	
	int round(float x){return static_cast<int>((x > 0.0) ? (x + 0.5) : (x - 0.5));}
	xdata::String picPath;
//	xdata::String xdaqRoot;
	
	
	public:
	
    static const char* RPCT_RPC_MONITOR_NS;
    static const char* RPCT_RPC_MONITOR_PREFIX;

	XDAQ_INSTANTIATOR();
	
	XdaqRpcMonitor(xdaq::ApplicationStub * s) throw (xdaq::exception::Exception);
	virtual ~XdaqRpcMonitor();
	
	void Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
    void febMonitoring(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
    void linkBoardMonitoring(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
    void detectorGui(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
    void diskPage(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
    void wheelPage(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
    void defineArea(xgi::Input* in, xgi::Output* out, int x1, int y1,int x2, int y2, int x3,
     int y3, int x4, int y4, std::string lbOrFebMonitoring, int disk, int layer, int sector, std::string subSector, std::string barrelOrEndcap)
     throw (xgi::exception::Exception);
	void XdaqRpcMonitor::resultsTable(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
    void monitorSystem(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
	void cellInfo(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
	void singleFebOverTime(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void start(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void stop(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void timeExpired (toolbox::task::TimerEvent& e);
	void histogram(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
	void saveHistogram(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
	void test(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);

	protected:
	
	xdata::UnsignedLong counter_;
	xdata::String lastExecutionTime_;
	xdata::InfoSpace* infoSpace_;
	xdata::UnsignedLong executionUnixTime_;
	xdata::UnsignedLong unixTimeStripResponse;
	toolbox::task::Timer* timer_;
	rpct::xdaqutils::XdaqRpcSupervisorClient::FebStateOverTimeVector febStateOverTimeVector_;
	short switch_;
	
	
};

#endif
