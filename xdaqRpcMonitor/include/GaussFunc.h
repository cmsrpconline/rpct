//Written by William Whitaker 20060719
#ifndef _GaussFunc_h_
#define _GaussFunc_h_

#include <math.h>
#include "Fit.h"

class GaussFunc  
{
	public:
	static double gfunc(double a[]);
	static void dgfunc(double a[],double df[]);	
};
double GaussFunc::gfunc(double a[])
{
 uint i;
 double chi=0.0;

 for(i=0;i<freqVec.size();i++)
 {
   double eq1=0.0;
   double eq2=0.0;
   eq1=freqVec[i]-(a[1]*exp((-1./2.)*((binCenVec[i]-a[2])/a[3])*((binCenVec[i]-a[2])/a[3])));
   eq2=eq1*eq1;
   chi=chi+eq2;
 }
 return chi;
}

void GaussFunc::dgfunc(double a[],double df[])
{
  uint i;
  double eq1, eq2, da1, da2, da3;
  double db1=0.0;
  double db2=0.0;
  double db3=0.0;
  for(i=0;i<hisSize;i++)
  {
	eq1=0.0;
	eq2=0.0;
	da1=0.0;
	da2=0.0;
	da3=0.0;
	eq1=(exp((-1./2.)*((binCenVec[i]-a[2])/a[3])*((binCenVec[i]-a[2])/a[3])));
	eq2=freqVec[i]-(a[1]*eq1);
	da1=-2.*eq1*eq2;
	da2=a[1]*da1*((binCenVec[i]-a[2])/(a[3]*a[3]));
	da3=a[1]*da1*(((binCenVec[i]-a[2])*(binCenVec[i]-a[2]))/(a[3]*a[3]*a[3]));
	db1=db1+da1;
	db2=db2+da2;
	db3=db3+da3;
  }
  df[1]=db1;
  df[2]=db2;
  df[3]=db3;
}

#endif
