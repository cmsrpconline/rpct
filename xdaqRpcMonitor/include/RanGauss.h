//Written by William Whitaker using "Numerical Recipes in C" functions
//20060719
#ifndef _RanGaus_h_
#define _RanGaus_h_

#include <math.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <vector>

#include "Fit.h"

#define IA 16807
#define IM 2147483647
#define AM (1.0/IM)
#define IQ 127773
#define IR 2836
#define NTAB 32
#define NDIV (1+(IM-1)/NTAB)
#define EPS 1.2e-7
#define RNMX (1.0-EPS)

long idum=-1;

double gasdev(long *idum);
double ran1(long *idum);

void gendis(int points, int mean, int sigma){
	double a[]={0.,10.,0.,5.};
	double eq, j=1.;
	int i;
	long *ptr=&idum;
	maxStrips = points;
	FILE *fp = fopen("/home/billw/root_gaus/root/data.txt","w");
	for(i=0;i<maxStrips;i++)
	{
		xStripData.push_back(j);
		eq=10*exp(-(1./2.)*((xStripData[i]-a[2])/a[3])*((xStripData[i]-a[2])/a[3]));
		yStripData.push_back(mean+sigma*gasdev(ptr));
		j=j+1.;
	}
	fclose(fp);
}

double gasdev(long *idum)
{
	double ran1(long *idum);
	static int iset=0;
	static double gset;
	double fac,rsq,v1,v2;

	if (*idum < 0) iset=0;
	if  (iset == 0) {
		do {
			v1=2.0*ran1(idum)-1.0;
			v2=2.0*ran1(idum)-1.0;
			rsq=v1*v1+v2*v2;
		} while (rsq >= 1.0 || rsq == 0.0);
		fac=sqrt(-2.0*log(rsq)/rsq);
		gset=v1*fac;
		iset=1;
		return v2*fac;
	} else {
		iset=0;
		return gset;
	}
}

double ran1(long *idum)
{
	int j;
	long k;
	static long iy=0;
	static long iv[NTAB];
	double temp;

	if (*idum <= 0 || !iy) {
		if (-(*idum) < 1) *idum=1;
		else *idum = -(*idum);
		for (j=NTAB+7;j>=0;j--) {
			k=(*idum)/IQ;
			*idum=IA*(*idum-k*IQ)-IR*k;
			if (*idum < 0) *idum += IM;
			if (j < NTAB) iv[j] = *idum;
		}
		iy=iv[0];
	}
	k=(*idum)/IQ;
	*idum=IA*(*idum-k*IQ)-IR*k;
	if (*idum < 0) *idum += IM;
	j=iy/NDIV;
	iy=iv[j];
	iv[j] = *idum;
	if ((temp=AM*iy) > RNMX) return RNMX;
	else return temp;
}
#undef IA
#undef IM
#undef AM
#undef IQ
#undef IR
#undef NTAB
#undef NDIV
#undef EPS
#undef RNMX

#endif
