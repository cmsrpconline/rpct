#ifndef _LorentzFunc_h_
#define _LorentzFunc_h_
#include <math.h>
#include "Fit.h"

class LorentzFunc  
{
	public:
	static double lfunc(double a[]);
	static void dlfunc(double a[],double df[]);	
	private:
	
};
double LorentzFunc::lfunc(double a[])
{
 uint i;
 double PI=3.1415926535898;
 double chi=0.0;

 for(i=0;i<freqVec.size();i++)
 {
   double eq1=0.0;
   double eq2=0.0;
   eq1=freqVec[i]-((a[1]/PI)*(a[2]/(((binCenVec[i]-a[3])*(binCenVec[i]-a[3]))+(a[2]*a[2]))));
   eq2=eq1*eq1;
   chi=chi+eq2;
 }
 return chi;
}

void LorentzFunc::dlfunc(double a[],double df[])
{
  uint i;
  double PI=3.1415926535898;
  double eq1, eq2, da1, da2, da3;
  double db1=0.0;
  double db2=0.0;
  double db3=0.0;
  for(i=0;i<hisSize;i++)
  {
	eq1=0.0;
	eq2=0.0;
	da1=0.0;
	da2=0.0;
	da3=0.0;
	eq1=((a[1]/PI)*(a[2]/(((binCenVec[i]-a[3])*(binCenVec[i]-a[3]))+(a[2]*a[2]))));
	eq2=freqVec[i]-eq1;
	da1=-2*(eq1/a[1])*eq2;
	da2=2.*((2*(PI/a[1])*eq1*eq1)-(eq1/a[2]))*eq2;
	da3=((-4*PI)/(a[1]*a[2]))*eq1*eq1*eq2*(binCenVec[i]-a[3]);
	db1=db1+da1;
	db2=db2+da2;
	db3=db3+da3;
  }
  df[1]=db1;
  df[2]=db2;
  df[3]=db3;
}

#endif
