#ifndef _RPCTHISTOINFO_H_
#define _RPCTHISTOINFO_H_


#include "xdata/Bag.h"
#include "xdata/Integer.h"
#include "xdata/Float.h"
#include "xdata/String.h"
#include "xdata/UnsignedLong.h"
namespace rpct {

class HistoInfo {
private:
    xdata::String febLocalEtaPartition;
    xdata::String chamberLocationName;
    xdata::Integer chamberStripNumber;
    xdata::Integer count;
	xdata::String executionTime;
	xdata::UnsignedLong executionUnixTime;
	
    
public:
    void registerFields(xdata::Bag<HistoInfo>* bag) {
        bag->addField("febLocalEtaPartition", &febLocalEtaPartition);
        bag->addField("chamberLocationName", &chamberLocationName);
        bag->addField("chamberStripNumber", &chamberStripNumber);
        bag->addField("count", &count);
		bag->addField("executionTime", &executionTime);
		bag->addField("executionUnixTime", &executionUnixTime);
		
    }
    
    xdata::String& getFebLocalEtaPartition() {
        return febLocalEtaPartition;
    }
    
    void setFebLocalEtaPartition(xdata::String febLocalEtaPartition) {
        this->febLocalEtaPartition = febLocalEtaPartition;
    }
    
    xdata::String& getChamberLocationName() {
        return chamberLocationName;
    }
    
    void setChamberLocationName(xdata::String chamberLocationName) {
        this->chamberLocationName = chamberLocationName;
    }
    
     xdata::Integer& getChamberStripNumber() {
        return chamberStripNumber;
    }
    
    void setChamberStripNumber(xdata::Integer chamberStripNumber) {
        this->chamberStripNumber = chamberStripNumber;
    }
    
    xdata::Integer& getCount() {
        return count;
    }
    
    void setCount(xdata::Integer count) {
        this->count = count;
    }
    
    xdata::String& getExecutionTime() {
        return executionTime;
    }
    
    void setExecutionTime(xdata::String executionTime) {
        this->executionTime = executionTime;
    }
    
    xdata::UnsignedLong& getExecutionUnixTime() {
        return executionUnixTime;
    }
    
    void setExecutionUnixTime(xdata::UnsignedLong executionUnixTime) {
        this->executionUnixTime = executionUnixTime;
    }
};

}

#endif /*_RPCTHISTOINFO_H_*/
