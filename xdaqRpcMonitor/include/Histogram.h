#ifndef _Histogram_h_
#define _Histogram_h_

#include <valarray>
#include <cassert>


class THistogram  
{
public:
	THistogram(uint nCounters = 10);
	virtual ~THistogram()				{ Clear();};
	void Compute( const std::vector<double>& v, bool bComputeMinMax = true);
	void Update( const std::vector<double>& v);
	void Update( const double& t);
	void Resize( uint nCounters );
	void Clear(){m_vCounters.clear();};
	void SetMinSpectrum( const double& tMin ){m_tMin = tMin; ComputeStep();};
	void SetMaxSpectrum( const double& tMax ){m_tMax = tMax; ComputeStep();};
	const double& GetMinSpectrum() const{	return m_tMin;};
	const double& GetMaxSpectrum() const{	return m_tMax;};
	double GetStep() const{	return m_dStep;};
	uint GetSum() const;
	double GetArea() const;

	static void GetMoments(const std::vector<double>& vData, double& fAve, double& fAdev, double& fSdev, double& fVar, double& fSkew, double& fKurt);

	uint GetSize() const{	return m_vCounters.size();};
	uint operator [] (uint i) const{ assert( i < m_vCounters.size() ); return m_vCounters[i];};
	const std::vector<uint>& GetHistogram() const{	return m_vCounters;};
	std::vector<double> GetHistogramD() const;
	std::vector<double> GetNormalizedHistogram() const;
	std::vector<double> GetLeftContainers() const;
	std::vector<double> GetCenterContainers() const;

protected:
	void ComputeStep(){
		m_dStep = (double)(((double)(m_tMax-m_tMin)) / (m_vCounters.size()-1));
	}
	std::vector<uint> m_vCounters;
	double m_tMin;
	double m_tMax;
	double m_dStep;
};

THistogram::THistogram(uint nCounters)
: m_vCounters(nCounters,0), m_tMin(0), m_tMax(0), m_dStep(0)
{

}

void THistogram::Resize( uint nCounters )
{
	Clear();

	m_vCounters.resize(nCounters,0);

	ComputeStep();
}

void THistogram::Compute( const std::vector<double>& v, bool bComputeMinMax)
{
	using namespace std;
	uint i;
	int index;

	if (m_vCounters.empty())
		return;

	if (bComputeMinMax)
	{
		m_tMax = m_tMin = v[0];
		for (i=1;i<v.size();i++)
		{
			if(m_tMax<v[i])	m_tMax = v[i];
			if(m_tMin>v[i]) m_tMin = v[i];
		}
	}

	ComputeStep();

	for (i = 0;i < v.size() ; i++)
	{
		index=(int) floor( ((double)(v[i]-m_tMin))/m_dStep ) ;

		if (index >= m_vCounters.size() || index < 0)
			return;

		m_vCounters[index]++;
	}
}

void THistogram::Update( const std::vector<double>& v)
{
	if (m_vCounters.empty())
		return;

	ComputeStep();

	double uSize = m_vCounters.size();

	int index;
	for (uint i = 0;i < uSize ; i++)
	{
		index = (int)floor(((double)(v[i]-m_tMin))/m_dStep);

		if (index >= m_vCounters.size() || index < 0)
			return;

		m_vCounters[index]++;
	}
}

void THistogram::Update( const double& t)
{	
	int index=(int) floor( ((double)(t-m_tMin))/m_dStep ) ;

	if (index >= m_vCounters.size() || index < 0)
		return;

	m_vCounters[index]++;
};

std::vector<double> THistogram::GetHistogramD() const
{
	std::vector<double> v(m_vCounters.size());
	for (uint i = 0;i<m_vCounters.size(); i++)
		v[i]=(double)m_vCounters[i];

	return v;
}

std::vector<double> THistogram::GetLeftContainers() const
{
	std::vector<double> vX( m_vCounters.size());

	for (uint i = 0;i<m_vCounters.size(); i++)
		vX[i]= m_tMin + i*m_dStep;

	return vX;
}

std::vector<double> THistogram::GetCenterContainers() const
{
	std::vector<double> vX( m_vCounters.size());

	for (uint i = 0;i<m_vCounters.size(); i++)
		vX[i]= m_tMin + (i+0.5)*m_dStep;

	return vX;
}

uint THistogram::GetSum() const
{
	uint uSum = 0;
	for (uint i = 0;i<m_vCounters.size(); i++)
		uSum+=m_vCounters[i];

	return uSum;
}

double THistogram::GetArea() const
{
	const size_t n=m_vCounters.size();
	double area=0;

	if (n>6)
	{
		area=3.0/8.0*(m_vCounters[0]+m_vCounters[n-1])
		+7.0/6.0*(m_vCounters[1]+m_vCounters[n-2])
		+23.0/24.0*(m_vCounters[2]+m_vCounters[n-3]);
		for (uint i=3;i<n-3;i++)
		{
			area+=m_vCounters[i];
		}
	}
	else if (n>4)
	{
		area=5.0/12.0*(m_vCounters[0]+m_vCounters[n-1])
		+13.0/12.0*(m_vCounters[1]+m_vCounters[n-2]);
		for (uint i=2;i<n-2;i++)
		{
			area+=m_vCounters[i];
		}
	}
	else if (n>1)
	{
		area=1/2.0*(m_vCounters[0]+m_vCounters[n-1]);
		for (uint i=1;i<n-1;i++)
		{
			area+=m_vCounters[i];
		}
	}
	else 
		area=0;

	return area*m_dStep;
}

std::vector<double> THistogram::GetNormalizedHistogram() const
{
	std::vector<double> vNormCounters( m_vCounters.size());
	double dArea = (double)GetArea();

	for (uint i = 0;i<m_vCounters.size(); i++)
	{
		vNormCounters[i]= (double)m_vCounters[i]/dArea;
	}

	return vNormCounters;
};

void THistogram::GetMoments(const std::vector<double>& vData, double& fAve, double& fAdev, double& fSdev, double& fVar, double& fSkew, double& fKurt)
{
	int j;
	double ep=0.0,s,p;
	const size_t n = vData.size();

	if (n <= 1)
		return;

	s=0.0;
	for (j=0;j<n;j++){
		s += vData[j];
	}
	
	fAve=s/(n);
	fAdev=fVar=fSkew=fKurt=0.0; 
/*	printf("Sum: %6.1f\n",s);
	printf("Points: %d\n",n);
	printf("Mean_1: %6.1f\n",fAve);
*/
		for (j=0;j<n;j++) 
		{
			fAdev += fabs(s=vData[j]-(fAve));
			ep += s;
			fVar += (p=s*s);
			fSkew += (p *= s);
			fKurt += (p *= s);
		}


	fAdev /= n;
	fVar=(fVar-ep*ep/n)/(n-1);
	fSdev=sqrt(fVar);
	if (fVar) 
	{
		fSkew /= (n*(fVar)*(fSdev));
		fKurt=(fKurt)/(n*(fVar)*(fVar))-3.0;
	} 
	else
		return;
}

#endif
