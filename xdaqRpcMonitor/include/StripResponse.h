//Written by Michal Pietrusinski & William Whitaker 20060719
#ifndef _StripResponse_h_
#define _StripResponse_h_

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/exception/Exception.h" 
#include "xdaq/NamespaceURI.h"
 
#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"

#include "xdata/UnsignedLong.h"
#include "xdata/rpctsoap/Serializer.h"
#include "xdata/String.h"
#include "xdata/Table.h"
#include "xdata/InfoSpace.h"

#include "xgi/Utils.h"
#include "xgi/Method.h"


#include "toolbox/task/Timer.h"

#include "cgicc/HTMLClasses.h"


#include "xdata/soap/NamespaceURI.h"
#include "xdata/Serializable.h"
#include "xdata/exception/Exception.h"
#include "xdata/XStr.h"
#include "xdata/Float.h"
#include "xdata/Bag.h"
#include "xdata/Boolean.h"
#include "xdata/Integer.h"
#include "xdata/String.h"
#include "xdata/Vector.h"

#include "rpct/xdaqlboxaccess/XdaqLBoxAccessClient.h"
#include "rpct/xdaqutils/XdaqRpcSupervisorClient.h"
#include "rpct/xdaqutils/XdaqDbServiceClient.h"
#include "rpct/xdaqdiagaccess/XdaqDiagAccessClient.h"
#include "XdaqRpcMonitor.h"

using namespace std;
using namespace rpct;
using namespace rpct::xdaqutils;
using namespace rpct::xdaqdiagaccess;

class StripResponse{
	
	protected:
	     rpct::xdaqdiagaccess::XdaqDiagAccessClient diagAccClnt;
	public:
	 	void lBoxHistograms(int instance, int chipId, double rate[2][96]);
	 	StripResponse();
	     
};
		
		void StripResponse::lBoxHistograms (int instance, int chipId, double rate[2][96]){
	
	
		    const int binCount = 96;
		    const int maxLength = 256;
		    const int width = 24;
		
		    typedef XdaqDiagAccessClient::DiagIdVector DiagIds;
		    typedef XdaqDiagAccessClient::HistoMgrInfoVector Histos;
		    typedef XdaqDiagAccessClient::PulserInfoVector Pulsers;
		
		
		    DiagIdBag diagCtrlId1;
		    DiagIdBag diagCtrlId2;
		    DiagIdBag diagCtrlId3;
		    diagCtrlId1.bag.init(chipId, "DAQ_DIAG_CTRL");
		    diagCtrlId2.bag.init(chipId, "PULSER_DIAG_CTRL");
		    diagCtrlId3.bag.init(chipId, "STATISTICS_DIAG_CTRL");
		    DiagIds diagCtrlIds;
		    diagCtrlIds.push_back(diagCtrlId1);
		    diagCtrlIds.push_back(diagCtrlId2);
		    diagCtrlIds.push_back(diagCtrlId3);
		
		
		    DiagId rateId1;
		    DiagId rateId2;
		    rateId1.init(chipId, "RATE");
		    rateId2.init(chipId, "RATE_WIND");
		    HistoMgrInfoBag histoInfoBag1;
		    HistoMgrInfoBag histoInfoBag2;
		    histoInfoBag1.bag.init(rateId1, binCount, rateId1);
		    histoInfoBag2.bag.init(rateId2, binCount, rateId2);
		    Histos histos;
		    histos.push_back(histoInfoBag1);
		    histos.push_back(histoInfoBag2);
		
		    DiagId pulserId1;
		    pulserId1.init(chipId, "PULSER");
		    PulserInfoBag pulserInfoBag1;
		    pulserInfoBag1.bag.init(pulserId1, maxLength, width, pulserId1);
		    Pulsers pulsers;
		    pulsers.push_back(pulserInfoBag1);
		
		    diagAccClnt.resetDiagCtrl(diagCtrlIds, "XdaqLBoxAccess", instance);
		    diagAccClnt.resetHistoMgr(histos, "XdaqLBoxAccess", instance);
		
		    xdata::Vector<xdata::Binary> pulserData;
		    xdata::Binary pulse;
		    for (int i = 0; i < 128; i++) {
		        pulse = (i % 8 == 0) ? 0xffffff : 0;
		        pulserData.push_back(pulse);
		        //System.out.println(pulserData[i].toString());
		    }
		    cout << "Configuring pulsers" << endl;
		    xdata::Binary pulseLength = pulserData.size();
		    diagAccClnt.configurePulser(pulsers, pulserData, pulseLength, false, "XdaqLBoxAccess", 0);
		
		    cout << "Configuring diag ctrls" << endl;
		    xdata::Binary counterLimit(40000000ul);
		    diagAccClnt.configureDiagCtrl(diagCtrlIds, counterLimit, IDiagCtrl::ttManual, "XdaqLBoxAccess", instance);
		   
		    cout << "Starting diag ctrls" << endl;
		    diagAccClnt.startDiagCtrl(diagCtrlIds, "XdaqLBoxAccess", instance);
		    while (!diagAccClnt.checkCountingEnded(diagCtrlIds, "XdaqLBoxAccess", instance)) {
		        cout << "Checking if all have finished" << endl;
		        //Thread.sleep(1000);
		    }
		    cout << "All diagnostics have finished" << endl;
		    diagAccClnt.stopDiagCtrl(diagCtrlIds, "XdaqLBoxAccess", instance);
		    
			int j=0;
		    for (Histos::iterator iHisto = histos.begin(); iHisto != histos.end(); ++iHisto) {
		        cout << "Data " << iHisto->bag.toString() << endl;
		        xdata::Vector<xdata::Binary>* data = diagAccClnt.readDataHistoMgr(
		                iHisto->bag.getIdBag(), "XdaqLBoxAccess", instance);
		        for (int bin = 0, k=0; bin < iHisto->bag.getBinCount(); bin++, k++) {
		            cout << dec << ((unsigned long)data->at(bin)) << ' ';
		            rate[j][k] = ((unsigned long)data->at(bin));
		        }
		        j++;
		        cout << endl;
	    	}
		}
#endif
