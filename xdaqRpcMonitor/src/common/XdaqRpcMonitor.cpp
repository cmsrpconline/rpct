#include "XdaqRpcMonitor.h"
#include "toolbox/task/TimerFactory.h"

#include "xdaq/ApplicationGroup.h"

#include "xdata/soap/NamespaceURI.h"
#include "xdata/Serializable.h"
#include "xdata/exception/Exception.h"
#include "xdata/XStr.h"
#include "xdata/Float.h"
#include "xdata/Bag.h"
#include "xdata/Boolean.h"
#include "xdata/Integer.h"
#include "xdata/String.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "chartdir.h"

#include "rpct/devices/System.h"
#include "rpct/devices/XmlSystemBuilder.h"
#include "rpct/xdaqutils/FebAccessInfo.h"
#include "rpct/xdaqutils/BoardHistoInfo.h"
#include "rpct/xdaqutils/FebHistoInfo.h"

#include "Fit.h"
#include "GaussFunc.h"
#include "LorentzFunc.h"
#include "RanGauss.h"

#include <log4cplus/configurator.h>
#include <sstream>
#include <iostream>
#include <ctime>
#include <stdio.h>
#include <fstream>
#include <math.h>

using namespace std;
using namespace rpct;
using namespace rpct::xdaqutils;
using namespace rpct::xdaqdiagaccess;
using namespace cgicc;
 

XDAQ_INSTANTIATOR_IMPL(XdaqRpcMonitor);

const char* XdaqRpcMonitor::RPCT_RPC_MONITOR_NS = "urn:rpct-rpc-monitor:1.0";
const char* XdaqRpcMonitor::RPCT_RPC_MONITOR_PREFIX = "rrm";


XdaqRpcMonitor::XdaqRpcMonitor(xdaq::ApplicationStub * s) throw (xdaq::exception::Exception) 
: xdaq::Application(s), dbServiceClient(this), lboxAccessClient(this),
diagAccessClient(this),lastSelectiveReadResponseBag(0), lastSfotReadResponseBag(0){	

    LOG4CPLUS_INFO(getApplicationLogger(),"Hello World!");
    //getApplicationInfoSpace()->fireItemAvailable("dbServiceUrl", &dbServiceUrl);
    getApplicationInfoSpace()->fireItemAvailable("picPath", &picPath);
    
    getApplicationLogger().setLogLevel(log4cplus::DEBUG_LOG_LEVEL);           
    //getApplicationContext()->setLogLevel("debug");
    
    xgi::bind(this,&XdaqRpcMonitor::Default, "Default");
    xgi::bind(this,&XdaqRpcMonitor::febMonitoring, "febMonitoring");
    xgi::bind(this,&XdaqRpcMonitor::linkBoardMonitoring, "linkBoardMonitoring");
    xgi::bind(this,&XdaqRpcMonitor::detectorGui, "detectorGui");
    xgi::bind(this,&XdaqRpcMonitor::diskPage, "diskPage");
    xgi::bind(this,&XdaqRpcMonitor::wheelPage, "wheelPage");
    xgi::bind(this,&XdaqRpcMonitor::monitorSystem, "monitorSystem");
    xgi::bind(this,&XdaqRpcMonitor::resultsTable, "resultsTable");
	xgi::bind(this,&XdaqRpcMonitor::start, "start");
	xgi::bind(this,&XdaqRpcMonitor::stop, "stop");
	xgi::bind(this,&XdaqRpcMonitor::cellInfo, "cellInfo");
	xgi::bind(this,&XdaqRpcMonitor::singleFebOverTime, "singleFebOverTime");
    xgi::bind(this,&XdaqRpcMonitor::histogram, "histogram");
    xgi::bind(this,&XdaqRpcMonitor::saveHistogram, "saveHistogram");
    xgi::bind(this,&XdaqRpcMonitor::test, "test");
	counter_ = 0;
	executionUnixTime_=0;
	lastExecutionTime_ = "";
	infoSpace_ = 0;
	switch_=1;
	for(int i=0;i<20;i++){
		sfotTh1[i]=0.0;
		sfotTh2[i]=0.0;
		sfotVmon1[i]=0.0;
		sfotVmon2[i]=0.0;
		sfotTemp[i]=0.0;
		sfotTimes[i]=0.0;
	}
				
	timer_ = toolbox::task::getTimerFactory()->createTimer("XdaqRpcMonitorTimer");
	timer_->stop();

}        

XdaqRpcMonitor::~XdaqRpcMonitor() {    
    delete lastSelectiveReadResponseBag;
    delete lastSfotReadResponseBag;
}


void XdaqRpcMonitor::Default(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception) {

    *out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << endl;
    *out << cgicc::html().set("lang", "en").set("dir","ltr") << endl;
    *out << cgicc::title("Send SOAP Message") << endl;
     
    xgi::Utils::getPageHeader(
            out, 
            "RPC Monitor",
            getApplicationDescriptor()->getContextDescriptor()->getURL(),
            getApplicationDescriptor()->getURN(),
            "/daq/xgi/images/Application.jpg"
            );

    string appUrl1 = "/" + getApplicationDescriptor()->getURN();

/*	xdata::Integer response = dbServiceClient.setStripResponseValues(5, 24, 15, 788332);
	*out << cgicc::p(response.toString()).set("class","style1 style3") << cgicc::p() << endl;
	xdata::Integer response2 = dbServiceClient.setFebConditionVaules(345, 25, 250, 250, 250, 250, 3000, 3000, 3000, 3000, 061120);
	*out << cgicc::p(response2.toString()).set("class","style1 style3") << cgicc::p() << endl;
*/
	*out << cgicc::style().set("type", "text/css") << endl;
	*out << cgicc::comment() << endl;
	*out << ".style1 {font-family:Arial, Helvetica, sans-serif;" << endl;
	*out << 	"font-style: italic;" << endl;
	*out << 	"font-weight: bold;" << endl;
	*out << 	"font-size: 32px;" << endl;
	*out << 	"color: #000099;}" << endl;
	*out << ".style2 {font-size: 18px; font-style: normal; color: #000000}" << endl;
	*out << ".style3 {font-family:Arial, Helvetica, sans-serif;" << endl;
	*out << 	"font-weight: bold;" << endl;
	*out << 	"font-size: 16px;}" << endl;
	*out << cgicc::comment() << endl;
	*out << cgicc::style() << endl;
	*out << cgicc::p("Welcome to RPC Monitoring System").set("class", "style1") << cgicc::p() << endl;
	*out << cgicc::p("Please select one of the following:").set("class", "style1 style2") << cgicc::p() << endl;
	*out << cgicc::p() << endl;
    *out << cgicc::a("Frontend Board Monitoring").set("class", "style3").set("href", appUrl1 + "/febMonitoring") << endl;
    *out << cgicc::a() << cgicc::p() << endl;
	*out << cgicc::p() << endl;
	*out << cgicc::a("Link Board Monitoring").set("class", "style3").set("href", appUrl1 + "/linkBoardMonitoring")  << endl;
    *out << cgicc::a() << cgicc::p() << endl;
	*out << cgicc::body();

	makeMenu(in, out);

    xgi::Utils::getPageFooter(*out);
   
}

void XdaqRpcMonitor::febMonitoring(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception) {

    *out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << endl;
    *out << cgicc::html().set("lang", "en").set("dir","ltr") << endl;
    *out << cgicc::title("Send SOAP Message") << endl;
     
    xgi::Utils::getPageHeader(
            out, 
            "RPC Monitor",
            getApplicationDescriptor()->getContextDescriptor()->getURL(),
            getApplicationDescriptor()->getURN(),
            "/daq/xgi/images/Application.jpg"
            );

    string appUrl2 = "/" + getApplicationDescriptor()->getURN();

	*out << cgicc::style().set("type", "text/css") << endl;
	*out << cgicc::comment() << endl;
	*out << ".style1 {font-family:Arial, Helvetica, sans-serif;" << endl;
	*out << 	"font-style: italic;" << endl;
	*out << 	"font-weight: bold;" << endl;
	*out << 	"font-size: 32px;" << endl;
	*out << 	"color: #000099;}" << endl;
	*out << ".style2 {font-size: 18px; font-style: normal; color: #000000}" << endl;
	*out << ".style3 {font-family:Arial, Helvetica, sans-serif;" << endl;
	*out << 	"font-weight: bold;" << endl;
	*out << 	"font-size: 16px;}" << endl;
	*out << cgicc::comment() << endl;
	*out << cgicc::style() << endl;
	*out << cgicc::p("Welcome to RPC Frontend Board Monitoring System").set("class", "style1") << cgicc::p() << endl;
	*out << cgicc::p("Please select one of the following:").set("class", "style1 style2") << cgicc::p() << endl;
	*out << cgicc::p() << endl;
    *out << cgicc::a("Background Monitoring of All FEBs (underconstruction)").set("class", "style3") << cgicc::p() << endl;//.set("href","http://localhost:1972/urn:xdaq-application:lid=8060/febBackground") << cgicc::a() << cgicc::p() << endl;
	*out << cgicc::p() << endl;
    *out << cgicc::a("Selective Chamber Monitoring").set("class", "style3").set("href", appUrl2 + "/detectorGui?hardware=Feb")
    	 << cgicc::a() << cgicc::p() << endl;

    makeMenu(in, out);
    xgi::Utils::getPageFooter(*out);
   
}

void XdaqRpcMonitor::linkBoardMonitoring(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception) {

    *out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << endl;
    *out << cgicc::html().set("lang", "en").set("dir","ltr") << endl;
    *out << cgicc::title("Send SOAP Message") << endl;
     
    xgi::Utils::getPageHeader(
            out, 
            "RPC Monitor",
            getApplicationDescriptor()->getContextDescriptor()->getURL(),
            getApplicationDescriptor()->getURN(),
            "/daq/xgi/images/Application.jpg"
            );

    string appUrl2 = "/" + getApplicationDescriptor()->getURN();

	*out << cgicc::style().set("type", "text/css") << endl;
	*out << cgicc::comment() << endl;
	*out << ".style1 {font-family:Arial, Helvetica, sans-serif;" << endl;
	*out << 	"font-style: italic;" << endl;
	*out << 	"font-weight: bold;" << endl;
	*out << 	"font-size: 32px;" << endl;
	*out << 	"color: #000099;}" << endl;
	*out << ".style2 {font-size: 18px; font-style: normal; color: #000000}" << endl;
	*out << ".style3 {font-family:Arial, Helvetica, sans-serif;" << endl;
	*out << 	"font-weight: bold;" << endl;
	*out << 	"font-size: 16px;}" << endl;
	*out << cgicc::comment() << endl;
	*out << cgicc::style() << endl;
	*out << cgicc::p("Welcome to RPC Linkboard Board Monitoring System").set("class", "style1") << cgicc::p() << endl;
	*out << cgicc::p("Please select one of the following:").set("class", "style1 style2") << cgicc::p() << endl;
	*out << cgicc::p() << endl;
    *out << cgicc::a("Background Monitoring of All Link Boards (underconstruction)").set("class", "style3") << cgicc::p() << endl;//.set("href","http://localhost:1972/urn:xdaq-application:lid=8060/febBackground") << cgicc::a() << cgicc::p() << endl;
	*out << cgicc::p() << endl;
    *out << cgicc::a("Selective Link Board Monitoring").set("class", "style3").set("href", appUrl2 + "/detectorGui?hardware=Linkboard")
    	 << cgicc::a() << cgicc::p() << endl;

    makeMenu(in, out);
    xgi::Utils::getPageFooter(*out);
   
}
void XdaqRpcMonitor::detectorGui(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception) {

    *out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << endl;
    *out << cgicc::html().set("lang", "en").set("dir","ltr") << endl;
    *out << cgicc::head() << endl;
    *out << cgicc::title("Choose a Detector Segment") << endl;
    *out << cgicc::meta().set("http-equiv", "Content-Type").set("content", "text/html; charset=iso-8859-1") << endl;
    *out << cgicc::head() << endl;
	*out << cgicc::body() << endl;
     
    xgi::Utils::getPageHeader(
            out, 
            "RPC Monitor",
            getApplicationDescriptor()->getContextDescriptor()->getURL(),
            getApplicationDescriptor()->getURN(),
            "/daq/xgi/images/Application.jpg"
            );
    string appUrl = "/" + getApplicationDescriptor()->getURN();


	*out << cgicc::style().set("type", "text/css") << endl;
	*out << cgicc::comment() << endl;
	*out << ".style1 {font-family:Arial, Helvetica, sans-serif;" << endl;
	*out << 	"font-style: italic;" << endl;
	*out << 	"font-weight: bold;" << endl;
	*out << 	"font-size: 32px;" << endl;
	*out << 	"color: #000099;}" << endl;
	*out << ".style3 {font-family:Arial, Helvetica, sans-serif;" << endl;
	*out << 	"font-weight: bold;" << endl;
	*out << 	"font-size: 16px;}" << endl;
	*out << cgicc::comment() << endl;
	*out << cgicc::style() << endl;

    cgicc::Cgicc cgi(in);
	xdata::String iHardware = cgi["hardware"]->getValue();

	*out << cgicc::p("RPC Selective " + (string)iHardware + " Monitoring").set("class","style1") << cgicc::p() << endl;
	*out << cgicc::p("Select a Detector Segment").set("class","style1 style3") << cgicc::p() << endl;

	const string disk_coor [11][2]= {{"856,144,897,457","/diskPage?disk=3&barrelOrEndcap=Endcap&hardware=" + (string)iHardware},//disk3
		{"803,143,844,456","/diskPage?disk=2&barrelOrEndcap=Endcap&hardware=" + (string)iHardware},//disk2
		{"753,146,794,459","/diskPage?disk=1&barrelOrEndcap=Endcap&hardware=" + (string)iHardware},//disk1
		{"630,148,695,457","/wheelPage?disk=2&barrelOrEndcap=Barrel&hardware=" + (string)iHardware},//wheel2
		{"553,149,618,458","/wheelPage?disk=1&barrelOrEndcap=Barrel&hardware=" + (string)iHardware},//wheel1
		{"478,150,543,459","/wheelPage?disk=0&barrelOrEndcap=Barrel&hardware=" + (string)iHardware},//wheel0
		{"400,146,465,455","/wheelPage?disk=-1&barrelOrEndcap=Barrel&hardware=" + (string)iHardware},//wheel-1
		{"326,149,391,458","/wheelPage?disk=-2&barrelOrEndcap=Barrel&hardware=" + (string)iHardware},//wheel-2
		{"230,147,271,460","/diskPage?disk=-1&barrelOrEndcap=Endcap&hardware=" + (string)iHardware},//disk-1
		{"181,147,222,460","/diskPage?disk=-2&barrelOrEndcap=Endcap&hardware=" + (string)iHardware},//disk-2
		{"128,148,169,461","/diskPage?disk=-3&barrelOrEndcap=Endcap&hardware=" + (string)iHardware}};//disk-3
	
		*out << cgicc::p() << endl;
		*out << cgicc::img().set("src",picPath.toString()+"detector.png").set("width","1000").set("height","500").set("border", "0").set("usemap", "Det_map") << endl;
	
		 *out << cgicc::map().set("name", "Det_map") << endl;
		 for(int i=0;i<11;i++)
		  *out << cgicc::area().set("shape", "rect").set("coords", disk_coor[i][0]).set("href",appUrl + disk_coor[i][1]).set("target", "_self") << endl;
		 *out << cgicc::map() << endl;
		*out << cgicc::p() << endl;
		*out << cgicc::body();
	    *out << cgicc::html();


	*out << cgicc::body() << endl;
    makeMenu(in, out);

	xgi::Utils::getPageFooter(*out);
}
void XdaqRpcMonitor::diskPage(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception) {

    *out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << endl;
    *out << cgicc::html().set("lang", "en").set("dir","ltr") << endl;
    *out << cgicc::head() << endl;
    *out << cgicc::title("Choose a Chamber") << endl;
    *out << cgicc::meta().set("http-equiv", "Content-Type").set("content", "text/html; charset=iso-8859-1") << endl;
    *out << cgicc::head() << endl;
	*out << cgicc::body() << endl;
     
    xgi::Utils::getPageHeader(
            out, 
            "RPC Monitor",
            getApplicationDescriptor()->getContextDescriptor()->getURL(),
            getApplicationDescriptor()->getURN(),
            "/daq/xgi/images/Application.jpg"
            );
    string appUrl = "/" + getApplicationDescriptor()->getURN();


	*out << cgicc::style().set("type", "text/css") << endl;
	*out << cgicc::comment() << endl;
	*out << ".style1 {font-family:Arial, Helvetica, sans-serif;" << endl;
	*out << 	"font-style: italic;" << endl;
	*out << 	"font-weight: bold;" << endl;
	*out << 	"font-size: 32px;" << endl;
	*out << 	"color: #000099;}" << endl;
	*out << ".style3 {font-family:Arial, Helvetica, sans-serif;" << endl;
	*out << 	"font-weight: bold;" << endl;
	*out << 	"font-size: 16px;}" << endl;
	*out << cgicc::comment() << endl;
	*out << cgicc::style() << endl;

    cgicc::Cgicc cgi(in);
	xdata::String iHardware = cgi["hardware"]->getValue();
	xdata::Integer	iDisk = cgi["disk"]->getIntegerValue();
	xdata::String	iBarrelOrEndcap = cgi["barrelOrEndcap"]->getValue();
	string lbOrFebMonitoring;
	if(iHardware == "Linkboard") lbOrFebMonitoring = "histogram";
	else lbOrFebMonitoring = "monitorSystem";
	*out << cgicc::p("RPC Selective " + (string)iHardware + " Monitoring-Disk "+iDisk.toString()).set("class","style1") << cgicc::p() << endl;
	if(iHardware=="Feb"){
	
	        *out << cgicc::table().set("width", "250").set("border", "0") << endl;
	         *out << cgicc::tr() << endl;
		      *out << cgicc::td().set("width", "101") << endl;
				*out << cgicc::form().set("action", appUrl + "/monitorSystem?grouping=4&disk="
					+iDisk.toString()+"&barrelOrEndcap="+(string)iBarrelOrEndcap).set("method","post").set("name","form1") << endl;
				 *out << cgicc::input().set("name", "Get Entire Disk").set("type","submit").set("id",
					"Get Entire Disk").set("value","Get Entire Disk") << endl;
				*out << cgicc::form() << endl;
		      *out << cgicc::td() << endl;
	         *out << cgicc::tr() << endl;
	        *out << cgicc::table() << endl;
	
	}

	*out << cgicc::p("Select a Chamber").set("class","style1 style3") << cgicc::p() << endl;
	*out << cgicc::p() << endl;
	if(iDisk==1 || iDisk==-1)
	*out << cgicc::img().set("src",picPath.toString()+"re1.png").set("width","800").set("height",
		"800").set("border", "0").set("usemap", "Disk_map") << endl;
	else
	*out << cgicc::img().set("src",picPath.toString()+"re2.png").set("width","800").set("height",
		"800").set("border", "0").set("usemap", "Disk_map") << endl;

		*out << cgicc::map().set("name", "Disk_map") << endl;
		float radius[]={255,190,184,140,135,90};		
		float posAngle;
		float negAngle;
		for(int i=0,j=0;i<3;i++){
			for(int deg=4, cham=1;deg<355;deg+=10){
			 	if((iDisk!=1 && iDisk!=-1) && i==2){
					deg+=10;
			 		negAngle = ((deg-18)*3.14159)/180;
			 	} 
			 	else{
			 		negAngle = ((deg-8)*3.14159)/180;
			 	} 
			 	posAngle = (deg*3.14159)/180;
				defineArea(in, out,
				round(400+(radius[j]*cos(posAngle))), round(400-(radius[j]*sin(posAngle))),
				round(400+(radius[j]*cos(negAngle))), round(400-(radius[j]*sin(negAngle))),
				round(400+(radius[j+1]*cos(negAngle))), round(400-(radius[j+1]*sin(negAngle))),
				round(400+(radius[j+1]*cos(posAngle))), round(400-(radius[j+1]*sin(posAngle))),
				lbOrFebMonitoring, iDisk, 3-i, cham, "", "Endcap");
				cham++;
			}
			j+=2;
		 }
		*out << cgicc::map() << endl;
	*out << cgicc::p() << endl;
		*out << cgicc::body();
	    *out << cgicc::html();


	*out << cgicc::body() << endl;
    makeMenu(in, out);

	xgi::Utils::getPageFooter(*out);
}

void XdaqRpcMonitor::wheelPage(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception) {
    *out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << endl;
    *out << cgicc::html().set("lang", "en").set("dir","ltr") << endl;
    *out << cgicc::head() << endl;
    *out << cgicc::title("Choose a Chamber") << endl;
    *out << cgicc::meta().set("http-equiv", "Content-Type").set("content", "text/html; charset=iso-8859-1") << endl;
    *out << cgicc::head() << endl;
     
    *out << cgicc::body() << endl;

    xgi::Utils::getPageHeader(
            out, 
            "RPC Monitor",
            getApplicationDescriptor()->getContextDescriptor()->getURL(),
            getApplicationDescriptor()->getURN(),
            "/daq/xgi/images/Application.jpg"
            );
    string appUrl = "/" + getApplicationDescriptor()->getURN();

	*out << cgicc::style().set("type", "text/css") << endl;
	*out << cgicc::comment() << endl;
	*out << ".style1 {font-family:Arial, Helvetica, sans-serif;" << endl;
	*out << 	"font-style: italic;" << endl;
	*out << 	"font-weight: bold;" << endl;
	*out << 	"font-size: 32px;" << endl;
	*out << 	"color: #000099;}" << endl;
	*out << ".style3 {font-family:Arial, Helvetica, sans-serif;" << endl;
	*out << 	"font-weight: bold;" << endl;
	*out << 	"font-size: 16px;}" << endl;
	*out << cgicc::comment() << endl;
	*out << cgicc::style() << endl;

    cgicc::Cgicc cgi(in);
	xdata::String iHardware = cgi["hardware"]->getValue();
	xdata::Integer	iDisk = cgi["disk"]->getIntegerValue();
	xdata::String	iBarrelOrEndcap = cgi["barrelOrEndcap"]->getValue();
	string lbOrFebMonitoring;
	if(iHardware == "Linkboard") lbOrFebMonitoring = "histogram";
	else lbOrFebMonitoring = "monitorSystem";
	float iniVer [13][8]={
		{246, 57, 246, 0, 232, 0, 232, 57},			//0 layer6+
		{246, 0, 246, -57, 232, -57, 232, 0},		//1 2layer6-
		{215, 50, 215, 0, 200, 0, 200, 50},			//2 layer5+
		{215, 0, 215, -50, 200, -50, 200, 0},		//3 layer5-
		{185, 35, 185, -35, 170, -35, 170, 35},		//4 layer4
		{165, 35, 165, -35, 150, -35, 150, 35},		//5 layer3
		{135, 25, 135, -25, 120, -25, 120, 25},		//6 layer2
		{115, 25, 115, -25, 100, -25, 100, 25},		//7 layer1
		{245, 55, 245, 30, 230, 30, 230, 55},		//8 layer6++ sector 4
		{245, 30, 245, 0, 230, 0, 230, 30},			//9 layer6+ sector 4
		{245, 0, 245, -30, 230, -30, 230, 0},		//10 layer6- sector 4
		{245, -30, 245, -55, 230, -55, 230, -30},	//11 layer6-- sector 4
		{245, 40, 245, -40, 230, -40, 230, 40}};	//12 layer6 sector 9&11
	
	*out << cgicc::p("RPC Selective " + (string)iHardware + " Monitoring-Wheel "+iDisk.toString()).set("class","style1") << cgicc::p() << endl;
	if(iHardware=="Feb"){
	
	        *out << cgicc::table().set("width", "250").set("border", "0") << endl;
	         *out << cgicc::tr() << endl;
		      *out << cgicc::td().set("width", "101") << endl;
				*out << cgicc::form().set("action", appUrl + "/monitorSystem?grouping=4&disk="
					+iDisk.toString()+"&barrelOrEndcap="+(string)iBarrelOrEndcap).set("method","post").set("name","form1") << endl;
				 *out << cgicc::input().set("name", "Get Entire Disk").set("type","submit").set("id",
					"Get Entire Disk").set("value","Get Entire Disk") << endl;
				*out << cgicc::form() << endl;
		      *out << cgicc::td() << endl;
	         *out << cgicc::tr() << endl;
	
	         *out << cgicc::tr() << endl;
		      *out << cgicc::td() << endl;
		      *out << cgicc::td() << endl;
	         *out << cgicc::tr() << endl;
	
	         *out << cgicc::tr() << endl;
		      *out << cgicc::td().set("width", "139") << endl;
				*out << cgicc::form().set("action", appUrl + "/monitorSystem?grouping=3&disk="
					+iDisk.toString()+"&barrelOrEndcap="+(string)iBarrelOrEndcap).set("method","post").set("name","form2") << endl;
					*out <<  cgicc::select().set("name", "sector").set("id", "sector") << endl;
					for(int i=1;i<13;i++){
						xdata::Integer sec=i;
						*out <<   cgicc::option(sec.toString()).set("value", sec.toString()) << endl;
					}
					*out <<  cgicc::select() << endl;
				 *out << cgicc::input().set("name", "Get Sector").set("type","submit").set("id",
					"Get Sector").set("value","Get Sector") << endl;
				*out << cgicc::form() << endl;
		      *out << cgicc::td() << endl;
	         *out << cgicc::tr() << endl;
	        *out << cgicc::table() << endl;
	
	}
	*out << cgicc::p("Select a Chamber").set("class","style1 style3") << cgicc::p() << endl;
	*out << cgicc::p() << endl;
	*out << cgicc::img().set("src",picPath.toString()+"barrel.png").set("width","800").set("height",
		"800").set("border", "0").set("usemap", "Wheel_map") << endl;
	 *out << cgicc::map().set("name", "Wheel_map") << endl;
		float posAngle;
		for(int i=0,j=0;i<6;i++){
			int k;
			string subsec;
			if(i==0 || i==1) k=0;
			else k=1;
			for(;k<2;k++){
				if(k==0 && (i==0 || i==1)) subsec = "p";
				else if(k==1 && (i==0 || i==1)) subsec = "-";
				else subsec = "";
				for(int deg=0, sec=1;deg<360;deg+=30){
					int m;
					int n=j;
					string ss = subsec;
					string ssa[]={"pp","p","-","--"};
					if(deg==90 && i==0){
						if(k==1){
							sec++;
							continue;
						}
						m=0;
					}
					else m=3;
					for(;m<4;m++){
						if(deg==90 && i==0){
							n=8+m;
							ss=ssa[m];
						}
						if ((deg == 240 || deg == 300) && i==0){
							if(k==1) continue;
							n=12;
							ss="";
						}
						posAngle = (deg*3.14159)/180;
						defineArea(in, out,
						round(400+((iniVer[n][0]*cos(posAngle))-(iniVer[n][1]*sin(posAngle)))),
						round(400-((iniVer[n][1]*cos(posAngle))+(iniVer[n][0]*sin(posAngle)))),
						round(400+((iniVer[n][2]*cos(posAngle))-(iniVer[n][3]*sin(posAngle)))),
						round(400-((iniVer[n][3]*cos(posAngle))+(iniVer[n][2]*sin(posAngle)))),
						round(400+((iniVer[n][4]*cos(posAngle))-(iniVer[n][5]*sin(posAngle)))),
						round(400-((iniVer[n][5]*cos(posAngle))+(iniVer[n][4]*sin(posAngle)))),
						round(400+((iniVer[n][6]*cos(posAngle))-(iniVer[n][7]*sin(posAngle)))),
						round(400-((iniVer[n][7]*cos(posAngle))+(iniVer[n][6]*sin(posAngle)))),
						lbOrFebMonitoring, iDisk, 6-i, sec, ss,"Barrel");
					}
					sec++;
				}
			j++;
			}
		 }
	 *out << cgicc::map() << endl;
	*out << cgicc::p() << endl;
	*out << cgicc::body();
    *out << cgicc::html();
    makeMenu(in, out);
    xgi::Utils::getPageFooter(*out);
}

void XdaqRpcMonitor::defineArea(xgi::Input* in, xgi::Output* out, int x1, int y1,int x2, int y2, int x3,
     int y3, int x4, int y4, string lbOrFebMonitoring, int disk, int layer, int sector, string subsector, string barrelOrEndcap) throw (xgi::exception::Exception){
     
     xdata::Integer X1 =x1;
     xdata::Integer Y1 =y1;
     xdata::Integer X2 =x2;
     xdata::Integer Y2 =y2;
     xdata::Integer X3 =x3;
     xdata::Integer Y3 =y3;
     xdata::Integer X4 =x4;
     xdata::Integer Y4 =y4;
     xdata::Integer Disk =disk;
     xdata::Integer Layer =layer;
     xdata::Integer Sector =sector;
 
     string appUrl = "/" + getApplicationDescriptor()->getURN();

	  *out << cgicc::area().set("shape", "poly").set("coords", (string)X1.toString()+","+(string)Y1.toString()+
		  ","+(string)X2.toString()+","+(string)Y2.toString()+","+(string)X3.toString()+","+(string)Y3.toString()+
		  ","+(string)X4.toString()+","+(string)Y4.toString()).set("href",  appUrl + "/"+lbOrFebMonitoring+"?disk=" +
		  (string)Disk.toString()+"&layer="+(string)Layer.toString()+"&sector="+(string)Sector.toString()+
		  "&subsector="+subsector+"&barrelOrEndcap="+barrelOrEndcap+"&grouping=1").set("target", "_self") << endl;
}

XdaqLBoxAccessClient::MassiveReadResponseBag* XdaqRpcMonitor::monitorSystem(rpct::xdaqutils::XdaqDbServiceClient::FebAccessInfoVector* febAccessInfoVector, xdata::Boolean& endcap) {  

	    typedef std::map<int, XdaqLBoxAccessClient::MassiveReadRequestBag> instanceMap;
	    instanceMap lBoxInstance;
		std::vector<rpct::XdaqLBoxAccessClient::MassiveReadResponseBag*> instanceReadBagVector;

	    for (XdaqDbServiceClient::FebAccessInfoVector::iterator iFAI = febAccessInfoVector->begin();
	    	iFAI != febAccessInfoVector->end(); ++iFAI) {            
		        FebAccessInfo& febAccessInfo = iFAI->bag;
           		int xdaqInstanceNumber = febAccessInfo.getXdaqAppInstance();
            	MassiveReadRequest& readRequest = lBoxInstance[xdaqInstanceNumber].bag;
	    		MassiveReadRequest::Febs& febs = readRequest.getFebs();
	            if(lBoxInstance[xdaqInstanceNumber].bag.getProperties().empty())
	            {
				    readRequest.getProperties().push_back(MassiveReadRequest::PROP_TEMPERATURE);
				    readRequest.getProperties().push_back(MassiveReadRequest::PROP_VTH1);
				    readRequest.getProperties().push_back(MassiveReadRequest::PROP_VTH2);
				    if(endcap){
				    	readRequest.getProperties().push_back(MassiveReadRequest::PROP_VTH3);
				    	readRequest.getProperties().push_back(MassiveReadRequest::PROP_VTH4);
				    } 
				    readRequest.getProperties().push_back(MassiveReadRequest::PROP_VMON1);
				    readRequest.getProperties().push_back(MassiveReadRequest::PROP_VMON2);
				    if(endcap){
				    	readRequest.getProperties().push_back(MassiveReadRequest::PROP_VMON3);
				    	readRequest.getProperties().push_back(MassiveReadRequest::PROP_VMON4);
				    } 
	            }
	    		xdata::Bag<FebInfo> febInfoBag;
		        febInfoBag.bag.setXdaqAppInstance(xdaqInstanceNumber);
		        febInfoBag.bag.setFebLocalEtaPartition(febAccessInfo.getFebLocalEtaPartition());
		        febInfoBag.bag.setChamberLocationName(febAccessInfo.getChamberLocationName());
		        febInfoBag.bag.setCcuAddress(febAccessInfo.getCcuAddress());
		        febInfoBag.bag.setChannel(febAccessInfo.getI2cChannel());
		        febInfoBag.bag.setAddress(febAccessInfo.getFebAddress());
		        febInfoBag.bag.setEndcap(endcap);
		        febs.push_back(febInfoBag);
	    }
    for(instanceMap::iterator iMap = lBoxInstance.begin(); iMap != lBoxInstance.end(); ++iMap){

		try {
				LOG4CPLUS_INFO(getApplicationLogger(), "Putting instance " << iMap->first << " into instanceReadBagVector");
				if(iMap->first != -1) instanceReadBagVector.push_back(lboxAccessClient.massiveRead(iMap->second, iMap->first));
				else LOG4CPLUS_INFO(getApplicationLogger(), "At least on instance was" << iMap->first << " Skipped insert");
		}
	    catch (xdaq::exception::Exception& e) {
	        XCEPT_RETHROW(xgi::exception::Exception, "Cannot send message", e);                                                 
	    }     
		catch (exception& e) {
    		XCEPT_RAISE(xgi::exception::Exception, string("Trouble filling instanceReadBagVector ") + e.what());			
		}
	    catch (...) {
	        XCEPT_RAISE(xgi::exception::Exception, "Cannot send message - unknown reason");                                                 
	    }                                                                                                                            
    }
   	MassiveReadResponse& readResponse_0 = instanceReadBagVector[0]->bag;
    FebValuesVector& valuesVector_0 = readResponse_0.getFebValues();
    for (uint i=1; i<instanceReadBagVector.size(); i++){
		try {
	    	MassiveReadResponse& readResponse = instanceReadBagVector[i]->bag;
	        FebValuesVector& valuesVector = readResponse.getFebValues();
	        for(uint j=0;j<valuesVector.size();j++) valuesVector_0.push_back(valuesVector[j]);
		}
	    catch (xdaq::exception::Exception& e) {
	        XCEPT_RETHROW(xgi::exception::Exception, "Cannot send message", e);                                                 
	    }     
		catch (exception& e) {
    		XCEPT_RAISE(xgi::exception::Exception, string("Trouble iterating through instanceReadBagVector ") + e.what());			
		}
	    catch (...) {
	        XCEPT_RAISE(xgi::exception::Exception, "Cannot send message - unknown reason");                                                 
	    }                                                                                                                            
    }
    readResponse_0.setFebValues(valuesVector_0);
	return instanceReadBagVector[0];
}
 
void XdaqRpcMonitor::monitorSystem(xgi::Input* in, xgi::Output* out) 
throw (xgi::exception::Exception) {
    
    delete lastSelectiveReadResponseBag;
    lastSelectiveReadResponseBag = 0;
    try {
	    cgicc::Cgicc cgi(in);
	    int grouping = cgi["grouping"]->getIntegerValue();
		int disk = cgi["disk"]->getIntegerValue();
		string barrelOrEndcap = cgi["barrelOrEndcap"]->getValue();
	    int sector;
	    int layer;
	    string subsector;
	    string febLocalEtaPartition;
	    int posInLocalEtaPartition;
		switch (grouping){
			case 4:
			    sector = 0;
			    layer = 0;
			    subsector = "";
			    febLocalEtaPartition = "";
			    posInLocalEtaPartition = 0;
				break;
			case 3:
			    sector = cgi["sector"]->getIntegerValue();
			    layer = 0;
			    subsector = "";
			    febLocalEtaPartition = "";
			    posInLocalEtaPartition = 0;
				break;
			case 1:
			    sector = cgi["sector"]->getIntegerValue();
			    layer = cgi["layer"]->getIntegerValue();
			    subsector = cgi["subsector"]->getValue();
			    if (subsector=="pp") subsector="++";
			    if (subsector=="p") subsector="+";
			    febLocalEtaPartition = "";
			    posInLocalEtaPartition = 0;
				break;
		}
			limits[0] =  0;//iTempMin->getIntegerValue();
			limits[1] =  50;//iTempMax->getIntegerValue();
			limits[2] =  100;//iTh1Min->getIntegerValue();
			limits[3] =  300;//iTh1Max->getIntegerValue();
			limits[4] =  100;//iTh2Min->getIntegerValue();
			limits[5] =  300;//iTh2Max->getIntegerValue();
			limits[6] =  100;//iTh3Min->getIntegerValue();
			limits[7] =  300;//iTh3Max->getIntegerValue();
			limits[8] =  100;//iTh4Min->getIntegerValue();
			limits[9] =  300;//iTh4Max->getIntegerValue();
			limits[10] =  2000;//iVmon1Min->getIntegerValue();
			limits[11] =  4000;//iVmon1Max->getIntegerValue();
			limits[12] =  2000;//iVmon2Min->getIntegerValue();
			limits[13] =  4000;//iVmon2Max->getIntegerValue();
			limits[14] =  2000;//iVmon3Min->getIntegerValue();
			limits[15] =  4000;//iVmon3Max->getIntegerValue();
			limits[16] =  2000;//iVmon4Min->getIntegerValue();
			limits[17] =  4000;//iVmon4Max->getIntegerValue();
			LOG4CPLUS_INFO(getApplicationLogger(),"loaded values");
		    XdaqDbServiceClient::FebAccessInfoVector* febAccessInfoVector = getFebMap(
		    	grouping, disk, layer, sector, subsector, barrelOrEndcap, febLocalEtaPartition, posInLocalEtaPartition);
		
		int test = 1;    	
	    for (XdaqDbServiceClient::FebAccessInfoVector::iterator iFAI = febAccessInfoVector->begin();
	    	iFAI != febAccessInfoVector->end(); ++iFAI) {            
		        FebAccessInfo& febAccessInfo = iFAI->bag;
		        int instance = febAccessInfo.getXdaqAppInstance();
    			LOG4CPLUS_INFO(getApplicationLogger(), "Instance " << instance << endl);			
           		if (instance != -1)
           		{
           			test = 0;
//    			LOG4CPLUS_ERROR(getApplicationLogger(), "test " << test << endl);			
           			break;
           		}
	    	}
//    			LOG4CPLUS_ERROR(getApplicationLogger(), "test " << test << endl);			

			if(test == 1){
				LOG4CPLUS_INFO(getApplicationLogger(),"The Feb Access Info Vector is Empty, when returning from getFebMap");
			}
			else{
				xdata::Boolean endcap;
				if (barrelOrEndcap=="Endcap") endcap = true;
				else endcap = false;
	        	lastSelectiveReadResponseBag = monitorSystem(febAccessInfoVector, endcap);
			}
	        delete febAccessInfoVector;
	        
//		}

    }
    catch (xdaq::exception::Exception& e) {
        XCEPT_RETHROW(xgi::exception::Exception, "Cannot send message", e);                                                 
    }     
    catch (exception& e) {
        XCEPT_RAISE(xgi::exception::Exception, string("Cannot send message ") + e.what());                                                 
    }             
    catch (...) {
        XCEPT_RAISE(xgi::exception::Exception, "Cannot send message - unknown reason");                                                 
    }                                                                                                                            
    this->resultsTable(in,out);                                                                                                                                     
}

void XdaqRpcMonitor::resultsTable(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception) {

    *out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << endl;
    *out << cgicc::html().set("lang", "en").set("dir","ltr") << endl;
    *out << cgicc::title("Monitoring results") << endl;
//    *out << cgicc::meta().set("http-equiv", "refresh").set("content", "5") << endl;
    string appUrl = "/" + getApplicationDescriptor()->getURN();
     
    xgi::Utils::getPageHeader(
            out, 
            "RPC Monitor",
            getApplicationDescriptor()->getContextDescriptor()->getURL(),
            getApplicationDescriptor()->getURN(),
            "/daq/xgi/images/Application.jpg"
            );

    *out << cgicc::br(); 
	*out << cgicc::style().set("type", "text/css") << endl;
	*out << cgicc::comment() << endl;
	*out << ".style1 {font-family:Arial, Helvetica, sans-serif;" << endl;
	*out << 	"font-style: italic;" << endl;
	*out << 	"font-weight: bold;" << endl;
	*out << 	"font-size: 32px;" << endl;
	*out << 	"color: #000099;}" << endl;
	*out << cgicc::comment() << endl;
	*out << cgicc::style() << endl;
	*out << cgicc::body() << endl;
	*out << cgicc::p("Results from the Last Selective Query").set("class","style1") << cgicc::p() << endl;

	string red="#00FF00";
	string green="#FF0000";
	
    if (lastSelectiveReadResponseBag == 0) {
    	*out << "No results";
    }
    else {
        MassiveReadResponse& readResponse = lastSelectiveReadResponseBag->bag;
        FebProperties& props = readResponse.getProperties();
 
        *out << cgicc::table().set("width", "600").set("border", "2").set("bordercolor", "#000066")
        		.set("cellpadding", "6").set("cellspacing", "2");
        *out << cgicc::tr();         
			makeColHead(in, out, "Chamber");
			makeColHead(in, out, "Xdaq LBox Instance");
			makeColHead(in, out, "Local Partition");
			makeColHead(in, out, "CCU Address");
			makeColHead(in, out, "CB Channel");
			makeColHead(in, out, "I2C Local Number");
	        for (FebProperties::iterator iProp = props.begin(); iProp != props.end(); ++iProp)       
				makeColHead(in, out, (string)(*iProp));
        *out << cgicc::tr();
        
        typedef FebValuesVector ValuesVector;
        ValuesVector& valuesVector = readResponse.getFebValues();
        for (ValuesVector::iterator iVal = valuesVector.begin(); iVal != valuesVector.end(); ++iVal) {
            FebValues& febValues = iVal->bag;
            FebInfo& febInfo = febValues.getFeb().bag;
	        
	        *out << cgicc::tr() << endl;
		        makeCell(in, out, febInfo.getChamberLocationName());
		        makeCell(in, out, febInfo.getXdaqAppInstance());
		        makeCell(in, out, febInfo.getFebLocalEtaPartition());
		        makeCell(in, out, febInfo.getCcuAddress());
		        makeCell(in, out, febInfo.getChannel());
		        makeCell(in, out, febInfo.getAddress());
		        bool endcap = febInfo.isEndcap();
	            typedef FebValues::Values Values;
	            Values& values = febValues.getValues();
				
				for(int j=0, k=0; k<18; j++, k+=2){
					if (endcap==false && j==3) k=10;
					if (endcap==false && j==5) break;
					int val=values[j];
		            if (val>limits[k]&&val<limits[k+1]){
				        makeCell(in, out, values[j], red, febInfo.getChamberLocationName(),
				        febInfo.getXdaqAppInstance(), febInfo.getFebLocalEtaPartition(), febInfo.getCcuAddress(),
				        febInfo.getChannel(), febInfo.getAddress(), febInfo.isEndcap());
		            } else{
				        makeCell(in, out, values[j], green, febInfo.getChamberLocationName(),
				        febInfo.getXdaqAppInstance(), febInfo.getFebLocalEtaPartition(), febInfo.getCcuAddress(),
				        febInfo.getChannel(), febInfo.getAddress(), febInfo.isEndcap());
		            }
				}  
           *out << cgicc::tr();
        }        
        *out << cgicc::table();
    }
    makeMenu(in, out);
    xgi::Utils::getPageFooter(*out);
}

void XdaqRpcMonitor::cellInfo(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception)
{
    *out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << endl;
    *out << cgicc::html().set("lang", "en").set("dir","ltr") << endl;
    *out << cgicc::title("Send SOAP Message") << endl;
    *out << cgicc::meta().set("http-equiv", "refresh").set("content", "5") << endl;
     
    xgi::Utils::getPageHeader(
            out, 
            "RPC Monitor",
            getApplicationDescriptor()->getContextDescriptor()->getURL(),
            getApplicationDescriptor()->getURN(),
            "/daq/xgi/images/Application.jpg"
            );
	*out << cgicc::style().set("type", "text/css") << endl;
	*out << cgicc::comment() << endl;
	*out << ".style1 {font-family:Arial, Helvetica, sans-serif;" << endl;
	*out << 	"font-style: italic;" << endl;
	*out << 	"font-weight: bold;" << endl;
	*out << 	"font-size: 32px;" << endl;
	*out << 	"color: #000099;}" << endl;
	*out << ".style2 {font-size: 16px; font-style: normal; color: #000000;}" << endl;
	*out << ".style3 {font-size: 24px;}" << endl;
	*out << cgicc::comment() << endl;
	*out << cgicc::style() << endl;
	*out << cgicc::p("Current FEB Values").set("class","style1") << cgicc::p() << endl;
	*out << cgicc::p("This page performs an auto-refresh every 5 sec.").set("class","style2") << cgicc::p() << endl;
	*out << cgicc::p("Press Esc to stop this action").set("class","style2") << cgicc::p() << endl;
	*out << cgicc::p("Press Refresh or F5 to start this action again").set("class","style2") << cgicc::p() << endl;

	std::string singleMonitor = toolbox::toString("/%s/start",getApplicationDescriptor()->getURN().c_str());
	
	*out << cgicc::a("Start Monitoring this FEB over time").set("href",singleMonitor) << cgicc::br() << std::endl;

//Get form data
	Cgicc formData(in);
	form_iterator iData = formData.getElement("data");
	form_iterator iChamberLocationName = formData.getElement("chamberLocationName");
	form_iterator iXdaqAppInstance = formData.getElement("xdaqAppInstance");
	form_iterator iFebLocalEtaPartition = formData.getElement("febLocalEtaPartition");
	form_iterator iCcuAddress = formData.getElement("ccuAddress");
	form_iterator iChannel = formData.getElement("channel");
	form_iterator iAddress = formData.getElement("address");
	form_iterator iEndcap = formData.getElement("endcap");
	double data=0.0;
	if (iData != formData.getElements().end()) {
	    data = iData->getDoubleValue();
	    singleFebChamberLocationName = iChamberLocationName->getValue();
	    singleFebXdaqAppInstance = iXdaqAppInstance->getIntegerValue();
	    singleFebLocalEtaPartition = iFebLocalEtaPartition->getValue();
	    singleFebCcuAddress = iCcuAddress->getIntegerValue();
	    singleFebChannel = iChannel->getIntegerValue();
	    singleFebAddress = iAddress->getIntegerValue();
	    if (iEndcap->getValue()=="true") singleFebEndcap = true;
	    else singleFebEndcap = false;
	}
	rpct::xdaqutils::XdaqDbServiceClient::FebAccessInfoVector singFebAccessInfoVector;
	xdata::Bag<FebAccessInfo> singFebAccessInfo;
	FebAccessInfo& accessInfo = singFebAccessInfo.bag;
	    accessInfo.setXdaqAppInstance(singleFebXdaqAppInstance);    
		accessInfo.setFebLocalEtaPartition(singleFebLocalEtaPartition);    
		accessInfo.setChamberLocationName(singleFebChamberLocationName);    
		accessInfo.setCcuAddress(singleFebCcuAddress);    
		accessInfo.setI2cChannel(singleFebChannel);    
		accessInfo.setFebAddress(singleFebAddress);
		singFebAccessInfoVector.push_back(singFebAccessInfo);
	if(singFebAccessInfoVector.empty()){
		LOG4CPLUS_INFO(getApplicationLogger(),"The Feb Access Info Vector is empty for cell info");
		*out << cgicc::p(" Your query did not return any results ").set("class","style1 style3") << cgicc::p() << endl;
	}
	else{
			    
	    delete lastSelectiveReadResponseBag;
		LOG4CPLUS_INFO(getApplicationLogger(), "Endcap Boolean = " << singleFebEndcap.toString());

	    lastSelectiveReadResponseBag =  monitorSystem(&singFebAccessInfoVector, singleFebEndcap);
	
	
	
	        MassiveReadResponse& readResponse = lastSelectiveReadResponseBag->bag;
	        typedef FebValuesVector ValuesVector;
	        ValuesVector& valuesVector = readResponse.getFebValues();
	    for (ValuesVector::iterator iVal = valuesVector.begin(); iVal != valuesVector.end(); ++iVal) {
	        FebValues& febValues = iVal->bag;
	        typedef FebValues::Values Values;
	        Values& values = febValues.getValues();
	
			*out << cgicc::p(singleFebChamberLocationName + " Local Eta Partition " + singleFebLocalEtaPartition).set("class","style1 style3") << cgicc::p() << endl;
	
			//Angle Meter
			double value[9]={0,0,0,0,0,0,0,0,0};
			if (singleFebEndcap == false){
				for(int k=0, j=0;k<7;k++,j++){
					if(k==3) k+=2;
					value[k] = values[j];
				}
			}
		    else{
	    		for(int k=0;k<9;k++) value[k] = values[k];
		    }
		    for (int i=0; i<9;i++){
			    AngularMeter *m = new AngularMeter(200, 100);
			    if (i==0) m->setBackground(Chart::metalColor(0x9898e0), 0x0, -2);
			    else if (i==1) m->setBackground(Chart::goldColor(0x9898e0), 0x0, -2);
			    else if (i==2) m->setBackground(Chart::silverColor(0x9898e0), 0x0, -2);
			    else if (i==3) m->setBackground(Chart::metalColor(0x9898e0), 0x0, -2);
			    else if (i==4) m->setBackground(Chart::goldColor(0x9898e0), 0x0, -2);
			    else if (i==5) m->setBackground(Chart::silverColor(0x9898e0), 0x0, -2);
			    else if (i==6) m->setBackground(Chart::metalColor(0x9898e0), 0x0, -2);
			    else if (i==7) m->setBackground(Chart::goldColor(0x9898e0), 0x0, -2);
			    else if (i==8) m->setBackground(Chart::silverColor(0x9898e0), 0x0, -2);
			    m->setMeter(100, 235, 210, -24, 24);
				if (i==0) { m->setScale(0, 100, 20);
					m->setLabelStyle("/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arial.ttf", 6);
				    m->addZone(0, 30, 0x99ff99, 0x808080);
				    m->addZone(30, 50, 0xffff66, 0x808080);
				    m->addZone(50, 100, 0xff3333, 0x808080);
				}
				if (i<5 && i>0){
					m->setScale(0, 500, 100);
					m->setLabelStyle("/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arial.ttf", 6);
				    m->addZone(0, 30, 0x99ff99, 0x808080);
					m->addZone(0, 100, 0xff3333, 0x808080);
			    	m->addZone(100, 300, 0x99ff99, 0x808080);
			    	m->addZone(300, 500, 0xff3333, 0x808080);
				}		
				else if (i<9 && i>4){
					m->setScale(0, 5000, 1000);
					m->setLabelStyle("/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arial.ttf", 6);
				    m->addZone(0, 30, 0x99ff99, 0x808080);
				    m->addZone(0, 2000, 0xff3333, 0x808080);
				    m->addZone(2000, 4000, 0x99ff99, 0x808080);
				    m->addZone(4000, 5000, 0xff3333, 0x808080);
				}
			    if (i==0) m->addTitle(Chart::Top, "TEMP\n", "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 10);
			    else if (i==1) m->addTitle(Chart::Top, "TH1\n", "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 10);
			    else if (i==2) m->addTitle(Chart::Top, "TH2\n", "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 10);
			    else if (i==3) m->addTitle(Chart::Top, "TH3\n", "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 10);
			    else if (i==4) m->addTitle(Chart::Top, "TH4\n", "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 10);
			    else if (i==5) m->addTitle(Chart::Top, "VMON1\n", "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 10);
			    else if (i==6) m->addTitle(Chart::Top, "VMON2\n", "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 10);
			    else if (i==7) m->addTitle(Chart::Top, "VMON3\n", "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 10);
			    else if (i==8) m->addTitle(Chart::Top, "VMON4\n", "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 10);
			    m->addPointer(value[i], 0x000000);
			    m->addText(100, 80, m->formatValue(value[i], "2"), "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arial.ttf", 8, 0xffffff, Chart::BottomCenter)->setBackground(0, 0, -1);
				ostringstream ostr;
				ostr << std::getenv("XDAQ_ROOT") << "/htdocs" << picPath.toString() << "wideameter" << i << ".png";
				std::cout << std::getenv("XDAQ_ROOT") << "/htdocs" << picPath.toString() << endl;
				m->makeChart(ostr.str().c_str());
			    delete m;
		    }
		}
	
	    *out << cgicc::img().set("src", picPath.toString()+"wideameter1.png"); 
	    *out << cgicc::img().set("src", picPath.toString()+"wideameter2.png");
	    if (singleFebEndcap == true){
		    *out << cgicc::img().set("src", picPath.toString()+"wideameter3.png"); 
		    *out << cgicc::img().set("src", picPath.toString()+"wideameter4.png");
	    } 
	    *out << cgicc::img().set("src", picPath.toString()+"wideameter5.png"); 
	    *out << cgicc::img().set("src", picPath.toString()+"wideameter6.png");
	    if (singleFebEndcap == true){
		    *out << cgicc::img().set("src", picPath.toString()+"wideameter7.png"); 
		    *out << cgicc::img().set("src", picPath.toString()+"wideameter8.png");
	    } 
	    *out << cgicc::img().set("src", picPath.toString()+"wideameter0.png"); 
	    *out << cgicc::br();
	    *out << cgicc::br();
	}
	makeMenu(in, out);

	xgi::Utils::getPageFooter(*out);
     
}
      
void XdaqRpcMonitor::makeCell(xgi::Input* in, xgi::Output* out, xdata::Float& data, string& color,
		 xdata::String& chamberLocationName, xdata::Integer& xdaqAppInstance, xdata::String& febLocalEtaPartition,
		 xdata::Integer& ccuAddress, xdata::Integer& channel, xdata::Integer& address, xdata::Boolean& endcap)
		 throw (xgi::exception::Exception){
    string appUrl = "/" + getApplicationDescriptor()->getURN()+ "/cellInfo";
	static xdata::Integer i=0;
	i++;
	xdata::String num = i.toString();
	string formName = "form" + (std::string)num;
	string bgcolor = "background-color:" + color;

	*out << cgicc::td() << endl;
    *out << cgicc::form().set("method","post").set("action", appUrl)
    		.set("enctype","multipart/form-data").set("name",formName) << endl;
    *out << cgicc::div().set("align", "center") << endl;
    *out << cgicc::input().set("name", "RE1/1/1").set("type", "submit")
    		.set("id", "RE1/1/1").set("value", (string)data.toString()).set("style", bgcolor) << endl;
    *out << cgicc::input().set("type","hidden").set("name", "data").set("value", (string)data.toString()) << endl;
    *out << cgicc::input().set("type","hidden").set("name", "chamberLocationName").set("value", (string)chamberLocationName) << endl;
    *out << cgicc::input().set("type","hidden").set("name", "xdaqAppInstance").set("value", (string)xdaqAppInstance.toString()) << endl;
    *out << cgicc::input().set("type","hidden").set("name", "febLocalEtaPartition").set("value", (string)febLocalEtaPartition) << endl;
    *out << cgicc::input().set("type","hidden").set("name", "ccuAddress").set("value", (string)ccuAddress.toString()) << endl;
    *out << cgicc::input().set("type","hidden").set("name", "channel").set("value", (string)channel.toString()) << endl;
    *out << cgicc::input().set("type","hidden").set("name", "address").set("value", (string)address.toString()) << endl;
    *out << cgicc::input().set("type","hidden").set("name", "endcap").set("value", (string)endcap.toString()) << endl;
    *out << cgicc::div() << endl;
    *out << cgicc::form() << endl;
	*out << cgicc::td() << endl;
}
    
void XdaqRpcMonitor::makeCell(xgi::Input* in, xgi::Output* out, xdata::String& data) throw (xgi::exception::Exception){

	 *out << cgicc::td() << endl;
	 *out << cgicc::div().set("align", "center") << (string)data << cgicc::div() << endl;
	 *out << cgicc::td() << endl;
}

void XdaqRpcMonitor::makeCell(xgi::Input* in, xgi::Output* out, xdata::Integer& data) throw (xgi::exception::Exception){

	 *out << cgicc::td() << endl;
	 *out << cgicc::div().set("align", "center") << data << cgicc::div() << endl;
	 *out << cgicc::td() << endl;
}
void XdaqRpcMonitor::makeCell(xgi::Input* in, xgi::Output* out, xdata::Float& data) throw (xgi::exception::Exception){

	 *out << cgicc::td() << endl;
	 *out << cgicc::div().set("align", "center") << data << cgicc::div() << endl;
	 *out << cgicc::td() << endl;
}
void XdaqRpcMonitor::makeCell(xgi::Input* in, xgi::Output* out, xdata::UnsignedLong& data) throw (xgi::exception::Exception){

	 *out << cgicc::td() << endl;
	 *out << cgicc::div().set("align", "center") << data << cgicc::div() << endl;
	 *out << cgicc::td() << endl;
}
    
void XdaqRpcMonitor::makeColHead(xgi::Input* in, xgi::Output* out, string header) throw (xgi::exception::Exception){

    *out << cgicc::th().set("scope","col") <<  endl;
    *out << header <<  endl;
    *out << cgicc::th() <<  endl;  
}

void XdaqRpcMonitor::setGraphLimits(){
	
	static short limitSwitch = 0;
	if (limitSwitch>0) return;
	for(int i=0;i<20;i++){
		upperTemp[i] = limits[0];
		lowerTemp[i] = limits[1];
		upperTh1[i] = limits[2];
		lowerTh1[i] = limits[3];
		upperTh2[i] = limits[4];
		lowerTh2[i] = limits[5];
		upperTh3[i] = limits[6];
		lowerTh3[i] = limits[7];
		upperTh4[i] = limits[8];
		lowerTh4[i] = limits[9];
		upperVmon1[i] = limits[10];
		lowerVmon1[i] = limits[11];
		upperVmon2[i] = limits[12];
		lowerVmon2[i] = limits[13];
		upperVmon3[i] = limits[14];
		lowerVmon3[i] = limits[15];
		upperVmon4[i] = limits[16];
		lowerVmon4[i] = limits[17];
	}
	limitSwitch = 1;
	return;
}
    
void XdaqRpcMonitor::makeMenu(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception) {	
	
    string appUrl = "/" + getApplicationDescriptor()->getURN();
    *out << cgicc::div().set("align", "right") << endl;
    *out << '[' << cgicc::a("Home").set("href", appUrl + "/Default"
    	).set("style","font-size: 10pt; font-family: arial") 
    	<< cgicc::a() << "] ";
    *out << '[' << cgicc::a("Selective Link Board Monitor").set("href", appUrl + "/detectorGui?hardware=Linkboard"
    	).set("style","font-size: 10pt; font-family: arial") 
    	<< cgicc::a() << "] ";
    *out << '[' << cgicc::a("Selective FEB Monitor").set("href", appUrl + "/detectorGui?hardware=Feb"
    	).set("style","font-size: 10pt; font-family: arial") 
    	<< cgicc::a() << "] ";
/*    *out << '[' << cgicc::a("Results").set("href", appUrl + "/resultsTable"
    	).set("style","font-size: 10pt; font-family: arial") 
    	<< cgicc::a() << "] ";
   	*out << '[' << cgicc::a("Background FEB Monitor").set("href",
   		"http://localhost:1972/urn:xdaq-application:lid=8040/Default"
   		).set("style","font-size: 10pt; font-family: arial") 
    	<< cgicc::a() << "] ";
*/    *out << cgicc::div() << endl;
}

rpct::xdaqutils::XdaqDbServiceClient::FebAccessInfoVector* XdaqRpcMonitor::getFebMap(int& grouping, int& disk, int& layer, int& sector, string& subsector, string& barrelOrEndcap, string& febLocalEtaPartition, int& posInLocalEtaPartition)
{
	if(grouping == 0)
		return dbServiceClient.getSingleFebByLocParameters(disk, layer, sector, subsector, barrelOrEndcap, febLocalEtaPartition, posInLocalEtaPartition);
	else if(grouping == 1)
		return dbServiceClient.getFebsByChamberLocation(disk, layer, sector, subsector, 
            XdaqDbServiceClient::stringToBarrelOrEndcap(barrelOrEndcap));
	else if (grouping == 2)
		return dbServiceClient.getFebsByLayer(disk, layer, 
            XdaqDbServiceClient::stringToBarrelOrEndcap(barrelOrEndcap));
	else if(grouping == 3)
		return dbServiceClient.getFebsBySector(disk, sector, 
            XdaqDbServiceClient::stringToBarrelOrEndcap(barrelOrEndcap));
	else 
		return dbServiceClient.getFebsByDiskOrWheel(disk, 
            XdaqDbServiceClient::stringToBarrelOrEndcap(barrelOrEndcap));
}
	
void XdaqRpcMonitor::singleFebOverTime(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
    *out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << endl;
    *out << cgicc::html().set("lang", "en").set("dir","ltr") << endl;
    *out << cgicc::title("Send SOAP Message") << endl;
    *out << cgicc::meta().set("http-equiv", "refresh").set("content", "5") << endl;
     
    xgi::Utils::getPageHeader(
            out, 
            "RPC Monitor",
            getApplicationDescriptor()->getContextDescriptor()->getURL(),
            getApplicationDescriptor()->getURN(),
            "/daq/xgi/images/Application.jpg"
            );

	*out << cgicc::style().set("type", "text/css") << endl;
	*out << cgicc::comment() << endl;
	*out << ".style1 {font-family:Arial, Helvetica, sans-serif;" << endl;
	*out << 	"font-style: italic;" << endl;
	*out << 	"font-weight: bold;" << endl;
	*out << 	"font-size: 32px;" << endl;
	*out << 	"color: #000099;}" << endl;
	*out << ".style2 {font-size: 16px; font-style: normal; color: #000000;}" << endl;
	*out << ".style3 {font-size: 16px;}" << endl;
	*out << ".style4 {font-size: 14px; font-style: normal; color: #000000;}" << endl;
	*out << ".style5 {font-family:Times New Roman, Times, serif;" << endl;
	*out << 	"font-size: 16px;" << endl;
	*out << 	"color: #ff0000;}" << endl;
	*out << cgicc::comment() << endl;
	*out << cgicc::style() << endl;
	*out << cgicc::body() << endl;
	*out << cgicc::p("FEB State as a Function of Time").set("class","style1") << cgicc::p() << endl;
	*out << cgicc::body() << endl;

	std::string startMethod = toolbox::toString("/%s/start",getApplicationDescriptor()->getURN().c_str());
	std::string stopMethod = toolbox::toString("/%s/stop",getApplicationDescriptor()->getURN().c_str());
	
	if (timer_->isActive())
	{
		*out << cgicc::a("Stop").set("href",stopMethod) << " producing data " << cgicc::br()  << std::endl;
		*out << cgicc::p("When you stop the system, this may take a few minutes. Please be patient.").set("class","style5")
			 << cgicc::p() << endl;
		*out << "Currently monitoring: " << cgicc::p(sfotChamberLocationName + " Local Eta Partition " + sfotLocalEtaPartition) << cgicc::p() << endl;
	} else
	{
		*out << cgicc::a("Start").set("href",startMethod) << " producing data " << cgicc::br() << std::endl;
		*out << "Once started, monitoring will begin at the end of the first interval." << cgicc::br() << endl;
		*out << "If you need to know values now, use \"Selective Chamber Monitoring.\"" << cgicc::br() << endl;
		*out << "Once started make sure that you are still monitoring the correct FEB." << cgicc::br() << endl;
	}
	
	*out << "Last checked FEB values at: " << lastExecutionTime_.toString() << cgicc::br() << std::endl;
	*out << "Unix Time: " << executionUnixTime_.toString() << cgicc::br() << std::endl;
	*out << "Counter: " << counter_.toString() << cgicc::br() << std::endl;
	    

	 std::string updateMethod = toolbox::toString("/%s/singleFebOverTime",getApplicationDescriptor()->getURN().c_str());
	*out << cgicc::a("Update").set("href",updateMethod) << " display" << cgicc::br() << cgicc::br() << std::endl;


	for(int j=0;j<9;j++){
	    XYChart *c = new XYChart(600, 335, 0xc0c0c0, 0, 1);
	    c->setPlotArea(55, 50, 520, 205, 0xffffff, -1, -1, 0xc0c0c0, 0xc0c0c0);
	    c->addLegend(55, 25, false, "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbi.ttf", 8)->setBackground(Chart::Transparent);
	    c->xAxis()->setTitle("Elapsed Time", "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 10);
	    c->xAxis()->setWidth(2);
        c->xAxis()->setLabelStyle("/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 8);
        c->yAxis()->setLabelStyle("/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 8);
	    LineLayer *layer = c->addLineLayer();
	    LineLayer *lineLayer = c->addLineLayer();
	    layer->setXData(DoubleArray(sfotTimes,  sizeof(sfotTimes)/sizeof(sfotTimes[0])));
	    if(j==0){
	    	c->addTitle("FEB Temperature", "arialbi.ttf", 13, 0xffffff)->setBackground(0x0, -1, 1);
	    	c->yAxis()->setTitle("Temperature (C)", "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 10);
	        c->xAxis()->setLabelStyle("/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 8);
	        c->yAxis()->setLabelStyle("/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 8);
	    	layer->addDataSet(DoubleArray(sfotTemp,  sizeof(sfotTemp)/sizeof(sfotTemp[0])), 0x800080, "Temperature");
		    lineLayer->addDataSet(DoubleArray(upperTemp, sizeof(upperTemp)/sizeof(upperTemp[0])), 0x338033, "Target Zone");
		    lineLayer->addDataSet(DoubleArray(lowerTemp, sizeof(lowerTemp)/sizeof(lowerTemp[0])), 0x338033);
	    }
	    if(j==1){
	    	c->addTitle("FEB Threshold 1", "arialbi.ttf", 13, 0xffffff)->setBackground(0x0, -1, 1);
	    	c->yAxis()->setTitle("Threshold 1 (mV)", "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 10);
	        c->xAxis()->setLabelStyle("/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 8);
	        c->yAxis()->setLabelStyle("/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 8);
	    	layer->addDataSet(DoubleArray(sfotTh1,  sizeof(sfotTh1)/sizeof(sfotTh1[0])), 0x800080, "TH1");
		    lineLayer->addDataSet(DoubleArray(upperTh1, sizeof(upperTh1)/sizeof(upperTh1[0])), 0x338033, "Target Zone");
		    lineLayer->addDataSet(DoubleArray(lowerTh1, sizeof(lowerTh1)/sizeof(lowerTh1[0])), 0x338033);
	    }	    	
	    if(j==2){
	    	c->addTitle("FEB Threshold 2", "arialbi.ttf", 13, 0xffffff)->setBackground(0x0, -1, 1);
	    	c->yAxis()->setTitle("Threshold 2 (mV)", "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 10);
	        c->xAxis()->setLabelStyle("/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 8);
	        c->yAxis()->setLabelStyle("/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 8);
	    	layer->addDataSet(DoubleArray(sfotTh2,  sizeof(sfotTh2)/sizeof(sfotTh2[0])), 0x800080, "TH2");
		    lineLayer->addDataSet(DoubleArray(upperTh2, sizeof(upperTh2)/sizeof(upperTh2[0])), 0x338033, "Target Zone");
		    lineLayer->addDataSet(DoubleArray(lowerTh2, sizeof(lowerTh2)/sizeof(lowerTh2[0])), 0x338033);
	    }	    	
	    if(j==3){
	    	c->addTitle("FEB Threshold 3", "arialbi.ttf", 13, 0xffffff)->setBackground(0x0, -1, 1);
	    	c->yAxis()->setTitle("Threshold 3 (mV)", "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 10);
	        c->xAxis()->setLabelStyle("/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 8);
	        c->yAxis()->setLabelStyle("/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 8);
	        c->xAxis()->setLabelStyle("/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 8);
	        c->yAxis()->setLabelStyle("/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 8);
	    	layer->addDataSet(DoubleArray(sfotTh3,  sizeof(sfotTh3)/sizeof(sfotTh3[0])), 0x800080, "TH3");
		    lineLayer->addDataSet(DoubleArray(upperTh3, sizeof(upperTh3)/sizeof(upperTh3[0])), 0x338033, "Target Zone");
		    lineLayer->addDataSet(DoubleArray(lowerTh3, sizeof(lowerTh3)/sizeof(lowerTh3[0])), 0x338033);
	    }	    	
	    if(j==4){
	    	c->addTitle("FEB Threshold 4", "arialbi.ttf", 13, 0xffffff)->setBackground(0x0, -1, 1);
	    	c->yAxis()->setTitle("Threshold 4 (mV)", "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 10);
	    	layer->addDataSet(DoubleArray(sfotTh4,  sizeof(sfotTh4)/sizeof(sfotTh4[0])), 0x800080, "TH4");
		    lineLayer->addDataSet(DoubleArray(upperTh4, sizeof(upperTh4)/sizeof(upperTh4[0])), 0x338033, "Target Zone");
		    lineLayer->addDataSet(DoubleArray(lowerTh4, sizeof(lowerTh4)/sizeof(lowerTh4[0])), 0x338033);
	    }	    	
	    if(j==5){
	    	c->addTitle("FEB VMON1", "arialbi.ttf", 13, 0xffffff)->setBackground(0x0, -1, 1);
	    	c->yAxis()->setTitle("VMON1 (mV)", "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 10);
	        c->xAxis()->setLabelStyle("/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 8);
	        c->yAxis()->setLabelStyle("/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 8);
	    	layer->addDataSet(DoubleArray(sfotVmon1,  sizeof(sfotVmon1)/sizeof(sfotVmon1[0])), 0x800080, "VMON1");
		    lineLayer->addDataSet(DoubleArray(upperVmon1, sizeof(upperVmon1)/sizeof(upperVmon1[0])), 0x338033, "Target Zone");
		    lineLayer->addDataSet(DoubleArray(lowerVmon1, sizeof(lowerVmon1)/sizeof(lowerVmon1[0])), 0x338033);
	    }	    	
	    if(j==6){
	    	c->addTitle("FEB VMON2", "arialbi.ttf", 13, 0xffffff)->setBackground(0x0, -1, 1);
	    	c->yAxis()->setTitle("VMON2 (mV)", "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 10);
	        c->xAxis()->setLabelStyle("/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 8);
	        c->yAxis()->setLabelStyle("/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 8);
	    	layer->addDataSet(DoubleArray(sfotVmon2,  sizeof(sfotVmon2)/sizeof(sfotVmon2[0])), 0x800080, "VMON2");
		    lineLayer->addDataSet(DoubleArray(upperVmon2, sizeof(upperVmon2)/sizeof(upperVmon2[0])), 0x338033, "Target Zone");
		    lineLayer->addDataSet(DoubleArray(lowerVmon2, sizeof(lowerVmon2)/sizeof(lowerVmon2[0])), 0x338033);
	    }	    	
	    if(j==7){
	    	c->addTitle("FEB VMON3", "arialbi.ttf", 13, 0xffffff)->setBackground(0x0, -1, 1);
	    	c->yAxis()->setTitle("VMON3 (mV)", "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 10);
	        c->xAxis()->setLabelStyle("/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 8);
	        c->yAxis()->setLabelStyle("/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 8);
	    	layer->addDataSet(DoubleArray(sfotVmon3,  sizeof(sfotVmon3)/sizeof(sfotVmon3[0])), 0x800080, "VMON3");
		    lineLayer->addDataSet(DoubleArray(upperVmon3, sizeof(upperVmon3)/sizeof(upperVmon3[0])), 0x338033, "Target Zone");
		    lineLayer->addDataSet(DoubleArray(lowerVmon3, sizeof(lowerVmon3)/sizeof(lowerVmon3[0])), 0x338033);
	    }	    	
	    if(j==8){
	    	c->addTitle("FEB VMON4", "arialbi.ttf", 13, 0xffffff)->setBackground(0x0, -1, 1);
	    	c->yAxis()->setTitle("VMON4 (mV)", "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 10);
	        c->xAxis()->setLabelStyle("/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 8);
	        c->yAxis()->setLabelStyle("/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 8);
	    	layer->addDataSet(DoubleArray(sfotVmon4,  sizeof(sfotVmon4)/sizeof(sfotVmon4[0])), 0x800080, "VMON4");
		    lineLayer->addDataSet(DoubleArray(upperVmon4, sizeof(upperVmon4)/sizeof(upperVmon4[0])), 0x338033, "Target Zone");
		    lineLayer->addDataSet(DoubleArray(lowerVmon4, sizeof(lowerVmon4)/sizeof(lowerVmon4[0])), 0x338033);
	    }	    	
	    lineLayer->setXData(DoubleArray(sfotTimes, sizeof(sfotTimes)/sizeof(sfotTimes[0])));
	    lineLayer->setLineWidth(2);
	    c->addInterLineLayer(lineLayer->getLine(0), lineLayer->getLine(1), 0x8099ff99, 0x8099ff99);
	    c->addInterLineLayer(layer->getLine(0), lineLayer->getLine(1), 0xff0000, Chart::Transparent);
	    c->addInterLineLayer(layer->getLine(0), lineLayer->getLine(0), Chart::Transparent, 0xff);
	    c->yAxis()->setWidth(2);
	    layer->setLineWidth(2);
	    c->addText(565, 255,"<*font=/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/timesbi.ttf,size=10,color=804040*>CMS\nRPC<*/*>"
	        )->setAlignment(Chart::BottomRight);
	
		ostringstream ostr;
		ostr << std::getenv("XDAQ_ROOT") << "/htdocs" << picPath.toString() << "linecompare" << j << ".png";
		c->makeChart(ostr.str().c_str());
	    delete c;
	}
	*out << cgicc::img().set("src", picPath.toString()+"linecompare0.png") << cgicc::br() << std::endl;
	*out << cgicc::img().set("src", picPath.toString()+"linecompare1.png") << cgicc::br() << std::endl;
	*out << cgicc::img().set("src", picPath.toString()+"linecompare2.png") << cgicc::br() << std::endl;
    if (sfotEndcap == true){
		*out << cgicc::img().set("src", picPath.toString()+"linecompare3.png") << cgicc::br() << std::endl;
		*out << cgicc::img().set("src", picPath.toString()+"linecompare4.png") << cgicc::br() << std::endl;
    }
	*out << cgicc::img().set("src", picPath.toString()+"linecompare5.png") << cgicc::br() << std::endl;
	*out << cgicc::img().set("src", picPath.toString()+"linecompare6.png") << cgicc::br() << std::endl;
    if (sfotEndcap == true){
		*out << cgicc::img().set("src", picPath.toString()+"linecompare7.png") << cgicc::br() << std::endl;
		*out << cgicc::img().set("src", picPath.toString()+"linecompare8.png") << cgicc::br() << std::endl;
    }
    *out << cgicc::br();
    *out << cgicc::br();

    makeMenu(in, out);

    xgi::Utils::getPageFooter(*out);
}

void XdaqRpcMonitor::start(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	if (timer_->isActive())
	{
		for(int i=0;i<10;i++){
			if(switch_==0) wait(6);
			else break;
		}
		timer_->remove("XdaqRpcMonitor");
		timer_->stop();
		std::string infoSpaceName = toolbox::toString("urn:xdaq-monitorable:XdaqRpcMonitor-%d", getApplicationDescriptor()->getInstance());
		xdata::InfoSpace::remove(infoSpaceName);
	}
		
		for(int i=0;i<20;i++){
		sfotTh1[i]=0.0;
		sfotTh2[i]=0.0;
		sfotTh3[i]=0.0;
		sfotTh4[i]=0.0;
		sfotVmon1[i]=0.0;
		sfotVmon2[i]=0.0;
		sfotVmon3[i]=0.0;
		sfotVmon4[i]=0.0;
		sfotTemp[i]=0.0;
		sfotTimes[i]=0.0;
	}
	counter_ = 0;
	executionUnixTime_=0;
	lastExecutionTime_ = "";
	infoSpace_ = 0;
	switch_=1;
	sfotCcuAddress = singleFebCcuAddress;
	sfotChannel = singleFebChannel;
	sfotAddress = singleFebAddress;
	sfotChamberLocationName = singleFebChamberLocationName;
	sfotXdaqAppInstance = singleFebXdaqAppInstance;
	sfotLocalEtaPartition = singleFebLocalEtaPartition;
	sfotEndcap = singleFebEndcap;
	
	setGraphLimits();
	
	std::string infoSpaceName = toolbox::toString("urn:xdaq-monitorable:XdaqRpcMonitor-%d", getApplicationDescriptor()->getInstance());
	infoSpace_ = xdata::InfoSpace::get(infoSpaceName);
	
	infoSpace_->fireItemAvailable("counter", &counter_, 0);
	infoSpace_->fireItemAvailable("lastUpdated", &lastExecutionTime_, 0);
	
	toolbox::TimeInterval interval;
	interval.fromString("00:00:00:00:05");
	toolbox::TimeVal startTime;
	startTime = toolbox::TimeVal::gettimeofday();
	
	timer_->start();
	timer_->scheduleAtFixedRate(startTime,this, interval, infoSpace_, "XdaqRpcMonitor" );
	
	string appUrl = "/" + getApplicationDescriptor()->getURN();
	string redirect = "0; " + appUrl + "/singleFebOverTime"; // This is needed, because using the refresh button when the browser is
     													  // at appUrl + "/start" or "/stop" will crash Xdaq	 
    *out << cgicc::meta().set("http-equiv", "refresh").set("content", redirect) << endl;
}

void XdaqRpcMonitor::stop(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	for(int i=0;i<10;i++){
		if(switch_==0) wait(6);
		else break;
	}
	timer_->remove("XdaqRpcMonitor");
	timer_->stop();
	
	std::string infoSpaceName = toolbox::toString("urn:xdaq-monitorable:XdaqRpcMonitor-%d", getApplicationDescriptor()->getInstance());
	xdata::InfoSpace::remove(infoSpaceName);

	string appUrl = "/" + getApplicationDescriptor()->getURN();
	string redirect = "0; " + appUrl + "/singleFebOverTime"; // This is needed, because using the refresh button when the browser is
     													  // at appUrl + "/start" or "/stop" will crash Xdaq	 
    *out << cgicc::meta().set("http-equiv", "refresh").set("content", redirect) << endl;
}

void XdaqRpcMonitor::timeExpired (toolbox::task::TimerEvent& e)
{ 
	switch_=0;
	std::string name = e.getTimerTask()->name;
	infoSpace_->lock();
	counter_ = counter_ + 1;
	time_t cur_time;
	executionUnixTime_ = time(&cur_time);
	lastExecutionTime_ = e.getTimerTask()->lastExecutionTime.toString(toolbox::TimeVal::loc);

	rpct::xdaqutils::XdaqDbServiceClient::FebAccessInfoVector sfotAccessInfoVector;
	xdata::Bag<FebAccessInfo> sfotAccessInfo;
	FebAccessInfo& accessInfo2 = sfotAccessInfo.bag;
	    accessInfo2.setXdaqAppInstance(sfotXdaqAppInstance);    
		accessInfo2.setFebLocalEtaPartition(sfotLocalEtaPartition);    
		accessInfo2.setChamberLocationName(sfotChamberLocationName);    
		accessInfo2.setCcuAddress(sfotCcuAddress);    
		accessInfo2.setI2cChannel(sfotChannel);    
		accessInfo2.setFebAddress(sfotAddress);
		sfotAccessInfoVector.push_back(sfotAccessInfo);
	    
    delete lastSfotReadResponseBag;
	if(sfotAccessInfoVector.empty()){
		LOG4CPLUS_INFO(getApplicationLogger(),"The Feb Access Info Vector is empty for single feb over time. Timer is being stopped");
		switch_=1;
		xgi::Output out;
		string appUrl = "/" + getApplicationDescriptor()->getURN();
		string redirect = "0; " + appUrl + "/stop";
		out << cgicc::meta().set("http-equiv", "refresh").set("content", redirect) << endl;
	}
	else{

	    lastSfotReadResponseBag = monitorSystem(&sfotAccessInfoVector, sfotEndcap);

        MassiveReadResponse& readResponse = lastSfotReadResponseBag->bag;
        typedef FebValuesVector ValuesVector;
        ValuesVector& valuesVector = readResponse.getFebValues();
        for (ValuesVector::iterator iVal = valuesVector.begin(); iVal != valuesVector.end(); ++iVal) {
            FebValues& febValues = iVal->bag;
            typedef FebValues::Values Values;
            Values& values = febValues.getValues();
            for(int i=0;i<19;i++){
            	sfotTimes[i] = sfotTimes[i+1];
            	if (sfotTimes[i]<1) sfotTimes[i] = chartTime2((executionUnixTime_-200)+(i*10));
            	sfotTemp[i] = sfotTemp[i+1];
				sfotTh1[i] = sfotTh1[i+1];
				sfotTh2[i] = sfotTh2[i+1];
				sfotTh3[i] = sfotTh3[i+1];
				sfotTh4[i] = sfotTh4[i+1];
				sfotVmon1[i] = sfotVmon1[i+1];
				sfotVmon2[i] = sfotVmon2[i+1];
				sfotVmon3[i] = sfotVmon3[i+1];
				sfotVmon4[i] = sfotVmon4[i+1];
            }
            sfotTimes[19]=chartTime2(executionUnixTime_);
            if(sfotEndcap == true){
	            sfotTemp[19] = values[0];
				sfotTh1[19] = values[1];
				sfotTh2[19] = values[2];
				sfotTh3[19] = values[3];
				sfotTh4[19] = values[4];
				sfotVmon1[19] = values[5];
				sfotVmon2[19] = values[6];
				sfotVmon3[19] = values[7];
				sfotVmon4[19] = values[8];
            }
            else{
	            sfotTemp[19] = values[0];
				sfotTh1[19] = values[1];
				sfotTh2[19] = values[2];
				sfotVmon1[19] = values[3];
				sfotVmon2[19] = values[4];
            }
        }           
	
	infoSpace_->unlock();
	switch_=1;
	}	
}
	
void XdaqRpcMonitor::histogram(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception)
{
    *out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << endl;
    *out << cgicc::html().set("lang", "en").set("dir","ltr") << endl;
    *out << cgicc::title("Send SOAP Message") << endl;
     
    xgi::Utils::getPageHeader(
            out, 
            "RPC Monitor",
            getApplicationDescriptor()->getContextDescriptor()->getURL(),
            getApplicationDescriptor()->getURN(),
            "/daq/xgi/images/Application.jpg"
            );

    *out << cgicc::br(); 
	*out << cgicc::style().set("type", "text/css") << endl;
	*out << cgicc::comment() << endl;
	*out << ".style1 {font-family:Arial, Helvetica, sans-serif;" << endl;
	*out << 	"font-style: italic;" << endl;
	*out << 	"font-weight: bold;" << endl;
	*out << 	"font-size: 32px;" << endl;
	*out << 	"color: #000099;}" << endl;
	*out << cgicc::comment() << endl;
	*out << cgicc::style() << endl;

	histoData.clear();
	unixTimeStripResponse = 0;

    try {
	    cgicc::Cgicc cgi(in);
		int disk = cgi["disk"]->getIntegerValue();
	    int layer = cgi["layer"]->getIntegerValue();
	    int sector = cgi["sector"]->getIntegerValue();
	    string subsector = cgi["subsector"]->getValue();
	    if (subsector=="pp") subsector="++";
	    if (subsector=="p") subsector="+";
		string barrelOrEndcap = cgi["barrelOrEndcap"]->getValue();
		XdaqDbServiceClient::ChamStripAccessInfoVector* chamStripAccessInfoVector
			= dbServiceClient.getChamStripByChamLoc(disk, layer, sector, subsector,
			XdaqDbServiceClient::stringToBarrelOrEndcap(barrelOrEndcap));
		if(chamStripAccessInfoVector->empty()){
    		
    		*out << cgicc::p("No results were returned from the database. Perhaps this chamber does not have a link board assignment in the database.").set("class","style1");
    		*out << cgicc::p() << endl;
		    *out << cgicc::br(); 
    	}
    	else{ 
    		std::vector<rpct::BoardHistoInfo> boardsVector;
    		std::vector<rpct::FebHistoInfo> febsVector;
    		std::vector<int> chipIds;
    		std::vector<int> febIds;
	 	    for (XdaqDbServiceClient::ChamStripAccessInfoVector::iterator iCSAI = chamStripAccessInfoVector->begin();
		    	iCSAI != chamStripAccessInfoVector->end(); ++iCSAI) {            
			        ChamStripAccessInfo& chamStripAccessInfo = iCSAI->bag;
			        string chamberName = chamStripAccessInfo.getChamberLocationName();
				    int boardId = chamStripAccessInfo.getBoardId();
				    int xdaqAppInstance = chamStripAccessInfo.getXdaqAppInstance();
				    int chipId = chamStripAccessInfo.getChipId();
				    int febId = chamStripAccessInfo.getFebId();
				    int febConnectorNum = chamStripAccessInfo.getFebConnectorNum();
				    string febLocalEtaPartition = chamStripAccessInfo.getFebLocalEtaPartition();
				    int febI2cAddress = chamStripAccessInfo.getFebI2cAddress();
				    int cableChanNum = chamStripAccessInfo.getCableChanNum();
				    int chamberStripNum = chamStripAccessInfo.getChamberStripNum();
				    int chamberStripId = chamStripAccessInfo.getChamberStripId();
	    			LOG4CPLUS_INFO(getApplicationLogger(), "Chamber Name: " << chamberName << "  "
	    			<< "Board Id: " << boardId << "  "
	    			<< "Instance: " << xdaqAppInstance << "  "
	    			<< "Chip Id: " << chipId << "  "
	    			<< "Feb Id: " << febId << "  "
	    			<< "FebConnectorNum: " << febConnectorNum << "  "
	    			<< "FebLocalEtaPartition: " << febLocalEtaPartition << "  "
	    			<< "FebI2cAddress: " << febI2cAddress << "  "
	    			<< "CableChanNum: " << cableChanNum << "  "
	    			<< "ChamberStripNum: " << chamberStripNum << "  "		
	    			<< "ChamberStripId: " << chamberStripId << endl);			
				    chipIds.push_back(chamStripAccessInfo.getChipId());
				    febIds.push_back(chamStripAccessInfo.getFebId());
		    }
			vector<int>::iterator iCI, iCI_end;
			iCI_end = unique(chipIds.begin(), chipIds.end());
			vector<int>::iterator iFI,iFI_end;
			iFI_end = unique(febIds.begin(), febIds.end());
			for(iCI = chipIds.begin(); iCI < iCI_end; iCI++){
				rpct::BoardHistoInfo boardHistoInfo;
				xdata::Integer chip=*iCI;
				boardHistoInfo.setChipId(chip);
				xdata::Integer boardId;
				xdata::Integer instance;
				xdata::String name;
		 	    for (XdaqDbServiceClient::ChamStripAccessInfoVector::iterator iCSAI = chamStripAccessInfoVector->begin();
			    	iCSAI != chamStripAccessInfoVector->end(); ++iCSAI) {            
				        ChamStripAccessInfo& chamStripAccessInfo = iCSAI->bag;
				    if(chamStripAccessInfo.getChipId()==chip){ 
					    boardId = chamStripAccessInfo.getBoardId();
					    instance = chamStripAccessInfo.getXdaqAppInstance();
					    name = chamStripAccessInfo.getChamberLocationName();
				    }
			    }
			    boardHistoInfo.setId(boardId);
			    boardHistoInfo.setXdaqAppInstance(instance);
			    boardHistoInfo.setChamberName(name);
				boardsVector.push_back(boardHistoInfo);
			}
			int numBoards=boardsVector.size();
			LOG4CPLUS_INFO(getApplicationLogger(), "Number of boards: " << numBoards << endl);
			int numPics=0;
			for(iFI = febIds.begin(); iFI < iFI_end; iFI++){
				xdata::Integer feb=*iFI;
				std::vector<int> connectors;
		 	    for (XdaqDbServiceClient::ChamStripAccessInfoVector::iterator iCSAI = chamStripAccessInfoVector->begin();
			    	iCSAI != chamStripAccessInfoVector->end(); ++iCSAI) {            
				        ChamStripAccessInfo& chamStripAccessInfo = iCSAI->bag;
				    if(chamStripAccessInfo.getFebId()==feb){
					    connectors.push_back(chamStripAccessInfo.getFebConnectorNum());
				    }
			    }
				vector<int>::iterator iConnector, iConnector_end;
				iConnector_end = unique(connectors.begin(), connectors.end());
				for(iConnector = connectors.begin(); iConnector < iConnector_end; iConnector++){
					rpct::FebHistoInfo febHistoInfo;
					febHistoInfo.setId(feb);
					xdata::Integer connector=*iConnector;
					febHistoInfo.setFebConnectorNum(connector);
					xdata::String partition;
					xdata::Vector<xdata::Integer> cableChannelMap;
					xdata::Vector<xdata::Integer> stripMap;
					xdata::Vector<xdata::Integer> stripIdMap;
					xdata::Integer i2c;
					xdata::Integer chip;
			 	    for (XdaqDbServiceClient::ChamStripAccessInfoVector::iterator iCSAI = chamStripAccessInfoVector->begin();
				    	iCSAI != chamStripAccessInfoVector->end(); ++iCSAI) {            
					        ChamStripAccessInfo& chamStripAccessInfo = iCSAI->bag;
					    if(chamStripAccessInfo.getFebId()==feb && chamStripAccessInfo.getFebConnectorNum()==connector){
						    partition = chamStripAccessInfo.getFebLocalEtaPartition();
						    i2c = chamStripAccessInfo.getFebI2cAddress();
						    chip = chamStripAccessInfo.getChipId();
						    cableChannelMap.push_back(chamStripAccessInfo.getCableChanNum());
						    if(partition == "B") stripMap.push_back(chamStripAccessInfo.getChamberStripNum()+32);
						    else if(partition == "C") stripMap.push_back(chamStripAccessInfo.getChamberStripNum()+64);
						    else stripMap.push_back(chamStripAccessInfo.getChamberStripNum());
						    stripIdMap.push_back(chamStripAccessInfo.getChamberStripId());
					    }
				    }
				    febHistoInfo.setChip(chip);
				    febHistoInfo.setFebLocalEtaPartition(partition);
					febHistoInfo.setCableChannelNums(cableChannelMap);
					febHistoInfo.setStrips(stripMap);
					febHistoInfo.setStripIds(stripIdMap);
					febsVector.push_back(febHistoInfo);
				}
			}
			int size=febsVector.size();
			LOG4CPLUS_INFO(getApplicationLogger(), "Number of febs (double for endcap): " << size << endl);
			for(std::vector<rpct::BoardHistoInfo>::iterator iBoard = boardsVector.begin(); iBoard != boardsVector.end(); ++iBoard){
				rpct::BoardHistoInfo boardHistoInfo = *iBoard;
				int chip = boardHistoInfo.getChipId();
				int board = boardHistoInfo.getId();
				int instance = boardHistoInfo.getXdaqAppInstance();
				string chamName = boardHistoInfo.getChamberName();
				LOG4CPLUS_INFO(getApplicationLogger(), "ChipId: " << chip << "  "
				<< "Instance: " << instance << endl);
				double data4[2][96];
				time_t cur_time;
				unixTimeStripResponse = time(&cur_time);
					linkBoardCounters(instance, chip, data4);
//					    int RANGE_MIN = 16000;
//					    int RANGE_MAX = 20000;
/*					for(int i=0;i<96;i++){
						data4[0][i]=(i+1)*1000;//(((double) rand() / (double) RAND_MAX) * RANGE_MAX + RANGE_MIN);
						data4[1][i]=(i+1)*1000;//(((double) rand() / (double) RAND_MAX) * RANGE_MAX + RANGE_MIN);
					}
*/				double stripCounts[96];
				double lbChanCnts[96];
				double usedLbChanCnts[96];
				double stripNumbers[96];
				double lbChanNumbers[96];
				double stripNumIds[96];
				for(int i=0;i<96;i++){
					stripCounts[i]=0;
					usedLbChanCnts[i]=0;
					lbChanCnts[i]=data4[0][i];
					stripNumbers[i]=0;
					stripNumIds[i]=0;
					lbChanNumbers[i]=i+1;
				}
    			for(std::vector<rpct::FebHistoInfo>::iterator iFeb = febsVector.begin(); iFeb != febsVector.end(); ++iFeb){
					rpct::FebHistoInfo febHistoInfo = *iFeb;
					int id = febHistoInfo.getId();
					int chip_ref = febHistoInfo.getChip();
					if(chip_ref != chip) continue;
    				LOG4CPLUS_INFO(getApplicationLogger(), "FebId: " << id << endl);
    				int connectorNum = febHistoInfo.getFebConnectorNum();
    				xdata::Vector<xdata::Integer> cableChannelMap = febHistoInfo.getCableChannelNums();
    				xdata::Vector<xdata::Integer> stripMap = febHistoInfo.getStrips();
    				xdata::Vector<xdata::Integer> stripIdMap = febHistoInfo.getStripIds();
    				switch (connectorNum){
    					case 1:
    						for(uint i=0;i<cableChannelMap.size();i++){
    							int j=cableChannelMap[i];
    							int k=stripMap[i];
    							stripCounts[k-1]=data4[0][j-1];
    							usedLbChanCnts[j-1]=data4[0][j-1];
    							stripNumbers[k-1]=k;
    							stripNumIds[k-1]=stripIdMap[i];
    						}
    						break;
    					case 2:
    						for(uint i=0;i<cableChannelMap.size();i++){
    							int j=cableChannelMap[i]+16;
    							int k=stripMap[i];
    							stripCounts[k-1]=data4[0][j-1];
    							usedLbChanCnts[j-1]=data4[0][j-1];
    							stripNumbers[k-1]=k;
    							stripNumIds[k-1]=stripIdMap[i];
    						}
    						break;
    					case 3:
    						for(uint i=0;i<cableChannelMap.size();i++){
    							int j=cableChannelMap[i]+32;
    							int k=stripMap[i];
    							stripCounts[k-1]=data4[0][j-1];
    							usedLbChanCnts[j-1]=data4[0][j-1];
    							stripNumbers[k-1]=k;
    							stripNumIds[k-1]=stripIdMap[i];
    						}
    						break;
    					case 4:
    						for(uint i=0;i<cableChannelMap.size();i++){
    							int j=cableChannelMap[i]+48;
    							int k=stripMap[i];
    							stripCounts[k-1]=data4[0][j-1];
    							stripNumbers[k-1]=k;
    							usedLbChanCnts[j-1]=data4[0][j-1];
    							stripNumIds[k-1]=stripIdMap[i];
    						}
    						break;
    					case 5:
    						for(uint i=0;i<cableChannelMap.size();i++){
    							int j=cableChannelMap[i]+64;
    							int k=stripMap[i];
    							stripCounts[k-1]=data4[0][j-1];
    							usedLbChanCnts[j-1]=data4[0][j-1];
    							stripNumbers[k-1]=k;
    							stripNumIds[k-1]=stripIdMap[i];
    						}
    						break;
    					case 6:
    						for(uint i=0;i<cableChannelMap.size();i++){
    							int j=cableChannelMap[i]+80;
    							int k=stripMap[i];
    							stripCounts[k-1]=data4[0][j-1];
    							stripNumbers[k-1]=k;
    							usedLbChanCnts[j-1]=data4[0][j-1];
    							stripNumIds[k-1]=stripIdMap[i];
    						}
    						break;
    				}
    			}

//Chamber Strip Graph

			   XYChart *c = new XYChart(1500, 750, Chart::goldColor(), -1, 2);
			
			   ostringstream title;
			   title << "Strip Response for " << chamName << "\n"
			   		<< "LinkboardId: " << board << "  "<< "ChipId: " << chip << "  " << "Xdaq Linkbox Instance: "
			   		<< instance;
			    c->addTitle(title.str().c_str(), "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 24)->setBackground(
			        Chart::metalColor(0x9999ff), -1, 1);
			    c->setPlotArea(100, 60, 1300, 580);
			    BarLayer *barLayer = c->addBarLayer(DoubleArray(stripCounts, sizeof(stripCounts)/sizeof(stripCounts[0])), IntArray(0,0));
				barLayer->setBorderColor(-1, 1);
				barLayer->setXData(DoubleArray(stripNumbers, sizeof(stripNumbers)/sizeof(stripNumbers[0])));
			    c->yAxis()->setTitle("Counts", "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbi.ttf", 12, 0x80);
				c->xAxis()->setTitle("Strip Number", "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbi.ttf", 12, 0x80);
				c->xAxis()->setLabelStyle("/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 6);
                c->yAxis()->setLabelStyle("/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 6);
				c->yAxis()->setLogScale();
				ostringstream febString;
				int j=100;
				string partitionInc = "";
    			for(std::vector<rpct::FebHistoInfo>::iterator iFeb = febsVector.begin(); iFeb != febsVector.end(); ++iFeb){
					rpct::FebHistoInfo febHistoInfo = *iFeb;
					int chip_ref = febHistoInfo.getChip();
					if(chip_ref != chip) continue;
					string partition = febHistoInfo.getFebLocalEtaPartition();
					if(partition==partitionInc) continue;
					febString << "<*block*>Eta Partition: " << "\t" << partition << "<*/*> ";
				    TextBox *textbox = c->addText(j, 700, febString.str().c_str(), "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 12);
				    textbox->setBackground(0xc0c0c0, 0, 1);
				    partitionInc = partition;
				    febString.str("");
				    j+=130;
    			}

				ostringstream ostr;
				ostr << std::getenv("XDAQ_ROOT") << "/htdocs" << picPath.toString() << "stripResponse" << numPics << ".png";
				c->makeChart(ostr.str().c_str());
			    delete c;
				std::string pic = picPath.toString() + toolbox::toString("stripResponse%d.png", numPics);
			    *out << cgicc::img().set("src", pic);
			    
				for(int i=0;i<96;i++){
					if((int)stripNumIds[i] != 0){
						histoData[(int)stripNumIds[i]]=(int)stripCounts[i];
					}
				}

//Linkboard Graph

			    XYChart *d = new XYChart(1500, 750, Chart::goldColor(), -1, 2);
			
			   ostringstream title2;
			   title2 << "Link Board Channels" << "\n"
			   		<< "LinkboardId: " << board << "  "<< "ChipId: " << chip << "  " << "Xdaq Linkbox Instance: "
			   		<< instance;
			    d->addTitle(title2.str().c_str(), "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 24)->setBackground(
			        Chart::metalColor(0x9999ff), -1, 1);
			    d->setPlotArea(100, 60, 1300, 580);
			    BarLayer *barLayerUsedChan = d->addBarLayer(DoubleArray(usedLbChanCnts, sizeof(usedLbChanCnts)/sizeof(usedLbChanCnts[0])), IntArray(0,0));
			    BarLayer *barLayerChan = d->addBarLayer(DoubleArray(lbChanCnts, sizeof(lbChanCnts)/sizeof(lbChanCnts[0])), 0x000000);
				barLayerUsedChan->setBorderColor(-1, 1);
				barLayerChan->setXData(DoubleArray(lbChanNumbers, sizeof(lbChanNumbers)/sizeof(lbChanNumbers[0])));
				barLayerUsedChan->setXData(DoubleArray(lbChanNumbers, sizeof(lbChanNumbers)/sizeof(lbChanNumbers[0])));
			    d->yAxis()->setTitle("Counts", "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbi.ttf", 12, 0x80);
				d->xAxis()->setTitle("Channel Number", "/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbi.ttf", 12, 0x80);
				d->xAxis()->setLabelStyle("/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 6);
                d->yAxis()->setLabelStyle("/nfshome0/rpcdev/rpct/xdaqRpcMonitor/fonts/arialbd.ttf", 6);
				d->yAxis()->setLogScale();
				ostringstream ostr2;
				ostr2 << std::getenv("XDAQ_ROOT") << "/htdocs" << picPath.toString() << "lbChannelResponse" << numPics << ".png";
				d->makeChart(ostr2.str().c_str());
			    delete d;
				std::string pic2 = picPath.toString() + toolbox::toString("lbChannelResponse%d.png", numPics);
			    *out << cgicc::img().set("src", pic2);
			    numPics++;
			    
			}
    	}
		delete chamStripAccessInfoVector;
    }
    catch (xdaq::exception::Exception& e) {
        XCEPT_RETHROW(xgi::exception::Exception, "Cannot send message", e);                                                 
    }     
    catch (exception& e) {
        XCEPT_RAISE(xgi::exception::Exception, string("Trouble with ChamStripAccessVector ") + e.what());                                                 
    }             
    catch (...) {
        XCEPT_RAISE(xgi::exception::Exception, "Cannot send message - unknown reason");                                                 
    }                                                                                                                            

	string appUrl6 = "/" + getApplicationDescriptor()->getURN();
/*    *out << cgicc::table().set("width", "250").set("border", "0") << endl;
     *out << cgicc::tr() << endl;
      *out << cgicc::td().set("width", "101") << endl;
		*out << cgicc::form().set("action", appUrl6 + "/saveHistogram"
			).set("method","post").set("name","form3") << endl;
		 *out << cgicc::input().set("name", "Save histogram data to database").set("type","submit").set("id",
			"Save histogram data to database").set("value","Save histogram data to database") << endl;
		*out << cgicc::form() << endl;
      *out << cgicc::td() << endl;
     *out << cgicc::tr() << endl;
    *out << cgicc::table() << endl;
	*out << cgicc::p() << endl;
	*out << cgicc::p() << endl;
*/
    xgi::Utils::getPageFooter(*out);
}

void XdaqRpcMonitor::linkBoardCounters(int instance, int chipId, double rate[2][96]) throw (xgi::exception::Exception)
{
//    const int instance = 0;
//    const int chipId = 521;
    const int binCount = 96;
//    const int maxLength = 256;
//    const int width = 24;
    typedef XdaqDiagAccessClient::DiagIdVector DiagIds;
    typedef XdaqDiagAccessClient::HistoMgrInfoVector Histos;
    typedef XdaqDiagAccessClient::PulserInfoVector Pulsers;


    DiagIdBag diagCtrlId1;
    DiagIdBag diagCtrlId2;
    DiagIdBag diagCtrlId3;
    diagCtrlId1.bag.init(chipId, "DAQ_DIAG_CTRL");
    diagCtrlId2.bag.init(chipId, "PULSER_DIAG_CTRL");
    diagCtrlId3.bag.init(chipId, "STATISTICS_DIAG_CTRL");
    DiagIds diagCtrlIds;
    diagCtrlIds.push_back(diagCtrlId1);
    diagCtrlIds.push_back(diagCtrlId2);
    diagCtrlIds.push_back(diagCtrlId3);


//    DiagId rateId1;
    DiagId rateId2;
//    DiagId rateId3;
//    rateId1.init(chipId, "BX_HIST");
    rateId2.init(chipId, "RATE");
//    rateId3.init(chipId, "RATE_WIND");
//    HistoMgrInfoBag histoInfoBag1;
    HistoMgrInfoBag histoInfoBag2;
//    HistoMgrInfoBag histoInfoBag3;
//    histoInfoBag1.bag.init(rateId1, binCount, rateId1);
    histoInfoBag2.bag.init(rateId2, binCount, rateId2);
//    histoInfoBag3.bag.init(rateId3, binCount, rateId3);
    Histos histos;
//    histos.push_back(histoInfoBag1);
    histos.push_back(histoInfoBag2);
//    histos.push_back(histoInfoBag3);

//    DiagId pulserId1;
//    pulserId1.init(chipId, "PULSER");
//    PulserInfoBag pulserInfoBag1;
//    pulserInfoBag1.bag.init(pulserId1, maxLength, width, pulserId1);
//    Pulsers pulsers;
//    pulsers.push_back(pulserInfoBag1);

    diagAccessClient.resetDiagCtrl(diagCtrlIds, "XdaqLBoxAccess", instance);
    diagAccessClient.resetHistoMgr(histos, "XdaqLBoxAccess", instance);

//    xdata::Vector<xdata::Binary> pulserData;
//    xdata::Binary pulse;
//    for (int i = 0; i < 128; i++) {
//        pulse = (i % 8 == 0) ? 0xffffff : 0;
//        pulserData.push_back(pulse);
//        //System.out.println(pulserData[i].toString());
//    }
//    cout << "Configuring pulsers" << endl;
//    xdata::Binary pulseLength = pulserData.size();
//    diagAccessClient.configurePulser(pulsers, pulserData, pulseLength, false, "XdaqLBoxAccess", 0);

    cout << "Configuring diag ctrls" << endl;
    xdata::Binary counterLimit(40000000ul);
    diagAccessClient.configureDiagCtrl(diagCtrlIds, counterLimit, IDiagCtrl::ttManual, "XdaqLBoxAccess", instance);
   
    cout << "Starting diag ctrls" << endl;
    diagAccessClient.startDiagCtrl(diagCtrlIds, "XdaqLBoxAccess", instance);
    while (!diagAccessClient.checkCountingEnded(diagCtrlIds, "XdaqLBoxAccess", instance)) {
        cout << "Checking if all have finished" << endl;
        //Thread.sleep(1000);
    }
    cout << "All diagnostics have finished" << endl;
    diagAccessClient.stopDiagCtrl(diagCtrlIds, "XdaqLBoxAccess", instance);

	int j=0;
    for (Histos::iterator iHisto = histos.begin(); iHisto != histos.end(); ++iHisto) {
        cout << "Data " << iHisto->bag.toString() << endl;
        xdata::Vector<xdata::Binary>* data = diagAccessClient.readDataHistoMgr(
                iHisto->bag.getIdBag(), "XdaqLBoxAccess", instance);
        for (int bin = 0, k=0; bin < iHisto->bag.getBinCount(); bin++, k++) {
            cout << dec << ((unsigned long)data->at(bin)) << ' ' ;
            rate[j][k] = ((unsigned long)data->at(bin));
        }
        cout << endl;
    }
}

void XdaqRpcMonitor::saveHistogram(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception) {

    *out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << endl;
    *out << cgicc::html().set("lang", "en").set("dir","ltr") << endl;
    *out << cgicc::title("Send SOAP Message") << endl;
     
    xgi::Utils::getPageHeader(
            out, 
            "RPC Monitor",
            getApplicationDescriptor()->getContextDescriptor()->getURL(),
            getApplicationDescriptor()->getURN(),
            "/daq/xgi/images/Application.jpg"
            );

    string appUrl1 = "/" + getApplicationDescriptor()->getURN();

	*out << cgicc::style().set("type", "text/css") << endl;
	*out << cgicc::comment() << endl;
	*out << ".style1 {font-family:Arial, Helvetica, sans-serif;" << endl;
	*out << 	"font-style: italic;" << endl;
	*out << 	"font-weight: bold;" << endl;
	*out << 	"font-size: 32px;" << endl;
	*out << 	"color: #000099;}" << endl;
	*out << ".style2 {font-size: 18px; font-style: normal; color: #000000}" << endl;
	*out << ".style3 {font-family:Arial, Helvetica, sans-serif;" << endl;
	*out << 	"font-weight: bold;" << endl;
	*out << 	"font-size: 16px;}" << endl;
	*out << cgicc::comment() << endl;
	*out << cgicc::style() << endl;
	if(histoData.empty() || histoData.empty()){
		*out << cgicc::p("No Data to save.  Perhaps it was save previously.").set("class", "style1") << cgicc::p() << endl;
		*out << cgicc::p("After saving, the data is cleared.").set("class", "style1") << cgicc::p() << endl;
	}
	else{
		for(std::map<int, int>::iterator iData = histoData.begin(); iData != histoData.end(); ++iData){
			try {
				LOG4CPLUS_INFO(getApplicationLogger(), "Saving strip id " << iData->first << " = " << iData->second << endl);
				xdata::Integer response = dbServiceClient.setStripResponseValues(iData->first, iData->second, 0, unixTimeStripResponse);
			}
		    catch (xdaq::exception::Exception& e) {
		        XCEPT_RETHROW(xgi::exception::Exception, "Cannot send message", e);                                                 
		    }     
		    catch (exception& e) {
		        XCEPT_RAISE(xgi::exception::Exception, string("Trouble saving histogram ") + e.what());                                                 
		    }             
		    catch (...) {
		        XCEPT_RAISE(xgi::exception::Exception, "Cannot send message - unknown reason");                                                 
		    }                                                                                                                            
		}
		histoData.clear();
		unixTimeStripResponse = 0;
		*out << cgicc::p("Data has been saved.").set("class", "style1") << cgicc::p() << endl;
	}
	*out << cgicc::p() << endl;

	makeMenu(in, out);

    xgi::Utils::getPageFooter(*out);
   
}

void XdaqRpcMonitor::test(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception)
{
    *out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << endl;
    *out << cgicc::html().set("lang", "en").set("dir","ltr") << endl;
    *out << cgicc::title("Send SOAP Message") << endl;

    xgi::Utils::getPageHeader(
            out,
            "RPC Monitor",
            getApplicationDescriptor()->getContextDescriptor()->getURL(),
            getApplicationDescriptor()->getURN(),
            "/daq/xgi/images/Application.jpg"
            );

    typedef XdaqDiagAccessClient::DiagIdVector DiagIds;
    typedef XdaqDiagAccessClient::HistoMgrInfoVector Histos;
    typedef XdaqDiagAccessClient::PulserInfoVector Pulsers;

    DiagIds* diagCtrls = diagAccessClient.getDiagCtrlList("XdaqLBoxAccess", 0);
    for (DiagIds::iterator iDiagId = diagCtrls->begin(); iDiagId != diagCtrls->end(); ++iDiagId) {
        cout << "DiagId " << iDiagId->bag.toString()
            << "State :" << diagAccessClient.getDiagCtrlState(*iDiagId, "XdaqLBoxAccess", 0) << endl;
    }
    
    Histos* histos = diagAccessClient.getHistoMgrList("XdaqLBoxAccess", 0);
    for (Histos::iterator iHisto = histos->begin(); iHisto != histos->end(); ++iHisto) {
        cout << "HistoMgr " << iHisto->bag.toString() << endl;
        HistoMgrInfo& histoInfo = iHisto->bag;
        xdata::Integer bin = histoInfo.getBinCount();
        cout << "Bin Count " << bin.toString() << endl;
    }

    Pulsers* pulsers = diagAccessClient.getPulserList("XdaqLBoxAccess", 0);
    for (Pulsers::iterator iPulser = pulsers->begin(); iPulser != pulsers->end(); ++iPulser) {
        cout << "Pulser " << iPulser->bag.toString() << endl;
        PulserInfo& pulserInfo = iPulser->bag;
        xdata::Integer maxLength = pulserInfo.getMaxLength();
        cout << "Pulser Max Length " << maxLength.toString() << endl;
        xdata::Integer width = pulserInfo.getWidth();
        cout << "Pulser Width " << width.toString() << endl;
        DiagIdBag diagId = pulserInfo.getIdBag();
        cout << "DiagId " << diagId.bag.toString() << endl;
        DiagIdBag diagCrtId = pulserInfo.getDiagCtrlId();
        cout << "DiagCrtId " << diagCrtId.bag.toString() << endl;
    }
                
    cout << "Reseting diag ctrls" << endl;
    diagAccessClient.resetDiagCtrl(*diagCtrls, "XdaqLBoxAccess", 0);
    diagAccessClient.resetHistoMgr(*histos, "XdaqLBoxAccess", 0);
    
    xdata::Vector<xdata::Binary> pulserData;
    xdata::Binary pulse;
    for (int i = 0; i < 128; i++) {
        pulse = (i % 8 == 0) ? 0xffffff : 0;
        pulserData.push_back(pulse);
        //System.out.println(pulserData[i].toString());
    }
    cout << "Configuring pulsers" << endl;
    xdata::Binary pulseLength = pulserData.size();
    diagAccessClient.configurePulser(*pulsers, pulserData, pulseLength, false, "XdaqLBoxAccess", 0);

    cout << "Configuring diag ctrls" << endl;
    xdata::Binary counterLimit(40000000ul * 10);
    diagAccessClient.configureDiagCtrl(*diagCtrls, counterLimit, IDiagCtrl::ttManual, "XdaqLBoxAccess", 0);    

    cout << "Starting diag ctrls" << endl;
    diagAccessClient.startDiagCtrl(*diagCtrls, "XdaqLBoxAccess", 0);
    while (!diagAccessClient.checkCountingEnded(*diagCtrls, "XdaqLBoxAccess", 0)) {
        cout << "Checking if all have finished" << endl;
        //Thread.sleep(1000);
    }
    cout << "All diagnostics have finished" << endl;
    diagAccessClient.stopDiagCtrl(*diagCtrls, "XdaqLBoxAccess", 0);
    
    for (Histos::iterator iHisto = histos->begin(); iHisto != histos->end(); ++iHisto) {
        cout << "Data " << iHisto->bag.toString() << endl;
        *out << "Data " << iHisto->bag.toString() << endl;
        xdata::Vector<xdata::Binary>* data = diagAccessClient.readDataHistoMgr(
        		iHisto->bag.getIdBag(), "XdaqLBoxAccess", 0);
        for (int bin = 0; bin < iHisto->bag.getBinCount(); bin++) {
            cout << dec << ((unsigned long)data->at(bin)) << ' ';
            *out << dec << ((unsigned long)data->at(bin)) << ' ';
        }
        cout << endl;
        *out << cgicc::br();
    }
    xgi::Utils::getPageFooter(*out);
}
