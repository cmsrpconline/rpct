#ifndef _BINARY_H_
#define _BINARY_H_

#include "xdata/exception/Exception.h"
#include "xdata/Serializable.h"
#include <stdint.h>
#include <cstring>

namespace xdata {
    
    
class Binary : public xdata::Serializable {
public: 
    Binary(unsigned char* value, size_t size);
    Binary(uint32_t value);
    Binary();
    Binary(const Binary& b);
                        
    virtual ~Binary();    
    
    virtual std::string type() const;            
        
    void clear();
    operator uint32_t();
    operator uint64_t();
            
    //! Assignment operator
    Binary& operator=(const Binary& value);
    Binary& operator=(const uint32_t& value);
    
    void setValue(const xdata::Serializable & s) throw (xdata::exception::Exception);

    // Comparison operators for simple type

    int operator==(const Binary & b) const;
            
    int operator!=(const Binary & b);
    
    /*int operator<(const Binary & b);
    
    int operator<=(const Binary & b);
    
    int operator>(const SimpleType<T> & b);
    
    int operator>=(const SimpleType<T> & b);*/
    

    // Comparison operators for primitive value types

    /*int operator==(const T & b);
    
    int operator!=(const T & b);
    
    int operator!();
    
    int operator<(const T & b);
    
    int operator<=(const T & b);
    
    int operator>(const T & b);
    
    int operator>=(const T & b);*/
            
    // Mathematical operators for primitive value types

   /* //! Prefix ++
    T& operator++();
    
    //! Prefix --
    T& operator--();

    //! Postfix ++
    T operator++(int);

    //! Postfix --
    T operator--(int);*/
    
    // Comparison operators inherited from Serializable
    int equals(const xdata::Serializable & s) const;
    
    virtual std::string toString () const throw (xdata::exception::Exception) ;

    virtual void  fromString (const std::string &value) throw (xdata::exception::Exception);
              
    virtual size_t size() {
        return size_;
    }  
    
    virtual unsigned char* buffer() {
        return value_;
    }
    void copy(void* dest);
private:
    unsigned char* value_;
    size_t size_;
    mutable std::string string_;
};
    
}

#endif /*_BINARY_H_*/
