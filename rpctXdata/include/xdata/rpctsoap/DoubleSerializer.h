// $Id: DoubleSerializer.h,v 1.1 2008/04/30 14:46:20 tb Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xdata_rpctsoap_DoubleSerializer_h_
#define _xdata_rpctsoap_DoubleSerializer_h_

#include "xdata/rpctsoap/ObjectSerializer.h"
#include "xdata/Double.h"

namespace xdata {
namespace rpctsoap {

class DoubleSerializer : public xdata::rpctsoap::ObjectSerializer  
{	
	public:

	std::string type() const;
	DOMElement *  exportAll(xdata::Serializer * serializer,  xdata::Serializable * serializable, DOMElement * targetNode);
	void exportQualified (xdata::Serializer * serializer,  xdata::Serializable * serializable,
	DOMNode* queryNode, DOMElement* resultNode) throw (xdata::exception::Exception);
	void import (xdata::Serializer * serializer,  xdata::Serializable * serializable, DOMNode* targetNode) throw (xdata::exception::Exception);
    
    virtual std::string xmlType() {
        return "xsd:double";
    }
};


}}

#endif
