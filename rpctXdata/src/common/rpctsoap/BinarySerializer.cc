// $Id: BinarySerializer.cc,v 1.1 2008/04/30 14:46:20 tb Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/rpctsoap/BinarySerializer.h"
#include "xdata/rpctsoap/NamespaceURI.h"
#include "xdata/XStr.h"

using namespace std;

std::string xdata::rpctsoap::BinarySerializer::type() const
{
	return "binary";		
}


DOMElement* xdata::rpctsoap::BinarySerializer::exportAll(xdata::Serializer * serializer,  xdata::Serializable * serializable, DOMElement * targetNode)
{
	DOMDocument* d = targetNode->getOwnerDocument();
	targetNode->setAttributeNS(XStr(XSI_NAMESPACE_URI), XStr("xsi:type"), XStr( xmlType().c_str() ) );
	xdata::Binary * variable = dynamic_cast<xdata::Binary*>(serializable);
	DOMText* t = d->createTextNode( XStr(variable->toString()) );
	targetNode->appendChild (t);
	return 0;
}



void  xdata::rpctsoap::BinarySerializer::exportQualified (xdata::Serializer * serializer,  xdata::Serializable * serializable, DOMNode* queryNode, DOMElement * resultNode) throw (xdata::exception::Exception)
{
	std::string type = XMLCh2String( ((DOMElement*) queryNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));
	XCEPT_ASSERT (type != "", xdata::exception::Exception,  XMLCh2String(queryNode->getNodeName())+ ": Missing type attribute or type namespace wrong");

	if (type == xmlType() )
	{
		DOMDocument* d = resultNode->getOwnerDocument();
		resultNode->setAttribute ( XStr("xsi:type"), XStr( xmlType().c_str() ) );
		xdata::Binary * variable = dynamic_cast<xdata::Binary*>(serializable);
		DOMText* t = d->createTextNode( XStr(variable->toString()) );
		resultNode->appendChild (t);
	} 
	else 
        {
		std::string msg = "Parameter import mismatch for tag ";
		msg += XMLCh2String (queryNode->getNodeName());
		msg +=", expected type ";
        msg += xmlType();
        msg += ", received type ";
		msg += type;
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}	
}


void  xdata::rpctsoap::BinarySerializer::import (xdata::Serializer * serializer,  xdata::Serializable * serializable, DOMNode* targetNode) throw (xdata::exception::Exception)
{
	std::string type = XMLCh2String( ((DOMElement*) targetNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));	
	XCEPT_ASSERT (type != "", xdata::exception::Exception,  XMLCh2String(targetNode->getNodeName())+ ": Missing type attribute or type namespace wrong");

	// Check if tag is called <Parameter> and node type is ELEMENT node
	//
	if ( ((type != xmlType()) && (type != "soapenc:hexBinary"))
        || (targetNode->getNodeType() != DOMNode::ELEMENT_NODE) )
	{
		string msg = "Wrong node type or tag name ";
		msg += XMLCh2String(targetNode->getNodeName());
        msg +=", expected type ";
        msg += xmlType();
        msg += ", received type ";
		msg += type;
		XCEPT_RAISE (xdata::exception::Exception, msg); 
	}
		
	xdata::Binary * variable = dynamic_cast<xdata::Binary*>(serializable);
		
	if (!targetNode->hasChildNodes())
	{
		// ERROR don't allow setting with empty tag
		std::string msg = "Missing binary value for imported tag name ";
		msg += XMLCh2String(targetNode->getNodeName());
                XCEPT_RAISE (xdata::exception::Exception, msg);
	} else 
        {					
		std::string tmp = XMLCh2String ( targetNode->getFirstChild()->getNodeValue() );
		variable->fromString(tmp);
	}
}
