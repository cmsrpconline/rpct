// $Id: InfoSpaceSerializer.cc,v 1.1 2008/04/30 14:46:20 tb Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/rpctsoap/InfoSpaceSerializer.h"
#include "xdata/rpctsoap/Serializer.h"
#include "xdata/rpctsoap/NamespaceURI.h"
#include "xdata/XStr.h"

std::string xdata::rpctsoap::InfoSpaceSerializer::type() const
{
	return "infospace";
}


DOMElement *  xdata::rpctsoap::InfoSpaceSerializer::exportAll(xdata::Serializer * serializer,  xdata::Serializable * serializable, DOMElement * targetNode)
{
	DOMDocument* d = targetNode->getOwnerDocument();
	targetNode->setAttributeNS(XStr(XSI_NAMESPACE_URI), XStr("xsi:type"), XStr( "soapenc:Struct" ) );	

	std::map< std::string, xdata::Serializable*, std::less<std::string> > * b = dynamic_cast<std::map< std::string, xdata::Serializable*, std::less<std::string> >* >(serializable);
	std::map< std::string, xdata::Serializable*, std::less<std::string> >::iterator i;
	for ( i = b->begin(); i != b->end(); i++)
	{
		// fire listener before export
		dynamic_cast<xdata::InfoSpace*>(serializable)->fireItemValueRetrieve((*i).first, this );

		xdata::Serializable* var = (*i).second;		
		DOMElement* e = d->createElementNS ( targetNode->getNamespaceURI(), XStr( (*i).first ) );		
		e->setPrefix(targetNode->getPrefix());
		targetNode->appendChild(e);
		//DOMElement * child = dynamic_cast<xdata::rpctsoap::Serializer*>(serializer)->exportAll(var, e);
		dynamic_cast<xdata::rpctsoap::Serializer*>(serializer)->exportAll(var, e);
	}

	return 0;	
}



void  xdata::rpctsoap::InfoSpaceSerializer::exportQualified (xdata::Serializer * serializer,  xdata::Serializable * serializable, DOMNode* queryNode, DOMElement * resultNode) throw (xdata::exception::Exception)
{				
	std::string type = XMLCh2String( ((DOMElement*) queryNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));
	XCEPT_ASSERT (type != "", xdata::exception::Exception,  XMLCh2String(queryNode->getNodeName())+ ": Missing type attribute or type namespace wrong");

	if (type != "soapenc:Struct")
	{
		std::string msg = "Export type mismatch for tag ";
		msg += XMLCh2String(queryNode->getNodeName());
		msg += ", expected type is soapenc:Struct, received type ";		
		msg += type;
		XCEPT_RAISE (xdata::exception::Exception, msg);	
	}
	
	DOMDocument* d = resultNode->getOwnerDocument();
	resultNode->setAttribute ( XStr("xsi:type"), XStr( "soapenc:Struct" ) );	

	DOMNodeList* children = queryNode->getChildNodes();
	for (unsigned int i = 0; i < children->getLength(); i++)
	{
		DOMNode* current = children->item(i);

		if (current->getNodeType() == DOMNode::ELEMENT_NODE)
		{
			std::string currentName = XMLCh2String(current->getLocalName());
			std::map< std::string, xdata::Serializable*, std::less<std::string> > * b = dynamic_cast<std::map< std::string, xdata::Serializable*, std::less<std::string> >* >(serializable);
			std::map< std::string, xdata::Serializable*, std::less<std::string> >::iterator i;
			i = b->find(currentName);  
			if ( i != b->end() )
			{
				// fire listener before export
				dynamic_cast<xdata::InfoSpace*>(serializable)->fireItemValueRetrieve(currentName, this );
				
				DOMElement* e = d->createElementNS ( resultNode->getNamespaceURI(), XStr( currentName ) );		
				e->setPrefix(resultNode->getPrefix());
				resultNode->appendChild(e);				
				
				dynamic_cast<xdata::rpctsoap::Serializer*>(serializer)->exportQualified((*i).second, current, e);

			} 
			else 
			{
				std::string msg = "Bag member ";
				msg += currentName;
				msg += " not exported in tag name ";
				msg += XMLCh2String (queryNode->getNodeName());
				XCEPT_RAISE (xdata::exception::Exception, msg);
			}
		}
	}	
}


void  xdata::rpctsoap::InfoSpaceSerializer::import (xdata::Serializer * serializer,  xdata::Serializable * serializable, DOMNode* targetNode) throw (xdata::exception::Exception)
{
	std::string type = XMLCh2String( ((DOMElement*) targetNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));
	XCEPT_ASSERT (type != "", xdata::exception::Exception,  XMLCh2String(targetNode->getNodeName())+ ": Missing type attribute or type namespace wrong");

	if (type != "soapenc:Struct")
	{
		std::string msg = "Import type mismatch in imported tag name ";
		msg += XMLCh2String (targetNode->getNodeName());
		msg += ", expected type soapenc:Struct, received type ";
		msg += type;
		XCEPT_RAISE (xdata::exception::Exception, msg);	
	}

	DOMNodeList* children = targetNode->getChildNodes();
	for (unsigned int i = 0; i < children->getLength(); i++)
	{
		DOMNode* current = children->item(i);

		if (current->getNodeType() == DOMNode::ELEMENT_NODE)
		{	
			std::string currentName = XMLCh2String(current->getLocalName());
			std::map< std::string, xdata::Serializable*, std::less<std::string> > * b = dynamic_cast<std::map< std::string, xdata::Serializable*, std::less<std::string> >* >(serializable);
			std::map< std::string, xdata::Serializable*, std::less<std::string> >::iterator i;
			i = b->find(currentName);
			if ( i != b->end() )
			{
				dynamic_cast<xdata::rpctsoap::Serializer*>(serializer)->import((*i).second, current);
				// Fire listener
				dynamic_cast<xdata::InfoSpace*>(serializable)->fireItemValueChanged(currentName, this );

			} else 
                        {
				std::string msg = "Infospace member ";
				msg += currentName;
				msg += " not found in imported tag name ";
				msg += XMLCh2String (targetNode->getNodeName());
				XCEPT_RAISE (xdata::exception::Exception, msg);
			}
		}
	}	
}
