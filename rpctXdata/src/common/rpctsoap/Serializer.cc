// $Id: Serializer.cc,v 1.1 2008/04/30 14:46:20 tb Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/


#include "xdata/rpctsoap/Serializer.h"  
#include "xdata/rpctsoap/VectorSerializer.h"  
#include "xdata/rpctsoap/IntegerSerializer.h"  
#include "xdata/rpctsoap/FloatSerializer.h" 
#include "xdata/rpctsoap/BooleanSerializer.h" 
#include "xdata/rpctsoap/StringSerializer.h"  
#include "xdata/rpctsoap/BagSerializer.h"  
#include "xdata/rpctsoap/UnsignedShortSerializer.h"  
#include "xdata/rpctsoap/UnsignedLongSerializer.h"  
#include "xdata/rpctsoap/DoubleSerializer.h"  
#include "xdata/rpctsoap/PropertiesSerializer.h"  
#include "xdata/rpctsoap/InfoSpaceSerializer.h"  
#include "xdata/rpctsoap/BinarySerializer.h"  

#include "xdata/rpctsoap/NamespaceURI.h"  

//
// Serializer * soapSerializer = new xdata::soap::Serializer();
// xdata::Vector<xdata::Integer> myVector; 
// Node * myNode = soapSerializer.serialize(myVector,node);
//
xdata::rpctsoap::Serializer::Serializer()
{
	// grabage collection is perfromed automatically at destruction time
	this->addObjectSerializer(new xdata::rpctsoap::VectorSerializer());
	this->addObjectSerializer(new xdata::rpctsoap::IntegerSerializer());
	this->addObjectSerializer(new xdata::rpctsoap::UnsignedShortSerializer());
	this->addObjectSerializer(new xdata::rpctsoap::UnsignedLongSerializer());
	this->addObjectSerializer(new xdata::rpctsoap::FloatSerializer());
	this->addObjectSerializer(new xdata::rpctsoap::DoubleSerializer());
	this->addObjectSerializer(new xdata::rpctsoap::BooleanSerializer());
	this->addObjectSerializer(new xdata::rpctsoap::StringSerializer());
	this->addObjectSerializer(new xdata::rpctsoap::BagSerializer());
	this->addObjectSerializer(new xdata::rpctsoap::PropertiesSerializer());
	this->addObjectSerializer(new xdata::rpctsoap::InfoSpaceSerializer());
    this->addObjectSerializer(new xdata::rpctsoap::BinarySerializer());
}	
	
DOMElement * xdata::rpctsoap::Serializer::exportAll(xdata::Serializable * s, DOMElement * targetNode, bool createNS) throw (xdata::exception::Exception)
{
	// Assume that the namespace of the given targetNode has been set outside.
	// Resude the namespace of this node 
    
	if (XMLCh2String(targetNode->getNamespaceURI()) == "")
	{
		XCEPT_RAISE (xdata::exception::Exception, "Missing namespace declaration in soap serializer export all");
	}

	if (createNS == true)
	{
		// Add the namespaces needed for SOAP encoding	
		targetNode->setAttribute(xdata::XStr("xmlns:soapenc"), xdata::XStr(SOAPENC_NAMESPACE_URI)); 
		targetNode->setAttribute(xdata::XStr("xmlns:xsi"), xdata::XStr(XSI_NAMESPACE_URI)); 
	}
	
	xdata::rpctsoap::ObjectSerializer * os = dynamic_cast<xdata::rpctsoap::ObjectSerializer*>(this->getObjectSerializer(s->type()));
    
    if (os == NULL) {
        XCEPT_RAISE (xdata::exception::Exception, "Could not find serializer for type " + s->type());
    }
	return os->exportAll(this, s, targetNode);
}
		
void xdata::rpctsoap::Serializer::exportQualified (xdata::Serializable * s, DOMNode* queryNode, DOMElement* resultNode, bool createNS) throw (xdata::exception::Exception) 
{
	if (XMLCh2String(resultNode->getNamespaceURI()) == "")
	{
		XCEPT_RAISE (xdata::exception::Exception, "Missing namespace declaration in soap serializer export qualified");
	}
	
	if (createNS == true)
	{
		// Add the namespaces needed for SOAP encoding	
		resultNode->setAttribute (xdata::XStr("xmlns:soapenc"),xdata::XStr(SOAPENC_NAMESPACE_URI)); 
		resultNode->setAttribute (xdata::XStr("xmlns:xsi"),xdata::XStr(XSI_NAMESPACE_URI)); 
	}
	
	xdata::rpctsoap::ObjectSerializer * os = dynamic_cast<xdata::rpctsoap::ObjectSerializer*>(this->getObjectSerializer(s->type()));	
	return os->exportQualified(this, s, queryNode, resultNode);
}

void xdata::rpctsoap::Serializer::import (xdata::Serializable * s, DOMNode* targetNode) throw (xdata::exception::Exception)
{
	/*if (XMLCh2String(targetNode->getNamespaceURI()) == "")
	{
		XCEPT_RAISE (xdata::exception::Exception, "Missing namespace declaration in soap serializer import");
	}*/
	
	xdata::rpctsoap::ObjectSerializer * os = dynamic_cast<xdata::rpctsoap::ObjectSerializer*>(this->getObjectSerializer(s->type()));
	return os->import(this, s, targetNode);
}
