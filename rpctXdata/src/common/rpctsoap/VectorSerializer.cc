// $Id: VectorSerializer.cc,v 1.1 2008/04/30 14:46:20 tb Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/rpctsoap/VectorSerializer.h"
#include "xdata/rpctsoap/NamespaceURI.h"
#include "xdata/XStr.h"
#include "xdata/Integer.h"
#include "xdata/UnsignedShort.h"
#include "xdata/UnsignedLong.h"
#include "xdata/Float.h"
#include "xdata/Double.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "xdata/Binary.h"
#include "xdata/AbstractBag.h"
#include <sstream>


using namespace std;

std::string xdata::rpctsoap::VectorSerializer::type() const
{
	return "vector";		
}

std::string xdata::rpctsoap::VectorSerializer::getXmlTypeForXdataType(std::string type)
{    
    Serializer serializer;
    return dynamic_cast<ObjectSerializer*>(serializer.getObjectSerializer(type))->xmlType();
}
    
DOMElement*  xdata::rpctsoap::VectorSerializer::exportAll(xdata::Serializer * serializer, xdata::Serializable * serializable, DOMElement * targetNode)
{
	AbstractVector * v = dynamic_cast<AbstractVector*>(serializable);
	DOMDocument* d = targetNode->getOwnerDocument();

	// Array and type sepcification
	//targetNode->setAttribute ( XStr("xsi:type"), XStr( "soapenc:Array" ) );	
    targetNode->setAttributeNS(XStr(XSI_NAMESPACE_URI), XStr("xsi:type"), XStr("soapenc:Array"));
    
	
	// SOAP 1.1
	// Us the generic ur-type as the array type: Version 1.1: soapenc:ur-type[<size>]
	// --
	// SOAP 1.2
	// For version 1.2 there are two attributes: ENC:arraySize="<size>" ENC:itemType="xsd:<type>"
	//
	std::stringstream arrayType;
    arrayType << getXmlTypeForXdataType(v->getElementType());
    /*if (v->elements() > 0) {
        arrayType << getXmlTypeForXdataType(v->elementAt(0)->type());                
    }
    else {
        // try to guess array element type from the type id
        if (dynamic_cast<xdata::Vector<xdata::Integer>*>(v) != 0) {
            arrayType << getXmlTypeForXdataType("int");
        }
        else if (dynamic_cast<xdata::Vector<xdata::UnsignedShort>*>(v) != 0) {
            arrayType << getXmlTypeForXdataType("unsigned short");
        }
        else if (dynamic_cast<xdata::Vector<xdata::UnsignedLong>*>(v) != 0) {
            arrayType << getXmlTypeForXdataType("unsigned long");
        }
        else if (dynamic_cast<xdata::Vector<xdata::Float>*>(v) != 0) {
            arrayType << getXmlTypeForXdataType("float");
        }
        else if (dynamic_cast<xdata::Vector<xdata::Double>*>(v) != 0) {
            arrayType << getXmlTypeForXdataType("double");
        }
        else if (dynamic_cast<xdata::Vector<xdata::Boolean>*>(v) != 0) {
            arrayType << getXmlTypeForXdataType("bool");
        }
        else if (dynamic_cast<xdata::Vector<xdata::String>*>(v) != 0) {
            arrayType << getXmlTypeForXdataType("string");
        }
        else if (dynamic_cast<xdata::Vector<xdata::Binary>*>(v) != 0) {
            arrayType << getXmlTypeForXdataType("binary");
        }        
        else {
            arrayType << "xsd:ur-type";
        }
        
        //arrayType << "xsd:ur-type[" << v->elements() << "]";
    }*/
    
    arrayType << "[" << v->elements() << "]";
    
	//targetNode->setAttribute ( XStr("soapenc:arrayType"), XStr( arrayType.str() ) );
    targetNode->setAttributeNS(XStr(SOAPENC_NAMESPACE_URI), XStr("soapenc:arrayType"), XStr(arrayType.str()));

	for (unsigned int i = 0; i < v->elements(); i++)
	{
		DOMElement* e = d->createElementNS (targetNode->getNamespaceURI(), XStr("item"));
		e->setPrefix(targetNode->getPrefix());
		std::stringstream position;
		position << '[' << i << ']';
		//e->setAttribute ( XStr("soapenc:position"), XStr(position.str()));	
        e->setAttributeNS(XStr(SOAPENC_NAMESPACE_URI), XStr("soapenc:position"), XStr(position.str()));
		targetNode->appendChild(e);		
		//DOMElement * child =  dynamic_cast<xdata::rpctsoap::Serializer*>(serializer)->exportAll(v->elementAt(i), e);
		dynamic_cast<xdata::rpctsoap::Serializer*>(serializer)->exportAll(v->elementAt(i), e);
	}
	return 0;
}



void xdata::rpctsoap::VectorSerializer::exportQualified (xdata::Serializer * serializer,  Serializable * serializable,
 DOMNode* queryNode, DOMElement* resultNode) throw (xdata::exception::Exception)
{
	DOMDocument* d = resultNode->getOwnerDocument();
	AbstractVector * v = dynamic_cast<AbstractVector*>(serializable);

	std::string type = XMLCh2String( ((DOMElement*) queryNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));
	XCEPT_ASSERT (type != "", xdata::exception::Exception,  XMLCh2String(queryNode->getNodeName())+ ": Missing type attribute or type namespace wrong");

	
	if (type != "soapenc:Array")
	{
		string msg = "Array type import mismatch in tag name ";
		msg += XMLCh2String(queryNode->getNodeName());
		msg += ", expected type soapenc:Array, received type ";
		msg += type;
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}
	
	// arrayType string is in format: soapenc:ArrayType="xsd:something[size]"
	std::string arrayType = XMLCh2String( ((DOMElement*) queryNode)->getAttributeNS (XStr(SOAPENC_NAMESPACE_URI), XStr("arrayType")));
	
	if (arrayType == "")
	{
		std::string msg = "Missing soapenc:arrayType specifier in SOAP input stream for tag ";
		msg += XMLCh2String (queryNode->getNodeName());
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}
	
	
	if (arrayType.find ("xsd:ur-type[") == std::string::npos)
	{
		std::string msg = "Could not find ur-type specifier in ArrayType attribute of tag ";
		msg += XMLCh2String (queryNode->getNodeName());
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}

	resultNode->setAttribute ( XStr("xsi:type"), XStr( "soapenc:Array" ) );	
		
	std::string sizeStr = arrayType.substr(arrayType.find("[")+1, arrayType.find("]")-1);
	std::stringstream sizeStream(sizeStr);
	int size = 0;
	sizeStream >> size;
	
	if (size != v->elements())
	{
		// if size different than local vector size, re-export the whole vector
		this->exportAll(serializer,  serializable, resultNode);
	}
	else
	{
		// Export elements indicated by position attribute
		//
		DOMNodeList* children = queryNode->getChildNodes();
		for (unsigned int i = 0; i < children->getLength(); i++)
		{
			DOMNode* current = children->item(i);
			// index is indicated as soapenc:position="[<number>]"
			string currentIndex = XMLCh2String( ((DOMElement*) current)->getAttributeNS (XStr(SOAPENC_NAMESPACE_URI), XStr("position")));
			unsigned int index = 0;
			std::string indexStr = currentIndex.substr(currentIndex.find("[")+1, currentIndex.find("]")-1);
			std::stringstream indexStream(indexStr);
			indexStream >> index;

			if (index < v->elements())
			{
				DOMElement* e = d->createElementNS (resultNode->getNamespaceURI(), XStr("item"));
				e->setPrefix(resultNode->getPrefix());
				e->setAttribute ( XStr("soapenc:position"), XStr(currentIndex));
				resultNode->appendChild(e);
				dynamic_cast<xdata::rpctsoap::Serializer*>(serializer)->exportQualified(v->elementAt(index), current, e);
			} 
			else
			{
				string msg = "Vector element position out of bounds during the qualified export of tag ";
				msg +=  XMLCh2String (current->getNodeName());
				msg += ", position: ";
				msg += currentIndex;
				XCEPT_RAISE (xdata::exception::Exception, msg);
			}
		}	
	}		
}


void xdata::rpctsoap::VectorSerializer::import (xdata::Serializer * serializer,  Serializable * serializable, DOMNode* targetNode) throw (xdata::exception::Exception)
{
	AbstractVector * v = dynamic_cast<AbstractVector*>(serializable);

	std::string type = XMLCh2String( ((DOMElement*) targetNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));
	XCEPT_ASSERT (type != "", xdata::exception::Exception,  XMLCh2String(targetNode->getNodeName())+ ": Missing type attribute or type namespace wrong");	    
    
	if (type != "soapenc:Array")
	{
		string msg = "Array import mismatch, expected type soapenc:Array, received type ";
		msg += type;
		msg += " for tag ";
		msg += XMLCh2String (targetNode->getNodeName());
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}
	
	// arrayType string is in format: soapenc:ArrayType="xsd:something[size]"
	std::string arrayType = XMLCh2String( ((DOMElement*) targetNode)->getAttributeNS (XStr(SOAPENC_NAMESPACE_URI), XStr("arrayType")));
	
	if (arrayType == "")
	{
		std::string msg = "Missing soapenc:arrayType specifier in SOAP input stream for tag ";
		msg += XMLCh2String (targetNode->getNodeName());
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}	
	
	/*if (arrayType.find ("xsd:ur-type[") == std::string::npos)
	{
		std::string msg = "Could not find ur-type specifier in arrayType attribute of tag ";
		msg += XMLCh2String (targetNode->getNodeName());
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}*/
	
	std::string sizeStr = arrayType.substr(arrayType.find("[")+1, arrayType.find("]")-1);
	std::stringstream sizeStream(sizeStr);
	int size = 0;
	sizeStream >> size;

	DOMNodeList* l = targetNode->getChildNodes();

	// Resize vector according to received size specification
	v->setSize( size );

	// Import all children
    int index = -1;
	for (unsigned int i = 0; i < l->getLength(); i++)
	{
		DOMNode* current = l->item(i);

		if (current->getNodeType() == DOMNode::ELEMENT_NODE)
		{					
			//string indexStr = getNodeAttribute (current, "index");
			std::string currentIndex = XMLCh2String( ((DOMElement*) current)->getAttributeNS (XStr(SOAPENC_NAMESPACE_URI), XStr("position")));
			if ( currentIndex == "" )
			{
				/*string msg = "Missing soapenc:position in item in array";
				msg += XMLCh2String (targetNode->getNodeName());
				msg += ", at item ";
				std::stringstream tmpIndex;
				tmpIndex << i;
				msg += tmpIndex.str();
				XCEPT_RAISE (xdata::exception::Exception, msg);*/
                index++;
			} else {          
    			//int index = 0;
    			std::string indexStr = currentIndex.substr(currentIndex.find("[")+1, currentIndex.find("]")-1);
    			std::stringstream indexStream(indexStr);
    			indexStream >> index;
            }

			if (index < v->elements()) 
			{
				dynamic_cast<xdata::rpctsoap::Serializer*>(serializer)->import(v->elementAt(index), current);

			} else 
			{
				// TBD
				// Need to add new values from a
				// type factory
				string msg = "Vector element position out of bounds during the import of tag ";
				msg += XMLCh2String (targetNode->getNodeName());
				msg += ", position: ";
				msg += currentIndex;
				XCEPT_RAISE (xdata::exception::Exception, msg);
			}	
		}
	}
}

