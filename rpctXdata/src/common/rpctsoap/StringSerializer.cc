// $Id: StringSerializer.cc,v 1.1 2008/04/30 14:46:20 tb Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/rpctsoap/StringSerializer.h"
#include "xdata/rpctsoap/NamespaceURI.h"
#include "xdata/XStr.h"

using namespace std;

std::string xdata::rpctsoap::StringSerializer::type() const
{
	return "string";		
}


DOMElement* xdata::rpctsoap::StringSerializer::exportAll(xdata::Serializer * serializer,  xdata::Serializable * serializable, DOMElement * targetNode)
{
	// <Parameter name="" type=""> value </Parameter>
	DOMDocument* d = targetNode->getOwnerDocument();
	targetNode->setAttributeNS(XStr(XSI_NAMESPACE_URI), XStr("xsi:type"), XStr( "xsd:string" ) );
	xdata::String * variable = dynamic_cast<xdata::String*>(serializable);
	std::string value = variable->toString();

	// Create only a text node if the string is not empty.
	if (value != "")
	{
		DOMText* t = d->createTextNode( XStr(value) );
		targetNode->appendChild(t);
	}

	return 0;
}



void xdata::rpctsoap::StringSerializer::exportQualified (xdata::Serializer * serializer,  xdata::Serializable * serializable,
DOMNode* queryNode, DOMElement* resultNode) throw (xdata::exception::Exception)
{
	std::string type = XMLCh2String( ((DOMElement*) queryNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));
	XCEPT_ASSERT (type != "", xdata::exception::Exception,  XMLCh2String(queryNode->getNodeName())+ ": Missing type attribute or type namespace wrong");

	if (type == "xsd:string")
	{
		DOMDocument* d = resultNode->getOwnerDocument();
		resultNode->setAttribute ( XStr("xsi:type"), XStr( "xsd:string" ) );
		xdata::String * variable = dynamic_cast<xdata::String*>(serializable);
		std::string value = variable->toString();

		// Create only a text node if the string is not empty.
		if (value != "")
		{
			DOMText* t = d->createTextNode( XStr(value) );
			resultNode->appendChild(t);
		}
	} 
	else 
        {
		std::string msg = "Type import mismatch for tag name ";
		msg += XMLCh2String (queryNode->getNodeName());
		msg +=", expected type xsd:string, received type ";
		msg += type;
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}	
}


void xdata::rpctsoap::StringSerializer::import (xdata::Serializer * serializer,  xdata::Serializable * serializable, DOMNode* targetNode) throw (xdata::exception::Exception)
{
	std::string type = XMLCh2String( ((DOMElement*) targetNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));	
	XCEPT_ASSERT (type != "", xdata::exception::Exception,  XMLCh2String(targetNode->getNodeName())+ ": Missing type attribute or type namespace wrong");
	
	// Check if tag is called <Parameter> and node type is ELEMENT node
	//
	if ( ((type != "xsd:string") && (type != "soapenc:string"))
        || (targetNode->getNodeType() != DOMNode::ELEMENT_NODE) )
	{
		string msg = "Wrong node type or imported tag name ";
		msg += XMLCh2String(targetNode->getNodeName());
		msg += ", expected type xsd:string received type ";
		msg += type;
		XCEPT_RAISE (xdata::exception::Exception, msg); 
	}
		
	xdata::String * variable = dynamic_cast<xdata::String*>(serializable);
		
	if (targetNode->hasChildNodes())
	{
		std::string tmp = XMLCh2String ( targetNode->getFirstChild()->getNodeValue() );
		variable->fromString(tmp);
	}
}
	
