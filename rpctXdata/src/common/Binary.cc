// $Id: Binary.cc,v 1.2 2009/10/10 15:42:37 tb Exp $

#include "xdata/Binary.h"
#include <algorithm>
#include <sstream>

using namespace std;

namespace xdata {

Binary::Binary(unsigned char* value, size_t size) {
    size_ = size;
    value_ = new unsigned char[size_];
    memcpy(value_, value, size_);
}


Binary::Binary(uint32_t value) {
    size_ = sizeof(uint32_t);
    value_ = new unsigned char[size_];
    memcpy(value_, &value, size_);
}

Binary::Binary() {
    size_ = 0;
    value_ = 0;
}

Binary::Binary(const Binary& b) {
    size_ = b.size_;
    value_ = new unsigned char[size_];
    memcpy(value_, b.value_, size_);    
}
    
                    
Binary::~Binary() {
    delete [] value_;
}

string Binary::type() const {
    return "binary";
}

void Binary::clear() {
    delete [] value_;
    value_ = 0;
    size_ = 0;
    string_.clear();
}


Binary::operator uint32_t()
{ 
    uint32_t result = 0;
    int max = std::min(sizeof(uint32_t), size_);
    for (int i = 0; i < max; i++) {
        uint32_t val = *(value_ + i);
        result |= (val << (i * 8));
    }
        
	return result; 
}

Binary::operator uint64_t()
{ 
    uint64_t result = 0;
    int max = std::min(sizeof(uint64_t), size_);
    for (int i = 0; i < max; i++) {
        uint64_t val = *(value_ + i);
        result |= (val << (i * 8));
    }
        
	return result; 
}

/*
template<class T> T & xdata::SimpleType<T>::operator *() 
{ 
	return *this; 
}
*/

// virtual assignement operator
void Binary::setValue(const xdata::Serializable & s) throw (xdata::exception::Exception)
{	
    /*Binary& src = dynamic_cast<Binary&>(s);
    if (value_ != 0) {
        delete [] value_;
    }
    
    size_ = size;
    value_ = new unsigned char[size_];
    memcpy(value_, value, size_);*/
	*this = dynamic_cast<const Binary&>(s);	
}

//! C++ types compatible Assignment operator
Binary& Binary::operator=(const Binary& value)
{
	//std::cout << "Binary::operator=(const Binary& value)" << std::endl;
    clear();
    size_ = value.size_;
    value_ = new unsigned char[size_];
    memcpy(value_, value.value_, size_);    
	return *this;
}

Binary& Binary::operator=(const uint32_t& value)
{
    //std::cout << "Binary::operator=(onst uint32_t& value)" << std::endl;
    clear();
    size_ = sizeof(uint32_t);
    value_ = new unsigned char[size_];
    memcpy(value_, &value, size_);    
    return *this;
}

// ------------------------------------------------------------------

int Binary::operator==(const Binary& b) const
{
    return (size_ == b.size_) && !memcmp(value_, b.value_, size_);
}

int Binary::operator!=(const Binary& b)
{
	return (value_ != b.value_);
}
/*
template<class T> int xdata::SimpleType<T>::operator<(const SimpleType<T> & b)
{
	return (value_ < b.value_);
}

template<class T> int xdata::SimpleType<T>::operator<=(const SimpleType<T> & b)
{
	return (value_ <= b.value_);
}

template<class T> int xdata::SimpleType<T>::operator>(const SimpleType<T> & b)
{
	return (value_ > b.value_);
}

template<class T> int xdata::SimpleType<T>::operator>=(const SimpleType<T> & b)
{
	return (value_ >= b.value_);
}

// ------------------------------------------------------------------

template<class T> int xdata::SimpleType<T>::operator==(const T & b)
{
	return (value_ == b);
}

template<class T> int xdata::SimpleType<T>::operator!=(const T & b)
{
	return (value_ != b);
}

template<class T> int xdata::SimpleType<T>::operator!()
{
	return (!value_);
}

template<class T> int xdata::SimpleType<T>::operator<(const T & b)
{
	return (value_ < b);
}

template<class T> int xdata::SimpleType<T>::operator<=(const T & b)
{
	return (value_ <= b);
}

template<class T> int xdata::SimpleType<T>::operator>(const T & b)
{
	return (value_ > b);
}

template<class T> int xdata::SimpleType<T>::operator>=(const T & b)
{
	return (value_ >= b);
}
*/
// ------------------------------------------------------------------

int Binary::equals(const xdata::Serializable & s) const
{
	return (*this == dynamic_cast<const Binary&>(s));
}
/*
// ------------------------------------------------------------------
template<class T> T& xdata::SimpleType<T>::operator++()
{
	return (++value_);
}

template<class T> T& xdata::SimpleType<T>::operator--()
{
	return (--value_);
}

template<class T> T xdata::SimpleType<T>::operator++(int)
{
	value_++;
	return value_;
}

template<class T> T xdata::SimpleType<T>::operator--(int)
{
	value_--;
	return value_;
}
*/

void Binary::copy(void* dest) {
    memcpy(dest, value_, size_);
}

string Binary::toString() const throw (xdata::exception::Exception)
{
    if (string_.length() == 0) {
        ostringstream str;
        str.fill('0');
        str << hex << uppercase;
        int b = 0;
        for (int i = (size_ - 1); i >= 0; i--) {
            b = *(value_ + i);
            str.width(2);
            str << b;
        }
        string_ = str.str();
    }
    return string_;
}

void Binary::fromString(const std::string& valueRef) throw (xdata::exception::Exception)
{    
    std::string value = valueRef;
    clear();

    string::size_type firstNotZero = value.find_first_not_of('0');
    if (firstNotZero == string::npos) {
        return;
    }
    
    value = value.substr(firstNotZero);
    
    istringstream str;
    str >> hex >> uppercase;
    if (value.size()%2) {
        value.insert(value.begin(), '0');
    }

    //while (text.size()/2 < size)  {
    //    text.insert(text.begin(), 2, '0');
    //}
    
    size_ = value.size() / 2;
    value_ = new unsigned char[size_];

    unsigned short us;
    for (int i=size_-1; i>=0; i--) {
        // move back the input sequence to the beginning
        str.seekg(0);
        // clear the state flags
        str.clear();

        str.str(value.substr((size_-i-1)*2, 2));
        str >> us;
        ((unsigned char*)value_)[i] = us;
    }
}
    
    
}
