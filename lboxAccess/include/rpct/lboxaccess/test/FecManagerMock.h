#ifndef RPCTFECMANAGERMOCK_H
#define RPCTFECMANAGERMOCK_H

#include <log4cplus/logger.h>
#include "log4cplus/loggingmacros.h"

#include "rpct/lboxaccess/CCUException.h"
#include "rpct/lboxaccess/FecManager.h"
#include "rpct/lboxaccess/StdLinkBoxAccessAdapter.h"
#include "rpct/lboxaccess/cbic_const.h"
#include "rpct/std/IllegalArgumentException.h"

#include <sstream>
#include <istream>
#include <algorithm>
#include <boost/timer.hpp>
#include <map>
#include <time.h>

namespace rpct {

class FecManagerMock : public FecManager {
private:
    static log4cplus::Logger logger;
public:
	static void setupLogger(std::string namePrefix);
    static const uint32_t MAX_BLOCK_SIZE = 64;
    static const uint32_t II_PAGE_MASK = 0x7ff;
    enum FecType { FEC_TYPE_PCI, FEC_TYPE_VME };
private:
    int pciSlot;
    int fec;
    int ring;

    class StdLinkBoxAccessImpl : public StdLinkBoxAccessAdapter {
    private:
        FecManagerMock& fecManager;
        unsigned int ccuAddress;

        static const int PIAChanCount = 4;

        virtual void loadLbc(const std::string& file1, const std::string& file2) {
            LOG4CPLUS_DEBUG(logger, "loadLbc");
        }
        virtual void setCbpcFlashAddr(uint32_t addr) {
            LOG4CPLUS_DEBUG(logger, "setCbpcFlashAddr");
        }
        virtual uint32_t readCbpcFlashAddr(uint32_t addr) {
            LOG4CPLUS_DEBUG(logger, "readCbpcFlashAddr");
            return 0;
        }
        virtual void resetCbpcFlash() {
            LOG4CPLUS_DEBUG(logger, "resetCbpcFlash");
        }
        virtual void setLbcFlashAddr(int lb, uint32_t addr) {
            LOG4CPLUS_DEBUG(logger, "setLbcFlashAddr");
        }
        virtual uint32_t readLbcFlashAddr(int lb, uint32_t addr) {
            LOG4CPLUS_DEBUG(logger, "readLbcFlashAddr");
            return 0;
        }
        virtual uint32_t writeLbcFlashAddr(int lb, uint32_t addr, uint32_t data) {
            LOG4CPLUS_DEBUG(logger, "writeLbcFlashAddr");
            return 0;
        }
        virtual void resetLbcFlash(int lb) {
            LOG4CPLUS_DEBUG(logger, "resetLbcFlash");
        }
        virtual void waitCbpcFlashDone()  {
            LOG4CPLUS_DEBUG(logger, "waitCbpcFlashDone");
        }
        virtual void waitLbcFlashDone(int lb)  {
            LOG4CPLUS_DEBUG(logger, "waitLbcFlashDone");
        }

    public:
        StdLinkBoxAccessImpl(FecManagerMock& fm, unsigned int ccu)
        : fecManager(fm), ccuAddress(ccu) {
            LOG4CPLUS_DEBUG(logger, "Creating StdLinkBoxAccessImpl ccuAddr = " << std::hex << ccu);
        }

        virtual ~StdLinkBoxAccessImpl() {
        }


        virtual void setName(std::string nam) {
        	//name = nam;
        }

        virtual FecManager& getFecManager() {
            return fecManager;
        }

        virtual unsigned int getCcuAddress() {
            return ccuAddress;
        }

        virtual void memoryWrite(uint32_t address, unsigned short data) {
            LOG4CPLUS_DEBUG(logger, std::hex << "w " << address << " " << data);
        }

        virtual unsigned short memoryRead(uint32_t address) {
            LOG4CPLUS_DEBUG(logger, std::hex << "r " << address << " = ");
            return rand();
        }

        virtual void memoryWriteBlock(uint32_t address, unsigned char* data,
                          uint32_t length) {
            LOG4CPLUS_DEBUG(logger, std::hex << "wr address " << address << " length " << length);
        }

        virtual void memoryReadBlock(uint32_t address, unsigned char* data,
                         uint32_t length) {
            LOG4CPLUS_DEBUG(logger,  std::hex << "rb " << address << " " << " length " << "=" << length);

            for (uint32_t i = 0; i < length; i++) {
                data[i] = rand();
                LOG4CPLUS_DEBUG(logger, std::hex << "  [" << i << "]=" << data[i]);
            }
        }

        virtual void memoryRead16Block(uint32_t address, unsigned short* data,
                         uint32_t length) {
            LOG4CPLUS_DEBUG(logger,  std::hex << "rblock " << address << " " << length << "=");

            for (uint32_t i = 0; i < length; i++) {
                data[i] = rand();
                LOG4CPLUS_DEBUG(logger, std::hex << "  [" << i << "]=" << data[i]);
            }
        }

        virtual void memoryWrite16Block(uint32_t address, unsigned short* data,
                         uint32_t length) {
            LOG4CPLUS_DEBUG(logger, std::hex << "wblock address " << address << " length " << length);
        }

        virtual void memoryWrite16(uint32_t address, unsigned short data) {
        }

        virtual unsigned short memoryRead16(uint32_t address) {
            return rand();
        }

        virtual void ccuExternalReset() {
        }
        // obsolete
        virtual void programFlash(const std::string& file) {
        }
        // obsolete
        virtual void programXilinx(const char* file) {
        }
        // obsolete
        virtual void programCbpc(const std::string& file) {
        }
        // obsolete
        virtual void programLbc(const std::string& file) {
        }
        // obsolete
        virtual void programFlash(uint32_t lbcbase, const std::string& file) {
            programFlash(file.c_str());
        }
        // obsolete
        virtual void programXilinx(int lbc, const std::string& file) {
            programXilinx(file.c_str());
        }

        virtual unsigned char piaRead(int channel) {
            return rand();
        }

        virtual void piaWrite(int channel, unsigned char value) {
        }

        virtual void lbWrite(int lb, uint32_t address, uint32_t data) {
        }

        virtual uint32_t lbRead(int lb, uint32_t address) {
            return rand();
        }

        virtual void lbWriteBlock(int lb, uint32_t address,
                          unsigned short* data, uint32_t count) {
        }

        virtual void lbReadBlock(int lb, uint32_t address,
                         unsigned short* data, uint32_t count) {
            for (uint32_t i = 0; i < count; i++) {
                data[i] = rand();
                LOG4CPLUS_DEBUG(logger, std::hex << "  [" << i << "]=" << data[i]);
            }
        }

        virtual void loadCb(const std::string& cbpcBin, const std::string& cbicIni,
                    const std::string& lbcBin, const std::string& lbcIni) {
            LOG4CPLUS_DEBUG(logger, "loadCb");
        }
        virtual void loadKtp(int lb, const std::string& ciiLbBin) {
            LOG4CPLUS_DEBUG(logger, "loadKtp");
        }
        virtual void programFlashCb(const std::string& cbromBin) {
            LOG4CPLUS_DEBUG(logger, "programFlashCbpcLbc");
        }
        virtual uint32_t programFlashKtp(int lb, const std::string& lbromBin) {
            LOG4CPLUS_DEBUG(logger, "programFlashKtp");
            return 0;
        }
        virtual void programConfigToFlashKtp(int lb, std::istream *lbromBin, uint32_t addr) {
            LOG4CPLUS_DEBUG(logger, "programConfigToFlashKtp");
        }
        virtual void loadFromFlash() {
            LOG4CPLUS_DEBUG(logger, "loadFromFlash");
        }

        virtual bool isTTCHardResetEnabled() {
        	return false;
        }
     };
    friend class StdLinkBoxAccessImpl;

    LinkBoxAccessList linkBoxAccessList;

    void init(FecType fecType, int pciSlot, int fec, int ring, bool vmeHardInit=false) {
        if (pciSlot > 3) {
            throw IllegalArgumentException("pciSlot");
        }
        if (fec > 21 || fec < 1) {
            throw IllegalArgumentException("fec");
        }
        if (ring > 7 || ring < 0) {
            throw IllegalArgumentException("ring");
        }

        //this->fecType = ft;
        this->pciSlot = pciSlot;
        this->fec = fec;
        this->ring = ring;
    }

public:

    FecManagerMock() {
        LOG4CPLUS_DEBUG(logger, "entered the constructor");

        int fec, ring;
        unsigned int ccu;
        FecType ft;
        char* env_str;

        env_str = getenv("FEC_ADDRESS");
        if (env_str == NULL) {
            fec = 0x0;
        }
        else {
            sscanf(env_str, "%d", &fec);
        }

        env_str = getenv("RING_ADDRESS");
        if (env_str == NULL) {
            ring = 0x0;
        }
        else {
            sscanf(env_str, "%x", &ring);
        }

        env_str = getenv("CCU_ADDRESS");
        if (env_str == NULL) {
            ccu = 0x1;
        }
        else {
            sscanf(env_str, "%x", &ccu);
        }

        env_str = getenv("FEC_TYPE");
        if (env_str == NULL) {
            throw TException("Environment variable FEC_TYPE not set. Possible values: pci, vme.");
        }
        std::string fecTypeStr = env_str;
        if (fecTypeStr == "pci") {
            ft = FEC_TYPE_PCI;
        }
        else if (fecTypeStr == "vme") {
            ft = FEC_TYPE_VME;
        }
        else {
            throw TException("Invalid environment variable FEC_TYPE: " + fecTypeStr
                    + ". Possible values: pci, vme.");
        }

        init(ft, 0, fec, ring);
        try {
            addLinkBoxAccess(ccu);
        }
        catch(std::exception& e) {
            LOG4CPLUS_WARN(logger, "Exception during initializing: " << e.what()
                << "\nReseting FEC and trying once again.");
            resetFec();
            addLinkBoxAccess(ccu);
        }
    }

    FecManagerMock(int pciSlot, int fec, int ring) {
        init(FEC_TYPE_VME, pciSlot, fec, ring);
    }

    virtual ~FecManagerMock() {
        for_each(linkBoxAccessList.begin(), linkBoxAccessList.end(),
                FDelete<LinkBoxAccessList::value_type>());
    }

    virtual int getPciSlot() {
        return pciSlot;
    }

    virtual int getVmeSlot() {
        return fec;
    }

    virtual int getRingNumber() {
        return ring;
    }

    virtual void resetFec() {
    }

    static const unsigned int GOOD_FEC_STATUS = 0x4c90;
    virtual bool getFecRingSR0(unsigned int &status) {
        status = GOOD_FEC_STATUS;
        return true;
    }

    virtual bool resetPlxFec(unsigned int &status, bool vmeHardInit=false)
    {
        status = GOOD_FEC_STATUS;
        return true;
    }

    virtual void resetPlxFecForAutoRecovery() {
    }
    virtual unsigned int getResetFecAutoRecoveryGoodCnt() {
        return 0;
    }
    virtual unsigned int getResetFecAutoRecoveryBadCnt() {
        return 0;
    }
    virtual time_t getResetFecAutoRecoveryStartTime() {
        return 0;
    }
    virtual void resetGetResetFecAutoRecoveryCnt() {
    }

    virtual StdLinkBoxAccess& addLinkBoxAccess(unsigned int ccuAddress) {
        LOG4CPLUS_DEBUG(logger, "adding linkBoxAccess for ccu address " << std::hex << ccuAddress);
        StdLinkBoxAccessImpl* lbacc = new StdLinkBoxAccessImpl(*this, ccuAddress);
        linkBoxAccessList.push_back(lbacc);
        return *lbacc;
    }

    virtual LinkBoxAccessList& getLinkBoxAccessList() {
        return linkBoxAccessList;
    }

///////////////////////////////////////////////////////
#ifdef MIKOLAJ_FEC_DEBUG
    virtual std::string getFecCounterIO() {
        std::stringstream ostr;
        ostr << "FEC counters not implemented in MOCK!!!";
        return ostr.str();
    }
    virtual void resetFecCounterIO() {
    }
#endif
///////////////////////////////////////////////////////

};

}

#endif

