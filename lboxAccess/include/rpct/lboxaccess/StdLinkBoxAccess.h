#ifndef STDLINKBOXACCESS_H
#define STDLINKBOXACCESS_H

#include <string>
#include <istream>
#include <stdint.h>
namespace rpct {

class FecManager;

class StdLinkBoxAccess {
public:
	virtual ~StdLinkBoxAccess() {}

	virtual void setName(std::string nam) = 0;

    virtual FecManager& getFecManager() = 0;
    virtual unsigned int getCcuAddress() = 0;

    virtual void resetTtc() = 0;
    virtual void ccuExternalReset() = 0;

    virtual unsigned short memoryRead(uint32_t address) = 0;

    virtual void memoryWrite(uint32_t address, unsigned short data) = 0;

    virtual void memoryWrite16(uint32_t address, unsigned short data) = 0;

    virtual void memoryWrite16Block(uint32_t address, unsigned short* data,
                            uint32_t count) = 0;

    virtual void memoryWriteBlock(uint32_t address, unsigned char* data,
                            uint32_t count) = 0;

    virtual unsigned short memoryRead16(uint32_t address) = 0;

    virtual void memoryRead16Block(uint32_t address, unsigned short* data,
                           uint32_t count) = 0;

    virtual void memoryReadBlock(uint32_t address, unsigned char* data,
                           uint32_t count) = 0;

    static const uint32_t LB_ADDRESS_SPACE_SIZE = 0x800;
    virtual void lbWrite(int lb, uint32_t address, uint32_t data) = 0;

    virtual uint32_t lbRead(int lb, uint32_t address) = 0;

    virtual void lbWriteBlock(int lb, uint32_t address,
                      unsigned short* data, uint32_t count) = 0;

    virtual void lbReadBlock(int lb, uint32_t address,
                     unsigned short* data, uint32_t count) = 0;

    virtual unsigned char piaRead(int channel) = 0;

    virtual void piaWrite(int channel, unsigned char value) = 0;

    virtual void loadCb(const std::string& cbpcBin, const std::string& cbicIni, const std::string& lbcBin, const std::string& lbcIni) = 0;

    virtual void loadKtp(int lb, const std::string& ciiLbBin) = 0;

    virtual void programFlashCb(const std::string& cbromBin) = 0;

    virtual uint32_t programFlashKtp(int lb, const std::string& lbromBin) = 0;

    virtual void programConfigToFlashKtp(int lb, std::istream *in, uint32_t startaddr) = 0;

    virtual void loadFromFlash() = 0;


    virtual void putCBICinResetState() {};
    virtual void putCBICinRunState() {};

    virtual void configureCBICforLoadFromFlash() {};
    virtual void resetTTCrxStep1()  {};
    virtual void resetTTCrxStep2()  {};
    virtual bool resetTTCrxStep3()  {return false; };
    virtual void checkCBICafterLoadFromFlash()  {};

    virtual void enableTTCHardReset()  {};
    virtual void disableTTCHardReset()  {};

    virtual void configureCBICOperationMode(bool enableTTCHardReset)  {};

    virtual bool isTTCHardResetEnabled() = 0;

    //virtual bool checkCb() = 0;
};

}

#endif // ISTDLINKBOXACCESS_H
