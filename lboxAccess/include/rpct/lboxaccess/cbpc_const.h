#ifndef CBPC_CONST_H
#define CBPC_CONST_H

namespace rpct {
namespace CBPC {

#include "rpct/lboxaccess/cbpc_pkg.h"

}
}

/*
const int CBPC_VERSION = 0x01;
const int CBPC_VERSION_MAJOR = 0x01;
const int CBPC_FL_BUFB = 0xfd00;
const int CBPC_FL_BUFE = 0xfdff;
const int CBPC_TTC_ADH = 0xfe00;
const int CBPC_TTC_ADL = 0xfe01;
const int rlbcs_pkgCBPC_FL1_DIL = 0xfe02;
const int CBPC_FL1_DIH = 0xfe03;
const int CBPC_FL2_DIL = 0xfe04;
const int CBPC_FL2_DIH = 0xfe05;
const int CBPC_FL_AB0 = 0xfe06;
const int CBPC_FL_AB1 = 0xfe07;
const int CBPC_FL_AB2 = 0xfe08;
const int CBPC_FL1_DOL = 0xfe09;
const int CBPC_FL1_DOH = 0xfe0a;
const int CBPC_FL2_DOL = 0xfe0b;
const int CBPC_FL2_DOH = 0xfe0c;
const int CBPC_FL_CMD = 0xfe0d;
const int CBPC_FL_STAT = 0xfe0e;
const int CBPC_FL_STATL = 0xfe0f;
const int CBPC_FL_STATH = 0xfe10;
const int CBPC_JTAG_REG = 0xfe11;
const int CBPC_I2C_CHAN = 0xfe17;
const int CBPC_I2C = 0xfe18;
const int CBPC_FL_CMD2 = 0xfe20;
const int CBPC_BLK_STAT = 0xfe21;
const int CBPC_IREGS = 0xfe28;
const int CBPC_IRQ0_MASK = 0xfe40;
const int CBPC_IRQ1_MASK = 0xfe41;
const int CBPC_IRQ2_MASK = 0xfe42;
const int CBPC_IRQ3_MASK = 0xfe43;
const int CBPC_IRQ_ENABLE = 0xfe44;
const int CBPC_IRQ_STATUS = 0xfe45;
const int CBPC_VER_ID = 0xfe46;
const int CBPC_VER_MJR = 0xfe47;
const int CBPC_VRF_STAT = 0xfefc;
const int CBPC_DUMMY = 0xfefd;
const int CBPC_IDENT = 0xfefe;
const int CFC_WRITE = 0;
const int CFC_READ = 1;
const int CFC_CHIP_ERASE = 2;
const int CFC_SECTOR_ERASE = 3;
const int CFC_RESET = 4;
const int CFC_ERR_CLEAR = 5;
const int CFC_INC = 7;
const int CFC2_BLK_WRITE = 0;
const int CFC2_BLK_ERRCLR = 1;
const int CFC2_BLK_NEWADD = 2;
const int CBPCI_COR1 = 0;
const int CBPCI_COR2 = 1;
const int CBPCI_ERR1 = 2;
const int CBPCI_ERR2 = 3;
const int CBPCI_BLK = 4;
const int CBPCI_FCNTL = 5;

*/

#endif

