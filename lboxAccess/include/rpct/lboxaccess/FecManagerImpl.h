#ifndef RPCTFECMANAGERIMPL_H
#define RPCTFECMANAGERIMPL_H

#include <log4cplus/logger.h>

#include "rpct/lboxaccess/CCUException.h"
#include "rpct/lboxaccess/FecManager.h"
#include "rpct/lboxaccess/StdLinkBoxAccessAdapter.h"
//#include "rpct/lboxaccess/lbctrl_const.h"
#include "rpct/lboxaccess/cbic_const.h"
#include "rpct/lboxaccess/cbpc_const.h"
#include "rpct/lboxaccess/lbc_const.h"

#include "FecAccess.h"
#include "piaAccess.h"

#include <sstream>
#include <istream>
#include <algorithm>
#include <boost/timer.hpp>
#include <map>


namespace rpct {

class FecManagerImpl : public FecManager {
public:
	static void setupLogger(std::string namePrefix);
    static const uint32_t MAX_BLOCK_SIZE;
    static const uint32_t II_PAGE_MASK;
    enum FecType { FEC_TYPE_PCI, FEC_TYPE_VME };

    class StdLinkBoxAccessImpl : public StdLinkBoxAccessAdapter {
    private:
        FecManagerImpl& fecManager;
        unsigned int ccuAddress;
        std::string name;

        /*static const int CBIC_W0 = 0xfff0;
        static const int CBIC_R0 = 0xfff2;

        static const int EMG_NWRITE = 0x1;
        static const int EMG_NCS = 0x2;
        static const int EMG_NPROGRAM = 0x8;

        static const int EMG_DONE = 0x1;
        static const int EMG_INIT = 0x2;
        static const int CBPC_DONE = 0x4;
        static const int CBPC_INIT = 0x8;
        static const int LBC_DONE = 0x10;
        static const int LBC_INIT = 0x20;*/

        uint32_t LBCBase;
        uint32_t CB_PAGE;

        static const int PIAChanCount = 4;
        keyType PIAKeys[4];

        keyType CCUKey;
        keyType CCUMemoryKey;
        bool BlockTransfer;
        static const unsigned short MEM_CRA_BLOCK = 0xf;
        static const unsigned short MEM_CRA_SINGLE = 0xc;
        //static const int I2CChanCount = 4;
        //keyType CCUI2CKeys[I2CChanCount];

        boost::timer Timer;
        typedef std::map<int, uint32_t> TPageMap;
        TPageMap PageMap;


        bool ttcHardResetEnabled_;

        void setBlockTransfer(bool value);

        void startBlockTransfer16() {
            piaWrite(3, 0xfe);
        }
        void endBlockTransfer16() {
            piaWrite(3, 0xfc);
        }

        //void writeFlash(uint32_t address, uint32_t data);
        unsigned short w0Shadow;
        void w0Set(unsigned short bits) {
            w0Shadow |= bits;
            memoryWrite(CBIC::CBIC_W0, w0Shadow);
        }
        void w0Clr(unsigned short bits) {
            w0Shadow &= ~bits;
            memoryWrite(CBIC::CBIC_W0, w0Shadow);
        }
        unsigned short bitVal(int bitPos) {
            return 1 << bitPos;
        }

    public:
        StdLinkBoxAccessImpl(FecManagerImpl& fecManager, unsigned int ccuAddress);

        virtual ~StdLinkBoxAccessImpl() {
        }


    	virtual void setName(std::string nam) {
    		name = nam;
    	}

        void initFec();

        virtual FecManager& getFecManager() {
            return fecManager;
        }

        virtual unsigned int getCcuAddress() {
            return ccuAddress;
        }

        void selectLbc(int number); // LBC numbers start from 0

        void selectPage(int lb, uint32_t address);

        virtual void memoryWrite(uint32_t address, unsigned short data);

        virtual unsigned short memoryRead(uint32_t address);

        virtual void memoryWriteBlock(uint32_t address, unsigned char* data,
                          uint32_t length);

        virtual void memoryReadBlock(uint32_t address, unsigned char* data,
                         uint32_t length);

        virtual void memoryRead16Block(uint32_t address, unsigned short* data,
                         uint32_t length);

        virtual void memoryWrite16Block(uint32_t address, unsigned short* data,
                         uint32_t length);

        virtual void memoryWrite16(uint32_t address, unsigned short data);

        virtual unsigned short memoryRead16(uint32_t address);

        virtual void ccuExternalReset();

        /*virtual void programFlash(const std::string& file);
        virtual void programXilinx(const char* file);
        virtual void programCbpc(const std::string& file);
        virtual void programLbc(const std::string& file);

        virtual void programFlash(uint32_t lbcbase, const std::string& file) {
            programFlash(file.c_str());
        }

        virtual void programXilinx(int lbc, const std::string& file) {
            selectLbc(lbc);
            programXilinx(file.c_str());
        }*/


        virtual unsigned char piaRead(int channel);

        virtual void piaWriteBase(int channel, unsigned char value);
        virtual void piaWrite(int channel, unsigned char value);

        virtual void lbWrite(int lb, uint32_t address, uint32_t data);

        virtual uint32_t lbRead(int lb, uint32_t address);

        virtual void lbWriteBlock(int lb, uint32_t address,
                          unsigned short* data, uint32_t count);

        virtual void lbReadBlock(int lb, uint32_t address,
                         unsigned short* data, uint32_t count);

        virtual void loadLbc(const std::string& file1, const std::string& file2);
        virtual void setCbpcFlashAddr(uint32_t addr);
        virtual void setLbcFlashAddr(int lb, uint32_t addr);
        virtual uint32_t readCbpcFlashAddr(uint32_t addr);
        virtual uint32_t readLbcFlashAddr(int lb, uint32_t addr);
        virtual uint32_t writeLbcFlashAddr(int lb, uint32_t addr, uint32_t data);
        virtual void resetCbpcFlash();
        virtual void resetLbcFlash(int lb);
        virtual void waitCbpcFlashDone();
        virtual void waitLbcFlashDone(int lb);

        // Programs Control Board's CBPC and LBC flash memories with a given firmware file.
        // Binary file 'cb_rom.bin' is taken from $(HOME)/boot/LBB directory.
        virtual void programFlashCb(const std::string& cbromBin);

        // Programs Link Board's KTP flash memory with a given firmware file (LB slot numbers are 0...8).
        // Binary file 'lb_rom.bin' is taken from $(HOME)/boot/LBB directory.
        virtual uint32_t programFlashKtp(int lb, const std::string& lbromBin);

        // Add configuration after firmware in KTP chip.
        // Programs Link Board's KTP flash memory with a given firmware file (LB slot numbers are 0...8).
        void programConfigToFlashKtp(int lb, std::istream *in, uint32_t startaddr);
        void programConfigToFlashKtpOld(int lb, std::istream *in, uint32_t startaddr);

        // Downloads firmware to FPGA from flash memories on the Link- and Control Boards.
        virtual void loadFromFlash();

        virtual void putCBICinResetState();
        virtual void putCBICinRunState();

        //velow is the sequence equivalent to the  loadFromFlash(bool enableTTCHardReset)
        virtual void configureCBICforLoadFromFlash();
        virtual void resetTTCrxStep1();
        //needed usleep(8000000);
        virtual void resetTTCrxStep2();
        //needed usleep(1000000);
        virtual bool resetTTCrxStep3();
        //needed putCBICinRunState();
        //needed usleep(FixedHardwareSettings::CONFIGURE_FROM_FLASH_TIME);
        virtual void checkCBICafterLoadFromFlash();

        virtual void enableTTCHardReset();
        virtual void disableTTCHardReset();

        virtual void configureCBICOperationMode(bool enableTTCHardReset);

        // Downloads KTP firmware to Link Board's FPGA via CCU (LB slot numbers are 0...8).
        // Binary file 'cii_lb_std.bin' is taken from $(HOME)/boot/LBB directory.
        virtual void loadKtp(int lb, const std::string& ciiLbBin);

        // Downloads LBC firmware and CBPC firmware to Control Board's FPGA via CCU.
        // Binary files 'lbc.bin', 'lbc.ini', 'cbpc.bin', 'cbic.ini' are taken from $(HOME)/boot/LBB directory.
        virtual void loadCb(const std::string& cbpcBin, const std::string& cbicIni,
                const std::string& lbcBin, const std::string& lbcIni);
        // Check if CB is in a good state
        virtual bool checkCb();

        /*
         * set during the loadFromFlash(bool enableTTCHardReset)
         */
        virtual bool isTTCHardResetEnabled() {
        	return ttcHardResetEnabled_;
        }
    };
    friend class StdLinkBoxAccessImpl;
private:
    static log4cplus::Logger logger;
    FecAccess* fecAccess_;
    FecType fecType;
    int pciSlot;
    int fec;
    int ring;
    std::list<keyType>* fecList;
    std::string lockFilePath;

    LinkBoxAccessList linkBoxAccessList;

    void init(FecType fecType, int pciSlot, int fec, int ring, bool vmeHardInit=false);

    void handleFecException(FecExceptionHandler& e);

    unsigned int resetFecAutoRecoveryGoodCnt_;
    unsigned int resetFecAutoRecoveryBadCnt_;
    time_t resetFecAutoRecoveryStartTime_;

public:
    static log4cplus::Logger& getLogger() {  return logger; }

    FecManagerImpl();
    FecManagerImpl(int pciSlot, int fec, int ring);
    virtual ~FecManagerImpl();

    virtual int getPciSlot();
    virtual int getVmeSlot();
    virtual int getRingNumber();

    virtual FecAccess* getFecAccess() {
        return fecAccess_;
    }

///////////////////////////////////////////////////////
#ifdef MIKOLAJ_FEC_DEBUG
   virtual std::string getFecCounterIO();
   virtual void resetFecCounterIO();
#endif
///////////////////////////////////////////////////////

    virtual void resetFec();
    virtual StdLinkBoxAccess& addLinkBoxAccess(unsigned int ccuAddress);
    virtual LinkBoxAccessList& getLinkBoxAccessList() {
        return linkBoxAccessList;
    }
    static const unsigned int GOOD_FEC_STATUS1 = 0x4c90;
    static const unsigned int GOOD_FEC_STATUS2 = 0x3c90;
    static const unsigned int GOOD_FEC_STATUS3 = 0xc90;
    virtual bool getFecRingSR0(unsigned int &status);

    virtual bool resetPlxFec(unsigned int &status, bool vmeHardInit=false);

    virtual void resetPlxFecForAutoRecovery();

    virtual unsigned int getResetFecAutoRecoveryGoodCnt() {
        return resetFecAutoRecoveryGoodCnt_;
    }

    virtual unsigned int getResetFecAutoRecoveryBadCnt() {
        return resetFecAutoRecoveryBadCnt_;
    }

    virtual time_t getResetFecAutoRecoveryStartTime() {
        return resetFecAutoRecoveryStartTime_;
    }

    virtual void resetGetResetFecAutoRecoveryCnt();
};

}

#endif

