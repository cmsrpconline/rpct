static const int RLBCS_NOF_CBPC_IREGS = 8;
static const int RLBCS_NOF_LBC_IREGS = 2;
static const int RLBCS_NOF_LBS = 9;
static const int RLBCS_IRQ0_POLL = 0x6ffc;
static const int RLBCS_IRQ1_POLL = 0x6ffd;
static const int RLBCS_IRQ2_POLL = 0x6ffe;
static const int RLBCS_IRQ3_POLL = 0x6fff;
