#if !defined CCUEXCEPTION_HH
#define CCUEXCEPTION_HH


//#include <exception>
#include "tb_std.h"
#include <string>

namespace rpct {

class CCUException : public TException {
public:
    CCUException(const std::string& msg = "") throw()
	: TException( "CCU Error: " + msg ) {}
    EXCFASTCALL virtual ~CCUException() throw() {}
};

class CCUExceptionRingInUse : public TException {
public:
	CCUExceptionRingInUse(const std::string& msg = "") throw()
	: TException( "Error: " + msg ) {}
    EXCFASTCALL virtual ~CCUExceptionRingInUse() throw() {}
};

}


#endif

