#ifndef TFECMANAGEROLD_H
#define TFECMANAGEROLD_H

#include "rpct/lboxaccess/CCUException.h"
#include "rpct/lboxaccess/LinkBoardoxAccessAdapter.h"
#include "rpct/lboxaccess/cbic_const.h"

#include "FecAccess.h"
#include "piaAccess.h"
#include <sstream>
#include <algorithm>
#include <boost/timer.hpp>
#include <map>

extern bool G_CCUDebug;


class TFECManagerOld : public LinkBoardoxAccessAdapter {
public:
    static const uint32_t MaxBlockSize;
    static const uint32_t IIPageMask = 0x7ff;
private:
    uint32_t LBCBase;
//    uint32_t CB_IDREG;
//    uint32_t CB_TESTREG;
//    uint32_t CB_FDIN;
//    uint32_t CB_FDOUT;
//    uint32_t CB_FADL;
//    uint32_t CB_FADH;
//    uint32_t CB_FCMD;
//    uint32_t CB_FSTAT;
    uint32_t CB_LSTAT;
//    uint32_t CB_END1L;
//    uint32_t CB_END1H;
//    uint32_t CB_END2L;
//    uint32_t CB_END2H;
    uint32_t CB_INTERF;
//    uint32_t CB_JTAG;
    uint32_t CB_BLCK_BUF;

    uint32_t CB_XLNXD;
    uint32_t CB_XLNXC;
    uint32_t CB_PAGE;


    static const int CBIC_W0 = 0xfff0;
    static const int CBIC_R0 = 0xfff2;

    static const int EMG_NWRITE = 0x1;
    static const int EMG_NCS = 0x2;
    static const int EMG_NPROGRAM = 0x8;

    static const int EMG_DONE = 0x1;
    static const int EMG_INIT = 0x2;
    static const int CBPC_DONE = 0x4;
    static const int CBPC_INIT = 0x8;
    static const int LBC_DONE = 0x10;
    static const int LBC_INIT = 0x20;
    FecAccess* FECAcc;
    //FecDevice* FEC;
    static const int PIAChanCount = 4;
    //piaAccess* PIA[PIAChanCount];
    keyType PIAKeys[4];

    keyType CCUKey;
    keyType CCUMemoryKey;
    bool BlockTransfer;
    static const int I2CChanCount = 4;
    static const unsigned short MEM_CRA_BLOCK = 0xf;
    static const unsigned short MEM_CRA_SINGLE = 0xc;
    keyType CCUI2CKeys[I2CChanCount];

    boost::timer Timer;
    //int CurLBC;
    typedef std::map<int, uint32_t> TPageMap;
    TPageMap PageMap;

    void SetBlockTransfer(bool value) {
      // rozpoczecie transferu blokowego D <<< fe (bit 2 na 1)
      // zakonczenie D << fc (bit 2 na 0)
      if (BlockTransfer != value) {
        if (G_CCUDebug)
          cout << (value ? "Changing to block transfer" : "Changing to single transfer") << endl;
        FECAcc->setMemoryChannelCRA(CCUMemoryKey, value ? MEM_CRA_BLOCK : MEM_CRA_SINGLE);
        BlockTransfer = value;
      }
    }

    void StartBlockTransfer16() {
        PIAWrite(3, 0xfe);
    }
    void EndBlockTransfer16() {
        PIAWrite(3, 0xfc);
    }

    /*void FlashAF(uint32_t data) {
      CCUMemoryWrite16(CB_FADL, data & 0xffff);
      CCUMemoryWrite16(CB_FADH, (data >> 16) & 0xf);
    }

    void FlashIF() {
      CCUMemoryWrite16(CB_FCMD, 0x10);
    }

    void FlashWF(unsigned short data) {
      CCUMemoryWrite16(CB_FDIN, data);
      CCUMemoryWrite16(CB_FCMD, 0x2);
    }

    unsigned short FlashRF() {
      CCUMemoryWrite16(CB_FCMD, 0x1);
      CCUMemoryRead16(CB_FSTAT);
      return CCUMemoryRead16(CB_FDOUT);
    }

    void FlashRSTF() {
      CCUMemoryWrite16(CB_FCMD, 0x8);
      CCUMemoryWrite16(CB_FCMD, 0x0);
    }

    void FlashEF() {
      CCUMemoryWrite16(CB_FCMD, 0x4);
    }*/

    void WriteFlash(uint32_t address, uint32_t data);


    /*class TI2C : public TI2CInterface {
    private:
       TFECManager& Parent;
       const int Channel;
    public:
        TI2C(TFECManager& parent, int channel) : Parent(parent), Channel(channel) {}

       virtual void Write8(unsigned char address, unsigned char value)
         {
           Parent.I2CWrite(Channel, address, value);
         }

       virtual unsigned char Read8(unsigned char address)
         {
           return Parent.I2CRead(Channel, address);
         }

       virtual void WriteADD(unsigned char address, unsigned char value1,
                                    unsigned char value2)
         {
                throw EI2C("TI2C::WriteADD: not implememted");
         }

       virtual void ReadADD(unsigned char address, unsigned char& value1,
                                unsigned char& value2)
         {
            throw EI2C("TI2C::ReadADD: not implememted");
         }

       virtual void Write(unsigned char address, unsigned char* data, size_t count)
         {
            throw EI2C("TI2C::Write: not implememted");
         }
    };
    std::vector<TI2C*> I2CVector;*/

public:
    TFECManagerOld();

    ~TFECManagerOld()
      {
        //delete FEC;
      }

    FecAccess* GetFecAccess();

    void SelectLBC(int number); // LBC numbers start from 0

    void SelectPage(int lb, uint32_t address);

    virtual void MemoryWrite(uint32_t address, unsigned short data);

    virtual unsigned short MemoryRead(uint32_t address);

  void MemoryWriteBlock(uint32_t address, unsigned char* data,
                      uint32_t length);

  void MemoryReadBlock(uint32_t address, unsigned char* data,
                     uint32_t length);

  void MemoryRead16Block(uint32_t address, unsigned short* data,
                     uint32_t length);

  void MemoryWrite16Block(uint32_t address, unsigned short* data,
                     uint32_t length);

  void MemoryWrite16(uint32_t address, unsigned short data);

  unsigned short MemoryRead16(uint32_t address);

  void CCUExternalReset();

  void ProgramFlash(const std::string& file);
  void ProgramXilinx(const char* file);
  void ProgramCBPC(const std::string& file);
  void ProgramLBC(const std::string& file);

  virtual void ProgramFlash(uint32_t lbcbase, const std::string& file)
    {
      ProgramFlash(file);
    }

  virtual void ProgramXilinx(int lbc, const std::string& file)
    {
      SelectLBC(lbc);
      ProgramXilinx(file.c_str());
    }



/*
  void I2CWrite(int channel, unsigned char address, unsigned char value)
    {
      if (G_CCUDebug) {
        cout << hex << "i2c w " << channel << " " <<  address << " "
             << value << endl;
      }

      FecRingDevice *fecRingDevice = FecAcc->getFecRingDevice(
            CCUI2CKeys[channel] | setAddressKey((DD_TYPE_FEC_DATA16)address));
      fecRingDevice->writei2cDevice()


      FEC->writei2cDevice(
        CCUI2CKeys[channel] | setAddressKey((DD_TYPE_FEC_DATA16)address),
        (DD_TYPE_FEC_DATA16)value);
    }

  unsigned char I2CRead(int channel, unsigned char address)
    {
      if (G_CCUDebug)
        cout << hex << "i2c r " << channel << " " <<  address << " = " << flush;

      unsigned char result = FEC->readi2cDevice(CCUI2CKeys[channel] | setAddressKey((DD_TYPE_FEC_DATA16)address));
        if (G_CCUDebug)
      cout << result << endl;
     return result;
    }


  TI2CInterface& GetI2C(int channel) {
    return *I2CVector.at(channel);
  }*/

    unsigned char PIARead(int channel);

    void PIAWrite(int channel, unsigned char value);

    /*FecDevice* GetFecDevice() {
      return FEC;
    }*/

    void ResetFec();

    virtual void LBWrite(int lb, uint32_t address, uint32_t data);

    virtual uint32_t LBRead(int lb, uint32_t address);

    virtual void LBWriteBlock(int lb, uint32_t address,
                      unsigned short* data, uint32_t count);

    virtual void LBReadBlock(int lb, uint32_t address,
                     unsigned short* data, uint32_t count);

};



#endif

