#ifndef STDLINKBOXACCESSADAPTER_H
#define STDLINKBOXACCESSADAPTER_H

#include "rpct/lboxaccess/StdLinkBoxAccess.h"

namespace rpct {

class StdLinkBoxAccessAdapter : public StdLinkBoxAccess {
public:
    StdLinkBoxAccessAdapter();
    virtual ~StdLinkBoxAccessAdapter();

    virtual void resetTtc();
};

}

#endif // STDLINKBOXACCESSADAPTER_H
