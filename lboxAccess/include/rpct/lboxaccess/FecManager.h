#ifndef FECMANAGER_H_
#define FECMANAGER_H_

#include <vector>
#include <iostream>
#include "rpct/lboxaccess/StdLinkBoxAccess.h"
#include <time.h>

namespace rpct {

class FecManager {
public:
    virtual ~FecManager() {}
    virtual int getPciSlot() = 0;
    virtual int getVmeSlot() = 0;
    virtual int getRingNumber() = 0;
    virtual void resetFec() = 0;
    virtual bool getFecRingSR0(unsigned int &status) = 0;
    virtual bool resetPlxFec(unsigned int &status, bool vmeHardInit) = 0;
    virtual void resetPlxFecForAutoRecovery() = 0;
    virtual unsigned int getResetFecAutoRecoveryGoodCnt() = 0;
    virtual unsigned int getResetFecAutoRecoveryBadCnt() = 0;
    virtual time_t getResetFecAutoRecoveryStartTime() = 0;
    virtual void resetGetResetFecAutoRecoveryCnt() = 0;
    virtual StdLinkBoxAccess& addLinkBoxAccess(unsigned int ccuAddress) = 0;
    typedef std::vector<StdLinkBoxAccess*> LinkBoxAccessList;
    virtual LinkBoxAccessList& getLinkBoxAccessList() = 0;
    //virtual void bypass(unsigned int ccu1, unsigned int ccu3) = 0;

///////////////////////////////////////////////////////
#ifdef MIKOLAJ_FEC_DEBUG
    virtual std::string getFecCounterIO() = 0;
    virtual void resetFecCounterIO() = 0;
#endif
///////////////////////////////////////////////////////
};
}
#endif /*FECMANAGER_H_*/
