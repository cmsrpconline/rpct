#include "rpct/lboxaccess/FecManagerImpl.h"
#include "rpct/std/IllegalArgumentException.h"
#include "rpct/devices/FixedHardwareSettings.h"
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include <boost/filesystem/operations.hpp>

#include "FecAccess.h"
#include "FecVmeRingDevice.h"

#include "log4cplus/loggingmacros.h"

using namespace log4cplus;
using namespace std;

namespace rpct {

const uint32_t FecManagerImpl::MAX_BLOCK_SIZE = 64;
const uint32_t FecManagerImpl::II_PAGE_MASK = 0x7ff;
Logger FecManagerImpl::logger = Logger::getInstance("FecManagerImpl");
void FecManagerImpl::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}

FecManagerImpl::FecManagerImpl()
{

    LOG4CPLUS_DEBUG(logger, "Constructor start (params from env)");

    int fec, ring;
    unsigned int ccu;
    FecType ft;
    char* env_str;

    env_str = getenv("FEC_ADDRESS");
    if (env_str == NULL) {
        fec = 0x0;
    } else {
        sscanf(env_str, "%d", &fec);
    }

    env_str = getenv("RING_ADDRESS");
    if (env_str == NULL) {
        ring = 0x0;
    } else {
        sscanf(env_str, "%x", &ring);
    }

    env_str = getenv("CCU_ADDRESS");
    if (env_str == NULL) {
        ccu = 0x1;
    } else {
        sscanf(env_str, "%x", &ccu);
    }

    env_str = getenv("FEC_TYPE");
    if (env_str == NULL) {
        throw TException("Environment variable FEC_TYPE not set. Possible values: pci, vme.");
    }
    string fecTypeStr = env_str;
    if (fecTypeStr == "pci") {
        ft = FEC_TYPE_PCI;
    } else if (fecTypeStr == "vme") {
        ft = FEC_TYPE_VME;
    } else {
        throw TException("Invalid environment variable FEC_TYPE: " + fecTypeStr + ". Possible values: pci, vme.");
    }

    LOG4CPLUS_DEBUG(logger, "Constructor for: fec_type=" << ((string) fecTypeStr) << " fec=" << fec << " ring=" << ring
            << " ccu_addr=" << ccu);

    init(ft, 0, fec, ring);
    try {
        addLinkBoxAccess(ccu);
    } catch (std::exception& e) {
        LOG4CPLUS_WARN(logger, "Exception during initializing: " << e.what() << "\nReseting FEC and trying once again.");
        resetFec();
        addLinkBoxAccess(ccu);
    }
    LOG4CPLUS_DEBUG(logger, "Constructor end (params from env)");
}

FecManagerImpl::FecManagerImpl(int pciSlot, int fec, int ring)
{
    LOG4CPLUS_DEBUG(logger, "Constructor start (vme: pciSlot=" << pciSlot << " fec=" << fec << " ring=" << ring << ")");

    init(FEC_TYPE_VME, pciSlot, fec, ring);
    /*this do not helped for the CCU errors during XDAQ configure
    try {
    	init(FEC_TYPE_VME, pciSlot, fec, ring);
    }
    catch(CCUException& e) {
    	LOG4CPLUS_WARN(logger, "CCUException during FecManagerImpl::init : "<<e.what()<<". Trying FecManagerImpl::init one more time");
    	init(FEC_TYPE_VME, pciSlot, fec, ring);
    }*/
    LOG4CPLUS_DEBUG(logger, "Constructor end (vme: pciSlot=" << pciSlot << " fec=" << fec << " ring=" << ring << ")");
}

FecManagerImpl::~FecManagerImpl() {
    for_each(linkBoxAccessList.begin(), linkBoxAccessList.end(), FDelete<LinkBoxAccessList::value_type> ());
    delete fecAccess_;
    delete fecList;
    boost::filesystem::remove(lockFilePath);
}

int FecManagerImpl::getPciSlot() {
    return pciSlot;
}
int FecManagerImpl::getVmeSlot() {
    return fec;
}
int FecManagerImpl::getRingNumber() {
    return ring;
}

void FecManagerImpl::init(FecType ft, int pciSlot, int fec, int ring, bool vmeHardInit){
	LOG4CPLUS_DEBUG(logger, __FUNCTION__<<": STARTED (old fecAccess_="<<hex<<fecAccess_<<")");

    // check/create lock files for this ring
    ostringstream lockFilePathStr;
    lockFilePathStr << "/tmp/fecring" << fec << "_" << ring << ".lck";
    lockFilePath = lockFilePathStr.str();
    LOG4CPLUS_INFO(logger, "Corresponding lock file is "<<lockFilePath);
    if (boost::filesystem::exists(lockFilePath)) {
        // a lock file exists: open it and read process number, check if the process is still running
        ifstream lckfile(lockFilePath.c_str());
        if (!lckfile) {
            LOG4CPLUS_DEBUG(logger, "Could not open lock file " << lockFilePath);
        }
        int processNum;
        lckfile >> processNum;
        lckfile.close();
        ostringstream procDir;
        procDir << "/proc/" << processNum;
        if (processNum != getpid() && boost::filesystem::exists(procDir.str())) {
            throw CCUExceptionRingInUse("Another process (pid " + rpct::toString(processNum) + ") is using the same CCU ring. Try restart the application.");
        }
        boost::filesystem::remove(lockFilePath);
    }
    ofstream newlckfile(lockFilePath.c_str());
    if (!newlckfile) {
        LOG4CPLUS_WARN(logger, "Could not create lock file " << lockFilePath);
    }
    newlckfile << getpid();
    newlckfile.close();

    if (pciSlot > 3) {
        throw IllegalArgumentException("pciSlot");
    }
    if (fec > 21 || fec < 1) {
        throw IllegalArgumentException("fec");
    }
    if (ring > 7 || ring < 0) {
        throw IllegalArgumentException("ring");
    }

    this->fecType = ft;
    this->pciSlot = pciSlot;
    this->fec = fec;
    this->ring = ring;
    this->fecList = 0;
    try {
        if (fecType == FEC_TYPE_PCI) {
            // FecAccess( forceAck, initFec, scanFECs, scanCCUs, i2cSpeed, invertClockPolarity )
            fecAccess_ = new FecAccess(true, false, true, false, 100); // force init
        } else if (fecType == FEC_TYPE_VME) {
            string vmeFileName;
            char* env_str = getenv("FECSOFTWARE_ROOT");
            if (env_str == 0) {
                LOG4CPLUS_INFO(logger, "Environment variable FECSOFTWARE_ROOT not set.");
                env_str = getenv("XDAQ_SETUP_ROOT");
                if (env_str == 0) {
                    throw TException("Environment variables FECSOFTWARE_ROOT nor XDAQ_SETUP_ROOT not set.");
                }
                char* zone = getenv("XDAQ_ZONE");
                if (zone == 0) {
                    throw TException("Environment variable XDAQ_ZONE not set.");
                }
                vmeFileName = string(env_str) + "/" + zone + "/profile/FecAddressTable.dat";
                //throw TException("Environment variable FECSOFTWARE_ROOT not set.");
            }
            else {
                vmeFileName = env_str + string("/config/FecAddressTable.dat");
            }
            LOG4CPLUS_INFO(logger, "FEC address table: " << vmeFileName);
            /*fecAccess_ = new FecAccess (0, vmeFileName, FecVmeRingDevice::VMEFECBASEADDRESSES,
             true, false, true, false, 100, FecVmeRingDevice::STR_BUSADAPTER_NAME[CAENPCI]) ;*/

            unsigned int fecBaseAddresses[MAX_NUMBER_OF_SLOTS] = { 0 };
            fecBaseAddresses[fec] = FecVmeRingDevice::VMEFECBASEADDRESSES[fec];

            LOG4CPLUS_DEBUG(logger, "creating fecAccess" << " pciSlot = " << pciSlot << " fecAddress = 0x" << hex << fec
                    << " ringAddress = " << dec << ring);

            if(vmeHardInit) {
                LOG4CPLUS_INFO(logger, "creating fecAccess" << " pciSlot = " << pciSlot << " fecAddress = 0x" << hex << fec << " ringAddress = " << dec << ring);
                // a la ProgramTest FEC SETTINGS
                LOG4CPLUS_DEBUG(logger, "FecAccess constructor [params from ProgramTest]: "
                    <<"  pciSlot="<<(ulong) pciSlot<<endl
                    <<"  vmeFileName="<<vmeFileName<<endl
                    <<"  fecBaseAddr="<<fecBaseAddresses<<endl
                    <<"  forceAck="<<true<<endl
                    <<"  initFec="<<false<<endl
                    <<"  scanFECs="<<true<<endl
                    <<"  scanCCUs="<<false<<endl
                    <<"  i2cSpeed="<<100<<endl
                    <<"  FecVmeRingDevice="<<FecVmeRingDevice::STR_BUSADAPTER_NAME[CAENPCI]<<endl
                    <<"  blockMode="<<false<<endl
                    <<"  numberOfRing="<<8<<endl
                    <<"  invertClockPolarity="<<false);
                fecAccess_ = new FecAccess((ulong) pciSlot, vmeFileName, fecBaseAddresses,
                    true, // bool forceAck = false
                    false, // bool initFec = true   <---- changed on 6 Aug 2008 (MP & MC)
                    true, // bool scanFECs = false
                    false, // bool scanCCUs = false
                    100, // tscType16 i2cSpeed = 100
                    FecVmeRingDevice::STR_BUSADAPTER_NAME[CAENPCI],
                    false, // bool blockMode = false,
                    6, //tscType8 numberOfRing = 8,
                    false //bool invertClockPolarity = false
                    );
            } else { // DEFAULT FEC SETTINGS
                LOG4CPLUS_DEBUG(logger, "FecAccess constructor [optimized params]: "
                    <<"  pciSlot="<<(ulong) pciSlot<<endl
                    <<"  vmeFileName="<<vmeFileName<<endl
                    <<"  fecBaseAddr="<<fecBaseAddresses<<endl
                    <<"  forceAck="<<true<<endl
                    <<"  initFec="<<true<<endl
                    <<"  scanFECs="<<false<<endl
                    <<"  scanCCUs="<<false<<endl
                    <<"  i2cSpeed="<<100<<endl
                    <<"  FecVmeRingDevice="<<FecVmeRingDevice::STR_BUSADAPTER_NAME[CAENPCI]<<endl
                    <<"  blockMode="<<true<<endl
                    <<"  numberOfRing="<<6<<endl
                    <<"  invertClockPolarity="<<false);
                fecAccess_ = new FecAccess((ulong) pciSlot, vmeFileName, fecBaseAddresses,
                        true, // bool forceAck = false
                        true, // bool initFec = true   <---- changed on 6 Aug 2008 (MP & MC)
                        false, // bool scanFECs = false
                        false, // bool scanCCUs = false
                        100, // tscType16 i2cSpeed = 100
                        FecVmeRingDevice::STR_BUSADAPTER_NAME[CAENPCI],
                        true, // bool blockMode = false,
                        6, //tscType8 numberOfRing = 8,
                        false //bool invertClockPolarity = false
                        );
            } // end of if(vmeHardInit)
            LOG4CPLUS_DEBUG(logger, "creating fecAccess done");

            // After a crate reset, the plug and play must re-charged (if it is used)
            // it is automatically done by FecAccess class
            /*if (crateReset_) {
             fecAccess_->crateReset() ;
             }*/

            // Retrieve the FEC hardware ID
            keyType fecRingKey = buildFecRingKey(fec, ring);
            fecAccess_->setFecRingDevice(fecRingKey);
            LOG4CPLUS_DEBUG(logger, "fec ring device added");

            /*
             * It seems that firmware version checking works only for ring = 0, so I comment this out
             try {
             unsigned int fecFirmwareVersion = fecAccess_->getFecFirmwareVersion(fecRingKey);
             LOG4CPLUS_INFO(logger, "FEC firmware version 0x" << hex << fecFirmwareVersion);
             LOG4CPLUS_INFO(logger, "FEC vme version 0x" << hex << fecAccess_->getVmeVersion(fecRingKey));
             LOG4CPLUS_INFO(logger, "FEC trigger version 0x" << hex << fecAccess_->getTriggerVersion(fecRingKey));
             }
             catch (FecExceptionHandler &e) {
             std::stringstream msgInfo ;
             msgInfo << "Problem checking FEC version on CCS " << fec << ", ring " << ring << ". " << e.what();
             throw CCUException(msgInfo.str());
             }*/

            //            fecAccess_ = new FecAccess((ulong)pciSlot, vmeFileName, fecBaseAddresses,
            //                    true,  // bool forceAck = false
            //                    false, // bool initFec = true
            //                    true,  // bool scanFECs = false
            //                    false, // bool scanCCUs = false
            //                    100,   // tscType16 i2cSpeed = 100
            //                    FecVmeRingDevice::STR_BUSADAPTER_NAME[CAENPCI]);
        } else {
            throw CCUException("Invalid value of fecType");
        }

        LOG4CPLUS_DEBUG(logger, "FecAccess created");

    } catch (FecExceptionHandler& e) {
        handleFecException(e);
        throw CCUException(e.getErrorMessage());
    }

    resetGetResetFecAutoRecoveryCnt();
    LOG4CPLUS_DEBUG(logger, __FUNCTION__<<": DONE (new fecAccess_="<<hex<<fecAccess_<<")");
}

void FecManagerImpl::handleFecException(FecExceptionHandler& e) {
    LOG4CPLUS_ERROR(logger, e.getErrorMessage());

#ifdef DEBUGGETREGISTERS
    FecRingRegisters* regs = e.getFecRingRegisters();
    if (regs != NULL) {
        FecRingRegisters::displayAllRegisters(*regs);
    }
#endif
}

StdLinkBoxAccess& FecManagerImpl::addLinkBoxAccess(unsigned int ccuAddress) {
    LOG4CPLUS_DEBUG(logger, "adding linkBoxAccess for ccu address 0x" << hex << ccuAddress);
    try {
        if (fecList == 0) {
            LOG4CPLUS_DEBUG(logger, "getting fecList");
            fecList = fecAccess_->getFecList();
            LOG4CPLUS_DEBUG(logger, "fecList retrieved");
            if (fecList == 0 || fecList->empty()) {
                LOG4CPLUS_DEBUG(logger, "fecList empty");
            } else {
                for (list<keyType>::iterator i = fecList->begin(); i != fecList->end(); ++i) {
                    LOG4CPLUS_DEBUG(logger, "0x" << hex << (*i));
                }
            }
        }
        LOG4CPLUS_DEBUG(logger, "addLinkBoxAccess: 1");
        if ((fecList == 0) || (fecList->empty())) {
            LOG4CPLUS_WARN(logger, "No FEC rings board found");
            throw TException("No FEC rings board found");
        }

        LOG4CPLUS_INFO(logger, "!!! TODO check if expected fec_key is present");
        StdLinkBoxAccessImpl* lbacc = new StdLinkBoxAccessImpl(*this, ccuAddress);
        linkBoxAccessList.push_back(lbacc);
        LOG4CPLUS_DEBUG(logger, "adding linkBoxAccess for ccu address 0x" << hex << ccuAddress << ": Ended OK");
        return *lbacc;
    } catch (FecExceptionHandler& e) {
        LOG4CPLUS_INFO(logger, "adding linkBoxAccess for ccu address 0x" << hex << ccuAddress << ": Failed");
        handleFecException(e);
        throw CCUException(e.getErrorMessage());
    };
}

FecManagerImpl::StdLinkBoxAccessImpl::StdLinkBoxAccessImpl(FecManagerImpl& fm, unsigned int ccu) :
    fecManager(fm), ccuAddress(ccu), BlockTransfer(true), w0Shadow(0) {
    ttcHardResetEnabled_ = false;
    LOG4CPLUS_DEBUG(logger, "Creating StdLinkBoxAccessImpl ccuAddr = 0x" << hex << ccu);

    try {
        CCUKey = setFecSlotKey(fecManager.fec) | setRingKey(fecManager.ring) | setCcuKey(ccuAddress);
        CCUMemoryKey = CCUKey | setChannelKey(0x40);

        initFec();
    } catch (FecExceptionHandler& e) {
        fecManager.handleFecException(e);
        throw CCUException(e.getErrorMessage());
    };
    LOG4CPLUS_DEBUG(logger, "initialized PIA channels");
}

void FecManagerImpl::StdLinkBoxAccessImpl::initFec() {
    LOG4CPLUS_DEBUG(logger, __FUNCTION__<<": initializing PIA channels: started");

    try {
        selectLbc(0);

        LOG4CPLUS_DEBUG(logger, "initializing memory channel");
        fecManager.fecAccess_->addMemoryAccess(CCUMemoryKey, MODE_SHARE);
        fecManager.fecAccess_->setMemoryChannelCRA(CCUMemoryKey, MEM_CRA_SINGLE);
        fecManager.fecAccess_->setMemoryChannelWin1HReg(CCUMemoryKey, 0xFFFF);
        LOG4CPLUS_DEBUG(logger, "memory channel initialized");
        // enable interrupts
        fecManager.fecAccess_->setCcuCRB(CCUKey, 0x3F);
        LOG4CPLUS_DEBUG(logger, "enabled interrupts");
        for (int i = 0; i < PIAChanCount; i++) {
            keyType myKey = (CCUKey | setChannelKey((0x30 + i)) );
            PIAKeys[i] = myKey;
            fecManager.fecAccess_->addPiaAccess(myKey, MODE_SHARE);
            fecManager.fecAccess_->setPiaChannelGCR(myKey, 0x10);
            if (i == 0 || i == 2) {
                fecManager.fecAccess_->setPiaChannelDDR(myKey, 0x0);
            } else {
                fecManager.fecAccess_->setPiaChannelDDR(myKey, 0xff);
            }
        }

        //LOG4CPLUS_INFO(logger, "fetCcuCRE(CCUKey): "<<hex<<fecManager.fecAccess_->getCcuCRE(CCUKey));


        endBlockTransfer16(); //to recover after ccu error or other failure, KB
    } catch (FecExceptionHandler& e) {
        fecManager.handleFecException(e);
        throw CCUException(e.getErrorMessage());
    };
    LOG4CPLUS_DEBUG(logger, __FUNCTION__<<": initializing PIA channels: done");
}

void FecManagerImpl::resetFec() {
    try {
        keyType key = buildFecRingKey(fec, ring);
        fecAccess_->fecHardReset(key);
        fecAccess_->fecRingReset(key);
        //endBlockTransfer16(); //to recover after ccu error or other failure, KB
    } catch (FecExceptionHandler& e) {
        handleFecException(e);
        throw CCUException(e.getErrorMessage());
    };
}

bool FecManagerImpl::getFecRingSR0(unsigned int &status) {

    try {
        // Build the index for the given ring
        keyType index = buildFecRingKey(fec, ring);

        tscType16 fecSR0 = fecAccess_->getFecRingSR0(index);
        LOG4CPLUS_INFO(logger, "Value of the Status Register 0 of the FEC " << (int) getFecKey(index) << ", ring "
                << (int) getRingKey(index) << ": 0x" << hex << (int) fecSR0);
        if (fecSR0 & 0x1)
            LOG4CPLUS_INFO(logger, "\tFIFO transmit running");
        if (fecSR0 & 0x2)
            LOG4CPLUS_INFO(logger, "\tFIFO receive running");
        if (fecSR0 & 0x4)
            LOG4CPLUS_INFO(logger, "\tFIFO receive half full");
        if (fecSR0 & 0x8)
            LOG4CPLUS_INFO(logger, "\tFIFO receive full");
        if (fecSR0 & 0x10)
            LOG4CPLUS_INFO(logger, "\tFIFO receive empty");
        if (fecSR0 & 0x20)
            LOG4CPLUS_INFO(logger, "\tFIFO return half full");
        if (fecSR0 & 0x40)
            LOG4CPLUS_INFO(logger, "\tFIFO return full");
        if (fecSR0 & 0x80)
            LOG4CPLUS_INFO(logger, "\tFIFO return empty");
        if (fecSR0 & 0x100)
            LOG4CPLUS_INFO(logger, "\tFIFO transmit half full");
        if (fecSR0 & 0x200)
            LOG4CPLUS_INFO(logger, "\tFIFO transmit full");
        if (fecSR0 & 0x400)
            LOG4CPLUS_INFO(logger, "\tFIFO transmit empty");
        if (fecSR0 & 0x800)
            LOG4CPLUS_INFO(logger, "\tLink initialise");
        if (fecSR0 & 0x1000)
            LOG4CPLUS_INFO(logger, "\tPending irg");
        if (fecSR0 & 0x2000)
            LOG4CPLUS_INFO(logger, "\tData to FEC");
        if (fecSR0 & 0x4000)
            LOG4CPLUS_INFO(logger, "\tTTCRx ok");
        status = (unsigned int) fecSR0;
        return (status == GOOD_FEC_STATUS1) || (status == GOOD_FEC_STATUS2) || (status == GOOD_FEC_STATUS3);
    } catch (FecExceptionHandler &e) {
        LOG4CPLUS_ERROR(logger, e.what());
        handleFecException(e);
        throw CCUException(e.getErrorMessage());
    }
}
/**
 * THIS IS A COPY TAKEN FROM 'APIAccess.cc':
 *
 * This method try to find all the device driver loaded for FECs and reset all PLX
 * and FECs
 * <p>command: -reset
 * \param fecAccess_ - FEC access object
 * \param fec - FEC slot to be reseted
 * \param ring - ring to be reseted
 * \warning the loop is used only for the FEC reset it self
 */

    bool FecManagerImpl::resetPlxFec(unsigned int &status, bool vmeHardInit){

    int nLoops = 3; // repeat up to 3 times
    unsigned long usDelay = 500000; // wait 0.5 sec

    LOG4CPLUS_DEBUG(logger, "resetPlxFec: start (fec=" << fec << " ring=" << ring << " nLoops=" << nLoops
            << " usDelay=" << usDelay << ")");

    try {
        LOG4CPLUS_INFO(logger, __FUNCTION__<<": vmeHardInit="<<vmeHardInit);
        // vmeHardInit flag works only for VME FEC type (not PCI one)
        // this slows down reset FEC, but makes sure that it is
        // fully initialized (e.g. after LBB power cycling)
       if(vmeHardInit) {
            LOG4CPLUS_DEBUG(logger, "resetPlxFec: Full reinitialization of FEC access - started");
            init(FEC_TYPE_VME, pciSlot, fec, ring, true);
            LOG4CPLUS_DEBUG(logger, "resetPlxFec: Full reinitialization of FEC access - done");
        }

        // Build the index for the given ring
        keyType index = buildFecRingKey(fec, ring);

        // Make a reset
        //fecAccess->fecHardReset(index) ;

#ifdef TTCRx
        LOG4CPLUS_DEBUG(logger, "resetPlxFec: initTTCRx" );
        fecAccess_->initTTCRx(index);
#endif

        // Wait for any alarms
        bool result = false;
        for (int loop = 0; loop < nLoops; loop++) {
            // Ring A
            fecAccess_->fecRingReset(index);
            LOG4CPLUS_DEBUG(logger, "resetPlxFec: reset ring A (loop=" << loop << ")");

            // Ring B
            fecAccess_->fecRingResetB(index);
            LOG4CPLUS_DEBUG(logger, "resetPlxFec: reset ring B (loop=" << loop << ")");

            // Reset the FSM machine
            LOG4CPLUS_DEBUG(logger, "resetPlxFec: resetting FSM machine (loop=" << loop << ")");
            fecAccess_->fecRingResetFSM(index);
	    // Filip - 20141212
	    usleep(5000);
            result = getFecRingSR0(status);
            LOG4CPLUS_DEBUG(logger, "resetPlxFec: status 0x" << hex << status << std::dec << " (loop=" << loop
                    << ")");

            // Wait
            if (result)
                break;
            if ((usDelay > 0) && (nLoops > 1))
                usleep(usDelay);
        }

        // mpietrus: reinitialize FEC
        for (LinkBoxAccessList::iterator iLBA = linkBoxAccessList.begin(); iLBA != linkBoxAccessList.end(); iLBA++) {
            StdLinkBoxAccessImpl* impl = dynamic_cast<StdLinkBoxAccessImpl*> (*iLBA);
            impl->initFec();
        }
        LOG4CPLUS_DEBUG(logger, "resetPlxFec: end (fec=" << fec << " ring=" << ring << " nLoops=" << nLoops
                << " usDelay=" << usDelay << ")");
        return result;
    } catch (FecExceptionHandler &e) {
        LOG4CPLUS_ERROR(logger, e.what());
        handleFecException(e);
        throw CCUException(e.getErrorMessage());
    }
}

void FecManagerImpl::resetPlxFecForAutoRecovery() {
	try {
	    sleep(1); //wait 1s, maybe the CCU error will go out
	    unsigned int  status = 0;
	    bool isGood = resetPlxFec(status, false);
	    if (!isGood) {
	        stringstream msg;
	        msg << __FUNCTION__ << ": Bad FEC status: 0x" << hex << status;
	        throw CCUException(msg.str());
	    }
	    resetFecAutoRecoveryGoodCnt_++;
	    LOG4CPLUS_WARN(logger, __FUNCTION__<<" done successfully");
	} catch (CCUException& e) {
        resetFecAutoRecoveryBadCnt_++;
        throw CCUException(e.what());
	}
}

void FecManagerImpl::resetGetResetFecAutoRecoveryCnt() {
    resetFecAutoRecoveryBadCnt_ = 0;
    resetFecAutoRecoveryGoodCnt_ = 0;
    resetFecAutoRecoveryStartTime_ = time(NULL);
}

void FecManagerImpl::StdLinkBoxAccessImpl::ccuExternalReset() {
    try {
        fecManager.fecAccess_->setCcuCRA(CCUKey, 0xa0);
        sleep(1);
    } catch (FecExceptionHandler& e) {
        fecManager.handleFecException(e);
        throw CCUException(e.getErrorMessage());
    };
}

void FecManagerImpl::StdLinkBoxAccessImpl::setBlockTransfer(bool value) {
    try {
        // rozpoczecie transferu blokowego D <<< fe (bit 2 na 1)
        // zakonczenie D << fc (bit 2 na 0)
        if (BlockTransfer != value) {
            LOG4CPLUS_DEBUG(logger, (value ? "Changing to block transfer" : "Changing to single transfer"));
            fecManager.fecAccess_->setMemoryChannelCRA(CCUMemoryKey, value ? MEM_CRA_BLOCK : MEM_CRA_SINGLE);
            BlockTransfer = value;
        }
    } catch (FecExceptionHandler& e) {
        fecManager.handleFecException(e);
        throw CCUException(e.getErrorMessage());
    };
}

void FecManagerImpl::StdLinkBoxAccessImpl::selectLbc(int lbc) {
    uint32_t base = 0x7000 + lbc * 0x100;
    if (base != LBCBase) {
        LBCBase = base;
        /*CB_LSTAT = LBCBase + 0x08;
         CB_INTERF = LBCBase + 0x0d;
         CB_BLCK_BUF = LBCBase + 0x80;
         CB_XLNXD = LBCBase + 0x10;
         CB_XLNXC = LBCBase + 0x11;*/
        CB_PAGE = LBCBase + LBC::LBC_PAGE;
    }
}

void FecManagerImpl::StdLinkBoxAccessImpl::selectPage(int lb, uint32_t address) {
    LOG4CPLUS_DEBUG(logger, "selectPage " << lb << " 0x" << hex << address);
    selectLbc(lb);
    /*TPageMap::iterator iPage = PageMap.find(lb);

     if (iPage == PageMap.end()) {
     PageMap.insert(TPageMap::value_type(lb, address));
     MemoryWrite16(CB_PAGE, address);
     }
     else if ((address & 0xf800) != (iPage->second & 0xf800)) {
     MemoryWrite16(CB_PAGE, address);
     iPage->second = address;
     if (G_CCUDebug)
     cout << "Changing page for lb " << lb << " address " << (address & 0xf800) << endl;
     }*/
    memoryWrite16(CB_PAGE, address);
}

void FecManagerImpl::StdLinkBoxAccessImpl::memoryWrite(uint32_t address, unsigned short data) {
    setBlockTransfer(false);
    LOG4CPLUS_DEBUG(logger, hex << "w 0x" << address << " 0x" << data);

    try {
        tscType16 ah = (address & 0xFF00) >> 8;
        tscType16 al = (address & 0xFF);
        fecManager.fecAccess_->write(CCUMemoryKey, ah, al, data);
    } catch (FecExceptionHandler& e) {
        fecManager.handleFecException(e);
        throw CCUException(e.getErrorMessage());
    };
}

unsigned short FecManagerImpl::StdLinkBoxAccessImpl::memoryRead(uint32_t address) {
    setBlockTransfer(false);
    LOG4CPLUS_DEBUG(logger, hex << "r 0x" << address << " = ");

    try {
        tscType16 ah = (address & 0xFF00) >> 8;
        tscType16 al = (address & 0xFF);
        tscType16 result = fecManager.fecAccess_->read(CCUMemoryKey, ah, al);
        LOG4CPLUS_DEBUG(logger, "0x" << hex << result);

        return result;
    } catch (FecExceptionHandler& e) {
        fecManager.handleFecException(e);
        throw CCUException(e.getErrorMessage());
    };
}

void FecManagerImpl::StdLinkBoxAccessImpl::memoryWriteBlock(uint32_t address, unsigned char* data,
        uint32_t length) {
    setBlockTransfer(true);
    tscType16 ah;
    tscType16 al;
    uint32_t len;
    LOG4CPLUS_DEBUG(logger, "wr address 0x" << hex << address << " length " << dec << length);

    try {
        for (int i = 0; length > 0; i += MAX_BLOCK_SIZE) {
            ah = ((address + i) & 0xFF00) >> 8;
            al = ((address + i) & 0xFF);
            len = std::min(length, MAX_BLOCK_SIZE);
            fecManager.fecAccess_->write(CCUMemoryKey, ah, al, data + i, len);
            length -= len;
        }
    } catch (FecExceptionHandler& e) {
        fecManager.handleFecException(e);
        throw CCUException(e.getErrorMessage());
    };
}

void FecManagerImpl::StdLinkBoxAccessImpl::memoryReadBlock(uint32_t address, unsigned char* data,
        uint32_t length) {
    setBlockTransfer(true);
    LOG4CPLUS_DEBUG(logger, "rb 0x" << hex << address << " " << dec << length << "=");

    try {
        tscType16 ah;
        tscType16 al;
        uint32_t len;
        for (int i = 0; length > 0; i += MAX_BLOCK_SIZE) {
            ah = ((address + i) & 0xFF00) >> 8;
            al = ((address + i) & 0xFF);
            //fecManager.fecAccess_->read(MemoryKey, ah, al, length, data);
            len = std::min(length, MAX_BLOCK_SIZE);
            fecManager.fecAccess_->read(CCUMemoryKey, ah, al, len, data + i);
            length -= len;
        }
    } catch (FecExceptionHandler& e) {
        fecManager.handleFecException(e);
        throw CCUException(e.getErrorMessage());
    };

    for (uint32_t i = 0; i < length; i++) {
        LOG4CPLUS_DEBUG(logger, "  [" << i << "]=0x" << hex << (int)data[i]);
    }
}

void FecManagerImpl::StdLinkBoxAccessImpl::memoryRead16Block(uint32_t address, unsigned short* data,
        uint32_t length) {
    tscType8* buffer = new tscType8[MAX_BLOCK_SIZE];
    try {
        const uint32_t maxLength = MAX_BLOCK_SIZE / 2 - 1;
        int words;
        for (int i = 0; length > 0; i += maxLength) {
            words = std::min(length, maxLength);
            startBlockTransfer16();
            memoryReadBlock((address + i) * 2, buffer, words * 2 + 2);
            endBlockTransfer16();

            unsigned short temp, temp1;
            for (int word = 0; word < words; word++) {
                temp = buffer[word * 2 + 2];
                temp1 = buffer[word * 2 + 3];
                data[i + word] = (temp & 0xFF) | ((temp1 << 8) & 0xFF00);
            }

            length -= words;
        }
    } catch (...) {
        delete[] buffer;
        throw ;
    }

    delete [] buffer;
}

void FecManagerImpl::StdLinkBoxAccessImpl::memoryWrite16Block(uint32_t address, unsigned short* data,
        uint32_t length)
{
    LOG4CPLUS_DEBUG(logger, "wr16B address 0x" << hex << address << " length " << dec << length);
    tscType8* buffer = new tscType8[MAX_BLOCK_SIZE];
    try {
        const uint32_t maxLength = MAX_BLOCK_SIZE / 2 - 1;
        int words;
        for (int i = 0; length> 0; i += maxLength) {
            words = std::min(length, maxLength);
            for (int word = 0; word < words; word++) {
                buffer[word*2] = data[i + word] & 0xff;
                buffer[word*2+1] = (data[i + word] >> 8) & 0xff;
            }
            buffer[words*2] = 0;

            startBlockTransfer16();
            memoryWriteBlock((address + i) * 2, buffer, words * 2 + 1);
            endBlockTransfer16();

            length -= words;
        }
    }
    catch(...) {
        delete [] buffer;
        throw;
    }

    delete [] buffer;
}

void FecManagerImpl::StdLinkBoxAccessImpl::memoryWrite16(uint32_t address, unsigned short data)
{
    setBlockTransfer(false);
    address *= 2;
    unsigned int data1 = (data & 0xFF);
    memoryWrite(address, (DD_TYPE_FEC_DATA16)data1);
    address += 1;
    data1 = (data >> 8) & 0xFF;
    memoryWrite(address, (DD_TYPE_FEC_DATA16)data1);
}

unsigned short FecManagerImpl::StdLinkBoxAccessImpl::memoryRead16(uint32_t address)
{
    setBlockTransfer(false);
    address *= 2;
    unsigned int data = memoryRead(address);
    address += 1;
    unsigned int data1 = memoryRead(address);
    data = (data & 0xFF) | ((data1 << 8) & 0xFF00);
    return data;
}

/*

 static FILE* flashDump = NULL;

 void FecManagerImpl::StdLinkBoxAccessImpl::writeFlash(uint32_t address, uint32_t data)
 {

 if (flashDump != NULL) {
 fwrite(&data, sizeof(data), 1, flashDump);
 }
 // send data and address
 tscType8 dataaddr[7];
 dataaddr[0] = data & 0xff;
 dataaddr[1] = (data >> 8) & 0xff;
 dataaddr[2] = (data >> 16) & 0xff;
 dataaddr[3] = (data >> 24) & 0xff;
 dataaddr[4] = address & 0xff;
 dataaddr[5] = (address >> 8) & 0xff;
 dataaddr[6] = (address >> 16) & 0xff;
 memoryWriteBlock(CBPC_FL1_DIL, dataaddr, 7);

 // start programming
 memoryWrite(CBPC_FL_CMD, 1);
 try {

 // wait until programming is finished
 int val;
 Timer.restart();
 do {
 val = memoryRead(CBPC_FL_STAT);
 if (Timer.elapsed() >= 1)
 throw CCUException(":  CBPC_FL_STAT bit 7 not set");
 } while (!(val & 0x80));

 // read data from flash and check if it is correct

 memoryWrite(CBPC_FL_CMD, 2);
 Timer.restart();
 do {
 val = memoryRead(CBPC_FL_STAT);
 if (Timer.elapsed() >= 1)
 throw CCUException(":  CBPC_FL_STAT bit 7 not set");
 } while (!(val & 0x80));

 memset(dataaddr, 0, sizeof(dataaddr));
 memoryReadBlock(CBPC_FL1_DIL, dataaddr, 4);
 uint32_t recdata = *((uint32_t*)dataaddr);
 if (data != recdata) {
 ostringstream ost;
 ost << "WriteFlash: writen and read values differ: received 0x"
 << hex << recdata << " expected " << data;
 throw CCUException(ost.str());
 }
 }
 catch(...) {
 memoryWrite(CBPC_FL_CMD, 0);
 }

 }

 void FecManagerImpl::StdLinkBoxAccessImpl::programFlash(const std::string& file)
 {
 FILE* fin = fopen(file.c_str(), "r");
 if (fin == NULL) {
 LOG4CPLUS_ERROR(logger, "Problems opening file '" + file + "'");
 throw CCUException("Problems opening file '" + file + "'");
 }
 flashDump = fopen("flush.dump", "w");

 uint32_t data;
 ssize_t bread = 1;
 int count = 0;
 while (bread != 0) {
 bread = fread(&data, sizeof(data), 1, fin);
 writeFlash(count, data);

 if ((count % 1000) == 0)
 cerr << " " << count << flush;
 count++;
 }
 cerr << "\nDone" << endl;

 fclose(fin);
 fclose(flashDump);
 flashDump = NULL;
 }*/

/*
 void FecManagerImpl::ProgramFlash(const char* file1, const char* file2)
 {
 FILE* fin1 = fopen(file1, "r");
 if (fin1 == NULL)
 throw CCUException("Problems opening file '" + string(file1) + "'");

 FILE* fin2 = fopen(file2, "r");
 if (fin2 == NULL)
 throw CCUException("Problems opening file '" + string(file2) + "'");

 unsigned char data1;
 unsigned char data2;
 unsigned short value, rvalue;

 ssize_t b_read1 = 1, b_read2 = 1;
 int count1 = 0, count2 = 0, count = 0;
 FlashAF(0x0);
 while ((b_read1 != 0) || (b_read2 != 0)) {
 data1 = 0xff;
 data2 = 0xff;
 b_read1 = fread(&data1, 1, 1, fin1);
 count1 += b_read1;
 b_read2 = fread(&data2, 1, 1, fin2);
 count2 += b_read2;

 value = ((((int)data1 << 8) & 0xff00) | (((int)data2) & 0xff));
 FlashWF(value);
 for (int waitcount = 1; (memoryRead16(CB_FSTAT) && 0x8000); waitcount++) {
 if (waitcount == 5000) {
 cerr << "Problems with programming flash. Error" << endl;
 throw CCUException("Problems with programming flash.");
 }
 if ((waitcount % 1000) == 0) {
 cerr << "Problems with programming flash. Trying again." << endl;
 // reset flash controller and try once again
 FlashRSTF();
 FlashWF(value);
 }
 }

 rvalue = FlashRF();
 if (rvalue != value) {
 ostringstream ost;
 ost << hex << "Writen and read values differ: writen 0x" << value << "read 0x" << rvalue;
 cerr << ost.str() << endl;
 throw CCUException(ost.str());
 }

 FlashIF();

 if ((count++ % 1000) == 0)
 cerr << " " << count << flush;
 }
 if (count2 > count1)
 count1 = count2;
 memoryWrite16(CB_END1L, count1 & 0xffff);
 memoryWrite16(CB_END1H, (count1 >> 16) & 0xf);
 cerr << "\nDone" << endl;
 }*/

/*
 void FecManagerImpl::ProgramXilinx(const char* file1, const char* file2)
 {

 FILE* fin1 = fopen(file1, "r");
 if (fin1 == NULL)
 throw CCUException("Problems opening file '" + string(file1) + "'");

 FILE* fin2 = fopen(file2, "r");
 if (fin2 == NULL)
 throw CCUException("Problems opening file '" + string(file2) + "'");

 memoryWrite16(CB_XLNXC, 0xff);
 memoryWrite16(CB_INTERF, 0x08);

 try {
 // PROGRAM_A and PROGRAM_B down
 memoryWrite16(CB_XLNXC, 0xfc);
 // wait for INIT_A and INIT_B down
 for (int waitcount = 1; (memoryRead16(CB_LSTAT) & 0xa000) != 0; waitcount++) {
 if (waitcount == 10000) {
 cerr << "INIT signal did not go down" << endl;
 throw CCUException("INIT signal did not go down");
 }
 }

 // WRITE_A and WRITE_B down
 memoryWrite16(CB_XLNXC, 0xf0);

 // PROGRAM_A and PROGRAM_B up
 memoryWrite16(CB_XLNXC, 0xf3);
 // wait for INIT_A and INIT_B up
 for (int waitcount = 1; (memoryRead16(CB_LSTAT) & 0xa000) != 0xa000; waitcount++) {
 if (waitcount == 10000) {
 cerr << "INIT signal did not go up" << endl;
 throw CCUException("INIT signal did not go up");
 }
 }

 // CS_A and CS_B down
 memoryWrite16(CB_XLNXC, 0xC3);

 // start sending data
 unsigned char data1;
 unsigned char data2;
 unsigned short value;

 ssize_t b_read1 = 1, b_read2 = 1;
 int count1 = 0, count2 = 0, count = 0;
 FlashAF(0x0);
 while ((b_read1 != 0) || (b_read2 != 0)) {
 data1 = 0xff;
 data2 = 0xff;
 b_read1 = fread(&data1, 1, 1, fin1);
 count1 += b_read1;
 b_read2 = fread(&data2, 1, 1, fin2);
 count2 += b_read2;

 value = ((((int)data1 << 8) & 0xff00) | (((int)data2) & 0xff));
 memoryWrite16(CB_XLNXD, value);

 if ((count++ % 1000) == 0)
 cerr << " " << count << flush;
 }

 // generate 10 clocks
 for (int i=0; i < 10; i++)
 memoryWrite16(CB_XLNXD, 0xff);

 // CS_A, CS_B, WRITE_A and WRITE_B down
 memoryWrite16(CB_XLNXC, 0xff);

 if ((memoryRead16(CB_LSTAT) & 0x5000) != 0x5000)
 throw CCUException("DONE did not go up");

 } catch(...) {
 memoryWrite16(CB_INTERF, 0x00);
 throw;
 }
 memoryWrite16(CB_INTERF, 0x00);
 }
 */

/*void FecManagerImpl::ProgramXilinx(const char* file1, const char* file2)
 {

 FILE* fin1 = fopen(file1, "r");
 if (fin1 == NULL)
 throw CCUException("Problems opening file '" + string(file1) + "'");

 memoryWrite16(CB_XLNXC, 0xff);
 memoryWrite16(CB_INTERF, 0x08);

 try {
 // PROGRAM_A and PROGRAM_B down
 memoryWrite16(CB_XLNXC, 0xfc);
 // wait for INIT_A
 for (int waitcount = 1; (memoryRead16(CB_LSTAT) & 0x8000) != 0; waitcount++) {
 if (waitcount == 10000) {
 ostringstream ost;
 ost << "INIT signal did not go down: address 0x" << hex << CB_LSTAT;
 cerr << ost.str() << endl;
 throw CCUException(ost.str().c_str());
 }
 }

 // WRITE_A and WRITE_B down
 memoryWrite16(CB_XLNXC, 0xf0);

 // PROGRAM_A and PROGRAM_B up
 memoryWrite16(CB_XLNXC, 0xf3);
 // wait for INIT_A and INIT_B up
 for (int waitcount = 1; (memoryRead16(CB_LSTAT) & 0x8000) != 0x8000; waitcount++) {
 if (waitcount == 10000) {
 ostringstream ost;
 ost << "INIT signal did not go up: address 0x" << hex << CB_LSTAT;
 cerr << ost.str() << endl;
 throw CCUException(ost.str().c_str());
 }
 }

 // CS_A and CS_B down
 memoryWrite16(CB_XLNXC, 0xC3);

 // start sending data
 unsigned char data1;
 unsigned char data2;
 unsigned short value, readvalue;

 ssize_t b_read1 = 1, b_read2 = 1;
 int count1 = 0, count2 = 0, count = 0;
 FlashAF(0x0);

 // rozpoczecie transferu blokowego D <<< fe (bit 2 na 1)
 // zakonczenie D << fc (bit 2 na 0)
 // przestrzen 0xfe80 blokami

 const bool block = true;
 const int BlockSize = 128;
 unsigned short dataBlock[BlockSize];

 while ((b_read1 != 0) || (b_read2 != 0)) {
 data1 = 0xff;
 data2 = 0xff;
 b_read1 = fread(&data1, 1, 1, fin1);
 count1 += b_read1;
 b_read2 = 0; //fread(&data2, 1, 1, fin2);
 count2 += b_read2;

 if ((b_read1 == 0) && (b_read2 == 0))
 break;

 value = ((((int)data1 << 8) & 0xff00) | (((int)data2) & 0xff));
 memoryWrite16(CB_XLNXD, value);
 //readvalue = memoryRead16(CB_XLNXD);
 //if (readvalue != value) {
 //  ostringstream ostr;
 //  ostr << ": CB_XLNXD read 0x" << hex << readvalue << " expected 0x" << value;
 //  throw CCUException(ostr.str().c_str());
 //}

 if ((count++ % 1000) == 0)
 cerr << " " << count << flush;
 }

 // generate 10 clocks
 for (int i=0; i < 20; i++)
 memoryWrite16(CB_XLNXD, 0xffff);

 // CS_A, CS_B, WRITE_A and WRITE_B down
 memoryWrite16(CB_XLNXC, 0xff);

 if ((memoryRead16(CB_LSTAT) & 0x4000) != 0x4000)
 throw CCUException("DONE did not go up");

 } catch(...) {
 memoryWrite16(CB_INTERF, 0x00);
 throw;
 }
 memoryWrite16(CB_INTERF, 0x00);
 } */

/*
 void FecManagerImpl::StdLinkBoxAccessImpl::programXilinx(const char* file)
 {
 FILE* fin1 = fopen(file, "r");
 if (fin1 == NULL)
 throw CCUException("Problems opening file '" + string(file) + "'");

 memoryWrite16(LBCBase + LBC::LBC_XLNXC, 0xff);
 memoryWrite16(LBCBase + LBC::LBC_INTERF, 0x08);

 try {
 // PROGRAM_A and PROGRAM_B down
 memoryWrite16(LBCBase + LBC::LBC_XLNXC, 0xfc);
 // wait for INIT_A
 for (int waitcount = 1; (memoryRead16(LBCBase + LBC::LBC_LSTAT) & 0x8000) != 0; waitcount++) {
 if (waitcount == 10000) {
 ostringstream ost;
 ost << "INIT signal did not go down: address 0x" << hex << (LBCBase + LBC::LBC_LSTAT);
 LOG4CPLUS_ERROR(logger, ost.str());
 throw CCUException(ost.str().c_str());
 }
 }

 // WRITE_A and WRITE_B down
 memoryWrite16(LBCBase + LBC::LBC_XLNXC, 0xf0);

 // PROGRAM_A and PROGRAM_B up
 memoryWrite16(LBCBase + LBC::LBC_XLNXC, 0xf3);
 // wait for INIT_A and INIT_B up
 for (int waitcount = 1; (memoryRead16(LBCBase + LBC::LBC_LSTAT) & 0x8000) != 0x8000; waitcount++) {
 if (waitcount == 10000) {
 ostringstream ost;
 ost << "INIT signal did not go up: address 0x" << hex << (LBCBase + LBC::LBC_LSTAT);
 LOG4CPLUS_ERROR(logger, ost.str());
 throw CCUException(ost.str().c_str());
 }
 }

 // CS_A and CS_B down
 memoryWrite16(LBCBase + LBC::LBC_XLNXC, 0xC3);

 // start sending data
 unsigned char data1;
 unsigned char data2;
 //unsigned short value, readvalue;

 ssize_t b_read1 = 1, b_read2 = 1;
 int count1 = 0, count2 = 0, count = 0;
 //FlashAF(0x0);

 // przestrzen 0xfe80 blokami

 const bool block = true;
 const int BlockSize = 32;
 int blockIdx = 0;
 unsigned char dataBlock[BlockSize * 2 + 1];

 while ((b_read1 != 0) || (b_read2 != 0)) {
 data1 = 0xff;
 data2 = 0xff;
 b_read1 = fread(&data1, 1, 1, fin1);
 count1 += b_read1;
 b_read2 = 0; //fread(&data2, 1, 1, fin2);
 count2 += b_read2;

 if ((b_read1 == 0) && (b_read2 == 0))
 break;

 if (block) {
 dataBlock[blockIdx] = data2;
 dataBlock[blockIdx+1] = data1;
 blockIdx+=2;
 if (blockIdx == (BlockSize*2)) {
 dataBlock[blockIdx] = 0;
 startBlockTransfer16();
 memoryWriteBlock(2* (LBCBase + LBC::LBC_BLCK_BUF), dataBlock, blockIdx+1);
 endBlockTransfer16();
 blockIdx = 0;
 }
 }
 else {
 memoryWrite16(LBCBase + LBC::LBC_XLNXD, ((((int)data1 << 8) & 0xff00) | (((int)data2) & 0xff)));
 };

 if ((count++ % 1000) == 0)
 cerr << " " << count << flush;
 }

 if (block && (blockIdx != 0)) {
 dataBlock[blockIdx] = 0;
 startBlockTransfer16();
 memoryWriteBlock(2 * (LBCBase + LBC::LBC_BLCK_BUF), dataBlock, blockIdx+1);
 endBlockTransfer16();
 blockIdx = 0;
 }

 // generate 10 clocks
 for (int i=0; i < 20; i++)
 memoryWrite16(LBCBase + LBC::LBC_XLNXD, 0xffff);

 // CS_A, CS_B, WRITE_A and WRITE_B down
 memoryWrite16(LBCBase + LBC::LBC_XLNXC, 0xff);

 if ((memoryRead16(LBCBase + LBC::LBC_LSTAT) & 0x4000) != 0x4000) {
 LOG4CPLUS_ERROR(logger, "DONE did not go up");
 throw CCUException("DONE did not go up");
 }

 } catch(...) {
 memoryWrite16(LBCBase + LBC::LBC_INTERF, 0x00);
 throw;
 }
 memoryWrite16(LBCBase + LBC::LBC_INTERF, 0x00);
 }


 void FecManagerImpl::StdLinkBoxAccessImpl::programCbpc(const std::string& file1)
 {

 LOG4CPLUS_DEBUG(logger, "ProgramCBPC " << file1);
 int fin1 = open(file1.c_str(), O_RDONLY);
 if (fin1 == -1)
 throw CCUException("Problems opening file '" + string(file1) + "'");

 //memoryWrite16(CB_PAGE, 0);
 memoryWrite(CBIC_W0, 0xff);
 piaWrite(1, 0xfa);
 usleep(10);
 piaWrite(1, 0xfe);
 usleep(10);
 memoryWrite(CBIC_W0, 0xf7);

 int val;
 Timer.restart();
 do {
 val = memoryRead(CBIC_R0);
 if (Timer.elapsed() > 3)
 throw CCUException(":  EMG_INIT still up after 3 seconds");
 } while (val & EMG_INIT);


 memoryWrite(CBIC_W0, 0xff);
 Timer.restart();
 do {
 val = memoryRead(CBIC_R0);
 if (Timer.elapsed() > 3)
 throw CCUException(":  EMG_INIT still down after 3 seconds");
 } while (!(val & EMG_INIT));

 memoryWrite(CBIC_W0, 0xfc);


 const bool block = true;
 const int BlockSize = 64;
 int blockIdx = 0;
 unsigned char dataBlock[BlockSize];

 // start sending data
 unsigned char data1;
 ssize_t b_read1 = 1;
 for (int i=0; b_read1 == 1; i++) {
 b_read1 = read(fin1, &data1, 1);
 if (b_read1 == 1) {
 if (block) {
 dataBlock[blockIdx] = data1;
 blockIdx++;
 if (blockIdx == BlockSize) {
 memoryWriteBlock(0xff00, dataBlock, blockIdx);
 blockIdx = 0;
 }
 }
 else {
 memoryWrite(0xff00, data1);
 };

 if ((i % 10000) == 0) {
 LOG4CPLUS_DEBUG(logger, i);
 }
 }
 }

 if (block && (blockIdx != 0)) {
 memoryWriteBlock(0xff00, dataBlock, blockIdx);
 blockIdx = 0;
 }

 close(fin1);

 // generate 10 clocks
 for (int i=0; i < 10; i++)
 memoryWrite(0xff00, 0);

 memoryWrite(CBIC_W0, 0xff);
 if (!(memoryRead(CBIC_R0) & EMG_DONE))
 throw CCUException(": emg_DONE is down");

 }

 void FecManagerImpl::StdLinkBoxAccessImpl::programLbc(const std::string& file1)
 {

 LOG4CPLUS_DEBUG(logger, "ProgramLBC " << file1);
 int fin1 = open(file1.c_str(), O_RDONLY);
 if (fin1 == -1)
 throw CCUException("Problems opening file '" + string(file1) + "'");

 //memoryWrite16(CB_PAGE, 0);
 memoryWrite(CBIC_W0, 0xff);
 piaWrite(1, 0xf9);
 usleep(10);
 piaWrite(1, 0xfd);
 usleep(10);
 memoryWrite(CBIC_W0, 0xf7);

 int val;
 Timer.restart();
 do {
 val = memoryRead(CBIC_R0);
 if (Timer.elapsed() > 3)
 throw CCUException(":  EMG_INIT still up after 3 seconds");
 } while (val & EMG_INIT);


 memoryWrite(CBIC_W0, 0xff);
 Timer.restart();
 do {
 val = memoryRead(CBIC_R0);
 if (Timer.elapsed() > 3)
 throw CCUException(":  EMG_INIT still down after 3 seconds");
 } while (!(val & EMG_INIT));

 memoryWrite(CBIC_W0, 0xfc);

 const bool block = true;
 const int BlockSize = 64;
 int blockIdx = 0;
 unsigned char dataBlock[BlockSize];

 // start sending data
 unsigned char data1;
 ssize_t b_read1 = 1;
 for (int i=0; b_read1 == 1; i++) {
 b_read1 = read(fin1, &data1, 1);
 if (b_read1 == 1) {
 if (block) {
 dataBlock[blockIdx] = data1;
 blockIdx++;
 if (blockIdx == BlockSize) {
 memoryWriteBlock(0xff00, dataBlock, blockIdx);
 blockIdx = 0;
 }
 }
 else {
 memoryWrite(0xff00, data1);
 };

 if ((i % 10000) == 0) {
 LOG4CPLUS_DEBUG(logger, i);
 }
 }
 }

 if (block && (blockIdx != 0)) {
 memoryWriteBlock(0xff00, dataBlock, blockIdx);
 blockIdx = 0;
 }

 close(fin1);

 // generate 10 clocks
 for (int i=0; i < 10; i++)
 memoryWrite(0xff00, 0);

 memoryWrite(CBIC_W0, 0xff);
 if (!(memoryRead(CBIC_R0) & EMG_DONE)) {
 throw CCUException(": emg_DONE is down");
 }

 }
 */

unsigned char FecManagerImpl::StdLinkBoxAccessImpl::piaRead(int channel) {
    //return PIA[channel]->read();
    try {
        return fecManager.fecAccess_->getPiaChannelDataReg(PIAKeys[channel]);
    }
    catch (FecExceptionHandler& e) {
        fecManager.handleFecException(e);
        throw CCUException(e.getErrorMessage());
    };
}

void FecManagerImpl::StdLinkBoxAccessImpl::piaWriteBase(int channel, unsigned char value) {
    // same value is sent twice in order to handle both old (1.x) and new (>=2.x)
    // CBIC firmware versions.
    if(1==channel) {
        memoryWrite(CBIC::CBIC_PB, value);
    } else if(3==channel) {
        memoryWrite(CBPC::CBPC_PD, value);  // 18.09.2009 - fix needed for new CB firmware by WZab
	}
    //PIA[channel]->write(value);
    try {
        fecManager.fecAccess_->setPiaChannelDataReg(PIAKeys[channel], value);
    }
    catch (FecExceptionHandler& e) {
        fecManager.handleFecException(e);
        throw CCUException(e.getErrorMessage());
    };
}

void FecManagerImpl::StdLinkBoxAccessImpl::piaWrite(int channel, unsigned char value) {
    try {
        piaWriteBase(channel, value);
    }
    catch (CCUException& e) {
        usleep(100000);
        // try once again
        LOG4CPLUS_WARN(logger, "CCUException during StdLinkBoxAccessImpl::piaWrite for channel " << channel << " reseting FEC and trying once again");
        getFecManager().resetPlxFecForAutoRecovery();
        piaWriteBase(channel, value);
    }
    catch (...) {//for some unknown reasons the above catch sometimes do not work, therefore trying this (KB)
        usleep(100000);
        // try once again
        LOG4CPLUS_WARN(logger, "unknown exception during StdLinkBoxAccessImpl::piaWrite for channel " << channel << " reseting FEC and trying once again");
        getFecManager().resetPlxFecForAutoRecovery();
        piaWriteBase(channel, value);
    }
}

void FecManagerImpl::StdLinkBoxAccessImpl::lbWrite(int lb, uint32_t address, uint32_t data)
{
    selectPage(lb, address);
    memoryWrite16(lb * LB_ADDRESS_SPACE_SIZE + (address & II_PAGE_MASK), data);
}

uint32_t FecManagerImpl::StdLinkBoxAccessImpl::lbRead(int lb, uint32_t address)
{
    selectPage(lb, address);
    return memoryRead16(lb * LB_ADDRESS_SPACE_SIZE + (address & II_PAGE_MASK));
}

void FecManagerImpl::StdLinkBoxAccessImpl::lbWriteBlock(int lb, uint32_t address,
        unsigned short* data, uint32_t count)
{
    selectPage(lb, address);
    memoryWrite16Block(lb * LB_ADDRESS_SPACE_SIZE + (address & II_PAGE_MASK), data, count);
}

void FecManagerImpl::StdLinkBoxAccessImpl::lbReadBlock(int lb, uint32_t address,
        unsigned short* data, uint32_t count)
{
    selectPage(lb, address);
    memoryRead16Block(lb * LB_ADDRESS_SPACE_SIZE + (address & II_PAGE_MASK), data, count);
}

// Auxilary method for loading either CBPC or LBC chip via CCU using
// given firmware- and initialization files.
void FecManagerImpl::StdLinkBoxAccessImpl::loadLbc(const std::string& file1, const std::string& file2)
{
    const int USLEEP_DELAY = 1;

    LOG4CPLUS_DEBUG(logger, "loadLbc " << file1 << " " << file2);
    int fin1 = open(file1.c_str(), O_RDONLY);
    if (fin1 == -1) {
        LOG4CPLUS_ERROR(logger, "Cannot open file '" << file1 << "': " << strerror(errno));
        throw CCUException("Cannot open file '" + file1 + "': " + strerror(errno));
    }
    int fin2 = open(file2.c_str(), O_RDONLY);
    if (fin2 == -1) {
        LOG4CPLUS_ERROR(logger, "Cannot open file '" << file2 << "': " << strerror(errno));
        throw CCUException("Cannot open file '" + file2 + "': " + strerror(errno));
    }

    // Set nPROGRAM to 0 and wait until INIT becomes 0
    w0Set(bitVal(CBIC::CBIC_W0_EMG_nCS) | bitVal(CBIC::CBIC_W0_EMG_nWRITE) | bitVal(CBIC::CBIC_W0_EMG_RESET)
            | bitVal(CBIC::CBIC_W0_EMG_nPROGRAM) | bitVal(CBIC::CBIC_W0_RUN));
    usleep(1000000);
    // Reset should be disabled
    w0Clr(bitVal(CBIC::CBIC_W0_EMG_RESET));
    w0Clr(bitVal(CBIC::CBIC_W0_EMG_nPROGRAM));
    LOG4CPLUS_DEBUG(logger, "Waiting for CBIC_R0_EMG_INIT=0");
    Timer.restart();
    while (true) {
        // lbx.ccu_write8(cbic.CBIC_BLK_BUF,0) #Generate clock
        if (! (memoryRead(CBIC::CBIC_R0) & bitVal(CBIC::CBIC_R0_EMG_INIT))) {
            LOG4CPLUS_DEBUG(logger, "Confirmed CBIC_R0_EMG_INIT=0");
            break;
        }
        if (Timer.elapsed()> 3) {
            LOG4CPLUS_ERROR(logger, "CBIC_R0_EMG_INIT still up after 3 seconds");
            throw CCUException("CBIC_R0_EMG_INIT still up after 3 seconds");
        }
        usleep(USLEEP_DELAY);
    }

    // Set nPROGRAM to 1 and wait until INIT becomes 1
    w0Set(bitVal(CBIC::CBIC_W0_EMG_nPROGRAM));
    LOG4CPLUS_DEBUG(logger, "Waiting for CBIC_R0_EMG_INIT=1");
    Timer.restart();
    while (true) {
        // lbx.ccu_write8(cbic.CBIC_BLK_BUF,0) #Generate clock
        if (memoryRead(CBIC::CBIC_R0) & bitVal(CBIC::CBIC_R0_EMG_INIT)) {
            LOG4CPLUS_DEBUG(logger, "Confirmed CBIC_R0_EMG_INIT=1");
            break;
        }
        if (Timer.elapsed()> 3) {
            LOG4CPLUS_ERROR(logger, "CBIC_R0_EMG_INIT still down after 3 seconds");
            throw CCUException("CBIC_R0_EMG_INIT still down after 3 seconds");
        }
        usleep(USLEEP_DELAY);
    }
    LOG4CPLUS_DEBUG(logger, "CBIC_R0: 0x" << hex << (int)memoryRead(CBIC::CBIC_R0));
    // Set  nCS to 0 and nWRITE to 0
    w0Clr(bitVal(CBIC::CBIC_W0_EMG_nCS) | bitVal(CBIC::CBIC_W0_EMG_nWRITE));

    // Sending subsequent configuration bytes using block transfer
    const int buflen = 80;
    unsigned char buf[buflen] = {0};
    ssize_t readnum = 0;
    LOG4CPLUS_DEBUG(logger, "Sending configuration");
    while (true) {
        readnum = read(fin1, buf, buflen);
        if (readnum < 0) {
            LOG4CPLUS_ERROR(logger, "Cannot read from file '" << file1 << "': " << strerror(errno));
            throw CCUException("Cannot read from file '" + file1 + "': " + strerror(errno));
        }
        if (readnum == 0) {
            LOG4CPLUS_DEBUG(logger, "Configuration sent");
            break;
        }
        memoryWriteBlock(CBIC::CBIC_BLK_BUF, buf, readnum);
    }

    LOG4CPLUS_DEBUG(logger, "Sending 10 additional clocks");
    // Sending 10 dummy bytes
    unsigned char dummyBytes[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    memoryWriteBlock(CBIC::CBIC_BLK_BUF, dummyBytes, sizeof(dummyBytes));

    // Set both nCS and nWRITE to 1
    w0Set(bitVal(CBIC::CBIC_W0_EMG_nCS) | bitVal(CBIC::CBIC_W0_EMG_nWRITE));

    // Wait until DONE becomes 1
    LOG4CPLUS_DEBUG(logger, "Waiting for CBIC_R0_EMG_DONE=1");
    while (true) {
        if (memoryRead(CBIC::CBIC_R0) & bitVal(CBIC::CBIC_R0_EMG_DONE)) {
            LOG4CPLUS_DEBUG(logger, "Confirmed CBIC_R0_EMG_DONE=1");
            break;
        }
        if (Timer.elapsed()> 3) {
            LOG4CPLUS_ERROR(logger, "CBIC_R0_EMG_DONE still down after 3 seconds");
            throw CCUException("CBIC_R0_EMG_DONE still down after 3 seconds");
        }
        usleep(USLEEP_DELAY);
    }
    // Set both nCS and nWRITE to 0
    w0Clr(bitVal(CBIC::CBIC_W0_EMG_nCS) | bitVal(CBIC::CBIC_W0_EMG_nWRITE));
    // Enable RESET signal
    w0Set(bitVal(CBIC::CBIC_W0_EMG_RESET));
    // Sending subsequent initialization bytes
    LOG4CPLUS_DEBUG(logger, "Sending initialization");
    while (true) {
        readnum = read(fin2, buf, buflen);
        if (readnum < 0) {
            LOG4CPLUS_ERROR(logger, "Cannot read from file '" << file2 << "': " << strerror(errno));
            throw CCUException("Cannot read from file '" + file2 + "': " + strerror(errno));
        }
        if (readnum == 0) {
            LOG4CPLUS_DEBUG(logger, "Initialization sent");
            break;
        }
        memoryWriteBlock(CBIC::CBIC_BLK_BUF, buf, readnum);
    }

    // Enable WRITE signal and proceed to normal operation...
    w0Set(bitVal(CBIC::CBIC_W0_EMG_nWRITE));
    close(fin1);
    close(fin2);
}

// Downloads LBC firmware and CBPC firmware to Control Board's FPGA via CCU.
// Binary files 'lbc.bin', 'lbc.ini', 'cbpc.bin', 'cbic.ini' are taken from $(HOME)/boot/LBB directory.
// This method calls internally loadLbc().
void FecManagerImpl::StdLinkBoxAccessImpl::loadCb(const std::string& cbpcBin, const std::string& cbicIni, const std::string& lbcBin, const std::string& lbcIni)
{
    const int USLEEP_DELAY = 1;

    LOG4CPLUS_INFO(logger, "loadCb " << cbpcBin << " " << cbicIni << " " << lbcBin << " " << lbcIni);

    w0Clr(bitVal(CBIC::CBIC_W0_RUN));
    piaWrite(1, 0xf0);

    // Prepare CBIC to program CBPC
    LOG4CPLUS_DEBUG(logger, "Configuring CBPC");
    w0Clr(bitVal(CBIC::CBIC_W0_RUN));
    piaWrite(1, 0xd2);
    piaWrite(1, 0xd6);
    w0Set(bitVal(CBIC::CBIC_W0_RUN));
    // Make sure Emergency CBPC configuration mode is turned ON
    Timer.restart();
    while (true) {
        if ( (memoryRead(CBIC::CBIC_R1) & 0x3) == 2 ) {
            LOG4CPLUS_DEBUG(logger, "EMG CBPC mode entered correctly!");
            break;
        }
        if (Timer.elapsed()> 3) {
            LOG4CPLUS_ERROR(logger, "CBIC didn't enter EMG CBPC mode after 3 seconds");
            throw CCUException("CBIC didn't enter EMG CBPC mode after 3 seconds");
        }
        usleep(USLEEP_DELAY);
    }
    loadLbc(cbpcBin, cbicIni);

    // Prepare CBIC to program LBC
    LOG4CPLUS_DEBUG(logger, "Configuring LBC");
    w0Clr(bitVal(CBIC::CBIC_W0_RUN));
    piaWrite(1, 0xd1);
    piaWrite(1, 0xd5);
    w0Set(bitVal(CBIC::CBIC_W0_RUN));
    // Make sure Emergency LBC configuration mode is turned ON
    Timer.restart();
    while (true) {
        if ( (memoryRead(CBIC::CBIC_R1) & 0x3) == 1 ) {
            LOG4CPLUS_DEBUG(logger, "EMG LBC mode entered correctly!");
            break;
        }
        if (Timer.elapsed()> 3) {
            LOG4CPLUS_ERROR(logger, "CBIC didn't enter EMG LBC mode after 3 seconds");
            throw CCUException("CBIC didn't enter EMG LBC mode after 3 seconds");
        }
        usleep(USLEEP_DELAY);
    }
    loadLbc(lbcBin, lbcIni);
    piaWrite(1, 0xec);
    piaWrite(3, 0xfc);
    w0Clr(bitVal(CBIC::CBIC_W0_RUN));
    // Reset the LBs
    piaWrite(3, 0xe4);
    piaWrite(3, 0xfc);
}

// Downloads KTP firmware to Link Board's FPGA via CCU (LB slot numbers are 0...8).
// Binary file 'cii_lb_std.bin' is taken from $(HOME)/boot/LBB directory.
void FecManagerImpl::StdLinkBoxAccessImpl::loadKtp(int lb, const std::string& ciiLbBin)
{
    const int USLEEP_DELAY = 1;

    LOG4CPLUS_INFO(logger, "loadKtp " << lb << " " << ciiLbBin);
    // perform sanity checks
    if (lb<0 || lb>8) {
        LOG4CPLUS_ERROR(logger, "LB slot number "<<lb<<" is outside of 0..8 range");
        throw CCUException("LB slot number "+rpct::toString(lb)+" is outside of 0..8 range");
    }
    int fin = open(ciiLbBin.c_str(), O_RDONLY);
    if (fin == -1) {
        LOG4CPLUS_ERROR(logger, "Cannot open file '" << ciiLbBin << "'" << strerror(errno));
        throw CCUException("Cannot open file '" + ciiLbBin + "':" + strerror(errno));
    }

    //prepare for programming
    selectLbc(lb);
    piaWrite(1, 0xec);
    piaWrite(3, 0xfc);
    // Enable KTP's Xilinx programming
    memoryWrite16(LBCBase+LBC::LBC_WRKMODE, (1 << LBC::LWM_VRFY_DIS ));
    memoryWrite16(LBCBase+LBC::LBC_INTERF, (1 << LBC::INT_XLNX ) | ( 1 << LBC::INT_JT ));
    // Set nPROGRAM to 0 and wait until INIT becomes 0
    memoryWrite16(LBCBase+LBC::LBC_XLNXC, (1 << LBC::XLX_APRG ) | (1 << LBC::XLX_AWRT) | (1 << LBC::XLX_ACS));
    memoryWrite16(LBCBase+LBC::LBC_XLNXC, (0 << LBC::XLX_APRG ) | (1 << LBC::XLX_AWRT) | (1 << LBC::XLX_ACS));
    // Reset should be disabled by now
    LOG4CPLUS_DEBUG(logger, "Waiting for LBC_XLX_INIT=0");
    Timer.restart();
    while (true) {
        if ((memoryRead16(LBCBase+LBC::LBC_LSTAT) & (1 << LBC::LBC_XLX_INIT)) == 0) {
            LOG4CPLUS_DEBUG(logger, "Confirmed LBC_XLX_INIT=0");
            break;
        }
        if (Timer.elapsed()> 3) {
            LOG4CPLUS_ERROR(logger, "LBC_XLX_INIT still up after 3 seconds");
            throw CCUException("LBC_XLX_INIT still up after 3 seconds");
        }
        usleep(USLEEP_DELAY);
    }
    // Set nPROGRAM to 1 and wait until INIT becomes 1
    memoryWrite16(LBCBase+LBC::LBC_XLNXC, ( 1 << LBC::XLX_APRG ) | (1 << LBC::XLX_AWRT) | (1 << LBC::XLX_ACS));
    LOG4CPLUS_DEBUG(logger, "Waiting for LBC_XLX_INIT=1");
    Timer.restart();
    while (true) {
        if ((memoryRead16(LBCBase+LBC::LBC_LSTAT) & (1 << LBC::LBC_XLX_INIT)) != 0) {
            LOG4CPLUS_DEBUG(logger, "Confirmed LBC_XLX_INIT=1");
            break;
        }
        if (Timer.elapsed()> 3) {
            LOG4CPLUS_ERROR(logger, "LBC_XLX_INIT still down after 3 seconds");
            throw CCUException("LBC_XLX_INIT still down after 3 seconds");
        }
        usleep(USLEEP_DELAY);
    }
    // Set nCS to 0 and nWRITE to 0
    memoryWrite16(LBCBase+LBC::LBC_XLNXC, (1 << LBC::XLX_APRG) | (0 << LBC::XLX_AWRT) | (0 << LBC::XLX_ACS));
    // Send subsequent configuration bytes using block transfer
    LOG4CPLUS_DEBUG(logger, "Sending configuration");
    unsigned char data8[40]={0};
    unsigned short data16[20];
    int nwords16=0;
    Timer.restart();
    ssize_t readnum=0;
    while(true) {
        readnum = read(fin, data8, 40);
        if (readnum == 0) {
            LOG4CPLUS_DEBUG(logger, "Sent "<<nwords16<<" 16-bit configuration words");
            break; // no more bytes to read
        }
        if (readnum < 0) {
            LOG4CPLUS_ERROR(logger, "Cannot read from file '" << ciiLbBin << "': " << strerror(errno));
            throw CCUException("Cannot read from file '" + ciiLbBin + "': " + strerror(errno));
        }
        // If readnum is odd, then add extra null byte
        if ( readnum % 2 ) {
            data8[readnum]=0;
            readnum++;
        }
        for (int i=0, j = 0; i<readnum; i+=2, j++) {
            data16[j] = *(unsigned short*)(data8 + i);
        }
        memoryWrite16Block(LBCBase+LBC::LBC_BLCK_BUF, data16, readnum / 2);
        nwords16 += (readnum/2);

        if (Timer.elapsed()> 30) {
            LOG4CPLUS_ERROR(logger, "Configuration not sent after 30 seconds");
            throw CCUException("Configuration not sent after 30 seconds");
        }
    }
    LOG4CPLUS_DEBUG(logger, "Sending 10 additional clocks");
    // Sending 10 dummy words
    unsigned short data16_dummy[10]= {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    memoryWrite16Block(LBCBase+LBC::LBC_BLCK_BUF, data16_dummy, 10);
    // Set both nCS and nWRITE to 1
    memoryWrite16(LBCBase+LBC::LBC_XLNXC, (1 << LBC::XLX_APRG ) | (1 << LBC::XLX_AWRT) | (1 << LBC::XLX_ACS));
    // Wait until DONE becomes 1
    LOG4CPLUS_DEBUG(logger, "Waiting for LBC_XLX_DONE=1");
    Timer.restart();
    while (true) {
        if (memoryRead16(LBCBase+LBC::LBC_LSTAT) & (1 << LBC::LBC_XLX_DONE)) {
            LOG4CPLUS_DEBUG(logger, "Confirmed LBC_XLX_DONE=1");
            break;
        }
        memoryWrite16Block(LBCBase+LBC::LBC_BLCK_BUF, data16_dummy, 10);
        if (Timer.elapsed()> 30) {
            LOG4CPLUS_ERROR(logger, "LBC_XLX_DONE still down after 3 seconds");
            throw CCUException("LBC_XLX_DONE still down after 3 seconds");
        }
    }
    close(fin);
    int val=memoryRead16(LBCBase+1);
    LOG4CPLUS_INFO(logger, "KTP_ID: 0x"<<hex<<val<<" is configured");
    piaWrite(1, 0xec);
    piaWrite(3, 0xfc);
}

// Auxilary
void FecManagerImpl::StdLinkBoxAccessImpl::setCbpcFlashAddr(uint32_t addr) {
    memoryWrite(CBPC::CBPC_FL_AB0, addr & 0xff);
    memoryWrite(CBPC::CBPC_FL_AB1, (addr >> 8) & 0xff);
    memoryWrite(CBPC::CBPC_FL_AB2, (addr >> 16) & 0xff);
}
// Auxilary
uint32_t FecManagerImpl::StdLinkBoxAccessImpl::readCbpcFlashAddr(uint32_t addr) {
    const int USLEEP_DELAY=1;
    setCbpcFlashAddr(addr);
    memoryWrite(CBPC::CBPC_FL_CMD, 0);
    memoryWrite(CBPC::CBPC_FL_CMD, (1<<CBPC::CFC_READ));
    Timer.restart();
    while(true) {
        if (memoryRead(CBPC::CBPC_FL_STAT) & 0x80) break;
        if(Timer.elapsed()> 3) {
            LOG4CPLUS_ERROR(logger, "(CBPC_FL_STAT & 0x80) still down after 3 seconds");
            throw CCUException("(CBPC_FL_STAT & 0x80) still down after 3 seconds");
        }
        usleep(USLEEP_DELAY);
    }
    uint32_t res = (uint32_t)memoryRead(CBPC::CBPC_FL1_DOL) |
              ((uint32_t)memoryRead(CBPC::CBPC_FL1_DOH) << 8) |
              ((uint32_t)memoryRead(CBPC::CBPC_FL2_DOL) << 16) |
              ((uint32_t)memoryRead(CBPC::CBPC_FL2_DOH) << 24);
    return res;
}

// Auxilary
void FecManagerImpl::StdLinkBoxAccessImpl::waitCbpcFlashDone() {
    const int USLEEP_DELAY=100;
    Timer.restart();
    while(true) {
        if (memoryRead(CBPC::CBPC_FL_STAT) & 0x80) break;
        if(Timer.elapsed()> 20) {
            LOG4CPLUS_ERROR(logger, "(CBPC_FL_STAT & 0x80) still down after 20 seconds");
            throw CCUException("(CBPC_FL_STAT & 0x80) still down after 20 seconds");
        }
        usleep(USLEEP_DELAY);
    }
}
// Auxilary
void FecManagerImpl::StdLinkBoxAccessImpl::resetCbpcFlash() {
    // Unlock the FLASH controller - added 18 Sep 2009
    // Needed for new CB firmware ver >=0x10e before reset sequence
    memoryWrite(CBPC::CBPC_FL_CMD, 0);
    memoryWrite(CBPC::CBPC_FL_CMD2, 0);
    memoryWrite(CBPC::CBPC_FL_ENABLE, 0xac);
    // Reset sequence before erasing - added 22 April 2008
    // The FLASH controller locks after trying to program a non-erased FLASH
    memoryWrite(CBPC::CBPC_FL_CMD, 0x20);
    memoryWrite(CBPC::CBPC_FL_CMD2, 2);
    usleep(500000); // 0.5 sec
    memoryWrite(CBPC::CBPC_FL_CMD, 0);
    memoryWrite(CBPC::CBPC_FL_CMD2,0);
    // Reset flash memory
    memoryWrite(CBPC::CBPC_FL_CMD, 0);
    memoryWrite(CBPC::CBPC_FL_CMD, (1<<CBPC::CFC_RESET));
    waitCbpcFlashDone();
    // Lock the FLASH controller again to prevent random corruption - added 18 Sep 2009
    // Needed for new CB firmware ver >=0x10e after reset sequence
    memoryWrite(CBPC::CBPC_FL_ENABLE ,0);
}

// Programs Control Board's CBPC and LBC flash memories with a given firmware file.
// Binary file 'cb_rom.bin' is taken from $(HOME)/boot/LBB directory.
// This method calls internally resetCbpcFlash(), readCbpcFlashAddr().
void FecManagerImpl::StdLinkBoxAccessImpl::programFlashCb(const std::string& cbromBin) {
    uint32_t val=0;
    const int USLEEP_DELAY=1;
    LOG4CPLUS_INFO(logger, "programFlashCb: Started");

    ///////////////////////////////////////
    val=memoryRead(CBPC::CBPC_FL_ENABLE);
    LOG4CPLUS_INFO(logger, "programFlashCb: before RESET : CBPC_FL_ENABLE=0x" << hex << val);
    //usleep(5000000);
    ///////////////////////////////////////

    // Reset sequence - added 22 April 2008
    LOG4CPLUS_DEBUG(logger, "programFlashCb: Reset sequence");
    resetCbpcFlash();
    // Check result
    val = readCbpcFlashAddr(0);
    LOG4CPLUS_DEBUG(logger, "programFlashCb: readCbpcFlash(addr=0)=0x" << hex << val);

    ///////////////////////////////////////
    val=memoryRead(CBPC::CBPC_FL_ENABLE);
    LOG4CPLUS_INFO(logger, "programFlashCb: after RESET, before UNLOCK : CBPC_FL_ENABLE=0x" << hex << val);
    //usleep(5000000);
    ///////////////////////////////////////

    // Unlock the FLASH controller - added 18 Sep 2009
    // Needed for new CB firmware ver >=0x10e before erase sequence
    LOG4CPLUS_DEBUG(logger, "programFlashCb: Unlock sequence");
    memoryWrite(CBPC::CBPC_FL_CMD, 0);
    memoryWrite(CBPC::CBPC_FL_CMD2, 0);
    memoryWrite(CBPC::CBPC_FL_ENABLE, 0xac);

    ///////////////////////////////////////
    val=memoryRead(CBPC::CBPC_FL_ENABLE);
    LOG4CPLUS_INFO(logger, "programFlashCb: after UNLOCK, before ERASE : CBPC_FL_ENABLE=0x" << hex << val);
    //usleep(5000000);
    ///////////////////////////////////////

    // Erase sequence
    LOG4CPLUS_DEBUG(logger, "programFlashCb: Erase sequence");
    memoryWrite(CBPC::CBPC_FL_CMD, 0);
    memoryWrite(CBPC::CBPC_FL_CMD, (1<<CBPC::CFC_CHIP_ERASE));
    waitCbpcFlashDone();
    // Lock the FLASH controller again to prevent random corruption - added 18 Sep 2009
    // Needed for new CB firmware ver >=0x10e after erase sequence
    LOG4CPLUS_DEBUG(logger, "programFlashCb: Lock sequence");
    memoryWrite(CBPC::CBPC_FL_ENABLE ,0);

    // Check result
    val = readCbpcFlashAddr(0);
    LOG4CPLUS_DEBUG(logger, "programFlashCb: readCbpcFlash(addr=0)=0x" << hex << val);

    ///////////////////////////////////////
    val=memoryRead(CBPC::CBPC_FL_ENABLE);
    LOG4CPLUS_INFO(logger, "programFlashCb: after ERASE, before RESET : CBPC_FL_ENABLE=0x" << hex << val);
    //usleep(5000000);
    ///////////////////////////////////////

    // Reset sequence - added 22 April 2008
    LOG4CPLUS_DEBUG(logger, "programFlashCb: Reset sequence");
    resetCbpcFlash();
    // Check result
    val = readCbpcFlashAddr(0x500);
    LOG4CPLUS_DEBUG(logger, "programFlashCb: readCbpcFlashAddr(addr=0x500)=0x" << hex << val);

    ///////////////////////////////////////
    val=memoryRead(CBPC::CBPC_FL_ENABLE);
    LOG4CPLUS_INFO(logger, "programFlashCb: after RESET, before UNLOCK : CBPC_FL_ENABLE=0x" << hex << val);
    //usleep(5000000);
    ///////////////////////////////////////

    // Programming FLASH with data read from file using block transfers
    LOG4CPLUS_DEBUG(logger, "programFlashCb " << cbromBin);
    int fin = open(cbromBin.c_str(), O_RDONLY);
    if (fin == -1) {
        LOG4CPLUS_ERROR(logger, "Cannot open file '" << cbromBin << "': " << strerror(errno));
        throw CCUException("Cannot open file '" + cbromBin + "': " + strerror(errno));
    }
    // Unlock the FLASH controller - added 18 Sep 2009
    // Needed for new CB firmware ver >=0x10e before program sequence
    LOG4CPLUS_DEBUG(logger, "programFlashCb: Unlock sequence");
    memoryWrite(CBPC::CBPC_FL_CMD, 0);
    memoryWrite(CBPC::CBPC_FL_CMD2, 0);
    memoryWrite(CBPC::CBPC_FL_ENABLE, 0xac);

    ///////////////////////////////////////
    val=memoryRead(CBPC::CBPC_FL_ENABLE);
    LOG4CPLUS_INFO(logger, "programFlashCb: after UNLOCK, before PROGRAM : CBPC_FL_ENABLE=0x" << hex << val);
    //usleep(5000000);
    ///////////////////////////////////////

    // Start programming FLASH
    LOG4CPLUS_DEBUG(logger, "programFlashCb: Program sequence");
    setCbpcFlashAddr(0);
    memoryWrite(CBPC::CBPC_FL_CMD, 0);
    memoryWrite(CBPC::CBPC_FL_CMD2, 0);
    memoryWrite(CBPC::CBPC_FL_CMD2, (1<<CBPC::CFC2_BLK_NEWADD));
    int n=0, m=0;
    bool eof=false;
    // Read subsequent blocks of 120-word size (1 word = 4 bytes)
    while(true) {
        int ntotal=0;
        unsigned char data[120]={0};
        while(true) {
            unsigned char buf[4]={0, 0, 0, 0};
            ssize_t nread = read(fin, buf, 4);
            if (nread!=4) {
                eof=true;
                break;
            }
            data[ntotal  ]=buf[0];
            data[ntotal+1]=buf[1];
            data[ntotal+2]=buf[2];
            data[ntotal+3]=buf[3];
            ntotal+=4;
            if (ntotal==120) break;
        } // end of while
        // Send data
        if (ntotal>0) {
            LOG4CPLUS_DEBUG(logger, "programFlashCb: Sending " << ntotal << " data words");
            memoryWriteBlock(CBPC::CBPC_FL_BUFE+1-ntotal, data, ntotal);
            // wait until programming is complete
            LOG4CPLUS_DEBUG(logger, "programFlashCb: Waiting for programming to complete");
            unsigned char res=0;
            Timer.restart();
            while(true) {
                res = memoryRead(CBPC::CBPC_BLK_STAT);
                if (res != 0x80) break;
                usleep(USLEEP_DELAY);
                if(Timer.elapsed()> 30) {
                    LOG4CPLUS_ERROR(logger, "CBPC_BLK_STAT still 0x80 after 30 seconds");
                    throw CCUException("CBPC_BLK_STAT still 0x80 after 30 seconds");
                }
            }
            LOG4CPLUS_DEBUG(logger, "programFlashCb: Programming is completed");
            // Check result
            if (res!=0x0) {
                LOG4CPLUS_WARN(logger, "programFlashCb: Suspicious status after programming: 0x" << hex << (int)res );
                break;
            }
            memoryWrite(CBPC::CBPC_FL_CMD2, (1<<CBPC::CFC2_BLK_ERRCLR));
            memoryWrite(CBPC::CBPC_FL_CMD2, 0);
        }
        if (eof) break;
        m++;
        if ((m-n)>100) {
            LOG4CPLUS_INFO(logger, "programFlashCb: " << m);
            n=m;
        }
    } // end of while

    ///////////////////////////////////////
    val=memoryRead(CBPC::CBPC_FL_ENABLE);
    LOG4CPLUS_INFO(logger, "programFlashCb: after PROGRAM, before LOCK : CBPC_FL_ENABLE=0x" << hex << val);
    //usleep(5000000);
    ///////////////////////////////////////

    // Lock the FLASH controller again to prevent random corruption - added 18 Sep 2009
    // Needed for new CB firmware ver >=0x10e after program sequence
    LOG4CPLUS_DEBUG(logger, "programFlashCb: Lock sequence");
    memoryWrite(CBPC::CBPC_FL_ENABLE ,0);

    ///////////////////////////////////////
    val=memoryRead(CBPC::CBPC_FL_ENABLE);
    LOG4CPLUS_INFO(logger, "programFlashCb: after PROGRAM, after LOCK : CBPC_FL_ENABLE=0x" << hex << val);
    //usleep(5000000);
    ///////////////////////////////////////

    close(fin);
    LOG4CPLUS_INFO(logger, "programFlashCb: Done");
}

// Auxilary
void FecManagerImpl::StdLinkBoxAccessImpl::waitLbcFlashDone(int lb) {
    const int USLEEP_DELAY=100;
    selectLbc(lb);
    Timer.restart();
    while(true) {
        if (memoryRead16(LBCBase+LBC::LBC_FSTAT) & 0x80) break;
        if(Timer.elapsed()> 20) {
            LOG4CPLUS_ERROR(logger, "(LBC_FSTAT & 0x80) still down after 20 seconds. LBC_FSTAT = "<<memoryRead16(LBCBase+LBC::LBC_FSTAT));
            throw CCUException("(LBC_FSTAT & 0x80) still down after 20 seconds");
        }
        usleep(USLEEP_DELAY);
    }
}
// Auxilary
void FecManagerImpl::StdLinkBoxAccessImpl::setLbcFlashAddr(int lb, uint32_t addr) {
    selectLbc(lb);
    memoryWrite16(LBCBase+LBC::LBC_FADL, addr & 0xffff);
    memoryWrite16(LBCBase+LBC::LBC_FADH, (addr >> 16) & 0xffff);
}
// Auxilary
uint32_t FecManagerImpl::StdLinkBoxAccessImpl::readLbcFlashAddr(int lb, uint32_t addr) {
    selectLbc(lb);
    setLbcFlashAddr(lb, addr);
    memoryWrite16(LBCBase+LBC::LBC_FCMD, 0);
    memoryWrite16(LBCBase+LBC::LBC_FCMD, (1<<LBC::CFC_READ));
    waitLbcFlashDone(lb);
    uint32_t res = (uint32_t)memoryRead16(LBCBase+LBC::LBC_FDOUT) |
        ((uint32_t)memoryRead16(LBCBase+LBC::LBC_2FDOUT) << 16);
    return res;
}
// Auxilary
uint32_t FecManagerImpl::StdLinkBoxAccessImpl::writeLbcFlashAddr(int lb, uint32_t addr, uint32_t data) {
    setLbcFlashAddr(lb, addr);
    memoryWrite16(LBCBase+LBC::LBC_FDIN, data & 0xffff);
    memoryWrite16(LBCBase+LBC::LBC_2FDIN, (data >> 16) & 0xffff);
    memoryWrite16(LBCBase+LBC::LBC_FCMD, 0);
    memoryWrite16(LBCBase+LBC::LBC_FCMD, (1<<LBC::CFC_WRITE));
    waitLbcFlashDone(lb);
    uint32_t res = (uint32_t)memoryRead16(LBCBase+LBC::LBC_FDOUT) |
     ((uint32_t)memoryRead16(LBCBase+LBC::LBC_2FDOUT) << 16);
    return res;
}

// Auxilary
void FecManagerImpl::StdLinkBoxAccessImpl::resetLbcFlash(int lb) {
    selectLbc(lb);
    // Unlock the FLASH controller - added 18 Sept 2009
    // Needed for new CB firmware ver >=0x10e before reset sequence
    memoryWrite16(LBCBase+LBC::LBC_FCMD,0x0);
    memoryWrite16(LBCBase+LBC::LBC_FL_ENABLE,0xacce);
    // 22 April 2008 - Added reset sequence before erasing
    // because there was a problem with FLASH CONTROLLER lock after programming non-erased FLASH
    memoryWrite16(LBCBase+LBC::LBC_FCMD, 0x820);
    usleep(500000); // 0.5 sec
    memoryWrite16(LBCBase+LBC::LBC_FCMD, 0x0);
    setLbcFlashAddr(lb, 0);
    memoryWrite16(LBCBase+LBC::LBC_FCMD, 0);
    memoryWrite16(LBCBase+LBC::LBC_FCMD, (1<<LBC::CFC_RESET));
    waitLbcFlashDone(lb);
    // Lock the FLASH controller to prevent random corruption - added 18 Sept 2009
    // Needed for new CB firmware ver >=0x10e after reset sequence
    memoryWrite16(LBCBase+LBC::LBC_FL_ENABLE, 0);
}

// Programs Link Board's KTP flash memory with a given firmware file (LB slot numbers are 0...8).
// Binary file 'lb_rom.bin' is taken from $(HOME)/boot/LBB directory.
// This method calls internally eraseFlashKtp().
uint32_t FecManagerImpl::StdLinkBoxAccessImpl::programFlashKtp(int lb, const std::string& lbromBin) {
    uint32_t val=0;
    uint32_t len = 0;
    const int USLEEP_DELAY=1;
    LOG4CPLUS_INFO(logger, "programFlashKtp " << lb << " " << lbromBin);
    selectLbc(lb);

    ///////////////////////////////////////
    val=memoryRead16(LBCBase+LBC::LBC_FL_ENABLE);
    LOG4CPLUS_INFO(logger, "programFlashKtp: before RESET : LBC_FL_ENABLE=0x" << hex << val);
    //usleep(500000);
    ///////////////////////////////////////

    // Reset sequence - added on 22 April 2008
    LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": Reset sequence");
    resetLbcFlash(lb);
    // Check result
    val = readLbcFlashAddr(lb, 0);
    LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": readLbcFlash(addr=0)=0x" << hex << val);

    ///////////////////////////////////////
    val=memoryRead16(LBCBase+LBC::LBC_FL_ENABLE);
    LOG4CPLUS_INFO(logger, "programFlashKtp: after RESET, before UNLOCK : LBC_FL_ENABLE=0x" << hex << val);
    //usleep(500000);
    ///////////////////////////////////////

    // Unlock the FLASH controller - added 18 Sept 2009
    // Needed for new CB firmware ver >=0x10e before erase sequence
    memoryWrite16(LBCBase+LBC::LBC_FCMD,0x0);
    memoryWrite16(LBCBase+LBC::LBC_FL_ENABLE,0xacce);
    ///////////////////////////////////////
    val=memoryRead16(LBCBase+LBC::LBC_FL_ENABLE);
    LOG4CPLUS_INFO(logger, "programFlashKtp: after UNLOCK, before ERASE : LBC_FL_ENABLE=0x" << hex << val);
    //usleep(500000);
    ///////////////////////////////////////

    // Erase sequence
    LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": Erase sequence");
    setLbcFlashAddr(lb, 0);
    memoryWrite16(LBCBase+LBC::LBC_FCMD, 0);
    memoryWrite16(LBCBase+LBC::LBC_FCMD, (1<<LBC::CFC_CHIP_ERASE));
    waitLbcFlashDone(lb);
    // Lock the FLASH controller to prevent random corruption - added 18 Sept 2009
    // Needed for new CB firmware ver >=0x10e after erase sequence
    memoryWrite16(LBCBase+LBC::LBC_FL_ENABLE, 0);

    // Check result
    val = readLbcFlashAddr(lb, 0);
    LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": readLbcFlash(addr=0)=0x" << hex << val);

    ///////////////////////////////////////
    val=memoryRead16(LBCBase+LBC::LBC_FL_ENABLE);
    LOG4CPLUS_INFO(logger, "programFlashKtp: after ERASE, before RESET : LBC_FL_ENABLE=0x" << hex << val);
    //usleep(5000000);
    ///////////////////////////////////////

    // Reset sequence - added on 22 April 2008
    LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": Reset sequence");
    resetLbcFlash(lb);
    // Check result
    val = readLbcFlashAddr(lb, 0);
    LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": readLbcFlash(addr=0)=0x" << hex << val);

    ///////////////////////////////////////
    val=memoryRead16(LBCBase+LBC::LBC_FL_ENABLE);
    LOG4CPLUS_INFO(logger, "programFlashKtp: after RESET, before UNLOCK : LBC_FL_ENABLE=0x" << hex << val);
    //usleep(5000000);
    ///////////////////////////////////////

    // Unlock the FLASH controller - added 18 Sept 2009
    // Needed for new CB firmware ver >=0x10e before program sequence
    memoryWrite16(LBCBase+LBC::LBC_FCMD,0x0);
    memoryWrite16(LBCBase+LBC::LBC_FL_ENABLE,0xacce);

    ///////////////////////////////////////
    val=memoryRead16(LBCBase+LBC::LBC_FL_ENABLE);
    LOG4CPLUS_INFO(logger, "programFlashKtp: after UNLOCK, before PROGRAM : LBC_FL_ENABLE=0x" << hex << val);
    //usleep(5000000);
    ///////////////////////////////////////

    // Prepare for FLASH programming
    memoryWrite16(LBCBase+LBC::LBC_WRKMODE, (1<<LBC::LWM_VRFY_DIS));
    memoryWrite16(LBCBase+LBC::LBC_FCMD, (1<<LBC::CFC_RESET));
    memoryWrite16(LBCBase+LBC::LBC_INTERF, (1<<LBC::INT_JT));
    waitLbcFlashDone(lb);
    unsigned short lbid = memoryRead16(LBCBase);
    val = readLbcFlashAddr(lb, 0x500);
    LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": After flash reset ID=0x" << hex << lbid
                << " readLbcFlash(addr=0x500)=0x" << hex << val);
    // Program FLASH with data read from file using block transfers
    int fin = open(lbromBin.c_str(), O_RDONLY);
    if (fin == -1) {
        LOG4CPLUS_ERROR(logger, "Cannot open file '" << lbromBin << "': " << strerror(errno));
        throw CCUException("Cannot open file '" + lbromBin + "': " + strerror(errno));
    }
    setLbcFlashAddr(lb, 0);
    memoryWrite16(LBC::LBC_FCMD, 0);
    memoryWrite16(LBC::LBC_FCMD, (1<<LBC::CFC_BLK_NEWADD));
    int n=0, m=0;
    bool eof=false;
    // Read subsequent blocks of 32-word size (1 word = 2 bytes)
    while(true) {
        int ntotal=0;
        unsigned short data16[32]={0};
        // Read data from file
        while(true) {
            unsigned char buf[4]={0, 0, 0, 0};
            int nread = read(fin, buf, 4);
            len += nread;
            if (nread != 4) {
                eof=true;
                break;
            }
            data16[ntotal  ]=256*buf[1]+buf[0];
            data16[ntotal+1]=256*buf[3]+buf[2];
            ntotal+=2;
            if (ntotal==32) break;
        }
        // Send data to flash
        if (ntotal>0) {
            LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": Sending " << ntotal << " data words");
            memoryWrite16Block(LBCBase+0x100-ntotal, data16, ntotal);
            // wait until programming is complete
            LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": Waiting for programming to complete");
            unsigned short res=0;
            Timer.restart();
            while(true) {
                res = memoryRead16(LBCBase+LBC::LBC_BLKST);
                if ((res & 0x8000)==0) break;
                if(Timer.elapsed()> 30) {
                    LOG4CPLUS_ERROR(logger, "(LBC_BLKST & 0x8000) still up after 30 seconds");
                    throw CCUException("(LBC_BLKST & 0x8000) still up after 30 seconds");
                }
                usleep(USLEEP_DELAY);
            }
            LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": Programming is completed");
            // Check result
            if (res!=0x0) {
                LOG4CPLUS_WARN(logger, "programFlashKtp: LB=" << lb << ": Suspicious status after programming: 0x" << hex << (int)res );
                break;
            }
            memoryWrite16(LBCBase+LBC::LBC_FCMD, (1<<LBC::CFC_BLK_ERRCLR));
            memoryWrite16(LBCBase+LBC::LBC_FCMD, 0);
        }
        if (eof) break;
        m++;
        if (m-n>100) {
            LOG4CPLUS_INFO(logger, "programFlashKtp: LB=" << lb << ": " << m);
            n=m;
        }
    } // end of external while loop

    ///////////////////////////////////////
    val=memoryRead16(LBCBase+LBC::LBC_FL_ENABLE);
    LOG4CPLUS_INFO(logger, "programFlashKtp: after PROGRAM, before LOCK : LBC_FL_ENABLE=0x" << hex << val);
    //usleep(5000000);
    ///////////////////////////////////////

    // Lock the FLASH controller to prevent random corruption - added 18 Sept 2009
    // Needed for new CB firmware ver >=0x10e after program sequence
    LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": Lock sequence");
    memoryWrite16(LBCBase+LBC::LBC_FL_ENABLE, 0);

    ///////////////////////////////////////
    val=memoryRead16(LBCBase+LBC::LBC_FL_ENABLE);
    LOG4CPLUS_INFO(logger, "programFlashKtp: after PROGRAM, after LOCK : LBC_FL_ENABLE=0x" << hex << val);
    //usleep(5000000);
    ///////////////////////////////////////

    LOG4CPLUS_INFO(logger, "programFlashKtp: LB=" << lb << ": Done");
    return len;
}

//void FecManagerImpl::StdLinkBoxAccessImpl::programConfigToFlashKtp(int lb, std::istream *in, uint32_t startaddr) {
//    uint32_t val=0;
//    const int USLEEP_DELAY=1;
//    selectLbc(lb);
//
//    ///////////////////////////////////////
//    val=memoryRead16(LBCBase+LBC::LBC_FL_ENABLE);
//    LOG4CPLUS_INFO(logger, "programFlashKtp: before RESET : LBC_FL_ENABLE=0x" << hex << val);
//    //usleep(5000000);
//    ///////////////////////////////////////
//
//    // Reset sequence - added on 22 April 2008
//    LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": Reset sequence");
//    resetLbcFlash(lb);
//    // Check result
////    val = readLbcFlashAddr(lb, 0);
//    LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": readLbcFlash(addr=0)=0x" << hex << val);
//
//    ///////////////////////////////////////
//    val=memoryRead16(LBCBase+LBC::LBC_FL_ENABLE);
//    LOG4CPLUS_INFO(logger, "programFlashKtp: after RESET, before UNLOCK : LBC_FL_ENABLE=0x" << hex << val);
//    //usleep(5000000);
//    ///////////////////////////////////////
//
//    // Unlock the FLASH controller - added 18 Sept 2009
//    // Needed for new CB firmware ver >=0x10e before erase sequence
//    memoryWrite16(LBCBase+LBC::LBC_FCMD,0x0);
//    memoryWrite16(LBCBase+LBC::LBC_FL_ENABLE,0xacce);
//
//    ///////////////////////////////////////
//    val=memoryRead16(LBCBase+LBC::LBC_FL_ENABLE);
//    LOG4CPLUS_INFO(logger, "programFlashKtp: after UNLOCK, before ERASE : LBC_FL_ENABLE=0x" << hex << val);
//    //usleep(5000000);
//    ///////////////////////////////////////
//
//    // Erase sequence
//    LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": Erase sequence");
//    setLbcFlashAddr(lb, startaddr);
//    memoryWrite16(LBCBase+LBC::LBC_FCMD, 0);
//    // Lock the FLASH controller to prevent random corruption - added 18 Sept 2009
//    // Needed for new CB firmware ver >=0x10e after erase sequence
//    memoryWrite16(LBCBase+LBC::LBC_FL_ENABLE, 0);
//
//    // Check result
////    val = readLbcFlashAddr(lb, 0);
//    LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": readLbcFlash(addr=0)=0x" << hex << val);
//
//    ///////////////////////////////////////
//    val=memoryRead16(LBCBase+LBC::LBC_FL_ENABLE);
//    LOG4CPLUS_INFO(logger, "programFlashKtp: after ERASE, before RESET : LBC_FL_ENABLE=0x" << hex << val);
//    //usleep(5000000);
//    ///////////////////////////////////////
//
//    // Reset sequence - added on 22 April 2008
//    LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": Reset sequence");
//    resetLbcFlash(lb);
//    // Check result
////    val = readLbcFlashAddr(lb, 0);
//    LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": readLbcFlash(addr=0)=0x" << hex << val);
//
//    ///////////////////////////////////////
//    val=memoryRead16(LBCBase+LBC::LBC_FL_ENABLE);
//    LOG4CPLUS_INFO(logger, "programFlashKtp: after RESET, before UNLOCK : LBC_FL_ENABLE=0x" << hex << val);
//    //usleep(5000000);
//    ///////////////////////////////////////
//
//    // Unlock the FLASH controller - added 18 Sept 2009
//    // Needed for new CB firmware ver >=0x10e before program sequence
//    memoryWrite16(LBCBase+LBC::LBC_FCMD,0x0);
//    memoryWrite16(LBCBase+LBC::LBC_FL_ENABLE,0xacce);
//
//    ///////////////////////////////////////
//    val=memoryRead16(LBCBase+LBC::LBC_FL_ENABLE);
//    LOG4CPLUS_INFO(logger, "programFlashKtp: after UNLOCK, before PROGRAM : LBC_FL_ENABLE=0x" << hex << val);
//    //usleep(5000000);
//    ///////////////////////////////////////
//
//    // Prepare for FLASH programming
//    memoryWrite16(LBCBase+LBC::LBC_WRKMODE, (1<<LBC::LWM_VRFY_DIS));
//    //memoryWrite16(LBCBase+LBC::LBC_FCMD, (1<<LBC::CFC_RESET));
//    //memoryWrite16(LBCBase+LBC::LBC_WRKMODE, 0);
//    memoryWrite16(LBCBase+LBC::LBC_FCMD, (1<<LBC::CFC_RESET));
//    memoryWrite16(LBCBase+LBC::LBC_FCMD, (1<<LBC::CFC_BLK_NEWADD));
//    memoryWrite16(LBCBase+LBC::LBC_INTERF, (1<<LBC::INT_JT));
//    waitLbcFlashDone(lb);
//    unsigned short lbid = memoryRead16(LBCBase);
////    val = readLbcFlashAddr(lb, 0x500);
//    LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": After flash reset ID=0x" << hex << lbid
//                << " readLbcFlash(addr=0x500)=0x" << hex << val);
//    // Program FLASH with data read from file using block transfers
//
//    setLbcFlashAddr(lb, startaddr);
//    memoryWrite16(LBC::LBC_FCMD, 0);
//    memoryWrite16(LBC::LBC_FCMD, (1<<LBC::CFC_BLK_NEWADD));
//    int n=0, m=0;
//    bool eof=false;
//    // Read subsequent blocks of 32-word size (1 word = 2 bytes)
//    while(true) {
//        int ntotal=0;
//        unsigned short data16[32]={0};
//        // Read data from file
//        ntotal=32;        // Send data to flash
//        if (ntotal>0) {
//            LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": Sending " << ntotal << " data words");
//            memoryWrite16Block(LBCBase+0x100-ntotal, data16, ntotal);
//            // wait until programming is complete
//            LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": Waiting for programming to complete");
//            unsigned short res=0;
//            Timer.restart();
//            while(true) {
//                res = memoryRead16(LBCBase+LBC::LBC_BLKST);
//                if ((res & 0x8000)==0) break;
//                if(Timer.elapsed()> 30) {
//                    LOG4CPLUS_ERROR(logger, "(LBC_BLKST & 0x8000) still up after 30 seconds");
//                    throw CCUException("(LBC_BLKST & 0x8000) still up after 30 seconds");
//                }
//                usleep(USLEEP_DELAY);
//            }
//            LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": Programming is completed");
//            // Check result
//            if (res!=0x0) {
//                LOG4CPLUS_WARN(logger, "programFlashKtp: LB=" << lb << ": Suspicious status after programming: 0x" << hex << (int)res );
//                break;
//            }
//            memoryWrite16(LBCBase+LBC::LBC_FCMD, (1<<LBC::CFC_BLK_ERRCLR));
//            memoryWrite16(LBCBase+LBC::LBC_FCMD, 0);
//        }
//        break;
//        m++;
//        if (m-n>100) {
//            LOG4CPLUS_INFO(logger, "programFlashKtp: LB=" << lb << ": " << m);
//            n=m;
//        }
//    } // end of external while loop
//
//    ///////////////////////////////////////
//    val=memoryRead16(LBCBase+LBC::LBC_FL_ENABLE);
//    LOG4CPLUS_INFO(logger, "programFlashKtp: after PROGRAM, before LOCK : LBC_FL_ENABLE=0x" << hex << val);
//    //usleep(5000000);
//    ///////////////////////////////////////
//
//    // Lock the FLASH controller to prevent random corruption - added 18 Sept 2009
//    // Needed for new CB firmware ver >=0x10e after program sequence
//    LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": Lock sequence");
//    memoryWrite16(LBCBase+LBC::LBC_FL_ENABLE, 0);
//
//    ///////////////////////////////////////
//    val=memoryRead16(LBCBase+LBC::LBC_FL_ENABLE);
//    LOG4CPLUS_INFO(logger, "programFlashKtp: after PROGRAM, after LOCK : LBC_FL_ENABLE=0x" << hex << val);
//    //usleep(5000000);
//    ///////////////////////////////////////
//
//    LOG4CPLUS_INFO(logger, "programFlashKtp: LB=" << lb << ": Done");
//}

//// Add configuration after firmware in KTP chip.
//// Programs Link Board's KTP flash memory with a given firmware file (LB slot numbers are 0...8).
//void FecManagerImpl::StdLinkBoxAccessImpl::programConfigToFlashKtp(int lb, std::istream *in, uint32_t startaddr) {
//    uint32_t val=0;
//    const int USLEEP_DELAY=1;
//
//
//    memoryWrite16(LBCBase+LBC::LBC_FCMD,0x0);
//    memoryWrite16(LBCBase+LBC::LBC_FL_ENABLE,0xacce);
//    setLbcFlashAddr(lb, startaddr);
//    memoryWrite16(LBCBase+LBC::LBC_INTERF, (1<<LBC::INT_JT));
//    memoryWrite16(LBC::LBC_FCMD, 0);
//    memoryWrite16(LBC::LBC_FCMD, (1<<LBC::CFC_BLK_NEWADD));
//
//    int n=0, m=0;
//    bool eof=false;
//    // Read subsequent blocks of 32-word size (1 word = 2 bytes)
//    while(true) {
//        int ntotal=0;
//        unsigned short data16[32]={0};
//        // Read data from file
//        while(true) {
//            unsigned char buf[4]={0, 0, 0, 0};
//            /*in.get(buf[0]);
//            in.get(buf[1]);
//            in.get(buf[2]);
//            in.get(buf[3]);*/
//            in->read((char*)buf, 4);
//            int nread = in->gcount();
//            if (nread != 4) {
//                eof=true;
//                break;
//            }
//
//            data16[ntotal  ]=256*buf[1]+buf[0];
//            data16[ntotal+1]=256*buf[3]+buf[2];
//            ntotal+=2;
//            if (ntotal==32) break;
//        }
//        // Send data to flash
//        if (ntotal>0) {
//            LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": Sending " << ntotal << " data words");
//            memoryWrite16Block(LBCBase+0x100-ntotal, data16, ntotal);
//            // wait until programming is complete
//            LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": Waiting for programming to complete");
//            unsigned short res=0;
//            Timer.restart();
//            while(true) {
//                res = memoryRead16(LBCBase+LBC::LBC_BLKST);
//                if ((res & 0x8000)==0) break;
//                if(Timer.elapsed()> 30) {
//                    LOG4CPLUS_ERROR(logger, "(LBC_BLKST & 0x8000) still up after 30 seconds");
//                    throw CCUException("(LBC_BLKST & 0x8000) still up after 30 seconds");
//                }
//                usleep(USLEEP_DELAY);
//            }
//            LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": Programming is completed");
//            // Check result
//            if (res!=0x0) {
//                LOG4CPLUS_WARN(logger, "programFlashKtp: LB=" << lb << ": Suspicious status after programming: 0x" << hex << (int)res );
//                break;
//            }
//            memoryWrite16(LBCBase+LBC::LBC_FCMD, (1<<LBC::CFC_BLK_ERRCLR));
//            memoryWrite16(LBCBase+LBC::LBC_FCMD, 0);
//        }
//        if (eof) break;
//        m++;
//        if (m-n>100) {
//            LOG4CPLUS_INFO(logger, "programFlashKtp: LB=" << lb << ": " << m);
//            n=m;
//        }
//    } // end of external while loop
//
//    ///////////////////////////////////////
//    val=memoryRead16(LBCBase+LBC::LBC_FL_ENABLE);
//    LOG4CPLUS_INFO(logger, "programFlashKtp: after PROGRAM, before LOCK : LBC_FL_ENABLE=0x" << hex << val);
//    //usleep(5000000);
//    ///////////////////////////////////////
//
//    // Lock the FLASH controller to prevent random corruption - added 18 Sept 2009
//    // Needed for new CB firmware ver >=0x10e after program sequence
//    LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": Lock sequence");
//    memoryWrite16(LBCBase+LBC::LBC_FL_ENABLE, 0);
//
//    ///////////////////////////////////////
//    val=memoryRead16(LBCBase+LBC::LBC_FL_ENABLE);
//    LOG4CPLUS_INFO(logger, "programFlashKtp: after PROGRAM, after LOCK : LBC_FL_ENABLE=0x" << hex << val);
//    //usleep(5000000);
//    ///////////////////////////////////////
//
//    LOG4CPLUS_INFO(logger, "programFlashKtp: LB=" << lb << ": Done");
//}

// Add configuration after firmware in KTP chip.
// Programs Link Board's KTP flash memory with a given firmware file (LB slot numbers are 0...8).
void FecManagerImpl::StdLinkBoxAccessImpl::programConfigToFlashKtp(int lb, std::istream *in, uint32_t startaddr) {
    uint32_t val=0;
    const int USLEEP_DELAY=1;
    LOG4CPLUS_INFO(logger, "programConfigToFlashKtp " << lb);
    selectLbc(lb);

    ///////////////////////////////////////
    val=memoryRead16(LBCBase+LBC::LBC_FL_ENABLE);
    LOG4CPLUS_INFO(logger, "programFlashKtp: before RESET : LBC_FL_ENABLE=0x" << hex << val);
    //usleep(5000000);
    ///////////////////////////////////////

    // Reset sequence - added on 22 April 2008
    LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": Reset sequence");
    resetLbcFlash(lb);
    // Check result
    val = readLbcFlashAddr(lb, 0);
    LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": readLbcFlash(addr=0)=0x" << hex << val);

    ///////////////////////////////////////
    val=memoryRead16(LBCBase+LBC::LBC_FL_ENABLE);
    LOG4CPLUS_INFO(logger, "programFlashKtp: after RESET, before UNLOCK : LBC_FL_ENABLE=0x" << hex << val);
    //usleep(5000000);
    ///////////////////////////////////////

    // Unlock the FLASH controller - added 18 Sept 2009
    // Needed for new CB firmware ver >=0x10e before program sequence
    memoryWrite16(LBCBase+LBC::LBC_FCMD,0x0);
    memoryWrite16(LBCBase+LBC::LBC_FL_ENABLE,0xacce);

    ///////////////////////////////////////
    val=memoryRead16(LBCBase+LBC::LBC_FL_ENABLE);
    LOG4CPLUS_INFO(logger, "programFlashKtp: after UNLOCK, before PROGRAM : LBC_FL_ENABLE=0x" << hex << val);
    //usleep(5000000);
    ///////////////////////////////////////

    // Prepare for FLASH programming
    memoryWrite16(LBCBase+LBC::LBC_WRKMODE, (1<<LBC::LWM_VRFY_DIS));
    memoryWrite16(LBCBase+LBC::LBC_FCMD, (1<<LBC::CFC_RESET));
    memoryWrite16(LBCBase+LBC::LBC_FCMD, (1<<LBC::CFC_BLK_NEWADD));
    setLbcFlashAddr(lb, startaddr);
    memoryWrite16(LBCBase+LBC::LBC_INTERF, (1<<LBC::INT_JT));
//    waitLbcFlashDone(lb);
//    unsigned short lbid = memoryRead16(LBCBase);
//    val = readLbcFlashAddr(lb, 0x500);
//    LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": After flash reset ID=0x" << hex << lbid
//                << " readLbcFlash(addr=0x500)=0x" << hex << val);
    // Program FLASH with data read from file using block transfers

    LOG4CPLUS_INFO(logger, "programConfigToFlashKtp: setLbcFlashAddr(lb, "<<startaddr<<")");
    memoryWrite16(LBC::LBC_FCMD, 0);
    memoryWrite16(LBC::LBC_FCMD, (1<<LBC::CFC_BLK_NEWADD));
    int n=0, m=0;
    bool eof=false;
    // Read subsequent blocks of 32-word size (1 word = 2 bytes)
    while(true) {
        int ntotal=0;
        unsigned short data16[32]={0};
        // Read data from file
        while(true) {
            unsigned char buf[4]={0, 0, 0, 0};
            /*in.get(buf[0]);
            in.get(buf[1]);
            in.get(buf[2]);
            in.get(buf[3]);*/
            in->read((char*)buf, 4);
            int nread = in->gcount();
            if (nread != 4) {
                eof=true;
                break;
            }

            data16[ntotal  ]=256*buf[1]+buf[0];
            data16[ntotal+1]=256*buf[3]+buf[2];
            ntotal+=2;
            if (ntotal==32) break;
        }
        // Send data to flash
        if (ntotal>0) {
            LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": Sending " << ntotal << " data words");
            memoryWrite16Block(LBCBase+0x100-ntotal, data16, ntotal);
            // wait until programming is complete
            LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": Waiting for programming to complete");
            unsigned short res=0;
            Timer.restart();
            while(true) {
                res = memoryRead16(LBCBase+LBC::LBC_BLKST);
                if ((res & 0x8000)==0) break;
                if(Timer.elapsed()> 30) {
                    LOG4CPLUS_ERROR(logger, "(LBC_BLKST & 0x8000) still up after 30 seconds");
                    throw CCUException("(LBC_BLKST & 0x8000) still up after 30 seconds");
                }
                usleep(USLEEP_DELAY);
            }
            LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": Programming is completed");
            // Check result
            if (res!=0x0) {
                LOG4CPLUS_WARN(logger, "programFlashKtp: LB=" << lb << ": Suspicious status after programming: 0x" << hex << (int)res );
                break;
            }
            memoryWrite16(LBCBase+LBC::LBC_FCMD, (1<<LBC::CFC_BLK_ERRCLR));
            memoryWrite16(LBCBase+LBC::LBC_FCMD, 0);
        }
        if (eof) break;
        m++;
        if (m-n>100) {
            LOG4CPLUS_INFO(logger, "programFlashKtp: LB=" << lb << ": " << m);
            n=m;
        }
    } // end of external while loop

    ///////////////////////////////////////
    val=memoryRead16(LBCBase+LBC::LBC_FL_ENABLE);
    LOG4CPLUS_INFO(logger, "programFlashKtp: after PROGRAM, before LOCK : LBC_FL_ENABLE=0x" << hex << val);
    //usleep(5000000);
    ///////////////////////////////////////

    // Lock the FLASH controller to prevent random corruption - added 18 Sept 2009
    // Needed for new CB firmware ver >=0x10e after program sequence
    LOG4CPLUS_DEBUG(logger, "programFlashKtp: LB=" << lb << ": Lock sequence");
    memoryWrite16(LBCBase+LBC::LBC_FL_ENABLE, 0);

    ///////////////////////////////////////
    val=memoryRead16(LBCBase+LBC::LBC_FL_ENABLE);
    LOG4CPLUS_INFO(logger, "programFlashKtp: after PROGRAM, after LOCK : LBC_FL_ENABLE=0x" << hex << val);
    //usleep(5000000);
    ///////////////////////////////////////

    LOG4CPLUS_INFO(logger, "programFlashKtp: LB=" << lb << ": Done");
}

/*
// Downloads firmware to FPGA from flash memories on the Link- and Control Boards.
void FecManagerImpl::StdLinkBoxAccessImpl::loadFromFlash() {
    LOG4CPLUS_INFO(logger, "loadFromFlash: Start");
    // Reset CBIC
    LOG4CPLUS_DEBUG(logger, "loadFromFlash: Reseting CBIC");
    memoryWrite(CBIC::CBIC_W0, 0x0);
    piaWrite(1, 0xdb);
    piaWrite(1, 0xdf);
    piaWrite(3, 0xd8);
    memoryWrite(CBIC::CBIC_W0, 0x0);
    // Set CBIC initialization registers
    LOG4CPLUS_DEBUG(logger, "loadFromFlash: Setting CBIC initialization registers");
    memoryWrite(CBIC::CBIC_RDT_C_BASE,0x97);
    memoryWrite(CBIC::CBIC_RDT_C_BASE+1,0x67);
    memoryWrite(CBIC::CBIC_RDT_C_BASE+2,0x32);
    memoryWrite(CBIC::CBIC_RDT_C_BASE+3,0xce);
    memoryWrite(CBIC::CBIC_RDT_C_BASE+4,0x12);
    memoryWrite(CBIC::CBIC_RDT_C_BASE+5,0x9a);
    memoryWrite(CBIC::CBIC_RDT_C_BASE+6,0x50);
    memoryWrite(CBIC::CBIC_RDT_C_BASE+7,0xb6);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+0,0x88);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+1,0xf1);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+2,0x88);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+3,0xf2);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+4,0x88);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+5,0xf3);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+6,0x88);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+7,0xf4);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+8,0x88);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+9,0xf5);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+10,0x88);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+11,0xf6);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+12,0x88);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+13,0xf7);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+14,0x88);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+15,0xf8);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+16,0x88);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+17,0xf9);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+18,0x88);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+19,0xf4);
    // Set the reconfiguration command
    memoryWrite(CBIC::CBIC_RELOAD_CMD,0x34);
    // Changed per WZ request on 11 July 2008
    // --------------------------
    // memoryWrite(CBIC::CBIC_RELOAD_CMD_MASK,0xc3);
    // --------------------------
    memoryWrite(CBIC::CBIC_RELOAD_CMD_MASK,0x00);
    // --------------------------

    w0Set(bitVal(CBIC::CBIC_W0_RUN));
    usleep(1000000); // sec //TODO
    LOG4CPLUS_DEBUG(logger, "loadFromFlash: CBIC_STATE=0x" << hex << (int)memoryRead(CBIC::CBIC_STATE)
                << " CBIC_ERR=0x" << hex << (int)memoryRead(CBIC::CBIC_ERROR));
    piaWrite(3, 0xec);
    piaWrite(3, 0xfc);

    unsigned char data[3]={0};
    data[2] = memoryRead(CBIC::CBIC_MADH);
    data[1] = memoryRead(CBIC::CBIC_MADM);
    data[0] = memoryRead(CBIC::CBIC_MADL);
    uint32_t addr = (((uint32_t)data[2])<<16) | (((uint32_t)data[1])<<8) | ((uint32_t)data[0]);
    LOG4CPLUS_DEBUG(logger, "loadFromFlash: Addr=0x" << hex << addr
                << " 4*Addr=0x" << hex << (4*addr));

    usleep(200000); // 0.2 sec //TODO
    memoryWrite(CBIC::CBIC_RDT_L_BASE+0,0x00);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+2,0x00);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+4,0x00);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+6,0x00);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+8,0x00);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+10,0x00);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+12,0x00);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+14,0x00);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+16,0x00);
    memoryWrite(CBIC::CBIC_RDT_L_BASE+18,0x00);
    usleep(100000); // 0.1 sec
    piaWrite(1, 0xdc);
    w0Clr(bitVal(CBIC::CBIC_W0_RUN));
    w0Set(bitVal(CBIC::CBIC_W0_RUN));
    usleep(100000); // 0.1 sec
    piaWrite(1, 0xcd);
    w0Set(bitVal(CBIC::CBIC_W0_EMG_RESET)    |
          bitVal(CBIC::CBIC_W0_EMG_nPROGRAM) |
          bitVal(CBIC::CBIC_W0_EMG_nWRITE)   |
          bitVal(CBIC::CBIC_W0_EMG_nCS)       );
    w0Clr(bitVal(CBIC::CBIC_W0_RUN));
    w0Set(bitVal(CBIC::CBIC_W0_RUN));
    usleep(100000); // 0.1 sec
    // Added by WZab - 14 Oct 2009
    //piaWrite(3, 0xfc);  // not needed for CBPC ver >=0x110

    LOG4CPLUS_DEBUG(logger, "loadFromFlash: CBPC_ID=0x" << hex << (int)memoryRead(CBPC::CBPC_IDENT));
    // Check if initial values have been transferred correctly
    for(int i=0; i<8; i++) {
        LOG4CPLUS_DEBUG(logger, "loadFromFlash: CBPC_IR"<<i<<"=0x" << hex << (int)memoryRead(CBPC::CBPC_IREGS+i));
    }
    if(!checkCb()) {
        LOG4CPLUS_WARN(logger, "loadFromFlash: CB is in a bad state!");
        throw CCUException("CB is in a bad state!");
    }
}
*/

// Downloads firmware to FPGA from flash memories on the Link- and Control Boards. faster version
// at the end enables hard reset trigger by the CCU, but disables the hard reset triggered by the TTC
void FecManagerImpl::StdLinkBoxAccessImpl::loadFromFlash() {
    LOG4CPLUS_INFO(logger, __FUNCTION__<<": Start");
    ttcHardResetEnabled_ = false;
    // Reset CBIC
    LOG4CPLUS_DEBUG(logger, __FUNCTION__<<": Reseting CBIC");
    memoryWrite(CBIC::CBIC_W0, 0x0);
    piaWrite(1, 0xdb);
    piaWrite(1, 0xdf);
    piaWrite(3, 0xd8);
    memoryWrite(CBIC::CBIC_W0, 0x0);
    // Set CBIC initialization registers
    LOG4CPLUS_DEBUG(logger, __FUNCTION__<<": Setting CBIC initialization registers");

    unsigned char rdt_c_data[8] = {
    		0x97,
    		0x67,
    		0x32,
    		0xce,
    		0x12,
    		0x9a,
    		0x50,
    		0xb6
    };
    memoryWriteBlock(CBIC::CBIC_RDT_C_BASE, rdt_c_data, 8);

    unsigned char rdt_l_data1[20] = {
    	    0x88,
    	    0xf1,
    	    0x88,
    	    0xf2,
    	    0x88,
    	    0xf3,
    	    0x88,
    	    0xf4,
    	    0x88,
    	    0xf5,
    	    0x88,
    	    0xf6,
    	    0x88,
    	    0xf7,
    	    0x88,
    	    0xf8,
    	    0x88,
    	    0xf9,
    	    0x88,
    	    0xf4
    };
    memoryWriteBlock(CBIC::CBIC_RDT_L_BASE, rdt_l_data1, 20);

    // Set the reconfiguration command
    memoryWrite(CBIC::CBIC_RELOAD_CMD,0x4c); //0x0c is the pretriger2 TTC command that blocks the input of the OPTO

    memoryWrite(CBIC::CBIC_RELOAD_CMD_MASK,0xc3);
    // --------------------------

/* CB TTCrx should be reset only if needed or on explicit call
    w0Set(bitVal(CBIC::CBIC_W0_TTCrx_RESET));
    usleep(4000000);
    w0Clr(bitVal(CBIC::CBIC_W0_TTCrx_RESET));
    usleep(1000000);
    LOG4CPLUS_INFO(logger, __FUNCTION__<<":"<<__LINE__<<" after CBTTCrx rest: TTCrx ready: "<<(unsigned int)piaRead(CBIC::CCU_PA_TTCReady));
*/

    w0Set(bitVal(CBIC::CBIC_W0_RUN));
    usleep(1000000); //TODO maciekz
    //LOG4CPLUS_INFO(logger, __FUNCTION__<<":"<<__LINE__<<" TTCrx ready: "<<(unsigned int)piaRead(CBIC::CCU_PA_TTCReady));
    memoryWrite(CBIC::CBIC_TTC_RST_HOLD,0xff); //1.67 second or 3.3 second - not sure
    memoryWrite(CBIC::CBIC_TTC_RST_LEN,0xff); //1.67 second or 3.3 second, but this is much too short, we need 4 seconds!!!
    memoryWrite(CBIC::CBIC_RELOAD_START, 0xff);
    memoryWrite(CBIC::CBIC_RELOAD_THRES, 0xfd);
    //w0Set(bitVal(CBIC::CBIC_W0_AUTO_TTCrx_RESET));
    w0Clr(bitVal(CBIC::CBIC_W0_AUTO_TTCrx_RESET)); //we turn this off as the automatic reset time is much to short now

    LOG4CPLUS_DEBUG(logger, "loadFromFlash: CBIC_STATE=0x" << hex << (int)memoryRead(CBIC::CBIC_STATE)
                << " CBIC_ERR=0x" << hex << (int)memoryRead(CBIC::CBIC_ERROR));
    piaWrite(3, 0xec);
    piaWrite(3, 0xfc);

    unsigned char data[3]={0};
    data[2] = memoryRead(CBIC::CBIC_MADH);
    data[1] = memoryRead(CBIC::CBIC_MADM);
    data[0] = memoryRead(CBIC::CBIC_MADL);
    uint32_t addr = (((uint32_t)data[2])<<16) | (((uint32_t)data[1])<<8) | ((uint32_t)data[0]);
    LOG4CPLUS_DEBUG(logger, "loadFromFlash: Addr=0x" << hex << addr
                << " 4*Addr=0x" << hex << (4*addr));

    usleep(50000); //TODO increase in case of problems
/*    if (!enableTTCHardReset)
    {
    	unsigned char rdt_l_data2[20] = {
    			0x00,
    			0xf1,
    			0x00,
    			0xf2,
    			0x00,
    			0xf3,
    			0x00,
    			0xf4,
    			0x00,
    			0xf5,
    			0x00,
    			0xf6,
    			0x00,
    			0xf7,
    			0x00,
    			0xf8,
    			0x00,
    			0xf9,
    			0x00,
    			0xf4
    	};
    	memoryWriteBlock(CBIC::CBIC_RDT_L_BASE, rdt_l_data2, 20);
    }*/

    usleep(50000); //TODO increase in case of problems
    piaWrite(1, 0xdc); //‭1101 1100 , 00-emergency mode‬
    w0Clr(bitVal(CBIC::CBIC_W0_RUN));
    w0Set(bitVal(CBIC::CBIC_W0_RUN));
    usleep(50000); //TODO increase in case of problems

/*    if (!enableTTCHardReset)
    	piaWrite(1, 0xcd);
    else
    	piaWrite(1, 0xc7);*/
    piaWrite(1, 0xcf); //1100 1111 TTChard reset disabled, 11-normal operation

    w0Set(bitVal(CBIC::CBIC_W0_EMG_RESET)    |
          bitVal(CBIC::CBIC_W0_EMG_nPROGRAM) |
          bitVal(CBIC::CBIC_W0_EMG_nWRITE)   |
          bitVal(CBIC::CBIC_W0_EMG_nCS)       );
    w0Clr(bitVal(CBIC::CBIC_W0_RUN));
    w0Set(bitVal(CBIC::CBIC_W0_RUN));
    usleep(50000); //TODO increase in case of problems
    usleep(100000); //TODO maciekz
    //usleep(6000000); //TODO maciekz
    // Added by WZab - 14 Oct 2009
    //piaWrite(3, 0xfc);  // not needed for CBPC ver >=0x110

    LOG4CPLUS_DEBUG(logger, "loadFromFlash: CBPC_ID=0x" << hex << (int)memoryRead(CBPC::CBPC_IDENT));
    // Check if initial values have been transferred correctly
    for(int i=0; i<8; i++) {
        LOG4CPLUS_DEBUG(logger, "loadFromFlash: CBPC_IR"<<i<<"=0x" << hex << (int)memoryRead(CBPC::CBPC_IREGS+i));
    }
    if(!checkCb()) {
        LOG4CPLUS_WARN(logger, "loadFromFlash: CB is in a bad state!");
        throw CCUException("CB is in a bad state!");
    }
}




//going via putCBICinResetState and putCBICinRunState is neede to apply some settings. e.g. those written to piaWrite(1, ..)
void FecManagerImpl::StdLinkBoxAccessImpl::putCBICinResetState() {
    w0Clr(bitVal(CBIC::CBIC_W0_RUN));
}

/*
 * going from putCBICinResetState to putCBICinRunState
 * triggers firmware reloading if the CBIC is configured for that
 */
void FecManagerImpl::StdLinkBoxAccessImpl::putCBICinRunState() {
    w0Set(bitVal(CBIC::CBIC_W0_RUN));
}

/*
 * puts the CBIC in the reset state
 * enables hard reset trigger by the CCU, but disables the hard reset triggered by the TTC
 */
void FecManagerImpl::StdLinkBoxAccessImpl::configureCBICforLoadFromFlash() {
    LOG4CPLUS_INFO(logger, __FUNCTION__<<": Start");
    LOG4CPLUS_DEBUG(logger, __FUNCTION__<<": Reseting CBIC");
    ttcHardResetEnabled_ = false;

    memoryWrite(CBIC::CBIC_W0, 0x0); //This puts the CBIC in the resets state

    //piaWrite(1, 0xdb);
    piaWrite(1, 0xdf); //bit4=1 - blocks the access to the CBUS lines needed for the LBC reconfiguration for the CBPC and gives it o the CBIC
    				   //bit3=1 blocks reception of the TTC hardreset; bits 1 and 0 = 11 - normal mode - sensitive to hardreset via the CCU
    piaWrite(3, 0xd8); //Why? It only switches CBPC to known mode

    // Set CBIC initialization registers
    LOG4CPLUS_DEBUG(logger, __FUNCTION__<<": Setting CBIC initialization registers");

    unsigned char rdt_c_data[8] = { //These values are copied to the initialization registers in CBPC
    		//right after the firmware is uploaded
    		0x97,
    		0x67,
    		0x32,
    		0xce,
    		0x12,
    		0x9a,
    		0x50,
    		0xb6
    };
    memoryWriteBlock(CBIC::CBIC_RDT_C_BASE, rdt_c_data, 8);

    unsigned char rdt_l_data1[20] = { //These values are copied to the initialization registers in LBC
    		//right after the firmware is uploaded
    	    0x88, //LBC 0
    	    0xf1,
    	    0x88, //LBC 1
    	    0xf2,
    	    0x88, //...
    	    0xf3,
    	    0x88,
    	    0xf4,
    	    0x88,
    	    0xf5,
    	    0x88,
    	    0xf6,
    	    0x88,
    	    0xf7,
    	    0x88,
    	    0xf8,
    	    0x88,
    	    0xf9,
    	    0x88,
    	    0xf4
    };
    memoryWriteBlock(CBIC::CBIC_RDT_L_BASE, rdt_l_data1, 20);

    // Set the TTC reconfiguration command
    memoryWrite(CBIC::CBIC_RELOAD_CMD,0x4c); //0x0c is the pretriger2 TTC command that blocks the output of the OPTO

    memoryWrite(CBIC::CBIC_RELOAD_CMD_MASK,0xc3);


    //set the time between the two TTC BGO commands needed to trigger the Hard Reset
    memoryWrite(CBIC::CBIC_RELOAD_START, 0xff);
    memoryWrite(CBIC::CBIC_RELOAD_THRES, 0xfd);
    // --------------------------

    //Parameters of the automatic TTCrx reset
    memoryWrite(CBIC::CBIC_TTC_RST_HOLD,0xff); //1.67 second or 3.3 second - not sure
    memoryWrite(CBIC::CBIC_TTC_RST_LEN,0xff); //1.67 second or 3.3 second, but this is much too short, we need 4 seconds!!!
}

void FecManagerImpl::StdLinkBoxAccessImpl::resetTTCrxStep1() {
	unsigned int  ready = (unsigned int)piaRead(CBIC::CCU_PA_TTCReady);
	unsigned short cbicW0 = memoryRead(CBIC::CBIC_W0);
	unsigned short resetBit =  (cbicW0 & bitVal(CBIC::CBIC_W0_TTCrx_RESET))>> CBIC::CBIC_W0_TTCrx_RESET;
	LOG4CPLUS_INFO(logger, __FUNCTION__<<" "<<name<<" before CBTTCrx reset: TTCrx ready: "<<ready<<",  CBIC_W0_TTCrx_RESET "<<resetBit);
	LOG4CPLUS_INFO(logger, "fetCcuCRE(CCUKey): "<<hex<<fecManager.fecAccess_->getCcuCRE(CCUKey));

	w0Clr(bitVal(CBIC::CBIC_W0_AUTO_TTCrx_RESET)); //to be sure that it does not interfere with the software TTCrx reset
    w0Set(bitVal(CBIC::CBIC_W0_TTCrx_RESET));
}

	//usleep(8000000);
void FecManagerImpl::StdLinkBoxAccessImpl::resetTTCrxStep2() {
	w0Clr(bitVal(CBIC::CBIC_W0_TTCrx_RESET));
}
	//usleep(1000000);
bool FecManagerImpl::StdLinkBoxAccessImpl::resetTTCrxStep3() {
	unsigned int  ready = (unsigned int)piaRead(CBIC::CCU_PA_TTCReady);
    LOG4CPLUS_INFO(logger, __FUNCTION__<<" "<<name<<" after CBTTCrx reset: TTCrx ready: "<<ready);
    //w0Set(bitVal(CBIC::CBIC_W0_AUTO_TTCrx_RESET)); //TODO decide if we want to have the automatic TTCrx reset
    //w0Clr(bitVal(CBIC::CBIC_W0_AUTO_TTCrx_RESET));

    return ready;
}

	//w0Set(bitVal(CBIC::CBIC_W0_RUN)); //starts the firmware reloading, call putCBICinRunState() instead

	//usleep(FixedHardwareSettings::CONFIGURE_FROM_FLASH_TIME);

void FecManagerImpl::StdLinkBoxAccessImpl::checkCBICafterLoadFromFlash() {
    LOG4CPLUS_DEBUG(logger, __FUNCTION__<<" "<<name<<": CBIC_STATE=0x" << hex << (int)memoryRead(CBIC::CBIC_STATE)
                << " CBIC_ERR=0x" << hex << (int)memoryRead(CBIC::CBIC_ERROR));

    //KB. this is in the laodFromFlash, is this needed here???
    //piaWrite(3, 0xec);
    //piaWrite(3, 0xfc);

    piaWrite(1, 0xcf); //1100 1111 TTChard reset disabled, 11-normal operation

    unsigned char data[3]={0};
    data[2] = memoryRead(CBIC::CBIC_MADH);
    data[1] = memoryRead(CBIC::CBIC_MADM);
    data[0] = memoryRead(CBIC::CBIC_MADL);
    uint32_t addr = (((uint32_t)data[2])<<16) | (((uint32_t)data[1])<<8) | ((uint32_t)data[0]);
    LOG4CPLUS_DEBUG(logger, __FUNCTION__<<": Addr=0x" << hex << addr
                << " 4*Addr=0x" << hex << (4*addr));

    /*
     * The code below has probably also historic meaning. Probably the emergency mode was not functioning
     * correctly in certain version CBIC, so instead we used the "program LBC mode", but all lines of the programming
     * interface were set to inactive values as below.
     */
    w0Set(bitVal(CBIC::CBIC_W0_EMG_RESET)    |
          bitVal(CBIC::CBIC_W0_EMG_nPROGRAM) |
          bitVal(CBIC::CBIC_W0_EMG_nWRITE)   |
          bitVal(CBIC::CBIC_W0_EMG_nCS)       );

    //is this needed here????
    //w0Clr(bitVal(CBIC::CBIC_W0_RUN));
    //w0Set(bitVal(CBIC::CBIC_W0_RUN));
    putCBICinRunState();

    LOG4CPLUS_DEBUG(logger, "checkCBICafterLoadFromFlash: CBPC_ID=0x" << hex << (int)memoryRead(CBPC::CBPC_IDENT));
    // Check if initial values have been transferred correctly
    for(int i=0; i<8; i++) {
        LOG4CPLUS_DEBUG(logger, "checkCBICafterLoadFromFlash: CBPC_IR"<<i<<"=0x" << hex << (int)memoryRead(CBPC::CBPC_IREGS+i));
    }
    if(!checkCb()) {
        LOG4CPLUS_WARN(logger, "checkCBICafterLoadFromFlash: CB is in a bad state!");
        throw CCUException("CB is in a bad state!");
    }
}

//bit3=0 allows reception of hardreset
void FecManagerImpl::StdLinkBoxAccessImpl::enableTTCHardReset()  {
    ttcHardResetEnabled_ = true;
    piaWrite(1, 0xc7);
};
void FecManagerImpl::StdLinkBoxAccessImpl::disableTTCHardReset()  {
    ttcHardResetEnabled_ = false;
    piaWrite(1, 0xcf);
};

void FecManagerImpl::StdLinkBoxAccessImpl::configureCBICOperationMode(bool enableTTCHardReset) {
	ttcHardResetEnabled_ = enableTTCHardReset;
    ///usleep(50000); //TODO increase in case of problems
    if (!enableTTCHardReset)
    {
    	//Historic issue - LBC got spontaneously reset due to CCU errors
    	//setting of 0'th initialization register in the LBC blocks
    	//reconfiguration of syncoder! (bit3 is responsible for that)
    	unsigned char rdt_l_data2[20] = {
    			0x00,
    			0xf1,
    			0x00,
    			0xf2,
    			0x00,
    			0xf3,
    			0x00,
    			0xf4,
    			0x00,
    			0xf5,
    			0x00,
    			0xf6,
    			0x00,
    			0xf7,
    			0x00,
    			0xf8,
    			0x00,
    			0xf9,
    			0x00,
    			0xf4
    	};
    	memoryWriteBlock(CBIC::CBIC_RDT_L_BASE, rdt_l_data2, 20);
    	//I understand that this reset is needed to put the above values to the LBC
    	putCBICinResetState();
    	piaWrite(1, 0xdc); //CBIC will start in emergency mode after reset
    	putCBICinRunState(); //after that it is in the emergency mode and does not listen to TTC commands
    	usleep(50000); //TODO increase in case of problems
    }

    if (!enableTTCHardReset)
    	piaWrite(1, 0xcd); 	//bit4=0 - unblocks the CBPC access to the LBC
    						//bit3 - blocks reception of hardreset, bits1 and 0 - enter "emergency + programming LBC mode" (why?)
                           	//maybe it should be rather 0xc8 or 0xcc (bit 2 is unused)
    else
    	piaWrite(1, 0xc7); //bit3=0 allows reception of hardreset, bits 1 and 0 - normal mode - sensitive to hardreset
    /*
     * The code below has probably also historic meaning. Probably the emergency mode was not functioning
     * correctly in certain version CBIC, so instead we used the "program LBC mode", but all lines of the programming
     * interface were set to inactive values as below.
     */
    w0Set(bitVal(CBIC::CBIC_W0_EMG_RESET)    |
          bitVal(CBIC::CBIC_W0_EMG_nPROGRAM) |
          bitVal(CBIC::CBIC_W0_EMG_nWRITE)   |
          bitVal(CBIC::CBIC_W0_EMG_nCS)       );
    //w0Clr(bitVal(CBIC::CBIC_W0_RUN));
    //w0Set(bitVal(CBIC::CBIC_W0_RUN)); //These 2 lines reset the CBIC and force it to enter the above specified operation mode

    putCBICinRunState();

    LOG4CPLUS_DEBUG(logger, "configureCBICOperationMode: CBPC_ID=0x" << hex << (int)memoryRead(CBPC::CBPC_IDENT));
    // Check if initial values have been transferred correctly
    for(int i=0; i<8; i++) {
        LOG4CPLUS_DEBUG(logger, "configureCBICOperationMode: CBPC_IR"<<i<<"=0x" << hex << (int)memoryRead(CBPC::CBPC_IREGS+i));
    }
    if(!checkCb()) {
        LOG4CPLUS_WARN(logger, "configureCBICOperationMode: CB is in a bad state!");
        throw CCUException("CB is in a bad state!");
    }
}


// Check if CB is in a good state
bool FecManagerImpl::StdLinkBoxAccessImpl::checkCb() {
    LOG4CPLUS_DEBUG(logger, "checkCb: Start");
    bool isOK=true;
    unsigned char cbicid = memoryRead(CBIC::CBIC_ID0);
    LOG4CPLUS_DEBUG(logger, "checkCb: CBIC ID:  0x" << hex << (int)cbicid);
    if (cbicid != 0xcb) {
        ostringstream str;
        str<<"checkCb: CBIC_ID=0x" << hex << (int)cbicid << " is not 0xcd !";
        LOG4CPLUS_WARN(logger, str.str() );
        isOK=false;
    }
    int cbicver = memoryRead(CBIC::CBIC_ID_VER_MJR)*256+memoryRead(CBIC::CBIC_ID_VER);
    LOG4CPLUS_DEBUG(logger, "checkCb: CBIC VER: 0x" << hex << (int)cbicver);
    int cbpcid = memoryRead(CBPC::CBPC_IDENT);
    int test = memoryRead(CBPC::CBPC_IREGS);
    LOG4CPLUS_DEBUG(logger, "checkCb: CBPC ID:  0x" << hex << (int)cbpcid);
    LOG4CPLUS_WARN(logger, "checkCb: CBPC_IREGS:  0x" << hex << (int)test);
    if (cbpcid != 0xcc) {
        ostringstream str;
        str<<"checkCb: CBPC_ID=0x" << hex << (int)cbpcid << " is not 0xcc !";
        LOG4CPLUS_WARN(logger, str.str());
        isOK=false;
    }
    LOG4CPLUS_DEBUG(logger, "checkCb: Done (status="<<(isOK ? "OK" : "BAD"));
    return isOK;
}

///////////////////////////////////////////////////////
#ifdef MIKOLAJ_FEC_DEBUG
string FecManagerImpl::getFecCounterIO() {
    if(!fecAccess_) {
        throw CCUException("fecAccess pointer is 0!");
    }
    std::stringstream ostr;
    ostr << fecAccess_->getFecCounterIO();
    return ostr.str();
}
void FecManagerImpl::resetFecCounterIO() {
    if(!fecAccess_) {
        throw CCUException("fecAccess pointer is 0!");
    }
    fecAccess_->resetFecCounterIO();
}
#endif
///////////////////////////////////////////////////////

} // namespace

