#include "rpct/lboxaccess/StdLinkBoxAccessAdapter.h"

#ifdef linux
# include <unistd.h>
#endif

#include "tb_std.h"

namespace rpct {

StdLinkBoxAccessAdapter::StdLinkBoxAccessAdapter()
{}

StdLinkBoxAccessAdapter::~StdLinkBoxAccessAdapter()
{}

void StdLinkBoxAccessAdapter::resetTtc() 
{
    throw TException("StdLinkBoxAccessAdapter::resetTtc: nie wiem jak to ma dzialac na nowym sprzecie");
    memoryWrite(0xfffd, 0);
    #ifndef __BORLANDC__
      usleep(2000000ul);
    #else
      Sleep(2000);
    #endif
    memoryWrite(0xfffd, 0x80);
}


} // namespace
