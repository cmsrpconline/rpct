#include "rpct/lboxaccess/TFECManagerOld.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


bool G_CCUDebug = false;

const uint32_t TFECManagerOld::MaxBlockSize = 64;



TFECManagerOld::TFECManagerOld() : /*FEC(NULL),*/ BlockTransfer(false) {
    
    SelectLBC(0);

    unsigned int fec, ring, ccu;
    char* env_str;

      env_str = getenv("FEC_ADDRESS");
      if (env_str == NULL)
        fec = 0x0;
      else
        sscanf(env_str, "%d", &fec);

      env_str = getenv("RING_ADDRESS");
      if (env_str == NULL)
        ring = 0x0;
      else
        sscanf(env_str, "%x", &ring);


      env_str = getenv("CCU_ADDRESS");
      if (env_str == NULL)
        ccu = 0x1;
      else
        sscanf(env_str, "%x", &ccu);

    CCUKey = setFecKey(fec) | setRingKey(ring) | setCcuKey(ccu);
    CCUMemoryKey = CCUKey | setChannelKey(0x40);
    
    for (int i =0; i < I2CChanCount; i++)
        CCUI2CKeys[i] = CCUKey | setChannelKey(0x10 + i)/* |
                     setAddressKey((DD_TYPE_FEC_DATA16)1)*/;

    try {
        // Open the FEC
        //FECAcc = new FecAccess(true); // force init
        env_str = getenv("FEC_TYPE");
        if (env_str == NULL) {
            throw TException("Environment variable FEC_TYPE not set. Possible values: pci, vme.");
        }
        string fecType = env_str;
        if (fecType == "pci") {            
            FECAcc = new FecAccess(true, false, true, false, 100); // force init
        }
        else if (fecType == "vme") {
            env_str = getenv("FECSOFTWARE_ROOT");
            if (env_str == NULL) {
                throw TException("Environment variable FECSOFTWARE_ROOT not set.");
            }
            string vmeFileName = env_str + string("/config/FecAddressTable.dat");
            /*FECAcc = new FecAccess (0, vmeFileName, FecVmeRingDevice::VMEFECBASEADDRESSES, 
                true, false, true, false, 100, FecVmeRingDevice::STR_BUSADAPTER_NAME[CAENPCI]) ;*/
                
            uint32_t fecBaseAddresses[MAX_NUMBER_OF_SLOTS] = { 0 };
            fecBaseAddresses[fec] = FecVmeRingDevice::VMEFECBASEADDRESSES[fec];
            FECAcc = new FecAccess (0, vmeFileName, fecBaseAddresses, 
                true, false, true, false, 100, FecVmeRingDevice::STR_BUSADAPTER_NAME[CAENPCI]) ;
            /*FECAcc = new FecAccess (0, vmeFileName, FecVmeRingDevice::VMEFECBASEADDRESSES, 
                true, false, true, false, 100, FecVmeRingDevice::STR_BUSADAPTER_NAME[CAENPCI]) ;*/
        }
        else {
            throw TException("Invalid value of FEC_TYPE: " + fecType);
        }

        cout << "DEBUG: FecAccess created" << endl;
        
        //delete FECAcc->scanForFECs(0, 4);
        list<keyType>* fecList = FECAcc->getFecList();
        if ((fecList == NULL) || (fecList->empty())) {
            cerr << "Warning: No FEC rings board found" << endl;
            delete fecList;
            throw TException("No FEC rings board found");
        }
        delete fecList;
        //ResetFec();
        //CCUExternalReset();
        cout << "DEBUG: fecList retrieved and not empty" << endl;
        FECAcc->addMemoryAccess(CCUMemoryKey, MODE_SHARE);
        cout << "DEBUG: memory access added" << endl;
        FECAcc->setMemoryChannelCRA(CCUMemoryKey, MEM_CRA_SINGLE);
        cout << "DEBUG: memory channel CRA set" << endl;
        FECAcc->setMemoryChannelWin1HReg(CCUMemoryKey, 0xFFFF);
        cout << "DEBUG: memory channel initialized" << endl;
        // enable interrupts
        FECAcc->setCcuCRB(CCUKey, 0x3F);
        cout << "DEBUG: enabled interrupts" << endl;
        //FEC = FECAcc->getFecDevice(fec);
        for (int i = 0; i < PIAChanCount; i++) {
            keyType myKey = CCUKey | setChannelKey(0x30 + i); 
            PIAKeys[i] = myKey;
            //cout << "********przed addPiaAccess" << endl;
            /*keyType index =*/ FECAcc->addPiaAccess(myKey, MODE_SHARE);
            //cout << "********po addPiaAccess" << endl;
            //if (i == 1) throw TException("dupa");
            /*
            FECAcc->setChannelEnable(myKey, true);*/
            FECAcc->setPiaChannelGCR(myKey, 0x10);
            if (i == 0) {
                FECAcc->setPiaChannelDDR(myKey, 0x0);
            }
            else {
                FECAcc->setPiaChannelDDR(myKey, 0xff);    
            }
            //PIA[i] = new piaAccess(myKey, MODE_SHARE, FECAcc->getFecRingDevice(index));
            //PIA[i] = new piaAccess(myKey, MODE_SHARE, FEC);
            //PIA[i]->setChannelEnable(true);
        }    
        cout << "DEBUG: initialized PIA channels" << endl;
    }
    catch (FecExceptionHandler& e) {
        cerr << e.getMessage().c_str() << endl;
        throw;
    };
        
    /*for (int i=0; i < I2CChanCount; i++) {
        I2CVector.push_back(new TI2C(*this, i));
    }*/
}

FecAccess* TFECManagerOld::GetFecAccess() {
    return FECAcc;
}

void TFECManagerOld::ResetFec() {
    //FECAcc->plxReset();
    //sleep(1);
    FECAcc->fecHardReset(); 
    //sleep(1);  
    //FECAcc->fecRingReset(); 
}

void TFECManagerOld::CCUExternalReset() {
    FECAcc->setCcuCRA(CCUKey, 0xa0);
    sleep(1);  
}

void TFECManagerOld::SelectLBC(int lbc)
{
    /*uint32_t base = 0x7000 + lbc * 0x100;
    if (base != LBCBase) {
        LBCBase = base;
        CB_LSTAT = LBCBase + 0x08;
        CB_INTERF = LBCBase + 0x0d;
        CB_BLCK_BUF = LBCBase + 0x80;
        CB_XLNXD = LBCBase + 0x10;
        CB_XLNXC = LBCBase + 0x11;
        CB_PAGE = LBCBase + 0x15;
    }*/
    
    uint32_t base = 0x7000 + lbc * 0x100;
    if (base != LBCBase) {
        LBCBase = base;
//         CB_IDREG = LBCBase + 0x00;
//         CB_TESTREG = LBCBase + 0x01;
//         CB_FDIN = LBCBase + 0x02;
//         CB_FDOUT = LBCBase + 0x03;
//         CB_FADL = LBCBase + 0x04;
//         CB_FADH = LBCBase + 0x05;
//         CB_FCMD = LBCBase + 0x06;
//         CB_FSTAT = LBCBase + 0x07;
        CB_LSTAT = LBCBase + 0x08;
//         CB_END1L = LBCBase + 0x09;
//         CB_END1H = LBCBase + 0x0a;
//         CB_END2L = LBCBase + 0x0b;
//         CB_END2H = LBCBase + 0x0c;
        CB_INTERF = LBCBase + 0x0d;
//         CB_JTAG = LBCBase + 0x0e;
        CB_BLCK_BUF = LBCBase + 0x80;
        CB_XLNXD = LBCBase + 0x10;
        CB_XLNXC = LBCBase + 0x11;
        CB_PAGE = LBCBase + 0x15;
    }
}


void TFECManagerOld::SelectPage(int lb, uint32_t address)
{     
  if (G_CCUDebug)
    cout << "SelectPage " << lb << " " << address << flush;
  SelectLBC(lb);
  TPageMap::iterator iPage = PageMap.find(lb);
  if (iPage == PageMap.end()) {
    PageMap.insert(TPageMap::value_type(lb, address));
    MemoryWrite16(CB_PAGE, address);
  }
  else if ((address & 0xf800) != (iPage->second & 0xf800)) {
    MemoryWrite16(CB_PAGE, address);
    iPage->second = address;
    if (G_CCUDebug)
      cout << "Changing page for lb " << lb << " address " << (address & 0xf800) << endl;
  }
}
    

void TFECManagerOld::MemoryWrite(uint32_t address, unsigned short data) {
    SetBlockTransfer(false);
    if (G_CCUDebug) {
        cout << hex << "w " << address << " " << data << endl;
    }

    tscType16 ah = (address & 0xFF00) >> 8;
    tscType16 al = (address & 0xFF);
    FECAcc->write(CCUMemoryKey, ah, al, data);
}


unsigned short TFECManagerOld::MemoryRead(uint32_t address) {
    SetBlockTransfer(false);
    if (G_CCUDebug) {
        cout << hex << "r " << address << " = " << flush;
    }

    tscType16 ah = (address & 0xFF00) >> 8;
    tscType16 al = (address & 0xFF);
    tscType16 result = FECAcc->read(CCUMemoryKey, ah, al);

    if (G_CCUDebug) {
        cout << result << endl;
    }

    return result;
}
    
void TFECManagerOld::MemoryWriteBlock(uint32_t address, unsigned char* data,
                      uint32_t length)
{
      SetBlockTransfer(true);
      tscType16 ah;
      tscType16 al;
      uint32_t len;
      if (G_CCUDebug) {
      	cout << hex;
      	cout << "wr address " << address << " length " << length << endl;
      }
      for (int i = 0; length > 0; i += MaxBlockSize) {
         
        //cout << "MemoryWrite " 
        //     << " length " << length 
        //     << " i " << i << endl;
        ah = ((address + i) & 0xFF00) >> 8;
        al = ((address + i) & 0xFF);
        len = std::min(length, MaxBlockSize);
        FECAcc->write(CCUMemoryKey, ah, al, data + i, len);
        //cout << "MemoryWrite data: "; 
        //for (int a = 0; a < len; a++)
        //  cout << (data + i)[a] << ' ';
        //cout << endl;
        length -= len;
      }
}

void TFECManagerOld::MemoryReadBlock(uint32_t address, unsigned char* data,
                     uint32_t length)
{
      SetBlockTransfer(true);
      if (G_CCUDebug)
        cout << hex << "rb " << address << " " << length << "=\n" << flush;
        
      tscType16 ah;
      tscType16 al;
      uint32_t len;
      for (int i = 0; length > 0; i += MaxBlockSize) {
          ah = ((address + i) & 0xFF00) >> 8;
          al = ((address + i) & 0xFF);
          //FECAcc->read(MemoryKey, ah, al, length, data);
          len = std::min(length, MaxBlockSize);
          FECAcc->read(CCUMemoryKey, ah, al, len, data + i);
          length -= len;
      }
      
      if (G_CCUDebug) {
        for (uint32_t i = 0; i < length; i++)
          cout << "  [" << i << "]=" << data[i] << "\n";
        cout << flush;
      }
}
    
void TFECManagerOld::MemoryRead16Block(uint32_t address, unsigned short* data,
                     uint32_t length)
{
      tscType8* buffer = new tscType8[MaxBlockSize];
      try {
          const uint32_t maxLength = MaxBlockSize / 2 - 1;
          int words;
          for (int i = 0; length > 0; i += maxLength) {        
            words = std::min(length, maxLength);
            StartBlockTransfer16();
            MemoryReadBlock((address + i) * 2, buffer, words * 2 + 2);
            EndBlockTransfer16();
    
            unsigned short temp, temp1;
            for (int word = 0; word < words; word++) {
                temp = buffer[word * 2 + 2];
                temp1 = buffer[word * 2 + 3];
                data[i + word] = (temp & 0xFF) | ((temp1 << 8) & 0xFF00);
            }
            
            length -= words;
          }
      }
      catch(...) {
          delete [] buffer;
          throw;
      }
      
      delete [] buffer;
}
    
    
    
void TFECManagerOld::MemoryWrite16Block(uint32_t address, unsigned short* data,
                     uint32_t length)
{
      tscType8* buffer = new tscType8[MaxBlockSize];
      try {
          const uint32_t maxLength = MaxBlockSize / 2 - 1;
          int words;
          for (int i = 0; length > 0; i += maxLength) {   
            words = std::min(length, maxLength); 
            for (int word = 0; word < words; word++) {
              buffer[word*2] = data[i + word] & 0xff;
              buffer[word*2+1] = (data[i + word] >> 8) & 0xff;
            }
            buffer[words*2] = 0;
            
            StartBlockTransfer16();
            MemoryWriteBlock((address + i) * 2, buffer, words * 2 + 1);  
            EndBlockTransfer16();  
            
            length -= words;
          }
      }
      catch(...) {
          delete [] buffer;
          throw;
      }
      
      delete [] buffer;
}


void TFECManagerOld::MemoryWrite16(uint32_t address, unsigned short data)
{
  SetBlockTransfer(false);
  address *= 2;
  unsigned int data1 = (data & 0xFF);
  MemoryWrite(address, (DD_TYPE_FEC_DATA16)data1);
  address += 1;
  data1 = (data >> 8) & 0xFF;
  MemoryWrite(address, (DD_TYPE_FEC_DATA16)data1);
}

unsigned short TFECManagerOld::MemoryRead16(uint32_t address)
{
  SetBlockTransfer(false);
  address *= 2;
  unsigned int data = MemoryRead(address);
  address += 1;
  unsigned int data1 = MemoryRead(address);
  data = (data & 0xFF) | ((data1 << 8) & 0xFF00);
  return data;
}

static FILE* flashDump = NULL;

void TFECManagerOld::WriteFlash(uint32_t address, uint32_t data)
{
    if (flashDump != NULL) {
       fwrite(&data, sizeof(data), 1, flashDump);
    }
    // send data and address 
    unsigned char dataaddr[7];
    dataaddr[0] = data & 0xff;
    dataaddr[1] = (data >> 8) & 0xff;
    dataaddr[2] = (data >> 16) & 0xff;
    dataaddr[3] = (data >> 24) & 0xff;
    dataaddr[4] = address & 0xff;
    dataaddr[5] = (address >> 8) & 0xff;
    dataaddr[6] = (address >> 16) & 0xff;
    MemoryWriteBlock(CBPC_FL1_DIL, dataaddr, 7);
    
    // start programming
    MemoryWrite(CBPC_FL_CMD, 1);
    try {
    
        // wait until programming is finished        
        int val;
        Timer.restart();
        do {
            val = MemoryRead(CBPC_FL_STAT);
            if (Timer.elapsed() >= 1)
                throw CCUException(":  CBPC_FL_STAT bit 7 not set");
        } while (!(val & 0x80));
                        
        // read data from flash and check if it is correct 
        
        MemoryWrite(CBPC_FL_CMD, 2);
        Timer.restart();
        do {
            val = MemoryRead(CBPC_FL_STAT);
            if (Timer.elapsed() >= 1)
                throw CCUException(":  CBPC_FL_STAT bit 7 not set");
        } while (!(val & 0x80));
        
        memset(dataaddr, 0, sizeof(dataaddr));
        MemoryReadBlock(CBPC_FL1_DIL, dataaddr, 4);
        uint32_t recdata = *((uint32_t*)dataaddr);
        if (data != recdata) {
            ostringstream ost;
            ost << "WriteFlash: writen and read values differ: received " 
                << hex << recdata << " expected " << data;
            throw CCUException(ost.str());
        }
    }
    catch(...) {
        MemoryWrite(CBPC_FL_CMD, 0);       
    }
}

void TFECManagerOld::ProgramFlash(const std::string& file)
{
    FILE* fin = fopen(file.c_str(), "r");
    if (fin == NULL)
        throw CCUException("Problems opening file '" + file + "'");
    flashDump = fopen("flush.dump", "w");
          
    uint32_t data;
    ssize_t bread = 1;
    int count = 0;
    while (bread != 0) {
        bread = fread(&data, sizeof(data), 1, fin);
        WriteFlash(count, data);        

        if ((count % 1000) == 0)
            cerr << " " << count << flush;
        count++;
    }
    cerr << "\nDone" << endl;
    
    fclose(fin);
    fclose(flashDump);
    flashDump = NULL;
}

  

void TFECManagerOld::ProgramXilinx(const char* file) 
{
    FILE* fin1 = fopen(file, "r");
    if (fin1 == NULL)
        throw CCUException("Problems opening file '" + string(file) + "'");

    //MemoryWrite16(CB_PAGE, 0);
    MemoryWrite16(CB_XLNXC, 0xff);
    MemoryWrite16(CB_INTERF, 0x08);
        
    try {
        // PROGRAM_A and PROGRAM_B down
        MemoryWrite16(CB_XLNXC, 0xfc); 
        // wait for INIT_A
        for (int waitcount = 1; (MemoryRead16(CB_LSTAT) & 0x8000) != 0; waitcount++) {
            if (waitcount == 10000) {
              ostringstream ost;
              ost << "INIT signal did not go down: address " << hex << CB_LSTAT;
              cerr << ost.str() << endl;
                throw CCUException(ost.str().c_str());
            }
        }
        
        // WRITE_A and WRITE_B down
        MemoryWrite16(CB_XLNXC, 0xf0); 
                
        // PROGRAM_A and PROGRAM_B up
        MemoryWrite16(CB_XLNXC, 0xf3); 
        // wait for INIT_A and INIT_B up
        for (int waitcount = 1; (MemoryRead16(CB_LSTAT) & 0x8000) != 0x8000; waitcount++) {
            if (waitcount == 10000) {
              ostringstream ost;
              ost << "INIT signal did not go up: address " << hex << CB_LSTAT;
              cerr << ost.str() << endl;
                throw CCUException(ost.str().c_str());
            }
        }       
        
        // CS_A and CS_B down
        MemoryWrite16(CB_XLNXC, 0xC3); 
            
        // start sending data
        unsigned char data1;
        unsigned char data2;
        //unsigned short value, readvalue;

        ssize_t b_read1 = 1, b_read2 = 1;
        int count1 = 0, count2 = 0, count = 0;
        //FlashAF(0x0);
             
        // przestrzen 0xfe80 blokami 
        
        const bool block = true;
        const int BlockSize = 32;
        int blockIdx = 0;
        unsigned char dataBlock[BlockSize * 2 + 1];
        
        while ((b_read1 != 0) || (b_read2 != 0)) {
            data1 = 0xff;
            data2 = 0xff;
            b_read1 = fread(&data1, 1, 1, fin1);
            count1 += b_read1;
            b_read2 = 0; //fread(&data2, 1, 1, fin2);
            count2 += b_read2;

            if ((b_read1 == 0) && (b_read2 == 0))
              break; 
                     
            if (block) {
                dataBlock[blockIdx] = data2;
                dataBlock[blockIdx+1] = data1;
                blockIdx+=2;
                if (blockIdx == (BlockSize*2)) {
                    dataBlock[blockIdx] = 0;
                    StartBlockTransfer16();
                    MemoryWriteBlock(2* CB_BLCK_BUF, dataBlock, blockIdx+1);
                    EndBlockTransfer16();
                    blockIdx = 0;
                }
            }
            else {
                MemoryWrite16(CB_XLNXD, ((((int)data1 << 8) & 0xff00) | (((int)data2) & 0xff)));
            };
            /*readvalue = MemoryRead16(CB_XLNXD);
            if (readvalue != value) { 
              ostringstream ostr;
              ostr << ": CB_XLNXD read " << hex << readvalue << " expected " << value;
              throw CCUException(ostr.str().c_str());
            }*/

            if ((count++ % 1000) == 0)
              cerr << " " << count << flush;
        }
        
        if (block && (blockIdx != 0)) {
            dataBlock[blockIdx] = 0;
            StartBlockTransfer16();
            MemoryWriteBlock(2 * CB_BLCK_BUF, dataBlock, blockIdx+1);
            EndBlockTransfer16();
            blockIdx = 0;
        }
        
        // generate 10 clocks
        for (int i=0; i < 20; i++)
            MemoryWrite16(CB_XLNXD, 0xffff);
            
        // CS_A, CS_B, WRITE_A and WRITE_B down
        MemoryWrite16(CB_XLNXC, 0xff); 
            
        if ((MemoryRead16(CB_LSTAT) & 0x4000) != 0x4000)
          throw CCUException("DONE did not go up");

    } catch(...) {
        MemoryWrite16(CB_INTERF, 0x00);
        throw;
    }
    MemoryWrite16(CB_INTERF, 0x00);
}   


void TFECManagerOld::ProgramCBPC(const std::string& file1)
{
                                       
    cout << file1 << endl;                                       
    int fin1 = open(file1.c_str(), O_RDONLY);
    if (fin1 == -1)
      throw CCUException("Problems opening file '" + string(file1) + "'");
        
    //MemoryWrite16(CB_PAGE, 0);
    MemoryWrite(CBIC_W0, 0xff);
    PIAWrite(1, 0xfa);
    usleep(10);
    PIAWrite(1, 0xfe);
    usleep(10);
    MemoryWrite(CBIC_W0, 0xf7);

    int val;
    Timer.restart();
    do {
        val = MemoryRead(CBIC_R0);
        if (Timer.elapsed() > 3)
            throw CCUException(":  EMG_INIT still up after 3 seconds");
    } while (val & EMG_INIT);
                        
        
    MemoryWrite(CBIC_W0, 0xff);
    Timer.restart();
    do {
        val = MemoryRead(CBIC_R0);
        if (Timer.elapsed() > 3)
            throw CCUException(":  EMG_INIT still down after 3 seconds");
    } while (!(val & EMG_INIT));

    MemoryWrite(CBIC_W0, 0xfc);
        
        
    const bool block = true;
    const int BlockSize = 64;
    int blockIdx = 0;
    unsigned char dataBlock[BlockSize];
    
    // start sending data
    unsigned char data1;
    ssize_t b_read1 = 1;
    for (int i=0; b_read1 == 1; i++) {
        b_read1 = read(fin1, &data1, 1);
        if (b_read1 == 1) {
            if (block) {
                dataBlock[blockIdx] = data1;
                blockIdx++;
                if (blockIdx == BlockSize) {
                    MemoryWriteBlock(0xff00, dataBlock, blockIdx);
                    blockIdx = 0;
                }
            }
            else {            
                MemoryWrite(0xff00, data1);
            };
                
            if ((i % 10000) == 0)
                cout << " " << i << flush;
        }
    }
    
    if (block && (blockIdx != 0)) {
        MemoryWriteBlock(0xff00, dataBlock, blockIdx);
        blockIdx = 0;
    }
    
    close(fin1);

    // generate 10 clocks
    for (int i=0; i < 10; i++)
        MemoryWrite(0xff00, 0);
        
    MemoryWrite(CBIC_W0, 0xff);
    if (!(MemoryRead(CBIC_R0) & EMG_DONE))
        throw CCUException(": emg_DONE is down");
        
}

void TFECManagerOld::ProgramLBC(const std::string& file1)
{
                                       
    cout << file1 << endl;                                       
    int fin1 = open(file1.c_str(), O_RDONLY);
    if (fin1 == -1)
      throw CCUException("Problems opening file '" + string(file1) + "'");
        
    //MemoryWrite16(CB_PAGE, 0);
    MemoryWrite(CBIC_W0, 0xff);
    PIAWrite(1, 0xf9);
    usleep(10);
    PIAWrite(1, 0xfd);
    usleep(10);
    MemoryWrite(CBIC_W0, 0xf7);

    int val;
    Timer.restart();
    do {
        val = MemoryRead(CBIC_R0);
        if (Timer.elapsed() > 3)
            throw CCUException(":  EMG_INIT still up after 3 seconds");
    } while (val & EMG_INIT);
                        
        
    MemoryWrite(CBIC_W0, 0xff);
    Timer.restart();
    do {
        val = MemoryRead(CBIC_R0);
        if (Timer.elapsed() > 3)
            throw CCUException(":  EMG_INIT still down after 3 seconds");
    } while (!(val & EMG_INIT));

    MemoryWrite(CBIC_W0, 0xfc);
        
    const bool block = true;
    const int BlockSize = 64;
    int blockIdx = 0;
    unsigned char dataBlock[BlockSize];
        
    // start sending data
    unsigned char data1;
    ssize_t b_read1 = 1;
    for (int i=0; b_read1 == 1; i++) {
        b_read1 = read(fin1, &data1, 1);
        if (b_read1 == 1) {
            if (block) {
                dataBlock[blockIdx] = data1;
                blockIdx++;
                if (blockIdx == BlockSize) {
                    MemoryWriteBlock(0xff00, dataBlock, blockIdx);
                    blockIdx = 0;
                }
            }
            else {            
                MemoryWrite(0xff00, data1);
            };
                
            if ((i % 10000) == 0)
                cout << " " << i << flush;
        }
    }

    if (block && (blockIdx != 0)) {
        MemoryWriteBlock(0xff00, dataBlock, blockIdx);
        blockIdx = 0;
    }
        
    close(fin1);
    
    // generate 10 clocks
    for (int i=0; i < 10; i++)
        MemoryWrite(0xff00, 0);
        
    MemoryWrite(CBIC_W0, 0xff);
    if (!(MemoryRead(CBIC_R0) & EMG_DONE))
        throw CCUException(": emg_DONE is down");
}


unsigned char TFECManagerOld::PIARead(int channel) {
    //return PIA[channel]->read(); 
    return FECAcc->getPiaChannelDataReg(PIAKeys[channel]);
}

void TFECManagerOld::PIAWrite(int channel, unsigned char value) {
    //PIA[channel]->write(value);
    FECAcc->setPiaChannelDataReg(PIAKeys[channel], value);
}

void TFECManagerOld::LBWrite(int lb, uint32_t address, uint32_t data)
{
  SelectPage(lb, address);
  MemoryWrite16(lb * LB_ADDRESS_SPACE_SIZE + (address & IIPageMask), data);
}
    
uint32_t TFECManagerOld::LBRead(int lb, uint32_t address)
{
  SelectPage(lb, address);
  return MemoryRead16(lb * LB_ADDRESS_SPACE_SIZE + (address & IIPageMask));
}
    
void TFECManagerOld::LBWriteBlock(int lb, uint32_t address,
  unsigned short* data, uint32_t count)
{
  SelectPage(lb, address);
  MemoryWrite16Block(lb * LB_ADDRESS_SPACE_SIZE + (address & IIPageMask), data, count);
}
                      
void TFECManagerOld::LBReadBlock(int lb, uint32_t address,
  unsigned short* data, uint32_t count)
{
  SelectPage(lb, address);
  MemoryRead16Block(lb * LB_ADDRESS_SPACE_SIZE + (address & IIPageMask), data, count);
}
