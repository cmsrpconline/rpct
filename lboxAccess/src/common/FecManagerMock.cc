/*
 * FecManagerMock.cc
 *
 *  Created on: Aug 28, 2012
 *      Author: tb
 */

#include "rpct/lboxaccess/test/FecManagerMock.h"


#include "tb_std.h"

namespace rpct {

log4cplus::Logger FecManagerMock::logger = log4cplus::Logger::getInstance("FecManagerMock");

void FecManagerMock::setupLogger(std::string namePrefix) {
	logger = log4cplus::Logger::getInstance(namePrefix + "." + logger.getName());
}

}


