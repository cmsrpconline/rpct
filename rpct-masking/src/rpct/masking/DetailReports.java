package rpct.masking;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.math.BigDecimal;
import rpct.db.domain.equipment.ChipType;
import org.apache.myfaces.trinidad.component.UIXTable;
import org.hibernate.HibernateException;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.configuration.SynCoderConf;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.FebConnector;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.chamberloaction.BarrelOrEndcap;
import rpct.db.domain.equipment.chamberstrip.ChamberStrip;
import rpct.db.domain.equipment.feblocation.FebLocation;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.xdaq.axis.Binary;
import rpct.db.domain.configuration.ChipConfAssignment;
/**
 * @author Muhammad Imran
 * 
 * $Id: SummaryReports.java 495 2011-04-05  $
 */
public class DetailReports {
	private List<ChipsDisabledDetail> chipsDisabled = null;
	private List<DTLinesDetail> dtLinesDetail = null;
	private List<StripsDisabledDetail> sddtest = null;
	
	
	private UIXTable dataTable;
	DBDriver dbd;
	
	
	public DetailReports() throws DataAccessException{
		super();
		dbd = new DBDriver();

		try {
			dbd.configure();
			fetchData();
			
			// TODO: Add report initialization here and uncomment the finally block
		} catch (DataAccessException e) {
			e.printStackTrace();
			dbd.getContext().rollback();
		}
		
		
		
		
	}

	public UIXTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(UIXTable dataTable) {
		this.dataTable = dataTable;
	}

		

	public List<ChipsDisabledDetail> getChipsDisabled() throws DataAccessException {
	
		// TODO: merge DbState to DBDriver
		DBDriver dbDriver = new DBDriver();
		// TODO: add configure(context, ...) to use already set configuration settings 
		dbDriver.configure();
		chipsDisabled = dbDriver.getChipsDisabled(); 
		return chipsDisabled;
	}


	public List<DTLinesDetail> getDtLinesDetail() throws DataAccessException {

		// TODO: merge DbState to DBDriver
		DBDriver dbDriver = new DBDriver();
		// TODO: add configure(context, ...) to use already set configuration settings 
		dbDriver.configure();
		dtLinesDetail = dbDriver.getDtLinesDetail(); 
	
		return dtLinesDetail;
	}

	public List<StripsDisabledDetail> getStripsDisabledDetail() throws DataAccessException {

		return sddtest;
	}

	private void fetchData() throws DataAccessException {
		
		MaskingUtils.log("stripsDisabledDetail() starts");
		sddtest = getTest();
		MaskingUtils.log("stripsDisabledDetail() ends");
	}

	
	public String getDate() {
		return MaskingUtils.dateNow() + " " + MaskingUtils.timeNow();
	}
	public List<StripsDisabledDetail> getTest() throws HibernateException, DataAccessException{
		
		
		
		//sddtest.clear();
		
		if (sddtest != null)
			return sddtest;
		
		sddtest = new ArrayList<StripsDisabledDetail>();


		final List<LinkBoardWithConfiguration> allLBoards = new ArrayList<LinkBoardWithConfiguration>();

		List<? extends Crate> crates = null;
		final boolean useShortList = false;
		
/*		
		if (useShortList) {
			// report only W-2/RB1in/1Forward
			final Integer diskOrWheel = -2;
			final Integer layer = 1;
			final Integer sector = 1;
			final String subsector = "";
			final BarrelOrEndcap barrelOrEndcap = BarrelOrEndcap.Barrel;
			
			crates = dbd.equipmentDAO.getLinkBoxesByChamberLocation(diskOrWheel, layer, sector,
					subsector, barrelOrEndcap);
		}
		else {
			crates = dbd.equipmentDAO.getCratesByType(CrateType.LINKBOX, false);
		}
		for(Crate crate : crates) {
			for(Board board : crate.getBoards()) {
        		if(board.getType() == BoardType.LINKBOARD) {
        			final LinkBoard lb = (LinkBoard) board;

        			StaticConfiguration configuration = dbd.configurationManager
					.getConfiguration(lb.getSynCoder(), dbd.inputConfigKey);

        			if (configuration != null) {
        				allLBoards.add(new LinkBoardWithConfiguration(lb, (SynCoderConf) configuration));
					}
					else {
						System.out.println("LB " + lb.getName() + " has null configuration!");
					}
        		}
        	}
        }

		for (LinkBoardWithConfiguration lb: allLBoards) {
			final LinkBoard lbRaw = lb.getRawLinkBoard();
			TreeMap<Integer, Integer> stripToChannel = new TreeMap<Integer, Integer>();	
			TreeMap<Integer, Integer> lepToChannel = new TreeMap<Integer, Integer>();
			TreeMap<Integer, String> etaToChannel = new TreeMap<Integer, String>();
			TreeMap<Integer, String> febnameToChannel = new TreeMap<Integer, String>();
			
			
			for (FebLocation fl: lbRaw.getFEBLocations()) {
								
				for (FebConnector fc : fl.getFebConnectors()) {
					for (ChamberStrip cs : fc.getChamberStrips()) {
						final Integer channelNumber = DBDriver.getChannelNumber(cs);
						final Integer stripNumber = DBDriver.getStripNumber(cs);
						stripToChannel.put(channelNumber,stripNumber );
						lepToChannel.put(channelNumber,fl.getPosInLocalEtaPartition() );
						etaToChannel.put(channelNumber,fl.getFebLocalEtaPartition());
						String name = fl.getFebBoard().getName();
							
							if (name.contains("FEB_"))
								name = name.replace("FEB_", "");
						
						febnameToChannel.put(channelNumber, name);
					}
				}
			}
			
			
			Binary hexMaskNew = new Binary(lb.getEnabledChannelsMask());
			Binary hexMaskConnected = new Binary(lb.getConnectedChannelsMask());
			for (int i=0; i <hexMaskNew.getBitNum();i++){
				if (stripToChannel.containsKey(i)){
						
					boolean bitValue = hexMaskNew.getBits(i, 1) != 0;
					boolean bitConnValue = hexMaskConnected.getBits(i, 1) != 0;
					
					if (bitConnValue){
						if(bitValue)
						{
						}
						else{
							StripsDisabledDetail std = new StripsDisabledDetail();
							std.setDisable(true);
							std.setStripnumber(stripToChannel.get(i));
							std.setLbchannelnumber(i);
							std.setFebname(febnameToChannel.get(i));
							std.setLep(etaToChannel.get(i));
							std.setPosinlep(lepToChannel.get(i));
							sddtest.add(std);
						}
						
					}else{
						
					}
				}
				else{
					
				}
			}
		}			

	*/
		
		// Karol Code
		
		   
		    	long start = System.currentTimeMillis();
		    	List<ChipConfAssignment> chipConfAssignments = dbd.configurationDAO.getChipConfAssignments(ChipType.SYNCODER, dbd.inputConfigKey);
		    	//System.out.println("getChipConfAssignments() " + (System.currentTimeMillis() - start)/1000. + " s");
		    	
		    	int maskedStripsCnt = 0;
		    	int iMasked = 0;
		    	for(ChipConfAssignment chipConfAssignment : chipConfAssignments) {   
		    		SynCoderConf synCoderConf  = (SynCoderConf)chipConfAssignment.getStaticConfiguration();

		    		LinkBoard linkBoard = ((LinkBoard)chipConfAssignment.getChip().getBoard());
		    		Binary connectedMask = new Binary( linkBoard.getConnectedStripsMask() );
		    		
		    		Binary mask = new Binary(synCoderConf.getInChannelsEna());
		    		TreeMap<Integer, Integer> stripToChannel = new TreeMap<Integer, Integer>();	
					TreeMap<Integer, Integer> lepToChannel = new TreeMap<Integer, Integer>();
					TreeMap<Integer, String> etaToChannel = new TreeMap<Integer, String>();
					TreeMap<Integer, String> febnameToChannel = new TreeMap<Integer, String>();
			   		    		
		    		for(FebLocation febLocation : linkBoard.getFEBLocations()) {
		    		
		    			for(FebConnector febConnector : febLocation.getFebConnectors() ) {
		    				int linkBoardInputNum = febConnector.getLinkBoardInputNum() -1;
		    				for(ChamberStrip strip : febConnector.getChamberStrips()) {
		    					int cableChannelNum = strip.getCableChannelNum() -1;
		    					int channelNumber = linkBoardInputNum * 16 + cableChannelNum;

		    					
		    					boolean bitValue = mask.getBits(channelNumber, 1) != 0;		
		    					boolean connectedbitValue = connectedMask.getBits(channelNumber, 1) != 0;
		    					if(!bitValue && connectedbitValue) {
		    						iMasked++;
		    						//System.out.println(iMasked + " " + febLocation.getChamberLocation().getChamberLocationName() + "_" +
		    						//		febLocation.getI2cCbChannel().getCbChannel() + "_" + febLocation.getI2cLocalNumber() + " " + 
		    						//		febLocation.getFebLocalEtaPartition()  +
		    		    			//		" LB channel number "  + channelNumber + " strip number " +  strip.getChamberStripNumber());
		    						
		    						
		    						final Integer stripNumber = strip.getChamberStripNumber();
		    						stripToChannel.put(channelNumber,stripNumber );
		    						lepToChannel.put(channelNumber,febLocation.getPosInLocalEtaPartition() );
		    						etaToChannel.put(channelNumber,febLocation.getFebLocalEtaPartition());
		    						String name = febLocation.getFebBoard().getName();
		    							
		    							if (name.contains("FEB_"))
		    								name = name.replace("FEB_", "");
		    						
		    						febnameToChannel.put(channelNumber, name);
		    						
		    					}
		    				}
		    			}
		    		}
		    		
		    		for (int i=0; i < mask.getBitNum();i++){
						boolean bitValue = mask.getBits(i, 1) != 0;		
						boolean connectedbitValue = connectedMask.getBits(i, 1) != 0;
						if(!bitValue && connectedbitValue) {
							maskedStripsCnt++;
							StripsDisabledDetail std = new StripsDisabledDetail();
							std.setDisable(true);
							std.setStripnumber(stripToChannel.get(i));
							std.setLbchannelnumber(i+1);
							std.setFebname(febnameToChannel.get(i));
							std.setLep(etaToChannel.get(i));
							std.setPosinlep(lepToChannel.get(i));
							sddtest.add(std);
							
						}
					}		    
		    	}
		
		return sddtest;
	}
	
}
