package rpct.masking;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import rpct.db.domain.equipment.*;

/**
 * @author dnikolay
 * @version $Id: ChamberWithMasks.java 496 2010-02-10 21:14:11Z dnikolay $
 * 
 * Decorate ChamberLocation with Masks
 */
public class ChipsDisabledDetail {
	
	private  String febname;
	private  String lep;
	private  int posinlep;
	private  int chipid;
	private  int chippos;
	private  boolean disable;
	private String status;
	private Date modificationDate;
	private static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public String getFebname() {
		return febname;
	}
	
	public ChipsDisabledDetail() {
		super();
	}
	public ChipsDisabledDetail(String febname, int chippos, String status,
			Date modificationDate) {
		super();
		this.febname = febname;
		this.chippos = chippos;
		this.status = status;
		this.modificationDate = modificationDate;
	}
	public ChipsDisabledDetail(String febname, String lep, int posinlep,
			int chipid, int chippos, boolean disable) {
		super();
		this.febname = febname;
		this.lep = lep;
		this.posinlep = posinlep;
		this.chipid = chipid;
		this.chippos = chippos;
		this.disable = disable;
	}
	public String getFormattedModified() {
		return DATE_FORMAT.format(modificationDate);
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setFebname(String febname) {
		this.febname = febname;
	}
	public String getLep() {
		return lep;
	}
	public void setLep(String lep) {
		this.lep = lep;
	}
	public int getPosinlep() {
		return posinlep;
	}
	public void setPosinlep(int posinlep) {
		this.posinlep = posinlep;
	}
	public int getChipid() {
		return chipid;
	}
	public void setChipid(int chipid) {
		this.chipid = chipid;
	}
	public int getChippos() {
		return chippos;
	}
	public void setChippos(int chippos) {
		this.chippos = chippos;
	}
	public boolean isDisable() {
		return disable;
	}
	public void setDisable(boolean disable) {
		this.disable = disable;
	}
	@Temporal(TemporalType.TIMESTAMP)
    public Date getModificationDate() {
        return modificationDate;
    }
    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }
    	
	
	
}
