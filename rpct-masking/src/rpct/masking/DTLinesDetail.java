package rpct.masking;

import java.text.SimpleDateFormat;
import java.util.Date;

import rpct.db.domain.equipment.*;

/**
 * @author dnikolay
 * @version $Id: ChamberWithMasks.java 496 2010-02-10 21:14:11Z dnikolay $
 * 
 * Decorate ChamberLocation with Masks
 */
public class DTLinesDetail {
	
	private  String febname;
	private  String lep;
	private  int posinlep;
	private  int boardid;
	private  boolean disable;
	private int startingstrip;
	private int endingstrip;
	private String status;
	private Date modificationDate;
	private Board board;
	private static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public DTLinesDetail(String febname, String lep, int posinlep, int boardid,
			boolean disable, int startingstrip, int endingstrip) {
		super();
		this.febname = febname;
		this.lep = lep;
		this.posinlep = posinlep;
		this.boardid = boardid;
		this.disable = disable;
		this.startingstrip = startingstrip;
		this.endingstrip = endingstrip;
	}
	
	public String getFormattedModified() {
		return DATE_FORMAT.format(modificationDate);
	}
	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	public DTLinesDetail() {
		super();
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public String getFebname() {
		return febname;
	}
	public void setFebname(String febname) {
		this.febname = febname;
	}
	public String getLep() {
		return lep;
	}
	public void setLep(String lep) {
		this.lep = lep;
	}
	public int getPosinlep() {
		return posinlep;
	}
	public void setPosinlep(int posinlep) {
		this.posinlep = posinlep;
	}
	public int getBoardid() {
		return boardid;
	}
	public void setBoardid(int boardid) {
		this.boardid = boardid;
	}
	public boolean isDisable() {
		return disable;
	}
	public void setDisable(boolean disable) {
		this.disable = disable;
	}
	public int getStartingstrip() {
		return startingstrip;
	}
	public void setStartingstrip(int startingstrip) {
		this.startingstrip = startingstrip;
	}
	public int getEndingstrip() {
		return endingstrip;
	}
	public void setEndingstrip(int endingstrip) {
		this.endingstrip = endingstrip;
	}

}

