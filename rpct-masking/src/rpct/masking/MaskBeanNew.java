package rpct.masking;

import org.apache.myfaces.trinidad.component.UIXTable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import rpct.datastream.TestOptionsXml.ChipSource;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ChipDisabledHistory;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.FebBoardDTControlled;
import rpct.db.domain.configuration.FebChipConf;
import rpct.db.domain.configuration.FebDTControlledHistory;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.SynCoderConf;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.FebBoard;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.feblocation.FebLocation;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.synchro.DefOut;

import rpct.db.domain.equipment.Chip;
import java.io.StringWriter;
import java.util.Map.Entry;
import java.util.*;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.FebChipConf;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.SynCoderConf;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.FebBoard;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.feblocation.FebLocation;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.synchro.DefOut;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import org.apache.catalina.connector.Connector;
import org.apache.log4j.Logger;
import org.apache.myfaces.trinidad.component.UIXValue;
import org.apache.myfaces.trinidad.component.core.nav.CoreCommandButton;
import org.apache.myfaces.trinidad.context.RequestContext;
import org.apache.myfaces.trinidad.event.RangeChangeEvent;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.configuration.SynCoderConf;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.FebConnector;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.chamberloaction.BarrelOrEndcap;
import rpct.db.domain.equipment.chamberstrip.ChamberStrip;
import rpct.db.domain.equipment.feblocation.FebLocation;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.masking.utils.Division;
import rpct.xdaq.axis.Binary;

/**
 * @author Muhammad Imran
 * 
 * $Id: MaskBeanNew.java 495 2011-03-20  $
 */
public class MaskBeanNew {
	private static Logger logger = Logger.getLogger(MaskBeanNew.class);

	private boolean barrelCheck ;

	private UIXTable dataTable;

	
	
	private String title= "J1";

	private String rpcDesc="";
	private String dtDesc="";
	private UIXTable maskHistoryTable;

	private int vthglobal=90;
	private int vmonglobal=1434;
	
	private List<DBDriver.ParamModified> dbpm = null;
	
	
	public UIXTable getMaskHistoryTable() {
		return maskHistoryTable;
	}
	public void setMaskHistoryTable(UIXTable maskHistoryTable) {
		this.maskHistoryTable = maskHistoryTable;
	}
	public List<DBDriver.ParamModified> getDbpm() throws DataAccessException {
		
		return dbpm;
	}
	public void setDbpm(List<DBDriver.ParamModified> dbpm) {
	this.dbpm = dbpm;
	}
	public int getVthglobal() {
		return vthglobal;
	}
	public void setVthglobal(int vthglobal) {
		this.vthglobal = vthglobal;
	}
	public int getVmonglobal() {
		return vmonglobal;
	}
	public void setVmonglobal(int vmonglobal) {
		this.vmonglobal = vmonglobal;
	}



	public boolean isBarrelCheck() {
		return barrelCheck;
	}

	
	
	public String getTitle() {
	
		if (dataTable.isShowAll()){
			setTitle("All");
			}
		else if(dataTable.getFirst()==0)
			setTitle("J1");
		else if(dataTable.getFirst()==2)
			setTitle("J2");
		else if(dataTable.getFirst()==4)
			setTitle("J3");
		else if(dataTable.getFirst()==6)
			setTitle("J4");
		else if(dataTable.getFirst()==8)
			setTitle("J5");
		else if(dataTable.getFirst()==10)
			setTitle("J6");
		
		return title;
	}



	public String getDtDesc() {
		
		int index= dataTable.getRowIndex();

		if(febChipInfo.get(index).isDtControl())
		{
			//System.out.println("true");
//		rpcDesc = "Select Main RPC Line for Threshold Configuration";
		dtDesc = "Redundant DT Line in use";
		}
	else{
		//System.out.println("false");
		//rpcDesc = "Main RPC Line is in use";
		dtDesc = "Select redundant DT Line for Threshold Configuration";
		
	}
	
		
		return dtDesc;
	}



	public void setDtDesc(String dtDesc) {
		this.dtDesc = dtDesc;
	}



	public String getRpcDesc() {
		
		int index= dataTable.getRowIndex();
		if(febChipInfo.get(index).isDtControl())
			{
			rpcDesc = "Select Main RPC Line for Threshold Configuration";
			//dtDesc = "Redundant DT Line in use";
			}
		else{
			rpcDesc = "Main RPC Line is in use";
			//dtDesc = "Select redundant DT Line for Threshold Configuration";
			
		}
		

		return rpcDesc;
	}



	public void setRpcDesc(String rpcDesc) {
		this.rpcDesc = rpcDesc;
	}



	public void setTitle(String title) {
		this.title = title;
		
	}



	public void setBarrelCheck(boolean barrelCheck) {
		this.barrelCheck = barrelCheck;
	}

	public UIXTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(UIXTable dataTable) {
		this.dataTable = dataTable;
	}

	public static class FebInfo {
		private  String name;
		private  int position;
		private  String eta;
		private Chip chip;	
		private FebChipConf fcc;
		private int lep;
		private List<Bit> newStripBits = new ArrayList<Bit>();
		private List<Bit> newStripBitsConnected = new ArrayList<Bit>();
		private boolean dtControl;
		private boolean thresholdDisable;
		private boolean showDtControl;
		private boolean chipEnable;
		
		
		public boolean isThresholdDisable() {
			return thresholdDisable;
		}
		public void setThresholdDisable(boolean thresholdDisable) {
			this.thresholdDisable = thresholdDisable;
		}
		public boolean isDtControl() {
			return dtControl;
		}


		public boolean isShowDtControl() {
			return showDtControl;
		}


		public void setShowDtControl(boolean showDtControl) {
			this.showDtControl = showDtControl;
		}


		public void setDtControl(boolean dtControl) {
			this.dtControl = dtControl;
		}


	


		public boolean isChipEnable() {
			return chipEnable;
		}


		public void setChipEnable(boolean chipEnable) {
			this.chipEnable = chipEnable;
		}


		public String getName() {
			return name;
		}
		
		
		public int getLep() {
			return lep;
		}


		public void setLep(int lep) {
			this.lep = lep;
		}


		public Chip getChip() {
			return chip;
		}


		public List<Bit> getNewStripBits() {
			return newStripBits;
		}


		public void setNewStripBits(List<Bit> newStripBits) {
			this.newStripBits = newStripBits;
		}


		public List<Bit> getNewStripBitsConnected() {
			return newStripBitsConnected;
		}


		public void setNewStripBitsConnected(List<Bit> newStripBitsConnected) {
			this.newStripBitsConnected = newStripBitsConnected;
		}


		public void setChip(Chip chip) {
			this.chip = chip;
		}


		public String getEta() {
			return eta;
		}


		public void setEta(String eta) {
			this.eta = eta;
		}


		public void setName(String name) {
			this.name = name;
		}
		public int getPosition() {
			return position;
		}
		public void setPosition(int position) {
			this.position = position;
		}
		public FebChipConf getFcc() {
			return fcc;
		}
		public void setFcc(FebChipConf fcc) {
			this.fcc = fcc;
		}
	
		
		private FebInfo(String name, int position, FebChipConf fcc) {
			this.name = name;
			this.position = position;
			this.fcc = fcc;
		}
		

		
	}
	
	public static class Bit {
		private final String name;
		private  String channelnumber;
		private  String stripnumber;

		private String febeta;
		private boolean value;


		private Bit(String name, boolean value) {
			this.name = name;
			this.value = value;
		}

		private Bit(String name, boolean value, String channelnumber,String stripnumber,String febeta) {
			this.name = name;
			this.value = value;
			this.channelnumber=channelnumber;
			this.stripnumber = stripnumber;
			this.febeta = febeta;
		}

		public String getFebeta() {
			return febeta;
		}

		public void setFebeta(String febeta) {
			this.febeta = febeta;
		}

		public void setChannelnumber(String channelnumber) {
			this.channelnumber = channelnumber;
		}

		public void setStripnumber(String stripnumber) {
			this.stripnumber = stripnumber;
		}

		public String getChannelnumber() {
			return channelnumber;
		}

		public String getStripnumber() {
			return stripnumber;
		}

		public String getName() {
			return name;
		}

		public boolean getValue() {

			return value;
		}

		public void setValue(boolean value) {
			this.value = value;
		}

		// A bit tagged as dummy:
		private static final String DUMMY_BIT_NAME = "A.DUMMY.BIT";

		public static Bit dummy() {
			return new Bit(DUMMY_BIT_NAME, false);
		}

		public boolean isDummy() {
			return this.name.equals(DUMMY_BIT_NAME);
		}
	}

	/**
	 * The mask (1 bit per strip) of enabled strips
	 */
	private Binary hexMask;

	List<FebChipConf> original = new ArrayList<FebChipConf>();

	/**
	 * To keep track of the history Updated when mask changes (besides the ctor
	 * in setSelectedStrips() only)
	 */
	
	private Binary oldHexMask;

	/**
	 * The mask of connected strips
	 * Immutable once fetched from DB 
	 */
	private Binary hexMaskConnected;

	/**
	 * Tied to the hexMask
	 * Used by the presentation
	 */
	private List<Bit> stripBits = new ArrayList<Bit>();

	private List<FebInfo> febChipInfo = new ArrayList<FebInfo>();
	
	public List<FebInfo> getFebChipInfo() {
		return febChipInfo;
	}

	public void setFebChipInfo(List<FebInfo> febChipInfo) {
		this.febChipInfo = febChipInfo;
	}

	public List<Bit> getStripBitsConnected() {
		return stripBitsConnected;
	}

	/**
	 * Tied to the hexMaskConnected
	 * Used by the presentation
	 */
	private List<Bit> stripBitsConnected = new ArrayList<Bit>();

	private final static int NUMBER_OF_STRIP_COLUMNS = 10;

	public int getNumberOfColumns() {
		return NUMBER_OF_STRIP_COLUMNS;
	}

	/**
	 * @return range 0 (NumberOfRows - 1)
	 */
	public List<Integer> getTableRows() {
		final int numRows = getNumberOfStrips() / NUMBER_OF_STRIP_COLUMNS + 1;

		List<Integer> rowIndexes = new ArrayList<Integer>(numRows);
		for (int i = 0; i < numRows; i++)
			rowIndexes.add(i);

		return rowIndexes;
	}

	public MaskBeanNew() throws DataAccessException {
		// load from DB

		loadMaskBytes();
	}

	public String getNewHexMask() {
		syncHexMask();
		return hexMask.toString();
	}

	public String getOldHexMask() {
		return oldHexMask.toString();
	}

	public List<Bit> getStripBits() {
		return stripBits;
	}

	public int getNumberOfStrips() {
		int result = 0;

		result = maskMapper.getNumberMax();

		// Ugly, but fast solution
		// TODO: make it pretty
		switch (result) {
		case 47:
		case 95:
			result ++;
			break;
		case 46:
		case 94:
			result += 2;
			break;
		}
		
		return result;
	}

	/**
	 * Load the mask, corresponding to the current chamber coordinates
	 * @throws DataAccessException 
	 * @throws IllegalArgumentException
	 */
	private void loadMaskBytes() throws DataAccessException,
			IllegalArgumentException {
		
	//	maskMapper = MaskMapper.CHANNEL;
//		ChamberPositionBean position = (ChamberPositionBean) FacesContext
//				.getCurrentInstance().getExternalContext().getSessionMap().get(
//						"chamberPosition");
	    RequestContext requestContext = RequestContext.getCurrentInstance();
	    ChamberPositionBean position = (ChamberPositionBean) requestContext.getPageFlowScope().get("chamberPosition");

		//System.out.println("Position"+position.getLocation());

	    
	   // ChamberPositionBean position = new ChamberPositionBean();
	    
	   // position.setLocation("RE+1/2/1");
	
		if (position != null) {
			logger.info("loadMaskBytes() of " + position.toString());

			final boolean useDB = true;

			if (useDB) {
				try {
					System.err.println("Fetching Mask from DB");
					fetchMaskFromDB(position.getLocation());
				} catch (DataAccessException e) {
					e.printStackTrace();
					System.err.println("Error on canceling session!");
				}
			} else {
				// imitate fetching
				hexMask = new Binary("f10f00000000000000000000");
			}

			System.err.println("hexMask numberOfBits: " + hexMask.getBitNum());
			System.out.println("Testing position : "+position);

			System.out.println("New MaskMapper : "+maskMapper);

			System.out.println("Testing position : "+position.getEtaPartition()+" :: "+position.getLayer()+" :: "+position.getLayerNumber()+" :: "+position.getSection()+" :: "+position.getSector()+" :: "+position.getSubsector());
			
			System.out.println("HexMAsk: " + hexMask);
			System.out.println("mapper  numberOfBits: " + hexMaskConnected);
				
			System.out.println("mapper  numberOfBits: " + getNumberOfStrips());
			
			oldHexMask = hexMask;
			System.out.println("loadMaskBytes(): oldHexMask set to "+ oldHexMask.toString());
		} else {
			throw new IllegalArgumentException("ChamberPositionBean is null!");
		}

		syncStripBits();
	
		//System.out.println("StripsBits: " + stripBits.size());
		//System.out.println("StripsBitsConnected: " + stripBitsConnected.size());
		
	}

	private StringWriter loggerWeb = new StringWriter();

	private void presentLog() {
		// TODO: consider removing 
		//		loggerUI.setValue(logger.toString());
	}

	/**
	 * @author dnikolay
	 *
	 * Keeps the state between loading from and saving to DB   
	 */
	private class DBState {
		private HibernateContext context = null;
		private EquipmentDAO equipmentDAO = null;
		private ConfigurationManager configurationManager = null;
		private SynCoderConf coderConf = null;
		private ConfigurationDAO configurationDAO = null;
		private LinkBoard lb = null;
	}

	private DBState dbState = new DBState();

	/**
	 * @param position Chamber position name
	 * @return 
	 * @return mask
	 * @throws DataAccessException
	 * @throws IllegalArgumentException
	 * 
	 * sets hexMask{,Connected};
	 */
	private void fetchMaskFromDB(String position) throws DataAccessException,
			IllegalArgumentException {
		byte[] dbMaskBytes = null;
		byte[] dbMaskBytesConnected = null;
		// Following rpct-db/src/main/java/rpct/testAndUtils/PutStripMasksToDB.java
		if (dbState.context == null) {
			dbState.context = new SimpleHibernateContextImpl();
			dbState.equipmentDAO = new EquipmentDAOHibernate(dbState.context);
			dbState.configurationDAO = new ConfigurationDAOHibernate(
					dbState.context);
			dbState.configurationManager = new ConfigurationManager(
					dbState.context, dbState.equipmentDAO,
					dbState.configurationDAO);
		}

		try {
			//the source of the configuration data, on which the correction will be applied
			LocalConfigKey inputConfigKey = dbState.configurationManager
					.getLocalConfigKey(ConfigurationManager.CURRENT_LBS_CONF_KEY);
			LocalConfigKey localConfigKeyFEBs = dbState.configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_FEBS_DEFAULT);

		//	LocalConfigKey localConfigKeyFEBs = dbState.configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_FEBS_TEST_CONF);

			
			Map<String, LinkBoard> chamberBoardsMap = new HashMap<String, LinkBoard>();

			

			// See CHAMBERLOCATION and FEBLOCATION
			// See http://cmslxr.fnal.gov/lxr/source/DataFormats/MuonDetId/interface/RPCDetId.h

			/* Convert from ChamberPositionBean to rpct-db coordinates (diskOrWheel, layer, sector, subsector, barrelOrEndcap)
			 * BARREL: (section, layer, sector, subsector) -> (diskOrWheel, layer, sector, subsector)
			 * ENDCAP:                                        (diskOrWheel, localetapartition, sector, layer), 
			 *        etaPartition -> rpct-db.subsector is undef
			 */
//			ChamberPositionBean cpBean = (ChamberPositionBean) FacesContext
//					.getCurrentInstance().getExternalContext().getSessionMap()
//					.get("chamberPosition");
			RequestContext requestContext = RequestContext.getCurrentInstance();
		    ChamberPositionBean cpBean = (ChamberPositionBean) requestContext.getPageFlowScope().get("chamberPosition");

			Division division = cpBean.getDivision();

			Integer diskOrWheel = Integer.valueOf(cpBean.getSection()
					.replaceFirst("\\+", ""));

			Integer layer = null;
			Integer sector = null;
			String subsector = null;
			BarrelOrEndcap barrelOrEndcap = null;

			switch (division) {
			case BARREL:
				barrelOrEndcap = BarrelOrEndcap.Barrel;
				layer = cpBean.getLayerNumber();
				sector = Integer.valueOf(cpBean.getSector());
				subsector = cpBean.getSubsector();
				break;
			case ENDCAP:
				barrelOrEndcap = BarrelOrEndcap.Endcap;
				layer = cpBean.getLayerNumber();
				sector = Integer.valueOf(cpBean.getSector());
				subsector = null;
				break;
			}
			System.out.format("Coordinates of %s: (%d, %d, %d, '%s', %s)%n",
					cpBean.toString(), diskOrWheel, layer, sector, subsector,
					barrelOrEndcap);
			System.out.println("linkboxes size: "
					+ dbState.equipmentDAO.getLinkBoxesByChamberLocation(
							diskOrWheel, layer, sector, subsector,
							barrelOrEndcap).size());
			for (Crate crate : dbState.equipmentDAO
					.getLinkBoxesByChamberLocation(diskOrWheel, layer, sector,
							subsector, barrelOrEndcap)) {
			//	System.out.format("Got crate: <%s> with %d boards%n", crate
					//	.toString(), crate.getBoards().size());
				for (Board board : crate.getBoards()) {
					if (board.getType() == BoardType.LINKBOARD) {
						LinkBoard lb = (LinkBoard) board;
				//		System.out.println("Got lb: " + lb.toString());
						for (String chamberName : lb.getChamberNames()) {
						//	System.out.println("   Serving " + chamberName);
							if (chamberBoardsMap.containsKey(chamberName)) {
								throw new RuntimeException("Chambername <"
										+ chamberName + "> already inserted!");
							}
							chamberBoardsMap.put(chamberName.replaceFirst(
									"[ ]+", ""), lb);
						}
					}
				}
			}

			//System.out.println("Chamber names: "
			///		+ new TreeMap<String, LinkBoard>(chamberBoardsMap));

			//System.out.println("Chamber names: "
			//		+ new TreeMap<String, LinkBoard>(chamberBoardsMap));

			
			final String chamberName = position.replaceFirst("[ ]+", "");
			dbState.lb = chamberBoardsMap.get(chamberName);

			if (dbState.lb == null)
				throw new IllegalArgumentException("FEB location '"
						+ cpBean.toString() + "' not found in DB!");

		//	System.out.println("Selected lb: " + dbState.lb.toString());
			loggerWeb.write(String.format(
					"LB name: <b>%s</b> of chamber <b>%s</b>%n", dbState.lb
							.getName(), chamberName));

			//FebChipConf original = new FebChipConf();
			if(barrelOrEndcap.name().equalsIgnoreCase("BARREL"))
				setBarrelCheck(true);
			else
				setBarrelCheck(false);

			int even = 0;
			
			for(Chip febChip :dbState.lb.getFebChips()) {					
			//	System.out.print(position++);
				if(febChip != null) {
				//	System.out.println(" Feb Name " + febChip.getBoard().getName() + " Feb Eta Partition "
				//		+ ((FebBoard)febChip.getBoard()).getFebLocation().getFebLocalEtaPartition() + " "  
				//		+ ((FebBoard)febChip.getBoard()).getFebLocation().getPosInLocalEtaPartition() +" Position " + febChip.getPosition());

					
					//to take the settings from the DB
					
					FebChipConf fcc =  (FebChipConf)dbState.configurationManager.getConfiguration(febChip, localConfigKeyFEBs);
				
									
					FebChipConf temp = new FebChipConf();
					temp.setId(fcc.getId());
					temp.setVth(fcc.getVth());
					temp.setVthOffset(fcc.getVthOffset());
					temp.setVmon(fcc.getVmon());
					temp.setVmonOffset(fcc.getVmonOffset());
				
					
					
					FebChipConf temp1 = new FebChipConf();
					temp1.setId(fcc.getId());
					temp1.setVth(fcc.getVth());
					temp1.setVthOffset(fcc.getVthOffset());
					temp1.setVmon(fcc.getVmon());
					temp1.setVmonOffset(fcc.getVmonOffset());
					
			
					if(original.size()<12)
						original.add(temp);
				
					String name = febChip.getBoard().getName();
					
					if (name.contains("FEB_"))
						name = name.replace("FEB_", "");
					
					boolean enable = false;
					if(febChip.getDisabled()==null)
						enable=true;
					
			    	FebBoardDTControlled febBoardDTControlled = dbState.configurationDAO.getFebDtControlled(febChip.getBoard());

			    	boolean dtcontrol = true;
					if(febBoardDTControlled==null)
						dtcontrol=false;
			
			    	//System.out.println("Board "+febChip.getBoard()+" : "+febBoardDTControlled);
			    		
					FebInfo febinfo = new FebInfo(name, febChip.getPosition(),temp1);
					febinfo.setEta(((FebBoard)febChip.getBoard()).getFebLocation().getFebLocalEtaPartition());
					febinfo.setChip(febChip);
					febinfo.setChipEnable(enable);
					febinfo.setLep(((FebBoard)febChip.getBoard()).getFebLocation().getPosInLocalEtaPartition());
				
					if(even%2==0)
						{febinfo.setShowDtControl(true);
						febinfo.setDtControl(dtcontrol);
						}
					else
						febinfo.setShowDtControl(false);
							
					
					febChipInfo.add(febinfo);
					//System.out.println("Feb Chip ID : "+febChip.getId());
						
					even++;
				}
				else
					System.out.println(" null"); 
					
			}
			
	
			
			
			// Used in MaskMapper initialization
			// TreeMap used for ordered indexes
			final TreeMap<Integer, Integer> conjugateToChannel = new TreeMap<Integer, Integer>();


			// Map of etaPart to conjugateToChannel
			final Map<String, SortedMap<Integer, Integer>> etaPartConjugateToChanel = new TreeMap<String, SortedMap<Integer,Integer>>();
			conjugateToEtaPart = new HashMap<Integer, String>();
		
			for (FebLocation fl : dbState.lb.getFEBLocations()) {
			    String etaPartitionFL = fl.getFebLocalEtaPartition();
				logger.debug("etaPartitionFL is '" + etaPartitionFL + "'");
			
				
				//System.out.format("Got fl: (%d, %s, %s)%n", fl.getId(), fl.getChamberLocation().getChamberLocationName(), etaPartitionFL);
				// Endcap doesn't have etaPart attached - all (A,B,C) are merged in one big chamber 
				final String chamberNameFL = fl.getChamberLocation().getChamberLocationName() + 
				(cpBean.getDivision() == Division.BARREL ? etaPartitionFL : "");
				//etaPartitionFL = etaPartitionFL+fl.getPosInLocalEtaPartition();
				if (chamberNameFL.equals(chamberName)) {
					logger.debug(String.format(
							"This fl %d matches target chambername %s%n", fl
									.getId(), chamberName));
					logger.debug(String.format("fl %d has %d fcs%n", fl.getId(), fl
							.getFebConnectors().size()));
					
					if (! etaPartConjugateToChanel.containsKey(etaPartitionFL))
						etaPartConjugateToChanel.put(etaPartitionFL, new TreeMap<Integer, Integer>());
					final Map<Integer, Integer> thisEtaPartConjugateToChanel = etaPartConjugateToChanel.get(etaPartitionFL);
					
					for (FebConnector fc : fl.getFebConnectors()) {
						for (ChamberStrip cs : fc.getChamberStrips()) {
							
							final Integer channelNumber = MaskMapper.CHANNEL.getNumber(cs);
							final Integer conjugateNumber = maskMapper.STRIP.getNumber(cs);
					
							thisEtaPartConjugateToChanel.put(conjugateNumber,channelNumber );
						
						//	System.out.println("Channel "+channelNumber + " Strip No "+conjugateNumber );
						}
					}
				}
			}

			// Merge etaPartitions of (the sorted)etaPartConjugateToChanel
			int conjugateOffset = 0;
			//System.out.println("keyset "+etaPartConjugateToChanel.keySet());
			for (String etaPart: etaPartConjugateToChanel.keySet()) {
				final SortedMap<Integer, Integer> m = etaPartConjugateToChanel.get(etaPart);
				
				
			//	System.out.println("Merging " + m.toString());
				for (Entry<Integer, Integer> e: m.entrySet()) {
					final Integer conjugateNumber = e.getKey() + conjugateOffset;
					final Integer channelNumber = e.getValue();
					
			//		System.out.println("Channel "+channelNumber + " Conjugate "+conjugateNumber );
								
					conjugateToChannel.put(channelNumber,conjugateNumber);
					conjugateToEtaPart.put(channelNumber, etaPart);
				}
				if (maskMapper != MaskMapper.CHANNEL)
					// Only non-CHANNEL mappers have to be rearanged
					// It is assumed that conjugateNumber returned by maskMapper starts (at least) from 1!
					//    (if from 0, overlapping of the last previous and the first next will occur)
					conjugateOffset += m.lastKey();
			}
			
			//System.out.println("Last Key"+conjugateToChannel.lastKey());

			maskMapper.setMapConjugateToChannel(conjugateToChannel);

			//System.out.println(maskMapper.longDescription());

			StaticConfiguration configuration = dbState.configurationManager
					.getConfiguration(dbState.lb.getSynCoder(), inputConfigKey);

		//	configuration.getChipType
			
			
			
				
			
			// TODO: move the verification upper
			if (configuration != null) {
				dbState.coderConf = (SynCoderConf) configuration;
				dbMaskBytes = dbState.coderConf.getInChannelsEna();
				dbMaskBytesConnected = dbState.lb.getConnectedStripsMask();

				//System.out.println("dbMaskBytes "+dbMaskBytes);
				//System.out.println("dbMaskBytesConnected "+dbMaskBytesConnected);

				
				hexMaskConnected = new Binary(dbMaskBytesConnected);
				

				
				loggerWeb
						.write(String
								.format(
										"Masks read:%n"
												+ "   * <tt>ConnectedStripsMask():</tt> <big>%s</big>%n"
												+ "   * <tt>InChannelsEna():&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</tt> <big>%s</big>%n",
										new Binary(dbMaskBytesConnected)
												.toString().toUpperCase(),
										new Binary(dbMaskBytes).toString()
												.toUpperCase()));
				// final mask is the connected AND enabled
				for (int i = 0; i < dbMaskBytes.length; i++) {
					dbMaskBytes[i] = (byte) (dbMaskBytes[i] & dbMaskBytesConnected[i]);
				}

				hexMask = new Binary(dbMaskBytes);
				hexMaskConnected = new Binary(dbMaskBytesConnected);
				
				//System.out.println("hexMask : "+hexMask);
				//System.out.println("hexMaskConnected : "+hexMaskConnected);

			} else {
				loggerWeb.write("no configuration found for the LB "
						+ dbState.lb.getName() + " !!!!!!!!!!!!!!!!!!\n");
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			dbState.context.rollback();
			loggerWeb.write("context.rollback()\n");
		} finally {
			dbState.context.closeSession();
		}

		presentLog();
	}

	/**
	 * save mask to DB
	 * @throws DataAccessException 
	 */
	public void saveMask(ActionEvent ae) throws DataAccessException {
	
		syncHexMask();
		final boolean saveToDB = true;
		try {
		loggerWeb.write(String.format("[saveMask] old: %s, new: %s%n", oldHexMask
				.toString().toUpperCase(), hexMask.toString().toUpperCase()));
		presentLog();

		// TODO: Save if the new mask is different from the old one
		// oldHexMask doesn't change with hexMask - stays the same as initially read from DB?
		if (Arrays.equals(hexMask.getBytes(), oldHexMask.getBytes())) {
			loggerWeb
					.write(String
							.format("   * newMask = oldMask   * <big>Mask Saving Ignored!</big>%n"));
			presentLog();
		}
		else{
			
			
				
			LocalConfigKey outputConfigKey = dbState.configurationManager.getLocalConfigKey(ConfigurationManager.CURRENT_LBS_CONF_KEY);
			SynCoderConf newCoderConf = new SynCoderConf(dbState.coderConf);

			final byte[] channelBytes = hexMask.getBytes();

			newCoderConf.setInChannelsEna(channelBytes);
			loggerWeb.write(String.format("   * Trying to save %s ...%n",
					new Binary(newCoderConf.getInChannelsEna()).toString()
							.toUpperCase()));
			
			dbState.configurationDAO.saveObject(newCoderConf);
			dbState.configurationManager.assignConfiguration(dbState.lb.getSynCoder(),	newCoderConf, outputConfigKey.getName());
			dbState.configurationDAO.flush();
			dbState.context.commit();
			loggerWeb.write(String.format("   * New mask saved%n"));
			presentLog();
			
			
		}

		if (saveToDB) {

			LocalConfigKey localConfigKeyFEBs = dbState.configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_FEBS_DEFAULT);

//			LocalConfigKey localConfigKeyFEBs = dbState.configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_FEBS_TEST_CONF);

			if(barrelCheck){
				for (int i=0; i<febChipInfo.size();i+=2){
					dbState.configurationManager.setFebDtControlled(febChipInfo.get(i).getChip().getBoard(), febChipInfo.get(i).isDtControl());
					
					FebDTControlledHistory dthistory = new FebDTControlledHistory();
					dthistory.setBoard(febChipInfo.get(i).getChip().getBoard());
					String status = "";
					if (febChipInfo.get(i).isDtControl())
						status = "DT";
					else
						status = "RPC";
					dthistory.setStatus(status);
					//loggerWeb.write("Saving New Threshold Values: Vth = ");
					//presentLog();
					//dbState.configurationDAO.saveObject(dthistory);
				}
			}
				for (int i=0; i<febChipInfo.size();i++){
					
					FebChipConf oldConf = febChipInfo.get(i).getFcc();
					FebChipConf newConf = new FebChipConf();
					FebChipConf org = new FebChipConf(original.get(i));
					dbState.configurationManager.enableChip(febChipInfo.get(i).getChip(),febChipInfo.get(i).isChipEnable());
					
					ChipDisabledHistory chiphistory = new ChipDisabledHistory();
					chiphistory.setChip(febChipInfo.get(i).getChip());
					String status = "";
					if (febChipInfo.get(i).isChipEnable())
						status = "Enable";
					else
						status = "Disable";
					chiphistory.setStatus(status);
					
					//System.out.println("Original Vth: "+org.getVth() + " Changed Vth "+oldConf.getVth());
					//System.out.println("Original ID: "+org.getId() + " Changed ID "+oldConf.getId());
					
					if(org.getVth()==oldConf.getVth() 
							&& org.getVthOffset()==oldConf.getVthOffset()
							&& org.getVmon()==oldConf.getVmon()
							&& org.getVmonOffset()==oldConf.getVmonOffset()){
						
					}
					else{		
						newConf.setVth(oldConf.getVth());
						newConf.setVmon(oldConf.getVmon());
						newConf.setVthOffset(oldConf.getVthOffset());
						newConf.setVmonOffset(oldConf.getVmonOffset());
						dbState.configurationDAO.saveObject(newConf); 
						dbState.configurationManager.assignConfiguration(febChipInfo.get(i).getChip(), newConf, localConfigKeyFEBs.getName());
						original.get(i).setVmon( newConf.getVmon());
						original.get(i).setVth( newConf.getVth());
						original.get(i).setVmonOffset( newConf.getVmonOffset());
						original.get(i).setVthOffset( newConf.getVthOffset());
						dbState.configurationDAO.flush();
						dbState.context.commit();
							
						loggerWeb.write("Saving New Threshold Values: Vth = "+newConf.getVth()+" and Vmon = "+newConf.getVmon() +"\n");
				presentLog();
						
					}
				}
				
		
			} else {
			loggerWeb.write(String.format("*Dummy* saving new mask %s done.",
					hexMask.toString().toUpperCase()));
		}
		} catch (DataAccessException e) {
			e.printStackTrace();
			loggerWeb.write(e.getMessage());
			loggerWeb.flush();
			dbState.context.rollback();
		} finally {
			dbState.context.closeSession();
			loggerWeb.write(String.format("   * DB Session closed%n"));
		}
		//FacesContext.getCurrentInstance().renderResponse(); 
		presentLog();
	}

	/**
	 * Used to convert from/to channel number
	 *
	 * A variant of "State" Pattern
	 * A "Decorator" of a subset of Map
	 */
	private enum MaskMapper {
		CHANNEL {
			@Override
			public int getNumber(ChamberStrip cs) {
				final int lbInputNum = cs.getFebConnector()
						.getLinkBoardInputNum() - 1;
				final int cableChannelNum = cs.getCableChannelNum();
				final int channelsPerFEB = 16;
				final int channelNumber = lbInputNum * channelsPerFEB
						+ cableChannelNum;

				return channelNumber;
			}
		},
		STRIP {
			@Override
			public int getNumber(ChamberStrip cs) {
				return cs.getChamberStripNumber();
			}
		},
		STRIPCMS {
			@Override
			public int getNumber(ChamberStrip cs) {
				return cs.getCmsStripNumber();
			}
		};

		// TODO: TreeMap is good only for getNumberMax():
		//       consider replacing it with a HashMap for a better performance
		private TreeMap<Integer, Integer> mapConjugateToChannel;


		public void setMapConjugateToChannel(
				TreeMap<Integer, Integer> mapConjugateToChannel) {
			this.mapConjugateToChannel = mapConjugateToChannel;

		}

	
		

		// Delegate to Map
		public Set<Integer> keySet() {
			return mapConjugateToChannel.keySet();
		}

		public Integer get(Integer key) {
			return mapConjugateToChannel.get(key);
		}

		public boolean containsKey(Integer key) {
			return mapConjugateToChannel.containsKey(key);
		}

		public String longDescription() {
			return "MaskMapper."
					+ this.name()
					+ " map: "
					+ (mapConjugateToChannel == null ? null
							: mapConjugateToChannel.toString());
		}

		// Delegation ends

		// Specific methods
		public int getNumberMax() {
			// this is a TreeMap - the keys are ordered
			// Not good when in "Channel" view if the last channel is not connect
			return mapConjugateToChannel.lastKey();
		}
		

		abstract int getNumber(ChamberStrip cs);
	}

	private MaskMapper maskMapper = MaskMapper.STRIP;

	public MaskMapper getMaskMapper() {
		return maskMapper;
	}

	public void setMaskMapper(MaskMapper maskMapper) {
		System.out.format("setMaskMapper(%s)%n", maskMapper);
		this.maskMapper = maskMapper;
	}

	/**
	 * Synchronize hexMask to match stripBits
	 */
	private void syncHexMask() {
		final byte[] maskBytes = hexMask.getBytes().clone();
		final int conjugateMax = 96;

		for (int conjugate = 1; conjugate <= conjugateMax; conjugate++) {

			if (maskMapper.containsKey(conjugate)) {

//		for (Integer conjugate : maskMapper.keySet()) {
			final Bit bit = stripBits.get(conjugate);
			//System.out.println("Bit+ " +bit);
			if (!bit.isDummy()) {
				final Integer channel = conjugate;
				if (channel == null)
					throw new RuntimeException("No mapping for conjChannel "
							+ conjugate);
				final int indexBit = channel - 1;
				Binary.setBit(maskBytes, indexBit, bit.getValue() ? 1 : 0);
			}
		}
		}
		oldHexMask = hexMask;
		System.out.println("syncHexMask(): oldHexMask set to "
				+ oldHexMask.toString());
		hexMask = new Binary(maskBytes, oldHexMask.getBitNum());

		loggerWeb.write(String.format("<i>mask synced to</i> %s%n", hexMask
				.toString().toUpperCase()));
	}

	// TODO move to Utils class
	/**
	 * @param i argument to step
	 * @param width step width
	 * @return step height: 1 in [0, width], 2 in [width + 1, 2 * width] ...
	 */
	private static int step(int i, int width) {
		return i / width + ((i != 0 && i % width == 0) ? 0 : 1);
	}

	/**
	 * Synchronize stripBits to match hexMask
	 */
	private void syncStripBits() {
		stripBits.clear();
		stripBitsConnected.clear();

		// because of the presentation, strips are indexed starting from 1;
		stripBits.add(Bit.dummy());
		stripBitsConnected.add(Bit.dummy());

	//	final int conjugateMax = maskMapper.getNumberMax();
		final int conjugateMax = 96;
		//System.out.println("hexmask "+hexMask);
		//System.out.println("maximum number "+conjugateMax);
		int[] testconfiguration = new int[96];
		final boolean appendEtaPart = getNumberOfEtaPartitions() > 1;
		for (int conjugate = 1; conjugate <= conjugateMax; conjugate++) {
			Bit bit = null;
			Bit bitConnected = null;
	
			if (maskMapper.containsKey(conjugate)) {
				testconfiguration[conjugate-1]=1;
				final Integer stripno = maskMapper.get(conjugate);
		//		if (channel == null)
		//			throw new RuntimeException("Conjugate channel " + conjugate
		//					+ " has corresponding null channel");

				final int bitIndex = conjugate - 1; //DB mask numbering starts from 1, see Linkboard.getConnectedStripsMask() 

				String bitName = String.valueOf(stripno);
				String bitChannel = String.valueOf(conjugate);
				if (appendEtaPart)
					bitName = bitName + " " + conjugateToEtaPart.get(conjugate);
				final boolean bitValue = hexMask.getBits(bitIndex, 1) != 0;
				bit = new Bit(bitName, bitValue,Integer.toString(conjugate),Integer.toString(stripno),conjugateToEtaPart.get(conjugate));

				final boolean bitConnectedValue = hexMaskConnected.getBits(
						bitIndex, 1) != 0;
				bitConnected = new Bit(bitName, bitConnectedValue,Integer.toString(conjugate),Integer.toString(stripno),conjugateToEtaPart.get(conjugate));
			} else { // conjugate is not mapped
				testconfiguration[conjugate-1]=0;
				bit = Bit.dummy();
				bitConnected = Bit.dummy();
			}
			stripBits.add(bit);
			stripBitsConnected.add(bitConnected);
			
		//	System.out.println("ETA PART : "+ conjugateToEtaPart.get(conjugate));
		}
		boolean[] a = new boolean[12];
		for (int k =0; k <12;k++)
			a[k]=false;
			int i =0;
			for(int k = 0; k< 96; k+=8)
			{
			if (testconfiguration[k]==0 && testconfiguration[k+1]==0 &&testconfiguration[k+2]==0 &&testconfiguration[k+3]==0 &&testconfiguration[k+4]==0 &&testconfiguration[k+5]==0 &&testconfiguration[k+6]==0 &&testconfiguration[k+7]==0)
					a[i]=true;
			i++;		
			}		
		
			for (int j =0; j<febChipInfo.size(); j++){
				febChipInfo.get(j).setThresholdDisable(a[j]);
			}
		
	
			
	
	}

	public String[] getEtaPartitions() {
		return new HashSet<String>(conjugateToEtaPart.values()).toArray(new String[]{});
	}
	public int getNumberOfEtaPartitions() {
		return getEtaPartitions().length;
	}
	public void doPageStrips(ActionEvent ae) {
		
	//	System.out.println("Action Event :"+ae);
		final String btnId = ((CoreCommandButton) ae.getSource()).getId();

		
		int start =0, end=0;
	
		
		if (dataTable.isShowAll()){
			start = 1;
			end = stripBits.size();
		}
		else{
			start = dataTable.getFirst()*8+1;
			end = start + 15;
		
		}
		for (int conjugate = start; conjugate <= end; conjugate++) {
			if (maskMapper.containsKey(conjugate) && 
					stripBitsConnected.get(conjugate).getValue() // strip is connected
					) {
				final Bit bit = stripBits.get(conjugate);
				
				if(btnId=="btnEnablePage")
					bit.setValue(true);
				else if(btnId=="btnDisablePage")
					bit.setValue(false);
				 
			}
		}
	}

	public void setPageTitle(RangeChangeEvent ae) {
		
			getTitle();
			//System.out.println("Title :"+getTitle());
			
			}

	public void doPageChips(ActionEvent ae) {
		
		//	System.out.println("Action Event :"+ae);
			final String btnId = ((CoreCommandButton) ae.getSource()).getId();

			
			int start =0, end=0;
		
			
			if (dataTable.isShowAll()){
				start = 0;
				end = febChipInfo.size()-1;
			}
			else{
				start = dataTable.getFirst();
				end = start + 1;
			
			}
			for (int conjugate = start; conjugate <= end; conjugate++) {
				
				if (btnId.equalsIgnoreCase("chipEnable"))
				febChipInfo.get(conjugate).setChipEnable(true);
				else if (btnId.equalsIgnoreCase("chipDisable"))
					febChipInfo.get(conjugate).setChipEnable(false);
							
			}
		}

public void setDTControl( ValueChangeEvent ae) {
		
		
	boolean test = ( Boolean ) ((UIXValue)ae.getSource()).getValue();
		
			int start =0, end=0;
		
			//System.out.println("test; "+test);
			int index = dataTable.getRowIndex();
				if(test)
					{
					febChipInfo.get(index).setDtControl(true);
    				setDtDesc("Redundant DT Line in use");
    				setRpcDesc("Select Main RPC Line for Threshold Configuration");
					}
				else 
					{
					febChipInfo.get(index).setDtControl(false);
					setDtDesc("Select redundant DT Line for Threshold Configuration");
    				setRpcDesc("Main RPC Line is in use");
					
					}
			getDtDesc();
			getRpcDesc();
			}

	public void setVthMv(ValueChangeEvent ae) {
	
		int rowno = dataTable.getRowIndex();
		int a = (Integer)((UIXValue) ae.getSource()).getValue();

//		System.out.println("testing change value");
//		System.out.println(a);
		febChipInfo.get(rowno).getFcc().setVth(a);
//		System.out.println(febChipInfo.get(rowno).getFcc().getVth());
		
		double mv = a * 2.5 / 1.024;
		febChipInfo.get(rowno).getFcc().setVth_mV((int)Math.round(mv));
		
//		System.out.println(febChipInfo.get(rowno).getFcc().getVth_mV());
		FacesContext.getCurrentInstance().renderResponse();

		
	}


	public void setVthOffsetMv(ValueChangeEvent ae) {
	
		int rowno = dataTable.getRowIndex();
		int a = (Integer)((UIXValue) ae.getSource()).getValue();

//		System.out.println("testing change value");
//		System.out.println(a);
		febChipInfo.get(rowno).getFcc().setVthOffset(a);
//		System.out.println(febChipInfo.get(rowno).getFcc().getVth());
		
		double mv = a * 2.5 / 1.024;
		febChipInfo.get(rowno).getFcc().setVthOffset_mV((int)Math.round(mv));
		
//		System.out.println(febChipInfo.get(rowno).getFcc().getVth_mV());
		FacesContext.getCurrentInstance().renderResponse();

		
	}

	public void setVmonMv(ValueChangeEvent ae) {
	
		int rowno = dataTable.getRowIndex();
		int a = (Integer)((UIXValue) ae.getSource()).getValue();

//		System.out.println("testing change value");
//		System.out.println(a);
		febChipInfo.get(rowno).getFcc().setVmon(a);
//		System.out.println(febChipInfo.get(rowno).getFcc().getVth());
		
		double mv = a * 2.5 / 1.024;
		febChipInfo.get(rowno).getFcc().setVmon_mV((int)Math.round(mv));
		
//		System.out.println(febChipInfo.get(rowno).getFcc().getVth_mV());
		FacesContext.getCurrentInstance().renderResponse();

		
	}

	public void setVmonOffsetMv(ValueChangeEvent ae) {
	
		int rowno = dataTable.getRowIndex();
		int a = (Integer)((UIXValue) ae.getSource()).getValue();

//		System.out.println("testing change value");
//		System.out.println(a);
		febChipInfo.get(rowno).getFcc().setVmonOffset(a);
//		System.out.println(febChipInfo.get(rowno).getFcc().getVth());
		
		double mv = a * 2.5 / 1.024;
		febChipInfo.get(rowno).getFcc().setVmonOffset_mV((int)Math.round(mv));
		
//		System.out.println(febChipInfo.get(rowno).getFcc().getVth_mV());
		FacesContext.getCurrentInstance().renderResponse();

		
	}

	public void setFECGlobalSettingsVth(ActionEvent ae) {
		

		
		for (int i=0; i<febChipInfo.size();i++){
			if(febChipInfo.get(i).isThresholdDisable()){
				
			}
			else{
			febChipInfo.get(i).getFcc().setVth(vthglobal);
			
			}
		}
		
		
		
	}

	public void setFECGlobalSettingsVmon(ActionEvent ae) {
		

		
		for (int i=0; i<febChipInfo.size();i++){
			if(febChipInfo.get(i).isThresholdDisable()){
				
			}
			else{
			
			febChipInfo.get(i).getFcc().setVmon(vmonglobal);
			}
		}
		
		FacesContext.getCurrentInstance().renderResponse();

		
	}

	
	public void doAllOfEtaPartition(ActionEvent ae) {
		//System.out.println("Action Event :"+ae);
		final String btnId = ((CoreCommandButton) ae.getSource()).getId();
		//System.out.println("Button ID :"+btnId);

		// Hack the Id to get the button index
		final String btnIdIndex = btnId.substring(btnId.length() - 1);
		System.out.println("Button Index :"+btnIdIndex);

		int etaPartIndex = 0;
		
		try {
			etaPartIndex = Integer.parseInt(btnIdIndex);
		} catch (NumberFormatException e) {
			//TODO: Warn?
		} 
		final String etaPartition = getEtaPartitions()[etaPartIndex];

		System.out.println("Eta Partition :"+etaPartition);

		
		if (btnId.startsWith("btnEnableAll"))
			new EtaPartLooperEnabler().loop(etaPartition);
		else if (btnId.startsWith("btnDisableAll"))
			new EtaPartLooperDisabler().loop(etaPartition);
		else 
			throw new IllegalArgumentException("doAllOfEtaPartition(): Can't handle button id " + btnId);
	}
	/**
	 * Loop over eta partition and set stripBits to bitValueToSet(conjugate)
	 * Template Method
	 */
	private abstract class EtaPartLooper {
		public void loop(String etaPartition) {
			for (int conjugate = 1; conjugate <= stripBits.size(); conjugate++) {
				if (maskMapper.containsKey(conjugate) && 
						conjugateToEtaPart.get(conjugate).equals(etaPartition) && // is in the right etaPartition
						stripBitsConnected.get(conjugate).getValue() // strip is connected
						) {
					final Bit bit = stripBits.get(conjugate);
					bit.setValue(bitValueToSet(conjugate));
				}
			}
		}
		abstract boolean bitValueToSet(int conjugate);
	}
	private final class EtaPartLooperEnabler extends EtaPartLooper{
		@Override
		boolean bitValueToSet(int conjugate) {
			return true;
		}
	}
	private final class EtaPartLooperDisabler extends EtaPartLooper{
		@Override
		boolean bitValueToSet(int conjugate) {
			return false;
		}
	}
	
	public void maskMapperChanged(ValueChangeEvent event)
			throws DataAccessException, IllegalArgumentException {
		final MaskMapper newMaskMapper = (MaskMapper) ((UIXValue) event
				.getSource()).getValue();

		System.out.println("maskMapperChanged(): newMaskMapper = "
				+ newMaskMapper);
		maskMapper = newMaskMapper;
		System.out.println("Changed MaskMapper: "+maskMapper);
		loadMaskBytes();
	}
	
	/**
	 * @return String of hexadecimal digit characters (two per byte). The leading
	 * zeroes (if any) are presented.
	 */
	@Override
	public String toString() {
		final String s = hexMask.toString();

		return s;
	}

	private boolean numbersOn = false;

	// etaPartition of a conjugate
	private Map<Integer, String> conjugateToEtaPart;
	
	public void setNumbersOn(boolean numbersOn) {
		this.numbersOn = numbersOn;
	}

	public boolean isNumbersOn() {
		return numbersOn;
	}

	public void setLogtxt(String logtxt) {
		loggerWeb.write(logtxt);
	}

	public String getLogtxt() {
		return loggerWeb.toString();
	}

	public List<DBDriver.ParamModified> getMaskModifications() throws DataAccessException {
		
		//dbpm.clear();
		
		DBDriver dbDriver = new DBDriver();
		// TODO: add configure(context, ...) to use already set configuration settings 
		dbDriver.configure();
		
		final int synCoderId = dbState.lb.getSynCoder().getId();
		//System.out.println("Syncid"+synCoderId);
		dbpm = dbDriver.getMaskHistory(synCoderId);;
		return dbpm;
		}
	
	
	public List<DBDriver.ParamModified> getFECHistory() throws DataAccessException {
		// TODO: merge DbState to DBDriver
		DBDriver dbDriver = new DBDriver();
		// TODO: add configure(context, ...) to use already set configuration settings 
		dbDriver.configure();
		
		int size = 0;
		
		
		int[] chipid = new int[12];     
		int i =0;
		for(Chip febChip :dbState.lb.getFebChips()) {
			
		//	System.out.println(febChip);

			if(febChip==null)
				chipid[i]=0;
			else{
				chipid[i]=febChip.getId();
		//	System.out.println("Chip Id "+ chipid[i]+" : "+i);
		//	System.out.println("Chip i: "+i+":"+febChip.getId());
			}
			i++;
			
		}	
		
			return dbDriver.getFECHistory(chipid);
	}
	
	public List<ChipsDisabledDetail> getChipDisabledHistory() throws DataAccessException {
		
		List<ChipsDisabledDetail> cdd = new ArrayList<ChipsDisabledDetail>();

		for(Chip chip :dbState.lb.getFebChips()) {
			if(chip==null){
				
			}
			else{
				List<ChipDisabledHistory> chipDisabledHistories = dbState.configurationDAO.getChipDisablingHistory(chip);
				for (Iterator iter = chipDisabledHistories.iterator(); iter.hasNext();) {
					ChipDisabledHistory element = (ChipDisabledHistory) iter.next();
					ChipsDisabledDetail cd = new ChipsDisabledDetail();
					String febname = chip.getBoard().getName();
					if (febname.contains("FEB_"))
						febname = febname.replace("FEB_", "");
					cd.setFebname(febname);
					cd.setModificationDate(element.getModificationDate());
					cd.setStatus(element.getStatus());
					cd.setChippos(chip.getPosition());
					cd.setLep(Integer.toString(((FebBoard)chip.getBoard()).getFebLocation().getPosInLocalEtaPartition()));
					cdd.add(cd);
				}

			}
			
		}
		Comp comp = new Comp();
		Collections.sort(cdd,comp);

	
		
		return cdd;
	}
	
	public List<DTLinesDetail> getDTLinesHistory() throws DataAccessException {
		
		List<DTLinesDetail> dtl = new ArrayList<DTLinesDetail>();

		int id = 0;
		for(Chip chip :dbState.lb.getFebChips()) {
			if(chip==null){
				
			}
			else{
				int idnew = chip.getBoard().getId();
				if (id==idnew){}
				else{
		            List<FebDTControlledHistory> dtControlledHistories = dbState.configurationDAO.getFebDTControlledHistory(chip.getBoard());
	
					
					for (Iterator iter = dtControlledHistories.iterator(); iter.hasNext();) {
						FebDTControlledHistory element = (FebDTControlledHistory) iter.next();
						DTLinesDetail dd = new DTLinesDetail();
						String febname = chip.getBoard().getName();
						if (febname.contains("FEB_"))
							febname = febname.replace("FEB_", "");
						dd.setFebname(febname);
						dd.setModificationDate(element.getModificationDate());
						dd.setStatus(element.getStatus());
						dd.setLep(Integer.toString(((FebBoard)chip.getBoard()).getFebLocation().getPosInLocalEtaPartition()));
						dtl.add(dd);
						id = chip.getBoard().getId();
					}
				}

			}
			
		}
		
		Comp1 comp = new Comp1();
		Collections.sort(dtl,comp);
		return dtl;
	}	

	public void setHistoryMask(ActionEvent ae) throws DataAccessException {
		
			//System.out.println("Action Event :"+ae);
			//System.out.println(hexMask);
			final String btnId = ((CoreCommandButton) ae.getSource()).getId();
			
			int rowno = maskHistoryTable.getRowIndex();
			Binary hM =  new Binary(dbpm.get(rowno).getParamVal().toString());
			//System.out.println(hM);
			
			for (int conjugate = 1; conjugate <= 96; conjugate++) {
				if (maskMapper.containsKey(conjugate) && 
						stripBitsConnected.get(conjugate).getValue() // strip is connected
						) {
					final Bit bit = stripBits.get(conjugate);
					int bitIndex = conjugate - 1; //DB mask numbering starts from 1, see Linkboard.getConnectedStripsMask() 
					final boolean bitValue = hM.getBits(bitIndex, 1) != 0;
					bit.setValue(bitValue);
					//System.out.println(bitValue);		
					 
				}
			}
			
			 RequestContext.getCurrentInstance().addPartialTarget(dataTable); 
             RequestContext.getCurrentInstance().partialUpdateNotify(dataTable); 
             FacesContext.getCurrentInstance().renderResponse(); 


	}

	
public String onkeypressevent(){
	System.out.println("on key press event called;");
	
	return "test";
}	
	
}


class Comp implements Comparator {
    public int compare (Object n1, Object n2) {
        return -((ChipsDisabledDetail)n1).getModificationDate().compareTo(((ChipsDisabledDetail)n2).getModificationDate());
    }
}			

class Comp1 implements Comparator {
    public int compare (Object n1, Object n2) {
        return -((DTLinesDetail)n1).getModificationDate().compareTo(((DTLinesDetail)n2).getModificationDate());
    }
}		
