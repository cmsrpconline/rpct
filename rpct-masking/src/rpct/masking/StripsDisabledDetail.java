package rpct.masking;

import rpct.db.domain.equipment.*;

/**
 * @author dnikolay
 * @version $Id: ChamberWithMasks.java 496 2010-02-10 21:14:11Z dnikolay $
 * 
 * Decorate ChamberLocation with Masks
 */
public class StripsDisabledDetail {
	
	private  String febname;
	private  String lep;
	private  int posinlep;
	private  int boardid;
	private  boolean disable;
	private int stripnumber;
	private int lbchannelnumber;
	private int chipposition;
	
	public StripsDisabledDetail() {
		super();
	}
	public StripsDisabledDetail(String febname, String lep, int posinlep,
			int boardid, boolean disable, int stripnumber, int lbchannelnumber,
			int chipposition) {
		super();
		this.febname = febname;
		this.lep = lep;
		this.posinlep = posinlep;
		this.boardid = boardid;
		this.disable = disable;
		this.stripnumber = stripnumber;
		this.lbchannelnumber = lbchannelnumber;
		this.chipposition = chipposition;
	}
	public String getFebname() {
		return febname;
	}
	public void setFebname(String febname) {
		this.febname = febname;
	}
	public String getLep() {
		return lep;
	}
	public void setLep(String lep) {
		this.lep = lep;
	}
	public int getPosinlep() {
		return posinlep;
	}
	public void setPosinlep(int posinlep) {
		this.posinlep = posinlep;
	}
	public int getBoardid() {
		return boardid;
	}
	public void setBoardid(int boardid) {
		this.boardid = boardid;
	}
	public boolean isDisable() {
		return disable;
	}
	public void setDisable(boolean disable) {
		this.disable = disable;
	}
	public int getStripnumber() {
		return stripnumber;
	}
	public void setStripnumber(int stripnumber) {
		this.stripnumber = stripnumber;
	}
	public int getLbchannelnumber() {
		return lbchannelnumber;
	}
	public void setLbchannelnumber(int lbchannelnumber) {
		this.lbchannelnumber = lbchannelnumber;
	}
	public int getChipposition() {
		return chipposition;
	}
	public void setChipposition(int chipposition) {
		this.chipposition = chipposition;
	}
	


}


