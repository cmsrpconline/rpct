package rpct.masking;

import java.util.*;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.configuration.SynCoderConf;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.chamberloaction.BarrelOrEndcap;
import rpct.db.domain.equipment.feblocation.FebLocation;

/**
 * @author dnikolay
 * @version $Id: ChambersReportBean.java 359 2010-01-07 21:29:12Z dnikolay $
 * 
 */
public class ChambersReportBean {
	private final DBDriver dbd;
	private List<LinkBoardWithConfiguration> ALL_LINKBOARDS;
	private List<ChamberWithMasks> ALL_CHAMBERS;
	
	private final String startDate;
	
	public ChambersReportBean() throws DataAccessException {
		startDate = getDate();
		
		dbd = new DBDriver();
		
		try {
			dbd.configure();
			fetchData();
			dbd.free();
			// TODO: Add report initialization here and uncomment the finally block
		} catch (DataAccessException e) {
			e.printStackTrace();
			dbd.getContext().rollback();
		}
//		finally {
//			dbd.getContext().closeSession();
//		}
	}


	private void fetchData() throws DataAccessException {
		MaskingUtils.log("ChambersReportBean.fetchData() starts");

		ALL_LINKBOARDS = allLB();

		ALL_CHAMBERS = allCh();

		MaskingUtils.log("ChambersReportBean.fetchData() ends");
	}


	public List<LinkBoardWithConfiguration> getAllLBoards() {
		MaskingUtils.log("ChambersReportBean.getAllLBoards() called");

		return ALL_LINKBOARDS; 
	}

	public List<ChamberWithMasks> getAllChambers() {
		MaskingUtils.log("ChambersReportBean.getAllChambers() called");

		return ALL_CHAMBERS; 
	}
	
	public String getDate() {
		return MaskingUtils.dateNow() + " " + MaskingUtils.timeNow();
	}

	public String getStartDate() {
		return startDate;
	}

	public static void main (String arg[]){
		System.out.println("testing");
		try{
		ChambersReportBean cha = new ChambersReportBean();
		System.out.println(cha);
		List<ChamberWithMasks> test1 = cha.getAllChambers();
		List<LinkBoardWithConfiguration> test2 = cha.getAllLBoards();
		System.out.println(test1.size());
		System.out.println(test2.size());
		java.util.Iterator itr = test1.iterator(); 
		while(itr.hasNext()) {
		    ChamberWithMasks element = (ChamberWithMasks) itr.next(); 
		    System.out.println(element.getEnabledStripsMask());
		    System.out.println(element.getLBoardName());
		    System.out.println(element.getName());
		    System.out.println(element.getId());
		    System.out.println(element.hashCode());
		    
		} 
		itr = test2.iterator(); 
		while(itr.hasNext()) {
		    LinkBoardWithConfiguration element = (LinkBoardWithConfiguration) itr.next(); 
		    System.out.println(element.getConnectedChannelsMask());
		    System.out.println(element.getEnabledChannelsMask());
		    System.out.println(element.getRawLinkBoard());
		    System.out.println(element.getId());
		    System.out.println(element.getName());
			
		} 
		
		}catch(Exception e ){
			System.out.println();
		}
	}
	public List<LinkBoardWithConfiguration> allLB() throws DataAccessException {
		MaskingUtils.log("DBDriver.getAllLinkBoards() starts");

		if (ALL_LINKBOARDS != null)
			return ALL_LINKBOARDS;

		final List<LinkBoardWithConfiguration> result = new ArrayList<LinkBoardWithConfiguration>();
		List<? extends Crate> crates = null;
		
		final boolean useShortList = false;
	
		if (useShortList) {
			// report only W-2/RB1in/1Forward
			final Integer diskOrWheel = -2;
			final Integer layer = 1;
			final Integer sector = 1;
			final String subsector = "";
			final BarrelOrEndcap barrelOrEndcap = BarrelOrEndcap.Barrel;
			
			crates = dbd.equipmentDAO.getLinkBoxesByChamberLocation(diskOrWheel, layer, sector,
					subsector, barrelOrEndcap);
		}
		else {
			crates = dbd.equipmentDAO.getCratesByType(CrateType.LINKBOX, false);
		}
		for(Crate crate : crates) {
			for(Board board : crate.getBoards()) {
        		if(board.getType() == BoardType.LINKBOARD) {
        			final LinkBoard lb = (LinkBoard) board;

        			StaticConfiguration configuration = dbd.configurationManager
					.getConfiguration(lb.getSynCoder(), dbd.inputConfigKey);

        			if (configuration != null) {
						result.add(new LinkBoardWithConfiguration(lb, (SynCoderConf) configuration));
					}
					else {
						System.out.println("LB " + lb.getName() + " has null configuration!");
					}
        		}
        	}
        }

		// sort by name
		Collections.sort(result);

		ALL_LINKBOARDS = result;
		
		MaskingUtils.log("DBDriver.getAllLinkBoards() ends");
		
		return result;

	}
	
		public List<ChamberWithMasks> allCh() throws DataAccessException {
		MaskingUtils.log("DBDriver.getAllChambers() starts");
		
		if (ALL_CHAMBERS != null)
			return ALL_CHAMBERS;

		final List<ChamberWithMasks> result =	new ArrayList<ChamberWithMasks>();

		final List<LinkBoardWithConfiguration> allLBoards = getAllLBoards();
		
		for (LinkBoardWithConfiguration lb: allLBoards) {
			final LinkBoard lbRaw = lb.getRawLinkBoard();
			
			final Set<ChamberWithMasks> lbChambers = new HashSet<ChamberWithMasks>();
			for (FebLocation fl: lbRaw.getFEBLocations()) {
				final String chamberName = DBDriver.chamberNameOf(fl);
				final String chamberID = fl.getChamberLocation().getId() +
				// Endcap chamber are merged over all etaPartition into one (see also  chamberNameOf())
				(fl.getChamberLocation().getBarrelOrEndcap() == BarrelOrEndcap.Barrel ? fl.getFebLocalEtaPartition() : "");

				final String chamberEnabledStripsMask = DBDriver.getMaskEnabledStrips(lb, chamberName);  
				
				lbChambers.add(new ChamberWithMasks(chamberID, chamberName, lb.getName(), chamberEnabledStripsMask));
			}
			
			result.addAll(lbChambers);
		}
		
		// sort by name
		Collections.sort(result);
		
		ALL_CHAMBERS = result;

		MaskingUtils.log("DBDriver.getAllChambers() ends");

		return result;
	}

}


