package rpct.masking;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.math.BigDecimal;
import rpct.db.domain.equipment.ChipType;
import org.apache.myfaces.trinidad.component.UIXTable;
import org.hibernate.HibernateException;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.configuration.SynCoderConf;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.FebConnector;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.chamberloaction.BarrelOrEndcap;
import rpct.db.domain.equipment.chamberstrip.ChamberStrip;
import rpct.db.domain.equipment.feblocation.FebLocation;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.xdaq.axis.Binary;
import rpct.db.domain.configuration.ChipConfAssignment;
/**
 * @author Muhammad Imran
 * 
 * $Id: SummaryReports.java 495 2011-04-05  $
 */
public class SummaryReports {
	private List<ChipsDisabledDetail> chipsDisabled = null;
	private List<DTLinesDetail> dtLinesDetail = null;
	private List<StripsDisabledDetail> sddtest = null;
	private  String date=null;

	
	private UIXTable dataTable;
	DBDriver dbd;
	
	
	public String getDate() {
		date= MaskingUtils.dateNow() + " " + MaskingUtils.timeNow();
		
		return date;
	}

	public UIXTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(UIXTable dataTable) {
		this.dataTable = dataTable;
	}

	public long getNoOfDisableStrips() throws DataAccessException{
		DBDriver dbDriver = new DBDriver();
		dbDriver.configure();
		return dbDriver.getNoOfDisableStrips();
		
	}
	
	public long getTotalNoOfStrips() throws DataAccessException {
		// TODO: merge DbState to DBDriver
		DBDriver dbDriver = new DBDriver();
		// TODO: add configure(context, ...) to use already set configuration settings 
		dbDriver.configure();
		
		
		return dbDriver.getTotalNoOfStrips();
	}
	
	public double getPercentageOfStrips() throws DataAccessException {
		// TODO: merge DbState to DBDriver
		DBDriver dbDriver = new DBDriver();
		// TODO: add configure(context, ...) to use already set configuration settings 
		dbDriver.configure();
		
		DecimalFormat df = new DecimalFormat("##.00");
		BigDecimal test = new BigDecimal(df.format(dbDriver.getPercentageOfStrips()));
		return (test.doubleValue());
	}
		
	
	public BigDecimal getNoOfDisableChips() throws DataAccessException {
		// TODO: merge DbState to DBDriver
		DBDriver dbDriver = new DBDriver();
		// TODO: add configure(context, ...) to use already set configuration settings 
		dbDriver.configure();
		
		
		return dbDriver.getNoOfDisableChips();
	}
	
	public BigDecimal getTotalNoOfChips() throws DataAccessException {
		// TODO: merge DbState to DBDriver
		DBDriver dbDriver = new DBDriver();
		// TODO: add configure(context, ...) to use already set configuration settings 
		dbDriver.configure();
		
		
		return dbDriver.getTotalNoOfChips();
	}
	
	public BigDecimal getTotalFEBBoard() throws DataAccessException {
		// TODO: merge DbState to DBDriver
		DBDriver dbDriver = new DBDriver();
		// TODO: add configure(context, ...) to use already set configuration settings 
		dbDriver.configure();
		
		
		return dbDriver.getTotalFEBBoard();
	}
	public double getPercentageOfDTBoard() throws DataAccessException {
		// TODO: merge DbState to DBDriver
		DBDriver dbDriver = new DBDriver();
		// TODO: add configure(context, ...) to use already set configuration settings 
		dbDriver.configure();
		
		BigDecimal a =dbDriver.getTotalFEBBoard();
		BigDecimal b =dbDriver.getNoOfFEBDTControlled();
		
		double d = a.doubleValue();
		double e = b.doubleValue();
		
		DecimalFormat df = new DecimalFormat("##.00");
		BigDecimal test = new BigDecimal((e/d));
		return (Double.parseDouble(df.format(test.doubleValue()*100)));
		
		}
	
	public double getPercentageOfChips() throws DataAccessException {
		// TODO: merge DbState to DBDriver
		DBDriver dbDriver = new DBDriver();
		// TODO: add configure(context, ...) to use already set configuration settings 
		dbDriver.configure();
		
		BigDecimal a =dbDriver.getTotalNoOfChips();
		BigDecimal b =dbDriver.getNoOfDisableChips();
		
		double d = a.doubleValue();
		double e = b.doubleValue();
		
		DecimalFormat df = new DecimalFormat("##.00");
		BigDecimal test = new BigDecimal((e/d));
		return (Double.parseDouble(df.format(test.doubleValue()*100)));
		
		
		
		}
	
	public BigDecimal getNoOfFEBDTControlled() throws DataAccessException {
		// TODO: merge DbState to DBDriver
		DBDriver dbDriver = new DBDriver();
		// TODO: add configure(context, ...) to use already set configuration settings 
		dbDriver.configure();
		
		
		return dbDriver.getNoOfFEBDTControlled();
	}
	

	public List<ChipsDisabledDetail> getChipsDisabled() throws DataAccessException {
	
		// TODO: merge DbState to DBDriver
		DBDriver dbDriver = new DBDriver();
		// TODO: add configure(context, ...) to use already set configuration settings 
		dbDriver.configure();
		chipsDisabled = dbDriver.getChipsDisabled(); 
		return chipsDisabled;
	}


	public List<DTLinesDetail> getDtLinesDetail() throws DataAccessException {

		// TODO: merge DbState to DBDriver
		DBDriver dbDriver = new DBDriver();
		// TODO: add configure(context, ...) to use already set configuration settings 
		dbDriver.configure();
		dtLinesDetail = dbDriver.getDtLinesDetail(); 
	
		return dtLinesDetail;
	}


}
