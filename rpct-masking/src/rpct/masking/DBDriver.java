package rpct.masking;
import rpct.masking.*;
import rpct.db.domain.configuration.ChipConfAssignment;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import javax.management.Query;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.configuration.SynCoderConf;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.FebConnector;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.chamberloaction.BarrelOrEndcap;
import rpct.db.domain.equipment.chamberstrip.ChamberStrip;
import rpct.db.domain.equipment.feblocation.FebLocation;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;

import rpct.xdaq.axis.Binary;
import rpct.db.domain.equipment.ChipType;

/**
 * @author Nikolay Darmenov
 * @version $Id: DBDriver.java 495 2010-02-10 21:13:07Z dnikolay $
 * 
 * Wraps key DB access
 */
public class DBDriver {
	private static Logger logger = Logger.getLogger(DBDriver.class);
	
//	private static long totalstrips=0,stripsdisable=0;

	public HibernateContext context = null;
	public EquipmentDAO equipmentDAO = null;
	public ConfigurationManager configurationManager = null;
	public ConfigurationDAO configurationDAO = null;
	public LocalConfigKey inputConfigKey = null;

	// ALL_LINKBOARDS with getAllLinkBoards() is a singleton
	private List<LinkBoardWithConfiguration> ALL_LINKBOARDS = null;
	
	// ALL_CHAMBERS with getAllChambers() is a singleton
	private List<ChamberWithMasks> ALL_CHAMBERS = null;
	
	private List<ChipsDisabledDetail> chipsDisabled = null;
	private List<DTLinesDetail> dtLinesDetail = null;
	private List<StripsDisabledDetail> sdd = null;
	private List<StripsDisabledDetail> sddtest = null;
	public HibernateContext getContext() {
		return context;
	}
	
	
	public void configure() throws DataAccessException {
		// Following rpct-db/src/main/java/rpct/testAndUtils/PutStripMasksToDB.java
		if (context == null) {
			context = new SimpleHibernateContextImpl();
			equipmentDAO = new EquipmentDAOHibernate(context);
			configurationDAO = new ConfigurationDAOHibernate(
					context);
			configurationManager = new ConfigurationManager(
					context, equipmentDAO,
					configurationDAO);

			//the source of the configuration data
			inputConfigKey = configurationManager.getCurrnetLBsConfigKey();
		}
	}
	
	public void free() {
		ALL_CHAMBERS = null;
		ALL_LINKBOARDS = null;
	}
	
	public List<LinkBoardWithConfiguration> getAllLinkBoards() throws DataAccessException {
		MaskingUtils.log("DBDriver.getAllLinkBoards() starts");

		if (ALL_LINKBOARDS != null)
			return ALL_LINKBOARDS;

		final List<LinkBoardWithConfiguration> result = new ArrayList<LinkBoardWithConfiguration>();
		List<? extends Crate> crates = null;
		
		final boolean useShortList = true;
	
		if (useShortList) {
			// report only W-2/RB1in/1Forward
			final Integer diskOrWheel = -2;
			final Integer layer = 1;
			final Integer sector = 1;
			final String subsector = "";
			final BarrelOrEndcap barrelOrEndcap = BarrelOrEndcap.Barrel;
			
			crates = equipmentDAO.getLinkBoxesByChamberLocation(diskOrWheel, layer, sector,
					subsector, barrelOrEndcap);
		}
		else {
			crates = equipmentDAO.getCratesByType(CrateType.LINKBOX, false);
		}
		for(Crate crate : crates) {
			for(Board board : crate.getBoards()) {
        		if(board.getType() == BoardType.LINKBOARD) {
        			final LinkBoard lb = (LinkBoard) board;

        			StaticConfiguration configuration = configurationManager
					.getConfiguration(lb.getSynCoder(), inputConfigKey);

        			if (configuration != null) {
						result.add(new LinkBoardWithConfiguration(lb, (SynCoderConf) configuration));
					}
					else {
						System.out.println("LB " + lb.getName() + " has null configuration!");
					}
        		}
        	}
        }

		// sort by name
		Collections.sort(result);

		ALL_LINKBOARDS = result;
		
		MaskingUtils.log("DBDriver.getAllLinkBoards() ends");
		
		return result;

	}
	
		public List<ChamberWithMasks> getAllChambers() throws DataAccessException {
		MaskingUtils.log("DBDriver.getAllChambers() starts");
		
		if (ALL_CHAMBERS != null)
			return ALL_CHAMBERS;

		final List<ChamberWithMasks> result =	new ArrayList<ChamberWithMasks>();

		final List<LinkBoardWithConfiguration> allLBoards = getAllLinkBoards();
		
		for (LinkBoardWithConfiguration lb: allLBoards) {
			final LinkBoard lbRaw = lb.getRawLinkBoard();
			
			final Set<ChamberWithMasks> lbChambers = new HashSet<ChamberWithMasks>();
			for (FebLocation fl: lbRaw.getFEBLocations()) {
				final String chamberName = DBDriver.chamberNameOf(fl);
				final String chamberID = fl.getChamberLocation().getId() +
				// Endcap chamber are merged over all etaPartition into one (see also  chamberNameOf())
				(fl.getChamberLocation().getBarrelOrEndcap() == BarrelOrEndcap.Barrel ? fl.getFebLocalEtaPartition() : "");

				final String chamberEnabledStripsMask = DBDriver.getMaskEnabledStrips(lb, chamberName);  
				
				lbChambers.add(new ChamberWithMasks(chamberID, chamberName, lb.getName(), chamberEnabledStripsMask));
			}
			
			result.addAll(lbChambers);
		}
		
		// sort by name
		Collections.sort(result);
		
		ALL_CHAMBERS = result;

		MaskingUtils.log("DBDriver.getAllChambers() ends");

		return result;
	}

	public static String getMaskEnabledStrips(LinkBoardWithConfiguration lb, String chamberName) {
		final SortedMap<Integer, Integer> stripToChannel =  getStripToChannelMap(lb, chamberName);
		final Binary enabledChannelsLB = new Binary(lb.getEnabledChannelsMask());

		final int stripBitNumberMax = stripToChannel.lastKey() - 1;
		final byte[] enabledStripsBytes = new byte[stripBitNumberMax / Byte.SIZE + 1];
		
		for (int stripNumber: stripToChannel.keySet()) {
			final int channelNumber = stripToChannel.get(stripNumber);
			final int stripBitNumber = stripNumber - 1;
			final int channelBitNumber = channelNumber - 1;
			final int enabledChannelBitValue = (enabledChannelsLB.getBits(channelBitNumber, 1) == 0 ? 0 : 1);

			Binary.setBit(enabledStripsBytes, stripBitNumber, enabledChannelBitValue);
		}
		
		return new Binary(enabledStripsBytes).toString().toUpperCase();
	}

	private static SortedMap<Integer, Integer> getStripToChannelMap(LinkBoardWithConfiguration lb, String chamberName) {
		String canonChamberName = DBDriver.canonicalChamberName(chamberName);

		// The mapping strip => channel
		final TreeMap<Integer, Integer> stripToChannel = new TreeMap<Integer, Integer>();
		// Map of etaPart to stripToChannel
		final Map<String, SortedMap<Integer, Integer>> etaPartStripToChanel = new TreeMap<String, SortedMap<Integer,Integer>>();


		for (FebLocation fl : lb.getRawLinkBoard().getFEBLocations()) {
			final String etaPartitionFL = fl.getFebLocalEtaPartition();
		
			//			System.out.format("Got fl: (%d, %s, %s)%n", fl.getId(), fl
			//					.getChamberLocation().getChamberLocationName(), etaPartitionFL);
			// Endcap doesn't have etaPart attached - all (A,B,C) are merged in one big chamber 
			final String canonChamberNameFL = DBDriver.canonicalChamberName(chamberNameOf(fl));
			
			if (! canonChamberNameFL.equals(canonChamberName))
				continue;
			
			//			System.out.format("This fl %d matches target chambername %s%n", fl.getId(), chamberName);
			
			if (! etaPartStripToChanel.containsKey(etaPartitionFL))
				etaPartStripToChanel.put(etaPartitionFL, new TreeMap<Integer, Integer>());
			final Map<Integer, Integer> thisEtaPartStripToChanel = etaPartStripToChanel.get(etaPartitionFL);
			
			for (FebConnector fc : fl.getFebConnectors()) {
				//				System.out.format("fc: (id = %d, lbInputNum = %d, cs_num = %d)%n",
				//						fc.getId(), fc.getLinkBoardInputNum(),
				//						fc.getChamberStrips().size());

				for (ChamberStrip cs : fc.getChamberStrips()) {
					final Integer channelNumber = DBDriver.getChannelNumber(cs);
					final Integer stripNumber = DBDriver.getStripNumber(cs);
					
					thisEtaPartStripToChanel.put(stripNumber, channelNumber);
				}
			}
		}
		
		// Merge etaPartitions of (the sorted)etaPartStripToChanel
		int stripOffset = 0;
		for (SortedMap<Integer, Integer> m: etaPartStripToChanel.values()) {
			//			System.out.println("Merging " + m.toString());
			for (Entry<Integer, Integer> e: m.entrySet()) {
				final Integer stripNumber = e.getKey() + stripOffset;
				final Integer channelNumber = e.getValue();
				
				stripToChannel.put(stripNumber, channelNumber);
			}
			stripOffset += m.lastKey();
		}
		
		return stripToChannel;
	}

	public static String canonicalChamberName(String chamberName) {
		return chamberName.replaceAll("[ ]+", "");
	}
	
	// In compliance with LinkBoard.getChamberNames() format
	public static String chamberNameOf(FebLocation fl) {
		String result =	fl.getChamberLocation().getChamberLocationName();
		
		if(fl.getChamberLocation().getBarrelOrEndcap() == BarrelOrEndcap.Barrel) {
			result += " " + fl.getFebLocalEtaPartition();
		}
		
		return result;
	}
	
	public static int getChannelNumber(ChamberStrip cs) {
		final int lbInputNum = cs.getFebConnector().getLinkBoardInputNum() - 1;
		final int cableChannelNum = cs.getCableChannelNum();
		final int channelsPerFEB = 16;
		final int channelNumber = lbInputNum * channelsPerFEB
		+ cableChannelNum;

		return channelNumber;
	}
	
	public static int getStripNumber(ChamberStrip cs) {
		return cs.getChamberStripNumber();
	}
	
	public static int getCMSStripNumber(ChamberStrip cs) {
		return cs.getCmsStripNumber();
	}
	
	public static class ParamModified {
		private  Object paramVal;
		private  Timestamp modified;
		private  String lckName;
		
		private  Timestamp datemodified;
		private  String febname;
		private  int chiposition;
		private  int vth;
		private  int vthoffset;
		private  int vmon;
		private  int vmonoffset;
		private  String keyName;

		
		// TODO: add chipId to avoid ParamModified anonymousity
		
		public ParamModified(Object paramVal, Timestamp modified, String lckName) {
			this.paramVal = paramVal;
			this.modified = modified;
			this.lckName = lckName;
		}

		public ParamModified(Timestamp datemodified, String febname,
				int chiposition, int vth, int vthoffset, int vmon,
				int vmonoffset, String keyName) {
			this.datemodified = datemodified;
			this.febname = febname;
			this.chiposition = chiposition;
			this.vth = vth;
			this.vthoffset = vthoffset;
			this.vmon = vmon;
			this.vmonoffset = vmonoffset;
			this.keyName = keyName;
		}

		public Timestamp getDatemodified() {
			return datemodified;
		}

		public void setDatemodified(Timestamp datemodified) {
			this.datemodified = datemodified;
		}

		public String getFebname() {
			return febname;
		}

		public void setFebname(String febname) {
			this.febname = febname;
		}

		public int getChiposition() {
			return chiposition;
		}

		public void setChiposition(int chiposition) {
			this.chiposition = chiposition;
		}

		public int getVth() {
			return vth;
		}

		public void setVth(int vth) {
			this.vth = vth;
		}

		public int getVthoffset() {
			return vthoffset;
		}

		public void setVthoffset(int vthoffset) {
			this.vthoffset = vthoffset;
		}

		public int getVmon() {
			return vmon;
		}

		public void setVmon(int vmon) {
			this.vmon = vmon;
		}

		public int getVmonoffset() {
			return vmonoffset;
		}

		public void setVmonoffset(int vmonoffset) {
			this.vmonoffset = vmonoffset;
		}

		public String getKeyName() {
			return keyName;
		}

		public void setKeyName(String keyName) {
			this.keyName = keyName;
		}

		public static SimpleDateFormat getDATE_FORMAT() {
			return DATE_FORMAT;
		}

		public static void setDATE_FORMAT(SimpleDateFormat dATE_FORMAT) {
			DATE_FORMAT = dATE_FORMAT;
		}

		public Object getParamVal() {
			return paramVal;
		}
		public Timestamp getModified() {
			return modified;
		}
		public String getLckName() {
			return lckName;
		}

		private static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		/**
		 * @return DATE_FORMAT formatted timestamp - fractional seconds are truncated!
		 */
		public String getFormattedModified() {
			return DATE_FORMAT.format(modified);
		}
		public String getFormattedModifiedFEC() {
			return DATE_FORMAT.format(datemodified);
		}
		
		@Override
		public String toString() {
			return ParamModified.class + " (" + modified + ", " + paramVal + ", " + lckName + ")";
		}
	}

	public List<ParamModified> getMaskHistory(int chipId) throws HibernateException, DataAccessException {
		// TODO: remove the {lag,lead}_modified from the query string
		// TODO: switch to entity query, see http://docs.jboss.org/hibernate/core/3.3/reference/en/html/querysql.html#d0e13696
		LocalConfigKey inputConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.CURRENT_LBS_CONF_KEY);
		
		final String query_string = "" + 
		"select chipid, paramval, modified, lckname" +
		"  from (" +
		"    select chipid, lag(paramval) over (partition by chipid order by modified desc) lag_paramval," +
		"           lead(paramval) over (partition by chipid order by modified desc) lead_paramval," +
		"           paramval, modified, lckname," + 
		"           lag(modified) over (partition by chipid order by modified desc) lag_modified," +
		"           lead(modified) over (partition by chipid order by modified desc) lead_modified" +
		"      from (" +
		"        select cca.chip_chipid chipid, cc.inchannelsena paramval, cca.creationdate modified, lck.name lckname" +
		"          from linkchipconf cc" +
		"          join chipconfassignment cca" +
		"               on cca.sc_staticconfigurationid = cc.linkchipstaticconfid" +
		"          join chip" +
		"               on chip.chipid = cca.chip_chipid" +
        "          join localconfigkey lck" +
        "               on lck.localconfigkeyid = cca.lck_localconfigkeyid" +
		"         where chipid = :id and lck.localconfigkeyid = " +inputConfigKey.getId()+
		"      order by modified desc" +
		"        )" +
		"       ) " + 
		"where lead_paramval is null" +
		"      or lead_paramval <> paramval " +
		"order by 1, 3 desc";
		
		logger.debug("created query:\n" + query_string);
		final SQLQuery query = context.currentSession().createSQLQuery(query_string);
		query
		.addScalar("CHIPID", Hibernate.LONG)
		.addScalar("PARAMVAL", Hibernate.STRING)
		.addScalar("MODIFIED", Hibernate.TIMESTAMP)
		.addScalar("LCKNAME", Hibernate.STRING)
		.setParameter("id", chipId, Hibernate.INTEGER);

		List<ParamModified> result = new ArrayList<ParamModified>();

		for (Object every: query.list()) {
			Object[] row = (Object[]) every;

			final ParamModified pm = new ParamModified((String) row[1], (Timestamp) row[2], (String) row[3]); 
			logger.debug("chipId " + row[0] + ": " + pm);
			result.add(pm);
		}
		
		return result;
	}
	
	
	public BigDecimal getNoOfDisableChips() throws HibernateException, DataAccessException {
		final String query_string = "select count(*) from chipdisabled cd join chip ch on cd.chip_chipid = ch.chipid where type = 'FEBCHIP' ";
		
		logger.debug("created query:\n" + query_string);
		final SQLQuery query = context.currentSession().createSQLQuery(query_string);
		BigDecimal result = (BigDecimal)query.uniqueResult();
		
		System.out.println("Result : "+result);
		
		return result;
	}

	public BigDecimal getTotalNoOfChips() throws HibernateException, DataAccessException {
		final String query_string = "select count(*) from chip where type = 'FEBCHIP' ";
		
		logger.debug("created query:\n" + query_string);
		final SQLQuery query = context.currentSession().createSQLQuery(query_string);
		BigDecimal result = (BigDecimal)query.uniqueResult();
		
		System.out.println("Result : "+result);
		
		return result;
	}
	public long getNoOfDisableStrips() throws HibernateException, DataAccessException {

		long maskedStripsCnt = 0;
		LocalConfigKey inputConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.CURRENT_LBS_CONF_KEY);

		//System.out.println("IDDDDDD"+inputConfigKey.getId());
		
		/*
		long start = System.currentTimeMillis();
    List<ChipConfAssignment> chipConfAssignments = configurationDAO.getChipConfAssignments(ChipType.SYNCODER, inputConfigKey);    	
    	System.out.println("getChipConfAssignments() " + (System.currentTimeMillis() - start)/1000. + " s");
    	
    	
    	for(ChipConfAssignment chipConfAssignment : chipConfAssignments) {   
    		SynCoderConf synCoderConf  = (SynCoderConf)chipConfAssignment.getStaticConfiguration();

    		Binary connectedMask = new Binary( ((LinkBoard)chipConfAssignment.getChip().getBoard()).getConnectedStripsMask() );
    		
    		Binary mask = new Binary(synCoderConf.getInChannelsEna());    		
    		for (int i=0; i < mask.getBitNum();i++){
				boolean bitValue = mask.getBits(i, 1) != 0;		
				boolean connectedbitValue = connectedMask.getBits(i, 1) != 0;
				if(!bitValue && connectedbitValue) {
					maskedStripsCnt++;
				}
			}
    	}

    	System.out.println("countMaskedStrips() " + (System.currentTimeMillis() - start)/1000. + " s");
    	System.out.println("countMaskedStrips: " + maskedStripsCnt);
	*/
		
		
		long stripsdisable=0,totalstrips=0;
		
		//LocalConfigKey inputConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.CURRENT_LBS_CONF_KEY);
		
		String sql_query = " select lcf.inchannelsena from linkchipconf lcf "+

		" join staticconfiguration st on st.staticconfigurationid = lcf.linkchipstaticconfid "+ 

		" join chipconfassignment cca on cca.sc_staticconfigurationid = st.staticconfigurationid "+

		" join localconfigkey lck on lck.localconfigkeyid = cca.lck_localconfigkeyid " +

		" join Chip ch on ch.chipid = cca.chip_chipid "+

		" join Board b on  ch.board_boardid = b.boardid  "+

		" where b.type= 'LINKBOARD' and b.crate_crateid in (select c.crateid from Crate c where c.type = 'LINKBOX')"+ 

		" and lck.name = '" +inputConfigKey.getName()+ "' and cca.creationdate = (select max(cca1.creationdate) from chipconfassignment cca1 where cca1.chip_chipid = cca.chip_chipid and cca1.lck_localconfigkeyid = cca.lck_localconfigkeyid)";
		
		SQLQuery query = context.currentSession().createSQLQuery(sql_query);
		
		Binary hexMask;
		List a = query.list();
		
		for (Iterator iter = a.iterator(); iter.hasNext();){
			byte[] lb = (byte[]) iter.next();
			hexMask = new Binary(lb);
			//System.out.println("Bit Number "+hexMask.getBitNum());
			for (int i=0; i <hexMask.getBitNum();i++){
				
				boolean bitValue = hexMask.getBits(i, 1) != 0;
				
				if(bitValue)
				{
					
				}else{
					stripsdisable++;
				}
			}
		}
		System.out.println("Disable Strips : "+stripsdisable);

		sql_query = "select count(*) from board b where b.type= 'LINKBOARD' and b.crate_crateid in (select c.crateid from Crate c where c.type = 'LINKBOX') ";
		query = context.currentSession().createSQLQuery(sql_query);
		
		BigDecimal linkboardchannels = (BigDecimal)query.uniqueResult();
		
		stripsdisable = stripsdisable - (linkboardchannels.longValue()*96-getTotalNoOfStrips())  ;
		
		System.out.println("Disable Strips : "+stripsdisable);
		//stripsdisable = stripsdisable -8664;
		maskedStripsCnt = stripsdisable;
//*/
    	
		return  maskedStripsCnt;
		
		
		
	}
		
	public long getTotalNoOfStrips() throws HibernateException, DataAccessException {
		
		LocalConfigKey inputConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.CURRENT_LBS_CONF_KEY);
	
		String sql_query = "select count(*) from chamberstrip ch "+

		" join febconnector fc on ch.fc_febconnectorid = fc.febconnectorid "+
		" join feblocation fl on fl.feblocationid = fc.fl_feblocationid "+
		" where fl.lb_linkboardid in ( select b.boardid from linkchipconf lcf "+

		" join staticconfiguration st on st.staticconfigurationid = lcf.linkchipstaticconfid "+ 

		" join chipconfassignment cca on cca.sc_staticconfigurationid = st.staticconfigurationid "+

		" join localconfigkey lck on lck.localconfigkeyid = cca.lck_localconfigkeyid " +

		" join Chip ch on ch.chipid = cca.chip_chipid "+

		" join Board b on  ch.board_boardid = b.boardid  "+

		" where b.type= 'LINKBOARD' and b.crate_crateid in (select c.crateid from Crate c where c.type = 'LINKBOX')"+ 

		" and lck.name = '" +inputConfigKey.getName()+ "' and cca.creationdate = (select max(cca1.creationdate) from chipconfassignment cca1 where cca1.chip_chipid = cca.chip_chipid and cca1.lck_localconfigkeyid = cca.lck_localconfigkeyid))";
		
		SQLQuery query = context.currentSession().createSQLQuery(sql_query);
		
		BigDecimal result = (BigDecimal)query.uniqueResult();
		
		
		return  result.longValue() ;
		
	}
	public double getPercentageOfStrips() throws DataAccessException {
		
		return ((double)getNoOfDisableStrips() / (double) (getTotalNoOfStrips())*100);
	}
	public BigDecimal getTotalFEBBoard() throws HibernateException, DataAccessException {
		final String query_string = "select count(*) from board b " +
		" join febboard feb on b.boardid = feb.febboardid " +
		" join feblocation fl on feb.fl_feblocationid = fl.feblocationid " +
		" join chamberlocation ch on ch.chamberlocationid = fl.cl_chamberlocationid "+
		" where ch.barrelorendcap = 'Barrel' ";
		
		logger.debug("created query:\n" + query_string);
		final SQLQuery query = context.currentSession().createSQLQuery(query_string);
		BigDecimal result = (BigDecimal)query.uniqueResult();
		
		System.out.println("Result : "+result);
		
		return result;
	}
	
	public BigDecimal getNoOfFEBDTControlled() throws HibernateException, DataAccessException {
		final String query_string = "select count(*) from febboarddtcontrolled ";
		
		logger.debug("created query:\n" + query_string);
		final SQLQuery query = context.currentSession().createSQLQuery(query_string);
		BigDecimal result = (BigDecimal)query.uniqueResult();
		
		System.out.println("Result : "+result);
		
		return result;
	}
	public List<ParamModified> getFECHistory(int[] chipid) throws HibernateException, DataAccessException {
		// TODO: remove the {lag,lead}_modified from the query string
		// TODO: switch to entity query, see http://docs.jboss.org/hibernate/core/3.3/reference/en/html/querysql.html#d0e13696
		
		LocalConfigKey localConfigKeyFEBs = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_FEBS_DEFAULT);
		
		final String query_string = "" + 
		
		" select  cca.creationdate modified,  brd.name name, chip.position pos, fcc.vth vth, fcc.vthoffset vthoffset ,fcc.vmon vmon,fcc.vmonoffset vmonoffset , lck.name lckname from febchipconf fcc join chipconfassignment cca on cca.sc_staticconfigurationid = fcc.febchipconfid" +
		" join chip on chip.chipid = cca.chip_chipid join localconfigkey lck on lck.localconfigkeyid = cca.lck_localconfigkeyid" +
		" join board brd on chip.board_boardid = brd.boardid"  +
		" where chipid in (" +
		chipid[0]+","+chipid[1]+","+chipid[2]+","+chipid[3]+","+chipid[4]+","+chipid[5]+","+chipid[6]+","+chipid[7]+","+chipid[8]+","+chipid[9]+","+chipid[10]+","+chipid[11]+")" +
		" and lck.localconfigkeyid = "+localConfigKeyFEBs.getId()+		
		" order by cca.creationdate desc";
		
		logger.debug("created query:\n" + query_string);
		final SQLQuery query = context.currentSession().createSQLQuery(query_string);
		query
		.addScalar("MODIFIED", Hibernate.TIMESTAMP)
		.addScalar("NAME", Hibernate.STRING)
		.addScalar("POS", Hibernate.INTEGER)
		.addScalar("VTH", Hibernate.INTEGER)
		.addScalar("VTHOFFSET", Hibernate.INTEGER)
		.addScalar("VMON", Hibernate.INTEGER)
		.addScalar("VMONOFFSET", Hibernate.INTEGER)
		.addScalar("LCKNAME", Hibernate.STRING);
	/*
		.setParameter("id1", chipId[0], Hibernate.INTEGER)
		.setParameter("id2", chipId[1], Hibernate.INTEGER)
		.setParameter("id3", chipId[2], Hibernate.INTEGER)
		.setParameter("id4", chipId[3], Hibernate.INTEGER)
		.setParameter("id5", chipId[4], Hibernate.INTEGER)
		.setParameter("id6", chipId[5], Hibernate.INTEGER)
		.setParameter("id7", chipId[6], Hibernate.INTEGER)
		.setParameter("id8", chipId[7], Hibernate.INTEGER)
		.setParameter("id9", chipId[8], Hibernate.INTEGER)
		.setParameter("id10", chipId[9], Hibernate.INTEGER)
		.setParameter("id11", chipId[10], Hibernate.INTEGER)
		.setParameter("id12", chipId[11], Hibernate.INTEGER);
*/
		System.out.println("Query String "+query_string);
		
		List<ParamModified> result = new ArrayList<ParamModified>();

		for (Object every: query.list()) {
			Object[] row = (Object[]) every;
			String name = (String) row[1];
			if (name.contains("FEB_"))
				name = name.replace("FEB_", "");
		
			final ParamModified pm = new ParamModified((Timestamp) row[0], name,((Integer) row[2]).intValue(),((Integer)row[3]).intValue(),((Integer) row[4]).intValue(),((Integer)row[5]).intValue(),((Integer)row[6]).intValue(),(String)row[7]); 
			logger.debug("chipId " + row[0] + ": " + pm);
			result.add(pm);
		}
		
		return result;
	}

	public List<ChipsDisabledDetail> getChipsDisabled() throws HibernateException, DataAccessException{

		final String query_string = "" + 
	"	select b.name, ch.chipid, ch.position, fl.posinlocaletapartition, fl.feblocaletapartition from chipdisabled cd join chip ch on cd.chip_chipid = ch.chipid "+
	"	join board b on ch.board_boardid = b.boardid "+
	"	join febboard fb on b.boardid = fb.febboardid "+
	"	join feblocation fl on fb.fl_feblocationid = fl.feblocationid "+
	"	where ch.type = 'FEBCHIP' ";
		final SQLQuery query = context.currentSession().createSQLQuery(query_string);
		chipsDisabled = new ArrayList<ChipsDisabledDetail>();
		chipsDisabled.clear();
		for (Object every: query.list()) {
			Object[] row = (Object[]) every;
			String name = (String) row[0];
			if (name.contains("FEB_"))
				name = name.replace("FEB_", "");
		
			ChipsDisabledDetail cd = new ChipsDisabledDetail(name, (String) row[4],((BigDecimal) row[3]).intValue(), ((BigDecimal) row[1]).intValue(), ((BigDecimal) row[2]).intValue(), true);
		
			chipsDisabled.add(cd);
		}
	
		
		return chipsDisabled;
	}



	public List<DTLinesDetail> getDtLinesDetail() throws HibernateException, DataAccessException {
	
		final String query_string = "" + 
		" select dt.board_boardid, b.name, fl.feblocaletapartition,fl.posinlocaletapartition, (select max(cs.chamberstripnumber) from chamberstrip cs where cs.fc_febconnectorid = fc.febconnectorid ) as ending, (select min(cs.chamberstripnumber) from chamberstrip cs where cs.fc_febconnectorid = fc.febconnectorid ) as starting from febboarddtcontrolled dt "+
		" join board b on dt.board_boardid = b.boardid "+
		" join febboard fb on b.boardid = fb.febboardid "+
		" join feblocation fl on fb.fl_feblocationid = fl.feblocationid "+
		" join febconnector fc on fl.feblocationid = fc.fl_feblocationid"; 	
		
		final SQLQuery query = context.currentSession().createSQLQuery(query_string);
			dtLinesDetail = new ArrayList<DTLinesDetail>();
			dtLinesDetail.clear();
			for (Object every: query.list()) {
				Object[] row = (Object[]) every;
				String name = (String) row[1];
				if (name.contains("FEB_"))
					name = name.replace("FEB_", "");
			
				DTLinesDetail cd = new DTLinesDetail(name, (String) row[2],((BigDecimal) row[3]).intValue(), ((BigDecimal) row[0]).intValue(),  true, ((BigDecimal) row[5]).intValue(), ((BigDecimal) row[4]).intValue());
			
				dtLinesDetail.add(cd);
			}
		
		
		
		return dtLinesDetail;
	}
	
		
	public int getNumber(ChamberStrip cs) {
		final int lbInputNum = cs.getFebConnector()
				.getLinkBoardInputNum() - 1;
		final int cableChannelNum = cs.getCableChannelNum();
		final int channelsPerFEB = 16;
		final int channelNumber = lbInputNum * channelsPerFEB
				+ cableChannelNum;

		return channelNumber;
	}
}