//---------------------------------------------------------------------------

#include "precompiled.h"
#pragma hdrstop

#include "rpct/aboot2/TTTFDataSource.h"
#include "rpct/aboot2/tb.h"
#include "rpct/aboot2/tbvme.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)




TTTFDataSource::TTTFDataSource(int bit_count,
                 const char* file0,
                 const char* file1,
                 const char* file2,
                 const char* file3,
                 bool pack)
 : BitCount(bit_count), CurrBitPos(0),
   FIn0(NULL), FIn1(NULL), FIn2(NULL), FIn3(NULL), Pack(pack)
{
  if((FIn0=fopen(file0,"rt"))==NULL)
    throw TException( std::string("can't open file: ") + file0 );

  if (file1 != NULL)
    if((FIn1=fopen(file1,"rt"))==NULL)
      throw TException( std::string("can't open file: ") + file1 );

  if (file2 != NULL)
    if((FIn2=fopen(file2,"rt"))==NULL)
      throw TException( std::string("can't open file: ") + file2 );
            
  if (file3 != NULL)
    if((FIn3=fopen(file3,"rt"))==NULL)
      throw TException( std::string("can't open file: ") + file3 );

  DataMask = (1 << BitCount) - 1;

  if (FIn1 != NULL)
    DataMask |= (DataMask << 8);
  if (FIn2 != NULL)
    DataMask |= (DataMask << 8); 
  if (FIn3 != NULL)
    DataMask |= (DataMask << 8);
  
  GetData();
}


TTTFDataSource::~TTTFDataSource()
{
  if (FIn0 != NULL)
    fclose(FIn0);
  if (FIn1 != NULL)
    fclose(FIn1);
  if (FIn2 != NULL)
    fclose(FIn2);
  if (FIn3 != NULL)
    fclose(FIn3);
}


bool TTTFDataSource::More()
{
  return !FMore;
}

void TTTFDataSource::GetData()
{
  TB_MESSAGE m;
  uint32_t tmp_data;

  if((m=aboot_read_data2(FIn0,&Data))!=YES){
    if(m!=NO)
      throw TException(tb_perror_str());
    else
      FMore = true;
  }
  else
    FMore = false;

  /////////////////////////////////
  // FIn1
  if (FIn1 == NULL)
    return;

  if((m=aboot_read_data2(FIn1,&tmp_data))!=YES){
    if(m!=NO)
      throw TException(tb_perror_str());
    else
      FMore = true;
  }
  else
    FMore = false;


  Data = (Data & 0xFF) | (tmp_data << 8);
                                                 
  /////////////////////////////////
  // FIn2
  if (FIn2 == NULL)
    return;

  if((m=aboot_read_data2(FIn2,&tmp_data))!=YES){
    if(m!=NO)
      throw TException(tb_perror_str());
    else
      FMore = true;
  }
  else
    FMore = false;
    
  Data = (Data & 0xFFFF) | (tmp_data << 16);
                                                    
  /////////////////////////////////
  // FIn3
  if (FIn3 == NULL)
    return;

  if((m=aboot_read_data2(FIn3,&tmp_data))!=YES){
    if(m!=NO)
      throw TException(tb_perror_str());
    else
      FMore = true;
  }
  else
    FMore = false;

  Data = (Data & 0xFFFFFF) | (tmp_data << 24);
}

       
uint32_t TTTFDataSource::PackData(uint32_t data)
{
  if (Pack && (FIn1 != NULL) && (BitCount != 8)) {
    uint32_t byteMask = DataMask & 0xff;
    uint32_t newdata = data & DataMask;
    newdata |= ((data >> 8) & DataMask) << BitCount;
    newdata |= ((data >> 16) & DataMask) << (BitCount*2);  
    newdata |= ((data >> 24) & DataMask) << (BitCount*3);
    return newdata;
  }
  else
    return data;
}

uint32_t TTTFDataSource::ReadData()
{  
  if (BitCount == 8) {
    uint32_t data = Data;
    GetData();
    return data;
  }
  else {
    if (CurrBitPos == (8 - BitCount)) {
      uint32_t data = (Data >> CurrBitPos) & DataMask;
      GetData();
      CurrBitPos = 0;
      return PackData(data);
    }
    else {                
      uint32_t data = (Data >> CurrBitPos) & DataMask;
      CurrBitPos += BitCount;
      return PackData(data);
    }
  }
}



