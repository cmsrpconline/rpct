/*
 * vmeTest.cpp
 *
 *  Created on: Mar 1, 2018
 *      Author: rpcpro
 */
#include "tb_vme.h"
#include <sstream>
#include <stdlib.h>
#include "rpct/std/IllegalArgumentException.h"

using namespace std;
using namespace rpct;

int main(int argc, char* argv[]){
    G_Debug.SetLevel(TDebug::VME_OPER);

    int link = 1;
    if(argc >= 2) {
        link = atol(argv[1]);
    }

    string block = "";
    if(argc >= 3) {
        block = string(argv[2]);
    }

    TVMECAEN vme(link, 0);
    uint32_t address = 0x414000;
    size_t  count = 200;
    char* data = new char[count];
    for(unsigned int i = 0; i < count; i+=4) {
        data[i] = i/4;
    }

    for(int i = 0; i < 100000; i++) {
        if(block == "b") {
            vme.Write(address, data, count);
            vme.Read(address, data, count);
            //cout<<"read "<<i<<endl;
        }
        vme.Write32(address, 0x1234);
        if(i%10 == 0) {
            cout<<i<<"\r";
        }
    }
    cout<<"\ndone"<<"\n";

}



