
#include "precompiled.h"
#pragma hdrstop

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>

#include "rpct/aboot2/tb.h"

TB_MESSAGE tb_errno = YES;
char *tb_logfile_name="tb-logfile.txt";

static TB_MESSAGE _break_flag=ILLEGAL_PARAMETER;

char *tb_perror_str(void)
{
    switch( tb_errno ){
    case NO:                  return("Error type not defined");
    case YES:                 return("No error: Last action succesful");
    case NULL_MESSAGE:        return("No error : null message");
    case ILLEGAL_PARAMETER:   return("Illegal parameter");
    case NULL_POINTER:        return("Null pointer");
    case BOARD_NOT_INSTALLED: return("Board not installed");
    case BOARD_IS_INSTALLED:  return("Board is installed");
    case VME_ERROR:           return("VME error");
    case STATE_NOT_ALLOWED:   return("Board is in not allowed state");
    case PARSE_ERROR:         return("Parse error");
    case NO_ANSWER_FROM_RC:   return("RC did not answer");
    case NO_CHANNEL_AVAIL:    return("No channel available");
    case FILE_ERROR:          return("couldn't access a file");
    case REOPEN:              return("reopening not allowed");
    case RC_ERR:              return("RC error");
    case RC_STACK_OVER:       return("RC include file stack over");
    case ENV_ERROR:           return("environment variable not defined");
    case RC_NO_EVENT:         return("event no exist");
    case RC_NOT_CONNECTED:    return("client didn't connect to the RC");
    default:                  return("not supported tb_errno value");
    }
}

void tb_perror( char* s )
{
    fprintf( stderr,"test bench error no %d :%s\n",tb_errno,tb_perror_str());
    if( s!=NULL && *s!='\0' )
	fprintf(stderr,"%s\n",s);
}

TB_MESSAGE envpath(char *dst, char *envname, char *src)
{
    char *envpath;

    if(dst==NULL)
	RETURN(tb_errno=ILLEGAL_PARAMETER);
    if(envname==NULL)
	RETURN(tb_errno=ILLEGAL_PARAMETER);
    if(src==NULL)
	RETURN(tb_errno=ILLEGAL_PARAMETER);
    if((envpath=getenv(envname))==NULL)
	RETURN(tb_errno=ENV_ERROR);
    strcpy(dst,envpath);
    strcat(dst,"/");
    strcat(dst,src);
    return(YES);
}

void _break_function(int)
{
    _break_flag=YES;
    signal( SIGINT,_break_function); 
}

TB_MESSAGE install_break(void)
{
    if(_break_flag!=ILLEGAL_PARAMETER)
	RETURN(tb_errno=ILLEGAL_PARAMETER);
    _break_flag=NO;
    signal( SIGINT,_break_function); 
    return(YES);
}

TB_MESSAGE uninstall_break(void)
{
    if(_break_flag==ILLEGAL_PARAMETER)
	RETURN(tb_errno=ILLEGAL_PARAMETER);
    _break_flag=ILLEGAL_PARAMETER;
    signal( SIGINT,SIG_DFL); 
    return(YES);
}

TB_MESSAGE test_break(void)
{
   TB_MESSAGE flag;
 
    if(_break_flag==ILLEGAL_PARAMETER)
	RETURN(tb_errno=ILLEGAL_PARAMETER);
    flag=_break_flag;
    _break_flag=NO;
    return(flag);
}

TB_MESSAGE logfile_message(char *message)
{
    FILE *fp;

    if(tb_logfile_name!=NULL){
	if((fp=fopen(tb_logfile_name,"at"))==NULL)
	    return(FILE_ERROR);
	if(fprintf(fp,"%s",message==NULL?"(null)":message)==EOF)
	    return(FILE_ERROR);
	fclose(fp);}
    return(YES);
}

TB_MESSAGE logfile_fileline(char *filename, int line, char *info)
{
    TB_MESSAGE m;
    char str[100];

    sprintf(str,"file: %s, ",filename);
    if((m=logfile_message(str))!=YES)
	return(m);
    sprintf(str,"line: %d, ",line);
    if((m=logfile_message(str))!=YES)
	return(m);
    sprintf(str,"message: %d (%s), ",tb_errno,tb_perror_str());
    if((m=logfile_message(str))!=YES)
	return(m);
    sprintf(str,"info:%s\n",info==NULL?"(none)":info);
    if((m=logfile_message(str))!=YES)
	return(m);
    return(YES);
}

TB_MESSAGE return_message(TB_MESSAGE message, char *filename, int line, char *info)
{
    TB_MESSAGE m;

    if(message<0)
	if((m=logfile_fileline(filename,line,info))!=YES){
	    fprintf(stderr,"return_message: message= %d\n",m);
	    fflush(stderr);
	}
    return(message);
}
