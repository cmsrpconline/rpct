//---------------------------------------------------------------------------

#include "precompiled.h"
#pragma hdrstop

#include "rpct/aboot2/TVMEABootContr2.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)



void TVMEABootContr2::Reset()
{
  uint32_t value = VME.Read32(Base);
  VME.Write32(Base, (value & 0x1f) | 0x60);
}

int TVMEABootContr2::GetStatusFlag(uint32_t mask)
{
  return ((VME.Read32(Base) & mask) == 0) ? 0 : 1;
}

void TVMEABootContr2::SetStatusFlag(int level, uint32_t mask)
{
    if ((level != 0) && (level != 1))
      throw TException("Illegal parameter");

    uint32_t value = VME.Read32(Base);
    VME.Write32(Base, ((level==0?0:mask) | (value & (~mask))) & 0x1f);
}

void TVMEABootContr2::TestStatusFlag(int level, int count, uint32_t mask)
{
  int get_level;

  if (level!=0 && level!=1)
    throw TException("Illegal parameter");
  if (count<1)
    throw TException("Illegal parameter");

  for (int k=0; k<count; k++) {
    if (level == GetStatusFlag(mask))
      return;
  }
  throw TException("incorrect level");
}

void TVMEABootContr2::TSetStatusFlag(int level, int count, uint32_t mask)
{
  SetStatusFlag(level, mask);
  TestStatusFlag(level, count, mask);
}



void TVMEABootContr2::SendData(uint32_t data)
{
  VME.Write32(Base+8ul, data);
}


void TVMEABootContr2::SendData(uint32_t *buf, int number)
{
  if (number<1)
    throw TException("Illegal parameter");

  VME.Write(Base+0x40000, (char *)buf, sizeof(uint32_t)*number);
}


