/************************************\
*                                    *
* TEST BENCH VME INTERFACE - LIBRARY *
*                                    *
* written by Krzysztof Pozniak, 1996 *
*                                    *
\************************************/

#include "precompiled.h"
#pragma hdrstop
#include <string.h>

#include "rpct/aboot2/vme.h"
#include "rpct/aboot2/tbvme.h"


#define BOOT_DATABUF_LEN 4096
#define BOOT_REPEAT_NUM 1000
#define BOOT_CLOCK_NUM 50


TB_MESSAGE tbvme_reset(uint32_t base)
{
    if(VME32Write(base+0xcul, 0x13ul)<0)
		RETURN(tb_errno=VME_ERROR);
    return(YES);
}

TB_MESSAGE tbvme_reset2(uint32_t base)
{
    uint32_t value;
	
    if(VME32Read(base+0ul, &value)<0)
		RETURN(tb_errno=VME_ERROR);
    if(VME32Write(base+0ul, (value & 0x1f) | 0x60)<0)
		RETURN(tb_errno=VME_ERROR);
    return(YES);
}

TB_MESSAGE tbvme_get_status_flag(uint32_t base, int *level,
							  uint32_t mask)
{
    uint32_t value;
	
    if(level==NULL)
		RETURN(tb_errno=NULL_POINTER);
    if(VME32Read(base+0ul, &value)<0)
		RETURN(tb_errno=VME_ERROR);
    (*level)= (value & mask)==0?0:1;
    return(YES);
}

TB_MESSAGE tbvme_set_status_flag(uint32_t base, int level,
							  uint32_t mask)
{
    uint32_t value;
	
    if(level!=0 && level!=1)
		RETURN(tb_errno=ILLEGAL_PARAMETER);
    if(VME32Read(base+0ul, &value)<0)
		RETURN(tb_errno=VME_ERROR);
    value = (level==0?0:mask) | (value & (~mask));
    if(VME32Write(base+0ul, value)<0)
		RETURN(tb_errno=VME_ERROR);
    return(YES);
}

TB_MESSAGE tbvme_set_status_flag2(uint32_t base, int level,
							   uint32_t mask)
{
    uint32_t value;
	
    if(level!=0 && level!=1)
		RETURN(tb_errno=ILLEGAL_PARAMETER);
    if(VME32Read(base+0ul, &value)<0)
		RETURN(tb_errno=VME_ERROR);
    value = ((level==0?0:mask) | (value & (~mask))) & 0x1f;
    if(VME32Write(base+0ul, value)<0)
		RETURN(tb_errno=VME_ERROR);
    return(YES);
}

TB_MESSAGE tbvme_test_status_flag(uint32_t base, int level, int count,
							   uint32_t mask)
{
    TB_MESSAGE m;
    int k, get_level;
	
    if(level!=0 && level!=1)
		RETURN(tb_errno=ILLEGAL_PARAMETER);
	if(count<1)
		RETURN(tb_errno=ILLEGAL_PARAMETER);
	
    for(k=0;k<count;k++){
		if((m=tbvme_get_status_flag(base, &get_level, mask))!=YES)
			RETURN(m);
		if(level==get_level)
			return(YES);
    }
    return(NO);
}

TB_MESSAGE tbvme_tset_status_flag(uint32_t base, int level, int count,
							   uint32_t mask)
{
    TB_MESSAGE m;
	
    if((m=tbvme_set_status_flag(base,level,mask))!=YES)
		RETURN(m);
    RETURN(tbvme_test_status_flag(base,level,count,mask));
}

TB_MESSAGE tbvme_tset_status_flag2(uint32_t base, int level, int count,
								uint32_t mask)
{
    TB_MESSAGE m;
	
    if((m=tbvme_set_status_flag2(base,level,mask))!=YES)
		RETURN(m);
    RETURN(tbvme_test_status_flag2(base,level,count,mask));
}

TB_MESSAGE aboot_send_data(uint32_t base, uint32_t data)
{
    if(VME32Write(base+8ul,data)<0)
        RETURN(tb_errno=VME_ERROR);
    return(YES);
}

TB_MESSAGE aboot_send_buf(uint32_t base, uint32_t *buf, int number)
{
    if(number<1)
		RETURN(tb_errno=ILLEGAL_PARAMETER);
    if(VMEWrite(base+0x40000,(char *)buf,sizeof(uint32_t)*number)<0)
        RETURN(tb_errno=VME_ERROR);
    return(YES);
}

TB_MESSAGE aboot_read_data(FILE *fp, uint32_t *data)
{
    int value;
    char c;
	
    if(fp==NULL)
		RETURN(tb_errno=NULL_POINTER);
    if(data==NULL)
		RETURN(tb_errno=NULL_POINTER);
    if(fscanf(fp,"%d%c",&value,&c)!=2)
		return(NO);
    if(value<0 || value>255)
		RETURN(FILE_ERROR);
    (*data)=(uint32_t)value;
    return(YES);
}

TB_MESSAGE aboot_exec(uint32_t base, int def_path_flag, char *file_name)
{
    TB_MESSAGE m;
    FILE *fp;
    char path[200];
    uint32_t buffer[BOOT_DATABUF_LEN];
    int number;
	
    if(def_path_flag!=NO && def_path_flag!=YES)
		RETURN(tb_errno=ILLEGAL_PARAMETER);
    if(file_name!=NULL){
		if(def_path_flag==NO)
			strcpy(path,file_name);
		else
			if((m=envpath(path,TB_ETC_DIR,file_name))!=YES)
				RETURN(m);
			if((fp=fopen(path,"rt"))==NULL)
				RETURN(tb_errno=FILE_ERROR);
    }
    else
		fp=stdin;
	
    if((m=aboot_tset_nCONFIG(base, 0, 1))!=YES)
		RETURN(m);
    if((m=aboot_test_nSTATUS(base, 0, BOOT_REPEAT_NUM))!=YES)
		RETURN(m);
    if((m=aboot_tset_nCONFIG(base, 1, 1))!=YES)
		RETURN(m);
    if((m=aboot_test_nSTATUS(base, 1, BOOT_REPEAT_NUM))!=YES)
		RETURN(m);
    if((m=aboot_test_CONF_DONE(base, 0, 1))!=YES)
		RETURN(m);
	
    do{
		for(number=0;number<BOOT_DATABUF_LEN;)
			if((m=aboot_read_data(fp,&buffer[number]))!=YES){
				if(m!=NO)
					RETURN(m);
				break;
			}
			else
				number++;
			if(number>0)
				if((m=aboot_send_buf(base,buffer,number))!=YES)
					RETURN(m);
    } while(number==BOOT_DATABUF_LEN);
	
    if(fp!=stdin)
		fclose(fp);
	
    if((m=aboot_test_nSTATUS(base, 1, 1))!=YES)
		RETURN(m);
    if((m=aboot_test_CONF_DONE(base, 0, 1))!=YES)
		RETURN(m);
    for(number=0;number<BOOT_CLOCK_NUM;number++)
		if((m=aboot_send_data(base, 255ul))!=YES)
			RETURN(m);
		if((m=aboot_test_CONF_DONE(base, 1, BOOT_REPEAT_NUM))!=YES)
			RETURN(m);
		
		return(YES);
}
