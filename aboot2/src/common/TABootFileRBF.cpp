//---------------------------------------------------------------------------

#include "precompiled.h"
#pragma hdrstop

#include "rpct/aboot2/TABootFileRBF.h"
#include "tb_std.h"
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>

//---------------------------------------------------------------------------


TABootFileRBF::TABootFileRBF(std::string fileName, int bitCount) :
    FileName(fileName), FIn(NULL) /*FD(0)*/, FHasNext(true), BitCount(bitCount), CurrBitPos(0), FileBuf(0),
            FileBufSize(64), FileBufValid(0) {
    DataMask = (1 << BitCount) - 1;
    LastBitPos = 8 - BitCount; // when reading bit by bit, this is a last valid position of CurrBitPos
    FileBufPos = FileBufSize;

    FIn = fopen(FileName.c_str(), "rb");
    if (FIn == NULL)
        throw TException("Can't open file: '" + FileName + "'");
    /*FD = open(FileName.c_str(), O_RDONLY |  O_NONBLOCK);  
     if (FD == -1) {
     throw TException("Can't open file: '" + FileName + "': " + strerror(errno));
     }*/

    FileBuf = new unsigned char[FileBufSize];

    GetData();
}

TABootFileRBF::~TABootFileRBF() {
    if (FIn != NULL)
        fclose(FIn);
    /*if (FD != 0 && FD != -1) {
     close(FD);
     }*/delete [] FileBuf;
}

/*
 void TABootFileRBF::GetData()
 {    
 // TODO !!! dorobic czytanie wiekszej ilosci bajtow do bufora FileBuf
 unsigned char tmp_data;
 FHasNext = fread(&tmp_data, 1, 1, FIn) == 1;
 //FHasNext = read(FD, &tmp_data, 1) == 1;
 Data = FHasNext ? tmp_data : 0xff; 
 }*/

void TABootFileRBF::GetData() {
    if (FileBufPos == FileBufSize) {
        FileBufValid = fread(FileBuf, 1, FileBufSize, FIn);
        FileBufPos = 0;
    }
    FHasNext = FileBufPos < FileBufValid;
    Data = FHasNext ? FileBuf[FileBufPos] : 0xff;
    FileBufPos++;
}

uint32_t TABootFileRBF::Next() {
    if (BitCount == 8) {
        uint32_t data = Data;
        GetData();
        return data;
    } else {
        uint32_t data = (Data >> CurrBitPos) & DataMask;
        if (CurrBitPos == LastBitPos) {
            GetData();
            CurrBitPos = 0;
        } else {
            CurrBitPos += BitCount;
        }
        return data;
    }
}
