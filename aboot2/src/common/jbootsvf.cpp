/*
 * jbootsvf.cpp
 *
 *  Created on: Jun 14, 2012
 *      Author: Karol Bunkowski
 */

#include "jal/jtagChain/JTAGChain.h"
#include "jal/jtagChain/JTAGScanData.h"
#include "jal/jtagSVFSequencer/JTAGSVFSequencer.h"
#include "rpct/devices/JTAGControllerImpl.h"

#include "rpct/devices/hsb.h"
#include "rpct/devices/fsb.h"

#include <iostream>
#include <exception>

#include <stdint.h>

#include "log4cplus/consoleappender.h"

#include <boost/timer.hpp>
#include <log4cplus/configurator.h>

using namespace std;
using namespace HAL;
using namespace jal;
using namespace rpct;
using namespace log4cplus;

int main(int argc, char **argv) {
	SharedAppenderPtr myAppender(new ConsoleAppender());
	myAppender->setName("myAppenderName");
	std::auto_ptr<Layout> myLayout = std::auto_ptr<Layout>(new log4cplus::TTCCLayout());
	myAppender->setLayout( myLayout );
	//Logger::getInstance("RpctSystem").addAppender(myAppender);
	Logger::Logger::getRoot().setLogLevel(INFO_LOG_LEVEL);
	//Logger::getInstance("TIIDevice").addAppender(myAppender);
	//Logger::getInstance("JTAGControllerImpl").addAppender(myAppender);
	Logger::getInstance("TIIJtagIO").addAppender(myAppender);

	//Logger::getInstance("JTAGControllerImpl").setLogLevel(DEBUG_LOG_LEVEL);
	Logger::getInstance("TIIJtagIO").setLogLevel(INFO_LOG_LEVEL);


	try {
		double SCK_frequency = 1.e6;

		string ii_board = "hsb";
		string vmeInterface = "CAEN";
		std::string vmeUnit = "2";
		unsigned short addr = 0xA;
		bool fast = true;

		std::auto_ptr<TVMEInterface> vme;
		if (vmeInterface.empty()) {
			cout << "Using default VME interface" << endl;
			vme.reset(TVMEFactory().Create());
		}
		else {
			if (vmeUnit.empty()) {
				cout << "Using '" << vmeInterface << "' VME interface" << endl;
				vme.reset(TVMEFactory().Create(vmeInterface));
			}
			else {
				cout << "Using '" << vmeInterface << "' VME interface, unit " << vmeUnit << endl;
				vme.reset(TVMEFactory().Create(vmeInterface, vmeUnit));
			}
		}

		std::auto_ptr<rpct::IBoard> board;
		IJtagIO* ijtagIO = 0;

		if (ii_board == "hsb") {
			rpct::THsb* hsb(new rpct::THsb(1900, "HSB", vme.get(), addr, NULL, -1, 1998));
			hsb->GetHsbVme().SetJtagEnable(true);
			hsb->GetHsbVme().checkName();
			cout<<"BITS_STATUS "<<hex<<hsb->GetHsbVme().readWord(THsbVme::WORD_CHECKSUM, 0)<<endl;
			board.reset(hsb);
			ijtagIO = &hsb->GetJTagIO();
			if (fast == true) {
				ijtagIO->SetCaching(true);
			}
		}
		else if (ii_board == "fsb") {
			rpct::TFsb* fsb(new rpct::TFsb(1900, "FSB", vme.get(), addr, NULL, -1, 1998));
			fsb->GetFsbVme().SetJtagEnable(true);
			board.reset(fsb);
			ijtagIO = &fsb->GetJTagIO();
			if (fast == true) {
				ijtagIO->SetCaching(true);
			}
		}


		JTAGControllerImpl jtag_controller(ijtagIO, false, SCK_frequency);


		jtag_controller.setDebugFlag(5);

		// open JTAG Chain for chain 0 on the controller
		JTAGChain jtag_chain( jtag_controller, 0 );

		// instantiate SVF Sequencer
		JTAGSVFSequencer seq;

				// load SVF file

		if (argc==2) {
			cout << "Loading File " << argv[1] << endl;
			bool result = seq.loadSVFFile(argv[1]);
			if(result == false) {
				cout<<"error durin Loading File, finisheing"<<endl;
				return 1;
			}
		}

		cout << "Commands in the chain : " << endl << endl;

		// print the sequence
		//seq.print();

		cout << endl << endl;

		// run it
		bool status = seq.execute( jtag_chain );

		if (status)
			cout << "successfully played SVF file." << endl;
		else
			cout << "error playing SVF file." << endl;
	}
	catch (exception &e) {
		cout << "Unknown Exception caught: " << e.what() << endl;
		return 1;
	}
	return 0;
}
