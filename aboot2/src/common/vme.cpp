
#include "precompiled.h"
#pragma hdrstop

#include <fstream>
#include <iostream>

//
//  Michal Pietrusinski 2000
//  Warsaw University
//


#include "rpct/aboot2/vme.h"
#include "rpct/aboot2/tb_vme.h"

using namespace std;

static TVMEInterface* VME;

int VMEOpen (void)
{
	try {
		//G_Debug.SetLevel(TDebug::VME_OPER);
		//G_Debug.SetStream(cerr);
		//G_Debug.SetStream( *new ofstream( "\\temp\\aboot.log", 
		//								  ios::app|ios::out) );
		VME = new TVMEWinBit3;
		return 0;
	}
	catch(TException& e) {
		cerr << e.what();
		return -1;
	};
}

int VMEClose(void)
{
	try {
		delete VME;
		return 0;
	}
	catch(TException& e) {
		cerr << e.what();
		return -1;
	};
}

int VMEWrite (uint32_t address, char * data, size_t count)
{
	try {
		VME->Write(address, data, count);
		return 0;
	}
	catch(TException& e) {
		cerr << e.what();
		return -1;
	};
}

int VME32Write (uint32_t address, uint32_t value)
{
	try {
		VME->Write32(address, value);
		return 0;
	}
	catch(TException& e) {
		cerr << e.what();
		return -1;
	};
}

int VME16Write (uint32_t address, unsigned short int value)
{
	try {
		VME->Write16(address, value);
		return 0;
	}
	catch(TException& e) {
		cerr << e.what();
		return -1;
	};
}

int VMERead (uint32_t address, char * data, size_t count)
{
	try {
		VME->Read(address, data, count);
		return 0;
	}
	catch(TException& e) {
		cerr << e.what();
		return -1;
	};
}

int VME32Read (uint32_t address, uint32_t * value)
{
	try {
		*value = VME->Read32(address);
		return 0;
	}
	catch(TException& e) {
		cerr << e.what();
		return -1;
	};
}

int VME16Read (uint32_t address, unsigned short int * value)
{
	try {
		*value = VME->Read16(address);
		return 0;
	}
	catch(TException& e) {
		cerr << e.what();
		return -1;
	};
}
