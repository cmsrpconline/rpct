#include "rpct/aboot2/TABootDataSource.h"
#include <algorithm>
#include "tb_std.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)

using namespace std;

TABootDataSource::TABootDataSource(TABootFiles abootFiles, bool pack, bool filesOwner) :
    ABootFiles(abootFiles), Pack(pack), FilesOwner(filesOwner) {
    // check input parameters
    if (Pack) {
        int sum = 0;
        for (TABootFiles::iterator iFile = ABootFiles.begin(); iFile!= ABootFiles.end(); ++iFile)
            sum += (*iFile)->GetBitCount();
        DataWidth = sum;
        if (sum > 32) {
            DeleteABootFiles();
            throw TException("Input files make data more than 32 bits wide.");
        }
    } else {
        DataWidth = ABootFiles.size() * 8;
        if (ABootFiles.size() > 4) {
            DeleteABootFiles();
            throw TException("More than 4 files without bit packing makes more then 32 bits.");
        }
    }
}

void TABootDataSource::DeleteABootFiles() {
    if (FilesOwner)
        for_each(ABootFiles.begin(), ABootFiles.end(), rpct::FDelete<IABootFile*>());
    }

TABootDataSource::~TABootDataSource() {
    DeleteABootFiles();
}

bool TABootDataSource::More() {
    for (TABootFiles::iterator iFile = ABootFiles.begin(); iFile!= ABootFiles.end(); ++iFile) {
        if ((*iFile)->HasNext())
            return true;
    }

    return false;
}

uint32_t TABootDataSource::ReadData() {
    uint32_t data = 0;
    int bitPos = 0;

    if (Pack) {
        for (TABootFiles::iterator iFile = ABootFiles.begin(); iFile!= ABootFiles.end(); ++iFile) {
            data |= (*iFile)->Next() << bitPos;
            bitPos += (*iFile)->GetBitCount();
        }
    } else {
        for (TABootFiles::iterator iFile = ABootFiles.begin(); iFile!= ABootFiles.end(); ++iFile) {
            data |= (*iFile)->Next() << bitPos;
            bitPos += 8;
        }
    }

    return data;
}
