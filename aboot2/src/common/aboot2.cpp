/* K. Pozniak, 1999 - Altera's loader by VME_I2C */
#ifdef __BORLANDC__
#include "precompiled.h"
#pragma hdrstop
#endif

//#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory>
#include <ctime>
#include <stdint.h>


#include "rpct/aboot2/tb.h"
//#include "uni.h"
//#include "rfgun.h"
//#include "trg.h"
#include "rpct/devices/TriggerBoard.h"
//#include "TAboot.h"
#include "tb_std.h"   
//#include "TIITrgABootContr.h"
//#include "TRBFDataSource.h"
#include "rpct/aboot2/TABootDataSource.h"
#include "rpct/aboot2/TABootFileRBF.h"
#include "rpct/aboot2/TABootFileEmpty.h"
#include "rpct/aboot2/TVMEABootContr2.h"
#include "hal/CAENLinuxBusAdapter.hh"


using namespace std;
using namespace rpct;


#define BUFLEN 2048
#define REPEAT 1000
#define CNT_DEF 50

static uint32_t buffer[BUFLEN];

/*
class TBoard : public IBoard {
private:
TDevices Devices;
int ID;
std::string Description;
public:
virtual const TDevices& GetDevices()
{ 
return Devices; 
}

virtual ICrate* GetCrate()
{
return NULL;
}
                  
virtual int GetID()
{
return ID;
}

virtual const std::string& GetDescription()
{
return Description;
}

virtual std::string GetType()
{
return "uni";
}
};*/

int error_message(TB_MESSAGE m)
{
    if(m==NO)
        fprintf(stderr,"incorrect level\n");
    else{
        tb_errno=m;
        tb_perror(NULL);
    }
    return(1); /* error exit code */
}

int info_exit(char *name)
{
    fprintf(stderr,"use: %s <base> [-D]<file> [-d] [-t] [-s]\n"
            "<base> - 4-bit base address (hex) (A23-A20)\n"
            "<file> - name of boot file\n"
            "  [-D] - set default directory\n"
            "  [-d] - dissable messages\n"
            "  [-t] - enable testing\n"
            "  [-s] - slow loading\n"
            "  [-i <uni|trg|trgc|rf>] - indirect (using controller on the trigger board)\n"
            "  [-n <8|4|2|1>] - specify valid bits count: 8, 4, 2 or 1. Default value is 8.\n"
            "  [-p] - if working with multiple input files, the data will be aligned according to the valid bit count setting (-n)\n"
            "  [-l] - turn on logging \n"
            "  [-v <CAEN|Bit3|VMESimul>] - vme interface  \n"
            "  [-u unit] - vme unit   \n"
            "  [-f] - skip checking hardware flags - for testing purposes only \n",
            name);
    return(1);
}

int main (int argc, char *argv[])
{
    //TB_MESSAGE m;
    char path[200];
    typedef std::list<std::string> TFileNames;
    TFileNames fileNames;

    int value, number, displ_flag=1, test_flag=0, slow_flag=0, indirect_flag=0;
    std::string indirect_board;
    std::string vmeInterface;
    std::string vmeUnit;
    uint32_t base;
    int maxbuf=BUFLEN;
    std::auto_ptr<IABootContr> contr;
    IABootDataSource* datasource = NULL;
    TVMEInterface* vme = NULL;
    int bits_count = 8;
    bool pack = false;
    TABootDataSource::TABootFiles abootFiles;
    bool log = false;
    bool skip_checking = false;


    if(argc<3)
        return(info_exit(argv[0]));

    int start = clock();
    
    /*if(argv[2][0]=='-'){
      if(argv[2][1]!='D')
        return(info_exit(argv[0]));
      if((m=envpath(path, TB_ETC_DIR, argv[2]+2))!=YES)
        goto finish;
    }
    else*/
    strcpy(path,argv[2]);

    fileNames.push_back(path);

    number=2;

    while ((argc > (number+1)) && (argv[number+1][0] != '-')) {
        fileNames.push_back(argv[number+1]);
        number++;
    }

    /*if ((argc > 3) && (argv[3][0] != '-')) {
      fileNames.push_back(argv[3]);
      number = 3;

      if ((argc > 4) && (argv[4][0] != '-')) {
        fileNames.push_back(argv[4]);
        number = 4;

        if ((argc > 5) && (argv[5][0] != '-')) {
          path3 = argv[5];
          number = 5;
        }
      }
    }; */

    while((++number)<argc){
        if(argv[number][0]!='-')
            return(info_exit(argv[0]));
        switch(argv[number][1]){
        case 'd':
            displ_flag=0;
            break;
        case 't':
            test_flag=1;
            break;
        case 's':
            slow_flag=1;
            maxbuf=1;
            break;
        case 'i':
            number++;
            if (number >= argc)
                return(info_exit(argv[0]));
            indirect_flag=1;
            indirect_board = argv[number];
            break;
        case 'v':
            number++;
            if (number >= argc)
                return(info_exit(argv[0]));
            vmeInterface = argv[number];
            break;
        case 'u':
            number++;
            if (number >= argc)
                return(info_exit(argv[0]));
            vmeUnit = argv[number];
            break;
        case 'n':
            number++;
            if (number >= argc)
                return(info_exit(argv[0]));
            bits_count = atoi(argv[number]);
            if (bits_count == 0)
                throw TException("Invalid value for -n option.");
            break;
        case 'p':
            pack = true;
            break;
        case 'l':
            log = true;
            break;
        case 'f':
            skip_checking=true;
            break;
        default:
            return(info_exit(argv[0]));
        }
    }

    sscanf(argv[1],"%x",&value);
    base=(uint32_t)value<<20;

    auto_ptr<ofstream> logfile;
    if (log) {
        logfile.reset(new ofstream("aboot2.log"));
        (*logfile) << hex;
        G_Debug.SetStream(*logfile);
        G_Debug.SetLevel(TDebug::VME_OPER);
        G_Log.out(*logfile);
        cout << hex;
    }

    try {
        for (TFileNames::iterator iName = fileNames.begin(); iName != fileNames.end(); ++iName) {
            if ((*iName) == "1") {
                abootFiles.push_back(new TABootFileEmpty(bits_count));
            }
            else if ((iName->find(".rbf") != string::npos)
                     || (iName->find(".RBF") != string::npos)) {
                abootFiles.push_back(new TABootFileRBF(*iName, bits_count));
            }
            else
                throw TException("Unsupported boot file type");
        }
        datasource = new TABootDataSource(abootFiles, pack);

        /*if ((strstr(path0, ".ttf") != NULL)
            || (strstr(path0, ".TTF") != NULL))
          datasource = new TTTFDataSource(bits_count, path0, path1, path2, path3, pack);
        else if ((strstr(path0, ".rbf") != NULL)
            || (strstr(path0, ".RBF") != NULL))
          datasource = new TRBFDataSource(bits_count, path0, path1, path2, path3, pack);
        else
          throw TException("Unsupported boot file type");
          */
    }
    catch(TException& e) {
        cerr << e.what() << endl;
        return 1;
    }


    try {
        if (vmeInterface.empty()) {
            cout << "Using default VME interface" << endl;
            vme = TVMEFactory().Create();
        }
        else {
            if (vmeUnit.empty()) {
                cout << "Using '" << vmeInterface << "' VME interface" << endl;
                vme = TVMEFactory().Create(vmeInterface);
            }
            else {
                cout << "Using '" << vmeInterface << "' VME interface, unit " << vmeUnit << endl;
                vme = TVMEFactory().Create(vmeInterface, vmeUnit);
            }
        }

        /*vme = new TVMEHAL(new HAL::CAENLinuxBusAdapter(HAL::CAENLinuxBusAdapter::V2718, 1), true);*/
        /*if (1) {
          TVMEEpp* vmeepp = dynamic_cast<TVMEEpp*>(vme);
          if (vmeepp != NULL) {
            vmeepp->SetAutoOptimize(true);
          }
        }*/

        if (indirect_flag) {

            /*if (indirect_board == "uni") {
              //TUni* uni = new TUni(vme, 0x8, NULL);  
              TUni* uni = new TUni(1, "uni", vme, 0x8, NULL);
              contr.reset(&uni->GetUniControl().GetABootContr());
            }*/
            /*else if (indirect_board == "trg") {
              TIITrgABootContr* abootContr = new TIITrgABootContr(base >> 20, *new TBoard);
              abootContr->getMemoryMap().SetHardwareIfc(
                      new TIIVMEAccess(
                            abootContr->GetVMEBoardBaseAddr(),
                            abootContr->getBaseAddress() << abootContr->GetVMEBaseAddrShift(),
                            vme,
                            3 << 18),
                      true);
              contr.reset(abootContr);
            } 
            else if (indirect_board == "rf") {
              TRfGun* rfgun = new TRfGun(vme, base >> 20, NULL);
              contr.reset(&rfgun->GetRfGunVme().GetABootContr());
            }    
            else if (indirect_board == "trg") {
                cout << 1 << endl;
              TTrg* trg = new TTrg(1, "trg", vme, base >> 20, NULL);
                cout << 2 << endl;
              trg->GetTrgVme().writeBits(TTrgVme::BITS_STATUS_ALT_ENA, 1);
                cout << 3 << endl;
              contr.reset(&trg->GetTrgVme().GetABootContr());
                cout << 4 << endl;
            }    
            else*/ if (indirect_board == "tb3") {
                TriggerBoard* tb = new TriggerBoard(1, "tb3", vme, base >> 20, NULL, -1);
                tb->GetVme().writeBits(TriggerBoardVme::BITS_STATUS_ALT_ENA, 1);
                contr.reset(&tb->GetVme().GetABootContr());
                try {
                    tb->GetVme().checkName();
                    tb->GetVme().checkVersion();
                    tb->GetVme().checkChecksum();
                }
                catch(TException& e) {
                    cerr << "!!!! Error during version checking: " << e.what() << endl;
                }
            }
            /*else if (indirect_board == "trgc") {
              TTrg* trg = new TTrg(1, "trg", vme, base >> 20, NULL);
              contr.reset(&trg->GetTrgControl().GetABootContr());
            } */
            else if (indirect_board == "tb3c") {
                TriggerBoard* tb = new TriggerBoard(1, "tb3", vme, base >> 20, NULL, -1);
                contr.reset(&tb->GetControl().GetABootContr());
                try {
                    tb->GetControl().checkName();
                    tb->GetControl().checkVersion();
                    tb->GetControl().checkChecksum();
                }
                catch(TException& e) {
                    cerr << "!!!! Error during version checking: " << e.what() << endl;
                }
            }

            else
                throw TException("Unknown indirect board type '" + indirect_board + "'.");


        }
        else
            contr.reset(new TVMEABootContr2(base, *vme));

        
        
        if(displ_flag){
            fprintf(stderr,"base address: %ix\n", base);
            fprintf(stderr,"status of 'display' flag: %d\n", displ_flag);
            fprintf(stderr,"status of 'test' flag: %d\n", test_flag);
            fprintf(stderr,"status of 'slow' flag: %d\n", slow_flag);
        }

        if (!skip_checking) {

        	if(displ_flag)
        		fprintf(stderr,"setting 'nCONFIG' signal to LOW level: ");
        	contr->TSetNConfig(0, 1);
        	if(displ_flag)
        		fprintf(stderr,"correct level\n");

        	if(displ_flag)
        		fprintf(stderr,"checking LOW level of 'nSTATUS' signal: ");
        	contr->TestNStatus(0, REPEAT);
        	if(displ_flag)
        		fprintf(stderr,"correct level\n");

        	if(displ_flag)
        		fprintf(stderr,"setting 'nCONFIG' signal to HIGH level: ");
        	contr->TSetNConfig(1, 1);
        	if(displ_flag)
        		fprintf(stderr,"correct level\n");

        	if(displ_flag)
        		fprintf(stderr,"checking HIGH level of 'nSTATUS' signal: ");
        	contr->TestNStatus(1, REPEAT);
        	if(displ_flag)
        		fprintf(stderr,"correct level\n");

        	if(displ_flag)
        		fprintf(stderr,"checking LOW level of 'CONF_DONE' signal: ");
        	contr->TestConfDone(0, 1);
        	if(displ_flag)        
        		fprintf(stderr,"correct level\n");
        }

        if (displ_flag)
            fprintf(stderr,"data sending ... ");

        do {
            for (number=0; number<maxbuf; ) {
                if (datasource->More()) {
                    buffer[number] = datasource->ReadData();
                    //cout << (int)buffer[number] << '\n';
                    number++;
                }
                else
                    break;
            }

            if (number>0) {
                if(slow_flag)
                    contr->SendData(buffer[0]);
                else {
                    /*TVMEEpp* vmeepp = dynamic_cast<TVMEEpp*>(vme);
                    if (vmeepp != NULL) {
                        int dataWidth = datasource->GetDataWidth();
                        int dataSize = (dataWidth > 24) ? TVMEEpp::DATA_SIZE_32 :
                                       (dataWidth > 16) ? TVMEEpp::DATA_SIZE_24 :
                                       (dataWidth > 8)  ? TVMEEpp::DATA_SIZE_16 :
                                       TVMEEpp::DATA_SIZE_8;
                        int origDataSize = vmeepp->GetEppModeDataSize();
                        if (dataSize != origDataSize) {
                            G_Log.out() << "***** wlaczam tryb " << dataSize << "-bitowy *****\n";
                            vmeepp->SetEppModeDataSize(dataSize);
                        }
                        contr->SendData(buffer, number);
                        if (dataSize != origDataSize) {
                            G_Log.out() << "***** wylaczam " << dataSize << "-bitowy *****\n";
                            vmeepp->SetEppModeDataSize(origDataSize);
                        }
                    }
                    else {*/
                        contr->SendData(buffer, number);
                    //}
                }
            }

            if (test_flag) {
                try {
                    contr->TestNStatus(1, 1);
                }
                catch(TException& e) {
                    fprintf(stderr,"checking HIGH level of 'nSTATUS' signal: ");
                    throw;
                }
            }

        } while (number == maxbuf);

        if(displ_flag)
            fprintf(stderr,"done\n");


        if (!skip_checking) {

        	if(displ_flag)
        		fprintf(stderr,"checking HIGH level of 'nSTATUS' signal: ");
        	contr->TestNStatus(1, 1);
        	if(displ_flag)
        		fprintf(stderr,"correct level\n");

        	if(displ_flag)
        		fprintf(stderr,"checking HIGH level of 'CONF_DONE' signal: ");

        	try {
        		contr->TestConfDone(1, REPEAT);
        		if(displ_flag)
        			fprintf(stderr,"correct level\n");
        	}
        	catch(TException& e) {
        		if(displ_flag)
        			fprintf(stderr,"sets LOW - adding some dummy clocks\n");

        		for(number=0;number<CNT_DEF;number++)
        			contr->SendData(255u);

        		if(displ_flag)
        			fprintf(stderr,"checking HIGH level of 'CONF_DONE' signal: ");
        		contr->TestConfDone(1, REPEAT);
        		if(displ_flag)
        			fprintf(stderr,"correct level\n");
        	};

        }

    }
    catch(TException& e) {
        fprintf(stderr, e.what());

        delete datasource;
        delete vme;

        return 1;
    }
    catch(exception& e) {
        fprintf(stderr, e.what());

        delete datasource;
        delete vme;

        return 1;
    };

    delete datasource;
    delete vme;

    cout << "Total time " << std::dec << ((clock() - start) / (float)CLOCKS_PER_SEC) << " sec" << endl;
    
    return(0);
}
