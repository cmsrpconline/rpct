/*
 * jbootsvf.cpp
 *
 *  Created on: July 4, 2012
 *      Author: Karol Bunkowski
 */


#include "rpct/devices/hsb.h"
#include "rpct/devices/fsb.h"

#include <iostream>
#include <exception>

#include <stdint.h>

#include "log4cplus/consoleappender.h"

#include <boost/timer.hpp>
#include <log4cplus/configurator.h>
#include <boost/program_options.hpp>

using namespace std;
using namespace rpct;
using namespace log4cplus;
namespace po = boost::program_options;

int main(int argc, char **argv) {
	SharedAppenderPtr myAppender(new ConsoleAppender());
	myAppender->setName("myAppenderName");
	std::auto_ptr<Layout> myLayout = std::auto_ptr<Layout>(new log4cplus::TTCCLayout());
	myAppender->setLayout( myLayout );
	//Logger::getInstance("RpctSystem").addAppender(myAppender);
	Logger::Logger::getRoot().setLogLevel(INFO_LOG_LEVEL);
	//Logger::getInstance("TIIDevice").addAppender(myAppender);
	//Logger::getInstance("JTAGControllerImpl").addAppender(myAppender);
	Logger::getInstance("TIIJtagIO").addAppender(myAppender);

	//Logger::getInstance("JTAGControllerImpl").setLogLevel(DEBUG_LOG_LEVEL);
	Logger::getInstance("TIIJtagIO").setLogLevel(INFO_LOG_LEVEL);

	try {
		string ii_board = "fsb";
		string vmeInterface = "CAEN";
		std::string vmeUnit = "2";
		unsigned short addr = 0xC;
		string fileName = "";
		bool fast = true;


		po::options_description desc("Allowed options");
		desc.add_options()
		    ("help", "produce help message")
		    ("b", po::value<string>(), "board type: hsb or fsb")
		    ("p", po::value<string>(), "CAEN PCI number 0, 1, or 2")
		    ("e", po::value<string>(), "board address")
		    ("f", po::value<string>(), "file name")
		;

		po::variables_map vm;
		po::store(po::parse_command_line(argc, argv, desc), vm);
		po::notify(vm);

		if (vm.count("help")) {
		    cout << desc << "\n";
		    return 1;
		}

		if (vm.count("b")) {
			ii_board = vm["b"].as<string>();
		    cout << "board "<<ii_board<<endl;
		    if(ii_board != "hsb" && ii_board != "fsb") {
		    	cout<<"wrong board type"<<endl;
		    	return 1;
		    }
		} else {
		    cout << "board type not given"<<endl;
		    return 1;
		}

		if (vm.count("p")) {
			vmeUnit = vm["p"].as<string>();
		    cout << "vmeUnit "<<vmeUnit<<endl;
		    /*if(vmeUnit < 0 || vmeUnit > 2) {
		    	cout<<"wrong vmeUnit"<<endl;
		    	return 1;
		    }*/
		} else {
		    cout << "vmeUnit not given"<<endl;
		    return 1;
		}

		if (vm.count("e")) {
			stringstream interpreter;
			interpreter << std::hex << vm["e"].as<string>();
			interpreter >> addr;
		    cout << "address "<<addr<<endl;
		} else {
		    cout << "address not given"<<endl;
		    return 1;
		}

		if (vm.count("f")) {
			fileName = vm["f"].as<string>();
		    cout << "fileName "<<fileName<<endl;
		} else {
		    cout << "fileName type not given"<<endl;
		    return 1;
		}


		std::auto_ptr<TVMEInterface> vme;
		if (vmeInterface.empty()) {
			cout << "Using default VME interface" << endl;
			vme.reset(TVMEFactory().Create());
		}
		else {
			if (vmeUnit.empty()) {
				cout << "Using '" << vmeInterface << "' VME interface" << endl;
				vme.reset(TVMEFactory().Create(vmeInterface));
			}
			else {
				cout << "Using '" << vmeInterface << "' VME interface, unit " << vmeUnit << endl;
				vme.reset(TVMEFactory().Create(vmeInterface, vmeUnit));
			}
		}

		std::auto_ptr<rpct::IBoard> board;
		IJtagIO* ijtagIO = 0;

		if (ii_board == "hsb") {
			rpct::THsb* hsb(new rpct::THsb(1900, "HSB", vme.get(), addr, NULL, -1, 1998));
			hsb->GetHsbVme().SetJtagEnable(true);
			hsb->GetHsbVme().checkName();
			cout<<"BITS_STATUS "<<hex<<hsb->GetHsbVme().readWord(THsbVme::WORD_CHECKSUM, 0)<<endl;
			board.reset(hsb);
			ijtagIO = &hsb->GetJTagIO();
			if (fast == true) {
				ijtagIO->SetCaching(true);
			}
		}
		else if (ii_board == "fsb") {
			rpct::TFsb* fsb(new rpct::TFsb(1900, "FSB", vme.get(), addr, NULL, -1, 1998));
			fsb->GetFsbVme().SetJtagEnable(true);
			board.reset(fsb);
			ijtagIO = &fsb->GetJTagIO();
			if (fast == true) {
				ijtagIO->SetCaching(true);
			}
		}


		ifstream dumpfile;

		cout << "Loading File " << fileName << endl;
		dumpfile.open(fileName.c_str(), ifstream::in);
		if(dumpfile.good() == false) {
			cout<<"file is not good"<<endl;
			return 1;
		}

		bool tms = false;
		bool tdo = false;
		bool read_tdi = false;
		bool tdi = false;
		bool tdi_fromHard = false;

		unsigned long long line = 0;
		cout<<"lines read "<<endl;
		while(dumpfile.good()) {
			dumpfile>>tms>>tdo>>read_tdi>>tdi;
			line++;
			tdi_fromHard = ijtagIO->JtagIO(tms, tdo, read_tdi);
			if(tdi_fromHard != tdi) {
				cout<<"Error: tdi_fromHard "<<tdi_fromHard<<" tdi "<<tdi<<endl;
				return 1;
			}
			if(line%1000000 == 0){
				cout<<dec<<line<<"\r"<<flush;
				//cout<<tms<<" "<<tdo<<" "<<read_tdi<<" "<<tdi<<endl;
			}
		}
		cout<<endl<<"done"<<endl;
	}
	catch (exception &e) {
		cout << "Unknown Exception caught: " << e.what() << endl;
		return 1;
	}
	return 0;
}
