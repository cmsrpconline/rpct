//---------------------------------------------------------------------------

#include "precompiled.h"
#pragma hdrstop

#include "rpct/aboot2/TRBFDataSource.h"
#include "tb_std.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)




TRBFDataSource::TRBFDataSource(int bit_count,
                 const char* file0,
                 const char* file1,
                 const char* file2,
                 const char* file3,
                 bool pack)
 : BitCount(bit_count), CurrBitPos(0),
   FIn0(NULL), FIn1(NULL), FIn2(NULL), FIn3(NULL), FMore(true), Pack(pack)
{
  if((FIn0=fopen(file0,"rb"))==NULL)
    throw TException( std::string("can't open file: ") + file0 );

  if (file1 != NULL)
    if((FIn1=fopen(file1,"rb"))==NULL)
      throw TException( std::string("can't open file: ") + file1 );

  if (file2 != NULL)
    if((FIn2=fopen(file2,"rb"))==NULL)
      throw TException( std::string("can't open file: ") + file2 );

  if (file3 != NULL)
    if((FIn3=fopen(file3,"rb"))==NULL)
      throw TException( std::string("can't open file: ") + file3 );

  DataMask = (1 << BitCount) - 1;

  if (FIn1 != NULL)
    DataMask |= (DataMask << 8);
  if (FIn2 != NULL)
    DataMask |= (DataMask << 8); 
  if (FIn3 != NULL)
    DataMask |= (DataMask << 8);
    

  GetData();
}


TRBFDataSource::~TRBFDataSource()
{
  if (FIn0 != NULL)
    fclose(FIn0);
  if (FIn1 != NULL)
    fclose(FIn1);
  if (FIn2 != NULL)
    fclose(FIn2);
  if (FIn3 != NULL)
    fclose(FIn3);
}


bool TRBFDataSource::More()
{
  return FMore;
}

void TRBFDataSource::GetData()
{
  unsigned char tmp_data;

  FMore = fread(&tmp_data, 1, 1, FIn0) == 1;
  Data = tmp_data;

  /////////////////////////////////
  // FIn1
  if (FIn1 == NULL)
    return;

  FMore = fread(&tmp_data, 1, 1, FIn1) == 1;
  Data = (Data & 0xFF) | (tmp_data << 8);

  /////////////////////////////////
  // FIn2
  if (FIn2 == NULL)
    return;

  FMore = fread(&tmp_data, 1, 1, FIn2) == 1;
  Data = (Data & 0xFFFF) | (tmp_data << 16);
                                                    
  /////////////////////////////////
  // FIn3
  if (FIn3 == NULL)
    return;

  FMore = fread(&tmp_data, 1, 1, FIn3) == 1;
  Data = (Data & 0xFFFFFF) | (tmp_data << 24);
}


uint32_t TRBFDataSource::PackData(uint32_t data)
{
  if (Pack && (FIn1 != NULL) && (BitCount != 8)) {
    uint32_t byteMask = DataMask & 0xff;
    uint32_t newdata = data & DataMask;
    newdata |= ((data >> 8) & DataMask) << BitCount;
    newdata |= ((data >> 16) & DataMask) << (BitCount*2);  
    newdata |= ((data >> 24) & DataMask) << (BitCount*3);
    return newdata;
  }
  else
    return data;
}


uint32_t TRBFDataSource::ReadData()
{
  if (BitCount == 8) {
    uint32_t data = Data;
    GetData();
    return data;
  }
  else {
    if (CurrBitPos == (8 - BitCount)) {
      uint32_t data = (Data >> CurrBitPos) & DataMask;
      GetData();
      CurrBitPos = 0;
      return PackData(data);
    }
    else {
      uint32_t data = (Data >> CurrBitPos) & DataMask;
      CurrBitPos += BitCount;
      return PackData(data);
    }
  }
}
