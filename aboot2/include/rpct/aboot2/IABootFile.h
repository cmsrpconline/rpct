#ifndef IABOOTFILE_H
#define IABOOTFILE_H

#include <string>
#include <stdint.h>


class IABootFile {
public:
  virtual ~IABootFile() {}

  virtual std::string GetFileName() = 0;
  virtual uint32_t Next() = 0;
  virtual bool HasNext() = 0;
  virtual int GetBitCount() = 0;
};

#endif
