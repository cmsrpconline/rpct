#ifndef TABOOTFILEEMPTY_H
#define TABOOTFILEEMPTY_H

#include "rpct/aboot2/IABootFile.h"

class TABootFileEmpty : public IABootFile {
private:
  int BitCount;
  uint32_t DataMask;
public:
  TABootFileEmpty(int bitCount) : BitCount(bitCount)
    {
      DataMask = (1 << BitCount) - 1;
    }
                  
  virtual std::string GetFileName() {
    return "--EMPTY--";
  }
  
  virtual uint32_t Next() {
    return DataMask;
  }
  
  virtual bool HasNext() {
    return false;
  }

  virtual int GetBitCount() {
    return BitCount;
  }
};


#endif
