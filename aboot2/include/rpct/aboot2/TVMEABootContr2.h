//---------------------------------------------------------------------------

#ifndef TVMEABootContr2H
#define TVMEABootContr2H
//---------------------------------------------------------------------------

#include "tb_vme.h"
#include "rpct/aboot2/IABootContr.h"

class TVMEABootContr2: public IABootContr {
private:
  uint32_t Base;
  TVMEInterface& VME;
public:
  TVMEABootContr2(uint32_t base, TVMEInterface& vme)
    : Base(base), VME(vme)
    {         
    }

  virtual ~TVMEABootContr2()
    {
    }

  virtual void Reset();

  virtual int GetStatusFlag(uint32_t mask);

  virtual void SetStatusFlag(int level, uint32_t mask);

  virtual void TestStatusFlag(int level, int count, uint32_t mask);

  virtual void TSetStatusFlag(int level, int count, uint32_t mask);

  /* Altera's boot utility */

  virtual int GetNConfig()
    {
      return GetStatusFlag(0x01);
    }

  virtual void SetNConfig(int level)
    {
      SetStatusFlag(level, 0x01);
    }

  virtual void TestNConfig(int level, int count)
    {
      TestStatusFlag(level, count, 0x01);
    }

  virtual void TSetNConfig(int level, int count)
    {
      TSetStatusFlag(level, count, 0x01);
    }

  virtual int GetNStatus()
    {
      return GetStatusFlag(0x20);
    }

  virtual void TestNStatus(int level, int count)
    {
      TestStatusFlag(level, count, 0x20);
    }

  virtual int GetConfDone()
    {
      return GetStatusFlag(0x40);
    }


  virtual void TestConfDone(int level, int count)
    {
      TestStatusFlag(level, count, 0x40);
    }

  virtual void SendData(uint32_t data);
  virtual void SendData(uint32_t *buf, int number);
};


#endif
