#ifndef IABOOTCONTR_H
#define IABOOTCONTR_H


class IABootContr {
public:
  virtual ~IABootContr() {}
  virtual void Reset() = 0;

  virtual int GetNConfig() = 0;
  virtual void SetNConfig(int level) = 0;
  virtual void TestNConfig(int level, int count) = 0;
  virtual void TSetNConfig(int level, int count) = 0;

  virtual int GetNStatus() = 0;
  virtual void TestNStatus(int level, int count) = 0;

  virtual int GetConfDone() = 0;
  virtual void TestConfDone(int level, int count) = 0;

  virtual void SendData(uint32_t data) = 0;
  virtual void SendData(uint32_t *buf, int number) = 0;
};


#endif

