//---------------------------------------------------------------------------

//
//  Michal Pietrusinski 2001
//  Warsaw University
//
#ifndef TIITrgABootContrH
#define TIITrgABootContrH
//---------------------------------------------------------------------------

#include "trg.h"
#include "rpct/aboot2/TAboot.h"



class TIITrgABootContr: public TTrgControl, public IABootContr {
public:
  TIITrgABootContr(int base, IBoard& board): TTrgControl(base, board)  {}   
  virtual ~TIITrgABootContr() {}

  virtual void Reset()
    {
      writeWord(WORD_RESET, 0,  0x13ul);
    }

  virtual int GetStatusFlag(TID bit_id)
    {
      return readBits(bit_id);
    }

  virtual void SetStatusFlag(int level, TID bit_id)
    {
      unsigned long value;

      if(level!=0 && level!=1)
        throw TException("SetStatusFlag: illegal prameter");

      value = readVector(VECT_STATUS);

      setBitsInVector(bit_id, level, value);

      //value = (level==0?0:mask) | (value & (~mask));

      writeVector(VECT_STATUS, value);

      // if(VME32Write(base+0ul, value)<0)
      //             RETURN(tb_errno=VME_ERROR);
      // return(YES);
    }

  virtual void TestStatusFlag(int level, int count, TID bit_id)
    {
      //static int i=0;
      //if (i++ < 7) return;
      
      if (level!=0 && level!=1)
        throw TException("TestStatusFlag: illegal prameter");
      if (count<1)
        throw TException("TestStatusFlag: illegal prameter");

      for (int k=0; k<count; k++)
        if (level == GetStatusFlag(bit_id))
          return;

      throw TException("incorrect level");
    }

  virtual void TSetStatusFlag(int level, int count, TID bit_id)
    {
      SetStatusFlag(level, bit_id);
      TestStatusFlag(level, count, bit_id);
    }

  /* Altera's boot utility */

  virtual int GetNConfig()
    {
      return GetStatusFlag(BITS_STATUS_NCONF);
    }

  virtual void SetNConfig(int level)
    {
      SetStatusFlag(level, BITS_STATUS_NCONF);
    }

  virtual void TestNConfig(int level, int count)
    {
      TestStatusFlag(level, count, BITS_STATUS_NCONF);
    }

  virtual void TSetNConfig(int level, int count)
    {
      TSetStatusFlag(level, count, BITS_STATUS_NCONF);
    }

  virtual int GetNStatus()
    {
      return GetStatusFlag(BITS_STATUS_NSTATUS);
    }

  virtual void TestNStatus(int level, int count)
    {
      TestStatusFlag(level, count, BITS_STATUS_NSTATUS);
    }

  virtual int GetConfDone()
    {
      return GetStatusFlag(BITS_STATUS_CONFDONE);
    }


  virtual void TestConfDone(int level, int count)
    {
      TestStatusFlag(level, count, BITS_STATUS_CONFDONE);
    }

  virtual void SendData(uint32_t data)
    {
      writeArea(AREA_BOOT, &data, 0, 1);
    }


  virtual void SendData(uint32_t *buf, int number)
    {
      writeArea(AREA_BOOT, buf, 0, number);
    }
};






#endif
