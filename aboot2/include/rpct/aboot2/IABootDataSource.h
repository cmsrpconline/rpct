#ifndef IABOOTDATASOURCE_H
#define IABOOTDATASOURCE_H
#include <stdint.h>

class IABootDataSource {
public:
  virtual bool More() = 0;
  virtual uint32_t ReadData() = 0;
  virtual int GetDataWidth() = 0;
  virtual ~IABootDataSource() {};
};


#endif
