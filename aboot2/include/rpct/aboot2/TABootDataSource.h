//---------------------------------------------------------------------------
#ifndef TABootDataSourceH
#define TABootDataSourceH
//---------------------------------------------------------------------------

#include "rpct/aboot2/IABootDataSource.h"
#include "rpct/aboot2/IABootFile.h"
#include <string>
#include <list>


class TABootDataSource: public IABootDataSource {
public:
  typedef std::list<IABootFile*> TABootFiles;
protected:
  TABootFiles ABootFiles;
  bool Pack;
  bool FilesOwner;
  void DeleteABootFiles();
  int DataWidth;
public:
  TABootDataSource(TABootFiles abootFiles,
                   bool pack = false,
                   bool filesOwner = true);
  virtual ~TABootDataSource();

  virtual bool More();
  virtual uint32_t ReadData();
  virtual int GetDataWidth()
    {
      return DataWidth;
    }
};


#endif
