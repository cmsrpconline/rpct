#if !defined WZVME_H
#define WZVME_H
#define VME_ERR_REOPEN (-1)
#define VME_ERR_CANT_OPEN (-2)
#define VME_ERR_HARDWARE (-3)
#include <stdint.h>

//
//  Michal Pietrusinski 2000
//  Warsaw University
//

int bit3_perror(int chan);
int VMEOpen (void);
int VMEClose(void);
int VMEWrite (uint32_t address, char * data, size_t count);
int VME32Write (uint32_t address, uint32_t value);
int VME16Write (uint32_t address, unsigned short int value);
int VMERead (uint32_t  address, char * data, size_t count);
int VME32Read (uint32_t address, uint32_t * value);
int VME16Read (uint32_t address, unsigned short int * value);



#endif
