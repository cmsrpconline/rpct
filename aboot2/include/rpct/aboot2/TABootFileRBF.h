//---------------------------------------------------------------------------

#ifndef TABootFileRBFH
#define TABootFileRBFH
//---------------------------------------------------------------------------

#include "rpct/aboot2/IABootFile.h"
#include <stdlib.h>


class TABootFileRBF : public IABootFile {
private:
  std::string FileName;
  FILE* FIn;
  //int FD; // file descriptor
  uint32_t Data;
  uint32_t DataMask;
  bool FHasNext;
  int BitCount; 
  int CurrBitPos; 
  int LastBitPos; // when reading bit by bit, this is a last valid position of CurrBitPos
  unsigned char* FileBuf;
  size_t FileBufSize;
  size_t FileBufPos;
  size_t FileBufValid;
  void GetData();
public:
  TABootFileRBF(std::string fileName, int bitCount);
  virtual ~TABootFileRBF();
                  
  virtual std::string GetFileName() {
    return FileName;
  }
  
  virtual uint32_t Next();
  
  virtual bool HasNext() {
    return FHasNext;
  }

  virtual int GetBitCount() {
    return BitCount;
  }
};


#endif
