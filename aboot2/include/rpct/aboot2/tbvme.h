/************************************\
*                                    *
* TEST BENCH VME INTERFACE - LIBRARY *
*                                    *
* written by Krzysztof Pozniak, 1996 *
*                                    *
\************************************/

#if !defined TBVME_HH
#define TBVME_HH 

#include "tb.h"
#include <stdio.h>


TB_MESSAGE tbvme_reset(uint32_t base);
TB_MESSAGE tbvme_reset2(uint32_t base);

TB_MESSAGE tbvme_get_status_flag(uint32_t base, int *level,
			      uint32_t mask);
#define tbvme_get_status_flag2(b,l,m) tbvme_get_status_flag((b),(l),(m))

TB_MESSAGE tbvme_set_status_flag(uint32_t base, int level,
			      uint32_t mask);
TB_MESSAGE tbvme_set_status_flag2(uint32_t base, int level,
			       uint32_t mask);

TB_MESSAGE tbvme_test_status_flag(uint32_t base, int level, int count,
			       uint32_t mask);
#define tbvme_test_status_flag2(b,l,c,m) tbvme_test_status_flag((b),(l),(c),(m))

TB_MESSAGE tbvme_tset_status_flag(uint32_t base, int level, int count,
			       uint32_t mask);
TB_MESSAGE tbvme_tset_status_flag2(uint32_t base, int level, int count,
			        uint32_t mask);

/* Altera's boot utility */

#define aboot_get_nCONFIG(b,l) tbvme_get_status_flag((b),(l),0x01)
#define aboot_get_nCONFIG2(b,l) tbvme_get_status_flag2((b),(l),0x01)

#define aboot_set_nCONFIG(b,l) tbvme_set_status_flag((b),(l),0x01)
#define aboot_set_nCONFIG2(b,l) tbvme_set_status_flag2((b),(l),0x01)

#define aboot_test_nCONFIG(b,l,c) tbvme_test_status_flag((b),(l),(c),0x01)
#define aboot_test_nCONFIG2(b,l,c) tbvme_test_status_flag2((b),(l),(c),0x01)

#define aboot_tset_nCONFIG(b,l,c) tbvme_tset_status_flag((b),(l),(c), 0x01)
#define aboot_tset_nCONFIG2(b,l,c) tbvme_tset_status_flag2((b),(l),(c), 0x01)

#define aboot_get_nSTATUS(b,l) tbvme_get_status_flag((b),(l),0x20)
#define aboot_get_nSTATUS2(b,l) tbvme_get_status_flag2((b),(l),0x20)

#define aboot_test_nSTATUS(b,l,c) tbvme_test_status_flag((b),(l),(c),0x20)
#define aboot_test_nSTATUS2(b,l,c) tbvme_test_status_flag2((b),(l),(c),0x20)

#define aboot_get_CONF_DONE(b,l) tbvme_get_status_flag((b),(l),0x40)
#define aboot_get_CONF_DONE2(b,l) tbvme_get_status_flag2((b),(l),0x40)

#define aboot_test_CONF_DONE(b,l,c) tbvme_test_status_flag((b),(l),(c),0x40)
#define aboot_test_CONF_DONE2(b,l,c) tbvme_test_status_flag2((b),(l),(c),0x40)


TB_MESSAGE aboot_send_data(uint32_t base, uint32_t data);
#define aboot_send_data2(b,d) aboot_send_data((b),(d))

TB_MESSAGE aboot_send_buf(uint32_t base, uint32_t *buf, int number);
#define aboot_send_buf2(b,f,n) aboot_send_buf((b),(f),(n))

TB_MESSAGE aboot_read_data(FILE *fp, uint32_t *data);
#define aboot_read_data2(f,d) aboot_read_data((f),(d))

TB_MESSAGE aboot_exec(uint32_t base, int def_path_flag, char *file_name);

#endif    



