#if !defined TB_H
#define TB_H

typedef enum {
    NO = 0,
    YES = 1,
    NULL_MESSAGE=100,
    ILLEGAL_PARAMETER = (-1),
    NULL_POINTER = (-2),
    BOARD_NOT_INSTALLED = (-3),
    BOARD_IS_INSTALLED = (-4),
    VME_ERROR = (-5),
    STATE_NOT_ALLOWED = (-6),
    PARSE_ERROR = (-7),
    NO_ANSWER_FROM_RC = (-8 ),
    NO_CHANNEL_AVAIL = (-9 ),
    FILE_ERROR = (-10),
    REOPEN = (-11),
    RC_ERR = (-12 ),
    RC_STACK_OVER = (-13),
    ENV_ERROR = (-14),
    RC_NO_EVENT = (-15),
    RC_NOT_CONNECTED = (-16 )
} TB_MESSAGE;

#define TB_ETC_DIR "TB_ETC_DIR"

extern TB_MESSAGE tb_errno;
extern char *tb_logfile_name;

char *tb_perror_str(void);
void tb_perror( char* s );
TB_MESSAGE envpath(char *dst, char *envname, char *src);
TB_MESSAGE install_break(void);
TB_MESSAGE uninstall_break(void);
TB_MESSAGE test_break(void);

TB_MESSAGE logfile_message(char *message);
TB_MESSAGE logfile_fileline(char *filename, int line, char *info);
TB_MESSAGE return_message(TB_MESSAGE message, char *filename, int line, char *info);

#define RETURN(m) return(return_message((m),__FILE__,__LINE__,"RETURN"))

/*extern"C"{
int usleep(unsigned);
}*/

#endif
