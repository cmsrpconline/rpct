//---------------------------------------------------------------------------

#ifndef TTTFDataSourceH
#define TTTFDataSourceH
//---------------------------------------------------------------------------

#include "rpct/aboot2/IABootDataSource.h"


class TTTFDataSource: public IABootDataSource {
private:   
  int BitCount;
  int CurrBitPos;
  FILE* FIn0;
  FILE* FIn1;
  FILE* FIn2;
  FILE* FIn3;
  uint32_t Data;
  uint32_t DataMask;
  bool FMore;
  bool Pack;
  void GetData();      
  uint32_t PackData(uint32_t data);
public:
  TTTFDataSource(int bit_count,
                 const char* file0,
                 const char* file1 = NULL,
                 const char* file2 = NULL,
                 const char* file3 = NULL,
                 bool pack = false);
  virtual ~TTTFDataSource();

  virtual bool More();
  virtual uint32_t ReadData();
};




#endif
