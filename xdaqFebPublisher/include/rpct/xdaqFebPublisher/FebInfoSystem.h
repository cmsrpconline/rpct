/** Filip Thyssen */

#ifndef rpct_xdaqFebPublisher_FebInfoSystem_h_
#define rpct_xdaqFebPublisher_FebInfoSystem_h_

#include <map>
#include <vector>
#include <stdint.h>

#include "rpct/tools/Printable.h"
#include "rpct/tools/LogState.h"
#include "rpct/xdaqtools/Drawable.h"
#include "rpct/ii/FebChipLocationId.h"

namespace rpct {
namespace xdaqfebpublisher {

extern const unsigned int state_type_temperature_;
extern const unsigned int state_type_vth_;
extern const unsigned int state_type_vmon_;
extern const unsigned int state_type_offset_;
extern const unsigned int state_type_state_;
const unsigned int state_type_max_ = 5;
extern const char * state_types_[state_type_max_];

extern const int nwheels_[2];
extern const float wheeloffset_[2];
extern const int minwheel_[2];
extern const int maxwheel_[2];
extern const float radius_;
extern const float rspacer_;
extern const float anglespacer_;
extern const float wheelspacer_;

class ChamberInfo;
class FebChipInfo;

class FebInfoSystem
    : public rpct::xdaqtools::Drawable
{
public:
    FebInfoSystem();
    ~FebInfoSystem();

    void reset();
    void clear();

    int size() const;

    void printTable(std::ostream & os, rpct::FebChipLocationId mask, unsigned char min_state = rpct::tools::State::notset_) const;
    void printSVGCSS(std::ostream & os, int type = 0) const;
    void printSVG(std::ostream & os, int type = 0) const;

    int getSVGWidth(int depth = 0) const;
    int getSVGHeight(int depth = 0) const;

    void addFebChip(int id, int pvss_id, rpct::FebChipLocationId fclid);
    FebChipInfo const * getFebChipInfo(int id) const;
    FebChipInfo * getFebChipInfo(int id);

protected:
    std::map<rpct::FebChipLocationId, ChamberInfo * > cis_;
    std::map<int, FebChipInfo * > fcis_;
};

class ChamberInfo
{
public:
    ChamberInfo(rpct::FebChipLocationId fclid = rpct::FebChipLocationId());
    ~ChamberInfo();

    void reset();
    void clear();

    void printRows(std::ostream & os, rpct::FebChipLocationId mask, unsigned char min_state = rpct::tools::State::notset_) const;
    void printSVG(std::ostream & os, const std::string & href, int type) const;

    rpct::FebChipLocationId const & getLocation() const;

    rpct::tools::TreeLogState const & getState(unsigned int type = state_type_state_) const;
    rpct::tools::TreeLogState & getState(unsigned int type = state_type_state_);

    FebChipInfo & addFebChip(int id, int pvss_id, rpct::FebChipLocationId fclid);

protected:
    rpct::FebChipLocationId fclid_;

    std::map<rpct::FebChipLocationId, FebChipInfo * > fcis_;

    rpct::tools::TreeLogState states_[state_type_max_];
};

class FebChipInfo : public rpct::tools::Printable
{
public:
    FebChipInfo(int id, int pvss_id, rpct::FebChipLocationId fclid);
    ~FebChipInfo();

    void reset();

    void printRow(std::ostream & os, rpct::FebChipLocationId mask, unsigned char min_state = rpct::tools::State::notset_) const;
    void printParameters(rpct::tools::Parameters & parameters) const;
    void printHTML(std::ostream & os) const;

    int getId() const;
    int getPvssId() const;
    rpct::FebChipLocationId const & getLocation() const;

    rpct::tools::TreeLogState const & getState(unsigned int type = state_type_state_) const;
    rpct::tools::TreeLogState & getState(unsigned int type = state_type_state_);

    float getTemperature() const;
    uint16_t getVTH(bool db = false) const;
    uint16_t getVMON(bool db = false) const;
    int16_t getVTHOffset(bool db = false) const;
    int16_t getVMONOffset(bool db = false) const;

    void set(int type, double value);

    void setTemperature(float temp);
    void setVTH(uint16_t vth, bool db = false);
    void setVMON(uint16_t vmon, bool db = false);
    void setVTHOffset(int16_t vth_offset, bool db = false);
    void setVMONOffset(int16_t vmon_offset, bool db = false);

protected:
    static bool init_;
    static int state_categories_[state_type_max_];
    static rpct::tools::LogCenter * lc_;

    int id_;
    int pvss_id_;
    rpct::FebChipLocationId fclid_;

    float temperature_;
    uint16_t vth_, vmon_;
    int16_t vth_offset_, vmon_offset_;
    uint16_t vth_db_, vmon_db_;
    int16_t vth_offset_db_, vmon_offset_db_;

    rpct::tools::TreeLogState states_[state_type_max_];
};

} // namespace xdaqfebpublisher
} // namespace rpct

#include "rpct/xdaqFebPublisher/FebInfoSystem-inl.h"

#endif // rpct_xdaqFebPublisher_FebInfoSystem_h_
