/** Filip Thyssen */

#ifndef rpct_xdaqfebpublisher_const_h_
#define rpct_xdaqfebpublisher_const_h_

#include "rpct/ii/const.h"

namespace rpct {
namespace xdaqfebpublisher {

const int type_offset_ = rpct::publish::type::feb::temp_;
extern const char * pvss_typename_[3];

} // namespace xdaqfebpublisher
} // namespace rpct

#endif // rpct_xdaqfebpublisher_const_h_
