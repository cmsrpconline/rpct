/** Filip Thyssen */

#ifndef rpct_xdaqFebPublisher_xdaqFebPublisher_h_
#define rpct_xdaqFebPublisher_xdaqFebPublisher_h_

#include "xdaq/Application.h"

#include "xgi/Input.h"
#include "xgi/Output.h"
#include "xgi/exception/Exception.h"

#include "rpct/xdaqtools/Timer.h"

#include "rpct/tools/Action.h"
#include "rpct/tools/RWMutex.h"

#include "rpct/xdaqutils/XdaqDbServiceClient.h"

#include "rpct/xdaqFebPublisher/FebInfoSystem.h"

namespace rpct {
namespace tools {
class WebLogger;
} // namespace tools

namespace xdaqtools {

namespace psx {
class Client;
} // namespace psx

namespace publisher {
class Service;
class Item;
} // namespace publisher

} // namespace xdaqtools
} // namespace rpct

class xdaqFebPublisher : public xdaq::Application
{
public:
    XDAQ_INSTANTIATOR();

    xdaqFebPublisher(xdaq::ApplicationStub * s);
    ~xdaqFebPublisher();

    int item_action(rpct::xdaqtools::publisher::Item & item);

    void Default(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);

    void showSVG(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
    void showTable(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
    void showFebChipInfo(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);

    void reset();
    void clear();
    void load_from_db();

    void menu(std::ostream & os) const;
    void actions(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
    void reset(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
    void clear(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
    void load_from_db(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);

    int xMonitorCycle(toolbox::task::TimerEvent &);
    void monitorCycle();

protected:
    rpct::tools::WebLogger * wl_;

    rpct::xdaqfebpublisher::FebInfoSystem * fis_;
    rpct::tools::RWMutex fis_mutex_;

    rpct::xdaqtools::publisher::Service * service_;

    rpct::xdaqtools::psx::Client * psx_client_;

    rpct::tools::ObjectActionSignature<rpct::xdaqtools::publisher::Item &> * action_;

    rpct::xdaqutils::XdaqDbServiceClient * db_client_;

    // publisher/psx status monitoring
    rpct::xdaqtools::Timer * monitor_status_timer_;
    rpct::tools::RWMutex monitor_status_mutex_;
    unsigned int monitor_status_;
    std::string monitor_status_description_;
};

#endif // rpct_xdaqFebPublisher_xdaqFebPublisher_h_
