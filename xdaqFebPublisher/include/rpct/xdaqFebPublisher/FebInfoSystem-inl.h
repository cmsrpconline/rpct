/** Filip Thyssen */

#ifndef rpct_xdaqFebPublisher_FebInfoSystem_inl_h_
#define rpct_xdaqFebPublisher_FebInfoSystem_inl_h_

#include "rpct/xdaqFebPublisher/FebInfoSystem.h"

#include <sstream>

namespace rpct {
namespace xdaqfebpublisher {

inline int FebInfoSystem::size() const
{
    return fcis_.size();
}

inline int FebInfoSystem::getSVGWidth(int depth) const
{
    return (int)(nwheels_[0]*2.*radius_ + nwheels_[0]*wheelspacer_ + 2.*wheelspacer_);
}
inline int FebInfoSystem::getSVGHeight(int depth) const
{
    return (int)(6.*radius_ + 3.*wheelspacer_);
}

inline rpct::FebChipLocationId const & ChamberInfo::getLocation() const
{
    return fclid_;
}

inline rpct::tools::TreeLogState const & ChamberInfo::getState(unsigned int type) const
{
    if (type < state_type_max_)
        return states_[type];
    else
        return states_[state_type_temperature_];
}
inline rpct::tools::TreeLogState & ChamberInfo::getState(unsigned int type)
{
    if (type < state_type_max_)
        return states_[type];
    else
        return states_[state_type_temperature_];
}

inline int FebChipInfo::getId() const
{
    return id_;
}
inline int FebChipInfo::getPvssId() const
{
    return pvss_id_;
}
inline rpct::FebChipLocationId const & FebChipInfo::getLocation() const
{
    return fclid_;
}

inline rpct::tools::TreeLogState const & FebChipInfo::getState(unsigned int type) const
{
    if (type < state_type_max_)
        return states_[type];
    else
        return states_[state_type_temperature_];
}
inline rpct::tools::TreeLogState & FebChipInfo::getState(unsigned int type)
{
    if (type < state_type_max_)
        return states_[type];
    else
        return states_[state_type_temperature_];
}

inline float FebChipInfo::getTemperature() const
{
    return temperature_;
}
inline uint16_t FebChipInfo::getVTH(bool db) const
{
    if (db)
        return vth_db_;
    else
        return vth_;
}
inline uint16_t FebChipInfo::getVMON(bool db) const
{
    if (db)
        return vmon_db_;
    else
        return vmon_;
}
inline int16_t FebChipInfo::getVTHOffset(bool db) const
{
    if (db)
        return vth_offset_db_;
    else
        return vth_offset_;
}
inline int16_t FebChipInfo::getVMONOffset(bool db) const
{
    if (db)
        return vmon_offset_db_;
    else
        return vmon_offset_;
}

inline void FebChipInfo::setTemperature(float temp)
{
    temperature_ = temp;
    states_[state_type_temperature_].log("monitored");
}
inline void FebChipInfo::setVTH(uint16_t vth, bool db)
{
    if (db)
        vth_db_ = vth;
    else
        vth_ = vth;
}
inline void FebChipInfo::setVMON(uint16_t vmon, bool db)
{
    if (db)
        vmon_db_ = vmon;
    else
        vmon_ = vmon;
}
inline void FebChipInfo::setVTHOffset(int16_t vth_offset, bool db)
{
    if (db)
        vth_offset_db_ = vth_offset;
    else
        vth_offset_ = vth_offset;
}
inline void FebChipInfo::setVMONOffset(int16_t vmon_offset, bool db)
{
    if (db)
        vmon_offset_db_ = vmon_offset;
    else
        vmon_offset_ = vmon_offset;
}

} // namespace xdaqfebpublisher
} // namespace rpct

#endif // rpct_xdaqFebPublisher_FebInfoSystem_inl_h_
