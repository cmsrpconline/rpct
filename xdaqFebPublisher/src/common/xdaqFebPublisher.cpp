#include "rpct/xdaqFebPublisher/xdaqFebPublisher.h"

#include <fstream>
#include <sstream>
#include <vector>

#include "xgi/Method.h"

#include "rpct/tools/html.h"
#include "rpct/tools/WebLogger.h"

#include "rpct/xdaqtools/publisher/Service.h"
#include "rpct/xdaqtools/publisher/Item.h"
#include "rpct/xdaqtools/psx/Client.h"
#include "rpct/xdaqtools/xgitools.h"

#include "rpct/xdaqutils/FebChipInfo.h"

#include "rpct/ii/const.h"
#include "rpct/ii/feb_const.h"
#include "rpct/ii/FebChipLocationId.h"
#include "rpct/xdaqFebPublisher/const.h"

XDAQ_INSTANTIATOR_IMPL(xdaqFebPublisher);

xdaqFebPublisher::xdaqFebPublisher(xdaq::ApplicationStub * s)
    : xdaq::Application(s)
    , wl_(new rpct::tools::WebLogger())
{
    wl_->addReference();

    // TODO: change location of the file
    rpct::tools::LogState ls;
    rpct::tools::LogCenter & lc = ls.getLogCenter();
    lc.loadCategories("/nfshome0/rpcpro/rpct/devices/feb_log_categories.txt");
    lc.loadMessages("/nfshome0/rpcpro/rpct/devices/feb_log_messages.txt");

    db_client_ = new rpct::xdaqutils::XdaqDbServiceClient(this);

    fis_ = new rpct::xdaqfebpublisher::FebInfoSystem();

    action_ = new rpct::tools::ObjectAction<rpct::xdaqtools::publisher::Item &, xdaqFebPublisher>
        (*this, &xdaqFebPublisher::item_action);
    service_ = new rpct::xdaqtools::publisher::Service(*this, action_);

    psx_client_ = new rpct::xdaqtools::psx::Client(*this, getApplicationLogger(), "psx::Application", 0, "cms_rpc_dcs_2");

    rpct::xdaqtools::xgitools::bind(*this, *this, &xdaqFebPublisher::Default, "Default");

    rpct::xdaqtools::xgitools::bind(*this, *this, &xdaqFebPublisher::showSVG, "showSVG");
    rpct::xdaqtools::xgitools::bind(*this, *this, &xdaqFebPublisher::showTable, "showTable");
    rpct::xdaqtools::xgitools::bind(*this, *this, &xdaqFebPublisher::showFebChipInfo, "showFebChipInfo");

    rpct::xdaqtools::xgitools::bind(*this, *this, &xdaqFebPublisher::actions, "actions");
    rpct::xdaqtools::xgitools::bind(*this, *this, &xdaqFebPublisher::reset, "reset");
    rpct::xdaqtools::xgitools::bind(*this, *this, &xdaqFebPublisher::clear, "clear");
    rpct::xdaqtools::xgitools::bind(*this, *this, &xdaqFebPublisher::load_from_db, "load_from_db");

    log4cplus::SharedAppenderPtr wlPtr(wl_);

    wl_->setThreshold(log4cplus::INFO_LOG_LEVEL);
    getApplicationLogger().addAppender(wlPtr);
    log4cplus::Logger::getInstance("psx").addAppender(wlPtr);
    log4cplus::Logger::getInstance("Publisher").addAppender(wlPtr);

    // publisher/psx status monitoring
    log4cplus::Logger _monitor_status_logger(log4cplus::Logger::getInstance("MonitorStatusTimer"));
    _monitor_status_logger.addAppender(wlPtr);
    monitor_status_timer_ = new rpct::xdaqtools::Timer(_monitor_status_logger, "xdaqFebPublisherMonitorStatusTimer");
    try {
        monitor_status_timer_->start();
        toolbox::TimeInterval _interval(15*60); // every 15 minutes
        toolbox::TimeVal _start = toolbox::TimeVal::gettimeofday();
        monitor_status_timer_->scheduleAtFixedRate(*this, &xdaqFebPublisher::xMonitorCycle, "monitorcycle", _start, _interval, 0);
    } catch (toolbox::task::exception::Exception & _e) {
        LOG4CPLUS_ERROR(getApplicationLogger(), "Exception starting monitor status timer: " << _e.what());
    }

    load_from_db();

    monitorCycle();
}

xdaqFebPublisher::~xdaqFebPublisher()
{
    // publisher/psx status monitoring
    try {
        if (monitor_status_timer_->isActive())
            monitor_status_timer_->stop();
    } catch (toolbox::task::exception::Exception & _e) {
        LOG4CPLUS_ERROR(getApplicationLogger(), "Exception stopping monitor status timer: " << _e.what());
    }
    delete monitor_status_timer_;

    rpct::xdaqtools::xgitools::unbind(*this, "Default");
    rpct::xdaqtools::xgitools::unbind(*this, "showSVG");
    rpct::xdaqtools::xgitools::unbind(*this, "showTable");
    rpct::xdaqtools::xgitools::unbind(*this, "showFebChipInfo");
    rpct::xdaqtools::xgitools::unbind(*this, "reset");
    rpct::xdaqtools::xgitools::unbind(*this, "clear");
    rpct::xdaqtools::xgitools::unbind(*this, "load_from_db");
    delete psx_client_;
    delete service_;
    delete action_;
    delete fis_;

    wl_->removeReference();
}

int xdaqFebPublisher::item_action(rpct::xdaqtools::publisher::Item & item)
{
    int type = item.getType();
    {
        rpct::tools::WLock wl(fis_mutex_);
        rpct::xdaqfebpublisher::FebChipInfo * fci = fis_->getFebChipInfo(item.getId());
        if (fci)
            {
                fci->set(item.getType(), item.getValue());
                if (type == rpct::publish::type::feb::temp_
                    || type == rpct::publish::type::feb::vth_
                    || type == rpct::publish::type::feb::vmon_)
                    {
                        std::stringstream dpname;
                        dpname << "Feb" << fci->getPvssId();
                        std::stringstream dptype;
                        dptype << rpct::xdaqfebpublisher::pvss_typename_[type - rpct::xdaqfebpublisher::type_offset_];
                        double dpvalue = item.getValue();
                        if (type != rpct::publish::type::feb::temp_)
                            {
                                dptype << (int)(fci->getLocation().getPosition() + 1);
                                dpvalue *= rpct::preset::feb::unit_;
                            }
                        else
                            dptype << ((fci->getLocation().getPosition()) > 1 ? 2 : 1);
                        psx_client_->publish(dpname.str(), dptype.str(), dpvalue);
                    }
            }
    }
    if (type == rpct::publish::type::feb::vth_offset_
        || type == rpct::publish::type::feb::vmon_offset_)
        {
            std::ofstream ofile("/tmp/febdata.txt", std::ios::out | std::ios::app);
            if (ofile.is_open())
                ofile << item.getId() << " " << item.getType() << " " << item.getValue() << std::endl;
            ofile.close();
        }
    return 0;
}

void xdaqFebPublisher::Default(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception)
{
    cgicc::Cgicc cgi(in);
    unsigned int type(rpct::xdaqfebpublisher::state_type_state_);
    cgicc::const_form_iterator fi = cgi.getElement("type");
    if (fi != cgi.getElements().end())
        type = fi->getIntegerValue();
    if (type >= rpct::xdaqfebpublisher::state_type_max_)
        type = rpct::xdaqfebpublisher::state_type_state_;

    std::stringstream ssubtitle;
    ssubtitle << rpct::xdaqfebpublisher::state_types_[type] << " summary";
    rpct::tools::html::header(*out, "FEB Detector Overview", ssubtitle.str(), true);

    *out << "<div class=\"box topleft\">" << std::endl;
    menu(*out);
    *out << "<h3>Category</h3>\n <ul class=\"states\">" << std::endl;
    for (unsigned char itype = 0 ; itype < rpct::xdaqfebpublisher::state_type_max_ ; ++itype)
        {
            *out << "<li";
            if (itype == type)
                *out << " style=\"background-color:#729fcf;\"";
            *out << "><a href=\"?type=" << (int)itype << "\">" << rpct::xdaqfebpublisher::state_types_[itype] << "</a></li>\n";
        }
    *out << "</ul>" << std::endl;
    *out << "</div>" << std::endl;

    *out << "<div class=\"box\"><div class=\"info\">Please refresh the page to get the latest states and values.</div></div>" << std::endl;

    {
        rpct::tools::RLock rl(fis_mutex_);
        std::stringstream page;
        page.str("");
        page << "showSVG?type=" << type;
        fis_->printSVGEmbed(*out, page.str());
    }

    wl_->printHTML(*out);

    rpct::tools::html::footer(*out);
}
void xdaqFebPublisher::showTable(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception)
{
    cgicc::Cgicc cgi(in);

    unsigned int min_level(rpct::tools::State::info_);
    cgicc::const_form_iterator fi = cgi.getElement("level");
    if (fi != cgi.getElements().end())
        min_level = fi->getIntegerValue();
    if (min_level > rpct::tools::State::max_)
        min_level = rpct::tools::State::info_;

    double dfclid(0);
    fi = cgi.getElement("fclid");
    if (fi != cgi.getElements().end())
        dfclid = fi->getDoubleValue();
    uint32_t fclid_id((uint32_t)dfclid);
    rpct::FebChipLocationId fclid(fclid_id);

    std::stringstream ssubtitle;
    ssubtitle << fclid << " summary table";
    rpct::tools::html::header(*out, "FEB Detector Overview", ssubtitle.str(), true);

    *out << "<div class=\"box topleft\">" << std::endl;
    menu(*out);
    *out << "<h3>Minimum state</h3>\n <ul class=\"states\">\n" << std::endl;
    for (unsigned char level = 0 ; level < rpct::tools::State::max_ ; ++level)
        {
            *out << "<li class=\"fg_" << rpct::tools::State::getStateShort(level);
            if (level == min_level)
                *out << " bg_" << rpct::tools::State::getStateShort(level);
            *out << "\">";
            *out << "<a href=\"?fclid=" << fclid_id
                 << "&level=" << (int)(level)
                 <<  "\">" << rpct::tools::State::getStateName(level) << "</a>\n</li>" << std::endl;
        }
    *out << "</ul>" << std::endl;
    *out << "</div>" << std::endl;

    *out << "<div class=\"box\"><div class=\"info\">Please refresh the page to get the latest states and values.</div></div>" << std::endl;

    {
        rpct::tools::RLock rl(fis_mutex_);
        fis_->printTable(*out, fclid, min_level);
    }
    rpct::tools::html::footer(*out);
}

void xdaqFebPublisher::showFebChipInfo(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception)
{
    cgicc::Cgicc cgi(in);

    int id(0);
    cgicc::const_form_iterator fi = cgi.getElement("id");
    if (fi != cgi.getElements().end())
        id = fi->getIntegerValue();

    {
        rpct::tools::RLock rl(fis_mutex_);
        rpct::xdaqfebpublisher::FebChipInfo * fci = fis_->getFebChipInfo(id);
        if (fci)
            {
                rpct::tools::html::header(*out, "FebChipInfo", fci->getLocation().getName(), true);
                *out << "<div class=\"box topleft\">" << std::endl;
                menu(*out);
                *out << "</div>" << std::endl;

                *out << "<div class=\"box\"><div class=\"info\">Please refresh the page to get the latest states and values.</div></div>" << std::endl;

                fci->printHTML(*out);
                wl_->printHTML(*out);
                rpct::tools::html::footer(*out);
            }
        else
            {
                rpct::tools::html::header(*out, "FEB chip Info", "Unknown FEB chip", true);
                *out << "<div class=\"box topleft\">" << std::endl;
                menu(*out);
                *out << "</div>" << std::endl;

                *out << "<div class=\"box\"><div class=\"warn\">FEB chip not found.</div></div>" << std::endl;

                wl_->printHTML(*out);
                rpct::tools::html::footer(*out);
            }
    }
}

void xdaqFebPublisher::showSVG(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception)
{
    int type = 0;
    cgicc::Cgicc cgi(in);
    cgicc::const_form_iterator fi = cgi.getElement("type");
    if (fi != cgi.getElements().end())
        type = fi->getIntegerValue();

    rpct::tools::RLock rl(fis_mutex_);
    fis_->printSVGDocument(*out, type);
}

void xdaqFebPublisher::reset()
{
    rpct::tools::WLock wl(fis_mutex_);
    fis_->reset();
    LOG4CPLUS_INFO(getApplicationLogger(), "Reset all states and values.");
}
void xdaqFebPublisher::load_from_db()
{
    /* */
    try {
        LOG4CPLUS_INFO(getApplicationLogger(), "Loading FebChips from DB");
        rpct::FebChipInfoVector * fciv = db_client_->getFebChipInfoVector();
        if (fciv)
            {
                LOG4CPLUS_INFO(getApplicationLogger(), "Loading " << fciv->size() << " FebChips");
                rpct::tools::WLock wl(fis_mutex_);

                fis_->clear();

                rpct::FebChipLocationId fclid;
                int subsector, partition;
                rpct::FebChipInfoVector::const_iterator IfciEnd = fciv->end();
                for (rpct::FebChipInfoVector::const_iterator Ifci = fciv->begin()
                         ; Ifci != IfciEnd
                         ; ++Ifci)
                    {
                        fclid.setBoE(Ifci->bag.getBoE());
                        fclid.setWheel(Ifci->bag.getWheel());
                        fclid.setLayer(Ifci->bag.getLayer());
                        fclid.setSector(Ifci->bag.getSector());

                        std::string const & ssubsector = Ifci->bag.getSubsector();
                        subsector = rpct::tools::locationid::wildcard_;
                        if (ssubsector != "")
                            {
                                if (ssubsector == "-")
                                    subsector = 1;
                                else if (ssubsector == "+")
                                    subsector = 2;
                                else if (ssubsector == "--")
                                    subsector = 3;
                                else if (ssubsector == "++")
                                    subsector = 6;
                                if (Ifci->bag.getSector() == 4 && Ifci->bag.getLayer() == 6)
                                    {
                                        if (subsector == 1)
                                            subsector = 4;
                                        else if (subsector == 2)
                                            subsector = 5;
                                    }
                                else if ((Ifci->bag.getSector() == 9 || Ifci->bag.getSector() == 11) && Ifci->bag.getLayer() == 6)
                                    subsector = rpct::tools::locationid::wildcard_;
                            }
                        fclid.setSubsector(subsector);

                        std::string const & spartition = Ifci->bag.getPartition();
                        partition = rpct::tools::locationid::wildcard_;
                        if (spartition == "D")
                            partition = 4;
                        else if (spartition == "C" || spartition == "Forward")
                            partition = 3;
                        else if (spartition == "B" || spartition == "Central")
                            partition = 2;
                        else if (spartition == "A" || spartition == "Backward")
                            partition = 1;
                        fclid.setPartition(partition);

                        fclid.setChannel(Ifci->bag.getChannel());
                        fclid.setAddress(Ifci->bag.getAddress());
                        fclid.setPosition(Ifci->bag.getPosition());

                        fis_->addFebChip(Ifci->bag.getChipId(),Ifci->bag.getFebLocationId(), fclid);
                    }
                delete fciv;
                LOG4CPLUS_INFO(getApplicationLogger(), "Loaded " << fis_->size() << " FebChips");
            }
        else
            {
                LOG4CPLUS_WARN(getApplicationLogger(), "error loading from db, didn't receive information.");
            }
    } catch (xcept::Exception& e) {
        LOG4CPLUS_WARN(getApplicationLogger(), "error loading from db, " << e.what());
    } catch (std::exception& e) {
        LOG4CPLUS_WARN(getApplicationLogger(),  "error loading from db, " << e.what());
    } catch(...) {
        LOG4CPLUS_WARN(getApplicationLogger(), "unknown error loading from db");
    }
    /*    */ // this is a 904 test fill
    /*
    {
        rpct::tools::WLock wl(fis_mutex_);
        int id(3456);
        rpct::FebChipLocationId fclid;
        fclid.setBoE(0);

        fclid.setWheel(-2);
        fclid.setPosition(0);
        for (int l = 1 ; l <= 6 ; ++l)
            {
                fclid.setLayer(l);
                for (int s = 1 ; s <= 12 ; ++s)
                    {
                        fclid.setSector(s);
                        ++id;
                        fis_->addFebChip(id, id, fclid);
                    }
            }
    }
    */

    monitorCycle();

    LOG4CPLUS_INFO(getApplicationLogger(), "Loaded information.");
}
void xdaqFebPublisher::clear()
{
    rpct::tools::WLock wl(fis_mutex_);
    fis_->clear();

    monitorCycle();

    LOG4CPLUS_INFO(getApplicationLogger(), "Cleared information.");
}

void xdaqFebPublisher::actions(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception)
{
    rpct::tools::html::header(*out, "FEB Detector Overview", "Actions", true);

    *out << "<div class=\"box topleft\">" << std::endl;
    menu(*out);
    *out << "</div>" << std::endl;

    *out << "<div class=\"box\">" << std::endl;
    *out << "<h3>States and values</h3>" << std::endl;
    *out << "<div class=\"info\">Resets the values and states</div>" << std::endl;
    *out << "<form method=\"get\" action=\"reset\"><input type=\"submit\" value=\"reset\" /></form>" << std::endl;
    *out << "</div>" << std::endl;
    *out << "<div class=\"box\">" << std::endl;
    *out << "<h3>Information</h3>" << std::endl;
    *out << "<div class=\"warn\">It takes quite some time to load from DB</div>" << std::endl;
    *out << "<form method=\"get\" action=\"load_from_db\"><input type=\"submit\" value=\"load information\" /></form>" << std::endl;
    *out << "<form method=\"get\" action=\"clear\"><input type=\"submit\" value=\"clear information\" /></form>" << std::endl;
    *out << "</div>" << std::endl;

    wl_->printHTML(*out);

    rpct::tools::html::footer(*out);
}

void xdaqFebPublisher::menu(std::ostream & os) const
{
    os << "<h3>Menu</h3>\n <ul class=\"states\">" << std::endl;
    os << "<li><a href=\"Default\">Overview</a></li>" << std::endl;
    os << "<li><a href=\"actions\">Actions</a></li>" << std::endl;
    os << "</ul>" << std::endl;
}

void xdaqFebPublisher::reset(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception)
{
    reset();
    rpct::xdaqtools::xgitools::redirect(*this, *out, "actions");
}
void xdaqFebPublisher::clear(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception)
{
    clear();
    rpct::xdaqtools::xgitools::redirect(*this, *out, "actions");
}
void xdaqFebPublisher::load_from_db(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception)
{
    load_from_db();
    rpct::xdaqtools::xgitools::redirect(*this, *out, "actions");
}

int xdaqFebPublisher::xMonitorCycle(toolbox::task::TimerEvent &)
{
    monitorCycle();
    return 0;
}

void xdaqFebPublisher::monitorCycle()
{
    LOG4CPLUS_TRACE(getApplicationLogger(), "called xdaqFebPublisher::monitorCycle");
    {
        rpct::tools::WLock _lock(monitor_status_mutex_);
        monitor_status_ = 0;
        monitor_status_description_.clear();
        if (fis_->size() == 0)
            {
                monitor_status_ |= 0x02; // not loaded
                monitor_status_description_ += "RPC FEB locations not loaded in the RPC FEB Detector Overview Application. Please verify if this is intentional.\n";
            }
        psx_client_->publish("FebCommunicationStatus", "fvalue", monitor_status_);
        if (monitor_status_ > 0)
            psx_client_->publish("FebCommunicationStatus", "svalue", monitor_status_description_);
    }
}
