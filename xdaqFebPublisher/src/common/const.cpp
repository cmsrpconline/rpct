#include "rpct/xdaqFebPublisher/const.h"

namespace rpct {
namespace xdaqfebpublisher {

const char * pvss_typename_[3] = {"temperature", "vth", "vmon"};

} // namespace xdaqfebpublisher
} // namespace rpct
