#include "rpct/xdaqFebPublisher/FebInfoSystem.h"

#include <cmath>
#include <iomanip>

#include "rpct/ii/const.h"
#include "rpct/ii/feb_const.h"
#include "rpct/ii/FebChipLocationId.h"

namespace rpct {
namespace xdaqfebpublisher {

const unsigned int state_type_temperature_ = 0;
const unsigned int state_type_vth_         = 1;
const unsigned int state_type_vmon_        = 2;
const unsigned int state_type_offset_      = 3;
const unsigned int state_type_state_       = 4;
const char * state_types_[state_type_max_] = {"Temperature", "VTH", "VMON", "Offset", "State"};
const int nwheels_[2] = {5,6};
const float wheeloffset_[2] = {2,2};
const int minwheel_[2] = {-2,-4};
const int maxwheel_[2] = {2,4};
const float radius_ = 75.;
const float rspacer_ = 2.;
const float anglespacer_ = .02;
const float wheelspacer_ = 5.;

bool FebChipInfo::init_ = false;
int FebChipInfo::state_categories_[state_type_max_] = {0,0,0,0,0};
rpct::tools::LogCenter * FebChipInfo::lc_ = 0;

FebInfoSystem::FebInfoSystem()
{}
FebInfoSystem::~FebInfoSystem()
{
    clear();
}

void FebInfoSystem::reset()
{
    std::map<rpct::FebChipLocationId, ChamberInfo *>::iterator IcisEnd = cis_.end();
    for (std::map<rpct::FebChipLocationId, ChamberInfo *>::iterator Icis = cis_.begin()
             ; Icis != IcisEnd
             ; ++Icis)
        Icis->second->reset();
}
void FebInfoSystem::clear()
{
    std::map<rpct::FebChipLocationId, ChamberInfo * >::iterator IcisEnd = cis_.end();
    for (std::map<rpct::FebChipLocationId, ChamberInfo * >::iterator Icis = cis_.begin()
             ; Icis != IcisEnd
             ; ++Icis)
        delete Icis->second;
    cis_.clear();
    fcis_.clear();
    reset();
}

void FebInfoSystem::printTable(std::ostream & os, rpct::FebChipLocationId mask, unsigned char min_state) const
{
    os << "<table>\n<tr><th>location</th><th>id</th><th>vth</th><th>vth db</th><th>vmon</th><th>vmon db</th><th>vth_offset</th><th>vth_offset db</th><th>vmon_offset</th><th>vmon_offset db</th><th>temperature</th></tr>" << std::endl;

    std::map<rpct::FebChipLocationId, ChamberInfo *>::const_iterator IcisEnd = cis_.end();
    for (std::map<rpct::FebChipLocationId, ChamberInfo *>::const_iterator Icis = cis_.begin()
             ; Icis != IcisEnd
             ; ++Icis)
        Icis->second->printRows(os, mask, min_state);
    os << "</table>" << std::endl;
}

void FebInfoSystem::printSVGCSS(std::ostream & os, int type) const
{
    os << "text.label {font-family:Helvetica, Verdana, Arial, Times New Roman, Times, Serif;font-size:16px;text-anchor:middle;dominant-baseline:middle;}" << std::endl;
    for (int i = 0 ; i <= rpct::tools::State::max_ ; ++i)
        os << "path." << rpct::tools::State::getStateShort(i) << " {fill:" << rpct::tools::State::getStateColor(i) << ";}" << std::endl;
}
void FebInfoSystem::printSVG(std::ostream & os, int type) const
{
    rpct::FebChipLocationId fclid;
    for (int boe = 0 ; boe < 2 ; ++boe)
        {
            fclid.setBoE(boe);
            for (int wheel = minwheel_[boe] ; wheel <= maxwheel_[boe] ; ++wheel)
                {
                    if (boe && wheel == 0)
                        ++wheel;
                    fclid.setWheel(wheel);
                    float wheelx = (wheel * (boe ? .5 : 1.)) + wheeloffset_[boe];
                    float x = wheelx*2.*radius_ + wheelspacer_*wheelx + wheelspacer_;
                    float y = (boe * (1. + (std::abs(wheel)+1)%2)) * 2.*radius_ + boe * wheelspacer_;
                    os << "<a xlink:href=\"showTable?fclid=" << fclid.getId() << "\" target=\"_parent\">"
                       << "<text x=\"" << (int)(x+radius_) << "\" y=\"" << (int)(y+radius_) << "\" class=\"label\">"
                       << rpct::FebChipLocationId::boe_[boe+1] << std::showpos << wheel << std::noshowpos
                       << "</text></a>" << std::endl;
                }
        }
    std::map<rpct::FebChipLocationId, ChamberInfo *>::const_iterator IcisEnd = cis_.end();
    for (std::map<rpct::FebChipLocationId, ChamberInfo *>::const_iterator Icis = cis_.begin()
             ; Icis != IcisEnd
             ; ++Icis)
        Icis->second->printSVG(os, "showTable", type);
}

void FebInfoSystem::addFebChip(int id, int pvss_id, rpct::FebChipLocationId fclid)
{
    rpct::FebChipLocationId clid(fclid);
    clid.setPosition();
    clid.setAddress();
    clid.setChannel();
    clid.setPartition();
    clid.setSubsector();
    std::map<rpct::FebChipLocationId, ChamberInfo *>::iterator Icis = cis_.find(clid);
    if (Icis == cis_.end())
        {
            ChamberInfo * ci = new ChamberInfo(clid);
            Icis = (cis_.insert(std::pair<rpct::FebChipLocationId, ChamberInfo * >(clid, ci))).first;
        }
    fcis_.insert(std::pair<int, FebChipInfo *>
                 (id, &(Icis->second->addFebChip(id, pvss_id, fclid)))
                 );
}

FebChipInfo * FebInfoSystem::getFebChipInfo(int id)
{
    FebChipInfo * fci = 0;
    std::map<int, FebChipInfo * >::iterator it = fcis_.find(id);
    if (it != fcis_.end())
        fci = it->second;
    return fci;
}

FebChipInfo const * FebInfoSystem::getFebChipInfo(int id) const
{
    FebChipInfo const * fci = 0;
    std::map<int, FebChipInfo * >::const_iterator it = fcis_.find(id);
    if (it != fcis_.end())
        fci = it->second;
    return fci;
}

ChamberInfo::ChamberInfo(rpct::FebChipLocationId fclid)
    : fclid_(fclid)
{
    fclid_.setAddress();
    fclid_.setChannel();
    fclid_.setRoll();
    fclid_.setSubsector();
    for (unsigned int type = 0 ; type < state_type_max_ ; ++type)
        states_[type].log("ok");
}

ChamberInfo::~ChamberInfo()
{
    clear();
}
void ChamberInfo::reset()
{
    std::map<rpct::FebChipLocationId, FebChipInfo * >::iterator IfcisEnd = fcis_.end();
    for (std::map<rpct::FebChipLocationId, FebChipInfo * >::iterator Ifcis = fcis_.begin()
             ; Ifcis != IfcisEnd
             ; ++Ifcis)
        Ifcis->second->reset();
    for (unsigned int type = 0 ; type < state_type_max_ ; ++type)
        {
            states_[type].reset();
            states_[type].log("ok");
        }
}
void ChamberInfo::clear()
{
    std::map<rpct::FebChipLocationId, FebChipInfo * >::iterator IfcisEnd = fcis_.end();
    for (std::map<rpct::FebChipLocationId, FebChipInfo * >::iterator Ifcis = fcis_.begin()
             ; Ifcis != IfcisEnd
             ; ++Ifcis)
        delete Ifcis->second;
    fcis_.clear();
    reset();
}

void ChamberInfo::printRows(std::ostream & os, rpct::FebChipLocationId mask, unsigned char min_state) const
{
    if (fclid_.matches(mask) && states_[state_type_state_].getPState() >= min_state)
        {
            std::map<rpct::FebChipLocationId, FebChipInfo * >::const_iterator IfcisEnd = fcis_.end();
            for (std::map<rpct::FebChipLocationId, FebChipInfo * >::const_iterator Ifcis = fcis_.begin()
                     ; Ifcis != IfcisEnd
                     ; ++Ifcis)
                Ifcis->second->printRow(os, mask, min_state);

        }
}

void ChamberInfo::printSVG(std::ostream & os, const std::string & href, int type) const
{
    bool boe = fclid_.getBoE();
    int wheel = fclid_.getWheel();
    float layer = fclid_.getLayer();
    float sector = fclid_.getSector();

    float wheelx = (wheel * (boe ? .5 : 1.)) + wheeloffset_[boe];
    float x = wheelx*2.*radius_ + wheelspacer_*wheelx + wheelspacer_;
    float y = (boe * (1. + (std::abs(wheel)+1)%2)) * 2.*radius_ + boe * wheelspacer_;

    float cradius = radius_ / (boe?3:2);
    float sectors = 12;
    if (boe)
        {
            if (layer == 1 && wheel != 1 && wheel != -1)
                sectors = 18;
            else
                sectors = 36;
        }
    unsigned int layers = 6;
    if (boe)
        layers = 3;

    // the inner ring with 18 chambers starts together with the 10 degree chamber
    float diffangle = 1;
    if (boe && layer == 1 && wheel != 1 && wheel != -1)
        diffangle = .5;

    float unitangle = 6.283/sectors;
    float anglea = (sector - diffangle - .5) * unitangle + anglespacer_;
    float angleb = (sector - diffangle + .5) * unitangle - anglespacer_;
    float xfaca = cos(anglea);
    float yfaca = -sin(anglea);
    float xfacb = cos(angleb);
    float yfacb = -sin(angleb);
    int rx = (int)(x + radius_);
    int ry = (int)(y + radius_);
    float ra = cradius + (radius_ - cradius) / layers * (layer-1.);
    float rb = cradius + (radius_ - cradius) / layers * layer - rspacer_;
    //    if (getState(type).getPState() > rpct::tools::State::ok_)
    os << "<a xlink:href=\"" << href << "?fclid=" << fclid_.getId() << "\" target=\"_parent\">";
    os << "<path title=\"" << fclid_ << "\" d=\"M"
       << (int)(rx + ra*xfaca) << "," << (int)(ry + ra*yfaca) << "L"
       << (int)(rx + rb*xfaca) << "," << (int)(ry + rb*yfaca) << " "
       << (int)(rx + rb*xfacb) << "," << (int)(ry + rb*yfacb) << " "
       << (int)(rx + ra*xfacb) << "," << (int)(ry + ra*yfacb) << "z"
       << "\" class=\"" << rpct::tools::State::getStateShort(getState(type).getPState()) << "\"/>";
//    if (getState(type).getPState() > rpct::tools::State::ok_)
    os << "</a>" << std::endl;
}

FebChipInfo & ChamberInfo::addFebChip(int id, int pvss_id, rpct::FebChipLocationId fclid)
{
    std::map<rpct::FebChipLocationId, FebChipInfo *>::iterator Ifcis = fcis_.find(fclid);
    if (Ifcis == fcis_.end())
        {
            FebChipInfo * fci = new FebChipInfo(id, pvss_id, fclid);
            Ifcis = (fcis_.insert(std::pair<rpct::FebChipLocationId, FebChipInfo * >(fclid, fci))).first;
            for (unsigned int type = 0 ; type < state_type_max_ ; ++type)
                states_[type].addTreeChild(fci->getState(type));
        }
    return *(Ifcis->second);
}

FebChipInfo::FebChipInfo(int id, int pvss_id, rpct::FebChipLocationId fclid)
    : id_(id)
    , pvss_id_(pvss_id)
    , fclid_(fclid)
    , temperature_(0)
    , vth_(0)
    , vmon_(0)
    , vth_offset_(rpct::preset::feb::offset_keep_)
    , vmon_offset_(rpct::preset::feb::offset_keep_)
    , vth_db_(0)
    , vmon_db_(0)
    , vth_offset_db_(rpct::preset::feb::offset_keep_)
    , vmon_offset_db_(rpct::preset::feb::offset_keep_)
{
    // TODO: change location of the file
    if (!init_)
        {
            init_ = 1;
            rpct::tools::LogState ls;
            lc_ = &(ls.getLogCenter());
            state_categories_[state_type_temperature_] = lc_->getCategory("febchip_temp").getId();
            state_categories_[state_type_vth_] = lc_->getCategory("febchip_vth").getId();
            state_categories_[state_type_vmon_] = lc_->getCategory("febchip_vmon").getId();
            state_categories_[state_type_offset_] = lc_->getCategory("febchip_offset").getId();
            state_categories_[state_type_state_] = lc_->getCategory("febchip").getId();
        }
}
FebChipInfo::~FebChipInfo()
{
}

void FebChipInfo::reset()
{
    temperature_ = 0;
    vth_ = 0;
    vmon_ = 0;
    vth_offset_ = rpct::preset::feb::offset_keep_;
    vmon_offset_ = rpct::preset::feb::offset_keep_;
    vth_db_ = 0;
    vmon_db_ = 0;
    vth_offset_db_ = rpct::preset::feb::offset_keep_;
    vmon_offset_db_ = rpct::preset::feb::offset_keep_;
    for (unsigned int type = 0 ; type < state_type_max_ ; ++type)
        states_[type].reset();
}

void FebChipInfo::printRow(std::ostream & os, rpct::FebChipLocationId mask, unsigned char min_state) const
{
    if (fclid_.matches(mask) && states_[state_type_state_].getPState() >= min_state)
        {
            os << "<tr>"
               << "<td class=\"fg_" << rpct::tools::State::getStateShort(states_[state_type_state_].getState()) << "\">"
               << "<a href=\"showFebChipInfo?id=" << id_ << "\">" << fclid_ << "</a></td>"
               << "<td>" << id_ << "</td>"
               << "<td class=\"fg_" << rpct::tools::State::getStateShort(states_[state_type_vth_].getState()) << "\">"
               << vth_ * rpct::preset::feb::unit_ << "</td>"
               << "<td>" << vth_db_ * rpct::preset::feb::unit_ << "</td>"
               << "<td class=\"fg_" << rpct::tools::State::getStateShort(states_[state_type_vmon_].getState()) << "\">"
               << vmon_* rpct::preset::feb::unit_<< "</td>"
               << "<td>" << vmon_db_ * rpct::preset::feb::unit_ << "</td>"
               << "<td class=\"fg_" << rpct::tools::State::getStateShort(states_[state_type_offset_].getState()) << "\">"
               << (vth_offset_ == rpct::preset::feb::offset_keep_ ? 0 : (vth_offset_ * rpct::preset::feb::unit_) ) << "</td>"
               << "<td>" << (vth_offset_db_ == rpct::preset::feb::offset_keep_ ? 0 : vth_offset_db_ * rpct::preset::feb::unit_)  << "</td>"
               << "<td>" << (vmon_offset_ == rpct::preset::feb::offset_keep_ ? 0 : vmon_offset_ * rpct::preset::feb::unit_) << "</td>"
               << "<td>" << (vmon_offset_db_ == rpct::preset::feb::offset_keep_ ? 0 : vmon_offset_db_ * rpct::preset::feb::unit_)  << "</td>"
               << "<td class=\"fg_" << rpct::tools::State::getStateShort(states_[state_type_temperature_].getState()) << "\">"
               << temperature_ << "</td></tr>" << std::endl;
        }
}

void FebChipInfo::printParameters(rpct::tools::Parameters & parameters) const
{
    parameters.add("id", id_);
    parameters.add("pvss id", pvss_id_);
    parameters.add("temperature", temperature_);
    parameters.add("vth  read", (vth_ * rpct::preset::feb::unit_));
    parameters.add("vmon  read", (vmon_ * rpct::preset::feb::unit_));
    parameters.add("vth  in", (vth_db_ * rpct::preset::feb::unit_));
    parameters.add("vmon in", (vmon_db_ * rpct::preset::feb::unit_));
    parameters.add("vth  offset", (vth_offset_ == rpct::preset::feb::offset_keep_ ? 0 : (vth_offset_ * rpct::preset::feb::unit_)));
    parameters.add("vmon offset", (vmon_offset_ == rpct::preset::feb::offset_keep_ ? 0 : (vmon_offset_ * rpct::preset::feb::unit_)));
    parameters.add("vth  offset in", (vth_offset_db_ == rpct::preset::feb::offset_keep_ ? 0 : (vth_offset_db_ * rpct::preset::feb::unit_)));
    parameters.add("vmon offset in", (vmon_offset_db_ == rpct::preset::feb::offset_keep_ ? 0 : (vmon_offset_db_ * rpct::preset::feb::unit_)));
}

void FebChipInfo::printHTML(std::ostream & os) const
{
    os << "<div class=\"box\">" << std::endl;
    states_[state_type_state_].printLogHTML(os);
    rpct::tools::Printable::printHTML(os);
    os << "</div>" << std::endl;
}

void FebChipInfo::set(int type, double value)
{
    if (type == rpct::publish::type::feb::temp_)
        setTemperature((float)value);
    else if (type == rpct::publish::type::feb::vth_)
        setVTH((uint16_t)value);
    else if (type == rpct::publish::type::feb::vmon_)
        setVMON((uint16_t)value);
    else if (type == rpct::publish::type::feb::vth_offset_)
        setVTHOffset((int16_t)value);
    else if (type == rpct::publish::type::feb::vmon_offset_)
        setVMONOffset((int16_t)value);
    else if (type == rpct::publish::type::feb::vth_set_)
        setVTH((uint16_t)value, true);
    else if (type == rpct::publish::type::feb::vmon_set_)
        setVMON((uint16_t)value, true);
    else if (type == rpct::publish::type::feb::vth_offset_set_)
        setVTHOffset((int16_t)value, true);
    else if (type == rpct::publish::type::feb::vmon_offset_set_)
        setVMONOffset((int16_t)value, true);
    else if (type == rpct::publish::type::log_)
        {
            int _log((int)value);
            int category = lc_->getMessage(_log).getCategoryId();
            bool done(false);
            for (unsigned int i = 0 ; i < state_type_max_ && !done; ++i)
                if (category == state_categories_[i])
                    {
                        done = true;
                        getState(i).log(_log);
                    }
            getState(state_type_state_).log(_log);
        }
    else if (type == rpct::publish::type::unlog_)
        {
            int _unlog((int)value);
            int category = lc_->getMessage(_unlog).getCategoryId();
            bool done(false);
            for (unsigned int i = 0 ; i < state_type_max_ && !done; ++i)
                if (category == state_categories_[i])
                    {
                        done = true;
                        getState(i).unlog(_unlog);
                    }
            getState(state_type_state_).unlog(_unlog);
        }
}

} // namespace xdaqfebpublisher
} // namespace rpct
