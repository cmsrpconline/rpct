#include <iostream>
#include <sstream>
#include <ext/hash_map>
#include <list>
#include <time.h>
#include <sys/time.h>

using namespace std;    

namespace __gnu_cxx
{
        template<> struct hash< std::string >
        {
                size_t operator()( const std::string& x ) const
                {
                        return hash< const char* >()( x.c_str() );
                }
        };
}

class ElementBase {
private:
	int id_;
	string name_;
public:
	ElementBase(int id, string& name) : id_(id), name_(name) {
	}
	int getId() {
		return id_;
	}
	void setId(int id) {
		id_ = id;
	}
	string& getName() {
		return name_;
	}
	void setName(string& name) {
		name_ = name;
	}
};

class Element : public ElementBase {
public:
	Element(int id, string name) : ElementBase(id, name) {
	}        
};

class Container {
public:
	typedef __gnu_cxx::hash_map<string, ElementBase*> ElementMap;
	typedef list<ElementBase*> ElementList;
private:
	int id_;
	string name_;    
	ElementMap map_;
	ElementList list_;
public:
	Container(int id, string name) : id_(id), name_(name) {
	}

	~Container() {
		for (ElementList::iterator iEl = list_.begin(); iEl != list_.end(); ++iEl) {
			delete *iEl;
		}
	}
	void addElement(int id, const string& name) {
		Element* e = new Element(id, name);
		//map_.insert(ElementMap::value_type(name, e));
		list_.push_back(e);
	}

	int getId() {
		return id_;
	}
	void setId(int id) {
		id_ = id;
	}
	string getName() {
		return name_;
	}
	void setName(string name) {
		name_ = name;
	}
	ElementMap& getMap() {
		return map_;
	}
	ElementList& getList() {
		return list_;
	}
	ElementBase& getElementByName(const string& name) {
		return *map_[name];
	}
};


int main() {
	cout << "Start:" << endl;
	long milis = time(NULL);
	Container c(1, "c");
	for (int i = 0; i < 100000; i++) {
		stringstream str;
		str << i;
		c.addElement(i, str.str());
	}
	for (int n = 0; n < 100; n++) { 
		int totalNameLength = 0;
		for (int i = 0; i < 100000; i++) {
			stringstream str;
			str << i;
			totalNameLength += c.getElementByName(str.str()).getName().size();
		}
		/*for (Container::ElementList::iterator iEl = c.getList().begin(); iEl != c.getList().end(); ++iEl) {
			totalNameLength += (*iEl)->getName().size();
		}*/
		//System.out.println("totalNameLength = " + totalNameLength);
		/*if ((n % 10) == 1) {
			//System.out.println("Done " + (System.currentTimeMillis() - milis));
			cout << "Done" << endl;
		}*/
	}
	cout << "Done " << (time(NULL) - milis) <<  endl;
}
