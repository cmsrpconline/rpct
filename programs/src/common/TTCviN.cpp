/***********************************************************************
*  Karol Bunkowski 2001
*  Warsaw University
*  kbunkow@fuw.edu.pl
*	
*  
*  
/***********************************************************************/

#pragma hdrstop

#include "TTCviN.h"

void B_Go::Settings(bool enable, bool sync, bool single, bool fifo, bool calibration)
       {
        std::bitset<5> buf;
        buf[0]= enable; buf[1]=sync; buf[2]=single; buf[3]=fifo; buf[4]=calibration;
        VME->Write16(BoardAddress + SettingsAddres, buf.to_ulong());
       }

std::bitset<5> B_Go::ReadSettings()
       {
        unsigned short a=VME->Read16(BoardAddress + SettingsAddres);
         std::bitset<5> buf(a);
         return buf;
       }

void B_Go::B_GoSignal()
       {
        VME->Write16(BoardAddress + B_GoSignalAddres, 0xff);
       }

void B_Go::WriteDataLong(unsigned short rx_addr, bool E,
                  unsigned short subaddr, unsigned short data )
       {
         std::bitset<32> word;
         std::bitset<14> b_rx_addr(rx_addr);
         std::bitset<8>  b_subaddr(subaddr);
         std::bitset<8>  b_data(data);

         word[31]=1;
		 int i=0;
         for( i=0;i<14;i++) 
			 word[i+17]=b_rx_addr[i];
         word[16]=E;
         for( i=0;i<8 ;i++) 
			 word[i+8]=b_subaddr[i];
         for( i=0;i<8 ;i++) 
			 word[i]=b_data[i];
         VME->Write32(BoardAddress + DataAddres, word.to_ulong() );
       }

void B_Go::WriteDataShort(unsigned short command )
       {
         std::bitset<32> word;
         std::bitset<8>  b_command(command);

         word[31]=0;
         for(int i=0;i<8 ;i++) word[i+23]=b_command[i];
         VME->Write32(BoardAddress + DataAddres, word.to_ulong() );
       }

void B_Go::WriteData(std::bitset<32> word)
       {
         VME->Write32(BoardAddress + DataAddres, word.to_ulong() );
       }
//end B-Go--------------------------------------------------------------------------------
//CSR1---------------------------------------------------------------------------
void TTTCvi:: WriteCSR1(unsigned short word )
        {
            VME->Write16(BoardAddress + 0x80,word);
        }

void TTTCvi::OrbitSignalSel(bool s)
        {
           CSR1[3]=s;
           WriteCSR1(CSR1.to_ulong() );
        }

void TTTCvi::SetL1ATriggerSource(int s)
        {
          std::bitset<3> buf(s);

          CSR1[2]=buf[2]; CSR1[1]=buf[1]; CSR1[0]=buf[0];

          WriteCSR1(CSR1.to_ulong() );
        }

void TTTCvi::SetRanTrigRate(int s)
        {
          std::bitset<3> buf(s);

          CSR1[14]=buf[2]; CSR1[13]=buf[1]; CSR1[12]=buf[0];

          WriteCSR1(CSR1.to_ulong() );
        }

void TTTCvi::EventOrbitCountSel(bool s)
        {
          CSR1[15]=s;
          WriteCSR1(CSR1.to_ulong() );
        }

void TTTCvi::L1A_FIFOReset(bool s)
        {
          CSR1[6]=s;
          WriteCSR1(CSR1.to_ulong() );
		  CSR1[6]=0; 
        }

unsigned short TTTCvi::ReadCSR1()
        {
         return VME->Read16(BoardAddress + 0x80);
        }

void TTTCvi::Trigger()
        {
            CSR1[2]=1; CSR1[1]=0; CSR1[0]=0;
            WriteCSR1(CSR1.to_ulong() );
            VME->Write16(BoardAddress + 0x86, 0x000f);
        }
//CSR2  -----------------------------------------------------------------------------
void TTTCvi::WriteCSR2(unsigned short word )
        {
            VME->Write16(BoardAddress + 0x82,word);
        }

void TTTCvi::ResetBGoFIFO(std::bitset<4> b)      //see page 9 on TTC doc.
        {
		 int i;
         for(i=0;i<4;i++) CSR2[i+12]=b[i];
         WriteCSR2(CSR2.to_ulong() );
	     for(i=0;i<4;i++) CSR2[i+12]=0;
        }

void TTTCvi::RetransmiteBGoFIFO(std::bitset<4> b)
        {
         for(int i=0;i<4;i++) CSR2[i+8]=b[i];
         WriteCSR2(CSR2.to_ulong() );
        }

unsigned short TTTCvi::ReadCSR2()
        {
          return VME->Read16(BoardAddress+0x82);
        }

//Inhibit------------------------------------------------------------------------
void TTTCvi::SetInhibit0(unsigned short delay, unsigned short duration)
        {
          VME->Write16(BoardAddress + 0x92, delay);     //delay 12 bits
          VME->Write16(BoardAddress + 0x94, duration);  //duration 8bits
        }

void TTTCvi::SetInhibit1(unsigned short delay, unsigned short duration)
        {
          VME->Write16(BoardAddress + 0x9A, delay);
          VME->Write16(BoardAddress + 0x9C, duration);
        }

void TTTCvi::SetInhibit2(unsigned short delay, unsigned short duration)
        {
          VME->Write16(BoardAddress + 0xA2, delay);
          VME->Write16(BoardAddress + 0xA4, duration);
        }

void TTTCvi::SetInhibit3(unsigned short delay, unsigned short duration)
        {
          VME->Write16(BoardAddress + 0xAA, delay);
          VME->Write16(BoardAddress + 0xAC, duration);
        }
//end Inhibit-------------------------------------------------------------------
void TTTCvi::SetTRIGWORD(int  ad, int sad, bool ie, bool size) //only for TTCvi MKII
        {
          VME->Write16(BoardAddress + 0xC8, ad);
          std::bitset<6> sadbitset(sad);
          std::bitset<10> buf;
          buf[0]=0; buf[1]=0; //set by TTCvi to marc cycle number
          for(int i=0;i<6;i++)
            buf[i+2]=sadbitset[i];
          buf[8]=ie;
          buf[9]=size;
          VME->Write16(BoardAddress + 0xCA, buf.to_ulong());
        };

void TTTCvi::Reset()   //resets TTCvi
        {
	        VME->Write16(BoardAddress + 0x84, 0);
        }

unsigned long TTTCvi::ReadL1ACounter()     //Event/Orbit Counter
        {
           unsigned short  msb = VME->Read16(BoardAddress+0x88);
           unsigned short  lsb= VME->Read16(BoardAddress+0x8a);
           unsigned long counter = (msb<<16) + lsb;
           return  counter;
        }

void TTTCvi::WriteL1ACounter(unsigned short msb,unsigned short lsb)
        {
            VME->Write16(BoardAddress+0x88,msb);
            VME->Write16(BoardAddress+0x8a,lsb);
        }

// TTCrx - asynchronous cycle
void TTTCvi::WriteRX( unsigned short rx_addr,         //long asynchronous cycle with commad for rx
                  unsigned short subaddr, unsigned short data )
        {
            VME->Write16( BoardAddress + 0xc0,
                          0x8000 + ((rx_addr & 0x3fff)<<1) );

            VME->Write16( BoardAddress + 0xc2,
                          ((subaddr & 0xff)<<8) + (data & 0xff) );
        }


void TTTCvi::WriteExternal( unsigned short rx_addr,  //long asynchronous cycle with commad for pararel exterenal bus on rx
                  unsigned short subaddr, unsigned short data )
        {
            VME->Write16( BoardAddress + 0xc0,
                          0x8001 + ((rx_addr & 0x3fff)<<1) );

            VME->Write16( BoardAddress + 0xc2,
                          ((subaddr & 0xff)<<8) + (data & 0xff) );
        }
void TTTCvi::WriteShortAsynchronous(unsigned short command)
        {
            VME->Write16(BoardAddress + 0xc4, command);   // command - 8 bits
        }
