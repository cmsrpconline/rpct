//C++ headers
#include <iostream>
#include <time.h>
#include <sys/time.h>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

//rpctc headers
#include "rpct/devices/System.h"
#include "rpct/devices/XmlSystemBuilder.h"
#include "rpct/hardwareTests/RpctSystem.h"
#include "rpct/hardwareTests/DiagReadWriter.h"
#include "rpct/hardwareTests/TestPulsesManager.h"
#include "rpct/devices/tcsort.h"
#include "rpct/devices/fsb.h"
#include "rpct/devices/ConfigurationSetImpl.h"
#include "rpct/devices/RmbSettingsImpl.h"

//logging
#include "log4cplus/consoleappender.h"
#include <log4cplus/configurator.h>

//utilities
#include <boost/timer.hpp>
#include <boost/dynamic_bitset.hpp>

#ifdef TCC_SOFTWARE
//external
#include "TTCci.hh"
using namespace ttc;
#include "CAENLinuxBusAdapter.hh"
#endif

using namespace std;
using namespace HAL;
using namespace rpct;
using namespace log4cplus;
using namespace boost;

const unsigned long secBx = 40000000;

unsigned int ttcCommand = 0x4;

const IDiagCtrl::TTriggerType pulserStartTrigger = IDiagCtrl::ttBCN0; //ttBCN0 ttPretrigger0
const IDiagCtrl::TTriggerType diagStartTrigger = IDiagCtrl::ttBCN0;

IDiagnosable::TTriggerDataSel triggerDataSel = IDiagnosable::tdsNone;

const unsigned long long  pulserLimit = secBx * 1000; //secBx * 1000; //256; // how many clocks pulser will work
const bool                pulserRepeatEna = false;

const unsigned long  readoutLimit = 128;


//const unsigned long       pulseLength = 256; // lenght of pulse pattern 256
//AK const unsigned long       pulseLength = 30; // lenght of pulse pattern 256
const unsigned long       pulseLength = 16; // lenght of pulse pattern 256

const unsigned long       daqDataDelay = 0;

rpct::HardwareItemType pulsesSource = Opto::TYPE;
// Opto::TYPE Pac::TYPE  GbSort::TYPE TTCSortTCSort::TYPE	 THsbSortHalf::TYPE TFsbSortFinal

unsigned int pulseTarget = 0; //0 - chip out, 1 - chip in

int main(int argc, char** argv){

  PropertyConfigurator config(LOG4CPLUS_TEXT("log4cplus.properties"));
  config.configure();
  //Logger::Logger::getInstance("TTb3").setLogLevel(DEBUG_LOG_LEVEL); FATAL_LOG_LEVEL
  
  SharedAppenderPtr myAppender(new ConsoleAppender());
  myAppender->setName("myAppenderName");
  std::auto_ptr<Layout> myLayout = std::auto_ptr<Layout>(new log4cplus::TTCCLayout());
  myAppender->setLayout( myLayout );
  Logger::getInstance("RpctSystem").addAppender(myAppender);
  Logger::getInstance("TTb3Device").addAppender(myAppender);
  Logger::getInstance("THsb").addAppender(myAppender);
  Logger::getInstance("TDiagCtrl").addAppender(myAppender);
  Logger::getInstance("XmlSystemBuilder").addAppender(myAppender);
  Logger::getInstance("TIIDevice").addAppender(myAppender);
  Logger::getInstance("XmlBxDataReader").addAppender(myAppender);
  Logger::getInstance("TestPulsesManager").addAppender(myAppender);
  Logger::getInstance("RPCDataStream").addAppender(myAppender);
  Logger::getInstance("RPCBxData").addAppender(myAppender);
  Logger::getInstance("TriggerBoardDevice").addAppender(myAppender);  
  Logger::getInstance("THsbDevice").addAppender(myAppender); 

  Logger::Logger::getRoot().setLogLevel(INFO_LOG_LEVEL);

  try {
    System& system = System::getInstance();
    XmlSystemBuilder(system).build("system-tc.xml"); 
  		 
    RpctSystem rpctSystem;
  
    char yn;
    cout<<endl<<"Init devices (reset TTCrxs and QPLLs)? ";
    cin>>yn;
    if(yn == 'y') {
    	const System::BoardMap& boardMap = system.getBoardMap(); 
    	for(System::BoardMap::const_iterator itBoard = boardMap.begin(); itBoard != boardMap.end(); itBoard++) {
    		itBoard->second->init();  
    		itBoard->second->selfTest();  		
    	}
    }
    
    for(unsigned int iHsb = 0; iHsb < rpctSystem.getHsbSortsVec().size(); iHsb++) {
		boost::dynamic_bitset<> enabledInputs(8, 0x0ul);
		if(iHsb == 0) {
			enabledInputs[0] = true;
			enabledInputs[1] = true;
			rpctSystem.getHsbSortsVec()[iHsb]->SetEnabledInputs(enabledInputs);
		}
		else {
			rpctSystem.getHsbSortsVec()[iHsb]->SetEnabledInputs(enabledInputs);	
		}
	} 
    
    cout<<endl<<"reset and configure devices? ";
    cin>>yn;
    if(yn == 'y') {    	
    	const System::DeviceMap& deviceMap = system.getDeviceMap(); 
    	for(System::DeviceMap::const_iterator it = deviceMap.begin(); it != deviceMap.end(); it++) {
    		it->second->reset();    	
    		cout<<it->second->getDescription()<<" reseted"<<endl;
    	}
    	
    	ConfigurationSetImpl settings;	
    	int preTriggerVal = 2;
    	int postTriggerVal = 4;
    	int dataDelay = 0;
    	int trgDelay = 0;
    	
    	dynamic_bitset<> maskedLinks(18, 0ul);
    		
    	maskedLinks.reset();
		maskedLinks[4] = true;
		maskedLinks[5] = true;	
		DeviceSettingsPtr rmb1611(new RmbSettingsImpl(maskedLinks.to_ulong(), preTriggerVal, postTriggerVal, dataDelay, trgDelay));
		settings.addDeviceSettings(1611, rmb1611);

		maskedLinks.reset();
		maskedLinks[9] = true;
		maskedLinks[16] = true;
		maskedLinks[17] = true;
		DeviceSettingsPtr rmb1711(new RmbSettingsImpl(maskedLinks.to_ulong(), preTriggerVal, postTriggerVal, dataDelay, trgDelay));
		settings.addDeviceSettings(1711, rmb1711);
		
		maskedLinks.reset();
		maskedLinks[3] = true;
		maskedLinks[4] = true;
		maskedLinks[5] = true;
		maskedLinks[6] = true;
		DeviceSettingsPtr rmb1811(new RmbSettingsImpl(maskedLinks.to_ulong(), preTriggerVal, postTriggerVal, dataDelay, trgDelay));
		settings.addDeviceSettings(1811, rmb1811);    	
    	system.configureSelected(settings);    	
    }
            
    cout<<"enable transmission check? ";
    cin>>yn;
    if(yn == 'y') {	
   	 	bool enableBCNcheck = true;
   	 	bool enableDataCheck = true;
      	rpctSystem.enableTransmissionCheck(enableBCNcheck, enableDataCheck);	
      	rpctSystem.resetRecErrorCnt();	
      	cout<<"RecErrorCnt reset"<<endl;
    }
    else  
    	rpctSystem.enableTransmissionCheck(false, false);	

    TestPulsesManager testPulsesManager("data/testBxData-simple.xml");    //testpulses testBxData optoPulses.xml
    //TestPulsesManager testPulsesManager("testBxDataOpt.xml");    //testpulses testBxData
    testPulsesManager.configurePulsers(pulsesSource, pulseTarget, pulseLength, pulserLimit, pulserStartTrigger, pulserRepeatEna);
    
	System::HardwareItemList readoutChipList;
	const System::HardwareItemList& optoList = system.getHardwareItemsByType(Opto::TYPE);
	const System::HardwareItemList& pacList = system.getHardwareItemsByType(Pac::TYPE);
	const System::HardwareItemList& rmbList = system.getHardwareItemsByType(Rmb::TYPE);
	const System::HardwareItemList& tbGbList = system.getHardwareItemsByType(GbSort::TYPE);
	const System::HardwareItemList& tcGbList = system.getHardwareItemsByType(TTCSortTCSort::TYPE);	
	const System::HardwareItemList& halfGbList = system.getHardwareItemsByType(THsbSortHalf::TYPE);
	const System::HardwareItemList& finalGbList = system.getHardwareItemsByType(TFsbSortFinal::TYPE);
	
	
	readoutChipList.insert(readoutChipList.end(), pacList.begin(), pacList.end());
	//readoutChipList.insert(readoutChipList.end(), optoList.begin(), optoList.end());
	//readoutChipList.insert(readoutChipList.end(), rmbList.begin(), rmbList.end());
	//readoutChipList.insert(readoutChipList.end(), tbGbList.begin(), tbGbList.end());
	//readoutChipList.insert(readoutChipList.end(), tcGbList.begin(), tcGbList.end());
	//readoutChipList.insert(readoutChipList.end(), halfGbList.begin(), halfGbList.end());
    //readoutChipList.insert(readoutChipList.end(), finalGbList.begin(), finalGbList.end());
    
	testPulsesManager.configureReadouts(rmbList, -1, 10, secBx * 1000, IDiagCtrl::ttLocal, triggerDataSel, 0);
	
	testPulsesManager.startReadouts(rmbList);
	
    testPulsesManager.configureReadouts(pacList, -1, daqDataDelay, readoutLimit, diagStartTrigger, triggerDataSel, 0);

    testPulsesManager.stopReadouts(rmbList);
    
    
    testPulsesManager.startPulsers(pulsesSource);
    
    testPulsesManager.startReadouts(readoutChipList);

    #ifdef TTC_SOFTWARE
    //::system("ttccommand.exe"); 
    CAENLinuxBusAdapter bus(CAENLinuxBusAdapter::V2718, 1); 
	TTCci myCI(bus, 8);
    myCI.SendShortBGODataFromVME(ttcCommand);
    #endif	
    	
    cout<<"Done"<<endl;
    
    cout<<"hit a key to stop ";
    char a;
    cin>>a;
    
    while (! (testPulsesManager.checkCountingEnded(readoutChipList, "DAQ_DIAG_CTRL") ) ) 
      usleep(500000);
      
    cout<<"DiagnosticReadout Counting Ended"<<endl;
           
    testPulsesManager.stopReadouts(readoutChipList);

    testPulsesManager.stopPulsers(pulsesSource);	 			

    cout<<"Pulser stopped"<<endl;                    
       
    vector<TStandardDiagnosticReadout*> diagReadoutsVec;
    
    readoutChipList.insert(readoutChipList.end(), rmbList.begin(), rmbList.end()); ///zeby wypisac
    
    for (System::HardwareItemList::const_iterator iItem = readoutChipList.begin(); iItem != readoutChipList.end(); ++iItem) { 
    	IDiagnosable* chip = dynamic_cast<IDiagnosable*>(*iItem);    	
    	IDiagnosable::TDiagVector& diagVector = chip->GetDiags();
    	unsigned int i = 0;
    	for(; i < diagVector.size(); i++) {
    		if(diagVector[i]->GetName() == "READOUT") {
    			TStandardDiagnosticReadout* diagRead = dynamic_cast<TStandardDiagnosticReadout*>(diagVector[i]); 
				diagReadoutsVec.push_back(diagRead);
    			break;
    		}
    	}
    	if(i == diagVector.size()) {
    		throw TException("READOUT not foud on a chip");
    	}
    }

	 		
    ofstream out("./PacReadoutKB.txt");			
    //DiagReadWriter diagReadWriter(&cout, diagReadoutsVec);  
    DiagReadWriter diagReadWriter(&out, diagReadoutsVec);			
        
    diagReadWriter.enableEVNCheck = false;
    diagReadWriter.writeEvents();  
    out.close();                     
                     
    rpctSystem.printRecErrorCnt();
            
  }
  catch (TException& e) {
    cerr << e.what() << endl;
  }
  catch (exception& e){
    cerr << e.what() << endl;
  }
  catch (...) {
    cerr << "Unknown exception" << endl;
  }
  
}
