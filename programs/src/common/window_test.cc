#include <iostream>
#include <time.h>
#include <sys/time.h>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>/*
#include "rpct/devices/TSystem.h"
#include "rpct/lboxaccess/FecManagerImpl.h"
#include "rpct/devices/TCrate.h"*/
#include "rpct/devices/LinkBoard.h"
#include "rpct/devices/System.h"
#include "rpct/devices/VmeCrate.h"
#include "rpct/devices/XmlSystemBuilder.h"
#include <boost/timer.hpp>


using namespace std;
using namespace HAL;
using namespace rpct;


int main(int argc, char** argv)
{  
/*  PropertyConfigurator config(LOG4CPLUS_TEXT("log4cplus.properties"));
  config.configure();
  //Logger::Logger::getInstance("TTb2").setLogLevel(DEBUG_LOG_LEVEL);
  Logger::Logger::getRoot().setLogLevel(FATAL_LOG_LEVEL);*/
  
  try {
    //G_CCUDebug = true;
    G_Log.out(cout);
    //TDiagCtrl::SetTracking(true);
    
    System& system = System::getInstance();
    XmlSystemBuilder(system).build("system-lbox.xml");
    system.checkVersions();
    
        
//boards configuration
    //slave 0
    IDiagCtrl::TTriggerType triggerType = IDiagCtrl::ttBCN0; //ttL1A; ttManual
    
    std::vector<SynCoder*> syncsVec;
    
    std::vector<LinkBoard*> lbsVec;
    
    cout<<"LB ID?";
    unsigned int lbID;
    cin>>lbID;
    LinkBoard& lb3 = dynamic_cast<LinkBoard&>(system.getBoardById(lbID));   
       
    cout << "Using " << lb3.getDescription() << endl;
    syncsVec.push_back(&lb3.getSynCoder());

    cout<<"Init boards? ";
    char yn = 'y';
    cin>>yn;
       
    if(yn == 'y') {
	    lb3.Initialize();
	    for(u_int iS = 0; iS < syncsVec.size(); iS++) {
	    	cout<<"BITS_CLOCK_WIN_OPEN_TEST"<<endl;
	    	for(int i = 0; i < 4; i++)
	    		cout<<syncsVec[iS]->ReadBits(SynCoder::BITS_CLOCK_WIN_OPEN_TEST)<<endl;
	    		
	    	cout<<"BITS_CLOCK_WIN_CLOSE_TEST"<<endl;
	    	for(int i = 0; i < 4; i++)
	    		cout<<syncsVec[iS]->ReadBits(SynCoder::BITS_CLOCK_WIN_CLOSE_TEST)<<endl;
	    }
	    
	    for(u_int iS = 0; iS < syncsVec.size(); iS++) {
	        syncsVec[iS]->SetLMuxInEna(0, 0, 0);
	        syncsVec[iS]->SetLMuxInDelay(0, 0, 0);
	        if(1) { //master
	            syncsVec[iS]->SetSlaveOrMaster(0); //1 = slave
	            syncsVec[iS]->WriteBits(SynCoder::BITS_GOL_DATA_SEL, 0);
	            //syncsVec[iS]->SynchGOL();
	            syncsVec[iS]->WriteBits(SynCoder::BITS_GOL_LASER_ENA, 1);        
	        }
	    }
    }
    
    lbsVec.push_back(&lb3);
//end boards configuration
    
    unsigned long long pulserLimit = 0x4fffffff; // how many clocks pulser will work
    const unsigned long secBx = 40000000;
    
    const unsigned long pulseLen = 256; // lenght of pulse pattern
    unsigned long pulseData[pulseLen];
    unsigned long val;
    for (unsigned long i = 0; i < pulseLen; i++) {
        /*val = 1 << (i % 8);
        pulseData[i] = (val) |
                       (val << 8);
                      // | (val << 16) 
                      // | (val << 24);
*/        pulseData[i] = 0x0;
    }
    
//    for (unsigned long i = 0; i < pulseLen; i++) {
//        if(i%8 < 4)
//            pulseData[i] = 0xffffff;
//        else
//            pulseData[i] = 0;
//    }

    for (unsigned long i = 0; i < 1; i++) {
        pulseData[i] = 0xffffff;
    }
    
    for (unsigned long i = 0; i < 1; i++) {
        pulseData[i+50] = 0xffffff;
    }
    
    for (unsigned long i = 0; i < 1; i++) {
        pulseData[i+100] = 0xffffff;
    }
        
// set masks - allow all strips
    unsigned char masks[12] = { 0 };
    for (int i = 0; i < 12; i++) {
        masks[i] = 0xff;
    }
       
    for(u_int iS = 0; iS < syncsVec.size(); iS++) {
        syncsVec[iS]->WriteWord(SynCoder::WORD_CHAN_ENA, 0, masks);
        
        //pulsers configuration 
        syncsVec[iS]->WriteBits(SynCoder::BITS_PULSER_OUT_ENA, 1);
        syncsVec[iS]->WriteArea(SynCoder::AREA_MEM_PULSE, pulseData, 0, pulseLen);
        syncsVec[iS]->WriteWord(SynCoder::WORD_PULSER_LENGTH, 0, pulseLen - 1);     
        
        cout<<"reading AREA_MEM_PULSE sync3"<<endl;
        syncsVec[iS]->ReadArea(SynCoder::AREA_MEM_PULSE, pulseData, 0, pulseLen); 
        for (unsigned long i = 0; i < pulseLen; i++) {
            cout << "pulseData["<<setw(3)<<i<<"] = " << hex << pulseData[i] << endl;
        }  
        //end of pulsers configuration        
    }
    
    int trigDataSel = 0;
//            cout<<"trigDataSel? "<<endl;
//            cin>>trigDataSel;
    

    for(u_int iS = 0; iS < syncsVec.size(); iS++) {
        syncsVec[iS]->WriteBits(SynCoder::BITS_STATUS_TRG_DATA_SEL, trigDataSel);
        syncsVec[iS]->WriteBits(SynCoder::BITS_STATUS_EDGE_PULSE, 0);
        syncsVec[iS]->WriteBits(SynCoder::BITS_STATUS_FULL_OUT, 0);
        //syncsVec[iS]->WriteBits(SynCoder::BITS_STATUS_TRG_DATA_SEL, trigDataSel); LOOP_ENA
    }
    
    unsigned long long timeLimit;
    cout << "how long should one cycle work [10s *]" << endl;
    cin >> timeLimit;
    pulserLimit = timeLimit * 0.1* secBx;
    
    const unsigned long long readoutLimit =  pulserLimit + secBx; // how many clock histograms will work 
 
    for(u_int iS = 0; iS < syncsVec.size(); iS++) {
        syncsVec[iS]->GetPulserDiagCtrl().Reset();  
        syncsVec[iS]->GetFullRateHistoMgr().Reset();
        syncsVec[iS]->GetFullRateHistoMgr().GetDiagCtrl().Reset();
        syncsVec[iS]->GetWinRateHistoMgr().Reset();
    }
                                    
    for(u_int iS = 0; iS < syncsVec.size(); iS++) {
             //pulsers configuration 
        syncsVec[iS]->GetPulserDiagCtrl().Configure(pulserLimit + secBx, triggerType);    //<<<<<<<<<<< + secBx
        //histos config
        syncsVec[iS]->GetFullRateHistoMgr().GetDiagCtrl().Configure(pulserLimit, triggerType);
    } 

    int winStepsCnt = 60;
    int channelsCnt = 96;
    vector<vector<double> > winOEffTab(winStepsCnt, vector<double>(channelsCnt, 0)); //[iWinPos][iChanel]
    vector<vector<double> > winCEffTab(winStepsCnt, vector<double>(channelsCnt, 0)); //[iWinPos][iChanel]
    vector<int> winPos(winStepsCnt, 0); //[iWinPos]
           
           
    int winOInit, winCInit, winStep;
            
    char c;        
    while (!cin.eof()) {
        cout<<"winO - o, winC - c, quit - q, t- test"<<endl;
        cin>>c;
        if(c == 'q' || (c != 'o' && c != 'c' && c != 't'))
            break;
        else if(c == 'o' || c == 'c') {
            cout<<"initial winO ";
            cin>>winOInit;
            cout<<"initial winC ";
            cin>>winCInit;
            cout<<"winStepsCnt ";
            cin>>winStepsCnt;
            cout<<"winStep ";
            cin>>winStep;
            cout<<"invert clock ";
            char clkInv;
            cin>>clkInv;
            if(clkInv == 'y') {
                for(int iS = 0; iS < syncsVec.size(); iS++) 
                    syncsVec[iS]->WriteBits(SynCoder::BITS_STATUS_INV_CLOCK, 1);               
            }
            else {
                for(int iS = 0; iS < syncsVec.size(); iS++) 
                    syncsVec[iS]->WriteBits(SynCoder::BITS_STATUS_INV_CLOCK, 0);               
            }
        
            int winO, winC;
            for (int iWin = 0; iWin < winStepsCnt; iWin++) {
                for(u_int iS = 0; iS < syncsVec.size(); iS++) {  
                    syncsVec[iS]->GetFullRateHistoMgr().Reset();
                    syncsVec[iS]->GetWinRateHistoMgr().Reset();
                }
                     
                if(c == 'o') {
                    winO = winOInit + iWin*winStep;
                    winC = winCInit;
                }
                
                else if(c == 'c') {
                    winO = winOInit;
                    winC = winCInit + iWin*winStep;
                }
                
                for(u_int iLB = 0; iLB < syncsVec.size(); iLB++) {                
                                               //Desc1, Decs2             
                    lbsVec[iLB]->SetTTCrxDelays(winC, winO, 0, 0);                
                }
                  
                if(c == 'o')  
                    winPos[iWin] = winO;
                else if(c == 'c')
                    winPos[iWin] = winC;
                    
                cout<<"winO " <<winO<<" winC "<<winC<<endl;                                                            
        
                for(u_int iS = 0; iS < syncsVec.size(); iS++) {
                    syncsVec[iS]->GetPulserDiagCtrl().Start();                                         
                    syncsVec[iS]->GetFullRateHistoMgr().GetDiagCtrl().Start();                                     
                }
                 
                //::system("TTCciCommand.exe");
        		//::system("ttccommand.exe");

                         
                while (!syncsVec[0]->GetFullRateHistoMgr().GetDiagCtrl().CheckCountingEnded()) {
                    usleep(100000);
                    cout << "*" << flush;
                }
                    
                for(u_int iS = 0; iS < syncsVec.size(); iS++) {
                    syncsVec[iS]->GetPulserDiagCtrl().Stop();                                         
                    syncsVec[iS]->GetFullRateHistoMgr().GetDiagCtrl().Stop();                     
                }
                   
                for(u_int iS = 0; iS < syncsVec.size(); iS++) { 
                    syncsVec[iS]->GetFullRateHistoMgr().Refresh();
                    syncsVec[iS]->GetWinRateHistoMgr().Refresh();
                    
                    int binCount = syncsVec[iS]->GetFullRateHistoMgr().GetBinCount();
                    cout<<"fullHisto "<<"winHisto "<<endl;
                    for (int iCh = 0; iCh < binCount; iCh++) {
                        int fullBinCnt = syncsVec[iS]->GetFullRateHistoMgr().GetLastData()[iCh];
                        int winBinCnt = syncsVec[iS]->GetWinRateHistoMgr().GetLastData()[iCh];
                        if (fullBinCnt > 0) {
                            cout << dec << setw(3) << iCh << ": " << dec 
                            << setw(8) << fullBinCnt << setw(8) << winBinCnt<<setw(8)<<" ";
                            if(c == 'o') {
                                winOEffTab[iWin][iCh] = ((double)winBinCnt)/fullBinCnt;
                                cout <<" "<< winOEffTab[iWin][iCh]<<endl;
                            }
                            else if(c == 'c') {
                                winCEffTab[iWin][iCh] = ((double)winBinCnt)/fullBinCnt;
                                cout <<" "<< winCEffTab[iWin][iCh]<<endl;
                            }                         
                        }                                        
                    }                
                }                                      
            }
            cout << "Done" << endl;
            ofstream outFile;
            ostringstream ostr;            
            if(c == 'o') {
            	ostr<<"lb_"<<lbID<<"_winO.txt";
                outFile.open(ostr.str().c_str());
                outFile<<"winC "<<winC<<" clkInv "<<clkInv<<endl;
            }
            else if(c == 'c') {
                ostr<<"lb_"<<lbID<<"_winC.txt";
                outFile.open(ostr.str().c_str());
                outFile<<"winO "<<winO<<" clkInv "<<clkInv<<endl;
            }
            outFile<<"\t";
            for (u_int iW = 0; iW < winOEffTab.size(); iW++) {
                outFile<<setw(12)<<winPos[iW];
            }
            outFile<<endl;
            for (u_int iCh = 0; iCh < winOEffTab[0].size(); iCh++) {
                outFile<<iCh<<"\t";
                for (u_int iW = 0; iW < winOEffTab.size(); iW++) {
                    if(c == 'o')
                        outFile<<setw(12)<<winOEffTab[iW][iCh];
                    else if(c == 'c')
                        outFile<<setw(12)<<winCEffTab[iW][iCh];
                }           
                outFile<<endl; 
            }
            
            outFile.close();
        }
        else if(c == 't') {       
            for(u_int iS = 0; iS < syncsVec.size(); iS++) {
             //pulsers configuration 
                syncsVec[iS]->GetPulserDiagCtrl().Reset(); 
                syncsVec[iS]->GetPulserDiagCtrl().Configure(500 * secBx, IDiagCtrl::ttManual); 
            }
            for(u_int iS = 0; iS < syncsVec.size(); iS++) {
               syncsVec[iS]->GetPulserDiagCtrl().Start();   
            }
            cout<<"pulser started "<<endl;
            while (!cin.eof()) {
                cout<<"continue y/n ";                
                cin>>c;
                if(c == 'n')
                    break;
                int winO, winC;
                cout<<"winO ";
                cin>>winO;
                cout<<"winC ";
                cin>>winC;
                cout<<"invert clock ";
                char clkInv;
                cin>>clkInv;
                if(clkInv == 'y') {
                    for(u_int iS = 0; iS < syncsVec.size(); iS++) 
                        syncsVec[iS]->WriteBits(SynCoder::BITS_STATUS_INV_CLOCK, 1);               
                }
                else {
                    for(u_int iS = 0; iS < syncsVec.size(); iS++) 
                        syncsVec[iS]->WriteBits(SynCoder::BITS_STATUS_INV_CLOCK, 0);               
                }
                for(u_int iLB = 0; iLB < syncsVec.size(); iLB++) {                
                                               //Desc1, Decs2             
                    lbsVec[iLB]->SetTTCrxDelays(winC, winO, 0, 0);                
                }
            }
            for(u_int iS = 0; iS < syncsVec.size(); iS++) {
                syncsVec[iS]->GetPulserDiagCtrl().Stop(); 
            }
            cout<<"pulser stoped "<<endl;                    
        }
    }
  }
  catch (TException& e) {
     cerr << e.what() << endl;
  }
catch (exception& e){
    cerr << e.what() << endl;
}
catch (...) {
    cerr << "Unknown exception" << endl;
}  
}


