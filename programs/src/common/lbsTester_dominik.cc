#include <iostream>
#include <time.h>
#include <sys/time.h>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "rpct/lboxaccess/FecManagerImpl.h"
#include "rpct/devices/System.h"
#include "rpct/devices/XmlSystemBuilder.h"
#include "rpct/devices/LinkBoard.h"
#include "rpct/hardwareTests/LBTester.h"
#include "rpct/hardwareTests/RpctSystem.h"
//#include "rpct/devices/TCrate.h"
#include <boost/timer.hpp>
#include <log4cplus/configurator.h>

//#include "TTCci.hh"
//using namespace ttc;
//#include "CAENLinuxBusAdapter.hh"

using namespace std;
using namespace HAL;
using namespace rpct;
using namespace log4cplus;

/*const int LB_IN_CHANNLES_CNT = 96;
const int CSC_DATA_SIZE = 16; //bits

enum DiagReadExtDataType {
    NONE,
    CSC,
    LMUX,
    CODER,
    SLAVE0,
    SLAVE1
};*/


int main(int argc, char** argv)
{ 
	PropertyConfigurator config(LOG4CPLUS_TEXT("log4cplus.properties"));
	config.configure();
	//Logger::Logger::getInstance("TTb2").setLogLevel(DEBUG_LOG_LEVEL);
	Logger::Logger::getRoot().setLogLevel(FATAL_LOG_LEVEL);

	try {
		System& system = System::getInstance();
		XmlSystemBuilder(system).build("system-lbox.xml"); 

		RpctSystem rpctSystem;
		int winOInit = 0;
		int winCInit = 0;
		int winStep = 1;
		LBTester lbTester(LBTester::testBoards, rpctSystem);

		/*   	cout<<"lbTester.rpcInputsTestWithReadout(1)"<<endl;
    lbTester.rpcInputsTestWithReadout(1); 

    cout<<"lbTester.rpcInputsTestWithCounters()"<<endl;
    lbTester.rpcInputsTestWithCounters(3);*/

		cout<<"lbTester.windowTest()"<<endl;
		//lbTester.windowTestWithFEBs(0.25, LBTester::testWinO);  
		lbTester.windowTest(0.1, LBTester::testWinC, winOInit, winCInit, winStep); 
		//lbTester.histosTest(0.25);
		/*    cout<<"lbTester.slaveMasterTest()"<<endl;
    lbTester.slaveMasterTest(10);*/

		lbTester.coutResults();
		lbTester.saveResults("lbTesterResults.txt");				
	}
	catch (TException& e) {
		cerr << e.what() << endl;
	}
	catch (exception& e){
		cerr << e.what() << endl;
	}
	catch (...) {
		cerr << "Unknown exception" << endl;
	} 

}
