//C++ headers
#include <iostream>
#include <time.h>
#include <sys/time.h>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

//rpctc headers
#include "rpct/devices/System.h"
#include "rpct/devices/XmlSystemBuilder.h"
#include "rpct/hardwareTests/DiagReadWriter.h"
#include "rpct/hardwareTests/TestPulsesManager.h"
#include "rpct/devices/LinkBoard.h"

//logging
#include "log4cplus/consoleappender.h"
#include <log4cplus/configurator.h>
   
//utilities
#include <boost/timer.hpp>

//external
#include "TTCci.hh" 
using namespace ttc;
#include "CAENLinuxBusAdapter.hh"

using namespace std;
using namespace HAL;
using namespace rpct;
using namespace log4cplus;

const unsigned long secBx = 40000000;

unsigned int ttcCommand = 0x4;

const IDiagCtrl::TTriggerType pulserStartTrigger = IDiagCtrl::ttPretrigger0; //ttBCN0 ttPretrigger0
const IDiagCtrl::TTriggerType diagStartTrigger = IDiagCtrl::ttPretrigger0;

IDiagnosable::TTriggerDataSel triggerDataSel = IDiagnosable::tdsNone;

const unsigned long long  pulserLimit =  secBx * 1000; //256; // how many clocks pulser will work
const bool                pulserRepeatEna = false;

const unsigned long  readoutLimit = 256;


//const unsigned long       pulseLength = 256; // lenght of pulse pattern 256
//AK const unsigned long       pulseLength = 30; // lenght of pulse pattern 256
const unsigned long       pulseLength = 126; // lenght of pulse pattern 256

const unsigned long       daqDataDelay = 0;

int main(int argc, char** argv){ 

  PropertyConfigurator config(LOG4CPLUS_TEXT("log4cplus.properties"));
  config.configure();
  //Logger::Logger::getInstance("TTb3").setLogLevel(DEBUG_LOG_LEVEL); FATAL_LOG_LEVEL
  
  SharedAppenderPtr myAppender(new ConsoleAppender());
  myAppender->setName("myAppenderName");
  std::auto_ptr<Layout> myLayout = std::auto_ptr<Layout>(new log4cplus::TTCCLayout());
  myAppender->setLayout( myLayout );
  Logger::getInstance("TIIDevice").addAppender(myAppender);
  Logger::getInstance("SynCoder").addAppender(myAppender);
  Logger::getInstance("LinkBoard").addAppender(myAppender);  
  

  Logger::Logger::getRoot().setLogLevel(INFO_LOG_LEVEL);

  try { 
    System& system = System::getInstance();
    XmlSystemBuilder(system).build("system-lbox.xml");   		 

    cout<<"initialising boards() "<<endl;
	//const System::BoardMap& boardMap = system.getBoardMap(); 
    LinkBoard& linkBoard = dynamic_cast<LinkBoard&>(system.getBoardById(2020));
      
    std::vector<rpct::BigInteger> pulsData;
    for(unsigned int iBx = 0; iBx < pulseLength; iBx++) {
        if(iBx == 10)
            pulsData.push_back(rpct::BigInteger(0xffffff));
        else
            pulsData.push_back(rpct::BigInteger(0ul));
    }
    unsigned int target = 0;
    linkBoard.getSynCoder().getPulser().Configure(pulsData, pulseLength, pulserRepeatEna, target);
    linkBoard.getSynCoder().getPulser().GetDiagCtrl().Reset();
    linkBoard.getSynCoder().getPulser().GetDiagCtrl().Configure(pulserLimit - 1, pulserStartTrigger, 0);          
    
    linkBoard.getSynCoder().getPulser().GetDiagCtrl().Start();
    cout<<"hit a key to stop\n";
    char a;
    cin>>a;
    linkBoard.getSynCoder().getPulser().GetDiagCtrl().Stop();             
  }
  catch (TException& e) {
    cerr << e.what() << endl;
  }
  catch (exception& e){
    cerr << e.what() << endl;
  }
  catch (...) {
    cerr << "Unknown exception" << endl;
  }
  
}
