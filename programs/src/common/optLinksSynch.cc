#include <iostream>
#include <time.h>
#include <sys/time.h>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "rpct/devices/System.h"
#include "rpct/devices/XmlSystemBuilder.h"
#include "rpct/devices/LinkBoard.h"
#include "rpct/hardwareTests/RpctSystem.h"
#include "rpct/hardwareTests/XmlDevicesSettings.h"
#include "rpct/devices/SynCoderSettingsImpl.h"

#include "log4cplus/consoleappender.h"

//#include "rpct/devices/TCrate.h"
#include <boost/timer.hpp>
#include <log4cplus/configurator.h>

#include "TTCci.hh"
using namespace ttc;
#include "CAENLinuxBusAdapter.hh"

using namespace std;
using namespace HAL;
using namespace rpct;
using namespace log4cplus;

int main(int argc, char** argv)
{ 
  PropertyConfigurator config(LOG4CPLUS_TEXT("log4cplus.properties"));
  config.configure();
  //Logger::Logger::getInstance("TTb2").setLogLevel(DEBUG_LOG_LEVEL); FATAL_LOG_LEVEL
  
  SharedAppenderPtr myAppender(new ConsoleAppender());
  myAppender->setName("myAppenderName");
  std::auto_ptr<Layout> myLayout = std::auto_ptr<Layout>(new log4cplus::TTCCLayout());
  myAppender->setLayout( myLayout );
  Logger::getInstance("RpctSystem").addAppender(myAppender);    
  Logger::getInstance("TriggerBoardDevice").addAppender(myAppender);
  Logger::getInstance("TriggerBoard").addAppender(myAppender);    
  Logger::getInstance("XmlDevicesSettings").addAppender(myAppender);   
  Logger::getInstance("System").addAppender(myAppender);   
  Logger::getInstance("LinkBoard").addAppender(myAppender);
  Logger::getInstance("SynCoder").addAppender(myAppender);
  Logger::getInstance("LinkBox").addAppender(myAppender);       
  
  Logger::Logger::getRoot().setLogLevel(INFO_LOG_LEVEL);
  
  try {
    //G_CCUDebug = true;
    //G_Log.out(cout);
    //TDiagCtrl::SetTracking(true);    
               
//boards configuration
   const unsigned long secBx = 40000000;
       
    System& system = System::getInstance();    
    XmlSystemBuilder(system).build("system-lbox.xml"); 
  	XmlSystemBuilder(system).build("system-tc.xml"); 
  		 
    RpctSystem rpctSystem;
    
	char yn;
	try {  		
/*	    cout<<"initialise boards (y/n) ?"<<endl;
	    cin>>yn;
	    if(yn == 'y') {
			const System::BoardMap& boardMap = system.getBoardMap(); 
			for(System::BoardMap::const_iterator itBoard = boardMap.begin(); itBoard != boardMap.end(); itBoard++) {
				itBoard->second->init();  
				itBoard->second->selfTest();  	
		        cout<<itBoard->second->getDescription()<<" initialised"<<endl;	
			}		
	    }*/

		//cout<<" configuring and reseting devices"<<endl;
		cout<<"initialise and configure boards (y/n) ?"<<endl;
		cin>>yn;
		if(yn == 'y') {
			string settingsFileName = "optoSettings.xml";
			XmlDevicesSettings xmlSettings;
			xmlSettings.readFile(settingsFileName);
			xmlSettings.adddefaultDeviceSettings();

			DeviceSettingsPtr lb(new SynCoderSettingsImpl()); 	        
			xmlSettings.addDeviceSettings(lb);
			system.configureSelected(xmlSettings);

			cout<<"devices configured form file "<<settingsFileName<<endl;

			cout<<"Done "<<endl;	
		}
	}
	catch (TException& e ) {
		//LOG4CPLUS_INFO(logger, "error during initialization of "<<lbsVec[iLB]->getDescription()<<" id "<< lbsVec[iLB]->getId());
		cerr<<"error  system.checkVersions()"<<endl;
		cerr << e.what() <<endl;		
		cout<<"continue? (y/n) ";
		char yn;
		cin>>yn;
		if(yn == 'n')
		    throw e; 
    }

	

//end boards configuration
    rpct::HalfBox::MasterLinkBoards& masterLBsVec = rpctSystem.getMasterLBsVec();
	rpct::HalfBox::LinkBoards&       lbsVec = rpctSystem.getLBsVec();	
 
	cout<<"synchronize links? ";
    cin>>yn; 
    if(yn == 'y') {
	 	rpctSystem.synchronizeOptLinks(); 	
		cout<<"continue? ";
		cin>>yn;
		if(yn == 'n') {
			return 0;
		}
    } 
    
    cout<<"enable transmission check? ";
    cin>>yn;
    if(yn == 'y') {	
    	bool enableBCNcheck = true;
   	 	bool enableDataCheck = true;
      	rpctSystem.enableTransmissionCheck(enableBCNcheck, enableDataCheck);	
      	rpctSystem.resetRecErrorCnt();	
      	cout<<"RecErrorCnt reset"<<endl;
    }
    else  
    	rpctSystem.enableTransmissionCheck(false, false);
    
	cout<<"check links with random data? ";
    cin>>yn; 
    if(yn == 'y') {
    	for (unsigned int iMLB = 0; iMLB < masterLBsVec.size(); iMLB++) {
    		masterLBsVec[iMLB]->getSynCoder().writeBits(SynCoder::BITS_SEND_TEST_ENA, 1);
			masterLBsVec[iMLB]->getSynCoder().writeBits(SynCoder::BITS_SEND_TEST_RND_ENA, 1);
    	} 
    	for (unsigned int iTb = 0; iTb < rpctSystem.GetTbsVec().size(); iTb++) {			
			for(Optos::iterator optoIt = rpctSystem.GetTbsVec()[iTb]->GetOptos().begin(); 
			                         optoIt != rpctSystem.GetTbsVec()[iTb]->GetOptos().end(); optoIt++) { 
				if((*optoIt) == NULL)
					continue;
				(*optoIt)->writeWord(Opto::WORD_REC_TEST_ENA, 0, (*optoIt)->GetEnabledOptLinks().to_ulong());
				(*optoIt)->writeWord(Opto::WORD_REC_TEST_RND_ENA, 0, (*optoIt)->GetEnabledOptLinks().to_ulong());
/*				(*optoIt)->writeBits(Opto::BITS_ERROR_RX_ER , (*optoIt)->GetEnabledOptLinks().to_ulong()); //jak 1 to liczy blady z TLK 
				(*optoIt)->writeBits(Opto::BITS_ERROR_RX_DV , (*optoIt)->GetEnabledOptLinks().to_ulong()); //jak 1 to liczy brak DV  (Data Valid) z TLK 
				(*optoIt)->writeBits(Opto::BITS_ERROR_REC_ERR , (*optoIt)->GetEnabledOptLinks().to_ulong());  //jak 1 to liczy bledy analizy danych
*/				
				for(unsigned int iLink = 0; iLink < 3; iLink++) {
					(*optoIt)->readWord(Opto::WORD_REC_TEST_OR_DATA , iLink);  			
				}
				(*optoIt)->writeWord(Opto::WORD_REC_ERROR_COUNT , 0, 0ul); 
			}
		}
		
		cout<<"hit a key to stop ";
		cin>>yn;
   		for (unsigned int iTb = 0; iTb < rpctSystem.GetTbsVec().size(); iTb++) {			
			cout<<rpctSystem.GetTbsVec()[iTb]->getDescription()<<endl;
			for(Optos::iterator optoIt = rpctSystem.GetTbsVec()[iTb]->GetOptos().begin(); 
			                         optoIt != rpctSystem.GetTbsVec()[iTb]->GetOptos().end(); optoIt++) { 
				if((*optoIt) == NULL)
					continue;
				
				cout<<(*optoIt)->getDescription()<<endl;
				cout<<"ERROR_COUNT "<<(*optoIt)->readWord(Opto::WORD_REC_ERROR_COUNT, 0ul)<<endl;
				for(unsigned int iLink = 0; iLink < 3; iLink++) {	
					cout<<"TEST_OR_DATA "<<iLink<<" "<<(*optoIt)->readWord(Opto::WORD_REC_TEST_OR_DATA, iLink)<<endl;
				}
			}
   		}
		
    }        				
  }
  catch (TException& e) {
     cerr << e.what() << endl;
  }
catch (exception& e){
    cerr << e.what() << endl;
}
catch (...) {
    cerr << "Unknown exception" << endl;
} 

}
