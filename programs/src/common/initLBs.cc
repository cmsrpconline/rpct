//C++ headers
#include <iostream>
#include <time.h>
#include <sys/time.h>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

//rpctc headers
#include "rpct/devices/System.h"
#include "rpct/devices/XmlSystemBuilder.h"
#include "rpct/hardwareTests/DiagReadWriter.h"
#include "rpct/hardwareTests/TestPulsesManager.h"
#include "rpct/devices/tcsort.h"

//logging
#include "log4cplus/consoleappender.h"
#include <log4cplus/configurator.h>
   
//utilities
#include <boost/timer.hpp>

//external
#include "TTCci.hh" 
using namespace ttc;
#include "CAENLinuxBusAdapter.hh"

using namespace std;
using namespace HAL;
using namespace rpct;
using namespace log4cplus;

int main(int argc, char** argv){ 

  PropertyConfigurator config(LOG4CPLUS_TEXT("log4cplus.properties"));
  config.configure();
  //Logger::Logger::getInstance("TTb3").setLogLevel(DEBUG_LOG_LEVEL); FATAL_LOG_LEVEL
  
  SharedAppenderPtr myAppender(new ConsoleAppender());
  myAppender->setName("myAppenderName");
  std::auto_ptr<Layout> myLayout = std::auto_ptr<Layout>(new log4cplus::TTCCLayout());
  myAppender->setLayout( myLayout );
  Logger::getInstance("TIIDevice").addAppender(myAppender);
  Logger::getInstance("SynCoder").addAppender(myAppender);
  Logger::getInstance("LinkBoard").addAppender(myAppender);  
  

  Logger::Logger::getRoot().setLogLevel(INFO_LOG_LEVEL);

  try { 
    System& system = System::getInstance();
    XmlSystemBuilder(system).build("system-lbox.xml");   		 
	system.checkVersions();
	
    cout<<"initialising boards() "<<endl;
	const System::BoardMap& boardMap = system.getBoardMap(); 
	for(System::BoardMap::const_iterator itBoard = boardMap.begin(); itBoard != boardMap.end(); itBoard++) {
		try {
			itBoard->second->init();  
			itBoard->second->selfTest();  	
        	cout<<itBoard->second->getDescription()<<" initialised"<<endl;
		}	
		  catch (TException& e) {
		  	cerr<<itBoard->second->getDescription()<<": ";
		    cerr << e.what() << endl;
		  }
		  catch (exception& e){
		  	cerr<<itBoard->second->getDescription()<<": ";
		    cerr << e.what() << endl;
		  }
		  catch (...) {
		  	cerr<<itBoard->second->getDescription()<<": ";
		    cerr << "Unknown exception" << endl;
		  }
	}

    cout<<" reseting devices"<<endl;
    const System::DeviceMap& deviceMap = system.getDeviceMap(); 
    for(System::DeviceMap::const_iterator it = deviceMap.begin(); it != deviceMap.end(); it++) {
        it->second->reset();            
    }
    cout<<"Done "<<endl;
                       
  }
  catch (TException& e) {
    cerr << e.what() << endl;
  }
  catch (exception& e){
    cerr << e.what() << endl;
  }
  catch (...) {
    cerr << "Unknown exception" << endl;
  }
  
}
