/***********************************************************************
*  Karol Bunkowski  2001 (kbunkow@fuw.edu.pl)
*  Warsaw University
*	
*  Class that gives control over TTCvi module 
*  
***********************************************************************/

#ifndef TTCVI_H
#define TTCVI_H

#include "tb_vme.h"
#include <bitset>	//standard C++ library

//Class that provides functions for setting B-Go mode, loading B-Go FIFO with data, etc.
//objects of this class representing four B-Gos are used in TTTCvi class
class B_Go
{
       TVMEInterface* VME;
       unsigned long  BoardAddress;
       
 public:									//TTCvi board address
       B_Go(TVMEInterface* vme,unsigned long  board_address,unsigned short dataAddres, unsigned short settingsAddres,
            unsigned short b_GoSignalAddres)
          : VME(vme), BoardAddress(board_address), DataAddres(dataAddres),
            SettingsAddres(settingsAddres), B_GoSignalAddres(b_GoSignalAddres) {};

       void Settings(bool enable, bool sync, bool single, bool fifo,bool calibration);  //mode selection for B-Go<i>

       std::bitset<5> ReadSettings();  //if compiler have a problem, try to remove "std::"

       void B_GoSignal();

       void WriteDataLong(unsigned short rx_addr, bool E,
                                    unsigned short subaddr, unsigned short data ); //load B-Go FIFO with "long format cycle" ("individually addressed command")

       void WriteDataShort(unsigned short command ); //load B-Go FIFO with "short format cycle" ("broadcast command")
 
       void WriteData(std::bitset<32> word); //load B-Go FIFO with long or short format cycle

 private:
       unsigned short DataAddres;
       unsigned short SettingsAddres;
       unsigned short B_GoSignalAddres;
};


//Class that provides functions controling TTCvi 
class TTTCvi{
        TVMEInterface* VME;
		unsigned long  BoardAddress;
private:
        std::bitset<16> CSR1; //control registers
        std::bitset<16> CSR2;
public:
        B_Go* b_Go[4];  //pointers to four B_Go objects representing four B-Gos

	TTTCvi(TVMEInterface* vme, unsigned long  board_address)
         : VME(vme), BoardAddress(board_address)
          {
           b_Go[0]= new  B_Go(VME,BoardAddress,0xB0,0x90,0x96);  
           b_Go[1]= new  B_Go(VME,BoardAddress,0xB4,0x98,0x9E);
           b_Go[2]= new  B_Go(VME,BoardAddress,0xB8,0xA0,0xA6);
           b_Go[3]= new  B_Go(VME,BoardAddress,0xBC,0xA8,0xAE);
          }

        ~TTTCvi() {
          for (int i=0; i<4;i++) delete b_Go[i];
        }
//CSR1----------------------------------------
        void WriteCSR1(unsigned short word ); //set CSR1 control register
				
				//settings are remembered in CSR1 bitset, always whole register is written
        void SetL1ATriggerSource(int s);  //s= 0 - 7

        void SetRanTrigRate(int s);      //s= 0 - 7

        void OrbitSignalSel(bool s);	//s=1 internal orbit generation, s=0 external orbit

        void EventOrbitCountSel(bool s); //s=0 Event Count, s=1 Orbit count (only in TTCvi MKII)

        void L1A_FIFOReset(bool s);		//if write s =1 (then CSR1<6> is egain set to 0)

        unsigned short ReadCSR1();

       	void Trigger();   //sets CSR1<0...2> to proper value (=4) and generates L1A signal
//CSR1 end-----------------------------------
//CSR2  -------------------------------------
        void WriteCSR2(unsigned short word );
				//settings are remembered in CSR2 bitset, always whole register is written
        void ResetBGoFIFO(std::bitset<4> b);      //see page 9 on TTCvi doc, colling this function with b[i] = 1 resets b-go fifo<i>, (then CSR2[12...15] is set to "0")

        void RetransmiteBGoFIFO(std::bitset<4> b); //if set b[i] to "0" then B-Go<i> FIFO will be retransmited (warrning! bitset is constructed with all bits set to "0"!)

        unsigned short ReadCSR2();

//CSR2 end-----------------------------------
//Inhibit------------------------------------
        void SetInhibit0(unsigned short delay, unsigned short duration);   
                                               //delay: 12 bits   //duration: 8 bits
        void SetInhibit1(unsigned short delay, unsigned short duration);

        void SetInhibit2(unsigned short delay, unsigned short duration);

        void SetInhibit3(unsigned short delay, unsigned short duration);

//end Inhibit------------------------------------
        void SetTRIGWORD(int  ad, int sad, bool ie, bool size);  //only for TTCvi MKII
                                           //seting size to 0 inhibit after-trigger transfer,
                                           //setting ie to 0 is nonsens, it may change TTCrx setings     
		void Reset();   //resets TTCvi

		unsigned long ReadL1ACounter();     //Event/Orbit Counter

		void WriteL1ACounter(unsigned short msb,unsigned short lsb);


// TTCrx - asynchronous cycle
        void WriteRX( unsigned short rx_addr,         //long asynchronous cycle with commad for rx (internal rx registers)
                  unsigned short subaddr, unsigned short data );

        void WriteExternal( unsigned short rx_addr,  //long asynchronous cycle with commad for pararel exterenal bus on rx
                  unsigned short subaddr, unsigned short data );

        void WriteShortAsynchronous(unsigned short command); //command: 8 bits
};

/*
//use this functin to convert descew in ps for proper numbers
//see TTCrx manual Appendix A 
    unsigned short Convert(unsigned short K)
        {
            unsigned short n, m;
            n = K % 15;
            m = (K/15 - n + 14) %16;
            return 16*n + m;
        }
*/

#endif
