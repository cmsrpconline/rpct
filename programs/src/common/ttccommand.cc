#include <memory>
#include <map>
#include "rpct/devices/System.h"
#include "rpct/devices/VmeCrate.h"
#include "rpct/devices/XmlSystemBuilder.h"
//#include "rpct/ttccontr/TTCviN.h"
#include "TTCviN.h"
#include <exception>

using namespace std;
using namespace rpct;


int main(int argc, char** argv)
{ 

    try {
        //G_CCUDebug = true;
        //G_Log.out(cout);
        //G_Debug.SetLevel(TDebug::VME_OPER);
        //G_Debug.SetStream(cout);
        //TSystem system;
    	//LinkSystem& system = LinkSystem::getInstance();
    	//LinkSystem* system = new LinkSystem();
    	LinkSystem system;
        XmlSystemBuilder(system).build("/home/tb/bin/system-ttc.xml");
        system.checkVersions();
        VmeCrate& vmeCrate = dynamic_cast<VmeCrate&>(system.getCrateById(200));    
        TTTCvi ttcvi(vmeCrate.getVme(), 0xdddd00);
        map<string, int> rndRateMap;
        rndRateMap["1"] = 0;
        rndRateMap["100"] = 1;
        rndRateMap["1k"] = 2;
        rndRateMap["5k"] = 3;
        rndRateMap["10k"] = 4;
        rndRateMap["25k"] = 5;
        rndRateMap["50k"] = 6;
        rndRateMap["100k"] = 7;

        if(argc > 1) {
            cout<<argv[1]<<endl;
            if(string(argv[1]) == "-b") {    
                cout<<"sending BC0"<<endl;
                ttcvi.WriteShortAsynchronous(0x1);   
            }
            else if(string(argv[1]) == "-e") {
                cout<<"sending EC0"<<endl;
                ttcvi.WriteShortAsynchronous(0x2);
            }
            else if(string(argv[1]) == "-p0") {
            	cout<<"sending pretigger0 (0x4)"<<endl;
            	ttcvi.WriteShortAsynchronous(0x4);
            }
            else if(string(argv[1]) == "-r") {
                cout<<"sending Resynch"<<endl;
                ttcvi.WriteShortAsynchronous(0x10);
            }
            else if(string(argv[1]) == "-t") {
                if(argc != 3) {
                    cout<<"get second agument after t"<<endl;
                    return 0;
                }
                cout<<argv[2]<<endl;
                if(string(argv[2]) == "vme") { 
                    ttcvi.Trigger();
                    cout<<"sending vme L1A"<<endl;
                }
                else if(string(argv[2]) == "off") { 
                    ttcvi.SetL1ATriggerSource(1);
                    cout<<"L1A NIM 1 (off if nothing copnnected)"<<endl;
                }
                else if(string(argv[2]) == "ecl") { 
                    ttcvi.SetL1ATriggerSource(0);
                    cout<<"L1A ecl (off if nothing copnnected)"<<endl;
                }
                else if(rndRateMap.find(string(argv[2])) != rndRateMap.end() ) {
                    ttcvi.SetL1ATriggerSource(5);   
                    ttcvi.SetRanTrigRate(rndRateMap[string(argv[2])]);
                    cout<<"rnandom L1A, rate "<<argv[2]<<" Hz ("<<rndRateMap[string(argv[2])]<<")"<<endl;
                }
                else {
                    cout<<"the second argument is unknow"<<endl;
                }
            }
            else if(string(argv[1]) == "-bc0") {
            	cout<<"Configuring BC0 BGo"<<endl;

            	std::bitset<4> bResetFifo;
            	bResetFifo[0]=true;
            	bResetFifo[1]=true;
            	bResetFifo[2]=true;
            	bResetFifo[3]=true;
            	ttcvi.ResetBGoFIFO(bResetFifo);

            	ttcvi.OrbitSignalSel(true);
            	ttcvi.SetInhibit0(0, 1);
            	ttcvi.b_Go[0]->Settings(true, false, true, false, false);

            	std::bitset<4> bRetransmite;
            	bRetransmite[0]=false;
            	bRetransmite[1]=false;
            	bRetransmite[2]=false;
            	bRetransmite[3]=false;
            	ttcvi.RetransmiteBGoFIFO(bRetransmite);

            	ttcvi.b_Go[0]->WriteDataShort(0x01);

            }
            else 
                cout<<"the first argument is unknow"<<endl;
        }
        else {
            cout<<"-[option [L1Aoption]]"<<endl;
            cout<<" b - BC0"<<endl;
            cout<<" e - EC0"<<endl;
            cout<<" p0 - pretriger0 (0x4)"<<endl;
            cout<<" r - Resynch (0x10)"<<endl;
            cout<<" t [L1Aoption] - triger; L1Aoption: vme, off, ecl, random triger with rate:"<<endl;
            cout<<" bc0 - configures the TTCvi for sending repetitive BC0 signal"<<endl;
            for(map<string, int>::iterator it = rndRateMap.begin(); it != rndRateMap.end(); it++) {
                cout<<it->first<<" ";
            }
            cout<<endl;
        }
        return 0;
    }
    catch (TException& e) {
        cerr << e.what() << endl;
    }
    catch (std::exception& e){
        cerr << e.what() << endl;
    }
    catch (...) {
        cerr << "Unknown exception" << endl;
    }

}


