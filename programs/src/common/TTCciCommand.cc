#include <iostream>
#include <time.h>
#include <sys/time.h>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>


#include "TTCci.hh"
using namespace ttc;
#include "CAENLinuxBusAdapter.hh"

using namespace std;
using namespace HAL;

int main(int argc, char** argv) {
	try	{ 
    	unsigned int command = 0x4;
    	CAENLinuxBusAdapter bus(CAENLinuxBusAdapter::V2718, 1); 
		TTCci myCI(bus, 8);
    	myCI.SendShortBGODataFromVME(0x1);
    	myCI.SendShortBGODataFromVME(0x2);
 
	}
	catch (exception& e){
    	cerr << e.what() << endl;
	}
	catch (...) {
    	cerr << "Unknown exception" << endl;
	}  
}


