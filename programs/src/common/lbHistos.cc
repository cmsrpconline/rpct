#include <iostream>
#include <time.h>
#include <sys/time.h>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "rpct/lboxaccess/FecManagerImpl.h"
#include "rpct/devices/System.h"
#include "rpct/devices/XmlSystemBuilder.h"
#include "rpct/devices/LinkBoard.h"

#include "log4cplus/consoleappender.h"

#include <boost/timer.hpp>
#include <log4cplus/configurator.h>

using namespace std;
using namespace HAL;
using namespace rpct;
using namespace log4cplus;

int main(int argc, char** argv)
{ 
  PropertyConfigurator config(LOG4CPLUS_TEXT("log4cplus.properties"));
  config.configure();
  //Logger::Logger::getInstance("TTb2").setLogLevel(DEBUG_LOG_LEVEL); FATAL_LOG_LEVEL
  
  SharedAppenderPtr myAppender(new ConsoleAppender());
  myAppender->setName("myAppenderName");
  std::auto_ptr<Layout> myLayout = std::auto_ptr<Layout>(new log4cplus::TTCCLayout());
  myAppender->setLayout( myLayout );
  //Logger::getInstance("RpctSystem").addAppender(myAppender);    
  
  Logger::Logger::getRoot().setLogLevel(DEBUG_LOG_LEVEL);
  
  try {
	System& system = System::getInstance();
	XmlSystemBuilder(system).build("system-lbox.xml");  
    cout<<"LB ID?";
    unsigned int lbID;
    cin>>lbID;
    LinkBoard& lb = dynamic_cast<LinkBoard&>(system.getBoardById(lbID)); 
    cout<<lb.getCrate()->getDescription()<<" "<<lb.getDescription()<<endl;
    char yn = 'y';
    while (yn == 'y') {
        unsigned int winO, winC;
        cout<<"winO? ";
        cin>>winO;
        cout<<"winC? ";
        cin>>winC;
        lb.setWindow(winO, winC);
    }
    
    
  }
  catch (TException& e) {
     cerr << e.what() << endl;
  }
catch (exception& e){
    cerr << e.what() << endl;
}
catch (...) {
    cerr << "Unknown exception" << endl;
} 

}
