//C++ headers
#include <iostream>
#include <time.h>
#include <sys/time.h>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

//rpctc headers
#include "rpct/devices/System.h"
#include "rpct/devices/XmlSystemBuilder.h"
#include "rpct/devices/TriggerBoard.h"

//logging
#include <log4cplus/consoleappender.h>
#include <log4cplus/configurator.h>
   
//utilities
#include <boost/timer.hpp>


using namespace std;
using namespace rpct;
using namespace log4cplus;

int main(int argc, char** argv){ 

  PropertyConfigurator config(LOG4CPLUS_TEXT("log4cplus.properties"));
  config.configure();
  //Logger::Logger::getInstance("TriggerBoard").setLogLevel(DEBUG_LOG_LEVEL); FATAL_LOG_LEVEL
  
  Logger::Logger::getRoot().setLogLevel(INFO_LOG_LEVEL);

  try { 
    System& system = System::getInstance();
    XmlSystemBuilder(system).build("system-rmb-mh.xml"); 
    system.checkVersions();

    cout<<"initializing boards() "<<endl;
    RmbMH& rmbMH = dynamic_cast<RmbMH&>(system.getDeviceById(1711));
    cout << hex << rmbMH.readWord(RmbMH::WORD_LFSR_STATE, 0) << endl;;
  }
  catch (TException& e) {
    cerr << e.what() << endl;
  }
  catch (exception& e){
    cerr << e.what() << endl;
  }
  catch (...) {
    cerr << "Unknown exception" << endl;
  }
  
}
