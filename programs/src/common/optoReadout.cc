//C++ headers
#include <iostream>
#include <time.h>
#include <sys/time.h>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

//rpctc headers
#include "rpct/devices/System.h"
#include "rpct/devices/XmlSystemBuilder.h"
#include "rpct/hardwareTests/RpctSystem.h"
#include "rpct/hardwareTests/DiagReadWriter.h"
#include "rpct/hardwareTests/TestPulsesManager.h"
#include "rpct/devices/tcsort.h"

//logging
#include "log4cplus/consoleappender.h"
#include <log4cplus/configurator.h>

//utilities
#include <boost/timer.hpp>

//external
#include "TTCci.hh"
using namespace ttc;
#include "CAENLinuxBusAdapter.hh"

using namespace std;
using namespace HAL;
using namespace rpct;
using namespace log4cplus;

const unsigned long secBx = 40000000;

unsigned int ttcCommand = 0x4;

const IDiagCtrl::TTriggerType pulserStartTrigger = IDiagCtrl::ttBCN0; //ttBCN0 ttPretrigger0
const IDiagCtrl::TTriggerType diagStartTrigger = IDiagCtrl::ttBCN0;

IDiagnosable::TTriggerDataSel triggerDataSel = IDiagnosable::tdsNone;

const unsigned long long  pulserLimit =  secBx * 1000; //256; // how many clocks pulser will work
const bool                pulserRepeatEna = false;

const unsigned long  readoutLimit = 256;


//const unsigned long       pulseLength = 256; // lenght of pulse pattern 256
//AK const unsigned long       pulseLength = 30; // lenght of pulse pattern 256
const unsigned long       pulseLength = 126; // lenght of pulse pattern 256

const unsigned long       daqDataDelay = 0;

//AK rpct::HardwareItemType pulsesSource = GbSort::TYPE;
rpct::HardwareItemType pulsesSource = TTCSortTCSort::TYPE;
// Opto::TYPE Pac::TYPE  GbSort::TYPE TTCSortTCSort::TYPE	 THsbSortHalf::TYPE

int main(int argc, char** argv){

  PropertyConfigurator config(LOG4CPLUS_TEXT("log4cplus.properties"));
  config.configure();
  //Logger::Logger::getInstance("TTb3").setLogLevel(DEBUG_LOG_LEVEL); FATAL_LOG_LEVEL
  
  SharedAppenderPtr myAppender(new ConsoleAppender());
  myAppender->setName("myAppenderName");
  std::auto_ptr<Layout> myLayout = std::auto_ptr<Layout>(new log4cplus::TTCCLayout());
  myAppender->setLayout( myLayout );
  Logger::getInstance("RpctSystem").addAppender(myAppender);
  Logger::getInstance("TTb3Device").addAppender(myAppender);
  Logger::getInstance("THsb").addAppender(myAppender);
  Logger::getInstance("TDiagCtrl").addAppender(myAppender);
  Logger::getInstance("XmlSystemBuilder").addAppender(myAppender);
  Logger::getInstance("TIIDevice").addAppender(myAppender);
  Logger::getInstance("XmlBxDataReader").addAppender(myAppender);
  Logger::getInstance("TestPulsesManager").addAppender(myAppender);
  Logger::getInstance("RPCDataStream").addAppender(myAppender);
  Logger::getInstance("TriggerBoardDevice").addAppender(myAppender);  
  

  Logger::Logger::getRoot().setLogLevel(INFO_LOG_LEVEL);

  try {
    System& system = System::getInstance();
    //XmlSystemBuilder(system).build("system-tc.xml"); 
    XmlSystemBuilder(system).build("system-tc.xml"); 
  		 
    RpctSystem rpctSystem;
  
    char yn;
    cout<<endl<<"Init devices (reset TTCrxs and QPLLs)? ";
    cin>>yn;
    if(yn == 'y') {
    	const System::BoardMap& boardMap = system.getBoardMap(); 
    	for(System::BoardMap::const_iterator itBoard = boardMap.begin(); itBoard != boardMap.end(); itBoard++) {
    		itBoard->second->init();  
    		itBoard->second->selfTest();  		
    	}
    }
    
    cout<<endl<<"reset and configure devices? ";
    cin>>yn;
    if(yn == 'y') {    	
    	const System::DeviceMap& deviceMap = system.getDeviceMap(); 
    	for(System::DeviceMap::const_iterator it = deviceMap.begin(); it != deviceMap.end(); it++) {
    		it->second->reset();    		
    	}
    }
            
    cout<<"enable transmission check? ";
    cin>>yn;
    if(yn == 'y') {	
    	bool enableBCNcheck = true;
   	 	bool enableDataCheck = true;
      	rpctSystem.enableTransmissionCheck(enableBCNcheck, enableDataCheck);	
      	rpctSystem.resetRecErrorCnt();	
      	cout<<"RecErrorCnt reset"<<endl;
    }
    else  
    	rpctSystem.enableTransmissionCheck(false, false);

    TestPulsesManager testPulsesManager("data/testBxData.xml");    //testpulses testBxData
	System::HardwareItemList readoutChipList;
	const System::HardwareItemList& optoList = system.getHardwareItemsByType(Opto::TYPE);
	
	readoutChipList.insert(readoutChipList.end(), optoList.begin(), optoList.end());
    
    testPulsesManager.configureReadouts(readoutChipList, -1, daqDataDelay, readoutLimit, diagStartTrigger, triggerDataSel, 0);
    
    testPulsesManager.startReadouts(readoutChipList);

    //::system("ttccommand.exe");
    #ifdef TTC_SOFTWARE 
    CAENLinuxBusAdapter bus(CAENLinuxBusAdapter::V2718, 1); 
	TTCci myCI(bus, 8);
    myCI.SendShortBGODataFromVME(ttcCommand);
    #endif
    	
    cout<<"Done"<<endl;
    
    cout<<"hit a key to stop ";
    char a;
    cin>>a;
    
    System::HardwareItemList::const_iterator iItem = readoutChipList.begin();
   	IDiagnosable* chip = dynamic_cast<IDiagnosable*>(*iItem);    	
	IDiagnosable::TDiagVector& diagVector = chip->GetDiags();
	unsigned int i = 0;
	for(; i < diagVector.size(); i++) {
		if(diagVector[i]->GetName() == "READOUT") {
			TStandardDiagnosticReadout* diagRead = dynamic_cast<TStandardDiagnosticReadout*>(diagVector[i]); 
    		while (! (diagRead->GetDiagCtrl().CheckCountingEnded() ) ) {
      			usleep(500000);
      			cout<<"*"<<flush;
    		}
			break;
		}
	}
	if(i == diagVector.size()) {
		throw TException("READOUT not foud on a chip");
	}
    cout<<endl;
    cout<<"DiagnosticReadout Counting Ended"<<endl;
           
    testPulsesManager.stopReadouts(readoutChipList);			            
       
    vector<TStandardDiagnosticReadout*> diagReadoutsVec;
    
    for (System::HardwareItemList::const_iterator iItem = readoutChipList.begin(); iItem != readoutChipList.end(); ++iItem) { 
    	IDiagnosable* chip = dynamic_cast<IDiagnosable*>(*iItem);    	
    	IDiagnosable::TDiagVector& diagVector = chip->GetDiags();
    	unsigned int i = 0;
    	for(; i < diagVector.size(); i++) {
    		if(diagVector[i]->GetName() == "READOUT") {
    			TStandardDiagnosticReadout* diagRead = dynamic_cast<TStandardDiagnosticReadout*>(diagVector[i]); 
				diagReadoutsVec.push_back(diagRead);
    			break;
    		}
    	}
    	if(i == diagVector.size()) {
    		throw TException("READOUT not foud on a chip");
    	}
    }

	 		
    ofstream out("./OptoReadout.txt");			
    //DiagReadWriter diagReadWriter(&cout, diagReadoutsVec);  
    DiagReadWriter diagReadWriter(&out, diagReadoutsVec);			
        
    diagReadWriter.enableEVNCheck = false;
    diagReadWriter.writeEvents();  
    out.close();                     
                     
    rpctSystem.printRecErrorCnt();
            
  }
  catch (TException& e) {
    cerr << e.what() << endl;
  }
  catch (exception& e){
    cerr << e.what() << endl;
  }
  catch (...) {
    cerr << "Unknown exception" << endl;
  }
  
}
