object frmMain: TfrmMain
  Left = 113
  Top = 64
  Width = 734
  Height = 623
  Caption = 'TTCvi & TTCRx Control'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefaultPosOnly
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel3: TPanel
    Left = 5
    Top = 5
    Width = 185
    Height = 260
    TabOrder = 4
    object Label11: TLabel
      Left = 8
      Top = 4
      Width = 28
      Height = 13
      Caption = 'CSR1'
    end
    object rgTriggerSource: TRadioGroup
      Left = 5
      Top = 21
      Width = 172
      Height = 151
      Caption = 'Trigger Source'
      Color = clBtnFace
      Items.Strings = (
        '0 L1A ECL'
        '1 L1A NIM'
        '2 L1A NIM'
        '3 L1A NIM'
        '4 VME sended'
        '5 Random'
        '6 Calibr.')
      ParentColor = False
      TabOrder = 0
      OnClick = rgTriggerSourceClick
    end
    object btVMETriger: TButton
      Left = 107
      Top = 111
      Width = 62
      Height = 19
      Caption = 'Triger'
      Enabled = False
      TabOrder = 1
      OnClick = btVMETrigerClick
    end
    object cbRanGenRate: TComboBox
      Left = 107
      Top = 130
      Width = 64
      Height = 21
      Enabled = False
      ItemHeight = 13
      TabOrder = 2
      Text = 'cbRanGenRate'
      OnChange = cbRanGenRateChange
      Items.Strings = (
        '1 Hz'
        '100 Hz'
        '1 kHz'
        '5 kHz'
        '10 kHz'
        '25 kHz'
        '50 kHz'
        '100 kHz')
    end
    object Panel1: TPanel
      Left = 5
      Top = 177
      Width = 121
      Height = 23
      Alignment = taLeftJustify
      BevelInner = bvRaised
      BevelOuter = bvLowered
      Caption = ' Orbit Signal'
      TabOrder = 3
      object chbOrbitSignalSel: TCheckBox
        Left = 62
        Top = 4
        Width = 57
        Height = 17
        Caption = 'internal'
        Checked = True
        State = cbChecked
        TabOrder = 0
        OnClick = chbOrbitSignalSelClick
      end
    end
    object bL1AFIFOReset: TButton
      Left = 6
      Top = 205
      Width = 85
      Height = 22
      Hint = 'L1A  FIFO Reset'
      Caption = 'L1A FIFO Reset'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = bL1AFIFOResetClick
    end
    object eCSR1: TEdit
      Left = 76
      Top = 233
      Width = 103
      Height = 21
      ReadOnly = True
      TabOrder = 5
    end
    object Button1: TButton
      Left = 6
      Top = 233
      Width = 69
      Height = 22
      Caption = 'READ CSR1'
      TabOrder = 6
      OnClick = Button1Click
    end
    object Panel10: TPanel
      Left = 41
      Top = 150
      Width = 36
      Height = 15
      Caption = 'Calibr.'
      Color = clInfoBk
      TabOrder = 7
    end
  end
  object GroupBox1: TGroupBox
    Left = 531
    Top = 3
    Width = 190
    Height = 224
    Caption = 'RX'
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 84
      Width = 48
      Height = 13
      Caption = 'Deskew 1'
    end
    object Label2: TLabel
      Left = 8
      Top = 113
      Width = 48
      Height = 13
      Caption = 'Deskew 2'
    end
    object Label3: TLabel
      Left = 8
      Top = 143
      Width = 50
      Height = 13
      Hint = 'L1Accept, Brcst1, BcntRes, EvCntRes, Brcst<5:2>'
      Caption = 'Coar Del 1'
      ParentShowHint = False
      ShowHint = True
    end
    object Label18: TLabel
      Left = 8
      Top = 172
      Width = 50
      Height = 13
      Hint = 'BrcstStr2, Brs<7:6>'
      Caption = 'Coar Del 2'
      ParentShowHint = False
      ShowHint = True
    end
    inline frSelectReg0: TfrSelectValue
      Left = 65
      Top = 77
      inherited tbTrackBar: TTrackBar
        OnChange = frSelectReg0tbTrackBarChange
      end
    end
    inline frSelectReg1: TfrSelectValue
      Left = 65
      Top = 106
      TabOrder = 1
      inherited tbTrackBar: TTrackBar
        OnChange = frSelectReg1tbTrackBarChange
      end
      inherited seSpinEdit: TCSpinEdit
        OnChange = frSelectReg1seSpinEditChange
      end
    end
    inline frSelectReg2: TfrSelectValue
      Left = 65
      Top = 134
      Hint = 'L1Accept, Brcst1, BcntRes, EvCntRes, Brcst<5:2>'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      inherited tbTrackBar: TTrackBar
        OnChange = frSelectReg2tbTrackBarChange
      end
      inherited seSpinEdit: TCSpinEdit
        OnChange = frSelectReg2seSpinEditChange
      end
    end
    object Panel5: TPanel
      Left = 8
      Top = 192
      Width = 145
      Height = 24
      Alignment = taLeftJustify
      Caption = ' Control Register'
      TabOrder = 3
      object pCntrolReg: TPanel
        Left = 86
        Top = 4
        Width = 53
        Height = 16
        Hint = 'Bouble Click to edit'
        BevelOuter = bvLowered
        Caption = '10010011'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = pCntrolRegClick
      end
    end
    inline frSelectReg3: TfrSelectValue
      Left = 65
      Top = 164
      Hint = 'BrcstStr2, Brs<7:6>'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      inherited tbTrackBar: TTrackBar
        OnChange = frSelectReg3tbTrackBarChange
      end
      inherited seSpinEdit: TCSpinEdit
        OnChange = frSelectReg3seSpinEditChange
      end
    end
    object Panel7: TPanel
      Left = 6
      Top = 13
      Width = 178
      Height = 60
      TabOrder = 5
      object Label17: TLabel
        Left = 5
        Top = 8
        Width = 49
        Height = 13
        Caption = 'Rx Addres'
      end
      object cmbTTC: TComboBox
        Left = 62
        Top = 5
        Width = 91
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 0
      end
      object eRxName: TEdit
        Left = 6
        Top = 33
        Width = 34
        Height = 21
        Hint = 'Name must be difrent!!!'
        MaxLength = 4
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Text = 'Name'
      end
      object eRxAddres: TEdit
        Left = 48
        Top = 33
        Width = 42
        Height = 21
        MaxLength = 6
        TabOrder = 2
        Text = '000000'
      end
      object bAddRxAddres: TButton
        Left = 95
        Top = 33
        Width = 39
        Height = 22
        Caption = 'Add'
        TabOrder = 3
        OnClick = bAddRxAddresClick
      end
      object bRxAddressDelete: TButton
        Left = 134
        Top = 33
        Width = 39
        Height = 22
        Caption = 'Delete'
        TabOrder = 4
        OnClick = bRxAddressDeleteClick
      end
    end
  end
  object bReset: TButton
    Left = 53
    Top = 564
    Width = 96
    Height = 25
    Caption = 'Reset TTCvi'
    TabOrder = 1
    OnClick = bResetClick
  end
  object GroupBox2: TGroupBox
    Left = 5
    Top = 364
    Width = 186
    Height = 193
    Caption = 'Inhibit'
    TabOrder = 2
    object Label4: TLabel
      Left = 9
      Top = 36
      Width = 37
      Height = 13
      Caption = 'Inhibit 0'
    end
    object Label5: TLabel
      Left = 54
      Top = 16
      Width = 27
      Height = 13
      Caption = 'Delay'
    end
    object Label6: TLabel
      Left = 107
      Top = 16
      Width = 40
      Height = 13
      Caption = 'Duration'
    end
    object Label7: TLabel
      Left = 9
      Top = 68
      Width = 37
      Height = 13
      Caption = 'Inhibit 1'
    end
    object Label8: TLabel
      Left = 9
      Top = 100
      Width = 37
      Height = 13
      Caption = 'Inhibit 2'
    end
    object Label9: TLabel
      Left = 9
      Top = 134
      Width = 37
      Height = 13
      Caption = 'Inhibit 3'
    end
    object cseInh0Delay: TCSpinEdit
      Left = 52
      Top = 32
      Width = 49
      Height = 22
      TabStop = True
      MaxValue = 4095
      ParentColor = False
      TabOrder = 0
    end
    object cseInh0Duration: TCSpinEdit
      Left = 108
      Top = 32
      Width = 41
      Height = 22
      TabStop = True
      MaxValue = 255
      ParentColor = False
      TabOrder = 1
    end
    object cseInh1Delay: TCSpinEdit
      Left = 52
      Top = 64
      Width = 49
      Height = 22
      TabStop = True
      MaxValue = 4095
      ParentColor = False
      TabOrder = 2
    end
    object cseInh1Duration: TCSpinEdit
      Left = 108
      Top = 64
      Width = 41
      Height = 22
      TabStop = True
      MaxValue = 255
      ParentColor = False
      TabOrder = 3
    end
    object cseInh2Delay: TCSpinEdit
      Left = 52
      Top = 96
      Width = 49
      Height = 22
      TabStop = True
      MaxValue = 4095
      ParentColor = False
      TabOrder = 4
    end
    object cseInh2Duration: TCSpinEdit
      Left = 108
      Top = 96
      Width = 41
      Height = 22
      TabStop = True
      MaxValue = 255
      ParentColor = False
      TabOrder = 5
    end
    object cseInh3Delay: TCSpinEdit
      Left = 52
      Top = 130
      Width = 49
      Height = 22
      TabStop = True
      MaxValue = 4095
      ParentColor = False
      TabOrder = 6
    end
    object cseInh3Duration: TCSpinEdit
      Left = 108
      Top = 130
      Width = 41
      Height = 22
      TabStop = True
      MaxValue = 255
      ParentColor = False
      TabOrder = 7
    end
    object btAplayInhSetings: TButton
      Left = 56
      Top = 160
      Width = 75
      Height = 25
      Caption = 'Apply'
      TabOrder = 8
      OnClick = btAplayInhSetingsClick
    end
  end
  object Panel2: TPanel
    Left = 5
    Top = 271
    Width = 185
    Height = 87
    TabOrder = 3
    object Label10: TLabel
      Left = 4
      Top = 3
      Width = 28
      Height = 13
      Caption = 'CSR2'
    end
    object eCSR2: TEdit
      Left = 75
      Top = 24
      Width = 105
      Height = 21
      ReadOnly = True
      TabOrder = 0
    end
    object btReadCSR2: TButton
      Left = 6
      Top = 24
      Width = 68
      Height = 22
      Caption = 'READ CSR2'
      TabOrder = 1
      OnClick = btReadCSR2Click
    end
    object btCSR2ResetFIFO: TButton
      Left = 6
      Top = 56
      Width = 96
      Height = 22
      Caption = 'RESET B-Go FIFO'
      TabOrder = 2
      OnClick = btCSR2ResetFIFOClick
    end
  end
  object Panel4: TPanel
    Left = 200
    Top = 266
    Width = 190
    Height = 153
    Caption = 'Panel4'
    TabOrder = 5
    object GroupBox4: TGroupBox
      Left = 7
      Top = 8
      Width = 177
      Height = 89
      Caption = 'Long-Format asynchronus cycles'
      TabOrder = 0
      object Label12: TLabel
        Left = 8
        Top = 45
        Width = 53
        Height = 13
        Caption = 'SUBADDR'
      end
      object Label13: TLabel
        Left = 71
        Top = 45
        Width = 29
        Height = 13
        Caption = 'DATA'
      end
      object eLFACSubaddr: TEdit
        Left = 7
        Top = 61
        Width = 26
        Height = 21
        Hint = 'Decyamal or Hexagonal (with x on begin)'
        MaxLength = 3
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Text = '000'
      end
      object eLFACData: TEdit
        Left = 69
        Top = 61
        Width = 28
        Height = 21
        Hint = 'Decyamal or Hexagonal (with x on begin)'
        MaxLength = 3
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Text = '000'
      end
      object bLFACSend: TButton
        Left = 118
        Top = 60
        Width = 42
        Height = 23
        Caption = 'Send'
        TabOrder = 2
        OnClick = bLFACSendClick
      end
      object chbLFACExt: TCheckBox
        Left = 8
        Top = 20
        Width = 113
        Height = 17
        Caption = 'External subadres'
        Checked = True
        State = cbChecked
        TabOrder = 3
        OnClick = chbLFACExtClick
      end
    end
    object GroupBox5: TGroupBox
      Left = 7
      Top = 102
      Width = 177
      Height = 45
      Caption = 'Short-Format asynchronous cycles'
      TabOrder = 1
      object Label14: TLabel
        Left = 8
        Top = 23
        Width = 56
        Height = 13
        Caption = 'COMMAND'
      end
      object eSFACCommand: TEdit
        Left = 71
        Top = 19
        Width = 28
        Height = 21
        Hint = 'Decyamal or Hexagonal (with x on begin)'
        MaxLength = 3
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Text = '000'
      end
      object bSFACSend: TButton
        Left = 118
        Top = 18
        Width = 42
        Height = 23
        Caption = 'Send'
        TabOrder = 1
        OnClick = bSFACSendClick
      end
    end
  end
  object TabControl1: TTabControl
    Left = 200
    Top = 6
    Width = 321
    Height = 225
    TabOrder = 6
    Tabs.Strings = (
      'B-Go 0'
      'B-Go 1'
      'B-Go 2'
      'B-Go 3')
    TabIndex = 0
    OnChange = TabControl1Change
    object Label15: TLabel
      Left = 122
      Top = 29
      Width = 23
      Height = 13
      Caption = 'Data'
    end
    object GroupBox3: TGroupBox
      Left = 14
      Top = 41
      Width = 89
      Height = 108
      Caption = 'Setings'
      TabOrder = 0
      object chbEnable: TCheckBox
        Left = 8
        Top = 16
        Width = 57
        Height = 17
        Caption = 'Enable'
        TabOrder = 0
        OnClick = chbEnableClick
      end
      object chbSync: TCheckBox
        Left = 8
        Top = 34
        Width = 72
        Height = 17
        Caption = 'Asynchron'
        TabOrder = 1
        OnClick = chbEnableClick
      end
      object chSingle: TCheckBox
        Left = 8
        Top = 51
        Width = 73
        Height = 17
        Caption = 'Repetetive'
        TabOrder = 2
        OnClick = chbEnableClick
      end
      object chbFIFO: TCheckBox
        Left = 8
        Top = 68
        Width = 55
        Height = 17
        Caption = 'FIFO'
        TabOrder = 3
        OnClick = chbEnableClick
      end
      object chbCalibration: TCheckBox
        Left = 8
        Top = 88
        Width = 73
        Height = 17
        Caption = 'Calibration'
        Color = clInfoBk
        Enabled = False
        ParentColor = False
        TabOrder = 4
        OnClick = chbCalibrationClick
      end
    end
    object btB_GoSignal: TButton
      Left = 233
      Top = 152
      Width = 75
      Height = 25
      Caption = 'B-Go Signal'
      TabOrder = 1
      OnClick = btB_GoSignalClick
    end
    object bFIFO0Reset: TButton
      Left = 94
      Top = 192
      Width = 75
      Height = 25
      Caption = 'Reset FIFO'
      TabOrder = 2
      OnClick = bFIFO0ResetClick
    end
    object chbRetransmit: TCheckBox
      Left = 22
      Top = 149
      Width = 75
      Height = 17
      Caption = 'Retransmit'
      Checked = True
      State = cbChecked
      TabOrder = 3
      OnClick = chbRetransmitClick
    end
    object lbB_GoDataSeq: TListBox
      Left = 122
      Top = 45
      Width = 97
      Height = 137
      ItemHeight = 13
      TabOrder = 4
      OnDblClick = lbB_GoDataSeqDblClick
    end
    object bLoadFIFO: TButton
      Left = 234
      Top = 117
      Width = 75
      Height = 25
      Caption = 'Load FIFO'
      TabOrder = 5
      OnClick = bLoadFIFOClick
    end
    object bAddData: TButton
      Left = 226
      Top = 45
      Width = 41
      Height = 25
      Caption = 'Add'
      TabOrder = 6
      OnClick = bAddDataClick
    end
    object bDeleteData: TButton
      Left = 226
      Top = 77
      Width = 41
      Height = 25
      Caption = 'Delete'
      TabOrder = 7
      OnClick = bDeleteDataClick
    end
    object bFIFOStaus: TButton
      Left = 171
      Top = 192
      Width = 75
      Height = 25
      Caption = 'FIFO Staus'
      TabOrder = 8
      OnClick = bFIFOStausClick
    end
  end
  object bReadBGoSettings: TButton
    Left = 200
    Top = 232
    Width = 102
    Height = 23
    Caption = 'Read B-Go Settings'
    TabOrder = 7
    OnClick = bReadBGoSettingsClick
  end
  object eBGoSeting: TEdit
    Left = 304
    Top = 232
    Width = 56
    Height = 21
    ReadOnly = True
    TabOrder = 8
  end
  object GroupBox6: TGroupBox
    Left = 529
    Top = 368
    Width = 193
    Height = 108
    Caption = 'Presettings'
    TabOrder = 9
    object lbPresettings: TComboBox
      Left = 8
      Top = 18
      Width = 113
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
    end
    object bAplayPreset: TButton
      Left = 128
      Top = 18
      Width = 57
      Height = 21
      Caption = 'Apply'
      TabOrder = 1
      OnClick = bAplayPresetClick
    end
    object bDeletePreset: TButton
      Left = 49
      Top = 48
      Width = 41
      Height = 25
      Caption = 'Delete'
      TabOrder = 2
      OnClick = bDeletePresetClick
    end
    object bAddPreset: TButton
      Left = 8
      Top = 48
      Width = 41
      Height = 25
      Caption = 'Add'
      TabOrder = 3
      OnClick = bAddPresetClick
    end
    object eNewPresetName: TEdit
      Left = 8
      Top = 79
      Width = 113
      Height = 21
      TabOrder = 4
      Text = 'eNewPresetName'
    end
  end
  object Panel6: TPanel
    Left = 616
    Top = 484
    Width = 105
    Height = 105
    Color = clSilver
    TabOrder = 10
    object Image1: TImage
      Left = 31
      Top = 21
      Width = 47
      Height = 49
      Center = True
      Picture.Data = {
        0A544A504547496D61676520140000FFD8FFE000104A46494600010101012C01
        2C0000FFDB004300080606070605080707070909080A0C140D0C0B0B0C191213
        0F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F2739
        3D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232
        3232323232323232323232323232323232323232323232323232323232323232
        32323232323232323232323232FFC00011080088007D03012200021101031101
        FFC4001F0000010501010101010100000000000000000102030405060708090A
        0BFFC400B5100002010303020403050504040000017D01020300041105122131
        410613516107227114328191A1082342B1C11552D1F02433627282090A161718
        191A25262728292A3435363738393A434445464748494A535455565758595A63
        6465666768696A737475767778797A838485868788898A92939495969798999A
        A2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6
        D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F01000301
        01010101010101010000000000000102030405060708090A0BFFC400B5110002
        0102040403040705040400010277000102031104052131061241510761711322
        328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728
        292A35363738393A434445464748494A535455565758595A636465666768696A
        737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7
        A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3
        E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00F7FA
        28A2800A28ACDB1D6ECF51D5750D3AD8B3CB6023F39C0F9033EEF941EEC36F3E
        9B877C8001A5451450032690C50BC811A42AA4845C65BD867BD602F8C34AB88A
        1BAB7BB8DEC9839967276088A8E8FBB054FA83822B76E7CEFB3C9F67D9E76D3B
        3CCCEDCFBE39C578EEB5A7EBFA7789D1E4D59266991A72EB64A225C1F993CB2C
        430259792D9E4F39158D57349729A535177E63D674AD56DB58B35BBB42CD038C
        A33295DC3B100F383DBD473D0D5EAE1BC233DD4D3A79A0E072760381FF00D6FC
        4FE35DCD6C8CD8514552D3F54B6D4FED22DD8EEB699E09518619594E3A7A1EA0
        F706802ED14514005145140051451401CCF8E7C487C39E1D9E5B69A21A948B8B
        58988DCC72033053D76839FCBD6B86F877AEB45637F89434D7777E7C8C4619B7
        28E7A9EBC9ACDF8BF74B3F8AAD9619439B48155D14F2AC4B311FF7CE3F31585E
        19B9FB15FAC391B64F9C306041EE3A71D33537D4BB687D15049E6C11BE739519
        FAD4959DA25C0B8D36360CCD818C9FE55A354405729ABC6B2F8823479ECFECE6
        D9879649F38C9B97DF1B718F7CFE9CFF00C48F106B1A35EC1B20074B2B969002
        46FCE30F8EC4903B7BD709FF0009848259DAF3488C2631CC2C3CBC12386EA73C
        7735C55B12E32E548E9A787728F35CF75D334C82CE34913EF3283F4AD1AF17F0
        4F8CAFAE6762F12C76872A986721883D4061C0C7D3A741C67D86D2E05D5B24A0
        633DABAA9CF9E376AC6338F2CAD727AF0AF116AD73A67C40D42E6CAE658923B8
        123C6B215590A280430E87F8873D33C57B94B22C31348EC151416627A003A9AF
        9AB58BF6D4757D4AE16262672464A70A59892707D3CC1F95390411EF5E1EF12D
        B6B906F8A40E3715DC0639071D0FD3F5F4ADEAF15F034ED692989598EE90B364
        639E074FC2BDA572146E396C7354897A3168A28A0414555D46F869B632DDB413
        CC910DCEB02EE7DBDC85EA7039C0E78E013C573B3F8DAC61B737EAB3CD68C804
        491C45A595FD157AE483DF18C738A894E316932945BD8F2DF89364F6DE35B995
        81CCBB258F1C1742A01E9DC143F957351B056888654DADBD30C4E49192075009
        C6476E08ED5E89F12265BCD2B4EB9BC8996F4C87CB8EDDC610704A1720E5B18E
        718CAFA649F3396E46405976AB9C36C4076B93EE077EA33C1E4527B9A2D8F65F
        87FE23FB459A2CFDC60E0601F423FCFAD7A30208041C835F31E8DAADEE91389F
        7976DDBE5DFD4FB8C9E41FAFA720F07D73C2DE3DB4D62CFECC6E2382FC02AC1B
        901BB7CBD7DF07048E9EB56999C96A6EF8DADB4DBCF0F4B6DAA5D8B5B79481BF
        7286DC0861B770209CAFA1AF179A084DDCD6F69AA4B71A4AC6BB644FB301E6E5
        838C88781B76F61D4F26AAEB9A2F8C2C67B9BABB8E7BB8D198BDC1BF5713007E
        F984B1241C676803038038CD675BDC48D6E2430DAC3BC73E54582D927AB124FE
        19ACE708C9DE48D22DC568CF40F06E8D15F3C4D6F2C52463E456560578E3A8E3
        F0AF60B5B75B5B74897B0E4FBD79B7802E9592D116344508BB5234014123A003
        000CD743E2DF1C41E1C682DA2816E6F27C811F99F74EDCAF0012C49C703B771C
        674D1232B36CC8F89BE29B8D3AD8699A79059D18DD918CAA153B5413C024FAFA
        8F5AF28B5648D8453FDF8CB3CA727E790F619183925BA73CAD58D5AF25D444F7
        17D7266999BCC9D9180518E7683F9F3F53C80B98AD84DB83CCA46DC1259C392D
        9C633DB9FCC803A254377354ADA1BFE102D6F7D8720966CB1C9232704F539FF3
        DEBDD6D9FCCB68DFE6E541F9BAD784E8914E80CD63024D2463290B3E37FF00B2
        1B9C1F7391DB81C8EFE3F13DE5AC2F1AD883A814554B17B98D59081925C86200
        03938CB608E287351B264F2B936D1DE515474ABB3776A0BBA3CAA007645DA09F
        619381ED93F5357AB43321B949A4B775B79522948F95DD0B807DC0233F9D7916
        B7A0EB169AD6D7D46FEE51C1B8FB71609224A30A50606029523E5C63E5C638CD
        7B1D43716B0DD05132060BD2A25052DCA8CDC763CC8E9C6F7C37731DF9B99963
        8CC9BD00F341504E540006EF6C60F4208245792DEA463E72C5438F96475C071D
        B8E78E075CE3B362BE94D5F434BCD0EEACADEEE7B179130B716E3F788720F18E
        48E3040E48240209CD7CDF2D8C490CAD6B1DAF91965F37CC787CCC7192A1060F
        1C8E7AF5A2454352BED9C2290A7CBCE7006369EE448831F8E549F6E953437AB6
        052EC943B0611D78247BF39279EA41AAB1389605963B72BB78C4A23910903A2C
        8D92474EE3D38AB71CB6F0DCC525E5BA5A42C43028A40524E41FBEDC7E469228
        F4FB2D40DE7869EEEEC6258ED77B06E3E7DB9C1FC6BCE2EECFCAB761146C5233
        FBC3DBD48C039CF15D1DE6B169FD81FD9F693A5C4B72CAAFE5303B57AE7DF90B
        C0F5AC38FC88E18F699BC938550E3ABF03FA9A722628EB7C2EF7B67E1E99EC90
        CB7B0D99312E37179027007AE48AE3AF7567D488BEBABA8613321F3267C972BD
        D00E48073CE076E73D2BB5F0BEBBA668CA24D42EE24F2941903F1B8E067F3AE1
        F5CD56C350BFBDBD82C5A6B49E7778DC02033B316C31DC00C9E7A1A6C71EA42B
        A8398CB46EB12A46CC080D88C632492C7E66F6EA4FE1B6E45F28076059D142EE
        64FF005608C7CDEF8C0C9EB9C000673937093051125A45F687708AB3B8254F52
        546403F4E4FD2AFB453087CABA8E250C3608D5BCB1C9E870DB8F5E83AF7CD4B2
        91E89E11D2D6FA0F295E454700662728D8F661823EA08346A1E0786CAEA58AD6
        01FD9EC565585897DB3739604E4838C739CE58FB6357C00D2A98848DBDB3F33E
        DDB93F4ED5E92C8AF8DC01C1C8CF6354E2A5B992935B3399F09D8CD6D6F97395
        51B7E762CDF99E7F135D3D22A8550A0600180296A89396F1AF8964F0E5809238
        4C9E6452F29F7908030C3D719C91FDD0C7F8706EF873591A8E996C66B8867B86
        401DE1CED76190C4640C0C8E98E3F5A87C5967A75E5AC49792A41328678662DB
        19318190FDB961C679E9C8C8AE106B2F06B56F13C176C5992490A395F29BBAB9
        CFCC01C9CF20E08E70456129B8D4D5E86AA2A50D373D76BC53E28785A6B0D64E
        B9003341798598BC08DE4300AA30DB95806F7CF3DC645763378D6E2DEEAE615B
        45C2DAB35BA4C421926DC4202DB8AED7F971D0E491D7217A19A1B2F12E99F66B
        B861962910178A440EA7A1E41E3838AD14A33BA4F626CE166CF98DC35B5DBFEE
        EF2412E43AC336DC918FBC0328E99E3F53504D6C6CE46531DBAC1BB74AE18C8C
        A3BE00E3A64F1E9DC633D6F8AFC1D73A14E4344B71A688CE088D638E362C400C
        339C7DDE00C1E78F4E522B59ECF60B79B0A24DC90329447704124A8E7070063E
        953B17B90DB37971F95044CAAC49F9C90CBC673C73EC39ABBF6C52CAEAB3C70B
        FF00C7B20E9BB8E7DBFF00AD9ACF922FDEB4975340A6752C421C0C2F202F5E3F
        C9EB44D23490812798AB2615503E7CBF9B93DF3FAD52770D8825B982F2F43CD7
        0A7C88410AC70667C93B47A75C77E8335A16EC91C8F2C33FF66AA00FF62B9936
        09F1D4827839F6CF3DAA8E9CACC81D16D962B8611619F0531C71EE707AF722B7
        62B5375B8DD4E6E446C102F940BC43923A0E4E0E7A67F2A4D8EDA096B10798DE
        496D6B6920FB8047E6C4475C9C636E71D785FC6B6AC927BD990472C02153C082
        1E1F81FC6DCEDEBD073FDEA82CED6E58848999141FBE8483D3AEC742073DFAFA
        1AECF41B282CB51B38AF4EC370B2323B9E3F76033649FF0067273FEC9A697564
        C9F44775E0DD305BDA79ACA723183EF5D657994DF13AC922B64D2ACE66479DA3
        327945D8A2F3855E32ECA33D70A1959B19C55693C67A8EB139F3A4B9B6B32374
        71692A1D8E0E086B86C2B107A88CF1D0B3722A5D682EA1EC67D51EAD4572BA07
        8AA0BD75B3FB1EA50B01846BA85B0C07AC9C8CFB139F6AEAAB5466D58C4F13E9
        8356D3E1B76821963132BB991B695519CEDF94F27A76E09E4579C6AF6FF64BF4
        B5B77950CEDB4C11B825F009C92E72300641DC0FCA073D0FB1573BE20D25DED4
        C9631426E88281E404EC53D48008C9E071B97D73C62B29D38CB5EA5C26D69D0F
        25D42C2C1DADAFA586F6DEEE2511B8926575650A4162D924678C9E3BF4C935A7
        6DE2F6B1D5F4F413B8B78E47FB50F2A4FDE7CB8544F970C4B303807A80075E72
        BEC37B0DF1DB6CB0BC4E164924B77B718EE55564649091D58F1BBFBDD556F049
        13405E3B97B4B3766F35D4208C3A90C015C031FDD270A36E01CF0D8C23CC97BA
        8DDF2B7EF3B9EB56779078A2C7CE9AD64B290BB22C53B297C02473B491CE0918
        278239E6BCA7C67A1E83A15FBF9D13497977BA52379DCAA49E873F28273C0C67
        0735DEF8360996488EDDB1A0002A8E00E9DBB570DF1CF4B9EDF52D335981F08F
        1FD99C91C0752597F1219B8FF66BA9DEC73C6DCC715ACC5A21D3D2686DE31234
        80C722463F84818EC48C03C63AF358976AD877664DCC8449F2E021E71F9F1545
        E4BC924440C24891B009EBC8E338FAD5EBD7944089B64959149958B01BFA91FD
        3F952344AC759656DA3916CBE5439404448C3EE03D402473DFEBCD747A4E8163
        A94286D00DB13ED03A1561CE3F5CFE35E57F6D9ED40612AB4B951E50E8A00072
        71EE7F4AF4EF8712CEB12C8F9FF49713007B2E005FCC283F8D344495B63BED13
        C161087954018E091FE7D6B8AF88F2E6E22B4912245B19E458DB763CD4744272
        30785F994F2010470735EBB7DAF69FA5D909EFEEA1B7C26712480678F7F5AF2C
        BAD16EE7BA7D5EE2E248AF666324623C131827231B81C1E4FE78181C5615F9A5
        6840BA368BE791C65B25BF2F7EF710445768CA6DF30673CB3028173C91F36E3C
        9E79AEA2D34B92F154402E60F9400E9752B63A60ED6629D00E36E3DAB5749F0D
        DEDEDC2B4EEF2C84FDE6EA7FC8FF0022BD1B47D061D3A305906F5C6DC76AAA54
        1437DC2A57727A187E19D36EE18E349DF7CABD640B8CFBE3B1FEBD2BB5A28ADC
        C028A28A00E6FC41A2473433DC232AC9B1994BF40F838FD715CCE9DA8E9FA85B
        5AC6B6E5667768E78F1BBC8DA0E771ED92001EBBB8CE0D767E23D134DD6F4D78
        B52D3A2BD58D59915D7E6071FC2DC1527A704578F6A5E1BBB3AA09751B0B47B7
        71FBAB58576AD963EEAAE0E1811F7BB1639C11CD6351D4E65CBB1AC141C5F36E
        66378A7C45A0EB8D6B6F3C6B6D60CD1CB6F6B6F2C81543000812150C483951BB
        80073C0AE8B5087C4DE2ED2DED756D66C6DA075CB5BDBDA79DCF5197257183E9
        91C753D69E9E0CB2BC64BA8EC238A755215A21B429FEF051F286FF006B19ABFE
        1A4D460D4E4B5D5B4B30A658C73C4FBA3700FE2578E99FA751C918B8BB37A152
        9A92BC51E497FE0FBED0EE0CB2DD47B18B46AA80EE9533F7981C8E4E3819C646
        0D407899413F2B0201E3A64FF87A574FE39D692F7C59709015F22D7746A30080
        13209EF9CB6FEDD02FA56188FCA6B479594B031A607A10C4F7F538FF000AB624
        52B5F0F4D79708083E41761248CD977C1CE401C609E3D7DB1D7BFD3B4DF10D8D
        B492E9F3C73FCA7092A049718FE17195CFA65719C678AA1E1968C6AA6D6404A0
        E5723A74FE631FAD76976FA86A7A97F64DB593C1A4A90B75765369651CB057C8
        3938DA36838DDBB7646D132B5BCC1377F23CCD6E669AF64637735CEAB70FB6E6
        6B58D5D635DC37007CC503049C920AB1239CF27D0A2D7D9E68E59D9E348C8370
        D7303842878E2403606CE38CF3C8EF5AEBE14B7BDBDDF6D6B0A31509B950292A
        07033E80741ED56755F87A97F68915C319ADE3C916C9950EF8237311C900741C
        7539CF188F62D45A4F5653ACA524DAD06784BC4B1DBDCDEDADE33CB7D2CE59A3
        5B79912DD428023DCC80718273C64B138E6BD0D595D4329041E841AF31D3BC3F
        7F1DE2895A490C402EE61F36D5000CFBF1C9EF5E956A862B58D1BA8519E315AD
        34D4527B98CDA72D0968A28AB2428A28A002A9DE69B6F784348837FF007B1572
        8A00821B38205DA918FC46699736315C92CD9DF8C03E956A8A00F9C7E27787A3
        F0D6AF3DC442E45B6A5B76C8FF003A094B3174CE32A36804027B9C7038C1D427
        8E18E491E42614F2C9739273BB8E9DB9EC315F4AF88BC3B61E26D2DAC3518127
        80F3B1F3D4742083907DC735F3ADAE8E6FBC4FAB6957170D7D6FA64C123122A9
        2C4165CB903E623691E8793D6958B52D0B7E18B5371A88BC8D64556004636950
        4633BB079EA48E78C0C8EBCFB9697A6B5C69FF00BCC86C0C67A7F2AE6BC2BE18
        24895C0CF5E6BD21542A851D00C0A6896EE436D6915A47B235FC7BD4F4514082
        8A28A0028A28A0028A28A0028A28A0028A28A002B979BC1BA6C3793DD5858DBC
        335D3EEB878E30A5CE49C9F5E589A28A00DFB2B41676E22041C1EA062ACD1450
        01451450014514500145145007FFD9}
      Stretch = True
    end
    object Label19: TLabel
      Left = 8
      Top = 5
      Width = 88
      Height = 13
      Caption = 'Warsaw University'
    end
    object Label20: TLabel
      Left = 4
      Top = 73
      Width = 98
      Height = 13
      Caption = '(C) Karol Bunkowski '
    end
    object Label21: TLabel
      Left = 7
      Top = 88
      Width = 87
      Height = 13
      Caption = 'Michal Pietrusinski'
    end
  end
  object GroupBox7: TGroupBox
    Left = 200
    Top = 427
    Width = 189
    Height = 86
    Caption = 'TRIGWORD Register'
    Color = clInfoBk
    ParentColor = False
    TabOrder = 11
    object Label22: TLabel
      Left = 7
      Top = 63
      Width = 53
      Height = 13
      Caption = 'SUBADDR'
    end
    object chbTRIGRegEnable: TCheckBox
      Left = 8
      Top = 16
      Width = 145
      Height = 17
      Caption = 'Enable after-trigger tranfer'
      Checked = True
      State = cbChecked
      TabOrder = 0
      OnClick = chbTRIGRegEnableClick
    end
    object eTRIGRegSad: TEdit
      Left = 70
      Top = 57
      Width = 26
      Height = 21
      Hint = 'Decyamal or Hexagonal (with x on begin)'
      MaxLength = 3
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Text = '000'
    end
    object chbTrigRegExt: TCheckBox
      Left = 8
      Top = 39
      Width = 113
      Height = 17
      Caption = 'External subadres'
      Checked = True
      State = cbChecked
      TabOrder = 2
      OnClick = chbLFACExtClick
    end
    object bTRIGRegSet: TButton
      Left = 120
      Top = 45
      Width = 42
      Height = 23
      Caption = 'Set'
      TabOrder = 3
      OnClick = bTRIGRegSetClick
    end
  end
  object Panel8: TPanel
    Left = 403
    Top = 572
    Width = 209
    Height = 17
    Caption = 'Yellow things available only in TTCvi Mk II'
    Color = clInfoBk
    TabOrder = 12
  end
  object bHelp: TButton
    Left = 528
    Top = 497
    Width = 75
    Height = 25
    Caption = 'Help'
    TabOrder = 13
    OnClick = bHelpClick
  end
end
