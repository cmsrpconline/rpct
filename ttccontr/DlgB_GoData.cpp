//---------------------------------------------------------------------

#include "precompiled.h"
#pragma hdrstop

#include "DlgB_GoData.h"
//--------------------------------------------------------------------- 
#pragma resource "*.dfm"
TB_GoDataDlg *B_GoDataDlg;
//---------------------------------------------------------------------
__fastcall TB_GoDataDlg::TB_GoDataDlg(TComponent* AOwner)
	: TForm(AOwner)
{
}
//---------------------------------------------------------------------
void __fastcall TB_GoDataDlg::bAddLongClick(TObject *Sender)
{
  TBitVector word;
         std::bitset<14> b_rx_addr(StrToInt(eRxAddres->Text) );
         std::bitset<8>  b_subaddr(StrToInt(eSubaddr->Text) );
         std::bitset<8>  b_data(StrToInt(eData->Text) );

         word[31]=1;
         for(int i=0;i<14;i++) word[i+17]=b_rx_addr[i];
         word[16]=chbExternal->Checked;
         for(int i=0;i<8 ;i++) word[i+8]=b_subaddr[i];
         for(int i=0;i<8 ;i++) word[i]=b_data[i];

  cur_seq->push_back(word);
  lbB_BoData->Items->Add(eRxAddres->Text+", "+IntToStr(chbExternal->Checked)+", " + eSubaddr->Text+ ", " + eData->Text);
}
//---------------------------------------------------------------------------

void TB_GoDataDlg::ShowSeq()
{
 TVectorSeq::iterator iterator;
 TBitVector word;

         std::bitset<14> b_rx_addr;
         std::bitset<8>  b_subaddr;
         std::bitset<8>  b_data;
         std::bitset<8>  b_command;
 for(iterator=cur_seq->begin();iterator!=cur_seq->end();iterator++)
 {
  word= *iterator;
  if(word[31]==1)
  {
         for(int i=0;i<14;i++) b_rx_addr[i]=word[i+17];
         for(int i=0;i<8 ;i++) b_subaddr[i]=word[i+8];
         for(int i=0;i<8 ;i++) b_data[i]=word[i];
   lbB_BoData->Items->Add(IntToStr(b_rx_addr.to_ulong() ) +", "+ IntToStr(word[16])+", " + IntToStr(b_subaddr.to_ulong())+", "+ IntToStr(b_data.to_ulong()));
  }
  else
  {
   for(int i=0;i<8 ;i++) b_command[i]=word[i+23];
   lbB_BoData->Items->Add(IntToStr(b_command.to_ulong()) );
  }
 }
}
//---------------------------------------------------------------------------
void __fastcall TB_GoDataDlg::AddShortClick(TObject *Sender)
{
  TBitVector word;
  std::bitset<8>  b_command(StrToInt(eCommand->Text));
  word[31]=0;
  for(int i=0;i<8 ;i++) word[i+23]=b_command[i];

  cur_seq->push_back(word);
  lbB_BoData->Items->Add(eCommand->Text);
}
//---------------------------------------------------------------------------

void __fastcall TB_GoDataDlg::lbB_BoDataClick(TObject *Sender)
{
   TBitVector word;
         std::bitset<14> b_rx_addr;
         std::bitset<8>  b_subaddr;
         std::bitset<8>  b_data;
         std::bitset<8>  b_command;
  word= (*cur_seq)[lbB_BoData->ItemIndex];

  if(word[31]==1)
  {
    Panel1->BevelOuter=bvRaised;
    Panel2->BevelOuter=bvLowered;
    for(int i=0;i<14;i++) b_rx_addr[i]=word[i+17];
    for(int i=0;i<8 ;i++) b_subaddr[i]=word[i+8];
    for(int i=0;i<8 ;i++) b_data[i]=word[i];
    eRxAddres->Text=IntToStr(b_rx_addr.to_ulong() );
    chbExternal->Checked= word[16];
    eSubaddr->Text=IntToStr(b_subaddr.to_ulong() );
    eData->Text = IntToStr(b_data.to_ulong() );
  }
  else
  {
    Panel2->BevelOuter=bvRaised;
    Panel1->BevelOuter=bvLowered;
    for(int i=0;i<8 ;i++) b_command[i]=word[i+23];
    eCommand->Text= IntToStr(b_command.to_ulong() );
  }
}
//---------------------------------------------------------------------------


void __fastcall TB_GoDataDlg::bDeleteClick(TObject *Sender)
{
  TVectorSeq::iterator iterator;
  int i;                              // iterator!=cur_seq->end()
  for(iterator=cur_seq->begin(), i=0;i<lbB_BoData->ItemIndex; i++,iterator++);
  cur_seq->erase(iterator);
  lbB_BoData->Items->Delete(lbB_BoData->ItemIndex);
}
//---------------------------------------------------------------------------

void __fastcall TB_GoDataDlg::bCurRxClick(TObject *Sender)
{
  eRxAddres->Text= IntToStr(CurrentRX);
}
//---------------------------------------------------------------------------

