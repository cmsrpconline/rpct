//----------------------------------------------------------------------------
#ifndef DlgRxControlRegH
#define DlgRxControlRegH
//----------------------------------------------------------------------------
#include <vcl\System.hpp>
#include <vcl\Windows.hpp>
#include <vcl\SysUtils.hpp>
#include <vcl\Classes.hpp>
#include <vcl\Graphics.hpp>
#include <vcl\StdCtrls.hpp>
#include <vcl\Forms.hpp>
#include <vcl\Controls.hpp>
#include <vcl\Buttons.hpp>
#include <vcl\ExtCtrls.hpp>
#include <bitset>
//----------------------------------------------------------------------------
class TdlgRxControlRegister : public TForm
{
__published:        
	TButton *OKBtn;
	TButton *CancelBtn;
	TBevel *Bevel1;
        TCheckBox *chb0;
        TCheckBox *chb1;
        TCheckBox *chb2;
        TCheckBox *chb3;
        TCheckBox *chb4;
        TCheckBox *chb5;
        TCheckBox *chb6;
        TCheckBox *chb7;
private:
        
public:
	virtual __fastcall TdlgRxControlRegister(TComponent* AOwner);
    typedef std::bitset<8> TControlReg;
    TControlReg contolReg;
    TControlReg GetControlReg();
    void SetControlReg(TControlReg b);
        
};
//----------------------------------------------------------------------------
extern PACKAGE TdlgRxControlRegister *dlgRxControlRegister;
//----------------------------------------------------------------------------
#endif    
