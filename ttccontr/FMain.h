//---------------------------------------------------------------------------

#ifndef FMainH
#define FMainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//#include "TTTCvi.h"
#include "TTCviN.h"
#include "FSelectValue.h"
#include "CSPIN.h"
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include <IniFiles.hpp>
#include <jpeg.hpp>
#include <vector>
//---------------------------------------------------------------------------
class TfrmMain : public TForm
{
__published:	// IDE-managed Components
    TGroupBox *GroupBox1;
    TfrSelectValue *frSelectReg0;
    TfrSelectValue *frSelectReg1;
    TfrSelectValue *frSelectReg2;
    TButton *bReset;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label3;
        TGroupBox *GroupBox2;
        TCSpinEdit *cseInh0Delay;
        TCSpinEdit *cseInh0Duration;
        TLabel *Label4;
        TLabel *Label5;
        TLabel *Label6;
        TLabel *Label7;
        TCSpinEdit *cseInh1Delay;
        TCSpinEdit *cseInh1Duration;
        TLabel *Label8;
        TCSpinEdit *cseInh2Delay;
        TCSpinEdit *cseInh2Duration;
        TLabel *Label9;
        TCSpinEdit *cseInh3Delay;
        TCSpinEdit *cseInh3Duration;
        TButton *btAplayInhSetings;
        TPanel *Panel2;
        TLabel *Label10;
        TEdit *eCSR2;
        TButton *btReadCSR2;
        TButton *btCSR2ResetFIFO;
        TPanel *Panel3;
        TRadioGroup *rgTriggerSource;
        TButton *btVMETriger;
        TComboBox *cbRanGenRate;
        TPanel *Panel1;
        TCheckBox *chbOrbitSignalSel;
        TLabel *Label11;
        TPanel *Panel5;
        TPanel *pCntrolReg;
        TPanel *Panel4;
        TGroupBox *GroupBox4;
        TEdit *eLFACSubaddr;
        TEdit *eLFACData;
        TLabel *Label12;
        TLabel *Label13;
        TButton *bLFACSend;
        TGroupBox *GroupBox5;
        TEdit *eSFACCommand;
        TLabel *Label14;
        TButton *bSFACSend;
        TTabControl *TabControl1;
        TGroupBox *GroupBox3;
        TCheckBox *chbEnable;
        TCheckBox *chbSync;
        TCheckBox *chSingle;
        TCheckBox *chbFIFO;
        TButton *btB_GoSignal;
        TButton *bFIFO0Reset;
        TCheckBox *chbRetransmit;
        TListBox *lbB_GoDataSeq;
        TLabel *Label15;
        TButton *bLoadFIFO;
        TButton *bAddData;
        TCheckBox *chbCalibration;
        TButton *bReadBGoSettings;
        TEdit *eBGoSeting;
        TButton *bDeleteData;
        TButton *bL1AFIFOReset;
        TEdit *eCSR1;
        TButton *Button1;
        TGroupBox *GroupBox6;
        TComboBox *lbPresettings;
        TButton *bAplayPreset;
        TButton *bDeletePreset;
        TButton *bAddPreset;
        TEdit *eNewPresetName;
        TfrSelectValue *frSelectReg3;
        TLabel *Label18;
        TPanel *Panel6;
        TImage *Image1;
        TLabel *Label19;
        TLabel *Label20;
        TLabel *Label21;
        TPanel *Panel7;
        TComboBox *cmbTTC;
        TLabel *Label17;
        TEdit *eRxName;
        TEdit *eRxAddres;
        TButton *bAddRxAddres;
        TButton *bRxAddressDelete;
        TGroupBox *GroupBox7;
        TCheckBox *chbTRIGRegEnable;
        TEdit *eTRIGRegSad;
        TLabel *Label22;
        TCheckBox *chbLFACExt;
        TCheckBox *chbTrigRegExt;
        TButton *bTRIGRegSet;
        TPanel *Panel8;
        TPanel *Panel10;
        TButton *bFIFOStaus;
        TButton *bHelp;
    void __fastcall frSelectReg0tbTrackBarChange(TObject *Sender);
    void __fastcall frSelectReg1tbTrackBarChange(TObject *Sender);
    void __fastcall frSelectReg2tbTrackBarChange(TObject *Sender);
    void __fastcall rgTriggerSourceClick(TObject *Sender);
    void __fastcall bResetClick(TObject *Sender);
        void __fastcall btVMETrigerClick(TObject *Sender);
        void __fastcall cbRanGenRateChange(TObject *Sender);
        void __fastcall chbOrbitSignalSelClick(TObject *Sender);
        void __fastcall btAplayInhSetingsClick(TObject *Sender);
        void __fastcall btB_GoSignalClick(TObject *Sender);
        void __fastcall chbEnableClick(TObject *Sender);
        void __fastcall btReadCSR2Click(TObject *Sender);
        void __fastcall btCSR2ResetFIFOClick(TObject *Sender);
        void __fastcall pCntrolRegClick(TObject *Sender);
        void __fastcall bLFACSendClick(TObject *Sender);
        void __fastcall chbLFACExtClick(TObject *Sender);
        void __fastcall bSFACSendClick(TObject *Sender);
        void __fastcall bAddDataClick(TObject *Sender);
        void __fastcall lbB_GoDataSeqDblClick(TObject *Sender);
        void __fastcall TabControl1Change(TObject *Sender);
        void __fastcall chbRetransmitClick(TObject *Sender);
        void __fastcall bFIFO0ResetClick(TObject *Sender);
        void __fastcall bLoadFIFOClick(TObject *Sender);
        void __fastcall chbCalibrationClick(TObject *Sender);
        void __fastcall bReadBGoSettingsClick(TObject *Sender);
        void __fastcall bDeleteDataClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall bL1AFIFOResetClick(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall bAddPresetClick(TObject *Sender);
        void __fastcall frSelectReg1seSpinEditChange(TObject *Sender);
        void __fastcall bDeletePresetClick(TObject *Sender);
        void __fastcall bAplayPresetClick(TObject *Sender);
        void __fastcall bAddRxAddresClick(TObject *Sender);
        void __fastcall frSelectReg2seSpinEditChange(TObject *Sender);
        void __fastcall frSelectReg3seSpinEditChange(TObject *Sender);
        void __fastcall frSelectReg3tbTrackBarChange(TObject *Sender);
        void __fastcall bRxAddressDeleteClick(TObject *Sender);
        void __fastcall bTRIGRegSetClick(TObject *Sender);
        void __fastcall chbTRIGRegEnableClick(TObject *Sender);
        void __fastcall bFIFOStausClick(TObject *Sender);
        void __fastcall bHelpClick(TObject *Sender);
private:	// User declarations
    TTTCvi* TTC;
    unsigned short Convert(unsigned short K)
        {
            unsigned short n, m;
            n = K % 15;
            m = (K/15 - n + 14) %16;
            return 16*n + m;
        }

    unsigned short GetCurrentRX();

    bool CBSkipOnClick;

public:		// User declarations
    //const unsigned short RXAddress;
    __fastcall TfrmMain(TComponent* Owner);

   typedef std::bitset<32> TBitVector;
   typedef std::vector<TBitVector> TVectorSeq;
// TVectorSeq* GetVectorSeq();
   bool B_GoSet[4][4];  //BGoSet[BGo No][Eneble, Sync, Single,FiFo,Retransmit]
//   int VecInFIFO[4];
   std::bitset<4> ResetFIFO;
   std::bitset<4> RetransmitFIFO;

       void WriteIniFile(TIniFile* iniFile ,AnsiString sectionName);
       void ReadIniFile(TIniFile* iniFile ,AnsiString sectionName);
       void PreSettings_ReadIniFile(TIniFile* iniFile, AnsiString sectionName);
       void PreSettings_WriteIniFile(TIniFile* iniFile, AnsiString sectionName);
       void Rx_Addresses_WriteIniFile(TIniFile* iniFile ,AnsiString sectionName);
       void Rx_Addresses_ReadIniFile(TIniFile* iniFile ,AnsiString sectionName);

};
//---------------------------------------------------------------------------
extern PACKAGE TfrmMain *frmMain;
//---------------------------------------------------------------------------
class PreSettings
    {
     public:
     int RanGenRate;
     int TriggerSource;
     bool OrbitSignal;
     int InhibitDelay[4];
     int InhibitDuration[4];
     int Deskew1;
     int Deskew2;
     int CoarDel1;
     int CoarDel2;
     std::bitset<8> RxControlRegister;
     bool B_Go[4][5];
     bool Calibration;
     AnsiString RxName;
     TfrmMain::TVectorSeq* seq[4];
     PreSettings::PreSettings()
       {
         for(int i=0;i<4;i++) seq[i]= new TfrmMain::TVectorSeq;
       }
    };
//---------------------------------------------------------------------------
#endif
