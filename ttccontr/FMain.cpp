//---------------------------------------------------------------------------

#include "precompiled.h"
#pragma hdrstop

#include "FMain.h"
#include "DlgRxControlReg.h"
#include "DlgB_GoData.h"
#include "FDataForSave.h"
#include "tb_vme.h"
#include <fstream>
#include <memory>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "FSelectValue"
#pragma link "CSPIN"
#pragma resource "*.dfm"
#include <sstream>
TfrmMain *frmMain;

using namespace std;
auto_ptr<ofstream> LogStream;

//---------------------------------------------------------------------------
__fastcall TfrmMain::TfrmMain(TComponent* Owner)
    : TForm(Owner)
{
    //G_Debug.SetStream(*new std::ofstream("test.txt"));
    //G_Debug.SetLevel(TDebug::VME_OPER);
    
    CBSkipOnClick = false;
    //TTC = new TTTCvi(new TVMESimul(),0xdddd00);
    //TTC = new TTTCvi(new TVMEWinBit3(),0xdddd00);
    TTC = new TTTCvi(TVMEFactory().Create(),0xdddd00);
    LogStream.reset(new ofstream("ttccontr.log")); 
    G_Log.out(*LogStream);
    G_Debug.SetLevel(TDebug::VME_OPER);
    G_Debug.SetStream(*LogStream);
    G_Log.out() << hex;

   /* cmbTTC->Items->Add("DAQ=0x1234");
    cmbTTC->Items->Add("LINX=0x1111");
    cmbTTC->Items->Add("LIN1=0x0004"); */
    

    frSelectReg0->SetMaxValue(239);
    frSelectReg1->SetMaxValue(239);
    frSelectReg2->SetMaxValue(15);
    frSelectReg3->SetMaxValue(15);
    cbRanGenRate->ItemIndex=0;

    TTC->Reset();
    TTC->b_Go[0]->Settings(0,0,0,0,0);
    TTC->b_Go[1]->Settings(0,0,0,0,0);
    TTC->b_Go[2]->Settings(0,0,0,0,0);
    TTC->b_Go[3]->Settings(0,0,0,0,0);
    TTC->OrbitSignalSel(chbOrbitSignalSel->Checked);

   //reading Ini File
    TIniFile* iniFile;
    iniFile = new TIniFile("ttccontr.ini" );
    ReadIniFile( iniFile, "B_GoSequnces");
    PreSettings_ReadIniFile( iniFile, "PreSettings");
    Rx_Addresses_ReadIniFile(iniFile , "Rx_Addresses");
    cmbTTC->ItemIndex = 0;
    delete iniFile;
}
//---------------------------------------------------------------------------
unsigned short TfrmMain::GetCurrentRX()
{
    AnsiString address;
    TStrings* list = cmbTTC->Items;
    return list->Values[list->Names[cmbTTC->ItemIndex]].ToInt();
}
//---------------------------------------------------------------------------
void __fastcall TfrmMain::frSelectReg0tbTrackBarChange(TObject *Sender)
{
  frSelectReg0->tbTrackBarChange(Sender);

  TTC->WriteRX(GetCurrentRX(), 0, Convert( frSelectReg0->GetValue() ) );
}
//---------------------------------------------------------------------------
void __fastcall TfrmMain::frSelectReg1tbTrackBarChange(TObject *Sender)
{
  frSelectReg1->tbTrackBarChange(Sender);

  TTC->WriteRX(GetCurrentRX(), 1, Convert( frSelectReg1->GetValue() ) );
}
//---------------------------------------------------------------------------
void __fastcall TfrmMain::frSelectReg2tbTrackBarChange(TObject *Sender)
{
  frSelectReg2->tbTrackBarChange(Sender);

  TTC->WriteRX(GetCurrentRX(), 2,(frSelectReg2->GetValue() ) + ((frSelectReg3->GetValue() )<<4)  );
}                                          
//---------------------------------------------------------------------------
void __fastcall TfrmMain::frSelectReg3tbTrackBarChange(TObject *Sender)
{
  frSelectReg3->tbTrackBarChange(Sender);

  TTC->WriteRX(GetCurrentRX(), 2, (frSelectReg2->GetValue() ) + ((frSelectReg3->GetValue() )<<4) );
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::rgTriggerSourceClick(TObject *Sender)
{

    if(rgTriggerSource->ItemIndex==4) btVMETriger->Enabled=true;
    else  btVMETriger->Enabled=false;

    if(rgTriggerSource->ItemIndex==5)
      {
       cbRanGenRate->Enabled=true;
       TTC->SetRanTrigRate(cbRanGenRate->ItemIndex);
      }
    else cbRanGenRate->Enabled=false;

    TTC->SetL1ATriggerSource(rgTriggerSource->ItemIndex);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::bResetClick(TObject *Sender)
{
    TTC->Reset();
}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::btVMETrigerClick(TObject *Sender)
{
  TTC->Trigger();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbRanGenRateChange(TObject *Sender)
{
 TTC->SetRanTrigRate(cbRanGenRate->ItemIndex);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::chbOrbitSignalSelClick(TObject *Sender)
{
  TTC->OrbitSignalSel(chbOrbitSignalSel->Checked);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::btAplayInhSetingsClick(TObject *Sender)
{
  TTC->SetInhibit0(cseInh0Delay->Value,cseInh0Duration->Value);
  TTC->SetInhibit1(cseInh1Delay->Value,cseInh1Duration->Value);
  TTC->SetInhibit2(cseInh2Delay->Value,cseInh2Duration->Value);
  TTC->SetInhibit3(cseInh3Delay->Value,cseInh3Duration->Value);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::btB_GoSignalClick(TObject *Sender)
{           
 TTC->b_Go[TabControl1->TabIndex]->B_GoSignal();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::chbEnableClick(TObject *Sender)
{
  if (CBSkipOnClick)
    return;
  /*if(chbSync->Checked)  chbSync->Caption="Asynchr";
  else   chbSync->Caption="Synchr";

  if(chSingle->Checked)  chSingle->Caption="Repetiv";
  else chSingle->Caption="Single"; */

  bool calibration;
  if(TabControl1->TabIndex==2) calibration =chbCalibration->Checked;
  else  calibration=false;
  
  TTC->b_Go[TabControl1->TabIndex]->Settings(chbEnable->Checked,chbSync->Checked , chSingle->Checked, chbFIFO->Checked,calibration);

  B_GoSet[TabControl1->TabIndex][0]= chbEnable->Checked;
  B_GoSet[TabControl1->TabIndex][1]= chbSync->Checked;
  B_GoSet[TabControl1->TabIndex][2]= chSingle->Checked;
  B_GoSet[TabControl1->TabIndex][3]= chbFIFO->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::btReadCSR2Click(TObject *Sender)
{
  std::bitset<16> CSR2(TTC->ReadCSR2() );
  std::ostringstream ost;
  ost<<CSR2;
  AnsiString str= ost.str().c_str();
  eCSR2->Text=str;
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::btCSR2ResetFIFOClick(TObject *Sender)
{
  std::bitset<4> b(15);
  TTC->ResetBGoFIFO(b);
}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::pCntrolRegClick(TObject *Sender)
{
  dlgRxControlRegister->ShowModal();
  if (dlgRxControlRegister->ModalResult == mrOk)
   {
    std::ostringstream ost;
    ost<<dlgRxControlRegister->GetControlReg();
    pCntrolReg->Caption= ost.str().c_str();
    TTC->WriteRX(GetCurrentRX(),3, (dlgRxControlRegister->GetControlReg()).to_ulong() );      //control register setings
   }
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::bLFACSendClick(TObject *Sender)
{
  if(chbLFACExt->Checked) TTC->WriteExternal(GetCurrentRX(), StrToInt(eLFACSubaddr->Text), StrToInt(eLFACData->Text) );
  else   TTC->WriteRX(GetCurrentRX(), StrToInt(eLFACSubaddr->Text), StrToInt(eLFACData->Text) );
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::chbLFACExtClick(TObject *Sender)
{
  if(chbLFACExt->Checked)  chbLFACExt->Caption="External Subaddr";
  else  chbLFACExt->Caption="Write Rx";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::bSFACSendClick(TObject *Sender)
{                                             
  TTC->WriteShortAsynchronous(StrToInt(eSFACCommand->Text) );
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::bAddDataClick(TObject *Sender)
{
  B_GoDataDlg->CurrentRX=frmMain->GetCurrentRX();
  TVectorSeq* seq = new TVectorSeq;
  B_GoDataDlg->cur_seq = seq;

  B_GoDataDlg->ShowModal();
  if (B_GoDataDlg->ModalResult == mrOk)
  {
    lbB_GoDataSeq->Items->AddObject(B_GoDataDlg->eName->Text, (TObject*) seq);
  }
  else delete seq;
  B_GoDataDlg->lbB_BoData->Items->Clear();
}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::lbB_GoDataSeqDblClick(TObject *Sender)
{
  B_GoDataDlg->CurrentRX=frmMain->GetCurrentRX();
  TVectorSeq* seq = new TVectorSeq;    //copy of sequnce
  *seq = *( (TVectorSeq*)lbB_GoDataSeq->Items->Objects[lbB_GoDataSeq->ItemIndex] );
  B_GoDataDlg->cur_seq = seq;
  B_GoDataDlg->eName->Text= lbB_GoDataSeq->Items->Strings[lbB_GoDataSeq->ItemIndex];
  B_GoDataDlg->ShowSeq();
  B_GoDataDlg->ShowModal();
  if (B_GoDataDlg->ModalResult == mrOk)
  {
   TVectorSeq* buf_seq = (TVectorSeq*)lbB_GoDataSeq->Items->Objects[lbB_GoDataSeq->ItemIndex];   //I got to delete old seq
   lbB_GoDataSeq->Items->Strings[lbB_GoDataSeq->ItemIndex] = B_GoDataDlg->eName->Text;
   lbB_GoDataSeq->Items->Objects[lbB_GoDataSeq->ItemIndex] = (TObject*)seq  ;   //replceing pointers
   delete buf_seq;
  }
  else delete seq;
  B_GoDataDlg->lbB_BoData->Items->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::TabControl1Change(TObject *Sender)
{
  CBSkipOnClick = true;
  try {
    chbEnable->Checked= B_GoSet[TabControl1->TabIndex][0];
    chbSync->Checked=   B_GoSet[TabControl1->TabIndex][1];
    chSingle->Checked=  B_GoSet[TabControl1->TabIndex][2];
    chbFIFO->Checked=   B_GoSet[TabControl1->TabIndex][3];

    chbRetransmit->Checked= !RetransmitFIFO[TabControl1->TabIndex];

    if(TabControl1->TabIndex==2) chbCalibration->Enabled=true;
    else chbCalibration->Enabled = false;
  }
  __finally {
    CBSkipOnClick = false;
  };

}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::chbRetransmitClick(TObject *Sender)
{                                     
  RetransmitFIFO[TabControl1->TabIndex]= !chbRetransmit->Checked ;    //0 mens retransmite
  TTC->RetransmiteBGoFIFO(RetransmitFIFO);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::bFIFO0ResetClick(TObject *Sender)
{
   int i= TabControl1->TabIndex ;
   ResetFIFO[i]=true;
   TTC->ResetBGoFIFO(ResetFIFO);
   ResetFIFO[TabControl1->TabIndex]=false;
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::bLoadFIFOClick(TObject *Sender)
{
 TVectorSeq::iterator iterator;
 TBitVector word;
 TVectorSeq* cur_seq = (TVectorSeq*)lbB_GoDataSeq->Items->Objects[lbB_GoDataSeq->ItemIndex];
  for(iterator=cur_seq->begin();iterator!=cur_seq->end();iterator++)
  {
   word= *iterator;
   TTC->b_Go[TabControl1->TabIndex]->WriteData(word);
  }
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::chbCalibrationClick(TObject *Sender)
{
  TTC->b_Go[TabControl1->TabIndex]->Settings(chbEnable->Checked,chbSync->Checked , chSingle->Checked, chbFIFO->Checked,chbCalibration->Checked);
}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::bReadBGoSettingsClick(TObject *Sender)
{
  std::bitset<5> setings =TTC->b_Go[TabControl1->TabIndex]->ReadSettings();
  std::ostringstream ost;
  ost<<setings;
  AnsiString str= ost.str().c_str();
  eBGoSeting->Text=str;
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::bDeleteDataClick(TObject *Sender)
{
  lbB_GoDataSeq->Items->Delete(lbB_GoDataSeq->ItemIndex);
}
//---------------------------------------------------------------------------

void TfrmMain::WriteIniFile( TIniFile *iniFile, AnsiString sectionName)
{
  int NoOfSeq=lbB_GoDataSeq->Items->Count;
  iniFile->WriteInteger( sectionName, "NoOfSeq" ,NoOfSeq);

  for(int i=0;i< NoOfSeq;i++)
   {
     //zapisanie sekwencji
     TVectorSeq* seq = (TVectorSeq*)lbB_GoDataSeq->Items->Objects[i];
     iniFile->WriteString ( sectionName+"Seq"+IntToStr(i), "Caption", lbB_GoDataSeq->Items->Strings[i]);
     iniFile->WriteInteger( sectionName+"Seq"+IntToStr(i), "NoOfVectors", seq->size());
        TVectorSeq::iterator iterator;
        int vecNo=0;
       for(iterator=seq->begin();iterator!=seq->end();iterator++,vecNo++)
          {
           std::ostringstream ost;
           ost<<(*iterator);
           AnsiString str= ost.str().c_str();
           iniFile->WriteString(sectionName+"Seq"+IntToStr(i),"vector"+IntToStr(vecNo),str);
          }
   }
}
//---------------------------------------------------------------------------

void TfrmMain::ReadIniFile( TIniFile *iniFile, AnsiString sectionName)
{
  int NoOfSeq;
  NoOfSeq = iniFile->ReadInteger( sectionName, "NoOfSeq" ,0);

  for(int i=0;i< NoOfSeq;i++)
   {
     //odczytanie sekwencji
     TVectorSeq* seq;
     seq = new TVectorSeq;
     AnsiString Caption = iniFile->ReadString ( sectionName+"Seq"+IntToStr(i), "Caption"," " );
     int NoOfVectors;
     NoOfVectors=iniFile->ReadInteger(sectionName+"Seq"+IntToStr(i), "NoOfVectors", 0);
       for(int vecNo=0; vecNo < NoOfVectors; vecNo++)
          {
           AnsiString astr;
           astr =iniFile->ReadString(sectionName+"Seq"+IntToStr(i),"vector"+IntToStr(vecNo),"0");
           std::string str=astr.c_str();
           TBitVector vector(str);
           seq->push_back(vector);
          }
     lbB_GoDataSeq->Items->AddObject( Caption, (TObject*) seq);
   }
}
//---------------------------------------------------------------------------
void TfrmMain::Rx_Addresses_WriteIniFile(TIniFile* iniFile ,AnsiString sectionName)
{
 int NoOfRxAddresses = cmbTTC->Items->Count;
 iniFile->WriteInteger( sectionName, "NoOfRxAddresses" , NoOfRxAddresses);

  for(int i=0; i < NoOfRxAddresses; i++)
    iniFile->WriteString( sectionName, "RxAddress"+IntToStr(i) , cmbTTC->Items->Strings[i]);

}
//---------------------------------------------------------------------------
void TfrmMain::Rx_Addresses_ReadIniFile(TIniFile* iniFile ,AnsiString sectionName)
{
  int NoOfRxAddresses;
  NoOfRxAddresses = iniFile->ReadInteger( sectionName, "NoOfRxAddresses" , 0);
  for(int i=0; i < NoOfRxAddresses; i++)
     cmbTTC->Items->Add(iniFile->ReadString( sectionName, "RxAddress"+IntToStr(i) , " "));
}
//---------------------------------------------------------------------------
void __fastcall TfrmMain::FormClose(TObject *Sender, TCloseAction &Action)
{
     TIniFile* iniFile;
     iniFile = new TIniFile("ttccontr.ini" );
     WriteIniFile( iniFile, "B_GoSequnces");
     PreSettings_WriteIniFile( iniFile, "PreSettings");
     Rx_Addresses_WriteIniFile(iniFile ,"Rx_Addresses");
     delete iniFile;
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::bL1AFIFOResetClick(TObject *Sender)
{
 TTC->L1A_FIFOReset(true);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::Button1Click(TObject *Sender)
{
  std::bitset<16> CSR1(TTC->ReadCSR1() );
  std::ostringstream ost;
  ost<<CSR1;
  AnsiString str= ost.str().c_str();
  eCSR1->Text=str;
}
//---------------------------------------------------------------------------

void TfrmMain::PreSettings_ReadIniFile(TIniFile* iniFile, AnsiString sectionName)
{
  int NoOfPreSets;
  NoOfPreSets = iniFile->ReadInteger( sectionName, "NoOfPreSets" ,0);

  for(int i=0;i< NoOfPreSets;i++)
   {
     //odczytanie Presetow
     PreSettings* preset;
     preset = new PreSettings;
     AnsiString Caption = iniFile->ReadString ( sectionName+"Preset"+IntToStr(i), "Caption"," " );

      preset->RanGenRate =iniFile->ReadInteger(sectionName+"Preset"+IntToStr(i),"RanGenRate",0);
      preset->TriggerSource =iniFile->ReadInteger(sectionName+"Preset"+IntToStr(i),"TriggerSource",0);
      preset->OrbitSignal =iniFile->ReadInteger(sectionName+"Preset"+IntToStr(i),"OrbitSignal",0);
      for (int j=0;j<4;j++)
        preset->InhibitDelay[j] =iniFile->ReadInteger(sectionName+"Preset"+IntToStr(i),"InhibitDelay"+IntToStr(j),0);
      for (int j=0;j<4;j++)
        preset->InhibitDuration[j] =iniFile->ReadInteger(sectionName+"Preset"+IntToStr(i),"InhibitDuration"+IntToStr(j),0);

      preset->Deskew1 =iniFile->ReadInteger(sectionName+"Preset"+IntToStr(i),"Deskew1",0);
      preset->Deskew2 =iniFile->ReadInteger(sectionName+"Preset"+IntToStr(i),"Deskew2",0);
      preset->CoarDel1 =iniFile->ReadInteger(sectionName+"Preset"+IntToStr(i),"CoarDel1",0);
      preset->CoarDel2 =iniFile->ReadInteger(sectionName+"Preset"+IntToStr(i),"CoarDel2",0);
      for (int j=0;j<4;j++)
        for(int k=0;k<6;k++)
        preset->B_Go[j][k] =iniFile->ReadInteger(sectionName+"Preset"+IntToStr(i),"B_Go"+IntToStr(j)+IntToStr(k),0);

      preset->Calibration=iniFile->ReadInteger(sectionName+"Preset"+IntToStr(i),"Calibration",0);

      for (int j=0;j<8;j++)
        preset->RxControlRegister[j]=iniFile->ReadInteger(sectionName+"Preset"+IntToStr(i),"RxControlRegister"+IntToStr(j),0);

      preset->RxName= iniFile->ReadString ( sectionName+"Preset"+IntToStr(i), "RxName"," " );

     //readin b-go data
      for(int i=0;i< 4;i++)
        {
         //odczytanie sekwencji
         int NoOfVectors;
         NoOfVectors=iniFile->ReadInteger(sectionName+"Seq"+IntToStr(i), "NoOfVectors", 0);
           for(int vecNo=0; vecNo < NoOfVectors; vecNo++)
              {
               preset->seq[i]=new  TVectorSeq;
               AnsiString astr;
               astr =iniFile->ReadString(sectionName+"Seq"+IntToStr(i),"vector"+IntToStr(vecNo),"0");
               std::string str=astr.c_str();
               TBitVector vector(str);
               preset->seq[i]->push_back(vector);
              }
       }

     lbPresettings->Items->AddObject( Caption, (TObject*) preset);
   }
}

//---------------------------------------------------------------------------
void TfrmMain::PreSettings_WriteIniFile(TIniFile* iniFile, AnsiString sectionName)
{
  int NoOfPreSets=lbPresettings->Items->Count;;
  iniFile->WriteInteger( sectionName, "NoOfPreSets" ,NoOfPreSets);

  for(int i=0;i< NoOfPreSets;i++)
   {
     //zapisanie Presetow
     PreSettings* preset = (PreSettings*)lbPresettings->Items->Objects[i];
     iniFile->WriteString ( sectionName+"Preset"+IntToStr(i), "Caption",lbPresettings->Items->Strings[i] );

     iniFile->WriteInteger(sectionName+"Preset"+IntToStr(i),"RanGenRate",preset->RanGenRate);
     iniFile->WriteInteger(sectionName+"Preset"+IntToStr(i),"TriggerSource",preset->TriggerSource);
     iniFile->WriteInteger(sectionName+"Preset"+IntToStr(i),"OrbitSignal",preset->OrbitSignal);
      for (int j=0;j<4;j++)
         iniFile->WriteInteger(sectionName+"Preset"+IntToStr(i),"InhibitDelay"+IntToStr(j),preset->InhibitDelay[j]);
      for (int j=0;j<4;j++)
         iniFile->WriteInteger(sectionName+"Preset"+IntToStr(i),"InhibitDuration"+IntToStr(j),preset->InhibitDuration[j]);

      iniFile->WriteInteger(sectionName+"Preset"+IntToStr(i),"Deskew1",preset->Deskew1);
      iniFile->WriteInteger(sectionName+"Preset"+IntToStr(i),"Deskew2",preset->Deskew2);
      iniFile->WriteInteger(sectionName+"Preset"+IntToStr(i),"CoarDel1",preset->CoarDel1);
      iniFile->WriteInteger(sectionName+"Preset"+IntToStr(i),"CoarDel2",preset->CoarDel2);
      for (int j=0;j<4;j++)
        for(int k=0;k<5;k++)
         iniFile->WriteInteger(sectionName+"Preset"+IntToStr(i),"B_Go"+IntToStr(j)+IntToStr(k),preset->B_Go[j][k]);

      for (int j=0;j<8;j++)
        iniFile->WriteInteger(sectionName+"Preset"+IntToStr(i),"RxControlRegister"+IntToStr(j),preset->RxControlRegister[j]);

     iniFile->WriteInteger(sectionName+"Preset"+IntToStr(i),"Calibration",preset->Calibration);
     iniFile->WriteString( sectionName+"Preset"+IntToStr(i), "RxName", preset->RxName);

     //savin b-go data
     for(int i=0;i< 4;i++)
       {
         //zapisanie sekwencji

         iniFile->WriteInteger( sectionName+"Seq"+IntToStr(i), "NoOfVectors", preset->seq[i]->size());
            TVectorSeq::iterator iterator;
            int vecNo=0;

           for(iterator=preset->seq[i]->begin();iterator!=preset->seq[i]->end();iterator++,vecNo++)
              {
               std::ostringstream ost;
               ost<<(*iterator);
               AnsiString str= ost.str().c_str();
               iniFile->WriteString(sectionName+"Seq"+IntToStr(i),"vector"+IntToStr(vecNo),str);
              }

       }
   }
};
//------------------------------------------------------------------------------------------------------------------------------------
void __fastcall TfrmMain::bAddPresetClick(TObject *Sender)
{
  PreSettings* preset;
  preset = new PreSettings;

  preset->RanGenRate =cbRanGenRate->ItemIndex;
  preset->TriggerSource =rgTriggerSource->ItemIndex;
  preset->OrbitSignal = chbOrbitSignalSel->Checked;
  preset->InhibitDelay[0] =cseInh0Delay->Value;
  preset->InhibitDelay[1] =cseInh1Delay->Value;
  preset->InhibitDelay[2] =cseInh2Delay->Value;
  preset->InhibitDelay[3] =cseInh3Delay->Value;
  preset->InhibitDuration[0] =cseInh0Duration->Value;
  preset->InhibitDuration[1] =cseInh1Duration->Value;
  preset->InhibitDuration[2] =cseInh2Duration->Value;
  preset->InhibitDuration[3] =cseInh3Duration->Value;
  preset->Deskew1 = frSelectReg0->GetValue();
  preset->Deskew2 = frSelectReg1->GetValue();
  preset->CoarDel1 = frSelectReg2->GetValue();
  preset->CoarDel2 = frSelectReg3->GetValue();


  for (int j=0;j<4;j++)
  {
    for(int k=0;k<4;k++)
        preset->B_Go[j][k] = B_GoSet[j][k];
  preset->B_Go[j][4] = RetransmitFIFO[j];
  }
  preset->Calibration=chbCalibration->Checked;
  preset->RxControlRegister=dlgRxControlRegister->GetControlReg();

  preset->RxName = cmbTTC->Items->Names[cmbTTC->ItemIndex];

  //saving b-go fifo data
  frmDataForSave->lbB_GoDataSeq->Items->Clear();
  for(int i=0; i < lbB_GoDataSeq->Items->Count;i++)
    frmDataForSave->lbB_GoDataSeq->Items->Add(lbB_GoDataSeq->Items->Strings[i]);
  for(int i=0;i<4;i++)
  {
    frmDataForSave->Label3->Caption=IntToStr(i);
    frmDataForSave->ShowModal();
    if (frmDataForSave->ModalResult == mrOk)
     {
       *(preset->seq[i])= *((TVectorSeq*)lbB_GoDataSeq->Items->Objects[frmDataForSave->lbB_GoDataSeq->ItemIndex]);
     }
    else
       preset->seq[i]->clear();
  }

  lbPresettings->Items->AddObject( eNewPresetName->Text, (TObject*) preset);

}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::frSelectReg1seSpinEditChange(TObject *Sender)
{
  frSelectReg1->seSpinEditChange(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::bDeletePresetClick(TObject *Sender)
{
  lbPresettings->Items->Delete(lbPresettings->ItemIndex);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::bAplayPresetClick(TObject *Sender)
{
  PreSettings* preset=(PreSettings*)lbPresettings->Items->Objects[lbPresettings->ItemIndex];
  //setting inteface
   cbRanGenRate->ItemIndex= preset->RanGenRate;
   rgTriggerSource->ItemIndex= preset->TriggerSource;
   chbOrbitSignalSel->Checked= preset->OrbitSignal;
   cseInh0Delay->Value = preset->InhibitDelay[0];
   cseInh1Delay->Value= preset->InhibitDelay[1];
   cseInh2Delay->Value= preset->InhibitDelay[2];
   cseInh3Delay->Value = preset->InhibitDelay[3];
   cseInh0Duration->Value= preset->InhibitDuration[0];
   cseInh1Duration->Value= preset->InhibitDuration[1];
   cseInh2Duration->Value= preset->InhibitDuration[2];
   cseInh3Duration->Value= preset->InhibitDuration[3];
   frSelectReg0->SetValue(preset->Deskew1);
   frSelectReg1->SetValue(preset->Deskew2);
   frSelectReg2->SetValue(preset->CoarDel1);
   frSelectReg3->SetValue(preset->CoarDel2);
   TStrings* list = cmbTTC->Items;
   cmbTTC->ItemIndex = list->IndexOfName(preset->RxName);

  for (int j=0;j<4;j++)
  {
    for(int k=0;k<4;k++)
         B_GoSet[j][k]= preset->B_Go[j][k];
    RetransmitFIFO[j] = preset->B_Go[j][4];
  }
  chbCalibration->Checked=preset->Calibration;
  dlgRxControlRegister->SetControlReg(preset->RxControlRegister);

  //---------------------applaing
  cbRanGenRateChange(Sender);
  rgTriggerSourceClick(Sender);
  chbOrbitSignalSelClick(Sender);
  btAplayInhSetingsClick(Sender);
  frSelectReg0tbTrackBarChange(Sender);
  frSelectReg1tbTrackBarChange(Sender);
  frSelectReg2tbTrackBarChange(Sender);
  int i;
  for(i=0; i<2;i++)
   TTC->b_Go[i]->Settings(preset->B_Go[i][0],preset->B_Go[i][1] ,preset->B_Go[i][2] ,preset->B_Go[i][3] ,false);
  i=2;
  TTC->b_Go[i]->Settings(preset->B_Go[i][0],preset->B_Go[i][1] ,preset->B_Go[i][2] ,preset->B_Go[i][3] ,preset->Calibration);
  i=3;
  TTC->b_Go[i]->Settings(preset->B_Go[i][0],preset->B_Go[i][1] ,preset->B_Go[i][2] ,preset->B_Go[i][3] ,false);

  std::ostringstream ost;
  ost<<dlgRxControlRegister->GetControlReg();
  pCntrolReg->Caption= ost.str().c_str();
  TTC->WriteRX(GetCurrentRX(),3, (dlgRxControlRegister->GetControlReg()).to_ulong() );

  chbRetransmitClick(Sender);
  TfrmMain::TabControl1Change(Sender);

  //load fifo
   std::bitset<4> b(15);
   TTC->ResetBGoFIFO(b);

    for(int i=0; i<4;i++)
    {
      TVectorSeq::iterator iterator;
      TBitVector word;
      TVectorSeq* cur_seq = preset->seq[i];
      if(!cur_seq->empty())
       {
        for(iterator=cur_seq->begin();iterator!=cur_seq->end();iterator++)
         {
          word= *iterator;
          TTC->b_Go[i]->WriteData(word);

          MessageDlg("B-Go Fifo [" +IntToStr(i)+ "] loaded", mtInformation , TMsgDlgButtons() << mbOK , 0);
         }
       }
    }
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::bAddRxAddresClick(TObject *Sender)
{
  cmbTTC->Items->Add(eRxName->Text + "=" + eRxAddres->Text);
  cmbTTC->ItemIndex= cmbTTC->Items->Count - 1;
}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::frSelectReg2seSpinEditChange(TObject *Sender)
{
  frSelectReg2->seSpinEditChange(Sender);

}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::frSelectReg3seSpinEditChange(TObject *Sender)
{
  frSelectReg3->seSpinEditChange(Sender);

}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

void __fastcall TfrmMain::bRxAddressDeleteClick(TObject *Sender)
{
  cmbTTC->Items->Delete(cmbTTC->ItemIndex);
}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::bTRIGRegSetClick(TObject *Sender)
{
  TTC->SetTRIGWORD(GetCurrentRX(), StrToInt(eTRIGRegSad->Text), chbTrigRegExt->Checked, true);
}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::chbTRIGRegEnableClick(TObject *Sender)
{
  if(chbTRIGRegEnable->Checked==false)
   {
     TTC->SetTRIGWORD(0, 0, 0, false);
     bTRIGRegSet->Enabled=false;
     eTRIGRegSad->Enabled=false;
     chbTrigRegExt->Enabled=false;
   }
  else
  {
     TTC->SetTRIGWORD(GetCurrentRX(), StrToInt(eTRIGRegSad->Text), chbTrigRegExt->Checked, true);
     bTRIGRegSet->Enabled=true;
     eTRIGRegSad->Enabled=true;
     chbTrigRegExt->Enabled=true;
  }
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::bFIFOStausClick(TObject *Sender)
{
  std::bitset<16> CSR2(TTC->ReadCSR2() );
  AnsiString status;
  status = "LOADED";
  if( CSR2[2* TabControl1->TabIndex] )
   status = "EMPTY";
  if( CSR2[1 + 2* TabControl1->TabIndex] )
   status = "FULL";
  MessageDlg("B-Go Fifo [" +IntToStr(TabControl1->TabIndex)+ "] " + status, mtInformation , TMsgDlgButtons() << mbOK , 0);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::bHelpClick(TObject *Sender)
{
  Application->HelpFile = "Probny.hlp";
  Application->HelpCommand(HELP_CONTENTS, 0);
}
//---------------------------------------------------------------------------
    
