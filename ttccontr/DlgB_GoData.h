//----------------------------------------------------------------------------
#ifndef DlgB_GoDataH
#define DlgB_GoDataH
//----------------------------------------------------------------------------
#include <vcl\System.hpp>
#include <vcl\Windows.hpp>
#include <vcl\SysUtils.hpp>
#include <vcl\Classes.hpp>
#include <vcl\Graphics.hpp>
#include <vcl\StdCtrls.hpp>
#include <vcl\Forms.hpp>
#include <vcl\Controls.hpp>
#include <vcl\Buttons.hpp>
#include <vcl\ExtCtrls.hpp>
#include <bitset>
#include <vector>
//----------------------------------------------------------------------------
class TB_GoDataDlg : public TForm
{
__published:        
	TButton *OKBtn;
	TButton *CancelBtn;
	TBevel *Bevel1;
        TListBox *lbB_BoData;
        TPanel *Panel1;
        TCheckBox *chbExternal;
        TLabel *Label13;
        TLabel *Label12;
        TEdit *eSubaddr;
        TEdit *eData;
        TButton *bAddLong;
        TButton *bDelete;
        TPanel *Panel2;
        TLabel *Label14;
        TEdit *eCommand;
        TButton *AddShort;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label3;
        TEdit *eRxAddres;
        TEdit *eName;
        TLabel *Label4;
        TLabel *Label5;
        TLabel *Label6;
        TBevel *Bevel2;
        TButton *bCurRx;
        void __fastcall bAddLongClick(TObject *Sender);
        void __fastcall AddShortClick(TObject *Sender);
        void __fastcall lbB_BoDataClick(TObject *Sender);
        void __fastcall bDeleteClick(TObject *Sender);
        void __fastcall bCurRxClick(TObject *Sender);
private:
public:
	virtual __fastcall TB_GoDataDlg(TComponent* AOwner);
        typedef std::bitset<32> TBitVector;
        typedef std::vector<TBitVector> TVectorSeq;
        TVectorSeq* cur_seq;
        void ShowSeq();
        int CurrentRX;
};
//----------------------------------------------------------------------------
extern PACKAGE TB_GoDataDlg *B_GoDataDlg;
//----------------------------------------------------------------------------
#endif    
