//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("ttccontr.res");
USEFORM("FMain.cpp", frmMain);
USELIB("..\bbin\bit3_api_omf.lib");
USEFORM("DlgRxControlReg.cpp", dlgRxControlRegister);
USEFORM("DlgB_GoData.cpp", B_GoDataDlg);
USEFORM("DlgDataForSave.cpp", OKBottomDlg);
USEFORM("FDataForSave.cpp", frmDataForSave);
USEUNIT("TTCviN.cpp");
USEFORM("..\util\FSelectValue.cpp", frSelectValue); /* TFrame: File Type */
USELIB("..\bbin\tbstd.lib");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
    try
    {
         Application->Initialize();
         Application->Title = "TTC Control";
         Application->CreateForm(__classid(TfrmMain), &frmMain);
         Application->CreateForm(__classid(TdlgRxControlRegister), &dlgRxControlRegister);
         Application->CreateForm(__classid(TB_GoDataDlg), &B_GoDataDlg);
         Application->CreateForm(__classid(TOKBottomDlg), &OKBottomDlg);
         Application->CreateForm(__classid(TfrmDataForSave), &frmDataForSave);
         Application->Run();
    }
    catch (Exception &exception)
    {
         Application->ShowException(&exception);
    }
    return 0;
}
//---------------------------------------------------------------------------
