//----------------------------------------------------------------------------
#ifndef FDataForSaveH
#define FDataForSaveH
//----------------------------------------------------------------------------
#include <vcl\System.hpp>
#include <vcl\Windows.hpp>
#include <vcl\SysUtils.hpp>
#include <vcl\Classes.hpp>
#include <vcl\Graphics.hpp>
#include <vcl\StdCtrls.hpp>
#include <vcl\Forms.hpp>
#include <vcl\Controls.hpp>
#include <vcl\Buttons.hpp>
#include <vcl\ExtCtrls.hpp>
//----------------------------------------------------------------------------
class TfrmDataForSave : public TForm
{
__published:        
	TButton *OKBtn;
	TButton *CancelBtn;
	TBevel *Bevel1;
        TListBox *lbB_GoDataSeq;
        TLabel *Label15;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label3;
private:
public:
	virtual __fastcall TfrmDataForSave(TComponent* AOwner);
};
//----------------------------------------------------------------------------
extern PACKAGE TfrmDataForSave *frmDataForSave;
//----------------------------------------------------------------------------
#endif    
