#ifndef ITTCRX_H
#define ITTCRX_H

class ITTCrx {
public:
  virtual ~ITTCrx() {}

  virtual bool IsInterfaceI2C() = 0;

  virtual void WriteFineDelay1(int delay) = 0;
  virtual int ReadFineDelay1() = 0;

  virtual void WriteFineDelay2(int delay) = 0;
  virtual int ReadFineDelay2() = 0;
  
  virtual void WriteCoarseDelay(int delay1, int delay2) = 0;
  virtual void ReadCoarseDelay(int& delay1, int& delay2) = 0;

  virtual void WriteControlReg(int value) = 0;
  virtual int ReadControlReg() = 0;

  virtual int ReadSingleErrorCount() = 0;
  virtual int ReadDoubleErrorCount() = 0;   
  virtual int ReadSEUErrorCount() = 0;

  virtual int ReadID() = 0;
  virtual int ReadMasterModeA() = 0;
  virtual int ReadMasterModeB() = 0;

  virtual int ReadConfig1() = 0;  
  virtual int ReadConfig2() = 0;
  virtual int ReadConfig3() = 0;

  virtual int ReadStatus() = 0;

  virtual int ReadBunchCounter() = 0;

  virtual int ReadEventCounter() = 0;
};


#endif
