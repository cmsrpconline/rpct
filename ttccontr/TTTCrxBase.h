//---------------------------------------------------------------------------

#ifndef TTTCrxBaseH
#define TTTCrxBaseH
//---------------------------------------------------------------------------

#include "ITTCrx.h"
#include "tb_std.h"

class TTTCrxBase : public ITTCrx {
public:
  static const unsigned char FineDelay1Addr = 0;
  static const unsigned char FineDelay2Addr = 1;
  static const unsigned char ControlRegisterAddr = 3;
  unsigned short Convert(unsigned short K) // returns nm
    {
      unsigned short n, m;
      n = K % 15;
      m = (K/15 - n + 14) %16;
      return 16*n + m;
    }

  unsigned short UnConvert(unsigned short nm) // returns K
    {
      unsigned short n, m;
      n = nm / 16;
      m = nm % 16;                        
      return (m * 15 + n * 16 + 30) % 240;
    }

protected:
  virtual void WriteReg(unsigned char address, unsigned char data) = 0;
  virtual unsigned char ReadReg(unsigned char address) = 0;
  bool InterfaceI2C;
  int FineDelay1;
  int FineDelay2;
  int CoarseDelay1;
  int CoarseDelay2;

  std::string boardName_;
public:
  TTTCrxBase(bool interfaceI2C, std::string boardName)
    : InterfaceI2C(interfaceI2C), FineDelay1(-1), FineDelay2(-1), CoarseDelay1(-1),
      CoarseDelay2(-1), boardName_(boardName) {}
  
  virtual bool IsInterfaceI2C()
    {
      return InterfaceI2C;
    }
    
  virtual void WriteFineDelay1(int delay)
    {          
      WriteReg(FineDelay1Addr, Convert(delay));
      FineDelay1 = delay;
    }

  virtual int ReadFineDelay1()
    {
      return UnConvert(ReadReg(FineDelay1Addr));
    }

  virtual int GetFineDelay1()
    {
      return FineDelay1;
    }

  virtual void SetFineDelay1(int delay)
    {
      FineDelay1 = delay;
    }

  virtual void WriteFineDelay2(int delay)
    {
      WriteReg(FineDelay2Addr, Convert(delay));
      FineDelay2 = delay;
    }

  virtual int ReadFineDelay2()
    {
      return UnConvert(ReadReg(FineDelay2Addr));
    }                   

  virtual int GetFineDelay2()
    {
      return FineDelay2;
    }
  virtual void SetFineDelay2(int delay)
    {
      FineDelay2 = delay;
    }

  virtual void WriteCoarseDelay(int delay1, int delay2)
    {
      WriteReg(2, delay1 + (delay2 << 4));
      CoarseDelay1 = delay1;
      CoarseDelay2 = delay2;
    }

  virtual void ReadCoarseDelay(int& delay1, int& delay2)
    {
      unsigned int val = ReadReg(2);
      delay1 = val & 0xf;
      delay2 = (val >> 4) & 0xf;
    }

  virtual int GetCoarseDelay1()
    {
      return CoarseDelay1;
    }

  virtual int GetCoarseDelay2()
    {
      return CoarseDelay2;
    }

  virtual void WriteControlReg(int value)
    {
      WriteReg(ControlRegisterAddr, value);
    }

  virtual int ReadControlReg()
    {
      return ReadReg(3);
    }

  virtual int ReadSingleErrorCount()
    {
      if (InterfaceI2C) {
        int result = ReadReg(8);
        return result | ((ReadReg(9) << 8) & 0xff00);
      }
      throw TException("Operation not available");
    }

  virtual int ReadDoubleErrorCount()
    {
      if (InterfaceI2C)
        return ReadReg(10);
      throw TException("Operation not available");
    }

  virtual int ReadSEUErrorCount()
    {
      if (InterfaceI2C)
        return ReadReg(11);
      throw TException("Operation not available");
    }

  virtual int ReadID()
    {
      return ReadReg(InterfaceI2C ? 16 : 4);
    }
    
  virtual int ReadMasterModeA()
    {
      return ReadReg(InterfaceI2C ? 17 : 5);
    }

  virtual int ReadMasterModeB()
    {
      return ReadReg(InterfaceI2C ? 18 : 6);
    }


  virtual int ReadConfig1()
    {
      return ReadReg(InterfaceI2C ? 19 : 7);
    }

  virtual int ReadConfig2()
    {
      return ReadReg(InterfaceI2C ? 20 : 8);
    }

  virtual int ReadConfig3()
    {
      return ReadReg(InterfaceI2C ? 21 : 9);
    }  

  virtual int ReadStatus()
    {
      if (InterfaceI2C)
        return ReadReg(22);
      throw TException("Operation not available");
    }

  virtual int ReadBunchCounter()
    {
      if (InterfaceI2C) {
        int result = ReadReg(24);
        return result | ((ReadReg(25) << 8) & 0xff00);
      }
      throw TException("Operation not available");
    }

  virtual int ReadEventCounter()
    {
      if (InterfaceI2C) {
        int result = ReadReg(26);
        result |= ((ReadReg(27) <<  8) & 0x00ff00);
        result |= ((ReadReg(28) << 16) & 0xff0000);
        return result;
      }
      throw TException("Operation not available");
    }
};

#endif
