//---------------------------------------------------------------------

#include "precompiled.h"
#pragma hdrstop

#include "DlgRxControlReg.h"
//--------------------------------------------------------------------- 
#pragma resource "*.dfm"
TdlgRxControlRegister *dlgRxControlRegister;
//---------------------------------------------------------------------
__fastcall TdlgRxControlRegister::TdlgRxControlRegister(TComponent* AOwner)
	: TForm(AOwner)
{
}
//---------------------------------------------------------------------

TdlgRxControlRegister::TControlReg TdlgRxControlRegister::GetControlReg()
{
 contolReg[0]=chb0->Checked;
 contolReg[1]=chb1->Checked;
 contolReg[2]=chb2->Checked;
 contolReg[3]=chb3->Checked;
 contolReg[4]=chb4->Checked;
 contolReg[5]=chb5->Checked;
 contolReg[6]=chb6->Checked;
 contolReg[7]=chb7->Checked;
 return  contolReg;
}

void TdlgRxControlRegister::SetControlReg(TControlReg b)
{
 contolReg=b;
 chb0->Checked= contolReg[0];
 chb1->Checked= contolReg[1];
 chb2->Checked= contolReg[2];
 chb3->Checked= contolReg[3];
 chb4->Checked= contolReg[4];
 chb5->Checked= contolReg[5];
 chb6->Checked= contolReg[6];
 chb7->Checked= contolReg[7];
}
