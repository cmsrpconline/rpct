<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="rpct-jsf-components" prefix="rpct"%>
<f:view>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" >
	<head>
		<title>Boot</title>
		<jsp:include page="/links/layout/head.jsp" />
	</head>
	<body>
	<div id="header"><jsp:include page="/links/layout/header.jsp" /></div>
	<div id="leftmenu">
		<h:form id="leftMenuForm">		
			<rpct:navigatorList immediate="false" 
				binding="#{MenuBean.bootNavList}"
				headerClass="leftNavHeader"	containerClass="leftNavContainer" 
				currentClass="leftNavCurrent" activeClass="leftNavActive" listClass="leftNavList">
			</rpct:navigatorList>
		</h:form> 
	</div>

	<div id="body">
	<h3><h:outputText value="Boot Files" /></h3>	
	<h:form>
		<div class="prettyTable"><h:dataTable rowClasses="odd,even"
			cellpadding="3" value="#{BootBean.bootFiles}" var="bootFile"
			binding="#{BootBean.bootFileTable}">

			<h:column>
				<f:facet name="header">
					<h:outputText value="File Name" />
				</f:facet>
				<h:outputText value="#{bootFile.fileName}" />
			</h:column>

			<h:column>
				<f:facet name="header">
					<h:outputText value="Description" />
				</f:facet>
				<h:outputText value="#{bootFile.description}" />
			</h:column>

			<h:column>
				<f:facet name="header">
					<h:outputText value="Type" />
				</f:facet>
				<h:outputText value="#{bootFile.bootFileType}" />
			</h:column>

			<h:column>
				<h:commandButton action="#{BootBean.deleteBootFile}" value="Delete" />
			</h:column>
		</h:dataTable></div>
	</h:form> 
	
	<h3><h:outputText value="Add new boot file" /></h3>
	<h:form>
		<h:panelGrid columns="8">
			<h:outputLabel value="File Name:" for="newFileName" />
			<h:inputText id="newFileName" value="#{BootBean.newFileName}"
				required="true" />
			<h:message for="newFileName" />

			<h:outputLabel value="Description:" for="newFileDescription" />
			<h:inputText id="newFileDescription"
				value="#{BootBean.newFileDescription}" required="true" />

			<h:outputLabel value="Type:" for="newFileType" />
			<h:inputText id="newFileType" value="#{BootBean.newFileType}"
				required="true" />

			<h:commandButton id="submitAdd" action="#{BootBean.addNewFile}"
				value="Add" />
		</h:panelGrid>
	</h:form></div>
	
	</body>
	</html>
</f:view>
