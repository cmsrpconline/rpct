<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://xmlns.oracle.com/adf/faces" prefix="af"%>
<%@ taglib uri="http://xmlns.oracle.com/adf/faces/html" prefix="afh"%>
<%@ taglib uri="rpct-jsf-components" prefix="rpct"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<f:view>
	<afh:html>
	<afh:head title="II Access" />
	<afh:body>
		<af:panelHeader text="II Access">
			<af:form>
				<af:panelGroup layout="horizontal">
					<af:commandButton actionListener="#{IIAccessBean.getCrates}"
						text="Reload Device List" />
					<%--<af:selectOneRadio value="#{IIAccessBean.converterId}"
						autoSubmit="true" labelAndAccessKey="Representation:"
						layout="horizontal">
						<f:selectItems value="#{IIAccessBean.converterSelectItems}" />
					</af:selectOneRadio>--%>
				</af:panelGroup>
			</af:form> 
		</af:panelHeader>
		<af:form>
			<af:treeTable var="itemData" value="#{IIAccessBean.devicesTreeModel}" 
				summary="Devices Tree" binding="#{IIAccessBean.devicesTree}"
				rowsByDepth="100">
				<f:facet name="nodeStamp">
					<af:column>
						<f:facet name="header">
							<af:outputText value="Label" />
						</f:facet>
						<af:outputText value="#{itemData.name}" />
					</af:column>
				</f:facet>
				<f:facet name="pathStamp">
					<af:outputText value="#{itemData.name}" />
				</f:facet>
				<af:column>
					<f:facet name="header">
						<af:outputText value="Type" />
					</f:facet>
					<af:outputText value="#{itemData.type}" />
				</af:column>
				<af:column formatType="number">
					<f:facet name="header">
						<af:outputText value="Width" />
					</f:facet>
					<af:outputText value="#{itemData.width}" />
				</af:column>
				<af:column formatType="number">
					<f:facet name="header">
						<af:outputText value="Read Value" />
					</f:facet>
					<af:outputText value="#{itemData.readValue}"
						converter="#{IIAccessBean.converter}" partialTriggers="cRead" inlineStyle="font-family: monospace;"/>
				</af:column>
				<af:column formatType="number">
					<f:facet name="header">
						<af:outputText value="Write Value" />
					</f:facet>
					<af:inputText value="#{itemData.writeValue}"
						inlineStyle="width: 8em; text-align: right; font-family: monospace;"
						rendered="#{itemData.writeable}" 
						converter="HEX_CONVERTER"
						partialTriggers="cRead cWrite">
					</af:inputText>
					<%--<af:message for="itWriteValue" partialTriggers="*"/>--%>
				</af:column>
				<af:column formatType="icon">
					<f:facet name="header">
						<af:outputText value="Action" />
					</f:facet>
					<af:panelGroup layout="horizontal">
						<af:commandButton id="cRead"
							actionListener="#{IIAccessBean.readTree}"
							rendered="#{itemData.readable}" text="Read" 
							partialSubmit="#{not itemData.nestedRead}" />
						<af:outputText value=" " />
						<af:commandButton id="cWrite"
							actionListener="#{IIAccessBean.writeTree}"
							rendered="#{itemData.writeable}" text="Write"
							partialSubmit="false" /> 
							<!-- we do not use partial submit here, as this causes problems with display of conversion errors -->
					</af:panelGroup>
				</af:column>
			</af:treeTable>
		</af:form>
	</afh:body>
	</afh:html>
</f:view>
