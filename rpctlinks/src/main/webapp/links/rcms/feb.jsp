<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://xmlns.oracle.com/adf/faces" prefix="af"%>
<%@ taglib uri="http://xmlns.oracle.com/adf/faces/html" prefix="afh"%>
<%@ taglib uri="rpct-jsf-components" prefix="rpct"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<f:view>
	<afh:html>
	<afh:head title="FEB Access" />
	<afh:body>
		<af:panelHeader text="FEB Access" />
		<af:form>
			<af:panelBox text="Configuration" background="medium">
				<af:panelGroup layout="horizontal">
					<af:inputText id="xdaqUrl" label="XDAQ URL:" value="#{FebBean.xdaqUrl}"
						required="true" />
					<af:inputText id="xdaqAppInstance" label="Instance:" value="#{FebBean.xdaqAppInstance}"
						required="true" />
					<af:message for="xdaqUrl" />
					<af:commandButton actionListener="#{FebBean.setup}" text="Setup" />
				</af:panelGroup>
			</af:panelBox>
		</af:form>

		<h:panelGroup rendered="#{FebBean.ready}">
			<af:form>
				<h:panelGrid>
				</h:panelGrid>
				<af:panelBox text="FEB Selection" background="medium">
					<af:panelForm>
						<af:inputText id="ccuAddress" label="CCU Address:"
							value="#{FebBean.ccuAddress}" required="true" />
						<af:message for="ccuAddress" />
						<af:inputText id="channel" label="I2C Channel:"
							value="#{FebBean.channel}" required="true">
							<f:validateLongRange minimum="1" maximum="8" />
						</af:inputText>
						<af:message for="channel" />
						<af:inputText id="addr" label="I2C Address:"
							value="#{FebBean.addr}" required="true" />
						<af:selectBooleanCheckbox id="endcap" text="Endcap"
							value="#{FebBean.endcap}" />
						<af:message for="addr" />
					</af:panelForm>
				</af:panelBox>
				<af:panelButtonBar>
					<af:commandButton actionListener="#{FebBean.configure}"
						text="Configure XDAQ" />
					<af:commandButton actionListener="#{FebBean.enableDAC}"
						text="Enable DAC" />
					<af:commandButton actionListener="#{FebBean.disableDAC}"
						text="Disable DAC" />
					<af:commandButton actionListener="#{FebBean.testDb}" text="Test" />
					<af:outputText value="#{FebBean.testMessage}" />
				</af:panelButtonBar>

				<af:panelBox text="FEB Values" background="medium">
					<h:panelGrid columns="5">
						<af:outputText value="" />
						<af:outputText value="Read Value" />
						<af:commandButton actionListener="#{FebBean.readAll}"
							text="Read All" />
						<af:outputText value="Write Value" />
						<af:commandButton actionListener="#{FebBean.writeAll}"
							text="Write All" />

						<af:outputText value="Temperature:" />
						<af:inputText value="#{FebBean.readTemperatureValue}" />
						<af:commandButton actionListener="#{FebBean.readTemperature}"
							text="Read" />
						<af:outputText value="" />
						<af:outputText value="" />
						
						<af:outputText value="Temperature2:" />
						<af:inputText value="#{FebBean.readTemperature2Value}" />
						<af:commandButton actionListener="#{FebBean.readTemperature2}"
							text="Read" />
						<af:outputText value="" />
						<af:outputText value="" />

						<af:outputText value="VTH1:" />
						<af:inputText value="#{FebBean.readVTH1Value}" />
						<af:commandButton actionListener="#{FebBean.readVTH1}" text="Read" />
						<af:inputText value="#{FebBean.writeVTH1Value}" />
						<af:commandButton actionListener="#{FebBean.writeVTH1}"
							text="Write" />

						<af:outputText value="VTH2:" />
						<af:inputText value="#{FebBean.readVTH2Value}" />
						<af:commandButton actionListener="#{FebBean.readVTH2}" text="Read" />
						<af:inputText value="#{FebBean.writeVTH2Value}" />
						<af:commandButton actionListener="#{FebBean.writeVTH2}"
							text="Write" />

						<af:outputText value="VTH3:" />
						<af:inputText value="#{FebBean.readVTH3Value}" />
						<af:commandButton actionListener="#{FebBean.readVTH3}" text="Read" />
						<af:inputText value="#{FebBean.writeVTH3Value}" />
						<af:commandButton actionListener="#{FebBean.writeVTH3}"
							text="Write" />							

						<af:outputText value="VTH4:" />
						<af:inputText value="#{FebBean.readVTH4Value}" />
						<af:commandButton actionListener="#{FebBean.readVTH4}" text="Read" />
						<af:inputText value="#{FebBean.writeVTH4Value}" />
						<af:commandButton actionListener="#{FebBean.writeVTH4}"
							text="Write" />

						<af:outputText value="VMon1:" />
						<af:inputText value="#{FebBean.readVMon1Value}" />
						<af:commandButton actionListener="#{FebBean.readVMon1}"
							text="Read" />
						<af:inputText value="#{FebBean.writeVMon1Value}" />
						<af:commandButton actionListener="#{FebBean.writeVMon1}"
							text="Write" />

						<af:outputText value="VMon2:" />
						<af:inputText value="#{FebBean.readVMon2Value}" />
						<af:commandButton actionListener="#{FebBean.readVMon2}"
							text="Read" />
						<af:inputText value="#{FebBean.writeVMon2Value}" />
						<af:commandButton actionListener="#{FebBean.writeVMon2}"
							text="Write" />

						<af:outputText value="VMon3:" />
						<af:inputText value="#{FebBean.readVMon3Value}" />
						<af:commandButton actionListener="#{FebBean.readVMon3}"
							text="Read" />
						<af:inputText value="#{FebBean.writeVMon3Value}" />
						<af:commandButton actionListener="#{FebBean.writeVMon3}"
							text="Write" />

						<af:outputText value="VMon4:" />
						<af:inputText value="#{FebBean.readVMon4Value}" />
						<af:commandButton actionListener="#{FebBean.readVMon4}"
							text="Read" />
						<af:inputText value="#{FebBean.writeVMon4Value}" />
						<af:commandButton actionListener="#{FebBean.writeVMon4}"
							text="Write" />
					</h:panelGrid>
				</af:panelBox>

			</af:form>
		</h:panelGroup>

	</afh:body>
	</afh:html>

</f:view>
