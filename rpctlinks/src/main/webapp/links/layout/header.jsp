<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="rpct-jsf-components" prefix="rpct"%>


<f:subview id="headerView">
	<%--<f:verbatim>
		<!-- to correct the unsightly Flash of Unstyled Content. http://www.bluerobot.com/web/css/fouc.asp -->
		<script type="text/javascript"></script>
		
		<style type="text/css" title="baseStyle" media="screen">
			@import "<%=request.getContextPath()%>/script/content.css";
		</style>
	</f:verbatim>--%>
	<h:form id="menuForm"> 
		<%--<rpct:navigatorList id="navList" immediate="false" 
			headerClass="navHeader"	containerClass="navContainer" 
			currentClass="navCurrent" activeClass="navActive" listClass="navList">
			<rpct:navigatorItem name="functionalDb" label="Functional DB" action="#{MenuBean.functionaldb}" />
			<rpct:navigatorItem name="hardwareDb" label="Hardware DB" action="#{MenuBean.hardwaredb}" />
			<rpct:navigatorItem name="boot" label="Boot" action="#{MenuBean.boot}" />
			<rpct:navigatorItem name="logout" label="Logout" action="#{MenuBean.logout}" />			
		</rpct:navigatorList> --%>
		<rpct:navigatorList immediate="false" 
				binding="#{MenuBean.mainNavList}"
				headerClass="navHeader"	containerClass="navContainer" 
				currentClass="navCurrent" activeClass="navActive" listClass="navList">
		</rpct:navigatorList>
	</h:form>

</f:subview>
