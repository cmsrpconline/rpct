<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="rpct-jsf-components" prefix="rpct"%>
<f:view>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" >
	<head>
		<title>Hardware - Link Boards</title>
		<jsp:include page="/links/layout/head.jsp" />
	</head>
	<body>
	<div id="header"><jsp:include page="/links/layout/header.jsp" /></div>
	<div id="leftmenu">
		<h:form id="leftMenuForm">		
			<rpct:navigatorList immediate="false" 
				binding="#{MenuBean.hardwareDbNavList}"
				headerClass="leftNavHeader"	containerClass="leftNavContainer" 
				currentClass="leftNavCurrent" activeClass="leftNavActive" listClass="leftNavList">
			</rpct:navigatorList>
		</h:form> 
	</div>

	<div id="body">
	<h3><h:outputText value="Link Boards" /></h3>	
	<h:form>
		<div class="prettyTable"><h:dataTable rowClasses="odd,even"
			cellpadding="3" value="#{LinkBoardHardwareBean.linkBoards}" var="linkBoard"
			binding="#{LinkBoardHardwareBean.linkBoardTable}">

			<h:column>
				<f:facet name="header">
					<h:outputText value="Name" />
				</f:facet>
				<h:outputText value="#{linkBoard.name}" />
			</h:column>

			<h:column>
				<f:facet name="header">
					<h:outputText value="Notes" />
				</f:facet>
				<h:outputText value="#{linkBoard.notes}" />
			</h:column>

			<h:column>
				<f:facet name="header">
					<h:outputText value="Production Date"/>
				</f:facet>
				<h:outputText value="#{linkBoard.productionDate}">
					<f:convertDateTime type="date"/>
				</h:outputText>
			</h:column>

			<h:column>
				<h:commandButton actionListener="#{LinkBoardHardwareBean.deleteLinkBoard}" value="Delete" 
				onclick="return window.confirm('Are you sure you want to delete #{linkBoard.name}?')"/>
			</h:column>
		</h:dataTable></div>
	</h:form> 
	
	<h3><h:outputText value="Add new Link Board" /></h3>
	<h:form>
		<h:panelGrid columns="3">
			<h:outputLabel value="Name:" for="newLinkBoardName" />
			<h:inputText id="newLinkBoardName" value="#{LinkBoardHardwareBean.newLinkBoardName}"
				required="true" />
			<h:message for="newLinkBoardName" />

			<h:outputLabel value="Production Date:" for="newLinkBoardProductionDate" />
			<h:inputText id="newLinkBoardProductionDate"
				value="#{LinkBoardHardwareBean.newLinkBoardProductionDate}" required="true">
				<f:convertDateTime type="date"/>
			</h:inputText>
			<h:message for="newLinkBoardProductionDate" />

			<h:outputLabel value="Notes:" for="newLinkBoardNotes" />
			<h:inputText id="newLinkBoardNotes" value="#{LinkBoardHardwareBean.newLinkBoardNotes}"/>
			<h:panelGroup/>

			<h:commandButton id="submitAdd" actionListener="#{LinkBoardHardwareBean.addNewLinkBoard}"
				value="Add" />
		</h:panelGrid>
	</h:form></div>
	
	</body>
	</html>
</f:view>
