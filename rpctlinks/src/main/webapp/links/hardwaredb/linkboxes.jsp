<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="rpct-jsf-components" prefix="rpct"%>
<f:view>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" >
	<head>
		<title>Hardware - Link Boxes</title>
		<jsp:include page="/links/layout/head.jsp" />
	</head>
	<body>
	<div id="header"><jsp:include page="/links/layout/header.jsp" /></div>
	<div id="leftmenu">
		<h:form id="leftMenuForm">		
			<rpct:navigatorList immediate="false" 
				binding="#{MenuBean.hardwareDbNavList}"
				headerClass="leftNavHeader"	containerClass="leftNavContainer" 
				currentClass="leftNavCurrent" activeClass="leftNavActive" listClass="leftNavList">
			</rpct:navigatorList>
		</h:form> 
	</div>

	<div id="body">
	<h3><h:outputText value="Link Boxes" /></h3>	
	<h:form>
		<div class="prettyTable"><h:dataTable rowClasses="odd,even"
			cellpadding="3" value="#{LinkBoxHardwareBean.linkBoxes}" var="linkBox"
			binding="#{LinkBoxHardwareBean.linkBoxTable}">

			<h:column>
				<f:facet name="header">
					<h:outputText value="Name" />
				</f:facet>
				<h:outputText value="#{linkBox.name}" />
			</h:column>

			<h:column>
				<f:facet name="header">
					<h:outputText value="Notes" />
				</f:facet>
				<h:outputText value="#{linkBox.notes}" />
			</h:column>

			<h:column>
				<f:facet name="header">
					<h:outputText value="Production Date"/>
				</f:facet>
				<h:outputText value="#{linkBox.productionDate}">
					<f:convertDateTime type="date"/>
				</h:outputText>
			</h:column>

			<h:column>
				<h:commandButton actionListener="#{LinkBoxHardwareBean.deleteLinkBox}" value="Delete" 
				onclick="return window.confirm('Are you sure you want to delete #{linkBox.name}?')"/>
			</h:column>
		</h:dataTable></div>
	</h:form> 
	
	<h3><h:outputText value="Add new Link Box" /></h3>
	<h:form>
		<h:panelGrid columns="3">
			<h:outputLabel value="Name:" for="newLinkBoxName" />
			<h:inputText id="newLinkBoxName" value="#{LinkBoxHardwareBean.newLinkBoxName}"
				required="true" />
			<h:message for="newLinkBoxName" />

			<h:outputLabel value="Production Date:" for="newLinkBoxProductionDate" />
			<h:inputText id="newLinkBoxProductionDate"
				value="#{LinkBoxHardwareBean.newLinkBoxProductionDate}" required="true">
				<f:convertDateTime type="date"/>
			</h:inputText>
			<h:message for="newLinkBoxProductionDate" />

			<h:outputLabel value="Notes:" for="newLinkBoxNotes" />
			<h:inputText id="newLinkBoxNotes" value="#{LinkBoxHardwareBean.newLinkBoxNotes}"/>
			<h:panelGroup/>

			<h:commandButton id="submitAdd" actionListener="#{LinkBoxHardwareBean.addNewLinkBox}"
				value="Add" />
		</h:panelGrid>
	</h:form></div>
	
	</body>
	</html>
</f:view>
