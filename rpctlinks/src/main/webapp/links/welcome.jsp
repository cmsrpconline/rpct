<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<f:view>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" >
	<head>
		<title>Welcome</title>
		<jsp:include page="/links/layout/head.jsp" />
	</head>
	<body>
	<div id="header"><jsp:include page="/links/layout/header.jsp" /></div>

	<div class="body">
	<h3><h:outputText value="Welcome" /></h3>		
	</body>
	</html>
</f:view>
