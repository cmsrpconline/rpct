<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="rpct-jsf-components" prefix="rpct"%>
<f:view>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
	<head>
		<title>Link Boards</title>
		<jsp:include page="/links/layout/head.jsp" />
	</head>
	<body>
	<div id="header"><jsp:include page="/links/layout/header.jsp" /></div>
	<div id="leftmenu">
		<h:form id="leftMenuForm">		
			<rpct:navigatorList immediate="false" 
				binding="#{MenuBean.functionalDbNavList}"
				headerClass="leftNavHeader"	containerClass="leftNavContainer" 
				currentClass="leftNavCurrent" activeClass="leftNavActive" listClass="leftNavList">
			</rpct:navigatorList>
		</h:form> 
	</div>
	<div id="body">
	<h3><h:outputText value="Link Boards" /></h3>
	<h:form> 
		<h4>Define Link Board Filter</h4>
		<p>
			Link Box:
			<h:selectOneMenu value="#{LinkBoardBean.searchByLinkBox}">
				<f:selectItems value="#{LinkBoardBean.linkBoxSelectItems}" />
			</h:selectOneMenu>&nbsp;
			Master:
			<h:selectOneMenu value="#{LinkBoardBean.searchByMaster}">
				<f:selectItem itemLabel="[Any]" itemValue="[Any]" />
				<f:selectItem itemLabel="true" itemValue="true" />
				<f:selectItem itemLabel="false" itemValue="false" />
			</h:selectOneMenu>&nbsp;
			Link Name:
			<h:inputText value="#{LinkBoardBean.searchByLinkName}" />&nbsp;
			Strip Inversion:
			<h:selectOneMenu value="#{LinkBoardBean.searchByStripInversion}">
				<f:selectItem itemLabel="[Any]" itemValue="[Any]" />
				<f:selectItem itemLabel="true" itemValue="true" />
				<f:selectItem itemLabel="false" itemValue="false" />
			</h:selectOneMenu>&nbsp;
			<h:commandButton actionListener="#{LinkBoardBean.search}" value="Go" />
		</p>
		<h4>Display Options</h4>
		<p>
			<div>
			<h:selectBooleanCheckbox id="showCablesSGN" value="#{LinkBoardBean.showCablesSGN}"/>
			<h:outputLabel for="showCablesSGN" value="Show Cables SGN"/>
			</div>
		</p>
			
		<div class="prettyTable"><h:dataTable rowClasses="odd,even"
			binding="#{LinkBoardBean.linkBoardTable}" cellpadding="3"
			value="#{LinkBoardBean.linkBoards}" var="linkBoard">
			<h:column>
				<f:facet name="header">
					<h:outputText value="Link Box" />
				</f:facet>
				<h:commandLink action="#{LinkBoardBean.showLinkBoxDetails}">								
					<h:outputText value="#{linkBoard.linkBox}" />
				</h:commandLink>
			</h:column>
			<h:column>
				<f:facet name="header">
					<h:outputText value="Link Box Slot" />
				</f:facet>
				<h:outputText value="#{linkBoard.linkBoxSlot}" />
			</h:column>
			<h:column>
				<f:facet name="header">
					<h:outputText value="Master" />
				</f:facet>
				<h:outputText value="#{linkBoard.master}" />
			</h:column>
			<h:column>
				<f:facet name="header">
					<h:outputText value="Link Name" />
				</f:facet>
				<h:outputText value="#{linkBoard.cableTRG.name}" />
			</h:column>
			<h:column>
				<f:facet name="header">
					<h:outputText value="Strip Inversion" />
				</f:facet>
				<h:outputText value="#{linkBoard.stripInversion}" />
			</h:column>
			<h:column rendered="#{LinkBoardBean.showCablesSGN}">
				<f:facet name="header">
					<h:outputText value="SGN Cable #1" />
				</f:facet>
				<h:outputText value="#{linkBoard.cablesSGN[0].name}" />
			</h:column>
			<h:column rendered="#{LinkBoardBean.showCablesSGN}">
				<f:facet name="header">
					<h:outputText value="SGN Cable #2" />
				</f:facet>
				<h:outputText value="#{linkBoard.cablesSGN[1].name}" />
			</h:column>
			<h:column rendered="#{LinkBoardBean.showCablesSGN}">
				<f:facet name="header">
					<h:outputText value="SGN Cable #3" />
				</f:facet>
				<h:outputText value="#{linkBoard.cablesSGN[2].name}" />
			</h:column>
			<h:column rendered="#{LinkBoardBean.showCablesSGN}">
				<f:facet name="header">
					<h:outputText value="SGN Cable #4" />
				</f:facet>
				<h:outputText value="#{linkBoard.cablesSGN[3].name}" />
			</h:column>
			<h:column rendered="#{LinkBoardBean.showCablesSGN}">
				<f:facet name="header">
					<h:outputText value="SGN Cable #5" />
				</f:facet>
				<h:outputText value="#{linkBoard.cablesSGN[4].name}" />
			</h:column>
			<h:column rendered="#{LinkBoardBean.showCablesSGN}">
				<f:facet name="header">
					<h:outputText value="SGN Cable #6" />
				</f:facet>
				<h:outputText value="#{linkBoard.cablesSGN[5].name}" />
			</h:column>
		</h:dataTable></div>
	</h:form>
	</div>
	
	</body>
	</html>
</f:view>
