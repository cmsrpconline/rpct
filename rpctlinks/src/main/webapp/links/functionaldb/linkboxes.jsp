<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="rpct-jsf-components" prefix="rpct"%>
<f:view>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
	<head>
		<title>Link Boxes</title>
		<jsp:include page="/links/layout/head.jsp" />
	</head>
	<body>
	<div id="header"><jsp:include page="/links/layout/header.jsp" /></div>
	<div id="leftmenu">
		<h:form id="leftMenuForm">		
			<rpct:navigatorList id="leftNavList" immediate="false" 
				binding="#{MenuBean.functionalDbNavList}"
				headerClass="leftNavHeader"	containerClass="leftNavContainer" 
				currentClass="leftNavCurrent" activeClass="leftNavActive" listClass="leftNavList">
			</rpct:navigatorList>
		</h:form>
	</div>

	<div id="body">
	<h3><h:outputText value="Link Boxes" /></h3>	
	<h:form>
		<div class="prettyTable"><h:dataTable rowClasses="odd,even"
			cellpadding="3" value="#{LinkBoxBean.linkBoxes}" var="linkBox"
			binding="#{LinkBoxBean.linkBoxTable}">
			
			<h:column>
				<f:facet name="header">
					<h:outputText value="Name" />
				</f:facet>
				<h:commandLink action="#{LinkBoxBean.showLinkBoxDetails}">								
					<h:outputText value="#{linkBox.name}" />
				</h:commandLink>
			</h:column>
			
			<h:column>
				<f:facet name="header">
					<h:outputText value="Wheel" />
				</f:facet>
				<h:outputText value="#{linkBox.wheel}" />
			</h:column>

			<h:column>
				<f:facet name="header">
					<h:outputText value="Sector" />
				</f:facet>
				<h:outputText value="#{linkBox.sector}" />
			</h:column>

			<h:column>
				<f:facet name="header">
					<h:outputText value="Location" />
				</f:facet>
				<h:outputText value="#{linkBox.location}" />
			</h:column>
		</h:dataTable></div>
	</h:form> 	
	</body>
	</html>
</f:view>
