<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="rpct-jsf-components" prefix="rpct"%>
<f:view>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
	<head>
		<title>Link Box Details</title>
		<jsp:include page="/links/layout/head.jsp" />
	</head>
	<body>
	<div id="header"><jsp:include page="/links/layout/header.jsp" /></div>
	<div id="leftmenu">
		<h:form id="leftMenuForm">		
			<rpct:navigatorList id="leftNavList" immediate="false" 
				binding="#{MenuBean.functionalDbNavList}"
				headerClass="leftNavHeader"	containerClass="leftNavContainer" 
				currentClass="leftNavCurrent" activeClass="leftNavActive" listClass="leftNavList">
			</rpct:navigatorList>
		</h:form>
	</div>
	<div id="body">
	<h3><h:outputText value="Link Box Details" /></h3>
	<h:form>
		<h4>Link Box Details:</h4>
		<p>Name: <h:outputText value="#{LinkBoxDetailsBean.selectedLinkBox.name}"/></p>
		<p>Location: <h:outputText value="#{LinkBoxDetailsBean.selectedLinkBox.location}"/></p>
		<p>Sector: <h:outputText value="#{LinkBoxDetailBean.selectedLinkBox.sector}"/></p>
		<p>Wheel: <h:outputText value="#{LinkBoxDetailsBean.selectedLinkBox.wheel}"/></p>
		<h4>Link Boards</h4>		
		<div class="prettyTable"><h:dataTable rowClasses="odd,even" binding="#{LinkBoxDetailsBean.linkBoardsTable}"
			cellpadding="3" value="#{LinkBoxDetailsBean.selectedLinkBox.linkBoards}" var="linkBoard">
			
			<h:column>
				<f:facet name="header">
					<h:outputText value="Link Box Slot" />
				</f:facet>
				<h:outputText value="#{LinkBoxDetailsBean.linkBoardsRowIndex + 1}" />&nbsp;
			</h:column>
			<h:column>
				<f:facet name="header">
					<h:outputText value="Master" />
				</f:facet>
				<h:outputText value="#{linkBoard.master}" />
			</h:column>
			<h:column>
				<f:facet name="header">
					<h:outputText value="Link Name" />
				</f:facet>
				<h:outputText value="#{linkBoard.cableTRG.name}" />
			</h:column>
			<h:column>
				<f:facet name="header">
					<h:outputText value="Strip Inversion" />
				</f:facet>
				<h:outputText value="#{linkBoard.stripInversion}" />
			</h:column>
			
			<h:column>
				<f:facet name="header">
					<h:outputText value="Feb Cables" />
				</f:facet>
				<h:dataTable rowClasses="odd,even" 	
					binding="#{LinkBoxDetailsBean.cablesSGNTable}"
					rendered="#{not empty linkBoard}"
					cellpadding="3" value="#{linkBoard.cablesSGN}" var="cableSGN">
					<h:column>
						<f:facet name="header">
							<h:outputText value="Pos" />
						</f:facet>
						<h:outputText value="#{LinkBoxDetailsBean.cablesSGNRowIndex + 1}" />&nbsp;
					</h:column>
					<h:column>
						<f:facet name="header">
							<h:outputText value="Name" />
						</f:facet>
						<h:outputText value="#{cableSGN.name}" />
					</h:column>
					<h:column>
						<f:facet name="header">
							<h:outputText value="Missing Channels" />
						</f:facet>
						<h:outputText value="#{cableSGN.missChanStr}" />
					</h:column>
				</h:dataTable>
			</h:column>
			
		</h:dataTable>
		</div>
	</h:form>
	</div>
	
	</body>
	</html>
</f:view>
