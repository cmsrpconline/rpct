package rpct.temp;
import java.util.Iterator;

import rpct.xdaq.axis.bag.Bag;


/**
 * Created on 2005-05-05
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class FebAccessTester {
    
    //private Random rand = new Random();
    private static float vth1;
    private static float vth2;
    private static float vmon1;
    private static float vmon2;

    
    public void enableDAC(int channel, int addr, boolean value) {  
    }

    public void enableFebAccess(int channel, boolean value) {
    }

    public float readTemperature(int channel, int addr) {   
        //return rand.nextFloat();

        return Thread.currentThread().getId();
    }

    public float readVTH1(int channel, int addr) {      
        return vth1;
    }
    
    public void writeVTH1(int channel, int addr, float vth1) {
        this.vth1 = vth1;
    }

    public float readVTH2(int channel, int addr) {
        return vth2;
    }
    
    public void writeVTH2(int channel, int addr, float vth2) {
        this.vth2 = vth2;
    }

    public float readVMon1(int channel, int addr) {
        return vmon1;
    }
    
    public void writeVMon1(int channel, int addr, float vmon1) {
        this.vmon1 = vmon1;
    }

    public float readVMon2(int channel, int addr) {
        return vmon2;
    }
    
    public void writeVMon2(int channel, int addr, float vmon2) {
        this.vmon2 = vmon2;
    }  
    
    /*public FebAccessImplAxis.MassiveReadResponse massiveRead(FebAccessImplAxis.MassiveReadRequest readRequest) {
        return new FebAccessImplAxis.MassiveReadResponse();
    }*/
    
    public int addArray(int[] numbers) {
        int result = 0;
        for (int i : numbers) {
            result += i;
        }
        return result;
    }
    
    public int testBag(Bag bag) {
        int result = 0;
        for (Iterator iter = bag.entrySet().iterator(); iter.hasNext();) {
            Integer element = (Integer) iter.next();
            result += element.intValue();
        }
        return result;        
    }
    
    public String dupa() {
        return "dupa";
    }
}
