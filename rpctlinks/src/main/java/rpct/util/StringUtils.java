package rpct.util;

/**
 * Created on 2005-03-09
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class StringUtils {
    public static boolean isEmpty(String string) {
        return (string == null) || (string.length() == 0);
    }
    
    public static boolean isTrimmedEmpty(String string) {
        return (string == null) || (string.trim().length() == 0);
    }
    
    public static String trimToNull(String string) {
        if (string == null) {
            return null;
        }
        
        String trimmed = string.trim();
        if (trimmed.length() == 0) {
            return null;
        }
        
        return trimmed;
    }
    
    public static String toUpperCase(String string) {
        return string == null ? null : string.toUpperCase();
    }
}
