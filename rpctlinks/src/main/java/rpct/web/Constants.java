package rpct.web;

/**
 * Created on 2005-03-02
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class Constants {
    
    // Request parameter and attribute names
    public final static String HIBERNATE_CONTEXT = "hibernateContext";
    
    // Action outcomes
    public final static String SUCCESS_READONLY_OUTCOME = "success_readonly";
    public final static String SUCCESS_READWRITE_OUTCOME = "success_readwrite";
    public final static String SUCCESS_OUTCOME = "success";

    public final static String CANCEL_READONLY_OUTCOME = "cancel_readonly";
    public final static String CANCEL_READWRITE_OUTCOME = "cancel_readwrite";
    public final static String CANCEL_OUTCOME = "cancel";

    public final static String FAILURE_OUTCOME = "failure";
    public final static String ERROR_OUTCOME = "error";
    
    // action outcomes targeting some page
    public final static String LINK_BOX_DETAILS_OUTCOME = "linkBoxDetails";
   
}
