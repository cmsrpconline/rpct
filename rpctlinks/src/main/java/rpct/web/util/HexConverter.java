package rpct.web.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import rpct.xdaq.axis.Binary;

/**
 * Created on 2005-08-08
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class HexConverter implements Converter {
    
    private boolean acceptNulls = true;
    public final static String CONVERTER_ID = "HEX_CONVERTER";
    
    public HexConverter() {
    }
    
    public boolean isAcceptNulls() {
        return acceptNulls;
    }

    public void setAcceptNulls(boolean acceptNulls) {
        this.acceptNulls = acceptNulls;
    }

    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (acceptNulls && value.length() == 0) {
            return null;
        }
        try {
            return new Binary(value);
        } catch (Exception e) {
            throw new ConverterException(e.getMessage(), e);
        }
    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (acceptNulls && value == null) {
            return "";
        }
        Binary bi;
        if (value instanceof Binary) {
            bi = (Binary) value;
        }
        else if (value instanceof Integer || value instanceof Long
        		|| value instanceof Short || value instanceof Byte) {
            bi = new Binary(((Number)value).longValue());        
        }
        else {
            bi = new Binary(value.toString());
        }
        //return new String(Hex.encodeHex(bi.toByteArray()));
        return bi.toString();
    }        
}