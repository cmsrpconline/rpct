/*
 * Created on Feb 11, 2005
 *
 */
package rpct.web.bean;

import java.util.HashMap;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.jia.components.UINavigator;
import org.jia.components.model.NavigatorItem;
import org.jia.components.model.NavigatorItemList;

/**
 * @author tb
 *
 */
public class MenuBean {

    private UINavigator mainNavList;
    private UINavigator functionalDbNavList;
    private UINavigator hardwareDbNavList;
    private UINavigator bootNavList;
    private UINavigator rcmsNavList;
    private Map<String, NavigatorItemList> itemListMap = new HashMap<String, NavigatorItemList>();
    
    class MenuItem {
        private String parentName;
        private String name;
        private String title;
        private String link;
        public MenuItem(String parentName, String name, String title, String link) {
            this.parentName = parentName;
            this.name = name;
            this.title = title;
            this.link = link;
        }    
    }
    
    private MenuItem[] menuItems = {
            new MenuItem("",            "fuctionalDb",              "Equipment",    null),
            new MenuItem("fuctionalDb", "functionalDb.LinkBoxes",   "Link Boxes",       "/links/functionaldb/linkboxes.faces"),
            new MenuItem("fuctionalDb", "functionalDb.LinkBoards",  "Link Boards",      "/links/functionaldb/linkboards.faces"),

            new MenuItem("",            "hardwareDb",               "Hardware",      null),
            new MenuItem("hardwareDb",  "hardwareDb.LinkBoxes",     "Link Boxes",       "/links/hardwaredb/linkboxes.faces"),
            new MenuItem("hardwareDb",  "hardwareDb.LinkBoards",    "Link Boards",       "/links/hardwaredb/linkboards.faces"),
            
            new MenuItem("",            "boot",                     "Boot",             null),
            new MenuItem("boot",        "boot.bootFiles",           "Boot Files",       "/links/boot/boot.faces"),
            
            new MenuItem("",            "rcms",                     "RCMS",             null),
            new MenuItem("rcms",        "rcms.feb",                 "FEB Access",       "/links/rcms/feb.faces"),
            new MenuItem("rcms",        "rcms.iiAccess",            "II Access",        "/links/rcms/iiaccess.faces"),
    };
    
	public MenuBean() {
        String contextPath = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
        
        for (int i = 0; i < menuItems.length; i++) {
            MenuItem item = menuItems[i];
            if (item.parentName.length() > 0) {
                NavigatorItemList itemList = itemListMap.get(item.parentName);
                if (itemList == null) {
                    itemList = new NavigatorItemList();       
                    itemListMap.put(item.parentName, itemList);
                }
                itemList.add(new NavigatorItem(
                        item.name, item.title, null, null, contextPath + item.link, true, false));
            }
        }

        for (int i = 0; i < menuItems.length; i++) {
            MenuItem item = menuItems[i];
            if (item.parentName.length() == 0) {
                NavigatorItemList itemList = itemListMap.get(item.parentName);
                if (itemList == null) {
                    itemList = new NavigatorItemList();       
                    itemListMap.put(item.parentName, itemList);
                }
                NavigatorItem firstSubItem = (NavigatorItem) itemListMap.get(item.name).get(0);
                itemList.add(new NavigatorItem(
                        item.name, item.title, null, null, firstSubItem.getLink(), true, false));
            }
        }        
	}


    public UINavigator getMainNavList() {
        return mainNavList;
    }
    

    public void setMainNavList(UINavigator mainNavList) {
        this.mainNavList = mainNavList;
        if (this.mainNavList.getItems() == null) {
            this.mainNavList.setItems(itemListMap.get(""));
        }
    }
    
    public UINavigator getFunctionalDbNavList() {
        return functionalDbNavList;
    }

    public void setFunctionalDbNavList(UINavigator functionalDbNavList) {
        this.functionalDbNavList = functionalDbNavList;
        if (this.functionalDbNavList.getItems() == null) {
            this.functionalDbNavList.setItems(itemListMap.get("fuctionalDb"));
        }
    }
    

    public UINavigator getHardwareDbNavList() {
        return hardwareDbNavList;
    }
    


    public void setHardwareDbNavList(UINavigator hardwareDbNavList) {
        this.hardwareDbNavList = hardwareDbNavList;
        if (this.hardwareDbNavList.getItems() == null) {
            this.hardwareDbNavList.setItems(itemListMap.get("hardwareDb"));
        }
    }
    


    public UINavigator getBootNavList() {
        return bootNavList;
    }

    public void setBootNavList(UINavigator bootNavList) {
        this.bootNavList = bootNavList;
        if (this.bootNavList.getItems() == null) {
            this.bootNavList.setItems(itemListMap.get("boot"));
        }
    }


    public UINavigator getRcmsNavList() {
        return rcmsNavList;
    }

    public void setRcmsNavList(UINavigator rcmsNavList) {
        this.rcmsNavList = rcmsNavList;
        if (this.rcmsNavList.getItems() == null) {
            this.rcmsNavList.setItems(itemListMap.get("rcms"));
        }
    }
    
}
