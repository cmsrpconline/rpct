package rpct.web.bean;

import java.util.Date;

import javax.faces.component.UIData;
import javax.faces.event.ActionEvent;

/**
 * Created on 2005-03-30
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class LinkBoxHardwareBean {
    
    private UIData linkBoxTable;
    
    private String newLinkBoxName;
    private Date newLinkBoxProductionDate;
    private String newLinkBoxNotes;
    
    //private HardwareItemDAO daoHardwareItem = new HardwareItemDAOHibernate();

    public UIData getLinkBoxTable() {
        return linkBoxTable;
    }

    public void setLinkBoxTable(UIData linkBoxTable) {
        this.linkBoxTable = linkBoxTable;
    }

    /*public List getLinkBoxes() {  
        try {
            return daoHardwareItem.getAllLinkBoxes(); 
        } catch (DataAccessException e) {
            throw new FacesException(e);
        }
    }*/


    public String getNewLinkBoxName() {
        return newLinkBoxName;
    }
    


    public void setNewLinkBoxName(String newLinkBoxName) {
        this.newLinkBoxName = newLinkBoxName;
    }
    


    public String getNewLinkBoxNotes() {
        return newLinkBoxNotes;
    }
    


    public void setNewLinkBoxNotes(String newLinkBoxNotes) {
        this.newLinkBoxNotes = newLinkBoxNotes;
    }
    


    public Date getNewLinkBoxProductionDate() {
        return newLinkBoxProductionDate;
    }
    


    public void setNewLinkBoxProductionDate(Date newLinkBoxProductionDate) {
        this.newLinkBoxProductionDate = newLinkBoxProductionDate;
    }

    public void addNewLinkBox(ActionEvent event) {
        /*LinkBoxHardware linkBox = new LinkBoxHardware();
        
        linkBox.setName(newLinkBoxName);
        linkBox.setProductionDate(newLinkBoxProductionDate);
        linkBox.setNotes(newLinkBoxNotes);
        try {
            daoHardwareItem.saveObject(linkBox);
        }
        catch (DataAccessException e) {
            throw new FacesException(e);
        }    */
    }     

    public void deleteLinkBox(ActionEvent event) {
        /*LinkBoxHardware linkBox = (LinkBoxHardware) linkBoxTable.getRowData();
        try {
            daoHardwareItem.deleteObject(linkBox);
        }
        catch (DataAccessException e) {
            throw new FacesException(e);
        }*/
    }
    
}
