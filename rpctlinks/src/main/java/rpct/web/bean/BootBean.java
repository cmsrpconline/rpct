package rpct.web.bean;

import javax.faces.component.UIData;

/**
 * Created on 2005-03-01
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class BootBean {
    
    private UIData bootFileTable;
    
    String newFileName;
    String newFileDescription;
    String newFileType;
    
    //BootFileDAO daoBootFile = new BootFileDAOHibernate();
    
    
    public BootBean() {  
    }

    public UIData getBootFileTable() {
        return bootFileTable;
    }
    
    public void setBootFileTable(UIData bootFileTable) {
        this.bootFileTable = bootFileTable;
    }

    /*public List getBootFiles() {  
        try {
            return daoBootFile.getAll(); 
        } catch (DataAccessException e) {
            throw new FacesException(e);
        }
    }*/
    
    public String getNewFileName() {
        return newFileName;
    }
    
    public void setNewFileName(String newFileName) {
        this.newFileName = newFileName;
    }    

    public String getNewFileDescription() {
        return newFileDescription;
    }    

    public void setNewFileDescription(String newFileDescription) {
        this.newFileDescription = newFileDescription;
    }
    

    public String getNewFileType() {
        return newFileType;
    }

    public void setNewFileType(String newFileType) {
        this.newFileType = newFileType;
    }
    
    /*public String addNewFile() {
        BootFile bootFile = new BootFile();
        bootFile.setFileName(newFileName);
        bootFile.setDescription(newFileDescription);
        bootFile.setBootFileType(newFileType);
        try {
            daoBootFile.saveObject(bootFile);
            return Constants.SUCCESS_OUTCOME;
        }
        catch (DataAccessException e) {
            throw new FacesException(e);
        }   
    }*/
    
    /*public String deleteBootFile() {
        BootFile bootFile = (BootFile) bootFileTable.getRowData();
        try {
            daoBootFile.deleteObject(bootFile);
            return Constants.SUCCESS_OUTCOME;
        }
        catch (DataAccessException e) {
            throw new FacesException(e);
        }
    }*/
}
