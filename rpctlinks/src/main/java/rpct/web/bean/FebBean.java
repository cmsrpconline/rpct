package rpct.web.bean;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

import javax.faces.FacesException;
import javax.faces.event.ActionEvent;
import javax.xml.rpc.ServiceException;

import rpct.db.domain.equipment.chamberloaction.BarrelOrEndcap;
import rpct.db.service.DbService;
import rpct.db.service.FebAccessInfo;
import rpct.db.service.axis.DbServiceClientImplAxis;
import rpct.rcms.lbox.FebAccess;
import rpct.rcms.lbox.axis.FebAccessImplAxis;
import rpct.rcms.lbox.axis.FebInfoImplAxis;

/**
 * Created on 2005-05-06
 *
 * @author Michal Pietrusinski
 * @version $Id: FebBean.java,v 1.16 2007/06/19 16:39:30 tb Exp $
 */
public class FebBean {
    private FebAccess febAccess;

    private URL xdaqUrl;
    private int xdaqAppInstance = 0;
    private int ccuAddress = 0x50;
    private int channel = 1;
    private int addr = 0;
    private boolean endcap = false;
    private String testMessage = "This is a test message";

    static class FebData {
        float readTemperatureValue;
        float readTemperature2Value;
        float readVTH1Value;
        float writeVTH1Value = 210;
        float readVTH2Value;
        float writeVTH2Value = 210;   
        float readVTH3Value;
        float writeVTH3Value = 210;   
        float readVTH4Value;
        float writeVTH4Value = 210;   
        float readVMon1Value;
        float writeVMon1Value = 3500;
        float readVMon2Value;
        float writeVMon2Value = 3500;  
        float readVMon3Value;
        float writeVMon3Value = 3500;   
        float readVMon4Value;
        float writeVMon4Value = 3500;       
    }

    private Integer makeKey(int address, int channel, int ccuAddress) {
        return ccuAddress * 10000 + address * 100 + channel;
    }

    private Map<Integer, FebData> febDataMap = new HashMap<Integer, FebData>();

    private FebData getFebData() {
        Integer key = makeKey(addr, channel, ccuAddress);
        FebData febData = febDataMap.get(key);
        if (febData == null) {
            febData = new FebData();
            febDataMap.put(key, febData);
        }
        return febData;
    }


    public FebBean() throws MalformedURLException  {
        xdaqUrl = new URL("http://localhost:1972");
        //febAccess = new FebAccessImplAxis(new URL("http://mpport:1972"), "XdaqLBoxAccess", 0);
        //febAccess.configure();
    }

    public boolean isReady() {
        return febAccess != null;
    }

    public String getCcuAddress() {
        return Integer.toHexString(ccuAddress);
    }

    public void setCcuAddress(String ccuAddress) {
        this.ccuAddress = Integer.parseInt(ccuAddress, 16);
    }

    public int getAddr() {
        return addr;
    }


    public void setAddr(int addr) {
        this.addr = addr;
    }


    public int getChannel() {
        return channel;
    }


    public void setChannel(int channel) {
        this.channel = channel;
    }


    public boolean isEndcap() {
        return endcap;
    }


    public void setEndcap(boolean endcap) {
        this.endcap = endcap;
    }


    public void enableDAC(ActionEvent event) {        
        try {
            febAccess.enableDAC(new FebInfoImplAxis(ccuAddress, channel, addr, endcap), true);
        } catch (RemoteException e) {
            throw new FacesException(e);
        } catch (ServiceException e) {
            throw new FacesException(e);
        } 
    }

    public void disableDAC(ActionEvent event) {        
        try {
            febAccess.enableDAC(new FebInfoImplAxis(ccuAddress, channel, addr, endcap), false);
        } catch (RemoteException e) {
            throw new FacesException(e);
        } catch (ServiceException e) {
            throw new FacesException(e);
        } 
    }


    public void readAll(ActionEvent event) {        
        try {
            FebAccess.FebProperties[] properties;
            if (endcap) {
                properties = new  FebAccess.FebProperties[] { 
                        FebAccess.FebProperties.TEMPERATURE,
                        FebAccess.FebProperties.TEMPERATURE2,           
                        FebAccess.FebProperties.VTH1,
                        FebAccess.FebProperties.VTH2,
                        FebAccess.FebProperties.VTH3,
                        FebAccess.FebProperties.VTH4,
                        FebAccess.FebProperties.VMON1,
                        FebAccess.FebProperties.VMON2,
                        FebAccess.FebProperties.VMON3,
                        FebAccess.FebProperties.VMON4 };  
            }
            else {  
                properties = new  FebAccess.FebProperties[] {
                        FebAccess.FebProperties.TEMPERATURE,
                        FebAccess.FebProperties.VTH1,
                        FebAccess.FebProperties.VTH2,
                        FebAccess.FebProperties.VMON1,
                        FebAccess.FebProperties.VMON2 };            
            }


            FebAccess.FebInfo[] febs = new FebAccess.FebInfo[] {
                    febAccess.createFebInfo(ccuAddress, channel, addr, endcap)};

            FebAccess.MassiveReadRequest readRequest = febAccess.createMassiveReadRequest(
                    properties, febs);            
            FebAccess.MassiveReadResponse response = febAccess.massiveRead(readRequest);   
            FebAccess.FebValues[] febValues = response.getFebValues();
            if (febValues.length != 1) {
                throw new FacesException("Unexpected febValues length: " + febValues.length);
            }

            if (endcap) {
                getFebData().readTemperatureValue = febValues[0].getValues()[0];
                getFebData().readTemperature2Value = febValues[0].getValues()[1];
                getFebData().readVTH1Value = febValues[0].getValues()[2];
                getFebData().readVTH2Value = febValues[0].getValues()[3];
                getFebData().readVTH3Value = febValues[0].getValues()[4];
                getFebData().readVTH4Value = febValues[0].getValues()[5];
                getFebData().readVMon1Value = febValues[0].getValues()[6];
                getFebData().readVMon2Value = febValues[0].getValues()[7];
                getFebData().readVMon3Value = febValues[0].getValues()[8];
                getFebData().readVMon4Value = febValues[0].getValues()[9];
            }
            else {
                getFebData().readTemperatureValue = febValues[0].getValues()[0];
                getFebData().readTemperature2Value = 0;
                getFebData().readVTH1Value = febValues[0].getValues()[1];
                getFebData().readVTH2Value = febValues[0].getValues()[2];
                getFebData().readVTH3Value = 0;
                getFebData().readVTH4Value = 0;
                getFebData().readVMon1Value = febValues[0].getValues()[3];
                getFebData().readVMon2Value = febValues[0].getValues()[4];
                getFebData().readVMon3Value = 0;
                getFebData().readVMon4Value = 0;
            }
        } catch (RemoteException e) {
            throw new FacesException(e);
        } catch (ServiceException e) {
            throw new FacesException(e);
        } 
    }

    public void writeAll(ActionEvent event) {        
        try {
            FebAccess.FebProperties[] properties;
            FebAccess.FebValues febValues;
            if (endcap) {
                properties = new  FebAccess.FebProperties[] {            
                        FebAccess.FebProperties.VTH1,
                        FebAccess.FebProperties.VTH2,
                        FebAccess.FebProperties.VTH3,
                        FebAccess.FebProperties.VTH4,
                        FebAccess.FebProperties.VMON1,
                        FebAccess.FebProperties.VMON2,
                        FebAccess.FebProperties.VMON3,
                        FebAccess.FebProperties.VMON4 };
                febValues = febAccess.createFebValues(ccuAddress, channel, addr, endcap, 
                        new float[] { getFebData().writeVTH1Value, 
                        getFebData().writeVTH2Value,
                        getFebData().writeVTH3Value,
                        getFebData().writeVTH4Value,
                        getFebData().writeVMon1Value,
                        getFebData().writeVMon2Value,
                        getFebData().writeVMon3Value,
                        getFebData().writeVMon4Value});
            }
            else {
                properties = new  FebAccess.FebProperties[] {            
                        FebAccess.FebProperties.VTH1,
                        FebAccess.FebProperties.VTH2,
                        FebAccess.FebProperties.VMON1,
                        FebAccess.FebProperties.VMON2 };
                febValues = febAccess.createFebValues(ccuAddress, channel, addr, endcap,
                        new float[] { getFebData().writeVTH1Value, getFebData().writeVTH2Value,
                        getFebData().writeVMon1Value,
                        getFebData().writeVMon2Value});
            }



            FebAccess.MassiveWriteRequest writeRequest = febAccess.createMassiveWriteRequest(
                    properties, new FebAccess.FebValues[] { febValues });            
            febAccess.massiveWrite(writeRequest); 
        } catch (RemoteException e) {
            throw new FacesException(e);
        } catch (ServiceException e) {
            throw new FacesException(e);
        } 
    }

    public float getReadTemperatureValue() {
        return getFebData().readTemperatureValue;
    }

    public void setReadTemperatureValue(float readTemperatureValue) {
        getFebData().readTemperatureValue = readTemperatureValue;
    }

    public float getReadTemperature2Value() {
        return getFebData().readTemperature2Value;
    }

    public void setReadTemperature2Value(float readTemperature2Value) {
        getFebData().readTemperature2Value = readTemperature2Value;
    }

    public void readTemperature(ActionEvent event) {        
        try {
            getFebData().readTemperatureValue = febAccess.readTemperature(
                    new FebInfoImplAxis(ccuAddress, channel, addr, endcap));
        } catch (RemoteException e) {
            throw new FacesException(e);
        } catch (ServiceException e) {
            throw new FacesException(e);
        } 
    }


    public float getReadVTH1Value() {
        return getFebData().readVTH1Value;
    }

    public void setReadVTH1Value(float readVTH1Value) {
        getFebData().readVTH1Value = readVTH1Value;
    }

    public float getWriteVTH1Value() {
        return getFebData().writeVTH1Value;
    }

    public void setWriteVTH1Value(float writeVTH1Value) {
        getFebData().writeVTH1Value = writeVTH1Value;
    }

    public void writeVTH1(ActionEvent event) {        
        try {
            febAccess.writeVTH1(
                    new FebInfoImplAxis(ccuAddress, channel, addr, endcap), getFebData().writeVTH1Value);
        } catch (RemoteException e) {
            throw new FacesException(e);
        } catch (ServiceException e) {
            throw new FacesException(e);
        } 
    }

    public void readVTH1(ActionEvent event) {        
        try {
            getFebData().readVTH1Value = febAccess.readVTH1(
                    new FebInfoImplAxis(ccuAddress, channel, addr, endcap));
        } catch (RemoteException e) {
            throw new FacesException(e);
        } catch (ServiceException e) {
            throw new FacesException(e);
        } 
    }


    public float getReadVTH2Value() {
        return getFebData().readVTH2Value;
    }

    public void setReadVTH2Value(float readVTH2Value) {
        getFebData().readVTH2Value = readVTH2Value;
    }

    public float getWriteVTH2Value() {
        return getFebData().writeVTH2Value;
    }

    public void setWriteVTH2Value(float writeVTH2Value) {
        getFebData().writeVTH2Value = writeVTH2Value;
    }

    public void writeVTH2(ActionEvent event) {        
        try {
            febAccess.writeVTH2(
                    new FebInfoImplAxis(ccuAddress, channel, addr, endcap), getFebData().writeVTH2Value);
        } catch (RemoteException e) {
            throw new FacesException(e);
        } catch (ServiceException e) {
            throw new FacesException(e);
        } 
    }

    public void readVTH2(ActionEvent event) {        
        try {
            getFebData().readVTH2Value = febAccess.readVTH2(
                    new FebInfoImplAxis(ccuAddress, channel, addr, endcap));
        } catch (RemoteException e) {
            throw new FacesException(e);
        } catch (ServiceException e) {
            throw new FacesException(e);
        } 
    }

    public float getReadVTH3Value() {
        return getFebData().readVTH3Value;
    }

    public void setReadVTH3Value(float readVTH3Value) {
        getFebData().readVTH3Value = readVTH3Value;
    }

    public float getWriteVTH3Value() {
        return getFebData().writeVTH3Value;
    }

    public void setWriteVTH3Value(float writeVTH3Value) {
        getFebData().writeVTH3Value = writeVTH3Value;
    }

    public void writeVTH3(ActionEvent event) {     
        throw new UnsupportedOperationException();
        /*try {
            febAccess.writeVTH3(
                    new FebInfoImplAxis(ccuAddress, channel, addr, endcap), getFebData().writeVTH2Value);
        } catch (RemoteException e) {
            throw new FacesException(e);
        } catch (ServiceException e) {
            throw new FacesException(e);
        } */
    }

    public void readVTH3(ActionEvent event) {    
        throw new UnsupportedOperationException();      
        /*try {
            getFebData().readVTH3Value = febAccess.readVTH3(
                    new FebInfoImplAxis(ccuAddress, channel, addr, endcap));
        } catch (RemoteException e) {
            throw new FacesException(e);
        } catch (ServiceException e) {
            throw new FacesException(e);
        } */
    }
    
    ///
    public float getReadVTH4Value() {
        return getFebData().readVTH4Value;
    }

    public void setReadVTH4Value(float readVTH4Value) {
        getFebData().readVTH4Value = readVTH4Value;
    }

    public float getWriteVTH4Value() {
        return getFebData().writeVTH4Value;
    }

    public void setWriteVTH4Value(float writeVTH4Value) {
        getFebData().writeVTH4Value = writeVTH4Value;
    }

    public void writeVTH4(ActionEvent event) {     
        throw new UnsupportedOperationException();
        /*try {
            febAccess.writeVTH4(
                    new FebInfoImplAxis(ccuAddress, channel, addr, endcap), getFebData().writeVTH2Value);
        } catch (RemoteException e) {
            throw new FacesException(e);
        } catch (ServiceException e) {
            throw new FacesException(e);
        } */
    }

    public void readVTH4(ActionEvent event) {    
        throw new UnsupportedOperationException();      
        /*try {
            getFebData().readVTH4Value = febAccess.readVTH4(
                    new FebInfoImplAxis(ccuAddress, channel, addr, endcap));
        } catch (RemoteException e) {
            throw new FacesException(e);
        } catch (ServiceException e) {
            throw new FacesException(e);
        } */
    }
    ///


    public float getReadVMon1Value() {
        return getFebData().readVMon1Value;
    }

    public void setReadVMon1Value(float readVMon1Value) {
        getFebData().readVMon1Value = readVMon1Value;
    }

    public float getWriteVMon1Value() {
        return getFebData().writeVMon1Value;
    }

    public void setWriteVMon1Value(float writeVMon1Value) {
        getFebData().writeVMon1Value = writeVMon1Value;
    }

    public void writeVMon1(ActionEvent event) {        
        try {
            febAccess.writeVMon1(
                    new FebInfoImplAxis(ccuAddress, channel, addr, endcap), getFebData().writeVMon1Value);
        } catch (RemoteException e) {
            throw new FacesException(e);
        } catch (ServiceException e) {
            throw new FacesException(e);
        } 
    }

    public void readVMon1(ActionEvent event) {        
        try {
            getFebData().readVMon1Value = febAccess.readVMon1(
                    new FebInfoImplAxis(ccuAddress, channel, addr, endcap));
        } catch (RemoteException e) {
            throw new FacesException(e);
        } catch (ServiceException e) {
            throw new FacesException(e);
        } 
    }


    public float getReadVMon2Value() {
        return getFebData().readVMon2Value;
    }

    public void setReadVMon2Value(float readVMon2Value) {
        getFebData().readVMon2Value = readVMon2Value;
    }

    public float getWriteVMon2Value() {
        return getFebData().writeVMon2Value;
    }

    public void setWriteVMon2Value(float writeVMon2Value) {
        getFebData().writeVMon2Value = writeVMon2Value;
    }

    public void writeVMon2(ActionEvent event) {        
        try {
            febAccess.writeVMon2(
                    new FebInfoImplAxis(ccuAddress, channel, addr, endcap), getFebData().writeVMon2Value);
        } catch (RemoteException e) {
            throw new FacesException(e);
        } catch (ServiceException e) {
            throw new FacesException(e);
        } 
    }

    public void readVMon2(ActionEvent event) {        
        try {
            getFebData().readVMon2Value = febAccess.readVMon2(
                    new FebInfoImplAxis(ccuAddress, channel, addr, endcap));
        } catch (RemoteException e) {
            throw new FacesException(e);
        } catch (ServiceException e) {
            throw new FacesException(e);
        } 
    }
    

    public float getReadVMon3Value() {
        return getFebData().readVMon3Value;
    }

    public void setReadVMon3Value(float readVMon3Value) {
        getFebData().readVMon3Value = readVMon3Value;
    }

    public float getWriteVMon3Value() {
        return getFebData().writeVMon3Value;
    }

    public void setWriteVMon3Value(float writeVMon3Value) {
        getFebData().writeVMon3Value = writeVMon3Value;
    }

    public void writeVMon3(ActionEvent event) {  
        throw new UnsupportedOperationException();
        /*try {
            febAccess.writeVMon3(
                    new FebInfoImplAxis(ccuAddress, channel, addr, endcap), getFebData().writeVMon3Value);
        } catch (RemoteException e) {
            throw new FacesException(e);
        } catch (ServiceException e) {
            throw new FacesException(e);
        } */
    }

    public void readVMon3(ActionEvent event) {    
        throw new UnsupportedOperationException();    
        /*try {
            getFebData().readVMon3Value = febAccess.readVMon3(
                    new FebInfoImplAxis(ccuAddress, channel, addr, endcap));
        } catch (RemoteException e) {
            throw new FacesException(e);
        } catch (ServiceException e) {
            throw new FacesException(e);
        } */
    }
    
    //////
    public float getReadVMon4Value() {
        return getFebData().readVMon4Value;
    }

    public void setReadVMon4Value(float readVMon4Value) {
        getFebData().readVMon4Value = readVMon4Value;
    }

    public float getWriteVMon4Value() {
        return getFebData().writeVMon4Value;
    }

    public void setWriteVMon4Value(float writeVMon4Value) {
        getFebData().writeVMon4Value = writeVMon4Value;
    }

    public void writeVMon4(ActionEvent event) {  
        throw new UnsupportedOperationException();
        /*try {
            febAccess.writeVMon4(
                    new FebInfoImplAxis(ccuAddress, channel, addr, endcap), getFebData().writeVMon4Value);
        } catch (RemoteException e) {
            throw new FacesException(e);
        } catch (ServiceException e) {
            throw new FacesException(e);
        } */
    }

    public void readVMon4(ActionEvent event) {    
        throw new UnsupportedOperationException();    
        /*try {
            getFebData().readVMon4Value = febAccess.readVMon4(
                    new FebInfoImplAxis(ccuAddress, channel, addr, endcap));
        } catch (RemoteException e) {
            throw new FacesException(e);
        } catch (ServiceException e) {
            throw new FacesException(e);
        } */
    }
    //////


    public void setup(ActionEvent event) {   
        febAccess = new FebAccessImplAxis(xdaqUrl, "XdaqLBoxAccess", xdaqAppInstance);
    }

    public void configure(ActionEvent event) {        
        try {
            febAccess.configure();
        } catch (RemoteException e) {
            throw new FacesException(e);
        } catch (ServiceException e) {
            throw new FacesException(e);
        } 
    }

    public void testDb(ActionEvent event) {        
        try {
            DbService dbService = new DbServiceClientImplAxis(
                    new URL("http://localhost:8080/rpct-dbservice/services/urn:rpct-dbservice"));
            FebAccessInfo[] infos = dbService.getFebsByChamberLocation(1, 1, 1, "", BarrelOrEndcap.Endcap);
            testMessage = "What's the answer?";
            for (FebAccessInfo info : infos) {
                testMessage = testMessage + info.toString();
            }

        } catch (Exception e) {
            testMessage = e.getMessage();
            throw new FacesException(e);
        } 
    }

    public String getTestMessage() {
        return testMessage;
    }

    public void setTestMessage(String testMessage) {
        this.testMessage = testMessage;
    }

    public String getXdaqUrl() {
        return xdaqUrl.toString();
    }

    public void setXdaqUrl(String xdaqUrl) {
        try {
            this.xdaqUrl = new URL(xdaqUrl);
        } catch (MalformedURLException e) {
            throw new FacesException(e);
        }
    }

    public int getXdaqAppInstance() {
        return xdaqAppInstance;
    }

    public void setXdaqAppInstance(int xdaqAppInstance) {
        this.xdaqAppInstance = xdaqAppInstance;
    }
    
}
