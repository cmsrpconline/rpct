package rpct.web.bean;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIData;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import rpct.db.DataAccessException;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkBox;

/**
 * Created on 2005-03-01
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class LinkBoardBean {
    
    private final static String ANY_LABEL = "[Any]";

    private UIData linkBoardTable;

    //private LinkBoxDAO daoLinkBox = new LinkBoxDAOHibernate();
    //private LinkBoardDAO daoLinkBoard = new LinkBoardDAOHibernate();
    private List<LinkBoard> linkBoards;

    private String searchByLinkBox;
    List<SelectItem> listBoxSelectItems;
    Map<String, LinkBox> linkBoxesByName;
    
    private String searchByMaster;
    private String searchByLinkName;
    private String searchByStripInversion;
    
    private Boolean showCablesSGN = false;
    
    public LinkBoardBean() throws DataAccessException {  
        
        /*List<LinkBox> linkBoxes;
        linkBoxes = daoLinkBox.getAll();
        listBoxSelectItems = new ArrayList<SelectItem>(linkBoxes.size());
        listBoxSelectItems.add(new SelectItem(ANY_LABEL, ANY_LABEL));
        linkBoxesByName = new HashMap<String, LinkBox>(linkBoxes.size());
        for (LinkBox linkBox : linkBoxes) {
            listBoxSelectItems.add(new SelectItem(linkBox.getName()));
            linkBoxesByName.put(linkBox.getName(), linkBox);
        }   
        searchByLinkBox = (String) listBoxSelectItems.get(0).getValue();*/
    }
    
    
    
    public UIData getLinkBoardTable() {
        return linkBoardTable;
    }
    
    public void setLinkBoardTable(UIData linkBoardTable) {
        this.linkBoardTable = linkBoardTable;
    }
    
    public String getSearchByLinkBox() {
        return searchByLinkBox;
    }

    public void setSearchByLinkBox(String searchByLinkBox) {
        this.searchByLinkBox = searchByLinkBox;
    }

    public List getLinkBoxSelectItems() {  
        return listBoxSelectItems;
    }   
    
    public String getSearchByMaster() {
        return searchByMaster;
    }    

    public void setSearchByMaster(String searchByMaster) {
        this.searchByMaster = searchByMaster;
    }  
    

    public String getSearchByLinkName() {
        return searchByLinkName;
    }
    

    public void setSearchByLinkName(String searchByLinkName) {
        this.searchByLinkName = searchByLinkName;
    }
    
    

    public String getSearchByStripInversion() {
        return searchByStripInversion;
    }
    

    public void setSearchByStripInversion(String searchByStripInversion) {
        this.searchByStripInversion = searchByStripInversion;
    }

    public Boolean getShowCablesSGN() {
        return showCablesSGN;
    }
    

    public void setShowCablesSGN(Boolean showCablesSGN) {
        this.showCablesSGN = showCablesSGN;
    }    

    
    public void search(ActionEvent event) {        
        /*try {
            LinkBox linkBox = ANY_LABEL.equals(searchByLinkBox) ? null : linkBoxesByName.get(searchByLinkBox);
            Boolean master = ANY_LABEL.equals(searchByMaster) ? null : Boolean.parseBoolean(searchByMaster);
            String linkName = StringUtils.toUpperCase(StringUtils.trimToNull(searchByLinkName));
            Boolean stripInversion = ANY_LABEL.equals(searchByStripInversion) ? null : Boolean.parseBoolean(searchByStripInversion);
            
            linkBoards = daoLinkBoard.get(linkBox, master, linkName, stripInversion, true, showCablesSGN);
        } catch (DataAccessException e) {
            throw new FacesException(e);
        }*/
    }
    
    public List getLinkBoards() {
        if (linkBoards == null)
            return Collections.EMPTY_LIST;
        return linkBoards;
    }
    
    /*public String showLinkBoxDetails() {
        FacesContext context = FacesContext.getCurrentInstance();      
        
        LinkBox linkBox;
        try {
            // retrieve linkBox in the new hibernate session, so that lazy initializing works fine
            linkBox = daoLinkBox.getById(((LinkBoard) linkBoardTable.getRowData()).getLinkBox().getId());
        } catch (DataAccessException e) {
            throw new FacesException(e);
        }
        context.getApplication().createValueBinding("#{LinkBoxDetailsBean.selectedLinkBox}")
            .setValue(context, linkBox);
        return Constants.LINK_BOX_DETAILS_OUTCOME;
    }*/
}
