package rpct.web.bean;

import javax.faces.component.UIData;
import javax.faces.context.FacesContext;

import rpct.db.domain.equipment.LinkBox;
import rpct.web.Constants;

/**
 * Created on 2005-03-01
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class LinkBoxBean {
    
    private UIData linkBoxTable;
        
    //LinkBoxDAO daoLinkBox = new LinkBoxDAOHibernate();
    
    public LinkBoxBean() {  
    }
/*
    public List getLinkBoxes() {  
        try {
            return daoLinkBox.getAll(); 
        } catch (DataAccessException e) {
            throw new FacesException(e);
        }
    }*/

    public UIData getLinkBoxTable() {
        return linkBoxTable;
    }
    
    public void setLinkBoxTable(UIData linkBoxTable) {
        this.linkBoxTable = linkBoxTable;
    }
    
    public String showLinkBoxDetails() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getApplication().createValueBinding("#{LinkBoxDetailsBean.selectedLinkBox}")
            .setValue(context, (LinkBox) linkBoxTable.getRowData());
        return Constants.LINK_BOX_DETAILS_OUTCOME;
    }
    
}
