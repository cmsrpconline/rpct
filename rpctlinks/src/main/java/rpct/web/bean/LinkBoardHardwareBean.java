package rpct.web.bean;

import java.util.Date;

import javax.faces.component.UIData;

/**
 * Created on 2005-03-30
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class LinkBoardHardwareBean {
    
    private UIData linkBoardTable;
    
    private String newLinkBoardName;
    private Date newLinkBoardProductionDate;
    private String newLinkBoardNotes;
    
    //private HardwareItemDAO daoHardwareItem = new HardwareItemDAOHibernate();

/*
    public List getLinkBoards() {  
        try {
            return daoHardwareItem.getAllLinkBoards(); 
        } catch (DataAccessException e) {
            throw new FacesException(e);
        }
    }    */

    public UIData getLinkBoardTable() {
        return linkBoardTable;
    }

    public void setLinkBoardTable(UIData linkBoardTable) {
        this.linkBoardTable = linkBoardTable;
    }
    
    public String getNewLinkBoardName() {
        return newLinkBoardName;
    }

    public void setNewLinkBoardName(String newLinkBoardName) {
        this.newLinkBoardName = newLinkBoardName;
    }
    
    public String getNewLinkBoardNotes() {
        return newLinkBoardNotes;
    }
    
    public void setNewLinkBoardNotes(String newLinkBoardNotes) {
        this.newLinkBoardNotes = newLinkBoardNotes;
    }
    
    public Date getNewLinkBoardProductionDate() {
        return newLinkBoardProductionDate;
    }
    
    public void setNewLinkBoardProductionDate(Date newLinkBoardProductionDate) {
        this.newLinkBoardProductionDate = newLinkBoardProductionDate;
    }
    
    /*public void addNewLinkBoard(ActionEvent event) {
        LinkBoardHardware linkBoard = new LinkBoardHardware();
        
        linkBoard.setName(newLinkBoardName);
        linkBoard.setProductionDate(newLinkBoardProductionDate);
        linkBoard.setNotes(newLinkBoardNotes);
        try {
            daoHardwareItem.saveObject(linkBoard);
        }
        catch (DataAccessException e) {
            throw new FacesException(e);
        }    
    }     */

    /*public void deleteLinkBoard(ActionEvent event) {
        LinkBoardHardware linkBoard = (LinkBoardHardware) linkBoardTable.getRowData();
        try {
            daoHardwareItem.deleteObject(linkBoard);
        }
        catch (DataAccessException e) {
            throw new FacesException(e);
        }
    }
    */
}
