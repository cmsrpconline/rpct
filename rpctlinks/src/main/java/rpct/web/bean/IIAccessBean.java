package rpct.web.bean;

import java.beans.IntrospectionException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.FacesException;
import javax.faces.convert.Converter;
import javax.faces.convert.NumberConverter;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.xml.rpc.ServiceException;

import oracle.adf.view.faces.component.core.data.CoreTreeTable;
import oracle.adf.view.faces.model.ChildPropertyTreeModel;
import oracle.adf.view.faces.model.TreeModel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import rpct.web.ApplicationResources;
import rpct.web.util.HexConverter;
import rpct.xdaq.XdaqApplicationInfo;
import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.AccessType;
import rpct.xdaqaccess.Board;
import rpct.xdaqaccess.Crate;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.DeviceItem;
import rpct.xdaqaccess.DeviceItemArea;
import rpct.xdaqaccess.DeviceItemBits;
import rpct.xdaqaccess.DeviceItemType;
import rpct.xdaqaccess.DeviceItemVector;
import rpct.xdaqaccess.DeviceItemWord;
import rpct.xdaqaccess.HardwareItem;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.IIAccess;
import rpct.xdaqaccess.axis.IIAccessImplAxis;

/**
 * Created on 2005-05-06
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class IIAccessBean {
    private static Log LOG = LogFactory.getLog(IIAccessBean.class);
        
    private Crate[] crates; 
    //private List<IIAccess> iiAccessList = new ArrayList<IIAccess>();
    /*private List<SelectItem> deviceSelectItems;*/
    /*private Integer deviceId;*/
    /*private UIData deviceItemsTable;*/
    private CoreTreeTable devicesTree;
    private TreeModel devicesTreeModel;
    private Map<String, Converter> convertersMap = new HashMap<String, Converter>();
    private Converter converter;
    private List<SelectItem> converterSelectItems = new ArrayList<SelectItem>();    

   /* private Map<Integer, Device> devicesById;  
    private Map<Integer, List<DeviceItemData>> deviceItemsDataById;  */
    
    public interface TreeItemInfo {
        String getName();
        String getType();
        Integer getWidth();
        Binary getReadValue();
        Binary getWriteValue();
        void setWriteValue(Binary writeValue);
        boolean isReadable();
        boolean isWriteable();
        public void read();
        public void write();
    }
    
    
    public class HardwareItemInfo implements TreeItemInfo {   
        private HardwareItem hardwareItem;
        private String name;
        
        public HardwareItemInfo(HardwareItem hardwareItem) {
            this.hardwareItem = hardwareItem;
            if (hardwareItem == null) {
                this.name = "System";
            }
            else if (hardwareItem instanceof Board) {
                Board board = (Board) hardwareItem;
                this.name = board.getName() + " (" + hardwareItem.getId() +
                    ", " + board.getPosition() + ")";
            }
            else {
                this.name = hardwareItem.getName() + " (" + hardwareItem.getId() + ")";
            }
        }
        
        public String getName() {
            return name;
        }
        public String getType() { 
            if (hardwareItem == null) {
                return "";
            }
            return hardwareItem.getType();
        }
        public boolean isReadable() {
            return hardwareItem != null;
        }
        public boolean isWriteable() {
            return false;
        }
        public Integer getWidth() {
            return null;
        }
        public Binary getReadValue() {
            return null;
        }
        public Binary getWriteValue() {
            return null;
        }
        public void setWriteValue(Binary writeValue) {            
        }

        public void read() {           
        }

        public void write() {         
        }    
    }    
    
    public static class DeviceItemData implements TreeItemInfo  {
        
        private DeviceItem deviceItem;
        private int index; // used for indexed device items
        private Binary readValue;
        private Binary writeValue;
        private String name;
        public DeviceItemData(DeviceItem item, int index) {
            this.deviceItem = item;
            this.index = index;  
            StringBuilder nameBuilder = new StringBuilder(item.getName());
            
            if (item.getNumber() > 1 || item.getType().equals(DeviceItemType.AREA)) { 
                if (index == -1) {
                    nameBuilder.append("[]");                    
                }
                else {
                    nameBuilder.append('[').append(index).append(']');
                }
            }
            this.name = nameBuilder.toString();
        }
        public DeviceItem getDeviceItem() {
            return deviceItem;
        }
        public int getIndex() {
            return index;
        }
        public Binary getReadValue() {
            return readValue;
        }
        public void setReadValue(Binary readValue) {
            this.readValue = readValue;
            if (writeValue == null) {
                //writeValue = new Binary(this.readValue.getBytes());
                writeValue = readValue;
            }
        }
        public Binary getWriteValue() {
            return writeValue;
        }
        public void setWriteValue(Binary writeValue) {
            this.writeValue = writeValue;
        }
        public String getName() {
            return name;
        }    
        public String getType() {
            return deviceItem.getType().toString();
        }   
        public boolean isReadable() { 
            if (deviceItem.getType().equals(DeviceItemType.AREA) && index == -1) {
                return false;
            }
            AccessType accessType = deviceItem.getDesc().getAccessType();
            return accessType == AccessType.READ || accessType == AccessType.BOTH;
        } 
        public boolean isNestedRead() {
            if (deviceItem.getType().equals(DeviceItemType.WORD) && index == -1) {
                return true;
            }
            return false;
        }
        public boolean isWriteable() { 
            if ((deviceItem.getType().equals(DeviceItemType.AREA)
                    || deviceItem.getType().equals(DeviceItemType.WORD)) 
                    && index == -1) {
                return false;
            }
            AccessType accessType = deviceItem.getDesc().getAccessType(); 
            return accessType == AccessType.WRITE || accessType == AccessType.BOTH;
        }
        public void write() {
            if (!isWriteable()) {
                return;
            }
            if (writeValue == null) {
                return;
            }
            try {
                if (deviceItem instanceof DeviceItemWord) {
                    DeviceItemWord word = (DeviceItemWord) deviceItem;
                    word.writeBinary(index, writeValue);
                }
                else if (deviceItem instanceof DeviceItemBits) {
                    DeviceItemBits bits = (DeviceItemBits) deviceItem;
                    bits.writeBinary(index, writeValue);
                }
                else if (deviceItem instanceof DeviceItemVector) {
                    DeviceItemVector vector = (DeviceItemVector) deviceItem;
                    vector.writeBinary(writeValue);
                }
                else if (deviceItem instanceof DeviceItemArea) {
                    DeviceItemArea area = (DeviceItemArea) deviceItem;
                    area.writeAreaBinary(index, new Binary[] { writeValue } );
                }
            } catch (RemoteException e) {
                throw new FacesException(e);
            } catch (ServiceException e) {
                throw new FacesException(e);
            }
            
        }
        public void read() {
            if (!isReadable()) {
                return;
            }
            if (index == -1) {
            	return;            	
            }
            try {
                if (deviceItem instanceof DeviceItemWord) {
                    DeviceItemWord word = (DeviceItemWord) deviceItem;
                    setReadValue(word.readBinary(index));
                }
                else if (deviceItem instanceof DeviceItemBits) {
                    DeviceItemBits bits = (DeviceItemBits) deviceItem;
                    setReadValue(bits.readBinary(index));
                }
                else if (deviceItem instanceof DeviceItemVector) {
                    DeviceItemVector vector = (DeviceItemVector) deviceItem;
                    setReadValue(vector.readBinary());
                }
                else if (deviceItem instanceof DeviceItemArea) {
                    DeviceItemArea area = (DeviceItemArea) deviceItem;
                    setReadValue(area.readAreaBinary(index, 1)[0]);
                }
            } catch (RemoteException e) {
                throw new FacesException(e);
            } catch (ServiceException e) {
                throw new FacesException(e);
            }            
        }
        public Integer getWidth() {
            return deviceItem.getDesc().getWidth();
        }
    }
    
    public class TreeItem {
        private TreeItemInfo treeItemInfo;
        private List<TreeItem> items = null;
        
        public TreeItem(TreeItemInfo treeItemInfo) {
            this.treeItemInfo = treeItemInfo;
        }

        public String getName() {
            return treeItemInfo.getName();
        }

        public Binary getReadValue() {
            return treeItemInfo.getReadValue();
        }

        public String getType() {
            return treeItemInfo.getType();
        }

        public Integer getWidth() {
            return treeItemInfo.getWidth();
        }

        public Binary getWriteValue() {
            return treeItemInfo.getWriteValue();
        }

        public boolean isReadable() {
            return treeItemInfo.isReadable();
        }

        public boolean isWriteable() {
            return treeItemInfo.isWriteable();
        }

        public void setWriteValue(Binary writeValue) {
            treeItemInfo.setWriteValue(writeValue);
        }

        public List<TreeItem> getItems() {
            return items;
        }    
        
        public void addItem(TreeItem item) {
            if (items == null) {
                items = new ArrayList<TreeItem>();
            }
            items.add(item);
        }
        
        public void read() {
            if (isNestedRead()) {
                for (TreeItem item : items) {
                    item.read();
                }
            }
            else {
                treeItemInfo.read();
            }
        }
        
        public void write() {
            treeItemInfo.write();
        }
        
        public boolean isNestedRead() {
        	if (!isReadable()) {
        		return false;
        	}
        	if (treeItemInfo instanceof HardwareItemInfo) {
        		return true;
        	}
        	if (treeItemInfo instanceof DeviceItemData) {
        		DeviceItemData deviceItemData = (DeviceItemData) treeItemInfo;
        		return deviceItemData.isNestedRead();
        	}
        	return false;
        }
        
        /*public boolean isNestedWrite() {
            return (treeItemInfo instanceof DeviceInfo) && isWriteable();
        }*/
    }

    public IIAccessBean() throws MalformedURLException, UnknownHostException, RemoteException, ServiceException {
        /*HardwareRegistry hardwareRegistry = new HardwareRegistry();
        for (XdaqApplicationInfo appInfo : ApplicationResources.getIIAccessApplications()) {
            System.out.println("IIAccessBean: connecting to " + appInfo.getUrl() + "," + 
                    appInfo.getClassName() + "," + appInfo.getInstance());
            XdaqApplicationInfo info = new XdaqApplicationInfo(appInfo.getUrl(), appInfo.getClassName(), appInfo.getInstance());
            hardwareRegistry.registerXdaqApplication(info);
            iiAccessList.add(hardwareRegistry.getXdaqApplicationAccess(info).getIIAccess());
        }*/
        /*URL localHostUrl = new URL("http://localhost:1972");
    	System.out.println("IIAccessBean: connecting to " + localHostUrl.toString());
        iiAccessList.add(new IIAccessImplAxis(localHostUrl, "XdaqTestBench", 0));
        iiAccessList.add(new IIAccessImplAxis(localHostUrl, "XdaqTestBench", 1));
        iiAccessList.add(new IIAccessImplAxis(localHostUrl, "XdaqTestBench", 2));
        iiAccessList.add(new IIAccessImplAxis(localHostUrl, "XdaqTestBench", 3));*/
        
        converter = new HexConverter();
        convertersMap.put(HexConverter.CONVERTER_ID, converter);
        converterSelectItems.add(new SelectItem(HexConverter.CONVERTER_ID, "hex"));
        
        NumberConverter numberConverter = new NumberConverter();
        numberConverter.setType("number");        
        convertersMap.put(NumberConverter.CONVERTER_ID, numberConverter);        
        converterSelectItems.add(new SelectItem(NumberConverter.CONVERTER_ID, "dec"));
    }

    private void addNodes(TreeItem parent, DeviceItem item) {
        if (item.getDesc().getType() == DeviceItemType.AREA
                || (item.getDesc().getType() == DeviceItemType.WORD && item.getNumber() > 1)) {
            TreeItem groupNode = new TreeItem(new DeviceItemData(item, -1)); 
            parent.addItem(groupNode);
            for (int i = 0; i < item.getNumber(); i++) {
                groupNode.addItem(new TreeItem(new DeviceItemData(item, i)));                
            }
        }
        else if (item.getNumber() > 1) {
            for (int i = 0; i < item.getNumber(); i++) {
                parent.addItem(new TreeItem(new DeviceItemData(item, i)));
            }            
        }
        else {
            parent.addItem(new TreeItem(new DeviceItemData(item, 0)));
        }
    }
    
    private void loadCrates() {    
        List<Crate> crateList = new ArrayList<Crate>();
        /*for (IIAccess access : iiAccessList) {
            try {
                Collections.addAll(crateList, access.getCrates());
            }
            catch (Exception e) {
                LOG.warn("Error during loding crates", e);
            }
        }*/
    	
    	HardwareRegistry hardwareRegistry = new HardwareRegistry();
        for (XdaqApplicationInfo appInfo : ApplicationResources.getIIAccessApplications()) {
            System.out.println("IIAccessBean: connecting to " + appInfo.getUrl() + "," + 
                    appInfo.getClassName() + "," + appInfo.getInstance());
            hardwareRegistry.registerXdaqApplication(appInfo);
            try {
                Collections.addAll(crateList, 
                		hardwareRegistry.getXdaqApplicationAccess(appInfo).getIIAccess().getCrates(true));
            }
            catch (Exception e) {
                LOG.warn("Error during loding crates", e);
            }
        }
        
        crates = crateList.toArray(new Crate[] {});
        TreeItem root = new TreeItem(new HardwareItemInfo(null));
        for (Crate crate : crates) {
            Board[] boards = crate.getBoards();
            TreeItem crateNode = new TreeItem(new HardwareItemInfo(crate));
            root.addItem(crateNode);
            for (Board board : boards) {
                Device[] devices = board.getDevices();
                TreeItem boardNode = new TreeItem(new HardwareItemInfo(board));
                crateNode.addItem(boardNode);                
                for (Device device : devices) {
                    TreeItem deviceNode = new TreeItem(new HardwareItemInfo(device));
                    boardNode.addItem(deviceNode);
                    for (DeviceItem item : device.getItems()) {
                        DeviceItemType itemType = item.getDesc().getType();
                        if (itemType == DeviceItemType.WORD) {
                            addNodes(deviceNode, item);
                        }
                        else if (itemType == DeviceItemType.AREA) {
                            addNodes(deviceNode, item);
                        }
                        else if (itemType == DeviceItemType.VECT) {
                            DeviceItemVector vectorItem = (DeviceItemVector) item;
                            TreeItem vectorNode = new TreeItem(new DeviceItemData(item, 0));
                            deviceNode.addItem(vectorNode);
                            for (DeviceItemBits bitsItem : vectorItem.getBits()) {
                                addNodes(vectorNode, bitsItem);
                            }                                                             
                        }                                                       
                    }
                }    
            }
        }

        try {
            devicesTreeModel = new ChildPropertyTreeModel(root, "items");
        } catch (IntrospectionException e) {
            throw new FacesException(e);
        }
    }
    
    
    public void getCrates(ActionEvent event) {  
        loadCrates();
    }
    /*
    public List<SelectItem> getDeviceSelectItems() {
        if (crates == null) {
            loadCrates();
        }
        return deviceSelectItems;
    }
    
    public Integer getDeviceId() {
        return deviceId;
    }
    
    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }
    
    public Device getDevice() {
        return devicesById.get(deviceId);
    }*/
    /*
    public UIData getDeviceItemsTable() {
        return deviceItemsTable;
    }
    
    public void setDeviceItemsTable(UIData deviceItemsTable) {
        this.deviceItemsTable = deviceItemsTable;
    }    */
/*
    public Map<Integer, List<DeviceItemData>> getDeviceItemsDataById() {
        return deviceItemsDataById;
    }   */
    
    /*public Map<Integer, DefaultTreeModel> getDeviceTreeModelsById() {
        return deviceTreeModelsById;
    }*/


    public void readTree(ActionEvent event) {  
        TreeItem treeItem = (TreeItem) devicesTree.getRowData();
        treeItem.read();
    }
    
    public void writeTree(ActionEvent event) {  
        TreeItem treeItem = (TreeItem) devicesTree.getRowData();
        treeItem.write();
    }
    /*
    public void readAll(ActionEvent event) {  
        List<DeviceItemData> deviceItemsData = deviceItemsDataById.get(deviceId);
        for (DeviceItemData data : deviceItemsData) {
            if (data.isReadable()) {
                data.read();
            }
        }
    }*/
    
    public Converter getConverter() {
        return converter;
    }
    
    public void setConverter(Converter converter) {
        this.converter = converter;
    }
    
    public String getConverterId() {
        if (converter instanceof HexConverter) {
            return HexConverter.CONVERTER_ID;
        }
        return NumberConverter.CONVERTER_ID;
    }
    
    public void setConverterId(String converterId) {
        converter = convertersMap.get(converterId);
    }
    
    
    public List<SelectItem> getConverterSelectItems() {
        return converterSelectItems;
    }

    public TreeModel getDevicesTreeModel() {
        if (crates == null) {
            loadCrates();
        }
        return devicesTreeModel;
    }

    public CoreTreeTable getDevicesTree() {
        return devicesTree;
    }

    public void setDevicesTree(CoreTreeTable devicesTree) {
        this.devicesTree = devicesTree;
    }    
}
