package rpct.web.bean;

import javax.faces.component.UIData;

import rpct.db.domain.equipment.LinkBox;

/**
 * Created on 2005-03-01
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class LinkBoxDetailsBean {
    
    private UIData linkBoardsTable;
    private UIData cablesSGNTable;
    private LinkBox selectedLinkBox;
        
    
    public LinkBoxDetailsBean() {  
    }
    

    public UIData getLinkBoardsTable() {
        return linkBoardsTable;
    }   


    public void setLinkBoardsTable(UIData linkBoardsTable) {
        this.linkBoardsTable = linkBoardsTable;
    }
    
    public UIData getCablesSGNTable() {
        return cablesSGNTable;
    }   


    public void setCablesSGNTable(UIData cablesSGNTable) {
        this.cablesSGNTable = cablesSGNTable;
    }
    


    public LinkBox getSelectedLinkBox() {
        return selectedLinkBox;
    }    

    public void setSelectedLinkBox(LinkBox selectedLinkBox) {
        this.selectedLinkBox = selectedLinkBox;
    }
    
    public int getLinkBoardsRowIndex() {
        return linkBoardsTable.getRowIndex();
    }
    
    public int getCablesSGNRowIndex() {
        return cablesSGNTable.getRowIndex();
    }
}
