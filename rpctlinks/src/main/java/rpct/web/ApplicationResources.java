package rpct.web;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import rpct.xdaq.XdaqApplicationInfo;

public class ApplicationResources {
    
    private final static String PROPERTIES_FILE_NAME = "rpctlinks.properties";
    private static List<XdaqApplicationInfo> iiAccessApplications = new ArrayList<XdaqApplicationInfo>();

    static {
        Properties properties = new Properties();
        try {
            InputStream is = ApplicationResources.class.getClassLoader()
                    .getResourceAsStream(PROPERTIES_FILE_NAME);
            if (is != null) {
                try {
                    properties.load(is);
                } finally {
                    is.close();
                }
                
                int max = Integer.parseInt(properties.getProperty("rpct.web.iiaccess.app.max", "10"));
                for (int i = 0; i <= max; i++) {
                    String nameBase = "rpct.web.iiaccess.app." + i;
                    String url = properties.getProperty(nameBase + ".url");
                    if (url != null) {
                        XdaqApplicationInfo info = new XdaqApplicationInfo(
                            new URL(url), properties.getProperty(nameBase + ".class"),
                            Integer.parseInt(properties.getProperty(nameBase + ".instance")));
                        iiAccessApplications.add(info);
                    }
                }
                
            } else {
                throw new ExceptionInInitializerError("Could not load "
                        + PROPERTIES_FILE_NAME);
            }
        } catch (IOException e) {
            throw new ExceptionInInitializerError(e);
        }
    }
    
    public static List<XdaqApplicationInfo> getIIAccessApplications() {
        return iiAccessApplications;
    }
}
