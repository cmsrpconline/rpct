/** Filip Thyssen */

#ifndef rpct_devices_DTFebSystem_inl_h_
#define rpct_devices_DTFebSystem_inl_h_

#include "rpct/devices/DTFebSystem.h"

namespace rpct {

inline const HardwareItemType & DTFebSystem::getType() const
{
    return *type_;
}

} // namespace rpct

#endif // rpct_devices_DTFebSystem_inl_h_
