/** Filip Thyssen */

#ifndef rpct_devices_DTFebAccessPoint_h_
#define rpct_devices_DTFebAccessPoint_h_

#include "rpct/devices/IFebAccessPoint.h"

namespace rpct {

class DTFebSystem;

class DTFebAccessPoint
    : public IFebAccessPoint
{
public:
    DTFebAccessPoint(DTFebSystem & system
                     , int id
                     , std::string const & ccb_server
                     , short ccb_port
                     , bool disabled = false);
    ~DTFebAccessPoint();

};

} // namespace rpct

#endif // rpct_devices_DTFebAccessPoint_h_
