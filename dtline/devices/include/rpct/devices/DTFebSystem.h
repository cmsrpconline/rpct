/** Filip Thyssen */

#ifndef rpct_devices_DTFebSystem_h_
#define rpct_devices_DTFebSystem_h_

#include "rpct/devices/FebSystem.h"

namespace rpct {

class DTFebAccessPoint;

namespace devices {
namespace dti2c {
class DTI2c;
} // namespace dti2c
} // namespace devices

namespace i2c {
class II2cAccess;
} // namespace i2c


class DTFebSystem : public virtual FebSystem
{
public:
    DTFebSystem();
    ~DTFebSystem();

    void reset();

    // THardwareItem
    const HardwareItemType & getType() const;

    IFebAccessPoint & addDTFebAccessPoint(int id
                                          , std::string const & ccb_server, short ccb_port
                                          , bool disabled = false)
    throw (rpct::exception::SystemException);

    rpct::i2c::II2cAccess & getDTI2c(std::string const & ccb_server);

private:
    static log4cplus::Logger logger_;
    static HardwareItemType * type_;

    std::map<std::string, rpct::devices::dti2c::DTI2c *> dti2cs_;
};

} // namespace rpct

#include "rpct/devices/DTFebSystem-inl.h"

#endif // rpct_devices_DTFebSystem_h_
