/** Filip Thyssen */

#ifndef rpct_devices_dti2c_DTI2c_h_
#define rpct_devices_dti2c_DTI2c_h_

#include <log4cplus/logger.h>

#include "rpct/ii/i2c/II2cAccess.h"

#include "rpct/devices/dti2c/const.h"

class Ccommand;
class Ci2c;

namespace rpct {
namespace devices {
namespace dti2c {

class DTI2c : public rpct::i2c::II2cAccess
{
protected:
    // CCB
    void create_ccb_objects();
    void destroy_ccb_objects();

    void disconnect();
    bool connect();

    void setCcbPort(short ccb_port);

    // II2cAccess
    void idelay(unsigned int nseconds)
        throw (rpct::exception::Exception);
    void iread(unsigned short ccb_port
               , unsigned char address
               , unsigned char * data = 0
               , unsigned char count = 1
               , rpct::i2c::II2cResponse * i2c_response = 0)
        throw (rpct::exception::Exception);
    void iwrite(unsigned short ccb_port
                , unsigned char address
                , unsigned char const * data
                , unsigned char count = 1
                , rpct::i2c::II2cResponse * i2c_response = 0)
        throw (rpct::exception::Exception);

public:
    DTI2c(std::string const & ccb_server, short ccb_port = 0);
    ~DTI2c();

    // II2cAccess
    void log(rpct::exception::Exception const * e) const;
    void execute() throw (rpct::exception::Exception);

    void reset();

    bool startGroup();
    void cancelGroup();
    void endGroup();
    bool groupMode() const;

private:
    static log4cplus::Logger logger_;

    const std::string ccb_server_;
    char * c_ccb_server_;
    short ccb_port_;

    short input_[buffer_size_], output_[buffer_size_];

    Ccommand * ccb_command_;
    Ci2c * ccb_i2c_;

    bool group_mode_;
    bool error_;
};

} // namespace dti2c
} // namespace devices
} // namespace rpct

#include "rpct/devices/dti2c/DTI2c-inl.h"

#endif // rpct_devices_dti2c_DTI2c_h_
