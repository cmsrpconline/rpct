/** Filip Thyssen */

#ifndef rpct_devices_dti2c_const_h_
#define rpct_devices_dti2c_const_h_

#include "ccbcmds/Ci2c.h"
#include "ccbcmds/Cdef.h"

namespace rpct {
namespace devices {
namespace dti2c {

const int ccb_server_port_ = CCBPORT;
const int buffer_size_     = I2C_DATA_MAX;
const int buffer_min_      = I2C_DATA_MIN;

namespace command {

const short start_ = 0x0100;
const short stop_  = 0x0200;
const short read_  = 0x0400;
const short ackn_  = 0x0800;
const short aboe_  = 0x1000;

} // namespace command

namespace state {

const char ok_        =  0;
const char write_ack_ =  1;
const char SDA_short_ = -1;
const char SCL_short_ = -2;

} // namespace state

} // namespace dti2c
} // namespace devices
} // namespace rpct

#endif // rpct_devices_dti2c_const_h_
