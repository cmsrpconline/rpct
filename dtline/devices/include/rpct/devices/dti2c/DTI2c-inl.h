/** Filip Thyssen */

#ifndef rpct_devices_dti2c_DTI2c_inl_h_
#define rpct_devices_dti2c_DTI2c_inl_h_

#include "rpct/devices/dti2c/DTI2c.h"

#include "log4cplus/loggingmacros.h"

namespace rpct {
namespace devices {
namespace dti2c {

inline void DTI2c::log(rpct::exception::Exception const * e) const
{
    if (e)
        {
            LOG4CPLUS_WARN(logger_, e->what());
        }
    else
        {
            LOG4CPLUS_WARN(logger_, "DTI2c log called without exception description.");
        }
}

inline void DTI2c::execute() throw (rpct::exception::Exception)
{ // block i2c compatible code will disconnect properly
    disconnect();
}

inline bool DTI2c::startGroup()
{
    if (group_mode_)
        return 0;
    else
        {
            error_ = 0;
            return group_mode_ = 1;
        }
}
inline void DTI2c::cancelGroup()
{
    group_mode_ = 0;
    error_ = 0;
}
inline void DTI2c::endGroup()
{
    group_mode_ = 0;
    error_ = 0;
}

inline bool DTI2c::groupMode() const
{
    return group_mode_;
}

} // namespace dti2c
} // namespace devices
} // namespace rpct

#endif // rpct_devices_dti2c_DTI2c_inl_h_
