#include "rpct/devices/DTFebAccessPoint.h"

#include "rpct/devices/DTFebSystem.h"
#include "rpct/devices/dti2c/DTI2c.h"

namespace rpct {

DTFebAccessPoint::DTFebAccessPoint(DTFebSystem & system
                                   , int id
                                   , std::string const & ccb_server
                                   , short ccb_port
                                   , bool disabled)
    : IFebAccessPoint(system
                      , system.getDTI2c(ccb_server)
                      , id
                      , ccb_server
                      , ccb_port
                      , disabled)
{}

DTFebAccessPoint::~DTFebAccessPoint()
{}

} // namespace rpct
