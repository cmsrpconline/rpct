#include "rpct/devices/dti2c/DTI2c.h"

#include "log4cplus/loggingmacros.h"

#include "ccbcmds/Ccommand.h"
#include "ccbcmds/Ci2c.h"

#include "rpct/ii/i2c/I2cResponse.h"

namespace rpct {
namespace devices {
namespace dti2c {

log4cplus::Logger DTI2c::logger_ = log4cplus::Logger::getInstance("DTI2c");

void DTI2c::create_ccb_objects()
{
    LOG4CPLUS_TRACE(logger_, "DTI2c::create_ccb_objects() for " << ccb_server_);
    destroy_ccb_objects();

    ccb_command_ = new Ccommand(ccb_port_, c_ccb_server_, ccb_server_port_, 0);
}

void DTI2c::destroy_ccb_objects()
{
    LOG4CPLUS_TRACE(logger_, "DTI2c::destroy_ccb_objects() for " << ccb_server_);
    disconnect();
    if (ccb_command_)
        {
            delete ccb_command_;
            ccb_command_ = 0;
        }
}

void DTI2c::disconnect()
{
    LOG4CPLUS_TRACE(logger_, "DTI2c::disconnect() for " << ccb_server_);
    if (ccb_command_)
        ccb_command_->disconnect();
    if (ccb_i2c_)
        {
            delete ccb_i2c_;
            ccb_i2c_ = 0;
        }
}

bool DTI2c::connect()
{
    LOG4CPLUS_TRACE(logger_, "DTI2c::connect() for " << ccb_server_);
    if (!ccb_command_)
        create_ccb_objects();
    if (!ccb_command_->connect())
        {
            destroy_ccb_objects();
            return false;
        }
    if (ccb_i2c_)
        {
            delete ccb_i2c_;
            ccb_i2c_ = 0;
        }
    ccb_i2c_ = new Ci2c(ccb_command_);    
    return true;
}

void DTI2c::setCcbPort(short ccb_port)
{
    if (ccb_port != ccb_port_)
        destroy_ccb_objects();
    ccb_port_ = ccb_port;
}

void DTI2c::idelay(unsigned int nseconds)
    throw (rpct::exception::Exception)
{
    LOG4CPLUS_TRACE(logger_, "DTI2c::idelay() for " << ccb_server_ << ", counting on clock stretching.");
}

void DTI2c::iread(unsigned short ccb_port
                  , unsigned char address
                  , unsigned char * data
                  , unsigned char count
                  , rpct::i2c::II2cResponse * i2c_response)
    throw (rpct::exception::Exception)
{
    LOG4CPLUS_TRACE(logger_, "DTI2c::iread() for " << ccb_server_);

    try {
        if (group_mode_ && error_)
            throw rpct::exception::Exception
                ("dti2c in group mode and an error occured before.");
        else if (!count)
            { // do nothing
            }
        else if (count > (buffer_size_ - 1))
            throw rpct::exception::InputException
                ("too many bytes to read in one command");
        else
            {
                setCcbPort(ccb_port);
                if (!connect())
                    throw rpct::exception::Exception("Could not connect to " + ccb_server_);

                input_[0] = ( command::start_ | ( (address << 1) & 0xfe ) | 0x01 );
                for (int i = 1 ; i < count ; ++i)
                    input_[i] = command::read_;
                input_[count] = command::read_ | command::stop_;

                try {
                    ccb_i2c_->rpc_I2C(input_, (count + 1));
                } catch(...) {
                    throw rpct::exception::Exception("error for i2c command, unknown error sending command.");
                }

                if (ccb_i2c_->rpc_i2c.ccbserver_code)
                    throw rpct::exception::Exception(ccb_i2c_->rpc_i2c.msg);

                if (ccb_i2c_->rpc_i2c.size != (count + 1))
                    throw rpct::exception::Exception("unknown error for i2c command, the response size is wrong.");

                char state;
                for (int i = 0 ; i <= count ; ++i)
                    {
                        state = ((ccb_i2c_->rpc_i2c.err_data[i]) >> 8 & 0x00ff);
                        if (state != state::ok_)
                            {
                                if (state == state::write_ack_)
                                    throw rpct::exception::Exception("error for i2c command, i2c ackn bit was 1.");
                                else if (state == state::SDA_short_)
                                    throw rpct::exception::Exception("error for i2c command, sda short circuit.");
                                else if (state == state::SCL_short_)
                                    throw rpct::exception::Exception("error for i2c command, scl short circuit.");
                                else
                                    throw rpct::exception::Exception("error for i2c command, unknown error in err_data.");
                            }
                    }
                for (int i = 0 ; i < count ; ++i)
                    data[i] = (ccb_i2c_->rpc_i2c.err_data[i+1] & 0x00ff);

                if (i2c_response)
                    i2c_response->i2c_ready();
            }
    } catch (rpct::exception::Exception & e) {
        error_ = 1;
        throw e;
    }
}
void DTI2c::iwrite(unsigned short port
                   , unsigned char address
                   , const unsigned char * data
                   , unsigned char count
                   , rpct::i2c::II2cResponse * i2c_response)
    throw (rpct::exception::Exception)
{
    LOG4CPLUS_TRACE(logger_, "DTI2c::iwrite() for " << ccb_server_);
    try {
        if (group_mode_ && error_)
            throw rpct::exception::Exception
                ("dti2c in group mode and an error occured before.");
        else if (!count)
            { // do nothing
            }
        else if (count > (buffer_size_ - 1))
            throw rpct::exception::InputException
                ("too many bytes to read in one command");
        else
            {
                setCcbPort(port);
                if (!connect())
                    throw rpct::exception::Exception("Could not connect to " + ccb_server_);

                input_[0] = ( command::start_ | ( (address << 1) & 0xfe) );
                for (int i = 1 ; i < count ; ++i)
                    input_[i] = (data[i-1] & 0xff);
                input_[count] = (command::stop_ | (data[count-1] & 0xff) );

                try {
                    ccb_i2c_->rpc_I2C(input_, (count + 1));
                } catch(...) {
                    throw rpct::exception::Exception("error for i2c command, unknown error sending command.");
                }

                if (ccb_i2c_->rpc_i2c.ccbserver_code)
                    throw rpct::exception::Exception(ccb_i2c_->rpc_i2c.msg);

                if (ccb_i2c_->rpc_i2c.size != (count + 1))
                    throw rpct::exception::Exception("unknown error for i2c command, the response size is wrong.");

                char state;
                for (int i = 0 ; i <= count ; ++i)
                    {
                        state = ((ccb_i2c_->rpc_i2c.err_data[i]) >> 8 & 0x00ff);
                        if (state != state::ok_)
                            {
                                if (state == state::write_ack_)
                                    throw rpct::exception::Exception("error for i2c command, i2c ackn bit was 1.");
                                else if (state == state::SDA_short_)
                                    throw rpct::exception::Exception("error for i2c command, sda short circuit.");
                                else if (state == state::SCL_short_)
                                    throw rpct::exception::Exception("error for i2c command, scl short circuit.");
                                else
                                    throw rpct::exception::Exception("error for i2c command, unknown error in err_data.");
                            }
                    }

                if (i2c_response)
                    i2c_response->i2c_ready();
            }
    } catch (rpct::exception::Exception & e) {
        error_ = 1;
        throw e;
    }
}

DTI2c::DTI2c(std::string const & ccb_server, short ccb_port)
    : ccb_server_(ccb_server)
    , ccb_port_(ccb_port)
    , ccb_command_(0)
    , ccb_i2c_(0)
    , group_mode_(0)
    , error_(0)
{
    std::string::size_type ssize = ccb_server_.size();
    c_ccb_server_ = new char[ssize + 1];
    ccb_server_.copy(c_ccb_server_, ssize);
    c_ccb_server_[ssize] = '\0';
}

DTI2c::~DTI2c()
{
    destroy_ccb_objects();
    delete[] c_ccb_server_;
}

void DTI2c::reset()
{
    destroy_ccb_objects();
    group_mode_ = 0;
    error_ = 0;
}

} // namespace dti2c
} // namespace devices
} // namespace rpct
