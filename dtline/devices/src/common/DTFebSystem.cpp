#include "rpct/devices/DTFebSystem.h"

#include "rpct/devices/IFebAccessPoint.h"
#include "rpct/devices/DTFebAccessPoint.h"
#include "rpct/devices/dti2c/DTI2c.h"

namespace rpct {

log4cplus::Logger DTFebSystem::logger_ = log4cplus::Logger::getInstance("DTFebSystem");
HardwareItemType * DTFebSystem::type_ = 0;

DTFebSystem::DTFebSystem()
    : FebSystem()
{
    if (!type_)
        type_ = new HardwareItemType(HardwareItemType::cBoard
                                     , type::feb::dtfebsystem_);
}

DTFebSystem::~DTFebSystem()
{
    reset();
}

void DTFebSystem::reset()
{
    FebSystem::reset();
    std::map<std::string, rpct::devices::dti2c::DTI2c *>::iterator itEnd = dti2cs_.end();
    for (std::map<std::string, rpct::devices::dti2c::DTI2c *>::iterator it = dti2cs_.begin()
             ; it != itEnd
             ; ++it)
        delete it->second;
}

IFebAccessPoint & DTFebSystem::addDTFebAccessPoint(int id
                                                   , std::string const & ccbserver, short port
                                                   , bool disabled)
    throw (rpct::exception::SystemException)
{
    faps_type::iterator it = faps_.find(id);
    if (it == faps_.end())
        {
            return *(faps_.insert(std::pair<int, IFebAccessPoint *>
                                  (id, new DTFebAccessPoint(*this, id, ccbserver, port, disabled))
                                  ).first->second);
        }
    else
        return *(it->second);
}

rpct::i2c::II2cAccess & DTFebSystem::getDTI2c(std::string const & ccb_server)
{
    std::map<std::string, rpct::devices::dti2c::DTI2c *>::iterator it = dti2cs_.find(ccb_server);
    if (it == dti2cs_.end())
        {
            return *(dti2cs_.insert(std::pair<std::string, rpct::devices::dti2c::DTI2c *>
                                    (ccb_server, new rpct::devices::dti2c::DTI2c(ccb_server))
                                    ).first->second);
        }
    return *(it->second);
}

} // namespace rpct
