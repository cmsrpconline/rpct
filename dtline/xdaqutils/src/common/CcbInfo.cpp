#include "rpct/xdaqutils/CcbInfo.h"

#include "xdata/AbstractBag.h"

namespace rpct {
namespace xdaqutils {

CcbInfo::CcbInfo(int _id
                 , int _wheel, int _sector, int _station
                 , std::string const & _server
                 , int _port)
    : id_(_id)
    , wheel_(_wheel), sector_(_sector), station_(_station)
    , server_(_server)
    , port_(_port)
{}

void CcbInfo::registerFields(xdata::AbstractBag * _bag)
{
    _bag->addField("id", &id_);

    _bag->addField("wheel", &wheel_);
    _bag->addField("sector", &sector_);
    _bag->addField("station", &station_);

    _bag->addField("server", &server_);
    _bag->addField("port", &port_);
}

} // namespace xdaqutils
} // namespace rpct
