#include "rpct/xdaqutils/xdaqDTFebSystem.h"

namespace rpct {
namespace xdaqutils {

xdaqDTFebSystem::xdaqDTFebSystem(xdaq::Application & _application
                                 , toolbox::BSem & _hwmutex
                                 , const std::string & _service_name
                                 , unsigned int _service_instance
                                 , size_t _max_size)
    : rpct::DTFebSystem()
    , xdaqFebSystem(_application, _hwmutex, _service_name, _service_instance, _max_size)
{}

} // namespace xdaqutils
} // namespace rpct
