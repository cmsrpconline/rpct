#include "rpct/xdaqutils/xdaqDTFebSystemBuilder.h"

#include <fstream>
#include <sstream>
#include <iomanip>

#include "xdata/Integer.h"

#include "rpct/xdaqutils/XdaqDbServiceClient.h"

#include "rpct/xdaqutils/DTLineFebInfo.h"
#include "rpct/devices/DTFebSystem.h"
#include "rpct/devices/DTFebAccessPoint.h"

#include "rpct/tools/RollId.h"

namespace rpct {
namespace xdaqutils {

xdaqDTFebSystemBuilder::xdaqDTFebSystemBuilder(xdaq::Application & _application)
    : application_(_application)
    , soap_access_(&application_)
{}

void xdaqDTFebSystemBuilder::getDTLineFebInfo(int _wheel
                                              , xdata::Vector<xdata::Bag<rpct::xdaqutils::DTLineFebInfo> > & _febs)
{
    XdaqSoapAccess::TSOAPParamList _param_list;
    xdata::Integer _xwheel = _wheel;
    _param_list.push_back(XdaqSoapAccess::TSOAPParamList::value_type("wheel", &_xwheel));

    LOG4CPLUS_DEBUG(application_.getApplicationLogger()
                    , "sending request to DB Service: getDTLineFebInfo, wheel = " << _xwheel.value_);

    xoap::MessageReference _response
        = soap_access_.sendSOAPRequest("getDTLineFebInfo"
                                       , XdaqDbServiceClient::RPCT_DB_SERVICE_PREFIX
                                       , XdaqDbServiceClient::RPCT_DB_SERVICE_NS
                                       , _param_list, "RpctDbService", 0, true);

    LOG4CPLUS_DEBUG(application_.getApplicationLogger()
                    , "received response from DB Service");

    soap_access_.parseSOAPResponse(_response
                                   , "getDTLineFebInfo"
                                   , XdaqDbServiceClient::RPCT_DB_SERVICE_PREFIX
                                   , XdaqDbServiceClient::RPCT_DB_SERVICE_NS
                                   , _febs);
}

void xdaqDTFebSystemBuilder::getCCBInfoFromFile(int _wheel
                                                , xdata::Vector<xdata::Bag<rpct::xdaqutils::CcbInfo> > & _ccbs)
{
    std::ifstream _ccbs_file("/nfshome0/rpcpro/rpct/dtline/CCBserversDT.prn");
    if (_ccbs_file.is_open()) {
        int _id
            , _wheel_in, _sector, _station
            , _port;
        std::string _line, _server, _empty;
        std::getline(_ccbs_file, _line); // header
        while(std::getline(_ccbs_file, _line)) {
            std::stringstream _sline;
            _sline.str(_line);
            _sline >> std::skipws
                   >> _id
                   >> _wheel_in >> _sector >> _station
                   >> _empty >> _empty >> _empty // chamber, minicrate, online
                   >> _port >> _server;
            if (_wheel_in == _wheel) {
                _ccbs.push_back(xdata::Bag<rpct::xdaqutils::CcbInfo>());
                _ccbs.back().bag = rpct::xdaqutils::CcbInfo(_id, _wheel, _sector, _station, _server, _port);
            }
        }
    }
    _ccbs_file.close();
}

void xdaqDTFebSystemBuilder::buildDTFebSystem(rpct::DTFebSystem & _febsystem
                                              , xdata::Vector<xdata::Bag<rpct::xdaqutils::CcbInfo> > const & _ccbs
                                              , xdata::Vector<xdata::Bag<rpct::xdaqutils::DTLineFebInfo> > const & _febs)
{
    try {
        _febsystem.reset();

        std::map<rpct::tools::RollId, rpct::DTFebAccessPoint *> _chamber_ccb;
        xdata::Vector<xdata::Bag<rpct::xdaqutils::CcbInfo> >::const_iterator _ccb_bag_end = _ccbs.end();
        for (xdata::Vector<xdata::Bag<rpct::xdaqutils::CcbInfo> >::const_iterator _ccb_bag = _ccbs.begin()
                 ; _ccb_bag != _ccb_bag_end ; ++_ccb_bag) {
            rpct::xdaqutils::CcbInfo const & _ccb = _ccb_bag->bag;
            rpct::IFebAccessPoint & _dt_fap
                = _febsystem.addDTFebAccessPoint(_ccb.getId(), _ccb.getServer(), _ccb.getPort());

            int _sector(_ccb.getSector());
            int _subsector(1); // should be wildcard, but easier like this
            // Check this logic...
            if (_sector == 13) {
                _sector = 4;
                _subsector = 2;
            }
            else if (_sector == 14) {
                _sector = 10;
                _subsector = 2;
            }
            rpct::tools::RollId _roll_id(0
                                         , _ccb.getStation()
                                         , rpct::tools::RollId::wildcard_
                                         , _ccb.getWheel()
                                         , _sector, _subsector);
            _chamber_ccb[_roll_id] = dynamic_cast<rpct::DTFebAccessPoint *>(&_dt_fap);
        }

        xdata::Vector<xdata::Bag<rpct::xdaqutils::DTLineFebInfo> >::const_iterator _dfinfo_bag_end(_febs.end());
        for (xdata::Vector<xdata::Bag<rpct::xdaqutils::DTLineFebInfo> >::const_iterator _dfinfo_bag = _febs.begin()
                 ; _dfinfo_bag != _dfinfo_bag_end ; ++_dfinfo_bag)
            if (!(_dfinfo_bag->bag.getDisabled())) {
                rpct::xdaqutils::DTLineFebInfo const & _dfinfo(_dfinfo_bag->bag);

                rpct::tools::RollId _roll_id; // from _dfinfo.getName()
                _roll_id.parseDBName(_dfinfo.getLocation(), _dfinfo.getPartition());
                rpct::tools::RollId _chamber_id(_roll_id);
                _chamber_id.setGap();
                _chamber_id.setRoll();
                _chamber_id.setSubSubSector();
                _chamber_id.setLayer();
                int _sector(_chamber_id.sector());
                if ((_sector != 4 && _sector != 10) || (_chamber_id.station() < 4))
                    _chamber_id.setSubSector(1);

                std::map<rpct::tools::RollId, rpct::DTFebAccessPoint *>::const_iterator _ccb_it(_chamber_ccb.find(_chamber_id));
                if (_ccb_it == _chamber_ccb.end())
                    throw rpct::exception::SystemException("Could not find the corresponding CCB for a FEB");

                rpct::DTFebAccessPoint & _ccb(*(_ccb_it->second));

                try {
                    unsigned char _pca9544_address_offset(0x07);
                    if (_roll_id.layer() == 2) // out
                        _pca9544_address_offset = 0x06;

                    unsigned char _dt_channel(1); // bwd
                    if (_roll_id.station() < 3 || (_roll_id.station() == 4 && _roll_id.sector() == 10)) {
                        int _roll(_roll_id.roll());
                        if (_roll == rpct::tools::RollId::wildcard_)
                            throw rpct::exception::SystemException("The roll for the FEB was not set.");
                        bool _reference(_roll_id.station() == 2);
                        if (_reference) {
                            if (std::abs(_roll_id.ring()) < 2)
                                _reference = (_roll_id.layer() == 1);
                            else
                                _reference = (_roll_id.layer() == 2);
                        }

                        if (_reference)
                            _dt_channel = 3 - _roll;
                        else if (_roll == 3) // fwd
                            _dt_channel = 0;
                    } else {
                        int _subsector(_roll_id.subsector());
                        if (_roll_id.station() == 4 && (_roll_id.sector() == 9 || _roll_id.sector() == 11))
                            _dt_channel = 0;
                        else if (_roll_id.sector() == 4 && _roll_id.station() == 4) {
                            if (_subsector + _roll_id.subsubsector() == 2)
                                _dt_channel = (_roll_id.sector() + std::abs(_roll_id.wheel()) + 1 + 1) % 2;
                            else
                                _dt_channel = (_roll_id.sector() + std::abs(_roll_id.wheel()) + 2 + 1) % 2;
                        }
                        else
                            _dt_channel = (_roll_id.sector() + std::abs(_roll_id.wheel()) + _subsector + 1) %2;
                    }

                    FebBoard & _febboard =
                        _ccb.addFebDistributionBoard(_dfinfo.getId()
                                                     , _dfinfo.getName()
                                                     , _ccb.getPosition()
                                                     , true, _pca9544_address_offset, _dt_channel
                                                     , 0).
                        addFebBoard(_dfinfo.getId()
                                    , _dfinfo.getName()
                                    , (_dfinfo.getFebChipInfos().size()>2?1:0)
                                    , _dfinfo.getI2cAddress()
                                    , _dfinfo.getDisabled());
                    bool _disable_febboard(true);
                    xdata::Vector<xdata::Bag<rpct::xdaqutils::DTLineFebChipInfo> > const & _chips = _dfinfo.getFebChipInfos();
                    xdata::Vector<xdata::Bag<rpct::xdaqutils::DTLineFebChipInfo> >::const_iterator _chipinfo_end = _chips.end();
                    for (xdata::Vector<xdata::Bag<rpct::xdaqutils::DTLineFebChipInfo> >::const_iterator _chipinfo = _chips.begin()
                             ; _chipinfo != _chipinfo_end ; ++_chipinfo) {
                        _febboard.addFebChip(_chipinfo->bag.getId(), _chipinfo->bag.getPosition()
                                             , _chipinfo->bag.getDisabled());
                        if (! _chipinfo->bag.getDisabled())
                            _disable_febboard = false;
                    }
                    // avoid PCF8574a monitoring if all chips are disabled
                    if (_disable_febboard)
                        _febboard.disable(true);
                } catch(TException & _e) {
                    LOG4CPLUS_ERROR(application_.getApplicationLogger(), "Error building FEB System: " << _e.what());
                }
            }
            else {
                LOG4CPLUS_INFO(application_.getApplicationLogger(), "Not building disabled FebBoard " << _dfinfo_bag->bag.getName());
            }
    } catch (xoap::exception::Exception & _e) {
        LOG4CPLUS_ERROR(application_.getApplicationLogger(), "Error building FEB System: " << _e.what());
    }
}

} // namespace xdaqutils
} // namespace rpct
