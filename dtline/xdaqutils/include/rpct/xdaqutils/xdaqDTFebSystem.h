/** Filip Thyssen */

#ifndef rpct_xdaqutils_xdaqDTFebSystem_h_
#define rpct_xdaqutils_xdaqDTFebSystem_h_

#include "rpct/devices/DTFebSystem.h"
#include "rpct/xdaqutils/xdaqFebSystem.h"
#include "rpct/xdaqutils/const.h"

namespace rpct {
namespace xdaqutils {

class xdaqDTFebSystem
    : public rpct::DTFebSystem
    , public xdaqFebSystem
{
public:
    xdaqDTFebSystem(xdaq::Application & _application
                    , toolbox::BSem & _hwmutex
                    , const std::string & _service_name = std::string(febpublisher::service_name_)
                    , unsigned int _service_instance = febpublisher::service_instance_
                    , size_t _max_size = rpct::xdaqtools::publisher::preset::client_itemlist_size_);
};

} // namespace xdaqutils
} // namespace rpct

#endif // rpct_xdaqutils_xdaqDTFebSystem_h_
