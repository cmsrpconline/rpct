/** Filip Thyssen */

#ifndef rpct_xdaqutils_CcbInfo_inl_h_
#define rpct_xdaqutils_CcbInfo_inl_h_

namespace rpct {
namespace xdaqutils {

inline int CcbInfo::getId() const
{
    return id_.value_;
}

inline int CcbInfo::getWheel() const
{
    return wheel_.value_;
}

inline int CcbInfo::getSector() const
{
    return sector_.value_;
}

inline int CcbInfo::getStation() const
{
    return station_.value_;
}

inline std::string const & CcbInfo::getServer() const
{
    return server_.value_;
}

inline int CcbInfo::getPort() const
{
    return port_.value_;
}

inline void CcbInfo::setId(int _id)
{
    id_ = _id;
}

inline void CcbInfo::setWheel(int _wheel)
{
    wheel_ = _wheel;
}

inline void CcbInfo::setSector(int _sector)
{
    sector_ = _sector;
}

inline void CcbInfo::setStation(int _station)
{
    station_ = _station;
}

inline void CcbInfo::setServer(std::string const & _server)
{
    server_.value_ = _server;
}

inline void CcbInfo::setPort(int _port)
{
    port_ = _port;
}

} // namespace xdaqutils
} // namespace rpct

#endif // rpct_xdaqutils_CcbInfo_inl_h_
