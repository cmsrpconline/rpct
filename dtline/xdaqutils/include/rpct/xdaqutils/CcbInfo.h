/** Filip Thyssen */

#ifndef rpct_xdaqutils_CcbInfo_h_
#define rpct_xdaqutils_CcbInfo_h_

#include <string>

#include "xdata/Integer.h"
#include "xdata/String.h"

namespace xdata {
class AbstractBag;
}

namespace rpct {
namespace xdaqutils {

class CcbInfo
{
public:
    CcbInfo(int _id = -1
            , int _wheel = 0, int _sector = 0, int _station = 0
            , std::string const & _server = ""
            , int _port = -1);

    void registerFields(xdata::AbstractBag * _bag);

    int getId() const;
    int getWheel() const;
    int getSector() const;
    int getStation() const;
    std::string const & getServer() const;
    int getPort() const;

    void setId(int _id);
    void setWheel(int _wheel);
    void setSector(int _sector);
    void setStation(int _station);
    void setServer(std::string const & _server);
    void setPort(int _port);

protected:
    xdata::Integer id_;
    xdata::Integer wheel_, sector_, station_;
    xdata::String  server_;
    xdata::Integer port_;
};

} // namespace xdaqutils
} // namespace rpct

#include "rpct/xdaqutils/CcbInfo-inl.h"

#endif // rpct_xdaqutils_CcbInfo_h_
