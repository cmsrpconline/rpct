/** Filip Thyssen */

#ifndef rpct_xdaqutils_xdaqDTFebSystemBuilder_h_
#define rpct_xdaqutils_xdaqDTFebSystemBuilder_h_

#include "rpct/devices/DTFebSystem.h"
#include "rpct/xdaqutils/XdaqSoapAccess.h"

#include "xdata/Vector.h"
#include "xdata/Bag.h"

#include "rpct/xdaqutils/DTLineFebInfo.h"
#include "rpct/xdaqutils/CcbInfo.h"

namespace rpct {
namespace xdaqutils {

class xdaqDTFebSystemBuilder
{
public:
    void getDTLineFebInfo(int _wheel
                          , xdata::Vector<xdata::Bag<rpct::xdaqutils::DTLineFebInfo> > & _febs);
    void getCCBInfoFromFile(int _wheel
                            , xdata::Vector<xdata::Bag<rpct::xdaqutils::CcbInfo> > & _ccbs);

public:
    xdaqDTFebSystemBuilder(xdaq::Application & _application);

    void buildDTFebSystem(rpct::DTFebSystem & _febsystem
                          , xdata::Vector<xdata::Bag<rpct::xdaqutils::CcbInfo> > const & _ccbs
                          , xdata::Vector<xdata::Bag<rpct::xdaqutils::DTLineFebInfo> > const & _febs);
protected:
    xdaq::Application & application_;
    XdaqSoapAccess soap_access_;
};

} // namespace xdaqutils
} // namespace rpct

#endif // rpct_xdaqutils_xdaqDTFebSystemBuilder_h_
