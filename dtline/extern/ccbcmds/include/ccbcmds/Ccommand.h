// Include Ccommand.h only once
#ifndef __CCOMMAND_H__
#define __CCOMMAND_H__

#include <ccbcmds/Cdef.h>

#ifdef __USE_PTYPES__ // ptypes enabled
  #include <ptypes/pinet.h> // tcp support
  #include <ptypes/pasync.h> // for mutex
#endif

#include <ccbcmds/Clog.h> // for logging
#include <ccbcmds/Cccbheader.h> // command header format

#ifdef __USE_CCBDB__ // DB access enabled
  #include <ccbdb/Ccmsdtdb.h>
#endif

// some string dimensions
#define WRITE_DIM          256
#define READ_DIM           256

// some defaults
#define WRITE_LOG_DEFAULT  false
#define SERVER_DEFAULT     "127.0.0.1"
#define PORT_DEFAULT       18889

// some codes
#define CCB_BUSY_CODE      0x3F
#define CCB_UNKNOWN1       0xFC
#define CCB_UNKNOWN2       0x00
#define CCB_OXFC           0xFC

// enable/disable mutex
#define ENABLE_CMD_MUTEX true

// wait between two commands (ms)
//#define WAIT_AFTER_CMD 100
#define WAIT_AFTER_CMD 0


/*****************************************************************************/
// Ccommand class definition
/*****************************************************************************/

class Ccommand : public Cccbheader {
 public:
// Constructors/Destructor
  Ccommand(); // empty costructor
  Ccommand(short, Clog*); // Input: ccb_id, Clog_object
  Ccommand(short, char*, int, Clog*); // Input: ccb_id, server_name, server_port, Clog_object
  Ccommand(short, short, char*, int, Clog*); // Input: ccb_id, ccb_port, server_name, server_port, Clog_object
  Ccommand(short, short, short, char*, int, Clog*); // Input: ccb_id, ccb_port, ccb_secport, server_name, server_port, Clog_object
  Ccommand(short, short, short, char*, int, char*, int, Clog*); // Input: ccb_id, ccb_port, ccb_secport, servername&port , secserveriname&port, Clog_object

  ~Ccommand();

#ifdef __USE_PTYPES__ // ptypes enabled (thus mutexes)
  pt::mutex cmd_mutex; // mutex object: avoid conflicts in using Ccommand objct
#endif

// Lock/unlock the mutex
  void cmdlock();
  void cmdunlock();

  unsigned char data_read[READ_DIM]; // CCB reply to the command
  int data_read_len; // its dimension

// Display the set-up
// short read_ccb_id() and read_timeout() are inherithed from Cccbheader
  char* read_server_name(); // read the server name
  int read_server_port(); // read the server_port

// Set-up
// set_timeout(int) is inherithed from Cccbheader
  void set_server(char*,int); // set the server name and port
  void set_write_log(bool in); // Write to log (in=true) or not
#ifdef __USE_CCBDB__ // DB access enabled
  void set_db_connector(cmsdtdb *thisdtdb); // Set the DB connector
#endif

// Connect and disconnect to/from the server
  bool connect(); // return true if connected
  void disconnect();

// Create the command line (header+command). Input: Command_string, lenght, create_header
  void create_command_line(unsigned char*, int);

// Send command to the server, wait for reply
// Input: command_line, command_line_len, timeout. Return 0 if ok, <0 if communication error
  int send_command_single(unsigned char*, int, int); // Use create_header=true
  int send_command(unsigned char*, int, int); // Use create_header=true
  int send_command(unsigned char*, int, int,int nrtry); // Like previuos method, but makes up to <nrtry> attempts

// CCB reply to command:
// No data: -5
// Errors from ccbserver: -103:-100
// Connection error (to ccbserver): -4, Timeout (ccbserver reply): -3,
// Ccb_busy:-1, No error: 0,
// Unknown_ccb_command: +1, Wrong_ccb_reply: +2
  int ccb_reply;

// Return errcounter value
  int get_errcounter();

// Return a string with the error message returned by send_command()
  char* read_error_message(int code);

  Clog *ccb_log; // logger
#ifdef __USE_CCBDB__ // DB access enabled
  cmsdtdb *dtdbobj;
#endif

 private:
//  ccb_id, ccb_port, ccb_secport and timeout are inherithed from Cccbheader
#ifdef __USE_PTYPES__ // ptypes enabled (thus TCP/IP)
  pt::ipstream *client;
  pt::ipstream *priclient;
  pt::ipstream *secclient;
  std::string server_name; // server name
  std::string secserver_name; // server name
  int server_port; // server port
  int secserver_port; // secserver port
#endif
  bool command_ready; // commad line is ready (or not)

  unsigned char data_write[WRITE_DIM]; // data to be sent to CCB
  int data_write_len; // its dimension

  bool write_log; // write command to log (or not)
  int errcounter; // Counts communication errors (reset after each success)
};

#endif // __CCOMMAND_H__
