// Include Cconf.h only once
#ifndef __CCONF_H__
#define __CCONF_H__

#include <fstream>
#include <string>

#include <ccbcmds/Cdef.h>       // Some definitions
#include <ccbcmds/Cccbheader.h> // command header format

#ifdef __USE_CCBDB__ // DB access enabled
  #include <ccbdb/Ccmsdtdb.h>
#endif

// some dimensions
//#define CMD_LINE_DIM       256
#define CMD_LINE_DIM       512 // For compatibility with Cccbconfdb class
#define CMD_MAXLINES      1500

#define MASK_XML_ROOT "conf_mask"


/*****************************************************************************/
// Cconf class definition
/*****************************************************************************/

class Cconf : public Cccbheader{
 public:
// Constructor
  Cconf(char *filename); // Input: filename
#ifdef __USE_CCBDB__ // DB access enabled
  Cconf(char *confname,cmsdtdb *thisdtdb); // Input: confname, DB_connector
#endif

/* Switch on/off debug printouts */
  void verbose_mode(bool in); // Go to verbose mode (in=true) or not

// Get config_name or config_file_name
  char* get_conf_name();

// ccbid is needed to read configurations from DB (unused in files)
// Read configuration. Input: ccbid,cmd_code, brd, chip. Output: cmd_line[], cmd_len. Return the number of cmd_lines
  int read_conf(short,unsigned char,char,char,unsigned char[][CMD_LINE_DIM],int[]);

// Write the current configuration to a file. Input: ccbid,filename
  int write_conf_to_file(short,char *);

// Reset/Read/Print components to be masked in configuration
  void reset_confmask();
  int read_confmask(char *filename); // Read mask from file
  int read_confmask(short ccbID); // Read mask from DB
  int read_confmask(std::string strin); // Read mask from string
  void print_confmask();

// Associate RunNr to configuration (ie set configsets.Run)
  int set_run(int runnr);

// Components to be masked in configuration
  bool BTImask[8][32]; // BTI mask [board] [chip]
  bool TRACOmask[6][4]; // TRACO mask [board] [chip]
  bool TSSmask[6]; // TSS mask [board]
  bool TSMmask; // TSM mask
  bool TDCmask[7][4]; // TDC mask [board] [chip]
  bool FEmask[3][24]; // FE mask [SL] [chip]

 private:
  file_or_db ConfigurationType; // Configuration type: from file or DB

  std::string fname; // filename
  std::string cname; // configuration name

  bool _verbose_;

#ifdef __USE_CCBDB__ // DB access enabled
  cmsdtdb *dtdbobj;
#endif
};

#endif // __CCONF_H__
