// include Ci2c.h only once
#ifndef __CI2C_H__
#define __CI2C_H__

// defines classes Ccmn_cmd
#include <ccbcmds/Ccmn_cmd.h>

// reference values class
#include <ccbcmds/Cref.h>

#include <ccbcmds/apfunctions.h>

#define I2C_DATA_MAX 100
#define I2C_DATA_MIN   1

// reference voltage for PADCs
#define VREF 4.5

// (MAX) Number of pressure values used in averaging procedure
#define PNMAX 100

// Timeouts
#define RPC_I2C_TIMEOUT 1500
#define LED_I2C_TIMEOUT 1500
#define READ_ADC_TIMEOUT 1500

/*****************************************************************************/
// I2C data structures
/*****************************************************************************/

// Format of return data from "RPC I2C commands" (0x64),
// also "Led allignements I2C commands" (0x66)
struct Si2c_1 {
  unsigned char code;
  char size;
  short err_data[I2C_DATA_MAX];
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "Read ADC" (0x69)
struct Sread_ADC {
  unsigned char code;
  short adc[32];
  int ccbserver_code;
  std::string msg;
};

// Status PADC results
struct Sstatus_PADC {
  bool error, warning; // error and warning flags
  std::string stmsg; // PADC status
  std::string errmsg; // error/warning messages
  std::string xmlmsg;

  flarray p100hvA, p100hvB, p500hv; // All available HV readings
  flarray p100feA, p100feB, p500fe; // All available FE readings
};

/*****************************************************************************/
// I2C class
/*****************************************************************************/

class Ci2c : public Ccmn_cmd {
 public:
// Class constructor; arguments: pointer_to_command_object
  Ci2c(Ccommand* ppp);

/* Override ROB/TDC methods */
  void read_TDC(char,char);
  int read_TDC_decode(unsigned char*);
  void read_TDC_print();

  void status_TDC(char,char);
  void status_TDC_print();

  void read_ROB_error();
  int read_ROB_error_decode(unsigned char*);
  void read_ROB_error_print();

/* HIGH LEVEL COMMANDS */

// Test LED I2C. Input: Nome. Output: Test_error
  void test_LED_I2C(bool*);
// Test RPC I2C. Input: Nome. Output: Test_error
  void test_RPC_I2C(bool*);

// Test LEDs. Input: None. Output: Test_error
  void test_LED(bool*);

// Read PADCs. Input: None. Output: Test_error, adc_count[10], volt_read[10]
  void read_PADC(bool*, short[10], float[10]);
// Use PADC LUTs to convert digits to pressure/voltage. Input: (Filename,) adc_count[10]. Output: calib_data[10]
  int use_PADC_LUT(char*,short[10],float[10]); // Read file.
  int use_PADC_LUT(short[10],float[10]); // Use DB

// Check PADC status.
  Sstatus_PADC status_padc;
  void status_PADC(Cref*,float*); // Input: Cref_obj, calib_data[10]
  void status_PADC_reset(); // Reset status_padc struct (except averages)
  void status_PADC_reset_average(); // Reset status_padc struct (only averages)
  void status_PADC_print();

// Save PADC status to DB
  int PADC_status_to_db(Cref*,short[10]); // Input: Cref_obj, adc_count[10]

/* LOW LEVEL COMMANDS */


// "RPC I2C commands" (0x64) command
  Si2c_1 rpc_i2c; // Contains command results
  void rpc_I2C(short*, char); // Send command and decode reply. Input: ctrl_data[100], size
  void rpc_I2C_reset(); // Reset rpc_i2c struct
  int rpc_I2C_decode(unsigned char*); // Decode reply. Input: data_read
  void rpc_I2C_print(); // Print reply to standard output

// "Led allignement I2C commands" (0x66) command
  Si2c_1 led_i2c; // Contains command results
  void led_I2C(short*, char); // Send command and decode reply. Input: ctrl_data[100], size
  void led_I2C_reset(); // Reset led_i2c struct
  int led_I2C_decode(unsigned char*); // Decode reply. Input: data_read
  void led_I2C_print(); // Print reply to standard output

// "Read ADC" (0x69) command
  Sread_ADC read_adc; // Contains command results
  void read_ADC(); // Send command and decode reply.
  void read_ADC_reset(); // Reset read_adc struct
  int read_ADC_decode(unsigned char*); // Decode reply. Input: data_read
  void read_ADC_print(); // Print reply to standard output

};

#endif // __CI2C_H__
