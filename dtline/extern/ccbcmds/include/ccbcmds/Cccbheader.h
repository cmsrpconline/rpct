// Here the header format and some related variables are defined 

// Include Cccbheader.h only once
#ifndef __CCCBHEADER_H__
#define __CCCBHEADER_H__

// Header length
#define SRV_HDR_LEN        12
#define CCB_HDR_LEN        2

// some defaults
#define TIMEOUT_DEFAULT    1500
#define TIMEOUT_LONG       6000
#define CCB_ID_DEFAULT     0x00

class Cccbheader {
 public:
// Constructors/Destructor
  Cccbheader(); // empty costructor
  Cccbheader(short, short, short, int); // Input: ccb_id, ccb_port, ccb_secport, timeout

// Display the set-up
  short read_ccb_id(); // Read the ccb_id
  short read_ccb_port(); // Read the ccb port
  int read_timeout(); // Return the timeout (ms)

// Set-up
  int set_timeout(int); // Set the timeout (ms). Return the timeout
  int use_primary_port(); // Switch to optical link. Return non-0 if alredy using it
  int use_secondary_port(); // Switch to copper link. Return non-0 if alredy using it
  bool which_port(); // Return optical/copper link

// Encode-decode the ccb_server header
  void make_srv_header(int, unsigned char*); // Input: payload length. Return: unsigned char *header
  void make_srv_header(int,int,unsigned char*); // Input: length, timeout. Return: unsigned char *header

  void decode_srv_header(unsigned char*, short*, int*, int*); // Input: header. Output: ccb_port, length, error

// Encode-decode the ccb header
  void make_ccb_header(int,unsigned char*); // Input: length. Return: unsigned char *header
  void decode_ccb_header(unsigned char*, unsigned char*); // Input: header. Output: length
  void decode_ccb_header(unsigned char*, int*); // Input: header. Output: length

// Check if the input string contains the header
  bool is_there_srv_header(unsigned char*); // Input: string_to_be_checked

  bool use_secondary; // must be "true" to use the copper link
 protected:
  short ccb_id, ccb_port; // CCB id, CCB port
  short ccb_secport; // CCB secondary port
  int timeout; // timeout for CCB communication
};


#endif // __CCCBHEADER_H__
