#include <ccbcmds/Cref.h>
#include <ccbcmds/apfunctions.h>

#ifdef __USE_CCBDB__ // DB access enabled
  #include <ccbdb/Cccbrefdb.h> // support for cmsdtdb DB
#endif

#include <iostream>
#include <cstdio>

// class constructor
Cref::Cref(char *file1, char *file2, char *file3, char *file4, char *file5) {

// filenames
  BootName=file1;
  StatusName=file2;
  TestName=file3;
  RobName=file4;
  PadcName=file5;

// Origin of reference: file
  reffrom = UFILE;

  for (int ii=0;ii<kMaxErrs;ii++)
     ref_status.errmask[ii]=true;

  load_boot_ref(); // load boot reference values
  load_status_ref(); // load mc_status reference values
  load_test_ref(); // load mc_test reference values
  load_rob_ref(); // load ROB reference values
  load_padc_ref(); // load PADC reference values

}

#ifdef __USE_CCBDB__ // DB access enabled
Cref::Cref(short the_id,cmsdtdb *thisdtdb) {

// set ccb_id
  ccb_id = the_id;

// Set the DB connector
  dtdbobj = thisdtdb;

// Origin of reference: DB
  reffrom = UDATABASE;

  for (int ii=0;ii<kMaxErrs;ii++)
     ref_status.errmask[ii]=true;

  load_status_ref(); // load mc_status reference values
  load_test_ref(); // load mc_test reference values
  load_boot_ref(); // load boot reference values
  load_rob_ref(); // load ROB reference values
  load_padc_ref(); // load PADC reference values
}
#endif

// Load boot reference values from file <BootName> or DB,
// put them in <ref_boot>
// Author: A.Parenti, Apr28 2005
// Modified/Tested: AP, Oct22 2007
int Cref::load_boot_ref() {
  char c;
  unsigned short flag;
  std::string bootstr="";

  if (this==NULL) return -1; // Object deleted -> return

  if (reffrom==UFILE) { // Read from file
    bootstr = read_file(BootName);
  }
  else if (reffrom==UDATABASE) { // Read from DB
#ifdef __USE_CCBDB__ // DB access enabled
    char tmpxml[XML_MAX_DIM], cmnt[LEN2];
    ccbrefdb rCcbref(dtdbobj); // Use an existing connection to DB

    if (rCcbref.retrieveref(ccb_id,boot,tmpxml,cmnt)==0)
      bootstr.insert(0,tmpxml);
#else
    nodbmsg();
#endif
  }

  if (bootstr.length()>0) { // The reference has been read

    if (xml_get_root(bootstr).find(BOOT_XML_ROOT,0)==std::string::npos)
      return -2; // Root tag not found: return

// Read reference values for BOOT status

    read_from_xml(bootstr,"code",&ref_boot.code);

    read_from_xml(bootstr,"Flags",&flag);

    c = flag&0xFF;
    ref_boot.EUartIRQ = takebit(c,0);
    ref_boot.TTCi2c = takebit(c,1);
    ref_boot.CpldTtc = takebit(c,2);
    ref_boot.QpllArdy = takebit(c,3);
    ref_boot.QpllBrdy = takebit(c,4);
    ref_boot.QpllAChng = takebit(c,5);
    ref_boot.QpllBChng = takebit(c,6);
    ref_boot.TtcCk = takebit(c,7);

    c = (flag>>8)&0xFF;
    ref_boot.IUart = takebit(c,0);
    ref_boot.EUartA = takebit(c,1);
    ref_boot.EUartB = takebit(c,2);
    ref_boot.ExtRam = takebit(c,3);
    ref_boot.Flash = takebit(c,4);
// bit5 is autorun, not used for error check
    ref_boot.TTCrxRdy = takebit(c,6);
    ref_boot.LinkAutoset = takebit(c,7);

    read_from_xml(bootstr,"PwrRam",&ref_boot.PwrRam);
    read_from_xml(bootstr,"ErrRam",&ref_boot.ErrRam);
    read_from_xml(bootstr,"vErrRam",&ref_boot.vErrRam);
    read_from_xml(bootstr,"PwrFlash",&ref_boot.PwrFlash);
    read_from_xml(bootstr,"FlashID",&ref_boot.FlashID);

    read_from_xml(bootstr,"LinkOfsMin",&ref_boot.LinkOfsMin);
    read_from_xml(bootstr,"LinkHystMin",&ref_boot.LinkHystMin);
    read_from_xml(bootstr,"LinkAmpMin",&ref_boot.LinkAmpMin);
    read_from_xml(bootstr,"LinkOfsMax",&ref_boot.LinkOfsMax);
    read_from_xml(bootstr,"LinkHystMax",&ref_boot.LinkHystMax);

    read_from_xml(bootstr,"PwrCrashFlags",&ref_boot.PwrCrashFlags);
    read_from_xml(bootstr,"CrashFlags",&ref_boot.CrashFlags); 
    read_from_xml(bootstr,"PwrAnalog",&ref_boot.PwrAnalog);

    read_from_xml(bootstr,"VccinMin",&ref_boot.VccinMin);
    read_from_xml(bootstr,"VddinMin",&ref_boot.VddinMin);
    read_from_xml(bootstr,"VccMin",&ref_boot.VccMin);
    read_from_xml(bootstr,"VddMin",&ref_boot.VddMin);
    read_from_xml(bootstr,"Tp1HMin",&ref_boot.Tp1HMin);
    read_from_xml(bootstr,"Tp1LMin",&ref_boot.Tp1LMin);
    read_from_xml(bootstr,"Tp2HMin",&ref_boot.Tp2HMin);
    read_from_xml(bootstr,"Tp2LMin",&ref_boot.Tp2LMin);

    read_from_xml(bootstr,"VccinMax",&ref_boot.VccinMax);
    read_from_xml(bootstr,"VddinMax",&ref_boot.VddinMax);
    read_from_xml(bootstr,"VccMax",&ref_boot.VccMax);
    read_from_xml(bootstr,"VddMax",&ref_boot.VddMax);
    read_from_xml(bootstr,"Tp1HMax",&ref_boot.Tp1HMax);
    read_from_xml(bootstr,"Tp1LMax",&ref_boot.Tp1LMax);
    read_from_xml(bootstr,"Tp2HMax",&ref_boot.Tp2HMax);
    read_from_xml(bootstr,"Tp2LMax",&ref_boot.Tp2LMax);

    return 0; // No error
  }
  else
    return -1; // Error

}


// Print boot reference values which have been loaded
// Author: A.Parenti, Apr28 2005
// Modified: AP, Oct22 2007
void Cref::print_boot_ref() {
  if (this==NULL) return; // Object deleted -> return

  printf("\n*** Boot Reference Values ***\n");
  if (reffrom==UFILE)
    printf("-> File: %s\n",BootName.c_str());
  else if (reffrom==UDATABASE)
    printf("-> Values retrieved from DB (ccbid=%d)\n",ccb_id);
  else
    return;
  printf("-> Values:\n");

  printf("code: %#x\n",ref_boot.code);

  printf("EUartIRQ: %d\n",ref_boot.EUartIRQ);
  printf("TTCi2c: %d\n",ref_boot.TTCi2c);
  printf("CpldTtc: %d\n",ref_boot.CpldTtc);
  printf("QpllArdy: %d\n",ref_boot.QpllArdy);
  printf("QpllBrdy: %d\n",ref_boot.QpllBrdy);
  printf("QpllAChng: %d\n",ref_boot.QpllAChng);
  printf("QpllBChng: %d\n",ref_boot.QpllBChng);
  printf("TtcCk: %d\n",ref_boot.TtcCk);
  printf("IUart: %d\n",ref_boot.IUart);
  printf("EUartA: %d\n",ref_boot.EUartA);
  printf("EUartB: %d\n",ref_boot.EUartB);
  printf("ExtRam: %d\n",ref_boot.ExtRam);
  printf("Flash: %d\n",ref_boot.Flash);
  printf("TTCrxRdy: %d\n",ref_boot.TTCrxRdy);
  printf("LinkAutoset: %d\n",ref_boot.LinkAutoset);

  printf("PwrRam: %d\n",ref_boot.PwrRam);
  printf("ErrRam: %d\n",ref_boot.ErrRam);
  printf("vErrRam: %d\n",ref_boot.vErrRam);
  printf("PwrFlash: %d\n",ref_boot.PwrFlash);
  printf("FlashID: %#x\n",ref_boot.FlashID);
  printf("LinkOfs: %d <-> %d\n",ref_boot.LinkOfsMin,ref_boot.LinkOfsMax);
  printf("LinkHyst: %d <-> %d\n",ref_boot.LinkHystMin,ref_boot.LinkHystMax);
  printf("LinkAmp: >%d\n",ref_boot.LinkAmpMin);
  printf("PwrCrashFlags: %d\n",ref_boot.PwrCrashFlags);
  printf("CrashFlags: %d\n",ref_boot.CrashFlags);
  printf("PwrAnalog: %d\n",ref_boot.PwrAnalog);

  printf("Vccin: %.2f <-> %.2f (V)\n",ref_boot.VccinMin,ref_boot.VccinMax);
  printf("Vddin: %.2f <-> %.2f (V)\n",ref_boot.VddinMin,ref_boot.VddinMax);
  printf("Vcc: %.2f <-> %.2f (V)\n",ref_boot.VccMin,ref_boot.VccMax);
  printf("Vdd: %.2f <-> %.2f (V)\n",ref_boot.VddMin,ref_boot.VddMax);
  printf("Tp1L: %.2f <-> %.2f (V)\n",ref_boot.Tp1LMin,ref_boot.Tp1LMax);
  printf("Tp1H: %.2f <-> %.2f (V)\n",ref_boot.Tp1HMin,ref_boot.Tp1HMax);
  printf("Tp2L: %.2f <-> %.2f (V)\n",ref_boot.Tp2LMin,ref_boot.Tp2LMax);
  printf("Tp2H: %.2f <-> %.2f (V)\n",ref_boot.Tp2HMin,ref_boot.Tp2HMax);

  printf("*** End Boot Reference Values ***\n");
}


// Load mc_status reference values from file <StatusName>,
// put them in <ref_status>
// Author: A.Parenti, Apr28 2005
// Modified/Tested: AP, Oct22 2007
int Cref::load_status_ref() {
  char c;
  unsigned short flag;
  std::string statstr="";

  if (this==NULL) return -1; // Object deleted -> return

  if (reffrom==UFILE) { // Read from file
    statstr = read_file(StatusName);
  }
  else if (reffrom==UDATABASE) { // Read from DB
#ifdef __USE_CCBDB__ // DB access enabled
    char tmpxml[XML_MAX_DIM], cmnt[LEN2];
    ccbrefdb rCcbref(dtdbobj); // Use an existing connection to DB

    if (rCcbref.retrieveref(ccb_id,status,tmpxml,cmnt)==0)
      statstr.insert(0,tmpxml);
		rCcbref.retrieveErrMask(ccb_id,ref_status.errmask,kMaxErrs);
#else
    nodbmsg();
#endif
  }

  if (statstr.length()>0) { // The file has been read

    if (xml_get_root(statstr).find(STATUS_XML_ROOT,0)==std::string::npos)
      return -2; // Root tag not found: return

// Read reference values for Minicrate status

//    read_from_xml(statstr,"code",&ref_status.code);
    read_from_xml(statstr,"code",&ref_status.code);
    read_from_xml(statstr,"McType",&ref_status.McType);
    read_from_xml(statstr,"flags1",&flag);

    c = flag&0xFF;
    ref_status.PwrAn = takebit(c,0);
    ref_status.PwrCK = takebit(c,1);
    ref_status.PwrLed = takebit(c,2);
    ref_status.PwrRpc = takebit(c,3);
    ref_status.PwrTrbBuf = takebit(c,4);
    ref_status.PwrTrbVcc = takebit(c,5);
    ref_status.PwrSO = takebit(c,6);
    ref_status.PwrDU = takebit(c,7);

    c = (flag>>8)&0xFF;
    ref_status.PwrDD = takebit(c,0);
    ref_status.PwrSBCK = takebit(c,1);
    ref_status.PwrTH = takebit(c,2);
    ref_status.TTCrdy = takebit(c,3);
    ref_status.PwrFlash = takebit(c,4);
    ref_status.EnTtcCkMux = takebit(c,5);
    ref_status.QpllARdy = takebit(c,6);
    ref_status.QpllBRdy = takebit(c,7);

    read_from_xml(statstr,"PwrTrb",&ref_status.PwrTrb);
    read_from_xml(statstr,"PwrRob",&ref_status.PwrRob);

    read_from_xml(statstr,"flags2",&flag);

    c = flag&0xFF;
    ref_status.AlrmPwrAn = takebit(c,0);
    ref_status.AlrmPwrCK = takebit(c,1);
    ref_status.AlrmPwrLed = takebit(c,2);
    ref_status.AlrmPwrRpc = takebit(c,3);
    ref_status.AlrmPwrTrbBuf = takebit(c,4);
    ref_status.AlrmPwrTrbVcc = takebit(c,5);
    ref_status.AlrmPwrSO = takebit(c,6);
    ref_status.AlrmPwrDU = takebit(c,7);

    c = (flag>>8)&0xFF;
    ref_status.AlrmPwrDD = takebit(c,0);
    ref_status.AlrmPwrSBCK = takebit(c,1);
    ref_status.AlrmPwrTH = takebit(c,2);
    ref_status.AlrmTTCrdy = takebit(c,3);
    ref_status.AlrmPwrFlash = takebit(c,4);
    ref_status.AlrmEnTtcCkMux = takebit(c,5);
    ref_status.AlrmQpllAChng = takebit(c,6);
    ref_status.AlrmQpllBChng = takebit(c,7);


    read_from_xml(statstr,"AlrmPwrTrb",&ref_status.AlrmPwrTrb);
    read_from_xml(statstr,"AlrmPwrRob",&ref_status.AlrmPwrRob);
    read_from_xml(statstr,"AlrmTempTrb",&ref_status.AlrmTempTrb);
    read_from_xml(statstr,"AlrmTempRob",&ref_status.AlrmTempRob);
    read_from_xml(statstr,"LoseLockCountTTC",&ref_status.LoseLockCountTTC);
    read_from_xml(statstr,"LoseLockCountQPLL1",&ref_status.LoseLockCountQPLL1);
    read_from_xml(statstr,"LoseLockCountQPLL2",&ref_status.LoseLockCountQPLL2);
    read_from_xml(statstr,"SeuRam",&ref_status.SeuRam);
    read_from_xml(statstr,"SeuIntRam",&ref_status.SeuIntRam);
    read_from_xml(statstr,"SeuBTI",&ref_status.SeuBTI);
    read_from_xml(statstr,"SeuTRACO",&ref_status.SeuTRACO);
    read_from_xml(statstr,"SeuLUT",&ref_status.SeuLUT);
    read_from_xml(statstr,"SeuTSS",&ref_status.SeuTSS);

    read_from_xml(statstr,"VccinMin",&ref_status.VccinMin);
    read_from_xml(statstr,"VddinMin",&ref_status.VddinMin);
    read_from_xml(statstr,"VccMin",&ref_status.VccMin);
    read_from_xml(statstr,"VddMin",&ref_status.VddMin);
    read_from_xml(statstr,"Tp1LMin",&ref_status.Tp1LMin);
    read_from_xml(statstr,"Tp1HMin",&ref_status.Tp1HMin);
    read_from_xml(statstr,"Tp2LMin",&ref_status.Tp2LMin);
    read_from_xml(statstr,"Tp2HMin",&ref_status.Tp2HMin);
    read_from_xml(statstr,"in_Fe_VccMin",&ref_status.Fe_VccMin[0]);
    read_from_xml(statstr,"th_Fe_VccMin",&ref_status.Fe_VccMin[1]);
    read_from_xml(statstr,"out_Fe_VccMin",&ref_status.Fe_VccMin[2]);
    read_from_xml(statstr,"in_Fe_VddMin",&ref_status.Fe_VddMin[0]);
    read_from_xml(statstr,"th_Fe_VddMin",&ref_status.Fe_VddMin[1]);
    read_from_xml(statstr,"out_Fe_VddMin",&ref_status.Fe_VddMin[2]);
    read_from_xml(statstr,"Sp_VccMin",&ref_status.Sp_VccMin);
    read_from_xml(statstr,"Sp_VddMin",&ref_status.Sp_VddMin);

    read_from_xml(statstr,"VccinMax",&ref_status.VccinMax);
    read_from_xml(statstr,"VddinMax",&ref_status.VddinMax);
    read_from_xml(statstr,"VccMax",&ref_status.VccMax);
    read_from_xml(statstr,"VddMax",&ref_status.VddMax);
    read_from_xml(statstr,"Tp1LMax",&ref_status.Tp1LMax);
    read_from_xml(statstr,"Tp1HMax",&ref_status.Tp1HMax);
    read_from_xml(statstr,"Tp2LMax",&ref_status.Tp2LMax);
    read_from_xml(statstr,"Tp2HMax",&ref_status.Tp2HMax);
    read_from_xml(statstr,"in_Fe_VccMax",&ref_status.Fe_VccMax[0]);
    read_from_xml(statstr,"th_Fe_VccMax",&ref_status.Fe_VccMax[1]);
    read_from_xml(statstr,"out_Fe_VccMax",&ref_status.Fe_VccMax[2]);
    read_from_xml(statstr,"in_Fe_VddMax",&ref_status.Fe_VddMax[0]);
    read_from_xml(statstr,"th_Fe_VddMax",&ref_status.Fe_VddMax[1]);
    read_from_xml(statstr,"out_Fe_VddMax",&ref_status.Fe_VddMax[2]);
    read_from_xml(statstr,"Sp_VccMax",&ref_status.Sp_VccMax);
    read_from_xml(statstr,"Sp_VddMax",&ref_status.Sp_VddMax);

    read_from_xml(statstr,"in_TmaxMin",&ref_status.in_TmaxMin);
    read_from_xml(statstr,"in_TmedMin",&ref_status.in_TmedMin);
    read_from_xml(statstr,"th_TmaxMin",&ref_status.th_TmaxMin);
    read_from_xml(statstr,"th_TmedMin",&ref_status.th_TmedMin);
    read_from_xml(statstr,"out_TmaxMin",&ref_status.out_TmaxMin);
    read_from_xml(statstr,"out_TmedMin",&ref_status.out_TmedMin);
    read_from_xml(statstr,"in_TmaxMax",&ref_status.in_TmaxMax);
    read_from_xml(statstr,"in_TmedMax",&ref_status.in_TmedMax);
    read_from_xml(statstr,"th_TmaxMax",&ref_status.th_TmaxMax);
    read_from_xml(statstr,"th_TmedMax",&ref_status.th_TmedMax);
    read_from_xml(statstr,"out_TmaxMax",&ref_status.out_TmaxMax);
    read_from_xml(statstr,"out_TmedMax",&ref_status.out_TmedMax);

    read_from_xml(statstr,"BrdMaxtempMin",&ref_status.BrdMaxtempMin);
    read_from_xml(statstr,"BrdMaxtempMax",&ref_status.BrdMaxtempMax);

    read_from_xml(statstr,"FeOffTMin",&ref_status.FeOffTMin);
    read_from_xml(statstr,"FeOffTMax",&ref_status.FeOffTMax);
    read_from_xml(statstr,"McOffTMin",&ref_status.McOffTMin);
    read_from_xml(statstr,"McOffTMax",&ref_status.McOffTMax);

    return 0; // No error
  }
  else
    return -1; // Error

}


// Print mc_status reference values which have been loaded
// Author: A.Parenti, Apr28 2005
// Modified: AP, Oct22 2007
void Cref::print_status_ref() {
  if (this==NULL) return; // Object deleted -> return

  printf("\n*** Minicrate Status Reference Values ***\n");
  if (reffrom==UFILE)
    printf("-> File: %s\n",StatusName.c_str());
  else if (reffrom==UDATABASE)
    printf("-> Values retrieved from DB (ccbid=%d)\n",ccb_id);
  else
    return;
  printf("-> Values:\n");

  printf("code: %#x\n",ref_status.code);
//  printf("HVersion: %d\n",ref_status.HVersion);
//  printf("LVersion: %d\n",ref_status.LVersion);
  printf("McType: %d\n",ref_status.McType);

  printf("PwrAn: %d\n",ref_status.PwrAn);
  printf("PwrCK: %d\n",ref_status.PwrCK);
  printf("PwrLed: %d\n",ref_status.PwrLed);
  printf("PwrRpc: %d\n",ref_status.PwrRpc);
  printf("PwrTrbBuf: %d\n",ref_status.PwrTrbBuf);
  printf("PwrTrbVcc: %d\n",ref_status.PwrTrbVcc);
  printf("PwrSO: %d\n",ref_status.PwrSO);
  printf("PwrDU: %d\n",ref_status.PwrDU);
  printf("PwrDD: %d\n",ref_status.PwrDD);
  printf("PwrSBCK: %d\n",ref_status.PwrSBCK);
  printf("PwrTH: %d\n",ref_status.PwrTH);
  printf("TTCrdy: %d\n",ref_status.TTCrdy);
  printf("PwrFlash: %d\n",ref_status.PwrFlash);
  printf("EnTtcCkMux: %d\n",ref_status.EnTtcCkMux);
  printf("QpllARdy: %d\n",ref_status.QpllARdy);
  printf("QpllBRdy: %d\n",ref_status.QpllBRdy);

  printf("PwrTrb: %#x\n",ref_status.PwrTrb);
  printf("PwrRob: %#x\n",ref_status.PwrRob);

  printf("AlrmPwrAn: %d\n",ref_status.AlrmPwrAn);
  printf("AlrmPwrCK: %d\n",ref_status.AlrmPwrCK);
  printf("AlrmPwrLed: %d\n",ref_status.AlrmPwrLed);
  printf("AlrmPwrRpc: %d\n",ref_status.AlrmPwrRpc);
  printf("AlrmPwrTrbBuf: %d\n",ref_status.AlrmPwrTrbBuf);
  printf("AlrmPwrTrbVcc: %d\n",ref_status.AlrmPwrTrbVcc);
  printf("AlrmPwrSO: %d\n",ref_status.AlrmPwrSO);
  printf("AlrmPwrDU: %d\n",ref_status.AlrmPwrDU);
  printf("AlrmPwrDD: %d\n",ref_status.AlrmPwrDD);
  printf("AlrmPwrSBCK: %d\n",ref_status.AlrmPwrSBCK);
  printf("AlrmPwrTH: %d\n",ref_status.AlrmPwrTH);
  printf("AlrmTTCrdy: %d\n",ref_status.AlrmTTCrdy);
  printf("AlrmPwrFlash: %d\n",ref_status.AlrmPwrFlash);
  printf("AlrmEnTtcCkMux: %d\n",ref_status.AlrmEnTtcCkMux);
  printf("AlrmQpllAChng: %d\n",ref_status.AlrmQpllAChng);
  printf("AlrmQpllBChng: %d\n",ref_status.AlrmQpllBChng);

  printf("AlrmPwrTrb: %d\n",ref_status.AlrmPwrTrb);
  printf("AlrmPwrRob: %d\n",ref_status.AlrmPwrRob);
  printf("AlrmTempTrb: %d\n",ref_status.AlrmTempTrb);
  printf("AlrmTempRob: %d\n",ref_status.AlrmTempRob);
  printf("LoseLockCountTTC: %d\n",ref_status.LoseLockCountTTC);
  printf("LoseLockCountQPLL1: %d\n",ref_status.LoseLockCountQPLL1);
  printf("LoseLockCountQPLL2: %d\n",ref_status.LoseLockCountQPLL2);
  printf("SeuRam: %d\n",ref_status.SeuRam);
  printf("SeuIntRam: %d\n",ref_status.SeuIntRam);
  printf("SeuBTI: %d\n",ref_status.SeuBTI);
  printf("SeuTRACO: %d\n",ref_status.SeuTRACO);
  printf("SeuLUT: %d\n",ref_status.SeuLUT);
  printf("SeuTSS: %d\n",ref_status.SeuTSS);

  printf("Vccin: %.2f <-> %.2f (V)\n",ref_status.VccinMin,ref_status.VccinMax);
  printf("Vddin: %.2f <-> %.2f (V)\n",ref_status.VddinMin,ref_status.VddinMax);
  printf("Vcc: %.2f <-> %.2f (V)\n",ref_status.VccMin,ref_status.VccMax);
  printf("Vdd: %.2f <-> %.2f (V)\n",ref_status.VddMin,ref_status.VddMax);
  printf("Tp1L: %.2f <-> %.2f (V)\n",ref_status.Tp1LMin,ref_status.Tp1LMax);
  printf("Tp1H: %.2f <-> %.2f (V)\n",ref_status.Tp1HMin,ref_status.Tp1HMax);
  printf("Tp2L: %.2f <-> %.2f (V)\n",ref_status.Tp2LMin,ref_status.Tp2LMax);
  printf("Tp2H: %.2f <-> %.2f (V)\n",ref_status.Tp2HMin,ref_status.Tp2HMax);
  printf("Fe_Vcc[0]: %.2f <-> %.2f (V)\n",ref_status.Fe_VccMin[0],ref_status.Fe_VccMax[0]);
  printf("Fe_Vcc[1]: %.2f <-> %.2f (V)\n",ref_status.Fe_VccMin[1],ref_status.Fe_VccMax[1]);
  printf("Fe_Vcc[2]: %.2f <-> %.2f (V)\n",ref_status.Fe_VccMin[2],ref_status.Fe_VccMax[2]);
  printf("Fe_Vdd[0]: %.2f <-> %.2f (V)\n",ref_status.Fe_VddMin[0],ref_status.Fe_VddMax[0]);
  printf("Fe_Vdd[1]: %.2f <-> %.2f (V)\n",ref_status.Fe_VddMin[1],ref_status.Fe_VddMax[1]);
  printf("Fe_Vdd[2]: %.2f <-> %.2f (V)\n",ref_status.Fe_VddMin[2],ref_status.Fe_VddMax[2]);
  printf("Sp_Vcc: %.2f <-> %.2f (V)\n",ref_status.Sp_VccMin,ref_status.Sp_VccMax);
  printf("Sp_Vdd: %.2f <-> %.2f (V)\n",ref_status.Sp_VddMin,ref_status.Sp_VddMax);

  printf("in_Tmax: %d <-> %d (0.1 C)\n",ref_status.in_TmaxMin,ref_status.in_TmaxMax);
  printf("in_Tmed: %d <-> %d (0.1 C)\n",ref_status.in_TmedMin,ref_status.in_TmedMax);
  printf("th_Tmax: %d <-> %d (0.1 C)\n",ref_status.th_TmaxMin,ref_status.th_TmaxMax);
  printf("th_Tmed: %d <-> %d (0.1 C)\n",ref_status.th_TmedMin,ref_status.th_TmedMax);
  printf("out_Tmax: %d <-> %d (0.1 C)\n",ref_status.out_TmaxMin,ref_status.out_TmaxMax);
  printf("out_Tmed: %d <-> %d (0.1 C)\n",ref_status.out_TmedMin,ref_status.out_TmedMax);

  printf("BrdMaxtemp: %.2f <-> %.2f (C)\n",ref_status.BrdMaxtempMin,ref_status.BrdMaxtempMax);

  printf("FeOffT: %d <-> %d (0.1 C)\n",ref_status.FeOffTMin,ref_status.FeOffTMax);
  printf("McOffT: %.2f <-> %.2f (C)\n",ref_status.McOffTMin,ref_status.McOffTMax);


  printf("*** End Minicrate Status Reference Values ***\n");
}


// Load mc_test reference values from file <TestName>, put them in <ref_test>
// Author: A.Parenti, Apr28 2005
// Modified/Tesetd: AP, Apr05 2007
int Cref::load_test_ref() {
  char c;
  unsigned short flag;
  std::string teststr="";

  if (this==NULL) return -1; // Object deleted -> return

  if (reffrom==UFILE) { // Read from file
    teststr = read_file(TestName);
  }
  else if (reffrom==UDATABASE) { // Read from DB
#ifdef __USE_CCBDB__ // DB access enabled
    char tmpxml[XML_MAX_DIM], cmnt[LEN2];
    ccbrefdb rCcbref(dtdbobj); // Use an existing connection to DB

    if (rCcbref.retrieveref(ccb_id,test,tmpxml,cmnt)==0)
      teststr.insert(0,tmpxml);
#else
    nodbmsg();
#endif
  }

  if (teststr.length()>0) { // The reference has been read


    if (xml_get_root(teststr).find(TEST_XML_ROOT,0)==std::string::npos)
      return -2; // Root tag not found: return

// Read reference values for Minicrate Test

    read_from_xml(teststr,"code",&ref_test.code);
    read_from_xml(teststr,"Flags",&flag);

    c = flag&0xFF;
    ref_test.TTCFpga = takebit(c,0);
    ref_test.AnPwr = takebit(c,1);
    ref_test.CKPwr = takebit(c,2);
    ref_test.LedPwr = takebit(c,3);
    ref_test.RpcPwr = takebit(c,4);
    ref_test.BufTrbPwr = takebit(c,5);
    ref_test.VccTrbPwr = takebit(c,6);
    ref_test.B1w = takebit(c,7);

    c = (flag>>8)&0xFF;
    ref_test.SOPwr = takebit(c,0);
    ref_test.DUPwr = takebit(c,1);
    ref_test.DDPwr = takebit(c,2);
    ref_test.SBCKPwr = takebit(c,3);
    ref_test.THPwr = takebit(c,4);
    ref_test.CPUdelay = takebit(c,5);
    ref_test.TPFineDelay1 = takebit(c,6);
    ref_test.TPFineDelay2 = takebit(c,7);

    read_from_xml(teststr,"OnTimeMin",11,ref_test.OnTimeMin);
    read_from_xml(teststr,"OnTimeMax",11,ref_test.OnTimeMax);
    read_from_xml(teststr,"AdcNoiseMin",32,ref_test.AdcNoiseMin);
    read_from_xml(teststr,"AdcNoiseMax",32,ref_test.AdcNoiseMax);

    read_from_xml(teststr,"Dac",&ref_test.Dac);
    read_from_xml(teststr,"SbTestJtag",&ref_test.SbTestJtag);
    read_from_xml(teststr,"SbTestPi",&ref_test.SbTestPi);

    read_from_xml(teststr,"TrbPwr",&ref_test.TrbPwr);
    read_from_xml(teststr,"TrbBadJtagAddr",&ref_test.TrbBadJtagAddr);

    read_from_xml(teststr,"TrbPresMsk",8,ref_test.TrbPresMsk);
    read_from_xml(teststr,"TrbTestJtag",8,ref_test.TrbTestJtag);
    read_from_xml(teststr,"TrbFindSensor",8,ref_test.TrbFindSensor);
    read_from_xml(teststr,"TrbOnTimeMin",8,ref_test.TrbOnTimeMin);
    read_from_xml(teststr,"TrbOnTimeMax",8,ref_test.TrbOnTimeMax);
    read_from_xml(teststr,"TrbPiTest",6,ref_test.TrbPiTest);

    read_from_xml(teststr,"RobPwr",&ref_test.RobPwr);
    read_from_xml(teststr,"RobBadJtagAddr",&ref_test.RobBadJtagAddr);
    read_from_xml(teststr,"RobOverlapAddr",&ref_test.RobOverlapAddr);

    read_from_xml(teststr,"RobPresMsk",7,ref_test.RobPresMsk);
    read_from_xml(teststr,"RobTestJtag",7,ref_test.RobTestJtag);
    read_from_xml(teststr,"RobFindSensor",7,ref_test.RobFindSensor);
    read_from_xml(teststr,"RobOnTimeMin",7,ref_test.RobOnTimeMin);
    read_from_xml(teststr,"RobOnTimeMax",7,ref_test.RobOnTimeMax);

    read_from_xml(teststr,"McType",&ref_test.McType);

    return 0; // No error
  }
  else
    return -1; // Error

}


// Print mc_status reference values which have been loaded
// Author: A.Parenti, Apr28 2005
// Modified: AP, Sep08 2006
void Cref::print_test_ref() {
  int i;

  if (this==NULL) return; // Object deleted -> return

  printf("\n*** Minicrate Test Reference Values ***\n");
  if (reffrom==UFILE)
    printf("-> File: %s\n",TestName.c_str());
  else if (reffrom==UDATABASE)
    printf("-> Values retrieved from DB (ccbid=%d)\n",ccb_id);
  else
    return;
  printf("-> Values:\n");

  printf("code: %#x\n",ref_test.code);

  printf("TTCFpga: %d\n",ref_test.TTCFpga);
  printf("AnPwr: %d\n",ref_test.AnPwr);
  printf("CKPwr: %d\n",ref_test.CKPwr);
  printf("LedPwr: %d\n",ref_test.LedPwr);
  printf("RpcPwr: %d\n",ref_test.RpcPwr);
  printf("BufTrbPwr: %d\n",ref_test.BufTrbPwr);
  printf("VccTrbPwr: %d\n",ref_test.VccTrbPwr);
  printf("B1w: %d\n",ref_test.B1w);
  printf("SOPwr: %d\n",ref_test.SOPwr);
  printf("DUPwr: %d\n",ref_test.DUPwr);
  printf("DDPwr: %d\n",ref_test.DDPwr);
  printf("SBCKPwr: %d\n",ref_test.SBCKPwr);
  printf("THPwr: %d\n",ref_test.THPwr);
  printf("CPUdelay: %d\n",ref_test.CPUdelay);
  printf("TPFineDelay1: %d\n",ref_test.TPFineDelay1);
  printf("TPFineDelay2: %d\n",ref_test.TPFineDelay2);

  for (i=0;i<11;++i)
    printf("OnTime[%d]: %d <-> %d (ms)\n",i,ref_test.OnTimeMin[i],ref_test.OnTimeMax[i]);
  for (i=0;i<32;++i)
    printf("AdcNoise[%d]: %d <-> %d\n",i,ref_test.AdcNoiseMin[i],ref_test.AdcNoiseMax[i]);

  printf("Dac: %#x\n",ref_test.Dac);
  printf("SbTestJtag: %#x\n",ref_test.SbTestJtag);
  printf("SbTestPi: %#x\n",ref_test.SbTestPi);
  printf("TrbPwr: %#x\n",ref_test.TrbPwr);
  printf("TrbBadJtagAddr: %#x\n",ref_test.TrbBadJtagAddr);

  for (i=0;i<8;++i)
    printf("TrbPresMsk[%d]: %#x\n",i,ref_test.TrbPresMsk[i]);
  for (i=0;i<8;++i)
    printf("TrbTestJtag[%d]: %#x\n",i,ref_test.TrbTestJtag[i]);
  for (i=0;i<8;++i)
    printf("TrbFindSensor[%d]: %d\n",i,ref_test.TrbFindSensor[i]);
  for (i=0;i<8;++i)
    printf("TrbOnTime[%d]: %d <-> %d (ms)\n",i,ref_test.TrbOnTimeMin[i],ref_test.TrbOnTimeMax[i]);
  for (i=0;i<6;++i)
    printf("TrbPiTest[%d]: %#x\n",i,ref_test.TrbPiTest[i]);

  printf("RobPwr: %#x\n",ref_test.RobPwr);
  printf("RobBadJtagAddr: %#x\n",ref_test.RobBadJtagAddr);
  printf("RobOverlapAddr: %#x\n",ref_test.RobOverlapAddr);

  for (i=0;i<7;++i)
    printf("RobPresMsk[%d]: %#x\n",i,ref_test.RobPresMsk[i]);
  for (i=0;i<7;++i)
    printf("RobTestJtag[%d]: %#x\n",i,ref_test.RobTestJtag[i]);
  for (i=0;i<7;++i)
    printf("RobFindSensor[%d]: %d\n",i,ref_test.RobFindSensor[i]);
  for (i=0;i<7;++i)
    printf("RobOnTime[%d]: %d <-> %d (ms)\n",i,ref_test.RobOnTimeMin[i],ref_test.RobOnTimeMax[i]);

  printf("McType: %d\n",ref_test.McType);

  printf("*** End Minicrate Test Reference Values ***\n");
}



// Load ROB reference values from file <RobName> or DB,
// put them in <ref_rob>
// Author: A.Parenti, Jul24 2007
int Cref::load_rob_ref() {
  std::string robstr="";

  if (this==NULL) return -1; // Object deleted -> return

  if (reffrom==UFILE) { // Read from file
    robstr = read_file(RobName);
  }
  else if (reffrom==UDATABASE) { // Read from DB
#ifdef __USE_CCBDB__ // DB access enabled
    char tmpxml[XML_MAX_DIM], cmnt[LEN2];
    ccbrefdb rCcbref(dtdbobj); // Use an existing connection to DB

    if (rCcbref.retrieveref(ccb_id,rob,tmpxml,cmnt)==0)
      robstr.insert(0,tmpxml);
#else
    nodbmsg();
#endif
  }

  if (robstr.length()>0) { // The reference has been read

    if (xml_get_root(robstr).find(ROB_XML_ROOT,0)==std::string::npos)
      return -2; // Root tag not found: return

// Read reference values for ROB status

    read_from_xml(robstr,"RobTempMin",7,ref_rob.RobTempMin);
    read_from_xml(robstr,"RobTempMax",7,ref_rob.RobTempMax);

    read_from_xml(robstr,"RobVccMin",7,ref_rob.RobVccMin);
    read_from_xml(robstr,"RobVccMax",7,ref_rob.RobVccMax);

    read_from_xml(robstr,"RobVddMin",7,ref_rob.RobVddMin);
    read_from_xml(robstr,"RobVddMax",7,ref_rob.RobVddMax);

    read_from_xml(robstr,"RobIMin",7,ref_rob.RobIMin);
    read_from_xml(robstr,"RobIMax",7,ref_rob.RobIMax);

    return 0; // No error
  }
  else
    return -1; // Error

}


// Print ROB reference values which have been loaded
// Author: A.Parenti, Jul24 2007
void Cref::print_rob_ref() {
  int i;

  if (this==NULL) return; // Object deleted -> return

  printf("\n*** ROB Reference Values ***\n");
  if (reffrom==UFILE)
    printf("-> File: %s\n",RobName.c_str());
  else if (reffrom==UDATABASE)
    printf("-> Values retrieved from DB (ccbid=%d)\n",ccb_id);
  else
    return;
  printf("-> Values:\n");

  for (i=0;i<7;++i)
    printf("RobTemp[%d]: %f <-> %f (C)\n",i,ref_rob.RobTempMin[i],ref_rob.RobTempMax[i]);
  for (i=0;i<7;++i)
    printf("RobVcc[%d]: %f <-> %f (V)\n",i,ref_rob.RobVccMin[i],ref_rob.RobVccMax[i]);
  for (i=0;i<7;++i)
    printf("RobVdd[%d]: %f <-> %f (V)\n",i,ref_rob.RobVddMin[i],ref_rob.RobVddMax[i]);
  for (i=0;i<7;++i)
    printf("RobI[%d]: %f <-> %f (A)\n",i,ref_rob.RobIMin[i],ref_rob.RobIMax[i]);

  printf("*** End ROB Reference Values ***\n");
}



// Load PADC reference values from file <PadcName> or DB,
// put them in <ref_padc>
// Author: A.Parenti, Jul25 2007
// Modified: AP, Oct19 2007
int Cref::load_padc_ref() {
  std::string padcstr="";

  if (this==NULL) return -1; // Object deleted -> return

  if (reffrom==UFILE) { // Read from file
    padcstr = read_file(PadcName);
  }
  else if (reffrom==UDATABASE) { // Read from DB
#ifdef __USE_CCBDB__ // DB access enabled
    char tmpxml[XML_MAX_DIM], cmnt[LEN2];
    ccbrefdb rCcbref(dtdbobj); // Use an existing connection to DB

    if (rCcbref.retrieveref(ccb_id,padc,tmpxml,cmnt)==0)
      padcstr.insert(0,tmpxml);
#else
    nodbmsg();
#endif
  }

  if (padcstr.length()>0) { // The reference has been read

    if (xml_get_root(padcstr).find(PADC_XML_ROOT,0)==std::string::npos)
      return -2; // Root tag not found: return

// Read reference values for PADC status

    read_from_xml(padcstr,"Sens100HvMin",&ref_padc.Sens100HvMin);
    read_from_xml(padcstr,"Sens100HvMax",&ref_padc.Sens100HvMax);
    read_from_xml(padcstr,"Sens500HvMin",&ref_padc.Sens500HvMin);
    read_from_xml(padcstr,"Sens500HvMax",&ref_padc.Sens500HvMax);
    read_from_xml(padcstr,"SensHvVccMin",&ref_padc.SensHvVccMin);
    read_from_xml(padcstr,"SensHvVccMax",&ref_padc.SensHvVccMax);
    read_from_xml(padcstr,"Sens100FeMin",&ref_padc.Sens100FeMin);
    read_from_xml(padcstr,"Sens100FeMax",&ref_padc.Sens100FeMax);
    read_from_xml(padcstr,"Sens500FeMin",&ref_padc.Sens500FeMin);
    read_from_xml(padcstr,"Sens500FeMax",&ref_padc.Sens500FeMax);
    read_from_xml(padcstr,"SensFeVccMin",&ref_padc.SensFeVccMin);
    read_from_xml(padcstr,"SensFeVccMax",&ref_padc.SensFeVccMax);
    read_from_xml(padcstr,"PadcVccMin",&ref_padc.PadcVccMin);
    read_from_xml(padcstr,"PadcVccMax",&ref_padc.PadcVccMax);
    read_from_xml(padcstr,"PadcVddMin",&ref_padc.PadcVddMin);
    read_from_xml(padcstr,"PadcVddMax",&ref_padc.PadcVddMax);
    read_from_xml(padcstr,"pRelVarMax",&ref_padc.pRelVarMax);
    read_from_xml(padcstr,"pMin",&ref_padc.pMin);


    return 0; // No error
  }
  else
    return -1; // Error

}


// Print ROB reference values which have been loaded
// Author: A.Parenti, Jul25 2007
// Modified: AP, Oct19 2007
void Cref::print_padc_ref() {
  if (this==NULL) return; // Object deleted -> return

  printf("\n*** PADC Reference Values ***\n");
  if (reffrom==UFILE)
    printf("-> File: %s\n",PadcName.c_str());
  else if (reffrom==UDATABASE)
    printf("-> Values retrieved from DB (ccbid=%d)\n",ccb_id);
  else
    return;
  printf("-> Values:\n");

  printf("Sens100Hv: %f <-> %f (bar)\n",ref_padc.Sens100HvMin,ref_padc.Sens100HvMax);
  printf("Sens500Hv: %f <-> %f (bar)\n",ref_padc.Sens500HvMin,ref_padc.Sens500HvMax);
  printf("SensHvVcc: %f <-> %f (V)\n",ref_padc.SensHvVccMin,ref_padc.SensHvVccMax);
  printf("Sens100Fe: %f <-> %f (bar)\n",ref_padc.Sens100FeMin,ref_padc.Sens100FeMax);
  printf("Sens500Fe: %f <-> %f (bar)\n",ref_padc.Sens500FeMin,ref_padc.Sens500FeMax);
  printf("SensFeVcc: %f <-> %f (V)\n",ref_padc.SensFeVccMin,ref_padc.SensFeVccMax);
  printf("PadcVcc: %f <-> %f (V)\n",ref_padc.PadcVccMin,ref_padc.PadcVccMax);
  printf("PadcVdd: %f <-> %f (V)\n",ref_padc.PadcVddMin,ref_padc.PadcVddMax);
  printf("pRelVarMax: %f\n",ref_padc.pRelVarMax);
  printf("pMin: %f (bar)\n",ref_padc.pMin);

  printf("*** End PADC Reference Values ***\n");
}
