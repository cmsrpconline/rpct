#include <ccbcmds/apfunctions.h>
#include <iostream>
#include <cstdio>
#include <cmath>
#include <ctime>
#include <netinet/in.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <fstream>

#include <unistd.h>
#include <fcntl.h>
#include <string.h>
/***************************************/
/* Additional functions of general use */
/***************************************/


/***********************************************************************/
// Copy <len> characters from str2 to str1; return the No. of copied char.
// Author: A.Parenti, Nov26 2004


// Function template
template <class Type> int mystrcpy(Type str1, Type str2, int len) {
  int i;

  for (i=0; i<len; ++i)
    str1[i] = str2[i];
  return i;
}


// Explicit instantiations
template int mystrcpy<char*>(char*,char*,int);
template int mystrcpy<unsigned char*>(unsigned char*,unsigned char*,int);

/***********************************************************************/
// Write to standard output a string as hex integers
// Author: A.Parenti, Nov26 2004
// Modified: A.Parenti, Dec12 2006

// Function template
template <class Type>
void strprint(Type stringa, int len) {
  int i;

  printf("0x");
  for (i=0;i<len;++i)
//    printf("%02hhx",stringa[i]);
    printf("%02x",stringa[i]);
  putchar('\n');
}

// Explicit instantiations
template void strprint<char*>(char*, int);
template void strprint<unsigned char*>(unsigned char*, int);

/***********************************************************************/
// Convert an array of char in a string

// 1) Function definitions

template <class Type>
void array_to_string(Type strin, int len, char* strout) {
// Author: A.Parenti, Jan28 2005
// Modified: AP, Mar13 2006
  int i;

  strcpy(strout,""); // Empty string
  sprintf(strout,"%02X",strin[0]); // First character
  for (i=1;i<len;++i)
    sprintf(strout,"%s %02X",strout,strin[i]);
}

template <class Type>
void array_to_string(Type strin, int len, std::string *strout) {
// Author: A.Parenti, Apr12 2005
// Modified: AP, Mar13 2006
  char sss[5];
  int i;

  *strout="";
  for (i=0;i<len;++i) {
    sprintf(sss,"%02X ",strin[i]);
    *strout += sss;
  }
}


// 2) Explicit instantiations
template void array_to_string<char*>(char*,int,char*);
template void array_to_string<unsigned char*>(unsigned char*,int,char*);
template void array_to_string<char*>(char*,int,std::string*);
template void array_to_string<unsigned char*>(unsigned char*,int,std::string*);


/***********************************************************************/
// Convert a string in an array of chars

// 1) Function definition

// A.Parenti, Feb10 2005
template <class Type>
void string_to_array(char *strin, int base, Type arrout, int *arr_len) {
  char *start, *end;

  for (end=start=strin, (*arr_len)=0;end<strin+strlen(strin);start=end) {
    arrout[(*arr_len)++]=(unsigned char)strtol(start,&end,base);
    while (end[0]==' ') end++;
  }
}

// 2) Explicit instantiations
template void string_to_array<char*>(char*, int, char*, int *);
template void string_to_array<unsigned char*>(char*, int, unsigned char*, int *);

/***********************************************************************/
// Reset a vector


// 1) Function definition

// Author: A.Parenti, Aug25 2006
template <class Type>
void vector_reset(Type vector, int vector_size) {
  int i;

  for (i=0;i<vector_size;++i)
    vector[i]=0;
}

// 2) Explicit instantiations
template void vector_reset(char*,int);
template void vector_reset(unsigned char*,int);
template void vector_reset(short*,int);
template void vector_reset(unsigned short*,int);
template void vector_reset(int*,int);
template void vector_reset(unsigned int*,int);
template void vector_reset(bool*,int);
template void vector_reset(float*,int);
template void vector_reset(double*,int);


/***********************************************************************/
// Get the local time, put it in tempo[26]
// Author: A.Parenti, Jan19 2005
// Modified: AP, Feb02 2006
char* get_time(void) {
  static char tempo[26];
  int i;
  time_t seconds;

  time(&seconds);
  strcpy(tempo,ctime(&seconds));

  for (i=0;i<26;++i)
    if (tempo[i]=='\n') tempo[i]='\0'; // Replaces '\n' with '\0'

  return tempo;
}


/***********************************************************************/
// Sleep for <timewait> msec's
// Author: A.Parenti, Jan23 2007
// Modified: AP, Feb13 2007
void apsleep(int timewait) { 
  int i, fd1;
  struct timeval Timeout;
  fd_set readfs;

// Use sleep to wait seconds
  for (i=0; i<int(timewait/1000); ++i) {
    pthread_testcancel(); // if called in a thread, test for pending cancellations
    sleep(1);
  }


// Use select to wait milliseconds
  fd1 = open(".",O_RDONLY);

  if (fd1>0) {
    FD_ZERO(&readfs);
    FD_SET(fd1,&readfs);

    Timeout.tv_usec = 1000*(timewait%1000);  /* microseconds */
    Timeout.tv_sec  = 0;  /* seconds */

    select(1,&readfs,NULL,NULL,&Timeout); // Wait for timeout

    close(fd1);
  }
}


/***********************************************************************/
// Wait for a key to be pressed
// Author: A.Parenti, Feb13 2006
void mywait() {
  std::cout << "Press <return> to continue...\n";
  getchar();
}


/***********************************************************************/
// Conversion from DSP float to IEEE32 format
// Function from Lorenzo Castellani
// Author: A. Parenti, Nov02 2004

//void DSPtoIEEE32(short DSPmantissa, short DSPexp, float *f) {
//  int sign;
//
//  sign = DSPmantissa & 0x8000;
//  DSPmantissa &= 0x7FFF;
//  if (sign)
//    DSPmantissa = -DSPmantissa; // convert negative values in 2.s complement
//  DSPexp -= 15;
//  *f = DSPmantissa*pow(2.0,DSPexp);
//}

// Modified: AP, Mar30 2005
void DSPtoIEEE32(short DSPmantissa, short DSPexp, float *f) {
  DSPexp -= 15;
  *f = DSPmantissa * (float)pow( 2.0, DSPexp ); 
}


/***********************************************************************/
// Conversion from IEEE32 float to DSP float
// Function from Lorenzo Castellani
// Author: A. Parenti, Nov02 2004

//void IEEE32toDSP(float f, short *DSPmantissa, short *DSPexp) {
//  long *pl, lm;
//
//  if (f==0.0)
//    *DSPexp = *DSPmantissa = 0;
//  else {
//    pl = (long *)&f; // [sign][1][23bit mantissa]
//    lm = ((*pl & 0x80000000)>>7) | 0x800000 | (*pl & 0x7FFFFF);
//    lm >>=9; // reduce to 16 bits
//    *DSPexp = ((*pl>>23) & 0xFF)-126;
//    *DSPmantissa = (short)lm;
//  }
//}

// Modified: AP, Mar30 2005
void IEEE32toDSP(float f, short *DSPmantissa, short *DSPexp) {
  long *pl, lm;
  bool sign=false;
  if( f==0.0 ) {
    *DSPexp = 0;
    *DSPmantissa = 0;
  } else {
    pl = (long *)&f;
    if((*pl & 0x80000000)!=0) sign=true;
    lm =( 0x800000 | (*pl & 0x7FFFFF)); // [1][23bit mantissa]
    lm >>= 9; //reduce to 15bits
    lm &= 0x7FFF;
    *DSPexp = ((*pl>>23)&0xFF)-126;
    *DSPmantissa = (short)lm;
    if(sign)
      *DSPmantissa = - *DSPmantissa; // convert negative value in 2.s
                                     // complement
  }
}

/**********************************************************************/
// CRC evaluation
// Translated from the Labview VI
// Author: A. Parenti, May11 2005

unsigned short crc(unsigned char *data, int len) {
  unsigned char tmpdata;
  unsigned short tmpcrc;
  int i, j;

  for (i=0, tmpcrc=0; i<len; ++i) {
    for (j=0, tmpdata=data[i]; j<8; ++j) {
      if ((((tmpdata^((tmpcrc>>8)&0xFF))>>7)&1)==1)
        tmpcrc = (tmpcrc<<1)^(0x1021);
      else
        tmpcrc = (tmpcrc<<1);

      tmpdata = (tmpdata<<1);
    }
  }
  return tmpcrc;
}



/*********************************************************************/
// Convert a number from host notation to network notation (MSB = BIT0)

void host_to_ccb(char input, unsigned char* output) {
  output[0] = (unsigned char) input;
}

void host_to_ccb(unsigned char input, unsigned char* output) {
  output[0] = input;
}

void host_to_ccb(short input, unsigned char* output) {
  short inter;
  inter = htons(input); // convert to "network" format
  // pick up the bytes
  output[0] = inter & 0xFF;
  output[1] = (inter>>8) & 0xFF;
}

void host_to_ccb(unsigned short input, unsigned char* output) {
  unsigned short inter;
  inter = htons(input); // convert to "network" format
  // pick up the bytes
  output[0] = inter & 0xFF;
  output[1] = (inter>>8) & 0xFF;
}

void host_to_ccb(int input, unsigned char* output) {
  int inter;
  inter = htonl(input); // convert to "network" format
  // pick up the bytes
  output[0] = inter & 0xFF;
  output[1] = (inter>>8) & 0xFF;
  output[2] = (inter>>16) & 0xFF;
  output[3] = (inter>>24) & 0xFF;
}

void host_to_ccb(unsigned int input, unsigned char* output) {
  unsigned int inter;
  inter = htonl(input); // convert to "network" format
  // pick up the bytes
  output[0] = inter & 0xFF;
  output[1] = (inter>>8) & 0xFF;
  output[2] = (inter>>16) & 0xFF;
  output[3] = (inter>>24) & 0xFF;
}

void host_to_ccb(float input, unsigned char* output) {
  short inter1, inter2;
  short inter3, inter4;

  IEEE32toDSP(input,&inter1,&inter2); // convert to DSP format

  inter3    = htons(inter1); // convert to "network" format
  output[0] = inter3 & 0xFF;
  output[1] = (inter3>>8) & 0xFF;

  inter4    = htons(inter2); // convert to "network" format
  output[2] = inter4 & 0xFF;
  output[3] = (inter4>>8) & 0xFF;  
}

void ccb_to_host(unsigned char* input, char* output) {
  *output = (char)input[0];
}

void ccb_to_host(unsigned char* input, unsigned char* output) {
  *output = input[0];
}


void ccb_to_host(unsigned char* input, short* output) {
  short inter;

  inter = (short)input[0] + ((short)input[1]<<8);
  *output = ntohs(inter);
}

void ccb_to_host(unsigned char* input, unsigned short* output) {
  unsigned short inter;

  inter = (unsigned short)input[0] + ((unsigned short)input[1]<<8);
  *output = ntohs(inter);
}

void ccb_to_host(unsigned char* input, int* output) {
  int inter;

  inter = (short)input[0] + ((short)input[1]<<8) + ((short)input[2]<<16) +
    ((short)input[3]<<24);
 *output = ntohl(inter);
}

void ccb_to_host(unsigned char* input, unsigned int* output) {
  unsigned int inter;

  inter = (unsigned int)input[0] + ((unsigned int)input[1]<<8) + ((unsigned int)input[2]<<16) +
    ((unsigned int)input[3]<<24);
 *output = ntohl(inter);
}

void ccb_to_host(unsigned char* input, float* output) {
  short inter1, inter2;
  short inter3, inter4;

  inter1 = (short)input[0] + ((short)input[1]<<8);
  inter3 = ntohs(inter1); // convert to "host" format

  inter2 = (short)input[2] + ((short)input[3]<<8);
  inter4 = ntohs(inter2); // convert to "host" format

  DSPtoIEEE32(inter3, inter4, output); // convert to IEEE32 format
}


/**********************************************************************/
// Read a bit from a char
// Author: A.Parenti, Dec15 2004
// Modified: AP, Mar09 2005
bool takebit(unsigned char word, int idx) {
  if (idx>=0 && idx<8*(int)sizeof(word))
    return (bool) ((word>>idx)&1);
  else
    return false;
}


/**********************************************************************/
// Read a variable from a unsigned char string, starting from <index>
// Return the pointer to the variable next to the read one.
// Author: A.Parenti, Dec15 2004
// Modified: AP, Aug02 2005


// Function template
template <class Type>
int rstring(unsigned char* string, int index, Type pvar) {
  ccb_to_host(string+index, pvar);
  return (index+sizeof(*pvar));
}

// Explicit instantiations
template int rstring<char*>(unsigned char*,int,char*);
template int rstring<unsigned char*>(unsigned char*,int,unsigned char*);
template int rstring<short*>(unsigned char*,int,short*);
template int rstring<unsigned short*>(unsigned char*,int,unsigned short*);
template int rstring<int*>(unsigned char*,int,int*);
template int rstring<unsigned int*>(unsigned char*,int,unsigned int*);
template int rstring<float*>(unsigned char*,int,float*);


/**********************************************************************/
// Write a variable to a unsigned char string, starting at <index>
// Return the pointer to the array element next to the written one.
// Author: A.Parenti, Dec22 2004
// Modified: Aug03 2005

template <class Type> int wstring(Type var, int index, unsigned char* string) {
  unsigned char tmpstr[4];

  host_to_ccb(var,tmpstr);
  mystrcpy(string+index,tmpstr,sizeof(var));
  return (index+sizeof(var));
}

template int wstring<unsigned char>(unsigned char,int,unsigned char*);
template int wstring<char>(char,int,unsigned char*);
template int wstring<unsigned short>(unsigned short,int,unsigned char*);
template int wstring<short>(short,int,unsigned char*);
template int wstring<unsigned int>(unsigned int,int,unsigned char*);
template int wstring<int>(int,int,unsigned char*);
template int wstring<float>(float,int,unsigned char*);


/**********************************************************************/
// Read a file and return the content
// Author: A. Parenti, Oct11 2005
char *read_file(char *filename) {
  int c;
  std::string content="";
  FILE *fstream;

  fstream = fopen(filename, "r") ; // Open the file

  if (fstream!=NULL) { // File has been open
    while ((c=fgetc(fstream))!=EOF) // Loop while extraction from file is possible
      content += (char)c; // Get character from file, put it in <content>
//      putchar(c);
    fclose(fstream); // Close file
  }
  return (char*)content.c_str();
}

// Read a file and return the content
// Author: A. Parenti, Jul07 2006
std::string read_file(std::string filename) {
  int c;
  std::string content="";
  FILE *fstream;

  fstream = fopen(filename.c_str(), "r") ; // Open the file

  if (fstream!=NULL) { // File has been open
    while ((c=fgetc(fstream))!=EOF) // Loop while extraction from file is possible
      content += (char)c; // Get character from file, put it in <content>
    fclose(fstream); // Close file
  }
  return content;
}

/**********************************************************************/
// XML parser. Uses libxml2 from gnome

// Recover root tag from an xml buffer
// Author: A. Parenti, Feb16 2006
// Modified: Oct26 2007
std::string xml_get_root(std::string strin) {
  std::string strout="";
  xmlDocPtr doc=NULL; // Document pointer
  xmlNodePtr cur=NULL; // Node pointer

// Return, if strin is empty
  if (strin.length()==0)
    return strout;

  LIBXML_TEST_VERSION; // Check libxml version

// Declare document pointer
  doc = xmlParseMemory(strin.c_str(),strin.length());
  if (doc == NULL)
    return strout;

// Declare node pointer, point to root element
  cur = xmlDocGetRootElement(doc);
  if (cur != NULL) {
    strout = (char*)cur->name;
  }

  xmlFreeDoc(doc); // Free pointer. All memory can be sucked by xml2 otherwise.
  return strout;
}


// Recover tag content from an xml buffer
// Author: A. Parenti, Feb14 2006
// Modified: Oct26 2007
std::string xml_get(std::string strin, char *tag) {
  std::string strout="";
  xmlDocPtr doc=NULL; // Document pointer
  xmlNodePtr cur=NULL; // Node pointer

// Return, if strin is empty
  if (strin.length()==0)
    return strout;

  LIBXML_TEST_VERSION; // Check libxml version

// Declare document pointer
  doc = xmlParseMemory(strin.c_str(),strin.length());
  if (doc == NULL)
    return strout;

// Declare node pointer, point to root element
  cur = xmlDocGetRootElement(doc);
  if (cur == NULL) {
    xmlFreeDoc(doc); // Free pointer
    return strout;
  }

  cur = cur->xmlChildrenNode; // Point to first child of root
  while ((strout.length()==0) && (cur!=NULL)) {
    if ((!xmlStrcmp(cur->name, (const xmlChar *)tag))){
      strout = (char*)xmlNodeGetContent(cur);
    }    
    cur = cur->next; // Next element
  }

  xmlFreeDoc(doc); // Free pointer. All memory can be sucked by xml2 otherwise.
  return strout;
}

// Recover tag content from an xml buffer
// Author: A. Parenti, Feb14 2006
// Modified: Oct26 2007
std::string xml_get(std::string strin, char *tag, unsigned int idx) {
  std::string strout="";
  xmlDocPtr doc=NULL; // Document pointer
  xmlNodePtr cur=NULL; // Node pointer
  xmlChar *attr; // Element attribute

// Return, if strin is empty
  if (strin.length()==0)
    return strout;

  LIBXML_TEST_VERSION; // Check libxml version

// Declare document pointer
  doc = xmlParseMemory(strin.c_str(),strin.length());
  if (doc == NULL)
    return strout;

// Declare node pointer, point to root element
  cur = xmlDocGetRootElement(doc);
  if (cur == NULL) {
    xmlFreeDoc(doc); // Free pointer
    return strout;
  }

  cur = cur->xmlChildrenNode; // Point to first child of root
  while ((strout.length()==0) && (cur!=NULL)) {
    if ((!xmlStrcmp(cur->name, (const xmlChar *)tag))){
      attr = xmlGetProp(cur, (const xmlChar *)"idx");
      if (idx==strtol((const char*)attr,NULL,0))
        strout = (char*)xmlNodeGetContent(cur);
    }    
    cur = cur->next; // Next element
  }

  xmlFreeDoc(doc); // Free pointer. All memory can be sucked by xml2 otherwise.
  return strout;
}

/**********************************************************************/
// Read a variable from an xml buffer
// Author: A. Parenti, Oct11 2005

void read_from_xml(std::string strin, char *tag, unsigned char *varout) {
  unsigned int tmp;

  read_from_xml(strin, tag, &tmp);
  *varout = (unsigned char)tmp;
}

void read_from_xml(std::string strin, char *tag, unsigned short *varout) {
  unsigned int tmp;

  read_from_xml(strin, tag, &tmp);
  *varout = (unsigned short)tmp;
}

void read_from_xml(std::string strin, char *tag, unsigned int *varout) {
  std::string varstr;

  varstr = xml_get(strin, tag);
  *varout=strtoul(varstr.c_str(),NULL,0);
}

void read_from_xml(std::string strin, char *tag, char *varout) {
  int tmp;

  read_from_xml(strin, tag, &tmp);
  *varout = (char)tmp;
}

void read_from_xml(std::string strin, char *tag, short *varout) {
  int tmp;

  read_from_xml(strin, tag, &tmp);
  *varout = (short)tmp;
}

void read_from_xml(std::string strin, char *tag, int *varout) {
  std::string varstr;

  varstr = xml_get(strin, tag);
  *varout=strtol(varstr.c_str(),NULL,0);
}

void read_from_xml(std::string strin, char *tag, float *varout) {
  std::string varstr;

  varstr = xml_get(strin, tag);
  *varout=strtod(varstr.c_str(),NULL);  // strtof doesn't work on some systems
}

/* Recover array elements */
// A.Parenti, Oct19 2005

void read_from_xml(std::string strin, char *tag, int len,
  unsigned char *varout) {
  unsigned int *tmp;
  int i;

  tmp = new unsigned int[len];
  read_from_xml(strin, tag, len, tmp);
  for (i=0; i<len; ++i)
    varout[i] = (unsigned char)tmp[i];

  delete[] tmp;
}

void read_from_xml(std::string strin, char *tag, int len,
  unsigned short *varout) {
  unsigned int *tmp;
  int i;

  tmp = new unsigned int[len];
  read_from_xml(strin, tag, len, tmp);
  for (i=0; i<len; ++i)
    varout[i] = (unsigned short)tmp[i];

  delete[] tmp;
}

void read_from_xml(std::string strin, char *tag, int len, unsigned int *varout) {
  int i;
  std::string varstr;

  for (i=0; i<len; ++i) {
    varstr = xml_get(strin, tag, i);
    varout[i]=strtoul(varstr.c_str(),NULL,0);
  }
}

void read_from_xml(std::string strin, char *tag, int len, char *varout) {
  int i;
  int *tmp;

  tmp = new int[len];
  read_from_xml(strin, tag, len, tmp);
  for (i=0; i<len; ++i)
    varout[i] = (char)tmp[i];

  delete[] tmp;
}

void read_from_xml(std::string strin, char *tag, int len, short *varout) {
  int i;
  int *tmp;

  tmp = new int[len];
  read_from_xml(strin, tag, len, tmp);

  for (i=0; i<len; ++i)
    varout[i] = (short)tmp[i];

  delete[] tmp;
}

void read_from_xml(std::string strin, char *tag, int len, int *varout) {
  int i;
  std::string varstr;

  for (i=0; i<len; ++i) {
    varstr = xml_get(strin, tag, i);
    varout[i]=strtol(varstr.c_str(),NULL,0);
  }
}

void read_from_xml(std::string strin, char *tag, int len, float *varout) {
  int i;
  std::string varstr;

  for (i=0; i<len; ++i) {
    varstr = xml_get(strin, tag, i);
    varout[i]=strtod(varstr.c_str(),NULL);  // strtof doesn't work on some systems
  } 
}

/**********************************************************************/
// Define an array with some additional methods
// Author: A. Parenti, Nov03 2008

flarray::flarray() {
  len   = 0;
  nmax  = 0;
  initialized = false;
}

flarray::~flarray() {
  if (initialized) delete[] array;
}

void flarray::init(int i) {
  if (!initialized && i > 0) {
    initialized = true;
    nmax = i;
    array = new float[i];
  }
}

int flarray::size() {
  return len;
}

void flarray::insert(float x) {
  int i;

  if (!initialized) return;

  if (len >= nmax) {
    for (i=0; i<(nmax-1); ++i)
      array[i] = array[i+1];
    array[nmax-1] = x;
  } else {
    array[len++] = x;
  }
}

void flarray::clear() {
  int i;

  if (!initialized) return;

  len = 0;
  for (i=0;i<nmax;++i)
    array[i] = 0.;
}

float flarray::mean() {
  int i;
  float fmean;

  if (!initialized || len<=0) return 0.;

  for (i=0, fmean=0.;i<len;++i)
    fmean += array[i];

  fmean /= len;
  return fmean;
}
