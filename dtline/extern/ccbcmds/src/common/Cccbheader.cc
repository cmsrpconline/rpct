#include <ccbcmds/Cccbheader.h>
#include <ccbcmds/apfunctions.h>

/*****************************************************************************/
// Constructors/Destructor
/*****************************************************************************/

// Class constructor
// Author: A. Parenti, Jun23 2005
// Modified: AP, Mar02 2007
Cccbheader::Cccbheader() {
  ccb_id   = CCB_ID_DEFAULT;
  ccb_port = CCB_ID_DEFAULT;
  ccb_secport = CCB_ID_DEFAULT;
  timeout  = TIMEOUT_DEFAULT;
  use_secondary = false; // default: use optical link
}

// Class constructor
// Author: A. Parenti, Oct10 2007
Cccbheader::Cccbheader(short this_ccb, short this_ccbport, short this_ccbsecport, int this_timeout) {
  ccb_id   = this_ccb;
  ccb_port = this_ccbport;
  ccb_secport = this_ccbsecport;
  timeout  = this_timeout;
  use_secondary = false; // default: use optical link
}

/*****************************************************************************/
// Set-up
/*****************************************************************************/

// Read the CCB_ID
// A.Parenti, Dec13 2004
short Cccbheader::read_ccb_id() {
  if (this==NULL) return 0; // Object deleted -> return

  return ccb_id;
}

// Read the CCB_PORT
// A.Parenti, Oct14 2006
short Cccbheader::read_ccb_port() {
  if (this==NULL) return 0; // Object deleted -> return

  return ccb_port;
}

// Read the timeout (ms)
// A. Parenti, Dec13 2004
int Cccbheader::read_timeout() {
  if (this==NULL) return 0; // Object deleted -> return

  return timeout;
}

// Set the timeout (ms). Return the timeout
// A. Parenti, Dec14 2004
// Modified: AP, Mar23 2005
int Cccbheader::set_timeout(int this_timeout) {
  if (this==NULL) return 0; // Object deleted -> return

  if (timeout>0)
    timeout = this_timeout;

  return timeout;
}


// Switch to optical link. Return non-0 if alredy using it
// Author: A.Parenti, Mar02 2007
int Cccbheader::use_primary_port() {
  if (this==NULL) return 0; // Object deleted -> return

  if (!use_secondary) // Already using optical link
    return -1;

  use_secondary=false;
  return 0;
}


// Switch to copper link. Return non-0 if alredy using it
// Author: A.Parenti, Mar02 2007
int Cccbheader::use_secondary_port() {
  if (this==NULL) return 0; // Object deleted -> return

  if (use_secondary) // Already using copper link
    return -1;

  use_secondary=true;
  return 0;
}


// Switch to copper link. Return non-0 if alredy using it
// Author: A.Parenti, Mar02 2007
bool Cccbheader::which_port() {
  if (this==NULL) return 0; // Object deleted -> return


  return use_secondary;
}

/*****************************************************************************/
// Header encoding/decoding
/*****************************************************************************/
// Create the ccb_server header
// A. Parenti, Jun24 2005
void Cccbheader::make_srv_header(int length, unsigned char header[SRV_HDR_LEN]) {
  if (this==NULL) return; // Object deleted -> return

  make_srv_header(length, timeout, header);
}

// Create the ccb_server header
// Author: A. Parenti, Jun24 2005
// Modified: AP, Oct10 2007 (Use of secondary port)
void Cccbheader::make_srv_header(int length, int this_timeout, unsigned char header[SRV_HDR_LEN]) {
  unsigned char temp[4];

  if (this==NULL) return; // Object deleted -> return

  timeout = this_timeout; // set new timeout

  header[0] = header[1] = 0x55;

  if (use_secondary) // Use secondary port
    host_to_ccb(ccb_secport,temp);
  else // Use optical link
    host_to_ccb(ccb_port,temp);

  mystrcpy(header+2,temp,2); // add ccb_port info
  host_to_ccb((int)(SRV_HDR_LEN+length),temp);
  mystrcpy(header+4,temp,4); // add length info
  host_to_ccb((int)timeout,temp);
  mystrcpy(header+8,temp,4); // add timeout info
}

// Decode the ccb_server header
// Author: A. Parenti, Jun24 2005
// Modified: AP, Sep20 2006
void Cccbheader::decode_srv_header(unsigned char *header, short *Pccbport, int *Plength, int *Perror) {
  if (this==NULL) return; // Object deleted -> return

  if (header[0]==0x55 && header[1]==0x55) {
    ccb_to_host(header+2,Pccbport); // CCB PORT
    ccb_to_host(header+4,Plength); // message length
    ccb_to_host(header+8,Perror); // timeout or error code
  } else { // Corrupted header...
    *Pccbport=*Plength=*Perror=0;
  }
}


// Create the ccb header
// A. Parenti, Jul21 2005
void Cccbheader::make_ccb_header(int length, unsigned char header[CCB_HDR_LEN]) {
  if (this==NULL) return; // Object deleted -> return

  header[0] = 0x55;
  if (length<=0xFF)
    header[1] = (unsigned char)length+2;
  else
    header[1] = 0xFF;
}

// Decode the ccb header
// Author: A. Parenti, Jul21 2005
// Modified: AP, Jul25 2005
void Cccbheader::decode_ccb_header(unsigned char *header, unsigned char *Plength) {
  if (this==NULL) return; // Object deleted -> return

  *Plength = header[2]-CCB_HDR_LEN; // message length
}
// Author: A. Parenti, Jul21 2005
void Cccbheader::decode_ccb_header(unsigned char *header, int *Plength) {
  unsigned char lll;

  if (this==NULL) return; // Object deleted -> return

  decode_ccb_header(header,&lll);
  *Plength = (int)lll;
}


// Check if the input string contains the ccb_server header
// Author: A.Parenti, Jul11 2005
bool Cccbheader::is_there_srv_header(unsigned char *str_in) {
  if (str_in[0]==0x55 && str_in[1]==0x55)
    return true;
  else
    return false;
}
