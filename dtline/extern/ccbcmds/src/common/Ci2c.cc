#include <ccbcmds/Ci2c.h>

#ifdef __USE_CCBDB__ // DB access enabled
  #include <ccbdb/Cpadcdata.h>
  #include <ccbdb/Cpadcstatus.h>
#endif

#include <iostream>
#include <cstdio>
#include <cmath>

/*****************************************************************************/
// I2C class
/*****************************************************************************/

// Class constructor; arguments: pointer_to_command_object
// Author: A.Parenti, Dec01 2004
// Modified: AP, Sep04 2008
Ci2c::Ci2c(Ccommand* ppp) : Ccmn_cmd(ppp) {
  rpc_I2C_reset(); // Reset rpc_i2c struct
  led_I2C_reset(); // Reset led_i2c struct
  read_ADC_reset(); // Reset read_adc struct
  status_PADC_reset(); // Reset status_padc struct (except averages)
  status_PADC_reset_average(); // Reset status_padc struct (only averages)
}


/****************************/
/* Override ROB/TDC methods */
/****************************/

void Ci2c::read_TDC(char tdc_brd, char tdc_chip) {}
int Ci2c::read_TDC_decode(unsigned char *rdstr) {return 0;}
void Ci2c::read_TDC_print() {}

void Ci2c::status_TDC(char tdc_brd, char tdc_chip) {}
void Ci2c::status_TDC_print() {}

void Ci2c::read_ROB_error() {}
int Ci2c::read_ROB_error_decode(unsigned char *rdstr) {return 0;}
void Ci2c::read_ROB_error_print() {}

/*****************/
/*  Test LED I2C */
/*****************/

// Test LED I2C. Input: None. Output: Test_error
// Author: A. Parenti, May12 2005
// Modified: AP, May27 2005
// AP: Tested at Cessy, Jul08 2005
void Ci2c::test_LED_I2C(bool *Test_error) {
  bool switch_off;
  char i, data_size;
  short ctrl_data[]={0x171,0xE00};

  if (this==NULL) return; // Object deleted -> return

  *Test_error = false; // no error, till now

  PWR_on(5,1); // Power on LEDs
  if (pwr_on.result==3) // LEDs were already ON
    switch_off = false;

  data_size = sizeof(ctrl_data)/sizeof(short);
  led_I2C(ctrl_data,data_size); // Send commands

  if (led_i2c.ccbserver_code!=0) *Test_error = true; // Wrong reply
  for (i=0;i<data_size;++i)
    if ((led_i2c.err_data[i]>>8)!=0)
      *Test_error = true; // some error occured

  if ((led_i2c.err_data[0]&0xFF) != 0x113)
    *Test_error = true; // wrong reply

  if ((led_i2c.err_data[1]&0xFF) != 0x231)
    *Test_error = true; // wrong reply

// Here I also check mc_status.AlrmPwrLed
  MC_read_status();
  if (mc_status.AlrmPwrLed==1) *Test_error = true; // Led error

  if (switch_off)
    PWR_on(5,0); // Power off LEDs

}


/*****************/
/*  Test RPC I2C */
/*****************/

// Test RPC I2C. Input: None. Output: Test_error
// Author: A. Parenti, May12 2005
// Modified: AP, May27 2005
// AP: Tested at Cessy, Jul08 2005
void Ci2c::test_RPC_I2C(bool *Test_error) {
  bool switch_off;
  char i, data_size;
  short ctrl_data[]={0x171,0xE00};

  if (this==NULL) return; // Object deleted -> return

  *Test_error = false; // no error, till now

  PWR_on(4,1); // Power on RPCs
  if (pwr_on.result==3) // RPCs were already ON
    switch_off = false;

  data_size = sizeof(ctrl_data)/sizeof(short);
  rpc_I2C(ctrl_data,data_size); // Send commands

  if (rpc_i2c.ccbserver_code!=0) *Test_error = true; // Wrong reply
  for (i=0;i<data_size;++i)
    if ((rpc_i2c.err_data[i]>>8)!=0)
      *Test_error = true; // some error occured

  if ((rpc_i2c.err_data[0]&0xFF) != 0x113)
    *Test_error = true; // wrong reply

  if ((rpc_i2c.err_data[1]&0xFF) != 0x231)
    *Test_error = true; // wrong reply

// Here I also check mc_status.AlrmPwrRpc
  MC_read_status();
  if (mc_status.AlrmPwrRpc==1) *Test_error = true; // Led error

  if (switch_off)
    PWR_on(4,0); // Power off RPCs

}


/*************/
/* Test LEDs */
/*************/

// Test LEDs. Input: none. Output: Test_error
// Author: A. Parenti, May12 2005
// Modified: AP, May27 2005
// Tested at Cessy, Jul09 2005
void Ci2c::test_LED(bool *Test_error) {
  bool switch_off;
  char i, data_size;
  short ctrl_data[]={0x1a0,0x4,0x3f,0,0,0x25a,0x1a2,0x4,0x3f,0,0,0x25a,0x1a4,0x4,0x3f,0,0,0x25a,0x1a6,0x4,0x3f,0,0,0x25a};

  if (this==NULL) return; // Object deleted -> return

  *Test_error = false; // no error, till now

  PWR_on(5,1); // Power on leds
  if (pwr_on.result==3) // LEDs were already ON
    switch_off = false;

  data_size = sizeof(ctrl_data)/sizeof(short);
  led_I2C(ctrl_data,data_size); // Send commands

  if (led_i2c.ccbserver_code!=0) *Test_error = true; // Wrong reply
  for (i=0;i<data_size;++i)
    if ((led_i2c.err_data[i]>>8)!=0)
      *Test_error = true; // some error occured

// Here I also check mc_status.AlrmPwrLed
  MC_read_status();
  if (mc_status.AlrmPwrLed==1) *Test_error = true; // Led error

  if (switch_off)
    PWR_on(5,0); // Power off LEDs
}


/**************/
/* Read PADCs */
/**************/

// Read PADCs
// Author: A. Parenti, May12 2005
// Tested at Cessy, Jul09 2005
// Modified: AP, Aug23 2006
void Ci2c::read_PADC(bool *Test_error, short adc_count[10],float volt_read[10]) {
  bool switch_off=false;
  char i, data_size;
  short ctrl_data[]={0x16A,0xAA,0x213,0x16B,0x400,0x400,0x400,0x400,0x400,0x400,0x400,0x400,0x400,0x400,0x400,0x400,0x400,0x400,0x400,0x400,0x400,0x400,0x400,0xE00};

  if (this==NULL) return; // Object deleted -> return

  *Test_error = false; // no error, till now

  PWR_on(5,1); // Power on leds
  if (pwr_on.result==3) // LEDs were already ON
    switch_off = false;

  data_size = sizeof(ctrl_data)/sizeof(short);
  led_I2C(ctrl_data,data_size); // Send commands

  if (led_i2c.ccbserver_code!=0) *Test_error = true; // Wrong reply
  for (i=0;i<data_size;++i)
    if ((led_i2c.err_data[i]>>8)!=0)
      *Test_error = true; // some error occured

  for (i=0; i<10; ++i) {
    adc_count[i] = (((led_i2c.err_data[4+2*i]<<8) + led_i2c.err_data[5+2*i])&0x3FF); // ADC counts
    volt_read[i] = adc_count[i] * VREF/1023; // Uncalibrated volts
  }

// Here I also check mc_status.AlrmPwrLed
  MC_read_status();
  if (mc_status.AlrmPwrLed==1) *Test_error = true; // Led error

  if (switch_off)
    PWR_on(5,0); // Power off LEDs
}

// Use PADC LUTs (from file) to convert digits to pressure/voltage.
// Author: A. Parenti, Jul25 2006
// Modified/Tested: AP, Aug08 2006
// Modified/Tested: AP, Sep14 2007 (Pressures corrected for Vcc)
// Modified/Tested: AP, Sep25 2007 (Bug fix in Vcc/Vdd calculation)
int Ci2c::use_PADC_LUT(char *filename,short adc_count[10],float calib_data[10]){
  int i, j;
  float lut[10];

  std::string file_cont, file_name;
  std::string::size_type pos1, pos2;

  if (this==NULL) return -1; // Object deleted -> return

// Read file 
  file_name = filename;
  file_cont = read_file(file_name);

  if (file_cont.length()<=0) { // Error in opening/reading file
    for (i=0;i<10;++i)
      calib_data[i]=0.0;
    return -1;
  }

// Remove comments (comprises between '$' and newline
  while ((pos1=file_cont.find("$",0))!=std::string::npos &&
         ((pos2=file_cont.find("\n",pos1))!=std::string::npos))
    file_cont.erase(pos1,pos2-pos1+1);

// Remove comments (comprises between '$' and EOF
  if ((pos1=file_cont.find("$",0))!=std::string::npos)
    file_cont.erase(pos1,file_cont.length()-pos1);

// Use PADC LUT
  for (i=0;i<10;++i) {
    for (j=0,pos1=0;j<=adc_count[i];++j) { // Look for j-th line
      if (j>0) pos1=pos2+1;
      pos2=file_cont.find("\n",pos1);
    }

    sscanf(file_cont.substr(pos1,pos2-pos1).c_str(), // Read j-th line
           "%f %f %f %f %f %f %f %f %f %f",
           lut,lut+1,lut+2,lut+3,lut+4,lut+5,lut+6,lut+7,lut+8,lut+9);
    calib_data[i]=lut[i]; // Save converted value
  }

// Pressures must be rescaled when Vcc!=5V
  for (i=0;i<3;++i) {
    calib_data[i]=calib_data[i]*5/calib_data[3];
    calib_data[4+i]=calib_data[4+i]*5/calib_data[7];
  }

  return 0;
}


// Use PADC LUTs (from DB) to convert digits to pressure/voltage.
// Author: A. Parenti, Aug03 2006
// Modified/Tested: AP, Apr10 2007
// Modified: AP, Sep14 2007 (Pressures corrected for Vcc)
// Modified/Tested: AP, Sep25 2007 (Bug fix in Vcc/Vdd calculation)
// Modified: AP, Jun04 2008 (Pedestal subtraction)
int Ci2c::use_PADC_LUT(short adc_count[10],float calib_data[10]) {
  if (this==NULL) return -1; // Object deleted -> return

  float pedestal[10];
  short icount[10]={9999,9999,9999,9999,9999,9999,9999,9999,9999,9999};

#ifdef __USE_CCBDB__ // DB access enabled
  int i;

  padcdata *myPadc;
  if (cmd_obj->dtdbobj==NULL) {
    myPadc=new padcdata; // Create a new connection to DB
  } else {
    myPadc=new padcdata(cmd_obj->dtdbobj); // Use an existing connection to DB
  }

// Select the PADC corresponding to current ccb
  if (myPadc->select(cmd_obj->read_ccb_id())!=0) { // Error in reading from DB
    for (i=0;i<10;++i)
      calib_data[i]=0.0;
    return -1;
  }

// Use PADC LUT
  if (myPadc->retrievepadcdata(adc_count,calib_data)!=0)  { // Error
    for (i=0;i<10;++i)
      calib_data[i]=0.0;
    return -1;
  }

// Pressures must be rescaled when Vcc!=5V
  for (i=0;i<3;++i) {
    calib_data[i]=calib_data[i]*5/calib_data[3];
    calib_data[4+i]=calib_data[4+i]*5/calib_data[7];
  }

// Subtract pedestals (after pressure rescaling)
  if (myPadc->retrievepadcdata(icount,pedestal)==0)  { // Pedestals were found
    for (i=0;i<3;++i) {
      calib_data[i]   -= pedestal[i];  // HV sensors
      calib_data[4+i] -=pedestal[4+i]; // FE sensors
    }
  }

  delete myPadc;
  return 0;
#else
  nodbmsg();
  return -1;
#endif 
}


/**********************/
/* Check PADC status  */
/**********************/

// Check PADC status.
// Author: A. Parenti, Jul25 2007
// Modified: AP, Sep20 2007 (Compute pressure average, use it for monitoring)
// Modified: AP, Oct05 2007 (Monitoring of averages modified)
// Modified: AP, Oct15 2007 (sensFE_Vcc wasn't written to statusxml)
// Modified: AP, Oct19 2007 (Computing of averages modified)
// Modified: AP, Apr22 2008 (Bug fix in <padc_status> closure)
// Modified: AP, Nov03 2008 (New averaging procedure)
void Ci2c::status_PADC(Cref *Pcref, float calib_data[10]) {
  bool nodata;
  char tmpstr[100];
  unsigned int i;
  float PRelVar;
  float s100_HV_mean, s100_FE_mean; // Average of all readings
  float s500_HV_mean, s500_FE_mean; // Average of all readings

  if (this==NULL) return; // Object deleted -> return

  status_PADC_reset(); // Reset status_padc struct

// Read Error
  for (i=0,nodata=true;i<10&&nodata;i++) {
    if (calib_data[i]!=0) nodata=false;
  }

  if (nodata) {
    //taken away to avoid errors
	 //status_padc.error=true;
    status_padc.errmsg+="\nNo PADC calibrated data\n";
    return;
  }

// First store pressure values ...
  if (fabs(calib_data[0])<Pcref->ref_padc.Sens100HvMax && fabs(calib_data[1])<Pcref->ref_padc.Sens100HvMax) {
    status_padc.p100hvA.insert(calib_data[0]); // Store the new value
    status_padc.p100hvB.insert(calib_data[1]); // Idem
  }
  if (fabs(calib_data[2])<Pcref->ref_padc.Sens500HvMax) {
    status_padc.p500hv.insert(calib_data[2]); // Store the new value
  }
  if (fabs(calib_data[4])<Pcref->ref_padc.Sens100FeMax &&  fabs(calib_data[5])<Pcref->ref_padc.Sens100FeMax) {
    status_padc.p100feA.insert(calib_data[4]); // Store the new value
    status_padc.p100feB.insert(calib_data[5]); // Idem
  }
  if (fabs(calib_data[6])<Pcref->ref_padc.Sens500FeMax) {
    status_padc.p500fe.insert(calib_data[6]); // Store the new value
  }

// ... then compute averages
  s100_HV_mean = 0.5*(status_padc.p100hvA.mean()+status_padc.p100hvB.mean());
  s100_FE_mean = 0.5*(status_padc.p100feA.mean()+status_padc.p100feB.mean());
  s500_HV_mean = status_padc.p500hv.mean();
  s500_FE_mean = status_padc.p500fe.mean();

// Fill-up txt message
  status_padc.stmsg="\nPADC Status:\n";
  sprintf(tmpstr,"sens100_HV: %.2e %.2e bar\n",calib_data[0],calib_data[1]);
  status_padc.stmsg+=tmpstr;

  sprintf(tmpstr,"sens500_HV: %.2e bar\n",calib_data[2]);
  status_padc.stmsg+=tmpstr;

  sprintf(tmpstr,"sensHV_Vcc: %4.2f V\n",calib_data[3]);
  status_padc.stmsg+=tmpstr;

  sprintf(tmpstr,"sens100_FE: %.2e %.2e bar\n",calib_data[4],calib_data[5]);
  status_padc.stmsg+=tmpstr;

  sprintf(tmpstr,"sens500_FE: %.2e bar\n",calib_data[6]);
  status_padc.stmsg+=tmpstr;

  sprintf(tmpstr,"sensFE_Vcc: %4.2f V\n",calib_data[7]);
  status_padc.stmsg+=tmpstr;

  sprintf(tmpstr,"PADC_Vcc: %4.2f V\n",calib_data[8]);
  status_padc.stmsg+=tmpstr;

  sprintf(tmpstr,"PADC_Vdd: %4.2f V\n",calib_data[9]);
  status_padc.stmsg+=tmpstr;

// Fill up xml message
  status_padc.xmlmsg+="<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
  status_padc.xmlmsg+="<padc_status>\n";

  sprintf(tmpstr,"  <sens100A_HV>%.2e</sens100A_HV>\n",calib_data[0]);
  status_padc.xmlmsg+=tmpstr;
  sprintf(tmpstr,"  <sens100B_HV>%.2e</sens100B_HV>\n",calib_data[1]);
  status_padc.xmlmsg+=tmpstr;
  sprintf(tmpstr,"  <sens500_HV>%.2e</sens500_HV>\n",calib_data[2]);
  status_padc.xmlmsg+=tmpstr;
  sprintf(tmpstr,"  <sensHV_Vcc>%4.2f</sensHV_Vcc>\n",calib_data[3]);
  status_padc.xmlmsg+=tmpstr;

  sprintf(tmpstr,"  <sens100A_FE>%.2e</sens100A_FE>\n",calib_data[4]);
  status_padc.xmlmsg+=tmpstr;
  sprintf(tmpstr,"  <sens100B_FE>%.2e</sens100B_FE>\n",calib_data[5]);
  status_padc.xmlmsg+=tmpstr;
  sprintf(tmpstr,"  <sens500_FE>%.2e</sens500_FE>\n",calib_data[6]);
  status_padc.xmlmsg+=tmpstr;
  sprintf(tmpstr,"  <sensFE_Vcc>%4.2f</sensFE_Vcc>\n",calib_data[7]);
  status_padc.xmlmsg+=tmpstr;

  sprintf(tmpstr,"  <PADC_Vcc>%4.2f</PADC_Vcc>\n",calib_data[8]);
  status_padc.xmlmsg+=tmpstr;
  sprintf(tmpstr,"  <PADC_Vdd>%4.2f</PADC_Vdd>\n",calib_data[9]);
  status_padc.xmlmsg+=tmpstr;

// Check sens100A_HV for errors
  if (fabs(s100_HV_mean)>Pcref->ref_padc.pMin) // Variation wrt average:
    PRelVar=(calib_data[0]-s100_HV_mean)/s100_HV_mean;
  else
    PRelVar=0;

  if (fabs(calib_data[0])<Pcref->ref_padc.Sens100HvMin || fabs(calib_data[0])>Pcref->ref_padc.Sens100HvMax) {
    sprintf(tmpstr,"  sens100A_HV: out-of-range\n");status_padc.errmsg+=tmpstr;
    sprintf(tmpstr,"  <ErrMsg>sens100A_HV=%.2e is out-of-range</ErrMsg>\n",calib_data[0]); status_padc.xmlmsg+=tmpstr;
    status_padc.error=true;
  } else if (fabs(PRelVar)>Pcref->ref_padc.pRelVarMax) {
    sprintf(tmpstr,"  sens100A_HV: %.2f rel. variation wrt mean\n",PRelVar); status_padc.errmsg+=tmpstr;
    sprintf(tmpstr,"  <ErrMsg>sens100A_HV=%.2e large variation wrt mean</ErrMsg>\n",calib_data[0]); status_padc.xmlmsg+=tmpstr;
    status_padc.error=true;
  }

// Check sens100B_HV for errors
  if (fabs(s100_HV_mean)>Pcref->ref_padc.pMin) // Variation wrt average:
    PRelVar=(calib_data[1]-s100_HV_mean)/s100_HV_mean;
  else
    PRelVar=0;
  if (fabs(calib_data[1])<Pcref->ref_padc.Sens100HvMin || fabs(calib_data[1])>Pcref->ref_padc.Sens100HvMax) {
    sprintf(tmpstr,"  sens100B_HV: out-of-range\n");status_padc.errmsg+=tmpstr;
    sprintf(tmpstr,"  <ErrMsg>sens100B_HV=%.2e is out-of-range</ErrMsg>\n",calib_data[1]); status_padc.xmlmsg+=tmpstr;
    status_padc.error=true;
  } else if (fabs(PRelVar)>Pcref->ref_padc.pRelVarMax) {
    sprintf(tmpstr,"  sens100B_HV: %.2f rel. variation wrt mean\n",PRelVar); status_padc.errmsg+=tmpstr;
    sprintf(tmpstr,"  <ErrMsg>sens100B_HV=%.2e large variation wrt mean</ErrMsg>\n",calib_data[1]); status_padc.xmlmsg+=tmpstr;
    status_padc.error=true;
  }

// Check sens500_HV for errors
  if (fabs(s500_HV_mean)>Pcref->ref_padc.pMin) // Variation wrt average:
    PRelVar=(calib_data[2]-s500_HV_mean)/s500_HV_mean;
  else
    PRelVar=0;
  if (fabs(calib_data[2])<Pcref->ref_padc.Sens500HvMin || fabs(calib_data[2])>Pcref->ref_padc.Sens500HvMax) {
    sprintf(tmpstr,"  sens500_HV: out-of-range\n"); status_padc.errmsg+=tmpstr;
    sprintf(tmpstr,"  <ErrMsg>sens500_HV=%.2e is out-of-range</ErrMsg>\n",calib_data[2]); status_padc.xmlmsg+=tmpstr;
    status_padc.error=true;
  } else if (fabs(PRelVar)>Pcref->ref_padc.pRelVarMax) {
    sprintf(tmpstr,"  sens500_HV: %.2f rel. variation wrt mean\n",PRelVar); status_padc.errmsg+=tmpstr;
    sprintf(tmpstr,"  <ErrMsg>sens500_HV=%.2e large variation wrt mean</ErrMsg>\n",calib_data[2]); status_padc.xmlmsg+=tmpstr;
    status_padc.error=true;
  }

  if (calib_data[3]<Pcref->ref_padc.SensHvVccMin || calib_data[3]>Pcref->ref_padc.SensHvVccMax) {
    sprintf(tmpstr,"  sensHV_Vcc error\n"); status_padc.errmsg+=tmpstr;
    sprintf(tmpstr,"  <ErrMsg>sensHV_Vcc=%.2f</ErrMsg>\n",calib_data[3]); status_padc.xmlmsg+=tmpstr;
    status_padc.error=true;
  }


// Check sens100A_FE for errors
  if (fabs(s100_FE_mean)>Pcref->ref_padc.pMin) // Variation wrt average:
    PRelVar=(calib_data[4]-s100_FE_mean)/s100_FE_mean;
  else
    PRelVar=0;
  if (fabs(calib_data[4])<Pcref->ref_padc.Sens100FeMin || fabs(calib_data[4])>Pcref->ref_padc.Sens100FeMax) {
    sprintf(tmpstr,"  sens100A_FE: out-of-range\n"); status_padc.errmsg+=tmpstr;
    sprintf(tmpstr,"  <ErrMsg>sens100A_FE=%.2e is out-of-range</ErrMsg>\n",calib_data[4]); status_padc.xmlmsg+=tmpstr;
    status_padc.error=true;
  } else if (fabs(PRelVar)>Pcref->ref_padc.pRelVarMax) {
    sprintf(tmpstr,"  sens100A_FE: %.2f rel. variation wrt mean\n",PRelVar); status_padc.errmsg+=tmpstr;
    sprintf(tmpstr,"  <ErrMsg>sens100A_FE=%.2e large variation wrt mean</ErrMsg>\n",calib_data[4]); status_padc.xmlmsg+=tmpstr;
    status_padc.error=true;
  }


// Check sens100B_FE for errors
  if (fabs(s100_FE_mean)>Pcref->ref_padc.pMin) // Variation wrt average:
    PRelVar=(calib_data[5]-s100_FE_mean)/s100_FE_mean;
  else
    PRelVar=0;
  if (fabs(calib_data[5])<Pcref->ref_padc.Sens100FeMin || fabs(calib_data[5])>Pcref->ref_padc.Sens100FeMax) {
    sprintf(tmpstr,"  sens100B_FE: out-of-range\n"); status_padc.errmsg+=tmpstr;
    sprintf(tmpstr,"  <ErrMsg>sens100B_FE=%.2e is out-of-range</ErrMsg>\n",calib_data[5]); status_padc.xmlmsg+=tmpstr;
    status_padc.error=true;
  } else if (fabs(PRelVar)>Pcref->ref_padc.pRelVarMax) {
    sprintf(tmpstr,"  sens100B_FE: %.2f rel. variation wrt mean\n",PRelVar); status_padc.errmsg+=tmpstr;
    sprintf(tmpstr,"  <ErrMsg>sens100B_FE=%.2e large variation wrt mean</ErrMsg>\n",calib_data[4]); status_padc.xmlmsg+=tmpstr;
    status_padc.error=true;
  }


// Check sens500_FE for errors
  if (fabs(s500_FE_mean)>Pcref->ref_padc.pMin) // Variation wrt average:
    PRelVar=(calib_data[6]-s500_FE_mean)/s500_FE_mean;
  else
    PRelVar=0;
  if (fabs(calib_data[6])<Pcref->ref_padc.Sens500FeMin || fabs(calib_data[6])>Pcref->ref_padc.Sens500FeMax) {
    sprintf(tmpstr,"  sens500_FE: out-of-range\n"); status_padc.errmsg+=tmpstr;
    sprintf(tmpstr,"  <ErrMsg>sens500_FE=%.2e is out-of-range</ErrMsg>\n",calib_data[6]); status_padc.xmlmsg+=tmpstr;
    status_padc.error=true;
  } else if (fabs(PRelVar)>Pcref->ref_padc.pRelVarMax) {
    sprintf(tmpstr,"  sens500_FE: %.2f rel. variation wrt mean\n",PRelVar); status_padc.errmsg+=tmpstr;
    sprintf(tmpstr,"  <ErrMsg>sens500_FE=%.2e large variation wrt mean</ErrMsg>\n",calib_data[6]); status_padc.xmlmsg+=tmpstr;
    status_padc.error=true;
  }


  if (calib_data[7]<Pcref->ref_padc.SensFeVccMin || calib_data[7]>Pcref->ref_padc.SensFeVccMax) {
    sprintf(tmpstr,"  sensFE_Vcc error\n"); status_padc.errmsg+=tmpstr;
    sprintf(tmpstr,"  <ErrMsg>sensFE_Vcc=%.2f</ErrMsg>\n",calib_data[7]); status_padc.xmlmsg+=tmpstr;
    status_padc.error=true;
  }

  if (calib_data[8]<Pcref->ref_padc.PadcVccMin || calib_data[8]>Pcref->ref_padc.PadcVccMax) {
    sprintf(tmpstr,"  PADC_Vcc error\n"); status_padc.errmsg+=tmpstr;
    sprintf(tmpstr,"  <ErrMsg>PADC_Vcc=%.2f</ErrMsg>\n",calib_data[8]); status_padc.xmlmsg+=tmpstr;
    status_padc.error=true;
  }

  if (calib_data[9]<Pcref->ref_padc.PadcVddMin || calib_data[9]>Pcref->ref_padc.PadcVddMax) {
    sprintf(tmpstr,"  PADC_Vdd error\n"); status_padc.errmsg+=tmpstr;
    sprintf(tmpstr,"  <ErrMsg>PADC_Vdd=%.2f</ErrMsg>\n",calib_data[9]); status_padc.xmlmsg+=tmpstr;
    status_padc.error=true;
  }

  //added to avoid mc errors due to padcs
  status_padc.error=false;


  if (status_padc.error==true) {
    status_padc.xmlmsg+="  <status>error</status>\n";
    status_padc.stmsg+="Status: error\n";
  } else {
    status_padc.xmlmsg+="  <status>ok</status>\n";
    status_padc.stmsg+="Status: ok\n";
  }
  status_padc.xmlmsg+="</padc_status>\n";

  return;
}

// Reset status_padc struct (except averages)
// Author: A.Parenti, Jul25 2007
// Modified: AP, Nov04 2008
void Ci2c::status_PADC_reset() {
  if (this==NULL) return; // Object deleted -> return

  status_padc.p100hvA.init(PNMAX);
  status_padc.p100hvB.init(PNMAX);
  status_padc.p500hv.init(PNMAX);
  status_padc.p100feA.init(PNMAX);
  status_padc.p100feB.init(PNMAX);
  status_padc.p500fe.init(PNMAX);

  status_padc.error = status_padc.warning = false;
  status_padc.stmsg="";
  status_padc.errmsg="";
  status_padc.xmlmsg="";
}

// Reset status_padc struct (only averages)
// Author: A.Parenti, Nov03 2008
void Ci2c::status_PADC_reset_average() {
  if (this==NULL) return; // Object deleted -> return

  status_padc.p100hvA.clear();
  status_padc.p100hvB.clear();
  status_padc.p500hv.clear();
  status_padc.p100feA.clear();
  status_padc.p100feB.clear();
  status_padc.p500fe.clear();
}


// Print result of "PADC Status" to stdout
// Author: A. Parenti, Jul25 2007
// Modified: AP, Oct08 2007
void Ci2c::status_PADC_print() {
  if (this==NULL) return; // Object deleted -> return

// Set font color
  if (status_padc.error) std::cout << REDBKG; // Error
  else if (status_padc.warning) std::cout << YLWBKG; // Warning
  else std::cout << GRNBKG; // All OK

  printf("%s",status_padc.stmsg.c_str());
  printf("Messages:\n%s",status_padc.errmsg.c_str());

// Unset font color
  std::cout << STDBKG << std::endl; // Unset font color
}


/**************************/
/* Save PADC status to DB */
/**************************/

// Save PADC status to DB
// Author: A. Parenti, Oct13 2006
// Modified/Tested: AP, Oct30 2007
int Ci2c::PADC_status_to_db(Cref *Pcref, short adc_count[10]) {
  if (this==NULL) return -1; // Object deleted -> return

#ifdef __USE_CCBDB__ // DB access enabled
  char adccountstr[256]="";
  int i, err;
  float calib_data[10];

  padcstatus *myPadcs;
  if (cmd_obj->dtdbobj==NULL) {
    myPadcs=new padcstatus; // Create a new connection to DB
  } else {
    myPadcs=new padcstatus(cmd_obj->dtdbobj); // Use an existing connection to DB
  }

  err=use_PADC_LUT(adc_count,calib_data); // Convert digits to pressure/voltage
  status_PADC(Pcref,calib_data); // Check PADC status


// Create addcount string
  for (i=0;i<10;++i)
    sprintf(adccountstr,"%s %d",adccountstr,adc_count[i]);

// Write status to DB
  myPadcs->insert(cmd_obj->read_ccb_id(),adccountstr,(char*)status_padc.xmlmsg.c_str());

  delete myPadcs;
  return 0;
#else
  nodbmsg();
  return -1;
#endif
}

/*************************************/
/* "RPC I2C commands" (0x64) command */
/*************************************/

// Send "RPC I2C commands" (0x64) command and decode reply
// Author: A. Parenti, Jan16 2005
// AP: Tested at Cessy, Jul08 2005
// Modified: AP, Aug11 2006
void Ci2c::rpc_I2C(short ctrl_data[I2C_DATA_MAX], char size) {
  const unsigned char command_code=0x64;
  unsigned char *command_line;
  int i, send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// Check "size"
  if (size>I2C_DATA_MAX)
    size=I2C_DATA_MAX;
  if (size<I2C_DATA_MIN)
    size=I2C_DATA_MIN;

// create the command line
  command_line = new unsigned char [1+2*size];
  command_line[0]=command_code;
  for (i=0;i<size;++i)
    wstring(ctrl_data[i],1+2*i,command_line);

  rpc_I2C_reset(); // Reset rpc_i2c struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1+2*size,RPC_I2C_TIMEOUT);

  delete[] command_line;

  if (send_command_result>=0) { // alright
    rpc_i2c.size = size;
    idx = rpc_I2C_decode(cmd_obj->data_read); // Decode reply
  }
  else { // copy error code
    rpc_i2c.ccbserver_code = send_command_result;
  }

  if (_verbose_)
    printf("\nRPC I2C commands (0x64): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset rpc_i2c struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec06 2006
void Ci2c::rpc_I2C_reset() {
  int i;

  if (this==NULL) return; // Object deleted -> return

  rpc_i2c.code=rpc_i2c.size=0;
  for (i=0;i<I2C_DATA_MAX;++i)
    rpc_i2c.err_data[i]=0;

  rpc_i2c.ccbserver_code=-5;
  rpc_i2c.msg="";
}


// Decode "RPC I2C commands" (0x64) reply
// Author: A. Parenti, Feb03 2005
// Modified: AP, Dec06 2006
int Ci2c::rpc_I2C_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x65;
  unsigned char code2;
  int i, idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&rpc_i2c.code);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  for (i=0;i<rpc_i2c.size;++i)
    idx = rstring(rdstr,idx,rpc_i2c.err_data+i);

// Fill-up the error code
  if (rpc_i2c.code==CCB_BUSY_CODE) // ccb is busy
    rpc_i2c.ccbserver_code = -1;
  else if (rpc_i2c.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    rpc_i2c.ccbserver_code = 1;
  else if (rpc_i2c.code==right_reply) // ccb: right reply
    rpc_i2c.ccbserver_code = 0;
  else // wrong ccb reply
    rpc_i2c.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

   rpc_i2c.msg="\nRPC I2C commands (0x64)\n";

  if (rpc_i2c.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(rpc_i2c.ccbserver_code)); rpc_i2c.msg+=tmpstr;
  } else {

    sprintf(tmpstr,"code: %#2X\n", rpc_i2c.code); rpc_i2c.msg+=tmpstr;
    for (i=0;i<rpc_i2c.size;++i) {
      sprintf(tmpstr,"err_data[%d]: %#2X\n",i,rpc_i2c.err_data[i]); rpc_i2c.msg+=tmpstr;
    }
  }

  return idx;
}


// Print results of "RPC I2C commands" (0x64) command to standard output
// Author:  A.Parenti Jan16 2005
// Modified: AP, Dec06 2006
void Ci2c::rpc_I2C_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",rpc_i2c.msg.c_str());
}


/*************************************************/
/* "Led allignement I2C commands" (0x66) command */
/*************************************************/

// Send "Led allignement I2C commands" (0x66) command and decode reply
// Author: A. Parenti, Jan16 2005
// AP: Tested at Cessy, Jul08 2005
// Modified: AP, Aug11 2006
void Ci2c::led_I2C(short ctrl_data[I2C_DATA_MAX], char size) {
  const unsigned char command_code=0x66;
  unsigned char *command_line;
  int i, send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// Check "size"
  if (size>I2C_DATA_MAX)
    size=I2C_DATA_MAX;
  if (size<I2C_DATA_MIN)
    size=I2C_DATA_MIN;

// create the command line
  command_line = new unsigned char [1+2*size];
  command_line[0]=command_code;
  for (i=0;i<size;++i)
    wstring(ctrl_data[i],1+2*i,command_line);

  led_I2C_reset(); // Reset led_i2c struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1+2*size,LED_I2C_TIMEOUT);

  delete[] command_line;

  if (send_command_result>=0) { // alright
    led_i2c.size = size;
    idx = led_I2C_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    led_i2c.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nLed allignement I2C commands (0x66): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset led_i2c struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec06 2006
void Ci2c::led_I2C_reset() {
  int i;

  if (this==NULL) return; // Object deleted -> return

  led_i2c.code=led_i2c.size=0;
  for (i=0;i<I2C_DATA_MAX;++i)
    led_i2c.err_data[i]=0;

  led_i2c.ccbserver_code=-5;
  led_i2c.msg="";
}


// Decode "Led allignement I2C commands" (0x66) reply
// Author: A. Parenti, Feb03 2005
// Modified: AP, Dec06 2006
int Ci2c::led_I2C_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x67;
  unsigned char code2;
  int i, idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&led_i2c.code);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  for (i=0;i<led_i2c.size;++i)
    idx = rstring(rdstr,idx,led_i2c.err_data+i);

// Fill-up the error code
  if (led_i2c.code==CCB_BUSY_CODE) // ccb is busy
    led_i2c.ccbserver_code = -1;
  else if (led_i2c.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    led_i2c.ccbserver_code = 1;
  else if (led_i2c.code==right_reply) // ccb: right reply
    led_i2c.ccbserver_code = 0;
  else // wrong ccb reply
    led_i2c.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  led_i2c.msg="\nLed allignement I2C commands (0x66)\n";
  if (led_i2c.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(led_i2c.ccbserver_code)); led_i2c.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code: %#2X\n", led_i2c.code); led_i2c.msg+=tmpstr;
    for (i=0;i<led_i2c.size;++i) {
      sprintf(tmpstr,"err_data[%d]: %#2X\n", i, led_i2c.err_data[i]); led_i2c.msg+=tmpstr;
    }
  }

  return idx;
}


// Print results of "Led allignement I2C commands" (0x66) command to standard output
// Author:  A.Parenti Jan16 2005
// Modified: AP, Dec06 2006
void Ci2c::led_I2C_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",led_i2c.msg.c_str());
}


/*****************************/
/* "Read ADC" (0x69) command */
/*****************************/

// Send "Read ADC" (0x69) command and decode reply
// Author: A. Parenti, Jan17 2005
// Modified: AP, Mar24 2005
// Tested at Cessy, Jul09 2005
void Ci2c::read_ADC() {
  const unsigned char command_code=0x69;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

  read_ADC_reset(); // Reset read_adc struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,READ_ADC_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = read_ADC_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    read_adc.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nRead ADC (0x69): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset read_adc struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec06 2006
void Ci2c::read_ADC_reset() {
  int i;

  if (this==NULL) return; // Object deleted -> return

  read_adc.code=0;
  for (i=0;i<32;++i)
    read_adc.adc[i]=0;

  read_adc.ccbserver_code=-5;
  read_adc.msg="";
}


// Decode "Read ADC" (0x69) reply
// Author: A. Parenti, Feb03 2005
// Modified: AP, Dec06 2006
int Ci2c::read_ADC_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x6A;
  unsigned char code2;
  int i, idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&read_adc.code);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  for (i=0;i<32;++i)
    idx = rstring(rdstr,idx,read_adc.adc+i);

// Fill-up the error code
  if (read_adc.code==CCB_BUSY_CODE) // ccb is busy
    read_adc.ccbserver_code = -1;
  else if (read_adc.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    read_adc.ccbserver_code = 1;
  else if (read_adc.code==right_reply) // ccb: right reply
    read_adc.ccbserver_code = 0;
  else // wrong ccb reply
    read_adc.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  read_adc.msg="\nRead ADC (0x69)\n";
  if (read_adc.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(read_adc.ccbserver_code)); read_adc.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code: %#2X\n", read_adc.code); read_adc.msg+=tmpstr;
    for (i=0;i<32;++i) {
      sprintf(tmpstr,"adc[%d]: %#2X\n", i, read_adc.adc[i]); read_adc.msg+=tmpstr;
    }
  }

  return idx;
}



// Print results of "Read ADC" (0x69) command to standard output
// Author:  A.Parenti Jan17 2005
// Modified: AP, Dec06 2006
void Ci2c::read_ADC_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",read_adc.msg.c_str());
}
