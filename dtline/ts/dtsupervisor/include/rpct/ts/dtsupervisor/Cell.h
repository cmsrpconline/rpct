#ifndef rpct_ts_dtsupervisor_Cell_h
#define rpct_ts_dtsupervisor_Cell_h

#include "ts/framework/CellAbstract.h"
#include "rpct/ts/dtsupervisor/CellContext.h"

namespace rpcttsdtsupervisor {

class Cell
    : public tsframework::CellAbstract
{
public:

    XDAQ_INSTANTIATOR();

    Cell(xdaq::ApplicationStub * _stub);
    ~Cell();

    void init();

    CellContext * getContext();
};

inline CellContext * Cell::getContext()
{
    CellContext * _context(dynamic_cast<CellContext *>(cellContext_));
    if (!_context)
        XCEPT_RAISE(tsexception::CellException, "The Cell Context is not of class CellContext");

    return _context;
}

} // namespace rpcttsdtsupervisor

#endif // rpct_ts_dtsupervisor_Cell_h
