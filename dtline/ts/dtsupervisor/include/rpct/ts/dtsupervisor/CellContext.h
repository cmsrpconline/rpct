#ifndef rpct_ts_dtsupervisor_CellContext_h
#define rpct_ts_dtsupervisor_CellContext_h

#include "rpct/ts/supervisor/XhannelSelectionCellContext.h"
#include "rpct/ts/supervisor/FebConfigurationCellContext.h"

#include "rpct/ts/worker/LoggerCellContext.h"

namespace rpcttsdtsupervisor {

class CellContext
    : public rpcttsworker::LoggerCellContext
    , public rpct::ts::supervisor::XhannelSelectionCellContext
    , public rpct::ts::supervisor::FebConfigurationCellContext
{
public:
    CellContext(log4cplus::Logger & _log, tsframework::CellAbstract * _cell);

    std::string getClassName();

    std::map<std::string, tsframework::CellXhannelCell*> getFebWorkers(bool _active_only = false);
};

inline std::string CellContext::getClassName()
{
    return "CellContext";
}

} // namespace rpcttsdtsupervisor

#endif // rpct_ts_dtsupervisor_CellContext_h
