#ifndef rpct_ts_dtsupervisor_Configuration_h
#define rpct_ts_dtsupervisor_Configuration_h

#include "ts/runcontrol/ConfigurationBase.h"

/*
#include "ts/itf/XTopNodeUI.h"
#include "ts/itf/XUserInterface.h"
#include "ts/itf/XOperationList.h"
*/

namespace rpcttsdtsupervisor {

class CellContext;

class Configuration
    : public tsframework::CellOperation // not using tsitf::ConfigurationBase to avoid TTCEnableMap and FEDMap
{
public:
    Configuration(log4cplus::Logger & _log, tsframework::CellAbstractContext * _context);
    ~Configuration();

    // tsitf::ConfigurationBase
    //std::string getDcsLhcParam(std::string const & _name);
    //tsitf::XOperationList & subsystems();

protected:
    // tsitf::ConfigurationBase
    virtual bool c_coldReset();
    virtual bool c_configure();
    virtual bool c_start();
    virtual bool c_stop();
    virtual bool c_pause();
    virtual bool c_resume();
    virtual void f_coldReset();
    virtual void f_configure();
    virtual void f_start();
    virtual void f_stop();
    virtual void f_pause();
    virtual void f_resume();
    virtual void executeSubsystems();

    // tsitf::ConfigurationBase
    virtual bool includeSubsystem(std::string const & _xhannel_name);

private:
    // tsitf::ConfigurationBase
    bool check_coldReset();
    bool check_configure();
    bool check_start();
    bool check_stop();
    bool check_pause();
    bool check_resume();
    void funct_coldReset();
    void funct_configure();
    void funct_start();
    void funct_stop();
    void funct_pause();
    void funct_resume();

public:
    // tsitf::ConfigurationBase
    void resetting();

protected:
    // tsitf::ConfigurationBase
    virtual bool addSubsystems(); // changed
    virtual void initTransitions(std::string const & _transition);

protected:
    // tsitf::ConfigurationBase
    void initVars(); // changed

protected:
    CellContext & context_;

    // tsitf::ConfigurationBase
/*    tsitf::XUserInterface ui_;
    tsitf::XTopNodeUI top_ui_;
    tsitf::XOperationList xoperations_;*/
};

} // namespace rpcttsdtsupervisor

#include "rpct/ts/dtsupervisor/Configuration-inl.h"

#endif // rpct_ts_dtsupervisor_Configuration_h
