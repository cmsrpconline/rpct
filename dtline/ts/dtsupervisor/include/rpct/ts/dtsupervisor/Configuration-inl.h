#ifndef rpct_ts_dtsupervisor_Configuration_inl_h
#define rpct_ts_dtsupervisor_Configuration_inl_h

#include "rpct/ts/dtsupervisor/Configuration.h"

namespace rpcttsdtsupervisor {

/*inline tsitf::XOperationList & Configuration::subsystems()
{
    return xoperations_;
}*/

inline bool Configuration::c_coldReset()
{
    return true;
}

inline bool Configuration::c_configure()
{
    return true;
}

inline bool Configuration::c_start()
{
    return true;
}

inline bool Configuration::c_stop()
{
    return true;
}

inline bool Configuration::c_pause()
{
    return true;
}

inline bool Configuration::c_resume()
{
    return true;
}

inline void Configuration::f_coldReset()
{
    executeSubsystems();
}

inline void Configuration::f_configure()
{
    executeSubsystems();
}

inline void Configuration::f_start()
{
    executeSubsystems();
}

inline void Configuration::f_stop()
{
    executeSubsystems();
}

inline void Configuration::f_pause()
{
    executeSubsystems();
}

inline void Configuration::f_resume()
{
    executeSubsystems();
}

inline void Configuration::executeSubsystems()
{
    //setResult(subsystems().autoExec());
}

inline bool Configuration::includeSubsystem(std::string const &)
{
    return false;
}

inline bool Configuration::check_coldReset()
{
    initVars();
    return c_coldReset() && addSubsystems();
}

inline bool Configuration::check_configure()
{
    initVars();
    return c_configure() && addSubsystems();
}

inline bool Configuration::check_start()
{
    return c_start();
}

inline bool Configuration::check_stop()
{
    return c_stop();
}

inline bool Configuration::check_pause()
{
    return c_pause();
}

inline bool Configuration::check_resume()
{
    return c_resume();
}

inline void Configuration::funct_coldReset()
{
    initTransitions("coldReset");
    f_coldReset();
}

inline void Configuration::funct_configure()
{
    initTransitions("configure");
    f_configure();
}

inline void Configuration::funct_start()
{
    initTransitions("start");
    f_start();
}

inline void Configuration::funct_stop()
{
    initTransitions("stop");
    f_stop();
}

inline void Configuration::funct_pause()
{
    initTransitions("pause");
    f_pause();
}

inline void Configuration::funct_resume()
{
    initTransitions("resume");
    f_resume();
}

} // namespace rpcttsdtsupervisor

#endif // rpct_ts_dtsupervisor_Configuration_inl_h
