#ifndef _rpct_ts_dtsupervisor_version_h_
#define _rpct_ts_dtsupervisor_version_h_

#include "config/PackageInfo.h"

#define rpcttsdtsupervisor_VERSION_MAJOR 1
#define rpcttsdtsupervisor_VERSION_MINOR 0
#define rpcttsdtsupervisor_VERSION_PATCH 0


#define rpcttsdtsupervisor_VERSION_CODE PACKAGE_VERSION_CODE(rpcttsdtsupervisor_VERSION_MAJOR,rpcttsdtsupervisor_VERSION_MINOR,rpcttsdtsupervisor_VERSION_PATCH)
#ifndef rpcttsdtsupervisor_PREVIOUS_VERSIONS
#define rpcttsdtsupervisor_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(rpcttsdtsupervisor_VERSION_MAJOR,rpcttsdtsupervisor_VERSION_MINOR,rpcttsdtsupervisor_VERSION_PATCH)
#else
#define rpcttsdtsupervisor_FULL_VERSION_LIST  rpcttsdtsupervisor_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(rpcttsdtsupervisor_VERSION_MAJOR,rpcttsdtsupervisor_VERSION_MINOR,rpcttsdtsupervisor_VERSION_PATCH)
#endif

namespace rpcttsdtsupervisor
{
const std::string package  = "rpcttsdtsupervisor";
const std::string versions = rpcttsdtsupervisor_FULL_VERSION_LIST;
const std::string description = "RPC TS DT-Line Supervisor";
const std::string authors = "";
const std::string summary = "";
const std::string link = "";

config::PackageInfo getPackageInfo();
void checkPackageDependencies() throw (config::PackageInfo::VersionException);
std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
