#include "rpct/ts/dtsupervisor/Cell.h"

#include "ts/framework/CellOperationFactory.h"
#include "ts/framework/CellPanelFactory.h"

#include "rpct/ts/supervisor/FebConfigurationPanel.h"
#include "rpct/ts/supervisor/XhannelSelectionPanel.h"

#include "rpct/ts/dtsupervisor/Configuration.h"

#include "xcept/tools.h"

XDAQ_INSTANTIATOR_IMPL(rpcttsdtsupervisor::Cell)

namespace rpcttsdtsupervisor {

Cell::Cell(xdaq::ApplicationStub * _stub)
    : tsframework::CellAbstract(_stub)
{
    LOG4CPLUS_TRACE(getApplicationLogger(), "cell constructor");

    try {
        cellContext_ = new CellContext(getApplicationLogger(), this);
    } catch(xcept::Exception & _exception) {
        LOG4CPLUS_ERROR(getApplicationLogger(), "Error instantiating the CellContext. Find the bug and restart the executive. Exception found: "
                        << xcept::stdformat_exception_history(_exception));
    }

    LOG4CPLUS_TRACE(getApplicationLogger(), "cell constructor done");
}

Cell::~Cell()
{
    // tsframework::CellAbstract deletes cellContext_;
}

void Cell::init()
{
    LOG4CPLUS_TRACE(getApplicationLogger(), "cell init");

    getContext()->addImport("/rpct/ts/xworker/html/elements/elements.html");

    tsframework::CellOperationFactory * _operation_factory = getContext()->getOperationFactory();
    _operation_factory->add<Configuration>("Configuration");

    tsframework::CellPanelFactory * _panel_factory = getContext()->getPanelFactory();
    _panel_factory->add<rpct::ts::supervisor::XhannelSelectionPanel> ("Worker Control");
    _panel_factory->add<rpct::ts::supervisor::FebConfigurationPanel> ("FEB Control");

    LOG4CPLUS_TRACE(getApplicationLogger(), "cell init done");
}

} // namespace rpcttsdtsupervisor
