#include "rpct/ts/dtsupervisor/CellContext.h"

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

#include "ts/framework/CellXhannelCell.h"
#include "ts/framework/Level1Source.h"

namespace rpcttsdtsupervisor {

CellContext::CellContext(log4cplus::Logger & _log, tsframework::CellAbstract * _cell)
    : tsframework::CellAbstractContext(_log, _cell)
    , rpcttsworker::LoggerCellContext(_log, _cell)
    , rpct::ts::supervisor::XhannelSelectionCellContext(_log, _cell)
    , rpct::ts::supervisor::FebConfigurationCellContext(_log, _cell)
{
    LOG4CPLUS_TRACE(getLogger(), "cellcontext constructor");

    //logger_ = log4cplus::Logger::getInstance(getLogger().getName() + ".CellContext");

    getLevel1Source().setSubsystem("RPC");

    getXhannelSelection().setTitle("FSM and FEB Worker Control");
    getXhannelSelection().setMessage("Please leave the use of this panel to experts.");

    LOG4CPLUS_TRACE(getLogger(), "cellcontext constructor done");
}

std::map<std::string, tsframework::CellXhannelCell *> CellContext::getFebWorkers(bool _active_only)
{
    std::map<std::string, tsframework::CellXhannelCell*> _result;
    std::map<std::string, tsframework::CellXhannel *> _xhannels = getXhannelSelection().getXhannelList(!_active_only);
    tsframework::CellXhannelCell* _xhannel_cell(0);
    for(std::map<std::string, tsframework::CellXhannel*>::const_iterator _xhannel = _xhannels.begin()
            ; _xhannel != _xhannels.end() ; ++_xhannel)
        if ((_xhannel_cell = dynamic_cast<tsframework::CellXhannelCell*>(_xhannel->second))
            && _xhannel_cell->getTargetDescriptor()->getClassName() == "rpcttsdtworker::Cell")
            _result.insert(std::pair<std::string, tsframework::CellXhannelCell*>(_xhannel->first, _xhannel_cell));

    return _result;
}

} // namespace rpcttsdtsupervisor
