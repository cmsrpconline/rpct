#include "config/version.h"
#include "rpct/ts/dtsupervisor/version.h"

GETPACKAGEINFO(rpcttsdtsupervisor)

void rpcttsdtsupervisor::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
    CHECKDEPENDENCY(config);
}

std::set<std::string, std::less<std::string> > rpcttsdtsupervisor::getPackageDependencies()
{
    std::set<std::string, std::less<std::string> > dependencies;
    ADDDEPENDENCY(dependencies,config);
    return dependencies;
}
