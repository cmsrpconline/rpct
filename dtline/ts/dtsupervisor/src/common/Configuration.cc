#include "rpct/ts/dtsupervisor/Configuration.h"

#include "log4cplus/loggingmacros.h"

#include "ts/toolbox/Tools.h"
#include "xdata/UnsignedLong.h"
#include "xdata/Boolean.h"
/*
#include "ts/itf/Resources.h"
#include "ts/itf/XAction.h"
*/

#include "rpct/ts/dtsupervisor/CellContext.h"
#include "ts/framework/CellXhannelCell.h"

namespace rpcttsdtsupervisor {

Configuration::Configuration(log4cplus::Logger & _log, tsframework::CellAbstractContext * _context)
    : tsframework::CellOperation(_log, _context)
    , context_(dynamic_cast<CellContext &>(*_context))
/*    , ui_(this)
    , top_ui_(this)
    , xoperations_(ui_)*/
{
    logger_ = log4cplus::Logger::getInstance(_log.getName() + ".Configuration");

    // tsitf::ConfigurationBase
    //xoperations_.sequences("configure[*:110]");

    try {
        getFSM().addState("halted");
        getFSM().addState("configured");
        getFSM().addState("running");
        getFSM().addState("paused");
        getFSM().addTransition("halted",     "halted",     "coldReset", this, &Configuration::check_coldReset, &Configuration::funct_coldReset);
        getFSM().addTransition("halted",     "configured", "configure", this, &Configuration::check_configure, &Configuration::funct_configure);
        getFSM().addTransition("configured", "running",    "start",     this, &Configuration::check_start,     &Configuration::funct_start);
        getFSM().addTransition("running",    "configured", "stop",      this, &Configuration::check_stop,      &Configuration::funct_stop);
        getFSM().addTransition("running",    "paused",     "pause",     this, &Configuration::check_pause,     &Configuration::funct_pause);
        getFSM().addTransition("paused",     "running",    "resume",    this, &Configuration::check_resume,    &Configuration::funct_resume);
        getFSM().addTransition("paused",     "configured", "stop",      this, &Configuration::check_stop,      &Configuration::funct_stop);
        getFSM().setInitialState("halted");
        getFSM().reset();
    } catch (xcept::Exception & _e) {
        XCEPT_RETHROW(tsexception::CellException, "Failed to initialize the FSM!", _e);
    }

    //! Subsystem configuration key
    getParamList()["KEY"] = new xdata::String("");
    //! Subsystem run settings key
    getParamList()["RS_KEY"] = new xdata::String("");
    //! L1 configuration key
    getParamList()["TSC_KEY"] = new xdata::String("");
    getParamList()["L1_KEY"] = new xdata::String("");
    //! Run number
    getParamList()["Run Number"] = new xdata::UnsignedLong(0);
    getParamList()["TTCMap"] = new xdata::String("{dummy=0}");
    getParamList()["FEDMap"] = new xdata::String("0&0%");
    getParamList()["DCS_LHC_Flags"] = new xdata::String("");
    getParamList()["AutoMode"] = new xdata::Boolean(false);
    getParamList()["UsePrimaryTCDS" ] = new xdata::Boolean(true);
}

Configuration::~Configuration()
{
    // tsitf::ConfigurationBase
    //xoperations_.clear();
}

/*std::string Configuration::getDcsLhcParam(std::string const & _name)
{
    std::string _before = "%" + _name + "&";
    std::string _after = "%";
    // add a '%' in front to make the parsing easier and more reliable
    return tstoolbox::Tools::extractParam("%" + getParam("DCS_LHC_Flags"), _before, _after);
}*/

void Configuration::resetting()
{
    CellOperation::resetting();
    //xoperations_.clear();
    LOG4CPLUS_INFO(getLogger(), "Reset operation.");
}

bool Configuration::addSubsystems()
{
    std::map<std::string, tsframework::CellXhannel *> _xhannels(context_.getXhannelSelection().getXhannelList());
    for (std::map<std::string, tsframework::CellXhannel *>::const_iterator _xhannel = _xhannels.begin()
             ; _xhannel != _xhannels.end() ; ++_xhannel)
        if (_xhannel->second->getTargetDescriptor()->getClassName() == "rpcttsdtworker::Cell")
            {
                try {
                    //subsystems().add(_xhannel->first, "Configuration");
                } catch (xcept::Exception & _e) {
                    //publishError(_e.message());
                }
            }

    //return !hasErrors();
    return true;
}

void Configuration::initTransitions(std::string const & _transition)
{
/*    for (tsitf::XOperationList::ActionList::iterator _it(subsystems().transitions(_transition))
             ; _it != subsystems().actions().end() ; ++_it)
        _it->second->set(getParamList()).setNoParamLog();*/
}

void Configuration::initVars()
{
/*    tsitf::CellOperationBase::_init();
    xoperations_.clear();

    tsitf::Resources::property("state_run_number", getParam("Run Number"));
    tsitf::Resources::property("state_tsc_key", getParam("TSC_KEY"));
    tsitf::Resources::clearErrorsAndWarnings();*/
}

} // namespace rpcttsdtsupervisor
