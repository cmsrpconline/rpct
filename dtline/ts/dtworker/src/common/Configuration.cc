#include "rpct/ts/dtworker/Configuration.h"

#include "log4cplus/loggingmacros.h"

#include "xdata/UnsignedLong.h"

#include "ts/toolbox/Tools.h"
//#include "ts/itf/Resources.h"

#include "rpct/ts/dtworker/CellContext.h"
#include "ts/framework/CellXhannelCell.h"

namespace rpcttsdtworker {

Configuration::Configuration(log4cplus::Logger & _log, tsframework::CellAbstractContext * _context)
    : tsframework::CellOperation(_log, _context)
    , context_(dynamic_cast<CellContext &>(*_context))
{
    logger_ = log4cplus::Logger::getInstance(_log.getName() + ".Configuration");

    try {
        getFSM().addState("halted");
        getFSM().addState("configured");
        getFSM().addState("running");
        getFSM().addState("paused");
        getFSM().addTransition("halted",     "halted",     "coldReset", this, &Configuration::check_coldReset, &Configuration::funct_coldReset);
        getFSM().addTransition("halted",     "configured", "configure", this, &Configuration::check_configure, &Configuration::funct_configure);
        getFSM().addTransition("configured", "running",    "start",     this, &Configuration::check_start,     &Configuration::funct_start);
        getFSM().addTransition("running",    "configured", "stop",      this, &Configuration::check_stop,      &Configuration::funct_stop);
        getFSM().addTransition("running",    "paused",     "pause",     this, &Configuration::check_pause,     &Configuration::funct_pause);
        getFSM().addTransition("paused",     "running",    "resume",    this, &Configuration::check_resume,    &Configuration::funct_resume);
        getFSM().addTransition("paused",     "configured", "stop",      this, &Configuration::check_stop,      &Configuration::funct_stop);
        getFSM().setInitialState("halted");
        getFSM().reset();
    } catch (xcept::Exception & _e) {
        XCEPT_RETHROW(tsexception::CellException, "Failed to initialize the FSM!", _e);
    }

    //! Subsystem configuration key
    getParamList()["KEY"] = new xdata::String("");
    //! Subsystem run settings key
    getParamList()["RS_KEY"] = new xdata::String("");
    //! L1 configuration key
    getParamList()["TSC_KEY"] = new xdata::String("");
    getParamList()["L1_KEY"] = new xdata::String("");
    //! Run number
    getParamList()["Run Number"] = new xdata::UnsignedLong(0);
    getParamList()["TTCMap"] = new xdata::String("{dummy=0}");
    getParamList()["FEDMap"] = new xdata::String("0&0%");
    getParamList()["DCS_LHC_Flags"] = new xdata::String("");
    getParamList()["AutoMode"] = new xdata::Boolean(false);
    getParamList()["UsePrimaryTCDS" ] = new xdata::Boolean(true);
}

Configuration::~Configuration()
{}

void Configuration::resetting()
{
    CellOperation::resetting();
    LOG4CPLUS_INFO(getLogger(), "Reset operation.");

    context_.stop();
}

void Configuration::initVars()
{
/*    tsframework::CellOperation::_init();

    CellOperation::property("state_run_number", getParam("Run Number"));
    tsitf::Resources::property("state_tsc_key", getParam("TSC_KEY"));
    tsitf::Resources::clearErrorsAndWarnings();*/
}

} // namespace rpcttsdtworker
