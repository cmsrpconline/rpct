#include "config/version.h"
#include "rpct/ts/dtworker/version.h"

GETPACKAGEINFO(rpcttsdtworker)

void rpcttsdtworker::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
    CHECKDEPENDENCY(config);
}

std::set<std::string, std::less<std::string> > rpcttsdtworker::getPackageDependencies()
{
    std::set<std::string, std::less<std::string> > dependencies;
    ADDDEPENDENCY(dependencies,config);
    return dependencies;
}
