#include "rpct/ts/dtworker/Cell.h"

#include "ts/framework/CellOperationFactory.h"
#include "ts/framework/CellFactory.h"
#include "ts/framework/CellPanelFactory.h"

#include "rpct/ts/worker/FebConfigurationPanel.h"
#include "rpct/ts/worker/FebConfigurationCommandConfigure.h"
#include "rpct/ts/worker/FebConfigurationCommandLoadConfiguration.h"
#include "rpct/ts/worker/FebConfigurationCommandMonitor.h"

#include "rpct/ts/dtworker/Configuration.h"

#include "xcept/tools.h"

XDAQ_INSTANTIATOR_IMPL(rpcttsdtworker::Cell)

namespace rpcttsdtworker {

Cell::Cell(xdaq::ApplicationStub * _stub)
    : tsframework::CellAbstract(_stub)
{
    getApplicationLogger().setLogLevel(log4cplus::TRACE_LOG_LEVEL);
    LOG4CPLUS_TRACE(getApplicationLogger(), "cell constructor");

    try {
        cellContext_ = new rpcttsdtworker::CellContext(getApplicationLogger(), this);
    } catch(xcept::Exception & _exception) {
        LOG4CPLUS_ERROR(getApplicationLogger(), "Error instantiating the CellContext. Find the bug and restart the executive. Exception found: "
                        << xcept::stdformat_exception_history(_exception));
    }

    this->getApplicationInfoSpace()->fireItemAvailable("wheel", &getContext()->wheel_);
    this->getApplicationInfoSpace()->addItemChangedListener("wheel", this);

    LOG4CPLUS_TRACE(getApplicationLogger(), "cell constructor done");
}

Cell::~Cell()
{
    // tsframework::CellAbstract deletes cellContext_;
}

void Cell::init()
{
    LOG4CPLUS_TRACE(getApplicationLogger(), "cell init");

    getContext()->addImport("/rpct/ts/xworker/html/elements/elements.html");
    getContext()->init();

    tsframework::CellOperationFactory* _operation_factory = getContext()->getOperationFactory();
    _operation_factory->add<Configuration>("Configuration");

    tsframework::CellFactory* _command_factory = getContext()->getCommandFactory();
    _command_factory->add<rpct::ts::worker::FebConfigurationCommandConfigure>("RPCT","ConfigureFEBs");
    _command_factory->add<rpct::ts::worker::FebConfigurationCommandLoadConfiguration>("RPCT","LoadConfigurationFEBs");
    _command_factory->add<rpct::ts::worker::FebConfigurationCommandMonitor>("RPCT","MonitorFEBs");

    tsframework::CellPanelFactory* _panel_factory = getContext()->getPanelFactory();
    _panel_factory->add<rpct::ts::worker::FebConfigurationPanel> ("FEB Control");

    LOG4CPLUS_TRACE(getApplicationLogger(), "cell init done");
}

} // namespace rpcttsdtworker
