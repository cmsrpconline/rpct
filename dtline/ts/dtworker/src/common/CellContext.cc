#include "rpct/ts/dtworker/CellContext.h"

#include <stdexcept>

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Bag.h"

#include "ts/framework/CellAbstract.h"
#include "ts/framework/Level1Source.h"

#include "rpct/xdaqutils/CcbInfo.h"
#include "rpct/xdaqutils/DTLineFebInfo.h"
#include "rpct/xdaqutils/xdaqDTFebSystem.h"
#include "rpct/xdaqutils/xdaqDTFebSystemBuilder.h"
#include "rpct/xdaqtools/Timer.h"

#include "rpct/ts/dtworker/const.h"

namespace rpcttsdtworker {

CellContext::CellContext(log4cplus::Logger & _log, tsframework::CellAbstract * _cell)
    : tsframework::CellAbstractContext(_log, _cell)
    , rpcttsworker::LoggerCellContext(_log, _cell)
    , rpct::ts::worker::FebConfigurationCellContext(_log, _cell, "rpcttsdtsupervisor::Cell", 0)
    , hwmutex_(toolbox::BSem::FULL)
    , wheel_(0)
{
    // log4cplus::Logger::getRoot().setLogLevel(log4cplus::TRACE_LOG_LEVEL);
    logger_ = log4cplus::Logger::getInstance(_log.getName() + ".CellContext");

    febsystem_ = new rpct::xdaqutils::xdaqDTFebSystem(*_cell, getHardwareMutex());

    monitortimer_ = new rpct::xdaqtools::Timer(logger_, "FebMonitorTimer");

    getLevel1Source().setSubsystem("RPC");

    log4cplus::Logger::getInstance("DTI2c").setLogLevel(log4cplus::TRACE_LOG_LEVEL);

    LOG4CPLUS_TRACE(logger_, "cellcontext constructor done");
}

CellContext::~CellContext()
{
    delete monitortimer_;
    delete febsystem_;
}

void CellContext::init()
{
    rpct::xdaqutils::xdaqDTFebSystemBuilder _dtfebsystembuilder(*getCell());

    xdata::Vector<xdata::Bag<rpct::xdaqutils::DTLineFebInfo> > _febs;
    xdata::Vector<xdata::Bag<rpct::xdaqutils::CcbInfo> > _ccbs;
    _dtfebsystembuilder.getDTLineFebInfo(wheel_.value_, _febs);
    _dtfebsystembuilder.getCCBInfoFromFile(wheel_.value_, _ccbs);
    _dtfebsystembuilder.buildDTFebSystem(dynamic_cast<rpct::DTFebSystem &>(*febsystem_)
                                         , _ccbs
                                         , _febs);
    LOG4CPLUS_TRACE(logger_, "cellcontext init done");
}

rpcttsworker::FebConnectivityTest & CellContext::getFebConnectivityTest()
{
    throw std::logic_error("FebConnectivityTest is not implemented for the DT line");
};

void CellContext::start()
{
    try {
        monitortimer_->start();

        toolbox::TimeInterval _interval(preset::monitoring_timer_);
        toolbox::TimeVal _start = toolbox::TimeVal::gettimeofday();
        monitortimer_->scheduleAtFixedRate(*this, &CellContext::monitorcycle, "monitorcycle", _start, _interval, 0);
    } catch (toolbox::task::exception::Exception & _e) {
        LOG4CPLUS_ERROR(logger_, "Exception starting monitor timer: " << _e.what());
    }
}

void CellContext::stop()
{
    try {
        if (monitortimer_->isActive())
            monitortimer_->stop();
    } catch (toolbox::task::exception::Exception & _e) {
        LOG4CPLUS_WARN(logger_, "Exception stopping monitor timer: " << _e.what());
    }
}

int CellContext::monitorcycle(toolbox::task::TimerEvent &)
{
    LOG4CPLUS_TRACE(logger_, "called CellContext::monitorcycle");
    febsystem_->xmonitorcycle();
    return 0;
}

} // namespace rpcttsdtworker
