#ifndef rpct_ts_dtworker_CellContext_h
#define rpct_ts_dtworker_CellContext_h

#include <string>
#include <vector>

#include "ts/framework/CellAbstractContext.h"
#include "rpct/ts/worker/FebConfigurationCellContext.h"
#include "rpct/ts/worker/LoggerCellContext.h"

#include "xdata/Integer.h"
#include "toolbox/BSem.h"

namespace rpct {
namespace xdaqutils {
class xdaqFebSystem;
} // namespace xdaqutils
namespace xdaqtools {
class Timer;
} // namespace xdaqtools
} // namespace rpct

namespace rpcttsdtworker {

class CellContext
    : public rpcttsworker::LoggerCellContext
    , public rpct::ts::worker::FebConfigurationCellContext
{
public:
    CellContext(log4cplus::Logger & _log , tsframework::CellAbstract * _cell);
    ~CellContext();

    void init();

    std::string getClassName();

    int getWheel() const;

    toolbox::BSem & getHardwareMutex();
    rpct::xdaqutils::xdaqFebSystem & getFebSystem();
    rpcttsworker::FebConnectivityTest & getFebConnectivityTest();

    void start();
    void stop();

    int monitorcycle(toolbox::task::TimerEvent & _event);

protected:
    toolbox::BSem hwmutex_;

    rpct::xdaqutils::xdaqFebSystem * febsystem_;
    rpct::xdaqtools::Timer * monitortimer_;

public:
    xdata::Integer wheel_;
};

} // namespace rpcttsdtworker

#include "rpct/ts/dtworker/CellContext-inl.h"

#endif // rpct_ts_dtworker_CellContext_h
