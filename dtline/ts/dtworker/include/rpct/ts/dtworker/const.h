/** Filip Thyssen */

#ifndef rpct_ts_dtworker_const_h_
#define rpct_ts_dtworker_const_h_

namespace rpcttsdtworker {
namespace preset {
extern unsigned int monitoring_timer_;
} // namespace preset
} // namespace rpcttsdtworker

#endif // rpct_ts_dtworker_const_h_
