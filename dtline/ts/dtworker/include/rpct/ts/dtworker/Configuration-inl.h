#ifndef rpct_ts_dtworker_Configuration_inl_h
#define rpct_ts_dtworker_Configuration_inl_h

#include "rpct/ts/dtworker/Configuration.h"

#include "rpct/ts/dtworker/CellContext.h"
#include "rpct/ts/worker/FebConfiguration.h"

namespace rpcttsdtworker {

inline bool Configuration::check_coldReset()
{
    initVars();
    return true;
}

inline bool Configuration::check_configure()
{
    initVars();
    return true;
}

inline bool Configuration::check_start()
{
    return true;
}

inline bool Configuration::check_stop()
{
    return true;
}

inline bool Configuration::check_pause()
{
    return true;
}

inline bool Configuration::check_resume()
{
    return true;
}

inline void Configuration::funct_coldReset()
{}

inline void Configuration::funct_configure()
{
    context_.getFebConfiguration().getProgress().terminate();
    context_.getFebConfiguration().getProgress().resume();
    context_.getFebConfiguration().loadConfiguration("FEBS_DEFAULT", true, true);
}

inline void Configuration::funct_start()
{
    context_.getFebConfiguration().getProgress().terminate();
    context_.getFebConfiguration().getProgress().resume();
    context_.start();
}

inline void Configuration::funct_stop()
{
    context_.stop();
}

inline void Configuration::funct_pause()
{}

inline void Configuration::funct_resume()
{}

} // namespace rpcttsdtworker

#endif // rpct_ts_dtworker_Configuration_inl_h
