#ifndef _rpct_ts_dtworker_version_h_
#define _rpct_ts_dtworker_version_h_

#include "config/PackageInfo.h"

#define rpcttsdtworker_VERSION_MAJOR 1
#define rpcttsdtworker_VERSION_MINOR 0
#define rpcttsdtworker_VERSION_PATCH 0


#define rpcttsdtworker_VERSION_CODE PACKAGE_VERSION_CODE(rpcttsdtworker_VERSION_MAJOR,rpcttsdtworker_VERSION_MINOR,rpcttsdtworker_VERSION_PATCH)
#ifndef rpcttsdtworker_PREVIOUS_VERSIONS
#define rpcttsdtworker_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(rpcttsdtworker_VERSION_MAJOR,rpcttsdtworker_VERSION_MINOR,rpcttsdtworker_VERSION_PATCH)
#else
#define rpcttsdtworker_FULL_VERSION_LIST  rpcttsdtworker_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(rpcttsdtworker_VERSION_MAJOR,rpcttsdtworker_VERSION_MINOR,rpcttsdtworker_VERSION_PATCH)
#endif

namespace rpcttsdtworker
{
const std::string package  = "rpcttsdtworker";
const std::string versions = rpcttsdtworker_FULL_VERSION_LIST;
const std::string description = "RPC TS DT-Line Worker";
const std::string authors = "";
const std::string summary = "";
const std::string link = "";

config::PackageInfo getPackageInfo();
void checkPackageDependencies() throw (config::PackageInfo::VersionException);
std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif


