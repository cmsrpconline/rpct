#ifndef rpct_ts_dtworker_CellContext_inl_h
#define rpct_ts_dtworker_CellContext_inl_h

#include "rpct/ts/dtworker/CellContext.h"

#include "rpct/xdaqutils/xdaqFebSystem.h"

namespace rpcttsdtworker {

inline std::string CellContext::getClassName()
{
    return "CellContext";
}

inline int CellContext::getWheel() const
{
    return wheel_.value_;
}

inline toolbox::BSem & CellContext::getHardwareMutex()
{
    return hwmutex_;
}

inline rpct::xdaqutils::xdaqFebSystem & CellContext::getFebSystem()
{
    return *febsystem_;
}

} // namespace rpcttsdtworker

#endif // rpct_ts_dtworker_CellContext_inl_h
