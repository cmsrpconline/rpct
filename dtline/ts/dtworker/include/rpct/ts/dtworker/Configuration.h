#ifndef rpct_ts_dtworker_Configuration_h
#define rpct_ts_dtworker_Configuration_h

#include "ts/runcontrol/ConfigurationBase.h"

namespace rpcttsdtworker {

class CellContext;

class Configuration
    : public tsframework::CellOperation // not using tsitf::ConfigurationBase to avoid TTCEnableMap and FEDMap
{
public:
    Configuration(log4cplus::Logger & _log, tsframework::CellAbstractContext * _context);
    ~Configuration();

private:
    bool check_coldReset();
    bool check_configure();
    bool check_start();
    bool check_stop();
    bool check_pause();
    bool check_resume();
    void funct_coldReset();
    void funct_configure();
    void funct_start();
    void funct_stop();
    void funct_pause();
    void funct_resume();

public:
    void resetting();

protected:
    void initVars();

protected:
    CellContext & context_;
};

} // namespace rpcttsdtworker

#include "rpct/ts/dtworker/Configuration-inl.h"

#endif // rpct_ts_dtworker_Configuration_h
