#ifndef _XDAQRPCCONFIG_H_
#define _XDAQRPCCONFIG_H_


#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/exception/Exception.h" 
#include "xdaq/NamespaceURI.h"
 
#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"

#include "xdata/Serializable.h"
#include "xdata/rpctsoap/Serializer.h"
#include "xdata/String.h"
#include "xgi/Utils.h"
#include "xgi/Method.h"

#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/fsm/FailedEvent.h"

#include "rpct/xdaqlboxaccess/XdaqLBoxAccessClient.h"
#include "rpct/xdaqutils/XdaqDbServiceClient.h"

 
class XdaqRpcConfig: public xdaq::Application { 
private:
    rpct::xdaqutils::XdaqDbServiceClient dbServiceClient;
    rpct::XdaqLBoxAccessClient lboxAccessClient;
    void config(xgi::Output* out,int infoVectors[], std::string bar, std::string end);
    typedef std::map<int ,rpct::XdaqLBoxAccessClient::MassiveWriteResponseBag*> instanceReadMap;
    void ReadBack(xgi::Output* , instanceReadMap,int sum, int infoVector[]);
    bool flagHTML;
    
	toolbox::fsm::FiniteStateMachine fsm; 
	xdata::String stateName; 
	xdata::String globalConfKey;
public:
    static const char* RPCT_RPC_CONFIG_NS;
    static const char* RPCT_RPC_CONFIG_PREFIX;

    XDAQ_INSTANTIATOR();    
 
    XdaqRpcConfig(xdaq::ApplicationStub * s);      
    virtual ~XdaqRpcConfig();
                
    void Default(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
    void configDemo(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
                   
    xoap::MessageReference onRpcConfigDemo(xoap::MessageReference msg) throw (xoap::exception::Exception);;
    
    //
	// SOAP Callback trigger state change 
	//
	xoap::MessageReference fireEvent (xoap::MessageReference msg) 
		throw (xoap::exception::Exception);

	//
	// SOAP Callback to reset the state machine
	//
	xoap::MessageReference reset (xoap::MessageReference msg) 
		throw (xoap::exception::Exception);

	//
	// Finite State Machine action callbacks
	//
	void ConfigureAction (toolbox::Event::Reference e) 
		throw (toolbox::fsm::exception::Exception);
	
	void EnableAction (toolbox::Event::Reference e) 
		throw (toolbox::fsm::exception::Exception);
	
	void SuspendAction (toolbox::Event::Reference e) 
		throw (toolbox::fsm::exception::Exception);
	
	void ResumeAction (toolbox::Event::Reference e) 
		throw (toolbox::fsm::exception::Exception);

	void HaltAction (toolbox::Event::Reference e) 
		throw (toolbox::fsm::exception::Exception);
	
	void failedTransition (toolbox::Event::Reference e) 
		throw (toolbox::fsm::exception::Exception);
	
	//
	// Finite State Machine callback for entring state
	//
	void stateChanged (toolbox::fsm::FiniteStateMachine & fsm) 
		throw (toolbox::fsm::exception::Exception);		
};
        



#endif 
