/*
 *  Author: Michal Pietrusinski, William Whitacher & Giovanni Polese
 *  Version: $Id: XdaqRpcConfig.cpp,v 1.22 2009/10/10 15:44:00 tb Exp $
 *
 */

#include "XdaqRpcConfig.h"

#include "xdaq/ApplicationGroup.h"

#include "xdata/soap/NamespaceURI.h"
#include "xdata/Serializable.h"
#include "xdata/exception/Exception.h"
#include "xdata/XStr.h"
#include "xdata/Float.h"
#include "xdata/Bag.h"
#include "xdata/Boolean.h"
#include "xdata/Integer.h"

#include "xoap/domutils.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "rpct/devices/System.h"
#include "rpct/devices/XmlSystemBuilder.h"
#include <log4cplus/configurator.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <time.h>
#include <string>

using namespace std;
using namespace rpct;
using namespace rpct::xdaqutils;
using namespace cgicc;


XDAQ_INSTANTIATOR_IMPL(XdaqRpcConfig);


const char* XdaqRpcConfig::RPCT_RPC_CONFIG_NS = "urn:rpct-rpc-config:1.0";
const char* XdaqRpcConfig::RPCT_RPC_CONFIG_PREFIX = "rrc";


XdaqRpcConfig::XdaqRpcConfig(xdaq::ApplicationStub * s)
        : xdaq::Application(s), dbServiceClient(this), lboxAccessClient(this) {

    LOG4CPLUS_INFO(getApplicationLogger(),"Hello World!");
    //getApplicationLogger().setLogLevel(log4cplus::DEBUG_LOG_LEVEL);
    flagHTML = false;
    xgi::bind(this,&XdaqRpcConfig::Default, "Default");
    xgi::bind(this,&XdaqRpcConfig::configDemo, "configDemo");

    xoap::bind(this, &XdaqRpcConfig::onRpcConfigDemo, "rpcConfigDemo", RPCT_RPC_CONFIG_NS);

    ////////////////////// F S M /////////////////////////
    xoap::bind(this, &XdaqRpcConfig::fireEvent, "Configure", XDAQ_NS_URI );
    xoap::bind(this, &XdaqRpcConfig::fireEvent, "Start", XDAQ_NS_URI );
    xoap::bind(this, &XdaqRpcConfig::fireEvent, "Pause", XDAQ_NS_URI );
    xoap::bind(this, &XdaqRpcConfig::fireEvent, "Resume", XDAQ_NS_URI );
    xoap::bind(this, &XdaqRpcConfig::fireEvent, "Halt", XDAQ_NS_URI );
    xoap::bind(this, &XdaqRpcConfig::reset, "Reset", XDAQ_NS_URI );


    fsm.addState('H', "Halted", this, &XdaqRpcConfig::stateChanged);
    fsm.addState('C', "Configured", this, &XdaqRpcConfig::stateChanged);
    fsm.addState('R', "Running", this, &XdaqRpcConfig::stateChanged);
    fsm.addState('P', "Paused", this, &XdaqRpcConfig::stateChanged);

    fsm.addStateTransition('H', 'C', "Configure", this, &XdaqRpcConfig::ConfigureAction);
    fsm.addStateTransition('C', 'R', "Start", this,&XdaqRpcConfig::EnableAction);
    fsm.addStateTransition('R', 'P', "Pause",this, &XdaqRpcConfig::SuspendAction);
    fsm.addStateTransition('P', 'R', "Resume",this, &XdaqRpcConfig::ResumeAction);

    fsm.addStateTransition('H', 'H', "Halt", this, &XdaqRpcConfig::HaltAction);
    fsm.addStateTransition('C', 'H', "Halt", this, &XdaqRpcConfig::HaltAction);
    fsm.addStateTransition('R', 'H', "Halt", this, &XdaqRpcConfig::HaltAction);
    fsm.addStateTransition('P', 'H', "Halt", this, &XdaqRpcConfig::HaltAction);

    // Failure state setting
    fsm.setFailedStateTransitionAction(this, &XdaqRpcConfig::failedTransition);
    fsm.setFailedStateTransitionChanged(this, &XdaqRpcConfig::stateChanged);

    fsm.setInitialState('H');
    fsm.setStateName('F', "Failed"); // give a name to the 'F' state

    fsm.reset();

    // Export a "State" variable that reflects the state of the state machine
    getApplicationInfoSpace()->fireItemAvailable("stateName", &stateName);
    getApplicationInfoSpace()->fireItemAvailable("GLOBAL_CONF_KEY", &globalConfKey);

    ///////////////////////// F S M   E N D ////////////////////

}

XdaqRpcConfig::~XdaqRpcConfig() {
}



void XdaqRpcConfig::ReadBack( xgi::Output* out,instanceReadMap instanceForReadLBBox,int sum, int infoVector[]){


	LOG4CPLUS_INFO(getApplicationLogger(),"New Size Endcap:");
	//int csa1=0;


	*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
    *out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
    *out << cgicc::title("Command Receive Message") << std::endl;
    std::string url = "/";
    url += getApplicationDescriptor()->getURN();
    url += "/Default";


    *out <<cgicc::h2().set("align","center").set("font color","#347C17").set("font size","6")<< "RPC Configuration Completed" << cgicc::h2()<< std::endl;

    *out <<cgicc::h4().set("align","left").set("font color","#0000FF").set("font size","6")<< "Total Number of Febs configured: "<< sum<<cgicc::h4()<< std::endl;
 	*out << cgicc::a("Return to settings menu").set("href",url) << std::endl;



// INSERISCI QUI LA TABELLA DI VISUALIZZAZIONE

ofstream prova;
time_t rawtime;
time(&rawtime);
const char* nome = "febSet.list";
prova.open(nome,ios::app);
prova<<ctime(&rawtime)<<endl;

 ofstream tempfile;
 tempfile.open("tempfile.txt", ios::app);
 int tempInfoVectorInfo[4] = {7,8,14,15};
 time_t t0=time(NULL);

for(instanceReadMap::iterator iMap = instanceForReadLBBox.begin(); iMap != instanceForReadLBBox.end(); ++iMap){

MassiveReadResponse& readResponse = instanceForReadLBBox[iMap->first]->bag;
    FebProperties& props = readResponse.getProperties();
    	*out << "<p>" << std::endl;
 		*out << cgicc::table().set("width", "600").set("align","left").set("border", "2").set("bordercolor", "#000066").set("cellpadding", "6").set("cellspacing", "2");
        *out << cgicc::tr();
        *out << cgicc::td();
        *out << cgicc::div().set("align", "center") << "Chamber" << cgicc::div();
        *out << cgicc::td();
        *out << cgicc::td();
        *out << cgicc::div().set("align", "center") << "Local Partition" << cgicc::div();
        *out << cgicc::td();
        *out << cgicc::td();
        *out << cgicc::div().set("align", "center") << "CCU Address" << cgicc::div();
        *out << cgicc::td();
        *out << cgicc::td();
        *out << cgicc::div().set("align", "center") << "CB Channel" << cgicc::div();
        *out << cgicc::td();
        *out << cgicc::td();
        *out << cgicc::div().set("align", "center") << "I2C Local Number" << cgicc::div();
        *out << cgicc::td();
        for (FebProperties::iterator iProp = props.begin(); iProp != props.end(); ++iProp) {
            *out << cgicc::td();
            *out << cgicc::div().set("align", "center") << (string)(*iProp) << cgicc::div();
            *out << cgicc::td();
        }
        *out << cgicc::tr();

        FebValuesVector& valuesVector = readResponse.getFebValues();
        for (FebValuesVector::iterator iVal = valuesVector.begin(); iVal != valuesVector.end(); ++iVal) {
            FebValues& febValues = iVal->bag;
            FebInfo& febInfo = febValues.getFeb().bag;
	        *out << cgicc::tr()
	        	 << cgicc::td()
	        	 << cgicc::div().set("align", "center") << (string)febInfo.getChamberLocationName() << cgicc::div()
	        	 << cgicc::td()
	        	 << cgicc::td()
	        	 << cgicc::div().set("align", "center") << (string)febInfo.getFebLocalEtaPartition() << cgicc::div()
	        	 << cgicc::td()
	        	 << cgicc::td()
	        	 << cgicc::div().set("align", "center") << febInfo.getCcuAddress() << cgicc::div()
	        	 << cgicc::td()
	        	 << cgicc::td()
	        	 << cgicc::div().set("align", "center") << febInfo.getChannel() << cgicc::div()
	        	 << cgicc::td()
	        	 << cgicc::td()
	        	 << cgicc::div().set("align", "center") << febInfo.getAddress() << cgicc::div()
	        	 << cgicc::td();
			prova<<(string)febInfo.getChamberLocationName()<<"\t";
            FebValues::Values& values = febValues.getValues();
	    int i=0;
            for (FebValues::Values::iterator iValue = values.begin(); iValue != values.end(); ++iValue) {
            	 prova<<(*iValue)<<"\t";
                *out << cgicc::td() << cgicc::div().set("align", "center") << (*iValue) << cgicc::div() << cgicc::td();

		if (i<2 || (febInfo.isEndcap() && i<4))
		  tempfile << t0 << " " << infoVector[18] << " " << febInfo.getFebId() << " " << i << " " << *iValue << " " << ((*iValue)-infoVector[tempInfoVectorInfo[i]]) << std::endl;
		i++;
		  
            }
            *out << cgicc::tr();

            prova<<endl;
        }


        FebErrorsVector& errorsVector = readResponse.getFebErrors();
        for (FebErrorsVector::iterator iErr = errorsVector.begin(); iErr != errorsVector.end(); ++iErr) {
            FebErrors& febErrors = iErr->bag;
            FebInfo& febInfo = febErrors.getFeb().bag;
            *out << cgicc::tr()
                 << cgicc::td()
                 << cgicc::div().set("align", "center") << (string)febInfo.getChamberLocationName() << cgicc::div()
                 << cgicc::td()
                 << cgicc::td()
                 << cgicc::div().set("align", "center") << (string)febInfo.getFebLocalEtaPartition() << cgicc::div()
                 << cgicc::td()
                 << cgicc::td()
                 << cgicc::div().set("align", "center") << febInfo.getCcuAddress() << cgicc::div()
                 << cgicc::td()
                 << cgicc::td()
                 << cgicc::div().set("align", "center") << febInfo.getChannel() << cgicc::div()
                 << cgicc::td()
                 << cgicc::td()
                 << cgicc::div().set("align", "center") << febInfo.getAddress() << cgicc::div()
                 << cgicc::td();
            prova<<(string)febInfo.getChamberLocationName() << "\t" << febErrors.getError() << "\t";
            *out << cgicc::td().set("colspan", rpct::toString(props.size())) << cgicc::div().set("align", "center") << febErrors.getError() << cgicc::div() << cgicc::td();
            *out << cgicc::tr();

            prova<<endl;
        }

        *out<< cgicc::table();
		delete instanceForReadLBBox[iMap->first];
}
//delete lastMassiveWriteResponse;

flagHTML=false;
prova<<endl;
prova.close();

 tempfile.close();
}
void XdaqRpcConfig::Default(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception) {
  std::string mainTitle = std::string("RPC Configuration Program");

  *out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
  *out << cgicc::html().set("lang","en").set("dir","ltr") << std::endl;
  *out << cgicc::head() << std::endl;
  *out << cgicc::title(mainTitle) << std::endl;
  *out << cgicc::style() << cgicc::comment() << std::endl;
  
  *out << "body{font-size:14px;font-family:Arial, Times New Roman, Times, Serif;}" << std::endl;
  *out << "hr{border:0px solid #204a87;height:1px;background-color:#204a87;}" << std::endl;
  *out << "label.before{display:block;float:left;width:100px;padding:5px;text-align:right;}" << std::endl;
  *out << "select, input.after{width:100px;}" << std::endl;
  *out << "table{border:0;}" << std::endl;
  *out << "table.form, table.form td{border:0;align:center;}" << std::endl;
  *out << "h1,h2,h3{color:#204a87;font-weight:bold;}" << std::endl;
  *out << "h2,h3{color:#204a87;font-weight:bold;border-bottom:1px dotted #204a87;}" << std::endl;
  *out << "h1{font-size:18px;}" << std::endl;
  *out << "h2{font-size:16px;}" << std::endl;
  *out << "h3{font-size:16px;font-style:italic;}" << std::endl;
  *out << "a{color:#204a87;text-decoration:none;font-weight:normal;}" << std::endl;
  *out << "a:hover{color:#204a87;text-decoration:underline;font-weight:normal;}" << std::endl;
  
  *out << cgicc::comment() << cgicc::style() << std::endl;
  *out << cgicc::head() << std::endl;
  *out << cgicc::body() << std::endl;
  xgi::Utils::getPageHeader(
			    out,
			    mainTitle,
			    getApplicationDescriptor()->getContextDescriptor()->getURL(),
			    getApplicationDescriptor()->getURN(),
			    "/daq/xgi/images/Application.jpg"
			    );
  
  flagHTML = true;
  
  
  // HTML PAGE
  std::string url = "/";
  url += getApplicationDescriptor()->getURN();
  url += "/configDemo";
  
  *out << cgicc::form().set("method","POST").set("action", url).set("enctype","multipart/form-data") << std::endl;
  
  *out << cgicc::table().set("class","form").set("align","center") << std::endl;
  
  *out << cgicc::tr()<< std::endl;
  *out << cgicc::td().set("width", "300").set("cellspacing", "2").set("align","center") << std::endl;
  *out << cgicc::fieldset() << cgicc::legend() << "Barrel" << cgicc::legend() << std::endl;
  *out << cgicc::label().set("class","before").set("for","isCheckBarrel") << "barrel" << cgicc::label() << std::endl;
  *out << cgicc::input().set("type","checkbox").set("name","isCheckBarrel").set("id","isCheckBarrel").set("value","Barrel") << cgicc::br() << cgicc::br() << std::endl;
  *out << cgicc::label().set("class","before").set("for","wheel") << "wheel" << cgicc::label() << std::endl;
  *out << cgicc::select().set("name","wheel").set("id","wheel") << std::endl;
  *out << cgicc::option().set("value","99") << "all" << cgicc::option() << std::endl;
  for (int i=-2;i<=2;i++)
    *out << "<option value=\"" << i << "\">" << i << "</option>" << std::endl;
  *out << cgicc::select() << cgicc::br() << cgicc::br() << std::endl;

  *out << cgicc::label().set("class","before").set("for","bSector") << "sector" << cgicc::label() << std::endl;
  *out << cgicc::select().set("name","bSector").set("id","bSector") << std::endl;
  *out << cgicc::option().set("value","99") << "all" << cgicc::option() << std::endl;
  for (int i=1;i<=12;i++)
    *out << "<option value=\"" << i << "\">" << i << "</option>" << std::endl;
  *out << cgicc::select() << cgicc::br() << cgicc::br() << std::endl;

  *out << cgicc::label().set("class","before").set("for","chamber") << "chamber" << cgicc::label() << std::endl;
  *out << cgicc::select().set("name","chamber").set("id","chamber") << std::endl;
  *out << cgicc::option().set("value","99") << "all" << cgicc::option() << std::endl;
  *out << "    <option value=\"1\">RB1in</option>" << std::endl;
  *out << "    <option value=\"2\">RB1out</option>" << std::endl;
  *out << "    <option value=\"3\">RB2in</option>" << std::endl;
  *out << "    <option value=\"4\">RB2out</option>" << std::endl;
  *out << "    <option value=\"5\">RB3-</option>" << std::endl;
  *out << "    <option value=\"6\">RB3+</option>" << std::endl;
  *out << "    <option value=\"7\">RB4-</option>" << std::endl;
  *out << "    <option value=\"8\">RB4+</option>" << std::endl;
  *out << "    <option value=\"9\">RB4--</option>" << std::endl;
  *out << "    <option value=\"10\">RB4++</option>" << std::endl;
  *out << "    <option value=\"11\">RB4</option>" << std::endl;
  *out << cgicc::select() << cgicc::br() << cgicc::br() << std::endl;

  *out << cgicc::label().set("class","before").set("for","posOnChambB") << "eta-partition" << cgicc::label() << std::endl;
  *out << cgicc::select().set("name","posOnChambB").set("id","posOnChambB") << std::endl;
  *out << cgicc::option().set("value","99") << "all" << cgicc::option() << std::endl;
  *out << "    <option value=\"1\">Forward</option>" << std::endl;
  *out << "    <option value=\"2\">Central</option>" << std::endl;
  *out << "    <option value=\"3\">Backward</option>" << std::endl;
  *out << cgicc::select() << cgicc::br() << cgicc::br() << std::endl;

  *out << cgicc::label().set("class","before").set("for","febPos") << "Local Partition" << cgicc::label() << std::endl;
  *out << cgicc::select().set("name","febPos").set("id","febPos") << std::endl;
  //  *out << cgicc::option().set("value","99") << "all" << cgicc::option() << std::endl;
  for (int i=1;i<=6;i++)
    *out << "<option value=\"" << i << "\">" << (i-1) << "</option>" << std::endl;
  *out << cgicc::select() << cgicc::br() << cgicc::br() << std::endl;
  *out << cgicc::fieldset();
  *out << cgicc::td();


  *out << cgicc::td().set("width", "300").set("cellspacing", "2").set("align","center")<< std::endl;
  *out << cgicc::fieldset() << cgicc::legend() << "Endcap" << cgicc::legend() << std::endl;
  *out << cgicc::label().set("class","before").set("for","isCheckEndcap") << "endcap" << cgicc::label() << std::endl;
  *out << cgicc::input().set("type","checkbox").set("name","isCheckEndcap").set("id","isCheckEndcap").set("value","Endcap") << cgicc::br() << cgicc::br() << std::endl;
  *out << cgicc::label().set("class","before").set("for","disk") << "disk" << cgicc::label() << std::endl;
  *out << cgicc::select().set("name","disk").set("id","disk") << std::endl;
  *out << cgicc::option().set("value","99") << "all" << cgicc::option() << std::endl;
  for (int i=-4;i<=4;i++)
    if (i != 0)
      *out << "<option value=\"" << i << "\">" << i << "</option>" << std::endl;
  *out << cgicc::select() << cgicc::br() << cgicc::br() << std::endl;

  *out << cgicc::label().set("class","before").set("for","layer") << "layer" << cgicc::label() << std::endl;
  *out << cgicc::select().set("name","layer").set("id","layer") << std::endl;
  *out << cgicc::option().set("value","99") << "all" << cgicc::option() << std::endl;
  for (int i=1;i<=6;i++)
    *out << "<option value=\"" << i << "\">" << i << "</option>" << std::endl;
  *out << cgicc::select() << cgicc::br() << cgicc::br() << std::endl;

  *out << cgicc::label().set("class","before").set("for","eSector") << "sector" << cgicc::label() << std::endl;
  *out << cgicc::select().set("name","eSector").set("id","eSector") << std::endl;
  *out << cgicc::option().set("value","99") << "all" << cgicc::option() << std::endl;
  for (int i=1;i<=36;i++)
    *out << "<option value=\"" << i << "\">" << i << "</option>" << std::endl;
  *out << cgicc::select() << cgicc::br() << cgicc::br() << std::endl;

  *out << cgicc::label().set("class","before").set("for","posOnChambE") << "Eta Partition" << cgicc::label() << std::endl;
  *out << cgicc::select().set("name","posOnChambE").set("id","posOnChambE") << std::endl;
  *out << cgicc::option().set("value","99") << "all" << cgicc::option() << std::endl;
  *out << "    <option value=\"1\">A</option>" << std::endl;
  *out << "    <option value=\"2\">B</option>" << std::endl;
  *out << "    <option value=\"3\">C</option>" << std::endl;
  *out << cgicc::select() << cgicc::br() << cgicc::br() << std::endl;
  *out << cgicc::fieldset();  
  *out  << cgicc::td();
  
  *out << cgicc::tr();
  *out << cgicc::tr();

  *out << cgicc::td() << std::endl;
  *out << cgicc::fieldset() << cgicc::legend() << "Threshold (mV)" << cgicc::legend() << std::endl;
  *out << cgicc::label().set("for","VTH1").set("class","before") << "chip 1" << cgicc::label();
  *out << cgicc::input().set("type","text").set("class","after").set("name","VTH1").set("id","VTH1").set("value","210") << cgicc::br() << cgicc::br() << std::endl;
  *out << cgicc::label().set("for","VTH2").set("class","before") << "chip 2" << cgicc::label();
  *out << cgicc::input().set("type","text").set("class","after").set("name","VTH2").set("id","VTH2").set("value","210") << cgicc::br() << cgicc::br() << std::endl;
  *out << "only for endcap:" << cgicc::br() << std::endl;
  *out << cgicc::label().set("for","VTH3").set("class","before") << "chip 3" << cgicc::label();
  *out << cgicc::input().set("type","text").set("class","after").set("name","VTH3").set("id","VTH3").set("value","210") << cgicc::br() << cgicc::br() << std::endl;
  *out << cgicc::label().set("for","VTH4").set("class","before") << "chip 4" << cgicc::label();
  *out << cgicc::input().set("type","text").set("class","after").set("name","VTH4").set("id","VTH4").set("value","210") << cgicc::br() << cgicc::br() << std::endl;
  *out << cgicc::fieldset();
  *out << cgicc::td();
  
  *out << cgicc::td() << std::endl;
  *out << cgicc::fieldset() << cgicc::legend() << "Pulse width (mV)" << cgicc::legend() << std::endl;
  *out << cgicc::label().set("for","VMON1").set("class","before") << "chip 1" << cgicc::label();
  *out << cgicc::input().set("type","text").set("class","after").set("name","VMON1").set("id","VMON1").set("value","3500") << cgicc::br() << cgicc::br() << std::endl;
  *out << cgicc::label().set("for","VMON2").set("class","before") << "chip 2" << cgicc::label();
  *out << cgicc::input().set("type","text").set("class","after").set("name","VMON2").set("id","VMON2").set("value","3500") << cgicc::br() << cgicc::br() << std::endl;
  *out << "only for endcap:" << cgicc::br() << std::endl;
  *out << cgicc::label().set("for","VMON3").set("class","before") << "chip 3" << cgicc::label();
  *out << cgicc::input().set("type","text").set("class","after").set("name","VMON3").set("id","VMON3").set("value","3500") << cgicc::br() << cgicc::br() << std::endl;
  *out << cgicc::label().set("for","VMON4").set("class","before") << "chip 4" << cgicc::label();
  *out << cgicc::input().set("type","text").set("class","after").set("name","VMON4").set("id","VMON4").set("value","3500") << cgicc::br() << cgicc::br() << std::endl;
  *out << cgicc::fieldset();
  *out << cgicc::td();

  *out << cgicc::tr();
  *out << cgicc::tr();

  *out << cgicc::td().set("colspan","2");
  *out << cgicc::fieldset() << cgicc::legend() << "Correction" << cgicc::legend() << std::endl;
  *out << cgicc::input().set("type","radio").set("name","precision").set("value","0").set("checked","checked").set("id","precisionDefault");
  *out << cgicc::label().set("for","precisionDefault") << "Default" << cgicc::label() << cgicc::br() << std::endl;
  *out << cgicc::input().set("type","radio").set("name","precision").set("value","1").set("id","precisionOff");
  *out << cgicc::label().set("for","precisionOff") << "Off" << cgicc::label() << cgicc::br() << std::endl;
  *out << cgicc::input().set("type","radio").set("name","precision").set("value","2").set("id","precisionOn");
  *out << cgicc::label().set("for","precisionOn") << "On" << cgicc::label() << cgicc::br() << std::endl;
  *out << cgicc::fieldset() << std::endl; 
  *out << cgicc::td();

  *out << cgicc::tr();
  *out << cgicc::tr();
  
  *out << cgicc::td().set("colspan","2").set("align","center");
  *out << cgicc::input().set("type", "submit").set("align","center").set("name", "send").set("value", "Write on Febs");
  *out << cgicc::td();
  
  *out << cgicc::tr();
  *out<< cgicc::table();
  
  
  *out << cgicc::form() << std::endl;
  LOG4CPLUS_INFO(getApplicationLogger(),"Fine Default");
  xgi::Utils::getPageFooter(*out);
  
  *out << cgicc::body() << std::endl;
  *out << cgicc::html() << std::endl;
}



void XdaqRpcConfig::config(xgi::Output* out,int infoVector[], string bar, string end) {
    XdaqSoapAccess::TSOAPParamList paramList;

    // connect to DBService - get information about which Febs should be contacted
    // and how

    LOG4CPLUS_INFO(getApplicationLogger(),"F :"<< infoVector[1]<<infoVector[2]<< infoVector[3]<<infoVector[4]);
    LOG4CPLUS_INFO(getApplicationLogger(),"E :"<< infoVector[5]<<infoVector[6]<< infoVector[7]<<infoVector[8]<< infoVector[9]<<infoVector[10]);

    typedef XdaqDbServiceClient::FebAccessInfoVector FAIV;

    int boe = 0;
    int sum = 0;
    FAIV* febAccessInfoVector=0;
    FAIV* febAccessInfoVectorEndcap=0;

    if(bar == "Barrel")
    {
        boe ++;

        if(infoVector[4]==99) {
            febAccessInfoVector = dbServiceClient.getFebsByBarrelOrEndcap(XdaqDbServiceClient::BARREL);
        }
        else if(infoVector[5]==99)
        {

            febAccessInfoVector = dbServiceClient.getFebsByDiskOrWheel(infoVector[4],XdaqDbServiceClient::BARREL);
        }
        else if(infoVector[6]==99)
        {

            febAccessInfoVector =dbServiceClient.getFebsBySector(infoVector[4],infoVector[5],XdaqDbServiceClient::BARREL);
        }
        else if(infoVector[11]==99)
        {
            //LOG4CPLUS_INFO(getApplicationLogger(),"Dss:"<<infoVector[6]);
            if(infoVector[6]<=4)
                febAccessInfoVector =dbServiceClient.getFebsByChamberLocation(infoVector[4],infoVector[6],infoVector[5],"",XdaqDbServiceClient::BARREL);
            else
            {

                int value = infoVector[6];
                switch (value){
                case 5: febAccessInfoVector =dbServiceClient.getFebsByChamberLocation(infoVector[4],5,infoVector[5],"-",XdaqDbServiceClient::BARREL);break;
                case 6: febAccessInfoVector =dbServiceClient.getFebsByChamberLocation(infoVector[4],5,infoVector[5],"+",XdaqDbServiceClient::BARREL);break;
                case 7: febAccessInfoVector =dbServiceClient.getFebsByChamberLocation(infoVector[4],6,infoVector[5],"-",XdaqDbServiceClient::BARREL);break;
                case 8: febAccessInfoVector =dbServiceClient.getFebsByChamberLocation(infoVector[4],6,infoVector[5],"+",XdaqDbServiceClient::BARREL);break;
                case 9: febAccessInfoVector =dbServiceClient.getFebsByChamberLocation(infoVector[4],6,infoVector[5],"--",XdaqDbServiceClient::BARREL);break;
                case 10: febAccessInfoVector =dbServiceClient.getFebsByChamberLocation(infoVector[4],6,infoVector[5],"++",XdaqDbServiceClient::BARREL);break;
  		case 11: febAccessInfoVector =dbServiceClient.getFebsByChamberLocation(infoVector[4],6,infoVector[5],"",XdaqDbServiceClient::BARREL);break;
		}
            }

        }
     	else
    	{
    	std::string febPosition;
    	switch(infoVector[11]){
    		case 1: febPosition = "Forward";break;
     		case 2: febPosition = "Central";break;
    		case 3: febPosition = "Backward";break;

    		}

    	if(infoVector[6]<=4)
    		febAccessInfoVector =dbServiceClient.getSingleFebByLocParameters(infoVector[4],infoVector[6],infoVector[5],"","Barrel",febPosition,infoVector[13]);
    	else
 			switch (infoVector[6]){
    			case 5: febAccessInfoVector =dbServiceClient.getSingleFebByLocParameters(infoVector[4],5,infoVector[5],"-","Barrel",febPosition,infoVector[13]);break;
    			case 6: febAccessInfoVector =dbServiceClient.getSingleFebByLocParameters(infoVector[4],5,infoVector[5],"+","Barrel",febPosition,infoVector[13]);break;
    			case 7: febAccessInfoVector =dbServiceClient.getSingleFebByLocParameters(infoVector[4],6,infoVector[5],"-","Barrel",febPosition,infoVector[13]);break;
    			case 8: febAccessInfoVector =dbServiceClient.getSingleFebByLocParameters(infoVector[4],6,infoVector[5],"+","Barrel",febPosition,infoVector[13]);break;
    			case 9: febAccessInfoVector =dbServiceClient.getSingleFebByLocParameters(infoVector[4],6,infoVector[5],"--","Barrel",febPosition,infoVector[13]);break;
    			case 10: febAccessInfoVector =dbServiceClient.getSingleFebByLocParameters(infoVector[4],6,infoVector[5],"++","Barrel",febPosition,infoVector[13]);break;
			case 11: febAccessInfoVector =dbServiceClient.getSingleFebByLocParameters(infoVector[4],6,infoVector[5],"","Barrel",febPosition,infoVector[13]);break;
    			}
    	}

     //febAccessInfoVector =dbServiceClient.getSingleFebByLocParameters(1,1,1,"","Barrel","Forward",1);
     int csa =febAccessInfoVector->size();
     sum +=csa;
     LOG4CPLUS_INFO(getApplicationLogger(),"Size Barrel: " << csa  );

    }


    if(end == "Endcap")
    {
        boe +=2;
        if(infoVector[1]==99)
            febAccessInfoVectorEndcap = dbServiceClient.getFebsByBarrelOrEndcap(XdaqDbServiceClient::ENDCAP);
        else if(infoVector[2]==99)
            febAccessInfoVectorEndcap = dbServiceClient.getFebsByDiskOrWheel(infoVector[1],XdaqDbServiceClient::ENDCAP);
        else if(infoVector[3]==99)
            febAccessInfoVectorEndcap =dbServiceClient.getFebsByLayer(infoVector[1],infoVector[2],XdaqDbServiceClient::ENDCAP);
        else if(infoVector[12]==99)
            febAccessInfoVectorEndcap =dbServiceClient.getFebsByChamberLocation(infoVector[1],infoVector[2],infoVector[3],"",XdaqDbServiceClient::ENDCAP);
		else
    	{
    	string febPosition;
    	switch(infoVector[12]){
    		case 1: febPosition = "A";break;
     		case 2: febPosition = "B";break;
    		case 3: febPosition = "C";break;
    		}
    		febAccessInfoVectorEndcap =dbServiceClient.getSingleFebByLocParameters(infoVector[1],infoVector[2],infoVector[3],"","Endcap",febPosition,1);
    	}



        int csa1 =febAccessInfoVectorEndcap->size();
		sum +=csa1;
        LOG4CPLUS_INFO(getApplicationLogger(),"Size Endcap:" << csa1  );

    }



    typedef std::map<int ,XdaqLBoxAccessClient::MassiveWriteRequestBag > instanceMap;
    instanceMap instanceForLBBox;
    LOG4CPLUS_INFO(getApplicationLogger(),"FAIV Endcap ready:");
    //int incr=1;
    if(bar == "Barrel")
    {
        for (FAIV::iterator iFAI = febAccessInfoVector->begin(); iFAI != febAccessInfoVector->end(); ++iFAI) {
            FebAccessInfo& febAccessInfo = iFAI->bag;

            int xdaqInstanceNumber = febAccessInfo.getXdaqAppInstance();

            MassiveWriteRequest& writeRequest = instanceForLBBox[xdaqInstanceNumber].bag;

	    int autocorrection;
	    if (infoVector[18] == 1)
	      autocorrection = rpct::MassiveWriteRequest::AUTO_CORRECTION_FALSE;
	    else if(infoVector[18] == 2)
	      autocorrection = rpct::MassiveWriteRequest::AUTO_CORRECTION_TRUE;
	    else
	      autocorrection = rpct::MassiveWriteRequest::AUTO_CORRECTION_DEFAULT;
	    writeRequest.setAutoCorrection(autocorrection);

            if(instanceForLBBox[xdaqInstanceNumber].bag.getProperties().empty())
            {
                writeRequest.getProperties().push_back(MassiveWriteRequest::PROP_VTH1);
                writeRequest.getProperties().push_back(MassiveWriteRequest::PROP_VTH2);
                writeRequest.getProperties().push_back(MassiveWriteRequest::PROP_VMON1);
                writeRequest.getProperties().push_back(MassiveWriteRequest::PROP_VMON2);
            }
            xdata::Bag<FebValues> valuesBag;
            valuesBag.bag.getFeb().bag.setFebId(febAccessInfo.getFebId());
            valuesBag.bag.getFeb().bag.setCcuAddress(febAccessInfo.getCcuAddress());
            valuesBag.bag.getFeb().bag.setChannel(febAccessInfo.getI2cChannel());
            valuesBag.bag.getFeb().bag.setAddress(febAccessInfo.getFebAddress());
            valuesBag.bag.getFeb().bag.setChamberLocationName(febAccessInfo.getChamberLocationName());
            valuesBag.bag.getFeb().bag.setFebLocalEtaPartition(febAccessInfo.getFebLocalEtaPartition());
			valuesBag.bag.getFeb().bag.setEndcap(false);
            valuesBag.bag.getValues().push_back(infoVector[7]); // vth1
            valuesBag.bag.getValues().push_back(infoVector[8]); // vth2
            valuesBag.bag.getValues().push_back(infoVector[9]); // vmon1
            valuesBag.bag.getValues().push_back(infoVector[10]); // vmon2

            writeRequest.getFebValues().push_back(valuesBag);
        }
        delete febAccessInfoVector;
    }
    LOG4CPLUS_INFO(getApplicationLogger(),"FAIV barrel ready:");
    if(end == "Endcap")
    {
        for (FAIV::iterator iFAI = febAccessInfoVectorEndcap->begin(); iFAI != febAccessInfoVectorEndcap->end(); ++iFAI) {
            FebAccessInfo& febAccessInfo = iFAI->bag;

            //in febaccessInfo c'e la chiave xdaqAppInstance ccome base per la mappa
            // valueBag legge e setta i parametri da scrivere sulle LLBox

            int xdaqInstanceNumber = febAccessInfo.getXdaqAppInstance();

            MassiveWriteRequest& writeRequest = instanceForLBBox[xdaqInstanceNumber].bag;

	    int autocorrection;
	    if (infoVector[18] == 1)
	      autocorrection = rpct::MassiveWriteRequest::AUTO_CORRECTION_FALSE;
	    else if(infoVector[18] == 2)
	      autocorrection = rpct::MassiveWriteRequest::AUTO_CORRECTION_TRUE;
	    else
	      autocorrection = rpct::MassiveWriteRequest::AUTO_CORRECTION_DEFAULT;
	    writeRequest.setAutoCorrection(autocorrection);

            if(instanceForLBBox[xdaqInstanceNumber].bag.getProperties().empty())
            {

                writeRequest.getProperties().push_back(MassiveWriteRequest::PROP_VTH1);
                writeRequest.getProperties().push_back(MassiveWriteRequest::PROP_VTH2);
                writeRequest.getProperties().push_back(MassiveWriteRequest::PROP_VTH3);
                writeRequest.getProperties().push_back(MassiveWriteRequest::PROP_VTH4);
                writeRequest.getProperties().push_back(MassiveWriteRequest::PROP_VMON1);
                writeRequest.getProperties().push_back(MassiveWriteRequest::PROP_VMON2);
                writeRequest.getProperties().push_back(MassiveWriteRequest::PROP_VMON3);
                writeRequest.getProperties().push_back(MassiveWriteRequest::PROP_VMON4);
            }
            xdata::Bag<FebValues> valuesBag;
            valuesBag.bag.getFeb().bag.setFebId(febAccessInfo.getFebId());
            valuesBag.bag.getFeb().bag.setCcuAddress(febAccessInfo.getCcuAddress());
            valuesBag.bag.getFeb().bag.setChannel(febAccessInfo.getI2cChannel());
            valuesBag.bag.getFeb().bag.setAddress(febAccessInfo.getFebAddress());
            valuesBag.bag.getFeb().bag.setChamberLocationName(febAccessInfo.getChamberLocationName());
            valuesBag.bag.getFeb().bag.setFebLocalEtaPartition(febAccessInfo.getFebLocalEtaPartition());
			valuesBag.bag.getFeb().bag.setEndcap(true);
            valuesBag.bag.getValues().push_back(infoVector[7]);  // vth1
            valuesBag.bag.getValues().push_back(infoVector[8]);  // vth2
            valuesBag.bag.getValues().push_back(infoVector[14]); // vth3
            valuesBag.bag.getValues().push_back(infoVector[15]); // vth4
            valuesBag.bag.getValues().push_back(infoVector[9]);  // vmon1
            valuesBag.bag.getValues().push_back(infoVector[10]); // vmon2
            valuesBag.bag.getValues().push_back(infoVector[16]); // vmon3
            valuesBag.bag.getValues().push_back(infoVector[17]); // vmon4

            writeRequest.getFebValues().push_back(valuesBag);
        }
        delete febAccessInfoVectorEndcap;
    }
    LOG4CPLUS_INFO(getApplicationLogger(),"Sending data:");
   //  XdaqLBoxAccessClient::MassiveWriteResponseBag* lastMassiveWriteResponse;

    instanceReadMap instanceForReadLBBox;


    for(instanceMap::iterator iMap = instanceForLBBox.begin(); iMap != instanceForLBBox.end(); ++iMap){

      LOG4CPLUS_INFO(getApplicationLogger()," Map number "<< iMap->first);


		try {

        	instanceForReadLBBox[iMap->first] =lboxAccessClient.massiveWrite(iMap->second, iMap->first);

		}
		catch (std::exception& e) {
    		LOG4CPLUS_ERROR(getApplicationLogger(), "std exception: " << e.what());
		}

    }
    LOG4CPLUS_INFO(getApplicationLogger(),"Completed :" );
    if(flagHTML)
    			ReadBack(out,instanceForReadLBBox,sum,infoVector);

}

void XdaqRpcConfig::configDemo(xgi::Input* in, xgi::Output* out)
throw (xgi::exception::Exception) {

    int infoVector[18];


    try {
        Cgicc formData(in);
        form_iterator iDisk = formData.getElement("disk");
        form_iterator iLayer = formData.getElement("layer");
        form_iterator ieSector = formData.getElement("eSector");
        form_iterator iWheel = formData.getElement("wheel");
        form_iterator ibSector = formData.getElement("bSector");
        form_iterator iChamber = formData.getElement("chamber");
        form_iterator ibEta = formData.getElement("posOnChambB");
		form_iterator ieEta = formData.getElement("posOnChambE");
		form_iterator ifeb = formData.getElement("febPos");
        form_iterator ivth1 = formData.getElement("VTH1");
        form_iterator ivth2 = formData.getElement("VTH2");
        form_iterator ivth3 = formData.getElement("VTH3");
        form_iterator ivth4 = formData.getElement("VTH4");
        form_iterator ivmon1 = formData.getElement("VMON1");
        form_iterator ivmon2 = formData.getElement("VMON2");
        form_iterator ivmon3 = formData.getElement("VMON3");
        form_iterator ivmon4 = formData.getElement("VMON4");
        form_iterator precision = formData.getElement("precision");
	

        vector<FormEntry> endc;
        vector<FormEntry> barr;

        bool isEnd = formData.getElement("isCheckEndcap",endc);
        bool isBar = formData.getElement("isCheckBarrel",barr);

        string end,bar;

        if (iDisk != formData.getElements().end()) {


            infoVector[1] = iDisk->getIntegerValue();
            infoVector[2] = iLayer->getIntegerValue();
            infoVector[3] = ieSector->getIntegerValue();
            infoVector[4] = iWheel->getIntegerValue();
            infoVector[5] = ibSector->getIntegerValue();
            infoVector[6] = iChamber->getIntegerValue();
            infoVector[7] = ivth1->getIntegerValue();
            infoVector[8] = ivth2->getIntegerValue();
            infoVector[9] = ivmon1->getIntegerValue();
            infoVector[10] = ivmon2->getIntegerValue();
			infoVector[11] = ibEta->getIntegerValue();
		    infoVector[12] = ieEta->getIntegerValue();
		    infoVector[13] = ifeb->getIntegerValue();
            infoVector[14] = ivth3->getIntegerValue();
            infoVector[15] = ivth4->getIntegerValue();
            infoVector[16] = ivmon3->getIntegerValue();
            infoVector[17] = ivmon4->getIntegerValue();
            infoVector[18] = precision->getIntegerValue();


            if(isEnd)
                end =endc.front().getValue();
            else
                end= "";
            if(isBar)
                bar =barr.front().getValue();
            else
                bar= "";


            LOG4CPLUS_INFO(getApplicationLogger(),"BoE: "<<bar << end);
	    flagHTML = true;
            config(out, infoVector, bar, end);
        }
    }
    catch (xdaq::exception::Exception& e) {
        LOG4CPLUS_ERROR(getApplicationLogger(), "Error");
        XCEPT_RETHROW(xgi::exception::Exception, "Cannot send message", e);
    }
    catch (std::exception& e) {
        LOG4CPLUS_ERROR(getApplicationLogger(), "Error1");
        XCEPT_RAISE(xgi::exception::Exception, string("Cannot send message ") + e.what());
    }
    catch (...) {
        LOG4CPLUS_ERROR(getApplicationLogger(), "Error2");
        XCEPT_RAISE(xgi::exception::Exception, "Cannot send message - unknown reason");
    }

}


xoap::MessageReference XdaqRpcConfig::onRpcConfigDemo(xoap::MessageReference msg)
throw (xoap::exception::Exception) {
   xgi::Output* out;
    try {
        int infoVector[19];

        infoVector[1] = 99;
        infoVector[2] = 1;
        infoVector[3] = 1;
        infoVector[4] = 99;
        infoVector[5] = 1;
        infoVector[6] = 1;
        infoVector[7] = 210;
        infoVector[8] = 210;
        infoVector[9] = 3500;
        infoVector[10] = 3500;
        infoVector[11] = 1;
		infoVector[12] = 1;
		infoVector[13] = 1;
        infoVector[14] = 210;
        infoVector[15] = 210;
        infoVector[16] = 3500;
        infoVector[17] = 3500;
        infoVector[18] = 0;
        config(out, infoVector, "Barrel","EndCap");
        return xoap::createMessage();
    }
    catch (TException& e) {
        LOG4CPLUS_ERROR(getApplicationLogger(), e.what());
        XCEPT_RAISE(xoap::exception::Exception, e.what());
    }
}



xoap::MessageReference XdaqRpcConfig::fireEvent (xoap::MessageReference msg)
throw (xoap::exception::Exception) {

    xoap::SOAPPart part = msg->getSOAPPart();
    xoap::SOAPEnvelope env = part.getEnvelope();
    xoap::SOAPBody body = env.getBody();
    DOMNode* node = body.getDOMNode();
    DOMNodeList* bodyList = node->getChildNodes();
    for (unsigned int i = 0; i < bodyList->getLength(); i++) {
        DOMNode* command = bodyList->item(i);

        if (command->getNodeType() == DOMNode::ELEMENT_NODE) {

            std::string commandName = xoap::XMLCh2String (command->getLocalName());

            try {
                toolbox::Event::Reference e(new toolbox::Event(commandName,this));
                fsm.fireEvent(e);
            }
            catch (toolbox::fsm::exception::Exception & e)	{
                XCEPT_RETHROW(xoap::exception::Exception, "invalid command", e);
            }

            xoap::MessageReference reply = xoap::createMessage();
            xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
            xoap::SOAPName responseName = envelope.createName( commandName +"Response", "xdaq", XDAQ_NS_URI);
            // xoap::SOAPBodyElement e = envelope.getBody().addBodyElement ( responseName );
            (void) envelope.getBody().addBodyElement ( responseName );
            return reply;
        }
    }

    XCEPT_RAISE(xcept::Exception, "command not found");
}


xoap::MessageReference XdaqRpcConfig::reset(xoap::MessageReference msg) throw (xoap::exception::Exception)
{
    LOG4CPLUS_INFO (getApplicationLogger(), "New state before reset is: " << fsm.getStateName (fsm.getCurrentState()) );

    fsm.reset();
    stateName = fsm.getStateName (fsm.getCurrentState());

    xoap::MessageReference reply = xoap::createMessage();
    xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
    xoap::SOAPName responseName = envelope.createName("ResetResponse", "xdaq", XDAQ_NS_URI);
    // xoap::SOAPBodyElement e = envelope.getBody().addBodyElement ( responseName );
    (void) envelope.getBody().addBodyElement ( responseName );

    LOG4CPLUS_INFO (getApplicationLogger(), "New state after reset is: " << fsm.getStateName(fsm.getCurrentState()));

    return reply;
}


void XdaqRpcConfig::ConfigureAction (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception) {
    LOG4CPLUS_INFO (getApplicationLogger(), e->type());

    // Giovanni - please add your code here
    /*
     int infoVector[13];
	xgi::Output* out;
	string config;
	switch(config){
		case all:
        infoVector[1] = 99;
        infoVector[2] = 1;
        infoVector[3] = 1;
        infoVector[4] = 99;
        infoVector[5] = 1;
        infoVector[6] = 1;
        infoVector[7] = 210;
        infoVector[8] = 210;
        infoVector[9] = 3600;
        infoVector[10] = 3600;
        infoVector[11] = 1;
		infoVector[12] = 1;
		infoVector[13] = 1;
        config(out, infoVector, "Barrel","Endcap");

        case endcap:
         infoVector[1] = 99;
        infoVector[2] = 1;
        infoVector[3] = 1;
        infoVector[4] = 99;
        infoVector[5] = 1;
        infoVector[6] = 1;
        infoVector[7] = 210;
        infoVector[8] = 210;
        infoVector[9] = 3600;
        infoVector[10] = 3600;
        infoVector[11] = 1;
		infoVector[12] = 1;
		infoVector[13] = 1;
        config(out, infoVector, "","Endcap");

        case barrel:
         infoVector[1] = 99;
        infoVector[2] = 1;
        infoVector[3] = 1;
        infoVector[4] = 99;
        infoVector[5] = 1;
        infoVector[6] = 1;
        infoVector[7] = 210;
        infoVector[8] = 210;
        infoVector[9] = 3600;
        infoVector[10] = 3600;
        infoVector[11] = 1;
		infoVector[12] = 1;
		infoVector[13] = 1;
        config(out, infoVector, "Barrel","");
	}
	*/



}

void XdaqRpcConfig::EnableAction (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception) {
    LOG4CPLUS_INFO (getApplicationLogger(), e->type());
}

void XdaqRpcConfig::SuspendAction (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception) {
    LOG4CPLUS_INFO (getApplicationLogger(), e->type());
    // a failure is forced here
    //XCEPT_RAISE(toolbox::fsm::exception::Exception, "error in suspend");
}

void XdaqRpcConfig::ResumeAction (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception) {
    LOG4CPLUS_INFO (getApplicationLogger(), e->type());
}

void XdaqRpcConfig::HaltAction (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception) {
    LOG4CPLUS_INFO (getApplicationLogger(), e->type());
}

void XdaqRpcConfig::stateChanged (toolbox::fsm::FiniteStateMachine & fsm) throw (toolbox::fsm::exception::Exception) {
    // Reflect the new state
    stateName = fsm.getStateName (fsm.getCurrentState());
    LOG4CPLUS_INFO (getApplicationLogger(), "New state is:" << fsm.getStateName (fsm.getCurrentState()) );
}

void XdaqRpcConfig::failedTransition (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception) {
    toolbox::fsm::FailedEvent & fe = dynamic_cast<toolbox::fsm::FailedEvent&>(*e);
    LOG4CPLUS_INFO (getApplicationLogger(), "Failure occurred when performing transition from: "  <<
                    fe.getFromState() <<  " to: " << fe.getToState() << " exception: " << fe.getException().what() );
}

