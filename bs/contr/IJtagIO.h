#ifndef IJTAGIOH
#define IJTAGIOH

class IJtagIO {
public:
	virtual ~IJtagIO() {
	}
	virtual bool IsCaching() = 0;
	virtual void SetCaching(bool caching) = 0;
	virtual void Flush() = 0;
	
	/**
	 * tdo - output of the JTAG controller, i.e. bits send to the chips input
	 * read_tdi - read the input of the JTAG controller, i.e. bits from the chips output
	 */
	virtual bool JtagIO(bool tms, bool tdo, bool read_tdi) = 0;
	//virtual void JtagIO(bool* tms, bool* tdi, int count) = 0;

	// reads tdo and sens the value to tdi, return tdo read  
	virtual bool JtagIOLoop(bool tms) = 0;

	virtual int GetChannel()= 0;
	virtual void SetChannel(int channel) = 0;
};

#endif
