#include "TWZContrRecSeq.h"
#include "tb_std.h"
#include <iostream>



using namespace bs;


void TWZContrRecSeq::ToCharSequence( TScanVector& char_seq, unsigned long offset, unsigned long cnt )
{
  if( offset + cnt > Count )
    throw TException(":  TWZContrRecSeq::ToCharSequence: offset + cnt > Count\n");

  if (char_seq.size() < cnt)
    char_seq.resize(cnt);

  unsigned long  i;
  for( i=0; i<cnt; i++ ) {
    //char_seq[i] = (   (  ( RecData[offset/16] & mask[offset%16] ) == 0  ) ? '0' : '1'   );
    char_seq[cnt - i - 1] = (   (  ( RecData[offset/16] & mask[offset%16] ) == 0  ) ? '0' : '1'   );
    offset++;
  }
}
