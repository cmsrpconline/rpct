#ifndef TBSCONTRIIRECSEQ_HH
#define TBSCONTRIIRECSEQ_HH

#include "TBSContrIISeq.h" 
#include "../TScanVector.h"

namespace bs {

class TBSContrIIRecSeq : public TBSContrIISeq {
    unsigned long* RecData;
    unsigned long Count;
public:
    TBSContrIIRecSeq(unsigned long* rec_data, unsigned long count)
      : RecData(rec_data), Count(count) {}
    ~TBSContrIIRecSeq() {}

    void ToCharSequence(TScanVector&, unsigned long offset, unsigned long cnt);
};


} // namespace bs

#endif
