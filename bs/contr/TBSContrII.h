//---------------------------------------------------------------------------

#ifndef TBSContrIIH
#define TBSContrIIH
//---------------------------------------------------------------------------
               
#include "TBSContr.h"  
#include "TBSContrIISendSeq.h"
#include <deque>
          
namespace bs {

class TBSContrII : public TBSContr {
private:   
  typedef IDevice::TID TID;
  TIIDevice& Device;
  TID WORD_RSELECTOR;
  TID WORD_RSTEER;
  TID WORD_RDATAIN;
  TID WORD_RDATAOUT;
  TID WORD_RINREAD;
  TID WORD_RINWRITE;
  TID WORD_ROUTREAD;
  TID WORD_ROUTWRITE;
  TID WORD_STATUS;
  //TID BITS_RSTATUS;

  static const unsigned long StartMemoryAddress = 0;
  const unsigned long  MemorySize;
  const unsigned short MaxTransmissionLength;
  unsigned long WriteAddress; // adres pamieci kontrolera, pod jaki maja byc zapisywane
                                // dane odebrane z BS -- widziany tak, jak to widzi kontroler
  
  TBSContrIISendSeq *SendSeq;

  struct TRecSeqQueueItem {
    TScanVector* Seq;
    unsigned long  Count;
    TRecSeqQueueItem();
    TRecSeqQueueItem(TScanVector* s, unsigned long c) : Seq(s), Count(c) {}
  };
  typedef std::deque<TRecSeqQueueItem> TRecSeqQueue;
  TRecSeqQueue RecSeqQueue;

  unsigned long RecSeqLength;

  TTAPState TAPState;

  unsigned int Channel;

public:
  TBSContrII(TIIDevice& device,
             TID WORD_RSELECTOR_,
             TID WORD_RSTEER_,
             TID WORD_RDATAIN_,
             TID WORD_RDATAOUT_,
             TID WORD_RINREAD_,
             TID WORD_RINWRITE_,
             TID WORD_ROUTREAD_,
             TID WORD_ROUTWRITE_,
             TID WORD_STATUS_,
             unsigned long memorySize);

  // Programming
  void ProgramStart()
    {
      if( SendSeq != NULL )
        delete SendSeq;
      SendSeq = new TBSContrIISendSeq;
      RecSeqLength = 0;
    }

  void ProgramEnd();
  void ProgramExecute();

  void IRTransmit( TScanVector& send_seq )
    {
      SetTAPState( SHIFT_IR );
      Transmit( send_seq );
    }

  void IRTransmitRec( TScanVector& send_seq, TScanVector& rec_seq )
    {
      SetTAPState( SHIFT_IR );
      TransmitRec( send_seq, rec_seq );
    }

  void IRLoop( unsigned long length )
    {
      SetTAPState( SHIFT_IR );
      Loop( length );
    }

  void IRLoopRec( unsigned long length, TScanVector& rec_seq )
    {
      SetTAPState( SHIFT_IR );
      LoopRec( length, rec_seq );
    }

  void DRTransmit( TScanVector& send_seq )
    {
      SetTAPState( SHIFT_DR );
      Transmit( send_seq );
    }

  void DRTransmitRec( TScanVector& send_seq, TScanVector& rec_seq )
    {
      SetTAPState( SHIFT_DR );
      TransmitRec( send_seq, rec_seq );
    }

  void DRLoop( unsigned long length )
    {
      SetTAPState( SHIFT_DR );
      Loop( length );
    }

  void DRLoopRec( unsigned long length, TScanVector& rec_seq )
    {
      SetTAPState( SHIFT_DR );
      LoopRec( length, rec_seq );
    }

  void TAPtoReset()
    {
      SetTAPState( RESET );
    }

  void TAPtoIdle()
    {
      SetTAPState( IDLE );
    }

  // Programowanie szczegolowe

  void SetTAPState(TTAPState ts)
    {
      if( SendSeq == NULL )
      throw EBSContr(": there was no ProgramStart(); statement\n");

      if(  ts!=RESET && ts!=IDLE && TAPState!=RESET && TAPState!=IDLE  )
      SendSeq->CommSetTAPState(IDLE);
      SendSeq->CommSetTAPState(ts);
      TAPState = ts;
    }

  void Transmit( TScanVector& send_seq );
  void TransmitRec( TScanVector& send_seq, TScanVector& rec_seq );
  void Loop( unsigned long length );
  void LoopRec( unsigned long length, TScanVector& rec_seq );

  void SetChannel(int channel)
    {
      Channel = channel;
    }

  int GetChannel()
    {
      return Channel;
    }

  friend std::ostream& operator<<( std::ostream&, TBSContrII& );
};



} // namespace bs

#endif
