#ifndef TBSCONTRIISEQ_HH
#define TBSCONTRIISEQ_HH


namespace bs {

class TBSContrIISeq {
    typedef union {
	unsigned char tab[2];
	unsigned short num;
    } inv_union;

    unsigned short invert( unsigned short n );
    unsigned short do_nothing( unsigned short n );
    

protected:
    int hi_order;
    unsigned short mask[16];
    unsigned short (TBSContrIISeq:: *may_invert)( unsigned short n );
public:
    TBSContrIISeq();
    virtual ~TBSContrIISeq() {}
    unsigned short mi( unsigned short n );
};



inline unsigned short TBSContrIISeq::invert( unsigned short n )
{
    inv_union u;
    u.num=n;
    unsigned char temp=u.tab[0];
    u.tab[0] = u.tab[1];
    u.tab[1] = temp;
    return u.num;
}

inline unsigned short TBSContrIISeq::do_nothing( unsigned short n )
{
    return n;
}

inline    unsigned short TBSContrIISeq::mi( unsigned short n )
{
    return (this->*may_invert) (n);
}


} // namespace bs

#endif
