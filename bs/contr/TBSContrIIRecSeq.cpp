#include "TBSContrIIRecSeq.h"
#include "tb_std.h"
#include <iostream.h>



using namespace bs;


void TBSContrIIRecSeq::ToCharSequence( TScanVector& char_seq, unsigned long offset, unsigned long cnt )
{
  if( offset + cnt > Count )
    throw TException(":  TBSContrIIRecSeq::ToCharSequence: offset + cnt > Count");

  if (char_seq.size() < cnt)
    char_seq.resize(cnt);

  unsigned long  i;
  for( i=0; i<cnt; i++ ) {
    //char_seq[i] = (   (  ( RecData[offset/16] & mask[offset%16] ) == 0  ) ? '0' : '1'   );
    char_seq[cnt - i - 1] = (   (  ( RecData[offset/16] & mask[offset%16] ) == 0  ) ? '0' : '1'   );
    offset++;
  }
}
