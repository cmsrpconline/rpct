#include "TWZContrSeq.h"



using namespace bs;


TWZContrSeq::TWZContrSeq()
{
    unsigned short check = 1;

    check >>= 1;
    if( check ) {   // low byte order: LoHi
	mask[0]  = 0x0100;
	mask[1]  = 0x0200;
	mask[2]  = 0x0400;
	mask[3]  = 0x0800;
	mask[4]  = 0x1000;
	mask[5]  = 0x2000;
	mask[6]  = 0x4000;
	mask[7]  = 0x8000;
	mask[8]  = 0x0001;
	mask[9]  = 0x0002;
	mask[10] = 0x0004;
	mask[11] = 0x0008;
	mask[12] = 0x0010;
	mask[13] = 0x0020;
	mask[14] = 0x0040;
	mask[15] = 0x0080;
	may_invert = &TWZContrSeq::invert;
	hi_order = 0;
    }
    else {
	mask[0]  = 0x0001;
	mask[1]  = 0x0002;
	mask[2]  = 0x0004;
	mask[3]  = 0x0008;
	mask[4]  = 0x0010;
	mask[5]  = 0x0020;
	mask[6]  = 0x0040;
	mask[7]  = 0x0080;
	mask[8]  = 0x0100;
	mask[9]  = 0x0200;
	mask[10] = 0x0400;
	mask[11] = 0x0800;
	mask[12] = 0x1000;
	mask[13] = 0x2000;
	mask[14] = 0x4000;
	mask[15] = 0x8000;
	may_invert = &TWZContrSeq::do_nothing;
	hi_order = 1;
    }
}
