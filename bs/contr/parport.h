//---------------------------------------------------------------------------

#ifndef parportH
#define parportH
//---------------------------------------------------------------------------


#ifdef __cplusplus
extern "C"{
#endif

void parportinit();
void parportclose();
void outp(int base, int value);
char inp(int base);


#ifdef __cplusplus
}
#endif


#endif
