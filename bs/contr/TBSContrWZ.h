#ifndef BSCONTRWZ_HH
#define BSCONTRWZ_HH

//#define TB_DEBUG 5
#include "TBSContr.h"
#include "tb_vme.h"
#include <typeinfo>
#include <deque>
#include "TWZContrSendSeq.h"


namespace bs {


class TBSContrWZ : public TBSContr {
   
    TVMEInterface *VME;
    
    
    const unsigned long StartMemoryAddress;
    const unsigned long StatusRegister;
    const unsigned long WriteAddressRegister;
    const unsigned long ReadAddressRegister;

    const unsigned long  MemorySize;
    const unsigned short MaxTransmissionLength;
    unsigned long WriteAddress; // adres pamieci kontrolera, pod jaki maja byc zapisywane 
                                // dane odebrane z BS -- widziany tak, jak to widzi kontroler


    TWZContrSendSeq *SendSeq;

    struct TRecSeqQueueItem {
	TScanVector* Seq;
	unsigned long  Count;
	TRecSeqQueueItem();
	TRecSeqQueueItem(TScanVector* s, unsigned long c) : Seq(s), Count(c) {}
    };

    std::deque<TRecSeqQueueItem> RecSeqQueue;
    unsigned long RecSeqLength;
    TTAPState TAPState;

public:
    TBSContrWZ( TVMEInterface* vme, unsigned long board_address );
    ~TBSContrWZ();


    // Programowanie
    void ProgramStart();
    void ProgramEnd();
    void ProgramExecute();
    
    void IRTransmit( TScanVector& send_seq );
    void IRTransmitRec( TScanVector& send_seq, TScanVector& rec_seq );
    void IRLoop( unsigned long length );
    void IRLoopRec( unsigned long length, TScanVector& rec_seq );
    
    void DRTransmit( TScanVector& send_seq );
    void DRTransmitRec( TScanVector& send_seq, TScanVector& rec_seq );
    void DRLoop( unsigned long length );
    void DRLoopRec( unsigned long length, TScanVector& rec_seq );
    
    void TAPtoReset();
    void TAPtoIdle();
    
    // Programowanie szczegolowe
    
    void SetTAPState( TTAPState );
    
    void Transmit( TScanVector& send_seq );
    void TransmitRec( TScanVector& send_seq, TScanVector& rec_seq );
    void Loop( unsigned long length );
    void LoopRec( unsigned long length, TScanVector& rec_seq );

    friend std::ostream& operator<<( std::ostream&, TBSContrWZ& );
};


inline
TBSContrWZ::TBSContrWZ( TVMEInterface* vme, unsigned long ba ) : TBSContr(), VME(vme),
    StartMemoryAddress( 0xc0000 | (ba<<20) ), StatusRegister( 0x80000 | (ba<<20) ), 
    WriteAddressRegister( 0x80004 | (ba<<20) ), ReadAddressRegister( 0x80008 | (ba<<20) ), 
    MemorySize( 0xffff ), MaxTransmissionLength(0x1000), 
    WriteAddress(0), SendSeq(NULL), RecSeqLength(0), TAPState(RESET)
{
}


inline
TBSContrWZ::~TBSContrWZ()
{
}


inline
void TBSContrWZ::ProgramStart()
{
    if( SendSeq != NULL ) 
	delete SendSeq;
    SendSeq = new TWZContrSendSeq;      
    RecSeqLength = 0;
}


inline
void TBSContrWZ::IRTransmit( TScanVector& send_seq )
{
    SetTAPState( SHIFT_IR );
    Transmit( send_seq );
}


inline
void TBSContrWZ::IRTransmitRec( TScanVector& send_seq, TScanVector& rec_seq )
{
    SetTAPState( SHIFT_IR );
    TransmitRec( send_seq, rec_seq );
}


inline
void TBSContrWZ::IRLoop( unsigned long length )
{
    SetTAPState( SHIFT_IR );
    Loop( length );
}


inline
void TBSContrWZ::IRLoopRec( unsigned long length, TScanVector& rec_seq )
{
    SetTAPState( SHIFT_IR );
    LoopRec( length, rec_seq );
}


inline
void TBSContrWZ::DRTransmit( TScanVector& send_seq )
{
    SetTAPState( SHIFT_DR );
    Transmit( send_seq );
}


inline
void TBSContrWZ::DRTransmitRec( TScanVector& send_seq, TScanVector& rec_seq )
{
    SetTAPState( SHIFT_DR );
    TransmitRec( send_seq, rec_seq );
}


inline
void TBSContrWZ::DRLoop( unsigned long length )
{
    SetTAPState( SHIFT_DR );
    Loop( length );
}


inline
void TBSContrWZ::DRLoopRec( unsigned long length, TScanVector& rec_seq )
{
    SetTAPState( SHIFT_DR );
    LoopRec( length, rec_seq );
}


inline
void TBSContrWZ::TAPtoReset()
{
    SetTAPState( RESET );
}


inline
void TBSContrWZ::TAPtoIdle()
{
    SetTAPState( IDLE );
}


inline
void TBSContrWZ::SetTAPState( TTAPState ts )
{
    if( SendSeq == NULL )
	throw EBSContr(": there was no ProgramStart(); statement\n");

    if(  ts!=RESET && ts!=IDLE && TAPState!=RESET && TAPState!=IDLE  ) 
	SendSeq->CommSetTAPState(IDLE);	
    SendSeq->CommSetTAPState(ts);
    TAPState = ts;
}
    

} // namespace bs

#endif
