#if !defined TBITSQN_H
#define TBITSQN_H

#include <iostream.h>
#include <stdlib.h>
#include <string.h>
#include "TBSController.hh"

extern TBSController *Controller;

extern const unsigned short mask[16];

#if defined HI_BYTE_ORDER
#elif defined LO_BYTE_ORDER

typedef union {
    unsigned char tab[2];
    unsigned short num;
} inv_union;

inline unsigned short invert( unsigned short n )
{
    inv_union u;
    u.num=n;
    unsigned char temp=u.tab[0];
    u.tab[0] = u.tab[1];
    u.tab[1] = temp;
    return u.num;
}
#else
#error Byte orded not defined ( LO_BYTE_ORDER or HI_BYTE_ORDER )
#endif




class TBitSequenceElement {
    unsigned short* Data;
    TBitSequenceElement* Next;
    int BitPosition;
    int DataIndex;
    const int DataSize;   // in sizeof(unsigned short)
    int Full;
    void IncreaseDataIndex();
public:
    TBitSequenceElement();
    ~TBitSequenceElement()
    {
	delete Next;
	delete [] Data;
    }
    void InsertBit( const int );
    void InsertNumber( const unsigned short );
    int NumBitsStored() const ;
    int CurrentInsertPosition() const  
    {
	if( Full )
	    return Next->CurrentInsertPosition();
	else
	    return BitPosition;
    }                
    // 		void CopyData( unsigned short *dest ) const;
    void Print();
    void CopyDataToController() const;
    
};







class TBitSequence {
    TBitSequenceElement Data;
public:
    TBitSequence(){}
    void Print() 
    { Data.Print(); }
    void CopyDataToControllerMemory() const;
    void SendBit( const int i )
    {
	Data.InsertBit( i );
	if( 15== ( Data.CurrentInsertPosition() % 16 ) )
	    Data.InsertBit( 0 );
    }
    void SendString( const char* str, int len=0 ); 
    void EndWordWithZeros();
    inline void ProgramEnd();
    inline void TAPtoTestLogicReset();
    inline void TAPtoRunTestIdle();
    inline void SelectIR();
    inline void SelectDR();
    inline void ChangeTAPmode( int );
    inline void TransmitBits( int read, int write, int nbits );
    inline void CyclicTransmission( int nbits );
    inline void CyclicTransmissionExit( int lastbit );
    inline void CyclicWriteTransmission( int nbits );
    inline void CyclicWriteTransmissionExit( int lastbit );
    inline void Transmit( int nbits );
    inline void TransmitExit( int lastbit );
    inline void TransmitWrite( int nbits );
    inline void TransmitWriteExit( int lastbit );
    
    
    
};




inline void TBitSequence::ProgramEnd()
{
#if defined HI_BYTE_ORDER
    Data.InsertNumber( 0x8000 );
#else
    Data.InsertNumber( 0x0080 );
#endif
}



inline void TBitSequence::TAPtoTestLogicReset()
{
#if defined HI_BYTE_ORDER
    Data.InsertNumber( 0x9000 );
#else
    Data.InsertNumber( 0x0090 );
#endif
}


inline void TBitSequence::TAPtoRunTestIdle()
{
#if defined HI_BYTE_ORDER
    Data.InsertNumber( 0x9001 );
#else
    Data.InsertNumber( 0x0190 );
#endif
}



inline void TBitSequence::SelectIR()
{
#if defined HI_BYTE_ORDER
    Data.InsertNumber( 0x9002 );
#else
    Data.InsertNumber( 0x0290 );
#endif
}


inline void TBitSequence::SelectDR()
{
#if defined HI_BYTE_ORDER
    Data.InsertNumber( 0x9003 );
#else
    Data.InsertNumber( 0x0390 );
#endif
}


inline void TBitSequence::ChangeTAPmode( int modeCode)
{
#if defined HI_BYTE_ORDER
    Data.InsertNumber( 0x9000 | (unsigned short)modeCode );
#else
    Data.InsertNumber( 0x0090 | invert( (unsigned short)modeCode ) );
#endif
}



inline void TBitSequence::TransmitBits( int read, int write, int nbits )
{
    nbits--;
    if( read ) {
	if( write )
	    TransmitWrite( nbits );
	else
	    Transmit( nbits );
    }
    else {
	if( write )
	    CyclicWriteTransmission( nbits );
	else
	    CyclicTransmission( nbits );
    };
}

inline void TBitSequence::CyclicTransmission( int nbits )
{
    nbits--;
#if defined HI_BYTE_ORDER
    Data.InsertNumber( 0xC000 | (unsigned short)nbits );
#else
    Data.InsertNumber( 0x00C0 | invert( (unsigned short)nbits ) );
#endif
}

inline void TBitSequence::CyclicTransmissionExit( int lastbit )
{
#if defined HI_BYTE_ORDER
    Data.InsertNumber( 0x9008 | (unsigned short)lastbit );
#else
    Data.InsertNumber( 0x0890 | invert( (unsigned short)lastbit ) );
#endif
}


inline void TBitSequence::CyclicWriteTransmission( int nbits )
{
    nbits--;
#if defined HI_BYTE_ORDER
    Data.InsertNumber( 0xD000 | (unsigned short)nbits );
#else
    Data.InsertNumber( 0x00D0 | invert( (unsigned short)nbits ) );
#endif
}

inline void TBitSequence::CyclicWriteTransmissionExit( int lastbit )
{
#if defined HI_BYTE_ORDER
    Data.InsertNumber( 0x900A | (unsigned short)lastbit );
#else
    Data.InsertNumber( 0x0A90 | invert( (unsigned short)lastbit ) );
#endif
}

inline void TBitSequence::Transmit( int nbits )
{
    nbits--;
#if defined HI_BYTE_ORDER
    Data.InsertNumber( 0xE000 | (unsigned short)nbits );
#else
    Data.InsertNumber( 0x00E0 | invert( (unsigned short)nbits ) );
#endif
}

inline void TBitSequence::TransmitExit( int lastbit )
{
#if defined HI_BYTE_ORDER
    Data.InsertNumber( 0x900C | (unsigned short)lastbit );
#else
    Data.InsertNumber( 0x0C90 | invert( (unsigned short)lastbit ) );
#endif
}




inline void TBitSequence::TransmitWrite( int nbits )
{
    nbits--;
#if defined HI_BYTE_ORDER
    Data.InsertNumber( 0xF000 | (unsigned short)nbits );
#else
    Data.InsertNumber( 0x00F0 | invert( (unsigned short)nbits ) );
#endif
}

inline void TBitSequence::TransmitWriteExit( int lastbit )
{
#if defined HI_BYTE_ORDER
    Data.InsertNumber( 0x900E | (unsigned short)lastbit );
#else
    Data.InsertNumber( 0x0E90 | invert( (unsigned short)lastbit ) );
#endif
}


class TReceiveBitSequence {
    unsigned short *Data;
    int NumBits;
    int CurrentBit;
public:
    TReceiveBitSequence( int numbits ): NumBits(numbits), CurrentBit(0) {	
	if( (Data = new unsigned short[ numbits/(8*sizeof(unsigned short)) + 1 ] ) == NULL ) {
	    cerr << "Memory allocation problem : class \
BitSequenceElement.\n Terminating... \n";
	    exit(1);
	};
	Controller->CopyReceivedData( Data, numbits/(8*sizeof(unsigned short)) + 1 ); 
	cout << "\n TReceiveBitSequence :";
	int i;
	for( i=0; i< (int)(numbits/(8*sizeof(unsigned short)) + 1); i++ )
	    cout << hex<<Data[i] << '\t';
    }
    ~TReceiveBitSequence() {
	delete [] Data;
    }
    // 	void CopyDataFromControllerMemory()
    //   			{
    // 				memcpy( Data, Controller->GetWriteAddress(),2*(NumBits/(8*sizeof(unsigned short))+1) );
    // 			}
    void GetData( char * str, int numbits );
    friend ostream& operator <<( ostream&,TReceiveBitSequence& ); 
  
};





class TCharSequence {
    char* Data;
    int Length;
    char* Current;
public:
    TCharSequence( int len ): Length(len)
    {
	if( NULL == ( Current=Data=new char[Length+1] ) ) {
	    cerr << "Memory allocation problem : class \
BitSequenceElement.\n Terminating... \n";
	    exit(1);
	}
	Data[Length]='\0';
    }
    char* GetPointer()
    { return Data; }
    void SendString( const char* str )
    {
	int l=strlen(str);
	strncpy( Current, str, l );
	Current+=l;
    }
    void SendBit( const int i )
    {
	(*Current) = (i ?'1':'0');
	Current++;
    }
};



#endif  // !defined TBITSQN_H
