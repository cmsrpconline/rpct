#include <typeinfo>
#include <iostream.h>
#include <string>
#include "tb_std.h"
#include "TBSContrJAM.h"

using namespace bs;

int main()
{
  //parportinit();
  //outp(
  while(true) {

      int i;
      try {
        TBSContr * bsc = new TBSContrJAM();
        TScanVector seq1(30,' '), seq2(30,' ');

        bsc->ProgramStart();
        bsc->TAPtoReset();
        bsc->SetTAPState( TBSContr::IDLE );
        bsc->SetTAPState( TBSContr::SHIFT_IR );
        //bsc->IRTransmitRec( "1000101010101010101010", seq1 );
        bsc->DRTransmitRec( "01010101010101010101", seq2 );

        //cout <<hex<<"bsc:\n"<<(*(TBSContrWZ*)bsc);
        bsc->ProgramEnd();

        bsc->ProgramExecute();
        cout << "seq1="<<seq1<<endl;
        cout << "seq2="<<seq2<<endl;
      }
      catch( TException& e ) {
        cerr << e.what() << endl;
      }
      catch( ... ) {
        cerr << "Exception\n";
      };
      Sleep(10);
      cout.flush();
  };

  char c;
  cin >> c;

  return 1;
}
