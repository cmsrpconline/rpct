#ifndef TWZCONTRSENDSEQ_HH
#define TWZCONTRSENDSEQ_HH

#include "tb_std.h"
#include "TWZContrSeq.h"
#include "../TScanVector.h"
//#include "..\BStypes.h"  
#include "TBSContr.h"

namespace bs {

class TWZContrSendSeq : public TWZContrSeq {
    unsigned short* Data;
    TWZContrSendSeq* Next;
    int BitPosition;
    int DataIndex;
    const int DataSize;   // in sizeof(unsigned short)
    int Full;
    
    static unsigned long* ULBuffer; //used by function   unsigned short* GetAsUS();

    void increaseDataIndex();
    void insertBit(const int);
    void insertNumber(const unsigned short);
    int  currentInsertPosition() const;
    void copyDataToULBuffer( int ULBufferIndex ); // ULBufferIndex index which to copy at
public:
    TWZContrSendSeq();
    ~TWZContrSendSeq();
    
    void CommSetTAPState( TBSContr::TTAPState );
    
    void CommTransmit(int);
    void CommTransmitRec(int);
    void CommLoop(int);
    void CommLoopRec(int);
    
    void CommTransmitExit( int lastbit );
    void CommTransmitRecExit( int lastbit);
    void CommLoopExit();
    void CommLoopRecExit();
    
    void CommProgramEnd();
    
    void SendBit( const int i );
    void SendData( TScanVector& send_seq ); 
    void EndWordWithZeros();
    
    
    unsigned long GetLength();     // zwraca ilosc dwubajtowych slow
    unsigned long GetLengthBits();
    unsigned long* GetAsUL();
    
    friend std::ostream& operator<<(std::ostream&,TWZContrSendSeq&); 
};



inline void TWZContrSendSeq::increaseDataIndex()
{
    BitPosition = 0;
    if( (++DataIndex ) == DataSize ) {
	Next = new TWZContrSendSeq();
	Full = 1;
    };
}





   
inline void TWZContrSendSeq::CommTransmit(int nbits)   
{
    insertNumber( (this->*may_invert)( 0xE000 | ((unsigned short)nbits-1) ) );
}
    
inline void TWZContrSendSeq::CommTransmitRec(int nbits)
{
    insertNumber( (this->*may_invert)( 0xF000 | ((unsigned short)nbits-1) ) );
}  

inline void TWZContrSendSeq::CommLoop(int nbits)
{
    insertNumber( (this->*may_invert)( 0xC000 | ((unsigned short)nbits-1) ) );
}  

inline void TWZContrSendSeq::CommLoopRec(int nbits)
{
    insertNumber( (this->*may_invert)( 0xD000 | ((unsigned short)nbits-1) ) );
}  


inline void TWZContrSendSeq::CommTransmitExit(int lastbit)
{
    insertNumber( (this->*may_invert)( 0x900C | ((unsigned short)lastbit) ) );
}  

inline void TWZContrSendSeq::CommTransmitRecExit(int lastbit)
{
    insertNumber( (this->*may_invert)( 0x900E | ((unsigned short)lastbit) ) );
}  
  
inline void TWZContrSendSeq::CommLoopExit()
{
    insertNumber( (this->*may_invert)( 0x9008 ) );
} 
 
inline void TWZContrSendSeq::CommLoopRecExit()
{
    insertNumber( (this->*may_invert)( 0x900A ) );
}  

inline void TWZContrSendSeq::CommProgramEnd()
{
    insertNumber( (this->*may_invert)( 0x8000 ) );
}  


inline void TWZContrSendSeq::SendBit( const int i )
{
    insertBit( i );
    if( 15== ( currentInsertPosition() % 16 ) )
	insertBit( 0 );
}


} // namespace bs

#endif
