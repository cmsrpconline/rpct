#include <iostream.h>
#include "TBSContrIISendSeq.h"

                    

using namespace bs;

unsigned long* TBSContrIISendSeq::ULBuffer = NULL; 

TBSContrIISendSeq::TBSContrIISendSeq() : TBSContrIISeq(),
    Next(NULL), BitPosition(0), DataIndex(0), DataSize(64), Full(0)
{
    try {
	  Data = new unsigned short[DataSize];
    }
    catch(...) {
	  throw EOutOfMemory(": TBSContrIISendSeq::TBSContrIISendSeq()\n");
    }

    for( int i=0; i< DataSize; i++ )
	Data[i] = 0;
}



TBSContrIISendSeq::~TBSContrIISendSeq()
{
    delete Next;
    delete [] Data;
    if( ULBuffer != NULL ) {
	  delete [] ULBuffer;
	ULBuffer = NULL;
    }
}




void TBSContrIISendSeq::insertBit( const int i )
{
    if( Full ) {
	  Next -> insertBit( i );
	  return;
    }
    if( i )
	  Data[DataIndex] |= mask[BitPosition];
    BitPosition++;
    if( BitPosition == 16 )
	  increaseDataIndex();
}



void TBSContrIISendSeq::insertNumber( const unsigned short n )
{
    if( Full ) {
	  Next -> insertNumber( n );
	  return;
    };
    if( !BitPosition ) {
	  Data[DataIndex] = n;
	  increaseDataIndex();
    }
    else {
	  increaseDataIndex();
	  insertNumber( n );
    };
}


int TBSContrIISendSeq::currentInsertPosition() const
{
    if( Full )
	  return Next->currentInsertPosition();
    else
	  return BitPosition;
}   


void TBSContrIISendSeq::copyDataToULBuffer( int ULBufferIndex )
{
    int i, 
	size=DataIndex + (BitPosition>0);
    for( i=0; i<size; i++ )
	  ULBuffer[ULBufferIndex++] = Data[i] ;
    

    if( Next!=NULL )
	  Next->copyDataToULBuffer(ULBufferIndex);
}


void TBSContrIISendSeq::CommSetTAPState( TBSContr::TTAPState ts )
{
    switch( ts ) {
    case TBSContr::RESET:
	if(hi_order)
	    insertNumber( 0x9000 );
	else
	    insertNumber( 0x0090 );
	break;
    case TBSContr::IDLE:
	if(hi_order)
	    insertNumber( 0x9001 );
	else
	    insertNumber( 0x0190 );
	break;
    case TBSContr::SHIFT_IR:
	if(hi_order)
	    insertNumber( 0x9002 );
	else
	    insertNumber( 0x0290 );
	break;
    case TBSContr::SHIFT_DR:
	if(hi_order)
	    insertNumber( 0x9003 );
	else
	    insertNumber( 0x0390 );
	break;
    default:
	throw TException("TBSContrIISendSeq::SetTAPState : undefined TAP state\n");
    }  
} 


void TBSContrIISendSeq::SendData( TScanVector& send_seq )
{                   
    //G_Log.out() << "TBSContrIISendSeq::SendData: " << send_seq << std::endl;
    for(TScanVector::iterator iter = send_seq.begin(); iter != send_seq.end(); ++iter)
      SendBit(*iter =='1');
}

void TBSContrIISendSeq::EndWordWithZeros()
{
    int max= 16 - currentInsertPosition();
    if( max == 16 )
	return;
    for( ; max > 0; max-- )
	insertBit( 0 );
}

unsigned long TBSContrIISendSeq::GetLength()
{
    return DataIndex + (BitPosition>0) +( ( Next==NULL ) ? 0 : Next->GetLength() );
}

unsigned long TBSContrIISendSeq::GetLengthBits()
{
    return DataIndex * 16 + BitPosition +( ( Next==NULL ) ? 0 : Next->GetLengthBits() );
}


unsigned long* TBSContrIISendSeq::GetAsUL()
{
    if( ULBuffer != NULL ) {
	  delete [] ULBuffer;
	  ULBuffer = NULL;
    } 
    try {
	  ULBuffer = new unsigned long[ GetLength() ];
    }  
    catch(...) {
	  throw EOutOfMemory( ": TBSContrIISendSeq::GetAsUL()\n" );
    }
    copyDataToULBuffer(0);
    return ULBuffer;
}


ostream& bs::operator<<(ostream& os,TBSContrIISendSeq& ss) 
{
    int size =  ss.DataIndex + (ss.BitPosition>0);
    for( int i=0; i<size; i++ )
	os << ss.Data[i] << "\t";
    if( ss.Next != NULL )
	os << *(ss.Next);
    else
	os << endl;
    return os;
}


