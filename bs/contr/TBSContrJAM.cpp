//---------------------------------------------------------------------------
#include "TBSContrJAM.h"
#include <memory>
#include <cstring> 

//---------------------------------------------------------------------------

#pragma package(smart_init)

using namespace bs;  
using namespace std;




long* TBSContrJAM::TCommand::ScanVectorToArray(const TScanVector& seq)
{
  int size = seq.size();
  int items = (size + sizeof(long) * 8 - 1) / (sizeof(long) * 8);
  long* data = new long[items];
  memset(data, 0, items * sizeof(long));
  for (int i = 0; i < size; i++)
    data[i/(sizeof(long) * 8)] |= ((seq[i] == '1') << (i % (sizeof(long) * 8)));
  return data;
}

void TBSContrJAM::TCommand::ArrayToScanVector(long* data, int count, TScanVector& seq)
{
  seq.resize(count);
  for (int i = 0; i < count; i++)
    seq[i] = (((data[i/(sizeof(long) * 8)] >> (i % (sizeof(long) * 8))) & 0x1) == 1) ? '1' : '0';
}
                   
void TBSContrJAM::ProgramStart()
{
  while (!CommandQueue.empty()) {
    delete CommandQueue.front();
    CommandQueue.pop_front();
  };
}

void TBSContrJAM::ProgramExecute()
{
  auto_ptr<TCommand> command;
  while (!CommandQueue.empty()) {
    command.reset(CommandQueue.front());
    CommandQueue.pop_front();
    command->Execute();
    delete command.release();
  };
}


void TBSContrJAM::TSetTAPState::Execute()
{
  static JAME_JTAG_STATE state_map[] = {
    ::RESET, ::IDLE, ::IRSHIFT, ::DRSHIFT
  };

  SetJTAGIO(JtagIO);
  CheckResult(jam_goto_jtag_state(state_map[TAPState]));
}
