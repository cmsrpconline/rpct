//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "parport.h"    
#include "TDLPortIO.h"
#include "tb_std.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
    
TDLPortIO* M_DLPortIO = NULL;
                      
void parportinit()
{
  if (M_DLPortIO != NULL)
    throw("Another instance of TDLPortIO already created");

  M_DLPortIO = new TDLPortIO(NULL);

  M_DLPortIO->OpenDriver();

  if (!M_DLPortIO->ActiveHW)
    throw TException("Could not open the DriverLINX driver.");
}


void parportclose()
{
  delete M_DLPortIO;
  M_DLPortIO = NULL;
}

void outp(int base, int value)
{
  M_DLPortIO->Port[base]=value;
}

char inp(int base)
{
  return M_DLPortIO->Port[base];
}
