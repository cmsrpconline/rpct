#ifndef TBSCONTR_HH
#define TBSCONTR_HH

#include "tb_std.h"
#include "../TScanVector.h"

namespace bs {

class EBSContr : public TException {
public:
    EBSContr() throw()
	: TException("BS Controller Error") {}

    EBSContr(const std::string& msg) throw()
	: TException( "BS Controller Error" + msg ) {}
    EXCFASTCALL virtual ~EBSContr() throw() {}
};

class EBSContrOutOfMemory : public EBSContr {
public:
    EBSContrOutOfMemory() throw()
	: EBSContr(": controller is out of memory") {}
    EBSContrOutOfMemory(const std::string& msg) throw()
	: EBSContr( ": controller is out of memory" + msg ) {}
    EXCFASTCALL virtual ~EBSContrOutOfMemory() throw() {}
};


class TBSContr {
public:      
    enum TTAPState{ RESET, IDLE, SHIFT_IR, SHIFT_DR};
    TBSContr() {}
    virtual ~TBSContr() {}

    //virtual void Reset() = 0;

    virtual void ProgramStart() = 0;
    virtual void ProgramEnd() = 0;
    virtual void ProgramExecute() = 0;

    virtual void IRTransmit(TScanVector& send_seq) = 0;
    virtual void IRTransmitRec(TScanVector& send_seq, TScanVector& rec_seq) = 0;
    virtual void IRLoop(unsigned long length) = 0;
    virtual void IRLoopRec(unsigned long length, TScanVector& rec_seq) = 0;

    virtual void DRTransmit(TScanVector& send_seq) = 0;
    virtual void DRTransmitRec(TScanVector& send_seq, TScanVector& rec_seq) = 0;
    virtual void DRLoop(unsigned long length) = 0;
    virtual void DRLoopRec(unsigned long length, TScanVector& rec_seq) = 0;

    virtual void TAPtoReset() = 0;
    virtual void TAPtoIdle() = 0;

    virtual void SetTAPState(TTAPState) = 0;

    friend std::ostream& operator<<(std::ostream&, TBSContr&);

};


} // namespace bs

#endif
