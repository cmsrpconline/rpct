#include <iostream>
#include "TBSContrWZ.h"
#include "TWZContrRecSeq.h"

using namespace bs;
using namespace std;

void TBSContrWZ::ProgramEnd()
{
    if( SendSeq == NULL )
	throw EBSContr(": there was no ProgramStart(); statement\n");

    SendSeq->CommProgramEnd();

    VME->Write32( StatusRegister, 0x0 );  //przejscie kontrolera do trybu spoczynkowego
    
    unsigned long seq_len = SendSeq->GetLength(); //kopiowanie SendSeq do pamieci kontrolera 
    if( seq_len > MemorySize ) 
	throw EBSContrOutOfMemory(": To little memory to load the program to the controller");
      
    if( (seq_len + RecSeqLength/16 + 1) > MemorySize )
	throw EBSContrOutOfMemory(": To little memory to receive the data in the controller");
    

    unsigned long* seq_ptr = SendSeq->GetAsUL();
    VME->Write( StartMemoryAddress, (char*)seq_ptr, sizeof(unsigned long) * seq_len );

    WriteAddress = seq_len + 1;
    VME->Write32( WriteAddressRegister, WriteAddress );


}



void TBSContrWZ::ProgramExecute()
{
    VME->Write32( StatusRegister, 0x0001 ); // uruchomienie programu
    unsigned short sr;
    while( !( (sr=VME->Read32(StatusRegister)) & 0x2 ) ); // czekanie az sie skonczy
    if(  sr & 0x0004 ) {    // sprawdz, czy wystapil blad
	  int last_inst = VME->Read32(ReadAddressRegister);
	  char buf[15];
	  sprintf( buf, "%x",last_inst - 1 );
	  throw EBSContr(": execution error : last instruction read from address " + string(buf) + "\n");
    }
    VME->Write32( StatusRegister, 0x0000 ); // przejscie do trybu spoczynkowego

    unsigned long rsl = RecSeqLength/16 + ((RecSeqLength%16) != 0);
    unsigned long offset = 0, cnt;
    unsigned long *RecData = new unsigned long[rsl];
    VME->Read( StartMemoryAddress + WriteAddress*4, (char*)RecData, rsl*sizeof(unsigned long) );
    TWZContrRecSeq wzcontrrecseq( RecData, RecSeqLength );
    while( RecSeqQueue.empty() != true ) {
	  cnt = RecSeqQueue.back().Count;
	  wzcontrrecseq.ToCharSequence( *(RecSeqQueue.back().Seq), offset, cnt );
	  RecSeqQueue.pop_back();
	  offset += cnt;
    }	
}






void TBSContrWZ::Transmit( TScanVector& send_seq )
{
  if( SendSeq == NULL )
	  throw EBSContr(": there was no ProgramStart(); statement\n");

  unsigned long to_send = send_seq.length(),
	              offset = 0;
	              //offset = 1;

  TScanVector rev_seq = send_seq;
  reverse(rev_seq.begin(), rev_seq.end());

  while( (to_send-offset) > MaxTransmissionLength ) {
    TScanVector seq( rev_seq, offset,  MaxTransmissionLength );
    SendSeq->CommTransmit( MaxTransmissionLength );
    SendSeq->SendData( seq );
    SendSeq->EndWordWithZeros();
    offset += MaxTransmissionLength;
  }

  if ((to_send-offset) != 1) {
  //if ((to_send-offset) != 0) {
    TScanVector seq( rev_seq, offset, to_send - offset -1 );
    SendSeq->CommTransmit( to_send - offset -1 );
    //SendSeq->CommTransmit( to_send - offset );
    SendSeq->SendData( seq );
    SendSeq->EndWordWithZeros();
  }

  SendSeq->CommTransmitExit( rev_seq[to_send-1] == '1' );
  //SendSeq->CommTransmitExit( send_seq[0] == '1' );

  TAPState = IDLE;
}



void TBSContrWZ::TransmitRec( TScanVector& send_seq, TScanVector& rec_seq )
{
  if( SendSeq == NULL )
    throw EBSContr(": there was no ProgramStart(); statement\n");

  unsigned long to_send = send_seq.length(),
                offset = 0;
                //offset = 1;     

  TScanVector rev_seq = send_seq;
  reverse(rev_seq.begin(), rev_seq.end());

  while( (to_send-offset) > MaxTransmissionLength ) {
    TScanVector seq( rev_seq, offset,  MaxTransmissionLength );
    SendSeq->CommTransmitRec( MaxTransmissionLength );
    SendSeq->SendData( seq );
    SendSeq->EndWordWithZeros();
    offset += MaxTransmissionLength;
  }

  if ((to_send-offset) != 1) {
  //if ((to_send-offset) != 0) {
    TScanVector seq( rev_seq, offset, to_send - offset -1 );
    SendSeq->CommTransmitRec( to_send - offset -1 );
    //SendSeq->CommTransmitRec( to_send - offset );
    SendSeq->SendData( seq );
    SendSeq->EndWordWithZeros();
  }

  SendSeq->CommTransmitRecExit( rev_seq[to_send-1] == '1' );
  //SendSeq->CommTransmitRecExit( send_seq[0] == '1' );

  rec_seq.reserve(to_send);
  RecSeqQueue.push_front( TRecSeqQueueItem(&rec_seq,to_send) );
  RecSeqLength += to_send;

  TAPState = IDLE;
}



void TBSContrWZ::Loop( unsigned long length )
{
  if( SendSeq == NULL )
    throw EBSContr(": there was no ProgramStart(); statement\n");

  while( length > MaxTransmissionLength ) {
    SendSeq->CommLoop( MaxTransmissionLength );
    length -= MaxTransmissionLength;
  }

  SendSeq->CommLoop( length - 1 );
  SendSeq->CommLoopExit();

  TAPState = IDLE;
}



void TBSContrWZ::LoopRec( unsigned long length, TScanVector& rec_seq )
{
  if( SendSeq == NULL )
    throw EBSContr(": there was no ProgramStart(); statement\n");

  while( length > MaxTransmissionLength ) {
    SendSeq->CommLoopRec( MaxTransmissionLength );
    length -= MaxTransmissionLength;
  }

  SendSeq->CommLoopRec( length - 1 );
  SendSeq->CommLoopRecExit();

  rec_seq.reserve(length);
  RecSeqQueue.push_front( TRecSeqQueueItem(&rec_seq,length) );
  RecSeqLength += length;

  TAPState = IDLE;
}


ostream& bs::operator<<( ostream& os, TBSContrWZ& bsc )
{
    os << "send sequence :\n";
    if( bsc.SendSeq==NULL)
	os << "NULL\n" ;
    else
	os <<  (*(bsc.SendSeq))  <<endl;
    return os;
}
