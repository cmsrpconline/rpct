#ifndef TBSCONTRIISENDSEQ_HH
#define TBSCONTRIISENDSEQ_HH

#include "tb_std.h"
#include "TBSContrIISeq.h"
#include "..\TScanVector.h"
//#include "..\BStypes.h"  
#include "TBSContr.h"

namespace bs {

class TBSContrIISendSeq : public TBSContrIISeq {
    unsigned short* Data;
    TBSContrIISendSeq* Next;
    int BitPosition;
    int DataIndex;
    const int DataSize;   // in sizeof(unsigned short)
    int Full;
    
    static unsigned long* ULBuffer; //used by function   unsigned short* GetAsUS();

    void increaseDataIndex();
    void insertBit(const int);
    void insertNumber(const unsigned short);
    int  currentInsertPosition() const;
    void copyDataToULBuffer( int ULBufferIndex ); // ULBufferIndex index which to copy at
public:
    TBSContrIISendSeq();
    ~TBSContrIISendSeq();
    
    void CommSetTAPState( TBSContr::TTAPState );
    
    void CommTransmit(int);
    void CommTransmitRec(int);
    void CommLoop(int);
    void CommLoopRec(int);
    
    void CommTransmitExit( int lastbit );
    void CommTransmitRecExit( int lastbit);
    void CommLoopExit();
    void CommLoopRecExit();
    
    void CommProgramEnd();
    
    void SendBit( const int i );
    void SendData( TScanVector& send_seq ); 
    void EndWordWithZeros();
    
    
    unsigned long GetLength();     // zwraca ilosc dwubajtowych slow
    unsigned long GetLengthBits();
    unsigned long* GetAsUL();
    
    friend std::ostream& operator<<(std::ostream&,TBSContrIISendSeq&);
};



inline void TBSContrIISendSeq::increaseDataIndex()
{
    BitPosition = 0;
    if( (++DataIndex ) == DataSize ) {
	Next = new TBSContrIISendSeq();
	Full = 1;
    };
}





   
inline void TBSContrIISendSeq::CommTransmit(int nbits)
{
    insertNumber( (this->*may_invert)( 0xE000 | ((unsigned short)nbits-1) ) );
}
    
inline void TBSContrIISendSeq::CommTransmitRec(int nbits)
{
    insertNumber( (this->*may_invert)( 0xF000 | ((unsigned short)nbits-1) ) );
}  

inline void TBSContrIISendSeq::CommLoop(int nbits)
{
    insertNumber( (this->*may_invert)( 0xC000 | ((unsigned short)nbits-1) ) );
}  

inline void TBSContrIISendSeq::CommLoopRec(int nbits)
{
    insertNumber( (this->*may_invert)( 0xD000 | ((unsigned short)nbits-1) ) );
}  


inline void TBSContrIISendSeq::CommTransmitExit(int lastbit)
{
    insertNumber( (this->*may_invert)( 0x900C | ((unsigned short)lastbit) ) );
}  

inline void TBSContrIISendSeq::CommTransmitRecExit(int lastbit)
{
    insertNumber( (this->*may_invert)( 0x900E | ((unsigned short)lastbit) ) );
}  
  
inline void TBSContrIISendSeq::CommLoopExit()
{
    insertNumber( (this->*may_invert)( 0x9008 ) );
} 
 
inline void TBSContrIISendSeq::CommLoopRecExit()
{
    insertNumber( (this->*may_invert)( 0x900A ) );
}  

inline void TBSContrIISendSeq::CommProgramEnd()
{
    insertNumber( (this->*may_invert)( 0x8000 ) );
}  


inline void TBSContrIISendSeq::SendBit( const int i )
{
  //G_Log.out() << "TBSContrIISendSeq::SendBit:" << i << std::endl;
  insertBit( i );
  if( 15== ( currentInsertPosition() % 16 ) )
    insertBit( 0 );
}


} // namespace bs

#endif
