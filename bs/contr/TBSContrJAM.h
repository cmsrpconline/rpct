//---------------------------------------------------------------------------

#ifndef TBSContrJAMH
#define TBSContrJAMH
//---------------------------------------------------------------------------


#include "TBSContr.h"

extern "C" {
#include "../jp_22/source/jamport.h"
#include "../jp_22/source/jamexprt.h"
#include "../jp_22/source/jamjtag.h"

#if PORT == WINDOWS
extern BOOL windows_nt;
extern BOOL jtag_hardware_initialized;
extern BOOL specified_lpt_addr;
extern int lpt_port;
extern WORD lpt_addr;
void close_jtag_hardware();
#endif

};

#include <deque>
#include "IJtagIO.h"


namespace bs {

class TBSContrJAM: public TBSContr  {
private:
  class TCommand {
  protected:
    void CheckResult(JAM_RETURN_TYPE result)
      {
        if (result != JAMC_SUCCESS)
          switch (result) {
            case JAMC_OUT_OF_MEMORY: throw EBSContr("out of memory");
          };
      }
    long* ScanVectorToArray(const TScanVector& seq);
    void ArrayToScanVector(long* data, int count, TScanVector& seq);
    TBSContrJAM& BSContr; 
    IJtagIO* JtagIO;
  public:
    TCommand(TBSContrJAM& bscontr) : BSContr(bscontr), JtagIO(bscontr.JtagIO) {}
    virtual ~TCommand() {}
    virtual void Execute() = 0;
  };
  friend class TCommand;
  
  class TIRTransmit: public TCommand {
    TScanVector SendSeq;
  public:
    TIRTransmit(TBSContrJAM& bscontr, TScanVector& send_seq)
      : TCommand(bscontr), SendSeq(send_seq) {}
      
    virtual void Execute()
      {
        long* data = ScanVectorToArray(SendSeq);
        SetJTAGIO(JtagIO);
        try {
          CheckResult(jam_do_irscan(SendSeq.size(), data, 0));
        }
        catch(...) {
          delete [] data;
          throw;
        };
        delete [] data;
      }
  };

  class TDRTransmit: public TCommand {
    TScanVector SendSeq;
  public:
    TDRTransmit(TBSContrJAM& bscontr, TScanVector& send_seq)
      : TCommand(bscontr), SendSeq(send_seq) {}
    virtual void Execute()
      {
        long* data = ScanVectorToArray(SendSeq);    
        SetJTAGIO(JtagIO);
        try {
          CheckResult(jam_do_drscan(SendSeq.size(), data, 0));
        }
        catch(...) {  
          delete [] data;
          throw;
        };
        delete [] data;
      }
  };

  class TIRTransmitRec: public TCommand {
    TScanVector SendSeq;                   
    TScanVector& RecSeq;
  public:
    TIRTransmitRec(TBSContrJAM& bscontr, TScanVector& send_seq,
                   TScanVector& rec_seq)
      : TCommand(bscontr), SendSeq(send_seq), RecSeq(rec_seq) {}

    virtual void Execute()
      {
        int count = SendSeq.size();
        long* data = ScanVectorToArray(SendSeq);
        long* recdata = new long[(count + sizeof(long) * 8 - 1) / (sizeof(long) * 8)];   
        SetJTAGIO(JtagIO);
        try {
          CheckResult(jam_swap_ir(count,
                                  data,
                                  0,
                                  recdata,
                                  0));
          ArrayToScanVector(recdata, count, RecSeq);
        }
        catch(...) {
          delete [] data;
          delete [] recdata;
          throw;
        };
        delete [] data;     
        delete [] recdata;
      }
  };     

  class TDRTransmitRec: public TCommand {
    TScanVector SendSeq;                   
    TScanVector& RecSeq;
  public:
    TDRTransmitRec(TBSContrJAM& bscontr, TScanVector& send_seq,
                   TScanVector& rec_seq)
      : TCommand(bscontr), SendSeq(send_seq), RecSeq(rec_seq) {}

    virtual void Execute()
      {
        int count = SendSeq.size();
        long* data = ScanVectorToArray(SendSeq);
        long* recdata = new long[(count + sizeof(long) - 1) / sizeof(long)];
        SetJTAGIO(JtagIO);
        try {
          CheckResult(jam_swap_dr(count,
                                  data,
                                  0,
                                  recdata,
                                  0));
          ArrayToScanVector(recdata, count, RecSeq);
        }
        catch(...) {
          delete [] data;
          delete [] recdata;
          throw;
        };
        delete [] data;     
        delete [] recdata;
      }
  };

  class TSetTAPState: public TCommand {
  private:
    TTAPState TAPState;
  public:
    TSetTAPState(TBSContrJAM& bscontr, TTAPState tapState)
      : TCommand(bscontr), TAPState(tapState) {}
    virtual void Execute();
  };
  
  class TLoopRec: public TCommand {    
  private:
    unsigned long Length;     
    TScanVector& RecSeq;
  public:
    TLoopRec(TBSContrJAM& bscontr, unsigned long length, TScanVector& rec_seq)
      : TCommand(bscontr), Length(length), RecSeq(rec_seq) {}

    // we start in Shift-IR or DR, we end in IDLE state
    virtual void Execute()
      {
        RecSeq.resize(Length);
        for (unsigned long i = 0; i < (Length-1); i++)
          RecSeq[i] = (JtagIO->JtagIOLoop(0) ? '1' : '0');

        // last bit is sent during shift-IR -> Exit1-IR transition   
        RecSeq[Length-1] = (JtagIO->JtagIOLoop(1) ? '1' : '0');

        // we are in EXIT1-IR, go to IDLE state
        JtagIO->JtagIOLoop(1);
        JtagIO->JtagIOLoop(0);
      }
  };
  
  class TLoop: public TCommand {       
  private:
    unsigned long Length;
  public:
    TLoop(TBSContrJAM& bscontr, unsigned long length)
      : TCommand(bscontr), Length(length) {}

    // we start in Shift-IR or DR, we end in IDLE state
    virtual void Execute()
      {
        for (unsigned long i = 0; i < (Length-1); i++)
          JtagIO->JtagIOLoop(0);

        // last bit is sent during shift-IR -> Exit1-IR transition   
        JtagIO->JtagIOLoop(1);

        // we are in EXIT1-IR, go to IDLE state
        JtagIO->JtagIOLoop(1);
        JtagIO->JtagIOLoop(0);
      }
  };

  typedef std::deque<TCommand*> TCommandQueue;
  TCommandQueue CommandQueue;

  IJtagIO* JtagIO;
public:
    TBSContrJAM(int lpt_port_addr = 0x378) : JtagIO(NULL)
      {
#if PORT == WINDOWS
        specified_lpt_addr = TRUE;
        lpt_addr = (WORD) lpt_port_addr;
        lpt_port = 1;
#endif
        jam_init_jtag();  
      }
    TBSContrJAM(IJtagIO& jtagIO) : JtagIO(&jtagIO)
      {
        jam_init_jtag();
        SetJTAGIO(JtagIO);
      }
    virtual ~TBSContrJAM()
      {
#       if PORT == WINDOWS
          if (jtag_hardware_initialized && (JtagIO == NULL))
            close_jtag_hardware();
#       endif
      }

    virtual void ProgramStart();
    virtual void ProgramEnd() {}
    virtual void ProgramExecute();

    virtual void IRTransmit(TScanVector& send_seq)
      {
        CommandQueue.push_back(new TIRTransmit(*this, send_seq));
      }

    virtual void IRTransmitRec(TScanVector& send_seq, TScanVector& rec_seq)
      {
        CommandQueue.push_back(new TIRTransmitRec(*this, send_seq, rec_seq));
      }

    virtual void IRLoop(unsigned long length)
      {
        SetTAPState(SHIFT_IR);
        CommandQueue.push_back(new TLoop(*this, length));
      }

    virtual void IRLoopRec(unsigned long length, TScanVector& rec_seq)
      {
        SetTAPState(SHIFT_IR);
        CommandQueue.push_back(new TLoopRec(*this, length, rec_seq));
      }

    virtual void DRTransmit(TScanVector& send_seq) 
      {
        CommandQueue.push_back(new TDRTransmit(*this, send_seq));
      }

    virtual void DRTransmitRec(TScanVector& send_seq, TScanVector& rec_seq)
      {
        CommandQueue.push_back(new TDRTransmitRec(*this, send_seq, rec_seq));
      }

    virtual void DRLoop(unsigned long length)
      {
        SetTAPState(SHIFT_DR);
        CommandQueue.push_back(new TLoop(*this, length));
      }

    virtual void DRLoopRec(unsigned long length, TScanVector& rec_seq)
      {
        SetTAPState(SHIFT_DR);
        CommandQueue.push_back(new TLoopRec(*this, length, rec_seq));
      }

    virtual void TAPtoReset()
      {
        SetTAPState(RESET);
      }
    virtual void TAPtoIdle()
      {             
        SetTAPState(IDLE);
      }

    virtual void SetTAPState(TTAPState state)
      {     
        CommandQueue.push_back(new TSetTAPState(*this, state));
      }

    friend std::ostream& operator<<(std::ostream&, TBSContr&);

};


  
} // namespace bs

#endif
