#ifndef TWZCONTRRECSEQ_HH
#define TWZCONTRRECSEQ_HH

#include "TWZContrSeq.h"
#include "TScanVector.h"

namespace bs {

class TWZContrRecSeq : public TWZContrSeq {
    unsigned long* RecData;
    unsigned long Count;
public:
    TWZContrRecSeq( unsigned long* rec_data, unsigned long count ) : TWZContrSeq(),
	RecData(rec_data), Count(count) {}
    ~TWZContrRecSeq() {}

    void ToCharSequence( TScanVector&, unsigned long offset, unsigned long cnt );
};


} // namespace bs

#endif
