#ifndef TWZCONTRSEQ_HH
#define TWZCONTRSEQ_HH

#include <typeinfo>

namespace bs {

class TWZContrSeq {
    typedef union {
	unsigned char tab[2];
	unsigned short num;
    } inv_union;

    unsigned short invert( unsigned short n );
    unsigned short do_nothing( unsigned short n );
    

protected:
    int hi_order;
    unsigned short mask[16];
    //unsigned short (TWZContrSeq::*may_invert)( unsigned short n );
    unsigned short (TWZContrSeq:: *may_invert)( unsigned short n );
public:
    TWZContrSeq();
    virtual ~TWZContrSeq() {}
    unsigned short mi( unsigned short n );
};



inline unsigned short TWZContrSeq::invert( unsigned short n )
{
    inv_union u;
    u.num=n;
    unsigned char temp=u.tab[0];
    u.tab[0] = u.tab[1];
    u.tab[1] = temp;
    return u.num;
}

inline unsigned short TWZContrSeq::do_nothing( unsigned short n )
{
    return n;
}

//inline unsigned short TWZContrSeq::may_invert( unsigned short n )
//{
//    return ((hi_order) ? invert(n) : n);
//}


inline    unsigned short TWZContrSeq::mi( unsigned short n )
{
    return (this->*may_invert) (n);
}


} // namespace bs

#endif
