//---------------------------------------------------------------------------

#include "precompiled.h"
#pragma hdrstop

#include "TBSContrII.h"
#include "TBSContrIIRecSeq.h"
#include <algorithm>


//---------------------------------------------------------------------------

#pragma package(smart_init)


using namespace bs;  

TBSContrII::TBSContrII(TIIDevice& device,
                       TID WORD_RSELECTOR_,
                       TID WORD_RSTEER_,
                       TID WORD_RDATAIN_,
                       TID WORD_RDATAOUT_,
                       TID WORD_RINREAD_,
                       TID WORD_RINWRITE_,
                       TID WORD_ROUTREAD_,
                       TID WORD_ROUTWRITE_,
                       TID WORD_STATUS_,
                       unsigned long memorySize)
 : Device(device),
   WORD_RSELECTOR(WORD_RSELECTOR_),
   WORD_RSTEER(WORD_RSTEER_),
   WORD_RDATAIN(WORD_RDATAIN_),
   WORD_RDATAOUT(WORD_RDATAOUT_),
   WORD_RINREAD(WORD_RINREAD_),
   WORD_RINWRITE(WORD_RINWRITE_),
   WORD_ROUTREAD(WORD_ROUTREAD_),
   WORD_ROUTWRITE(WORD_ROUTWRITE_),
   WORD_STATUS(WORD_STATUS_),
   MemorySize(memorySize), MaxTransmissionLength(0x1000),
   WriteAddress(0), SendSeq(NULL), RecSeqLength(0), TAPState(RESET), Channel(1)
{
}


void TBSContrII::ProgramEnd()
{
  if( SendSeq == NULL )
    throw EBSContr(": there was no ProgramStart(); statement\n");

  SendSeq->CommProgramEnd();

  unsigned long seq_len = SendSeq->GetLength(); //kopiowanie SendSeq do pamieci kontrolera
  if( seq_len > MemorySize )
    throw EBSContrOutOfMemory(": To little memory to load the program to the controller");

  if( (seq_len + RecSeqLength/16 + 1) > MemorySize )
    throw EBSContrOutOfMemory(": To little memory to receive the data in the controller");

  // wybor ukladu dokonujacego obslugi pamieci
  Device.WriteWord(WORD_RSELECTOR, 0, 0UL);

  Sleep(10);

  // adres pod ktory wprowadzamy program
  Device.WriteWord(WORD_RINWRITE, 0, StartMemoryAddress);  

  Sleep(10);
  
  // zaladowanie wewnetrznego rejestru adresu zapisu wartoscia przechowywana w RINWRITE
  Device.WriteWord(WORD_RSTEER, 0, 0xa);       

  Sleep(10);

  // sprawdz, czy uklad zakonczyl operacje zapisu wewnetrznego rejestru adresu zapisu
  //if (Device.ReadBits(BITS_RSTATUS) != 0x002)
  //  throw EBSContr(": could not write program to controller memory");  

  //Sleep(10);

  unsigned long* data = SendSeq->GetAsUL();
  for (unsigned long i=0; i<seq_len; i++) {
    Device.WriteWord(WORD_RDATAIN, 0, data[i]);

    //Sleep(10);

    // zapis danych przechowywanych w rejestrze DATAIN do komorki pamieci
    // okreslanej przez wewnetrzny rejestr adresu zapisu
    Device.WriteWord(WORD_RSTEER, 0, 0x3);

    //Sleep(10);

    // sprawdz, czy uklad zakonczyl operacje zapisu wewnetrznego rejestru adresu zapisu
    //if (Device.ReadBits(BITS_RSTATUS) != 0x002)
    //  throw EBSContr(": could not write program to controller memory");

    //Sleep(10);

    // inkrementacja adresu
    Device.WriteWord(WORD_RSTEER, 0, 0x6);

    //Sleep(10);

    // sprawdz, czy uklad zakonczyl operacje zapisu wewnetrznego rejestru adresu zapisu
    //if (Device.ReadBits(BITS_RSTATUS) != 0x002)
    //  throw EBSContr(": could not write program to controller memory");

    //Sleep(10);
  };

  //unsigned long* seq_ptr = SendSeq->GetAsUL();
  //VME->Write( StartMemoryAddress, (char*)seq_ptr, sizeof(unsigned long) * seq_len );

  WriteAddress = StartMemoryAddress + seq_len + 1;
  //VME->Write32( WriteAddressRegister, WriteAddress );

  /*
  // zaladowanie wewnetrznego rejestru adresu zapisu wartoscia przechowywana w RINWRITE
  Device.WriteWord(WORD_RSTEER, 0, 0xb);

  // sprawdz, czy uklad zakonczyl operacje zapisu wewnetrznego rejestru adresu zapisu
  unsigned long status = Device.ReadBits(BITS_RSTATUS);
  G_Log.out() << "status = " << status << std::endl;
  if (status != 0x002)
    throw EBSContr(": could not write program to controller memory");

  unsigned long* data = SendSeq->GetAsUL();
  for (unsigned long i=0; i<seq_len; i++) {
    Device.WriteWord(WORD_RDATAIN, 0, data[i]);

    // zapis danych przechowywanych w rejestrze DATAIN do komorki pamieci
    // okreslanej przez wewnetrzny rejestr adresu zapisu
    Device.WriteWord(WORD_RSTEER, 0, 0x7);

    // sprawdz, czy uklad zakonczyl operacje zapisu wewnetrznego rejestru adresu zapisu
    if (Device.ReadBits(BITS_RSTATUS) != 0x002)
      throw EBSContr(": could not write program to controller memory");
  };

  //unsigned long* seq_ptr = SendSeq->GetAsUL();
  //VME->Write( StartMemoryAddress, (char*)seq_ptr, sizeof(unsigned long) * seq_len );

  WriteAddress = StartMemoryAddress + seq_len + 1;
  //VME->Write32( WriteAddressRegister, WriteAddress );
  */
}


void TBSContrII::ProgramExecute()
{
  Device.WriteWord(WORD_RSELECTOR, 0, Channel);         

  //Sleep(10);

  // adres poprzednio uzyty do zapisu programu
  Device.WriteWord(WORD_RINREAD, 0, StartMemoryAddress);

  //Sleep(10);

  //  adres pod ktory beda wprowadzane bity odczytane z lancucha
  Device.WriteWord(WORD_RINWRITE, 0, WriteAddress);

  //Sleep(10);

  // uruchomienie modulu kontrolujacego wybrany kanal
  Device.WriteWord(WORD_RSTEER, 0, 0x1);         

  //Sleep(10);

  unsigned long statusMask = 0xf06;
  unsigned long stopValue = (Channel << 8) | 0x2;
  unsigned long errorValue = (Channel << 8) | 0x6;

  unsigned long status;
  int i = 0;
  do {
    status = Device.ReadWord(WORD_STATUS, 0) & statusMask;
    if (status == errorValue)
      throw EBSContr(": execution error");
    if (i++ > 100)
      throw EBSContr(": execution error, timeout");  

    Sleep(10);
  } while (status != stopValue);

  /*unsigned short sr;
  while( !( (sr=VME->Read32(StatusRegister)) & 0x2 ) ); // czekanie az sie skonczy
  if(  sr & 0x0004 ) {    // sprawdz, czy wystapil blad
    int last_inst = VME->Read32(ReadAddressRegister);
    char buf[15];
    sprintf( buf, "%x",last_inst - 1 );
    throw EBSContr(": execution error : last instruction read from address " + string(buf) + "\n");
  }
  VME->Write32( StatusRegister, 0x0000 ); // przejscie do trybu spoczynkowego*/

  // odczytanie danych odebranych z lancucha

  unsigned long rsl = RecSeqLength/16 + ((RecSeqLength%16) != 0);
  unsigned long offset = 0, cnt;
  unsigned long *recData = new unsigned long[rsl];

  //VME->Read( StartMemoryAddress + WriteAddress*4, (char*)RecData, rsl*sizeof(unsigned long) );
  Device.WriteWord(WORD_RSELECTOR, 0, 0UL);  

  //Sleep(10);

  // adres poprzednio uzyty do zapisu bitow odczytanych z lancucha
  Device.WriteWord(WORD_RINREAD, 0, WriteAddress);

  //Sleep(10);

  // zaladowanie wewnetrznego rejestru adresu odczytu wartoscia przechowywana w RINREAD
  Device.WriteWord(WORD_RSTEER, 0, 0x8);

  //Sleep(10);

  // sprawdz, czy uklad zakonczyl operacje zapisu wewnetrznego rejestru adresu odczytu
  //if (Device.ReadBits(BITS_RSTATUS) != 0x002)
  //  throw EBSContr(": could not read program from controller memory");

  //Sleep(10);

  for (i = 0; i < rsl; i++) {
    // uruchomienie modulu odczytujacego dane spod adresu RINREAD
    Device.WriteWord(WORD_RSTEER, 0, 0x1);

    //Sleep(10);

    // sprawdz, czy uklad zakonczyl operacje zapisu wewnetrznego rejestru adresu odczytu
    //if (Device.ReadBits(BITS_RSTATUS) != 0x002)
    //  throw EBSContr(": could not read program from controller memory");

    //Sleep(10);

    // RDATAOUT powinien przechowywac pierwsza czesc odczytanych bitow - 16
    recData[i] = Device.ReadWord(WORD_RDATAOUT, 0);

    //Sleep(10);

    // inkrementacja adresu odczytu
    Device.WriteWord(WORD_RSTEER, 0, 0x4);

    //Sleep(10);

    // sprawdz, czy uklad zakonczyl operacje zapisu wewnetrznego rejestru adresu odczytu
    //if (Device.ReadBits(BITS_RSTATUS) != 0x002)
    //  throw EBSContr(": could not read program from controller memory");

    //Sleep(10);
  };

  /*
  // zaladowanie wewnetrznego rejestru adresu odczytu wartoscia przechowywana w RINREAD
  Device.WriteWord(WORD_RSTEER, 0, 0x9);

  // sprawdz, czy uklad zakonczyl operacje zapisu wewnetrznego rejestru adresu odczytu
  if (Device.ReadBits(BITS_RSTATUS) != 0x002)
    throw EBSContr(": could not read program from controller memory");

  for (i = 0; i < rsl; i++) {
    // uruchomienie modulu odczytujacego dane spod adresu RINREAD
    Device.WriteWord(WORD_RSTEER, 0, 0x5);

    // RDATAOUT powinien przechowywac pierwsza czesc odczytanych bitow - 16
    recData[i] = Device.ReadWord(WORD_RDATAOUT, 0);

    // sprawdz, czy uklad zakonczyl operacje zapisu wewnetrznego rejestru adresu odczytu
    if (Device.ReadBits(BITS_RSTATUS) != 0x002)
      throw EBSContr(": could not read program from controller memory");
  };
  */

  TBSContrIIRecSeq recseq(recData, RecSeqLength);
  while (RecSeqQueue.empty() != true) {
    cnt = RecSeqQueue.back().Count;
    recseq.ToCharSequence( *(RecSeqQueue.back().Seq), offset, cnt );
    RecSeqQueue.pop_back();
    offset += cnt;
  };

  delete [] recData;
}




void TBSContrII::Transmit( TScanVector& send_seq )
{
  if( SendSeq == NULL )
	  throw EBSContr(": there was no ProgramStart(); statement\n");

  unsigned long to_send = send_seq.length(),
	              offset = 0;
	              //offset = 1;

  TScanVector rev_seq = send_seq;
  std::reverse(rev_seq.begin(), rev_seq.end());

  while( (to_send-offset) > MaxTransmissionLength ) {
    TScanVector seq( rev_seq, offset,  MaxTransmissionLength );
    SendSeq->CommTransmit( MaxTransmissionLength );
    SendSeq->SendData( seq );
    SendSeq->EndWordWithZeros();
    offset += MaxTransmissionLength;
  }

  if ((to_send-offset) != 1) {
  //if ((to_send-offset) != 0) {
    TScanVector seq( rev_seq, offset, to_send - offset -1 );
    SendSeq->CommTransmit( to_send - offset -1 );
    //SendSeq->CommTransmit( to_send - offset );
    SendSeq->SendData( seq );
    SendSeq->EndWordWithZeros();
  }

  SendSeq->CommTransmitExit( rev_seq[to_send-1] == '1' );
  //SendSeq->CommTransmitExit( send_seq[0] == '1' );

  TAPState = IDLE;
}



void TBSContrII::TransmitRec( TScanVector& send_seq, TScanVector& rec_seq )
{
  if( SendSeq == NULL )
    throw EBSContr(": there was no ProgramStart(); statement\n");

  unsigned long to_send = send_seq.length(),
                offset = 0;
                //offset = 1;     

  TScanVector rev_seq = send_seq;
  std::reverse(rev_seq.begin(), rev_seq.end());

  while( (to_send-offset) > MaxTransmissionLength ) {
    TScanVector seq( rev_seq, offset,  MaxTransmissionLength );
    SendSeq->CommTransmitRec( MaxTransmissionLength );
    SendSeq->SendData( seq );
    SendSeq->EndWordWithZeros();
    offset += MaxTransmissionLength;
  }

  if ((to_send-offset) != 1) {
  //if ((to_send-offset) != 0) {
    TScanVector seq( rev_seq, offset, to_send - offset -1 );
    SendSeq->CommTransmitRec( to_send - offset -1 );
    //SendSeq->CommTransmitRec( to_send - offset );
    SendSeq->SendData( seq );
    SendSeq->EndWordWithZeros();
  }

  SendSeq->CommTransmitRecExit( rev_seq[to_send-1] == '1' );
  //SendSeq->CommTransmitRecExit( send_seq[0] == '1' );

  rec_seq.reserve(to_send);
  RecSeqQueue.push_front( TRecSeqQueueItem(&rec_seq,to_send) );
  RecSeqLength += to_send;

  TAPState = IDLE;
}



void TBSContrII::Loop( unsigned long length )
{
  if( SendSeq == NULL )
    throw EBSContr(": there was no ProgramStart(); statement\n");

  while( length > MaxTransmissionLength ) {
    SendSeq->CommLoop( MaxTransmissionLength );
    length -= MaxTransmissionLength;
  }

  SendSeq->CommLoop( length - 1 );
  SendSeq->CommLoopExit();

  TAPState = IDLE;
}



void TBSContrII::LoopRec( unsigned long length, TScanVector& rec_seq )
{
  if( SendSeq == NULL )
    throw EBSContr(": there was no ProgramStart(); statement\n");

  while( length > MaxTransmissionLength ) {
    SendSeq->CommLoopRec( MaxTransmissionLength );
    length -= MaxTransmissionLength;
  }

  SendSeq->CommLoopRec( length - 1 );
  SendSeq->CommLoopRecExit();

  rec_seq.reserve(length);
  RecSeqQueue.push_front( TRecSeqQueueItem(&rec_seq,length) );
  RecSeqLength += length;

  TAPState = IDLE;
}


std::ostream& bs::operator<<( std::ostream& os, TBSContrII& bsc )
{
    os << "send sequence :\n";
    if( bsc.SendSeq==NULL)
	os << "NULL\n" ;
    else
	os <<  (*(bsc.SendSeq)) << std::endl;
    return os;
}
