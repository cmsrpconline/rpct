-- Data Register1
--
-- Input_masks (selected bits are set to "1"): 
DR1.MMS1[0..17] = #FFFFF;
DR1.MMS2[0..3] = #F;
DR1.MMS3[0..13] = #FFFF;
DR1.MMS4[0..17] = #FFFFF;
-- Hpt bit (bit(5)):
DR1.FF3= #0;
-- Negative group bit (sign bit of negatives groups (bit(4)) set to "1"):
DR1.FF2=#0;
-- Positive group bit (sign bit of positives groups (bit(4)) set to "1"):
DR1.FF1=#0;
-- Output register bit:
DR1.F6=#0;
-- Output_masks (selected bits are set to "0"): 
DR1.M[0..6] = #00;
-- Cascade delays:
DR1.F5=#1;
DR1.F4=#1;
DR1.F3=#1;
DR1.F2=#1;
--
--  Data Register3  (programming bits related to ref ms2(2))
--
--  Positive patterns Multiplexers and OR group Demultiplexers:
--

--  Negative patterns Multiplexers and OR group Demultiplexers:
--
-- OR group 1_8 (dmux):



DR1.DO1_61[0..3]=#7;	-- active!!!
-- Patterns (mux's):
DR1.P159_MX1[0..3]=#F;
DR1.P159_MX3[0..3]=#2;	-- wplywa na dwa(60)
DR1.P159_MX4[0..3]=#2;	-- wplywa na dwa(60)
