typedef union {
    unsigned long  val;  /* For returning numbers.                   */
    char  id[128];   /* For returning identifier names    */
} YYSTYPE;

#ifndef YYLTYPE
typedef
  struct yyltype
    {
      int timestamp;
      int first_line;
      int first_column;
      int last_line;
      int last_column;
      char *text;
   }
  yyltype;

#define YYLTYPE yyltype
#endif

#define	P_MS1	258
#define	P_MS2	259
#define	P_MS3	260
#define	P_MS4	261
#define	P_CODE_IN	262
#define	P_CODE_OUT	263
#define	P_TEST_NO	264
#define	P_USE_PATTERNS	265
#define	P_NUM	266
#define	P_ID	267


extern YYSTYPE yylval;
