#include <iostream.h>
#include <stdio.h>
#include <stdlib.h>
#include <new>
#include <libgen.h>
#include "TException.hh"
#include "tb.h"
#include "TMag.hh"

int debug;

int testpacparse(  FILE* file, FILE* errptr, FILE* loadpat[2], 
		   char* cur_pat, int count, unsigned uwait );




const char* usage = "usage: testpac [-d] [-c count] [-w wait] file.bsc  file1.pt ... \n";


int main( int argc, char ** argv )
{
    //   char c;
    try {
	set_new_handler(my_new_handler);
	debug = 0;
	//int opt_no=1;
	char cbuf[64];
	
	cout << hex;
	cerr << hex;
	
	int optc;
	extern char *optarg;
	extern int optind;
	int count = 1;
	unsigned uwait = 1;
	int errflg = 0;
        //  char *ofile = NULL;

	while ((optc = getopt(argc, argv, "dc:w:")) != EOF)
	    switch (optc) {
	    case 'd':
		debug=1;
		break;
	    case 'c':
		count = atoi(optarg);
		cout << "count = " << count << endl;
		break;
	    case 'w':
		uwait = atof(optarg) * 1000000.0;
		cout << "uwait = " << uwait << endl;
		break;
	    case '?':
		errflg++;
	    }
	if( errflg || (optind+2)>argc )
	    throw TException( usage );
    /*////////////////////////////////////////
      if( argc < 3 ) 
      throw TException( usage );
      
	    if( !strcmp( argv[opt_no], "-d" ) ) {
	    debug = 1;
	    opt_no++;
	    }
	/////////////////////////////////*/
	cout << "optind=" << optind << " arg=" << argv[optind] << endl;
	FILE* loadpat[2];
	strcpy( cbuf, "loadpat " );
	strcat( cbuf, argv[optind++] );
	if( p2open( cbuf, loadpat) < 0) 
	    throw TException(" error opening loadpat " );

	FILE* ptfile;


	if( YES!=VME.Open() ) {
	    cerr << "Problems opening VME\n";
	    exit(1);
	}
	if( YES!= VME.ResetWait() ) {
	    cerr<<"nie moglem zresetowac hard\n";
	    exit(1);
	}
	char current_pat[256];
	strcpy( current_pat, "" );
	
	while( optind < argc ) {
	    cout << "optind=" << optind << " arg=" << argv[optind] << endl;
	    cout.flush();
	    if( NULL == (ptfile = fopen( argv[optind++], "r" )) )
		throw TException( "Couldn't open file " + string(argv[optind-1]) + "\n" );
	    cout << "Executing " << argv[optind-1] << " ... ";
	    cout.flush();
	    testpacparse( ptfile, stderr, loadpat, current_pat, count, uwait);
	    cout << "Done" << endl;
	    cout.flush();
	    fclose(ptfile);
	}
	
	//cout << "zamykam pipe\n";
	//cin >> c;
        write( fileno(loadpat[0]), "quit\n", 6 );
	//cout << "zamykam pipe\n";
	//cin >> c;
	if( p2close(loadpat) );
	//	    cerr << "p2close error\n";
    }
    catch( TException& e ) {
	cerr << e.what() << endl;
    }
    catch( ... ) {
	cerr << "Exception\n";
    }
    
    VME.Close();
    return 0;
}
