-- Data Register1
--
-- Input_masks (selected bits are set to "1"): 
DR1.MMS1[0..17] = #3FFFF;
DR1.MMS2[0..3] = #F;
DR1.MMS3[0..13] = #3FFF;
DR1.MMS4[0..17] = #3FFFF;
-- Hpt bit (bit(4)):
DR1.FF3= #0;
-- Positive group bit (sign bit of positives groups (bit(5)) set to "1"):
DR1.FF2=#0;
-- Negative group bit (sign bit of negatives groups (bit(5)) set to "1"):
DR1.FF1=#1;
-- Output register bit:
DR1.F6=#0;
-- Output_masks (selected bits are set to "0"): 
DR1.M[0..6] = #00;
-- Cascade delays:
DR1.F5=#1;
DR1.F4=#1;
DR1.F3=#1;
DR1.F2=#1;
--  Data Register3  (programming bits related to ref ms2(2))
--
--  Positive patterns Multiplexers and OR group Demultiplexers:
--

--  Negative patterns Multiplexers and OR group Demultiplexers:
--
-- OR group 1_1 (dmux):
DR2.DO1_16[0..3]=#1;
-- Patterns (mux's):
DR2.P065_MX1[0..3]=#1;
DR2.P065_MX3[0..3]=#1;
DR2.P065_MX4[0..3]=#1;
