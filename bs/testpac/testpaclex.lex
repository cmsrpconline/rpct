%{
#include "testpacparse.tab.h"
extern YYLTYPE yylloc;
%}
%option yylineno
DIGIT    [0-9]
HEXDIGIT [0-9a-fA-F]
/* BINDIGIT [0-1] */
ID       [a-zA-Z][_.a-zA-Z0-9]{0,31}



%%

MS1              return P_MS1;
MS2              return P_MS2;
MS3              return P_MS3;
MS4              return P_MS4;
CODE_IN          return P_CODE_IN;
CODE_OUT         return P_CODE_OUT;

test_no          return P_TEST_NO;
use_patterns     return P_USE_PATTERNS;


#{HEXDIGIT}+     {
                    sscanf(yytext,"#%lx",&yylval.val);
		    yylloc.first_line = yylineno;
		    yylloc.last_line = yylineno;
                    return P_NUM;
                 }

{DIGIT}+         {   
                    sscanf(yytext,"%ld",&yylval.val);
		    yylloc.first_line = yylineno;
		    yylloc.last_line = yylineno;
                    return P_NUM;
                 }



{ID}             {
                    strcpy(yylval.id, yytext);
		    yylloc.first_line = yylineno;
		    yylloc.last_line = yylineno;
		    return P_ID;
                 }




"--"[^\n]*\n    /* eat up one-line comments */

[ \t\n]+

"="           {
		    yylloc.first_line = yylineno;
		    yylloc.last_line = yylineno;
		    return '=';
              } 
";"           {
		    yylloc.first_line = yylineno;
		    yylloc.last_line = yylineno;
		    return ';';
              }

<<EOF>>      { YY_FLUSH_BUFFER; yyterminate(); }

%%
