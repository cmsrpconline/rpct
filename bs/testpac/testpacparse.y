%{
#define YYDEBUG 1
#define YYERROR_VERBOSE
#include <stdio.h>
int  yylex();
void yyrestart(FILE*);
extern "C" { 
    int yyerror(char *); 
    int senderrmsg();
 	   }
 
extern int debug;
char* current_pat;
FILE* loadpat_input;
FILE* loadpat_output;
int test_no;

#define BUFFLEN 256
char buffer[BUFFLEN];

#include <new>
#include "TException.hh"

#include "TMag.hh"
TMemory* mem;
TMag* mag;
TSignalSequence* sigseq;
unsigned long first_part, sec_part;


#define SWITCH_SIG(x)    (((x)     &  0x1ul)     << 31)
#define CODE_OUT_SIG(x)  (((x)     &  0x7ful)    << 24)
#define MS1_SIG(x)        ((x)     &  0x3fffful)
#define MS2_SIG(x)       (((x)     &  0xful)     << 18)
#define MS3_0_SIG(x)     (((x)     &  0x3ul)     << 22)

#define MS3_1_SIG(x)     (((x)>>2) &  0xffful) 
#define MS4_SIG(x)       (((x)     &  0x3fffful) << 12 ) 
%}

%union {
    unsigned long  val;  /* For returning numbers.                   */
    char  id[128];   /* For returning identifier names    */
}
     
%token P_MS1 P_MS2 P_MS3 P_MS4
%token P_CODE_IN P_CODE_OUT
%token P_TEST_NO P_USE_PATTERNS

%token <val>   P_NUM
%token <id>    P_ID


%%     /* Grammar */

input:   
       | input line
;


line:     
        command ';'           
;



command:  P_TEST_NO P_NUM              {
                                           if(debug)      
					       cout << "test_no = " << $2 << endl;
					   test_no = $2;
					   first_part = SWITCH_SIG(0);
					   sec_part   = SWITCH_SIG(1);
                                       }
          | P_USE_PATTERNS  P_ID       {
                                           if(debug)      
					       cout << "use pat " << $2 << endl;
					   if( strcmp( $2, current_pat ) ) {
					       strcpy( buffer, $2 );
					       strcat( buffer, ".pat\n" );
					       write(fileno(loadpat_input),
						     buffer, BUFFLEN);

					       //fputs( buffer, loadpat_input );
					       strcpy( current_pat, $2 );
					       
					       ssize_t ret;
					       do {
						   ret = read(fileno(loadpat_output), 
							      buffer, BUFFLEN);
						   if(debug) {
						       write(1, buffer, BUFFLEN);
						       cout << "ret = " << ret << endl;
						   }
					       } while( ret > 0 &&
						   strstr(buffer,"Success\nloadpat>")==NULL );
					       if( ret <= 0 ) {
						   yyerror(" problem with loadpat.Exiting\n");
						   senderrmsg();
						   YYABORT;
					       }
						   
					       if(debug)      
						   cout << "loadpat " << buffer << endl;
				       }    
	                               }
          | P_MS1 '=' P_NUM            {
                                           if(debug)      
					       cout << "MS1 = " << $3 << endl;
					   first_part |= MS1_SIG($3);
	                               }
          | P_MS2 '=' P_NUM            {
                                           if(debug)      
					       cout << "MS2 = " << $3 << endl;
					   first_part |= MS2_SIG($3);
	                               }
          | P_MS3 '=' P_NUM            {
                                           if(debug)      
					       cout << "MS3 = " << $3 << endl;
					   first_part |= MS3_0_SIG($3);
					   sec_part   |= MS3_1_SIG($3);
	                               }
          | P_MS4 '=' P_NUM            {
                                           if(debug)      
					       cout << "MS4 = " << $3 << endl;
					   sec_part   |= MS4_SIG($3);
	                               }
          | P_CODE_IN '=' P_NUM        {
                                           if(debug)      
					       cout << "CODE_IN = " << $3 << endl;
	                               }
          | P_CODE_OUT '=' P_NUM       {
                                           if(debug)      
					       cout << "CODE_OUT = " << $3 << endl;
					   first_part |= CODE_OUT_SIG($3);
					   sigseq->Insert( revert_bitsul(first_part) );
					   sigseq->Insert( revert_bitsul(sec_part)   );
	                               }
          | error                      {
	                                   char buf[256];
					   sprintf( buf, " : unknown command, line %d\n",
						    @1.last_line );
                                           yyerror(buf);
					   senderrmsg();
					   YYABORT;
	                               }
;



%%
#include <stdio.h>
#include <iostream.h>
#include <ctype.h>

extern FILE* yyin;
FILE *err;

char errorbuf[256];
int  errorbufpnt;


int testpacparse(  FILE* file, FILE* errptr, FILE* loadpat[2], 
		   char* pat,  int count,    unsigned uwait )
{
    yyin = file;
    err = errptr;
    errorbufpnt = 0;
    loadpat_input = loadpat[0];
    loadpat_output = loadpat[1];
    current_pat = pat;
    test_no = -1;
    mag = new TMag;
    mem = new TMemory;
    sigseq = new TSignalSequence;

    if( YES!=mag->Command( TMag::SOFT_RESET )) {
	fprintf( errptr, "nie moglem zresetowac soft\n" );
	return 0;
    }

    yyrestart(yyin);
    yyparse();

    TSignalSequenceHndl sigseqhndl = mem->Insert( *sigseq );
    mag->SendSequence( sigseqhndl, 1 );

    mag->Halt();
    if( YES!=mag->CopyIRegsToFDPM() ) {
	cerr<<"nie moglem CopyIRegsToFDPM\n";
	exit(1);
    }
    if( YES!=mem->CopyDataToFDPM() ) {
	cerr<<"nie moglem CopyDataToFDPM\n";
	exit(1);
    }

    VME.Write32( 0x3c000, 0);
    while( count-- > 0 ) {
	mag->Command( TMag::START | TMag::OUTPUT
		      | TMag::BLOCK0 | TMag::BLOCK1 | TMag::BLOCK2 | TMag::BLOCK3 );
	usleep(uwait);
    }

    delete mag;
    delete mem;
    delete sigseq              ;
} 


int yyerror (char*s)  /* Called by yyparse on error */
{
    strcpy( errorbuf + errorbufpnt, s );
    errorbufpnt += strlen(s);
}

int senderrmsg()
{
    fputs( errorbuf, err );
    fflush( err );
    errorbufpnt = 0;
}


