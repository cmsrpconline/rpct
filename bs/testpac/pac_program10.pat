-- Data Register1
--
-- Input_masks (selected bits are set to "1"): 
DR1.MMS1[0..17] = #3FFFF;	-- "1" pozwala przejsc wejsciom,"0" blokuje
DR1.MMS2[0..3] = #F;		-- "0" ustawia okreslony bit aktywny (zastepuje sygnal z wejscia)
DR1.MMS3[0..13] = #3FFF;	-- "1" pozwala przejsc wejsciom,"0" blokuje
DR1.MMS4[0..17] = #3FFFF;	-- "1" pozwala przejsc wejsciom,"0" blokuje
-- Hpt bit (bit(4)):
DR1.FF3= #1;
-- Negative group bit (sign bit of negatives groups (bit(5)) set to "1"):
DR1.FF2=#1;		-- ustawia znak grupy pozytywnej !!!!!
-- Positive group bit (sign bit of positives groups (bit(5)) set to "1"):
DR1.FF1=#1;		-- ustawia znak grupy negatywnej !!!!!
-- Output register bit:
DR1.F6=#0;			-- #0 bez latchowania
-- Output_masks (selected bits are set to "0"): 
DR1.M[6..0] = #00;		-- zmieniony porzadek bitow!!!
-- Cascade delays:		-- #F minimalne opoznienie		
DR1.F5=#1;
DR1.F4=#1;
DR1.F3=#1;
DR1.F2=#1;
--
-- OR group 16 (dmux):
DR2.DO16_01[0..3]=#a;
-- Patterns (mux's):
DR2.P001_MX1[0..3]=#5; -- mux 1 do 15
DR2.P001_MX3[0..3]=#5; -- mux 1 do 11
DR2.P001_MX4[0..3]=#5; -- mux 1 do 15
-- 
