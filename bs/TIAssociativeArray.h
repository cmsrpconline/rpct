#if !defined TIASSOCARR_H
#define TIASSOCARR_H

#include <string.h>


enum TIAssociativeArrayError { 
    NotEnoughMemory, 
    NonExistentKey,
    IndexOutOfBounds,
    ArrayAlreadyFull,
    DuplicateKey
};

template<class T>  
class TIAssociation{
    
    friend class TIAssociativeArray<T>;
    char*	Key;
    T*		Value;
    TIAssociation<T>*	Left;
    TIAssociation<T>*	Right;
public:
    TIAssociation() {
	Key=NULL;
	Value=NULL;
	Left=Right=NULL;
    }
    TIAssociation( const char* k ,  T* val ,TIAssociation<T>* l = NULL,
		   TIAssociation<T>* r = NULL  ) throw(TIAssociativeArrayError) {
		       Key = new char[ strlen(k) + 1 ];
		       if( Key==NULL ) 
			   throw NotEnoughMemory;
		       strcpy( Key, k );
		       Value = val;
		       Left = l;
		       Right = r;
    }
    TIAssociation<T>& operator= (const TIAssociation<T>& ass ) {
	    if( Key != NULL )  delete [] Key;
	    Key = new char[ strlen(ass.Key) + 1 ];
	    if( Key==NULL ) 
		throw NotEnoughMemory;
	    strcpy( Key,ass.Key);
	    Value=ass.Value;
	    Left=ass.Left;
	    Right=ass.Right;
	    return *this;
    }
    ~TIAssociation() { 
	delete [] Key; 
    }
};


template<class T>
class TIAssociativeArrayIterator;

template<class T>
class TIAssociativeArrayBackwardIterator;


template<class T>
class TIAssociativeArray { 
    // wskaznik do tablicy w ktorym przechowuje TIAssocjacje
    TIAssociation<T>* Data;
    
    // wskaznik do asocjacji w tablicy ktora jest korzeniem drzewa
    TIAssociation<T>* Root; 
    
    // ilosc aktualnie przechowywanow elementow w tablicy
    int ItemsNumber;  
    
    // maksymalna ilosc obiektow jaka tablica moze przechowac ( rozmiar tablicy Data[] )
    const int MaxItemsNumber; 
    
    // pomocnicza tablica potrzebna przy tworzeniu drzewa; 
    // jest niszczona, gdy drzewo jest juz zbudowane ( ItemsNumber==MaxItemsNumber )
    int* Weights; 
    
    // wklada do tablicy Data nowy element ( key, val ) i wywaza 
    // drzewo. Rekurencyjna.
    // Zaczerpnieta z ksiazki:'N.Wirth Algorytmy+StrukturyDanych=Programy'
    void Insert( const char* key, T* val, TIAssociation<T>*& p, int *h ); 
    
    // wyszukuje w tablicy Data[] elementu o kluczu key i zwraca
    //  do niego wskaznik( lub NULL gdy go nie ma ). Rekurencyjna. 
    // Poszukiwanie rozpoczyna od elementu node
    TIAssociation<T>* Find( const char * key, TIAssociation<T>*& node ) const; 
public:
    // stworz tablice n-elementowa
    TIAssociativeArray( int nelem ); 
    
    // wstaw do tablicy element o kluczu key i wartosci val
    void Add( const char* key, T* val );  

    ~TIAssociativeArray() { delete [] Data; }
    
    // pozwala na odwolywanie sie do elementu tablicy w sposob tab[1] 
    // - zwraca referencje do pierwszego elementu
    T& operator [] ( int loc ) const  {
	if( loc<0 || loc>ItemsNumber )
	    throw IndexOutOfBounds;
	return *( (Data+loc)->Value );
    }
    
    // pozwala na odwolywanie sie do elementu tablicy w sposob tab["klucz"] 
    // - zwraca referencje do elementu o kluczu "klucz";
    //  wyrzucz blad, gdy takiego elementu nie ma
    T& operator [] ( const char* key ) const {
	TIAssociation<T>* x=Root;
	if( ( x=  Find( key, x ) ) == NULL )
	    throw NonExistentKey;
	return  *(x->Value);
    }
    
     friend class TIAssociativeArrayIterator<T>;
     friend class TIAssociativeArrayBackwardIterator<T>;

};



template<class T>
TIAssociativeArray<T>::TIAssociativeArray( int nelem ) : MaxItemsNumber( nelem )
{
    Weights = new int[nelem];
    if( Weights==NULL ) 
	throw NotEnoughMemory;
     
    Data = new TIAssociation<T>[ nelem ];
    if( Data==NULL ) {
	delete [] Weights;
     	throw NotEnoughMemory;
    }
    ItemsNumber = 0;
    Root=NULL;
}


template<class T>
void TIAssociativeArray<T>::Add( const char* key, T* val )
{
    int h;
    if( ItemsNumber < MaxItemsNumber ) {
	Insert( key, val, Root, &h );
	if( ItemsNumber == MaxItemsNumber )
	    delete [] Weights;
    }
    else 
	throw ArrayAlreadyFull;
}



template<class T>
void TIAssociativeArray<T>::Insert( const char* key, T* val, TIAssociation<T>*& p, int *h )
{
    TIAssociation<T> *p1, *p2;
    
    if( p==NULL ) {
	*h=1;
	p=Data+ItemsNumber;
	Weights[ItemsNumber]=0;
	Data[ItemsNumber++] =  TIAssociation<T> ( key, val );
    }
    else if( strcmp( key , p->Key) < 0 ) {
	Insert( key,val,p->Left,h );
	if( *h ) {
	    switch( Weights[p-Data] ) {
	    case 1: Weights[p-Data]=0; *h=0; break;
	    case 0: Weights[p-Data]=-1; break;
	    case -1: p1=p->Left;
		if( Weights[p1-Data]==-1 ) {
		    p->Left=p1->Right;
		    p1->Right=p;
		    Weights[p-Data]=0;
		    p=p1;
		}
		else {
		    p2=p1->Right;
		    p1->Right=p2->Left;
		    p2->Left=p1;
		    p->Left=p2->Right;
		    p2->Right=p;
		    Weights[p-Data]= (( Weights[p2-Data] ==-1 ) ? 1 : 0);
		    Weights[p1-Data]= (( Weights[p2-Data]==1 ) ? -1 : 0);
		    p=p2;
		}
		Weights[p-Data]=0; *h=0;
		break;
	    }
	}
    }
    else if( strcmp(key , p->Key) > 0 ) {
	Insert( key,val,p->Right,h );
	if( *h ) {
	    switch( Weights[p-Data] ) {
	    case -1: Weights[p-Data]=0; *h=0; break;
	    case  0: Weights[p-Data]=+1; break;
	    case  1: p1=p->Right;
		if( Weights[p1-Data]==1 ) {
		    p->Right=p1->Left;
		    p1->Left=p;
		    Weights[p-Data]=0; p=p1;
		}
		else {
		    p2=p1->Left;
		    p1->Left=p2->Right;
		    p2->Right=p1;
		    p->Right=p2->Left;
		    p2->Left=p;
		    Weights[p-Data]= (Weights[p2-Data]==1) ? -1 : 0;
		    Weights[p1-Data]=(Weights[p2-Data]==-1) ? 1 : 0;
		    p=p2;
		}
		Weights[p-Data]=0; *h=0;
		break;
	    }
	}
    }
    else 
	throw DuplicateKey;
}


template<class T>
TIAssociation<T>* TIAssociativeArray<T>::Find( const char* key, TIAssociation<T>*& p ) const
{
    TIAssociation<T>* ret;
    
    if( p==NULL )
	return NULL;
    else if( strcmp( key, p->Key ) < 0 )
	ret = Find( key, p->Left );
    else if( strcmp( key, p->Key ) > 0 )
	ret = Find( key, p->Right );
    else
	ret = p;
    return ret;
}



template<class T>
class TIAssociativeArrayIterator {
    TIAssociativeArray<T> *Array;
    int Current;
public:
    TIAssociativeArrayIterator(TIAssociativeArray<T> *arr) 
	: Array(arr), Current(-1){}
    int More() {
	return (Current+1)<(Array->ItemsNumber);
    }
    T* Next() {
	return  &(*Array)[++Current];
    }
    T* Last() {
	return  &(*Array)[Current];
    }
};

    
template<class T>
class TIAssociativeArrayBackwardIterator {
    TIAssociativeArray<T> *Array;
    int Current;
public:
    TIAssociativeArrayBackwardIterator(TIAssociativeArray<T> *arr) 
	: Array(arr), Current(arr->ItemsNumber){}
    int More() {
	return Current!=0;
    }
    T* Next() {
	return  &(*Array)[--Current];
    }
    T* Last() {
	return  &(*Array)[Current];
    }
};

    
    


#endif
