#include "TCharSequence.h"

using namespace bs;

bool operator==( TPattern& pat,  TScanVector& seq )
{
    int len;
    if( (len=pat.length()) != seq.length() )
        return false;
    int i;
    for( i=0; i<len; i++ )
        if( pat[i] != 'X'   &&   pat[i] != seq[i] )
            return false;
    return true;
}

TScanVector
TPattern::ToScanVector()
{
    TScanVector seq;
    int i;

    for( i=0; i<length(); i++ )
	seq += ( (at(i)=='X') ? '0' : at(i) );
    
    return seq;
}
