#ifndef TSCANVECTOR_HH
#define TSCANVECTOR_HH

#include <typeinfo>
#include <string>

namespace bs {

class TScanVector;
class TPattern;
bool operator==( TPattern& pat,  TScanVector& seq );

class TPattern : public std::string {
public:
    TPattern() {}

    TPattern( const char* str ) : std::string(str) {}

    TPattern( char c ) : std::string( 1, c ) {}
    TPattern( int len, char c ) : std::string( len, c ) {}

    TPattern( TPattern& pat, std::string::size_type pos, std::string::size_type n )
	: std::string( pat, pos, n ) {}
   
    virtual ~TPattern() {}

  //  virtual bool
  //  Compare( TScanVector& seq )
  //      { return (*this)==seq; }

    TScanVector
    ToScanVector();
};

class TScanVector : public TPattern {
public:
    TScanVector() {}
    TScanVector( char* str ) : TPattern(str) {}
    TScanVector( int len, char c ) : TPattern( len, c ) {}
    TScanVector( char c ) : TPattern( c ) {}
    TScanVector( TScanVector& pat, std::string::size_type pos, std::string::size_type n )
	: TPattern( pat, pos, n ) {}
    virtual ~TScanVector() {}
};


inline std::ostream& operator <<(std::ostream& os, TScanVector& seq)
{
    os << seq;
    return os;
}

inline std::ostream& operator <<(std::ostream& os, TPattern& pat)
{
    os << pat;
    return os;
}

inline bool operator <(TPattern& p1, TPattern& p2) 
{
    return (std::string)p1 < (std::string)p2;
}



inline bool operator==( TScanVector& seq, TPattern& pat )
{
    return pat==seq;
}

} // namespace bs

#endif
