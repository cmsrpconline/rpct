%{
#define YYDEBUG 1
#define YYERROR_VERBOSE
#include <stdio.h>
#include <string>


int  yylex();
void yyrestart(FILE*);
extern "C" {
    int yyerror(const char *);
    void senderrmsg();
 	   }

extern int debug;

#include "tb_bs.h"                     
using namespace bs;

const char ADDRESS_SEP = '#';
const char* PIN_PREFIX = "pin";
const char* SIGNAL_PREFIX = "sig";


TBSContr *bscontr;
TBoard   *board;

%}

%union {
    unsigned long  val;  /* For returning numbers.                   */
    char  id[256];   /* For returning identifier names    */
    bs::TScanVector* seq;  /* For sequences */
}

%token P_SIR
%token P_SDR

%token P_RESET
%token P_IDLE
%token P_SHIFTDR
%token P_SHIFTIR

%token P_SS   /* set state */
%token P_SSS  /* set state seg */
%token P_GS   /* get state */
%token P_GSS  /* get state seg */

%token P_SEP 
%token P_PIN
%token P_SIGNAL

%token P_DEBUGON
%token P_DEBUGOFF
%token P_QUIT


%token <val> P_NUM
%token <id>  P_ID
%token <seq> P_SEQ


%%     /* Grammar */

input:
       | input line
;


line:       '\n'                    { cout << "interbs>" << flush; }
          | command '\n'            { cout << "interbs>" << flush; }
;



command:    P_SIR    {
                       if(debug)
					       printf("sir\n");
					   bs::TScanVector sendseq = board->GenerateIRSequence();


					   bs::TScanVector recseq(sendseq.size(),' ');

					   bscontr->ProgramStart();
					   bscontr->IRTransmitRec( sendseq, recseq );
					   if(debug) {
					       cout << "sending sequence:"<< sendseq<<endl;
					       //cout <<hex<<"bscontr:\n"<<(*(TBSContrWZ*)bscontr);
					   }
					   bscontr->ProgramEnd();

					   bscontr->ProgramExecute();

					   board->InterpreteIRSequence( recseq );
					   cout << "received="<<recseq<<endl;
                                       }
          | P_SIR P_SEQ                {
                                           if(debug)
					       cout << "sir " << (*$2) <<
						    " :"<< typeid(*$2).name() << endl;

					   bs::TScanVector recseq($2->size(),' ');

					   bscontr->ProgramStart();
					   bscontr->IRTransmitRec( *$2, recseq );
					   //if(debug)
					   //    cout <<hex<<"bscontr:\n"<<(*(TBSContrWZ*)bscontr);
					   
					   bscontr->ProgramEnd();

					   bscontr->ProgramExecute();
					   cout << "received="<<recseq<<endl;
					   delete $2;
                                       }
          | P_SDR                      {
                                           if(debug)
					       printf("sdr\n");
					   bs::TScanVector sendseq = board->GenerateDRSequence();

					   bs::TScanVector recseq(sendseq.size(),' ');

					   bscontr->ProgramStart();
					   bscontr->DRTransmitRec( sendseq, recseq );
					   if(debug) {
					       cout <<"sending sequence:"<<sendseq << endl;
					       //cout <<hex<<"bscontr:\n"
						   // <<(*(TBSContrWZ*)bscontr);
					   }
					   bscontr->ProgramEnd();

					   bscontr->ProgramExecute();
					   
					   board->InterpreteDRSequence( recseq );

					   cout << "received="<<recseq<<endl;
                                       }
          | P_SDR P_SEQ                {
                                           if(debug)
					       cout << "sdr " << (*$2) <<
						    " :"<< typeid(*$2).name() << endl;

					   bs::TScanVector recseq($2->size(),' ');

					   bscontr->ProgramStart();
					   bscontr->DRTransmitRec( *$2, recseq );
					   //if(debug)
					   //    cout <<hex<<"bscontr:\n"<<(*(TBSContrWZ*)bscontr);
					   bscontr->ProgramEnd();

					   bscontr->ProgramExecute();
					   cout << "received="<<recseq<<endl;
					   delete $2;
                                       }
          | P_SS  P_ID P_ID   {  
                       if(debug)
					       printf("ss %s %s\n",$2, $3);

					   try {
                         dynamic_cast<TChip&>((*board)[$2]).SetState($3);
					   }
					   catch( TException& e ) {
					       yyerror( "Error:  " );
					       yyerror( e.what() );
					       yyerror( "\n" );
					       senderrmsg();
					   };
	                               }


          | P_SS  P_ID P_SEP P_ID P_ID  { // chip#reg

                       if(debug)
					       printf("ss %s # %s %s\n", $2, $4, $5);

					   try {
                           TChip& chip = dynamic_cast<TChip&>((*board)[$2]);
                           chip[$4].SetState($5);
					   }
					   catch( TException& e ) {
					       yyerror( "Error:  " );
					       yyerror( e.what() );
					       yyerror( "\n" );
					       senderrmsg();
					   }
                           }
          | P_SS  P_ID P_SEP P_ID P_SEP P_ID P_ID  { // chip#reg#frag 
                       if(debug)
					       printf("ss %s # %s # %s %s\n", $2, $4, $6, $7);

					   try {      
                           TChip& chip = dynamic_cast<TChip&>((*board)[$2]);
                           // this must be a fragmented register
                           dynamic_cast<TFragmentedRegister&>(chip[$4])[$6].SetState($7);

					   }
					   catch( TException& e ) {
					       yyerror( "Error:  " );
					       yyerror( e.what() );
					       yyerror( "\n" );
					       senderrmsg();
					   }   }
          | P_SS  P_ID P_SEP P_ID P_SEP P_PIN P_SEP P_ID P_ID  { // chip#reg#pin#id
                       if(debug)
					       printf("ss %s # %s #pin# %s %s\n", $2, $4, $8, $9);

					   try {
                           TChip& chip = dynamic_cast<TChip&>((*board)[$2]);
                           TBoundaryRegister& boundary = dynamic_cast<TBoundaryRegister&>(chip[$4]);
                           boundary.Pin($8).SetState($9);
					   }
					   catch( TException& e ) {
					       yyerror( "Error:  " );
					       yyerror( e.what() );
					       yyerror( "\n" );
					       senderrmsg();
					   }   }
          | P_SS  P_ID P_SEP P_ID P_SEP P_SIGNAL P_SEP P_ID P_ID  { // chip#reg#sig#id 
                       if(debug)
					       printf("ss %s # %s #sig# %s %s\n", $2, $4, $8, $9);

					   try {
                           TChip& chip = dynamic_cast<TChip&>((*board)[$2]);
                           TBoundaryRegister& boundary = dynamic_cast<TBoundaryRegister&>(chip[$4]);
                           boundary.Signal($8).SetState($9);
					   }
					   catch( TException& e ) {
					       yyerror( "Error:  " );
					       yyerror( e.what() );
					       yyerror( "\n" );
					       senderrmsg();
					   }  }

          | P_SSS P_ID P_SEQ           {
	                   if(debug)
					       printf("sss %s %s\n",$2, $3->c_str());
					   try {
                         dynamic_cast<TChip&>((*board)[$2]).SetStateSeq(*$3);
					   }
					   catch( TException& e ) {
					       yyerror( "Error:  " );
					       yyerror( e.what() );
					       yyerror( "\n" );
					       senderrmsg();
					   }
					   delete $3;
	                               }  
          | P_SSS  P_ID P_SEP P_ID P_SEQ  { // chip#reg

                       if(debug)
					       printf("sss %s # %s %s\n", $2, $4, $5);

					   try {
                           TChip& chip = dynamic_cast<TChip&>((*board)[$2]);
                           chip[$4].SetStateSeq(*$5);
					   }
					   catch( TException& e ) {
					       yyerror( "Error:  " );
					       yyerror( e.what() );
					       yyerror( "\n" );
					       senderrmsg();
					   }
                           }
          | P_SSS  P_ID P_SEP P_ID P_SEP P_ID P_ID  { // chip#reg#frag 
                       if(debug)
					       printf("sss %s # %s # %s %s\n", $2, $4, $6, $7);

					   try {      
                           TChip& chip = dynamic_cast<TChip&>((*board)[$2]);
                           // this must be a fragmented register
                           dynamic_cast<TFragmentedRegister&>(chip[$4])[$6].SetStateSeq(*$7);

					   }
					   catch( TException& e ) {
					       yyerror( "Error:  " );
					       yyerror( e.what() );
					       yyerror( "\n" );
					       senderrmsg();
					   }   }
          | P_SSS  P_ID P_SEP P_ID P_SEP P_PIN P_SEP P_ID P_ID  { // chip#reg#pin#id
                       if(debug)
					       printf("sss %s # %s #pin# %s %s\n", $2, $4, $8, $9);

					   try {
                           TChip& chip = dynamic_cast<TChip&>((*board)[$2]);
                           TBoundaryRegister& boundary = dynamic_cast<TBoundaryRegister&>(chip[$4]);
                           boundary.Pin($8).SetStateSeq($9);
					   }
					   catch( TException& e ) {
					       yyerror( "Error:  " );
					       yyerror( e.what() );
					       yyerror( "\n" );
					       senderrmsg();
					   }   }
          | P_SSS  P_ID P_SEP P_ID P_SEP P_SIGNAL P_SEP P_ID P_ID  { // chip#reg#sig#id
                       if(debug)
					       printf("sss %s # %s #sig# %s %s\n", $2, $4, $8, $9);

					   try {
                           TChip& chip = dynamic_cast<TChip&>((*board)[$2]);
                           TBoundaryRegister& boundary = dynamic_cast<TBoundaryRegister&>(chip[$4]);
                           boundary.Signal($8).SetStateSeq($9);
					   }
					   catch( TException& e ) {
					       yyerror( "Error:  " );
					       yyerror( e.what() );
					       yyerror( "\n" );
					       senderrmsg();
					   }  }
                       
          | P_GS  P_ID                 {
	                                   if(debug)
					       printf("gs %s\n",$2);
					   try {
					       cout << dynamic_cast<TChip&>((*board)[$2]).GetState()
                                << endl;
					   }
					   catch( TException& e ) {
					       yyerror( "Error:  " );
					       yyerror( e.what() );
					       yyerror( "\n" );
					       senderrmsg();
					   }

	                               }   
          | P_GS  P_ID P_SEP P_ID  { // chip#reg

                       if(debug)
					       printf("gs %s # %s \n", $2, $4);

					   try {
                           TChip& chip = dynamic_cast<TChip&>((*board)[$2]);
                           cout << chip[$4].GetState()
                                << endl;
					   }
					   catch( TException& e ) {
					       yyerror( "Error:  " );
					       yyerror( e.what() );
					       yyerror( "\n" );
					       senderrmsg();
					   }
                           }
          | P_GS  P_ID P_SEP P_ID P_SEP P_ID  { // chip#reg#frag
                       if(debug)
					       printf("gs %s # %s # %s\n", $2, $4, $6);

					   try {      
                           TChip& chip = dynamic_cast<TChip&>((*board)[$2]);
                           // this must be a fragmented register
                           cout << dynamic_cast<TFragmentedRegister&>(chip[$4])[$6].GetState()
                                << endl;

					   }
					   catch( TException& e ) {
					       yyerror( "Error:  " );
					       yyerror( e.what() );
					       yyerror( "\n" );
					       senderrmsg();
					   }   }
          | P_GS  P_ID P_SEP P_ID P_SEP P_PIN P_SEP P_ID  { // chip#reg#pin#id
                       if(debug)
					       printf("gs %s # %s #pin# %s\n", $2, $4, $8);

					   try {
                           TChip& chip = dynamic_cast<TChip&>((*board)[$2]);
                           TBoundaryRegister& boundary = dynamic_cast<TBoundaryRegister&>(chip[$4]);
                           cout << boundary.Pin($8).GetStateSeq()
                                << endl;
					   }
					   catch( TException& e ) {
					       yyerror( "Error:  " );
					       yyerror( e.what() );
					       yyerror( "\n" );
					       senderrmsg();
					   }   }
          | P_GS  P_ID P_SEP P_ID P_SEP P_SIGNAL P_SEP P_ID  { // chip#reg#sig#id
                       if(debug)
					       printf("gs %s # %s #sig# %s\n", $2, $4, $8);

					   try {
                           TChip& chip = dynamic_cast<TChip&>((*board)[$2]);
                           TBoundaryRegister& boundary = dynamic_cast<TBoundaryRegister&>(chip[$4]);
                           cout << boundary.Signal($8).GetState()
                                << endl;
					   }
					   catch( TException& e ) {
					       yyerror( "Error:  " );
					       yyerror( e.what() );
					       yyerror( "\n" );
					       senderrmsg();
					   }  }
          | P_GSS P_ID                 {
                       if(debug)
					       printf("gss %s\n",$2);
					   try {
					       cout << dynamic_cast<TChip&>((*board)[$2]).GetStateSeq()
                                << endl;
					   }
					   catch( TException& e ) {
					       yyerror( "Error:  " );
					       yyerror( e.what() );
					       yyerror( "\n" );
					       senderrmsg();
					   }
	                               } 
          | P_GSS  P_ID P_SEP P_ID  { // chip#reg

                       if(debug)
					       printf("gss %s # %s \n", $2, $4);

					   try {
                           TChip& chip = dynamic_cast<TChip&>((*board)[$2]);
                           cout << chip[$4].GetStateSeq()
                                << endl;
					   }
					   catch( TException& e ) {
					       yyerror( "Error:  " );
					       yyerror( e.what() );
					       yyerror( "\n" );
					       senderrmsg();
					   }
                           }
          | P_GSS  P_ID P_SEP P_ID P_SEP P_ID  { // chip#reg#frag
                       if(debug)
					       printf("gss %s # %s # %s\n", $2, $4, $6);

					   try {      
                           TChip& chip = dynamic_cast<TChip&>((*board)[$2]);
                           // this must be a fragmented register
                           cout << dynamic_cast<TFragmentedRegister&>(chip[$4])[$6].GetStateSeq()
                                << endl;

					   }
					   catch( TException& e ) {
					       yyerror( "Error:  " );
					       yyerror( e.what() );
					       yyerror( "\n" );
					       senderrmsg();
					   }   }
          | P_GSS  P_ID P_SEP P_ID P_SEP P_PIN P_SEP P_ID  { // chip#reg#pin#id
                       if(debug)
					       printf("gss %s # %s #pin# %s\n", $2, $4, $8);

					   try {
                           TChip& chip = dynamic_cast<TChip&>((*board)[$2]);
                           TBoundaryRegister& boundary = dynamic_cast<TBoundaryRegister&>(chip[$4]);
                           cout << boundary.Pin($8).GetStateSeq()
                                << endl;
					   }
					   catch( TException& e ) {
					       yyerror( "Error:  " );
					       yyerror( e.what() );
					       yyerror( "\n" );
					       senderrmsg();
					   }   }
          | P_GSS  P_ID P_SEP P_ID P_SEP P_SIGNAL P_SEP P_ID  { // chip#reg#sig#id
                       if(debug)
					       printf("gss %s # %s #sig# %s\n", $2, $4, $8);

					   try {
                           TChip& chip = dynamic_cast<TChip&>((*board)[$2]);
                           TBoundaryRegister& boundary = dynamic_cast<TBoundaryRegister&>(chip[$4]);
                           cout << boundary.Signal($8).GetStateSeq()
                                << endl;
					   }
					   catch( TException& e ) {
					       yyerror( "Error:  " );
					       yyerror( e.what() );
					       yyerror( "\n" );
					       senderrmsg();
					   }  }
          | P_RESET                    {
                                           if(debug)
					       printf("reset\n");
					   bscontr->ProgramStart();
					   bscontr->TAPtoReset();
					   //if(debug)
					   //    cout <<hex<<"bscontr:\n"<<(*(TBSContrWZ*)bscontr);
					   bscontr->ProgramEnd();

					   bscontr->ProgramExecute();
                                       }
          | P_IDLE                     {
                                           if(debug)
					       printf("idle\n");
					   bscontr->ProgramStart();
					   bscontr->TAPtoIdle();
					   //if(debug)
					   //    cout <<hex<<"bscontr:\n"<<(*(TBSContrWZ*)bscontr);
					   bscontr->ProgramEnd();

					   bscontr->ProgramExecute();
                                       }
          | P_SHIFTIR                  {
                                           if(debug)
					       printf("shiftir\n");
					   bscontr->ProgramStart();
					   bscontr->SetTAPState( TBSContr::SHIFT_IR );
					   //if(debug)
					   //    cout <<hex<<"bscontr:\n"<<(*(TBSContrWZ*)bscontr);
					   bscontr->ProgramEnd();

					   bscontr->ProgramExecute();
                                       }
          | P_SHIFTDR                  {
                                           if(debug)
					       printf("shiftdr\n");
					   bscontr->ProgramStart();
					   bscontr->SetTAPState( TBSContr::SHIFT_DR );
					   //if(debug)
					   //    cout <<hex<<"bscontr:\n"<<(*(TBSContrWZ*)bscontr);
					   bscontr->ProgramEnd();

					   bscontr->ProgramExecute();
                                       }
          | P_DEBUGON                  { 
                                           debug = 1;
	                               }
          | P_DEBUGOFF                 { 
                                           debug = 0;
	                               }
          | P_QUIT                     {
	                                   delete bscontr;
                                           YYABORT;
	                               } 
          | P_ID                   {
                                           yyerror("Unknown command: ");
                                           yyerror($1);                 
                                           yyerror("\n");
					                       senderrmsg();
	                               }
          | error                      {
                                           yyerror(" : unknown command\n");
					   senderrmsg();
	                               }
;



%%
#include <stdio.h>
#include <iostream.h>
#include <ctype.h>

extern FILE* yyin;
FILE *err;

char errorbuf[256];
int  errorbufpnt;


int interbsparse(  FILE* file, FILE* errptr, TBoard* brd, TBSContr* contr )
{
    yyin = file;
    err = errptr;
    errorbufpnt = 0;
    //bscontr =  new TBSContrWZ(0xa);
    bscontr = contr;
    board = brd;

    yyrestart(yyin);    
    cout << "interbs>" << flush;
    return yyparse();
} 


int yyerror (const char*s)  /* Called by yyparse on error */
{
    strcpy( errorbuf + errorbufpnt, s );
    errorbufpnt += strlen(s);
    return 0;
}

void senderrmsg()
{
    fputs( errorbuf, err );
    fflush( err );
    errorbufpnt = 0;
}



