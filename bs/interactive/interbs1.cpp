#include <iostream>
#include <stdio.h>
#include "tb_std.h"  
#include "tb_bs.h"

using namespace bs;

int debug;


int interbsparse( FILE* file, FILE* errptr, TBoard* board, TBSContr* contr);

struct TBSCEl {
    char ChipType[32];
    char ChipName[32];
};

int main( int argc, char ** argv )
{
    int tmp;
    debug = 1;
    if( argc > 2 ) {
      cout << argv[0] << ": too many arguments in cmd line\n";
	    exit(1);
    }


    TBSCEl chips[10];
    int chip_no;

    if (argc==2) {
      FILE* bscfile;
      if( NULL == (bscfile = fopen( argv[1], "r" )) ) {
        cout <<  argv[0] << ": couldn't open " << argv[1] << endl;
        exit(1);
      }
      tmp=1;
      for( chip_no=0; (tmp!=0 && tmp!=EOF); chip_no++ )
        tmp = fscanf( bscfile, "%s %s\n",
            chips[chip_no].ChipType,
            chips[chip_no].ChipName );
      chip_no--;
    };


    try {
	    TBoard board("board",chip_no);

	    for( int i=0; i<chip_no; i++ ) {
	      cout << chips[i].ChipType << ":" << chips[i].ChipName << endl;
	      if( !strcmp(chips[i].ChipType, "PAC" ) )
	      	board.AddElement( *new TPac(chips[i].ChipName) );
	      else
	      	if( !strcmp(chips[i].ChipType, "PAC2" ) )
		        board.AddElement( *new TPac2(chips[i].ChipName) );
                else
                  if( !strcmp(chips[i].ChipType, "ALTERA" ) )
                          board.AddElement( *new TAltera(chips[i].ChipName) );
                  else {
                          cout << "not supported chip type : "<< chips[i].ChipType << endl;
                          exit(1);
                  }
	    }
      cout << "interbs>";
      TVMEWinBit3 vme;
      TBSContrWZ bscontr( &vme, 0xa );
	    interbsparse( stdin, stderr, &board, &bscontr );
    }
    catch( TException& e ) {
	    cerr << e.what() << endl;
    }
    catch( ... ) {
	    cerr << "Exception\n";
    }

    return 0;
}
