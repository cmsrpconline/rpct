typedef union {
    unsigned long  val;  /* For returning numbers.                   */
    char  id[256];   /* For returning identifier names    */
    bs::TScanVector* seq;  /* For sequences */
} YYSTYPE;
#define	P_SIR	258
#define	P_SDR	259
#define	P_RESET	260
#define	P_IDLE	261
#define	P_SHIFTDR	262
#define	P_SHIFTIR	263
#define	P_SS	264
#define	P_SSS	265
#define	P_GS	266
#define	P_GSS	267
#define	P_SEP	268
#define	P_PIN	269
#define	P_SIGNAL	270
#define	P_DEBUGON	271
#define	P_DEBUGOFF	272
#define	P_QUIT	273
#define	P_NUM	274
#define	P_ID	275
#define	P_SEQ	276


extern YYSTYPE yylval;
