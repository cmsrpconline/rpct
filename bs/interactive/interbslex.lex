%{
#include "..\TScanVector.h"
#include "interbsparse.tab.h"

using namespace bs;
%}
DIGIT    [0-9]
/* HEXDIGIT [0-9a-fA-F] */
/* BINDIGIT [0-1] */
SEQITEM  [0-1]
ID       [a-zA-Z][<>#_.a-zA-Z0-9]{0,31}

%%

{DIGIT}+      {   
                  sscanf(yytext,"%ld",&yylval.val);
		  return P_NUM;
	      }
\"{SEQITEM}+\"   {
                   yytext[yyleng-1] = 0;
                   yylval.seq = new bs::TScanVector( yytext+1 );
		   return P_SEQ;
                 }


sir           return P_SIR;
sdr           return P_SDR;

reset         return P_RESET;
idle          return P_IDLE;
shiftdr       return P_SHIFTDR;
shiftir       return P_SHIFTIR;

ss            return P_SS;
sss           return P_SSS;
gs            return P_GS;
gss           return P_GSS;

"|"           return P_SEP;
pin           return P_PIN;
sig           return P_SIGNAL;              

debugon       return P_DEBUGON;
debugoff      return P_DEBUGOFF;
quit          return P_QUIT;




{ID}          {
                strcpy(yylval.id, yytext);
                return P_ID;
              }

"%"[^\n]*\n    /* eat up one-line comments */

[ \t]+

"\n"         { 
                 return '\n'; 
             }

<<EOF>>      { YY_FLUSH_BUFFER; yyterminate(); }

%%




