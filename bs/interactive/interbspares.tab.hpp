/* A Bison parser, made from interbsparse.y, by GNU bison 1.75.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

#ifndef BISON_INTERBSPARES_TAB_HPP
# define BISON_INTERBSPARES_TAB_HPP

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     P_SIR = 258,
     P_SDR = 259,
     P_RESET = 260,
     P_IDLE = 261,
     P_SHIFTDR = 262,
     P_SHIFTIR = 263,
     P_SS = 264,
     P_SSS = 265,
     P_GS = 266,
     P_GSS = 267,
     P_SEP = 268,
     P_PIN = 269,
     P_SIGNAL = 270,
     P_DEBUGON = 271,
     P_DEBUGOFF = 272,
     P_QUIT = 273,
     P_NUM = 274,
     P_ID = 275,
     P_SEQ = 276
   };
#endif
#define P_SIR 258
#define P_SDR 259
#define P_RESET 260
#define P_IDLE 261
#define P_SHIFTDR 262
#define P_SHIFTIR 263
#define P_SS 264
#define P_SSS 265
#define P_GS 266
#define P_GSS 267
#define P_SEP 268
#define P_PIN 269
#define P_SIGNAL 270
#define P_DEBUGON 271
#define P_DEBUGOFF 272
#define P_QUIT 273
#define P_NUM 274
#define P_ID 275
#define P_SEQ 276




#ifndef YYSTYPE
#line 30 "interbsparse.y"
typedef union {
    unsigned long  val;  /* For returning numbers.                   */
    char  id[256];   /* For returning identifier names    */
    bs::TScanVector* seq;  /* For sequences */
} yystype;
/* Line 1281 of /usr/share/bison/yacc.c.  */
#line 88 "interbspares.tab.hpp"
# define YYSTYPE yystype
#endif

extern YYSTYPE yylval;


#endif /* not BISON_INTERBSPARES_TAB_HPP */

