#include <iostream>
#include <stdio.h>
#include "tb_std.h"  
#include "tb_bs.h"

#include <memory>

using namespace bs;

int debug;


int interbsparse( FILE* file, FILE* errptr, TBoard* board, TBSContr* contr);

int main( int argc, char ** argv )
{
    debug = 0;
    TBoard* board = NULL;
    try {
      board = CreateTbBoard( (argc>=2) ? argv[1] : NULL );

      /*auto_ptr<TVMEInterface> vme;

      try {
        vme.reset(new TVMEWinBit3(0, BT_DEV_A24, 128));
      }
      catch(EVMEOpen& e) {
        cout << e.what() << "\nWill continue with VME emulator." << endl;
        vme.reset(new TVMESimul);
      };


      TBSContrWZ bscontr( vme.get(), 0xa );*/
      TBSContrJAM bscontr;
      interbsparse( stdin, stderr, board, &bscontr );

    }
    catch( TException& e ) {
	    cerr << e.what() << endl;
    }
    catch( std::exception& e ) {
	    cerr << "C++ Exception\n" << e.what() << endl;
    }   
    catch(...) {
	    cerr << "Exception\n" << endl;
    }

    if (board!=NULL)
      delete board;
      
    return 0;
}
