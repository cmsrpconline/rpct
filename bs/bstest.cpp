//---------------------------------------------------------------------------

#include "tb_bs.h"
//---------------------------------------------------------------------------



using namespace std;   
using namespace bs;


int main(int argc, char* argv[])
{
  try {
    TBoard board("board");   
    TChipFactory factory("D:\\Michal\\tb\\bsdl\\chipinfo.txt", &cout);
    TChip& altera = factory.CreateChip("EPF1K50F256", "altera");
    TChip* gol = new TGOL1("GOL");

    board.AddElement(*gol);
    gol->Describe(cout);
    //delete &altera;
  }
  catch(TException& e) {
    cout << e.what() << endl;
  }
  catch(exception& e)   {
    cout << e.what() << endl;
  }
  return 0;
}
//---------------------------------------------------------------------------
 