#if !defined TASSOCARR_H
#define TASSOCARR_H

#include <string.h>

class EAssociativeArray : public TException {
public:
    EAssociativeArray( String msg )
	: TException( "AssociativeArray: " + msg ) {}
};


class EAssoArrNotEnoughMemory : public EAssociativeArray, ENotEnoughMemory {
public:
    EAssoArrNotEnoughMemory( String msg )
	: EAssociativeArray( "Not enough memory " + msg ) {}
};

class EAssoArrNonExistentKey : public EAssociativeArray {
public:
    EAssoArrNonExistentKey( String key, String msg )
	: EAssociativeArray( "Non existent key '" + key + "' " + msg ) {}
};

class EAssoArrIndexOutOfBounds : public EAssociativeArray {
public:
    EAssoArrIndexOutOfBounds(  String msg )
	: EAssociativeArray( "Index out of bounds " + msg ) {}
};

class EAssoArrAlreadyFull : public EAssociativeArray {
public:
    EAssoArrAlreadyFull( String msg )
	: EAssociativeArray( "Array already full " + msg ) {}
};

class EAssoArrDuplicateKey : public EAssociativeArray {
public:
    EAssoArrDuplicateKey( String key, String msg )
	: EAssoArrDuplicateKey( "Duplicate key '" + key + " '" + msg ) {}
};

// class TAssociativeArray { 
//     NotEnoughMemory, 
//     NonExistentKey,
//     IndexOutOfBounds,
//     ArrayAlreadyFull,
//     DuplicateKey
// };

template<class T>  
class TAssociation{
    
    friend class TAssociativeArray<T>;
    char*	Key;
    T		Value;
    TAssociation<T>*	Left;
    TAssociation<T>*	Right;
public:
    TAssociation() {
	Key=NULL;
	//Value=NULL;
	Left=Right=NULL;
    }
    TAssociation( const char* k ,  T val ,TAssociation<T>* l = NULL,
		  TAssociation<T>* r = NULL  ) throw(EAssoArrNotEnoughMemory) {
		      Key = new char[ strlen(k) + 1 ];
		      if( Key==NULL ) 
			  throw EAssoArrNotEnoughMemory("");
		      strcpy( Key, k );
		      Value = val;
		      Left = l;
		      Right = r;
    }
    TAssociation<T>& operator= (const TAssociation<T>& ass ) throw(EAssoArrNotEnoughMemory) {
	    if( Key != NULL )  delete [] Key;
	    Key = new char[ strlen(ass.Key) + 1 ];
	    if( Key==NULL ) 
		throw EAssoArrNotEnoughMemory("");
	    strcpy( Key,ass.Key);
	    Value=ass.Value;
	    Left=ass.Left;
	    Right=ass.Right;
	    return *this;
    }
    ~TAssociation() { 
	delete [] Key; 
    }
};


template<class T>
class TAssociativeArrayIterator;

template<class T>
class TAssociativeArrayBackwardIterator;


template<class T>
class TAssociativeArray { 
    // wskaznik do tablicy w ktorym przechowuje TAssocjacje
    TAssociation<T>* Data;
    
    // wskaznik do asocjacji w tablicy ktora jest korzeniem drzewa
    TAssociation<T>* Root; 
    
    // ilosc aktualnie przechowywanow elementow w tablicy
    int ItemsNumber;  
    
    // maksymalna ilosc obiektow jaka tablica moze przechowac ( rozmiar tablicy Data[] )
    const int MaxItemsNumber; 
    
    // pomocnicza tablica potrzebna przy tworzeniu drzewa; 
    // jest niszczona, gdy drzewo jest juz zbudowane ( ItemsNumber==MaxItemsNumber )
    int* Weights; 
    
    // wklada do tablicy Data nowy element ( key, val ) i wywaza 
    // drzewo. Rekurencyjna.
    // Zaczerpnieta z ksiazki:'N.Wirth Algorytmy+StrukturyDanych=Programy'
    void Insert( const char* key, T val, TAssociation<T>*& p, int *h ); 
    
    // wyszukuje w tablicy Data[] elementu o kluczu key i zwraca
    //  do niego wskaznik( lub NULL gdy go nie ma ). Rekurencyjna. 
    // Poszukiwanie rozpoczyna od elementu node
    TAssociation<T>* Find( const char * key, TAssociation<T>*& node ) const; 
public:
    // stworz tablice n-elementowa
    TAssociativeArray( int nelem )  throw(EAssoArrNotEnoughMemory) ; 
    
    // wstaw do tablicy element o kluczu key i wartosci val
    void Add( const char* key, T val );  

    ~TAssociativeArray() { delete [] Data; }
    
    // pozwala na odwolywanie sie do elementu tablicy w sposob tab[1] 
    // - zwraca referencje do pierwszego elementu
    T& operator [] ( int loc ) throw(EAssoArrIndexOutOfBounds) {
	if( loc<0 || loc>ItemsNumber )
	    throw EAssoArrIndexOutOfBounds("");
	return ( (Data+loc)->Value );
    }
    
    // pozwala na odwolywanie sie do elementu tablicy w sposob tab["klucz"] 
    // - zwraca referencje do elementu o kluczu "klucz";
    //  wyrzucz blad, gdy takiego elementu nie ma
    T& operator [] ( const char* key ) throw(EAssoArrNonExistentKey) {
	TAssociation<T>* x=Root;
	if( ( x=  Find( key, x ) ) == NULL )
	    throw EAssoArrNonExistentKey(key, "");
	return  (x->Value);
    }
    
    char* GetKey( int loc ) throw(EAssoArrIndexOutOfBounds) {
	if( loc<0 || loc>ItemsNumber )
	    throw EAssoArrIndexOutOfBounds("");
	return ( (Data+loc)->Key );
    }

    int Size() {
	return ItemsNumber;
    }

    friend class TAssociativeArrayIterator<T>;
    friend class TAssociativeArrayBackwardIterator<T>;

};



template<class T>
TAssociativeArray<T>::TAssociativeArray( int nelem )  throw(EAssoArrNotEnoughMemory)
    : MaxItemsNumber( nelem )
{
    Weights = new int[nelem];
    if( Weights==NULL ) 
	throw EAssoArrNotEnoughMemory("");
     
    Data = new TAssociation<T>[ nelem ];
    if( Data==NULL ) {
	delete [] Weights;
     	throw EAssoArrNotEnoughMemory("");
    }
    ItemsNumber = 0;
    Root=NULL;
}


template<class T>
void TAssociativeArray<T>::Add( const char* key, T val ) throw(EAssoArrAlreadyFull)
{
    int h;
    if( ItemsNumber < MaxItemsNumber ) {
	Insert( key, val, Root, &h );
	if( ItemsNumber == MaxItemsNumber )
	    delete [] Weights;
    }
    else 
	throw EAssoArrAlreadyFull("");
}



template<class T>
void TAssociativeArray<T>::Insert( const char* key, T val, TAssociation<T>*& p, int *h ) 
    throw(EAssoArrDuplicateKey)
{
    TAssociation<T> *p1, *p2;
    
    if( p==NULL ) {
	*h=1;
	p=Data+ItemsNumber;
	Weights[ItemsNumber]=0;
	Data[ItemsNumber++] =  TAssociation<T> ( key, val );
    }
    else if( strcmp( key , p->Key) < 0 ) {
	Insert( key,val,p->Left,h );
	if( *h ) {
	    switch( Weights[p-Data] ) {
	    case 1: Weights[p-Data]=0; *h=0; break;
	    case 0: Weights[p-Data]=-1; break;
	    case -1: p1=p->Left;
		if( Weights[p1-Data]==-1 ) {
		    p->Left=p1->Right;
		    p1->Right=p;
		    Weights[p-Data]=0;
		    p=p1;
		}
		else {
		    p2=p1->Right;
		    p1->Right=p2->Left;
		    p2->Left=p1;
		    p->Left=p2->Right;
		    p2->Right=p;
		    Weights[p-Data]= (( Weights[p2-Data] ==-1 ) ? 1 : 0);
		    Weights[p1-Data]= (( Weights[p2-Data]==1 ) ? -1 : 0);
		    p=p2;
		}
		Weights[p-Data]=0; *h=0;
		break;
	    }
	}
    }
    else if( strcmp(key , p->Key) > 0 ) {
	Insert( key,val,p->Right,h );
	if( *h ) {
	    switch( Weights[p-Data] ) {
	    case -1: Weights[p-Data]=0; *h=0; break;
	    case  0: Weights[p-Data]=+1; break;
	    case  1: p1=p->Right;
		if( Weights[p1-Data]==1 ) {
		    p->Right=p1->Left;
		    p1->Left=p;
		    Weights[p-Data]=0; p=p1;
		}
		else {
		    p2=p1->Left;
		    p1->Left=p2->Right;
		    p2->Right=p1;
		    p->Right=p2->Left;
		    p2->Left=p;
		    Weights[p-Data]= (Weights[p2-Data]==1) ? -1 : 0;
		    Weights[p1-Data]=(Weights[p2-Data]==-1) ? 1 : 0;
		    p=p2;
		}
		Weights[p-Data]=0; *h=0;
		break;
	    }
	}
    }
    else 
	throw EAssoArrDuplicateKey(key,"");
}


template<class T>
TAssociation<T>* TAssociativeArray<T>::Find( const char* key, TAssociation<T>*& p ) const
{
    TAssociation<T>* ret;
    
    if( p==NULL )
	return NULL;
    else if( strcmp( key, p->Key ) < 0 )
	ret = Find( key, p->Left );
    else if( strcmp( key, p->Key ) > 0 )
	ret = Find( key, p->Right );
    else
	ret = p;
    return ret;
}



template<class T>
class TAssociativeArrayIterator {
    TAssociativeArray<T> *Array;
    int Current;
public:
    TAssociativeArrayIterator(TAssociativeArray<T> *arr) 
	: Array(arr), Current(-1){}
    int More() {
	return (Current+1)<(Array->ItemsNumber);
    }
    T& Next() {
	return  (*Array)[++Current];
    }
    T& Last() {
	return  (*Array)[Current];
    }
};

    
template<class T>
class TAssociativeArrayBackwardIterator {
    TAssociativeArray<T> *Array;
    int Current;
public:
    TAssociativeArrayBackwardIterator(TAssociativeArray<T> *arr) 
	: Array(arr), Current(arr->ItemsNumber){}
    int More() {
	return Current!=0;
    }
    T& Next() {
	return  (*Array)[--Current];
    }
    T& Last() {
	return  (*Array)[Current];
    }
};

    
    


#endif
