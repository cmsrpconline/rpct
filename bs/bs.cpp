#include <iostream.h>
#include "TBSLayout.hh"

int main()
{
    /*TElementPath path = "kazio#ziutek";

    TBoard                  a("a",3),
         b1("b1",1),     b2("b2",2),             b3("b3",1),
         c1("c1",0),  c2("c2",0),c3("c3",0),     c4("c4",0);

    a.AddElement( b1 );
    a.Describe();
    a.AddElement( b2 );
    a.Describe();
    a.AddElement( b3 );
    a.Describe();

    b1.AddElement( c1 );
    a.Describe();
    b2.AddElement( c2 );
    a.Describe();
    b2.AddElement( c3 );
    a.Describe();
    b3.AddElement( c4 );
    a.Describe();

    a.Describe();
    cout << path.GetHead() << "---"<< path.GetTail()<<"-pupa\n";
              */

    TPattern pat("10X01");
    TCharSequence seq("10101");
    cout <<"Compare:"<<pat.Compare(seq)<<endl;

    TInstruction inst("SAMPLE", "BOUNDARY");
    inst.AddOpCode("1100");                   
    inst.AddOpCode("1X10");
    inst.SetCaptureVal("1XX01");


    cout
      << "GetOpCode:" << inst.GetOpCode() << endl
      << "CheckOpCode(1100):" << inst.CheckOpCode("1100") << endl
      << "CheckOpCode(1110):" << inst.CheckOpCode("1110") << endl
      << "CheckCaptureVal(1100):" << inst.CheckCaptureVal("11001") << endl
      ;
    char c;
    cin >> c;
	 return 0;
}