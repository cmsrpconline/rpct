#if !defined TSTACK_HH
#define TSTACK_HH

#include <stdio.h>


template<class T>
class TStack {
    TStack *Next;
    T Value;
public:
    TStack() : Next(NULL) {}
    ~TStack() 
	{
	    if( Next != NULL )
		delete Next;
	}
    int Empty()
	{
	    return Next==NULL;
	}
    T Top()
	{
	    if( Next==NULL )
		return Value;
	    else
		return Next->Top();
	}
    int Push( T val )  //returns 1 on success, 0 on failure
	{
	    if( Next != NULL )
		return Next->Push(val);
	    if( NULL == (Next=new TStack) )
		return 0;
	    Next->Value=val;
	    return 1;
	}
    T Pop()
	{
	    if( Empty() ) {
		cerr <<"Error: Trying to pop from an empty stack\n";
		exit(1);
	    }
	    if( Next->Next != NULL )
		return Next->Pop();
	    T nextval = Next->Value;
	    delete Next;
	    Next = NULL;
	    return nextval;
	}

};

#endif
