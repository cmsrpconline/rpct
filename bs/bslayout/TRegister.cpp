#include "TRegister.h"


using namespace bs;
using namespace std;

      

const std::string TRegister::BOUNDARY_REG_NAME    = "BOUNDARY";
const std::string TRegister::BYPASS_REG_NAME      = "BYPASS";
const std::string TRegister::INSTRUCTION_REG_NAME = "INSTRUCTION";
const std::string TRegister::IDCODE_REG_NAME      = "IDCODE";




void TRegister::SetState(TElementState st, TBufferType bt )
{
    TStates::iterator iState = States.find(st);
    if (iState == States.end())
      throw TException("TRegister::SetState: State " + st
               + " not found. Element: " + GetName());

    if (bt== btSEND)
      SendSequence = iState->second;
    else
      RecSequence = iState->second;
}
    

TRegister::TElementState TRegister::GetState(TBufferType bt)
{
    for(TStates::iterator iState = States.begin(); iState != States.end(); ++iState)
      if (iState->second == ((bt==btSEND) ? SendSequence : RecSequence))
        return iState->first;
                   
	throw TException("TRegister::GetState:"
			"not known state: " + ((bt==btSEND) ? SendSequence : RecSequence)
			+ ": Element: " + GetName());
}


/*

void TRegister::SetStateSeq(TScanVector& seq, TBufferType bt)
{
    int offset=0;
    for(reverse_iterator iter = rbegin(); iter != rend(); ++iter) {
      (*iter)->SetStateSeq(
                   TScanVector(seq, offset, (*iter)->GetLength() ),
                   bt);
      offset += (*iter)->GetLength();
    }
}



TScanVector TRegister::GetStateSeq(TBufferType bt)
{
    TScanVector seq;
    for(reverse_iterator iter = rbegin(); iter != rend(); ++iter)
      seq += (*iter)->GetStateSeq(bt);
    return seq;
}



int TRegister::GetLength()
{
    int len=0;
    for(reverse_iterator iter = rbegin(); iter != rend(); ++iter)
	  len += (*iter)->GetLength();
    return len;
}    */

       

void TRegisterFragment::AddState(TElementState st, const bs::TPattern& pat)
{
  TPattern::size_type pos = pat.find('X');
  if (pos == TPattern::npos) {
    TScanVector vec(pat);
    AddState(st, vec);
  }
  else {
    TPattern pat1(pat);
    pat1[pos] = '0';
    AddState(st, pat1);
    pat1[pos] = '1';   
    AddState(st, pat1);
  };
}

void TRegisterFragment::SetStateSeq(const TScanVector& seq, TBufferType bt)
{
  if (seq.size() != Cells.size())
    throw TException("TRegisterFragment::SetStateSeq: " + GetName() + " bad seq length"); 
  TCells::iterator iCell = Cells.begin();
  TScanVector::const_iterator iVal = seq.begin();
  for(; iCell != Cells.end(); ++iCell, ++iVal)
    Parent.SetCellState(*iCell, *iVal, bt);
}

TScanVector TRegisterFragment::GetStateSeq(TBufferType bt)
{
  TScanVector seq(Cells.size(), 0);
  
  TCells::iterator iCell = Cells.begin();
  TScanVector::iterator iVal = seq.begin();
  for(; iCell != Cells.end(); ++iCell, ++iVal)
    *iVal = Parent.GetCellState(*iCell, bt);

  return seq;
}


void TRegisterFragment::SetState(TElementState st, TBufferType bt )
{
    TStates::iterator iState = States.find(st);
    if (iState == States.end())
      throw TException("TRegisterFragment::SetState: State " + st
               + " not found. Element: " + GetName());

    SetStateSeq(iState->second, bt);
}


TRegisterFragment::TElementState TRegisterFragment::GetState(TBufferType bt)
{
    TScanVector seq = GetStateSeq(bt);
    for(TStates::iterator iState = States.begin(); iState != States.end(); ++iState)
      if (iState->second == seq)
        return iState->first;
                   
	throw TException("TRegisterFragment::GetState:"
			"not known state: " + seq
			+ ": Element: " + GetName());
}


int TRegisterFragment::GetCellIndex( int cell_no )
{
    int i=0;
    for(TCells::iterator iCell = Cells.begin(); iCell != Cells.end(); ++iCell, i++)
	  if( *iCell == cell_no )
	    return i;
        
    throw( TException("TSignal: GetIndex: no such cell") );
}


void TRegisterFragment::Describe(ostream& ost)
{
    int i;
    TElement::Describe(ost);

    ost <<  string(DescribeOffset+5,' ') << "Cells: ";
    for(iterator iter=begin(); iter!=end(); ++iter )
      ost << *iter << ' ';
    ost << endl;

    ost <<  string(DescribeOffset+5,' ') << "States: " << endl;
    for(TStates::iterator iState=States.begin(); iState!=States.end(); ++iState)
      ost << string(DescribeOffset+7,' ') << iState->first
          << ": " << iState->second << endl;
}



TFragmentedRegister::~TFragmentedRegister()
{
  for(TFragments::iterator iter = Fragments.begin(); iter != Fragments.end(); ++iter)
    delete iter->second;
}




void TFragmentedRegister::Describe(std::ostream& ost)
{
    TRegister::Describe(ost);

    ost << string(DescribeOffset,' ')
         << "  Fragments:" <<  Fragments.size() <<  endl;

    for(TFragments::iterator iFrag=Fragments.begin(); iFrag!=Fragments.end(); ++iFrag ) {
	  ost <<  string(DescribeOffset,' ') << iFrag->first << ": ";
      iFrag->second->Describe(ost);
    };

}
