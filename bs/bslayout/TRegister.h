#ifndef TREGISTER_HH
#define TREGISTER_HH

#include "TComplexElement.h"
                              
namespace bs {


class TRegister : public TElement {
public:
  typedef std::string TElementState;
private:
    // make copy constructor private - it should not be used
    TRegister(const TRegister&);
protected:
  int Length;       
  typedef std::multimap<TElementState, TScanVector> TStates;
  TStates States;
  TScanVector SendSequence;
  TScanVector RecSequence;
  TScanVector CaptureValue;
public:

    static const std::string BOUNDARY_REG_NAME;
    static const std::string BYPASS_REG_NAME;
    static const std::string INSTRUCTION_REG_NAME;
    static const std::string IDCODE_REG_NAME;

    TRegister(const std::string& name, int length)
      : TElement(name), Length(length),
        SendSequence(length, '0'), RecSequence(length, '0'){}

    TRegister(const std::string& name, int length, const TScanVector& capture)
      : TElement(name), Length(length), SendSequence(length, '0'), 
      RecSequence(length, '0'), CaptureValue(capture) {}

    virtual ~TRegister() {}

    virtual int GetLength()
      { return Length; }   

    virtual void AddState( TElementState& st, TScanVector& seq )
      {
        States.insert( TStates::value_type(st, seq) );
      }

    virtual void SetState(TElementState, TBufferType bt=btSEND );

    virtual TElementState GetState(TBufferType bt=btREC );

    virtual void SetStateSeq(const TScanVector& seq, TBufferType bt=btSEND)
      {
        if (bt == btSEND)
          SendSequence.assign(seq, 0, Length);
        else
          RecSequence.assign(seq, 0, Length);
      }

    virtual TScanVector GetStateSeq(TBufferType bt=btREC)
      {
        return (bt==btSEND) ? SendSequence : RecSequence;
      }
      
    virtual void SetCellState(int pos, TScanVector::value_type val, TBufferType bt=btSEND)
      {
        if (bt == btSEND)
          SendSequence[pos] = val;
        else
          RecSequence[pos] = val;
      }

    virtual TScanVector::value_type GetCellState(int pos, TBufferType bt=btREC)
      {
        return (bt==btSEND) ? SendSequence[pos] : RecSequence[pos];
      }

    virtual TScanVector GetCaptureValue()
      {
        return CaptureValue;
      }
};


class TBypassRegister: public TRegister {
private:
    // make copy constructor private - it should not be used
    TBypassRegister(const TBypassRegister&);
public:
    TBypassRegister(): TRegister(BYPASS_REG_NAME, 1) {}
    virtual ~TBypassRegister() {}
};

class TInstructionRegister: public TRegister {
private:
    // make copy constructor private - it should not be used
    TInstructionRegister(const TInstructionRegister&);
public:
    TInstructionRegister(int length, const TScanVector& capture)
      : TRegister(INSTRUCTION_REG_NAME, length, capture) {}
    virtual ~TInstructionRegister() {}
};    

class TIDCodeRegister: public TRegister {
private:
    // make copy constructor private - it should not be used
    TIDCodeRegister(const TIDCodeRegister&);
public:
    TIDCodeRegister(const TScanVector& capture)
      : TRegister(IDCODE_REG_NAME, 32, capture) {}
    virtual ~TIDCodeRegister() {}
};
    
// represents fragment of a register
class TRegisterFragment : public TElement {
public:
  typedef std::string TElementState;
  typedef std::vector<int> TCells;
  typedef TCells::const_iterator const_iterator;
  typedef TCells::iterator iterator;
  typedef TCells::const_reverse_iterator const_reverse_iterator;
  typedef TCells::reverse_iterator reverse_iterator;  
private:
    // make copy constructor private - it should not be used
    TRegisterFragment(const TRegisterFragment&);
protected:
  TRegister& Parent;
  TCells Cells;
  typedef std::multimap<TElementState, TScanVector> TStates;
  TStates States;
public:     
    TRegisterFragment(const std::string& name, TRegister& parent, const TCells& cells)
      : TElement(name), Parent(parent), Cells(cells) {}

    TRegisterFragment(const std::string& name, TRegister& parent)
      : TElement(name), Parent(parent){}

    virtual void AddCell(int cell_no)
      {
        Cells.push_back(cell_no);
      }

    virtual void AddCells(const TCells& cells)
      {
        Cells.insert(Cells.end(), cells.begin(), cells.end());
      }

    virtual void AddState( TElementState& st, const TScanVector& seq )
      {
        States.insert( TStates::value_type(st, seq) );
      }
        
    virtual void AddState(TElementState st, const TPattern& pat);

    virtual void SetState(TElementState, TBufferType bt=btSEND);

    virtual TElementState GetState(TBufferType bt=btREC);

    virtual void SetStateSeq(const TScanVector& seq, TBufferType bt=btSEND);

    virtual TScanVector GetStateSeq(TBufferType bt=btREC);
                                      
    virtual int GetCellIndex( int cell_no );
    
    // STL like iterators

    iterator begin ()
      { return Cells.begin();  }

    const_iterator begin () const
      { return Cells.begin();  }

    iterator end ()
      { return Cells.end();  }

    const_iterator end ()   const
      { return Cells.end();  }

    // reverse iterators    
    reverse_iterator rbegin ()
      { return Cells.rbegin(); }

    const_reverse_iterator rbegin () const
      { return Cells.rbegin(); }

    reverse_iterator rend ()
      { return Cells.rend(); }

    const_reverse_iterator rend () const
      { return Cells.rend(); }


    virtual int size()
      { return Cells.size(); }
      
    virtual void Describe(std::ostream& ost);
};

  
class TFragmentedRegister: public TRegister {
private:
    // make copy constructor private - it should not be used
    TFragmentedRegister(const TFragmentedRegister&);
protected:
  typedef std::map<std::string, TRegisterFragment*> TFragments;
  TFragments Fragments;  
public:
  TFragmentedRegister(const std::string& name, int length)
    : TRegister(name, length) {}
  virtual ~TFragmentedRegister();

  virtual void AddFragment(TRegisterFragment& fragment)
    {
      Fragments.insert( TFragments::value_type(fragment.GetName(), &fragment) );
    }

  TRegisterFragment& operator[] (const std::string& key )
    {
      TFragments::iterator iFrag = Fragments.find(key);
      if (iFrag == Fragments.end())
        throw ENonExistentKey(key, "");

      return *iFrag->second;
    }
    
    virtual void Describe(std::ostream& ost);
};


} // namespace bs

#endif
