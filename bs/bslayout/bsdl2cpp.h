#ifndef BSDL2CPP_H
#define BSDL2CPP_H
#include <fstream>
#include <conio>
#include <string>
#include <sstream>
#include <vector>
#include <typeinfo>
#include <stdio.h>
#include "TChip.h"

//#include "tb_std.h"

namespace bs {


class EBsdl2cpp : public TException
{
public:
    EBsdl2cpp (const std::string& msg)
       : TException ("Bsdl2cpp ERROR: " + msg ) {}

    EBsdl2cpp ()
       : TException ("Bsdl2cpp ERROR") {}
};
/*#include <exception>              //if program is Consol Apllication
                                  //it cant be lincked with tb_std
class EBsdl2cpp : public std::runtime_error
{
public:
    EBsdl2cpp (const std::string& msg)
       : std::runtime_error ("Bsdl2cpp ERROR: " + msg ) {}

    EBsdl2cpp ()
       : std::runtime_error ("Bsdl2cpp ERROR") {}
};     */

class EChipFactory : public EBsdl2cpp
{
public:
    EChipFactory (const std::string& msg)
       : EBsdl2cpp ("Chip Factory ERROR: " + msg ) {}

    EChipFactory ()
       : EBsdl2cpp ("Chip Factory ERROR") {}
};

class EPinMapParsing : public EBsdl2cpp
{
public:
      EPinMapParsing( const std::string& msg )
         : EBsdl2cpp ("pin map parsing ERROR in line " + msg) {}

      EPinMapParsing()
         : EBsdl2cpp ("pin map parsing ERROR") {}
};

class EInstructionParsing : public EBsdl2cpp
{
public:
      EInstructionParsing( const std::string& msg )
         : EBsdl2cpp ("instruction parsing ERROR in line " + msg) {}

      EInstructionParsing()
         : EBsdl2cpp ("instruction parsing ERROR") {}
};

class EBoundaryRegisterParsing : public EBsdl2cpp
{
public:
      EBoundaryRegisterParsing( const std::string& msg )
         : EBsdl2cpp ("boundary register parsing ERROR in line " + msg) {}

      EBoundaryRegisterParsing()
         : EBsdl2cpp ("boundary register parsing ERROR") {}
};

class EInstructionCaptureParsing : public EBsdl2cpp
{
public:
      EInstructionCaptureParsing( const std::string& msg )
         : EBsdl2cpp ("instruction capture parsing ERROR in line " + msg) {}

      EInstructionCaptureParsing()
         : EBsdl2cpp ("instruction capture parsing ERROR") {}
};

class EIdCodeParsing : public EBsdl2cpp
{
public:
      EIdCodeParsing( const std::string& msg )
         : EBsdl2cpp ("IdCode parsing ERROR in line " + msg) {}
      EIdCodeParsing()
         : EBsdl2cpp ("IdCode parsing ERROR") {}
};

class TBsdlParser
{
   std::ifstream file;
   std::ostream* Log;         //for printing messages
   public:
   TBsdlParser(const char* fileName, std::ostream* log = NULL )
    {
      file.open(fileName);
      cur_line=0;
      if (!file)
        throw EBsdl2cpp ("can't open the file " + std::string(fileName) );
      Log = log;
    }

private:
   int cur_line;
   void RemoveComments(std::string& line)
    {
      int p;
      p=line.find("--");
      if(p!= -1)
        {
          line.erase(p);
        }
    }

   struct TInstr
   {
    std::string name;
    std::string opcode;
   };

   struct TPinMap
    {
     std::string sig;
     std::string pin;
    };

   struct TPinInfo
    {
      std::string name;
      std::string port;
      std::string function;
      std::string safe;
      std::string ccell;    //-1 jak nie ma
      std::string disval;
      std::string rslt;
      std::string bc;      //not used by Michal
    };
public:
   typedef std::vector<TInstr> TInstVec;
   typedef std::vector<TPinMap> TPinMapVec;
   typedef std::vector<TPinInfo> TPinInfoVec;

   TInstVec InstVec;
   TPinMapVec PinMapVec;
   TPinInfoVec PinInfoVec;
   std::string Name;
   std::string Lenght;
   std::string InstCapture;
   std::string IdCode;
private:
   void PinMapParsing();
   void InstructionParsing();
   void BoundaryRegisterParsing();
   void NameParsing();
   void InstructionCaptureParsing();
   void IdCodeParsing();
   void BoundaryLenghtParsing();

public:
   void Run()
   {
     BoundaryRegisterParsing();
     PinMapParsing();
     InstructionParsing();
     NameParsing();
     InstructionCaptureParsing();
     IdCodeParsing();
     BoundaryLenghtParsing();
     if(Log != NULL)
       (*Log)<<"\nSUCCES\n";
   }
};
//------------------------------------------------------------------------------
class TChipFactory
{
private:
  std::ostream* Log;     //for printing messages
  std::string InfoFileName;
  bs::TChip* Chip;         //needs to be destroyed????????????????????????????????
public:
  TChipFactory(char* infoFileName = NULL, std::ostream* log = NULL )
    {
      InfoFileName = infoFileName;
      Log = log;
    }

/*  ~TChipFactory()
    {
      delete Chip;
    }*/

  std::string GetBSDLFileForChip(std::string chipName);

  bs::TChip& CreateChipFromFile(const char* BSDLFileName);
  bs::TChip& CreateChip(std::string chipName)
   {
    return  CreateChipFromFile(GetBSDLFileForChip(chipName).c_str());
   }

private:


  bs::TBoundaryCell::TCellType ConvertCellType( std::string bc)
    {
      if (bc=="BC_1")
        return bs::TBoundaryCell::BC_1;
      else if (bc=="BC_4")
        return bs::TBoundaryCell::BC_4;
      else
        return bs::TBoundaryCell::BC_1; //<<<<<<<<<<<<<<<<<<<<<should be changed on error or somthing else
    }

  bs::TBoundaryCell::TFunction ConvertFunction( std::string bc)
    {
       if(bc == "input")
         return bs::TBoundaryCell::INPUT;
       else if(bc == "clock")
         return bs::TBoundaryCell::CLOCK;
       else if(bc == "output2")
         return bs::TBoundaryCell::OUTPUT2;
       else if(bc == "output3")
         return bs::TBoundaryCell::OUTPUT3;
       else if(bc == "control")
         return bs::TBoundaryCell::CONTROL;
       else if(bc == "controlr")
         return bs::TBoundaryCell::CONTROLR ;
       else if(bc == "internal")
         return bs::TBoundaryCell::INTERNAL;
       else if(bc == "bidir")
         return bs::TBoundaryCell::BIDIR;
       else if(bc == "observe_only")
         return bs::TBoundaryCell::OBSERVE_ONLY;
    //   else
    //     return -1;   //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<zmienic na error albo cos innego
   }

   bs::TBoundaryCell::TDisResult ConvertDisResult( std::string rslt)
    {
      if(rslt == "Z")
       return bs::TBoundaryCell::Z;
      else if(rslt == "WEAK0")
       return bs::TBoundaryCell::WEAK0;
      else if(rslt == "WEAK1")
       return bs::TBoundaryCell::WEAK1;
      else if(rslt == "PULL0")
       return bs::TBoundaryCell::PULL0;
      else if(rslt == "PULL1")
       return bs::TBoundaryCell::PULL1;
     // else
   //     return -1;   //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<zmienic na error albo cos innego
    }

   std::string ConvertRegName(std::string reg_name)
    {
      std::string s="USERCODE";
      if(reg_name == "BYPASS")
        return  bs::TRegister::BYPASS_REG_NAME;
      else if(reg_name == "EXTEST")
        return  bs::TRegister::BOUNDARY_REG_NAME;
      else if(reg_name == "SAMPLE")
        return  bs::TRegister::BOUNDARY_REG_NAME;
      else if(reg_name == "IDCODE")
        return  bs::TRegister::IDCODE_REG_NAME;
      else if(reg_name == "USERCODE")
        return s;
//      else
//      return ??? <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    }
};



} // namespace bs


#endif

