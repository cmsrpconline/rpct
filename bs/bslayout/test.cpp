#include <typeinfo>
#include <iostream.h>
#include "TBoard.h"
#include "TChip.h"
#include "TPac2.h"
#include "TAltera2.h"

#include <string>
#include <sstream>
#include <map>
#include <time.h>

using namespace bs;
using namespace std;


int main()
{

  try {


    TBoard board("board");

    //TPac2& pac2 = *new TPac2("pac2");
    //board.AddElement(pac2);

    //pac2.Describe(cout);

    TAltera2& alt = *new TAltera2("alt");
    board.AddElement(alt);

    //alt.Describe(cout);

    /*TFragmentedRegister& reg = dynamic_cast<TFragmentedRegister&>(board.Chip("pac2")["BLOCK9"]);
    reg["A_1_msp1"].SetStateSeq("0001");
    reg["A_80_msn4"].SetStateSeq("0010");
    reg["A_1"].SetStateSeq("010");
    reg["A_62"].SetStateSeq("100");
    pac2.SetState("BLOCK9");*/

    /*TFragmentedRegister& reg = dynamic_cast<TFragmentedRegister&>(board.Chip("pac2")["MASK"]);
    reg["ms2_or4"].SetStateSeq("1000000000");

    pac2.SetState("MASK");*/
    /*board["pac2"].  SetStateSeq(pac1|DR2|P001_MX1","1010");
    board.SetStateSeq("pac1|DR2|DO16_01","0101");
    board.SetState("pac1","USER_7");  */


    board.Chip("alt").Pin("E14").SetState(TPin::HIGH_IN);// SetState("alt|pin|U12","i1");
    board.Chip("alt").Pin("F4").SetState(TPin::HIGH_OUT);// board.SetState("alt|pin|T12","o1");
    board.Chip("alt").Pin("F1").SetState(TPin::LOW_IN);// board.SetState("alt|pin|U13","i0");
    board.Chip("alt").Pin("G3").SetState(TPin::LOW_OUT);// board.SetState("alt|pin|R12","o0");

    board.Chip("alt").SetState("SAMPLE");


    cout << "alt|sig|IOE14 = " << board.Chip("alt").Signal("IOE14").GetState(IElement::btSEND) << endl;
    cout << "alt|sig|IOF4 = " << board.Chip("alt").Signal("IOF4").GetState(IElement::btSEND) << endl;
    cout << "alt|sig|IOF1 = " << board.Chip("alt").Signal("IOF1").GetState(IElement::btSEND) << endl;
    cout << "alt|sig|IOG3 = " << board.Chip("alt").Signal("IOG3").GetState(IElement::btSEND) << endl;
      
   /* TScanVector seq;
    a.GetStateSeq( "pac2|BLOCK1|A_01_msp1", seq, SEND );
    cout << "pac2|BLOCK1|A_01_msp1 = " << seq << endl;

    seq.clear();
    a.GetStateSeq( "pac2|BLOCK1|A_80_msn4", seq, SEND );
    cout << "pac2|BLOCK1|A_80_msn4 = " << seq << endl;

    seq.clear();
    a.GetStateSeq( "pac2|BLOCK1|A_01_or16", seq, SEND );
    cout << "pac2|BLOCK1|A_01_or16 = " << seq << endl;

    seq.clear();
    a.GetStateSeq(  "pac2|BLOCK1|A_62_or1", seq, SEND );
    cout << "pac2|BLOCK1|A_62_or1  = " << seq << endl;*/


    TScanVector irseq;
    irseq = board.GenerateIRSequence();

    cout <<"ir="<<irseq<<endl;

    TScanVector drseq;

    drseq = board.GenerateDRSequence();
    cout <<"dr="<<drseq<<endl;

    board.InterpreteDRSequence( drseq );

    //board.Describe(cout);

  }
  catch( TException& e ) {
      cerr << "Blad: " << e.what() << endl;
  }
  char c;
  cin >> c;
  return 0;
}
