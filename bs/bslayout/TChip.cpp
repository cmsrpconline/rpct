#include "TChip.h"

       
using namespace bs;
using namespace std;




const string TChip::BYPASS_INST_NAME     = "BYPASS";
const string TChip::SAMPLE_INST_NAME     = "SAMPLE";
const string TChip::INTEST_INST_NAME     = "INTEST";
const string TChip::EXTEST_INST_NAME     = "EXTEST";
const string TChip::IDCODE_INST_NAME     = "IDCODE";


           
TChip::~TChip()
{
  for(TInstructions::iterator iInst = Instructions.begin(); iInst != Instructions.end(); ++iInst)
    delete iInst->second;
}

void TChip::AddInstruction( TInstruction& inst )
{
    Instructions.insert(
               TInstructions::value_type(inst.GetName(), &inst ) );

    if (inst.GetName() == IDCODE_INST_NAME)
        CurrentInst = DefaultInst = IDCODE_INST_NAME;
}


void TChip::AddRegister( TRegister& reg )
{
    AddElement( reg );	
}



void TChip::SetState(TElementState st, TBufferType bt)
{
    CurrentInst = st;
    InstructionRegister.SetStateSeq(GetCurrentInst().GetOpCode(), bt);
    /*
    else {
      char* head = path.GetHead();
      if( !strcmp(head, SIGNAL_PREFIX) ||  !strcmp(head, PIN_PREFIX)  )
          (*Elements)[BOUNDARY_REG_NAME].SetState( path, st, bt );
      else
          (*Elements)[head].SetState( path.GetTail(), st, bt );
      delete [] head;
    } */
}



TChip::TElementState TChip::GetState(TBufferType bt )
{
    return CurrentInst;
    /*
    else {
      TElementState st;
      char* head = path.GetHead();
      if( !strcmp(head, SIGNAL_PREFIX) ||  !strcmp(head, PIN_PREFIX)  )
          st = (*Elements)[BOUNDARY_REG_NAME].GetState( path, bt );
      else
          st = (*Elements)[head].GetState( path.GetTail(), bt );
      delete [] head;
      return st;
    } */
}


void TChip::SetStateSeq(const TScanVector& seq, TBufferType bt )
{
    TInstructions::iterator iter = Instructions.begin();
    for(; iter != Instructions.end(); ++iter)
      if( iter->second->CheckOpCode(seq) ) {
        InstructionRegister.SetStateSeq(seq, bt);
        CurrentInst = iter->second->GetName();
        return;
      }

    throw TException( string("TChip: ") + GetName()
                        + string(": bad instruction reg sequence:") + (string)seq );

/*
    }
    else
        TComplexElement<TRegister>::SetStateSeq( path, seq, bt );*/
}


TScanVector TChip::GetStateSeq(TBufferType bt)
{
	return InstructionRegister.GetStateSeq(bt);
    /*
    else
        TComplexElement<TRegister>::GetStateSeq( path, seq, bt );*/
}

