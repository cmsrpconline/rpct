#ifndef TPAC_HH
#define TPAC_HH

#include <typeinfo>
#include <stdio.h>
#include "TChip.h"

namespace bs {

class TPac : public TChip {
public:
    TPac(const std::string&);
};

} // namespace bs


#endif




