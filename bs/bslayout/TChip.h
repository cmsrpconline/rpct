#ifndef TCHIP_HH
#define TCHIP_HH


#include "TBoardElement.h"
#include "TComplexElement.h"
#include "TInstruction.h"
#include "TRegister.h"
#include "TBoundaryRegister.h"


namespace bs {

class TChip : public IBoardElement, public TComplexElement<TRegister>  {
public:
  typedef std::string TElementState;
private:
    // make copy constructor private - it should not be used
    TChip(const TChip& chip);
protected:
    typedef std::map<std::string, TInstruction*> TInstructions;
    TInstructions Instructions;
    TElementState CurrentInst;
    TElementState DefaultInst;

    TInstructionRegister& InstructionRegister;
    TBoundaryRegister& BoundaryRegister;
    
    int IRLength;

    TInstruction& GetCurrentInst()
      {
        TInstructions::iterator iInst = Instructions.find(CurrentInst);
        if (iInst == Instructions.end())
          throw TException("TChip::GetCurrentInst: instruction " + CurrentInst + " not found");

        return (*iInst->second);
      }

    TRegister& GetCurrentDR()
      {
        return (*this)[GetCurrentInst().GetRegisterName()];
      }


public:

    static const std::string BYPASS_INST_NAME;
    static const std::string SAMPLE_INST_NAME;
    static const std::string INTEST_INST_NAME;
    static const std::string EXTEST_INST_NAME;
    static const std::string IDCODE_INST_NAME;

    TChip(const std::string& name, const TScanVector& inst_capture, int boundary_length)
      : TComplexElement<TRegister>(name),
        InstructionRegister( *new TInstructionRegister(inst_capture.size(), inst_capture)),
        BoundaryRegister( * new TBoundaryRegister(boundary_length)),
        IRLength(inst_capture.size())
      {
          AddRegister( *new TBypassRegister() ); 
          AddRegister( InstructionRegister );
          AddRegister( BoundaryRegister );
          CurrentInst = DefaultInst = BYPASS_INST_NAME;
      };
      
    TChip(const std::string& name, const TScanVector& inst_capture, int boundary_length,
          const TScanVector& idcode)
      : TComplexElement<TRegister>(name),
        InstructionRegister( *new TInstructionRegister(inst_capture.size(), inst_capture)),
        BoundaryRegister( * new TBoundaryRegister(boundary_length)),
        IRLength(inst_capture.size())
      {
          AddRegister( *new TBypassRegister() ); 
          AddRegister( InstructionRegister );
          AddRegister( BoundaryRegister );
          if (idcode.empty())
            CurrentInst = DefaultInst = BYPASS_INST_NAME;
          else {
            CurrentInst = DefaultInst = IDCODE_INST_NAME;
            AddRegister( *new TIDCodeRegister(idcode) );
          };
      };

    virtual ~TChip();

    virtual void AddInstruction( TInstruction& );

    virtual void AddRegister( TRegister& );


    ////////////////////////////////////////////////////
    virtual void SetState(TElementState, TBufferType bt=btSEND);

    virtual TElementState GetState(TBufferType bt=btREC);

    virtual void SetStateSeq(const TScanVector&, TBufferType bt=btSEND);

    virtual TScanVector GetStateSeq(TBufferType bt=btREC);

    //////////////////////////////////////////////////////
    /*virtual void GenerateIRSequence(TScanVector& seq)
      {
        seq += InstructionRegister.GetStateSeq(btSEND);
      }

    virtual void GenerateDRSequence(TScanVector& seq)
      {
        seq += GetCurrentDR().GetStateSeq(btSEND);
      }*/
    virtual TScanVector GenerateIRSequence(TBufferType bt=btSEND)
      {
        return GetStateSeq(bt);
      }

    virtual TScanVector GenerateDRSequence(TBufferType bt=btSEND)
      {
        return GetCurrentDR().GetStateSeq(bt);
      }

    virtual void InterpreteIRSequence(TScanVector& seq, TBufferType bt=btREC)
      {
          InstructionRegister.SetStateSeq(seq, bt);
      }

    virtual void InterpreteDRSequence(TScanVector& seq, TBufferType bt=btREC)
      {
          GetCurrentDR().SetStateSeq(seq, bt);
      }


    virtual int GetIRLength()
      {  return IRLength;  }

    virtual int GetDRLength()
      {
        return GetCurrentDR().GetLength();
      }        

    void AddCell(TBoundaryCell& cell)
      {
        BoundaryRegister.AddCell(cell);
      }

    void AddPinMapping(const std::string& port_name, const std::string& pin_name)
      {
        BoundaryRegister.AddPinMapping(port_name, pin_name);
      }

    // should be called after the chips initialization has ended
    void InitEnd()
      {
        BoundaryRegister.ScanBoundaryCells();
      }

    TSignal& Signal(const std::string& name)
      {
        return BoundaryRegister.Signal(name);
      }

    TSignal& Pin(const std::string& name)
      {                                      
        return BoundaryRegister.Pin(name);
      }

    /////////////////////////////////////////////////////
    virtual void Describe(std::ostream& ost)
      {
          TElement::Describe(ost);
          ost << std::string(DescribeOffset,' ')
               << " CurrentInst=" << CurrentInst
               << std::endl;
          TComplexElement<TRegister>::Describe(ost);
      }
};








} // namespace bs

#endif
