#ifndef TCOMPLEXELEMENT_HH
#define TCOMPLEXELEMENT_HH


#include "TElement.h"
//#include "TElementPath.h"
//#include "..\TAssociativeArrayRef.h"  
#include "tb_std.h"
#include <vector>
#include <map>              
#include <string>


namespace bs {


class ENonExistentKey : public TException {
public:
    ENonExistentKey(std::string key, std::string msg) throw()
	  : TException( "Non existent key '" + key + "' " + msg ) {}

    ENonExistentKey(std::string key) throw()
	  : TException( "Non existent key '" + key + "' " ) {}
      
    EXCFASTCALL virtual ~ENonExistentKey() throw() {}
};



template<class ComponentType>
class TComplexElement : public TElement {
private:
    // make copy constructor private - it should not be used
    TComplexElement(const TComplexElement&);
protected:
    //TAssociativeArrayRef<ComponentType>* Elements;
    typedef std::vector<ComponentType*> TVector;
    typedef std::map<std::string, ComponentType*> TMap;

    TVector ElemVector;
    TMap    ElemMap;

    class FunctionDelete {
    public:
      void operator () (ComponentType* pointer)
        {
          delete pointer;
        }
    };              

    class FunctionDescribe {
      std::ostream& Ost;
    public:
      void operator () (ComponentType* pointer)
        {
          pointer->Describe(Ost);
        }
      FunctionDescribe(std::ostream& ost): Ost(ost) {}
    };

public:
    typedef typename TVector::const_iterator const_iterator;   
    typedef typename TVector::iterator iterator;
    typedef typename TVector::const_reverse_iterator const_reverse_iterator;
    typedef typename TVector::reverse_iterator reverse_iterator;
    
    //TComplexElement();

    TComplexElement(const std::string& name): TElement(name) {}

    virtual ~TComplexElement()
      {
        std::for_each(begin(), end(), FunctionDelete() );
        //for (iterator iter = begin(); iter != end(); ++iter)
        //  delete *iter;
      }

    // initializers

    virtual void AddElement( ComponentType& comp )
      {
          ElemVector.push_back( &comp );
          ElemMap.insert(typename TMap::value_type(comp.GetName(), &comp) );
      }

    ComponentType& operator[] (const std::string& key )
      {
        typename TMap::iterator iMap = ElemMap.find(key);
        if (iMap == ElemMap.end())
          throw ENonExistentKey(key, "");

        return *iMap->second;
      }

    ComponentType& operator[] (const int n )
      {
        return *ElemVector[n];
      }


    virtual void Describe(std::ostream& ost)
      {
          TElement::Describe(ost);
          DescribeOffset += DescribeOffsetDelta;

          //for (iterator iter = begin(); iter != end(); ++iter)
          //  (*iter)->Describe(ost);
          std::for_each(begin(), end(), FunctionDescribe(ost));
            
          DescribeOffset -= DescribeOffsetDelta;
      }

    // STL like iterators

    iterator begin ()
      { return ElemVector.begin();  }

    const_iterator begin () const
      { return ElemVector.begin();  }

    iterator end ()
      { return ElemVector.end();  }

    const_iterator end ()   const
      { return ElemVector.end();  }

    // reverse iterators    
    reverse_iterator rbegin ()
      { return ElemVector.rbegin(); }

    const_reverse_iterator rbegin () const
      { return ElemVector.rbegin(); }

    reverse_iterator rend ()
      { return ElemVector.rend(); }

    const_reverse_iterator rend () const
      { return ElemVector.rend(); }


    virtual int size()
      { return ElemVector.size(); }

    //    friend ostream& operator<<( ostream&, TComplexElement<ComponentType>& );

};




// template<class ComponentType>
// ostream& operator<<( ostream& os, TComplexElement<ComponentType>& el )
// {
//     os <<  typeid(el).name() <<": '" << el.GetName() << "':\n";
//  	 TAssociativeArrayRefIterator<ComponentType>  iter(Elements);
//  	 while(iter.More())
//  	     os << iter.Next().GetName() << endl;
//     return os;
// }


} // namespace bs

#endif
