[Options]
CodeGuard=yes
Stats=yes
MessageBox=yes
DebugInfo=yes
Append=no
Repeats=yes
Srcpath=
IgnoredModules=
ErrorCaption=CodeGuard Error
ErrorText=CodeGuard detected error(s) in the program. A log file will be created
OutputDebugString=no
StackFillFrequency=2
ResourceLeakReport=yes
MaxCGLerrors=65535
LimitCGLerrors=no
[memory block]
Track=yes
Param=yes
Leak=yes
Delay=yes
DelayMax=256
DelayMaxSize=512
[object array]
Track=yes
Param=yes
Leak=yes
Delay=yes
DelayMax=256
DelayMaxSize=512
[file handle]
Track=yes
Param=yes
Leak=yes
Delay=yes
DelayMax=4
DelayMaxSize=0
[file stream]
Track=yes
Param=yes
Leak=yes
Delay=yes
DelayMax=4
DelayMaxSize=0
[pipe stream]
Track=yes
Param=yes
Leak=yes
Delay=yes
DelayMax=4
DelayMaxSize=0
[directory stream]
Track=yes
Param=yes
Leak=yes
Delay=no
DelayMax=0
DelayMaxSize=0
