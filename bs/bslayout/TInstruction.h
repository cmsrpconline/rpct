#ifndef TINSTRUCTION_HH
#define TINSTRUCTION_HH

#include <typeinfo>
#include <string>
#include <algorithm>
#include <set>
#include "../TScanVector.h"

namespace bs {

class TInstruction {
private:
    std::string           Name;
    std::string           RegisterName;
    //std::set<TPattern, std::less<TPattern> >   OpCodes;
    typedef std::set<TScanVector> TOpCodes;
    TOpCodes OpCodes;
     
    // make copy constructor private - it should not be used
    TInstruction(const TInstruction&);
public:
    TInstruction(const std::string& name, const std::string& reg_name,
                 const TScanVector& opcode)
      : Name(name), RegisterName(reg_name)
      {
        OpCodes.insert(opcode);
      }

    ~TInstruction() {}

    void AddOpCode(const TScanVector& opcode)
      {
          OpCodes.insert(opcode);
      }

    TScanVector GetOpCode()
      {
        return *OpCodes.begin();
      }

    bool CheckOpCode(const TScanVector& seq)
      {
        return OpCodes.count(seq) > 0;
      }

    //bool CheckCaptureVal( TScanVector& seq)
    //  { return seq==CaptureVal; }

    std::string GetName()
      { return Name; }

    std::string GetRegisterName()
      { return RegisterName; }
};







} // namespace bs

#endif
