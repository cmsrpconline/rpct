#include <fstream>
#include <sstream>
#ifdef __BORLANDC__
#include <registry.hpp>
#endif
#include "BSLUtils.h"
//#include "TPac.h"
//#include "TAltera.h"
#include "TAltera2.h"
#include "TPac2.h"
#include "TChipFactory.h"

/*
[HKEY_LOCAL_MACHINE\Software\tb]

[HKEY_LOCAL_MACHINE\Software\tb\JTAG]
"TbBoard"="ac"
*/

                    
using namespace std;
using namespace bs;


#ifdef __BORLANDC__

TBoard* CreateTbBoardFromRegistry()
{
  TBoard* board;
  TRegistry* pRegistry = new TRegistry;
  TStringList* pNameList = new TStringList;
  TStringList* pTypeList = new TStringList;    
  TChipFactory factory("chipInfo.txt");
  try {

    pRegistry->RootKey = HKEY_LOCAL_MACHINE;
    // False because we do not want to create it if it doesn't exist
    if (!pRegistry->OpenKey("Software\\tb\\JTAG",false))
      throw TException("Could not open registry key: HKEY_LOCAL_MACHINE\\Software\\tb\\JTAG.");

    AnsiString curBoard = pRegistry->ReadString("TbBoard");

    if (curBoard == "")
      throw TException("Could not open registry: HKEY_LOCAL_MACHINE\\Software\\tb\\JTAG, Name: TbBoard.");

    if (!pRegistry->OpenKey(curBoard,false))
      throw TException(string("Could not open registry key: HKEY_LOCAL_MACHINE\\Software\\tb\\JTAG\\") + curBoard.c_str() + ".");

    pNameList->CommaText = pRegistry->ReadString("");
    //pRegistry->GetValueNames(pNameList);

    for (int i=0; i<pNameList->Count; i++)
      pTypeList->Add( pRegistry->ReadString(pNameList->Strings[i]) );

    // now we have names of chips in pNameList and corresponding
    // class names in pTypeList

    board = new TBoard("TbBoard");

    for( int i=0; i<pNameList->Count; i++ ) {
      char* name = pNameList->Strings[i].c_str();
      string type = pTypeList->Strings[i].c_str();

      //cout << type << ":" << name << endl;

      try {
        board->AddElement( factory.CreateChip(type, name) );
      }
      catch(EChipFactory& e) {

        if( type == "TPac2" )
          board->AddElement( *new TPac2(name) );
              else
                  if( type == "TAltera2" )
                          board->AddElement( *new TAltera2(name) );
                  else
                    throw TException(string("CreateTbBoard: Not supported chip type: '") + type + "'.");
        };
    }

    
    delete pRegistry;
    delete pNameList;
    delete pTypeList;

  }
  catch(...) {
    delete pRegistry;
    delete pNameList;
    delete pTypeList;
    throw;
  }


  return board;
}

#endif


TBoard* CreateTbBoardFromFile(char* filename)
{
#ifdef __BORLANDC__
  TBoard* board;
  //TRegistry* pRegistry = new TRegistry;
  TStringList* pNameList = new TStringList;
  TStringList* pTypeList = new TStringList;
  try {
    ifstream fin(filename);
    string name, type;
    const int BUFS = 256;
    char buf[BUFS];

    while(fin.getline(buf, BUFS)) { // Removes \n
      istringstream istr(buf);
      istr >> type >> name;
      pNameList->Add(name.c_str());
      pTypeList->Add(type.c_str());
    }

    // now we have names of chips in pNameList and corresponding
    // class names in pTypeList

    board = new TBoard("TbBoard");

    for( int i=0; i<pNameList->Count; i++ ) {
      char* name = pNameList->Strings[i].c_str();
      string type = pTypeList->Strings[i].c_str();

      cout << type << ":" << name << endl;

      //if( type == "TPac"  )
      //  board->AddElement( *new TPac(name) );
      //else
        if( type == "TPac2" )
          board->AddElement( *new TPac2(name) );
              else
                //if( type == "TAltera" )
                //        board->AddElement( *new TAltera(name) );
                //else
                  if( type == "TAltera2" )
                          board->AddElement( *new TAltera2(name) );
                  else
                    throw TException(string("CreateTbBoard: Not supported chip type: '") + type + "'.");
    }

    //delete pRegistry;
    delete pNameList;
    delete pTypeList;

  }
  catch(...) {
    //delete pRegistry;
    delete pNameList;
    delete pTypeList;
    throw;
  }


  return board;   
#endif
}


TBoard* CreateTbBoard(char* filename)
{
#ifdef __BORLANDC__
  if (filename==NULL)
    return CreateTbBoardFromRegistry();
#endif

  return CreateTbBoardFromFile(filename);
}




