#include "TBoundaryRegister.h"

using namespace bs;
using namespace std;


const TSignal::TElementState TSignal::HIGH_IN  = "i1";
const TSignal::TElementState TSignal::HIGH_OUT = "o1";
const TSignal::TElementState TSignal::LOW_IN   = "i0";
const TSignal::TElementState TSignal::LOW_OUT  = "o0";


TBoundaryRegister::~TBoundaryRegister()
{
  for(TBCells::iterator iter = BCells.begin(); iter != BCells.end(); ++iter)
    delete *iter;

  for(TSignals::iterator iter = Signals.begin(); iter != Signals.end(); ++iter)
    delete iter->second;
}


void TBoundaryRegister::SetSafeValues()
{  
    for(TBCells::iterator iCell = BCells.begin(); iCell != BCells.end(); ++iCell)
      SetCellState((*iCell)->GetNum(), //BCells.size() - (*iCell)->GetNum() - 1,
                   ((*iCell)->GetSafe() == 'X') ? '0' : '1',
                   btSEND);
}



void TBoundaryRegister::ScanBoundaryCells()
{                    
    // sort in descending order of cell.GetNum
    sort(BCells.begin(), BCells.end(), TCompareCells());


    for(TBCells::iterator iCell = BCells.begin(); iCell != BCells.end(); ++iCell) {
      string port = (*iCell)->GetPort();
      if (port != "*")  {
          TSignals::iterator iSig = Signals.find(port);
          if (iSig == Signals.end())
            throw TException("TBoundaryRegister::ScanBoundaryCells: signal not found."); 
          iSig->second->AddCell((*iCell)->GetNum());
          //iSig->second->AddCell(BCells.size() - (*iCell)->GetNum() - 1);
      }
    }


    // przelec przez tablice Signals i dopisz komorki kontrolne
    int ccell;
    for(TSignals::iterator iSig = Signals.begin(); iSig != Signals.end(); ++iSig) {
	  TSignal::TCells::iterator iCell = iSig->second->begin();
	  TSignal::TCells::iterator iCellEnd  = iSig->second->end();
	  for(; iCell != iCellEnd; ++iCell ) {
	    ccell = dynamic_cast<TBoundaryCell&>(*BCells[*iCell]).GetCCell();
	    if( ccell != -1 ) {
		  iSig->second->AddCell(ccell);
          //iSig->second->AddCell(BCells.size() - ccell - 1);
		  //i--; wykomentowalem, tzn zakladam, ze kontrolny nie moze byc kontrolowany
		  //break;
	    }
	  }
    }



    // ustaw HighIn
    TBoundaryCell::TFunction fnct;
    char disval, safe;
    for(TSignals::iterator iSig=Signals.begin(); iSig!=Signals.end(); ++iSig ) {

	  // sprawdz, czy ten sygnal moze byc wejsciowy
	  TSignal::TCells::iterator iCell = iSig->second->begin();
	  TSignal::TCells::iterator iCellEnd  = iSig->second->end();
	  for(; iCell != iCellEnd; ++iCell ) {
	    fnct = dynamic_cast<TBoundaryCell&>(*BCells[*iCell]).GetFunction();
	    if( (fnct == TBoundaryCell::INPUT)
		  || (fnct == TBoundaryCell::BIDIR) )
		break;
	  }
	  if (iCell == iCellEnd)
	    continue;

	  iCell = iSig->second->begin();
	  int j=0;
      bs::TPattern pat(iSig->second->size(), '0');
	  for(; iCell != iCellEnd; ++iCell ) {
        TBoundaryCell& cell = dynamic_cast<TBoundaryCell&>(*BCells[*iCell]);
	    fnct = cell.GetFunction();
	    switch (fnct) {
	      case TBoundaryCell::INPUT :
	      case TBoundaryCell::BIDIR :
		    pat[j] = '1';
		    ccell = cell.GetCCell();
		    if( ccell != -1 ) {
		      disval =  cell.GetDisVal();                                                     
		      pat[iSig->second->GetCellIndex(ccell)] = ((disval=='0') ? '1' : '0');
		      //pat[iSig->second->GetCellIndex(BCells.size() - ccell -1)] = ((disval=='0') ? '1' : '0');
	     	}
	     	break;
	      case TBoundaryCell::OUTPUT2 :
	      case TBoundaryCell::OUTPUT3 :
	     	/*safe = dynamic_cast<TBoundaryCell&>((*this)[*iCell]).GetSafe();
	    	(*Signals)[i].HighIn[j] = ((safe=='X') ? '0' : safe);*/
	    	pat[j] = 'X';
	    	ccell = cell.GetCCell();
	    	if( ccell != -1 ) {
		     disval = cell.GetDisVal();                                          
		     pat[iSig->second->GetCellIndex(ccell)] = disval;
		     //pat[iSig->second->GetCellIndex(BCells.size() - ccell - 1)] = disval;
		    }
		    break;
          case TBoundaryCell::CONTROL :
	      case TBoundaryCell::CONTROLR:
	      case TBoundaryCell::INTERNAL:
		    break;
	      default:
		    throw TException("TBoundaryRegister: not supported type of cell\n");
	    }
	    j++;
	  }

      iSig->second->AddState(TSignal::HIGH_IN, pat);
    }

    // ustaw HighOut

    for(TSignals::iterator iSig=Signals.begin(); iSig!=Signals.end(); ++iSig ) {

	  // sprawdz, czy ten sygnal moze byc wyjsciowy
	  TSignal::TCells::iterator iCell = iSig->second->begin();
	  TSignal::TCells::iterator iCellEnd  = iSig->second->end();
	  for(; iCell != iCellEnd; ++iCell ) {
	    fnct = dynamic_cast<TBoundaryCell&>(*BCells[*iCell]).GetFunction();
	    if( (fnct == TBoundaryCell::OUTPUT2)
		|| (fnct == TBoundaryCell::OUTPUT3)
		|| (fnct == TBoundaryCell::BIDIR) )
		break;
	  }
	  if(iCell == iCellEnd)
	    continue;

	  iCell = iSig->second->begin();
	  int j=0;
      bs::TPattern pat(iSig->second->size(), '0');
	  for(; iCell != iCellEnd; ++iCell ) {
        TBoundaryCell& cell = dynamic_cast<TBoundaryCell&>(*BCells[*iCell]);
	    fnct = cell.GetFunction();
	    switch( fnct) {
	    case TBoundaryCell::OUTPUT2 :
	    case TBoundaryCell::OUTPUT3 :
	    case TBoundaryCell::BIDIR :
		pat[j] = '1';
		ccell = cell.GetCCell();
		if( ccell != -1 ) {
		    disval =  cell.GetDisVal();
		    pat[iSig->second->GetCellIndex(ccell)] = ((disval=='0') ? '1' : '0');
		    //pat[iSig->second->GetCellIndex(BCells.size() - ccell - 1)] = ((disval=='0') ? '1' : '0');
		}
		break;
	    case TBoundaryCell::INPUT :
		/*safe = dynamic_cast<TBoundaryCell&>((*this)[*iCell]).GetSafe();
		(*Signals)[i].HighOut[j] = ((safe=='X') ? '0' : safe);*/
		pat[j] = 'X';
		ccell = cell.GetCCell();
		if( ccell != -1 ) {
		    disval = cell.GetDisVal();                                          
		    pat[iSig->second->GetCellIndex(ccell)] = disval;
		    //pat[iSig->second->GetCellIndex(BCells.size() - ccell - 1)] = disval;
		}
		break;
	    case TBoundaryCell::CONTROL :
	    case TBoundaryCell::CONTROLR:
	    case TBoundaryCell::INTERNAL:
		break;
	    default:
		throw TException("TBoundaryRegister: not supported type of cell\n");
	    }
	    j++;
	  }

      iSig->second->AddState(TSignal::HIGH_OUT, pat);
      //cout << iSig->second->GetName() << " high out " << pat << endl;
    }
    // ustaw LowIn

    for(TSignals::iterator iSig=Signals.begin(); iSig!=Signals.end(); ++iSig ) {

      // sprawdz, czy ten sygnal moze byc wejsciowy
	  TSignal::TCells::iterator iCell = iSig->second->begin();
	  TSignal::TCells::iterator iCellEnd  = iSig->second->end();
	  for(; iCell != iCellEnd; ++iCell ) {
	    fnct = dynamic_cast<TBoundaryCell&>(*BCells[*iCell]).GetFunction();
	    if( (fnct == TBoundaryCell::INPUT)
		|| (fnct == TBoundaryCell::BIDIR) )
		  break;
        }
        if(iCell == iCellEnd)
            continue;

	    iCell = iSig->second->begin();
	    int j=0;
        bs::TPattern pat(iSig->second->size(), '0');
	    for(; iCell != iCellEnd; ++iCell ) {
          TBoundaryCell& cell = dynamic_cast<TBoundaryCell&>(*BCells[*iCell]);
          fnct = cell.GetFunction();
          switch( fnct) {
          case TBoundaryCell::INPUT :
          case TBoundaryCell::BIDIR :
            pat[j] = '0';
            ccell = cell.GetCCell();
            if( ccell != -1 ) {
              disval = cell.GetDisVal();                                                               
              pat[iSig->second->GetCellIndex(ccell)] = ((disval == '0') ? '1' : '0');
              //pat[iSig->second->GetCellIndex(BCells.size() - ccell - 1)] = ((disval=='0') ? '1' : '0');
            }
            break;
          case TBoundaryCell::OUTPUT2 :
          case TBoundaryCell::OUTPUT3 :
            /*safe = dynamic_cast<TBoundaryCell&>((*this)[*iCell]).GetSafe();
            (*Signals)[i].LowIn[j] = ((safe=='X') ? '0' : safe);*/
            pat[j] = 'X';
            ccell = cell.GetCCell();
            if( ccell != -1 ) {
              disval = cell.GetDisVal();                                           
              pat[iSig->second->GetCellIndex(ccell)] = disval;
              //pat[iSig->second->GetCellIndex(BCells.size() - ccell - 1)] = disval;
            }
            break;
          case TBoundaryCell::CONTROL :
          case TBoundaryCell::CONTROLR:
          case TBoundaryCell::INTERNAL:
            break;
          default:
            throw TException("TBoundaryRegister: not supported type of cell\n");
        }
        j++;
      }

      iSig->second->AddState(TSignal::LOW_IN, pat);
    }


    // ustaw LowOut

    for(TSignals::iterator iSig=Signals.begin(); iSig!=Signals.end(); ++iSig ) {

	  // sprawdz, czy ten sygnal moze byc wyjsciowy
	  TSignal::TCells::iterator iCell = iSig->second->begin();
	  TSignal::TCells::iterator iCellEnd  = iSig->second->end();
	  for(; iCell != iCellEnd; ++iCell ) {
	    fnct = dynamic_cast<TBoundaryCell&>(*BCells[*iCell]).GetFunction();
	    if( (fnct == TBoundaryCell::OUTPUT2)
		|| (fnct == TBoundaryCell::OUTPUT3)
		|| (fnct == TBoundaryCell::BIDIR) )
		break;
	  }
	  if(iCell == iCellEnd)
	    continue;

	  iCell = iSig->second->begin();
	  int j=0;
      bs::TPattern pat(iSig->second->size(), '0');
	  for(; iCell != iCellEnd; ++iCell ) {
        TBoundaryCell& cell = dynamic_cast<TBoundaryCell&>(*BCells[*iCell]);
	    fnct = cell.GetFunction();
	    switch( fnct) {
	    case TBoundaryCell::OUTPUT2 :
	    case TBoundaryCell::OUTPUT3 :
	    case TBoundaryCell::BIDIR :
		pat[j] = '0';
		ccell = cell.GetCCell();
		if( ccell != -1 ) {
		    disval = cell.GetDisVal();                                                               
		    pat[iSig->second->GetCellIndex(ccell)] = ((disval=='0') ? '1' : '0');
		    //pat[iSig->second->GetCellIndex(BCells.size() - ccell - 1)] = ((disval=='0') ? '1' : '0');
		}
		break;
	    case TBoundaryCell::INPUT :
		/*safe = dynamic_cast<TBoundaryCell&>((*this)[*iCell]).GetSafe();
		iSig->second->LowOut[j] = ((safe=='X') ? '0' : safe);*/
		pat[j] = 'X';
		ccell = cell.GetCCell();
		if( ccell != -1 ) {
		    disval = cell.GetDisVal();                                          
		    pat[iSig->second->GetCellIndex(ccell)] = disval;
		    //pat[iSig->second->GetCellIndex(BCells.size() - ccell - 1)] = disval;
		}
		break;
	    case TBoundaryCell::CONTROL :
	    case TBoundaryCell::CONTROLR:
	    case TBoundaryCell::INTERNAL:
		break;
	    default:
		throw TException("TBoundaryRegister: not supported type of cell\n");
	    }
	    j++;
	  }

      iSig->second->AddState(TSignal::LOW_OUT, pat);
      //cout << iSig->second->GetName() << " low out " << pat << endl;
    }

    SetSafeValues();
}



void TBoundaryRegister::Describe(ostream& ost)
{
    TElement::Describe(ost);

    ost << string(DescribeOffset,' ')
         << "  Signals:" <<  Signals.size() <<  '\n';

    for(TSignals::iterator iSig=Signals.begin(); iSig!=Signals.end(); ++iSig ) {
	  ost <<  string(DescribeOffset,' ') << iSig->first << ": ";
      iSig->second->Describe(ost);
      ost << "GetStateSeq(btREC) = " << iSig->second->GetStateSeq() << '\n'; 
      ost << "GetState(btREC) = " << iSig->second->GetState() << '\n';
      ost << "GetStateSeq(btSEND) = " << iSig->second->GetStateSeq(btSEND) << '\n';  
      ost << "GetState(btSEND) = " << iSig->second->GetState(btSEND) << '\n';
    };

    ost << string(DescribeOffset,' ')
        << "  PinMap:" <<  PinMap.size() <<  '\n';

    for(TPinMap::iterator iPin = PinMap.begin(); iPin != PinMap.end(); ++iPin)
	  ost <<  string(DescribeOffset+5,' ') << iPin->first << ": "
          << iPin->second->GetName() << '\n';

    TRegister::Describe(ost);
}
