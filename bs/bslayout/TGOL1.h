#ifndef TGOL1_HH
#define TGOL1_HH

#include <typeinfo>
#include <stdio.h>
#include "TChip.h"

namespace bs {

class TGOL1 : public TChip {
public:
    TGOL1(const std::string& name );
    virtual ~TGOL1() {}
};



} // namespace bs


#endif




