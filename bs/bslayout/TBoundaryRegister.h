#ifndef BOUNDARY_REGISTER
#define BOUNDARY_REGISTER


#include "TRegister.h"

namespace bs {


class TSignal : public TRegisterFragment {
public: 
  static const TElementState HIGH_IN ;
  static const TElementState HIGH_OUT;
  static const TElementState LOW_IN  ;
  static const TElementState LOW_OUT ;

  TSignal(const std::string& name, TRegister& parent)
      : TRegisterFragment(name, parent) {}
  virtual ~TSignal() {}
};

typedef TSignal TPin;

                
class TBoundaryCell {
public:
    enum TCellType { BC_1, BC_4 };
    enum TFunction { INPUT, CLOCK, OUTPUT2, OUTPUT3, CONTROL,
                    CONTROLR, INTERNAL, BIDIR, OBSERVE_ONLY };
    enum TDisResult { Z, WEAK0, WEAK1, PULL0, PULL1 };   
private:
    // make copy constructor private - it should not be used
    TBoundaryCell(const TBoundaryCell& cell);
protected:
    const int Num;
    const TCellType Type;
    const std::string Port;
    const TFunction Function;
    const char Safe;
    const int CCell;
    const char DisVal;
    const TDisResult Rslt;
public:  
    TBoundaryCell(int num, TCellType type, const std::string& port,
                   TFunction function, char safe, int ccell=-1,
                   char disval=0, TDisResult rslt=Z )
      : Num(num), Type(type), Port(port), Function(function),
        Safe(safe), CCell(ccell), DisVal(disval), Rslt(rslt) {}

    int       GetNum()      { return Num; }
    TCellType GetType()     { return Type; }
    std::string GetPort()   { return Port; }
    TFunction GetFunction() { return Function; }
    char      GetSafe()     { return Safe; }
    int       GetCCell()    { return CCell; }
    char      GetDisVal()   { return DisVal; }
    TDisResult GetRslt()    { return Rslt; }
};


class TBoundaryRegister : public TRegister {
private:

    class TCompareCells {
    public:
      bool operator() (TBoundaryCell* x, TBoundaryCell* y)
        {
          return x->GetNum() < y->GetNum();
          //return x->GetNum() > y->GetNum();
        }
    };

    typedef std::vector<TBoundaryCell*> TBCells;
    TBCells BCells;

    typedef std::map<std::string, TSignal*> TSignals;
    TSignals Signals;

    //typedef std::map<std::string, std::string> TPinMap;
    typedef std::map<std::string, TSignal*> TPinMap;
    TPinMap PinMap;

    void SetSafeValues();

public:
    TBoundaryRegister(int length)
      : TRegister(BOUNDARY_REG_NAME, length) {}

    virtual ~TBoundaryRegister();

    void AddCell(TBoundaryCell& cell)
      {
        BCells.push_back(&cell);

        std::string port = cell.GetPort();
        if (port != "*")  {
            TSignals::iterator iSig = Signals.find(port);
            if (iSig == Signals.end())
              iSig = Signals.insert(
                       TSignals::value_type(port, new TSignal(port, *this) )).first;
            //iSig->second->AddCell(BCells.size() - 1);
            //iSig->second->AddCell((*iCell)->GetNum());
        }
      }

    void AddPinMapping(const std::string& port_name, const std::string& pin_name)
      {
        TSignals::iterator iSig = Signals.find(port_name);
        if (iSig != Signals.end())
          PinMap.insert( TPinMap::value_type(pin_name, iSig->second) );
      }

    void ScanBoundaryCells();

    //virtual void SetState(TElementState st, TBufferType bt=btSEND  );

    //virtual TElementState GetState(TBufferType bt=btREC );

    virtual void Describe(std::ostream& ost);

    TSignal& Signal(const std::string& name)
      {
        TSignals::iterator iSig = Signals.find(name);
        if (iSig == Signals.end())
          throw ENonExistentKey(name, "");

        return *iSig->second;
      }    

    TSignal& Pin(const std::string& name)
      {      
        TPinMap::iterator iPin = PinMap.find(name);
        if (iPin == PinMap.end())
          throw ENonExistentKey(name, "");

        return *iPin->second;
      }
};


} // namespace bs




#endif 
