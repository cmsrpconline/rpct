#include "TBoard.h"
                    
using namespace bs;


 
TScanVector TBoard::GenerateIRSequence(TBufferType bt)
{
    TScanVector seq;
    seq.reserve(GetIRLength());
    //for(reverse_iterator iter = rbegin(); iter != rend(); ++iter)
    //  (*iter)->GenerateIRSequence( seq );
    for(iterator iter = begin(); iter != end(); ++iter)
      seq += (*iter)->GenerateIRSequence(bt);

    return seq;
}


TScanVector TBoard::GenerateDRSequence(TBufferType bt)
{
    TScanVector seq;
    seq.reserve(GetDRLength());
    //for(reverse_iterator iter = rbegin(); iter != rend(); ++iter)
    //  (*iter)->GenerateDRSequence( seq );
    for(iterator iter = begin(); iter != end(); ++iter)
      seq += (*iter)->GenerateDRSequence(bt);

    return seq;
}




void TBoard::InterpreteIRSequence(TScanVector& seq, TBufferType bt)
{
    int offset=0;
    /*for(reverse_iterator iter = rbegin(); iter != rend(); ++iter) {
      (*iter)->InterpreteIRSequence(
             TScanVector(seq, offset, (*iter)->GetIRLength() )  );
      offset += (*iter)->GetIRLength();
    } */

    for(iterator iter = begin(); iter != end(); ++iter) {
      TScanVector sv(seq, offset, (*iter)->GetIRLength());
      (*iter)->InterpreteIRSequence(sv, bt);
      offset += (*iter)->GetIRLength();
    }

}



void TBoard::InterpreteDRSequence(TScanVector& seq, TBufferType bt)
{
    int offset=0;
    /*for(reverse_iterator iter = rbegin(); iter != rend(); ++iter) {
	  (*iter)->InterpreteDRSequence(
	       TScanVector(seq, offset, (*iter)->GetDRLength() )  );
	  offset += (*iter)->GetDRLength();
    } */
    for(iterator iter = begin(); iter != end(); ++iter) {
      TScanVector sv(seq, offset, (*iter)->GetDRLength());
	  (*iter)->InterpreteDRSequence(sv, bt);
	  offset += (*iter)->GetDRLength();
    }
}



int TBoard::GetIRLength()
{
    int len=0;
    for(iterator iter = begin(); iter != end(); ++iter)
	  len += (*iter)->GetIRLength();
    return len;
}



int TBoard::GetDRLength()
{
    int len=0;
    for(iterator iter = begin(); iter != end(); ++iter)
	  len += (*iter)->GetDRLength();
    return len;
}
