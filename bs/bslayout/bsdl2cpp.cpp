#include "bsdl2cpp.h"
#include <algorithm>

  
using namespace bs;

const int line_width=150;
void TBsdlParser::PinMapParsing()
{
 string line;
 ostringstream ostr;
   char chline[line_width];
   int beg, end, cur, wbeg;
   file.seekg(0);
   while(file.getline(chline,line_width))   //finding  "PIN_MAP_STRING"
     {
      cur_line++;         //empty lines are not counted
      line=chline;
      RemoveComments(line);
      if(line.find("PIN_MAP_STRING")!= -1)
       {
        if(Log!= NULL)
          (*Log)<<line<<endl;
        break;
       }
     }
   cur = line.find(":=");  //cur = current line position
   if(cur == -1)
     {
        ostr<<cur_line;
        throw EPinMapParsing(ostr.str() + ": ':=' not found");
     }

   TPinMap pin_map;

   bool last_line = false;
   while(!last_line)               //parsing one line
   {
     cur=line.find('\"',cur);                           //open "
     if(cur!= -1)
       {
         if(line.find('\"',cur+1)==-1)
           {
             ostr<<cur_line;
             throw EPinMapParsing(ostr.str() + ": '\"' not found");
           }
         while(1)
         {                                     //find structure SIG : PIN
           cur = line.find_first_not_of (" ", cur + 1);
           wbeg = cur;
           cur = line.find_first_of (" :", cur+1);
           pin_map.sig.clear();
           pin_map.sig = line.substr( wbeg, cur - wbeg);
           if(Log!= NULL)
             (*Log)<<pin_map.sig<<":";
           cur = line.find_first_not_of(" ", cur);
           if(line[cur]!=':')                         //finding :
             {
                ostr<<cur_line;
                throw EPinMapParsing(ostr.str() + ": ':' not found");
             }

           cur = line.find_first_not_of (" ", cur+1);
            //<<<<<<<<<<<<<<<<<<<<<<<<if for one signal thery is more then one pin, only first is taken 
           if(line[cur]=='(')
             {
               cur = line.find_first_not_of (" ", cur+1);
               wbeg = cur;                                        //finding pin
               cur = line.find_first_of (" ,", cur+1);
               pin_map.pin.clear();
               pin_map.pin = line.substr( wbeg, cur - wbeg);
               if(Log!= NULL)
                 (*Log)<<pin_map.pin<<endl;
               //cur = line.find_first_not_of (" ", cur+1);
               while(1)
                 {
                   cur=line.find( ')');
                   if(cur !=-1 )
                     {
                      cur++;
                      cur = line.find_first_not_of (" ", cur);
                      break;
                     }
                   file.getline(chline,line_width);
                   line = chline;
                   RemoveComments(line);
                   cur_line++;
                 }
             }
           else
             {
               wbeg = cur;                                        //finding pin
               cur = line.find_first_of (" ,\"", cur+1);
               pin_map.pin.clear();
               pin_map.pin = line.substr( wbeg, cur - wbeg);
               if(Log!= NULL)
                 (*Log)<<pin_map.pin<<endl;
               cur = line.find_first_not_of (" ", cur);
             }

           if(pin_map.sig=="GND" || pin_map.sig=="VCC" )
            {                                 //<<<<<<<<<<<<<<<<<<sciping pins "GND and VCC
             if(Log!= NULL)
               (*Log)<<"pin "<<pin_map.sig<<" sciped!!!!!!!!!!!!!!!!!!!!!!!"<<endl;
            }
           else
            PinMapVec.push_back(pin_map);

           if(line[cur]=='\"')    //last line [";] end of main loop
             {
               cur = line.find_first_not_of (" ", cur+1);
               if(line[cur]=='\;')
                {
                  last_line= true;
                  break;
                }
                else
                {
                  ostr<<cur_line;
                  throw EPinMapParsing(ostr.str() + ": ';' not found");
                }
             }
           if(line[cur]!='\,')                                 //finding ,
             {
                ostr<<cur_line;
                throw EPinMapParsing(ostr.str() + ": ',' not found");
             }
           cur = line.find_first_not_of (" ", cur+1);
           if(line[cur]=='\"')    //closing ",
             {
               cur = line.find_first_not_of (" ", cur+1);
               if(line[cur]!='&')                                   //finding &
                 {
                   ostr<<cur_line;
                   throw EPinMapParsing(ostr.str() + ": '&' not found");
                 }
              break;                          //end of inner loop
             }
         }  //end loop finding each  SIG : PIN structure, break after finding closing "
       }                                      //end of inner loop = end of line
    if(last_line)
      break;                                  //end of main loop (don't get next line)
    cur=0;
    if(Log!= NULL)
      (*Log)<<endl;                                      //geting new line
    file.getline(chline,line_width);
    cur_line++;
    line = chline;
    RemoveComments(line);
   }  //end main loop
}

//------------------------------------------------------------------------------

void TBsdlParser::InstructionParsing()
{
  string line;
  ostringstream ostr;
  char chline[line_width];
  int beg, end, cur, wbeg;
  file.seekg(0);
  while(file.getline(chline,line_width))   //finding  "PIN_MAP_STRING"
   {
    cur_line++;         //empty lines are not counted
    line=chline;
    RemoveComments(line);
    cur = line.find("INSTRUCTION_OPCODE");
    if(cur!= -1)
     {
      if(Log!= NULL)
        (*Log)<<line<<endl;
      break;
     }
   }

  TInstr instr;

  bool last_line = false;
  while(!last_line)               //parsing one line
   {
    cur=line.find('\"',cur);                           //open "
    if(cur!= -1)
     {
      if(line.find('\"',cur+1)==-1)
       {
        ostr<<cur_line;
        throw EInstructionParsing(ostr.str() + ": '\"' not found");
       }
      while(1)
       {                                     //find structure SIG : PIN
        cur = line.find_first_not_of (" ", cur + 1);
        wbeg = cur;
        cur = line.find_first_of (" (", cur+1);
        instr.name.clear();
        instr.name = line.substr( wbeg, cur - wbeg);
        cur = line.find_first_not_of(" ", cur);
        if(line[cur]!='(')
         {
          ostr<<cur_line;
          throw EInstructionParsing(ostr.str() + ": '(' not found");
         }
        cur = line.find_first_not_of(" ", cur+1);
        wbeg = cur;
        cur = line.find_first_of (" )", cur+1);
        instr.opcode.clear();
        instr.opcode = line.substr( wbeg, cur - wbeg);
        if(Log!= NULL)
          (*Log)<<instr.name<<" "<<instr.opcode<<endl;
        cur = line.find_first_not_of(" ", cur);
        if(line[cur]!=')')
         {
          ostr<<cur_line;
          throw EInstructionParsing(ostr.str() + ": ')' not found");
         }

        InstVec.push_back(instr);

        cur = line.find_first_not_of(" ", cur+1);
        //do tad
        if(line[cur]=='\"')    //last line [";] end of main loop
         {
          cur = line.find_first_not_of (" ", cur+1);
          if(line[cur]=='\;')
           {
            last_line= true;
            break;
           }
          else
           {
            ostr<<cur_line;
            throw EInstructionParsing(ostr.str() + ": ';' not found");
           }
         }
        if(line[cur]!='\,')                                 //finding ,
         {
          ostr<<cur_line;
          throw EInstructionParsing(ostr.str() + ": ',' not found");
         }
         cur = line.find_first_not_of (" ", cur+1);
         if(line[cur]=='\"')    //closing ",
          {
           cur = line.find_first_not_of (" ", cur+1);
           if(line[cur]!='&')                                   //finding &
            {
             ostr<<cur_line;
             throw EInstructionParsing(ostr.str() + ": '&' not found");
            }
           break;                          //end of inner loop
          }
       }  //end loop finding each  SIG : PIN structure, break after finding closing "
     }                                      //end of inner loop = end of line
    if(last_line)
      break;                                  //end of main loop (don't get next line)
    cur=0;
    if(Log!= NULL)
      (*Log)<<endl;                                      //geting new line
    file.getline(chline,line_width);
    cur_line++;
    line = chline;
    RemoveComments(line);
   } //end main loop

}

//------------------------------------------------------------------------------
                 //BOUNDARY_REGISTER
void TBsdlParser::BoundaryRegisterParsing()
{
  string line;
  ostringstream ostr;
  char chline[line_width];
  int beg, end, cur, wbeg;
  file.seekg(0);
  while(file.getline(chline,line_width))   //finding  "PIN_MAP_STRING"
   {
    cur_line++;         //empty lines are not counted
    line=chline;
    RemoveComments(line);
    cur = line.find("BOUNDARY_REGISTER");
    if(cur!= -1)
     {
      if(Log!= NULL)
        (*Log)<<line<<endl;
      break;
     }
   }

   TPinInfo pin_info;
                               //             optional
 //   name         port  function             safe ccell disval  rslt
  //  "8 (BC_1, IOE13, output3,                 X,  7,      1,    Z               )," &  BSDL
  // {"8",    "IOE13", TBoundaryCell::OUTPUT3, 'X', 7,     '1',   TBoundaryCell::Z }, MICHAL
  bool last_line = false;
  while(!last_line)               //parsing one line
   {
    cur=line.find('\"',cur);                           //open "
    if(cur!= -1)
     {
      if(line.find('\"',cur+1)==-1)
       {
        ostr<<cur_line;
        throw EBoundaryRegisterParsing(ostr.str() + ": '\"' not found");
       }
      while(1)
       {                                     //find structure SIG : PIN
        cur = line.find_first_not_of (" ", cur + 1);
        //stad
        wbeg = cur;
        cur = line.find_first_of (" (", cur+1);
        pin_info.name.clear();
        pin_info.name = line.substr( wbeg, cur - wbeg);
        cur = line.find_first_not_of (" ", cur);
        if(line[cur]!='(')
         {
          ostr<<cur_line;
          throw EBoundaryRegisterParsing(ostr.str() + ": '(' not found");
         }

        cur = line.find_first_not_of (" ", cur+1);
        wbeg = cur;
        cur = line.find_first_of (" ,", cur+1);
        pin_info.bc.clear();
        pin_info.bc = line.substr( wbeg, cur - wbeg);
        cur = line.find_first_not_of (" ", cur);
        if(line[cur]!=',')
         {
          ostr<<cur_line;
          throw EBoundaryRegisterParsing(ostr.str() + ": ',' not found");
         }

        cur = line.find_first_not_of (" ", cur+1);
        wbeg = cur;
        cur = line.find_first_of (" ,", cur+1);
        pin_info.port.clear();
        pin_info.port = line.substr( wbeg, cur - wbeg);
        cur = line.find_first_not_of (" ", cur);
        if(line[cur]!=',')
         {
          ostr<<cur_line;
          throw EBoundaryRegisterParsing(ostr.str() + ": ',' not found");
         }

        cur = line.find_first_not_of (" ", cur+1);
        wbeg = cur;
        cur = line.find_first_of (" ,", cur+1);
        pin_info.function.clear();
        pin_info.function = line.substr( wbeg, cur - wbeg);
        cur = line.find_first_not_of (" ", cur);
        if(line[cur]!=',')
         {
          ostr<<cur_line;
          throw EBoundaryRegisterParsing(ostr.str() + ": ',' not found");
         }

        cur = line.find_first_not_of (" ", cur+1);
        wbeg = cur;
        cur = line.find_first_of (" ,)", cur+1);
        pin_info.safe = line.substr( wbeg, cur - wbeg);
        if(pin_info.safe.size() != 1)
         {
          ostr<<cur_line;
          throw EBoundaryRegisterParsing(ostr.str() + ": safe is not one character");
         }
        cur = line.find_first_not_of (" ", cur);
        if( (line[cur]!=',') && (line[cur]!=')') )
         {
          ostr<<cur_line;
          throw EBoundaryRegisterParsing(ostr.str() + ": ',' not found");
         }
        if(line[cur]=='\)')    //defoult
         {
           pin_info.ccell = "-1";
           pin_info.disval = "0";
           pin_info.rslt ="Z";
         }
        else
         {
          cur = line.find_first_not_of (" ", cur+1);
          wbeg = cur;
          cur = line.find_first_of (" ,", cur+1);
          pin_info.ccell.clear();
          pin_info.ccell = line.substr( wbeg, cur - wbeg);
          cur = line.find_first_not_of (" ", cur);
          if( (line[cur]!=',') && (line[cur]!=')') )
           {
            ostr<<cur_line;
            throw EBoundaryRegisterParsing(ostr.str() + ": ',' not found");
           }

          cur = line.find_first_not_of (" ", cur+1);
          wbeg = cur;
          cur = line.find_first_of (" ,", cur+1);
          
          pin_info.disval = line.substr( wbeg, cur - wbeg);
          if(pin_info.disval.size() != 1)
           {
            ostr<<cur_line;
            throw EBoundaryRegisterParsing(ostr.str() + "disval is not one character");
           }
          cur = line.find_first_not_of (" ", cur);
          if( (line[cur]!=',') && (line[cur]!=')') )
           {
            ostr<<cur_line;
            throw EBoundaryRegisterParsing(ostr.str() + ": ',' not found");
           }

          cur = line.find_first_not_of (" ", cur+1);
          wbeg = cur;
          cur = line.find_first_of (" )", cur+1);
          pin_info.rslt.clear();
          pin_info.rslt = line.substr( wbeg, cur - wbeg);
          if(Log!= NULL)
            (*Log)<<pin_info.name<<" "<<pin_info.bc<<" "<<pin_info.port<<" "<<pin_info.function<<" "
                <<pin_info.safe<<" "<<pin_info.ccell<<" "<<pin_info.disval<<" "<<pin_info.rslt<<endl;
          cur = line.find_first_of (")", cur);
          if(cur == -1)
           {
            ostr<<cur_line;
            throw EBoundaryRegisterParsing(ostr.str() + ": ')' not found");
           }
         }

        PinInfoVec.push_back(pin_info);

        cur = line.find_first_not_of(" ", cur+1);
        if(line[cur]=='\"')    //last line [";] end of main loop
         {
          cur = line.find_first_not_of (" ", cur+1);
          if(line[cur]=='\;')
           {
            last_line= true;
            break;
           }
          else
           {
            ostr<<cur_line;
            throw EBoundaryRegisterParsing(ostr.str() + ": ';' not found");
           }
         }
        if(line[cur]!='\,')                                 //finding ,
         {
          ostr<<cur_line;
          throw EBoundaryRegisterParsing(ostr.str() + ": ',' not found");
         }
         cur = line.find_first_not_of (" ", cur+1);
         if(line[cur]=='\"')    //closing ",
          {
           cur = line.find_first_not_of (" ", cur+1);
           if(line[cur]!='&')                                   //finding &
            {
             ostr<<cur_line;
             throw EBoundaryRegisterParsing(ostr.str() + ": '&' not found");
            }
           break;                          //end of inner loop
          }
       }  //end loop finding each  SIG : PIN structure, break after finding closing "
     }                                      //end of inner loop = end of line
    if(last_line)
      break;                                  //end of main loop (don't get next line)
    cur=0;                                      //geting new line
    file.getline(chline,line_width);
    cur_line++;
    line = chline;
    RemoveComments(line);
   } //end main loop

}
//------------------------------------------------------------------------------
void TBsdlParser::NameParsing()
{
  string line;
  ostringstream ostr;
  char chline[line_width];
  int beg, end, cur, wbeg;
  file.seekg(0);
  while(file.getline(chline,line_width))   //finding  "PIN_MAP_STRING"
   {
    cur_line++;         //empty lines are not counted
    line=chline;
    RemoveComments(line);
    cur = line.find("entity");
    if(cur!= -1)
     {
      if(Log!= NULL)
        (*Log)<<line<<endl;
      break;
     }
   }

  cur = line.find_first_not_of (" ", cur + 6);
  wbeg = cur;
  cur = line.find_first_of (" ", cur+1);
  Name = line.substr( wbeg, cur - wbeg);
  if(Log!= NULL)
    (*Log)<<Name<<endl;

};
//------------------------------------------------------------------------------
void TBsdlParser::BoundaryLenghtParsing()
{
  string line;
  char chline[line_width];
  int beg, end, cur, wbeg;
  file.seekg(0);
  while(file.getline(chline,line_width))   //finding  "BOUNDARY_LENGTH"
 // while(file.getline(line))   //finding  "BOUNDARY_LENGTH"
   {
    cur_line++;         //empty lines are not counted
    line=chline;
    RemoveComments(line);
    cur = line.find("BOUNDARY_LENGTH");
    if(cur!= -1)
     {
      if(Log!= NULL)
        (*Log)<<line<<endl;
      break;
     }
   }
  cur = line.find("is", cur + 15);
  cur = line.find_first_not_of (" ", cur+2);
  wbeg = cur;
  cur = line.find_first_of (" ;", cur+1);
  Lenght = line.substr( wbeg, cur - wbeg);
  if(Log!= NULL)
    (*Log)<<Lenght<<endl;
};
//------------------------------------------------------------------------------
void TBsdlParser::InstructionCaptureParsing()
{
  string line;
  ostringstream ostr;
  char chline[line_width];
  int str_end, cur, wbeg;
  file.seekg(0);
  while(file.getline(chline,line_width))   //finding  "PIN_MAP_STRING"
   {
    cur_line++;         //empty lines are not counted
    line=chline;
    RemoveComments(line);
    cur = line.find("INSTRUCTION_CAPTURE");
    if(cur!= -1)
     {
      if(Log!= NULL)
        (*Log)<<line<<endl;
      break;
     }
   }
  while(1)               //parsing one line
   {
    cur=line.find('\"',cur);                           //open "
    if(cur!= -1)
     {
      str_end=line.find('\"',cur+1);
      if(str_end==-1)
       {
        ostr<<cur_line;
        throw EInstructionCaptureParsing(ostr.str() + ": '\"' not found");
       }
      cur = line.find_first_not_of (" ", cur + 1);
      wbeg = cur;
      cur = line.find_first_of ("\"", cur+1);
      InstCapture = InstCapture + line.substr( wbeg, cur - wbeg);

      cur = line.find_first_not_of(" ", cur);
      if(line.find(";",str_end)!= -1)
        break;
      else
       {
        if(line.find("&",str_end)== -1)
         {
          ostr<<cur_line;
          throw EInstructionCaptureParsing(ostr.str() + ": '\"' not found");
         }
       }
     }
    cur=0;
    if(Log!= NULL)
      (*Log)<<endl;                                      //geting new line
    file.getline(chline,line_width);
    cur_line++;
    line = chline;
    RemoveComments(line);
   }
  if(Log!= NULL)
    (*Log)<<InstCapture<<" "<<endl;
};
//------------------------------------------------------------------------------
void TBsdlParser::IdCodeParsing()
{
  string line;
  ostringstream ostr;
  char chline[line_width];
  int str_end, cur, wbeg;
  file.seekg(0);
  while(file.getline(chline,line_width))   //finding  "PIN_MAP_STRING"
   {
    cur_line++;         //empty lines are not counted
    line=chline;
    RemoveComments(line);
    cur = line.find("IDCODE_REGISTER");
    if(cur!= -1)
     {
      if(Log!= NULL)
        (*Log)<<line<<endl;
      break;
     }
   }
  
  while(1)               //parsing one line
   {
    cur=line.find('\"',cur);                           //open "
    if(cur!= -1)
     {
      str_end=line.find('\"',cur+1);
      if(str_end==-1)
       {
        ostr<<cur_line;
        throw EIdCodeParsing(ostr.str() + ": '\"' not found");
       }
      cur = line.find_first_not_of (" ", cur + 1);
      wbeg = cur;
      cur = line.find_first_of ("\"", cur+1);
      IdCode = IdCode + line.substr( wbeg, cur - wbeg);

      cur = line.find_first_not_of(" ", cur);
      if(line.find(";",str_end)!= -1)
        break;
      else
       {
        if(line.find("&",str_end)== -1)
         {
          ostr<<cur_line;
          throw EIdCodeParsing(ostr.str() + ": '\"' not found");
         }
       }
     }
    cur=0;
    if(Log!= NULL)
      (*Log)<<endl;                                      //geting new line
    file.getline(chline,line_width);
    cur_line++;
    line = chline;
    RemoveComments(line);
   }
  if(Log!= NULL)
    (*Log)<<IdCode<<" "<<endl;
};
//------------------------------------------------------------------------------

std::string TChipFactory::GetBSDLFileForChip(std::string chipName)
{
  if(InfoFileName.empty())
     throw EChipFactory ("chip info file not specified");
  ifstream infoFile;
  infoFile.open(InfoFileName.c_str());
  if (!infoFile)
     throw EChipFactory ("can't open the chip info file: " + string(InfoFileName) );

  std::string chip, fileName;
  while(infoFile>>chip)
   {
    infoFile>>fileName;
    if(chip == chipName)
      return fileName;
   }
  throw EChipFactory ("can't find file name for chip " + chipName);
}
//------------------------------------------------------------------------------
bs::TChip& TChipFactory::CreateChipFromFile(const char* BSDLFileName)
{
                        //&file_out
   TBsdlParser pars(BSDLFileName, Log);
   pars.Run();

   Chip = new bs::TChip(pars.Name, pars.InstCapture, atoi(pars.Lenght.c_str()), pars.IdCode );

                         //----------adding cells-----------------
   TBsdlParser::TPinInfoVec::iterator iPinInfo;
   for( iPinInfo=pars.PinInfoVec.begin(); iPinInfo < pars.PinInfoVec.end(); iPinInfo++ )
      Chip->AddCell( *new bs::TBoundaryCell( atoi(iPinInfo->name.c_str()),
                                             ConvertCellType( iPinInfo->bc ),
                                             iPinInfo->port,
                                             ConvertFunction(iPinInfo->function),
                                             iPinInfo->safe[0],        //safe is one character
                                             atoi(iPinInfo->ccell.c_str()),
                                             iPinInfo->disval[0],      //disval is one character
                                             ConvertDisResult(iPinInfo->rslt)     ) );

                        //---------adding pin map---------------
   TBsdlParser::TPinMapVec::iterator iPinMap;
   for( iPinMap=pars.PinMapVec.begin(); iPinMap < pars.PinMapVec.end(); iPinMap++ )
      Chip->AddPinMapping(iPinMap->sig, iPinMap->pin);
         //inside AddPinMapping function ther is chacked, if sighnal exists in boundary cells

                      //---------adding instructions---------------
   TBsdlParser::TInstVec::iterator iInstr;
   for( iInstr=pars.InstVec.begin(); iInstr < pars.InstVec.end(); iInstr++ )
      Chip->AddInstruction( *new bs::TInstruction(iInstr->name,
                                                  ConvertRegName(iInstr->name),
                                                  iInstr->opcode) );

   return *Chip;
}
