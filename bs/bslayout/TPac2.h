#ifndef TPAC2_HH
#define TPAC2_HH

#include <typeinfo>
#include <stdio.h>
#include "TChip.h"

namespace bs {


class TPac2 : public TChip {
public:
    TPac2(const std::string& name);
    virtual ~TPac2() {}
};

} // namespace bs


#endif




