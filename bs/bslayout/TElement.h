#ifndef TELEMENT_HH
#define TELEMENT_HH

#include "../TScanVector.h"
#include <string>
#include <iostream>
#include <typeinfo>

namespace bs {

class IElement {
public:        
    enum TBufferType { btSEND, btREC };
    virtual std::string GetName() = 0;    
    virtual ~IElement() {} 

    //virtual void GenerateIRSequence( TScanVector& seq ) = 0;

    //virtual void GenerateDRSequence( TScanVector& seq ) = 0;

    //virtual void InterpreteIRSequence( TScanVector& seq ) = 0;

    //virtual void InterpreteDRSequence( TScanVector& seq ) = 0;

    //virtual int GetIRLength() = 0;

    //virtual int GetDRLength() = 0;

    //virtual void SetState(TElementState, TBufferType bt=SEND) = 0;

    //virtual TElementState GetState(TBufferType bt=REC) = 0;

    //virtual void SetStateSeq(TScanVector&, TBufferType bt=btSEND) = 0;

    //virtual TScanVector GetStateSeq(TBufferType bt=btREC) = 0;
};


class TElement: virtual public IElement {
private:
    // make copy constructor private - it should not be used
    TElement(const TElement&);
protected:           
    std::string Name;
    static int DescribeOffset;
    static const int DescribeOffsetDelta;
public:
    //TElement();

    TElement(const std::string& name): Name(name) {}

    virtual ~TElement() {}

    virtual std::string GetName()
      { return Name; }

    virtual void Describe(std::ostream& ost)
      {
          ost << std::string(DescribeOffset,' ') << typeid(*this).name()
               <<": '" << GetName() << "':\n";
      }
};




} // namespace bs

#endif
