#include "TPac.h"
#include "TBoundaryRegister.h"

using namespace bs; 
using namespace std;


TPac::TPac(const std::string& name )
    : TChip(name)
{
    int i,j;

    // instrukcje
    // trzeba jeszcze dodac capture values
    const string inst_tab [16][3] = {
	{ "EXTEST",   TRegister::BOUNDARY_REG_NAME,    "0000" },
	{ "IDCODE",   TRegister::IDCODE_REG_NAME,      "1000" },
	{ "SAMPLE",   TRegister::BOUNDARY_REG_NAME,    "0100" },
	{ "INTEST",   TRegister::BOUNDARY_REG_NAME,    "1100" },
	{ "RUNBIST",  TRegister::BYPASS_REG_NAME,      "0010" }, // nie ma
	{ "USERCODE", TRegister::BYPASS_REG_NAME,      "1010" }, // nie ma
	{ "ES2SPARE", TRegister::BYPASS_REG_NAME,      "0110" }, // nie ma
	{ "USER_7",   "DR2",                "1110" },
	{ "USER_8",   "DR3",                "0001" },
	{ "USER_9",   "DR4",                "1001" },
	{ "USER_A",   "DR5",                "0101" },
	{ "USER_B",   "DR1",                "1101" },
	{ "USER_C",   TRegister::BYPASS_REG_NAME,      "0011" },
	{ "USER_D",   TRegister::BYPASS_REG_NAME,      "1011" },
	{ "USER_E",   TRegister::BYPASS_REG_NAME,      "0111" },
	{ "BYPASS",   TRegister::BYPASS_REG_NAME,      "1111" }
    };
    TInstruction * inst;
    for( i=0; i<16; i++ ) {
	inst = new TInstruction( inst_tab[i][0],inst_tab[i][1] );
	inst->AddOpCode(inst_tab[i][2]);
	AddInstruction( *inst );
    }
    

    // rejestry
    TRegister* reg;

    // boundary register
    
    struct pin_info {
	char* name;
	char* port;
	TBoundaryCell::TFunction function;
	char safe;
	int ccell;    //-1 jak nie ma
	char disval;
	TBoundaryCell::TDisResult rslt;
    };

    pin_info  b_reg_tab[]= {
    {"codeout6", "codeout6",  TBoundaryCell::OUTPUT2,   '0', -1, '0', TBoundaryCell::Z },
    {"codeout5", "codeout5",  TBoundaryCell::OUTPUT2,   '0', -1, '0', TBoundaryCell::Z },
    {"codeout4", "codeout4",  TBoundaryCell::OUTPUT2,   '0', -1, '0', TBoundaryCell::Z },
    {"codeout3", "codeout3",  TBoundaryCell::OUTPUT2,   '0', -1, '0', TBoundaryCell::Z },
    {"codeout2", "codeout2",  TBoundaryCell::OUTPUT2,   '0', -1, '0', TBoundaryCell::Z },
    {"codeout1", "codeout1",  TBoundaryCell::OUTPUT2,   '0', -1, '0', TBoundaryCell::Z },
    {"codeout0", "codeout0",  TBoundaryCell::OUTPUT2,   '0', -1, '0', TBoundaryCell::Z },

    {"codein6",  "codein6",   TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"codein5",  "codein5",   TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"codein4",  "codein4",   TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"codein3",  "codein3",   TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"codein2",  "codein2",   TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"codein1",  "codein1",   TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"codein0",  "codein0",   TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },

    {"ms1.1",    "ms1.1",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms1.2",    "ms1.2",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms1.3",    "ms1.3",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms1.4",    "ms1.4",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms1.5",    "ms1.5",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms1.6",    "ms1.6",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms1.7",    "ms1.7",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms1.8",    "ms1.8",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms1.9",    "ms1.9",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms1.10",   "ms1.10",    TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms1.11",   "ms1.11",    TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms1.12",   "ms1.12",    TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms1.13",   "ms1.13",    TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms1.14",   "ms1.14",    TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms1.15",   "ms1.15",    TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms1.16",   "ms1.16",    TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms1.17",   "ms1.17",    TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms1.18.",  "ms1.18",    TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },

    {"ms2.1",    "ms2.1",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms2.2",    "ms2.2",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms2.3",    "ms2.3",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms2.4",    "ms2.4",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },

    {"ms3.1",    "ms3.1",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms3.2",    "ms3.2",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms3.3",    "ms3.3",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms3.4",    "ms3.4",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms3.5",    "ms3.5",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms3.6",    "ms3.6",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms3.7",    "ms3.7",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms3.8",    "ms3.8",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms3.9",    "ms3.9",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms3.10",   "ms3.10",    TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms3.11",   "ms3.11",    TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms3.12",   "ms3.12",    TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms3.13",   "ms3.13",    TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms3.14",   "ms3.14",    TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },

    {"ms4.1",    "ms4.1",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms4.2",    "ms4.2",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms4.3",    "ms4.3",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms4.4",    "ms4.4",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms4.5",    "ms4.5",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms4.6",    "ms4.6",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms4.7",    "ms4.7",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms4.8",    "ms4.8",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms4.9",    "ms4.9",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms4.10",   "ms4.10",    TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms4.11",   "ms4.11",    TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms4.12",   "ms4.12",    TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms4.13",   "ms4.13",    TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms4.14",   "ms4.14",    TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms4.15",   "ms4.15",    TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms4.16",   "ms4.16",    TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms4.17",   "ms4.17",    TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
    {"ms4.18",   "ms4.18",    TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z }
    };
    reg = new TBoundaryRegister((sizeof(b_reg_tab)/sizeof(pin_info)),
				(sizeof(b_reg_tab)/sizeof(pin_info)));
    for( i=0; i<(sizeof(b_reg_tab)/sizeof(pin_info)); i++ )
	reg->AddElement( *new TBoundaryCell( b_reg_tab[i].name, 
					     67-i-1, 
					     TBoundaryCell::BC_1, 
					     b_reg_tab[i].port, 
					     b_reg_tab[i].function, 
					     b_reg_tab[i].safe, 
					     b_reg_tab[i].ccell, 
					     b_reg_tab[i].disval, 
					     b_reg_tab[i].rslt       ) );
    ((TBoundaryRegister*)reg)->ScanBoundaryCells();
    AddRegister( *reg );


    // idcode register
    reg = new TRegister(TRegister::IDCODE_REG_NAME);
    reg->AddElement( *new TComplexRegisterElement(TRegister::IDCODE_REG_NAME,32) );
    AddRegister( *reg );

    struct reg_el_info {
	char* name;
	int len; 
    };

    // rej 1
    reg_el_info dr1_tab[] = { "MMS1",18,
			      "MMS2",4,
			      "MMS3",14,
			      "MMS4",18,
			      "FF1",1,
			      "FF2",1,
			      "FF3",1,
			      "F6",1,
			      "M",7,
			      "F5",1,
			      "F4",1,
			      "F3",1,
			      "F2",1    };
    reg = new TRegister("DR1");

    for( i=0; i<(sizeof(dr1_tab)/sizeof(reg_el_info)); i++ ) {
	reg->AddElement( 
			*new TComplexRegisterElement( dr1_tab[i].name,dr1_tab[i].len ) 
			);
    }
    AddRegister(*reg);

    // rej 2
    int pat, or;
    char buffer[16];
    TRegister *dr2, *dr3, *dr4, *dr5;
    dr2 = new TRegister("DR2"); // = 542
    dr3 = new TRegister("DR3");
    dr4 = new TRegister("DR4");
    dr5 = new TRegister("DR5");

    for( pat=1; pat<=160; pat+=2) {
	sprintf( buffer,"P%.3d_MX4",pat);
	dr2->AddElement( *new TComplexRegisterElement(buffer, 4) );
	dr3->AddElement( *new TComplexRegisterElement(buffer, 4) );
	dr4->AddElement( *new TComplexRegisterElement(buffer, 4) );
	dr5->AddElement( *new TComplexRegisterElement(buffer, 4) );

	sprintf( buffer,"P%.3d_MX4",pat+1);
	dr2->AddElement( *new TComplexRegisterElement(buffer, 4) );
	dr3->AddElement( *new TComplexRegisterElement(buffer, 4) );
	dr4->AddElement( *new TComplexRegisterElement(buffer, 4) );
	dr5->AddElement( *new TComplexRegisterElement(buffer, 4) );

	sprintf( buffer,"P%.3d_MX3",pat);
	dr2->AddElement( *new TComplexRegisterElement(buffer, 4) );
	dr3->AddElement( *new TComplexRegisterElement(buffer, 4) );
	dr4->AddElement( *new TComplexRegisterElement(buffer, 4) );
	dr5->AddElement( *new TComplexRegisterElement(buffer, 4) );

	sprintf( buffer,"P%.3d_MX3",pat+1);
	dr2->AddElement( *new TComplexRegisterElement(buffer, 4) );
	dr3->AddElement( *new TComplexRegisterElement(buffer, 4) );
	dr4->AddElement( *new TComplexRegisterElement(buffer, 4) );
	dr5->AddElement( *new TComplexRegisterElement(buffer, 4) );

	sprintf( buffer,"P%.3d_MX1",pat);
	dr2->AddElement( *new TComplexRegisterElement(buffer, 4) );
	dr3->AddElement( *new TComplexRegisterElement(buffer, 4) );
	dr4->AddElement( *new TComplexRegisterElement(buffer, 4) );
	dr5->AddElement( *new TComplexRegisterElement(buffer, 4) );

	sprintf( buffer,"P%.3d_MX1",pat+1);
	dr2->AddElement( *new TComplexRegisterElement(buffer, 4) );
	dr3->AddElement( *new TComplexRegisterElement(buffer, 4) );
	dr4->AddElement( *new TComplexRegisterElement(buffer, 4) );
	dr5->AddElement( *new TComplexRegisterElement(buffer, 4) );
    }
    //pozytywne dmuxy
    or = 1;
    i=62;
    while( i>=32 ) {
	for( j=0; j<(16/or); j++ ) {
	    sprintf( buffer,"DO%d_%.2d",or, i);
	    dr2->AddElement( *new TComplexRegisterElement(buffer, 4) );
	    dr3->AddElement( *new TComplexRegisterElement(buffer, 4) );
	    dr4->AddElement( *new TComplexRegisterElement(buffer, 4) );
	    dr5->AddElement( *new TComplexRegisterElement(buffer, 4) );
	    i--;
	}
	or *= 2;
    }

    // negatywne dmuxy
    or = 1;
    i=31;
    while( i>=1 ) {
	for( j=0; j<(16/or); j++ ) {
	    sprintf( buffer,"DO%d_%.2d",or, i);
	    dr2->AddElement( *new TComplexRegisterElement(buffer, 4) );
	    dr3->AddElement( *new TComplexRegisterElement(buffer, 4) );
	    dr4->AddElement( *new TComplexRegisterElement(buffer, 4) );
	    dr5->AddElement( *new TComplexRegisterElement(buffer, 4) );
	    i--;
	}
	or *= 2;
    }



    AddRegister(*dr2);
    AddRegister(*dr3);
    AddRegister(*dr4);
    AddRegister(*dr5);


    
		     
}
