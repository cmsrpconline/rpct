#include "TPac2.h"
#include "TBoundaryRegister.h"

#include <vector>
#include <sstream>
#include <iomanip>

using namespace bs;
using namespace std;
      
TRegisterFragment::TCells CreateCells(int from, int to) // 0,4 daje 0123, 4,0 daje 3210
{
  TRegisterFragment::TCells cells;
  if (from < to)
    for (; from < to; from++)
      cells.push_back(from);
  else
    for (from--; from >= to; --from)
      cells.push_back(from);

  return cells;
}


class TMaskRegister: public TFragmentedRegister {    
public:
  TMaskRegister();
  virtual ~TMaskRegister() {}
};




TMaskRegister::TMaskRegister()
  : TFragmentedRegister("MASK", 241)
{
  int from=0, to=0;
  AddFragment( *new TRegisterFragment("ms1p_or4"    , *this, CreateCells(to+=14, from)) );  from=to;
  AddFragment( *new TRegisterFragment("ms2_or4"     , *this, CreateCells(to+=10, from)) );  from=to;
  AddFragment( *new TRegisterFragment("ms1_or4"     , *this, CreateCells(to+=12, from)) );  from=to;
  AddFragment( *new TRegisterFragment("ms2"         , *this, CreateCells(to+= 4, from)) );  from=to;
  AddFragment( *new TRegisterFragment("ms1"         , *this, CreateCells(to+=32, from)) );  from=to;
  AddFragment( *new TRegisterFragment("ms3"         , *this, CreateCells(to+=24, from)) );  from=to;
  AddFragment( *new TRegisterFragment("ms4"         , *this, CreateCells(from, to+=24)) );  from=to;
                              (*this)["ms2"].AddCells(       CreateCells(from, to+= 4)  );  from=to;
  AddFragment( *new TRegisterFragment("hpt_neg_sign", *this, CreateCells(to+= 1, from)) );  from=to;
  AddFragment( *new TRegisterFragment("hpt_pos_sign", *this, CreateCells(to+= 1, from)) );  from=to;
  AddFragment( *new TRegisterFragment("lpt_neg_sign", *this, CreateCells(to+= 1, from)) );  from=to;
  AddFragment( *new TRegisterFragment("lpt_pos_sign", *this, CreateCells(to+= 1, from)) );  from=to;
  AddFragment( *new TRegisterFragment("flag"        , *this, CreateCells(to+= 1, from)) );  from=to;
  AddFragment( *new TRegisterFragment("sin_lut4"    , *this, CreateCells(to+=80, from)) );  from=to;
  AddFragment( *new TRegisterFragment("sin_lut3"    , *this, CreateCells(to+=32, from)) );  from=to;

  if (from != Length)
    throw TException("TMaskRegister::TMaskRegister: from != 0");
}





class TBlockRegister: public TFragmentedRegister {
public:
  TBlockRegister(const std::string& name);
  virtual ~TBlockRegister() {}
};




TBlockRegister::TBlockRegister(const std::string& name)
  : TFragmentedRegister(name, 80*3*4 * 2 + 62*4)
{

  char buffer[16];
  int from=0, to=0;

  // dodaj A_01_msp1 ... A_80_msp4
  for(int pat=1; pat<=80; pat++) {

    sprintf( buffer,"A_%d_msp1",pat);
    //(*ireg)->AddElement( *new TComplexRegisterElement(buffer, 4) );
    AddFragment( *new TRegisterFragment(buffer, *this, CreateCells(to+=4, from)) );  from=to;

    sprintf( buffer,"A_%d_msp3",pat);
    //for(ireg = regs.begin(); ireg != regs.end(); ++ireg)
    //  (*ireg)->AddElement( *new TComplexRegisterElement(buffer, 4) );
    AddFragment( *new TRegisterFragment(buffer, *this, CreateCells(to+=4, from)) );  from=to;

    sprintf( buffer,"A_%d_msp4",pat);
    //for(ireg = regs.begin(); ireg != regs.end(); ++ireg)
    //  (*ireg)->AddElement( *new TComplexRegisterElement(buffer, 4) );
    AddFragment( *new TRegisterFragment(buffer, *this, CreateCells(to+=4, from)) );  from=to;
  }



  // dodaj A_01_msn1 ... A_80_msn4
  for(int pat=1; pat<=80; pat++) {

    sprintf( buffer,"A_%d_msn1",pat);
    //for(ireg = regs.begin(); ireg != regs.end(); ++ireg)
    //  (*ireg)->AddElement( *new TComplexRegisterElement(buffer, 4) );
    AddFragment( *new TRegisterFragment(buffer, *this, CreateCells(to+=4, from)) );  from=to;

    sprintf( buffer,"A_%d_msn3",pat);
    //for(ireg = regs.begin(); ireg != regs.end(); ++ireg)
    //  (*ireg)->AddElement( *new TComplexRegisterElement(buffer, 4) );
    AddFragment( *new TRegisterFragment(buffer, *this, CreateCells(to+=4, from)) );  from=to;

    sprintf( buffer,"A_%d_msn4",pat);
    //for(ireg = regs.begin(); ireg != regs.end(); ++ireg)
    //  (*ireg)->AddElement( *new TComplexRegisterElement(buffer, 4) );
    AddFragment( *new TRegisterFragment(buffer, *this, CreateCells(to+=4, from)) );  from=to;
  }


  //pozytywne dmuxy
  int or_= 16;
  int i=1;
  while( i<=31 ) {
    for(int j=0; j<(16/or_); j++ ) {
        sprintf( buffer,"A_%d", i);

        //for(ireg = regs.begin(); ireg != regs.end(); ++ireg)
        //  (*ireg)->AddElement( *new TComplexRegisterElement(buffer, 4) );
    AddFragment( *new TRegisterFragment(buffer, *this, CreateCells(to+=4, from)) );  from=to;
        i++;
    }
	  or_/= 2;
  }

  // negatywne dmuxy
  or_= 16;
  i=32;
  while( i<=62 ) {
    for(int j=0; j<(16/or_); j++ ) {
        sprintf( buffer,"A_%d", i);
        //for(ireg = regs.begin(); ireg != regs.end(); ++ireg)
        //  (*ireg)->AddElement( *new TComplexRegisterElement(buffer, 4) );
    AddFragment( *new TRegisterFragment(buffer, *this, CreateCells(to+=4, from)) );  from=to;
        i++;
    }
	  or_/= 2;
  }

  if (from != Length)
    throw TException("TBlockRegister::TBlockRegister: from != 0");
}




class TBlock9Register: public TFragmentedRegister {
public:
  TBlock9Register();
  virtual ~TBlock9Register() {}
};




TBlock9Register::TBlock9Register()
  : TFragmentedRegister("BLOCK9", 80*3*4 * 2 + 62*3)
{

  char buffer[16];
  int from=0, to=0;

  // dodaj A_01_msp1 ... A_80_msp4
  for(int pat=1; pat<=80; pat++) {

    //sprintf( buffer,"A_%.2d_msp1",pat);
    sprintf( buffer,"A_%d_msp1",pat);
    AddFragment( *new TRegisterFragment(buffer, *this, CreateCells(to+=4, from)) );  from=to;

    //sprintf( buffer,"A_%.2d_msp3",pat);
    sprintf( buffer,"A_%d_msp3",pat);
    AddFragment( *new TRegisterFragment(buffer, *this, CreateCells(to+=4, from)) );  from=to;

    //sprintf( buffer,"A_%.2d_msp4",pat);
    sprintf( buffer,"A_%d_msp4",pat);
    AddFragment( *new TRegisterFragment(buffer, *this, CreateCells(to+=4, from)) );  from=to;
  }



  // dodaj A_01_msn1 ... A_80_msn4
  for(int pat=1; pat<=80; pat++) {

    //sprintf( buffer,"A_%.2d_msn1",pat);
    sprintf( buffer,"A_%d_msn1",pat);
    AddFragment( *new TRegisterFragment(buffer, *this, CreateCells(to+=4, from)) );  from=to;

    //sprintf( buffer,"A_%.2d_msn3",pat);
    sprintf( buffer,"A_%d_msn3",pat);
    AddFragment( *new TRegisterFragment(buffer, *this, CreateCells(to+=4, from)) );  from=to;

    //sprintf( buffer,"A_%.2d_msn4",pat);
    sprintf( buffer,"A_%d_msn4",pat);
    AddFragment( *new TRegisterFragment(buffer, *this, CreateCells(to+=4, from)) );  from=to;
  }


  //pozytywne dmuxy
  int or_= 16;
  int i=1;
  while( i<=31 ) {
    for(int j=0; j<(16/or_); j++ ) {
        //sprintf( buffer,"A_%.2d_or%d", i, or);
        sprintf( buffer,"A_%d", i);

        AddFragment( *new TRegisterFragment(buffer, *this, CreateCells(to+=3, from)) );  from=to;

        i++;
    }
	  or_/= 2;
  }

  // negatywne dmuxy
  or_= 16;
  i=32;
  while( i<=62 ) {
    for(int j=0; j<(16/or_); j++ ) {
        //sprintf( buffer,"A_%.2d_or%d", i, or);
        sprintf( buffer,"A_%d", i);

        AddFragment( *new TRegisterFragment(buffer, *this, CreateCells(to+=3, from)) );  from=to;

        i++;
    }
	  or_/= 2;
  }


  if (from != Length)
    throw TException("TBlockRegister::TBlockRegister: from != 0");
}





struct pin_info {
  char* port;
  TBoundaryCell::TFunction function;
  char safe;
  int ccell;    //-1 jak nie ma
  char disval;
  TBoundaryCell::TDisResult rslt;
};

static pin_info  b_reg_tab[]= {
  {"code<7>",  TBoundaryCell::OUTPUT2,   '0', -1, '0', TBoundaryCell::Z },
  {"code<6>",  TBoundaryCell::OUTPUT2,   '0', -1, '0', TBoundaryCell::Z },
  {"code<5>",  TBoundaryCell::OUTPUT2,   '0', -1, '0', TBoundaryCell::Z },
  {"code<4>",  TBoundaryCell::OUTPUT2,   '0', -1, '0', TBoundaryCell::Z },
  {"code<3>",  TBoundaryCell::OUTPUT2,   '0', -1, '0', TBoundaryCell::Z },
  {"code<2>",  TBoundaryCell::OUTPUT2,   '0', -1, '0', TBoundaryCell::Z },
  {"code<1>",  TBoundaryCell::OUTPUT2,   '0', -1, '0', TBoundaryCell::Z },
  {"code<0>",  TBoundaryCell::OUTPUT2,   '0', -1, '0', TBoundaryCell::Z },

  {"ms2<4>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms2<3>",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },

  {"ms4<12>",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms4<11>",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms4<10>",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms4<9>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms4<8>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms4<7>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms4<6>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms4<5>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms4<4>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms4<3>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms4<2>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms4<1>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },

  {"ms3<1>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms3<2>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms3<3>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms3<4>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms3<5>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms3<6>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms3<7>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms3<8>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms3<9>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms3<10>",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms3<11>",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms3<12>",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },

  {"ms1<1>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms1<2>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms1<3>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms1<4>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms1<5>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms1<6>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms1<7>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms1<8>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms1<9>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms1<10>",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms1<11>",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms1<12>",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms1<13>",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms1<14>",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms1<15>",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms1<16>",     TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },

  {"ms2<1>",       TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms2<2>",      TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },


  {"ms1_or4<1>",  TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms1_or4<2>",  TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms1_or4<3>",  TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms1_or4<4>",  TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms1_or4<5>",  TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms1_or4<6>",  TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },

  {"ms2_or4<1>",  TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms2_or4<2>",  TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms2_or4<3>",  TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms2_or4<4>",  TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms2_or4<5>",  TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },

  {"ms1p_or4<1>",   TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms1p_or4<2>",   TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms1p_or4<3>",   TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms1p_or4<4>",   TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms1p_or4<5>",   TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms1p_or4<6>",   TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z },
  {"ms1p_or4<7>",   TBoundaryCell::INPUT,     '0', -1, '0', TBoundaryCell::Z }
};

TPac2::TPac2(const std::string& name)
    : TChip(name, "1000", sizeof(b_reg_tab)/sizeof(pin_info), "00000000000000000000000000000000")
{
  int i,j;

  // instrukcje
  // trzeba jeszcze dodac capture values
  string inst_tab [16][3] = {
    { "EXTEST",   TRegister::BOUNDARY_REG_NAME,    "0000" },
    { "IDCODE",   TRegister::IDCODE_REG_NAME,      "0001" },
    { "SAMPLE",   TRegister::BOUNDARY_REG_NAME,    "0010" },
    { "INTEST",   TRegister::BOUNDARY_REG_NAME,    "0011" },
    { "BLOCK1",   "BLOCK1",                        "0100" },
    { "BLOCK2",   "BLOCK2",                        "0101" },
    { "BLOCK3",   "BLOCK3",                        "0110" },
    { "BLOCK4",   "BLOCK4",                        "0111" },
    { "BLOCK5",   "BLOCK5",                        "1000" },
    { "BLOCK6",   "BLOCK6",                        "1001" },
    { "BLOCK7",   "BLOCK7",                        "1010" },
    { "BLOCK8",   "BLOCK8",                        "1011" },
    { "BLOCK9",   "BLOCK9",                        "1100" },
    { "MASK",     "MASK",                          "1101" },
    { "INTSCAN",  "INTSCAN",                       "1110" },
    { "BYPASS",   TRegister::BYPASS_REG_NAME,      "1111" }
  };
  for( i=0; i<16; i++ ) {
    AddInstruction( *new TInstruction( inst_tab[i][0],inst_tab[i][1], inst_tab[i][2] ) );
  }


  // rejestry
  TRegister* reg;

  /////////////////////////////
  // boundary register

  //int cellsnum = sizeof(b_reg_tab)/sizeof(pin_info);
  //TBoundaryRegister* breg = new TBoundaryRegister(cellsnum);

  for( i=sizeof(b_reg_tab)/sizeof(pin_info)-1; i>=0; i-- )
    AddCell( *new TBoundaryCell(
                               i,
                               TBoundaryCell::BC_1,
                               string(b_reg_tab[i].port),
                               b_reg_tab[i].function,
                               b_reg_tab[i].safe,
                               b_reg_tab[i].ccell,
                               b_reg_tab[i].disval,
                               b_reg_tab[i].rslt       ) );

  //breg->ScanBoundaryCells();
     
  /// pin mapping
  struct pin_map_el {
    char* pin;
    char* sig;
  };         

  pin_map_el pin_map[] = {
    {"C3",  "ms1p_or4<5>"},
    //{"B2",  "vdd"},
    //{"B1",  "gnd"},
    {"D3",  "ms1p_or4<6>"},
    {"C2",  "ms2_or4<1>"},
    {"C1",  "ms2_or4<2>"},
    {"D2",  "ms2_or4<3>"},
    {"E3",  "ms2_or4<4>"},
    {"D1",  "ms2_or4<5>"},
    {"E2",  "ms1_or4<1>"},
    //{"E1",  "gnd"},
    //{"F3",  "vdd"},
    //{"F2",  "gnd"},
    //{"F1",  "vdd"},
    {"G2",  "ms1_or4<2>"},
    {"G3",  "ms1_or4<3>"},
    {"G1",  "ms1_or4<4>"},
    {"H1",  "ms1_or4<5>"},
    {"H2",  "ms1_or4<6>"},
    {"H3",  "ms1_or4<7>"},
    //{"J1",  "tdi"},
    //{"J2",  "vdd"},
    //{"K1",  "gnd"},
    //{"J3",  "tms"},
    //{"K2",  "tdo"},
    //{"L1",  "tck"},
    //{"M1",  "trst"},
    //{"K3",  "gnd"},
    //{"L2",  "vdd"},
    //{"N1",  "empty"},
    {"L3",  "code<7>"},
    {"M2",  "code<6>"},
    {"N2",  "code<5>"},
    {"L4",  "code<4>"},
    //{"M3",  "vdd"},
    //{"N3",  "gnd"},
    {"M4",  "code<3>"},
    {"L5",  "code<2>"},
    {"N4",  "code<1>"},
    {"M5",  "code<0>"},

    {"N5",  "ms2<4>"},
    //{"L6",  "vdd"},
    //{"M6",  "gnd"},
    {"N6",  "ms2<3>"},
    {"M7",  "ms4<12>"},
    {"L7",  "ms4<11>"},
    {"N7",  "ms4<10>"},
    {"N8",  "ms4<9>"},
    //{"M8",  "vdd"},
    //{"L8",  "gnd"},
    {"N9",  "ms4<8>"},
    {"M9",  "ms4<7>"},
    {"N10", "ms4<6>"},
    {"L9",  "ms4<5>"},
    {"M10",  "ms4<4>"},
    //{"N11",  "vdd"},
    //{"N12",  "gnd"},
    {"L10",  "ms4<3>"},
    {"M11",  "ms4<2>"},
    {"N13",  "ms4<1>"},
    //{"L11",  "empty"},
    //{"M12",  "empty"},
    //{"M13",  "empty"},
    //{"K11",  "vdd"},
    //{"L12",  "gnd"},
    //{"L13",  "vdd"},
    //{"K12",  "gnd"},
    {"J11",  "ms3<1>"},
    {"K13",  "ms3<2>"},
    {"J12",  "ms3<3>"},
    //{"J13",  "gnd"},
    //{"H11",  "vdd"},
    {"H12",  "ms3<4>"},
    {"H13",  "ms3<5>"},
    //{"G12",  "clock"},
    {"G11",  "ms3<6>"},
    {"G13",  "ms3<7>"},
    //{"F13",  "vdd"},
    //{"F12",  "gnd"},
    //{"F11",  "vdd"},

    //{"E13",  "gnd"},
    {"E12",  "ms3<8>"},
    {"D13",  "ms3<9>"},
    {"E11",  "ms3<10>"},
    {"D12",  "ms3<11>"},
    {"C13",  "ms3<12>"},
    //{"B13",  "gnd"},
    //{"D11",  "vdd"},
    //{"C12",  "empty"},
    //{"A13",  "empty"},
    {"C11",  "ms1<1>"},
    {"B12",  "ms1<2>"},
    {"A12",  "ms1<3>"},
    {"C10",  "ms1<4>"},
    {"B11",  "ms1<5>"},
    //{"A11",  "gnd"},
    //{"B10",  "vdd"},
    {"C9",   "ms1<6>"},
    {"A10",  "ms1<7>"},
    {"B9",  "ms1<8>"},
    {"A9",  "ms1<9>"},
    {"C8",  "ms1<10>"},
    //{"B8",  "gnd"},
    //{"A8",  "vdd"},
    {"B7",  "ms1<11>"},
    {"C7",  "ms1<12>"},
    {"A7",  "ms1<13>"},
    {"A6",  "ms1<14>"},
    {"B6",  "ms1<15>"},
    //{"C6",  "gnd"},
    //{"A5",  "vdd"},
    {"B5",  "ms1<16>"},
    {"A4",  "ms2<1>"},
    {"C5",  "ms2<2>"},
    {"B4",  "ms1p_or4<1>"},
    {"A3",  "ms1p_or4<2>"},
    //{"A2",  "gnd"},
    //{"C4",  "vdd"},
    {"B3",  "ms1p_or4<3>"},
    {"A1",  "ms1p_or4<4>"},

  };

  for( i=sizeof(pin_map)/sizeof(pin_map_el)-1; i>=0; i-- )
    AddPinMapping(pin_map[i].sig, pin_map[i].pin);

  AddRegister( *new TMaskRegister );
  AddRegister( *new TBlockRegister("BLOCK1") ); 
  AddRegister( *new TBlockRegister("BLOCK2") );
  AddRegister( *new TBlockRegister("BLOCK3") );
  AddRegister( *new TBlockRegister("BLOCK4") );
  AddRegister( *new TBlockRegister("BLOCK5") );
  AddRegister( *new TBlockRegister("BLOCK6") );
  AddRegister( *new TBlockRegister("BLOCK7") );
  AddRegister( *new TBlockRegister("BLOCK8") );
  AddRegister( *new TBlock9Register );            
  AddRegister( *new TRegister("INTSCAN", 62*9) );



  InitEnd();
}
