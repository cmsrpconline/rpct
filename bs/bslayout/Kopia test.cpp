#include <typeinfo>
#include <iostream.h>
#include "TBoard.h"
#include "TChip.h"
#include "TPac2.h"
#include "TAltera2.h"

#include <string>
#include <sstream>
#include <map>          
#include <time.h>

using namespace bs;
using namespace std;

/*char* getkey(int max)
{
  static char buf[5];
  memset(buf+1, 0, 4);
  buf[0] = 'A';
  itoa(rand()%max, buf+1, 10);
  return buf;
} */


int main()
{

 // const int ElemCount = 1000;
 // const int TestCount = 10000000;

  try {

  /*  TAssociativeArrayRef<string> Elements(ElemCount);
    for (int i=0; i<ElemCount; i++) {
      ostringstream ostr;
      ostr << "A" << i;
      string* str = new string(ostr.str());
      Elements.Add(str->c_str(), *str);
    };

    typedef map<const char*, string*> TMap;
    TMap Map;
    for (int i=0; i<ElemCount; i++) {
      ostringstream ostr;
      ostr << "A" << i;
      string* str = new string(ostr.str());
      Map.insert( TMap::value_type(str->c_str(), str) );
      //Elements.Add(str->c_str(), *str);
    };


    cout << "Preparing...";

    time_t t = time(NULL);
    for (int i=0; i<TestCount; i++) {
      //static ostringstream ostr; //("A000");
      //ostr.clear();
      //ostr << "A" << rand()%ElemCount;
      getkey(ElemCount);
    };
    cout << "Done: " << time(NULL) - t << endl;
    time_t t1 = time(NULL) - t;


    cout << "Start1...";

    t = time(NULL);
    for (int i=0; i<TestCount; i++) {
      //ostringstream ostr; //("A000");
      //ostr << "A" << rand()%ElemCount;
      Elements[getkey(ElemCount)];
    };


    cout << "Done: " << time(NULL) - t - t1 << endl;

    cout << "Start2...";

    t = time(NULL);
    for (int i=0; i<TestCount; i++) {
      //ostringstream ostr; //("A000");
      //ostr << "A" << rand()%ElemCount;
      Map.find(getkey(ElemCount));
    };

    cout << "Done: " << time(NULL) - t - t1 << endl;
                                     */

    TBoard board("board");

    //TPac2& pac2 = *new TPac2("pac2");
    //board.AddElement(pac2);

    //pac2.Describe(cout);

    TAltera2& alt = *new TAltera2("alt");
    board.AddElement(alt);
                           
    alt.Describe(cout);
    
    /*board["pac2"].  SetStateSeq(pac1|DR2|P001_MX1","1010");
    board.SetStateSeq("pac1|DR2|DO16_01","0101");
    board.SetState("pac1","USER_7");  */


    board.Chip("alt").Pin("E14").SetState(HIGH_IN);// SetState("alt|pin|U12","i1");
    board.Chip("alt").Pin("F4").SetState(HIGH_OUT);// board.SetState("alt|pin|T12","o1");
    board.Chip("alt").Pin("F1").SetState(LOW_IN);// board.SetState("alt|pin|U13","i0");
    board.Chip("alt").Pin("G3").SetState(LOW_OUT);// board.SetState("alt|pin|R12","o0");

    board.Chip("alt").SetState("SAMPLE");


    cout << "alt|sig|IOE14 = " << board.Chip("alt").Signal("IOE14").GetState(IElement::btSEND) << endl;
    cout << "alt|sig|IOF4 = " << board.Chip("alt").Signal("IOF4").GetState(IElement::btSEND) << endl;
    cout << "alt|sig|IOF1 = " << board.Chip("alt").Signal("IOF1").GetState(IElement::btSEND) << endl;
    cout << "alt|sig|IOG3 = " << board.Chip("alt").Signal("IOG3").GetState(IElement::btSEND) << endl;

   /* TScanVector seq;
    a.GetStateSeq( "pac2|BLOCK1|A_01_msp1", seq, SEND );
    cout << "pac2|BLOCK1|A_01_msp1 = " << seq << endl;

    seq.clear();
    a.GetStateSeq( "pac2|BLOCK1|A_80_msn4", seq, SEND );
    cout << "pac2|BLOCK1|A_80_msn4 = " << seq << endl;

    seq.clear();
    a.GetStateSeq( "pac2|BLOCK1|A_01_or16", seq, SEND );
    cout << "pac2|BLOCK1|A_01_or16 = " << seq << endl;

    seq.clear();
    a.GetStateSeq(  "pac2|BLOCK1|A_62_or1", seq, SEND );
    cout << "pac2|BLOCK1|A_62_or1  = " << seq << endl;*/


    TScanVector irseq;
    board.GenerateIRSequence( irseq );

    cout <<"ir="<<irseq<<endl;

    TScanVector drseq;

    board.GenerateDRSequence( drseq );
    cout <<"dr="<<drseq<<endl;

    board.InterpreteDRSequence( drseq );

    //board.Describe(cout);

  }
  catch( TException& e ) {
      cerr << "Blad: " << e.what() << endl;
  }
  char c;
  cin >> c;
  return 0;
}
