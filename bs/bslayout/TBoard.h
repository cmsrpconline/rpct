#ifndef TBOARD_HH
#define TBOARD_HH

#include "TComplexElement.h"
#include "TBoardElement.h"
#include "TChip.h"

namespace bs {

class TBoard : public TComplexElement<IBoardElement> {
private:
    // make copy constructor private - it should not be used
    TBoard(const TBoard& board);
public:
    TBoard(const std::string& name)
      :TComplexElement<IBoardElement>(name) {}

    virtual ~TBoard() {}

    virtual TScanVector GenerateIRSequence(TBufferType bt=btSEND);

    virtual TScanVector GenerateDRSequence(TBufferType bt=btSEND);

    virtual void InterpreteIRSequence(TScanVector& seq, TBufferType bt=btREC);

    virtual void InterpreteDRSequence(TScanVector& seq, TBufferType bt=btREC);

    virtual int GetIRLength();

    virtual int GetDRLength();

    TChip& Chip(const std::string& name)
      {
        return dynamic_cast<TChip&>((*this)[name]);
      }


  /*  virtual void
    SetState( TElementPath, TElementState, TBufferType bt=SEND );

    virtual TElementState
    GetState( TElementPath, TBufferType bt=REC  );

    virtual void
    SetStateSeq( TElementPath, TScanVector&, TBufferType bt=SEND  );

    virtual void
    GetStateSeq( TElementPath, TScanVector&, TBufferType bt=REC );*/
};

/*

inline
void
TBoard::SetState( TElementPath path, TElementState st, TBufferType bt )
{
    TComplexElement<TBoardElement>::SetState( path, st, bt );
}


inline
TElementState
TBoard::GetState( TElementPath path, TBufferType bt  )
{
    return TComplexElement<TBoardElement>::GetState( path, bt );
}


inline
void
TBoard::SetStateSeq( TElementPath path, TScanVector& seq, TBufferType bt  )
{
    TComplexElement<TBoardElement>::SetStateSeq( path, seq ,bt );
}
  

inline   
void
TBoard::GetStateSeq( TElementPath path, TScanVector& seq, TBufferType bt  )
{
    TComplexElement<TBoardElement>::GetStateSeq( path, seq, bt );
}
     */

} // namespace bs

#endif
