#ifndef TBOARDELEMENT_HH
#define TBOARDELEMENT_HH

#include "TElement.h"
#include "../TScanVector.h"
//#include "..\TElementPath.h"

namespace bs {

class IBoardElement : virtual public IElement  {
public:
  
    //virtual void GenerateIRSequence( TScanVector& seq ) = 0;

    //virtual void GenerateDRSequence( TScanVector& seq ) = 0;

    virtual TScanVector GenerateIRSequence(TBufferType bt = btSEND) = 0;

    virtual TScanVector GenerateDRSequence(TBufferType bt = btSEND) = 0;

    virtual void InterpreteIRSequence(TScanVector& seq, TBufferType bt = btREC) = 0;

    virtual void InterpreteDRSequence(TScanVector& seq, TBufferType bt = btREC) = 0;

    virtual int GetIRLength() = 0;

    virtual int GetDRLength() = 0;

    virtual void Describe(std::ostream& ost) = 0;
  /*
    //TBoardElement() {}

    TBoardElement(std::string& name ) : TElement(name) {}

    virtual
    ~TBoardElement() {}

    virtual void
    SetState( TElementPath, TElementState, TBufferType bt  ) = 0;

    virtual TElementState
    GetState( TElementPath, TBufferType bt  ) = 0;

    virtual void
    SetStateSeq( TElementPath, TScanVector&, TBufferType bt  ) = 0;

    virtual void
    GetStateSeq( TElementPath, TScanVector&, TBufferType bt  ) = 0;*/ 
};


} // namespace bs

#endif
