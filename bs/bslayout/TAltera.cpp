#include "TAltera.h"
#include "TBoundaryRegister.h"

using namespace bs;

TAltera::TAltera( char* name )
    : TChip( name, 3, 3 )
{
    int i,j;

    // instrukcje
    // trzeba jeszcze dodac capture values
    const std::string inst_tab [3][3] = {
	  { "EXTEST",  TRegister::BOUNDARY_REG_NAME, "000" },
	  { "SAMPLE",  TRegister::BOUNDARY_REG_NAME, "101" },
	  { "BYPASS",  TRegister::BYPASS_REG_NAME,   "111" }
    };
    TInstruction * inst;
    for( i=0; i<3; i++ ) {
	inst = new TInstruction( inst_tab[i][0],inst_tab[i][1] );
	inst->AddOpCode(inst_tab[i][2]);
	AddInstruction( *inst );
    }
    

    // rejestry
    TRegister* reg;


    // boundary register
    
    struct pin_info {
	char* name;
	char* port;
	TBoundaryCell::TFunction function;
	char safe;
	int ccell;    //-1 jak nie ma
	char disval;
	TBoundaryCell::TDisResult rslt;
    };

    pin_info  b_reg_tab[]= {
    {"0", "IOE(11)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"1", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    { "2", "IOE(11)",  TBoundaryCell::OUTPUT3, 'X',  1, '1', TBoundaryCell::Z },

    {"3", "IOE(12)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"4", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"5", "IOE(12)",  TBoundaryCell::OUTPUT3, 'X',  4, '1', TBoundaryCell::Z },

    {"6", "IOE(13)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"7", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"8", "IOE(13)",  TBoundaryCell::OUTPUT3, 'X',  7, '1', TBoundaryCell::Z },

    {"9", "IOE(14)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"10", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"11", "IOE(14)",  TBoundaryCell::OUTPUT3, 'X', 10, '1', TBoundaryCell::Z },

    {"12", "IOE(15)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"13", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"14", "IOE(15)",  TBoundaryCell::OUTPUT3, 'X', 13, '1', TBoundaryCell::Z },

    {"15", "IOE(16)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"16", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"17", "IOE(16)",  TBoundaryCell::OUTPUT3, 'X', 16, '1', TBoundaryCell::Z },

    {"18", "IOE(17)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"19", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"20", "IOE(17)",  TBoundaryCell::OUTPUT3, 'X', 19, '1', TBoundaryCell::Z },

    {"21", "IOE(18)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"22", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"23", "IOE(18)",  TBoundaryCell::OUTPUT3, 'X', 22, '1', TBoundaryCell::Z },

    {"24", "IOE(19)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"25", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"26", "IOE(19)",  TBoundaryCell::OUTPUT3, 'X', 25, '1', TBoundaryCell::Z },

    {"27", "IOE(20)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"28", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"29", "IOE(20)",  TBoundaryCell::OUTPUT3, 'X', 28, '1', TBoundaryCell::Z },

    {"30", "IOE(21)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"31", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"32", "IOE(21)",  TBoundaryCell::OUTPUT3, 'X', 31, '1', TBoundaryCell::Z },

    {"33", "DIN(2)",  TBoundaryCell::INPUT,    'X', -1, '0', TBoundaryCell::Z },
    {"34", "*",       TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
    {"35", "*",       TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

    {"36", "IOE(22)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"37", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"38", "IOE(22)",  TBoundaryCell::OUTPUT3, 'X', 37, '1', TBoundaryCell::Z },

    {"39", "IOE(23)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"40", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"41", "IOE(23)",  TBoundaryCell::OUTPUT3, 'X', 40, '1', TBoundaryCell::Z },

    {"42", "IOE(24)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"43", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"44", "IOE(24)",  TBoundaryCell::OUTPUT3, 'X', 43, '1', TBoundaryCell::Z },

    {"45", "IOE(25)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"46", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"47", "IOE(25)",  TBoundaryCell::OUTPUT3, 'X', 46, '1', TBoundaryCell::Z },

    {"48", "IOE(26)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"49", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"50", "IOE(26)",  TBoundaryCell::OUTPUT3, 'X', 49, '1', TBoundaryCell::Z },

    {"51", "IOE(27)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"52", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"53", "IOE(27)",  TBoundaryCell::OUTPUT3, 'X', 52, '1', TBoundaryCell::Z },

    {"54", "IOE(28)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"55", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"56", "IOE(28)",  TBoundaryCell::OUTPUT3, 'X', 55, '1', TBoundaryCell::Z },

    {"57", "IOE(29)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"58", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"59", "IOE(29)",  TBoundaryCell::OUTPUT3, 'X', 58, '1', TBoundaryCell::Z },

    {"60", "IOE(30)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"61", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"62", "IOE(30)",  TBoundaryCell::OUTPUT3, 'X', 61, '1', TBoundaryCell::Z },

    {"63", "MSEL1",  TBoundaryCell::INPUT,    'X', -1, '0', TBoundaryCell::Z },
    {"64", "*",      TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
    {"65", "*",      TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

    {"66", "IOE(31)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"67", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"68", "IOE(31)",  TBoundaryCell::OUTPUT3, 'X', 67, '1', TBoundaryCell::Z },

    {"69", "IOE(32)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"70", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"71", "IOE(32)",  TBoundaryCell::OUTPUT3, 'X', 70, '1', TBoundaryCell::Z },

    {"72", "IOE(33)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"73", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"74", "IOE(33)",  TBoundaryCell::OUTPUT3, 'X', 73, '1', TBoundaryCell::Z },

    {"75", "IOE(34)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"76", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"77", "IOE(34)",  TBoundaryCell::OUTPUT3, 'X', 76, '1', TBoundaryCell::Z },

    {"78", "IOE(35)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"79", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"80", "IOE(35)",  TBoundaryCell::OUTPUT3, 'X', 79, '1', TBoundaryCell::Z },

    {"81", "IOE(36)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"82", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"83", "IOE(36)",  TBoundaryCell::OUTPUT3, 'X', 82, '1', TBoundaryCell::Z },

    {"84", "IOE(37)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"85", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"86", "IOE(37)",  TBoundaryCell::OUTPUT3, 'X', 85, '1', TBoundaryCell::Z },

    {"87", "IOE(38)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"88", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"89", "IOE(38)",  TBoundaryCell::OUTPUT3, 'X', 88, '1', TBoundaryCell::Z },

    {"90", "IOE(39)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"91", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"92", "IOE(39)",  TBoundaryCell::OUTPUT3, 'X', 91, '1', TBoundaryCell::Z },

    {"93", "IOE(40)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"94", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"95", "IOE(40)",  TBoundaryCell::OUTPUT3, 'X', 94, '1', TBoundaryCell::Z },

    {"96", "IOE(41)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"97", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"98", "IOE(41)",  TBoundaryCell::OUTPUT3, 'X', 97, '1', TBoundaryCell::Z },

    {"99", "IOE(42)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"100", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"101", "IOE(42)",  TBoundaryCell::OUTPUT3, 'X',100, '1', TBoundaryCell::Z },

    {"102", "IOE(43)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"103", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"104", "IOE(43)",  TBoundaryCell::OUTPUT3, 'X',103, '1', TBoundaryCell::Z },

    {"105", "IOE(44)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"106", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"107", "IOE(44)",  TBoundaryCell::OUTPUT3, 'X',106, '1', TBoundaryCell::Z },

    {"108", "IOE(45)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"109", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"110", "IOE(45)",  TBoundaryCell::OUTPUT3, 'X',109, '1', TBoundaryCell::Z },

    {"111", "IOE(46)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"112", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"113", "IOE(46)",  TBoundaryCell::OUTPUT3, 'X',112, '1', TBoundaryCell::Z },

    {"114", "IOE(47)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"115", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"116", "IOE(47)",  TBoundaryCell::OUTPUT3, 'X',115, '1', TBoundaryCell::Z },

    {"117", "IOE(48)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"118", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"119", "IOE(48)",  TBoundaryCell::OUTPUT3, 'X',118, '1', TBoundaryCell::Z },

    {"120", "IOE(49)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"121", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"122", "IOE(49)",  TBoundaryCell::OUTPUT3, 'X',121, '1', TBoundaryCell::Z },

    {"123", "IOE(50)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"124", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"125", "IOE(50)",  TBoundaryCell::OUTPUT3, 'X',124, '1', TBoundaryCell::Z },

    {"126", "IOE(51)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"127", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"128", "IOE(51)",  TBoundaryCell::OUTPUT3, 'X',127, '1', TBoundaryCell::Z },

    {"129", "IOE(52)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"130", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"131", "IOE(52)",  TBoundaryCell::OUTPUT3, 'X',130, '1', TBoundaryCell::Z },

    {"132", "IOE(53)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"133", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"134", "IOE(53)",  TBoundaryCell::OUTPUT3, 'X',133, '1', TBoundaryCell::Z },

    {"135", "IOE(54)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"136", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"137", "IOE(54)",  TBoundaryCell::OUTPUT3, 'X',136, '1', TBoundaryCell::Z },

    {"138", "IOE(55)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"139", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"140", "IOE(55)",  TBoundaryCell::OUTPUT3, 'X',139, '1', TBoundaryCell::Z },

    {"141", "IOE(56)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"142", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"143", "IOE(56)",  TBoundaryCell::OUTPUT3, 'X',142, '1', TBoundaryCell::Z },

    {"144", "IOE(57)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"145", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"146", "IOE(57)",  TBoundaryCell::OUTPUT3, 'X',145, '1', TBoundaryCell::Z },

    {"147", "IOE(58)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"148", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"149", "IOE(58)",  TBoundaryCell::OUTPUT3, 'X',148, '1', TBoundaryCell::Z },

    {"150", "IOE(59)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"151", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"152", "IOE(59)",  TBoundaryCell::OUTPUT3, 'X',151, '1', TBoundaryCell::Z },

    {"153", "IOE(60)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"154", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"155", "IOE(60)",  TBoundaryCell::OUTPUT3, 'X',154, '1', TBoundaryCell::Z },

    {"156", "IOE(61)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"157", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"158", "IOE(61)",  TBoundaryCell::OUTPUT3, 'X',157, '1', TBoundaryCell::Z },

    {"159", "IOE(62)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"160", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"161", "IOE(62)",  TBoundaryCell::OUTPUT3, 'X',160, '1', TBoundaryCell::Z },

    {"162", "IOE(63)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"163", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"164", "IOE(63)",  TBoundaryCell::OUTPUT3, 'X',163, '1', TBoundaryCell::Z },

    {"165", "IOE(64)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"166", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"167", "IOE(64)",  TBoundaryCell::OUTPUT3, 'X',166, '1', TBoundaryCell::Z },

    {"168", "IOE(65)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"169", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"170", "IOE(65)",  TBoundaryCell::OUTPUT3, 'X',169, '1', TBoundaryCell::Z },

    {"171", "IOE(66)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"172", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"173", "IOE(66)",  TBoundaryCell::OUTPUT3, 'X',172, '1', TBoundaryCell::Z },

    {"174", "IOE(67)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"175", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"176", "IOE(67)",  TBoundaryCell::OUTPUT3, 'X',175, '1', TBoundaryCell::Z },

    {"177", "IOE(68)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"178", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"179", "IOE(68)",  TBoundaryCell::OUTPUT3, 'X',178, '1', TBoundaryCell::Z },

    {"180", "IOE(69)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"181", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"182", "IOE(69)",  TBoundaryCell::OUTPUT3, 'X',181, '1', TBoundaryCell::Z },

    {"183", "IOE(70)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"184", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"185", "IOE(70)",  TBoundaryCell::OUTPUT3, 'X',184, '1', TBoundaryCell::Z },

    {"186", "IOE(71)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"187", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"188", "IOE(71)",  TBoundaryCell::OUTPUT3, 'X',187, '1', TBoundaryCell::Z },

    {"189", "nCONFIG",  TBoundaryCell::INPUT,    'X', -1, '0', TBoundaryCell::Z },
    {"190", "*",        TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
    {"191", "*",        TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

    {"192", "*",        TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },  //nStatus
    {"193", "*",        TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
    {"194", "*",        TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

    {"195", "IOE(72)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"196", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"197", "IOE(72)",  TBoundaryCell::OUTPUT3, 'X',196, '1', TBoundaryCell::Z },

    {"198", "IOE(73)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"199", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"200", "IOE(73)",  TBoundaryCell::OUTPUT3, 'X',199, '1', TBoundaryCell::Z },

    {"201", "IOE(74)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"202", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"203", "IOE(74)",  TBoundaryCell::OUTPUT3, 'X',202, '1', TBoundaryCell::Z },

    {"204", "IOE(75)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"205", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"206", "IOE(75)",  TBoundaryCell::OUTPUT3, 'X',205, '1', TBoundaryCell::Z },

    {"207", "IOE(76)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"208", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"209", "IOE(76)",  TBoundaryCell::OUTPUT3, 'X',208, '1', TBoundaryCell::Z },

    {"210", "IOE(77)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"211", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"212", "IOE(77)",  TBoundaryCell::OUTPUT3, 'X',211, '1', TBoundaryCell::Z },

    {"213", "IOE(78)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"214", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"215", "IOE(78)",  TBoundaryCell::OUTPUT3, 'X',214, '1', TBoundaryCell::Z },

    {"216", "IOE(79)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"217", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"218", "IOE(79)",  TBoundaryCell::OUTPUT3, 'X',217, '1', TBoundaryCell::Z },

    {"219", "DIN(3)",  TBoundaryCell::INPUT,    'X', -1, '0', TBoundaryCell::Z },
    {"220", "*",       TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
    {"221", "*",       TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

    {"222", "IOE(80)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"223", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"224", "IOE(80)",  TBoundaryCell::OUTPUT3, 'X',223, '1', TBoundaryCell::Z },

    {"225", "IOE(81)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"226", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"227", "IOE(81)",  TBoundaryCell::OUTPUT3, 'X',226, '1', TBoundaryCell::Z },

    {"228", "IOE(82)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"229", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"230", "IOE(82)",  TBoundaryCell::OUTPUT3, 'X',229, '1', TBoundaryCell::Z },

    {"231", "IOE(83)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"232", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"233", "IOE(83)",  TBoundaryCell::OUTPUT3, 'X',232, '1', TBoundaryCell::Z },

    {"234", "IOE(84)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"235", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"236", "IOE(84)",  TBoundaryCell::OUTPUT3, 'X',235, '1', TBoundaryCell::Z },

    {"237", "IOE(85)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"238", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"239", "IOE(85)",  TBoundaryCell::OUTPUT3, 'X',238, '1', TBoundaryCell::Z },

    {"240", "IOE(86)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"241", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"242", "IOE(86)",  TBoundaryCell::OUTPUT3, 'X',241, '1', TBoundaryCell::Z },

    {"243", "IOE(87)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"244", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"245", "IOE(87)",  TBoundaryCell::OUTPUT3, 'X',244, '1', TBoundaryCell::Z },

    {"246", "IOE(88)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"247", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"248", "IOE(88)",  TBoundaryCell::OUTPUT3, 'X',247, '1', TBoundaryCell::Z },

    {"249", "IOE(89)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"250", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"251", "IOE(89)",  TBoundaryCell::OUTPUT3, 'X',250, '1', TBoundaryCell::Z },

    {"252", "IOE(90)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"253", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"254", "IOE(90)",  TBoundaryCell::OUTPUT3, 'X',253, '1', TBoundaryCell::Z },

    {"255", "IOE(91)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"256", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"257", "IOE(91)",  TBoundaryCell::OUTPUT3, 'X',256, '1', TBoundaryCell::Z },

    {"258", "IOE(92)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"259", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"260", "IOE(92)",  TBoundaryCell::OUTPUT3, 'X',259, '1', TBoundaryCell::Z },

    {"261", "IOE(93)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"262", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"263", "IOE(93)",  TBoundaryCell::OUTPUT3, 'X',262, '1', TBoundaryCell::Z },

    {"264", "IOE(94)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"265", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"266", "IOE(94)",  TBoundaryCell::OUTPUT3, 'X',265, '1', TBoundaryCell::Z },

    {"267", "DIN(4)",   TBoundaryCell::INPUT,    'X', -1, '0', TBoundaryCell::Z },
    {"268", "*",        TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
    {"269", "*",        TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

    {"270", "IOE(95)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"271", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"272", "IOE(95)",  TBoundaryCell::OUTPUT3, 'X',271, '1', TBoundaryCell::Z },

    {"273", "IOE(96)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"274", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"275", "IOE(96)",  TBoundaryCell::OUTPUT3, 'X',274, '1', TBoundaryCell::Z },

    {"276", "IOE(97)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"277", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"278", "IOE(97)",  TBoundaryCell::OUTPUT3, 'X',277, '1', TBoundaryCell::Z },

    {"279", "IOE(98)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"280", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"281", "IOE(98)",  TBoundaryCell::OUTPUT3, 'X',280, '1', TBoundaryCell::Z },

    {"282", "IOE(99)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"283", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"284", "IOE(99)",  TBoundaryCell::OUTPUT3, 'X',283, '1', TBoundaryCell::Z },

    {"285", "IOE(100)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"286", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"287", "IOE(100)", TBoundaryCell::OUTPUT3, 'X',286, '1', TBoundaryCell::Z },

    {"288", "IOE(101)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"289", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"290", "IOE(101)", TBoundaryCell::OUTPUT3, 'X',289, '1', TBoundaryCell::Z },

    {"291", "IOE(102)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"292", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"293", "IOE(102)", TBoundaryCell::OUTPUT3, 'X',292, '1', TBoundaryCell::Z },

    {"294", "IOE(103)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"295", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"296", "IOE(103)", TBoundaryCell::OUTPUT3, 'X',295, '1', TBoundaryCell::Z },

    {"297", "*",         TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },//CONF_DONE
    {"298", "*",         TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
    {"299", "*",         TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

    {"300", "DCLK",      TBoundaryCell::INPUT,    'X', -1, '0', TBoundaryCell::Z },
    {"301", "*",         TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
    {"302", "*",         TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

    {"303", "IOE(104)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"304", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"305", "IOE(104)", TBoundaryCell::OUTPUT3, 'X',304, '1', TBoundaryCell::Z },

    {"306", "IOE(105)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"307", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"308", "IOE(105)", TBoundaryCell::OUTPUT3, 'X',307, '1', TBoundaryCell::Z },

    {"309", "IOE(106)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"310", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"311", "IOE(106)", TBoundaryCell::OUTPUT3, 'X',310, '1', TBoundaryCell::Z },

    {"312", "IOE(107)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"313", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"314", "IOE(107)", TBoundaryCell::OUTPUT3, 'X',313, '1', TBoundaryCell::Z },

    {"315", "IOE(108)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"316", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"317", "IOE(108)", TBoundaryCell::OUTPUT3, 'X',316, '1', TBoundaryCell::Z },

    {"318", "IOE(109)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"319", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"320", "IOE(109)", TBoundaryCell::OUTPUT3, 'X',319, '1', TBoundaryCell::Z },

    {"321", "IOE(110)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"322", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"323", "IOE(110)", TBoundaryCell::OUTPUT3, 'X',322, '1', TBoundaryCell::Z },

    {"324", "IOE(111)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"325", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"326", "IOE(111)", TBoundaryCell::OUTPUT3, 'X',325, '1', TBoundaryCell::Z },

    {"327", "IOE(112)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"328", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"329", "IOE(112)", TBoundaryCell::OUTPUT3, 'X',328, '1', TBoundaryCell::Z },

    {"330", "IOE(113)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"331", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"332", "IOE(113)", TBoundaryCell::OUTPUT3, 'X',331, '1', TBoundaryCell::Z },

    {"333", "IOE(114)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"334", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"335", "IOE(114)", TBoundaryCell::OUTPUT3, 'X',334, '1', TBoundaryCell::Z },

    {"336", "IOE(115)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"337", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"338", "IOE(115)", TBoundaryCell::OUTPUT3, 'X',337, '1', TBoundaryCell::Z },

    {"339", "IOE(116)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"340", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"341", "IOE(116)", TBoundaryCell::OUTPUT3, 'X',340, '1', TBoundaryCell::Z },

    {"342", "IOE(117)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"343", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"344", "IOE(117)", TBoundaryCell::OUTPUT3, 'X',343, '1', TBoundaryCell::Z },

    {"345", "IOE(118)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"346", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"347", "IOE(118)", TBoundaryCell::OUTPUT3, 'X',346, '1', TBoundaryCell::Z },

    {"348", "IOE(119)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"349", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"350", "IOE(119)", TBoundaryCell::OUTPUT3, 'X',349, '1', TBoundaryCell::Z },

    {"351", "IOE(120)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"352", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"353", "IOE(120)", TBoundaryCell::OUTPUT3, 'X',352, '1', TBoundaryCell::Z },

    {"354", "IOE(121)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"355", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"356", "IOE(121)", TBoundaryCell::OUTPUT3, 'X',355, '1', TBoundaryCell::Z },

    {"357", "IOE(122)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"358", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"359", "IOE(122)", TBoundaryCell::OUTPUT3, 'X',358, '1', TBoundaryCell::Z },

    {"360", "IOE(123)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"361", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"362", "IOE(123)", TBoundaryCell::OUTPUT3, 'X',361, '1', TBoundaryCell::Z },

    {"363", "IOE(124)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"364", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"365", "IOE(124)", TBoundaryCell::OUTPUT3, 'X',364, '1', TBoundaryCell::Z },

    {"366", "IOE(125)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"367", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"368", "IOE(125)", TBoundaryCell::OUTPUT3, 'X',367, '1', TBoundaryCell::Z },

    {"369", "IOE(126)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"370", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"371", "IOE(126)", TBoundaryCell::OUTPUT3, 'X',370, '1', TBoundaryCell::Z },

    {"372", "IOE(127)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"373", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"374", "IOE(127)", TBoundaryCell::OUTPUT3, 'X',373, '1', TBoundaryCell::Z },

    {"375", "IOE(128)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"376", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"377", "IOE(128)", TBoundaryCell::OUTPUT3, 'X',376, '1', TBoundaryCell::Z },

    {"378", "IOE(129)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"379", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"380", "IOE(129)", TBoundaryCell::OUTPUT3, 'X',379, '1', TBoundaryCell::Z },

    {"381", "IOE(130)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"382", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"383", "IOE(130)", TBoundaryCell::OUTPUT3, 'X',382, '1', TBoundaryCell::Z },

    {"384", "IOE(131)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"385", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"386", "IOE(131)", TBoundaryCell::OUTPUT3, 'X',385, '1', TBoundaryCell::Z },

    {"387", "IOE(132)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"388", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"389", "IOE(132)", TBoundaryCell::OUTPUT3, 'X',388, '1', TBoundaryCell::Z },

    {"390", "IOE(133)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"391", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"392", "IOE(133)", TBoundaryCell::OUTPUT3, 'X',391, '1', TBoundaryCell::Z },

    {"393", "IOE(134)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"394", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"395", "IOE(134)", TBoundaryCell::OUTPUT3, 'X',394, '1', TBoundaryCell::Z },

    {"396", "IOE(135)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"397", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"398", "IOE(135)", TBoundaryCell::OUTPUT3, 'X',397, '1', TBoundaryCell::Z },

    {"399", "IOE(136)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"400", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"401", "IOE(136)", TBoundaryCell::OUTPUT3, 'X',400, '1', TBoundaryCell::Z },

    {"402", "IOE(137)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"403", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"404", "IOE(137)", TBoundaryCell::OUTPUT3, 'X',403, '1', TBoundaryCell::Z },

    {"405", "IOE(138)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"406", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"407", "IOE(138)", TBoundaryCell::OUTPUT3, 'X',406, '1', TBoundaryCell::Z },

    {"408", "IOE(139)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"409", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"410", "IOE(139)", TBoundaryCell::OUTPUT3, 'X',409, '1', TBoundaryCell::Z },

    {"411", "IOE(140)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"412", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"413", "IOE(140)", TBoundaryCell::OUTPUT3, 'X',412, '1', TBoundaryCell::Z },

    {"414", "IOE(141)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"415", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"416", "IOE(141)", TBoundaryCell::OUTPUT3, 'X',415, '1', TBoundaryCell::Z },

    {"417", "IOE(142)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"418", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"419", "IOE(142)", TBoundaryCell::OUTPUT3, 'X',418, '1', TBoundaryCell::Z },

    {"420", "IOE(143)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"421", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"422", "IOE(143)", TBoundaryCell::OUTPUT3, 'X',421, '1', TBoundaryCell::Z },

    {"423", "IOE(144)", TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"424", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"425", "IOE(144)", TBoundaryCell::OUTPUT3, 'X',424, '1', TBoundaryCell::Z },

    {"426", "nSP",      TBoundaryCell::INPUT,    'X', -1, '0', TBoundaryCell::Z },
    {"427", "*",        TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
    {"428", "*",        TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

    {"429", "MSEL0",    TBoundaryCell::INPUT,    'X', -1, '0', TBoundaryCell::Z },
    {"430", "*",        TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
    {"431", "*",        TBoundaryCell::INTERNAL, 'X', -1, '1', TBoundaryCell::Z },

    {"432", "IOE(1)",   TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"433", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"434", "IOE(1)",   TBoundaryCell::OUTPUT3, 'X',433, '1', TBoundaryCell::Z },

    {"435", "IOE(2)",   TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"436", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"437", "IOE(2)",   TBoundaryCell::OUTPUT3, 'X',436, '1', TBoundaryCell::Z },

    {"438", "IOE(3)",   TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"439", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"440", "IOE(3)",   TBoundaryCell::OUTPUT3, 'X',439, '1', TBoundaryCell::Z },

    {"441", "IOE(4)",   TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"442", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"443", "IOE(4)",   TBoundaryCell::OUTPUT3, 'X',442, '1', TBoundaryCell::Z },

    {"444", "IOE(5)",   TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"445", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"446", "IOE(5)",   TBoundaryCell::OUTPUT3, 'X',445, '1', TBoundaryCell::Z },

    {"447", "IOE(6)",   TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"448", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"449", "IOE(6)",   TBoundaryCell::OUTPUT3, 'X',448, '1', TBoundaryCell::Z },

    {"450", "IOE(7)",   TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"451", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"452", "IOE(7)",   TBoundaryCell::OUTPUT3, 'X',451, '1', TBoundaryCell::Z },

    {"453", "IOE(8)",   TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"454", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"455", "IOE(8)",   TBoundaryCell::OUTPUT3, 'X',454, '1', TBoundaryCell::Z },

    {"456", "DIN(1)",   TBoundaryCell::INPUT,    'X', -1, '0', TBoundaryCell::Z },
    {"457", "*",        TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
    {"458", "*",        TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

    {"459", "IOE(9)",   TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"460", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"461", "IOE(9)",   TBoundaryCell::OUTPUT3, 'X',460, '1', TBoundaryCell::Z },

    {"462", "IOE(10)",  TBoundaryCell::INPUT,   'X', -1, '0', TBoundaryCell::Z },
    {"463", "*",        TBoundaryCell::CONTROL, '0', -1, '0', TBoundaryCell::Z },
    {"464", "IOE(10)",  TBoundaryCell::OUTPUT3, 'X',463, '1', TBoundaryCell::Z }
    };

    TBoundaryRegister*  breg = new TBoundaryRegister( (sizeof(b_reg_tab)/sizeof(pin_info)),
				 (sizeof(b_reg_tab)/sizeof(pin_info))/3, 192 );

    for( i=(sizeof(b_reg_tab)/sizeof(pin_info))-1; i>=0; i-- )
	breg->AddElement( *new TBoundaryCell( b_reg_tab[i].name, 
					     i, 
					     TBoundaryCell::BC_1, 
					     b_reg_tab[i].port, 
					     b_reg_tab[i].function, 
					     b_reg_tab[i].safe, 
					     b_reg_tab[i].ccell, 
					     b_reg_tab[i].disval, 
					     b_reg_tab[i].rslt       ) );
    breg->ScanBoundaryCells();


    /// pin mapping
    char cbuf[32];
    char * IOE_pinmap[] = { 
	"T16", "T14", "U16", "R13", "U15", "T13", "U14", "R12", "T12", "U12", 
	"U11", "T11", "U10", "T10", "U9", "T9", "T8", "T7", "R7", "U6", "T6", "R6", "U4", 
	"T5", "U3", "R5", "U2", "T4", "T2", "R4", "U1", "P3", "R2", "P2", 
	"N3", "T1", "N2", "R1", "M3", "P1", "M2", "N1", "M1", "L3", "L1", "L2", "K1", "K3", "J1", "K2", "J4", "J2", 
	"J3", "H1", "H2", "G1", "H3", "F1", "E1", "G2", "D1", "G3", "C1", "F2", "B1", "F3", "E2", "C2", "E3", "A1", 
	"D2", "B2", "B4", "A2", "C5", "A3", "B5", "A4", "C6", "B6", "A6", "C7", "A7", "B7", "A8", "B8", 
	"A9", "B10", "A10", "B11", "A11", "C11", "A12", "B12", "C12", "A14", "B13", "A15", 
	"C13", "A16", "B14", "B16", "C14", "A17", 
	"D16", "C16", "E15", "E16", "B17", "F15", "C17", "F16", "D17", "G15", "E17", "F17", "G16", 
	"G17", "H15", "H17", "H16", "J16", "J14", "J15", "J17", "K16", "K17", "K15", "L17", "L16", "M17", 
	"N17", "L15", "P17", "M16", "R17", "M15", "T17", "N16", "N15", "R16", "P16", "U17", "P15"
    };

    char * DIN_pinmap[] = { "U13", "U5", "A5", "A13" };
    char * VCC_pinmap[] = { 
	"C8", "C9", "C10", "D3", "D4", "D9", "D14", "D15", "G4", "G14", 
	"L4", "L14", "P4", "P9", "P14", "R8", "R9", "R10", "R14"
    };
    char * GND_pinmap[] = { 
	"C4", "D7", "D8", "D10", "D11", "H4", "H14", "K4", "K14", "P7", "P8", "P10", "P11"
    };
	
    for( i=0; i<(sizeof(IOE_pinmap)/sizeof(char*)); i++ ) {
	sprintf( cbuf, "IOE(%d)",i+1 );
	breg->AddPinMapping( cbuf, IOE_pinmap[i] );
    }
	
    for( i=0; i<(sizeof(DIN_pinmap)/sizeof(char*)); i++ ) {
	sprintf( cbuf, "DIN(%d)",i+1 );
	breg->AddPinMapping( cbuf, DIN_pinmap[i] );
    }
	
    for( i=0; i<(sizeof(VCC_pinmap)/sizeof(char*)); i++ ) {
	sprintf( cbuf, "VCC(%d)",i+1 );
	breg->AddPinMapping( cbuf, VCC_pinmap[i] );
    }
	
    for( i=0; i<(sizeof(GND_pinmap)/sizeof(char*)); i++ ) {
	sprintf( cbuf, "GND(%d)",i+1 );
	breg->AddPinMapping( cbuf, GND_pinmap[i] );
    }
		   
    breg->AddPinMapping( "nSP", "R15" );
    breg->AddPinMapping( "MSEL0", "T15" );
    breg->AddPinMapping( "MSEL1", "T3" );
    breg->AddPinMapping( "nSTATUS", "B3" );
    breg->AddPinMapping( "nCONFIG", "C3" );
    breg->AddPinMapping( "DCLK", "C15" );
    breg->AddPinMapping( "CONF_DONE", "B15" );
    breg->AddPinMapping( "TCK", "U8" );
    breg->AddPinMapping( "TDI", "R11" );
    breg->AddPinMapping( "TDO", "B9" );
    breg->AddPinMapping( "TMS", "U7" );
    breg->AddPinMapping( "nTRST", "R3" );
    AddRegister( *breg );		     
}
