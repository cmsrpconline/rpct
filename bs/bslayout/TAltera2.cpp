#include "TAltera2.h"

#include <algorithm>
using namespace bs;

/*
attribute INSTRUCTION_LENGTH of EPF1K50F256 : entity is 10;
attribute INSTRUCTION_OPCODE of EPF1K50F256 : entity is
  "BYPASS   (1111111111)," &
  "EXTEST   (0000000000)," &
  "SAMPLE   (0001010101)," &
  "IDCODE   (0000000110)," &
  "USERCODE (0000000111)";
attribute INSTRUCTION_CAPTURE of EPF1K50F256 : entity is "0101010101";

attribute IDCODE_REGISTER of EPF1K50F256 : entity is
  "0001"&               --4-bit Version
  "0001000001010000"&   --16-bit Part Number (hex 1050)
  "00001101110"&        --11-bit Manufacturer's Identity
  "1";                  --Mandatory LSB
attribute USERCODE_REGISTER of EPF1K50F256 : entity is
  "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";  --Bits 26-20 are programmable
attribute REGISTER_ACCESS of EPF1K50F256 : entity is
  "DEVICE_ID (IDCODE)";
  */


TAltera2::TAltera2(const std::string& name )
    : TChip( name, "0101010101", 798, "00010001000001010000000011011101" )
{
    int i,j;

    const char* USERCODE_REG_NAME = "USERCODE";

    // instrukcje
    // trzeba jeszcze dodac capture values
    const std::string inst_tab [5][3] = {
      { "BYPASS"  ,  TRegister::BYPASS_REG_NAME,   "1111111111" },
      { "EXTEST"  ,  TRegister::BOUNDARY_REG_NAME, "0000000000" },
      { "SAMPLE"  ,  TRegister::BOUNDARY_REG_NAME, "0001010101" },
      { "IDCODE"  ,  TRegister::IDCODE_REG_NAME,   "0000000110" },
      { "USERCODE",             USERCODE_REG_NAME, "0000000111" }
    };
    for( i=0; i<5; i++ ) {
	    AddInstruction( *new TInstruction(inst_tab[i][0], inst_tab[i][1], inst_tab[i][2]) );
    }
    

    // rejestry
    TRegister* reg;


    // boundary register
    
    struct pin_info {
      char* name;
      char* port;
      TBoundaryCell::TFunction function;
      char safe;
      int ccell;    //-1 jak nie ma
      char disval;
      TBoundaryCell::TDisResult rslt;
    };

    pin_info  b_reg_tab[]= {
       {"0",    "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"1",    "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"2",    "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

        //BSC group 1 for untestable Family-specific pin
       {"3",    "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"4",    "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"5",    "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

        //BSC group 2 for I/O pin E13
       {"6",    "IOE13", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"7",    "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"8",    "IOE13", TBoundaryCell::OUTPUT3, 'X', 7, '1', TBoundaryCell::Z },

        //BSC group 3 for unused pad
       {"9",    "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"10",   "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"11",   "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

        //BSC group 4 for I/O pin E14
       {"12",   "IOE14", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"13",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"14",   "IOE14", TBoundaryCell::OUTPUT3, 'X', 13, '1', TBoundaryCell::Z },
 
        //BSC group 5 for I/O pin D16
       {"15",   "IOD16", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"16",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"17",   "IOD16", TBoundaryCell::OUTPUT3, 'X', 16, '1', TBoundaryCell::Z },
 
        //BSC group 6 for I/O pin D15
       {"18",   "IOD15", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"19",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"20",   "IOD15", TBoundaryCell::OUTPUT3, 'X', 19, '1', TBoundaryCell::Z },

        //BSC group 7 for I/O pin F13
       {"21",   "IOF13", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"22",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"23",   "IOF13", TBoundaryCell::OUTPUT3, 'X', 22, '1', TBoundaryCell::Z },
 
        //BSC group 8 for unused pad
       {"24",   "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"25",   "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"26",   "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 9 for I/O pin E15
       {"27",   "IOE15", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"28",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"29",   "IOE15", TBoundaryCell::OUTPUT3, 'X', 28, '1', TBoundaryCell::Z },
 
        //BSC group 10 for unused pad
       {"30",   "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"31",   "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"32",   "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

        //BSC group 11 for I/O pin E16
       {"33",   "IOE16", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"34",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"35",   "IOE16", TBoundaryCell::OUTPUT3, 'X', 34, '1', TBoundaryCell::Z },
 
        //BSC group 12 for I/O pin F14
       {"36",   "IOF14", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"37",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"38",   "IOF14", TBoundaryCell::OUTPUT3, 'X', 37, '1', TBoundaryCell::Z },
 
        //BSC group 13 for I/O pin G12
       {"39",   "IOG12", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"40",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"41",   "IOG12", TBoundaryCell::OUTPUT3, 'X', 40, '1', TBoundaryCell::Z },
 
        //BSC group 14 for I/O pin G13
       {"42",   "IOG13", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"43",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"44",   "IOG13", TBoundaryCell::OUTPUT3, 'X', 43, '1', TBoundaryCell::Z },

        //BSC group 15 for unused pad
       {"45",   "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"46",   "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"47",   "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 16 for I/O pin F15
       {"48",   "IOF15", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"49",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"50",   "IOF15", TBoundaryCell::OUTPUT3, 'X', 49, '1', TBoundaryCell::Z },
 
        //BSC group 17 for unused pad
       {"51",   "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"52",   "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"53",   "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 18 for I/O pin F16
       {"54",   "IOF16", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"55",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"56",   "IOF16", TBoundaryCell::OUTPUT3, 'X', 55, '1', TBoundaryCell::Z },

        //BSC group 19 for I/O pin G14
       {"57",   "IOG14", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"58",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"59",   "IOG14", TBoundaryCell::OUTPUT3, 'X', 58, '1', TBoundaryCell::Z },
 
        //BSC group 20 for I/O pin H12
       {"60",   "IOH12", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"61",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"62",   "IOH12", TBoundaryCell::OUTPUT3, 'X', 61, '1', TBoundaryCell::Z },
 
        //BSC group 21 for I/O pin G15
       {"63",   "IOG15", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"64",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"65",   "IOG15", TBoundaryCell::OUTPUT3, 'X', 64, '1', TBoundaryCell::Z },
 
        //BSC group 22 for unused pad
       {"66",   "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"67",   "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"68",   "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

        //BSC group 23 for I/O pin G16
       {"69",   "IOG16", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"70",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"71",   "IOG16", TBoundaryCell::OUTPUT3, 'X', 70, '1', TBoundaryCell::Z },
 
        //BSC group 24 for I/O pin H13
       {"72",   "IOH13", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"73",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"74",   "IOH13", TBoundaryCell::OUTPUT3, 'X', 73, '1', TBoundaryCell::Z },
 
        //BSC group 25 for I/O pin H14
       {"75",   "IOH14", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"76",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"77",   "IOH14", TBoundaryCell::OUTPUT3, 'X', 76, '1', TBoundaryCell::Z },
 
        //BSC group 26 for I/O pin H16
       {"78",   "IOH16", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"79",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"80",   "IOH16", TBoundaryCell::OUTPUT3, 'X', 79, '1', TBoundaryCell::Z },

        //BSC group 27 for unused pad
       {"81",   "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"82",   "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"83",   "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 28 for I/O pin H15
       {"84",   "IOH15", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"85",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"86",   "IOH15", TBoundaryCell::OUTPUT3, 'X', 85, '1', TBoundaryCell::Z },
 
        //BSC group 29 for I/O pin J12
       {"87",   "IOJ12", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"88",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"89",   "IOJ12", TBoundaryCell::OUTPUT3, 'X', 88, '1', TBoundaryCell::Z },
 
        //BSC group 30 for I/O pin J13
       {"90",   "IOJ13", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"91",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"92",   "IOJ13", TBoundaryCell::OUTPUT3, 'X', 91, '1', TBoundaryCell::Z },

        //BSC group 31 for I/O pin J14
       {"93",   "IOJ14", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"94",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"95",   "IOJ14", TBoundaryCell::OUTPUT3, 'X', 94, '1', TBoundaryCell::Z },
 
        //BSC group 32 for unused pad
       {"96",   "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"97",   "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"98",   "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 33 for I/O pin J16
       {"99",   "IOJ16", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"100",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"101",  "IOJ16", TBoundaryCell::OUTPUT3, 'X', 100, '1', TBoundaryCell::Z },
 
        //BSC group 34 for I/O pin J15
       {"102",  "IOJ15", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"103",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"104",  "IOJ15", TBoundaryCell::OUTPUT3, 'X', 103, '1', TBoundaryCell::Z },

        //BSC group 35 for I/O pin K12
       {"105",  "IOK12", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"106",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"107",  "IOK12", TBoundaryCell::OUTPUT3, 'X', 106, '1', TBoundaryCell::Z },
 
        //BSC group 36 for I/O pin K13
       {"108",  "IOK13", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"109",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"110",  "IOK13", TBoundaryCell::OUTPUT3, 'X', 109, '1', TBoundaryCell::Z },
 
        //BSC group 37 for I/O pin K16
       {"111",  "IOK16", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"112",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"113",  "IOK16", TBoundaryCell::OUTPUT3, 'X', 112, '1', TBoundaryCell::Z },
 
        //BSC group 38 for I/O pin K15
       {"114",  "IOK15", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"115",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"116",  "IOK15", TBoundaryCell::OUTPUT3, 'X', 115, '1', TBoundaryCell::Z },

        //BSC group 39 for I/O pin K14
       {"117",  "IOK14", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"118",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"119",  "IOK14", TBoundaryCell::OUTPUT3, 'X', 118, '1', TBoundaryCell::Z },
 
        //BSC group 40 for I/O pin L14
       {"120",  "IOL14", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"121",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"122",  "IOL14", TBoundaryCell::OUTPUT3, 'X', 121, '1', TBoundaryCell::Z },
 
        //BSC group 41 for unused pad
       {"123",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"124",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"125",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 42 for I/O pin L15
       {"126",  "IOL15", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"127",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"128",  "IOL15", TBoundaryCell::OUTPUT3, 'X', 127, '1', TBoundaryCell::Z },

        //BSC group 43 for I/O pin L16
       {"129",  "IOL16", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"130",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"131",  "IOL16", TBoundaryCell::OUTPUT3, 'X', 130, '1', TBoundaryCell::Z },
 
        //BSC group 44 for I/O pin L13
       {"132",  "IOL13", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"133",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"134",  "IOL13", TBoundaryCell::OUTPUT3, 'X', 133, '1', TBoundaryCell::Z },
 
        //BSC group 45 for I/O pin M14
       {"135",  "IOM14", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"136",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"137",  "IOM14", TBoundaryCell::OUTPUT3, 'X', 136, '1', TBoundaryCell::Z },
 
        //BSC group 46 for I/O pin N14
       {"138",  "ION14", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"139",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"140",  "ION14", TBoundaryCell::OUTPUT3, 'X', 139, '1', TBoundaryCell::Z },

        //BSC group 47 for I/O pin M16
       {"141",  "IOM16", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"142",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"143",  "IOM16", TBoundaryCell::OUTPUT3, 'X', 142, '1', TBoundaryCell::Z },
 
        //BSC group 48 for I/O pin M15
       {"144",  "IOM15", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"145",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"146",  "IOM15", TBoundaryCell::OUTPUT3, 'X', 145, '1', TBoundaryCell::Z },
 
        //BSC group 49 for unused pad
       {"147",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"148",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"149",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 50 for I/O pin P14
       {"150",  "IOP14", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"151",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"152",  "IOP14", TBoundaryCell::OUTPUT3, 'X', 151, '1', TBoundaryCell::Z },

        //BSC group 51 for I/O pin N15
       {"153",  "ION15", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"154",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"155",  "ION15", TBoundaryCell::OUTPUT3, 'X', 154, '1', TBoundaryCell::Z },
 
        //BSC group 52 for I/O pin N16
       {"156",  "ION16", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"157",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"158",  "ION16", TBoundaryCell::OUTPUT3, 'X', 157, '1', TBoundaryCell::Z },
 
        //BSC group 53 for I/O pin P16
       {"159",  "IOP16", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"160",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"161",  "IOP16", TBoundaryCell::OUTPUT3, 'X', 160, '1', TBoundaryCell::Z },
 
        //BSC group 54 for unused pad
       {"162",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"163",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"164",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

        //BSC group 55 for untestable Family-specific pin
       {"165",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"166",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"167",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 56 for I/O pin R15
       {"168",  "IOR15", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"169",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"170",  "IOR15", TBoundaryCell::OUTPUT3, 'X', 169, '1', TBoundaryCell::Z },
 
        //BSC group 57 for I/O pin R14
       {"171",  "IOR14", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"172",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"173",  "IOR14", TBoundaryCell::OUTPUT3, 'X', 172, '1', TBoundaryCell::Z },
 
        //BSC group 58 for I/O pin T15
       {"174",  "IOT15", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"175",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"176",  "IOT15", TBoundaryCell::OUTPUT3, 'X', 175, '1', TBoundaryCell::Z },

        //BSC group 59 for unused pad
       {"177",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"178",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"179",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 60 for I/O pin T14
       {"180",  "IOT14", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"181",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"182",  "IOT14", TBoundaryCell::OUTPUT3, 'X', 181, '1', TBoundaryCell::Z },
 
        //BSC group 61 for unused pad
       {"183",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"184",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"185",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 62 for I/O pin M13
       {"186",  "IOM13", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"187",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"188",  "IOM13", TBoundaryCell::OUTPUT3, 'X', 187, '1', TBoundaryCell::Z },

        //BSC group 63 for unused pad
       {"189",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"190",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"191",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 64 for I/O pin R13
       {"192",  "IOR13", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"193",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"194",  "IOR13", TBoundaryCell::OUTPUT3, 'X', 193, '1', TBoundaryCell::Z },
 
        //BSC group 65 for unused pad
       {"195",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"196",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"197",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 66 for I/O pin T13
       {"198",  "IOT13", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"199",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"200",  "IOT13", TBoundaryCell::OUTPUT3, 'X', 199, '1', TBoundaryCell::Z },

        //BSC group 67 for unused pad
       {"201",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"202",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"203",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 68 for I/O pin P13
       {"204",  "IOP13", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"205",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"206",  "IOP13", TBoundaryCell::OUTPUT3, 'X', 205, '1', TBoundaryCell::Z },
 
        //BSC group 69 for unused pad
       {"207",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"208",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"209",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 70 for I/O pin N13
       {"210",  "ION13", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"211",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"212",  "ION13", TBoundaryCell::OUTPUT3, 'X', 211, '1', TBoundaryCell::Z },

        //BSC group 71 for I/O pin T12
       {"213",  "IOT12", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"214",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"215",  "IOT12", TBoundaryCell::OUTPUT3, 'X', 214, '1', TBoundaryCell::Z },
 
        //BSC group 72 for unused pad
       {"216",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"217",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"218",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 73 for I/O pin P12
       {"219",  "IOP12", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"220",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"221",  "IOP12", TBoundaryCell::OUTPUT3, 'X', 220, '1', TBoundaryCell::Z },
 
        //BSC group 74 for I/O pin R12
       {"222",  "IOR12", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"223",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"224",  "IOR12", TBoundaryCell::OUTPUT3, 'X', 223, '1', TBoundaryCell::Z },

        //BSC group 75 for unused pad
       {"225",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"226",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"227",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 76 for I/O pin R11
       {"228",  "IOR11", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"229",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"230",  "IOR11", TBoundaryCell::OUTPUT3, 'X', 229, '1', TBoundaryCell::Z },
 
        //BSC group 77 for I/O pin T11
       {"231",  "IOT11", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"232",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"233",  "IOT11", TBoundaryCell::OUTPUT3, 'X', 232, '1', TBoundaryCell::Z },
 
        //BSC group 78 for I/O pin P11
       {"234",  "IOP11", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"235",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"236",  "IOP11", TBoundaryCell::OUTPUT3, 'X', 235, '1', TBoundaryCell::Z },

        //BSC group 79 for I/O pin N11
       {"237",  "ION11", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"238",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"239",  "ION11", TBoundaryCell::OUTPUT3, 'X', 238, '1', TBoundaryCell::Z },
 
        //BSC group 80 for I/O pin M10
       {"240",  "IOM10", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"241",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"242",  "IOM10", TBoundaryCell::OUTPUT3, 'X', 241, '1', TBoundaryCell::Z },
 
        //BSC group 81 for unused pad
       {"243",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"244",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"245",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 82 for I/O pin N10
       {"246",  "ION10", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"247",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"248",  "ION10", TBoundaryCell::OUTPUT3, 'X', 247, '1', TBoundaryCell::Z },

        //BSC group 83 for unused pad
       {"249",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"250",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"251",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 84 for I/O pin P10
       {"252",  "IOP10", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"253",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"254",  "IOP10", TBoundaryCell::OUTPUT3, 'X', 253, '1', TBoundaryCell::Z },
 
        //BSC group 85 for unused pad
       {"255",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"256",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"257",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 86 for I/O pin T10
       {"258",  "IOT10", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"259",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"260",  "IOT10", TBoundaryCell::OUTPUT3, 'X', 259, '1', TBoundaryCell::Z },

        //BSC group 87 for I/O pin R10
       {"261",  "IOR10", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"262",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"263",  "IOR10", TBoundaryCell::OUTPUT3, 'X', 262, '1', TBoundaryCell::Z },
 
        //BSC group 88 for I/O pin N9
       {"264",  "ION9", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"265",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"266",  "ION9", TBoundaryCell::OUTPUT3, 'X', 265, '1', TBoundaryCell::Z },
 
        //BSC group 89 for I/O pin P9
       {"267",  "IOP9", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"268",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"269",  "IOP9", TBoundaryCell::OUTPUT3, 'X', 268, '1', TBoundaryCell::Z },
 
        //BSC group 90 for I/O pin R9
       {"270",  "IOR9", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"271",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"272",  "IOR9", TBoundaryCell::OUTPUT3, 'X', 271, '1', TBoundaryCell::Z },

        //BSC group 91 for I/O pin T9
       {"273",  "IOT9", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"274",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"275",  "IOT9", TBoundaryCell::OUTPUT3, 'X', 274, '1', TBoundaryCell::Z },
 
        //BSC group 92 for dedicated input pin M9
       {"276",  "INM9", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"277",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"278",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 93 for dedicated clock pin L8
       {"279",  "CLKL8", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"280",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"281",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 94 for dedicated input pin R8
       {"282",  "INR8", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"283",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"284",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

        //BSC group 95 for I/O pin P8
       {"285",  "IOP8", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"286",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"287",  "IOP8", TBoundaryCell::OUTPUT3, 'X', 286, '1', TBoundaryCell::Z },
 
        //BSC group 96 for unused pad
       {"288",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"289",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"290",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 97 for I/O pin N8
       {"291",  "ION8", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"292",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"293",  "ION8", TBoundaryCell::OUTPUT3, 'X', 292, '1', TBoundaryCell::Z },
 
        //BSC group 98 for unused pad
       {"294",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"295",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"296",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

        //BSC group 99 for I/O pin M8
       {"297",  "IOM8", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"298",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"299",  "IOM8", TBoundaryCell::OUTPUT3, 'X', 298, '1', TBoundaryCell::Z },
 
        //BSC group 100 for unused pad
       {"300",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"301",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"302",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 101 for I/O pin T7
       {"303",  "IOT7", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"304",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"305",  "IOT7", TBoundaryCell::OUTPUT3, 'X', 304, '1', TBoundaryCell::Z },
 
        //BSC group 102 for unused pad
       {"306",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"307",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"308",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

        //BSC group 103 for I/O pin R7
       {"309",  "IOR7", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"310",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"311",  "IOR7", TBoundaryCell::OUTPUT3, 'X', 310, '1', TBoundaryCell::Z },
 
        //BSC group 104 for I/O pin P7
       {"312",  "IOP7", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"313",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"314",  "IOP7", TBoundaryCell::OUTPUT3, 'X', 313, '1', TBoundaryCell::Z },
 
        //BSC group 105 for I/O pin N7
       {"315",  "ION7", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"316",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"317",  "ION7", TBoundaryCell::OUTPUT3, 'X', 316, '1', TBoundaryCell::Z },
 
        //BSC group 106 for I/O pin M7
       {"318",  "IOM7", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"319",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"320",  "IOM7", TBoundaryCell::OUTPUT3, 'X', 319, '1', TBoundaryCell::Z },

        //BSC group 107 for I/O pin T6
       {"321",  "IOT6", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"322",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"323",  "IOT6", TBoundaryCell::OUTPUT3, 'X', 322, '1', TBoundaryCell::Z },
 
        //BSC group 108 for I/O pin R6
       {"324",  "IOR6", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"325",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"326",  "IOR6", TBoundaryCell::OUTPUT3, 'X', 325, '1', TBoundaryCell::Z },
 
        //BSC group 109 for I/O pin P6
       {"327",  "IOP6", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"328",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"329",  "IOP6", TBoundaryCell::OUTPUT3, 'X', 328, '1', TBoundaryCell::Z },
 
        //BSC group 110 for unused pad
       {"330",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"331",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"332",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

        //BSC group 111 for I/O pin N6
       {"333",  "ION6", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"334",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"335",  "ION6", TBoundaryCell::OUTPUT3, 'X', 334, '1', TBoundaryCell::Z },
 
        //BSC group 112 for unused pad
       {"336",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"337",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"338",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 113 for I/O pin T5
       {"339",  "IOT5", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"340",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"341",  "IOT5", TBoundaryCell::OUTPUT3, 'X', 340, '1', TBoundaryCell::Z },
 
        //BSC group 114 for I/O pin R5
       {"342",  "IOR5", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"343",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"344",  "IOR5", TBoundaryCell::OUTPUT3, 'X', 343, '1', TBoundaryCell::Z },

        //BSC group 115 for I/O pin P5
       {"345",  "IOP5", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"346",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"347",  "IOP5", TBoundaryCell::OUTPUT3, 'X', 346, '1', TBoundaryCell::Z },
 
        //BSC group 116 for unused pad
       {"348",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"349",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"350",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 117 for I/O pin N5
       {"351",  "ION5", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"352",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"353",  "ION5", TBoundaryCell::OUTPUT3, 'X', 352, '1', TBoundaryCell::Z },
 
        //BSC group 118 for I/O pin T4
       {"354",  "IOT4", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"355",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"356",  "IOT4", TBoundaryCell::OUTPUT3, 'X', 355, '1', TBoundaryCell::Z },

        //BSC group 119 for I/O pin R4
       {"357",  "IOR4", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"358",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"359",  "IOR4", TBoundaryCell::OUTPUT3, 'X', 358, '1', TBoundaryCell::Z },
 
        //BSC group 120 for unused pad
       {"360",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"361",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"362",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 121 for unused pad
       {"363",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"364",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"365",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 122 for I/O pin P4
       {"366",  "IOP4", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"367",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"368",  "IOP4", TBoundaryCell::OUTPUT3, 'X', 367, '1', TBoundaryCell::Z },

        //BSC group 123 for I/O pin P3
       {"369",  "IOP3", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"370",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"371",  "IOP3", TBoundaryCell::OUTPUT3, 'X', 370, '1', TBoundaryCell::Z },
 
        //BSC group 124 for unused pad
       {"372",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"373",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"374",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 125 for I/O pin R3
       {"375",  "IOR3", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"376",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"377",  "IOR3", TBoundaryCell::OUTPUT3, 'X', 376, '1', TBoundaryCell::Z },
 
        //BSC group 126 for unused pad
       {"378",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"379",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"380",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

        //BSC group 127 for I/O pin T3
       {"381",  "IOT3", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"382",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"383",  "IOT3", TBoundaryCell::OUTPUT3, 'X', 382, '1', TBoundaryCell::Z },
 
        //BSC group 128 for I/O pin T2
       {"384",  "IOT2", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"385",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"386",  "IOT2", TBoundaryCell::OUTPUT3, 'X', 385, '1', TBoundaryCell::Z },
 
        //BSC group 129 for I/O pin T1
       {"387",  "IOT1", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"388",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"389",  "IOT1", TBoundaryCell::OUTPUT3, 'X', 388, '1', TBoundaryCell::Z },
 
        //BSC group 130 for unused pad
       {"390",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"391",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"392",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

        //BSC group 131 for untestable Family-specific pin
       {"393",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"394",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"395",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 132 for unused pad "*"
       {"396",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"397",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"398",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 133 for Family-specific input pin R1
       {"399",  "MSEL1", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"400",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"401",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 134 for Family-specific input pin P1
       {"402",  "MSEL0", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"403",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"404",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

        //BSC group 135 for I/O pin P2
       {"405",  "IOP2", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"406",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"407",  "IOP2", TBoundaryCell::OUTPUT3, 'X', 406, '1', TBoundaryCell::Z },
 
        //BSC group 136 for I/O pin N2
       {"408",  "ION2", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"409",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"410",  "ION2", TBoundaryCell::OUTPUT3, 'X', 409, '1', TBoundaryCell::Z },
 
        //BSC group 137 for unused pad
       {"411",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"412",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"413",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 138 for I/O pin N1
       {"414",  "ION1", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"415",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"416",  "ION1", TBoundaryCell::OUTPUT3, 'X', 415, '1', TBoundaryCell::Z },

        //BSC group 139 for I/O pin N3
       {"417",  "ION3", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"418",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"419",  "ION3", TBoundaryCell::OUTPUT3, 'X', 418, '1', TBoundaryCell::Z },
 
        //BSC group 140 for I/O pin M3
       {"420",  "IOM3", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"421",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"422",  "IOM3", TBoundaryCell::OUTPUT3, 'X', 421, '1', TBoundaryCell::Z },
 
        //BSC group 141 for I/O pin M2
       {"423",  "IOM2", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"424",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"425",  "IOM2", TBoundaryCell::OUTPUT3, 'X', 424, '1', TBoundaryCell::Z },
 
        //BSC group 142 for I/O pin M4
       {"426",  "IOM4", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"427",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"428",  "IOM4", TBoundaryCell::OUTPUT3, 'X', 427, '1', TBoundaryCell::Z },

        //BSC group 143 for unused pad
       {"429",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"430",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"431",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 144 for I/O pin M1
       {"432",  "IOM1", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"433",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"434",  "IOM1", TBoundaryCell::OUTPUT3, 'X', 433, '1', TBoundaryCell::Z },
 
        //BSC group 145 for I/O pin L4
       {"435",  "IOL4", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"436",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"437",  "IOL4", TBoundaryCell::OUTPUT3, 'X', 436, '1', TBoundaryCell::Z },
 
        //BSC group 146 for I/O pin L2
       {"438",  "IOL2", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"439",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"440",  "IOL2", TBoundaryCell::OUTPUT3, 'X', 439, '1', TBoundaryCell::Z },

        //BSC group 147 for unused pad
       {"441",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"442",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"443",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 148 for unused pad
       {"444",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"445",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"446",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 149 for I/O pin L3
       {"447",  "IOL3", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"448",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"449",  "IOL3", TBoundaryCell::OUTPUT3, 'X', 448, '1', TBoundaryCell::Z },
 
        //BSC group 150 for unused pad
       {"450",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"451",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"452",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

        //BSC group 151 for I/O pin K2
       {"453",  "IOK2", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"454",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"455",  "IOK2", TBoundaryCell::OUTPUT3, 'X', 454, '1', TBoundaryCell::Z },
 
        //BSC group 152 for I/O pin K1
       {"456",  "IOK1", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"457",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"458",  "IOK1", TBoundaryCell::OUTPUT3, 'X', 457, '1', TBoundaryCell::Z },
 
        //BSC group 153 for unused pad
       {"459",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"460",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"461",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 154 for I/O pin K3
       {"462",  "IOK3", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"463",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"464",  "IOK3", TBoundaryCell::OUTPUT3, 'X', 463, '1', TBoundaryCell::Z },

        //BSC group 155 for I/O pin K4
       {"465",  "IOK4", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"466",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"467",  "IOK4", TBoundaryCell::OUTPUT3, 'X', 466, '1', TBoundaryCell::Z },
 
        //BSC group 156 for I/O pin J2
       {"468",  "IOJ2", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"469",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"470",  "IOJ2", TBoundaryCell::OUTPUT3, 'X', 469, '1', TBoundaryCell::Z },
 
        //BSC group 157 for I/O pin J1
       {"471",  "IOJ1", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"472",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"473",  "IOJ1", TBoundaryCell::OUTPUT3, 'X', 472, '1', TBoundaryCell::Z },
 
        //BSC group 158 for I/O pin J3
       {"474",  "IOJ3", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"475",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"476",  "IOJ3", TBoundaryCell::OUTPUT3, 'X', 475, '1', TBoundaryCell::Z },

        //BSC group 159 for unused pad
       {"477",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"478",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"479",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 160 for I/O pin J4
       {"480",  "IOJ4", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"481",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"482",  "IOJ4", TBoundaryCell::OUTPUT3, 'X', 481, '1', TBoundaryCell::Z },
 
        //BSC group 161 for I/O pin J5
       {"483",  "IOJ5", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"484",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"485",  "IOJ5", TBoundaryCell::OUTPUT3, 'X', 484, '1', TBoundaryCell::Z },
 
        //BSC group 162 for I/O pin H1
       {"486",  "IOH1", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"487",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"488",  "IOH1", TBoundaryCell::OUTPUT3, 'X', 487, '1', TBoundaryCell::Z },

        //BSC group 163 for I/O pin H2
       {"489",  "IOH2", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"490",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"491",  "IOH2", TBoundaryCell::OUTPUT3, 'X', 490, '1', TBoundaryCell::Z },
 
        //BSC group 164 for unused pad
       {"492",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"493",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"494",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 165 for I/O pin H3
       {"495",  "IOH3", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"496",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"497",  "IOH3", TBoundaryCell::OUTPUT3, 'X', 496, '1', TBoundaryCell::Z },
 
        //BSC group 166 for I/O pin H4
       {"498",  "IOH4", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"499",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"500",  "IOH4", TBoundaryCell::OUTPUT3, 'X', 499, '1', TBoundaryCell::Z },

        //BSC group 167 for I/O pin H5
       {"501",  "IOH5", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"502",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"503",  "IOH5", TBoundaryCell::OUTPUT3, 'X', 502, '1', TBoundaryCell::Z },
 
        //BSC group 168 for I/O pin G1
       {"504",  "IOG1", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"505",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"506",  "IOG1", TBoundaryCell::OUTPUT3, 'X', 505, '1', TBoundaryCell::Z },
 
        //BSC group 169 for unused pad
       {"507",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"508",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"509",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 170 for I/O pin G2
       {"510",  "IOG2", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"511",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"512",  "IOG2", TBoundaryCell::OUTPUT3, 'X', 511, '1', TBoundaryCell::Z },

        //BSC group 171 for I/O pin G3
       {"513",  "IOG3", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"514",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"515",  "IOG3", TBoundaryCell::OUTPUT3, 'X', 514, '1', TBoundaryCell::Z },
 
        //BSC group 172 for I/O pin G4
       {"516",  "IOG4", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"517",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"518",  "IOG4", TBoundaryCell::OUTPUT3, 'X', 517, '1', TBoundaryCell::Z },
 
        //BSC group 173 for I/O pin G5
       {"519",  "IOG5", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"520",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"521",  "IOG5", TBoundaryCell::OUTPUT3, 'X', 520, '1', TBoundaryCell::Z },
 
        //BSC group 174 for unused pad
       {"522",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"523",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"524",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

        //BSC group 175 for I/O pin F2
       {"525",  "IOF2", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"526",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"527",  "IOF2", TBoundaryCell::OUTPUT3, 'X', 526, '1', TBoundaryCell::Z },
 
        //BSC group 176 for I/O pin F1
       {"528",  "IOF1", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"529",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"530",  "IOF1", TBoundaryCell::OUTPUT3, 'X', 529, '1', TBoundaryCell::Z },
 
        //BSC group 177 for I/O pin E1
       {"531",  "IOE1", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"532",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"533",  "IOE1", TBoundaryCell::OUTPUT3, 'X', 532, '1', TBoundaryCell::Z },
 
        //BSC group 178 for I/O pin F3
       {"534",  "IOF3", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"535",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"536",  "IOF3", TBoundaryCell::OUTPUT3, 'X', 535, '1', TBoundaryCell::Z },

        //BSC group 179 for unused pad
       {"537",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"538",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"539",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 180 for I/O pin E2
       {"540",  "IOE2", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"541",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"542",  "IOE2", TBoundaryCell::OUTPUT3, 'X', 541, '1', TBoundaryCell::Z },
 
        //BSC group 181 for I/O pin F4
       {"543",  "IOF4", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"544",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"545",  "IOF4", TBoundaryCell::OUTPUT3, 'X', 544, '1', TBoundaryCell::Z },
 
        //BSC group 182 for I/O pin E3
       {"546",  "IOE3", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"547",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"548",  "IOE3", TBoundaryCell::OUTPUT3, 'X', 547, '1', TBoundaryCell::Z },

        //BSC group 183 for I/O pin D1
       {"549",  "IOD1", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"550",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"551",  "IOD1", TBoundaryCell::OUTPUT3, 'X', 550, '1', TBoundaryCell::Z },
 
        //BSC group 184 for unused pad
       {"552",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"553",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"554",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 185 for I/O pin D2
       {"555",  "IOD2", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"556",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"557",  "IOD2", TBoundaryCell::OUTPUT3, 'X', 556, '1', TBoundaryCell::Z },
 
        //BSC group 186 for I/O pin D3
       {"558",  "IOD3", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"559",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"560",  "IOD3", TBoundaryCell::OUTPUT3, 'X', 559, '1', TBoundaryCell::Z },

        //BSC group 187 for I/O pin C1
       {"561",  "IOC1", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"562",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"563",  "IOC1", TBoundaryCell::OUTPUT3, 'X', 562, '1', TBoundaryCell::Z },
 
        //BSC group 188 for untestable Family-specific pin
       {"564",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"565",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"566",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 189 for Family-specific input pin B2
       {"567",  "DCLK", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"568",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"569",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 190 for Family-specific input pin A1
       {"570",  "DATA0", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"571",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"572",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 191 for unused pad
       {"573",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"574",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"575",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 192 for I/O pin B3
       {"576",  "IOB3", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"577",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"578",  "IOB3", TBoundaryCell::OUTPUT3, 'X', 577, '1', TBoundaryCell::Z },
 
        //BSC group 193 for unused pad
       {"579",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"580",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"581",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 194 for I/O pin A2
       {"582",  "IOA2", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"583",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"584",  "IOA2", TBoundaryCell::OUTPUT3, 'X', 583, '1', TBoundaryCell::Z },
 
        //BSC group 195 for unused pad
       {"585",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"586",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"587",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 196 for I/O pin C3
       {"588",  "IOC3", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"589",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"590",  "IOC3", TBoundaryCell::OUTPUT3, 'X', 589, '1', TBoundaryCell::Z },
 
        //BSC group 197 for unused pad
       {"591",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"592",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"593",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 198 for I/O pin B4
       {"594",  "IOB4", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"595",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"596",  "IOB4", TBoundaryCell::OUTPUT3, 'X', 595, '1', TBoundaryCell::Z },
 
        //BSC group 199 for unused pad
       {"597",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"598",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"599",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 200 for I/O pin A4
       {"600",  "IOA4", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"601",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"602",  "IOA4", TBoundaryCell::OUTPUT3, 'X', 601, '1', TBoundaryCell::Z },
 
        //BSC group 201 for I/O pin C4
       {"603",  "IOC4", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"604",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"605",  "IOC4", TBoundaryCell::OUTPUT3, 'X', 604, '1', TBoundaryCell::Z },
 
        //BSC group 202 for I/O pin D4
       {"606",  "IOD4", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"607",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"608",  "IOD4", TBoundaryCell::OUTPUT3, 'X', 607, '1', TBoundaryCell::Z },
 
        //BSC group 203 for unused pad
       {"609",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"610",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"611",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 204 for I/O pin B5
       {"612",  "IOB5", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"613",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"614",  "IOB5", TBoundaryCell::OUTPUT3, 'X', 613, '1', TBoundaryCell::Z },
 
        //BSC group 205 for unused pad
       {"615",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"616",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"617",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 206 for I/O pin A5
       {"618",  "IOA5", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"619",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"620",  "IOA5", TBoundaryCell::OUTPUT3, 'X', 619, '1', TBoundaryCell::Z },
 
        //BSC group 207 for unused pad
       {"621",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"622",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"623",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 208 for I/O pin C5
       {"624",  "IOC5", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"625",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"626",  "IOC5", TBoundaryCell::OUTPUT3, 'X', 625, '1', TBoundaryCell::Z },
 
        //BSC group 209 for unused pad
       {"627",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"628",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"629",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 210 for I/O pin D5
       {"630",  "IOD5", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"631",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"632",  "IOD5", TBoundaryCell::OUTPUT3, 'X', 631, '1', TBoundaryCell::Z },
 
        //BSC group 211 for I/O pin E4
       {"633",  "IOE4", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"634",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"635",  "IOE4", TBoundaryCell::OUTPUT3, 'X', 634, '1', TBoundaryCell::Z },
 
        //BSC group 212 for I/O pin B6
       {"636",  "IOB6", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"637",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"638",  "IOB6", TBoundaryCell::OUTPUT3, 'X', 637, '1', TBoundaryCell::Z },
 
        //BSC group 213 for I/O pin A6
       {"639",  "IOA6", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"640",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"641",  "IOA6", TBoundaryCell::OUTPUT3, 'X', 640, '1', TBoundaryCell::Z },
 
        //BSC group 214 for I/O pin C6
       {"642",  "IOC6", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"643",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"644",  "IOC6", TBoundaryCell::OUTPUT3, 'X', 643, '1', TBoundaryCell::Z },
 
        //BSC group 215 for unused pad
       {"645",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"646",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"647",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 216 for I/O pin D6
       {"648",  "IOD6", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"649",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"650",  "IOD6", TBoundaryCell::OUTPUT3, 'X', 649, '1', TBoundaryCell::Z },
 
        //BSC group 217 for unused pad
       {"651",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"652",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"653",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 218 for I/O pin B7
       {"654",  "IOB7", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"655",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"656",  "IOB7", TBoundaryCell::OUTPUT3, 'X', 655, '1', TBoundaryCell::Z },
 
        //BSC group 219 for I/O pin A7
       {"657",  "IOA7", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"658",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"659",  "IOA7", TBoundaryCell::OUTPUT3, 'X', 658, '1', TBoundaryCell::Z },
 
        //BSC group 220 for I/O pin D7
       {"660",  "IOD7", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"661",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"662",  "IOD7", TBoundaryCell::OUTPUT3, 'X', 661, '1', TBoundaryCell::Z },
 
        //BSC group 221 for unused pad
       {"663",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"664",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"665",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 222 for I/O pin E7
       {"666",  "IOE7", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"667",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"668",  "IOE7", TBoundaryCell::OUTPUT3, 'X', 667, '1', TBoundaryCell::Z },
 
        //BSC group 223 for I/O pin C8
       {"669",  "IOC8", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"670",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"671",  "IOC8", TBoundaryCell::OUTPUT3, 'X', 670, '1', TBoundaryCell::Z },
 
        //BSC group 224 for I/O pin B8
       {"672",  "IOB8", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"673",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"674",  "IOB8", TBoundaryCell::OUTPUT3, 'X', 673, '1', TBoundaryCell::Z },
 
        //BSC group 225 for I/O pin A8
       {"675",  "IOA8", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"676",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"677",  "IOA8", TBoundaryCell::OUTPUT3, 'X', 676, '1', TBoundaryCell::Z },
 
        //BSC group 226 for I/O pin D8
       {"678",  "IOD8", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"679",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"680",  "IOD8", TBoundaryCell::OUTPUT3, 'X', 679, '1', TBoundaryCell::Z },
 
        //BSC group 227 for dedicated input pin E8
       {"681",  "INE8", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"682",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"683",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 228 for dedicated clock pin A9
       {"684",  "CLKA9", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"685",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"686",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 229 for dedicated input pin B9
       {"687",  "INB9", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"688",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"689",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 230 for unused pad
       {"690",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"691",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"692",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 231 for I/O pin C9
       {"693",  "IOC9", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"694",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"695",  "IOC9", TBoundaryCell::OUTPUT3, 'X', 694, '1', TBoundaryCell::Z },
 
        //BSC group 232 for I/O pin D9
       {"696",  "IOD9", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"697",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"698",  "IOD9", TBoundaryCell::OUTPUT3, 'X', 697, '1', TBoundaryCell::Z },
 
        //BSC group 233 for I/O pin E9
       {"699",  "IOE9", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"700",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"701",  "IOE9", TBoundaryCell::OUTPUT3, 'X', 700, '1', TBoundaryCell::Z },
 
        //BSC group 234 for unused pad
       {"702",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"703",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"704",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 235 for I/O pin A10
       {"705",  "IOA10", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"706",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"707",  "IOA10", TBoundaryCell::OUTPUT3, 'X', 706, '1', TBoundaryCell::Z },
 
        //BSC group 236 for I/O pin B10
       {"708",  "IOB10", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"709",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"710",  "IOB10", TBoundaryCell::OUTPUT3, 'X', 709, '1', TBoundaryCell::Z },
 
        //BSC group 237 for I/O pin C10
       {"711",  "IOC10", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"712",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"713",  "IOC10", TBoundaryCell::OUTPUT3, 'X', 712, '1', TBoundaryCell::Z },
 
        //BSC group 238 for I/O pin D10
       {"714",  "IOD10", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"715",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"716",  "IOD10", TBoundaryCell::OUTPUT3, 'X', 715, '1', TBoundaryCell::Z },
 
        //BSC group 239 for I/O pin E10
       {"717",  "IOE10", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"718",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"719",  "IOE10", TBoundaryCell::OUTPUT3, 'X', 718, '1', TBoundaryCell::Z },
 
        //BSC group 240 for unused pad
       {"720",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"721",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"722",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 241 for I/O pin A11
       {"723",  "IOA11", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"724",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"725",  "IOA11", TBoundaryCell::OUTPUT3, 'X', 724, '1', TBoundaryCell::Z },
 
        //BSC group 242 for unused pad
       {"726",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"727",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"728",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 243 for I/O pin B11
       {"729",  "IOB11", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"730",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"731",  "IOB11", TBoundaryCell::OUTPUT3, 'X', 730, '1', TBoundaryCell::Z },
 
        //BSC group 244 for I/O pin C11
       {"732",  "IOC11", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"733",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"734",  "IOC11", TBoundaryCell::OUTPUT3, 'X', 733, '1', TBoundaryCell::Z },
 
        //BSC group 245 for I/O pin D11
       {"735",  "IOD11", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"736",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"737",  "IOD11", TBoundaryCell::OUTPUT3, 'X', 736, '1', TBoundaryCell::Z },
 
        //BSC group 246 for I/O pin C12
       {"738",  "IOC12", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"739",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"740",  "IOC12", TBoundaryCell::OUTPUT3, 'X', 739, '1', TBoundaryCell::Z },
 
        //BSC group 247 for I/O pin B12
       {"741",  "IOB12", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"742",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"743",  "IOB12", TBoundaryCell::OUTPUT3, 'X', 742, '1', TBoundaryCell::Z },
 
        //BSC group 248 for unused pad
       {"744",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"745",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"746",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 249 for I/O pin A12
       {"747",  "IOA12", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"748",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"749",  "IOA12", TBoundaryCell::OUTPUT3, 'X', 748, '1', TBoundaryCell::Z },
 
        //BSC group 250 for unused pad
       {"750",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"751",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"752",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 251 for I/O pin D13
       {"753",  "IOD13", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"754",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"755",  "IOD13", TBoundaryCell::OUTPUT3, 'X', 754, '1', TBoundaryCell::Z },
 
        //BSC group 252 for unused pad
       {"756",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"757",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"758",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 253 for I/O pin C13
       {"759",  "IOC13", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"760",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"761",  "IOC13", TBoundaryCell::OUTPUT3, 'X', 760, '1', TBoundaryCell::Z },
 
        //BSC group 254 for unused pad
       {"762",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"763",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"764",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 255 for I/O pin D14
       {"765",  "IOD14", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"766",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"767",  "IOD14", TBoundaryCell::OUTPUT3, 'X', 766, '1', TBoundaryCell::Z },
 
        //BSC group 256 for I/O pin B13
       {"768",  "IOB13", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"769",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"770",  "IOB13", TBoundaryCell::OUTPUT3, 'X', 769, '1', TBoundaryCell::Z },
 
        //BSC group 257 for I/O pin A13
       {"771",  "IOA13", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"772",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"773",  "IOA13", TBoundaryCell::OUTPUT3, 'X', 772, '1', TBoundaryCell::Z },
 
        //BSC group 258 for unused pad
       {"774",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"775",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"776",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 259 for I/O pin C14
       {"777",  "IOC14", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"778",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"779",  "IOC14", TBoundaryCell::OUTPUT3, 'X', 778, '1', TBoundaryCell::Z },
 
        //BSC group 260 for unused pad
       {"780",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"781",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"782",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 261 for I/O pin B14
       {"783",  "IOB14", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"784",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"785",  "IOB14", TBoundaryCell::OUTPUT3, 'X', 784, '1', TBoundaryCell::Z },
 
        //BSC group 262 for unused pad
       {"786",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"787",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"788",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 263 for I/O pin A15
       {"789",  "IOA15", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"790",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"791",  "IOA15", TBoundaryCell::OUTPUT3, 'X', 790, '1', TBoundaryCell::Z },
 
        //BSC group 264 for unused pad
       {"792",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"793",  "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"794",  "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 265 for I/O pin A16
       {"795",  "IOA16", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"796",  "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"797",  "IOA16", TBoundaryCell::OUTPUT3, 'X', 796, '1', TBoundaryCell::Z }
    };

    //TBoundaryRegister*  breg = new TBoundaryRegister(sizeof(b_reg_tab)/sizeof(pin_info));

    for( i=(sizeof(b_reg_tab)/sizeof(pin_info))-1; i>=0; i-- )
    //for( i=0; i < (sizeof(b_reg_tab)/sizeof(pin_info)); i++ )
      /*breg->*/AddCell( *new TBoundaryCell( //b_reg_tab[i].name,
                   atoi(b_reg_tab[i].name),
                   TBoundaryCell::BC_1,
                   b_reg_tab[i].port,
                   b_reg_tab[i].function,
                   b_reg_tab[i].safe,
                   b_reg_tab[i].ccell,
                   b_reg_tab[i].disval,
                   b_reg_tab[i].rslt       ) );
    //breg->ScanBoundaryCells();


    /// pin mapping
    struct pin_map_el {
      char* sig;
      char* pin;
    };

    pin_map_el pin_map[] = {

      // I/O Pins      {"IOC1"   , "C1" } , {"IOD3"   , "D3" } , {"IOD2"   , "D2" } , {"IOD1"   , "D1" } ,      {"IOE3"   , "E3" } , {"IOF4"   , "F4" } , {"IOE2"   , "E2" } , {"IOF3"   , "F3" } ,      {"IOE1"   , "E1" } , {"IOF1"   , "F1" } , {"IOF2"   , "F2" } , {"IOG5"   , "G5" } ,      {"IOG4"   , "G4" } , {"IOG3"   , "G3" } , {"IOG2"   , "G2" } , {"IOG1"   , "G1" } ,      {"IOH5"   , "H5" } , {"IOH4"   , "H4" } , {"IOH3"   , "H3" } , {"IOH2"   , "H2" } ,      {"IOH1"   , "H1" } , {"IOJ5"   , "J5" } , {"IOJ4"   , "J4" } , {"IOJ3"   , "J3" } ,      {"IOJ1"   , "J1" } , {"IOJ2"   , "J2" } , {"IOK4"   , "K4" } , {"IOK3"   , "K3" } ,      {"IOK1"   , "K1" } , {"IOK2"   , "K2" } , {"IOL3"   , "L3" } , {"IOL2"   , "L2" } ,      {"IOL4"   , "L4" } , {"IOM1"   , "M1" } , {"IOM4"   , "M4" } , {"IOM2"   , "M2" } ,      {"IOM3"   , "M3" } , {"ION3"   , "N3" } , {"ION1"   , "N1" } , {"ION2"   , "N2" } ,      {"IOP2"   , "P2" } , {"IOT1"   , "T1" } , {"IOT2"   , "T2" } , {"IOT3"   , "T3" } ,      {"IOR3"   , "R3" } , {"IOP3"   , "P3" } , {"IOP4"   , "P4" } , {"IOR4"   , "R4" } ,      {"IOT4"   , "T4" } , {"ION5"   , "N5" } , {"IOP5"   , "P5" } , {"IOR5"   , "R5" } ,      {"IOT5"   , "T5" } , {"ION6"   , "N6" } , {"IOP6"   , "P6" } , {"IOR6"   , "R6" } ,      {"IOT6"   , "T6" } , {"IOM7"   , "M7" } , {"ION7"   , "N7" } , {"IOP7"   , "P7" } ,      {"IOR7"   , "R7" } , {"IOT7"   , "T7" } , {"IOM8"   , "M8" } , {"ION8"   , "N8" } ,      {"IOP8"   , "P8" } , {"IOT9"   , "T9" } , {"IOR9"   , "R9" } , {"IOP9"   , "P9" } ,      {"ION9"   , "N9" } , {"IOR10"  , "R10"} , {"IOT10"  , "T10"} , {"IOP10"  , "P10"} ,      {"ION10"  , "N10"} , {"IOM10"  , "M10"} , {"ION11"  , "N11"} , {"IOP11"  , "P11"} ,      {"IOT11"  , "T11"} , {"IOR11"  , "R11"} , {"IOR12"  , "R12"} , {"IOP12"  , "P12"} ,      {"IOT12"  , "T12"} , {"ION13"  , "N13"} , {"IOP13"  , "P13"} , {"IOT13"  , "T13"} ,      {"IOR13"  , "R13"} , {"IOM13"  , "M13"} , {"IOT14"  , "T14"} , {"IOT15"  , "T15"} ,      {"IOR14"  , "R14"} , {"IOR15"  , "R15"} , {"IOP16"  , "P16"} , {"ION16"  , "N16"} ,      {"ION15"  , "N15"} , {"IOP14"  , "P14"} , {"IOM15"  , "M15"} , {"IOM16"  , "M16"} ,      {"ION14"  , "N14"} , {"IOM14"  , "M14"} , {"IOL13"  , "L13"} , {"IOL16"  , "L16"} ,      {"IOL15"  , "L15"} , {"IOL14"  , "L14"} , {"IOK14"  , "K14"} , {"IOK15"  , "K15"} ,      {"IOK16"  , "K16"} , {"IOK13"  , "K13"} , {"IOK12"  , "K12"} , {"IOJ15"  , "J15"} ,      {"IOJ16"  , "J16"} , {"IOJ14"  , "J14"} , {"IOJ13"  , "J13"} , {"IOJ12"  , "J12"} ,      {"IOH15"  , "H15"} , {"IOH16"  , "H16"} , {"IOH14"  , "H14"} , {"IOH13"  , "H13"} ,      {"IOG16"  , "G16"} , {"IOG15"  , "G15"} , {"IOH12"  , "H12"} , {"IOG14"  , "G14"} ,      {"IOF16"  , "F16"} , {"IOF15"  , "F15"} , {"IOG13"  , "G13"} , {"IOG12"  , "G12"} ,      {"IOF14"  , "F14"} , {"IOE16"  , "E16"} , {"IOE15"  , "E15"} , {"IOF13"  , "F13"} ,      {"IOD15"  , "D15"} , {"IOD16"  , "D16"} , {"IOE14"  , "E14"} , {"IOE13"  , "E13"} ,      {"IOA16"  , "A16"} , {"IOA15"  , "A15"} , {"IOB14"  , "B14"} , {"IOC14"  , "C14"} ,      {"IOA13"  , "A13"} , {"IOB13"  , "B13"} , {"IOD14"  , "D14"} , {"IOC13"  , "C13"} ,      {"IOD13"  , "D13"} , {"IOA12"  , "A12"} , {"IOB12"  , "B12"} , {"IOC12"  , "C12"} ,      {"IOD11"  , "D11"} , {"IOC11"  , "C11"} , {"IOB11"  , "B11"} , {"IOA11"  , "A11"} ,      {"IOE10"  , "E10"} , {"IOD10"  , "D10"} , {"IOC10"  , "C10"} , {"IOB10"  , "B10"} ,      {"IOA10"  , "A10"} , {"IOE9"   , "E9" } , {"IOD9"   , "D9" } , {"IOC9"   , "C9" } ,      {"IOD8"   , "D8" } , {"IOA8"   , "A8" } , {"IOB8"   , "B8" } , {"IOC8"   , "C8" } ,      {"IOE7"   , "E7" } , {"IOD7"   , "D7" } , {"IOA7"   , "A7" } , {"IOB7"   , "B7" } ,      {"IOD6"   , "D6" } , {"IOC6"   , "C6" } , {"IOA6"   , "A6" } , {"IOB6"   , "B6" } ,      {"IOE4"   , "E4" } , {"IOD5"   , "D5" } , {"IOC5"   , "C5" } , {"IOA5"   , "A5" } ,      {"IOB5"   , "B5" } , {"IOD4"   , "D4" } , {"IOC4"   , "C4" } , {"IOA4"   , "A4" } ,      {"IOB4"   , "B4" } , {"IOC3"   , "C3" } , {"IOA2"   , "A2" } , {"IOB3"   , "B3" } ,      //Dedicated Input Pins      {"INR8"   , "R8" } , {"INM9"   , "M9" } , {"INB9"   , "B9" } , {"INE8"   , "E8" } ,      //Dedicated Clock Pins      {"CLKL8"  , "L8" } , {"CLKA9"  , "A9" } ,      //ACEX 1K Configuration Pins      {"DATA0"  , "A1" } , {"DCLK"   , "B2" } , {"MSEL0" , "P1" }  , {"MSEL1"  , "R1" } ,      {"CONF_DONE","C15"}, {"NCE"    , "B1" } , {"NCEO"  , "B16" } , {"NCONFIG", "N4" } ,      {"NSTATUS", "T16"} ,      //JTAG ports      {"TCK"   , "B15" } , {"TMS"   , "P15" } , {"TDI"   , "C2" } , {"TRST"   , "R16" } ,      {"TDO"   , "C16" }      /*      //Power Pins      "VCC    : (F5  , G6  , H6  , J6  , H7  , J7  , K6  , L5  , "                "R2  , M6  , K8  , L9  , K9  , L10 , N12 , M11 , "                "J10 , K11 , L12 , H10 , H11 , F12 , E11 , D12 , "                "F10 , F9  , F8  , E6  , G11 , G8  , F7  , J11 , "                "L7  ), "      //Ground Pins      "GND    : (F6  , G7  , H8  , J8  , K7  , L6  , M5  , T8  , "                "L11 , J9  , K10 , H9  , G10 , F11 , E12 , G9  , "                "E5  , M12 , K5  , L1  , A14 , C7  , A3  )"*/

    };

    for( i=sizeof(pin_map)/sizeof(pin_map_el)-1; i>=0; i-- )
      /*breg->*/AddPinMapping(pin_map[i].sig, pin_map[i].pin);
    //AddRegister( *breg );

                        
  //////////////////////////////////////////
  // usercode register
  AddRegister( *new TRegister(USERCODE_REG_NAME, 32) );

  InitEnd();

}
