#ifndef TALTERA2_HH
#define TALTERA2_HH

#include <typeinfo>
#include <stdio.h>
#include "TChip.h"

namespace bs {

class TAltera2 : public TChip {
public:
    TAltera2(const std::string& name );  
    virtual ~TAltera2() {}
};



} // namespace bs


#endif




