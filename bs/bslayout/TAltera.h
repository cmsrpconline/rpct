#ifndef TALTERA_HH
#define TALTERA_HH

#include <typeinfo>
#include <stdio.h>
#include "TChip.h"

namespace bs {

class TAltera : public TChip {
public:
    TAltera( char* name );
};

} // namespace bs


#endif




