#include "TGOL1.h"

#include <algorithm>
using namespace bs;



class TConfRegister: public TFragmentedRegister {
private:        
  TRegisterFragment::TCells CreateCells(int from, int to) // 0,4 daje 0123, 4,0 daje 3210
  {
    TRegisterFragment::TCells cells;
    if (from < to)
      for (; from < to; from++)
        cells.push_back(from);
    else
      for (from--; from >= to; --from)
        cells.push_back(from);

    return cells;
  }

public:
  TConfRegister();
  virtual ~TConfRegister() {}
};



TConfRegister::TConfRegister()
  : TFragmentedRegister("CONF", 55)
{

  /*int from=0, to=0;
  AddFragment( *new TRegisterFragment("hamming", *this, CreateCells(from, to+=7)) );  from=to;
  AddFragment( *new TRegisterFragment("config3", *this, CreateCells(from, to+=8)) );  from=to;
  AddFragment( *new TRegisterFragment("config2", *this, CreateCells(from, to+=8)) );  from=to;
  AddFragment( *new TRegisterFragment("config1", *this, CreateCells(from, to+=8)) );  from=to;
  AddFragment( *new TRegisterFragment("config0", *this, CreateCells(from, to+=8)) );  from=to;
  AddFragment( *new TRegisterFragment("status1", *this, CreateCells(from, to+=8)) );  from=to;
  AddFragment( *new TRegisterFragment("status0", *this, CreateCells(from, to+=8)) );  from=to;

  if (from != Length)
    throw TException("TConfRegister::TConfRegister: from != 0"); */
    
  int from=0, to=0;
  AddFragment( *new TRegisterFragment("status0", *this, CreateCells(to+=8, from)) );  from=to;
  AddFragment( *new TRegisterFragment("status1", *this, CreateCells(to+=8, from)) );  from=to;
  AddFragment( *new TRegisterFragment("config0", *this, CreateCells(to+=8, from)) );  from=to;
  AddFragment( *new TRegisterFragment("config1", *this, CreateCells(to+=8, from)) );  from=to;
  AddFragment( *new TRegisterFragment("config2", *this, CreateCells(to+=8, from)) );  from=to;
  AddFragment( *new TRegisterFragment("config3", *this, CreateCells(to+=8, from)) );  from=to;
  AddFragment( *new TRegisterFragment("hamming", *this, CreateCells(to+=7, from)) );  from=to;

  if (from != Length)
    throw TException("TConfRegister::TConfRegister: from != 0");
}



TGOL1::TGOL1(const std::string& name )
    : TChip( name, "01010", 57, "00010100010100110101000001001001" )
{
    int i,j;

    const char* CONF_REG_NAME = "CONF";
    const char* CORETEST_REG_NAME = "CORETEST";

    // instrukcje
    // trzeba jeszcze dodac capture values
    const std::string inst_tab [][3] = {
      { "BYPASS"  ,  TRegister::BYPASS_REG_NAME,   "11111" },
      { "EXTEST"  ,  TRegister::BOUNDARY_REG_NAME, "00000" },
      { "SAMPLE"  ,  TRegister::BOUNDARY_REG_NAME, "01000" },
      { "IDCODE"  ,  TRegister::IDCODE_REG_NAME,   "10000" },
      { "CONF_RW" ,               CONF_REG_NAME,   "10010" },
      { "CONF_R"  ,               CONF_REG_NAME,   "01010" },
   //   { "CORETEST",           CORETEST_REG_NAME,   "01011" } 
    };

    for( i=0; i < sizeof(inst_tab) / sizeof(inst_tab[0]); i++ ) {
	    AddInstruction( *new TInstruction(inst_tab[i][0], inst_tab[i][1], inst_tab[i][2]) );
    }
    

    // rejestry
    TRegister* reg;


    // boundary register
    
    struct pin_info {
      char* name;
      char* port;
      TBoundaryCell::TFunction function;
      char safe;
      int ccell;    //-1 jak nie ma
      char disval;
      TBoundaryCell::TDisResult rslt;
    };

    pin_info  b_reg_tab[]= {
       {"0",    "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"1",    "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"2",    "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

        //BSC group 1 for untestable Family-specific pin
       {"3",    "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"4",    "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"5",    "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

        //BSC group 2 for I/O pin E13
       {"6",    "IOE13", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"7",    "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"8",    "IOE13", TBoundaryCell::OUTPUT3, 'X', 7, '1', TBoundaryCell::Z },

        //BSC group 3 for unused pad
       {"9",    "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"10",   "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"11",   "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

        //BSC group 4 for I/O pin E14
       {"12",   "IOE14", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"13",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"14",   "IOE14", TBoundaryCell::OUTPUT3, 'X', 13, '1', TBoundaryCell::Z },
 
        //BSC group 5 for I/O pin D16
       {"15",   "IOD16", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"16",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"17",   "IOD16", TBoundaryCell::OUTPUT3, 'X', 16, '1', TBoundaryCell::Z },
 
        //BSC group 6 for I/O pin D15
       {"18",   "IOD15", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"19",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"20",   "IOD15", TBoundaryCell::OUTPUT3, 'X', 19, '1', TBoundaryCell::Z },

        //BSC group 7 for I/O pin F13
       {"21",   "IOF13", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"22",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"23",   "IOF13", TBoundaryCell::OUTPUT3, 'X', 22, '1', TBoundaryCell::Z },
 
        //BSC group 8 for unused pad
       {"24",   "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"25",   "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"26",   "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 9 for I/O pin E15
       {"27",   "IOE15", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"28",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"29",   "IOE15", TBoundaryCell::OUTPUT3, 'X', 28, '1', TBoundaryCell::Z },
 
        //BSC group 10 for unused pad
       {"30",   "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"31",   "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"32",   "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },

        //BSC group 11 for I/O pin E16
       {"33",   "IOE16", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"34",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"35",   "IOE16", TBoundaryCell::OUTPUT3, 'X', 34, '1', TBoundaryCell::Z },
 
        //BSC group 12 for I/O pin F14
       {"36",   "IOF14", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"37",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"38",   "IOF14", TBoundaryCell::OUTPUT3, 'X', 37, '1', TBoundaryCell::Z },
 
        //BSC group 13 for I/O pin G12
       {"39",   "IOG12", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"40",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"41",   "IOG12", TBoundaryCell::OUTPUT3, 'X', 40, '1', TBoundaryCell::Z },
 
        //BSC group 14 for I/O pin G13
       {"42",   "IOG13", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"43",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"44",   "IOG13", TBoundaryCell::OUTPUT3, 'X', 43, '1', TBoundaryCell::Z },

        //BSC group 15 for unused pad
       {"45",   "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"46",   "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"47",   "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 16 for I/O pin F15
       {"48",   "IOF15", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"49",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"50",   "IOF15", TBoundaryCell::OUTPUT3, 'X', 49, '1', TBoundaryCell::Z },
 
        //BSC group 17 for unused pad
       {"51",   "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
       {"52",   "*", TBoundaryCell::INTERNAL, '1', -1, '0', TBoundaryCell::Z },
       {"53",   "*", TBoundaryCell::INTERNAL, 'X', -1, '0', TBoundaryCell::Z },
 
        //BSC group 18 for I/O pin F16
       {"54",   "IOF16", TBoundaryCell::INPUT, 'X', -1, '0', TBoundaryCell::Z },
       {"55",   "*", TBoundaryCell::CONTROL, '1', -1, '0', TBoundaryCell::Z },
       {"56",   "IOF16", TBoundaryCell::OUTPUT3, 'X', 55, '1', TBoundaryCell::Z }
    };

    for( i=(sizeof(b_reg_tab)/sizeof(pin_info))-1; i>=0; i-- )
      AddCell( *new TBoundaryCell( 
                   atoi(b_reg_tab[i].name),
                   TBoundaryCell::BC_1,
                   b_reg_tab[i].port,
                   b_reg_tab[i].function,
                   b_reg_tab[i].safe,
                   b_reg_tab[i].ccell,
                   b_reg_tab[i].disval,
                   b_reg_tab[i].rslt       ) );

    /*
    /// pin mapping
    struct pin_map_el {
      char* sig;
      char* pin;
    };

    pin_map_el pin_map[] = {

      // I/O Pins      {"IOC1"   , "C1" } , {"IOD3"   , "D3" } , {"IOD2"   , "D2" } , {"IOD1"   , "D1" } ,      {"IOE3"   , "E3" } , {"IOF4"   , "F4" } , {"IOE2"   , "E2" } , {"IOF3"   , "F3" } ,      {"IOE1"   , "E1" } , {"IOF1"   , "F1" } , {"IOF2"   , "F2" } , {"IOG5"   , "G5" } ,      {"IOG4"   , "G4" } , {"IOG3"   , "G3" } , {"IOG2"   , "G2" } , {"IOG1"   , "G1" } ,      {"IOH5"   , "H5" } , {"IOH4"   , "H4" } , {"IOH3"   , "H3" } , {"IOH2"   , "H2" } ,      {"IOH1"   , "H1" } , {"IOJ5"   , "J5" } , {"IOJ4"   , "J4" } , {"IOJ3"   , "J3" } ,      {"IOJ1"   , "J1" } , {"IOJ2"   , "J2" } , {"IOK4"   , "K4" } , {"IOK3"   , "K3" } ,      {"IOK1"   , "K1" } , {"IOK2"   , "K2" } , {"IOL3"   , "L3" } , {"IOL2"   , "L2" } ,      {"IOL4"   , "L4" } , {"IOM1"   , "M1" } , {"IOM4"   , "M4" } , {"IOM2"   , "M2" } ,      {"IOM3"   , "M3" } , {"ION3"   , "N3" } , {"ION1"   , "N1" } , {"ION2"   , "N2" } ,      {"IOP2"   , "P2" } , {"IOT1"   , "T1" } , {"IOT2"   , "T2" } , {"IOT3"   , "T3" } ,      {"IOR3"   , "R3" } , {"IOP3"   , "P3" } , {"IOP4"   , "P4" } , {"IOR4"   , "R4" } ,      {"IOT4"   , "T4" } , {"ION5"   , "N5" } , {"IOP5"   , "P5" } , {"IOR5"   , "R5" } ,      {"IOT5"   , "T5" } , {"ION6"   , "N6" } , {"IOP6"   , "P6" } , {"IOR6"   , "R6" } ,      {"IOT6"   , "T6" } , {"IOM7"   , "M7" } , {"ION7"   , "N7" } , {"IOP7"   , "P7" } ,      {"IOR7"   , "R7" } , {"IOT7"   , "T7" } , {"IOM8"   , "M8" } , {"ION8"   , "N8" } ,      {"IOP8"   , "P8" } , {"IOT9"   , "T9" } , {"IOR9"   , "R9" } , {"IOP9"   , "P9" } ,      {"ION9"   , "N9" } , {"IOR10"  , "R10"} , {"IOT10"  , "T10"} , {"IOP10"  , "P10"} ,      {"ION10"  , "N10"} , {"IOM10"  , "M10"} , {"ION11"  , "N11"} , {"IOP11"  , "P11"} ,      {"IOT11"  , "T11"} , {"IOR11"  , "R11"} , {"IOR12"  , "R12"} , {"IOP12"  , "P12"} ,      {"IOT12"  , "T12"} , {"ION13"  , "N13"} , {"IOP13"  , "P13"} , {"IOT13"  , "T13"} ,      {"IOR13"  , "R13"} , {"IOM13"  , "M13"} , {"IOT14"  , "T14"} , {"IOT15"  , "T15"} ,      {"IOR14"  , "R14"} , {"IOR15"  , "R15"} , {"IOP16"  , "P16"} , {"ION16"  , "N16"} ,      {"ION15"  , "N15"} , {"IOP14"  , "P14"} , {"IOM15"  , "M15"} , {"IOM16"  , "M16"} ,      {"ION14"  , "N14"} , {"IOM14"  , "M14"} , {"IOL13"  , "L13"} , {"IOL16"  , "L16"} ,      {"IOL15"  , "L15"} , {"IOL14"  , "L14"} , {"IOK14"  , "K14"} , {"IOK15"  , "K15"} ,      {"IOK16"  , "K16"} , {"IOK13"  , "K13"} , {"IOK12"  , "K12"} , {"IOJ15"  , "J15"} ,      {"IOJ16"  , "J16"} , {"IOJ14"  , "J14"} , {"IOJ13"  , "J13"} , {"IOJ12"  , "J12"} ,      {"IOH15"  , "H15"} , {"IOH16"  , "H16"} , {"IOH14"  , "H14"} , {"IOH13"  , "H13"} ,      {"IOG16"  , "G16"} , {"IOG15"  , "G15"} , {"IOH12"  , "H12"} , {"IOG14"  , "G14"} ,      {"IOF16"  , "F16"} , {"IOF15"  , "F15"} , {"IOG13"  , "G13"} , {"IOG12"  , "G12"} ,      {"IOF14"  , "F14"} , {"IOE16"  , "E16"} , {"IOE15"  , "E15"} , {"IOF13"  , "F13"} ,      {"IOD15"  , "D15"} , {"IOD16"  , "D16"} , {"IOE14"  , "E14"} , {"IOE13"  , "E13"} ,      {"IOA16"  , "A16"} , {"IOA15"  , "A15"} , {"IOB14"  , "B14"} , {"IOC14"  , "C14"} ,      {"IOA13"  , "A13"} , {"IOB13"  , "B13"} , {"IOD14"  , "D14"} , {"IOC13"  , "C13"} ,      {"IOD13"  , "D13"} , {"IOA12"  , "A12"} , {"IOB12"  , "B12"} , {"IOC12"  , "C12"} ,      {"IOD11"  , "D11"} , {"IOC11"  , "C11"} , {"IOB11"  , "B11"} , {"IOA11"  , "A11"} ,      {"IOE10"  , "E10"} , {"IOD10"  , "D10"} , {"IOC10"  , "C10"} , {"IOB10"  , "B10"} ,      {"IOA10"  , "A10"} , {"IOE9"   , "E9" } , {"IOD9"   , "D9" } , {"IOC9"   , "C9" } ,      {"IOD8"   , "D8" } , {"IOA8"   , "A8" } , {"IOB8"   , "B8" } , {"IOC8"   , "C8" } ,      {"IOE7"   , "E7" } , {"IOD7"   , "D7" } , {"IOA7"   , "A7" } , {"IOB7"   , "B7" } ,      {"IOD6"   , "D6" } , {"IOC6"   , "C6" } , {"IOA6"   , "A6" } , {"IOB6"   , "B6" } ,      {"IOE4"   , "E4" } , {"IOD5"   , "D5" } , {"IOC5"   , "C5" } , {"IOA5"   , "A5" } ,      {"IOB5"   , "B5" } , {"IOD4"   , "D4" } , {"IOC4"   , "C4" } , {"IOA4"   , "A4" } ,      {"IOB4"   , "B4" } , {"IOC3"   , "C3" } , {"IOA2"   , "A2" } , {"IOB3"   , "B3" } ,      //Dedicated Input Pins      {"INR8"   , "R8" } , {"INM9"   , "M9" } , {"INB9"   , "B9" } , {"INE8"   , "E8" } ,      //Dedicated Clock Pins      {"CLKL8"  , "L8" } , {"CLKA9"  , "A9" } ,      //ACEX 1K Configuration Pins      {"DATA0"  , "A1" } , {"DCLK"   , "B2" } , {"MSEL0" , "P1" }  , {"MSEL1"  , "R1" } ,      {"CONF_DONE","C15"}, {"NCE"    , "B1" } , {"NCEO"  , "B16" } , {"NCONFIG", "N4" } ,      {"NSTATUS", "T16"} ,      //JTAG ports      {"TCK"   , "B15" } , {"TMS"   , "P15" } , {"TDI"   , "C2" } , {"TRST"   , "R16" } ,      {"TDO"   , "C16" }    };

    for( i=sizeof(pin_map)/sizeof(pin_map_el)-1; i>=0; i-- )
      AddPinMapping(pin_map[i].sig, pin_map[i].pin); */

                        
  //////////////////////////////////////////  
  AddRegister( *new TConfRegister );

  InitEnd();

}
