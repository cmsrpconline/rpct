#if !defined TASSOCARRREF_H
#define TASSOCARRREF_H

#include <string>
//#include "TException.hh"
#include "tb_std.h"

using namespace std;

class EAssociativeArrayRef : public TException {
public:
    EAssociativeArrayRef( string msg = "" )
	: TException( "TAssociativeArrayRef: " + msg ) {}
    __fastcall virtual ~EAssociativeArrayRef() {}
};


class EAssoArrRNotEnoughMemory : public EAssociativeArrayRef /*, std::bad_alloc*/ {
public:
    EAssoArrRNotEnoughMemory( string msg )
	: EAssociativeArrayRef( "Not enough memory " + msg ) {}
    __fastcall virtual ~EAssoArrRNotEnoughMemory() {}
};

class EAssoArrRNonExistentKey : public EAssociativeArrayRef {
public:
    EAssoArrRNonExistentKey( string key, string msg )
	: EAssociativeArrayRef( "Non existent key '" + key + "' " + msg ) {}
    __fastcall virtual ~EAssoArrRNonExistentKey() {}
};

class EAssoArrRIndexOutOfBounds : public EAssociativeArrayRef {
public:
    EAssoArrRIndexOutOfBounds(  string msg )
	: EAssociativeArrayRef( "Index out of bounds " + msg ) {}
    __fastcall virtual ~EAssoArrRIndexOutOfBounds() {}
};

class EAssoArrRAlreadyFull : public EAssociativeArrayRef {
public:
    EAssoArrRAlreadyFull( string msg )
	: EAssociativeArrayRef( "Array already full " + msg ) {}
    __fastcall virtual ~EAssoArrRAlreadyFull() {}
};

class EAssoArrRDuplicateKey : public EAssociativeArrayRef {
public:
    EAssoArrRDuplicateKey( string key, string msg )
	: EAssociativeArrayRef( "Duplicate key '" + key + " '" + msg ) {}
    __fastcall virtual ~EAssoArrRDuplicateKey() {}
};

      
template<class T>
class TAssociativeArrayRef;


template<class T>  
class TAssociation{
    
    friend class TAssociativeArrayRef<T>;
    char*	Key;
    T*		Value;
    TAssociation<T>*	Left;
    TAssociation<T>*	Right;
public:
    TAssociation() : Key(NULL), Value(NULL), Left(NULL), Right(NULL) {}
    TAssociation( const char* k ,  T* val ,TAssociation<T>* l = NULL,
		  TAssociation<T>* r = NULL  ) throw(EAssoArrRNotEnoughMemory) 
	: Value(val), Left(l), Right(r) {
	    try {
		Key = new char[ strlen(k) + 1 ];
	    }
	    catch(...) {
		throw EAssoArrRNotEnoughMemory("");
	    }
	    strcpy( Key, k );
    }
    TAssociation<T>& operator= (const TAssociation<T>& ass ) 
	/*throw(EAssoArrRNotEnoughMemory)*/ {
	    if( Key != NULL )  delete [] Key;
	    try {
		Key = new char[ strlen(ass.Key) + 1 ];
	    }
	    catch(...) { 
		throw EAssoArrRNotEnoughMemory("");
	    }
	    strcpy( Key,ass.Key);
	    Value=ass.Value;
	    Left=ass.Left;
	    Right=ass.Right;
	    return *this;
    }
    ~TAssociation() { 
	delete [] Key; 
    }
};


template<class T>
class TAssociativeArrayRefIterator;

template<class T>
class TAssociativeArrayRefConstIterator;

template<class T>
class TAssociativeArrayRefBackwardIterator;


template<class T>
class TAssociativeArrayRef { 
    // wskaznik do tablicy w ktorym przechowuje TAssocjacje
    TAssociation<T>* Data;
    
    // wskaznik do asocjacji w tablicy ktora jest korzeniem drzewa
    TAssociation<T>* Root; 
    
    // ilosc aktualnie przechowywanow elementow w tablicy
    int ItemsNumber;  
    
    // maksymalna ilosc obiektow jaka tablica moze przechowac ( rozmiar tablicy Data[] )
    const int MaxItemsNumber; 
    
    // pomocnicza tablica potrzebna przy tworzeniu drzewa; 
    // jest niszczona, gdy drzewo jest juz zbudowane ( ItemsNumber==MaxItemsNumber )
    int* Weights; 
    
    // wklada do tablicy Data nowy element ( key, val ) i wywaza 
    // drzewo. Rekurencyjna.
    // Zaczerpnieta z ksiazki:'N.Wirth Algorytmy+StrukturyDanych=Programy'
    void Insert( const char* key, T& val, TAssociation<T>*& p, int *h ) throw(EAssoArrRDuplicateKey);
    
    // wyszukuje w tablicy Data[] elementu o kluczu key i zwraca
    //  do niego wskaznik( lub NULL gdy go nie ma ). Rekurencyjna. 
    // Poszukiwanie rozpoczyna od elementu node
    TAssociation<T>* Find( const char * key, TAssociation<T>*& node ) const; 
    
    // czy niszczyc zawarte obiekty podczas destrukcji tablicy
    const bool Owner;
public:
    // stworz tablice n-elementowa
    TAssociativeArrayRef( int nelem, bool owner=false )  throw(EAssoArrRNotEnoughMemory) ; 

    ~TAssociativeArrayRef(); 
    
    // wstaw do tablicy element o kluczu key i wartosci val
    void Add( const char* key, T& val ) throw(EAssoArrRAlreadyFull);

    // pozwala na odwolywanie sie do elementu tablicy w sposob tab[1] 
    // - zwraca referencje do pierwszego elementu
    T& operator [] ( int loc ) /*throw(EAssoArrRIndexOutOfBounds)*/ {
	if( loc<0 || loc>ItemsNumber )
	    throw EAssoArrRIndexOutOfBounds("");
	return *( (Data+loc)->Value );
    }
    
    // pozwala na odwolywanie sie do elementu tablicy w sposob tab["klucz"] 
    // - zwraca referencje do elementu o kluczu "klucz";
    //  wyrzucz blad, gdy takiego elementu nie ma
    T& operator [] ( const char* key ) /*throw(EAssoArrRNonExistentKey)*/ {
	TAssociation<T>* x=Root;
	if( ( x=  Find( key, x ) ) == NULL )
	    throw EAssoArrRNonExistentKey(key, "");
	return  *(x->Value);
    }

    char* GetKey( int loc ) /*throw(EAssoArrIndexOutOfBounds) */{
	if( loc<0 || loc>ItemsNumber )
	    throw EAssoArrRIndexOutOfBounds("");
	return ( (Data+loc)->Key );
    }

    T* Contains( const char* key ) { 
	TAssociation<T>* x=Root;
	if( ( x=  Find( key, x ) ) == NULL )
	    return NULL;
	return  (x->Value);
    } 

    int Size() {
	return ItemsNumber;
    }

    friend class TAssociativeArrayRefIterator<T>;    
    friend class TAssociativeArrayRefConstIterator<T>;
    friend class TAssociativeArrayRefBackwardIterator<T>;

};



template<class T>
TAssociativeArrayRef<T>::TAssociativeArrayRef( int nelem, bool owner )  throw(EAssoArrRNotEnoughMemory)
    : MaxItemsNumber( nelem ), Owner(owner)
{
    try {
	Weights = new int[nelem];
    }
    catch(...) { 
	throw EAssoArrRNotEnoughMemory("");
    }
     
    try {
	Data = new TAssociation<T>[ nelem ];
    }
    catch(...) { 
    	delete [] Weights;
	throw EAssoArrRNotEnoughMemory("");
    }
    
    ItemsNumber = 0;
    Root=NULL;
}


template<class T>
TAssociativeArrayRef<T>::~TAssociativeArrayRef() 
{ 
    if( Owner ) {
	int i;
	for( i=0; i<ItemsNumber; i++ )
	    delete &(*this)[i];
    }
    delete [] Data; 
}
    
template<class T>
void TAssociativeArrayRef<T>::Add( const char* key, T& val ) throw(EAssoArrRAlreadyFull)
{
    int h;
    if( ItemsNumber < MaxItemsNumber ) {
	Insert( key, val, Root, &h );
	if( ItemsNumber == MaxItemsNumber )
	    delete [] Weights;
    }
    else 
	throw EAssoArrRAlreadyFull("");
}



template<class T>
void TAssociativeArrayRef<T>::Insert( const char* key, T& val, TAssociation<T>*& p, int *h ) 
    throw(EAssoArrRDuplicateKey)
{
    TAssociation<T> *p1, *p2;
    
    if( p==NULL ) {
	*h=1;
	p=Data+ItemsNumber;
	Weights[ItemsNumber]=0;
	Data[ItemsNumber++] =  TAssociation<T> ( key, &val );
    }
    else if( strcmp( key , p->Key) < 0 ) {
	Insert( key,val,p->Left,h );
	if( *h ) {
	    switch( Weights[p-Data] ) {
	    case 1: Weights[p-Data]=0; *h=0; break;
	    case 0: Weights[p-Data]=-1; break;
	    case -1: p1=p->Left;
		if( Weights[p1-Data]==-1 ) {
		    p->Left=p1->Right;
		    p1->Right=p;
		    Weights[p-Data]=0;
		    p=p1;
		}
		else {
		    p2=p1->Right;
		    p1->Right=p2->Left;
		    p2->Left=p1;
		    p->Left=p2->Right;
		    p2->Right=p;
		    Weights[p-Data]= (( Weights[p2-Data] ==-1 ) ? 1 : 0);
		    Weights[p1-Data]= (( Weights[p2-Data]==1 ) ? -1 : 0);
		    p=p2;
		}
		Weights[p-Data]=0; *h=0;
		break;
	    }
	}
    }
    else if( strcmp(key , p->Key) > 0 ) {
	Insert( key,val,p->Right,h );
	if( *h ) {
	    switch( Weights[p-Data] ) {
	    case -1: Weights[p-Data]=0; *h=0; break;
	    case  0: Weights[p-Data]=+1; break;
	    case  1: p1=p->Right;
		if( Weights[p1-Data]==1 ) {
		    p->Right=p1->Left;
		    p1->Left=p;
		    Weights[p-Data]=0; p=p1;
		}
		else {
		    p2=p1->Left;
		    p1->Left=p2->Right;
		    p2->Right=p1;
		    p->Right=p2->Left;
		    p2->Left=p;
		    Weights[p-Data]= (Weights[p2-Data]==1) ? -1 : 0;
		    Weights[p1-Data]=(Weights[p2-Data]==-1) ? 1 : 0;
		    p=p2;
		}
		Weights[p-Data]=0; *h=0;
		break;
	    }
	}
    }
    else 
	throw EAssoArrRDuplicateKey(key,"");
}


template<class T>
TAssociation<T>* TAssociativeArrayRef<T>::Find( const char* key, TAssociation<T>*& p ) const
{
    TAssociation<T>* ret;
    
    if( p==NULL )
	return NULL;
    else if( strcmp( key, p->Key ) < 0 )
	ret = Find( key, p->Left );
    else if( strcmp( key, p->Key ) > 0 )
	ret = Find( key, p->Right );
    else
	ret = p;
    return ret;
}



template<class T>
class TAssociativeArrayRefIterator {
    TAssociativeArrayRef<T> *Array;
    int Current;
public:
    TAssociativeArrayRefIterator(TAssociativeArrayRef<T> *arr) 
	: Array(arr), Current(-1){}
    int More() const {
	    return (Current+1)<(Array->ItemsNumber);
    }
    T& Next() {
	    return  (*Array)[++Current];
    }
    T& Last() const {
	    return  (*Array)[Current];
    }
};


template<class T>
class TAssociativeArrayRefConstIterator {
    TAssociativeArrayRef<T> *Array;
    int Current;
public:
    TAssociativeArrayRefConstIterator(TAssociativeArrayRef<T> *arr)
	    : Array(arr), Current(-1){}
    int More() const {
	    return (Current+1)<(Array->ItemsNumber);
    }
    const T& Next() {
	    return  (*Array)[++Current];
    }
    const T& Last() const{
	    return  (*Array)[Current];
    }
};


template<class T>
class TAssociativeArrayRefBackwardIterator {
    TAssociativeArrayRef<T> *Array;
    int Current;
public:
    TAssociativeArrayRefBackwardIterator(TAssociativeArrayRef<T> *arr) 
	: Array(arr), Current(arr->ItemsNumber){}
    int More() const {
	    return Current!=0;
    }
    T& Next() {
     	return  (*Array)[--Current];
    }
    T& Last() const {
	    return  (*Array)[Current];
    }
};



#endif
