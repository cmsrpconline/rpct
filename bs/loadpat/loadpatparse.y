%{
#define YYDEBUG 1
#define YYERROR_VERBOSE
#include <stdio.h>
int  yylex();
void yyrestart(FILE*);
extern "C" { 
    int yyerror(char *); 
    int senderrmsg();
 	   }
 
extern int debug;

#include "TCharSequence.hh"

#include <new>
#include "TException.hh"
#include "TBoard.hh"

TBoard   *board;

char buffer[256];

%}

%union {
    unsigned long  val;  /* For returning numbers.                   */
    char  id[128];   /* For returning identifier names    */
    int   range[2];
}
     


%token <val>   P_NUM
%token <id>    P_REG
%token <id>    P_REG_EL
%token <range> P_RANGE


%%     /* Grammar */

input:   
       | input line
;


line:     
       | command ';'           
;



command:  P_REG P_REG_EL '=' P_NUM     {
                                           if(debug)      
					       cout << $1 << "."<< $2
						    << "=" << $4 << endl;
					   strcpy( buffer, "pac");
					   strncat( buffer, &PATH_SEP, 1 );
					   strcat( buffer, $1 );
					   strncat( buffer, &PATH_SEP, 1 );
					   strcat( buffer, $2 );
					   board->SetStateSeq( 
							buffer, 
							TCharSequence( $4 ? "1" : "0" ) );
					   if(debug)
					       cout<<"SetStateSeq("<<buffer<<", "
						   <<TCharSequence( $4 ? "1" : "0" ) 
						   <<")\n";
                                       } 
          | P_REG P_REG_EL P_RANGE '=' P_NUM     {
                                           if(debug)
					       cout << $1 << "."<< $2
						    << "[" << $3[0] << ".." <<$3[1] << "]"
						    << "=" <<$5 << endl;
					   int i;
					   //int delta = (($3[0]>$3[1]) ? -1 : 1 );
					   TCharSequence state;
					   unsigned long mask;
					   if( $3[0] < $3[1] )
					       for( i=$3[0]; i<=$3[1]; i++ ) {
						   mask = (1<<i);
						   state += (($5 & mask) ? '1' : '0');
					       }
					   else
					       for( i=$3[0]; i>=$3[1]; i-- ) {
						   mask = (1<<i);
						   state += (($5 & mask) ? '1' : '0');
					       }
					   strcpy( buffer, "pac");
					   strncat( buffer, &PATH_SEP, 1 );
					   strcat( buffer, $1 );
					   strncat( buffer, &PATH_SEP, 1 );
					   strcat( buffer, $2 );
					   if(debug)
					       cout<<"SetStateSeq("<<buffer<<", "<<state<<")\n";
					   board->SetStateSeq( 
							buffer, 
							state );
					   
                                       } 
          | error                      {
	                                   char buf[256];
					   sprintf( buf, " : unknown command, line %d\n",
						    @1.last_line );
                                           yyerror(buf);
					   senderrmsg();
					   YYABORT;
	                               }
;



%%
#include <stdio.h>
#include <iostream.h>
#include <ctype.h>

extern FILE* yyin;
FILE *err;

char errorbuf[256];
int  errorbufpnt;


int loadpatparse(  FILE* file, FILE* errptr, TBoard* brd )
{
    yyin = file;
    err = errptr;
    errorbufpnt = 0;
    board = brd;

    yyrestart(yyin);
    return yyparse();
} 


int yyerror (char*s)  /* Called by yyparse on error */
{
    strcpy( errorbuf + errorbufpnt, s );
    errorbufpnt += strlen(s);
}

int senderrmsg()
{
    fputs( errorbuf, err );
    fflush( err );
    errorbufpnt = 0;
}


