typedef union {
    unsigned long  val;  /* For returning numbers.                   */
    char  id[128];   /* For returning identifier names    */
    int   range[2];
} YYSTYPE;

#ifndef YYLTYPE
typedef
  struct yyltype
    {
      int timestamp;
      int first_line;
      int first_column;
      int last_line;
      int last_column;
      char *text;
   }
  yyltype;

#define YYLTYPE yyltype
#endif

#define	P_NUM	258
#define	P_REG	259
#define	P_REG_EL	260
#define	P_RANGE	261


extern YYSTYPE yylval;
