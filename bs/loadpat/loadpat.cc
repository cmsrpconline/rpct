#include <iostream.h>
#include <stdio.h>
#include <new>
#include "TException.hh"
#include "TBoard.hh"
#include "TAltera.hh"
#include "TPac.hh"
#include "TBSContrWZ.hh"

int debug;

int loadpatparse( FILE* file, FILE* errptr, TBoard* board);

struct TBSCEl {
    char ChipType[32];
    char ChipName[32];
};


const char usage[] = "usage: loadpat [-d] file.bsc [ file1.pat ] \n"
                    "    If there is no file.pat, names of the files with patterns\n"
                    "    are taken from stdin.\n";


void sendpat( TBoard& board, TBSContrWZ& bscontr )
{
    TCharSequence irseq7, irseq8, irseq9, irseqA, irseqB,
                  drseq7, drseq8, drseq9, drseqA, drseqB;

    cout << "Preparing sequences...";
    cout.flush();

    board.SetState( "pac", "USER_7" );
    board.GenerateIRSequence( irseq7 );
    board.GenerateDRSequence( drseq7 );

    board.SetState( "pac", "USER_8" );
    board.GenerateIRSequence( irseq8 );
    board.GenerateDRSequence( drseq8 );

    board.SetState( "pac", "USER_9" );
    board.GenerateIRSequence( irseq9 );
    board.GenerateDRSequence( drseq9 );
     
    board.SetState( "pac", "USER_A" );
    board.GenerateIRSequence( irseqA );
    board.GenerateDRSequence( drseqA );
    
    board.SetState( "pac", "USER_B" );
    board.GenerateIRSequence( irseqB );
    board.GenerateDRSequence( drseqB );

    cout << "Done\n";

    cout << "Programing BS controller...";
    cout.flush();
    bscontr.ProgramStart();
    //board.SetState( "pac", "USER_7");
    //board.GenerateIRSequence(seq);
    TCharSequence inst_cap(irseq7.size(), ' ');
    bscontr.IRTransmitRec(irseq7, inst_cap);
    //seq.erase();
    //board.GenerateDRSequence(seq);
    bscontr.DRTransmit(drseq7);
    //seq.erase();
    //bscontr.ProgramEnd();
    //	bscontr.ProgramExecute();
    
    //	bscontr.ProgramStart();
    //board.SetState( "pac", "USER_8");
    //board.GenerateIRSequence(seq);
    bscontr.IRTransmit(irseq8);
    //seq.erase();
    //board.GenerateDRSequence(seq);
    bscontr.DRTransmit(drseq8);
    //seq.erase();
    // 	bscontr.ProgramEnd();
    // 	bscontr.ProgramExecute();
    
    // 	bscontr.ProgramStart();
    //board.SetState( "pac", "USER_9");
    //board.GenerateIRSequence(seq);
    bscontr.IRTransmit(irseq9);
    //seq.erase();
    //board.GenerateDRSequence(seq);
    bscontr.DRTransmit(drseq9);
    //seq.erase();
    // 	bscontr.ProgramEnd();
    // 	bscontr.ProgramExecute();
    
    // 	bscontr.ProgramStart();
    //board.SetState( "pac", "USER_A");
    //board.GenerateIRSequence(seq);
    bscontr.IRTransmit(irseqA);
    //seq.erase();
    //board.GenerateDRSequence(seq);
    bscontr.DRTransmit(drseqA);
    //seq.erase();
    // 	bscontr.ProgramEnd();
    // 	bscontr.ProgramExecute();
    
    // 	bscontr.ProgramStart();
    //board.SetState( "pac", "USER_B");
    //board.GenerateIRSequence(seq);
    bscontr.IRTransmit(irseqB);
    //seq.erase();
    //board.GenerateDRSequence(seq);
    bscontr.DRTransmit(drseqB);
    //seq.erase();
    bscontr.ProgramEnd();

    cout << "Done\n";

    cout << "Loading patterns...";
    cout.flush();
    bscontr.ProgramExecute();
    cout << "Done\n";

    board.InterpreteIRSequence(inst_cap);

    if(debug)
	board.Describe();
}


int main( int argc, char ** argv )
{
    int parse_ret_val=0;
    cout << "Initializing...";
    cout.flush();
    try {
	int tmp;
	int opt_no=1;
	set_new_handler(my_new_handler);
	debug = 0;
	
	if( argc < 2 ) 
	    throw TException( usage );
	
	if( !strcmp( argv[opt_no], "-d" ) ) {
	    debug = 1;
	    opt_no++;
	}

	FILE* bscfile;
	if( NULL == (bscfile = fopen( argv[opt_no++], "r" )) )
	    throw TException( "Couldn't open " + string(argv[opt_no-1]) + "\n" );
	
	tmp=1;
	TBSCEl chips[10];
	for( int chip_no=0; (tmp!=0 && tmp!=EOF); chip_no++ )  
	    tmp = fscanf( bscfile, "%s %s\n",
			  chips[chip_no].ChipType,
			  chips[chip_no].ChipName );
	chip_no--;
	
	TBoard board("board",chip_no);
	TBSContrWZ bscontr(0xa);
	int i;
	
	for( i=0; i<chip_no; i++ ) {
	    if(debug)
		cout << chips[i].ChipType << ":" << chips[i].ChipName << endl;
	    if( !strcmp(chips[i].ChipType, "PAC" ) ) 
		board.AddElement( *new TPac(chips[i].ChipName) );
	    else
		if( !strcmp(chips[i].ChipType, "ALTERA" ) ) 
		    board.AddElement( *new TAltera(chips[i].ChipName) );
		else {
		    cerr << "not supported chip type : "<< chips[i].ChipType << endl;
		    exit(1);
		}
	}
		  


	char buffer[256];
	for( i=0; i<chip_no; i++ ) 
	    board.SetState( chips[i].ChipName, "BYPASS" );
	
        cout << "Done\n";

	FILE *patfile;
	if( opt_no < argc ) {
            cout << "Parsing " << argv[opt_no] << "...";
            cout.flush();
	    if( NULL == (patfile = fopen( argv[opt_no++], "r" )) )
		throw TException( "Couldn't open " + string(argv[opt_no-1]) + "\n" );
            parse_ret_val = loadpatparse( patfile, stderr, &board );
	    if(!parse_ret_val) {
		cout << "Done\n";

		sendpat( board, bscontr );
		for( i=0; i<chip_no; i++ ) {
		    TCharSequence inst_cap;
		    char buf[256];
		    strcpy( buf, chips[i].ChipName );
		    strncat( buf,& PATH_SEP, 1);
		    strcat( buf, INSTRUCTION_REG_NAME );
		    board.GetStateSeq( buf, inst_cap );
		    if(debug) 
			cout << "inst capture: chip:" 
			     << chips[i].ChipName << " : " 
			     << inst_cap << endl;
		    if( string(inst_cap,0, 2) != "10" )
			throw TException(" Bad instruction capture value for chip '" 
			       + string(chips[i].ChipName) 
			       + "'. Wrong .bsc file, or boundary scan chain broken." );
		}
	    }
	    else 
		throw TException("Parse error\n");		
	    fclose(patfile);
	}
	else {
	    cout << argv[0] << '>';
	    tmp=fscanf(stdin, "%s", buffer);
	    while( tmp!=0 && tmp!= EOF ) {
		if( !strcmp(buffer, "quit") )
		    break;		    
                cout << "Parsing " << buffer << "...";
                cout.flush();
		if( NULL == (patfile = fopen( buffer, "r" )) )
		    throw TException( "Couldn't open " + string(buffer) + "\n" );
		parse_ret_val = loadpatparse( patfile, stderr, &board );
		if(!parse_ret_val) {
		    cout << "Done\n";
		    
		    sendpat( board, bscontr );
		    for( i=0; i<chip_no; i++ ) {
			TCharSequence inst_cap;
			char buf[256];
			strcpy( buf, chips[i].ChipName );
			strncat( buf,& PATH_SEP, 1);
			strcat( buf, INSTRUCTION_REG_NAME );
			board.GetStateSeq( buf, inst_cap );
			if(debug) 
			    cout << "inst capture: chip:" 
				 << chips[i].ChipName 
				 << " : " << inst_cap << endl;
			if( string(inst_cap,0, 2) != "10" )
			    throw TException(" Bad instruction capture value for chip '" 
				    + string(chips[i].ChipName) 
				    + "'. Wrong .bsc file, or boundary scan chain broken." );
		    }
		}
		else 
		    throw TException( "Parse error\n");
		
		fclose(patfile);
		char buf[256];
		sprintf( buf, "Success\nloadpat>");
		cout << buf ;
		cout.flush();
		tmp=fscanf(stdin, "%s", buffer);
	    }
	    cout << endl;
	}
	

    }
    catch( TException& e ) {
	cerr << e.what() << endl;
	return 1;
    }
    catch( ... ) {
	cerr << "Exception\n";
	return 1;
    }
    return parse_ret_val;
}
