#include "TCharSequence.hh"


bool operator==( TPattern& pat,  TCharSequence& seq )
{
    // cout <<"syf\n";
    int len;
    if( (len=pat.length()) != seq.length() )
        return false;
    int i;
    for( i=0; i<len; i++ )
        if( pat[i] != 'X'   &&   pat[i] != seq[i] )
            return false;
    return true;
}

TCharSequence
TPattern::ToCharSequence()
{
    TCharSequence seq;
    int i;

    for( i=0; i<length(); i++ )
	seq += ( (at(i)=='X') ? '0' : at(i) );
    
    return seq;
}
