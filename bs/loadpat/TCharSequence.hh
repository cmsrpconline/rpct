#ifndef TSCANVECTOR_HH
#define TSCANVECTOR_HH

#include <typeinfo>
#include <string>

using namespace std;

class TScanVector;
class TPattern;
bool operator==( TPattern& pat,  TScanVector& seq );

class TPattern : public string {
public:
    TPattern() {}

    TPattern( const char* str ) : string(str) {}

    TPattern( char c ) : string( 1, c ) {}
    TPattern( int len, char c ) : string( len, c ) {}

    TPattern( TPattern& pat, string::size_type pos, string::size_type n )
	: string( pat, pos, n ) {}
   
    virtual ~TPattern() {}

    virtual bool 
    Compare( TScanVector& seq )
        { return (*this)==seq; }

    TScanVector
    ToScanVector();
};

class TScanVector : public TPattern {
public:
    TScanVector() {}
    TScanVector( char* str ) : TPattern(str) {}
    TScanVector( int len, char c ) : TPattern( len, c ) {}
    TScanVector( char c ) : TPattern( c ) {}
    TScanVector( TScanVector& pat, string::size_type pos, string::size_type n )
	: TPattern( pat, pos, n ) {}
    virtual ~TScanVector() {}
};



#ifdef __GNUG__

inline ostream& operator <<(ostream& os, TScanVector& seq) 
{
    os << (string)seq;
    return os;
}

inline ostream& operator <<(ostream& os, TPattern& pat) 
{
    os << (string)pat;
    return os;
}

inline bool operator <(TPattern& p1, TPattern& p2) 
{
    return (string)p1 < (string)p2;
}

#endif /* __GNUG__ */




inline bool operator==( TScanVector& seq, TPattern& pat )
{
    return pat==seq;
}

//typedef string TScanVector;


#endif
