%{
#include "loadpatparse.tab.h"

extern YYLTYPE yylloc;
%}
%option yylineno
DIGIT    [0-9]
HEXDIGIT [0-9a-fA-F]
/* BINDIGIT [0-1] */
ID       [a-zA-Z][_a-zA-Z0-9]{0,31}



%%

#{HEXDIGIT}+     {
                    sscanf(yytext,"#%lx",&yylval.val);
		    yylloc.first_line = yylineno;
		    yylloc.last_line = yylineno;
                    return P_NUM;
                 }



{ID}             {
                    strcpy(yylval.id, yytext);
		    yylloc.first_line = yylineno;
		    yylloc.last_line = yylineno;
		    return P_REG;
                 }

"."{ID}          {
                    strcpy(yylval.id, yytext+1);
		    yylloc.first_line = yylineno;
		    yylloc.last_line = yylineno;
		    return P_REG_EL;
                 }

"["{DIGIT}+".."{DIGIT}+"]"   {  /* np. [10..50] */
                    sscanf(yytext,"[%d..%d]", &yylval.range[0], &yylval.range[1] );
		    yylloc.first_line = yylineno;
		    yylloc.last_line = yylineno;
		    return P_RANGE;
                 }

"--"[^\n]*\n    /* eat up one-line comments */

[ \t\n]+

"="            {
                 yylloc.first_line = yylineno;
                 yylloc.last_line = yylineno;
                 return '='; 
               }
";"            {
                 yylloc.first_line = yylineno;
                 yylloc.last_line = yylineno;
		 return ';';
               }

<<EOF>>      { YY_FLUSH_BUFFER; yyterminate(); }

%%
