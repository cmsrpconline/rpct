#ifndef ELEMAPPING_H
#define ELEMAPPING_H

typedef int M_febId;
typedef int M_chipId;
typedef int M_strip;
typedef int M_channel;
typedef int M_instance;
typedef std::set<M_febId> febsContainer;
typedef std::map<M_chipId,febsContainer > chipsContainer;
typedef std::map<M_strip, M_channel> FEBMap;
typedef std::map<M_febId, FEBMap> ROMap;

struct chindex{
  chindex(M_chipId c,M_febId f) : chip(c), feb(f) {}
  M_chipId chip;
  M_febId  feb;
  bool operator < (const chindex& ci)const{
    if (ci.chip ==this->chip){
      return ci.feb < this->feb;
    }
    return ci.chip < this->chip;
  }
};

class ELEMapping{

 public:
  ELEMapping(){}
  virtual ~ELEMapping(){}
  void insertFEB (M_febId feb, M_chipId chip, M_instance inst,
		 const std::string& chname, 
		 const std::string& etapar) {
    
    febsContainer febs;
    if (chipIds.find(chip)!= chipIds.end()){
      febs = chipIds[chip];
    } else {
      chipToinstance[chip] = inst;
      boardToChamber[chip] = chname;
      boardToPartition[chip] = etapar;
    }
    chindex ind(chip,feb);
    getCh[ind]=chname;
    febs.insert(feb);
    chipIds[chip] = febs;
  }

  void insertStripMap(M_febId feb, M_strip strip, M_channel chann){

    std::map<M_strip,M_channel> strips;
    if (febIds.find(feb) != febIds.end()){
      strips = febIds[feb];
    }
    strips[strip] = chann;
    febIds[feb] = strips;      
  }

  const chipsContainer& allchips() const{return chipIds;}

  const febsContainer&  allfebs(M_chipId chip) const{
    return chipIds[chip];

  }
  const FEBMap& febMap(M_febId feb)const {return febIds[feb];}
  const M_instance instance(M_chipId chip)const {return chipToinstance[chip];}
  const std::string & chamber(M_chipId chip) const
    {return boardToChamber[chip];}
  const std::string & etaPartition(M_chipId chip) const
    {return boardToPartition[chip];}

  const std::string& chName(M_chipId chip, M_febId feb) const
    {chindex ind(chip,feb); return getCh[ind];}

 private:
  mutable ROMap febIds;
  mutable chipsContainer chipIds;
  mutable std::map<M_chipId, M_instance> chipToinstance;
  mutable std::map<M_chipId, std::string > boardToChamber;
  mutable std::map<M_chipId, std::string > boardToPartition;
  mutable std::map<chindex, std::string> getCh;
};

class NoiseRes{
 public:
  NoiseRes(){}
  ~NoiseRes(){}
  void insert(const std::string& chamber,const std::string& etaPartition,
	      int strip, double noise){

  }
 private:
  
};
#endif
