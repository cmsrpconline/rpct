#ifndef _XDAQRPCTHRESHOLDSCAN_H_
#define _XDAQRPCTHRESHOLDSCAN_H_

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/exception/Exception.h" 
#include "xdaq/NamespaceURI.h"
 
#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"

#include "xdata/Serializable.h"
#include "xdata/rpctsoap/Serializer.h"
#include "xdata/String.h"
#include "xgi/Utils.h"
#include "xgi/Method.h"

#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/fsm/FailedEvent.h"

#include "rpct/xdaqlboxaccess/XdaqLBoxAccessClient.h"
#include "rpct/xdaqutils/XdaqDbServiceClient.h"
#include "rpct/xdaqdiagaccess/XdaqDiagAccessClient.h"
#include "rpct/xdaqRpcThresholdScan_dev/include/ELEMapping.h"
 
using namespace rpct;
using namespace rpct::xdaqutils;
using namespace rpct::xdaqdiagaccess;


typedef XdaqDbServiceClient::FebAccessInfoVector FAIV;
typedef std::vector<XdaqDbServiceClient::FebAccessInfoVector> FAIVV;
typedef std::map<int ,rpct::XdaqLBoxAccessClient::MassiveWriteRequestBag >        instanceWriteMap;
typedef std::map<int ,rpct::XdaqLBoxAccessClient::MassiveWriteResponseBag*> instanceReadMap;

  
struct chIndex{
  chIndex(std::string b, int w, int s, int l, std::string subs) :
    boe_str(b),wheel(w),sector(s),layer(l),subsector(subs){}
  std::string boe_str;
  int wheel;
  int sector;
  int layer;
  std::string subsector;
  bool operator < (const chIndex& c)const{
    if (c.boe_str==this->boe_str){
      if (c.wheel == this->wheel){
	if (c.sector == this->sector){
	  if (c.layer == this->layer){
	    return c.subsector < this->subsector;
	  }
	  else{
	    return c.layer < this->layer;
	  }
	}
	else{
	  return c.layer < this->layer;
	}
      }
      else{
	return c.wheel < this->wheel;
      }
    }
    else{
      return c.boe_str < this->boe_str;
    }
  }
};


class XdaqRpcThresholdScan: public xdaq::Application { 
  
 private:
  
  FAIVV FAIVsecVector;
  std::set<chIndex> chambers;
  std::string str_addedBSectors;
  std::string str_maskedBChambers;
  std::string str_addedESectors;
  std::string str_maskedEChambers;

  //all nescessery?
  rpct::xdaqutils::XdaqDbServiceClient dbServiceClient;
  rpct::XdaqLBoxAccessClient lboxAccessClient;
  rpct::xdaqdiagaccess::XdaqDiagAccessClient diagAccessClient;
  bool flagHTML;
  toolbox::fsm::FiniteStateMachine fsm; 
  xdata::String stateName; 
  xdata::String globalConfKey;

  //what is this?
  int fthr; //first threshold
  int lthr; //last threshold
  int sthr; //stepsize of thresholdscan
  
  
 public:
  
  //what are these?
  static const char* RPCT_RPC_CONFIG_NS;
  static const char* RPCT_RPC_CONFIG_PREFIX;
  
  XDAQ_INSTANTIATOR();    
  
  XdaqRpcThresholdScan(xdaq::ApplicationStub * s);      
  
  virtual ~XdaqRpcThresholdScan();


 private:

  void GetStartFormData(xgi::Input * in,int& fthr,int& lthr, int& sthr);

  void GetPostData(xgi::Input * in,int& wheel,std::string& secOrTow,std::string& tower,int& sector);

  void AddSectorsToString(std::string& str_addedSections, std::string wheeltype,int wheel,std::string secOrTow,std::string tower,int sector);

  void AddSectorsToFAIVV(XdaqDbServiceClient::BarrelOrEndcap boe,int wheel, std::string secOrTow,std::string tower,int sector);

  void GetPostMaskData(xgi::Input * in,int& wheel,int& sector,std::string& rb,std::string& subs);
  
  void AddChambersToString(std::string& str_maskedChambers,std::string wheeltype,int wheel,int sector,std::string ringtype, std::string rb,std::string subs);

  void AddChambersToSet(XdaqDbServiceClient::BarrelOrEndcap boe,int wheel,int sector,std::string rb,std::string subs);

  void PerformeTest(xgi::Output* out,std::vector<int> thrvalues);

  void prepareThreshold(int thrvalue,std::string boe_str, rpct::FebAccessInfo& febAccessInfo, instanceWriteMap& map);

  void setThreshold(instanceWriteMap& instanceLBBox, 
		    instanceReadMap& instanceForReadLBBox);
  
  void readThreshold(instanceReadMap& map);

  void buildReadoutMapping(const std::string& chName, ELEMapping& elemap);

  void getNoise(const ELEMapping& elemap, NoiseRes& noiseres);

  void fromNametoLocation(const std::string& name, std::string& boe_str, int& wheel, int& sector, int& layer, std::string& subs);

  void linkBoardCounters(int instance, int chipId, double rate[96]);

  void AddChambersToSet(std::string,int,int,int,int,std::string);

  void AddSectorToFAIVV(std::string bore,int wheel,int disc ,std::string secOrTow,std::string tower,int section);

 public:
  
  void Default(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
  void MaskForm(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
  
  void AddBSectors(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);

  void AddESectors(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);

  void AddMaskedBChambers(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);

  void AddMaskedEChambers(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
  
  void Reset(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
    
  void ThresholdScan(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
  
  
};
#endif

