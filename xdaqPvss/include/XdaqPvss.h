// $Id $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _XdaqPvss_h_
#define _XdaqPvss_h_

#include "xdaq/WebApplication.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/exception/Exception.h"

#include "xdaq/NamespaceURI.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"
#include "xgi/Method.h"
#include "cgicc/HTMLClasses.h"

#define PSX_NS_URI "http://xdaq.cern.ch/xdaq/xsd/2006/psx-pvss-10.xsd"

class XdaqPvss: public xdaq::Application  
{	
	public:
		
	XDAQ_INSTANTIATOR();
	
	XdaqPvss(xdaq::ApplicationStub * s);
	
	//
	// SOAP Callback  
	//
	xoap::MessageReference dpNotify (xoap::MessageReference msg) throw (xoap::exception::Exception);
	
	void Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	
	void dpGet(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	
	void dpSet(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	
	void dpConnect(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	
	void dpDisconnect(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	
	void dpGetNames(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	
	void dpGetFields(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	
	void showForm(xgi::Output * out);
	
	protected:
	
	std::string response_;
	std::string lastDP_;
};

#endif
