package rpct.rcms.lbox.axis;

import java.net.URL;
import java.rmi.RemoteException;

import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;
import javax.xml.rpc.ServiceException;

import org.apache.axis.Constants;
import org.apache.axis.client.Call;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import rpct.rcms.lbox.FebAccess;
import rpct.xdaq.XdaqApplicationInfo;
import rpct.xdaq.axis.XdaqAccessImplAxisBase;
import rpct.xdaq.axis.bag.Bag;
import rpct.xdaq.axis.bag.BagUtils;
import rpct.xdaq.axis.bag.HashBag;

/**
 * Created on 2005-05-05
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class FebAccessImplAxis extends XdaqAccessImplAxisBase implements FebAccess  {    
	
	private static Log log = LogFactory.getLog(FebAccessImplAxis.class); 

	public static String RPCT_LBOX_ACCESS_NS = "urn:rpct-lbox-access:1.0";
	
	public FebAccessImplAxis(URL endpoint, String xdaqClassName, int instance) {
		super(new XdaqApplicationInfo(endpoint, xdaqClassName, instance), RPCT_LBOX_ACCESS_NS);
		//BagUtils.registerType(service, FebInfo.class);
		//BagUtils.registerType(service, FebValues.class);
	}
	
	public void configure() throws RemoteException, ServiceException {
		Call call = prepareCall("configure");
		/*call.addParameter(new QName(XDAQ_LBOX_ACCESS_NS, "dummy"), 
		 Constants.XSD_INTEGER,
		 ParameterMode.IN);
		 call.setReturnType(Constants.XSD_ANY);*/
		call.invoke(new Object[] { /*BigInteger.valueOf(1)*/});
	} 
	
	public void enableDAC(FebInfo febInfo, boolean enable) throws RemoteException, ServiceException {
		Call call = prepareCall("enableDAC"); 
		call.addParameter(new QName(RPCT_LBOX_ACCESS_NS, "febInfo"), 
				Bag.XML_TYPE,
				ParameterMode.IN);
		call.addParameter(new QName(RPCT_LBOX_ACCESS_NS, "enable"), 
				Constants.XSD_BOOLEAN,
				ParameterMode.IN); 
		//call.setReturnType(Constants.XSD_FLOAT);
		call.invoke(new Object[] { febInfo, enable});
	} 
	
	
	public float readTemperature(FebInfo febInfo) throws RemoteException, ServiceException {  
		Call call = prepareCall("readTemperature"); 
		call.addParameter(new QName(RPCT_LBOX_ACCESS_NS, "febInfo"), 
				Bag.XML_TYPE,
				ParameterMode.IN);
		call.setReturnType(Constants.XSD_FLOAT);
		return (Float) call.invoke(new Object[] { febInfo });
	}
	
	public float readVTH1(FebInfo febInfo) throws RemoteException, ServiceException {
		Call call = prepareCall("readVTH1"); 
		call.addParameter(new QName(RPCT_LBOX_ACCESS_NS, "febInfo"), 
				Bag.XML_TYPE,
				ParameterMode.IN);
		call.setReturnType(Constants.XSD_FLOAT);
		return (Float) call.invoke(new Object[] { febInfo });
	}
	
	public void writeVTH1(FebInfo febInfo, float vth1) throws RemoteException, ServiceException {  
		Call call = prepareCall("writeVTH1");
		//call.setOperationName(new QName(XDAQ_LBOX_ACCESS_NS, "writeVTH1"));  
		call.addParameter(new QName(RPCT_LBOX_ACCESS_NS, "febInfo"), 
				Bag.XML_TYPE,
				ParameterMode.IN);
		call.addParameter(new QName(RPCT_LBOX_ACCESS_NS, "vth1"), 
				Constants.XSD_FLOAT,
				ParameterMode.IN); 
		//call.setReturnType(Constants.XSD_FLOAT);
		call.invoke(new Object[] { febInfo, new Float(vth1)});
	}
	
	public float readVTH2(FebInfo febInfo) throws RemoteException, ServiceException {
		Call call = prepareCall("readVTH2"); 
		call.addParameter(new QName(RPCT_LBOX_ACCESS_NS, "febInfo"), 
				Bag.XML_TYPE,
				ParameterMode.IN);
		call.setReturnType(Constants.XSD_FLOAT);
		return (Float) call.invoke(new Object[] { febInfo });
	}
	
	public void writeVTH2(FebInfo febInfo, float vth2) throws RemoteException, ServiceException {  
		Call call = prepareCall("writeVTH1");
		//call.setOperationName(new QName(XDAQ_LBOX_ACCESS_NS, "writeVTH2"));  
		call.addParameter(new QName(RPCT_LBOX_ACCESS_NS, "febInfo"), 
				Bag.XML_TYPE,
				ParameterMode.IN);
		call.addParameter(new QName(RPCT_LBOX_ACCESS_NS, "vth2"), 
				Constants.XSD_FLOAT,
				ParameterMode.IN); 
		//call.setReturnType(Constants.XSD_FLOAT);
		call.invoke(new Object[] { febInfo,  new Float(vth2)});
	}
	
	public float readVMon1(FebInfo febInfo) throws RemoteException, ServiceException {
		Call call = prepareCall("readVMon1"); 
		call.addParameter(new QName(RPCT_LBOX_ACCESS_NS, "febInfo"), 
				Bag.XML_TYPE,
				ParameterMode.IN);
		call.setReturnType(Constants.XSD_FLOAT);
		return (Float) call.invoke(new Object[] { febInfo });
	}
	
	public void writeVMon1(FebInfo febInfo, float vmon1) throws RemoteException, ServiceException {  
		Call call = prepareCall("writeVTH1");
		//call.setOperationName(new QName(XDAQ_LBOX_ACCESS_NS, "writeVMon1"));  
		call.addParameter(new QName(RPCT_LBOX_ACCESS_NS, "febInfo"), 
				Bag.XML_TYPE,
				ParameterMode.IN);
		call.addParameter(new QName(RPCT_LBOX_ACCESS_NS, "vmon1"), 
				Constants.XSD_FLOAT,
				ParameterMode.IN); 
		//call.setReturnType(Constants.XSD_FLOAT);
		call.invoke(new Object[] { febInfo, new Float(vmon1)});
	}
	
	public float readVMon2(FebInfo febInfo) throws RemoteException, ServiceException {
		Call call = prepareCall("readVMon2"); 
		call.addParameter(new QName(RPCT_LBOX_ACCESS_NS, "febInfo"), 
				Bag.XML_TYPE,
				ParameterMode.IN);
		call.setReturnType(Constants.XSD_FLOAT);
		return (Float) call.invoke(new Object[] { febInfo });
	}
	
	public void writeVMon2(FebInfo febInfo, float vmon2) throws RemoteException, ServiceException {  
		Call call = prepareCall("writeVTH1");
		//call.setOperationName(new QName(XDAQ_LBOX_ACCESS_NS, "writeVTH2"));  
		call.addParameter(new QName(RPCT_LBOX_ACCESS_NS, "febInfo"), 
				Bag.XML_TYPE,
				ParameterMode.IN);
		call.addParameter(new QName(RPCT_LBOX_ACCESS_NS, "vmon2"), 
				Constants.XSD_FLOAT,
				ParameterMode.IN); 
		//call.setReturnType(Constants.XSD_FLOAT);
		call.invoke(new Object[] { febInfo, new Float(vmon2)});
	}
	
	public MassiveReadResponse massiveRead(MassiveReadRequest readRequest) throws RemoteException, ServiceException { 
		Call call = prepareCall("massiveRead");    
		
		log.debug("massiveRead: request = " + readRequest + " febs = " + readRequest.getFebs() 
				+ " properties = " + readRequest.getProperties());           
		
		call.addParameter(new QName(RPCT_LBOX_ACCESS_NS, "readRequest"), 
				Bag.XML_TYPE,
				ParameterMode.IN);
		call.setReturnType(Bag.XML_TYPE);
		return new MassiveReadResponseImplAxis(
				(HashBag<Object>) call.invoke(new Object[] { readRequest }));
	}
	
	public int testBag(Bag bag) throws RemoteException, ServiceException {  
		
		Call call = prepareCall("testBag");        
		call.addParameter(new QName(RPCT_LBOX_ACCESS_NS, "febInfo"), 
				Bag.XML_TYPE,
				ParameterMode.IN);
		return (Integer)call.invoke(new Object[] {bag});
	}
	
	
	public FebInfo createFebInfo(int ccuAddress, int channel, int address, boolean endcap) {
		return new FebInfoImplAxis(ccuAddress, channel, address, endcap);
	}

	public FebValues createFebValues(int ccuAddress, int channel, int address, boolean endcap, float[] values) {
		return new FebValuesImplAxis(ccuAddress, channel, address, endcap, values);
	}
	
	public MassiveReadRequest createMassiveReadRequest(FebProperties[] properties, FebInfo[] febs) {
		return new MassiveReadRequestImplAxis(properties, febs);
	}
	
	public MassiveWriteRequest createMassiveWriteRequest(FebProperties[] properties, FebValues[] febValues) {		
		return new MassiveWriteRequestImplAxis(properties, febValues);
	}
	
	public void massiveWrite(MassiveWriteRequest writeRequest) throws RemoteException, ServiceException  {
		Call call = prepareCall("massiveWrite");    
		
		log.debug("massiveWrite request = " + writeRequest 
				+ " febValues = " + writeRequest.getFebValues() 
				+ " properties = " + writeRequest.getProperties());        
		
		call.addParameter(new QName(RPCT_LBOX_ACCESS_NS, "writeRequest"), 
				Bag.XML_TYPE,
				ParameterMode.IN);
		call.setReturnType(Constants.XSD_ANY);
		call.invoke(new Object[] { writeRequest });
	}   
}
