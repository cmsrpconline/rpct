package rpct.rcms.lbox.axis;

import java.math.BigInteger;

import rpct.rcms.lbox.FebAccess.FebInfo;
import rpct.xdaq.axis.bag.HashBag;

/**
 * Created on 2005-07-04
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class FebInfoImplAxis extends HashBag<Object> implements FebInfo {
    public final static String KEY_CCU_ADDRESS = "ccuAddress";
    public final static String KEY_CHANNEL = "channel";
    public final static String KEY_ADDRESS = "address";
    public final static String KEY_ENDCAP = "endcap";
    public FebInfoImplAxis(int ccuAddress, int channel, int address, boolean endcap) {
        super();
        setCcuAddress(ccuAddress);
        setChannel(channel);
        setAddress(address);
        setEndcap(endcap);
    }
    public FebInfoImplAxis(HashBag<Object> bag) {
        super(bag);
    }
    public int getAddress() {
        return ((BigInteger) get(KEY_ADDRESS)).intValue();
    }
    public void setAddress(int address) {
        put(KEY_ADDRESS, BigInteger.valueOf(address));
    }
    public int getChannel() {
        return ((BigInteger) get(KEY_CHANNEL)).intValue();
    }
    public void setChannel(int channel) {
        put(KEY_CHANNEL, BigInteger.valueOf(channel));
    }
    public int getCcuAddress() {
        return ((BigInteger) get(KEY_CCU_ADDRESS)).intValue();
    }
    public void setCcuAddress(int ccuAddress) {
        put(KEY_CCU_ADDRESS, BigInteger.valueOf(ccuAddress));
    }
    public boolean isEndcap() {
        return (Boolean)get(KEY_ENDCAP);
    }
    public void setEndcap(boolean endcap) {
        put(KEY_ENDCAP, endcap);
    }
}