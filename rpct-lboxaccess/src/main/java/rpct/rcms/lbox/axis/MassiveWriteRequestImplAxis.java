package rpct.rcms.lbox.axis;

import rpct.rcms.lbox.FebAccess.FebProperties;
import rpct.rcms.lbox.FebAccess.FebValues;
import rpct.rcms.lbox.FebAccess.MassiveWriteRequest;
import rpct.xdaq.axis.bag.HashBag;

public class MassiveWriteRequestImplAxis extends HashBag<Object> implements MassiveWriteRequest {    
    
    public final static String KEY_PROPERTIES = "properties";
    public final static String KEY_FEB_VALUES = "febValues";
    
    public MassiveWriteRequestImplAxis(FebProperties[] properties, FebValues[] febValues) {
        setProperties(properties);
        setFebValues(febValues);
    }    

    public FebProperties[] getProperties() {
        String[] props = (String[]) get(KEY_PROPERTIES);
        FebProperties[] properties = new FebProperties[props.length];
        for (int i = 0; i < props.length; i++) {
            properties[i] = FebProperties.valueOf(props[i]);
        }
        return properties;
    }
    
    public void setProperties(FebProperties[] properties) {
        String[] props = new String[properties.length];
        for (int i = 0; i < properties.length; i++) {
            props[i] = properties[i].toString();
        }
        put(KEY_PROPERTIES, props);
    }
    
    public void setFebValues(FebValues[] febValues) {
        put(KEY_FEB_VALUES, febValues);  
    }
    
    public FebValues[] getFebValues() {
        return (FebValues[]) get(KEY_FEB_VALUES);
    }
    
}
