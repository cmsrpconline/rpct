/**
 * Created on Jan 27, 2006
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */

package rpct.rcms.lbox.axis;

import rpct.rcms.lbox.FebAccess;
import rpct.rcms.lbox.FebAccess.FebInfo;
import rpct.xdaq.axis.bag.HashBag;

public class FebValuesImplAxis extends HashBag<Object> implements FebAccess.FebValues {

    public final static String KEY_FEB = "feb";
    public final static String KEY_VALUES = "values";
	
	public FebValuesImplAxis(int ccuAddress, int channel, int address, boolean endcap, 
                float[] values) {
		setFeb(new FebInfoImplAxis(ccuAddress, channel, address, endcap));
		setValues(values);
	}
    public FebValuesImplAxis(HashBag<Object> bag) {
    	super(bag);
        //feb = new FebInfoImplAxis((HashBag<BigInteger>) bag.get("feb"));
        //values = (float[]) bag.get("values");
    }
    
    public FebInfo getFeb() {
        return (FebInfo) get(KEY_FEB);
    }

    public void setFeb(FebInfo feb) {
        put(KEY_FEB, feb);  
    }
    
    public float[] getValues() {
        return (float[]) get(KEY_VALUES);
    }

    public void setValues(float[] values) {
        put(KEY_VALUES, values);  
    }
}