package rpct.rcms.lbox.axis;

import java.util.List;

import rpct.rcms.lbox.FebAccess;
import rpct.rcms.lbox.FebAccess.FebValues;
import rpct.xdaq.axis.bag.HashBag;

public class MassiveReadResponseImplAxis  implements FebAccess.MassiveReadResponse {

    private String[] properties;
    private FebValues[] febValues;
    
    public MassiveReadResponseImplAxis(HashBag<Object> bag) {
        properties = (String[]) bag.get("properties");
        List<HashBag> febValuesBagsList = (List<HashBag>) bag.get("febValues");
        febValues = new FebValues[febValuesBagsList.size()];
        for (int i = 0; i < febValuesBagsList.size(); i++) {
            febValues[i] = new FebValuesImplAxis(febValuesBagsList.get(i));
        }
    }    
    
    public String[] getProperties() {
        return properties;
    }

    public FebValues[] getFebValues() {
        return febValues;
    }
}
