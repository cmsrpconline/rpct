package rpct.rcms.lbox.axis;

import rpct.rcms.lbox.FebAccess.FebInfo;
import rpct.rcms.lbox.FebAccess.FebProperties;
import rpct.rcms.lbox.FebAccess.MassiveReadRequest;
import rpct.xdaq.axis.bag.HashBag;

public class MassiveReadRequestImplAxis extends HashBag<Object> implements MassiveReadRequest {    
    
    public final static String KEY_PROPERTIES = "properties";
    public final static String KEY_FEBS = "febs";
    
    public MassiveReadRequestImplAxis(FebProperties[] properties, FebInfo[] febs) {
        setProperties(properties);
        setFebs(febs);
    }    

    public FebProperties[] getProperties() {
        String[] props = (String[]) get(KEY_PROPERTIES);
        FebProperties[] properties = new FebProperties[props.length];
        for (int i = 0; i < props.length; i++) {
            properties[i] = FebProperties.valueOf(props[i]);
        }
        return properties;
    }
    
    public void setProperties(FebProperties[] properties) {
        String[] props = new String[properties.length];
        for (int i = 0; i < properties.length; i++) {
            props[i] = properties[i].toString();
        }
        put(KEY_PROPERTIES, props);
    }
    public void setFebs(FebInfo[] febs) {
        put(KEY_FEBS, febs);  
    }
    
    public FebInfo[] getFebs() {
        return (FebInfo[]) get(KEY_FEBS);
    }
    
}
