/**
 * Created on 2005-05-05
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */

package rpct.rcms.lbox;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;


public interface FebAccess {

    public abstract void configure() throws RemoteException, ServiceException;
    
    public abstract void enableDAC(FebInfo febInfo, boolean enable)
    		throws RemoteException, ServiceException;

    public abstract float readTemperature(FebInfo febInfo)
            throws RemoteException, ServiceException;

    public abstract float readVTH1(FebInfo febInfo)
            throws RemoteException, ServiceException;

    public abstract void writeVTH1(FebInfo febInfo, float vth1)
            throws RemoteException, ServiceException;

    public abstract float readVTH2(FebInfo febInfo)
            throws RemoteException, ServiceException;

    public abstract void writeVTH2(FebInfo febInfo, float vth2)
            throws RemoteException, ServiceException;

    public abstract float readVMon1(FebInfo febInfo)
            throws RemoteException, ServiceException;

    public abstract void writeVMon1(FebInfo febInfo, float vmon1)
            throws RemoteException, ServiceException;

    public abstract float readVMon2(FebInfo febInfo)
            throws RemoteException, ServiceException;

    public abstract void writeVMon2(FebInfo febInfo, float vmon2)
            throws RemoteException, ServiceException;


    public interface FebInfo {
        public int getCcuAddress();
        public void setCcuAddress(int ccuAddress);
        public int getChannel();
        public void setChannel(int channel);
        public int getAddress();
        public void setAddress(int address);
        public boolean isEndcap();
        public void setEndcap(boolean endcap);
    }
    
    public FebInfo createFebInfo(int ccuAddress, int channel, int address, boolean endcap);
    
    static public enum FebProperties {
        TEMPERATURE, TEMPERATURE2, VTH1, VTH2, VTH3, VTH4, VMON1, VMON2, VMON3, VMON4
    }

    public interface MassiveReadRequest {
        public FebProperties[] getProperties();
        //public void setProperties(FebProperties[] properties);
        public FebInfo[] getFebs();
        //public void setFebs(FebInfo[] febs);
    }
    public MassiveReadRequest createMassiveReadRequest(FebProperties[] properties, FebInfo[] febs);

    public interface FebValues {
        public FebInfo getFeb();
        public float[] getValues();
    }
    public FebValues createFebValues(int ccuAddress, int channel, int address, boolean endcap, float[] values);
    
    public interface MassiveReadResponse {
        public String[] getProperties();
        public FebValues[] getFebValues();
    }
    
    public abstract MassiveReadResponse massiveRead(
            MassiveReadRequest readRequest) throws RemoteException, ServiceException; 

    
    
    public interface MassiveWriteRequest {
        public FebProperties[] getProperties();
        public FebValues[] getFebValues();
    }
    public MassiveWriteRequest createMassiveWriteRequest(FebProperties[] properties, FebValues[] febValues);
    public abstract void massiveWrite(MassiveWriteRequest writeRequest) throws RemoteException, ServiceException;
    
}