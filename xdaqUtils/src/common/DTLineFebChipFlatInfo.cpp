#include "rpct/xdaqutils/DTLineFebChipFlatInfo.h"

#include "xdata/AbstractBag.h"

namespace rpct {
namespace xdaqutils {

DTLineFebChipFlatInfo::DTLineFebChipFlatInfo(int _chip_id, int _board_id
                                             , std::string const & _name
                                             , int _i2c_address
                                             , int _position
                                             , bool _chip_disabled, bool _board_disabled
                                             , std::string const & _location, std::string const & _partition)
    : chip_id_(_chip_id), board_id_(_board_id)
    , name_(_name)
    , i2c_address_(_i2c_address)
    , position_(_position)
    , chip_disabled_(_chip_disabled), board_disabled_(_board_disabled)
    , location_(_location), partition_(_partition)
{}

void DTLineFebChipFlatInfo::registerFields(xdata::AbstractBag * _bag)
{
    _bag->addField("chipId", &chip_id_);
    _bag->addField("boardId", &board_id_);
    _bag->addField("name", &name_);
    _bag->addField("i2cAddress", &i2c_address_);
    _bag->addField("position", &position_);
    _bag->addField("boardDisabled", &board_disabled_);
    _bag->addField("chipDisabled", &chip_disabled_);
    _bag->addField("location", &location_);
    _bag->addField("partition", &partition_);
    // _bag->addField("febLocationId", &feb_location_id_);
}

} // namespace xdaqutils
} // namespace rpct
