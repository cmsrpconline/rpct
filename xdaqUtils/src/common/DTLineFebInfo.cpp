#include "rpct/xdaqutils/DTLineFebInfo.h"

#include "xdata/AbstractBag.h"

namespace rpct {
namespace xdaqutils {

DTLineFebChipInfo::DTLineFebChipInfo(int _id
                                     , int _position
                                     , bool _disabled)
    : id_(_id)
    , position_(_position)
    , disabled_(_disabled)
{}

void DTLineFebChipInfo::registerFields(xdata::AbstractBag * _bag)
{
    _bag->addField("id", &id_);
    _bag->addField("position", &position_);
    _bag->addField("disabled", &disabled_);
}

DTLineFebInfo::DTLineFebInfo(int _id
                             , std::string const & _name
                             , int _i2c_address
                             , bool _disabled
                             , std::string const & _location
                             , std::string const & _partition
                             // , int _feb_location_id
                             , std::vector<DTLineFebChipInfo> const & _feb_chip_infos)
    : id_(_id)
    , name_(_name)
    , i2c_address_(_i2c_address)
    , disabled_(_disabled)
    , location_(_location)
    , partition_(_partition)
      // , feb_location_id_(_feb_location_id)
{
    setFebChipInfos(_feb_chip_infos);
}

DTLineFebInfo::DTLineFebInfo(int _id
                             , std::string const & _name
                             , int _i2c_address
                             , bool _disabled
                             , std::string const & _location
                             , std::string const & _partition
                             // , int _feb_location_id
                             , xdata::Vector<xdata::Bag<DTLineFebChipInfo> > const & _feb_chip_infos)
    : id_(_id)
    , name_(_name)
    , i2c_address_(_i2c_address)
    , disabled_(_disabled)
    , location_(_location)
    , partition_(_partition)
      // , feb_location_id_(_feb_location_id)
    , feb_chip_infos_(_feb_chip_infos)
{}

void DTLineFebInfo::registerFields(xdata::AbstractBag * _bag)
{
    _bag->addField("id", &id_);
    _bag->addField("name", &name_);
    _bag->addField("i2cAddress", &i2c_address_);
    _bag->addField("disabled", &disabled_);
    _bag->addField("location", &location_);
    _bag->addField("partition", &partition_);
    // _bag->addField("febLocationId", &feb_location_id_);
    _bag->addField("febChipInfos", &feb_chip_infos_);
}

} // namespace xdaqutils
} // namespace rpct
