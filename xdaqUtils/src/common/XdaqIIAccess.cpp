/*
 *  Author: Michal Pietrusinski
 *  Version: $Id: XdaqIIAccess.cpp,v 1.1 2008/11/21 11:55:42 tb Exp $
 *
 */

#include "rpct/xdaqutils/XdaqIIAccess.h"

#include "rpct/xdaqutils/CrateInfo.h"


#include "xdata/Serializable.h"
#include "xdata/exception/Exception.h"
#include "xdata/XStr.h"
#include "xdata/Float.h"
#include "xdata/Bag.h"
#include "xdata/Boolean.h"
#include "xdata/Integer.h"
#include "xdata/Binary.h"
#include "xoap/DOMParser.h"
#include "xcept/tools.h"
#include "tb_std.h"

using namespace std;
using namespace rpct::xdaqutils;
using namespace log4cplus;

namespace rpct {
namespace xdaqutils {


std::string XdaqIIAccess::GET_CRATES_INFO_NAME = "getCratesInfo";
std::string XdaqIIAccess::GET_BOARDS_INFO_NAME = "getBoardsInfo";
std::string XdaqIIAccess::GET_DEVICES_INFO_NAME = "getDevicesInfo";
std::string XdaqIIAccess::GET_DEVICEITEMS_INFO_NAME = "getDeviceItemsInfo";
std::string XdaqIIAccess::WRITE_II_NAME = "writeII";
std::string XdaqIIAccess::READ_II_NAME = "readII";

Logger XdaqIIAccess::logger = Logger::getInstance("XdaqIIAccess");
void XdaqIIAccess::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}
/*
XdaqIIAccess::XdaqIIAccess(xdaq::Application* app, Crates c) : crates(c), xdaqSoapAccess(app) {
    init();
}*/


XdaqIIAccess::XdaqIIAccess(xdaq::Application* app, System& system) : xdaqSoapAccess(app), system_(system) {
    /*System::CrateList& sysc = system.getCrates();
    for (System::CrateList::iterator iCrate = sysc.begin(); iCrate != sysc.end(); ++iCrate) {
        crates.push_back(*iCrate);
    }*/
    //init();
}
/*
void XdaqIIAccess::init() {
    for (Crates::iterator iCrate = crates.begin(); iCrate != crates.end(); ++iCrate) {
        ICrate::Boards& boards = (*iCrate)->getBoards();
        for (ICrate::Boards::iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {
            const IBoard::Devices& devices = (*iBoard)->getDevices();
            for (IBoard::Devices::const_iterator iDevice = devices.begin(); iDevice != devices.end(); ++iDevice) {
                mapDevice(*iDevice);
            }
        }
    }
}*/

/*void XdaqIIAccess::mapDevice(IDevice* device) {
    DevicesMap::iterator iEntry = devicesMap.find(device->getId());
    if (iEntry != devicesMap.end()) {
        cout << "Warning! Duplicate device with id = " << device->getId()
             << ", previous name = " << iEntry->second->getDescription()
             << ", current name = " << device->getDescription() << endl;
        return;
    }
    devicesMap.insert(DevicesMap::value_type(device->getId(), device));
}*/

/*
void XdaqIIAccess::mapHardwareItem(IHardwareItem* item) {
    HardwareItemsMap::iterator iEntry = hardwareItemsMap.find(item->getId());
    if (iEntry != hardwareItemsMap.end()) {
        cout << "Warning! Duplicate element with id = " << item->getId()
             << ", previous name = " << iEntry->second->getDescription()
             << ", current name = " << item->getDescription() << endl;
        return;
    }
    hardwareItemsMap.insert(HardwareItemsMap::value_type(item->getId(), item));
}*/


xoap::MessageReference XdaqIIAccess::getCratesInfo(xoap::MessageReference msg)
throw (xoap::exception::Exception) {

    xdata::Boolean nested;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("nested", &nested));
    xdaqSoapAccess.parseSOAPRequest(msg, GET_CRATES_INFO_NAME, "xdaq", XDAQ_NS_URI, paramMap);


    xdata::Vector<CrateInfoBag> crateInfos;
    System::CrateList& crates = system_.getCrates();
    for (System::CrateList::iterator iCrate = crates.begin(); iCrate != crates.end(); ++iCrate) {
        CrateInfoBag crateInfo;
        crateInfo.bag.setId((*iCrate)->getId());
        crateInfo.bag.setName((*iCrate)->getDescription());
        crateInfo.bag.setType((*iCrate)->getType().getType());

        if (nested) {

            ICrate::Boards& boards = (*iCrate)->getBoards();
            for (ICrate::Boards::iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {

                if ((*iBoard)->getDevices().empty()) {
                    continue;
                }

                BoardInfoBag boardInfo;
                boardInfo.bag.setId((*iBoard)->getId());
                boardInfo.bag.setName((*iBoard)->getDescription());
                boardInfo.bag.setType((*iBoard)->getType().getType());
                boardInfo.bag.setPosition((*iBoard)->getPosition());

                const IBoard::Devices& devices = (*iBoard)->getDevices();
                for (IBoard::Devices::const_iterator iDevice = devices.begin(); iDevice != devices.end(); ++iDevice) {
                    DeviceInfoBag deviceInfo;
                    deviceInfo.bag.setId((*iDevice)->getId());
                    deviceInfo.bag.setName((*iDevice)->getDescription());
                    deviceInfo.bag.setType((*iDevice)->getType().getType());
                    deviceInfo.bag.setPosition((*iDevice)->getPosition());

                    const IDevice::TItemsMap& itemsMap = (*iDevice)->getItems();
                    for (IDevice::TItemsMap::const_iterator iItemEntry = itemsMap.begin(); iItemEntry != itemsMap.end(); ++iItemEntry) {
                        DeviceItemInfoBag deviceItemInfo;
                        std::string type;
                        switch (iItemEntry->second.Type) {
                        case IDevice::itPage:
                            type = "PAGE";
                            break;
                        case IDevice::itArea:
                            type = "AREA";
                            break;
                        case IDevice::itWord:
                            type = "WORD";
                            break;
                        case IDevice::itVect:
                            type = "VECT";
                            break;
                        case IDevice::itBits:
                            type = "BITS";
                            break;
                        default:
                            XCEPT_RAISE(xoap::exception::Exception, "Unrecognized item type");
                        }
                        std::string accessType;
                        switch (iItemEntry->second.AccessType) {
                        case IDevice::atNone:
                            accessType = "NONE";
                            break;
                        case IDevice::atRead:
                            accessType = "READ";
                            break;
                        case IDevice::atWrite:
                            accessType = "WRITE";
                            break;
                        case IDevice::atAll:
                            accessType = "BOTH";
                            break;
                        default:
                            XCEPT_RAISE(xoap::exception::Exception, "Unrecognized item access type");
                        }

                        deviceItemInfo.bag.init(iItemEntry->second.ID,
                                iItemEntry->second.Name,
                                iItemEntry->second.Description,
                                type,
                                iItemEntry->second.Width,
                                iItemEntry->second.Number,
                                iItemEntry->second.ParentID,
                                accessType);

                        deviceInfo.bag.getItems().push_back(deviceItemInfo);
                    }

                    boardInfo.bag.getDevices().push_back(deviceInfo);
                }

                crateInfo.bag.getBoards().push_back(boardInfo);
            }
        }


        crateInfos.push_back(crateInfo);
    }

    try {
        return xdaqSoapAccess.sendSOAPResponse(GET_CRATES_INFO_NAME, "xdaq", XDAQ_NS_URI, crateInfos);
    }
    catch (TException& e) {
        //LOG4CPLUS_ERROR(getApplicationLogger(), e.what());
        XCEPT_RAISE(xoap::exception::Exception, e.what());
    }
}



xoap::MessageReference XdaqIIAccess::getBoardsInfo(xoap::MessageReference msg)
throw (xoap::exception::Exception) {


    xdata::Integer crateId;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("crateId", &crateId));
    xdaqSoapAccess.parseSOAPRequest(msg, GET_BOARDS_INFO_NAME, "xdaq", XDAQ_NS_URI, paramMap);

    LOG4CPLUS_DEBUG(logger, __FUNCTION__ << " crateId = " << crateId.value_ );

    try {
        ICrate& crate = system_.getCrateById(crateId.value_);

        xdata::Vector<BoardInfoBag> boardInfos;

        ICrate::Boards& boards = crate.getBoards();
        for (ICrate::Boards::iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {

            if ((*iBoard)->getDevices().empty()) {
                continue;
            }

            BoardInfoBag boardInfo;
            boardInfo.bag.setId((*iBoard)->getId());
            boardInfo.bag.setName((*iBoard)->getDescription());
            boardInfo.bag.setType((*iBoard)->getType().getType());
            boardInfo.bag.setPosition((*iBoard)->getPosition());

            boardInfos.push_back(boardInfo);
        }

        return xdaqSoapAccess.sendSOAPResponse(GET_BOARDS_INFO_NAME, "xdaq", XDAQ_NS_URI, boardInfos);
    }
    catch (TException& e) {
        //LOG4CPLUS_ERROR(getApplicationLogger(), e.what());
        XCEPT_RAISE(xoap::exception::Exception, e.what());
    }
}

xoap::MessageReference XdaqIIAccess::getDevicesInfo(xoap::MessageReference msg)
throw (xoap::exception::Exception) {

    xdata::Integer boardId;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("boardId", &boardId));
    xdaqSoapAccess.parseSOAPRequest(msg, GET_DEVICES_INFO_NAME, "xdaq", XDAQ_NS_URI, paramMap);

    LOG4CPLUS_DEBUG(logger, __FUNCTION__ << " boardId = " << boardId.value_ );

    try {
        IBoard& board = system_.getBoardById(boardId.value_);

        xdata::Vector<DeviceInfoBag> deviceInfos;

        const IBoard::Devices& devices = board.getDevices();
        for (IBoard::Devices::const_iterator iDevice = devices.begin(); iDevice != devices.end(); ++iDevice) {

            DeviceInfoBag deviceInfo;
            deviceInfo.bag.setId((*iDevice)->getId());
            deviceInfo.bag.setName((*iDevice)->getDescription());
            deviceInfo.bag.setType((*iDevice)->getType().getType());
            deviceInfo.bag.setPosition((*iDevice)->getPosition());

            deviceInfos.push_back(deviceInfo);
        }

        return xdaqSoapAccess.sendSOAPResponse(GET_DEVICES_INFO_NAME, "xdaq", XDAQ_NS_URI, deviceInfos);
    }
    catch (TException& e) {
        //LOG4CPLUS_ERROR(getApplicationLogger(), e.what());
        XCEPT_RAISE(xoap::exception::Exception, e.what());
    }
}


xoap::MessageReference XdaqIIAccess::getDeviceItemsInfo(xoap::MessageReference msg)
throw (xoap::exception::Exception) {

    xdata::Integer deviceId;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("deviceId", &deviceId));
    xdaqSoapAccess.parseSOAPRequest(msg, GET_DEVICEITEMS_INFO_NAME, "xdaq", XDAQ_NS_URI, paramMap);

    LOG4CPLUS_DEBUG(logger, __FUNCTION__ << " deviceId = " << deviceId.value_ );

    try {
        IDevice& device = system_.getDeviceById(deviceId.value_);

        xdata::Vector<DeviceItemInfoBag> deviceItemInfos;

        const IDevice::TItemsMap& itemsMap = device.getItems();
        for (IDevice::TItemsMap::const_iterator iItemEntry = itemsMap.begin(); iItemEntry != itemsMap.end(); ++iItemEntry) {
            DeviceItemInfoBag deviceItemInfo;
            std::string type;
            switch (iItemEntry->second.Type) {
            case IDevice::itPage:
                type = "PAGE";
                break;
            case IDevice::itArea:
                type = "AREA";
                break;
            case IDevice::itWord:
                type = "WORD";
                break;
            case IDevice::itVect:
                type = "VECT";
                break;
            case IDevice::itBits:
                type = "BITS";
                break;
            default:
                XCEPT_RAISE(xoap::exception::Exception, "Unrecognized item type");
            }
            std::string accessType;
            switch (iItemEntry->second.AccessType) {
            case IDevice::atNone:
                accessType = "NONE";
                break;
            case IDevice::atRead:
                accessType = "READ";
                break;
            case IDevice::atWrite:
                accessType = "WRITE";
                break;
            case IDevice::atAll:
                accessType = "BOTH";
                break;
            default:
                XCEPT_RAISE(xoap::exception::Exception, "Unrecognized item access type");
            }

            deviceItemInfo.bag.init(iItemEntry->second.ID,
                    iItemEntry->second.Name,
                    iItemEntry->second.Description,
                    type,
                    iItemEntry->second.Width,
                    iItemEntry->second.Number,
                    iItemEntry->second.ParentID,
                    accessType);

            deviceItemInfos.push_back(deviceItemInfo);
        }

        return xdaqSoapAccess.sendSOAPResponse(GET_DEVICEITEMS_INFO_NAME, "xdaq", XDAQ_NS_URI, deviceItemInfos);
    }
    catch (TException& e) {
        //LOG4CPLUS_ERROR(getApplicationLogger(), e.what());
        XCEPT_RAISE(xoap::exception::Exception, e.what());
    }
}


xoap::MessageReference XdaqIIAccess::readII(xoap::MessageReference msg)
throw (xoap::exception::Exception) {

    xdata::Integer deviceId;
    xdata::Integer deviceItemId;
    xdata::Integer startPos;
    xdata::Integer count;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("deviceId", &deviceId));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("deviceItemId", &deviceItemId));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("startPos", &startPos));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("count", &count));
    xdaqSoapAccess.parseSOAPRequest(msg, READ_II_NAME, "xdaq", XDAQ_NS_URI, paramMap);

    try {
        /*DevicesMap::iterator iDevice = devicesMap.find((deviceId));
        if (iDevice == devicesMap.end()) {
            XCEPT_RAISE(xoap::exception::Exception, "Could not find device with id = " + deviceId.toString());
        }

        IDevice* device = iDevice->second;*/
        IDevice* device = &system_.getDeviceById(deviceId);

        typedef xdata::Vector<xdata::Binary> Data;
        Data data;

        const IDevice::TItemDesc& desc = device->getItemDesc((int)deviceItemId);
        LOG4CPLUS_INFO(logger, __FUNCTION__<<":"<<__LINE__<<" reading " << device->getDescription() << '.' << desc.Name);
        if (desc.Type == IDevice::itWord) {
            int size = (desc.Width-1) / 8 + 1;
            unsigned char* buffer = new unsigned char[size];
            try {
                int max = (int)startPos + (int)count;
                for (int i = (int)startPos; i < max; i++) {
                    device->readWord((int)deviceItemId, i, buffer);
                    xdata::Binary val(buffer, size);
                    //cout << " value = " << val.toString() << endl;
                    data.push_back(val);
                }
                delete [] buffer;
            }
            catch (...) {
                delete [] buffer;
                throw;
            }
        }
        else if (desc.Type == IDevice::itVect) {
            int size = (desc.Width-1) / 8 + 1;
            unsigned char* buffer = new unsigned char[size];
            try {
                device->readVector((int)deviceItemId, buffer);
                xdata::Binary val(buffer, size);
                //cout << " value = " << val.toString() << endl;
                data.push_back(val);
            }
            catch (...) {
                delete [] buffer;
                throw;
            }
        }
        else if (desc.Type == IDevice::itBits) {
            uint32_t ul;
            int max = (int)startPos + (int)count;
            for (int i = (int)startPos; i < max; i++) {
                ul = device->readBits((int)deviceItemId); // !!! index is not implemented in readBits
                xdata::Binary val(ul);
                data.push_back(val);
            }
        }
        else if (desc.Type == IDevice::itArea) {
            size_t width = (desc.Width-1) / 8 + 1;
            size_t pos = (int)startPos;
            size_t cnt = (int)count;
            if (cnt == 0) {
                cnt = desc.Number - pos;
            }
            else if (cnt > (desc.Number - pos)) {
                cnt = desc.Number - pos;
            }
            size_t size = width * cnt;
            unsigned char* buffer = new unsigned char[size];
            try {
                device->readArea((int)deviceItemId, buffer, pos, cnt);
                /*for (size_t t = 0; t < size; t++) {
                    cout << hex << (*(unsigned int*)(buffer + t));
                }
                cout << endl;*/
                unsigned char* pdata = buffer;
                for (size_t i = 0; i < cnt; i++) {
                    xdata::Binary val(pdata, width);
                    data.push_back(val);
                    pdata += width;
                }
                delete [] buffer;
            }
            catch (...) {
                delete [] buffer;
                throw;
            }
        }

        return xdaqSoapAccess.sendSOAPResponse(READ_II_NAME, "xdaq", XDAQ_NS_URI, data);
    }
    catch (TException& e) {
        //LOG4CPLUS_ERROR(getApplicationLogger(), e.what());
        XCEPT_RAISE(xoap::exception::Exception, e.what());
    }
}

xoap::MessageReference XdaqIIAccess::writeII(xoap::MessageReference msg)
throw (xoap::exception::Exception) {

    typedef xdata::Vector<xdata::Binary> Data;
    xdata::Integer deviceId;
    xdata::Integer deviceItemId;
    xdata::Integer startPos;
    Data data;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("deviceId", &deviceId));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("deviceItemId", &deviceItemId));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("startPos", &startPos));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("data", &data));
    xdaqSoapAccess.parseSOAPRequest(msg, WRITE_II_NAME, "xdaq", XDAQ_NS_URI, paramMap);

    if (logger.isEnabledFor(DEBUG_LOG_LEVEL)) {
        for (Data::iterator iData = data.begin(); iData != data.end(); ++iData) {
            LOG4CPLUS_DEBUG(logger, "writeII :" << iData->toString());
        }
    }

    try {
        /*DevicesMap::iterator iDevice = devicesMap.find((deviceId));
        if (iDevice == devicesMap.end()) {
            XCEPT_RAISE(xoap::exception::Exception, "Could not find device with id = " + deviceId.toString());
        }
        IDevice* device = iDevice->second;*/
        IDevice* device = &system_.getDeviceById(deviceId);

        const IDevice::TItemDesc& desc = device->getItemDesc((int)deviceItemId);


        if (logger.isEnabledFor(DEBUG_LOG_LEVEL)) {
            LOG4CPLUS_DEBUG(logger, "writeII " << device->getDescription() << '.' << desc.Name << " startPos = " << startPos);
            for (Data::iterator iData = data.begin(); iData != data.end(); ++iData) {
                LOG4CPLUS_DEBUG(logger, "writeII :" << iData->toString());
            }
        }
        else
        	LOG4CPLUS_INFO(logger, __FUNCTION__<<":"<<__LINE__<<" writing "<< desc.Name << '.' << desc.Name << " startPos = " << startPos);

        //cout << "Writing " << device->getDescription() << '.' << desc.Name << endl;
        if (desc.Type == IDevice::itWord) {
            size_t size = (desc.Width-1) / 8 + 1;
            unsigned char* buffer = new unsigned char[size];
            try {
                for (size_t i = 0; i < data.size(); i++) {
                    if (data[i].size() > size) {
                        XCEPT_RAISE(xoap::exception::Exception,
                            toolbox::toString("Value size %d exceeds register capacity %d", data[i].size(), size));
                    }
                    memset(buffer, 0, size);
                    memcpy(buffer, data[i].buffer(), data[i].size());
                    device->writeWord((int)deviceItemId, i + (int)startPos, buffer);
                }
                delete [] buffer;
            }
            catch (...) {
                delete [] buffer;
                throw;
            }
        }
        else if (desc.Type == IDevice::itBits) {
            int max = (int)startPos + data.size();
            for (int i = (int)startPos; i < max; i++) {
                device->writeBits((int)deviceItemId, (uint32_t)data[i]); // !!! index is not implemented in readBits
            }
        }
        else if (desc.Type == IDevice::itArea) {
            LOG4CPLUS_DEBUG(logger, "writeII : area");
            size_t width = (desc.Width-1) / 8 + 1;
            size_t pos = (int)startPos;
            size_t cnt = data.size();
            if (cnt > (desc.Number - pos)) {
                cnt = desc.Number - pos;
            }
            size_t size = width * cnt;
            unsigned char* buffer = new unsigned char[size];
            memset(buffer, 0, size);
            try {
                unsigned char* pdata = buffer;
                for (size_t i = 0; i < cnt; i++) {
                    data[i].copy(pdata);
                    pdata += width;
                }
                device->writeArea((int)deviceItemId, buffer, pos, cnt);
                delete [] buffer;
            }
            catch (...) {
                delete [] buffer;
                throw;
            }
        }

        xoap::MessageReference reply = xoap::createMessage();
        return reply;
    }
    catch (TException& e) {
        //LOG4CPLUS_ERROR(getApplicationLogger(), e.what());
        XCEPT_RAISE(xoap::exception::Exception, e.what());
    }
}

}}
