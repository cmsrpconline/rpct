/*
 *  Author: Michal Pietrusinski
 *  Version: $Id: XdaqSoapAccess.cpp,v 1.12 2008/09/30 21:48:45 tb Exp $
 *
 */

#include "rpct/xdaqutils/XdaqSoapAccess.h"

#include "rpct/xdaqutils/RemoteException.h"

#include "xdaq/ApplicationGroup.h"
#include "xdata/soap/NamespaceURI.h"
#include "xdata/Serializable.h"
#include "xdata/exception/Exception.h"
#include "xdata/XStr.h"
#include "xdata/Float.h"
#include "xdata/Bag.h"
#include "xdata/Boolean.h"
#include "xdata/Integer.h"
#include "xdata/Binary.h"
#include "xoap/DOMParser.h"
#include "xcept/tools.h"
#include "tb_std.h"


using namespace std;

namespace rpct {
namespace xdaqutils {

void XdaqSoapAccess::parseSOAPRequest(xoap::MessageReference msg, std::string functionName,
                                      std::string prefix, std::string uri, TSOAPParamMap& paramMap) {
    try {
        typedef vector<xoap::SOAPElement> Elements;
        //xdata::rpctsoap::Serializer serializer;

        xoap::SOAPName sName(functionName, prefix, uri);
        Elements elements = msg->getSOAPPart().getEnvelope().getBody().getChildElements(sName);
        if (elements.size() != 1) {
            std::string errorMsg("Syntax error - no child elements found for function " + functionName);
            //LOG4CPLUS_ERROR(getApplicationLogger(), errorMsg);
            XCEPT_RAISE(xcept::Exception, errorMsg);
        }
        xoap::SOAPElement reqElement = elements.front();
        Elements paramElements = reqElement.getChildElements();
        for (Elements::iterator iter = paramElements.begin(); iter != paramElements.end(); iter++) {
            TSOAPParamMap::iterator iParam = paramMap.find(iter->getElementName().getLocalName());
            if (iParam != paramMap.end()) {
                serializer.import(iParam->second, iter->getDOM());
            }
        }
    }
    catch(xcept::Exception& e) {
        //LOG4CPLUS_ERROR(getApplicationLogger(), xcept::stdformat_exception_history(e) );
        XCEPT_RAISE(xoap::exception::Exception, e.what());
    }
}

xoap::MessageReference XdaqSoapAccess::sendSOAPResponse(std::string functionName, std::string prefix,
        std::string uri, xdata::Serializable& value) {
    xoap::MessageReference reply = xoap::createMessage();
    xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
    xoap::SOAPName responseName = envelope.createName(functionName + "Response", prefix, uri);
    xoap::SOAPBodyElement responseElement = envelope.getBody().addBodyElement ( responseName );
    responseElement.addNamespaceDeclaration("xsi","http://www.w3.org/2001/XMLSchema-instance");
    responseElement.addNamespaceDeclaration("xsd","http://www.w3.org/2001/XMLSchema");
    xoap::SOAPName returnName = envelope.createName(functionName + "Return", prefix, uri);
    xoap::SOAPElement returnElement = responseElement.addChildElement(returnName);
    serializer.exportAll(&value, dynamic_cast<DOMElement*>(returnElement.getDOMNode()), true);

    return reply;
}


xoap::MessageReference XdaqSoapAccess::sendSOAPRequest(std::string functionName, std::string prefix,
        std::string uri, TSOAPParamList& paramList, std::string application, int instance, bool external) {

    const xdaq::ApplicationDescriptor * orig = this->application->getApplicationDescriptor();
    const xdaq::ApplicationDescriptor* dest = this->application->getApplicationContext()
        ->getDefaultZone()->getApplicationDescriptor(application, instance);
    xoap::MessageReference request = xoap::createMessage();
    xoap::SOAPEnvelope envelope = request->getSOAPPart().getEnvelope();
    envelope.addNamespaceDeclaration("xsi", "http://www.w3.org/2001/XMLSchema-instance");
    envelope.addNamespaceDeclaration("xsd", "http://www.w3.org/2001/XMLSchema");

    xoap::SOAPBody body = envelope.getBody();
    xoap::SOAPName command = envelope.createName(functionName, prefix, uri);
    xoap::SOAPElement bodyElement = body.addBodyElement(command);
    // xoap::SOAPName encodingStyle = envelope.createName("encodingStyle","env", "http://schemas.xmlsoap.org/soap/envelope/");
    // bodyElement.addAttribute(encodingStyle, SOAPENC_NAMESPACE_URI);

    for (TSOAPParamList::iterator iParam = paramList.begin(); iParam != paramList.end(); ++iParam) {
        xoap::SOAPName paramName = envelope.createName(iParam->first, prefix, uri);
        xoap::SOAPElement paramElement = bodyElement.addChildElement(paramName);
        serializer.exportAll(iParam->second, dynamic_cast<DOMElement*>(paramElement.getDOMNode()), true);
    }
    if (external) {
        request->getMimeHeaders()->setHeader("SOAPAction", functionName);
    }
    //cout << "sendSOAPRequest:\n";
    //request->writeTo(cout);
    //cout << endl;
    // temporary putting back the deprecated version to check timeout times
    try{
        // xoap::MessageReference response =  this->application->getApplicationContext()->postSOAP(request, dest);
        xoap::MessageReference response =  this->application->getApplicationContext()->postSOAP(request, *orig, *dest);

        LOG4CPLUS_DEBUG(this->application->getApplicationLogger(), "received response for function " + functionName);
        return response;
    }catch (xcept::Exception& e) {
        //LOG4CPLUS_ERROR(getApplicationLogger(), xcept::stdformat_exception_history(e) );
        stringstream msg;
        msg << "exception while sending request to ";
        msg << dest->getURN() << ", " << uri << " : ";
	//        request->writeTo(msg);
        msg << functionName;
	XCEPT_RETHROW(xoap::exception::Exception, msg.str(),e);
    }
}


void XdaqSoapAccess::parseSOAPResponse(xoap::MessageReference msg, std::string functionName,
                                       std::string prefix, std::string uri, xdata::Serializable& value) {
    try {
        typedef vector<xoap::SOAPElement> Elements;

        // Check if the reply indicates a fault occurred
        xoap::SOAPBody responseBody = msg->getSOAPPart().getEnvelope().getBody();

        if (responseBody.hasFault()) {
            ostringstream ostr;
            ostr << "Received fault reply from function " << prefix << ':' << functionName
            << ", uri " << uri
            << ": " << responseBody.getFault().getFaultString();

            XCEPT_RAISE(RemoteException, ostr.str());
        }

        xoap::SOAPName sResponseName(functionName + "Response", prefix, uri);

        Elements responseElements = responseBody.getChildElements(sResponseName);
        if (responseElements.size() != 1) {
            std::string errorMsg("Syntax error - wrong number of child elements found in response for function " + functionName);
            //LOG4CPLUS_ERROR(getApplicationLogger(), errorMsg);
            XCEPT_RAISE(RemoteException, errorMsg);
        }

        xoap::SOAPName sReturnName(functionName + "Return", prefix, uri);
        Elements returnElements = responseElements[0].getChildElements(sReturnName);
        if (returnElements.size() != 1) {
            std::string errorMsg("Syntax error - wrong number of return elements found in response for function " + functionName);
            //LOG4CPLUS_ERROR(getApplicationLogger(), errorMsg);
            XCEPT_RAISE(RemoteException, errorMsg);
        }
        serializer.import(&value, returnElements[0].getDOM());
    }
    catch (RemoteException&) {
        throw;
    }
    catch (xcept::Exception& e) {
        //LOG4CPLUS_ERROR(getApplicationLogger(), xcept::stdformat_exception_history(e) );
        XCEPT_RAISE(xoap::exception::Exception, e.what());
    }
}


void XdaqSoapAccess::parseSOAPResponse(xoap::MessageReference msg, std::string functionName,
                                       std::string prefix, std::string uri) {
    try {
        typedef vector<xoap::SOAPElement> Elements;

        // Check if the reply indicates a fault occurred
        xoap::SOAPBody responseBody = msg->getSOAPPart().getEnvelope().getBody();

        if (responseBody.hasFault()) {
            ostringstream ostr;
            ostr << "Received fault reply from function " << prefix << ':' << functionName
            << ", uri " << uri
            << ": " << responseBody.getFault().getFaultString();

            XCEPT_RAISE(RemoteException, ostr.str());
        }

        xoap::SOAPName sResponseName(functionName + "Response", prefix, uri);

        /*Elements responseElements = responseBody.getChildElements(sResponseName);
        if (responseElements.size() != 1) {
            std::string errorMsg("Syntax error - wrong number of child elements found in response for function " + functionName);
            //LOG4CPLUS_ERROR(getApplicationLogger(), errorMsg);
            XCEPT_RAISE(RemoteException, errorMsg);
        }

        xoap::SOAPName sReturnName(functionName + "Return", prefix, uri);
        Elements returnElements = responseElements[0].getChildElements(sReturnName);
        if (returnElements.size() != 1) {
            std::string errorMsg("Syntax error - wrong number of return elements found in response for function " + functionName);
            //LOG4CPLUS_ERROR(getApplicationLogger(), errorMsg);
            XCEPT_RAISE(RemoteException, errorMsg);
        }
        serializer.import(&value, returnElements[0].getDOM());*/
    }
    catch (RemoteException&) {
        throw;
    }
    catch (xcept::Exception& e) {
        //LOG4CPLUS_ERROR(getApplicationLogger(), xcept::stdformat_exception_history(e) );
        XCEPT_RAISE(xoap::exception::Exception, e.what());
    }
}


}}
