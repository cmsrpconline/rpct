#include "rpct/xdaqutils/XdaqDbSystemBuilder.h"

//#include "rpct/devices/XmlSystemBuilder.h"
#include "rpct/lboxaccess/FecManagerImpl.h"
#include "rpct/lboxaccess/test/FecManagerMock.h"
#include "rpct/devices/VmeCrate.h"
#include "rpct/devices/TriggerCrate.h"
#include "rpct/devices/SorterCrate.h"
#include "rpct/std/IllegalArgumentException.h"

#include "xoap/domutils.h"
#include "xdaq/exception/Exception.h"

#include "rpct/devices/LinkBoard.h"
#include "rpct/devices/cb.h"
#include "rpct/devices/TriggerBoard.h"
#include "rpct/devices/tcsort.h"
#include "rpct/devices/hsb.h"
#include "rpct/devices/fsb.h"
#include "rpct/devices/TTUBoard.h"
#include "rpct/devices/TTUBackplane.h"
#include "rpct/devices/Rbc.h"

#include "rpct/devices/RPCFebSystem.h"
#include "rpct/devices/RPCFebAccessPoint.h"

#include <xercesc/parsers/SAXParser.hpp>
#include <sstream>

using namespace xoap;
using namespace std;
using namespace log4cplus;

namespace rpct {
namespace xdaqutils {

Logger XdaqDbSystemBuilder::logger = Logger::getInstance("XdaqDbSystemBuilder");
void XdaqDbSystemBuilder::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
	logger.setLogLevel(log4cplus::INFO_LOG_LEVEL);
}

XdaqDbSystemBuilder::XdaqDbSystemBuilder(xdaq::Application* app, bool m) :
    mock(m), dbServiceClient(app) {
}

ChipInfo& XdaqDbSystemBuilder::getChipInfo(BoardInfo& boardInfo, int index) {
    BoardInfo::ChipInfoVector& chipInfos = boardInfo.getChipInfos();

    if (index >= chipInfos.size()) {
        ostringstream ostr;
        ostr << "Invalid number of chips (" << chipInfos.size() << ") for "
                << ((string) boardInfo.getName());
        LOG4CPLUS_ERROR(logger, ostr.str());
        throw TException(ostr.str());
    }
    return chipInfos[index].bag;
}

ChipInfo& XdaqDbSystemBuilder::extractExpectedChipInfo(BoardInfo& boardInfo, string chipType) {

    BoardInfo::ChipInfoVector& chipInfos = boardInfo.getChipInfos();

    /*if (chipInfos.size() != 1) {
     ostringstream ostr;
     ostr << "Invalid number of chips (" << chipInfos.size()
     << ") for " << ((string)boardInfo.getName());
     LOG4CPLUS_ERROR(logger, ostr.str());
     throw TException(ostr.str());
     }*/
    ChipInfo& chipInfo = getChipInfo(boardInfo, 0);
    if (chipInfo.isDisabled()) {
        LOG4CPLUS_WARN(logger, "Disabled chip id " << ((int)chipInfo.getId())
                << " board " << ((string)boardInfo.getName()));
    } else {
        if (chipInfo.getType() != chipType) {
            ostringstream ostr;
            ostr << "Invalid type of chip (" << ((string) chipInfo.getType()) << ") for board "
                    << ((string) boardInfo.getName());
            LOG4CPLUS_ERROR(logger, ostr.str());
            throw TException(ostr.str());
        }
        LOG4CPLUS_DEBUG(logger, "chip id " << ((int)chipInfo.getId())
                << " pos = " << boardInfo.getPositionOrAddress());

    }
    return chipInfo;
}

void XdaqDbSystemBuilder::buildFebSystem(RPCFebSystem & febsystem, CcuRingInfo::FebBoardInfoVector & febBoardInfos)
{
    febsystem.reset();
    CcuRingInfo::FebBoardInfoVector::iterator IfivEnd = febBoardInfos.end();
    for (CcuRingInfo::FebBoardInfoVector::iterator Ifiv = febBoardInfos.begin()
             ; Ifiv != IfivEnd
             ; ++Ifiv)
        {
            FebBoardInfo & fbi = Ifiv->bag;
            // this is the only point where we know it's an RPC controlled one
            bool _disabled(fbi.isDisabled().value_ || fbi.isDTControlled());
            // avoid use of the FebDistributionBoard if its FEBs are all disabled or controlled via the DT-line
            if (!_disabled)
                {
                    BoardInfo::ChipInfoVector & civ = fbi.getChipInfos();
                    try {
                        FebBoard & fb =
                            febsystem.addRPCFebAccessPoint(fbi.getControlBoardId()).
                            addFebDistributionBoard(fbi.getId()
                                                    , fbi.getName()
                                                    , fbi.getControlBoardChannel()).
                            addFebBoard(fbi.getId()
                                        , fbi.getName()
                                        , (civ.size()>2?1:0)
                                        , fbi.getI2cAddress()
                                        , _disabled);
                        bool _disable_febboard(true);
                        BoardInfo::ChipInfoVector::iterator IcivEnd = civ.end();
                        for (BoardInfo::ChipInfoVector::iterator Iciv = civ.begin()
                                 ; Iciv != IcivEnd
                                 ; ++Iciv) {
                            fb.addFebChip(Iciv->bag.getId(), Iciv->bag.getPosition(), _disabled || Iciv->bag.isDisabled());
                            if (! Iciv->bag.isDisabled())
                                _disable_febboard = false;
                        }
                        // avoid PCF8574a monitoring if all chips are disabled
                        if (_disable_febboard)
                            fb.disable(true);
                    } catch(TException & e) {
                        LOG4CPLUS_ERROR(logger, "Error building FEB System: " << e.what());
                    }
                }
            else
                {
                    LOG4CPLUS_INFO(logger, "Not building " << (fbi.isDisabled() ? "disabled" : "DT-line-controlled") << " FebBoard " << fbi.getName().value_);
                }
        }
}
void XdaqDbSystemBuilder::buildCcuRing(LinkSystem& system, std::string host, int port, int instance, RPCFebSystem * febsystem)
{
  LOG4CPLUS_INFO(logger, "buildCcuRing for " << host << ":" << port << " instance " << instance);
    CcuRingInfoBag* ccuRingInfo = dbServiceClient.getCcuRingInfoForXdaqApp(host, port, instance);
    try {
        LOG4CPLUS_DEBUG(logger, "linkBoxes count " << ccuRingInfo->bag.getLinkBoxInfos().size());
        FecManager* fecManager;
        if (mock) {
            fecManager = new FecManagerMock(ccuRingInfo->bag.getPciSlot(),
                    ccuRingInfo->bag.getCcsPosition(), ccuRingInfo->bag.getFecPosition());
        } else {
        	LOG4CPLUS_DEBUG(logger, "creating FecManagerImpl: PciSlot" << ccuRingInfo->bag.getPciSlot()<<" CcsPosition "<<ccuRingInfo->bag.getCcsPosition());
            fecManager = new FecManagerImpl(ccuRingInfo->bag.getPciSlot(),
                    ccuRingInfo->bag.getCcsPosition(), ccuRingInfo->bag.getFecPosition());
        }
        system.addFecManager(*fecManager);

        CcuRingInfo::LinkBoxInfoVector& lbbInfos = ccuRingInfo->bag.getLinkBoxInfos();
        for (CcuRingInfo::LinkBoxInfoVector::iterator iLbox = lbbInfos.begin(); iLbox
                != lbbInfos.end(); ++iLbox) {

            LinkBoxInfo& lboxInfo = iLbox->bag;
            if (lboxInfo.isDisabled()) {
                LOG4CPLUS_WARN(logger, "Disabled lbox " << ((int)lboxInfo.getId())
                        << " name = " << ((string)lboxInfo.getName()));
                continue;
            }

            int addr10 = (int) lboxInfo.getCcu10Address();
            int addr20 = (int) lboxInfo.getCcu20Address();
            LOG4CPLUS_DEBUG(logger, "LinkBoxInfo: addr10=" << addr10 << " addr20=" << addr20);
            bool ccu10 = addr10 != 0;
            bool ccu20 = addr20 != 0;

            int cb10Id = -1;
            int cb20Id = -1;

            LinkBoxInfo::BoardInfoVector& lbInfos = lboxInfo.getBoardInfos();
            for (LinkBoxInfo::BoardInfoVector::iterator iLb = lbInfos.begin(); iLb != lbInfos.end(); ++iLb) {
                BoardInfo& boardInfo = iLb->bag;
                if (boardInfo.getType() == "CONTROLBOARD") {
                	int pos = boardInfo.getPositionOrAddress();
                	if (boardInfo.isDisabled()) {
                        LOG4CPLUS_WARN(logger, "Disabled HALFBOX " << ((string)lboxInfo.getName())
                                << " board name = " << ((string)boardInfo.getName()));
                        if (pos == 10) {
                            ccu10 = false;
                        }
                        else if (pos == 20) {
                            ccu20 = false;
                        }
                        else {
                            throw TException("buildCcuRing: not correct CB position");
                        }
                    }
                    else {
                        if (pos == 10) {
                        	cb10Id = boardInfo.getId();
                        }
                        else if (pos == 20) {
                        	cb20Id = boardInfo.getId();
                        }
                    }

                }
            }

            ///////////////////////////////////////////////////////////////// - MC
            LOG4CPLUS_DEBUG(logger, "Creating new LinkBox instance: (Id="<<((int)lboxInfo.getId())
                    <<" Name="<<((string)lboxInfo.getName())
                    <<" ccu_addr10="<<(int)addr10
                    <<" ccu_addr20="<<(int)addr20
                    <<" mock="<<(int)mock<<")" ); // MC
            {
                LOG4CPLUS_DEBUG(logger, "+++++ BEFORE SYSTEM.ADDLINKBOX");
                LinkBoxInfo::BoardInfoVector& lbInfos = lboxInfo.getBoardInfos();
                for (LinkBoxInfo::BoardInfoVector::iterator iLb = lbInfos.begin(); iLb
                        != lbInfos.end(); ++iLb) {
                    BoardInfo& boardInfo = iLb->bag;
                    if (!boardInfo.isDisabled()) {
                        LOG4CPLUS_DEBUG(logger, "LBOX=" << ((string)lboxInfo.getName())
                                << " BOARD: Id=" << ((int)boardInfo.getId())
                                << " Label=" << ((string)boardInfo.getLabel())
                                << " Pos="<< ((int)boardInfo.getPositionOrAddress())
                                << " Type=" << ((string)boardInfo.getType()) );
                    }
                }
                LOG4CPLUS_DEBUG(logger, "+++++ BEFORE SYSTEM.ADDLINKBOX");
            }
            //////////////////////////////////////////////////////////////// - MC

            LinkBox* lbox = new LinkBox((int) lboxInfo.getId(),
                    ((std::string) lboxInfo.getName()).c_str(),
                    &system,
                    ccu10 ? &fecManager->addLinkBoxAccess(addr10) : 0,
                    ccu20 ? &fecManager->addLinkBoxAccess(addr20) : 0,
                    cb10Id, cb20Id, mock, lboxInfo.getLabel().value_);

            if(ccu10)
            	system.registerItem(lbox->getHalfBox10()->getCb());

            if(ccu20)
            	system.registerItem(lbox->getHalfBox20()->getCb());

            //////////////////////////////////////////////////////////////// - MC
            LOG4CPLUS_DEBUG(logger, "LinkBox instance "<<hex<<(void*)lbox<<" before system.addLinkBox"); // MC
            //////////////////////////////////////////////////////////////// - MC

            system.addLinkBox(*lbox);

            //////////////////////////////////////////////////////////////// - MC
            LOG4CPLUS_DEBUG(logger, "LinkBox instance "<<hex<<(void*)lbox<<" added to system"); // MC
            //////////////////////////////////////////////////////////////// - MC

            //////////////////////////////////////////////////////////////// - MC
            {
                LOG4CPLUS_DEBUG(logger, "+++++ AFTER SYSTEM.ADDLINKBOX");
                LinkBoxInfo::BoardInfoVector& lbInfos = lboxInfo.getBoardInfos();
                for (LinkBoxInfo::BoardInfoVector::iterator iLb = lbInfos.begin(); iLb
                        != lbInfos.end(); ++iLb) {
                    BoardInfo& boardInfo = iLb->bag;
                    if (!boardInfo.isDisabled()) {
                        LOG4CPLUS_DEBUG(logger, "LBOX=" << ((string)lboxInfo.getName())
                                << " BOARD: Id=" << ((int)boardInfo.getId())
                                << " Label=" << ((string)boardInfo.getLabel())
                                << " Pos="<< ((int)boardInfo.getPositionOrAddress())
                                << " Type=" << ((string)boardInfo.getType()) );
                    }
                }
                LOG4CPLUS_DEBUG(logger, "+++++ AFTER SYSTEM.ADDLINKBOX");
            }
            //////////////////////////////////////////////////////////////// - MC

            lbInfos = lboxInfo.getBoardInfos();
            for (LinkBoxInfo::BoardInfoVector::iterator iLb = lbInfos.begin(); iLb != lbInfos.end(); ++iLb) {
                BoardInfo& boardInfo = iLb->bag;
                if (boardInfo.isDisabled()) {
                    LOG4CPLUS_WARN(logger, "Disabled board in lbox " << ((string)lboxInfo.getName())
                            << " board name = " << ((string)boardInfo.getName()));
                    continue;
                }
                int pos = boardInfo.getPositionOrAddress();
                if (boardInfo.getType() == "CONTROLBOARD") {
                } else if (boardInfo.getType() == "LINKBOARD") {

                    LOG4CPLUS_DEBUG(logger, ((string)boardInfo.getName()));

                    ChipInfo& chipInfo = extractExpectedChipInfo(boardInfo, "SYNCODER");
                    if (!chipInfo.isDisabled()) {
                        LinkBoard& lb = lbox->addLinkBoard(pos, boardInfo.getId(),
                                ((string) boardInfo.getName()).c_str(), (int) chipInfo.getId());
                        system.registerItem(lb);
                        system.registerItem(lb.getSynCoder());
                    }
                } else if (boardInfo.getType() == "RBCBOARD") {

                    LOG4CPLUS_DEBUG(logger, ((string)boardInfo.getName()));

                    ChipInfo& chipInfo = extractExpectedChipInfo(boardInfo, "RBC");
                    if (!chipInfo.isDisabled()) {
                        Rbc* rbc = new Rbc(boardInfo.getId(),
                                ((string) boardInfo.getName()).c_str(), lbox, pos,
                                &(lbox->getHalfBox10()->getCb()), (int) chipInfo.getId());

                        lbox->getBoards().push_back(rbc);
                        lbox->getHalfBox10()->addBoard(*rbc);
                        system.registerItem(*rbc);
                        //system.registerItem(rbc->getRbcDevice());
                    }
                } else if (boardInfo.getType() == "RBCFAKEBOARD") {
                	LOG4CPLUS_DEBUG(logger, ((string)boardInfo.getName()));
                }
                else {
                    ostringstream ostr;
                    ostr << "Not supported board type " << (string) boardInfo.getType();
                    LOG4CPLUS_ERROR(logger, ostr.str());
                    throw TException(ostr.str());
                }
            }
            /*
            //...temporary solution
            LOG4CPLUS_DEBUG(logger,"XdaqDbSystemBuilder: adding RBCs (no from DB setup)");
            std::string lboxName = lbox->getDescription();

            int rbcSector = 0;
            bool hasRbcIn = Rbc::parseLBoxName(lboxName, rbcSector);

            if (ccu10 && hasRbcIn && ( (rbcSector == 1) ||
						(rbcSector == 3) ||
						(rbcSector == 5) ||
						(rbcSector == 7) ||
						(rbcSector == 9) ||
						(rbcSector == 11) )) {

            	std::string rbcName = lboxName + std::string("_RBC");
            	LOG4CPLUS_DEBUG(logger,rbcName);

            	int rbcId = ((int)lbox->getId()) + 10000;
            	int rbcPos = 12;
            	int rbcChipId = ((int)lbox->getId()) + 30000;

            	Rbc* rbc = new Rbc( rbcId, rbcName.c_str(), lbox, rbcPos,
                    &(lbox->getHalfBox10()->getCb()), rbcChipId );

            	lbox->getBoards().push_back(rbc);

            	system.registerItem(*rbc);
            	//system.registerItem(rbc->getRbcDevice());

            }
*/
        }

        if (febsystem && ccuRingInfo)
            buildFebSystem(*febsystem, ccuRingInfo->bag.getFebBoardInfos());

        delete ccuRingInfo;
    } catch (std::exception& e) {
        delete ccuRingInfo;
        throw ;
    }

}

void XdaqDbSystemBuilder::buildTriggerCrate(System& system, std::string host, int port, std::string className, int instance) {

    cout << "Geting data from DBService" << endl;
    TriggerCrateInfoBag* crateInfoBag = dbServiceClient.getTriggerCrateInfo(host, port, className, instance);
    cout << "Data received from DBService" << endl;

    try {
        TriggerCrateInfo& info = crateInfoBag->bag;

        if (info.isDisabled()) {
            LOG4CPLUS_WARN(logger, "Disabled crate " << ((string)info.getName()));
        }
        else {

            TriggerCrate* tc = new TriggerCrate((int) info.getId(), ((string) info.getName()).c_str(), info.getLabel().value_
                                                , (int) info.getLogSector());

            if (mock) {
                tc->setVme(new TVMESimul());
            }
            else {
                string vmeInterface = "CAEN"; //getAttribute("interface", attributes);
                ostringstream vmeUnit;
                vmeUnit << ((int)info.getPciSlot()) << ' ' << ((int)info.getPosInVmeChain());
                TVMEInterface* vme = TVMEFactory().Create(vmeInterface, vmeUnit.str());
                system.addVmeInterface(*vme);
                tc->setVme(vme);
            }

            system.addVmeCrate(*tc);

            TriggerCrateInfo::BoardInfoVector& bInfos = info.getBoardInfos();
            for (TriggerCrateInfo::BoardInfoVector::iterator iBoard = bInfos.begin(); iBoard != bInfos.end(); ++iBoard) {
                BoardInfo& boardInfo = iBoard->bag;
                LOG4CPLUS_DEBUG(logger, ((string)boardInfo.getName()));
                if (boardInfo.isDisabled()) {
                    LOG4CPLUS_WARN(logger, "Disabled board in crate " << ((string)info.getName())
                            << " board name = " << ((string)boardInfo.getName()));
                    continue;
                }
                int pos = boardInfo.getPositionOrAddress();
                if (boardInfo.getType() == "TCBACKPLANE") {
                    ChipInfo& chipInfo = extractExpectedChipInfo(boardInfo, "TCSORT");
                    if (!chipInfo.isDisabled()) {
                        TTCSort* tcsort = new TTCSort(boardInfo.getId(), ((string)boardInfo.getName()).c_str(),
                                tc->getVme(), /*0xa*/0xf, tc, pos, (int)chipInfo.getId());
                        tc->addBoard(*tcsort);
                        system.registerItem(*tcsort);
                        system.registerItem(tcsort->GetTCSortTCSort());
                    }
                }
                else if (boardInfo.getType() == "TRIGGERBOARD") {
                    TriggerBoard* tb = new TriggerBoard(boardInfo.getId(), ((string)boardInfo.getName()).c_str(),
                            tc->getVme(), pos - 10, tc, pos);
                    tc->addBoard(*tb);
                    system.registerItem(*tb);
                    BoardInfo::ChipInfoVector& chipInfos = boardInfo.getChipInfos();
                    for (BoardInfo::ChipInfoVector::iterator iChipInfo = chipInfos.begin(); iChipInfo != chipInfos.end(); ++iChipInfo) {
                        ChipInfo& chipInfo = iChipInfo->bag;
                        if (!chipInfo.isDisabled()) {
                            IDevice& chip = tb->addChip(
                                    (string) chipInfo.getType(),
                                    (int) chipInfo.getPosition(),
                                    (int) chipInfo.getId());
                            system.registerItem(chip);
                        }
                        else {
                            LOG4CPLUS_WARN(logger, "Disabled chip id " << ((int)chipInfo.getId())
                                    << " board " << ((string)boardInfo.getName()));
                        }
                    }
                }
                else if (boardInfo.getType() == "TTUBOARD") {
                    TTUBoard* tb = new TTUBoard(boardInfo.getId(), ((string)boardInfo.getName()).c_str(),
                            tc->getVme(), pos - 10, tc, pos);
                    tc->addBoard(*tb);
                    system.registerItem(*tb);
                    BoardInfo::ChipInfoVector& chipInfos = boardInfo.getChipInfos();
                    for (BoardInfo::ChipInfoVector::iterator iChipInfo = chipInfos.begin(); iChipInfo != chipInfos.end(); ++iChipInfo) {
                        ChipInfo& chipInfo = iChipInfo->bag;
                        if (!chipInfo.isDisabled()) {
                            IDevice& chip = tb->addChip(
                                    (string) chipInfo.getType(),
                                    (int) chipInfo.getPosition(),
                                    (int) chipInfo.getId());
                            system.registerItem(chip);
                        }
                        else {
                            LOG4CPLUS_WARN(logger, "Disabled chip id " << ((int)chipInfo.getId())
                                    << " board " << ((string)boardInfo.getName()));
                        }
                    }
                }
                else {
                    ostringstream ostr;
                    ostr << "Not supported board type " << (string)boardInfo.getType();
                    LOG4CPLUS_ERROR(logger, ostr.str());
                    throw TException(ostr.str());
                }
            }
        }
        delete crateInfoBag;
    }
    catch (std::exception& e) {
        delete crateInfoBag;
        throw;
    }
}

void XdaqDbSystemBuilder::buildVmeCrate(System& system, std::string host, int port, std::string className, int instance) {

    LOG4CPLUS_DEBUG(logger, "Geting data from DBService");
    VmeCrateInfoBag* crateInfoBag = dbServiceClient.getVmeCrateInfo(host, port, className, instance);
    LOG4CPLUS_DEBUG(logger, "Data received from DBService");

    try {
        VmeCrateInfo& info = crateInfoBag->bag;
        VmeCrate* crate;
        LOG4CPLUS_DEBUG(logger, "Creating crate type = " << info.getType().toString() << ", name = " << info.getName().toString());
        if (info.getType() == "TRIGGERCRATE") {
            crate = new TriggerCrate((int) info.getId(), ((string) info.getName()).c_str(), info.getLabel().value_
                                     , (int) info.getLogSector());
        }
        else if (info.getType() == "SORTERCRATE") {
            crate = new SorterCrate((int) info.getId(), ((string) info.getName()).c_str(), info.getLabel().value_);
        }
        else {
            crate = new VmeCrate((int) info.getId(), ((string) info.getName()).c_str(), info.getLabel().value_);
        }

        if (mock) {
            crate->setVme(new TVMESimul());
        }
        else {
            string vmeInterface = "CAEN"; //getAttribute("interface", attributes);
            ostringstream vmeUnit;
            vmeUnit << ((int)info.getPciSlot()) << ' ' << ((int)info.getPosInVmeChain());
            TVMEInterface* vme = TVMEFactory().Create(vmeInterface, vmeUnit.str());
            system.addVmeInterface(*vme);
            crate->setVme(vme);
        }

        if (info.isDisabled()) {
            LOG4CPLUS_WARN(logger, "Disabled crate " << ((string)info.getName()));
        }
        else {
            system.addVmeCrate(*crate);
            VmeCrateInfo::BoardInfoVector& bInfos = info.getBoardInfos();
            for (VmeCrateInfo::BoardInfoVector::iterator iBoard = bInfos.begin(); iBoard != bInfos.end(); ++iBoard) {
                BoardInfo& boardInfo = iBoard->bag;
                LOG4CPLUS_DEBUG(logger, ((string)boardInfo.getName()));
                if (boardInfo.isDisabled()) {
                    LOG4CPLUS_WARN(logger, "Disabled board in crate " << ((string)info.getName())
                            << " board name = " << ((string)boardInfo.getName()));
                    continue;
                }
                int pos = boardInfo.getPositionOrAddress();

                /*if (boardInfo.getName() == "TC_4_BP") { // TODO temporary - TTU backplane temporarily in TC_4
                 ChipInfo& chipInfo = extractExpectedChipInfo(boardInfo, "TCSORT");
                 if (!chipInfo.isDisabled()) {
                 TTTUSort* ttusort = new TTTUSort(boardInfo.getId(), ((string)boardInfo.getName()).c_str(),
                 crate->getVme(), 0xf, crate, pos, (int)chipInfo.getId());
                 crate->addBoard(*ttusort);
                 system.registerItem(*ttusort);
                 system.registerItem(ttusort->GeTTTUSortFinal());
                 }
                 }
                 else*/if (boardInfo.getType() == "TCBACKPLANE") {
                    ChipInfo& chipInfo = extractExpectedChipInfo(boardInfo, "TCSORT");
                    if (!chipInfo.isDisabled()) {
                        TTCSort* tcsort = new TTCSort(boardInfo.getId(), ((string)boardInfo.getName()).c_str(),
                                crate->getVme(), /*0xa*/0xf, crate, pos, (int)chipInfo.getId());
                        crate->addBoard(*tcsort);
                        system.registerItem(*tcsort);
                        system.registerItem(tcsort->GetTCSortVme());
                        system.registerItem(tcsort->GetTCSortTCSort());
                    }
                }
                else if (boardInfo.getType() == "SORTERBOARD") {

                    ChipInfo& chipInfo = getChipInfo(boardInfo, 0);
                    if (chipInfo.isDisabled()) {
                        LOG4CPLUS_WARN(logger, "Disabled chip id " << ((int)chipInfo.getId())
                                << " board " << ((string)boardInfo.getName()));
                    }
                    else {
                        if (chipInfo.getType() == "HALFSORTER") {
                            THsb* hsb = new THsb(boardInfo.getId(), ((string)boardInfo.getName()).c_str(),
                                    crate->getVme(), pos == 1 ? 0xb : 0xa, crate, pos, (int)chipInfo.getId());
                            crate->addBoard(*hsb);
                            system.registerItem(*hsb);
                            system.registerItem(hsb->GetHsbVme());
                            system.registerItem(hsb->GetHsbSortHalf());
                        }
                        else if (chipInfo.getType() == "FINALSORTER") {
                            TFsb* fsb = new TFsb(boardInfo.getId(), ((string)boardInfo.getName()).c_str(),
                                    crate->getVme(), 0xc, crate, pos, (int)chipInfo.getId());
                            crate->addBoard(*fsb);
                            system.registerItem(*fsb);
                            system.registerItem(fsb->GetFsbVme());
                            system.registerItem(fsb->GetFsbSortFinal());
                        }
                        else if (chipInfo.getType() == "TTUFINAL") {
                            TTTUBackplane* ttuBackplane = new TTTUBackplane(boardInfo.getId(), ((string)boardInfo.getName()).c_str(),
                                    crate->getVme(), 0xf, crate, pos, (int)chipInfo.getId());
                            crate->addBoard(*ttuBackplane);
                            system.registerItem(*ttuBackplane);
                            system.registerItem(ttuBackplane->GeTTTUBackplaneVme());
                            system.registerItem(ttuBackplane->GeTTTUBackplaneFinal());
                        }
                        else {
                            throw TException("Invalid chip type for SORTERBOARD: " + (string)chipInfo.getType());
                        }
                    }
                }
                else if (boardInfo.getType() == "TRIGGERBOARD") {
                    TriggerBoard* tb = new TriggerBoard(boardInfo.getId(), ((string)boardInfo.getName()).c_str(),
                            crate->getVme(), pos - 10, crate, pos);
                    crate->addBoard(*tb);
                    system.registerItem(*tb);
                    system.registerItem(tb->GetVme());
                    system.registerItem(tb->GetControl());
                    BoardInfo::ChipInfoVector& chipInfos = boardInfo.getChipInfos();
                    for (BoardInfo::ChipInfoVector::iterator iChipInfo = chipInfos.begin(); iChipInfo != chipInfos.end(); ++iChipInfo) {
                        ChipInfo& chipInfo = iChipInfo->bag;
                        if (!chipInfo.isDisabled()) {
                            IDevice& chip = tb->addChip(
                                    (string) chipInfo.getType(),
                                    (int) chipInfo.getPosition(),
                                    (int) chipInfo.getId());
                            system.registerItem(chip);
                        }
                        else {
                            LOG4CPLUS_WARN(logger, "Disabled chip id " << ((int)chipInfo.getId())
                                    << " board " << ((string)boardInfo.getName()));
                        }
                    }
                }
                else if (boardInfo.getType() == "TTUBOARD") {
                    TTUBoard* tb = new TTUBoard(boardInfo.getId(), ((string)boardInfo.getName()).c_str(),
                            crate->getVme(), pos - 10, crate, pos);
                    crate->addBoard(*tb);
                    system.registerItem(*tb);
                    system.registerItem(tb->GetVme());
                    system.registerItem(tb->GetControl());
                    BoardInfo::ChipInfoVector& chipInfos = boardInfo.getChipInfos();
                    for (BoardInfo::ChipInfoVector::iterator iChipInfo = chipInfos.begin(); iChipInfo != chipInfos.end(); ++iChipInfo) {
                        ChipInfo& chipInfo = iChipInfo->bag;
                        if (!chipInfo.isDisabled()) {
                            IDevice& chip = tb->addChip(
                                    (string) chipInfo.getType(),
                                    (int) chipInfo.getPosition(),
                                    (int) chipInfo.getId());
                            system.registerItem(chip);
                        }
                        else {
                            LOG4CPLUS_WARN(logger, "Disabled chip id " << ((int)chipInfo.getId())
                                    << " board " << ((string)boardInfo.getName()));
                        }
                    }
                }
                else {
                    ostringstream ostr;
                    ostr << "Not supported board type " << (string)boardInfo.getType();
                    LOG4CPLUS_ERROR(logger, ostr.str());
                    throw TException(ostr.str());
                }
            }
        }
        delete crateInfoBag;
    }
    catch (std::exception& e) {
        delete crateInfoBag;
        throw;
    }
}

}}
