#include "rpct/xdaqutils/FebBoardInfo.h"

#include "xdata/AbstractBag.h"

namespace rpct {

FebBoardInfo::FebBoardInfo(int id
                           , const std::string & name
                           , const std::string & label
                           , const std::string & type
                           , int i2cAddress
                           , bool disabled
                           , BoardInfo::ChipInfoVector chipInfos
                           , int cb_id
                           , int cb_channel
                           , int feblocation_id
                           , bool dt_controlled)
    : BoardInfo() //(id, name, label, type, i2cAddress, disabled, chipInfos)
    , cb_id_(cb_id)
    , cb_channel_(cb_channel)
    , feblocation_id_(feblocation_id)
    , dt_controlled_(dt_controlled)
{
    this->id.value_ = id;
    this->name.value_ = name;
    this->label.value_ = label;
    this->positionOrAddress.value_ = i2cAddress;
    this->disabled.value_ = disabled;
    setChipInfos(chipInfos);
}

void FebBoardInfo::registerFields(xdata::AbstractBag * bag)
{
    BoardInfo::registerFields(bag);
    bag->addField("cbId", &cb_id_);
    bag->addField("cbChannel", &cb_channel_);
    bag->addField("febLocationId", &feblocation_id_);
    bag->addField("dtControlled", &dt_controlled_);
}

} // namespace rpct
