#include "rpct/xdaqutils/XdaqDbServiceClient.h"


namespace rpct {
namespace xdaqutils {

const char* XdaqDbServiceClient::RPCT_DB_SERVICE_NS = "urn:rpct-dbservice";
const char* XdaqDbServiceClient::RPCT_DB_SERVICE_PREFIX = "rdbs";
const char* XdaqDbServiceClient::RPCT_I_SERVICE_NS = "urn:rpct-iservice";
const char* XdaqDbServiceClient::RPCT_I_SERVICE_PREFIX = "ris";

//the same as in LocalConfigKey.java
const char* XdaqDbServiceClient::SUBSYSTEM_LBS = "LBS";
const char* XdaqDbServiceClient::SUBSYSTEM_TCS = "TCS";
const char* XdaqDbServiceClient::SUBSYSTEM_SC = "SC";
const char* XdaqDbServiceClient::SUBSYSTEM_RBCS_TTUS = "RBCS_TTUS";
const char* XdaqDbServiceClient::SUBSYSTEM_FEBS = "FEBS";
const char* XdaqDbServiceClient::SUBSYSTEM_ALL = "ALL";

XdaqDbServiceClient::XdaqDbServiceClient(xdaq::Application* app) :
	 logger(log4cplus::Logger::getInstance(app->getApplicationLogger().getName() + ".XdaqDbServiceClient") ), application(app), soapAccess(app){
	//logger = log4cplus::Logger::getInstance(application->getApplicationLogger().getName() + ".XdaqDbServiceClient");
	logger.setLogLevel(log4cplus::INFO_LOG_LEVEL);
}

XdaqDbServiceClient::FebConnectorStripsVector *
XdaqDbServiceClient::getFebConnectorStrips(std::string const & _host, int _port)
{
    XdaqSoapAccess::TSOAPParamList paramList;
    xdata::String _xhost = _host;
    xdata::Integer _xport = _port;
    paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type("xdaqExecutiveHost", &_xhost));
    paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type("xdaqExecutivePort", &_xport));

    LOG4CPLUS_DEBUG(logger,
                    "sending request to DB Service: getFebConnectorStrips, host = "
                    << _xhost.value_ << " port = "<< _xport.value_ );

    xoap::MessageReference getFebConnectorStripsResponse
        = soapAccess.sendSOAPRequest("getFebConnectorStrips", RPCT_DB_SERVICE_PREFIX,
                                     RPCT_DB_SERVICE_NS, paramList, "RpctDbService", 0, true);

    LOG4CPLUS_DEBUG(logger,
                    "received response from DB Service");

    FebConnectorStripsVector
        * _febconnectorstrips_vector = new FebConnectorStripsVector();
    soapAccess.parseSOAPResponse(getFebConnectorStripsResponse
                                 , "getFebConnectorStrips", RPCT_DB_SERVICE_PREFIX
                                 , RPCT_DB_SERVICE_NS, *_febconnectorstrips_vector);
    return _febconnectorstrips_vector;
}

XdaqDbServiceClient::DTLineFebChipFlatInfoVector *
XdaqDbServiceClient::getDTLineFebChipFlatInfo(int _wheel)
{
    XdaqSoapAccess::TSOAPParamList _param_list;
    xdata::Integer _xwheel = _wheel;
    _param_list.push_back(XdaqSoapAccess::TSOAPParamList::value_type("wheel", &_xwheel));

    LOG4CPLUS_DEBUG(logger,
                    "sending request to DB Service: getDTLineFebChipFlatInfo, wheel = " << _xwheel.value_);

    xoap::MessageReference _response
        = soapAccess.sendSOAPRequest("getDTLineFebChipFlatInfo", RPCT_DB_SERVICE_PREFIX,
                                     RPCT_DB_SERVICE_NS, _param_list, "RpctDbService", 0, true);

    LOG4CPLUS_DEBUG(logger,
                    "received response from DB Service");

    DTLineFebChipFlatInfoVector
        * _dtlinefebinfo_vector = new DTLineFebChipFlatInfoVector();
    soapAccess.parseSOAPResponse(_response
                                 , "getDTLineFebChipFlatInfo", RPCT_DB_SERVICE_PREFIX
                                 , RPCT_DB_SERVICE_NS, *_dtlinefebinfo_vector);
    return _dtlinefebinfo_vector;
}

} // namespace xdaqutils
} // namespace rpct
