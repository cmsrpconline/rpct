/*
 * LogUtils.cpp
 *
 *  Created on: 2010-04-07
 *      Author: Krzysiek
 */

#include "rpct/xdaqutils/LogUtils.h"
#include "log4cplus/fileappender.h"
//----------------------------------------------
#include "rpct/ii/BoardBase.h"
#include "rpct/devices/i2c/BI2cClient.h"
#include "rpct/lboxaccess/StdLinkBoxAccess.h"
#include "rpct/devices/i2c/CommandI2c.h"
#include "rpct/devices/FebBoard.h"
#include "rpct/devices/FebChip.h"
#include "rpct/devices/FebDistributionBoard.h"
#include "rpct/devices/FebPart.h"
#include "rpct/devices/FebSystem.h"
#include "rpct/devices/IFebAccessPoint.h"
#include "rpct/devices/RPCFebSystem.h"
#include "rpct/devices/LinkSystem.h"
#include "rpct/devices/System.h"
#include "rpct/devices/cb.h"
#include "rpct/devices/Ccs.h"
#include "rpct/devices/CcsSettings.h"
#include "rpct/devices/Crate.h"
#include "rpct/devices/Dcc.h"
#include "rpct/devices/DccDevice.h"
#include "rpct/devices/Feb.h"
#include "rpct/devices/FebInstanceManager.h"
#include "rpct/devices/fsb.h"
#include "rpct/devices/hsb.h"
#include "rpct/devices/LinkBoard.h"
#include "rpct/devices/LinkBox.h"
#include "rpct/devices/LinkBoxConfigurator.h"
//#include "rpct/devices/cb.h"
#include "rpct/devices/Rbc.h"
#include "rpct/devices/SorterCrate.h"
#include "rpct/devices/StandardBxData.h"
#include "rpct/devices/tcsort.h"
#include "rpct/devices/TIICCUAccess.h"
#include "rpct/devices/TIIDummyAccess.h"
#include "rpct/devices/TriggerBoard.h"
#include "rpct/devices/TriggerCrate.h"
#include "rpct/devices/TTTCrxI2C.h"
#include "rpct/devices/TTUBackplane.h"
#include "rpct/devices/TTUBoard.h"
#include "rpct/devices/VmeCrate.h"
#include "rpct/devices/XmlSystemBuilder.h"
#include "rpct/devices/JTAGControllerImpl.h"
#include "rpct/ii/IBoard.h"
#include "rpct/hardwareTests/StatisticDiagManagerBase.h"
#include "rpct/lboxaccess/FecManagerImpl.h"
#include "rpct/lboxaccess/test/FecManagerMock.h"
#include "rpct/hardwareTests/LBStatisticDiagManager.h"
#include "rpct/xdaqutils/XdaqDbSystemBuilder.h"
#include "rpct/devices/FebSystem.h"
#include "rpct/diag/IDiagnosticReadout.h"
#include "rpct/xdaqutils/xdaqFebSystem.h"
#include "rpct/hardwareTests/TTUStatisticDiagManager.h"
#include "rpct/ii/IDevice.h"
#include "rpct/devices/TTUBackplane.h"
#include "rpct/xdaqutils/XdaqDbSystemBuilder.h"
#include "rpct/xdaqutils/XdaqIIAccess.h"
#include "rpct/hardwareTests/XmlBxDataReader.h"
#include "rpct/hardwareTests/XmlDevicesSettings.h"
//#include "rpct/ts/worker/xdaqlboxaccess/LinkBoxTester.h"
//----------------------------------------------



using namespace std;
using namespace rpct;

void rpct::setupLoggers(std::string namePrefix) {
	TriggerBoardDevice::setupLogger(namePrefix);
	TCB::setupLogger(namePrefix);
	devices::i2c::BI2cClient::setupLogger(namePrefix);
	devices::i2c::BlockI2c::setupLogger(namePrefix);
	BoardBase::setupLogger(namePrefix);
	Ccs::setupLogger(namePrefix);
	CcsTriggerFPGA::setupLogger(namePrefix);
	devices::i2c::CommandI2c::setupLogger(namePrefix);
	Crate::setupLogger(namePrefix);
	Dcc::setupLogger(namePrefix);
	DccDevice::setupLogger(namePrefix);
	Feb::setupLogger(namePrefix);
	FebBoard::setupLogger(namePrefix);
	FebChip::setupLogger(namePrefix);
	FebDistributionBoard::setupLogger(namePrefix);
	FebPart::setupLogger(namePrefix);
	FebSystem::setupLogger(namePrefix);
	xdaqutils::xdaqFebSystem::setupLogger(namePrefix);	//-----------------<=
	FebInstanceManager::setupLogger(namePrefix);
	FecManagerImpl::setupLogger(namePrefix);
	FecManagerMock::setupLogger(namePrefix);
	HalfBox::setupLogger(namePrefix);
	IFebAccessPoint::setupLogger(namePrefix);
	JTAGControllerImpl::setupLogger(namePrefix);
	LBStatisticDiagManager::setupLogger(namePrefix);
	LinkBoard::setupLogger(namePrefix);
	LinkBox::setupLogger(namePrefix);
	LinkBoxConfigurator::setupLogger(namePrefix);
	LinkSystem::setupLogger(namePrefix);
	//Publisher::setupLogger(namePrefix);	//<====-----------------------
	RPCBxData::setupLogger(namePrefix);
	RPCDataStream::setupLogger(namePrefix);
	RPCFebSystem::setupLogger(namePrefix);
	Rbc::setupLogger(namePrefix);
	SorterCrate::setupLogger(namePrefix);
	StandardBxData::setupLogger(namePrefix);
	StatisticDiagManagerBase::setupLogger(namePrefix);
	SynCoder::setupLogger(namePrefix);
	System::setupLogger(namePrefix);
	TDiagCtrl::setupLogger(namePrefix);
	TDiagnosticReadout::setupLogger(namePrefix);
	TFsb::setupLogger(namePrefix);
	TFsbDevice::setupLogger(namePrefix);
	THsb::setupLogger(namePrefix);
	THsbDevice::setupLogger(namePrefix);
	TI2CMasterCore::setupLogger(namePrefix);
	TI2CMasterCorev09::setupLogger(namePrefix);
	TIICCUAccess::setupLogger(namePrefix);
	TIIDevice::setupLogger(namePrefix);
	TIIDummyAccess::setupLogger(namePrefix);
	TIIJtagIO::setupLogger(namePrefix);
	TIILBCCUAccess::setupLogger(namePrefix);
	TTCSortDevice::setupLogger(namePrefix);
	TTTCrxI2C::setupLogger(namePrefix);
	//TTTUBackplane::setupLogger(namePrefix);has no its own logger,only the one from the BoardBase
	TTUBoard::setupLogger(namePrefix);
	TTUBoardDevice::setupLogger(namePrefix);
	TTUStatisticDiagManager::setupLogger(namePrefix);	//<--------------------
	TriggerBoard::setupLogger(namePrefix);
	TriggerCrate::setupLogger(namePrefix);
	VmeCrate::setupLogger(namePrefix);
	xdaqutils::XdaqDbSystemBuilder::setupLogger(namePrefix);
	xdaqutils::XdaqIIAccess::setupLogger(namePrefix);
	XmlBxDataReader::setupLogger(namePrefix);
	XmlDevicesSettings::setupLogger(namePrefix);
	XmlSystemBuilder::setupLogger(namePrefix);
	//LinkBoxTester::setupLogger(namePrefix);
}



//appender singleton

FileAppender* daily_appender_singleton = 0;

void rpct::rpcLogSetup(log4cplus::Logger& logger, xdaq::Application* app){
/*
	stringstream pattern;
	pattern<<"%d{%d %b %Y %H:%M:%S} [%t] %p "<<
			app->getApplicationDescriptor()->getContextDescriptor()->getURL().substr(7)<<"."<<
			app->getApplicationDescriptor()->getClassName()<<".instance("<<
			app->getApplicationDescriptor()->getInstance() << ") %c <> - %m%n";
	logger.getAllAppenders().at(0)->setLayout(auto_ptr<Layout>(new PatternLayout(pattern.str().c_str())));
*/

    if (!daily_appender_singleton){
        stringstream stream;
        stream << "/var/log/";
        stream << app->getApplicationDescriptor()->getClassName();
        stream << "-";
        stream << app->getApplicationDescriptor()->getInstance();
        stream << ".log";
        string str = stream.str();
        for (u_int i = 0;i < str.size();i++){
            if (str[i] == ':'){
                str.replace(i,1,"_");
            };
        };
        daily_appender_singleton = new DailyRollingFileAppender(str,DAILY, 1);
        daily_appender_singleton->setLayout(auto_ptr<Layout>(new PatternLayout("%d{%d %b %Y %H:%M:%S:%Q} [%t] %p %c <> - %m%n")));
    };
    logger.addAppender(*(new SharedAppenderPtr(daily_appender_singleton)));

/*    stringstream pattern;
    pattern<<"%d{%d %b %Y %H:%M:%S:%Q} [%t] %p "<<
        app->getApplicationDescriptor()->getContextDescriptor()->getURL().substr(7)<<"."<<
        app->getApplicationDescriptor()->getClassName()<<".instance("<<
        app->getApplicationDescriptor()->getInstance() << ") %c <> - %m%n";

    SharedAppenderPtrList appenders = Logger::getRoot().getAllAppenders();

    for (SharedAppenderPtrList::iterator it = appenders.begin(); it!=appenders.end(); ++it) {
        (*it)->setLayout(auto_ptr<Layout>(new PatternLayout(pattern.str().c_str())));
    };*/
};
