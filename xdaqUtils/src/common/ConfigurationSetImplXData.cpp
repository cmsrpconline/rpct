/*
 *  Author: Michal Pietrusinski
 *  Version: $Id: ConfigurationSetImplXData.cpp,v 1.4 2009/09/24 11:13:27 tb Exp $
 *
 */

#include "rpct/xdaqutils/ConfigurationSetImplXData.h"
#include "tb_std.h"



namespace rpct {
namespace xdaqutils {


ConfigurationSetImplXData::ChipConfigurationsByConfId& ConfigurationSetImplXData::getChipConfigurationsByConfId() {
    if (chipConfigurationsByConfId_.empty()) {

        for (SynCoderConfigurationVector::iterator iSynCoderConf = synCoderConfigurations_.begin();
                iSynCoderConf != synCoderConfigurations_.end(); ++iSynCoderConf) {
            SynCoderConfiguration& c = iSynCoderConf->bag;
            chipConfigurationsByConfId_.insert(ChipConfigurationsByConfId::value_type(c.getConfigurationId(), &c));
        }

        for (OptoConfigurationVector::iterator iOptoConf = optoConfigurations_.begin();
                iOptoConf != optoConfigurations_.end(); ++iOptoConf) {
            OptoConfiguration& c = iOptoConf->bag;
            chipConfigurationsByConfId_.insert(ChipConfigurationsByConfId::value_type(c.getConfigurationId(), &c));
        }

        for (PacConfigurationVector::iterator iPacConf = pacConfigurations_.begin();
                iPacConf != pacConfigurations_.end(); ++iPacConf) {
            PacConfiguration& c = iPacConf->bag;
            chipConfigurationsByConfId_.insert(ChipConfigurationsByConfId::value_type(c.getConfigurationId(), &c));
        }

        for (TbGbSortConfigurationVector::iterator iTbGbSortConf = tbGbSortConfigurations_.begin();
                iTbGbSortConf != tbGbSortConfigurations_.end(); ++iTbGbSortConf) {
        	TbGbSortConfiguration& c = iTbGbSortConf->bag;
            chipConfigurationsByConfId_.insert(ChipConfigurationsByConfId::value_type(c.getConfigurationId(), &c));
        }

        for (RmbConfigurationVector::iterator iRmbConf = rmbConfigurations_.begin();
                iRmbConf != rmbConfigurations_.end(); ++iRmbConf) {
            RmbConfiguration& c = iRmbConf->bag;
            chipConfigurationsByConfId_.insert(ChipConfigurationsByConfId::value_type(c.getConfigurationId(), &c));
        }

        for (TcSortConfigurationVector::iterator iTcSortConf = tcSortConfigurations_.begin();
                iTcSortConf != tcSortConfigurations_.end(); ++iTcSortConf) {
            TcSortConfiguration& c = iTcSortConf->bag;
            chipConfigurationsByConfId_.insert(ChipConfigurationsByConfId::value_type(c.getConfigurationId(), &c));
        }

        for (HalfSortConfigurationVector::iterator iHalfSortConf = halfSortConfigurations_.begin();
                iHalfSortConf != halfSortConfigurations_.end(); ++iHalfSortConf) {
        	HalfSortConfiguration& c = iHalfSortConf->bag;
            chipConfigurationsByConfId_.insert(ChipConfigurationsByConfId::value_type(c.getConfigurationId(), &c));
        }

        //For RBC
        for (RbcDeviceConfigurationVector::iterator iRbcConf = rbcDeviceConfigurations_.begin();
        		iRbcConf != rbcDeviceConfigurations_.end(); ++iRbcConf) {
                	RbcDeviceConfiguration& c = iRbcConf->bag;
                    chipConfigurationsByConfId_.insert(ChipConfigurationsByConfId::value_type(c.getConfigurationId(), &c));
                    std::cout << "RBCdevice Loop " << std::endl;
        }
        //For TTU Trigger
        for (TtuTriggerConfigurationVector::iterator iTtuTrigConf = ttuTriggerConfigurations_.begin();
        		iTtuTrigConf != ttuTriggerConfigurations_.end(); ++iTtuTrigConf) {
        	TtuTriggerConfiguration& c = iTtuTrigConf->bag;
                    chipConfigurationsByConfId_.insert(ChipConfigurationsByConfId::value_type(c.getConfigurationId(), &c));
                    std::cout << "TTU trigger Loop " << std::endl;
        }
        //For TTU Backplane
        for (TtuBackplaneConfigurationVector::iterator iTtuBackConf = ttuBackplaneConfigurations_.begin();
        		iTtuBackConf != ttuBackplaneConfigurations_.end(); ++iTtuBackConf) {
        	TtuBackplaneConfiguration& c = iTtuBackConf->bag;
                            chipConfigurationsByConfId_.insert(ChipConfigurationsByConfId::value_type(c.getConfigurationId(), &c));
                            std::cout << "Ttu Back device Loop " << std::endl;
        }
        //For FEB Chip
        for (FebChipConfigurationVector::iterator iFebChipConf = febChipConfigurations_.begin();
             iFebChipConf != febChipConfigurations_.end(); ++iFebChipConf) {
            FebChipConfiguration& c = iFebChipConf->bag;
            chipConfigurationsByConfId_.insert(ChipConfigurationsByConfId::value_type(c.getConfigurationId(), &c));
        }

    }
    return chipConfigurationsByConfId_;
}


ConfigurationSetImplXData::ChipConfigurationsByChipId& ConfigurationSetImplXData::getChipConfigurationsByChipId() {
    if (chipConfigurationsByChipId_.empty()) {
        for (ChipConfigurationAssignmentVector::iterator iAssign = chipConfigurationAssignments_.begin();
                iAssign != chipConfigurationAssignments_.end(); ++iAssign) {
            ChipConfigurationAssignment& a = iAssign->bag;
            ChipConfigurationsByConfId::iterator iConf = getChipConfigurationsByConfId().find(a.getConfigurationId());
            if (iConf == chipConfigurationsByConfId_.end()) {
                throw TException("ConfigurationSet integrity error: configuration with id"
                                 + rpct::toString(a.getConfigurationId() + " not found "));
            }
            chipConfigurationsByChipId_.insert(ChipConfigurationsByChipId::value_type(
                                                   a.getChipId(), iConf->second));
        }
    }
    return chipConfigurationsByChipId_;
}

ChipConfiguration* ConfigurationSetImplXData::getChipConfigurationForChip(int id) {
    ChipConfigurationsByChipId::iterator iConf = getChipConfigurationsByChipId().find(id);
    if (iConf == getChipConfigurationsByChipId().end()) {
        return 0;
    }
    return iConf->second;
}


DeviceSettings* ConfigurationSetImplXData::getDeviceSettings(IDevice& device) {
    return getChipConfigurationForChip(device.getId());
}

DeviceSettings* ConfigurationSetImplXData::getDeviceSettings(int id) {
    return getChipConfigurationForChip(id);
}

ISynCoderConfInFlash* ConfigurationSetImplXData::getSynCoderConfInFlash(int synCoderId) {
	if(synCoderConfInFlashByChipId_.empty()) {
		for(SynCoderConfInFlashVector::iterator it = synCoderConfInFlashes_.begin(); it != synCoderConfInFlashes_.end(); it++) {
			SynCoderConfInFlash& synCoderConfInFlash = it->bag;
			synCoderConfInFlashByChipId_[synCoderConfInFlash.getChipId()] = &synCoderConfInFlash;
		}
	}

	ISynCoderConfInFlash* synCoderConfInFlash = 0;
	SynCoderConfInFlashByChipId::iterator it = synCoderConfInFlashByChipId_.find(synCoderId);
	if(it != synCoderConfInFlashByChipId_.end()) {
		synCoderConfInFlash = it->second;
	}
	return synCoderConfInFlash;
}

}}
