#include "rpct/xdaqutils/xdaqRPCFebSystem.h"

namespace rpct {
namespace xdaqutils {

xdaqRPCFebSystem::xdaqRPCFebSystem(LinkSystem & linksystem
                                   , xdaq::Application & application
                                   , toolbox::BSem & hwmutex
                                   , const std::string & service_name
                                   , unsigned int service_instance
                                   , size_t max_size)
    : rpct::RPCFebSystem(linksystem)
    , xdaqFebSystem(application, hwmutex, service_name, service_instance, max_size)
{}

} // namespace xdaqutils
} // namespace rpct
