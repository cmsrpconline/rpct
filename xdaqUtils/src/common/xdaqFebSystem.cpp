#include "rpct/xdaqutils/xdaqFebSystem.h"

#include "xdaq/Application.h"

#include "rpct/tools/Mutex.h"
#include "rpct/tools/Chrono.h"

#include "toolbox/BSem.h"
#include "toolbox/task/WaitingWorkLoop.h"

#include "xdata/String.h"

#include "xgi/Utils.h"

#include "rpct/ii/Configurable.h"
#include "rpct/ii/Monitorable.h"
#include "rpct/tools/html.h"
#include "rpct/xdaqtools/xgitools.h"
#include "rpct/xdaqtools/remotelogger/Client.h"

#include "rpct/xdaqutils/XdaqDbServiceClient.h"

#include "rpct/devices/IFebAccessPoint.h"

namespace rpct {
namespace xdaqutils {

log4cplus::Logger xdaqFebSystem::logger_ = log4cplus::Logger::getInstance("Feb.xdaqFebSystem");
void xdaqFebSystem::setupLogger(std::string namePrefix) {
	logger_ = log4cplus::Logger::getInstance(namePrefix + "." + logger_.getName());
}
log4cplus::Logger xdaqFebSystem::remote_logger_ = log4cplus::Logger::getInstance("Remote.Feb.xdaqFebSystem");

xdaqFebSystem::xdaqFebSystem(xdaq::Application & application
                             , toolbox::BSem & hwmutex
                             , const std::string & publisher_service_name
                             , unsigned int publisher_service_instance
                             , size_t max_size)
    : rpct::xdaqtools::publisher::Client(application, logger_, publisher_service_name, publisher_service_instance, max_size)
    , actionwl_(new toolbox::task::WaitingWorkLoop("FebSystemWorkLoop"))
    , dbclient_(new XdaqDbServiceClient(&application))
    , hwmutex_(hwmutex)
    , mutex_(1)
    , interrupt_sync_(1)
    , paused_(0)
    , stopped_(0)
    , running_(0)
    , pause_(1)
    , rlclient_(new rpct::xdaqtools::remotelogger::Client(application_, remote_logger_, "rpcttscell::Cell", 2))
{
    rlclient_->addReference();
    remote_logger_.addAppender(rlclient_);

    configure_action_
        = toolbox::task::bind(this
                              , &xdaqFebSystem::configure_wl
                              , "configure_action");
    loadconfiguration_action_
        = toolbox::task::bind(this
                              , &xdaqFebSystem::loadconfiguration_wl
                              , "loadconfiguration_action");
    monitor_action_
        = toolbox::task::bind(this
                              , &xdaqFebSystem::monitor_wl
                              , "monitor_action");
    rpct::xdaqtools::xgitools::bind(application_, *this, &xdaqFebSystem::xgi_interface, "FebSystem");

    actionwl_->activate();
}

xdaqFebSystem::~xdaqFebSystem()
{
    remote_logger_.removeAppender(rlclient_);

    rpct::xdaqtools::xgitools::unbind(application_, "FebSystem");

    // waitingworkloop problem: no ~WaitingWorkloop
    try {
        if (actionwl_->isActive())
            actionwl_->cancel();
    } catch (...) {}
    delete actionwl_;

    delete configure_action_;
    delete loadconfiguration_action_;
    delete monitor_action_;

    delete dbclient_;
    rlclient_->removeReference();
}

bool xdaqFebSystem::xconfigure(const std::string & configkey)
{
    LOG4CPLUS_INFO(logger_, "xdaqFebSystem::configure");

    rpct::tools::Lock pauselock(pause_);
    if (!running_)
        {
            configkey_ = configkey;
            statussummary_ = "configuring...";
            running_ = true;
            actionwl_->submit(configure_action_);
            return true;
        }
    else
        {
            LOG4CPLUS_ERROR(logger_, "FEBs NOT configured (key " << configkey_ << "), another FEB action is running.");
            LOG4CPLUS_ERROR(remote_logger_, getDescription() << ": FEBs NOT configured (key " << configkey_ << "), another FEB action is running.");
            return false;
        }
}

bool xdaqFebSystem::xconfigure_attach(const std::string & configkey)
{
    LOG4CPLUS_INFO(logger_, "xdaqFebSystem::configure");

    rpct::tools::Lock pauselock(pause_);
    if (!running_)
        {
            configkey_ = configkey;
            statussummary_ = "configuring...";
            running_ = true;
            pauselock.give();
            configure_wl(0);
            return true;
        }
    else
        {
            LOG4CPLUS_ERROR(logger_, "FEBs NOT configured (key " << configkey_ << "), another FEB action is running.");
            LOG4CPLUS_ERROR(remote_logger_, getDescription() << ": FEBs NOT configured (key " << configkey_ << "), another FEB action is running.");
            return false;
        }
}

bool xdaqFebSystem::xloadConfiguration(const std::string & configkey)
{
    LOG4CPLUS_INFO(logger_, "xdaqFebSystem::loadConfiguration - started");

    rpct::tools::Lock pauselock(pause_);
    if (!running_)
        {
            configkey_ = configkey;
            statussummary_ = "loading configuration...";
            running_ = true;
            actionwl_->submit(loadconfiguration_action_);
            return true;
        }
    else
        {
            LOG4CPLUS_ERROR(logger_, "FEB configuration not loaded for key " << configkey_ << ", another FEB action is running.");
            LOG4CPLUS_ERROR(remote_logger_, getDescription() << ": FEB configuration not loaded for key " << configkey_ << ", another FEB action is running.");
            return false;
        }

    LOG4CPLUS_INFO(logger_, "xdaqFebSystem::loadConfiguration - done");
}

bool xdaqFebSystem::xloadConfiguration_attach(const std::string & configkey)
{
    LOG4CPLUS_INFO(logger_, "xdaqFebSystem::loadConfiguration - started");

    rpct::tools::Lock pauselock(pause_);
    if (!running_)
        {
            configkey_ = configkey;
            statussummary_ = "loading configuration...";
            running_ = true;
            pauselock.give();
            loadconfiguration_wl(0);
            return true;
        }
    else
        {
            LOG4CPLUS_ERROR(logger_, "FEB configuration not loaded for key " << configkey_ << ", another FEB action is running.");
            LOG4CPLUS_ERROR(remote_logger_, getDescription() << ": FEB configuration not loaded for key " << configkey_ << ", another FEB action is running.");
            return false;
        }

    LOG4CPLUS_INFO(logger_, "xdaqFebSystem::loadConfiguration - done");
}

bool xdaqFebSystem::xmonitor()
{
    LOG4CPLUS_INFO(logger_, "xdaqFebSystem::monitor");

    rpct::tools::Lock pauselock(pause_);
    if (!running_)
        {
            running_ = true;
            statussummary_ = "monitoring...";
            actionwl_->submit(monitor_action_);
            return true;
        }
    else
        {
            LOG4CPLUS_ERROR(logger_, "FEBs not monitored, another FEB action is running.");
            LOG4CPLUS_ERROR(remote_logger_, getDescription() << ": FEBs not monitored, another FEB action is running.");
            return false;
        }
}

bool xdaqFebSystem::xmonitor_attach()
{
    LOG4CPLUS_INFO(logger_, "xdaqFebSystem::monitor");

    rpct::tools::Lock pauselock(pause_);
    if (!running_)
        {
            running_ = true;
            statussummary_ = "monitoring...";
            pauselock.give();
            monitor_wl(0);
            return true;
        }
    else
        {
            LOG4CPLUS_ERROR(logger_, "FEBs not monitored, another FEB action is running.");
            LOG4CPLUS_ERROR(remote_logger_, getDescription() << ": FEBs not monitored, another FEB action is running.");
            return false;
        }
}

bool xdaqFebSystem::xmonitorcycle()
{
    {
        rpct::tools::Lock pauselock(pause_);
        if (running_ || paused_ || stopped_)
            return false;
    }
    try {
        rpct::tools::Lock swlock(mutex_, 0, 500000); // lock the febsystem or give up
        rpct::tools::TLock<toolbox::BSem> hwlock(hwmutex_);
        FebSystem::monitorcycle();
        return true;
    } catch (int e) {
        LOG4CPLUS_INFO(logger_, "FEB monitor cycle skipped, couldn't lock the FebSystem.");
    }
    return false;
}

bool xdaqFebSystem::configure_wl(toolbox::task::WorkLoop *)
{
    try {
        { // swlock
            rpct::tools::Lock swlock(mutex_);

            rpct::ConfigurationSet * cfset(0);
            rpct::xdaqutils::ConfigurationSetBagPtr cfsetbag;
            if (configkey_ != "")
                {
                    std::vector<int> chipIds = getChipIds();
                    cfsetbag = dbclient_->getConfigurationSetForLocalConfigKey(chipIds, configkey_);
                    cfset = &(cfsetbag->bag);
                }

            rpct::tools::Chrono _chrono;
            _chrono.pause();

            { // hwlock
                rpct::tools::TLock<toolbox::BSem> hwlock(hwmutex_);

                faps_type::iterator IfapsEnd = faps_.end();
                for (faps_type::iterator Ifaps = faps_.begin()
                         ; Ifaps != IfapsEnd
                         ; )
                    {
                        {
                            LOG4CPLUS_TRACE(logger_, "starting next chunk");
                            rpct::tools::Lock pauselock(pause_);
                            if (paused_)
                                {
                                    hwlock.give();
                                    pause_.wait(false);
                                    pauselock.give(); // to keep hwlock always out of plock
                                    hwlock.take();
                                }
                            else if (stopped_)
                                throw rpct::exception::Exception("FEB configuration was stopped.");
                        }
                        {
                            rpct::tools::Lock islock(interrupt_sync_);
                            _chrono.resume();
                            if (cfset)
                                Ifaps->second->configure(cfset, 0);
                            else
                                Ifaps->second->configure();
                            _chrono.pause();

                            if (!getInterrupt())
                                ++Ifaps;
                        }
                    }
            } // hwlock

            configuration_time_ = _chrono.duration();
            configured();
            log("configured");
        } // swlock
        { // plock
            rpct::tools::Lock pauselock(pause_);
            statussummary_ = FebSystem::getStatusSummary();
        } // plock

        LOG4CPLUS_INFO(logger_, "FEBs configured with key " << configkey_ << "; " << statussummary_);
        LOG4CPLUS_INFO(remote_logger_, getDescription() << ": FEBs configured with key " << configkey_ << "; " << statussummary_);

    } catch (xcept::Exception & e) {
        rpct::tools::Lock pauselock(pause_);
        statussummary_ = std::string("FEBs NOT configured (key ") + configkey_ + std::string("), caught exception: ") + e.what();
        LOG4CPLUS_ERROR(logger_, statussummary_);
        LOG4CPLUS_ERROR(remote_logger_, getDescription() << ": " << statussummary_);
    } catch (rpct::exception::CCUException & e) {
        rpct::tools::Lock pauselock(pause_);
        statussummary_ = std::string("FEBs NOT configured (key ") + configkey_ + std::string("), caught exception: ") + e.what();
        LOG4CPLUS_ERROR(logger_, statussummary_);
        LOG4CPLUS_ERROR(remote_logger_, getDescription() << ": " << statussummary_);
    } catch (rpct::exception::Exception & e) {
        rpct::tools::Lock pauselock(pause_);
        statussummary_ = std::string("FEBs NOT configured (key ") + configkey_ + std::string("), caught exception: ") + e.what();
        LOG4CPLUS_ERROR(logger_, statussummary_);
        LOG4CPLUS_ERROR(remote_logger_, getDescription() << ": " << statussummary_);
    } catch (std::exception & e) {
        rpct::tools::Lock pauselock(pause_);
        statussummary_ = std::string("FEBs NOT configured (key ") + configkey_ + std::string("), caught exception: ") + e.what();
        LOG4CPLUS_ERROR(logger_, statussummary_);
        LOG4CPLUS_ERROR(remote_logger_, getDescription() << ": " << statussummary_);
    } catch (...) {
        rpct::tools::Lock pauselock(pause_);
        statussummary_ = std::string("FEBs NOT configured (key ") + configkey_ + std::string("), caught unknown exception");
        LOG4CPLUS_ERROR(logger_, statussummary_);
        LOG4CPLUS_ERROR(remote_logger_, getDescription() << ": " << statussummary_);
    }

    {
        rpct::tools::Lock pauselock(pause_);
        running_ = false;
    }
    return false;
}

bool xdaqFebSystem::loadconfiguration_wl(toolbox::task::WorkLoop *)
{
    try {
        { // swlock
            rpct::tools::Lock swlock(mutex_);
            if (configkey_ != "")
                {
                    std::vector<int> chipIds = getChipIds();
                    rpct::xdaqutils::ConfigurationSetBagPtr cfsetbag =
                        dbclient_->getConfigurationSetForLocalConfigKey(chipIds, configkey_);
                    loadConfiguration(&(cfsetbag->bag), 0);
                }
        } // swlock
        { // plock
            rpct::tools::Lock pauselock(pause_);
            statussummary_ = FebSystem::getStatusSummary();
        } // plock

        LOG4CPLUS_INFO(logger_, "FEBs configuration loaded for key " << configkey_ << "; " << statussummary_);
        LOG4CPLUS_INFO(remote_logger_, getDescription() << ": FEBs configuration loaded for key " << configkey_ << "; " << statussummary_);
    } catch (xcept::Exception & e) {
        rpct::tools::Lock pauselock(pause_);
        statussummary_ = std::string("FEB configuration not loaded for key ") + configkey_ + std::string(", caught exception: ") + e.what();
        LOG4CPLUS_ERROR(logger_, statussummary_);
        LOG4CPLUS_ERROR(remote_logger_, getDescription() << ": " << statussummary_);
    } catch (std::exception& e) {
        rpct::tools::Lock pauselock(pause_);
        statussummary_ = std::string("FEB configuration not loaded for key ") + configkey_ + std::string(", caught exception: ") + e.what();
        LOG4CPLUS_ERROR(logger_, statussummary_);
        LOG4CPLUS_ERROR(remote_logger_, getDescription() << ": " << statussummary_);
    } catch (...) {
        rpct::tools::Lock pauselock(pause_);
        statussummary_ = std::string("FEB configuration not loaded for key ") + configkey_ + std::string(", caught unknown exception.");
        LOG4CPLUS_ERROR(logger_, statussummary_);
        LOG4CPLUS_ERROR(remote_logger_, getDescription() << ": " << statussummary_);
    }

    {
        rpct::tools::Lock pauselock(pause_);
        running_ = false;
    }

    return false;
}

bool xdaqFebSystem::monitor_wl(toolbox::task::WorkLoop *)
{
    try {
        { // swlock
            rpct::tools::Lock swlock(mutex_);

            rpct::tools::Chrono _chrono;
            _chrono.pause();

            { // hwlock
                rpct::tools::TLock<toolbox::BSem> hwlock(hwmutex_);

                faps_type::iterator IfapsEnd = faps_.end();
                for (faps_type::iterator Ifaps = faps_.begin()
                         ; Ifaps != IfapsEnd
                         ; )
                    {
                        {
                            LOG4CPLUS_TRACE(logger_, "starting next chunk");
                            rpct::tools::Lock pauselock(pause_);
                            if (paused_)
                                {
                                    hwlock.give();
                                    pause_.wait(false);
                                    pauselock.give(); // to keep hwlock always out of plock
                                    hwlock.take();
                                }
                            else if (stopped_)
                                throw rpct::exception::Exception("FEB monitoring was stopped.");
                        }
                        {
                            rpct::tools::Lock islock(interrupt_sync_);
                            _chrono.resume();
                            Ifaps->second->monitor(0); //TODO pass stop if needed
                            _chrono.pause();
                            
                            if (!getInterrupt())
                                ++Ifaps;
                        }
                    }
            } // hwlock

            monitoring_time_ = _chrono.duration();
            log("monitored");
        } // swlock
        { // plock
            rpct::tools::Lock pauselock(pause_);
            statussummary_ = FebSystem::getStatusSummary();
        }

        LOG4CPLUS_INFO(logger_, "FEBs monitored; " << statussummary_);
        LOG4CPLUS_INFO(remote_logger_, getDescription() << ": FEBs monitored; " << statussummary_);

    } catch (xcept::Exception & e) {
        rpct::tools::Lock pauselock(pause_);
        statussummary_ = std::string("FEBs NOT monitored, caught exception: ") + e.what();
        LOG4CPLUS_ERROR(logger_, statussummary_);
        LOG4CPLUS_ERROR(remote_logger_, getDescription() << ": " << statussummary_);
    } catch (rpct::exception::CCUException & e) {
        rpct::tools::Lock pauselock(pause_);
        statussummary_ = std::string("FEBs NOT monitored, caught exception: ") + e.what();
        LOG4CPLUS_ERROR(logger_, statussummary_);
        LOG4CPLUS_ERROR(remote_logger_, getDescription() << ": " << statussummary_);
    } catch (rpct::exception::Exception & e) {
        rpct::tools::Lock pauselock(pause_);
        statussummary_ = std::string("FEBs NOT monitored, caught exception: ") + e.what();
        LOG4CPLUS_ERROR(logger_, statussummary_);
        LOG4CPLUS_ERROR(remote_logger_, getDescription() << ": " << statussummary_);
    } catch (std::exception& e) {
        rpct::tools::Lock pauselock(pause_);
        statussummary_ = std::string("FEBs NOT monitored, caught exception: ") + e.what();
        LOG4CPLUS_ERROR(logger_, statussummary_);
        LOG4CPLUS_ERROR(remote_logger_, getDescription() << ": " << statussummary_);
    } catch (...) {
        rpct::tools::Lock pauselock(pause_);
        statussummary_ = std::string("FEBs NOT monitored, caught unknown exception");
        LOG4CPLUS_ERROR(logger_, statussummary_);
        LOG4CPLUS_ERROR(remote_logger_, getDescription() << ": " << statussummary_);
    }

    {
        rpct::tools::Lock pauselock(pause_);
        running_ = false;
    }

    return false;
}

void xdaqFebSystem::xgi_interface(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
    try {
        rpct::tools::Lock lock(mutex_, 10);

        rpct::FebSystemItem * fsi(this);

        cgicc::Cgicc cgi(in);

        unsigned int min_level(rpct::tools::State::info_);
        cgicc::const_form_iterator fi = cgi.getElement("fsi_level");
        if (fi != cgi.getElements().end())
            min_level = fi->getIntegerValue();
        if (min_level > rpct::tools::State::max_)
            min_level = rpct::tools::State::info_;

        int depth(-1);
        fi = cgi.getElement("fsi_depth");
        if (fi != cgi.getElements().end())
            depth = fi->getIntegerValue();


        int id = -1;
        std::string type;
        fi = cgi.getElement("fsi_id");
        if (fi != cgi.getElements().end())
            id = fi->getIntegerValue();
        fi = cgi.getElement("fsi_type");
        if (fi != cgi.getElements().end())
            type = fi->getValue();

        if (id > 0)
            fsi = getItem(type, id);
        if (!fsi)
            fsi = this;
        /*
          else
          {
          std::string action;
          fi = cgi.getElement("fsi_action");
          if (fi != cgi.getElements().end())
          {
          action = fi->getValue();
          if (action == "configure")
          {
          rpct::Configurable * configurable = dynamic_cast<rpct::Configurable *>(fsi);
          if (configurable)
          {
          rpct::tools::TLock<toolbox::BSem> hwlock(hwmutex_);
          configurable->configure();
          }
          }
          else if (action == "monitor")
          {
          rpct::IMonitorable * monitorable = dynamic_cast<rpct::IMonitorable *>(fsi);
          if (monitorable)
          {
          rpct::tools::TLock<toolbox::BSem> hwlock(hwmutex_);
          monitorable->monitor();
          }
          }
          std::stringstream sslocation;
          sslocation << "FebSystem?fsi_id=" << fsi->getId() << "&fsi_type=" << fsi->getType().getType();
          rpct::xdaqtools::xgitools::redirect(application_, *out, sslocation.str());
          }
          }
        */

        tools::html::header(*out, "FebSystem", fsi->getDescription(), true);

        *out << "<div class=\"box topleft\">" << std::endl;
        *out << "<h3>Minimum state</h3>\n <ul class=\"states\">\n" << std::endl;
        for (unsigned char level = 0 ; level < rpct::tools::State::max_ ; ++level)
            {
                *out << "<li class=\"fg_" << rpct::tools::State::getStateShort(level);
                if (level == min_level)
                    *out << " bg_" << rpct::tools::State::getStateShort(level);
                *out << "\">";
                *out << "<a href=\"?fsi_id=" << id << "&fsi_type=" << type
                     << "&fsi_level=" << (int)(level)
                     <<  "\">" << rpct::tools::State::getStateName(level) << "</a>\n</li>" << std::endl;
            }
        *out << "</ul>" << std::endl;
        *out << "</div>" << std::endl;

        *out << "<div class=\"box\"><div class=\"info\">Please refresh the page to get the latest states and values.</div></div>" << std::endl;

        *out << "<div class=\"box\">" << std::endl;
        if (fsi != this)
            *out << "<a style=\"display:block;margin-left:0;padding:2px 2px 2px 26px;background-image:url('/ts/ajaxell/extern/icons/html/icons/house.png');background-repeat:no-repeat;background-position:2px 2px;\" href=\"?"
                 << "fsi_level=" << min_level
                 << "\">" << getType().getType() << " " << getDescription() << "</a>" << std::endl;
        if (fsi->getTreeParent())
            *out << "<a style=\"display:block;margin-left:5px;padding:2px 2px 2px 26px;background-image:url('/ts/ajaxell/extern/icons/html/icons/arrow_turn_left.png');background-repeat:no-repeat;background-position:2px 2px;\" href=\"?fsi_id=" << fsi->getTreeParent()->getTreeItem()->getId()
                 << "&fsi_type=" << fsi->getTreeParent()->getTreeItem()->getType().getType()
                 << "&fsi_level=" << min_level
                 <<  "\">" << fsi->getTreeParent()->getTreeItem()->getType().getType() << " " << fsi->getTreeParent()->getTreeItem()->getDescription() << "</a>" << std::endl;

        *out << "<div class=\"fg_" << rpct::tools::State::getStateShort(fsi->getState())
             << " bg_" << rpct::tools::State::getStateShort(fsi->getPState()) << "\">" << std::endl;
        *out << "<strong>" << fsi->getType().getType() << "</strong></br>" << std::endl
             << fsi->getDescription() << " (" << fsi->getId() << ")<br />" << std::endl;
        *out << "state: " << rpct::tools::State::getStateName(fsi->getState()) << "<br />" << std::endl;

        fsi->LogState::printLogHTML(*out);

        tools::Parameters parameters;
        fsi->HardwareFlags::printParameters(parameters);
        fsi->printParameters(parameters);
        parameters.printHTML(*out);

        if (fsi->getTreeChild())
            {
                int curlevel(-1), n(0);
                FebSystemItem::iterator IfsiEnd = fsi->getTreeChild()->tree_end();
                for (FebSystemItem::iterator Ifsi = FebSystemItem::iterator(*fsi->getTreeChild(), depth)
                         ; Ifsi != IfsiEnd
                         ; ++Ifsi)
                    {
                        if ((*Ifsi)->getPState() >= min_level)
                            {
                                if (Ifsi.depth() <= curlevel)
                                    *out << "</li>" << std::endl;
                                n = curlevel - Ifsi.depth();
                                while (n-- > 0)
                                    *out << "</ul></li>" << std::endl;
                                if (Ifsi.depth() > curlevel)
                                    *out << "<ul class=\"states\">" << std::endl;
                                curlevel = Ifsi.depth();
                                *out << "<li class=\"fg_" << rpct::tools::State::getStateShort((*Ifsi)->getState())
                                     << " bg_" << rpct::tools::State::getStateShort((*Ifsi)->getPState()) << "\">";
                                *out << "<a href=\"?fsi_id=" << (*Ifsi)->getId() << "&fsi_type=" << (*Ifsi)->getType().getType()
                                     << "&fsi_level=" << min_level
                                     <<  "\">" << *(*Ifsi) << "</a>" << std::endl;
                            }
                        else
                            Ifsi.set_skip_children();
                    }
                while (curlevel-- > 0)
                    *out << "</ul></li>" << std::endl;
                *out << "</ul>";
            }
        *out << "</div>"<< std::endl;
        *out << "</div>"<< std::endl;
        tools::html::footer(*out);
    } catch(int e) {
        tools::html::header(*out, "FebSystem", "timeout", true);
        *out << "<div class=\"box\"><div class=\"info\">There was a timeout getting access to the Feb System information, most likely configuration or monitoring is ongoing.  Please try again later.</div></div>" << std::endl;
        tools::html::footer(*out);
    }
}

std::string xdaqFebSystem::getStatusSummary() const
{
    rpct::tools::Lock pauselock(pause_);
    return statussummary_;
}

void xdaqFebSystem::pause()
{
    rpct::tools::Lock pauselock(pause_);
    setInterrupt(1);
    paused_ = true;
}

void xdaqFebSystem::resume()
{
    {
        rpct::tools::Lock islock(interrupt_sync_);
    } // to not change interrupt back too soon
    rpct::tools::Lock pauselock(pause_);
    setInterrupt(0);
    paused_ = false;
    pause_.free_one(false);
}

void xdaqFebSystem::stop()
{
    {
        rpct::tools::Lock pauselock(pause_);
        setInterrupt(1);
        paused_ = false;
        stopped_ = true;
        pause_.free_one(false);
    }
    {
        rpct::tools::Lock swlock(mutex_);
        setInterrupt(0);
        stopped_ = false;
    }
}

} // namespace xdaqutils
} // namespace rpct
