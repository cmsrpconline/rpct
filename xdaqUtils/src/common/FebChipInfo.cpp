#include "rpct/xdaqutils/FebChipInfo.h"

#include "xdata/AbstractBag.h"

namespace rpct {

FebChipInfo::FebChipInfo(int chipid
                         , int flid
                         , int boe
                         , int wheel
                         , int layer
                         , int sector
                         , const std::string & subsector
                         , const std::string & partition
                         , int channel
                         , int address
                         , int position)
    : chipid_(chipid)
    , flid_(flid)
    , boe_(boe)
    , wheel_(wheel)
    , layer_(layer)
    , sector_(sector)
    , subsector_(subsector)
    , partition_(partition)
    , channel_(channel)
    , address_(address)
    , position_(position)
{}

void FebChipInfo::registerFields(xdata::AbstractBag * bag)
{
    bag->addField("id", &chipid_);
    bag->addField("flid", &flid_);
    bag->addField("boe", &boe_);
    bag->addField("wheel", &wheel_);
    bag->addField("layer", &layer_);
    bag->addField("sect", &sector_);
    bag->addField("subsect", &subsector_);
    bag->addField("part", &partition_);
    bag->addField("ch", &channel_);
    bag->addField("add", &address_);
    bag->addField("pos", &position_);
}

} // namespace rpct
