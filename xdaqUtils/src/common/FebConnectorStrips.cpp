#include "rpct/xdaqutils/FebConnectorStrips.h"

#include "xdata/AbstractBag.h"

namespace rpct {
namespace xdaqutils {

FebConnectorStrips::FebConnectorStrips(int _id
                                       , int _linkboard_id, int _linkboard_input
                                       , int _febboard_id, int _febboard_connector
                                       , std::string const & _location, std::string const & _partition, int _roll_connector
                                       , int _pins, int _first_strip, int _slope)
    : id_(_id)
    , linkboard_id_(_linkboard_id), linkboard_input_(_linkboard_input)
    , febboard_id_(_febboard_id), febboard_connector_(_febboard_connector)
    , location_(_location), partition_(_partition), roll_connector_(_roll_connector)
    , pins_(_pins), first_strip_(_first_strip), slope_(_slope)
{}

void FebConnectorStrips::registerFields(xdata::AbstractBag * bag)
{
    bag->addField("id", &id_);
    bag->addField("linkBoardId", &linkboard_id_);
    bag->addField("linkBoardInput", &linkboard_input_);
    bag->addField("febBoardId", &febboard_id_);
    bag->addField("febBoardConnector", &febboard_connector_);
    bag->addField("location", &location_);
    bag->addField("partition", &partition_);
    bag->addField("rollConnector", &roll_connector_);
    bag->addField("pins", &pins_);
    bag->addField("firstStrip", &first_strip_);
    bag->addField("slope", &slope_);
}

} // namespace xdaqutils
} // namespace rpct
