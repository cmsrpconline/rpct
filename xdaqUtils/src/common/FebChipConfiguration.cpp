#include "rpct/xdaqutils/FebChipConfiguration.h"

#include "xdata/AbstractBag.h"

namespace rpct {
namespace xdaqutils {

FebChipConfiguration::FebChipConfiguration(int id
                                           , unsigned int vth
                                           , unsigned int vmon
                                           , int vth_offset
                                           , int vmon_offset)
    : IFebChipConfiguration()
    , id_(id)
    , vth_(vth)
    , vmon_(vmon)
    , vth_offset_(vth_offset)
    , vmon_offset_(vmon_offset)
{}
void FebChipConfiguration::registerFields(xdata::AbstractBag * bag)
{
    bag->addField("id", &id_);
    bag->addField("vth", &vth_);
    bag->addField("vmon", &vmon_);
    bag->addField("vthOffset", &vth_offset_);
    bag->addField("vmonOffset", &vmon_offset_);
}

} // namespace xdaqutils
} // namespace rpct
