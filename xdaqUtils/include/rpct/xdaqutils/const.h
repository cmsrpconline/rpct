/** Filip Thyssen */

#ifndef rpct_xdaqutils_const_h_
#define rpct_xdaqutils_const_h_

namespace rpct {
namespace xdaqutils {

namespace febpublisher {
extern const char service_name_[];
extern const unsigned int service_instance_;
} // namespace febpublisher

} // namespace xdaqutils
} // namespace rpct

#endif // rpct_xdaqutils_const_h_
