#ifndef _RPCTFEBSTATEINFO_H_
#define _RPCTFEBSTATEINFO_H_


#include "xdata/Bag.h"
#include "xdata/Integer.h"
#include "xdata/Float.h"
#include "xdata/String.h"
#include "xdata/UnsignedLong.h"
namespace rpct {

class OptLinksStatusInfo {
private:
    xdata::String tbName;
    xdata::Integer optLinkNum;
    xdata::Integer status;

public:
    void registerFields(xdata::Bag<OptLinksStatusInfo>* bag) {
        bag->addField("tbName", &tbName);
        bag->addField("optLinkNum", &optLinkNum);
        bag->addField("status", &status);
    }

    xdata::String& getTbName() {
        return tbName;
    }

    void setTbName(xdata::String tbName) {
        this->tbName = tbName;
    }

    xdata::Integer& getOptLinkNum() {
        return optLinkNum;
    }

    void setOptLinkNum(xdata::Integer optLinkNum) {
        this->optLinkNum = optLinkNum;
    }

    xdata::Integer& getStatus() {
        return status;
    }

    void setStatus(xdata::Integer status) {
        this->status = status;
    }
};
typedef xdata::Bag<OptLinksStatusInfo> OptLinksStatusInfoBag;

typedef xdata::Vector<OptLinksStatusInfoBag > OptLinksStatusInfoVector;

}

#endif /*_RPCTFEBSTATEINFO_H_*/
