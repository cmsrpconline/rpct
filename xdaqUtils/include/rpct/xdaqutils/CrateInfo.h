#ifndef _CRATEINFO_H_
#define _CRATEINFO_H_


#include "xdata/Bag.h"
#include "xdata/Vector.h"
#include "xdata/String.h"
#include "xdata/Integer.h"

namespace rpct {
namespace xdaqutils {



class DeviceItemInfo {
private:
    xdata::Integer id;
    xdata::String name;
    xdata::String description;
    xdata::String type;
    xdata::Integer width;
    xdata::Integer number;
    xdata::Integer parentId;
    xdata::String accessType;
public:
    void registerFields(xdata::Bag<DeviceItemInfo>* bag) {
        bag->addField("id", &id);
        bag->addField("name", &name);
        bag->addField("description", &description);
        bag->addField("type", &type);
        bag->addField("width", &width);
        bag->addField("number", &number);
        bag->addField("parentId", &parentId);
        bag->addField("accessType", &accessType);
    }

    void init(int id, std::string name, std::string description, std::string type,
                int width, int number, int parentId, std::string accessType) {
        this->id = id;
        this->name = name;
        this->description = description;
        this->type = type;
        this->width = width;
        this->number = number;
        this->parentId = parentId;
        this->accessType = accessType;
    }
};
typedef xdata::Bag<DeviceItemInfo> DeviceItemInfoBag;



class HardwareItemInfo {
private:
    xdata::Integer id;
    xdata::String name;
    xdata::String type;
public:
    virtual ~HardwareItemInfo() {
    }

    virtual void registerFields(xdata::AbstractBag* bag) {
        bag->addField("id", &id);
        bag->addField("name", &name);
        bag->addField("type", &type);
    }

    virtual void setId(int id) {
        this->id = id;
    }

    virtual void setName(const std::string name) {
        this->name = name;
    }

    virtual void setType(const std::string type) {
        this->type = type;
    }
};


class DeviceInfo : public HardwareItemInfo {
public:
    typedef xdata::Vector<DeviceItemInfoBag> Items;
private:
    Items items;
    xdata::Integer position;
public:
    void registerFields(xdata::Bag<DeviceInfo>* bag) {
        HardwareItemInfo::registerFields(bag);
        bag->addField("items", &items);
        bag->addField("position", &position);
    }

    Items& getItems() {
        return items;
    }
    xdata::Integer getPosition() {
        return position;
    }

    void setPosition(int position) {
        this->position = position;
    }
};
typedef xdata::Bag<DeviceInfo> DeviceInfoBag;


class BoardInfo : public HardwareItemInfo {
public:
    typedef xdata::Vector<DeviceInfoBag> Devices;
private:
    Devices devices;
    xdata::Integer position;
public:
    void registerFields(xdata::Bag<BoardInfo>* bag) {
        HardwareItemInfo::registerFields(bag);
        bag->addField("devices", &devices);
        bag->addField("position", &position);
    }

    Devices& getDevices() {
        return devices;
    }

    xdata::Integer getPosition() {
        return position;
    }

    void setPosition(int position) {
        this->position = position;
    }
};
typedef xdata::Bag<BoardInfo> BoardInfoBag;

class CrateInfo : public HardwareItemInfo {
public:
    typedef xdata::Vector<BoardInfoBag> Boards;
private:
    Boards boards;
public:
    void registerFields(xdata::Bag<CrateInfo>* bag) {
        HardwareItemInfo::registerFields(bag);
        bag->addField("boards", &boards);
    }

    Boards& getBoards() {
        return boards;
    }
};


typedef xdata::Bag<CrateInfo> CrateInfoBag;


}}


#endif /*_CRATEINFO_H_*/
