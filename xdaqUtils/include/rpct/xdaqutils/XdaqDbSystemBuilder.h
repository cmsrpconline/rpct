#ifndef RPCTXDAQDBSYSTEMBUILDER_H_
#define RPCTXDAQDBSYSTEMBUILDER_H_


#include "xdaq/Application.h"
#include "rpct/ii/IHardwareItem.h"
#include "rpct/ii/ICrate.h"
#include "rpct/devices/System.h"
#include "rpct/devices/LinkSystem.h"
#include "rpct/xdaqutils/XdaqDbServiceClient.h"
#include "rpct/xdaqutils/CcuRingInfo.h"
#include <string>
#include <log4cplus/logger.h>


namespace rpct {
    
class FecManager;
class VmeCrate;
class LinkBox;
class RPCFebSystem;
    
namespace xdaqutils {


class XdaqDbSystemBuilder {
public:
	static void setupLogger(std::string namePrefix);

private:
	static log4cplus::Logger logger;
    bool mock;
    XdaqDbServiceClient dbServiceClient;
    ChipInfo& getChipInfo(BoardInfo& boardInfo, int index);
    ChipInfo& extractExpectedChipInfo(BoardInfo& boardInfo, std::string chipType);
public:
    XdaqDbSystemBuilder(xdaq::Application* app, bool mock = false);
    virtual ~XdaqDbSystemBuilder() {}
    void buildFebSystem(RPCFebSystem & system, CcuRingInfo::FebBoardInfoVector & febBoardInfos);
    void buildCcuRing(LinkSystem& system, std::string host, int port, int instance, RPCFebSystem * febsystem = 0);
    void buildTriggerCrate(System& system, std::string host, int port, std::string className, int instance);
    void buildVmeCrate(System& system, std::string host, int port, std::string className, int instance);
};

}
}

#endif 
