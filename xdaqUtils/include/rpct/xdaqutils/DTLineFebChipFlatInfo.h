/** Filip Thyssen */

#ifndef rpct_xdaqutils_DTLineFebChipFlatInfo_h
#define rpct_xdaqutils_DTLineFebChipFlatInfo_h

#include "xdata/Integer.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Bag.h"

namespace xdata {
class AbstractBag;
}

namespace rpct {
namespace xdaqutils {

class DTLineFebChipFlatInfo
{
public:
    DTLineFebChipFlatInfo(int _chip_id = -1, int _board_id = -1
                          , std::string const & _name = std::string("")
                          , int _i2c_address = -1
                          , int _position = -1
                          , bool _chip_disabled = false, bool _board_disabled = false
                          , std::string const & _location = std::string(""), std::string const & _partition = std::string(""));

    void registerFields(xdata::AbstractBag * _bag);

    int getChipId() const;
    int getBoardId() const;
    std::string const & getName() const;
    int getI2cAddress() const;
    int getPosition() const;
    bool getChipDisabled() const;
    bool getBoardDisabled() const;
    std::string const & getLocation() const;
    std::string const & getPartition() const;

    void setChipId(int _chip_id);
    void setBoardId(int _board_id);
    void setName(std::string const & _name);
    void setI2cAddress(int _i2c_address);
    void setPosition(int _position);
    void setChipDisabled(bool _chip_disabled);
    void setBoardDisabled(bool _board_disabled);
    void setLocation(std::string const & _location);
    void setPartition(std::string const & _partition);

protected:
    xdata::Integer chip_id_, board_id_;
    xdata::String name_;
    xdata::Integer i2c_address_;
    xdata::Integer position_;
    xdata::Boolean chip_disabled_, board_disabled_;
    xdata::String location_, partition_;
};

} // namespace xdaqutils
} // namespace rpct

#include "rpct/xdaqutils/DTLineFebChipFlatInfo-inl.h"

#endif // rpct_xdaqutils_DTLineFebChipFlatInfo_h
