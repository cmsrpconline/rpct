/** Filip Thyssen */

#ifndef rpct_xdaqutils_xdaqRPCFebSystem_h_
#define rpct_xdaqutils_xdaqRPCFebSystem_h_

#include "rpct/devices/RPCFebSystem.h"
#include "rpct/xdaqutils/xdaqFebSystem.h"
#include "rpct/xdaqutils/const.h"

namespace rpct {
namespace xdaqutils {

class xdaqRPCFebSystem
    : public rpct::RPCFebSystem
    , public xdaqFebSystem
{
public:
    xdaqRPCFebSystem(LinkSystem & linksystem
                     , xdaq::Application & application
                     , toolbox::BSem & hwmutex
                     , const std::string & service_name = std::string(febpublisher::service_name_)
                     , unsigned int service_instance = febpublisher::service_instance_
                     , size_t max_size = rpct::xdaqtools::publisher::preset::client_itemlist_size_);
};

} // namespace xdaqutils
} // namespace rpct

#endif // rpct_xdaqutils_xdaqRPCFebSystem_h_
