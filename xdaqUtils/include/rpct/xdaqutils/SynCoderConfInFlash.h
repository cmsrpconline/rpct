#ifndef _RPCTSYNCODERCONFINFLASH_H_
#define _RPCTSYNCODERCONFINFLASH_H_

#include "rpct/std/tbutil.h"
#include "rpct/ii/ISynCoderConfInFlash.h"
#include "xdata/Bag.h"
#include "xdata/Integer.h"
#include "xdata/String.h"
#include "xdata/Binary.h"


namespace rpct {
namespace xdaqutils {

class SynCoderConfInFlash  : virtual public ISynCoderConfInFlash {
private:
	xdata::Integer  chipId_;
	xdata::Integer  synCoderConfId_;
	xdata::Integer  beginAddress_;
	xdata::Integer  endAddress_;
    
public:
    void registerFields(xdata::Bag<SynCoderConfInFlash>* bag) {
        bag->addField("chipId", &chipId_);
        bag->addField("confId", &synCoderConfId_);
        bag->addField("begAddr", &beginAddress_);
        bag->addField("endAddr", &endAddress_);
    }
    
    virtual int getChipId() {
        return chipId_;
    }    
    void setChipId(xdata::Integer& chipId) {
        this->chipId_ = chipId;
    }
    void setChipId(xdata::Integer chipId) {
            this->chipId_ = chipId;
    }

    virtual int getSynCoderConfId() {
        return synCoderConfId_;
    }
    void setSynCoderConfId(xdata::Integer& synCoderConfId) {
        this->synCoderConfId_ = synCoderConfId;
    }
    void setSynCoderConfId(xdata::Integer synCoderConfId) {
        this->synCoderConfId_ = synCoderConfId;
    }

    virtual int getBeginAddress() {
        return beginAddress_;
    }
    void setBeginAddress(xdata::Integer& beginAddress) {
        this->beginAddress_ = beginAddress;
    }
    void setBeginAddress(xdata::Integer beginAddress) {
        this->beginAddress_ = beginAddress;
    }

    virtual int getEndAddress() {
        return endAddress_;
    }
    void setEndAddress(xdata::Integer& endAddress) {
        this->endAddress_ = endAddress;
    }
    void setEndAddress(xdata::Integer endAddress) {
        this->endAddress_ = endAddress;
    }

};

}}

#endif /*_RPCTSYNCODERCONFIGURATION_H_*/
