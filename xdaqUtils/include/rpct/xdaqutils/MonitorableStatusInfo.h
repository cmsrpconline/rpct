/*
 *      Author: Karol Bunkowski
 *      Version: $Id$
 */

#ifndef MONITORABLESTATUSINFO_H_
#define MONITORABLESTATUSINFO_H_

#include "rpct/ii/Monitorable.h"
#include "xdata/Bag.h"
#include "xdata/Integer.h"
#include "xdata/String.h"
#include "xdata/UnsignedLong.h"

namespace rpct {

class MonitorableStatusInfo {
private:
	xdata::String boardName;
	xdata::String chipName;
	xdata::String name;
	xdata::Integer level;
	xdata::String message;
	xdata::UnsignedLong value;

public:
	void registerFields(xdata::Bag<MonitorableStatusInfo>* bag) {
		bag->addField("boardName", &boardName);
		bag->addField("chipName", &chipName);
		bag->addField("name", &name);
		bag->addField("level", &level);
		bag->addField("message", &message);
		bag->addField("value", &value);
	}

	void init(xdata::String boardName, xdata::String chipName, MonitorableStatus monStatus) {
		this->boardName = boardName;
		this->chipName = chipName;
		this->name = monStatus.monitorItem;
		this->level = monStatus.level;
		this->message = monStatus.message;
		this->value = monStatus.value;
	}
};

typedef xdata::Bag<MonitorableStatusInfo> MonitorableStatusInfoBag;
}
#endif /* MONITORABLESTATUSINFO_H_ */
