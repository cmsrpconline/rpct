#ifndef _RPCTCHIPINFO_H_
#define _RPCTCHIPINFO_H_


#include "xdata/Bag.h"
#include "xdata/Integer.h"
#include "xdata/String.h"
#include "xdata/Boolean.h"
namespace rpct {

class ChipInfo {
private:
    xdata::Integer id;
    xdata::String type;
    xdata::Integer position;
    xdata::Boolean disabled;

public:
    void registerFields(xdata::Bag<ChipInfo>* bag) {
        bag->addField("id", &id);
        bag->addField("type", &type);
        bag->addField("position", &position);
        bag->addField("disabled", &disabled);
    }

    xdata::Integer& getId() {
        return id;
    }

    void setId(xdata::Integer& id) {
        this->id = id;
    }

    xdata::String& getType() {
        return type;
    }

    void setType(xdata::String& type) {
        this->type = type;
    }

    xdata::Integer& getPosition() {
        return position;
    }

    void setPosition(xdata::Integer& position) {
        this->position = position;
    }

    xdata::Boolean& isDisabled() {
        return disabled;
    }

    void setDisabled(xdata::Boolean& disabled) {
        this->disabled = disabled;
    }
};

}

#endif /*_RPCTFEBSTATEINFO_H_*/
