#ifndef _RPCTCHIPCONFIGURATION_H_
#define _RPCTCHIPCONFIGURATION_H_


#include "rpct/ii/DeviceSettings.h"

namespace rpct {
namespace xdaqutils {

class ChipConfiguration : virtual public DeviceSettings  {    
public:
    virtual ~ChipConfiguration() {
    }
    
    virtual int getConfigurationId() = 0;
};

}}

#endif 
