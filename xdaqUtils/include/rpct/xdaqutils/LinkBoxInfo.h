#ifndef _RPCTLINKBOXINFO_H_
#define _RPCTLINKBOXINFO_H_


#include "xdata/Bag.h"
#include "xdata/Integer.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "rpct/xdaqutils/BoardInfo.h"

namespace rpct {

class LinkBoxInfo {
public:
    typedef xdata::Vector<xdata::Bag<BoardInfo> > BoardInfoVector; 
private:
    xdata::Integer id;
    xdata::Integer ccu10Address;
    xdata::Integer ccu20Address;
    xdata::String name;
    xdata::String label;  
    xdata::Boolean disabled;   
    BoardInfoVector boardInfos;
    
public:
    void registerFields(xdata::Bag<LinkBoxInfo>* bag) {
        bag->addField("id", &id);
        bag->addField("ccu10Address", &ccu10Address);
        bag->addField("ccu20Address", &ccu20Address);
        bag->addField("name", &name);
        bag->addField("label", &label);
        bag->addField("disabled", &disabled);  
        bag->addField("boardInfos", &boardInfos);       
    }
    
    xdata::Integer& getId() {
        return id;
    }
    
    void setId(xdata::Integer& id) {
        this->id = id;
    }
    
    xdata::Integer& getCcu10Address() {
        return ccu10Address;
    }
    
    void setCcu10Address(xdata::Integer& ccu10Address) {
        this->ccu10Address = ccu10Address;
    }
    
    xdata::Integer& getCcu20Address() {
        return ccu20Address;
    }
    
    void setCcu20Address(xdata::Integer& ccu20Address) {
        this->ccu20Address = ccu20Address;
    }
    
    xdata::String& getName() {
        return name;
    }
    
    void setName(xdata::String& name) {
        this->name = name;
    }
    
    xdata::String& getLabel() {
        return label;
    }
    
    void setLabel(xdata::String& label) {
        this->label = label;
    } 

    xdata::Boolean& isDisabled() {
        return disabled;
    }

    void setDisabled(xdata::Boolean& disabled) {
        this->disabled = disabled;
    }   
    
    BoardInfoVector& getBoardInfos() {
        return boardInfos;
    }
    
    void setBoardInfos(BoardInfoVector& boardInfos) {
        this->boardInfos = boardInfos;
    }
};

}

#endif /*_RPCTFEBSTATEINFO_H_*/
