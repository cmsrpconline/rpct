#ifndef _RPCTSYNCODERCONFIGURATION_H_
#define _RPCTSYNCODERCONFIGURATION_H_

#include "rpct/xdaqutils/ChipConfiguration.h"
#include "rpct/devices/SynCoderSettings.h"
#include "rpct/devices/LinkBoard.h"
#include "rpct/std/tbutil.h"

#include "xdata/Bag.h"
#include "xdata/Integer.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "xdata/Binary.h"

#include <boost/scoped_ptr.hpp>

namespace rpct {
namespace xdaqutils {

class SynCoderConfiguration : virtual public SynCoderSettings, virtual public ChipConfiguration {
private:
    xdata::Integer configurationId_;
    xdata::Integer windowOpen_;
    xdata::Integer windowClose_;
    xdata::Boolean invertClock_;
    xdata::Integer lmuxInDelay_;
    xdata::Integer rbcDelay_;
    xdata::Integer bcn0Delay_;
    xdata::Integer dataTrgDelay_;
    xdata::Integer dataDaqDelay_;
    xdata::Integer pulserTimerTrgDelay_;
    xdata::Binary inChannelsEna_;
    
    boost::scoped_ptr<boost::dynamic_bitset<> > inChannelsEnaPtr_; 
public:
    void registerFields(xdata::Bag<SynCoderConfiguration>* bag) {
        bag->addField("id", &configurationId_);
        bag->addField("windowOpen", &windowOpen_);
        bag->addField("windowClose", &windowClose_);
        bag->addField("invertClock", &invertClock_);
        bag->addField("lmuxInDelay", &lmuxInDelay_);
        bag->addField("rbcDelay", &rbcDelay_);
        bag->addField("bcn0Delay", &bcn0Delay_);
        bag->addField("dataTrgDelay", &dataTrgDelay_);
        bag->addField("dataDaqDelay", &dataDaqDelay_);
        bag->addField("pulserTimerTrgDelay", &pulserTimerTrgDelay_);
        bag->addField("inChannelsEna", &inChannelsEna_);
    }
    
    virtual int getConfigurationId() {
        return configurationId_;
    }    
    void setConfigurationId(xdata::Integer& configurationId) {
        this->configurationId_ = configurationId;
    }
    
    virtual unsigned int getWindowOpen() {
        return windowOpen_;
    }    
    void setWindowOpen(xdata::Integer& windowOpen) {
        this->windowOpen_ = windowOpen;
    }
    
    virtual unsigned int getWindowClose() {
        return windowClose_;
    }    
    void setWindowClose(xdata::Integer& windowClose) {
        this->windowClose_ = windowClose;
    }    
    
    virtual bool getInvertClock() {
        return invertClock_;
    }    
    void setInvertClock(xdata::Boolean& invertClock) {
        this->invertClock_ = invertClock;
    }
    
    virtual unsigned int getLMuxInDelay() {
        return lmuxInDelay_;
    }    
    void setLMuxInDelay(xdata::Integer& lmuxInDelay) {
        this->lmuxInDelay_ = lmuxInDelay;
    }
    
    virtual unsigned int getRbcDelay() {
        return rbcDelay_;
    }    
    void setRrbcDelay(xdata::Integer& rbcDelay) {
        this->rbcDelay_ = rbcDelay;
    }
    
    virtual unsigned int getBcn0Delay() {
        return bcn0Delay_;
    }    
    void setBcn0Delay(xdata::Integer& bcn0Delay) {
        this->bcn0Delay_ = bcn0Delay;
    }
    
    virtual unsigned int getDataTrgDelay() {
        return dataTrgDelay_;
    }    
    void setDataTrgDelay(xdata::Integer& dataTrgDelay) {
        this->dataTrgDelay_ = dataTrgDelay;
    }
    
    virtual unsigned int getDataDaqDelay() {
        return dataDaqDelay_;
    }    
    void setDataDaqDelay(xdata::Integer& dataDaqDelay) {
        this->dataDaqDelay_ = dataDaqDelay;
    }
    
    virtual unsigned int getPulserTimerTrgDelay() {
        return pulserTimerTrgDelay_;
    }    
    void setPulserTimerTrgDelay(xdata::Integer& pulserTimerTrgDelay) {
        this->pulserTimerTrgDelay_ = pulserTimerTrgDelay;
    }    
    
    xdata::Binary& getInChannelsEnaAsBinary() {
        return inChannelsEna_;
    }    
    void setInChannelsEna(xdata::Binary& inChannelsEna) {
        this->inChannelsEna_ = inChannelsEna;
    }
    
    virtual boost::dynamic_bitset<>& getInChannelsEna() {
        if (inChannelsEnaPtr_.get() == 0) {
            inChannelsEnaPtr_.reset(createBitsetFromBits(inChannelsEna_.buffer(), inChannelsEna_.size() * 8));
        }
        return *inChannelsEnaPtr_;
    }     
    
    virtual rpct::HardwareItemType getDeviceType() {
        return rpct::SynCoder::TYPE;
    }
};

}}

#endif /*_RPCTSYNCODERCONFIGURATION_H_*/
