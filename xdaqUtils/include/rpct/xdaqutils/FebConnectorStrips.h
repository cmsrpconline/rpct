/** Filip Thyssen */

#ifndef rpct_xdaqutils_FebConnectorStrips_h_
#define rpct_xdaqutils_FebConnectorStrips_h_

#include "xdata/Integer.h"
#include "xdata/String.h"

namespace xdata {
class AbstractBag;
}

namespace rpct {
namespace xdaqutils {

class FebConnectorStrips
{
public:
    FebConnectorStrips(int _id = -1
                       , int _linkboard_id = -1, int _linkboard_input = 0
                       , int _febboard_id = -1, int _febboard_connector = 0
                       , std::string const & _location = std::string(""), std::string const & _partition = std::string(""), int _roll_connector = 0
                       , int _pins = 0xff, int _first_strip = 1, int _slope = 1);

    void registerFields(xdata::AbstractBag * bag);

    int getId() const;
    int getLinkBoardId() const;
    int getLinkBoardInput() const;
    int getFebBoardId() const;
    int getFebBoardConnector() const;
    std::string const & getLocation() const;
    std::string const & getPartition() const;
    int getRollConnector() const;
    int getPins() const;
    int getFirstStrip() const;
    int getSlope() const;

    void setId(int _id);
    void setLinkBoardId(int _linkboard_id);
    void setLinkBoardInput(int _linkboard_input);
    void setFebBoardId(int _febboard_id);
    void setFebBoardConnector(int _febboard_connector);
    void setLocation(std::string const & _location);
    void setPartition(std::string const & _partition);
    void setRollConnector(int _roll_connector);
    void setPins(int _pins);
    void setFirstStrip(int _first_strip);
    void setSlope(int _slope);

protected:
    xdata::Integer id_;
    xdata::Integer linkboard_id_, linkboard_input_;
    xdata::Integer febboard_id_, febboard_connector_;
    xdata::String location_, partition_;
    xdata::Integer roll_connector_;
    xdata::Integer pins_, first_strip_, slope_;
};

} // namespace xdaqutils
} // namespace rpct

#include "rpct/xdaqutils/FebConnectorStrips-inl.h"

#endif // rpct_xdaqutils_FebConnectorStrips_h_
