#ifndef RPCTREMOTEEXCEPTION_H_
#define RPCTREMOTEEXCEPTION_H_


#include "xoap/exception/Exception.h" 


namespace rpct {
    namespace xdaqutils {
        
        class RemoteException : public xoap::exception::Exception {
        public:
 
            RemoteException(std::string name,
                      std::string message,
                      std::string module,
                      int line,
                      std::string function)
            : xoap::exception::Exception(name, message, module, line, function) {
            }
 
            RemoteException(std::string name,
                      std::string message,
                      std::string module,
                      int line,
                      std::string function,
                      xcept::Exception & e)
            : xoap::exception::Exception(name, message, module, line, function, e)  {
            }
        };
        
        
        
    }
}

#endif /*RPCTREMOTEEXCEPTION_H_*/
