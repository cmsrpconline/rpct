#ifndef _RPCTBOARDHISTOINFO_H_
#define _RPCTBOARDHISTOINFO_H_


#include "xdata/Bag.h"
#include "xdata/Integer.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "rpct/xdaqutils/FebHistoInfo.h"

namespace rpct {

class BoardHistoInfo {
public:
    typedef std::vector<rpct::FebHistoInfo> FebHistoInfoVector;
private:
    xdata::Integer id;
    xdata::Integer chipId;
    xdata::Integer xdaqAppInstance;
    xdata::String chamberName;
    FebHistoInfoVector febHistoInfos;
    
public:
    
    xdata::Integer& getId() {
        return id;
    }
    
    void setId(xdata::Integer& id) {
        this->id = id;
    }
    
    xdata::Integer& getChipId() {
        return chipId;
    }
    
    void setChipId(xdata::Integer& chipId) {
        this->chipId = chipId;
    }
    
    xdata::Integer& getXdaqAppInstance() {
        return xdaqAppInstance;
    }
    
    void setXdaqAppInstance(xdata::Integer& xdaqAppInstance) {
        this->xdaqAppInstance = xdaqAppInstance;
    }
    
    xdata::String& getChamberName() {
        return chamberName;
    }
    
    void setChamberName(xdata::String& chamberName) {
        this->chamberName = chamberName;
    }


    FebHistoInfoVector& getFebHistoInfos() {
        return febHistoInfos;
    }
    
    void setFebHistoInfos(FebHistoInfoVector& febHistoInfos) {
        this->febHistoInfos = febHistoInfos;
    }
};

}

#endif /*_RPCTFEBSTATEINFO_H_*/
