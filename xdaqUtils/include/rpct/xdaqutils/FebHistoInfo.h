#ifndef _RPCTFEBHISTOINFO_H_
#define _RPCTFEBHISTOINFO_H_


#include "xdata/Bag.h"
#include "xdata/Integer.h"
#include "xdata/String.h"
#include "xdata/Vector.h"

namespace rpct {

class FebHistoInfo {
public:
    typedef xdata::Vector<xdata::Integer> Strips; 
private:
    xdata::Integer id;
    xdata::Integer febConnectorNum;
    xdata::Integer chip;
    xdata::String febLocalEtaPartition;
    xdata::Integer febI2cAddress;
    xdata::Vector<xdata::Integer> cableChannelNums;
    xdata::Vector<xdata::Integer> strips;
    xdata::Vector<xdata::Integer> stripIds;

public:
    void registerFields(xdata::Bag<FebHistoInfo>* bag) {
        bag->addField("id", &id);
        bag->addField("febConnectorNum", &febConnectorNum);
        bag->addField("chip", &chip);
        bag->addField("febLocalEtaPartition", &febLocalEtaPartition);
        bag->addField("febI2cAddress", &febI2cAddress);
        bag->addField("cableChannelNums", &cableChannelNums);
        bag->addField("strips", &strips);
        bag->addField("stripIds", &stripIds);
    }

    xdata::Integer& getId() {
        return id;
    }

    void setId(xdata::Integer& id) {
        this->id = id;
    }

    xdata::Integer& getFebConnectorNum() {
        return febConnectorNum;
    }

    void setFebConnectorNum(xdata::Integer& febConnectorNum) {
        this->febConnectorNum = febConnectorNum;
    }

    xdata::Integer& getChip() {
        return chip;
    }

    void setChip(xdata::Integer& chip) {
        this->chip = chip;
    }

    xdata::String& getFebLocalEtaPartition() {
        return febLocalEtaPartition;
    }

    void setFebLocalEtaPartition(xdata::String& febLocalEtaPartition) {
        this->febLocalEtaPartition = febLocalEtaPartition;
    }

    xdata::Integer& getFebI2cAddress() {
        return febI2cAddress;
    }

    void setFebI2cAddress(xdata::Integer& febI2cAddress) {
        this->febI2cAddress = febI2cAddress;
    }

    xdata::Vector<xdata::Integer>& getCableChannelNums() {
        return cableChannelNums;
    }

    void setCableChannelNums(xdata::Vector<xdata::Integer>& cableChannelNums) {
        this->cableChannelNums = cableChannelNums;
    }

    xdata::Vector<xdata::Integer>& getStrips() {
        return strips;
    }

    void setStrips(xdata::Vector<xdata::Integer>& strips) {
        this->strips = strips;
    }
    xdata::Vector<xdata::Integer>& getStripIds() {
        return stripIds;
    }

    void setStripIds(xdata::Vector<xdata::Integer>& stripIds) {
        this->stripIds = stripIds;
    }
};

}

#endif /*_RPCTFEBHISTOINFO_H_*/
