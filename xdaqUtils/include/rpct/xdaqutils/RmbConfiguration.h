#ifndef _RPCTRMBCONFIGURATION_H_
#define _RPCTRMBCONFIGURATION_H_


#include "xdata/Bag.h"
#include "xdata/Integer.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "xdata/Binary.h"


#include "rpct/devices/RmbSettings.h"
#include "rpct/devices/TriggerBoard.h"
#include "rpct/xdaqutils/ChipConfiguration.h"

namespace rpct {
namespace xdaqutils {

class RmbConfiguration : public RmbSettings, virtual public ChipConfiguration {
private:
    xdata::Integer configurationId_;
    xdata::Binary recMuxClkInv_;
    xdata::Binary recMuxClk90_;
    xdata::Binary recMuxRegAdd_;
    xdata::Vector<xdata::Integer> recMuxDelay_;
    xdata::Vector<xdata::Integer> recDataDelay_;  
    xdata::Integer chanEna_; 
    xdata::Integer preTriggerVal_; 
    xdata::Integer postTriggerVal_; 
    xdata::Integer dataDelay_; 
    xdata::Integer trgDelay_;

    boost::scoped_ptr<boost::dynamic_bitset<> > recMuxClkInvPtr_; 
    boost::scoped_ptr<boost::dynamic_bitset<> > recMuxClk90Ptr_;
    boost::scoped_ptr<boost::dynamic_bitset<> > recMuxRegAddPtr_; 
    boost::scoped_ptr<std::vector<int> > recMuxDelayPtr_; 
    boost::scoped_ptr<std::vector<int> > recDataDelayPtr_; 
public:
    void registerFields(xdata::Bag<RmbConfiguration>* bag) {
        bag->addField("id", &configurationId_);
        bag->addField("recMuxClkInv", &recMuxClkInv_);
        bag->addField("recMuxClk90", &recMuxClk90_);
        bag->addField("recMuxRegAdd", &recMuxRegAdd_);
        bag->addField("recMuxDelay", &recMuxDelay_);
        bag->addField("recDataDelay", &recDataDelay_);
        bag->addField("chanEna", &chanEna_);
        bag->addField("preTriggerVal", &preTriggerVal_);
        bag->addField("postTriggerVal", &postTriggerVal_);
        bag->addField("dataDelay", &dataDelay_);
        bag->addField("trgDelay", &trgDelay_);
    }
    
    virtual int getConfigurationId() {
        return configurationId_;
    }    
    void setConfigurationId(xdata::Integer& configurationId) {
        this->configurationId_ = configurationId;
    }    

    xdata::Binary& getRecMuxClkInvAsBinary() {
        return recMuxClkInv_;
    }    
    void setRecMuxClkInv(xdata::Binary& recMuxClkInv) {
        this->recMuxClkInv_ = recMuxClkInv;
    }    

    xdata::Binary& getRecMuxClk90AsBinary() {
        return recMuxClk90_;
    }    
    void setRecMuxClk90(xdata::Binary& recMuxClk90) {
        this->recMuxClk90_ = recMuxClk90;
    }
    
    xdata::Binary& getRecMuxRegAddAsBinary() {
        return recMuxRegAdd_;
    }    
    void setRecMuxRegAdd(xdata::Binary& recMuxRegAdd) {
        this->recMuxRegAdd_ = recMuxRegAdd;
    }       
    
    xdata::Vector<xdata::Integer>& getRecMuxDelayAsBinary() {
        return recMuxDelay_;
    }    
    void setRecMuxDelay(xdata::Vector<xdata::Integer>& recMuxDelay) {
        if ((int) recMuxDelay.size() != Rmb::TB_LINK_NUM) {
            throw IllegalArgumentException("PacConfiguration::setRecMuxDelay: invalid size");
        }
        this->recMuxDelay_ = recMuxDelay;
    }        
    
    xdata::Vector<xdata::Integer>& getRecDataDelayAsBinary() {
        return recDataDelay_;
    }    
    void setRecDataDelay(xdata::Vector<xdata::Integer>& recDataDelay) {
        if ((int) recDataDelay.size() != Rmb::TB_LINK_NUM) {
            throw IllegalArgumentException("PacConfiguration::setRecDataDelay: invalid size");
        }
        this->recDataDelay_ = recDataDelay;
    }
    
    virtual boost::dynamic_bitset<>& getRecMuxClkInv() {
        if (recMuxClkInvPtr_.get() == 0) {
            recMuxClkInvPtr_.reset(createBitsetFromBits(recMuxClkInv_.buffer(), recMuxClkInv_.size() * 8));
        }
        return *recMuxClkInvPtr_;
    }   
    
    virtual boost::dynamic_bitset<>& getRecMuxClk90() {
        if (recMuxClk90Ptr_.get() == 0) {
            recMuxClk90Ptr_.reset(createBitsetFromBits(recMuxClk90_.buffer(), recMuxClk90_.size() * 8));
        }
        return *recMuxClk90Ptr_;
    }
    
    virtual boost::dynamic_bitset<>& getRecMuxRegAdd() {
        if (recMuxRegAddPtr_.get() == 0) {
            recMuxRegAddPtr_.reset(createBitsetFromBits(recMuxRegAdd_.buffer(), recMuxRegAdd_.size() * 8));
        }
        return *recMuxRegAddPtr_;
    }
    
    virtual std::vector<int>& getRecMuxDelay() {
        if (recMuxDelayPtr_.get() == 0) {
            recMuxDelayPtr_.reset(new std::vector<int>(Rmb::TB_LINK_NUM));
            if ((int) recMuxDelay_.size() != Rmb::TB_LINK_NUM) {
                throw IllegalArgumentException("PacConfiguration::getRecMuxDelay: invalid size");
            }
            for (std::vector<int>::size_type i = 0; i < recMuxDelay_.size(); i++) {
                (*recMuxDelayPtr_)[i] = recMuxDelay_[i];
            }            
        }
        return *recMuxDelayPtr_;
    } 
    
    virtual std::vector<int>& getRecDataDelay() {
        if (recDataDelayPtr_.get() == 0) {
            recDataDelayPtr_.reset(new std::vector<int>(Rmb::TB_LINK_NUM));
            if ((int) recDataDelay_.size() != Rmb::TB_LINK_NUM) {
                throw IllegalArgumentException("PacConfiguration::getRecDataDelay: invalid size");
            }
            for (std::vector<int>::size_type i = 0; i < recDataDelay_.size(); i++) {
                (*recDataDelayPtr_)[i] = recDataDelay_[i];
            }            
        }
        return *recDataDelayPtr_;
    }
    
    virtual int getChanEna() {
        return chanEna_;
    }  
    
    virtual int getPreTriggerVal() {
        return preTriggerVal_;
    }  
    
    virtual int getPostTriggerVal() {
        return postTriggerVal_;
    }  
    
    virtual int getDataDelay() {
        return dataDelay_;
    }   
    
    virtual int getTrgDelay() {
        return trgDelay_;
    }      
    
    virtual rpct::HardwareItemType getDeviceType() {
        return rpct::Rmb::TYPE;
    }
};

}}

#endif 
