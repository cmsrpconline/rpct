#ifndef _RPCTOPTOCONFIGURATION_H_
#define _RPCTOPTOCONFIGURATION_H_


#include "xdata/Bag.h"
#include "xdata/Integer.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "xdata/Binary.h"

#include "rpct/xdaqutils/ChipConfiguration.h"
#include "rpct/devices/OptoSettings.h"
#include "rpct/devices/TriggerBoard.h"


namespace rpct {
namespace xdaqutils {

class OptoConfiguration : virtual public OptoSettings, virtual public ChipConfiguration {
private:
    xdata::Integer configurationId_;
    xdata::Boolean enableLink0_;
    xdata::Boolean enableLink1_;
    xdata::Boolean enableLink2_;
    
    //TODO: andres
    //xdata::Integer tlkEnable_;
    //xdata::Integer tlkRefn_;
    //xdata::Integer recSynchRequest_;
    //xdata::Integer recCheckEnable_;
    //xdata::Integer recCheckDataEnable_;
    //xdata::Integer BCN0Delay_;

public:
    void registerFields(xdata::Bag<OptoConfiguration>* bag) {
        bag->addField("id", &configurationId_);
        bag->addField("enableLink0", &enableLink0_);
        bag->addField("enableLink1", &enableLink1_);
        bag->addField("enableLink2", &enableLink2_);
        //TODO: andres
        //bag->addField("tlkEnable", &tlkEnable_);
        //bag->addField("tlkRefn", &tlkRefn_);
        //bag->addField("recSynchRequest", &recSynchRequest_);
        //bag->addField("recCheckEnable", &recCheckEnable_);
        //bag->addField("recCheckDataEnable", &recCheckDataEnable_);
        //bag->addField("BCN0Delay", &BCN0Delay_);

    }
    
    virtual int getConfigurationId() {
        return configurationId_;
    }    
    void setConfigurationId(xdata::Integer& configurationId) {
        this->configurationId_ = configurationId;
    }
    
    virtual bool isEnableLink0() {
        return enableLink0_;
    }   
    
    virtual bool isEnableLink1() {
        return enableLink1_;
    }   
    
    virtual bool isEnableLink2() {
        return enableLink2_;
    }   
    
    virtual rpct::HardwareItemType getDeviceType() {
        return rpct::Opto::TYPE;
    } 

    //TODO andres:
    //implement the getter methods
    //
    //virtual unsigned int getTlkEnable() {
    // return tlkEnable_;
    //}

    //virtual unsigned int getTlkRefn() {
    // return tlkRefn_;
    //}

    //virtual unsigned int getRecSynchRequest() {
    // return recSynchRequest_;
    //}

    //virtual unsigned intgetRecCheckEnable() {
    // return recCheckEnable_;
    //}

    //virtual unsigned int getRecCheckDataEnable() {
    // return recCheckDataEnable_;
    //}

    //virtual unsigned int getBCN0Delay() {
    // return BCN0Delay_;
    //}

    //...define setters

    //void setTlkEnable( xdata::Integer& tlkEnable ) {
    //    this->tlkEnable_ = tlkEnable;
    // }

    //void setTlkRefn( xdata::Integer&  tlkRefn) {
     //   this->tlkRefn_ = tlkRefn;
    //}

    //void setRecCheckEnable( xdata::Integer& recCheckEnable ) {
     //   this->recCheckEnable_ = recCheckEnable;
    //}

    //void setRecSynchRequest( xdata::Integer& recSynchRequest ) {
      //  this->recSynchRequest_ = recSynchRequest;
    //}

    //void setRecCheckDataEnable( xdata::Integer& recCheckDataEnable ) {
      //  this->recCheckDataEnable_ = recCheckDataEnable;
    //}

    //void setBCN0Delay( xdata::Integer& BCN0Delay  ) {
      //  this->BCN0Delay_ = BCN0Delay;
    //}


};

}}

#endif 
