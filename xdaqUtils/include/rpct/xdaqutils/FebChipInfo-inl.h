/** Filip Thyssen */

#ifndef rpct_xdaqutils_FebChipInfo_inl_h_
#define rpct_xdaqutils_FebChipInfo_inl_h_

#include "rpct/xdaqutils/FebChipInfo.h"

namespace rpct {

inline int FebChipInfo::getChipId() const
{
    return chipid_.value_;
}
inline void FebChipInfo::setChipId(int chipid)
{
    chipid_ = chipid;
}

inline int FebChipInfo::getFebLocationId() const
{
    return flid_.value_;
}
inline void FebChipInfo::setFebLocationId(int flid)
{
    flid_ = flid;
}

inline int FebChipInfo::getBoE() const
{
    return boe_.value_;
}
inline void FebChipInfo::setBoE(int boe)
{
    boe_ = boe;
}

inline int FebChipInfo::getWheel() const
{
    return wheel_.value_;
}
inline void FebChipInfo::setWheel(int wheel)
{
    wheel_ = wheel;
}

inline int FebChipInfo::getLayer() const
{
    return layer_.value_;
}
inline void FebChipInfo::setLayer(int layer)
{
    layer_ = layer;
}

inline int FebChipInfo::getSector() const
{
    return sector_.value_;
}
inline void FebChipInfo::setSector(int sector)
{
    sector_ = sector;
}

inline const std::string & FebChipInfo::getSubsector() const
{
    return subsector_.value_;
}
inline void FebChipInfo::setSubsector(const std::string & subsector)
{
    subsector_ = subsector;
}

inline const std::string & FebChipInfo::getPartition() const
{
    return partition_.value_;
}
inline void FebChipInfo::setPartition(const std::string & partition)
{
    partition_ = partition;
}

inline int FebChipInfo::getChannel() const
{
    return channel_.value_;
}
inline void FebChipInfo::setChannel(int channel)
{
    channel_ = channel;
}

inline int FebChipInfo::getAddress() const
{
    return address_.value_;
}
inline void FebChipInfo::setAddress(int address)
{
    address_ = address;
}

inline int FebChipInfo::getPosition() const
{
    return position_.value_;
}
inline void FebChipInfo::setPosition(int position)
{
    position_ = position;
}

} // namespace rpct

#endif // rpct_xdaqutils_FebChipInfo_inl_h_
