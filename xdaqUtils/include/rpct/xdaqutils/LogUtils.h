/*
 * LogUtils.h
 *
 *  Created on: 2010-04-07
 *      Author: Krzysiek
 */

#ifndef LOGUTILS_H_
#define LOGUTILS_H_

#include "log4cplus/logger.h"
#include "xdaq/Application.h"

namespace rpct {

/** calls setupLogger(namePrefix) for each class put inside the body of this function
 *  setupLogger(namePrefix) creates new logger with the name starting with the namePrefix
 *  in this way all loggers are in the same tree
 */
void setupLoggers(std::string namePrefix);

/**
 * creates additional appenender (DailyRollingFileAppender)
 * default xdaq logger is not rolling, so this appender allow keeping the logs
 * it is enough to all it with the application logger - if the setupLoggers is also called
 * the other logger gets this applender as well, as they are in the same tree
 */
void rpcLogSetup(log4cplus::Logger& logger, xdaq::Application* app);

}
#endif /* LOGUTILS_H_ */
