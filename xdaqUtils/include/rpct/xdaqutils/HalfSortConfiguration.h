#ifndef _RPCTHALFSORTCONFIGURATION_H_
#define _RPCTHALFSORTCONFIGURATION_H_

#include "rpct/xdaqutils/ChipConfiguration.h"
#include "rpct/devices/HalfSortSettings.h"
#include "rpct/devices/hsb.h"
#include "rpct/std/tbutil.h"

#include "xdata/Bag.h"
#include "xdata/Integer.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "xdata/Binary.h"

#include <boost/scoped_ptr.hpp>

namespace rpct {
namespace xdaqutils {

class HalfSortConfiguration : virtual public HalfSortSettings, virtual public ChipConfiguration {
private:
    xdata::Integer configurationId_;
    
    xdata::Binary recChanEna_;
    xdata::Vector<xdata::Integer> recFastClkInv_; 
    xdata::Vector<xdata::Integer> recFastClk90_; 
    xdata::Vector<xdata::Integer> recFastRegAdd_; 
    xdata::Binary recFastDataDelay_;
    xdata::Binary recDataDelay_;

    boost::scoped_ptr<boost::dynamic_bitset<> > recChanEnaPtr_; 
    boost::scoped_ptr<std::vector<int> > recFastClkInvPtr_; 
    boost::scoped_ptr<std::vector<int> > recFastClk90Ptr_; 
    boost::scoped_ptr<std::vector<int> > recFastRegAddPtr_;
    boost::scoped_ptr<boost::dynamic_bitset<> > recFastDataDelayPtr_; 
    boost::scoped_ptr<boost::dynamic_bitset<> > recDataDelayPtr_; 
public:
    void registerFields(xdata::Bag<HalfSortConfiguration>* bag) {
        bag->addField("id", &configurationId_);
        bag->addField("recChanEna", &recChanEna_);
        bag->addField("recFastClkInv", &recFastClkInv_);
        bag->addField("recFastClk90", &recFastClk90_);
        bag->addField("recFastRegAdd", &recFastRegAdd_);
        bag->addField("recFastDataDelay", &recFastDataDelay_);
        bag->addField("recDataDelay", &recDataDelay_);
    }
    
    virtual int getConfigurationId() {
        return configurationId_;
    }    
    void setConfigurationId(xdata::Integer& configurationId) {
        this->configurationId_ = configurationId;
    }
    
    xdata::Binary& getRecChanEnaAsBinary() {
        return recChanEna_;
    }    
    void setRecChanEna(xdata::Binary& recChanEna) {
        this->recChanEna_ = recChanEna;
    }
    
    virtual boost::dynamic_bitset<>& getRecChanEna() {
        if (recChanEnaPtr_.get() == 0) {
            recChanEnaPtr_.reset(createBitsetFromBits(recChanEna_.buffer(), recChanEna_.size() * 8));
        }
        return *recChanEnaPtr_;
    }     
        

    virtual std::vector<int>& getRecFastClkInv() {
        if (recFastClkInvPtr_.get() == 0) {
            recFastClkInvPtr_.reset(new std::vector<int>(16));
            if ((int) recFastClkInv_.size() != 16) {
                throw IllegalArgumentException("HalfSortConfiguration::getRecFastClkInv: invalid size");
            }
            for (std::vector<int>::size_type i = 0; i < recFastClkInv_.size(); i++) {
                (*recFastClkInvPtr_)[i] = recFastClkInv_[i];
            }            
        }
        return *recFastClkInvPtr_;
    }
    
    

    virtual std::vector<int>& getRecFastClk90() {
        if (recFastClk90Ptr_.get() == 0) {
            recFastClk90Ptr_.reset(new std::vector<int>(16));
            if ((int) recFastClk90_.size() != 16) {
                throw IllegalArgumentException("HalfSortConfiguration::getRecFastClk90: invalid size");
            }
            for (std::vector<int>::size_type i = 0; i < recFastClk90_.size(); i++) {
                (*recFastClk90Ptr_)[i] = recFastClk90_[i];
            }            
        }
        return *recFastClk90Ptr_;
    }
    
    

    virtual std::vector<int>& getRecFastRegAdd() {
        if (recFastRegAddPtr_.get() == 0) {
            recFastRegAddPtr_.reset(new std::vector<int>(16));
            if ((int) recFastRegAdd_.size() != 16) {
                throw IllegalArgumentException("HalfSortConfiguration::getRecFastRegAdd: invalid size");
            }
            for (std::vector<int>::size_type i = 0; i < recFastRegAdd_.size(); i++) {
                (*recFastRegAddPtr_)[i] = recFastRegAdd_[i];
            }            
        }
        return *recFastRegAddPtr_;
    }

    xdata::Binary& getRecFastDataDelayAsBinary() {
        return recFastDataDelay_;
    }
    
    void setRecFastDataDelay(xdata::Binary& recFastDataDelay) {
        this->recFastDataDelay_ = recFastDataDelay;
    }
    
    virtual boost::dynamic_bitset<>& getRecFastDataDelay() {
        if (recFastDataDelayPtr_.get() == 0) {
            recFastDataDelayPtr_.reset(createBitsetFromBits(recFastDataDelay_.buffer(), recFastDataDelay_.size() * 8));
        }
        return *recFastDataDelayPtr_;
    }     
    

    xdata::Binary& getRecDataDelayAsBinary() {
        return recDataDelay_;
    }
    
    void setRecDataDelay(xdata::Binary& recDataDelay) {
        this->recDataDelay_ = recDataDelay;
    }
    
    virtual boost::dynamic_bitset<>& getRecDataDelay() {
        if (recDataDelayPtr_.get() == 0) {
            recDataDelayPtr_.reset(createBitsetFromBits(recDataDelay_.buffer(), recDataDelay_.size() * 8));
        }
        return *recDataDelayPtr_;
    }     
    
    virtual rpct::HardwareItemType getDeviceType() {
        return rpct::THsbSortHalf::TYPE;
    }
};

}}

#endif 
