#ifndef _RPCTCHIPCONFIGURATIONASSIGNMENT_H_
#define _RPCTCHIPCONFIGURATIONASSIGNMENT_H_


#include "xdata/Bag.h"
#include "xdata/Integer.h"

namespace rpct {

class ChipConfigurationAssignment { 
private:
    xdata::Integer chipId;
    xdata::Integer configurationId;
    
public:
    void registerFields(xdata::Bag<ChipConfigurationAssignment>* bag) {
        bag->addField("chipId", &chipId);
        bag->addField("configurationId", &configurationId);
    }
    
    int getChipId() {
        return chipId;
    }
    
    void setChipId(xdata::Integer& chipId) {
        this->chipId = chipId;
    }
    
    int getConfigurationId() {
        return configurationId;
    }
    
    void setConfigurationId(xdata::Integer& configurationId) {
        this->configurationId = configurationId;
    }
};

}

#endif /*_RPCTCHIPCONFIGURATIONASSIGNMENT_H_*/
