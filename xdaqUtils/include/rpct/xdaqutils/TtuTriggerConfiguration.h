#ifndef TTUTRIGGERCONFIGURATION_H
#define TTUTRIGGERCONFIGURATION_H

#include "xdata/Bag.h"
#include "xdata/Integer.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "xdata/Binary.h"

#include "rpct/devices/TtuTriggerSettings.h"
#include "rpct/devices/TTUBoard.h"
#include "rpct/std/tbutil.h"
#include "rpct/std/IllegalArgumentException.h"
#include "rpct/xdaqutils/ChipConfiguration.h"

#include <boost/scoped_ptr.hpp>

namespace rpct {
namespace xdaqutils {

class TtuTriggerConfiguration: virtual public TtuTriggerSettings,
		virtual public ChipConfiguration {
private:

	xdata::Integer configurationId_;

	xdata::Binary rec_MuxClk90_; // REC_MUX_CLK90
	xdata::Binary rec_MuxClkInv_; // REC_MUX_CLK_INV
	xdata::Binary rec_MuxRegAdd_; // REC_MUX_REG_ADD
	xdata::Vector<xdata::Integer> rec_MuxDelay_; // REC_MUX_DELAY[]
	xdata::Vector<xdata::Integer> rec_DataDelay_; // REC_DATA_DELAY[]

	xdata::Integer TA_TrgDelay_; // TA_TRG_DELAY
	xdata::Integer TA_TrgTOff_; // TA_TRG_TOFF
	xdata::Integer TA_TrgSel_; // TA_TRG_SELECT
	xdata::Integer TA_TrgCntrGate_; // TA_TRG_CNTR_GATE
	xdata::Integer TrgMajority_; // TRIG_MAJORITY
	xdata::Integer SectorTrigMask_; // SECTOR_TRIG_MASK
	xdata::Integer SectorThreshold_; // SECTOR_THRESHOLD
	xdata::Integer TowerThreshold_; // TOWER_THRESHOLD
	xdata::Integer WheelThreshold_; // WHEEL_THRESHOLD
	xdata::Integer SectorTrgSel_; // SECTOR_TRG_SEL

	xdata::Vector<xdata::Integer> TA_SectorDelay_; // TA_SECTOR_DELAY[]
	xdata::Vector<xdata::Integer> TA_ForceLogic0_; // TA_FORCE_LOGIC_0[]
	xdata::Vector<xdata::Integer> TA_ForceLogic1_; // TA_FORCE_LOGIC_1[]
	xdata::Vector<xdata::Integer> TA_ForceLogic2_; // TA_FORCE_LOGIC_2[]
	xdata::Vector<xdata::Integer> TA_ForceLogic3_; // TA_FORCE_LOGIC_3[]
	xdata::Vector<xdata::Integer> TrgConfig_; // TRIG_CONFIG[]
	xdata::Vector<xdata::Integer> TrgMask_; // TRIG_MASK[]
	xdata::Vector<xdata::Integer> TrgForce_; // TRIG_FORCE[]
	xdata::Vector<xdata::Integer> SectorMajority_; // SECTOR_MAJORITY[]
	xdata::Vector<xdata::Integer> TA_Config2_; // TA_CONFIG2[]
	xdata::Vector<xdata::Integer> ConnectedRbcMask_; // TA_CONFIG2[]


	boost::scoped_ptr<boost::dynamic_bitset<> > recMuxClkInvPtr_;
	boost::scoped_ptr<boost::dynamic_bitset<> > recMuxClk90Ptr_;
	boost::scoped_ptr<boost::dynamic_bitset<> > recMuxRegAddPtr_;
	boost::scoped_ptr<std::vector<int> > recMuxDelayPtr_;
	boost::scoped_ptr<std::vector<int> > recDataDelayPtr_;

	//
	boost::scoped_ptr<std::vector<int> > TA_SectorDelayPtr_;
	boost::scoped_ptr<std::vector<int> > TA_ForceLogic0Ptr_;
	boost::scoped_ptr<std::vector<int> > TA_ForceLogic1Ptr_;
	boost::scoped_ptr<std::vector<int> > TA_ForceLogic2Ptr_;
	boost::scoped_ptr<std::vector<int> > TA_ForceLogic3Ptr_;
	boost::scoped_ptr<std::vector<int> > TrigConfigPtr_;
	boost::scoped_ptr<std::vector<int> > TrigMaskPtr_;
	boost::scoped_ptr<std::vector<int> > TrigForcePtr_;
	boost::scoped_ptr<std::vector<int> > SectorMajorityPtr_;
	boost::scoped_ptr<std::vector<int> > TA_Config2Ptr_;
	boost::scoped_ptr<std::vector<int> > ConnectedRbcMaskPtr_;


public:

	void registerFields(xdata::Bag<TtuTriggerConfiguration>* bag) { // for SOAP

		bag->addField("id", &configurationId_);

		bag->addField("recMuxClk90", &rec_MuxClk90_);
		bag->addField("recMuxClkInv", &rec_MuxClkInv_);
		bag->addField("recMuxRegAdd", &rec_MuxRegAdd_);
		bag->addField("recMuxDelay", &rec_MuxDelay_);
		bag->addField("recDataDelay", &rec_DataDelay_);

		bag->addField("taTrgDelay", &TA_TrgDelay_);
		bag->addField("taTrgToff", &TA_TrgTOff_);
		bag->addField("taTrgSelect", &TA_TrgSel_);
		bag->addField("taTrgCntrGate", &TA_TrgCntrGate_);
		bag->addField("taSectorDelay", &TA_SectorDelay_);
		bag->addField("taForceLogic0", &TA_ForceLogic0_);
		bag->addField("taForceLogic1", &TA_ForceLogic1_);
		bag->addField("taForceLogic2", &TA_ForceLogic2_);
		bag->addField("taForceLogic3", &TA_ForceLogic3_);
		bag->addField("trigConfig", &TrgConfig_);
		bag->addField("trigMask", &TrgMask_);
		bag->addField("trigForce", &TrgForce_);
		bag->addField("trigMajority", &TrgMajority_);
		bag->addField("sectorMajority", &SectorMajority_);
		bag->addField("sectorTrigMask", &SectorTrigMask_);
		bag->addField("sectorThreshold", &SectorThreshold_);
		bag->addField("towerThreshold", &TowerThreshold_);
		bag->addField("wheelThreshold", &WheelThreshold_);
		bag->addField("sectTrgSel", &SectorTrgSel_);
		bag->addField("taConfig2", &TA_Config2_);
		bag->addField("connectedRbcsMask", &ConnectedRbcMask_);

	}

	virtual int getConfigurationId() {
		return configurationId_;
	}

	void setConfigurationId(xdata::Integer& configurationId) {
		this->configurationId_ = configurationId;
	}

	//...define getters, setters

	xdata::Binary& getRecMuxClk90AsBinary() {
		return rec_MuxClk90_;
	}

	void setRecMuxClk90(xdata::Binary & rec_MuxClk90) {
		this->rec_MuxClk90_ = rec_MuxClk90;
	}

	xdata::Binary& getRecMuxClkInvAsBinary() {
		return rec_MuxClkInv_;
	}

	void setRecMuxClkInv(xdata::Binary & rec_MuxClkInv) {
		this->rec_MuxClkInv_ = rec_MuxClkInv;
	}

	xdata::Binary& getRecMuxClkRegAddAsBinary() {
		return rec_MuxRegAdd_;
	}

	void setRecMuxRegAdd(xdata::Binary & rec_MuxRegAdd) {
		this->rec_MuxRegAdd_ = rec_MuxRegAdd;
	}

	//
	virtual boost::dynamic_bitset<>& getRecMuxClk90() {
		if (recMuxClk90Ptr_.get() == 0) {
			recMuxClk90Ptr_.reset(createBitsetFromBits(rec_MuxClk90_.buffer(),
					rec_MuxClk90_.size() * 8));
		}
		return *recMuxClk90Ptr_;
	}

	virtual boost::dynamic_bitset<>& getRecMuxClkInv() {
		if (recMuxClkInvPtr_.get() == 0) {
			recMuxClkInvPtr_.reset(createBitsetFromBits(
					rec_MuxClkInv_.buffer(), rec_MuxClkInv_.size() * 8));
		}
		return *recMuxClkInvPtr_;
	}

	virtual boost::dynamic_bitset<>& getRecMuxRegAdd() {
		if (recMuxRegAddPtr_.get() == 0) {
			recMuxRegAddPtr_.reset(createBitsetFromBits(
					rec_MuxRegAdd_.buffer(), rec_MuxRegAdd_.size() * 8));
		}
		return *recMuxRegAddPtr_;
	}

	//
	virtual std::vector<int>& getRecMuxDelay() {
		if (recMuxDelayPtr_.get() == 0) {
			recMuxDelayPtr_.reset(new std::vector<int>(TTUTrig::TTU_LINK_NUM));
			if ((int) rec_MuxDelay_.size() != TTUTrig::TTU_LINK_NUM) {
				throw IllegalArgumentException(
						"TtuTriggerConfiguration::getRecMuxDelay: invalid size");
			}
			for (std::vector<int>::size_type i = 0; i < rec_MuxDelay_.size(); i++) {
				(*recMuxDelayPtr_)[i] = rec_MuxDelay_[i];
			}
		}
		return *recMuxDelayPtr_;
	}

	void setRecMuxDelay(xdata::Vector<xdata::Integer>& rec_MuxDelay) {
		if ((int) rec_MuxDelay.size() != TTUTrig::TTU_LINK_NUM) {
			throw IllegalArgumentException(
					"TtuTriggerConfiguration::setRecMuxDelay: invalid size");
		}
		this->rec_MuxDelay_ = rec_MuxDelay;
	}

	//vector type return
	virtual std::vector<int>& getRecDataDelay() {
		if (recDataDelayPtr_.get() == 0) {
			recDataDelayPtr_.reset(new std::vector<int>(TTUTrig::TTU_LINK_NUM));
			if ((int) rec_DataDelay_.size() != TTUTrig::TTU_LINK_NUM) {
				throw IllegalArgumentException(
						"TtuTriggerConfiguration::getRecDataDelay: invalid size");
			}
			for (std::vector<int>::size_type i = 0; i < rec_DataDelay_.size(); i++) {
				(*recDataDelayPtr_)[i] = rec_DataDelay_[i];
			}
		}
		return *recDataDelayPtr_;
	}

	void setRecDataDelay(xdata::Vector<xdata::Integer>& rec_DataDelay) {
		if ((int) rec_DataDelay.size() != TTUTrig::TTU_LINK_NUM) {
			throw IllegalArgumentException(
					"TtuTriggerConfiguration::setRecDataDelay: invalid size");
		}
		this->rec_DataDelay_ = rec_DataDelay;
	}

	//....................................................................

	virtual unsigned int getTA_TrgDelay() {
		return TA_TrgDelay_;
	}

	void setTA_TrgDelay(xdata::Integer & TA_TrgDelay) {
		this->TA_TrgDelay_ = TA_TrgDelay;
	}

	virtual unsigned int getTA_TrgTOff() {
		return TA_TrgTOff_;
	}

	void setTA_TrgTOff(xdata::Integer & TA_TrgTOff) {
		this->TA_TrgTOff_ = TA_TrgTOff;
	}

	virtual unsigned int getTA_TrgSel() {
		return TA_TrgSel_;
	}

	void setTA_TrgSel(xdata::Integer & TA_TrgSel) {
		this->TA_TrgSel_ = TA_TrgSel;
	}

	virtual unsigned int getTA_TrgCntrGate() {
		return TA_TrgCntrGate_;
	}

	void setTA_TrgCntrGate(xdata::Integer & TA_TrgCntrGate) {
		this->TA_TrgCntrGate_ = TA_TrgCntrGate;
	}

	virtual unsigned int getTrgMajority() {
		return TrgMajority_;
	}

	void setTrgMajority(xdata::Integer & TrgMajority) {
		this->TrgMajority_ = TrgMajority;
	}

	virtual unsigned int getSectorTrigMask() {
		return SectorTrigMask_;
	}

	void setSectorTrigMask(xdata::Integer & SectorTrigMask) {
		this->SectorTrigMask_ = SectorTrigMask;
	}

	virtual unsigned int getSectorThreshold() {
		return SectorThreshold_;
	}

	void setSectorThreshold(xdata::Integer & SectorThreshold) {
		this->SectorThreshold_ = SectorThreshold;
	}

	virtual unsigned int getTowerThreshold() {
		return TowerThreshold_;
	}

	void setTowerThreshold(xdata::Integer & TowerThreshold) {
		this->TowerThreshold_ = TowerThreshold;
	}

	virtual unsigned int getWheelThreshold() {
		return WheelThreshold_;
	}

	void setWheelThreshold(xdata::Integer & WheelThreshold) {
		this->WheelThreshold_ = WheelThreshold;
	}

	virtual unsigned int getSectorTrgSel() {
		return SectorTrgSel_;
	}

	void setSectorTrgSel(xdata::Integer & SectorTrgSel) {
		this->SectorTrgSel_ = SectorTrgSel;
	}

	//... vector quantities:

	virtual std::vector<int>& getTA_SectorDelay() {
		if (TA_SectorDelayPtr_.get() == 0) {
			TA_SectorDelayPtr_.reset(new std::vector<int>(
					TTUTrig::TA_SECTOR_DELAY_NUMBER));
			if ((int) TA_SectorDelay_.size() != TTUTrig::TA_SECTOR_DELAY_NUMBER) {
				throw IllegalArgumentException(
						"TtuTriggerConfiguration::getRecDataDelay: invalid size");
			}
			for (std::vector<int>::size_type i = 0; i < TA_SectorDelay_.size(); i++) {
				(*TA_SectorDelayPtr_)[i] = TA_SectorDelay_[i];
			}
		}
		return *TA_SectorDelayPtr_;
	}

	void setTA_SectorDelay(xdata::Vector<xdata::Integer>& TA_SectorDelay) {
		if ((int) TA_SectorDelay.size() != TTUTrig::TA_SECTOR_DELAY_NUMBER) {
			throw IllegalArgumentException(
					"TtuTriggerConfiguration::setTA_SectorDelay: invalid size");
		}
		this->TA_SectorDelay_ = TA_SectorDelay;
	}

	virtual std::vector<int>& getTA_ForceLogic0() {
		if (TA_ForceLogic0Ptr_.get() == 0) {
			TA_ForceLogic0Ptr_.reset(new std::vector<int>(
					TTUTrig::TA_FORCE_LOGIC_NUMBER));
			if ((int) TA_ForceLogic0_.size() != TTUTrig::TA_FORCE_LOGIC_NUMBER) {
				throw IllegalArgumentException(
						"TtuTriggerConfiguration::getTA_ForceLogic0: invalid size");
			}
			for (std::vector<int>::size_type i = 0; i < TA_ForceLogic0_.size(); i++) {
				(*TA_ForceLogic0Ptr_)[i] = TA_ForceLogic0_[i];
			}
		}
		return *TA_ForceLogic0Ptr_;
	}

	void setTA_ForceLogic0(xdata::Vector<xdata::Integer>& TA_ForceLogic0) {
		if ((int) TA_ForceLogic0.size() != TTUTrig::TA_FORCE_LOGIC_NUMBER) {
			throw IllegalArgumentException(
					"TtuTriggerConfiguration::setTA_ForceLogic0: invalid size");
		}
		this->TA_ForceLogic0_ = TA_ForceLogic0;
	}

	virtual std::vector<int>& getTA_ForceLogic1() {
		if (TA_ForceLogic1Ptr_.get() == 0) {
			TA_ForceLogic1Ptr_.reset(new std::vector<int>(
					TTUTrig::TA_FORCE_LOGIC_NUMBER));
			if ((int) TA_ForceLogic1_.size() != TTUTrig::TA_FORCE_LOGIC_NUMBER) {
				throw IllegalArgumentException(
						"TtuTriggerConfiguration::getTA_ForceLogic1: invalid size");
			}
			for (std::vector<int>::size_type i = 0; i < TA_ForceLogic1_.size(); i++) {
				(*TA_ForceLogic1Ptr_)[i] = TA_ForceLogic1_[i];
			}
		}
		return *TA_ForceLogic1Ptr_;
	}

	void setTA_ForceLogic1(xdata::Vector<xdata::Integer> & TA_ForceLogic1) {
		if ((int) TA_ForceLogic1.size() != TTUTrig::TA_FORCE_LOGIC_NUMBER) {
			throw IllegalArgumentException(
					"TtuTriggerConfiguration::setTA_ForceLogic1: invalid size");
		}
		this->TA_ForceLogic1_ = TA_ForceLogic1;
	}

	virtual std::vector<int>& getTA_ForceLogic2() {
		if (TA_ForceLogic2Ptr_.get() == 0) {
			TA_ForceLogic2Ptr_.reset(new std::vector<int>(
					TTUTrig::TA_FORCE_LOGIC_NUMBER));
			if ((int) TA_ForceLogic2_.size() != TTUTrig::TA_FORCE_LOGIC_NUMBER) {
				throw IllegalArgumentException(
						"TtuTriggerConfiguration::getTA_ForceLogic2: invalid size");
			}
			for (std::vector<int>::size_type i = 0; i < TA_ForceLogic2_.size(); i++) {
				(*TA_ForceLogic2Ptr_)[i] = TA_ForceLogic2_[i];
			}
		}
		return *TA_ForceLogic2Ptr_;
	}

	void setTA_ForceLogic2(xdata::Vector<xdata::Integer>& TA_ForceLogic2) {
		if ((int) TA_ForceLogic2.size() != TTUTrig::TA_FORCE_LOGIC_NUMBER) {
			throw IllegalArgumentException(
					"TtuTriggerConfiguration::setTA_ForceLogic2: invalid size");
		}
		this->TA_ForceLogic2_ = TA_ForceLogic2;
	}

	virtual std::vector<int>& getTA_ForceLogic3() {
		if (TA_ForceLogic3Ptr_.get() == 0) {
			TA_ForceLogic3Ptr_.reset(new std::vector<int>(
					TTUTrig::TA_FORCE_LOGIC_NUMBER));
			if ((int) TA_ForceLogic3_.size() != TTUTrig::TA_FORCE_LOGIC_NUMBER) {
				throw IllegalArgumentException(
						"TtuTriggerConfiguration::getTA_ForceLogic3: invalid size");
			}
			for (std::vector<int>::size_type i = 0; i < TA_ForceLogic3_.size(); i++) {
				(*TA_ForceLogic3Ptr_)[i] = TA_ForceLogic3_[i];
			}
		}
		return *TA_ForceLogic3Ptr_;
	}

	void setTA_ForceLogic3(xdata::Vector<xdata::Integer>& TA_ForceLogic3) {
		if ((int) TA_ForceLogic3.size() != TTUTrig::TA_FORCE_LOGIC_NUMBER) {
			throw IllegalArgumentException(
					"TtuTriggerConfiguration::setTA_ForceLogic3: invalid size");
		}
		this->TA_ForceLogic3_ = TA_ForceLogic3;
	}

	virtual std::vector<int>& getTrgConfig() {
		if (TrigConfigPtr_.get() == 0) {
			TrigConfigPtr_.reset(new std::vector<int>(
					TTUTrig::TTU_CONFIG_NUMBER)); //<-check if this is correct
			if ((int) TrgConfig_.size() != TTUTrig::TTU_CONFIG_NUMBER) {
				throw IllegalArgumentException(
						"TtuTriggerConfiguration::getTrgConfig: invalid size");
			}
			for (std::vector<int>::size_type i = 0; i < TrgConfig_.size(); i++) {
				(*TrigConfigPtr_)[i] = TrgConfig_[i];
			}
		}
		return *TrigConfigPtr_;
	}

	void setTrgConfig(xdata::Vector<xdata::Integer>& TrgConfig) {
		if ((int) TrgConfig.size() != TTUTrig::TTU_CONFIG_NUMBER) {
			throw IllegalArgumentException(
					"TtuTriggerConfiguration::setTrgConfig: invalid size");
		}
		this->TrgConfig_ = TrgConfig;
	}

	virtual std::vector<int>& getTrgMask() {
		if (TrigMaskPtr_.get() == 0) {
			TrigMaskPtr_.reset(new std::vector<int>(TTUTrig::TTU_MASK_NUMBER));
			if ((int) TrgMask_.size() != TTUTrig::TTU_MASK_NUMBER) {
				throw IllegalArgumentException(
						"TtuTriggerConfiguration::getTrgMask: invalid size");
			}
			for (std::vector<int>::size_type i = 0; i < TrgMask_.size(); i++) {
				(*TrigMaskPtr_)[i] = TrgMask_[i];
			}
		}
		return *TrigMaskPtr_;
	}

	void setTrgMask(xdata::Vector<xdata::Integer>& TrgMask) {
		if ((int) TrgMask.size() != TTUTrig::TTU_MASK_NUMBER) {
			throw IllegalArgumentException(
					"TtuTriggerConfiguration::setTrgMask: invalid size");
		}
		this->TrgMask_ = TrgMask;
	}

	virtual std::vector<int>& getTrgForce() {
		if (TrigForcePtr_.get() == 0) {
			TrigForcePtr_.reset(new std::vector<int>(TTUTrig::TTU_FORCE_NUMBER));
			if ((int) TrgForce_.size() != TTUTrig::TTU_FORCE_NUMBER) {
				throw IllegalArgumentException(
						"TtuTriggerConfiguration::getTrgForce: invalid size");
			}
			for (std::vector<int>::size_type i = 0; i < TrgForce_.size(); i++) {
				(*TrigForcePtr_)[i] = TrgForce_[i];
			}
		}
		return *TrigForcePtr_;
	}

	void setTrgForce(xdata::Vector<xdata::Integer>& TrgForce) {
		if ((int) TrgForce.size() != TTUTrig::TTU_FORCE_NUMBER) {
			throw IllegalArgumentException(
					"TtuTriggerConfiguration::setTrgForce: invalid size");
		}
		this->TrgForce_ = TrgForce;
	}

	virtual std::vector<int>& getSectorMajority() {
		if (SectorMajorityPtr_.get() == 0) {
			SectorMajorityPtr_.reset(new std::vector<int>(
					TTUTrig::TA_SECT_MAJ_NUMBER));
			if ((int) SectorMajority_.size() != TTUTrig::TA_SECT_MAJ_NUMBER) {
				throw IllegalArgumentException(
						"TtuTriggerConfiguration::getSectorMajority: invalid size");
			}
			for (std::vector<int>::size_type i = 0; i < SectorMajority_.size(); i++) {
				(*SectorMajorityPtr_)[i] = SectorMajority_[i];
			}
		}
		return *SectorMajorityPtr_;
	}

	void setSectorMajority(xdata::Vector<xdata::Integer>& TA_SectorMajority) {
		if ((int) TA_SectorMajority.size() != TTUTrig::TA_SECT_MAJ_NUMBER) {
			throw IllegalArgumentException(
					"TtuTriggerConfiguration::setSectorMajority: invalid size");
		}
		this->SectorMajority_ = TA_SectorMajority;
	}

	virtual std::vector<int>& getTA_Config2() {
		if (TA_Config2Ptr_.get() == 0) {
			TA_Config2Ptr_.reset(new std::vector<int>(
					TTUTrig::TA_CONFIG2_NUMBER));
			if ((int) TA_Config2_.size() != TTUTrig::TA_CONFIG2_NUMBER) {
				throw IllegalArgumentException(
						"TtuTriggerConfiguration::getTA_Config2: invalid size");
			}
			for (std::vector<int>::size_type i = 0; i < TA_Config2_.size(); i++) {
				(*TA_Config2Ptr_)[i] = TA_Config2_[i];
			}
		}
		return *TA_Config2Ptr_;
	}

	void setTA_Config2(xdata::Vector<xdata::Integer>& TA_Config2) {
		if ((int) TA_Config2.size() != TTUTrig::TA_CONFIG2_NUMBER) {
			throw IllegalArgumentException(
					"TtuTriggerConfiguration::setTA_Config2: invalid size");
		}
		this->TA_Config2_ = TA_Config2;
	}

        virtual std::vector<int>& getConnectedRbcMask() {
		if (ConnectedRbcMaskPtr_.get() == 0) {
			ConnectedRbcMaskPtr_.reset(new std::vector<int>(
					TTUTrig::TTU_MASK_NUMBER));
			if ((int) ConnectedRbcMask_.size() != TTUTrig::TTU_MASK_NUMBER) {
				throw IllegalArgumentException(
						"TtuTriggerConfiguration::getConnectedRbcMask: invalid size");
			}
			for (std::vector<int>::size_type i = 0; i < ConnectedRbcMask_.size(); i++) {
				(*ConnectedRbcMaskPtr_)[i] = ConnectedRbcMask_[i];
			}
		}
		return *ConnectedRbcMaskPtr_;
	}

	void setConnectedRbcMask(xdata::Vector<xdata::Integer>& ConnectedRbcMask) {
		if ((int) ConnectedRbcMask.size() != TTUTrig::TTU_MASK_NUMBER) {
			throw IllegalArgumentException(
					"TtuTriggerConfiguration::setConnectedRbcMask: invalid size");
		}
		this->ConnectedRbcMask_ = ConnectedRbcMask;
	}


	//...

	virtual rpct::HardwareItemType getDeviceType() {
		return rpct::TTUTrig::TYPE;
	}

};

}
}

#endif // TTUTRIGGERCONFIGURATION_H
