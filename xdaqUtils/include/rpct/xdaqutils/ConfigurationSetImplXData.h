#ifndef _RPCTCONFIGURATIONSETIMPLXDATA_H_
#define _RPCTCONFIGURATIONSETIMPLXDATA_H_


#include "xdata/Bag.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "rpct/ii/ConfigurationSet.h"
#include "rpct/xdaqutils/SynCoderConfiguration.h"
#include "rpct/xdaqutils/OptoConfiguration.h"
#include "rpct/xdaqutils/PacConfiguration.h"
#include "rpct/xdaqutils/TbGbSortConfiguration.h"
#include "rpct/xdaqutils/RmbConfiguration.h"
#include "rpct/xdaqutils/TcSortConfiguration.h"
#include "rpct/xdaqutils/HalfSortConfiguration.h"
#include "rpct/xdaqutils/RbcDeviceConfiguration.h"
#include "rpct/xdaqutils/TtuTriggerConfiguration.h"
#include "rpct/xdaqutils/TtuBackplaneConfiguration.h"
#include "rpct/xdaqutils/FebChipConfiguration.h"
#include "rpct/xdaqutils/ChipConfigurationAssignment.h"
#include "rpct/xdaqutils/SynCoderConfInFlash.h"
#include <map>
#include <boost/shared_ptr.hpp>

namespace rpct {
namespace xdaqutils {

class ConfigurationSetImplXData : public virtual ConfigurationSet {
public:
    typedef xdata::Vector<xdata::Bag<SynCoderConfiguration> > SynCoderConfigurationVector;
    typedef xdata::Vector<xdata::Bag<OptoConfiguration> > OptoConfigurationVector;
    typedef xdata::Vector<xdata::Bag<PacConfiguration> > PacConfigurationVector;
    typedef xdata::Vector<xdata::Bag<TbGbSortConfiguration> > TbGbSortConfigurationVector;
    typedef xdata::Vector<xdata::Bag<RmbConfiguration> > RmbConfigurationVector;
    typedef xdata::Vector<xdata::Bag<TcSortConfiguration> > TcSortConfigurationVector;
    typedef xdata::Vector<xdata::Bag<HalfSortConfiguration> > HalfSortConfigurationVector;
    typedef xdata::Vector<xdata::Bag<RbcDeviceConfiguration> > RbcDeviceConfigurationVector;
    typedef xdata::Vector<xdata::Bag<TtuTriggerConfiguration> > TtuTriggerConfigurationVector;
    typedef xdata::Vector<xdata::Bag<TtuBackplaneConfiguration> > TtuBackplaneConfigurationVector;
    typedef xdata::Vector<xdata::Bag<FebChipConfiguration> > FebChipConfigurationVector;
    typedef xdata::Vector<xdata::Bag<ChipConfigurationAssignment> > ChipConfigurationAssignmentVector;

    typedef xdata::Vector<xdata::Bag<SynCoderConfInFlash> > SynCoderConfInFlashVector;

    typedef std::map<int, ChipConfiguration*> ChipConfigurationsByConfId;
    typedef std::map<int, ChipConfiguration*> ChipConfigurationsByChipId;

    typedef std::map<int, ISynCoderConfInFlash*> SynCoderConfInFlashByChipId;
private:
    xdata::Integer localConfigKeyId_;
    xdata::Integer timestamp_;
    SynCoderConfigurationVector synCoderConfigurations_;
    OptoConfigurationVector optoConfigurations_;
    PacConfigurationVector pacConfigurations_;
    TbGbSortConfigurationVector tbGbSortConfigurations_;
    RmbConfigurationVector rmbConfigurations_;
    TcSortConfigurationVector tcSortConfigurations_;
    HalfSortConfigurationVector halfSortConfigurations_;
    RbcDeviceConfigurationVector rbcDeviceConfigurations_;
    TtuTriggerConfigurationVector ttuTriggerConfigurations_;
    TtuBackplaneConfigurationVector ttuBackplaneConfigurations_;
    FebChipConfigurationVector febChipConfigurations_;

    ChipConfigurationAssignmentVector chipConfigurationAssignments_;

    SynCoderConfInFlashVector synCoderConfInFlashes_;

    ChipConfigurationsByConfId chipConfigurationsByConfId_;
    ChipConfigurationsByChipId chipConfigurationsByChipId_;

    SynCoderConfInFlashByChipId synCoderConfInFlashByChipId_;
public:
    void registerFields(xdata::Bag<ConfigurationSetImplXData>* bag) {
        bag->addField("localConfigKeyId", &localConfigKeyId_);
        bag->addField("timestamp", &timestamp_);
        bag->addField("synCoderConfigurations", &synCoderConfigurations_);
        bag->addField("optoConfigurations", &optoConfigurations_);
        bag->addField("pacConfigurations", &pacConfigurations_);
        bag->addField("tbGbSortConfigurations", &tbGbSortConfigurations_);
        bag->addField("rmbConfigurations", &rmbConfigurations_);
        bag->addField("tcSortConfigurations", &tcSortConfigurations_);
        bag->addField("halfSortConfigurations", &halfSortConfigurations_);
        bag->addField("rbcConfigurations", &rbcDeviceConfigurations_);
        bag->addField("ttuTrigConfigurations", &ttuTriggerConfigurations_);
        bag->addField("ttuFinalConfigurations", &ttuBackplaneConfigurations_);
        bag->addField("febChipConfigurations", &febChipConfigurations_);
        bag->addField("chipConfigurationAssignments", &chipConfigurationAssignments_);
        bag->addField("synCoderConfInFlashes", &synCoderConfInFlashes_);
    }

    int getLocalConfigKeyId() {
        return (int) localConfigKeyId_;
    }

    int getTimestamp() {
        return (int) timestamp_;
    }

    virtual unsigned int getId() {
        return (getLocalConfigKeyId() & 0xffff) | ((getTimestamp() << 16) & 0xffff0000);
    }

    SynCoderConfigurationVector& getSynCoderConfigurations() {
        return synCoderConfigurations_;
    }

    void setSynCoderConfigurations(SynCoderConfigurationVector& synCoderConfigurations) {
        this->synCoderConfigurations_ = synCoderConfigurations;
    }

    OptoConfigurationVector& getOptoConfigurations() {
        return optoConfigurations_;
    }

    void setOptoConfigurations(OptoConfigurationVector& optoConfigurations) {
        this->optoConfigurations_ = optoConfigurations;
    }

    PacConfigurationVector& getPacConfigurations() {
        return pacConfigurations_;
    }

    void setPacConfigurations(PacConfigurationVector& pacConfigurations) {
        this->pacConfigurations_ = pacConfigurations;
    }

    TbGbSortConfigurationVector& getTbGbSortConfigurations() {
        return tbGbSortConfigurations_;
    }

    void setTbGbSortConfigurations(TbGbSortConfigurationVector& tbGbSortConfigurations) {
        this->tbGbSortConfigurations_ = tbGbSortConfigurations;
    }

    RmbConfigurationVector& getRmbConfigurations() {
        return rmbConfigurations_;
    }

    void setRmbConfigurations(RmbConfigurationVector& rmbConfigurations) {
        this->rmbConfigurations_ = rmbConfigurations;
    }

    TcSortConfigurationVector& getTcSortConfigurations() {
        return tcSortConfigurations_;
    }

    void setTcSortConfigurations(TcSortConfigurationVector& tcSortConfigurations) {
        this->tcSortConfigurations_ = tcSortConfigurations;
    }

    HalfSortConfigurationVector& getHalfSortConfigurations() {
        return halfSortConfigurations_;
    }

    void setHalfSortConfigurations(HalfSortConfigurationVector& halfSortConfigurations) {
        this->halfSortConfigurations_ = halfSortConfigurations;
    }

    RbcDeviceConfigurationVector& getRbcDeviceConfigurations() {
        return rbcDeviceConfigurations_;
    }

    void setRbcDeviceConfigurations(RbcDeviceConfigurationVector& rbcDeviceConfigurations) {
        this->rbcDeviceConfigurations_ = rbcDeviceConfigurations;
    }

    TtuTriggerConfigurationVector& getTtuTriggerConfigurations() {
        return ttuTriggerConfigurations_;
    }

    void setTtuTriggerConfigurations(TtuTriggerConfigurationVector& ttuTriggerConfigurations) {
        this->ttuTriggerConfigurations_ = ttuTriggerConfigurations;
    }

    TtuBackplaneConfigurationVector& getTtuBackplaneConfigurations() {
        return ttuBackplaneConfigurations_;
    }

    void setTtuBackplaneConfigurations(TtuBackplaneConfigurationVector& ttuBackplaneConfigurations) {
        this->ttuBackplaneConfigurations_ = ttuBackplaneConfigurations;
    }

    FebChipConfigurationVector& getFebChipConfigurations() {
        return febChipConfigurations_;
    }

    void setFebChipConfigurations(FebChipConfigurationVector& febChipConfigurations) {
        this->febChipConfigurations_ = febChipConfigurations;
    }

    ChipConfigurationAssignmentVector& getChipConfigurationAssignments() {
        return chipConfigurationAssignments_;
    }

    void setChipConfigurationAssignments(ChipConfigurationAssignmentVector& chipConfigurationAssignments) {
        this->chipConfigurationAssignments_ = chipConfigurationAssignments;
    }


	SynCoderConfInFlashVector& getSynCoderConfInFlashes() {
        return synCoderConfInFlashes_;
    }

    void setSynCoderConfInFlashes(SynCoderConfInFlashVector& synCoderConfInFlashes) {
        this->synCoderConfInFlashes_ = synCoderConfInFlashes;
    }

    ChipConfigurationsByConfId& getChipConfigurationsByConfId();
    ChipConfigurationsByChipId& getChipConfigurationsByChipId();

    ChipConfiguration* getChipConfigurationForChip(int id);
    virtual DeviceSettings* getDeviceSettings(IDevice& device);

    virtual DeviceSettings* getDeviceSettings(int id);

    //returns 0 if the settings in flash are the same as requested from the DB (i.e. in this ConfigurationSet)
    virtual ISynCoderConfInFlash* getSynCoderConfInFlash(int synCoderId);
};

typedef xdata::Bag<ConfigurationSetImplXData> ConfigurationSetBag;
typedef boost::shared_ptr<ConfigurationSetBag> ConfigurationSetBagPtr;

}}

#endif
