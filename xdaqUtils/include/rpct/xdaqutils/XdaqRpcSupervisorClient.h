#ifndef _RPCTXDAQRPCSUPERVISOR_H_
#define _RPCTXDAQRPCSUPERVISOR_H_

#include "xdaq/Application.h" 

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"

#include "xdata/Vector.h"
#include "xdata/String.h"
#include "xdata/Integer.h"

#include "rpct/xdaqutils/XdaqSoapAccess.h"
#include "rpct/xdaqutils/FebWarningInfo.h"
#include "rpct/xdaqutils/FebStateInfo.h"

#include <list>
#include <map>

namespace rpct {
namespace xdaqutils {
 
class XdaqRpcSupervisorClient { 
protected:
    xdaq::Application* application;
    XdaqSoapAccess soapAccess;
    static const char* RPCT_DB_SERVICE_NS;
    static const char* RPCT_DB_SERVICE_PREFIX;
    
public:
    XdaqRpcSupervisorClient(xdaq::Application* app) 
    : application(app), soapAccess(app)  {}
        
    typedef xdata::Vector<xdata::Bag<FebWarningInfo> > FebWarningInfoVector;   
    typedef xdata::Vector<xdata::Bag<FebStateInfo> > FebStateOverTimeVector;   
};
        
}}


#endif 
