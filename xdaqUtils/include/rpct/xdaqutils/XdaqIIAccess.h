#ifndef _XDAQLBOXACCESS_H_
#define _XDAQLBOXACCESS_H_

#include "rpct/devices/System.h"
#include "rpct/xdaqutils/XdaqSoapAccess.h"
#include "rpct/ii/ICrate.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/exception/Exception.h"
#include "xdaq/NamespaceURI.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"
#include <log4cplus/logger.h>


namespace rpct {
namespace xdaqutils {

class XdaqIIAccess {
public:
private:
    static log4cplus::Logger logger;
    System& system_;
    //typedef std::vector<ICrate*> Crates;
    //Crates crates;
    //typedef std::map<int, IDevice*> DevicesMap;
    //DevicesMap devicesMap;
    rpct::xdaqutils::XdaqSoapAccess xdaqSoapAccess;
    //void mapHardwareItem(IHardwareItem*);
    //void mapDevice(IDevice* device);
    //void init();
public:
    static void setupLogger(std::string namePrefix);
    static std::string GET_CRATES_INFO_NAME;
    static std::string GET_BOARDS_INFO_NAME;
    static std::string GET_DEVICES_INFO_NAME;
    static std::string GET_DEVICEITEMS_INFO_NAME;
    static std::string WRITE_II_NAME;
    static std::string READ_II_NAME;
    XdaqIIAccess(xdaq::Application* app, System& system);
    xoap::MessageReference getCratesInfo(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference getBoardsInfo(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference getDevicesInfo(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference getDeviceItemsInfo(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference readII(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference writeII(xoap::MessageReference msg) throw (xoap::exception::Exception);
};

}}


#endif /*_XDAQLBOXACCESS_H_*/
