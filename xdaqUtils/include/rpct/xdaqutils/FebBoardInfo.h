/** Filip Thyssen */

#ifndef rpct_xdaqutils_FebBoardInfo_h_
#define rpct_xdaqutils_FebBoardInfo_h_

#include <string>

#include "xdata/Integer.h"
#include "xdata/Boolean.h"

#include "rpct/xdaqutils/BoardInfo.h"

namespace xdata {
class AbstractBag;
}

namespace rpct {

class FebBoardInfo : public BoardInfo
{
public:
    FebBoardInfo(int id = -1
                 , const std::string & name = ""
                 , const std::string & label = ""
                 , const std::string & type = "FEBBOARD"
                 , int i2cAddress = -1
                 , bool disabled = 0
                 , ChipInfoVector chipInfos = ChipInfoVector()
                 , int cb_id = -1
                 , int cb_channel = 0
                 , int feblocation_id = -1
                 , bool dt_controlled = 0);

    void registerFields(xdata::AbstractBag * bag);

    int getControlBoardId() const;
    void setControlBoardId(int id);

    int getControlBoardChannel() const;
    void setControlBoardChannel(int channel);

    int getFebLocationId() const;
    void setFebLocationId(int id);

    bool isDTControlled() const;
    void setDTControlled(bool dt_controlled);

    int getFebType() const;

    int getI2cAddress() const;
    void setI2cAddress(int position);

    const ChipInfoVector & getChipInfos() const;
    ChipInfoVector & getChipInfos();
    void clearChipInfos();
    void addChipInfo(const ChipInfo & chipinfo);

protected:
    xdata::Integer cb_id_;
    xdata::Integer cb_channel_;
    xdata::Integer feblocation_id_;
    xdata::Boolean dt_controlled_;
};

} // namespace rpct

#include "rpct/xdaqutils/FebBoardInfo-inl.h"

#endif // rpct_xdaqutils_FebBoardInfo_h_
