#ifndef _RPCTFEBWARNINGINFO_H_
#define _RPCTFEBWARNINGINFO_H_


#include "xdata/Bag.h"
#include "xdata/Integer.h"
#include "xdata/Float.h"
#include "xdata/String.h"
#include "xdata/UnsignedLong.h"
namespace rpct {

class FebWarningInfo {
private:
    xdata::String febLocalEtaPartition;
    xdata::String chamberLocationName;
    xdata::Integer ccuAddress;
    xdata::Integer channel;
    xdata::Integer address;
	xdata::Float tempValue;
	xdata::Integer tempMin;
	xdata::Integer tempMax;
	xdata::Float th1Value;
	xdata::Integer th1Min;
	xdata::Integer th1Max;
	xdata::Float th2Value;
	xdata::Integer th2Min;
	xdata::Integer th2Max;
	xdata::Float th3Value;
	xdata::Integer th3Min;
	xdata::Integer th3Max;
	xdata::Float th4Value;
	xdata::Integer th4Min;
	xdata::Integer th4Max;
	xdata::Float vmon1Value;
	xdata::Integer vmon1Min;
	xdata::Integer vmon1Max;
	xdata::Float vmon2Value;
	xdata::Integer vmon2Min;
	xdata::Integer vmon2Max;
	xdata::Float vmon3Value;
	xdata::Integer vmon3Min;
	xdata::Integer vmon3Max;
	xdata::Float vmon4Value;
	xdata::Integer vmon4Min;
	xdata::Integer vmon4Max;
	xdata::String executionTime;
	xdata::UnsignedLong executionUnixTime;
	
    
public:
    void registerFields(xdata::Bag<FebWarningInfo>* bag) {
        bag->addField("febLocalEtaPartition", &febLocalEtaPartition);
        bag->addField("chamberLocationName", &chamberLocationName);
        bag->addField("ccuAddress", &ccuAddress);
        bag->addField("channel", &channel);
        bag->addField("address", &address);
		bag->addField("tempValue", &tempValue);
		bag->addField("tempMin", &tempMin);
		bag->addField("tempMax", &tempMax);
		bag->addField("th1Value", &th1Value);
		bag->addField("th1Min", &th1Min);
		bag->addField("th1Max", &th1Max);
		bag->addField("th2Value", &th2Value);
		bag->addField("th2Min", &th2Min);
		bag->addField("th2Max", &th2Max);
		bag->addField("th3Value", &th3Value);
		bag->addField("th3Min", &th3Min);
		bag->addField("th3Max", &th3Max);
		bag->addField("th4Value", &th4Value);
		bag->addField("th4Min", &th4Min);
		bag->addField("th4Max", &th4Max);
		bag->addField("vmon1Value", &vmon1Value);
		bag->addField("vmon1Max", &vmon1Max);
		bag->addField("vmon1Min", &vmon1Min);
		bag->addField("vmon2Value", &vmon2Value);
		bag->addField("vmon2Min", &vmon2Min);
		bag->addField("vmon2Max", &vmon2Max);
		bag->addField("vmon3Value", &vmon3Value);
		bag->addField("vmon3Max", &vmon3Max);
		bag->addField("vmon3Min", &vmon3Min);
		bag->addField("vmon4Value", &vmon4Value);
		bag->addField("vmon4Min", &vmon4Min);
		bag->addField("vmon4Max", &vmon4Max);
		bag->addField("executionTime", &executionTime);
		bag->addField("executionUnixTime", &executionUnixTime);
		
    }
    
    xdata::String& getFebLocalEtaPartition() {
        return febLocalEtaPartition;
    }
    
    void setFebLocalEtaPartition(xdata::String febLocalEtaPartition) {
        this->febLocalEtaPartition = febLocalEtaPartition;
    }
    
    xdata::String& getChamberLocationName() {
        return chamberLocationName;
    }
    
    void setChamberLocationName(xdata::String chamberLocationName) {
        this->chamberLocationName = chamberLocationName;
    }
    
     xdata::Integer& getCcuAddress() {
        return ccuAddress;
    }
    
    void setCcuAddress(xdata::Integer ccuAddress) {
        this->ccuAddress = ccuAddress;
    }
    
    xdata::Integer& getChannel() {
        return channel;
    }
    
    void setChannel(xdata::Integer channel) {
        this->channel = channel;
    }
    
    xdata::Integer& getAddress() {
        return address;
    }
    
    void setAddress(xdata::Integer address) {
        this->address = address;
    }
     xdata::Float& getTempValue() {
        return tempValue;
    }
    
    void setTempValue(xdata::Float tempValue) {
        this->tempValue = tempValue;
    }
     xdata::Float& getTh1Value() {
        return th1Value;
    }
    
    void setTh1Value(xdata::Float th1Value) {
        this->th1Value = th1Value;
    }
     xdata::Float& getTh2Value() {
        return th2Value;
    }
    
    void setTh2Value(xdata::Float th2Value) {
        this->th2Value = th2Value;
    }
     xdata::Float& getTh3Value() {
        return th3Value;
    }
    
    void setTh3Value(xdata::Float th3Value) {
        this->th3Value = th3Value;
    }
     xdata::Float& getTh4Value() {
        return th4Value;
    }
    
    void setTh4Value(xdata::Float th4Value) {
        this->th4Value = th4Value;
    }
     xdata::Float& getVmon1Value() {
        return vmon1Value;
    }
    
    void setVmon1Value(xdata::Float vmon1Value) {
        this->vmon1Value = vmon1Value;
    }
     xdata::Float& getVmon2Value() {
        return vmon2Value;
    }
    
    void setVmon2Value(xdata::Float vmon2Value) {
        this->vmon2Value = vmon2Value;
    }
     xdata::Float& getVmon3Value() {
        return vmon3Value;
    }
    
    void setVmon3Value(xdata::Float vmon3Value) {
        this->vmon3Value = vmon3Value;
    }
     xdata::Float& getVmon4Value() {
        return vmon4Value;
    }
    
    void setVmon4Value(xdata::Float vmon4Value) {
        this->vmon4Value = vmon4Value;
    }
     xdata::Integer& getTempMax() {
        return tempMax;
    }
    
    void setTempMax(xdata::Integer tempMax) {
        this->tempMax = tempMax;
    }
     xdata::Integer& getTh1Max() {
        return th1Max;
    }
    
    void setTh1Max(xdata::Integer th1Max) {
        this->th1Max = th1Max;
    }
     xdata::Integer& getTh2Max() {
        return th2Max;
    }
    
    void setTh2Max(xdata::Integer th2Max) {
        this->th2Max = th2Max;
    }
     xdata::Integer& getTh3Max() {
        return th3Max;
    }
    
    void setTh3Max(xdata::Integer th3Max) {
        this->th3Max = th3Max;
    }
     xdata::Integer& getTh4Max() {
        return th4Max;
    }
    
    void setTh4Max(xdata::Integer th4Max) {
        this->th4Max = th4Max;
    }
     xdata::Integer& getVmon1Max() {
        return vmon1Max;
    }
    
    void setVmon1Max(xdata::Integer vmon1Max) {
        this->vmon1Max = vmon1Max;
    }
     xdata::Integer& getVmon2Max() {
        return vmon2Max;
    }
    
    void setVmon2Max(xdata::Integer vmon2Max) {
        this->vmon2Max = vmon2Max;
    }
     xdata::Integer& getVmon3Max() {
        return vmon3Max;
    }
    
    void setVmon3Max(xdata::Integer vmon3Max) {
        this->vmon3Max = vmon3Max;
    }
     xdata::Integer& getVmon4Max() {
        return vmon4Max;
    }
    
    void setVmon4Max(xdata::Integer vmon4Max) {
        this->vmon4Max = vmon4Max;
    }
     xdata::Integer& getTempMin() {
        return tempMin;
    }
    
    void setTempMin(xdata::Integer tempMin) {
        this->tempMin = tempMin;
    }
     xdata::Integer& getTh1Min() {
        return th1Min;
    }
    
    void setTh1Min(xdata::Integer th1Min) {
        this->th1Min = th1Min;
    }
     xdata::Integer& getTh2Min() {
        return th2Min;
    }
    
    void setTh2Min(xdata::Integer th2Min) {
        this->th2Min = th2Min;
    }
     xdata::Integer& getTh3Min() {
        return th3Min;
    }
    
    void setTh3Min(xdata::Integer th3Min) {
        this->th3Min = th3Min;
    }
     xdata::Integer& getTh4Min() {
        return th4Min;
    }
    
    void setTh4Min(xdata::Integer th4Min) {
        this->th4Min = th4Min;
    }
     xdata::Integer& getVmon1Min() {
        return vmon1Min;
    }
    
    void setVmon1Min(xdata::Integer vmon1Min) {
        this->vmon1Min = vmon1Min;
    }
     xdata::Integer& getVmon2Min() {
        return vmon2Min;
    }
    
    void setVmon2Min(xdata::Integer vmon2Min) {
        this->vmon2Min = vmon2Min;
    }
     xdata::Integer& getVmon3Min() {
        return vmon3Min;
    }
    
    void setVmon3Min(xdata::Integer vmon3Min) {
        this->vmon3Min = vmon3Min;
    }
     xdata::Integer& getVmon4Min() {
        return vmon4Min;
    }
    
    void setVmon4Min(xdata::Integer vmon4Min) {
        this->vmon4Min = vmon4Min;
    }
    
    xdata::String& getExecutionTime() {
        return executionTime;
    }
    
    void setExecutionTime(xdata::String executionTime) {
        this->executionTime = executionTime;
    }
    
    xdata::UnsignedLong& getExecutionUnixTime() {
        return executionUnixTime;
    }
    
    void setExecutionUnixTime(xdata::UnsignedLong executionUnixTime) {
        this->executionUnixTime = executionUnixTime;
    }
};

}

#endif /*_RPCTFEBWARNINGINFO_H_*/
