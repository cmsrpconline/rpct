#ifndef _RPCTXDAQSOAPACCESS_H_
#define _RPCTXDAQSOAPACCESS_H_

#include "xdaq/Application.h"
/*
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/exception/Exception.h"
#include "xdaq/NamespaceURI.h"*/


#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"

#include "xdata/soap/NamespaceURI.h"
#include "xdata/Serializable.h"
#include "xdata/rpctsoap/Serializer.h"


#include <list>
#include <map>



namespace rpct {
namespace xdaqutils {

class XdaqSoapAccess {
protected:
    xdaq::Application* application;
    xdata::rpctsoap::Serializer serializer;
public:
    XdaqSoapAccess(xdaq::Application* app) : application(app) {}

    typedef std::map<std::string, xdata::Serializable*> TSOAPParamMap;
    void parseSOAPRequest(xoap::MessageReference msg, std::string functionName,
            std::string prefix, std::string uri, TSOAPParamMap& paramMap);

    xoap::MessageReference sendSOAPResponse(std::string functionName, std::string prefix, std::string uri,
            xdata::Serializable& value);

    typedef std::list<std::pair<std::string, xdata::Serializable*> > TSOAPParamList;
    xoap::MessageReference sendSOAPRequest(std::string functionName,
            std::string prefix,  std::string uri, TSOAPParamList& paramList,
            std::string application, int instance, bool external);


    void parseSOAPResponse(xoap::MessageReference msg, std::string functionName,
            std::string prefix, std::string uri, xdata::Serializable& value);

    void parseSOAPResponse(xoap::MessageReference msg, std::string functionName,
            std::string prefix, std::string uri);
};

}}


#endif /*_XDAQLBOXACCESS_H_*/
