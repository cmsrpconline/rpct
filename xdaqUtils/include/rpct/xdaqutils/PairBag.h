#ifndef PAIRBAG_
#define PAIRBAG_

#include "xdata/Bag.h"

namespace rpct {
namespace xdaqutils {
    
    
template <class T1, class T2>
class Pair {
private:
    T1 first;
    T2 second;
public:
    void registerFields(xdata::Bag< Pair<T1,T2> >* bag) {
        bag->addField("first", &first);
        bag->addField("second", &second);
    }    
    
    template <class _T1, class _T2>
    void init(const std::pair<_T1,_T2>& pair) {
        first = pair.first;
        second = pair.second;
    }    
    
    template <class _T1, class _T2>
    void init(_T1& first, _T2& second) {
        this.first = first;
        this.second = second;
    }
    
    T1& getFirst() {
        return first;
    }    
    
    T2& getSecond() {
        return second;
    }
};
//typedef xdata::Bag<Pair> PairBag;

    
}}

#endif /*PAIRBAG_*/
