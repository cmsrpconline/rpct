/** Filip Thyssen */

#ifndef rpct_xdaqutils_DTLineFebInfo_h
#define rpct_xdaqutils_DTLineFebInfo_h

#include "xdata/Integer.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Bag.h"

namespace xdata {
class AbstractBag;
}

namespace rpct {
namespace xdaqutils {

class DTLineFebChipInfo
{
public:
    DTLineFebChipInfo(int _id = -1
                      , int _position = -1
                      , bool _disabled = false);

    void registerFields(xdata::AbstractBag * _bag);

    int getId() const;
    int getPosition() const;
    bool getDisabled() const;

    void setId(int _id);
    void setPosition(int _position);
    void setDisabled(int _disabled);

protected:
    xdata::Integer id_;
    xdata::Integer position_;
    xdata::Boolean disabled_;
};

class DTLineFebInfo
{
public:
    DTLineFebInfo(int _id
                  , std::string const & _name
                  , int _i2c_address
                  , bool _disabled
                  , std::string const & _location
                  , std::string const & _partition
                  // , int _feb_location_id
                  , std::vector<DTLineFebChipInfo> const & _feb_chip_infos);

    DTLineFebInfo(int _id = -1
                  , std::string const & _name = std::string("")
                  , int _i2c_address = -1
                  , bool _disabled = false
                  , std::string const & _location = std::string("")
                  , std::string const & _partition = std::string("")
                  // , int _feb_location_id = -1
                  , xdata::Vector<xdata::Bag<DTLineFebChipInfo> > const & _feb_chip_infos = xdata::Vector<xdata::Bag<DTLineFebChipInfo> >());

    void registerFields(xdata::AbstractBag * _bag);

    int getId() const;
    std::string const & getName() const;
    int getI2cAddress() const;
    bool getDisabled() const;
    std::string const & getLocation() const;
    std::string const & getPartition() const;
    // int getFebLocationId() const;
    xdata::Vector<xdata::Bag<DTLineFebChipInfo> > const & getFebChipInfos() const;

    void setId(int _id);
    void setName(std::string const & _name);
    void setI2cAddress(int _i2c_address);
    void setDisabled(bool _disabled);
    void setLocation(std::string const & _location);
    void setPartition(std::string const & _partition);
    // void setFebLocationId(int _feb_location_id);
    xdata::Vector<xdata::Bag<DTLineFebChipInfo> > & getFebChipInfos();
    void setFebChipInfos(std::vector<DTLineFebChipInfo> const & _feb_chip_infos);
    void setFebChipInfos(xdata::Vector<xdata::Bag<DTLineFebChipInfo> > const & _feb_chip_infos);

protected:
    xdata::Integer id_;
    xdata::String name_;
    xdata::Integer i2c_address_;
    xdata::Boolean disabled_;
    xdata::String location_, partition_;
    // xdata::Integer feb_location_id_;
    xdata::Vector<xdata::Bag<DTLineFebChipInfo> > feb_chip_infos_;
};

} // namespace xdaqutils
} // namespace rpct

#include "rpct/xdaqutils/DTLineFebInfo-inl.h"

#endif // rpct_xdaqutils_DTLineFebInfo_h
