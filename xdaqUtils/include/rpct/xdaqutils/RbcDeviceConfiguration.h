#ifndef RBCDEVICECONFIGURATION_H 
#define RBCDEVICECONFIGURATION_H 1


#include "xdata/Bag.h"
#include "xdata/Integer.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "xdata/Binary.h"

#include "rpct/devices/RbcDeviceSettings.h"
#include "rpct/devices/Rbc.h"
#include "rpct/std/tbutil.h"
#include "rpct/std/IllegalArgumentException.h"
#include "rpct/xdaqutils/ChipConfiguration.h"

#include <boost/scoped_ptr.hpp>

namespace rpct {
namespace xdaqutils {

class RbcDeviceConfiguration : virtual public RbcDeviceSettings, virtual public ChipConfiguration {
 private:
  
  xdata::Integer configurationId_;
  xdata::Integer recConfig_;
  xdata::Integer recConfigIn_;
  xdata::Integer recConfigVer_;
  xdata::Integer recMajority_;
  xdata::Integer recShape_;
  xdata::Integer recRbcMask_;
  xdata::Integer recCtrl_;
  
 public: 
  
  void registerFields(xdata::Bag<RbcDeviceConfiguration>* bag) { // for SOAP
    bag->addField("id",         &configurationId_);
    bag->addField("config",     &recConfig_);
    bag->addField("configIn",   &recConfigIn_);
    bag->addField("configVer",  &recConfigVer_);
    bag->addField("majority",   &recMajority_);
    bag->addField("shape",      &recShape_);
    bag->addField("mask",       &recRbcMask_);
    bag->addField("ctrl",       &recCtrl_);

  };

  //...define getters and setters
  virtual int getConfigurationId() {
      return configurationId_;
  }
  
  void setConfigurationId( xdata::Integer& configurationId ) {
    this->configurationId_ = configurationId;
  };

  unsigned int getRbcDeviceConfig() {
    return recConfig_;
  };
  
  void setRecConfig( xdata::Integer & recConfig ) {
    this->recConfig_ = recConfig;
  };

  unsigned int getRbcDeviceConfigIn(){
    return recConfigIn_;
  };

  void setRecConfigIn( xdata::Integer & recConfigIn ) {
    this->recConfigIn_ = recConfigIn;
  };

  unsigned int getRbcDeviceMajority() {
    return recMajority_;
  };

  void setRecMajority( xdata::Integer & recMajority ) {
    this->recMajority_ = recMajority;
  };

  unsigned int getRbcDeviceConfVer() {
    return recConfigVer_;    
  };

  void setRecConfigVer( xdata::Integer & recConfigVer ) {
    this->recConfigVer_ = recConfigVer;
  };

  unsigned int getRbcDeviceShape(){
      return recShape_;
  };

  void setRecShape( xdata::Integer & recShape ) {
      this->recShape_ = recShape;
  };


  unsigned int getRbcMask(){
	  return recRbcMask_;
  };

  void setRbcMask( xdata::Integer & rbcMask ) {
	  this->recRbcMask_ = rbcMask;
  };
  
  unsigned int getRbcCtrl(){
  	  return recCtrl_;
  };

  void setRbcCtrl( xdata::Integer & rbcCtrl ) {
	  this->recCtrl_ = rbcCtrl;
  };

  //...
  virtual rpct::HardwareItemType getDeviceType() {
      return rpct::RbcDevice::TYPE;
  }
  
};

}}

#endif // RBCDEVICECONFIGURATION_H
