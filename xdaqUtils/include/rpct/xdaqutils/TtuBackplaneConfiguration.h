#ifndef TTUBACKPLANECONFIGURATION_H
#define TTUBACKPLANECONFIGURATION_H 1


#include "xdata/Bag.h"
#include "xdata/Integer.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "xdata/Binary.h"
#include "rpct/devices/TtuBackplaneSettings.h"
#include "rpct/devices/TTUBackplane.h"
#include "rpct/std/tbutil.h"
#include "rpct/std/IllegalArgumentException.h"
#include "rpct/xdaqutils/ChipConfiguration.h"

#include <boost/scoped_ptr.hpp>

namespace rpct {
namespace xdaqutils {

class TtuBackplaneConfiguration : virtual public TtuBackplaneSettings, virtual public ChipConfiguration {
private:

	xdata::Integer configurationId_;
	xdata::Integer MaskTtu_;
	xdata::Integer MaskPointing_;
	xdata::Integer MaskBlast_;
	xdata::Integer DelayTtu_;
	xdata::Integer DelayPointing_;
	xdata::Integer DelayBlast_;
	xdata::Integer ShapeTtu_;
	xdata::Integer ShapePointing_;
	xdata::Integer ShapeBlast_;
	xdata::Vector<xdata::Integer> TrigConfig_; // TA_CONFIG2[]
	xdata::Vector<xdata::Integer> TrigConfig2_; // TA_CONFIG2[]


	boost::scoped_ptr<std::vector<int> > TrigConfigPtr_;
	boost::scoped_ptr<std::vector<int> > TrigConfig2Ptr_;
public:

	void registerFields(xdata::Bag<TtuBackplaneConfiguration>* bag) { // for SOAP
		bag->addField("id",                 &configurationId_);
		bag->addField("maskTtu",            &MaskTtu_);
		bag->addField("maskPointing",       &MaskPointing_);
		bag->addField("maskBlast",          &MaskBlast_);
		bag->addField("delayTtu",           &DelayTtu_);
		bag->addField("delayPointing",      &DelayPointing_);
		bag->addField("delayBlast",         &DelayBlast_);
		bag->addField("shapeTtu",           &ShapeTtu_);
		bag->addField("shapePointing",      &ShapePointing_);
		bag->addField("shapeBlast",         &ShapeBlast_);
		bag->addField("trigConfig",         &TrigConfig_);
		bag->addField("trigConfig2",        &TrigConfig2_);
	};

	//...define getters,setters

	virtual int getConfigurationId() {
		return configurationId_;
	}

	void setConfigurationId( xdata::Integer& configurationId ) {
		this->configurationId_ = configurationId;
	};

	virtual unsigned int  getMaskTtu() {
		return MaskTtu_;
	};

	void setMaskTtu( xdata::Integer &  MaskTtu) {
		this->MaskTtu_ = MaskTtu;
	};

	virtual unsigned int  getMaskPointing(){
		return MaskPointing_;
	};

	void setMaskPointing( xdata::Integer & MaskPointing ) {
		this->MaskPointing_ = MaskPointing;
	};

	virtual unsigned int  getMaskBlast(){
		return MaskBlast_;
	};

	void setMaskBlast( xdata::Integer & MaskBlast ) {
		this->MaskBlast_ = MaskBlast;
	};

	virtual unsigned int  getDelayTtu(){
		return DelayTtu_;
	};

	void setDelayTtu( xdata::Integer & DelayTtu ) {
		this->DelayTtu_ = DelayTtu;
	};

	virtual unsigned int  getDelayPointing() {
		return DelayPointing_;
	};

	void setDelayPointing( xdata::Integer & DelayPointing ) {
		this->DelayPointing_ = DelayPointing;
	};

	virtual unsigned int  getDelayBlast() {
		return DelayBlast_;
	};

	void setDelayBlast( xdata::Integer & DelayBlast ) {
		this->DelayBlast_ = DelayBlast;
	};

	virtual unsigned int  getShapeTtu() {
		return ShapeTtu_;
	};

	void setShapeTtu( xdata::Integer & ShapeTtu ) {
		this->ShapeTtu_ = ShapeTtu;
	};

	virtual unsigned int  getShapePointing() {
		return ShapePointing_;
	};

	void setShapePointing( xdata::Integer & ShapePointing) {
		this->ShapePointing_ = ShapePointing;
	};

	virtual unsigned int  getShapeBlast() {
		return ShapeBlast_;
	};

	void setShapeBlast( xdata::Integer & ShapeBlast ) {
		this->ShapeBlast_ = ShapeBlast;
	};
/*
	virtual unsigned int  getTrigConfig2() {
		return TrigConfig2_;
	};

	void setTrigConfig2( xdata::Integer & TrigConfig2 ) {
		this->TrigConfig2_ = TrigConfig2;
	};*/

	//...
	virtual rpct::HardwareItemType getDeviceType() {
		return rpct::TTTUBackplaneFinal::TYPE;
	};



	virtual std::vector<int>& getTrigConfig() {
		if (TrigConfigPtr_.get() == 0) {
			TrigConfigPtr_.reset(new std::vector<int>(TTTUBackplaneFinal::TTU_CONFIG_NUMBER));
			if ((int) TrigConfig_.size() != TTTUBackplaneFinal::TTU_CONFIG_NUMBER) {
				throw IllegalArgumentException(
						"TtuBackplaneConfiguration::getTrigConfig: invalid size");
			}
			for (std::vector<int>::size_type i = 0; i < TrigConfig_.size(); i++) {
				(*TrigConfigPtr_)[i] = TrigConfig_[i];
			}
		}
		return *TrigConfigPtr_;
	}

	void setTrigConfig(xdata::Vector<xdata::Integer>& TrigConfig) {
		if ((int) TrigConfig_.size() != TTTUBackplaneFinal::TTU_CONFIG_NUMBER) {
			throw IllegalArgumentException(
					"TtuBackplaneConfiguration::getTrigConfig: invalid size");
		}
		this->TrigConfig_ = TrigConfig;
	}

	virtual std::vector<int>& getTrigConfig2() {
		if (TrigConfig2Ptr_.get() == 0) {
			TrigConfig2Ptr_.reset(new std::vector<int>(TTTUBackplaneFinal::TTU_CONFIG2_NUMBER));
			if ((int) TrigConfig2_.size() != TTTUBackplaneFinal::TTU_CONFIG2_NUMBER) {
				throw IllegalArgumentException(
						"TtuBackplaneConfiguration::getTrigConfig2: invalid size");
			}
			for (std::vector<int>::size_type i = 0; i < TrigConfig2_.size(); i++) {
				(*TrigConfig2Ptr_)[i] = TrigConfig2_[i];
			}
		}
		return *TrigConfig2Ptr_;
	}

	void setTrigConfig2(xdata::Vector<xdata::Integer>& TrigConfig2) {
		if ((int) TrigConfig2_.size() != TTTUBackplaneFinal::TTU_CONFIG2_NUMBER) {
			throw IllegalArgumentException(
					"TtuBackplaneConfiguration::getTrigConfig2: invalid size");
		}
		this->TrigConfig2_ = TrigConfig2;
	}

};

}}

#endif // TTUTRIGGERCONFIGURATION_H
