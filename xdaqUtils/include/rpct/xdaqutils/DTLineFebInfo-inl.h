/** Filip Thyssen */

#ifndef rpct_xdaqutils_DTLineFebInfo_inl_h
#define rpct_xdaqutils_DTLineFebInfo_inl_h

namespace rpct {
namespace xdaqutils {

inline int DTLineFebChipInfo::getId() const
{
    return id_.value_;
}

inline int DTLineFebChipInfo::getPosition() const
{
    return position_.value_;
}

inline bool DTLineFebChipInfo::getDisabled() const
{
    return disabled_.value_;
}

inline void DTLineFebChipInfo::setId(int _id)
{
    id_ = _id;
}

inline void DTLineFebChipInfo::setPosition(int _position)
{
    position_ = _position;
}

inline void DTLineFebChipInfo::setDisabled(int _disabled)
{
    disabled_ = _disabled;
}


inline int DTLineFebInfo::getId() const
{
    return id_.value_;
}

inline std::string const & DTLineFebInfo::getName() const
{
    return name_.value_;
}

inline int DTLineFebInfo::getI2cAddress() const
{
    return i2c_address_.value_;
}

inline bool DTLineFebInfo::getDisabled() const
{
    return disabled_.value_;
}

inline std::string const & DTLineFebInfo::getLocation() const
{
    return location_.value_;
}

inline std::string const & DTLineFebInfo::getPartition() const
{
    return partition_.value_;
}

// inline int DTLineFebInfo::getFebLocationId() const
// {
//     return feb_location_id_.value_;
// }

inline xdata::Vector<xdata::Bag<DTLineFebChipInfo> > const & DTLineFebInfo::getFebChipInfos() const
{
    return feb_chip_infos_;
}

inline void DTLineFebInfo::setId(int _id)
{
    id_ = _id;
}

inline void DTLineFebInfo::setName(std::string const & _name)
{
    name_ = _name;
}

inline void DTLineFebInfo::setI2cAddress(int _i2c_address)
{
    i2c_address_ = _i2c_address;
}

inline void DTLineFebInfo::setDisabled(bool _disabled)
{
    disabled_ = _disabled;
}

inline void DTLineFebInfo::setLocation(std::string const & _location)
{
    location_ = _location;
}

inline void DTLineFebInfo::setPartition(std::string const & _partition)
{
    partition_ = _partition;
}

// inline void DTLineFebInfo::setFebLocationId(int _feb_location_id)
// {
//     feb_location_id_ = _feb_location_id;
// }

inline xdata::Vector<xdata::Bag<DTLineFebChipInfo> > & DTLineFebInfo::getFebChipInfos()
{
    return feb_chip_infos_;
}

inline void DTLineFebInfo::setFebChipInfos(std::vector<DTLineFebChipInfo> const & _feb_chip_infos)
{
    feb_chip_infos_.clear();
    feb_chip_infos_.reserve(_feb_chip_infos.size());
    for (std::vector<DTLineFebChipInfo>::const_iterator _feb_chip_info = _feb_chip_infos.begin()
             ; _feb_chip_info != _feb_chip_infos.end() ; ++ _feb_chip_info)
        {
            feb_chip_infos_.push_back(xdata::Bag<DTLineFebChipInfo>());
            feb_chip_infos_.back().bag = *_feb_chip_info;
        }
}

inline void DTLineFebInfo::setFebChipInfos(xdata::Vector<xdata::Bag<DTLineFebChipInfo> > const & _feb_chip_infos)
{
    feb_chip_infos_ = _feb_chip_infos;
}

} // namespace xdaqutils
} // namespace rpct

#endif // rpct_xdaqutils_DTLineFebInfo_inl_h
