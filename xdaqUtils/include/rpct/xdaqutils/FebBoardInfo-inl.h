/** Filip Thyssen */

#ifndef rpct_xdaqutils_FebBoardInfo_inl_h_
#define rpct_xdaqutils_FebBoardInfo_inl_h_

#include "rpct/xdaqutils/FebBoardInfo.h"

namespace rpct {

inline int FebBoardInfo::getControlBoardId() const
{
    return cb_id_.value_;
}
inline void FebBoardInfo::setControlBoardId(int id)
{
    cb_id_ = id;
}

inline int FebBoardInfo::getControlBoardChannel() const
{
    return cb_channel_.value_;
}
inline void FebBoardInfo::setControlBoardChannel(int channel)
{
    cb_channel_ = channel;
}

inline int FebBoardInfo::getFebLocationId() const
{
    return feblocation_id_.value_;
}
inline void FebBoardInfo::setFebLocationId(int id)
{
    feblocation_id_ = id;
}

inline bool FebBoardInfo::isDTControlled() const
{
    return dt_controlled_.value_;
}
inline void FebBoardInfo::setDTControlled(bool dt_controlled)
{
    dt_controlled_ = dt_controlled;
}

inline int FebBoardInfo::getFebType() const
{
    return (chipInfos.size() > 2 ? 1 : 0);
}

inline int FebBoardInfo::getI2cAddress() const
{
    return positionOrAddress.value_;
}
inline void FebBoardInfo::setI2cAddress(int position)
{
    positionOrAddress = position;
}

inline const BoardInfo::ChipInfoVector & FebBoardInfo::getChipInfos() const
{
    return chipInfos;
}
inline BoardInfo::ChipInfoVector & FebBoardInfo::getChipInfos()
{
    return chipInfos;
}
inline void FebBoardInfo::clearChipInfos()
{
    chipInfos.clear();
}
inline void FebBoardInfo::addChipInfo(const ChipInfo & chipinfo)
{
    xdata::Bag<ChipInfo> cib;
    chipInfos.push_back(cib);
    chipInfos.rbegin()->bag = chipinfo;
}

} // namespace rpct

#endif // rpct_xdaqutils_FebBoardInfo_inl_h_
