/** Filip Thyssen */

#ifndef rpct_xdaqutils_FebChipInfo_h_
#define rpct_xdaqutils_FebChipInfo_h_

#include <string>

#include "xdata/Integer.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Bag.h"

namespace xdata {
class AbstractBag;
}

namespace rpct {

class FebChipInfo
{
public:
    FebChipInfo(int chipid = -1
                , int flid = -1
                , int boe = 0
                , int wheel = 0
                , int layer = 0
                , int sector = 0
                , const std::string & subsector = ""
                , const std::string & partition = ""
                , int channel = 0
                , int address = 0
                , int position = 0);

    void registerFields(xdata::AbstractBag * bag);

    int getChipId() const;
    void setChipId(int chipid);

    int getFebLocationId() const;
    void setFebLocationId(int flid);

    int getBoE() const;
    void setBoE(int boe);

    int getWheel() const;
    void setWheel(int wheel);

    int getLayer() const;
    void setLayer(int layer);

    int getSector() const;
    void setSector(int sector);

    const std::string & getSubsector() const;
    void setSubsector(const std::string & subsector);

    const std::string & getPartition() const;
    void setPartition(const std::string & partition);

    int getChannel() const;
    void setChannel(int channel);

    int getAddress() const;
    void setAddress(int address);

    int getPosition() const;
    void setPosition(int position);

protected:
    xdata::Integer chipid_, flid_, boe_, wheel_, layer_, sector_;
    xdata::String subsector_, partition_;
    xdata::Integer channel_, address_, position_;
};

typedef xdata::Vector<xdata::Bag<FebChipInfo> > FebChipInfoVector;

} // namespace rpct

#include "rpct/xdaqutils/FebChipInfo-inl.h"

#endif // rpct_xdaqutils_FebChipInfo_h_
