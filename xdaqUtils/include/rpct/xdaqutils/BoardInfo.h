#ifndef _RPCTBOARDINFO_H_
#define _RPCTBOARDINFO_H_


#include "xdata/Bag.h"
#include "xdata/Integer.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "rpct/xdaqutils/ChipInfo.h"

namespace rpct {

class BoardInfo {
public:
    typedef xdata::Vector<xdata::Bag<ChipInfo> > ChipInfoVector; 
protected:
    xdata::Integer id;
    xdata::String name;
    xdata::String label;
    xdata::String type;
    xdata::Integer positionOrAddress;     
    xdata::Boolean disabled;
    ChipInfoVector chipInfos;
    
public:
    void registerFields(xdata::AbstractBag * bag) {
        bag->addField("id", &id);
        bag->addField("name", &name);
        bag->addField("label", &label);
        bag->addField("type", &type);
        bag->addField("positionOrAddress", &positionOrAddress); 
        bag->addField("disabled", &disabled);  
        bag->addField("chipInfos", &chipInfos);       
    }
    
    xdata::Integer& getId() {
        return id;
    }
    
    void setId(xdata::Integer& id) {
        this->id = id;
    }
    
    xdata::String& getName() {
        return name;
    }
    
    void setName(xdata::String& name) {
        this->name = name;
    }
    
    xdata::String& getLabel() {
        return label;
    }
    
    void setLabel(xdata::String& label) {
        this->label = label;
    }
    
    xdata::String& getType() {
        return type;
    }
    
    void setType(xdata::String& type) {
        this->type = type;
    }
    
    xdata::Integer& getPositionOrAddress() {
        return positionOrAddress;
    }
    
    void setPosition(xdata::Integer& positionOrAddress) {
        this->positionOrAddress = positionOrAddress;
    }

    xdata::Boolean& isDisabled() {
        return disabled;
    }

    void setDisabled(xdata::Boolean& disabled) {
        this->disabled = disabled;
    }
    
    ChipInfoVector& getChipInfos() {
        return chipInfos;
    }
    
    void setChipInfos(ChipInfoVector& chipInfos) {
        this->chipInfos = chipInfos;
    }
};

}

#endif /*_RPCTFEBSTATEINFO_H_*/
