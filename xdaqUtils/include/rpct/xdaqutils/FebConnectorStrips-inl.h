/** Filip Thyssen */

#ifndef rpct_xdaqutils_FebConnectorStrips_inl_h_
#define rpct_xdaqutils_FebConnectorStrips_inl_h_

#include "rpct/xdaqutils/FebConnectorStrips.h"

namespace rpct {
namespace xdaqutils {

inline int FebConnectorStrips::getId() const
{
    return id_.value_;
}

inline int FebConnectorStrips::getLinkBoardId() const
{
    return linkboard_id_.value_;
}

inline int FebConnectorStrips::getLinkBoardInput() const
{
    return linkboard_input_.value_;
}

inline int FebConnectorStrips::getFebBoardId() const
{
    return febboard_id_.value_;
}

inline int FebConnectorStrips::getFebBoardConnector() const
{
    return febboard_connector_.value_;
}

inline std::string const & FebConnectorStrips::getLocation() const
{
    return location_.value_;
}

inline std::string const & FebConnectorStrips::getPartition() const
{
    return partition_.value_;
}

inline int FebConnectorStrips::getRollConnector() const
{
    return roll_connector_.value_;
}

inline int FebConnectorStrips::getPins() const
{
    return pins_.value_;
}

inline int FebConnectorStrips::getFirstStrip() const
{
    return first_strip_.value_;
}

inline int FebConnectorStrips::getSlope() const
{
    return slope_.value_;
}

inline void FebConnectorStrips::setId(int _id)
{
    id_ = _id;
}

inline void FebConnectorStrips::setLinkBoardId(int _linkboard_id)
{
    linkboard_id_ = _linkboard_id;
}

inline void FebConnectorStrips::setLinkBoardInput(int _linkboard_input)
{
    linkboard_input_ = _linkboard_input;
}

inline void FebConnectorStrips::setFebBoardId(int _febboard_id)
{
    febboard_id_ = _febboard_id;
}

inline void FebConnectorStrips::setFebBoardConnector(int _febboard_connector)
{
    febboard_connector_ = _febboard_connector;
}

inline void FebConnectorStrips::setLocation(std::string const & _location)
{
    location_ = _location;
}

inline void FebConnectorStrips::setPartition(std::string const & _partition)
{
    partition_ = _partition;
}

inline void FebConnectorStrips::setRollConnector(int _roll_connector)
{
    roll_connector_ = _roll_connector;
}

inline void FebConnectorStrips::setPins(int _pins)
{
    pins_ = _pins;
}

inline void FebConnectorStrips::setFirstStrip(int _first_strip)
{
    first_strip_ = _first_strip;
}

inline void FebConnectorStrips::setSlope(int _slope)
{
    slope_ = _slope;
}

} // namespace xdaqutils
} // namespace rpct

#endif // rpct_xdaqutils_FebConnectorStrips_inl_h_
