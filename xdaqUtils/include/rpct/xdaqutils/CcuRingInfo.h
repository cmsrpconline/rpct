#ifndef _RPCTCCURINGINFO_H_
#define _RPCTCCURINGINFO_H_


#include "xdata/Bag.h"
#include "xdata/Integer.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "rpct/xdaqutils/LinkBoxInfo.h"
#include "rpct/xdaqutils/FebBoardInfo.h"

namespace rpct {

class CcuRingInfo {
public:
    typedef xdata::Vector<xdata::Bag<LinkBoxInfo> > LinkBoxInfoVector; 
    typedef xdata::Vector<xdata::Bag<FebBoardInfo> > FebBoardInfoVector; 
private:
    xdata::Integer pciSlot;
    xdata::Integer ccsPosition;
    xdata::Integer fecPosition;
    LinkBoxInfoVector linkBoxInfos;
    FebBoardInfoVector febBoardInfos;
    
public:
    void registerFields(xdata::Bag<CcuRingInfo>* bag) {
        bag->addField("pciSlot", &pciSlot);
        bag->addField("ccsPosition", &ccsPosition);
        bag->addField("fecPosition", &fecPosition);
        bag->addField("linkBoxInfos", &linkBoxInfos);       
        bag->addField("febBoardInfos", &febBoardInfos);       
    }
    
    xdata::Integer& getPciSlot() {
        return pciSlot;
    }
    
    void setPciSlot(xdata::Integer& pciSlot) {
        this->pciSlot = pciSlot;
    }
    
    xdata::Integer& getCcsPosition() {
        return ccsPosition;
    }
    
    void setCcsPosition(xdata::Integer& ccsPosition) {
        this->ccsPosition = ccsPosition;
    }
    
    xdata::Integer& getFecPosition() {
        return fecPosition;
    }
    
    void setFecPosition(xdata::Integer& fecPosition) {
        this->fecPosition = fecPosition;
    }
    
    LinkBoxInfoVector& getLinkBoxInfos() {
        return linkBoxInfos;
    }
    
    void setLinkBoxInfos(LinkBoxInfoVector& linkBoxInfos) {
        this->linkBoxInfos = linkBoxInfos;
    }

    FebBoardInfoVector& getFebBoardInfos() {
        return febBoardInfos;
    }
    
    void setFebBoardInfos(FebBoardInfoVector& febBoardInfos) {
        this->febBoardInfos = febBoardInfos;
    }
};

typedef xdata::Bag<CcuRingInfo> CcuRingInfoBag;

}

#endif /*_RPCTFEBSTATEINFO_H_*/
