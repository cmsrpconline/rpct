/*
 * LogUtils.h
 *
 *  Created on: 2010-04-07
 *      Author: Krzysiek
 */

#ifndef XDAQ_COMMON_UTILS_H_
#define XDAQ_COMMON_UTILS_H_


#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/Action.h"
#include "toolbox/TimeInterval.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"

#include "xdaq/Application.h"

#include <iostream>



template <typename LISTENER>
void alarm_me(LISTENER* app,std::string level,xcept::Exception exp){
	app->notifyQualified(level,exp);
}

#define ALARM_EXCEPT(XDAQ_APP,LEVEL, EXCEPTION) \
alarm_me(XDAQ_APP,LEVEL,EXCEPTION)

#define ALARM_STD(XDAQ_APP,LEVEL, EXCEPTION) \
alarm_me(XDAQ_APP,LEVEL,xcept::Exception("std::exception",e.what(),__FILE__,__LINE__,__FUNCTION__))

#define ALARM_OTHER(XDAQ_APP,LEVEL,MESSAGE) \
alarm_me(XDAQ_APP,LEVEL,xcept::Exception("Unknown exception",MESSAGE,__FILE__,__LINE__,__FUNCTION__))

namespace rpct {

template <typename LISTENER>
void bind(xdaq::Application* app, LISTENER * obj, 
	  xoap::MessageReference (LISTENER::*func)(xoap::MessageReference) throw (xoap::exception::Exception), 
	  const std::string & messageName, 
	  const std::string & namespaceURI) 
{
  xoap::Method<LISTENER> * f = new xoap::Method<LISTENER>;
  f->obj_ = obj;
  f->func_ = func;
  f->namespaceURI_ = namespaceURI;
  f->name_ = messageName;
  std::string name = namespaceURI + ":";
  name += messageName;
  app->removeMethod(name);
  app->addMethod(f, name);
}

class MutexHandler {
  public:
    MutexHandler(toolbox::BSem& mutex):mutex_(&mutex) {
      //log4cplus::Logger logger = log4cplus::Logger::getInstance("MutexHandler");
      //LOG4CPLUS_INFO(logger, "Take mutex");
      mutex_->take();
      //LOG4CPLUS_INFO(logger, "Take mutex2");
    };
    MutexHandler(toolbox::BSem* mutex):mutex_(mutex) {
      //log4cplus::Logger logger = log4cplus::Logger::getInstance("MutexHandler");
      //LOG4CPLUS_INFO(logger, "Take mutex");
      mutex_->take();
      //LOG4CPLUS_INFO(logger, "Take mutex2");
    };
    ~MutexHandler() {
      //log4cplus::Logger logger = log4cplus::Logger::getInstance("MutexHandler");
      //LOG4CPLUS_INFO(logger, "Give mutex");
      mutex_->give();
      //LOG4CPLUS_INFO(logger, "Give mutex2");
    };
  private:
    toolbox::BSem* mutex_;    
    MutexHandler(const MutexHandler&);
    MutexHandler& operator=(const MutexHandler&);
};


}
#endif /* XDAQ_COMMON_UTILS_H_ */
