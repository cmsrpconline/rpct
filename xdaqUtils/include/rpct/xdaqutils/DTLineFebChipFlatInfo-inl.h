/** Filip Thyssen */

#ifndef rpct_xdaqutils_DTLineFebChipFlatInfo_inl_h
#define rpct_xdaqutils_DTLineFebChipFlatInfo_inl_h

namespace rpct {
namespace xdaqutils {

inline int DTLineFebChipFlatInfo::getChipId() const
{
    return chip_id_.value_;
}

inline int DTLineFebChipFlatInfo::getBoardId() const
{
    return board_id_.value_;
}

inline std::string const & DTLineFebChipFlatInfo::getName() const
{
    return name_.value_;
}

inline int DTLineFebChipFlatInfo::getI2cAddress() const
{
    return i2c_address_.value_;
}

inline int DTLineFebChipFlatInfo::getPosition() const
{
    return position_.value_;
}

inline bool DTLineFebChipFlatInfo::getChipDisabled() const
{
    return chip_disabled_.value_;
}

inline bool DTLineFebChipFlatInfo::getBoardDisabled() const
{
    return board_disabled_.value_;
}

inline std::string const & DTLineFebChipFlatInfo::getLocation() const
{
    return location_.value_;
}

inline std::string const & DTLineFebChipFlatInfo::getPartition() const
{
    return partition_.value_;
}

// inline int DTLineFebChipFlatInfo::getFebLocationId() const
// {
//     return feb_location_id_.value_;
// }

inline void DTLineFebChipFlatInfo::setChipId(int _chip_id)
{
    chip_id_ = _chip_id;
}

inline void DTLineFebChipFlatInfo::setBoardId(int _board_id)
{
    board_id_ = _board_id;
}

inline void DTLineFebChipFlatInfo::setName(std::string const & _name)
{
    name_ = _name;
}

inline void DTLineFebChipFlatInfo::setI2cAddress(int _i2c_address)
{
    i2c_address_ = _i2c_address;
}

inline void DTLineFebChipFlatInfo::setPosition(int _position)
{
    position_ = _position;
}

inline void DTLineFebChipFlatInfo::setChipDisabled(bool _chip_disabled)
{
    chip_disabled_ = _chip_disabled;
}

inline void DTLineFebChipFlatInfo::setBoardDisabled(bool _board_disabled)
{
    board_disabled_ = _board_disabled;
}

inline void DTLineFebChipFlatInfo::setLocation(std::string const & _location)
{
    location_ = _location;
}

inline void DTLineFebChipFlatInfo::setPartition(std::string const & _partition)
{
    partition_ = _partition;
}

// inline void DTLineFebChipFlatInfo::setFebLocationId(int _feb_location_id)
// {
//     feb_location_id_ = _feb_location_id;
// }

} // namespace xdaqutils
} // namespace rpct

#endif // rpct_xdaqutils_DTLineFebChipFlatInfo_inl_h
