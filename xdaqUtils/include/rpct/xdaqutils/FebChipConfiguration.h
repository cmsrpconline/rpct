/** Filip Thyssen */

#ifndef rpct_xdaqutils_FebChipConfiguration_h_
#define rpct_xdaqutils_FebChipConfiguration_h_

#include "xdata/Integer.h"

#include "rpct/xdaqutils/ChipConfiguration.h"

#include "rpct/devices/IFebChipConfiguration.h"
#include "rpct/ii/feb_const.h"

namespace xdata {
class AbstractBag;
}

namespace rpct {
namespace xdaqutils {

class FebChipConfiguration : public rpct::IFebChipConfiguration
                           , public ChipConfiguration
{
public:
    FebChipConfiguration(int id = -1
                         , unsigned int vth = rpct::preset::feb::vth_
                         , unsigned int vmon = rpct::preset::feb::vmon_
                         , int vth_offset = rpct::preset::feb::offset_
                         , int vmon_offset = rpct::preset::feb::offset_);
    void registerFields(xdata::AbstractBag * bag);

    // ChipConfiguration
    int getConfigurationId();

    void setId(int id);
    int getId() const;

    void setVTH(unsigned int vth);
    void setVMON(unsigned int vmon);
    void setVTHOffset(int vth_offset);
    void setVMONOffset(int vmon_offset);

    unsigned int getVTH() const;
    unsigned int getVMON() const;
    int getVTHOffset() const;
    int getVMONOffset() const;
protected:
    xdata::Integer id_;
    xdata::Integer vth_, vmon_;
    xdata::Integer vth_offset_, vmon_offset_;
};

} // namespace xdaqutils
} // namespace rpct

#include "rpct/xdaqutils/FebChipConfiguration-inl.h"

#endif // rpct_xdaqutils_FebChipConfiguration_h_
