#ifndef _RPCTRBCINFO_H_
#define _RPCTRBCINFO_H_


#include "xdata/Bag.h"
#include "xdata/String.h"
#include "xdata/Integer.h"

namespace rpct {

class RbcInfo {
private:
    xdata::String xdaqUrl;
    xdata::Integer xdaqAppInstance;
    xdata::String linkBoxName;
public:
    void registerFields(xdata::Bag<RbcInfo>* bag) {
        bag->addField("xdaqUrl", &xdaqUrl);
        bag->addField("xdaqAppInstance", &xdaqAppInstance);
        bag->addField("linkBoxName", &linkBoxName);
    }
    
    xdata::String& getXdaqUrl() {
        return xdaqUrl;
    }
    
    void setXdaqUrl(xdata::String xdaqUrl) {
        this->xdaqUrl = xdaqUrl;
    }
    
    xdata::Integer& getXdaqAppInstance() {
        return xdaqAppInstance;
    }
    
    void setXdaqAppInstance(xdata::Integer xdaqAppInstance) {
        this->xdaqAppInstance = xdaqAppInstance;
    }
    
    xdata::String& getLinkBoxName() {
        return linkBoxName;
    }
    
    void setLinkBoxName(xdata::String linkBoxName) {
        this->linkBoxName = linkBoxName;
    }
};

}

#endif /*_FEBINFO_H_*/
