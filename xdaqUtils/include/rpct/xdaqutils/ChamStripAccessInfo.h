#ifndef _RPCTCHAMSTRIPACCESSINFO_H_
#define _RPCTCHAMSTRIPACCESSINFO_H_


#include "xdata/Bag.h"
#include "xdata/String.h"
#include "xdata/Integer.h"

namespace rpct {

class ChamStripAccessInfo {
private:
    xdata::String chamberLocationName;
    xdata::Integer boardId;
    xdata::Integer xdaqAppInstance;
    xdata::Integer chipId;
    xdata::Integer febId;
    xdata::Integer febConnectorNum;
    xdata::String febLocalEtaPartition;
    xdata::Integer febI2cAddress;
    xdata::Integer cableChanNum;
    xdata::Integer chamberStripNum;
    xdata::Integer chamberStripId;
public:
    void registerFields(xdata::Bag<ChamStripAccessInfo>* bag) {
        bag->addField("chamberLocationName", &chamberLocationName);
        bag->addField("boardId", &boardId);
        bag->addField("xdaqAppInstance", &xdaqAppInstance);
        bag->addField("chipId", &chipId);
        bag->addField("febId", &febId);
        bag->addField("febConnectorNum", &febConnectorNum);
        bag->addField("febLocalEtaPartition", &febLocalEtaPartition);
        bag->addField("febI2cAddress", &febI2cAddress);
        bag->addField("cableChanNum", &cableChanNum);
        bag->addField("chamberStripNum", &chamberStripNum);
        bag->addField("chamberStripId", &chamberStripId);
    }
    
    xdata::String& getChamberLocationName() {
        return chamberLocationName;
    }
    
    void setChamberLocationName(xdata::String chamberLocationName) {
        this->chamberLocationName = chamberLocationName;
    }

    xdata::Integer& getBoardId() {
        return boardId;
    }
    
    void setBoardId(xdata::Integer boardId) {
        this->boardId = boardId;
    }

    xdata::Integer& getXdaqAppInstance() {
        return xdaqAppInstance;
    }
    
    void setXdaqAppInstance(xdata::Integer xdaqAppInstance) {
        this->xdaqAppInstance = xdaqAppInstance;
    }
    
    xdata::Integer& getChipId() {
        return chipId;
    }
    
    void setChipId(xdata::Integer chipId) {
        this->chipId = chipId;
    }

    xdata::Integer& getFebId() {
        return febId;
    }
    
    void setFebId(xdata::Integer febId) {
        this->febId = febId;
    }

    xdata::Integer& getFebConnectorNum() {
        return febConnectorNum;
    }
    
    void setFebConnectorNum(xdata::Integer febConnectorNum) {
        this->febConnectorNum = febConnectorNum;
    }

    xdata::String& getFebLocalEtaPartition() {
        return febLocalEtaPartition;
    }
    
    void setFebLocalEtaPartition(xdata::String febLocalEtaPartition) {
        this->febLocalEtaPartition = febLocalEtaPartition;
    }
    
    xdata::Integer& getFebI2cAddress() {
        return febI2cAddress;
    }
    
    void setFebI2cAddress(xdata::Integer febI2cAddress) {
        this->febI2cAddress = febI2cAddress;
    }

    xdata::Integer& getCableChanNum() {
        return cableChanNum;
    }
    
    void setCableChanNum(xdata::Integer cableChanNum) {
        this->cableChanNum = cableChanNum;
    }

    xdata::Integer& getChamberStripNum() {
        return chamberStripNum;
    }
    
    void setChamberStripNum(xdata::Integer chamberStripNum) {
        this->chamberStripNum = chamberStripNum;
    }

    xdata::Integer& getChamberStripId() {
        return chamberStripId;
    }
    
    void setChamberStripId(xdata::Integer chamberStripId) {
        this->chamberStripId = chamberStripId;
    }
};

}

#endif /*_CHAMSTRIPINFO_H_*/
