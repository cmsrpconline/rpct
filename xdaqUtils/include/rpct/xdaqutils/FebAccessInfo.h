#ifndef _RPCTFEBACCESSINFO_H_
#define _RPCTFEBACCESSINFO_H_


#include "xdata/Bag.h"
#include "xdata/String.h"
#include "xdata/Integer.h"

namespace rpct {

class FebAccessInfo {
private:
    xdata::Integer febId;
    xdata::String xdaqUrl;
    xdata::Integer xdaqAppInstance;
    xdata::String febLocalEtaPartition;
    xdata::String chamberLocationName;
    xdata::Integer ccuAddress;
    xdata::Integer i2cChannel;
    xdata::Integer febAddress;
	xdata::Integer posInLocalEtaPartition;
public:
    void registerFields(xdata::Bag<FebAccessInfo>* bag) {
        bag->addField("febId", &febId);
        bag->addField("xdaqUrl", &xdaqUrl);
        bag->addField("xdaqAppInstance", &xdaqAppInstance);
        bag->addField("febLocalEtaPartition", &febLocalEtaPartition);
        bag->addField("chamberLocationName", &chamberLocationName);
        bag->addField("ccuAddress", &ccuAddress);
        bag->addField("i2cChannel", &i2cChannel);
        bag->addField("febAddress", &febAddress);
        bag->addField("posInLocalEtaPartition", &posInLocalEtaPartition);
    }
    
    xdata::Integer& getFebId() {
        return febId;
    }
    
    void setFebId(xdata::Integer febId) {
        this->febId = febId;
    }
    
    xdata::String& getXdaqUrl() {
        return xdaqUrl;
    }
    
    void setXdaqUrl(xdata::String xdaqUrl) {
        this->xdaqUrl = xdaqUrl;
    }
    
    xdata::Integer& getXdaqAppInstance() {
        return xdaqAppInstance;
    }
    
    void setXdaqAppInstance(xdata::Integer xdaqAppInstance) {
        this->xdaqAppInstance = xdaqAppInstance;
    }
    
    xdata::String& getFebLocalEtaPartition() {
        return febLocalEtaPartition;
    }
    
    void setFebLocalEtaPartition(xdata::String febLocalEtaPartition) {
        this->febLocalEtaPartition = febLocalEtaPartition;
    }
    
    xdata::String& getChamberLocationName() {
        return chamberLocationName;
    }
    
    void setChamberLocationName(xdata::String chamberLocationName) {
        this->chamberLocationName = chamberLocationName;
    }
    
    xdata::Integer& getCcuAddress() {
        return ccuAddress;
    }
    
    void setCcuAddress(xdata::Integer ccuAddress) {
        this->ccuAddress = ccuAddress;
    }
    
    xdata::Integer& getI2cChannel() {
        return i2cChannel;
    }
    
    void setI2cChannel(xdata::Integer i2cChannel) {
        this->i2cChannel = i2cChannel;
    }
    
    xdata::Integer& getFebAddress() {
        return febAddress;
    }
    
    void setFebAddress(xdata::Integer febAddress) {
        this->febAddress = febAddress;
    }
    xdata::Integer& getPosInLocalEtaPartition() {
        return posInLocalEtaPartition;
    }
    
    void setPosInLocalEtaPartition(xdata::Integer posInLocalEtaPartition) {
        this->posInLocalEtaPartition = posInLocalEtaPartition;
    }
};

}

#endif /*_FEBINFO_H_*/
