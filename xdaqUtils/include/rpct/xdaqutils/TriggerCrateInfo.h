#ifndef _RPCTTRIGGERCRATEINFO_H_
#define _RPCTTRIGGERCRATEINFO_H_


#include "xdata/Bag.h"
#include "xdata/Integer.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "rpct/xdaqutils/BoardInfo.h"

namespace rpct {

class TriggerCrateInfo {
public:
    typedef xdata::Vector<xdata::Bag<BoardInfo> > BoardInfoVector; 
private:
    xdata::Integer id;
    xdata::String name;
    xdata::String label;     
    xdata::Integer pciSlot;
    xdata::Integer posInVmeChain;
    xdata::Integer logSector;
    xdata::Boolean disabled;
    BoardInfoVector boardInfos;
    
public:
    void registerFields(xdata::Bag<TriggerCrateInfo>* bag) {
        bag->addField("id", &id);
        bag->addField("name", &name);
        bag->addField("label", &label);
        bag->addField("pciSlot", &pciSlot);
        bag->addField("posInVmeChain", &posInVmeChain);
        bag->addField("logSector", &logSector);   
        bag->addField("disabled", &disabled);  
        bag->addField("boardInfos", &boardInfos);    
    }
    
    xdata::Integer& getId() {
        return id;
    }
    
    void setId(xdata::Integer& id) {
        this->id = id;
    }    
    
    xdata::String& getName() {
        return name;
    }
    
    void setName(xdata::String& name) {
        this->name = name;
    }
    
    xdata::String& getLabel() {
        return label;
    }
    
    void setLabel(xdata::String& label) {
        this->label = label;
    }   
    
    xdata::Integer& getPciSlot() {
        return pciSlot;
    }
    
    void setPciSlot(xdata::Integer& pciSlot) {
        this->pciSlot = pciSlot;
    }    
    
    xdata::Integer& getPosInVmeChain() {
        return posInVmeChain;
    }
    
    void setPosInVmeChain(xdata::Integer& posInVmeChain) {
        this->posInVmeChain = posInVmeChain;
    }    
    
    xdata::Integer& getLogSector() {
        return logSector;
    }
    
    void setLogSector(xdata::Integer& logSector) {
        this->logSector = logSector;
    }     

    xdata::Boolean& isDisabled() {
        return disabled;
    }

    void setDisabled(xdata::Boolean& disabled) {
        this->disabled = disabled;
    }
    
    BoardInfoVector& getBoardInfos() {
        return boardInfos;
    }
    
    void setBoardInfos(BoardInfoVector& boardInfos) {
        this->boardInfos = boardInfos;
    }
};

typedef xdata::Bag<TriggerCrateInfo> TriggerCrateInfoBag;

}

#endif 
