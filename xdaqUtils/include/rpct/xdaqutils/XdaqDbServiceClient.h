#ifndef _RPCTXDAQDBSERVICECLIENT_H_
#define _RPCTXDAQDBSERVICECLIENT_H_

#include "xdaq/Application.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"

#include "xdata/Vector.h"
#include "xdata/String.h"
#include "xdata/Integer.h"

#include "rpct/std/IllegalArgumentException.h"
#include "rpct/xdaqutils/XdaqSoapAccess.h"
#include "rpct/xdaqutils/FebAccessInfo.h"
#include "rpct/xdaqutils/ChamStripAccessInfo.h"
#include "rpct/xdaqutils/FebConnectorStrips.h"
#include "rpct/xdaqutils/DTLineFebChipFlatInfo.h"
#include "rpct/xdaqutils/CcuRingInfo.h"
#include "rpct/xdaqutils/FebChipInfo.h"
#include "rpct/xdaqutils/ConfigurationSetImplXData.h"
#include "rpct/xdaqutils/TriggerCrateInfo.h"
#include "rpct/xdaqutils/VmeCrateInfo.h"
#include "rpct/xdaqutils/RbcInfo.h"

#include <list>
#include <map>

#include "log4cplus/logger.h"

namespace rpct {
namespace xdaqutils {

class XdaqDbServiceClient {
private:
	log4cplus::Logger logger;
public:
    static const char* RPCT_DB_SERVICE_NS;
    static const char* RPCT_DB_SERVICE_PREFIX;
    static const char* RPCT_I_SERVICE_NS;
    static const char* RPCT_I_SERVICE_PREFIX;

    //the same as in LocalConfigKey.java
    static const char* SUBSYSTEM_LBS;
    static const char* SUBSYSTEM_TCS;
    static const char* SUBSYSTEM_SC;
    static const char* SUBSYSTEM_RBCS_TTUS;
    static const char* SUBSYSTEM_FEBS;
    static const char* SUBSYSTEM_ALL;

public:
    enum BarrelOrEndcap {
        BARREL, ENDCAP
    };
    static const char* toString(BarrelOrEndcap boe) {
        if (boe == BARREL) {
            return "Barrel";
        }
        return "Endcap";

    }
    static BarrelOrEndcap stringToBarrelOrEndcap(std::string barrelOrEndcap) {
        if (barrelOrEndcap == "Barrel") {
            return BARREL;
        }
        if (barrelOrEndcap == "Endcap") {
            return ENDCAP;
        }
        throw rpct::IllegalArgumentException("Illegal BarrelOrEndcap string '" + barrelOrEndcap + "'");
    }

protected:
    xdaq::Application* application;
    XdaqSoapAccess soapAccess;

public:
    XdaqDbServiceClient(xdaq::Application* app);

    typedef xdata::Vector<xdata::Bag<FebAccessInfo> > FebAccessInfoVector;
    typedef xdata::Vector<xdata::Bag<ChamStripAccessInfo> > ChamStripAccessInfoVector;
    typedef xdata::Vector<xdata::Bag<FebConnectorStrips> > FebConnectorStripsVector;
    typedef xdata::Vector<xdata::Bag<DTLineFebChipFlatInfo> > DTLineFebChipFlatInfoVector;
    typedef xdata::Vector<xdata::Bag<RbcInfo> > RbcInfoVector;

    // !!! Caller should delete the result
    FebAccessInfoVector* getFebsByBarrelOrEndcap(BarrelOrEndcap barrelOrEndcap) {

        XdaqSoapAccess::TSOAPParamList paramList;
        xdata::String xBarrelOrEndcap = toString(barrelOrEndcap);
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "barrelOrEndcap", &xBarrelOrEndcap));

        LOG4CPLUS_DEBUG(logger,
                "sending request to DB Service: getFebsByBarrelOrEndcap barrelOrEndcap = "
                        << (std::string)xBarrelOrEndcap);

        xoap::MessageReference getFebsResponse = soapAccess.sendSOAPRequest(
                "getFebsByBarrelOrEndcap", RPCT_DB_SERVICE_PREFIX,
                RPCT_DB_SERVICE_NS, paramList, "RpctDbService", 0, true);

        FebAccessInfoVector* febAccessInfoVector = new FebAccessInfoVector();
        soapAccess.parseSOAPResponse(getFebsResponse,
                "getFebsByBarrelOrEndcap", RPCT_DB_SERVICE_PREFIX,
                RPCT_DB_SERVICE_NS, *febAccessInfoVector);
        return febAccessInfoVector;
    }

    // !!! Caller should delete the result
    FebAccessInfoVector* getFebsByDiskOrWheel(int diskOrWheel,
            BarrelOrEndcap barrelOrEndcap) {

        XdaqSoapAccess::TSOAPParamList paramList;
        xdata::Integer xDiskOrWheel = diskOrWheel;
        xdata::String xBarrelOrEndcap = toString(barrelOrEndcap);
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "diskOrWheel", &xDiskOrWheel));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "barrelOrEndcap", &xBarrelOrEndcap));

        LOG4CPLUS_DEBUG(logger,
                "sending request to DB Service: getFebsByDiskOrWheel diskOrWheel = "
                        << diskOrWheel<< " barrelOrEndcap = "
                        << (std::string)xBarrelOrEndcap);

        xoap::MessageReference getFebsResponse = soapAccess.sendSOAPRequest(
                "getFebsByDiskOrWheel", RPCT_DB_SERVICE_PREFIX,
                RPCT_DB_SERVICE_NS, paramList, "RpctDbService", 0, true);

        //cout << "getFebsResponse:\n";
        //getFebsResponse->writeTo(cout);
        //cout << endl;

        FebAccessInfoVector* febAccessInfoVector = new FebAccessInfoVector();
        soapAccess.parseSOAPResponse(getFebsResponse, "getFebsByDiskOrWheel",
                RPCT_DB_SERVICE_PREFIX, RPCT_DB_SERVICE_NS,
                *febAccessInfoVector);
        return febAccessInfoVector;
    }

    // !!! Caller should delete the result
    FebAccessInfoVector* getFebsByLayer(int diskOrWheel, int layer,
            BarrelOrEndcap barrelOrEndcap) {

        XdaqSoapAccess::TSOAPParamList paramList;
        xdata::Integer xDiskOrWheel = diskOrWheel;
        xdata::Integer xLayer = layer;
        xdata::String xBarrelOrEndcap = toString(barrelOrEndcap);
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "diskOrWheel", &xDiskOrWheel));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type("layer",
                &xLayer));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "barrelOrEndcap", &xBarrelOrEndcap));

        LOG4CPLUS_DEBUG(logger,
                "sending request to DB Service: getFebsByLayer diskOrWheel = "
                        << diskOrWheel<< " layer = "<< layer
                        << " barrelOrEndcap = "<< (std::string)xBarrelOrEndcap);

        xoap::MessageReference getFebsResponse = soapAccess.sendSOAPRequest(
                "getFebsByLayer", RPCT_DB_SERVICE_PREFIX, RPCT_DB_SERVICE_NS,
                paramList, "RpctDbService", 0, true);

        FebAccessInfoVector* febAccessInfoVector = new FebAccessInfoVector();
        soapAccess.parseSOAPResponse(getFebsResponse, "getFebsByLayer",
                RPCT_DB_SERVICE_PREFIX, RPCT_DB_SERVICE_NS,
                *febAccessInfoVector);
        return febAccessInfoVector;
    }

    // !!! Caller should delete the result
    FebAccessInfoVector* getFebsBySector(int diskOrWheel, int sector,
            BarrelOrEndcap barrelOrEndcap) {

        XdaqSoapAccess::TSOAPParamList paramList;
        xdata::Integer xDiskOrWheel = diskOrWheel;
        xdata::Integer xSector = sector;
        xdata::String xBarrelOrEndcap = toString(barrelOrEndcap);
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "diskOrWheel", &xDiskOrWheel));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "sector", &xSector));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "barrelOrEndcap", &xBarrelOrEndcap));

        LOG4CPLUS_DEBUG(logger,
                "sending request to DB Service: getFebsBySector diskOrWheel = "
                        << diskOrWheel<< " sector= "<< sector
                        << " barrelOrEndcap = "<< (std::string)xBarrelOrEndcap);

        xoap::MessageReference getFebsResponse = soapAccess.sendSOAPRequest(
                "getFebsBySector", RPCT_DB_SERVICE_PREFIX, RPCT_DB_SERVICE_NS,
                paramList, "RpctDbService", 0, true);

        FebAccessInfoVector* febAccessInfoVector = new FebAccessInfoVector();
        soapAccess.parseSOAPResponse(getFebsResponse, "getFebsBySector",
                RPCT_DB_SERVICE_PREFIX, RPCT_DB_SERVICE_NS,
                *febAccessInfoVector);
        return febAccessInfoVector;
    }

    // !!! Caller should delete the result
    FebAccessInfoVector* getFebsByChamberLocation(int diskOrWheel, int layer,
            int sector, std::string subsector, BarrelOrEndcap barrelOrEndcap) {

        XdaqSoapAccess::TSOAPParamList paramList;
        xdata::Integer xDiskOrWheel = diskOrWheel;
        xdata::Integer xLayer = layer;
        xdata::Integer xSector = sector;
        xdata::String xSubsector = subsector;
        xdata::String xBarrelOrEndcap = toString(barrelOrEndcap);
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "diskOrWheel", &xDiskOrWheel));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type("layer",
                &xLayer));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "sector", &xSector));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "subsector", &xSubsector));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "barrelOrEndcap", &xBarrelOrEndcap));

        LOG4CPLUS_DEBUG(logger,
                "sending request to DB Service: getFebsByChamberLocation diskOrWheel = "
                        << diskOrWheel<< " layer = "<< layer << " sector= "
                        << sector << " subsector= "<< subsector
                        << " barrelOrEndcap = "<< (std::string)xBarrelOrEndcap);

        xoap::MessageReference getFebsResponse = soapAccess.sendSOAPRequest(
                "getFebsByChamberLocation", RPCT_DB_SERVICE_PREFIX,
                RPCT_DB_SERVICE_NS, paramList, "RpctDbService", 0, true);

        FebAccessInfoVector* febAccessInfoVector = new FebAccessInfoVector();
        soapAccess.parseSOAPResponse(getFebsResponse,
                "getFebsByChamberLocation", RPCT_DB_SERVICE_PREFIX,
                RPCT_DB_SERVICE_NS, *febAccessInfoVector);
        return febAccessInfoVector;
    }

    // !!! Caller should delete the result
    FebAccessInfoVector* getSingleFebByLocParameters(int diskOrWheel,
            int layer, int sector, std::string subsector, std::string barrelOrEndcap,
            std::string febLocalEtaPartition, int posInLocalEtaPartition) {

        XdaqSoapAccess::TSOAPParamList paramList;
        xdata::Integer xDiskOrWheel = diskOrWheel;
        xdata::Integer xLayer = layer;
        xdata::Integer xSector = sector;
        xdata::String xSubsector = subsector;
        xdata::String xBarrelOrEndcap = barrelOrEndcap;
        xdata::String xFebLocalEtaPartition = febLocalEtaPartition;
        xdata::Integer xPosInLocalEtaPartition = posInLocalEtaPartition;
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "diskOrWheel", &xDiskOrWheel));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type("layer",
                &xLayer));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "sector", &xSector));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "subsector", &xSubsector));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "barrelOrEndcap", &xBarrelOrEndcap));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "febLocalEtaPartition", &xFebLocalEtaPartition));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "posInLocalEtaPartition", &xPosInLocalEtaPartition));

        LOG4CPLUS_DEBUG(logger,
                "sending request to DB Service: getSingleFebByLocParameters diskOrWheel = "
                        << diskOrWheel<< " layer = "<< layer << " sector= "
                        << sector << " subsector= "<< subsector
                        << " barrelOrEndcap = "<< (std::string)xBarrelOrEndcap
                        << " febLocalEtaPartition = "
                        << (std::string)xFebLocalEtaPartition
                        << " posInLocalEtaPartition = "
                        << posInLocalEtaPartition);

        xoap::MessageReference getFebsResponse = soapAccess.sendSOAPRequest(
                "getSingleFebByLocParameters", RPCT_DB_SERVICE_PREFIX,
                RPCT_DB_SERVICE_NS, paramList, "RpctDbService", 0, true);

        FebAccessInfoVector* febAccessInfoVector = new FebAccessInfoVector();
        soapAccess.parseSOAPResponse(getFebsResponse,
                "getSingleFebByLocParameters", RPCT_DB_SERVICE_PREFIX,
                RPCT_DB_SERVICE_NS, *febAccessInfoVector);
        return febAccessInfoVector;
    }

    // !!! Caller should delete the result
    ChamStripAccessInfoVector* getChamStripByChamLoc(int diskOrWheel,
            int layer, int sector, std::string subsector,
            BarrelOrEndcap barrelOrEndcap) {

        XdaqSoapAccess::TSOAPParamList paramList;
        xdata::Integer xDiskOrWheel = diskOrWheel;
        xdata::Integer xLayer = layer;
        xdata::Integer xSector = sector;
        xdata::String xSubsector = subsector;
        xdata::String xBarrelOrEndcap = toString(barrelOrEndcap);
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "diskOrWheel", &xDiskOrWheel));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type("layer",
                &xLayer));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "sector", &xSector));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "subsector", &xSubsector));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "barrelOrEndcap", &xBarrelOrEndcap));

        LOG4CPLUS_DEBUG(logger,
                "sending request to DB Service: getChamStripByChamLoc diskOrWheel = "
                        << diskOrWheel<< " layer = "<< layer << " sector= "
                        << sector << " subsector= "<< subsector
                        << " barrelOrEndcap = "<< (std::string)xBarrelOrEndcap);

        xoap::MessageReference
                getChamStripResponse = soapAccess.sendSOAPRequest(
                        "getChamStripByChamLoc", RPCT_DB_SERVICE_PREFIX,
                        RPCT_DB_SERVICE_NS, paramList, "RpctDbService", 0, true);

        LOG4CPLUS_DEBUG(logger,
                "received response from DB Service");

        ChamStripAccessInfoVector
                * chamStripAccessInfoVector = new ChamStripAccessInfoVector();
        soapAccess.parseSOAPResponse(getChamStripResponse,
                "getChamStripByChamLoc", RPCT_DB_SERVICE_PREFIX,
                RPCT_DB_SERVICE_NS, *chamStripAccessInfoVector);
        return chamStripAccessInfoVector;
    }

    // !!! Caller should delete the result
    FebConnectorStripsVector * getFebConnectorStrips(std::string const & _host, int _port);
    DTLineFebChipFlatInfoVector * getDTLineFebChipFlatInfo(int _wheel);

    // !!! Caller should delete the result
    FebAccessInfoVector* getFebsByLocationId(int locationId) {

        XdaqSoapAccess::TSOAPParamList paramList;
        xdata::Integer xLocationId = locationId;
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "locationId", &xLocationId));

        LOG4CPLUS_DEBUG(logger,
                "sending request to DB Service: getFebsByLocationId");

        xoap::MessageReference getFebsResponse = soapAccess.sendSOAPRequest(
                "getFebsByLocationId", RPCT_DB_SERVICE_PREFIX,
                RPCT_DB_SERVICE_NS, paramList, "RpctDbService", 0, true);

        LOG4CPLUS_DEBUG(logger,
                "received response from DB Service");

        FebAccessInfoVector* febAccessInfoVector = new FebAccessInfoVector();
        soapAccess.parseSOAPResponse(getFebsResponse, "getFebsByLocationId",
                RPCT_DB_SERVICE_PREFIX, RPCT_DB_SERVICE_NS,
                *febAccessInfoVector);
        return febAccessInfoVector;
    }


    // !!! Caller should delete the result
    RbcInfoVector* getAllRbcsInfo() {

        XdaqSoapAccess::TSOAPParamList paramList;
        LOG4CPLUS_DEBUG(logger,
                "sending request to DB Service: getAllRbcsInfo");

        xoap::MessageReference getRbcsResponse = soapAccess.sendSOAPRequest(
                "getAllRbcsInfo", RPCT_DB_SERVICE_PREFIX,
                RPCT_DB_SERVICE_NS, paramList, "RpctDbService", 0, true);

        LOG4CPLUS_DEBUG(logger,
                "received response from DB Service");

        RbcInfoVector* rbcInfoVector = new RbcInfoVector();
        soapAccess.parseSOAPResponse(getRbcsResponse, "getAllRbcsInfo",
                RPCT_DB_SERVICE_PREFIX, RPCT_DB_SERVICE_NS,
                *rbcInfoVector);
        return rbcInfoVector;
    }

    // !!! Caller should delete the result
    // Filip Thyssen
    FebChipInfoVector * getFebChipInfoVector()
    {
    	LOG4CPLUS_INFO(logger, __FUNCTION__<<":"<<__LINE__<<" started");
        XdaqSoapAccess::TSOAPParamList paramList;

        //LOG4CPLUS_INFO(logger, __FUNCTION__<<":"<<__LINE__<<"sending request to DB Service: getFebChipInfoVector");
        xoap::MessageReference getResponse =
            soapAccess.sendSOAPRequest("getFebChipInfoVector", RPCT_DB_SERVICE_PREFIX,
                                       RPCT_DB_SERVICE_NS, paramList, "RpctDbService", 0, true);
        LOG4CPLUS_INFO(logger, __FUNCTION__<<":"<<__LINE__<<" getFebChipInfoVector SOAP done");
        
        FebChipInfoVector * febChipInfoVector = new FebChipInfoVector();
        soapAccess.parseSOAPResponse(getResponse, "getFebChipInfoVector",
                                     RPCT_DB_SERVICE_PREFIX, RPCT_DB_SERVICE_NS,
                                     *febChipInfoVector);
        LOG4CPLUS_WARN(logger, __FUNCTION__<<":"<<__LINE__<<" finished");
        return febChipInfoVector;
    }

    // !!! Caller should delete the result
    CcuRingInfoBag* getCcuRingInfoForXdaqApp(std::string host, int port,
            int instance) {

        XdaqSoapAccess::TSOAPParamList paramList;
        xdata::String xHost = host;
        xdata::Integer xPort = port;
        xdata::Integer xInstance = instance;
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type("host",
                &xHost));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type("port",
                &xPort));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "instance", &xInstance));

        LOG4CPLUS_DEBUG(logger,
                "sending request to DB Service: getCcuRingInfoForXdaqApp");

        xoap::MessageReference
                getCcuRingInfoForXdaqAppResponse = soapAccess.sendSOAPRequest(
                        "getCcuRingInfoForXdaqApp", RPCT_DB_SERVICE_PREFIX,
                        RPCT_DB_SERVICE_NS, paramList, "RpctDbService", 0, true);

        LOG4CPLUS_DEBUG(logger,
                "received response from DB Service");

        CcuRingInfoBag* ccuRingInfoBag = new CcuRingInfoBag();
        soapAccess.parseSOAPResponse(getCcuRingInfoForXdaqAppResponse,
                "getCcuRingInfoForXdaqApp", RPCT_DB_SERVICE_PREFIX,
                RPCT_DB_SERVICE_NS, *ccuRingInfoBag);
        return ccuRingInfoBag;
    }

    // !!! Caller should delete the result
    VmeCrateInfoBag* getVmeCrateInfo(std::string host, int port,
            std::string className, int instance) {

        XdaqSoapAccess::TSOAPParamList paramList;
        xdata::String xHost = host;
        xdata::Integer xPort = port;
        xdata::String xClassName = className;
        xdata::Integer xInstance = instance;
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type("host",
                &xHost));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type("port",
                &xPort));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "className", &xClassName));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "instance", &xInstance));

        LOG4CPLUS_DEBUG(logger,
                "sending request to DB Service: getVmeCrateInfo");

        xoap::MessageReference
                getTriggerCrateInfoResponse = soapAccess.sendSOAPRequest(
                        "getVmeCrateInfo", RPCT_DB_SERVICE_PREFIX,
                        RPCT_DB_SERVICE_NS, paramList, "RpctDbService", 0, true);

        LOG4CPLUS_DEBUG(logger,
                "received response from DB Service");

        VmeCrateInfoBag* vmeCrateInfoBag = new VmeCrateInfoBag();
        soapAccess.parseSOAPResponse(getTriggerCrateInfoResponse,
                "getVmeCrateInfo", RPCT_DB_SERVICE_PREFIX, RPCT_DB_SERVICE_NS,
                *vmeCrateInfoBag);
        return vmeCrateInfoBag;
    }

    // !!! Caller should delete the result
    TriggerCrateInfoBag* getTriggerCrateInfo(std::string host, int port,
            std::string className, int instance) {

        XdaqSoapAccess::TSOAPParamList paramList;
        xdata::String xHost = host;
        xdata::Integer xPort = port;
        xdata::String xClassName = className;
        xdata::Integer xInstance = instance;
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type("host",
                &xHost));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type("port",
                &xPort));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "className", &xClassName));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "instance", &xInstance));

        LOG4CPLUS_DEBUG(logger,
                "sending request to DB Service: getTriggerCrateInfo");

        xoap::MessageReference
                getTriggerCrateInfoResponse = soapAccess.sendSOAPRequest(
                        "getTriggerCrateInfo", RPCT_DB_SERVICE_PREFIX,
                        RPCT_DB_SERVICE_NS, paramList, "RpctDbService", 0, true);

        LOG4CPLUS_DEBUG(logger,
                "received response from DB Service");

        TriggerCrateInfoBag* triggerCrateInfoBag = new TriggerCrateInfoBag();
        soapAccess.parseSOAPResponse(getTriggerCrateInfoResponse,
                "getTriggerCrateInfo", RPCT_DB_SERVICE_PREFIX,
                RPCT_DB_SERVICE_NS, *triggerCrateInfoBag);
        return triggerCrateInfoBag;
    }

    ConfigurationSetBagPtr getConfigurationSetForLocalConfigKey(xdata::Vector<xdata::Integer> & xChipIds
                                                                , xdata::String & xLocalConfigKey) {
        XdaqSoapAccess::TSOAPParamList paramList;

        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "chipIds", &xChipIds));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "localConfigKey", &xLocalConfigKey));

        LOG4CPLUS_DEBUG(logger,
                "sending request to DB Service: getConfigurationSetForLocalConfigKey");

        xoap::MessageReference
                getConfigurationSetResponse = soapAccess.sendSOAPRequest(
                        "getConfigurationSetForLocalConfigKey", RPCT_DB_SERVICE_PREFIX,
                        RPCT_DB_SERVICE_NS, paramList, "RpctDbService", 0, true);

        LOG4CPLUS_DEBUG(logger,
                "received response from DB Service");

        ConfigurationSetBagPtr configurationSetBag(new ConfigurationSetBag());
        soapAccess.parseSOAPResponse(getConfigurationSetResponse,
                "getConfigurationSetForLocalConfigKey", RPCT_DB_SERVICE_PREFIX,
                RPCT_DB_SERVICE_NS, *configurationSetBag);
        return configurationSetBag;
    }
    ConfigurationSetBagPtr getConfigurationSetForLocalConfigKey(const std::vector<int> & chipIds
                                                                , const std::string & localConfigKey) {
        xdata::Vector<xdata::Integer> xChipIds;
        xChipIds.reserve(chipIds.size());
        for (std::vector<int>::const_iterator iChipId = chipIds.begin(); iChipId
                != chipIds.end(); ++iChipId) {
            xChipIds.push_back(xdata::Integer(*iChipId));
        }
        xdata::String xLocalConfigKey = localConfigKey;
        return getConfigurationSetForLocalConfigKey(xChipIds, xLocalConfigKey);
    }

    ConfigurationSetBagPtr getConfigurationSetForLocalConfigKeys(xdata::Vector<xdata::Integer> & xChipIds
                                                                 , xdata::Vector<xdata::String> & xLocalConfigKeys) {
        XdaqSoapAccess::TSOAPParamList paramList;

        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "chipIds", &xChipIds));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "localConfigKeys", &xLocalConfigKeys));

        LOG4CPLUS_DEBUG(logger,
                "sending request to DB Service: getConfigurationSetForLocalConfigKeys");

        xoap::MessageReference
                getConfigurationSetResponse = soapAccess.sendSOAPRequest(
                        "getConfigurationSetForLocalConfigKeys", RPCT_DB_SERVICE_PREFIX,
                        RPCT_DB_SERVICE_NS, paramList, "RpctDbService", 0, true);

        LOG4CPLUS_DEBUG(logger,
                "received response from DB Service");

        ConfigurationSetBagPtr configurationSetBag(new ConfigurationSetBag());
        soapAccess.parseSOAPResponse(getConfigurationSetResponse,
                "getConfigurationSetForLocalConfigKeys", RPCT_DB_SERVICE_PREFIX,
                RPCT_DB_SERVICE_NS, *configurationSetBag);
        return configurationSetBag;
    }
    ConfigurationSetBagPtr getConfigurationSetForLocalConfigKeys(const std::vector<int> & chipIds
                                                                 , const std::vector<std::string> & localConfigKeys) {
        xdata::Vector<xdata::Integer> xChipIds;
        xChipIds.reserve(chipIds.size());
        for (std::vector<int>::const_iterator iChipId = chipIds.begin(); iChipId
                != chipIds.end(); ++iChipId) {
            xChipIds.push_back(xdata::Integer(*iChipId));
        }

        xdata::Vector<xdata::String> xLocalConfigKeys;
        xLocalConfigKeys.reserve(localConfigKeys.size());
        for (std::vector<std::string>::const_iterator iLocalConfigKey = localConfigKeys.begin(); iLocalConfigKey
                        != localConfigKeys.end(); ++iLocalConfigKey) {
        	xdata::String xLocalConfigKey = *iLocalConfigKey;
        	xLocalConfigKeys.push_back(xLocalConfigKey);
        }
        
        return getConfigurationSetForLocalConfigKeys(xChipIds, xLocalConfigKeys);
    }

    ConfigurationSetBagPtr getConfigurationSet(xdata::Vector<xdata::Integer> & xChipIds
                                               , xdata::String & xGlobalConfigKey) {
        XdaqSoapAccess::TSOAPParamList paramList;
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "chipIds", &xChipIds));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "globalConfigKey", &xGlobalConfigKey));

        LOG4CPLUS_DEBUG(logger,
                "sending request to DB Service: getConfigurationSet");

        xoap::MessageReference
                getConfigurationSetResponse = soapAccess.sendSOAPRequest(
                        "getConfigurationSet", RPCT_DB_SERVICE_PREFIX,
                        RPCT_DB_SERVICE_NS, paramList, "RpctDbService", 0, true);

        LOG4CPLUS_DEBUG(logger,
                "received response from DB Service");

        ConfigurationSetBagPtr configurationSetBag(new ConfigurationSetBag());
        soapAccess.parseSOAPResponse(getConfigurationSetResponse,
                "getConfigurationSet", RPCT_DB_SERVICE_PREFIX,
                RPCT_DB_SERVICE_NS, *configurationSetBag);
        return configurationSetBag;
    }
    ConfigurationSetBagPtr getConfigurationSet(const std::vector<int> & chipIds
                                               , const std::string & globalConfigKey) {
        xdata::Vector<xdata::Integer> xChipIds;
        xChipIds.reserve(chipIds.size());
        for (std::vector<int>::const_iterator iChipId = chipIds.begin(); iChipId
                != chipIds.end(); ++iChipId) {
            xChipIds.push_back(xdata::Integer(*iChipId));
        }
        xdata::String xGlobalConfigKey = globalConfigKey;
        
        return getConfigurationSet(xChipIds, xGlobalConfigKey);
    }

    xdata::Vector<xdata::String> getLocalConfigKeys(std::string subsystem) {

        LOG4CPLUS_DEBUG(logger,
        "sending request to DB Service: getLocalConfigKeys");

        XdaqSoapAccess::TSOAPParamList paramList;
        xdata::String xSubsystem = subsystem;
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                        "subsystem", &xSubsystem));

        xoap::MessageReference getLocalConfigKeysResponse = soapAccess.sendSOAPRequest(
                "getLocalConfigKeys", RPCT_DB_SERVICE_PREFIX,
                RPCT_DB_SERVICE_NS, paramList, "RpctDbService", 0, true);

        xdata::Vector<xdata::String> response;
        soapAccess.parseSOAPResponse(getLocalConfigKeysResponse, "getLocalConfigKeys",
                RPCT_DB_SERVICE_PREFIX, RPCT_DB_SERVICE_NS, response);
        return response;
    }

/* MichalS */
    void writeCurrentKeyValue(std::string currentKeyValue) {

        LOG4CPLUS_DEBUG(logger,
        "sending request to DB Service: writeCurrentKeyValue");

        XdaqSoapAccess::TSOAPParamList paramList;
        xdata::String xKeyValue = currentKeyValue;
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "currentKeyValue", &xKeyValue));
        soapAccess.sendSOAPRequest(
                "writeCurrentKeyValue", RPCT_DB_SERVICE_PREFIX,
                RPCT_DB_SERVICE_NS, paramList, "RpctDbService", 0, true);

    }
/* MichalS */

    xdata::Integer setStripResponseValues(/*Integer id,*/int chamberStrip,
            int count, int run, int unixTime) {

        XdaqSoapAccess::TSOAPParamList paramList;
        xdata::Integer xChamberStrip = chamberStrip;
        xdata::Integer xCount = count;
        xdata::Integer xRun = run;
        xdata::Integer xUnixTime = unixTime;
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "chamberStrip", &xChamberStrip));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type("count",
                &xCount));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type("run",
                &xRun));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "unixTime", &xUnixTime));

        LOG4CPLUS_DEBUG(logger,
                "sending request to DB Service: setStripResponseValues: chamberStripId = "
                        << chamberStrip<< " count = "<< count << " run= "<< run
                        << " unixTime= "<< unixTime);

        xoap::MessageReference getFebsResponse = soapAccess.sendSOAPRequest(
                "setStripResponseValues", RPCT_DB_SERVICE_PREFIX,
                RPCT_DB_SERVICE_NS, paramList, "RpctDbService", 0, true);

        xdata::Integer response;
        soapAccess.parseSOAPResponse(getFebsResponse, "setStripResponseValues",
                RPCT_DB_SERVICE_PREFIX, RPCT_DB_SERVICE_NS, response);
        return response;
    }

    xdata::Integer setFebConditionVaules(/*Integer id,*/int febLocation,
            int run, int th1, int th2, int th3, int th4, int vmon1, int vmon2,
            int vmon3, int vmon4, int unixTime) {

        XdaqSoapAccess::TSOAPParamList paramList;
        xdata::Integer xFebLocation = febLocation;
        xdata::Integer xRun = run;
        xdata::Integer xTh1 = th1;
        xdata::Integer xTh2 = th2;
        xdata::Integer xTh3 = th3;
        xdata::Integer xTh4 = th4;
        xdata::Integer xVmon1 = vmon1;
        xdata::Integer xVmon2 = vmon2;
        xdata::Integer xVmon3 = vmon3;
        xdata::Integer xVmon4 = vmon4;
        xdata::Integer xUnixTime = unixTime;
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "febLocation", &xFebLocation));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type("run",
                &xRun));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type("th1",
                &xTh1));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type("th2",
                &xTh2));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type("th3",
                &xTh3));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type("th4",
                &xTh4));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type("vmon1",
                &xVmon1));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type("vmon2",
                &xVmon2));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type("vmon3",
                &xVmon3));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type("vmon4",
                &xVmon4));
        paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
                "unixTime", &xUnixTime));

        LOG4CPLUS_DEBUG(logger,
                "sending request to DB Service: setStripResponseValues: febLocation = "
                        << febLocation<< " run= "<< run << " th1 = "<< th1
                        << " th2 = "<< th2<< " th3 = "<< th3 << " th4 = "<< th4
                        << " vmon1 = "<< vmon1 << " vmon2 = "<< vmon2
                        << " vmon3 = "<< vmon3 << " vmon4 = "<< vmon4
                        << "unixTime= "<< unixTime);

        xoap::MessageReference getFebsResponse = soapAccess.sendSOAPRequest(
                "setFebConditionVaules", RPCT_DB_SERVICE_PREFIX,
                RPCT_DB_SERVICE_NS, paramList, "RpctDbService", 0, true);

        xdata::Integer response;
        soapAccess.parseSOAPResponse(getFebsResponse, "setFebConditionVaules",
                RPCT_DB_SERVICE_PREFIX, RPCT_DB_SERVICE_NS, response);
        return response;
    }

    // Interconnection service
    // !!! Caller should delete the result
    void synchronizeLinks() {

        XdaqSoapAccess::TSOAPParamList paramList;
        LOG4CPLUS_DEBUG(logger,
                "sending request to IService: synchronizeLinks");

        xoap::MessageReference synchronizeLinksResponse = soapAccess.sendSOAPRequest(
                "synchronizeLinks", RPCT_DB_SERVICE_PREFIX,
                RPCT_DB_SERVICE_NS, paramList, "RpctDbService", 0, true);

        LOG4CPLUS_DEBUG(logger,
                "received response from I Service");

        xdata::Integer response;
        soapAccess.parseSOAPResponse(synchronizeLinksResponse, "synchronizeLinks",
                RPCT_DB_SERVICE_PREFIX, RPCT_DB_SERVICE_NS);
    }
/*
 * The ones below do no work, and never did.
    xdata::Integer putSynCoderConfInFlashes(ConfigurationSetImplXData::SynCoderConfInFlashVector& synCoderConfInFlashes) {
    	XdaqSoapAccess::TSOAPParamList paramList;
    	paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
    			"synCoderConfInFlashes", &synCoderConfInFlashes));

    	LOG4CPLUS_DEBUG(logger,
    			"sending request to DB Service: putSynCoderConfInFlashes");

    	xoap::MessageReference messageResponse = soapAccess.sendSOAPRequest(
    			"putSynCoderConfInFlashes", RPCT_DB_SERVICE_PREFIX,
    			RPCT_DB_SERVICE_NS, paramList, "RpctDbService", 0, true);

    	xdata::Integer response;
    	soapAccess.parseSOAPResponse(messageResponse, "putSynCoderConfInFlashes",
    			RPCT_DB_SERVICE_PREFIX, RPCT_DB_SERVICE_NS, response);

    	LOG4CPLUS_INFO(logger,
    	    			"putSynCoderConfInFlashes executed successfully, synCoderConfInFlashe updated for "
    					<<synCoderConfInFlashes.size() <<" devices. messageResponse: "<<response);
    	return response;
    }

    xdata::Integer putSynCoderConfInFlash(xdata::Bag<SynCoderConfInFlash>& synCoderConfInFlash) {
    	XdaqSoapAccess::TSOAPParamList paramList;
    	paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
    			"synCoderConfInFlash", &synCoderConfInFlash));

    	LOG4CPLUS_DEBUG(logger,
    			"sending request to DB Service: putSynCoderConfInFlash");

    	xoap::MessageReference messageResponse = soapAccess.sendSOAPRequest(
    			"putSynCoderConfInFlash", RPCT_DB_SERVICE_PREFIX,
    			RPCT_DB_SERVICE_NS, paramList, "RpctDbService", 0, true);

    	xdata::Integer response;
    	soapAccess.parseSOAPResponse(messageResponse, "putSynCoderConfInFlash",
    			RPCT_DB_SERVICE_PREFIX, RPCT_DB_SERVICE_NS, response);

    	LOG4CPLUS_INFO(logger,
    	    			"putSynCoderConfInFlash executed successfully. messageResponse: "<<response);
    	return response;
    }
    */

    xdata::Integer putSynCoderConfInFlash(ISynCoderConfInFlash& synCoderConfInFlash) {
    	XdaqSoapAccess::TSOAPParamList paramList;

        xdata::Integer xchipId = synCoderConfInFlash.getChipId();
        xdata::Integer xConfId = synCoderConfInFlash.getSynCoderConfId();
        xdata::Integer xBegAddr = synCoderConfInFlash.getBeginAddress();
        xdata::Integer xEndAddr = synCoderConfInFlash.getEndAddress();

    	paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
    			"chipId", &xchipId));
    	paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
    			"confId", &xConfId));
    	paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
    			"begAddr", &xBegAddr));
    	paramList.push_back(XdaqSoapAccess::TSOAPParamList::value_type(
    			"endAddr", &xEndAddr));

    	LOG4CPLUS_DEBUG(logger,
    			"sending request to DB Service: putSynCoderConfInFlash");

    	xoap::MessageReference messageResponse = soapAccess.sendSOAPRequest(
    			"putSynCoderConfInFlash", RPCT_DB_SERVICE_PREFIX,
    			RPCT_DB_SERVICE_NS, paramList, "RpctDbService", 0, true);

    	xdata::Integer response;
    	soapAccess.parseSOAPResponse(messageResponse, "putSynCoderConfInFlash",
    			RPCT_DB_SERVICE_PREFIX, RPCT_DB_SERVICE_NS, response);

    	LOG4CPLUS_INFO(logger,
    	    			"putSynCoderConfInFlash executed successfully. messageResponse: "<<response);
    	return response;
    }

};

}
}

#endif
