/** Filip Thyssen */

#ifndef rpct_xdaqutils_FebChipConfiguration_inl_h_
#define rpct_xdaqutils_FebChipConfiguration_inl_h_

#include "rpct/xdaqutils/FebChipConfiguration.h"

namespace rpct {
namespace xdaqutils {

inline int FebChipConfiguration::getConfigurationId()
{
    return id_.value_;
}

inline void FebChipConfiguration::setId(int id)
{
    id_ = id;
}
inline int FebChipConfiguration::getId() const
{
    return id_.value_;
}

inline void FebChipConfiguration::setVTH(unsigned int vth)
{
    vth_ = vth;
}
inline void FebChipConfiguration::setVMON(unsigned int vmon)
{
    vmon_ = vmon;
}
inline void FebChipConfiguration::setVTHOffset(int vth_offset)
{
    vth_offset_ = vth_offset;
}
inline void FebChipConfiguration::setVMONOffset(int vmon_offset)
{
    vmon_offset_ = vmon_offset;
}

inline unsigned int FebChipConfiguration::getVTH() const
{
    return (vth_.value_ > 0 ? vth_.value_ : 0);
}
inline unsigned int FebChipConfiguration::getVMON() const
{
    return (vmon_.value_ > 0 ? vmon_.value_ : 0);
}
inline int FebChipConfiguration::getVTHOffset() const
{
    return vth_offset_.value_;
}
inline int FebChipConfiguration::getVMONOffset() const
{
    return vmon_offset_.value_;
}

} // namespace xdaqutils
} // namespace rpct

#endif // rpct_xdaqutils_FebChipConfiguration_inl_h_
