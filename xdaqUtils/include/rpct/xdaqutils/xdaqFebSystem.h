/** Filip Thyssen */

#ifndef rpct_xdaqutils_xdaqFebSystem_h_
#define rpct_xdaqutils_xdaqFebSystem_h_

#include <string>
#include <vector>

#include <log4cplus/logger.h>

#include "rpct/devices/FebSystem.h"
#include "rpct/xdaqtools/publisher/Client.h"
#include "rpct/xdaqtools/xgitools.h"

#include "rpct/xdaqutils/const.h"

namespace toolbox {
class BSem;
namespace task {
class WorkLoop;
} // namespace task
} // namespace toolbox

namespace rpct {
namespace xdaqtools {
namespace remotelogger {
class Client;
} // remotelogger
} // namespace xdaqtools

namespace xdaqutils {

class XdaqDbServiceClient;

class xdaqFebSystem
    : public virtual rpct::FebSystem
    , public rpct::xdaqtools::publisher::Client
{
protected:
    bool configure_wl(toolbox::task::WorkLoop *);
    bool loadconfiguration_wl(toolbox::task::WorkLoop *);
    bool monitor_wl(toolbox::task::WorkLoop *);

public:
    static void setupLogger(std::string namePrefix);
    xdaqFebSystem(xdaq::Application & application
                  , toolbox::BSem & hwmutex
                  , const std::string & publisher_service_name = std::string(febpublisher::service_name_)
                  , unsigned int publisher_service_instance = febpublisher::service_instance_
                  , size_t max_size = rpct::xdaqtools::publisher::preset::client_itemlist_size_);
    ~xdaqFebSystem();

    bool xconfigure(const std::string & configkey);
    bool xconfigure_attach(const std::string & configkey);
    bool xloadConfiguration(const std::string & configkey);
    bool xloadConfiguration_attach(const std::string & configkey);
    bool xmonitor();
    bool xmonitor_attach();
    bool xmonitorcycle();

    void xgi_interface(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

    void publish(int id, int type, double value);
    
    std::string getStatusSummary() const;

    void pause();
    void resume();
    void stop();
    void addMethod(toolbox::task::ActionSignature * , const std::string &) {}


protected:
    toolbox::task::ActionSignature * configure_action_, * loadconfiguration_action_, * monitor_action_;
    std::string configkey_; // write protected by pause_
    toolbox::task::WorkLoop * actionwl_;

    XdaqDbServiceClient * dbclient_;

    std::string statussummary_; // write protected by pause_
    std::vector<std::string> configurationkeys_;

    toolbox::BSem & hwmutex_;

    mutable rpct::tools::Mutex mutex_, interrupt_sync_;
    volatile bool paused_, stopped_, running_; // protected by pause_
    mutable rpct::tools::Condition pause_;

    //rpct::xdaqtools::remotelogger::Client * rlclient_;
    SharedAppenderPtr  rlclient_;
private:
    static log4cplus::Logger remote_logger_;
    static log4cplus::Logger logger_;
};

inline void xdaqFebSystem::publish(int id, int type, double value)
{
    rpct::xdaqtools::publisher::Client::publish(id, type, value);
}

} // namespace xdaqutils
} // namespace rpct

#endif // rpct_xdaqutils_xdaqFebSystem_h_
