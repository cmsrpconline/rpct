#ifndef _RPCTFEBSTATEINFO_H_
#define _RPCTFEBSTATEINFO_H_


#include "xdata/Bag.h"
#include "xdata/Integer.h"
#include "xdata/Float.h"
#include "xdata/String.h"
#include "xdata/UnsignedLong.h"
namespace rpct {

class FebStateInfo {
private:
    xdata::String febLocalEtaPartition;
    xdata::String chamberLocationName;
    xdata::Integer ccuAddress;
    xdata::Integer channel;
    xdata::Integer address;
    xdata::Float tempValue;
    xdata::Float th1Value;
    xdata::Float th2Value;
    xdata::Float vmon1Value;
    xdata::Float vmon2Value;
    xdata::String executionTime;
    xdata::UnsignedLong executionUnixTime;
    
    
public:
    void registerFields(xdata::Bag<FebStateInfo>* bag) {
        bag->addField("febLocalEtaPartition", &febLocalEtaPartition);
        bag->addField("chamberLocationName", &chamberLocationName);
        bag->addField("ccuAddress", &ccuAddress);
        bag->addField("channel", &channel);
        bag->addField("address", &address);
        bag->addField("tempValue", &tempValue);
        bag->addField("th1Value", &th1Value);
        bag->addField("th2Value", &th2Value);
        bag->addField("vmon1Value", &vmon1Value);
        bag->addField("vmon2Value", &vmon2Value);
        bag->addField("executionTime", &executionTime);
        bag->addField("executionUnixTime", &executionUnixTime);
        
    }
    
    xdata::String& getFebLocalEtaPartition() {
        return febLocalEtaPartition;
    }
    
    void setFebLocalEtaPartition(xdata::String febLocalEtaPartition) {
        this->febLocalEtaPartition = febLocalEtaPartition;
    }
    
    xdata::String& getChamberLocationName() {
        return chamberLocationName;
    }
    
    void setChamberLocationName(xdata::String chamberLocationName) {
        this->chamberLocationName = chamberLocationName;
    }
    
     xdata::Integer& getCcuAddress() {
        return ccuAddress;
    }
    
    void setCcuAddress(xdata::Integer ccuAddress) {
        this->ccuAddress = ccuAddress;
    }
    
    xdata::Integer& getChannel() {
        return channel;
    }
    
    void setChannel(xdata::Integer channel) {
        this->channel = channel;
    }
    
    xdata::Integer& getAddress() {
        return address;
    }
    
    void setAddress(xdata::Integer address) {
        this->address = address;
    }
     xdata::Float& getTempValue() {
        return tempValue;
    }
    
    void setTempValue(xdata::Float tempValue) {
        this->tempValue = tempValue;
    }
     xdata::Float& getTh1Value() {
        return th1Value;
    }
    
    void setTh1Value(xdata::Float th1Value) {
        this->th1Value = th1Value;
    }
     xdata::Float& getTh2Value() {
        return th2Value;
    }
    
    void setTh2Value(xdata::Float th2Value) {
        this->th2Value = th2Value;
    }
     xdata::Float& getVmon1Value() {
        return vmon1Value;
    }
    
    void setVmon1Value(xdata::Float vmon1Value) {
        this->vmon1Value = vmon1Value;
    }
     xdata::Float& getVmon2Value() {
        return vmon2Value;
    }
    
    void setVmon2Value(xdata::Float vmon2Value) {
        this->vmon2Value = vmon2Value;
    }
    
    xdata::String& getExecutionTime() {
        return executionTime;
    }
    
    void setExecutionTime(xdata::String executionTime) {
        this->executionTime = executionTime;
    }
    
    xdata::UnsignedLong& getExecutionUnixTime() {
        return executionUnixTime;
    }
    
    void setExecutionUnixTime(xdata::UnsignedLong executionUnixTime) {
        this->executionUnixTime = executionUnixTime;
    }
};

}

#endif /*_RPCTFEBSTATEINFO_H_*/
