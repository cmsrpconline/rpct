# $Id: Makefile,v 1.44 2006/05/08 13:07:01 xdaq Exp $
XDAQ_BACK_TO_ROOT:=..

include $(XDAQ_ROOT)/config/mfAutoconf.rules

Project=rpct

include $(XDAQ_ROOT)/config/mfDefs.$(XDAQ_OS)

#Packages=tbstd ii i2c lboxAccess diag bs/bslib devices RPCCCU aboot2 hardwareTests programs xdaqUtils xdaqIIAccess xdaqDiagAccess xdaqLBoxAccess xdaqTestBench xdaqRpcConfig xdaqRpcMonitor
Packages=tbstd
Packages+=tools
Packages+=xdaqtools
Packages+=ii
Packages+=i2c
Packages+=diag
Packages+=bs/bslib
Packages+=devices
Packages+=lboxAccess
Packages+=aboot2
Packages+=RPCCCU
Packages+=hardwareTests
#Packages+=programs
Packages+=rpctXdata
Packages+=xdaqDiagAccess
#Packages+=xdaqIIAccess
Packages+=xdaqUtils
#Packages+=xdaqLBoxAccess
#Packages+=xdaqTestBench
#Packages+=xdaqHardwareAccess
#Packages+=xdaqRpcConfig
Packages+=xdaqDccAccess
#Packages+=xdaqRpcMonitor
#Packages+=xdaqFebBgMonitor
Packages+=xdaqFebPublisher
#Packages+=jamplayer
#Packages+=bs/jp_22
Packages+=xsvf
Packages+=hwd
Packages+=FebConnectivityTest
Packages+=processcontrol

.PHONY: cleanBinary
cleanBinary:
	$(foreach PATH, $(Packages), make -C $(PATH) cleanBinary;)


ifeq ($(Set), extern)
Packages=\
	extern/i2o \
	extern/xerces \
	extern/asyncresolv \
	extern/log4cplus \
	extern/log4cplus/udpappender \
	extern/log4cplus/xmlappender \
	extern/cgicc \
	extern/mimetic \
	extern/tinyproxy \
	extern/cppunit \
	extern/gmp
endif

ifeq ($(Set), coretools)
Packages=\
	xcept \
	toolbox \
	xoap \
	xdata \
	xgi \
	i2o \
	i2o/utils \
	pt \
	pt/http \
	pt/fifo \
	xdaq \
	xdaq/executive \
	xdaq/hyperdaq \
	xrelay
endif

ifeq ($(Set), powerpack)
Packages=\
	pt/tcp \
	benchmark/roundtrip \
	benchmark/mstreamio \
	xaccess \
	extern/slp \
	sentinel \
	sentinel/examples \
	xmem \
	xplore \
	xplore/xslp \
	xplore/xplore \
	monitor \
	multiview \
	extern/ssh \
	xact
endif

ifeq ($(Set), worksuite)
Packages=\
	extern/chartdir \
	dmon \
	pt/atcp \
	../RunControl/tools/xdaq2rc \
	evb/bu \
        evb/evm \
        evb/ru \
        evb/examples/fu \
        evb/examples/rui \
        evb/examples/ta \
        evb/rubuildertester \
        jobcontrol 
endif

include $(XDAQ_ROOT)/config/Makefile.rules
