/** Filip Thyssen */

#ifndef rpct_ts_common_ProgressBar_inl_h_
#define rpct_ts_common_ProgressBar_inl_h_

#include "rpct/ts/common/ProgressBar.h"

namespace rpct {



inline rpct::tools::Progress const & ProgressBar::getProgress() const
{
    return progress_;
}

inline rpct::tools::Progress & ProgressBar::getProgress()
{
    return progress_;
}

inline bool ProgressBar::showEndEstimate() const
{
    return show_end_estimate_;
}

inline void ProgressBar::showEndEstimate(bool _show_end_estimate)
{
    show_end_estimate_ = _show_end_estimate;
}

inline ProgressBar const & ProgressBarWidget::getProgressBar() const
{
    return *progressbar_;
}

inline ProgressBar & ProgressBarWidget::getProgressBar()
{
    return *progressbar_;
}

inline rpct::tools::Progress const & ProgressBarWidget::getProgress() const
{
    return progressbar_->getProgress();
}

inline rpct::tools::Progress & ProgressBarWidget::getProgress()
{
    return progressbar_->getProgress();
}

inline ajax::ResultBox & ProgressBarWidget::getResultBox()
{
    return *resultbox_;
}

} // namespace rpct

#endif // rpct_ts_common_ProgressBar_inl_h_
