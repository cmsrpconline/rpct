/** Filip Thyssen */

#ifndef rpct_ts_common_LoggerWidget_h_
#define rpct_ts_common_LoggerWidget_h_

#include <string>
#include <map>
#include <deque>

#include "time.h"

#include "log4cplus/logger.h"
#include "log4cplus/appender.h"

#include "ajax/Widget.h"
#include "ajax/AutoHandledWidget.h"

#include "rpct/tools/State.h"
#include "rpct/tools/RWMutex.h"

namespace ajax {
class Div;
class ResultBox;
class PlainHtml;
} // namespace ajax

namespace tsframework {
class CellAbstractContext;
} // namespace tsframework

namespace rpct {

class LogMessagesContainer : public log4cplus::Appender
                           , public ajax::Widget
{
protected:
    void append(log4cplus::spi::InternalLoggingEvent const & event);
    ajax::PlainHtml * log_message(std::string const & message
                                  , unsigned char level = rpct::tools::State::unknown_
                                  , time_t tv = 0);
public:
    LogMessagesContainer(size_t max_events = 50);
    ~LogMessagesContainer();

    void close();

    void clear();

    void html(cgicc::Cgicc &,std::ostream & out);

protected:
    static std::map<log4cplus::LogLevel, unsigned char> * levels_;

    size_t max_events_;
    std::deque<ajax::PlainHtml *> events_;

    rpct::tools::RWMutex rwmutex_;
};

class LoggerWidget : public ajax::AutoHandledWidget
{
public:
    LoggerWidget(tsframework::CellAbstractContext & context, size_t max_events = 50);
    ~LoggerWidget();

    void clear(cgicc::Cgicc & cgi, std::ostream & out);
    void refresh(cgicc::Cgicc & cgi, std::ostream & out);

    log4cplus::Appender & getAppender();

    ajax::ResultBox & getResultBox();

    void html(cgicc::Cgicc & cgi, std::ostream & out);
    void innerHtml(cgicc::Cgicc & cgi, std::ostream & out);
protected:
    LogMessagesContainer * logmessagescontainer_;

    ajax::Div * divcontainer_;
    ajax::ResultBox * resultbox_;
};

} // namespace rpct

#include "rpct/ts/common/LoggerWidget-inl.h"

#endif // rpct_ts_common_LoggerWidget_h_
