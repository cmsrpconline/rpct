/** Filip Thyssen */

#ifndef rpct_ts_common_LoggerWidget_inl_h_
#define rpct_ts_common_LoggerWidget_inl_h_

#include "rpct/ts/common/LoggerWidget.h"

// #include "ts/framework/CellAbstractContext.h"
// #include "rpct/ts/worker/RpctCellContext.h"


namespace rpct {

inline void LogMessagesContainer::close()
{}

// inline log4cplus::SharedAppenderPtr LoggerWidget::getAppender()
inline log4cplus::Appender & LoggerWidget::getAppender()
{
    return *logmessagescontainer_;
    // return *(mCellContext_->getlQueue());
}

inline ajax::ResultBox & LoggerWidget::getResultBox()
{
    return *resultbox_;
}

} // namespace rpct

#endif // rpct_ts_common_LoggerWidget_inl_h_
