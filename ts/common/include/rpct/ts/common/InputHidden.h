#ifndef _rpct_ts_common_InputHidden_h_
#define _rpct_ts_common_InputHidden_h_

#include "ajax/Widget.h"

#include <string>
#include <ostream>

namespace rpct {
namespace ajaxtools {

class InputHidden: public ajax::Widget
{
public:
    InputHidden(std::string const & _name = ""
                , std::string const & _value = "");

    void setName(std::string const & _name);
    void setValue(std::string const & _value);

    void html(cgicc::Cgicc & _cgi, std::ostream & _out);
};

} //namespace ajaxtools
} //namespace rpct

#endif // _rpct_ts_common_InputHidden_h_
