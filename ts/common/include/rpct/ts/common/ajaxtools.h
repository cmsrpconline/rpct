#ifndef _rpct_ts_common_ajaxtools_h_
#define _rpct_ts_common_ajaxtools_h_

#include <vector>
#include <utility>
#include <string>

#include "rpct/tools/State.h"

// #include "ajax/CheckBox.h"

namespace ajax {

class Container;
//class CheckBox;
// class ComboBox;
// class InputText;
class Button;
class PolymerElement;
// class SubmitButton;
// class PlainHtml;
// class Div;
// class Form;

} // namespace ajax

namespace rpct {
namespace ajaxtools {

// class Label;

// class CheckBoxWithSetStateOnClick: public ajax::CheckBox {
// public:
// 	// CheckBoxWithSetStateOnClick():CheckBox() {};
// 	//for call back in setEvent
// 	void onClick(cgicc::Cgicc& cgi, std::ostream& out);
// };

ajax::PolymerElement * button(ajax::Container * container, const std::string & caption);


// ajax::PlainHtml * link     (ajax::Container * container, const std::string & href, const std::string & caption, const std::string & target="");
// Label * label              (ajax::Container * _container, const std::string & _label
//                             , const std::string & _for = "", bool _before = true);
// ajax::CheckBox * checkbox  (ajax::Container * _container, const std::string & _id
//                             , const std::string & _caption = "", bool _checked = false);
// CheckBoxWithSetStateOnClick * checkBoxWithSetStateOnClick  (ajax::Container * _container, const std::string & _id
//                             , const std::string & _caption = "", bool _checked = false);

// ajax::ComboBox * combobox  (ajax::Container * container, const std::string & id
//                             , const std::vector<std::pair<std::string, std::string> > & values, const std::string & _default = ""
//                             , const std::string & _label = "");
// ajax::ComboBox * combobox  (ajax::Container * container, const std::string & id
//                             , const std::vector<std::string> & values, const std::string & _default = ""
//                             , const std::string & _label = "");
// ajax::InputText * inputtext(ajax::Container * container, const std::string & id
//                             , const std::string & _default = ""
//                             , const std::string & _label = "");
// ajax::Button * button      (ajax::Container * container, const std::string & caption);
// ajax::SubmitButton * submitbutton(ajax::Container * container, const std::string & caption);
// ajax::PlainHtml * title    (ajax::Container * container, int level, const std::string & title);
// ajax::Div * box            (ajax::Container * container, const std::string & title = "");
// ajax::Form * form          (ajax::Container * container, const std::string & title = "");
// ajax::Div * message        (ajax::Container * container, const std::string & message, unsigned char level = rpct::tools::State::info_);
// ajax::Div * message_ok     (ajax::Container * container, const std::string & message);
// ajax::Div * message_info   (ajax::Container * container, const std::string & message);
// ajax::Div * message_warning(ajax::Container * container, const std::string & message);
// ajax::Div * message_error  (ajax::Container * container, const std::string & message);

} // namespace ajaxtools
} // namespace rpct

#include "rpct/ts/common/ajaxtools-inl.h"

#endif // _rpct_ts_common_ajaxtools_h_
