/*
 * StringWidget.h
 *
 *  Created on: 2010-04-20
 *      Author: Krzysiek
 */

#ifndef STRINGWIDGET_H_
#define STRINGWIDGET_H_

#include "ajax/Widget.h"
#include <string>

namespace rpcttsworker {

class StringWidget: public ajax::Widget
{
	public:
		StringWidget();

		void setText(std::string text);
		void setBool(bool value);

		//!Creates the html/javascript to display the Widget
		void html(cgicc::Cgicc& cgi, std::ostream& out);

	private:
		std::string text;

};


}

#endif /* STRINGWIDGET_H_ */
