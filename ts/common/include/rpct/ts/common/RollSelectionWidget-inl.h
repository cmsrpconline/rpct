/** Filip Thyssen */

#ifndef rpct_ts_common_RollSelectionWidget_inl_h_
#define rpct_ts_common_RollSelectionWidget_inl_h_

namespace rpct {

inline rpct::tools::RollSelection & RollSelectionContainer::getSelection()
{
    return selection_;
}

inline rpct::tools::RollSelection const & RollSelectionContainer::getSelection() const
{
    return selection_;
}

inline rpct::tools::RollSelection const & RollSelectionWidget::getSelection() const
{
    return roll_selection_container_->getSelection();
}

} // namespace rpct

#endif // rpct_ts_common_RollSelectionWidget_inl_h_
