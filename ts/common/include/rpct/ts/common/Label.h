#ifndef _rpct_ts_common_Label_h_
#define _rpct_ts_common_Label_h_

#include "ajax/Widget.h"
#include "ajax/Container.h"

#include <string>
#include <ostream>

namespace rpct {
namespace ajaxtools {

class Label: public ajax::Widget
           , public ajax::Container
{
public:
    Label(std::string const & _label = ""
          , std::string const & _for = ""
          , bool _before = true);

    void setLabel(std::string const & _label);
    std::string const & getLabel() const;

    void setFor(std::string const & _for);

    void setBefore(bool _before);
    bool getBefore() const;

    void html(cgicc::Cgicc & _cgi, std::ostream & _out);

private:
    bool before_;
    std::string label_;
};

} //namespace ajaxtools
} //namespace rpct

#endif // _rpct_ts_common_Label_h_
