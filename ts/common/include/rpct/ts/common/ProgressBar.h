/** Filip Thyssen */

#ifndef rpct_ts_common_ProgressBar_h_
#define rpct_ts_common_ProgressBar_h_

#include <cstddef>

#include <string>

#include "ajax/Widget.h"
#include "ajax/AutoHandledWidget.h"

#include "rpct/tools/Progress.h"

namespace ajax {
class Div;
class ResultBox;
//class Button;
} // namespace ajax

namespace tsframework {
class CellAbstractContext;
} // namespace tsframework

namespace rpct {

class ProgressBar : public ajax::Widget
{
public:
    ProgressBar(rpct::tools::Progress & _progress
                , bool _show_end_estimate = true);

    rpct::tools::Progress const & getProgress() const;
    rpct::tools::Progress & getProgress();
    bool showEndEstimate() const;
    void showEndEstimate(bool _show_end_estimate);

    void html(cgicc::Cgicc &, std::ostream & _out);

    std::string progressStr_;
    std::string  getProgressStr() ;

protected:
    rpct::tools::Progress & progress_;
    bool show_end_estimate_;
};

class ProgressBarWidget : public ajax::AutoHandledWidget
{
public:
    ProgressBarWidget(tsframework::CellAbstractContext & _context
                      , rpct::tools::Progress & _progress
                      , std::string const & _title = std::string("Progress")
                      , bool _show_end_estimate = true);
    void refresh(cgicc::Cgicc &, std::ostream & _out);

    void pause(cgicc::Cgicc &, std::ostream & _out);
    void resume(cgicc::Cgicc &, std::ostream & _out);
    void terminate(cgicc::Cgicc &, std::ostream & _out);

    ProgressBar const & getProgressBar() const;
    ProgressBar & getProgressBar();

    rpct::tools::Progress const & getProgress() const;
    rpct::tools::Progress & getProgress();

    ajax::ResultBox & getResultBox();



    void html(cgicc::Cgicc & _cgi, std::ostream & _out);
    void innerHtml(cgicc::Cgicc & _cgi, std::ostream & _out);

protected:
    ProgressBar * progressbar_;
    std::string title_;

    ajax::Div * divcontainer_;
    ajax::ResultBox * resultbox_;

    //ajax::Button * pause_button_, * resume_button_, * terminate_button_;
};

} // namespace rpct

#include "rpct/ts/common/ProgressBar-inl.h"

#endif // rpct_ts_common_ProgressBar_h_
