/** Filip Thyssen */

#ifndef rpct_ts_common_RollSelectionWidget_h_
#define rpct_ts_common_RollSelectionWidget_h_

#include <string>
#include <set>

#include "log4cplus/logger.h"

#include "ajax/Widget.h"
#include "ajax/AutoHandledWidget.h"

#include "rpct/tools/RollId.h"
#include "rpct/tools/RollSelection.h"

namespace ajax {
class Div;
class ResultBox;
} // namespace ajax

namespace tsframework {
class CellAbstractContext;
} // namespace tsframework

namespace rpct {

class RollSelectionContainer : public ajax::Widget
{
public:
    RollSelectionContainer();

    rpct::tools::RollSelection & getSelection();
    rpct::tools::RollSelection const & getSelection() const;

    void html(cgicc::Cgicc & _cgi, std::ostream & _out);

protected:
    rpct::tools::RollSelection selection_;
};

class RollSelectionWidget : public ajax::AutoHandledWidget
{
public:
    RollSelectionWidget(tsframework::CellAbstractContext & _context
                        , log4cplus::Logger & _logger);

    void select(cgicc::Cgicc & _cgi, std::ostream & _out);
    void clear(cgicc::Cgicc & _cgi, std::ostream & _out);
    void selectName(cgicc::Cgicc & _cgi, std::ostream & _out);

    rpct::tools::RollSelection const & getSelection() const;

    ajax::ResultBox & getResultBox();

    void html(cgicc::Cgicc & _cgi, std::ostream & _out);
    void innerHtml(cgicc::Cgicc & _cgi, std::ostream & _out);

    // void getSelectedRolls(cgicc::Cgicc & _cgi, std::ostream & _out);
    RollSelectionContainer * roll_selection_container_;
protected:
    

    log4cplus::Logger logger_;

    ajax::Div * divcontainer_;
    ajax::ResultBox * resultbox_;
};

} // namespace rpct

#include "rpct/ts/common/RollSelectionWidget-inl.h"

#endif // rpct_ts_common_RollSelectionWidget_h_
