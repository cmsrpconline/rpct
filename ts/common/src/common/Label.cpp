#include "rpct/ts/common/Label.h"

namespace rpct {
namespace ajaxtools {

Label::Label(std::string const & _label
             , std::string const & _for
             , bool _before)
    : before_(_before)
    , label_(_label)
{
    if (_for != "")
        set("for", _for);
}

void Label::setLabel(std::string const & _label)
{
    label_ = _label;
}

std::string const & Label::getLabel() const
{
    return label_;
}

void Label::setFor(std::string const & _for)
{
    if (_for != "")
        set("for", _for);
    else
        unset("for");
}

void Label::setBefore(bool _before)
{
    before_ = _before;
}

bool Label::getBefore() const
{
    return before_;
}

void Label::html(cgicc::Cgicc & _cgi, std::ostream & _out)
{
    _out << "<label ";
    attributes(_out);
    _out << ">";
    if (before_)
        _out << label_ << " ";
    std::vector<ajax::Widget*> const _widgets(getWidgets());
    for (std::vector<ajax::Widget*>::const_iterator _widget = _widgets.begin()
             ; _widget != _widgets.end() ; ++_widget)
        (*_widget)->html(_cgi, _out);
    if (!before_)
        _out << " " << label_;
    _out << "</label>";
}

} // namespace ajaxtools
} // namespace rpct
