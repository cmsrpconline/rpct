/*
 * StringWidget.cc
 *
 *  Created on: 2010-04-20
 *      Author: Krzysiek
 */

#include "rpct/ts/common/StringWidget.h"

rpcttsworker::StringWidget::StringWidget()
	:ajax::Widget()
{

}

void rpcttsworker::StringWidget::setBool(bool value){
	this->text = value ? "YES" : "NO";
}

void rpcttsworker::StringWidget::setText(std::string text)
{
	this->text = text;
}

void rpcttsworker::StringWidget::html(cgicc::Cgicc& cgi, std::ostream& out)
{

	out << text;

}
