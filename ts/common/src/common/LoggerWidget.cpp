#include "rpct/ts/common/LoggerWidget.h"

#include "ajax/Div.h"
#include "ajax/ResultBox.h"
//#include "ajax/Button.h"
#include "ajax/PlainHtml.h"

#include "ts/framework/CellAbstractContext.h"
// #include "rpct/ts/worker/RpctCellContext.h"


//#include "rpct/ts/common/ajaxtools.h"
#include "log4cplus/spi/loggingevent.h"
// namespace rpcttsworker {
//     class RpctCellContext;
// }
namespace rpct {

std::map<log4cplus::LogLevel, unsigned char> * LogMessagesContainer::levels_ = 0;

LogMessagesContainer::LogMessagesContainer(size_t max_events)
    : max_events_(max_events)
{
    set("style", "font:8pt monospace;overflow:auto;height:120px;border:1px solid #888;margin:2px;background-color:#efefef;");
    set("id", getId());

    if (!levels_)
        {
            levels_ = new std::map<log4cplus::LogLevel, unsigned char>();
            levels_->insert(std::pair<log4cplus::LogLevel, unsigned char>(log4cplus::NOT_SET_LOG_LEVEL
                                                                          , rpct::tools::State::notset_));
            levels_->insert(std::pair<log4cplus::LogLevel, unsigned char>(log4cplus::ALL_LOG_LEVEL
                                                                          , rpct::tools::State::notset_));
            levels_->insert(std::pair<log4cplus::LogLevel, unsigned char>(log4cplus::TRACE_LOG_LEVEL
                                                                          , rpct::tools::State::notset_));
            levels_->insert(std::pair<log4cplus::LogLevel, unsigned char>(log4cplus::DEBUG_LOG_LEVEL
                                                                          , rpct::tools::State::notset_));
            levels_->insert(std::pair<log4cplus::LogLevel, unsigned char>(log4cplus::INFO_LOG_LEVEL
                                                                          , rpct::tools::State::info_));
            levels_->insert(std::pair<log4cplus::LogLevel, unsigned char>(log4cplus::WARN_LOG_LEVEL
                                                                          , rpct::tools::State::warn_));
            levels_->insert(std::pair<log4cplus::LogLevel, unsigned char>(log4cplus::ERROR_LOG_LEVEL
                                                                          , rpct::tools::State::error_));
            levels_->insert(std::pair<log4cplus::LogLevel, unsigned char>(log4cplus::FATAL_LOG_LEVEL
                                                                          , rpct::tools::State::error_));
            levels_->insert(std::pair<log4cplus::LogLevel, unsigned char>(log4cplus::OFF_LOG_LEVEL
                                                                          , rpct::tools::State::error_));
        }
}

LogMessagesContainer::~LogMessagesContainer()
{
    clear();
}

void LogMessagesContainer::clear()
{
    rpct::tools::WLock wlock(rwmutex_);
    std::deque<ajax::PlainHtml *>::iterator itEnd = events_.end();
    for (std::deque<ajax::PlainHtml *>::iterator it = events_.begin() ; it != itEnd ; ++it)
        delete (*it);
    events_.clear();
}

void LogMessagesContainer::append(const log4cplus::spi::InternalLoggingEvent & event)
{
    rpct::tools::WLock wlock(rwmutex_);
    if (events_.size() >= max_events_)
        {
            delete (events_.front());
            events_.pop_front();
        }
    unsigned char level = rpct::tools::State::unknown_;
    std::map<log4cplus::LogLevel, unsigned char>::const_iterator it = levels_->find(event.getLogLevel());
    if (it != levels_->end())
        level = it->second;

    events_.push_back(log_message(event.getMessage(), level, event.getTimestamp().getTime()));
}

ajax::PlainHtml * LogMessagesContainer::log_message(std::string const & message, unsigned char level, time_t tv)
{
    ajax::PlainHtml * ph = new ajax::PlainHtml();
    ph->getStream() << "<div style=\"background:"
                    << rpct::tools::State::getStateBgColor(level)
                    << " url('" << rpct::tools::State::getStateImage(level)
                    << "') no-repeat 2px 2px;padding:2px 28px;"
                    << "\">"
                    << "<span style=\"font-weight:bold;\">" << ctime(&tv) << ": </span>"
                    << message << "</div>" << std::endl;
    return ph;
}

void LogMessagesContainer::html(cgicc::Cgicc & cgi, std::ostream & out)
{
    rpct::tools::RLock rlock(rwmutex_);

    out << "<div ";
    attributes(out);
    out << ">" << std::endl;
    std::deque<ajax::PlainHtml *>::iterator itEnd = events_.end();
    for(std::deque<ajax::PlainHtml *>::iterator it = events_.begin() ; it != itEnd ; ++it)
        (*it)->html(cgi, out);
    out << "</div>" << std::endl;
    out << "<script type=\"text/javascript\">";
    out << "var lmc = document.getElementById(\"" << getId() <<"\");";
    out << "lmc.scrollTop = lmc.scrollHeight;";
    out << "</script>";
}

LoggerWidget::LoggerWidget(tsframework::CellAbstractContext & context, size_t max_events)
    : ajax::AutoHandledWidget("/" + context.getLocalUrn() + "/Default")
    // mCellContext_(dynamic_cast<rpcttsworker::RpctCellContext*>(&context))
{
    logmessagescontainer_ = new LogMessagesContainer(max_events);
    logmessagescontainer_->addReference();
    // LogMessagesContainer is a log4cplus SharedObject, so should not be destroyed
    logmessagescontainer_->setIsOwned(false);

    // mCellContext(dynamic_cast<rpcttsworker::RpctCellContext&>(*context))
    // mCellContext_ = &context;

    /*
    divcontainer_ = ajaxtools::box(this, "Log messages");
    resultbox_ = new ajax::ResultBox();
    divcontainer_->add(resultbox_);
    resultbox_->add(logmessagescontainer_);
    ajax::Button * _refreshbutton = ajaxtools::button(divcontainer_, "Refresh");
    _refreshbutton->set("style", "margin:5px;float:left;");
    // _refreshbutton->setImage("/ts/ajaxell/extern/icons/html/icons/arrow_refresh.png");
    setEvent(_refreshbutton, ajax::Eventable::OnClick, resultbox_, this, &rpct::LoggerWidget::refresh);
    ajax::Button * _clearbutton = ajaxtools::button(divcontainer_, "Clear");
    _clearbutton->set("style", "margin:5px;float:right;");
    setEvent(_clearbutton, ajax::Eventable::OnClick, resultbox_, this, &rpct::LoggerWidget::clear);
    ajax::Div * _cleardiv = new ajax::Div();
    _cleardiv->set("style", "clear:both;");
    divcontainer_->add(_cleardiv);*/
}

LoggerWidget::~LoggerWidget()
{
    logmessagescontainer_->removeReference();
}

void LoggerWidget::clear(cgicc::Cgicc & cgi, std::ostream & out)
{
    logmessagescontainer_->clear();
    innerHtml(cgi, out);
}

void LoggerWidget::refresh(cgicc::Cgicc & cgi, std::ostream & out)
{
    innerHtml(cgi, out);
}

void LoggerWidget::html(cgicc::Cgicc & cgi, std::ostream & out)
{
    divcontainer_->html(cgi, out);
}

void LoggerWidget::innerHtml(cgicc::Cgicc & cgi, std::ostream & out)
{
    resultbox_->innerHtml(cgi, out);
}

// // inline log4cplus::SharedAppenderPtr LoggerWidget::getAppender()
//  log4cplus::Appender & LoggerWidget::getAppender()
// {
//     // return *logmessagescontainer_;
//     return *(mCellContext_->getlQueue());
// }

} // namespace rpct
