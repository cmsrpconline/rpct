#include "rpct/ts/common/RollSelectionWidget.h"

#include <cmath>

#include "ajax/Div.h"
#include "ajax/ResultBox.h"
// #include "ajax/Button.h"
// #include "ajax/SubmitButton.h"
#include "ajax/PlainHtml.h"
// #include "ajax/InputText.h"

// #include "ajax/Form.h"

#include "ts/framework/CellAbstractContext.h"

#include "rpct/ts/common/ajaxtools.h"
#include "rpct/ts/common/InputHidden.h"
#include "rpct/ts/common/Label.h"

#include "log4cplus/loggingmacros.h"

#include "json/json.h"

#include <boost/lexical_cast.hpp>





namespace rpct {

RollSelectionContainer::RollSelectionContainer()
{
    set("style", "font:8pt monospace;overflow:auto;height:120px;border:1px solid #888;margin:2px;padding:5px;background-color:#efefef;");
}

void RollSelectionContainer::html(cgicc::Cgicc & _cgi, std::ostream & _out)
{

    Json::Value root;


    std::set<rpct::tools::RollId>::const_iterator _select_end(selection_.getSelected().end());
    for (std::set<rpct::tools::RollId>::const_iterator _select = selection_.getSelected().begin()
         ; _select != _select_end ; ++_select) {
        root["selected"].append(_select->name());
    }

    std::set<rpct::tools::RollId>::const_iterator _mask_end(selection_.getMasked().end());
    for (std::set<rpct::tools::RollId>::const_iterator _mask = selection_.getMasked().begin()
              ; _mask != _mask_end ; ++_mask) {
        root["masked"].append(_mask->name());
    }



    _out << root;

}

RollSelectionWidget::RollSelectionWidget(tsframework::CellAbstractContext & _context
                                         , log4cplus::Logger & _logger)
    : ajax::AutoHandledWidget("/" + _context.getLocalUrn() + "/Default")
    , roll_selection_container_(new RollSelectionContainer())
{
    logger_ = log4cplus::Logger::getInstance(_logger.getName() + ".RollSelectionWidget");

    divcontainer_ = new ajax::Div();
    add(divcontainer_);
    resultbox_ = new ajax::ResultBox();

    std::stringstream _label, _value;
    std::vector<std::pair<std::string, std::string> > _list;

    resultbox_->add(roll_selection_container_);
}

void RollSelectionWidget::select(cgicc::Cgicc & _cgi, std::ostream & _out)
{
    rpct::tools::RollId _roll;
    cgicc::const_form_iterator _fit, _fit_end(_cgi.getElements().end());
    bool _is_barrel
        = ((_fit = _cgi["isBarrel"]) != _fit_end) ? _fit->getIntegerValue() : 0;
    try {
        if (_is_barrel)
            {
                // std::cout << "OI" << std::endl;
                _roll.setRegion(0);
                int _wheel
                    = ((_fit = _cgi["wheel"]) != _fit_end) ? _fit->getIntegerValue() : 99;
                if (_wheel != 99)
                    _roll.setRing(_wheel);
                int _station
                    = ((_fit = _cgi["station"]) != _fit_end) ? _fit->getIntegerValue() : 99;
                if (_station != 99)
                    _roll.setStation(_station);
                int _layer
                    = ((_fit = _cgi["layer"]) != _fit_end) ? _fit->getIntegerValue() : 99;
                if (_layer != 99)
                    _roll.setLayer(_layer);
                int _sector
                    = ((_fit = _cgi["sector"]) != _fit_end) ? _fit->getIntegerValue() : 99;
                if (_sector != 99)
                    _roll.setSector(_sector);
                int _subsector
                    = ((_fit = _cgi["subsector"]) != _fit_end) ? _fit->getIntegerValue() : 99;
                if (_subsector != 99)
                    {
                        switch (_subsector) {
                        case 0:
                            _roll.setSubSector(0);
                            _roll.setSubSubSector(0);
                            break;
                        case 1:
                            _roll.setSubSector(1);
                            _roll.setSubSubSector(1);
                            break;
                        case 2:
                            _roll.setSubSector(1);
                            _roll.setSubSubSector(0);
                            break;
                        case 3:
                            _roll.setSubSector(2);
                            _roll.setSubSubSector(0);
                            break;
                        case 4:
                            _roll.setSubSector(2);
                            _roll.setSubSubSector(2);
                            break;
                        }
                    }
            }
        else
            {
                int _disk
                    = ((_fit = _cgi["disk"]) != _fit_end) ? _fit->getIntegerValue() : 99;
                if (_disk != 99)
                    {
                        _roll.setRegion(_disk > 0 ? 1 : -1);
                        _roll.setStation(std::abs(_disk));
                    }
                // _roll.setLayer(1); avoid R*in from confusing users, rather R*
                int _ring
                    = ((_fit = _cgi["ring"]) != _fit_end) ? _fit->getIntegerValue() : 99;
                if (_ring != 99)
                    _roll.setRing(_ring);
                int _chamber
                    = ((_fit = _cgi["chamber"]) != _fit_end) ? _fit->getIntegerValue() : 99;
                if (_chamber != 99)
                    {
                        if (_disk == 1 && _ring == 1)
                            {
                                _roll.setSector((_chamber - 1) / 3 + 1);
                                _roll.setSubSector((_chamber - 1) % 3);
                                _roll.setSubSubSector(0);
                            }
                        else
                            { // assuming not RE1/1
                                _roll.setSector((_chamber - 1) / 6 + 1);
                                _roll.setSubSector(((_chamber - 1) % 6) / 2);
                                _roll.setSubSubSector((_chamber - 1) % 2);
                            }
                    }
            }
    } catch(std::logic_error const & _e) {
        LOG4CPLUS_WARN(logger_, "Could not parse roll: " << _e.what());
        innerHtml(_cgi, _out);
        return;
    }

    int _action = ((_fit = _cgi["action"]) != _fit_end) ? _fit->getIntegerValue() : 1;
    switch (_action) {
    case 4:
        roll_selection_container_->getSelection().unmask(_roll);
        break;
    case 2:
        roll_selection_container_->getSelection().mask(_roll);
        break;
    case 3:
        roll_selection_container_->getSelection().deselect(_roll);
        break;
    default:
        roll_selection_container_->getSelection().select(_roll);
        break;
    }

    innerHtml(_cgi, _out);
}

void RollSelectionWidget::selectName(cgicc::Cgicc & _cgi, std::ostream & _out)
{
    // std::cout << "OI" << std::endl;
    rpct::tools::RollId _roll;
    cgicc::const_form_iterator _fit, _fit_end(_cgi.getElements().end());
    std::string _name = ((_fit = _cgi["name"]) != _fit_end) ? _fit->getValue() : "";
    if (!_name.empty())
        {

            try {
                _roll.parseName(_name);
            } catch(std::runtime_error const & _e) {
                LOG4CPLUS_WARN(logger_, "Could not parse name: " << _e.what());
                innerHtml(_cgi, _out);
                return;
            }

            int _action = ((_fit = _cgi["action"]) != _fit_end) ? _fit->getIntegerValue() : 1;

            switch (_action) {
            case 4:
                roll_selection_container_->getSelection().unmask(_roll);
                break;
            case 2:
                roll_selection_container_->getSelection().mask(_roll);
                break;
            case 3:
                roll_selection_container_->getSelection().deselect(_roll);
                break;
            default:
                roll_selection_container_->getSelection().select(_roll);
                break;
            }
        }

    innerHtml(_cgi, _out);
}

void RollSelectionWidget::clear(cgicc::Cgicc & _cgi, std::ostream & _out)
{
    roll_selection_container_->getSelection().clear();
    innerHtml(_cgi, _out);
}

void RollSelectionWidget::html(cgicc::Cgicc & _cgi, std::ostream & _out)
{
    divcontainer_->html(_cgi, _out);
}

void RollSelectionWidget::innerHtml(cgicc::Cgicc & _cgi, std::ostream & _out)
{
    resultbox_->innerHtml(_cgi, _out);
}

} // namespace rpct
