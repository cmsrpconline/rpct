#include "rpct/ts/common/ajaxtools.h"

// #include "ajax/Container.h"
// #include "ajax/CheckBox.h"
// #include "ajax/ComboBox.h"
// #include "ajax/InputText.h"
// #include "ajax/Button.h"
// #include "ajax/SubmitButton.h"
// #include "ajax/PlainHtml.h"
// #include "ajax/Div.h"
// #include "ajax/Form.h"
// #include "ajax/ajax.h"


// #include "ajax/AutoHandlerWidget.h"
#include "ajax/Container.h"
#include "ajax/Div.h"
#include "ajax/Eventable.h"
#include "ajax/EventHandler.h"
#include "ajax/Exception.h"
#include "ajax/NullWidget.h"
#include "ajax/Page.h"
#include "ajax/PlainHtml.h"
#include "ajax/PolymerElement.h"
#include "ajax/Registry.h"
#include "ajax/ResultBox.h"
#include "ajax/Table.h"
#include "ajax/toolbox.h"
#include "ajax/Widget.h"

#include "rpct/ts/common/Label.h"

namespace rpct {
namespace ajaxtools {

// void CheckBoxWithSetStateOnClick::onClick(cgicc::Cgicc& cgi, std::ostream& out) {
//   setChecked(!getChecked());
// }



ajax::PolymerElement * button(ajax::Container * container, const std::string & caption)
{
    ajax::PolymerElement* button = new ajax::PolymerElement("button-widget");
    // ajax::PolymerElement* button = new ajax::PolymerElement("log-panel");
    // button->setCaption(caption);
    // if (container)
    //     container->add(button);
    std::cout << "Button test: " << caption << std::endl;
    return button;
}



// ajax::PlainHtml * link(ajax::Container * container, const std::string & href, const std::string & caption, const std::string & target)
// {
//     ajax::PlainHtml * link = new ajax::PlainHtml();
//     link->getStream() << "<a href=\"" << href << "\""
//                       << (target != "" ? (" target=\"" + target + "\"") : "")
//                       << ">" << caption << "</a><br />";
//     if (container)
//         container->add(link);
//     return link;
// }

// Label * label(ajax::Container * _container, const std::string & _label
//               , const std::string & _for, bool _before)
// {
//     Label * _labelwidget = new Label(_label, _for, _before);
//     if (_container)
//         _container->add(_labelwidget);
//     return _labelwidget;
// }

// ajax::CheckBox * checkbox(ajax::Container * _container, const std::string & _id
//                           , const std::string & _caption, bool _checked)
// {
//     if (_caption != "")
//         _container = label(_container, _caption, _id, false);
//     ajax::CheckBox * _checkbox = new ajax::CheckBox();
//     _checkbox->setId(_id);
//     _checkbox->setChecked(_checked);
//     _checkbox->setDefaultValue("1");
//     _checkbox->set("style","margin:5px;");
//     if (_container)
//         _container->add(_checkbox);
//     return _checkbox;
// }


// CheckBoxWithSetStateOnClick * checkBoxWithSetStateOnClick  (ajax::Container * _container, const std::string & _id, const std::string & _caption, bool _checked)
// {
//     if (_caption != "")
//         _container = label(_container, _caption, _id, false);
//     CheckBoxWithSetStateOnClick * _checkbox = new CheckBoxWithSetStateOnClick();
//     _checkbox->setId(_id);
//     _checkbox->setChecked(_checked);
//     _checkbox->set("style","margin:5px;");
//     if (_container)
//         _container->add(_checkbox);
//     return _checkbox;
// }

// ajax::ComboBox * combobox(ajax::Container * container, const std::string & id
//                           , const std::vector<std::pair<std::string, std::string> > & values, const std::string & _default
//                           , const std::string & _label)
// {
//     if (_label != "")
//         label(container, _label, id, true);
//     ajax::ComboBox * combobox = new ajax::ComboBox();
//     combobox->setId(id);
//     combobox->set("style", "min-width=50px;");
//     std::vector<std::pair<std::string, std::string> >::const_iterator itEnd = values.end();
//     for (std::vector<std::pair<std::string, std::string> >::const_iterator it = values.begin()
//              ; it != itEnd
//              ; ++it)
//         combobox->add(it->first, it->second);
//     if (_default != "")
//         combobox->setDefaultValue(_default);
//     if (container)
//         container->add(combobox);
//     return combobox;
// }

// ajax::ComboBox * combobox(ajax::Container * container, const std::string & id
//                           , const std::vector<std::string> & values, const std::string & _default
//                           , const std::string & _label)
// {
//     if (_label != "")
//         label(container, _label, id, true);
//     ajax::ComboBox * combobox = new ajax::ComboBox();
//     combobox->setId(id);
//     combobox->set("style", "min-width=50px;");
//     std::vector<std::string>::const_iterator itEnd = values.end();
//     for (std::vector<std::string>::const_iterator it = values.begin()
//              ; it != itEnd
//              ; ++it)
//         combobox->add(*it, *it);
//     if (_default != "")
//         combobox->setDefaultValue(_default);
//     if (container)
//         container->add(combobox);
//     return combobox;
// }

// ajax::InputText * inputtext(ajax::Container * container, const std::string & id
//                             , const std::string & _default
//                             , const std::string & _label)
// {
//     if (_label != "")
//         label(container, _label, id, true);
//     ajax::InputText * inputtext = new ajax::InputText();
//     inputtext->setId(id);
//     inputtext->set("style", "min-width=50px;");
//     if (_default != "")
//         inputtext->setDefaultValue(_default);
//     if (container)
//         container->add(inputtext);
//     return inputtext;
// }

// ajax::Button * button(ajax::Container * container, const std::string & caption)
// {
//     ajax::Button * button = new ajax::Button();
//     button->setCaption(caption);
//     if (container)
//         container->add(button);
//     return button;
// }

// ajax::SubmitButton * submitbutton(ajax::Container * container, const std::string & caption)
// {
//     ajax::SubmitButton * button = new ajax::SubmitButton();
//     button->setCaption(caption);
//     if (container)
//         container->add(button);
//     return button;
// }

// ajax::PlainHtml * title(ajax::Container * container, int level, const std::string & title)
// {
//     ajax::PlainHtml * text = new ajax::PlainHtml();
//     text->getStream() << "<h" << level << " style=\"color:#204a87;margin:" << 2*level << "px;\">" << title << "</h" << level << ">";
//     if (container)
//         container->add(text);
//     return text;
// }

// ajax::Div * box(ajax::Container * container, const std::string & _title)
// {
//     ajax::Div * div = new ajax::Div();
//     div->set("style", "margin:10px;padding:5px;border-radius:5px;-moz-border-radius:5px;-webkit-box-shadow:5px 5px 5px #888;-moz-box-shadow:5px 5px 5px #888;background:#dde;");
//     if (_title != "")
//         title(div, 3, _title);
//     if (container)
//         container->add(div);
//     return div;
// }

// ajax::Form * form(ajax::Container * container, const std::string & _title)
// {
//     ajax::Form * form = new ajax::Form();
//     form->set("style", "margin:10px;padding:5px;border-radius:5px;-moz-border-radius:5px;-webkit-box-shadow:5px 5px 5px #888;-moz-box-shadow:5px 5px 5px #888;background:#dde;");
//     if (_title != "")
//         title(form, 3, _title);
//     if (container)
//         container->add(form);
//     return form;
// }

// ajax::Div * message(ajax::Container * container, const std::string & message, unsigned char level)
// {
//     ajax::Div * div = new ajax::Div();
//     div->set("style", "background:" + rpct::tools::State::getStateBgColor(level)
//              + " url('" + rpct::tools::State::getStateImage(level)
//              + "') no-repeat 2px 2px;margin:10px;padding:4px 4px 4px 28px;border:1px solid #888;font:italic 8pt sans-serif;");
//     ajax::PlainHtml * text = new ajax::PlainHtml();
//     text->getStream() << message;
//     div->add(text);
//     if (container)
//         container->add(div);
//     return div;
// }

} // namespace ajaxtools
} // namespace rpct
