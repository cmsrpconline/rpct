#include "rpct/ts/common/InputHidden.h"

namespace rpct {
namespace ajaxtools {

InputHidden::InputHidden(std::string const & _name
                         , std::string const & _value)
{
    if (_name != "")
        set("name", _name);
    if (_value != "")
        set("value", _value);
}

void InputHidden::setName(std::string const & _name)
{
    if (_name != "")
        set("name", _name);
    else
        unset("name");
}

void InputHidden::setValue(std::string const & _value)
{
    if (_value != "")
        set("value", _value);
    else
        unset("value");
}

void InputHidden::html(cgicc::Cgicc & _cgi, std::ostream & _out)
{
    _out << "<input type=\"hidden\" ";
    // no need for always-present set("type", "hidden")
    attributes(_out);
    _out << " />";
}

} // namespace ajaxtools
} // namespace rpct
