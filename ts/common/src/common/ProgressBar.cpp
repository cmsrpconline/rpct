#include "rpct/ts/common/ProgressBar.h"


#include "boost/lexical_cast.hpp"    

#include "ajax/Div.h"
#include "ajax/ResultBox.h"

#include "ajax/PlainHtml.h"

#include "ts/framework/CellAbstractContext.h"


#include "rpct/tools/Date.h"

#include "log4cplus/logger.h"


namespace rpct {

ProgressBar::ProgressBar(rpct::tools::Progress & _progress
                         , bool _show_end_estimate)
    : progress_(_progress)
    , show_end_estimate_(_show_end_estimate)
    , progressStr_("")
{}

void ProgressBar::html(cgicc::Cgicc &, std::ostream & _out)
{
    log4cplus::Logger lLogger_ProgressMonitoring = log4cplus::Logger::getInstance("ProgressMonitoring");
    progressStr_ = "";


    int _maximum = (int)progress_.getMaximum();
    int _value = (int)progress_.getValue();
    int _failed = (int)progress_.getFailed();

    std::string toPrint = progress_.getTitle() + ": ";
    if (progress_.isProcessActive())
        {
            if (_maximum > 0 && (_value > 0 || _failed > 0)) {

                toPrint += "Progress: "+ boost::lexical_cast<std::string>( 100.*((float)((_value + _failed))/(float)(_maximum)) ) + "%. ";
            }
            else if (_maximum > 0) {
            }
            if (show_end_estimate_)
                {
                    float _progress = progress_.getProgress();
                    if (_progress > 0. && _progress < 1.) {

                        toPrint += "Estimated to be ready at : " + rpct::tools::Date(progress_.getEndEstimate());
                        toPrint += ". ";                          
                    } else if (_progress == 0.) {
                        toPrint += "Waiting for the first step to make a time estimate. ";
                    }
                }
        }
    if (_failed > 0) {
        toPrint += " Failed: "+ boost::lexical_cast<std::string>(_failed);
        toPrint += " out of " + boost::lexical_cast<std::string>(_maximum);
        toPrint += ". ";
    }
    std::string const & _description = progress_.getDescription();
    if (!_description.empty()) {
        toPrint += "Status: " + _description + ". ";
    }

    if (toPrint != "") {
        if (_failed > 0) {
            LOG4CPLUS_ERROR(lLogger_ProgressMonitoring, toPrint);
        } else {
            LOG4CPLUS_WARN(lLogger_ProgressMonitoring, toPrint);
        }
    }
    progressStr_ = toPrint;
}

ProgressBarWidget::ProgressBarWidget(tsframework::CellAbstractContext & _context
                                     , rpct::tools::Progress & _progress
                                     , std::string const & _title
                                     , bool _show_end_estimate)
    : ajax::AutoHandledWidget("/" + _context.getLocalUrn() + "/Default")
    , progressbar_(new ProgressBar(_progress, _show_end_estimate))
    , title_(_title)
{


	divcontainer_ = new ajax::Div();
    divcontainer_->set("id", "progressBar");
    this->add(divcontainer_);
    resultbox_ = new ajax::ResultBox();
    divcontainer_->add(resultbox_);
    resultbox_->add(progressbar_);

    ajax::Div * _cleardiv = new ajax::Div();
    _cleardiv->set("style", "clear:both;");
    divcontainer_->add(_cleardiv);
  
}

std::string ProgressBar::getProgressStr() 
{
    return progressStr_;
}

void ProgressBarWidget::refresh(cgicc::Cgicc & _cgi, std::ostream & _out)
{
    innerHtml(_cgi, _out);
}

void ProgressBarWidget::terminate(cgicc::Cgicc & _cgi, std::ostream & _out)
{
    getProgress().terminate();
    innerHtml(_cgi, _out);
}

void ProgressBarWidget::pause(cgicc::Cgicc & _cgi, std::ostream & _out)
{
    getProgress().pause();
    innerHtml(_cgi, _out);
}

void ProgressBarWidget::resume(cgicc::Cgicc & _cgi, std::ostream & _out)
{
    getProgress().resume();
    innerHtml(_cgi, _out);
}

void ProgressBarWidget::html(cgicc::Cgicc & _cgi, std::ostream & _out)
{

    divcontainer_->html(_cgi, _out);

}

void ProgressBarWidget::innerHtml(cgicc::Cgicc & _cgi, std::ostream & _out)
{

    resultbox_->innerHtml(_cgi, _out);

}

} // namespace rpct
