/*
 *  Authors: Michal Pietrusinski, Karol Bunkowski, Krzysztof Niemkiewicz
 *  Version: $Id: XdaqTCrateAccess.cpp 1445 2009-10-15 21:23:25Z kbunkow $
 *
 */

#include "rpct/std/NullPointerException.h"
#include "rpct/ts/worker/XdaqTCrateAccess.h"
#include "rpct/ts/worker/AsyncSoapCallProxy.h"

#include "xdata/InfoSpaceFactory.h"
#include "xdata/Serializable.h"
#include "xdata/exception/Exception.h"
#include "xdata/TableIterator.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "rpct/xdaqutils/XdaqCommonUtils.h"
#include "rpct/xdaqutils/XdaqDbServiceClient.h"
#include "rpct/xdaqutils/XdaqDbSystemBuilder.h"
#include "rpct/xdaqutils/OptLinksStatusInfo.h"
#include "rpct/xdaqutils/LogUtils.h"

#include "rpct/devices/ConfigurationSetImpl.h"
#include "rpct/ii/ConfigurationFlags.h"

#include "rpct/ts/worker/panels/HistogramsWidget.h"
#include "rpct/ts/worker/panels/MonitorablesWidget.h"

#include <log4cplus/configurator.h>
#include <boost/shared_ptr.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/filesystem/operations.hpp>

#include "rpct/devices/cb.h"

#include "TStyle.h" //ROOT
//#include <boost/timer.hpp>

using namespace std;

//using namespace rpct;
using rpct::System;
using rpct::ICrate;
using rpct::IBoard;
using rpct::IDevice;
using rpct::Opto;
using rpct::Pac;
using rpct::GbSort;
using rpct::Rmb;
using rpct::TTCSortTCSort;
using rpct::THsbSortHalf;
using rpct::TTUOpto;
using rpct::TTUTrig;
using rpct::TTTUBackplaneFinal;
using rpct::IDiagnosable;
using rpct::ConfigurationSet;
using rpct::ConfigurationSetImpl;
using rpct::Monitorable;
using rpct::LinkBox;
using rpct::ConfigurationFlags;
using rpct::TGolI2C;
using rpct::EI2C;
using rpct::IDiagCtrl;

using namespace rpct::xdaqdiagaccess;
using namespace rpct::xdaqutils;

namespace fs = boost::filesystem;

const char* XdaqTCrateAccess::APP_NAMESPACE = "urn:rpct-XdaqTCrateAccess:1.0";
const char* XdaqTCrateAccess::APP_NAMESPACE_PREFIX = "xtb";

const char* XdaqTCrateAccess::GET_OPTLINKS_STATUS_INFO = "getOptLinksStatusInfo";

using namespace rpct;

XdaqTCrateAccess::XdaqTCrateAccess(xdaq::Application* app, 
                                   rpct::MonitorablesWidget* monitorablesWigdet,
                                   rpcttspanels::HistogramsWidget* histogramsWidget
                                   ) :
    XdaqHardwareAccess(app, monitorablesWigdet, histogramsWidget)
{

	std::cout<<"app->getApplicationLogger().getName() "<<app->getApplicationLogger().getName()<<std::endl;
	SharedAppenderPtrList appenders = Logger::getRoot().getAllAppenders();
	for (SharedAppenderPtrList::iterator it = appenders.begin(); it!=appenders.end(); ++it) {
		std::cout<<"appeneder "<<(*it)->getName()<<std::endl;
	};
	std::cout<<std::endl<<std::endl;

	getLogger().setLogLevel(log4cplus::INFO_LOG_LEVEL);
	getSummaryLogger().setLogLevel(log4cplus::INFO_LOG_LEVEL);
    try {
    	initialise();
    }
    catch (xcept::Exception& e) {
    	std::ostringstream msg;
    	msg << "error during XdaqTCrateAccess::initialize(): ";
    	msg << xcept::stdformat_exception_history(e);
    	LOG4CPLUS_ERROR(getSummaryLogger(), msg.str());
    	initializationErrorStr_ = msg.str();
    }

    LOG4CPLUS_INFO(getSummaryLogger(), "TC crate initialized");
}

void XdaqTCrateAccess::boot(ICrate& crate, ConfigurationSet& confSet) {
    LOG4CPLUS_INFO(getLogger(), "Staring booting " << crate.getDescription());
    ostringstream bootDir;
    char* tmp = getenv("RPCT_DATA_PATH");
    if (tmp != 0) {
        bootDir << getenv("RPCT_DATA_PATH")<<"/"<<"crates_boot";
    }
    else {
    	XCEPT_RAISE(xoap::exception::Exception, (string)(__FUNCTION__) + string(": Environment variable RPCT_DATA_PATH not defined"));
    }
    bootDir << "/" << crate.getDescription();

    string bootFilePathName; // = bootDir.str() + "/boot.sh";

    if (crate.getType() == rpct::TriggerCrate::TYPE) {
        rpct::TriggerCrate& tc = dynamic_cast<rpct::TriggerCrate&>(crate);
        rpct::TTCSort* tcSort = tc.getTcSort();
        if (tcSort == 0) {
            throw rpct::NullPointerException("TcSort not found in the trigger crate");
        }
        rpct::DeviceSettings* s = confSet.getDeviceSettings(tcSort->GetTCSortTCSort());
        if (s != 0) {
            rpct::TcSortSettings* tcs = dynamic_cast<rpct::TcSortSettings*>(s);
            if (tcs == 0) {
                throw rpct::NullPointerException("Could not cast settings to TcSortSettings");
            }
            if (tcs->getBootScript().size() != 0) {
                bootFilePathName = bootDir.str() + "/" + tcs->getBootScript();
            }
            else {
                LOG4CPLUS_WARN(getLogger(),
                        "Boot script for " << crate.getDescription() << " not defined. Default boot scipt will be used");
            }
        }
    }
    if (bootFilePathName.size() == 0) {
        bootFilePathName = bootDir.str() + "/boot.sh";
    }


    fs::path p(bootFilePathName); //, fs::native
    if (!fs::exists(p)) {
    	XCEPT_RAISE(xcept::Exception,"File  " + bootFilePathName + " not found");
    }

    LOG4CPLUS_INFO(getLogger(), "booting script: " << bootFilePathName);
    ostringstream bootScript;
    //bootScript << "cd " << bootDir.str() << "; ./boot.sh;";
    bootScript<<bootFilePathName;

    int res = ::system(bootScript.str().c_str());
    if (res != 0) {
    	XCEPT_RAISE(xcept::Exception,"Could not boot: invalid result code " + rpct::toString(res));
    }
    LOG4CPLUS_INFO(getLogger(), "Booting ended for crate" << crate.getDescription());
}


void XdaqTCrateAccess::buildSystem() {

    LOG4CPLUS_INFO(this->getLogger(), "Building system...");

    if (system_ != 0) {
        // system already built
        return;

    }
    
    system_ = new System();
    try{
        //if (((string) systemFileName_).empty()) {
        LOG4CPLUS_INFO(this->getLogger(), "Configuring system from DB ");
        string host;
        int port;
        if (port_ != 0) {
            host = (string) host_;
            port = (int) port_;
        } else {
            string url = app_->getApplicationDescriptor()->getContextDescriptor()->getURL();
            string httpStr = "http://";
            string::size_type posHttp = url.find(httpStr);
            string::size_type posPort = url.find_last_of(":");
            host = (posHttp == string::npos) ? url.substr(0, posPort) : url.substr(httpStr.length(), posPort
                    - httpStr.length());
            string sPort = url.substr(posPort + 1);
            istringstream istr(sPort);
            istr >> port;
        }
        LOG4CPLUS_INFO(this->getLogger(), "Host: " << host << " port " << port << " classname " << app_->getApplicationDescriptor()->getClassName());
        XdaqDbSystemBuilder(app_, mock_).buildVmeCrate(*system_, host, port,
                app_->getApplicationDescriptor()->getClassName(), app_->getApplicationDescriptor()->getInstance());
        /*} else {
         rpct::XmlSystemBuilder(*system_, mock_).build((string) systemFileName_);
         }*/

        LOG4CPLUS_INFO(this->getLogger(), "System created");

    } catch (std::exception& e) {
        LOG4CPLUS_ERROR(this->getLogger(), e.what());
    	monitorablesWidget_->handleMonitorException(e, "Exception during buildSystem:");
    	monitorablesWidget_->analyseAndGenerateAlarms();
    	delete system_;
    	system_ = 0;
        XCEPT_RAISE(xcept::Exception, e.what());
    }
    catch (...) {
        LOG4CPLUS_ERROR(this->getLogger(), "Unknown c++ exception");
        std::exception e;
        monitorablesWidget_->handleMonitorException(e, "Unknown c++ exception during buildSystem");
        monitorablesWidget_->analyseAndGenerateAlarms();
        delete system_;
        system_ = 0;
        XCEPT_RAISE(xcept::Exception, "Unknown c++ exception during buildSystem");
    }
}

void XdaqTCrateAccess::initialise() {
	buildSystem();
	monitorablesWidget_->initialise(system_, &hardwareMutex_) ;

	XdaqDiagAccess::Diagnosables diagnosables;
	System::CrateList& sysc = system_->getCrates();
	for (System::CrateList::iterator iCrate = sysc.begin(); iCrate != sysc.end(); ++iCrate) {
		ICrate* crate = *iCrate;
		typedef ICrate::Boards Boards;
		Boards& boards = crate->getBoards();
		for (Boards::iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {
			typedef IBoard::Devices Devices;
			IBoard* board = (*iBoard);

			const Devices& devices = board->getDevices();
			for (Devices::const_iterator iDevice = devices.begin(); iDevice != devices.end(); ++iDevice) {
				IDiagnosable* diagnosable = dynamic_cast<IDiagnosable*> (*iDevice);
				if (diagnosable != 0) {
					diagnosables.push_back(diagnosable);
				}
			}
		}
	}


	LOG4CPLUS_DEBUG(this->getLogger(), "Crates and diagnosables lists built");
	xdaqIIAccess_ = new XdaqIIAccess(app_, *system_);
	LOG4CPLUS_DEBUG(this->getLogger(), "xdaqIIAccess created");
	xdaqDiagAccess_->mapDiagnosables(diagnosables);// = new XdaqDiagAccess(this, diagnosables, &hardwareMutex_);
	LOG4CPLUS_DEBUG(this->getLogger(), "xdaqDiagAccess created");

	if (!system_->getCrates().empty()) {
		partitionName_ = system_->getCrates().front()->getDescription();
		partitionNameStr_ = system_->getCrates().front()->getDescription();
		monitorablesWidget_->setPartitionName(partitionName_);
		string picturesDir = "/tmp";
		rpct::TriggerCrate* tc = dynamic_cast<rpct::TriggerCrate*> (system_->getCrates().front());
		if(tc != 0) {
			histogramsWidget_->addManager("tc",new rpct::TCStatisticDiagManager(partitionNameStr_, picturesDir, tc));
		}

		rpct::SorterCrate* sc = dynamic_cast<rpct::SorterCrate*> (system_->getCrates().front());
		if(sc != 0) {
			histogramsWidget_->addManager("sc", new rpct::SCStatisticDiagManager(partitionNameStr_, picturesDir, sc));
			//histogramsWidget_->addManager("ttu", new rpct::TTUStatisticDiagManager(partitionNameStr_, picturesDir, sc, true)); disabled, as it works very slow
		}
	}

	string monitorHistosPath = "/rpcpacdata/MonitorHistos";
	char* tmp = getenv("MONITOR_HISTOS_PATH");
	if (tmp != 0) {
		monitorHistosPath =  tmp;
		LOG4CPLUS_INFO(this->getLogger(), "setting up the monitorHistosPath from the environment MONITOR_HISTOS_PATH to "<<monitorHistosPath);
	}
	else {
		LOG4CPLUS_INFO(this->getLogger(), "setting up the monitorHistosPath to "<<monitorHistosPath);
	}

	string filenamePrefix = monitorHistosPath + string("/Histos_") + partitionNameStr_;

	histogramsWidget_->initialise(filenamePrefix, "trigger/sorter crates histograms");

	if(dynamic_cast<rpct::TriggerCrate*> (system_->getCrates().front()) != 0) {
		subsytems_.push_back(XdaqDbServiceClient::SUBSYSTEM_TCS);
	}
	else if(dynamic_cast<rpct::SorterCrate*> (system_->getCrates().front()) != 0) {
		subsytems_.push_back(XdaqDbServiceClient::SUBSYSTEM_SC);
		subsytems_.push_back(XdaqDbServiceClient::SUBSYSTEM_RBCS_TTUS);
	}
}



void XdaqTCrateAccess::checkConfigId(rpct::ICrate& crate, rpct::ConfigurationSet& confSet) {

    LOG4CPLUS_DEBUG(getLogger(), "XdaqTCrateAccess::checkConfigId: start");

    rpct::TriggerCrate* tc = dynamic_cast<rpct::TriggerCrate*>(&crate);
    if (tc != 0) {
        try {
            tc->checkConfigId(&confSet);
        }
        catch (rpct::NullPointerException& e) {
            LOG4CPLUS_WARN(getSummaryLogger(), "Could not check config id - probably expected pacConfigId not entered in the DB");
        }
    }

    LOG4CPLUS_DEBUG(getLogger(), "XdaqTCrateAccess::checkConfigId: versions checked.");
}


vector<int> XdaqTCrateAccess::getChipIdsToSetup() {
    vector<int> chipIds;
    const System::DeviceMap& deviceMap = getSystem()->getDeviceMap();
    LOG4CPLUS_INFO(this->getLogger(), "Device map size " << deviceMap.size());
    if(deviceMap.size() == 0) {
        LOG4CPLUS_WARN(getSummaryLogger(), "No Devices in the system, nothing to setup. setupBoards finished");
        return chipIds;
    }
    for (System::DeviceMap::const_iterator iDevice = deviceMap.begin(); iDevice != deviceMap.end(); ++iDevice) {
        if (dynamic_cast<Opto*>(iDevice->second) != 0
                || dynamic_cast<Pac*>(iDevice->second) != 0
                || dynamic_cast<GbSort*>(iDevice->second) != 0
                || dynamic_cast<Rmb*>(iDevice->second) != 0
                || dynamic_cast<TTCSortTCSort*>(iDevice->second) != 0
                || dynamic_cast<THsbSortHalf*>(iDevice->second) != 0
                || dynamic_cast<TTUTrig*>(iDevice->second) != 0
                || dynamic_cast<TTTUBackplaneFinal*>(iDevice->second) != 0
                || dynamic_cast<TTUOpto*>(iDevice->second) != 0 ) {
            chipIds.push_back(iDevice->second->getId());
            LOG4CPLUS_DEBUG(this->getLogger(), "Adding chip id " << iDevice->second->getId());
        }
        else {
            LOG4CPLUS_INFO(this->getLogger(), "Skipping setup for chip "<<" id" << iDevice->second->getId());
        }
    }

    return chipIds;
}

void XdaqTCrateAccess::loadFirmware(bool forceLoad, rpct::ConfigurationSet& confSet) {
	rpct::MutexHandler handler(hardwareMutex_);

	// go through all crates, check firmware and boot crates if firmware is bad
    System::CrateList& crates = getSystem()->getCrates();
    for (System::CrateList::iterator iCrate = crates.begin(); iCrate != crates.end(); ++iCrate) {
        ICrate* crate = *iCrate;

        if (!mock_) {

            bool needBoot = forceLoad;

            if (! needBoot) {
                try {
                    crate->checkVersions();
                    checkConfigId(*crate, confSet); //throws EBadDeviceVersion if boot needed
                    LOG4CPLUS_DEBUG(this->getLogger(), "Versions checked for " << crate->getDescription());
                } catch (rpct::EBadDeviceVersion& e) {
                    LOG4CPLUS_INFO(getSummaryLogger(), "Should reboot " << crate->getDescription() << ". " << e.what());
                    needBoot = true;
                }
            }

            if (needBoot) {
            	 LOG4CPLUS_INFO(getSummaryLogger(), string("Booting ") + crate->getDescription() + ". ");
                try {
                	std::exception e;
                	MonitorableStatus monStatus(MonitorItemName::MESSAGE, MonitorableStatus::WARNING, "Firmware is being loaded, please wait ~3 min with next transitions", 1);
                	//only ERROR is seen on the L1 Page
                	monitorablesWidget_->handleMonitorWarning(monStatus, crate->getDescription());
                	monitorablesWidget_->analyseAndGenerateAlarms();
                    boot(*crate, confSet);
                    monitorablesWidget_->clearGlobalStatus();
                }
                catch (TException& e) {
                    LOG4CPLUS_ERROR(getSummaryLogger(), e.what());
                    throw e;
                }

                try {
                    crate->checkVersions();
                    checkConfigId(*crate, confSet);
                } catch (rpct::EBadDeviceVersion& e) {
                    LOG4CPLUS_ERROR(this->getSummaryLogger(), e.what());
                    if (interactive_) {
                        cout << "Press 'y' to continue" << endl;
                        string str;
                        cin >> str;
                        if (str != "y") {
                            throw ;
                        }
                    }
                    else {
                        throw;
                    }
                }

                //flags |= ConfigurationFlags::COLD_SETUP;
            }
        }
    }
}

void XdaqTCrateAccess::checkFirmware(rpct::ConfigurationSet& confSet) {
	rpct::MutexHandler handler(hardwareMutex_);

	System::CrateList& crates = getSystem()->getCrates();
    for (System::CrateList::iterator iCrate = crates.begin(); iCrate != crates.end(); ++iCrate) {
        ICrate* crate = *iCrate;
        crate->checkVersions();
        checkConfigId(*crate, confSet); //throws BadDeviceVersion if boot needed
        LOG4CPLUS_DEBUG(this->getLogger(), "Firmware checked for " << crate->getDescription()<<" - OK");
    }
}

void XdaqTCrateAccess::configure(std::string globalConfigKey, bool forceLoadFirmware, bool forceReset, bool forceSetup) {
	try {
		SharedAppenderPtrList appenders = Logger::getRoot().getAllAppenders();
		for (SharedAppenderPtrList::iterator it = appenders.begin(); it!=appenders.end(); ++it) {
			std::cout<<"Appender "<<(*it)->getName()<<std::endl;
		};
		std::cout<<std::endl<<std::endl;

		std::cout<<"app_->getApplicationLogger().getName(): "<<app_->getApplicationLogger().getName()<<std::endl;

		std::cout<<"LoggerList"<<std::endl;
		LoggerList loggerList = Logger::getCurrentLoggers();
		for(unsigned int i = 0; i < loggerList.size(); i++) {
			std::cout<<"logger name: "<<loggerList[i].getName()<<std::endl;
		}
		std::cout<<std::endl<<std::endl;

	    monitorablesWidget_->clearGlobalStatus();
		std::vector<int> chipIds = getChipIdsToSetup();
		ConfigurationSetBagPtr confSetBag = getConfigurationSet(globalConfigKey, chipIds);
		loadFirmware(forceLoadFirmware, confSetBag->bag);
		checkFirmware(confSetBag->bag);

		resetHardware(forceReset);

		setupHardware(forceSetup, confSetBag->bag);
	}
    catch (xcept::Exception& e) {
        ALARM_EXCEPT(app_,"FATAL",e);
        FUNCTION_ERR("", e.what());
        XCEPT_RETHROW(xgi::exception::Exception, e.what(), e);
    }
    catch (std::exception& e) {
        ALARM_STD(app_,"FATAL",e);
        FUNCTION_ERR("", e.what());
        XCEPT_RAISE(xcept::Exception, e.what());
    }
    catch (...) {
        ALARM_OTHER(app_,"FATAL","configure failed, unknown exception");
        FUNCTION_ERR("configure failed, unknown exception", "");
        XCEPT_RAISE(xcept::Exception,"configure failed, unknown exception");
    }
}

/*
void XdaqTCrateAccess::setupBoards(std::string globalConfKey, bool forceCold, bool forceBoot) {

    int flags = forceCold ? ConfigurationFlags::FORCE_CONFIGURE : 0;

    try {
        LOG4CPLUS_INFO(this->getLogger(), "Setting up boards from DB ");
        vector<int> chipIds;
        const System::DeviceMap& deviceMap = system_->getDeviceMap();
        LOG4CPLUS_INFO(this->getLogger(), "Device map size " << deviceMap.size());
        if(deviceMap.size() == 0) {
            MSGS_INFO("No Devices in the system, nothing to setup");
            LOG4CPLUS_INFO(this->getLogger(), "No Devices in the system, nothing to setup. setupBoards finshed");
            return;
        }
        for (System::DeviceMap::const_iterator iDevice = deviceMap.begin(); iDevice != deviceMap.end(); ++iDevice) {
            if (dynamic_cast<Opto*>(iDevice->second) != 0
                   || dynamic_cast<Pac*>(iDevice->second) != 0
                    || dynamic_cast<GbSort*>(iDevice->second) != 0
                    || dynamic_cast<Rmb*>(iDevice->second) != 0
                    || dynamic_cast<TTCSortTCSort*>(iDevice->second) != 0
                    || dynamic_cast<THsbSortHalf*>(iDevice->second) != 0
                    || dynamic_cast<TTUTrig*>(iDevice->second) != 0
                    || dynamic_cast<TTTUBackplaneFinal*>(iDevice->second) != 0
                    || dynamic_cast<TTUOpto*>(iDevice->second) != 0 ) {
                chipIds.push_back(iDevice->second->getId());
                LOG4CPLUS_INFO(this->getLogger(), "Adding chip id " << iDevice->second->getId());
            }
            else {
                LOG4CPLUS_INFO(this->getLogger(), "Skipping setup for " << iDevice->second->getId());
            }
        }
        

	LOG4CPLUS_INFO(this->getLogger(), "Configuring with globalConfKey = " << (string)globalConfKey);
	ConfigurationSetBagPtr confSetBag = xdaqDbServiceClient_->getConfigurationSet(chipIds, (string)globalConfKey);
        
        if (!mock_) {
            hardwareMutex_.take();
            try {
                // go through all crates, check firmware and boot crates if firmware is bad
                System::CrateList& crates = system_->getCrates();
                for (System::CrateList::iterator iCrate = crates.begin(); iCrate != crates.end(); ++iCrate) {
                    ICrate* crate = *iCrate;

                    if (!mock_) {

                        bool needBoot = forceBoot;

                        if (! needBoot) {
                            try {
                                crate->checkVersions();
                                checkConfigId(*crate, confSetBag->bag); //throws EBadDeviceVersion if boot needed
                                LOG4CPLUS_DEBUG(this->getLogger(), "Versions checked for " << crate->getDescription());
                            } catch (rpct::EBadDeviceVersion& e) {
                                LOG4CPLUS_INFO(this->getLogger(), "Should reboot " << crate->getDescription() << ". " << e.what());
                                MSGS_INFO(string("Should reboot ") + crate->getDescription() + ". " + e.what());
                                needBoot = true;
                            }
                        }

                        if (needBoot) {
                            MSGS_INFO(string("Booting ") + crate->getDescription() + ". ");
                            try {
                                boot(*crate, confSetBag->bag);
                            }
                            catch (TException& e) {
                                LOG4CPLUS_ERROR(this->getLogger(), e.what());
                                MSGS_ERROR(e.what());
                                if (interactive_) {
                                    cout << "Press 'y' to continue" << endl;
                                    string str;
                                    cin >> str;
                                    if (str != "y") {
                                        throw ;
                                    }
                                }
                                else {
                                    throw;
                                }
                            }


                            try {
                                crate->checkVersions();
                                checkConfigId(*crate, confSetBag->bag);
                            } catch (rpct::EBadDeviceVersion& e) {
                                LOG4CPLUS_ERROR(this->getLogger(), e.what());
                                MSGS_ERROR(e.what());
                                if (interactive_) {
                                    cout << "Press 'y' to continue" << endl;
                                    string str;
                                    cin >> str;
                                    if (str != "y") {
                                        throw ;
                                    }
                                }
                                else {
                                    throw;
                                }
                            }

                            flags |= ConfigurationFlags::FORCE_CONFIGURE;
                        }
                    }
                }

                try {
                    lastSetupCold_ = system_->configure(confSetBag->bag, flags);
                }
                catch (TException& e) {
                    LOG4CPLUS_ERROR(this->getLogger(), e.what());
                    MSGS_ERROR(e.what());
                    if (interactive_) {
                        cout << "Press 'y' to continue" << endl;
                        string str;
                        cin >> str;
                        if (str != "y") {
                            throw ;
                        }
                    }
                    else {
                        throw;
                    }
                }
                MSGS_INFO(string("System configured. Configuration type ") + (lastSetupCold_ ? "COLD" : "WARM"));
                hardwareMutex_.give();
            }
            catch (...) {
                hardwareMutex_.give();
                throw;
            }
        }
    }
    catch (xcept::Exception& e) {
        XCEPT_RETHROW(xgi::exception::Exception, e.what(), e);
    }
    catch (std::exception& e) {
        throw;
    }

    LOG4CPLUS_INFO(this->getLogger(), "setupBoards done");
}
*/

void XdaqTCrateAccess::resetRmbs() {
    rpct::MutexHandler handler(hardwareMutex_);
    try {
        rpct::TriggerCrate* tc = dynamic_cast<rpct::TriggerCrate*> (getSystem()->getCrates().front());
        if (tc != 0) {
            for(std::list<rpct::TriggerBoard*>::iterator iTB = tc->getTriggerBoards().begin(); iTB != tc->getTriggerBoards().end(); iTB++) {
                (*iTB)->GetRmb()->resetRmb();
            }
        }

        LOG4CPLUS_INFO(this->getSummaryLogger(), "RMBs reset");
    }
    catch (xcept::Exception& e) {
        LOG4CPLUS_ERROR(this->getSummaryLogger(), e.what());
        throw;
    }
    catch (std::exception& e) {
        LOG4CPLUS_ERROR(this->getSummaryLogger(), e.what());
        XCEPT_RAISE(xoap::exception::Exception, e.what());
    }
    catch(...) {
        LOG4CPLUS_ERROR(this->getSummaryLogger(), "unknown exception during resetRmbs");
        XCEPT_RAISE(xoap::exception::Exception, "unknown exception during resetRmbs");
    }

}

xoap::MessageReference XdaqTCrateAccess::onResetRmbs(xoap::MessageReference msg) throw (xoap::exception::Exception) {
    LOG4CPLUS_INFO(this->getLogger(), __FUNCTION__<<" soap called");
    resetRmbs();
    return xoap::createMessage();
}


xoap::MessageReference XdaqTCrateAccess::onGetOptLinksStatusInfo(xoap::MessageReference msg) throw (xoap::exception::Exception) {

    LOG4CPLUS_INFO(this->getLogger(), __FUNCTION__<<" soap called");
    rpct::MutexHandler handler(hardwareMutex_);
    try {
        xdata::Integer errorLevel;
        XdaqSoapAccess::TSOAPParamMap paramMap;
        paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("errorLevel", &errorLevel));
        soapAccess_.parseSOAPRequest(msg, "readTemperature", APP_NAMESPACE_PREFIX, //TODO APP_NAMESPACE_PREFIX??????????????????????????????????????????????????????????
                XDAQ_NS_URI, paramMap);                                             //TODO XDAQ_NS_URI??????????????????????????????????????????????????????????

        rpct::OptLinksStatusInfoVector optLinksStatusInfos;
        rpct::TriggerCrate* tc = dynamic_cast<rpct::TriggerCrate*> (getSystem()->getCrates().front());
        if (tc != 0) {
            if(!monitorablesWidget_->isMonitoringOn()) {
                for(std::list<rpct::TriggerBoard*>::iterator iTB = tc->getTriggerBoards().begin(); iTB != tc->getTriggerBoards().end(); iTB++) {
                    for(unsigned int iO = 0; iO < (*iTB)->GetOptos().size(); iO++) {
                        (*iTB)->GetOptos()[iO]->enable();
                    }
                }
                usleep(1000000);
                for(std::list<rpct::TriggerBoard*>::iterator iTB = tc->getTriggerBoards().begin(); iTB != tc->getTriggerBoards().end(); iTB++) {
                    for(unsigned int iO = 0; iO < (*iTB)->GetOptos().size(); iO++) {
                        (*iTB)->GetOptos()[iO]->monitor(0);
                        (*iTB)->GetOptos()[iO]->disable();
                    }
                }
            }
            for(std::list<rpct::TriggerBoard*>::iterator iTB = tc->getTriggerBoards().begin(); iTB != tc->getTriggerBoards().end(); iTB++) {
                std::vector<int> optLinkStatus = (*iTB)->getOptLinkStatus();
                for(unsigned int iLink = 0; iLink < optLinkStatus.size(); iLink++) {
                    if(optLinkStatus[iLink] >= errorLevel) {
                        rpct::OptLinksStatusInfoBag optLinksStatusInfo;
                        optLinksStatusInfo.bag.setTbName((*iTB)->getDescription());
                        optLinksStatusInfo.bag.setOptLinkNum(iLink);
                        optLinksStatusInfo.bag.setStatus(optLinkStatus[iLink]);
                        optLinksStatusInfos.push_back(optLinksStatusInfo);
                    }
                }
            }
        }
        return soapAccess_.sendSOAPResponse(GET_OPTLINKS_STATUS_INFO, "xdaq", XDAQ_NS_URI, optLinksStatusInfos);
    }
    catch (xcept::Exception& e) {
        LOG4CPLUS_ERROR(this->getSummaryLogger(), "error during "<<__FUNCTION__<<" "<< e.what());
        XCEPT_RAISE(xoap::exception::Exception, e.what());
    }
    catch (std::exception& e) {
        LOG4CPLUS_ERROR(this->getSummaryLogger(), "error during "<<__FUNCTION__<<" "<< e.what());
        XCEPT_RAISE(xoap::exception::Exception, e.what());
    }
    catch(...) {
        LOG4CPLUS_ERROR(this->getSummaryLogger(), "error during "<<__FUNCTION__);
        XCEPT_RAISE(xoap::exception::Exception, "unknown exception during onGetOptLinksStatusInfo");
    }
    LOG4CPLUS_DEBUG(this->getLogger(), __FUNCTION__<<" soap Done");
}
