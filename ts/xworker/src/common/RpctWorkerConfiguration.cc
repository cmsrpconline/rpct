
#include "rpct/ts/worker/RpctWorkerConfiguration.h"
#include "rpct/xdaqutils/LogUtils.h"

#include "ts/toolbox/CellToolbox.h"
#include "ts/exception/CellException.h"


#include "xdata/xdata.h"

#include "toolbox/string.h"
#include "xdaq/NamespaceURI.h"

#include "log4cplus/logger.h"

#include <iostream>

using namespace tsexception;
using namespace tsframework;

namespace rpcttsworker {

RpctWorkerConfiguration::RpctWorkerConfiguration(log4cplus::Logger& log,
  tsframework::CellAbstractContext* context) :
		tsruncontrol::ConfigurationBase(log,context){

    try {
    	getFSM().clear();
        getFSM().addState("halted");
        getFSM().addState("configured_no_synchro");
        getFSM().addState("configured");
        getFSM().addState("running");
        getFSM().addState("paused");

        getFSM().addTransition("halted", "configured_no_synchro", "configure", this,
                &RpctWorkerConfiguration::c_true,
                &RpctWorkerConfiguration::execConfigure);
        getFSM().addTransition("configured_no_synchro","configured", "synchronize_links", this,
                &RpctWorkerConfiguration::c_true,
                &RpctWorkerConfiguration::execSynchronize_links);
        getFSM().addTransition("configured_no_synchro","configured", "do_nothing", this,
                &RpctWorkerConfiguration::c_true,
                &RpctWorkerConfiguration::execDo_nothing);
        getFSM().addTransition("halted", "configured", "do_nothing", this,
                               &RpctWorkerConfiguration::c_true,
                               &RpctWorkerConfiguration::execDo_nothing);
        getFSM().addTransition("halted", "halted", "coldReset", this,
                        &RpctWorkerConfiguration::c_true,
                        &RpctWorkerConfiguration::execColdReset);
        getFSM().addTransition("configured", "running", "start", this,
                &RpctWorkerConfiguration::c_true, &RpctWorkerConfiguration::execStart);
        getFSM().addTransition("running", "paused", "pause", this,
                &RpctWorkerConfiguration::c_true, &RpctWorkerConfiguration::execPause);
        getFSM().addTransition("paused", "running", "resume", this,
                &RpctWorkerConfiguration::c_true, &RpctWorkerConfiguration::execResume);
        getFSM().addTransition("running", "configured", "stop", this,
                &RpctWorkerConfiguration::c_true, &RpctWorkerConfiguration::execStop);
        getFSM().addTransition("paused", "configured", "stop", this,
                &RpctWorkerConfiguration::c_true, &RpctWorkerConfiguration::execStop);
        getFSM().setInitialState("halted");
        getFSM().reset();
    } catch (tsexception::CellException e) {
        XCEPT_RETHROW (tsexception::CellException,"RpctWorkerConfiguration can not Init()",e);
    }
    LOG4CPLUS_INFO(getLogger(), "RpctWorkerConfiguration init() completed");
}



void RpctWorkerConfiguration::execConfigure() {
  configure();
}

bool RpctWorkerConfiguration::c_true() {
    return true;
}

void RpctWorkerConfiguration::execSynchronize_links() {
  synchronize_links();
}



void RpctWorkerConfiguration::execDo_nothing() {
  
}


void RpctWorkerConfiguration::execStart() {
  start();    
}



void RpctWorkerConfiguration::execPause() {
  pause();
}


void RpctWorkerConfiguration::execResume() {
  resume();
}


void RpctWorkerConfiguration::execStop() {
  stop();
}


void RpctWorkerConfiguration::execColdReset(){
  coldReset();
}

}
