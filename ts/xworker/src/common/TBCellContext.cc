/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: I. Magrans          				         *
 *************************************************************************/
#include "rpct/ts/worker/TBCellContext.h"
#include "rpct/ts/worker/AsyncSoapCallProxy.h"
#include "ts/framework/Level1Source.h"
#include "rpct/xdaqutils/LogUtils.h"
#include "rpct/ts/worker/XdaqTCrateAccess.h"
#include "ts/exception/CellException.h"
#include "ts/toolbox/CellToolbox.h"

#include "rpct/ts/worker/panels/MonitorablesWidget.h"
#include "rpct/ts/worker/panels/HistogramsWidget.h"
#include "rpct/ts/worker/MonitorSource.h"

#include <string>
#include <cstdlib>

#include "toolbox/string.h"

#include "log4cplus/logger.h"



rpcttsworker::TBCellContext::TBCellContext(log4cplus::Logger& log, 
                                           tsframework::CellAbstract* cell)
    : tsframework::CellAbstractContext(log, cell)
    , rpcttsworker::RpctCellContext(log,cell), tcCrateAccess_(0)
{
	logger_ = log4cplus::Logger::getInstance(getLogger().getName() + ".CellContext");

        try {
            tcCrateAccess_ = new XdaqTCrateAccess((xdaq::Application*)cell,
            		                                                monitorablesWidget_,
                                                                    histogramsWidget_);
        } catch (xcept::Exception& e) {
            std::ostringstream msg;
            msg << "error creating XdaqTCrateAccess: ";
            msg << xcept::stdformat_exception_history(e);
            LOG4CPLUS_ERROR(logger_,msg.str());
        }
        LOG4CPLUS_INFO(logger_, "TBCellContext constructor end");
}
rpcttsworker::TBCellContext::~TBCellContext()
{
  ;
}


