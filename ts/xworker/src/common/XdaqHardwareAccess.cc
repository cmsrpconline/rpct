/*
 *  Authors: Michal Pietrusinski, Karol Bunkowski, Krzysztof Niemkiewicz
 *  Version: $Id: XdaqHardwareAccess.cc 2000 2009-10-15 21:23:25Z kbunkow $
 *
 */

#include "rpct/std/NullPointerException.h"
#include "rpct/ts/worker/XdaqHardwareAccess.h"
#include "rpct/ts/worker/AsyncSoapCallProxy.h"

#include "xdata/InfoSpaceFactory.h"
#include "xdata/Serializable.h"
#include "xdata/exception/Exception.h"
#include "xdata/TableIterator.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "rpct/xdaqutils/XdaqCommonUtils.h"
#include "rpct/xdaqutils/XdaqDbServiceClient.h"
#include "rpct/xdaqutils/XdaqDbSystemBuilder.h"
#include "rpct/xdaqutils/OptLinksStatusInfo.h"
#include "rpct/xdaqutils/LogUtils.h"

#include "rpct/devices/ConfigurationSetImpl.h"
#include "rpct/ii/ConfigurationFlags.h"

#include "rpct/ts/worker/panels/HistogramsWidget.h"
#include "rpct/ts/worker/panels/MonitorablesWidget.h"

#include <log4cplus/configurator.h>
#include <boost/shared_ptr.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/filesystem/operations.hpp>

#include "TStyle.h" //ROOT
//#include <boost/timer.hpp>

using namespace std;

//using namespace rpct;
using rpct::System;
using rpct::ICrate;
using rpct::IBoard;
using rpct::IDevice;
using rpct::Opto;
using rpct::Pac;
using rpct::GbSort;
using rpct::Rmb;
using rpct::TTCSortTCSort;
using rpct::THsbSortHalf;
using rpct::TTUOpto;
using rpct::TTUTrig;
using rpct::TTTUBackplaneFinal;
using rpct::IDiagnosable;
using rpct::ConfigurationSet;
using rpct::ConfigurationSetImpl;
using rpct::Monitorable;
using rpct::LinkBox;
using rpct::ConfigurationFlags;
using rpct::TGolI2C;
using rpct::EI2C;
using rpct::IDiagCtrl;

using namespace rpct::xdaqdiagaccess;
using namespace rpct::xdaqutils;

namespace fs = boost::filesystem;

using namespace rpct;


Logger XdaqHardwareAccess::logger_ = Logger::getInstance("XdaqHardwareAccess");
Logger XdaqHardwareAccess::summaryLogger_ = Logger::getInstance("XdaqHardwareAccess.summary");
void XdaqHardwareAccess::setupLoggers(std::string namePrefix) {
	logger_ = log4cplus::Logger::getInstance(namePrefix + "." + logger_.getName());
	summaryLogger_ = log4cplus::Logger::getInstance(namePrefix + "." + summaryLogger_.getName());
}

XdaqHardwareAccess::XdaqHardwareAccess(xdaq::Application* app, 
                                   rpct::MonitorablesWidget* monitorablesWigdet,
                                   rpcttspanels::HistogramsWidget* histogramsWidget
                                   ) :
    app_(app),
    monitorablesWidget_(monitorablesWigdet), histogramsWidget_(histogramsWidget),
    interactive_(false), mock_(false), port_(0), lastResetCold_(false),
    xdaqIIAccess_(0), xdaqDiagAccess_(0), runNumber_(0), 
    system_(0), soapAccess_(app),  hardwareMutex_(toolbox::BSem::FULL)
{
    LOG4CPLUS_INFO(this->getLogger(), "Hello World!");
    LOG4CPLUS_INFO(this->getLogger(), "compilation path: "<<__FILE__);
    LOG4CPLUS_INFO(this->getLogger(), "compilation date: "<<__DATE__<<" "<<__TIME__);
    //G_Debug.SetLevel(TDebug::VME_OPER);

    // ********** IIAccess ************
    rpct::bind(app, this, &XdaqHardwareAccess::onGetCratesInfo, XdaqIIAccess::GET_CRATES_INFO_NAME, XDAQ_NS_URI);
    rpct::bind(app, this, &XdaqHardwareAccess::onGetBoardsInfo, XdaqIIAccess::GET_BOARDS_INFO_NAME, XDAQ_NS_URI);
    rpct::bind(app, this, &XdaqHardwareAccess::onGetDevicesInfo, XdaqIIAccess::GET_DEVICES_INFO_NAME, XDAQ_NS_URI);
    rpct::bind(app, this, &XdaqHardwareAccess::onGetDeviceItemsInfo, XdaqIIAccess::GET_DEVICEITEMS_INFO_NAME, XDAQ_NS_URI);
    rpct::bind(app, this, &XdaqHardwareAccess::onReadII, XdaqIIAccess::READ_II_NAME, XDAQ_NS_URI);
    rpct::bind(app, this, &XdaqHardwareAccess::onWriteII, XdaqIIAccess::WRITE_II_NAME, XDAQ_NS_URI);


    // ********** DiagAccess ************
    xdaqDiagAccess_ = new XdaqDiagAccess(app, &hardwareMutex_);
    // xdaqDiagAccess_->bind(app);
    rpct::bind(app,xdaqDiagAccess_, &XdaqDiagAccess::configureDiagnosable, XdaqDiagAccess::CONFIGURE_DIAGNOSABLE_NAME,
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS);
    rpct::bind(app,xdaqDiagAccess_, &XdaqDiagAccess::configureDiagnosableLB, XdaqDiagAccess::CONFIGURE_DIAGNOSABLE_LB_NAME,
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS);

    // diagCtrl
    rpct::bind(app,xdaqDiagAccess_, &XdaqDiagAccess::getDiagCtrlList, XdaqDiagAccess::GET_DIAG_CTRL_LIST_NAME,
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS);
    rpct::bind(app,xdaqDiagAccess_, &XdaqDiagAccess::resetDiagCtrl, XdaqDiagAccess::RESET_DIAG_CTRL_NAME,
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS);
    rpct::bind(app,xdaqDiagAccess_, &XdaqDiagAccess::configureDiagCtrl, XdaqDiagAccess::CONFIGURE_DIAG_CTRL_NAME,
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS);
    rpct::bind(app,xdaqDiagAccess_, &XdaqDiagAccess::startDiagCtrl, XdaqDiagAccess::START_DIAG_CTRL_NAME,
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS);
    rpct::bind(app,xdaqDiagAccess_, &XdaqDiagAccess::stopDiagCtrl, XdaqDiagAccess::STOP_DIAG_CTRL_NAME,
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS);
    rpct::bind(app,xdaqDiagAccess_, &XdaqDiagAccess::getDiagCtrlState, XdaqDiagAccess::GET_DIAG_CTRL_STATE_NAME,
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS);
    rpct::bind(app,xdaqDiagAccess_, &XdaqDiagAccess::checkCountingEnded, XdaqDiagAccess::CHECK_COUNTING_ENDED_NAME,
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS);
    //  histoMgr
    rpct::bind(app,xdaqDiagAccess_, &XdaqDiagAccess::getHistoMgrList, XdaqDiagAccess::GET_HISTO_MGR_LIST_NAME,
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS);
    rpct::bind(app,xdaqDiagAccess_, &XdaqDiagAccess::resetHistoMgr, XdaqDiagAccess::RESET_HISTO_MGR_NAME,
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS);
    rpct::bind(app,xdaqDiagAccess_, &XdaqDiagAccess::readDataHistoMgr, XdaqDiagAccess::READ_DATA_HISTO_MGR_NAME,
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS);
    //  pulser
    rpct::bind(app,xdaqDiagAccess_, &XdaqDiagAccess::getPulserList, XdaqDiagAccess::GET_PULSER_LIST_NAME,
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS);
    rpct::bind(app,xdaqDiagAccess_, &XdaqDiagAccess::configurePulser, XdaqDiagAccess::CONFIGURE_PULSER_NAME,
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS);
    rpct::bind(app,xdaqDiagAccess_, &XdaqDiagAccess::configurePulserMuons, XdaqDiagAccess::CONFIGURE_PULSER_MUONS_NAME,
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS);
    // diagnostic readout
    rpct::bind(app,xdaqDiagAccess_, &XdaqDiagAccess::getDiagnosticReadoutList, XdaqDiagAccess::GET_DIAGNOSTIC_READOUT_LIST_NAME,
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS);
    rpct::bind(app,xdaqDiagAccess_, &XdaqDiagAccess::configureDiagnosticReadout, XdaqDiagAccess::CONFIGURE_DIAGNOSTIC_READOUT_NAME,
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS);
    rpct::bind(app,xdaqDiagAccess_, &XdaqDiagAccess::configureDiagnosticReadoutLB,
            XdaqDiagAccess::CONFIGURE_DIAGNOSTIC_READOUT_LB_NAME, XdaqDiagAccess::RPCT_DIAG_ACCESS_NS);
    rpct::bind(app,xdaqDiagAccess_, &XdaqDiagAccess::readDataDiagnosticReadout, XdaqDiagAccess::READ_DATA_DIAGNOSTIC_READOUT_NAME,
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS);

   
    G_Log.out(std::cout);
    G_Log.out() << std::hex;

    app->getApplicationInfoSpace()->fireItemAvailable("interactive", &interactive_);
    app->getApplicationInfoSpace()->fireItemAvailable("mock", &mock_);
    app->getApplicationInfoSpace()->fireItemAvailable("host", &host_);
    app->getApplicationInfoSpace()->fireItemAvailable("port", &port_);

    xdaqDbServiceClient_ = new XdaqDbServiceClient(app);


}

XdaqHardwareAccess::~XdaqHardwareAccess() {

    delete xdaqIIAccess_;
    xdaqIIAccess_ = 0;
        
    /*delete xdaqDiagAccess_;
    xdaqDiagAccess_ = 0;*/
    xdaqDiagAccess_->clearDiagnosables();

    if(system_)
    	delete system_;
    system_ = 0;

    delete xdaqDiagAccess_;
    delete xdaqDbServiceClient_;
}

rpct::System* XdaqHardwareAccess::getSystem() {
    if(system_ == 0) {
        if(initializationErrorStr_.size() != 0) {
            std::string msg = std::string("There was an exception during system initialization, try restarting the application. The exception was: ") + initializationErrorStr_;
            XCEPT_RAISE(xcept::Exception, msg);
        }
        else
            XCEPT_RAISE(xcept::Exception,"System not initialized, try restarting the application.");
    }
    return system_;
}

void XdaqHardwareAccess::loadLocalConfigKeys() {
    try {
        if (subsystemsLocalConfigKeys_.empty()) {
            for(unsigned int iS = 0; iS <  subsytems_.size(); iS++) {
                xdata::Vector<xdata::String> configKeys = xdaqDbServiceClient_->getLocalConfigKeys(subsytems_[iS]);
                std::vector<std::string> localConfigKeys;
                for (size_t i = 0; i < configKeys.size(); i++) {
                    localConfigKeys.push_back(configKeys[i].toString());
                }
                subsystemsLocalConfigKeys_[subsytems_[iS] ] = localConfigKeys;
            }
        }
    } catch (std::exception& e) {
        LOG4CPLUS_ERROR(getSummaryLogger(), e.what());
    }
}

ConfigurationSetBagPtr XdaqHardwareAccess::getConfigurationSet(std::string confKey, std::vector<int>& chipIds) {
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<" started. Number of chips to configure: " << chipIds.size());

    if (chipIds.empty()) {
        XCEPT_RAISE(xcept::Exception,"No chips to configure. All disabled?");
    }

    return xdaqDbServiceClient_->getConfigurationSet(chipIds, confKey);
}

void XdaqHardwareAccess::resetHardware(bool forceReset) {
	rpct::MutexHandler handler(hardwareMutex_);

	FUNCTION_INFO(": started (forceReset="<<forceReset<<")");
    try {
    	lastResetCold_ = getSystem()->resetHardware(forceReset);
    }
    catch (std::exception& e) {
        ALARM_STD(app_,"FATAL",e);
        FUNCTION_ERR("", e.what());
        XCEPT_RAISE(xcept::Exception, e.what());
    }
    catch (...) {
        ALARM_OTHER(app_,"FATAL","resetHardware failed, unknown exception");
        FUNCTION_ERR("resetHardware failed, unknown exception", "");
        XCEPT_RAISE(xcept::Exception,"resetHardware failed, unknown exception");
    }
}

void XdaqHardwareAccess::setupHardware(bool forceSetup, rpct::ConfigurationSet& confSet) {
	rpct::MutexHandler handler(hardwareMutex_);

	FUNCTION_INFO(": started (forceSetup="<<forceSetup<<")");
    try {
    	getSystem()->setupHardware( &confSet, forceSetup);
    }
    catch (std::exception& e) {
        ALARM_STD(app_,"FATAL",e);
        FUNCTION_ERR("", e.what());
        XCEPT_RAISE(xcept::Exception, e.what());
    }
    catch (...) {
        ALARM_OTHER(app_,"FATAL","setupHardware failed, unknown exception");
        FUNCTION_ERR("setupHardware failed, unknown exception", "");
        XCEPT_RAISE(xcept::Exception,"setupHardware failed, unknown exception");
    }
}

void XdaqHardwareAccess::enable() {
	try {
		if(histogramsWidget_->isActive() == false) {
			rpct::MutexHandler handler(hardwareMutex_);
			getSystem()->enable(); //can be done inside the monitorablesWidget_, but system_ global field is not  there
			//monitorablesWidget_->startMonitoring();
		}

		histogramsWidget_->start(runNumber_.toString(), &hardwareMutex_);
	}
    catch (std::exception& e) {
        ALARM_STD(app_,"FATAL",e);
        FUNCTION_ERR("", e.what());
        monitorablesWidget_->handleMonitorException(e, "Exception during start: ");
        monitorablesWidget_->analyseAndGenerateAlarms();
        XCEPT_RAISE(xcept::Exception, e.what());
    }
    catch (...) {
        ALARM_OTHER(app_,"FATAL","configure failed, unknown exception");
        FUNCTION_ERR("configure failed, unknown exception", "");
        std::exception e;
        monitorablesWidget_->handleMonitorException(e, "Unknown c++ exception during start: ");
        monitorablesWidget_->analyseAndGenerateAlarms();
        XCEPT_RAISE(xcept::Exception,"configure failed, unknown exception");
    }
}

void XdaqHardwareAccess::disable() {
	try {
		//rpct::MutexHandler handler(hardwareMutex_); //hangs stipping histogramming
		histogramsWidget_->stop();
		//monitorablesWidget_->stopMonitoring();
		getSystem()->disable();
	}
	catch (std::exception& e) {
		ALARM_STD(app_,"FATAL",e);
		FUNCTION_ERR("", e.what());
		monitorablesWidget_->handleMonitorException(e, "Exception during stop: ");
		monitorablesWidget_->analyseAndGenerateAlarms();
		XCEPT_RAISE(xcept::Exception, e.what());
	}
	catch (...) {
		ALARM_OTHER(app_,"FATAL","configure failed, unknown exception");
		FUNCTION_ERR("configure failed, unknown exception", "");
		std::exception e;
		monitorablesWidget_->handleMonitorException(e, "Unknown c++ exception during stop: ");
		monitorablesWidget_->analyseAndGenerateAlarms();
		XCEPT_RAISE(xcept::Exception,"configure failed, unknown exception");
	}
}

xoap::MessageReference XdaqHardwareAccess::onGetCratesInfo(xoap::MessageReference msg) throw (xoap::exception::Exception) {

    LOG4CPLUS_DEBUG(this->getLogger(), __FUNCTION__);
    try {
        if (xdaqIIAccess_ == 0) {
            XCEPT_RAISE(xoap::exception::Exception, "xdaqIIAccess_ == 0, system is not built???");
        }
        rpct::MutexHandler handler(hardwareMutex_);
        return xdaqIIAccess_->getCratesInfo(msg);
    }
    catch (TException& e) {
        LOG4CPLUS_ERROR(getLogger(), e.what());
        XCEPT_RAISE(xoap::exception::Exception, e.what());
    }
}

xoap::MessageReference XdaqHardwareAccess::onGetBoardsInfo(xoap::MessageReference msg) throw (xoap::exception::Exception) {
    LOG4CPLUS_DEBUG(this->getLogger(), __FUNCTION__);
    rpct::MutexHandler handler(hardwareMutex_);
    return xdaqIIAccess_->getBoardsInfo(msg);
}

xoap::MessageReference XdaqHardwareAccess::onGetDevicesInfo(xoap::MessageReference msg) throw (xoap::exception::Exception) {

    LOG4CPLUS_DEBUG(this->getLogger(), __FUNCTION__);
    rpct::MutexHandler handler(hardwareMutex_);
    return xdaqIIAccess_->getDevicesInfo(msg);
}

xoap::MessageReference XdaqHardwareAccess::onGetDeviceItemsInfo(xoap::MessageReference msg)
throw (xoap::exception::Exception) {

    LOG4CPLUS_DEBUG(this->getLogger(), __FUNCTION__);
    rpct::MutexHandler handler(hardwareMutex_);
    return xdaqIIAccess_->getDeviceItemsInfo(msg);
}

xoap::MessageReference XdaqHardwareAccess::onReadII(xoap::MessageReference msg) throw (xoap::exception::Exception) {

    LOG4CPLUS_DEBUG(this->getLogger(), __FUNCTION__);
    rpct::MutexHandler handler(hardwareMutex_);
    return xdaqIIAccess_->readII(msg);
}

xoap::MessageReference XdaqHardwareAccess::onWriteII(xoap::MessageReference msg) throw (xoap::exception::Exception) {

    LOG4CPLUS_DEBUG(this->getLogger(), __FUNCTION__);
    rpct::MutexHandler handler(hardwareMutex_);
    return xdaqIIAccess_->writeII(msg);
}

