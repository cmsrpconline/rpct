
#include "rpct/ts/worker/LBCellContext.h"
#include "rpct/ts/worker/xdaqlboxaccess/XdaqLBoxAccess.h"
#include "rpct/ts/worker/FebConnectivityTest.h"
#include "rpct/xdaqutils/LogUtils.h"

#include "ts/exception/CellException.h"
#include "ts/toolbox/CellToolbox.h"
#include "ts/framework/CellAbstract.h"
#include "ts/framework/CellAbstractContext.h"

#include <string>
#include <cstdlib>

#include "toolbox/string.h"
#include "xcept/tools.h"
#include "log4cplus/logger.h"



rpcttsworker::LBCellContext::LBCellContext(log4cplus::Logger& log, 
                                           tsframework::CellAbstract* cell)
    : tsframework::CellAbstractContext(log, cell)
    , RpctCellContext(log, cell)
    , rpct::ts::worker::FebConfigurationCellContext(log, cell)
    , lboxAccess_(0)
    , febconnectivitytest_(0)
{
    // Debug: log4cplus::Logger::getRoot().setLogLevel(log4cplus::TRACE_LOG_LEVEL);
    logger_ = log4cplus::Logger::getInstance(getLogger().getName() + ".CellContext");
        try {
            lboxAccess_    = new XdaqLBoxAccess(cell,
                                                monitorablesWidget_,
                                                histogramsWidget_);
            //(cell, 
        } catch (xcept::Exception& e) {
            std::ostringstream msg;
            msg << "error creating XdaqLBoxAccess: ";
            msg << xcept::stdformat_exception_history(e);
            LOG4CPLUS_ERROR(logger_, msg.str());
	    lboxAccess_ = 0;
	    initializationErrorStr_ = msg.str();
        }
        try {
            if (lboxAccess_)
                febconnectivitytest_ = new rpcttsworker::FebConnectivityTest(*this, *lboxAccess_);
            else
                LOG4CPLUS_ERROR(logger_
                                , "Could not create FebConnectivityTest: no LBoxAccess available.");
        } catch (xcept::Exception & e) {
            LOG4CPLUS_ERROR(logger_
                            , "Error creating FebConnectivityTest: "
                            << xcept::stdformat_exception_history(e));
	    febconnectivitytest_ = 0;
        } catch (...) {
            LOG4CPLUS_ERROR(logger_
                            , "Error creating FebConnectivityTest: unknown exception.");
	    febconnectivitytest_ = 0;
        }
}

rpcttsworker::LBCellContext::~LBCellContext()
{
    if (lboxAccess_)
        delete lboxAccess_;
    if (febconnectivitytest_)
        delete febconnectivitytest_;
}

std::string rpcttsworker::LBCellContext::getClassName()
{
  return "CellContext";
}


XdaqLBoxAccess* rpcttsworker::LBCellContext::getLBoxAccess(){
	if (!lboxAccess_) {
		std::string msg = std::string("There was an exception during initialization, try restarting the application. The exception was: ") + initializationErrorStr_;
		LOG4CPLUS_ERROR(logger_, msg);
		XCEPT_RAISE(xcept::Exception, msg);
	}
	return lboxAccess_;
}

toolbox::BSem & rpcttsworker::LBCellContext::getHardwareMutex()
{
    return *(getLBoxAccess()->getHardwareMutex());
}

rpct::xdaqutils::xdaqFebSystem & rpcttsworker::LBCellContext::getFebSystem()
{
	if (!lboxAccess_) {
		std::string msg = std::string("There is an exception during initialization, try restarting cell. The exception description was : ") + initializationErrorStr_;
		LOG4CPLUS_ERROR(logger_, msg);
		XCEPT_RAISE(xcept::Exception, msg);	}
    return getLBoxAccess()->getFebSystem();
}

rpcttsworker::FebConnectivityTest & rpcttsworker::LBCellContext::getFebConnectivityTest()
{
    if (!febconnectivitytest_)
        {
            std::string _msg("Requesting FebConnectivityTest, but it was not created.");
            LOG4CPLUS_ERROR(logger_, _msg);
            XCEPT_RAISE(xcept::Exception, _msg);
	}
    return *febconnectivitytest_;
}

