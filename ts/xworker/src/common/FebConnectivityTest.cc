#include "rpct/ts/worker/FebConnectivityTest.h"

#include <cstdlib>
#include <sstream>
#include <algorithm>

#include "sqlite3.h"

#include "rpct/ts/worker/xdaqlboxaccess/XdaqLBoxAccess.h"

#include "toolbox/task/WaitingWorkLoop.h"

#include "rpct/devices/LinkSystem.h"
#include "rpct/devices/FebSystem.h"
#include "rpct/devices/LinkBox.h"
#include "rpct/devices/cb.h"
#include "rpct/devices/LinkBoard.h"
#include "rpct/devices/RPCFebAccessPoint.h"
#include "rpct/devices/FebDistributionBoard.h"
#include "rpct/devices/FebBoard.h"
#include "rpct/devices/FebPart.h"
#include "rpct/devices/FebChip.h"

#include "rpct/FebConnectivityTest/LinkBox.h"
#include "rpct/FebConnectivityTest/ControlBoard.h"
#include "rpct/FebConnectivityTest/LinkBoard.h"
#include "rpct/FebConnectivityTest/FebDistributionBoard.h"
#include "rpct/FebConnectivityTest/FebBoard.h"
#include "rpct/FebConnectivityTest/FebPart.h"
#include "rpct/FebConnectivityTest/FebChip.h"
#include "rpct/FebConnectivityTest/FebConnector.h"
#include "rpct/FebConnectivityTest/FebConfiguration.h"
#include "rpct/FebConnectivityTest/FebConnectivityTest.h"
#include "rpct/FebConnectivityTest/FebThresholdScan.h"

#include "rpct/FebConnectivityTest/FebSystem.h"
#include "rpct/hwd/DeviceFlags.h"
#include "rpct/hwd/HardwareStorage.h"
#include "rpct/hwd/Configuration.h"
#include "rpct/hwd/DeviceConfiguration.h"
#include "rpct/hwd/StandaloneDeviceConfiguration.h"
#include "rpct/hwd/Observables.h"
#include "rpct/hwd/DeviceObservables.h"

#include "rpct/xdaqutils/XdaqCommonUtils.h"

#include "rpct/tools/Date.h"
#include "rpct/tools/Mutex.h"

#include "toolbox/BSem.h"
#include "ts/framework/CellAbstract.h"
#include "ts/framework/CellAbstractContext.h"

namespace rpcttsworker {

FebConnectivityTest::FebConnectivityTest(tsframework::CellAbstractContext & _context
                                         , XdaqLBoxAccess & _xdaqlboxaccess)
    : run_wl_(new toolbox::task::WaitingWorkLoop("FebConnectivityTestWorkLoop"))
    , febconfiguration_action_(0), connectivitytest_action_(0), thresholdscan_action_(0)
    , xdaqlboxaccess_(_xdaqlboxaccess)
    , cell_(*(_context.getCell()))
    , dbclient_(_context.getCell())
    , fct_febsystem_(0)
    , cellname_(cell_.getName())
    , filename_(":memory:")
    , hwstorage_(0)
    , count_duration_(2)
    , vth_(86), vmon_(1434)
    , vth_min_(46), vth_step_size_(4), vth_n_steps_(20)
    , vth_low_(16), vth_high_(164)
    , auto_correct_(true)
    , use_pulser_(false)
    , include_disabled_(true)
    , store_output_(true)
    , logger_(log4cplus::Logger::getInstance("FebConnectivityTest"))
    , logger_widget_(_context)
    , progress_(logger_)
    , progress_widget_(_context, progress_, "Progress", true)
{
    std::replace(cellname_.begin(), cellname_.end(), ' ', '_');
    std::replace(cellname_.begin(), cellname_.end(), '+', 'P');
    std::replace(cellname_.begin(), cellname_.end(), '-', 'N');

    febconfiguration_action_
        = toolbox::task::bind(this
                              , &FebConnectivityTest::febConfiguration_wl
                              , "febconfiguration_action");
    connectivitytest_action_
        = toolbox::task::bind(this
                              , &FebConnectivityTest::connectivityTest_wl
                              , "connectivitytest_action");
    thresholdscan_action_
        = toolbox::task::bind(this
                              , &FebConnectivityTest::thresholdScan_wl
                              , "thresholdscan_action");
    // as for XdaqLBoxAccess
    std::string _url = cell_.getApplicationDescriptor()->getContextDescriptor()->getURL();
    std::string::size_type _start = _url.find("http://");
    if (_start == std::string::npos)
        _start = 0;
    else
        _start += 7;
    std::string::size_type _stop = _url.find_first_of(':', _start);
    host_ = _url.substr(_start, _stop - _start);
    _start = _stop + 1;
    _stop = _url.find_first_not_of("0123456789");
    std::istringstream _sstr(_url.substr(_start, _stop - _start));
    _sstr >> port_;

    run_wl_->activate();

    logger_.addAppender(SharedAppenderPtr(&(logger_widget_.getAppender())));
    logger_widget_.setIsOwned(false);

    progress_widget_.setIsOwned(false);
}

FebConnectivityTest::~FebConnectivityTest()
{
    progress_.terminate();

    // waitingworkloop problem: no ~WaitingWorkloop
    try {
        if (run_wl_->isActive())
            run_wl_->cancel();
    } catch (...) {}

    delete run_wl_;

    delete febconfiguration_action_;
    delete connectivitytest_action_;
    delete thresholdscan_action_;
}

toolbox::BSem & FebConnectivityTest::getHardwareMutex()
{
    return *(xdaqlboxaccess_.getHardwareMutex());
}

rpct::LinkSystem & FebConnectivityTest::getLinkSystem()
{
    return *(xdaqlboxaccess_.getLinkSystem());
}

rpct::FebSystem & FebConnectivityTest::getFebSystem()
{
    return xdaqlboxaccess_.getFebSystem();
}

void FebConnectivityTest::prepareFctFebSystem()
{
    LOG4CPLUS_INFO(logger_, "Preparing FebConnectivityTest output: " << filename_);

    rpct::FebSystem & _febsystem = getFebSystem();
    rpct::LinkSystem & _linksystem = getLinkSystem();
    rpct::tools::Date _date;
    hwstorage_ = new rpct::hwd::HardwareStorage(filename_, rpct::hwd::HardwareStorage::write_);
    fct_febsystem_ = new rpct::fct::FebSystem(*hwstorage_, "FebSystem");
    fct_febsystem_->registerSystem(cellname_);

    rpct::hwd::Transaction _transaction(*hwstorage_, false);
    // needed for FebConnectors
    std::map<int, rpct::fct::FebBoard *> _rpcid_febboard;
    std::map<int, rpct::fct::LinkBoard *> _rpcid_linkboard;
    // needed for FebSystem
    std::map<int, rpct::fct::ControlBoard *> _rpcid_controlboard;

    // LinkSystem
    rpct::LinkSystem::LinkBoxList & _rpct_linkboxes = _linksystem.getLinkBoxes();
    rpct::LinkSystem::LinkBoxList::iterator _rpct_linkbox_end = _rpct_linkboxes.end();
    for (rpct::LinkSystem::LinkBoxList::iterator _rpct_linkbox = _rpct_linkboxes.begin()
             ; _rpct_linkbox != _rpct_linkbox_end ; ++_rpct_linkbox)
        {
            _transaction.begin();
            rpct::fct::LinkBox & _fct_linkbox
                = rpct::fct::LinkBox::registerDevice(*fct_febsystem_
                                                     , (*_rpct_linkbox)->getId()
                                                     , rpct::hwd::DeviceFlags()
                                                     , (*_rpct_linkbox)->getDescription()
                                                     , (*_rpct_linkbox)->getLabel());
            _fct_linkbox.setRpctLinkBox(*(*_rpct_linkbox));

            for (int _board = 10 ; _board <= 20 ; _board += 10)
                {
                    rpct::HalfBox & _rpct_halfbox = *((*_rpct_linkbox)->getHalfBox(_board));
                    rpct::ICB & _rpct_controlboard = _rpct_halfbox.getCb();
                    rpct::fct::ControlBoard & _fct_controlboard
                        = rpct::fct::ControlBoard::registerDevice(*fct_febsystem_
                                                                  , _rpct_controlboard.getId()
                                                                  , rpct::hwd::DeviceFlags()
                                                                  , _rpct_controlboard.getDescription()
                                                                  , _fct_linkbox
                                                                  , _rpct_controlboard.getPosition());
                    _rpcid_controlboard.insert(std::pair<int, rpct::fct::ControlBoard *>( _rpct_controlboard.getId()
                                                                                          , &_fct_controlboard));

                    rpct::HalfBox::LinkBoards & _rpct_linkboards = _rpct_halfbox.getLinkBoards();
                    rpct::HalfBox::LinkBoards::iterator _rpct_linkboard_end = _rpct_linkboards.end();
                    for (rpct::HalfBox::LinkBoards::iterator _rpct_linkboard = _rpct_linkboards.begin()
                             ; _rpct_linkboard != _rpct_linkboard_end ; ++_rpct_linkboard)
                        {
                            rpct::fct::LinkBoard & _fct_linkboard
                                = rpct::fct::LinkBoard::registerDevice(*fct_febsystem_
                                                                       , (*_rpct_linkboard)->getId()
                                                                       , rpct::hwd::DeviceFlags()
                                                                       , (*_rpct_linkboard)->getDescription()
                                                                       , _fct_linkbox
                                                                       , (*_rpct_linkboard)->getPosition());
                            _fct_linkboard.setRpctLinkBoard(*(*_rpct_linkboard));
                            _fct_linkboard.setRpctSynCoder((*_rpct_linkboard)->getSynCoder());
                            _rpcid_linkboard.insert(std::pair<int, rpct::fct::LinkBoard *>((*_rpct_linkboard)->getId()
                                                                                           , &_fct_linkboard));
                        }
                }
            _transaction.commit();
        }

    // FebSystem
    rpct::fct::FebDistributionBoard * _last_fdb(0);
    rpct::fct::FebBoard * _last_febboard(0);
    rpct::fct::FebPart * _last_febpart(0);

    _transaction.begin();

    rpct::FebSystemItem::iterator _fsi_end = _febsystem.getTreeChild()->tree_end();
    for (rpct::FebSystemItem::iterator _fsi = _febsystem.getTreeChild()->tree_begin()
             ; _fsi != _fsi_end ; ++_fsi)
        {
            switch (_fsi.depth())
                {
                case 0: // FebAccessPoint
                    break;
                case 1: // FebDistributionBoard
                    {
                        rpct::FebDistributionBoard & _rpct_fdb
                            = dynamic_cast<rpct::FebDistributionBoard &>(*(*_fsi));
                        rpct::RPCFebAccessPoint & _rpct_fap
                            = dynamic_cast<rpct::RPCFebAccessPoint &>(_rpct_fdb.getFebAccessPoint());
                        rpct::fct::ControlBoard & _fct_controlboard
                            = *(_rpcid_controlboard[_rpct_fap.getControlBoard().getId()]);
                        _last_fdb
                            = &(rpct::fct::FebDistributionBoard::registerDevice(*fct_febsystem_
                                                                                , _fct_controlboard
                                                                                , _rpct_fdb.getChannel()));
                    }
                    break;
                case 2: // FebBoard
                    {
                        rpct::FebBoard & _rpct_febboard
                            = dynamic_cast<rpct::FebBoard &>(*(*_fsi));
                        rpct::hwd::DeviceFlags _deviceflags;
                        if (_rpct_febboard.isDisabled())
                            _deviceflags.setDisabled();
                        _last_febboard
                            = &(rpct::fct::FebBoard::registerDevice(*fct_febsystem_
                                                                    , _rpct_febboard.getId()
                                                                    , _deviceflags
                                                                    , _rpct_febboard.getDescription()
                                                                    , _rpct_febboard.getFebType() ? 1 : 0
                                                                    , *_last_fdb
                                                                    , _rpct_febboard.getTreePosition()));
                        _last_febboard->setRpctFebBoard(_rpct_febboard);
                        _rpcid_febboard.insert(std::pair<int, rpct::fct::FebBoard *>(_rpct_febboard.getId()
                                                                                     , _last_febboard));
                    }
                    break;
                case 3: // FebPart
                    {
                        rpct::FebPart & _rpct_febpart
                            = dynamic_cast<rpct::FebPart &>(*(*_fsi));
                        _last_febpart
                            = &(rpct::fct::FebPart::registerDevice(*fct_febsystem_
                                                                   , *_last_febboard
                                                                   , _rpct_febpart.getTreePosition()));
                        _last_febpart->setRpctFebPart(_rpct_febpart);
                    }
                    break;
                case 4: // FebChip
                    {
                        rpct::FebChip & _rpct_febchip
                            = dynamic_cast<rpct::FebChip &>(*(*_fsi));
                        rpct::hwd::DeviceFlags _deviceflags;
                        if (_rpct_febchip.isDisabled())
                            _deviceflags.setDisabled();
                        rpct::fct::FebChip & _fct_febchip = rpct::fct::FebChip::registerDevice(*fct_febsystem_
                                                                                               , _rpct_febchip.getId()
                                                                                               , _deviceflags
                                                                                               , *_last_febpart
                                                                                               , _rpct_febchip.getTreePosition() % 2);
                        _fct_febchip.setRpctFebChip(_rpct_febchip);
                    }
                    break;
                }
        }
    _transaction.commit();

    // FebConnectors
    _transaction.begin();
    rpct::xdaqutils::XdaqDbServiceClient::FebConnectorStripsVector * _fcs
        = dbclient_.getFebConnectorStrips(host_, port_);
    for (rpct::xdaqutils::XdaqDbServiceClient::FebConnectorStripsVector::const_iterator _fcs_bag = _fcs->begin()
             ; _fcs_bag != _fcs->end() ; ++_fcs_bag)
        {
            rpct::xdaqutils::FebConnectorStrips const & _fcs = _fcs_bag->bag;
            if (_rpcid_febboard.find(_fcs.getFebBoardId()) == _rpcid_febboard.end())
                {
                    LOG4CPLUS_WARN(logger_, "Skipping FebConnector " << _fcs.getId()
                                   << ": the FebBoard " << _fcs.getFebBoardId() << " is missing.");
                }
            else if (_rpcid_linkboard.find(_fcs.getLinkBoardId()) == _rpcid_linkboard.end())
                {
                    LOG4CPLUS_WARN(logger_, "Skipping FebConnector " << _fcs.getId()
                                   << ": the LinkBoard " << _fcs.getLinkBoardId() << " is missing.");
                }
            else
                rpct::fct::FebConnector::registerDevice(*fct_febsystem_
                                                        , _fcs.getId()
                                                        , rpct::tools::RollId(_fcs.getLocation(), _fcs.getPartition())
                                                        , _fcs.getRollConnector()
                                                        , _fcs.getPins()
                                                        , _fcs.getFirstStrip()
                                                        , _fcs.getSlope()
                                                        , _rpcid_febboard[_fcs.getFebBoardId()]->getFebPart(_fcs.getFebBoardConnector())
                                                        , *(_rpcid_linkboard[_fcs.getLinkBoardId()])
                                                        , _fcs.getLinkBoardInput());
        }
    delete _fcs;
    _transaction.commit();
}

rpct::fct::FebSystem & FebConnectivityTest::getFctFebSystem()
{
    if (fct_febsystem_)
        return *fct_febsystem_;
    throw rpct::exception::Exception("FebConnectivityTest FebSystem has not been created yet");
}

rpct::hwd::HardwareStorage & FebConnectivityTest::getHardwareStorage()
{
    if (hwstorage_)
        return *hwstorage_;
    throw rpct::exception::Exception("FebConnectivityTest HardwareStorage has not been created yet");
}

void FebConnectivityTest::febConfiguration()
{
    LOG4CPLUS_INFO(logger_, "Running FebConfiguration");
    rpct::hwd::Transaction _transaction(*hwstorage_, false);

    _transaction.begin();
    rpct::fct::FebConfiguration::registerDevice(*fct_febsystem_
                                                , selection_
                                                , count_duration_
                                                , vth_, vmon_
                                                , auto_correct_
                                                , include_disabled_
                                                , "FebConfiguration");
    _transaction.commit();

    if (!store_output_)
        fct_febsystem_->setPublishing(false);

    rpct::hwd::SnapshotType const & _snapshot_type = fct_febsystem_->registerSnapshotType("FebConfiguration");

    rpct::hwd::DeviceFlagsMask _deviceflags;
    if (!include_disabled_)
        _deviceflags.requireDisabled(false);

    rpct::fct::FebChipType const & _febchip_type
        = dynamic_cast<rpct::fct::FebChipType const &>(fct_febsystem_->getDeviceType("FebChip"));
    rpct::fct::FebPartType const & _febpart_type
        = dynamic_cast<rpct::fct::FebPartType const &>(fct_febsystem_->getDeviceType("FebPart"));
    rpct::fct::LinkBoardType const & _linkboard_type
        = dynamic_cast<rpct::fct::LinkBoardType const &>(fct_febsystem_->getDeviceType("LinkBoard"));
    rpct::hwd::DeviceType const & _febconnector_type
        = fct_febsystem_->getDeviceType("FebConnector");

    std::vector<rpct::hwd::Device *> const & _febconnectors
        = fct_febsystem_->getDevices(_febconnector_type);

    _transaction.begin();

    rpct::hwd::Configuration _feb_configuration
        = fct_febsystem_->registerConfiguration("FebConfiguration.Feb");
    rpct::hwd::DeviceConfiguration & _febchip_deviceconfiguration
        = _feb_configuration.registerDeviceTypeConfiguration(_febchip_type);
    _febchip_deviceconfiguration.registerParameterSetting(_febchip_type.getVThSetParameter(), vth_);
    _febchip_deviceconfiguration.registerParameterSetting(_febchip_type.getVMonSetParameter(), vmon_);
    _febchip_deviceconfiguration.registerParameterSetting(_febchip_type.getAutoCorrectParameter(), auto_correct_);

    rpct::hwd::Configuration _linkboard_configuration
        = fct_febsystem_->registerConfiguration("FebConfiguration.LinkBoard");
    rpct::hwd::DeviceConfiguration & _linkboard_deviceconfiguration
        = _linkboard_configuration.registerDeviceTypeConfiguration(_linkboard_type);
    ::int64_t _duration = count_duration_.seconds() * rpct::fct::LinkBoard::bx_per_second_
          + count_duration_.nanoseconds() / rpct::fct::LinkBoard::nanoseconds_per_bx_;

    _linkboard_deviceconfiguration.registerParameterSetting(_linkboard_type.getDurationParameter()
                                                            , _duration);

    rpct::hwd::Observables _linkboard_observables;
    rpct::hwd::DeviceObservables & _linkboard_deviceobservables
        = _linkboard_observables.addDeviceObservables(_linkboard_type);
    _linkboard_deviceobservables.addParameter(_linkboard_type.getCountsParameter());

    rpct::hwd::Observables _feb_observables;
    rpct::hwd::DeviceObservables & _febchip_deviceobservables
        = _feb_observables.addDeviceObservables(_febchip_type);
    _febchip_deviceobservables.addParameter(_febchip_type.getVThWriteParameter());
    _febchip_deviceobservables.addParameter(_febchip_type.getVMonWriteParameter());
    _febchip_deviceobservables.addParameter(_febchip_type.getVThReadParameter());
    _febchip_deviceobservables.addParameter(_febchip_type.getVMonReadParameter());
    rpct::hwd::DeviceObservables & _febpart_deviceobservables
        = _feb_observables.addDeviceObservables(_febpart_type);
    _febpart_deviceobservables.addParameter(_febpart_type.getTemperatureParameter());

    std::map<rpct::hwd::integer_type, int> _nfebconnectors_per_linkbox;
    int _nfebconnectors(0);
    { // list LinkBoxes
        std::vector<rpct::hwd::Device *>::const_iterator _febconnector_it_end = _febconnectors.end();
        for (std::vector<rpct::hwd::Device *>::const_iterator _febconnector_it = _febconnectors.begin()
                 ; _febconnector_it != _febconnector_it_end ; ++_febconnector_it)
            {
                rpct::fct::FebConnector & _febconnector
                    = dynamic_cast<rpct::fct::FebConnector &>(*(*_febconnector_it));
                if (selection_.matches(_febconnector.getRoll()))
                    {
                        rpct::fct::LinkBoard & _linkboard = _febconnector.getLinkBoard();
                        rpct::fct::LinkBox & _linkbox = _linkboard.getLinkBox();
                        rpct::fct::FebPart const & _febpart = _febconnector.getFebPart();

                        _febconnector.setSelected(true);
                        _linkboard.setSelected(true);
                        _linkbox.setSelected(true);

                        ++_nfebconnectors;
                        ++(_nfebconnectors_per_linkbox[_linkbox.getId()]);

                        if (_febconnector.getFebPart().getFebBoard().getDeviceFlags().isDisabled())
                            {
                                LOG4CPLUS_WARN(logger_, "FebBoard is disabled for selected connector "
                                               << _febconnector.getRoll() << "_c" << _febconnector.getRollConnector()
                                               << (include_disabled_ ? ", including anyway" : ""));
                            }

                        _feb_configuration.registerDeviceConfiguration(_febpart.getFebChip(0)
                                                                       , _febchip_deviceconfiguration);
                        _feb_configuration.registerDeviceConfiguration(_febpart.getFebChip(1)
                                                                       , _febchip_deviceconfiguration);

                        _feb_observables.addDeviceObservables(_febpart, _febpart_deviceobservables);
                        _feb_observables.addDeviceObservables(_febpart.getFebChip(0)
                                                              , _febchip_deviceobservables);
                        _feb_observables.addDeviceObservables(_febpart.getFebChip(1)
                                                              , _febchip_deviceobservables);

                        _linkboard_configuration.registerDeviceConfiguration(_linkboard
                                                                             , _linkboard_deviceconfiguration);
                        _linkboard_observables.addDeviceObservables(_linkboard
                                                                    , _linkboard_deviceobservables);
                    }
            }
    }
    _transaction.commit();

    getProgress().init();
    getProgress().setMaximum(_nfebconnectors);
    getProgress().start();
    bool _is_terminated(false);

    getProgress().checkPaused();
    _is_terminated = getProgress().checkTerminated();

    std::vector<rpct::fct::LinkBox *> _linkboxes(fct_febsystem_->getLinkBoxes());
    std::vector<rpct::fct::LinkBox *>::iterator _linkboxes_it_end(_linkboxes.end());
    for (std::vector<rpct::fct::LinkBox *>::iterator _linkbox_it = _linkboxes.begin()
             ; _linkbox_it != _linkboxes_it_end && !_is_terminated ; ++_linkbox_it)
        if ((*_linkbox_it)->isSelected())
            {
                rpct::fct::LinkBox & _linkbox(*(*_linkbox_it));
                std::vector<rpct::fct::LinkBoard *> const & _linkboards (_linkbox.getLinkBoards());

                getProgress().setDescription("Processing LinkBox " + _linkbox.getName());

                _linkbox.setActive(true);
                for (std::vector<rpct::fct::LinkBoard *>::const_iterator _linkboard_it = _linkboards.begin()
                         ; _linkboard_it != _linkboards.end() ; ++_linkboard_it)
                    if (*_linkboard_it && (*_linkboard_it)->isSelected())
                        (*_linkboard_it)->setActive(true);

                {
                    rpct::tools::TLock<toolbox::BSem> _hwlock(getHardwareMutex());
                    std::list<rpct::HalfBoxAccessHandler> _halfbox_handlers;
                    if (_linkbox.getRpctLinkBox().getHalfBox10())
                        _halfbox_handlers.push_back(rpct::HalfBoxAccessHandler(*(_linkbox.getRpctLinkBox().getHalfBox10())));
                    if (_linkbox.getRpctLinkBox().getHalfBox20())
                        _halfbox_handlers.push_back(rpct::HalfBoxAccessHandler(*(_linkbox.getRpctLinkBox().getHalfBox20())));

                    _transaction.begin();
                    _linkbox.configure(_feb_configuration, _deviceflags);
                    _transaction.commit();
                    _transaction.begin();
                    _linkbox.monitor(_feb_observables, _deviceflags);
                    _transaction.commit();
                    if (_duration > 0)
                        {
                            _transaction.begin();
                            _linkbox.configure(_linkboard_configuration, _deviceflags);
                            _transaction.commit();
                            _transaction.begin();
                            _linkbox.monitor(_linkboard_observables, _deviceflags);
                            _transaction.commit();
                        }
                    _transaction.begin();
                    fct_febsystem_->takeSnapshot(_snapshot_type);
                    _transaction.commit();
                }

                getProgress().addValue(_nfebconnectors_per_linkbox[_linkbox.getId()]);

                getProgress().checkPaused();
                _is_terminated = getProgress().checkTerminated();
                _linkbox.setActive(false);
                for (std::vector<rpct::fct::LinkBoard *>::const_iterator _linkboard_it = _linkboards.begin()
                         ; _linkboard_it != _linkboards.end() ; ++_linkboard_it)
                    if (*_linkboard_it && (*_linkboard_it)->isSelected())
                        (*_linkboard_it)->setActive(false);
            }

    LOG4CPLUS_INFO(logger_, "FebConfiguration done");
}

void FebConnectivityTest::connectivityTest()
{
    LOG4CPLUS_INFO(logger_, "Running Connectivitytest");
    rpct::hwd::Transaction _transaction(*hwstorage_, false);

    _transaction.begin();
    rpct::fct::FebConnectivityTest & _febconnectivitytest
        = rpct::fct::FebConnectivityTest::registerDevice(*fct_febsystem_
                                                         , selection_
                                                         , count_duration_
                                                         , vth_low_, vth_high_
                                                         , vmon_
                                                         , auto_correct_
                                                         , use_pulser_
                                                         , include_disabled_
                                                         , "ConnectivityTest");
    _transaction.commit();

    rpct::hwd::SnapshotType const & _snapshot_type = fct_febsystem_->registerSnapshotType("ConnectivityTest");

    rpct::hwd::DeviceFlagsMask _deviceflags;
    if (!include_disabled_)
        _deviceflags.requireDisabled(false);

    rpct::fct::FebChipType const & _febchip_type
        = dynamic_cast<rpct::fct::FebChipType const &>(fct_febsystem_->getDeviceType("FebChip"));
    rpct::fct::FebPartType const & _febpart_type
        = dynamic_cast<rpct::fct::FebPartType const &>(fct_febsystem_->getDeviceType("FebPart"));
    rpct::fct::LinkBoardType const & _linkboard_type
        = dynamic_cast<rpct::fct::LinkBoardType const &>(fct_febsystem_->getDeviceType("LinkBoard"));
    rpct::hwd::DeviceType const & _febconnector_type
        = fct_febsystem_->getDeviceType("FebConnector");

    std::vector<rpct::hwd::Device *> const & _febconnectors
        = fct_febsystem_->getDevices(_febconnector_type);

    _transaction.begin();

    rpct::hwd::StandaloneDeviceConfiguration _febchip_deviceconfiguration_low
        = fct_febsystem_->registerDeviceTypeConfiguration(_febchip_type);
    _febchip_deviceconfiguration_low.registerParameterSetting(_febchip_type.getVThSetParameter(), vth_low_);
    _febchip_deviceconfiguration_low.registerParameterSetting(_febchip_type.getVMonSetParameter(), vmon_);
    _febchip_deviceconfiguration_low.registerParameterSetting(_febchip_type.getAutoCorrectParameter(), auto_correct_);

    rpct::hwd::StandaloneDeviceConfiguration _febchip_deviceconfiguration_high
        = fct_febsystem_->registerDeviceTypeConfiguration(_febchip_type);
    _febchip_deviceconfiguration_high.registerParameterSetting(_febchip_type.getVThSetParameter(), vth_high_);
    _febchip_deviceconfiguration_high.registerParameterSetting(_febchip_type.getVMonSetParameter(), vmon_);
    _febchip_deviceconfiguration_high.registerParameterSetting(_febchip_type.getAutoCorrectParameter(), auto_correct_);

    rpct::hwd::Configuration _linkboard_configuration
        = fct_febsystem_->registerConfiguration("ConnectivityTest.LinkBoard");
    rpct::hwd::DeviceConfiguration & _linkboard_deviceconfiguration
        = _linkboard_configuration.registerDeviceTypeConfiguration(_linkboard_type);
    ::int64_t _duration = count_duration_.seconds() * rpct::fct::LinkBoard::bx_per_second_
          + count_duration_.nanoseconds() / rpct::fct::LinkBoard::nanoseconds_per_bx_;

    rpct::hwd::Configuration _linkboard_pulser_start_configuration
        = fct_febsystem_->registerConfiguration("ConnectivityTest.LinkBoard.Pulser.Start");
    rpct::hwd::DeviceConfiguration & _linkboard_pulser_start_deviceconfiguration
        = _linkboard_pulser_start_configuration.registerDeviceTypeConfiguration(_linkboard_type);
    rpct::hwd::Configuration _linkboard_pulser_stop_configuration
        = fct_febsystem_->registerConfiguration("ConnectivityTest.LinkBoard.Pulser.Stop");
    rpct::hwd::DeviceConfiguration & _linkboard_pulser_stop_deviceconfiguration
        = _linkboard_pulser_stop_configuration.registerDeviceTypeConfiguration(_linkboard_type);

    if (use_pulser_)
        {
            std::vector< ::uint32_t> _pulser_data;
            _pulser_data.reserve(250);
            ::uint32_t _pulser_data_block(0xffffff);
            for (std::size_t _channel = 0 ; _channel <= 24 ; ++_channel)
                {
                    _pulser_data_block = 0xffffff & ~(0x1 << _channel);
                    for (std::size_t _bx = 0 ; _bx < 4 ; ++_bx)
                        _pulser_data.push_back(_pulser_data_block);
                    for (std::size_t _bx = 4 ; _bx < 10 ; ++_bx)
                        _pulser_data.push_back(0xffffff);
                }
            _linkboard_pulser_start_deviceconfiguration.registerParameterSetting(_linkboard_type.getPulserDataParameter()
                                                                                 , _pulser_data);
            _linkboard_pulser_start_deviceconfiguration.registerParameterSetting(_linkboard_type.getRunPulserParameter()
                                                                                 , true);
            _linkboard_pulser_stop_deviceconfiguration.registerParameterSetting(_linkboard_type.getRunPulserParameter()
                                                                                , false);
            _duration = (_duration / _pulser_data.size()) * _pulser_data.size();
        }
    _linkboard_deviceconfiguration.registerParameterSetting(_linkboard_type.getDurationParameter()
                                                            , _duration);

    rpct::hwd::Observables _linkboard_observables;
    rpct::hwd::DeviceObservables & _linkboard_deviceobservables
        = _linkboard_observables.addDeviceObservables(_linkboard_type);
    _linkboard_deviceobservables.addParameter(_linkboard_type.getCountsParameter());

    rpct::hwd::Observables _feb_observables;
    rpct::hwd::DeviceObservables & _febchip_deviceobservables
        = _feb_observables.addDeviceObservables(_febchip_type);
    _febchip_deviceobservables.addParameter(_febchip_type.getVThWriteParameter());
    _febchip_deviceobservables.addParameter(_febchip_type.getVMonWriteParameter());
    _febchip_deviceobservables.addParameter(_febchip_type.getVThReadParameter());
    _febchip_deviceobservables.addParameter(_febchip_type.getVMonReadParameter());
    rpct::hwd::DeviceObservables & _febpart_deviceobservables
        = _feb_observables.addDeviceObservables(_febpart_type);
    _febpart_deviceobservables.addParameter(_febpart_type.getTemperatureParameter());

    int _progress_max(0);
    std::map<int, rpct::fct::LinkBox *> _linkboxes;
    { // all high, prepare _linkboard_configuration and list LinkBoxes
        rpct::hwd::Configuration _feb_configuration = fct_febsystem_->registerConfiguration("ConnectivityTest.Start");
        std::vector<rpct::hwd::Device *>::const_iterator _febconnector_it_end = _febconnectors.end();
        for (std::vector<rpct::hwd::Device *>::const_iterator _febconnector_it = _febconnectors.begin()
                 ; _febconnector_it != _febconnector_it_end ; ++_febconnector_it)
            {
                rpct::fct::FebConnector & _febconnector
                    = dynamic_cast<rpct::fct::FebConnector &>(*(*_febconnector_it));
                if (selection_.matches(_febconnector.getRoll()))
                    {
                        rpct::fct::LinkBoard & _linkboard = _febconnector.getLinkBoard();
                        rpct::fct::LinkBox & _linkbox = _linkboard.getLinkBox();
                        rpct::fct::FebPart const & _febpart = _febconnector.getFebPart();

                        _febconnector.setSelected(true);
                        _linkboard.setSelected(true);

                        if (_febconnector.getFebPart().getFebBoard().getDeviceFlags().isDisabled())
                            {
                                LOG4CPLUS_WARN(logger_, "FebBoard is disabled for selected connector "
                                               << _febconnector.getRoll() << "_c" << _febconnector.getRollConnector()
                                               << (include_disabled_ ? ", including anyway" : ""));
                            }

                        _feb_configuration.registerDeviceConfiguration(_febpart.getFebChip(0)
                                                                       , _febchip_deviceconfiguration_high);
                        _feb_configuration.registerDeviceConfiguration(_febpart.getFebChip(1)
                                                                       , _febchip_deviceconfiguration_high);
                        _feb_observables.addDeviceObservables(_febpart, _febpart_deviceobservables);
                        _feb_observables.addDeviceObservables(_febpart.getFebChip(0)
                                                              , _febchip_deviceobservables);
                        _feb_observables.addDeviceObservables(_febpart.getFebChip(1)
                                                              , _febchip_deviceobservables);

                        _linkboard_configuration.registerDeviceConfiguration(_linkboard
                                                                             , _linkboard_deviceconfiguration);
                        _linkboard_observables.addDeviceObservables(_linkboard
                                                                    , _linkboard_deviceobservables);

                        if (use_pulser_)
                            {
                                _linkboard_pulser_start_configuration.registerDeviceConfiguration(_linkboard
                                                                                                  , _linkboard_pulser_start_deviceconfiguration);
                                _linkboard_pulser_stop_configuration.registerDeviceConfiguration(_linkboard
                                                                                                 , _linkboard_pulser_stop_deviceconfiguration);
                            }

                        if (!_linkbox.isSelected()) {
                            _linkbox.setSelected(true);
                            _linkboxes.insert(std::pair<int, rpct::fct::LinkBox *>(_linkbox.getId(), &_linkbox));
                        }
                        ++_progress_max;
                    }
            }
        _transaction.commit();
        if (use_pulser_)
            getProgress().setDescription("Setting all Thresholds high, configuring Pulsers and checking for response");
        else
            getProgress().setDescription("Setting all Thresholds high and checking for noise");
        {
            rpct::tools::TLock<toolbox::BSem> _hwlock(getHardwareMutex());
            std::list<rpct::HalfBoxAccessHandler> _halfbox_handlers;
            // prepare access
            for (std::map<int, rpct::fct::LinkBox *>::iterator _linkbox_it = _linkboxes.begin()
                     ; _linkbox_it != _linkboxes.end() ; ++_linkbox_it)
                {
                    rpct::fct::LinkBox & _linkbox = *(_linkbox_it->second);

                    _linkbox.setActive(true);
                    std::vector<rpct::fct::LinkBoard *> const & _linkboards = _linkbox.getLinkBoards();
                    std::vector<rpct::fct::LinkBoard *>::const_iterator _linkboard_it_end = _linkboards.end();
                    for (std::vector<rpct::fct::LinkBoard *>::const_iterator _linkboard_it = _linkboards.begin()
                             ; _linkboard_it != _linkboard_it_end ; ++_linkboard_it)
                        if (*_linkboard_it && (*_linkboard_it)->isSelected())
                            (*_linkboard_it)->setActive(true);

                    rpct::HalfBox * _halfbox = _linkbox.getRpctLinkBox().getHalfBox10();
                    if (_halfbox)
                        _halfbox_handlers.push_back(rpct::HalfBoxAccessHandler(*_halfbox));
                    _halfbox = _linkbox.getRpctLinkBox().getHalfBox20();
                    if (_halfbox)
                        _halfbox_handlers.push_back(rpct::HalfBoxAccessHandler(*_halfbox));
                }
            // febs
            _transaction.begin();
            fct_febsystem_->configure(_feb_configuration, _deviceflags);
            _transaction.commit();
            _transaction.begin();
            fct_febsystem_->monitor(_feb_observables, _deviceflags);
            _transaction.commit();
            // linkboards
            if (use_pulser_)
                {
                    _transaction.begin();
                    fct_febsystem_->configure(_linkboard_pulser_start_configuration);
                    _transaction.commit();
                }
            _transaction.begin();
            fct_febsystem_->configure(_linkboard_configuration, _deviceflags);
            _transaction.commit();
            _transaction.begin();
            fct_febsystem_->monitor(_linkboard_observables, _deviceflags);
            _transaction.commit();
            _transaction.begin();
            // snapshot
            fct_febsystem_->takeSnapshot(_snapshot_type);
            _transaction.commit();
            for (std::map<int, rpct::fct::LinkBox *>::iterator _linkbox_it = _linkboxes.begin()
                     ; _linkbox_it != _linkboxes.end() ; ++_linkbox_it)
                {
                    (_linkbox_it->second)->setActive(false);
                    std::vector<rpct::fct::LinkBoard *> const & _linkboards = _linkbox_it->second->getLinkBoards();
                    std::vector<rpct::fct::LinkBoard *>::const_iterator _linkboard_it_end = _linkboards.end();
                    for (std::vector<rpct::fct::LinkBoard *>::const_iterator _linkboard_it = _linkboards.begin()
                             ; _linkboard_it != _linkboard_it_end ; ++_linkboard_it)
                        if (*_linkboard_it && (*_linkboard_it)->isSelected())
                            (*_linkboard_it)->setActive(false);
                }
        }
    }

    getProgress().init();
    getProgress().setMaximum(_progress_max);
    getProgress().start();
    // one connector at a time, per linkbox
    bool _is_terminated(false);

    getProgress().checkPaused();
    _is_terminated = getProgress().checkTerminated();

    std::map<int, rpct::fct::LinkBox *>::iterator _linkbox_it_end = _linkboxes.end();
    for (std::map<int, rpct::fct::LinkBox *>::iterator _linkbox_it = _linkboxes.begin()
             ; _linkbox_it != _linkbox_it_end && !_is_terminated ; ++_linkbox_it)
        {
            rpct::fct::LinkBox & _linkbox = *(_linkbox_it->second);
            _linkbox.setActive(true);
            getProgress().setDescription("Processing LinkBox " + _linkbox.getName());
            std::vector<rpct::fct::LinkBoard *> const & _linkboards = _linkbox.getLinkBoards();
            std::vector<rpct::fct::LinkBoard *>::const_iterator _linkboard_it_end = _linkboards.end();
            for (std::vector<rpct::fct::LinkBoard *>::const_iterator _linkboard_it = _linkboards.begin()
                     ; _linkboard_it != _linkboard_it_end && !_is_terminated ; ++_linkboard_it)
                if (*_linkboard_it && (*_linkboard_it)->isSelected())
                    {
                        (*_linkboard_it)->setActive(true);
                        rpct::tools::TLock<toolbox::BSem> _hwlock(getHardwareMutex());
                        std::list<rpct::HalfBoxAccessHandler> _halfbox_handlers;
                        if (_linkbox.getRpctLinkBox().getHalfBox10())
                            _halfbox_handlers.push_back(rpct::HalfBoxAccessHandler(*(_linkbox.getRpctLinkBox().getHalfBox10())));
                        if (_linkbox.getRpctLinkBox().getHalfBox20())
                            _halfbox_handlers.push_back(rpct::HalfBoxAccessHandler(*(_linkbox.getRpctLinkBox().getHalfBox20())));

                        std::vector<rpct::fct::FebConnector *> const & _febconnectors
                            = (*_linkboard_it)->getFebConnectors();
                        std::vector<rpct::fct::FebConnector *>::const_iterator _febconnector_it_end = _febconnectors.end();
                        for (std::vector<rpct::fct::FebConnector *>::const_iterator _febconnector_it = _febconnectors.begin()
                                 ; _febconnector_it != _febconnector_it_end && !_is_terminated ; ++_febconnector_it)
                            if (*_febconnector_it && (*_febconnector_it)->isSelected())
                                {
                                    rpct::fct::FebConnector & _febconnector
                                        = dynamic_cast<rpct::fct::FebConnector &>(*(*_febconnector_it));

                                    _febconnectivitytest.setActiveFebConnectorId(_febconnector.getId());

                                    rpct::fct::FebPart & _febpart = _febconnector.getFebPart();
                                    { // connector low
                                        std::stringstream _feb_configuration_name;
                                        _feb_configuration_name << "ConnectivityTest." << _febconnector.getId() << ".Low";
                                        rpct::hwd::Configuration _feb_configuration
                                            = fct_febsystem_->registerConfiguration(_feb_configuration_name.str());
                                        _feb_configuration.registerDeviceConfiguration(_febpart.getFebChip(0)
                                                                                       , _febchip_deviceconfiguration_low);
                                        _feb_configuration.registerDeviceConfiguration(_febpart.getFebChip(1)
                                                                                       , _febchip_deviceconfiguration_low);
                                        _febpart.getFebBoard().configure(_feb_configuration, _deviceflags);

                                        _febconnector.setActive(true);

                                        _febpart.getFebBoard().monitor(_feb_observables, _deviceflags);
                                    }

                                    // read out noise
                                    _linkbox.configure(_linkboard_configuration, _deviceflags);
                                    _linkbox.monitor(_linkboard_observables, _deviceflags);

                                    // take snapshot
                                    fct_febsystem_->takeSnapshot(_snapshot_type);

                                    { // connector high
                                        std::stringstream _feb_configuration_name;
                                        _feb_configuration_name << "ConnectivityTest." << _febconnector.getId() << ".High";
                                        rpct::hwd::Configuration _feb_configuration
                                            = fct_febsystem_->registerConfiguration(_feb_configuration_name.str());
                                        _feb_configuration.registerDeviceConfiguration(_febpart.getFebChip(0)
                                                                                       , _febchip_deviceconfiguration_high);
                                        _feb_configuration.registerDeviceConfiguration(_febpart.getFebChip(1)
                                                                                       , _febchip_deviceconfiguration_high);
                                        _febpart.getFebBoard().configure(_feb_configuration, _deviceflags);

                                        _febconnector.setActive(false);

                                        _febpart.getFebBoard().monitor(_feb_observables, _deviceflags);
                                    }

                                    getProgress().addValue();

                                    getProgress().checkPaused();
                                    _is_terminated = getProgress().checkTerminated();
                                }
                        (*_linkboard_it)->setActive(false);
                    }
            _linkbox.setActive(false);
        }

    if (use_pulser_)
        {
            getProgress().setDescription("Stopping pulsers");
            rpct::tools::TLock<toolbox::BSem> _hwlock(getHardwareMutex());
            std::list<rpct::HalfBoxAccessHandler> _halfbox_handlers;
            // prepare access
            for (std::map<int, rpct::fct::LinkBox *>::iterator _linkbox_it = _linkboxes.begin()
                     ; _linkbox_it != _linkboxes.end() ; ++_linkbox_it)
                {
                    rpct::fct::LinkBox & _linkbox = *(_linkbox_it->second);
                    rpct::HalfBox * _halfbox = _linkbox.getRpctLinkBox().getHalfBox10();
                    if (_halfbox)
                        _halfbox_handlers.push_back(rpct::HalfBoxAccessHandler(*_halfbox));
                    _halfbox = _linkbox.getRpctLinkBox().getHalfBox20();
                    if (_halfbox)
                        _halfbox_handlers.push_back(rpct::HalfBoxAccessHandler(*_halfbox));
                }
            _transaction.begin();
            fct_febsystem_->configure(_linkboard_pulser_stop_configuration, _deviceflags);
            _transaction.commit();
        }

    LOG4CPLUS_INFO(logger_, "ConnectivityTest done");
}

void FebConnectivityTest::thresholdScan()
{
    LOG4CPLUS_INFO(logger_, "Running ThresholdScan");
    rpct::hwd::Transaction _transaction(*hwstorage_, false);

    _transaction.begin();
    rpct::fct::FebThresholdScan & _febthresholdscan
        = rpct::fct::FebThresholdScan::registerDevice(*fct_febsystem_
                                                      , selection_
                                                      , count_duration_
                                                      , vth_min_, vth_n_steps_, vth_step_size_
                                                      , vmon_
                                                      , auto_correct_
                                                      , use_pulser_
                                                      , include_disabled_
                                                      , "ThresholdScan");
    _transaction.commit();

    rpct::hwd::SnapshotType const & _snapshot_type = fct_febsystem_->registerSnapshotType("ThresholdScan");

    rpct::hwd::DeviceFlagsMask _deviceflags;
    if (!include_disabled_)
        _deviceflags.requireDisabled(false);

    rpct::fct::FebChipType const & _febchip_type
        = dynamic_cast<rpct::fct::FebChipType const &>(fct_febsystem_->getDeviceType("FebChip"));
    rpct::fct::FebPartType const & _febpart_type
        = dynamic_cast<rpct::fct::FebPartType const &>(fct_febsystem_->getDeviceType("FebPart"));
    rpct::fct::LinkBoardType const & _linkboard_type
        = dynamic_cast<rpct::fct::LinkBoardType const &>(fct_febsystem_->getDeviceType("LinkBoard"));
    rpct::hwd::DeviceType const & _febconnector_type
        = fct_febsystem_->getDeviceType("FebConnector");

    std::vector<rpct::hwd::Device *> const & _febconnectors
        = fct_febsystem_->getDevices(_febconnector_type);

    _transaction.begin();

    rpct::hwd::Configuration _linkboard_configuration
        = fct_febsystem_->registerConfiguration("ThresholdScan.LinkBoard");
    rpct::hwd::DeviceConfiguration & _linkboard_deviceconfiguration
        = _linkboard_configuration.registerDeviceTypeConfiguration(_linkboard_type);
    ::int64_t _duration = count_duration_.seconds() * rpct::fct::LinkBoard::bx_per_second_
          + count_duration_.nanoseconds() / rpct::fct::LinkBoard::nanoseconds_per_bx_;

    rpct::hwd::Configuration _linkboard_pulser_start_configuration
        = fct_febsystem_->registerConfiguration("ThresholdScan.LinkBoard.Pulser.Start");
    rpct::hwd::DeviceConfiguration & _linkboard_pulser_start_deviceconfiguration
        = _linkboard_pulser_start_configuration.registerDeviceTypeConfiguration(_linkboard_type);
    rpct::hwd::Configuration _linkboard_pulser_stop_configuration
        = fct_febsystem_->registerConfiguration("ThresholdScan.LinkBoard.Pulser.Stop");
    rpct::hwd::DeviceConfiguration & _linkboard_pulser_stop_deviceconfiguration
        = _linkboard_pulser_stop_configuration.registerDeviceTypeConfiguration(_linkboard_type);

    if (use_pulser_)
        {
            std::vector< ::uint32_t> _pulser_data;
            _pulser_data.reserve(250);
            ::uint32_t _pulser_data_block(0xffffff);
            for (std::size_t _channel = 0 ; _channel <= 24 ; ++_channel)
                {
                    _pulser_data_block = 0xffffff & ~(0x1 << _channel);
                    for (std::size_t _bx = 0 ; _bx < 4 ; ++_bx)
                        _pulser_data.push_back(_pulser_data_block);
                    for (std::size_t _bx = 4 ; _bx < 10 ; ++_bx)
                        _pulser_data.push_back(0xffffff);
                }
            _linkboard_pulser_start_deviceconfiguration.registerParameterSetting(_linkboard_type.getPulserDataParameter()
                                                                                 , _pulser_data);
            _linkboard_pulser_start_deviceconfiguration.registerParameterSetting(_linkboard_type.getRunPulserParameter()
                                                                                 , true);
            _linkboard_pulser_stop_deviceconfiguration.registerParameterSetting(_linkboard_type.getRunPulserParameter()
                                                                                , false);
            _duration = (_duration / _pulser_data.size()) * _pulser_data.size();
        }
    _linkboard_deviceconfiguration.registerParameterSetting(_linkboard_type.getDurationParameter()
                                                            , _duration);

    rpct::hwd::Observables _linkboard_observables;
    rpct::hwd::DeviceObservables & _linkboard_deviceobservables
        = _linkboard_observables.addDeviceObservables(_linkboard_type);
    _linkboard_deviceobservables.addParameter(_linkboard_type.getCountsParameter());

    rpct::hwd::Observables _feb_observables;
    rpct::hwd::DeviceObservables & _febchip_deviceobservables
        = _feb_observables.addDeviceObservables(_febchip_type);
    _febchip_deviceobservables.addParameter(_febchip_type.getVThWriteParameter());
    _febchip_deviceobservables.addParameter(_febchip_type.getVMonWriteParameter());
    _febchip_deviceobservables.addParameter(_febchip_type.getVThReadParameter());
    _febchip_deviceobservables.addParameter(_febchip_type.getVMonReadParameter());
    rpct::hwd::DeviceObservables & _febpart_deviceobservables
        = _feb_observables.addDeviceObservables(_febpart_type);
    _febpart_deviceobservables.addParameter(_febpart_type.getTemperatureParameter());

    std::map<int, rpct::fct::LinkBox *> _linkboxes;
    { // list LinkBoxes
        std::vector<rpct::hwd::Device *>::const_iterator _febconnector_it_end = _febconnectors.end();
        for (std::vector<rpct::hwd::Device *>::const_iterator _febconnector_it = _febconnectors.begin()
                 ; _febconnector_it != _febconnector_it_end ; ++_febconnector_it)
            {
                rpct::fct::FebConnector & _febconnector
                    = dynamic_cast<rpct::fct::FebConnector &>(*(*_febconnector_it));
                if (selection_.matches(_febconnector.getRoll()))
                    {
                        rpct::fct::LinkBoard & _linkboard = _febconnector.getLinkBoard();
                        rpct::fct::LinkBox & _linkbox = _linkboard.getLinkBox();
                        rpct::fct::FebPart const & _febpart = _febconnector.getFebPart();

                        _febconnector.setSelected(true);
                        _linkboard.setSelected(true);

                        _febconnector.setActive(true);
                        _linkboard.setActive(true);
                        _linkbox.setActive(true);

                        if (_febconnector.getFebPart().getFebBoard().getDeviceFlags().isDisabled())
                            {
                                LOG4CPLUS_WARN(logger_, "FebBoard is disabled for selected connector "
                                               << _febconnector.getRoll() << "_c" << _febconnector.getRollConnector()
                                               << (include_disabled_ ? ", including anyway" : ""));
                            }

                        _feb_observables.addDeviceObservables(_febpart, _febpart_deviceobservables);
                        _feb_observables.addDeviceObservables(_febpart.getFebChip(0)
                                                              , _febchip_deviceobservables);
                        _feb_observables.addDeviceObservables(_febpart.getFebChip(1)
                                                              , _febchip_deviceobservables);

                        _linkboard_configuration.registerDeviceConfiguration(_linkboard
                                                                             , _linkboard_deviceconfiguration);
                        _linkboard_observables.addDeviceObservables(_linkboard
                                                                    , _linkboard_deviceobservables);

                        if (use_pulser_)
                            {
                                _linkboard_pulser_start_configuration.registerDeviceConfiguration(_linkboard
                                                                                                  , _linkboard_pulser_start_deviceconfiguration);
                                _linkboard_pulser_stop_configuration.registerDeviceConfiguration(_linkboard
                                                                                                 , _linkboard_pulser_stop_deviceconfiguration);
                            }

                        if (!_linkbox.isSelected()) {
                            _linkbox.setSelected(true);
                            _linkboxes.insert(std::pair<int, rpct::fct::LinkBox *>(_linkbox.getId(), &_linkbox));
                        }
                    }
            }
    }
    _transaction.commit();

    if (use_pulser_)
        {
            getProgress().setDescription("Configuring Pulsers");
            rpct::tools::TLock<toolbox::BSem> _hwlock(getHardwareMutex());
            std::list<rpct::HalfBoxAccessHandler> _halfbox_handlers;
            // prepare access
            for (std::map<int, rpct::fct::LinkBox *>::iterator _linkbox_it = _linkboxes.begin()
                     ; _linkbox_it != _linkboxes.end() ; ++_linkbox_it)
                {
                    rpct::fct::LinkBox & _linkbox = *(_linkbox_it->second);
                    rpct::HalfBox * _halfbox = _linkbox.getRpctLinkBox().getHalfBox10();
                    if (_halfbox)
                        _halfbox_handlers.push_back(rpct::HalfBoxAccessHandler(*_halfbox));
                    _halfbox = _linkbox.getRpctLinkBox().getHalfBox20();
                    if (_halfbox)
                        _halfbox_handlers.push_back(rpct::HalfBoxAccessHandler(*_halfbox));
                }
            _transaction.begin();
            fct_febsystem_->configure(_linkboard_pulser_start_configuration, _deviceflags);
            _transaction.commit();
        }

    getProgress().init();
    getProgress().setMaximum(vth_n_steps_ + 1);
    getProgress().start();
    bool _is_terminated(false);

    getProgress().checkPaused();
    _is_terminated = getProgress().checkTerminated();

    rpct::hwd::Configuration * _high_configuration(0);
    for (int _vth_step = vth_n_steps_ - 1 ; _vth_step >= 0 && ! _is_terminated ; --_vth_step)
        {
            int _vth = vth_min_ + _vth_step * vth_step_size_;

            _febthresholdscan.setActiveVTh(_vth);

            std::stringstream _desc;
            _desc << "Processing threshold step " << (vth_n_steps_ - _vth_step) << " of " << vth_n_steps_;
            getProgress().setDescription(_desc.str());

            _transaction.begin();

            std::stringstream _ss;
            _ss << "ThresholdScan.FebBoard." << (vth_n_steps_ - _vth_step);
            rpct::hwd::Configuration _feb_configuration
                = fct_febsystem_->registerConfiguration(_ss.str());

            rpct::hwd::DeviceConfiguration & _febchip_deviceconfiguration
                = _feb_configuration.registerDeviceTypeConfiguration(_febchip_type);
            _febchip_deviceconfiguration.registerParameterSetting(_febchip_type.getVThSetParameter(), _vth);
            _febchip_deviceconfiguration.registerParameterSetting(_febchip_type.getVMonSetParameter(), vmon_);
            _febchip_deviceconfiguration.registerParameterSetting(_febchip_type.getAutoCorrectParameter(), auto_correct_);

            std::vector<rpct::hwd::Device *>::const_iterator _febconnector_it_end = _febconnectors.end();
            for (std::vector<rpct::hwd::Device *>::const_iterator _febconnector_it = _febconnectors.begin()
                     ; _febconnector_it != _febconnector_it_end && !_is_terminated ; ++_febconnector_it)
                {
                    rpct::fct::FebConnector & _febconnector
                        = dynamic_cast<rpct::fct::FebConnector &>(*(*_febconnector_it));
                    if (_febconnector.isSelected())
                        {
                            rpct::fct::FebPart const & _febpart = _febconnector.getFebPart();
                            _feb_configuration.registerDeviceConfiguration(_febpart.getFebChip(0)
                                                                           , _febchip_deviceconfiguration);
                            _feb_configuration.registerDeviceConfiguration(_febpart.getFebChip(1)
                                                                           , _febchip_deviceconfiguration);
                        }
                }
            _transaction.commit();
            if (_vth_step == vth_n_steps_ - 1)
                _high_configuration = &_feb_configuration;
            {
                rpct::tools::TLock<toolbox::BSem> _hwlock(getHardwareMutex());
                std::list<rpct::HalfBoxAccessHandler> _halfbox_handlers;
                // prepare access
                for (std::map<int, rpct::fct::LinkBox *>::iterator _linkbox_it = _linkboxes.begin()
                         ; _linkbox_it != _linkboxes.end() ; ++_linkbox_it)
                    {
                        rpct::fct::LinkBox & _linkbox = *(_linkbox_it->second);
                        rpct::HalfBox * _halfbox = _linkbox.getRpctLinkBox().getHalfBox10();
                        if (_halfbox)
                            _halfbox_handlers.push_back(rpct::HalfBoxAccessHandler(*_halfbox));
                        _halfbox = _linkbox.getRpctLinkBox().getHalfBox20();
                        if (_halfbox)
                            _halfbox_handlers.push_back(rpct::HalfBoxAccessHandler(*_halfbox));
                    }

                _transaction.begin();
                fct_febsystem_->configure(_feb_configuration, _deviceflags);
                _transaction.commit();
                _transaction.begin();
                fct_febsystem_->monitor(_feb_observables, _deviceflags);
                _transaction.commit();
                if (_duration > 0)
                    {
                        _transaction.begin();
                        fct_febsystem_->configure(_linkboard_configuration, _deviceflags);
                        _transaction.commit();
                        _transaction.begin();
                        fct_febsystem_->monitor(_linkboard_observables, _deviceflags);
                        _transaction.commit();
                    }
                _transaction.begin();
                fct_febsystem_->takeSnapshot(_snapshot_type);
                _transaction.commit();
            }
            getProgress().addValue();

            getProgress().checkPaused();
            _is_terminated = getProgress().checkTerminated();
        }

    if (_high_configuration && !_is_terminated)
        {
            getProgress().setDescription("Setting Thresholds high again");
            rpct::tools::TLock<toolbox::BSem> _hwlock(getHardwareMutex());
            std::list<rpct::HalfBoxAccessHandler> _halfbox_handlers;
            // prepare access
            for (std::map<int, rpct::fct::LinkBox *>::iterator _linkbox_it = _linkboxes.begin()
                     ; _linkbox_it != _linkboxes.end() ; ++_linkbox_it)
                {
                    rpct::fct::LinkBox & _linkbox = *(_linkbox_it->second);
                    rpct::HalfBox * _halfbox = _linkbox.getRpctLinkBox().getHalfBox10();
                    if (_halfbox)
                        _halfbox_handlers.push_back(rpct::HalfBoxAccessHandler(*_halfbox));
                    _halfbox = _linkbox.getRpctLinkBox().getHalfBox20();
                    if (_halfbox)
                        _halfbox_handlers.push_back(rpct::HalfBoxAccessHandler(*_halfbox));
                }

            _transaction.begin();
            fct_febsystem_->configure(*_high_configuration, _deviceflags);
            _transaction.commit();
        }

    getProgress().addValue();

    if (use_pulser_)
        {
            getProgress().setDescription("Stopping pulsers");
            rpct::tools::TLock<toolbox::BSem> _hwlock(getHardwareMutex());
            std::list<rpct::HalfBoxAccessHandler> _halfbox_handlers;
            // prepare access
            for (std::map<int, rpct::fct::LinkBox *>::iterator _linkbox_it = _linkboxes.begin()
                     ; _linkbox_it != _linkboxes.end() ; ++_linkbox_it)
                {
                    rpct::fct::LinkBox & _linkbox = *(_linkbox_it->second);
                    rpct::HalfBox * _halfbox = _linkbox.getRpctLinkBox().getHalfBox10();
                    if (_halfbox)
                        _halfbox_handlers.push_back(rpct::HalfBoxAccessHandler(*_halfbox));
                    _halfbox = _linkbox.getRpctLinkBox().getHalfBox20();
                    if (_halfbox)
                        _halfbox_handlers.push_back(rpct::HalfBoxAccessHandler(*_halfbox));
                }
            _transaction.begin();
            fct_febsystem_->configure(_linkboard_pulser_stop_configuration, _deviceflags);
            _transaction.commit();
        }

    LOG4CPLUS_INFO(logger_, "ThresholdScan done");
}

void FebConnectivityTest::runFebConfiguration()
{
    LOG4CPLUS_INFO(logger_, "FebConnectivityTest::runFebConfiguration");

    if (!(getProgress().isProcessActive()))
        {
            getProgress().setTitle("FebConfiguration");
            getProgress().init();
            getProgress().setDescription("Submitting a FebConfiguration");
            getProgress().setMaximum(1);
            getProgress().start();
            {
                rpct::tools::WLock _lock(mutex_);
                if (cellname_.empty())
                    {
                        cellname_ = cell_.getName();
                        std::replace(cellname_.begin(), cellname_.end(), ' ', '_');
                        std::replace(cellname_.begin(), cellname_.end(), '+', 'P');
                        std::replace(cellname_.begin(), cellname_.end(), '-', 'N');
                    }
                if (store_output_)
                    {
                        std::stringstream _filename_ss;
                        std::string _folder_base("/rpctdata");
                        char * _folder_base_env = std::getenv("RPCT_DATA_PATH");
                        if (_folder_base_env)
                            _folder_base = _folder_base_env;
                        _filename_ss << _folder_base << "/FebConnectivityTest/"
                                     << cellname_ << "_"
                                     << "config" << "_"
                                     << rpct::tools::Date().filename()  << ".db";
                        filename_ = _filename_ss.str();
                    }
                else
                    filename_ = ":memory:";
                run_wl_->submit(febconfiguration_action_);
            }
        }
    else
        {
            LOG4CPLUS_ERROR(logger_, "A Commissioning task is already running; runFebConfiguration job cancelled.");
        }
}

void FebConnectivityTest::runConnectivityTest()
{
    LOG4CPLUS_INFO(logger_, "FebConnectivityTest::runConnectivityTest");

    if (!(getProgress().isProcessActive()))
        {
            getProgress().setTitle("ConnectivityTest");
            getProgress().init();
            getProgress().setDescription("Submitting a ConnectivityTest");
            getProgress().setMaximum(1);
            getProgress().start();
            {
                rpct::tools::WLock _lock(mutex_);
                if (cellname_.empty())
                    {
                        cellname_ = cell_.getName();
                        std::replace(cellname_.begin(), cellname_.end(), ' ', '_');
                        std::replace(cellname_.begin(), cellname_.end(), '+', 'P');
                        std::replace(cellname_.begin(), cellname_.end(), '-', 'N');
                    }
                std::stringstream _filename_ss;
                std::string _folder_base("/rpctdata");
                char * _folder_base_env = std::getenv("RPCT_DATA_PATH");
                if (_folder_base_env)
                    _folder_base = _folder_base_env;
                _filename_ss << _folder_base << "/FebConnectivityTest/"
                             << cellname_ << "_"
                             << "conntest" << "_"
                             << rpct::tools::Date().filename()  << ".db";
                filename_ = _filename_ss.str();
                run_wl_->submit(connectivitytest_action_);
            }
        }
    else
        {
            LOG4CPLUS_ERROR(logger_, "A Commissioning task is already running; runConnectivityTest job cancelled.");
        }
}

void FebConnectivityTest::runThresholdScan()
{
    LOG4CPLUS_INFO(logger_, "FebConnectivityTest::runThresholdScan");

    if (!(getProgress().isProcessActive()))
        {
            getProgress().setTitle("ThresholdScan");
            getProgress().init();
            getProgress().setDescription("Submitting a ThresholdScan");
            getProgress().setMaximum(1);
            getProgress().start();
            {
                rpct::tools::WLock _lock(mutex_);
                if (cellname_.empty())
                    {
                        cellname_ = cell_.getName();
                        std::replace(cellname_.begin(), cellname_.end(), ' ', '_');
                        std::replace(cellname_.begin(), cellname_.end(), '+', 'P');
                        std::replace(cellname_.begin(), cellname_.end(), '-', 'N');
                    }
                std::stringstream _filename_ss;
                std::string _folder_base("/rpctdata");
                char * _folder_base_env = std::getenv("RPCT_DATA_PATH");
                if (_folder_base_env)
                    _folder_base = _folder_base_env;
                _filename_ss << _folder_base << "/FebConnectivityTest/"
                             << cellname_ << "_"
                             << "thscan" << "_"
                             << rpct::tools::Date().filename()  << ".db";
                filename_ = _filename_ss.str();

                run_wl_->submit(thresholdscan_action_);
            }
        }
    else
        {
            LOG4CPLUS_ERROR(logger_, "A Commissioning task is already running; runThresholdScan job cancelled.");
        }
}

bool FebConnectivityTest::febConfiguration_wl(toolbox::task::WorkLoop *)
{
    try {
        LOG4CPLUS_INFO(xdaqlboxaccess_.getSummaryLogger(), "Starting FebConfiguration");
        getProgress().setDescription("Preparing the system for a FebConfiguration");
        prepareFctFebSystem();
        febConfiguration();
        getProgress().setDescription("Finishing FebConfiguration");
    } catch (std::exception & _e) {
        getProgress().setDescription(std::string("Error: ") + _e.what());
        getProgress().fail();
        LOG4CPLUS_ERROR(logger_, "Caught unknown exception in FebConnectivityTest::febConfiguration_wl: "
                        << _e.what());
        LOG4CPLUS_ERROR(xdaqlboxaccess_.getSummaryLogger(), "Error in FebConfiguration");
    } catch (...) {
        getProgress().setDescription("Unknown Error");
        getProgress().fail();
        LOG4CPLUS_ERROR(logger_, "Caught unknown exception in FebConnectivityTest::febConfiguration_wl");
        LOG4CPLUS_ERROR(xdaqlboxaccess_.getSummaryLogger(), "Error in FebConfiguration");
    }

    if (fct_febsystem_)
        delete fct_febsystem_;
    if (hwstorage_)
        delete hwstorage_;
    fct_febsystem_ = 0;
    hwstorage_ = 0;

    getProgress().stop();
    getProgress().setDescription("Finished FebConfiguration");

    LOG4CPLUS_INFO(xdaqlboxaccess_.getSummaryLogger(), "Finished FebConfiguration");
    return false;
}

bool FebConnectivityTest::connectivityTest_wl(toolbox::task::WorkLoop *)
{
    try {
        LOG4CPLUS_INFO(xdaqlboxaccess_.getSummaryLogger(), "Starting FebConnectivityTest");
        getProgress().setDescription("Preparing the system for a ConnectivityTest");
        prepareFctFebSystem();
        connectivityTest();
        getProgress().setDescription("Finishing ConnectivityTest");
    } catch (std::exception & _e) {
        getProgress().setDescription(std::string("Error: ") + _e.what());
        getProgress().fail();
        LOG4CPLUS_ERROR(logger_, "Caught unknown exception in FebConnectivityTest::connectivityTest_wl: "
                        << _e.what());
        LOG4CPLUS_ERROR(xdaqlboxaccess_.getSummaryLogger(), "Error in FebConnectivityTest");
    } catch (...) {
        getProgress().setDescription("Unknown Error");
        getProgress().fail();
        LOG4CPLUS_ERROR(logger_, "Caught unknown exception in FebConnectivityTest::connectivityTest_wl");
        LOG4CPLUS_ERROR(xdaqlboxaccess_.getSummaryLogger(), "Error in FebConnectivityTest");
    }

    if (fct_febsystem_)
        delete fct_febsystem_;
    if (hwstorage_)
        delete hwstorage_;
    fct_febsystem_ = 0;
    hwstorage_ = 0;

    getProgress().stop();
    getProgress().setDescription("Finished ConnectivityTest");

    LOG4CPLUS_INFO(xdaqlboxaccess_.getSummaryLogger(), "Finished FebConnectivityTest");
    return false;
}

bool FebConnectivityTest::thresholdScan_wl(toolbox::task::WorkLoop *)
{
    try {
        LOG4CPLUS_INFO(xdaqlboxaccess_.getSummaryLogger(), "Starting FebThresholdScan");
        prepareFctFebSystem();
        thresholdScan();
        getProgress().setDescription("Finishing ThresholdScan");
    } catch (std::exception & _e) {
        getProgress().setDescription(std::string("Error: ") + _e.what());
        getProgress().fail();
        LOG4CPLUS_ERROR(logger_, "Caught unknown exception in FebConnectivityTest::connectivityTest_wl: "
                        << _e.what());
        LOG4CPLUS_ERROR(xdaqlboxaccess_.getSummaryLogger(), "Error in FebThresholdScan");
    } catch (...) {
        getProgress().setDescription("Unknown Error");
        getProgress().fail();
        LOG4CPLUS_ERROR(logger_, "Caught unknown exception in FebConnectivityTest::thresholdScan_wl");
        LOG4CPLUS_ERROR(xdaqlboxaccess_.getSummaryLogger(), "Error in FebThresholdScan");
    }

    if (fct_febsystem_)
        delete fct_febsystem_;
    if (hwstorage_)
        delete hwstorage_;
    fct_febsystem_ = 0;
    hwstorage_ = 0;

    getProgress().stop();
    getProgress().setDescription("Finished FebThresholdScan");

    LOG4CPLUS_INFO(xdaqlboxaccess_.getSummaryLogger(), "Finished FebThresholdScan");
    return false;
}

} // namespace rpcttsworker
