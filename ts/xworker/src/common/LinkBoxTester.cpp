/*
 * LinkBoxTester.cpp
 *
 *  Created on: May 31, 2013
 *      Author: Karol Bunkowski
 */

#include "rpct/ts/worker/xdaqlboxaccess/LinkBoxTester.h"

#include "rpct/xdaqutils/XdaqCommonUtils.h"

#include "rpct/hardwareTests/FEBsTimingTester.h"


#include "rpct/devices/LinkBoard.h"
#include "rpct/devices/LinkBox.h"
#include "rpct/devices/LinkSystem.h"

#include <log4cplus/configurator.h>
#include <log4cplus/fileappender.h>

using namespace std;
using namespace rpct;
using namespace log4cplus;

Logger LinkBoxTester::logger = Logger::getInstance("LinkBoxTester");
void LinkBoxTester::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}

LinkBoxTester::LinkBoxTester(XdaqLBoxAccess* lboxAccess): lboxAccess(lboxAccess), linkSystem_(lboxAccess->getLinkSystem()) {
	ostringstream ostr;
	char* tmp = getenv("RPCT_DATA_PATH");
	if (tmp != 0) {
		ostr << getenv("RPCT_DATA_PATH")<<"/"<<"linkBoxTests";
	}
	else {
		XCEPT_RAISE(xoap::exception::Exception, (string)(__FUNCTION__) + string(": Environment variable RPCT_DATA_PATH not defined"));
	}
	linkBoxTestsDir = ostr.str();
	string fileDirName =  linkBoxTestsDir + "/boardsLabels.txt";
	string backplaneLabel = lboxAccess->getLinkSystem()->readBoardsLabels(fileDirName);

	ostr<<"/lbox_"<<backplaneLabel<<"_"<< timeString();
	thisLinkBoxTestsDir = ostr.str();

	::system( (string("mkdir ") + thisLinkBoxTestsDir).c_str());
	::system( (string("cp ") + fileDirName + " " + thisLinkBoxTestsDir).c_str());

	string linkBoxTesterLogDir = thisLinkBoxTestsDir +"/linkBoxTester.log";

	FileAppender* appender = new FileAppender(linkBoxTesterLogDir);
	appender->setLayout(auto_ptr<Layout>(new PatternLayout("%d{%d %b %Y %H:%M:%S} %p %c{1} <> - %m%n")));

	appenderPtr = appender;
	lboxAccess->getSummaryLogger().addAppender(appenderPtr);
	FEBsTimingTester::getLogger().addAppender(appenderPtr);
	logger.addAppender(appenderPtr);

	febsTimingTester =  new FEBsTimingTester(thisLinkBoxTestsDir);
}

LinkBoxTester::~LinkBoxTester() {
	LOG4CPLUS_INFO(lboxAccess->getSummaryLogger(), "finishing the test. The output directory is ");
	LOG4CPLUS_INFO(lboxAccess->getSummaryLogger(), "file:///"<<thisLinkBoxTestsDir);

			///<a href="http://www.html.net/">Oto odnonik do HTML.net</a>

	FEBsTimingTester::getLogger().removeAppender(appenderPtr);
	lboxAccess->getSummaryLogger().removeAppender(appenderPtr);
	logger.removeAppender(appenderPtr);
	delete febsTimingTester;
}

void LinkBoxTester::testRegisters(rpct::LinkSystem::LinkBoards linkBoards) {
	LOG4CPLUS_INFO(lboxAccess->getSummaryLogger(), "test of registers started");
	//TID id
	vector<IDevice::TID> registers;
	//registers.push_back(SynCoder::WORD_CHAN_ENA);
	//registers.push_back(SynCoder::WORD_SEND_TEST_DATA); used to check the SEU in the SynCoder::monitor()
	registers.push_back(SynCoder::WORD_STAT_TIMER_LIMIT);
	//registers.push_back(SynCoder::WORD_STAT_TRIG_COUNT);
	registers.push_back(SynCoder::WORD_DAQ_TIMER_LIMIT);
	registers.push_back(SynCoder::WORD_PULSER_TIMER_LIMIT);

	vector<uint32_t> values;
	values.push_back(0);
	values.push_back(0xffffffff);
	values.push_back(0xaaaaaaaa);
	values.push_back(0x55555555);
	for(unsigned int i = 0; i < 32; i++) {
		uint32_t one = 1;
		values.push_back(one<<i);
	}


	for (LinkSystem::LinkBoards::iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); ++iLB) {
		LinkBoard* lb = (*iLB);
		for(unsigned int iV = 0; iV < values.size(); iV++) {
			uint32_t writtenValue = values[iV];
			for(unsigned int i = 0; i < registers.size(); i++) {
				lb->getSynCoder().writeWord(registers[i], 0, writtenValue);
				uint32_t readValue = lb->getSynCoder().readWord(registers[i], 0);
				if(writtenValue != readValue) {
					LOG4CPLUS_WARN(lboxAccess->getSummaryLogger(), lb->getDescription()<<" - SynCoder register test: register "
							<<lb->getSynCoder().getItemDesc(registers[i]).Name
							<<" writtenValue "<<hex<<writtenValue<<" readValue "<<hex<<readValue
							);
				}
			}
		}
	}
	LOG4CPLUS_INFO(lboxAccess->getSummaryLogger(), "test of registers finished");
}

void LinkBoxTester::testInputsWithPulses(rpct::LinkSystem::LinkBoards linkBoards) {
	LOG4CPLUS_INFO(lboxAccess->getSummaryLogger(), "test of inputs with pulses started");
	LOG4CPLUS_INFO(lboxAccess->getSummaryLogger(), "testing even channels (0x555555)");
	string result = febsTimingTester->timingTest(0.01, FEBsTimingTester::testWinO, 0x555555, 0, 239, 128, &analyserForTestBoard, linkBoards);
	if(result.find("BAD") !=  string::npos)  {
		LOG4CPLUS_WARN(lboxAccess->getSummaryLogger(), result);
	}
	else {
		LOG4CPLUS_INFO(lboxAccess->getSummaryLogger(), result);
	}

	LOG4CPLUS_INFO(lboxAccess->getSummaryLogger(), "testing odd channels (0xaaaaaa)");
	result = febsTimingTester->timingTest(0.01, FEBsTimingTester::testWinO, 0xaaaaaa, 0, 239, 128, &analyserForTestBoard, linkBoards);
	if(result.find("BAD") !=  string::npos)  {
		LOG4CPLUS_WARN(lboxAccess->getSummaryLogger(), result);
	}
	else {
		LOG4CPLUS_INFO(lboxAccess->getSummaryLogger(), result);
	}

	LOG4CPLUS_INFO(lboxAccess->getSummaryLogger(), "test of inputs with pulses finished");
}

void LinkBoxTester::testOptLinks() {
	LOG4CPLUS_INFO(lboxAccess->getSummaryLogger(), "checkOptLinks started");

	string checkOptLinksCommand = linkBoxTestsDir + "/CheckOptLinks.sh TC_904";
	string checkOptLinkslogFileName = linkBoxTestsDir + "/checkOptLinks.log";

	int checkOptLinksReturned = ::system(checkOptLinksCommand.c_str());

	ifstream checkOptLinkslog;
	checkOptLinkslog.open(checkOptLinkslogFileName.c_str());
	if(!checkOptLinkslog) {
		LOG4CPLUS_WARN(lboxAccess->getSummaryLogger(), "checkOptLinks.log file could not be opened: "<<checkOptLinkslogFileName);
	}
	else {
		string line;
		while ( checkOptLinkslog.good() ) {
			std::getline(checkOptLinkslog, line);
			if(line.find("ERROR") != string::npos)  {
				LOG4CPLUS_ERROR(lboxAccess->getSummaryLogger(), line);
			}
			else
				LOG4CPLUS_INFO(lboxAccess->getSummaryLogger(), line);
		}
		checkOptLinkslog.close();
	}

	if(checkOptLinksReturned == -1 || WEXITSTATUS(checkOptLinksReturned) ==  255) {
		LOG4CPLUS_WARN(lboxAccess->getSummaryLogger(), "error during execution on the CheckOptLinks. Check if the TC worker is running and is configured.");
	}
	else if(WEXITSTATUS(checkOptLinksReturned) > 0) {
		//LOG4CPLUS_WARN(lboxAccess->getSummaryLogger(), "checkOptLinks ended: BAD links count "<<WEXITSTATUS(checkOptLinksReturned));
	}
	else if(WEXITSTATUS(checkOptLinksReturned) == 0) {
		//LOG4CPLUS_WARN(lboxAccess->getSummaryLogger(),"checkOptLinks ended: ALL links OK");
	}

}

void LinkBoxTester::checkClocks(LinkSystem::LinkBoards& linkBoards) {
	LOG4CPLUS_INFO(lboxAccess->getSummaryLogger(), "checkClocks started");
	for (LinkSystem::LinkBoards::iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); ++iLB) {
		LinkBoard* lb = (*iLB);
		uint32_t mainClk = 0;
		uint32_t winoClk = 0;
		uint32_t wincClk = 0;

		bool mainClkOK = false;
		bool winoClkOK = false;
		bool wincClkOK = false;

		uint32_t clockBits = lb->getSynCoder().readVector(SynCoder::VECT_CLOCK);
		mainClk = lb->getSynCoder().getBitsInVector(SynCoder::BITS_CLOCK_MAIN_TEST, clockBits);
		winoClk = lb->getSynCoder().getBitsInVector(SynCoder::BITS_CLOCK_WIN_OPEN_TEST, clockBits);
		wincClk = lb->getSynCoder().getBitsInVector(SynCoder::BITS_CLOCK_WIN_CLOSE_TEST, clockBits);

		for (int i = 0; i < 10; i++) {
			clockBits = lb->getSynCoder().readVector(SynCoder::VECT_CLOCK);
			if(mainClk != lb->getSynCoder().getBitsInVector(SynCoder::BITS_CLOCK_MAIN_TEST, clockBits))
				mainClkOK = true;
			if(winoClk != lb->getSynCoder().getBitsInVector(SynCoder::BITS_CLOCK_WIN_OPEN_TEST, clockBits))
				winoClkOK = true;
			if(wincClk != lb->getSynCoder().getBitsInVector(SynCoder::BITS_CLOCK_WIN_CLOSE_TEST, clockBits))
				wincClkOK = true;
		}

		if(mainClkOK && winoClkOK && winoClkOK) {
			//LOG4CPLUS_INFO(lboxAccess->getSummaryLogger(), lb->getDescription()<<" checkClocks: all clocks OK");
		}
		else {
			LOG4CPLUS_ERROR(lboxAccess->getSummaryLogger(), " checkClocks: "<< lb->getDescription()<<" - MAIN "<<mainClkOK<<", WIN_OPEN "<<winoClkOK<<", WIN_CLOSE "<<wincClkOK);
		}
	}
}

LinkSystem::LinkBoards subRange(LinkSystem::LinkBoards& linkBoards, rpct::LinkSystem::LinkBoards::iterator& first, const unsigned int n) {
	if(first >= linkBoards.end()) {
		return rpct::LinkSystem::LinkBoards();
	}
	rpct::LinkSystem::LinkBoards::iterator last = first + n;

	if(last >= linkBoards.end()) {
		last = linkBoards.end();
	}

	rpct::LinkSystem::LinkBoards subRange(first, last);
	first = last;

	return subRange;
}


bool LinkBoxTester::startTest(bool programFlash, bool testWithPulses, rpct::LinkSystem::ControlBoards controlBoards, rpct::LinkSystem::LinkBoards linkBoards) {
	controlBoards_ = controlBoards;
	linkBoards_ =  linkBoards;

	try {
		LOG4CPLUS_INFO(lboxAccess->getSummaryLogger(),
				"test started for "<<controlBoards.size()<<" ControlBoards and "<<linkBoards.size()<<" LinkBoards");
		if(programFlash) {
			LOG4CPLUS_INFO(lboxAccess->getSummaryLogger(), "starting programFlash");
			lboxAccess->loadFromCcuCb(controlBoards);
			lboxAccess->programFlash(controlBoards, linkBoards);
		}

		lboxAccess->configureFromFlash("LHC6",true, true);
		lboxAccess->resetCBTTCrxs(controlBoards);

		MutexHandler handler(lboxAccess->getHardwareMutex());

		linkSystem_->resetLinkBoards(linkBoards);

		linkSystem_->synchronizeLinksLBs(linkBoards);

		checkClocks(linkBoards);

		linkSystem_->enable();



		if(testWithPulses) {
			linkBoardFirst = linkBoards_.begin();

			linkBoardsRange = subRange(linkBoards_, linkBoardFirst, 2);
			string rangeLBsNames;
			for(unsigned int i = 0; i < linkBoardsRange.size(); i++) {
				rangeLBsNames = rangeLBsNames + " " + linkBoardsRange[i]->getDescription();
			}

			LOG4CPLUS_INFO(lboxAccess->getSummaryLogger(), "connect the test boards to the LBs:"<<rangeLBsNames<<" and press continue");
		}
	}
	catch (std::exception& e) {
		LOG4CPLUS_ERROR(lboxAccess->getSummaryLogger(), e.what());
		LOG4CPLUS_ERROR(lboxAccess->getSummaryLogger(), "Test stopped due to error");
		return false;
	}
	return true;
}

bool LinkBoxTester::continueTest() {
	MutexHandler handler(lboxAccess->getHardwareMutex());

	if(linkBoardsRange.size() > 0) {
		testInputsWithPulses(linkBoardsRange);
		linkBoardsRange = subRange(linkBoards_, linkBoardFirst, 2);

		if(linkBoardsRange.size() > 0) {
			string rangeLBsNames;
			for(unsigned int i = 0; i < linkBoardsRange.size(); i++) {
				rangeLBsNames = rangeLBsNames + " " + linkBoardsRange[i]->getDescription();
			}
			LOG4CPLUS_INFO(lboxAccess->getSummaryLogger(), "connect the test boards to the LBs:"<<rangeLBsNames<<" and press continue");
			return false;
		}

	}


	return true;
}

bool LinkBoxTester::finalizeTest() {
	//testOptLinks();

	MutexHandler handler(lboxAccess->getHardwareMutex());
	monitorHardware(controlBoards_, linkBoards_);
	testRegisters(linkBoards_);
	//LOG4CPLUS_INFO(lboxAccess->getSummaryLogger(), "tests finished");
	return true;
}

void LinkBoxTester::monitorHardware(rpct::LinkSystem::ControlBoards controlBoards, rpct::LinkSystem::LinkBoards linkBoards) {
	volatile bool stop =  false;
	LOG4CPLUS_INFO(lboxAccess->getSummaryLogger(), __FUNCTION__<<": started, reading the monitorables");

	vector<Monitorable*> monitorables;
	for (LinkSystem::ControlBoards::iterator iCB = controlBoards.begin(); iCB != controlBoards.end(); iCB++) {
		Monitorable* monitorable = dynamic_cast<Monitorable*> ((*iCB));
		if (monitorable != 0) {
			monitorables.push_back(monitorable);
		}
	}

	for (LinkSystem::LinkBoards::iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); ++iLB) {
		LinkBoard* lb = (*iLB);
		Monitorable* monitorable = dynamic_cast<Monitorable*> (&(lb->getSynCoder()));
		if (monitorable != 0) {
			monitorables.push_back(monitorable);
		}
	}


	for (vector<Monitorable*>::iterator iMonitorable = monitorables.begin(); iMonitorable != monitorables.end(); ++iMonitorable) {
		try {
			(*iMonitorable)->monitor(&stop);
			for (Monitorable::MonitorStatusList::iterator iStat = (*iMonitorable)->getWarningStatusList().begin();
					iStat != (*iMonitorable)->getWarningStatusList().end(); ++iStat) {
				if (iStat->level != MonitorableStatus::OK) {
					LOG4CPLUS_ERROR(lboxAccess->getSummaryLogger(), (*iMonitorable)->getDescription() << "(" << iStat->monitorItem << "): " << iStat->message);
				}
			}
		}
		catch (std::exception& e) {
			xdata::String str(e.what()) ;
			LOG4CPLUS_ERROR(lboxAccess->getSummaryLogger(), "Exception during hardware monitoring: "<< e.what() <<" breaking the monitoring loop");
			break;//in prctice it can be only unrecovered CCU error, so here we may break
		}
	}
	LOG4CPLUS_INFO(lboxAccess->getSummaryLogger(), __FUNCTION__<<": done");;
}
