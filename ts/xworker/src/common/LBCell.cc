#include "rpct/ts/worker/LBCell.h"
#include "rpct/xdaqutils/LogUtils.h"

#include "ts/framework/CellOperationFactory.h"
#include "ts/framework/CellFactory.h"
#include "ts/framework/CellPanelFactory.h"


#include "rpct/ts/worker/LBWorkerConfiguration.h"
//#include "rpct/ts/worker/PushMON.h"

#include "rpct/ts/worker/panels/MessagesPanel.h"
#include "rpct/ts/worker/panels/LogPanel.h"

#include "rpct/ts/worker/panels/HistogramPanel.h"
#include "rpct/ts/worker/panels/LBoxControlPanel.h" 
#include "rpct/ts/worker/FebConfigurationPanel.h"
#include "rpct/ts/worker/panels/FebConnectivityTestPanel.h"

#include "xcept/tools.h"
 
#include "rpct/ts/worker/SendAlarm.h"

#include "rpct/ts/worker/FebConfigurationCommandConfigure.h"
#include "rpct/ts/worker/FebConfigurationCommandLoadConfiguration.h"
#include "rpct/ts/worker/FebConfigurationCommandMonitor.h"
#include "rpct/ts/worker/commands/ResetAllNotReadyCBTTCrxs.h"
#include "rpct/ts/worker/commands/ResetAllCBTTCrxs.h"
#include "rpct/ts/worker/commands/SynchronizeLinksCommand.h"

XDAQ_INSTANTIATOR_IMPL(rpcttsworker::LBCell)

rpcttsworker::LBCell::LBCell(xdaq::ApplicationStub * s) :
    tsframework::CellAbstract(s) {
    LOG4CPLUS_INFO(getApplicationLogger(), "LB worker cell constructor");

    try {
    	rpct::rpcLogSetup(getApplicationLogger(),this);
        cellContext_ = new rpcttsworker::LBCellContext(getApplicationLogger(), this);
    } catch (xcept::Exception& e) {
        std::ostringstream msg;
        msg << "Error instantiating the CellContext. Find the bug and restart the executive. Exception found: ";
        msg << xcept::stdformat_exception_history(e);
        LOG4CPLUS_ERROR(getApplicationLogger(),msg.str());
    }
    LOG4CPLUS_INFO(getApplicationLogger(), "Worker cell constructor exit");
}

rpcttsworker::LBCell::~LBCell() {
}

void rpcttsworker::LBCell::init() {

    getContext()->addImport("/rpct/ts/xworker/html/elements/elements.html");

    LOG4CPLUS_INFO(getApplicationLogger(), "Worker cell init");
 
    tsframework::CellOperationFactory* opF = getContext()->getOperationFactory();
    opF->add<rpcttsworker::LBWorkerConfiguration>("Run Control");

    tsframework::CellFactory* cmdF = getContext()->getCommandFactory();
    cmdF->add<rpcttsworker::SendAlarm>("Default","SendAlarm");
    cmdF->add<rpct::ts::worker::FebConfigurationCommandConfigure>("RPCT","ConfigureFEBs");
    cmdF->add<rpct::ts::worker::FebConfigurationCommandLoadConfiguration>("RPCT","LoadConfigurationFEBs");
    cmdF->add<rpct::ts::worker::FebConfigurationCommandMonitor>("RPCT","MonitorFEBs");
    cmdF->add<rpcttsworker::ResetAllNotReadyCBTTCrxs>("RPCT","ResetAllNotReadyCBTTCrxs");
    cmdF->add<rpcttsworker::ResetAllCBTTCrxs>("RPCT","ResetAllCBTTCrxs");
    cmdF->add<rpcttsworker::SynchronizeLinksCommand>("RPCT","SynchronizeLinksCommand");

    tsframework::CellPanelFactory* panelF = getContext()->getPanelFactory();
    panelF->add<rpcttspanels::MessagesPanel> ("Monitoring");
    panelF->add<rpcttspanels::LogPanel> ("Logging");
    panelF->add<rpcttspanels::LogPanel> ("Home");
    panelF->add<rpcttspanels::HistogramPanel> ("Histograms");
    panelF->add<rpcttspanels::LBoxControlPanel> ("Control"); 
    panelF->add<rpct::ts::worker::FebConfigurationPanel> ("FEB Control");
    panelF->add<rpcttsworker::FebConnectivityTestPanel> ("FEB Commissioning");

    LOG4CPLUS_INFO(getApplicationLogger(), "Worker cell init exit");
}


 