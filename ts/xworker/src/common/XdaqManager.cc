#include "rpct/ts/worker/XdaqManager.h"
#include "rpct/ts/worker/AsyncCommandList.h"
#include "rpct/xdaqutils/LogUtils.h"

#include "ts/toolbox/CellToolbox.h"
#include "ts/framework/CellXhannelCell.h"
#include "ts/framework/CellXhannelRequestCell.h"
#include "ts/framework/CellXhannelRequestXdaqSimple.h"
#include "ts/exception/CellException.h"

#include "xdata/xdata.h"

#include "toolbox/string.h"
#include "xdaq/NamespaceURI.h"

#include "log4cplus/logger.h"
#include "xcept/tools.h"
#include <iostream>

using namespace tsexception;
using namespace tsframework;
using namespace std;

namespace rpcttsworker {

XdaqManager::XdaqManager(rpcttsworker::CellContext* context)
	: logger(log4cplus::Logger::getInstance("XdaqManager"))
{
	if (!context){
		XCEPT_RAISE( xcept::Exception, "Invalid context" );
	}
	this->context = context;
	this->resultSetter = 0;
    LOG4CPLUS_INFO(this->logger, "XdaqManager constructor end");
    xdaqXhannelBuffer = 0;
}

XdaqManager::XdaqManager(rpcttsworker::CellContext* context, IResultSetter* resultSetter)
	: logger(log4cplus::Logger::getInstance("XdaqManager"))
{
	this->context = context;
	this->resultSetter = resultSetter;
    LOG4CPLUS_INFO(this->logger, "XdaqManager constructor end");
    xdaqXhannelBuffer = 0;
}

XdaqManager::~XdaqManager() {
	if (resultSetter){
		delete resultSetter;
	};
}



void XdaqManager::setParameter(tsframework::CellXhannelXdaqSimple& x,
        const std::string& name, const std::string& nameSpace, const std::string& type,
        const std::string& value) {

	// this is called only for external xdaqs
    LOG4CPLUS_INFO(this->logger, "... setParameter called");
    CellXhannelRequestXdaqSimple* req = dynamic_cast<CellXhannelRequestXdaqSimple*> (x.createRequest());
	if (!req){
		XCEPT_RAISE( xcept::Exception, "Invalid xhannel - no CellXhannelRequestXdaqSimple created" );
	}
    req->doParameterSet(name, nameSpace, type, value);
    try {
        x.send(req);
    } catch (xcept::Exception& e) {
        x.removeRequest(req);
        XCEPT_RETHROW(CellException, "Error requesting ParameterSet for xhannel ??" /*+ req->getTargetUrl()*/, e);
    }
    while (!req->hasResponse())
        sleepmillis(100);

    x.removeRequest(req);
}

std::string XdaqManager::getParameter(tsframework::CellXhannelXdaqSimple& x,
        const std::string& name, const std::string& nameSpace, const std::string& type) {

    // this is called only for external xdaqs
    LOG4CPLUS_INFO(this->logger, "... getParameter called");
    CellXhannelRequestXdaqSimple* req = dynamic_cast<CellXhannelRequestXdaqSimple*> (x.createRequest());
    req->doParameterGet(name, nameSpace, type);
    try {
        x.send(req);
    } catch (xcept::Exception& e) {
        x.removeRequest(req);
        XCEPT_RETHROW(CellException, "Error requesting ParameterGet for xhannel ??" /*+ req->getTargetUrl()*/, e);
    }
    while (!req->hasResponse())
        sleepmillis(100);

    std::string res = req->parameterGetReply()->toString();
    x.removeRequest(req);
    return res;
}

void XdaqManager::doCommand(tsframework::CellXhannelXdaqSimple& x,
        const std::string& name, const std::string& nameSpace) {

    // this is called only for external xdaqs
    LOG4CPLUS_INFO(this->logger, "... doCommand called for command " + name);
    CellXhannelRequestXdaqSimple* req = dynamic_cast<CellXhannelRequestXdaqSimple*> (x.createRequest());
    req->doCommand(name, nameSpace);
    try {
        x.send(req);
    } catch (xcept::Exception& e) {
        x.removeRequest(req);
        //XCEPT_RETHROW(CellException, "Error requesting ParameterSet for xhannel 'TC_TEST'.", e);
        XCEPT_RETHROW(CellException, "Error requesting command '" + name + "' for xhannel ??" /*+ req->getTargetUrl()*/, e);
    }

    while (!req->hasResponse()) {
        sleepmillis(100);
    }

    x.removeRequest(req);
}

void XdaqManager::doAsyncCommand(tsframework::CellXhannelXdaqSimple& x,
        const std::string& name, const std::string& commandNameSpace,
        const std::string& parameterNameSpace
        ){
	LOG4CPLUS_INFO(this->logger, "... doAsyncCommand called for command " + name);
	doCommand(x,name+"_start",commandNameSpace);
	LOG4CPLUS_INFO(this->logger, "doAsyncCommand called " + name + "_start");
	std::string xdaqCompleted = "false";
	do{
		sleepmillis(1000);
		xdaqCompleted = getParameter(x, name+"_check",
				parameterNameSpace, "boolean");
		LOG4CPLUS_INFO(this->logger, "doAsyncCommand called " + name + "_check with result "+xdaqCompleted);
	}while(xdaqCompleted != "true");
	LOG4CPLUS_INFO(this->logger, "doAsyncCommand calling for result");
	doCommand(x,name+"_result",commandNameSpace);
	LOG4CPLUS_INFO(this->logger, "doAsyncCommand completed");
}

std::string XdaqManager::getXhannelDescription(CellXhannelXdaqSimple& xhannel){
	xdaq::ApplicationDescriptor * descriptor = xhannel.getTargetDescriptor ();
	ostringstream ss;
	ss << descriptor->getClassName ();
	ss << ":";
	ss << descriptor->getInstance ();
	return ss.str();
}


std::vector<std::string>* XdaqManager::doMultipleAsyncCommand(std::vector<CellXhannelXdaqSimple*>* xdaqList,
        const std::string& name, const std::string& commandNameSpace,
        const std::string& parameterNameSpace
        ){
	std::vector<std::string>* results = new std::vector<std::string>();
	for(std::vector<CellXhannelXdaqSimple*>::iterator it = xdaqList->begin(); it != xdaqList->end(); it++)
	{
		std::string description = this->getXhannelDescription(**it);
		try{
			doAsyncCommand(**it,name,commandNameSpace,parameterNameSpace);
			results->push_back(name+" "+description+" [SUCCESS]");
		}catch(xcept::Exception& e){
			LOG4CPLUS_ERROR (this->logger, xcept::stdformat_exception_history(e));
			results->push_back(name+" "+description+" [ERROR] : " + xcept::stdformat_exception_history(e));
			throw e;
		}
	}
	return results;
}


void XdaqManager::fillResults(std::vector<AsyncCommandList*>* commands){
	if (!resultSetter) return;
	std::vector<std::string>* results = new std::vector<std::string>();
	std::string message;

	for(std::vector<AsyncCommandList*>::iterator it = commands->begin(); it != commands->end(); it++){
		(*it)->fillResults(results);
	}
	message = "<ul>";
	for(std::vector<std::string>::iterator it = results->begin(); it != results->end(); it++){
			message += "<li>" + *it + "</li>";
	}
	delete results;
	message += "</ul>";

	resultSetter->setResult(message);
}

void XdaqManager::doMultipleParallelAsyncCommand(std::vector<AsyncCommandList*>* commands)
{
	bool allCompleted = 0;

	while(!allCompleted){
		allCompleted = 1;
		try{
			for(std::vector<AsyncCommandList*>::iterator it = commands->begin(); it != commands->end(); it++){
				if (!((*it)->isCompleted())){
					allCompleted = 0;
					(*it)->doStep();
				};

			}
		}catch(...){
			fillResults(commands);
			throw;
		}
		fillResults(commands);

		if (!allCompleted){
			sleepmillis(1000);
		}
	}
}


CellXhannelXdaqSimple& XdaqManager::getXdaqXhannelByClass(std::string className) {

    LOG4CPLUS_INFO(this->logger, "Executing AbtractWorkerConfiguration::getXdaqTestBenchXhannel");

    if (xdaqXhannelBuffer == 0) {
        typedef std::map<std::string,CellXhannel*> XhannelsMap;
        XhannelsMap xhannels = context->getFilteredXhannelList();
        for (XhannelsMap::iterator iXhannel = xhannels.begin(); iXhannel != xhannels.end(); iXhannel++) {
            LOG4CPLUS_INFO(this->logger, "Got an xhannel");
            CellXhannelXdaqSimple* x = dynamic_cast<CellXhannelXdaqSimple*> (iXhannel->second);
            if (x != 0) {
            	LOG4CPLUS_INFO(this->logger, "x!=0");
                if (x->getTargetDescriptor()->getClassName() == className) {
                    xdaqXhannelBuffer = x;
                    break;
                }
            }
        }
        if (xdaqXhannelBuffer == 0) {
            XCEPT_RAISE(tsexception::CellException, className+" xhannel could not be found");
        }
    }
    return *xdaqXhannelBuffer;
}

std::vector<CellXhannelXdaqSimple*>* XdaqManager::getXdaqXhannelsListByClass(std::string className) {

    LOG4CPLUS_INFO(this->logger, "Executing AbtractWorkerConfiguration::getXdaqXhannelsListByClass");

    std::vector<CellXhannelXdaqSimple*>* temp = new std::vector<CellXhannelXdaqSimple*>();

    typedef std::map<std::string,CellXhannel*> XhannelsMap;
    XhannelsMap xhannels = context->getFilteredXhannelList();
	for (XhannelsMap::iterator iXhannel = xhannels.begin(); iXhannel != xhannels.end(); iXhannel++) {
		LOG4CPLUS_INFO(this->logger, "Got an xhannel");
		CellXhannelXdaqSimple* x = dynamic_cast<CellXhannelXdaqSimple*> (iXhannel->second);
		if (x != 0) {
			if (x->getTargetDescriptor()->getClassName() == className
					&& x->getTargetDescriptor()->hasInstanceNumber()) {
				temp->push_back(x);
			}
		}
	}

    return temp;
}

}

