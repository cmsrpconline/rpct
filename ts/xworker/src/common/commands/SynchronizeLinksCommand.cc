#include "rpct/ts/worker/commands/SynchronizeLinksCommand.h"

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

#include "rpct/ts/worker/LBCellContext.h"
#include "rpct/ts/worker/xdaqlboxaccess/XdaqLBoxAccess.h"

#include "ts/framework/CellFSM.h"
#include "ts/framework/CellOperation.h"
#include "ts/framework/CellOperationFactory.h"
#include "ts/framework/CellWarning.h"

namespace rpcttsworker {

SynchronizeLinksCommand::SynchronizeLinksCommand(log4cplus::Logger & _logger, tsframework::CellAbstractContext * _context)
    : tsframework::CellCommand(_logger, _context)
    , context_(dynamic_cast<LBCellContext &>(*_context))
{
    logger_ = log4cplus::Logger::getInstance(_logger.getName() + ".SynchronizeLinksCommand");
}

bool SynchronizeLinksCommand::preCondition()
{
    try {
        tsframework::CellFSM & _fsm = context_.getOperationFactory()->getOperation("Run Control").getFSM();
        if (_fsm.getStateName(_fsm.getCurrentState()) != "configured") {
            getWarning().append("Configuration Operation is not in state \"configured\".  Ignoring SynchronizeLinksCommand."
                                , tsframework::CellWarning::ERROR);
            return false;
        }
    } catch (tsexception::OperationDoesNotExist const & _error) {
        getWarning().append("Could not get Configuration Operation to verify state. Ignoring SynchronizeLinksCommand."
                            , tsframework::CellWarning::ERROR);
        return false;
    }
    return true;
}

void SynchronizeLinksCommand::code()
{
    LOG4CPLUS_INFO(logger_, "Executing SynchronizeLinksCommand");

    try {
        context_.getLBoxAccess()->synchronizeLinks();
        getWarning().append("SynchronizeLinksCommand executed.", tsframework::CellWarning::INFO);
        payload_->fromString("Synchronized Links.");
    } catch (...) {
        getWarning().append("Unknown exception while synchronizing links."
                            , tsframework::CellWarning::ERROR);
    }
}

} // namespace rpcttsworker
