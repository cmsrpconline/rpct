#include "rpct/ts/worker/commands/ResetAllNotReadyCBTTCrxs.h"
#include "rpct/ts/worker/xdaqlboxaccess/XdaqLBoxAccess.h"

#include <iostream>

#include "log4cplus/logger.h"

#include "xdata/String.h"

#include "rpct/ts/worker/LBCellContext.h"
//#include "rpct/xdaqutils/xdaqFebSystem.h"

namespace rpcttsworker {

ResetAllNotReadyCBTTCrxs::ResetAllNotReadyCBTTCrxs(log4cplus::Logger & log, tsframework::CellAbstractContext * context)
    : tsframework::CellCommand(log, context)
    , cellcontext_(dynamic_cast<LBCellContext &>(*context))
{
    logger_ = log4cplus::Logger::getInstance(log.getName() + ".ResetAllNotReadyCBTTCrxs");

    //getParamList().insert(std::make_pair("key", new xdata::String("")));
}

void ResetAllNotReadyCBTTCrxs::code()
{
    LOG4CPLUS_INFO(logger_, "Starting ResetAllNotReadyCBTTCrxs::code() method");

    //std::string key = getParamList()["key"]->toString();
    std::string command_result;

    /*if (cellcontext_.getLBoxAccess()->resetAllNotReadyCBTTCrxs())
        command_result = "started resetting all CB TTCrx in not-ready state.";
    else
        command_result = "didn't start resetting all CB TTCrx in not-ready state, another LB action is running.";
     */
    cellcontext_.getLBoxAccess()->resetAllNotReadyCBTTCrxs();
    command_result = "started resetting all CB TTCrx in not-ready state.";

    payload_->fromString(command_result);
    LOG4CPLUS_INFO(logger_, command_result);
}

} // namespace rpcttsworker
