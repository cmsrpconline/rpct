#include "rpct/ts/worker/panels/FebConnectivityTestPanel.h"

#include <ios>

#include "xdata/Vector.h"
#include "xdata/String.h"

#include "ajax/PlainHtml.h"

#include "ajax/ResultBox.h"
#include "ajax/toolbox.h"

#include "ts/framework/CellAbstract.h"

#include "rpct/ts/common/RollSelectionWidget.h"

#include "rpct/ts/common/ajaxtools.h"
#include "rpct/ts/worker/FebConfigurationCellContext.h"
#include "rpct/ts/worker/FebConnectivityTest.h"

#include "rpct/ii/feb_const.h"

#include "ajax/PolymerElement.h"
#include "json/json.h"
#include <sstream>

#include "rpct/ts/worker/panels/LogPanel.h"


namespace rpcttsworker {

FebConnectivityTestPanel::FebConnectivityTestPanel(tsframework::CellAbstractContext * _context
                                                   , log4cplus::Logger & _logger)
    : tsframework::CellPanel(_context, _logger)
    , context_(dynamic_cast<rpct::ts::worker::FebConfigurationCellContext &>(*_context))
    , logger_(log4cplus::Logger::getInstance(_logger.getName() + ".FebConnectivityTestPanel"))
    , rollselection_widget_(*_context, logger_)
{
    rollselection_widget_.setIsOwned(false);

    initLayout(_context->getCell()->getName());
}

FebConnectivityTestPanel::~FebConnectivityTestPanel()
{}

void FebConnectivityTestPanel::initLayout(const std::string & title)
{

}

void FebConnectivityTestPanel::onLockClick(cgicc::Cgicc & _cgi, std::ostream & _out)
{

}

void FebConnectivityTestPanel::onStartFebConfiguration(cgicc::Cgicc & _cgi, std::ostream & _out)
{
    LOG4CPLUS_INFO(logger_, "Preparing FebConfiguration.");

    cgicc::const_form_iterator _fit, _fit_end(_cgi.getElements().end());

    // get settings
    int _vth
        = ((_fit = _cgi["vth"]) != _fit_end) ? _fit->getIntegerValue() : 86;
    int _vmon
        = ((_fit = _cgi["vmon"]) != _fit_end) ? _fit->getIntegerValue() : 1434;
    int _duration
        = ((_fit = _cgi["duration"]) != _fit_end) ? _fit->getIntegerValue() : 0;
    bool _auto_correct
        = ((_fit = _cgi["autocorrect"]) != _fit_end) ? _fit->getIntegerValue() : 1;
    bool _include_disabled
        = ((_fit = _cgi["includedisabled"]) != _fit_end) ? _fit->getIntegerValue() : 1;
    bool _store_output
        = ((_fit = _cgi["storeoutput"]) != _fit_end) ? _fit->getIntegerValue() : 1;

    context_.getFebConnectivityTest().setSelection(rollselection_widget_.getSelection());
    context_.getFebConnectivityTest().setVTh(_vth);
    context_.getFebConnectivityTest().setVMon(_vmon);
    context_.getFebConnectivityTest().setCountDuration(rpct::tools::Time(_duration));
    context_.getFebConnectivityTest().setAutoCorrect(_auto_correct);
    context_.getFebConnectivityTest().setIncludeDisabled(_include_disabled);
    context_.getFebConnectivityTest().setStoreOutput(_store_output);
    context_.getFebConnectivityTest().runFebConfiguration();
    context_.getFebConnectivityTest().getProgressBarWidget().innerHtml(_cgi, _out);
}

void FebConnectivityTestPanel::onStartFebConnectivityTest(cgicc::Cgicc & _cgi, std::ostream & _out)
{
    LOG4CPLUS_INFO(logger_, "Preparing FebConnectivityTest.");

    cgicc::const_form_iterator _fit, _fit_end(_cgi.getElements().end());

    // get settings
    int _vth_low
        = ((_fit = _cgi["vthlow"]) != _fit_end) ? _fit->getIntegerValue() : 16;
    int _vth_high
        = ((_fit = _cgi["vthhigh"]) != _fit_end) ? _fit->getIntegerValue() : 164;
    int _vmon
        = ((_fit = _cgi["vmon"]) != _fit_end) ? _fit->getIntegerValue() : 1434;
    int _duration
        = ((_fit = _cgi["duration"]) != _fit_end) ? _fit->getIntegerValue() : 1;
    bool _auto_correct
        = ((_fit = _cgi["autocorrect"]) != _fit_end) ? _fit->getIntegerValue() : 1;
    bool _use_pulser
        = ((_fit = _cgi["usepulser"]) != _fit_end) ? _fit->getIntegerValue() : 0;
    bool _include_disabled
        = ((_fit = _cgi["includedisabled"]) != _fit_end) ? _fit->getIntegerValue() : 1;

    context_.getFebConnectivityTest().setSelection(rollselection_widget_.getSelection());
    context_.getFebConnectivityTest().setVThLow(_vth_low);
    context_.getFebConnectivityTest().setVThHigh(_vth_high);
    context_.getFebConnectivityTest().setVMon(_vmon);
    context_.getFebConnectivityTest().setCountDuration(rpct::tools::Time(_duration));
    context_.getFebConnectivityTest().setAutoCorrect(_auto_correct);
    context_.getFebConnectivityTest().setUsePulser(_use_pulser);
    context_.getFebConnectivityTest().setIncludeDisabled(_include_disabled);
    context_.getFebConnectivityTest().runConnectivityTest();
    context_.getFebConnectivityTest().getProgressBarWidget().innerHtml(_cgi, _out);
}

void FebConnectivityTestPanel::onStartFebThresholdScan(cgicc::Cgicc & _cgi, std::ostream & _out)
{
    LOG4CPLUS_INFO(logger_, "Preparing FebThresholdScan.");

    cgicc::const_form_iterator _fit, _fit_end(_cgi.getElements().end());

    // get settings
    int _vth_min
        = ((_fit = _cgi["vthmin"]) != _fit_end) ? _fit->getIntegerValue() : 46;
    int _vth_step_size
        = ((_fit = _cgi["vthstepsize"]) != _fit_end) ? _fit->getIntegerValue() : 4;
    int _vth_nsteps
        = ((_fit = _cgi["vthnsteps"]) != _fit_end) ? _fit->getIntegerValue() : 20;
    int _vmon
        = ((_fit = _cgi["vmon"]) != _fit_end) ? _fit->getIntegerValue() : 1434;
    int _duration
        = ((_fit = _cgi["duration"]) != _fit_end) ? _fit->getIntegerValue() : 2;
    bool _auto_correct
        = ((_fit = _cgi["autocorrect"]) != _fit_end) ? _fit->getIntegerValue() : 1;
    bool _use_pulser
        = ((_fit = _cgi["usepulser"]) != _fit_end) ? _fit->getIntegerValue() : 0;
    bool _include_disabled
        = ((_fit = _cgi["includedisabled"]) != _fit_end) ? _fit->getIntegerValue() : 1;

    context_.getFebConnectivityTest().setSelection(rollselection_widget_.getSelection());
    context_.getFebConnectivityTest().setVThMin(_vth_min);
    context_.getFebConnectivityTest().setVThStepSize(_vth_step_size);
    context_.getFebConnectivityTest().setVThNSteps(_vth_nsteps);
    context_.getFebConnectivityTest().setVMon(_vmon);
    context_.getFebConnectivityTest().setCountDuration(rpct::tools::Time(_duration));
    context_.getFebConnectivityTest().setAutoCorrect(_auto_correct);
    context_.getFebConnectivityTest().setUsePulser(_use_pulser);
    context_.getFebConnectivityTest().setIncludeDisabled(_include_disabled);
    context_.getFebConnectivityTest().runThresholdScan();
    context_.getFebConnectivityTest().getProgressBarWidget().innerHtml(_cgi, _out);
}

void FebConnectivityTestPanel::layout(cgicc::Cgicc & cgi) {
    remove();


    setEvent("onReadRollSelection", ajax::Eventable::OnClick, this, &FebConnectivityTestPanel::onReadRollSelection);
    setEvent("onSelectBarrel", ajax::Eventable::OnClick, this, &FebConnectivityTestPanel::onSelectBarrel);
    setEvent("onSelectEndcap", ajax::Eventable::OnClick, this, &FebConnectivityTestPanel::onSelectEndcap);
    setEvent("onSelectByName", ajax::Eventable::OnClick, this, &FebConnectivityTestPanel::onSelectByName);
    setEvent("onClearSelection", ajax::Eventable::OnClick, this, &FebConnectivityTestPanel::onClearSelection);

    setEvent("onStartFebConfiguration", ajax::Eventable::OnClick, this, &FebConnectivityTestPanel::onStartFebConfiguration);
    setEvent("onStartFebConnectivityTest", ajax::Eventable::OnClick, this, &FebConnectivityTestPanel::onStartFebConnectivityTest);
    setEvent("onStartFebThresholdScan", ajax::Eventable::OnClick, this, &FebConnectivityTestPanel::onStartFebThresholdScan);
    
    setEvent("onGetFebUnit", ajax::Eventable::OnClick, this, &FebConnectivityTestPanel::onGetFebUnit);


    setEvent("onMonitorPauseFEBsClick", ajax::Eventable::OnClick, this, &FebConnectivityTestPanel::onMonitorPauseFEBsClick);
    setEvent("onMonitorResumeFEBsClick", ajax::Eventable::OnClick, this, &FebConnectivityTestPanel::onMonitorResumeFEBsClick);
    setEvent("onMonitorTerminateFEBsClick", ajax::Eventable::OnClick, this, &FebConnectivityTestPanel::onMonitorTerminateFEBsClick);
    setEvent("onMonitorRefreshFEBsClick", ajax::Eventable::OnClick, this, &FebConnectivityTestPanel::onMonitorRefreshFEBsClick); 


    ajax::PolymerElement* root = new ajax::PolymerElement("feb-connectivity-test-panel");
    root->set("title",context_.getCell()->getName());


    rpcttspanels::LogPanel* logPanel = new rpcttspanels::LogPanel( getContext(), getLogger() );
    root->add(logPanel);
    

    root->add(&(context_.getFebConnectivityTest().getProgressBarWidget()));


    add(root);
}

void FebConnectivityTestPanel::onGetFebUnit(cgicc::Cgicc & _cgi, std::ostream & _out)
{
    LOG4CPLUS_INFO(logger_, "onGetFebUnit...");


    _out << rpct::preset::feb::unit_;
}


void FebConnectivityTestPanel::onSelectBarrel(cgicc::Cgicc & _cgi, std::ostream & _out)
{
    LOG4CPLUS_INFO(logger_, "onSelectBarrel...");

    rollselection_widget_.select(_cgi, _out);
}

void FebConnectivityTestPanel::onSelectEndcap(cgicc::Cgicc & _cgi, std::ostream & _out)
{
    LOG4CPLUS_INFO(logger_, "onSelectEndcap...");

    rollselection_widget_.select(_cgi, _out);
}

void FebConnectivityTestPanel::onSelectByName(cgicc::Cgicc & _cgi, std::ostream & _out)
{
    LOG4CPLUS_INFO(logger_, "onSelectByName...");

    rollselection_widget_.selectName(_cgi, _out);
}

void FebConnectivityTestPanel::onReadRollSelection(cgicc::Cgicc & _cgi, std::ostream & _out)
{
    LOG4CPLUS_INFO(logger_, "onReadRollSelection...");

    rollselection_widget_.roll_selection_container_->html(_cgi, _out);
}

void FebConnectivityTestPanel::onClearSelection(cgicc::Cgicc & _cgi, std::ostream & _out)
{
    LOG4CPLUS_INFO(logger_, "onClearSelection...");

    rollselection_widget_.clear(_cgi, _out);
}
 
void FebConnectivityTestPanel::onMonitorPauseFEBsClick(cgicc::Cgicc & _cgi, std::ostream & _out) {
    LOG4CPLUS_INFO(logger_, "Called onMonitorPauseFEBsClick.");
    context_.getFebConnectivityTest().getProgressBarWidget().pause(_cgi, _out);
}   
void FebConnectivityTestPanel::onMonitorResumeFEBsClick(cgicc::Cgicc & _cgi, std::ostream & _out) {
    LOG4CPLUS_INFO(logger_, "Called onMonitorResumeFEBsClick.");
    context_.getFebConnectivityTest().getProgressBarWidget().resume(_cgi, _out);
}   
void FebConnectivityTestPanel::onMonitorTerminateFEBsClick(cgicc::Cgicc & _cgi, std::ostream & _out) {
    LOG4CPLUS_INFO(logger_, "Called onMonitorTerminateFEBsClick.");
    context_.getFebConnectivityTest().getProgressBarWidget().terminate(_cgi, _out);
}   
void FebConnectivityTestPanel::onMonitorRefreshFEBsClick(cgicc::Cgicc & _cgi, std::ostream & _out) {
    LOG4CPLUS_INFO(logger_, "Called onMonitorRefreshFEBsClick.");
    context_.getFebConnectivityTest().getProgressBarWidget().refresh(_cgi, _out);
    _out << context_.getFebConnectivityTest().getProgressBarWidget().getProgressBar().getProgressStr();
} 

} // namespace rpcttsworker
