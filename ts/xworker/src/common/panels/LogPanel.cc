#include "rpct/ts/worker/panels/LogPanel.h"
#include "boost/foreach.hpp"
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include "ajax/PolymerElement.h"
#include "ajax/toolbox.h"
#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"
#include "rpct/ts/worker/panels/LogMessageQueue.h"
#include "jsoncpp/json/json.h"
#include "xcept/tools.h"

#include "rpct/ts/worker/LoggerCellContext.h"

namespace rpcttspanels
{
  LogPanel::LogPanel(tsframework::CellAbstractContext* context, log4cplus::Logger& logger, bool isMonitoring, std::string panelTitle)
  :
    CellPanel(context, logger),
    mLogger(logger),
    mCellContext(dynamic_cast<rpcttsworker::LoggerCellContext&>(*context)),
    mIsMonitoring(isMonitoring),
    mPanelTitle(panelTitle)
  {

  }
  LogPanel::~LogPanel()
  {
  }
  void LogPanel::layout(cgicc::Cgicc& cgi)
  {


    this->remove();
    setEvent("LogPanel::getLoggers", ajax::Eventable::OnClick, this, &LogPanel::getLoggers);
    setEvent("LogPanel::getLog", ajax::Eventable::OnClick, this, &LogPanel::getLog);
    ajax::PolymerElement* logPanel = new ajax::PolymerElement("log-panel");

    logPanel->set("paneltitle", mPanelTitle);
    this -> add(logPanel);


    return;
  }
 
  void LogPanel::getLoggers(cgicc::Cgicc& cgi, std::ostream& out) {

    std::string logIds = mCellContext.getAppLogger()->getName();

    Json::Value lLoggersJson(Json::arrayValue);
    
    lLoggersJson.append(logIds);
    // lLoggersJson.append(logIds+".XdaqHardwareAccess.summary");
    // lLoggersJson.append("FebConfiguration");
    // lLoggersJson.append(mCellContext.getLBoxAccess()->getSummaryLogger()->getName());
    // lLoggersJson.append("XdaqHardwareAccess.summary");

    out << lLoggersJson;
    return;
  } 
 
  void LogPanel::getLog(cgicc::Cgicc& cgi, std::ostream& out) {
    
    std::map<std::string, log4cplus::SharedAppenderPtr > mLogAppenders;

    std::string lSelectedLogger = ajax::toolbox::getSubmittedValue(cgi, "logger");

    std::string lCommand = ajax::toolbox::getSubmittedValue(cgi, "command");
    Json::Value lLogJson(Json::objectValue);
    lLogJson["logger"] = lSelectedLogger;

    log4cplus::SharedAppenderPtr  lQueue = mCellContext.getlQueue();

        log4cplus::Logger lLogger_ProgressMonitoring = log4cplus::Logger::getInstance("ProgressMonitoring");
        lLogger_ProgressMonitoring.addAppender(lQueue);

        log4cplus::Logger lLogger = log4cplus::Logger::getInstance(lSelectedLogger);
        lLogger.addAppender(lQueue);

        log4cplus::Logger lLogger_FebConfiguration = log4cplus::Logger::getInstance("FebConfiguration");
        lLogger_FebConfiguration.addAppender(lQueue);

        log4cplus::Logger lLogger_XdaqHardwareAccess = log4cplus::Logger::getInstance("XdaqHardwareAccess.summary");
        lLogger_XdaqHardwareAccess.addAppender(lQueue);

     
      if(lCommand == "clear") dynamic_cast<rpcttsworker::LogMessageQueue*>(lQueue.get())->clear();
      
      std::deque<rpcttsworker::LogMessageQueue::LogMessage> lMessages = dynamic_cast<rpcttsworker::LogMessageQueue*>(lQueue.get())->getMessages();

      Json::Value lMessagesJson(Json::arrayValue);

      BOOST_FOREACH(rpcttsworker::LogMessageQueue::LogMessage& lMessage, lMessages) {

        Json::Value lMessageJson(Json::objectValue);
        lMessageJson["logLevel"] = this -> localLogLevel(lMessage.level);
        lMessageJson["time"] = ctime(&lMessage.time);
        lMessageJson["logger"] = (lMessage.logger.empty() ? "" : lMessage.logger);
        lMessageJson["message"] = lMessage.message;
        std::cout << lMessage.message << std::endl;
        lMessagesJson.append(lMessageJson);
      }
     
      lLogJson["messages"] = lMessagesJson;

     
      lLogJson["found"] = true;

   
     out << lLogJson;
   
    return;
  }
  std::string LogPanel::localLogLevel(log4cplus::LogLevel aLevel ) {
    switch ( aLevel ) {
      case log4cplus::NOT_SET_LOG_LEVEL:
  //    case log4cplus::ALL_LOG_LEVEL:
      case log4cplus::TRACE_LOG_LEVEL:
      case log4cplus::DEBUG_LOG_LEVEL:
      case log4cplus::INFO_LOG_LEVEL:
        return "Info";
      case log4cplus::WARN_LOG_LEVEL:
        return "Warning";
      case log4cplus::ERROR_LOG_LEVEL:
      case log4cplus::FATAL_LOG_LEVEL:
      case log4cplus::OFF_LOG_LEVEL:
        return "Error";
      default:
        return "Unknown";
    }
  }
}
