/*************************************************************************
 * RPCT Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: Krzysztof Niemkiewicz               				         *
 *************************************************************************/
#include "rpct/ts/worker/panels/HistogramPanel.h"
#include "rpct/ts/worker/panels/HistogramsWidget.h"
#include "rpct/ts/worker/RpctCellContext.h"
#include "rpct/xdaqutils/LogUtils.h"

#include "ts/exception/CellException.h"
#include "ts/framework/CellAbstract.h"
#include "ts/framework/CellXhannelTB.h"
#include "ts/framework/CellXhannelRequestTB.h"
#include "ts/framework/CellXhannelXdaqSimple.h"
#include "ts/framework/CellXhannelMonitor.h"
#include "ts/framework/CellXhannelRequestMonitor.h"

#include "xdata/TableIterator.h"

#include <iostream>



#include "rpct/ts/worker/panels/MonitorablesWidget.h"

#include "ajax/PolymerElement.h"
#include "ajax/toolbox.h"
#include "ajax/PlainHtml.h"

// #include "rpct/ts/worker/panels/LogPanel.h"

using namespace std;
using namespace tsframework;
using namespace tsexception;

namespace rpcttspanels {

HistogramPanel::HistogramPanel(CellAbstractContext* context, log4cplus::Logger& logger) :
    CellPanel(context, logger) { 
    logger_ =  log4cplus::Logger::getInstance(getLogger().getName() + ".RpctTCHistogramPanel");
}


HistogramPanel::~HistogramPanel() {
    LOG4CPLUS_INFO(getLogger(), "RpctTCHistogramPanel destructor");
}

void HistogramPanel::layout(cgicc::Cgicc& cgi) {

    LOG4CPLUS_INFO(getLogger(), "layout");


    remove();
    add(new ajax::PlainHtml("<h2>Histograms</h2>"));

    rpcttsworker::RpctCellContext* rpctContext = dynamic_cast<rpcttsworker::RpctCellContext*>(getContext());
    add(rpctContext->getHistogramsWidget());


}

}

