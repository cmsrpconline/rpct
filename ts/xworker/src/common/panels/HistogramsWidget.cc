#include "rpct/ts/worker/panels/HistogramsWidget.h"
#include "rpct/ts/worker/panels/MonitorablesWidget.h"
#include "rpct/ts/worker/XdaqHardwareAccess.h"
#include "rpct/hardwareTests/include/rpct/hardwareTests/IStatisticDiagManager.h"
#include "rpct/xdaqutils/XdaqCommonUtils.h"
#include "rpct/ii/Monitorable.h"
#include "rpct/diag/TDiagCtrl.h"


#include "xdata/TableIterator.h"
#include "xcept/tools.h"

#include "TStyle.h" //ROOT
#include "TFile.h"

#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/sync/named_mutex.hpp>

#include <string> 

using namespace std;
using rpct::IDiagCtrl;


rpcttspanels::HistogramsWidget::HistogramsWidget(xdaq::Application* app, rpct::MonitorablesWidget* monitorablesWidget)  :
            mutex_(toolbox::BSem::FULL), app_(app), logScale_(true), monitorablesWidget_(monitorablesWidget) {
    setIsOwned(false);
    histoMonitoringWorkLoop_ = toolbox::task::getWorkLoopFactory()->getWorkLoop("HistoMonitoringWorkLoop."
            + app->getApplicationDescriptor()->getURN(), "waiting");
}


void rpcttspanels::HistogramsWidget::html(cgicc::Cgicc& cgi, std::ostream& out) {
    for (rpct::StatisticDiagManagersMap::iterator itSMgr = statisticDiagManagersMap_.begin();
            itSMgr != statisticDiagManagersMap_.end(); ++itSMgr) {

        out << itSMgr->second->getHtml();
    }

}

void rpcttspanels::HistogramsWidget::clear() {
    rpct::MutexHandler handler(mutex_);
    for (rpcttspanels::StatisticDiagManagersMap::iterator itSMgr = statisticDiagManagersMap_.begin(); itSMgr
    != statisticDiagManagersMap_.end(); ++itSMgr) {
        delete itSMgr->second;
    }
    statisticDiagManagersMap_.clear();
}

void rpcttspanels::HistogramsWidget::addManager( std::string name, rpct::IStatisticDiagManager* manager) {
    rpct::MutexHandler handler(mutex_);
    manager->setStopHistoPtr(&stopHisto_);
    statisticDiagManagersMap_[name] = manager;
}


void rpcttspanels::HistogramsWidget::start(std::string runNumber, toolbox::BSem* hardwareMutex) {

    if (!histoMonitoringWorkLoop_->isActive()) {
        hardwareMutex_ = hardwareMutex;

        LOG4CPLUS_INFO(XdaqHardwareAccess::getSummaryLogger(),  "Starting histo monitoring");
        runNumber_ = runNumber;
        stopHisto_ = false;
        histoMonitoringWorkLoop_->submit(toolbox::task::bind(this,
                &rpcttspanels::HistogramsWidget::histoMonitorJob, "HistoMonitorJob"));
        histoMonitoringWorkLoop_->activate();
        LOG4CPLUS_INFO(app_->getApplicationLogger(), __FUNCTION__<<": "<< "Started histo monitoring");
    }
}

void rpcttspanels::HistogramsWidget::stop() {

    if (histoMonitoringWorkLoop_->isActive()) {
        LOG4CPLUS_INFO(XdaqHardwareAccess::getSummaryLogger(),  "Stopping histo monitoring");
        stopHisto_ = true;
        histoMonitoringWorkLoop_->cancel();
    }
}



bool rpcttspanels::HistogramsWidget::histoMonitorJob(toolbox::task::WorkLoop* wl) {
    //boost::interprocess::named_mutex vme_mutex(boost::interprocess::open_or_create, "vme_mutex");
    {
        //boost::interprocess::scoped_lock<boost::interprocess::named_mutex> lock(vme_mutex);
        monitorablesWidget_->monitorHardware(&stopHisto_); //we want to monitor everything before the histogrms starts,
    }
	monitorablesWidget_->collectMonitorableStaus();
	monitorablesWidget_->analyseAndGenerateAlarms();
	//mutex is taken inside the monitor()

    time_t timer = time(NULL);
    tm* tblock = localtime(&timer);
    tblock->tm_mon += 1;

    std::stringstream sfile;
    //    string sdir = "/rpctdata/LBMonitorHistos/" + sfile.str();

    sfile << filenamePrefix_ << "_run_"<<runNumber_<<"_"
            << tblock->tm_year + 1900<< '_'
            << tblock->tm_mon << '_' << tblock->tm_mday << "__" << tblock->tm_hour << '_'
            << tblock->tm_min << '_' << tblock->tm_sec<<".root";

    LOG4CPLUS_INFO(app_->getApplicationLogger(), "\n monitorHistograms. Creating file: " << sfile.str() << endl);

    TFile outFile(sfile.str().c_str(), "new");
    try {
        const uint64_t secBx = 40000000;
        // const uint64_t countersLimit = 0xffffffffffull;//10 * secBx;
        const uint64_t coutingPeriod = 10 * secBx;

        //        const IDiagCtrl::TTriggerType triggerType = IDiagCtrl::ttManual;

        time_t startTime = time(NULL);
        LOG4CPLUS_INFO(app_->getApplicationLogger(), __FUNCTION__<<": "<< "Initialize");


        for(rpcttspanels::StatisticDiagManagersMap::iterator itSMgr = statisticDiagManagersMap_.begin(); itSMgr != statisticDiagManagersMap_.end(); ++itSMgr) {
            itSMgr->second->initialize(startTime);
        }

        gStyle->SetTimeOffset(startTime);
        gStyle->SetPalette(1);

        TTree tree("tree",treeName_.c_str());
        LOG4CPLUS_INFO(app_->getApplicationLogger(), __FUNCTION__<<": "<< "Create branches");

        for(rpcttspanels::StatisticDiagManagersMap::iterator itSMgr = statisticDiagManagersMap_.begin(); itSMgr != statisticDiagManagersMap_.end(); ++itSMgr) {
            itSMgr->second->createBranches(tree);
        }

        { // separate scope for handler
            if (stopHisto_)
                return false;
            LOG4CPLUS_INFO(app_->getApplicationLogger(), __FUNCTION__<<": "<< "resetAndConfigHistos");
            rpct::MutexHandler handler(hardwareMutex_);
            for(rpcttspanels::StatisticDiagManagersMap::iterator itSMgr = statisticDiagManagersMap_.begin();
                    itSMgr != statisticDiagManagersMap_.end(); ++itSMgr) {
                itSMgr->second->resetAndConfigHistos();
            }
        }

        { // separate scope for handler
            if (stopHisto_)
                return false;
            LOG4CPLUS_INFO(app_->getApplicationLogger(), __FUNCTION__<<": "<< "start");
            rpct::MutexHandler handler(hardwareMutex_);
            for(rpcttspanels::StatisticDiagManagersMap::iterator itSMgr = statisticDiagManagersMap_.begin();
                    itSMgr != statisticDiagManagersMap_.end(); ++itSMgr) {
                itSMgr->second->start();
            }
        }
        LOG4CPLUS_INFO(app_->getApplicationLogger(), __FUNCTION__<<": "<< "main loop");

        while (!stopHisto_) { //one iteration = readback of all histograms
        	monitorablesWidget_->monitorHardware(&stopHisto_); //clears global messages, TODO - is it the best place for that???
            while (true) {
                usleep(1000000);
                //boost::interprocess::scoped_lock<boost::interprocess::named_mutex> lock(vme_mutex);
                //cout << "*" << flush;

                if(stopHisto_)
                    break;
                rpcttspanels::StatisticDiagManagersMap::iterator itSMgr = statisticDiagManagersMap_.begin();
                if(itSMgr != statisticDiagManagersMap_.end()) {
                    rpct::MutexHandler handler(hardwareMutex_);
                    if( itSMgr->second->countingEnded(coutingPeriod)) {
                        break;
                    }
                }
            }

            if(stopHisto_) {
                //boost::interprocess::scoped_lock<boost::interprocess::named_mutex> lock(vme_mutex);
            	monitorablesWidget_->collectMonitorableStaus();
            	monitorablesWidget_->analyseAndGenerateAlarms();
            	break;
            }

            bool dataValid = false;
            {
            	rpct::MutexHandler handler(hardwareMutex_);
            	//boost::interprocess::scoped_lock<boost::interprocess::named_mutex> lock(vme_mutex);
            	LOG4CPLUS_INFO(app_->getApplicationLogger(), __FUNCTION__<<": "<< "readout start");
            	for(rpcttspanels::StatisticDiagManagersMap::iterator itSMgr = statisticDiagManagersMap_.begin();
            			itSMgr != statisticDiagManagersMap_.end(); ++itSMgr) {
            		dataValid = itSMgr->second->readout(true);//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            	}
            	LOG4CPLUS_INFO(app_->getApplicationLogger(), __FUNCTION__<<": "<< "readout end");
            }

            if(stopHisto_)
                break;
            LOG4CPLUS_INFO(app_->getApplicationLogger(), __FUNCTION__<<": "<< "analyze");

            for(rpcttspanels::StatisticDiagManagersMap::iterator itSMgr = statisticDiagManagersMap_.begin();
                    itSMgr != statisticDiagManagersMap_.end(); ++itSMgr) {
                itSMgr->second->setLogScale(logScale_);
                if(dataValid) {
                	rpct::Monitorable::MonitorStatusList& rateWarnings = itSMgr->second->analyse();//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                	monitorablesWidget_->handleRateWarnings(rateWarnings);
                }
            }
            monitorablesWidget_->collectMonitorableStaus();//in this way the warnings generated during the readout() and analyse() are also included
            monitorablesWidget_->analyseAndGenerateAlarms();
            if(dataValid) {
            	LOG4CPLUS_INFO(app_->getApplicationLogger(), __FUNCTION__<<": "<< "save");
            	outFile.cd();
            	tree.Fill();
            	tree.AutoSave("SaveSelf");
            }
            else {
            	LOG4CPLUS_INFO(app_->getApplicationLogger(), __FUNCTION__<<": data not valid, not saved");
            }
            LOG4CPLUS_INFO(app_->getApplicationLogger(), __FUNCTION__<<" "<<__LINE__<<": "<< "after save");
            /*if option contains "SaveSelf", gDirectory->SaveSelf() is called.
              This allows another process to analyze the Tree while the Tree is being filled.*/
        }//main loop end

        { // separate scope for handler
            rpct::MutexHandler handler(hardwareMutex_);
            //boost::interprocess::scoped_lock<boost::interprocess::named_mutex> lock(vme_mutex);
            for(rpcttspanels::StatisticDiagManagersMap::iterator itSMgr = statisticDiagManagersMap_.begin();
                    itSMgr != statisticDiagManagersMap_.end(); ++itSMgr) {
                itSMgr->second->stop();
            }
            LOG4CPLUS_INFO(app_->getApplicationLogger(), __FUNCTION__<<":"<<__LINE__<<": "<< "all histograms stopped");
        }

        outFile.cd();
        for(rpcttspanels::StatisticDiagManagersMap::iterator itSMgr = statisticDiagManagersMap_.begin();
                itSMgr != statisticDiagManagersMap_.end(); ++itSMgr) {
            itSMgr->second->finalize();
        }
        LOG4CPLUS_INFO(app_->getApplicationLogger(), __FUNCTION__<<":"<<__LINE__<<": "<< "finished");
    }
    catch (xcept::Exception& e) {//I think this is never thrown here (KB)
    	monitorablesWidget_->handleMonitorException(e, "Exception during histo monitoring:");
    	monitorablesWidget_->analyseAndGenerateAlarms();

        ostringstream str;
        str << "Exception in histo monitoring workloop: " << xcept::stdformat_exception_history(e);
        LOG4CPLUS_ERROR(app_->getApplicationLogger(), str.str());
        ALARM_EXCEPT(app_,"ERROR",e);

        outFile.cd();
        for(rpcttspanels::StatisticDiagManagersMap::iterator itSMgr = statisticDiagManagersMap_.begin(); itSMgr != statisticDiagManagersMap_.end(); ++itSMgr) {
            itSMgr->second->finalize();
        }
    }
    catch (std::exception& e) {
    	monitorablesWidget_->handleMonitorException(e, "Exception during histo monitoring:");
    	monitorablesWidget_->analyseAndGenerateAlarms();

        ostringstream str;
        str << "Exception in histo monitoring workloop: " << e.what();
        LOG4CPLUS_ERROR(app_->getApplicationLogger(), str.str());
        ALARM_STD(app_,"ERROR",e);
        //lbStatisticDiagManager_->finalize(); //might not be possible to call it
    }
    /*catch (rpct::CCUException& e) {//theoretically it has no sense, since the CCUException inherits from the std::exception, but for some reason the CCUExceptions are not caught here
        monitorablesWidget_->handleMonitorException(e, "CCUException during histo monitoring:");
        monitorablesWidget_->analyseAndGenerateAlarms();

        ostringstream str;
        str << "Exception in histo monitoring workloop: " << e.what();
        LOG4CPLUS_ERROR(app_->getApplicationLogger(), str.str());
        ALARM_STD(app_,"ERROR",e);
        //lbStatisticDiagManager_->finalize(); //might not be possible to call it
    }*/
    catch (...) {//theoretically it has no sense, since the CCUException inherits from the std::exception, but for some reason the CCUExceptions are not caught here
        rpct::exception::Exception e("unknown exception during histo monitoring");
        monitorablesWidget_->handleMonitorException(e, "unknown exception  during histo monitoring:");
        monitorablesWidget_->analyseAndGenerateAlarms();

        ostringstream str;
        str << "Exception in histo monitoring workloop: " << e.what();
        LOG4CPLUS_ERROR(app_->getApplicationLogger(), str.str());
        ALARM_STD(app_,"ERROR",e);
        //lbStatisticDiagManager_->finalize(); //might not be possible to call it
    }

    //histoRunning_ = false;
    return false;
}
