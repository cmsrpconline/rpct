#include "rpct/ts/worker/panels/MessagesPanel.h"
#include "rpct/xdaqutils/LogUtils.h"
#include "rpct/ts/worker/RpctCellContext.h"

#include "ts/exception/CellException.h"
#include "ts/framework/CellAbstract.h"
#include "ts/framework/CellXhannelTB.h"
#include "ts/framework/CellXhannelRequestTB.h"
#include "ts/framework/CellXhannelXdaqSimple.h"
#include "ts/framework/CellXhannelMonitor.h"
#include "ts/framework/CellXhannelRequestMonitor.h"

#include <iostream>

#include "rpct/ts/worker/panels/MonitorablesWidget.h"

#include "ajax/PolymerElement.h"
#include "ajax/toolbox.h"

#include "ajax/PlainHtml.h"


#include "rpct/ts/worker/RpctCellContext.h"

#include "rpct/ts/worker/panels/LogPanel.h"




// #include "ajax/Div.h"
#include "rpct/ts/common/ajaxtools.h"
#include "ajax/ResultBox.h"
using namespace rpct;


using namespace std;
using namespace tsframework;
using namespace tsexception;
using namespace rpcttsworker;


namespace rpcttspanels {

MessagesPanel::MessagesPanel(CellAbstractContext* context, log4cplus::Logger& logger)
:
    CellPanel(context, logger)    
    // mLogger(logger),
    // mLoggerwidget(dynamic_cast<rpcttsworker::RpctCellContext&>(*context).getLoggerwidget()),
    // resultBox_(new ajax::ResultBox())
{
    logger_ =  log4cplus::Logger::getInstance(getLogger().getName() + ".MessagesPanel");
    // resultBox_->setIsOwned(false);
    // rpcttsworker::RpctCellContext* rpctContext = dynamic_cast<rpcttsworker::RpctCellContext*>(context);
    // add(rpctContext->getLoggerwidget()->getResultBox());
    // add(rpctContext->getMonitorablesWidget());
}

MessagesPanel::~MessagesPanel() {
    LOG4CPLUS_INFO(getLogger(), "MessagesPanel destructor");
}

void MessagesPanel::layout(cgicc::Cgicc& cgi) {
    //LOG4CPLUS_INFO(*mCellContext.getAppLogger(), "MessagesPanel layout");

    remove();

    ajax::PolymerElement* root = new ajax::PolymerElement("messages-panel");
    LogPanel* logPanel = new LogPanel( getContext(), getLogger() );
    root->add(logPanel);
    add(root);


   // test button-widget
    // add(new LogPanel( getContext(), getLogger() ));
    // ajax::ResultBox * lockbox_ = new ajax::ResultBox();
    // add(lockbox_);
    // ajax::Div * lockdiv_ = new ajax::Div();
    // lockdiv_->set("id", "OI");
    // lockbox_->add(lockdiv_);
    // ajax::PolymerElement * testButton = ajaxtools::button(this, "oi");
    // ajax::PlainHtml * testButton = new ajax::PlainHtml("ooooooooooooooi");
    // lockdiv_->add(testButton);
    // lockbox_->add(testButton);
    // setEvent(testButton, ajax::Eventable::OnClick, lockbox_, this, &MessagesPanel::testButtonWidget);



    rpcttsworker::RpctCellContext* rpctContext = dynamic_cast<rpcttsworker::RpctCellContext*>(getContext());
    add(rpctContext->getMonitorablesWidget());



//    add(new ajax::PlainHtml("<h1>Oi</h1>"));

// 	add(new ajax::PolymerElement("messages-panel"));

    // add(new ajax::PolymerElement("log-panel"));

 	// add(mLoggerwidget->html( cgi, out));
 	// rpcttsworker::RpctCellContext* rpctContext = dynamic_cast<rpcttsworker::RpctCellContext*>(context);
 	// add(mCellContext.getLoggerwidget()->getResultBox());
    // add(rpctContext->getMonitorablesWidget());

    
 


}

void MessagesPanel::refresh(cgicc::Cgicc& cgi, std::ostream& out) {
    LOG4CPLUS_INFO(getLogger(), "MessagesPanel Refresh");
}

void MessagesPanel::testButtonWidget(cgicc::Cgicc& cgi, std::ostream& out) {
    LOG4CPLUS_INFO(getLogger(), "MessagesPanel testButtonWidget");
    std::cout << "Vai" << std::endl;
}



}
