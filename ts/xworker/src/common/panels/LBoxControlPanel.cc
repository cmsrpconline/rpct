#include "rpct/ts/worker/panels/LBoxControlPanel.h"
#include "rpct/ts/worker/xdaqlboxaccess/XdaqLBoxAccess.h"
#include "rpct/ts/worker/LBCell.h"
#include "rpct/xdaqutils/LogUtils.h"
#include "rpct/xdaqutils/XdaqCommonUtils.h"

#include "ts/exception/CellException.h"
#include "ts/framework/CellAbstract.h"
#include "xdata/TableIterator.h"

#include "ajax/PlainHtml.h"

#include "ajax/toolbox.h"
#include "ajax/Div.h"

#include "ajax/PolymerElement.h"

#include "jsoncpp/json/json.h"
#include <sstream>

#include <iostream>

#include "rpct/ts/worker/panels/LogPanel.h"




using namespace std;
using namespace tsframework;
using namespace tsexception;
using namespace rpct::xdaqutils;
using namespace rpcttsworker;
namespace rpcttspanels {


  void setSelectionStatus(std::vector<LBoxControlPanel::BoardView*>& views, bool status) {
    for (std::vector<LBoxControlPanel::BoardView*>::iterator it = views.begin(); it != views.end(); ++it) {
      LBoxControlPanel::BoardView* view = *it;
    }
  }

  LBoxControlPanel::LBoxControlPanel(CellAbstractContext* context, log4cplus::Logger& logger) :
  CellPanel(context, logger), lboxAccess_(dynamic_cast<rpcttsworker::LBCellContext*>(context)->getLBoxAccess())
  {
    logger_ =  log4cplus::Logger::getInstance(getLogger().getName() + ".LBoxControlPanel");
    
    LOG4CPLUS_INFO(getLogger(), "LBoxControlPanel constructor");



  initLayout(dynamic_cast<rpcttsworker::RpctCellContext*>(context));
  rpcttsworker::RpctCellContext* rpctContext = dynamic_cast<rpcttsworker::RpctCellContext*>(context);
  rpctContext->getLoggerwidget();
}


void LBoxControlPanel::layout(cgicc::Cgicc& cgi) {

  LOG4CPLUS_INFO(getLogger(), "layout");
  remove();

  // main panel
  // layout stuff
  setEvent("doGetHalfBoxes", ajax::Eventable::OnClick, this, &LBoxControlPanel::GetHalfBoxes);
  setEvent("doGetRBCConfigKeys", ajax::Eventable::OnClick, this, &LBoxControlPanel::GetRBCConfigKeys);
  setEvent("doGetLBConfigKeys", ajax::Eventable::OnClick, this, &LBoxControlPanel::GetLBConfigKeys);
  setEvent("doChangeLbKey", ajax::Eventable::OnClick, this, &LBoxControlPanel::onChangeLbKey);
  setEvent("doChangeRbcKey", ajax::Eventable::OnClick, this, &LBoxControlPanel::onChangeRbcKey);

  // buttons
  setEvent("onResetFEC", ajax::Eventable::OnClick, this, &LBoxControlPanel::onResetFec);
  setEvent("onHardReset", ajax::Eventable::OnClick, this, &LBoxControlPanel::onHardReset);
  setEvent("onCheckHardware", ajax::Eventable::OnClick, this, &LBoxControlPanel::onCheckHardware);
  setEvent("onResetNotReadyCBTTCrxs", ajax::Eventable::OnClick, this, &LBoxControlPanel::onResetAllNotReadyCBTTCrxs);
  setEvent("onSetupBoards", ajax::Eventable::OnClick, this, &LBoxControlPanel::onSetupBoards);
  setEvent("onMakeFlashConfig", ajax::Eventable::OnClick, this, &LBoxControlPanel::onMakeFlashConfig);
  setEvent("onLoadFromCCU", ajax::Eventable::OnClick, this, &LBoxControlPanel::onLoadFromCCU);
  setEvent("onLoadFromFlash", ajax::Eventable::OnClick, this, &LBoxControlPanel::onLoadFromFlash);
  setEvent("onProgramFlash", ajax::Eventable::OnClick, this, &LBoxControlPanel::onProgramFlash);
  setEvent("onTest", ajax::Eventable::OnClick, this, &LBoxControlPanel::onTest);
  setEvent("onSynchronizeLinks", ajax::Eventable::OnClick, this, &LBoxControlPanel::onSynchronizeLinks);
  setEvent("onReadCBIC_RELOAD_CNT", ajax::Eventable::OnClick, this, &LBoxControlPanel::onReadCBIC_RELOAD_CNT);
  setEvent("onResetCBTTCrxs", ajax::Eventable::OnClick, this, &LBoxControlPanel::onResetCBTTCrxs);
  setEvent("onLinkBoxTest", ajax::Eventable::OnClick, this, &LBoxControlPanel::onLinkBoxTest);
  setEvent("onLinkBoxTestContinue", ajax::Eventable::OnClick, this, &LBoxControlPanel::onLinkBoxTestContinue);


  ajax::PolymerElement* root = new ajax::PolymerElement("lbox-control-panel");
 
  // logger 
  rpcttspanels::LogPanel* logPanel = new rpcttspanels::LogPanel( getContext(), getLogger() );
  root->add(logPanel);


  add(root);
}

void LBoxControlPanel::GetHalfBoxes(cgicc::Cgicc& cgi, std::ostream& out) {
  LOG4CPLUS_INFO(getLogger(), "Getting Half Boxes...");

  //root JSON
  Json::Value halfboxesArrayJSON(Json::arrayValue);


  for (std::vector<rpct::HalfBox*>::const_iterator halfBox = lboxAccess_->getLinkSystem()->getHalfBoxes().begin(); halfBox != lboxAccess_->getLinkSystem()->getHalfBoxes().end(); ++halfBox) {

    std::string halfBoxStr = "";
    std::string controlBoardStr = "";
    std::string rbcBoardStr = "";
    std::vector<std::string> linkBoardsStr;

    //get CBs
    controlBoardStr = (*halfBox)->getCb().getDescription();

    // get RBCs
    if ((*halfBox)->getRbc()) {
      rbcBoardStr = (*halfBox)->getRbc()->getDescription();
    }
    
    rpct::LinkBox & linkBox = (*halfBox)->getLinkBox();
    if ((*halfBox) == linkBox.getHalfBox10()) {
      halfBoxStr = linkBox.getDescription()+"_10";
    } else {
      halfBoxStr = linkBox.getDescription()+"_20";
    }
   
    for (rpct::LinkSystem::LinkBoards::const_iterator linkBoard = (*halfBox)->getLinkBoards().begin(); linkBoard != (*halfBox)->getLinkBoards().end(); ++linkBoard) {
      linkBoardsStr.push_back((*linkBoard)->getDescription());
    } 

    // loop over halfboxes
    Json::Value halfboxesJSON;

    // halfb
    halfboxesJSON["halfb"] = halfBoxStr;
    halfboxesJSON["control_board"] = controlBoardStr;

    // control_board
    Json::Value control_boardArrayJSON(Json::arrayValue);

    for(vector<string>::const_iterator i = linkBoardsStr.begin(); i != linkBoardsStr.end(); ++i) {
      control_boardArrayJSON.append(*i);
    }
    halfboxesJSON["link_board"] = control_boardArrayJSON;

    // rbc_board
    halfboxesJSON["rbc_board"] = rbcBoardStr;

    halfboxesArrayJSON.append(halfboxesJSON);
  } // end loop over HBs

  // std::cout << halfboxesArrayJSON << std::endl;

  out << halfboxesArrayJSON;
}

void LBoxControlPanel::GetLBConfigKeys(cgicc::Cgicc& cgi, std::ostream& out) {
  LOG4CPLUS_INFO(getLogger(), "Getting LB Config Keys...");
  Json::Value lbconfigkeysJSON;
  std::vector<std::string> configKeys;
  try{
    // we could also take keys directly from db here
    configKeys = lboxAccess_->getLBConfigKeys();
  }catch(...){
    // config keys remain empty
  }
 
  for (size_t i = 0; i < configKeys.size(); i++) {
    lbconfigkeysJSON.append(configKeys[i]);
  }
  lbConfigKey_ = configKeys[0];

// lbconfigkeysJSON.append("OI");
  out << lbconfigkeysJSON;
}

void LBoxControlPanel::GetRBCConfigKeys(cgicc::Cgicc& cgi, std::ostream& out) {
  LOG4CPLUS_INFO(getLogger(), "Getting RBC Config Keys...");
  Json::Value rbcconfigkeysJSON;
  std::vector<std::string> configKeys;
  try{
    // we could also take keys directly from db here
    configKeys = lboxAccess_->getRBCConfigKeys();
  }catch(...){
    // config keys remain empty
  }
 
  for (size_t i = 0; i < configKeys.size(); i++) {
    rbcconfigkeysJSON.append(configKeys[i]);
  }
  rbcConfigKey_ = configKeys[0];

// rbcconfigkeysJSON.append("OI");
  out << rbcconfigkeysJSON;
}



LBoxControlPanel::~LBoxControlPanel() {
  LOG4CPLUS_INFO(getLogger(), "LBoxControlPanel destructor");

}


void LBoxControlPanel::initLayout(rpcttsworker::RpctCellContext* rpctContext){

}

ajax::Table* LBoxControlPanel::initCheckBoxTable() {

}



void LBoxControlPanel::refresh(cgicc::Cgicc& cgi, std::ostream& out) {
  LOG4CPLUS_INFO(getLogger(), "Refresh");
  resultTable_->innerHtml(cgi, out);
}

void LBoxControlPanel::onResetFec(cgicc::Cgicc& cgi, std::ostream& out) {
  {
    rpct::MutexHandler handler(lboxAccess_->getHardwareMutex());
    lboxAccess_->resetFec();
  }
  // refresh(cgi,out);
}

void LBoxControlPanel::onHardReset(cgicc::Cgicc& cgi, std::ostream& out) {
  {
    rpct::MutexHandler handler(lboxAccess_->getHardwareMutex());
    lboxAccess_->reset();
  }
  // refresh(cgi,out);
}


void LBoxControlPanel::onCheckHardware(cgicc::Cgicc& cgi, std::ostream& out) {
	{
		lboxAccess_->checkHardware();
	}
	// refresh(cgi,out);
}

void LBoxControlPanel::onLoadFromCCU(cgicc::Cgicc& cgi, std::ostream& out) {
  std::string controlBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "controlBoards");
  std::string linkBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "linkBoards");
  std::string rbcBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "rbcBoards");

  lboxAccess_->loadFromCcu(getSelectedControlBoards(controlBoardsStr), getSelectedLinkBoards(linkBoardsStr));
  // refresh(cgi,out);
}


void LBoxControlPanel::onLoadFromFlash(cgicc::Cgicc& cgi, std::ostream& out) {
  std::string controlBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "controlBoards");
  std::string linkBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "linkBoards");
  std::string rbcBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "rbcBoards");
  lboxAccess_->loadFromFlash(getSelectedControlBoards(controlBoardsStr), getSelectedLinkBoards(linkBoardsStr));
  // refresh(cgi,out);
}

void LBoxControlPanel::onProgramFlash(cgicc::Cgicc& cgi, std::ostream& out) {
  std::string controlBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "controlBoards");
  std::string linkBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "linkBoards");
  std::string rbcBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "rbcBoards");

  lboxAccess_->programFlash(getSelectedControlBoards(controlBoardsStr), getSelectedLinkBoards(linkBoardsStr));
  // refresh(cgi,out);
}

void LBoxControlPanel::onTest(cgicc::Cgicc& cgi, std::ostream& out) {
  std::string controlBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "controlBoards");
  std::string linkBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "linkBoards");
  std::string rbcBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "rbcBoards");

  lboxAccess_->testLBs(getSelectedLinkBoards(linkBoardsStr));
  // refresh(cgi,out);
}

void LBoxControlPanel::onSynchronizeLinks(cgicc::Cgicc& cgi, std::ostream& out){
  std::string controlBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "controlBoards");
  std::string linkBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "linkBoards");
  std::string rbcBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "rbcBoards");

	rpct::LinkSystem::RbcBoards rbcBoards;
	lboxAccess_->synchronizeLinks(getSelectedLinkBoards(linkBoardsStr), rbcBoards);
	// refresh(cgi,out);
}

void LBoxControlPanel::onSetupBoards(cgicc::Cgicc& cgi, std::ostream& out) {
  std::vector<string> keys;
  if (lbConfigKey_ != NULL) {
    keys.push_back(lbConfigKey_);
  }
  if (rbcConfigKey_ != NULL) {
    keys.push_back(rbcConfigKey_);
  }
  std::string controlBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "controlBoards");
  std::string linkBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "linkBoards");
  std::string rbcBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "rbcBoards");

  // getSelectedControlBoards(controlBoardsStr);
  lboxAccess_->configureBoards(keys,true, true, getSelectedControlBoards(controlBoardsStr), getSelectedLinkBoards(linkBoardsStr), getSelectedRbcBoards(rbcBoardsStr));
}

void LBoxControlPanel::onMakeFlashConfig(cgicc::Cgicc& cgi, std::ostream& out) {
  std::vector<string> keys;
  if (lbConfigKey_ != NULL) {
    keys.push_back(lbConfigKey_);
  }
  if (rbcConfigKey_ != NULL) {
    keys.push_back(rbcConfigKey_);
  }
  std::string controlBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "controlBoards");
  std::string linkBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "linkBoards");
  std::string rbcBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "rbcBoards");
  ConfigurationSetBagPtr confSetBag = lboxAccess_->getConfigurationSetForSelectedBoards(keys, getSelectedLinkBoards(linkBoardsStr), getSelectedRbcBoards(rbcBoardsStr));
  lboxAccess_->makeFlashConfig(confSetBag, getSelectedLinkBoards(linkBoardsStr));
}

void LBoxControlPanel::onReadCBIC_RELOAD_CNT(cgicc::Cgicc& cgi, std::ostream& out) {
  std::string controlBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "controlBoards");
  std::string linkBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "linkBoards");
  std::string rbcBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "rbcBoards");
  lboxAccess_->readCBIC_RELOAD_CNT(getSelectedControlBoards(controlBoardsStr) );
}

void LBoxControlPanel::onResetCBTTCrxs(cgicc::Cgicc& cgi, std::ostream& out) {
  std::string controlBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "controlBoards");
  std::string linkBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "linkBoards");
  std::string rbcBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "rbcBoards");
  lboxAccess_->resetCBTTCrxs(getSelectedControlBoards(controlBoardsStr));
}

void LBoxControlPanel::onResetAllNotReadyCBTTCrxs(cgicc::Cgicc& cgi, std::ostream& out) {
  lboxAccess_->resetAllNotReadyCBTTCrxs();
}

rpct::LinkSystem::LinkBoards LBoxControlPanel::getSelectedLinkBoards(std::string boardsStr) {
  std::stringstream ss(boardsStr);
  std::vector<std::string> boardsList;
  while(ss.good()) {
      std::string substr;
      getline(ss, substr, ',');
      boardsList.push_back(substr);
  }

  rpct::LinkSystem::LinkBoards boards;
  for (std::vector<std::string>::iterator it = boardsList.begin(); it != boardsList.end(); ++it) {
    for (std::vector<rpct::HalfBox*>::const_iterator halfBox = lboxAccess_->getLinkSystem()->getHalfBoxes().begin(); halfBox != lboxAccess_->getLinkSystem()->getHalfBoxes().end(); ++halfBox) {
      for (rpct::LinkSystem::LinkBoards::const_iterator linkBoard = (*halfBox)->getLinkBoards().begin(); linkBoard != (*halfBox)->getLinkBoards().end(); ++linkBoard) {
        if ((*linkBoard)->getDescription() == *it) {
          boards.push_back(dynamic_cast<rpct::LinkBoard*>((*linkBoard)));
        }   
      } 
    } // end loop over HBs
  }



  LOG4CPLUS_INFO(getLogger(), "Using " << boards.size() << " link boards");
  return boards;
}

rpct::LinkSystem::ControlBoards LBoxControlPanel::getSelectedControlBoards(std::string boardsStr) {
  std::stringstream ss(boardsStr);
  std::vector<std::string> boardsList;
  while(ss.good()) {
      std::string substr;
      getline(ss, substr, ',');
      boardsList.push_back(substr);
  }

  rpct::LinkSystem::ControlBoards boards;
  for (std::vector<std::string>::iterator it = boardsList.begin(); it != boardsList.end(); ++it) {
    for (std::vector<rpct::HalfBox*>::const_iterator halfBox = lboxAccess_->getLinkSystem()->getHalfBoxes().begin(); halfBox != lboxAccess_->getLinkSystem()->getHalfBoxes().end(); ++halfBox) {
      if ((*halfBox)->getCb().getDescription() == *it) {
        boards.push_back(dynamic_cast<rpct::ICB*>(&(*halfBox)->getCb()));
      }   
    } // end loop over HBs
  }



  LOG4CPLUS_INFO(getLogger(), "Using " << boards.size() << " control boards");
  return boards;
}

rpct::LinkSystem::RbcBoards LBoxControlPanel::getSelectedRbcBoards(std::string boardsStr) {
  std::stringstream ss(boardsStr);
  std::vector<std::string> boardsList;
  while(ss.good()) {
      std::string substr;
      getline(ss, substr, ',');
      boardsList.push_back(substr);
  }

  rpct::LinkSystem::RbcBoards boards;
  for (std::vector<std::string>::iterator it = boardsList.begin(); it != boardsList.end(); ++it) {
    for (std::vector<rpct::HalfBox*>::const_iterator halfBox = lboxAccess_->getLinkSystem()->getHalfBoxes().begin(); halfBox != lboxAccess_->getLinkSystem()->getHalfBoxes().end(); ++halfBox) {
      if ((*halfBox)->getRbc()) {
        if ((*halfBox)->getRbc()->getDescription() == *it) {
          boards.push_back(dynamic_cast<rpct::Rbc*>((*halfBox)->getRbc()));
        } 
      }  
    } // end loop over HBs
  }



  LOG4CPLUS_INFO(getLogger(), "Using " << boards.size() << " rbc boards");
  return boards;
}


void LBoxControlPanel::onSelectControlBoards(cgicc::Cgicc& cgi, std::ostream& out) {
  setSelectionStatus(controlBoards_, true);
  resultTable_->innerHtml(cgi,out);
}
void LBoxControlPanel::onSelectRbcBoards(cgicc::Cgicc& cgi, std::ostream& out) {
  setSelectionStatus(rbcBoards_, true);
  resultTable_->innerHtml(cgi,out);
}
void LBoxControlPanel::onSelectLinkBoards(cgicc::Cgicc& cgi, std::ostream& out) {
  setSelectionStatus(linkBoards_, true);
  resultTable_->innerHtml(cgi,out);
}
void LBoxControlPanel::onDeselectControlBoards(cgicc::Cgicc& cgi, std::ostream& out) {
  setSelectionStatus(controlBoards_, false);
  resultTable_->innerHtml(cgi,out);
}
void LBoxControlPanel::onDeselectRbcBoards(cgicc::Cgicc& cgi, std::ostream& out) {
  setSelectionStatus(rbcBoards_, false);
  resultTable_->innerHtml(cgi,out);
}
void LBoxControlPanel::onDeselectLinkBoards(cgicc::Cgicc& cgi, std::ostream& out) {
  setSelectionStatus(linkBoards_, false);
  resultTable_->innerHtml(cgi,out);
}

void LBoxControlPanel::onSelectAll(cgicc::Cgicc& cgi, std::ostream& out) {
  setSelectionStatus(rbcBoards_, true);
  setSelectionStatus(linkBoards_, true);
  setSelectionStatus(controlBoards_, true);
  
  resultTable_->innerHtml(cgi,out);
}

void LBoxControlPanel::onDeselectAll(cgicc::Cgicc& cgi, std::ostream& out) {
  setSelectionStatus(rbcBoards_, false);
  setSelectionStatus(linkBoards_, false);
  setSelectionStatus(controlBoards_, false);

  resultTable_->innerHtml(cgi,out);
}

void LBoxControlPanel::onChangeLbKey(cgicc::Cgicc& cgi, std::ostream& out) {
  std::string configKey = ajax::toolbox::getSubmittedValue(cgi, "lbConfigKeys");
  LOG4CPLUS_INFO(getLogger(), "Selected " + configKey);
  lbConfigKey_ = configKey;
}

void LBoxControlPanel::onChangeRbcKey(cgicc::Cgicc& cgi, std::ostream& out) {
  std::string configKey = ajax::toolbox::getSubmittedValue(cgi, "rbcConfigKeys");
  LOG4CPLUS_INFO(getLogger(), "Selected " + configKey);
  rbcConfigKey_ = configKey;
}

// end of LBoxControlPanel
// start of inner classes

LBoxControlPanel::BoardView::BoardView(rpct::IBoard* board, ajax::AutoHandledWidget* widget) :  board_(board) {


}

void LBoxControlPanel::BoardView::onCrateCheckBoxClick(cgicc::Cgicc& cgi, std::ostream& out) {
}

LBoxControlPanel::HalfBoxView::HalfBoxView(rpct::HalfBox* halfBox, ajax::AutoHandledWidget* widget, ajax::ResultBox* resultTable) 

{

}

LBoxControlPanel::HalfBoxView::~HalfBoxView() {
  if (controlBoard_) {
    delete controlBoard_;
  } 
  if (rbcBoard_) {
    delete rbcBoard_;
  }

  for (std::vector<BoardView*>::iterator it = linkBoards_.begin(); it != linkBoards_.end(); ++it) {
    delete *it;
  } 

}


void LBoxControlPanel::HalfBoxView::updateLists(std::vector<BoardView*>& linkBoards, 
  std::vector<BoardView*>& controlBoards, 
  std::vector<BoardView*>& rbcBoards) {

  for (std::vector<BoardView*>::iterator it = linkBoards_.begin(); it != linkBoards_.end(); ++it) {
    linkBoards.push_back(*it);
  } 
  if (controlBoard_) {
    controlBoards.push_back(controlBoard_);
  }
  if (rbcBoard_) {
    rbcBoards.push_back(rbcBoard_);
  }
}


void LBoxControlPanel::LBoxControlPanel::HalfBoxView::onSelect(cgicc::Cgicc& cgi, std::ostream& out) {
  setSelectionStatus(linkBoards_, true);
  if (controlBoard_) {
  }
  if (rbcBoard_) {
  }
  resultTable_->innerHtml(cgi,out);
}

void LBoxControlPanel::LBoxControlPanel::HalfBoxView::onDeselect(cgicc::Cgicc& cgi, std::ostream& out) {
  setSelectionStatus(linkBoards_, false);
  if (controlBoard_) {
  }
  if (rbcBoard_) {
  }
  resultTable_->innerHtml(cgi,out);
}


void LBoxControlPanel::onLinkBoxTest(cgicc::Cgicc& cgi, std::ostream& out) {
  std::string controlBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "controlBoards");
  std::string linkBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "linkBoards");
  std::string rbcBoardsStr = ajax::toolbox::getSubmittedValue(cgi, "rbcBoards");
  std::string programFlashStr = ajax::toolbox::getSubmittedValue(cgi, "programFlash");
  std::string testPulsesStr = ajax::toolbox::getSubmittedValue(cgi, "testPulses");

	LOG4CPLUS_INFO(getLogger(), "cbProgramFlash is checked: "<<programFlashStr
    <<" cbTestWithPulses is checked: "<< testPulsesStr);

	linkBoxTester_ = new LinkBoxTester(lboxAccess_);
	bool cont = linkBoxTester_->startTest((programFlashStr == "true"), (testPulsesStr == "true"), getSelectedControlBoards(controlBoardsStr), getSelectedLinkBoards(linkBoardsStr));

  bool activeLinkBoxText = true;
	if((testPulsesStr == "true") && cont) {

    activeLinkBoxText = false;
	}	else {
		linkBoxTester_->finalizeTest();
    activeLinkBoxText = true;
	}

}

void LBoxControlPanel::onLinkBoxTestContinue(cgicc::Cgicc& cgi, std::ostream& out) {
	bool testFinished = linkBoxTester_->continueTest();
  bool activeLinkBoxText = false;
	if(testFinished) {
		linkBoxTester_->finalizeTest();

    activeLinkBoxText = true;
		delete linkBoxTester_;
	}
}

}

