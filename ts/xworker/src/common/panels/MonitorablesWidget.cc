#include "rpct/ts/worker/panels/MonitorablesWidget.h"
#include "rpct/ts/worker/XdaqHardwareAccess.h"
#include "rpct/xdaqutils/XdaqCommonUtils.h"
#include "rpct/xdaqdiagaccess/XdaqDiagAccess.h"
#include "rpct/ii/Monitorable.h"
#include "xdata/TableIterator.h"
#include "ts/framework/Level1Source.h"
#include "ts/toolbox/SmtpMessenger.h"

#include <string>
#include <set>

using namespace std;
using namespace rpct::xdaqdiagaccess;

const char* rpct::MonitorablesWidget::MON_COL_NAME = "Monitorable";
const char* rpct::MonitorablesWidget::MON_COL_ITEMS = "Items";
const char* rpct::MonitorablesWidget::MON_COL_STATUS = "Status";
const char* rpct::MonitorablesWidget::STATUS_OK_STR = "OK";

const char* rpct::MonitorablesWidget::AL_COL_NAME = "Monitorables";
const char* rpct::MonitorablesWidget::AL_COL_SEVERITY = "Severity";
const char* rpct::MonitorablesWidget::AL_COL_COUNT = "AffectedDevicesCount";
const char* rpct::MonitorablesWidget::AL_COL_MESSAGE = "Message";

const char* rpct::MonitorablesWidget::CALL_TRIGGER = "Call RPC Trigger expert: 165877. ";
const char *rpct::MonitorablesWidget::CALL_RPC = "Call RPC DOC: 165508. ";
const char *rpct::MonitorablesWidget::RECONFIGURE = "Reconfiguration should be done a.s.a.p.";
const char *rpct::MonitorablesWidget::HARDRESET = "Hard Reset can help. ";
const char *rpct::MonitorablesWidget::INFORM = "Post an elog. ";

Logger rpct::MonitorablesWidget::logger_ = Logger::getInstance("MonitorablesWidget");

rpct::MonitorablesWidget::MonitorablesWidget(xdaq::Application* app, tsframework::Level1Source& level1Source)  :
    hardwareMonStatus_(0), mutex_(toolbox::BSem::FULL), monItemIndex_(0), app_(app), level1Source_(level1Source),
    system_(0), febsystem_(0) {
	logger_ = log4cplus::Logger::getInstance(app_->getApplicationLogger().getName() + ".MonitorablesWidget");
    setIsOwned(false);
    toolbox::net::URN flashlistIS = app->createQualifiedInfoSpace("rpct-worker-mon");
    flashlistISName_ = flashlistIS.toString();
    LOG4CPLUS_INFO(logger_, "Creating flashlist : " + flashlistISName_);
    xdata::InfoSpace * is = xdata::getInfoSpaceFactory()->get(flashlistISName_);
    monTest_ = 100;
    is->fireItemAvailable("partitionName", &partitionName_);
    is->fireItemAvailable("monTest", &monTest_);
    is->fireItemAvailable("monStatus", &hardwareMonStatus_);
    is->fireItemAvailable("monTable", &monitorItemsStatus_);

    is->fireItemAvailable("monAlarms", &alarmsTable_);
    is->addGroupRetrieveListener(this);

    timer_ = toolbox::task::getTimerFactory()->createTimer("PeriodicDiagnosticTimer."
                                                           + app_->getApplicationDescriptor()->getURN());
    timer_->stop();

    monitoringIteration_ = 0;
    severity_ = tsframework::Level1Source::INFO;
}

void rpct::MonitorablesWidget::html(cgicc::Cgicc& cgi, std::ostream& out) {
	string severityStr = "OK";

	{
	    rpct::MutexHandler handler(mutex_);
        if(severity_ == tsframework::Level1Source::WARNING) {
            severityStr = "WARNING";
        }
        else if(severity_ == tsframework::Level1Source::ERROR) {
            severityStr = "ERROR";
        }

	    out <<endl<<"Global Status: "<<severityStr<<"<br>"<<messages_<< "<br>";
	}

	LinkSystem* linkSystem = dynamic_cast<LinkSystem*> (system_);
    if(linkSystem != 0) {
        const time_t t = linkSystem->getFecManager().getResetFecAutoRecoveryStartTime();
        out <<endl<< "CCU errors:<br>"
            << " " << linkSystem->getFecManager().getResetFecAutoRecoveryGoodCnt() << " recovered / "
            << (linkSystem->getFecManager().getResetFecAutoRecoveryGoodCnt()+linkSystem->getFecManager().getResetFecAutoRecoveryBadCnt()) << " total<br>"
            << " since " << ctime(&t)
            << endl;
    }
    out << "<table cellpadding=\"2\" cellspacing=\"2\" border=\"1\"><thead>";
    out << "Monitorables: ";
    if (monLastTime_ > 0) {
        out << ctime(&monLastTime_);
    }
    out << "<br>Hardware Global Status: "<< MonitorableStatus::getStatusStr(hardwareMonStatus_);
    out << "</thead>\n<tr>";
    out << "<td>" << MON_COL_NAME << "</td>";
    out << "<td>" << MON_COL_ITEMS << "</td>";
    out << "<td>" << MON_COL_STATUS << "</td>";
    out << "</tr>";

    for (xdata::Table::iterator iRow = monitorItemsStatus_.begin(); iRow != monitorItemsStatus_.end(); ++iRow) {
        string s = iRow->getField(MON_COL_STATUS)->toString();
        string::size_type pos;
        string toFind = "|";
        while ((pos = s.find(toFind)) != string::npos) {
            s.replace(pos, toFind.size(), "<br>");
        }

        out << "\n<tr><td>" << iRow->getField(MON_COL_NAME)->toString() << "</td><td style=\"font-size: 50%;\">"
            << iRow->getField(MON_COL_ITEMS)->toString() << "</td><td>" << s << "</td></tr>";
    }
    out << "</table>";
}

void rpct::MonitorablesWidget::clearTables() {
    monitorItemsStatus_.clear();
    monitorItemsStatus_.addColumn(MON_COL_NAME, "string");
    monitorItemsStatus_.addColumn(MON_COL_ITEMS, "string");
    monitorItemsStatus_.addColumn(MON_COL_STATUS, "string");
}

void rpct::MonitorablesWidget::addTableRow(xdata::String& name, xdata::String& items, xdata::String& status) {
    xdata::Table::iterator iRow = monitorItemsStatus_.append();
    iRow->setField(MON_COL_NAME, name);
    iRow->setField(MON_COL_ITEMS, items);
    iRow->setField(MON_COL_STATUS, status);
}

void rpct::MonitorablesWidget::updateStatus(int row, xdata::String& status) {
    monitorItemsStatus_.setValueAt(row, MON_COL_STATUS, status);
    monLastTime_ = time(NULL);
}

void rpct::MonitorablesWidget::actionPerformed(xdata::Event& e) {
    cout << "**** actionPerformed " << e.type() << endl;

    if (e.type() == "urn:xdata-event:ItemGroupRetrieveEvent") {

        xdata::InfoSpace* is = xdata::getInfoSpaceFactory()->get(flashlistISName_);
        is->lock();
        monTest_ = monTest_ + 1;
        is->unlock();
    }
}

void rpct::MonitorablesWidget::setHardwareStatus(int status) {
    if (hardwareMonStatus_ != status) {
        hardwareMonStatus_ = status;
        std::list<std::string> names;
        names.push_back("monStatus");
        xdata::InfoSpace* is = xdata::getInfoSpaceFactory()->get(flashlistISName_);
        is->fireItemGroupChanged(names, this);
    }
}

bool rpct::MonitorablesWidget::isMonitoringOn() {
    return (timer_ != 0 && timer_->isActive());
}

void rpct::MonitorablesWidget::startMonitoring() {
    monitoringIteration_ = 0;
/*    if (!timer_->isActive()) {
        messagesWidget_->info("Starting monitoring");
        timer_->start();
        toolbox::TimeInterval interval(2);
        toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
        timer_->scheduleAtFixedRate(start, this, interval, 0, "MonitorablesChecker");
    }*/
}

/*it is not callednow*/
void rpct::MonitorablesWidget::stopMonitoring() {
/*    if (timer_->isActive()) {
        messagesWidget_->info("Stopping monitoring");
        try {
            timer_->remove("MonitorablesChecker");
        } catch (toolbox::task::exception::Exception& e) {
            LOG4CPLUS_WARN(logger_, string("Exception during stopping monitoring: ") + e.what());
        }
        timer_->stop();
    }*/
    //rpct::MutexHandler handler(mutex_);
    messages_.clear();
    severity_ = tsframework::Level1Source::INFO;
}

/*void rpct::MonitorablesWidget::monitor(MonitorablesInBoard& mib) {

}*/

void rpct::MonitorablesWidget::handleMonitorWarning(MonitorableStatus monStatus, std::string deviceName) {
	string key = monStatus.monitorItem + "_" + monStatus.getStatusStr();

	GlobalStatusMap::iterator itGS = globalStatusMap.find(key);

	if(itGS == globalStatusMap.end()) {
		monStatus.value = 1;
		monStatus.message = deviceName + ": " + monStatus.message;
		globalStatusMap.insert(GlobalStatusMap::value_type(key, monStatus));
	}
	else {
		itGS->second.value++;
		itGS->second.message = itGS->second.message + "; " + deviceName + ": " + monStatus.message;
	}
}

void rpct::MonitorablesWidget::handleMonitorException(std::exception& e, std::string where) {
	string str(e.what());
	if( (dynamic_cast<CCUException*>(&e)) != 0 || str.find("CCU") != string::npos || str.find("FEC") != string::npos) {
		MonitorableStatus monStatus(MonitorItemName::CCU_RING, MonitorableStatus::ERROR, e.what(), 1);
		handleMonitorWarning(monStatus, where);
		this->setHardwareStatus(MonitorableStatus::ERROR);
	}
	else if(str.find("VME") != string::npos) {
		MonitorableStatus monStatus(MonitorItemName::VME, MonitorableStatus::ERROR,e.what(), 1);
		handleMonitorWarning(monStatus, where);
		this->setHardwareStatus(MonitorableStatus::ERROR);
	}
    else if(str.find("db service") != string::npos) {
        MonitorableStatus monStatus(MonitorItemName::DB_SERVICE, MonitorableStatus::ERROR, e.what(), 1);
        handleMonitorWarning(monStatus, where);
        this->setHardwareStatus(MonitorableStatus::ERROR);
    }
	else {
		MonitorableStatus monStatus(MonitorItemName::EXCEPTION, MonitorableStatus::ERROR, e.what(), 1);
		handleMonitorWarning(monStatus, where);
	}
}

void rpct::MonitorablesWidget::handleRateWarnings(Monitorable::MonitorStatusList& monitorStatusList) {
	ostringstream ostr;
	for (Monitorable::MonitorStatusList::iterator iStat = monitorStatusList.begin();
			iStat != monitorStatusList.end(); ++iStat) {
		if (iStat->level > rateMonStatus_) {
			rateMonStatus_ = iStat->level;
		}
		//handleMonitorWarning(*(iStat));

		string key = iStat->monitorItem + "_" + iStat->getStatusStr();
		GlobalStatusMap::iterator itGS = globalStatusMap.find(key);

		ostr<<iStat->message<<", ";//<<": "<<iStat->value<<" Hz, ";
		if(itGS == globalStatusMap.end()) {
			itGS = globalStatusMap.insert(GlobalStatusMap::value_type(key, *(iStat))).first;
			itGS->second.value = 1;
			itGS->second.message = ostr.str();
		}
		else {
			itGS->second.value++;
		}
		if(itGS->second.value <= 3)
			itGS->second.message = ostr.str();
		else if(itGS->second.value == 4) {
			itGS->second.message += "... ";
		}
		//else - no more details
	}

	//this->analyseAndGenerateAlarms();
	LOG4CPLUS_DEBUG(logger_, __FUNCTION__<<":"<<__LINE__<<": "<<ostr.str());
}

void rpct::MonitorablesWidget::analyseAndGenerateAlarms() {
    alarmsTable_.clear();
    alarmsTable_.addColumn(AL_COL_NAME, "string");
    alarmsTable_.addColumn(AL_COL_SEVERITY, "int");
    alarmsTable_.addColumn(AL_COL_COUNT, "int");
    alarmsTable_.addColumn(AL_COL_MESSAGE, "string");

    string messages;
    std::set<std::string> foundErrorTypes;

    bool callTRigger =  false;
    bool callRpc =  false;
    bool reconfigure = false;
    bool executeHardReset = false;
    bool inform = false; //post an elog or automatic mail

    severity_ = tsframework::Level1Source::INFO; //should it be protected with mutex?
	for(GlobalStatusMap::iterator itGS = globalStatusMap.begin(); itGS != globalStatusMap.end(); itGS++) {
	    ostringstream message;
	    tsframework::Level1Source::SeverityLevel thisSeverity = tsframework::Level1Source::INFO;

        if(itGS->second.monitorItem == MonitorItemName::CCU_RING) {
            //if(itGS->second.value > 1)
            {
                thisSeverity = tsframework::Level1Source::ERROR;
                message<<"CCU error "<<" (check of the RPC LBOXes supply is needed), details: "<<itGS->second.message<<". ";
                foundErrorTypes.insert("CCU error");
                callTRigger = true;
            }
        }
        else if(itGS->second.monitorItem == MonitorItemName::VME) {
            //if(itGS->second.value > 1)
            {
                thisSeverity = tsframework::Level1Source::ERROR;
                message<<"VME error "<<" (check of the crates supply is needed, then the RPC TS processes should be restarted). ";
                foundErrorTypes.insert("VME error");
                callTRigger = true;
            }
        }
        else if(itGS->second.monitorItem == MonitorItemName::CONTROL_BUS) {
        	message<<"SEU-like problem detected on "<<itGS->second.value<<" device(s), details: "<<itGS->second.message<<". ";
        	if(itGS->second.value <= 9) {//one half box
        		thisSeverity = tsframework::Level1Source::WARNING;
        		inform =  true;
        	}
        	else {
        		thisSeverity = tsframework::Level1Source::ERROR;
        		foundErrorTypes.insert("Control bus error");
        		callTRigger =  true;
        	}
        }
        else if(itGS->second.monitorItem == MonitorItemName::FIRMWARE) {
	        message<<"Firmware lost in "<<itGS->second.value<<" device(s), details: "<<itGS->second.message<<". ";
	        if(itGS->second.value == 1) {
	            thisSeverity = tsframework::Level1Source::WARNING;
	            inform =  true;
	        }
	        else {
	            thisSeverity = tsframework::Level1Source::ERROR;
	            foundErrorTypes.insert("Firmware lost error");
	            callTRigger =  true;
	        }
	    }
	    else if(itGS->second.monitorItem == MonitorItemName::QPLL && itGS->second.level == MonitorableStatus::WARNING) {
	        message<<"Transient QPLL unlock detected on "<<itGS->second.value<<" device(s). The CMS clock was probably unstable (Was the clock source changed? If yes, reconfigure a.s.a.p). ";
	        if(itGS->second.value == 1) {
	            thisSeverity = tsframework::Level1Source::WARNING;
	            inform =  true;
	        }
	        else {
	            thisSeverity = tsframework::Level1Source::WARNING;
	            //reconfigure = true;
	            inform =  true;
	        }
	    }
	    else if(itGS->second.monitorItem == MonitorItemName::QPLL && itGS->second.level == MonitorableStatus::ERROR) {
	        message<<"Persistent QPLL unlock detected on "<<itGS->second.value<<" device(s). The CMS clock was probably unstable (was the clock source changed?). ";
	        if(itGS->second.value == 1) {
	            thisSeverity = tsframework::Level1Source::WARNING;
	            inform =  true;
	        }
	        else {
	            thisSeverity = tsframework::Level1Source::ERROR;
	            foundErrorTypes.insert("QPLL error");
	            reconfigure = true;
	        }
	    }
	    else if(itGS->second.monitorItem == MonitorItemName::TTCRX && itGS->second.level == MonitorableStatus::ERROR) {
	        message<<"TTCrx problem on "<<itGS->second.value<<" device(s). ";
	        if(itGS->second.value == 1) {
	            thisSeverity = tsframework::Level1Source::WARNING;
	            inform =  true;
	        }
	        else {
	            thisSeverity = tsframework::Level1Source::ERROR;
	            foundErrorTypes.insert("TTCrx error");
	            callTRigger =  true;
	        }
	    }
	    else if(itGS->second.monitorItem == MonitorItemName::TTCRX && itGS->second.level == MonitorableStatus::SOFT_WARNING) {
	    	//TODO - change the SOFT_WARNING into error when the CB TTCrx problem is solved
	    	thisSeverity = tsframework::Level1Source::INFO;
	    	ostringstream msg;
	    	msg<<"TTCrx not ready detected on "<<itGS->second.value<<" CBs.";
	    	//messagesWidget_->warn(msg.str());
	    	LOG4CPLUS_WARN(logger_, msg.str());
	    }
	    else if(itGS->second.monitorItem == MonitorItemName::GOL && itGS->second.level == MonitorableStatus::ERROR) {
	        message<<"GOL is not ready on "<<itGS->second.value<<" device(s). ";
	        if(itGS->second.value == 1) {
	            thisSeverity = tsframework::Level1Source::WARNING;
	            inform =  true;
	        }
	        else {
	            thisSeverity = tsframework::Level1Source::ERROR;
	            foundErrorTypes.insert("GOL error");
	            callTRigger =  true;
	        }
	    }
	    else if(itGS->second.monitorItem == MonitorItemName::RECEIVER && itGS->second.level == MonitorableStatus::ERROR) {
	    	if(itGS->second.value > 5) {
	    		thisSeverity = tsframework::Level1Source::ERROR;
	    		message<<"Transmission errors detected on "<<itGS->second.value<<" device(s). ";
	            foundErrorTypes.insert("Transmission errors");
	    		callTRigger =  true;
	    	}
	    }
	    else if(itGS->second.monitorItem == MonitorItemName::RECEIVER && itGS->second.level == MonitorableStatus::WARNING) {
	        if(itGS->second.value > 20) {
	            thisSeverity = tsframework::Level1Source::WARNING;
	            message<<"Transmission errors detected on "<<itGS->second.value<<" devices. ";
	            callTRigger =  true;
	        }
	    }
	    else if(itGS->second.monitorItem == MonitorItemName::OPT_LINK && itGS->second.level == MonitorableStatus::ERROR) {
	    	if(itGS->second.value > 1) {
	    		//thisSeverity = tsframework::Level1Source::ERROR; TODO uncomment when fixed
	    		//message<<"Optical link errors detected on "<<itGS->second.value<<" links. ";
	    		//reconfigure = true;
	    		//executeHardReset = true;

		    	ostringstream msg;
		    	msg<<"Optical link errors detected on "<<itGS->second.value<<" links. ";
		    	LOG4CPLUS_WARN(XdaqHardwareAccess::getSummaryLogger(), msg.str());
	    	}
	    }
	    else if(itGS->second.monitorItem == MonitorItemName::OPT_LINK && itGS->second.level == MonitorableStatus::WARNING) {
	        if(itGS->second.value > 10) {
	            //thisSeverity = tsframework::Level1Source::ERROR;
	            //message<<"Optical link errors detected on "<<itGS->second.value<<" devices. ";
	            //reconfigure = true;
	            //executeHardReset = true;

		    	ostringstream msg;
		    	msg<<"Optical link errors detected on "<<itGS->second.value<<" links. ";
		    	LOG4CPLUS_WARN(XdaqHardwareAccess::getSummaryLogger(), msg.str());
	        }
	    }
	    else if(itGS->second.monitorItem == MonitorItemName::CONFIG_LOST) {
	        message<<"Configuration lost detected on "<<itGS->second.value<<" device(s), details: "<<itGS->second.message<<". ";
	        if(itGS->second.value == 1) {
	            thisSeverity = tsframework::Level1Source::WARNING;
	            inform =  true;
	        }
	        else {
	            thisSeverity = tsframework::Level1Source::ERROR;
	            foundErrorTypes.insert("Configuration lost error");
	            callTRigger =  true;
	        }
	    }
	    else if(itGS->second.monitorItem == MonitorItemName::RMB_DATA && itGS->second.level) {
	        if(itGS->second.value > 1) {
	            thisSeverity = tsframework::Level1Source::WARNING;
	            message<<"RMB input data errors in "<<itGS->second.value<<" devices. ";
	            inform = true;
	        }
	    }
	    else if(itGS->second.monitorItem == MonitorItemName::RMB_ALGORITHM && itGS->second.level) {
	        if(itGS->second.value > 1) {
	            thisSeverity = tsframework::Level1Source::WARNING;
	            message<<"Buffers of the RPC DAQ overflow in "<<itGS->second.value<<" RMB devices. Check the L1A rate (global, not the RPC trigger)!";
	            inform = true;
	        }
	    }
	    else if(itGS->second.monitorItem == MonitorItemName::RBC && itGS->second.level == MonitorableStatus::ERROR) {
	    	if(itGS->second.value > 1) {
	    		thisSeverity = tsframework::Level1Source::WARNING;
	    		message<<"RBC errors in "<<itGS->second.value<<" devices. ";
	    		inform = true;
	    	}
	    }
	    else if(itGS->second.monitorItem == MonitorItemName::RATE && itGS->second.level == MonitorableStatus::WARNING) {
	    	thisSeverity = tsframework::Level1Source::WARNING;
	    	message<<"Rate warning on "<<itGS->second.value<<" devices: "<<itGS->second.message<<". ";
	    	LOG4CPLUS_DEBUG(logger_, __FUNCTION__<<":"<<__LINE__<<": "<<message.str());
	    	//inform = true;
	    }
	    else if(itGS->second.monitorItem == MonitorItemName::WIN_FULL_RATIO && itGS->second.level == MonitorableStatus::SOFT_WARNING) {
	    	thisSeverity = tsframework::Level1Source::INFO;
	    	ostringstream msg;
	    	msg<<"WIN_FULL_RATIO warning on "<<itGS->second.value<<" device(s): "<<itGS->second.message<<". ";
	    	LOG4CPLUS_WARN(XdaqHardwareAccess::getSummaryLogger(), msg.str());
	    }
	    else if(itGS->second.monitorItem == MonitorItemName::DB_SERVICE) {
	        thisSeverity = tsframework::Level1Source::ERROR;
	        message<<"Database service error: "<<itGS->second.message<<" (restart of the DB tomcat and then RPC TS processes is needed). ";
            foundErrorTypes.insert("DB service error");
	        callTRigger = true;
	    }
	    else if(itGS->second.monitorItem == MonitorItemName::EXCEPTION) {
	        thisSeverity = tsframework::Level1Source::ERROR;
	        message<<"Software error: "<<itGS->second.message<<". ";
            foundErrorTypes.insert("Software error");
	        //callTRigger = true;
	        inform = true;
	    }
	    else if(itGS->second.monitorItem == MonitorItemName::RELOAD_DETECTED) {
	    	thisSeverity = tsframework::Level1Source::INFO;
	    	ostringstream msg;
	    	msg<<"Firmware reload detected on "<<itGS->second.value<<" CBs. ";
	    	LOG4CPLUS_WARN(XdaqHardwareAccess::getSummaryLogger(), msg.str());
	    }
	    else if(itGS->second.monitorItem == MonitorItemName::MESSAGE) {
	    	thisSeverity = tsframework::Level1Source::WARNING;
	    	message<<itGS->second.message<<". ";
	    	//inform = true;
	    }
	    else if(itGS->second.level == MonitorableStatus::SOFT_WARNING) {
	    	if(itGS->second.value > 2) {
	    		thisSeverity = tsframework::Level1Source::INFO;
	    		ostringstream msg;
	    		msg<<"SOFT_WARNING on "<<itGS->second.value<<" devices: "<<itGS->second.message<<". ";
	    		LOG4CPLUS_WARN(XdaqHardwareAccess::getSummaryLogger(), msg.str());
	    	}
	    }
	    else {
	    	thisSeverity = tsframework::Level1Source::WARNING;
	    	message<<"Unknown type of monitoring error: "<<itGS->second.message<<". ";
	    	inform = true;
	    }

	    if(severity_ < thisSeverity)
	    	severity_ = thisSeverity;

	    messages += message.str();

	    try {
	    	xdata::Table::iterator iRow = alarmsTable_.append();
	    	xdata::String monitorItem = xdata::String(itGS->second.monitorItem);
	    	iRow->setField(AL_COL_NAME, monitorItem);
	    	xdata::Integer sev = xdata::Integer(severity_);
	    	iRow->setField(AL_COL_SEVERITY, sev);
	    	xdata::Integer value = xdata::Integer(itGS->second.value);
	    	iRow->setField(AL_COL_COUNT, value);
	    	xdata::String messageStr = xdata::String(message.str());
	    	iRow->setField(AL_COL_MESSAGE, messageStr);
	    }
	    catch (xcept::Exception& e) {
	    	LOG4CPLUS_ERROR(logger_, e.what());
	    }
	}

    if(reconfigure && !executeHardReset && !callRpc && !callTRigger)
    	messages += RECONFIGURE;
    if(!reconfigure && executeHardReset && !callRpc && !callTRigger)
    	messages += HARDRESET;
	if(callRpc)
		messages += CALL_RPC;
	if(callTRigger)
		messages += CALL_TRIGGER;
	if(inform && !callRpc && !callTRigger)
	    messages += INFORM;

    rpct::MutexHandler handler(mutex_); //to protect the messages_
	if(severity_ == tsframework::Level1Source::WARNING) {
		if(messages != messages_) {
			LOG4CPLUS_WARN(XdaqHardwareAccess::getSummaryLogger(), messages);
		}
		LOG4CPLUS_WARN(logger_, messages);
	}
	if(severity_ == tsframework::Level1Source::ERROR) {
		if(messages != messages_) {
			LOG4CPLUS_ERROR(XdaqHardwareAccess::getSummaryLogger(), messages);
		}
		LOG4CPLUS_ERROR(logger_, messages);

		string foundErrors;
		for(std::set<string>::iterator it = foundErrorTypes.begin(); it!= foundErrorTypes.end(); it++) {
			if(foundErrors.size() == 0)
				foundErrors = *it;
			else
				foundErrors += (", " +  *it);
		}
		if(foundErrors != foundErrors_) {
			std::string recipients = "cms-rpc-l1-trigger-notifications@cern.ch";
			xdata::Serializable* appName = app_->getApplicationInfoSpace()->find("name");
			std::string subject = foundErrors + " on " + appName->toString();
			tstoolbox::SmtpMessenger m(recipients, subject, messages);
			m.send();
		}
		foundErrors_ = foundErrors;
	}

	messages_ = messages;

	globalStatusMap.clear();
}

void rpct::MonitorablesWidget::updatel1Source() {
    rpct::MutexHandler handler(mutex_);
    if(severity_ >= tsframework::Level1Source::WARNING)
        level1Source_.setAlarm(partitionName_, severity_, partitionName_.value_ + " " + messages_);
}

//monitors everything at once
void rpct::MonitorablesWidget::monitorHardware(volatile bool *stop) {
	rpct::MutexHandler handler(hardwareMutex_);
	LOG4CPLUS_INFO(logger_, __FUNCTION__<<": started, reading the monitorables");
	System::CrateList& sysc = system_->getCrates();
	IMonitorable* monitorable;
	for (System::CrateList::iterator iCrate = sysc.begin(); iCrate != sysc.end(); ++iCrate) {
		ICrate* crate = *iCrate;
		monitorable = dynamic_cast<IMonitorable*> (crate);
		if (monitorable != 0) {
			try {
				//rpct::MutexHandler handler(hardwareMutex_);
				monitorable->monitor(stop);
			}
			catch (std::exception& e) {
				xdata::String str(e.what()) ;
				//this->updateStatus(iMib, str); not possible to assign the exception to a board

				handleMonitorException(e, "Exception during hardware monitoring:");

				LOG4CPLUS_ERROR(logger_, "Exception during hardware monitoring: "<< e.what() <<" breaking the monitoring loop");
				ALARM_STD(app_,"ERROR",e);

				break;//in prctice it can be only unrecovered CCU error, so here we may break
			}
		}
	}
	monitoringIteration_++;
	LOG4CPLUS_INFO(logger_, __FUNCTION__<<": done");;
}

void rpct::MonitorablesWidget::collectMonitorableStaus() {
	//////////////////////////////////////////////////////////////
	//collecting the monitorable status and updating the GUI table
	//////////////////////////////////////////////////////////////
	LOG4CPLUS_INFO(logger_, __FUNCTION__<<": collecting the monitorable status");
	int monStatus = 0;
	for(unsigned int iMib = 0; iMib < monitorablesPerBoard_.size(); iMib++) {
		MonitorablesInBoard& mib = monitorablesPerBoard_[iMib];
		ostringstream ostr;
		//loop over monitiroables in one board
		for (Monitorables::iterator iMon = mib.monitorables.begin(); iMon != mib.monitorables.end(); ++iMon) {
			Monitorable* mon = *iMon;

			for (Monitorable::MonitorStatusList::iterator iStat = mon->getWarningStatusList().begin();
					iStat != mon->getWarningStatusList().end(); ++iStat) {
				ostr << mon->getDescription() << "(" << iStat->monitorItem << "): " << iStat->message << " | ";
				if (iStat->level > monStatus) {
					monStatus = iStat->level;
				}
				if (monStatus > hardwareMonStatus_) { //we want to update the monStatus_ immediately
					this->setHardwareStatus(monStatus);
				}

				if( dynamic_cast<LinkBoard*>(mib.board) ) {
					handleMonitorWarning(*(iStat), mib.board->getDescription() +
							" chamber " + (dynamic_cast<LinkBoard*>(mib.board))->getChamberName());
				}
				else
					handleMonitorWarning(*(iStat), mib.board->getDescription());
			}
		}

		xdata::String status = ostr.str();
		if (status.toString().empty()) {
			status = STATUS_OK_STR;
		}
		else {
			LOG4CPLUS_WARN(logger_, mib.board->getDescription()<<" Monitoring message:" << ostr.str());
		}
		this->updateStatus(iMib, status);
	}
	this->setHardwareStatus(monStatus);
}

//monitors everything at once
/*void rpct::MonitorablesWidget::monitorHardware(volatile bool *stop) {
	LOG4CPLUS_INFO(logger_, __FUNCTION__<<": started");
	int monStatus = 0;
	globalStatusMap.clear();
	for(unsigned int iMib = 0; iMib < monitorablesPerBoard_.size(); iMib++) {
		MonitorablesInBoard& mib = monitorablesPerBoard_[iMib];
	    ostringstream ostr;
	    //loop over monitiroables in one board
	    for (Monitorables::iterator iMon = mib.monitorables.begin(); iMon != mib.monitorables.end(); ++iMon) {
	    	if(*stop) {
	    		LOG4CPLUS_WARN(logger_, "MonitorablesWidget::monitor() stopped before all boards monitored");
	    		break;
	    	}

	        Monitorable* mon = *iMon;
	        try {
	            rpct::MutexHandler handler(hardwareMutex_);
	            mon->monitor();
	            for (Monitorable::MonitorStatusList::iterator iStat = mon->getWarningStatusList().begin();
	                 iStat != mon->getWarningStatusList().end(); ++iStat) {
	                ostr << mon->getDescription() << "(" << iStat->monitorItem << "): " << iStat->message << " | ";
	                if (iStat->level > monStatus) {
	                    monStatus = iStat->level;
	                }
	                if (monStatus > hardwareMonStatus_) { //we want to update the monStatus_ immediately
	                	this->setHardwareStatus(monStatus);
	                }
	                handleMonitorWarning(*(iStat));
	            }
	        }
	        catch (std::exception& e) {
	        	xdata::String str(e.what()) ;
	        	this->updateStatus(iMib, str);

	        	handleMonitorException(e, "Exception during hardware monitoring:");

	            LOG4CPLUS_ERROR(logger_, e.what());
	            ALARM_STD(app_,"ERROR",e);

	            break;//TODO decide what to do
	        }
	    }

	    xdata::String status = ostr.str();
	    if (status.toString().empty()) {
	        status = STATUS_OK_STR;
	    }
	    else {
	        LOG4CPLUS_ERROR(logger_, mib.board->getDescription()<<" Monitoring message:" << ostr.str());
	    }
	    this->updateStatus(iMib, status);
	}
	this->setHardwareStatus(monStatus);

	//this->analyseAndGenerateAlarms();

	//--------------------
	try {
		if (febsystem_) //&& monitoringIteration_%2 == 0
			febsystem_->xmonitorcycle();
	} catch (std::exception& e) {
		handleMonitorException(e, "Exception during FEBs monitoring:");

        LOG4CPLUS_WARN(logger_, e.what());
        ALARM_STD(app_,"ERROR",e);
	} catch (...) {
	}

	monitoringIteration_++;
	LOG4CPLUS_INFO(logger_, __FUNCTION__<<": done");;
}*/

/*void rpct::MonitorablesWidget::timeExpired(toolbox::task::TimerEvent& e) {

    MonitorablesInBoard& mib = monitorablesPerBoard_.at(monItemIndex_);
    ostringstream ostr;

    int itemMonStatus = 0;
    //loop over monitiroables in one board
    for (Monitorables::iterator iMon = mib.monitorables.begin(); iMon != mib.monitorables.end(); ++iMon) {
        Monitorable* mon = *iMon;
        try {
            rpct::MutexHandler handler(hardwareMutex_);
            mon->monitor();
            for (Monitorable::MonitorStatusList::iterator iStat = mon->getWarningStatusList().begin();
                 iStat != mon->getWarningStatusList().end(); ++iStat) {
                ostr << mon->getDescription() << "(" << iStat->monitorItem << "): " << iStat->message << " | ";
                if (iStat->level > itemMonStatus) {
                    itemMonStatus = iStat->level;
                }
            }
        } catch (std::exception& e) { //TODO - maybe it should stop if the error persists
            ostr << e.what() << " | ";
            LOG4CPLUS_WARN(logger_, e.what());
            ALARM_STD(app_,"ERROR",e);
        }
    }
    try {
        if (febsystem_)
            febsystem_->xmonitorcycle();
    } catch (rpct::exception::CCUException & e) {
    } catch (...) {
    }

    if (itemMonStatus > hardwareMonStatus_) {
        this->setHardwareStatus(itemMonStatus);
    }
    else if (itemMonStatus < hardwareMonStatus_) {
        // mon status of this item is less critical that current monStatus_.
        // As we do not know if there are any items with more critical status
        // we must calculate monStatus basing on all items
        int newMonStatus = 0;
        for (std::vector<MonitorablesInBoard>::iterator iMPB = monitorablesPerBoard_.begin(); iMPB != monitorablesPerBoard_.end(); ++iMPB) {
            Monitorables& mons = iMPB->monitorables;
            for (Monitorables::iterator iMon = mons.begin(); iMon != mons.end(); ++iMon) {
                Monitorable* mon = *iMon;
                for (Monitorable::MonitorStatusList::iterator iStat = mon->getWarningStatusList().begin(); iStat != mon->getWarningStatusList().end(); ++iStat) {
                    if (iStat->level> newMonStatus) {
                        newMonStatus = iStat->level;
                    }
                }
            }
        }
        this->setHardwareStatus(newMonStatus);
    }

    xdata::String status = ostr.str();
    if (status.toString().empty()) {
        status = STATUS_OK_STR;
    }
    else {
        LOG4CPLUS_WARN(logger_, mib.board->getDescription()<<" Monitoring message:" << ostr.str());
    }
    this->updateStatus(monItemIndex_,status);
    monItemIndex_ = (monItemIndex_ + 1) % monitorablesPerBoard_.size();
}*/




void rpct::MonitorablesWidget::initialise(rpct::System* system, toolbox::BSem* hardwareMutex, bool lboxSort, rpct::xdaqutils::xdaqRPCFebSystem * febsystem) {

    LOG4CPLUS_INFO(logger_, "Start");

    hardwareMutex_ = hardwareMutex;
    system_ =  system;
    febsystem_ = febsystem;
    monitorablesPerBoard_.clear();

    Monitorable* monitorable;
    System::CrateList& sysc = system->getCrates();
    for (System::CrateList::iterator iCrate = sysc.begin(); iCrate != sysc.end(); ++iCrate) {
        ICrate* crate = *iCrate;
        monitorable = dynamic_cast<Monitorable*> (crate);
        if (monitorable != 0) {
            //LOG4CPLUS_WARN(logger_, "Skipping not monitorable crate " << crate->getDescription());
        	//we are dping nothing with the crate
        }

        typedef ICrate::Boards Boards;
        Boards& boards = crate->getBoards();

        if (lboxSort) {//TODO moove to system or system builder
            // order boards according to: ascending CCU address, ascending LB position, CB before LB
            std::sort(boards.begin(), boards.end(), LinkBox::boardSortPredicate );
        }

        for (Boards::iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {
            typedef IBoard::Devices Devices;
            IBoard* board = (*iBoard);
            monitorable = dynamic_cast<Monitorable*> (board);
            if (monitorable != 0) {
                MonitorablesInBoard mib;
                mib.board = board;
                mib.monitorables.push_back(monitorable);
                monitorablesPerBoard_.push_back(mib);
            }
            const Devices& devices = board->getDevices();
            for (Devices::const_iterator iDevice = devices.begin(); iDevice != devices.end(); ++iDevice) {
                monitorable = dynamic_cast<Monitorable*> (*iDevice);
                if (monitorable != 0) {
                    if (monitorablesPerBoard_.empty() || monitorablesPerBoard_.back().board->getId()
                        != board->getId()) {
                        MonitorablesInBoard mib;
                        mib.board = board;
                        mib.monitorables.push_back(monitorable);
                        monitorablesPerBoard_.push_back(mib);
                    } else {
                        monitorablesPerBoard_.back().monitorables.push_back(monitorable);
                    }
                }
            }
        }
    }
    this->clearTables();
    for (size_t i = 0; i < monitorablesPerBoard_.size(); i++) {
        MonitorablesInBoard& mib = monitorablesPerBoard_[i];

        xdata::String name = mib.board->getDescription();
        ostringstream ostr;
        for (Monitorables::iterator iMon = mib.monitorables.begin(); iMon != mib.monitorables.end(); ++iMon) {
            Monitorable* mon = *iMon;
            ostr << mon->getDescription() << '(';
            for (Monitorable::MonitorItems::const_iterator iMonItem = mon->getMonitorItems().begin(); iMonItem
                     != mon->getMonitorItems().end(); ++iMonItem) {
                ostr << (*iMonItem) << ' ';
            }
            ostr << ") ";
        }
        xdata::String items = ostr.str();
        xdata::String status;
        this->addTableRow(name,items,status);
    }
}
