#include "rpct/ts/worker/panels/GOLPanel.h"
#include "rpct/ts/worker/XdaqTCrateAccess.h"
#include "rpct/ts/worker/TBCellContext.h"
#include "rpct/xdaqutils/LogUtils.h"
#include "rpct/xdaqutils/XdaqCommonUtils.h"



#include "ts/exception/CellException.h"
#include "ts/framework/CellAbstract.h"
#include "xdata/TableIterator.h"

#include "ajax/PlainHtml.h"
#include "ajax/Form.h"
#include "ajax/SubmitButton.h"
#include "ajax/ComboBox.h"
#include "ajax/CheckBox.h"
#include "ajax/LayoutContainer.h"
#include "ajax/LayoutElement.h"
#include "ajax/toolbox.h"
#include "ajax/Image.h"

#include <iostream>

using namespace std;
using namespace tsframework;
using namespace tsexception;
using namespace rpct::xdaqutils;
using namespace rpcttsworker;
using rpct::Rmb;

namespace rpcttspanels {

GOLPanel::GOLPanel(CellAbstractContext* context, log4cplus::Logger& logger) :
  CellPanel(context, logger),  
  tCrateAccess_(dynamic_cast<rpcttsworker::TBCellContext*>(context)->getTCrateAccess())
    {
    logger_ =  log4cplus::Logger::getInstance(getLogger().getName() + ".GOLPanel");
    
    LOG4CPLUS_INFO(getLogger(), "GOLPanel constructor");



    resultTable_ = new ajax::ResultBox();
    resultTable_->setId(getId() + "resultTable_");
    resultTable_->setIsOwned(false);

    const rpct::System::HardwareItemList& rmbList = tCrateAccess_->getSystem()->getHardwareItemsByType(Rmb::TYPE);


    for (rpct::System::HardwareItemList::const_iterator iRmb = rmbList.begin(); iRmb != rmbList.end(); ++iRmb) {
      Rmb* r = dynamic_cast<Rmb*> (*iRmb);
      gols_.push_back(new GOLView(this,r));
    }
    initLayout(dynamic_cast<rpcttsworker::RpctCellContext*>(context));
}

GOLPanel::~GOLPanel() {
  LOG4CPLUS_INFO(getLogger(), "GOLPanel destructor");
  delete resultTable_;
  for (std::vector<GOLView*>::iterator it = gols_.begin(); it != gols_.end(); ++it) {
    delete *it;
  }
}



void GOLPanel::initLayout(rpcttsworker::RpctCellContext* rpctContext){
  remove();

  ajax::PlainHtml* title = new ajax::PlainHtml();
  title->getStream()
			<< "<h2 align=\"center\" style=\"font-family:arial; color:grey;\" >GOL Read/Write</h2>";
  add(title);

  add(resultTable_);
  resultTable_->remove();
  resultTable_->add(rpctContext->getLoggerwidget());

  
  ajax::Table* table = new ajax::Table();
  table->setShowRules(true);
  table->setShowHeader(true);
  table->addColumn("GOL", ajax::Table::Html);
  table->addColumn("Value", ajax::Table::Html);
  table->addColumn("Read", ajax::Table::Html);
  table->addColumn("Write", ajax::Table::Html);
    
  int row = 0;

  for (std::vector<GOLView*>::iterator it = gols_.begin(); it != gols_.end(); ++it) {
    /*setEvent(&((*it)->cbUse_), ajax::Eventable::OnClick, *it,
                &GOLPanel::BoardView::onCrateCheckBoxClick);
    */
    table->setWidgetAt("GOL", row, &((*it)->name_));
    table->setWidgetAt("Value", row, &((*it)->inputText_));
    setEvent(&((*it)->inputText_), ajax::Eventable::OnChange,resultTable_,*it, &GOLPanel::GOLView::onChangeValue);
    table->setWidgetAt("Read", row, &((*it)->readButton_));
    setEvent(&((*it)->readButton_), ajax::Eventable::OnClick, resultTable_, *it, &GOLPanel::GOLView::onReadGOL);
    table->setWidgetAt("Write", row, &((*it)->writeButton_));
    setEvent(&((*it)->writeButton_), ajax::Eventable::OnClick, resultTable_, *it, &GOLPanel::GOLView::onWriteGOL);
    row++;
    LOG4CPLUS_INFO(getLogger(), row);
  }

  resultTable_->add(table);
  LOG4CPLUS_INFO(getLogger(), "layout table created");


  ajax::LayoutContainer* lcMain = new ajax::LayoutContainer();
  add(lcMain);


  ajax::LayoutElement* leRefreshB = new ajax::LayoutElement();
  lcMain->add(leRefreshB);
  leRefreshB->setPosition(ajax::LayoutElement::Top);
  
  ajax::Button* refreshBtn = new ajax::Button();
  leRefreshB->add(refreshBtn);
  refreshBtn->setId(getId() + "refreshBtn_");
  refreshBtn->setImage("/ts/ajaxell/extern/icons/html/icons/arrow_refresh.png");
  setEvent(refreshBtn, ajax::Eventable::OnClick, resultTable_, this, &GOLPanel::refresh);
}

void GOLPanel::layout(cgicc::Cgicc& cgi) {

    LOG4CPLUS_INFO(getLogger(), "layout");
}

void GOLPanel::refresh(cgicc::Cgicc& cgi, std::ostream& out) {
    LOG4CPLUS_INFO(getLogger(), "Refresh");
    resultTable_->innerHtml(cgi, out);
}


GOLPanel::GOLView::GOLView(GOLPanel* panel, rpct::Rmb* rmb) : panel_(panel), rmb_(rmb) {       
  name_.setIsOwned(false);
  name_.setText(rmb->getDescription());
  readButton_.setIsOwned(false);
  readButton_.setCaption("Read");
  writeButton_.setIsOwned(false);
  writeButton_.setCaption("Write");
  inputText_.setIsOwned(false);
}

void GOLPanel::GOLView::onChangeValue(cgicc::Cgicc& cgi, std::ostream& out) {

  value_ =   ajax::toolbox::getSubmittedValue(cgi, inputText_.getId() );
  LOG4CPLUS_INFO(panel_->getLogger(), "value set to " + value_);
  inputText_.setDefaultValue(value_);
  panel_->refresh(cgi,out);
}


void GOLPanel::GOLView::onReadGOL(cgicc::Cgicc& cgi, std::ostream& out) {
  {
    rpct::MutexHandler handler(panel_->tCrateAccess_->getHardwareMutex());
    
    bool useConfReg = true;

    unsigned char ld_current;
    LOG4CPLUS_INFO(panel_->getLogger(), "rmb read");
    try{
      try {
	rmb_->getGolI2c().ReadConfig3(ld_current, useConfReg);
      } catch (rpct::EI2C& e) {
	LOG4CPLUS_WARN(panel_->getLogger(), "rmb read: " << e.what() << ". Trying once again.");
	//msgs() << "rmb read: " << e.what() << ". Trying one again." << endl;
	rmb_->getGolI2c().ReadConfig3(ld_current, useConfReg);
      }
      LOG4CPLUS_INFO(XdaqTCrateAccess::getSummaryLogger(), "rmb read done");
      ostringstream ostr;
      ostr << ((unsigned int) ld_current);
      value_ = ostr.str();
      inputText_.setDefaultValue(ostr.str());
    } catch (...) {
      LOG4CPLUS_ERROR(XdaqTCrateAccess::getSummaryLogger(), "Read failed.");
    }
  }
  panel_->refresh(cgi,out);
}

void GOLPanel::GOLView::onWriteGOL(cgicc::Cgicc& cgi, std::ostream& out) {
  
  bool useConfReg = true;
  
  istringstream istr(value_);
  unsigned int v = 0;
  istr >> v;
    
    
  LOG4CPLUS_INFO(XdaqTCrateAccess::getSummaryLogger(), "rmb write :" + value_);
  {
    rpct::MutexHandler handler(panel_->tCrateAccess_->getHardwareMutex());  
    try{
      try {
	rmb_->getGolI2c().WriteConfig3(v, useConfReg);
      } catch (rpct::EI2C& e) {
	LOG4CPLUS_WARN(panel_->getLogger(), "rmb write: " << e.what() << ". Trying once again.");
	//msgs() << "rmb read: " << e.what() << ". Trying one again." << endl;
	rmb_->getGolI2c().WriteConfig3(v, useConfReg);
      }
    } catch (xcept::Exception& e) {
      LOG4CPLUS_ERROR(XdaqTCrateAccess::getSummaryLogger(), e.what());

    } catch (std::exception& e) {
      LOG4CPLUS_ERROR(XdaqTCrateAccess::getSummaryLogger(), e.what());

    } catch (...) {
      LOG4CPLUS_ERROR(XdaqTCrateAccess::getSummaryLogger(), "Write failed (wrong parameter?).");
    }
  }
  panel_->refresh(cgi,out);
}


}


