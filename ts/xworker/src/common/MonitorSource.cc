
#include "rpct/ts/worker/MonitorSource.h"

#include "log4cplus/logger.h"

#include "xdata/xdata.h"
#include "xdata/Table.h"
#include "xdata/TableIterator.h"

#include <vector>
#include <map>
#include <string>

rpcttsworker::MonitorSource::MonitorSource(const std::string& infospaceName, tsframework::CellAbstractContext* context, log4cplus::Logger& logger)
  :tsframework::DataSource(infospaceName,context,logger)
{
  //Monitoring example code
  add<xdata::String>("item");
  autorefresh("item",this,&rpcttsworker::MonitorSource::refreshItem);

  add<xdata::Table>("large_data");
  autorefresh("large_data",this,&rpcttsworker::MonitorSource::refreshData);

  add<xdata::String>("itemNonAutoRefresh");

}


//Monitoring example code
void 
rpcttsworker::MonitorSource::refreshItem(const std::string& name, xdata::String& item)
{
  
  std::ostringstream msg;
  msg << "worker item is random = " << rand();
  item = msg.str();
}

void 
rpcttsworker::MonitorSource::refreshData(const std::string& name, xdata::Table& data)
{
  data.clear();
  data.addColumn("column1","string");
  data.addColumn("column2","int");

  for(unsigned int i(0); i!=1000;++i) {
    std::ostringstream msg;
    msg << "worker item is random = " << rand();
    xdata::String column1(msg.str());
    xdata::Integer column2(rand());

    data.setValueAt(i,"column1",column1);
    data.setValueAt(i,"column2",column2);
  }
}
