#include "rpct/ts/worker/RpctCellContext.h"
#include "ts/framework/Level1Source.h"
#include "rpct/xdaqutils/LogUtils.h"
#include "ts/exception/CellException.h"
#include "ts/toolbox/CellToolbox.h"
#include "ts/framework/CellAbstract.h"

#include "rpct/ts/worker/panels/MonitorablesWidget.h"
#include "rpct/ts/worker/panels/HistogramsWidget.h"
#include "rpct/ts/worker/MonitorSource.h"

#include <string>
#include <cstdlib>

#include "toolbox/string.h"

#include "log4cplus/logger.h"

#include "rpct/xdaqutils/LogUtils.h"

#include "rpct/ts/worker/XdaqHardwareAccess.h"
#include "rpct/ts/worker/xdaqlboxaccess/LinkBoxTester.h"

#include "rpct/ts/worker/panels/LogMessageQueue.h"


rpcttsworker::RpctCellContext::RpctCellContext(log4cplus::Logger& log, 
        tsframework::CellAbstract* cell)
: tsframework::CellAbstractContext(log, cell)
, LoggerCellContext(log, cell)
{
    monitorablesWidget_ = new rpct::MonitorablesWidget((xdaq::Application*)cell, getLevel1Source());
    histogramsWidget_ = new rpcttspanels::HistogramsWidget((xdaq::Application*)cell, monitorablesWidget_);

    datasource_ = new rpcttsworker::MonitorSource("rpct_worker",this, logger_);

    // https://savannah.cern.ch/task/index.php?14360
    //getLevel1Source().setPeriodicAction(monitorablesWidget_, &rpct::MonitorablesWidget::updatel1Source);  //TODO uncomment when ready
    getLevel1Source().setSubsystem("RPC");

    XdaqHardwareAccess::setupLoggers(cell->getApplicationLogger().getName());
    LinkBoxTester::setupLogger(cell->getApplicationLogger().getName());
    rpct::setupLoggers(cell->getApplicationLogger().getName());

    loggerwidget_ = new rpct::LoggerWidget(*this, 200);
    loggerwidget_->setIsOwned(false);

    // appLogger_ = &cell->getApplicationLogger();

    // lQueue_ = log4cplus::SharedAppenderPtr( new rpcttsworker::LogMessageQueue(cell->getApplicationLogger().getName(), 500) );


    SharedAppenderPtr saPtr( &(loggerwidget_->getAppender()) );
    XdaqHardwareAccess::getSummaryLogger().addAppender(saPtr);

    LOG4CPLUS_INFO(log, "RpctCellContext constructor end");
}
rpcttsworker::RpctCellContext::~RpctCellContext()
{ 
    ;
}

std::string 
rpcttsworker::RpctCellContext::getClassName()
{
    return "CellContext";
}

