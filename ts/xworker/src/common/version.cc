//#include "config/PackageInfo.h"
#include "config/version.h"
#include "rpct/ts/worker/version.h"
#include "ts/framework/version.h"
#include "ts/toolbox/version.h"
#include "toolbox/version.h"
#include "xcept/version.h"
#include "xoap/version.h"
#include "xdata/version.h"

GETPACKAGEINFO(rpcttsworker)

void rpcttsworker::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
  CHECKDEPENDENCY(config);
/*  CHECKDEPENDENCY(xoap); //see https://savannah.cern.ch/task/?20519
  CHECKDEPENDENCY(xcept);
  CHECKDEPENDENCY(xdata);
  CHECKDEPENDENCY(tstoolbox);
  CHECKDEPENDENCY(tsframework);
  CHECKDEPENDENCY(toolbox);*/
}

std::set<std::string, std::less<std::string> > rpcttsworker::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;
	ADDDEPENDENCY(dependencies,config);
/*	ADDDEPENDENCY(dependencies,xcept); //see https://savannah.cern.ch/task/?20519
	ADDDEPENDENCY(dependencies,xoap);
	ADDDEPENDENCY(dependencies,xdata);
	ADDDEPENDENCY(dependencies,tstoolbox);
	ADDDEPENDENCY(dependencies,tsframework);
	ADDDEPENDENCY(dependencies,toolbox);*/
	return dependencies;
}	
