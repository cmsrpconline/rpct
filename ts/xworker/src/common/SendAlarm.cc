#include "rpct/ts/worker/SendAlarm.h"

#include "xcept/tools.h"

#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellAbstract.h"

#include "ts/exception/CellException.h"
   

rpcttsworker::SendAlarm::SendAlarm(log4cplus::Logger& log, tsframework::CellAbstractContext* context)
  :tsframework::CellCommand(log,context)
{
  logger_ = log4cplus::Logger::getInstance(log.getName() +".SendAlarm");

}

void rpcttsworker::SendAlarm::code(){

  LOG4CPLUS_INFO(getLogger(), "Starting SendAlarm::code() method");
  
  XCEPT_DECLARE(tsexception::CellException,q,"Testing the alarms");
  
  getContext()->getCell()->notifyQualified("error",q);

  *dynamic_cast<xdata::String*>(payload_) = "Alarm sent to spotlight";

  LOG4CPLUS_INFO(getLogger(), "Ending SendAlarm::code() method");
}
