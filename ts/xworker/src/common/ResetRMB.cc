#include "rpct/ts/worker/ResetRMB.h"

#include "xcept/tools.h"

#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellAbstract.h"

#include "ts/exception/CellException.h"

#include "rpct/ts/worker/XdaqTCrateAccess.h"
#include "rpct/ts/worker/TBCell.h"

rpcttsworker::ResetRMB::ResetRMB(log4cplus::Logger& log, tsframework::CellAbstractContext* context)
  :tsframework::CellCommand(log,context)
{
  tcCrateAccess_ = dynamic_cast<rpcttsworker::TBCellContext*>(context)->getTCrateAccess();
  logger_ = log4cplus::Logger::getInstance(log.getName() +".ResetRMB");

}

void rpcttsworker::ResetRMB::code(){

  LOG4CPLUS_INFO(getLogger(), "Starting ResetRMB::code() method");
  

  LOG4CPLUS_INFO(getLogger(), "Ending ResetRMB::code() method");
}
