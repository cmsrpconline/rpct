/*
 *  Authors: Michal Pietrusinski, Mikolaj Cwiok, Karol Bunkowski, Krzysztof Niemkiewicz
 *  Version: $Id: XdaqLBoxAccess.cpp,v 1.121 2009/10/11 13:20:58 tb Exp $
 *
 */
#include "rpct/ts/worker/xdaqlboxaccess/LinkBoxSoapInterface.h"
#include "rpct/ts/worker/xdaqlboxaccess/XdaqLBoxAccess.h"
#include "rpct/ts/worker/xdaqlboxaccess/HardwareItemException.h"
#include "rpct/diag/IDiagnosable.h"
#include "rpct/devices/cb.h"
#include "rpct/devices/test/cbMock.h"
#include "rpct/devices/XmlSystemBuilder.h"
#include "rpct/devices/System.h"
#include "rpct/devices/LinkBoard.h"
#include "rpct/devices/LinkBox.h"
#include "rpct/devices/ConfigurationSetImpl.h"
#include "rpct/devices/SynCoderSettingsImpl.h"
#include "rpct/ii/ConfigurationFlags.h"
#include "rpct/lboxaccess/FecManagerImpl.h"
#include "rpct/xdaqutils/XdaqDbServiceClient.h"
#include "rpct/xdaqutils/XdaqDbSystemBuilder.h"
#include "rpct/std/NullPointerException.h"
#include "rpct/std/IllegalArgumentException.h"
#include "rpct/xdaqutils/MonitorableStatusInfo.h"
#include "rpct/xdaqutils/LogUtils.h"
#include "rpct/xdaqutils/XdaqCommonUtils.h"

#include "rpct/ts/worker/AsyncSoapCallProxy.h"

#include "rpct/ts/worker/xdaqlboxaccess/MassiveReadRequest.h"
#include "rpct/ts/worker/xdaqlboxaccess/MassiveReadResponse.h"
#include "rpct/ts/worker/xdaqlboxaccess/MassiveWriteRequest.h"
#include "rpct/ts/worker/xdaqlboxaccess/MassiveWriteResponse.h"
#include "rpct/ts/worker/xdaqlboxaccess/FecStatusResponse.h"

#include "xdata/Serializable.h"
#include "xdata/exception/Exception.h"
#include "xdata/XStr.h"
#include "xdata/Float.h"
#include "xdata/Bag.h"
#include "xdata/Boolean.h"
#include "xdata/Integer.h"
#include "xdata/TableIterator.h"



#include "xoap/DOMParser.h"
#include "xoap/domutils.h"
#include "xcept/tools.h"


#include <time.h>

#include <boost/shared_ptr.hpp>


// shortcut for binding a method via AsyncSoapCallProxy
// should be used only in cell constructor
#define LBOX_ASYNC_BIND(method,name) (new AsyncSoapCallProxy<XdaqLBoxAccess>(app_,this,(method)))->bindMe((name), RPCT_LBOX_ACCESS_NS)

using namespace rpct;
using namespace rpct::xdaqdiagaccess;
using namespace rpct::xdaqutils;
using namespace std;
using namespace toolbox;

// moved from headers as they caused linker errors

const char* MassiveReadRequest::PROP_TEMPERATURE = "TEMPERATURE";
const char* MassiveReadRequest::PROP_TEMPERATURE2 = "TEMPERATURE2";
const char* MassiveReadRequest::PROP_VTH1 = "VTH1";
const char* MassiveReadRequest::PROP_VTH2 = "VTH2";
const char* MassiveReadRequest::PROP_VTH3 = "VTH3";
const char* MassiveReadRequest::PROP_VTH4 = "VTH4";
const char* MassiveReadRequest::PROP_VMON1 = "VMON1";
const char* MassiveReadRequest::PROP_VMON2 = "VMON2";
const char* MassiveReadRequest::PROP_VMON3 = "VMON3";
const char* MassiveReadRequest::PROP_VMON4 = "VMON4";



const char* MassiveWriteRequest::PROP_VTH1 = "VTH1";
const char* MassiveWriteRequest::PROP_VTH2 = "VTH2";
const char* MassiveWriteRequest::PROP_VTH3 = "VTH3";
const char* MassiveWriteRequest::PROP_VTH4 = "VTH4";
const char* MassiveWriteRequest::PROP_VMON1 = "VMON1";
const char* MassiveWriteRequest::PROP_VMON2 = "VMON2";
const char* MassiveWriteRequest::PROP_VMON3 = "VMON3";
const char* MassiveWriteRequest::PROP_VMON4 = "VMON4";
const char* MassiveWriteRequest::PROP_AUTO_CORRECTON = "autoCorrection";



const char* LinkBoxSoapInterface::ENABLE_DAC_NAME = "enableDAC";
const char* LinkBoxSoapInterface::READ_TEMPERATURE_NAME = "readTemperature";
const char* LinkBoxSoapInterface::READ_VTH1_NAME = "readVTH1";
const char* LinkBoxSoapInterface::WRITE_VTH1_NAME = "writeVTH1";
const char* LinkBoxSoapInterface::READ_VTH2_NAME = "readVTH2";
const char* LinkBoxSoapInterface::WRITE_VTH2_NAME = "writeVTH2";
const char* LinkBoxSoapInterface::READ_VMON1_NAME = "readVMon1";
const char* LinkBoxSoapInterface::WRITE_VMON1_NAME = "writeVMon1";
const char* LinkBoxSoapInterface::READ_VMON2_NAME = "readVMon2";
const char* LinkBoxSoapInterface::WRITE_VMON2_NAME = "writeVMon2";
const char* LinkBoxSoapInterface::MASSIVE_READ_NAME = "massiveRead";
const char* LinkBoxSoapInterface::MASSIVE_WRITE_NAME = "massiveWrite";
const char* LinkBoxSoapInterface::READ_RBC_REGISTER_NAME = "readRbcRegister";
const char* LinkBoxSoapInterface::WRITE_RBC_REGISTER_NAME = "writeRbcRegister";
const char* LinkBoxSoapInterface::READ_RBC_TEMPERATURE_NAME = "readRbcTemperature";


LinkBoxSoapInterface::LinkBoxSoapInterface(xdaq::Application * app,
					   XdaqLBoxAccess* lboxAccess,
					   toolbox::BSem* hardwareMutex) :
  logger_(log4cplus::Logger::getInstance(app->getApplicationLogger().getName() +".LinkBoxSoapInterface")),
  summaryLogger_(log4cplus::Logger::getInstance(logger_.getName() +".summary")),
  lboxAccess_(lboxAccess), soapAccess_(app),
  hardwareMutex_(hardwareMutex), febWriteReadBack(false), febWriteAutoCorrection(false)
{
    app->getApplicationInfoSpace()->fireItemAvailable("febWriteReadBack", &febWriteReadBack);
    app->getApplicationInfoSpace()->fireItemAvailable("febWriteAutoCorrection", &febWriteAutoCorrection);

    if (app)
    {
        rpct::bind(app, this, &LinkBoxSoapInterface::onEnableDAC, ENABLE_DAC_NAME, XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS);
	rpct::bind(app, this, &LinkBoxSoapInterface::onReadTemperature, READ_TEMPERATURE_NAME, XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS);

	rpct::bind(app, this, &LinkBoxSoapInterface::onReadVTH1, READ_VTH1_NAME, XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS);
	rpct::bind(app, this, &LinkBoxSoapInterface::onWriteVTH1, WRITE_VTH1_NAME, XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS);
	rpct::bind(app, this, &LinkBoxSoapInterface::onReadVTH2, READ_VTH2_NAME, XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS);
	rpct::bind(app, this, &LinkBoxSoapInterface::onWriteVTH2, WRITE_VTH2_NAME, XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS);
	
	rpct::bind(app, this, &LinkBoxSoapInterface::onReadVMon1, READ_VMON1_NAME, XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS);
	rpct::bind(app, this, &LinkBoxSoapInterface::onWriteVMon1, WRITE_VMON1_NAME, XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS);
	rpct::bind(app, this, &LinkBoxSoapInterface::onReadVMon2, READ_VMON2_NAME, XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS);
	rpct::bind(app, this, &LinkBoxSoapInterface::onWriteVMon2, WRITE_VMON2_NAME, XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS);

	rpct::bind(app, this, &LinkBoxSoapInterface::onMassiveRead, MASSIVE_READ_NAME, XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS);
	rpct::bind(app, this, &LinkBoxSoapInterface::onMassiveWrite, MASSIVE_WRITE_NAME, XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS);

	rpct::bind(app, this, &LinkBoxSoapInterface::onReadRbcRegister, READ_RBC_REGISTER_NAME,
		   XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS);
	rpct::bind(app, this, &LinkBoxSoapInterface::onWriteRbcRegister, WRITE_RBC_REGISTER_NAME,
		   XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS);
	rpct::bind(app, this, &LinkBoxSoapInterface::onReadRbcTemperature, READ_RBC_TEMPERATURE_NAME,
		   XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS);
    }
}


xoap::MessageReference LinkBoxSoapInterface::onEnableDAC(xoap::MessageReference msg)
        throw (xoap::exception::Exception) {
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__);
    lboxAccess_->checkConfigured();
    xdata::Bag<FebInfo> febInfo;
    xdata::Boolean enable;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("febInfo", &febInfo));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("enable", &enable));
    soapAccess_.parseSOAPRequest(msg, "enableDAC", XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX, XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS,
            paramMap);
    try {
        prepareFeb(febInfo.bag, true, true, MassiveWriteRequest::AUTO_CORRECTION_DEFAULT);
        xoap::MessageReference reply = xoap::createMessage();
        return reply;
    } catch (xcept::Exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (std::exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (...) {
        FUNCTION_ERR("soap error", "unknown error");
        XCEPT_RAISE(xoap::exception::Exception, (string)"unknown error during "+__FUNCTION__);
    }
}

xoap::MessageReference LinkBoxSoapInterface::onReadTemperature(xoap::MessageReference msg)
        throw (xoap::exception::Exception) {
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__);
    lboxAccess_->checkConfigured();

    xdata::Bag<FebInfo> febInfo;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("febInfo", &febInfo));
    soapAccess_.parseSOAPRequest(msg, "readTemperature", XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS, paramMap);

    LOG4CPLUS_DEBUG(getLogger(), "FebInfo: " << hex << febInfo.bag.getCcuAddress() );

    rpct::MutexHandler handler(hardwareMutex_);
    try {
        xdata::Float temperature = prepareFeb(febInfo.bag, true, false,
                MassiveWriteRequest::AUTO_CORRECTION_DEFAULT).readTemperature();
        LOG4CPLUS_DEBUG(getLogger(), toolbox::toString("read %f", (float)temperature) );

        return soapAccess_.sendSOAPResponse("readTemperature", XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX,
                XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS, temperature);
    } catch (xcept::Exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (std::exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (...) {
        FUNCTION_ERR("soap error", "unknown error");
        XCEPT_RAISE(xoap::exception::Exception, (string)"unknown error during "+__FUNCTION__);
    }
}

xoap::MessageReference LinkBoxSoapInterface::onReadVTH1(xoap::MessageReference msg)
        throw (xoap::exception::Exception) {
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__);
    lboxAccess_->checkConfigured();
    xdata::Bag<FebInfo> febInfo;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("febInfo", &febInfo));
    soapAccess_.parseSOAPRequest(msg, "readVTH1", XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX, XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS,
            paramMap);

    rpct::MutexHandler handler(hardwareMutex_);
    try {
        xdata::Float vth1 = prepareFeb(febInfo.bag, true, false,
                MassiveWriteRequest::AUTO_CORRECTION_DEFAULT).readVTH1();
        LOG4CPLUS_DEBUG(getLogger(), toolbox::toString("read %f", (float)vth1) );

        return soapAccess_.sendSOAPResponse("readVTH1", XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX,
                XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS, vth1);
    } catch (xcept::Exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (std::exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (...) {
        FUNCTION_ERR("soap error", "unknown error");
        XCEPT_RAISE(xoap::exception::Exception, (string)"unknown error during "+__FUNCTION__);
    }
}

xoap::MessageReference LinkBoxSoapInterface::onWriteVTH1(xoap::MessageReference msg)
        throw (xoap::exception::Exception) {
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__);
    lboxAccess_->checkConfigured();

    xdata::Bag<FebInfo> febInfo;
    xdata::Float vth1;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("febInfo", &febInfo));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("vth1", &vth1));
    soapAccess_.parseSOAPRequest(msg, "writeVTH1", XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX, XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS,
            paramMap);

    rpct::MutexHandler handler(hardwareMutex_);
    try {
        prepareFeb(febInfo.bag, true, false, MassiveWriteRequest::AUTO_CORRECTION_DEFAULT).writeVTH1(
                (float) vth1);
        xoap::MessageReference reply = xoap::createMessage();
        return reply;
    } catch (TException& e) {
        LOG4CPLUS_ERROR(getLogger(), e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (...) {
        LOG4CPLUS_ERROR(getLogger(), (string)"unknown error during "+__FUNCTION__);
        XCEPT_RAISE(xoap::exception::Exception, (string)"unknown error during "+__FUNCTION__);
        throw ;
    }
}

xoap::MessageReference LinkBoxSoapInterface::onReadVTH2(xoap::MessageReference msg)
        throw (xoap::exception::Exception) {
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__);
    lboxAccess_->checkConfigured();

    xdata::Bag<FebInfo> febInfo;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("febInfo", &febInfo));
    soapAccess_.parseSOAPRequest(msg, "readVTH2", XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX, XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS,
            paramMap);

    rpct::MutexHandler handler(hardwareMutex_);
    try {
        xdata::Float vth2 = prepareFeb(febInfo.bag, true, false,
                MassiveWriteRequest::AUTO_CORRECTION_DEFAULT).readVTH2();
        LOG4CPLUS_DEBUG(getLogger(), toolbox::toString("read %f", (float)vth2) );

        return soapAccess_.sendSOAPResponse("readVTH2", XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX,
                XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS, vth2);
    } catch (xcept::Exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (std::exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (...) {
        FUNCTION_ERR("soap error", "unknown error");
        XCEPT_RAISE(xoap::exception::Exception, (string)"unknown error during "+__FUNCTION__);
    }
}

xoap::MessageReference LinkBoxSoapInterface::onWriteVTH2(xoap::MessageReference msg)
        throw (xoap::exception::Exception) {
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__);
    lboxAccess_->checkConfigured();

    xdata::Bag<FebInfo> febInfo;
    xdata::Float vth2;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("febInfo", &febInfo));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("vth2", &vth2));
    soapAccess_.parseSOAPRequest(msg, "writeVTH2", XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX, XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS,
            paramMap);
    rpct::MutexHandler handler(hardwareMutex_);
    try {
        prepareFeb(febInfo.bag, true, false, MassiveWriteRequest::AUTO_CORRECTION_DEFAULT).writeVTH2(
                (float) vth2);
        xoap::MessageReference reply = xoap::createMessage();
        return reply;
    } catch (xcept::Exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (std::exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (...) {
        FUNCTION_ERR("soap error", "unknown error");
        XCEPT_RAISE(xoap::exception::Exception, (string)"unknown error during "+__FUNCTION__);
    }
}

xoap::MessageReference LinkBoxSoapInterface::onReadVMon1(xoap::MessageReference msg)
        throw (xoap::exception::Exception) {
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__);
    lboxAccess_->checkConfigured();

    xdata::Bag<FebInfo> febInfo;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("febInfo", &febInfo));
    soapAccess_.parseSOAPRequest(msg, "readVMon1", XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX, XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS,
            paramMap);

    rpct::MutexHandler handler(hardwareMutex_);
    try {
        xdata::Float vmon1 = prepareFeb(febInfo.bag, true, false,
                MassiveWriteRequest::AUTO_CORRECTION_DEFAULT).readVMon1();
        LOG4CPLUS_DEBUG(getLogger(), toolbox::toString("read %f", (float)vmon1) );

        return soapAccess_.sendSOAPResponse("readVMon1", XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX,
                XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS, vmon1);
    } catch (xcept::Exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (std::exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (...) {
        FUNCTION_ERR("soap error", "unknown error");
        XCEPT_RAISE(xoap::exception::Exception, (string)"unknown error during "+__FUNCTION__);
    }
}

xoap::MessageReference LinkBoxSoapInterface::onWriteVMon1(xoap::MessageReference msg)
        throw (xoap::exception::Exception) {
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__);
    lboxAccess_->checkConfigured();

    xdata::Bag<FebInfo> febInfo;
    xdata::Float vmon1;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("febInfo", &febInfo));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("vmon1", &vmon1));
    soapAccess_.parseSOAPRequest(msg, "writeVMon1", XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX, XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS,
            paramMap);
    rpct::MutexHandler handler(hardwareMutex_);
    try {
        prepareFeb(febInfo.bag, true, false, MassiveWriteRequest::AUTO_CORRECTION_DEFAULT).writeVMon1(
                (float) vmon1);
        xoap::MessageReference reply = xoap::createMessage();
        return reply;
    } catch (xcept::Exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (std::exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (...) {
        FUNCTION_ERR("soap error", "unknown error");
        XCEPT_RAISE(xoap::exception::Exception, (string)"unknown error during "+__FUNCTION__);
    }
}

xoap::MessageReference LinkBoxSoapInterface::onReadVMon2(xoap::MessageReference msg)
        throw (xoap::exception::Exception) {
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__);
    lboxAccess_->checkConfigured();

    xdata::Bag<FebInfo> febInfo;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("febInfo", &febInfo));
    soapAccess_.parseSOAPRequest(msg, "readVMon2", XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX, XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS,
            paramMap);

    rpct::MutexHandler handler(hardwareMutex_);
    try {
        xdata::Float vmon2 = prepareFeb(febInfo.bag, true, false,
                MassiveWriteRequest::AUTO_CORRECTION_DEFAULT).readVMon2();
        LOG4CPLUS_DEBUG(getLogger(), toolbox::toString("read %f", (float)vmon2) );

        return soapAccess_.sendSOAPResponse("readVMon2", XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX,
                XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS, vmon2);
    } catch (xcept::Exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (std::exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (...) {
        FUNCTION_ERR("soap error", "unknown error");
        XCEPT_RAISE(xoap::exception::Exception, (string)"unknown error during "+__FUNCTION__);
    }
}

xoap::MessageReference LinkBoxSoapInterface::onWriteVMon2(xoap::MessageReference msg)
        throw (xoap::exception::Exception) {
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__);
    lboxAccess_->checkConfigured();

    xdata::Bag<FebInfo> febInfo;
    xdata::Float vmon2;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("febInfo", &febInfo));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("vmon2", &vmon2));
    soapAccess_.parseSOAPRequest(msg, "writeVMon2", XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX, XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS,
            paramMap);

    rpct::MutexHandler handler(hardwareMutex_);
    try {
        prepareFeb(febInfo.bag, true, false, MassiveWriteRequest::AUTO_CORRECTION_DEFAULT).writeVMon2(
                (float) vmon2);
        xoap::MessageReference reply = xoap::createMessage();
        return reply;
    } catch (xcept::Exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (std::exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (...) {
        FUNCTION_ERR("soap error", "unknown error");
        XCEPT_RAISE(xoap::exception::Exception, (string)"unknown error during "+__FUNCTION__);
    }
}





Feb& LinkBoxSoapInterface::prepareFeb(FebInfo& febInfo, bool enableDac, bool forceEnable,
        int autoCorrection) {
    LOG4CPLUS_INFO(getLogger(), "preparing " << febInfo);
    rpct::LinkSystem* system = lboxAccess_->getLinkSystem();
    if (!system) {
        string msg = "System pointer not initialized";
        LOG4CPLUS_ERROR(getLogger(), msg);
        throw TException(msg);
    }
    FecManager& fm = lboxAccess_->getFecManager();
    HalfBox& halfBox = system->getHalfBox(fm.getPciSlot(), fm.getVmeSlot(), fm.getRingNumber(),
            (int) febInfo.getCcuAddress());
    halfBox.getCb().selectI2CChannel((int) febInfo.getChannel());

    if (febInfo.isEndcap()) {
        FebEndcap& feb = halfBox.getCb().getFebEndcap((int) febInfo.getChannel(),
                (int) febInfo.getAddress());

        if (forceEnable || !feb.getDistributionBoard().isFebAccessEnabled()) {
            feb.getDistributionBoard().enableFebAccess(true);
        }
        if (enableDac) {
            if (forceEnable || !feb.isDacEnabled()) {
                feb.enableDAC(true);
            }
            if (forceEnable || !feb.isDac2Enabled()) {
                feb.enableDAC2(true);
            }
        }

        if (autoCorrection == MassiveWriteRequest::AUTO_CORRECTION_DEFAULT) {
            feb.setAutoCorrection(febWriteAutoCorrection);
        } else {
            feb.setAutoCorrection(autoCorrection == MassiveWriteRequest::AUTO_CORRECTION_TRUE);
        }
        return feb;
    } else {
        Feb& feb = halfBox.getCb().getFeb((int) febInfo.getChannel(), (int) febInfo.getAddress());

        if (forceEnable || !feb.getDistributionBoard().isFebAccessEnabled()) {
            feb.getDistributionBoard().enableFebAccess(true);
        }
        if (enableDac && (forceEnable || !feb.isDacEnabled())) {
            feb.enableDAC(true);
        }
        if (autoCorrection == MassiveWriteRequest::AUTO_CORRECTION_DEFAULT) {
            feb.setAutoCorrection(febWriteAutoCorrection);
        } else {
            feb.setAutoCorrection(autoCorrection == MassiveWriteRequest::AUTO_CORRECTION_TRUE);
        }
        return feb;
    }
}



xoap::MessageReference LinkBoxSoapInterface::onMassiveRead(xoap::MessageReference msg)
        throw (xoap::exception::Exception) {
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__);
    lboxAccess_->checkConfigured();

    xdata::Bag<MassiveReadRequest> readRequest;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("readRequest", &readRequest));
    soapAccess_.parseSOAPRequest(msg, "massiveRead", XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX, XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS,
            paramMap);
    FebProperties& properties = readRequest.bag.getProperties();
    MassiveReadRequest::Febs& febs = readRequest.bag.getFebs();

    rpct::MutexHandler handler(hardwareMutex_);
    FebInfo* febInfo = 0;
    try {
        xdata::Bag<MassiveReadResponse> result;
        result.bag.setProperties(properties);

        for (MassiveReadRequest::Febs::iterator iFeb = febs.begin(); iFeb != febs.end(); ++iFeb) {

            FebValuesBag febValuesBag;
            febValuesBag.bag.getFeb() = *iFeb;
            febInfo = &iFeb->bag;

            if (((int) febInfo->getCcuAddress() == 0) || ((int) febInfo->getCcuAddress() == -1)) {
                LOG4CPLUS_WARN(getLogger(), "skipping read " << *febInfo);
                /*typedef FebProperties Props;
                 for (Props::iterator iProp = properties.begin(); iProp != properties.end(); ++iProp) {
                 febValuesBag.bag.getValues().push_back(0);
                 }*/
                result.bag.getFebErrors().push_back(FebErrorsBag());
                FebErrors& febErrors = result.bag.getFebErrors().back().bag;
                febErrors.setFeb(febValuesBag.bag.getFeb());
                febErrors.setError("No CCU address for this FEB");
            } else {
                try {
                    Feb& feb = prepareFeb(*febInfo, false, false,
                            MassiveWriteRequest::AUTO_CORRECTION_DEFAULT);
                    readFeb(feb, properties, febValuesBag.bag.getValues());
                    result.bag.getFebValues().push_back(febValuesBag);
                } catch (std::exception& e) {
                    LOG4CPLUS_WARN(getLogger(), "Problems reading " << *febInfo << ": " << e.what());
                    result.bag.getFebErrors().push_back(FebErrorsBag());
                    FebErrors& febErrors = result.bag.getFebErrors().back().bag;
                    febErrors.setFeb(febValuesBag.bag.getFeb());
                    febErrors.setError(std::string("Problems reading: ") + e.what());
                    /*if (interactive_) {
                     LOG4CPLUS_WARN(getLogger(), "Problems reading " << *febInfo << ": " << e.what());
                     }
                     else {
                     throw;
                     }*/
                }
            }
            //result.bag.getFebValues().push_back(febValuesBag);
        }

        return soapAccess_.sendSOAPResponse("massiveRead", XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX,
                XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS, result);
    } catch (xcept::Exception& e) {
        ostringstream ostr;
        ostr << e.what();
        if (febInfo != 0) {
            ostr << " for " << *febInfo;
        }
        LOG4CPLUS_ERROR(getLogger(), ostr.str());
        XCEPT_RETHROW(xoap::exception::Exception, ostr.str(), e);
    } catch (std::exception& e) {
        ostringstream ostr;
        ostr << e.what();
        if (febInfo != 0) {
            ostr << " for " << *febInfo;
        }
        LOG4CPLUS_ERROR(getLogger(), ostr.str());
        XCEPT_RAISE(xoap::exception::Exception, ostr.str());
    } catch (...) {
        LOG4CPLUS_ERROR(getLogger(), (string)"unknown error during "+__FUNCTION__);
        XCEPT_RAISE(xoap::exception::Exception, (string)"unknown error during "+__FUNCTION__);
        throw ;
    }
}

xoap::MessageReference LinkBoxSoapInterface::onMassiveWrite(xoap::MessageReference msg)
        throw (xoap::exception::Exception) {
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__);
    lboxAccess_->checkConfigured();

    xdata::Bag<MassiveWriteRequest> writeRequest;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("writeRequest", &writeRequest));
    soapAccess_.parseSOAPRequest(msg, "massiveWrite", XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX, XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS,
            paramMap);
    FebProperties& properties = writeRequest.bag.getProperties();
    FebValuesVector& febValuesVector = writeRequest.bag.getFebValues();

    rpct::MutexHandler handler(hardwareMutex_);
    FebInfo* febInfo = 0;
    try {
        xdata::Bag<MassiveWriteResponse> result;
        if (febWriteReadBack) {
            result.bag.setProperties(properties);
        }

        for (FebValuesVector::iterator iFebValues = febValuesVector.begin(); iFebValues
                != febValuesVector.end(); ++iFebValues) {

            FebValues& febValues = iFebValues->bag;
            febInfo = &febValues.getFeb().bag;

            if (((int) febInfo->getCcuAddress() == 0) || ((int) febInfo->getCcuAddress() == -1)) {
                LOG4CPLUS_WARN(getLogger(), "Skipping write " << *febInfo);
                /*FebValues::Values& values = febValues.getValues();
                 for (FebValues::Values::size_type i = 0; i < values.size(); i++) {
                 values[i] = 0;
                 }*/

                result.bag.getFebErrors().push_back(FebErrorsBag());
                FebErrors& febErrors = result.bag.getFebErrors().back().bag;
                febErrors.setFeb(febValues.getFeb());
                febErrors.setError("No CCU address for this FEB");
            } else {
                try {
                    Feb& feb = prepareFeb(*febInfo, true, false,
                            writeRequest.bag.getAutoCorrection());
                    writeFeb(feb, properties, febValues.getValues());

                    // read vth, if this is 0 probably i2c access is not enabled
                    // set read values in the values vector
                    for (FebProperties::size_type i = 0; i < properties.size(); i++) {
                        string prop = (string)properties[i];
                        if (prop == MassiveWriteRequest::PROP_VTH1
                                || prop == MassiveWriteRequest::PROP_VTH2
                                || prop == MassiveWriteRequest::PROP_VTH3
                                || prop == MassiveWriteRequest::PROP_VTH4) {
                            if (febValues.getValues()[i].value_ == 0.0) {
                                // prepare FEB once again, with enforce flag set to true
                                Feb& feb = prepareFeb(*febInfo, true, true,
                                        writeRequest.bag.getAutoCorrection());
                                writeFeb(feb, properties, febValues.getValues());
                            }
                            break;
                        }
                    }

                    if (febWriteReadBack) {
                        result.bag.getFebValues().push_back(*iFebValues);
                    }

                } catch (std::exception& e) {
                    LOG4CPLUS_WARN(getLogger(), "Problems writing " << *febInfo << ": " << e.what());
                    result.bag.getFebErrors().push_back(FebErrorsBag());
                    FebErrors& febErrors = result.bag.getFebErrors().back().bag;
                    febErrors.setFeb(febValues.getFeb());
                    febErrors.setError(std::string("Problems writing: ") + e.what());

                    /*if (interactive_) {
                     LOG4CPLUS_WARN(getLogger(), "Problems writing " << *febInfo << ": " << e.what());
                     /
                     }
                     else {
                     throw;
                     }*/

                }
            }

        }
        return soapAccess_.sendSOAPResponse("massiveWrite", XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX,
                XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS, result);
    } catch (xcept::Exception& e) {
        ostringstream ostr;
        ostr << e.what();
        if (febInfo != 0) {
            ostr << " for " << *febInfo;
        }
        LOG4CPLUS_ERROR(getLogger(), ostr.str());
        XCEPT_RETHROW(xoap::exception::Exception, ostr.str(), e);
    } catch (std::exception& e) {
        ostringstream ostr;
        ostr << e.what();
        if (febInfo != 0) {
            ostr << " for " << *febInfo;
        }
        LOG4CPLUS_ERROR(getLogger(), ostr.str());
        XCEPT_RAISE(xoap::exception::Exception, ostr.str());
    } catch (...) {
        LOG4CPLUS_ERROR(getLogger(), (string)"unknown error during "+__FUNCTION__);
        XCEPT_RAISE(xoap::exception::Exception, (string)"unknown error during "+__FUNCTION__);
        throw ;
    }
}

/* int readRbcRegister(string lboxName, int registerAddress);
 * void writeRbcRegister(string lboxName, int regsiterAddress, int data);
 * int readRbcTemperature(string lboxName)
 * int readFecStatus(string lboxName)
 * int resetFec(string lboxName)       .*/
xoap::MessageReference LinkBoxSoapInterface::onReadRbcRegister(xoap::MessageReference msg)
        throw (xoap::exception::Exception) {
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__);
    lboxAccess_->checkConfigured();

    xdata::String linkBoxName;
    xdata::Integer address;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("linkBoxName", &linkBoxName));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("address", &address));
    soapAccess_.parseSOAPRequest(msg, READ_RBC_REGISTER_NAME, XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS, paramMap);

    rpct::MutexHandler handler(hardwareMutex_);
    try {
        TI2CInterface& i2c = prepareRbc((string) linkBoxName);
        i2c.Write8(0x0, (int) address); /* Write the reg. Address into the Address Pointer */
        xdata::Integer result = i2c.Read8(0x1);
        LOG4CPLUS_DEBUG(getLogger(), toolbox::toString("readRbcRegister %s %x ", ((string)linkBoxName).c_str(), (int)address) );

        return soapAccess_.sendSOAPResponse(READ_RBC_REGISTER_NAME, XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX,
                XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS, result);
    } catch (xcept::Exception& e) {
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (std::exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (...) {
        FUNCTION_ERR("soap error", "unknown error");
        XCEPT_RAISE(xoap::exception::Exception, (string)"unknown error during "+__FUNCTION__);
    }
}

xoap::MessageReference LinkBoxSoapInterface::onWriteRbcRegister(xoap::MessageReference msg)
        throw (xoap::exception::Exception) {
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__);
    lboxAccess_->checkConfigured();

    xdata::String linkBoxName;
    xdata::Integer address;
    xdata::Integer data;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("linkBoxName", &linkBoxName));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("address", &address));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("data", &data));
    soapAccess_.parseSOAPRequest(msg, WRITE_RBC_REGISTER_NAME, XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS, paramMap);

    rpct::MutexHandler handler(hardwareMutex_);
    try {
        TI2CInterface& i2c = prepareRbc((string) linkBoxName);
        i2c.Write8(0x0, (int) address); /* Write the reg. Address into the Address Pointer */
        i2c.Write8(0x1, (int) data);

        LOG4CPLUS_DEBUG(getLogger(), "readRbcRegister " << ((string)linkBoxName).c_str() << " " << ((int)address));

        xoap::MessageReference reply = xoap::createMessage();
        return reply;
    } catch (xcept::Exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (std::exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (...) {
        FUNCTION_ERR("soap error", "unknown error");
        XCEPT_RAISE(xoap::exception::Exception, (string)"unknown error during "+__FUNCTION__);
    }
}

xoap::MessageReference LinkBoxSoapInterface::onReadRbcTemperature(xoap::MessageReference msg)
        throw (xoap::exception::Exception) {
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__);
    lboxAccess_->checkConfigured();

    xdata::String linkBoxName;
    xdata::Integer address;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("linkBoxName", &linkBoxName));
    soapAccess_.parseSOAPRequest(msg, READ_RBC_TEMPERATURE_NAME, XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS, paramMap);

    rpct::MutexHandler handler(hardwareMutex_);
    try {
        TI2CInterface& i2c = prepareRbc((string) linkBoxName);
        //i2c.Write8(0x0, (int)address);   /* Write the reg. Address into the Address Pointer */
        unsigned char c1, c2;
        i2c.ReadADD(0x1, c1, c2);
        unsigned int d1 = c1;
        unsigned int d2 = c2;
        xdata::Integer result = d1 | (d2 << 8);
        LOG4CPLUS_DEBUG(getLogger(), "readRbcTemperature " << ((string)linkBoxName) << " " << ((int)result) );

        return soapAccess_.sendSOAPResponse(READ_RBC_TEMPERATURE_NAME, XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX,
                XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS, result);
    } catch (xcept::Exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (std::exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (...) {
        FUNCTION_ERR("soap error", "unknown error");
        XCEPT_RAISE(xoap::exception::Exception, (string)"unknown error during "+__FUNCTION__);
    }

}


rpct::TI2CInterface& LinkBoxSoapInterface::prepareRbc(string linkBoxName) {
    LOG4CPLUS_INFO(getLogger(), "preparing RBC in " << linkBoxName);
    rpct::LinkSystem* system = lboxAccess_->getLinkSystem();
    if (!system) {
        string msg = "System pointer not initialized";
        LOG4CPLUS_ERROR(getLogger(), msg);
        throw TException(msg);
    }
    LinkBox& linkBox = dynamic_cast<LinkBox&> (system->getCrateByName(linkBoxName));
    HalfBox* halfBox = linkBox.getHalfBox10();
    if (halfBox == 0) {
        throw TException("LinkBoxSoapInterface::prepareRbc: halfBox10 for " + linkBoxName + " is null");
    }
    halfBox->getCb().selectI2CChannel(8);
    return halfBox->getCb().getI2C();
}


void LinkBoxSoapInterface::readFeb(Feb& feb, FebProperties& properties, FebValues::Values& values) {
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__);
    typedef FebProperties Props;
    for (Props::iterator iProp = properties.begin(); iProp != properties.end(); ++iProp) {
        LOG4CPLUS_DEBUG(getLogger(), "Reading " << (string)*iProp);

        if (*iProp == MassiveReadRequest::PROP_TEMPERATURE) {
            float value = feb.readTemperature();
            LOG4CPLUS_DEBUG(getLogger(), "Value " << value);
            values.push_back(value);
        } else if (*iProp == MassiveReadRequest::PROP_VTH1) {
            float value = feb.readVTH1();
            LOG4CPLUS_DEBUG(getLogger(), "Value " << value);
            values.push_back(value);
        } else if (*iProp == MassiveReadRequest::PROP_VTH2) {
            float value = feb.readVTH2();
            LOG4CPLUS_DEBUG(getLogger(), "Value " << value);
            values.push_back(value);
        } else if (*iProp == MassiveReadRequest::PROP_VMON1) {
            float value = feb.readVMon1();
            LOG4CPLUS_DEBUG(getLogger(), "Value " << value);
            values.push_back(value);
        } else if (*iProp == MassiveReadRequest::PROP_VMON2) {
            float value = feb.readVMon2();
            LOG4CPLUS_DEBUG(getLogger(), "Value " << value);
            values.push_back(value);
        } else if (*iProp == MassiveReadRequest::PROP_TEMPERATURE2) {
            float value = dynamic_cast<FebEndcap&> (feb).readTemperature2();
            LOG4CPLUS_DEBUG(getLogger(), "Value " << value);
            values.push_back(value);
        } else if (*iProp == MassiveReadRequest::PROP_VTH3) {
            float value = dynamic_cast<FebEndcap&> (feb).readVTH3();
            LOG4CPLUS_DEBUG(getLogger(), "Value " << value);
            values.push_back(value);
        } else if (*iProp == MassiveReadRequest::PROP_VTH4) {
            float value = dynamic_cast<FebEndcap&> (feb).readVTH4();
            LOG4CPLUS_DEBUG(getLogger(), "Value " << value);
            values.push_back(value);
        } else if (*iProp == MassiveReadRequest::PROP_VMON3) {
            float value = dynamic_cast<FebEndcap&> (feb).readVMon3();
            LOG4CPLUS_DEBUG(getLogger(), "Value " << value);
            values.push_back(value);
        } else if (*iProp == MassiveReadRequest::PROP_VMON4) {
            float value = dynamic_cast<FebEndcap&> (feb).readVMon4();
            LOG4CPLUS_DEBUG(getLogger(), "Value " << value);
            values.push_back(value);
        } else {
            string msg = "Illegal property '" + (string) *iProp + "'";
            LOG4CPLUS_ERROR(getLogger(), msg);
            XCEPT_RAISE(xoap::exception::Exception, msg);
        }
    }
}

void LinkBoxSoapInterface::writeFeb(Feb& feb, FebProperties& properties, FebValues::Values& values) {
    typedef FebProperties Props;
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__);
    if (properties.size() != values.size()) {
        ostringstream ostr;
        ostr << "properties size (" << properties.size() << ") does not match values size("
                << values.size() << ")";
        LOG4CPLUS_ERROR(getLogger(), ostr.str());
        XCEPT_RAISE(xoap::exception::Exception, ostr.str());
    }

    float vth1, vth2, vmon1, vmon2;
    float* pvth1 = 0;
    float* pvth2 = 0;
    float* pvmon1 = 0;
    float* pvmon2 = 0;
    float vth3, vth4, vmon3, vmon4;
    float* pvth3 = 0;
    float* pvth4 = 0;
    float* pvmon3 = 0;
    float* pvmon4 = 0;

    for (Props::size_type i = 0; i < properties.size(); i++) {
        string prop = (string) properties[i];
        float value = (float) values[i];

        LOG4CPLUS_INFO(getLogger(), "Writing " << prop << " = " << value);

        if (prop == MassiveWriteRequest::PROP_VTH1) {
            vth1 = value;
            pvth1 = &vth1;
        } else if (prop == MassiveWriteRequest::PROP_VTH2) {
            vth2 = value;
            pvth2 = &vth2;
        } else if (prop == MassiveWriteRequest::PROP_VMON1) {
            vmon1 = value;
            pvmon1 = &vmon1;
        } else if (prop == MassiveWriteRequest::PROP_VMON2) {
            vmon2 = value;
            pvmon2 = &vmon2;
        } else if (prop == MassiveWriteRequest::PROP_VTH3) {
            vth3 = value;
            pvth3 = &vth3;
        } else if (prop == MassiveWriteRequest::PROP_VTH4) {
            vth4 = value;
            pvth4 = &vth4;
        } else if (prop == MassiveWriteRequest::PROP_VMON3) {
            vmon3 = value;
            pvmon3 = &vmon3;
        } else if (prop == MassiveWriteRequest::PROP_VMON4) {
            vmon4 = value;
            pvmon4 = &vmon4;
        } else {
            string msg = "Illegal property '" + prop + "'";
            LOG4CPLUS_ERROR(getLogger(), msg);
            XCEPT_RAISE(xoap::exception::Exception, msg);
        }
    }

    if (febWriteReadBack) {
        feb.writeRead(pvth1, pvth2, pvmon1, pvmon2);

        if (pvth3 != 0 || pvth4 != 0 || pvmon3 != 0 || pvmon4 != 0) {
            FebEndcap& febEndcap = dynamic_cast<FebEndcap&> (feb);
            febEndcap.writeRead2(pvth3, pvth4, pvmon3, pvmon4);
        }
    } else {
        feb.write(pvth1, pvth2, pvmon1, pvmon2);

        if (pvth3 != 0 || pvth4 != 0 || pvmon3 != 0 || pvmon4 != 0) {
            FebEndcap& febEndcap = dynamic_cast<FebEndcap&> (feb);
            febEndcap.write2(pvth3, pvth4, pvmon3, pvmon4);
        }
    }

    // set read values in the values vector
    for (Props::size_type i = 0; i < properties.size(); i++) {
        string prop = (string) properties[i];
        if (prop == MassiveWriteRequest::PROP_VTH1) {
            values[i] = vth1;
        } else if (prop == MassiveWriteRequest::PROP_VTH2) {
            values[i] = vth2;
        } else if (prop == MassiveWriteRequest::PROP_VMON1) {
            values[i] = vmon1;
        } else if (prop == MassiveWriteRequest::PROP_VMON2) {
            values[i] = vmon2;
        } else if (prop == MassiveWriteRequest::PROP_VTH3) {
            values[i] = vth3;
        } else if (prop == MassiveWriteRequest::PROP_VTH4) {
            values[i] = vth4;
        } else if (prop == MassiveWriteRequest::PROP_VMON3) {
            values[i] = vmon3;
        } else if (prop == MassiveWriteRequest::PROP_VMON4) {
            values[i] = vmon4;
        } else {
            string msg = "Illegal property '" + prop + "'";
            LOG4CPLUS_ERROR(getLogger(), msg);
            XCEPT_RAISE(xoap::exception::Exception, msg);
        }
    }
}
