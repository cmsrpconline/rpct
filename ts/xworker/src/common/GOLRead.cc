#include "rpct/ts/worker/GOLRead.h"

#include "xcept/tools.h"

#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellAbstract.h"

#include "ts/exception/CellException.h"
   

rpcttsworker::GOLRead::GOLRead(log4cplus::Logger& log, tsframework::CellAbstractContext* context)
  :tsframework::CellCommand(log,context)
{
  logger_ = log4cplus::Logger::getInstance(log.getName() +".GOLRead");

}

void rpcttsworker::GOLRead::code(){

  LOG4CPLUS_INFO(getLogger(), "Starting GOLRead::code() method");
  
  *dynamic_cast<xdata::String*>(payload_) = "??";

}
