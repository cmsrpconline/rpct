#include "rpct/ts/worker/LoggerCellContext.h"
#include "ts/exception/CellException.h"
// #include "ts/toolbox/CellToolbox.h"
#include "ts/framework/CellAbstract.h"


#include <string>
#include <cstdlib>

#include "toolbox/string.h"

#include "log4cplus/logger.h"

#include "rpct/ts/worker/panels/LogMessageQueue.h"


rpcttsworker::LoggerCellContext::LoggerCellContext(log4cplus::Logger& log, 
        tsframework::CellAbstract* cell)
: tsframework::CellAbstractContext(log, cell)
{

    appLogger_ = &cell->getApplicationLogger();

    lQueue_ = log4cplus::SharedAppenderPtr( new rpcttsworker::LogMessageQueue(cell->getApplicationLogger().getName(), 500) );

    LOG4CPLUS_INFO(log, "LoggertCellContext constructor end");
}
rpcttsworker::LoggerCellContext::~LoggerCellContext()
{ 
    ;
}

std::string 
rpcttsworker::LoggerCellContext::getClassName()
{
    return "CellContext";
}


