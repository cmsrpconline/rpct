/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Ildefons Magrans de Abril	                                 *
 *************************************************************************/

#include "rpct/ts/worker/LBWorkerConfiguration.h"
#include "rpct/ts/worker/LBCellContext.h"
#include "rpct/ts/worker/xdaqlboxaccess/XdaqLBoxAccess.h"
#include "rpct/xdaqutils/LogUtils.h"

#include "ts/toolbox/CellToolbox.h"
#include "ts/exception/CellException.h"

#include "rpct/ts/worker/panels/HistogramsWidget.h"
#include "rpct/ts/worker/panels/MonitorablesWidget.h"

#include "rpct/ts/worker/FebConfiguration.h"

#include "xdata/xdata.h"

#include "toolbox/string.h"
#include "xdaq/NamespaceURI.h"

#include "log4cplus/logger.h"

#include <iostream>

using namespace tsexception;
using namespace tsframework;

namespace rpcttsworker {

LBWorkerConfiguration::LBWorkerConfiguration(log4cplus::Logger& log,
        tsframework::CellAbstractContext* context) :
                rpcttsworker::RpctWorkerConfiguration(log,context){
  logger_ =  log4cplus::Logger::getInstance(getLogger().getName() + ".Configuration");

  context_ = dynamic_cast<LBCellContext*>(context);


  getParamList()["enable Hard Reset by TTC"] = new xdata::Boolean(true);
  getParamList()["enable opt links out"] = new xdata::Boolean(true);

  getParamList()["force_load_firmware"] = new xdata::Boolean(true);
  getParamList()["force_reset_setup"] = new xdata::Boolean(true);
  getParamList()["force_setup_boards"] = new xdata::Boolean(true);
}



void LBWorkerConfiguration::configure() { //TODO
    LOG4CPLUS_INFO(getLogger(), "Executing LBWorkerConfiguration::configure method...");
    
    context_->getFebConfiguration().getProgress().terminate();
    LOG4CPLUS_INFO(getLogger(), "Executing LBWorkerConfiguration::configure line "<<__LINE__);
    context_->getFebConfiguration().getProgress().resume();
    LOG4CPLUS_INFO(getLogger(), "Executing LBWorkerConfiguration::configure line "<<__LINE__);
    XdaqLBoxAccess* lboxAccess = context_->getLBoxAccess();
    LOG4CPLUS_INFO(getLogger(), "Executing LBWorkerConfiguration::configure line "<<__LINE__);
    std::string configKey = getParamList()["Configuration Key"]->toString();
    LOG4CPLUS_INFO(getLogger(), "Executing LBWorkerConfiguration::configure line "<<__LINE__);

    bool enableHardResetByTTC = ((xdata::Boolean*)getParamList()["enable Hard Reset by TTC"])->value_;
    bool enableOptLinksOut = ((xdata::Boolean*)getParamList()["enable opt links out"])->value_;

    bool forceLoadFirmware = getParamList()["force_load_firmware"];
    bool forceResetSetup = getParamList()["force_reset_setup"];
    bool forceSetupBoards = getParamList()["force_setup_boards"];
    LOG4CPLUS_INFO(getLogger(), "Executing LBWorkerConfiguration::configure line "<<__LINE__<< ". enableHardResetByTTC "
            <<enableHardResetByTTC<<" enableOptLinksOut "<<enableOptLinksOut);
    lboxAccess->configure(configKey, enableHardResetByTTC, enableOptLinksOut, forceLoadFirmware,forceResetSetup,forceSetupBoards);
    LOG4CPLUS_INFO(getLogger(), "Executing LBWorkerConfiguration::configure line "<<__LINE__);
    context_->getFebConfiguration().loadConfiguration("FEBS_DEFAULT", true, true);
    LOG4CPLUS_INFO(getLogger(), "Executing LBWorkerConfiguration::configure line "<<__LINE__);
    LOG4CPLUS_INFO(getLogger(), "LBWorkerConfiguration::configure method finished");
    LOG4CPLUS_INFO(getLogger(), "Executing LBWorkerConfiguration::configure line "<<__LINE__);
    setResult("CONFIGURE transition executed");
}

void LBWorkerConfiguration::synchronize_links() {
    LOG4CPLUS_INFO(getLogger(), "Executing LBWorkerConfiguration::synchronize_links method...");
    context_->getLBoxAccess()->synchronizeLinks();
    setResult("SYNCHRONIZE_LINKS transition executed");
}



void LBWorkerConfiguration::start() {
  LOG4CPLUS_INFO(getLogger(), "Executing LBWorkerConfiguration::start method...");

  context_->getFebConfiguration().getProgress().terminate();
  context_->getFebConfiguration().getProgress().resume();
  context_->getLBoxAccess()->setRunNumber(*dynamic_cast<xdata::UnsignedLong*>(getParamList()["Run Number"]));
  context_->getLBoxAccess()->enable();
  setResult("START transition executed");
    
}



void LBWorkerConfiguration::pause() {
    LOG4CPLUS_INFO(getLogger(), "Executed LBWorkerConfiguration::pause method...");
    //context_->getLBoxAccess()->disable();

    setResult("PAUSE transition executed");

}

void LBWorkerConfiguration::resume() {
  LOG4CPLUS_INFO(getLogger(), "Executing LBWorkerConfiguration::resume method...");

  context_->getLBoxAccess()->enable();
  setResult("RESUME transition executed");
    
}

void LBWorkerConfiguration::stop() {
    LOG4CPLUS_INFO(getLogger(), "Executing LBWorkerConfiguration::stop method...");
    context_->getFebConfiguration().getProgress().terminate();
    context_->getFebConfiguration().getProgress().resume();
    context_->getFebSystem().stop();
    context_->getLBoxAccess()->disable();
    setResult("STOP transition executed");

}


void LBWorkerConfiguration::coldReset(){

}

void LBWorkerConfiguration::resetting() {
    LOG4CPLUS_INFO(getLogger(), "Executing LBWorkerConfiguration::resetting method...");
    context_->getFebConfiguration().getProgress().terminate();
    context_->getFebConfiguration().getProgress().resume();
    context_->getFebSystem().stop();
    context_->getLBoxAccess()->disable();
    context_->getLBoxAccess()->reset();

    getFSM().reset();
    LOG4CPLUS_INFO(getLogger(), "Executing LBWorkerConfiguration::resetting method done");
}

void LBWorkerConfiguration::setResult(const std::string& value) {
  LOG4CPLUS_INFO(XdaqHardwareAccess::getSummaryLogger(), value);
  tsruncontrol::ConfigurationBase::setResult(value);
}


}
