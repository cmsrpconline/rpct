/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Krzysztof Niemkiewicz	                                 *
 *                                                                       *
 * Responsible: No one					                                 *
 *************************************************************************/

#include "rpct/ts/worker/TBWorkerConfiguration.h"
#include "rpct/ts/worker/XdaqTCrateAccess.h"
#include "rpct/xdaqutils/LogUtils.h"

#include "ts/toolbox/CellToolbox.h"
#include "ts/exception/CellException.h"

#include "rpct/ts/worker/panels/HistogramsWidget.h"
#include "rpct/ts/worker/panels/MonitorablesWidget.h"

#include "xdata/xdata.h"

#include "toolbox/string.h"

#include "log4cplus/logger.h"

#include <iostream>

using namespace tsexception;
using namespace tsframework;

namespace rpcttsworker {

TBWorkerConfiguration::TBWorkerConfiguration(log4cplus::Logger& log,
		tsframework::CellAbstractContext* context) :
        	rpcttsworker::RpctWorkerConfiguration(log,context){
  logger_ =  log4cplus::Logger::getInstance(getLogger().getName() + ".Configuration");

  context_ = dynamic_cast<TBCellContext*>(context);
}



void TBWorkerConfiguration::configure() {
    LOG4CPLUS_INFO(getLogger(), "Executing TBWorkerConfiguration::c_configure method...");
    
    context_->getTCrateAccess()->configure(getParamList()["Configuration Key"]->toString(), false, true, true);

    setResult("CONFIGURE transition executed");
}


void TBWorkerConfiguration::synchronize_links() {
    LOG4CPLUS_INFO(getLogger(), "Executed TBWorkerConfiguration::synchronize_links method...");

    setResult("SYNCHRONIZE_LINKS transition executed");
}




void TBWorkerConfiguration::start() {
    LOG4CPLUS_INFO(getLogger(), "Executed TBWorkerConfiguration::start method...");
    
    context_->getTCrateAccess()->setRunNumber(*dynamic_cast<xdata::UnsignedLong*>(getParamList()["Run Number"]));
    context_->getTCrateAccess()->enable();

    setResult("START transition executed");
}


void TBWorkerConfiguration::pause() {
    LOG4CPLUS_INFO(getLogger(), "Executed TBWorkerConfiguration::pause method...");

    setResult("PAUSE transition executed");

}

void TBWorkerConfiguration::resume() {
    LOG4CPLUS_INFO(getLogger(), "Executed TBWorkerConfiguration::resume method...");
    
    context_->getTCrateAccess()->enable();

    setResult("RESUME transition executed");
}

void TBWorkerConfiguration::stop() {
    LOG4CPLUS_INFO(getLogger(), "Executed TBWorkerConfiguration::stop method...");
    context_->getTCrateAccess()->disable();
    setResult("STOP transition executed");

}

void TBWorkerConfiguration::coldReset(){

}

void TBWorkerConfiguration::resetting() {
    LOG4CPLUS_INFO(getLogger(), "Executing TBWorkerConfiguration::resetting method...");
    context_->getMonitorablesWidget()->stopMonitoring();
    context_->getHistogramsWidget()->stop();
    getFSM().reset();
}

void TBWorkerConfiguration::setResult(const std::string& value) {
  LOG4CPLUS_INFO(XdaqHardwareAccess::getSummaryLogger(), value);
  tsruncontrol::ConfigurationBase::setResult(value);
}


}

