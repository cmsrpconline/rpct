/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Ildefons Magrans de Abril	                         *
 *************************************************************************/

#include "rpct/ts/worker/TBCell.h"
#include "rpct/xdaqutils/LogUtils.h"

#include "ts/framework/CellOperationFactory.h"
#include "ts/framework/CellFactory.h"
#include "ts/framework/CellPanelFactory.h"

//#include "rpct/ts/worker/HelloWorld.h"

#include "rpct/ts/worker/TBWorkerConfiguration.h"
//#include "rpct/ts/worker/PushMON.h"
#include "rpct/ts/worker/ResetRMB.h"

#include "rpct/ts/worker/panels/MessagesPanel.h"
#include "rpct/ts/worker/panels/HistogramPanel.h"
//#include "rpct/ts/worker/panels/GOLPanel.h" //TODO uncomment when ready

#include "xcept/tools.h"

#include "rpct/ts/worker/SendAlarm.h"

XDAQ_INSTANTIATOR_IMPL(rpcttsworker::TBCell)

rpcttsworker::TBCell::TBCell(xdaq::ApplicationStub * s) :
    tsframework::CellAbstract(s) {
    LOG4CPLUS_INFO(getApplicationLogger(), "TB worker cell constructor");

    try {
        cellContext_ = new rpcttsworker::TBCellContext(getApplicationLogger(), this);
        rpct::rpcLogSetup(getApplicationLogger(),this);
    } catch (xcept::Exception& e) {
        std::ostringstream msg;
        msg
                << "Error instantiating the CellContext. Find the bug and restart the executive. Exception found: ";
        msg << xcept::stdformat_exception_history(e);
        LOG4CPLUS_ERROR(getApplicationLogger(),msg.str());
    }
    LOG4CPLUS_INFO(getApplicationLogger(), "Worker cell constructor exit");
}

void rpcttsworker::TBCell::init() {
    LOG4CPLUS_INFO(getApplicationLogger(), "Worker cell init");

    tsframework::CellOperationFactory* opF = getContext()->getOperationFactory();
    opF->add<rpcttsworker::TBWorkerConfiguration>("Run Control");
    tsframework::CellFactory* cmdF = getContext()->getCommandFactory();
    cmdF->add<rpcttsworker::SendAlarm>("Test","SendAlarm");
    cmdF->add<rpcttsworker::ResetRMB>("TC","ResetRMB");
    tsframework::CellPanelFactory* panelF = getContext()->getPanelFactory();
    panelF->add<rpcttspanels::MessagesPanel> ("Monitoring");
    panelF->add<rpcttspanels::HistogramPanel> ("Histograms");
    //panelF->add<rpcttspanels::GOLPanel> ("GOLs"); //TODO uncomment when ready
    LOG4CPLUS_INFO(getApplicationLogger(), "Worker cell init exit");

	
}


