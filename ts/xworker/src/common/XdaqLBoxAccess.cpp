/*
 *  Authors: Michal Pietrusinski, Mikolaj Cwiok, Karol Bunkowski, Krzysztof Niemkiewicz
 *  Version: $Id: XdaqLBoxAccess.cpp, v 3000 2009/10/11 13:20:58 tb Exp $
 *
 */
#include "rpct/ts/worker/xdaqlboxaccess/XdaqLBoxAccess.h"
#include "rpct/ts/worker/xdaqlboxaccess/LinkBoxSoapInterface.h"
#include "rpct/ts/worker/xdaqlboxaccess/HardwareItemException.h"
#include "rpct/ts/worker/xdaqlboxaccess/FecStatusResponse.h"
#include "rpct/hardwareTests/FEBsTimingTester.h"
#include "rpct/diag/IDiagnosable.h"
#include "rpct/devices/cb.h"
#include "rpct/devices/test/cbMock.h"
#include "rpct/devices/XmlSystemBuilder.h"
#include "rpct/devices/System.h"
#include "rpct/devices/LinkBoard.h"
#include "rpct/devices/LinkBox.h"
#include "rpct/devices/ConfigurationSetImpl.h"
#include "rpct/devices/SynCoderSettingsImpl.h"
#include "rpct/ii/ConfigurationFlags.h"
#include "rpct/lboxaccess/FecManagerImpl.h"
#include "rpct/xdaqutils/XdaqDbServiceClient.h"
#include "rpct/xdaqutils/XdaqDbSystemBuilder.h"
#include "rpct/std/NullPointerException.h"
#include "rpct/std/IllegalArgumentException.h"
#include "rpct/xdaqutils/MonitorableStatusInfo.h"
#include "rpct/xdaqutils/LogUtils.h"
#include "rpct/xdaqutils/XdaqCommonUtils.h"
#include "rpct/lboxaccess/FecManagerImpl.h"
#include "rpct/devices/FixedHardwareSettings.h"

#include "rpct/ts/worker/AsyncSoapCallProxy.h"

#include "rpct/ts/worker/panels/HistogramsWidget.h"
#include "rpct/ts/worker/panels/MonitorablesWidget.h"

#include "xdata/Serializable.h"
#include "xdata/exception/Exception.h"
#include "xdata/XStr.h"
#include "xdata/Float.h"
#include "xdata/Bag.h"
#include "xdata/Boolean.h"
#include "xdata/Integer.h"
#include "xdata/TableIterator.h"
#include "xdata/InfoSpaceFactory.h"

#include "xoap/DOMParser.h"
#include "xoap/domutils.h"
#include "xcept/tools.h"

#include "TH1.h"
#include "TFile.h"
#include "TTree.h"
#include "TStyle.h"

#include <time.h>

#include <boost/shared_ptr.hpp>

#include <log4cplus/configurator.h>
#include <log4cplus/fileappender.h>


// shortcut for binding a method via AsyncSoapCallProxy
// should be used only in cell constructor
#define LBOX_ASYNC_BIND(method,name) (new AsyncSoapCallProxy<XdaqLBoxAccess>(app_,this,(method)))->bindMe((name), RPCT_LBOX_ACCESS_NS)

using namespace rpct;
using namespace rpct::xdaqdiagaccess;
using namespace rpct::xdaqutils;
using namespace std;
using namespace toolbox;

// moved from headers as they caused linker errors

const char* XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS = "urn:rpct-lbox-access:1.0";
const char* XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX = "rla";


const char* XdaqLBoxAccess::LOAD_FROM_CCU_NAME = "loadFromCcu";
const char* XdaqLBoxAccess::LOAD_FROM_FLASH_NAME = "loadFromFlash";
const char* XdaqLBoxAccess::PROGRAM_FLASH_NAME = "programFlash";
const char* XdaqLBoxAccess::READ_LBOX_FIRMWARE_VER_NAME = "readlboxfirmwarever";
const char* XdaqLBoxAccess::RESET_FEC_NAME = "resetFec";
const char* XdaqLBoxAccess::APPLY_DB_MASKS_NAME = "applyDbMasks";


XdaqLBoxAccess::XdaqLBoxAccess(xdaq::Application * app,
                               rpct::MonitorablesWidget* monitorablesWigdet,
                               rpcttspanels::HistogramsWidget* histogramsWidget) :
    XdaqHardwareAccess(app,monitorablesWigdet,histogramsWidget),
    lbStatisticDiagManager_(0), resetMutex_(toolbox::BSem::FULL), badResetFecLastTime_(0) {

    subsytems_.push_back(XdaqDbServiceClient::SUBSYSTEM_LBS);
    subsytems_.push_back(XdaqDbServiceClient::SUBSYSTEM_RBCS_TTUS);
    subsytems_.push_back(XdaqDbServiceClient::SUBSYSTEM_FEBS);
    
    runNumber_ = 0;
    getLogger().setLogLevel(log4cplus::INFO_LOG_LEVEL);
    getSummaryLogger().setLogLevel(log4cplus::INFO_LOG_LEVEL);

    //FecManagerImpl::getLogger().setLogLevel(log4cplus::DEBUG_LOG_LEVEL);

    LOG4CPLUS_INFO(getLogger(),"Hello World!");
    LOG4CPLUS_INFO(getLogger(), "compilation path: "<<__FILE__);
    LOG4CPLUS_INFO(getLogger(), "compilation date: "<<__DATE__<<" "<<__TIME__);
    LOG4CPLUS_INFO(getLogger(), "$Revision: 1445 $");
    
    // ********** DiagAccess ************

    rpct::bind(app, xdaqDiagAccess_, &XdaqDiagAccess::configureDiagnosable,
            XdaqDiagAccess::CONFIGURE_DIAGNOSABLE_NAME, XdaqDiagAccess::RPCT_DIAG_ACCESS_NS);

    rpct::bind(app, xdaqDiagAccess_, &XdaqDiagAccess::configureDiagnosableLB,
            XdaqDiagAccess::CONFIGURE_DIAGNOSABLE_LB_NAME, XdaqDiagAccess::RPCT_DIAG_ACCESS_NS);

    rpct::bind(app, xdaqDiagAccess_, &XdaqDiagAccess::configureDiagnosticReadoutLB,
         XdaqDiagAccess::CONFIGURE_DIAGNOSTIC_READOUT_LB_NAME,
         XdaqDiagAccess::RPCT_DIAG_ACCESS_NS);


    // LBOX & FEC configuration
    xoap::bind(this, &XdaqLBoxAccess::onResetFec, RESET_FEC_NAME, RPCT_LBOX_ACCESS_NS);

    xoap::bind(this, &XdaqLBoxAccess::onApplyDBMasks, APPLY_DB_MASKS_NAME, RPCT_LBOX_ACCESS_NS);

    LBOX_ASYNC_BIND(&XdaqLBoxAccess::onLoadFromFlash,LOAD_FROM_FLASH_NAME);
    LBOX_ASYNC_BIND(&XdaqLBoxAccess::onLoadFromCcu,LOAD_FROM_CCU_NAME);

    //xdaqDbServiceClient_ = new XdaqDbServiceClient(app_);

    G_Log.out(cout);
    G_Log.out() << hex;


    app_->getApplicationInfoSpace()->fireItemAvailable("systemFileName", &systemFileName);

    ringName = "unknown";
    xringName = ringName;
    app_->getApplicationInfoSpace()->fireItemAvailable("ringName", &xringName);
    enableTTCReload_ = false;
    app_->getApplicationInfoSpace()->fireItemAvailable("enableTTCReload", &enableTTCReload_);
    configureFromFlash_ = false;
    app_->getApplicationInfoSpace()->fireItemAvailable("configureFromFlash", &configureFromFlash_);


    try {
    	initialise();
    	FUNCTION_INFO(" created");
    }
    catch (xcept::Exception& e) {
    	std::ostringstream msg;
    	msg << "error during XdaqLBoxAccess::initialise(): ";
    	msg << xcept::stdformat_exception_history(e);
    	FUNCTION_ERR("", msg.str());
    	//LOG4CPLUS_ERROR(logger_, msg.str());
    	initializationErrorStr_ = msg.str();
    }

    string monitorHistosPath = "/rpctdata/LBMonitorHistos";
    char* tmp = getenv("LB_MONITOR_HISTOS_PATH");
    if (tmp != 0) {
    	monitorHistosPath =  tmp;
    	LOG4CPLUS_INFO(this->getLogger(), "setting up the monitorHistosPath from the environment LB_MONITOR_HISTOS_PATH to "<<monitorHistosPath);
    }
    else {
    	LOG4CPLUS_INFO(this->getLogger(), "setting up the monitorHistosPath to "<<monitorHistosPath);
    }

    std::string filenamePrefix = monitorHistosPath = monitorHistosPath + std::string("/Histos_") + towerName;
    histogramsWidget_->initialise(filenamePrefix, "link board histograms");

    // creates and binds soap interface. We never delete it, so it is not even stored
    
    new LinkBoxSoapInterface(app, this, &hardwareMutex_);
}

XdaqLBoxAccess::~XdaqLBoxAccess() {
	if(lbStatisticDiagManager_)
		delete lbStatisticDiagManager_;
	if(febsystem_)
		delete febsystem_;
	if(system_)
		delete system_;
}

void XdaqLBoxAccess::enable() { //TODO remove when the I2C intit is in the firmware
	/*{//scope pfor mutex
		rpct::MutexHandler handler(hardwareMutex_);
		getLinkSystem()->initI2CinControlBoards(getLinkSystem()->getControlBoards());
	}*/
	XdaqHardwareAccess::enable();
}

void XdaqLBoxAccess::testControlChannel() {
    FUNCTION_INFO(": started");
    // hardwareMutex_.take(); // mutexing is already by testFEBsTiming (changed by MC - 16 Dec 2009)
    try {
        time_t timerStart = time(NULL);
        int repetitions = 1000; // 1000
        int operations = 0;
        const LinkSystem::LinkBoards& LBs = getLinkSystem()->getLinkBoards();
        for(int i = 0; i < repetitions; i++) {
            if(i%1000 == 0) LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<"  iter=" << i << ", operations="<<operations);
            for (unsigned int iLB = 0; iLB < LBs.size(); iLB++) {
                LBs[iLB]->getSynCoder().writeWord(rpct::SynCoder::WORD_USER_REG1, 0, 0x1234); operations++;
                //linkBoards_[iLB]->getSynCoder().readWord(rpct::SynCoder::WORD_USER_REG2, 0); operations++;
            }
        }
        time_t timerStop = time(NULL);
        time_t dif = (time_t) difftime(timerStop, timerStart);
        LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<" iterations="<<repetitions<<", operations="<<operations<<" time "<<dif <<" seconds");
    } catch (xcept::Exception& e) {
        FUNCTION_ERR("", e.what());
        throw e;
    } catch (std::exception& e) {
        FUNCTION_ERR("", e.what());
        throw e;
    } catch (...) {
        FUNCTION_ERR("", "unknown error");
        throw ;
    }
    FUNCTION_INFO(": done");
}

void XdaqLBoxAccess::synchronizeLinks(rpct::LinkSystem::LinkBoards linkBoards, rpct::LinkSystem::RbcBoards rbcBoards) {
    FUNCTION_INFO(" started for "<<linkBoards.size()<<" LBs and "<<rbcBoards.size()<<" RBCs");
    rpct::MutexHandler handler(hardwareMutex_);

    if (histogramsWidget_->isActive()) {
        FUNCTION_WARN("", "Histogram monitoring is running!");
    }
    if (monitorablesWidget_->isMonitoringOn()) {
        FUNCTION_WARN("", "Monitoring is running!");
    }
    getLinkSystem()->synchronizeLinks(linkBoards, rbcBoards);
    FUNCTION_INFO(" done");
}

void XdaqLBoxAccess::synchronizeLinks() {
    LOG4CPLUS_DEBUG(getLogger(), __FUNCTION__<<": started for ALL links");
    try {
    	rpct::MutexHandler handler(hardwareMutex_);
    	getLinkSystem()->synchronizeLinks();
    }
    catch (std::exception& e) {
        ALARM_STD(app_,"FATAL",e);
        FUNCTION_ERR("", e.what());
        monitorablesWidget_->handleMonitorException(e, "Exception during synchronizeLinks: ");
        monitorablesWidget_->analyseAndGenerateAlarms();
        XCEPT_RAISE(xcept::Exception, e.what());
    }
    catch (...) {
        ALARM_OTHER(app_,"FATAL","configure failed, unknown exception");
        FUNCTION_ERR("configure failed, unknown exception", "");
        std::exception e;
        monitorablesWidget_->handleMonitorException(e, "Unknown c++ exception during synchronizeLinks: ");
        monitorablesWidget_->analyseAndGenerateAlarms();
        XCEPT_RAISE(xcept::Exception,"configure failed, unknown exception");
    }
    LOG4CPLUS_DEBUG(getLogger(), __FUNCTION__<<": done for ALL links");
}

FecManager& XdaqLBoxAccess::getFecManager() {
    LinkSystem::FecManagerList& fecManagerList = getLinkSystem()->getFecManagers();
    if (fecManagerList.empty()) {
    	XCEPT_RAISE(xcept::Exception,"No ccu ring found in the system");
    }

    FecManager* fm = fecManagerList.front();
    if (fm == 0) {
    	XCEPT_RAISE(xcept::Exception,"No fecManager found");
    }
    return *fm;
}


void XdaqLBoxAccess::initialise() {
    if (system_ == 0) {
        buildSystem();

        FecManager& fm = getFecManager();
        LOG4CPLUS_INFO(getLogger(), "Working on pciSlot = " << fm.getPciSlot()
                << " vmeSlot = " << fm.getVmeSlot() << " ringNumber = " << fm.getRingNumber());

        if (getLinkSystem()->getCrates().empty()) {
        	XCEPT_RAISE(xcept::Exception,"No lbox found");
        }

        ostringstream str;
        string crateName = getLinkSystem()->getCrates().front()->getDescription();
        //      string crateName("LBB_YEP1_S10");
        //      string crateName("PASTEURA_LBB");

        const string LBB_PREFIX("LBB_");
        const string LBB_SUFFIX("_LBB");
        if (crateName.find(LBB_PREFIX, 0) == 0) {
            string::size_type s = crateName.find("_", LBB_PREFIX.size() + 1);
            if (s != string::npos) {
                str << crateName.substr(LBB_PREFIX.size(), s - LBB_PREFIX.size());
                s = crateName.find("S", s + 1);
                istringstream istr(crateName.substr(s + 1));
                int sector;
                istr >> sector;
                str << ((sector < 4 || sector > 9) ? " near " : " far ");
            }
        } else if (crateName.find(LBB_SUFFIX, crateName.size() - LBB_SUFFIX.size()) != string::npos) {
            string::size_type s = crateName.find(LBB_SUFFIX);
            if (s != string::npos) {
                str << crateName.substr(0, s) << " ";
            }
        } else {
            str << crateName << " "; // have no LBB_ prefix or _LBB suffix
        }
        towerName = str.str();

		while (towerName[towerName.size() - 1] == ' ') { // this should usually do at most one iteration
		  towerName = towerName.substr(0, towerName.size() - 1);
		}

		for (unsigned int i = 0; i < towerName.size(); i++) {
			if (towerName[i] == ' ')
				towerName[i] = '_';
		}

		LOG4CPLUS_INFO(getLogger(), "towerName = " << towerName) ;

        partitionName_ = towerName;
        str << "(pci=" << fm.getPciSlot() << ", vmeSlot=" << fm.getVmeSlot() << ", ring="
                << fm.getRingNumber() << ")";
        ringName = str.str();
        xringName = ringName;

        monitorablesWidget_->setPartitionName(partitionName_);
        monitorablesWidget_->initialise(getLinkSystem(), &hardwareMutex_, true, febsystem_);

        XdaqDiagAccess::Diagnosables diagnosables;
        System::CrateList& sysc = getLinkSystem()->getCrates();
        for (System::CrateList::iterator iCrate = sysc.begin(); iCrate != sysc.end(); ++iCrate) {
            ICrate* crate = *iCrate;
            typedef ICrate::Boards Boards;
            Boards& boards = crate->getBoards();

            std::sort(boards.begin(), boards.end(), LinkBox::boardSortPredicate);

            for (Boards::iterator iBoard = boards.begin(); iBoard != boards.end(); ++iBoard) {
                typedef IBoard::Devices Devices;
                IBoard* board = (*iBoard);
                
                const Devices& devices = board->getDevices();
                for (Devices::const_iterator iDevice = devices.begin(); iDevice != devices.end(); ++iDevice) {
                    IDiagnosable* diagnosable = dynamic_cast<IDiagnosable*> (*iDevice);
                    if (diagnosable != 0) {
                        diagnosables.push_back(diagnosable);
                    }
                }
            }
        }

        xdaqDiagAccess_->clearDiagnosables();
        xdaqDiagAccess_->mapDiagnosables(diagnosables);
        xdaqIIAccess_ = new XdaqIIAccess(app_, *getLinkSystem());

        string picturesDir = "/tmp";
        LOG4CPLUS_DEBUG(getLogger(), "Creating LBStatisticDiagManager");

        getLinkSystem()->setTowerName(towerName);
        getLinkSystem()->readLbChamberMap(); //TODO set it from the DB???
        if (febsystem_)
            febsystem_->setDescription(towerName);

        lbStatisticDiagManager_ = new LBStatisticDiagManager(towerName, picturesDir, getLinkSystem()->getLinkBoards(), getLinkSystem()->getHalfBoxes(), true, true);
        histogramsWidget_->addManager("LB",lbStatisticDiagManager_);
       // lbStatisticDiagManager_->setStopHistoPtr(&stopHisto_); // TODO // check if this is still needed - MC, 3 Mar 2010
        LOG4CPLUS_DEBUG(getLogger(), "Creating LBStatisticDiagManager done");
    }
}


void XdaqLBoxAccess::buildSystem() {
    LOG4CPLUS_INFO(getLogger(), "System building started");

    if (system_ != 0) {
        // probably there was exception during previous configuration attempt
        // clear everything

        LOG4CPLUS_WARN(getLogger(), "Previously configured: starting from scratch");
        reset();
    }

    system_ = new LinkSystem();
    LinkSystem * ls = dynamic_cast<LinkSystem *>(system_);
    if (ls) {
        febsystem_ = new rpct::xdaqutils::xdaqRPCFebSystem(*ls, *app_, hardwareMutex_);
    }

    try {
        if (((string) systemFileName).empty()) {
            LOG4CPLUS_INFO(getLogger(), "Configuring system from DB ");
            string host;
            int port;
            if (port_ != 0) {
                host = (string) host_;
                port = (int) port_;
            } else {
                string url = app_->getApplicationDescriptor()->getContextDescriptor()->getURL();
                string httpStr = "http://";
                string::size_type posHttp = url.find(httpStr);
                string::size_type posPort = url.find_last_of(":");
                host = (posHttp == string::npos) ? url.substr(0, posPort) : url.substr(
                        httpStr.length(), posPort - httpStr.length());
                string sPort = url.substr(posPort + 1);
                istringstream istr(sPort);
                istr >> port;
            }
            LOG4CPLUS_INFO(getLogger(), "Host: " << host << " port " << port);

            //TODO(KN): use loop here 
            try {
            	LOG4CPLUS_WARN(getLogger(), "Mock : " << mock_);
            	XdaqDbSystemBuilder(app_, mock_).buildCcuRing(*getLinkSystem(), host, port,
                                                            app_->getApplicationDescriptor()->getInstance(), febsystem_);
            }
            catch(CCUExceptionRingInUse& e) {
            	LOG4CPLUS_WARN(getLogger(), "CCUException during XdaqLBoxAccess::buildSystem() : "<<e.what()<<". System not created");
            	throw;
            }
            catch(CCUException& e) {
            	LOG4CPLUS_WARN(getLogger(), "CCUException during XdaqLBoxAccess::buildSystem() : "<<e.what()<<". Reseting FEC and trying XdaqLBoxAccess::buildSystem() one more time");
            	resetFec(NULL, false);
            	if (system_ != 0) {
            	    delete febsystem_;
            	    febsystem_ = 0;
            	    LOG4CPLUS_INFO(getLogger(), "febsystem_ deleted");
            	    delete system_;
            	    system_ = 0;
            	    LOG4CPLUS_INFO(getLogger(), "system_ deleted");
            	}
            	system_ = new LinkSystem();
                ls = dynamic_cast<LinkSystem *>(system_);
                if (ls)
                    febsystem_ = new rpct::xdaqutils::xdaqRPCFebSystem(*ls, *app_, hardwareMutex_);
            	XdaqDbSystemBuilder(app_, mock_).buildCcuRing(*getLinkSystem(), host, port,
                                                              app_->getApplicationDescriptor()->getInstance(), febsystem_);
            	LOG4CPLUS_WARN(getLogger(), "Success now");
            }
            ls->registerFebSystem(febsystem_);

        }/* else { //obsolete
            LOG4CPLUS_INFO(getLogger(), "Configuring system from file " << ((string)systemFileName));

            XmlSystemBuilder(*system, mock_).build((string) systemFileName);
        }*/
    } catch (std::exception& e) {
        LOG4CPLUS_ERROR(getLogger(), e.what());
        //MSGS_ERROR(e.what());
    	monitorablesWidget_->handleMonitorException(e, "Exception during buildSystem:");
    	monitorablesWidget_->analyseAndGenerateAlarms();
    	//delete febsystem_;!!!!!!!!!!!!!!
    	febsystem_ = 0;
    	delete system_;
    	system_ = 0;
        XCEPT_RAISE(xcept::Exception, e.what());
    }
    catch (...) {
        LOG4CPLUS_ERROR(getLogger(), "Unknown c++ exception during buildSystem");
        //MSGS_ERROR("Unknown c++ exception");
        std::exception e;
        monitorablesWidget_->handleMonitorException(e, "Unknown c++ exception during buildSystem: ");
        monitorablesWidget_->analyseAndGenerateAlarms();
        //delete febsystem_;
        febsystem_ = 0;
        delete system_;
        system_ = 0;
        XCEPT_RAISE(xcept::Exception, "Unknown c++ exception during buildSystem");
    }
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": done");
}

/*void XdaqLBoxAccess::configure() {
    FUNCTION_INFO(": started");

    try {
        resetFec();


        try {
            if (!mock_) {
                LOG4CPLUS_INFO(getLogger(), "Checking versions");
                getLinkSystem()->checkVersions();
            }
        } catch (rpct::EBadDeviceVersion& e) {
            ostringstream ostr;
            ostr << "Some boards have bad device version: \n";
            for (EBadDeviceVersion::VersionInfoList::iterator iErr = e.getVersionInfoList().begin(); iErr
                    != e.getVersionInfoList().end(); ++iErr) {
                IHardwareItem* item = iErr->item;
                string name;
                if (dynamic_cast<IDevice*> (item) != 0) {
                    name = dynamic_cast<IDevice*> (item)->getBoard().getDescription() + "."
                            + item->getDescription();
                } else {
                    name = item->getDescription();
                }
                ostr << name << " " << iErr->element << " exp=" << iErr->expectedVal << " val="
                        << iErr->val << "\n";
            }
            MSGS_ERROR(ostr.str());
            LOG4CPLUS_ERROR(getLogger(), e.what());

            //////////////////////////////// commented out by MC - 1 aug 2008
            //

             LOG4CPLUS_ERROR(getLogger(), e.what());
             if (interactive_) {
             cout << "Press 'y' to continue" << endl;
             string str;
             cin >> str;
             if (str != "y") {
             throw;
             }
             }
             else {
             throw;
             }

            //
            //////////////////////////////// commented out by MC - 1 aug 2008
        }


    } catch (std::exception& e) {

    	ALARM_STD(app_,"FATAL",e);
        LOG4CPLUS_ERROR(getLogger(), e.what());
        MSGS_ERROR(e.what());
        throw ;
    }
    catch (...) {
    	ALARM_OTHER(app_,"FATAL","Unknown c++ exception");
        LOG4CPLUS_ERROR(getLogger(), "Unknown c++ exception");
        MSGS_ERROR("Unknown c++ exception");
        throw;
    }
    FUNCTION_INFO(": done");
}*/

vector<int> XdaqLBoxAccess::getChipIdsToSetup() {
    vector<int> chipIds;

    const System::DeviceMap& deviceMap = getLinkSystem()->getDeviceMap();
    LOG4CPLUS_INFO(getLogger(), "Device map size " << deviceMap.size());
    for (System::DeviceMap::const_iterator iDevice = deviceMap.begin(); iDevice != deviceMap.end(); ++iDevice) {
        if (dynamic_cast<SynCoder*> (iDevice->second) != 0) {
            chipIds.push_back(iDevice->second->getId());
            LOG4CPLUS_DEBUG(getLogger(), "Adding SynCoder chip id " << iDevice->second->getId());
        } else {
            LOG4CPLUS_DEBUG(getLogger(), "Skipping setup for " << iDevice->second->getId());
        }
    }

    const System::HardwareItemList& rbcs = getLinkSystem()->getHardwareItemsByType(Rbc::TYPE);
    for (System::HardwareItemList::const_iterator iRbc = rbcs.begin(); iRbc != rbcs.end(); ++iRbc) {
        Rbc* rbc = dynamic_cast<Rbc*> (*iRbc);
        if (rbc == 0) {
            LOG4CPLUS_ERROR(getLogger(), "Board " << (*iRbc)->getDescription() << " is not a Rbc");
        }
        chipIds.push_back(rbc->getRbcDevice().getId());
        LOG4CPLUS_DEBUG(getLogger(), "Adding RBC chip id " << rbc->getRbcDevice().getId());
    }

    return chipIds;
}

void XdaqLBoxAccess::loadFirmware(bool forceLoad, rpct::ConfigurationSet& confSet) {
    //TODO include forceLoad
    loadFromFlash();
}

void XdaqLBoxAccess::checkFirmware(rpct::ConfigurationSet& confSet) {
    LOG4CPLUS_INFO(getLogger(), "Checking firmware versions...");

    try {
        rpct::MutexHandler handler(hardwareMutex_);
        getLinkSystem()->checkVersions();
    }
    catch (rpct::EBadDeviceVersion& e) {
        ostringstream ostr;
        ostr << "Some boards have bad device version: \n";
        for (EBadDeviceVersion::VersionInfoList::iterator iErr = e.getVersionInfoList().begin(); iErr
        != e.getVersionInfoList().end(); ++iErr) {
            IHardwareItem* item = iErr->item;
            string name;
            if (dynamic_cast<IDevice*> (item) != 0) {
                name = dynamic_cast<IDevice*> (item)->getBoard().getDescription() + "."
                        + item->getDescription();
            } else {
                name = item->getDescription();
            }
            ostr << name << " " << iErr->element << " exp=" << iErr->expectedVal << " val="
                    << iErr->val << "\n";
        }
        LOG4CPLUS_ERROR(getLogger(), e.what());
        FUNCTION_ERR("", ostr.str());
        ostringstream ostr1;
        ostr1<<"XdaqLBoxAccess::checkFirmware(): BadDeviceVersion on "<< e.getVersionInfoList().size()<<" chips";
        throw TException(ostr1.str());
    }
    LOG4CPLUS_INFO(getLogger(), "Firmware OK");
}

void XdaqLBoxAccess::resetBoards(rpct::LinkSystem::ControlBoards& controlBoards,
                     rpct::LinkSystem::LinkBoards& linkBoards, rpct::LinkSystem::RbcBoards& rbcBoards) {
    FUNCTION_INFO(" for selected boards: started)");
    try {
        getLinkSystem()->resetBoards(getLinkSystem()->getControlBoards(), getLinkSystem()->getLinkBoards(), getLinkSystem()->getRbcBoards());
    }
    catch (std::exception& e) {
        ALARM_STD(app_,"FATAL",e);
        FUNCTION_ERR("", e.what());
        XCEPT_RAISE(xcept::Exception, e.what());
    }
    catch (...) {
        ALARM_OTHER(app_,"FATAL","resetBoards failed, unknown exception");
        FUNCTION_ERR("resetBoards failed, unknown exception", "");
        XCEPT_RAISE(xcept::Exception,"resetBoards failed, unknown exception");
    }
    FUNCTION_INFO(" for selected boards: ended");
}

void XdaqLBoxAccess::configure(std::string globalConfigKey, bool enableHardResetByTTC, bool enableOptLinksOut, bool forceLoadFirmware, bool forceReset, bool forceSetup) {
	if (configureFromFlash_)
		configureFromFlash(globalConfigKey, enableHardResetByTTC, enableOptLinksOut);
	else
		configureFromSoftware(globalConfigKey, forceLoadFirmware, forceReset, forceSetup);
}

/* NOTE: this will not update configuration in flashes, but configureFromFlash will */
void XdaqLBoxAccess::configureFromSoftware(std::string globalConfigKey, bool forceLoadFirmware, bool forceReset, bool forceSetup) {
	try {
		FUNCTION_INFO(" started");
	    monitorablesWidget_->clearGlobalStatus();
		std::vector<int> chipIds = getChipIdsToSetup();
		ConfigurationSetBagPtr confSetBag = getConfigurationSet(globalConfigKey, chipIds);
		//resetFec(); is done during the loadFromFlash
		loadFirmware(forceLoadFirmware, confSetBag->bag);
		checkFirmware(confSetBag->bag);

		{//remove this brackets
			try {
				//we want to catch the exception,
				//therefore we do not call XdaqHardwareAccess::resetHardware
			    rpct::MutexHandler handler(hardwareMutex_);
				lastResetCold_ = getLinkSystem()->resetHardware(forceReset);
			}
			catch(EI2CRbc& e) {
				/*in the resetHardware the RBCs are reset as the last ones,
				 * so we don't have to repeat the reset for all CBs and LBs*/
				Rbc* rbc = e.getRbc();
				LinkSystem::ControlBoards cbs;
				LinkSystem::LinkBoards lbs;
				for(ICrate::Boards::iterator it = rbc->getCrate()->getBoards().begin(); it != rbc->getCrate()->getBoards().end(); it++) {
					if( dynamic_cast<ICB*>(*it) != 0) {
						cbs.push_back(dynamic_cast<ICB*>(*it));
					}
					else if( dynamic_cast<LinkBoard*>(*it) != 0) {
						lbs.push_back(dynamic_cast<LinkBoard*>(*it));
					}
				}
				int rep = 3;
				FUNCTION_WARN("", string("error during RBC reset ") + string(e.what()) + ". Trying to reload the firmware for affected LBox");
				for(int i = 0; i < rep; i++) {
					usleep(3000000);
					/*in the resetHardware the RBCs are reset as the last ones,
					 * so we don't have to repeat the reset for all CBs and LBs*/
					loadFromFlash(cbs, lbs);
					rpct::MutexHandler handler(hardwareMutex_);
					getLinkSystem()->initI2CinControlBoards(cbs);
					getLinkSystem()->resetLinkBoards(lbs);  //FIXME maciekz
					try {
						getLinkSystem()->resetRbcBoards(getLinkSystem()->getRbcBoards()); //here we need to reset again all RBCs
						break;
					}
					catch(EI2CRbc& e1) {
						if(i == rep-1) {
							throw e1;
						}
						FUNCTION_WARN("", string("error during RBC reset ") + string(e1.what()) + ". Trying to reload the firmware for affected LBox");
					}
				}
			}
		}

        setupHardware(forceSetup, confSetBag->bag);
        FUNCTION_INFO(" ended");
    }
    catch (xcept::Exception& e) {
        ALARM_EXCEPT(app_,"FATAL",e);
        FUNCTION_ERR("", e.what());
        XCEPT_RETHROW(xgi::exception::Exception, e.what(), e);
    }
    catch (std::exception& e) {
        ALARM_STD(app_,"FATAL",e);
        FUNCTION_ERR("", e.what());
        XCEPT_RAISE(xcept::Exception, e.what());
    }
    catch (...) {
        ALARM_OTHER(app_,"FATAL","configure failed, unknown exception");
        FUNCTION_ERR("configure failed, unknown exception", "");
        XCEPT_RAISE(xcept::Exception,"configure failed, unknown exception");
    }
}

/*
 * updates the configuration in the FLASH only if the DB says it is needed, but not if the configuration uploeded
 * from FLASH is not correct, then  "program FLASH should be manually called
 * see makeFlashConfig
 */
void XdaqLBoxAccess::configureFromFlash(std::string globalConfigKey, bool enableHardResetByTTC, bool enableOptLinksOut) {
	try {
		FUNCTION_INFO(" started");
		bool reloadNeeded = false;
	    monitorablesWidget_->clearGlobalStatus();
		std::vector<int> chipIds = getChipIdsToSetup();
		ConfigurationSetBagPtr confSetBag = getConfigurationSet(globalConfigKey, chipIds);
		loadFromFlash();
		if(!enableOptLinksOut) { //if done here, then checkConfig() realods the firmware, because the register is not correct
		    getLinkSystem()->disableLinksLBs();
		}
		checkFirmware(confSetBag->bag);
        //setupHardware(forceSetup, confSetBag->bag);
        std::vector<std::string> configKeys, tmp;
        tmp = getLBConfigKeys();
        if (tmp.size() > 0)
        	configKeys.push_back(tmp[0]);
        tmp = getRBCConfigKeys();
        if (tmp.size() > 0)
        	configKeys.push_back(tmp[0]);

        getLinkSystem()->initI2CinControlBoards(getLinkSystem()->getControlBoards());

        const LinkSystem::RbcBoards rbcBoards = getLinkSystem()->getRbcBoards();
        for (LinkSystem::RbcBoards::const_iterator i = rbcBoards.begin(); i != rbcBoards.end(); ++i) {
        	try {
        		if ((*i)->putConfigurationToProm(&(confSetBag->bag), false)) {//checks if update is needed
        			FUNCTION_INFO(" RBC configuration in PROM updated for "<<(*i)->getDescription());
        			reloadNeeded = true;
        		}
        	}
        	catch(EI2CRbc& e) {
        		FUNCTION_WARN("", string("error during RBC putConfigurationToProm ") + string(e.what()) + ". Trying to reload the firmware in the HalfBox20");
        		(*i)->getHalfBox()->getLinkBox().getHalfBox20()->getLBoxAccess().loadFromFlash();
        		//the RBC receives the realodFirmware signal from the HalfBox20 even though it is controlled by the CB in the HalfBox10
        		(*i)->getHalfBox()->getLinkBox().getHalfBox20()->getCb().initI2C();
        		try {
        			if ((*i)->putConfigurationToProm(&(confSetBag->bag), false)) {
        				FUNCTION_INFO(" RBC configuration in PROM updated for "<<(*i)->getDescription());
        				reloadNeeded = true;
        			}
        		}
        		catch(EI2CRbc& e) {
        			FUNCTION_WARN("", string("error during RBC putConfigurationToProm ") + string(e.what()) + ". Trying to reload the firmware in the HalfBox10");
        			(*i)->getHalfBox()->getLinkBox().getHalfBox10()->getLBoxAccess().loadFromFlash();
        			(*i)->getHalfBox()->getLinkBox().getHalfBox10()->getCb().initI2C();

        			if ((*i)->putConfigurationToProm(&(confSetBag->bag), false)) {
        				FUNCTION_INFO(" RBC configuration in PROM updated for "<<(*i)->getDescription());
        				reloadNeeded = true;
        			}
        			//if exception here, the configuration will be terminated
        		}
        	}
        }
        if(reloadNeeded)
        	tbsleep(1000); //what for???

        if (makeFlashConfig(confSetBag, getLinkSystem()->getLinkBoards()) > 0)
        	reloadNeeded = true;

        if (reloadNeeded) {
        	loadFromFlash();
        	if(!enableOptLinksOut) { //if done here, then checkConfig() realods the firmware, because the register is not correct
        	    getLinkSystem()->disableLinksLBs();
        	}
        }

        /* it is not needed here because it is done in the checkConfig
        LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<":"<<__LINE__<<" LB->initI2C()");
        for (LinkSystem::LinkBoards::iterator iLB = getLinkSystem()->getLinkBoards().begin();
        		iLB != getLinkSystem()->getLinkBoards().end(); iLB++) {
        	(*iLB)->initI2C();
        }*/

        //if the enableOptLinksOut = fale it means that some registers were changed after the fimrware was loaded
        //so the checkConfig will find it as an error and will reload the firmware
        //therefore the firmware realoding in the checkConfig is disbaled in this case
        bool dontReload = false;
        if(!enableOptLinksOut)
            dontReload = true;
        checkConfig(dontReload);

        if(!enableOptLinksOut) {
            getLinkSystem()->disableLinksLBs();
            FUNCTION_INFO("LB opt links disabled");
        }

        LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<":"<<__LINE__<<" initI2CinControlBoards");
        getLinkSystem()->initI2CinControlBoards(getLinkSystem()->getControlBoards()); //should be here becasue checkConfig can realod firmware

        for (LinkSystem::RbcBoards::const_iterator i = rbcBoards.begin(); i != rbcBoards.end(); ++i) {
        	(*i)->getRbcDevice().checkConfiguration(&(confSetBag->bag));
        }


        if(enableHardResetByTTC) {
            FUNCTION_INFO(" enabling Hard Reset By TTC");
            for(LinkSystem::ControlBoards::const_iterator iCB = getLinkSystem()->getControlBoards().begin();
                    iCB!=getLinkSystem()->getControlBoards().end(); iCB++) {
                (dynamic_cast<TCB*> (*iCB))->getLBoxAccess()->enableTTCHardReset();
            }
        }
        else {
            FUNCTION_INFO("Hard Reset By TTC disabled");
        }

        FUNCTION_INFO(" ended");
    }
    catch (xcept::Exception& e) {
/*    	const LinkSystem::LinkBoards& lbs = getLinkSystem()->getLinkBoards();
   	 	for (LinkSystem::LinkBoards::const_iterator i = lbs.begin(); i!= lbs.end(); ++i)
   	 	{
   		 	unsigned int lbnr = ((*i)->getPosition() - 1) % 10;
   	        unsigned int baseAddr = 0x7000 + lbnr * 0x100;
   	        uint16_t res = (*i)->memoryRead16(baseAddr+LBC::LBC_DEBUG);
   	        LOG4CPLUS_INFO(getLogger(), "Board "<<(*i)->getDescription()<<" LBC_DEBUG = "<<res);
   	 	}*/

        ALARM_EXCEPT(app_,"FATAL",e);
        FUNCTION_ERR("", e.what());
        monitorablesWidget_->handleMonitorException(e, "Exception during configureFromFlash: ");
        monitorablesWidget_->analyseAndGenerateAlarms();
        XCEPT_RETHROW(xcept::Exception, e.what(), e);
    }
    catch (std::exception& e) {
        ALARM_STD(app_,"FATAL",e);
        FUNCTION_ERR("", e.what());
        monitorablesWidget_->handleMonitorException(e, "Exception during configureFromFlash: ");
        monitorablesWidget_->analyseAndGenerateAlarms();
        XCEPT_RAISE(xcept::Exception, e.what());
    }
    catch (...) {
        ALARM_OTHER(app_,"FATAL","configure failed, unknown exception");
        FUNCTION_ERR("configure failed, unknown exception", "");
        std::exception e;
        monitorablesWidget_->handleMonitorException(e, "Unknown c++ exception during configureFromFlash: ");
        monitorablesWidget_->analyseAndGenerateAlarms();
        XCEPT_RAISE(xcept::Exception,"configure failed, unknown exception");
    }  
}


// setup selected boards
unsigned XdaqLBoxAccess::makeFlashConfig(ConfigurationSetBagPtr confSetBag, LinkSystem::LinkBoards linkBoards) {
    unsigned ret = 0;
	if (!getLinkSystem()) {
        string msg = "System pointer not initialized";
        LOG4CPLUS_ERROR(getLogger(), msg);
        XCEPT_RAISE(xcept::Exception,msg);
    }

    try{
    	int nbad = 0;
        const int maxbad = 3;
    	HardwareItemException::Messages errorList;

    	{ // scope for mutex
    		rpct::MutexHandler handler(hardwareMutex_);
    		try {
				for (LinkSystem::LinkBoards::const_iterator iLB =
						linkBoards.begin(); iLB != linkBoards.end(); iLB++) {
					(*iLB)->getSynCoder().generateFlashConfig(&(confSetBag->bag)); //must be here, becouse the generated data are used in the SynCoder::verifyFirmawreWrites()
					LOG4CPLUS_INFO(getLogger(), "Configuration for flash prepared for "<<(*iLB)->getDescription());

					string LBBname = (*iLB)->getCrate()->getDescription();
					string lbname = (*iLB)->getDescription();
					int pos = (*iLB)->getPosition();
					int hbid = (*iLB)->getHalfBox()->getCb().getPosition();
					ISynCoderConfInFlash* flashConf = confSetBag->bag.getSynCoderConfInFlash((*iLB)->getSynCoder().getId());

					//the flashConf is not 0 only, of the latest LB configuration on the DB is different then this stored in the
					//see ConfigurationSet DbServiceImplAxis.getConfigurationSet() line 740
					//SynCoderConfInFlash in the DB (the one uploaded when it was updated in the FLASH))
					if (flashConf!=0) {
						LOG4CPLUS_INFO(getLogger(), "flash configuration for "<<lbname<<": begin"<<hex<<flashConf->getBeginAddress()<<", end: "<<flashConf->getEndAddress());
						if (flashConf->getBeginAddress() == 0) {//means that this is dummy SynCoderConfInFlash i.e. nothing was written to FLASH yet, see DbServiceImplAxis.getConfigurationSet()
							errorList.push_back(HardwareItemException::Messages::value_type(*iLB, ": Flash configuration addresses not known. Reprogram flash!"));
							LOG4CPLUS_ERROR(getLogger(), __FUNCTION__ << ": " << lbname<< ", lbox=" << LBBname<< ", halfbox=" << hbid << ", pos=" << pos<< ": Flash configuration addresses not known. Reprogram flash!");
							++nbad;
							continue;
						}
					} else {
						LOG4CPLUS_INFO(getLogger(), lbname+" has current configuration in flash.");
						continue;
					}

					StdLinkBoxAccess& LBA =	(*iLB)->getHalfBox()->getLBoxAccess();

					int iter = 0;
					const int maxtry = 3; // TODO // need to be adjusted
					bool isOk;
					do {
						isOk = false;
						try {
							LOG4CPLUS_INFO(getLogger(),	__FUNCTION__ << ": " << lbname << ", lbox="<< LBBname << ", halfbox=" << hbid<< ", pos=" << pos << ", iter="<< iter << ": started");
							std::stringstream in("");
							//uint32_t oldAddr = (*iLB)->getSynCoder().getLastFlashAddrOld(), newAddr = (*iLB)->getSynCoder().getLastFlashAddrNew() ;
							uint32_t oldAddr = flashConf->getBeginAddress(), newAddr = flashConf->getEndAddress();
							unsigned num = newAddr - oldAddr;
							LOG4CPLUS_INFO(getLogger(),	__FUNCTION__ << ": " << lbname << ", adding "<< num << " zeros and starting flash write at " << oldAddr);
							for (unsigned i=0; i<num; ++i)
								in.put((char) 0);

							in << ((*iLB)->getSynCoder().getFlashConfig());

							/* We set config id to 0 here. In case writing to flash fails, the next Configure will ask to reprogram flash. */
							flashConf->setSynCoderConfId(0);
							xdaqDbServiceClient_->putSynCoderConfInFlash(*(flashConf));

							LBA.programConfigToFlashKtp(((pos % 10) - 1), &in, oldAddr/4);
							ret++;

							flashConf->setBeginAddress(newAddr);
							flashConf->setEndAddress(oldAddr+ in.str().length());
							flashConf->setSynCoderConfId(confSetBag->bag.getChipConfigurationForChip((*iLB)->getSynCoder().getId())->getConfigurationId());
//							(*iLB)->getSynCoder().setLastFlashAddrNew(oldAddr + in.str().length());
//							(*iLB)->getSynCoder().setLastFlashAddrOld(newAddr);
							xdaqDbServiceClient_->putSynCoderConfInFlash(*(flashConf));
							FUNCTION_INFO(" SynCoder Configuration in FLASH upadted  for "<<lbname);
							LOG4CPLUS_INFO(getLogger(),__FUNCTION__ << ": " << lbname << ", lbox="<< LBBname << ", halfbox=" << hbid<< ", pos=" << pos << ", iter="<< iter << ": done");
							isOk = true;
						} catch (std::exception& e) {
							LOG4CPLUS_ERROR(getLogger(),__FUNCTION__ << ": " << lbname << ", lbox="<< LBBname << ", halfbox=" << hbid<< ", pos=" << pos << ", iter="<< iter << ": failed (" << e.what()<< ")");
							if (iter + 1 == maxtry) {
								nbad++;
								errorList.push_back(
										HardwareItemException::Messages::value_type(
												*iLB, e.what()));
							} else {
								// try to recover by clearing the ccu ring
								try {
									LOG4CPLUS_INFO(getLogger(),__FUNCTION__ << ": " << lbname<< ", lbox=" << LBBname<< ", halfbox=" << hbid<< ", pos=" << pos<< ", iter=" << iter<< ": trying reset FEC to recover");
									resetFec();
								} catch (std::exception& e) {
									errorList.push_back(
											HardwareItemException::Messages::value_type(
													*iLB, e.what()));
									LOG4CPLUS_ERROR(getLogger(), __FUNCTION__ << ": " << lbname<< ", lbox=" << LBBname<< ", halfbox=" << hbid << ", pos=" << pos<< ", iter=" << iter<< ": reset" << e.what() << ")");								}
							}
						}
						iter++;
					} while (!isOk && iter < maxtry && nbad <= maxbad);
					if (nbad > maxbad)
						break;
				}

				if (nbad > maxbad) {
					stringstream msg;
					msg << "Aborted due to >" << maxbad
							<< " LBs failed out of " << linkBoards.size();
					XCEPT_DECLARE(HardwareItemException, e, msg.str());
					e.setMessages(errorList);
					FUNCTION_INFO(": failed ("<<msg.str()<<")");
					throw e;
				}
				if (errorList.size() > 0) {
					stringstream msg;
					msg << nbad << " LBs failed out of " << linkBoards.size();
					XCEPT_DECLARE(HardwareItemException, e, msg.str());
					e.setMessages(errorList);
					LOG4CPLUS_ERROR(getLogger(), __FUNCTION__ << ": failed (" << msg.str() << ")");
					throw e;
				}
				    FUNCTION_INFO(": done");
    		}
    		catch (std::exception& e) {
    			XCEPT_RAISE(xcept::Exception, e.what());
    		}
    		catch (...) {
    			XCEPT_RAISE(xcept::Exception,"makeFlashConfig failed");
    		}
    	}
    }
    catch (xcept::Exception& e) {
		ALARM_EXCEPT(app_,"FATAL",e);
		FUNCTION_ERR("", e.what());
		XCEPT_RETHROW(xgi::exception::Exception, e.what(), e);
	}
	catch (std::exception& e) {
		ALARM_STD(app_,"FATAL",e);
		FUNCTION_ERR("", e.what());
		throw;
	}
	return ret;
}

// setup selected boards
void XdaqLBoxAccess::readCBIC_RELOAD_CNT(LinkSystem::ControlBoards controlBoards) {
    if (!getLinkSystem()) {
        string msg = "System pointer not initialized";
        LOG4CPLUS_ERROR(getLogger(), msg);
        XCEPT_RAISE(xcept::Exception,msg);
    }

    try{
    	{ // scope for mutex
    		rpct::MutexHandler handler(hardwareMutex_);
    		try {
				std::vector<int> chipIds = getChipIdsToSetup();
				//lastResetCold_ = true;
				LOG4CPLUS_INFO(getLogger(), "Configuration for flash prepared");


				for(LinkSystem::ControlBoards::const_iterator iCB=controlBoards.begin(); iCB!=controlBoards.end(); iCB++) {
				            TCB& cb = *(dynamic_cast<TCB*> (*iCB));
				            string LBBname = cb.getCrate()->getDescription();
				            string cbname = cb.getDescription();
				            StdLinkBoxAccess& LBA = cb.getHalfBox()->getLBoxAccess();
					unsigned short res = LBA.memoryRead(CBIC::CBIC_RELOAD_CNT);
					//uint32_t res = LBA.memoryRead16(CBIC::CBIC_RELOAD_CNT);
					stringstream o("");
					o<< cbname
					 << " CBIC_RELOAD_CNT: "<< res;
					FUNCTION_INFO(o.str().c_str());

				}
    		}
    		catch (std::exception& e) {
    			XCEPT_RAISE(xcept::Exception, e.what());
    		}
    		catch (...) {
    			XCEPT_RAISE(xcept::Exception,"Boards configuration failed");
    		}
    	}
    }
    catch (xcept::Exception& e) {
		ALARM_EXCEPT(app_,"FATAL",e);
		FUNCTION_ERR("", e.what());
		XCEPT_RETHROW(xgi::exception::Exception, e.what(), e);
	}
	catch (std::exception& e) {
		ALARM_STD(app_,"FATAL",e);
		FUNCTION_ERR("", e.what());
		throw;
	}
}

ConfigurationSetBagPtr XdaqLBoxAccess::getConfigurationSetForSelectedBoards(std::vector<std::string>& localConfigKeys, LinkSystem::LinkBoards linkBoards, LinkSystem::RbcBoards rbcBoards) {
	//FUNCTION_INFO(": started (forceReset="<<forceReset<<", forceConfigure "<<forceConfigure<<")");
	if (!getLinkSystem()) {
		string msg = "System pointer not initialized";
		LOG4CPLUS_ERROR(getLogger(), msg);
		XCEPT_RAISE(xcept::Exception,msg);
	}

	vector<int> chipIds;

	const System::DeviceMap& deviceMap = getLinkSystem()->getDeviceMap();
	LOG4CPLUS_INFO(getLogger(), "Device map size " << deviceMap.size());
	for (System::DeviceMap::const_iterator iDevice = deviceMap.begin(); iDevice != deviceMap.end(); ++iDevice) {
		if (dynamic_cast<SynCoder*> (iDevice->second) != 0) {
			chipIds.push_back(iDevice->second->getId());
			LOG4CPLUS_DEBUG(getLogger(), "Adding chip id " << iDevice->second->getId());
		} else {
			LOG4CPLUS_WARN(getLogger(), "Skipping setup for " << iDevice->second->getId());
		}
	}

	const System::HardwareItemList& rbcs = getLinkSystem()->getHardwareItemsByType(Rbc::TYPE);
	for (System::HardwareItemList::const_iterator iRbc = rbcs.begin(); iRbc != rbcs.end(); ++iRbc) {
		Rbc* rbc = dynamic_cast<Rbc*> (*iRbc);
		if (rbc == 0) {
			LOG4CPLUS_ERROR(getLogger(), "Board " << (*iRbc)->getDescription() << " is not a Rbc");
		}
		chipIds.push_back(rbc->getRbcDevice().getId());
	}

	LOG4CPLUS_INFO(getLogger(), "Number of chips to configure: " << chipIds.size());

	if (chipIds.empty()) {
		//string msg = "No chips to configure. All disabled?";
		//FUNCTION_ERR("", msg);
		XCEPT_RAISE(xcept::Exception,"No chips to configure. All disabled?");
	}


	//LOG4CPLUS_INFO(this->getLogger(), "Configuring with confKeys = " << configKeys);
	ConfigurationSetBagPtr confSetBag = xdaqDbServiceClient_->getConfigurationSetForLocalConfigKeys(chipIds, localConfigKeys);

	return confSetBag;
}

// setup selected boards
void XdaqLBoxAccess::configureBoards(std::vector<std::string>& configKeys, bool forceReset, bool forceConfigure, LinkSystem::ControlBoards controlBoards, LinkSystem::LinkBoards linkBoards, LinkSystem::RbcBoards rbcBoards) {
    FUNCTION_INFO(": started (forceReset="<<forceReset<<", forceConfigure "<<forceConfigure<<")");
    if (!getLinkSystem()) {
        string msg = "System pointer not initialized";
        LOG4CPLUS_ERROR(getLogger(), msg);
        XCEPT_RAISE(xcept::Exception,msg);
    }

    int configFlags = (forceConfigure ? ConfigurationFlags::FORCE_CONFIGURE : 0) ;
    try{
    	LOG4CPLUS_INFO(getLogger(), "Setting up boards from DB ");
    	vector<int> chipIds;

    	const System::DeviceMap& deviceMap = getLinkSystem()->getDeviceMap();
    	LOG4CPLUS_INFO(getLogger(), "Device map size " << deviceMap.size());
    	for (System::DeviceMap::const_iterator iDevice = deviceMap.begin(); iDevice != deviceMap.end(); ++iDevice) {
    		if (dynamic_cast<SynCoder*> (iDevice->second) != 0) {
    			chipIds.push_back(iDevice->second->getId());
    			LOG4CPLUS_DEBUG(getLogger(), "Adding chip id " << iDevice->second->getId());
    		} else {
    			LOG4CPLUS_WARN(getLogger(), "Skipping setup for " << iDevice->second->getId());
    		}
    	}

        const System::HardwareItemList& rbcs = getLinkSystem()->getHardwareItemsByType(Rbc::TYPE);
        for (System::HardwareItemList::const_iterator iRbc = rbcs.begin(); iRbc != rbcs.end(); ++iRbc) {
        	Rbc* rbc = dynamic_cast<Rbc*> (*iRbc);
        	if (rbc == 0) {
        		LOG4CPLUS_ERROR(getLogger(), "Board " << (*iRbc)->getDescription() << " is not a Rbc");
        	}
        	chipIds.push_back(rbc->getRbcDevice().getId());
        }

    	LOG4CPLUS_INFO(getLogger(), "Number of chips to configure: " << chipIds.size());

    	if (chipIds.empty()) {
    		//string msg = "No chips to configure. All disabled?";
    		//FUNCTION_ERR("", msg);
    		XCEPT_RAISE(xcept::Exception,"No chips to configure. All disabled?");
    	}


	//LOG4CPLUS_INFO(this->getLogger(), "Configuring with confKeys = " << configKeys);
    	ConfigurationSetBagPtr confSetBag = xdaqDbServiceClient_->getConfigurationSetForLocalConfigKeys(chipIds, configKeys);

    	{ // scope for mutex
    		rpct::MutexHandler handler(hardwareMutex_);
    		try {
    			//----------boards reset---------------
    			if (!forceReset) {
    				forceReset = getLinkSystem()->checkResetNeeded(controlBoards, linkBoards, rbcBoards);
    			}
    			LOG4CPLUS_INFO(getLogger(), "resetNeeded "<<forceReset);
    			if(forceReset) {
    				LOG4CPLUS_INFO(getLogger(), "system->resetBoards()");
    				getLinkSystem()->resetBoards(controlBoards, linkBoards, rbcBoards);
    				lastResetCold_ = true;
    			}
    			else {
    				lastResetCold_ = false;
    			}
    			//--------------------------------------

    			getLinkSystem()->setupBoards( &(confSetBag->bag), configFlags, controlBoards, linkBoards, rbcBoards);

    			LOG4CPLUS_INFO(getLogger(), "Boards configured from DB successfully");
    		}
    		catch (std::exception& e) {
    			XCEPT_RAISE(xcept::Exception, e.what());
    		}
    		catch (...) {
    			XCEPT_RAISE(xcept::Exception,"Boards configuration failed");
    		}
    	}
    }
    catch (xcept::Exception& e) {
		ALARM_EXCEPT(app_,"FATAL",e);
		FUNCTION_ERR("", e.what());
		XCEPT_RETHROW(xgi::exception::Exception, e.what(), e);
	}
	catch (std::exception& e) {
		ALARM_STD(app_,"FATAL",e);
		FUNCTION_ERR("", e.what());
		throw;
	}
    FUNCTION_INFO(": done (reset="<<forceReset<<", configure="<<forceConfigure<<")");
}

rpct::xdaqutils::xdaqFebSystem & XdaqLBoxAccess::getFebSystem()
{
    if(febsystem_ == 0) {
        if(initializationErrorStr_.size() != 0) {
            std::string msg = std::string("There was an exception during system initialization, try restarting the application. The exception was: ") + initializationErrorStr_;
            XCEPT_RAISE(xcept::Exception, msg);
        }
        else
            XCEPT_RAISE(xcept::Exception,"febsystem not initialized, try restarting the application");

    }
    return *febsystem_;
}

// mask noisy strips on selected link boards
void XdaqLBoxAccess::maskNoisyChannels(std::string configKey, bool preserveMasksFromDB, LinkSystem::LinkBoards& linkBoards) {
    FUNCTION_INFO(": started (preserveMasksFromDB="<<preserveMasksFromDB<<")");

    if (histogramsWidget_->isActive()) try {
        histogramsWidget_->stop();
    } catch (...) {
        string msg = "Stop histogram monitoring failed";
        FUNCTION_ERR("", msg);
        XCEPT_RAISE(xcept::Exception,msg);
    }
    ///typedef System::HardwareItemList HardwareItemList;
    ///const HardwareItemList& linkBoards = system->getHardwareItemsByType(LinkBoard::TYPE);

    const uint64_t secBx = 40000000;
    const uint64_t countersLimit = 100 * secBx;
    const IDiagCtrl::TTriggerType triggerType = IDiagCtrl::ttManual;
    const double noiseLimit = 100;

    time_t timer = time(NULL);
    tm* tblock = localtime(&timer);
    tblock->tm_mon += 1;

    std::stringstream sfile;
    sfile << "/data/MaskNoisyChannelsHistos/Histos" << tblock->tm_year << '_' << tblock->tm_mon
            << '_' << tblock->tm_mday << '_' << tblock->tm_hour << '_' << tblock->tm_min << '_'
            << tblock->tm_sec << ".txt";
    LOG4CPLUS_INFO(getLogger(), "\n MaskNoisyChannels. Creating file: " << sfile.str() << endl);

    ofstream outFile;
    outFile.open(sfile.str().c_str());
    if (outFile.fail()) {
        string msg = "Could not open the file '" + sfile.str() + "' for writing";
        FUNCTION_ERR("", msg);
        XCEPT_RAISE(xcept::Exception,msg);
    }
    outFile << "ringName " << ringName << endl;
    outFile << "countingTime[s] " << countersLimit / secBx << endl;

    // initialize pointer to the last Link Board for which diagnostics has actually started
    LinkBoard* lastLb = 0;
    {
        rpct::MutexHandler handler(hardwareMutex_);
        try {
            for (LinkSystem::LinkBoards::const_iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); ++iLB) {
                LinkBoard* lb = dynamic_cast<LinkBoard*> (*iLB);
                lb->getSynCoder().ConfigureDiagnosable(IDiagnosable::tdsNone, 0ul);
                //lb->getSynCoder().getFullRateHistoMgr().Reset();
                //lb->getSynCoder().getWinRateHistoMgr().Reset();
                lb->getSynCoder().getStatisticsDiagCtrl().Reset();
                lb->getSynCoder().getStatisticsDiagCtrl().Configure(countersLimit, triggerType, 0);
            }
            
            for (LinkSystem::LinkBoards::const_iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); ++iLB) {
                LinkBoard* lb = dynamic_cast<LinkBoard*> (*iLB);
                // get pointer to the last Link Board for which diagnostics has actually started
                lastLb = lb;
                lb->getSynCoder().getStatisticsDiagCtrl().Start();
        }
            // pointer to the last Link Boards for which diagnostics has started
            // should be non-zero after starting diagnostics for selected boards.
            if(lastLb==0) {
                string msg = "No linkboards to mask!";
                FUNCTION_WARN("", msg);
            }
        } catch (...) {
            string msg = "Syncoder reset/start failed";
            FUNCTION_ERR("", msg);
            XCEPT_RAISE(xcept::Exception,msg);
        }
    } // end of mutex
    bool ended = true;
    do {
        usleep(100000);
        cout << "*" << flush;
        rpct::MutexHandler handler(hardwareMutex_);
        try {
            if (lastLb) ended=lastLb->getSynCoder().getStatisticsDiagCtrl().CheckCountingEnded();
        }
        catch (...) {
            FUNCTION_ERR("", "Syncoder checkCountingEnded failed");
            throw;
        }
    } while (!ended);
    cout <<endl;


    rpct::MutexHandler handler(hardwareMutex_);
    try {
        for (LinkSystem::LinkBoards::const_iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); ++iLB) {
            LinkBoard* lb = dynamic_cast<LinkBoard*>(*iLB);
            lb->getSynCoder().getStatisticsDiagCtrl().Stop();
            lb->getSynCoder().getFullRateHistoMgr().Refresh();
            lb->getSynCoder().getWinRateHistoMgr().Refresh();
        }

        ConfigurationSet* configurationSet = 0;
        ConfigurationSetBagPtr confSetBag;
        if(preserveMasksFromDB) {
            vector<int> chipIds;
            for (LinkSystem::LinkBoards::const_iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); ++iLB) {
                LinkBoard* lb = dynamic_cast<LinkBoard*>(*iLB);
                chipIds.push_back(lb->getSynCoder().getId());
                cout<<"debuuuuuuuug lb->getSynCoder().getId() "<<lb->getSynCoder().getId()<<endl;
            }
            if (chipIds.size()>0) {
	      LOG4CPLUS_INFO(getLogger(), "Configuring with confKey = " << configKey);
	      confSetBag = xdaqDbServiceClient_->getConfigurationSet(chipIds, configKey);
	      configurationSet = &(confSetBag->bag);
            }
        }
        for (LinkSystem::LinkBoards::const_iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); ++iLB) {
            LinkBoard* lb = dynamic_cast<LinkBoard*>(*iLB);
            boost::dynamic_bitset<> channelsEna(SynCoder::RPC_FEBSTD_DATA_WIDTH, 0ul);
            if(preserveMasksFromDB) {
                DeviceSettings* settings = configurationSet->getDeviceSettings(lb->getSynCoder());
                SynCoderSettings* scs = dynamic_cast<SynCoderSettings*>(settings);
                if (scs == 0) {
                    throw rpct::IllegalArgumentException("SynCoder::configure: invalid cast for settings");
                }
                channelsEna = scs->getInChannelsEna();
            }
            else {
                channelsEna.set();
            }

            unsigned int binCount = lb->getSynCoder().getFullRateHistoMgr().GetBinCount();
            LOG4CPLUS_INFO(getLogger(), lb->getDescription());
            outFile<<lb->getDescription()<<endl;
            outFile<<"             fullHisto    "<<"winHisto     ratio       noise       n/g"<<endl;

            for (unsigned int iCh = 0; iCh < binCount; iCh++) {
                uint32_t fullBinCnt = lb->getSynCoder().getFullRateHistoMgr().GetLastData()[iCh];
                uint32_t winBinCnt = lb->getSynCoder().getWinRateHistoMgr().GetLastData()[iCh];

                double noise = ((double)fullBinCnt) / (countersLimit / secBx) / (130.0 * 3.0);

                outFile<<dec << setw(3) << iCh << ": " << dec
                << setw(12) << fullBinCnt << setw(12) << winBinCnt << "  "
                << dec << setw(12) << ((fullBinCnt> 0) ? ((double)winBinCnt)/fullBinCnt : 0)
                << " " << dec << setw(12) << noise
                << (noise >= noiseLimit ? " n" : " g")<<endl;

                if (noise >= noiseLimit) {
                    channelsEna[iCh] = false;
                    LOG4CPLUS_INFO(getLogger(), lb->getDescription()<<" channel " << dec << iCh << " masked as noisy, noise ~" << setw(10) << noise << " Hz/cm2 (not valid for endcap!)");
                }
            }
            lb->getSynCoder().setChannelsEna(channelsEna);
        }
    } catch (xcept::Exception& e) {
        FUNCTION_ERR("", e.what());
        XCEPT_RETHROW(xgi::exception::Exception, e.what(), e);
    } catch (std::exception& e) {
        FUNCTION_ERR("", e.what());
        throw;
    }
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__
            <<": done (preserveMasksFromDB="<<preserveMasksFromDB<<")");
}

// mask noisy strips on ALL link boards
void XdaqLBoxAccess::maskNoisyChannels(std::string configKey, bool preserveMasksFromDB) {
    FUNCTION_INFO(": started for ALL boards (preserveMasksFromDB="<<preserveMasksFromDB<<")");
    try {
        maskNoisyChannels(configKey, preserveMasksFromDB, getLinkSystem()->getLinkBoards());
    }
    catch (xcept::Exception& e) {
        FUNCTION_ERR("", e.what());
        XCEPT_RETHROW(xgi::exception::Exception, e.what(), e);
    }
    catch (std::exception& e) {
        FUNCTION_ERR("", e.what());
        throw;
    }
    FUNCTION_INFO(": done for ALL boards (preserveMasksFromDB="<<preserveMasksFromDB<<")");
}

// masks all channels on selected link boards
void XdaqLBoxAccess::maskAllChannels(rpct::LinkSystem::LinkBoards& linkBoards) {
    FUNCTION_INFO(": started");
    /*   if( histogramsWidget_->isActive() ) { //w zaadzie nie jest to potrzebne KB
     try {
     histogramsWidget_->stop();
     }
     catch(...)
     {
     string msg = "Stop histogram monitoring failed";
     MSGS_ERROR(msg);
     LOG4CPLUS_ERROR(getLogger(), msg);
     }
     }*/
    rpct::MutexHandler handler(hardwareMutex_);
 
    if(!linkBoards.size()) {
        throw TException("No linkboards to mask!");
    }
    LOG4CPLUS_INFO(getLogger(), "\n ************** MASKING ALL CHANNELS: " << endl);
    for (LinkSystem::LinkBoards::const_iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); ++iLB) {
        LinkBoard* lb = dynamic_cast<LinkBoard*> (*iLB);
        boost::dynamic_bitset<> channelsEna(SynCoder::RPC_FEBSTD_DATA_WIDTH, 0ul);
        channelsEna.set();
        unsigned int binCount = lb->getSynCoder().getFullRateHistoMgr().GetBinCount();
        LOG4CPLUS_INFO(getLogger(), " - NUMBER OF BINS TO DISABLE: "<< binCount<< endl);
        for (unsigned int iCh = 0; iCh < binCount; iCh++)
            channelsEna[iCh] = false;
        lb->getSynCoder().setChannelsEna(channelsEna);
    }
    FUNCTION_INFO(": done");
}

// masks all channels on all link boards
void XdaqLBoxAccess::maskAllChannels() {
    FUNCTION_INFO(": started for ALL boards");
    try {
        maskAllChannels(getLinkSystem()->getLinkBoards());
    }
    catch (xcept::Exception& e) {
        FUNCTION_ERR("", e.what());
        XCEPT_RETHROW(xgi::exception::Exception, e.what(), e);
    }
    catch (std::exception& e) {
        FUNCTION_ERR("", e.what());
        throw;
    }
    FUNCTION_INFO(": done for ALL boards");
}

// apply strip masks from DB to selected link boards
void XdaqLBoxAccess::applyDBMasks(std::string configKey, rpct::LinkSystem::LinkBoards& linkBoards) {
    FUNCTION_INFO(": started");
    /*   if( histogramsWidget_->isActive() ) {  //w zaadzie nie jest to potrzebne KB
     try {
     histogramsWidget_->stop();
     }
     catch(...)
     {
     string msg = "Stop histogram monitoring failed";
     MSGS_ERROR(msg);
     LOG4CPLUS_ERROR(getLogger(), msg);
     }
     }*/
    rpct::MutexHandler handler(hardwareMutex_);

    ConfigurationSet* configurationSet = 0;
    ConfigurationSetBagPtr confSetBag;
    vector<int> chipIds;
    for (LinkSystem::LinkBoards::const_iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); ++iLB) {
        LinkBoard* lb = dynamic_cast<LinkBoard*> (*iLB);
        chipIds.push_back(lb->getSynCoder().getId());
    }
    if (linkBoards.size()>0 && chipIds.size()>0) {
        
      FUNCTION_INFO("Configuring with globalConfKey = " << configKey);
      confSetBag = xdaqDbServiceClient_->getConfigurationSet(chipIds, configKey);
      configurationSet = &(confSetBag->bag);
    } else {
        throw TException("No linkboards to mask!");
    }
    for (LinkSystem::LinkBoards::const_iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); ++iLB) {
        LinkBoard* lb = dynamic_cast<LinkBoard*> (*iLB);
        boost::dynamic_bitset<> channelsEna(SynCoder::RPC_FEBSTD_DATA_WIDTH, 0ul);
        DeviceSettings* settings = configurationSet->getDeviceSettings(lb->getSynCoder());
        SynCoderSettings* scs = dynamic_cast<SynCoderSettings*> (settings);
        if (scs == 0) {
            throw rpct::IllegalArgumentException("SynCoder::configure: invalid cast for settings");
        }
        channelsEna = scs->getInChannelsEna();
        lb->getSynCoder().setChannelsEna(channelsEna);
    }
    FUNCTION_INFO(": done");
}

// apply strip masks from DB to all link boards
void XdaqLBoxAccess::applyDBMasks(std::string configKey) {
    FUNCTION_INFO(": started for ALL boards");
    try {
        applyDBMasks(configKey, getLinkSystem()->getLinkBoards());
    }
    catch (xcept::Exception& e) {
        FUNCTION_ERR("", e.what());
        XCEPT_RETHROW(xgi::exception::Exception, e.what(), e);
    }
    catch (std::exception& e) {
        FUNCTION_ERR("", e.what());
        throw;
    }
    FUNCTION_INFO(": done for ALL boards");
}

void XdaqLBoxAccess::checkConfigured() {
    /*
    if (fsm.getCurrentState() != 'C' && fsm.getCurrentState() != 'R') {
        string msg = "Application is not in proper state";
        LOG4CPLUS_ERROR(getLogger(), msg);
        XCEPT_RAISE(xoap::exception::Exception, msg);
    }
    */
}

xoap::MessageReference XdaqLBoxAccess::onApplyDBMasks(xoap::MessageReference msg)
        throw (xoap::exception::Exception) {
    FUNCTION_INFO("soap called");
    checkConfigured();
    try {
      //applyDBMasks(); TODO: what to do here ??? 
        xoap::MessageReference reply = xoap::createMessage();
        FUNCTION_INFO("soap done");
        return reply;
    } catch (xcept::Exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (std::exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (...) {
        FUNCTION_ERR("soap error", "unknown error");
        XCEPT_RAISE(xoap::exception::Exception, (string)"unknown error during "+__FUNCTION__);
    }
}




void XdaqLBoxAccess::reset() {
    runNumber_ = 0;
    FUNCTION_INFO(": done");
}



xoap::MessageReference XdaqLBoxAccess::onResetFec(xoap::MessageReference msg)
        throw (xoap::exception::Exception) {
    FUNCTION_INFO("soap called");
    //xdata::String linkBoxName;
    xdata::String rName;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("ringName", &rName));
    //paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("linkBoxName", &linkBoxName));
    soapAccess_.parseSOAPRequest(msg, RESET_FEC_NAME, RPCT_LBOX_ACCESS_PREFIX, RPCT_LBOX_ACCESS_NS,
            paramMap);
    rpct::MutexHandler handler(hardwareMutex_);
    try {
        unsigned int status;
        resetFec(&status);
        xdata::Bag<FecStatusResponse> result;
        result.bag.set(true, status);
        FUNCTION_INFO("soap done");
        return soapAccess_.sendSOAPResponse(RESET_FEC_NAME, RPCT_LBOX_ACCESS_PREFIX,
                RPCT_LBOX_ACCESS_NS, result);
    } catch (xcept::Exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (std::exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (...) {
        FUNCTION_ERR("soap error", "unknown error");
        XCEPT_RAISE(xoap::exception::Exception, (string)"unknown error during "+__FUNCTION__);
    }
}

void XdaqLBoxAccess::resetFec(unsigned int* status, bool vmeHardInit)
{
    FUNCTION_INFO(": started");
    // Because resetFec() is already protected by hardwareMutex_
    // DO NOT call histogramsWidget_->stop() and stopMonitoring() because they
    // internally call hardwareMutex_!!!
    if (histogramsWidget_->isActive()) {
        string msg = "Histogram monitoring is ongoing!";
        FUNCTION_WARN("", msg);
    }
    if (monitorablesWidget_->isMonitoringOn()) {
        string msg = "Monitoring is ongoing!";
        FUNCTION_WARN("", msg);
    }
    unsigned int stat=0;
    time_t currentTime=time(NULL);
    try {
        bool isGood = false;
        LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": vmeHardInit="<<vmeHardInit);
        isGood = getFecManager().resetPlxFec(stat, vmeHardInit);
        if (status) *status = stat;
        if (!isGood) {
            stringstream msg;
            msg << "Bad FEC status: 0x" << hex << stat;
            throw CCUException(msg.str());
        }
    } catch(rpct::CCUException& e) {
        badResetFecLastTime_=currentTime;
        FUNCTION_ERR("", e.what());
        XCEPT_RAISE(xcept::Exception, e.what());
    } catch(std::exception & e) {
        FUNCTION_ERR("", e.what());
        XCEPT_RAISE(xcept::Exception, e.what());
    } catch(...) {
        FUNCTION_ERR("", "unknown error!!!");
        XCEPT_RAISE(xoap::exception::Exception, "unknown exception");
    }
    FUNCTION_INFO(": done (FEC status = 0x"<<hex<<stat<<")");
}

bool XdaqLBoxAccess::readFecStatus(unsigned int &status) {
    FUNCTION_INFO(": started");
    bool isGood = getFecManager().getFecRingSR0(status);
    FUNCTION_INFO(": done (FEC status = 0x"<<hex<<status<<")");
    return isGood;
}

std::vector<std::string> XdaqLBoxAccess::getFEBConfigKeys() {
  loadLocalConfigKeys();
  return subsystemsLocalConfigKeys_[XdaqDbServiceClient::SUBSYSTEM_FEBS];
}

std::vector<std::string> XdaqLBoxAccess::getLBConfigKeys() {
  loadLocalConfigKeys();
  return subsystemsLocalConfigKeys_[XdaqDbServiceClient::SUBSYSTEM_LBS];
}

std::vector<std::string> XdaqLBoxAccess::getRBCConfigKeys() {
  loadLocalConfigKeys();
  return subsystemsLocalConfigKeys_[XdaqDbServiceClient::SUBSYSTEM_RBCS_TTUS];
}

/////////////////////////////////////////////////////////////////
//
// Auxilary methods for LB and CB firmware loading/programming
//
string XdaqLBoxAccess::getBootDir() {
    ostringstream bootDir;
    char* tmp = getenv("RPCT_DATA_PATH");
    if (tmp != 0) {
        bootDir << getenv("RPCT_DATA_PATH")<<"/"<<"crates_boot";
    }
    else {
    	XCEPT_RAISE(xoap::exception::Exception, (string)(__FUNCTION__) + string(": Environment variable RPCT_DATA_PATH not defined"));
    }
    bootDir << "/LBB/";
    return bootDir.str();
}

// NOTE: onProgramFlash MUST NOT be inside hardwareMutex_.take/give block!
xoap::MessageReference XdaqLBoxAccess::onProgramFlash(xoap::MessageReference msg)
        throw (xoap::exception::Exception) {
    FUNCTION_INFO("soap called");
    /*
    if (fsm.getCurrentState() != 'C' && fsm.getCurrentState() != 'R') {
        string msg = "Application is not in proper state";
        LOG4CPLUS_ERROR(getLogger(), msg);
        XCEPT_RAISE(xoap::exception::Exception, msg);
    }
    */
    xdata::String linkBoxName;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("linkBoxName", &linkBoxName));
    soapAccess_.parseSOAPRequest(msg, PROGRAM_FLASH_NAME, RPCT_LBOX_ACCESS_PREFIX,
            RPCT_LBOX_ACCESS_NS, paramMap);
    try {
        // program FLASH on ALL boards
        programFlash();
        xdata::Integer result = 1;
        FUNCTION_INFO("soap done");
        return soapAccess_.sendSOAPResponse(PROGRAM_FLASH_NAME, RPCT_LBOX_ACCESS_PREFIX,
                RPCT_LBOX_ACCESS_NS, result);
    } catch (xcept::Exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (std::exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (...) {
        FUNCTION_ERR("soap error", "unknown error");
        XCEPT_RAISE(xoap::exception::Exception, (string)"unknown error during "+__FUNCTION__);
    }
}

// NOTE: programFlash MUST NOT be inside hardwareMutex_.take/give block!
void XdaqLBoxAccess::programFlash(LinkSystem::ControlBoards controlBoards, LinkSystem::LinkBoards linkBoards) {
    FUNCTION_INFO(": started");

    histogramsWidget_->stop();
    monitorablesWidget_->stopMonitoring();
    
    {
        rpct::MutexHandler handler(hardwareMutex_);
        resetFec();
    }

    HardwareItemException::Messages errorList;
    try {
        programFlashCb(controlBoards);
    } catch (HardwareItemException& e) {
        errorList.insert(errorList.end(), e.getMessages().begin(), e.getMessages().end());
    }

    try {
        programFlashLb(linkBoards);
    } catch (HardwareItemException& e) {
        errorList.insert(errorList.end(), e.getMessages().begin(), e.getMessages().end());
    }

    if (errorList.size() > 0) {
        ostringstream s;
        for (HardwareItemException::Messages::iterator iErr = errorList.begin(); iErr
                != errorList.end(); ++iErr) {
            s << iErr->first->getDescription() << ": " << iErr->second << "\n";
        }
        FUNCTION_ERR("", s.str());
        LOG4CPLUS_ERROR(getLogger(), __FUNCTION__<<": failed");
    } else {
        FUNCTION_INFO(": done");
    }
}

// NOTE: programFlash MUST NOT be inside hardwareMutex_.take/give block!
void XdaqLBoxAccess::programFlash() {
    FUNCTION_INFO(": started for ALL boards");
    try {
        programFlash(getLinkSystem()->getControlBoards(), getLinkSystem()->getLinkBoards());
    }
    catch (xcept::Exception& e) {
        FUNCTION_ERR("", e.what());
        XCEPT_RETHROW(xgi::exception::Exception, e.what(), e);
    }
    catch (std::exception& e) {
        FUNCTION_ERR("", e.what());
        throw;
    }
    FUNCTION_INFO(": done for ALL boards");
}

// NOTE: programFlashCb MUST NOT be inside hardwareMutex_.take/give block!
void XdaqLBoxAccess::programFlashCb(LinkSystem::ControlBoards controlBoards) {
    FUNCTION_INFO(": started for "<<controlBoards.size()<<" boards");
    
    const string bootDir = getBootDir();
    const string cbromFile = bootDir + "cb_rom.bin";
    FUNCTION_INFO(": CB flash firmware: "<<cbromFile);

    HardwareItemException::Messages errorList;
    int nbad = 0;
    const int maxbad = controlBoards.size(); // TODO // need to be adjusted - MC
    {
        rpct::MutexHandler handler(hardwareMutex_);
        
        for(LinkSystem::ControlBoards::const_iterator iCB=controlBoards.begin(); iCB!=controlBoards.end(); iCB++) {
            TCB& cb = *(dynamic_cast<TCB*> (*iCB));
            string LBBname = cb.getCrate()->getDescription();
            string cbname = cb.getDescription();
            int hbid = cb.getPosition();
            StdLinkBoxAccess& LBA = cb.getHalfBox()->getLBoxAccess();
            
            int iter = 0;
            const int maxtry = 3;  // TODO // need to be adjusted
            bool isOk;
            do {
                isOk = false;
                try {
                    LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": "<<cbname<<", lbox="<<LBBname<<", halfbox="<<hbid<<", iter="<<iter<<": started");
                    LBA.programFlashCb(cbromFile);
                    LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": "<<cbname<<", lbox="<<LBBname<<", halfbox="<<hbid<<", iter="<<iter<<": done");
                    isOk = true;
                } catch (std::exception& e) {
                    LOG4CPLUS_ERROR(getLogger(), __FUNCTION__<<": "<<cbname<<", lbox="<<LBBname<<", halfbox="<<hbid<<", iter="<<iter<<": failed ("<<e.what()<<")");
                    if (iter + 1 == maxtry) {
                        nbad++;
                        errorList.push_back(HardwareItemException::Messages::value_type(*iCB, e.what()));
                    } else {
                        // try to recover by clearing the ccu ring
                        try {
                            LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": "<<cbname<<", lbox="<<LBBname<<", halfbox="<<hbid<<", iter="<<iter<<": trying reset FEC to recover");
                            resetFec();
                        } catch (std::exception& e) {
                            errorList.push_back(HardwareItemException::Messages::value_type(*iCB, e.what()));
                            LOG4CPLUS_ERROR(getLogger(), __FUNCTION__<<": "<<cbname<<", lbox="<<LBBname<<", halfbox="<<hbid<<", iter="<<iter<<": reset FEC failed ("<<e.what()<<")");
                        }
                    }
                }
                iter++;
            } while (!isOk && iter<maxtry && nbad<=maxbad);
            if (nbad > maxbad) break;
        }
    }
    if(nbad > maxbad) {
        stringstream msg;
        msg << "Aborted due to >"<<maxbad<<" CBs failed out of "<<controlBoards.size();
        XCEPT_DECLARE(HardwareItemException, e, msg.str());
        e.setMessages(errorList);
        LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": failed ("<<msg.str()<<")");
        throw e;
    }
    if (errorList.size() > 0) {
        stringstream msg;
        msg <<nbad<<" CBs failed out of "<<controlBoards.size();
        XCEPT_DECLARE(HardwareItemException, e, msg.str());
        e.setMessages(errorList);
        LOG4CPLUS_ERROR(getLogger(), __FUNCTION__<<": failed ("<<msg.str()<<")");
        throw e;
    }
    FUNCTION_INFO(": done");
}

// NOTE: programFlashLb MUST NOT be inside hardwareMutex_.take/give block!
void XdaqLBoxAccess::programFlashLb(LinkSystem::LinkBoards linkBoards) {
    FUNCTION_INFO(": started for "<<linkBoards.size()<<" boards");
    
    const string bootDir = getBootDir();
    const string lbromFile = bootDir + "lb_rom.bin";
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": LB flash firmware: "<<lbromFile);

    HardwareItemException::Messages errorList;
    int nbad = 0;
    const int maxbad = linkBoards.size(); // TODO // need to be adjusted - MC
    {
        rpct::MutexHandler handler(hardwareMutex_);
        
        for(LinkSystem::LinkBoards::const_iterator iLB=linkBoards.begin(); iLB!=linkBoards.end(); iLB++) {
            string LBBname = (*iLB)->getCrate()->getDescription();
            string lbname = (*iLB)->getDescription();
            int pos = (*iLB)->getPosition();
            int hbid = (*iLB)->getHalfBox()->getCb().getPosition();
            StdLinkBoxAccess& LBA = (*iLB)->getHalfBox()->getLBoxAccess();
            
        int iter = 0;
        const int maxtry = 3;  // TODO // need to be adjusted
        bool isOk;
        uint32_t len;
        do {
            isOk = false;
            try {
                LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": "<<lbname<<", lbox="<<LBBname<<", halfbox="<<hbid<<", pos="<<pos<<", iter="<<iter<<": started");
                len = LBA.programFlashKtp(((pos % 10) - 1), lbromFile);
                SynCoderConfInFlash flashConf;
                flashConf.setChipId((*iLB)->getSynCoder().getId());
                flashConf.setBeginAddress(len-32);
                flashConf.setEndAddress((xdata::Integer)len);
                flashConf.setSynCoderConfId(0);
                xdaqDbServiceClient_->putSynCoderConfInFlash(flashConf);
                LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": LB "<<(*iLB)->getId()<<", flash length="<<len);
                LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": "<<lbname<<", lbox="<<LBBname<<", halfbox="<<hbid<<", pos="<<pos<<", iter="<<iter<<": done");
                isOk = true;
            } catch (std::exception& e) {
                LOG4CPLUS_ERROR(getLogger(), __FUNCTION__<<": "<<lbname<<", lbox="<<LBBname<<", halfbox="<<hbid<<", pos="<<pos<<", iter="<<iter<<": failed ("<<e.what()<<")");
                if (iter + 1 == maxtry) {
                    nbad++;
                    errorList.push_back(HardwareItemException::Messages::value_type(*iLB, e.what()));
                } else {
                    // try to recover by clearing the ccu ring
                    try {
                        LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": "<<lbname<<", lbox="<<LBBname<<", halfbox="<<hbid<<", pos="<<pos<<", iter="<<iter<<": trying reset FEC to recover");
                        resetFec();
                    } catch (std::exception& e) {
                        errorList.push_back(HardwareItemException::Messages::value_type(*iLB, e.what()));
                        LOG4CPLUS_ERROR(getLogger(), __FUNCTION__<<": "<<lbname<<", lbox="<<LBBname<<", halfbox="<<hbid<<", pos="<<pos<<", iter="<<iter<<": reset FEC failed ("<<e.what()<<")");
                    }
                }
            }
            iter++;
        } while (!isOk && iter<maxtry && nbad<=maxbad);
        if (nbad > maxbad) break;
        }
    } // end of mutex
    if(nbad > maxbad) {
        stringstream msg;
        msg << "Aborted due to >"<<maxbad<<" LBs failed out of "<<linkBoards.size();
        XCEPT_DECLARE(HardwareItemException, e, msg.str());
        e.setMessages(errorList);
        FUNCTION_INFO(": failed ("<<msg.str()<<")");
        throw e;
    }
    if (errorList.size() > 0) {
        stringstream msg;
        msg <<nbad<<" LBs failed out of "<<linkBoards.size();
        XCEPT_DECLARE(HardwareItemException, e, msg.str());
        e.setMessages(errorList);
        LOG4CPLUS_ERROR(getLogger(), __FUNCTION__<<": failed ("<<msg.str()<<")");
        throw e;
    }
    FUNCTION_INFO(": done");
}

// NOTE: loadFromFlash MUST NOT be inside hardwareMutex_.take/give block!
xoap::MessageReference XdaqLBoxAccess::onLoadFromFlash(xoap::MessageReference msg)
        throw (xoap::exception::Exception) {
    FUNCTION_INFO("soap called");
    xdata::String linkBoxName;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("linkBoxName", &linkBoxName));
    soapAccess_.parseSOAPRequest(msg, LOAD_FROM_FLASH_NAME, RPCT_LBOX_ACCESS_PREFIX,
            RPCT_LBOX_ACCESS_NS, paramMap);
    try {
        // always build system first
        reset(); //TODO(KN): remove?
        buildSystem();
        // loads FPGA from FLASH for ALL halfboxes
        loadFromFlash();
        xdata::Integer result = 1;
        FUNCTION_INFO("soap done");
        return soapAccess_.sendSOAPResponse(LOAD_FROM_FLASH_NAME, RPCT_LBOX_ACCESS_PREFIX,
                RPCT_LBOX_ACCESS_NS, result);
    } catch (xcept::Exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (std::exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (...) {
        FUNCTION_ERR("soap error", "unknown error");
        XCEPT_RAISE(xoap::exception::Exception, (string)"unknown error during "+__FUNCTION__);
    }
}

/////////////////////////////////////////////////////////////////
//
// Load firmware to FPGA on the Link & Control Boards from FLASH
//
// NOTE: loadFromFlash MUST NOT be inside hardwareMutex_.take/give block!
void XdaqLBoxAccess::loadFromFlash(LinkSystem::ControlBoards controlBoards, LinkSystem::LinkBoards linkBoards) {
    FUNCTION_INFO(": started for "<<controlBoards.size()<<" CBs, "<<linkBoards.size()<<" LBs");

    try {
        rpct::MutexHandler handler(hardwareMutex_);
        resetFec();
    } catch (...) {
        string msg="reset FEC failed";
        LOG4CPLUS_ERROR(getLogger(), __FUNCTION__<<": failed ("<<msg<<")");
        throw TException(msg);
    }

    HardwareItemException::Messages errorsList;
    int nbad = 0;

    // construct a list of LBB half boxes, eliminate duplicates
    vector<HalfBox*> hblist;
    // scan CBs
    for(LinkSystem::ControlBoards::const_iterator iCB=controlBoards.begin(); iCB!=controlBoards.end(); iCB++) {
        TCB& cb = *(dynamic_cast<TCB*> (*iCB));
        HalfBox* hb=cb.getHalfBox();
        if(hb && find(hblist.begin(), hblist.end(), hb)==hblist.end()) hblist.push_back(hb);
    }
    // scan LBs
    for(LinkSystem::LinkBoards::const_iterator iLB=linkBoards.begin(); iLB!=linkBoards.end(); iLB++) {
        HalfBox* hb=(*iLB)->getHalfBox();
        if(hb && find(hblist.begin(), hblist.end(), hb)==hblist.end()) hblist.push_back(hb);
    }
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": Number of LBB half boxes="<<hblist.size());

    {
        rpct::MutexHandler handler(hardwareMutex_);
        for(vector<HalfBox*>::const_iterator iHB=hblist.begin(); iHB!=hblist.end(); iHB++) {
            string LBBname = (*iHB)->getLinkBox().getDescription();
            TCB& cb = dynamic_cast<TCB&>((*iHB)->getCb());
            string cbname = cb.getDescription();
            int hbid = cb.getPosition();
            StdLinkBoxAccess& LBA=(*iHB)->getLBoxAccess();

            int iter = 0;
            const int maxtry = 3;

            bool isOk;
            do {
                isOk = false;
                try {
                    LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": lbox="<<LBBname<<", halfbox="<<hbid<<", iter="<<iter<<": started");
                    LBA.loadFromFlash();
                    LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": lbox="<<LBBname<<", halfbox="<<hbid<<", iter="<<iter<<": done");
                    isOk=true;
//              } catch (CCUException& e) {
                } catch (std::exception& e) {
                    LOG4CPLUS_ERROR(getLogger(), __FUNCTION__<<": lbox="<<LBBname<<", halfbox="<<hbid<<", iter="<<iter<<": failed ("<<e.what()<<")");
                    if (iter + 1 == maxtry) {
                        nbad++;
                        errorsList.push_back(HardwareItemException::Messages::value_type(&cb, e.what()));
                    } else {
                        // try to recover by clearing the ccu ring
                        try {
                            LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": lbox="<<LBBname<<", halfbox="<<hbid<<", iter="<<iter<<": trying reset FEC to recover");
                            resetFec();
                        } catch (std::exception& e) {
                            LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": lbox="<<LBBname<<", halfbox="<<hbid<<", iter="<<iter<<": reset FEC failed ("<<e.what()<<")");
                        }
                    }
                }
                iter++;
            } while (!isOk && iter<maxtry);
        }

        usleep(2000000); //TODO
    } // end of mutex
    if (errorsList.size() > 0) {
        ostringstream s;
        for (HardwareItemException::Messages::iterator iErr = errorsList.begin(); iErr
                != errorsList.end(); ++iErr) {
            s << iErr->first->getDescription() << ": " << iErr->second << "\n";
        }
        FUNCTION_ERR("", s.str());
    }
    /* DEBUG */
    //tbsleep(10000);
    if (false)
    for(LinkSystem::LinkBoards::const_iterator iLB=linkBoards.begin(); iLB!=linkBoards.end(); iLB++) {
    	uint32_t pre_res = (*iLB)->getSynCoder().readWord(rpct::SynCoder::WORD_USER_REG1, 0);
    	uint32_t res = (*iLB)->getSynCoder().readWord(rpct::SynCoder::WORD_USER_REG2, 0);
    	uint32_t post_res = (*iLB)->getSynCoder().readWord(rpct::SynCoder::WORD_CHAN_ENA, 0);
        stringstream o;
        o<<"Link board "<<(*iLB)->getId()<<" WORD_USER_REG1: "<<hex<<pre_res
         <<" WORD_USER_REG2: "<<hex<<res
         <<" WORD_CHAN_ENA: "<<hex<<post_res;
        FUNCTION_INFO(o.str().c_str());

    }
    /* end DEBUG */

    //LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": Re-creating software objects.");
    //resetFSM(); //TODO(KN): ?????
    /*
    toolbox::Event::Reference e(new toolbox::Event("Configure", this));
    fsm.fireEvent(e);
    if (fsm.getCurrentState() == 'F') {
        LOG4CPLUS_ERROR(getLogger(), __FUNCTION__<<": failed");
    } else {
        LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": done");
    }
    */
}

void XdaqLBoxAccess::loadFromFlashParalel(LinkSystem::ControlBoards controlBoards, LinkSystem::LinkBoards linkBoards) {
    FUNCTION_INFO(": started for "<<controlBoards.size()<<" CBs, "<<linkBoards.size()<<" LBs");

    rpct::MutexHandler handler(hardwareMutex_);

    resetFec();

    HardwareItemException::Messages errorsList;
    //int nbad = 0;

    // construct a list of LBB half boxes, eliminate duplicates
    vector<HalfBox*> hblist;
    // scan CBs
    for(LinkSystem::ControlBoards::const_iterator iCB=controlBoards.begin(); iCB!=controlBoards.end(); iCB++) {
        TCB& cb = *(dynamic_cast<TCB*> (*iCB));
        HalfBox* hb=cb.getHalfBox();
        if(hb && find(hblist.begin(), hblist.end(), hb)==hblist.end()) hblist.push_back(hb);
    }
    // scan LBs
    for(LinkSystem::LinkBoards::const_iterator iLB=linkBoards.begin(); iLB!=linkBoards.end(); iLB++) {
        HalfBox* hb=(*iLB)->getHalfBox();
        if(hb && find(hblist.begin(), hblist.end(), hb)==hblist.end()) hblist.push_back(hb);
    }
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": Number of LBB half boxes="<<hblist.size());

    int iter = 0;
    const int maxtry = 3;

    do {
    	try {
    		for(vector<HalfBox*>::const_iterator iHB=hblist.begin(); iHB!=hblist.end(); iHB++) {
    			LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<":"<<__LINE__<<": HalfBox= "<<(*iHB)->getCb().getDescription()<<", iter="<<iter);
    			(*iHB)->getLBoxAccess().configureCBICforLoadFromFlash(); //puts CBIC in reset state
    			//configureCBICforLoadFromFlash enables hard reset trigger by the CCU, but disables the hard reset triggered by the TTC
    			//(*iHB)->getLBoxAccess().resetTTCrxStep1();
    		}
    		/*usleep(3000000); //3s
    		for(vector<HalfBox*>::const_iterator iHB=hblist.begin(); iHB!=hblist.end(); iHB++) {
    			LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<":"<<__LINE__<<": HalfBox= "<<(*iHB)->getCb().getDescription()<<", iter="<<iter);
    			(*iHB)->getLBoxAccess().resetTTCrxStep2();
    		}*/

    		resetAllNotReadyCBTTCrxsNoMutex(); //todo maybe it wrong that it is here, while cbic is in reset state?

    		usleep(1000000);
    		for(vector<HalfBox*>::const_iterator iHB=hblist.begin(); iHB!=hblist.end(); iHB++) {
    			LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<":"<<__LINE__<<": HalfBox= "<<(*iHB)->getCb().getDescription()<<", iter="<<iter);
    			//(*iHB)->getLBoxAccess().resetTTCrxStep3();//only reads the TTCrx ready
    			(*iHB)->getLBoxAccess().putCBICinRunState(); //Load from flash starts here
    		}
    		usleep(FixedHardwareSettings::CONFIGURE_FROM_FLASH_TIME * 2);
    		for(vector<HalfBox*>::const_iterator iHB=hblist.begin(); iHB!=hblist.end(); iHB++) {
    			LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<":"<<__LINE__<<": HalfBox= "<<(*iHB)->getCb().getDescription()<<", iter="<<iter);
    			(*iHB)->getLBoxAccess().checkCBICafterLoadFromFlash();
    			//(*iHB)->getLBoxAccess().configureCBICOperationMode(enableTTCReload_); dont want to enable the TTCReoad here
    		}

    		break; //this iteration was succesfull
    	} catch (std::exception& e) {
    		LOG4CPLUS_ERROR(getLogger(), __FUNCTION__<<": error: "<<e.what());
    		if (iter + 1 == maxtry) {
    			throw e;
    		} else {
    			// try to recover by clearing the ccu ring
    			LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": trying reset FEC to recover");
    			resetFec(); //will thorw an exeption if failed
    		}
    	}
    	iter++;
    } while (iter<maxtry);

    usleep(1000000); //TODO
    /*    if (errorsList.size() > 0) {
        ostringstream s;
        for (HardwareItemException::Messages::iterator iErr = errorsList.begin(); iErr
                != errorsList.end(); ++iErr) {
            s << iErr->first->getDescription() << ": " << iErr->second << "\n";
        }
        FUNCTION_ERR("", s.str());
    }*/
    /* DEBUG */
    //tbsleep(10000);
    if (false)
    	for(LinkSystem::LinkBoards::const_iterator iLB=linkBoards.begin(); iLB!=linkBoards.end(); iLB++) {
    		uint32_t pre_res = (*iLB)->getSynCoder().readWord(rpct::SynCoder::WORD_USER_REG1, 0);
    		uint32_t res = (*iLB)->getSynCoder().readWord(rpct::SynCoder::WORD_USER_REG2, 0);
    		uint32_t post_res = (*iLB)->getSynCoder().readWord(rpct::SynCoder::WORD_CHAN_ENA, 0);
    		stringstream o;
    		o<<"Link board "<<(*iLB)->getId()<<" WORD_USER_REG1: "<<hex<<pre_res
    				<<" WORD_USER_REG2: "<<hex<<res
					<<" WORD_CHAN_ENA: "<<hex<<post_res;
    		FUNCTION_INFO(o.str().c_str());

    	}
    /* end DEBUG */
}

// load from flash all boards
// NOTE: loadFromFlash MUST NOT be inside hardwareMutex_.take/give block!
void XdaqLBoxAccess::loadFromFlash() {
    FUNCTION_INFO(": started for ALL boards");
    try {
        //loadFromFlash(getLinkSystem()->getControlBoards(), getLinkSystem()->getLinkBoards());
    	loadFromFlashParalel(getLinkSystem()->getControlBoards(), getLinkSystem()->getLinkBoards());
    }
    catch (std::exception& e) {
        FUNCTION_ERR("", e.what());
        XCEPT_RAISE(xcept::Exception, e.what());
    }
   /* catch (xcept::Exception& e) { should not be thrown
        FUNCTION_ERR("", e.what());
        XCEPT_RETHROW(xgi::exception::Exception, e.what(), e);
    }*/
    FUNCTION_INFO(": done for ALL boards");
}

/////////////////////////////////////////////////////////////////
// check if values loaded from flash configuration match expected ones on all boards
// if dontReload == false performs at most one additional loadFromFlash() to try to clear the error
// no chcek is perfored if configure is not made from flash (magic - the map of checked addresses is empty)

void XdaqLBoxAccess::checkConfig(bool dontReload, LinkSystem::ControlBoards controlBoards, LinkSystem::LinkBoards linkBoards) {
	FUNCTION_INFO(" started");

    HardwareItemException::Messages errorsList;
    bool wasBad = false;

    // scan LBs
    for(LinkSystem::LinkBoards::const_iterator iLB=linkBoards.begin(); iLB!=linkBoards.end(); iLB++)
    {
    	(*iLB)->initI2C();
        if (!(*iLB)->getSynCoder().verifyFirmawreWrites())
        	wasBad = true;
    }
    if (!wasBad) {
    	FUNCTION_INFO(" ended");
    	return;
    }

    if(dontReload) {
        LOG4CPLUS_WARN(getLogger(), "values from flash did not match expected, but reloading disabled");
        return;
    }

    LOG4CPLUS_WARN(getLogger(), "values from flash did not match expected, reloading...");

    loadFromFlash(controlBoards, linkBoards);

    // scan LBs
    for(LinkSystem::LinkBoards::const_iterator iLB=linkBoards.begin(); iLB!=linkBoards.end(); iLB++)
    {
    	(*iLB)->initI2C();
        if (!(*iLB)->getSynCoder().verifyFirmawreWrites())
        	errorsList.push_back(HardwareItemException::Messages::value_type((*iLB), "Verify of configuration loaded from flash failed."));
    }

    if (errorsList.size() > 0) {
        ostringstream s;
        for (HardwareItemException::Messages::iterator iErr = errorsList.begin(); iErr
                != errorsList.end(); ++iErr) {
            s << iErr->first->getDescription() << ": " << iErr->second << "\n";
        }
        FUNCTION_ERR("", s.str());
        XCEPT_DECLARE(HardwareItemException, e, s.str());
        e.setMessages(errorsList);
        throw e;
    }

}

// check if values loaded from flash configuration match expected ones on all boards
// no check is performed if configure is not made from flash (magic - the map of checked addresses is empty)

void XdaqLBoxAccess::checkConfig(bool dontReload) {
    try {
        checkConfig(dontReload, getLinkSystem()->getControlBoards(), getLinkSystem()->getLinkBoards());
    }
    catch (std::exception& e) {
        FUNCTION_ERR("", e.what());
        throw;
    }
}

/////////////////////////////////////////////////////////////////
//
// Load firmware to FPGA on the Control Boards via CCU
//
// NOTE: loadFromCcuCb MUST NOT be inside hardwareMutex_.take/give block!
void XdaqLBoxAccess::loadFromCcuCb(LinkSystem::ControlBoards controlBoards) {
    FUNCTION_INFO(": started for "<<controlBoards.size()<<" boards");
    /*
     if (fsm.getCurrentState() != 'C' && fsm.getCurrentState() != 'R') {
     string msg = "Application is not in proper state";
     LOG4CPLUS_ERROR(getLogger(), msg);
     XCEPT_RAISE(xoap::exception::Exception, msg);
     }
     */
    const string bootDir = getBootDir();
    const string cbpcBinFile = bootDir + "cbpc.bin";
    const string cbicIniFile = bootDir + "cbic.ini";
    const string lbcBinFile  = bootDir + "lbc.bin";
    const string lbcIniFile  = bootDir + "lbc.ini";
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": CBPC firmware: "<<cbpcBinFile);
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": CBIC ini     : "<<cbicIniFile);
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": LBC firmware : "<<lbcBinFile);
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": LBC ini      : "<<lbcIniFile);

    HardwareItemException::Messages errorList;
    int nbad = 0;
    const int maxbad = 3;

    {
        rpct::MutexHandler handler(hardwareMutex_);
        for(LinkSystem::ControlBoards::const_iterator iCB=controlBoards.begin(); iCB!=controlBoards.end(); iCB++) {
            TCB& cb = *(dynamic_cast<TCB*> (*iCB));
            string LBBname = cb.getCrate()->getDescription();
            string cbname = cb.getDescription();
            int hbid = cb.getPosition();
            StdLinkBoxAccess& LBA = cb.getHalfBox()->getLBoxAccess();

            int iter = 0;
            const int maxtry = 3;
            bool isOk;
            do {
                isOk = false;
                try {
                    LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": "<<cbname<<", lbox="<<LBBname<<", halfbox="<<hbid<<", iter="<<iter<<": started");
                    LBA.loadCb(cbpcBinFile, cbicIniFile, lbcBinFile, lbcIniFile);
                    LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": "<<cbname<<", lbox="<<LBBname<<", halfbox="<<hbid<<", iter="<<iter<<": done");
                    isOk = true;
                } catch (std::exception& e) {
                    LOG4CPLUS_ERROR(getLogger(), __FUNCTION__<<": "<<cbname<<", lbox="<<LBBname<<", halfbox="<<hbid<<", iter="<<iter<<": failed ("<<e.what()<<")");
                    if (iter + 1 == maxtry) {
                        nbad++;
                        errorList.push_back(HardwareItemException::Messages::value_type(*iCB, e.what()));
                    } else {
                        // try to recover by clearing the ccu ring
                        try {
                            LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": "<<cbname<<", lbox="<<LBBname<<", halfbox="<<hbid<<", iter="<<iter<<": trying reset FEC to recover");
                            resetFec();
                        } catch (std::exception& e) {
                            errorList.push_back(HardwareItemException::Messages::value_type(*iCB, e.what()));
                            LOG4CPLUS_ERROR(getLogger(), __FUNCTION__<<": "<<cbname<<", lbox="<<LBBname<<", halfbox="<<hbid<<", iter="<<iter<<": reset FEC failed ("<<e.what()<<")");
                        }
                    }
                }
                iter++;
            } while (!isOk && iter<maxtry && nbad<=maxbad);
            if (nbad > maxbad) break;
        }
    } // end of mutex

    if(nbad > maxbad) {
        stringstream msg;
        msg << "Aborted due to >"<<maxbad<<" CBs failed out of "<<controlBoards.size();
        XCEPT_DECLARE(HardwareItemException, e, msg.str());
        e.setMessages(errorList);
        LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": failed ("<<msg.str()<<")");
        throw e;
    }
    if (errorList.size() > 0) {
        stringstream msg;
        msg <<nbad<<" CBs failed out of "<<controlBoards.size();
        XCEPT_DECLARE(HardwareItemException, e, msg.str());
        e.setMessages(errorList);
        LOG4CPLUS_ERROR(getLogger(), __FUNCTION__<<": failed ("<<msg.str()<<")");
        throw e;
    }
    FUNCTION_INFO(": done");
}

/////////////////////////////////////////////////////////////////
//
// Load firmware to FPGA on the Link Boards via CCU
//
// NOTE: loadFromCcuLb MUST NOT be inside hardwareMutex_.take/give block!
void XdaqLBoxAccess::loadFromCcuLb(LinkSystem::LinkBoards linkBoards) {
    FUNCTION_INFO(": started for "<<linkBoards.size()<<" boards");

    const string bootFile = getBootDir() + "cii_lb_std.bin";
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": KTP firmware="<<bootFile);

    HardwareItemException::Messages errorList;
    int nbad = 0;
    const int maxbad = 3;

    {
        rpct::MutexHandler handler(hardwareMutex_);
        for (LinkSystem::LinkBoards::const_iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); ++iLB) {
            int hbid = (*iLB)->getHalfBox()->getCb().getPosition();
            StdLinkBoxAccess& LBA = (*iLB)->getHalfBox()->getLBoxAccess();
            string lbname = (*iLB)->getDescription();
            string LBBname = (*iLB)->getCrate()->getDescription();
            int halfboxpos = ((*iLB)->getPosition()%10)-1; // counting from 0 to 8

            int iter = 0;
            const int maxtry = 3;
            bool isOk;

            do {
                isOk = false;
                try {
                    LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": "<<lbname<<", lbox="<<LBBname<<", halfbox="<<hbid<<", pos[0-8]="<<halfboxpos<<", iter="<<iter<<": started");
                    LBA.loadKtp(halfboxpos, bootFile);
                    LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": "<<lbname<<", lbox="<<LBBname<<", halfbox="<<hbid<<", pos[0-8]="<<halfboxpos<<", iter="<<iter<<": done");
                    isOk = true;
                } catch (std::exception& e) {
                    LOG4CPLUS_ERROR(getLogger(), __FUNCTION__<<": "<<lbname<<", lbox="<<LBBname<<", halfbox="<<hbid<<", pos[0-8]="<<halfboxpos<<", iter="<<iter<<": failed ("<<e.what()<<")");
                    if (iter + 1 == maxtry) {
                        errorList.push_back(HardwareItemException::Messages::value_type(*iLB, e.what()));
                        nbad++;
                    } else {
                        // try to recover by clearing the ccu ring
                        try {
                            LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": "<<lbname<<", lbox="<<LBBname<<", halfbox="<<hbid<<", pos[0-8]="<<halfboxpos<<", iter="<<iter<<": trying reset FEC to recover");
                            resetFec();
                        } catch (std::exception& e) {
                            errorList.push_back(HardwareItemException::Messages::value_type(*iLB, e.what()));
                            LOG4CPLUS_ERROR(getLogger(), __FUNCTION__<<": "<<lbname<<", lbox="<<LBBname<<", halfbox="<<hbid<<", pos[0-8]="<<halfboxpos<<", iter="<<iter<<": reset FEC failed ("<<e.what()<<")");
                        }
                    }
                }
                iter++;
            } while (!isOk && iter<maxtry && nbad<=maxbad);
            if(nbad > maxbad) break;
        }
    }  //end of mutex
    if(nbad > maxbad) {
        stringstream msg;
        msg << "Aborted due to >"<<maxbad<<" LBs failed out of "<<linkBoards.size();
        XCEPT_DECLARE(HardwareItemException, e, msg.str());
        e.setMessages(errorList);
        LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": failed ("<<msg.str()<<")");
        throw e;
    }
    if (errorList.size() > 0) {
        stringstream msg;
        msg <<nbad<<" LBs failed out of "<<linkBoards.size();
        XCEPT_DECLARE(HardwareItemException, e, msg.str());
        e.setMessages(errorList);
        LOG4CPLUS_ERROR(getLogger(), __FUNCTION__<<": failed ("<<msg.str()<<")");
        throw e;
    }
    FUNCTION_INFO(": done");
}

xoap::MessageReference XdaqLBoxAccess::onLoadFromCcu(xoap::MessageReference msg)
        throw (xoap::exception::Exception) {
    FUNCTION_INFO("soap called");
    xdata::String linkBoxName;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("linkBoxName", &linkBoxName));
    soapAccess_.parseSOAPRequest(msg, LOAD_FROM_CCU_NAME, RPCT_LBOX_ACCESS_PREFIX,
            RPCT_LBOX_ACCESS_NS, paramMap);
    try {
        loadFromCcu();
        xdata::Integer result = 1;
        FUNCTION_INFO("soap done");
        return soapAccess_.sendSOAPResponse(LOAD_FROM_CCU_NAME, RPCT_LBOX_ACCESS_PREFIX,
                RPCT_LBOX_ACCESS_NS, result);
    } catch (xcept::Exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (std::exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (...) {
        FUNCTION_ERR("soap error", "unknown error");
        XCEPT_RAISE(xoap::exception::Exception, (string)"unknown error during "+__FUNCTION__);
    }
}

void XdaqLBoxAccess::loadFromCcu(LinkSystem::ControlBoards controlBoards, LinkSystem::LinkBoards linkBoards) {
    LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": started");


    try {
        rpct::MutexHandler handler(hardwareMutex_);
        resetFec();
    } catch (...) {
        string msg="reset FEC failed";
        LOG4CPLUS_ERROR(getLogger(), __FUNCTION__<<": failed ("<<msg<<")");
        throw TException(msg);
    }

    HardwareItemException::Messages errorList;

    try {
        loadFromCcuCb(controlBoards);
    } catch (HardwareItemException& e) {
        errorList.insert(errorList.end(), e.getMessages().begin(), e.getMessages().end());
    }

    try {
        loadFromCcuLb(linkBoards);
    } catch (HardwareItemException& e) {
        errorList.insert(errorList.end(), e.getMessages().begin(), e.getMessages().end());
    }

    if (errorList.size() > 0) {
        ostringstream s;
        for (HardwareItemException::Messages::iterator iErr = errorList.begin(); iErr
                != errorList.end(); ++iErr) {
            s << iErr->first->getDescription() << ": " << iErr->second << "\n";
        }
        FUNCTION_ERR("", s.str());
    }

    LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": Re-creating software objects.");
    // resetFSM(); //TODO(KN): ???
    /*
    toolbox::Event::Reference e(new toolbox::Event("Configure", this));
    fsm.fireEvent(e);
    if (fsm.getCurrentState() == 'F') {
        LOG4CPLUS_ERROR(getLogger(), __FUNCTION__<<": failed");
    } else {
        LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": done");
    }
    */
}

void XdaqLBoxAccess::loadFromCcu() {
    FUNCTION_INFO(": started for ALL boards");
    try {
        loadFromCcu(getLinkSystem()->getControlBoards(), getLinkSystem()->getLinkBoards());
    }
    catch (xcept::Exception& e) {
        FUNCTION_ERR("", e.what());
        XCEPT_RETHROW(xgi::exception::Exception, e.what(), e);
    }
    catch (std::exception& e) {
        FUNCTION_ERR("", e.what());
        throw;
    }
    FUNCTION_INFO(": done for ALL boards");
}

xoap::MessageReference XdaqLBoxAccess::onReadLBoxFirmwareVer(xoap::MessageReference msg)
        throw (xoap::exception::Exception) {
    FUNCTION_INFO("soap called");
    /*
    if (fsm.getCurrentState() != 'C' && fsm.getCurrentState() != 'R') {
        string msg = "Application is not in proper state";
        LOG4CPLUS_ERROR(getLogger(), msg);
        XCEPT_RAISE(xoap::exception::Exception, msg);
    }
    */
    xdata::String linkBoxName;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("linkBoxName", &linkBoxName));
    soapAccess_.parseSOAPRequest(msg, READ_LBOX_FIRMWARE_VER_NAME, RPCT_LBOX_ACCESS_PREFIX,
            RPCT_LBOX_ACCESS_NS, paramMap);

    try {
        readLBoxFirmwareVer();
        xdata::Integer result = 1;
        FUNCTION_INFO("soap done");
        return soapAccess_.sendSOAPResponse(READ_LBOX_FIRMWARE_VER_NAME, RPCT_LBOX_ACCESS_PREFIX,
                RPCT_LBOX_ACCESS_NS, result);
    } catch (xcept::Exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (std::exception& e) {
        FUNCTION_ERR("soap error", e.what());
        XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
    } catch (...) {
        FUNCTION_ERR("soap error", "unknown error");
        XCEPT_RAISE(xoap::exception::Exception, (string)"unknown error during "+__FUNCTION__);
    }
}

// false = prints the most essential registers only
// true = also prints additional LB registers
void XdaqLBoxAccess::readLBoxFirmwareVer(bool verbose) {
    FUNCTION_INFO(": started (verbose="<<verbose<<")");
    rpct::MutexHandler handler(hardwareMutex_);
    histogramsWidget_->stop();
    monitorablesWidget_->stopMonitoring();
    
    //const System::HardwareItemList& linkBoxes = system->getHardwareItemsByType(LinkBox::TYPE);
    ostringstream ostr;
    //for (System::HardwareItemList::const_iterator iLBB = linkBoxes.begin(); iLBB != linkBoxes.end(); ++iLBB) {
    for (LinkSystem::LinkBoxList::const_iterator iLBB = getLinkSystem()->getLinkBoxes().begin(); iLBB != getLinkSystem()->getLinkBoxes().end(); ++iLBB) {
        LinkBox* lbb = dynamic_cast<LinkBox*> (*iLBB);
        string LBBname = lbb->getDescription();
        LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": LBB="<<LBBname);
        std::vector<HalfBox*> hb;
        hb.push_back(lbb->getHalfBox10());
        hb.push_back(lbb->getHalfBox20());
        for (unsigned int ihb = 0; ihb < hb.size(); ihb++)
            if (hb[ihb]) {
                TCB& cb = dynamic_cast<TCB&> (hb[ihb]->getCb());
                string cbname = cb.getDescription();
                int hbid = (ihb + 1) * 10;
                // Control Board
                try {
                    LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": LBB="<<LBBname<<" halfbox="<<hbid<<" CB="<<cbname);
                    ostr << cb.readTestRegs(verbose) << "\n";
                } catch (...) {
                    LOG4CPLUS_ERROR(getLogger(), __FUNCTION__<<": LBB="<<LBBname<<" halfbox="<<hbid<<" CB="<<cbname<<": Exception occurred !");
                }
                // Link Boards
                for (HalfBox::LinkBoards::const_iterator iLB = hb[ihb]->getLinkBoards().begin(); iLB != hb[ihb]->getLinkBoards().end(); ++iLB) {
                    string lbname = (*iLB)->getDescription();
                    try {
                        LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": LBB="<<LBBname<<" halfbox="<<hbid<<" LB="<<lbname);
                        ostr << (*iLB)->readTestRegs(verbose) << "\n";
                    } catch (...) {
                        LOG4CPLUS_ERROR(getLogger(), __FUNCTION__<<": LBB="<<LBBname<<" halfbox="<<hbid<<" LB="<<lbname<<": Exception occurred !");
                    }
                }
            }
    }
    if(ostr.str().size()>0) FUNCTION_INFO("\n"<<ostr.str());
    FUNCTION_INFO(": done");
}


void XdaqLBoxAccess::testLBs(rpct::LinkSystem::LinkBoards linkBoards) {
	FUNCTION_INFO(": started");
	try {
		rpct::MutexHandler handler(hardwareMutex_);
		/*ostringstream ostr;
		char* tmp = getenv("RPCT_DATA_PATH");
		if (tmp != 0) {
			ostr << getenv("RPCT_DATA_PATH")<<"/"<<"testFEBsTiming";
		}
		else {
			XCEPT_RAISE(xoap::exception::Exception, (string)(__FUNCTION__) + string(": Environment variable RPCT_DATA_PATH not defined"));
		}
		LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": febsTimingTester path: "<<ostr.str());
		FEBsTimingTester febsTimingTester(ostr.str());*/
		/*for(unsigned int i = 3; i < 0xf; i++) {
		febsTimingTester.timingTest(0.001, FEBsTimingTester::testWinO, i);
		FUNCTION_INFO(": febsTimingTester.timingTest iteration "<<i);
	}*/

		//FEBsTimingTester::AnalyserForTestBoard analyserForTestBoard;
		//febsTimingTester.timingTest(0.01, FEBsTimingTester::testWinO, 0x555555, 0, 239, 128, &analyserForTestBoard, linkBoards);
		//febsTimingTester.timingTest(0.01, FEBsTimingTester::testWinC, 0x555555, 239, 0, 128, &analyserForTestBoard, linkBoards);

		//febsTimingTester.pulseTest(linkBoards);

		//febsTimingTester.timingTest(0.01, FEBsTimingTester::testWinO, 0xaaaaaa, 0, 239, 16);
		//febsTimingTester.timingTest(0.01, FEBsTimingTester::testWinO, 0x555555, 0, 239, 16);

		//febsTimingTester.timingTest(0.01, FEBsTimingTester::testWinO, 0xffffff, 0, 100, 128);
		//febsTimingTester.timingTest(0.01, FEBsTimingTester::testWinO, 0xaaaaaa, 0, 100, 16);
		//febsTimingTester.timingTest(0.01, FEBsTimingTester::testWinO, 0x555555, 0, 100, 16);
		//febsTimingTester.timingTest(0.01, FEBsTimingTester::testWinO, 0x7);
		//febsTimingTester.timingTest(0.01, FEBsTimingTester::testWinC);

		/*getLinkSystem()->resetLinkBoards(linkBoards);
	for (LinkSystem::LinkBoards::iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); ++iLB) {
		LOG4CPLUS_INFO(getLogger(), "configuring the TTCrx in the "<<(*iLB)->getDescription());
		rpct::TTTCrxI2C& i2c = (*iLB)->getTtcRxI2c();

		i2c.WriteControlReg(FixedHardwareSettings::LB_TTC_RX_CONTROL_REG);
		i2c.WriteFineDelay1( i2c.GetFineDelay1() );
		i2c.WriteFineDelay2( i2c.GetFineDelay2() );
	}*/

		/*
	for (LinkSystem::LinkBoards::iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); ++iLB) {
		LOG4CPLUS_INFO(getLogger(), "moving the deskew clocks in the TTCrx in the "<<(*iLB)->getDescription());
		rpct::TTTCrxI2C& i2c = (*iLB)->getTtcRxI2c();

		int fineDelay1 = i2c.GetFineDelay1();
		int fineDelay2 = i2c.GetFineDelay2();
		i2c.WriteFineDelay1( 57);
		i2c.WriteFineDelay2( 57 );

		i2c.WriteFineDelay1( fineDelay1);
		i2c.WriteFineDelay2( fineDelay2 );
	}
		 */

		/*	ConfigurationSetImplXData::SynCoderConfInFlashVector v;
	for (LinkSystem::LinkBoards::iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); ++iLB) {
		xdata::Bag<SynCoderConfInFlash> flashConf;
		flashConf.bag.setChipId((*iLB)->getSynCoder().getId());
		flashConf.bag.setBeginAddress(123);
		flashConf.bag.setEndAddress((xdata::Integer)234);
		flashConf.bag.setSynCoderConfId(0);
		//v.push_back(flashConf);

		SynCoderConfInFlash synCoderConfInFlash;
		synCoderConfInFlash.setChipId((*iLB)->getSynCoder().getId());
		synCoderConfInFlash.setBeginAddress(123);
		synCoderConfInFlash.setEndAddress((xdata::Integer)234);
		synCoderConfInFlash.setSynCoderConfId(0);
		xdaqDbServiceClient_->putSynCoderConfInFlash(synCoderConfInFlash);
	}*/
		//xdaqDbServiceClient_->putSynCoderConfInFlashes(v);


		/*	rpct::MutexHandler handler(hardwareMutex_);
	//LinkSystem::LinkBoards& linkBoards = getLinkSystem()->getLinkBoards();
	LinkBoard* lb = getLinkSystem()->getLinkBoards()[0];

	for(unsigned int i = 0; i < 5000; i++) {
		LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": WORD_USER_REG1 "<<hex<<lb->getSynCoder().readWord(SynCoder::WORD_USER_REG1, 0));
		LOG4CPLUS_INFO(getLogger(), __FUNCTION__<<": WORD_USER_REG2 "<<hex<<lb->getSynCoder().readWord(SynCoder::WORD_USER_REG2, 0));
		//usleep(10000);
	}*/

		/*LOG4CPLUS_INFO(getLogger(), "reading selcted registers");
		for (LinkSystem::LinkBoards::iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); ++iLB) {



			LOG4CPLUS_INFO(getLogger(), (*iLB)->getDescription()<<" "
					<<(*iLB)->readTestRegs(true)<<"\n"
					<<" WORD_USER_REG1: "<<hex<<(*iLB)->getSynCoder().readWord(SynCoder::WORD_USER_REG1, 0)
					<<" WORD_USER_REG2: "<<hex<<(*iLB)->getSynCoder().readWord(SynCoder::WORD_USER_REG2, 0)
					<<" WORD_SEND_TEST_DATA: "<<hex<<(*iLB)->getSynCoder().readWord(SynCoder::WORD_SEND_TEST_DATA, 0)

			);
		}*/


		LOG4CPLUS_INFO(getLogger(), "starting pulser for twinMUx test");
		const unsigned int pulseLen = 64;
/*		uint32_t pulseData[pulseLen]  = {
				0x0408,
				0x1020,
				0x4103,
				0x060c,
				0x1830,
				0x6142,
				0x050a,
				0x1428,
				0x5123,
				0x470f,
				0x1e3c,
				0x7972,
				0x6448,
				0x1122,
				0x450b,
				0x162c,
				0x5933,
				0x674e,
				0x1d3a,
				0x756a,
				0x5429,
				0x5327,
				0x4f1f,
				0x3e7d,
				0x7a74,
				0x6850,
				0x2143,
				0x070e,
				0x1c38,
				0x7162,
				0x4409,
				0x1224,
				0x4913,
				0x264d,
				0x1b36,
				0x6d5a,
				0x356b,
				0x562d,
				0x5b37,
				0x6f5e,
				0x3d7b,
				0x766c,
				0x5831,
				0x6346,
				0x0d1a,
				0x3469,
				0x5225,
				0x4b17,
				0x2e5d,
				0x3b77,
				0x6e5c,
				0x3973,
				0x664c,
				0x1932,
				0x654a,
				0x152a,
				0x552b,
				0x572f,
				0x5f3f,
				0x7f7e,
				0x7c78,
				0x7060,
				0x4001,
				0x4001
		};*/
		uint32_t pulseData[pulseLen];
		for(int i = 0; i < pulseLen; i++) {
			pulseData[i] = i & 0xff;
		}
		//uint32_t pulsePattern = 0xffffff;//0xffffff
		std::vector<BigInteger> dataVec;
		for (unsigned int iBx = 0;iBx < pulseLen; iBx++) {
			dataVec.push_back(BigInteger(pulseData[iBx]));
		}

	    for (LinkSystem::LinkBoards::iterator iLB = linkBoards.begin(); iLB != linkBoards.end(); ++iLB) {
	    	LinkBoard* lb = (*iLB); //
	    	if(lb->getMasterSlaveType() == msMaster) {
	    		if(lb->getSynCoder().getPulserDiagCtrl().GetState() ==  IDiagCtrl::sRunning) {
	    			lb->getSynCoder().getPulserDiagCtrl().Stop();
	    			FUNCTION_INFO("pulser on "<<lb->getDescription()<<" stopped");
	    		}
	    		else {
	    			//lbsVec[iLB]->getStdLb().SetTTCrxDelays(winC, winO, 0, 0);
	    			//lb->getSynCoder().writeBits(SynCoder::BITS_SEND_CHECK_ENA, 0ul); //TODO!!!!!!!!!!!!!
	    			//lb->getSynCoder().writeBits(SynCoder::BITS_SEND_CHECK_DATA_ENA, 0ul);//TODO!!!!!!!!!!!!!
	    			//lbsVec[iLB]->getSynCoder().writeBits(SynCoder::BITS_GOL_DATA_SEL, golDataSel);

	    			lb->getSynCoder().writeBits(SynCoder::BITS_LMUX_SLAVE0_ENA, 0ul);
	    			lb->getSynCoder().writeBits(SynCoder::BITS_LMUX_SLAVE1_ENA, 0ul);

	    			lb->getSynCoder().getPulser().Configure(dataVec, pulseLen, 1, 2); //target 2 - GOL in
	    			lb->getSynCoder().getPulserDiagCtrl().Reset(); //<<<<<<<<<<<<<<<<<<<<<<<?????????????????????????
	    			lb->getSynCoder().getPulserDiagCtrl().Configure(0xffffffffff - 1, IDiagCtrl::ttManual, 0);

	    			lb->getSynCoder().getPulserDiagCtrl().Start();
	    			FUNCTION_INFO("pulser on "<<lb->getDescription()<<" started");
	    		}
	    	}

	    }

	}
	catch (std::exception& e) {
		FUNCTION_ERR("error ", e.what());
		//XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
	} catch (...) {
		FUNCTION_ERR("error ", "unknown error");
		//XCEPT_RAISE(xoap::exception::Exception, (string)"unknown error during "+__FUNCTION__);
	}
	FUNCTION_INFO(": finished");
}

void XdaqLBoxAccess::checkHardware() {
	FUNCTION_INFO(": started");
	getLinkSystem()->initI2CinControlBoards(getLinkSystem()->getControlBoards());
	LinkSystem::LinkBoards& linkBoards = getLinkSystem()->getLinkBoards();
	// scan LBs
	string str;
	{
		rpct::MutexHandler handler(hardwareMutex_);

		for(LinkSystem::LinkBoards::const_iterator iLB = linkBoards.begin(); iLB!=linkBoards.end(); iLB++)
		{
			(*iLB)->initI2C();
			if (!(*iLB)->getSynCoder().verifyFirmawreWrites()) {
				str += (*iLB)->getDescription();
			}
		}
	}
	if(str.size() != 0) {
		str = "Verify of configuration loaded from flash failed on: " + str;
		FUNCTION_ERR("", str);
	}

	bool stop = false;
	monitorablesWidget_->monitorHardware(&stop);//mutex is inside
	monitorablesWidget_->collectMonitorableStaus();
	monitorablesWidget_->analyseAndGenerateAlarms();

	FUNCTION_INFO(": finished");
}

void XdaqLBoxAccess::resetCBTTCrxs(rpct::LinkSystem::ControlBoards controlBoards) {
	FUNCTION_INFO(": started resetting CB TTCrx.");
	//LinkSystem::ControlBoards& controlBoards = getLinkSystem()->getControlBoards();

	{
		rpct::MutexHandler handler(hardwareMutex_);
		string notReadyNames = "";
	    for(LinkSystem::ControlBoards::const_iterator iCB = controlBoards.begin(); iCB!=controlBoards.end(); iCB++) {
	    	(dynamic_cast<TCB*> (*iCB))->getLBoxAccess()->resetTTCrxStep1();
		}
		usleep(100000); //5s
		for(LinkSystem::ControlBoards::const_iterator iCB = controlBoards.begin(); iCB!=controlBoards.end(); iCB++) {
			(dynamic_cast<TCB*> (*iCB))->getLBoxAccess()->resetTTCrxStep2();
		}
		usleep(4000000);
		int notReadyCnt = 0;
		for(LinkSystem::ControlBoards::const_iterator iCB = controlBoards.begin(); iCB!=controlBoards.end(); iCB++) {
			if( (dynamic_cast<TCB*> (*iCB))->getLBoxAccess()->resetTTCrxStep3() == false) {
				notReadyCnt++;
				notReadyNames = notReadyNames + " " + (dynamic_cast<TCB*> (*iCB))->getDescription();
				FUNCTION_INFO("resetting TTCrx on  "<<(dynamic_cast<TCB*> (*iCB))->getDescription()<<" - not OK");
			}
			else {
				FUNCTION_INFO("resetting TTCrx on  "<<(dynamic_cast<TCB*> (*iCB))->getDescription()<<" - OK");
			}
		}
		FUNCTION_INFO("CB TTCrx not ready on  "<<notReadyCnt<<" CBs"<<notReadyNames);
	}

	FUNCTION_INFO(": finished");
}



void XdaqLBoxAccess::resetAllNotReadyCBTTCrxs() {
    FUNCTION_INFO(": started resetting CB TTCrx in not ready state.");
    LinkSystem::ControlBoards& allControlBoards = getLinkSystem()->getControlBoards();
    LinkSystem::ControlBoards notReadyCBs;

    {
        rpct::MutexHandler handler(hardwareMutex_);
        resetAllNotReadyCBTTCrxsNoMutex();
    }
}

void XdaqLBoxAccess::resetAllNotReadyCBTTCrxsNoMutex() {
	FUNCTION_INFO(": started resetting CB TTCrx in not ready state.");

	LinkSystem::ControlBoards& allControlBoards = getLinkSystem()->getControlBoards();
	LinkSystem::ControlBoards notReadyCBs;

	{
		string notReadyNames = "";

		for(LinkSystem::ControlBoards::const_iterator iCB = allControlBoards.begin(); iCB!=allControlBoards.end(); iCB++) {
			if( (dynamic_cast<TCB*> (*iCB))->getLBoxAccess()->resetTTCrxStep3() == false)
				notReadyCBs.push_back((*iCB));
			//else
				//FUNCTION_INFO(": TTCrx on  "<<(dynamic_cast<TCB*> (*iCB))->getDescription()<<" was OK.");
		}

		//created vector with not-Ready TTCrx

		for(LinkSystem::ControlBoards::const_iterator iCB = notReadyCBs.begin(); iCB!=notReadyCBs.end(); iCB++) {
			(dynamic_cast<TCB*> (*iCB))->getLBoxAccess()->resetTTCrxStep1();
		}
		usleep(100000); //5s
		for(LinkSystem::ControlBoards::const_iterator iCB = notReadyCBs.begin(); iCB!=notReadyCBs.end(); iCB++) {
			(dynamic_cast<TCB*> (*iCB))->getLBoxAccess()->resetTTCrxStep2();
		}
		usleep(4000000);
		int notReadyCnt = 0;
		int ReadyCnt = 0;
		for(LinkSystem::ControlBoards::const_iterator iCB = notReadyCBs.begin(); iCB!=notReadyCBs.end(); iCB++) {
			if( (dynamic_cast<TCB*> (*iCB))->getLBoxAccess()->resetTTCrxStep3() == false) {
				notReadyCnt++;
				FUNCTION_INFO(": After resetting TTCrx on  "<<(dynamic_cast<TCB*> (*iCB))->getDescription()<<" it is still NOT-ready.");
			}
			else {
				ReadyCnt++;
				FUNCTION_INFO(": After resetting TTCrx on  "<<(dynamic_cast<TCB*> (*iCB))->getDescription()<<" it is now READY!");
			}
		}

		if ((notReadyCnt+ReadyCnt)==0){
			FUNCTION_INFO(": All CBs TTCrx were ready. No need to reset.");
		}
		else{
			FUNCTION_INFO(": From "<<(notReadyCnt+ReadyCnt)<<" not ready CB TTCrx now: "<<ReadyCnt<<" is READY and "<<notReadyCnt<<" is NOT ready.");
		}

	}

	FUNCTION_INFO(": finished");
}

void XdaqLBoxAccess::resetAllCBTTCrxs() {
	FUNCTION_INFO(": started resetting ALL CB TTCrx in the SYSTEM.");

	LinkSystem::ControlBoards& allControlBoards = getLinkSystem()->getControlBoards();

	{
		rpct::MutexHandler handler(hardwareMutex_);
		string notReadyNames = "";

		for(LinkSystem::ControlBoards::const_iterator iCB = allControlBoards.begin(); iCB!=allControlBoards.end(); iCB++) {
			(dynamic_cast<TCB*> (*iCB))->getLBoxAccess()->resetTTCrxStep1();
		}
		usleep(100000); //5s
		for(LinkSystem::ControlBoards::const_iterator iCB = allControlBoards.begin(); iCB!=allControlBoards.end(); iCB++) {
			(dynamic_cast<TCB*> (*iCB))->getLBoxAccess()->resetTTCrxStep2();
		}
		usleep(4000000);
		int notReadyCnt = 0;
		int ReadyCnt = 0;
		for(LinkSystem::ControlBoards::const_iterator iCB = allControlBoards.begin(); iCB!=allControlBoards.end(); iCB++) {
			if( (dynamic_cast<TCB*> (*iCB))->getLBoxAccess()->resetTTCrxStep3() == false) {
				notReadyCnt++;
				FUNCTION_INFO(": After resetting TTCrx on  "<<(dynamic_cast<TCB*> (*iCB))->getDescription()<<" it is NOT-ready.");
			}
			else {
				ReadyCnt++;
				FUNCTION_INFO(": After resetting TTCrx on  "<<(dynamic_cast<TCB*> (*iCB))->getDescription()<<" it is READY!");
			}
		}

		FUNCTION_INFO(": After restarting: "<<(notReadyCnt)<<" is not ready and "<<ReadyCnt<<" is READY .");

	}

	FUNCTION_INFO(": finished");
}


void XdaqLBoxAccess::linkBoxTest(rpct::LinkSystem::ControlBoards controlBoards, rpct::LinkSystem::LinkBoards linkBoards) {
	ostringstream linkBoxTestsDir;
	char* tmp = getenv("RPCT_DATA_PATH");
	if (tmp != 0) {
		linkBoxTestsDir << getenv("RPCT_DATA_PATH")<<"/"<<"linkBoxTests";
	}
	else {
		XCEPT_RAISE(xoap::exception::Exception, (string)(__FUNCTION__) + string(": Environment variable RPCT_DATA_PATH not defined"));
	}

	string fileDirName =  linkBoxTestsDir.str() + "/boardsLabels.txt";
	string backplaneLabel = getLinkSystem()->readBoardsLabels(fileDirName);

	string checkOptLinksCommand = linkBoxTestsDir.str() + "/CheckOptLinks.sh TC_904";
	string checkOptLinkslogFileName = linkBoxTestsDir.str() + "/checkOptLinks.log";

	linkBoxTestsDir<<"/lbox_"<<backplaneLabel<<"_"<< timeString();
	::system( (string("mkdir ") + linkBoxTestsDir.str()).c_str());
	::system( (string("cp ") + fileDirName + " " + linkBoxTestsDir.str()).c_str());

	stringstream stream;
	stream << linkBoxTestsDir.str()<<"/";
	stream << app_->getApplicationDescriptor()->getClassName();
	stream << "-";
	stream << app_->getApplicationDescriptor()->getInstance();
	stream << ".log";
	string str = stream.str();
	for (u_int i = 0;i < str.size();i++){
		if (str[i] == ':'){
			str.replace(i,1,"_");
		};
	};
	FileAppender* appender = new FileAppender(str);
	appender->setLayout(auto_ptr<Layout>(new PatternLayout("%d{%d %b %Y %H:%M:%S} %p %c{1} <> - %m%n")));

	SharedAppenderPtr appenderPtr(appender);
	getSummaryLogger().addAppender(appenderPtr);

	FUNCTION_INFO(": started");
	try {
		FUNCTION_INFO(": pulse test started");
		rpct::MutexHandler handler(hardwareMutex_);
		FEBsTimingTester febsTimingTester(linkBoxTestsDir.str());
		febsTimingTester.getLogger().addAppender(appenderPtr);

		FEBsTimingTester::AnalyserForTestBoard analyserForTestBoard;
		string result = febsTimingTester.timingTest(0.01, FEBsTimingTester::testWinO, 0xffffff, 0, 239, 128, &analyserForTestBoard,linkBoards);
		if(result.find("BAD") !=  string::npos)  {
			FUNCTION_WARN("", result);
		}
		else {
			FUNCTION_INFO(result);
		}
		febsTimingTester.getLogger().removeAppender(appenderPtr);
		FUNCTION_INFO(": pulse test ended");

		//----------------------------------opt links test ----------------------------
		FUNCTION_INFO(": checkOptLinks started");
		int checkOptLinksReturned = ::system(checkOptLinksCommand.c_str());

		ifstream checkOptLinkslog;
		checkOptLinkslog.open(checkOptLinkslogFileName.c_str());
		if(!checkOptLinkslog) {
			FUNCTION_WARN("", "checkOptLinks.log file could not be opened: "<<fileDirName);
		}
		else {
			std::string str((std::istreambuf_iterator<char>(checkOptLinkslog)), std::istreambuf_iterator<char>());
			LOG4CPLUS_INFO(getSummaryLogger(), "\n"<<str);
		}

		if(checkOptLinksReturned == -1) {
			FUNCTION_WARN("", "error during execution on the CheckOptLinks");
		}
		else if(WEXITSTATUS(checkOptLinksReturned) > 0) {
			FUNCTION_WARN("", "CheckOptLinks ended: BAD links count "<<WEXITSTATUS(checkOptLinksReturned));
		}
		else if(WEXITSTATUS(checkOptLinksReturned) == 0) {
			FUNCTION_WARN("", "CheckOptLinks ended: ALL links OK");
		}




	}
	catch (std::exception& e) {
		FUNCTION_ERR("error ", e.what());
		//XCEPT_RAISE(xoap::exception::Exception, (string)"error during "+__FUNCTION__+": "+e.what());
	} catch (...) {
		FUNCTION_ERR("error ", "unknown error");
		//XCEPT_RAISE(xoap::exception::Exception, (string)"unknown error during "+__FUNCTION__);
	}



	FUNCTION_INFO(": finished");

	getSummaryLogger().removeAppender(appenderPtr);
}
