/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: I. Magrans          				         *
 *************************************************************************/
#include "rpct/ts/worker/CellContext.h"
#include "ts/framework/Level1Source.h"
#include "rpct/ts/LogUtils.h"

#include "ts/exception/CellException.h"
#include "ts/toolbox/CellToolbox.h"

#include "rpct/ts/worker/MonitorSource.h"

#include <string>
#include <cstdlib>

#include "toolbox/string.h"

#include "log4cplus/logger.h"



rpcttsworker::CellContext::CellContext(log4cplus::Logger& log, tsframework::CellAbstract* cell):tsframework::CellAbstractContext(log, cell)
{
	logger_ = log4cplus::Logger::getInstance(getLogger().getName() + ".CellContext");
	datasource_ = new rpcttsworker::MonitorSource("rpct_worker",this, logger_);
	


	// https://savannah.cern.ch/task/index.php?14360
	getLevel1Source().setSubsystem("RPC");

}

rpcttsworker::CellContext::~CellContext()
{
  ;
}

std::string 
rpcttsworker::CellContext::getClassName()
{
  return "CellContext";
}

rpcttsworker::XhannelsMap rpcttsworker::CellContext::getFilteredXhannelList(){
	rpcttsworker::XhannelsMap res;
	rpcttsworker::XhannelsMap xhannels = this->getXhannelList();
	for (rpcttsworker::XhannelsMap::iterator iXhannel = xhannels.begin(); iXhannel != xhannels.end(); iXhannel++) {

		std::string name = 	iXhannel->first;
		if (!xhannelFiltering[name]){
			res[name] = iXhannel->second;
		}
	}
	return res;
}

bool rpcttsworker::CellContext::isIgnored(std::string name){
	return xhannelFiltering[name];
}

void rpcttsworker::CellContext::setIgnored(std::string name,bool value){
	xhannelFiltering[name] = value;
}
