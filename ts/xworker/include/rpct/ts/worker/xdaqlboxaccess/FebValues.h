#ifndef RPCTFEBVALUES_H_
#define RPCTFEBVALUES_H_

#include "rpct/ts/worker/xdaqlboxaccess/FebInfo.h"
#include "xdata/Bag.h"
#include "xdata/Float.h"
#include "xdata/Vector.h"
#include "xdata/String.h"

namespace rpct {


class FebValues {
public:
    typedef xdata::Bag<FebInfo> Feb;
    typedef xdata::Vector<xdata::Float> Values;
private:
    Feb feb_;
    Values values_;
    //xdata::String msg_;
public:
    void registerFields(xdata::Bag<FebValues>* bag) {
        bag->addField("feb", &feb_);
        bag->addField("values", &values_);
        //bag->addField("msg", &msg_);
    }

    Feb& getFeb() {
        return feb_;
    }

    Values& getValues() {
        return values_;
    }

    /*std::string getMsg() {
        return msg_;
    }

    void setMsg(std::string msg) {
        msg_ = msg;
    }*/
};



}

#endif /*FEBVALUES_H_*/
