#ifndef _RPCTXDAQLBOXACCESSCLIENT_H_
#define _RPCTXDAQLBOXACCESSCLIENT_H_

#include "xdaq/Application.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"

#include "xdata/Serializable.h"
#include "xdata/rpctsoap/Serializer.h"

#include "rpct/xdaqutils/XdaqSoapAccess.h"
#include "rpct/ts/worker/xdaqlboxaccess/XdaqLBoxAccess.h"
#include "rpct/ts/worker/xdaqlboxaccess/LinkBoxSoapInterface.h"

#include "rpct/ts/worker/xdaqlboxaccess/MassiveReadRequest.h"
#include "rpct/ts/worker/xdaqlboxaccess/MassiveReadResponse.h"
#include "rpct/ts/worker/xdaqlboxaccess/MassiveWriteRequest.h"
#include "rpct/ts/worker/xdaqlboxaccess/MassiveWriteResponse.h"

#include "rpct/ts/worker/xdaqlboxaccess/FecStatusResponse.h"

#include <list>
#include <map>

namespace rpct {

class XdaqLBoxAccessClient {
protected:
    xdaq::Application* application;
    rpct::xdaqutils::XdaqSoapAccess soapAccess;
public:
    XdaqLBoxAccessClient(xdaq::Application* app)
    : application(app), soapAccess(app)  {}

    typedef xdata::Bag<MassiveWriteRequest> MassiveWriteRequestBag;
    typedef xdata::Bag<MassiveWriteResponse> MassiveWriteResponseBag;

    MassiveWriteResponseBag* massiveWrite(MassiveWriteRequestBag& massiveWriteRequest, int appInstance) {
        rpct::xdaqutils::XdaqSoapAccess::TSOAPParamList paramList;
        paramList.push_back(
            rpct::xdaqutils::XdaqSoapAccess::TSOAPParamList::value_type("writeRequest",
            &massiveWriteRequest));
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "sending massive write request");
        xoap::MessageReference massvieWriteResponse = soapAccess.sendSOAPRequest(
            LinkBoxSoapInterface::MASSIVE_WRITE_NAME,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS,
            paramList, "rpcttsworker::LBCell", appInstance, false);
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "massive write request sent");
        /////////////
        MassiveWriteResponseBag* massvieWriteResponseBag = new MassiveWriteResponseBag();
        soapAccess.parseSOAPResponse(massvieWriteResponse,
            LinkBoxSoapInterface::MASSIVE_WRITE_NAME,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS,
            *massvieWriteResponseBag);
        return massvieWriteResponseBag;
    }

    typedef xdata::Bag<MassiveReadRequest> MassiveReadRequestBag;
    typedef xdata::Bag<MassiveReadResponse> MassiveReadResponseBag;

    // !!! Caller should delete the result
    MassiveReadResponseBag* massiveRead(MassiveReadRequestBag& massiveReadRequest, int appInstance) {
        rpct::xdaqutils::XdaqSoapAccess::TSOAPParamList paramList;
        paramList.push_back(
            rpct::xdaqutils::XdaqSoapAccess::TSOAPParamList::value_type("readRequest",
            &massiveReadRequest));
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "sending massive read request");
        xoap::MessageReference massvieReadResponse = soapAccess.sendSOAPRequest(
            LinkBoxSoapInterface::MASSIVE_READ_NAME,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS,
            paramList, "rpcttsworker::LBCell", appInstance, false);
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "massive read response received");

        MassiveReadResponseBag* massvieReadResponseBag = new MassiveReadResponseBag();
        soapAccess.parseSOAPResponse(massvieReadResponse,
                                     LinkBoxSoapInterface::MASSIVE_READ_NAME,
                                     XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX,
                                     XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS,
                                     *massvieReadResponseBag);
        return massvieReadResponseBag;
    }

    int readRbcRegister(std::string linkBoxName, int address, int appInstance) {
        rpct::xdaqutils::XdaqSoapAccess::TSOAPParamList paramList;
        xdata::String xLinkBoxName = linkBoxName;
        xdata::Integer xAddress = address;
        paramList.push_back(
            rpct::xdaqutils::XdaqSoapAccess::TSOAPParamList::value_type("linkBoxName", &xLinkBoxName));
        paramList.push_back(
            rpct::xdaqutils::XdaqSoapAccess::TSOAPParamList::value_type("address", &xAddress));
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "sending readRbcRegister request");
        xoap::MessageReference readRbcRegisterResponse = soapAccess.sendSOAPRequest(
            LinkBoxSoapInterface::READ_RBC_REGISTER_NAME,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS,
            paramList, "rpcttsworker::LBCell", appInstance, false);
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "massive read response received");

        xdata::Integer value;
        soapAccess.parseSOAPResponse(readRbcRegisterResponse,
            LinkBoxSoapInterface::READ_RBC_REGISTER_NAME,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS,
            value);
        return (int)value;
    }

    void writeRbcRegister(std::string linkBoxName, int address, int data, int appInstance) {
        rpct::xdaqutils::XdaqSoapAccess::TSOAPParamList paramList;
        xdata::String xLinkBoxName = linkBoxName;
        xdata::Integer xAddress = address;
        xdata::Integer xData = data;
        paramList.push_back(rpct::xdaqutils::XdaqSoapAccess::TSOAPParamList::value_type("linkBoxName", &xLinkBoxName));
        paramList.push_back(rpct::xdaqutils::XdaqSoapAccess::TSOAPParamList::value_type("address", &xAddress));
        paramList.push_back(rpct::xdaqutils::XdaqSoapAccess::TSOAPParamList::value_type("data", &xData));
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "sending writeRbcRegister request");
        xoap::MessageReference writeRbcRegisterResponse = soapAccess.sendSOAPRequest(
            LinkBoxSoapInterface::WRITE_RBC_REGISTER_NAME,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS,
            paramList, "rpcttsworker::LBCell", appInstance, false);
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "writeRbcRegister response received");

        soapAccess.parseSOAPResponse(writeRbcRegisterResponse,
            LinkBoxSoapInterface::WRITE_RBC_REGISTER_NAME,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS);
    }

    int readRbcTemperature(std::string linkBoxName, int appInstance) {
        rpct::xdaqutils::XdaqSoapAccess::TSOAPParamList paramList;
        xdata::String xLinkBoxName = linkBoxName;
        paramList.push_back(
            rpct::xdaqutils::XdaqSoapAccess::TSOAPParamList::value_type("linkBoxName", &xLinkBoxName));
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "sending readRbcTemperature request");
        xoap::MessageReference readRbcTemperatureResponse = soapAccess.sendSOAPRequest(
            LinkBoxSoapInterface::READ_RBC_TEMPERATURE_NAME,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS,
            paramList, "rpcttsworker::LBCell", appInstance, false);
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "readRbcTemperature received");

        xdata::Integer value;
        soapAccess.parseSOAPResponse(readRbcTemperatureResponse,
            LinkBoxSoapInterface::READ_RBC_TEMPERATURE_NAME,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS,
            value);
        return (int)value;
    }

    // LBOX & FEC configuration
    typedef xdata::Bag<FecStatusResponse> FecStatusResponseBag;
    FecStatusResponseBag resetFec(std::string linkBoxName, int appInstance) {
        rpct::xdaqutils::XdaqSoapAccess::TSOAPParamList paramList;
        xdata::String xLinkBoxName = linkBoxName;
        paramList.push_back(
            rpct::xdaqutils::XdaqSoapAccess::TSOAPParamList::value_type("linkBoxName", &xLinkBoxName));
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "sending resetFec request");
        xoap::MessageReference resetFecResponse = soapAccess.sendSOAPRequest(
            XdaqLBoxAccess::RESET_FEC_NAME,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS,
            paramList, "rpcttsworker::LBCell", appInstance, false);
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "resetFec received");

        FecStatusResponseBag data;
        soapAccess.parseSOAPResponse(resetFecResponse,
            XdaqLBoxAccess::RESET_FEC_NAME,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS,
            data);
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "resetFec response: "<< linkBoxName << ": "
                << (data.bag.isGood() ? "OK" : "BAD")<<" (0x"<<std::hex<<data.bag.getStatus()<<")" );
        return data;
    }
/*
    FecStatusResponseBag readFecStatus(std::string linkBoxName, int appInstance) {
        rpct::xdaqutils::XdaqSoapAccess::TSOAPParamList paramList;
        xdata::String xLinkBoxName = linkBoxName;
        paramList.push_back(
            rpct::xdaqutils::XdaqSoapAccess::TSOAPParamList::value_type("linkBoxName", &xLinkBoxName));
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "sending readFecStatus request");
        xoap::MessageReference readFecStatusResponse = soapAccess.sendSOAPRequest(
            XdaqLBoxAccess::READ_FEC_STATUS_NAME,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS,
            paramList, "rpcttsworker::LBCell", appInstance, false);
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "readFecStatus received");

        FecStatusResponseBag data;
        soapAccess.parseSOAPResponse(readFecStatusResponse,
            XdaqLBoxAccess::READ_FEC_STATUS_NAME,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_PREFIX,
            XdaqLBoxAccess::RPCT_LBOX_ACCESS_NS,
            data);
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "readFecStatus response: "<< linkBoxName << ": "
                << (data.bag.isGood() ? "OK" : "BAD")<<" (0x"<<std::hex<<data.bag.getStatus()<<")" );
        return data;
    }
*/
};

}


#endif /*_RPCTXDAQLBOXACCESSCLIENT_H_*/
