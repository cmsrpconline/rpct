#ifndef _RPCTMASSIVEREADRESPONSE_H_
#define _RPCTMASSIVEREADRESPONSE_H_

#include "rpct/ts/worker/xdaqlboxaccess/FebValuesVector.h"
#include "rpct/ts/worker/xdaqlboxaccess/FebProperties.h"
#include "rpct/ts/worker/xdaqlboxaccess/FebErrors.h"

#include "xdata/Bag.h"
#include "xdata/Vector.h"
#include "xdata/String.h"


namespace rpct {


class MassiveReadResponse {
private:
    FebProperties properties;
    FebValuesVector febValues;
    FebErrorsVector febErrors;
public:
    void registerFields(xdata::Bag<MassiveReadResponse>* bag) {
        bag->addField("properties", &properties);
        bag->addField("febValues", &febValues);
        bag->addField("febErrors", &febErrors);
    }

    FebProperties& getProperties() {
        return properties;
    }

    void setProperties(FebProperties& properties) {
        this->properties = properties;
    }

    FebValuesVector& getFebValues() {
        return febValues;
    }

    void setFebValues(FebValuesVector& febValues) {
        this->febValues = febValues;
    }


    FebErrorsVector& getFebErrors() {
        return febErrors;
    }

    void setFebErrors(FebErrorsVector& febErrors) {
        this->febErrors = febErrors;
    }
};

}

#endif /*_MASSIVEREADRESPONSE_H_*/
