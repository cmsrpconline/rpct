#ifndef _RPCTFEBINFO_H_
#define _RPCTFEBINFO_H_


#include "xdata/Bag.h"
#include "xdata/Integer.h"
#include "xdata/String.h"
#include "xdata/Boolean.h"

namespace rpct {

class FebInfo {
private:
    xdata::Integer febId;
    xdata::String febLocalEtaPartition;
    xdata::String chamberLocationName;
    xdata::Integer ccuAddress;
    xdata::Integer channel;
    xdata::Integer address;
    xdata::Boolean endcap;
    xdata::Integer xdaqAppInstance;
public:
    void registerFields(xdata::Bag<FebInfo>* bag) {
        bag->addField("febId", &febId);
        bag->addField("febLocalEtaPartition", &febLocalEtaPartition);
        bag->addField("chamberLocationName", &chamberLocationName);
        bag->addField("ccuAddress", &ccuAddress);
        bag->addField("channel", &channel);
        bag->addField("address", &address);
        bag->addField("endcap", &endcap);
        bag->addField("xdaqAppInstance", &xdaqAppInstance);
    }
    
     xdata::Integer& getFebId() {
        return febId;
    }
    
    void setFebId(xdata::Integer febId) {
        this->febId = febId;
    }
    
    xdata::String& getFebLocalEtaPartition() {
        return febLocalEtaPartition;
    }
    
    void setFebLocalEtaPartition(xdata::String febLocalEtaPartition) {
        this->febLocalEtaPartition = febLocalEtaPartition;
    }
    
    xdata::String& getChamberLocationName() {
        return chamberLocationName;
    }
    
    void setChamberLocationName(xdata::String chamberLocationName) {
        this->chamberLocationName = chamberLocationName;
    }
    
     xdata::Integer& getCcuAddress() {
        return ccuAddress;
    }
    
    void setCcuAddress(xdata::Integer ccuAddress) {
        this->ccuAddress = ccuAddress;
    }
    
    xdata::Integer& getChannel() {
        return channel;
    }
    
    void setChannel(xdata::Integer channel) {
        this->channel = channel;
    }
    
    xdata::Integer& getAddress() {
        return address;
    }
    
    void setAddress(xdata::Integer address) {
        this->address = address;
    }
    
    xdata::Boolean& isEndcap() {
        return endcap;
    }
    
    void setEndcap(xdata::Boolean endcap) {
        this->endcap = endcap;
    }

    xdata::Integer& getXdaqAppInstance() {
        return xdaqAppInstance;
    }

    void setXdaqAppInstance(xdata::Integer xdaqAppInstance) {
        this->xdaqAppInstance = xdaqAppInstance;
    }
    
    friend std::ostream& operator<< (std::ostream& ostr, FebInfo& fi) {    
        ostr << " FEB: ccuAddress = " << ((int)fi.ccuAddress)
             << " channel = " <<  ((int)fi.channel)
             << " address = " <<  ((int)fi.address)
             << " chamberLocation = " <<  ((std::string)fi.chamberLocationName) 
             << " " << ((std::string)fi.getFebLocalEtaPartition())             
             << " endcap = " << (fi.endcap ? "true" : "false");
        return ostr;
    }
};

}

#endif /*_FEBINFO_H_*/
