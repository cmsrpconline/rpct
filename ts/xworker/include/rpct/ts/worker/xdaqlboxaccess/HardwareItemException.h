#ifndef _HARDWAREITEMEXCEPTION_H_
#define _HARDWAREITEMEXCEPTION_H_

namespace rpct {

#include "xcept/Exception.h"
#include "rpct/ii/IHardwareItem.h"
#include <list>

class HardwareItemException: public xcept::Exception {
public:
    HardwareItemException(std::string name, std::string message, std::string module, int line, std::string function) :
        xcept::Exception(name, message, module, line, function) {
    }

    HardwareItemException(std::string name, std::string message, std::string module, int line, std::string function,
            xcept::Exception& e) :
        xcept::Exception(name, message, module, line, function, e) {
    }

    virtual ~HardwareItemException() throw() {
    }

    typedef std::list<std::pair<rpct::IHardwareItem*, std::string> > Messages;

    Messages& getMessages() {
        return messages_;
    }
    void setMessages(Messages& messages) {
        messages_ = messages;
    }

    /*const char* what () const throw() {
        std::ostringstream s;
        s << xcept::Exception::what();

    }*/
private:
    Messages messages_;
};

}

#endif /*_FEBINFO_H_*/
