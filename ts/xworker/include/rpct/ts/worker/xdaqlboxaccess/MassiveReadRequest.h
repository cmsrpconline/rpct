#ifndef _RPCTMASSIVEREADREQUEST_H_
#define _RPCTMASSIVEREADREQUEST_H_


#include "rpct/ts/worker/xdaqlboxaccess/FebInfo.h"
#include "rpct/ts/worker/xdaqlboxaccess/FebProperties.h"

#include "xdata/Bag.h"
#include "xdata/Vector.h"
#include "xdata/String.h"

namespace rpct {
    
class MassiveReadRequest {
public:
    typedef xdata::Vector<xdata::Bag<FebInfo> > Febs;  
    
    static const char* PROP_TEMPERATURE;
    static const char* PROP_TEMPERATURE2;
    static const char* PROP_VTH1;
    static const char* PROP_VTH2;
    static const char* PROP_VTH3;
    static const char* PROP_VTH4;
    static const char* PROP_VMON1;
    static const char* PROP_VMON2;
    static const char* PROP_VMON3;
    static const char* PROP_VMON4;
private:
    FebProperties properties;
    Febs febs;
public:
    void registerFields(xdata::Bag<MassiveReadRequest>* bag) {
        bag->addField("properties", &properties);
        bag->addField("febs", &febs);
    }
    
    FebProperties& getProperties() {
        return properties;
    }
    
    Febs& getFebs() {
        return febs;
    }
};

}

#endif /*_MASSIVEREADREQUEST_H_*/
