#ifndef _RPCTLBOXSOAPINTERFACE_H_
#define _RPCTLBOXSOAPINTERFACE_H_


#include "rpct/devices/LinkSystem.h"
#include "rpct/devices/Feb.h"
#include "rpct/devices/FebEndcap.h"
#include "rpct/devices/cb.h"
#include "rpct/ii/IBoard.h"

#include "rpct/lboxaccess/FecManager.h"
#include "rpct/ts/worker/xdaqlboxaccess/FebValues.h"
#include "rpct/ts/worker/XdaqHardwareAccess.h"
#include "rpct/ts/worker/xdaqlboxaccess/FebProperties.h"

#include "rpct/xdaqdiagaccess/XdaqDiagAccess.h"
#include "rpct/xdaqutils/XdaqDbServiceClient.h"
#include "rpct/xdaqutils/XdaqIIAccess.h"

#include "rpct/hardwareTests/LBStatisticDiagManager.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/WebApplication.h"
#include "xdaq/NamespaceURI.h"
#include "xdaq/exception/Exception.h"

#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/fsm/FailedEvent.h"

#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/Action.h"
#include "toolbox/TimeInterval.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"

#include "xdata/Serializable.h"
#include "xdata/rpctsoap/Serializer.h"
#include "xdata/String.h"
#include "xdata/Boolean.h"
#include "xdata/Integer.h"
#include "xdata/Table.h"
#include "xdata/UnsignedLong.h"

#include "xgi/Utils.h"
#include "xgi/Method.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include <vector>
#include <map>
#include <sstream>


class XdaqLBoxAccess;

namespace rpct {

class LinkBoxSoapInterface : public toolbox::lang::Class{
public:
  LinkBoxSoapInterface(xdaq::Application * app,
		       XdaqLBoxAccess* lboxAccess,
		       toolbox::BSem* hardwareMutex);

private:
    log4cplus::Logger logger_;
    log4cplus::Logger& getLogger() {  return logger_; }

    log4cplus::Logger summaryLogger_;
    log4cplus::Logger& getSummaryLogger() {  return summaryLogger_; }

    XdaqLBoxAccess* lboxAccess_;
    rpct::xdaqutils::XdaqSoapAccess soapAccess_; // not a copy of the one from xdaqLboxAccess
    toolbox::BSem* hardwareMutex_;

    xdata::Boolean febWriteReadBack;
    xdata::Boolean febWriteAutoCorrection;

    rpct::Feb& prepareFeb(rpct::FebInfo& febInfo, bool enableDac, bool forceEnable, int autoCorrection);
    void readFeb(rpct::Feb& feb, rpct::FebProperties& properties, rpct::FebValues::Values& values);
    void writeFeb(rpct::Feb& feb, rpct::FebProperties& properties, rpct::FebValues::Values& values);
    rpct::TI2CInterface& prepareRbc(std::string linkBoxName);

public:

    static const char* ENABLE_DAC_NAME;
    static const char* READ_TEMPERATURE_NAME;
    static const char* READ_VTH1_NAME;
    static const char* WRITE_VTH1_NAME;
    static const char* READ_VTH2_NAME;
    static const char* WRITE_VTH2_NAME;
    static const char* READ_VMON1_NAME;
    static const char* WRITE_VMON1_NAME;
    static const char* READ_VMON2_NAME;
    static const char* WRITE_VMON2_NAME;
    static const char* MASSIVE_READ_NAME;
    static const char* MASSIVE_WRITE_NAME;
    static const char* READ_RBC_REGISTER_NAME;
    static const char* WRITE_RBC_REGISTER_NAME;
    static const char* READ_RBC_TEMPERATURE_NAME;


    // ********** FEBS ************
    xoap::MessageReference onEnableDAC(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference onReadTemperature(xoap::MessageReference msg) throw (xoap::exception::Exception);

    xoap::MessageReference onReadVTH1(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference onWriteVTH1(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference onReadVTH2(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference onWriteVTH2(xoap::MessageReference msg) throw (xoap::exception::Exception);

    xoap::MessageReference onReadVMon1(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference onWriteVMon1(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference onReadVMon2(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference onWriteVMon2(xoap::MessageReference msg) throw (xoap::exception::Exception);

    xoap::MessageReference onMassiveRead(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference onMassiveWrite(xoap::MessageReference msg) throw (xoap::exception::Exception);

    // ********** RBC ************
    xoap::MessageReference onReadRbcRegister(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference onWriteRbcRegister(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference onReadRbcTemperature(xoap::MessageReference msg) throw (xoap::exception::Exception);

};
}
#endif
