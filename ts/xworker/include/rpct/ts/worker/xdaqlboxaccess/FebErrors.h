#ifndef RPCTFEBERRORS_H_
#define RPCTFEBERRORS_H_

#include "rpct/ts/worker/xdaqlboxaccess/FebInfo.h"
#include "xdata/Bag.h"
#include "xdata/Vector.h"
#include "xdata/String.h"

namespace rpct {


class FebErrors {
public:
    typedef xdata::Bag<FebInfo> Feb;
private:
    Feb feb_;
    xdata::String error_;
public:
    void registerFields(xdata::Bag<FebErrors>* bag) {
        bag->addField("feb", &feb_);
        bag->addField("error", &error_);
    }

    Feb& getFeb() {
        return feb_;
    }

    void setFeb(Feb& feb) {
        feb_ = feb;
    }

    std::string getError() {
        return error_;
    }

    void setError(std::string error) {
        error_ = error;
    }
};


typedef xdata::Bag<FebErrors> FebErrorsBag;

typedef xdata::Vector<FebErrorsBag> FebErrorsVector;

}

#endif
