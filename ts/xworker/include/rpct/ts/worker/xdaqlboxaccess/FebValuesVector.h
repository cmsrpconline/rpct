#ifndef _RPCTFEBVALUESVECTOR_H_
#define _RPCTFEBVALUESVECTOR_H_

#include "rpct/ts/worker/xdaqlboxaccess/FebValuesBag.h"

namespace rpct {
    typedef xdata::Vector<FebValuesBag> FebValuesVector; 
}

#endif /*_RPCTMASSIVEWRITEREQUEST_H_*/
