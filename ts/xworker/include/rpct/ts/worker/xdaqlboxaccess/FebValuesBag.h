#ifndef _RPCTFEBVALUESBAG_H_
#define _RPCTFEBVALUESBAG_H_

#include "rpct/ts/worker/xdaqlboxaccess/FebValues.h"

namespace rpct {
    typedef xdata::Bag<FebValues> FebValuesBag;
}

#endif /*_RPCTMASSIVEWRITEREQUEST_H_*/
