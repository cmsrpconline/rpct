/*
 * LinkBoxTester.h
 *
 *  Created on: May 31, 2013
 *      Author: Karol Bunkowski
 */

#ifndef LINKBOXTESTER_H_
#define LINKBOXTESTER_H_

#include "rpct/ts/worker/xdaqlboxaccess/XdaqLBoxAccess.h"
#include "rpct/hardwareTests/FEBsTimingTester.h"

#include <log4cplus/logger.h>

class LinkBoxTester {
private:
	static log4cplus::Logger logger;
public:
	static void setupLogger(std::string namePrefix);
private:
	XdaqLBoxAccess* lboxAccess;

	rpct::LinkSystem* linkSystem_;

	rpct::LinkSystem::ControlBoards controlBoards_;
	rpct::LinkSystem::LinkBoards linkBoards_;

	rpct::LinkSystem::LinkBoards::iterator linkBoardFirst;
	rpct::LinkSystem::LinkBoards linkBoardsRange;

public:
	LinkBoxTester(XdaqLBoxAccess* lboxAccess);

	void testRegisters(rpct::LinkSystem::LinkBoards linkBoards);

	void testOptLinks();

	void testInputsWithPulses(rpct::LinkSystem::LinkBoards linkBoards);

	void checkClocks(rpct::LinkSystem::LinkBoards& linkBoards);

	virtual ~LinkBoxTester();

	/**
	 * returns false if the test was finished (e.g. due to detected errors), true if the continueTest should be called
	 */
	bool startTest(bool programFlash,  bool testWithPulses, rpct::LinkSystem::ControlBoards controlBoards, rpct::LinkSystem::LinkBoards linkBoards);

	/**
	 * returns true if the test was finished, false if the continueTest should be called again
	 */
	bool continueTest();

	bool finalizeTest();

	void monitorHardware(rpct::LinkSystem::ControlBoards controlBoards, rpct::LinkSystem::LinkBoards linkBoards);

private:
	SharedAppenderPtr appenderPtr;

	std::string linkBoxTestsDir;
	std::string thisLinkBoxTestsDir;

	rpct::FEBsTimingTester* febsTimingTester;
	rpct::FEBsTimingTester::AnalyserForTestBoard analyserForTestBoard;
};

#endif /* LINKBOXTESTER_H_ */
