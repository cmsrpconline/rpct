#ifndef _RPCTFEBPROPERTIES_H_
#define _RPCTFEBPROPERTIES_H_

#include "xdata/Vector.h"
#include "xdata/String.h"

namespace rpct {
    typedef xdata::Vector<xdata::String> FebProperties; 
}

#endif /*_RPCTMASSIVEWRITEREQUEST_H_*/
