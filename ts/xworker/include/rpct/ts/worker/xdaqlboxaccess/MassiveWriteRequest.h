#ifndef _RPCTMASSIVEWRITEREQUEST_H_
#define _RPCTMASSIVEWRITEREQUEST_H_


#include "rpct/ts/worker/xdaqlboxaccess/FebValuesVector.h"
#include "rpct/ts/worker/xdaqlboxaccess/FebProperties.h"

#include "xdata/Bag.h"
#include "xdata/Vector.h"
#include "xdata/String.h"

namespace rpct {

class MassiveWriteRequest {
public:
    static const char* PROP_VTH1;
    static const char* PROP_VTH2;
    static const char* PROP_VTH3;
    static const char* PROP_VTH4;
    static const char* PROP_VMON1;
    static const char* PROP_VMON2;
    static const char* PROP_VMON3;
    static const char* PROP_VMON4;
    static const char* PROP_AUTO_CORRECTON;
    static const int AUTO_CORRECTION_DEFAULT = 0;
    static const int AUTO_CORRECTION_FALSE = 1;
    static const int AUTO_CORRECTION_TRUE = 2;
private:
    FebProperties properties;
    FebValuesVector febValues;
    xdata::Integer autoCorrection;
public:
    MassiveWriteRequest() {
        autoCorrection = AUTO_CORRECTION_DEFAULT;
    }

    void registerFields(xdata::Bag<MassiveWriteRequest>* bag) {
        bag->addField("properties", &properties);
        bag->addField("febValues", &febValues);
        bag->addField("autoCorrection", &autoCorrection);
    }

    FebProperties& getProperties() {
        return properties;
    }

    void setProperties(FebProperties& properties) {
        this->properties = properties;
    }

    FebValuesVector& getFebValues() {
        return febValues;
    }

    void setFebValues(FebValuesVector& febValues) {
        this->febValues = febValues;
    }



    int getAutoCorrection() {
        return autoCorrection.value_;
    }

    void setAutoCorrection(int autoCorrection) {
        this->autoCorrection = autoCorrection;
    }
};

}

#endif /*_RPCTMASSIVEWRITEREQUEST_H_*/
