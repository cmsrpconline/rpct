#ifndef _RPCTXDAQLBOXACCESS_H_
#define _RPCTXDAQLBOXACCESS_H_


#include "rpct/devices/LinkSystem.h"
#include "rpct/devices/Feb.h"
#include "rpct/devices/FebEndcap.h"
#include "rpct/devices/cb.h"
#include "rpct/ii/IBoard.h"

#include "rpct/tools/RWMutex.h"
#include "rpct/xdaqutils/xdaqRPCFebSystem.h"

#include "rpct/lboxaccess/FecManager.h"
#include "rpct/ts/worker/xdaqlboxaccess/FebValues.h"
#include "rpct/ts/worker/XdaqHardwareAccess.h"
#include "rpct/ts/worker/xdaqlboxaccess/FebProperties.h"

#include "rpct/xdaqdiagaccess/XdaqDiagAccess.h"
#include "rpct/xdaqutils/XdaqDbServiceClient.h"
#include "rpct/xdaqutils/XdaqIIAccess.h"

#include "rpct/hardwareTests/LBStatisticDiagManager.h"

//#include "AutoUpdateBoolean.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/WebApplication.h"
#include "xdaq/NamespaceURI.h"
#include "xdaq/exception/Exception.h"

#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/fsm/FailedEvent.h"

#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/Action.h"
#include "toolbox/TimeInterval.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"

#include "xdata/Serializable.h"
#include "xdata/rpctsoap/Serializer.h"
#include "xdata/String.h"
#include "xdata/Boolean.h"
#include "xdata/Integer.h"
#include "xdata/Table.h"
#include "xdata/UnsignedLong.h"

#include "xgi/Utils.h"
#include "xgi/Method.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include <vector>
#include <map>
#include <sstream>


namespace rpct {
class MonitorablesWidget;
}
namespace rpcttspanels {
class HistogramsWidget;
}


class XdaqLBoxAccess: public XdaqHardwareAccess {
private:

    xdata::String systemFileName;

    std::string ringName;
    xdata::String xringName;
    std::string towerName;


public:
    xdata::Boolean enableTTCReload_;
    xdata::Boolean configureFromFlash_;

    // flashlists
    std::string flashlistISName_; // infospace
    xdata::String partitionName_;


    rpct::LBStatisticDiagManager* lbStatisticDiagManager_;
    toolbox::task::WorkLoop * histoMonitoringWorkLoop_;

    rpct::xdaqutils::xdaqRPCFebSystem * febsystem_;
private:

     toolbox::BSem resetMutex_;
    volatile time_t badResetFecLastTime_;
 public:
    inline rpct::LinkSystem* getLinkSystem() { 
        return dynamic_cast<rpct::LinkSystem*>(getSystem());
    }
 private:

    void initialise();
public:
    void buildSystem();

    virtual std::vector<int> getChipIdsToSetup();

    virtual void loadFirmware(bool forceLoad, rpct::ConfigurationSet& confSet);
    virtual void checkFirmware(rpct::ConfigurationSet& confSet); //throws exception of firmware is not correct

    virtual void configure(std::string globalConfigKey, bool enableHardResetByTTC, bool enableOptLinksOut, bool forceLoadFirmware, bool forceReset, bool forceSetup);
    virtual void configureFromFlash(std::string globalConfigKey, bool enableHardResetByTTC, bool enableOptLinksOut);
    virtual void configureFromSoftware(std::string globalConfigKey, bool forceLoadFirmware, bool forceReset, bool forceSetup);

    void resetBoards(rpct::LinkSystem::ControlBoards& controlBoards,
                         rpct::LinkSystem::LinkBoards& linkBoards, rpct::LinkSystem::RbcBoards& rbcBoards);

    rpct::xdaqutils::ConfigurationSetBagPtr getConfigurationSetForSelectedBoards(std::vector<std::string>& localConfigKeys, rpct::LinkSystem::LinkBoards linkBoards,
    		rpct::LinkSystem::RbcBoards rbcBoards);

    void configureBoards(std::vector<std::string>& configKeys, bool forceReset, bool forceConfigure, rpct::LinkSystem::ControlBoards controlBoards,
    		         rpct::LinkSystem::LinkBoards linkBoards, rpct::LinkSystem::RbcBoards rbcBoards);

    unsigned makeFlashConfig(rpct::xdaqutils::ConfigurationSetBagPtr confSetBag, rpct::LinkSystem::LinkBoards linkBoards);
    void readCBIC_RELOAD_CNT(rpct::LinkSystem::ControlBoards controlBoards);

    rpct::xdaqutils::xdaqFebSystem & getFebSystem();

private:
    void maskNoisyChannels(std::string configKey, bool preserveMasksFromDB);
    void maskNoisyChannels(std::string configKey, bool preserveMasksFromDB, rpct::LinkSystem::LinkBoards& linkBoards);

    void maskAllChannels();
    void maskAllChannels(rpct::LinkSystem::LinkBoards& linkBoards);

    void applyDBMasks(std::string configKey);
    void applyDBMasks(std::string configKey, rpct::LinkSystem::LinkBoards& linkBoards);

public:
    void reset();
    rpct::FecManager& getFecManager();
    void resetFec(unsigned int* status = 0, bool vmeHardInit=false);
    void loadFromCcu(rpct::LinkSystem::ControlBoards controlBoards, rpct::LinkSystem::LinkBoards linkBoards);
    void loadFromFlash(rpct::LinkSystem::ControlBoards controlBoards, rpct::LinkSystem::LinkBoards linkBoards);
    void loadFromFlashParalel(rpct::LinkSystem::ControlBoards controlBoards, rpct::LinkSystem::LinkBoards linkBoards);

    void programFlash(rpct::LinkSystem::ControlBoards controlBoards, rpct::LinkSystem::LinkBoards linkBoards);
    void checkConfig(bool dontReload, rpct::LinkSystem::ControlBoards controlBoards, rpct::LinkSystem::LinkBoards linkBoards);

    void loadFromCcu();
    void loadFromCcuCb(rpct::LinkSystem::ControlBoards controlBoards);
    void loadFromCcuLb(rpct::LinkSystem::LinkBoards linkBoards);

    void loadFromFlash();
    void checkConfig(bool dontReload);

    void programFlash();
    void programFlashCb(rpct::LinkSystem::ControlBoards controlBoards);
    void programFlashLb(rpct::LinkSystem::LinkBoards linkBoards);

private:

    bool readFecStatus(unsigned int &status);
    
    std::string getBootDir();


public:
    void synchronizeLinks();

    void testLBs(rpct::LinkSystem::LinkBoards linkBoards);

    void checkHardware();

    void resetCBTTCrxs(rpct::LinkSystem::ControlBoards controlBoards);

    void resetAllNotReadyCBTTCrxs();
    void resetAllNotReadyCBTTCrxsNoMutex();

    void resetAllCBTTCrxs();


    void linkBoxTest(rpct::LinkSystem::ControlBoards controlBoards, rpct::LinkSystem::LinkBoards linkBoards);


    void synchronizeLinks(rpct::LinkSystem::LinkBoards linkBoards, rpct::LinkSystem::RbcBoards rbcBoards);

private:
    void readLBoxFirmwareVer(bool verbose=false); // false = print the most essential registers only
    void testControlChannel();

public:
    static const char* PROGRAM_FLASH_NAME;
    static const char* LOAD_FROM_FLASH_NAME;
    static const char* LOAD_FROM_CCU_NAME;
    static const char* RESET_FEC_NAME;
    static const char* READ_LBOX_FIRMWARE_VER_NAME;
    static const char* APPLY_DB_MASKS_NAME;

    static const char* RPCT_LBOX_ACCESS_NS;
    static const char* RPCT_LBOX_ACCESS_PREFIX;


    XdaqLBoxAccess(xdaq::Application * app,
                   rpct::MonitorablesWidget* monitorablesWigdet,
                   rpcttspanels::HistogramsWidget* histogramsWidget);

    virtual ~XdaqLBoxAccess();


    // LBOX & FEC configuration
//  xoap::MessageReference onReadFecStatus(xoap::MessageReference msg) throw (xoap::exception::Exception);  // MC 1 Aug 2008
    xoap::MessageReference onResetFec(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference onProgramFlash(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference onLoadFromFlash(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference onLoadFromCcu(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference onReadLBoxFirmwareVer(xoap::MessageReference msg) throw (xoap::exception::Exception);

    xoap::MessageReference onApplyDBMasks(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference onGetMonitoringData(xoap::MessageReference msg) throw (xoap::exception::Exception);


    void checkConfigured();

    virtual void enable();

    std::vector<std::string> getFEBConfigKeys();
    std::vector<std::string> getLBConfigKeys();
    std::vector<std::string> getRBCConfigKeys();

};

#endif
