#ifndef _RPCTFECSTATUSRESPONSE_H_
#define _RPCTFECSTATUSRESPONSE_H_

#include "xdata/Bag.h"
#include "xdata/Vector.h"
#include "xdata/String.h"


namespace rpct {

    class FecStatusResponse {
    private:
        xdata::Boolean FecIsGood;
        xdata::Integer  FecStatus;
    public:
        void registerFields(xdata::Bag<FecStatusResponse>* bag) {
            bag->addField("FecIsGood", &FecIsGood);
            bag->addField("FecStatus", &FecStatus);
        }

        unsigned int getStatus() {
            return (unsigned int)FecStatus;
        }

        bool isGood() {
            return (bool)FecIsGood;
        }

        void set(bool isGood, unsigned int &status) {
            this->FecIsGood = (xdata::Boolean)isGood;
            this->FecStatus = (xdata::Integer)status;
        }
    };
}

#endif /*_RPCTFECSTATUSRESPONSE_H_*/
