/** Filip Thyssen */

#ifndef rpct_ts_worker_FebConnectivityTest_inl_h_
#define rpct_ts_worker_FebConnectivityTest_inl_h_

#include "rpct/ts/worker/FebConnectivityTest.h"

namespace rpcttsworker {

inline log4cplus::Logger & FebConnectivityTest::getLogger()
{
    return logger_;
}

inline rpct::LoggerWidget & FebConnectivityTest::getLoggerWidget()
{
    return logger_widget_;
}

inline rpct::tools::Progress & FebConnectivityTest::getProgress()
{
    return progress_;
}

inline rpct::ProgressBarWidget & FebConnectivityTest::getProgressBarWidget()
{
    return progress_widget_;
}

inline XdaqLBoxAccess & FebConnectivityTest::getXdaqLBoxAccess()
{
    return xdaqlboxaccess_;
}

inline rpct::xdaqutils::XdaqDbServiceClient & FebConnectivityTest::getDBClient()
{
    return dbclient_;
}

inline std::string const & FebConnectivityTest::getHost() const
{
    return host_;
}

inline int FebConnectivityTest::getPort() const
{
    return port_;
}

inline std::string const & FebConnectivityTest::getFileName() const
{
    return filename_;
}

inline rpct::tools::RollSelection const & FebConnectivityTest::getSelection() const
{
    return selection_;
}

inline rpct::tools::Time const & FebConnectivityTest::getCountDuration() const
{
    return count_duration_;
}

inline int FebConnectivityTest::getVTh() const
{
    return vth_;
}

inline int FebConnectivityTest::getVMon() const
{
    return vmon_;
}

inline int FebConnectivityTest::getVThMin() const
{
    return vth_min_;
}

inline int FebConnectivityTest::getVThStepSize() const
{
    return vth_step_size_;
}

inline int FebConnectivityTest::getVThNSteps() const
{
    return vth_n_steps_;
}

inline int FebConnectivityTest::getVThLow() const
{
    return vth_low_;
}

inline int FebConnectivityTest::getVThHigh() const
{
    return vth_high_;
}

inline bool FebConnectivityTest::getAutoCorrect() const
{
    return auto_correct_;
}

inline bool FebConnectivityTest::getUsePulser() const
{
    return use_pulser_;
}

inline bool FebConnectivityTest::getIncludeDisabled() const
{
    return include_disabled_;
}

inline bool FebConnectivityTest::getStoreOutput() const
{
    return store_output_;
}

inline void FebConnectivityTest::setSelection(rpct::tools::RollSelection const & _selection)
{
    if (getProgress().getState() == rpct::tools::Progress::idle_)
        {
            rpct::tools::WLock _lock(mutex_);
            selection_ = _selection;
        }
    else
        LOG4CPLUS_WARN(logger_, "Cannot change Roll Selection while a test is ongoing.");
}

inline void FebConnectivityTest::setCountDuration(rpct::tools::Time const & _duration)
{
    if (getProgress().getState() == rpct::tools::Progress::idle_)
        {
            rpct::tools::WLock _lock(mutex_);
            count_duration_ = _duration;
        }
    else
        LOG4CPLUS_WARN(logger_, "Cannot change Counter Duration while a test is ongoing.");
}

inline void FebConnectivityTest::setVTh(int _vth)
{
    if (getProgress().getState() == rpct::tools::Progress::idle_)
        {
            rpct::tools::WLock _lock(mutex_);
            vth_ = _vth;
        }
    else
        LOG4CPLUS_WARN(logger_, "Cannot change threshold voltage while a test is ongoing.");
}

inline void FebConnectivityTest::setVMon(int _vmon)
{
    if (getProgress().getState() == rpct::tools::Progress::idle_)
        {
            rpct::tools::WLock _lock(mutex_);
            vmon_ = _vmon;
        }
    else
        LOG4CPLUS_WARN(logger_, "Cannot change monostable voltage while a test is ongoing.");
}

inline void FebConnectivityTest::setVThMin(int _vth_min)
{
    if (getProgress().getState() == rpct::tools::Progress::idle_)
        {
            rpct::tools::WLock _lock(mutex_);
            vth_min_ = _vth_min;
        }
    else
        LOG4CPLUS_WARN(logger_, "Cannot change minimum threshold while a test is ongoing.");
}

inline void FebConnectivityTest::setVThStepSize(int _vth_step_size)
{
    if (getProgress().getState() == rpct::tools::Progress::idle_)
        {
            rpct::tools::WLock _lock(mutex_);
            vth_step_size_ = _vth_step_size;
        }
    else
        LOG4CPLUS_WARN(logger_, "Cannot change threshold step size while a test is ongoing.");
}

inline void FebConnectivityTest::setVThNSteps(int _vth_n_steps)
{
    if (getProgress().getState() == rpct::tools::Progress::idle_)
        {
            rpct::tools::WLock _lock(mutex_);
            vth_n_steps_ = _vth_n_steps;
        }
    else
        LOG4CPLUS_WARN(logger_, "Cannot change number of threshold steps while a test is ongoing.");
}

inline void FebConnectivityTest::setVThLow(int _vth_low)
{
    if (getProgress().getState() == rpct::tools::Progress::idle_)
        {
            rpct::tools::WLock _lock(mutex_);
            vth_low_ = _vth_low;
        }
    else
        LOG4CPLUS_WARN(logger_, "Cannot change low threshold level while a test is ongoing.");
}

inline void FebConnectivityTest::setVThHigh(int _vth_high)
{
    if (getProgress().getState() == rpct::tools::Progress::idle_)
        {
            rpct::tools::WLock _lock(mutex_);
            vth_high_ = _vth_high;
        }
    else
        LOG4CPLUS_WARN(logger_, "Cannot change high threshold level while a test is ongoing.");
}

inline void FebConnectivityTest::setAutoCorrect(bool _auto_correct)
{
    if (getProgress().getState() == rpct::tools::Progress::idle_)
        {
            rpct::tools::WLock _lock(mutex_);
            auto_correct_ = _auto_correct;
        }
    else
        LOG4CPLUS_WARN(logger_, "Cannot change auto correct while a test is ongoing.");
}

inline void FebConnectivityTest::setUsePulser(bool _use_pulser)
{
    if (getProgress().getState() == rpct::tools::Progress::idle_)
        {
            rpct::tools::WLock _lock(mutex_);
            use_pulser_ = _use_pulser;
        }
    else
        LOG4CPLUS_WARN(logger_, "Cannot change pulser use while a test is ongoing.");
}

inline void FebConnectivityTest::setIncludeDisabled(bool _include_disabled)
{
    if (getProgress().getState() == rpct::tools::Progress::idle_)
        {
            rpct::tools::WLock _lock(mutex_);
            include_disabled_ = _include_disabled;
        }
    else
        LOG4CPLUS_WARN(logger_, "Cannot change disabled inclusion while a test is ongoing.");
}

inline void FebConnectivityTest::setStoreOutput(bool _store_output)
{
    if (getProgress().getState() == rpct::tools::Progress::idle_)
        {
            rpct::tools::WLock _lock(mutex_);
            store_output_ = _store_output;
        }
    else
        LOG4CPLUS_WARN(logger_, "Cannot change store output while a test is ongoing.");
}

} // namespace rpcttsworker

#endif // rpct_ts_worker_FebConnectivityTest_inl_h_
