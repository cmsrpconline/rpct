
#ifndef _rpct_ts_worker_TBCellContext_h_
#define _rpct_ts_worker_TBCellContext_h_

#include <string>
#include <map>

#include "rpct/ts/worker/RpctCellContext.h"
#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellXhannelCell.h"
#include "ts/framework/CellXhannelXdaqSimple.h"
#include "ts/framework/CellXhannel.h"
#include "ts/framework/CellXhannelRequest.h"

class XdaqTCrateAccess;


namespace rpcttsworker{


  class TBCellContext: public RpctCellContext
    {
    public:
        
      TBCellContext(log4cplus::Logger& log , tsframework::CellAbstract* cell);
      ~TBCellContext();


      
      XdaqTCrateAccess* getTCrateAccess(){
          return tcCrateAccess_;
      }

    private:
        XdaqTCrateAccess* tcCrateAccess_;
    };
}
#endif
