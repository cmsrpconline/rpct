/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Marc Magrans de Abril	                                 *
 *************************************************************************/

#ifndef _rpct_ts_worker_XdaqManager_h_
#define _rpct_ts_worker_XdaqManager_h_

#include "ts/framework/CellOperation.h"
#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellXhannelXdaqSimple.h"
#include "ts/system/ConfigurationBase.h"
#include <string>
#include "CellContext.h"
#include "log4cplus/logger.h"

using namespace tsframework;
namespace rpcttsworker{

class AsyncCommandList;
class rpcttsworker::CellContext;

class IResultSetter{
public:
	virtual void setResult(const std::string& result) = 0;
};

class CellOperationResultSetter : public IResultSetter{

	tsframework::CellOperation* cellOperation;
public:
	CellOperationResultSetter(tsframework::CellOperation* cellOperation){
		this->cellOperation = cellOperation;
	};
	virtual void setResult(const std::string& result){
		this->cellOperation->setResult(result);
	}
};

class XdaqManager
{
public:
	XdaqManager(rpcttsworker::CellContext* context);
	XdaqManager(rpcttsworker::CellContext* context, IResultSetter* resultSetter);
    virtual ~XdaqManager();

private:
    rpcttsworker::CellContext* context;
    log4cplus::Logger logger;
    tsframework::CellXhannelXdaqSimple* xdaqXhannelBuffer;
    IResultSetter* resultSetter;

public:
    tsframework::CellXhannelXdaqSimple& getXdaqXhannelByClass(std::string className);

    std::vector<tsframework::CellXhannelXdaqSimple*>* getXdaqXhannelsListByClass(std::string className);

    void setParameter(tsframework::CellXhannelXdaqSimple& x, const std::string& name,
            const std::string& nameSpace, const std::string& type, const std::string& value);
    std::string getParameter(tsframework::CellXhannelXdaqSimple& x, const std::string& name,
            const std::string& nameSpace, const std::string& type);
    void doCommand(tsframework::CellXhannelXdaqSimple& x, const std::string& name,
            const std::string& nameSpace);

    void doAsyncCommand(tsframework::CellXhannelXdaqSimple& x,
            const std::string& name,
            const std::string& commandNameSpace,
            const std::string& parameterNameSpace
            );
    std::vector<std::string>* doMultipleAsyncCommand(std::vector<CellXhannelXdaqSimple*>* xdaqList,
            const std::string& name, const std::string& commandNameSpace,
            const std::string& parameterNameSpace
            );



    void doMultipleParallelAsyncCommand(std::vector<AsyncCommandList*>* commands);

    std::string getXhannelDescription(CellXhannelXdaqSimple& xhannel);

private:
    void fillResults(std::vector<AsyncCommandList*>* commands);
};
}
#endif
