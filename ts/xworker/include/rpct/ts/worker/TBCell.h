/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: I. Magrans          				         *
 *************************************************************************/

#ifndef _rpct_ts_worker_TBCell_h_
#define _rpct_ts_worker_TBCell_h_

#include "ts/framework/CellAbstract.h"

#include "rpct/ts/worker/TBCellContext.h"

#include <string>
namespace rpcttsworker{
  class TBCell: public tsframework::CellAbstract
    {
    public:
      
      XDAQ_INSTANTIATOR();
      
      TBCell(xdaq::ApplicationStub * s);
      
      //!This method should be filled with addCommand and addOperation that corresponfs to that Cell
      void init();
      
      std::string getClassName()
	{
	  return "Cell";
	};
      
      //!Returns the CellContext pointer. CellContext contains the shared objects of the Cell 
      TBCellContext* getContext()
	{
	  TBCellContext* c(dynamic_cast<TBCellContext*>(cellContext_));
	  
	  if (!c)
	    XCEPT_RAISE(tsexception::CellException,"The Cell Context is not of class TBCellContext");
	  
	  return c;
	};
    };
}
#endif
