#include "ts/framework/CellPanel.h"
// #include "swatchcell/framework/CellContext.h"
#include "rpct/ts/worker/RpctCellContext.h"
#include "jsoncpp/json/json.h"
#include <string>
namespace rpcttspanels
{
  class LogPanel : public tsframework::CellPanel
  {
    public:
    LogPanel(tsframework::CellAbstractContext* context, log4cplus::Logger& logger, bool isMonitoring = false, std::string panelTitle = "Log Messages");
    log4cplus::Logger mLogger;
    rpcttsworker::LoggerCellContext& mCellContext;
    bool mIsMonitoring;
    std::string mPanelTitle;
    
    void getLoggers(cgicc::Cgicc& cgi, std::ostream& out);
    void getLog(cgicc::Cgicc& cgi, std::ostream& out);
    std::string localLogLevel(log4cplus::LogLevel aLevel );
    ~LogPanel();
    //! Layout method; called when panel is created
    void layout(cgicc::Cgicc& cgi);
  };
} 
