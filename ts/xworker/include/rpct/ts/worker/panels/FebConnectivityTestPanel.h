#ifndef _rpct_ts_worker_panels_FebConnectivityTestPanel_h_
#define _rpct_ts_worker_panels_FebConnectivityTestPanel_h_

#include <string>
#include <vector>
#include <utility>

#include "ts/framework/CellPanel.h"

#include "rpct/ts/common/LoggerWidget.h"
#include "rpct/ts/common/ProgressBar.h"
#include "rpct/ts/common/RollSelectionWidget.h"

namespace ajax {
class ResultBox;
class Div;
class Editable;
} // namespace ajax

namespace rpct {
namespace ts {
namespace worker {
class FebConfigurationCellContext;
} // namespace worker
} // namespace ts
} // namespace rpct

namespace rpcttsworker {

class FebConnectivityTest;

class FebConnectivityTestPanel : public tsframework::CellPanel
{
private:
    void layout(cgicc::Cgicc & _cgi);

protected:
    void initLayout(const std::string & _title);

    void onLockClick(cgicc::Cgicc & _cgi, std::ostream & _out);

    void onStartFebConfiguration(cgicc::Cgicc & _cgi, std::ostream & _out);
    void onStartFebConnectivityTest(cgicc::Cgicc & _cgi, std::ostream & _out);
    void onStartFebThresholdScan(cgicc::Cgicc & _cgi, std::ostream & _out);
    void onReadRollSelection(cgicc::Cgicc & _cgi, std::ostream & _out);
    void onSelectBarrel(cgicc::Cgicc & _cgi, std::ostream & _out);
    void onSelectEndcap(cgicc::Cgicc & _cgi, std::ostream & _out);
    void onSelectByName(cgicc::Cgicc & _cgi, std::ostream & _out);
    void onClearSelection(cgicc::Cgicc & _cgi, std::ostream & _out);
    void onGetFebUnit(cgicc::Cgicc & _cgi, std::ostream & _out);

    void onMonitorPauseFEBsClick(cgicc::Cgicc & _cgi, std::ostream & _out);
    void onMonitorResumeFEBsClick(cgicc::Cgicc & _cgi, std::ostream & _out);
    void onMonitorTerminateFEBsClick(cgicc::Cgicc & _cgi, std::ostream & _out);
    void onMonitorRefreshFEBsClick(cgicc::Cgicc & _cgi, std::ostream & _out);

public:
    FebConnectivityTestPanel(tsframework::CellAbstractContext * _context
                             , log4cplus::Logger & _logger);
    ~FebConnectivityTestPanel();

protected:
    rpct::ts::worker::FebConfigurationCellContext & context_;
    log4cplus::Logger logger_;
    
    rpct::RollSelectionWidget rollselection_widget_;

    bool locked_;

};

} // namespace rpcttsworker

#endif // _rpct_ts_worker_panels_FebConnectivityTestPanel_h_
