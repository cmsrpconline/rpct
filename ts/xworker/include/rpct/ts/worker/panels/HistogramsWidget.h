#ifndef _RPCT_HISTOGRAMSSWIDGET_H_
#define _RPCT_HISTOGRAMSESWIDGET_H_

#include <string>
#include "toolbox/BSem.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "xgi/Utils.h"
#include "xgi/Method.h"

#include "ajax/Widget.h"
#include "xdata/Serializable.h"
#include "xdata/UnsignedLong.h"
#include "xdata/Integer.h"
#include "xdata/String.h"
#include "xdata/Table.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/Serializable.h"
#include "xdata/exception/Exception.h"
#include "xdaq/Application.h"


#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/Action.h"

namespace rpct {
class IStatisticDiagManager;
class MonitorablesWidget;
}

namespace rpcttspanels{

typedef std::map<std::string, rpct::IStatisticDiagManager*> StatisticDiagManagersMap;

class HistogramsWidget : public ajax::Widget, public toolbox::lang::Class
{

private:

    StatisticDiagManagersMap statisticDiagManagersMap_;

   
    toolbox::BSem mutex_;
    xdaq::Application* app_;
    std::string filenamePrefix_;
    std::string treeName_; 
    std::string runNumber_;

    toolbox::BSem* hardwareMutex_;
    bool logScale_;
    volatile bool stopHisto_;
    rpct::MonitorablesWidget* monitorablesWidget_;
    toolbox::task::WorkLoop* histoMonitoringWorkLoop_;

public:
    HistogramsWidget(xdaq::Application* app, rpct::MonitorablesWidget* monitorablesWidget);

    //!Creates the html/javascript to display the Widget
    void html(cgicc::Cgicc& cgi, std::ostream& out);

    void initialise(std::string filenamePrefix, std::string treeName) {
        filenamePrefix_ = filenamePrefix;
        treeName_ = treeName;
    }

    void clear();

    void addManager(std::string name, rpct::IStatisticDiagManager* manager);

    bool histoMonitorJob(toolbox::task::WorkLoop* wl);

    void start(std::string runNumber, toolbox::BSem* hardwareMutex);

    void stop();
    
    bool isActive() { return histoMonitoringWorkLoop_->isActive(); };
};

}
#endif
