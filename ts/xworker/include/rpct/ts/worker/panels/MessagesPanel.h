/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Project Manager: Ildfons Magrans de Abril      				         *
 * Authors: Marc Magrans de Abril               				         *
 *************************************************************************/

#ifndef _rpct_ts_cell_panels_MessagesPanel_h_
#define _rpct_ts_cell_panels_MessagesPanel_h_

#include "ts/framework/CellPanel.h"
#include "ts/framework/CellXhannelCell.h"
#include "ts/framework/CellAbstractContext.h"

#include "xdata/Table.h"

#include "rpct/ts/worker/RpctCellContext.h"

#include <map>
#include <string>
#include <list>
#include <vector>

namespace rpcttspanels {

class MessagesPanel : public tsframework::CellPanel {
public:
    MessagesPanel(tsframework::CellAbstractContext* context, log4cplus::Logger& logger);
    ~MessagesPanel();

    void layout(cgicc::Cgicc& cgi);

    // log4cplus::Logger mLogger;
    // rpct::LoggerWidget * mLoggerwidget;
    // ajax::ResultBox* resultBox_;

private:
    void refresh(cgicc::Cgicc& cgi,std::ostream& out);
    void autorefresh(cgicc::Cgicc& cgi,std::ostream& out);
    void testButtonWidget(cgicc::Cgicc& cgi, std::ostream& out);

 
};
};
#endif
