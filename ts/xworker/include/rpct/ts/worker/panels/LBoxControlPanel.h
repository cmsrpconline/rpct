
#ifndef _rpct_ts_worker_panels_LBoxControlPanel_h_
#define _rpct_ts_worker_panels_LBoxControlPanel_h_

#include "rpct/xdaqutils/XdaqDbServiceClient.h"
#include "rpct/ts/common/StringWidget.h"

#include "ts/framework/CellPanel.h"
#include "ts/framework/CellXhannelCell.h"
#include "rpct/ts/common/ajaxtools.h"

#include "xdata/Table.h"

#include "ajax/Table.h"
#include "ajax/ResultBox.h"


#include "ajax/PlainHtml.h"

#include "rpct/devices/System.h"
#include "rpct/devices/LinkBoard.h"
#include "rpct/devices/LinkBox.h"
#include "rpct/devices/LinkSystem.h"
#include "rpct/ts/worker/xdaqlboxaccess/LinkBoxTester.h"


#include <map>
#include <string>
#include <list>
#include <vector>

namespace rpcttsworker {
  class RpctCellContext;
}
class XdaqLBoxAccess;


namespace rpcttspanels {



class LBoxControlPanel : public tsframework::CellPanel {
public:
    LBoxControlPanel(tsframework::CellAbstractContext* context, log4cplus::Logger& logger);
    ~LBoxControlPanel();

    void layout(cgicc::Cgicc& cgi);

private:
    void refresh(cgicc::Cgicc& cgi,std::ostream& out);
    void initLayout(rpcttsworker::RpctCellContext* rpctContext);
    ajax::Table* initCheckBoxTable();

    ajax::ResultBox* resultTable_;

    XdaqLBoxAccess* lboxAccess_;
    std::string lbConfigKey_;
    std::string rbcConfigKey_;  
 public:
    class BoardView {
    public:
      rpct::IBoard* board_;
      // ajax::CheckBox cbUse_;
      //      rpcttsworker::StringWidget name;
      BoardView(rpct::IBoard* board, ajax::AutoHandledWidget* widget);
      void onCrateCheckBoxClick(cgicc::Cgicc& cgi, std::ostream& out);
    };

    class HalfBoxView {
    public:
      rpct::HalfBox* halfBox_;
      ajax::ResultBox* resultTable_;

      std::vector<BoardView*> linkBoards_;
      BoardView* controlBoard_;
      BoardView* rbcBoard_;

      rpcttsworker::StringWidget label_;


      HalfBoxView(rpct::HalfBox* halfBox, ajax::AutoHandledWidget* widget, ajax::ResultBox* resultTable);
      ~HalfBoxView();

      void updateLists(std::vector<BoardView*>& linkBoards, std::vector<BoardView*>& controlBoards, std::vector<BoardView*>& rbcBoards);
      void onSelect(cgicc::Cgicc& cgi, std::ostream& out);
      void onDeselect(cgicc::Cgicc& cgi, std::ostream& out);
    };
 private:
    std::vector<BoardView*> linkBoards_;
    std::vector<BoardView*> controlBoards_;
    std::vector<BoardView*> rbcBoards_;

    std::vector<HalfBoxView*> halfBoxes_;

    rpct::LinkSystem::LinkBoards getSelectedLinkBoards(std::string boardsStr);
    rpct::LinkSystem::ControlBoards getSelectedControlBoards(std::string boardsStr);
    rpct::LinkSystem::RbcBoards getSelectedRbcBoards(std::string boardsStr);
    
    LinkBoxTester* linkBoxTester_;

    void GetHalfBoxes(cgicc::Cgicc& cgi, std::ostream& out);
    void GetRBCConfigKeys(cgicc::Cgicc& cgi, std::ostream& out);
    void GetLBConfigKeys(cgicc::Cgicc& cgi, std::ostream& out);

    void updateHalfBoxList(rpct::IBoard* board, rpct::HalfBox* halfBox);

    void onResetFec(cgicc::Cgicc& cgi, std::ostream& out);
    void onHardReset(cgicc::Cgicc& cgi, std::ostream& out);
    void onCheckHardware(cgicc::Cgicc& cgi, std::ostream& out);

    void onLoadFromCCU(cgicc::Cgicc& cgi, std::ostream& out);
    void onLoadFromFlash(cgicc::Cgicc& cgi, std::ostream& out);
    void onProgramFlash(cgicc::Cgicc& cgi, std::ostream& out);
    void onSetupBoards(cgicc::Cgicc& cgi, std::ostream& out);
    void onMakeFlashConfig(cgicc::Cgicc& cgi, std::ostream& out);
    void onReadCBIC_RELOAD_CNT(cgicc::Cgicc& cgi, std::ostream& out);
    void onTest(cgicc::Cgicc& cgi, std::ostream& out);
    void onSynchronizeLinks(cgicc::Cgicc& cgi, std::ostream& out);
    void onResetCBTTCrxs(cgicc::Cgicc& cgi, std::ostream& out);
    void onResetAllNotReadyCBTTCrxs(cgicc::Cgicc& cgi, std::ostream& out);

    void onSelectAll(cgicc::Cgicc& cgi, std::ostream& out);
    void onSelectControlBoards(cgicc::Cgicc& cgi, std::ostream& out);
    void onSelectRbcBoards(cgicc::Cgicc& cgi, std::ostream& out);
    void onSelectLinkBoards(cgicc::Cgicc& cgi, std::ostream& out);
    void onDeselectAll(cgicc::Cgicc& cgi, std::ostream& out);
    void onDeselectControlBoards(cgicc::Cgicc& cgi, std::ostream& out);
    void onDeselectRbcBoards(cgicc::Cgicc& cgi, std::ostream& out);
    void onDeselectLinkBoards(cgicc::Cgicc& cgi, std::ostream& out);

    void onChangeLbKey(cgicc::Cgicc& cgi, std::ostream& out);
    void onChangeRbcKey(cgicc::Cgicc& cgi, std::ostream& out);

    void onLinkBoxTest(cgicc::Cgicc& cgi, std::ostream& out);
    void onLinkBoxTestContinue(cgicc::Cgicc& cgi, std::ostream& out);


};
};
#endif
