#ifndef _RPCT_MONITORABLESWIDGET_H_
#define _RPCT_MONITORABLESWIDGET_H_

#include <string>
#include "toolbox/BSem.h"
#include "rpct/ii/Monitorable.h"
#include "rpct/ii/IBoard.h"

#include "rpct/devices/System.h"
#include "rpct/xdaqutils/xdaqRPCFebSystem.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "xgi/Utils.h"
#include "xgi/Method.h"

#include "ajax/Widget.h"
#include "xdata/Serializable.h"
#include "xdata/UnsignedLong.h"
#include "xdata/Integer.h"
#include "xdata/String.h"
#include "xdata/Table.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/Serializable.h"
#include "xdata/exception/Exception.h"
#include "xdaq/Application.h"
#include "ts/framework/Level1Source.h"

#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/task/Action.h"


namespace rpct{

typedef std::vector<rpct::Monitorable*> Monitorables;
struct MonitorablesInBoard {
    rpct::IBoard* board;
    Monitorables monitorables;
};

/*
class MonitoringError {
private:
	unsigned int severity_;
	MonitorItem monitorItem_;
	std::string actionToBeTaken_;
	stf::string message_;
};
*/


class MonitorablesWidget : public ajax::Widget, public xdata::ActionListener
//,  public toolbox::task::TimerListener
{

private:
	static log4cplus::Logger logger_;

    static const char* STATUS_OK_STR;
    
    static const char* MON_COL_NAME;
    static const char* MON_COL_ITEMS;
    static const char* MON_COL_STATUS;

    static const char* AL_COL_NAME;
    static const char* AL_COL_SEVERITY;
    static const char* AL_COL_COUNT;
    static const char* AL_COL_MESSAGE;

    static const char* CALL_TRIGGER;
    static const char* CALL_RPC;
    static const char* RECONFIGURE;
    static const char* HARDRESET;
    static const char* INFORM;

    xdata::Table monitorItemsStatus_;
    xdata::Table alarmsTable_;
    time_t monLastTime_;
    xdata::Integer hardwareMonStatus_; 
    toolbox::BSem mutex_;

    int rateMonStatus_;

    // flashlists
    std::string flashlistISName_; // infospace
    xdata::String partitionName_;
    xdata::Integer monTest_;

    toolbox::task::Timer * timer_;


    std::vector<MonitorablesInBoard> monitorablesPerBoard_;
    size_t monItemIndex_;

    std::string messages_;
    std::string foundErrors_;
    tsframework::Level1Source::SeverityLevel severity_;

    unsigned int monitoringIteration_;

    typedef std::map<std::string, MonitorableStatus> GlobalStatusMap;
    GlobalStatusMap globalStatusMap;//we will use this map to count the number of boards with different errors

    toolbox::BSem* hardwareMutex_;

    xdaq::Application* app_;
    tsframework::Level1Source& level1Source_;

    rpct::System* system_;
    xdaqutils::xdaqRPCFebSystem * febsystem_;
public:

    MonitorablesWidget(xdaq::Application* app, tsframework::Level1Source& level1Source);

    bool isMonitoringOn();
    void startMonitoring();
    void stopMonitoring();

    void monitorHardware(volatile bool *stop);
    void collectMonitorableStaus();
    //void timeExpired(toolbox::task::TimerEvent& e);

    //!Creates the html/javascript to display the Widget
    void html(cgicc::Cgicc& cgi, std::ostream& out);

    void setHardwareStatus(int s);
    void setMonitorLastTime(time_t& t){ monLastTime_ = t; };
    void setPartitionName(xdata::String& name) { partitionName_ = name; }; // WARNING: this has to set after constructing object, otherwise flashlists will be meaningless
    void clearTables();
    void addTableRow(xdata::String& name, xdata::String& items, xdata::String& status);
    void updateStatus(int row, xdata::String& status);

    void handleMonitorWarning(MonitorableStatus monStatus, std::string deviceName);

    void handleMonitorException(std::exception& e, std::string where);

    void handleRateWarnings(Monitorable::MonitorStatusList& monitorStatusList);

    void analyseAndGenerateAlarms();

    void clearGlobalStatus() {
    	globalStatusMap.clear();
    	severity_ = tsframework::Level1Source::INFO;
    	messages_.clear();
    }

    void updatel1Source();

    void actionPerformed(xdata::Event& e);

    void initialise(rpct::System* system, toolbox::BSem* hardwareMutex, bool lboxSort = false, xdaqutils::xdaqRPCFebSystem * febsystem = 0);
};

}
#endif
