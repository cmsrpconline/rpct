/*************************************************************************
 * RPCT Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: Krzysztof Niemkiewicz               				         *
 *************************************************************************/

#ifndef _rpct_ts_worker_panels_HistogramPanel_h_
#define _rpct_ts_worker_panels_HistogramPanel_h_

#include "ts/framework/CellPanel.h"

#include "xdata/Table.h"

#include <map>
#include <vector>
#include <string>

using namespace std;

namespace rpcttspanels {

class HistogramsWidget;

class HistogramPanel : public tsframework::CellPanel {
public:
    HistogramPanel(tsframework::CellAbstractContext* context, log4cplus::Logger& logger);
    ~HistogramPanel();

    void layout(cgicc::Cgicc& cgi);

private:
    HistogramsWidget* histogramsWidget_;

};

}

#endif
