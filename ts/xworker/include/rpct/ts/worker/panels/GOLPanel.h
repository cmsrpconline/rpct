
#ifndef _rpct_ts_worker_panels_GOLPanel_h_
#define _rpct_ts_worker_panels_GOLPanel_h_

#include "rpct/xdaqutils/XdaqDbServiceClient.h"
#include "rpct/ts/common/StringWidget.h"

#include "ts/framework/CellPanel.h"
#include "ts/framework/CellXhannelCell.h"

#include "xdata/Table.h"

#include "ajax/Table.h"
#include "ajax/ResultBox.h"
#include "ajax/Timer.h"
#include "ajax/CheckBox.h"
#include "ajax/SubmitButton.h"
#include "ajax/Button.h"
#include "ajax/InputText.h"

#include "rpct/devices/System.h"
#include "rpct/devices/LinkBoard.h"
#include "rpct/devices/LinkBox.h"
#include "rpct/devices/LinkSystem.h"


#include <map>
#include <string>
#include <list>
#include <vector>

namespace rpcttsworker {
  class RpctCellContext;
}
class XdaqTCrateAccess;
namespace rpct {
  class Rmb;
}

namespace rpcttspanels {



class GOLPanel : public tsframework::CellPanel {
public:
    GOLPanel(tsframework::CellAbstractContext* context, log4cplus::Logger& logger);
    ~GOLPanel();

    void layout(cgicc::Cgicc& cgi);



private:
    void refresh(cgicc::Cgicc& cgi,std::ostream& out);
    void initLayout(rpcttsworker::RpctCellContext* rpctContext);

    ajax::ResultBox* resultTable_;
    XdaqTCrateAccess* tCrateAccess_;

    class GOLView {
    public:
      GOLPanel* panel_;

      rpcttsworker::StringWidget name_;
      ajax::Button readButton_;
      ajax::Button writeButton_;
      ajax::InputText inputText_; 
      std::string value_;
      rpct::Rmb* rmb_;


      GOLView(GOLPanel* panel, rpct::Rmb* rmb);

      void onChangeValue(cgicc::Cgicc& cgi, std::ostream& out);
      void onReadGOL(cgicc::Cgicc& cgi, std::ostream& out);
      void onWriteGOL(cgicc::Cgicc& cgi, std::ostream& out);
    };

    friend class GOLView;

    std::vector<GOLView*> gols_;

};
};
#endif
