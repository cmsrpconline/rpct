
#ifndef _rpct_ts_worker_LoggerCellContext_h_
#define _rpct_ts_worker_LoggerCellContext_h_

#include <string>
#include <map>


#include "ts/framework/CellAbstractContext.h"

 

namespace rpcttsworker{

  class LoggerCellContext
      : public virtual tsframework::CellAbstractContext  
    {
    public:
        
      LoggerCellContext(log4cplus::Logger& log , tsframework::CellAbstract* cell);
      ~LoggerCellContext();

      std::string getClassName();
      
      log4cplus::Logger * getAppLogger() {
        return appLogger_;
      }
      log4cplus::SharedAppenderPtr  getlQueue() {
        return lQueue_;
      }
    protected:
        log4cplus::Logger* appLogger_;
        log4cplus::SharedAppenderPtr lQueue_;
    };
} // end rpcttsworker
#endif

