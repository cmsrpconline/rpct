/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Marc Magrans de Abril	                                 *
 *************************************************************************/

#ifndef _rpct_ts_worker_RpctWorkerConfiguration_h_
#define _rpct_ts_worker_RpctWorkerConfiguration_h_

#include "ts/framework/CellOperation.h"
#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellXhannelXdaqSimple.h"

#include "ts/runcontrol/ConfigurationBase.h"
#include <string>

#include "log4cplus/logger.h"

namespace rpcttsworker{

class RpctWorkerConfiguration: public tsruncontrol::ConfigurationBase
{
public:
  RpctWorkerConfiguration(log4cplus::Logger& log, tsframework::CellAbstractContext* context);

protected:
  virtual bool checkColdReset() {
	  return true; //TODO implement
  }
  virtual bool checkConfigure()  {
	  return true; //TODO implement
  }
  virtual bool checkStart() {
	  return true; //TODO implement
  }
  virtual bool checkStop()  {
	  return true; //TODO implement
  }
  virtual bool checkPause() {
	  return true; //TODO implement
  }
  virtual bool checkResume() {
	  return true; //TODO implement
  }

  void execColdReset();
  void execConfigure();
  void execSynchronize_links();
  void execDo_nothing();
  void execStart();
  void execPause();
  void execResume();
  void execStop();

  bool c_true();
  
  virtual void configure() = 0;
  virtual void synchronize_links() = 0; 
  virtual void start() = 0;
  virtual void pause() = 0;
  virtual void resume() = 0;
  virtual void stop() = 0;
  virtual void coldReset() = 0;

};

}
#endif
