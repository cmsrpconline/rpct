#ifndef _rpct_ts_worker_AsyncCommandList_h_
#define _rpct_ts_worker_AsyncCommandList_h_

#include "ts/framework/CellOperation.h"
#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellXhannelXdaqSimple.h"
#include "ts/system/ConfigurationBase.h"
#include "AbstractWorkerConfiguration.h"
#include <string>
#include <vector>
#include "xcept/tools.h"

#include "log4cplus/logger.h"

namespace rpcttsworker {

class AsyncCommandList {
	class Command {
	public:
		std::string commandName;
		std::string commandNameSpace;
		std::string parameterNamespace;
		Command(std::string commandName, std::string commandNameSpace,std::string parameterNamespace) {
			this->commandName = commandName;
			this->commandNameSpace = commandNameSpace;
			this->parameterNamespace = parameterNamespace;
		}
	};
	XdaqManager* xdaqManager;
	CellXhannelXdaqSimple& xhannel;
	std::vector<Command*> commandList;
	std::vector<std::string> results;
	bool completed;
	bool errorOccured;
	int currentCommand;

public:
	AsyncCommandList(XdaqManager* xdaqManager,CellXhannelXdaqSimple& xhannel): xhannel(xhannel) {
		this->xdaqManager = xdaqManager;
		this->completed = 0;
		this->errorOccured = 0;
		this->currentCommand = -1;
	}
	~AsyncCommandList(){
		for(std::vector<Command*>::iterator it = commandList.begin(); it != commandList.end(); it++)
		{
			delete *it;
		}
	}
	bool isCompleted() {
		return completed;
	}
	bool getErrorOccured(){
		return errorOccured;
	}
	void fillResults(std::vector<std::string>* externalResults){
		for(std::vector<std::string>::iterator it = results.begin(); it != results.end(); it++){
			externalResults->push_back(*it);
		}
	}
	void addCommand(std::string commandName,std::string commandNameSpace,std::string parameterNamespace){
		commandList.push_back(new Command(commandName,commandNameSpace,parameterNamespace));
	}
	void doStep(){
		if (currentCommand >= (int)commandList.size()){
			return;
		}
		try{
			if (currentCommand < 0){
				currentCommand = 0;
				startCommand();
				return;
			}

			if (checkCompleted()){
				checkResult();
				currentCommand++;
				startCommand();
				return;
			}
		}catch(xcept::Exception& e){
			this->completed = 1;
			this->errorOccured = 1;
			results.push_back(getDescription()+" [ERROR] : " + xcept::stdformat_exception_history(e));
			throw e;
		}
	}
private:
	std::string getDescription(){
			Command* com = commandList[currentCommand];
			return com->commandName+" "+xdaqManager->getXhannelDescription(xhannel);
		}

		void startCommand(){
			if (currentCommand >= (int)commandList.size()){
				this->completed = 1;
				return;
			}
			Command* com = commandList[currentCommand];
			xdaqManager->doCommand(xhannel,com->commandName+"_start",com->commandNameSpace);
		}

		bool checkCompleted(){
			std::string xdaqCompleted = "false";
			Command* com = commandList[currentCommand];
			xdaqCompleted = xdaqManager->getParameter(xhannel, com->commandName+"_check",
								com->parameterNamespace, "boolean");
			return (xdaqCompleted == "true");
		}

		void checkResult(){
			Command* com = commandList[currentCommand];
			xdaqManager->doCommand(xhannel,com->commandName+"_result",com->commandNameSpace);
			//Everything went OK, we may put info in the results
			results.push_back(getDescription()+" [SUCCESS]");
		}
};
}
#endif
