#ifndef _XDAQHARDWAREACCESS_H_
#define _XDAQHARDWAREACCESS_H_


#include "rpct/devices/System.h"

#include "rpct/xdaqdiagaccess/XdaqDiagAccess.h"
#include "rpct/xdaqutils/XdaqDbServiceClient.h"
#include "rpct/xdaqutils/XdaqIIAccess.h"
#include "rpct/xdaqutils/ConfigurationSetImplXData.h"
#include "rpct/ii/Monitorable.h"
#include "rpct/hardwareTests/TCStatisticDiagManager.h"
#include "rpct/hardwareTests/SCStatisticDiagManager.h"
#include "rpct/hardwareTests/TTUStatisticDiagManager.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/exception/Exception.h"
#include "xdaq/NamespaceURI.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"
#include "xoap/domutils.h"


#include "xdata/Serializable.h"
//#include "xdata/String.h"
#include "xdata/UnsignedLong.h"
#include "xdata/Table.h"

#include "xgi/Utils.h"
#include "xgi/Method.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "toolbox/BSem.h"

namespace rpct {
class MonitorablesWidget;
}
namespace rpcttspanels {
class HistogramsWidget;
}

class XdaqHardwareAccess:  public toolbox::lang::Class {
protected:
	static log4cplus::Logger logger_;


    static log4cplus::Logger summaryLogger_;

    xdaq::Application* app_;
    rpct::MonitorablesWidget* monitorablesWidget_;
    rpcttspanels::HistogramsWidget* histogramsWidget_;

    xdata::Boolean interactive_;
    xdata::Boolean mock_;
    xdata::String host_;
    xdata::Integer port_;
    //xdata::Boolean onConfigResetOnly_;

    xdata::Boolean lastResetCold_;

    rpct::xdaqutils::XdaqIIAccess* xdaqIIAccess_;
    rpct::xdaqutils::XdaqDbServiceClient* xdaqDbServiceClient_;
    rpct::xdaqdiagaccess::XdaqDiagAccess* xdaqDiagAccess_;

    std::string initializationErrorStr_;
    //<<"\n"<<memoryUsageInfo()) removed from the FUNCTION_INFO
#define FUNCTION_INFO(action) \
    do { \
        LOG4CPLUS_INFO(getSummaryLogger(), __FUNCTION__<<" "<<action); \
    } while(0);

#define FUNCTION_ERR(action,msg) \
    do { \
        stringstream act(""); \
        act<<action; \
        LOG4CPLUS_ERROR(getSummaryLogger(), __FUNCTION__<<(act.str().size() ? " ": "")<<action<<": "<<msg); \
    } while(0);

#define FUNCTION_WARN(action,msg) \
    do { \
        stringstream act(""); \
        act<<action; \
        LOG4CPLUS_WARN(getSummaryLogger(), __FUNCTION__<<(act.str().size() ? " ": "")<<action<<": "<<msg); \
    } while(0);

 public:
    static log4cplus::Logger& getLogger() {  return logger_; }
    static log4cplus::Logger& getSummaryLogger() {  return summaryLogger_; }

    static void setupLoggers(std::string namePrefix);

    rpct::System* getSystem();

    virtual void loadLocalConfigKeys();

    virtual std::vector<int> getChipIdsToSetup() = 0;

    virtual rpct::xdaqutils::ConfigurationSetBagPtr getConfigurationSet(std::string confKey, std::vector<int>& chipIds);

    virtual void loadFirmware(bool forceLoad, rpct::ConfigurationSet& confSet) = 0;
    virtual void checkFirmware(rpct::ConfigurationSet& confSet) = 0; //throws exception of firmware is not correct

    virtual void resetHardware(bool forceReset); //resets TTCrxs, QPLLs, GQLs, etc

    virtual void setupHardware(bool forceSetup, rpct::ConfigurationSet& confSet); //applies configuration settings from the DB

    //virtual void configure(std::string globalConfigKey, bool forceLoadFirmware, bool forceReset, bool forceSetup) = 0; //performs full configure: loadFirmware, resetHardware, setupHardware

    virtual void enable();
    virtual void disable();

 protected:

    std::vector<std::string> subsytems_;
    std::map<std::string, std::vector<std::string> > subsystemsLocalConfigKeys_; //all config keys, for drop dawn list in gui

    xdata::UnsignedLong runNumber_;
    rpct::System* system_;
    rpct::xdaqutils::XdaqSoapAccess soapAccess_;

    toolbox::BSem hardwareMutex_;

 public:
    // ONLY for locking
    toolbox::BSem* getHardwareMutex(){
      return &hardwareMutex_;
    }
 protected:

public:
    xdata::UnsignedLong getRunNumber() { return runNumber_; };
    void setRunNumber(xdata::UnsignedLong runNumber) { runNumber_ = runNumber; };


    XdaqHardwareAccess(xdaq::Application* app, 
                       rpct::MonitorablesWidget* monitorablesWigdet,
                       rpcttspanels::HistogramsWidget* histogramsWidget);
    virtual ~XdaqHardwareAccess();

    xoap::MessageReference onGetCratesInfo(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference onGetBoardsInfo(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference onGetDevicesInfo(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference onGetDeviceItemsInfo(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference onReadII(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference onWriteII(xoap::MessageReference msg) throw (xoap::exception::Exception);
    
    xoap::MessageReference onResetRmbs(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference onGetOptLinksStatusInfo(xoap::MessageReference msg) throw (xoap::exception::Exception);


};




#endif
