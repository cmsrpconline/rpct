/** Filip Thyssen */

#ifndef rpct_ts_worker_FebConnectivityTest_h_
#define rpct_ts_worker_FebConnectivityTest_h_

#include <string>

#include <log4cplus/logger.h>
#include "rpct/ts/common/LoggerWidget.h"

#include "rpct/tools/Progress.h"
#include "rpct/ts/common/ProgressBar.h"

#include "rpct/tools/RollSelection.h"
#include "rpct/tools/Time.h"
#include "rpct/tools/RWMutex.h"

#include "rpct/xdaqutils/XdaqDbServiceClient.h"

class sqlite3;
class XdaqLBoxAccess;
namespace rpct {
class LinkSystem;
class FebSystem;
namespace hwd {
class HardwareStorage;
} // namespace hwd
namespace fct {
class FebSystem;
} // namespace fct
} // namespace rpct
namespace toolbox {
namespace task {
class ActionSignature;
class WorkLoop;
} // namespace task
class BSem;
} // namespace toolbox
namespace tsframework {
class CellAbstract;
class CellAbstractContext;
} // namespace tsframework


namespace rpcttsworker {

class FebConnectivityTest
{
protected:
    bool febConfiguration_wl(toolbox::task::WorkLoop *);
    bool connectivityTest_wl(toolbox::task::WorkLoop *);
    bool thresholdScan_wl(toolbox::task::WorkLoop *);

    void febConfiguration();
    void connectivityTest();
    void thresholdScan();
public:
    FebConnectivityTest(tsframework::CellAbstractContext & _context
                        , XdaqLBoxAccess & _xdaqlboxaccess);
    ~FebConnectivityTest();

    log4cplus::Logger & getLogger();
    rpct::LoggerWidget & getLoggerWidget();

    rpct::tools::Progress & getProgress();
    rpct::ProgressBarWidget & getProgressBarWidget();

    XdaqLBoxAccess & getXdaqLBoxAccess();
    toolbox::BSem & getHardwareMutex();
    rpct::FebSystem & getFebSystem();
    rpct::LinkSystem & getLinkSystem();

    rpct::xdaqutils::XdaqDbServiceClient & getDBClient();
    std::string const & getHost() const;
    int getPort() const;

    void prepareFctFebSystem();
    std::string const & getFileName() const;
    rpct::fct::FebSystem & getFctFebSystem();
    rpct::hwd::HardwareStorage & getHardwareStorage();

    rpct::tools::RollSelection const & getSelection() const;
    rpct::tools::Time const & getCountDuration() const;

    int getVTh() const;
    int getVMon() const;

    int getVThMin() const;
    int getVThStepSize() const;
    int getVThNSteps() const;

    int getVThLow() const;
    int getVThHigh() const;

    bool getAutoCorrect() const;
    bool getUsePulser() const;
    bool getIncludeDisabled() const;

    bool getStoreOutput() const;

    void setSelection(rpct::tools::RollSelection const & _selection);
    void setCountDuration(rpct::tools::Time const & _duration);
    void setVTh(int _vth);
    void setVMon(int _vmon);
    void setVThMin(int _vth_min);
    void setVThStepSize(int _vth_step_size);
    void setVThNSteps(int _vth_n_steps);
    void setVThLow(int _vth_low);
    void setVThHigh(int _vth_high);
    void setAutoCorrect(bool _auto_correct);
    void setUsePulser(bool _use_pulser);
    void setIncludeDisabled(bool _include_disabled);
    void setStoreOutput(bool _store_output);

    void runFebConfiguration();
    void runConnectivityTest();
    void runThresholdScan();

    void addMethod(toolbox::task::ActionSignature * , const std::string &) {}

protected:
    rpct::tools::RWMutex mutex_;

    toolbox::task::WorkLoop * run_wl_;
    toolbox::task::ActionSignature * febconfiguration_action_;
    toolbox::task::ActionSignature * connectivitytest_action_;
    toolbox::task::ActionSignature * thresholdscan_action_;

    XdaqLBoxAccess & xdaqlboxaccess_;

    // from xdaq::Application / tsframework::CellAbstract:
    tsframework::CellAbstract & cell_;
    rpct::xdaqutils::XdaqDbServiceClient dbclient_;
    std::string host_;
    int port_;

    rpct::fct::FebSystem * fct_febsystem_;
    std::string cellname_, filename_;
    rpct::hwd::HardwareStorage * hwstorage_;

    rpct::tools::RollSelection selection_;
    rpct::tools::Time count_duration_;
    int vth_, vmon_;
    int vth_min_, vth_step_size_, vth_n_steps_;
    int vth_low_, vth_high_;
    bool auto_correct_;
    bool use_pulser_;
    bool include_disabled_;
    bool store_output_;

    log4cplus::Logger logger_;
    rpct::LoggerWidget logger_widget_;

    rpct::tools::Progress progress_;
    rpct::ProgressBarWidget progress_widget_;
};

} // namespace rpcttsworker

#include "rpct/ts/worker/FebConnectivityTest-inl.h"

#endif // rpct_ts_worker_FebConnectivityTest_h_
