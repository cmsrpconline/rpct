/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Marc Magrans de Abril	                                 *
 *************************************************************************/

#ifndef _rpct_ts_worker_LBWorkerConfiguration_h_
#define _rpct_ts_worker_LBWorkerConfiguration_h_

#include "rpct/ts/worker/RpctWorkerConfiguration.h"
#include "ts/framework/CellOperation.h"
#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellXhannelXdaqSimple.h"

#include "ts/runcontrol/ConfigurationBase.h"
#include <string>

#include "log4cplus/logger.h"

class XdaqLBoxAccess;

namespace rpcttsworker{

class LBCellContext;

class LBWorkerConfiguration: public rpcttsworker::RpctWorkerConfiguration
{
public:
  LBWorkerConfiguration(log4cplus::Logger& log, tsframework::CellAbstractContext* context);
 

protected:
    LBCellContext* context_;

    virtual void resetting();
    void setResult(const std::string& value);
	
    void configure();
    void synchronize_links();
    void start();
    void pause();
    void resume();
    void stop();
    void coldReset();
    
};

}
#endif
