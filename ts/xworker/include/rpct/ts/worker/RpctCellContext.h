
#ifndef _rpct_ts_worker_RpctCellContext_h_
#define _rpct_ts_worker_RpctCellContext_h_

#include <string>
#include <map>
#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellXhannelCell.h"
#include "ts/framework/CellXhannelXdaqSimple.h"
#include "ts/framework/CellXhannel.h"
#include "ts/framework/CellXhannelRequest.h"
#include "rpct/ts/common/LoggerWidget.h"

#include "rpct/ts/worker/LoggerCellContext.h"


namespace rpct{
class MonitorablesWidget;
class LoggerWidget;
}
namespace rpcttspanels {
class HistogramsWidget;
}

namespace rpcttsworker{

  class MonitorSource;
  class RpctCellContext
      // : public virtual tsframework::CellAbstractContext 
      : public rpcttsworker::LoggerCellContext 
    {
    public:
        
      RpctCellContext(log4cplus::Logger& log , tsframework::CellAbstract* cell);
      ~RpctCellContext();

      std::string getClassName();
      
      rpct::MonitorablesWidget* getMonitorablesWidget() {
          return monitorablesWidget_;
      }  
      rpcttspanels::HistogramsWidget* getHistogramsWidget() {
          return histogramsWidget_;
      }

      rpct::LoggerWidget* getLoggerwidget() {
    	  return loggerwidget_;
      }
      // log4cplus::Logger * getAppLogger() {
      //   return appLogger_;
      // }
      // log4cplus::SharedAppenderPtr  getlQueue() {
      //   return lQueue_;
      // }
    protected:
        rpct::MonitorablesWidget* monitorablesWidget_;
        rpcttspanels::HistogramsWidget* histogramsWidget_;
        rpcttsworker::MonitorSource* datasource_;

        rpct::LoggerWidget* loggerwidget_;
        log4cplus::Logger* appLogger_;
        log4cplus::SharedAppenderPtr lQueue_;
    };
}
#endif
