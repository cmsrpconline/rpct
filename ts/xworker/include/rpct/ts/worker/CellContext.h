/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: I. Magrans          				         *
 *************************************************************************/

#ifndef _rpct_ts_worker_CellContext_h_
#define _rpct_ts_worker_CellContext_h_

#include <string>
#include <map>

#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellXhannelCell.h"
#include "ts/framework/CellXhannelXdaqSimple.h"
#include "ts/framework/CellXhannel.h"
#include "ts/framework/CellXhannelRequest.h"

namespace rpcttsworker{

  typedef std::map<std::string,tsframework::CellXhannel*> XhannelsMap;

  class MonitorSource;
  class CellContext: public tsframework::CellAbstractContext  
    {
    public:
      
      CellContext(log4cplus::Logger& log , tsframework::CellAbstract* cell);
      ~CellContext();
      std::map<std::string, tsframework::CellXhannel*> getFilteredXhannelList();

      bool isIgnored(std::string name);
      void setIgnored(std::string name,bool value);

      std::string getClassName();
    private:
      std::map<std::string, bool> xhannelFiltering;
      rpcttsworker::MonitorSource* datasource_;
    };
}
#endif
