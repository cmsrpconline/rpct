#ifndef _XDAQTCRATEACCESS_H_
#define _XDAQTCRATEACCESS_H_


#include "rpct/devices/System.h"
#include "rpct/ts/worker/XdaqHardwareAccess.h"
#include "rpct/xdaqdiagaccess/XdaqDiagAccess.h"
#include "rpct/xdaqutils/XdaqDbServiceClient.h"
#include "rpct/xdaqutils/XdaqIIAccess.h"
#include "rpct/ii/Monitorable.h"
#include "rpct/hardwareTests/TCStatisticDiagManager.h"
#include "rpct/hardwareTests/SCStatisticDiagManager.h"
#include "rpct/hardwareTests/TTUStatisticDiagManager.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/exception/Exception.h"
#include "xdaq/NamespaceURI.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"
#include "xoap/domutils.h"


#include "xdata/Serializable.h"
//#include "xdata/String.h"
#include "xdata/UnsignedLong.h"
#include "xdata/Table.h"

#include "xgi/Utils.h"
#include "xgi/Method.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "toolbox/BSem.h"


class XdaqTCrateAccess:  public XdaqHardwareAccess {
public:
    static const char* APP_NAMESPACE;
    static const char* APP_NAMESPACE_PREFIX;
private:

    static const char* GET_OPTLINKS_STATUS_INFO;

    xdata::String partitionName_;
    std::string partitionNameStr_;

    void initialise();
    void buildSystem();
    void boot(rpct::ICrate& crate, rpct::ConfigurationSet& confSet);
    void checkConfigId(rpct::ICrate& crate, rpct::ConfigurationSet& confSet);

    xoap::MessageReference onResetRmbs(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference onGetOptLinksStatusInfo(xoap::MessageReference msg) throw (xoap::exception::Exception);

public:

    XdaqTCrateAccess(xdaq::Application* app, 
                     rpct::MonitorablesWidget* monitorablesWigdet,
                     rpcttspanels::HistogramsWidget* histogramsWidget);

    virtual std::vector<int> getChipIdsToSetup();

    virtual void loadFirmware(bool forceLoad, rpct::ConfigurationSet& confSet) ;
    virtual void checkFirmware(rpct::ConfigurationSet& confSet); //throws exception of firmware is not correct

    virtual void configure(std::string globalConfigKey, bool forceLoadFirmware, bool forceReset, bool forceSetup);

    void resetRmbs();
};




#endif
