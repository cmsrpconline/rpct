/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: I. Magrans          				         *
 *************************************************************************/

#ifndef _rpct_ts_worker_LBCellContext_h_
#define _rpct_ts_worker_LBCellContext_h_

#include <string>
#include <map>

#include "toolbox/BSem.h"

#include "rpct/ts/worker/FebConfigurationCellContext.h"
#include "rpct/ts/worker/RpctCellContext.h"

class XdaqLBoxAccess;

namespace rpcttsworker{

class FebConnectivityTest;

class LBCellContext
    : public RpctCellContext
    , public rpct::ts::worker::FebConfigurationCellContext
    {
    public:
        LBCellContext(log4cplus::Logger& log , tsframework::CellAbstract* cell);
        ~LBCellContext();

        XdaqLBoxAccess* getLBoxAccess();
        std::string getClassName();

        toolbox::BSem & getHardwareMutex();
        rpct::xdaqutils::xdaqFebSystem & getFebSystem();
        rpcttsworker::FebConnectivityTest & getFebConnectivityTest();

    private:
        XdaqLBoxAccess* lboxAccess_;
        rpcttsworker::FebConnectivityTest * febconnectivitytest_;
	std::string initializationErrorStr_;
    };
}
#endif
