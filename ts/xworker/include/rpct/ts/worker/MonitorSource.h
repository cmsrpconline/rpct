/**************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: Ildefons & Marc Magrans de Abril               		 *
 *************************************************************************/

#ifndef _rpct_ts_worker_MonitorSource_h_
#define _rpct_ts_worker_MonitorSource_h_

#include "ts/framework/DataSource.h"



namespace log4cplus
{
	class Logger;
}

#include "xdata/xdata.h"
#include "xdata/Table.h"

#include <string>

//!Contains the monitorable items for the subsystem cell and the refreshing callbacks
namespace rpcttsworker{
  class CellContext;
  class MonitorSource: public tsframework::DataSource
    {
    public:
      MonitorSource(const std::string& infospaceName, tsframework::CellAbstractContext* context, log4cplus::Logger& logger);
      
    private:
      void refreshItem(const std::string& name, xdata::String& item);
      void refreshData(const std::string& name, xdata::Table& data);

    };
}
#endif
