#ifndef _rpct_ts_worker_version_h_
#define _rpct_ts_worker_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define rpcttsworker_VERSION_MAJOR 1
#define rpcttsworker_VERSION_MINOR 6
#define rpcttsworker_VERSION_PATCH 4
// If any previous versions available E.g. #define XMASSENSOR_PREVIOUS_VERSIONS "3.8.0,3.8.1"
//#define rpcttsworker__PREVIOUS_VERSIONS "0.00"


//
// Template macros
//
#define rpcttsworker_VERSION_CODE PACKAGE_VERSION_CODE(rpcttsworker_VERSION_MAJOR,rpcttsworker_VERSION_MINOR,rpcttsworker_VERSION_PATCH)
#ifndef rpcttsworker_PREVIOUS_VERSIONS
#define rpcttsworker_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(rpcttsworker_VERSION_MAJOR,rpcttsworker_VERSION_MINOR,rpcttsworker_VERSION_PATCH)
#else 
#define rpcttsworker_FULL_VERSION_LIST  rpcttsworker_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(rpcttsworker_VERSION_MAJOR,rpcttsworker_VERSION_MINOR,rpcttsworker_VERSION_PATCH)
#endif 

namespace rpcttsworker
{
	const std::string package  = "rpcttsworker";
	const std::string versions = rpcttsworker_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "";
	const std::string summary = "";
	const std::string link = "";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif


