#ifndef _AUTOUPDATEBOOLEAN_H_
#define _AUTOUPDATEBOOLEAN_H_

#include "xdata/Serializable.h"
#include "xdata/String.h"
#include "xdata/Boolean.h"
#include "xdata/UnsignedLong.h"
#include "xdata/Table.h"
#include <string>

#include "xcept/tools.h"

#include "log4cplus/logger.h"

using namespace std;

/*
 * it is probably possible to write more generic template which
 * could be used with other serializable types, still function type
 * would have to be improved(with another parameter)
 */
template <typename LISTENER>
class AutoUpdateBoolean  : public xdata::Boolean{

	LISTENER *obj_;

	bool (LISTENER::*call_)();

public:

	AutoUpdateBoolean(
			LISTENER * obj,
			bool (LISTENER::*func)())
	  {
		this->obj_ = obj;
		this->call_ = func;
	  }

	void updateValue(){
		//this->setValue(  );
		xdata::Boolean::operator=((obj_->*call_)());
	}

	// Comparison operators
	virtual int equals(const xdata::Serializable & s){
		updateValue();
		return xdata::Boolean::equals(s);
	}

	//! Print value as a string
	virtual std::string toString() throw (xdata::exception::Exception){
		updateValue();
		return xdata::Boolean::toString();
	}
};



#endif /*_AUTOUPDATEBOOLEAN_H_*/
