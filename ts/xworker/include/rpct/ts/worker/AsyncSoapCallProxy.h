#ifndef _ASYNCSOAPCALLPROXY_H_
#define _ASYNCSOAPCALLPROXY_H_

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/exception/Exception.h"
#include "xdaq/NamespaceURI.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"
#include "xoap/domutils.h"


#include "xdata/Serializable.h"
#include "xdata/String.h"
#include "xdata/Boolean.h"
#include "xdata/UnsignedLong.h"
#include "xdata/Table.h"
#include <string>

#include "xcept/tools.h"

#include "log4cplus/logger.h"
#include "rpct/xdaqutils/XdaqCommonUtils.h"

#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/WorkLoopFactory.h"


using namespace std;

template <class LISTENER>
		toolbox::task::Action<LISTENER> * bindTask(xdaq::Application* xapp,LISTENER * obj, bool (LISTENER::*func)(toolbox::task::WorkLoop *), const std::string & name)
		{
			toolbox::task::Action<LISTENER> * f = new toolbox::task::Action<LISTENER>;
			f->obj_ = obj;
			f->func_ = func;
			f->name_ = name;
			xapp->addMethod(f, name);
			return f;
		}


template <typename LISTENER>
class AsyncSoapCallProxy{

	LISTENER *obj_;

	xoap::MessageReference (LISTENER::*call_)(xoap::MessageReference) throw (xoap::exception::Exception);

	xoap::MessageReference recentResult;

	// Probably it is necessary to use setValue on this object instead of
	// simple assign to a field
	xdata::Boolean* call_completed;

	string simpleName;

	xdaq::Application* xapp;

	toolbox::task::WorkLoop * workLoop;

	xoap::MessageReference parameter_msg;

	int caught_exc;

	log4cplus::Logger logger;
public:

	AsyncSoapCallProxy(
			xdaq::Application* xapp,
			LISTENER * obj,
			xoap::MessageReference (LISTENER::*func)(xoap::MessageReference) throw (xoap::exception::Exception))
	: logger(log4cplus::Logger::getInstance("AsyncSoapCallProxy"))
	  {
		this->call_completed = new xdata::Boolean(true);
		this->obj_ = obj;
		this->call_ = func;
		this->xapp = xapp;
		this->caught_exc   = 0;
	  }

	xoap::MessageReference doSimpleCall (xoap::MessageReference msg)  throw (xoap::exception::Exception){

	  return (obj_->*call_)(msg);
	}

	void replaceNames(string& str, string from,string toStr){
		size_t result = 0;
		do{
			result = str.find(from,result);
			if (result == string::npos) return;
			str.replace(result,from.size(),toStr);
			result++;
		}while (true);
	}

	void doTest() throw (xoap::exception::Exception){
		string testStr = "<ala_>ma<kota/></ala_>";
		replaceNames(testStr,"ala_","ala");
		if (testStr != "<ala>ma<kota/></ala>"){
			XCEPT_RAISE(xoap::exception::Exception, "Test failure, result = "+testStr);
		}
	}

	void setCallCompleted(bool value){
		*(this->call_completed) = value;
	}

	bool doCallJob(toolbox::task::WorkLoop* wl){
		LOG4CPLUS_INFO(this->logger, "doCallJob begin");
		this->caught_exc = 0;
		try{
			recentResult = (obj_->*call_)(parameter_msg);
			LOG4CPLUS_INFO(this->logger, "doCallJob operation completed OK");
		}catch(xcept::Exception& e){
			LOG4CPLUS_INFO(this->logger, "doCallJob exception found");
			LOG4CPLUS_ERROR (this->logger, xcept::stdformat_exception_history(e));
			this->caught_exc = 1;
		}

		this->setCallCompleted(true);
		LOG4CPLUS_INFO(this->logger, "doCallJob end");
		return false;
	}


	void internalDoStartCall(xoap::MessageReference msg){
		LOG4CPLUS_INFO(this->logger, "internalDoStartCall begin");
		parameter_msg = msg;

		workLoop->submit(bindTask(xapp,this, &AsyncSoapCallProxy::doCallJob, simpleName + "_call_job"));

		LOG4CPLUS_INFO(this->logger, "internalDoStartCall end");
	}

	xoap::MessageReference doStartCall (xoap::MessageReference msg)  throw (xoap::exception::Exception){
	  LOG4CPLUS_INFO(this->logger, "doStartCall begin");
	  string msg_data;
	  this->setCallCompleted(false);
	  msg->writeTo(msg_data);
	  this->replaceNames(msg_data,this->simpleName+"_start",this->simpleName);
	  msg->readFrom(msg_data.c_str(),msg_data.size()+1);
	  this->internalDoStartCall(msg);
	  xoap::MessageReference reply = xoap::createMessage();
	  LOG4CPLUS_INFO(this->logger, "doStartCall end");
	  return reply;
	}

	xoap::MessageReference doStopCall (xoap::MessageReference msg)  throw (xoap::exception::Exception){

	  //return (obj_->*call_)(msg);
	  XCEPT_RAISE(xoap::exception::Exception, "Not implemented");
	}


	xoap::MessageReference doResultCall (xoap::MessageReference msg)  throw (xoap::exception::Exception){
	  if (this->caught_exc != 0){
		  LOG4CPLUS_INFO(this->logger, "do Result Call - Throwing an exception");
		  XCEPT_RAISE(xoap::exception::Exception, "Exception caught during async call");
	  }
	  LOG4CPLUS_INFO(this->logger, "Returning valid result");
	  return recentResult;
	}

	
	void bindMe(
		    const string & simpleMessageName,
		    const string & namespaceURI){

	  this->workLoop = toolbox::task::getWorkLoopFactory()->getWorkLoop("AsyncSoap_"+simpleMessageName+"_WorkLoop", "waiting");
	  workLoop->activate();
	  this->simpleName = simpleMessageName;

	  deferredbind(xapp, this, &AsyncSoapCallProxy<LISTENER>::doSimpleCall,
	       simpleMessageName, namespaceURI);

	  deferredbind(xapp, this, &AsyncSoapCallProxy<LISTENER>::doStartCall,
	       simpleMessageName+"_start", namespaceURI);
	   
	  deferredbind(xapp, this, &AsyncSoapCallProxy<LISTENER>::doStopCall,
	       simpleMessageName+"_stop", namespaceURI);

	  xapp->getApplicationInfoSpace()->fireItemAvailable(simpleMessageName+"_check", call_completed);

	  deferredbind(xapp, this, &AsyncSoapCallProxy<LISTENER>::doResultCall,
	       simpleMessageName+"_result", namespaceURI);
	  doTest();
	};

};



#endif /*_XDAQLBOXACCESS_H_*/
