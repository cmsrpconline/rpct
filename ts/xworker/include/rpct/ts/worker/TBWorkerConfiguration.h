/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Marc Magrans de Abril	                                 *
 *************************************************************************/

#ifndef _rpct_ts_worker_TBWorkerConfiguration_h_
#define _rpct_ts_worker_TBWorkerConfiguration_h_

#include "rpct/ts/worker/RpctWorkerConfiguration.h"
#include "ts/framework/CellOperation.h"
#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellXhannelXdaqSimple.h"

#include "rpct/ts/worker/TBCellContext.h"
#include "ts/runcontrol/ConfigurationBase.h"
#include <string>

#include "log4cplus/logger.h"

namespace rpcttsworker{
class TBWorkerConfiguration: public rpcttsworker::RpctWorkerConfiguration
{
public:
    TBWorkerConfiguration(log4cplus::Logger& log, tsframework::CellAbstractContext* context);

protected:
    TBCellContext* context_;
    void configure();
    void synchronize_links();
    void start();
    void pause();
    void resume();
    void stop();
    void coldReset();

    virtual void resetting();
    void setResult(const std::string& result);
};

}
#endif
