#ifndef _subsystem_ts_cell_GOLRead_h_
#define _subsystem_ts_cell_GOLRead_h_

#include "ts/framework/CellCommand.h"

namespace rpcttsworker{
  class GOLRead: public tsframework::CellCommand {
  public:
    
    GOLRead(log4cplus::Logger& log, tsframework::CellAbstractContext* context);
    virtual void code();

  };
}
#endif
