#ifndef _rpct_ts_worker_commands_ResetAllNotReadyCBTTCrxs_h_
#define _rpct_ts_worker_commands_ResetAllNotReadyCBTTCrxs_h_

#include "ts/framework/CellCommand.h"

namespace rpcttsworker {

class LBCellContext;

class ResetAllNotReadyCBTTCrxs : public tsframework::CellCommand
{
public:
	ResetAllNotReadyCBTTCrxs(log4cplus::Logger & log, tsframework::CellAbstractContext * context);
    void code();

protected:
    LBCellContext & cellcontext_;
};

} // namespace rpcttsworker

#endif // _rpct_ts_worker_commands_ResetAllNotReadyCBTTCrxs_h_
