#ifndef _rpct_ts_worker_commands_ResetAllCBTTCrxs_h_
#define _rpct_ts_worker_commands_ResetAllCBTTCrxs_h_

#include "ts/framework/CellCommand.h"

namespace rpcttsworker {

class LBCellContext;

class ResetAllCBTTCrxs : public tsframework::CellCommand
{
public:
	ResetAllCBTTCrxs(log4cplus::Logger & log, tsframework::CellAbstractContext * context);
    void code();

protected:
    LBCellContext & cellcontext_;
};

} // namespace rpcttsworker

#endif // _rpct_ts_worker_commands_ResetAllCBTTCrxs_h_
