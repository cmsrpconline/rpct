#ifndef rpct_ts_xworker_command_SynchronizeLinksCommand_h
#define rpct_ts_xworker_command_SynchronizeLinksCommand_h

#include "ts/framework/CellCommand.h"

#include "rpct/tools/Mutex.h"

namespace rpcttsworker {

class LBCellContext;

class SynchronizeLinksCommand
    : public tsframework::CellCommand
{
public:
    SynchronizeLinksCommand(log4cplus::Logger & _logger, tsframework::CellAbstractContext * _context);
    bool preCondition();
    void code();

protected:
    rpct::tools::Mutex mutex_;
    LBCellContext & context_;
};

} // namespace rpcttsworker

#endif // rpct_ts_xworker_command_SynchronizeLinksCommand_h
