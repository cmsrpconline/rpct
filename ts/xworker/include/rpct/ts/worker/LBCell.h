/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: I. Magrans          				         *
 *************************************************************************/

#ifndef _rpct_ts_worker_LBCell_h_
#define _rpct_ts_worker_LBCell_h_

#include "ts/framework/CellAbstract.h"

#include "rpct/ts/worker/LBCellContext.h"
#include <string>
namespace rpcttsworker{
  class LBCell: public tsframework::CellAbstract
    {
    public:
      
      XDAQ_INSTANTIATOR();
      
      LBCell(xdaq::ApplicationStub * s);
      
      ~LBCell();

      //!This method should be filled with addCommand and addOperation that corresponfs to that Cell
      void init();
      
      std::string getClassName()
	{
	  return "Cell";
	};
      
      //!Returns the CellContext pointer. CellContext contains the shared objects of the Cell 
      LBCellContext* getContext()
	{
	  LBCellContext* c(dynamic_cast<LBCellContext*>(cellContext_));
	  
	  if (!c)
	    XCEPT_RAISE(tsexception::CellException,"The Cell Context is not of class LBCellContext");
	  
	  return c;
	};

    };
}
#endif
