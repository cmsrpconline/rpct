#ifndef _subsystem_ts_cell_ResetRMB_h_
#define _subsystem_ts_cell_ResetRMB_h_

#include "ts/framework/CellCommand.h"

class XdaqTCrateAccess;

namespace rpcttsworker{
  class ResetRMB: public tsframework::CellCommand {
  public:
    XdaqTCrateAccess* tcCrateAccess_; 
    ResetRMB(log4cplus::Logger& log, tsframework::CellAbstractContext* context);
    virtual void code();

  };
}
#endif
