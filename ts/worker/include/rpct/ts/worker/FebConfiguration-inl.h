#ifndef rpct_ts_worker_FebConfiguration_inl_h
#define rpct_ts_worker_FebConfiguration_inl_h

#include "rpct/ts/worker/FebConfiguration.h"

namespace rpct {
namespace ts {
namespace worker {

inline log4cplus::Logger & FebConfiguration::getLogger()
{
    return logger_;
}

inline rpct::LoggerWidget & FebConfiguration::getLoggerWidget()
{
    return logger_widget_;
}

inline rpct::tools::Progress & FebConfiguration::getProgress()
{
    return progress_;
}

inline rpct::ProgressBarWidget & FebConfiguration::getProgressBarWidget()
{
    return progress_widget_;
}

inline rpct::xdaqutils::XdaqDbServiceClient & FebConfiguration::getDBClient()
{
    return dbclient_;
}

} // namespace worker
} // namespace ts
} // namespace rpct

#endif // rpct_ts_worker_FebConfiguration_h
