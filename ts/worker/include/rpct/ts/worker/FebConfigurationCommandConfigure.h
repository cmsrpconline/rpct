#ifndef rpct_ts_worker_FebConfigurationCommandConfigure_h
#define rpct_ts_worker_FebConfigurationCommandConfigure_h

#include "ts/framework/CellCommand.h"

namespace rpct {
namespace ts {
namespace worker {

class FebConfigurationCellContext;

class FebConfigurationCommandConfigure
    : public tsframework::CellCommand
{
public:
    FebConfigurationCommandConfigure(log4cplus::Logger & _logger, tsframework::CellAbstractContext * _context);
    void code();

protected:
    FebConfigurationCellContext & context_;
};

} // namespace worker
} // namespace ts
} // namespace rpct

#endif // rpct_ts_worker_FebConfigurationCommandConfigure_h
