#ifndef rpct_ts_worker_FebConfigurationPanel_h
#define rpct_ts_worker_FebConfigurationPanel_h

#include <iosfwd>
#include <string>
#include <vector>
#include <utility>

#include "ts/framework/CellPanel.h"

#include "log4cplus/logger.h"

namespace ajax {
class ResultBox;
class Div;
class Editable;
} // namespace ajax

namespace rpct {
namespace ts {
namespace worker {

class FebConfigurationCellContext;

class FebConfigurationPanel
    : public tsframework::CellPanel
{
private:
    void layout(cgicc::Cgicc & _cgi);

protected:
    void initLayout(std::string const & _title);
    void loadFebConfigKeys();

    void onLockClick(cgicc::Cgicc & _cgi, std::ostream & _out);

    void onConfigureFEBsClick(cgicc::Cgicc & _cgi, std::ostream & _out);
    void onLoadConfigurationFEBsClick(cgicc::Cgicc & _cgi, std::ostream & _out);
    void onMonitorFEBsClick(cgicc::Cgicc & _cgi, std::ostream & _out);
    void onMonitorPauseFEBsClick(cgicc::Cgicc & _cgi, std::ostream & _out);
    void onMonitorResumeFEBsClick(cgicc::Cgicc & _cgi, std::ostream & _out);
    void onMonitorTerminateFEBsClick(cgicc::Cgicc & _cgi, std::ostream & _out);
    void onMonitorRefreshFEBsClick(cgicc::Cgicc & _cgi, std::ostream & _out);

    void febGetTitle(cgicc::Cgicc & _cgi, std::ostream & _out);
    void febGetFebSystemURL(cgicc::Cgicc & _cgi, std::ostream & _out);

    void getFebConfigKeys(cgicc::Cgicc& cgi,std::ostream& out);

public:
    FebConfigurationPanel(tsframework::CellAbstractContext * _context, log4cplus::Logger & _logger);
    ~FebConfigurationPanel();

protected:
    FebConfigurationCellContext & context_;
    log4cplus::Logger logger_;

    std::vector<std::pair<std::string, std::string> > feb_config_keys_;

    bool locked_;

};

} // namespace worker
} // namespace ts
} // namespace rpct

#endif // rpct_ts_worker_FebConfigurationPanel_h
