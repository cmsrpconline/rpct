#ifndef rpct_ts_worker_FebConfigurationCellContext_h
#define rpct_ts_worker_FebConfigurationCellContext_h

#include "ts/framework/CellAbstractContext.h"

#include <string>

#include "rpct/tools/Mutex.h"

namespace toolbox {
class BSem;
} // namespace toolbox

namespace rpct {
namespace xdaqutils {
class xdaqFebSystem;
} // namespace xdaqutils
} // namespace rpct

namespace rpcttsworker {
class FebConnectivityTest;
} // namespace rpcttsworker

namespace rpct {
namespace ts {
namespace worker {

class FebConfiguration;

class FebConfigurationCellContext
    : public virtual tsframework::CellAbstractContext
{
public:
    FebConfigurationCellContext(log4cplus::Logger & _log, tsframework::CellAbstract * _cell
                                , std::string const & _supervisor_name = "rpcttscell::Cell"
                                , size_t _supervisor_instance = 2);
    ~FebConfigurationCellContext();

    rpct::tools::Mutex & getSoftwareMutex();
    FebConfiguration & getFebConfiguration();

    virtual toolbox::BSem & getHardwareMutex() = 0;
    virtual rpct::xdaqutils::xdaqFebSystem & getFebSystem() = 0;
    virtual rpcttsworker::FebConnectivityTest & getFebConnectivityTest() = 0;

protected:
    rpct::tools::Mutex swmutex_;
    FebConfiguration * feb_configuration_;
};

inline rpct::tools::Mutex & FebConfigurationCellContext::getSoftwareMutex()
{
    return swmutex_;
}

inline FebConfiguration & FebConfigurationCellContext::getFebConfiguration()
{
    return *feb_configuration_;
}

} // namespace worker
} // namespace ts
} // namespace rpct

#endif // rpct_ts_worker_FebConfigurationCellContext_h
