#ifndef rpct_ts_worker_FebConfiguration_h
#define rpct_ts_worker_FebConfiguration_h

#include <string>
#include <vector>

#include "rpct/tools/Mutex.h"

#include "log4cplus/logger.h"
#include "rpct/xdaqtools/remotelogger/Client.h"
#include "rpct/ts/common/LoggerWidget.h"

#include "rpct/tools/Progress.h"
#include "rpct/ts/common/ProgressBar.h"
#include "rpct/xdaqutils/XdaqDbServiceClient.h"

namespace toolbox {
namespace task {
class ActionSignature;
class WorkLoop;
} // namespace task
} // namespace toolbox

namespace tsframework {
class CellXhannelCell;
class CellXhannelRequestCell;
} // namespace tsframework

namespace rpct {
namespace ts {
namespace worker {

class FebConfigurationCellContext;

class FebConfiguration
{
protected:
    bool configure_wl(toolbox::task::WorkLoop *);
    bool load_configuration_wl(toolbox::task::WorkLoop *);
    bool monitor_wl(toolbox::task::WorkLoop *);

    void iconfigure(std::string const & _key);
    void iload_configuration(std::string const & _key);
    void imonitor();

public:
    FebConfiguration(FebConfigurationCellContext & _context
                     , std::string const & _supervisor_name = "rpcttscell::Cell"
                     , size_t _supervisor_instance = 2);
    ~FebConfiguration();

    log4cplus::Logger & getLogger();
    rpct::LoggerWidget & getLoggerWidget();

    rpct::tools::Progress & getProgress();
    rpct::ProgressBarWidget & getProgressBarWidget();

    rpct::xdaqutils::XdaqDbServiceClient & getDBClient();
    std::vector<std::string> getFebConfigKeys() const;

    void configure(std::string const & _key, bool _attach = true, bool _nothrow = false);
    void loadConfiguration(std::string const & _key, bool _attach = true, bool _nothrow = false);
    void monitor(bool _attach = true, bool _nothrow = false);

    void addMethod(toolbox::task::ActionSignature *, std::string const &) {}

protected:
    FebConfigurationCellContext & context_;

    mutable rpct::tools::Mutex mutex_;

    log4cplus::Logger logger_, remote_logger_;
    rpct::xdaqtools::remotelogger::Client remote_logger_client_;
    SharedAppenderPtr remote_logger_client_ptr_;
    rpct::LoggerWidget logger_widget_;

    rpct::tools::Progress progress_;
    rpct::ProgressBarWidget progress_widget_;

    mutable rpct::xdaqutils::XdaqDbServiceClient dbclient_;

    std::string configuration_key_;
    rpct::tools::Time configuration_time_, monitor_time_;

    toolbox::task::WorkLoop * action_wl_;
    toolbox::task::ActionSignature * configure_action_, * load_configuration_action_, * monitor_action_;
};

} // namespace worker
} // namespace ts
} // namespace rpct

#include "rpct/ts/worker/FebConfiguration-inl.h"

#endif // rpct_ts_worker_FebConfiguration_h
