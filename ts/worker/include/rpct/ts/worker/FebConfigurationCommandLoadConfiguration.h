#ifndef rpct_ts_worker_FebConfigurationCommandLoadConfiguration_h
#define rpct_ts_worker_FebConfigurationCommandLoadConfiguration_h

#include "ts/framework/CellCommand.h"

namespace rpct {
namespace ts {
namespace worker {

class FebConfigurationCellContext;

class FebConfigurationCommandLoadConfiguration
    : public tsframework::CellCommand
{
public:
    FebConfigurationCommandLoadConfiguration(log4cplus::Logger & _logger, tsframework::CellAbstractContext * _context);
    void code();

protected:
    FebConfigurationCellContext & context_;
};

} // namespace worker
} // namespace ts
} // namespace rpct

#endif // rpct_ts_worker_FebConfigurationCommandLoadConfiguration_h
