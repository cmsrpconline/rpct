#include "rpct/ts/worker/FebConfigurationCellContext.h"

#include "rpct/ts/worker/FebConfiguration.h"

namespace rpct {
namespace ts {
namespace worker {

FebConfigurationCellContext::FebConfigurationCellContext(log4cplus::Logger & _log, tsframework::CellAbstract * _cell
                                                         , std::string const & _supervisor_name
                                                         , size_t _supervisor_instance)
    : tsframework::CellAbstractContext(_log, _cell)
    , feb_configuration_(new FebConfiguration(*this, _supervisor_name, _supervisor_instance))
{}

FebConfigurationCellContext::~FebConfigurationCellContext()
{
    delete feb_configuration_;
}

} // namespace worker
} // namespace ts
} // namespace rpct
