#include "rpct/ts/worker/FebConfiguration.h"

#include <stdexcept>
#include <sstream>

#include "log4cplus/loggingmacros.h"

#include "toolbox/task/WaitingWorkLoop.h"
#include "xcept/tools.h"

#include "rpct/ts/worker/FebConfigurationCellContext.h"
#include "rpct/ii/feb_const.h"

#include "ts/framework/CellAbstract.h"

#include "rpct/xdaqutils/xdaqFebSystem.h"

#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/sync/named_mutex.hpp>

namespace rpct {
namespace ts {
namespace worker {

bool FebConfiguration::configure_wl(toolbox::task::WorkLoop *)
{
    LOG4CPLUS_TRACE(logger_, "FebConfiguration::configure_wl");

    try {
        std::string _key;
        {
            rpct::tools::Lock _lock(mutex_);
            _key = configuration_key_;
        }
        iconfigure(_key);

        std::ostringstream _message;
        _message << "Configuration ready for "
                 << context_.getCell()->getName()
                 << ": " << context_.getFebSystem().rpct::FebSystem::getStatusSummary();
        remote_logger_.log(rpct::tools::State::getStateLogLevel(context_.getFebSystem().getPState())
                           , _message.str());
    } catch (std::runtime_error const & _e) {
        LOG4CPLUS_ERROR(remote_logger_, "Configuration failed for "
                        << context_.getCell()->getName()
                        << ": " << _e.what());
    }

    return false;
}

bool FebConfiguration::load_configuration_wl(toolbox::task::WorkLoop *)
{
    LOG4CPLUS_TRACE(logger_, "FebConfiguration::load_configuration_wl");

    try {
        std::string _key;
        {
            rpct::tools::Lock _lock(mutex_);
            _key = configuration_key_;
        }
        iload_configuration(_key);

        std::ostringstream _message;
        _message << "Load Configuration ready for "
                 << context_.getCell()->getName()
                 << ": " << context_.getFebSystem().rpct::FebSystem::getStatusSummary();
        remote_logger_.log(rpct::tools::State::getStateLogLevel(context_.getFebSystem().getPState())
                           , _message.str());
    } catch (std::runtime_error const & _e) {
        LOG4CPLUS_ERROR(remote_logger_, "Load Configuration failed for "
                        << context_.getCell()->getName()
                        << ": " << _e.what());
    }

    return false;
}

bool FebConfiguration::monitor_wl(toolbox::task::WorkLoop *)
{
    LOG4CPLUS_TRACE(logger_, "FebConfiguration::monitor_wl");

    try {
        imonitor();
        std::ostringstream _message;
        _message << "Monitoring ready for "
                 << context_.getCell()->getName()
                 << ": " << context_.getFebSystem().rpct::FebSystem::getStatusSummary();
        remote_logger_.log(rpct::tools::State::getStateLogLevel(context_.getFebSystem().getPState())
                           , _message.str());
    } catch (std::runtime_error const & _e) {
        LOG4CPLUS_ERROR(remote_logger_, "Monitoring failed for "
                        << context_.getCell()->getName()
                        << ": " << _e.what());
    }

    return false;
}

void FebConfiguration::iconfigure(std::string const & _key)
{
    //boost::interprocess::named_mutex feb_mutex(boost::interprocess::open_or_create, "feb_mutex");
    //boost::interprocess::scoped_lock<boost::interprocess::named_mutex> lock(feb_mutex);

    LOG4CPLUS_INFO(remote_logger_, "FebConfiguration::iconfigure for " << context_.getCell()->getName() << " with key " << _key);

    getProgress().setTitle("Feb Configuration");
    getProgress().init();

    // Software Lock
    getProgress().setDescription("Taking Software Mutex");
    rpct::tools::Lock _swlock(context_.getSoftwareMutex());

    // get ConfigurationSet
    rpct::ConfigurationSet * _configuration_set(0);
    rpct::xdaqutils::ConfigurationSetBagPtr _configuration_set_bag;
    if (!(_key.empty()))
        {
            try {
                std::vector<int> _chip_ids = context_.getFebSystem().getChipIds();
                if (_chip_ids.empty()) {
                    getProgress().stop();
                    getProgress().setDescription("No Front-End Chips to handle");
                    return;
                }
                _configuration_set_bag = dbclient_.getConfigurationSetForLocalConfigKey(_chip_ids, _key);
                _configuration_set = &(_configuration_set_bag->bag);
            } catch (xcept::Exception & _e) {
                LOG4CPLUS_ERROR(logger_, "Caught exception in FebConfiguration::iconfigure while getting Configurations from DB: " << _e.what());
                getProgress().fail();
                getProgress().setDescription("Error in Front-End Board Configuration: problem getting Configurations from DB");
                throw std::runtime_error("Front-End Board Configuration incomplete: problem getting Configurations from DB");
            } catch (std::exception & _e) {
                LOG4CPLUS_ERROR(logger_, "Caught exception in FebConfiguration::iconfigure while getting Configurations from DB: " << _e.what());
                getProgress().fail();
                getProgress().setDescription("Error in Front-End Board Configuration: problem getting Configurations from DB");
                throw std::runtime_error("Front-End Board Configuration incomplete: problem getting Configurations from DB");
            } catch (...) {
                LOG4CPLUS_ERROR(logger_, "Caught exception in FebConfiguration::iconfigure while getting Configurations from DB");
                getProgress().fail();
                getProgress().setDescription("Error in Front-End Board Configuration: problem getting Configurations from DB");
                throw std::runtime_error("Front-End Board Configuration incomplete: problem getting Configurations from DB");
            }
        }

    // get FebDistributionBoards
    std::map<int, FebSystemItem *> const & _fdbs(context_.getFebSystem().getFebSystemItems(rpct::type::feb::febdistributionboard_));

    // Hardware Lock
    getProgress().setDescription("Taking Hardware Mutex");
    rpct::tools::TLock<toolbox::BSem> _hwlock(context_.getHardwareMutex());

    getProgress().setDescription("Running Front-End Board Configuration");
    getProgress().setMaximum(_fdbs.size());
    getProgress().start();
    bool _is_terminated(false);

    // paused?
    if (getProgress().isControlPaused() && getProgress().isControlActive()) {
        _hwlock.give();
        getProgress().checkPaused();
        _hwlock.take();
    }
    // terminated?
    _is_terminated = getProgress().checkTerminated();

    for (std::map<int, FebSystemItem *>::const_iterator _fdb = _fdbs.begin()
             ; _fdb != _fdbs.end() && !_is_terminated ; ++_fdb)
        {
            getProgress().setDescription(std::string("Processing ") + _fdb->second->getDescription());
            try {
                rpct::Configurable * _configurable = dynamic_cast<rpct::Configurable *>(_fdb->second);
                _configurable->configure(_configuration_set, 0);
                getProgress().addValue();
            } catch (std::exception & _e) {
                LOG4CPLUS_ERROR(logger_, "Caught exception in FebConfiguration::iconfigure: " << _e.what());
                getProgress().fail();
                getProgress().setDescription("Error in Front-End Board Configuration");
                throw std::runtime_error(std::string("Front-End Board Configuration incomplete: ") + _e.what());
            } catch (...) {
                LOG4CPLUS_ERROR(logger_, "Caught unknown exception in FebConfiguration::iconfigure");
                getProgress().fail();
                getProgress().setDescription("Error in Front-End Board Configuration");
                throw std::runtime_error("Front-End Board Configuration incomplete: unknown error");
            }

            // paused?
            if (getProgress().isControlPaused() && getProgress().isControlActive()) {
                _hwlock.give();
                getProgress().checkPaused();
                _hwlock.take();
            }
            // terminated?
            _is_terminated = getProgress().checkTerminated();
        }

    if (_is_terminated)
        {
            LOG4CPLUS_ERROR(logger_, "FebConfiguration::iconfigure was terminated.");
            getProgress().fail();
            getProgress().setDescription("Front-End Board Configuration was terminated.");
        }
    else
        {
            getProgress().stop();
            getProgress().setDescription("Finished Front-End Board Configuration");
            context_.getFebSystem().getConfigurationTime() = getProgress().getDuration();
        }
}

void FebConfiguration::iload_configuration(std::string const & _key)
{
    LOG4CPLUS_INFO(remote_logger_, "FebConfiguration::iload_configuration for " << context_.getCell()->getName() << " with key " << _key);

    getProgress().setTitle("Feb Load Configuration");
    getProgress().init();

    // Software Lock
    getProgress().setDescription("Taking Software Mutex");
    rpct::tools::Lock _swlock(context_.getSoftwareMutex());

    // get ConfigurationSet
    rpct::ConfigurationSet * _configuration_set(0);
    rpct::xdaqutils::ConfigurationSetBagPtr _configuration_set_bag;
    if (!(_key.empty()))
        {
            try {
                std::vector<int> _chip_ids = context_.getFebSystem().getChipIds();
                if (_chip_ids.empty()) {
                    getProgress().stop();
                    getProgress().setDescription("No Front-End Chips to handle");
                    return;
                }
                _configuration_set_bag = dbclient_.getConfigurationSetForLocalConfigKey(_chip_ids, _key);
                _configuration_set = &(_configuration_set_bag->bag);
            } catch (xcept::Exception & _e) {
                LOG4CPLUS_ERROR(logger_, "Caught exception in FebConfiguration::iload_configuration while getting Configurations from DB: " << _e.what());
                getProgress().fail();
                getProgress().setDescription("Error in Front-End Board Load Configuration: problem getting Configurations from DB");
                throw std::runtime_error("Front-End Board Load Configuration incomplete: problem getting Configurations from DB");
            } catch (std::exception & _e) {
                LOG4CPLUS_ERROR(logger_, "Caught exception in FebConfiguration::iload_configuration while getting Configurations from DB: " << _e.what());
                getProgress().fail();
                getProgress().setDescription("Error in Front-End Board Load Configuration: problem getting Configurations from DB");
                throw std::runtime_error("Front-End Board Load Configuration incomplete: problem getting Configurations from DB");
            } catch (...) {
                LOG4CPLUS_ERROR(logger_, "Caught exception in FebConfiguration::iload_configuration while getting Configurations from DB");
                getProgress().fail();
                getProgress().setDescription("Error in Front-End Board Load Configuration: problem getting Configurations from DB");
                throw std::runtime_error("Front-End Board Load Configuration incomplete: problem getting Configurations from DB");
            }
        }

    // load Configuration
    getProgress().setDescription("Loading Front-End Board Configuration");
    getProgress().setMaximum(1);
    getProgress().start();
    context_.getFebSystem().loadConfiguration(_configuration_set, 0);
    getProgress().addValue();
    getProgress().stop();
    getProgress().setDescription("Finished Front-End Board Load Configuration");
}

void FebConfiguration::imonitor()
{
    LOG4CPLUS_INFO(remote_logger_, "FebConfiguration::imonitor for " << context_.getCell()->getName());

    getProgress().setTitle("Feb Monitoring");
    getProgress().init();

    // Software Lock
    getProgress().setDescription("Taking Software Mutex");
    rpct::tools::Lock _swlock(context_.getSoftwareMutex());

    // get FebDistributionBoards
    std::map<int, FebSystemItem *> const & _fdbs(context_.getFebSystem().getFebSystemItems(rpct::type::feb::febdistributionboard_));

    // Hardware Lock
    getProgress().setDescription("Taking Hardware Mutex");
    rpct::tools::TLock<toolbox::BSem> _hwlock(context_.getHardwareMutex());

    getProgress().setDescription("Running Front-End Board Monitoring");
    getProgress().setMaximum(_fdbs.size());
    getProgress().start();
    bool _is_terminated(false);

    // paused?
    if (getProgress().isControlPaused() && getProgress().isControlActive()) {
        _hwlock.give();
        getProgress().checkPaused();
        _hwlock.take();
    }
    // terminated?
    _is_terminated = getProgress().checkTerminated();

    for (std::map<int, FebSystemItem *>::const_iterator _fdb = _fdbs.begin()
             ; _fdb != _fdbs.end() && !_is_terminated ; ++_fdb)
        {
            getProgress().setDescription(std::string("Processing ") + _fdb->second->getDescription());
            try {
                rpct::IMonitorable * _monitorable = dynamic_cast<rpct::IMonitorable *>(_fdb->second);
                _monitorable->monitor(0);
                getProgress().addValue();
            } catch (std::exception & _e) {
                LOG4CPLUS_ERROR(logger_, "Caught exception in FebConfiguration::imonitor: " << _e.what());
                getProgress().fail();
                getProgress().setDescription("Error in Front-End Board Monitoring");
                throw std::runtime_error(std::string("Front-End Board Monitoring incomplete: ") + _e.what());
            } catch (...) {
                LOG4CPLUS_ERROR(logger_, "Caught unknown exception in FebConfiguration::imonitor");
                getProgress().fail();
                getProgress().setDescription("Error in Front-End Board Monitoring");
                throw std::runtime_error("Front-End Board Monitoring incomplete: unknown error");
            }

            // paused?
            if (getProgress().isControlPaused() && getProgress().isControlActive()) {
                _hwlock.give();
                getProgress().checkPaused();
                _hwlock.take();
            }
            // terminated?
            _is_terminated = getProgress().checkTerminated();
        }

    if (_is_terminated)
        {
            LOG4CPLUS_ERROR(logger_, "FebConfiguration::imonitor was terminated.");
            getProgress().fail();
            getProgress().setDescription("Front-End Board Monitoring was terminated.");
        }
    else
        {
            getProgress().stop();
            getProgress().setDescription("Finished Front-End Board Monitoring");
            context_.getFebSystem().getMonitoringTime() = getProgress().getDuration();
        }
}

FebConfiguration::FebConfiguration(FebConfigurationCellContext & _context
                                   , std::string const & _supervisor_name
                                   , size_t _supervisor_instance)
    : context_(_context)
    , logger_(log4cplus::Logger::getInstance("FebConfiguration"))
    , remote_logger_(log4cplus::Logger::getInstance("FebConfiguration.RemoteClient"))
    , remote_logger_client_(*(_context.getCell()), remote_logger_, _supervisor_name, _supervisor_instance)
    , remote_logger_client_ptr_(&remote_logger_client_)
    , logger_widget_(_context)
    , progress_(logger_)
    , progress_widget_(_context, progress_, "Progress", true)
    , dbclient_(_context.getCell())
    , action_wl_(new toolbox::task::WaitingWorkLoop("ActionWorkLoop"))
    , configure_action_(toolbox::task::bind(this
                                            , &FebConfiguration::configure_wl
                                            , "configure_action"))
    , load_configuration_action_(toolbox::task::bind(this
                                                     , &FebConfiguration::load_configuration_wl
                                                     , "load_configuration_action"))
    , monitor_action_(toolbox::task::bind(this
                                          , &FebConfiguration::monitor_wl
                                          , "monitor_action"))
{
    remote_logger_client_ptr_->addReference();
    remote_logger_.addAppender(remote_logger_client_ptr_);
    logger_.addAppender(SharedAppenderPtr(&(logger_widget_.getAppender())));
    logger_widget_.setIsOwned(false);

    progress_widget_.setIsOwned(false);

    action_wl_->activate();
}

FebConfiguration::~FebConfiguration()
{
    logger_.removeAppender(logger_widget_.getAppender().getName());

    progress_.terminate();

    // waitingworkloop problem: no ~WaitingWorkloop
    try {
        if (action_wl_->isActive())
            action_wl_->cancel();
    } catch (...) {}

    delete action_wl_;
    delete configure_action_;
    delete load_configuration_action_;
    delete monitor_action_;
}

std::vector<std::string> FebConfiguration::getFebConfigKeys() const
{
    std::vector<std::string> _feb_config_keys;
    try {
        std::vector<xdata::String> _xdata_feb_config_keys =
            dbclient_.getLocalConfigKeys(rpct::xdaqutils::XdaqDbServiceClient::SUBSYSTEM_FEBS);
        for (std::vector<xdata::String>::const_iterator _xdata_feb_config_key = _xdata_feb_config_keys.begin()
                 ; _xdata_feb_config_key != _xdata_feb_config_keys.end() ; ++_xdata_feb_config_key)
            _feb_config_keys.push_back(_xdata_feb_config_key->value_);
    } catch (xcept::Exception & _e) {
        LOG4CPLUS_ERROR(logger_, "Could not load FEB configuration keys, caught exception: " << _e.what());
    } catch (std::exception & _e) {
        LOG4CPLUS_ERROR(logger_, "Could not load FEB configuration keys, caught exception: " << _e.what());
    } catch (...) {
        LOG4CPLUS_ERROR(logger_, "Could not load FEB configuration keys, caught exception");
    }
    return _feb_config_keys;
}

void FebConfiguration::configure(std::string const & _key, bool _attach, bool _nothrow)
{
    LOG4CPLUS_INFO(logger_, "FebConfiguration::configure");

    try {
        rpct::tools::Lock _lock(mutex_, 1, 0); // only one action at a time

        if (getProgress().isProcessActive()) // only one wl at a time
            {
                LOG4CPLUS_ERROR(logger_, "Could not start configuration, previous request still running.");
                if (!_nothrow)
                    throw std::runtime_error("Could not start configuration, previous request still running.");
                return;
            }

        // progress: before wl
        getProgress().setTitle("Feb Configuration");
        getProgress().init();
        getProgress().setDescription("Starting Front-End Board Configuration");
        getProgress().setMaximum(1);
        // set progress_.isProcessActive()
        getProgress().start();

        configuration_key_ = _key;

        if (_attach)
            {
                try {
                    iconfigure(_key);
                    std::ostringstream _message;
                    _message << "Configuration ready for "
                             << context_.getCell()->getName()
                             << ": " << context_.getFebSystem().rpct::FebSystem::getStatusSummary();
                    remote_logger_.log(rpct::tools::State::getStateLogLevel(context_.getFebSystem().getPState())
                                       , _message.str());
                } catch (std::runtime_error const & _e) {
                    LOG4CPLUS_ERROR(remote_logger_, "Configuration failed for "
                                    << context_.getCell()->getName()
                                    << ": " << _e.what());
                    if (!_nothrow)
                        throw;
                }
            }
        else
            action_wl_->submit(configure_action_);

    } catch (int _e) {
        LOG4CPLUS_WARN(logger_, "Could not start configuration, mutex not available.");
        if (!_nothrow)
            throw std::runtime_error("Could not start configuration, mutex not available.");
    }
}

void FebConfiguration::loadConfiguration(std::string const & _key, bool _attach, bool _nothrow)
{
    LOG4CPLUS_INFO(logger_, "FebConfiguration::loadConfiguration");

    try {
        rpct::tools::Lock _lock(mutex_, 1, 0); // only one action at a time

        if (getProgress().isProcessActive()) // only one wl at a time
            {
                LOG4CPLUS_ERROR(logger_, "Could not start configuration loading, previous request still running.");
                if (!_nothrow)
                    throw std::runtime_error("Could not start configuration loading, previous request still running.");
                return;
            }

        // progress: before wl
        getProgress().setTitle("Feb Load Configuration");
        getProgress().init();
        getProgress().setDescription("Starting Front-End Board Configuration Loading");
        getProgress().setMaximum(1);
        // set progress_.isProcessActive()
        getProgress().start();

        configuration_key_ = _key;

        if (_attach)
            {
                try {
                    iload_configuration(_key);
                    std::ostringstream _message;
                    _message << "Load Configuration ready for "
                             << context_.getCell()->getName()
                             << ": " << context_.getFebSystem().rpct::FebSystem::getStatusSummary();
                    remote_logger_.log(rpct::tools::State::getStateLogLevel(context_.getFebSystem().getPState())
                                       , _message.str());
                } catch (std::runtime_error const & _e) {
                    LOG4CPLUS_ERROR(remote_logger_, "Load Configuration failed for "
                                    << context_.getCell()->getName()
                                    << ": " << _e.what());
                    if (!_nothrow)
                        throw;
                }
            }
        else
            action_wl_->submit(load_configuration_action_);
    } catch (int _e) {
        LOG4CPLUS_WARN(logger_, "Could not start configuration loading, mutex not available.");
        if (!_nothrow)
            throw std::runtime_error("Could not start configuration loading, mutex not available.");
    }
}

void FebConfiguration::monitor(bool _attach, bool _nothrow)
{
    LOG4CPLUS_INFO(logger_, "FebConfiguration::monitor");

    try {
        rpct::tools::Lock _lock(mutex_, 1, 0); // only one action at a time

        if (getProgress().isProcessActive()) // only one wl at a time
            {
                LOG4CPLUS_ERROR(logger_, "Could not start monitoring, previous request still running.");
                if (!_nothrow)
                    throw std::runtime_error("Could not start monitoring, previous request still running.");
                return;
            }

        // progress: before wl
        getProgress().setTitle("Feb Monitoring");
        getProgress().init();
        getProgress().setDescription("Starting Front-End Board Monitoring");
        getProgress().setMaximum(1);
        // set progress_.isProcessActive()
        getProgress().start();

        if (_attach)
            {
                try {
                    imonitor();
                    std::ostringstream _message;
                    _message << "Monitoring ready for "
                             << context_.getCell()->getName()
                             << ": " << context_.getFebSystem().rpct::FebSystem::getStatusSummary();
                    remote_logger_.log(rpct::tools::State::getStateLogLevel(context_.getFebSystem().getPState())
                                       , _message.str());
                } catch (std::runtime_error const & _e) {
                    LOG4CPLUS_ERROR(remote_logger_, "Monitoring failed for "
                                    << context_.getCell()->getName()
                                    << ": " << _e.what());
                    if (!_nothrow)
                        throw;
                }
            }
        else
            action_wl_->submit(monitor_action_);
    } catch (int _e) {
        LOG4CPLUS_WARN(logger_, "Could not start monitoring, mutex not available.");
        if (!_nothrow)
            throw std::runtime_error("Could not start monitoring, mutex not available.");
    }
}

} // namespace worker
} // namespace ts
} // namespace rpct
