#include "rpct/ts/worker/FebConfigurationCommandLoadConfiguration.h"

#include <iostream>

#include "log4cplus/logger.h"

#include "xdata/String.h"

#include "ts/framework/CellWarning.h"

#include "rpct/ts/worker/FebConfigurationCellContext.h"
#include "rpct/ts/worker/FebConfiguration.h"
#include "rpct/xdaqutils/xdaqFebSystem.h"

namespace rpct {
namespace ts {
namespace worker {

FebConfigurationCommandLoadConfiguration::FebConfigurationCommandLoadConfiguration(log4cplus::Logger & _logger, tsframework::CellAbstractContext * _context)
    : tsframework::CellCommand(_logger, _context)
    , context_(dynamic_cast<FebConfigurationCellContext &>(*_context))
{
    logger_ = log4cplus::Logger::getInstance(_logger.getName() + ".FebConfigurationCommandLoadConfiguration");

    getParamList().insert(std::make_pair("key", new xdata::String("")));
}

void FebConfigurationCommandLoadConfiguration::code()
{
    LOG4CPLUS_INFO(logger_, "Starting FebConfigurationCommandLoadConfiguration::code() method");

    std::string _key = getParamList()["key"]->toString();
    std::string _command_result;

    try {
        context_.getFebConfiguration().loadConfiguration(_key, true, false);
        _command_result = context_.getFebSystem().rpct::FebSystem::getStatusSummary();
    } catch (xcept::Exception & _e) {
        _command_result = _e.what();
        getWarning().append(_command_result, tsframework::CellWarning::ERROR);
    } catch (std::exception & _e) {
        _command_result = _e.what();
        getWarning().append(_command_result, tsframework::CellWarning::ERROR);
    } catch (...) {
        _command_result = "unknown exception";
        getWarning().append(_command_result, tsframework::CellWarning::ERROR);
    }

    payload_->fromString(_command_result);
    LOG4CPLUS_INFO(logger_, _command_result);
}

} // namespace worker
} // namespace ts
} // namespace rpct
