#include "rpct/ts/supervisor/FebConfiguration.h"

#include <stdexcept>

#include "log4cplus/loggingmacros.h"

#include "toolbox/task/WaitingWorkLoop.h"
#include "xcept/tools.h"

#include "rpct/ts/supervisor/FebConfigurationCellContext.h"

#include "ts/framework/CellXhannelCell.h"
#include "ts/framework/CellXhannelRequestCell.h"
#include "ts/framework/CellAbstract.h"

#include "rpct/tools/Timer.h"
#include "rpct/tools/Progress.h"

namespace rpct {
namespace ts {
namespace supervisor {

bool FebConfiguration::response_wl(toolbox::task::WorkLoop *)
{
    LOG4CPLUS_INFO(logger_, "Start " << progress_.getTitle() << " response retrieval");

    progress_.setDescription("Retrieving Responses");

    bool _is_terminated(false);
    getProgress().checkPaused();
    _is_terminated = getProgress().checkTerminated();

    rpct::tools::Timer _timer(timeout_);
    while (! requests_.empty() && !_timer.ready() && !_is_terminated) {
        std::vector<std::string> _ready_requests;
        for (std::map<std::string, XhannelRequest>::iterator _request = requests_.begin()
                 ; _request != requests_.end() ; ++_request)
            if (_request->second.request_->hasResponse())
                {
                    _ready_requests.push_back(_request->first);
                    xdata::Serializable * _reply = _request->second.request_->commandReply();
                    LOG4CPLUS_INFO(logger_, _request->second.xhannel_ << " ready: " << _reply->toString());

                    tsframework::CellWarning _warning = _request->second.request_->getWarning();
                    if (_warning.getLevel() >= tsframework::CellWarning::ERROR) {
                        LOG4CPLUS_ERROR(logger_, _request->second.xhannel_ << " ready but with error: " << _warning.getMessage());
                        progress_.addFailed();
                    } else {
                        progress_.addValue();
                    }
                    _request->second.cell_->removeRequest(_request->second.request_);
                }
        rpct::tools::Timer _sleep(1);

        for (std::vector<std::string>::const_iterator _ready_request = _ready_requests.begin()
                 ; _ready_request != _ready_requests.end() ; ++_ready_request)
            requests_.erase(*_ready_request);

        getProgress().checkPaused();
        _is_terminated = getProgress().checkTerminated();

        if (!_is_terminated)
            while (! _sleep.sleep()) {}
    }

    for (std::map<std::string, XhannelRequest>::iterator _request = requests_.begin()
             ; _request != requests_.end() ; ++_request)
        {
            _request->second.cell_->removeRequest(_request->second.request_);
            LOG4CPLUS_ERROR(logger_, _request->second.xhannel_ << " timed out.");
            progress_.addFailed();
        }

    requests_.clear();

    LOG4CPLUS_INFO(logger_, progress_.getTitle() << (_is_terminated ? " response retrieval was stopped." : " finished."));

    progress_.stop();
    progress_.setDescription(_is_terminated
                             ? "Retrieving responses was stopped."
                             : "Retrieving responses ended.");

    return false;
}

FebConfiguration::FebConfiguration(FebConfigurationCellContext & _context)
    : context_(_context)
    , logger_(log4cplus::Logger::getInstance("FebConfiguration"))
    , remote_logger_service_(*(dynamic_cast<xdaq::Application *>(_context.getCell())), logger_)
    , logger_widget_(_context)
    , progress_(logger_)
    , progress_widget_(_context, progress_, "Progress", false)
    , dbclient_(_context.getCell())
    , response_wl_(new toolbox::task::WaitingWorkLoop("ResponseWorkLoop"))
    , response_action_(toolbox::task::bind(this
                                           , &FebConfiguration::response_wl
                                           , "response_action"))
    , timeout_(300)
{
    logger_.addAppender(SharedAppenderPtr(&(logger_widget_.getAppender())));
    logger_widget_.setIsOwned(false);

    progress_widget_.setIsOwned(false);

    response_wl_->activate();
}

FebConfiguration::~FebConfiguration()
{
    logger_.removeAppender(logger_widget_.getAppender().getName());

    progress_.terminate();

    // waitingworkloop problem: no ~WaitingWorkloop
    try {
        if (response_wl_->isActive())
            response_wl_->cancel();
    } catch (...) {}

    delete response_wl_;
    delete response_action_;
}

std::vector<std::string> FebConfiguration::getFebConfigKeys() const
{
    std::vector<std::string> _feb_config_keys;
    try {
        std::vector<xdata::String> _xdata_feb_config_keys =
            dbclient_.getLocalConfigKeys(rpct::xdaqutils::XdaqDbServiceClient::SUBSYSTEM_FEBS);
        for (std::vector<xdata::String>::const_iterator _xdata_feb_config_key = _xdata_feb_config_keys.begin()
                 ; _xdata_feb_config_key != _xdata_feb_config_keys.end() ; ++_xdata_feb_config_key)
            _feb_config_keys.push_back(_xdata_feb_config_key->value_);
    } catch (xcept::Exception & _e) {
        LOG4CPLUS_ERROR(logger_, "Could not load FEB configuration keys, caught exception: " << _e.what());
    } catch (std::exception & _e) {
        LOG4CPLUS_ERROR(logger_, "Could not load FEB configuration keys, caught exception: " << _e.what());
    } catch (...) {
        LOG4CPLUS_ERROR(logger_, "Could not load FEB configuration keys, caught exception");
    }
    return _feb_config_keys;
}

void FebConfiguration::send_command(std::string const & _command, std::string const & _key, unsigned int _timeout)
{
    LOG4CPLUS_INFO(logger_, "start " << _command << " request");

    try {
        rpct::tools::Lock _lock(mutex_, 1, 0); // only one send_command call at a time

        if (progress_.isProcessActive()) // and only one set of requests at a time
            {
                LOG4CPLUS_ERROR(logger_, "Could not start " << _command << ", previous request still running.");
                return;
            }

        std::map<std::string, tsframework::CellXhannelCell*> _xhannels = context_.getFebWorkers(true);

        command_ = _command;
        progress_.init();
        progress_.setTitle(_command);
        progress_.setMaximum(_xhannels.size());
        // set progress_.isProcessActive()
        progress_.start();

        progress_.setDescription("Submitting Requests");

        timeout_ = _timeout;

        for (std::map<std::string, tsframework::CellXhannelCell*>::iterator _xhannel = _xhannels.begin()
                 ; _xhannel != _xhannels.end() ; ++_xhannel)
            {
                XhannelRequest _request;
                _request.xhannel_ = _xhannel->first;
                _request.cell_ = _xhannel->second;
                _request.request_ = dynamic_cast<tsframework::CellXhannelRequestCell*>(_request.cell_->createRequest());
                std::map<std::string, xdata::Serializable *> _params;
                xdata::String _xkey(_key);
                _params["key"] = &_xkey;
                _request.request_->doCommand("sid", true, _command, _params);
                requests_[_xhannel->first] = _request;
                try {
                    _request.cell_->send(_request.request_);
                } catch (xcept::Exception & _e) {
                    _request.cell_->removeRequest(_request.request_);
                    requests_.erase(_xhannel->first);
                    LOG4CPLUS_ERROR(logger_, "Problem submitting " << _command << " Request to " << _xhannel->first
                                    << xcept::stdformat_exception_history(_e));
                    progress_.addFailed();
                }
            }

        response_wl_->submit(response_action_);
    } catch (int _e) {
        LOG4CPLUS_ERROR(logger_, "Could not start " << _command << ", mutex not available.");
    }
}

} // namespace supervisor
} // namespace ts
} // namespace rpct
