#include "rpct/ts/supervisor/XhannelSelection.h"

#include "ts/framework/CellAbstract.h"
#include "ts/framework/CellXhannelCell.h"
#include "log4cplus/loggingmacros.h"

namespace rpct {
namespace ts {
namespace supervisor {

XhannelSelection::XhannelSelection(tsframework::CellAbstractContext & _context
                                   , std::string const & _title
                                   , std::string const & _message)
    : context_(_context)
    , title_(_title)
    , message_(_message)
    , logger_(log4cplus::Logger::getInstance(_context.getCell()->getLogger().getName() + ".XhannelSelection"))
    , logger_widget_(_context)
{
    logger_.addAppender(SharedAppenderPtr(&(logger_widget_.getAppender())));
    logger_widget_.setIsOwned(false);
}

bool XhannelSelection::isDisabled(std::string const & _xhannel) const
{
    return (disabled_xhannels_.count(_xhannel) > 0);
}

void XhannelSelection::setDisabled(std::string const & _xhannel, bool _disabled)
{
    if (_disabled && getXhannelList().count(_xhannel) == 0)
        return;
    if (_disabled)
        {
            disabled_xhannels_.insert(_xhannel);
            LOG4CPLUS_INFO(logger_, "Disabled " << _xhannel);
        }
    else if (disabled_xhannels_.erase(_xhannel))
        {
            LOG4CPLUS_INFO(logger_, "Enabled " << _xhannel);
        }
}

std::map<std::string, tsframework::CellXhannel*> XhannelSelection::getXhannelList(bool _include_disabled) const
{
    std::map<std::string, tsframework::CellXhannel*> _xhannels(context_.getXhannelList());
    for (std::map<std::string, tsframework::CellXhannel*>::iterator _xhannel = _xhannels.begin()
             ; _xhannel != _xhannels.end() ; )
        {
            if (!(dynamic_cast<tsframework::CellXhannelCell*> (_xhannel->second)) || (!_include_disabled && isDisabled(_xhannel->first)))
                _xhannels.erase(_xhannel++);
            else
                ++_xhannel;
        }
    return _xhannels;
}

} // namespace supervisor
} // namespace ts
} // namespace rpct
