#include "rpct/ts/supervisor/FebConfigurationCellContext.h"

#include "rpct/ts/supervisor/FebConfiguration.h"

namespace rpct {
namespace ts {
namespace supervisor {

FebConfigurationCellContext::FebConfigurationCellContext(log4cplus::Logger & _log, tsframework::CellAbstract * _cell)
    : tsframework::CellAbstractContext(_log, _cell)
    , feb_configuration_(new FebConfiguration(*this))
{}

FebConfigurationCellContext::~FebConfigurationCellContext()
{
    delete feb_configuration_;
}

} // namespace supervisor
} // namespace ts
} // namespace rpct
