#include "rpct/ts/supervisor/XhannelSelectionPanel.h"

#include "rpct/ts/supervisor/XhannelSelectionCellContext.h"

#include "log4cplus/logger.h"

#include "ajax/ResultBox.h"
#include "ajax/Div.h"

#include "ajax/PlainHtml.h"

#include "ajax/toolbox.h"
#include "ajax/PolymerElement.h"

#include "rpct/ts/worker/panels/LogPanel.h"

#include "jsoncpp/json/json.h"


namespace rpct {
namespace ts {
namespace supervisor {

void XhannelSelectionPanel::layout(cgicc::Cgicc & _cgi) {
        remove();

        setEvent("onDisableSelection", ajax::Eventable::OnClick, this, &XhannelSelectionPanel::onDisableSelection);
        setEvent("onEnableSelection", ajax::Eventable::OnClick, this, &XhannelSelectionPanel::onEnableSelection);
        setEvent("getXhannelsList", ajax::Eventable::OnClick, this, &XhannelSelectionPanel::getXhannelsList);

        ajax::PolymerElement * root = new ajax::PolymerElement("xhannel-selection-panel");
        root->set("title", selection_.getTitle());
        rpcttspanels::LogPanel* logPanel = new rpcttspanels::LogPanel( getContext(), getLogger() );
        root->add(logPanel);
        
        add(root);
}

void XhannelSelectionPanel::getXhannelsList(cgicc::Cgicc & _cgi, std::ostream & _out) {
    Json::Value xhannelList(Json::arrayValue);
    Json::Value xhannelItem(Json::objectValue);
    std::map<std::string, tsframework::CellXhannel *> _xhannels(selection_.getXhannelList(true));

    for (std::map<std::string, tsframework::CellXhannel *>::const_iterator _xhannel = _xhannels.begin(); _xhannel != _xhannels.end() ; ++_xhannel) {
        xhannelItem["name"] = _xhannel->first;
        xhannelItem["isEnabled"] = !(selection_.isDisabled(_xhannel->first));
        xhannelList.append(xhannelItem);
    }


    _out << xhannelList;
}
    

void XhannelSelectionPanel::initLayout()
{

}

void XhannelSelectionPanel::onLockClick(cgicc::Cgicc & _cgi, std::ostream & _out)
{

}

void XhannelSelectionPanel::onSelectAll(cgicc::Cgicc & _cgi, std::ostream & _out)
{
    LOG4CPLUS_TRACE(logger_, "Called onSelectAll.");

}

void XhannelSelectionPanel::onSelectNone(cgicc::Cgicc & _cgi, std::ostream & _out)
{
    LOG4CPLUS_TRACE(logger_, "Called onSelectNone.");

}

void XhannelSelectionPanel::onDisableSelection(cgicc::Cgicc & _cgi, std::ostream & _out)
{
    LOG4CPLUS_TRACE(logger_, "Called onDisableSelection.");
    selection_.setDisabled(ajax::toolbox::getSubmittedValue(_cgi,"xhannelname"), true);

}

void XhannelSelectionPanel::onEnableSelection(cgicc::Cgicc & _cgi, std::ostream & _out)
{
    LOG4CPLUS_TRACE(logger_, "Called onEnableSelection.");
    selection_.setDisabled(ajax::toolbox::getSubmittedValue(_cgi,"xhannelname"), false);

}

void XhannelSelectionPanel::refreshSelection()
{
    LOG4CPLUS_TRACE(logger_, "Called refreshSelection.");

}

XhannelSelectionPanel::XhannelSelectionPanel(tsframework::CellAbstractContext * _context, log4cplus::Logger & _log)
    : tsframework::CellPanel(_context, _log)
    , selection_(dynamic_cast<XhannelSelectionCellContext *>(_context)->getXhannelSelection())
    // , locked_(true)
{
    // initLayout();
}

} // namespace supervisor
} // namespace ts
} // namespace rpct
