#include "rpct/ts/supervisor/FebConfigurationPanel.h"

#include "log4cplus/loggingmacros.h"

#include "ajax/PlainHtml.h"
#include "ajax/Div.h"


#include "ajax/ResultBox.h"
#include "ajax/toolbox.h"

#include "ts/framework/CellAbstract.h"

#include "rpct/ts/common/ajaxtools.h"

#include "rpct/ts/supervisor/FebConfiguration.h"
#include "rpct/ts/supervisor/FebConfigurationCellContext.h"

#include "ajax/PolymerElement.h"
#include "jsoncpp/json/json.h"
#include <sstream>

#include "rpct/ts/worker/panels/LogPanel.h"


namespace rpct {
namespace ts {
namespace supervisor {

void FebConfigurationPanel::layout(cgicc::Cgicc & _cgi) {
    remove();
    setEvent("febGetTitle", ajax::Eventable::OnClick, this, &FebConfigurationPanel::febGetTitle);
    setEvent("febGetFebSystemURL", ajax::Eventable::OnClick, this, &FebConfigurationPanel::febGetFebSystemURL);
    setEvent("getFebConfigKeys", ajax::Eventable::OnClick, this, &FebConfigurationPanel::getFebConfigKeys);
    setEvent("onMonitorFEBsClick", ajax::Eventable::OnClick, this, &FebConfigurationPanel::onMonitorFEBsClick);
    setEvent("onMonitorPauseFEBsClick", ajax::Eventable::OnClick, this, &FebConfigurationPanel::onMonitorPauseFEBsClick);
    setEvent("onMonitorResumeFEBsClick", ajax::Eventable::OnClick, this, &FebConfigurationPanel::onMonitorResumeFEBsClick);
    setEvent("onMonitorTerminateFEBsClick", ajax::Eventable::OnClick, this, &FebConfigurationPanel::onMonitorTerminateFEBsClick);
    setEvent("onMonitorRefreshFEBsClick", ajax::Eventable::OnClick, this, &FebConfigurationPanel::onMonitorRefreshFEBsClick);
    setEvent("onConfigureFEBsClick", ajax::Eventable::OnClick, this, &FebConfigurationPanel::onConfigureFEBsClick);
    setEvent("onLoadConfigurationFEBsClick", ajax::Eventable::OnClick, this, &FebConfigurationPanel::onLoadConfigurationFEBsClick);


    context_.getFebConfiguration().getLoggerWidget();

    ajax::PolymerElement* root = new ajax::PolymerElement("feb-configuration-panel-supervisor");
    rpcttspanels::LogPanel* logPanel = new rpcttspanels::LogPanel( getContext(), getLogger() );
    root->add(logPanel);
    

    root->add(&(context_.getFebConfiguration().getProgressBarWidget()));
    add(root);
}

void FebConfigurationPanel::loadFebConfigKeys()
{
    feb_config_keys_.clear();
    std::vector<std::string> _feb_config_keys(context_.getFebConfiguration().getFebConfigKeys());
    if (_feb_config_keys.empty()) {
        feb_config_keys_.push_back(std::pair<std::string, std::string>("no config keys loaded!",""));
        feb_config_keys_.push_back(std::pair<std::string, std::string>("FEBS_DEFAULT","FEBS_DEFAULT"));
    } else {
        for (std::vector<std::string>::const_iterator _feb_config_key = _feb_config_keys.begin()
           ; _feb_config_key != _feb_config_keys.end() ; ++_feb_config_key)
            feb_config_keys_.push_back(std::pair<std::string, std::string>(*_feb_config_key, *_feb_config_key));
        feb_config_keys_.push_back(std::pair<std::string, std::string>("use previously obtained values",""));
    }
}

void FebConfigurationPanel::getFebConfigKeys(cgicc::Cgicc& cgi,std::ostream& out) {
    Json::Value root(Json::arrayValue);
    std::vector<std::pair<std::string, std::string> >::const_iterator itEnd = feb_config_keys_.end();
    for (std::vector<std::pair<std::string, std::string> >::const_iterator it = feb_config_keys_.begin()
       ; it != itEnd
       ; ++it) {
        std::stringstream ss;
        ss << it->first;
        root.append(ss.str());
    }
    out << root;
}

void FebConfigurationPanel::febGetTitle(cgicc::Cgicc & _cgi, std::ostream & _out) {
    _out << "FEB Control Panel for " + getContext()->getCell()->getName();
}

void FebConfigurationPanel::febGetFebSystemURL(cgicc::Cgicc & _cgi, std::ostream & _out) {

    std::set< const xdaq::ApplicationDescriptor * > _descs = getContext()->getApContext()->getDefaultZone()->getApplicationDescriptors("xdaqFebPublisher");
    if (!_descs.empty())
        {
            std::string _urn = (*(_descs.begin()))->getURN();
            std::string _url = (*(_descs.begin()))->getContextDescriptor()->getURL();
            _out << _url + "/" + _urn +"/";
        }
    else {
        _out << "";
    }
}

FebConfigurationPanel::FebConfigurationPanel(tsframework::CellAbstractContext* _context, log4cplus::Logger & _logger)
    : tsframework::CellPanel(_context, _logger)
    , context_(dynamic_cast<FebConfigurationCellContext & >(*_context))
    , logger_(log4cplus::Logger::getInstance(_logger.getName() + ".FebConfigurationPanel"))
    , locked_(true)
{
    loadFebConfigKeys();

    initLayout(context_.getCell()->getName());
}

FebConfigurationPanel::~FebConfigurationPanel()
{}

void FebConfigurationPanel::initLayout(std::string const & _title)
{

    add(&(context_.getFebConfiguration().getLoggerWidget()));
}


void FebConfigurationPanel::onLockClick(cgicc::Cgicc & _cgi, std::ostream & _out)
{

}

void FebConfigurationPanel::onConfigureFEBsClick(cgicc::Cgicc & _cgi, std::ostream & _out)
{
    LOG4CPLUS_INFO(logger_, "Called onConfigureFEBsClick.");

    std::string _key = ajax::toolbox::getSubmittedValue(_cgi, "febConfig");

    context_.getFebConfiguration().send_command("ConfigureFEBs", _key, 300);

    context_.getFebConfiguration().getProgressBarWidget().innerHtml(_cgi, _out);
}

void FebConfigurationPanel::onLoadConfigurationFEBsClick(cgicc::Cgicc & _cgi, std::ostream & _out)
{
    LOG4CPLUS_INFO(logger_, "Called onLoadConfigurationFEBsClick.");

    std::string _key = ajax::toolbox::getSubmittedValue(_cgi, "febLdConfig");
    context_.getFebConfiguration().send_command("LoadConfigurationFEBs", _key, 10);
    context_.getFebConfiguration().getProgressBarWidget().innerHtml(_cgi, _out);
}

void FebConfigurationPanel::onMonitorFEBsClick(cgicc::Cgicc & _cgi, std::ostream & _out)
{
    LOG4CPLUS_INFO(logger_, "Called onMonitorFEBsClick.");
    
    context_.getFebConfiguration().send_command("MonitorFEBs", "", 300);

    context_.getFebConfiguration().getProgressBarWidget().innerHtml(_cgi, _out);
}



    void FebConfigurationPanel::onMonitorPauseFEBsClick(cgicc::Cgicc & _cgi, std::ostream & _out) {
        LOG4CPLUS_INFO(logger_, "Called onMonitorPauseFEBsClick.");
        context_.getFebConfiguration().getProgressBarWidget().pause(_cgi, _out);
    }   
    void FebConfigurationPanel::onMonitorResumeFEBsClick(cgicc::Cgicc & _cgi, std::ostream & _out) {
        LOG4CPLUS_INFO(logger_, "Called onMonitorResumeFEBsClick.");
        context_.getFebConfiguration().getProgressBarWidget().resume(_cgi, _out);
    }   
    void FebConfigurationPanel::onMonitorTerminateFEBsClick(cgicc::Cgicc & _cgi, std::ostream & _out) {
        LOG4CPLUS_INFO(logger_, "Called onMonitorTerminateFEBsClick.");
        context_.getFebConfiguration().getProgressBarWidget().terminate(_cgi, _out);
    }   
    void FebConfigurationPanel::onMonitorRefreshFEBsClick(cgicc::Cgicc & _cgi, std::ostream & _out) {
        LOG4CPLUS_INFO(logger_, "Called onMonitorRefreshFEBsClick.");
        context_.getFebConfiguration().getProgressBarWidget().refresh(_cgi, _out);

        _out << context_.getFebConfiguration().getProgressBarWidget().getProgressBar().getProgressStr();
        // _out << "context_.getFebConfiguration().getProgressBarWidget().getProgressBar().getProgressStr()";
    }   

} // namespace supervisor
} // namespace ts
} // namespace rpct
