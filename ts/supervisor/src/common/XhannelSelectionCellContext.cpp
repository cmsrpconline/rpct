#include "rpct/ts/supervisor/XhannelSelectionCellContext.h"

namespace rpct {
namespace ts {
namespace supervisor {

XhannelSelectionCellContext::XhannelSelectionCellContext(log4cplus::Logger & _log, tsframework::CellAbstract * _cell)
    : tsframework::CellAbstractContext(_log, _cell)
    , xhannel_selection_(*this)
{}

} // namespace supervisor
} // namespace ts
} // namespace rpct
