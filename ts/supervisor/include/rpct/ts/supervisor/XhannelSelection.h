#ifndef rpct_ts_supervisor_XhannelSelection_h
#define rpct_ts_supervisor_XhannelSelection_h

#include <string>
#include <set>

#include "ts/framework/CellAbstractContext.h"

#include "log4cplus/logger.h"
#include "rpct/ts/common/LoggerWidget.h"

namespace rpct {
namespace ts {
namespace supervisor {

class XhannelSelection
{
public:
    XhannelSelection(tsframework::CellAbstractContext & _context
                     , std::string const & _title = std::string("Xhannel Selection Panel")
                     , std::string const & _message = std::string(""));

    tsframework::CellAbstractContext & getContext();

    std::string const & getTitle() const;
    std::string const & getMessage() const;
    void setTitle(std::string const & _title);
    void setMessage(std::string const & _message);

    log4cplus::Logger & getLogger();
    rpct::LoggerWidget & getLoggerWidget();

    bool isDisabled(std::string const & _xhannel) const;
    void setDisabled(std::string const & _xhannel, bool _disabled = true);
    std::map<std::string, tsframework::CellXhannel *> getXhannelList(bool _include_disabled = false) const;

protected:
    tsframework::CellAbstractContext & context_;

    std::string title_, message_;

    log4cplus::Logger logger_;
    rpct::LoggerWidget logger_widget_;

    std::set<std::string> disabled_xhannels_;
};

inline tsframework::CellAbstractContext & XhannelSelection::getContext()
{
    return context_;
}

inline std::string const & XhannelSelection::getTitle() const
{
    return title_;
}

inline std::string const & XhannelSelection::getMessage() const
{
    return message_;
}

inline void XhannelSelection::setTitle(std::string const & _title)
{
    title_ = _title;
}

inline void XhannelSelection::setMessage(std::string const & _message)
{
    message_ = _message;
}

inline log4cplus::Logger & XhannelSelection::getLogger()
{
    return logger_;
}

inline rpct::LoggerWidget & XhannelSelection::getLoggerWidget()
{
    return logger_widget_;
}

} // namespace supervisor
} // namespace ts
} // namespace rpct

#endif // rpct_ts_supervisor_XhannelSelection_h
