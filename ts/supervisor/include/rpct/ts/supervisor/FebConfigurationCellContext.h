#ifndef rpct_ts_supervisor_FebConfigurationCellContext_h
#define rpct_ts_supervisor_FebConfigurationCellContext_h

#include <string>
#include <map>

#include "ts/framework/CellAbstractContext.h"

namespace tsframework {
class CellXhannelCell;
} // namespace tsframework

namespace rpct {
namespace ts {
namespace supervisor {

class FebConfiguration;

class FebConfigurationCellContext
    : public virtual tsframework::CellAbstractContext
{
public:
    FebConfigurationCellContext(log4cplus::Logger & _log, tsframework::CellAbstract * _cell);
    ~FebConfigurationCellContext();

    virtual std::map<std::string, tsframework::CellXhannelCell *> getFebWorkers(bool _active_only = false) = 0;

    FebConfiguration & getFebConfiguration();

protected:
    FebConfiguration * feb_configuration_;
};

inline FebConfiguration & FebConfigurationCellContext::getFebConfiguration()
{
    return *feb_configuration_;
}

} // namespace supervisor
} // namespace ts
} // namespace rpct

#endif // rpct_ts_supervisor_FebConfigurationCellContext_h
