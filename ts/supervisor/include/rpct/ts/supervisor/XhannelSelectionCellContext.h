#ifndef rpct_ts_supervisor_XhannelSelectionCellContext_h
#define rpct_ts_supervisor_XhannelSelectionCellContext_h

#include "ts/framework/CellAbstractContext.h"

#include "rpct/ts/supervisor/XhannelSelection.h"

namespace rpct {
namespace ts {
namespace supervisor {

class XhannelSelectionCellContext
    : public virtual tsframework::CellAbstractContext
{
public:
    XhannelSelectionCellContext(log4cplus::Logger & _log, tsframework::CellAbstract * _cell);

    XhannelSelection & getXhannelSelection();
    XhannelSelection const & getXhannelSelection() const;

protected:
    XhannelSelection xhannel_selection_;
};

inline XhannelSelection & XhannelSelectionCellContext::getXhannelSelection()
{
    return xhannel_selection_;
}

inline XhannelSelection const & XhannelSelectionCellContext::getXhannelSelection() const
{
    return xhannel_selection_;
}

} // namespace supervisor
} // namespace ts
} // namespace rpct

#endif // rpct_ts_supervisor_XhannelSelectionCellContext_h
