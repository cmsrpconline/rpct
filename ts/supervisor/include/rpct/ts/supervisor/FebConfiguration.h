#ifndef rpct_ts_supervisor_FebConfiguration_h
#define rpct_ts_supervisor_FebConfiguration_h

#include <string>
#include <vector>
#include <map>

#include "rpct/tools/Mutex.h"

#include "log4cplus/logger.h"
#include "rpct/xdaqtools/remotelogger/Service.h"
#include "rpct/ts/common/LoggerWidget.h"

#include "rpct/tools/Progress.h"
#include "rpct/ts/common/ProgressBar.h"
#include "rpct/xdaqutils/XdaqDbServiceClient.h"

namespace toolbox {
namespace task {
class ActionSignature;
class WorkLoop;
} // namespace task
} // namespace toolbox

namespace tsframework {
class CellXhannelCell;
class CellXhannelRequestCell;
} // namespace tsframework

namespace rpct {
namespace ts {
namespace supervisor {

class FebConfigurationCellContext;

struct XhannelRequest {
    std::string xhannel_;
    tsframework::CellXhannelCell * cell_;
    unsigned int timeout_;
    tsframework::CellXhannelRequestCell * request_;
};

class FebConfiguration
{
protected:
    bool response_wl(toolbox::task::WorkLoop *);

public:
    FebConfiguration(FebConfigurationCellContext & _context);
    ~FebConfiguration();

    log4cplus::Logger & getLogger();
    rpct::LoggerWidget & getLoggerWidget();

    rpct::tools::Progress & getProgress();
    rpct::ProgressBarWidget & getProgressBarWidget();

    rpct::xdaqutils::XdaqDbServiceClient & getDBClient();
    std::vector<std::string> getFebConfigKeys() const;

    void send_command(std::string const & _command, std::string const & _key, unsigned int _timeout = 300);

    void addMethod(toolbox::task::ActionSignature *, const std::string &) {}

protected:
    FebConfigurationCellContext & context_;

    mutable rpct::tools::Mutex mutex_;

    log4cplus::Logger logger_;
    rpct::xdaqtools::remotelogger::Service remote_logger_service_;
    rpct::LoggerWidget logger_widget_;

    rpct::tools::Progress progress_;
    rpct::ProgressBarWidget progress_widget_;

    mutable rpct::xdaqutils::XdaqDbServiceClient dbclient_;

    std::string command_;
    std::map<std::string, XhannelRequest> requests_;

    toolbox::task::WorkLoop * response_wl_;
    toolbox::task::ActionSignature * response_action_;
    unsigned int timeout_;
};

} // namespace supervisor
} // namespace ts
} // namespace rpct

#include "rpct/ts/supervisor/FebConfiguration-inl.h"

#endif // rpct_ts_supervisor_FebConfiguration_h
