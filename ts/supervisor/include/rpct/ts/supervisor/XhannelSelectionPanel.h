#ifndef rpct_ts_supervisor_XhannelSelectionPanel_h
#define rpct_ts_supervisor_XhannelSelectionPanel_h

#include "ts/framework/CellPanel.h"

#include "rpct/ts/supervisor/XhannelSelection.h"

#include <iosfwd>
#include <vector>
#include <string>
#include <map>

namespace ajax {
class ResultBox;
class Div;
class Editable;
class CheckBox;
} // namespace ajax

namespace rpct {
namespace ts {
namespace supervisor {

class XhannelSelectionPanel
    : public tsframework::CellPanel
{
private:
    void layout(cgicc::Cgicc & _cgi);

protected:
    void initLayout();

    void getXhannelsList(cgicc::Cgicc & _cgi, std::ostream & _out);
    void onLockClick(cgicc::Cgicc & _cgi, std::ostream & _out);
    void onSelectAll(cgicc::Cgicc & _cgi, std::ostream & _out);
    void onSelectNone(cgicc::Cgicc & _cgi, std::ostream & _out);
    void onDisableSelection(cgicc::Cgicc & _cgi, std::ostream & _out);
    void onEnableSelection(cgicc::Cgicc & _cgi, std::ostream & _out);
    void refreshSelection();
    

public:
    XhannelSelectionPanel(tsframework::CellAbstractContext * _context, log4cplus::Logger & _log);

protected:
    XhannelSelection & selection_;


};

} // namespace supervisor
} // namespace ts
} // namespace rpct

#endif // rpct_ts_supervisor_XhannelSelectionPanel_h
