/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Ildefons Magrans de Abril	                         *
 *************************************************************************/

#include "rpct/ts/cell/Cell.h"
#include "rpct/xdaqutils/LogUtils.h"

#include "rpct/ts/cell/MTCCIISelfTest.h"
#include "rpct/ts/cell/Configuration.h"
#include "rpct/ts/cell/MTCCIIInterconnectionTest.h"

#include "ts/framework/CellOperationFactory.h"
#include "ts/framework/CellFactory.h"
#include "ts/framework/CellPanelFactory.h"


// #include "rpct/ts/panels/RpcMonPanel.h"
#include "rpct/ts/panels/RpctTCHistogramPanel.h"
#include "rpct/ts/panels/RpctLBHistogramPanel.h"
#include "rpct/ts/panels/RpctSCHistogramPanel.h"
#include "rpct/ts/panels/LBPanel.h"


#include "rpct/ts/worker/panels/LogPanel.h"
// #include "rpct/ts/worker/RpctCellContext.h"


  
#include "rpct/ts/supervisor/XhannelSelectionPanel.h"
#include "rpct/ts/supervisor/FebConfigurationPanel.h"

#include "xcept/tools.h"
//#include "ts/toolbox/CellLogMacros.h"

#include "rpct/ts/cell/SendAlarm.h"
#include "rpct/ts/cell/SendMail.h"
#include "rpct/ts/cell/SynchronizeLinksCommand.h"

// using namespace tsframework;

namespace rpcttscell {

XDAQ_INSTANTIATOR_IMPL(Cell)

Cell::Cell(xdaq::ApplicationStub * s) :
    tsframework::CellAbstract(s) {

    try {
        cellContext_ = new CellContext(getApplicationLogger(), this);
        rpct::rpcLogSetup(getApplicationLogger(),this);
    } catch (xcept::Exception& e) {
        std::ostringstream msg;
        msg << "Error instantiating the CellContext. Find the bug and restart the executive. Exception found: ";
        msg << xcept::stdformat_exception_history(e);
        LOG4CPLUS_ERROR(getApplicationLogger(),msg.str());
    }
}

// rpcttsworker::LBCell::LBCell(xdaq::ApplicationStub * s) :
//     tsframework::CellAbstract(s) {
//     LOG4CPLUS_INFO(getApplicationLogger(), "LB worker cell constructor");

//     try {
//         cellContext_ = new rpcttsworker::LBCellContext(getApplicationLogger(), this);
//         rpct::rpcLogSetup(getApplicationLogger(),this);
//     } catch (xcept::Exception& e) {
//         std::ostringstream msg;
//         msg << "Error instantiating the CellContext. Find the bug and restart the executive. Exception found: ";
//         msg << xcept::stdformat_exception_history(e);
//         LOG4CPLUS_ERROR(getApplicationLogger(),msg.str());
//     }
//     LOG4CPLUS_INFO(getApplicationLogger(), "Worker cell constructor exit");
// }

Cell::~Cell() {

} 

void Cell::init() {

    getContext()->addImport("/rpct/ts/xworker/html/elements/elements.html");

    tsframework::CellOperationFactory* opF = getContext()->getOperationFactory();
    opF->add<rpcttscell::Configuration>("Configuration"); //Run Control - in rpcFM is Configuration, should be changed there to Run Control

    tsframework::CellFactory* cmdF = getContext()->getCommandFactory();
    cmdF->add<rpcttscell::SendAlarm>("Default","SendAlarm");
    cmdF->add<rpcttscell::SendMail>("Default","SendMail");
    cmdF->add<rpcttscell::SynchronizeLinksCommand>("RPCT","SynchronizeLinksCommand");

    tsframework::CellPanelFactory* panelF = getContext()->getPanelFactory();
    // panelF->add<rpcttspanels::TCMonPanel> ("RPC TC Monitoring");
    // panelF->add<rpcttspanels::LBMonPanel> ("RPC LB Monitoring");
    panelF->add<rpcttspanels::RpctTCHistogramPanel> ("TC Histograms");
    panelF->add<rpcttspanels::RpctLBHistogramPanel> ("LB Histograms");
    panelF->add<rpcttspanels::RpctSCHistogramPanel> ("SC Histograms");
    panelF->add<rpct::ts::supervisor::XhannelSelectionPanel> ("Worker Control");
    panelF->add<rpct::ts::supervisor::FebConfigurationPanel> ("FEB Control");
    panelF->add<rpcttscell::LBPanel> ("LB Control");
    panelF->add<rpcttspanels::LogPanel> ("Logging");
    panelF->add<rpcttscell::LBPanel> ("Home");

    // To have the LBPanel initialized so log messages appear.
    rpcttscell::LBPanel lbpanel(cellContext_, getApplicationLogger());
}

}
