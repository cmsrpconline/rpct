/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Project Manager: Ildfons Magrans de Abril      			 *
 *                                                                       *
 * developer: Marc Magrans de Abril               		         *
 *************************************************************************/
#include "rpct/ts/cell/writesTTS.h"		

#include "xdata/xdata.h"

rpcttscell::writesTTS::writesTTS(log4cplus::Logger& log,tsframework::CellAbstractContext* context)
  :tsframework::CellCommand(log,context)
{
	logger_ =  log4cplus::Logger::getInstance(getLogger().getName() + ".writesTTS");
  getParamList().insert(std::make_pair("sTTS value", new xdata::UnsignedLong(0)));
}

// !! edit !!
bool rpcttscell::writesTTS::preCondition()
{
  return true;
}

// !! edit !!
void rpcttscell::writesTTS::code()									
{
  std::cout << "I am writing the sTTS register " << getParamList()["sTTS value"]->toString() << std::endl;
  *dynamic_cast<xdata::String*>(payload_) = "write operation successfull in sTTS register of trigger subsystem";
}
