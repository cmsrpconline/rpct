/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Marc Magrans de Abril                                *
 *************************************************************************/
 
#include "rpct/ts/cell/SimpleXdaq.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPFault.h"
#include "xoap/Method.h"

#include "xcept/tools.h"

XDAQ_INSTANTIATOR_IMPL(rpcttscell::SimpleXdaq)

rpcttscell::SimpleXdaq::SimpleXdaq(xdaq::ApplicationStub * s) throw (xdaq::exception::Exception)
  : xdaq::WebApplication(s), myString_(new xdata::String("test string")) 
{       
  LOG4CPLUS_INFO(this->getApplicationLogger(),"Starting XDAQ simple constructor");      
  
  //parameter initialization
  this->getApplicationInfoSpace()->fireItemAvailable("myString",myString_);
        
  //command      
  xoap::bind(this, &rpcttscell::SimpleXdaq::simpleCommand, "simpleCommand", "urn:xdaq-application:rpcttscell::SimpleXdaq");
  
  LOG4CPLUS_INFO(this->getApplicationLogger(),"Finishing XDAQ simple constructor");
}

xoap::MessageReference 
rpcttscell::SimpleXdaq::simpleCommand(xoap::MessageReference msg) throw(xoap::exception::Exception)
{
  LOG4CPLUS_INFO(this->getApplicationLogger(),"'simpleCommand' is to be executed!");
  
  xoap::MessageReference reply;
  try 
    {
      reply = xoap::createMessage();
      xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
      envelope.addNamespaceDeclaration("soap-enc","http://schemas.xmlsoap.org/soap/encoding/");
      envelope.addNamespaceDeclaration("xsi","http://www.w3.org/2001/XMLSchema-instance");
      envelope.addNamespaceDeclaration("xsd","http://www.w3.org/2001/XMLSchema");
    } catch (xcept::Exception& e)
      {
	std::ostringstream str;
	str << "Can not create the SOAP message in simpleCommand. Exception caught: ";
	str << xcept::stdformat_exception_history(e);
	
	LOG4CPLUS_ERROR(getApplicationLogger(), str.str());
	
	reply = xoap::createMessage();
	xoap::SOAPBody body=reply->getSOAPPart().getEnvelope().getBody();
	xoap::SOAPFault fault= body.addFault();
	fault.setFaultCode("Client");
	fault.setFaultString (str.str());
      } 
  return reply;
}
