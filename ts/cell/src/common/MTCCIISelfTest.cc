/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: I. Magrans          				         *
 *************************************************************************/
#include "rpct/ts/cell/MTCCIISelfTest.h"

#include "ts/toolbox/CellToolbox.h"
#include "ts/exception/CellException.h"
#include "ts/framework/CellAbstractContext.h"

#include <string>

#include "toolbox/string.h"

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

#include <iostream>
#include <unistd.h>

rpcttscell::MTCCIISelfTest::MTCCIISelfTest(log4cplus::Logger& log, tsframework::CellAbstractContext* context)
  :tsframework::CellOperation(log,context)
{
  logger_ =  log4cplus::Logger::getInstance(getLogger().getName() + ".MTCCIISelfTest");
  init();
}

rpcttscell::MTCCIISelfTest::~MTCCIISelfTest()
{
  ;
}

void rpcttscell::MTCCIISelfTest::init() {
  try {
    getFSM().addState("halted");
    getFSM().addState("prepared");
    getFSM().addState("tested");
    getFSM().addTransition("halted", "prepared", "prepare", this, &rpcttscell::MTCCIISelfTest::c_prepare, &rpcttscell::MTCCIISelfTest::f_prepare);
    getFSM().addTransition("prepared", "tested", "test", this, &rpcttscell::MTCCIISelfTest::c_test, &rpcttscell::MTCCIISelfTest::f_test);
    getFSM().addTransition("tested", "prepared", "prepare", this, &rpcttscell::MTCCIISelfTest::c_prepare, &rpcttscell::MTCCIISelfTest::f_prepare);
    getFSM().addTransition("tested", "tested", "test", this, &rpcttscell::MTCCIISelfTest::c_test, &rpcttscell::MTCCIISelfTest::f_test);
    getFSM().setInitialState("halted");
    getFSM().reset();
  } catch(tsexception::CellException e) {
    XCEPT_RETHROW (tsexception::CellException,"MTCCIISelfTest can not Init()",e);
  }
}

void rpcttscell::MTCCIISelfTest::initialization()
{
  LOG4CPLUS_INFO(getLogger(),"MTCCIISelfTest::initialization executed");

  setResult("MTCCIISelfTest::initialization executed");
}

void rpcttscell::MTCCIISelfTest::ending()
{
  LOG4CPLUS_INFO(getLogger(),"MTCCIISelfTest::ending executed");
  setResult("MTCCIISelfTest::ending executed");

}

void rpcttscell::MTCCIISelfTest::resetting()
{

  //your code here
}

bool rpcttscell::MTCCIISelfTest::c_prepare()
{
  LOG4CPLUS_INFO(getLogger(),"MTCCIISelfTest::c_prepare executed");
  setResult("MTCCIISelfTest::c_prepare executed");
  return true;
}

void rpcttscell::MTCCIISelfTest::f_prepare()
{
  LOG4CPLUS_INFO(getLogger(),"MTCCIISelfTest::f_prepare executed");
  setResult("MTCCIISelfTest::f_prepare executed");

  //your code here
  return;
}

bool rpcttscell::MTCCIISelfTest::c_test()
{
  LOG4CPLUS_INFO(getLogger(),"MTCCIISelfTest::c_test executed");
  setResult("MTCCIISelfTest::c_test executed");

  //your code here
  return true;
}

void rpcttscell::MTCCIISelfTest::f_test()
{
  LOG4CPLUS_INFO(getLogger(),"MTCCIISelfTest::f_test executed");
  setResult("MTCCIISelfTest::f_test executed");

  //sleepmillis(10000);
  //your code here
  return;
}
