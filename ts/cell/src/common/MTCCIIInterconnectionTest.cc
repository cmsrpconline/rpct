/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Ildefons Magrans de Abril	                         *
 *************************************************************************/

#include "rpct/ts/cell/MTCCIIInterconnectionTest.h"

#include "ts/toolbox/CellToolbox.h"
#include "ts/exception/CellException.h"
#include "ts/framework/CellAbstractContext.h"

#include <string>

#include "toolbox/string.h"

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

#include <iostream>
#include <unistd.h>

rpcttscell::MTCCIIInterconnectionTest::MTCCIIInterconnectionTest(log4cplus::Logger& log,tsframework::CellAbstractContext* context)
  :tsframework::CellOperation(log,context)
{
	logger_ =  log4cplus::Logger::getInstance(getLogger().getName() + ".MTCCIIInterconnectionTest");

    init();
}

rpcttscell::MTCCIIInterconnectionTest::~MTCCIIInterconnectionTest()
{
  ;
}

void rpcttscell::MTCCIIInterconnectionTest::init()
{
  try {
    getFSM().addState("halted");
    getFSM().addState("prepared");
    getFSM().addState("started");
    getFSM().addState("analysed");
    getFSM().addTransition("halted", "prepared", "prepare", this, &rpcttscell::MTCCIIInterconnectionTest::c_prepare, &rpcttscell::MTCCIIInterconnectionTest::f_prepare);
    getFSM().addTransition("prepared", "started", "start", this, &rpcttscell::MTCCIIInterconnectionTest::c_start, &rpcttscell::MTCCIIInterconnectionTest::f_start);
    getFSM().addTransition("started", "analysed", "analyse", this, &rpcttscell::MTCCIIInterconnectionTest::c_analyze, &rpcttscell::MTCCIIInterconnectionTest::f_analyze);
    getFSM().setInitialState("halted");
    getFSM().reset();
  } catch(tsexception::CellException e){
    XCEPT_RETHROW (tsexception::CellException,"MTCCIIInterconnectionTest can not Init()",e);
  }
  
  getParamList()["custom"] = new xdata::String("");
}

void rpcttscell::MTCCIIInterconnectionTest::initialization()
{
  LOG4CPLUS_INFO(getLogger(),"Running MTCCIIInterconnectionTest::initialization() method");

  //your code here
}

void rpcttscell::MTCCIIInterconnectionTest::ending()
{
  LOG4CPLUS_INFO(getLogger(),"Running MTCCIIInterconnectionTest::ending() method");

  //Your code here
}

void rpcttscell::MTCCIIInterconnectionTest::resetting()
{
  LOG4CPLUS_INFO(getLogger(),"Running MTCCIIInterconnectionTest::resetting() method");
  
  //your code here
}

bool rpcttscell::MTCCIIInterconnectionTest::c_prepare()
{
  LOG4CPLUS_INFO(getLogger(),"Running condition MTCCIIInterconnectionTest::c_prepare() method");

  //your code here
  return true;
}

void rpcttscell::MTCCIIInterconnectionTest::f_prepare()
{
  LOG4CPLUS_INFO(getLogger(),"Running transition functionality MTCCIIInterconnectionTest::f_prepare() method");
  
  setResult("MTCCIIInterconnectionTest::f_prepare() executed with 'custom' parameter = " + getParamList()["custom"]->toString());
  //your code here
  return;
}

bool rpcttscell::MTCCIIInterconnectionTest::c_start()
{
  LOG4CPLUS_INFO(getLogger(),"Running condition MTCCIIInterconnectionTest::c_start() method");
  
  //your code here
  return true;
}

void rpcttscell::MTCCIIInterconnectionTest::f_start()
{
  LOG4CPLUS_INFO(getLogger(),"Running transition functionality MTCCIIInterconnectionTest::f_start() method");
  
  setResult("MTCCIIInterconnectionTest::f_start() executed with 'custom' parameter = " + getParamList()["custom"]->toString());
  
  //your code here
  return;
}

bool rpcttscell::MTCCIIInterconnectionTest::c_analyze()
{
  LOG4CPLUS_INFO(getLogger(),"Running condition MTCCIIInterconnectionTest::c_analyze() method");
  
  //your code here
  return true;
}

void rpcttscell::MTCCIIInterconnectionTest::f_analyze()
{
  LOG4CPLUS_INFO(getLogger(),"Running transition functionality MTCCIIInterconnectionTest::f_analyze() method");
  
  setResult("MTCCIIInterconnectionTest::f_analyze() executed with 'custom' parameter = " + getParamList()["custom"]->toString());
  //your code here
  return;
}
