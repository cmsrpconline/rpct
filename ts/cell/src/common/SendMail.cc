#include "rpct/ts/cell/SendMail.h"

#include "ts/toolbox/SmtpMessenger.h"

#include "xdata/String.h"

rpcttscell::SendMail::SendMail(log4cplus::Logger& log,tsframework::CellAbstractContext* context)
  :tsframework::CellCommand(log,context)
{
  logger_ = log4cplus::Logger::getInstance(log.getName() +".SendMail");


  getParamList().insert(std::make_pair("to",new xdata::String("")));
  getParamList().insert(std::make_pair("subject",new xdata::String("Hello World!")));
  getParamList().insert(std::make_pair("message", new xdata::String("This is the body of a mail")));
}

bool rpcttscell::SendMail::preCondition() {
  return true;
}

void rpcttscell::SendMail::code() {

  std::string recipients = getParamList()["to"]->toString();
  std::string subject = getParamList()["subject"]->toString();
  std::string message = getParamList()["message"]->toString();
  
  tstoolbox::SmtpMessenger m(recipients,subject,message);
  //m.addAttachment("/my/file/subsystem.log");
  m.send();
  
  *dynamic_cast<xdata::String*>(payload_) = "Message send without problem";
}
