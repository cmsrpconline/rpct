/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: I. Magrans          				         *
 *************************************************************************/
#include "rpct/ts/cell/CellContext.h"
#include "rpct/xdaqutils/LogUtils.h"
#include "ts/exception/CellException.h"
#include "ts/toolbox/CellToolbox.h"
#include "ts/framework/Level1Source.h"

#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellXhannelCell.h"

#include <string>
#include <cstdlib>

#include "toolbox/string.h"

#include "log4cplus/logger.h"

#include "rpct/ts/cell/MonitorSource.h"

rpcttscell::CellContext::CellContext(log4cplus::Logger& log, tsframework::CellAbstract* cell)
    : tsframework::CellAbstractContext(log, cell)
    , rpcttsworker::LoggerCellContext(log, cell)
    , rpct::ts::supervisor::XhannelSelectionCellContext(log, cell)
    , rpct::ts::supervisor::FebConfigurationCellContext(log, cell)
{
	logger_ = log4cplus::Logger::getInstance(getLogger().getName() + ".CellContext");
	datasource_ = new MonitorSource("rpct_supervisor",this, logger_);

	// https://savannah.cern.ch/task/index.php?14360
	getLevel1Source().setSubsystem("RPC");

        getXhannelSelection().setTitle("FSM and FEB Worker Control");
        getXhannelSelection().setMessage("Please leave the use of this panel to experts.<br />It can disrupt the correct operation of the RPC Trigger and DAQ.");
}

rpcttscell::CellContext::~CellContext()
{
  delete datasource_;
}

std::string 
rpcttscell::CellContext::getClassName()
{
  return "CellContext";
}


std::map<std::string, tsframework::CellXhannelCell*> rpcttscell::CellContext::getActiveWorkers()
{
    std::map<std::string, tsframework::CellXhannel *> _xhannels = getXhannelSelection().getXhannelList(false);
    std::map<std::string, tsframework::CellXhannelCell*> _result;
    tsframework::CellXhannelCell* _xhannel_cell(0);
    for(std::map<std::string, tsframework::CellXhannel*>::const_iterator _xhannel = _xhannels.begin()
            ; _xhannel != _xhannels.end() ; ++_xhannel)
        if ((_xhannel_cell = dynamic_cast<tsframework::CellXhannelCell*>(_xhannel->second)))
            _result.insert(std::pair<std::string, tsframework::CellXhannelCell*>(_xhannel->first, _xhannel_cell));

    return _result;
}

std::map<std::string, tsframework::CellXhannelCell*> rpcttscell::CellContext::getLBWorkers(bool _active_only)
{
    std::map<std::string, tsframework::CellXhannelCell*> _result;
    std::map<std::string, tsframework::CellXhannel *> _xhannels = getXhannelSelection().getXhannelList(!_active_only);
    tsframework::CellXhannelCell* _xhannel_cell(0);
    for(std::map<std::string, tsframework::CellXhannel*>::const_iterator _xhannel = _xhannels.begin()
            ; _xhannel != _xhannels.end() ; ++_xhannel)
        if ((_xhannel_cell = dynamic_cast<tsframework::CellXhannelCell*>(_xhannel->second))
            && _xhannel_cell->getTargetDescriptor()->getClassName() == "rpcttsworker::LBCell")
            _result.insert(std::pair<std::string, tsframework::CellXhannelCell*>(_xhannel->first, _xhannel_cell));

    return _result;
}

std::map<std::string, tsframework::CellXhannelCell *> rpcttscell::CellContext::getFebWorkers(bool _active_only)
{
    return getLBWorkers(_active_only);
}
