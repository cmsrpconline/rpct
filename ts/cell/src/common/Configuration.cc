/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Ildefons Magrans de Abril                                    *
 *************************************************************************/

#include "rpct/ts/cell/Configuration.h"
#include "rpct/ts/cell/CellContext.h"
#include "rpct/ts/supervisor/XhannelSelectionCellContext.h"
#include "rpct/xdaqutils/LogUtils.h"

#include "ts/framework/CellXhannelRequestCell.h"
#include "ts/framework/CellXhannelRequestXdaqSimple.h"
#include "ts/toolbox/CellToolbox.h"
#include "ts/exception/CellException.h"

#include "xcept/tools.h"

#include "xdata/xdata.h"

#include <string>
#include <iomanip>
#include <sstream>

#include "toolbox/string.h"

#include "log4cplus/logger.h"

#include <iostream>
#include <unistd.h>

using namespace tsframework;
using namespace tsexception;

namespace rpcttscell {

const unsigned int Configuration::TIMEOUT_SEC(60);


Configuration::Configuration(log4cplus::Logger& log, CellAbstractContext* context) :
        		        tsruncontrol::ConfigurationBase(log, context) {
    logger_ =  log4cplus::Logger::getInstance(getLogger().getName() + ".Configuration");
    removeParameter("DCS LHC Flags");
    removeParameter("TSC KEY");
    removeParameter("L1 KEY");
    getParamList() ["KEY"] = new xdata::String ( "" );

    getParamList()["enable Hard Reset by TTC"] = new xdata::Boolean(true);
    getParamList()["enable opt links out"] = new xdata::Boolean(true);
}

Configuration::~Configuration() {
}

// --------------------------------------------------------
/*void Configuration::resetting()
{
  std::vector<std::string> workers(getWorkers());
  for(std::vector<std::string>::iterator i(workers.begin()); i != workers.end(); ++i) {

      reset(*i);
  }

  setResult("Configuration.reset done");
}*/

void Configuration::setParameter(CellXhannelXdaqSimple* x, const std::string& name,
        const std::string& nameSpace, const std::string& type, const std::string& value) {

    // this is called only for external xdaqs
    LOG4CPLUS_INFO(getLogger(), "... setParameter called");
    CellXhannelRequestXdaqSimple* req;

    req = dynamic_cast<CellXhannelRequestXdaqSimple*> (x->createRequest());
    req->doParameterSet(name, nameSpace, type, value);
    try {
        x->send(req);
    } catch (xcept::Exception& e) {
        x->removeRequest(req);
        XCEPT_RETHROW(CellException, "Error requesting ParameterSet for xhannel ??" /*+ req->getTargetUrl()*/, e);
    }
    while (!req->hasResponse())
        usleep(100 * 1000); //sleepmillis(100);

    x->removeRequest(req);
}

void Configuration::doCommand(CellXhannelXdaqSimple* x, const std::string& name,
        const std::string& nameSpace) {

    // this is called only for external xdaqs
    LOG4CPLUS_INFO(getLogger(), "... doCommand called for command " + name);
    CellXhannelRequestXdaqSimple* req;

    req = dynamic_cast<CellXhannelRequestXdaqSimple*> (x->createRequest());
    req->doCommand(name, nameSpace);
    try {
        x->send(req);
    } catch (xcept::Exception& e) {
        x->removeRequest(req);
        //XCEPT_RETHROW(CellException, "Error requesting ParameterSet for xhannel 'TC_TEST'.", e);
        XCEPT_RETHROW(CellException, "Error requesting command '" + name + "' for xhannel ??" /*+ req->getTargetUrl()*/, e);
    }
    while (!req->hasResponse())
        usleep(100 * 1000); //sleepmillis(100);

    x->removeRequest(req);
}

bool Configuration::checkConfigure() {
    getWarning().clear();
    return true;
}

void Configuration::execConfigure() {
    LOG4CPLUS_INFO(getLogger(), "Executing Configuration::f_configure");

    if(getParamList()["Configuration Key"]->toString() == "") {
    	getParamList()["Configuration Key"]->setValue(*getParamList()["KEY"]);
    }//temporary solution until the rpcFM is not corrected

    rpcttscell::CellContext* _context = dynamic_cast<rpcttscell::CellContext*>(getContext());

    std::map<std::string, tsframework::CellXhannelCell*> _workers(_context->getActiveWorkers());
    std::map<std::string, tsframework::CellXhannelCell*> _lb_tb_workers, _ici_workers, _pi_workers;

    std::map<std::string, tsframework::CellXhannelCell*>::const_iterator _workers_end(_workers.end());
    for (std::map<std::string, tsframework::CellXhannelCell*>::const_iterator _worker = _workers.begin()
            ; _worker != _workers_end ; ++_worker) {
        if (_worker->second->getTargetDescriptor()->getClassName().find("LBCell") != std::string::npos)
            _lb_tb_workers.insert(*_worker);
        else if (_worker->second->getTargetDescriptor()->getClassName().find("TBCell") != std::string::npos)
            _lb_tb_workers.insert(*_worker);
        else if (_worker->first.find("TCDS_ICI") != std::string::npos)
            _ici_workers.insert(*_worker);
        else if (_worker->first.find("TCDS_PI") != std::string::npos)
            _pi_workers.insert(*_worker);
    }

    std::map<std::string, xdata::Serializable*> const & _params(getParamList());
    std::map<std::string, xdata::Serializable*> _ici_params(_params), _pi_params(_params);

    xdata::String emptyKey("");
    // add code to select correct files
    xdata::String _ici_configuration_file("/nfshome0/rpcpro/TCDS/TCDS-RPC-CPM-HardReset.txt");
    xdata::String _pi_configuration_file("/nfshome0/rpcpro/TCDS/TCDS-RPC-pi.txt");
    _ici_params.insert(std::pair<std::string, xdata::Serializable*>("Configuration File"
            , &_ici_configuration_file));
    _ici_params["Configuration Key"] = &emptyKey; //was "KEY"

    _pi_params.insert(std::pair<std::string, xdata::Serializable*>("Configuration File"
            , &_pi_configuration_file));
    _pi_params["Configuration Key"] = &emptyKey; //was "KEY"

    std::string _result;

    // ICI Configure
    executeTransitions(_ici_workers, "configure", _ici_params, 120);
    if (getWarning().getLevel() >= tsframework::CellWarning::ERROR) return;
    _result += getResult();

    // PI Configure
    executeTransitions(_pi_workers, "configure", _pi_params, 120);
    if (getWarning().getLevel() >= tsframework::CellWarning::ERROR) return;
    _result += getResult();

    // LB/TB Configure
    executeTransitions(_lb_tb_workers, "configure", _params, 120);
    if (getWarning().getLevel() >= tsframework::CellWarning::ERROR) return;
    _result += getResult();

    // LB/TB Synchronise
    executeTransitions(_lb_tb_workers, "synchronize_links", _params, 120);
    _result += getResult();

    setResult(_result);
}

bool Configuration::checkStart() {
    getWarning().clear();
    return true;

}
void Configuration::execStart() {
    executeTransitions((dynamic_cast<rpcttscell::CellContext*>(getContext()))->getActiveWorkers(), "start", getParamList(), 38);
}

bool Configuration::checkPause() {
    getWarning().clear();
    return true;

}
void Configuration::execPause() {
    executeTransitions((dynamic_cast<rpcttscell::CellContext*>(getContext()))->getActiveWorkers(), "pause", getParamList(), 38);
}

bool Configuration::checkResume() {
    getWarning().clear();
    return true;

}
void Configuration::execResume() {
    executeTransitions((dynamic_cast<rpcttscell::CellContext*>(getContext()))->getActiveWorkers(), "resume", getParamList(), 38);

}

bool Configuration::checkStop() {
    getWarning().clear();
    return true;
}

void Configuration::execStop() {
    executeTransitions((dynamic_cast<rpcttscell::CellContext*>(getContext()))->getActiveWorkers(), "stop", getParamList(), 38);
}

bool Configuration::checkColdReset(){
    return true;
}

void Configuration::execColdReset() {
    executeTransitions((dynamic_cast<rpcttscell::CellContext*>(getContext()))->getActiveWorkers(), "coldReset", getParamList(), 10);
}

void Configuration::resetting() {


    LOG4CPLUS_INFO(getLogger(),"Configuration.reset...");

    int timeout = 24;

    std::map<std::string,RequestHandlerPtr> requests;
    std::vector<std::string> workers(getWorkers());
    for(std::vector<std::string>::iterator i(workers.begin()); i != workers.end(); ++i) {
        try {
            requests.insert(std::make_pair(*i, resetAsynch(*i)));
        } catch (xcept::Exception& e) {
            getWarning().append("@" + *i + " : " + xcept::stdformat_exception_history(e), tsframework::CellWarning::ERROR);
        }
    }

    setResult("Reset request send to workers");

    std::ostringstream replies;
    for(std::map<std::string,RequestHandlerPtr>::iterator i(requests.begin()); i != requests.end(); ++i) {

        std::string result = reply(i->first,i->second, timeout);

        replies << "@" << i->first << " : " << result << "\n" << std::endl;
    }

    getFSM().reset();

    setResult("Configuration.reset done");
    LOG4CPLUS_INFO(getLogger(),"Configuration.reset done");

}


Configuration::RequestHandlerPtr Configuration::resetAsynch(const std::string& xhannel) {
    tsframework::CellXhannelCell* x = getCellXhannelCell(xhannel);

    RequestHandlerPtr h_ptr(new RequestHandler(x));

    std::string opid = "Run Control";
    bool async=true;
    h_ptr->getRequest()->doOpReset(getSessionId(), async, opid);

    x->send(h_ptr->getRequest());

    return h_ptr;
}

// --------------------------------------------------------
/*void Configuration::reset(const std::string& xhannel) {
	tsframework::CellXhannelCell* x = getCell(xhannel);

	RequestHandlerPtr h_ptr(new RequestHandler(x));

	std::string opid = "Run Control";
	bool async=false;
	h_ptr->getRequest()->doOpReset(getSessionId(), async, opid);

	try {

		x->send(h_ptr->getRequest());
	}
	catch (xcept::Exception& e) {

		//reset should not throw
		getWarning().append("@" + xhannel + ".reset : " + xcept::stdformat_exception_history(e),tsframework::CellWarning::ERROR);
		return;
	}
	tsframework::CellWarning warning = h_ptr->getRequest()->getWarning();
	if (warning.getLevel() >= tsframework::CellWarning::ERROR)
		getWarning().append("@" + xhannel + ".reset : " + warning.getMessage()+"\n", warning.getLevel());

}*/

CellXhannelCell* Configuration::getCellXhannelCell(const std::string& xhannel) {
    CellXhannelCell* x = dynamic_cast<CellXhannelCell*> (getContext()->getXhannel(xhannel));
    if (!x) {
        XCEPT_RAISE(tsexception::CellException,"Can not get CellXhannelCell for xhannel '" + xhannel + ". This xhannel is not of class CellXhannelCell.");
    }
    return x;
}


Configuration::RequestHandlerPtr Configuration::request(std::pair<std::string, tsframework::CellXhannelCell*> const & _worker
        , std::string const & _transition
        , std::map<std::string, xdata::Serializable *> const & _params)
{
    RequestHandlerPtr h_ptr(new RequestHandler(_worker.second));

    std::string opid = "Run Control";
    bool async=true;
    h_ptr->getRequest()->doOpSendCommand(getSessionId(), async, opid, _transition, _params);

    _worker.second->send(h_ptr->getRequest());

    return h_ptr;
}

std::string Configuration::reply(const std::string& xhannel, Configuration::RequestHandlerPtr h_ptr, int timeout) {

    time_t start,end;
    time(&start);
    time(&end);

    while(!h_ptr->getRequest()->hasResponse() && difftime(end,start)<timeout) {
        usleep(100 * 1000); //sleepmillis(100);
        time(&end);
    }

    if (difftime(end,start)>=timeout) {
        std::ostringstream msg;
        msg << "Reply to '" << xhannel << "' timeouted after " << timeout << " sec";
        XCEPT_RAISE(tsexception::CellException,msg.str());
    }

    xdata::Serializable* s = h_ptr->getRequest()->opSendCommandReply();
    std::string result = s?s->toString():"";

    tsframework::CellWarning warning = h_ptr->getRequest()->getWarning();

    if (warning.getLevel() > tsframework::CellWarning::INFO)
        getWarning().append("@" +xhannel + " : " + warning.getMessage()+"\n", warning.getLevel());
    return result;
}


void Configuration::executeTransitions(std::map<std::string, tsframework::CellXhannelCell*> const & _workers
        , std::string const & _transition
        , std::map<std::string, xdata::Serializable *> const & _params
        , int _timeout)
{
    LOG4CPLUS_INFO(getLogger(),"Executing transition '" + _transition + "'");

    std::map<std::string,RequestHandlerPtr> requests;

    std::map<std::string, tsframework::CellXhannelCell*>::const_iterator _workers_end(_workers.end());
    for (std::map<std::string, tsframework::CellXhannelCell*>::const_iterator _worker = _workers.begin()
            ; _worker != _workers_end ; ++_worker) {
        try {
            requests.insert(std::make_pair(_worker->first, request(*_worker, _transition, _params)));
            if(_transition == "configure")
                usleep(300000);
                //to protect against problems with the DB services - but not sure if it realy helps 
        } catch (xcept::Exception& e) {
            getWarning().append("@" + _worker->first + "." + _transition + " : " + xcept::stdformat_exception_history(e), tsframework::CellWarning::ERROR);
        }
    }

    setResult("Request to execute '" + _transition + "' send to workers");

    std::ostringstream replies;
    for(std::map<std::string,RequestHandlerPtr>::iterator i(requests.begin()); i != requests.end(); ++i) {

        std::string result = reply(i->first,i->second, _timeout);

        replies << "@" << i->first << "." << _transition << " : " << result << std::endl;
    }

    setResult(replies.str());
    LOG4CPLUS_INFO(getLogger(),"Transition '" + _transition + "' executed: " << std::endl << replies.str());

}

} // namespace rpcttscell
