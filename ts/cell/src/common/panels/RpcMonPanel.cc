#include "rpct/ts/panels/RpcMonPanel.h"
#include "rpct/xdaqutils/LogUtils.h"

#include "ts/framework/CellXhannelTB.h"
#include "ts/framework/CellXhannelRequestTB.h"
#include "ts/exception/CellException.h"
#include "ts/framework/CellXhannelXdaqSimple.h"
#include "ts/framework/CellXhannelCell.h"
#include "ts/framework/CellXhannelMonitor.h"
#include "ts/framework/CellXhannelRequestMonitor.h"
#include "ts/framework/CellAbstract.h"

#include "xdata/TableIterator.h"

#include "ajax/PlainHtml.h"
#include "ajax/Form.h"
#include "ajax/SubmitButton.h"
#include "ajax/ComboBox.h"
#include "ajax/CheckBox.h"
#include "ajax/LayoutContainer.h"
#include "ajax/LayoutElement.h"
#include "ajax/toolbox.h"
#include "ajax/Image.h"

#include <iostream>

using namespace std;
using namespace tsframework;
using namespace tsexception;

namespace rpcttspanels {

const char* RpcMonPanel::STATUS_STRINGS[] = { "OK", "OK", "WARNING", "ERROR" };  //here SOFT_WARNING = OK

const char* RpcMonPanel::statusToString(int status) {
    if (status < 0 || status > (int)(sizeof(STATUS_STRINGS) / sizeof(STATUS_STRINGS[0]))) {
        return "Invalid status string";
    }
    return STATUS_STRINGS[status];
}

RpcMonPanel::RpcMonPanel(CellAbstractContext* context, log4cplus::Logger& logger) :
    CellPanel(context, logger), autoChkValue_(false) {
	logger_ =  log4cplus::Logger::getInstance(getLogger().getName() + ".RpcMonPanel");
    result_ = new ajax::ResultBox();
    result_->set("style", "margin:10px; ");
    result_->setIsOwned(false);

    resultTimer_ = new ajax::ResultBox();
    resultTimer_->setId(getId() + "resultTimer_");
    resultTimer_->setIsOwned(false);

    timer_ = new ajax::Timer();
    timer_->setId(getId() + "timer_");
    timer_->setEventRate(10);
    timer_->setIsOwned(false);
}

RpcMonPanel::~RpcMonPanel() {
    //delete result_;
}


void RpcMonPanel::fillResult() {

    result_->remove();

    getFlashlists();
    ajax::PlainHtml* statusHtml = new ajax::PlainHtml();
    result_->add(statusHtml);

    typedef std::map<std::string,CellXhannel*> XhannelsMap;
    XhannelsMap tcXhannels;
    XhannelsMap xhannels = getContext()->getXhannelList();
    for (XhannelsMap::iterator iXhannel = xhannels.begin(); iXhannel != xhannels.end(); iXhannel++) {
        CellXhannelCell* x = dynamic_cast<CellXhannelCell*>(iXhannel->second);
        if (x != 0) {
            tcXhannels.insert(XhannelsMap::value_type(iXhannel->first, iXhannel->second));
        }
    }

    ajax::Table* table = new ajax::Table();
    table->setShowRules(true);
    table->setShowHeader(true);

    typedef vector<string> Columns;
    Columns columns;
    columns.push_back("Crate");
    columns.push_back("Status");
    columns.push_back("Time");
    for (Columns::iterator iCol = columns.begin(); iCol != columns.end(); ++iCol) {
        table->addColumn(*iCol, ajax::Table::String);
    }

    toolbox::TimeVal newestTime;
    // find the newest time. It is needed to find timestamps which are too old in comparison with this time.
    for (XhannelsMap::iterator iXhannel = tcXhannels.begin(); iXhannel != tcXhannels.end(); iXhannel++) {
        TFlashlists::iterator i = flashlists_.find(iXhannel->first);
        if (i != flashlists_.end()) {
            xdata::Serializable* s = flashlistData_->getValueAt(i->second, "timestamp");
            if (s != 0) {
                xdata::TimeVal* time = dynamic_cast<xdata::TimeVal*>(s);
                if (time != 0) {
                    if (time->value_ > newestTime) {
                        newestTime = time->value_;
                    }
                }
            }
        }
    }

    int row = 0;
    int overallStatus = -1;
    bool noDataFound = false;
    for (XhannelsMap::iterator iXhannel = tcXhannels.begin(); iXhannel != tcXhannels.end(); iXhannel++) {
        string partName = iXhannel->first;

	if (!acceptXhannel(partName, iXhannel->second))continue;

        ajax::PlainHtml* cratePlain = new ajax::PlainHtml();
        table->setWidgetAt("Crate", row, cratePlain);

        ajax::PlainHtml* statusPlain = new ajax::PlainHtml();
        table->setWidgetAt("Status", row, statusPlain);

        ajax::PlainHtml* timePlain = new ajax::PlainHtml();
        table->setWidgetAt("Time", row, timePlain);

        TFlashlists::iterator i = flashlists_.find(partName);
        if (i == flashlists_.end()) {
	  cratePlain->getStream() << partName;
	  statusPlain->getStream() << "No data";
	  noDataFound = true;
        } else {
            try {

                xdata::Serializable* s;
                s = flashlistData_->getValueAt(i->second, "context");

                if (s == 0) {
		  
		  
		  cratePlain->getStream() << "|" << partName << "|";

		}


                cratePlain->getStream() << "<a href='";
                cratePlain->getStream() << s->toString();
                cratePlain->getStream() << "'>" << partName << "</a>";

                s = flashlistData_->getValueAt(i->second, "monStatus");
                if (s == 0) {
                    XCEPT_RAISE(tsexception::CellException, "monitoring data: monStatus not found");
                }
                xdata::Integer* status = dynamic_cast<xdata::Integer*>(s);
                if (status == 0) {
                    XCEPT_RAISE(tsexception::CellException, "invalid type of monStatus: " + s->type());
                }

                s = flashlistData_->getValueAt(i->second, "timestamp");
                if (s == 0) {
                    XCEPT_RAISE(tsexception::CellException, "monitoring data: timestamp not found");
                }
                xdata::TimeVal* time = dynamic_cast<xdata::TimeVal*>(s);
                if (time == 0) {
                    XCEPT_RAISE(tsexception::CellException, "invalid type of timestamp: " + s->type());
                }
                timePlain->getStream() << time->toString();

                // check if time is not too old, or period between current timestamp and newest timestamp is too big
                double age = (double)(toolbox::TimeVal::gettimeofday() - time->value_);
                double difference = (double)(newestTime - time->value_);
                if (age > 60.0 || difference > 10.1) {
                    statusPlain->getStream() << "WARNING: Monitoring data too old";
                    noDataFound = true;
                }
                else {
                    statusPlain->getStream() << statusToString(status->value_);
                    if (status->value_ > overallStatus) {
                        overallStatus = status->value_;
                    }
                }
            }
            catch (std::exception& e) {
                LOG4CPLUS_ERROR(getLogger(), e.what());
                statusPlain->getStream() << "Error: " << e.what();
            }
        }
        row++;
    }
    result_->add(table);

    statusHtml->getStream() << "<p>Status:&nbsp;";
    if (overallStatus != -1 && overallStatus != STATUS_ERROR && noDataFound) {
        // there is monitoring data but not from all expected crates
        statusHtml->getStream() << "WARNING: monitoring data lacking from some crates";
    }
    else {
        statusHtml->getStream() << (overallStatus == -1 ? "Unknown" : statusToString(overallStatus)) ;
    }
    statusHtml->getStream() << "</p>";

}



void RpcMonPanel::layout(cgicc::Cgicc& cgi) {

    LOG4CPLUS_INFO(getLogger(), "layout");

    remove();


    ajax::PlainHtml* title = new ajax::PlainHtml();
    title->getStream()
      << "<h2 align=\"center\" style=\"font-family:arial; color:grey;\" >" << getTitle() << "</h2>";
    add(title);

    fillResult();
    LOG4CPLUS_INFO(getLogger(), "result filled");
    add(result_);
    add(resultTimer_);

    ajax::Button* refreshBtn = new ajax::Button();
    refreshBtn->setId(getId() + "refreshBtn_");
    refreshBtn->setImage("ts/ajaxell/extern/icons/html/icons/arrow_refresh.png");
    setEvent(refreshBtn, ajax::Eventable::OnClick, result_, this, &RpcMonPanel::refresh);

    ajax::CheckBox* autoChk = new ajax::CheckBox();
    autoChk->setCaption("Autorefresh");
    autoChk->setId(getId() + "autoChk_");
    autoChk->setChecked(autoChkValue_);
    setEvent(autoChk, ajax::Eventable::OnClick, resultTimer_, this, &RpcMonPanel::autorefresh);

    ajax::LayoutElement* leRefreshB = new ajax::LayoutElement();
    leRefreshB->setPosition(ajax::LayoutElement::Left);
    leRefreshB->add(refreshBtn);

    ajax::LayoutElement* leAutoChk = new ajax::LayoutElement();
    leAutoChk->setPosition(ajax::LayoutElement::Right);
    leAutoChk->add(autoChk);

    ajax::LayoutContainer* lcRefresh = new ajax::LayoutContainer();
    lcRefresh->set("style", "padding:20px; height:20px; width:86%;");
    lcRefresh->add(leRefreshB);
    lcRefresh->add(leAutoChk);

    add(lcRefresh);
}

void RpcMonPanel::refresh(cgicc::Cgicc& cgi, std::ostream& out) {
    fillResult();
    result_->innerHtml(cgi, out);
}

void RpcMonPanel::autorefresh(cgicc::Cgicc& cgi, std::ostream& out) {

    std::string chBoxId = ajax::toolbox::getSubmittedValue(cgi, "_id_");
    std::string value = ajax::toolbox::getSubmittedValue(cgi, chBoxId);

    if (value == "false") {
        autoChkValue_ = false;

        resultTimer_->remove();
        removeEvent(timer_->getId(), ajax::Eventable::OnTime);

        ajax::PlainHtml* plain = new ajax::PlainHtml();
        plain->getStream() << " ";
        resultTimer_->add(plain);
    } else {
        autoChkValue_ = true;

        setEvent(timer_, ajax::Eventable::OnTime, result_, this, &RpcMonPanel::refresh);
        //
        resultTimer_->remove();
        resultTimer_->add(timer_);

    }

    resultTimer_->innerHtml(cgi, out);
}

void RpcMonPanel::getFlashlists() {
    string flashlistName = "urn:xdaq-flashlist:rpct_worker_monitoring";

    CellXhannelMonitor* xMon = dynamic_cast<CellXhannelMonitor*>(getContext()->getXhannel("MON"));
    CellXhannelRequestMonitor* req =
        dynamic_cast<CellXhannelRequestMonitor*>(xMon->createRequest());

        try {
            req->doRetrieveCollection(flashlistName);
        } catch (xcept::Exception& e) {
            std::ostringstream msg;
            msg << "Can not build the request for Xhannel MON";
            XCEPT_RETHROW(CellException,msg.str(),e);
        }

        try {
            xMon->send(req);
        } catch (xcept::Exception& e) {
            std::ostringstream msg;
            msg << "Error Sending message to Xhannel MON";
            XCEPT_RETHROW(CellException,msg.str(),e);
        }

        try {
            flashlistData_ = req->retrieveCollectionReply();
        } catch(xcept::Exception& e) {
            xMon->removeRequest(req);

            std::ostringstream msg;
            msg << "Error Parsing the reply of xmas::las - " << e.what();
            LOG4CPLUS_ERROR(getLogger(), msg.str());
            flashlists_.clear();
            XCEPT_RETHROW(CellException,msg.str(),e);
        }

        xMon->removeRequest(req);

        // scan flashlistData_: put the newest value per crate
        flashlists_.clear();

	//        vector<string> columns = flashlistData_->getColumns();
        unsigned int row = 0;
        for (xdata::TableIterator i = flashlistData_->begin(); i != flashlistData_->end(); ++i, ++row) {

            string partitionName = i->getField("partitionName")->toString();
            if (flashlists_.find(partitionName) == flashlists_.end()) {
                flashlists_[partitionName] = row;
            }
        }

}



}
