#include "rpct/ts/panels/LBPanel.h"

#include "log4cplus/logger.h"
#include "log4cplus/helpers/sleep.h"
#include "rpct/xdaqutils/LogUtils.h"

#include <sstream>
#include <vector>
#include <string>
#include <utility>

#include "xcept/tools.h"

#include "xdata/Vector.h"
#include "xdata/String.h"

#include "ajax/PlainHtml.h"
#include "ajax/Div.h"

#include "ajax/ResultBox.h"
#include "ajax/toolbox.h"

#include "ajax/PolymerElement.h"


#include "ts/framework/CellAbstract.h"

#include "rpct/ts/common/ajaxtools.h"

#include "rpct/ts/cell/CellContext.h"

#include "ts/exception/CellException.h"
#include "ts/framework/CellAbstract.h"
#include "ts/framework/CellXhannelTB.h"
#include "ts/framework/CellXhannelRequestTB.h"
#include "ts/framework/CellXhannelXdaqSimple.h"
#include "ts/framework/CellXhannelMonitor.h"
#include "ts/framework/CellXhannelRequestMonitor.h"

#include "rpct/xdaqutils/XdaqDbServiceClient.h"
#include "rpct/tools/RunningTimeVal.h"

#include "rpct/ts/worker/panels/LogPanel.h"


namespace rpcttscell {

rpct::LoggerWidget * LBPanel::loggerwidget_ = 0;
rpct::xdaqutils::XdaqDbServiceClient * LBPanel::dbclient_ = 0;

LBPanel::LBPanel(tsframework::CellAbstractContext* context, log4cplus::Logger& logger)
    : tsframework::CellPanel(context, logger)
    , cellcontext_(dynamic_cast<CellContext & >(*context))
    , locked_(true)
{
    logger_ =  log4cplus::Logger::getInstance(logger.getName() + ".LBPanel");
    if (!loggerwidget_)
        {
            loggerwidget_ = new rpct::LoggerWidget(cellcontext_);
            loggerwidget_->setIsOwned(false);
            SharedAppenderPtr saPtr( &(loggerwidget_->getAppender()) );
            logger_.addAppender(saPtr);
        }

    if (!dbclient_)
        dbclient_ = new rpct::xdaqutils::XdaqDbServiceClient(context->getCell());


}

LBPanel::~LBPanel()
{}

void LBPanel::onLockClick(cgicc::Cgicc& cgi, std::ostream& out)
{

}

void LBPanel::initLayout(const std::string & title)
{

}


void LBPanel::layout(cgicc::Cgicc & cgi) {
        remove();

        setEvent("onResetNotReady", ajax::Eventable::OnClick, this, &LBPanel::onResetAllNotReadyCBTTCrxsClick);
        setEvent("onResetAll", ajax::Eventable::OnClick, this, &LBPanel::onResetAllCBTTCrxsClick);

        ajax::PolymerElement * root = new ajax::PolymerElement("lb-panel-supervisor");
        root->set("title", "MM test; LB Panel for " + cellcontext_.getCell()->getName());
        rpcttspanels::LogPanel* logPanel = new rpcttspanels::LogPanel( getContext(), getLogger() );
        root->add(logPanel);
        
        add(root);
}



void LBPanel::onResetAllNotReadyCBTTCrxsClick(cgicc::Cgicc& cgi, std::ostream& out)
{
    LOG4CPLUS_INFO(logger_, "Called onResetAllNotReadyCBTTCrxsClick.");
    sendLBCommands("ResetAllNotReadyCBTTCrxs", "", 10);
}

void LBPanel::onResetAllCBTTCrxsClick(cgicc::Cgicc& cgi, std::ostream& out)
{
    LOG4CPLUS_INFO(logger_, "Called onResetAllCBTTCrxsClick.");
	sendLBCommands("ResetAllCBTTCrxs", "", 10);
	//sendLBCommands("ResetAllNotReadyCBTTCrxs", "", 10);
}


void LBPanel::sendLBCommands(const std::string & command
                               , const std::string & key
                               , unsigned int timeout)
{
    std::map<std::string,tsframework::CellXhannelRequestCell*> requests;
    std::map<std::string,tsframework::CellXhannelCell*> xhannels = cellcontext_.getLBWorkers();

    std::map<std::string,tsframework::CellXhannelCell*>::iterator icEnd = xhannels.end();

    LOG4CPLUS_INFO(logger_, "start sending commands " << command);

    for(std::map<std::string,tsframework::CellXhannelCell*>::iterator ic = xhannels.begin()
            ; ic != icEnd
            ; ++ic)
        {
            try {
                requests[ic->first] = sendCommand(command, ic->second, ic->first, key);
            } catch (xcept::Exception & e) {
                requests.erase(ic->first);
                std::ostringstream msg;
                msg << "---- Worker '" << ic->first << "' ERROR, please check the corresponding worker: ";
                msg << xcept::stdformat_exception_history(e) << std::endl;
                LOG4CPLUS_ERROR(logger_,msg.str());
            }
        }

    rpct::tools::RunningTimeVal rtv(rpct::tools::RunningTimeVal::countdown_, timeout);
    std::map<std::string,tsframework::CellXhannelRequestCell*>::iterator irEnd = requests.end();
    for(std::map<std::string,tsframework::CellXhannelRequestCell*>::iterator ir = requests.begin()
            ; ir != irEnd
            ; ++ir) {
        try {
            receiveReply(ir->first, xhannels[ir->first], ir->second, rtv.getTimeVal().tv_sec);
        } catch (xcept::Exception & e) {
            std::ostringstream msg;
            msg << "---- Worker '" << ir->first << "' ERROR, please check the corresponding worker: ";
            msg << xcept::stdformat_exception_history(e) << std::endl;
            LOG4CPLUS_ERROR(logger_, msg.str());
        }
    }
}

tsframework::CellXhannelRequestCell * LBPanel::sendCommand(const std::string & command
                                                            , tsframework::CellXhannelCell* x
                                                            , const std::string & name
                                                            , const std::string & key)
{
    tsframework::CellXhannelRequestCell * req
        = dynamic_cast<tsframework::CellXhannelRequestCell*>(x->createRequest());
    std::map<std::string,xdata::Serializable *> params;
    xdata::String xkey(key);
    params["key"] = &xkey;

    req->doCommand("sid", true, command, params);

    try {
        x->send(req);
    } catch (xcept::Exception& e) {
        x->removeRequest(req);
        XCEPT_RETHROW(tsexception::CellException,"Could not send command '" + command + "' to xhannel '" + name + "'",e);
    }

    return req;
}

void LBPanel::receiveReply(const std::string & xhannel
                            , tsframework::CellXhannelCell * x
                            , tsframework::CellXhannelRequestCell * req
                            , unsigned int timeout)
{
    int count = 0;
    int sleeptime = 100;
    int maxcount = (int)((float)(timeout * 1000) / (float)(sleeptime));
    LOG4CPLUS_INFO(logger_, "waiting for reply from " << xhannel << " for at most another " << timeout << " seconds.");

    while (!req->hasResponse() && count < maxcount)
        {
        log4cplus::helpers::sleepmillis(sleeptime);
            ++count;
        }

    if (!req->hasResponse())
        {
            x->removeRequest(req);
            std::ostringstream ostr;
            ostr << "Reply to '" << xhannel << "' has timed out.";
            XCEPT_RAISE(tsexception::CellException, ostr.str().c_str());
        }

    xdata::Serializable * s = req->commandReply();
    if (s)
        {
            LOG4CPLUS_INFO(logger_, xhannel + ": " + s->toString());
        }
    else
        {
            LOG4CPLUS_INFO(logger_, xhannel + ": Nothing returned...");
        }
    delete s;

    tsframework::CellWarning warning = req->getWarning();
    x->removeRequest(req);

    if (warning.getLevel() >= tsframework::CellWarning::ERROR)
        XCEPT_RAISE(tsexception::CellException, "Error while executing command on '" + xhannel + "': " + warning.getMessage());
}

} // namespace rpcttscell
