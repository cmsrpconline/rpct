/*************************************************************************
 * RPCT Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: Krzysztof Niemkiewicz               				         *
 *************************************************************************/
#include "rpct/ts/panels/RpctLBHistogramPanel.h"
#include "rpct/xdaqutils/LogUtils.h"

#include "ts/exception/CellException.h"
#include "ts/framework/CellAbstract.h"
#include "ts/framework/CellXhannelTB.h"
#include "ts/framework/CellXhannelRequestTB.h"
#include "ts/framework/CellXhannelXdaqSimple.h"
#include "ts/framework/CellXhannelMonitor.h"
#include "ts/framework/CellXhannelRequestMonitor.h"

#include "xdata/TableIterator.h"

#include "ajax/PlainHtml.h"


#include <iostream>

#define CONSTANT_WIDTH 0

using namespace std;
using namespace tsframework;
using namespace tsexception;

namespace rpcttspanels {

RpctLBHistogramPanel::RpctLBHistogramPanel(CellAbstractContext* context, log4cplus::Logger& logger) :
    CellPanel(context, logger) {

	logger_ =  log4cplus::Logger::getInstance(getLogger().getName() + ".RpctLBHistogramPanel");
    LOG4CPLUS_INFO(getLogger(), "RpctLBHistogramPanel constructor");
    crates = new vector<pair<string, string>* >();
    crates->push_back(new pair<string,string>("RBn2_near","pcrpct03.cms:1962"));
    crates->push_back(new pair<string,string>("RBn2_far", "pcrpct03.cms:1963"));
    crates->push_back(new pair<string,string>("RBn1_near","pcrpct03.cms:1960"));
    crates->push_back(new pair<string,string>("RBn1_far", "pcrpct03.cms:1961"));
    crates->push_back(new pair<string,string>("RB0_near", "pcrpct03.cms:1954"));
    crates->push_back(new pair<string,string>("RB0_far",  "pcrpct03.cms:1955"));
    crates->push_back(new pair<string,string>("RBp1_near","pcrpct03.cms:1950"));
    crates->push_back(new pair<string,string>("RBp1_far", "pcrpct03.cms:1953"));
    crates->push_back(new pair<string,string>("RBp2_near","pcrpct03.cms:1951"));
    crates->push_back(new pair<string,string>("RBp2_far", "pcrpct03.cms:1952"));
    crates->push_back(new pair<string,string>("YEN1_near","pcrpct03.cms:1942"));
    crates->push_back(new pair<string,string>("YEN1_far", "pcrpct03.cms:1943"));
    crates->push_back(new pair<string,string>("YEN3_near","pcrpct03.cms:1944"));
    crates->push_back(new pair<string,string>("YEN3_far", "pcrpct03.cms:1945"));
    crates->push_back(new pair<string,string>("YEP1_near","pcrpct03.cms:1964"));
    crates->push_back(new pair<string,string>("YEP1_far", "pcrpct03.cms:1965"));
    crates->push_back(new pair<string,string>("YEP3_near","pcrpct03.cms:1940"));
    crates->push_back(new pair<string,string>("YEP3_far","pcrpct03.cms:1941"));

}

RpctLBHistogramPanel::~RpctLBHistogramPanel() {
    LOG4CPLUS_INFO(getLogger(), "RpctLBHistogramPanel destructor");
    for (vector<pair<string, string>* >::iterator cratei = crates->begin(); cratei != crates->end(); ++cratei) {
    	delete *cratei;
    };
    delete crates;
}

void RpctLBHistogramPanel::layout(cgicc::Cgicc& cgi) {

    LOG4CPLUS_INFO(getLogger(), "layout");
    remove();

    ajax::PlainHtml* title = new ajax::PlainHtml();
	title->getStream()
			  << "<h2 align=\"center\" style=\"font-family:arial; color:grey;\" >RPCT LB Histograms</h2>";
	add(title);

	ajax::Table* table = new ajax::Table();
	table->setShowRules(true);
	table->setShowHeader(true);
	table->addColumn("Crate", ajax::Table::Html);
	table->addColumn("Histo", ajax::Table::Html);

	int row = 0;


	for (vector<pair<string, string>* >::iterator cratei = crates->begin(); cratei != crates->end(); ++cratei) {
		pair<string,string> crate = **cratei;
		ajax::PlainHtml* crateName = new ajax::PlainHtml();
		crateName->getStream() << crate.first;
		table->setWidgetAt("Crate", row, crateName);



        ajax::PlainHtml * image = new ajax::PlainHtml();
        string source = "http://"+crate.second + "/tmp/"+crate.first+"_allLBs.png";
        if (!CONSTANT_WIDTH) {
            image->getStream() << "<img src=\"" + source + " \" >";
        } else {
            image->getStream() << "<img src=\"" + source + " \" width=\"700\">";
        }



		table->setWidgetAt("Histo", row, image);

		row++;
    };

	add(table);
}

}
