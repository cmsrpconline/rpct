/*************************************************************************
 * RPCT Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: Krzysztof Niemkiewicz               				         *
 *************************************************************************/
#include "rpct/ts/panels/RpctSCHistogramPanel.h"
#include "rpct/xdaqutils/LogUtils.h"

#include "ts/exception/CellException.h"
#include "ts/framework/CellAbstract.h"
#include "ts/framework/CellXhannelTB.h"
#include "ts/framework/CellXhannelRequestTB.h"
#include "ts/framework/CellXhannelXdaqSimple.h"
#include "ts/framework/CellXhannelMonitor.h"
#include "ts/framework/CellXhannelRequestMonitor.h"

#include "xdata/TableIterator.h"

#include "ajax/PlainHtml.h"


#include <iostream>

#define CONSTANT_WIDTH 0

using namespace std;
using namespace tsframework;
using namespace tsexception;

namespace rpcttspanels {

RpctSCHistogramPanel::RpctSCHistogramPanel(CellAbstractContext* context, log4cplus::Logger& logger) :
    CellPanel(context, logger) {

	logger_ =  log4cplus::Logger::getInstance(getLogger().getName() + ".RpctSCHistogramPanel");
    LOG4CPLUS_INFO(getLogger(), "RpctSCHistogramPanel constructor");

}

RpctSCHistogramPanel::~RpctSCHistogramPanel() {
    LOG4CPLUS_INFO(getLogger(), "RpctSCHistogramPanel destructor");
}

void RpctSCHistogramPanel::layout(cgicc::Cgicc& cgi) {

    LOG4CPLUS_INFO(getLogger(), "layout");
    remove();

    ajax::PlainHtml* title = new ajax::PlainHtml();
	title->getStream()
			  << "<h2 align=\"center\" style=\"font-family:arial; color:grey;\" >RPCT SC Histograms</h2>";
	add(title);

	ajax::Table* table = new ajax::Table();
	table->setShowRules(true);
	table->setShowHeader(true);
	table->addColumn("Histo", ajax::Table::Html);


	ajax::PlainHtml * image = new ajax::PlainHtml();
	string source = "http://pcrpct03.cms:1972/tmp/SC_";
	if (CONSTANT_WIDTH) {
		image->getStream() << "<img src=\"" + source + "hsRate.png" +" \" >";
	} else {
		image->getStream() << "<img src=\"" + source + "hsRate.png" +" \" width=\"700\">";
	}

	table->setWidgetAt("Histo", 0, image);


	image = new ajax::PlainHtml();
	if (!CONSTANT_WIDTH) {
		image->getStream() << "<img src=\"" + source + "fsRateGraphs.png" +" \" >";
	} else {
		image->getStream() << "<img src=\"" + source + "fsRateGraphs.png" +" \" width=\"700\">";
	}





	table->setWidgetAt("Histo", 1, image);

	add(table);
}

}
