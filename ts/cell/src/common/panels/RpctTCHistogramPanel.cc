/*************************************************************************
 * RPCT Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: Krzysztof Niemkiewicz               				         *
 *************************************************************************/
#include "rpct/ts/panels/RpctTCHistogramPanel.h"
#include "rpct/xdaqutils/LogUtils.h"

#include "ts/exception/CellException.h"
#include "ts/framework/CellAbstract.h"
#include "ts/framework/CellXhannelTB.h"
#include "ts/framework/CellXhannelRequestTB.h"
#include "ts/framework/CellXhannelXdaqSimple.h"
#include "ts/framework/CellXhannelMonitor.h"
#include "ts/framework/CellXhannelRequestMonitor.h"

#include "xdata/TableIterator.h"

#include "ajax/PlainHtml.h"


#include <iostream>

#define CONSTANT_WIDTH 0

using namespace std;
using namespace tsframework;
using namespace tsexception;

namespace rpcttspanels {

RpctTCHistogramPanel::RpctTCHistogramPanel(CellAbstractContext* context, log4cplus::Logger& logger) :
    CellPanel(context, logger) {

	logger_ =  log4cplus::Logger::getInstance(getLogger().getName() + ".RpctTCHistogramPanel");
    LOG4CPLUS_INFO(getLogger(), "RpctTCHistogramPanel constructor");
    crates = new vector<pair<string, string>* >();
    crates->push_back(new pair<string,string>("TC_0","pcrpct01.cms:1973"));
    crates->push_back(new pair<string,string>("TC_1","pcrpct01.cms:1972"));
    crates->push_back(new pair<string,string>("TC_2","pcrpct01.cms:1974"));
    crates->push_back(new pair<string,string>("TC_3","pcrpct02.cms:1974"));
    crates->push_back(new pair<string,string>("TC_4","pcrpct02.cms:1973"));
    crates->push_back(new pair<string,string>("TC_5","pcrpct04.cms:1974"));
    crates->push_back(new pair<string,string>("TC_6","pcrpct05.cms:1972"));
    crates->push_back(new pair<string,string>("TC_7","pcrpct05.cms:1973"));
    crates->push_back(new pair<string,string>("TC_8","pcrpct05.cms:1974"));
    crates->push_back(new pair<string,string>("TC_9","pcrpct04.cms:1972"));
    crates->push_back(new pair<string,string>("TC_10","pcrpct04.cms:1973"));
    crates->push_back(new pair<string,string>("TC_11","pcrpct02.cms:1972"));
}

RpctTCHistogramPanel::~RpctTCHistogramPanel() {
    LOG4CPLUS_INFO(getLogger(), "RpctTCHistogramPanel destructor");
    for (vector<pair<string, string>* >::iterator cratei = crates->begin(); cratei != crates->end(); ++cratei) {
     	delete *cratei;
    };
    delete crates;
}

void RpctTCHistogramPanel::layout(cgicc::Cgicc& cgi) {

    LOG4CPLUS_INFO(getLogger(), "layout");
    remove();

    ajax::PlainHtml* title = new ajax::PlainHtml();
	title->getStream()
			  << "<h2 align=\"center\" style=\"font-family:arial; color:grey;\" >RPCT TC Histograms</h2>";
	add(title);

	ajax::Table* table = new ajax::Table();
	table->setShowRules(true);
	table->setShowHeader(true);
	table->addColumn("Crate", ajax::Table::Html);
	table->addColumn("Histo", ajax::Table::Html);

	int row = 0;


	for (vector<pair<string, string>* >::iterator cratei = crates->begin(); cratei != crates->end(); ++cratei) {
		pair<string,string> crate = **cratei;
		ajax::PlainHtml* crateName = new ajax::PlainHtml();
		crateName->getStream() << crate.first;
		table->setWidgetAt("Crate", row, crateName);



		// add(image);

		
		// <img src="http://"+crate.second + "/tmp/"+crate.first+"_rmbc1.png">
		// ajax::Image* image = new ajax::Image();
		// image->setSource(source);

		

		ajax::PlainHtml * image = new ajax::PlainHtml();
		string source = "http://"+crate.second + "/tmp/"+crate.first+"_rmbc1.png";
		if (CONSTANT_WIDTH) {
			image->getStream() << "<img src=\"" + source + " \" >";
		} else {
			image->getStream() << "<img src=\"" + source + " \" width=\"700\">";
		}


		table->setWidgetAt("Histo", row, image);

		row++;
    };

	add(table);

	table = new ajax::Table();
	table->setShowRules(true);
	table->setShowHeader(true);
	table->addColumn("Crate", ajax::Table::Html);
	table->addColumn("Histo", ajax::Table::Html);
	
	row = 0;
	for (vector<pair<string, string>* >::iterator cratei = crates->begin(); cratei != crates->end(); ++cratei) {
			pair<string,string> crate = **cratei;
			ajax::PlainHtml* crateName = new ajax::PlainHtml();
			crateName->getStream() << crate.first;
			table->setWidgetAt("Crate", row, crateName);



			ajax::PlainHtml * image = new ajax::PlainHtml();
			string source = "http://"+crate.second + "/tmp/"+crate.first+"_gbs1.png";
			if (!CONSTANT_WIDTH) {
				image->getStream() << "<img src=\"" + source + " \" >";
			} else {
				image->getStream() << "<img src=\"" + source + " \" width=\"700\">";
			}




			table->setWidgetAt("Histo", row, image);

			row++;
	 };
	add(table);
}

}
