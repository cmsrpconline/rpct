#include "config/version.h"
#include "rpct/ts/cell/version.h"
#include "ts/framework/version.h"
#include "ts/toolbox/version.h"
#include "ts/ajaxell/version.h"
#include "xcept/version.h"

GETPACKAGEINFO(rpcttscell)

void rpcttscell::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
  CHECKDEPENDENCY(config);
/*  CHECKDEPENDENCY(xcept); see https://savannah.cern.ch/task/?20519
  CHECKDEPENDENCY(tstoolbox);
  CHECKDEPENDENCY(tsframework);
  CHECKDEPENDENCY(tsajaxell);*/
}

std::set<std::string, std::less<std::string> > rpcttscell::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;
	ADDDEPENDENCY(dependencies,config);
/*	ADDDEPENDENCY(dependencies,xcept); see https://savannah.cern.ch/task/?20519
	ADDDEPENDENCY(dependencies,tstoolbox);
	ADDDEPENDENCY(dependencies,tsframework);
	ADDDEPENDENCY(dependencies,tsajaxell);*/
	return dependencies;
}	
