/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Project Manager: Ildfons Magrans de Abril      				         *
 * Authors: Marc Magrans de Abril               				         *
 *************************************************************************/
#include "rpct/ts/cell/CollectorReader.h"

#include "ts/framework/CellXhannelMonitor.h"
#include "ts/framework/CellXhannelRequestMonitor.h"
#include "ts/exception/CellException.h"

#include "xdata/TableIterator.h"

#include "xcept/tools.h"

#include <sstream>

rpcttscell::CollectorReader::CollectorReader(tsframework::CellAbstractContext* context, log4cplus::Logger& logger) 
  :mutex_(toolbox::BSem::FULL, true)
{
  context_ = context;
  logger_ =  log4cplus::Logger::getInstance(getLogger().getName() + ".CollectorReader");
  job_ = toolbox::task::bind (this, &rpcttscell::CollectorReader::refreshJob, "refreshJob");
  workloop_ = toolbox::task::getWorkLoopFactory()->getWorkLoop("CollectorReaderWorkLoop", "waiting");
  if ( !workloop_->isActive() )
    workloop_->activate();
  
  workloop_->submit(job_);
}

rpcttscell::CollectorReader::~CollectorReader()
{
	workloop_->cancel();
	toolbox::task::getWorkLoopFactory()->removeWorkLoop("CollectorReaderWorkLoop", "waiting");
	delete workloop_;
	delete job_;

	delete logger_;
}

log4cplus::Logger& 
rpcttscell::CollectorReader::getLogger()
{
	return *logger_;
}

tsframework::CellAbstractContext* 
rpcttscell::CollectorReader::getContext()
{
	return context_;
}

void rpcttscell::CollectorReader::getData(std::map<std::string,xdata::Table*>& data, const std::string& flashlist)
{
  //Getting the Xhannel
  tsframework::CellXhannelMonitor* mXhannel;
  try
    {
      mXhannel = dynamic_cast<tsframework::CellXhannelMonitor*>(getContext()->getXhannel("MON"));
      if (!mXhannel)
	XCEPT_RAISE(tsexception::CellException,"Xhannel MON is not of type CellXhannelMonitor");
    } catch (xcept::Exception& e)
      {
	std::ostringstream msg;
	msg << "Monitor Xhannel 'MON' not found. Is it defined in the Xhannel List?";
	XCEPT_RETHROW(tsexception::CellException,msg.str(),e);
      }
  
  //Looking for history
  tsframework::CellXhannelRequestMonitor* dataReq = dynamic_cast<tsframework::CellXhannelRequestMonitor*>(mXhannel->createRequest());
  dataReq->doRetrieveExdr(flashlist);
  try
    {
      mXhannel->send(dataReq);
      data.clear();
      data = dataReq->retrieveExdrReply();
      mXhannel->removeRequest(dataReq);
      
    } catch(xcept::Exception& e)
      {
	std::ostringstream msg;
	msg << "Exception caught while requesting collection for flashlist '" << flashlist << "' to Monitor collector";
	XCEPT_RETHROW(tsexception::CellException,msg.str(),e);
      }
}

std::vector<std::string> 
rpcttscell::CollectorReader::getFlashlists()
{
  std::vector<std::string> result;
  
  tsframework::CellXhannelMonitor* mXhannel;
  try
    {
      mXhannel = dynamic_cast<tsframework::CellXhannelMonitor*>(getContext()->getXhannel("MON"));
      if (!mXhannel)
	XCEPT_RAISE(tsexception::CellException,"Xhannel MON is not of type CellXhannelMonitor");
    } catch (xcept::Exception& e)
      {
	std::ostringstream msg;
	msg << "Monitor Xhannel 'MON' not found. Is it defined in the Xhannel List?";
	XCEPT_RETHROW(tsexception::CellException,msg.str(),e);
      }
  
  std::string type;
  tsframework::CellXhannelRequestMonitor* typeReq = dynamic_cast<tsframework::CellXhannelRequestMonitor*>(mXhannel->createRequest());
  typeReq->doRetrieveCatalog();
  try
    {
      mXhannel->send(typeReq);
      result = typeReq->retrieveCatalogReply();
      mXhannel->removeRequest(typeReq);
      
    } catch(xcept::Exception& e)
      {
	mXhannel->removeRequest(typeReq);
	
	std::ostringstream msg;
	msg << "Exception caught while requesting Catalog of flashlists. ";
	XCEPT_RETHROW(tsexception::CellException,msg.str(),e);
      }
	
  return result;
}

void 
rpcttscell::CollectorReader::removeFlashlists()
{
  std::map<std::string,std::map<std::string,xdata::Table*> >::iterator i(flashlists_.begin());
  for(; i != flashlists_.end(); ++i)
    {
      for(std::map<std::string,xdata::Table*>::iterator j(i->second.begin()); j != i->second.end(); j++)
	if (j->second)
	  delete j->second;
      
      i->second.clear();
    }
  flashlists_.clear();
}

void
rpcttscell::CollectorReader::refreshFlashlists()
{
  std::vector<std::string> names(getFlashlists());
  
  for(std::vector<std::string>::iterator i(names.begin()); i != names.end(); ++i)
    {
      //LOG4CPLUS_INFO(getLogger(),"Getting flashlist '" + *i + "' from Monitor Collector");
      std::map<std::string,xdata::Table*> flashlist;
      getData(flashlist,*i);
      flashlists_.insert(std::make_pair(*i,flashlist));
    }
}
void 
rpcttscell::CollectorReader::flashlists2root()
{

}

bool 
rpcttscell::CollectorReader::refreshJob(toolbox::task::WorkLoop* workloop)
{
  mutex_.take();
  
  //LOG4CPLUS_INFO(getLogger(),"Getting flashlists");
  
  try
    {
      removeFlashlists();
      refreshFlashlists();
    } catch (xcept::Exception& e)
      {
	std::ostringstream msg;
	msg << "Can not refresh the Flashlists in the CollectorReader. Exception caught: ";
	msg << xcept::stdformat_exception_history(e);
	
	//LOG4CPLUS_INFO(getLogger(),msg.str());
      }
  
  mutex_.give();
  
  sleepmillis(cycleLength_ms);
  
  return true;
}
