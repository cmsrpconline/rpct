/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: Ildefons & Marc Magrans de Abril               		 *
 *************************************************************************/

#include "rpct/ts/cell/MonitorSource.h"
#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"
#include "rpct/ts/cell/CellContext.h"

//#include "MonitorableItem.h"
#include "xdata/xdata.h"
#include "xdata/Table.h"

#include <vector>
#include <map>
#include <string>

rpcttscell::MonitorSource::MonitorSource(const std::string& infospaceName, rpcttscell::CellContext* context, log4cplus::Logger& logger)
  :tsframework::DataSource(infospaceName,context,logger)
{
  //Monitoring example code
  add<xdata::String>("item1");
  autorefresh("item1",this,&rpcttscell::MonitorSource::refreshItem1);
  
  add<xdata::Integer>("item2");
  autorefresh("item2",this,&rpcttscell::MonitorSource::refreshItem2);  
  xdata::Integer b(0);
  put("item2",b);
  

  add<xdata::Table>("data");
  autorefresh("data",this,&rpcttscell::MonitorSource::refreshTable);

}


//Monitoring example code
void rpcttscell::MonitorSource::refreshItem1(const std::string& name, xdata::String& item)
{
  std::string last = *dynamic_cast<xdata::String*>(get(name));
  LOG4CPLUS_DEBUG(getLogger(),"Last item1 = '" + last + "'");  

  std::ostringstream msg;
  msg << "item 1 random string = " << rand();
  item = msg.str();

  LOG4CPLUS_DEBUG(getLogger(),"New item1 = '" + msg.str() + "'"); 
}

void rpcttscell::MonitorSource::refreshItem2(const std::string& name, xdata::Integer& item)
{
  int actual = item;
  actual += 1;
  item = actual;
  
}

void rpcttscell::MonitorSource::refreshTable(const std::string& name, xdata::Table& item)
{
  srand(time(0));
  item.clear();
  //item.addColumn("data","double");
  item.addColumn("things","string");
  
  for(int i(0); i < 30; ++i) {
    //xdata::Double* serializable = new xdata::Double(rand()/100000);
    //item.setValueAt(i,"data",*serializable);
    std::ostringstream msg;
    msg << rand() << "aaaa";
    xdata::String* serializable = new xdata::String(msg.str());
    item.setValueAt(i,"things",*serializable);
  }
}
