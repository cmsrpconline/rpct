#include "rpct/ts/cell/SynchronizeLinksCommand.h"

#include "rpct/tools/Time.h"
#include "rpct/ts/cell/CellContext.h"

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

#include "ts/framework/CellFSM.h"
#include "ts/framework/CellOperation.h"
#include "ts/framework/CellOperationFactory.h"
#include "ts/framework/CellWarning.h"
#include "ts/framework/CellXhannelCell.h"
#include "ts/framework/CellXhannelRequestCell.h"

namespace rpcttscell {

SynchronizeLinksCommand::SynchronizeLinksCommand(log4cplus::Logger & _logger, tsframework::CellAbstractContext * _context)
    : tsframework::CellCommand(_logger, _context)
    , context_(dynamic_cast<CellContext &>(*_context))
{
    logger_ = log4cplus::Logger::getInstance(_logger.getName() + ".SynchronizeLinksCommand");
}

bool SynchronizeLinksCommand::preCondition()
{
    try {
        tsframework::CellFSM & _fsm = context_.getOperationFactory()->getOperation("Configuration").getFSM();
        if (_fsm.getStateName(_fsm.getCurrentState()) != "configured") {
            getWarning().append("Configuration Operation is not in state \"configured\".  Ignoring SynchronizeLinksCommand.<br />"
                                , tsframework::CellWarning::ERROR);
            return false;
        }
    } catch (tsexception::OperationDoesNotExist const & _error) {
        getWarning().append("Could not get Configuration Operation to verify state. Ignoring SynchronizeLinksCommand.<br />"
                            , tsframework::CellWarning::ERROR);
        return false;
    }
    return true;
}

void SynchronizeLinksCommand::code()
{
    LOG4CPLUS_INFO(logger_, "Executing SynchronizeLinksCommand");

    try {
        rpct::tools::Lock _lock(mutex_, 1, 0);

        std::map<std::string, tsframework::CellXhannelRequestCell *> _requests;

        std::map<std::string, tsframework::CellXhannelCell *> _lb_workers = context_.getLBWorkers(true);
        std::map<std::string, tsframework::CellXhannelCell *>::const_iterator _lb_workers_end = _lb_workers.end();
        for (std::map<std::string, tsframework::CellXhannelCell *>::const_iterator _lb_worker = _lb_workers.begin()
                 ; _lb_worker != _lb_workers_end ; ++_lb_worker) {
            tsframework::CellXhannelRequestCell * _request
                = dynamic_cast<tsframework::CellXhannelRequestCell *>(_lb_worker->second->createRequest());

            try {
                _request->doCommand("sid", true, "SynchronizeLinksCommand", getParamList());
                _lb_worker->second->send(_request);
                _requests.insert(std::pair<std::string
                                 , tsframework::CellXhannelRequestCell * >(_lb_worker->first
                                                                           , _request));
            } catch (xcept::Exception const & _error) {
                _lb_worker->second->removeRequest(_request);
                getWarning().append("Could not send SynchronizeLinksCommand to " + _lb_worker->first + "<br />"
                                    , tsframework::CellWarning::ERROR);
            }
        }

        rpct::tools::Time _sleep(1);
        rpct::tools::Time _end;
        _end += rpct::tools::Time(10);
        std::map<std::string, tsframework::CellXhannelRequestCell *>::const_iterator _request_end = _requests.end();
        for (std::map<std::string, tsframework::CellXhannelRequestCell *>::const_iterator _request = _requests.begin()
                 ; _request != _request_end ; ++_request) {
            try {
                rpct::tools::Time _now;
                while (_now < _end && !_request->second->hasResponse()) {
                    _sleep.sleep();
                    _now.set();
                }

                if (_request->second->hasResponse())
                    getWarning().append(_request->second->getWarning().getMessage() + "<br />"
                                        , _request->second->getWarning().getLevel());
                else
                    getWarning().append(std::string("Timeout while waiting for SynchronizeLinksCommand response for ")
                                        + _request->first + "<br />"
                                        , tsframework::CellWarning::ERROR);
                _lb_workers[_request->first]->removeRequest(_request->second);
            } catch (xcept::Exception const & _error) {
                getWarning().append(std::string("Could not retrieve SynchronizeLinksCommand response for ")
                                    + _request->first + "<br />"
                                    , tsframework::CellWarning::ERROR);
            }
        }
    } catch (int _error) {
        getWarning().append("Could not start SynchronizeLinksCommand, mutex not available<br />"
                            , tsframework::CellWarning::ERROR);
    } catch (xcept::Exception const & _error) {
        getWarning().append(std::string("Unexpected exception while running SynchronizeLinksCommand: ")
                            + _error.what() + "<br />"
                            , tsframework::CellWarning::ERROR);
    } catch (...) {
        getWarning().append("Unknown exception while running SynchronizeLinksCommand<br />"
                            , tsframework::CellWarning::ERROR);
    }
}

} // namespace rpcttscell
