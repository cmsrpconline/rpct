/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Ildefons Magrans de Abril	                         *
 *************************************************************************/

#ifndef _rpct_ts_cell_MTCCIIInterconnectionTest_h_
#define _rpct_ts_cell_MTCCIIInterconnectionTest_h_

#include "ts/framework/CellOperation.h"

#include <string>
namespace rpcttscell{
  class MTCCIIInterconnectionTest: public tsframework::CellOperation  {
  public:
    MTCCIIInterconnectionTest(log4cplus::Logger& log,tsframework::CellAbstractContext* context);
    ~MTCCIIInterconnectionTest();
    
  private:
    void initialization();
    void ending();
    void resetting();
    void init();
      
  private:
    bool c_prepare();
    void f_prepare();
    bool c_start();
    void f_start();
    bool c_analyze();
    void f_analyze();
    
  };
}//ns rpcttscell
#endif
