/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Marc Magrans de Abril	                                 *
 *************************************************************************/

#ifndef _rpct_ts_cell_Configuration_h_
#define _rpct_ts_cell_Configuration_h_

#include "ts/framework/CellOperation.h"
#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellXhannelXdaqSimple.h"
#include "ts/framework/CellXhannelRequestCell.h"
#include "ts/framework/CellXhannelRequestHandler.h"
#include "ts/framework/CellXhannelCell.h"
// #include "ts/itf/ConfigurationBase.h" // for TS2
#include "ts/runcontrol/ConfigurationBase.h" // for TS4

#include "log4cplus/helpers/sleep.h"

#include <string>
#include <boost/shared_ptr.hpp>

#include "log4cplus/logger.h"


namespace rpcttscell {

// class Configuration: public tsitf::ConfigurationBase { // for TS2
class Configuration: public tsruncontrol::ConfigurationBase {  // for TS4
public:
    typedef tsframework::CellXhannelRequestHandler<tsframework::CellXhannelCell> RequestHandler;
    typedef boost::shared_ptr<tsframework::CellXhannelRequestHandler<tsframework::CellXhannelCell> > RequestHandlerPtr;
    const static unsigned int TIMEOUT_SEC;

  Configuration(log4cplus::Logger& log, tsframework::CellAbstractContext*);
  virtual ~Configuration();


protected:
    virtual void resetting();
    
    bool checkColdReset();
    bool checkConfigure();
    bool checkStart();
    bool checkStop();
    bool checkPause();
    bool checkResume();
    void execColdReset();
    void execConfigure();
    void execStart();
    void execStop();
    void execPause();
    void execResume();

 private:
     tsframework::CellXhannelCell* getCellXhannelCell(const std::string& xhannel);
     typedef std::map<std::string, tsframework::CellXhannelRequestCell*> RequestsMap;

     void executeTransitions(std::map<std::string, tsframework::CellXhannelCell*> const & _workers
                             , std::string const & _transition
                             , std::map<std::string, xdata::Serializable *> const & _params
                             , int _timeout);
     void setParameter(tsframework::CellXhannelXdaqSimple* x, const std::string& name,
             const std::string& nameSpace, const std::string& type, const std::string& value);
     void doCommand(tsframework::CellXhannelXdaqSimple* x, const std::string& name,
             const std::string& nameSpace);

      Configuration::RequestHandlerPtr request(std::pair<std::string, tsframework::CellXhannelCell*> const & _worker
                                               , std::string const & _transition
                                               , std::map<std::string, xdata::Serializable *> const & _params);

     std::string reply(const std::string& xhannel, Configuration::RequestHandlerPtr h_ptr, int timeout);

     Configuration::RequestHandlerPtr resetAsynch(const std::string& xhannel);
     //std::string getState(const std::string& xhannel);

     //std::vector<std::string> getWorkers();
};
}//ns rpcttscell
#endif
