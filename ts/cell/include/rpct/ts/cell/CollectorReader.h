/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Project Manager: Ildfons Magrans de Abril      				         *
 * Authors: Marc Magrans de Abril               				         *
 *************************************************************************/

#ifndef _rpct_ts_cell_CellMonitorWidget_h_
#define _rpct_ts_cell_CellMonitorWidget_h_

#include "ts/framework/CellAbstractContext.h"

#include "log4cplus/logger.h"

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/WaitingWorkLoop.h"
#include "toolbox/task/Action.h"
#include "toolbox/lang/Class.h"

#include "xdata/Table.h"

#include "toolbox/BSem.h"

#include <string>
#include <vector>
#include <iostream>
#include <map>
namespace rpcttscell{
class CollectorReader: public toolbox::lang::Class
  {
  public:
    CollectorReader(tsframework::CellAbstractContext* context, log4cplus::Logger& logger);
    ~CollectorReader();
    
  private:
    void getData(std::map<std::string,xdata::Table*>& data, const std::string& flashlist);
    std::vector<std::string> getFlashlists();
    
    void monitorXhannelCall(std::ostream& out);
    std::string removeNamespace(const std::string& item);
    double string2double(const std::string& value);
    
  private:	
    bool refreshJob(toolbox::task::WorkLoop* workloop);
    toolbox::task::ActionSignature* job_;
    toolbox::task::WorkLoop* workloop_;
    mutable toolbox::BSem mutex_;
    static const unsigned int cycleLength_ms = 5000;
    
  private:	
    void removeFlashlists();
    void refreshFlashlists();
    void flashlists2root();
    //!<flashlist name, <time, data> >
    std::map<std::string,std::map<std::string,xdata::Table*> > flashlists_;
    
  private:
    log4cplus::Logger& getLogger();
    tsframework::CellAbstractContext* getContext();
    tsframework::CellAbstractContext* context_;
    log4cplus::Logger* logger_;
    
  };
}//ns rpcttscell
#endif
