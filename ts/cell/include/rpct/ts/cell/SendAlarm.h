#ifndef _subsystem_ts_cell_SendAlarm_h_
#define _subsystem_ts_cell_SendAlarm_h_

#include "ts/framework/CellCommand.h"

//!This command test the alarms
namespace rpcttscell{
  class SendAlarm: public tsframework::CellCommand {
  public:
    
    SendAlarm(log4cplus::Logger& log, tsframework::CellAbstractContext* context);
    virtual void code();

  };
}
#endif
