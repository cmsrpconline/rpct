#ifndef _rpct_ts_cell_SendMail_h_
#define _rpct_ts_cell_SendMail_h_

#include "ts/framework/CellCommand.h"

namespace rpcttscell{
  class SendMail: public tsframework::CellCommand {
  public:
      
    SendMail(log4cplus::Logger& log,tsframework::CellAbstractContext* context);
   
    virtual bool preCondition();
    virtual void code();
  };
}//ns subsystemtscell
#endif
