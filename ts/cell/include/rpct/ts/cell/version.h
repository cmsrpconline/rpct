#ifndef _rpct_ts_cell_version_h_
#define _rpct_ts_cell_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define rpcttscell_VERSION_MAJOR 1
#define rpcttscell_VERSION_MINOR 6
#define rpcttscell_VERSION_PATCH 6


//
// Template macros
//
#define rpcttscell_VERSION_CODE PACKAGE_VERSION_CODE(rpcttscell_VERSION_MAJOR,rpcttscell_VERSION_MINOR,rpcttscell_VERSION_PATCH)
#ifndef rpcttscell_PREVIOUS_VERSIONS
#define rpcttscell_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(rpcttscell_VERSION_MAJOR,rpcttscell_VERSION_MINOR,rpcttscell_VERSION_PATCH)
#else
#define rpcttscell_FULL_VERSION_LIST  rpcttscell_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(rpcttscell_VERSION_MAJOR,rpcttscell_VERSION_MINOR,rpcttscell_VERSION_PATCH)
#endif

namespace rpcttscell
{
        const std::string package  = "rpcttscell";
        const std::string versions = rpcttscell_FULL_VERSION_LIST;
        const std::string description = "";
        const std::string authors = "";
        const std::string summary = "";
        const std::string link = "";
        config::PackageInfo getPackageInfo();
        void checkPackageDependencies() throw (config::PackageInfo::VersionException);
        std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
