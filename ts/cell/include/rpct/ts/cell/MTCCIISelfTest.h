/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: I. Magrans          				         *
 *************************************************************************/
#ifndef _rpct_ts_cell_MTCCIISelfTest_h_
#define _rpct_ts_cell_MTCCIISelfTest_h_

#include "ts/framework/CellOperation.h"

#include <string>

namespace rpcttscell{
  class MTCCIISelfTest: public tsframework::CellOperation  {
  public:
    MTCCIISelfTest(log4cplus::Logger& log, tsframework::CellAbstractContext* context);
    ~MTCCIISelfTest();
      
  private:
    void initialization();
    void ending();
    void resetting();
    void init();
      
  private:
    bool c_prepare();
    void f_prepare();
    bool c_test();
    void f_test();
  };
}//rpcttscell
#endif
