/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: I. Magrans          				         *
 *************************************************************************/

#ifndef _rpct_ts_cell_Cell_h_
#define _rpct_ts_cell_Cell_h_

#include "ts/framework/CellAbstract.h"

#include "rpct/ts/cell/CellContext.h"

#include <string>
namespace rpcttscell {
class Cell: public tsframework::CellAbstract {
public:
    XDAQ_INSTANTIATOR();

    Cell(xdaq::ApplicationStub * s);

    virtual ~Cell();

    //!This method should be filled with addCommand and addOperation that corresponfs to that Cell
    void init();

    std::string getClassName() {
        return "Cell";
    }

    //!Returns the CellContext pointer. CellContext contains the shared objects of the Cell
    CellContext* getContext() {
        CellContext* c(dynamic_cast<CellContext*> (cellContext_));

        if (!c)
            XCEPT_RAISE(tsexception::CellException,"The Cell Context is not of class CellContext");

        return c;
    }

};
}//ns rpcttscell
#endif
