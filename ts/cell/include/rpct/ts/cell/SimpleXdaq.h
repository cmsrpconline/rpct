/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Marc Magrans de Abril                                    *
 *************************************************************************/
#ifndef _rpct_ts_cell_SimpleXdaq_h_
#define _rpct_ts_cell_SimpleXdaq_h_

#include "xdaq/WebApplication.h"

#include "xoap/MessageReference.h"
#include "xdata/xdata.h"
namespace rpcttscell{
class SimpleXdaq: public xdaq::WebApplication  
{
        public:
        
        	XDAQ_INSTANTIATOR();
        
        	SimpleXdaq(xdaq::ApplicationStub * s) throw (xdaq::exception::Exception);
        
        	xoap::MessageReference simpleCommand(xoap::MessageReference msg) throw(xoap::exception::Exception);
        
        private:
        	 xdata::String* myString_;
};
}//ns rpcttscell
#endif
