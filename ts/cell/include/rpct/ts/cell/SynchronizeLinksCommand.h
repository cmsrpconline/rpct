#ifndef rpct_ts_cell_SynchronizeLinksCommand_h
#define rpct_ts_cell_SynchronizeLinksCommand_h

#include "ts/framework/CellCommand.h"

#include "rpct/tools/Mutex.h"

namespace rpcttscell {

class CellContext;

class SynchronizeLinksCommand
    : public tsframework::CellCommand
{
public:
    SynchronizeLinksCommand(log4cplus::Logger & _logger, tsframework::CellAbstractContext * _context);
    bool preCondition();
    void code();

protected:
    rpct::tools::Mutex mutex_;
    CellContext & context_;
};

} // namespace rpcttscell

#endif // rpct_ts_cell_SynchronizeLinksCommand_h
