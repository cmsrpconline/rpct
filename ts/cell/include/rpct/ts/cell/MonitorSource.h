/**************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: Ildefons & Marc Magrans de Abril               		 *
 *************************************************************************/

#ifndef _rpct_ts_cell_MonitorSource_h_
#define _rpct_ts_cell_MonitorSource_h_

#include "ts/framework/DataSource.h"


namespace log4cplus
{
  class Logger;
}

#include "xdata/xdata.h"
#include "xdata/Table.h"

#include <string>
namespace rpcttscell{
  class CellContext;
  //!Contains the monitorable items for the subsystem cell and the refreshing callbacks
  class MonitorSource: public tsframework::DataSource
    {
    public:
      MonitorSource(const std::string& infospaceName,CellContext* context, log4cplus::Logger& logger);
      
    private:
      void refreshItem1(const std::string& name, xdata::String& item);
      void refreshItem2(const std::string& name, xdata::Integer& item);
      void refreshTable(const std::string& name, xdata::Table& item);
      
    };
}//ns rpcttscell
#endif
