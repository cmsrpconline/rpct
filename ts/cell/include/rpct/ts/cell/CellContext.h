/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors:                                     				         *
 *************************************************************************/

#ifndef _rpct_ts_cell_CellContext_h_
#define _rpct_ts_cell_CellContext_h_

#include <string>

#include "rpct/ts/supervisor/XhannelSelectionCellContext.h"
#include "rpct/ts/supervisor/FebConfigurationCellContext.h"

#include "rpct/ts/worker/LoggerCellContext.h"


namespace tsframework {
class CellXhannelCell;
} // namespace tsframework

namespace rpcttscell{
class MonitorSource;

class CellContext
	: public rpcttsworker::LoggerCellContext
    , public rpct::ts::supervisor::XhannelSelectionCellContext
    , public rpct::ts::supervisor::FebConfigurationCellContext
{
public:
  CellContext(log4cplus::Logger& log , tsframework::CellAbstract* cell);
  ~CellContext();

  std::map<std::string, tsframework::CellXhannelCell*> getActiveWorkers();
  std::map<std::string, tsframework::CellXhannelCell*> getLBWorkers(bool _active_only = false);

  std::map<std::string, tsframework::CellXhannelCell*> getFebWorkers(bool _active_only = false);

  std::string getClassName();

private:
  MonitorSource* datasource_;
};

}//ns rpcttscell
#endif
