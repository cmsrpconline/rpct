/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Project Manager: Ildfons Magrans de Abril      				         *
 * Authors: Marc Magrans de Abril               				         *
 *************************************************************************/

#ifndef _rpct_ts_cell_panels_RpcMonPanel_h_
#define _rpct_ts_cell_panels_RpcMonPanel_h_

#include "ts/framework/CellPanel.h"
#include "ts/framework/CellXhannel.h"

#include "xdata/Table.h"

#include "ajax/Table.h"
#include "ajax/ResultBox.h"
#include "ajax/Timer.h"

#include <map>
#include <string>

namespace rpcttspanels {

class RpcMonPanel : public tsframework::CellPanel {
public:
    RpcMonPanel(tsframework::CellAbstractContext* context, log4cplus::Logger& logger);
    ~RpcMonPanel();

    void layout(cgicc::Cgicc& cgi);
protected:
    virtual std::string getTitle() = 0;
    virtual bool acceptXhannel(std::string& partname, tsframework::CellXhannel* xhannel) = 0;
private:
    static const char* STATUS_STRINGS[];
    const static int STATUS_OK = 0;
    const static int STATUS_WARNING = 1;
    const static int STATUS_ERROR = 2;

    const char* statusToString(int status);

    ajax::Widget* createTablesView(std::map<std::string,xdata::Table*>& tables);
    void addRegister2tables(std::map<std::string,xdata::Table*>& tables, const std::string& table,
            const std::string& primaryColumn, const std::string& primaryValue);

    ajax::Table* createTable(xdata::Table* data);
    void refresh(cgicc::Cgicc& cgi,std::ostream& out);
    void autorefresh(cgicc::Cgicc& cgi,std::ostream& out);
    void getFlashlists();
    void fillResult();

    xdata::Table* flashlistData_;
    typedef std::map<std::string, size_t> TFlashlists;
    TFlashlists flashlists_; // flashlists by partition name, only the freshiest

    bool autoChkValue_;
    ajax::ResultBox* result_;
    ajax::ResultBox* resultTimer_;
    ajax::Timer* timer_;

};


class TCMonPanel : public RpcMonPanel {
 public:
  TCMonPanel(tsframework::CellAbstractContext* context, log4cplus::Logger& logger) : RpcMonPanel(context, logger) {}
 protected:
  virtual std::string getTitle() {
    return "TC and SC Monitoring";
  }

  virtual bool acceptXhannel(std::string& partname, tsframework::CellXhannel* xhannel) {
    return xhannel->getTargetDescriptor()->getClassName() == "rpcttsworker::TBCell";
  }
};

class LBMonPanel : public RpcMonPanel {
 public:
  LBMonPanel(tsframework::CellAbstractContext* context, log4cplus::Logger& logger) : RpcMonPanel(context, logger) {}
 protected:
  virtual std::string getTitle() {
    return "LB Monitoring";
  }

  virtual bool acceptXhannel(std::string& partname, tsframework::CellXhannel* xhannel) {
    return xhannel->getTargetDescriptor()->getClassName() == "rpcttsworker::LBCell";
  }
};

}

#endif
