#ifndef _rpct_ts_cell_panels_LBPanel_h_
#define _rpct_ts_cell_panels_LBPanel_h_

#include <string>
#include <vector>
#include <utility>

#include "ts/framework/CellPanel.h"

#include "ts/framework/CellXhannelCell.h"
#include "ts/framework/CellXhannelRequestCell.h"

#include "rpct/ts/common/LoggerWidget.h"

namespace ajax {
class ResultBox;
class Div;
class Editable;
} // namespace ajax

namespace rpct {
namespace xdaqutils {
class XdaqDbServiceClient;
} // namespace xdaqutils
} // namespace rpct

namespace rpcttscell {

class CellContext;

class LBPanel : public tsframework::CellPanel
{
private:
    void layout(cgicc::Cgicc & cgi);

protected:
    void initLayout(const std::string & title);
    void loadFebConfigKeys();

    void onLockClick(cgicc::Cgicc & cgi, std::ostream & out);

    void onResetAllNotReadyCBTTCrxsClick(cgicc::Cgicc & cgi, std::ostream & out);

    void onResetAllCBTTCrxsClick(cgicc::Cgicc & cgi, std::ostream & out);


    void sendLBCommands(const std::string & command
                         , const std::string & key = ""
                         , unsigned int timeout = 240);

    tsframework::CellXhannelRequestCell * sendCommand(const std::string & command
                                                      , tsframework::CellXhannelCell * x
                                                      , const std::string & name
                                                      , const std::string & key = "");

    void receiveReply(const std::string & xhannel
                      , tsframework::CellXhannelCell * x
                      , tsframework::CellXhannelRequestCell * req
                      , unsigned int timeout = 240);


public:
    LBPanel(tsframework::CellAbstractContext* context, log4cplus::Logger& logger);
    ~LBPanel();

protected:
    rpcttscell::CellContext & cellcontext_;

    static rpct::LoggerWidget * loggerwidget_;

    static rpct::xdaqutils::XdaqDbServiceClient * dbclient_;

    std::vector<std::pair<std::string, std::string> > feb_config_keys_;

    bool locked_;
    ajax::ResultBox * lockbox_;
    ajax::Div * lockdiv_;
    // std::vector<ajax::Editable *> lockables_;
};

} // namespace rpcttscell

#endif // _rpct_ts_cell_panels_LBPanel_h_
