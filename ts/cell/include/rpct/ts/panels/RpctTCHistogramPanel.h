/*************************************************************************
 * RPCT Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: Krzysztof Niemkiewicz               				         *
 *************************************************************************/

#ifndef _rpct_ts_cell_panels_RpctTCHistogramPanel_h_
#define _rpct_ts_cell_panels_RpctTCHistogramPanel_h_

#include "ts/framework/CellPanel.h"

#include "xdata/Table.h"

#include "ajax/Table.h"
#include "ajax/ResultBox.h"

#include <map>
#include <vector>
#include <string>

using namespace std;

namespace rpcttspanels {

class RpctTCHistogramPanel : public tsframework::CellPanel {
public:
	RpctTCHistogramPanel(tsframework::CellAbstractContext* context, log4cplus::Logger& logger);
    ~RpctTCHistogramPanel();

    void layout(cgicc::Cgicc& cgi);

private:
    vector<pair<string, string>* >* crates;


};

}

#endif
