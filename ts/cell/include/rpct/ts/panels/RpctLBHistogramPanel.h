/*************************************************************************
 * RPCT Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: Krzysztof Niemkiewicz               				         *
 *************************************************************************/

#ifndef _rpct_ts_cell_panels_RpctLBHistogramPanel_h_
#define _rpct_ts_cell_panels_RpctLBHistogramPanel_h_

#include "ts/framework/CellPanel.h"

#include "xdata/Table.h"

#include "ajax/Table.h"
#include "ajax/ResultBox.h"

#include <map>
#include <vector>
#include <string>

using namespace std;

namespace rpcttspanels {

class RpctLBHistogramPanel : public tsframework::CellPanel {
public:
	RpctLBHistogramPanel(tsframework::CellAbstractContext* context, log4cplus::Logger& logger);
    ~RpctLBHistogramPanel();

    void layout(cgicc::Cgicc& cgi);

private:
    vector<pair<string, string>* >* crates;


};

}

#endif
