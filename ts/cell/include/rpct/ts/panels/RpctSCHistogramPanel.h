/*************************************************************************
 * RPCT Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: Krzysztof Niemkiewicz               				         *
 *************************************************************************/

#ifndef _rpct_ts_cell_panels_RpctSCHistogramPanel_h_
#define _rpct_ts_cell_panels_RpctSCHistogramPanel_h_

#include "ts/framework/CellPanel.h"

#include "xdata/Table.h"

#include "ajax/Table.h"
#include "ajax/ResultBox.h"

#include <map>
#include <vector>
#include <string>

using namespace std;

namespace rpcttspanels {

class RpctSCHistogramPanel : public tsframework::CellPanel {
public:
	RpctSCHistogramPanel(tsframework::CellAbstractContext* context, log4cplus::Logger& logger);
    ~RpctSCHistogramPanel();

    void layout(cgicc::Cgicc& cgi);

private:

};

}

#endif
