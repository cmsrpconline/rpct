
struct write_arg {
    int lboxId;
    int lboxHalf;
	u_long address;
	u_short data;
};

typedef u_short datablock<>;

struct writeblock_arg {
    int lboxId;
    int lboxHalf;
	u_long address;
	datablock data;
};


struct read_arg {
    int lboxId;
    int lboxHalf;
	u_long address;
};


union read_res switch (int error_no) {
	case 0:
		u_short data;
		
	default:
        void; /* error occurred: nothing else to return */
};

struct readblock_arg {
    int lboxId;
    int lboxHalf;
	u_long address;
	u_long length;
};

union readblock_res switch (int error_no) {
	case 0:
		datablock data;
		
	default:
        void; /* error occurred: nothing else to return */
};

struct lbwrite_arg {
    int lboxId;
    int lboxHalf;
	int lbc;
	u_long address;
	u_short data;
};

struct lbwriteblock_arg {
    int lboxId;
    int lboxHalf;
	int lbc;
	u_long address;
	datablock data;
};

struct lbread_arg {
    int lboxId;
    int lboxHalf;
	int lbc;
	u_long address;
};

struct lbreadblock_arg {
    int lboxId;
    int lboxHalf;
	int lbc;
	u_long address;
	u_long length;
};

union lbreadblock_res switch (int error_no) {
	case 0:
		datablock data;
		
	default:
        void; /* error occurred: nothing else to return */
};

struct programflash_arg {
    int lboxId;
    int lboxHalf;
	u_long lbc_base;
	string file1<>;
	string file2<>;
};

struct programxilinx_arg {
    int lboxId;
    int lboxHalf;
	int lbc;
	string file1<>;
	string file2<>;
};

struct programchip_arg {
    int lboxId;
    int lboxHalf;
  string file<>;
};

struct i2cread_arg {
    int lboxId;
    int lboxHalf;
	int channel;
	u_char address;
};

union i2cread_res switch (int error_no) {
	case 0:
		u_char data;		
		
	default:
        void; /* error occurred: nothing else to return */
};

struct i2cwrite_arg {
    int lboxId;
    int lboxHalf;
	int channel;
	u_char address;
	u_char data;
};
                           
struct piaread_arg {
    int lboxId;
    int lboxHalf;
    int channel;
};

union piaread_res switch (int error_no) {
    case 0:
        u_char data;
                                                                                                                                                   default:
        void; /* error occurred: nothing else to return */
};
                                    
struct piawrite_arg {
    int lboxId;
    int lboxHalf;
    int channel;
    u_char data;
};
         
struct external_reset_arg {
    int lboxId;
    int lboxHalf;
};                           

program CCU_PROG {
	version CCU_VERS {
	    /* direct read/write */
		int				CCU_WRITE(write_arg) = 1;
		int				CCU_WRITE_BLOCK(writeblock_arg) = 2;
		read_res	    CCU_READ(read_arg) = 3;
		readblock_res	CCU_READ_BLOCK(readblock_arg) = 4;
		
		/* lb read/write */
		int				CCU_LBWRITE(lbwrite_arg) = 5;
		int				CCU_LBWRITE_BLOCK(lbwriteblock_arg) = 6;
		read_res	    CCU_LBREAD(lbread_arg) = 7;
		readblock_res	CCU_LBREAD_BLOCK(lbreadblock_arg) = 8;
		
		/* 16 bits read/write */
		int				CCU_WRITE16(write_arg) = 9;
		int				CCU_WRITE16_BLOCK(writeblock_arg) = 10;
		read_res	    CCU_READ16(read_arg) = 11;
		readblock_res	CCU_READ16_BLOCK(readblock_arg) = 12;
		
		/* other commands */
		int           CCU_PROGRAM_FLASH(programflash_arg) = 13;
		int           CCU_PROGRAM_XILINX(programxilinx_arg) = 14;
		i2cread_res   CCU_I2C_READ(i2cread_arg) = 15;
		int           CCU_I2C_WRITE(i2cwrite_arg) = 16;
        piaread_res   CCU_PIA_READ(piaread_arg) = 17;
        int           CCU_PIA_WRITE(piawrite_arg) = 18;
        int           CCU_PROGRAM_CBPC(programchip_arg) = 19;
        int           CCU_PROGRAM_LBC(programchip_arg) = 20;
        int           CCU_EXTERNAL_RESET(external_reset_arg) = 21;
        
        int           CCU_DEBUG(int) = 100;
	} = 3;
} = 0x31230000;
