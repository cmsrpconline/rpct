#include <stdio.h>
#include <log4cplus/configurator.h>
#include "rpct/rpcccu/rpcccu.h"
#include "rpct/lboxaccess/FecManagerImpl.h"
#include "rpct/devices/LinkSystem.h"
#include "rpct/devices/XmlSystemBuilder.h"
#include "rpct/devices/LinkBox.h"
#include "rpct/std/IllegalArgumentException.h"
#include <memory>

using namespace rpct;
using namespace log4cplus;
using namespace std;


Logger logger = Logger::getInstance("rpcccu_server");

// this class is here and log4cplus is configured here and not in the 'main' fuction
// as 'main' is in an auto generated file
class TLinkBoxContext {
private:
    static auto_ptr<System> system_;
public:
    static StdLinkBoxAccess& getStdLinkBoxAccess(int linkBoxId, int linkBoxHalf) {

        if (system_.get() == 0) {
            PropertyConfigurator config(LOG4CPLUS_TEXT("log4cplus.properties"));
            config.configure();
            //setLogLevel(INFO_LOG_LEVEL);
            LOG4CPLUS_INFO(logger, "Configuring FecManagers from system-lbox.xml");
            try {
            	LinkSystem* s = new LinkSystem();
                system_.reset(s);
                XmlSystemBuilder(*s).build("system-lbox.xml");
            }
            catch (TException& e) {
                cerr << e.what() << endl;
                exit(1);
            }
        }
        LinkBox& lbox = dynamic_cast<LinkBox&>(system_->getCrateById(linkBoxId));
        HalfBox* halfBox = lbox.getHalfBox(linkBoxHalf);
        if (halfBox == 0) {
            ostringstream ostr;
            ostr << "HalfBox not found for LinkBoxId = " << linkBoxId << ", half = " << linkBoxHalf;
            LOG4CPLUS_ERROR(logger, ostr.str());
            throw IllegalArgumentException(ostr.str());
        }
        return halfBox->getLBoxAccess();
    }

    static void setLogLevel(LogLevel logLevel) {
        logger.setLogLevel(logLevel);
        Logger::getInstance("FecManagerImpl").setLogLevel(logLevel);
    }

    static void reset() {
        LOG4CPLUS_WARN(logger, "Resetting FEC/CCU access layer ('system' object reset)");
    }
};

auto_ptr<System> TLinkBoxContext::system_;
static int fecErrorCount = 0;

int *
ccu_write_3_svc(write_arg *argp, struct svc_req *rqstp)
{
    static int  result;

    try {
        TLinkBoxContext::getStdLinkBoxAccess(argp->lboxId, argp->lboxHalf).memoryWrite(argp->address, argp->data);
        result = 0;
        fecErrorCount = 0;
    }
    catch (CCUException& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        if (fecErrorCount == 0) {
            fecErrorCount++;
            LOG4CPLUS_ERROR(logger, "Trying once again");
            return ccu_write_3_svc(argp, rqstp);
        }
        else {
            fecErrorCount++;
            TLinkBoxContext::reset();
            LOG4CPLUS_ERROR(logger, "Trying once again after reset");
            return ccu_write_3_svc(argp, rqstp);
        }
    }
    catch (std::exception& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        result = 1;
    }
    catch (...) {
        LOG4CPLUS_ERROR(logger, "Unknown exception");
        result = 1;
    };

    return &result;
}


int *
ccu_write16_3_svc(write_arg *argp, struct svc_req *rqstp)
{
    static int  result;
    try {
        TLinkBoxContext::getStdLinkBoxAccess(argp->lboxId, argp->lboxHalf).memoryWrite16(argp->address, argp->data);
        result = 0;
        fecErrorCount = 0;
    }
    catch (CCUException& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        if (fecErrorCount == 0) {
            fecErrorCount++;
            LOG4CPLUS_ERROR(logger, "Trying once again");
            return ccu_write16_3_svc(argp, rqstp);
        }
        else {
            fecErrorCount++;
            TLinkBoxContext::reset();
            LOG4CPLUS_ERROR(logger, "Trying once again after reset");
            return ccu_write16_3_svc(argp, rqstp);
        }
    }
    catch (std::exception& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        result = 1;
    }
    catch (...) {
        LOG4CPLUS_ERROR(logger, "Unknown exception");
        result = 1;
    };

    return &result;
}

int *
ccu_write_block_3_svc(writeblock_arg *argp, struct svc_req *rqstp)
{
    static int  result;

    unsigned char* cdata = new unsigned char[argp->data.datablock_len];
    for (uint32_t i = 0; i < argp->data.datablock_len; i++) {
        cdata[i] = argp->data.datablock_val[i];
    }
    try {
        TLinkBoxContext::getStdLinkBoxAccess(argp->lboxId, argp->lboxHalf).memoryWriteBlock(argp->address, cdata, argp->data.datablock_len);
        result = 0;
        fecErrorCount = 0;
        delete [] cdata;
    }
    catch (CCUException& e) {
        delete [] cdata;
        LOG4CPLUS_ERROR(logger, e.what());
        if (fecErrorCount == 0) {
            fecErrorCount++;
            LOG4CPLUS_ERROR(logger, "Trying once again");
            return ccu_write_block_3_svc(argp, rqstp);
        }
        else {
            fecErrorCount++;
            TLinkBoxContext::reset();
            LOG4CPLUS_ERROR(logger, "Trying once again after reset");
            return ccu_write_block_3_svc(argp, rqstp);
        }
    }
    catch (std::exception& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        result = 1;
        delete [] cdata;
    }
    catch (...) {
        LOG4CPLUS_ERROR(logger, "Unknown exception");
        result = 1;
        delete [] cdata;
    };

    return &result;
}



int *
ccu_write16_block_3_svc(writeblock_arg *argp, struct svc_req *rqstp)
{
    static int  result;

    try {
        TLinkBoxContext::getStdLinkBoxAccess(argp->lboxId, argp->lboxHalf).memoryWrite16Block(argp->address, argp->data.datablock_val, argp->data.datablock_len);
        result = 0;
        fecErrorCount = 0;
    }
    catch (CCUException& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        if (fecErrorCount == 0) {
            fecErrorCount++;
            LOG4CPLUS_ERROR(logger, "Trying once again");
            return ccu_write16_block_3_svc(argp, rqstp);
        }
        else {
            fecErrorCount++;
            TLinkBoxContext::reset();
            LOG4CPLUS_ERROR(logger, "Trying once again after reset");
            return ccu_write16_block_3_svc(argp, rqstp);
        }
    }
    catch (std::exception& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        result = 1;
    }
    catch (...) {
        LOG4CPLUS_ERROR(logger, "Unknown exception");
        result = 1;
    };

    return &result;
}


read_res *
ccu_read_3_svc(read_arg *argp, struct svc_req *rqstp)
{
    LOG4CPLUS_DEBUG(logger, "ccu_read_3_svc: Enter");
    static read_res  result;
    try {
        result.read_res_u.data = TLinkBoxContext::getStdLinkBoxAccess(argp->lboxId, argp->lboxHalf).memoryRead(argp->address);
        result.error_no = 0;
        fecErrorCount = 0;
        LOG4CPLUS_DEBUG(logger, "ccu_read_3_svc: Read successfully");
    }
    catch (CCUException& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        if (fecErrorCount == 0) {
            fecErrorCount++;
            LOG4CPLUS_ERROR(logger, "Trying once again");
            return ccu_read_3_svc(argp, rqstp);
        }
        else {
            fecErrorCount++;
            TLinkBoxContext::reset();
            LOG4CPLUS_ERROR(logger, "Trying once again after reset");
            return ccu_read_3_svc(argp, rqstp);
        }
    }
    catch (std::exception& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        result.error_no = 1;
    }
    catch (...) {
        LOG4CPLUS_ERROR(logger, "Unknown exception");
        result.error_no = 1;
    };

    LOG4CPLUS_DEBUG(logger, "ccu_read_3_svc: Returning");
    return &result;
}


read_res *
ccu_read16_3_svc(read_arg *argp, struct svc_req *rqstp)
{
    static read_res  result;
    try {
        result.read_res_u.data = TLinkBoxContext::getStdLinkBoxAccess(argp->lboxId, argp->lboxHalf).memoryRead16(argp->address);
        result.error_no = 0;
        fecErrorCount = 0;
    }
    catch (CCUException& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        if (fecErrorCount == 0) {
            fecErrorCount++;
            LOG4CPLUS_ERROR(logger, "Trying once again");
            return ccu_read16_3_svc(argp, rqstp);
        }
        else {
            fecErrorCount++;
            TLinkBoxContext::reset();
            LOG4CPLUS_ERROR(logger, "Trying once again after reset");
            return ccu_read16_3_svc(argp, rqstp);
        }
    }
    catch (std::exception& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        result.error_no = 1;
    }
    catch (...) {
        LOG4CPLUS_ERROR(logger, "Unknown exception");
        result.error_no = 1;
    };

    return &result;
}

readblock_res *
ccu_read_block_3_svc(readblock_arg *argp, struct svc_req *rqstp)
{
    static readblock_res  result;
    const u_long MAX_DATA_LEN = 4096;
    static unsigned short data[MAX_DATA_LEN];
    static unsigned char cdata[MAX_DATA_LEN];
    result.readblock_res_u.data.datablock_val = data;

    if (argp->length > MAX_DATA_LEN) {
        LOG4CPLUS_ERROR(logger, "data length " << argp->length << " exceeds maximum allowed value "
                        << MAX_DATA_LEN);
        result.error_no = 1;
        return &result;
    }

    try {
        TLinkBoxContext::getStdLinkBoxAccess(argp->lboxId, argp->lboxHalf).memoryReadBlock(argp->address, cdata, argp->length);
        for (uint32_t i = 0; i < argp->length; i++) {
            data[i] = cdata[i];
        }
        result.readblock_res_u.data.datablock_len = argp->length;
        fecErrorCount = 0;
    }
    catch (CCUException& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        if (fecErrorCount == 0) {
            fecErrorCount++;
            LOG4CPLUS_ERROR(logger, "Trying once again");
            return ccu_read_block_3_svc(argp, rqstp);
        }
        else {
            fecErrorCount++;
            TLinkBoxContext::reset();
            LOG4CPLUS_ERROR(logger, "Trying once again after reset");
            return ccu_read_block_3_svc(argp, rqstp);
        }
    }
    catch (std::exception& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        result.error_no = 1;
    }
    catch (...) {
        LOG4CPLUS_ERROR(logger, "Unknown exception");
        result.error_no = 1;
    };

    return &result;
}




readblock_res *
ccu_read16_block_3_svc(readblock_arg *argp, struct svc_req *rqstp)
{
    static readblock_res  result;
    const u_long MAX_DATA_LEN = 4096;
    static unsigned short data[MAX_DATA_LEN];
    result.readblock_res_u.data.datablock_val = data;

    if (argp->length > MAX_DATA_LEN) {
        LOG4CPLUS_ERROR(logger, "data length " << argp->length << " exceeds maximum allowed value "
                << MAX_DATA_LEN);
        result.error_no = 1;
        return &result;
    }

    try {
        TLinkBoxContext::getStdLinkBoxAccess(argp->lboxId, argp->lboxHalf).memoryRead16Block(argp->address, data, argp->length);
        result.readblock_res_u.data.datablock_len = argp->length;
        fecErrorCount = 0;
    }
    catch (CCUException& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        if (fecErrorCount == 0) {
            fecErrorCount++;
            LOG4CPLUS_ERROR(logger, "Trying once again");
            return ccu_read16_block_3_svc(argp, rqstp);
        }
        else {
            fecErrorCount++;
            TLinkBoxContext::reset();
            LOG4CPLUS_ERROR(logger, "Trying once again after reset");
            return ccu_read16_block_3_svc(argp, rqstp);
        }
    }
    catch (std::exception& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        result.error_no = 1;
    }
    catch (...) {
        LOG4CPLUS_ERROR(logger, "Unknown exception");
        result.error_no = 1;
    };

    return &result;
}

/***************************************/

int *
ccu_lbwrite_3_svc(lbwrite_arg *argp, struct svc_req *rqstp)
{
    static int  result;
    try {
        TLinkBoxContext::getStdLinkBoxAccess(argp->lboxId, argp->lboxHalf).lbWrite(argp->lbc, argp->address, argp->data);
        result = 0;
        fecErrorCount = 0;
    }
    catch (CCUException& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        if (fecErrorCount == 0) {
            fecErrorCount++;
            LOG4CPLUS_ERROR(logger, "Trying once again");
            return ccu_lbwrite_3_svc(argp, rqstp);
        }
        else {
            fecErrorCount++;
            TLinkBoxContext::reset();
            LOG4CPLUS_ERROR(logger, "Trying once again after reset");
            return ccu_lbwrite_3_svc(argp, rqstp);
        }
    }
    catch (std::exception& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        result = 1;
    }
    catch (...) {
        LOG4CPLUS_ERROR(logger, "Unknown exception");
        result = 1;
    };

    return &result;
}

int *
ccu_lbwrite_block_3_svc(lbwriteblock_arg *argp, struct svc_req *rqstp)
{
    static int  result;

    try {
        TLinkBoxContext::getStdLinkBoxAccess(argp->lboxId, argp->lboxHalf).lbWriteBlock(argp->lbc, argp->address, argp->data.datablock_val, argp->data.datablock_len);
        result = 0;
        fecErrorCount = 0;
    }
    catch (CCUException& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        if (fecErrorCount == 0) {
            fecErrorCount++;
            LOG4CPLUS_ERROR(logger, "Trying once again");
            return ccu_lbwrite_block_3_svc(argp, rqstp);
        }
        else {
            fecErrorCount++;
            TLinkBoxContext::reset();
            LOG4CPLUS_ERROR(logger, "Trying once again after reset");
            return ccu_lbwrite_block_3_svc(argp, rqstp);
        }
    }
    catch (std::exception& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        result = 1;
    }
    catch (...) {
        LOG4CPLUS_ERROR(logger, "Unknown exception");
        result = 1;
    };

    return &result;
}


read_res *
ccu_lbread_3_svc(lbread_arg *argp, struct svc_req *rqstp)
{
    static read_res  result;
    try {
        result.read_res_u.data = TLinkBoxContext::getStdLinkBoxAccess(argp->lboxId, argp->lboxHalf).lbRead(argp->lbc, argp->address);
        result.error_no = 0;
        fecErrorCount = 0;
    }
    catch (CCUException& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        if (fecErrorCount == 0) {
            fecErrorCount++;
            LOG4CPLUS_ERROR(logger, "Trying once again");
            return ccu_lbread_3_svc(argp, rqstp);
        }
        else {
            fecErrorCount++;
            TLinkBoxContext::reset();
            LOG4CPLUS_ERROR(logger, "Trying once again after reset");
            return ccu_lbread_3_svc(argp, rqstp);
        }
    }
    catch (std::exception& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        result.error_no = 1;
    }
    catch (...) {
        LOG4CPLUS_ERROR(logger, "Unknown exception");
        result.error_no = 1;
    };

    return &result;
}


readblock_res *
ccu_lbread_block_3_svc(lbreadblock_arg *argp, struct svc_req *rqstp)
{
    static readblock_res  result;
    const u_long MAX_DATA_LEN = 4096;
    static unsigned short data[MAX_DATA_LEN];
    result.readblock_res_u.data.datablock_val = data;

    if (argp->length > MAX_DATA_LEN) {
        LOG4CPLUS_ERROR(logger, "data length " << argp->length << " exceeds maximum allowed value "
                << MAX_DATA_LEN);
        result.error_no = 1;
        return &result;
    }

    try {
        TLinkBoxContext::getStdLinkBoxAccess(argp->lboxId, argp->lboxHalf).lbReadBlock(argp->lbc, argp->address, data, argp->length);
        result.readblock_res_u.data.datablock_len = argp->length;
        fecErrorCount = 0;
    }
    catch (CCUException& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        if (fecErrorCount == 0) {
            fecErrorCount++;
            LOG4CPLUS_ERROR(logger, "Trying once again");
            return ccu_lbread_block_3_svc(argp, rqstp);
        }
        else {
            fecErrorCount++;
            TLinkBoxContext::reset();
            LOG4CPLUS_ERROR(logger, "Trying once again after reset");
            return ccu_lbread_block_3_svc(argp, rqstp);
        }
    }
    catch (std::exception& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        result.error_no = 1;
    }
    catch (...) {
        LOG4CPLUS_ERROR(logger, "Unknown exception");
        result.error_no = 1;
    };

    return &result;
}


/***************************************/


int * ccu_program_flash_3_svc(programflash_arg *argp, struct svc_req *)
{
    static int  result;

    try {
        throw TException("ccu_program_flash_3_svc not implemented");
        /*TLinkBoxContext::getStdLinkBoxAccess(argp->lboxId, argp->lboxHalf).programFlash(argp->file1);
        result = 0;
        fecErrorCount = 0;*/
    }
    catch (CCUException& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        if (fecErrorCount == 0) {
            fecErrorCount++;
            LOG4CPLUS_ERROR(logger, "Trying once again");
            return ccu_program_flash_3_svc(argp, 0);
        }
        else {
            fecErrorCount++;
            TLinkBoxContext::reset();
            LOG4CPLUS_ERROR(logger, "Trying once again after reset");
            return ccu_program_flash_3_svc(argp, 0);
        }
    }
    catch (std::exception& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        result = 1;
    }
    catch (...) {
        LOG4CPLUS_ERROR(logger, "Unknown exception");
        result = 1;
    };

    return &result;
}


int * ccu_program_xilinx_3_svc(programxilinx_arg *argp, struct svc_req *)
{
    static int  result;
    try {
        throw TException("ccu_program_xilinx_3_svc not implemented");
        /*TLinkBoxContext::getStdLinkBoxAccess(argp->lboxId, argp->lboxHalf).programXilinx(argp->lbc, argp->file1);
        result = 0;
        fecErrorCount = 0;*/
    }
    catch (CCUException& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        if (fecErrorCount == 0) {
            fecErrorCount++;
            LOG4CPLUS_ERROR(logger, "Trying once again");
            return ccu_program_xilinx_3_svc(argp, 0);
        }
        else {
            fecErrorCount++;
            TLinkBoxContext::reset();
            LOG4CPLUS_ERROR(logger, "Trying once again after reset");
            return ccu_program_xilinx_3_svc(argp, 0);
        }
    }
    catch (std::exception& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        result = 1;
    }
    catch (...) {
        LOG4CPLUS_ERROR(logger, "Unknown exception");
        result = 1;
    };

    return &result;
}

int * ccu_program_cbpc_3_svc(programchip_arg *argp, struct svc_req *)
{
    static int  result;
    try {
        throw TException("ccu_program_cbpc_3_svc not implemented");
        /*TLinkBoxContext::getStdLinkBoxAccess(argp->lboxId, argp->lboxHalf).programCbpc(argp->file);
        result = 0;
        fecErrorCount = 0;*/
    }
    catch (CCUException& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        if (fecErrorCount == 0) {
            fecErrorCount++;
            LOG4CPLUS_ERROR(logger, "Trying once again");
            return ccu_program_cbpc_3_svc(argp, 0);
        }
        else {
            fecErrorCount++;
            TLinkBoxContext::reset();
            LOG4CPLUS_ERROR(logger, "Trying once again after reset");
            return ccu_program_cbpc_3_svc(argp, 0);
        }
    }
    catch (std::exception& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        result = 1;
    }
    catch (...) {
        LOG4CPLUS_ERROR(logger, "Unknown exception");
        result = 1;
    };

    return &result;
}

int * ccu_program_lbc_3_svc(programchip_arg *argp, struct svc_req *)
{
    static int  result;
    try {
        throw TException("ccu_program_lbc_3_svc not implemented");
       /* TLinkBoxContext::getStdLinkBoxAccess(argp->lboxId, argp->lboxHalf).programLbc(argp->file);
        result = 0;
        fecErrorCount = 0;*/
    }
    catch (CCUException& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        if (fecErrorCount == 0) {
            fecErrorCount++;
            LOG4CPLUS_ERROR(logger, "Trying once again");
            return ccu_program_lbc_3_svc(argp, 0);
        }
        else {
            fecErrorCount++;
            TLinkBoxContext::reset();
            LOG4CPLUS_ERROR(logger, "Trying once again after reset");
            return ccu_program_lbc_3_svc(argp, 0);
        }
    }
    catch (std::exception& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        result = 1;
    }
    catch (...) {
        LOG4CPLUS_ERROR(logger, "Unknown exception");
        result = 1;
    };

    return &result;
}

i2cread_res * ccu_i2c_read_3_svc(i2cread_arg *argp, struct svc_req *)
{
    static i2cread_res  result;

    /*try {
      result.i2cread_res_u.data = TLinkBoxContext::getStdLinkBoxAccess(argp->lboxId, argp->lboxHalf).I2CRead(argp->channel, argp->address);
      result.error_no = 0;
    }
       catch (CCUException& e) {
     	  cerr << e.getMessage().c_str() << endl;
     	  result.error_no = 1;
       };
    */

    LOG4CPLUS_ERROR(logger, "i2c read not implemented");
    result.error_no = 1;
    return &result;
}

int * ccu_i2c_write_3_svc(i2cwrite_arg *argp, struct svc_req *)
{
    static int  result;

    /*try {
      TLinkBoxContext::getStdLinkBoxAccess(argp->lboxId, argp->lboxHalf).I2CWrite(argp->channel, argp->address, argp->data);
      result = 0;
    }
       catch (CCUException& e) {
     	  cerr << e.getMessage().c_str() << endl;
     	  result = 1;
       };*/

    LOG4CPLUS_ERROR(logger, "i2c write not implemented");
    result = 1;

    return &result;
}

piaread_res * ccu_pia_read_3_svc(piaread_arg *argp, struct svc_req *)
{
    static piaread_res  result;

    try {
        result.piaread_res_u.data = TLinkBoxContext::getStdLinkBoxAccess(argp->lboxId, argp->lboxHalf).piaRead(argp->channel);
        result.error_no = 0;
        fecErrorCount = 0;
    }
    catch (CCUException& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        if (fecErrorCount == 0) {
            fecErrorCount++;
            LOG4CPLUS_ERROR(logger, "Trying once again");
            return ccu_pia_read_3_svc(argp, 0);
        }
        else {
            fecErrorCount++;
            TLinkBoxContext::reset();
            LOG4CPLUS_ERROR(logger, "Trying once again after reset");
            return ccu_pia_read_3_svc(argp, 0);
        }
    }
    catch (std::exception& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        result.error_no = 1;
    }
    catch (...) {
        LOG4CPLUS_ERROR(logger, "Unknown exception");
        result.error_no = 1;
    };

    return &result;
}

int * ccu_pia_write_3_svc(piawrite_arg *argp, struct svc_req *)
{
    static int result;

    try {
        TLinkBoxContext::getStdLinkBoxAccess(argp->lboxId, argp->lboxHalf).piaWrite(argp->channel, argp->data);
        result = 0;
        fecErrorCount = 0;
    }
    catch (CCUException& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        if (fecErrorCount == 0) {
            fecErrorCount++;
            LOG4CPLUS_ERROR(logger, "Trying once again");
            return ccu_pia_write_3_svc(argp, 0);
        }
        else {
            fecErrorCount++;
            TLinkBoxContext::reset();
            LOG4CPLUS_ERROR(logger, "Trying once again after reset");
            return ccu_pia_write_3_svc(argp, 0);
        }
    }
    catch (std::exception& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        result = 1;
    }
    catch (...) {
        LOG4CPLUS_ERROR(logger, "Unknown exception");
        result = 1;
    };

    return &result;
}

int * ccu_debug_3_svc(int * value, struct svc_req *)
{
    static int result = 0;
    TLinkBoxContext::setLogLevel((*value) ? DEBUG_LOG_LEVEL : INFO_LOG_LEVEL);
    return &result;
}


int * ccu_external_reset_3_svc(external_reset_arg *argp, struct svc_req *)
{
    static int result;

    try {
        TLinkBoxContext::getStdLinkBoxAccess(argp->lboxId, argp->lboxHalf).ccuExternalReset();
        result = 0;
        fecErrorCount = 0;
    }
    catch (std::exception& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        result = 1;
    }
    catch (...) {
        LOG4CPLUS_ERROR(logger, "Unknown exception");
        result = 1;
    };

    return &result;
}

