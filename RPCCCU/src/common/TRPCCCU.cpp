#include "TRPCCCU.h"

#ifdef linux
# include <unistd.h>
#endif

using namespace std;

namespace rpct {


TRPCCCU::TRPCCCU(const char* host) : Client(NULL), LogEnabled(false)
{
    Client = clnt_create(host, CCU_PROG, CCU_VERS, "tcp");
    if (Client == NULL) {
          clnt_pcreateerror(host);
          throw CCUException(std::string("Error connecting to the rpc server'") + host + "'." + clnt_spcreateerror(host));
    }
    timeval tv;
    tv.tv_sec = 60 * 10;
    tv.tv_usec = 0;
    clnt_control(Client, CLSET_TIMEOUT, (char*)&tv);
}

TRPCCCU::~TRPCCCU() {
    if (Client != NULL) {
        clnt_destroy(Client);
    }
}

StdLinkBoxAccess& TRPCCCU::GetLinkBoxAccess(int id, int half) {
    LinkBoxAccessKey key(id, half);
    TLinkBoxAccessMap::iterator i = LinkBoxAccessMap.find(key);
    if (i == LinkBoxAccessMap.end()) {
        LinkBoxAccess* lba = new LinkBoxAccess(*this, id, half);
        LinkBoxAccessMap.insert(TLinkBoxAccessMap::value_type(key, lba));
        return *lba;
    }
    return *i->second;
}


void TRPCCCU::SetDebug(bool value) {
    int ivalue = value ? 1 : 0;
    int* result = ccu_debug_3(&ivalue, Client);
    if (result == NULL) {
        clnt_perror (Client, "SetDebug call failed");
        throw CCUException("SetDebug call failed");
    }
}


TRPCCCU::LinkBoxAccess::LinkBoxAccess(TRPCCCU& parent, int linkBoxId, int linkBoxHalf)
: Parent(parent), LinkBoxId(linkBoxId), LinkBoxHalf(linkBoxHalf) {

    for (int i=0; i < I2CChanCount; ++i)
        I2CVector.push_back(PI2C(new TI2C(*this, i)));
    for (int i=0; i < PIAChanCount; i++)
        PIAVector.push_back(PPIA(new TPIA(*this, i)));
}



void TRPCCCU::LinkBoxAccess::resetLocalBus()
{
    memoryWrite(0xfffd, 0x20);
    #ifndef __BORLANDC__
        usleep(100000ul);
    #else
      Sleep(100);
    #endif
    memoryWrite(0xfffd, 0x00);
    #ifndef __BORLANDC__
       usleep(2000000ul);
    #else
      Sleep(2000);
    #endif
    memoryWrite(0xfffd, 0x80);
}

/*void TRPCCCU::ResetTTC()
{
    memoryWrite(0xfffd, 0);
    #ifndef __BORLANDC__
      usleep(2000000ul);
    #else
      Sleep(2000);
    #endif
    memoryWrite(0xfffd, 0x80);
}*/


void TRPCCCU::LinkBoxAccess::ccuExternalReset()
{
    if (Parent.LogEnabled)
      G_Log.out() << "rsto " << endl;
    int* result = ccu_external_reset_3(NULL, Parent.Client);
    if (result == NULL) {
        clnt_perror (Parent.Client, "CCUExternalReset call failed");
        throw CCUException("CCUExternalReset call failed");
    }

    if (*result != 0)
        throw CCUException("CCU error no " + toString(*result));
}



unsigned short TRPCCCU::LinkBoxAccess::memoryRead(uint32_t address)
{
    if (Parent.LogEnabled)
      G_Log.out() << "r " << address << " = " << flush;
    read_arg arg;
    arg.lboxId = LinkBoxId;
    arg.lboxHalf = LinkBoxHalf;
    arg.address = address;
    read_res* result = ccu_read_3(&arg, Parent.Client);
    if (result == NULL) {
        clnt_perror (Parent.Client, "memoryRead call failed");
        throw CCUException("memoryRead call failed");
    }

    if (result->error_no == 0) {
        if (Parent.LogEnabled)
          G_Log.out() << result->read_res_u.data << endl;
        return result->read_res_u.data;
    }
    else
        throw CCUException("CCU error no " + toString(result->error_no));
}

void TRPCCCU::LinkBoxAccess::memoryWrite(uint32_t address, unsigned short data)
{
    if (Parent.LogEnabled)
      G_Log.out() << "w " << address << " " << data << " " << endl;
    write_arg  arg;
    arg.address = address;
    arg.data = data;
    int* result = ccu_write_3(&arg, Parent.Client);
    if (result == NULL) {
        clnt_perror (Parent.Client, "memoryWrite call failed");
        throw CCUException("memoryWrite call failed");
    }

    if (*result != 0)
        throw CCUException("CCU error no " + toString(*result));
}

void TRPCCCU::LinkBoxAccess::memoryWrite16(uint32_t address, unsigned short data)
{
    if (Parent.LogEnabled)
      G_Log.out() << "W " << address << " " << data << " " << endl;
    write_arg  arg;
    arg.address = address;
    arg.data = data;
    int* result = ccu_write16_3(&arg, Parent.Client);
    if (result == NULL) {
        clnt_perror (Parent.Client, "memoryWrite16 call failed");
        throw CCUException("memoryWrite16 call failed");
    }

    if (*result != 0)
        throw CCUException("CCU error no " + toString(*result));
}


void TRPCCCU::LinkBoxAccess::memoryWrite16Block(uint32_t address,
    unsigned short* data, uint32_t count)
{
    writeblock_arg  arg;
    arg.address = address;
    arg.data.datablock_len = count;
    arg.data.datablock_val = data;
    int* result = ccu_write16_block_3(&arg, Parent.Client);
    if (result == NULL) {
        clnt_perror (Parent.Client, "memoryWrite16Block call failed");
        throw CCUException("memoryWrite16Block call failed");
    }

    if (*result != 0)
        throw CCUException("CCU error no " + toString(*result));
}


unsigned short TRPCCCU::LinkBoxAccess::memoryRead16(uint32_t address)
{
    if (Parent.LogEnabled)
      G_Log.out() << "R " << address << " = " << flush;
    read_arg arg;
    arg.lboxId = LinkBoxId;
    arg.lboxHalf = LinkBoxHalf;
    arg.address = address;
    read_res* result = ccu_read16_3(&arg, Parent.Client);
    if (result == NULL) {
        clnt_perror (Parent.Client, "memoryRead16 call failed");
        throw CCUException("memoryRead16 call failed");
    }

    if (result->error_no == 0) {
        if (Parent.LogEnabled)
          G_Log.out() << result->read_res_u.data << std::endl;
        return result->read_res_u.data;
    }
    else
        throw CCUException("CCU error no " + toString(result->error_no));
}

void TRPCCCU::LinkBoxAccess::memoryRead16Block(uint32_t address, unsigned short* data,
                       uint32_t count) {
    readblock_arg arg;
    arg.address = address;
    arg.length = count;

    readblock_res* result = ccu_read16_block_3(&arg, Parent.Client);
    if (result == NULL) {
        clnt_perror (Parent.Client, "memoryRead16Block call failed");
        throw CCUException("memoryRead16Block call failed");
    }

    if (result->error_no == 0) {
        memcpy(data,
               result->readblock_res_u.data.datablock_val,
               count * sizeof(unsigned short));
        return;
    }
    else
        throw CCUException("CCU error no " + toString(result->error_no));
}


void TRPCCCU::LinkBoxAccess::memoryReadBlock(uint32_t address, unsigned char* data,
                              uint32_t count)
{
    readblock_arg arg;
    arg.address = address;
    arg.length = count;

    if (Parent.LogEnabled) {
      G_Log.out() << "rb " << address << " " << count << endl;
    }

    readblock_res* result = ccu_read_block_3(&arg, Parent.Client);
    if (result == NULL) {
        clnt_perror (Parent.Client, "memoryReadBlock call failed");
        throw CCUException("memoryReadBlock call failed");
    }

    if (result->error_no == 0) {
        /*memcpy(data,
           result->readblock_res_u.data.datablock_val,
           count * sizeof(unsigned short));*/
        for (uint32_t i = 0; i < count; i++)    {
            data[i] = result->readblock_res_u.data.datablock_val[i];
        }
        return;
    }
    else
        throw CCUException("CCU error no " + toString(result->error_no));
}




void TRPCCCU::LinkBoxAccess::memoryWriteBlock(uint32_t address,
    unsigned char* data, uint32_t count)
{
    writeblock_arg  arg;
    arg.address = address;
    arg.data.datablock_len = count;

    unsigned short* sdata = new unsigned short[count];
    try {
        for (uint32_t i = 0; i < count; i++) {
            sdata[i] = data[i];
        }

        arg.data.datablock_val = sdata;
        int* result = ccu_write_block_3(&arg, Parent.Client);
        if (result == NULL) {
            clnt_perror (Parent.Client, "memoryWriteBlock call failed");
            throw CCUException("memoryWriteBlock call failed");
        }

        if (*result != 0) {
            throw CCUException("CCU error no " + toString(*result));
        }
        delete [] sdata;
    }
    catch (...) {
        delete [] sdata;
        throw;
    }
}

uint32_t TRPCCCU::LinkBoxAccess::lbRead(int lb, uint32_t address)
{
    if (Parent.LogEnabled)
      G_Log.out() << "LBR " << lb << " " << address << " = " << flush;

    lbread_arg  arg;
    arg.lbc = lb;
    arg.address = address;
    read_res* result = ccu_lbread_3(&arg, Parent.Client);
    if (result == NULL) {
        clnt_perror (Parent.Client, "LBRead call failed");
        throw CCUException("LBRead call failed");
    }

    if (result->error_no == 0) {
        if (Parent.LogEnabled)
          G_Log.out() << result->read_res_u.data << std::endl;
        return result->read_res_u.data;
    }
    else
        throw CCUException("CCU error no " + toString(result->error_no));
}

void TRPCCCU::LinkBoxAccess::lbReadBlock(int lb, uint32_t address,
                     unsigned short* data, uint32_t count) {
    lbreadblock_arg arg;
    arg.lbc = lb;
    arg.address = address;
    arg.length = count;

    readblock_res* result = ccu_lbread_block_3(&arg, Parent.Client);
    if (result == NULL) {
        clnt_perror (Parent.Client, "LBReadBlock call failed");
        throw CCUException("LBReadBlock call failed");
    }

    if (result->error_no == 0) {
        memcpy(data,
               result->readblock_res_u.data.datablock_val,
               count * sizeof(unsigned short));
        return;
    }
    else
        throw CCUException("CCU error no " + toString(result->error_no));
}


void TRPCCCU::LinkBoxAccess::lbWrite(int lb, uint32_t address, uint32_t data)
{
    if (Parent.LogEnabled)
      G_Log.out() << "LBW " << lb << "" << address << " " << data << " " << endl;
    lbwrite_arg  arg;
    arg.lbc = lb;
    arg.address = address;
    arg.data = data;
    int* result = ccu_lbwrite_3(&arg, Parent.Client);
    if (result == NULL) {
        clnt_perror (Parent.Client, "LBWrite call failed");
        throw CCUException("LBWrite call failed");
    }

    if (*result != 0)
        throw CCUException("CCU error no " + toString(*result));
}


void TRPCCCU::LinkBoxAccess::lbWriteBlock(int lb, uint32_t address,
                      unsigned short* data, uint32_t count)
{
    lbwriteblock_arg  arg;
    arg.lbc = lb;
    arg.address = address;
    arg.data.datablock_len = count;
    arg.data.datablock_val = data;
    int* result = ccu_lbwrite_block_3(&arg, Parent.Client);
    if (result == NULL) {
        clnt_perror (Parent.Client, "LBWriteBlock call failed");
        throw CCUException("LBWriteBlock call failed");
    }

    if (*result != 0)
        throw CCUException("CCU error no " + toString(*result));
}

/*
void TRPCCCU::LBWrite(int lb, uint32_t address, uint32_t data)
{
  memoryWrite16(lb * LB_ADDRESS_SPACE_SIZE + address, data);
}

uint32_t TRPCCCU::LBRead(int lb, uint32_t address)
{
  return memoryRead16(lb * LB_ADDRESS_SPACE_SIZE + address);
}

void TRPCCCU::LBWriteBlock(int lb, uint32_t address,
  unsigned short* data, uint32_t count)
{
  memoryWrite16Block(lb * LB_ADDRESS_SPACE_SIZE + address, data, count);
}

void TRPCCCU::LBReadBlock(int lb, uint32_t address,
  unsigned short* data, uint32_t count)
{
  memoryRead16Block(lb * LB_ADDRESS_SPACE_SIZE + address, data, count);
}
*/


void TRPCCCU::LinkBoxAccess::programFlash(const std::string& file)
{
    throw TException("ProgramFlash not implemented");
}


void TRPCCCU::LinkBoxAccess::programFlash(uint32_t lbcbase, const std::string& file)
{
    if (Parent.LogEnabled)
      G_Log.out() << "ProgramFlash(" << lbcbase << ", "
         << file << ")..." << flush;
    programflash_arg  arg;
    arg.lbc_base = lbcbase;
    arg.file1 = (char*)file.c_str();
    arg.file2 = ""; //(char*)file2.c_str();
    int* result = ccu_program_flash_3(&arg, Parent.Client);
    if (result == NULL) {
        clnt_perror (Parent.Client, "ProgramFlash call failed");
        throw CCUException("ProgramFlash call failed");
    }

    if (*result != 0)
        throw CCUException("CCU error no " + toString(*result));

    if (Parent.LogEnabled)
      G_Log.out() << "Done" << endl;
}

void TRPCCCU::LinkBoxAccess::programXilinx(int lbc, const std::string& file)
{
    if (Parent.LogEnabled)
      G_Log.out() << "ProgramXilinx(" << lbc << ", " << file << ")..." << flush;
    programxilinx_arg  arg;
    arg.lbc = lbc;
    arg.file1 = (char*)file.c_str();
    arg.file2 = "";//(char*)file2.c_str();
    int* result = ccu_program_xilinx_3(&arg, Parent.Client);
    if (result == NULL) {
        clnt_perror (Parent.Client, "ProgramXilinx call failed");
        throw CCUException("ProgramXilinx call failed");
    }

    if (*result != 0)
        throw CCUException("CCU error no " + toString(*result));

    if (Parent.LogEnabled)
      G_Log.out() << "Done" << endl;
}

void TRPCCCU::LinkBoxAccess::programCbpc(const std::string& file)
{
    if (Parent.LogEnabled)
      G_Log.out() << "ProgramCBPC(" << file << ")..." << flush;
    programchip_arg  arg;
    arg.file = (char*)file.c_str();
    int* result = ccu_program_cbpc_3(&arg, Parent.Client);
    if (result == NULL) {
        clnt_perror (Parent.Client, "ProgramCBPC call failed");
        throw CCUException("ProgramCBPC call failed");
    }

    if (*result != 0)
        throw CCUException("CCU error no " + toString(*result));

    if (Parent.LogEnabled)
      G_Log.out() << "Done" << endl;
}

void TRPCCCU::LinkBoxAccess::programLbc(const std::string& file)
{
    if (Parent.LogEnabled)
      G_Log.out() << "ProgramLBC(" << file << ")..." << flush;
    programchip_arg  arg;
    arg.file = (char*)file.c_str();
    int* result = ccu_program_lbc_3(&arg, Parent.Client);
    if (result == NULL) {
        clnt_perror (Parent.Client, "ProgramLBC call failed");
        throw CCUException("ProgramLBC call failed");
    }

    if (*result != 0)
      throw CCUException("CCU error no " + toString(*result));

    if (Parent.LogEnabled)
      G_Log.out() << "Done" << endl;
}

unsigned char TRPCCCU::LinkBoxAccess::piaRead(int channel) {
    return GetPIA(channel).Read();
}

void TRPCCCU::LinkBoxAccess::piaWrite(int channel, unsigned char value) {
    GetPIA(channel).Write(value);
}


void TRPCCCU::LinkBoxAccess::loadCb(const std::string& cbpcBin, const std::string& cbicIni, const std::string& lbcBin, const std::string& lbcIni) {
    throw TException("TRPCCCU::loadCb not implemented");
}

void TRPCCCU::LinkBoxAccess::loadKtp(int lb, const std::string& binfile) {
    throw TException("TRPCCCU::loadKtp not implemented");
}

void TRPCCCU::LinkBoxAccess::programFlashCb(const std::string& cbromBin) {
    throw TException("TRPCCCU::programFlashCb not implemented");
}

uint32_t TRPCCCU::LinkBoxAccess::programFlashKtp(int lb, const std::string& lbromBin) {
    throw TException("TRPCCCU::programFlashKtp not implemented");
    return 0ul;
}

void TRPCCCU::LinkBoxAccess::programConfigToFlashKtp(int lb, std::istream *in, uint32_t startaddr) {
    throw TException("TRPCCCU::programConfigToFlashKtp not implemented");
}

void TRPCCCU::LinkBoxAccess::loadFromFlash() {
    throw TException("TRPCCCU::loadFromFlash not implemented");
}

void TRPCCCU::LinkBoxAccess::TI2C::Write8(unsigned char address, unsigned char value)
{
    if (Parent.Parent.LogEnabled)
      G_Log.out() << "iw " << Channel << " " << address << " " << value << " " << endl;
    i2cwrite_arg  arg;
    arg.lboxId = Parent.LinkBoxId;
    arg.lboxHalf = Parent.LinkBoxHalf;
    arg.channel = Channel;
    arg.address = address;
    arg.data = value;
    int* result = ccu_i2c_write_3(&arg, Parent.Parent.Client);
    if (result == NULL) {
        clnt_perror (Parent.Parent.Client, "I2CWrite8 call failed");
        throw CCUException("I2CWrite8 call failed");
    }

    if (*result != 0)
        throw CCUException("CCU error no " + toString(*result));
}

unsigned char TRPCCCU::LinkBoxAccess::TI2C::Read8(unsigned char address, bool ack)
{
    if (Parent.Parent.LogEnabled)
      G_Log.out() << "i2c r " <<Channel << " " << address << " = " << flush;

    if (ack == false) {
        throw CCUException("TRPCCCU::LinkBoxAccess::TI2C::Read8: ack == false not supported");
    }

    i2cread_arg  arg;
    arg.lboxId = Parent.LinkBoxId;
    arg.lboxHalf = Parent.LinkBoxHalf;
    arg.channel = Channel;
    arg.address = address;
    i2cread_res* result = ccu_i2c_read_3(&arg, Parent.Parent.Client);
    if (result == NULL) {
        clnt_perror (Parent.Parent.Client, "I2CRead8 call failed");
        throw CCUException("I2CRead8 call failed");
    }

    if (result->error_no == 0) {
        if (Parent.Parent.LogEnabled)
          G_Log.out() << result->i2cread_res_u.data << endl;
        return result->i2cread_res_u.data;
    }
    else
        throw CCUException("CCU error no " + toString(result->error_no));
}


void TRPCCCU::LinkBoxAccess::TPIA::Write(unsigned char value)
{
    if (Parent.Parent.LogEnabled)
      G_Log.out() << "wp " << Channel << " " << value << " " << endl;
    piawrite_arg  arg;
    arg.lboxId = Parent.LinkBoxId;
    arg.lboxHalf = Parent.LinkBoxHalf;
    arg.channel = Channel;
    arg.data = value;
    int* result = ccu_pia_write_3(&arg, Parent.Parent.Client);
    if (result == NULL) {
        clnt_perror (Parent.Parent.Client, "PIAWrite call failed");
        throw CCUException("PIAWrite call failed");
    }

    if (*result != 0)
        throw CCUException("CCU error no " + toString(*result));
}

unsigned char TRPCCCU::LinkBoxAccess::TPIA::Read()
{
    if (Parent.Parent.LogEnabled)
      G_Log.out() << "rp " << Channel << " = " << flush;
    piaread_arg  arg;
    arg.lboxId = Parent.LinkBoxId;
    arg.lboxHalf = Parent.LinkBoxHalf;
    arg.channel = Channel;
    piaread_res* result = ccu_pia_read_3(&arg, Parent.Parent.Client);
    if (result == NULL) {
        clnt_perror (Parent.Parent.Client, "PIARead call failed");
        throw CCUException("PIARead call failed");
    }

    if (result->error_no == 0) {
        if (Parent.Parent.LogEnabled)
          G_Log.out() << (int)result->piaread_res_u.data << endl;
        return result->piaread_res_u.data;
    }
    else
        throw CCUException("CCU error no " + toString(result->error_no));
}







} // namespace
