#include <iostream>
#include <unistd.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <list>
#include <boost/tokenizer.hpp>
#include "rpct/rpcccu/TRPCCCU.h"
#include "rpct/lboxaccess/lbctrl_pkg.h"

using namespace std;
using namespace boost;
using namespace rpct;

class TCommand {
private:
    typedef char_delimiters_separator<char> TSep;
    tokenizer<TSep> Tokenizer;
    //char_delimiters_separator<char> Sep;
    tokenizer<>::iterator IToken;
protected:
    typedef std::list<TCommand*> TCommands;
    static TCommands Commands;
    static string EmptyString;
    TCommand(string command, string description) :
        Tokenizer(EmptyString, TSep(false, "", " ")) {
        Command = command;
        Description = description;
        Commands.push_back(this);
    }
    string Command;
    string Description;

    static TRPCCCU* CCU;

    virtual void NewLine(string line) {
        Tokenizer.assign(line);
        IToken = Tokenizer.begin();
    }
    virtual string NextToken() {
        return *IToken++;
    }
    virtual uint32_t NextTokenAsULongHex() {
        string token = NextToken();
        istringstream istr(token);
        uint32_t value;
        istr >> hex >> value;
        if (istr.fail())
            throw TException("'" + token + "' is not a hex number");
        return value;
    }
    virtual uint32_t NextTokenAsULong() {
        string token = NextToken();
        istringstream istr(token);
        uint32_t value;
        istr >> dec >> value;
        if (istr.fail())
            throw TException("'" + token + "' is not a dec number");
        return value;
    }
    virtual bool HasMoreTokens() {
        return IToken != Tokenizer.end();
    }

    virtual void ProcessCommand() = 0;
    virtual void ThrowSyntaxError() {
        throw TException(Command + ": invalid syntax.\n" + Description);
    }
public:
    virtual ~TCommand() {
    }

    virtual bool Execute(string line) {
        NewLine(line);
        if (HasMoreTokens() && (NextToken() == Command)) {
            ProcessCommand();
            return true;
        }
        return false;
    }

    virtual string GetDescription() {
        return Description;
    }

    static void SetCCU(TRPCCCU& ccu) {
        CCU = &ccu;
    }

    static bool ExecuteCommand(string line) {
        for (TCommands::iterator iComm = Commands.begin(); iComm != Commands.end(); ++iComm) {
            if ((*iComm)->Execute(line))
                return true;
        }
        return false;
    }

    static string GetDescriptions(string separator) {
        string result;
        for (TCommands::iterator iComm = Commands.begin(); iComm != Commands.end(); ++iComm) {
            result += (*iComm)->GetDescription();
            result += separator;
        }
        return result;
    }

};

string TCommand::EmptyString;
TCommand::TCommands TCommand::Commands;
TRPCCCU* TCommand::CCU;

////////////////////////////////////////////////////////////////////////////////

class TCommandDebug: public TCommand {
protected:
    TCommandDebug() :
        TCommand("dbg", "dbg 0|1 - turn debug off/on") {
    }
    static TCommandDebug Instance;
    virtual void ProcessCommand() {
        if (HasMoreTokens()) {
            CCU->SetDebug(NextToken() == "1");
        } else
            ThrowSyntaxError();
    }
};
TCommandDebug TCommandDebug::Instance;

////////////////////////////////////////////////////////////////////////////////

class TCommandRead: public TCommand {
protected:
    TCommandRead() :
        TCommand("r", "r lboxId address - read byte from the ccu memory channel") {
    }
    static TCommandRead Instance;

    virtual void ProcessCommand() {
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxId = NextTokenAsULong();
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxHalf = NextTokenAsULong();

        if (HasMoreTokens()) {
            cout << CCU->GetLinkBoxAccess(lboxId, lboxHalf).memoryRead(NextTokenAsULongHex()) << endl;
        } else
            ThrowSyntaxError();
    }
};
TCommandRead TCommandRead::Instance;

////////////////////////////////////////////////////////////////////////////////

class TCommandReadBlock: public TCommand {
protected:
    TCommandReadBlock() :
        TCommand("rb", "rb lboxId address count - read block of bytes from the ccu memory channel") {
    }
    static TCommandReadBlock Instance;

    virtual void ProcessCommand() {
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxId = NextTokenAsULong();
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxHalf = NextTokenAsULong();

        if (!HasMoreTokens())
            ThrowSyntaxError();
        unsigned int address = NextTokenAsULongHex();

        if (!HasMoreTokens())
            ThrowSyntaxError();
        unsigned int count = NextTokenAsULongHex();

        unsigned char* buffer = new unsigned char[count];
        try {
            CCU->GetLinkBoxAccess(lboxId, lboxHalf).memoryReadBlock(address, buffer, count);
            for (int i = 0; i < 8; i++) {
                cout << "(" << i << ")" << "\t";
            }
            for (uint32_t i = 0; i < count; i++) {
                cout << ((i % 8) ? '\t' : '\n') << (unsigned int) buffer[i];
            }
            cout << endl;
            delete[] buffer;
        } catch (...) {
            delete[] buffer;
            throw ;
        }
    }
};
TCommandReadBlock TCommandReadBlock::Instance;

////////////////////////////////////////////////////////////////////////////////

class TCommandRead16Block: public TCommand {
protected:
    TCommandRead16Block() :
        TCommand("RB", "RB lboxId address count - read block of 16 bits from the ccu memory channel") {
    }
    static TCommandRead16Block Instance;

    virtual void ProcessCommand() {
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxId = NextTokenAsULong();
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxHalf = NextTokenAsULong();

        if (!HasMoreTokens())
            ThrowSyntaxError();
        unsigned int address = NextTokenAsULongHex();

        if (!HasMoreTokens())
            ThrowSyntaxError();
        unsigned int count = NextTokenAsULongHex();

        unsigned short* buffer = new unsigned short[count];
        try {
            CCU->GetLinkBoxAccess(lboxId, lboxHalf).memoryRead16Block(address, buffer, count);
            for (int i = 0; i < 8; i++)
                cout << "(" << i << ")" << "\t";
            for (int i = 0; i < count; i++) {
                cout << ((i % 8) ? '\t' : '\n') << buffer[i];
            }
            cout << endl;
            delete[] buffer;
        } catch (...) {
            delete[] buffer;
            throw ;
        }
    }
};
TCommandRead16Block TCommandRead16Block::Instance;

////////////////////////////////////////////////////////////////////////////////

class TCommandWrite: public TCommand {
protected:
    TCommandWrite() :
        TCommand("w", "w lboxId address data - write byte to the ccu memory channel") {
    }
    static TCommandWrite Instance;

    virtual void ProcessCommand() {
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxId = NextTokenAsULong();
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxHalf = NextTokenAsULong();

        if (!HasMoreTokens())
            ThrowSyntaxError();
        unsigned int address = NextTokenAsULongHex();

        if (!HasMoreTokens())
            ThrowSyntaxError();
        unsigned int data = NextTokenAsULongHex();
        CCU->GetLinkBoxAccess(lboxId, lboxHalf).memoryWrite(address, data);
    }
};
TCommandWrite TCommandWrite::Instance;

////////////////////////////////////////////////////////////////////////////////

class TCommandWriteBlock: public TCommand {
protected:
    TCommandWriteBlock() :
        TCommand("wb", "wb lboxId address byte1 byte2 ... - write block of bytes to the ccu memory channel") {
    }
    static TCommandWriteBlock Instance;

    virtual void ProcessCommand() {
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxId = NextTokenAsULong();
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxHalf = NextTokenAsULong();

        if (!HasMoreTokens())
            ThrowSyntaxError();
        unsigned int address = NextTokenAsULongHex();

        unsigned char buffer[256];
        int count;
        for (count = 0; HasMoreTokens(); count++) {
            buffer[count] = NextTokenAsULongHex();
        }

        CCU->GetLinkBoxAccess(lboxId, lboxHalf).memoryWriteBlock(address, buffer, count);
    }
};
TCommandWriteBlock TCommandWriteBlock::Instance;

////////////////////////////////////////////////////////////////////////////////

class TCommandWrite16Block: public TCommand {
protected:
    TCommandWrite16Block() :
        TCommand("WB", "WB lboxId address byte1 byte2 ... - write block of 16 bits to the ccu memory channel") {
    }
    static TCommandWrite16Block Instance;

    virtual void ProcessCommand() {
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxId = NextTokenAsULong();
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxHalf = NextTokenAsULong();

        if (!HasMoreTokens())
            ThrowSyntaxError();
        unsigned int address = NextTokenAsULongHex();

        unsigned short buffer[256];
        int count;
        for (count = 0; HasMoreTokens(); count++)
            buffer[count] = NextTokenAsULongHex();

        CCU->GetLinkBoxAccess(lboxId, lboxHalf).memoryWrite16Block(address, buffer, count);
    }
};
TCommandWrite16Block TCommandWrite16Block::Instance;

////////////////////////////////////////////////////////////////////////////////

class TCommandRead16: public TCommand {
protected:
    TCommandRead16() :
        TCommand("R", "R lboxId address - read 16 bit data to the ccu memory channel") {
    }
    static TCommandRead16 Instance;

    virtual void ProcessCommand() {
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxId = NextTokenAsULong();
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxHalf = NextTokenAsULong();

        if (!HasMoreTokens())
            ThrowSyntaxError();
        unsigned int address = NextTokenAsULongHex();

        cout << CCU->GetLinkBoxAccess(lboxId, lboxHalf).memoryRead16(address) << endl;
    }
};
TCommandRead16 TCommandRead16::Instance;

////////////////////////////////////////////////////////////////////////////////

class TCommandWrite16: public TCommand {
protected:
    TCommandWrite16() :
        TCommand("W", "W lboxId address data - write 16 bit data to the ccu memory channel") {
    }
    static TCommandWrite16 Instance;

    virtual void ProcessCommand() {
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxId = NextTokenAsULong();
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxHalf = NextTokenAsULong();

        if (!HasMoreTokens())
            ThrowSyntaxError();
        unsigned int address = NextTokenAsULongHex();

        if (!HasMoreTokens())
            ThrowSyntaxError();
        unsigned int data = NextTokenAsULongHex();
        CCU->GetLinkBoxAccess(lboxId, lboxHalf).memoryWrite16(address, data);
    }
};
TCommandWrite16 TCommandWrite16::Instance;

////////////////////////////////////////////////////////////////////////////////

class TCommandRSTTC: public TCommand {
protected:
    TCommandRSTTC() :
        TCommand("rsttc", "rsttc lboxId - ...") {
    }
    static TCommandRSTTC Instance;

    virtual void ProcessCommand() {
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxId = NextTokenAsULong();
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxHalf = NextTokenAsULong();

        CCU->GetLinkBoxAccess(lboxId, lboxHalf).memoryWrite(0xfffd, 0);
        usleep(2000000ul);
        CCU->GetLinkBoxAccess(lboxId, lboxHalf).memoryWrite(0xfffd, 0x80);
    }
};
TCommandRSTTC TCommandRSTTC::Instance;

////////////////////////////////////////////////////////////////////////////////

class TCommandRSTO: public TCommand {
protected:
    TCommandRSTO() :
        TCommand("rsto", "rsto lboxId - CCU external reset") {
    }
    static TCommandRSTO Instance;

    virtual void ProcessCommand() {
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxId = NextTokenAsULong();
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxHalf = NextTokenAsULong();

        CCU->GetLinkBoxAccess(lboxId, lboxHalf).ccuExternalReset();
    }
};
TCommandRSTO TCommandRSTO::Instance;

////////////////////////////////////////////////////////////////////////////////

class TCommandLBC: public TCommand {
protected:
    TCommandLBC(string command, string description) :
        TCommand(command, description) {
    }
    static uint32_t LBCBase;
    static void SelectLBC(uint32_t lbc) {
        LBCBase = 0x7000 + lbc * 0x100;
    }
};
uint32_t TCommandLBC::LBCBase = 0x7000;

////////////////////////////////////////////////////////////////////////////////
class TCommandSelectLBC: public TCommandLBC {
protected:
    TCommandSelectLBC() :
        TCommandLBC("sl", "sl lboxId number - select LBC") {
    }
    static TCommandSelectLBC Instance;

    virtual void ProcessCommand() {
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxId = NextTokenAsULong();
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxHalf = NextTokenAsULong();

        if (!HasMoreTokens())
            ThrowSyntaxError();
        SelectLBC(NextTokenAsULongHex());
    }
};
TCommandSelectLBC TCommandSelectLBC::Instance;

////////////////////////////////////////////////////////////////////////////////

class TCommandAF: public TCommandLBC {
protected:
    TCommandAF() :
        TCommandLBC("af", "af lboxId address - ...") {
    }
    static TCommandAF Instance;

    virtual void ProcessCommand() {
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxId = NextTokenAsULong();
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxHalf = NextTokenAsULong();

        if (!HasMoreTokens())
            ThrowSyntaxError();
        uint32_t address = NextTokenAsULongHex();
        CCU->GetLinkBoxAccess(lboxId, lboxHalf).memoryWrite16(LBCBase + LBC_FADL, address & 0xffff);
        CCU->GetLinkBoxAccess(lboxId, lboxHalf).memoryWrite16(LBCBase + LBC_FADH, (address >> 16) & 0xf);
    }
};
TCommandAF TCommandAF::Instance;

////////////////////////////////////////////////////////////////////////////////

class TCommandIF: public TCommandLBC {
protected:
    TCommandIF() :
        TCommandLBC("if", "if lboxId - increment flash address") {
    }
    static TCommandIF Instance;

    virtual void ProcessCommand() {
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxId = NextTokenAsULong();
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxHalf = NextTokenAsULong();

        CCU->GetLinkBoxAccess(lboxId, lboxHalf).memoryWrite16(LBCBase + LBC_FCMD, 1 << CFC_INC);
    }
};
TCommandIF TCommandIF::Instance;

////////////////////////////////////////////////////////////////////////////////

class TCommandWF: public TCommandLBC {
protected:
    TCommandWF() :
        TCommandLBC("wf", "wf lboxId data - write data to flash") {
    }
    static TCommandWF Instance;

    virtual void ProcessCommand() {
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxId = NextTokenAsULong();
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxHalf = NextTokenAsULong();

        if (!HasMoreTokens())
            ThrowSyntaxError();
        uint32_t data = NextTokenAsULongHex();
        CCU->GetLinkBoxAccess(lboxId, lboxHalf).memoryWrite16(LBCBase + LBC_FDIN, data & 0xffff);
        CCU->GetLinkBoxAccess(lboxId, lboxHalf).memoryWrite16(LBCBase + LBC_2FDIN, (data >> 16) & 0xffff);
        CCU->GetLinkBoxAccess(lboxId, lboxHalf).memoryWrite16(LBCBase + LBC_FCMD, 1 << CFC_WRITE);
    }
};
TCommandWF TCommandWF::Instance;

////////////////////////////////////////////////////////////////////////////////

class TCommandRF: public TCommandLBC {
protected:
    TCommandRF() :
        TCommandLBC("rf", "rf lboxId - read data from flash") {
    }
    static TCommandRF Instance;

    virtual void ProcessCommand() {
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxId = NextTokenAsULong();
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxHalf = NextTokenAsULong();

        CCU->GetLinkBoxAccess(lboxId, lboxHalf).memoryWrite16(LBCBase + LBC_FCMD, 0);
        CCU->GetLinkBoxAccess(lboxId, lboxHalf).memoryWrite16(LBCBase + LBC_FCMD, 1 << CFC_READ);
        uint32_t uldata = CCU->GetLinkBoxAccess(lboxId, lboxHalf).memoryRead16(LBCBase + LBC_FDOUT);
        uint32_t tmp = CCU->GetLinkBoxAccess(lboxId, lboxHalf).memoryRead16(LBCBase + LBC_2FDOUT);
        uldata |= ((tmp << 16) & 0xffff0000);
        cout << uldata << endl;
    }
};
TCommandRF TCommandRF::Instance;

////////////////////////////////////////////////////////////////////////////////

class TCommandRSTF: public TCommandLBC {
protected:
    TCommandRSTF() :
        TCommandLBC("rstf", "rstf lboxId - reset flash") {
    }
    static TCommandRSTF Instance;

    virtual void ProcessCommand() {
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxId = NextTokenAsULong();
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxHalf = NextTokenAsULong();

        CCU->GetLinkBoxAccess(lboxId, lboxHalf).memoryWrite16(LBCBase + LBC_FCMD, 1 << CFC_RESET);
        CCU->GetLinkBoxAccess(lboxId, lboxHalf).memoryWrite16(LBCBase + LBC_FCMD, 0);
    }
};
TCommandRSTF TCommandRSTF::Instance;

////////////////////////////////////////////////////////////////////////////////

class TCommandEF: public TCommandLBC {
protected:
    TCommandEF() :
        TCommandLBC("ef", "ef lboxId - erase flash") {
    }
    static TCommandEF Instance;

    virtual void ProcessCommand() {
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxId = NextTokenAsULong();
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxHalf = NextTokenAsULong();

        CCU->GetLinkBoxAccess(lboxId, lboxHalf).memoryWrite16(LBCBase + LBC_FCMD, 1 << CFC_CHIP_ERASE);
    }
};
TCommandEF TCommandEF::Instance;

////////////////////////////////////////////////////////////////////////////////

class TCommandRSB: public TCommand {
protected:
    TCommandRSB() :
        TCommand("rsttc", "rsttc lboxId - ...") {
    }
    static TCommandRSB Instance;

    virtual void ProcessCommand() {
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxId = NextTokenAsULong();
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxHalf = NextTokenAsULong();

        CCU->GetLinkBoxAccess(lboxId, lboxHalf).memoryWrite(0xfffd, 0x20);
    }
};
TCommandRSB TCommandRSB::Instance;

////////////////////////////////////////////////////////////////////////////////

class TCommandPF: public TCommand {
protected:
    TCommandPF() :
        TCommand("pf", "pf lboxId file - program flash") {
    }
    static TCommandPF Instance;

    virtual void ProcessCommand() {
        throw TException("Not implemented");
        /*if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxId = NextTokenAsULong();
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxHalf = NextTokenAsULong();

        if (!HasMoreTokens())
            ThrowSyntaxError();
        //CCU->GetLinkBoxAccess(lboxId, lboxHalf).programFlash(0, NextToken());*/
    }
};
TCommandPF TCommandPF::Instance;

////////////////////////////////////////////////////////////////////////////////

class TCommandPX: public TCommand {
protected:
    TCommandPX() :
        TCommand("px", "px lboxId lbc file - program xilinx") {
    }
    static TCommandPX Instance;

    virtual void ProcessCommand() {
        throw TException("Not implemented");
        /*if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxId = NextTokenAsULong();
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxHalf = NextTokenAsULong();

        if (!HasMoreTokens())
            ThrowSyntaxError();
        uint32_t lbc = NextTokenAsULongHex();

        if (!HasMoreTokens())
            ThrowSyntaxError();
        string file = NextToken();

        CCU->GetLinkBoxAccess(lboxId, lboxHalf).programXilinx(lbc, file);*/
    }
};
TCommandPX TCommandPX::Instance;

////////////////////////////////////////////////////////////////////////////////

class TCommandCBPC: public TCommand {
protected:
    TCommandCBPC() :
        TCommand("pcbpc", "pcbpc lboxId file - program cbpc") {
    }
    static TCommandCBPC Instance;

    virtual void ProcessCommand() {
        throw TException("Not implemented");
        /*if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxId = NextTokenAsULong();
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxHalf = NextTokenAsULong();

        if (!HasMoreTokens())
            ThrowSyntaxError();
        CCU->GetLinkBoxAccess(lboxId, lboxHalf).programCbpc(NextToken());*/
    }
};
TCommandCBPC TCommandCBPC::Instance;

////////////////////////////////////////////////////////////////////////////////

class TCommandPLBC: public TCommand {
protected:
    TCommandPLBC() :
        TCommand("plbc", "plbc lboxId file - program lbc") {
    }
    static TCommandPLBC Instance;

    virtual void ProcessCommand() {
        throw TException("Not implemented");
        /*if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxId = NextTokenAsULong();
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxHalf = NextTokenAsULong();

        if (!HasMoreTokens())
            ThrowSyntaxError();
        CCU->GetLinkBoxAccess(lboxId, lboxHalf).programLbc(NextToken());*/
    }
};
TCommandPLBC TCommandPLBC::Instance;

////////////////////////////////////////////////////////////////////////////////
/*
 class TCommandI2CWrite : public TCommand {
 protected:
 TCommandI2CWrite() : TCommand("wi", "wi channel address data - write data to the i2c channel") {}
 static TCommandI2CWrite Instance;

 virtual void ProcessCommand() {
 if (!HasMoreTokens())
 ThrowSyntaxError();
 int lboxId = NextTokenAsULong();

 if (!HasMoreTokens())
 ThrowSyntaxError();
 unsigned int channel = NextTokenAsULongHex();

 if (!HasMoreTokens())
 ThrowSyntaxError();
 unsigned int address = NextTokenAsULongHex();

 if (!HasMoreTokens())
 ThrowSyntaxError();
 unsigned int data = NextTokenAsULongHex();

 CCU->GetLinkBoxAccess(lboxId, lboxHalf).GetI2C(channel).Write8(address, data);
 }
 };
 TCommandI2CWrite TCommandI2CWrite::Instance;

 ////////////////////////////////////////////////////////////////////////////////

 class TCommandI2CRead : public TCommand {
 protected:
 TCommandI2CRead() : TCommand("ri", "ri channel address - read data from the i2c channel") {}
 static TCommandI2CRead Instance;

 virtual void ProcessCommand() {
 if (!HasMoreTokens())
 ThrowSyntaxError();
 int lboxId = NextTokenAsULong();

 if (!HasMoreTokens())
 ThrowSyntaxError();
 unsigned int channel = NextTokenAsULongHex();

 if (!HasMoreTokens())
 ThrowSyntaxError();
 unsigned int address = NextTokenAsULongHex();

 cout << CCU->GetLinkBoxAccess(lboxId, lboxHalf).GetI2C(channel).Read8(address) << endl;
 }
 };
 TCommandI2CRead TCommandI2CRead::Instance;*/

////////////////////////////////////////////////////////////////////////////////

class TCommandPIAWrite: public TCommand {
protected:
    TCommandPIAWrite() :
        TCommand("wp", "wp lboxId channel data - write data to the PIA channel") {
    }
    static TCommandPIAWrite Instance;

    virtual void ProcessCommand() {
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxId = NextTokenAsULong();
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxHalf = NextTokenAsULong();

        if (!HasMoreTokens())
            ThrowSyntaxError();
        unsigned int channel = NextTokenAsULongHex();

        if (!HasMoreTokens())
            ThrowSyntaxError();
        unsigned int data = NextTokenAsULongHex();

        CCU->GetLinkBoxAccess(lboxId, lboxHalf).piaWrite(channel, data);
    }
};
TCommandPIAWrite TCommandPIAWrite::Instance;

////////////////////////////////////////////////////////////////////////////////

class TCommandPIAead: public TCommand {
protected:
    TCommandPIAead() :
        TCommand("rp", "rp lboxId channel address - read data from the PIA channel") {
    }
    static TCommandPIAead Instance;

    virtual void ProcessCommand() {
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxId = NextTokenAsULong();
        if (!HasMoreTokens())
            ThrowSyntaxError();
        int lboxHalf = NextTokenAsULong();

        if (!HasMoreTokens())
            ThrowSyntaxError();
        unsigned int channel = NextTokenAsULongHex();

        cout << (int) CCU->GetLinkBoxAccess(lboxId, lboxHalf).piaRead(channel) << endl;
    }
};
TCommandPIAead TCommandPIAead::Instance;

////////////////////////////////////////////////////////////////////////////////


int main(int argc, char *argv[]) {
    char *host;

    if (argc < 2) {
        printf("usage: %s server_host\n", argv[0]);
        exit(1);
    }
    host = argv[1];

    TRPCCCU ccu(host);

    TCommand::SetCCU(ccu);

    const int MAX_LINE = 256;
    char scrline[MAX_LINE];
    char* line = NULL;
    string command;
    cout << hex;
    unsigned int address, data;
    uint32_t uldata;
    uint32_t lbcbase = 0x7000;
    //uint32_t base = 0;

    ifstream* script = NULL;
    //bool from_script = false;

    //while (cin.getline(line, MAX_LINE)) {
    while ((script != NULL) || (line = readline(">")) != NULL) {
        if (script != NULL) {
            if (!script->getline(scrline, MAX_LINE, '\n')) {
                script->close();
                delete script;
                script = NULL;
                //from_script = false;
                continue;
            }
        }
        //istringstream& str = (script != NULL) ? istringstream(scrline) : istringstream(line);
        istringstream* pstr = (script != NULL) ? &istringstream(scrline) : &istringstream(line);
        //istringstream& str = *pstr;

        (*pstr) >> hex;
        (*pstr) >> command;
        try {
            if (command == "scr") {
                string file;
                (*pstr) >> file;
                script = new ifstream(file.c_str());
                if (script->bad())
                    throw TException("Could not open file '" + file + "'");
                scrline[0] = 0;
            }
            /*else if (command == "w") {
             (*pstr) >> address >> data;
             ccu.MemoryWrite(address, data);
             }
             else if (command == "wb") {
             (*pstr) >> address;
             unsigned short buffer[256];
             int count;
             for (count = 0; pstr->good(); count++)
             (*pstr) >> buffer[count];

             ccu.MemoryWriteBlock(address, buffer, count);
             }
             else if (command == "R") {
             (*pstr) >> address;
             address *= 2;
             data = ccu.MemoryRead(address);
             address += 1;
             unsigned int data1 = ccu.MemoryRead(address);
             data = (data & 0xFF) | ((data1 << 8) & 0xFF00);
             cout << data << endl;
             }
             else if (command == "W") {
             (*pstr) >> address >> data;
             address *= 2;
             unsigned int data1 = (data & 0xFF);
             ccu.MemoryWrite(address, data1);
             address += 1;
             data1 = (data >> 8) & 0xFF;
             ccu.MemoryWrite(address, data1);
             }
             else if (command == "rsttc") {
             ccu.MemoryWrite(0xfffd, 0);
             ccu.MemoryWrite(0xfffd, 0x80);
             }
             else if (command == "sl") {
             (*pstr) >> uldata;
             lbcbase = 0x7000 + uldata * 0x100;
             }
             else if (command == "af") {
             (*pstr) >> uldata;
             ccu.MemoryWrite16(lbcbase + LBC_FADL, uldata & 0xffff);
             ccu.MemoryWrite16(lbcbase + LBC_FADH, (uldata >> 16) & 0xf);
             }
             else if (command == "if") {
             ccu.MemoryWrite16(lbcbase + LBC_FCMD, 1 << CFC_INC);
             }
             else if (command == "wf") {
             (*pstr) >> uldata;
             ccu.MemoryWrite16(lbcbase + LBC_FDIN, uldata & 0xffff);
             ccu.MemoryWrite16(lbcbase + LBC_2FDIN, (uldata >> 16) & 0xffff);
             ccu.MemoryWrite16(lbcbase + LBC_FCMD, 1 << CFC_WRITE);
             }
             else if (command == "rf") {
             ccu.MemoryWrite16(lbcbase + LBC_FCMD, 0);
             ccu.MemoryWrite16(lbcbase + LBC_FCMD, 1 << CFC_READ);
             uldata = ccu.MemoryRead16(lbcbase + LBC_FDOUT);
             uint32_t tmp = ccu.MemoryRead16(lbcbase + LBC_2FDOUT);
             uldata |= ((tmp << 16) & 0xffff);
             cout << uldata << endl;
             }
             else if (command == "rstf") {
             ccu.MemoryWrite16(lbcbase + LBC_FCMD, 1 << CFC_RESET);
             ccu.MemoryWrite16(lbcbase + LBC_FCMD, 0);
             }
             else if (command == "ef") {
             ccu.MemoryWrite16(lbcbase + LBC_FCMD, 1 << CFC_CHIP_ERASE);
             }
             else if (command == "rsb") {
             ccu.MemoryWrite(0xfffd, 0x20);
             }
             else if (command == "pf") {
             string file1;
             (*pstr) >> file1;
             ccu.ProgramFlash(0, file1);
             }
             else if (command == "px") {
             string file;
             int lbc;
             (*pstr) >> lbc >> file;
             if (pstr->bad())
             throw TException("Invalid parameters");
             ccu.ProgramXilinx(lbc, file);
             }
             else if (command == "pcbpc") {
             string file;
             (*pstr) >> file;
             ccu.ProgramCBPC(file);
             }
             else if (command == "plbc") {
             string file;
             (*pstr) >> file;
             ccu.ProgramLBC(file);
             }
             else if (command == "wi") {
             int channel;
             unsigned short address, data;
             (*pstr) >> channel >> address >> data;
             ccu.GetI2C(channel).Write8(address, data);
             }
             else if (command == "ri") {
             int channel;
             unsigned short address;
             (*pstr) >> channel >> address;
             cout << ccu.GetI2C(channel).Read8(address) << endl;
             }
             else if (command == "wp") {
             int channel;
             unsigned short data;
             (*pstr) >> channel >> data;
             ccu.GetPIA(channel).Write(data);
             }
             else if (command == "rp") {
             int channel;
             (*pstr) >> channel;
             cout << (int)ccu.GetPIA(channel).Read() << endl;
             }*/
            else {
                if (!TCommand::ExecuteCommand((script != NULL) ? scrline : line)) {
                    cout << "Unknown command '" << command << "'." << endl;
                    cout << "Available commands:\n" << TCommand::GetDescriptions("\n") << endl;
                }
            }
        } catch (TException& e) {
            cout << "Error: " << e.what() << endl;
        } catch (exception& e) {
            cout << "Error: " << e.what() << endl;
        }

        if ((script != NULL) && *scrline) {
            add_history(scrline);
        } else if (*line) {
            add_history(line);
        }

        if (script == NULL)
            free(line);
    }
}
