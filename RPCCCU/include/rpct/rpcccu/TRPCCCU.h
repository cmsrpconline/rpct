#ifndef TRPCCCU_H
#define TRPCCCU_H

#include <string>
#include <iostream>
#include <sstream>
#include <istream>
#include <vector>
#include <memory>
#include <map>
#include "rpct/lboxaccess/CCUException.h"
#include "rpct/rpcccu/rpcccu.h"
#include "rpct/lboxaccess/StdLinkBoxAccessAdapter.h"
#include "rpct/std/ENotImplemented.h"
#include "rpct/i2c/TI2CInterface.h"

namespace rpct {


class TRPCCCU {
private:
	CLIENT* Client;

    class LinkBoxAccess : public StdLinkBoxAccessAdapter {
    private:

        TRPCCCU& Parent;
        int LinkBoxId;
        int LinkBoxHalf;

        class TI2C : public TI2CInterface {
        private:
          LinkBoxAccess& Parent;
          const int Channel;
        public:
            TI2C(LinkBoxAccess& parent, int channel) : Parent(parent), Channel(channel) {}

            virtual void Write8(unsigned char address, unsigned char value);
            virtual unsigned char Read8(unsigned char address, bool ack = true);
            virtual void WriteADD(unsigned char address, unsigned char value1,
                                    unsigned char value2)
              {
                throw EI2C("TI2C::WriteADD: not implememted");
              }

            virtual void ReadADD(unsigned char address, unsigned char& value1,
                                    unsigned char& value2)
              {
                throw EI2C("TI2C::ReadADD: not implememted");
              }

            virtual void Write(unsigned char address, unsigned char* data, size_t count)
              {
                throw EI2C("TI2C::Write: not implememted");
              }
        };

        friend class TI2C;

        static const int I2CChanCount = 4;
        typedef TI2C* PI2C;
        std::vector<PI2C> I2CVector;


        class TPIA{
        private:
          LinkBoxAccess& Parent;
          const int Channel;
        public:
            TPIA(LinkBoxAccess& parent, int channel) : Parent(parent), Channel(channel) {}

            virtual void Write(unsigned char value);
            virtual unsigned char Read();
        };

        friend class TPIA;
        static const int PIAChanCount = 4;
        typedef TPIA* PPIA;
        std::vector<PPIA> PIAVector;

    public:
        LinkBoxAccess(TRPCCCU& parent, int linkBoxId, int linkBoxHalf);

        virtual void setName(std::string nam) {
        	//name = nam;
        }

        virtual FecManager& getFecManager() {
            throw ENotImplemented("TRPCCCU::getFecManager()");
        }

        virtual unsigned int getCcuAddress() {
            throw ENotImplemented("TRPCCCU::getCcuAddress()");
        }

        void resetLocalBus();

        //void ResetTTC();
        virtual void ccuExternalReset();

        unsigned short memoryRead(uint32_t address);

        void memoryWrite(uint32_t address, unsigned short data);

        void memoryWrite16(uint32_t address, unsigned short data);


        void memoryWrite16Block(uint32_t address, unsigned short* data,
                                uint32_t count);

        void memoryWriteBlock(uint32_t address, unsigned char* data,
                                uint32_t count);

        unsigned short memoryRead16(uint32_t address);

        void memoryRead16Block(uint32_t address, unsigned short* data,
                               uint32_t count);


        void memoryReadBlock(uint32_t address, unsigned char* data,
                               uint32_t count);


        virtual void lbWrite(int lb, uint32_t address, uint32_t data);

        virtual uint32_t lbRead(int lb, uint32_t address);

        virtual void lbWriteBlock(int lb, uint32_t address,
                          unsigned short* data, uint32_t count);

        virtual void lbReadBlock(int lb, uint32_t address,
                         unsigned short* data, uint32_t count);

        virtual void programFlash(const std::string& file);
        virtual void programFlash(uint32_t lbcbase, const std::string& file);

        virtual void programXilinx(int lbc, const std::string& file);

        virtual void programCbpc(const std::string& file);

        virtual void programLbc(const std::string& file);

        TI2CInterface& GetI2C(int channel) {
            return *I2CVector.at(channel);
        }

        TPIA& GetPIA(int channel) {
            return *PIAVector[channel];
        }

        virtual unsigned char piaRead(int channel);

        virtual void piaWrite(int channel, unsigned char value);

        virtual void loadCb(const std::string& cbpcBin, const std::string& cbicIni, const std::string& lbcBin, const std::string& lbcIni);

        virtual void loadKtp(int lb, const std::string& binfile);

        virtual void programFlashCb(const std::string& cbromBin);

        virtual uint32_t programFlashKtp(int lb, const std::string& lbromBin);

        virtual void programConfigToFlashKtp(int lb, std::istream *in, uint32_t startaddr);

        virtual void loadFromFlash();

        virtual bool isTTCHardResetEnabled() {
        	return false;
        }

    };
    friend class LinkBoxAccess;


    struct LinkBoxAccessKey {
        int linkBoxId;
        int linkBoxHalf;

        LinkBoxAccessKey(int id, int half)
        : linkBoxId(id), linkBoxHalf(half){
        }
        bool operator< (const LinkBoxAccessKey& other) const {
            if (linkBoxId < other.linkBoxId) {
                return true;
            }
            if (linkBoxId > other.linkBoxId) {
                return false;
            }
            if (linkBoxHalf < other.linkBoxHalf) {
                return true;
            }
            return false;
        }
        std::string toString() {
            std::ostringstream ostr;
            ostr << "LinkBoxAccessKey(" << linkBoxId << ", "
                << linkBoxHalf << ")";
            return ostr.str();
        }
    };

    typedef std::map<LinkBoxAccessKey, LinkBoxAccess*> TLinkBoxAccessMap;
    TLinkBoxAccessMap LinkBoxAccessMap;

    bool LogEnabled;

public:
    TRPCCCU(const char* host);

    ~TRPCCCU();

    StdLinkBoxAccess& GetLinkBoxAccess(int id, int half);


    bool IsLogEnabled() {
        return LogEnabled;
    }

    void SetLogEnabled(bool value) {
        LogEnabled = value;
    }

    void SetDebug(bool value);
};

}

#endif
