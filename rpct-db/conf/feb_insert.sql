/*
 * Created on 2009-12-04
 * 
 * @author Nikolay Darmenov
 * @version $Id$
 */

set serveroutput on

--- Insert FEB structure

----- Fill BOARD   
----- Fill FEBBOARD
----- Fill CHIP
DECLARE
	/* BOARD */
   	bid BOARD.BOARDID%TYPE;
	bcid constant BOARD.CRATE_CRATEID%TYPE := NULL;
	bname BOARD.NAME%TYPE;
	blabel BOARD.LABEL%TYPE;
	bType constant BOARD.TYPE%TYPE := 'FEBBOARD';
	bPosition constant BOARD.POSITION%TYPE := 0;

	boardidMax BOARD.BOARDID%TYPE;
	
	/* FEBBOARD */
	fflid FEBBOARD.FL_FEBLOCATIONID%TYPE;
	fcbci2c FEBBOARD.CBC_I2CCBCHANNELID%TYPE;
	fi2cnum FEBBOARD.I2CLOCALNUMBER%TYPE;

	/* CHIP */
	chipid CHIP.CHIPID%TYPE;
	chipType constant CHIP.TYPE%TYPE := 'FEB';
	chipPosition CHIP.POSITION%TYPE;
	chipPositionStart constant CHIP.POSITION%TYPE := 0;
	chipPositionsBarrel constant INTEGER := 2;
	chipPositionsEndcap constant INTEGER := 4;
	chipidMax CHIP.CHIPID%TYPE;

	/* */
	cursor c_fl is
		SELECT fl.FebLocationId,
			fl.CBC_I2CCBCHANNELID, fl.I2CLOCALNUMBER,
			cl.chamberlocationname || '/' || fl.feblocaletapartition FlName,
			CASE 
				WHEN cl.BarrelOrEndcap = 'Barrel' THEN chipPositionsBarrel
				WHEN cl.BarrelOrEndcap = 'Endcap' THEN chipPositionsEndcap
			END numChipPositions 
		FROM FebLocation fl, ChamberLocation cl
		WHERE  fl.CL_ChamberLocationId = cl.ChamberLocationId
		AND fl.FebLocationId != -1 /* invalid FL */
		AND cl.ChamberLocationId != -1 /* invalid CL */
		AND rownum < 2
		ORDER BY fl.FebLocationId;
	

BEGIN
	/* BOARD */
	-- Append to the end of BOARD
	select max(boardid) into boardidMax from board;
	dbms_output.put_line('boardidMax: ' || boardidMax);

	-- Append to the end of CHIP
	select max(chipid) into chipidMax from chip;
	dbms_output.put_line('chipidMax: ' || chipidMax);

	bid := boardidMax + 1;
	chipid := chipidMax + 1;
	for fl_rec in c_fl
	loop
		bname := fl_rec.FlName;
		blabel := bname;
		
		INSERT INTO "BOARD"
		(BOARDID, CRATE_CRATEID, TYPE, NAME, LABEL, POSITION)
		VALUES
		(bid, bcid, bType, bname, blabel, bPosition);
		dbms_output.put_line('Added BOARD (' || bid || ', ' || bcid || ', ' || btype || ', ' || bname || ', ' || blabel || ', ' || bposition || ')');

		fflid := fl_rec.FebLocationId;
		fcbci2c := fl_rec.CBC_I2CCBCHANNELID;
		fi2cnum := fl_rec.I2CLOCALNUMBER;
		INSERT INTO "FEBBOARD"
		VALUES (bid, fflid, fcbci2c, fi2cnum);
		dbms_output.put_line('Added FEBBOARD(' || bid || ', ' || fflid || ', ' || fcbci2c || ', ' || fi2cnum || ')');

		/* CHIP */
		for chipPosition in chipPositionStart .. (chipPositionStart + fl_rec.numChipPositions - 1)
		loop
        	INSERT INTO "CHIP"
        	VALUES (chipid, bid, chipType, chipPosition);
        	dbms_output.put_line('Added CHIP (' || chipid || ', ' || bid || ', ' || chipType || ', ' || chipPosition || ')');
			chipid := chipid + 1;
		end loop;
		
		bid := bid + 1;
	end loop;
	
END;
/

----
-- Synchronize the sequencies
----
BEGIN
	syncSeqToId('BOARD', 'S_13_1_BOARD');
	syncSeqToId('CHIP', 'S_11_1_CHIP');
END;
/

