REM
REM Start Cascade on Updates and Deletes for Foreign Keys 
REM

--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_5EEBDCCE
before insert or update on TriggerChipRMBconf
referencing OLD as old NEW as new
for each row when (
	new.TriggerChipRMBStaticConfId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row StaticConfiguration%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM StaticConfiguration WHERE
			StaticConfigurationId = fkVal1
		for update of
			StaticConfigurationId;
begin
	open c1(:new.TriggerChipRMBStaticConfId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_2AE9F89C
after update of StaticConfigurationId on StaticConfiguration
referencing OLD as old NEW as new
for each row
begin
	UPDATE TriggerChipRMBconf
	SET
		TriggerChipRMBconf.TriggerChipRMBStaticConfId = :new.StaticConfigurationId
	WHERE
		TriggerChipRMBconf.TriggerChipRMBStaticConfId = :old.StaticConfigurationId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_2AE9F89C
after delete on StaticConfiguration
for each row
begin
	DELETE FROM TriggerChipRMBconf WHERE
		TriggerChipRMBconf.TriggerChipRMBStaticConfId = :OLD.StaticConfigurationId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_6F756EB1
before insert or update on TriggerChipGBConf
referencing OLD as old NEW as new
for each row when (
	new.TriggerChipGBStaticConfId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row StaticConfiguration%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM StaticConfiguration WHERE
			StaticConfigurationId = fkVal1
		for update of
			StaticConfigurationId;
begin
	open c1(:new.TriggerChipGBStaticConfId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_1574FC5E
after update of StaticConfigurationId on StaticConfiguration
referencing OLD as old NEW as new
for each row
begin
	UPDATE TriggerChipGBConf
	SET
		TriggerChipGBConf.TriggerChipGBStaticConfId = :new.StaticConfigurationId
	WHERE
		TriggerChipGBConf.TriggerChipGBStaticConfId = :old.StaticConfigurationId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_1574FC5E
after delete on StaticConfiguration
for each row
begin
	DELETE FROM TriggerChipGBConf WHERE
		TriggerChipGBConf.TriggerChipGBStaticConfId = :OLD.StaticConfigurationId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_8F75EE2C
before insert or update on TriggerPacChipConf
referencing OLD as old NEW as new
for each row when (
	new.TriggerPacChipStaticConfId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row StaticConfiguration%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM StaticConfiguration WHERE
			StaticConfigurationId = fkVal1
		for update of
			StaticConfigurationId;
begin
	open c1(:new.TriggerPacChipStaticConfId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_1574FC5F
after update of StaticConfigurationId on StaticConfiguration
referencing OLD as old NEW as new
for each row
begin
	UPDATE TriggerPacChipConf
	SET
		TriggerPacChipConf.TriggerPacChipStaticConfId = :new.StaticConfigurationId
	WHERE
		TriggerPacChipConf.TriggerPacChipStaticConfId = :old.StaticConfigurationId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_1574FC5F
after delete on StaticConfiguration
for each row
begin
	DELETE FROM TriggerPacChipConf WHERE
		TriggerPacChipConf.TriggerPacChipStaticConfId = :OLD.StaticConfigurationId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_8F7510AA
before insert or update on CSCChipConf
referencing OLD as old NEW as new
for each row when (
	new.CSCChipStaticConfId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row StaticConfiguration%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM StaticConfiguration WHERE
			StaticConfigurationId = fkVal1
		for update of
			StaticConfigurationId;
begin
	open c1(:new.CSCChipStaticConfId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_1574FC51
after update of StaticConfigurationId on StaticConfiguration
referencing OLD as old NEW as new
for each row
begin
	UPDATE CSCChipConf
	SET
		CSCChipConf.CSCChipStaticConfId = :new.StaticConfigurationId
	WHERE
		CSCChipConf.CSCChipStaticConfId = :old.StaticConfigurationId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_1574FC51
after delete on StaticConfiguration
for each row
begin
	DELETE FROM CSCChipConf WHERE
		CSCChipConf.CSCChipStaticConfId = :OLD.StaticConfigurationId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_8F751095
before insert or update on DCCChipConf
referencing OLD as old NEW as new
for each row when (
	new.DCCChipStaticConfId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row StaticConfiguration%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM StaticConfiguration WHERE
			StaticConfigurationId = fkVal1
		for update of
			StaticConfigurationId;
begin
	open c1(:new.DCCChipStaticConfId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_1574FC52
after update of StaticConfigurationId on StaticConfiguration
referencing OLD as old NEW as new
for each row
begin
	UPDATE DCCChipConf
	SET
		DCCChipConf.DCCChipStaticConfId = :new.StaticConfigurationId
	WHERE
		DCCChipConf.DCCChipStaticConfId = :old.StaticConfigurationId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_1574FC52
after delete on StaticConfiguration
for each row
begin
	DELETE FROM DCCChipConf WHERE
		DCCChipConf.DCCChipStaticConfId = :OLD.StaticConfigurationId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_AF4AFA75
before insert or update on TrigChipOpToReceiverConf
referencing OLD as old NEW as new
for each row when (
	new.TCOTRStaticConfId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row StaticConfiguration%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM StaticConfiguration WHERE
			StaticConfigurationId = fkVal1
		for update of
			StaticConfigurationId;
begin
	open c1(:new.TCOTRStaticConfId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_1574FC53
after update of StaticConfigurationId on StaticConfiguration
referencing OLD as old NEW as new
for each row
begin
	UPDATE TrigChipOpToReceiverConf
	SET
		TrigChipOpToReceiverConf.TCOTRStaticConfId = :new.StaticConfigurationId
	WHERE
		TrigChipOpToReceiverConf.TCOTRStaticConfId = :old.StaticConfigurationId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_1574FC53
after delete on StaticConfiguration
for each row
begin
	DELETE FROM TrigChipOpToReceiverConf WHERE
		TrigChipOpToReceiverConf.TCOTRStaticConfId = :OLD.StaticConfigurationId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_8F751F4A
before insert or update on SorterChipConf
referencing OLD as old NEW as new
for each row when (
	new.SorterChipStaticConfId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row StaticConfiguration%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM StaticConfiguration WHERE
			StaticConfigurationId = fkVal1
		for update of
			StaticConfigurationId;
begin
	open c1(:new.SorterChipStaticConfId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_1574FC50
after update of StaticConfigurationId on StaticConfiguration
referencing OLD as old NEW as new
for each row
begin
	UPDATE SorterChipConf
	SET
		SorterChipConf.SorterChipStaticConfId = :new.StaticConfigurationId
	WHERE
		SorterChipConf.SorterChipStaticConfId = :old.StaticConfigurationId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_1574FC50
after delete on StaticConfiguration
for each row
begin
	DELETE FROM SorterChipConf WHERE
		SorterChipConf.SorterChipStaticConfId = :OLD.StaticConfigurationId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_8F74C55B
before insert or update on TCBackPlaneChipConf
referencing OLD as old NEW as new
for each row when (
	new.TCBackPlaneChipStaticConfId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row StaticConfiguration%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM StaticConfiguration WHERE
			StaticConfigurationId = fkVal1
		for update of
			StaticConfigurationId;
begin
	open c1(:new.TCBackPlaneChipStaticConfId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_1574FC54
after update of StaticConfigurationId on StaticConfiguration
referencing OLD as old NEW as new
for each row
begin
	UPDATE TCBackPlaneChipConf
	SET
		TCBackPlaneChipConf.TCBackPlaneChipStaticConfId = :new.StaticConfigurationId
	WHERE
		TCBackPlaneChipConf.TCBackPlaneChipStaticConfId = :old.StaticConfigurationId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_1574FC54
after delete on StaticConfiguration
for each row
begin
	DELETE FROM TCBackPlaneChipConf WHERE
		TCBackPlaneChipConf.TCBackPlaneChipStaticConfId = :OLD.StaticConfigurationId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_8F751254
before insert or update on LinkChipConf
referencing OLD as old NEW as new
for each row when (
	new.LinkChipStaticConfId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row StaticConfiguration%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM StaticConfiguration WHERE
			StaticConfigurationId = fkVal1
		for update of
			StaticConfigurationId;
begin
	open c1(:new.LinkChipStaticConfId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_1574FC55
after update of StaticConfigurationId on StaticConfiguration
referencing OLD as old NEW as new
for each row
begin
	UPDATE LinkChipConf
	SET
		LinkChipConf.LinkChipStaticConfId = :new.StaticConfigurationId
	WHERE
		LinkChipConf.LinkChipStaticConfId = :old.StaticConfigurationId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_1574FC55
after delete on StaticConfiguration
for each row
begin
	DELETE FROM LinkChipConf WHERE
		LinkChipConf.LinkChipStaticConfId = :OLD.StaticConfigurationId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_C5A3197
before insert or update on GasMixture
referencing OLD as old NEW as new
for each row when (
	new.GasLine_GasLineId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row GasLine%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM GasLine WHERE
			GasLineId = fkVal1
		for update of
			GasLineId;
begin
	open c1(:new.GasLine_GasLineId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_189B197
after update of GasLineId on GasLine
referencing OLD as old NEW as new
for each row
begin
	UPDATE GasMixture
	SET
		GasMixture.GasLine_GasLineId = :new.GasLineId
	WHERE
		GasMixture.GasLine_GasLineId = :old.GasLineId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_189B197
after delete on GasLine
for each row
begin
	DELETE FROM GasMixture WHERE
		GasMixture.GasLine_GasLineId = :OLD.GasLineId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_Febconf_FEB_FK2
before insert or update on FEBConf
referencing OLD as old NEW as new
for each row when (
	new.FEB_FEBId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row FEB%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM FEB WHERE
			FEBId = fkVal1
		for update of
			FEBId;
begin
	open c1(:new.FEB_FEBId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_Feb_FEB_FK2
after update of FEBId on FEB
referencing OLD as old NEW as new
for each row
begin
	UPDATE FEBConf
	SET
		FEBConf.FEB_FEBId = :new.FEBId
	WHERE
		FEBConf.FEB_FEBId = :old.FEBId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_Feb_FEB_FK2
after delete on FEB
for each row
begin
	DELETE FROM FEBConf WHERE
		FEBConf.FEB_FEBId = :OLD.FEBId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_9C827FCC
before insert or update on DynamicParamValue
referencing OLD as old NEW as new
for each row when (
	new.DC_DynamicConfId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row DynamicConf%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM DynamicConf WHERE
			DynamicConfId = fkVal1
		for update of
			DynamicConfId;
begin
	open c1(:new.DC_DynamicConfId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_AEFB7FA6
after update of DynamicConfId on DynamicConf
referencing OLD as old NEW as new
for each row
begin
	UPDATE DynamicParamValue
	SET
		DynamicParamValue.DC_DynamicConfId = :new.DynamicConfId
	WHERE
		DynamicParamValue.DC_DynamicConfId = :old.DynamicConfId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_AEFB7FA6
after delete on DynamicConf
for each row
begin
	DELETE FROM DynamicParamValue WHERE
		DynamicParamValue.DC_DynamicConfId = :OLD.DynamicConfId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_C82779BE
before insert or update on DynamicParamValue
referencing OLD as old NEW as new
for each row when (
	new.DPD_DynamicParamDefId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row DynamicParamDef%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM DynamicParamDef WHERE
			DynamicParamDefId = fkVal1
		for update of
			DynamicParamDefId;
begin
	open c1(:new.DPD_DynamicParamDefId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_77177EA9
after update of DynamicParamDefId on DynamicParamDef
referencing OLD as old NEW as new
for each row
begin
	UPDATE DynamicParamValue
	SET
		DynamicParamValue.DPD_DynamicParamDefId = :new.DynamicParamDefId
	WHERE
		DynamicParamValue.DPD_DynamicParamDefId = :old.DynamicParamDefId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_77177EA9
after delete on DynamicParamDef
for each row
begin
	DELETE FROM DynamicParamValue WHERE
		DynamicParamValue.DPD_DynamicParamDefId = :OLD.DynamicParamDefId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_3B8BFF53
before insert or update on DynamicParamDef
referencing OLD as old NEW as new
for each row when (
	new.DCD_DynamicConfDefId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row DynamicConfDef%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM DynamicConfDef WHERE
			DynamicConfDefId = fkVal1
		for update of
			DynamicConfDefId;
begin
	open c1(:new.DCD_DynamicConfDefId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_BBCBFFEF
after update of DynamicConfDefId on DynamicConfDef
referencing OLD as old NEW as new
for each row
begin
	UPDATE DynamicParamDef
	SET
		DynamicParamDef.DCD_DynamicConfDefId = :new.DynamicConfDefId
	WHERE
		DynamicParamDef.DCD_DynamicConfDefId = :old.DynamicConfDefId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_BBCBFFEF
after delete on DynamicConfDef
for each row
begin
	DELETE FROM DynamicParamDef WHERE
		DynamicParamDef.DCD_DynamicConfDefId = :OLD.DynamicConfDefId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_9AC3FF86
before insert or update on Firmware
referencing OLD as old NEW as new
for each row when (
	new.DCD_DynamicConfDefId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row DynamicConfDef%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM DynamicConfDef WHERE
			DynamicConfDefId = fkVal1
		for update of
			DynamicConfDefId;
begin
	open c1(:new.DCD_DynamicConfDefId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_BBCBFFEC
after update of DynamicConfDefId on DynamicConfDef
referencing OLD as old NEW as new
for each row
begin
	UPDATE Firmware
	SET
		Firmware.DCD_DynamicConfDefId = :new.DynamicConfDefId
	WHERE
		Firmware.DCD_DynamicConfDefId = :old.DynamicConfDefId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_BBCBFFEC
after delete on DynamicConfDef
for each row
begin
	DELETE FROM Firmware WHERE
		Firmware.DCD_DynamicConfDefId = :OLD.DynamicConfDefId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_A575DF65
before insert or update on ChipConfAssignment
referencing OLD as old NEW as new
for each row when (
	new.SC_StaticConfigurationId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row StaticConfiguration%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM StaticConfiguration WHERE
			StaticConfigurationId = fkVal1
		for update of
			StaticConfigurationId;
begin
	open c1(:new.SC_StaticConfigurationId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_1574FC56
after update of StaticConfigurationId on StaticConfiguration
referencing OLD as old NEW as new
for each row
begin
	UPDATE ChipConfAssignment
	SET
		ChipConfAssignment.SC_StaticConfigurationId = :new.StaticConfigurationId
	WHERE
		ChipConfAssignment.SC_StaticConfigurationId = :old.StaticConfigurationId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_1574FC56
after delete on StaticConfiguration
for each row
begin
	DELETE FROM ChipConfAssignment WHERE
		ChipConfAssignment.SC_StaticConfigurationId = :OLD.StaticConfigurationId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_CC54458E
before insert or update on ChipConfAssignment
referencing OLD as old NEW as new
for each row when (
	new.Firware_FirmwareId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row Firmware%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM Firmware WHERE
			FirmwareId = fkVal1
		for update of
			FirmwareId;
begin
	open c1(:new.Firware_FirmwareId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_66B6597
after update of FirmwareId on Firmware
referencing OLD as old NEW as new
for each row
begin
	UPDATE ChipConfAssignment
	SET
		ChipConfAssignment.Firware_FirmwareId = :new.FirmwareId
	WHERE
		ChipConfAssignment.Firware_FirmwareId = :old.FirmwareId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_66B6597
after delete on Firmware
for each row
begin
	DELETE FROM ChipConfAssignment WHERE
		ChipConfAssignment.Firware_FirmwareId = :OLD.FirmwareId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_62A17F6A
before insert or update on ChipConfAssignment
referencing OLD as old NEW as new
for each row when (
	new.DC_DynamicConfId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row DynamicConf%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM DynamicConf WHERE
			DynamicConfId = fkVal1
		for update of
			DynamicConfId;
begin
	open c1(:new.DC_DynamicConfId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_AEFB7FA5
after update of DynamicConfId on DynamicConf
referencing OLD as old NEW as new
for each row
begin
	UPDATE ChipConfAssignment
	SET
		ChipConfAssignment.DC_DynamicConfId = :new.DynamicConfId
	WHERE
		ChipConfAssignment.DC_DynamicConfId = :old.DynamicConfId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_AEFB7FA5
after delete on DynamicConf
for each row
begin
	DELETE FROM ChipConfAssignment WHERE
		ChipConfAssignment.DC_DynamicConfId = :OLD.DynamicConfId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_9CC545E6
before insert or update on ChipConfAssignment
referencing OLD as old NEW as new
for each row when (
	new.Chip_ChipId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row Chip%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM Chip WHERE
			ChipId = fkVal1
		for update of
			ChipId;
begin
	open c1(:new.Chip_ChipId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_Chip_Chip_FK1
after update of ChipId on Chip
referencing OLD as old NEW as new
for each row
begin
	UPDATE ChipConfAssignment
	SET
		ChipConfAssignment.Chip_ChipId = :new.ChipId
	WHERE
		ChipConfAssignment.Chip_ChipId = :old.ChipId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_Chip_Chip_FK1
after delete on Chip
for each row
begin
	DELETE FROM ChipConfAssignment WHERE
		ChipConfAssignment.Chip_ChipId = :OLD.ChipId;
end;
/
REM
REM End Cascade on Updates and Deletes for Foreign Keys 
REM
