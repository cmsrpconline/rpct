REM
REM Start Create Sequences and Triggers

REM
REM Message : Created Trigger/Sequence: Primary key for Table LOCATION

CREATE SEQUENCE S_01_1_BACKPLANESORTERCONN START WITH 0 INCREMENT BY 1 MINVALUE 0 NOMAXVALUE;

CREATE TRIGGER TR_S_01_1_BACKPLANESORTERCONN BEFORE INSERT ON BACKPLANESORTERCONN
 FOR EACH ROW  BEGIN  SELECT S_01_1_BACKPLANESORTERCONN.nextval INTO :new.BACKPLANESORTERCONNID
 FROM dual; END;
/

CREATE SEQUENCE S_02_1_DISTRIBUTIONBOARD START WITH 0 INCREMENT BY 1 MINVALUE 0 NOMAXVALUE;

CREATE TRIGGER TR_S_02_1_DISTRIBUTIONBOARD BEFORE INSERT ON DISTRIBUTIONBOARD
 FOR EACH ROW  BEGIN  SELECT S_02_1_DISTRIBUTIONBOARD.nextval INTO :new.DISTRIBUTIONBOARDID
 FROM dual; END;
/


CREATE SEQUENCE S_03_1_CHAMBERSTRIP START WITH 0 INCREMENT BY 1 MINVALUE 0 NOMAXVALUE;

CREATE TRIGGER TR_S_03_1_CHAMBERSTRIP BEFORE INSERT ON CHAMBERSTRIP
 FOR EACH ROW  BEGIN  SELECT S_03_1_CHAMBERSTRIP.nextval INTO :new.CHAMBERSTRIPID
 FROM dual; END;
/

CREATE SEQUENCE S_04_1_LOCATION START WITH 0 INCREMENT BY 1 MINVALUE 0 NOMAXVALUE;

CREATE TRIGGER TR_S_04_1_LOCATION BEFORE INSERT ON LOCATION
 FOR EACH ROW  BEGIN  SELECT S_04_1_LOCATION.nextval INTO :new.LOCATIONID
 FROM dual; END;
/

CREATE SEQUENCE S_05_1_GASDISTRIBUTOR START WITH 0 INCREMENT BY 1 MINVALUE 0 NOMAXVALUE;

CREATE TRIGGER TR_S_05_1_GASDISTRIBUTOR BEFORE INSERT ON GASDISTRIBUTOR
 FOR EACH ROW  BEGIN  SELECT S_05_1_GASDISTRIBUTOR.nextval INTO :new.GASDISTRIBUTORID
 FROM dual; END;
/

CREATE SEQUENCE S_06_1_GASLINE START WITH 0 INCREMENT BY 1 MINVALUE 0 NOMAXVALUE;

CREATE TRIGGER TR_S_06_1_GASLINE BEFORE INSERT ON GASLINE
 FOR EACH ROW  BEGIN  SELECT S_06_1_GASLINE.nextval INTO :new.GASLINEID
 FROM dual; END;
/

CREATE SEQUENCE S_07_1_CHAMBER START WITH 0 INCREMENT BY 1 MINVALUE 0 NOMAXVALUE;

CREATE TRIGGER TR_S_07_1_CHAMBER BEFORE INSERT ON CHAMBER
 FOR EACH ROW  BEGIN  SELECT S_07_1_CHAMBER.nextval INTO :new.CHAMBERID
 FROM dual; END;
/

CREATE SEQUENCE S_08_1_FEB START WITH 0 INCREMENT BY 1 MINVALUE 0 NOMAXVALUE;

CREATE TRIGGER TR_S_08_1_FEB BEFORE INSERT ON FEB
 FOR EACH ROW  BEGIN  SELECT S_08_1_FEB.nextval INTO :new.FEBID
 FROM dual; END;
/

CREATE SEQUENCE S_09_1_HOBOARD START WITH 0 INCREMENT BY 1 MINVALUE 0 NOMAXVALUE;

CREATE TRIGGER TR_S_09_1_HOBOARD BEFORE INSERT ON HOBOARD
 FOR EACH ROW  BEGIN  SELECT S_09_1_HOBOARD.nextval INTO :new.HOBOARDID
 FROM dual; END;
/

CREATE SEQUENCE S_10_1_LINKCONN START WITH 0 INCREMENT BY 1 MINVALUE 0 NOMAXVALUE;

CREATE TRIGGER TR_S_10_1_LINKCONN BEFORE INSERT ON LINKCONN
 FOR EACH ROW  BEGIN  SELECT S_10_1_LINKCONN.nextval INTO :new.LINKCONNID
 FROM dual; END;
/

CREATE SEQUENCE S_11_1_LINKBOARD START WITH 0 INCREMENT BY 1 MINVALUE 0 NOMAXVALUE;

CREATE TRIGGER TR_S_11_1_LINKBOARD BEFORE INSERT ON LINKBOARD
 FOR EACH ROW  BEGIN  SELECT S_11_1_LINKBOARD.nextval INTO :new.LINKBOARDID
 FROM dual; END;
/

CREATE SEQUENCE S_12_1_SPLITTERBOARD START WITH 0 INCREMENT BY 1 MINVALUE 0 NOMAXVALUE;

CREATE TRIGGER TR_S_12_1_SPLITTERBOARD BEFORE INSERT ON SPLITTERBOARD
 FOR EACH ROW  BEGIN  SELECT S_12_1_SPLITTERBOARD.nextval INTO :new.SPLITTERBOARDID
 FROM dual; END;
/

CREATE SEQUENCE S_13_1_TRIGGERBOARD START WITH 0 INCREMENT BY 1 MINVALUE 0 NOMAXVALUE;

CREATE TRIGGER TR_S_13_1_TRIGGERBOARD BEFORE INSERT ON TRIGGERBOARD
 FOR EACH ROW  BEGIN  SELECT S_13_1_TRIGGERBOARD.nextval INTO :new.TRIGGERBOARDID
 FROM dual; END;
/

CREATE SEQUENCE S_14_1_DCCBOARD START WITH 0 INCREMENT BY 1 MINVALUE 0 NOMAXVALUE;

CREATE TRIGGER TR_S_14_1_DCCBOARD BEFORE INSERT ON DCCBOARD
 FOR EACH ROW  BEGIN  SELECT S_14_1_DCCBOARD.nextval INTO :new.DCCBOARDID
 FROM dual; END;
/

CREATE SEQUENCE S_15_1_SORTERBOARD START WITH 0 INCREMENT BY 1 MINVALUE 0 NOMAXVALUE;

CREATE TRIGGER TR_S_15_1_SORTERBOARD BEFORE INSERT ON SORTERBOARD
 FOR EACH ROW  BEGIN  SELECT S_15_1_SORTERBOARD.nextval INTO :new.SORTERBOARDID
 FROM dual; END;
/

CREATE SEQUENCE S_16_1_TCBACKPLANE START WITH 0 INCREMENT BY 1 MINVALUE 0 NOMAXVALUE;

CREATE TRIGGER TR_S_16_1_TCBACKPLANE BEFORE INSERT ON TCBACKPLANE
 FOR EACH ROW  BEGIN  SELECT S_16_1_TCBACKPLANE.nextval INTO :new.TCBACKPLANEID
 FROM dual; END;
/

CREATE SEQUENCE S_17_1_CONTROLBOARD START WITH 0 INCREMENT BY 1 MINVALUE 0 NOMAXVALUE;

CREATE TRIGGER TR_S_17_1_CONTROLBOARD BEFORE INSERT ON CONTROLBOARD
 FOR EACH ROW  BEGIN  SELECT S_17_1_CONTROLBOARD.nextval INTO :new.CONTROLBOARDID
 FROM dual; END;
/

CREATE SEQUENCE S_18_1_CCSBOARD START WITH 0 INCREMENT BY 1 MINVALUE 0 NOMAXVALUE;

CREATE TRIGGER TR_S_18_1_CCSBOARD BEFORE INSERT ON CCSBOARD
 FOR EACH ROW  BEGIN  SELECT S_18_1_CCSBOARD.nextval INTO :new.CCSBOARDID
 FROM dual; END;
/

CREATE SEQUENCE S_19_1_CHIP START WITH 0 INCREMENT BY 1 MINVALUE 0 NOMAXVALUE;

CREATE TRIGGER TR_S_19_1_CHIP BEFORE INSERT ON CHIP
 FOR EACH ROW  BEGIN  SELECT S_19_1_CHIP.nextval INTO :new.CHIPID
 FROM dual; END;
/

CREATE SEQUENCE S_20_1_CRATE START WITH 0 INCREMENT BY 1 MINVALUE 0 NOMAXVALUE;

CREATE TRIGGER TR_S_20_1_CRATE BEFORE INSERT ON CRATE
 FOR EACH ROW  BEGIN  SELECT S_20_1_CRATE.nextval INTO :new.CRATEID
 FROM dual; END;
/

CREATE SEQUENCE S_21_1_BOARD START WITH 0 INCREMENT BY 1 MINVALUE 0 NOMAXVALUE;

CREATE TRIGGER TR_S_21_1_BOARD BEFORE INSERT ON BOARD
 FOR EACH ROW  BEGIN  SELECT S_21_1_BOARD.nextval INTO :new.BOARDID
 FROM dual; END;
/

CREATE SEQUENCE S_22_1_CHAMBERTYPE START WITH 0 INCREMENT BY 1 MINVALUE 0 NOMAXVALUE;

CREATE TRIGGER TR_S_22_1_CHAMBERTYPE BEFORE INSERT ON CHAMBERTYPE
 FOR EACH ROW  BEGIN  SELECT S_22_1_CHAMBERTYPE.nextval INTO :new.CHAMBERTYPEID
 FROM dual; END;
/

REM
REM End Sequences and Triggers
REM
