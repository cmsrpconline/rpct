REM
REM Start Primary Keys
REM

ALTER TABLE BACKPLANESORTERCONN
	ADD CONSTRAINT BACKPLANESORTERCONN_PK
	PRIMARY KEY ( BACKPLANESORTERCONNID )
	USING INDEX TABLESPACE indx01;

ALTER TABLE DISTRIBUTIONBOARD
	ADD CONSTRAINT DISTRIBUTIONBOARD_PK
	PRIMARY KEY ( DISTRIBUTIONBOARDID )
	USING INDEX TABLESPACE indx01;

ALTER TABLE CHAMBERSTRIP
	ADD CONSTRAINT CHAMBERSTRIP_PK
	PRIMARY KEY ( CHAMBERSTRIPID )
	USING INDEX TABLESPACE indx01;

ALTER TABLE LOCATION
	ADD CONSTRAINT LOCATION_PK
	PRIMARY KEY ( LOCATIONID )
	USING INDEX TABLESPACE indx01;

ALTER TABLE GASDISTRIBUTOR
	ADD CONSTRAINT GASDISTRIBUTOR_PK
	PRIMARY KEY ( GASDISTRIBUTORID )
	USING INDEX TABLESPACE indx01;

ALTER TABLE GASLINE
	ADD CONSTRAINT GASLINE_PK
	PRIMARY KEY ( GASLINEID )
	USING INDEX TABLESPACE indx01;

ALTER TABLE CHAMBER
	ADD CONSTRAINT CHAMBER_PK
	PRIMARY KEY ( CHAMBERID )
	USING INDEX TABLESPACE indx01;

ALTER TABLE FEB
	ADD CONSTRAINT FEB_PK
	PRIMARY KEY ( FEBID )
	USING INDEX TABLESPACE indx01;

ALTER TABLE HOBOARD
	ADD CONSTRAINT HOBOARD_PK
	PRIMARY KEY ( HOBOARDID )
	USING INDEX TABLESPACE indx01;

ALTER TABLE LINKCONN
	ADD CONSTRAINT LINKCONN_PK
	PRIMARY KEY ( LINKCONNID )
	USING INDEX TABLESPACE indx01;

ALTER TABLE LINKBOARD
	ADD CONSTRAINT LINKBOARD_PK
	PRIMARY KEY ( LINKBOARDID )
	USING INDEX TABLESPACE indx01;

ALTER TABLE SPLITTERBOARD
	ADD CONSTRAINT SPLITTERBOARD_PK
	PRIMARY KEY ( SPLITTERBOARDID )
	USING INDEX TABLESPACE indx01;

ALTER TABLE TRIGGERBOARD
	ADD CONSTRAINT TRIGGERBOARD_PK
	PRIMARY KEY ( TRIGGERBOARDID )
	USING INDEX TABLESPACE indx01;

ALTER TABLE DCCBOARD
	ADD CONSTRAINT DCCBOARD_PK
	PRIMARY KEY ( DCCBOARDID )
	USING INDEX TABLESPACE indx01;

ALTER TABLE SORTERBOARD
	ADD CONSTRAINT SORTERBOARD_PK
	PRIMARY KEY ( SORTERBOARDID )
	USING INDEX TABLESPACE indx01;

ALTER TABLE TCBACKPLANE
	ADD CONSTRAINT TCBACKPLANE_PK
	PRIMARY KEY ( TCBACKPLANEID )
	USING INDEX TABLESPACE indx01;

ALTER TABLE CONTROLBOARD
	ADD CONSTRAINT CONTROLBOARD_PK
	PRIMARY KEY ( CONTROLBOARDID )
	USING INDEX TABLESPACE indx01;

ALTER TABLE CCSBOARD
	ADD CONSTRAINT CCSBOARD_PK
	PRIMARY KEY ( CCSBOARDID )
	USING INDEX TABLESPACE indx01;

ALTER TABLE CHIP
	ADD CONSTRAINT CHIP_PK
	PRIMARY KEY ( CHIPID )
	USING INDEX TABLESPACE indx01;

ALTER TABLE CRATE
	ADD CONSTRAINT CRATE_PK
	PRIMARY KEY ( CRATEID )
	USING INDEX TABLESPACE indx01;

ALTER TABLE BOARD
	ADD CONSTRAINT BOARD_PK
	PRIMARY KEY ( BOARDID )
	USING INDEX TABLESPACE indx01;

ALTER TABLE CHAMBERTYPE
	ADD CONSTRAINT CHAMBERTYPE_PK
	PRIMARY KEY ( CHAMBERTYPEID )
	USING INDEX TABLESPACE indx01;
REM
REM End Primary Keys
REM
