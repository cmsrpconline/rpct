REM
REM Set Id's = 0 values for tables
REM


ALTER SESSION SET NLS_DATE_FORMAT = 'YYYYMMDD';
SET DEFINE OFF

INSERT  INTO    GASDISTRIBUTOR     VALUES  ('', 'none');
INSERT  INTO    GASLINE     VALUES  ('', 0, 'none');
INSERT  INTO    CHAMBERTYPE     VALUES  ('', 'none', 0, 0, 0, 0,'none');
INSERT  INTO    LOCATION     VALUES  ('', 'none', 'none', 'none', 'none');
INSERT  INTO    CHAMBER     VALUES  ('', 0, 0, 0, 'none', 'none', '19700101', 0);
INSERT  INTO    CRATE     VALUES  ('', 0, 'none', 'none', 'none');
INSERT  INTO    BOARD     VALUES  ('', 0, 0,'none', 'none', '19700101');
INSERT  INTO    CHIP     VALUES  ('', 0, 'none', 0);
INSERT  INTO    LINKBOARD     VALUES  ('', 'none', 'none');
INSERT  INTO    CCSBOARD     VALUES  ('');
INSERT  INTO    TCBACKPLANE     VALUES  ('', 'none');
INSERT  INTO    SORTERBOARD     VALUES  ('',0 , 'none');
INSERT  INTO    SPLITTERBOARD     VALUES  ('');
INSERT  INTO    CONTROLBOARD     VALUES  ('', 0, 'none', 'none', 'none');
INSERT  INTO    DISTRIBUTIONBOARD     VALUES  ('', 0, 'none', '19700101');
INSERT  INTO    FEB     VALUES  ('', 0, 0, 0, 'none', 0, 0, 'none', '19700101', 0, 0);
INSERT  INTO    CHAMBERSTRIP     VALUES  ('', 0, 0, 0, 0, 0);
INSERT  INTO    BACKPLANESORTERCONN     VALUES  ('', 0, 0, 0);
INSERT  INTO    HOBOARD     VALUES  ('', 'none');
INSERT  INTO    DCCBOARD     VALUES  ('');
INSERT  INTO    TRIGGERBOARD     VALUES  ('', 0, 0, 0, 'none', 'none', 'none', 0);
INSERT  INTO    LINKCONN     VALUES  ('', 0, 0, 0);
COMMIT;
