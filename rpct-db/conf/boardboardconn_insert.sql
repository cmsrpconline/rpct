set serveroutput on

--- Rename LinkConn to BoardBoardConn
alter table LinkConn rename to BoardBoardConn;

alter table BoardBoardConn RENAME COLUMN LINKCONNID TO BoardBoardConnId;
alter table BoardBoardConn RENAME COLUMN TB_TRIGGERBOARDID TO BOARD_COLLECTORBOARDID;
alter table BoardBoardConn RENAME COLUMN TRIGGERBOARDINPUTNUM TO COLLECTORBOARDINPUTNUM;
alter table BoardBoardConn    ADD ("TYPE" VARCHAR2(30 BYTE));


alter table BoardBoardConn RENAME CONSTRAINT LINKCONN_PK TO BoardBoardConn_PK;
alter table BoardBoardConn DROP   CONSTRAINT TriggerBoard_FK1;
alter table BoardBoardConn ADD    CONSTRAINT BOARD_FK15 FOREIGN KEY ("BOARD_COLLECTORBOARDID") REFERENCES "BOARD" ("BOARDID");

---- Indexes
alter index "LINKCONN_IDX1" rename to "BOARDBOARDCONN_IDX1";
alter index "LINKCONN_IDX2" rename to "BOARDBOARDCONN_IDX2";
alter index "LINKCONN_IDX3" rename to "BOARDBOARDCONN_IDX3";

---- Triggers tuning
alter trigger TR_S_10_1_LINKCONN             rename TO TR_S_10_1_BoardBoardConn;
alter trigger VALIDATEFK_LINKCONN_BOARD_FK11 rename TO VALIDATEFK_BBCONN_BOARD_FK11;

CREATE SEQUENCE S_10_1_BOARDBOARDCONN START WITH 1 INCREMENT BY 1;
CREATE OR REPLACE TRIGGER "TR_S_10_1_BOARDBOARDCONN"
BEFORE INSERT ON BOARDBOARDCONN
 FOR EACH ROW
BEGIN  SELECT S_10_1_BOARDBOARDCONN.nextval INTO :new.BOARDBOARDCONNID
 FROM dual; END;
/
ALTER TRIGGER "TR_S_10_1_BOARDBOARDCONN" DISABLE;

CREATE OR REPLACE TRIGGER "VALIDATEFK_6A2CFE87" 
BEFORE INSERT OR UPDATE ON BOARDBOARDCONN
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
  WHEN (
new.Board_CollectorBoardId IS NOT NULL
      ) declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row Board%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM Board WHERE
			BoardId = fkVal1
		for update of
			BoardId;
begin
	open c1(:new.Board_CollectorBoardId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/

create or replace
TRIGGER "ONDELETECASCADE_33513F" 
AFTER DELETE ON BOARD
FOR EACH ROW
begin
	DELETE FROM BoardBoardConn WHERE
		BoardBoardConn.Board_BoardId = :OLD.BoardId;
end;
/
ALTER TRIGGER "ONDELETECASCADE_33513F" DISABLE;

drop trigger "ONDELETECASCADE_FBD8FE80";
create or replace
TRIGGER "ONDELETECASCADE_FBD8FE80" 
AFTER DELETE ON BOARD
FOR EACH ROW
begin
	DELETE FROM BoardBoardConn WHERE
		BoardBoardConn.Board_CollectorBoardId = :OLD.BoardId;
end;
/
ALTER TRIGGER "ONDELETECASCADE_FBD8FE80" DISABLE;

create or replace
TRIGGER "ONUPDATECASCADE_33513F" 
AFTER UPDATE OF BOARDID ON BOARD
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
begin
	UPDATE BoardBoardConn
	SET
		BoardBoardConn.Board_BoardId = :new.BoardId
	WHERE
		BoardBoardConn.Board_BoardId = :old.BoardId;
end;
/

drop trigger "ONUPDATECASCADE_FBD8FE80";
create or replace
TRIGGER "ONUPDATECASCADE_FBD8FE80" 
AFTER UPDATE OF BOARDID ON BOARD
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
begin
	UPDATE BoardBoardConn
	SET
		BoardBoardConn.Board_CollectorBoardId = :new.BoardId
	WHERE
		BoardBoardConn.Board_CollectorBoardId = :old.BoardId;
end;
/

---- Synchronize sequencies
BEGIN
		syncSeqToId('BOARDBOARDCONN', 'S_10_1_BOARDBOARDCONN');
END;
/

---- Update "type"
update BoardBoardConn
set type = 'LINKCONN'
 where type is null;


--- Tune LINKDISABLED
alter table "LINKDISABLED" rename column "LINK_LINKCONNID" to "BBC_BOARDBOARDCONNID";


--- Create LINKTBCONN (former LinkConn)
drop table LINKTBCONN;
CREATE TABLE "LINKTBCONN"
  (
    "LINKTBCONNID" NUMBER(11,0),
    CONSTRAINT "LINKTBCONN_PK" PRIMARY KEY ("LINKTBCONNID"),
    CONSTRAINT "BOARDBOARDCONN_FK1" FOREIGN KEY ("LINKTBCONNID") REFERENCES "BOARDBOARDCONN" ("BOARDBOARDCONNID")
  );

---- Fill it
insert  into LINKTBCONN
   select BOARDBOARDCONNID
     from BOARDBOARDCONN
    where type = 'LINKCONN';

---- Triggers
CREATE OR REPLACE TRIGGER "VALIDATEFK_LINKTBCONN_BBC_FK1" 
BEFORE INSERT OR UPDATE ON LINKTBCONN
FOR EACH ROW
  WHEN (
new.LinkTBConnId IS NOT NULL
      ) declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row BoardBoardConn%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM BoardBoardConn WHERE
			BoardBoardConnId = fkVal1
		for update of
			BoardBoardConnId;
begin
	open c1(:new.LinkTBConnId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/

create or replace
TRIGGER "ONUPDATECASCADE_BBC_1" 
AFTER UPDATE OF BoardBoardConnId ON BoardBoardConn
FOR EACH ROW
begin
	UPDATE LinkTBConn
	SET
		LinkTBConn.LinkTBConnId = :new.BoardBoardConnId
	WHERE
		LinkTBConn.LinkTBConnId = :old.BoardBoardConnId;
end;
/


--- Create RbcTtuConn
drop table RBCTTUCONN;
delete from BoardBoardConn where type = 'RBCTTUCONN';

CREATE TABLE "RBCTTUCONN"
  (
    "RBCTTUCONNID" NUMBER(11,0),
    CONSTRAINT "RBCTTUCONN_PK" PRIMARY KEY ("RBCTTUCONNID"),
    CONSTRAINT "BOARDBOARDCONN_FK2" FOREIGN KEY ("RBCTTUCONNID") REFERENCES "BOARDBOARDCONN" ("BOARDBOARDCONNID")
  );

---- Triggers
CREATE OR REPLACE TRIGGER "VALIDATEFK_RBCTTUCONN_BBC_FK1" 
BEFORE INSERT OR UPDATE ON RBCTTUCONN
FOR EACH ROW
  WHEN (
new.RbcTtuConnId IS NOT NULL
      ) declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row BoardBoardConn%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM BoardBoardConn WHERE
			BoardBoardConnId = fkVal1
		for update of
			BoardBoardConnId;
begin
	open c1(:new.RbcTtuConnId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/

create or replace
TRIGGER "ONUPDATECASCADE_BBC_2" 
AFTER UPDATE OF BoardBoardConnId ON BoardBoardConn
FOR EACH ROW
begin
	UPDATE RbcTtuConn
	SET
		RbcTtuConn.RbcTtuConnId = :new.BoardBoardConnId
	WHERE
		RbcTtuConn.RbcTtuConnId = :old.BoardBoardConnId;
end;
/

---- Fill RbcTtuConn
DECLARE
   TYPE t_RbcTtu_rec IS Record 
   (rbc Board.name%TYPE not null default '',
    ttu Board.name%TYPE not null default '',
    input BoardBoardConn.COLLECTORBOARDINPUTNUM%TYPE not null default 0);
    
   TYPE t_RbcTtu_tab IS VARRAY(30) OF t_RbcTtu_rec;
   
   rbcTtu_tab t_RbcTtu_tab := t_RbcTtu_tab();
   bbc_rec BoardBoardConn%ROWTYPE;
begin
   rbcTtu_tab.extend(30);
   rbcTtu_tab(1).rbc := 'RBC_RB-2_S1';
   rbcTtu_tab(1).ttu := 'TTU_3';
   rbcTtu_tab(1).input := 10;
   rbcTtu_tab(2).rbc := 'RBC_RB-2_S3';
   rbcTtu_tab(2).ttu := 'TTU_3';
   rbcTtu_tab(2).input := 12;
   rbcTtu_tab(3).rbc := 'RBC_RB-2_S5';
   rbcTtu_tab(3).ttu := 'TTU_3';
   rbcTtu_tab(3).input := 13;
   rbcTtu_tab(4).rbc := 'RBC_RB-2_S7';
   rbcTtu_tab(4).ttu := 'TTU_3';
   rbcTtu_tab(4).input := 15;
   rbcTtu_tab(5).rbc := 'RBC_RB-2_S9';
   rbcTtu_tab(5).ttu := 'TTU_3';
   rbcTtu_tab(5).input := 16;
   rbcTtu_tab(6).rbc := 'RBC_RB-2_S11';
   rbcTtu_tab(6).ttu := 'TTU_3';
   rbcTtu_tab(6).input := 9;
   rbcTtu_tab(7).rbc := 'RBC_RB-1_S1';
   rbcTtu_tab(7).ttu := 'TTU_3';
   rbcTtu_tab(7).input := 1;
   rbcTtu_tab(8).rbc := 'RBC_RB-1_S3';
   rbcTtu_tab(8).ttu := 'TTU_3';
   rbcTtu_tab(8).input := 3;
   rbcTtu_tab(9).rbc := 'RBC_RB-1_S5';
   rbcTtu_tab(9).ttu := 'TTU_3';
   rbcTtu_tab(9).input := 4;
   rbcTtu_tab(10).rbc := 'RBC_RB-1_S7';
   rbcTtu_tab(10).ttu := 'TTU_3';
   rbcTtu_tab(10).input := 6;
   rbcTtu_tab(11).rbc := 'RBC_RB-1_S9';
   rbcTtu_tab(11).ttu := 'TTU_3';
   rbcTtu_tab(11).input := 7;
   rbcTtu_tab(12).rbc := 'RBC_RB-1_S11';
   rbcTtu_tab(12).ttu := 'TTU_3';
   rbcTtu_tab(12).input := 0;
   rbcTtu_tab(13).rbc := 'RBC_RB0_S1';
   rbcTtu_tab(13).ttu := 'TTU_2';
   rbcTtu_tab(13).input := 1;
   rbcTtu_tab(14).rbc := 'RBC_RB0_S3';
   rbcTtu_tab(14).ttu := 'TTU_2';
   rbcTtu_tab(14).input := 3;
   rbcTtu_tab(15).rbc := 'RBC_RB0_S5';
   rbcTtu_tab(15).ttu := 'TTU_2';
   rbcTtu_tab(15).input := 4;
   rbcTtu_tab(16).rbc := 'RBC_RB0_S7';
   rbcTtu_tab(16).ttu := 'TTU_2';
   rbcTtu_tab(16).input := 6;
   rbcTtu_tab(17).rbc := 'RBC_RB0_S9';
   rbcTtu_tab(17).ttu := 'TTU_2';
   rbcTtu_tab(17).input := 7;
   rbcTtu_tab(18).rbc := 'RBC_RB0_S11';
   rbcTtu_tab(18).ttu := 'TTU_2';
   rbcTtu_tab(18).input := 0;
   rbcTtu_tab(19).rbc := 'RBC_RB+1_S1';
   rbcTtu_tab(19).ttu := 'TTU_1';
   rbcTtu_tab(19).input := 1;
   rbcTtu_tab(20).rbc := 'RBC_RB+1_S3';
   rbcTtu_tab(20).ttu := 'TTU_1';
   rbcTtu_tab(20).input := 3;
   rbcTtu_tab(21).rbc := 'RBC_RB+1_S5';
   rbcTtu_tab(21).ttu := 'TTU_1';
   rbcTtu_tab(21).input := 4;
   rbcTtu_tab(22).rbc := 'RBC_RB+1_S7';
   rbcTtu_tab(22).ttu := 'TTU_1';
   rbcTtu_tab(22).input := 6;
   rbcTtu_tab(23).rbc := 'RBC_RB+1_S9';
   rbcTtu_tab(23).ttu := 'TTU_1';
   rbcTtu_tab(23).input := 7;
   rbcTtu_tab(24).rbc := 'RBC_RB+1_S11';
   rbcTtu_tab(24).ttu := 'TTU_1';
   rbcTtu_tab(24).input := 0;
   rbcTtu_tab(25).rbc := 'RBC_RB+2_S1';
   rbcTtu_tab(25).ttu := 'TTU_1';
   rbcTtu_tab(25).input := 10;
   rbcTtu_tab(26).rbc := 'RBC_RB+2_S3';
   rbcTtu_tab(26).ttu := 'TTU_1';
   rbcTtu_tab(26).input := 12;
   rbcTtu_tab(27).rbc := 'RBC_RB+2_S5';
   rbcTtu_tab(27).ttu := 'TTU_1';
   rbcTtu_tab(27).input := 13;
   rbcTtu_tab(28).rbc := 'RBC_RB+2_S7';
   rbcTtu_tab(28).ttu := 'TTU_1';
   rbcTtu_tab(28).input := 15;
   rbcTtu_tab(29).rbc := 'RBC_RB+2_S9';
   rbcTtu_tab(29).ttu := 'TTU_1';
   rbcTtu_tab(29).input := 16;
   rbcTtu_tab(30).rbc := 'RBC_RB+2_S11';
   rbcTtu_tab(30).ttu := 'TTU_1';
   rbcTtu_tab(30).input := 9;

   syncSeqToId('BoardBoardConn', 'S_10_1_BOARDBOARDCONN');
   FOR i IN rbcTtu_tab.FIRST .. rbcTtu_tab.LAST
   LOOP
	    dbms_output.put_line('rbcTtu_tab #' || i || 
         '(' || rbcTtu_tab(i).rbc || ', ' || rbcTtu_tab(i).ttu || ', ' || rbcTtu_tab(i).input || ')');

      select S_10_1_BOARDBOARDCONN.nextval INTO bbc_rec.BoardBoardConnId FROM dual;
      select BoardID  into bbc_rec.Board_BoardId from Board where name = rbcTtu_tab(i).rbc;
      select BoardID  into bbc_rec.Board_CollectorBoardId from Board where name = rbcTtu_tab(i).ttu;
      bbc_rec.COLLECTORBOARDINPUTNUM := rbcTtu_tab(i).input;
      bbc_rec.type := 'RBCTTUCONN';

      insert into BoardBoardConn values bbc_rec;
      insert into RbcTtuConn values (bbc_rec.BoardBoardConnId);
   END LOOP;
end;
/
