REM
REM Start Create Tables
REM

-- Create new table TRIGGERCHIPRMBCONF.
-- TRIGGERCHIPRMBCONF : Table of TriggerChipRMBconf
-- 	TRIGGERCHIPRMBSTATICCONFID : TriggerChipRMBStaticConfId identifies TriggerChipRMBconf
-- 	BCN0DELAY : BCN0Delay is of TriggerChipRMBconf
-- 	L1ADELAY : L1ADelay is of TriggerChipRMBconf  
create table TRIGGERCHIPRMBCONF (
	TRIGGERCHIPRMBSTATICCONFID NUMBER(11,0) not null,
	BCN0DELAY NUMBER(11,0) null,
	L1ADELAY NUMBER(11,0) null); 

-- Create new table TRIGGERCHIPGBCONF.
-- TRIGGERCHIPGBCONF : Table of TriggerChipGBConf
-- 	TRIGGERCHIPGBSTATICCONFID : TriggerChipGBStaticConfId identifies TriggerChipGBConf
-- 	DELAY : Delay is of TriggerChipGBConf  
create table TRIGGERCHIPGBCONF (
	TRIGGERCHIPGBSTATICCONFID NUMBER(11,0) not null,
	DELAY NUMBER(11,0) null); 

-- Create new table TRIGGERPACCHIPCONF.
-- TRIGGERPACCHIPCONF : Table of TriggerPacChipConf
-- 	TRIGGERPACCHIPSTATICCONFID : TriggerPacChipStaticConfId identifies TriggerPacChipConf
-- 	DELAY : Delay is of TriggerPacChipConf  
create table TRIGGERPACCHIPCONF (
	TRIGGERPACCHIPSTATICCONFID NUMBER(11,0) not null,
	DELAY NUMBER(11,0) null); 

-- Create new table CSCCHIPCONF.
-- CSCCHIPCONF : Table of CSCChipConf
-- 	CSCCHIPSTATICCONFID : CSCChipStaticConfId identifies CSCChipConf  
create table CSCCHIPCONF (
	CSCCHIPSTATICCONFID NUMBER(11,0) not null); 


-- Create new table DCCCHIPCONF.
-- DCCCHIPCONF : Table of DCCChipConf
-- 	DCCCHIPSTATICCONFID : DCCChipStaticConfId identifies DCCChipConf
-- 	BCN0DELAY : BCN0Delay is of DCCChipConf  
create table DCCCHIPCONF (
	DCCCHIPSTATICCONFID NUMBER(11,0) not null,
	BCN0DELAY NUMBER(11,0) null); 

-- Create new table TRIGCHIPOPTORECEIVERCONF.
-- TRIGCHIPOPTORECEIVERCONF : Table of TrigChipOpToReceiverConf
-- 	TCOTRSTATICCONFID : TCOTRStaticConfId identifies TrigChipOpToReceiverConf
-- 	LINK1DELAY : Link1Delay is of TrigChipOpToReceiverConf
-- 	LINK2DELAY : Link2Delay is of TrigChipOpToReceiverConf
-- 	LINK3DELAY : Link3Delay is of TrigChipOpToReceiverConf  
create table TRIGCHIPOPTORECEIVERCONF (
	TCOTRSTATICCONFID NUMBER(11,0) not null,
	LINK1DELAY NUMBER(11,0) null,
	LINK2DELAY NUMBER(11,0) null,
	LINK3DELAY NUMBER(11,0) null); 

-- Create new table SORTERCHIPCONF.
-- SORTERCHIPCONF : Table of SorterChipConf
-- 	SORTERCHIPSTATICCONFID : SorterChipStaticConfId identifies SorterChipConf
-- 	BCN0DELAY : BCN0Delay is of SorterChipConf  
create table SORTERCHIPCONF (
	SORTERCHIPSTATICCONFID NUMBER(11,0) not null,
	BCN0DELAY NUMBER(11,0) null); 

-- Create new table TCBACKPLANECHIPCONF.
-- TCBACKPLANECHIPCONF : Table of TCBackPlaneChipConf
-- 	TCBACKPLANECHIPSTATICCONFID : TCBackPlaneChipStaticConfId identifies TCBackPlaneChipConf
-- 	BCN0DELAY : BCN0Delay is of TCBackPlaneChipConf  
create table TCBACKPLANECHIPCONF (
	TCBACKPLANECHIPSTATICCONFID NUMBER(11,0) not null,
	BCN0DELAY NUMBER(11,0) null); 

-- Create new table LINKCHIPCONF.
-- LINKCHIPCONF : Table of LinkChipConf
-- 	LINKCHIPSTATICCONFID : LinkChipStaticConfId identifies LinkChipConf
-- 	TTCRXADDRESS : TTCrxAddress is of LinkChipConf
-- 	WINDOWOPEN : WindowOpen is of LinkChipConf
-- 	WINDOWCLOSED : WindowClosed is of LinkChipConf
-- 	INCHANNELMASK : InChannelMask is of LinkChipConf
-- 	CLOCKINVERSION : ClockInversion is of LinkChipConf
-- 	LMUXMASTERENA : LmuxMasterEna is of LinkChipConf
-- 	LMUXSLAV0ENA : LmuxSlav0Ena is of LinkChipConf
-- 	LMUXSLAVE1ENA : LmuxSlave1Ena is of LinkChipConf
-- 	SLAVEENA : SlaveEna is of LinkChipConf
-- 	BCN0DELAY : BCN0Delay is of LinkChipConf
-- 	LMUXMASTERDELAY : LmuxMasterDelay is of LinkChipConf
-- 	LMUXSLAVE0DELAY : LmuxSlave0Delay is of LinkChipConf
-- 	LMUXSLAVE1DELAY : LmuxSlave1Delay is of LinkChipConf  
create table LINKCHIPCONF (
	LINKCHIPSTATICCONFID NUMBER(11,0) not null,
	TTCRXADDRESS NUMBER(11,0) null,
	WINDOWOPEN NUMBER(11,0) null,
	WINDOWCLOSED NUMBER(11,0) null,
	INCHANNELMASK NUMBER(11,0) null,
	CLOCKINVERSION VARCHAR2(1) null,
	LMUXMASTERENA VARCHAR2(1) null,
	LMUXSLAV0ENA VARCHAR2(1) null,
	LMUXSLAVE1ENA VARCHAR2(1) null,
	SLAVEENA VARCHAR2(1) null,
	BCN0DELAY NUMBER(11,0) null,
	LMUXMASTERDELAY NUMBER(11,0) null,
	LMUXSLAVE0DELAY NUMBER(11,0) null,
	LMUXSLAVE1DELAY NUMBER(11,0) null); 

-- Create new table GASMIXTURE.
-- GASMIXTURE : Table of GasMixture
-- 	GASMIXTUREID : GasMixtureId identifies GasMixture
-- 	EQUIPMENT_GASLINE_GASLINEID : Equipment_GasLine_GasLineId is of GasMixture
-- 	H2OPERCENT : H2OPercent is of GasMixture
-- 	FREON : Freon is of GasMixture
-- 	SF6 : SF6 is of GasMixture
-- 	ISOBUTANERECIRCULATIONPERCENT : IsobutaneRecirculationPercent is of GasMixture
-- 	TEMPERATURE : Temperature is of GasMixture
-- 	PRESSURE : Pressure is of GasMixture  
create table GASMIXTURE (
	GASMIXTUREID NUMBER(11,0) not null,
	GASLINE_GASLINEID NUMBER(11,0) not null,
	H2OPERCENT NUMBER(5,2) null,
	FREON NUMBER(5,2) null,
	SF6 NUMBER(5,2) null,
	ISOBUTANERECIRCULATIONPERCENT NUMBER(5,2) null,
	TEMPERATURE NUMBER(5,2) null,
	PRESSURE NUMBER(11,2) null); 

-- Create new table FEBCONF.
-- FEBCONF : Table of FEBConf
-- 	FEBCONFID : FEBConfId identifies FEBConf
-- 	EQUIPMENT_FEB_FEBID : Equipment_FEB_FEBId is of FEBConf
-- 	THRESHHOLD : Threshhold is of FEBConf  
create table FEBCONF (
	FEBCONFID NUMBER(11,0) not null,
	FEB_FEBID NUMBER(11,0) not null,
	THRESHHOLD NUMBER(11,3) null,
	DATEOFCONFIGURATION DATE null); 

-- Create new table DYNAMICCONF.
-- DYNAMICCONF : Table of DynamicConf
-- 	DYNAMICCONFID : DynamicConfId identifies DynamicConf
-- 	TARGET : Target is of DynamicConf  
create table DYNAMICCONF (
	DYNAMICCONFID NUMBER(11,0) not null,
	TARGET VARCHAR2(30) null); 

-- Create new table DYNAMICPARAMVALUE.
-- DYNAMICPARAMVALUE : Table of DynamicParamValue
-- 	DYNAMICPARAMVALUEID : DynamicParamValueId identifies DynamicParamValue
-- 	DC_DYNAMICCONFID : DC_DynamicConfId is of DynamicParamValue
-- 	DPD_DYNAMICPARAMDEFID : DPD_DynamicParamDefId is of DynamicParamValue
-- 	INTVALUE : IntValue is of DynamicParamValue
-- 	FLOATVALUE : FloatValue is of DynamicParamValue
-- 	STRINGVALUE : StringValue is of DynamicParamValue  
create table DYNAMICPARAMVALUE (
	DYNAMICPARAMVALUEID NUMBER(11,0) not null,
	DC_DYNAMICCONFID NUMBER(11,0) not null,
	DPD_DYNAMICPARAMDEFID NUMBER(11,0) not null,
	INTVALUE NUMBER(11,0) null,
	FLOATVALUE NUMBER null,
	STRINGVALUE VARCHAR2(30) null); 

-- Create new table DYNAMICPARAMDEF.
-- DYNAMICPARAMDEF : Table of DynamicParamDef
-- 	DYNAMICPARAMDEFID : DynamicParamDefId identifies DynamicParamDef
-- 	DCD_DYNAMICCONFDEFID : DCD_DynamicConfDefId is of DynamicParamDef
-- 	VERSION : Version is of DynamicParamDef  
create table DYNAMICPARAMDEF (
	DYNAMICPARAMDEFID NUMBER(11,0) not null,
	DCD_DYNAMICCONFDEFID NUMBER(11,0) not null,
	VERSION VARCHAR2(30) null); 

-- Create new table DYNAMICCONFDEF.
-- DYNAMICCONFDEF : Table of DynamicConfDef
-- 	DYNAMICCONFDEFID : DynamicConfDefId identifies DynamicConfDef
-- 	VERSION : Version is of DynamicConfDef  
create table DYNAMICCONFDEF (
	DYNAMICCONFDEFID NUMBER(11,0) not null,
	VERSION VARCHAR2(30) null); 

-- Create new table FIRMWARE.
-- FIRMWARE : Table of Firmware
-- 	FIRMWAREID : FirmwareId identifies Firmware
-- 	DCD_DYNAMICCONFDEFID : DCD_DynamicConfDefId is of Firmware
-- 	REFERENCELOCATION : ReferenceLocation is of Firmware
-- 	VERSION : Version is of Firmware
-- 	DESCRIPTION : Description is of Firmware
-- 	DATEOFCREATION : DateOfCreation is of Firmware
-- 	IIDREFERENCELOCATION : IIDReferenceLocation is of Firmware - lokation of IID file  
create table FIRMWARE (
	FIRMWAREID NUMBER(11,0) not null,
	DCD_DYNAMICCONFDEFID NUMBER(11,0) not null,
	REFERENCELOCATION VARCHAR2(30) null,
	VERSION VARCHAR2(30) null,
	DESCRIPTION VARCHAR2(30) null,
	DATEOFCREATION DATE null,
	IIDREFERENCELOCATION VARCHAR2(30) null); 

-- Create new table CHIPCONFASSIGNMENT.
-- CHIPCONFASSIGNMENT : Table of ChipConfAssignment
-- 	CHIPCONFASSIGNMENTID : ChipConfAssignmentId identifies ChipConfAssignment
-- 	SC_STATICCONFIGURATIONID : SC_StaticConfigurationId is of ChipConfAssignment
-- 	FIRWARE_FIRMWAREID : Firware_FirmwareId is of ChipConfAssignment
-- 	DC_DYNAMICCONFID : DC_DynamicConfId partly identifies ChipConfAssignment
-- 	CHIP_CHIPID : Chip_ChipId is of ChipConfAssignment  
create table CHIPCONFASSIGNMENT (
	CHIPCONFASSIGNMENTID NUMBER(11,0) not null,
	SC_STATICCONFIGURATIONID NUMBER(11,0) not null,
	FIRWARE_FIRMWAREID NUMBER(11,0) not null,
	DC_DYNAMICCONFID NUMBER(11,0) not null,
	CHIP_CHIPID NUMBER(11,0) not null); 

-- Create new table STATICCONFIGURATION.
-- STATICCONFIGURATION : Table of StaticConfiguration
-- 	STATICCONFIGURATIONID : StaticConfigurationId identifies StaticConfiguration
-- 	VERSION : Version is of StaticConfiguration
-- 	TAG : Tag is of StaticConfiguration
-- 	DATEOFCONFIGURATION : DateOfConfiguration is of StaticConfiguration  
create table STATICCONFIGURATION (
	STATICCONFIGURATIONID NUMBER(11,0) not null,
	VERSION VARCHAR2(30) null,
	TAG VARCHAR2(30) null,
	DATEOFCONFIGURATION DATE null); 

REM
REM End Create Tables
REM