REM
REM Start Cascade on Updates and Deletes for Foreign Keys 
REM

--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_50A2E51E
before insert or update on BackPlaneSorterConn
referencing OLD as old NEW as new
for each row when (
	new.TCBP_TCBackPlaneId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row TCBackPlane%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM TCBackPlane WHERE
			TCBackPlaneId = fkVal1
		for update of
			TCBackPlaneId;
begin
	open c1(:new.TCBP_TCBackPlaneId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_D449E496
after update of TCBackPlaneId on TCBackPlane
referencing OLD as old NEW as new
for each row
begin
	UPDATE BackPlaneSorterConn
	SET
		BackPlaneSorterConn.TCBP_TCBackPlaneId = :new.TCBackPlaneId
	WHERE
		BackPlaneSorterConn.TCBP_TCBackPlaneId = :old.TCBackPlaneId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_D449E496
after delete on TCBackPlane
for each row
begin
	DELETE FROM BackPlaneSorterConn WHERE
		BackPlaneSorterConn.TCBP_TCBackPlaneId = :OLD.TCBackPlaneId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_50A5370E
before insert or update on BackPlaneSorterConn
referencing OLD as old NEW as new
for each row when (
	new.SorterBoard_SorterBoardId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row SorterBoard%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM SorterBoard WHERE
			SorterBoardId = fkVal1
		for update of
			SorterBoardId;
begin
	open c1(:new.SorterBoard_SorterBoardId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_C96F3686
after update of SorterBoardId on SorterBoard
referencing OLD as old NEW as new
for each row
begin
	UPDATE BackPlaneSorterConn
	SET
		BackPlaneSorterConn.SorterBoard_SorterBoardId = :new.SorterBoardId
	WHERE
		BackPlaneSorterConn.SorterBoard_SorterBoardId = :old.SorterBoardId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_C96F3686
after delete on SorterBoard
for each row
begin
	DELETE FROM BackPlaneSorterConn WHERE
		BackPlaneSorterConn.SorterBoard_SorterBoardId = :OLD.SorterBoardId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_31D55248
before insert or update on DistributionBoard
referencing OLD as old NEW as new
for each row when (
	new.CB_ControlBoardId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row ControlBoard%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM ControlBoard WHERE
			ControlBoardId = fkVal1
		for update of
			ControlBoardId;
begin
	open c1(:new.CB_ControlBoardId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_4E555281
after update of ControlBoardId on ControlBoard
referencing OLD as old NEW as new
for each row
begin
	UPDATE DistributionBoard
	SET
		DistributionBoard.CB_ControlBoardId = :new.ControlBoardId
	WHERE
		DistributionBoard.CB_ControlBoardId = :old.ControlBoardId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_4E555281
after delete on ControlBoard
for each row
begin
	DELETE FROM DistributionBoard WHERE
		DistributionBoard.CB_ControlBoardId = :OLD.ControlBoardId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_334CBC7
before insert or update on ChamberStrip
referencing OLD as old NEW as new
for each row when (
	new.FEB_FEBId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row FEB%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM FEB WHERE
			FEBId = fkVal1
		for update of
			FEBId;
begin
	open c1(:new.FEB_FEBId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_Feb_FEB_FK1
after update of FEBId on FEB
referencing OLD as old NEW as new
for each row
begin
	UPDATE ChamberStrip
	SET
		ChamberStrip.FEB_FEBId = :new.FEBId
	WHERE
		ChamberStrip.FEB_FEBId = :old.FEBId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_Feb_FEB_FK1
after delete on FEB
for each row
begin
	DELETE FROM ChamberStrip WHERE
		ChamberStrip.FEB_FEBId = :OLD.FEBId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_86DB6606
before insert or update on GasLine
referencing OLD as old NEW as new
for each row when (
	new.GD_GasDistributorId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row GasDistributor%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM GasDistributor WHERE
			GasDistributorId = fkVal1
		for update of
			GasDistributorId;
begin
	open c1(:new.GD_GasDistributorId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_B3136664
after update of GasDistributorId on GasDistributor
referencing OLD as old NEW as new
for each row
begin
	UPDATE GasLine
	SET
		GasLine.GD_GasDistributorId = :new.GasDistributorId
	WHERE
		GasLine.GD_GasDistributorId = :old.GasDistributorId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_B3136664
after delete on GasDistributor
for each row
begin
	DELETE FROM GasLine WHERE
		GasLine.GD_GasDistributorId = :OLD.GasDistributorId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_337A307
before insert or update on Chamber
referencing OLD as old NEW as new
for each row when (
	new.Location_LocationId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row Location%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM Location WHERE
			LocationId = fkVal1
		for update of
			LocationId;
begin
	open c1(:new.Location_LocationId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_6A62307
after update of LocationId on Location
referencing OLD as old NEW as new
for each row
begin
	UPDATE Chamber
	SET
		Chamber.Location_LocationId = :new.LocationId
	WHERE
		Chamber.Location_LocationId = :old.LocationId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_6A62307
after delete on Location
for each row
begin
	DELETE FROM Chamber WHERE
		Chamber.Location_LocationId = :OLD.LocationId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_337ED97
before insert or update on Chamber
referencing OLD as old NEW as new
for each row when (
	new.GasLine_GasLineId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row GasLine%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM GasLine WHERE
			GasLineId = fkVal1
		for update of
			GasLineId;
begin
	open c1(:new.GasLine_GasLineId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_61B4D97
after update of GasLineId on GASLine
referencing OLD as old NEW as new
for each row
begin
	UPDATE Chamber
	SET
		Chamber.GasLine_GasLineId = :new.GasLineId
	WHERE
		Chamber.GasLine_GasLineId = :old.GasLineId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_61B4D97
after delete on GasLine
for each row
begin
	DELETE FROM Chamber WHERE
		Chamber.GasLine_GasLineId = :OLD.GasLineId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_19BE3757
before insert or update on Chamber
referencing OLD as old NEW as new
for each row when (
	new.ChamberType_ChamberTypeId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row ChamberType%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM ChamberType WHERE
			ChamberTypeId = fkVal1
		for update of
			ChamberTypeId;
begin
	open c1(:new.ChamberType_ChamberTypeId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_9A773756
after update of ChamberTypeId on ChamberType
referencing OLD as old NEW as new
for each row
begin
	UPDATE Chamber
	SET
		Chamber.ChamberType_ChamberTypeId = :new.ChamberTypeId
	WHERE
		Chamber.ChamberType_ChamberTypeId = :old.ChamberTypeId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_9A773756
after delete on ChamberType
for each row
begin
	DELETE FROM Chamber WHERE
		Chamber.ChamberType_ChamberTypeId = :OLD.ChamberTypeId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_Feb_Chamber_FK1
before insert or update on FEB
referencing OLD as old NEW as new
for each row when (
	new.Chamber_ChamberId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row Chamber%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM Chamber WHERE
			ChamberId = fkVal1
		for update of
			ChamberId;
begin
	open c1(:new.Chamber_ChamberId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_19BE3C7
after update of ChamberId on Chamber
referencing OLD as old NEW as new
for each row
begin
	UPDATE FEB
	SET
		FEB.Chamber_ChamberId = :new.ChamberId
	WHERE
		FEB.Chamber_ChamberId = :old.ChamberId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_19BE3C7
after delete on Chamber
for each row
begin
	DELETE FROM FEB WHERE
		FEB.Chamber_ChamberId = :OLD.ChamberId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_Feb_LinkBoard_FK1
before insert or update on FEB
referencing OLD as old NEW as new
for each row when (
	new.LB_LinkBoardId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row LinkBoard%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM LinkBoard WHERE
			LinkBoardId = fkVal1
		for update of
			LinkBoardId;
begin
	open c1(:new.LB_LinkBoardId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_1A826087
after update of LinkBoardId on LinkBoard
referencing OLD as old NEW as new
for each row
begin
	UPDATE FEB
	SET
		FEB.LB_LinkBoardId = :new.LinkBoardId
	WHERE
		FEB.LB_LinkBoardId = :old.LinkBoardId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_1A826087
after delete on LinkBoard
for each row
begin
	DELETE FROM FEB WHERE
		FEB.LB_LinkBoardId = :OLD.LinkBoardId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_6199AE87
before insert or update on FEB
referencing OLD as old NEW as new
for each row when (
	new.DB_DistributionBoardId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row DistributionBoard%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM DistributionBoard WHERE
			DistributionBoardId = fkVal1
		for update of
			DistributionBoardId;
begin
	open c1(:new.DB_DistributionBoardId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_3A99B761
after update of DistributionBoardId on DistributionBoard
referencing OLD as old NEW as new
for each row
begin
	UPDATE FEB
	SET
		FEB.DB_DistributionBoardId = :new.DistributionBoardId
	WHERE
		FEB.DB_DistributionBoardId = :old.DistributionBoardId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_3A99B761
after delete on DistributionBoard
for each row
begin
	DELETE FROM FEB WHERE
		FEB.DB_DistributionBoardId = :OLD.DistributionBoardId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_HOBOARD_Board_FK10
before insert or update on HOBOARD
referencing OLD as old NEW as new
for each row when (
	new.HOBoardId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row Board%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM Board WHERE
			BoardId = fkVal1
		for update of
			BoardId;
begin
	open c1(:new.HOBoardId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_33513E
after update of BoardId on Board
referencing OLD as old NEW as new
for each row
begin
	UPDATE HOBOARD
	SET
		HOBOARD.HOBoardId = :new.BoardId
	WHERE
		HOBOARD.HOBoardId = :old.BoardId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_33513E
after delete on Board
for each row
begin
	DELETE FROM HOBOARD WHERE
		HOBOARD.HOBoardId = :OLD.BoardId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_6A2CFE87
before insert or update on LinkConn
referencing OLD as old NEW as new
for each row when (
	new.TB_TriggerBoardId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row TriggerBoard%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM TriggerBoard WHERE
			TriggerBoardId = fkVal1
		for update of
			TriggerBoardId;
begin
	open c1(:new.TB_TriggerBoardId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_FBD8FE80
after update of TriggerBoardId on TriggerBoard
referencing OLD as old NEW as new
for each row
begin
	UPDATE LinkConn
	SET
		LinkConn.TB_TriggerBoardId = :new.TriggerBoardId
	WHERE
		LinkConn.TB_TriggerBoardId = :old.TriggerBoardId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_FBD8FE80
after delete on TriggerBoard
for each row
begin
	DELETE FROM LinkConn WHERE
		LinkConn.TB_TriggerBoardId = :OLD.TriggerBoardId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_Linkconn_Board_FK11
before insert or update on LinkConn
referencing OLD as old NEW as new
for each row when (
	new.Board_BoardId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row Board%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM Board WHERE
			BoardId = fkVal1
		for update of
			BoardId;
begin
	open c1(:new.Board_BoardId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_33513F
after update of BoardId on Board
referencing OLD as old NEW as new
for each row
begin
	UPDATE LinkConn
	SET
		LinkConn.Board_BoardId = :new.BoardId
	WHERE
		LinkConn.Board_BoardId = :old.BoardId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_33513F
after delete on Board
for each row
begin
	DELETE FROM LinkConn WHERE
		LinkConn.Board_BoardId = :OLD.BoardId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_Linkboard_Board_FK9
before insert or update on LinkBoard
referencing OLD as old NEW as new
for each row when (
	new.LinkBoardId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row Board%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM Board WHERE
			BoardId = fkVal1
		for update of
			BoardId;
begin
	open c1(:new.LinkBoardId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_19A88F
after update of BoardId on Board
referencing OLD as old NEW as new
for each row
begin
	UPDATE LinkBoard
	SET
		LinkBoard.LinkBoardId = :new.BoardId
	WHERE
		LinkBoard.LinkBoardId = :old.BoardId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_19A88F
after delete on Board
for each row
begin
	DELETE FROM LinkBoard WHERE
		LinkBoard.LinkBoardId = :OLD.BoardId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_F01A88E
before insert or update on SPLITTERBoard
referencing OLD as old NEW as new
for each row when (
	new.SplitterBoardId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row Board%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM Board WHERE
			BoardId = fkVal1
		for update of
			BoardId;
begin
	open c1(:new.SplitterBoardId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_19A88E
after update of BoardId on Board
referencing OLD as old NEW as new
for each row
begin
	UPDATE SPLITTERBoard
	SET
		SPLITTERBoard.SplitterBoardId = :new.BoardId
	WHERE
		SPLITTERBoard.SplitterBoardId = :old.BoardId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_19A88E
after delete on Board
for each row
begin
	DELETE FROM SPLITTERBoard WHERE
		SPLITTERBoard.SplitterBoardId = :OLD.BoardId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_FF7A881
before insert or update on TriggerBoard
referencing OLD as old NEW as new
for each row when (
	new.TriggerBoardId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row Board%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM Board WHERE
			BoardId = fkVal1
		for update of
			BoardId;
begin
	open c1(:new.TriggerBoardId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_19A881
after update of BoardId on Board
referencing OLD as old NEW as new
for each row
begin
	UPDATE TriggerBoard
	SET
		TriggerBoard.TriggerBoardId = :new.BoardId
	WHERE
		TriggerBoard.TriggerBoardId = :old.BoardId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_19A881
after delete on Board
for each row
begin
	DELETE FROM TriggerBoard WHERE
		TriggerBoard.TriggerBoardId = :OLD.BoardId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_7FBDBC87
before insert or update on TriggerBoard
referencing OLD as old NEW as new
for each row when (
	new.DCCBoard_DCCBoardId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row DCCBoard%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM DCCBoard WHERE
			DCCBoardId = fkVal1
		for update of
			DCCBoardId;
begin
	open c1(:new.DCCBoard_DCCBoardId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_659BC87
after update of DCCBoardId on DCCBoard
referencing OLD as old NEW as new
for each row
begin
	UPDATE TriggerBoard
	SET
		TriggerBoard.DCCBoard_DCCBoardId = :new.DCCBoardId
	WHERE
		TriggerBoard.DCCBoard_DCCBoardId = :old.DCCBoardId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_659BC87
after delete on DCCBoard
for each row
begin
	DELETE FROM TriggerBoard WHERE
		TriggerBoard.DCCBoard_DCCBoardId = :OLD.DCCBoardId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_Dccboard_Board_FK6
before insert or update on DCCBoard
referencing OLD as old NEW as new
for each row when (
	new.DCCBoardId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row Board%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM Board WHERE
			BoardId = fkVal1
		for update of
			BoardId;
begin
	open c1(:new.DCCBoardId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_19A880
after update of BoardId on Board
referencing OLD as old NEW as new
for each row
begin
	UPDATE DCCBoard
	SET
		DCCBoard.DCCBoardId = :new.BoardId
	WHERE
		DCCBoard.DCCBoardId = :old.BoardId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_19A880
after delete on Board
for each row
begin
	DELETE FROM DCCBoard WHERE
		DCCBoard.DCCBoardId = :OLD.BoardId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_725A883
before insert or update on SorterBoard
referencing OLD as old NEW as new
for each row when (
	new.SorterBoardId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row Board%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM Board WHERE
			BoardId = fkVal1
		for update of
			BoardId;
begin
	open c1(:new.SorterBoardId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_19A883
after update of BoardId on Board
referencing OLD as old NEW as new
for each row
begin
	UPDATE SorterBoard
	SET
		SorterBoard.SorterBoardId = :new.BoardId
	WHERE
		SorterBoard.SorterBoardId = :old.BoardId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_19A883
after delete on Board
for each row
begin
	DELETE FROM SorterBoard WHERE
		SorterBoard.SorterBoardId = :OLD.BoardId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_7512C82
before insert or update on TCBackPlane
referencing OLD as old NEW as new
for each row when (
	new.TCBackPlaneId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row Board%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM Board WHERE
			BoardId = fkVal1
		for update of
			BoardId;
begin
	open c1(:new.TCBackPlaneId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_19A882
after update of BoardId on Board
referencing OLD as old NEW as new
for each row
begin
	UPDATE TCBackPlane
	SET
		TCBackPlane.TCBackPlaneId = :new.BoardId
	WHERE
		TCBackPlane.TCBackPlaneId = :old.BoardId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_19A882
after delete on Board
for each row
begin
	DELETE FROM TCBackPlane WHERE
		TCBackPlane.TCBackPlaneId = :OLD.BoardId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_C9CA885
before insert or update on ControlBoard
referencing OLD as old NEW as new
for each row when (
	new.ControlBoardId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row Board%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM Board WHERE
			BoardId = fkVal1
		for update of
			BoardId;
begin
	open c1(:new.ControlBoardId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_19A885
after update of BoardId on Board
referencing OLD as old NEW as new
for each row
begin
	UPDATE ControlBoard
	SET
		ControlBoard.ControlBoardId = :new.BoardId
	WHERE
		ControlBoard.ControlBoardId = :old.BoardId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_19A885
after delete on Board
for each row
begin
	DELETE FROM ControlBoard WHERE
		ControlBoard.ControlBoardId = :OLD.BoardId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_64E5A487
before insert or update on ControlBoard
referencing OLD as old NEW as new
for each row when (
	new.CCSBoard_CCSBoardId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row CCSBoard%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM CCSBoard WHERE
			CCSBoardId = fkVal1
		for update of
			CCSBoardId;
begin
	open c1(:new.CCSBoard_CCSBoardId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_669A487
after update of CCSBoardId on CCSBoard
referencing OLD as old NEW as new
for each row
begin
	UPDATE ControlBoard
	SET
		ControlBoard.CCSBoard_CCSBoardId = :new.CCSBoardId
	WHERE
		ControlBoard.CCSBoard_CCSBoardId = :old.CCSBoardId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_669A487
after delete on CCSBoard
for each row
begin
	DELETE FROM ControlBoard WHERE
		ControlBoard.CCSBoard_CCSBoardId = :OLD.CCSBoardId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_Ccsboard_Board_FK2
before insert or update on CCSBoard
referencing OLD as old NEW as new
for each row when (
	new.CCSBoardId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row Board%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM Board WHERE
			BoardId = fkVal1
		for update of
			BoardId;
begin
	open c1(:new.CCSBoardId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_19A884
after update of BoardId on Board
referencing OLD as old NEW as new
for each row
begin
	UPDATE CCSBoard
	SET
		CCSBoard.CCSBoardId = :new.BoardId
	WHERE
		CCSBoard.CCSBoardId = :old.BoardId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_19A884
after delete on Board
for each row
begin
	DELETE FROM CCSBoard WHERE
		CCSBoard.CCSBoardId = :OLD.BoardId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_Chip_Board_FK1
before insert or update on Chip
referencing OLD as old NEW as new
for each row when (
	new.Board_BoardId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row Board%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM Board WHERE
			BoardId = fkVal1
		for update of
			BoardId;
begin
	open c1(:new.Board_BoardId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_19A887
after update of BoardId on Board
referencing OLD as old NEW as new
for each row
begin
	UPDATE Chip
	SET
		Chip.Board_BoardId = :new.BoardId
	WHERE
		Chip.Board_BoardId = :old.BoardId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_19A887
after delete on Board
for each row
begin
	DELETE FROM Chip WHERE
		Chip.Board_BoardId = :OLD.BoardId;
end;
/
--Before Update/Insert Validate Foreign Key.
create trigger ValidateFK_Board_Crate_FK1
before insert or update on Board
referencing OLD as old NEW as new
for each row when (
	new.Crate_CrateId IS NOT NULL )
declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row Crate%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM Crate WHERE
			CrateId = fkVal1
		for update of
			CrateId;
begin
	open c1(:new.Crate_CrateId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/
-- Trigger: Cascade updates to child table.
create trigger OnUpdateCascade_1A73D7
after update of CrateId on Crate
referencing OLD as old NEW as new
for each row
begin
	UPDATE Board
	SET
		Board.Crate_CrateId = :new.CrateId
	WHERE
		Board.Crate_CrateId = :old.CrateId;
end;
/
-- Trigger: Cascade deletes to child table.
create trigger OnDeleteCascade_1A73D7
after delete on Crate
for each row
begin
	DELETE FROM Board WHERE
		Board.Crate_CrateId = :OLD.CrateId;
end;
/
REM
REM End Cascade on Updates and Deletes for Foreign Keys 
REM
