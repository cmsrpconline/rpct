Rem
REM Start Create Tables
REM

-- Create new table BACKPLANESORTERCONN.
-- BACKPLANESORTERCONN : Table of BackPlaneSorterConn
-- 	BACKPLANESORTERCONNID : BackPlaneSorterConnId identifies BackPlaneSorterConn
-- 	TCBP_TCBACKPLANEID : TCBP_TCBackPlaneId is of BackPlaneSorterConn
-- 	SORTERBOARD_SORTERBOARDID : SorterBoard_SorterBoardId is of BackPlaneSorterConn
-- 	SORTERBOARDCONNECTOR : SorterBoardConnector is of BackPlaneSorterConn
CREATE TABLE BACKPLANESORTERCONN (
	BACKPLANESORTERCONNID NUMBER(11,0) not null,
	TCBP_TCBACKPLANEID NUMBER(11,0) not null,
	SORTERBOARD_SORTERBOARDID NUMBER(11,0) not null,
	SORTERBOARDCONNECTOR NUMBER(11,0) null);

-- Create new table DISTRIBUTIONBOARD.
-- DISTRIBUTIONBOARD : Table of DistributionBoard
-- 	DISTRIBUTIONBOARDID : DistributionBoardId identifies DistributionBoard
-- 	CB_CONTROLBOARDID : CB_ControlBoardId is of DistributionBoard
-- 	PRODUCTID : ProductId is of DistributionBoard
-- 	DATEOFINSTALLATION : DateOfInstallation is of DistributionBoard
CREATE TABLE DISTRIBUTIONBOARD (
	DISTRIBUTIONBOARDID NUMBER(11,0) not null,
	CB_CONTROLBOARDID NUMBER(11,0) not null,
	PRODUCTID VARCHAR2(30) null,
	DATEOFINSTALLATION DATE null);

-- Create new table CHAMBERSTRIP.
-- CHAMBERSTRIP : Table of ChamberStrip
-- 	CHAMBERSTRIPID : ChamberStripId identifies ChamberStrip
-- 	FEB_FEBID : FEB_FEBId is of ChamberStrip
-- 	CABLEPINNUMBER : CablePinNumber is of ChamberStrip
-- 	CHAMBERSTRIPNUMBER : ChamberStripNumber is of ChamberStrip. Strip number when "Chamber on a table" 1-96
-- 	CMSSTRIPNUMBER : CMSStripNumber is of ChamberStrip. Strip number when chamber in CMS.  1 - 96
-- 	CMSGLOBALSTRIPNUMBER : CMSGlobalStripNumber is of ChamberStrip. Global CMS strip number, clocwise, 0 - 1151 (or 1 -1152) ???
CREATE TABLE CHAMBERSTRIP (
	CHAMBERSTRIPID NUMBER(11,0) not null,
	FEB_FEBID NUMBER(11,0) not null,
	CABLEPINNUMBER NUMBER(3,0) null,
	CHAMBERSTRIPNUMBER NUMBER(6,0) null,
	CMSSTRIPNUMBER NUMBER(6,0) null,
	CMSGLOBALSTRIPNUMBER NUMBER(6,0) null);

-- Create new table LOCATION.
-- LOCATION : Table of Location
-- 	LOCATIONID : LocationId identifies Location
-- 	DISKORWHEEL : DiskOrWheel is of Location
-- 	LAYER : Layer is of Location
-- 	SECTOR : Sector is of Location
-- 	SUBSECTOR : Subsector is of Location
CREATE TABLE LOCATION (
	LOCATIONID NUMBER(11,0) not null,
	DISKORWHEEL VARCHAR2(30) null,
	LAYER VARCHAR2(30) null,
	SECTOR VARCHAR2(30) null,
	SUBSECTOR VARCHAR2(30) null);

-- Create new table GASDISTRIBUTOR.
-- GASDISTRIBUTOR : Table of GasDistributor
-- 	GASDISTRIBUTORID : GasDistributorId identifies GasDistributor
-- 	PRODUCTID : ProductId is of GasDistributor
CREATE TABLE GASDISTRIBUTOR (
	GASDISTRIBUTORID NUMBER(11,0) not null,
	PRODUCTID VARCHAR2(30) null );

-- Create new table GASLINE.
-- GASLINE : Table of GASLine
-- 	GASLINEID : GasLineId identifies GASLine
-- 	GD_GASDISTRIBUTORID : GD_GasDistributorId is of GASLine
-- 	PRODUCTID : ProductId is of GASLine
CREATE TABLE GASLINE (
	GASLINEID NUMBER(11,0) not null,
	GD_GASDISTRIBUTORID NUMBER(11,0) not null,
	PRODUCTID VARCHAR2(30) null);

-- Create new table CHAMBER.
-- CHAMBER : Table of Chamber
-- 	CHAMBERID : ChamberId identifies Chamber
-- 	LOCATION_LOCATIONID : Location_LocationId partly identifies Chamber
-- 	GASLINE_GASLINEID : GasLine_GasLineId is of Chamber
-- 	CHAMBERTYPE_CHAMBERTYPEID : ChamberType_ChamberTypeId is of Chamber
-- 	GASLINECONNECTION : GasLineConnection is of Chamber
-- 	PRODUCTID : ProductId is of Chamber
-- 	DATEOFINSTALLATION : DateOfInstallation is of Chamber
-- 	STRIPORDERSCHEMA : StripOrderSchema is of Chamber
CREATE TABLE CHAMBER (
	CHAMBERID NUMBER(11,0) not null,
	LOCATION_LOCATIONID NUMBER(11,0) not null,
	GASLINE_GASLINEID NUMBER(11,0) not null,
	CHAMBERTYPE_CHAMBERTYPEID NUMBER(11,0) not null,
	GASLINECONNECTION VARCHAR2(30) null,
	PRODUCTID VARCHAR2(30) null,
	DATEOFINSTALLATION DATE null,
	STRIPORDERSCHEMA NUMBER(2,0) null);

-- Create new table FEB.
-- FEB : Table of FEB
-- 	FEBID : FEBId identifies FEB
-- 	CHAMBER_CHAMBERID : Chamber_ChamberId is of FEB
-- 	LB_LINKBOARDID : LB_LinkBoardId is of FEB
-- 	DB_DISTRIBUTIONBOARDID : DB_DistributionBoardId is of FEB
-- 	PRODUCTID : ProductId is of FEB
-- 	FEBROW : FEBRow is of FEB. "Chamber on a table", F or M or B (Barrel); A or B or C (endcap) ???
-- 	FEBPOSITIONINROW : FEBPositionInRow is of FEB. "Chamber on a table"
-- 	I2CADDRESS : I2CAddress is of FEB
-- 	DATEOFINSTALLATION : DateOfInstallation is of FEB
-- 	ROLLNUMBER : RollNumber is of FEB. CMS global Roll Number: -17...17 (eta)
-- 	FEBCMSROW : FEBLogicalPosition is of FEB. Chmaber in CMS, row position in eta 0 or 1 (or 2 if ref palne)
CREATE TABLE FEB (
	FEBID NUMBER(11,0) not null,
	CHAMBER_CHAMBERID NUMBER(11,0) not null,
	LB_LINKBOARDID NUMBER(11,0) not null,
	DB_DISTRIBUTIONBOARDID NUMBER(11,0) not null,
	PRODUCTID VARCHAR2(30) null,
	FEBROW NUMBER(2,0) null,
	FEBPOSITIONINROW NUMBER(2,0) null,
	I2CADDRESS VARCHAR2(30) null,
	DATEOFINSTALLATION DATE null,
	ROLLNUMBER NUMBER(2,0) null,
	FEBCMSROW NUMBER(2,0) null);

-- Create new table HOBOARD.
-- HOBOARD : Table of HOBOARD
-- 	HOBOARDID : HOBoardId partly identifies HOBOARD
-- 	LOCATION : Location is of HOBOARD
CREATE TABLE HOBOARD (
	HOBOARDID NUMBER(11,0) not null,
	LOCATION VARCHAR2(30) null);

-- Create new table LINKCONN.
-- LINKCONN : Table of LinkConn
-- 	LINKCONNID : LINKCONNID identifies LinkConn
-- 	TB_TRIGGERBOARDID : TB_TriggerBoardId is of LinkConn
-- 	BOARD_BOARDID : Board_BoardId is of LinkConn. LinkBoard or H0Device
-- 	TRIGGERBOARDINPUTNUM : TriggerBoardInputNum is of LinkConn
CREATE TABLE LINKCONN (
	LINKCONNID NUMBER(11,0) not null,
	TB_TRIGGERBOARDID NUMBER(11,0) not null,
	BOARD_BOARDID NUMBER(11,0) not null,
	TRIGGERBOARDINPUTNUM NUMBER(11,0) null);

-- Create new table LINKBOARD.
-- LINKBOARD : Table of LinkBoard
-- 	LINKBOARDID : LinkBoardId identifies LinkBoard
-- 	SLAVEORMASTER : SlaveOrMaster is of LinkBoard
-- 	STRIPINV : StripInv is of LinkBoard
CREATE TABLE LINKBOARD (
	LINKBOARDID NUMBER(11,0) not null,
	SLAVEORMASTER VARCHAR2(30) null,
	STRIPINV VARCHAR2(30) null);

-- Create new table SPLITTERBOARD.
-- SPLITTERBOARD : Table of SPLITTERBoard
-- 	SPLITTERBOARDID : SplitterBoardId identifies SPLITTERBoard
CREATE TABLE SPLITTERBOARD (
	SPLITTERBOARDID NUMBER(11,0) not null);

-- Create new table TRIGGERBOARD.
-- TRIGGERBOARD : Table of TriggerBoard
-- 	TRIGGERBOARDID : TriggerBoardId identifies TriggerBoard
-- 	DCCBOARD_DCCBOARDID : DCCBoard_DCCBoardId is of TriggerBoard
-- 	DCCINPUTCHANNELNUM : DCCInputChannelNum is of TriggerBoard
-- 	DCCINPUTCONNNUM : DCCInputConnNum is of TriggerBoard
-- 	TOWERTO : TowerTo is of TriggerBoard
-- 	TOWERFROM : TowerFrom is of TriggerBoard
-- 	SECTOR : Sector is of TriggerBoard
-- 	LOGICALASSIGNMENT : LogicalAssignment is of TriggerBoard ???????
CREATE TABLE TRIGGERBOARD (
	TRIGGERBOARDID NUMBER(11,0) not null,
	DCCBOARD_DCCBOARDID NUMBER(11,0) not null,
	DCCINPUTCHANNELNUM NUMBER(11,0) null,
	DCCINPUTCONNNUM NUMBER(11,0) null,
	TOWERTO VARCHAR2(30) null,
	TOWERFROM VARCHAR2(30) null,
	SECTOR VARCHAR2(30) null,
	LOGICALASSIGNMENT NUMBER(11,0) null);

-- Create new table DCCBOARD.
-- DCCBOARD : Table of DCCBoard
-- 	DCCBOARDID : DCCBoardId identifies DCCBoard
CREATE TABLE DCCBOARD (
	DCCBOARDID NUMBER(11,0) not null);

-- Create new table SORTERBOARD.
-- SORTERBOARD : Table of SorterBoard
-- 	SORTERBOARDID : SorterBoardId identifies SorterBoard
-- 	LOGICALASSIGNMENT : LogicalAssignment is of SorterBoard
-- 	SORTERTYPE : SorterType is of SorterBoard. Half Sorter or Final Sorter
CREATE TABLE SORTERBOARD (
	SORTERBOARDID NUMBER(11,0) not null,
	LOGICALASSIGNMENT NUMBER(11,0) null,
	SORTERTYPE VARCHAR2(30) null);

-- Create new table TCBACKPLANE.
-- TCBACKPLANE : Table of TCBackPlane
-- 	TCBACKPLANEID : TCBackPlaneId identifies TCBackPlane
-- 	SECTOR : Sector is of TCBackPlane
CREATE TABLE TCBACKPLANE (
	TCBACKPLANEID NUMBER(11,0) not null,
	SECTOR VARCHAR2(30) null);

-- Create new table CONTROLBOARD.
-- CONTROLBOARD : Table of ControlBoard
-- 	CONTROLBOARDID : ControlBoardId identifies ControlBoard
-- 	CCSBOARD_CCSBOARDID : CCSBoard_CCSBoardId partly identifies ControlBoard
-- 	CCSBINPUTCONNECTOR : CCSBInputConnector is of ControlBoard
-- 	POSITIONINCCUCHAIN : PositionInCCUChain is of ControlBoard
-- 	REARPANELCHANNEL : RearPanelChannel is of ControlBoard
CREATE TABLE CONTROLBOARD (
	CONTROLBOARDID NUMBER(11,0) not null,
	CCSBOARD_CCSBOARDID NUMBER(11,0) not null,
	CCSBINPUTCONNECTOR VARCHAR2(30) null,
	POSITIONINCCUCHAIN VARCHAR2(30) null,
	REARPANELCHANNEL VARCHAR2(30) null);

-- Create new table CCSBOARD.
-- CCSBOARD : Table of CCSBoard
-- 	CCSBOARDID : CCSBoardId identifies CCSBoard
CREATE TABLE CCSBOARD (
	CCSBOARDID NUMBER(11,0) not null);

-- Create new table CHIP.
-- CHIP : Table of Chip
-- 	CHIPID : ChipId identifies Chip
-- 	BOARD_BOARDID : Board_BoardId partly identifies Chip
-- 	TYPE : Type is of Chip
-- 	"POSITION" : Position is of Chip
CREATE TABLE CHIP (
	CHIPID NUMBER(11,0) not null,
	BOARD_BOARDID NUMBER(11,0) not null,
	TYPE VARCHAR2(30) null,
	POSITION NUMBER(2,0) null);

-- Create new table CRATE.
-- CRATE : Table of Crate
-- 	CRATEID : CrateId identifies Crate
-- 	RACKNUMBER : RackNumber is of Crate
-- 	LOCATION : Location is of Crate
-- 	TYPE : Type is of Crate
-- 	PRODUCTID : ProductId is of Crate
CREATE TABLE CRATE (
	CRATEID NUMBER(11,0) not null,
	RACKNUMBER NUMBER(11,0) null,
	LOCATION VARCHAR2(30) null,
	TYPE VARCHAR2(30) null,
	PRODUCTID VARCHAR2(30) null);

-- Create new table BOARD.
-- BOARD : Table of Board
-- 	BOARDID : BoardId identifies Board
-- 	CRATE_CRATEID : Crate_CrateId partly identifies Board
-- 	CRATESLOT : CrateSlot is of Board
-- 	TYPE : Type is of Board
-- 	PRODUCTID : ProductId is of Board
-- 	DATEOFINSTALLATION : DateOfInstallation is of Board
CREATE TABLE BOARD (
	BOARDID NUMBER(11,0) not null,
	CRATE_CRATEID NUMBER(11,0) null,
	CRATESLOT NUMBER(3,0) null,
	TYPE VARCHAR2(30) null,
	PRODUCTID VARCHAR2(30) null,
	DATEOFINSTALLATION DATE null);

-- Create new table CHAMBERTYPE.
-- CHAMBERTYPE : Table of ChamberType
-- 	CHAMBERTYPEID : ChamberTypeId identifies ChamberType
-- 	TYPENAME : TypeName is of ChamberType
-- 	STRIPSCNT : StripsCnt is of ChamberType
-- 	FEBCNT : FebCnt is of ChamberType
-- 	FEBROWSCNT : FebRowsCnt is of ChamberType
-- 	FEBSINROWCNT : FebsInRowCnt is of ChamberType
-- 	BARRELORFORWARD : BarrelOrForward is of ChamberType
CREATE TABLE CHAMBERTYPE (
	CHAMBERTYPEID NUMBER(11,0) not null,
	TYPENAME VARCHAR2(30) null,
	STRIPSCOUNT NUMBER(3,0) null,
	FEBCOUNT NUMBER(2,0) null,
	FEBROWSCOUNT NUMBER(2,0) null,
	FEBSINROWCOUNT NUMBER(2,0) null,
	BARRELORFORWARD VARCHAR2(30) null);
REM
REM End Create Tables
REM
