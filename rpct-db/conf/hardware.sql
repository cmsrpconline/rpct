
CONNECT TO RPCTLINK;

CREATE TABLE H_HARDWARE_ITEM ( 
  ID INTEGER NOT NULL PRIMARY KEY, 
  NAME VARCHAR(255) NOT NULL UNIQUE,
  PRODUCTION_DATE DATE,
  NOTES VARCHAR(1024)
)
;

CREATE TABLE H_LINK_BOX ( 
  HARDWARE_ITEM_ID INTEGER NOT NULL PRIMARY KEY REFERENCES H_HARDWARE_ITEM(ID)
)
;

CREATE TABLE H_LINK_BOARD ( 
  HARDWARE_ITEM_ID INTEGER NOT NULL PRIMARY KEY REFERENCES H_HARDWARE_ITEM(ID)
)
;
