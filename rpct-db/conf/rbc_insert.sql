  set serveroutput on

-- Delete configuration
  delete from chipconfassignment
  where chip_chipid in (
    select chipid from chip
    where chip.type = 'RBC'
    )
  ;
  
  delete from staticconfiguration
  where staticconfigurationid in (
    select RBCCHIPSTATICCONFID
    from rbcchipconf
    )
  ;

  delete from chip 
  where chip.type = 'RBC';
  
  drop table rbcchipconf;
  
  
-- Delete RBC hierarchy   
  DROP TABLE "RBCBOARD";
  
  DELETE FROM "CHIP"
  WHERE "TYPE"='RBC';

  DROP TRIGGER "ONDELETECASCADE_BOARD_RBC";
  
  DELETE FROM "BOARD"
  WHERE "TYPE" IN ('RBCBOARD', 'RBCFAKEBOARD');
  
-- Crate RBC hierarchy
  CREATE TABLE "RBCBOARD" 
   ( "RBCBOARDID" NUMBER(11,0),
     "MASTERID" NUMBER(11,0),
     "I2CCHANNEL" NUMBER(11,0), 
	 CONSTRAINT "RBCBOARD_PK" PRIMARY KEY ("RBCBOARDID") ENABLE,
	 CONSTRAINT "BOARD_FK13" FOREIGN KEY
	    ("RBCBOARDID") REFERENCES "BOARD" ("BOARDID") ENABLE,
	 CONSTRAINT "RBCBOARD_FK1" FOREIGN KEY
	    ("MASTERID") REFERENCES "RBCBOARD" ("RBCBOARDID") ENABLE
   ) ;

   CREATE INDEX "RBCBOARD_IDX1" ON "RBCBOARD" ("MASTERID");

-- Fill BOARD   
-- Fill RBCBOARD
-- Fill CHIP
   DECLARE
     RBC_NUMBER_OF_BOARDS constant NUMBER := 30;
     
     /* BOARD */
     TYPE IdList IS VARRAY(30) OF BOARD.BOARDID%TYPE;
     TYPE CIdList IS VARRAY(30) OF BOARD.CRATE_CRATEID%TYPE;
     TYPE NameList IS VARRAY(30) OF BOARD.NAME%TYPE;
     TYPE LabelList IS VARRAY(30) OF BOARD.LABEL%TYPE;
     ids IdList;
     cids CIdList;
     names NameList;
     labels LabelList;
     bType constant BOARD.TYPE%TYPE := 'RBCBOARD';
     bPosition constant BOARD.POSITION%TYPE := 12;

     /* RBC_FAKE */
     idsFake IdList;
     cidsFake CIdList;
     namesFake NameList;
     labelsFake LabelList;
     bTypeFake constant BOARD.TYPE%TYPE := 'RBCFAKEBOARD';
     bPositionFake constant BOARD.POSITION%TYPE := 12;
     
     boardidMax BOARD.BOARDID%TYPE;

     /* RBCBOARD */
     TYPE MasterIdList IS VARRAY(30) OF RBCBOARD.MASTERID%TYPE;
     masterids MasterIdList;
	 i2cChannel constant RBCBOARD.I2CCHANNEL%TYPE := 8;
     /* FAKE */
	 masteridsFake MasterIdList;
	 i2cChannelFake constant RBCBOARD.I2CCHANNEL%TYPE := 0;
     
     /* CHIP */
     TYPE ChipIdList IS VARRAY(30) OF CHIP.CHIPID%TYPE;
     chipids ChipIdList;
     
     chipType constant CHIP.TYPE%TYPE := 'RBC';
     chipPosition constant CHIP.POSITION%TYPE := 0;
     chipidMax CHIP.CHIPID%TYPE;
   BEGIN
     /* BOARD */

     -- Append to the end of BOARD
	 select max(boardid) into boardidMax from board;

	 ids := IdList();
     ids.EXTEND(RBC_NUMBER_OF_BOARDS);
	 idsFake := IdList();
     idsFake.EXTEND(RBC_NUMBER_OF_BOARDS);

     FOR i IN 1 .. RBC_NUMBER_OF_BOARDS
	 LOOP
	 	ids(i) := boardidMax + 2*i - 1;
	 	idsFake(i) := boardidMax + 2*i;
	 END LOOP;

	 cids := CIdList(49, /* LBB_RB-2_S1 */
                     51, /* LBB_RB-2_S3 */
                     53, /* LBB_RB-2_S5 */
                     55, /* LBB_RB-2_S7 */
                     57, /* LBB_RB-2_S9 */
                     59, /* LBB_RB-2_S11 */
                     61, /* LBB_RB-1_S1 */
                     63, /* LBB_RB-1_S3 */
                     65, /* LBB_RB-1_S5 */
                     67, /* LBB_RB-1_S7 */
                     69, /* LBB_RB-1_S9 */
                     71, /* LBB_RB-1_S11 */
                     73, /* LBB_RB0_S1 */
                     75, /* LBB_RB0_S3 */
                     77, /* LBB_RB0_S5 */
                     79, /* LBB_RB0_S7 */
                     81, /* LBB_RB0_S9 */
                     83, /* LBB_RB0_S11 */
                     85, /* LBB_RB+1_S1 */
                     87, /* LBB_RB+1_S3 */
                     89, /* LBB_RB+1_S5 */
                     91, /* LBB_RB+1_S7 */
                     93, /* LBB_RB+1_S9 */
                     95, /* LBB_RB+1_S11 */
                     97, /* LBB_RB+2_S1 */
                     99, /* LBB_RB+2_S3 */
                    101, /* LBB_RB+2_S5 */
                    103, /* LBB_RB+2_S7 */
                    105, /* LBB_RB+2_S9 */
                    107  /* LBB_RB+2_S11 */
	 );
	 names := NameList('RBC_RB-2_S1',
	                   'RBC_RB-2_S3',
	                   'RBC_RB-2_S5',
	                   'RBC_RB-2_S7',
	                   'RBC_RB-2_S9',
	                   'RBC_RB-2_S11',
	                   'RBC_RB-1_S1',
	                   'RBC_RB-1_S3',
	                   'RBC_RB-1_S5',
	                   'RBC_RB-1_S7',
	                   'RBC_RB-1_S9',
	                   'RBC_RB-1_S11',
	                   'RBC_RB0_S1',
	                   'RBC_RB0_S3',
	                   'RBC_RB0_S5',
	                   'RBC_RB0_S7',
	                   'RBC_RB0_S9',
	                   'RBC_RB0_S11',
	                   'RBC_RB+1_S1',
	                   'RBC_RB+1_S3',
	                   'RBC_RB+1_S5',
	                   'RBC_RB+1_S7',
	                   'RBC_RB+1_S9',
	                   'RBC_RB+1_S11',
	                   'RBC_RB+2_S1',
	                   'RBC_RB+2_S3',
	                   'RBC_RB+2_S5',
	                   'RBC_RB+2_S7',
	                   'RBC_RB+2_S9',
	                   'RBC_RB+2_S11'
	                   
	 );
	 labels := LabelList('3060000000111000025',
	                     '3060000000111000026',
	                     '3060000000111000027',
	                     '3060000000111000028',
	                     '3060000000111000029',
	                     '3060000000111000030',
	                     '3060000000111000019',
	                     '3060000000111000020',
	                     '3060000000111000021',
	                     '3060000000111000022',
	                     '3060000000111000023',
	                     '3060000000111000024',
	                     '3060000000111000013',
	                     '3060000000111000014',
	                     '3060000000111000015',
	                     '3060000000111000016',
	                     '3060000000111000017',
	                     '3060000000111000018',
	                     '3060000000111000007',
	                     '3060000000111000008',
	                     '3060000000111000009',
	                     '3060000000111000010',
	                     '3060000000111000011',
	                     '3060000000111000012',
	                     '3060000000111000001',
	                     '3060000000111000002',
	                     '3060000000111000003',
	                     '3060000000111000004',
	                     '3060000000111000005',
	                     '3060000000111000006'
	 );

	 cidsFake := CIdList(60, /* LBB_RB-2_S12 */
                         50, /* LBB_RB-2_S2 */
                         52, /* LBB_RB-2_S4 */
                         54, /* LBB_RB-2_S6 */
                         56, /* LBB_RB-2_S8 */
                         58, /* LBB_RB-2_S10 */
                         72, /* LBB_RB-1_S12 */
                         62, /* LBB_RB-1_S2 */
                         64, /* LBB_RB-1_S4 */
                         66, /* LBB_RB-1_S6 */
                         68, /* LBB_RB-1_S8 */
                         70, /* LBB_RB-1_S10 */
                         84, /* LBB_RB0_S12 */
                         74, /* LBB_RB0_S2 */
                         76, /* LBB_RB0_S4 */
                         78, /* LBB_RB0_S6 */
                         80, /* LBB_RB0_S8 */
                         82, /* LBB_RB0_S10 */
                         96, /* LBB_RB+1_S12 */
                         86, /* LBB_RB+1_S2 */
                         88, /* LBB_RB+1_S4 */
                         90, /* LBB_RB+1_S6 */
                         92, /* LBB_RB+1_S8 */
                         94, /* LBB_RB+1_S10 */
                        108, /* LBB_RB+2_S12 */
                         98, /* LBB_RB+2_S2 */
                        100, /* LBB_RB+2_S4 */
                        102, /* LBB_RB+2_S6 */
                        104, /* LBB_RB+2_S8 */
                        106  /* LBB_RB+2_S10 */
	 );
	 namesFake := NameList('RBCFAKE_RB-2_S12',
	                       'RBCFAKE_RB-2_S2',
	                       'RBCFAKE_RB-2_S4',
	                       'RBCFAKE_RB-2_S6',
	                       'RBCFAKE_RB-2_S8',
	                       'RBCFAKE_RB-2_S10',
	                       'RBCFAKE_RB-1_S12',
	                       'RBCFAKE_RB-1_S2',
	                       'RBCFAKE_RB-1_S4',
	                       'RBCFAKE_RB-1_S6',
	                       'RBCFAKE_RB-1_S8',
	                       'RBCFAKE_RB-1_S10',
	                       'RBCFAKE_RB0_S12',
	                       'RBCFAKE_RB0_S2',
	                       'RBCFAKE_RB0_S4',
	                       'RBCFAKE_RB0_S6',
	                       'RBCFAKE_RB0_S8',
	                       'RBCFAKE_RB0_S10',
	                       'RBCFAKE_RB+1_S12',
	                       'RBCFAKE_RB+1_S2',
	                       'RBCFAKE_RB+1_S4',
	                       'RBCFAKE_RB+1_S6',
	                       'RBCFAKE_RB+1_S8',
	                       'RBCFAKE_RB+1_S10',
	                       'RBCFAKE_RB+2_S12',
	                       'RBCFAKE_RB+2_S2',
	                       'RBCFAKE_RB+2_S4',
	                       'RBCFAKE_RB+2_S6',
	                       'RBCFAKE_RB+2_S8',
	                       'RBCFAKE_RB+2_S10'
	 );
	 labelsFake := LabelList('3060000000112000030',
	                         '3060000000112000025',
	                         '3060000000112000026',
	                         '3060000000112000027',
	                         '3060000000112000028',
	                         '3060000000112000029',
	                         '3060000000112000024',
	                         '3060000000112000019',
	                         '3060000000112000020',
	                         '3060000000112000021',
	                         '3060000000112000022',
	                         '3060000000112000023',
	                         '3060000000112000018',
	                         '3060000000112000013',
	                         '3060000000112000014',
	                         '3060000000112000015',
	                         '3060000000112000016',
	                         '3060000000112000017',
	                         '3060000000112000012',
	                         '3060000000112000007',
	                         '3060000000112000008',
	                         '3060000000112000009',
	                         '3060000000112000010',
	                         '3060000000112000011',
	                         '3060000000112000006',
	                         '3060000000112000001',
	                         '3060000000112000002',
	                         '3060000000112000003',
	                         '3060000000112000004',
	                         '3060000000112000005'
	 );
	 /* RBCBOARD */
	 masterids := MasterIdList();
	 masterids.EXTEND(RBC_NUMBER_OF_BOARDS);
	 masteridsFake := MasterIdList();
	 masteridsFake.EXTEND(RBC_NUMBER_OF_BOARDS);

	 FOR i IN 1 .. RBC_NUMBER_OF_BOARDS
	 LOOP
	   masterids(i) := ids(i); /* master points to itself */
	   masteridsFake(i) := ids(i); /* and slave to master */
     END LOOP;
     
     
	 /* CHIP */
     -- Append to the end of CHIP
	 select max(chipid) into chipidMax from chip;
	 
	 chipids := ChipIdList();
	 chipids.EXTEND(RBC_NUMBER_OF_BOARDS);
	 FOR i IN 1 .. RBC_NUMBER_OF_BOARDS
	 LOOP
	 	chipids(i) := chipidMax + i;
	 END LOOP;

	 
     FOR i IN ids.FIRST .. ids.LAST
     LOOP
       dbms_output.put_line('===');
       dbms_output.put_line('#' || i);
       
       INSERT INTO "BOARD"
       (BOARDID, CRATE_CRATEID, TYPE, NAME, LABEL, POSITION)
       VALUES
       (ids(i), cids(i), bType, names(i), labels(i), bPosition);
       INSERT INTO "RBCBOARD" VALUES (ids(i), masterids(i), i2cChannel);
       dbms_output.put_line('Added BOARD (' || ids(i) || ', ' || cids(i) || ', ' || bType || ', ' || 
         names(i) || ', ' || labels(i) || ', ' || bPosition || ')');
       dbms_output.put_line('Added RBCBOARD (' || ids(i) || ', ' || masterids(i) || ', ' || i2cChannel || ')');
       

       -- RBC_FAKE
       INSERT INTO "BOARD"
       (BOARDID, CRATE_CRATEID, TYPE, NAME, LABEL, POSITION)
       VALUES
       (idsFake(i), cidsFake(i), bTypeFake, namesFake(i), labelsFake(i), bPositionFake);
       INSERT INTO "RBCBOARD" VALUES (idsFake(i), masteridsFake(i), i2cChannelFake);
       dbms_output.put_line('Added BOARD (' || idsFake(i) || ', ' || cidsFake(i) || ', ' || bTypeFake || ', ' || 
         namesFake(i) || ', ' || labelsFake(i) || ', ' || bPositionFake || ')');
       dbms_output.put_line('Added RBCBOARD (' || idsFake(i) || ', ' || masteridsFake(i) || ', ' || i2cChannelFake || ')');
     


       INSERT INTO "CHIP" VALUES (chipids(i), ids(i), chipType, chipPosition);
       dbms_output.put_line('Added CHIP (' || chipids(i) || ', ' || ids(i) || ', ' || chipType || ', ' || chipPosition || ')');

     END LOOP;
   END;
   /

   
CREATE OR REPLACE
TRIGGER VALIDATEFK_RBCBOARD_BOARD_FK12
BEFORE INSERT OR UPDATE ON RBCBOARD
REFERENCING OLD AS OLD NEW AS NEW 
FOR EACH ROW
 WHEN (
new.RbcBoardId IS NOT NULL
      ) declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row Board%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM Board WHERE
			BoardId = fkVal1
		for update of
			BoardId;
begin
	open c1(:new.RbcBoardId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/

CREATE OR REPLACE
TRIGGER ONUPDATECASCADE_BOARD_RBC
AFTER UPDATE OF BOARDID ON BOARD
FOR EACH ROW 
BEGIN
  UPDATE RbcBoard
	SET
		RbcBoard.RbcBoardId = :new.BoardId
	WHERE
		RbcBoard.RbcBoardId = :old.BoardId;
END;
/

create or replace TRIGGER ONDELETECASCADE_BOARD_RBC
AFTER DELETE ON BOARD
FOR EACH ROW 
BEGIN
	DELETE FROM RbcBoard WHERE
		RbcBoard.RbcBoardId = :OLD.BoardId;
END;
/


/* Configuration */

  CREATE TABLE "RBCCHIPCONF" 
   ("RBCCHIPSTATICCONFID" NUMBER(11,0), 
	"CONFIG" NUMBER(11,0), 
	"CONFIGIN" NUMBER(11,0), 
	"CONFIGVER" NUMBER(11,0), 
	"MAJORITY" NUMBER(11,0), 
	"SHAPE" NUMBER(11,0),
	"MASK" NUMBER(11,0),
	"CTRL" NUMBER(11,0),
	 CONSTRAINT "RBCCHIPCONF_PK" PRIMARY KEY ("RBCCHIPSTATICCONFID"),
	 CONSTRAINT "STATICCONFIGURATION_FK16" 
	   FOREIGN KEY ("RBCCHIPSTATICCONFID")
	   REFERENCES "STATICCONFIGURATION" ("STATICCONFIGURATIONID")
   );
   
create or replace TRIGGER VALIDATEFK_RBCCHIPCONF_1
BEFORE INSERT OR UPDATE ON RBCCHIPCONF
FOR EACH ROW 
  WHEN (
new.RbcChipStaticConfId IS NOT NULL
      ) declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row StaticConfiguration%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM StaticConfiguration WHERE
			StaticConfigurationId = fkVal1
		for update of
    			StaticConfigurationId;
BEGIN
	open c1(:new.RbcChipStaticConfId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
END;
/


CREATE OR REPLACE TRIGGER ONDELETECASCADE_ST_CONF_RBC
AFTER DELETE ON STATICCONFIGURATION
FOR EACH ROW 
BEGIN
  DELETE FROM RbcChipConf WHERE
		RbcChipConf.RbcChipStaticConfId = :OLD.StaticConfigurationId;
END;
/

create or replace TRIGGER ONUPDATECASCADE_ST_CONF_RBC
AFTER UPDATE OF STATICCONFIGURATIONID ON STATICCONFIGURATION
FOR EACH ROW 
BEGIN
	UPDATE RbcChipConf
	SET
		RbcChipConf.RbcChipStaticConfId = :new.StaticConfigurationId
	WHERE
		RbcChipConf.RbcChipStaticConfId = :old.StaticConfigurationId;
END;
/

----
-- Synchronize the sequencies
----
BEGIN
	syncSeqToId('STATICCONFIGURATION', 'S_18_1_STATICCONFIGURATION');
	syncSeqToId('CHIPCONFASSIGNMENT', 'S_17_1_CHIPCONFASSIGNMENT');
	syncSeqToId('BOARD', 'S_13_1_BOARD');
	syncSeqToId('CHIP', 'S_11_1_CHIP');
END;
/