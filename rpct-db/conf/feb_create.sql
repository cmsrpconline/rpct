set serveroutput on

/*
 * Created on 2009-12-03
 * 
 * @author Nikolay Darmenov
 * @version $Id$
 */

--- Board
---- Deleting
---- BOARD, FEBBOARD, CHIP
DELETE FROM "CHIP"
WHERE "TYPE"='FEB';

DROP TRIGGER CMS_RPC_CONF.ONDELETECASCADE_BOARD_FEB;
DROP TABLE "FEBBOARD";

DELETE FROM "BOARD"
WHERE "TYPE" = 'FEBBOARD';


---- Construction
---- FEBBOARD
CREATE TABLE "FEBBOARD" (
     "FEBBOARDID" NUMBER(11,0),
     "FL_FEBLOCATIONID" NUMBER(11,0) NULL,
     "CBC_I2CCBCHANNELID" NUMBER(11,0) NOT NULL,
     "I2CLOCALNUMBER" NUMBER(4,0) NOT NULL,
	 CONSTRAINT "FEBBOARD_PK" PRIMARY KEY ("FEBBOARDID"),
	 CONSTRAINT "BOARD_FK14" FOREIGN KEY
	    ("FEBBOARDID") REFERENCES "BOARD" ("BOARDID"),
	 CONSTRAINT "FEBLOCATION_FK3" FOREIGN KEY
	    ("FL_FEBLOCATIONID") REFERENCES "FEBLOCATION" ("FEBLOCATIONID"),
	 /* TODO: rename I2CCBCHANNEL_FEBLOCATION_FK1 when CBC_I2CCBCHANNELID is removed from FEBLOCATION */
	 CONSTRAINT "I2CCBCHANNEL_FEBLOCATION_FK2" FOREIGN KEY
	 ("CBC_I2CCBCHANNELID") REFERENCES "I2CCBCHANNEL" ("I2CCBCHANNELID")
);
CREATE INDEX "FEBBOARD_IDX1" ON "FEBBOARD" ("FL_FEBLOCATIONID");
CREATE INDEX "FEBBOARD_IDX2" ON "FEBBOARD" ("CBC_I2CCBCHANNELID");

CREATE OR REPLACE
TRIGGER VALIDATEFK_FEBBOARD_BOARD_FK13
BEFORE INSERT OR UPDATE ON FEBBOARD
REFERENCING OLD AS OLD NEW AS NEW 
FOR EACH ROW
 WHEN (
new.FebBoardId IS NOT NULL
      ) declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row Board%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM Board WHERE
			BoardId = fkVal1
		for update of
			BoardId;
begin
	open c1(:new.FebBoardId);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/

CREATE OR REPLACE
TRIGGER ONUPDATECASCADE_BOARD_FEB
AFTER UPDATE OF BOARDID ON BOARD
FOR EACH ROW 
BEGIN
  UPDATE FebBoard
	SET
		FebBoard.FebBoardId = :new.BoardId
	WHERE
		FebBoard.FebBoardId = :old.BoardId;
END;
/

CREATE OR REPLACE
TRIGGER ONUPDATECASCADE_FL_FEB
AFTER UPDATE OF FebLocationID ON FebLocation
FOR EACH ROW 
BEGIN
  UPDATE FebBoard
	SET
		FebBoard.FL_FebLocationId = :new.FebLocationId
	WHERE
		FebBoard.FL_FebLocationId = :old.FebLocationId;
END;
/

CREATE OR REPLACE
TRIGGER ONUPDATECASCADE_I2CCBCH_FEB
AFTER UPDATE OF I2CCBCHANNELID ON I2CCBCHANNEL
FOR EACH ROW 
BEGIN
  UPDATE FebBoard
	SET
		FebBoard.CBC_I2CCBCHANNELID = :new.I2CCBCHANNELID
	WHERE
		FebBoard.CBC_I2CCBCHANNELID = :old.I2CCBCHANNELID;
END;
/

create or replace TRIGGER ONDELETECASCADE_BOARD_FEB
AFTER DELETE ON BOARD
FOR EACH ROW 
BEGIN
	DELETE FROM FebBoard WHERE
		FebBoard.FebBoardId = :OLD.BoardId;
END;
/

--- Configuration
---- Deleting
---- FEBCHIPCONF
alter table FEBCHIPCONF DISABLE constraint STATICCONFIGURATION_FK19;
delete from chipconfassignment
where sc_staticconfigurationid in (
	select FEBCHIPCONFID from FEBCHIPCONF
    )
;
delete from staticconfiguration
where staticconfigurationid in (
	select FEBCHIPCONFID from FEBCHIPCONF
	)
;
drop table "FEBCHIPCONF";


---- Construction
---- FEBCHIPCONF
CREATE TABLE "FEBCHIPCONF" (
    "FEBCHIPCONFID" NUMBER(11),
    "VTH" NUMBER(11) NOT NULL,
    "VTHOFFSET" NUMBER(11) NOT NULL,
    "VMON" NUMBER(11) NOT NULL,
    "VMONOFFSET" NUMBER(11) NOT NULL,
    "DTLINE" NUMBER(1) NOT NULL,
	CONSTRAINT "FEBCHIPCONF_PK" PRIMARY KEY ("FEBCHIPCONFID"), 
    CONSTRAINT "STATICCONFIGURATION_FK19" FOREIGN KEY ("FEBCHIPCONFID")
      REFERENCES "STATICCONFIGURATION" ("STATICCONFIGURATIONID")
);
CREATE OR REPLACE TRIGGER "VALIDATEFK_FEBCHIPCONF_1" 
BEFORE INSERT OR UPDATE ON FEBCHIPCONF
FOR EACH ROW
  WHEN (
new.FEBCHIPCONFID IS NOT NULL
      ) declare
	pk_not_found EXCEPTION;
	mutating_table EXCEPTION;
	PRAGMA EXCEPTION_INIT (mutating_table,-4091);
	parent_row StaticConfiguration%ROWTYPE;
	cursor c1 (fkVal1 NUMBER) is
		SELECT *
		FROM StaticConfiguration WHERE
			StaticConfigurationId = fkVal1
		for update of
			StaticConfigurationId;
begin
	open c1(:new.FEBCHIPCONFID);
	fetch c1 into parent_row;
	if c1%NOTFOUND then
		raise pk_not_found;
	end if;
	close c1;
exception
	when pk_not_found then
		close c1;
		raise_application_error(-20000,'Invalid FK value');
	when mutating_table then
		NULL;
	when others then
		close c1;
		raise;
end;
/

----
-- Synchronize the sequencies
----
BEGIN
	syncSeqToId('STATICCONFIGURATION', 'S_18_1_STATICCONFIGURATION');
	syncSeqToId('CHIPCONFASSIGNMENT', 'S_17_1_CHIPCONFASSIGNMENT');
END;
/
