package rpct.datastream;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;


@XmlType(name="eventType")
public class EventXml implements AutoMapValueType {    
    private int number;
    
    private int bxNum;

    private AutoMap<BxDataXml> bxData;

    private Set<ReadoutItemId> addedReadoutItems = new TreeSet<ReadoutItemId>();
    
    public EventXml() {
        super();
        bxData = new AutoMap<BxDataXml>();
    }

    public EventXml(int number, int bxNum) {
        super();
        this.number = number;
        this.bxNum = bxNum;
        bxData = new AutoMap<BxDataXml>();
    }

    //@XmlElement(name="bxData")
    public AutoMap<BxDataXml> getBxData() {
        return bxData;
    }

    public void setBxData(AutoMap<BxDataXml> bxData) {
        this.bxData = bxData;
    }

    public Integer getKey() {
        return getNumber();
    }

    @XmlAttribute(name="bx")
    public int getBxNum() {
        return bxNum;
    }

    public void setBxNum(int bxNum) {
        this.bxNum = bxNum;
    }

    @XmlAttribute(name="num")
    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
    
    public BxDataXml getOrAddBxData(int bxPos) {    
        BxDataXml bxDataXml = bxData.get(new Integer(bxPos));
        if(bxDataXml == null) {
            bxDataXml = new BxDataXml(bxPos);
            bxData.add(bxDataXml);
        }
        return bxDataXml;
    }
    
    TreeMap<Integer, LMuxBxDataImplXml.Set > getLmuxBxDataMap(ReadoutItemId id) {
    	TreeMap<Integer, LMuxBxDataImplXml.Set > lmuxBxDataMap = new TreeMap<Integer, LMuxBxDataImplXml.Set >();
    	if(id.getType() == ReadoutItemId.Type.optLink) {
            for(BxDataXml bxD : bxData) {
        		OptLinkDataXml optLinkDataXml = bxD.getOptLinkData(id);
        		if(optLinkDataXml != null) 
        		    if(optLinkDataXml.getLinkData().size() != 0)
        		        lmuxBxDataMap.put(bxD.getNumber(), optLinkDataXml.getLinkData());
        	}
        }
        else if(id.getType() == ReadoutItemId.Type.lb) {
            for(BxDataXml bxD : bxData) {
                LbDataXml lbDataXml = bxD.getLbData().get(id.getLbId());
                if(lbDataXml != null) {
                    if(id.getLbDataSource() == ReadoutItemId.LbDataSource.stripData) {
                        if(lbDataXml.getStripData().size() != 0)
                            lmuxBxDataMap.put(bxD.getNumber(), lbDataXml.getStripData());
                    }
                    else { 
                        if(lbDataXml.getCoderData().size() != 0)
                            lmuxBxDataMap.put(bxD.getNumber(), lbDataXml.getCoderData());
                    }
                }
            }
        }
        else
            throw new IllegalArgumentException("ReadoutItemId id is not of the optLink or lb type");

    	return lmuxBxDataMap;
    }
    
    void putLMuxData(ReadoutItemId id, TreeMap<Integer, LMuxBxDataImplXml.Set > lmuxBxDataMap) {
        if(id.getType() == ReadoutItemId.Type.optLink) {
        	for(Map.Entry<Integer, LMuxBxDataImplXml.Set > op : lmuxBxDataMap.entrySet()) {
    	    	BxDataXml bxDataXml = getOrAddBxData(op.getKey());
    	        TcDataXml tcData = bxDataXml.getOrAddTcData(id.getTcNum());
    	        TbDataXml tbData = tcData.getOrAddTbData(id.getTbNum());                
    	        OptLinkDataXml optLinkData =  tbData.getOrAddOptLink(id.getOptLinkNum());	
    	
    	        optLinkData.getLinkData().addAll(op.getValue()); 	
            }
        }
        else if (id.getType() == ReadoutItemId.Type.lb) {
            for(Map.Entry<Integer, LMuxBxDataImplXml.Set > op : lmuxBxDataMap.entrySet()) {
                BxDataXml bxDataXml = getOrAddBxData(op.getKey());
                LbDataXml lbDataXml = bxDataXml.getOrAddLbData(id.getLbId());  
                if(id.getLbDataSource() == ReadoutItemId.LbDataSource.stripData)
                    lbDataXml.getStripData().addAll(op.getValue()); 
                else
                    lbDataXml.getCoderData().addAll(op.getValue());    
            }            
        }
        else
            throw new IllegalArgumentException("ReadoutItemId id is not of the optLink or lb type");
    }

/*    TreeMap<Integer, PacDataXml> getPacData(ReadoutItemId id) {
        TreeMap<Integer, PacDataXml> pacDataMap = new TreeMap<Integer, PacDataXml>();
        for(BxDataXml bxD : bxData) {
            PacDataXml pacDataXml = bxD.getPacOutData(id.getTcNum(), id.getTbNum(), id.getPacNum());
            if(pacDataXml != null) 
                pacDataMap.put(bxD.getNumber(), pacDataXml);
        }
        return pacDataMap;
    }*/
    
/*    void putPacData(ReadoutItemId id, TreeMap<Integer, PacDataXml> pacDataMap) {
        for(Map.Entry<Integer, PacDataXml> pacE : pacDataMap.entrySet()) {
            BxDataXml bxDataXml = getOrAddBxData(pacE.getKey());
            TcDataXml tcData = bxDataXml.getOrAddTcData(id.getTcNum());
            TbDataXml tbData = tcData.getOrAddTbData(id.getTbNum());                
            PacDataXml pacData =  tbData.getOrAddPac(id.getPacNum());   
    
            //pacData.getOutMuons().addAll(pacE.getValue().getOutMuons());              
            pacData.setMuons(pacE.getValue().getMuons());
        }
    }*/
    
    TreeMap<Integer, MuonsSet> getBxMuonsMap(ReadoutItemId id) {
        TreeMap<Integer, MuonsSet> bxMuonsMap = new TreeMap<Integer, MuonsSet>();
        for(BxDataXml bxD : bxData) {
            MuonsSet muons = bxD.getMuons(id);
            if(muons != null && muons.getMuons().size() != 0) 
                bxMuonsMap.put(bxD.getNumber(), muons);
        }
        return bxMuonsMap;
    }
    
    void putMuons(ReadoutItemId id, TreeMap<Integer, MuonsSet> bxMuonsMap) {
        for(Map.Entry<Integer, MuonsSet> gbsE : bxMuonsMap.entrySet()) {
            BxDataXml bxDataXml = getOrAddBxData(gbsE.getKey());
            bxDataXml.putMuons(id, gbsE.getValue());
        }
    }
    
    
    public boolean itemWasAdded(ReadoutItemId id) {        
        return addedReadoutItems.contains(id);
    }
    
    void addActiveReadoutItem(ReadoutItemId id) {
        addedReadoutItems.add(id);
    }

    public Set<ReadoutItemId> getAddedReadoutItemIds() {
        return addedReadoutItems;
    }
}

