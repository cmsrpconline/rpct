package rpct.datastream;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;

import rpct.datastream.TestsManager.PulserTarget;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.equipment.SorterCrate;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.diag.TriggerDataSel;
import rpct.xdaqaccess.diag.DiagCtrl.TriggerType;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbSynCoder;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;

public class OptLinksReadout {

    public static void main(String[] args) throws ServiceException,
    DataAccessException, IOException, InterruptedException,
    XdaqException {        
	//----------------------------------------------------------------------------------------------------
	long readoutLimit = RpctDelays.BX_IN_SEC * 10; //TODO

	HibernateContext context = new SimpleHibernateContextImpl();
	EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
	HardwareDbMapper dbMapper = new HardwareDbMapper(
		new HardwareRegistry(), context);
	String lbConfigKey = ConfigurationManager.LOCAL_CONF_KEY_LBS_LHC2; //TODO
	
	//TestPresets.TestPreset preset = new TestPresets.NoPulser_DaqLike(readoutLimit); //TODO
	TestPresets.TestPreset preset = new TestPresets.NoPulser_SnapshotBC0(); //TODO
	TestsManager pulsesManager = new TestsManager(preset, null, equipmentDAO, null, null, lbConfigKey);

	try {
	    //TODO - selct pulser source and readouts
	    HardwareDbTriggerCrate tcHdb = (HardwareDbTriggerCrate)dbMapper.getCrate(equipmentDAO.getCrateByName("TC_9"));
	    HardwareDbBoard tcBpHdb = dbMapper.getBoard(equipmentDAO.getBoardByName("TC_9_BP")); //TBp2_9

	    List<HardwareDBTriggerBoard> tbs = new ArrayList<HardwareDBTriggerBoard>();			
	    HardwareDBTriggerBoard tbHdb = (HardwareDBTriggerBoard)dbMapper.getBoard(equipmentDAO.getBoardByName("TBp2_9")); //TBp2_9
	    tbs.add(tbHdb);					

	    SorterCrate sc = (SorterCrate)equipmentDAO.getCrateByName("SC");
	    HardwareDbBoard hsb1Hdb = (HardwareDbBoard)dbMapper.getBoard(equipmentDAO.getBoardByName("HSB_1"));

	    Set<Chip> optosSet = new HashSet<Chip>();
	    Set<Chip> synCodersSet = new HashSet<Chip>();

/*	    for(HardwareDBTriggerBoard tb : tbs) {			
		for(LinkConn linkConn : tb.getDBTriggerBoard().getLinkConns()) {						
		    if(linkConn.getSynCoder().getBoard().getName().contains("LB_RB+2_S10") || linkConn.getSynCoder().getBoard().getName().contains("LB_RB+2_S11") ) {
			synCodersSet.add(linkConn.getSynCoder());
			optosSet.add(linkConn.getOpto());
		    }
		}	
	    }*/

	    //List<Chip> synCoders = new ArrayList<Chip>(synCodersSet);

	    //List<Chip> optos = new ArrayList<Chip>(optosSet);
	    List<Chip> optos = equipmentDAO.getChipsByTypeAndCrate(ChipType.OPTO, tcHdb.getDbCrate(), true);
	    //List<Chip> optos = equipmentDAO.getChipsByTypeAndBoard(ChipType.OPTO, equipmentDAO.getBoardByName("TBn3_9"), true);	//TBp2_9		
	    /*List<Chip> optos = new ArrayList<Chip>();
			optos.add(((TriggerBoard)equipmentDAO.getBoardByName("TBn3_9")).getOptoForLinkNum(0));
			optos.add(((TriggerBoard)equipmentDAO.getBoardByName("TBn3_9")).getOptoForLinkNum(6));*/
	    //pulsesManager.addPulsers(optos.get(0).getType(), dbMapper.getPulsers(optos));
	    pulsesManager.addReadouts(optos.get(0).getType(), dbMapper.getReadouts(optos));

	    //List<Chip> pacs = equipmentDAO.getChipsByTypeAndBoard(ChipType.PAC, tbHdb.getDbBoard(), true);
	    //List<Chip> pacs = new ArrayList<Chip>();
	    //pacs.add(tbHdb.getPacs().get(0).getDbChip());
	    //pulsesManager.addPulsers(ChipType.PAC, dbMapper.getPulsers(pacs));
	    //pulsesManager.addReadouts(pacs.get(0).getType(), dbMapper.getReadouts(pacs));

	    //List<Chip> rmbs = new ArrayList<Chip>();
//	    rmbs.add(tbHdb.getRmb().getDbChip());
//	    pulsesManager.addReadouts(rmbs.get(0).getType(), dbMapper.getReadouts(rmbs));                       

//	    List<Chip> rmbs = equipmentDAO.getChipsByTypeAndBoard(ChipType.RMB,tbHdb.getDbBoard(), true);
	    //List<Chip> rmbs = equipmentDAO.getChipsByTypeAndCrate(ChipType.RMB, tc);
	    //List<Chip> rmbs = new ArrayList<Chip>();
//	    rmbs.add(tbHdb.getRmb().getDbChip());
//	    pulsesManager.addReadouts(rmbs.get(0).getType(), dbMapper.getReadouts(rmbs));

/*	    //List<Chip> gbSorts = equipmentDAO.getChipsByTypeAndCrate(ChipType.GBSORT, tc);
	    List<Chip> gbSorts = equipmentDAO.getChipsByTypeAndBoard(ChipType.GBSORT, tbHdb.getDbBoard(), true);
	    //pulsesManager.addPulsers(ChipType.GBSORT, dbMapper.getPulsers(gbSorts));
	    pulsesManager.addReadouts(gbSorts.get(0).getType(), dbMapper.getReadouts(gbSorts));

	    List<Chip> tcSorts = equipmentDAO.getChipsByTypeAndBoard(ChipType.TCSORT, tcBpHdb.getDbBoard(), true);			
	    //pulsesManager.addPulsers(ChipType.TCSORT, dbMapper.getPulsers(tcSorts));
	    pulsesManager.addReadouts(ChipType.TCSORT, dbMapper.getReadouts(tcSorts));

	    //List<Chip> halfSorts = equipmentDAO.getChipsByTypeAndCrate(ChipType.HALFSORTER, sc, true);
	    List<Chip> halfSorts = equipmentDAO.getChipsByTypeAndBoard(ChipType.HALFSORTER, hsb1Hdb.getDbBoard(), true);
	    //pulsesManager.addPulsers(ChipType.HALFSORTER, dbMapper.getPulsers(halfSorts));
	    pulsesManager.addReadouts(ChipType.HALFSORTER, dbMapper.getReadouts(halfSorts));

	    List<Chip> finalSort = equipmentDAO.getChipsByTypeAndBoard(ChipType.FINALSORTER, equipmentDAO.getBoardByName("FSB"), true);
	    //pulsesManager.addPulsers(ChipType.FINALSORTER, dbMapper.getPulsers(finalSort));
	    pulsesManager.addReadouts(ChipType.FINALSORTER, dbMapper.getReadouts(finalSort));	*/		


	    MonitorManager monitorManager = new MonitorManager(tbHdb.getChips());
	    monitorManager.addChip(tcHdb.getTcSort());			
	    monitorManager.addChips(hsb1Hdb.getChips());
	    pulsesManager.setMonitorManager(monitorManager);
	    //monitorManager.enableTransmissionCheck(true, true); //TODO

	    //monitorManager.resetRecErrorCnt();

	    //pulsesManager.interConnectionTest(false);
	    //pulsesManager.singleRedaouts();
	    //pulsesManager.daqTest(false);
	    //pulsesManager.algorithmsTest();

	    int redoutCnt = 10; //TODO
	    //pulsesManager.multipleRedaouts( redoutCnt, "/nfshome0/rpcdev/bin/data/test/lastEventDataMutli.xml"); //TODO
	    pulsesManager.singleRedaouts();
	    
	    System.out.print(monitorManager.readRecErrorCnt());
	} catch (JAXBException e) {
	    e.printStackTrace();
	} catch (HardwareDbException e) {
	    e.printStackTrace();
	} finally {
	    context.closeSession();
	}
    }

}
