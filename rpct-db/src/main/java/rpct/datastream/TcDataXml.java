package rpct.datastream;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="tc")
public class TcDataXml implements AutoMapValueType {   
    private int number; //logSector

    private AutoMap<TbDataXml> tbData = new AutoMap<TbDataXml>();
    
    private MuonsSet gbSortMuons = new MuonsSet();
    
    public TcDataXml() {
        super();
    }
    
    public TcDataXml(int number) {
        super();
        this.number = number;
    }

    public Integer getKey() {
        return number;
    }

    @XmlAttribute(name="num")
    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @XmlElement(name="tb")
    public AutoMap<TbDataXml> getTbData() {
        return tbData;
    }

    public void setTbData(AutoMap<TbDataXml> tbData) {
        this.tbData = tbData;
    }

    @XmlElement(name="tcgs")
    public MuonsSet getGbSortMuons() {
        return gbSortMuons;
    }

    public void setGbSortMuons(MuonsSet gbSortMuons) {
        this.gbSortMuons = gbSortMuons;
    }
    
    TbDataXml getOrAddTbData(int number) {
        TbDataXml tbDataXml = tbData.get(number);
        if(tbDataXml == null) {
            tbDataXml = new TbDataXml(number);
            tbData.add(tbDataXml);
        }
        return tbDataXml;
    }
    
    public String toString() {
    	String str = "      tc: " + getNumber() + "\n";
    	for(TbDataXml tbDataXml : tbData) {
    		str += tbDataXml.toString();
    	}
    	
    	str += "      tcgs\n" + gbSortMuons.toString();
    	return str;
    }
}
