package rpct.datastream;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="tb")
public class TbDataXml implements AutoMapValueType {    
    private int number; //number in TC, 0...8    

    private AutoMap<OptLinkDataXml> optLinks = new AutoMap<OptLinkDataXml>();;
    
    private AutoMap<PacDataXml> pacs = new AutoMap<PacDataXml>();
    
    private MuonsSet gbSortMuons = new MuonsSet(); //output muons
    
    public TbDataXml() {
        super();
    }

    public TbDataXml(int number) {
        super();
        this.number = number;
    }

    public Integer getKey() {
        return number;
    }
    
    @XmlAttribute(name="num")
    public int getNumber() {
        return number;        
    }

    public void setNumber(int number) {
        this.number = number;
    }
    
    @XmlElement(name="ol")
    public AutoMap<OptLinkDataXml> getOptLinks() {
        return optLinks;
    }
    
    public void setOptLinks(AutoMap<OptLinkDataXml> optLinks) {
        this.optLinks = optLinks;
    }

    @XmlElement(name="pac")
    public AutoMap<PacDataXml> getPacs() {
        return pacs;
    }

    public void setPacs(AutoMap<PacDataXml> pacs) {
        this.pacs = pacs;
    }
    
    PacDataXml getOrAddPac(int number) {
        PacDataXml pacDataXml = pacs.get(number);
        if(pacDataXml == null) {
            pacDataXml = new PacDataXml(number);
            pacs.add(pacDataXml);
        }
        return pacDataXml;
    }
    
    @XmlElement(name="tbgs")
    public MuonsSet getGbSortMuons() {
        return gbSortMuons;
    }

    public void setGbSortMuons(MuonsSet gbSortMuons) {
        this.gbSortMuons = gbSortMuons;
    }
    
    OptLinkDataXml getOrAddOptLink(int number) {
        OptLinkDataXml data = optLinks.get(number);
        if(data == null) {
            data = new OptLinkDataXml(number);
            optLinks.add(data);
        }
        return data;
    }   
    
    public String toString() {
    	String str = "        tb " + getNumber() + "\n";
    	for(OptLinkDataXml optLinkDataXml : optLinks ) {
    		str += "          ol " + optLinkDataXml.getNumber() + "\n";
    		str += optLinkDataXml.getLinkData();
    	}
    	    	
    	for(PacDataXml pacDataXml : pacs) {
    		str += pacDataXml.toString();
    	}
    	
    	str += "          tbgs\n" + gbSortMuons.toString();
    	return str;
    }
}
