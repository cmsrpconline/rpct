package rpct.datastream;


public class HardwareMuon extends MuonImplXml {
/*	public enum MuonType {
		mtPacOut,
		mtTBSortOut,
		mtTCSortOut,
		mtHalfSortOut,
		mtFinalSortOut
	};
	protected MuonType type;

	// for input and output
	public MuonType getType() {
		return type;	
	}*/

	public int toBits() {
		return 0;
	}			

	public static int getMuonBitsCnt() {
		return 0;
	}	

	public static class PacOutMuon extends HardwareMuon {
		static int m_qualBitsCnt = 3;  static int m_qualBitsMask = 0x7;
		static int m_ptBitsCnt   = 5;  static int m_ptBitsMask   = 0x1f;
		static int m_signBitsCnt = 1;  static int m_signBitsMask = 0x1;	

		public PacOutMuon(int bitValue,  int num) {
			this.num = num;
			int shift = 0;
			sign       = (bitValue & (m_signBitsMask<<shift))  >> shift;  shift += m_signBitsCnt;  
			ptCode     = (bitValue & (m_ptBitsMask<<shift))    >> shift;  shift += m_ptBitsCnt;
			quality    = (bitValue & (m_qualBitsMask<<shift))  >> shift;  shift += m_qualBitsCnt;
		}

		public int toBits(){
			int bitValue = 0;
			int shift = 0;
			bitValue =         (sign<<shift);       shift += m_signBitsCnt;
			bitValue = bitValue | (ptCode<<shift);     shift += m_ptBitsCnt;
			bitValue = bitValue | (quality<<shift);    shift += m_qualBitsCnt;

			return bitValue;
		}

		public static int getMuonBitsCnt() {
			return m_qualBitsCnt + m_ptBitsCnt + m_signBitsCnt;
		}
	}

	public static class TbGbSortOutMuon extends HardwareMuon { 	
		static int m_qualBitsCnt = 3;   static int m_qualBitsMask = 0x7;  
		static int m_ptBitsCnt   = 5;   static int m_ptBitsMask   = 0x1f;	
		static int m_signBitsCnt = 1;   static int m_signBitsMask = 0x1;
		static int m_phiBitsCnt  = 4;   static int m_phiBitsMask  = 0xf;
		static int m_etaBitsCnt  = 2;   static int m_etaBitsMask  = 0x3;
		static int m_gbDataBitsCnt=2;   static int m_gbDataBitsMask = 0x3;

		public TbGbSortOutMuon(int bitValue,  int num) {
			this.num = num;
			int shift = 0;
			sign       = (bitValue & (m_signBitsMask<<shift))  >> shift;  shift += m_signBitsCnt;  
			ptCode     = (bitValue & (m_ptBitsMask<<shift))    >> shift;  shift += m_ptBitsCnt;
			quality    = (bitValue & (m_qualBitsMask<<shift))  >> shift;  shift += m_qualBitsCnt;
			phiAddress = (bitValue & (m_phiBitsMask<<shift))   >> shift;  shift += m_phiBitsCnt;
			etaAddress = (bitValue & (m_etaBitsMask<<shift))   >> shift;  shift += m_etaBitsCnt;
			gbData     = (bitValue & (m_gbDataBitsMask<<shift))>> shift;  shift += m_gbDataBitsCnt;  
		}

		public int toBits(){
			int bitValue = 0;
			int shift = 0;
			bitValue =            (sign<<shift);       shift += m_signBitsCnt;
			bitValue = bitValue | (ptCode<<shift);     shift += m_ptBitsCnt;
			bitValue = bitValue | (quality<<shift);    shift += m_qualBitsCnt;
			bitValue = bitValue | (phiAddress<<shift); shift += m_phiBitsCnt;
			bitValue = bitValue | (etaAddress<<shift); shift += m_etaBitsCnt; 
			bitValue = bitValue | (gbData<<shift);     shift += m_gbDataBitsCnt;
			return bitValue;
		} 

		public static int getMuonBitsCnt() {
			return m_qualBitsCnt + m_ptBitsCnt + m_signBitsCnt + m_phiBitsCnt + m_etaBitsCnt + m_gbDataBitsCnt;
		}
	};

	public static class TcSortOutMuon extends HardwareMuon {
		static int m_gbDataBitsCnt=2;   static int m_gbDataBitsMask = 0x3;
		static int m_etaBitsCnt  = 6;   static int m_etaBitsMask  = 0x3f;
		static int m_phiBitsCnt  = 4;   static int m_phiBitsMask  = 0xf;
		static int m_qualBitsCnt = 3;   static int m_qualBitsMask = 0x7;
		static int m_ptBitsCnt   = 5;   static int m_ptBitsMask   = 0x1f;
		static int m_signBitsCnt = 1;   static int m_signBitsMask = 0x1;
		
		public TcSortOutMuon(int bitValue,  int num) {
			this.num = num;
			int shift = 0;
			sign       = (bitValue & (m_signBitsMask<<shift))  >> shift;  shift += m_signBitsCnt; 
			ptCode     = (bitValue & (m_ptBitsMask<<shift))    >> shift;  shift += m_ptBitsCnt;
			quality    = (bitValue & (m_qualBitsMask<<shift))  >> shift;  shift += m_qualBitsCnt;
			phiAddress = (bitValue & (m_phiBitsMask<<shift))   >> shift;  shift += m_phiBitsCnt;
			etaAddress = (bitValue & (m_etaBitsMask<<shift))   >> shift;  shift += m_etaBitsCnt;
			gbData     = (bitValue & (m_gbDataBitsMask<<shift))>> shift;  shift += m_gbDataBitsCnt;  
		}

		public int toBits() {
			int bitValue = 0;
			int shift = 0;
			bitValue =            (sign<<shift);       shift += m_signBitsCnt;
			bitValue = bitValue | (ptCode<<shift);     shift += m_ptBitsCnt;
			bitValue = bitValue | (quality<<shift);    shift += m_qualBitsCnt;
			bitValue = bitValue | (phiAddress<<shift); shift += m_phiBitsCnt;  
			bitValue = bitValue | (etaAddress<<shift); shift += m_etaBitsCnt; 
			bitValue = bitValue | (gbData<<shift);     shift += m_gbDataBitsCnt;
			return bitValue;
		}

		public static int getMuonBitsCnt() {
			return m_qualBitsCnt + m_ptBitsCnt + m_signBitsCnt + m_phiBitsCnt + m_etaBitsCnt + m_gbDataBitsCnt;
		}
	};

	public static class HalfSortOutMuon extends HardwareMuon {
		static int m_signBitsCnt = 1;   static int m_signBitsMask = 0x1;
		static int m_ptBitsCnt   = 5;   static int m_ptBitsMask   = 0x1f;
		static int m_qualBitsCnt = 3;   static int m_qualBitsMask = 0x7;  	
		static int m_phiBitsCnt  = 7;   static int m_phiBitsMask  = 0x7f;  	  	
		static int m_etaBitsCnt  = 6;   static int m_etaBitsMask  = 0x3f;

		public HalfSortOutMuon( int bitValue,  int num) {
			this.num = num;
			int shift = 0;
			sign       = (bitValue & (m_signBitsMask<<shift)) >> shift;  shift += m_signBitsCnt;
			ptCode     = (bitValue & (m_ptBitsMask<<shift))   >> shift;  shift += m_ptBitsCnt;
			quality    = (bitValue & (m_qualBitsMask<<shift)) >> shift;  shift += m_qualBitsCnt;
			phiAddress = (bitValue & (m_phiBitsMask<<shift))  >> shift;  shift += m_phiBitsCnt;
			etaAddress = (bitValue & (m_etaBitsMask<<shift))  >> shift;  shift += m_etaBitsCnt;
		}

		public int toBits() {
			int bitValue = 0;

			int shift = 0;
			bitValue = bitValue | (sign<<shift);       shift += m_signBitsCnt;
			bitValue = bitValue | (ptCode<<shift);     shift += m_ptBitsCnt;
			bitValue = bitValue | (quality<<shift);    shift += m_qualBitsCnt;
			bitValue = bitValue | (phiAddress<<shift); shift += m_phiBitsCnt;
			bitValue = bitValue | (etaAddress<<shift); shift += m_etaBitsCnt; 

			return bitValue;
		}

		public static int getMuonBitsCnt() {
			return m_qualBitsCnt + m_ptBitsCnt + m_signBitsCnt + m_phiBitsCnt + m_etaBitsCnt;
		}
	};

	public static class FinalSortOutMuon extends HardwareMuon {
		int bc0_;
		int bcn_;
		int signValid_;
		int par_;
		static int m_phiBitsCnt  = 8;   	static int m_phiBitsMask  = 0xff;
		static int m_ptBitsCnt   = 5;   	static int m_ptBitsMask   = 0x1f;
		static int m_qualBitsCnt = 3;   	static int m_qualBitsMask = 0x7;
		static int m_etaBitsCnt  = 6;   	static int m_etaBitsMask  = 0x3f;  	
		static int m_hvBitsCnt = 1;     	static int m_hvBitsMask = 0x1; //H/F - CSC/DT bit  	 
		static int m_signBitsCnt = 1;   	static int m_signBitsMask = 0x1;  	
		static int m_signValidBitsCnt = 1;  static int m_signValidBitsMask = 0x1;  	
		static int m_bcnBitsCnt = 3;    	static int m_bcnBitsMask  = 0x7;
		static int m_bc0BitsCnt = 1;    	static int m_bc0BitsMask  = 0x1;  	
		static int m_synchErrBitsCnt = 1;   static int m_synchErrBitsMask = 0x1; 
		static int m_parityBitsCnt = 1;  	static int m_parityBitsMask = 0x1; 

		public FinalSortOutMuon( int bitValue,  int num) {
			this.num = num;
			int shift = 0;
			phiAddress =  bitValue &  m_phiBitsMask;                     shift += m_phiBitsCnt;
			ptCode     = (bitValue & (m_ptBitsMask<<shift))   >> shift;  shift += m_ptBitsCnt;
			quality    = (bitValue & (m_qualBitsMask<<shift)) >> shift;  shift += m_qualBitsCnt;
			etaAddress = (bitValue & (m_etaBitsMask<<shift))  >> shift;  shift += m_etaBitsCnt + 1; //+1 beacouse H/F bits, unused in RPC:
			sign       = (bitValue & (m_signBitsMask<<shift)) >> shift;  shift += m_signBitsCnt;
			signValid_   = (bitValue & (m_signValidBitsMask<<shift)) >> shift;  shift += m_signValidBitsCnt;
			bcn_         = (bitValue & (m_bcnBitsMask<<shift)) >> shift;  shift += m_bcnBitsCnt;	
			bc0_         = (bitValue & (m_bc0BitsMask<<shift)) >> shift;  shift += m_bc0BitsCnt;

			par_  = (bitValue & (m_parityBitsMask<<shift)) >> shift; shift += m_parityBitsCnt;  

			ptCode = (~(ptCode)) & m_ptBitsMask;
			quality = (~(quality)) & m_qualBitsMask;
		}

		public int toBits() {
			int bitValue = 0;

			int shift = 0;
			bitValue = bitValue | (sign<<shift);       shift += m_signBitsCnt;
			bitValue = bitValue | (ptCode<<shift);     shift += m_ptBitsCnt;
			bitValue = bitValue | (quality<<shift);    shift += m_qualBitsCnt;
			bitValue = bitValue | (phiAddress<<shift); shift += m_phiBitsCnt;
			bitValue = bitValue | (etaAddress<<shift); shift += m_etaBitsCnt; 

			return bitValue;
		}


		/**
		 * @return don't use this function, use the getPulserMuonBitsCnt or getDaqMuonBitsCnt
		 */
		public static int getMuonBitsCnt() {
		    return 0;
		}
		
		public static int getPulserMuonBitsCnt() {
			return m_qualBitsCnt + m_ptBitsCnt + m_signBitsCnt + m_phiBitsCnt + m_etaBitsCnt;
		}

		public static int getDaqMuonBitsCnt() {
			return m_qualBitsCnt + m_ptBitsCnt + m_signBitsCnt + m_phiBitsCnt + m_etaBitsCnt + m_bc0BitsCnt + m_bcnBitsCnt + m_signValidBitsCnt + m_synchErrBitsCnt + m_parityBitsCnt + m_hvBitsCnt;
		}
	}
}
