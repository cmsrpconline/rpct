package rpct.datastream;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

public class OptLinkDataXml implements AutoMapValueType {
    @XmlAttribute(name="num")
    private int number;
    
    private LMuxBxDataImplXml.Set linkData;
    
    public OptLinkDataXml() {
        super();
        linkData = new LMuxBxDataImplXml.Set();
    }    
    
    public OptLinkDataXml(int number) {
        super();
        this.number = number;
        linkData = new LMuxBxDataImplXml.Set();
    }

    public Integer getKey() {
        return number;
    }
    
    @XmlTransient
    public int getNumber() {
        return number;
    }

    @XmlElement(name="lmd")
    public LMuxBxDataImplXml.Set getLinkData() {
        return linkData;
    }

    public void setLinkData(LMuxBxDataImplXml.Set linkData) {
        this.linkData = linkData;        
        /*for(LMuxBxDataImplXml lmuxData : this.linkData) {
            if(lmuxData.getIndex() == 0) {
                lmuxData.setIndex(number);
            }                                        
        }*/ //looks that it has no effect during unmarshal of the xml
    }

}
