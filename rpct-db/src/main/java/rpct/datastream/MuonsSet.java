package rpct.datastream;

import java.util.Set;
import java.util.TreeSet;

import javax.xml.bind.annotation.XmlElement;

public class MuonsSet {
    private Set<MuonImplXml> muons = new TreeSet<MuonImplXml>();
    
    public MuonsSet() {}
    
    @XmlElement(name="mu")
    public Set<MuonImplXml> getMuons() {
        return muons;
    }

    public void setMuons(Set<MuonImplXml> muons) {
        this.muons = muons;
    }    
    
    public String toString() {
    	String str = "";
    	for(MuonImplXml muon : muons)
    		str += muon.toString() + "\n";
    	
    	return str;		
	}
    
    public boolean equals(Object obj) {
    	return this.getMuons().equals(((MuonsSet)obj).getMuons());
    }
}
