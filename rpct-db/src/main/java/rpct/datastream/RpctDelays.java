package rpct.datastream;

public class RpctDelays {
	public static final long BX_IN_SEC = 40000000;
	
	public static final int BX_IN_ORBIT = 3564;
	
	//public static final int RMB_DATA_DELAY = 84 +5; //should be good for the 116 GT latency
	public static final int RMB_DATA_DELAY = 83 -3  -2 -3 +2 +1 +2 +3 -1 +12 -2 +3 +2 -3;
	// -2 for new RMB firmware handling properly the overlap events, 2011 03 16
	//+12 for the increased L1 latency
	//-2 after switch to TCDS 2014 09 23 runs 226664 - 226673
	//+3 BX , 2014 09 24, from run 226'725 
	//+2 BX , 2014 09 24, from run 226'
	//-3 BX , 2014 11 05, from run - L1 latency decreased on tracker request
	
	public static final int DATA_DELAY_OFFSET = RMB_DATA_DELAY + 43 +2;

	public static final int PRE_TRIGGER_VAL = 1; //zakres BX branych do DAQ
	public static final int POST_TRIGGER_VAL = 6;
    
	//the same as in the FixedHardwareSettings
	public static final int LB_BC0_DELAY = 0;
	public static final int OPTO_BC0_DELAY = 27 + 6 + 2 + 10 -1;
	//public static final int OPTO_BC0_DELAY = 8; //pasteura
	public static final int PAC_BC0_DELAY = OPTO_BC0_DELAY + 5;// -1;
	public static final int RMB_BC0_DELAY = PAC_BC0_DELAY; // + 2 for new RMB firmware handling properly the overlap events, 2011 03 16
	public static final int TBSORT_BC0_DELAY = PAC_BC0_DELAY + 6;
	public static final int TCSORT_BC0_DELAY = TBSORT_BC0_DELAY + 9 + 3 -1;
	public static final int HSB_BC0_DELAY = TCSORT_BC0_DELAY + 5;
	public static final int FSB_BC0_DELAY = 3522;//for the new FSB firmware, compiled to svf, with latency changed
	
    public static final int LB_CODER_LATENCY = 2;
    public static final int LB_MUXER_LATENCY = 2;
	
	public static final int OPTO_LATENCY = 4 -4; //before the readout
	public static final int PAC_LATENCY = 12;
	public static final int TBSORT_LATENCY = 2;
	public static final int TCSORT_LATENCY = 2;
	public static final int HSORT_LATENCY = 2; //was 3BX, 2BX is in the firmware  Aug  9 17:16 cii_sc_hsort.svf checksum 0x8948
	public static final int FSORT_LATENCY = 1;
	    
    public static final int LB_SLAVE_MASTER_LATENCY = 2; 
	public static final int LB_OPTO_LATENCY = 27 + 6 +4 +2 +10 -1; //opt links transmission
	public static final int OPTO_PAC_LATENCY = 6; 	
	public static final int PAC_TBSORT_LATENCY = 7;
	public static final int TBSORT_TCSORT_LATENCY = 5 -1;
	public static final int TCSORT_HSORT_LATENCY = 6 + 1;//+1 comes from the REC_DATA_DELAY = 0x5555555
	public static final int HSORT_FSORT_LATENCY = -1;
	
    public static final int LB_LATENCY = LB_CODER_LATENCY + LB_SLAVE_MASTER_LATENCY + LB_MUXER_LATENCY + 1; //unified LB latency
    
	public static final int TO_LB_LATENCY = 0;
    
	public static final int TO_OPTO_LATENCY = TO_LB_LATENCY + LB_LATENCY + LB_OPTO_LATENCY + OPTO_LATENCY;
	public static final int TO_PAC_LATENCY = TO_OPTO_LATENCY + OPTO_PAC_LATENCY;
	public static final int TO_TBSORT_LATENCY = TO_PAC_LATENCY + PAC_LATENCY + PAC_TBSORT_LATENCY;
	public static final int TO_TCSORT_LATENCY = TO_TBSORT_LATENCY + TBSORT_LATENCY + TBSORT_TCSORT_LATENCY;
	public static final int TO_HSORT_LATENCY = TO_TCSORT_LATENCY + TCSORT_LATENCY + TCSORT_HSORT_LATENCY;
	public static final int TO_FSORT_LATENCY = TO_HSORT_LATENCY + HSORT_LATENCY + HSORT_FSORT_LATENCY;
	
	public static final int TTC_TC_TB_LATENCY = 8;
	
	//now it is relative to the TC and SC
	public static final int TTC_LB_LATENCY = 20 - 15;
	public static final int TTC_TC_LATENCY = 0;
	public static final int TTC_TB_LATENCY = TTC_TC_LATENCY + TTC_TC_TB_LATENCY;
	public static final int TTC_HS_LATENCY = 0;//-9;
	public static final int TTC_FS_LATENCY = 0;//-9;
	
	public static final int LB_DELAY_MAX = 12;
}
