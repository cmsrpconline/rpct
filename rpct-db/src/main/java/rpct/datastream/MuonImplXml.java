package rpct.datastream;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import rpct.db.domain.equipment.Chip;
import rpct.xdaqaccess.diag.Muon;

@XmlType(name="mu") 
public class MuonImplXml implements Muon, Comparable {
	protected int index; //given by the c++, continuous number of muon in the diagnostic readout data

	protected int num; //number of muon in given input or output
        
	protected int quality;
    
	protected int ptCode;
   
    protected int sign;
 
    protected int etaAddress; 

    protected int phiAddress;

    protected int gbData;    
    
    protected String notValid;         

    public MuonImplXml() {}
    
    
    /**
     * @param index given by the c++, continuous number of muon in the diagnostic readout data
     * @param num number of muon in given input or output
     * @param ptCode
     * @param quality
     * @param sign
     * @param etaAddress
     * @param phiAddress
     * @param gbData
     */
    public MuonImplXml(int index, int num, int ptCode, int quality, int sign, int etaAddress, int phiAddress, int gbData) {
        super();
        this.index = index;
        this.num = num;
        this.ptCode = ptCode;
        this.quality = quality;
        this.sign = sign;
        this.etaAddress = etaAddress;
        this.phiAddress = phiAddress;
        this.gbData = gbData;
    }
    
    //num must be calculated by EventBuilder
    public MuonImplXml(Muon muon, int num) {
        super();
        this.num = num;
        this.index = muon.getIndex();
        this.ptCode = muon.getPtCode();
        this.quality = muon.getQuality();
        this.sign = muon.getSign();
        this.etaAddress = muon.getEtaAddress();
        this.phiAddress = muon.getPhiAddress();
        this.gbData = muon.getGBData();
    }
    
    @XmlAttribute(name="eta" )
    public int getEtaAddress() {
        return etaAddress;
    }
    
    public void setEtaAddress(int etaAddress) {
        this.etaAddress = etaAddress;
    }

    @XmlTransient
    public int getIndex() {
        return index;
    }
    
    public void setIndex(int index) {
        this.index = index;
    }
    
    @XmlAttribute(name="num")
	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}
    
    @XmlAttribute(name="phi")
    public int getPhiAddress() {
        return phiAddress;
    }
    
    public void setPhiAddress(int phiAddress) {
        this.phiAddress = phiAddress;
    }
    
    @XmlAttribute(name="pt")
    public int getPtCode() {
        return ptCode;
    }
    
    public void setPtCode(int ptCode) {
        this.ptCode = ptCode;
    }
    
    @XmlAttribute(name="qual")   
    public int getQuality() {
        return quality;
    }
    
    public void setQuality(int quality) {
        this.quality = quality;
    }
    
    @XmlAttribute(name="sign") 
    public int getSign() {
        return sign;
    }
    
    public void setSign(int sign) {
        this.sign = sign;
    }
    
    @XmlAttribute(name="gbD")
    public int getGBData() {
        return gbData;
    }

    public void setGBData(int gbData) {
        this.gbData = gbData;
    }

    @XmlAttribute(name="nv")
	public String getNotValid() {
		return notValid;
	}

	public void setNotValid(String validInfo) {
		this.notValid = validInfo;
	}
    
	@XmlTransient
    public void setNotValidChip(Chip notValidChip) {
        this.notValid = notValidChip.getType() + " " + notValidChip.getPosition()+ " " + this.notValid;
    }
	
    public int compareTo(Object o) { 
        final int BEFORE = -1;
        final int EQUAL = 0;
        final int AFTER = 1;
        if(this.getNum() == ((MuonImplXml)o).getNum()) {
            if(this.getQuality() == ((MuonImplXml)o).getQuality()) {
                if(this.getPtCode() == ((MuonImplXml)o).getPtCode()) {
                    //if(this.getSign() == ((MuonImplXml)o).getSign()) {
                        if(this.getEtaAddress() == ((MuonImplXml)o).getEtaAddress()) {
                            if(this.getPhiAddress() == ((MuonImplXml)o).getPhiAddress()) {
                                if(this.getGBData() == ((MuonImplXml)o).getGBData()) {
/*                                    if(this.getIndex() > ((MuonImplXml)o).getIndex())
                                        return AFTER;
                                    else if(this.getIndex() < ((MuonImplXml)o).getIndex())
                                        return BEFORE;    
                                    else 
                                        return EQUAL;*/                                    
                                    return EQUAL;
                                }
                                else if(this.getGBData() > ((MuonImplXml)o).getGBData())   
                                    return AFTER;
                                else 
                                    return BEFORE;
                            }
                            else if(this.getPhiAddress() > ((MuonImplXml)o).getPhiAddress())
                                return AFTER;
                            else 
                                return BEFORE;
                        }
                        else if(this.getEtaAddress() > ((MuonImplXml)o).getEtaAddress())
                            return AFTER;
                        else 
                            return BEFORE;
/*                    }
                    else if(this.getSign() > ((MuonImplXml)o).getSign())
                        return AFTER;
                    else 
                        return BEFORE;*/
                }
                else if(this.getPtCode() > ((MuonImplXml)o).getPtCode())
                    return AFTER;
                else 
                    return BEFORE;
            }
            else if (this.getQuality() > ((MuonImplXml)o).getQuality())
                return AFTER;
            else 
                return BEFORE;                              
        }
        else if(this.getNum() > ((MuonImplXml)o).getNum())
            return AFTER;
        else 
            return BEFORE;
    }
    
    public boolean equals(Object obj) {
        if(this.num != ((MuonImplXml)obj).getNum())
        	return false;
/*        if(this.index != ((MuonImplXml)obj).getIndex())
        	return false;*/
        if(this.ptCode != ((MuonImplXml)obj).getPtCode())
        	return false;
        if(this.quality != ((MuonImplXml)obj).getQuality())
        	return false;
/*        if(this.sign != ((MuonImplXml)obj).getSign())
        	return false;*/
        if(this.etaAddress != ((MuonImplXml)obj).getEtaAddress())
        	return false;
        if(this.phiAddress != ((MuonImplXml)obj).getPhiAddress())
        	return false;
        if(this.gbData != ((MuonImplXml)obj).getGBData())
        	return false;
        
        return true;
    }
    
    public String toString() {
    	String str = "            ";  	
    	str += "num " + num + " qu " + quality + " pt " + ptCode + " si " + sign + " et " + etaAddress + " ph " + phiAddress + " gb " + gbData;
    	return str;   	
    }
}
