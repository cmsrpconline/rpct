package rpct.datastream;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Transient;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.equipment.TriggerBoard;

/**
 * @author rpcdev
 *
 */
@XmlRootElement(name="testOptions")
public class TestOptionsXml {    
    public static class EventBuilderOptions {
        private boolean storeLbStripData = false;        
        private boolean storeLbCoderData = false;        
        private boolean storeLbLmuxData = false;
        private ReadoutItemId.LbDataSource propagateLbDataToOptoSource = ReadoutItemId.LbDataSource.none;        
        private int filteredBXsCnt = -1; //the numnber of BX which are stored from the readout
        
        public enum ChipDataSel {
            in,
            out,
            inAndOut;     
            
            boolean ifStore(String source) {
                if(source == "in") 
                    if(this == ChipDataSel.in || this == ChipDataSel.inAndOut)
                        return true;
                
                if(source == "out") 
                    if(this == ChipDataSel.out || this == ChipDataSel.inAndOut)
                        return true;
                
                return false;
            }
        }
        
        //which data should be taken from readout during event building
        private ChipDataSel pacDataSel = ChipDataSel.inAndOut;
        private ChipDataSel tbsDataSel = ChipDataSel.inAndOut;
        private ChipDataSel tcsDataSel = ChipDataSel.inAndOut;
        private ChipDataSel hsDataSel = ChipDataSel.inAndOut;
        private ChipDataSel fsDataSel = ChipDataSel.inAndOut;
        
        public EventBuilderOptions() {
            
        }
        
        public EventBuilderOptions(boolean storeLbStripData,
                boolean storeLbCoderData, boolean storeLbLmuxData) {
            super();
            this.storeLbStripData = storeLbStripData;
            this.storeLbCoderData = storeLbCoderData;
            this.storeLbLmuxData = storeLbLmuxData;
        }        
        
        @XmlAttribute(name="storeLbStripData")
        public boolean getStoreLbStripData() {
            return storeLbStripData;
        }
        public void setStoreLbStripData(boolean storeLbStripData) {
            this.storeLbStripData = storeLbStripData;
        }
        
        @XmlAttribute(name="storeLbCoderData")
        public boolean getStoreLbCoderData() {
            return storeLbCoderData;
        }
        public void setStoreLbCoderData(boolean storeLbCoderData) {
            this.storeLbCoderData = storeLbCoderData;
        }
        
        @XmlAttribute(name="storeLbLmuxData")
        public boolean getStoreLbLmuxData() {
            return storeLbLmuxData;
        }
        public void setStoreLbLmuxData(boolean storeLbLmuxData) {
            this.storeLbLmuxData = storeLbLmuxData;
        }

        @XmlAttribute(name="propagateLbDataToOptoSource")
        public ReadoutItemId.LbDataSource getPropagateLbDataToOptoSource() {
            return propagateLbDataToOptoSource;
        }

        public void setPropagateLbDataToOptoSource(
                ReadoutItemId.LbDataSource propagateLbDataToOptoSource) {
            this.propagateLbDataToOptoSource = propagateLbDataToOptoSource;
        }

        @XmlAttribute(name="filteredBXsCnt")
        public int getFilteredBXsCnt() {
            return filteredBXsCnt;
        }

        public void setFilteredBXsCnt(int filteredBXsCnt) {
            this.filteredBXsCnt = filteredBXsCnt;
        }

        @XmlAttribute
        public ChipDataSel getPacDataSel() {
            return pacDataSel;
        }

        public void setPacDataSel(ChipDataSel pacDataSel) {
            this.pacDataSel = pacDataSel;
        }

        @XmlAttribute
        public ChipDataSel getTbsDataSel() {
            return tbsDataSel;
        }

        public void setTbsDataSel(ChipDataSel tbsDataSel) {
            this.tbsDataSel = tbsDataSel;
        }

        @XmlAttribute
        public ChipDataSel getTcsDataSel() {
            return tcsDataSel;
        }

        
        public void setTcsDataSel(ChipDataSel tcsDataSel) {
            this.tcsDataSel = tcsDataSel;
        }

        @XmlAttribute
        public ChipDataSel getHsDataSel() {
            return hsDataSel;
        }

        public void setHsDataSel(ChipDataSel hsDataSel) {
            this.hsDataSel = hsDataSel;
        }

        @XmlAttribute
        public ChipDataSel getFsDataSel() {
            return fsDataSel;
        }

        public void setFsDataSel(ChipDataSel fsDataSel) {
            this.fsDataSel = fsDataSel;
        }               
    }
    
    //---------------------------------------------------------------------    
    public enum HardwareType {
        TOWER, CRATE, BOARD;
    }
    
    public static class ChipSource {
        private HardwareType hardwareType;
        private String hardwareName;
        private ChipType chipType;       
        private int chipPos = -1; //to have a possibility of selecting one PAC only
        
        public ChipSource() {
            
        }
        
        public ChipSource(HardwareType hardwareType, String hardwareName,
                ChipType chipType) {
            super();
            this.hardwareType = hardwareType;
            this.hardwareName = hardwareName;
            this.chipType = chipType;
        } 
        
        @XmlAttribute(name="hwType")
        public HardwareType getHardwareType() {
            return hardwareType;
        }
        public void setHardwareType(HardwareType hardwareType) {
            this.hardwareType = hardwareType;
        }
        
        @XmlAttribute(name="hwName")
        public String getHardwareName() {
            return hardwareName;
        }
        public void setHardwareName(String hardwareName) {
            this.hardwareName = hardwareName;
        }
        
        @XmlAttribute(name="chipType")
        public ChipType getChipType() {
            return chipType;
        }
        public void setChipType(ChipType chipType) {
            this.chipType = chipType;
        }

        @XmlAttribute(name="chipPos")
        public int getChipPos() {
            return chipPos;
        }

        public void setChipPos(int chipPos) {
            this.chipPos = chipPos;
        }
       
    }
    
    //-------------------------------------------------------------------------
    public static class BoardSource {
        private HardwareType hardwareType;
        private String hardwareName;
        private BoardType boardType;
        private String options; //options        

        public BoardSource() {
            
        }
                      
        public BoardSource(HardwareType hardwareType, String hardwareName,
                BoardType boardType) {
            super();
            this.hardwareType = hardwareType;
            this.hardwareName = hardwareName;
            this.boardType = boardType;
        }

        @XmlAttribute(name="hwType")
        public HardwareType getHardwareType() {
            return hardwareType;
        }
        public void setHardwareType(HardwareType hardwareType) {
            this.hardwareType = hardwareType;
        }
        
        @XmlAttribute(name="hwName")
        public String getHardwareName() {
            return hardwareName;
        }
        public void setHardwareName(String hardwareName) {
            this.hardwareName = hardwareName;
        }

        @XmlAttribute(name="boardType")
        public BoardType getBoardType() {
            return boardType;
        }

        public void setBoardType(BoardType boardType) {
            this.boardType = boardType;
        }
        
        @XmlAttribute(name="opt")
        public String getOptions() {
            return options;
        }

        public void setOptions(String options) {
            this.options = options;
        }
    }
    //-------------------------------------------------------------------------------
    public static class OpticalLink {
        private String tbName;
        private Integer tbInput;
        private String option;
        
        public OpticalLink() {
            
        }
        
        @XmlAttribute(name="tb")
        public String getTbName() {
            return tbName;
        }
        public void setTbName(String tbName) {
            this.tbName = tbName;
        }
        
        @XmlAttribute(name="tbIn")
        public Integer getTbInput() {
            return tbInput;
        }
        public void setTbInput(Integer tbInput) {
            this.tbInput = tbInput;
        }

        @XmlAttribute(name="opt")
        public String getOption() {
            return option;
        }

        public void setOption(String option) {
            this.option = option;
        }
        
        
    }
    //-------------------------------------------------------------------------------
/*    enum DiagType {
        PULSER, READOUT;
    }*/
    
    public static class SourcesList {
        private String forWhat;
        
        private List<ChipSource> chipSources = new ArrayList<ChipSource>();

        private List<BoardSource> boardSources = new  ArrayList<BoardSource>();
        
        public SourcesList() {
            
        }
        
        public SourcesList(String forWhat) {
            this.forWhat = forWhat;
        }
        
        @XmlAttribute(name="forWhat")
        public String getForWhat() {
            return forWhat;
        }
        public void setForWhat(String forWhat) {
            this.forWhat = forWhat;
        }

        @XmlElement(name="chipSource")
        public List<ChipSource> getChipSources() {
            return chipSources;
        }
        public void setChipSources(List<ChipSource> chipSources) {
            this.chipSources = chipSources;
        }
         
        @XmlElement(name="boardSource")
        public List<BoardSource> getBoardSources() {
            return boardSources;
        }

        public void setBoardSources(List<BoardSource> boardSources) {
            this.boardSources = boardSources;
        }                      
    }
    
    //-------------------------------------------------------------------------
    private EventBuilderOptions eventBuilderOptions;
    
    //private ChipSources pulserSource;
    //private ChipSources pulserSource;
    private List<SourcesList> chipSourcesList = new ArrayList<SourcesList>();
    private List<SourcesList> boardSourcesList = new ArrayList<SourcesList>();
    private List<OpticalLink> opticalLinks = new ArrayList<OpticalLink>();

    @XmlElement(name="eventBuilderOptions")
    public EventBuilderOptions getEventBuilderOptions() {
        return eventBuilderOptions;
    }

    public void setEventBuilderOptions(EventBuilderOptions eventBuilderOptions) {
        this.eventBuilderOptions = eventBuilderOptions;
    }
    
    @XmlElement(name="chipSources")
    public List<SourcesList> getChipSourcesList() {
        return chipSourcesList;
    }

    public void setChipSourcesList(List<SourcesList> chipSourcesList) {
        this.chipSourcesList = chipSourcesList;
    }
    
    @XmlElement(name="boardSources") 
    public List<SourcesList> getBoardSourcesList() {
        return boardSourcesList;
    }

    public void setBoardSourcesList(List<SourcesList> boardSourcesList) {
        this.boardSourcesList = boardSourcesList;
    }
    
    @XmlElement(name="optLink")
    public List<OpticalLink> getOpticalLinks() {
        return opticalLinks;
    }

    public void setOpticalLinks(List<OpticalLink> opticalLinks) {
        this.opticalLinks = opticalLinks;
    }

    @Transient
    public SourcesList getSourcesList(String forWhat) {
        for(SourcesList sourcesList : chipSourcesList) {
            if(sourcesList.forWhat.equals(forWhat))
                return sourcesList;
        }
        for(SourcesList sourcesList : boardSourcesList) {
            if(sourcesList.forWhat.equals(forWhat))
                return sourcesList;
        }
        throw new IllegalArgumentException("no SourcesList for " + forWhat + " found in the testOptionsXml");
    }
    
    public TestOptionsXml() {
        eventBuilderOptions = new EventBuilderOptions();
    }
    
    public static TestOptionsXml readFromFile(String xmlFileName) throws JAXBException {
        if (xmlFileName.equals("")) {
            System.out.println("TestOptionsXml.init : empty string of xmlFileName, TestOptionsXml will have defoult values");
        }
            
        JAXBContext context = JAXBContext.newInstance(TestOptionsXml.class);
        
        Unmarshaller um = context.createUnmarshaller();
        return (TestOptionsXml) um.unmarshal(new File(xmlFileName));
    }

    private Map<String, List<LinkBox> > linkBoxesByTower = null;
    boolean lastEnabledOnly = true;
    
    @Transient
    public Map<String, List<LinkBox> > getLinkBoxesByTower(EquipmentDAO equipmentDAO, boolean enabledOnly) throws DataAccessException {
        if(linkBoxesByTower != null && enabledOnly == lastEnabledOnly) {
            return linkBoxesByTower;  
        }

        linkBoxesByTower = equipmentDAO.getLinkBoxesByTower(equipmentDAO, enabledOnly);
        lastEnabledOnly = enabledOnly;

        return linkBoxesByTower;
    }
    
    private Map<String, List<Chip> > chipsMap = new HashMap<String, List<Chip> >();
    
    @Transient
    public List<Chip> getChips(TestOptionsXml.ChipSource chipSource, EquipmentDAO equipmentDAO) throws DataAccessException, RemoteException, ServiceException {           
        List<Chip> chips =  new ArrayList<Chip>();
        Map<String, List<LinkBox> > linkBoxesByTower = getLinkBoxesByTower(equipmentDAO, true);

        if(chipSource.getHardwareType() == TestOptionsXml.HardwareType.TOWER) {
            List<LinkBox> linkBoxesInTower = linkBoxesByTower.get(chipSource.getHardwareName());
            if(linkBoxesInTower == null) {
                throw new IllegalArgumentException(" tower " + chipSource.getHardwareName() + "not found");
            }

            for(Crate crate : linkBoxesInTower) {                
                List<Chip> chipsFromCrate = equipmentDAO.getChipsByTypeAndCrate(chipSource.getChipType(), crate, true);
                if(chipSource.getChipPos() == -1)
                    chips.addAll(chipsFromCrate);
                else {
                    for(Chip chip : chipsFromCrate) {
                        if (chip.getPosition() == chipSource.getChipPos())
                            chips.add(chip);
                    }
                }
            }
        }
        else if(chipSource.getHardwareType() == TestOptionsXml.HardwareType.CRATE) {
            Crate crate = equipmentDAO.getCrateByName(chipSource.getHardwareName());
            if(crate == null) {
                throw new IllegalArgumentException(" crate " + chipSource.getHardwareName() + "not found");
            }


            List<Chip> chipsFromCrate = equipmentDAO.getChipsByTypeAndCrate(chipSource.getChipType(), crate, true);
            if(chipSource.getChipPos() == -1)
                chips.addAll(chipsFromCrate);
            else {
                for(Chip chip : chipsFromCrate) {
                    if (chip.getPosition() == chipSource.getChipPos())
                        chips.add(chip);
                }
            }
        }
        else if(chipSource.getHardwareType() == TestOptionsXml.HardwareType.BOARD) {
            Board board = equipmentDAO.getBoardByName(chipSource.getHardwareName());
            if(board == null) {
                throw new IllegalArgumentException(" board " + chipSource.getHardwareName() + "not found");
            }
            List<Chip> chipsFromBoard = equipmentDAO.getChipsByTypeAndBoard(chipSource.getChipType(), board, true);
            if(chipSource.getChipPos() == -1)
                chips.addAll(chipsFromBoard);
            else {
                for(Chip chip : chipsFromBoard) {
                    if (chip.getPosition() == chipSource.getChipPos())
                        chips.add(chip);
                }
            }
        }

        return chips;
    }

    @Transient
    public List<Chip> getChips(String forWhat, EquipmentDAO equipmentDAO) throws DataAccessException, RemoteException, ServiceException {       
        List<Chip> chips = chipsMap.get(forWhat);
        if(chips != null)
            return chips;
            
        chips =  new ArrayList<Chip>();
        Map<String, List<LinkBox> > linkBoxesByTower = getLinkBoxesByTower(equipmentDAO, true);
        
        for(TestOptionsXml.ChipSource chipSource : getSourcesList(forWhat).getChipSources()) {
            chips.addAll(getChips(chipSource, equipmentDAO));
            /*if(chipSource.getHardwareType() == TestOptionsXml.HardwareType.TOWER) {
                List<LinkBox> linkBoxesInTower = linkBoxesByTower.get(chipSource.getHardwareName());
                if(linkBoxesInTower == null) {
                    throw new IllegalArgumentException(" tower " + chipSource.getHardwareName() + "not found");
                }

                for(Crate crate : linkBoxesInTower) {                
                    List<Chip> chipsFromCrate = equipmentDAO.getChipsByTypeAndCrate(chipSource.getChipType(), crate, true);
                    if(chipSource.getChipPos() == -1)
                        chips.addAll(chipsFromCrate);
                    else {
                        for(Chip chip : chipsFromCrate) {
                            if (chip.getPosition() == chipSource.getChipPos())
                                chips.add(chip);
                        }
                    }
                }
            }
            else if(chipSource.getHardwareType() == TestOptionsXml.HardwareType.CRATE) {
                Crate crate = equipmentDAO.getCrateByName(chipSource.getHardwareName());
                if(crate == null) {
                    throw new IllegalArgumentException(" crate " + chipSource.getHardwareName() + "not found");
                }
                    
                
                List<Chip> chipsFromCrate = equipmentDAO.getChipsByTypeAndCrate(chipSource.getChipType(), crate, true);
                if(chipSource.getChipPos() == -1)
                    chips.addAll(chipsFromCrate);
                else {
                    for(Chip chip : chipsFromCrate) {
                        if (chip.getPosition() == chipSource.getChipPos())
                            chips.add(chip);
                    }
                }
            }
            else if(chipSource.getHardwareType() == TestOptionsXml.HardwareType.BOARD) {
                Board board = equipmentDAO.getBoardByName(chipSource.getHardwareName());
                if(board == null) {
                    throw new IllegalArgumentException(" board " + chipSource.getHardwareName() + "not found");
                }
                List<Chip> chipsFromBoard = equipmentDAO.getChipsByTypeAndBoard(chipSource.getChipType(), board, true);
                if(chipSource.getChipPos() == -1)
                    chips.addAll(chipsFromBoard);
                else {
                    for(Chip chip : chipsFromBoard) {
                        if (chip.getPosition() == chipSource.getChipPos())
                            chips.add(chip);
                    }
                }
            }*/
        }       
        
        chipsMap.put(forWhat, chips);
        return chips;
    }
    
    /**
     * @param boardSource
     * @param equipmentDAO
     * @param enabledOnly if true gives only BOARDS which are enabled
     * @return
     * @throws DataAccessException
     * @throws RemoteException
     * @throws ServiceException
     */
    @Transient
    public Set<Board> getBoards(TestOptionsXml.BoardSource boardSource, EquipmentDAO equipmentDAO, boolean enabledOnly) throws DataAccessException, RemoteException, ServiceException {
        Set<Board> boards = new HashSet<Board>();
        
        Map<String, List<LinkBox> > linkBoxesByTower = getLinkBoxesByTower(equipmentDAO, enabledOnly);

        if(boardSource.getHardwareType() == TestOptionsXml.HardwareType.TOWER) {
            List<LinkBox> linkBoxesInTower = linkBoxesByTower.get(boardSource.getHardwareName());
            if(linkBoxesInTower == null) {
                throw new IllegalArgumentException(" tower " + boardSource.getHardwareName() + " not found");
            }

            for(Crate crate : linkBoxesInTower) {     
                for(Board board : crate.getBoards()) {
                    if(board.getType() == boardSource.getBoardType()) {
                        if(enabledOnly) {
                            if(board.getDisabled() == null) {
                                boards.add(board);
                            }
                        }
                        else {
                            boards.add(board);
                        }
                    }
                }
            }
        }
        else if(boardSource.getHardwareType() == TestOptionsXml.HardwareType.CRATE) {
            Crate crate = equipmentDAO.getCrateByName(boardSource.getHardwareName());
            for(Board board : crate.getBoards()) {
                if(board.getType() == boardSource.getBoardType()) {
                    if(enabledOnly) {
                        if(board.getDisabled() == null) {
                            boards.add(board);
                        }
                    }
                    else {
                        boards.add(board);
                    }
                }
            }
        }
        else if(boardSource.getHardwareType() == TestOptionsXml.HardwareType.BOARD) {
            Board board = equipmentDAO.getBoardByName(boardSource.getHardwareName());
            if(board.getType() == boardSource.getBoardType()) {
                if(enabledOnly) {
                    if(board.getDisabled() == null) {
                        boards.add(board);
                    }
                }
                else {
                    boards.add(board);
                }
            }
            else
                throw new IllegalArgumentException("the requested board is not of requested type");
        }

        return boards;
    }
    
    @Transient
    public Set<Board> getBoards(String forWhat, EquipmentDAO equipmentDAO, boolean enabledOnly) throws DataAccessException, RemoteException, ServiceException {
        Set<Board> boards = new HashSet<Board>();
        
        //Map<String, List<LinkBox> > linkBoxesByTower = getLinkBoxesByTower(equipmentDAO, enabledOnly);
        
        for(TestOptionsXml.BoardSource boardSource : getSourcesList(forWhat).getBoardSources()) {
            boards.addAll(getBoards(boardSource, equipmentDAO, enabledOnly));
            /*if(boardSource.getHardwareType() == TestOptionsXml.HardwareType.TOWER) {
                List<LinkBox> linkBoxesInTower = linkBoxesByTower.get(boardSource.getHardwareName());
                if(linkBoxesInTower == null) {
                    throw new IllegalArgumentException(" tower " + boardSource.getHardwareName() + "not found");
                }

                for(Crate crate : linkBoxesInTower) {     
                    for(Board board : crate.getBoards()) {
                        if(board.getType() == boardSource.getBoardType())
                            boards.add(board);
                    }
                }
            }
            else if(boardSource.getHardwareType() == TestOptionsXml.HardwareType.CRATE) {
                Crate crate = equipmentDAO.getCrateByName(boardSource.getHardwareName());
                for(Board board : crate.getBoards()) {
                    if(board.getType() == boardSource.getBoardType())
                        boards.add(board);
                }
            }
            else if(boardSource.getHardwareType() == TestOptionsXml.HardwareType.BOARD) {
                Board board = equipmentDAO.getBoardByName(boardSource.getHardwareName());
                if(board.getType() == boardSource.getBoardType())
                    boards.add(board);
                else
                    throw new IllegalArgumentException("the requested board is not of requested type");
            }*/
        }       
        return boards;
    } 
    
    @Transient
    public
    void addChips(String forWhat, HardwareType hardwareType, String hardwareName,
            ChipType chipType) {
        
        SourcesList selSourcesList = null;
        for(SourcesList sourcesList : boardSourcesList) {
            if(sourcesList.getForWhat().equals(forWhat)) {
                selSourcesList = sourcesList;
                break;
            }
        }
        if(selSourcesList == null)
            selSourcesList = new SourcesList(forWhat);

        selSourcesList.getChipSources().add(new ChipSource(hardwareType, hardwareName, chipType));

        boardSourcesList.add(selSourcesList);
    }      
    
    @Transient
    public List<LinkConn> getLinkConns(EquipmentDAO equipmentDAO) throws DataAccessException {
        List<LinkConn> linkConns = new ArrayList<LinkConn>();
        for(OpticalLink opticalLink : opticalLinks) {
            TriggerBoard triggerBoard = (TriggerBoard)equipmentDAO.getBoardByName(opticalLink.getTbName());
            if(triggerBoard == null) {
                throw new IllegalArgumentException("TriggerBoard with name " + opticalLink.getTbName() + " not found");
            }
            LinkConn conn = triggerBoard.getLinkConn(opticalLink.getTbInput());
            if(conn == null) {
                throw new IllegalArgumentException("TriggerBoard with name " + opticalLink.getTbName() + " has no link for input " + opticalLink.getTbInput());
            }
            linkConns.add(conn);
        }
        
        return linkConns;
    }
    
    //----------------------------------------------------------------
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        
        final File xmlFile = new File("tesetOptions.xml");
        try {
            JAXBContext context1 = JAXBContext.newInstance(TestOptionsXml.class);
            
            
            System.out.println("writing to the file " + xmlFile);
            Marshaller m = context1.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);                                              
            
            TestOptionsXml testOptionsXml = new TestOptionsXml();
            
            TestOptionsXml.SourcesList pulserSource = new TestOptionsXml.SourcesList("pulsers");            
            List<ChipSource> chipSources = new ArrayList<ChipSource>();
            chipSources.add(new ChipSource(HardwareType.CRATE, "TC_9", ChipType.OPTO));
            pulserSource.setChipSources(chipSources);            
            testOptionsXml.getChipSourcesList().add(pulserSource);
            
            
            TestOptionsXml.SourcesList recSource = new TestOptionsXml.SourcesList("linkRec");
            List<BoardSource> boardSources = new ArrayList<BoardSource>();
            boardSources.add(new BoardSource(HardwareType.CRATE, "TC_9", BoardType.TRIGGERBOARD));
            recSource.setBoardSources(boardSources);
            testOptionsXml.getBoardSourcesList().add(recSource);
            
            
            BoardSource boardSource = new BoardSource(HardwareType.CRATE, "TC_9", BoardType.TRIGGERBOARD);
            System.out.println("aaa " + boardSource.toString());
            
            testOptionsXml.setEventBuilderOptions(new EventBuilderOptions(false, true, true));
            m.marshal(testOptionsXml, new FileOutputStream(xmlFile));
            
            Unmarshaller um = context1.createUnmarshaller();
            TestOptionsXml testOptionsXml1 = (TestOptionsXml) um.unmarshal(xmlFile);

        } catch (JAXBException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        };

    }

};