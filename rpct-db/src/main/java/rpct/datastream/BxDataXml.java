package rpct.datastream;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="bxData")
public class BxDataXml implements AutoMapValueType {
    @XmlAttribute(name="num")
    private int number;
    
    private AutoMap<LbDataXml> lbData = new AutoMap<LbDataXml>();;    
    
    private AutoMap<TcDataXml> tcData = new AutoMap<TcDataXml>();;
    
    private AutoMap<HalfSortDataXml> halfSortData = new AutoMap<HalfSortDataXml>();
    
    private AutoMap<FinalSortDataXml> finalSortData = new AutoMap<FinalSortDataXml>();
    
    public BxDataXml() {
    }

    public BxDataXml(int number) {
        this.number = number;
    }
        
    //@XmlTransient
    public int getNumber() {
        return number;
    }

    @XmlTransient
    public void setNumber(int number) {
        this.number = number;
    }

    public Integer getKey() {
        return number;
    }

    @XmlElement(name="lb")
    AutoMap<LbDataXml> getLbData() {
        return lbData;
    } 
    
    public void setLbData(AutoMap<LbDataXml> lbData) {
        this.lbData = lbData;
    }
        
    @XmlElement(name="tc")
    public AutoMap<TcDataXml> getTcData() {
        return tcData;
    } 
    
    public void setTcData(AutoMap<TcDataXml> tcData) {
        this.tcData = tcData;
    }
    
    LbDataXml getOrAddLbData(int id) {
        LbDataXml lbDataXml = lbData.get(id);
        if(lbDataXml == null) {
            lbDataXml = new LbDataXml(id);
            lbData.add(lbDataXml);
        }
        return lbDataXml;
    }
    
    TcDataXml getOrAddTcData(int number) {
        TcDataXml tcDataXml = tcData.get(number);
        if(tcDataXml == null) {
            tcDataXml = new TcDataXml(number);
            tcData.add(tcDataXml);
        }
        return tcDataXml;
    }

    @XmlElement(name="hs")
    public AutoMap<HalfSortDataXml> getHalfSortData() {
        return halfSortData;
    }

    public void setHalfSortData(AutoMap<HalfSortDataXml> halfSortData) {
        this.halfSortData = halfSortData;
    }
    
    HalfSortDataXml getOrAddHalfSortData(int number, int be) {
        HalfSortDataXml hsDataXml = halfSortData.get(HalfSortDataXml.createKey(number, be));
        if(hsDataXml == null) {
            hsDataXml = new HalfSortDataXml(number, be);
            halfSortData.add(hsDataXml);
        }
        return hsDataXml;
    }

    @XmlElement(name="fs")
    public AutoMap<FinalSortDataXml> getFinalSortData() {
        return finalSortData;
    }

    public void setFinalSortData(AutoMap<FinalSortDataXml> finalSortData) {
        this.finalSortData = finalSortData;
    }
    
    FinalSortDataXml getOrAddFinalSortData(int be) {
        FinalSortDataXml fsDataXml = finalSortData.get(be);
        if(fsDataXml == null) {
            fsDataXml = new FinalSortDataXml(be);
            finalSortData.add(fsDataXml);
        }
        return fsDataXml;
    }
    
/*    OptLinkDataXml getOptLinkData(int tcNum, int tbNum, int optLinkNum) {
    	TcDataXml tc = tcData.get(tcNum);
    	if(tc!= null) {
    		TbDataXml tb = tc.getTbData().get(tbNum);
    		if(tb != null) {
    			return tb.getOptLinks().get(optLinkNum);
    		}
    	}
    	return null;
    }*/
    
    void addOptLinkData(ReadoutItemId id, LMuxBxDataImplXml.Set linkData) {
        TcDataXml tc = getOrAddTcData(id.getTcNum());          
        TbDataXml tb = tc.getOrAddTbData(id.getTbNum());
        tb.getOrAddOptLink(id.getOptLinkNum()).setLinkData(linkData);               
    }
    
    OptLinkDataXml getOptLinkData(ReadoutItemId id) {
        TcDataXml tc = tcData.get(id.getTcNum());
        if(tc!= null) {
            TbDataXml tb = tc.getTbData().get(id.getTbNum());
            if(tb != null) {
                return tb.getOptLinks().get(id.getOptLinkNum());
            }
        }
        return null;
    }
    
/*    PacDataXml getPacOutData(int tcNum, int tbNum, int pacNum) {
    	TcDataXml tc = tcData.get(tcNum);
    	if(tc!= null) {
    		TbDataXml tb = tc.getTbData().get(tbNum);
    		if(tb != null) {
    			return tb.getPacs().get(pacNum);
    		}
    	}
    	return null;
    }*/

    public MuonsSet getMuons(ReadoutItemId id) {
        if(id.getType() == ReadoutItemId.Type.pacOut) {
            TcDataXml tc = tcData.get(id.getTcNum());
            if(tc!= null) {
                TbDataXml tb = tc.getTbData().get(id.getTbNum());
                if(tb != null) {                	
                	return tb.getPacs().get(id.getPacNum());
                }
            }
        }
        if(id.getType() == ReadoutItemId.Type.tbOut) {
            TcDataXml tc = tcData.get(id.getTcNum());
            if(tc!= null) {
                TbDataXml tb = tc.getTbData().get(id.getTbNum());
                if(tb != null) {
                    return tb.getGbSortMuons();
                }
            }
        }
        if(id.getType() == ReadoutItemId.Type.tcOut) {
            TcDataXml tc = tcData.get(id.getTcNum());
            if(tc != null) {
                return tc.getGbSortMuons();
            }
        }
        if(id.getType() == ReadoutItemId.Type.hsOut) {
            HalfSortDataXml hs = halfSortData.get(id.getHsKey());
            return hs;
        }
        if(id.getType() == ReadoutItemId.Type.fsOut) {
            FinalSortDataXml fs = finalSortData.get(id.getBe());
            return fs;
        }
        
        return null;
    }
    
    public void putMuons(ReadoutItemId id, MuonsSet muons) {
    	MuonsSet muonsSet;
        if(id.getType() == ReadoutItemId.Type.pacOut) {            
            TcDataXml tcData = getOrAddTcData(id.getTcNum());
            TbDataXml tbData = tcData.getOrAddTbData(id.getTbNum()); 
            muonsSet = tbData.getOrAddPac(id.getPacNum());
        }    	
        else if(id.getType() == ReadoutItemId.Type.tbOut) {            
            TcDataXml tcData = getOrAddTcData(id.getTcNum());
            TbDataXml tbData = tcData.getOrAddTbData(id.getTbNum()); 
            muonsSet = tbData.getGbSortMuons();
        }
        else if(id.getType() == ReadoutItemId.Type.tcOut) {
            TcDataXml tcData = getOrAddTcData(id.getTcNum());
            muonsSet = tcData.getGbSortMuons();
        }
        else if(id.getType() == ReadoutItemId.Type.hsOut) {
            HalfSortDataXml hs = getOrAddHalfSortData(id.getHsNum(), id.getBe());
            muonsSet = hs;
            
        }
        else if(id.getType() == ReadoutItemId.Type.fsOut) {
            FinalSortDataXml fs = getOrAddFinalSortData(id.getBe());
            muonsSet = fs;
        }
        else {
        	throw new RuntimeException("id is not of type which contain muons");
        }
        
        muonsSet.getMuons().addAll(muons.getMuons());
    }
}
