package rpct.datastream;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import rpct.datastream.ReadoutItemId.LbDataSource;
import rpct.db.DataAccessException;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.xdaqaccess.diag.BxData;
import rpct.xdaqaccess.diag.DiagnosticReadoutData;
import rpct.xdaqaccess.diag.DiagnosticReadoutEvent;
import rpct.xdaqaccess.diag.LMuxBxData;
import rpct.xdaqaccess.diag.Muon;
import rpct.xdaqaccess.synchro.DefOut;

/**
 * @author Karol
 * 
 */
public class EventBuilder {
    private static int lbStripDataOffset = 0;

    private static int lbCoderDataOffset = RpctDelays.LB_CODER_LATENCY;
    
    private static int lbMuxDataOffset = RpctDelays.LB_MUXER_LATENCY;

    private static int pacOutMuonsOffset = RpctDelays.PAC_LATENCY;

    private static int tbSortInMuonsOffset = 0;

    private static int tbSortOutMuonsOffset = tbSortInMuonsOffset + RpctDelays.TBSORT_LATENCY;

    private static int tcSortInMuonsOffset = 0;

    private static int tcSortOutMuonsOffset = tcSortInMuonsOffset + RpctDelays.TCSORT_LATENCY;

    private static int hsInMuonsOffset = 0; // + 10

    private static int hsOutMuonsOffset = hsInMuonsOffset + RpctDelays.HSORT_LATENCY;

    private static int fsInMuonsOffset = 0; //hsOutMuonsOffset

    private static int fsOutMuonsOffset = fsInMuonsOffset + RpctDelays.FSORT_LATENCY;
    
    private static HashMap<ChipType, Integer>  bc0Delay = new HashMap<ChipType, Integer>();
    
    private boolean storeLbStripData = true;
    
    private boolean storeLbCoderData = false;
    
    private boolean storeLbLmuxData = false;
    
    private boolean filterBXs = true;
    
    private int filteredBXsCnt = 5;
    
    static private int eventsPerReadout = 7;
    
    private boolean demultiplex = true;
    
    ReadoutItemId.LbDataSource propagateLbDataToOptoSource = ReadoutItemId.LbDataSource.lmux;
    
    static {
        bc0Delay.put(ChipType.SYNCODER,    RpctDelays.LB_BC0_DELAY);
    	bc0Delay.put(ChipType.OPTO,        RpctDelays.OPTO_BC0_DELAY);
    	bc0Delay.put(ChipType.PAC,         RpctDelays.PAC_BC0_DELAY);
    	bc0Delay.put(ChipType.RMB,         RpctDelays.RMB_BC0_DELAY);
    	bc0Delay.put(ChipType.GBSORT,      RpctDelays.TBSORT_BC0_DELAY);
    	bc0Delay.put(ChipType.TCSORT,      RpctDelays.TCSORT_BC0_DELAY);
    	bc0Delay.put(ChipType.HALFSORTER,  RpctDelays.HSB_BC0_DELAY);
    	bc0Delay.put(ChipType.FINALSORTER, RpctDelays.FSB_BC0_DELAY);
    	bc0Delay.put(ChipType.TTUOPTO,     RpctDelays.OPTO_BC0_DELAY);
    }

    // all in bx units

    private DataStreamXml sourceDataStream;

    /*
     * the data from reaadouts are stored here. If the data for given readput
     * item were already put to the outputDataStream, the incoming data are
     * compared with prevoiusly added (the same data can be readot by many
     * readouts)
     */
    private DataStreamXml outDataStream = new DataStreamXml();

    private Chip curentChip;

    // current events
    EventXml sourceEvent;

    EventXml outEvent;

    private EquipmentDAO equipmentDAO;
    
    private Map<Integer, Integer> lmuxInDelay; //<chipId, delay>

    private TestOptionsXml testOptions;
    
    public EventBuilder(EquipmentDAO equipmentDAO, TestOptionsXml testOptions) {
        this.equipmentDAO = equipmentDAO;
        
        storeLbStripData = testOptions.getEventBuilderOptions().getStoreLbStripData();
        storeLbCoderData = testOptions.getEventBuilderOptions().getStoreLbCoderData();
        storeLbLmuxData = testOptions.getEventBuilderOptions().getStoreLbLmuxData();
        propagateLbDataToOptoSource = testOptions.getEventBuilderOptions().getPropagateLbDataToOptoSource();
        filteredBXsCnt = testOptions.getEventBuilderOptions().getFilteredBXsCnt();
        if(filteredBXsCnt == -1) 
            filterBXs  = false;
        else
            filterBXs  = true;
       
        this.testOptions = testOptions;
    }

    /**
     * if the methomd is callad, the data from readouts will be also compared
     * withe the data from loaded xml
     * 
     * @param xmlFileName
     * @throws JAXBException
     */
    public void loadSourceData(String xmlFileName) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(DataStreamXml.class);
        System.out.println("reading the " + xmlFileName);
        Unmarshaller um = context.createUnmarshaller();
        sourceDataStream = (DataStreamXml) um.unmarshal(new File(xmlFileName));
/*        
        System.out.println("data1.getEvents().size() " + sourceDataStream.getEvents().size());
        for(EventXml eventXml : sourceDataStream.getEvents()) {
        	System.out.println(" eventXml.getBxData().size() "  + eventXml.getBxData().size());
        	for(BxDataXml bxDataXml : eventXml.getBxData()) {
        		System.out.println("bxDataXml.getHalfSortData().size() " + bxDataXml.getHalfSortData().size());
        	}
        }*/
    }
    
    public void loadSourceData(String linkDataXmlFileName, String muonDataXmlFileName) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(DataStreamXml.class);
        Unmarshaller um = context.createUnmarshaller();
        System.out.println("reading the " + linkDataXmlFileName);        
        sourceDataStream = (DataStreamXml) um.unmarshal(new File(linkDataXmlFileName));
        
        System.out.println("reading the " + muonDataXmlFileName);        
        DataStreamXml muonDataStream = (DataStreamXml) um.unmarshal(new File(muonDataXmlFileName));
        sourceDataStream.margeMuonData(muonDataStream);
        System.out.println("the data from files " + linkDataXmlFileName + " " + muonDataXmlFileName + "were marged"); 
    }

    enum ReadoutMode {
    	SNAPSHOT,
    	DAQLIKE
    }
    
    void processEvent(DiagnosticReadoutEvent event, ReadoutMode mode) {
    	// currentEventXml = dataStream.getOrAddEvent(event.getEvNum(),
    	// event.getBcn());

    	if (sourceDataStream != null)
    		sourceEvent = sourceDataStream.getOrAddEvent(0, 0); // ~!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    	int bcn = (event.getBcn() + RpctDelays.BX_IN_ORBIT + bc0Delay.get(curentChip.getType()) ) % RpctDelays.BX_IN_ORBIT;
    	
    	int eventNum = 0;
    	if(mode == ReadoutMode.DAQLIKE)
    		eventNum = event.getEvNum();
    	else {
    		eventNum = 0;
    		//this trick is needed, because for the RMB it isnot possible to synchronize the event number in the snapshot mode 
    	}
    	
    	outEvent = outDataStream.getOrAddEvent(eventNum, bcn);
    	if (outEvent.getBxNum() != bcn)  {
    		DefOut.out.println("device " + curentChip.getBoard().getName() + " " + curentChip.getType() + " " + curentChip.getPosition() 
    				+ " Id " + curentChip.getId()  +  " evNum " + event.getEvNum() 
    				+ " : the BCN of current event = " +  bcn + " (" + event.getBcn()  + " in hardware) " 
    				+ ", is not equall to the bxNum of previoulsy added event = " + outEvent.getBxNum());
    		/*         
            throw new RuntimeException("the Bcn of current event = " +  event.getBcn() + ": deviceId "
                    + curentChip.getId() + " evNum " + event.getEvNum()
                    + ", is not equall to the bxNum of previoulsy added event = " + outEvent.getBxNum());*/
    	}

        if (curentChip.getType() == ChipType.SYNCODER) {
            processSyncoderData(event.getBxData());
        } else if (curentChip.getType() == ChipType.OPTO) {
            processOptoData(event.getBxData());
        } else if (curentChip.getType() == ChipType.RMB) {
            processRmbData(event.getBxData());
        } else if (curentChip.getType() == ChipType.PAC) {
            processPacData(event.getBxData());
        } else if (curentChip.getType() == ChipType.GBSORT) {
            processTbSortData(event.getBxData());
        } else if (curentChip.getType() == ChipType.TCSORT) {
            processTcSortData(event.getBxData());
        }
        else if(curentChip.getType() == ChipType.HALFSORTER) {
            processHalfSortData(event.getBxData());
        } 
        else if(curentChip.getType() == ChipType.FINALSORTER) {
            processFinalSortData(event.getBxData());
        } 
    }

/*
 * dziala takze dla danych stripowych
 */
    public void propagateLbDataToOpto(ReadoutItemId lbId, TreeMap<Integer, LMuxBxDataImplXml.Set> synCoderDataMap) {
        LinkBoard lb = (LinkBoard) lbId.getChip().getBoard();
        LinkBoard mlb = lb.getMaster();
        for(LinkConn linkConn : mlb.getLinkConns() ) {            
            if(linkConn.getOpto().getBoard().getCrate().getName().contains("TC_9") ||
                    linkConn.getOpto().getBoard().getCrate().getName().contains("TC_1")) { //TODO usunac albo dac waronek ze tylko enabled links
                ReadoutItemId optLinkId = ReadoutItemId.createOptLinkId(linkConn);
                compareOrAddLmuxData(optLinkId, synCoderDataMap);
            }
        }
    }
    
    void processSyncoderData(BxData[] bxDatas) {
        // separate maps for strip and coder/lmux data
        
        TreeMap<ReadoutItemId.LbDataSource, TreeMap<Integer, LMuxBxDataImplXml.Set> > synCoderDataMaps = new TreeMap<ReadoutItemId.LbDataSource, TreeMap<Integer, LMuxBxDataImplXml.Set> >();
        
        int bxDataNum = 0;
        for (BxData bxData : bxDatas) {   
            for (LMuxBxData lmuxBxData : bxData.getLMuxBxData()) {   
                if (lmuxBxData.getIndex() == ReadoutItemId.LbDataSource.stripData.getIntVal()) { // strip data
                    int bxPos = bxData.getIndex() - lbStripDataOffset;     
                    if(!filterBXs ||  (bxPos >= 0 && bxPos < filteredBXsCnt)) {
                        LMuxBxDataImplXml lMuxBxDataXml = new LMuxBxDataImplXml(lmuxBxData);

                        lMuxBxDataXml.setLbNum(((LinkBoard)curentChip.getBoard()).getChannelNum());
                        lMuxBxDataXml.setPartitionDelay(0); //!!!!!!!!!!!!!!!!!!!!!
                        
                        TreeMap<Integer, LMuxBxDataImplXml.Set> synCoderDataMap = synCoderDataMaps.get(ReadoutItemId.LbDataSource.fromInt(lmuxBxData.getIndex()));
                        if(synCoderDataMap == null) {
                            synCoderDataMap = new TreeMap<Integer, LMuxBxDataImplXml.Set>();
                            synCoderDataMaps.put(ReadoutItemId.LbDataSource.stripData, synCoderDataMap);
                        }
                        if(synCoderDataMap.get(bxPos) == null) {
                            synCoderDataMap.put(bxPos, new LMuxBxDataImplXml.Set());
                        }
                        if (synCoderDataMap.get(bxPos).add(lMuxBxDataXml) == false) {
                            throw new RuntimeException("the synCoderDataMap already contains lMuxBxDataXml: " + lMuxBxDataXml);
                        }  
                    }
                }
                else { // extedned data                	
                    int bxPos = bxData.getIndex();
                    if(demultiplex)
                    	bxPos -= lmuxBxData.getPartitionDelay(); // decoding
                    if(!filterBXs ||  (bxPos >= 0 && bxPos < filteredBXsCnt)) {
                        LMuxBxDataImplXml lMuxBxDataXml = new LMuxBxDataImplXml(lmuxBxData);
                        if(demultiplex)
                        	lMuxBxDataXml.setPartitionDelay(0); // /after decodingthe
                        // partition delay is
                        // 0!!!!!!

                        if(lmuxBxData.getIndex() == ReadoutItemId.LbDataSource.coder.getIntVal())  {
                            lMuxBxDataXml.setLbNum(((LinkBoard)curentChip.getBoard()).getChannelNum());
                            bxPos -= lbCoderDataOffset;
                        }
                        else if(lmuxBxData.getIndex() == ReadoutItemId.LbDataSource.lmux.getIntVal()) {
                            bxPos -= (lbCoderDataOffset + lbMuxDataOffset);
                            bxPos -= lmuxInDelay.get(curentChip.getId());
                        }

                        TreeMap<Integer, LMuxBxDataImplXml.Set> synCoderDataMap = synCoderDataMaps.get(ReadoutItemId.LbDataSource.fromInt(lmuxBxData.getIndex()));
                        if(synCoderDataMap == null) {
                            synCoderDataMap = new TreeMap<Integer, LMuxBxDataImplXml.Set>();
                            synCoderDataMaps.put(ReadoutItemId.LbDataSource.fromInt(lmuxBxData.getIndex()), synCoderDataMap);
                        }
                        if(synCoderDataMap.get(bxPos) == null) {
                            synCoderDataMap.put(bxPos, new LMuxBxDataImplXml.Set());
                        }
                        if (synCoderDataMap.get(bxPos).add(lMuxBxDataXml) == false) {
                        	System.out.println("ERROR " + curentChip.getBoard().getName() + " BXN "+ bxData.getIndex() + " index " + lmuxBxData.getIndex() 
                                    + " the synCoderDataMap already contains lMuxBxDataXml: " + lMuxBxDataXml);
                        	
                        	throw new RuntimeException(curentChip.getBoard().getName() + " BXN "+ bxData.getIndex() + " index " + lmuxBxData.getIndex() 
                                    + " the synCoderDataMap already contains lMuxBxDataXml: " + lMuxBxDataXml);                                                      
                        }  
                    }
                }
            }
            bxDataNum++;
        }
                
        for(Map.Entry<ReadoutItemId.LbDataSource, TreeMap<Integer, LMuxBxDataImplXml.Set> >  es : synCoderDataMaps.entrySet()) {
            ReadoutItemId lbId = ReadoutItemId.createLbId(curentChip, es.getKey());
            if(es.getKey() == ReadoutItemId.LbDataSource.stripData) { 
                if(storeLbStripData)            
                    compareOrAddLmuxData(lbId, es.getValue());
                
                if(propagateLbDataToOptoSource == ReadoutItemId.LbDataSource.stripData)
                    propagateLbDataToOpto(lbId, es.getValue());
            }
            else if(es.getKey() == ReadoutItemId.LbDataSource.coder) {
                if(storeLbCoderData)
                    compareOrAddLmuxData(lbId, es.getValue());
                
                if(propagateLbDataToOptoSource == ReadoutItemId.LbDataSource.coder)
                    propagateLbDataToOpto(lbId, es.getValue());
            }
            else if(es.getKey() == ReadoutItemId.LbDataSource.lmux) {
                if(storeLbLmuxData)
                    compareOrAddLmuxData(lbId, es.getValue());
                
                if(propagateLbDataToOptoSource == ReadoutItemId.LbDataSource.lmux)
                    propagateLbDataToOpto(lbId, es.getValue());
            }
        }
    }

    boolean getBit(int value, int bitNum) {
    	if((value & (1 << bitNum)) != 0 )
    		return true;
    	return false;
    }
    
    void processOptoData(BxData[] bxDatas) {
        int optoNum = TriggerBoard.getChipNum(curentChip);
        int tbNum = ((TriggerBoard)curentChip.getBoard()).getNum();
        int tcNum = ((TriggerBoard)curentChip.getBoard()).getLogSector();

        ArrayList<TreeMap<Integer, LMuxBxDataImplXml.Set>> optLinksData = new ArrayList<TreeMap<Integer, LMuxBxDataImplXml.Set>>(3);
        for (int i = 0; i < 3; i++)
            optLinksData.add(new TreeMap<Integer, LMuxBxDataImplXml.Set>());

        int bxDataNum = 0;
        for (BxData bxData : bxDatas) {
            int dataValid = bxData.getChannelsValid();
            for (LMuxBxData lmuxBxData : bxData.getLMuxBxData()) {
                int bxPos = bxData.getIndex() - lmuxBxData.getPartitionDelay(); // decoding
                if(!filterBXs ||  (bxPos >= 0 && bxPos < filteredBXsCnt)) {
                    LMuxBxDataImplXml lMuxBxDataXml = new LMuxBxDataImplXml(lmuxBxData);

                    if(getBit(dataValid, lmuxBxData.getIndex()) == false)
                        lMuxBxDataXml.setNotValidChip(curentChip);

                    lMuxBxDataXml.setPartitionDelay(0); // /after decoding the
                    // partition delay is
                    // 0!!!!!!
                    if (optLinksData.get(lmuxBxData.getIndex()).get(bxPos) == null) {
                        optLinksData.get(lmuxBxData.getIndex()).put(bxPos,
                                new LMuxBxDataImplXml.Set());
                    }
                    if (optLinksData.get(lmuxBxData.getIndex()).get(bxPos).add(lMuxBxDataXml) == false) {
                        throw new RuntimeException(
                                "the optLinkData already contains lMuxBxDataXml: "
                                + lMuxBxDataXml);
                    }
                }
            }
            bxDataNum++;
        }

        for (int optLinkNum = 0; optLinkNum < 3; optLinkNum++) {
            int globalOptLinkNum = optoNum * 3 + optLinkNum;
            compareOrAddLmuxData(ReadoutItemId.createOptLinkId(tcNum, tbNum,
                    globalOptLinkNum, curentChip), optLinksData.get(optLinkNum));
        }
    }

    boolean compareLmuxDataMaps(
            TreeMap<Integer, LMuxBxDataImplXml.Set> curMap, String curName, 
            TreeMap<Integer, LMuxBxDataImplXml.Set> inMap, String inName, boolean print) {
//      TreeMap<Integer, LMuxBxDataImplXml.Set> curMap,
//      TreeMap<Integer, LMuxBxDataImplXml.Set> inMap) {
        int bxMax = 0;
        if(curMap.size() > 0)
            bxMax = curMap.lastKey();
        if (inMap.size() > 0 && bxMax < inMap.lastKey())
            bxMax = inMap.lastKey();

        boolean good = true;
        for (int iBx = 0; iBx <= bxMax; iBx++) {
            LMuxBxDataImplXml.Set curLmuxData = curMap.get(iBx);
            LMuxBxDataImplXml.Set inLmuxData = inMap.get(iBx);
            if (curLmuxData != null && inLmuxData != null) {
                if (curLmuxData.equals(inLmuxData) == false) {
                    if(print) {
                        DefOut.out.println("bx num " + iBx
                                + " -------------------------------");
                        DefOut.out.println("is in " + curName);
                        DefOut.out.println(curLmuxData);
                        DefOut.out.println("is in " + inName);
                        DefOut.out.println(inLmuxData);
                    }
                    good = false;
                }
            } else if (curLmuxData != inLmuxData) {
                if(print) {
                    DefOut.out.println("bx num " + iBx
                            + " -------------------------------");
                    DefOut.out.println("is in " + curName);
                    DefOut.out.println(curLmuxData);
                    DefOut.out.println("is in " + inName);
                    DefOut.out.println(inLmuxData);
                    DefOut.out.println();
                }
                good = false;
            }
        }

        return good;
    }

    /* checks if each frame in the inMap fits to a frame in the curMap,
     * but in the curMap might be frames not correspondig to any in the inMap */
    boolean compareLmuxDataMapsInclusive(
            TreeMap<Integer, LMuxBxDataImplXml.Set> curMap, String curName, 
            TreeMap<Integer, LMuxBxDataImplXml.Set> inMap, String inName, boolean print) {
        int bxMax = 0;
        if(curMap.size() > 0)
            bxMax = curMap.lastKey();
        if (inMap.size() > 0 && bxMax < inMap.lastKey())
            bxMax = inMap.lastKey();

        boolean good = true;
        for (int iBx = 0; iBx <= bxMax; iBx++) {            
            LMuxBxDataImplXml.Set inLmuxBxData = inMap.get(iBx);
            if (inLmuxBxData != null) {
            	LMuxBxDataImplXml.Set curLmuxBxData = curMap.get(iBx);
            	if(curLmuxBxData == null && inLmuxBxData.size() != 0) {
            		if(print) {
        				DefOut.out.println("bx num " + iBx
        						+ " -------------------------------");
        				DefOut.out.println("is in " + inName + " but not in the " +  curName);
        				DefOut.out.println(inLmuxBxData);
        			}
        			good = false;
        			continue;
            	}
            	for(LMuxBxDataImplXml inLmuxData : inLmuxBxData) {
            		if (curLmuxBxData.contains(inLmuxData) == false) {
            			if(print) {
            				DefOut.out.println("bx num " + iBx
            						+ " -------------------------------");
            				DefOut.out.println("is in " + inName + " but not in the "  +  curName);
            				DefOut.out.println(inLmuxData);
            			}
            			good = false;
            		}
            	}
            } 
        }

        return good;
    }
    
    void compareOrAddLmuxData(ReadoutItemId id,
            TreeMap<Integer, LMuxBxDataImplXml.Set> inLmuxDataMap) {
        if (sourceDataStream != null) {
            TreeMap<Integer, LMuxBxDataImplXml.Set> currentLmuxDataMap = sourceEvent.getLmuxBxDataMap(id);
            if (currentLmuxDataMap.equals(inLmuxDataMap) == false) {
                DefOut.out.println("event " + outEvent.getNumber() + " lmuxData from "
                        + curentChip.getBoard().getName() + " "
                        + curentChip.getType() + " " + curentChip.getPosition() +" in " + id
                        + " not consistent with source lmuxData--------------");
                compareLmuxDataMaps(currentLmuxDataMap, "sourceDataStream ",  inLmuxDataMap, id.toString(), true);
            }
        }

        if (outEvent.itemWasAdded(id)) {
            TreeMap<Integer, LMuxBxDataImplXml.Set> currentLmuxDataMap = outEvent
                    .getLmuxBxDataMap(id);
            if (currentLmuxDataMap.equals(inLmuxDataMap) == false) {
                DefOut.out.println("lmuxData from "
                                + curentChip.getBoard().getName()
                                + " "
                                + curentChip.getType() + " " + curentChip.getPosition()
                                + " in "
                                + id
                                + " not consistent with prevoiusly added lmuxData--------------");
                compareLmuxDataMaps(currentLmuxDataMap, "previously added",  inLmuxDataMap, id.toString(), true);
            }
        } 
        else {
        	outEvent.addActiveReadoutItem(id);
            outEvent.putLMuxData(id, inLmuxDataMap);
        }
    }

    void processRmbData(BxData[] bxDatas) {
        int tbNum = ((TriggerBoard)curentChip.getBoard()).getNum();
        int tcNum = ((TriggerBoard)curentChip.getBoard()).getLogSector();

        ArrayList<TreeMap<Integer, LMuxBxDataImplXml.Set>> optLinksData = new ArrayList<TreeMap<Integer, LMuxBxDataImplXml.Set>>(
                18);
        for (int i = 0; i < 18; i++)
            optLinksData
                    .add(new TreeMap<Integer, LMuxBxDataImplXml.Set>());

        int bxDataNum = 0;        
        for (BxData bxData : bxDatas) {
            int dataValid = bxData.getChannelsValid();
            for (LMuxBxData lmuxBxData : bxData.getLMuxBxData()) {
                // decoding
                int bxPos = bxData.getIndex() - lmuxBxData.getPartitionDelay();
                if(!filterBXs ||  (bxPos >= 0 && bxPos < filteredBXsCnt)) {
                    LMuxBxDataImplXml lMuxBxDataXml = new LMuxBxDataImplXml(lmuxBxData);

                    if(getBit(dataValid, lmuxBxData.getIndex()) == false)
                        lMuxBxDataXml.setNotValidChip(curentChip);

                    lMuxBxDataXml.setPartitionDelay(0); // /after decoding the
                    // partition delay s 0!!!!!!
                    if (optLinksData.get(lmuxBxData.getIndex()).get(bxPos) == null) {
                        optLinksData.get(lmuxBxData.getIndex()).put(bxPos,
                                new LMuxBxDataImplXml.Set());
                    }
                    if (optLinksData.get(lmuxBxData.getIndex()).get(bxPos).add(
                            lMuxBxDataXml) == false) {
                        throw new RuntimeException(
                                curentChip.getBoard().getName() + "  RMB: " + "BXN "+ bxData.getIndex() + " optLink " + lmuxBxData.getIndex() + 
                                 " the optLinkData already contains lMuxBxDataXml: "
                                + lMuxBxDataXml);
                    }
                }
            }
            bxDataNum++;
        }

        for (int optLinkNum = 0; optLinkNum < 18; optLinkNum++) {
            compareOrAddLmuxData(ReadoutItemId.createOptLinkId(tcNum, tbNum,
                    optLinkNum, curentChip), optLinksData.get(optLinkNum));
        }
    }

    void processPacData(BxData[] bxDatas) {
        int pacNum = TriggerBoard.getChipNum(curentChip); 
        int tbNum = ((TriggerBoard)curentChip.getBoard()).getNum();
        int tcNum = ((TriggerBoard)curentChip.getBoard()).getLogSector();

        ArrayList<TreeMap<Integer, LMuxBxDataImplXml.Set>> optLinksData = new ArrayList<TreeMap<Integer, LMuxBxDataImplXml.Set>>(18);
        for (int i = 0; i < 18; i++)
            optLinksData.add(new TreeMap<Integer, LMuxBxDataImplXml.Set>());

        TreeMap<Integer, MuonsSet> pacMuonsMap = new TreeMap<Integer, MuonsSet>();

        int bxDataNum = 0;
        for (BxData bxData : bxDatas) {
            int dataValid = bxData.getChannelsValid();
            if(testOptions.getEventBuilderOptions().getPacDataSel().ifStore("in")) {
                for (LMuxBxData lmuxBxData : bxData.getLMuxBxData()) {
                    // decoding
                    int bxPos = bxData.getIndex() - lmuxBxData.getPartitionDelay();
                    if(!filterBXs ||  (bxPos >= 0 && bxPos < filteredBXsCnt)) {
                        LMuxBxDataImplXml lMuxBxDataXml = new LMuxBxDataImplXml(lmuxBxData);

                        if(getBit(dataValid, lmuxBxData.getIndex()) == false)
                            lMuxBxDataXml.setNotValidChip(curentChip);

                        lMuxBxDataXml.setPartitionDelay(0); // /after decoding the
                        // partition delay is 0!!!!!!
                        if (optLinksData.get(lmuxBxData.getIndex()).get(bxPos) == null) {
                            optLinksData.get(lmuxBxData.getIndex()).put(bxPos,
                                    new LMuxBxDataImplXml.Set());
                        }
                        if (optLinksData.get(lmuxBxData.getIndex()).get(bxPos).add(lMuxBxDataXml) == false) {
                            System.out.println(curentChip.getBoard().getName() + " the optLinkData already contains lMuxBxDataXml: " + lMuxBxDataXml + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1");
                            throw new RuntimeException("the optLinkData already contains lMuxBxDataXml: " + lMuxBxDataXml);
                        }
                    }
                }
            }

            if(testOptions.getEventBuilderOptions().getPacDataSel().ifStore("out")) {
                int bxPos = bxData.getIndex() - pacOutMuonsOffset;
                if(!filterBXs ||  (bxPos >= 0 && bxPos < filteredBXsCnt)) {
                    if (bxData.getMuons().length != 0)
                        pacMuonsMap.put(bxPos, new PacDataXml(pacNum));
                    for (Muon muon : bxData.getMuons()) {
                        MuonImplXml muonXml = new MuonImplXml(muon, muon.getIndex());
                        if (pacMuonsMap.get(bxPos).getMuons().add(muonXml) == false) {
                            throw new RuntimeException(
                                    "the pacData already contains muonXml: " + muonXml);
                        }
                    }
                }
            }

            bxDataNum++;
        }

        for (int optLinkNum = 0; optLinkNum < 18; optLinkNum++) {
            compareOrAddLmuxData(ReadoutItemId.createOptLinkId(tcNum, tbNum,
                    optLinkNum, curentChip), optLinksData.get(optLinkNum));
        }

        compareOrAddMuons(ReadoutItemId.createPacId(tcNum, tbNum, pacNum),
                pacMuonsMap);
    }

    void putMuon(TreeMap<Integer, MuonsSet> bxMuonsMap, int bxPos,
            MuonImplXml muonXml) {
        MuonsSet muons = bxMuonsMap.get(bxPos);
        if (muons == null) {
            muons = new MuonsSet();
            bxMuonsMap.put(bxPos, muons);
        }
        if (muons.getMuons().add(muonXml) == false) {
            throw new RuntimeException("muonSet aready contains muonXml: "
                    + muonXml);
        }
    }

    void processTbSortData(BxData[] bxDatas) {
        int tbNum = ((TriggerBoard)curentChip.getBoard()).getNum();
        int tcNum = ((TriggerBoard)curentChip.getBoard()).getLogSector();

        TreeMap<Integer, MuonsSet> gbSortMuonsMap = new TreeMap<Integer, MuonsSet>();
        ArrayList<TreeMap<Integer, MuonsSet>> pacsMuons = new ArrayList<TreeMap<Integer, MuonsSet>>(
                4);
        for (int i = 0; i < 4; i++)
            pacsMuons.add(new TreeMap<Integer, MuonsSet>());

        for (BxData bxData : bxDatas) {
            int dataValid = bxData.getChannelsValid();
            for (Muon muon : bxData.getMuons()) {
                // decoding
                if (muon.getIndex() < 4) { // gbSort out muons
                    if(testOptions.getEventBuilderOptions().getTbsDataSel().ifStore("out")) {
                        int bxPos = bxData.getIndex() - tbSortOutMuonsOffset;
                        MuonImplXml muonXml = new MuonImplXml(muon, muon.getIndex());

                        if(!filterBXs ||  (bxPos >= 0 && bxPos < filteredBXsCnt))
                            putMuon(gbSortMuonsMap, bxPos, muonXml);
                    }
                } 
                else {
                    if(testOptions.getEventBuilderOptions().getTbsDataSel().ifStore("in")) {
                        int bxPos = bxData.getIndex() - tbSortInMuonsOffset;
                        int pacNum = (muon.getIndex() - 4) / 12;
                        MuonImplXml muonXml = new MuonImplXml(muon, (muon
                                .getIndex() - 4) % 12);

                        if(getBit(dataValid, pacNum) == false)
                            muonXml.setNotValidChip(curentChip);

                        if(!filterBXs ||  (bxPos >= 0 && bxPos < filteredBXsCnt))
                            putMuon(pacsMuons.get(pacNum), bxPos, muonXml);
                    }
                }
            }
        }
        
        for (int iPac = 0; iPac < 4; iPac++) {
            compareOrAddMuons(ReadoutItemId.createPacId(tcNum, tbNum, iPac),
                    pacsMuons.get(iPac));
        }
        
        compareOrAddMuons(ReadoutItemId.createTbSortId(tcNum, tbNum), gbSortMuonsMap);
    }

    void processTcSortData(BxData[] bxDatas) {
        int tcNum = ((TriggerCrate) curentChip.getBoard().getCrate())
                .getLogSector();

        TreeMap<Integer, MuonsSet> tcSortMuonsMap = new TreeMap<Integer, MuonsSet>();
        ArrayList<TreeMap<Integer, MuonsSet>> tbSortMuons = new ArrayList<TreeMap<Integer, MuonsSet>>(
                9);
        for (int iTb = 0; iTb < 9; iTb++)
            tbSortMuons.add(new TreeMap<Integer, MuonsSet>());

        int bxDataNum = 0;
        for (BxData bxData : bxDatas) {
            int dataValid = bxData.getChannelsValid();
            for (Muon muon : bxData.getMuons()) {
                if (muon.getIndex() < 4) { // gbSort out muons
                    int bxPos = bxData.getIndex() - tcSortOutMuonsOffset;
                    MuonImplXml muonXml = new MuonImplXml(muon, muon.getIndex());
                    
                    if(!filterBXs ||  (bxPos >= 0 && bxPos < filteredBXsCnt))
                        putMuon(tcSortMuonsMap, bxPos, muonXml);
                } else {
                    int bxPos = bxData.getIndex() - tcSortInMuonsOffset;
                    int tbNum = (muon.getIndex() - 4) / 4;
                    MuonImplXml muonXml = new MuonImplXml(muon, (muon
                            .getIndex() - 4) % 4);
                    
                    if(getBit(dataValid, tbNum) == false)
                        muonXml.setNotValidChip(curentChip);
                    
                    if(!filterBXs ||  (bxPos >= 0 && bxPos < filteredBXsCnt))
                        putMuon(tbSortMuons.get(tbNum), bxPos, muonXml);
                }
            }
            bxDataNum++;
        }      

        for (int iTb = 0; iTb < 9; iTb++) {
            compareOrAddMuons(ReadoutItemId.createTbSortId(tcNum, iTb),
                    tbSortMuons.get(iTb));
        }
        
        compareOrAddMuons(ReadoutItemId.createTcSortId(tcNum), tcSortMuonsMap);
    }

    int hsbPosToNum(int pos) {
        if (pos == 1)
            return 0;
        if (pos == 3)
            return 1;
        return 0;
    }

    int hsbNameToNum(String name) {
    	if(name.contains("0"))
    		return 0;
    	if(name.contains("1"))
    		return 1;
    	throw new RuntimeException("hsbNameToNum: hsb name: " + name + " does not contain 0 or 1");
    }
    
    void processHalfSortData(BxData[] bxDatas) {
        //int hsNum = hsbPosToNum(curentChip.getBoard().getPosition());
        int hsNum = hsbNameToNum(curentChip.getBoard().getName());
        
        ArrayList<TreeMap<Integer, MuonsSet>> hsOutMuonsMap = new ArrayList<TreeMap<Integer, MuonsSet>>(2);
        for (int be = 0; be < 2; be++)
            hsOutMuonsMap.add(new TreeMap<Integer, MuonsSet>());

        ArrayList<TreeMap<Integer, MuonsSet>> tcSortMuons = new ArrayList<TreeMap<Integer, MuonsSet>>(8);
        for (int iTc = 0; iTc < 8; iTc++)
            tcSortMuons.add(new TreeMap<Integer, MuonsSet>());

        int bxDataNum = 0;
        for (BxData bxData : bxDatas) {
            int dataValid = bxData.getChannelsValid();
            for (Muon muon : bxData.getMuons()) {
                if (muon.getIndex() < 8) { // gbSort out muons
                    if(testOptions.getEventBuilderOptions().getHsDataSel().ifStore("out")) {
                        int bxPos = bxData.getIndex() - hsOutMuonsOffset;
                        MuonImplXml muonXml = new MuonImplXml(muon, muon.getIndex()%4);

                        if(!filterBXs ||  (bxPos >= 0 && bxPos < filteredBXsCnt))
                            putMuon(hsOutMuonsMap.get(muon.getIndex() / 4), bxPos, muonXml);
                    }
                } 
                else {
                    if(testOptions.getEventBuilderOptions().getHsDataSel().ifStore("in")) {
                        int bxPos = bxData.getIndex() - hsInMuonsOffset;
                        int tcNum = (muon.getIndex() - 8) / 4; //tc input num on the hsb, not the log_sector num
                        MuonImplXml muonXml = new MuonImplXml(muon, (muon.getIndex() - 8) % 4);

                        if(getBit(dataValid, (muon.getIndex() - 8) / 2) == false) //one dataValid bit per one cable, i.e. two muons
                            muonXml.setNotValidChip(curentChip);

                        if(!filterBXs ||  (bxPos >= 0 && bxPos < filteredBXsCnt))
                            putMuon(tcSortMuons.get(tcNum), bxPos, muonXml);
                    }
                }
            }
            bxDataNum++;
        }

        for (int iTc = 0; iTc < 8; iTc++) {
            int tcNum = 0;
            if (hsNum == 0) {
                if (iTc == 0)
                    tcNum = 11;
                else
                    tcNum = iTc - 1;
            } else {
                if (iTc == 7)
                    tcNum = 0;
                else
                    tcNum = iTc + 5;
            }
            compareOrAddMuons(ReadoutItemId.createTcSortId(tcNum), tcSortMuons.get(iTc));
        }
        
        for (int be = 0; be < 2; be++) {
            compareOrAddMuons(ReadoutItemId.createHalfSortId(hsNum, be), hsOutMuonsMap.get(be));
        }
    }

    void processFinalSortData(BxData[] bxDatas) {
        ArrayList<TreeMap<Integer, MuonsSet>> fsOutMuons = new ArrayList<TreeMap<Integer, MuonsSet>>(2);
        for (int iHs = 0; iHs < 3; iHs++)
            fsOutMuons.add(new TreeMap<Integer, MuonsSet>());

        ArrayList<TreeMap<Integer, MuonsSet>> hsOutMuons = new ArrayList<TreeMap<Integer, MuonsSet>>(4);
        for (int be = 0; be < 4; be++)
            hsOutMuons.add(new TreeMap<Integer, MuonsSet>());

        int bxDataNum = 0;
        for (BxData bxData : bxDatas) {
            for (Muon muon : bxData.getMuons()) {
                if (muon.getIndex() < 8) { // gbSort out muons
                    int bxPos = bxData.getIndex() - fsOutMuonsOffset;
                    MuonImplXml muonXml = new MuonImplXml(muon, muon.getIndex()%4);
                    
                    if(!filterBXs ||  (bxPos >= 0 && bxPos < filteredBXsCnt))
                        putMuon(fsOutMuons.get(muon.getIndex() / 4), bxPos, muonXml);
                } else {
                    int bxPos = bxData.getIndex() - fsInMuonsOffset;
                    int iHs = (muon.getIndex() - 8) / 4;
                    MuonImplXml muonXml = new MuonImplXml(muon, (muon.getIndex() - 8) % 4);
                    
                    if(!filterBXs ||  (bxPos >= 0 && bxPos < filteredBXsCnt))
                        putMuon(hsOutMuons.get(iHs), bxPos, muonXml);
                }
            }
            bxDataNum++;
        }

        for (int i = 0; i < 4; i++) {
            compareOrAddMuons(ReadoutItemId.createHalfSortId(i % 2, i / 2), hsOutMuons.get(i));
        }
        
        for (int be = 0; be < 2; be++) {
            compareOrAddMuons(ReadoutItemId.createFinalSortId(be), fsOutMuons
                    .get(be));
        }
    }

    void comapreBxMuonsMaps(TreeMap<Integer, MuonsSet> curMuMap,
            TreeMap<Integer, MuonsSet> inMuMap) {
    	int bxMax = 0;
    	if(curMuMap.size() > 0)
    		bxMax = curMuMap.lastKey();
        if (inMuMap.size() > 0 && bxMax < inMuMap.lastKey())
            bxMax = inMuMap.lastKey();
        
        for (int iBx = 0; iBx <= bxMax; iBx++) {
            MuonsSet curMuons = curMuMap.get(iBx);
            MuonsSet inMuons = inMuMap.get(iBx);
            if (curMuons != null && inMuons != null) {
                if (curMuons.equals(inMuons) == false) {
                    DefOut.out.println("bx num " + iBx
                            + " -------------------------------");
                    DefOut.out.println("should be:");
                    DefOut.out.println(curMuons);
                    DefOut.out.println("is in readout:");
                    DefOut.out.println(inMuons);
                }
            } else if (curMuons != inMuons) {
                DefOut.out.println("bx num " + iBx
                        + " -------------------------------");
                DefOut.out.println("should be:");
                DefOut.out.println(curMuons);
                DefOut.out.println("is in readout:");
                DefOut.out.println(inMuons);
                DefOut.out.println();
            }
        }
    }

    void compareOrAddMuons(ReadoutItemId id, TreeMap<Integer, MuonsSet> inMuMap) {
        if (sourceDataStream != null) {
            TreeMap<Integer, MuonsSet> sourceMuMap = sourceEvent.getBxMuonsMap(id);
            if (sourceMuMap.equals(inMuMap) == false) {
                DefOut.out.println("muons from "
                        + curentChip.getBoard().getName() + " "
                        + curentChip.getType() + " in " + id
                        + " not consistent with source muons--------------");
                comapreBxMuonsMaps(sourceMuMap, inMuMap);
            }
        }

        if (outEvent.itemWasAdded(id)) {
            TreeMap<Integer, MuonsSet> outMuMap = outEvent.getBxMuonsMap(id); //[bx], muons
            if (outMuMap.equals(inMuMap) == false) {
                DefOut.out.println("event " + outEvent.getNumber() + " muons from device "
                        + curentChip.getBoard().getName() + " "
                        + curentChip.getType() + " in channel " + id
                        + " not consistent with prevoius muons --------------");
                comapreBxMuonsMaps(outMuMap, inMuMap);
            }
        } else {
        	outEvent.addActiveReadoutItem(id);
            outEvent.putMuons(id, inMuMap);
        }
    }

    public void build(DiagnosticReadoutData[] datas) throws DataAccessException {
        outDataStream.getEvents().clear(); //<<<<<<!!!!!!!!!!!!!!
        if(datas.length == 0) {
            DefOut.out.println("datas.length == 0"); 
            return;
        }
    	if(datas[0].getEvents().length == 0) {    	 
    		DefOut.out.println("no events in the datas[0] !!!!!!!!!!!!!!!!!!!");    		    	
    		return;
    	}
    	
    	int firstEventNum = datas[0].getEvents()[0].getEvNum();
    	int firstEventsCnt = datas[0].getEvents().length;
    	ReadoutMode mode = ReadoutMode.DAQLIKE;
    	if(firstEventsCnt == 1) {
    		mode = ReadoutMode.SNAPSHOT;
    	}
        for (DiagnosticReadoutData data : datas) {
            int chipId = data.getDiagId().getOwnerId();
            curentChip = (Chip) (equipmentDAO.getObject(Chip.class, chipId));      
            if(data.getEvents().length == 0) {
                DefOut.out.println("device " + curentChip.getBoard().getName() + " " + curentChip.getType() + " " + curentChip.getPosition() 
                        + " Id " + curentChip.getId()  +  "data.getEvents().length == 0  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");   
                continue;
            }
            if(firstEventNum != data.getEvents()[0].getEvNum()) {
            	DefOut.out.println("device " + curentChip.getBoard().getName() + " " + curentChip.getType() + " " + curentChip.getPosition() 
        				+ " Id " + curentChip.getId()  +  " the evNum " + data.getEvents()[0].getEvNum() 
        				+ " of the first event in the readout is not equall to the first evNum " + firstEventNum + " of the first readout - evnts number are not synchronized !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            }
            
            if(firstEventsCnt != data.getEvents().length) {
            	DefOut.out.println("device " + curentChip.getBoard().getName() + " " + curentChip.getType() + " " + curentChip.getPosition() 
        				+ " Id " + curentChip.getId()  +  " the EventsCnt " + data.getEvents().length 
        				+ " is not equal to the  first firstEventsCnt " + firstEventsCnt + " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

            }
            
            int eventInReadNum = 0;
            for (DiagnosticReadoutEvent event : data.getEvents()) {
                processEvent(event, mode);
                eventInReadNum++;
                if(eventInReadNum == eventsPerReadout) {                    
                    //DefOut.out.println("skipping last evetn in readout event");
                    break;
                }
            }
        }
    }

    public void writeToXmlFile(String xmlFileName) throws JAXBException,
            FileNotFoundException {
        System.out.println("writing to the file" + xmlFileName);
        JAXBContext context = JAXBContext.newInstance(DataStreamXml.class);
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        m.marshal(outDataStream, new FileOutputStream(xmlFileName));
    }
    
    public void appendToXmlFile(FileOutputStream file, Marshaller m) throws JAXBException, IOException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        m.marshal(outDataStream, stream);
        byte[] bytes = stream.toByteArray();
/*        String dataStr = stream.toString();
        dataStr.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>", "");
        dataStr.replace("<rpctDataStream>", "");
        dataStr.replace("</rpctDataStream>", "");*/
        //stream.writeTo(file);        
        if((bytes.length - 73 - 18) > 0) {
            file.write(bytes, 73, bytes.length - 73 - 18);            
            file.write("\n".getBytes());
        }
    }
    
    public void analyseOptLinkConnections() throws DataAccessException {
        DefOut.out.println("****************************************************************************************");
        DefOut.out.println("analyseOptLinkConnections\n");
        Set<ReadoutItemId> readoutItemIds = outEvent.getAddedReadoutItemIds();        
        for (ReadoutItemId lbId : readoutItemIds) {
            if(lbId.getType() == ReadoutItemId.Type.lb && 
                    lbId.getLbDataSource() == ReadoutItemId.LbDataSource.lmux) {               
                LinkBoard lb = (LinkBoard) lbId.getChip().getBoard(); 
                if(lb.isMaster() == false) {
                    throw new RuntimeException("The LinkBoard with id " + lb.getId() + "is not MASTER");
                }
                DefOut.out.println(lbId + "---------------------------------------------------");
                for(LinkConn linkConn : lb.getLinkConns() ) {
                    ReadoutItemId optLinkId = ReadoutItemId.createOptLinkId(linkConn);
                    for(EventXml event : outDataStream.getEvents()) {                    
                        TreeMap<Integer, LMuxBxDataImplXml.Set> synCodDataMap = event.getLmuxBxDataMap(lbId);
                        if(outEvent.getAddedReadoutItemIds().contains(optLinkId)) {       
                            DefOut.out.println("compared to " + optLinkId);
                            TreeMap<Integer, LMuxBxDataImplXml.Set> optLinkDataMap = event.getLmuxBxDataMap(optLinkId);
                            compareLmuxDataMaps(synCodDataMap, lb.getName(), optLinkDataMap, optLinkId.toString(), true);
                        }
                        else {
                            DefOut.out.println("no data for the " + optLinkId);
                        }
                    }
                }
            }
        }
    }
    
    public void analyseOptLinkConnections(ArrayList<LinkConn> usedLinkConn) throws DataAccessException {
        DefOut.out.println("****************************************************************************************");
        DefOut.out.println("                              analyseOptLinkConnections");
        DefOut.out.println("****************************************************************************************");
        Set<ReadoutItemId> readoutItemIds = outEvent.getAddedReadoutItemIds();        
        int connCnt = 0;
        int faildCnt = 0;
        for(LinkConn linkCon : usedLinkConn) {
        	connCnt++;
            LinkBoard lb = (LinkBoard) linkCon.getBoard();
            ReadoutItemId lbId = ReadoutItemId.createLbId(lb.getSynCoder(), LbDataSource.lmux);
            ReadoutItemId optLinkId = ReadoutItemId.createOptLinkId(linkCon);
            DefOut.out.println("testing " + linkCon + " ");
            //for(EventXml event : outDataStream.getEvents()) 
            {   EventXml event = outEvent;                                 
                if(readoutItemIds.contains(lbId)) {       
                    //DefOut.out.println("compared to " + optLinkId);
                    TreeMap<Integer, LMuxBxDataImplXml.Set> synCodDataMap = event.getLmuxBxDataMap(lbId);
                    TreeMap<Integer, LMuxBxDataImplXml.Set> optLinkDataMap = event.getLmuxBxDataMap(optLinkId);
                    if(compareLmuxDataMaps(synCodDataMap, lb.getName(), optLinkDataMap, optLinkId.toString(), false)) {
                        DefOut.out.println("  -  OK");
                    }
                    else {
                    	faildCnt++;
                        DefOut.out.println("not good, trying to mach the lmux data to othere optLinkIn");
                        boolean nothing = true;
                        for(ReadoutItemId id : readoutItemIds) {
                            if(id.getType() == ReadoutItemId.Type.optLink) {
                                TreeMap<Integer, LMuxBxDataImplXml.Set> optLinkDataMap1 = event.getLmuxBxDataMap(id);
                                if(compareLmuxDataMaps(synCodDataMap, lb.getName(), optLinkDataMap1, optLinkId.toString(), false)) {
                                    DefOut.out.println(" the data fits to " + id);
                                    nothing = false;
                                }
                            }
                        }
                        if(nothing)
                        	DefOut.out.println("nothing found");
                        DefOut.out.println("\n");
                    }                        
                }
                else {
                    DefOut.out.println(" no data for the " + lbId);
                }
            }
        }
        
        if(connCnt == 0)
        	DefOut.out.println("opt links tesed: " + connCnt);
        else
        	DefOut.out.println("opt links tesed: " + connCnt + " faild " + faildCnt + ", failed ratio: " + faildCnt/connCnt);
    }
    
/*    public void analyseSlaveMasterConnections() throws DataAccessException {
        DefOut.out.println("analyseOptLinkConnections");
        Set<ReadoutItemId> readoutItemIds = outEvent.getAddedReadoutItemIds();        
        for (ReadoutItemId masterId : readoutItemIds) {
            if(masterId.getType() == ReadoutItemId.Type.lb) {
                if(masterId.getLbDataSource() == ReadoutItemId.LbDataSource.slave0 ||
                        masterId.getLbDataSource() == ReadoutItemId.LbDataSource.slave1) {               
                    LinkBoard masterLb = (LinkBoard) masterId.getChip().getBoard(); 
                    if(masterLb.isMaster()!= false) {
                        throw new RuntimeException("The LinkBoard with id " + masterLb.getId() + "is not MASTER");
                    }

                    LinkBoard slaveLB;
                    if(masterId.getLbDataSource() == ReadoutItemId.LbDataSource.slave0)
                        slaveLB = masterLb.getLeftSlave();
                    else
                        slaveLB = masterLb.getRightSlave();                    
                    if(slaveLB != null) {
                        ReadoutItemId slaveId = ReadoutItemId.createLbId(slaveLB.getSynCoder(), ReadoutItemId.LbDataSource.coder);
                        DefOut.out.println("masterId " + masterId + " - slaveId " + slaveId + "------------------------------");
                        for(EventXml event : outDataStream.getEvents()) {                    
                            TreeMap<Integer, LMuxBxDataImplXml.Set> masterDataMap = event.getLmuxBxDataMap(masterId);                        
                            if(outEvent.getAddedReadoutItemIds().contains(slaveId)) {
                                TreeMap<Integer, LMuxBxDataImplXml.Set> slaveDataMap = event.getLmuxBxDataMap(slaveId);
                                compareLmuxDataMaps(masterDataMap, slaveDataMap, true);
                            }
                            else {
                                DefOut.out.println("no data for the " + slaveId);
                            }
                        }
                    }
                    else {
                        DefOut.out.println("no " + masterId.getLbDataSource() + "for that master");
                    }
                    
                }
            }
        }
    }*/
    
    
    /**
     * @param masterLBs
     * @throws DataAccessException
     * the data should be sent only to one slave LB at the moment!!!
     */
    public void analyseSlaveMasterConnections(Set<LinkBoard> masterLBs, int slaveChannel) throws DataAccessException {        
        for(LinkBoard mlb : masterLBs) {
            ReadoutItemId masterId = ReadoutItemId.createLbId(mlb.getSynCoder(), LbDataSource.lmux);
            for(EventXml event : outDataStream.getEvents()) {                    
                TreeMap<Integer, LMuxBxDataImplXml.Set> masterDataMap = event.getLmuxBxDataMap(masterId);      
                //for(int iCh = 1; iCh < 3; iCh++) 
                {
                    LinkBoard slaveLB;
                    if(slaveChannel == 1)
                        slaveLB = mlb.getLeftSlave();
                    else if (slaveChannel == 2)
                        slaveLB = mlb.getRightSlave();  
                    else {
                    	throw new IllegalArgumentException("illegal slaveChannel: " + slaveChannel);
                    }
                    
                    if(slaveLB != null) {
                        ReadoutItemId slaveId = ReadoutItemId.createLbId(slaveLB.getSynCoder(), ReadoutItemId.LbDataSource.coder);
                        DefOut.out.println("masterId " + masterId + " - slaveId " + slaveId + "------------------------------");
                        
                        if(outEvent.getAddedReadoutItemIds().contains(slaveId)) {
                            TreeMap<Integer, LMuxBxDataImplXml.Set> slaveDataMap = event.getLmuxBxDataMap(slaveId);
                            boolean good = compareLmuxDataMaps(masterDataMap, masterId.toString(), slaveDataMap, slaveId.toString(), true);
                            if(good)
                            	DefOut.out.println("OK");	
                        }
                        else {
                            DefOut.out.println("no data for the " + slaveId);
                        }
                    }
                }
            }
        }
    }       

    public void analyseLmux() throws DataAccessException {
        DefOut.out.println("analyseLmux");
        Set<ReadoutItemId> readoutItemIds = outEvent.getAddedReadoutItemIds();        
        for (ReadoutItemId masterId : readoutItemIds) {
            if(masterId.getType() == ReadoutItemId.Type.lb) {
                if(masterId.getLbDataSource() == ReadoutItemId.LbDataSource.lmux) {               
                    LinkBoard masterLb = (LinkBoard) masterId.getChip().getBoard(); 
                    if(masterLb.isMaster() == false) {
                        throw new RuntimeException("The LinkBoard with id " + masterLb.getId() + "is not MASTER");
                    }

                    DefOut.out.println("masterId " + masterId + "------------------------------");
                    for(EventXml event : outDataStream.getEvents()) {                    
                        TreeMap<Integer, LMuxBxDataImplXml.Set> masterDataMap = event.getLmuxBxDataMap(masterId);
                        ArrayList<TreeMap<Integer, LMuxBxDataImplXml.Set>> codedDataInLbs = new ArrayList<TreeMap<Integer, LMuxBxDataImplXml.Set>>();// = demultiplex(masterDataMap);
                        for(int iSlb = 0; iSlb < 3; iSlb++) {
                        	codedDataInLbs.add(null);
                            LinkBoard slaveLB = masterLb.getSlave(iSlb);                 
                            if(slaveLB != null) {
                                ReadoutItemId slaveId;
                                if(iSlb == 0) //this master, we take strips data fpor it 
                                    slaveId = ReadoutItemId.createLbId(slaveLB.getSynCoder(), ReadoutItemId.LbDataSource.stripData);
                                else
                                    slaveId = ReadoutItemId.createLbId(slaveLB.getSynCoder(), ReadoutItemId.LbDataSource.stripData);
                                
                                DefOut.out.println("slaveId " + slaveId + "------------------------------");
                                if(outEvent.getAddedReadoutItemIds().contains(slaveId)) {
                                    TreeMap<Integer, LMuxBxDataImplXml.Set> slaveDataMap = event.getLmuxBxDataMap(slaveId);
                                    TreeMap<Integer, LMuxBxDataImplXml.Set> codedeData = LinkCoderAlgoritms.code(slaveDataMap, -2, iSlb, 1, RpctDelays.LB_CODER_LATENCY);                                    
                                    codedDataInLbs.set(iSlb, codedeData);
                                    //compareLmuxDataMaps(demuxedData.get(iSlb), masterId.toString(), slaveDataMap, slaveId.toString(), true);
                                }
                                else {
                                    DefOut.out.println("no data for the " + slaveId);
                                }
                                
                                
                            }
                            else {
                                DefOut.out.println("no slave num" + iSlb + "for that master");
                            }
                        }
                        TreeMap<Integer, LMuxBxDataImplXml.Set> multiplexdDataMap = LinkCoderAlgoritms.multiplex(codedDataInLbs, 128);
                        compareLmuxDataMaps(masterDataMap, masterId.toString(), multiplexdDataMap, "slaves multiplexd", true);
                    }
                }
            }
        }
    }

    public void analyseLmux2() throws DataAccessException {
        DefOut.out.println("analyseLmux");
        Set<ReadoutItemId> readoutItemIds = outEvent.getAddedReadoutItemIds();        
        for (ReadoutItemId masterId : readoutItemIds) {
            if(masterId.getType() == ReadoutItemId.Type.lb) {
                if(masterId.getLbDataSource() == ReadoutItemId.LbDataSource.lmux) {               
                    LinkBoard masterLb = (LinkBoard) masterId.getChip().getBoard(); 
                    if(masterLb.isMaster() == false) {
                        throw new RuntimeException("The LinkBoard with id " + masterLb.getId() + "is not MASTER");
                    }

                    DefOut.out.println("masterId " + masterId + "------------------------------");
                    for(EventXml event : outDataStream.getEvents()) {                    
                        TreeMap<Integer, LMuxBxDataImplXml.Set> masterDataMap = event.getLmuxBxDataMap(masterId);
                        ArrayList<TreeMap<Integer, LMuxBxDataImplXml.Set>> demuxedData = LinkCoderAlgoritms.demultiplex(masterDataMap);
                        for(int iSlb = 0; iSlb < 3; iSlb++) {
                            LinkBoard slaveLB = masterLb.getSlave(iSlb);                 
                            if(slaveLB != null) {
                                ReadoutItemId slaveId;
                                if(iSlb == 0) //this master, we take strips data fpor it 
                                    slaveId = ReadoutItemId.createLbId(slaveLB.getSynCoder(), ReadoutItemId.LbDataSource.stripData);
                                else
                                    slaveId = ReadoutItemId.createLbId(slaveLB.getSynCoder(), ReadoutItemId.LbDataSource.stripData);
                                
                                DefOut.out.println("slaveId " + slaveId + "------------------------------");
                                if(outEvent.getAddedReadoutItemIds().contains(slaveId)) {
                                    TreeMap<Integer, LMuxBxDataImplXml.Set> slaveDataMap = event.getLmuxBxDataMap(slaveId);   
                                    compareLmuxDataMapsInclusive(slaveDataMap, slaveId.toString(), demuxedData.get(iSlb), masterId.toString(),  true);
                                }
                                else {
                                    DefOut.out.println("no data for the " + slaveId);
                                }
                            }
                            else {
                                DefOut.out.println("no slave num" + iSlb + "for that master");
                            }
                        }
                    }
                }
            }
        }
    }

    public void setLmuxInDelay(Map<Integer, Integer> lmuxInDelay) {
        this.lmuxInDelay = lmuxInDelay;
    }

    public static int getEventsPerReadout() {
        return eventsPerReadout;
    }
    
    public void clear() {
        if(outDataStream != null)
            outDataStream.clear();
        
        if(sourceDataStream != null)
            sourceDataStream.clear();
    }
}
