package rpct.datastream;

import rpct.xdaqaccess.diag.TriggerDataSel;
import rpct.xdaqaccess.diag.DiagCtrl.TriggerType;

public class TestPresets {
    static public class TestPreset {
        private int pulseLenght;

        public void setPulseLenght(int pulseLenght) {
			this.pulseLenght = pulseLenght;
		}

		public void setPulseReapet(boolean pulseReapet) {
			this.pulseReapet = pulseReapet;
		}

		public void setPulserLimit(long pulserLimit) {
			this.pulserLimit = pulserLimit;
		}

		public void setReadoutLimit(long readoutLimit) {
			this.readoutLimit = readoutLimit;
		}

		public void setPulserStartTriggerType(TriggerType pulserStartTriggerType) {
			this.pulserStartTriggerType = pulserStartTriggerType;
		}

		public void setReadoutStartTriggerType(TriggerType readoutStartTriggerType) {
			this.readoutStartTriggerType = readoutStartTriggerType;
		}

		public void setTriggerDataSel(TriggerDataSel triggerDataSel) {
			this.triggerDataSel = triggerDataSel;
		}

		private boolean pulseReapet;

        private long pulserLimit;

        private long readoutLimit;

        private TriggerType pulserStartTriggerType = TriggerType.BCN0;
        private TriggerType readoutStartTriggerType = TriggerType.BCN0;         

        private TriggerDataSel triggerDataSel;

        public TestPreset(int pulseLenght, boolean pulseReapet,
                long pulserLimit, long readoutLimit,
                TriggerType pulserStartTriggerType,
                TriggerType readoutStartTriggerType,
                TriggerDataSel triggerDataSel) {
            super();
            this.pulseLenght = pulseLenght;
            this.pulseReapet = pulseReapet;
            this.pulserLimit = pulserLimit;
            this.readoutLimit = readoutLimit;
            this.pulserStartTriggerType = pulserStartTriggerType;
            this.readoutStartTriggerType = readoutStartTriggerType;
            this.triggerDataSel = triggerDataSel;
        }

        public int getPulseLenght() {
            return pulseLenght;
        }

        public boolean getPulseReapet() {
            return pulseReapet;
        }

        public long getPulserLimit() {
            return pulserLimit;
        }

        public long getReadoutLimit() {
            return readoutLimit;
        }

        public TriggerType getPulserStartTriggerType() {
            return pulserStartTriggerType;
        }

        public TriggerType getReadoutStartTriggerType() {
            return readoutStartTriggerType;
        }

        public TriggerDataSel getTriggerDataSel() {
            return triggerDataSel;
        }


    }

    static public class SinglePulser_SnapshotPretrigger0 extends TestPreset {
        public SinglePulser_SnapshotPretrigger0() {
            super(  128,  //pulseLenght, 
                    false, //pulseReapet, 
                    128 - 1, //pulserLimit,  128 - 1
                    128, //readoutLimit,
                    TriggerType.PRETRIGGER_0, //pulserStartTriggerType, 
                    TriggerType.PRETRIGGER_0, //readoutStartTriggerType, 
                    TriggerDataSel.NONE //triggerDataSel
            );
        }		
    }
    
    static public class RepaetPulser_SnapshotPretrigger0 extends TestPreset {
        public RepaetPulser_SnapshotPretrigger0() {
            super(  128,  //pulseLenght, 
                    true, //pulseReapet, 
                    128 - 1, //pulserLimit,  128 - 1
                    128, //readoutLimit,
                    TriggerType.PRETRIGGER_0, //pulserStartTriggerType, 
                    TriggerType.PRETRIGGER_0, //readoutStartTriggerType, 
                    TriggerDataSel.NONE //triggerDataSel
            );
        }		
    }

    static public class LongPulser_SnapshotPretrigger0 extends TestPreset {
        public LongPulser_SnapshotPretrigger0(int pulseLenght, long pulserLimit) {
            super(          pulseLenght, 
                    false, //pulseReapet, 
                    pulserLimit, 
                    128, //readoutLimit,
                    TriggerType.PRETRIGGER_0, //pulserStartTriggerType, 
                    TriggerType.PRETRIGGER_0, //readoutStartTriggerType, 
                    TriggerDataSel.NONE //triggerDataSel
            );
        }		
    }

    static public class LongPulser_SnapshotBc0 extends TestPreset {
        public LongPulser_SnapshotBc0(int pulseLenght, long pulserLimit) {
            super(          pulseLenght, 
                    false, //pulseReapet, 
                    pulserLimit, 
                    128, //readoutLimit,
                    TriggerType.BCN0, //pulserStartTriggerType, 
                    TriggerType.BCN0, //readoutStartTriggerType, 
                    TriggerDataSel.NONE //triggerDataSel
            );
        }               
    }
    
    static public class LongPulser_SnapshotNone extends TestPreset {
        public LongPulser_SnapshotNone(int pulseLenght, long pulserLimit) {
            super(          pulseLenght, 
                    false, //pulseReapet, 
                    pulserLimit, 
                    128, //readoutLimit,
                    TriggerType.MANUAL, //pulserStartTriggerType, 
                    TriggerType.MANUAL, //readoutStartTriggerType, 
                    TriggerDataSel.NONE //triggerDataSel
            );
        }               
    }

    static public class SinglePulser_SnapshotL1A extends TestPreset {
        public SinglePulser_SnapshotL1A() {
            super(  128,  //pulseLenght, 
                    false, //pulseReapet, 
                    0xffffffff, //pulserLimit, 
                    128, //readoutLimit,
                    TriggerType.PRETRIGGER_0, //pulserStartTriggerType, 
                    TriggerType.L1A, //readoutStartTriggerType, 
                    TriggerDataSel.NONE //triggerDataSel
            );
        }		
    }

    static public class SinglePulser_DaqLike extends TestPreset {
        public SinglePulser_DaqLike() {
            super(  128,  //pulseLenght, 
                    false, //pulseReapet, 
                    128 - 1, //pulserLimit, 
                    128, //readoutLimit,
                    TriggerType.PRETRIGGER_0, //pulserStartTriggerType, 
                    TriggerType.PRETRIGGER_0, //readoutStartTriggerType, 
                    TriggerDataSel.L1A //triggerDataSel
            );
        }		
    }


    /**
     * @author KAROL
     * Use e.g for the DAQ test
     */
    static public class ReapetPulserBC0_SnapshotL1A extends TestPreset {
        public ReapetPulserBC0_SnapshotL1A() {
            super(  128,  //pulseLenght, 
                    true, //pulseReapet, 
                    128 - 1, //pulserLimit, 
                    10 * RpctDelays.BX_IN_ORBIT, //readoutLimit,
                    TriggerType.BCN0, //pulserStartTriggerType, 
                    TriggerType.L1A, //readoutStartTriggerType, 
                    TriggerDataSel.NONE //triggerDataSel
            );
        }		

        public ReapetPulserBC0_SnapshotL1A(int pulserLimit) {
            super(  128,  //pulseLenght, 
                    true, //pulseReapet, 
                    pulserLimit, 
                    10 * RpctDelays.BX_IN_ORBIT, //readoutLimit,
                    TriggerType.BCN0, //pulserStartTriggerType, 
                    TriggerType.L1A, //readoutStartTriggerType, 
                    TriggerDataSel.NONE //triggerDataSel
            );
        }
    }

    static public class ReapetPulserBC0_SnapshotBC0 extends TestPreset {
        public ReapetPulserBC0_SnapshotBC0() {
            super(  128,  //pulseLenght, 
                    true, //pulseReapet, 
                    128 - 1, //pulserLimit, 
                    10 * RpctDelays.BX_IN_ORBIT, //readoutLimit,
                    TriggerType.BCN0, //pulserStartTriggerType, 
                    TriggerType.BCN0, //readoutStartTriggerType, 
                    TriggerDataSel.NONE //triggerDataSel
            );
        }		
               
        /**
         * @param pulserLimit must be less than BX count in the orbit
         */
        public ReapetPulserBC0_SnapshotBC0(int pulserLimit) {
            super(  128,  //pulseLenght, 
                    true, //pulseReapet, 
                    pulserLimit, //must be less than < 3500, 
                    10 * RpctDelays.BX_IN_ORBIT, //readoutLimit,
                    TriggerType.BCN0, //pulserStartTriggerType, 
                    TriggerType.BCN0, //readoutStartTriggerType, 
                    TriggerDataSel.NONE //triggerDataSel
            );
        }   
    }

    static public class ReapetPulserPretrigger0_SnapshotL1A extends TestPreset {
        public ReapetPulserPretrigger0_SnapshotL1A() {
            super(  128,  //pulseLenght, 
                    true, //pulseReapet, 
                    128 - 1, //pulserLimit, 
                    10 * RpctDelays.BX_IN_ORBIT, //readoutLimit,
                    TriggerType.PRETRIGGER_0, //pulserStartTriggerType, 
                    TriggerType.L1A, //readoutStartTriggerType, 
                    TriggerDataSel.NONE //triggerDataSel
            );
        }
    }
    
    static public class ReapetPulserPretrigger0_SnapshotPretrigger0 extends TestPreset {
        public ReapetPulserPretrigger0_SnapshotPretrigger0() {
            super(  128,  //pulseLenght, 
                    true, //pulseReapet, 
                    128 - 1, //pulserLimit, 
                    10 * RpctDelays.BX_IN_ORBIT, //readoutLimit,
                    TriggerType.PRETRIGGER_0, //pulserStartTriggerType, 
                    TriggerType.PRETRIGGER_0, //readoutStartTriggerType, 
                    TriggerDataSel.NONE //triggerDataSel
            );
        }
        
        public ReapetPulserPretrigger0_SnapshotPretrigger0(int bxCnt) {
            super(  bxCnt,  //pulseLenght, 
                    true, //pulseReapet, 
                    bxCnt - 1, //pulserLimit, 
                    10 * RpctDelays.BX_IN_ORBIT, //readoutLimit,
                    TriggerType.PRETRIGGER_0, //pulserStartTriggerType, 
                    TriggerType.PRETRIGGER_0, //readoutStartTriggerType, 
                    TriggerDataSel.NONE //triggerDataSel
            );
        }
    }
    
    
    /**
     * @author KAROL
     * Use e.g for the DAQ test
     */
    static public class ReapetPulserBC0_DaqLike extends TestPreset {
        public ReapetPulserBC0_DaqLike() {
            super(  128,  //pulseLenght, 
                    true, //pulseReapet, 
                    128 - 1, //pulserLimit, 
                    10 * RpctDelays.BX_IN_ORBIT, //readoutLimit,
                    TriggerType.BCN0, //pulserStartTriggerType, 
                    TriggerType.PRETRIGGER_0, //readoutStartTriggerType, 
                    TriggerDataSel.L1A //triggerDataSel
            );
        }		
    }

    static public class NoPulser_SnapshotL1A extends TestPreset {
        public NoPulser_SnapshotL1A(long readoutLimitInSec) {
            super(  128,  //pulseLenght, 
                    true, //pulseReapet, 
                    128 - 1, //pulserLimit, 
                    readoutLimitInSec * RpctDelays.BX_IN_SEC,
                    TriggerType.BCN0, //pulserStartTriggerType, 
                    TriggerType.L1A, //readoutStartTriggerType, 
                    TriggerDataSel.NONE //triggerDataSel
            );
        }		
    }
    
    static public class NoPulser_Pretrigger0 extends TestPreset {
        public NoPulser_Pretrigger0(double readoutLimitInSec) {
            super(  128,  //pulseLenght, 
                    true, //pulseReapet, 
                    128 - 1, //pulserLimit, 
                    (long)(readoutLimitInSec * RpctDelays.BX_IN_SEC),
                    TriggerType.BCN0, //pulserStartTriggerType, 
                    TriggerType.PRETRIGGER_0, //readoutStartTriggerType, 
                    TriggerDataSel.NONE //triggerDataSel
            );
        }       
    }

    static public class NoPulser_SnapshotBC0 extends TestPreset {
        public NoPulser_SnapshotBC0() {
            super(  128,  //pulseLenght, 
                    true, //pulseReapet, 
                    128 - 1, //pulserLimit, 
                    128,
                    TriggerType.BCN0, //pulserStartTriggerType, 
                    TriggerType.BCN0, //readoutStartTriggerType, 
                    TriggerDataSel.NONE //triggerDataSel
            );
        }       
    }
    
    static public class NoPulser_SnapshotTriggerLocal extends TestPreset {
        public NoPulser_SnapshotTriggerLocal(long readoutLimitInSec) {
            super(  128,  //pulseLenght, 
                    true, //pulseReapet, 
                    128 - 1, //pulserLimit, 
                    readoutLimitInSec * RpctDelays.BX_IN_SEC,
                    TriggerType.BCN0, //pulserStartTriggerType, 
                    TriggerType.LOCAL, //readoutStartTriggerType, 
                    TriggerDataSel.NONE //triggerDataSel
            );
        }       
    }

    /**
     * @author KAROL
     * Use e.g for the DAQ test
     */
    static public class NoPulser_DaqLike extends TestPreset {
        public NoPulser_DaqLike(long readoutLimitInSec) {
            super(  128,  //pulseLenght, 
                    true, //pulseReapet, 
                    128 - 1, //pulserLimit, 
                    readoutLimitInSec * RpctDelays.BX_IN_SEC,
                    TriggerType.BCN0, //pulserStartTriggerType, 
                    TriggerType.PRETRIGGER_0, //readoutStartTriggerType, 
                    TriggerDataSel.L1A //triggerDataSel
            );
        }		
    }
}
