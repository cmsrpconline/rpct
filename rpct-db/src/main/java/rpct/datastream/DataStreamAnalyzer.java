package rpct.datastream;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import rpct.datastream.TestOptionsXml.HardwareType;
import rpct.db.DataAccessException;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;


public class DataStreamAnalyzer {
	public DataStreamXml dataStream = new DataStreamXml();

	public static void main(String[] args) {
		final File xmlFile = new File(System.getenv("HOME") + "/bin/out/eventData.xml");
		HibernateContext context = new SimpleHibernateContextImpl();
		EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
		try {       	

			List<Board> lbs = equipmentDAO.getBoardsByType(BoardType.LINKBOARD, false);

			JAXBContext context1 = JAXBContext.newInstance(DataStreamXml.class) ;
			System.out.println("reading the " + xmlFile);
			Unmarshaller um = context1.createUnmarshaller();
			DataStreamXml data1 = (DataStreamXml) um.unmarshal(xmlFile);

			int bxFrom = 20;
			int bxTo = 50;

			System.out.println("data1.getEvents().size() " + data1.getEvents().size());
			for(EventXml eventXml : data1.getEvents()) {
				System.out.println(" eventXml.getBxData().size() "  + eventXml.getBxData().size());

				for(Board board  : lbs) {
					LinkBoard lb = (LinkBoard)board;
					if(lb.getCrate().getName().contains("LBB_YE") == false) {
						continue;
					}
					
					System.out.print(lb.getName() + " " + lb.getChamberNames().first() + "\t");
					for(int bx = bxFrom; bx <= bxTo; bx++) {
						//for(BxDataXml bxDataXml : eventXml.getBxData()) {
						BxDataXml bxDataXml = eventXml.getBxData().get(bx);
						int partitionsCnt = 0;
						if(bxDataXml != null) {
							LbDataXml lbDataXml = bxDataXml.getLbData().get(lb.getId());
							
							if(lbDataXml != null) {
								LMuxBxDataImplXml.Set stripData = lbDataXml.getStripData();
								if(stripData != null) {
									partitionsCnt = stripData.size();
								}
							}
						}
						System.out.print(" " + partitionsCnt);
					}
					System.out.print("\n");
				}
			}
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} /*catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/ catch (DataAccessException e) {
        	// TODO Auto-generated catch block
        	e.printStackTrace();
        }
        System.out.println("end");
	}
}



