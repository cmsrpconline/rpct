package rpct.datastream;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import rpct.db.DataAccessException;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;


public class TestPulsesGenerator {
    public DataStreamXml dataStream = new DataStreamXml();
    
/*    void generateLB_OptoData(List<Board> lbs) {
        Random rndGen = new Random();
        EventXml event = dataStream.getOrAddEvent(0, 0);
        for(int iBx = 10; iBx < 11; iBx+=10) {
            BxDataXml bxData = event.getOrAddBxData(iBx);  
            for(Board lb : lbs) {
                LMuxBxDataImplXml.Set coderData = new LMuxBxDataImplXml.Set();
                for(int i = 0; i < 8; i++) {
                    int index = ReadoutItemId.LbDataSource.lmux.getIntVal();
                    int lbNum = rndGen.nextInt(3);
                    int partitionNum = rndGen.nextInt(12);                       
                    int partitionDelay = 0;                    
                    int partitionData = rndGen.nextInt(256);;
                    int endOfData = 0;
                    int halfPartition = 0;
                    coderData.add(new LMuxBxDataImplXml(index, lbNum, partitionNum, partitionDelay, partitionData, endOfData, halfPartition));
                }
                bxData.getOrAddLbData(lb.getId()).setCoderData(coderData);
                
                LinkBoard mlb = (LinkBoard) lb; 
                if(mlb.isMaster() == false) {
                    throw new RuntimeException("The LinkBoard with id " + lb.getId() + "is not MASTER");
                }
                
                for(LinkConn linkConn : mlb.getLinkConns() ) {
                    ReadoutItemId optLinkId = ReadoutItemId.createOptLinkId(linkConn);
                    bxData.addOptLinkData(optLinkId, coderData);
                }
            }
        }
    }*/
    
    
    public void generateSLB_OptoData(List<LinkConn> linkConns) {
        Random rndGen = new Random();
        EventXml event = dataStream.getOrAddEvent(0, 0);
        for(int iBx = 10; iBx < 11; iBx+=10) {
            BxDataXml bxData = event.getOrAddBxData(iBx);  
            for(LinkConn linkConn : linkConns) {                               
                LinkBoard mlb = (LinkBoard)linkConn.getSynCoder().getBoard();
                LMuxBxDataImplXml.Set lmuxData = new LMuxBxDataImplXml.Set();
                for(LinkBoard slb : mlb.getSlaves()) {
                    if(slb == mlb)
                        continue;
                   
                    LMuxBxDataImplXml.Set coderData = new LMuxBxDataImplXml.Set();                    
                    for(int i = 0; i < 1; i++) {
                        int index = ReadoutItemId.LbDataSource.coder.getIntVal();
                        int lbNum = 0;//slb.getChannelNum();
                        int partitionNum = 0;//rndGen.nextInt(12);                       
                        int partitionDelay = 0;                    
                        int partitionData = rndGen.nextInt(256);;
                        int endOfData = 0;
                        int halfPartition = 0;
                        coderData.add(new LMuxBxDataImplXml(index, lbNum, partitionNum, partitionDelay, partitionData, endOfData, halfPartition));
                        index = ReadoutItemId.LbDataSource.lmux.getIntVal();
                        lmuxData.add(new LMuxBxDataImplXml(index, lbNum, partitionNum, partitionDelay, partitionData, endOfData, halfPartition));
                    }
                    bxData.getOrAddLbData(slb.getId()).setCoderData(coderData);
                }
                bxData.getOrAddLbData(mlb.getId()).setCoderData(lmuxData);

                ReadoutItemId optLinkId = ReadoutItemId.createOptLinkId(linkConn);
                bxData.addOptLinkData(optLinkId, lmuxData);
            }
        }
    }
    
    public void generateMLB_OptoData(List<LinkConn> linkConns) {
        Random rndGen = new Random();
        EventXml event = dataStream.getOrAddEvent(0, 0);
        for(int iBx = 10; iBx < 11; iBx+=10) {
            BxDataXml bxData = event.getOrAddBxData(iBx);  
            for(LinkConn linkConn : linkConns) {
                LinkBoard mlb = (LinkBoard)linkConn.getSynCoder().getBoard(); 
                LMuxBxDataImplXml.Set coderData = new LMuxBxDataImplXml.Set();
                for(int i = 0; i < 8; i++) {
                    int index = ReadoutItemId.LbDataSource.lmux.getIntVal();
                    int lbNum = rndGen.nextInt(3);
                    int partitionNum = rndGen.nextInt(12);                       
                    int partitionDelay = 0;                    
                    int partitionData = rndGen.nextInt(256);;
                    int endOfData = 0;
                    int halfPartition = 0;
                    coderData.add(new LMuxBxDataImplXml(index, lbNum, partitionNum, partitionDelay, partitionData, endOfData, halfPartition));
                }
                bxData.getOrAddLbData(mlb.getId()).setCoderData(coderData);

                ReadoutItemId optLinkId = ReadoutItemId.createOptLinkId(linkConn);
                bxData.addOptLinkData(optLinkId, coderData);

            }
        }
    }
    
    public void generateLB_OptoData() {
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);               
        try {
            //List<Board> lbs = equipmentDAO.getBoardsByNameLike("*RB+2_S10*", true);
/*            List<Board> lbs = new ArrayList<Board>();          
            for(Board b : equipmentDAO.getCrateByName("LBB_RB+2_S10").getBoards()) {
                if(b.getType() == BoardType.LINKBOARD && ((LinkBoard)b).isMaster())
                    lbs.add(b);
            }*/
            List<TriggerBoard> tbs = new ArrayList<TriggerBoard>();
      
            tbs.add((TriggerBoard) equipmentDAO.getBoardByName("TBp1_9"));
            tbs.add((TriggerBoard) equipmentDAO.getBoardByName("TBp2_9"));
            
            List<LinkConn> linkConns = new ArrayList<LinkConn>();
            for(TriggerBoard tb : tbs) {            
                for(LinkConn linkConn : tb.getLinkConns()) {                        
                    if(linkConn.getSynCoder().getBoard().getName().contains("LB_RB+2_S10") || linkConn.getSynCoder().getBoard().getName().contains("LB_RB+2_S11") ) 
                        linkConns.add(linkConn);
                }
            }
            generateSLB_OptoData(linkConns);
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    public void generateLBData() {
    	EventXml event = dataStream.getOrAddEvent(0, 0);
    	for(int iBx = 10; iBx < 11; iBx+=10) {
    		BxDataXml bxData = event.getOrAddBxData(iBx);  
    		for(int lbId = 1528; lbId <= 1555; lbId+=1) {
    	  		//bxData.getOrAddLbData(lbId).setPulserData(20+1);
    			//bxData.getOrAddLbData(lbId).setPulserData(iBx+1);
    			bxData.getOrAddLbData(lbId).setPulserData(0x1);
/*    			LMuxBxDataImplXml lmuxData = new LMuxBxDataImplXml(0, );
    			
    			bxData.getOrAddLbData(lbId).setPulserData(LMuxBxDataImplXml);*/
    		}
    	}
    }
    
    public void generateLBData(List<Integer> lbIds) {
        EventXml event = dataStream.getOrAddEvent(0, 0);
        
        int iBx = 0;
        for(int lbId : lbIds) {
            BxDataXml bxData = event.getOrAddBxData(iBx%100);  

            bxData.getOrAddLbData(lbId).setPulserData(0x1);
            iBx++;
        }
    }
    
    public void generateLBDataOneBx(List<Integer> lbIds) {
        //Random generator = new Random();
        EventXml event = dataStream.getOrAddEvent(0, 0);      
        for(int iBx = 0; iBx <128; iBx+=1) {
        	if(iBx!=90)
        		continue;
            BxDataXml bxData = event.getOrAddBxData(iBx);  
            for(int lbId : lbIds) {
                //bxData.getOrAddLbData(lbId).setPulserData(0x1);
                //bxData.getOrAddLbData(lbId).setPulserData(generator.nextInt(0xffffff));
                //bxData.getOrAddLbData(lbId).setPulserData(1 << (shift%24));
            	/*if(iBx == 20 || iBx == 21 || iBx == 22 || iBx == 23 || iBx == 24 || iBx == 25)
            		bxData.getOrAddLbData(lbId).setPulserData(0x555555);
            	else 
            		bxData.getOrAddLbData(lbId).setPulserData(0xffffff);*/
            	//if(iBx%10 == 0 )
            		bxData.getOrAddLbData(lbId).setPulserData(0x1);//0x030303
            }
            if(iBx > 127)
                break;
        }
    }
    
   
    public void generateLBDataForTwinMux(List<Integer> lbIds, int[] data ) {
        //Random generator = new Random();
        EventXml event = dataStream.getOrAddEvent(0, 0);      
        for(int iBx = 0; iBx <data.length; iBx+=1) {        
            BxDataXml bxData = event.getOrAddBxData(iBx);  
            for(int lbId : lbIds) {                
            	bxData.getOrAddLbData(lbId).setPulserData(data[iBx]);
            }
            if(iBx > 127)
                break;
        }
    }
    
    /**
     * @param lbIds
     * @param bxProb - 0....100
     * @param stripProb 
     */
    public void generateLBDataRandom(List<Integer> lbIds, int bxProb, int maxStripsInBx) {
        Random generator = new Random();
        EventXml event = dataStream.getOrAddEvent(0, 0);
        for(int iBx = 0; iBx < 127; iBx+=1) {
            BxDataXml bxData = event.getOrAddBxData(iBx);  
            for(int lbId : lbIds) {
            	if(generator.nextInt(100) < bxProb) {
            		int strips = 0;
            		for(int i = 0; i < maxStripsInBx; i++) {
            			strips |= 1 << generator.nextInt(24);
            		}            		
            		bxData.getOrAddLbData(lbId).setPulserData(strips);
            	}
            }
            if(iBx > 100)
                break;
        }
    }
    
    public void generateLBDataForOptLinksTets(List<Integer> lbIds) {
        //Random generator = new Random();
        EventXml event = dataStream.getOrAddEvent(0, 0);
        int shift = 0;
        int iBx = 0; 
        for(int lbId : lbIds) {
            BxDataXml bxData = event.getOrAddBxData(iBx); 
            //bxData.getOrAddLbData(lbId).setPulserData(0x1);
            //bxData.getOrAddLbData(lbId).setPulserData(generator.nextInt(0xffffff));
            bxData.getOrAddLbData(lbId).setPulserData(1 << (shift%24));
            shift++;
            iBx++;
            if(iBx > 50)
                iBx = 0;
        }
        
    }
    
    public void generateLBDataForSlaveMasterTest(List<Integer> lbIds) {
        //Random generator = new Random();
        EventXml event = dataStream.getOrAddEvent(0, 0);
        int shift = 0;
        for(int iBx = 0; iBx < 118; iBx+=10) {
            BxDataXml bxData = event.getOrAddBxData(iBx);  
            for(int lbId : lbIds) {
                bxData.getOrAddLbData(lbId).setPulserData(0x5aa5 << (shift%16));               
            }
            shift++;
            if(iBx > 127)
                break;
        }
    }
/*    
    public void generateLBGolDataForSlaveMasterTest(List<Integer> lbIds) {
        //Random generator = new Random();
        EventXml event = dataStream.getOrAddEvent(0, 0);
        int shift = 0;
        for(int iBx = 0; iBx < 118; iBx+=10) {
            BxDataXml bxData = event.getOrAddBxData(iBx);  
            for(int lbId : lbIds) {
                bxData.getOrAddLbData(lbId).setCoderData(0x5aa5 << (shift%16));               
            }
            shift++;
            if(iBx > 127)
                break;
        }
    }
    */
    public void generateLBCPulserData() {
        EventXml event = dataStream.getOrAddEvent(0, 0);
        for(int iBx = 10; iBx < 11; iBx+=10) {
            BxDataXml bxData = event.getOrAddBxData(iBx);  
            for(int lbId = 1848; lbId <= 1875; lbId+=1) {
                //bxData.getOrAddLbData(lbId).setPulserData(20+1);
                //bxData.getOrAddLbData(lbId).setPulserData(iBx+1);
                
                LMuxBxDataImplXml lmuxData = new LMuxBxDataImplXml(0, 0, 0, 0, lbId, 0, 0);
                
                //bxData.getOrAddLbData(lbId).setPulserData(lbId - 1848 + 1);
                bxData.getOrAddLbData(lbId).setPulserData(1);
                
                //bxData.getOrAddLbData(lbId).setCoderData(coderData)
                
/*              LMuxBxDataImplXml lmuxData = new LMuxBxDataImplXml(0, );
                
                bxData.getOrAddLbData(lbId).setPulserData(LMuxBxDataImplXml);*/
            }
            break;
        }
    }
    
    public void generateLinkData(int[] tcNums) {
        Random generator = new Random();
        EventXml event = dataStream.getOrAddEvent(0, 0);
        for(Integer tcNum : tcNums) {
            for(int iBx = 0; iBx < 110; iBx+=2) {
                BxDataXml bxData = event.getOrAddBxData(iBx);
                TcDataXml tcDataXml = bxData.getOrAddTcData(tcNum);
                for(int tbNum = 1; tbNum < 9; tbNum++) {
                    TbDataXml tbDataXml = tcDataXml.getOrAddTbData(tbNum);
                    for(int iLink = 0; iLink < 5; iLink++) {
                        OptLinkDataXml linkDataXml = tbDataXml.getOrAddOptLink(iLink);
                        //for(int i = 0; i < 3; i++) 
                        {
                            int index = iLink;
                            int lbNum = 0;//i;//i%3;//generator.nextInt(3);
                            int partitionNum = 0;//tbNum;//generator.nextInt(12);
                            int partitionDelay = 0;
                            int partitionData  = 0xff;//iLink;//generator.nextInt(256);
                            int endOfData = 0;
                            int halfPartition = 0;
                            //linkDataXml.getLinkData().add(new LMuxBxDataImplXml(index, lbNum, partitionNum, partitionDelay, partitionData, endOfData, halfPartition));
                            linkDataXml.getLinkData().add(new LMuxBxDataImplXml(index, lbNum, partitionNum, partitionDelay, partitionData, endOfData, halfPartition));
                        }
                    }
                }
            }
        }
    }
    
    public void generateLinkData2(int[] tcNums) {
        Random generator = new Random();
        EventXml event = dataStream.getOrAddEvent(0, 0);
        for(Integer tcNum : tcNums) {
            for(int iBx = 5; iBx < 12; iBx+=2) {
                BxDataXml bxData = event.getOrAddBxData(iBx);
                TcDataXml tcDataXml = bxData.getOrAddTcData(tcNum);
                for(int tbNum = 1; tbNum < 9; tbNum++) {
                    TbDataXml tbDataXml = tcDataXml.getOrAddTbData(tbNum);
                    for(int iLink = 1; iLink < 5; iLink++) 
                    {                      
                        OptLinkDataXml linkDataXml = tbDataXml.getOrAddOptLink(iLink);
                        for(int i = 0; i < 3; i++) {
                            int index = iLink;
                            int lbNum = i;//i%3;//generator.nextInt(3);
                            int partitionNum = 3;//generator.nextInt(12);
                            int partitionDelay = 0;
                            int partitionData  = 0xff;//generator.nextInt(256);
                            int endOfData = 0;
                            int halfPartition = 0;
                            linkDataXml.getLinkData().add(new LMuxBxDataImplXml(index, lbNum, partitionNum, partitionDelay, partitionData, endOfData, halfPartition));
                        }
                    }
                }
            }
        }
    }
    public void generateLinkData3(int[] tcNums) {
        Random generator = new Random();
        EventXml event = dataStream.getOrAddEvent(0, 0);
        for(Integer tcNum : tcNums) {
            for(int iBx = 2; iBx < 60; iBx+=4) 
            {
                //int iBx = 15;
                BxDataXml bxData = event.getOrAddBxData(iBx);
                TcDataXml tcDataXml = bxData.getOrAddTcData(tcNum);
                for(int tbNum = 1; tbNum < 9; tbNum++) {
                    TbDataXml tbDataXml = tcDataXml.getOrAddTbData(tbNum);
                    for(int iLink = 0; iLink < 18; iLink++) 
                    {                      
                        OptLinkDataXml linkDataXml = tbDataXml.getOrAddOptLink(iLink);
                        for(int i = 0; i < 8; i++) {
                            int index = iLink;
                            int lbNum = 1;//;//generator.nextInt(3);
                            int partitionNum = i;//generator.nextInt(12);
                            int partitionDelay = 0;
                            int partitionData  = 0xff;//generator.nextInt(256);
                            int endOfData = 0;
                            int halfPartition = 0;
                            linkDataXml.getLinkData().add(new LMuxBxDataImplXml(index, lbNum, partitionNum, partitionDelay, partitionData, endOfData, halfPartition));
                        }
                    }
                }
            }
            
            {
                int iBx = 25;
                BxDataXml bxData = event.getOrAddBxData(iBx);
                TcDataXml tcDataXml = bxData.getOrAddTcData(tcNum);
                for(int tbNum = 1; tbNum < 9; tbNum++) {
                    TbDataXml tbDataXml = tcDataXml.getOrAddTbData(tbNum);
                    for(int iLink = 0; iLink < 18; iLink++) 
                    {                      
                        OptLinkDataXml linkDataXml = tbDataXml.getOrAddOptLink(iLink);
                        for(int i = 0; i < 6; i++) {
                            int index = iLink;
                            int lbNum = i%3;//;//generator.nextInt(3);
                            int partitionNum = 3;//generator.nextInt(12);
                            int partitionDelay = 0;
                            int partitionData  = 0xff;//generator.nextInt(256);
                            int endOfData = 0;
                            int halfPartition = 0;
                            linkDataXml.getLinkData().add(new LMuxBxDataImplXml(index, lbNum, partitionNum, partitionDelay, partitionData, endOfData, halfPartition));
                        }
                    }
                }
            }
        }
    }
    
    public void generateLinkDataBxNum(int[] tcNums, int bxCn) {
        Random generator = new Random();
        EventXml event = dataStream.getOrAddEvent(0, 0);
        for(Integer tcNum : tcNums) {
            for(int iBx = 1; iBx < bxCn; iBx+=1) {
                BxDataXml bxData = event.getOrAddBxData(iBx);
                TcDataXml tcDataXml = bxData.getOrAddTcData(tcNum);
                for(int tbNum = 1; tbNum < 9; tbNum++) {
                    TbDataXml tbDataXml = tcDataXml.getOrAddTbData(tbNum);
                    for(int iLink = tbNum; iLink < tbNum+1; iLink++) {
                        OptLinkDataXml linkDataXml = tbDataXml.getOrAddOptLink(iLink);
                        for(int i = 0; i < 1; i++) {
                            int index = iLink;
                            int lbNum = tbNum%3;//i%3;//generator.nextInt(3);
                            int partitionNum = tbNum;//generator.nextInt(12);
                            int partitionDelay = 0;
                            int partitionData  = iBx;//generator.nextInt(256);
                            int endOfData = 0;
                            int halfPartition = 0;
                            linkDataXml.getLinkData().add(new LMuxBxDataImplXml(index, lbNum, partitionNum, partitionDelay, partitionData, endOfData, halfPartition));
                        }
                    }
                }
            }
        }
    }
    
    public void generateLinkDataRnd(int[] tcNums, int bxCnt) {
        Random generator = new Random();
        EventXml event = dataStream.getOrAddEvent(0, 0);
        for(Integer tcNum : tcNums) {
            for(int iBx = 3; iBx < bxCnt +3; iBx+=10) {
                BxDataXml bxData = event.getOrAddBxData(iBx);
                TcDataXml tcDataXml = bxData.getOrAddTcData(tcNum);
                for(int tbNum = 1; tbNum < 9; tbNum++) {
                    TbDataXml tbDataXml = tcDataXml.getOrAddTbData(tbNum);
                    for(int iLink = 0; iLink < 18; iLink++) {
                        OptLinkDataXml linkDataXml = tbDataXml.getOrAddOptLink(iLink);
                        int offset = generator.nextInt(12);
                        for(int i = 0; i < 1; i++) {
                            int index = iLink;
                            int lbNum = generator.nextInt(3);
                            int partitionNum = (offset + i)%12;
                            int partitionDelay = 0;
                            int partitionData  = generator.nextInt(256);
                            if(partitionData == 0) {
                                partitionData  = generator.nextInt(256); 
                            }
                            int endOfData = 0;
                            int halfPartition = 0;
                            linkDataXml.getLinkData().add(new LMuxBxDataImplXml(index, lbNum, partitionNum, partitionDelay, partitionData, endOfData, halfPartition));
                        }
                    }
                }
            }
        }
    }
    
    public void generateLinkDataRnd1(int[] tcNums, int bxCnt) {
        Random generator = new Random();
        EventXml event = dataStream.getOrAddEvent(0, 0);
        for(Integer tcNum : tcNums) {
            for(int iBx = 0; iBx < bxCnt; iBx+=9) {
                BxDataXml bxData = event.getOrAddBxData(iBx);
                TcDataXml tcDataXml = bxData.getOrAddTcData(tcNum);
                for(int tbNum = 4; tbNum < 5; tbNum++) {
                    TbDataXml tbDataXml = tcDataXml.getOrAddTbData(tbNum);
                    for(int iLink = 0; iLink < 4; iLink++) {
                        OptLinkDataXml linkDataXml = tbDataXml.getOrAddOptLink(iLink);
                        int offset = generator.nextInt(12);
                        for(int i = 0; i < 2; i++) {
                            int index = iLink;
                            int lbNum = generator.nextInt(3);
                            int partitionNum = (offset + i)%12;
                            int partitionDelay = 0;
                            int partitionData  = generator.nextInt(256);
                            if(partitionData == 0) {
                                partitionData  = generator.nextInt(256); 
                            }
                            int endOfData = 0;
                            int halfPartition = 0;
                            linkDataXml.getLinkData().add(new LMuxBxDataImplXml(index, lbNum, partitionNum, partitionDelay, partitionData, endOfData, halfPartition));
                        }
                    }
                }
            }
        }
    }
    
    
    
    public void genereateDataFromMLB(LinkBoard mLB, BxDataXml bxData, Random generator ) {
        List<LMuxBxDataImplXml>  linkData = new ArrayList<LMuxBxDataImplXml>();
        for(int iLB = 0; iLB < 3; iLB++) {
            LinkBoard lb = mLB.getSlave(iLB);
            if(lb != null) {
                int offset = generator.nextInt(12);
                int framesCnt = 0;
                for(int i = 0; i < 1; i++) {
                    int index = 0;
                    int lbNum = iLB;
                    int partitionNum = (offset + i)%12;
                    int partitionDelay = 0;
                    int partitionData  = generator.nextInt(256);
                    int endOfData = 0;
                    int halfPartition = 0;
                    if(generator.nextInt(2) != 0 && partitionData != 0 ) {
                        if(framesCnt > 8)
                            break;
                        linkData.add(new LMuxBxDataImplXml(index, lbNum, partitionNum, partitionDelay, partitionData, endOfData, halfPartition));
                        framesCnt++;                        
                    }
                }
            }
        }
         
        for(LinkConn linkConn: mLB.getLinkConns()) {
            TcDataXml tcDataXml = bxData.getOrAddTcData(linkConn.getTriggerBoard().getTriggerCrate().getLogSector() );
            
            TbDataXml tbDataXml = tcDataXml.getOrAddTbData(linkConn.getTriggerBoard().getNum());
            
            OptLinkDataXml linkDataXml = tbDataXml.getOrAddOptLink(linkConn.getOptoInputNum());
            if(linkDataXml.getLinkData().size() != 0) {
                System.out.println("linkDataXml not empty");
            }
            else
                linkDataXml.getLinkData().addAll(linkData);
        }
    }
    
    public void generateLinkDataDetecorLike(int bxCnt, int muonsSpacing, EquipmentDAO equipmentDAO) throws DataAccessException {
        Random generator = new Random();
        EventXml event = dataStream.getOrAddEvent(0, 0);
        
        List<Board> boards = equipmentDAO.getBoardsByType(BoardType.LINKBOARD, false);
        List<LinkBoard> mLBs = new ArrayList<LinkBoard>();
        for(Board board : boards) {
            if(  ((LinkBoard)board).isMaster() ) {
                if(board.getName().contains("LB_RB0_S1_")) {
                    mLBs.add((LinkBoard)board);
                    System.out.println(board.getName());
                }
            }
        }
        
        for(int iBx = 0; iBx < bxCnt; iBx++) {  
            for(int iMLB = 0; iMLB <  mLBs.size(); iMLB++) {
                BxDataXml bxData = event.getOrAddBxData(iBx);             
                //noise
                //LinkBoard mLB = mLBs.get(generator.nextInt(mLBs.size()));              
                LinkBoard mLB = mLBs.get(iMLB);
                genereateDataFromMLB(mLB, bxData, generator);
                //muon
                if(iBx%muonsSpacing == 1) {                
                    for(LinkBoard lb : mLB.getLinkBox().getLinkBoards()) {
                        if((lb.getId() == mLB.getId()))
                            continue;
                        if(lb.isMaster()) {
                            genereateDataFromMLB(lb, bxData, generator);
                        }
                    }
                }    
            }
        }
    }
    
    public void generateLinkDataRnd() {
        Random generator = new Random();
        EventXml event = dataStream.getOrAddEvent(0, 0);
        int tcNum = 9;
        for(int iBx = 10; iBx < 120; iBx+=9) {
            BxDataXml bxData = event.getOrAddBxData(iBx);
            TcDataXml tcDataXml = bxData.getOrAddTcData(tcNum);
            for(int tbNum = 1; tbNum < 9; tbNum++) {
                TbDataXml tbDataXml = tcDataXml.getOrAddTbData(tbNum);
                for(int iLink = 0; iLink < 18; iLink++) {
                    OptLinkDataXml linkDataXml = tbDataXml.getOrAddOptLink(iLink);
                    int partNum = generator.nextInt(12);
                    for(int i = 0; i < 8; i++) {
                        int index = iLink;
                        int lbNum = generator.nextInt(3);
                        int partitionNum = (partNum + i)%8; //aby byly niepowtarzalne
                        int partitionDelay = 0;
                        int partitionData  = generator.nextInt(256);
                        int endOfData = generator.nextInt(2);
                        int halfPartition = generator.nextInt(2);;
                        linkDataXml.getLinkData().add(new LMuxBxDataImplXml(index, lbNum, partitionNum, partitionDelay, partitionData, endOfData, halfPartition));
                    }
                }
            }
        }
    }
    
    public void generatePACTest(int[] tcNum) {
        //Random generator = new Random();
        EventXml event = dataStream.getOrAddEvent(0, 0);        
        int value = 1;
        int indx = 1;
        for(int iBx = 10; iBx < 12; iBx+=3) {            
            BxDataXml bxData = event.getOrAddBxData(iBx);           
            for(int iTc : tcNum) {    
                TcDataXml tcDataXml =  bxData.getOrAddTcData(iTc);
                for(int iTB = 0; iTB < 9; iTB++) {
                    TbDataXml tbDataXml = tcDataXml.getOrAddTbData(iTB);
                    for(int iPac = 0; iPac < 4; iPac++) {
                        if(iTB >= 3 && iTB <=5 && iPac == 3)
                            break;
                        PacDataXml pacDataXml = tbDataXml.getOrAddPac(iPac);
                        for(int iMu = 0; iMu < 12; iMu++) {
                            int bitValue = 0;
                            //if(indx == iBx % 4) 
                            {
                               // bitValue = value;
                                //bitValue =  generator.nextInt(1 << HardwareMuon.PacOutMuon.getMuonBitsCnt() + 1);
                                MuonImplXml muon = new HardwareMuon.PacOutMuon(0, iMu);
                                muon.setPtCode(iBx);
                                pacDataXml.getMuons().add(muon);                       
                            }                       
                        }                
                    }
                }
            }
        }
    }

    public void generatePACBXnum(int[] tcNum) {
        //Random generator = new Random();
        EventXml event = dataStream.getOrAddEvent(0, 0);        
        int value = 1;
        int indx = 1;
        for(int iBx = 10; iBx < 12; iBx+=3) {            
            BxDataXml bxData = event.getOrAddBxData(iBx);           
            for(int iTc : tcNum) {    
                TcDataXml tcDataXml =  bxData.getOrAddTcData(iTc);
                for(int iTB = 4; iTB < 5; iTB++) {
                    TbDataXml tbDataXml = tcDataXml.getOrAddTbData(iTB);
                    for(int iPac = 1; iPac < 2; iPac++) {
                        if(iTB >= 3 && iTB <=5 && iPac == 3)
                            break;
                        PacDataXml pacDataXml = tbDataXml.getOrAddPac(iPac);
                        for(int iMu = 0; iMu < 12; iMu++) {
                            int bitValue = 0;
                            if(iMu == 0) 
                            {
                               // bitValue = value;
                                //bitValue =  generator.nextInt(1 << HardwareMuon.PacOutMuon.getMuonBitsCnt() + 1);
                                MuonImplXml muon = new HardwareMuon.PacOutMuon(0, iMu);
                                muon.setPtCode(31);
                                pacDataXml.getMuons().add(muon);                       
                            }      
                            if(iMu == 10) 
                            {
                               // bitValue = value;
                                //bitValue =  generator.nextInt(1 << HardwareMuon.PacOutMuon.getMuonBitsCnt() + 1);
                                MuonImplXml muon = new HardwareMuon.PacOutMuon(0, iMu);
                                muon.setPtCode(31);
                                pacDataXml.getMuons().add(muon);                       
                            }  
                            if(iMu == 11) 
                            {
                               // bitValue = value;
                                //bitValue =  generator.nextInt(1 << HardwareMuon.PacOutMuon.getMuonBitsCnt() + 1);
                                MuonImplXml muon = new HardwareMuon.PacOutMuon(0, iMu);
                                muon.setPtCode(30);
                                pacDataXml.getMuons().add(muon);                       
                            }  
                        }                
                    }
                }
            }
        }
    }
    
    public void generatePACOutRnd(int[] tcNum, int bxCnt) {
        Random generator = new Random();
        EventXml event = dataStream.getOrAddEvent(0, 0);        
        int value = 1;
        int indx = 1;
        for(int iBx = 1; iBx < bxCnt; iBx+=1) {            
            BxDataXml bxData = event.getOrAddBxData(iBx);           
            for(int iTc : tcNum) {    
                TcDataXml tcDataXml =  bxData.getOrAddTcData(iTc);
                for(int iTB = 0; iTB < 9; iTB++) {
                    TbDataXml tbDataXml = tcDataXml.getOrAddTbData(iTB);
                    for(int iPac = 0; iPac < 4; iPac++) {
                        if(iTB >= 3 && iTB <=5 && iPac == 3)
                            break;
                        PacDataXml pacDataXml = tbDataXml.getOrAddPac(iPac);
                        for(int iMu = 0; iMu < 12; iMu++) {
                            int bitValue = 0;
                            //if(indx == iBx % 4) 
                            {
                                bitValue = 0;
                                MuonImplXml muon = new MuonImplXml();;
                                while (muon.getPtCode() == 0) {
                                    bitValue =  generator.nextInt(1 << HardwareMuon.PacOutMuon.getMuonBitsCnt() + 1);
                                    muon = new HardwareMuon.PacOutMuon(bitValue, iMu);
                                }
                                
                                pacDataXml.getMuons().add(muon);                       
                            }                       
                        }                
                    }
                }
            }
        }
    }
    
    public void generateTcSortOut() {
        EventXml event = dataStream.getOrAddEvent(0, 0);		
        int value = 1;
        int i = 1;
        int eta_tab[] = {0x11, 0x12, 0x14, 0x15, 0x16, 0x18, 0x19, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29}; 
        ///for(int iBx = 0; iBx < 4 * HardwareMuon.TcSortOutMuon.getMuonBitsCnt(); iBx+=4) {
        for(int iBx = 2; iBx < 3; iBx++) {
            int indx = 0;
            BxDataXml bxData = event.getOrAddBxData(iBx);			
            for(int iTc = 0; iTc < 12; iTc+=1) {												
                for(int iMu = 0; iMu < 2; iMu++, indx++) {	//petla po sektorze logicznym
                    int bitValue = 0;
                    if(iBx % 2 == 0) 
                    {
                        //bitValue = value;
                        //MuonImplXml muon = new HardwareMuon.TcSortOutMuon(bitValue, iMu);
                       // MuonImplXml muon = new MuonImplXml(0, iMu, i, i%8, i%2, 0, 0, 0); <---TO DO
                    	//MuonImplXml muon = new MuonImplXml(0, iMu, i, 2, i%2, eta_tab[iBx%16], 0, 0);
                    	MuonImplXml muon = new MuonImplXml(0, iMu, i%32, 2, i%2, i%33 - 16, iMu * 5 +1 , 0);
                    	//MuonImplXml muon = new HardwareMuon.TcSortOutMuon(bitValue, iMu);
                        TcDataXml tcDataXml =  bxData.getOrAddTcData(iTc);
                        tcDataXml.getGbSortMuons().getMuons().add(muon);						
                        i++;
                    }						
                }
                value = value <<1;
            }
                         
            //break; //TODO
        }

        System.out.println("last value " + value);
    }

    public void generateTcSortOutRnd(String[] tcInputs, int bxCnt) {
        Random generator = new Random();
        EventXml event = dataStream.getOrAddEvent(0, 0);        
        int value = 1;
        int indx = 1;
        for(int iBx = 0; iBx < bxCnt; iBx+=3) {            
            BxDataXml bxData = event.getOrAddBxData(iBx + 4);           
            for(int iTc = 0; iTc < 12; iTc++) {                                             
                for(int iMu = 0; iMu < 4; iMu++) {
                    int bitValue = 0;
                    if(tcInputs[iTc * 2 + 1].charAt(1 - iMu / 2) == '1') 
                    {
                        bitValue = value;
                        bitValue =  generator.nextInt(1 << HardwareMuon.TcSortOutMuon.getMuonBitsCnt() + 1);
                        MuonImplXml muon = new HardwareMuon.TcSortOutMuon(bitValue, iMu);
                        TcDataXml tcDataXml =  bxData.getOrAddTcData(iTc);
                        tcDataXml.getGbSortMuons().getMuons().add(muon);                        
                    }                       
                }                
            }
            //value = (1 << indx) + indx;
            value = (1 << indx);
            indx++;
        }

        System.out.println("last value " + value);
    }
    
    public void generateTcSortOutRnd(int bxCnt) {
        String[] tcInputs = {
                "TC_0", "11",
                "TC_1", "11",
                "TC_2", "11",
                "TC_3", "11",
                "TC_4", "11",
                "TC_5", "11",
                "TC_6", "11",
                "TC_7", "11",
                "TC_8", "11",
                "TC_9", "11",
                "TC_10", "11",
                "TC_11", "11",
        };
        
        generateTcSortOutRnd(tcInputs, bxCnt);
    }
    
    public void generateTcSortOut(int[] tcNum) {
        Random generator = new Random();
        EventXml event = dataStream.getOrAddEvent(0, 0);        
        int value = 1;
        int indx = 1;
        int bitValue = 1;
        for(int iBx = 10; iBx < 60; iBx+=3) {            
            BxDataXml bxData = event.getOrAddBxData(iBx);                       
            for(int iTc : tcNum) {                                             
                //bitValue =  generator.nextInt(1 << HardwareMuon.TcSortOutMuon.getMuonBitsCnt() + 1);
                //bitValue = 0x88888888>>(iBx%4);                
                for(int iMu = 0; iMu < 4; iMu++) {
                    //if(indx == iBx % 4) 
                    {
                        //bitValue = 0xffffffff;
                        //bitValue = indx %16;                        
                        bitValue = 0x100 + iTc;
                        MuonImplXml muon = new HardwareMuon.TcSortOutMuon(bitValue, iMu);
                        TcDataXml tcDataXml =  bxData.getOrAddTcData(iTc);
                        tcDataXml.getGbSortMuons().getMuons().add(muon);                        
                    }                       
                }                
            }
            bitValue = bitValue<<1;
        }
        
        for(int iBx = 70; iBx < 77; iBx+=3) {            
            BxDataXml bxData = event.getOrAddBxData(iBx);           
            for(int iTc : tcNum) {                                             
                bitValue =  0;//generator.nextInt(1 << HardwareMuon.TcSortOutMuon.getMuonBitsCnt() + 1);
                if(iBx == 70)
                    bitValue = 0x33 + (0x33<<7)  + (0x33<<14)  + (0x33<<21);
                if(iBx == 73)
                    bitValue = 0x4c + (0x4c<<7)  + (0x4c<<14)  + (0x4c<<21);
                if(iBx == 76)
                    bitValue = 0xffffffff;
                
                for(int iMu = 0; iMu < 4; iMu++) {
                    //if(indx == iBx % 4) 
                    {
                        //bitValue = 0xffffffff;
                        //bitValue = indx %16;                        
                        MuonImplXml muon = new HardwareMuon.TcSortOutMuon(bitValue, iMu);
                        TcDataXml tcDataXml =  bxData.getOrAddTcData(iTc);
                        tcDataXml.getGbSortMuons().getMuons().add(muon);                        
                    }                       
                }                
            }
        }
        for(int iBx = 79; iBx < 120; iBx+=1) {            
            BxDataXml bxData = event.getOrAddBxData(iBx);           
            for(int iTc : tcNum) {                                             
                bitValue =  0;//generator.nextInt(1 << HardwareMuon.TcSortOutMuon.getMuonBitsCnt() + 1);
                bitValue = 0xffffffff;
               
                for(int iMu = 0; iMu < 4; iMu++) {
                    //if(indx == iBx % 4) 
                    {
                        //bitValue = 0xffffffff;
                        //bitValue = indx %16;                        
                        MuonImplXml muon = new HardwareMuon.TcSortOutMuon(bitValue, iMu);
                        TcDataXml tcDataXml =  bxData.getOrAddTcData(iTc);
                        tcDataXml.getGbSortMuons().getMuons().add(muon);                        
                    }                       
                }                
            }
        }
    }
    
    public void generateHalfSortOut() {
        EventXml event = dataStream.getOrAddEvent(0, 0);		
        int value = 131072 / 2;
        for(int iBx = 0; iBx < 120; iBx++) {
            int indx = 0;
            BxDataXml bxData = event.getOrAddBxData(iBx);	
            for(int be =  0; be < 2; be++) {
                for(int iHsb = 0; iHsb < 2; iHsb++) {				
                    for(int iMu = 0; iMu < 4; iMu++, indx++) {
                        int bitValue = 0;
                        if(indx == iBx % 16) {
                            bitValue = value;
                            MuonImplXml muon = new HardwareMuon.HalfSortOutMuon(bitValue, iMu);						
                            HalfSortDataXml halfSortData = bxData.getOrAddHalfSortData(iHsb, be);
                            halfSortData.getMuons().add(muon);
                        }						
                    }
                }
            }
            if(iBx % 16 == 15)
                value = value <<1;
        }

        System.out.println("last value " + value);
    }

    public void generateHalfSortOut1() {
        EventXml event = dataStream.getOrAddEvent(0, 0);		
        int iBx = 0;
        for(int be =  1; be < 2; be++) {
            for(int iHsb = 0; iHsb < 2; iHsb++) {				
                for(int iMu = 0; iMu < 4; iMu++) {
                    int bitValue = 1 << 9;
                    for( ; ; ) {
                        if(iBx > 120)
                            return;
                        BxDataXml bxData = event.getOrAddBxData(iBx);	
                        MuonImplXml muon = new HardwareMuon.HalfSortOutMuon(bitValue, iMu);						
                        HalfSortDataXml halfSortData = bxData.getOrAddHalfSortData(iHsb, be);
                        halfSortData.getMuons().add(muon);
                        iBx++;
                        bitValue = bitValue <<1;
                        if(bitValue > (1<<21))
                            break;
                    }
                }
            }

        }
    }

    public void generateHalfSortOut2() {
        EventXml event = dataStream.getOrAddEvent(0, 0);		
        int iBx = 0;
        for(int be =  1; be < 2; be++) {
            for(int iHsb = 0; iHsb < 2; iHsb++) {				
                for(int iMu = 0; iMu < 4; iMu++) {
                    int bitValue = 1 << 9;
                    for( ; ; ) {
                        if(iBx > 120)
                            return;
                        BxDataXml bxData = event.getOrAddBxData(iBx);	
                        MuonImplXml muon = new HardwareMuon.HalfSortOutMuon(bitValue, iMu);						
                        HalfSortDataXml halfSortData = bxData.getOrAddHalfSortData(iHsb, be);
                        halfSortData.getMuons().add(muon);
                        iBx++;
                        if(iBx%10 == 0)
                            bitValue = bitValue <<1;
                        if(bitValue > (1<<21))
                            break;
                    }
                }
            }

        }
    }

    public void generateHalfSortOut3() {
        EventXml event = dataStream.getOrAddEvent(0, 0);		

        for(int be =  0; be < 2; be++) {
            for(int iHsb = 0; iHsb < 2; iHsb++) {				
                for(int iMu = 0; iMu < 4; iMu++) {
                    int bitValue = 0;
                    for(int iBx = 4; iBx < 115; iBx++) {						
                        BxDataXml bxData = event.getOrAddBxData(iBx);	
                        MuonImplXml muon = new HardwareMuon.HalfSortOutMuon(bitValue, iMu);			
                        muon.setPtCode(iBx%32);
                        muon.setQuality(iBx%3);
                        muon.setEtaAddress(iBx%31);
                        muon.setPhiAddress(iBx%143);
                        HalfSortDataXml halfSortData = bxData.getOrAddHalfSortData(iHsb, be);
                        halfSortData.getMuons().add(muon);						
                    }
                }
            }

        }
    }

    public void generateFinalSortOutWalking1() {
        EventXml event = dataStream.getOrAddEvent(0, 0);                
        for(int iBx = 0; iBx < 256; iBx++) { 
            //int bitValue = 0x1 << (iBx%HardwareMuon.FinalSortOutMuon.getDaqMuonBitsCnt());
            int bitValue = 0x1 << ( (iBx) %( HardwareMuon.FinalSortOutMuon.getPulserMuonBitsCnt()+2));
            //int bitValue = 0x1 << ( iBx%16 );
            for(int be =  0; be < 2; be++) {             
                for(int iMu = 0; iMu < 4; iMu++) {                    
                    BxDataXml bxData = event.getOrAddBxData(iBx);   
                    MuonImplXml muon = new HardwareMuon.FinalSortOutMuon(bitValue, iMu);                     
                    FinalSortDataXml finalSortDataXml = bxData.getOrAddFinalSortData(be); 
                    finalSortDataXml.getMuons().add(muon);
                }
            }
        }
        
    }
    
    public void generateFinalSortOutWalking0() {
        EventXml event = dataStream.getOrAddEvent(0, 0);                
        for(int iBx = 0; iBx < 256; iBx++) { 
            int bitValue = ~(0x1 << (iBx% (HardwareMuon.FinalSortOutMuon.getPulserMuonBitsCnt()+2) ));
            for(int be =  0; be < 2; be++) {             
                for(int iMu = 0; iMu < 4; iMu++) {                    
                    BxDataXml bxData = event.getOrAddBxData(iBx);   
                    MuonImplXml muon = new HardwareMuon.FinalSortOutMuon(bitValue, iMu);                     
                    FinalSortDataXml finalSortDataXml = bxData.getOrAddFinalSortData(be); 
                    finalSortDataXml.getMuons().add(muon);
                }
            }
        }
        
    }
    
    public void generateFinalSortOutRND() {
        Random rndGen = new Random(1234);
        EventXml event = dataStream.getOrAddEvent(0, 0);
        int bitMask = 0;
        for (int i = 0; i < HardwareMuon.FinalSortOutMuon.getPulserMuonBitsCnt(); ++i){
            bitMask |= 0x01 <<i;
        }
         
        for(int iBx = 0; iBx < 256; iBx++) { 
            int bitValue = rndGen.nextInt() & bitMask;
            for(int be =  0; be < 2; be++) {             
                for(int iMu = 0; iMu < 4; iMu++) {                    
                    BxDataXml bxData = event.getOrAddBxData(iBx);   
                    MuonImplXml muon = new HardwareMuon.FinalSortOutMuon(bitValue, iMu);                     
                    FinalSortDataXml finalSortDataXml = bxData.getOrAddFinalSortData(be); 
                    finalSortDataXml.getMuons().add(muon);
                }
            }
        }
        
    }
    
    public void writeFile(String fileName) throws JAXBException, IOException, InterruptedException {
        final File xmlFile = new File(fileName);
        JAXBContext context = JAXBContext.newInstance(DataStreamXml.class);
        System.out.println("writing to the file" + xmlFile);
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);                                                                                                     
        FileOutputStream  fileOutputStream = new FileOutputStream(xmlFile);
        m.marshal(dataStream, fileOutputStream);
        
        fileOutputStream.close();
        //Thread.sleep(5000);
        
    }
    public static void main(String[] args) {
        //final File xmlFile = new File("/nfshome0/rpcdev/bin/data/otpLinkRndTestPulses.xml");
        //final File xmlFile = new File("/nfshome0/rpcdev/bin/data/optLinksConnRnd.xml");
        String fileName = System.getenv("HOME") + "/bin/data/testPulses.xml";
        //final File xmlFile = new File("./out/gentestpulses.xml");

        JAXBContext context1;
        try {
            TestPulsesGenerator generator = new TestPulsesGenerator();
            //generator.generateLBData();
            //generator.generateLinkData();            
            //generator.generateLinkDataRnd();
            int[] tcNum = {11};
            //generator.generateTcSortOut(tcNum);
            
            HibernateContext context = new SimpleHibernateContextImpl();
            EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
            //generator.generateLinkDataDetecorLike(95, 30, equipmentDAO);
            //generator.generateLinkData2(tcNum);
            generator.generateHalfSortOut();
            generator.writeFile(fileName);
            
            
            //-------------------------------------------------------

            /*            System.out.println("reading the " + xmlFile);
            Unmarshaller um = context1.createUnmarshaller();
            DataStreamXml data1 = (DataStreamXml) um.unmarshal(xmlFile);

            System.out.println("data1.getEvents().size() " + data1.getEvents().size());
            for(EventXml eventXml : data1.getEvents()) {
            	System.out.println(" eventXml.getBxData().size() "  + eventXml.getBxData().size());
            	for(BxDataXml bxDataXml : eventXml.getBxData()) {
            		System.out.println("bxDataXml.getHalfSortData().size() " + bxDataXml.getHalfSortData().size());
            	}
            }*/

//          Marshaller m1 = context1.createMarshaller();
//          m1.marshal(data1, System.out);generateHalfSortOut


        } catch (JAXBException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
/*        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();*/
        }
        System.out.println("end");
    }
}



