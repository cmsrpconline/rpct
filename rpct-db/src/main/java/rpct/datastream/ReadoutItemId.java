package rpct.datastream;

import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkConn;

public class ReadoutItemId implements Comparable {
    public enum Type {
        lb,
        optLink,
        pacOut,
        tbOut,
        tcOut,
        hsOut,
        fsOut		
    }

    public enum LbDataSource {
        none(-1),
        stripData(0),
        lmux(2),
        coder(3),
        slave0(4),
        slave1(5);

        private final int intVal;

        LbDataSource(int intVal) {
            this.intVal = intVal;
        }

        public int getIntVal() {
            return intVal;
        }

        public static LbDataSource fromInt(int intVal) {
            for(LbDataSource dataSource : LbDataSource.values()) {
                if (dataSource.getIntVal() == intVal)
                    return dataSource;
            }
            throw new IllegalArgumentException("intVal " + intVal + "has now corresponiding LbDataSource");
        }

    }

    private Type type;
    private int lbId = -1;
    private LbDataSource lbDataSource; //0 - strip data, 1 - extended data
    private int tcNum = -1;
    private int tbNum = -1;
    private int optLinkNum = -1;
    private int pacNum = -1;
    private int hsNum = -1;
    private int be = -1;
    private Chip chip;
    private LinkConn linkConn;

    /*    public static ReadoutItemId createLbId(int lbId, LbDataSource lbDataSource) {        
        ReadoutItemId id = new ReadoutItemId();
        id.type = Type.lb;
        id.lbId = lbId;
        id.lbDataSource = lbDataSource;
        return id;
    }*/

    public static ReadoutItemId createLbId(Chip chip, LbDataSource lbDataSource) {        
        ReadoutItemId id = new ReadoutItemId();
        id.type = Type.lb;
        id.lbId = chip.getBoard().getId();
        id.lbDataSource = lbDataSource;
        id.chip = chip;
        return id;
    }

    //opt link input on TB
    public static ReadoutItemId createOptLinkId(int tcNum, int tbNum, int optLinkNum, Chip chip) {        
        ReadoutItemId id = new ReadoutItemId();
        id.type = Type.optLink;
        id.tcNum = tcNum;
        id.tbNum = tbNum;
        id.optLinkNum = optLinkNum;
        id.chip = chip;
        return id;
    }

    public static ReadoutItemId createOptLinkId(LinkConn linkConn) {        
        ReadoutItemId id = new ReadoutItemId();
        id.type = Type.optLink;
        id.tcNum = linkConn.getTriggerBoard().getLogSector();
        id.tbNum = linkConn.getTriggerBoard().getNum();
        id.optLinkNum = linkConn.getTriggerBoardInputNum();
        id.linkConn = linkConn;
        id.chip = linkConn.getTriggerBoard().getPac(0);
        return id;
    }

    public static ReadoutItemId createPacId(int tcNum, int tbNum, int pacNum) {        
        ReadoutItemId id = new ReadoutItemId();
        id.type = Type.pacOut;
        id.tcNum = tcNum;
        id.tbNum = tbNum;
        id.pacNum = pacNum;
        return id;
    }

    public static ReadoutItemId createTbSortId(int tcNum, int tbNum) {        
        ReadoutItemId id = new ReadoutItemId();
        id.type = Type.tbOut;
        id.tcNum = tcNum;
        id.tbNum = tbNum;
        return id;
    }

    public static ReadoutItemId createTcSortId(int tcNum) {        
        ReadoutItemId id = new ReadoutItemId();
        id.type = Type.tcOut;
        id.tcNum = tcNum;
        return id;
    }

    public static ReadoutItemId createHalfSortId(int hsNum, int be) {        
        ReadoutItemId id = new ReadoutItemId();
        id.type = Type.hsOut;
        id.hsNum = hsNum;
        id.be = be;
        return id;
    }

    public static ReadoutItemId createFinalSortId(int be) {        
        ReadoutItemId id = new ReadoutItemId();
        id.type = Type.fsOut;
        id.be = be;
        return id;
    }

    public int getHsNum() {
        return hsNum;
    }

    public int getLbId() {
        //return lbId;
        return chip.getBoard().getId();
    }

    public int getOptLinkNum() {
        return optLinkNum;
    }

    public int getPacNum() {
        return pacNum;
    }

    public int getTbNum() {
        return tbNum;
    }

    public int getTcNum() {
        return tcNum;
    }

    public Type getType() {
        return type;
    }

    public int getBe() {
        return be;
    }

    public int getHsKey() {
        return HalfSortDataXml.createKey(hsNum, be);
    }    

    public boolean equals(Object obj) {
        if(this.type.equals(((ReadoutItemId)obj).type) == false )
            return false;
        if(this.lbId != ((ReadoutItemId)obj).lbId)
            return false;
        if(this.lbDataSource != ((ReadoutItemId)obj).lbDataSource)
            return false;
        if(this.tcNum != ((ReadoutItemId)obj).tcNum)
            return false;
        if(this.tbNum != ((ReadoutItemId)obj).tbNum)
            return false;
        if(this.optLinkNum != ((ReadoutItemId)obj).optLinkNum)
            return false;
        if(this.pacNum != ((ReadoutItemId)obj).pacNum)
            return false;
        if(this.hsNum != ((ReadoutItemId)obj).hsNum)
            return false;
        if(this.be != ((ReadoutItemId)obj).be)
            return false;

        return true;
    }

    public int compareTo(Object obj) {
        final int BEFORE = -1;
        final int EQUAL = 0;
        final int AFTER = 1;

        if(this.type.equals(((ReadoutItemId)obj).type)) {
            if(this.lbId == ((ReadoutItemId)obj).lbId) {
                if(this.lbDataSource == ((ReadoutItemId)obj).lbDataSource) {
                    if(this.tcNum == ((ReadoutItemId)obj).tcNum) {
                        if(this.tbNum == ((ReadoutItemId)obj).tbNum) {
                            if(this.optLinkNum == ((ReadoutItemId)obj).optLinkNum) {
                                if(this.pacNum == ((ReadoutItemId)obj).pacNum) {
                                    if(this.hsNum == ((ReadoutItemId)obj).hsNum) {
                                        if(this.be > ((ReadoutItemId)obj).be) {
                                            return AFTER;
                                        }
                                        if(this.be < ((ReadoutItemId)obj).be) {
                                            return BEFORE;
                                        }
                                        return EQUAL;
                                    }
                                    else if(this.hsNum < ((ReadoutItemId)obj).hsNum)
                                        return BEFORE;    
                                    else 
                                        return AFTER;
                                }
                                else if(this.pacNum < ((ReadoutItemId)obj).pacNum)
                                    return BEFORE;    
                                else 
                                    return AFTER;
                            }
                            else if(this.optLinkNum < ((ReadoutItemId)obj).optLinkNum)
                                return BEFORE;    
                            else 
                                return AFTER;
                        }
                        else if(this.tbNum < ((ReadoutItemId)obj).tbNum)
                            return BEFORE;    
                        else 
                            return AFTER;
                    }
                    else if(this.tcNum < ((ReadoutItemId)obj).tcNum)
                        return BEFORE;    
                    else 
                        return AFTER;
                }
                else if(this.lbDataSource.intVal < ((ReadoutItemId)obj).lbDataSource.intVal)
                    return BEFORE;    
                else 
                    return AFTER;
            }
            else if(this.lbId < ((ReadoutItemId)obj).lbId)
                return BEFORE;    
            else 
                return AFTER;
        }
        else 
            return this.type.compareTo(((ReadoutItemId)obj).type);
    }


    public String toString() {  	
        if(type == Type.lb)
            return ((LinkBoard)chip.getBoard()).getName() + " " + ((LinkBoard)chip.getBoard()).getId() + " "+ lbDataSource;

        if(type == Type.optLink) {
            //return "optLink: tc " + tcNum + " tb " + tbNum + " ol " + optLinkNum;
        	if(linkConn != null)
        		return linkConn.toString();
        	else 
        		return "optLink: " + chip.getBoard().getName() + " (tc " + tcNum + " tb " + tbNum + ") optLinkInput " + optLinkNum;
        }

        if(type == Type.pacOut)
            return "pacOut: tc " + tcNum + " tb " + tbNum + " pacNum " + pacNum;

        if(type == Type.tbOut)
            return "tbSortOut: tc " + tcNum + " tb " + tbNum;

        if(type == Type.tcOut)
            return "tcSortOut: tc " + tcNum;

        if(type == Type.hsOut)
            return "hsSortOut: hs " + hsNum + " be " + be; 

        if(type == Type.fsOut)
            return "fsSortOut: " + be; 

        return "";

    }

    public LbDataSource getLbDataSource() {
        return lbDataSource;
    }

    public Chip getChip() {
        return chip;
    }

}

