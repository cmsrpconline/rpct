package rpct.datastream;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

public class AutoMap<E> extends LinkedList<E> {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private TreeMap<Integer, E> map;
    
    /**
     * the map elements can be used for read only access, they cannot be changed or removed, or added to the map!!!
     */
    public TreeMap<Integer, E> getMap() {
        return map;
    }

    public AutoMap(){
        super();
        map = new TreeMap<Integer, E>();
    }
    
    public boolean add(E o) {
        if(map.put(((AutoMapValueType)o).getKey(), o) == null)
             super.add(o);
        else { 
            //throw new IllegalArgumentException("the AutoMap already contains the object with this key: " + o);
            //System.out.println("the AutoMap already contains the object with this key: " + o);
        }
        return true;
    } 
    
    public void add(int index, E o) {
    	throw new RuntimeException("you cannot call this function");
/*    	if(map.put(((AutoMapValueType)o).getKey(), o) == null)
    		super.add(index, o);
    	else 
    		throw new IllegalArgumentException("the AutoMap already contains the object with this key: " + o); */      
    }

    public void addFirst(E o) {
       if(map.put(((AutoMapValueType)o).getKey(), o) == null)
            super.addFirst(o);
       else 
           throw new IllegalArgumentException("the AutoMap already contains the object with this key: " + o);       
    }
    
    public void addLast(E o) {
        if(map.put(((AutoMapValueType)o).getKey(), o) == null)
             super.addLast(o);
        else 
            throw new IllegalArgumentException("the AutoMap already contains the object with this key: " + o);       
    }
    
    public boolean addAll(Collection<?extends E> c) {
    	throw new RuntimeException("AutoMap: addAll unipplmented");
    }
    
    public boolean addAll(int index, Collection<?extends E> c) {
    	throw new RuntimeException("AutoMap: addAll unipplmented");
    }
    
    public E get(int key) {
        return map.get(key);
    }
    
    public E get(Integer key) {
        return map.get(key);
    }
    
/*    public E getFirst() {
        return map.firstEntry().getValue();
    }*/
    
    public boolean containsKey(Integer key) {
        return map.containsKey(key);
    }
    
    public void checkSize() {
    	if (this.size() != map.size()) {
    		throw new RuntimeException("AutoMap: this.size() != map.size())");
    	}
    }
}
