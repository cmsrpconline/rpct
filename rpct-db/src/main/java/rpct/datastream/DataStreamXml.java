package rpct.datastream;

import java.util.Set;
import java.util.TreeSet;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/*
 * @XmlType przy class-ie to typ danej, w zasadzie wazne to jest tylko w xsd
 * nie mylic z elemtem!
 * (@XmlType to nazwa typu, a @XmlElement to nazwa konkternego "obiektu"
 * tylko @XmlElement wystepuje w xml-u)
 * 
 * elementy (@XmlElement) sa generowane automatycznie na podstawie PUBLICZNYCH 
 *  pol i metod. (nazwa podana w @XmlType nie ma tu znaczenia) 
 *  W przypadku metod dotyczy to zestawu getter i seter 
 *  (musza byc oba koniecznie, inaczej sie krzaczy!!!)
 *  prywatne pola sa traktowane jako @XmlElement jesli sa explicite zaanotowane
 *  ale lepij jest annotwa publiczne metody, bo inaczej moze sie krzaczyc
 *  
 *  bez sensu jest zaanortowanie 
 *  prywatenego pola jako @XmlElement, a potem jego gettera jako @XmlTransient
 *   
 */
@XmlRootElement(name="rpctDataStream")
public class DataStreamXml {    
    //private Set<ReadoutItemId> addedReadoutItems = new TreeSet<ReadoutItemId>();
    
    private AutoMap<EventXml> events;
    
    public DataStreamXml() {
        events = new AutoMap<EventXml>();
    }
    
    @XmlElement(name="event")
    public AutoMap<EventXml> getEvents() {
        return events;
    }

    public void setEvents(AutoMap<EventXml> events) {
        this.events = events;
    }
    
    public EventXml getOrAddEvent(int number, int bxNum) {    
    	EventXml eventXml = events.get(number);
        if(eventXml == null) {
        	eventXml = new EventXml(number, bxNum);
            events.add(eventXml);
        }
        return eventXml;
    }
    
/*    public boolean itemWasAdded(ReadoutItemId id) {        
        return addedReadoutItems.contains(id);
    }
    
    void addActiveReadoutItem(ReadoutItemId id) {
        addedReadoutItems.add(id);
    }

    public Set<ReadoutItemId> getAddedReadoutItemIds() {
        return addedReadoutItems;
    }*/
 
    public void margeMuonData(DataStreamXml muonDataStream) {
        for(EventXml event : events) {
            EventXml muonEvent = muonDataStream.getEvents().get(event.getKey());
            if(muonEvent != null) {
                for(BxDataXml bxData : event.getBxData()) {
                	System.out.println("bxData.getKey() " + bxData.getKey());
                    BxDataXml muonBxData = muonEvent.getBxData().get(bxData.getKey());
                    if(muonBxData != null) {
                        for(TcDataXml tcData : bxData.getTcData()) {
                            TcDataXml muonTcData = muonBxData.getTcData().get(tcData.getKey());
                            if(muonTcData != null) {
                                for(TbDataXml tbData : tcData.getTbData()) {
                                    TbDataXml muonTbData = muonTcData.getTbData().get(tbData.getKey());
                                    if(muonTbData != null) {
                                        tbData.setPacs(muonTbData.getPacs());                                                                                
                                        tbData.setGbSortMuons(muonTbData.getGbSortMuons());          
                                    }
                                }                                
                                tcData.setGbSortMuons(muonTcData.getGbSortMuons());
                            }                            
                        }
                        bxData.setHalfSortData(muonBxData.getHalfSortData());
                        bxData.setFinalSortData(muonBxData.getFinalSortData());
                    }
                    else
                        throw new RuntimeException("no muonBxData for bxData");
                }
            }
            else {
                //throw new RuntimeException("now event ");
            }
        }      
    }  
    
    public void clear() {
        events.clear();
    }
}
