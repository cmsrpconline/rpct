package rpct.datastream;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="lb")
public class LbDataXml implements AutoMapValueType {
    private int id;

    private int pulserData;

    private LMuxBxDataImplXml.Set coderData; //coder or lmux data

    private LMuxBxDataImplXml.Set stripData;
    
    public LbDataXml() {
        coderData = new LMuxBxDataImplXml.Set();
        stripData = new LMuxBxDataImplXml.Set();
    }
    
    public LbDataXml(int id) {
        this();
        this.id = id;
    }    
    
    @XmlAttribute(name="id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @XmlAttribute(name="pulse")
    public String getPulserDataStr() {
        return Integer.toHexString(pulserData);
    }

    public void setPulserDataStr(String pulserData) {
        this.pulserData = Integer.parseInt(pulserData, 16);
    }

    @XmlTransient
    public int getPulserData() {
        return pulserData;
    }

    public void setPulserData(int pulserData) {
        this.pulserData = pulserData;
    }

    @XmlElement(name="lmd")
    //@XmlTransient  urde, nie wem czemu to bylo transient
    public LMuxBxDataImplXml.Set getData() {
        LMuxBxDataImplXml.Set data = new LMuxBxDataImplXml.Set();
        data.addAll(stripData);
        data.addAll(coderData);
        return data;
    }

    public void setData(LMuxBxDataImplXml.Set data) {
        for(LMuxBxDataImplXml d : data) {
            if(d.getIndex() == 0)
                stripData.add(d);
            else
                coderData.add(d);
        }
    }    
    
    @XmlTransient
    public LMuxBxDataImplXml.Set getCoderData() {
        return coderData;
    }

    public void setCoderData(LMuxBxDataImplXml.Set coderData) {
        this.coderData = coderData;
    }
    
    @XmlTransient
    //@XmlElement(name="lmd")
    public LMuxBxDataImplXml.Set getStripData() {
        return stripData;
    }

    public void setStripData(LMuxBxDataImplXml.Set stripData) {
        this.stripData = stripData;
    }
    
    public Integer getKey() {
        return id;
    }
}
;