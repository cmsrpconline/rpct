package rpct.datastream;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.DeviceItem;
import rpct.xdaqaccess.DeviceItemWord;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbChipImpl;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;

public class MonitorManager {
    private List<HardwareDbChip> chips;

    public MonitorManager() {
		super();
		this.chips = new ArrayList<HardwareDbChip>();
	}
    
    public MonitorManager(List<HardwareDbChip> chips) {
		super();
		this.chips = chips;
	}
    
    public MonitorManager(HardwareDbChip[] chips) {
		super();
		this.chips = new ArrayList<HardwareDbChip>();
		for(HardwareDbChip chip : chips)
			this.chips.add(chip);
	}
    
    public void addChip(HardwareDbChip chip) {
    	chips.add(chip);
    }
    
    public void addChips(List<HardwareDbChip> chips) {
    	this.chips.addAll(chips);
    }
    
    public void addChips(List<Chip> chips, HardwareDbMapper dbMapper) throws DataAccessException, RemoteException, ServiceException {                  
        for(Chip ch : chips) {               
            this.chips.add(dbMapper.getChip(ch));
        }
    }
    /*public void enableTest(boolean enable) throws RemoteException, ServiceException {
    	for(Device device : devices) {
    		if(enable) {
    			if(device.getType().equals("TB3OPTO")) {
    				DeviceItem reg;
    				reg = device.findByName("SEND_TEST_RND_ENA");
    				if(reg != null)
    					reg.write(7);

    				reg = device.findByName("SEND_CHECK_DATA_ENA");
    				if(reg != null)
    					reg.write(7);

    				reg = device.findByName("SEND_TEST_ENA");
    				if(reg != null)
    					reg.write(7);

    			}
    		}
    		else {
    			if(device.getType().equals("TB3OPTO")) {
    				DeviceItem reg;
    				reg = device.findByName("SEND_TEST_RND_ENA");
    				if(reg != null)
    					reg.write(0);

    				reg = device.findByName("SEND_TEST_ENA");
    				if(reg != null)
    					reg.write(0);
    			}
    		}
    	}
    }
*/
	public void enableTransmissionCheck(boolean enableBCNcheck, boolean enableDataCheck) throws RemoteException, ServiceException, HardwareDbException { 
		for(HardwareDbChip chip : chips) {		
		    if(chip.getDbChip().getType() == ChipType.OPTO) {
		        ((HardwareDbChipImpl)chip).enableSendTransCheck(enableBCNcheck, enableDataCheck);
		    }
		    else
		        ((HardwareDbChipImpl)chip).enableTransmissionCheck(enableBCNcheck, enableDataCheck);
		}
	}

	public void resetRecErrorCnt() throws RemoteException, ServiceException {
		for(HardwareDbChip chip : chips) {			
			((HardwareDbChipImpl)chip).resetRecErrorCnt();
		}
	} 

	public String readRecErrorCnt() throws RemoteException, ServiceException {
		StringBuilder strb = new StringBuilder();
		strb.append("REC_ERROR_COUNT");
		strb.append("\n");
		for(HardwareDbChip chip : chips) {
			DeviceItem reg;
			reg = chip.getHardwareChip().getItem("REC_ERROR_COUNT");
			if(reg != null) {
				strb.append(chip.getHardwareChip().getBoard().getName() + " " + chip.getHardwareChip().getName() + ": " + reg.read());
				strb.append("\n");
			}

			if(chip.getHardwareChip().getType().equals("TB3RMB")) {
				strb.append("   RMB_CHMB3_ERR_CNT     " + chip.getHardwareChip().getItem("RMB_CHMB3_ERR_CNT").read() + "\n");
				strb.append("   RMB_DDM30_ERR_CNT     " + chip.getHardwareChip().getItem("RMB_DDM30_ERR_CNT").read() + "\n");
				strb.append("   RMB_DDM31_ERR_CNT     " + chip.getHardwareChip().getItem("RMB_DDM31_ERR_CNT").read() + "\n");

				strb.append("      RMB_DEL_ERR_CNT    RMB_DDM_ERR_CNT \n");
				String format = "    %1$-2s    %2$-4s   %3$-4s\n";
				DeviceItemWord delErrCnt = ((DeviceItemWord)chip.getHardwareChip().getItem("RMB_DEL_ERR_CNT"));
				DeviceItemWord ddmErrCnt = ((DeviceItemWord)chip.getHardwareChip().getItem("RMB_DDM_ERR_CNT"));
				for(int iLinkNum = 0; iLinkNum < delErrCnt.getNumber(); iLinkNum++) {		
					strb.append(String.format(format, iLinkNum, delErrCnt.read(iLinkNum), ddmErrCnt.read(iLinkNum)) );
				}
			}
		}
		return strb.toString();   
	} 
	
	public String readRecErrorCntShorted() throws RemoteException, ServiceException {
		StringBuilder strb = new StringBuilder();
		strb.append("REC_ERROR_COUNT");
		strb.append("\n");
		for(HardwareDbChip chip : chips) {
			DeviceItem reg;
			reg = chip.getHardwareChip().getItem("REC_ERROR_COUNT");
			if(reg != null) {
				long value = reg.read();
				if(value != 0) {
					strb.append(chip.getHardwareChip().getBoard().getName() + " " + chip.getHardwareChip().getName() + ": " + value);
					strb.append("\n");
				}
			}

			if(chip.getHardwareChip().getType().equals("TB3RMB")) {
				long[] values = new long[3];
				values[0] = chip.getHardwareChip().getItem("RMB_CHMB3_ERR_CNT").read();
				values[1] = chip.getHardwareChip().getItem("RMB_DDM30_ERR_CNT").read();
				values[2] = chip.getHardwareChip().getItem("RMB_DDM31_ERR_CNT").read();
				if(values[0] != 0 || values[1] != 0 || values[2] != 0) {
					strb.append("   RMB_CHMB3_ERR_CNT     " + values[0] + "\n");
					strb.append("   RMB_DDM30_ERR_CNT     " + values[1] + "\n");
					strb.append("   RMB_DDM31_ERR_CNT     " + values[2] + "\n");

					strb.append("      RMB_DEL_ERR_CNT    RMB_DDM_ERR_CNT \n");
					String format = "    %1$-2s    %2$-4s   %3$-4s\n";
					DeviceItemWord delErrCnt = ((DeviceItemWord)chip.getHardwareChip().getItem("RMB_DEL_ERR_CNT"));
					DeviceItemWord ddmErrCnt = ((DeviceItemWord)chip.getHardwareChip().getItem("RMB_DDM_ERR_CNT"));
					for(int iLinkNum = 0; iLinkNum < chip.getHardwareChip().getItem("RMB_DEL_ERR_CNT").getNumber(); iLinkNum++) {		
						strb.append(String.format(format, iLinkNum, delErrCnt.read(iLinkNum), ddmErrCnt.read(iLinkNum)) );
					}
				}
			}
		}
		return strb.toString();   
	} 
}
