package rpct.datastream;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import rpct.datastream.LMuxBxDataImplXml.Set;

public class LinkCoderAlgoritms {
	public static final int maxDelay = 7;//was 16

	public static TreeMap<Integer, LMuxBxDataImplXml.Set> code(TreeMap<Integer, LMuxBxDataImplXml.Set> stripBxDataMap, int lmuxInDealy, int lbNum, int bxPerFrame, int latency) {
		TreeMap<Integer, LMuxBxDataImplXml.Set> codedeData = new TreeMap<Integer, LMuxBxDataImplXml.Set>();
	
		int bxToInsert = 0;    	
		for(Map.Entry<Integer, LMuxBxDataImplXml.Set> lmuxBxData : stripBxDataMap.entrySet()) {            
			int currBx = lmuxBxData.getKey();
			if(bxToInsert < currBx) {
				bxToInsert = currBx;
			}
			for(LMuxBxDataImplXml lmuxData : lmuxBxData.getValue() ) {
				LMuxBxDataImplXml newLmuxData = new LMuxBxDataImplXml(lmuxData);
	
				int dealy = bxToInsert - currBx;
				if(dealy >= maxDelay) {
					//codedeData.lastEntry().getValue().last().setEndOfData(1);
					codedeData.get(codedeData.lastKey()).last().setEndOfData(1);
					break;
				}
				newLmuxData.setPartitionDelay(dealy);
				newLmuxData.setLbNum(lbNum);
				newLmuxData.setIndex(2);
				LMuxBxDataImplXml.Set newLmuxBxData = new LMuxBxDataImplXml.Set();
				newLmuxBxData.add(newLmuxData);
	
				if(codedeData.put(bxToInsert + latency + lmuxInDealy, newLmuxBxData) != null) {
					throw new RuntimeException("some data were already putinto this position!!!");
				}
	
				bxToInsert += bxPerFrame;    				
	
			}    		
		}
		return codedeData;
	}

	public static void printCoder(TreeMap<Integer, LMuxBxDataImplXml.Set> stripBxDataMap, TreeMap<Integer, LMuxBxDataImplXml.Set> codedeData, 
			int stripsPerPartion, int partitionsCnt) {
		String empty = "                                                                                           ";
		for(int iBx = 0; iBx <= codedeData.lastKey(); iBx++) {   
			LMuxBxDataImplXml.Set lmuxBxData = codedeData.get(iBx);
			System.out.print(iBx);
			
			LMuxBxDataImplXml.Set stripData = stripBxDataMap.get(iBx);
			if(stripData != null) {
				System.out.print(" " + stripData.toStripString(stripsPerPartion, partitionsCnt));
			}
			else {
				System.out.print(" " + empty.substring(0, stripsPerPartion/4 * partitionsCnt));
			}
			
			if(lmuxBxData != null) {
				for(LMuxBxDataImplXml lmuxData : lmuxBxData ) {
					System.out.print(" " + lmuxData);
				}
			}
			System.out.print("\n");
		}
	}
	
	//ten algorytm dziala inaczej niz to co jest w firmwarze
	public static TreeMap<Integer, LMuxBxDataImplXml.Set> multiplex(ArrayList<TreeMap<Integer, LMuxBxDataImplXml.Set>> codedDataInLbs, int maxBx) {
		TreeMap<Integer, LMuxBxDataImplXml.Set> lmuxBxDataMap = new TreeMap<Integer, LMuxBxDataImplXml.Set>();    	
		
		int localBx[] = {-1, -1, -1};
		int iBx = 0;
		while(true) { 
			for(int iLb = 2; iLb >= 0; iLb--) {
				localBx[iLb]++;
				TreeMap<Integer, LMuxBxDataImplXml.Set> codedeData = codedDataInLbs.get(iLb);    			
				if(codedeData != null) {    				
					LMuxBxDataImplXml.Set lmuxBxData = codedeData.get(localBx[iLb]);    				
					if(lmuxBxData != null) {
						if(lmuxBxData.size() != 1) {
							throw new RuntimeException("lmuxBxData.size() != 1");
						}
		    			LMuxBxDataImplXml lmuxData = lmuxBxData.first(); 
		    			int newDelay = (iBx - (localBx[iLb] - lmuxData.getPartitionDelay()));
		    			if(newDelay < 8) {
		    				LMuxBxDataImplXml newLmuxData = new LMuxBxDataImplXml(lmuxData);
		    				newLmuxData.setPartitionDelay(newDelay);
	
		    				LMuxBxDataImplXml.Set newLmuxBxData = new LMuxBxDataImplXml.Set();
		    				newLmuxBxData.add(newLmuxData);    	    					
		    				lmuxBxDataMap.put(iBx, newLmuxBxData);
		    				if(newDelay == 7) {
		    					newLmuxData.setEndOfData(1);
		    					iBx++;
		    					if(iBx > maxBx)
		    		    			break;
		    					continue; //???????????????????????????????????????czy to potrzebne    	    					
		    				}
		    				break;
		    			}
		    		}
				}
			}
			iBx++;
			if(iBx > maxBx)
				break;
		}
		
		    
		System.out.print("multiplexed data\n");
		for(iBx = 0; iBx <= lmuxBxDataMap.lastKey(); iBx++) {   
			LMuxBxDataImplXml.Set lmuxBxData = lmuxBxDataMap.get(iBx);
			System.out.print(iBx);
			if(lmuxBxData != null) {
				for(LMuxBxDataImplXml lmuxData : lmuxBxData ) {
					System.out.print(" " + lmuxData);
				}
			}
			System.out.print("\n");
		}
		return lmuxBxDataMap;
	}

	public static ArrayList<TreeMap<Integer, LMuxBxDataImplXml.Set>> demultiplex(TreeMap<Integer, LMuxBxDataImplXml.Set> lmuxBxDataMap) {
	    ArrayList<TreeMap<Integer, LMuxBxDataImplXml.Set>> demuxedData = new ArrayList<TreeMap<Integer,LMuxBxDataImplXml.Set>>();
	    for(int i = 0; i < 3; i++) {
	    	demuxedData.add( new TreeMap<Integer, LMuxBxDataImplXml.Set>());
	    }
	    for(Map.Entry<Integer, LMuxBxDataImplXml.Set> lmuxBxData : lmuxBxDataMap.entrySet()) {            
	        for(LMuxBxDataImplXml lmuxData : lmuxBxData.getValue() ) {
	            LMuxBxDataImplXml.Set demuxBxData = demuxedData.get(lmuxData.getLbNum()).get(lmuxBxData.getKey());
	            if(demuxBxData == null) {
	                demuxBxData = new LMuxBxDataImplXml.Set();
	                demuxedData.get(lmuxData.getLbNum()).put(lmuxBxData.getKey(), demuxBxData);
	            }
	            LMuxBxDataImplXml newlmuxData = new  LMuxBxDataImplXml(lmuxData);
	            newlmuxData.setIndex(0);
	            //newlmuxData.setLbNum(0); //kurcze, czemu to dzialalo na elhepie????
	            demuxBxData.add(newlmuxData);
	        }
	    }
	    return demuxedData;
	}

}
