package rpct.datastream;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.SynCoderConf;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.equipment.SorterCrate;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.synch.PulserGeneratorForSynchronization;
import rpct.xdaq.XdaqApplicationInfo;
import rpct.xdaq.XdaqException;
import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.TtcAccess;
import rpct.xdaqaccess.axis.TtcAccessImplAxis;
import rpct.xdaqaccess.diag.DiagAccessDispatcher;
import rpct.xdaqaccess.diag.DiagCtrl;
import rpct.xdaqaccess.diag.DiagnosticReadout;
import rpct.xdaqaccess.diag.DiagnosticReadoutData;
import rpct.xdaqaccess.diag.Pulser;
import rpct.xdaqaccess.diag.TriggerDataSel;
import rpct.xdaqaccess.diag.DiagCtrl.TriggerType;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbSynCoder;
import rpct.xdaqaccess.synchro.DefOut;
import rpct.xdaqaccess.synchro.LVDSLinksSynchronizer.TestType;

public class TestsManager {	 
    private String xmlFileName;

    public void setXmlFileName(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public enum PulserTarget {
        chipOut(0),
        chipIn(1),
        lbGol(2);
        private final int intVal;

        private PulserTarget(int intVal) {
            this.intVal = intVal;
        }

        public int getIntVal() {
            return intVal;
        }
    }

    public enum LBExtReadoutData {
        off(0),
        csc(1),
        lmux(2),
        coder(3),
        slave0(4),
        slave1(5);

        private final int intVal;

        private LBExtReadoutData(int intVal) {
            this.intVal = intVal;
        }

        public int getIntVal() {
            return intVal;
        }
    }

    TestOptionsXml testOptions;
    
    private PulserTarget pulseTarget = PulserTarget.chipOut;

    private int pulseLenght;

    private boolean pulseReapet;

    private long pulserLimit;

    private long readoutLimit;

    private TriggerType pulserStartTriggerType = TriggerType.BCN0;
    private TriggerType readoutStartTriggerType = TriggerType.BCN0;         

    private TriggerDataSel triggerDataSel;

//  ---------------------------------------------------------------------------    
    private EquipmentDAO equipmentDAO;

    private ConfigurationManager configurationManager;

    HardwareDbMapper dbMapper;

    MonitorManager monitorManager; 

//  --------------------------------------------------------------------------
    private ChipType pulsersChipType;

    private List<Pulser> pulsers = new ArrayList<Pulser>();

    private TreeMap<ChipType, List<Pulser> > pulsersByType = new TreeMap<ChipType, List<Pulser> >();

    private List<DiagnosticReadout> readouts = new ArrayList<DiagnosticReadout>();

    private TreeMap<ChipType, List<DiagnosticReadout> > diagReadsByType = new TreeMap<ChipType, List<DiagnosticReadout> >();

    private List<DiagCtrl> diagCtrls = new ArrayList<DiagCtrl>(); //all diag controls

    private List<DiagCtrl> pulserDiagCtrls =  new ArrayList<DiagCtrl>(); 

    private List<DiagCtrl> readoutDiagCtrls = new ArrayList<DiagCtrl>(); 

    HashMap<Integer, Integer> lmuxInDelay = new HashMap<Integer, Integer>();

    public static class DualMap<K1, K2, V> { // extends TreeMap<K1,V>
        private TreeMap<K1, TreeMap<K2, V> > map = new TreeMap<K1, TreeMap<K2, V> >();

        DualMap() {    		
        }

        public V put(K1 k1, K2 k2, V v) {
            TreeMap<K2, V> inerMap = map.get(k1);
            if(inerMap == null) {
                inerMap = new TreeMap<K2, V>();
                map.put(k1, inerMap);
            }
            return inerMap.put(k2, v);
        }

        public V get(K1 k1, K2 k2) {
            TreeMap<K2, V> inerMap = map.get(k1);
            if(inerMap != null) {
                return inerMap.get(k2);
            }
            return null;
        }
    }

    private String lbConfigKey = ConfigurationManager.CURRENT_LBS_CONF_KEY;
    
    private DualMap<ChipType, PulserTarget, Integer> pulserStartTrigDelay = new DualMap<ChipType, PulserTarget, Integer>();

    private TreeMap<ChipType, Integer>  readoutStartTrigDelay = new TreeMap<ChipType, Integer>();

    private TreeMap<ChipType, Integer>  dataTriggerDelay = new TreeMap<ChipType, Integer>();

    private TreeMap<ChipType, Integer>  dataReadoutDelay = new TreeMap<ChipType, Integer>();

    private int pulserStartOffset;    

    private void intitDelays() {
        int dataDelayOffset;
        int readoutStartDelay;        
        int l1aOffset;

        pulserStartOffset = 32 + RpctDelays.TO_FSORT_LATENCY + (RpctDelays.TTC_LB_LATENCY - RpctDelays.TTC_FS_LATENCY);
        if(readoutStartTriggerType == TriggerType.L1A) {
            dataDelayOffset = RpctDelays.DATA_DELAY_OFFSET + 1;// depends on the L1 latency!!!!!!!          
            readoutStartDelay = 1;
            l1aOffset = 1;

            readoutStartTrigDelay.put(ChipType.SYNCODER,    readoutStartDelay);
            readoutStartTrigDelay.put(ChipType.OPTO,        readoutStartDelay);
            readoutStartTrigDelay.put(ChipType.PAC,         readoutStartDelay);
            readoutStartTrigDelay.put(ChipType.RMB,         readoutStartDelay);
            readoutStartTrigDelay.put(ChipType.GBSORT,      readoutStartDelay);
            readoutStartTrigDelay.put(ChipType.TCSORT,      readoutStartDelay);
            readoutStartTrigDelay.put(ChipType.HALFSORTER,  readoutStartDelay - l1aOffset);
            readoutStartTrigDelay.put(ChipType.FINALSORTER, readoutStartDelay);
            readoutStartTrigDelay.put(ChipType.TTUOPTO,        readoutStartDelay);
            
            dataTriggerDelay.put(ChipType.SYNCODER,    0);
            dataTriggerDelay.put(ChipType.OPTO,        0);
            dataTriggerDelay.put(ChipType.PAC,         0);
            dataTriggerDelay.put(ChipType.RMB,         0);
            dataTriggerDelay.put(ChipType.GBSORT,      0);
            dataTriggerDelay.put(ChipType.TCSORT,      0);
            dataTriggerDelay.put(ChipType.HALFSORTER,  0);
            dataTriggerDelay.put(ChipType.FINALSORTER, 0);
            dataTriggerDelay.put(ChipType.TTUOPTO,        0);
        }
        else if(triggerDataSel == TriggerDataSel.L1A) {
            dataDelayOffset = RpctDelays.DATA_DELAY_OFFSET ;// depends on the L1 latency!!!!!!!         
            readoutStartDelay = 0;
            l1aOffset = 1;

            readoutStartTrigDelay.put(ChipType.SYNCODER,    readoutStartDelay);
            readoutStartTrigDelay.put(ChipType.OPTO,        readoutStartDelay);
            readoutStartTrigDelay.put(ChipType.PAC,         readoutStartDelay);
            readoutStartTrigDelay.put(ChipType.RMB,         readoutStartDelay);
            readoutStartTrigDelay.put(ChipType.GBSORT,      readoutStartDelay);
            readoutStartTrigDelay.put(ChipType.TCSORT,      readoutStartDelay);
            readoutStartTrigDelay.put(ChipType.HALFSORTER,  readoutStartDelay);
            readoutStartTrigDelay.put(ChipType.FINALSORTER, readoutStartDelay);
            readoutStartTrigDelay.put(ChipType.TTUOPTO,        readoutStartDelay);

            dataTriggerDelay.put(ChipType.SYNCODER,    1);
            dataTriggerDelay.put(ChipType.OPTO,        1);
            dataTriggerDelay.put(ChipType.PAC,         1);
            dataTriggerDelay.put(ChipType.RMB,         1 +5);
            dataTriggerDelay.put(ChipType.GBSORT,      1);
            dataTriggerDelay.put(ChipType.TCSORT,      1);
            dataTriggerDelay.put(ChipType.HALFSORTER,  0);
            dataTriggerDelay.put(ChipType.FINALSORTER, 0);
            dataTriggerDelay.put(ChipType.TTUOPTO,        1);
        }
        else {
            //dataDelayOffset = RpctDelays.TO_FSORT_LATENCY + (RpctDelays.TTC_LB_LATENCY - RpctDelays.TTC_FS_LATENCY) + 5;
            dataDelayOffset = RpctDelays.TO_FSORT_LATENCY + 10 + 2 -10; // + RpctDelays.TTC_FS_LATENCY 
            readoutStartDelay = dataDelayOffset + 5 + 7; // - 9
            l1aOffset = 0;

            pulserStartOffset = dataDelayOffset +3 +4; //-5

            readoutStartTrigDelay.put(ChipType.SYNCODER,    readoutStartDelay);
            readoutStartTrigDelay.put(ChipType.OPTO,        readoutStartDelay);
            readoutStartTrigDelay.put(ChipType.PAC,         readoutStartDelay);
            readoutStartTrigDelay.put(ChipType.RMB,         readoutStartDelay); //readoutStartDelay + 40 = wiecej niz 127 ;
            readoutStartTrigDelay.put(ChipType.GBSORT,      readoutStartDelay);
            readoutStartTrigDelay.put(ChipType.TCSORT,      readoutStartDelay);
            readoutStartTrigDelay.put(ChipType.HALFSORTER,  readoutStartDelay - l1aOffset);
            readoutStartTrigDelay.put(ChipType.FINALSORTER, readoutStartDelay);
            readoutStartTrigDelay.put(ChipType.TTUOPTO,        readoutStartDelay);
            
            dataTriggerDelay.put(ChipType.SYNCODER,    0);
            dataTriggerDelay.put(ChipType.OPTO,        0);
            dataTriggerDelay.put(ChipType.PAC,         0);
            dataTriggerDelay.put(ChipType.RMB,         0);
            dataTriggerDelay.put(ChipType.GBSORT,      0);
            dataTriggerDelay.put(ChipType.TCSORT,      0);
            dataTriggerDelay.put(ChipType.HALFSORTER,  0);
            dataTriggerDelay.put(ChipType.FINALSORTER, 0);
            dataTriggerDelay.put(ChipType.TTUOPTO,        0);            
        }

        dataReadoutDelay.put(ChipType.SYNCODER,    dataDelayOffset - RpctDelays.TO_LB_LATENCY + RpctDelays.TTC_LB_LATENCY);
        dataReadoutDelay.put(ChipType.OPTO,    dataDelayOffset - RpctDelays.TO_OPTO_LATENCY + RpctDelays.TTC_TB_LATENCY);       
        dataReadoutDelay.put(ChipType.PAC,    dataDelayOffset - RpctDelays.TO_PAC_LATENCY + RpctDelays.TTC_TB_LATENCY);
        //dataReadoutDelay.put(ChipType.RMB,    dataDelayOffset - RpctDelays.TO_PAC_LATENCY + RpctDelays.TTC_TB_LATENCY - RpctDelays.RMB_DATA_DELAY); //2 - offset of the RMB readout
        dataReadoutDelay.put(ChipType.RMB,    0); //2 - offset of the RMB readout
        dataReadoutDelay.put(ChipType.GBSORT,    dataDelayOffset - RpctDelays.TO_TBSORT_LATENCY + RpctDelays.TTC_TB_LATENCY);       
        dataReadoutDelay.put(ChipType.TCSORT,    dataDelayOffset - RpctDelays.TO_TCSORT_LATENCY + RpctDelays.TTC_TC_LATENCY);       
        dataReadoutDelay.put(ChipType.HALFSORTER,    dataDelayOffset - RpctDelays.TO_HSORT_LATENCY + RpctDelays.TTC_HS_LATENCY);        
        dataReadoutDelay.put(ChipType.FINALSORTER,    dataDelayOffset - RpctDelays.TO_FSORT_LATENCY + RpctDelays.TTC_FS_LATENCY -1);//1 to compensate the L1A timing difference

        dataReadoutDelay.put(ChipType.TTUOPTO,    dataDelayOffset - RpctDelays.TO_OPTO_LATENCY + RpctDelays.TTC_TB_LATENCY); 
        
        pulserStartTrigDelay.put(ChipType.SYNCODER,    PulserTarget.chipOut,pulserStartOffset - dataReadoutDelay.get(ChipType.SYNCODER) + RpctDelays.LB_LATENCY + RpctDelays.TTC_LB_LATENCY);
        pulserStartTrigDelay.put(ChipType.OPTO,        PulserTarget.chipOut,pulserStartOffset - dataReadoutDelay.get(ChipType.OPTO) + RpctDelays.OPTO_LATENCY);
        pulserStartTrigDelay.put(ChipType.TTUOPTO,     PulserTarget.chipOut,pulserStartOffset - dataReadoutDelay.get(ChipType.OPTO) + RpctDelays.OPTO_LATENCY);
        pulserStartTrigDelay.put(ChipType.PAC,         PulserTarget.chipOut,pulserStartOffset - dataReadoutDelay.get(ChipType.PAC) + RpctDelays.PAC_LATENCY);
        pulserStartTrigDelay.put(ChipType.GBSORT,      PulserTarget.chipIn, pulserStartOffset - dataReadoutDelay.get(ChipType.GBSORT));
        pulserStartTrigDelay.put(ChipType.GBSORT,      PulserTarget.chipOut,pulserStartOffset - dataReadoutDelay.get(ChipType.GBSORT) + RpctDelays.TBSORT_LATENCY);
        pulserStartTrigDelay.put(ChipType.TCSORT,      PulserTarget.chipIn, pulserStartOffset - dataReadoutDelay.get(ChipType.TCSORT));
        pulserStartTrigDelay.put(ChipType.TCSORT,      PulserTarget.chipOut,pulserStartOffset - dataReadoutDelay.get(ChipType.TCSORT) + RpctDelays.TCSORT_LATENCY);
        pulserStartTrigDelay.put(ChipType.HALFSORTER,  PulserTarget.chipIn, pulserStartOffset - dataReadoutDelay.get(ChipType.HALFSORTER));
        pulserStartTrigDelay.put(ChipType.HALFSORTER,  PulserTarget.chipOut,pulserStartOffset - dataReadoutDelay.get(ChipType.HALFSORTER) + RpctDelays.HSORT_LATENCY);
        pulserStartTrigDelay.put(ChipType.FINALSORTER, PulserTarget.chipIn, pulserStartOffset - dataReadoutDelay.get(ChipType.FINALSORTER));
        pulserStartTrigDelay.put(ChipType.FINALSORTER, PulserTarget.chipOut,pulserStartOffset - dataReadoutDelay.get(ChipType.FINALSORTER) + RpctDelays.FSORT_LATENCY);     


/*        for(Map.Entry<ChipType, Integer> e : dataReadoutDelay.entrySet()) {
            System.out.println("dataReadoutDelay " + e.getKey() + " : " + e.getValue());
        }

        System.out.println("readoutStartDelay " + readoutStartDelay);

        System.out.println("pulserStartTrigDelay \n " + pulserStartTrigDelay.toString());  

        System.out.println("ChipType.SYNCODER,    PulserTarget.chipOut " + pulserStartTrigDelay.get(ChipType.SYNCODER,    PulserTarget.chipOut));
        System.out.println("ChipType.OPTO,        PulserTarget.chipOut " + pulserStartTrigDelay.get(ChipType.OPTO,        PulserTarget.chipOut));
        System.out.println("ChipType.PAC,         PulserTarget.chipOut " + pulserStartTrigDelay.get(ChipType.PAC,         PulserTarget.chipOut));      
        System.out.println("ChipType.GBSORT,      PulserTarget.chipIn " + pulserStartTrigDelay.get(ChipType.GBSORT,      PulserTarget.chipIn));       
        System.out.println("ChipType.GBSORT,      PulserTarget.chipOut " + pulserStartTrigDelay.get(ChipType.GBSORT,      PulserTarget.chipOut));        
        System.out.println("ChipType.TCSORT,      PulserTarget.chipIn " + pulserStartTrigDelay.get(ChipType.TCSORT,      PulserTarget.chipIn));       
        System.out.println("ChipType.TCSORT,      PulserTarget.chipOut " + pulserStartTrigDelay.get(ChipType.TCSORT,      PulserTarget.chipOut));        
        System.out.println("ChipType.HALFSORTER,  PulserTarget.chipIn " + pulserStartTrigDelay.get(ChipType.HALFSORTER,  PulserTarget.chipIn));        
        System.out.println("ChipType.HALFSORTER,  PulserTarget.chipOut " + pulserStartTrigDelay.get(ChipType.HALFSORTER,  PulserTarget.chipOut));       
        System.out.println("ChipType.FINALSORTER, PulserTarget.chipIn " + pulserStartTrigDelay.get(ChipType.FINALSORTER, PulserTarget.chipIn));        
        System.out.println("ChipType.FINALSORTER, PulserTarget.chipOut " + pulserStartTrigDelay.get(ChipType.FINALSORTER, PulserTarget.chipOut));
        
        */
    }

/*    public TestsManager(String xmlFileName,
            int pulseLenght, boolean pulseReapet, long pulserLimit,
            long readoutLimit, TriggerType pulserStartTriggerType,
            TriggerType readoutStartTriggerType, 
            TriggerDataSel triggerDataSel,
            EquipmentDAO equipmentDAO) {
        super();
        this.xmlFileName = xmlFileName;
        this.pulseLenght = pulseLenght;
        this.pulseReapet = pulseReapet;
        this.pulserLimit = pulserLimit;
        this.readoutLimit = readoutLimit;
        this.pulserStartTriggerType = pulserStartTriggerType;
        this.readoutStartTriggerType = readoutStartTriggerType;
        this.triggerDataSel = triggerDataSel;
        this.equipmentDAO = equipmentDAO;                	

        intitDelays();
    }*/

    public TestsManager(String xmlFileName, TestPresets.TestPreset preset, TestOptionsXml testOptions, EquipmentDAO equipmentDAO, 
    		ConfigurationManager configurationManager, HardwareDbMapper dbMapper, String lbConfigKey) {
        super();
        this.xmlFileName = xmlFileName;
        this.pulseLenght = preset.getPulseLenght();
        this.pulseReapet = preset.getPulseReapet();
        this.pulserLimit = preset.getPulserLimit();
        this.readoutLimit = preset.getReadoutLimit();
        this.pulserStartTriggerType = preset.getPulserStartTriggerType();
        this.readoutStartTriggerType = preset.getReadoutStartTriggerType();
        this.triggerDataSel = preset.getTriggerDataSel();
        this.equipmentDAO = equipmentDAO;                	
        this.configurationManager = configurationManager;
        this.dbMapper = dbMapper;
        this.testOptions = testOptions;
        this.lbConfigKey = lbConfigKey;
        intitDelays();
    }
    public TestsManager(String xmlFileName, TestPresets.TestPreset preset, TestOptionsXml testOptions, EquipmentDAO equipmentDAO, 
    		ConfigurationManager configurationManager, HardwareDbMapper dbMapper) {
    	this (xmlFileName, preset, testOptions, equipmentDAO, 
    		configurationManager, dbMapper, "");
    }

    	
    public TestsManager(TestPresets.TestPreset preset, TestOptionsXml testOptions, EquipmentDAO equipmentDAO, 
    		ConfigurationManager configurationManager, HardwareDbMapper dbMapper, String lbConfigKey) {
        
    	this (null, preset, testOptions, equipmentDAO, 
        		configurationManager, dbMapper, "");
    }

    /*    public void addPulsers(ChipType pulsersChipType, List<Pulser> pulsers, PulserTarget pulseTarget) {
        this.pulsers = pulsers;
        this.pulsersChipType = pulsersChipType;

        this.pulserDiagCtrls = DiagAccessDispatcher.getDiagCtrls(pulsers);
        this.diagCtrls.addAll(pulserDiagCtrls);

        this.pulseTarget = pulseTarget;
    }*/

    public void setPulseTarget(PulserTarget pulseTarget) {
        this.pulseTarget = pulseTarget;
    }

    public void addPulser(Chip chip) throws DataAccessException, RemoteException, ServiceException {    
        List<Pulser> pulsersBuf = pulsersByType.get(chip.getType());
        if(pulsersBuf == null) {
            pulsersBuf =  new ArrayList<Pulser>();
            pulsersByType.put(chip.getType(), pulsersBuf);
        }
        Pulser pulser = (Pulser) dbMapper.getDiag(chip, "PULSER");
        pulsersBuf.add(pulser);
        pulsers.add(pulser);
        diagCtrls.add(pulser.getDiagCtrl());
        pulserDiagCtrls.add(pulser.getDiagCtrl());
    }

    public void addPulsers(List<Chip> chips) throws DataAccessException, RemoteException, ServiceException {    
        for (Chip chip : chips) {
            addPulser(chip);
            //DefOut.out.println("Added pulses: " + chip.getBoard().getName() + " " + chip);
        }         
    }

    public void addPulsersHdb(List<HardwareDbChip> chips) throws DataAccessException, RemoteException, ServiceException {    
        for (HardwareDbChip chip : chips) {
            addPulser(chip.getDbChip());
        }        
    }

    public void addReadout(Chip chip) throws DataAccessException, RemoteException, ServiceException {
        List<DiagnosticReadout> readaoutsBuf = diagReadsByType.get(chip.getType());        
        if(readaoutsBuf == null) {
            readaoutsBuf = new ArrayList<DiagnosticReadout>();
            diagReadsByType.put(chip.getType(), readaoutsBuf);
        }
        DiagnosticReadout readout = (DiagnosticReadout) dbMapper.getDiag(chip, "READOUT");
        readaoutsBuf.add(readout);
        readouts.add(readout);

        readoutDiagCtrls.add(readout.getDiagCtrl());
        diagCtrls.add(readout.getDiagCtrl());		
    }	

    public void addReadouts(List<Chip> chips) throws DataAccessException, RemoteException, ServiceException {
        //System.out.println("chips selected for readouts ");
        for (Chip chip : chips) {
            addReadout(chip);
            //System.out.println(chip.getBoard().getName() + " " + chip.getType() + " " + chip.getPosition());
        }
    }

    public void addReadouts(ChipType readoutsChipType, List<DiagnosticReadout> readouts) {
        if(diagReadsByType.get(readoutsChipType) == null)
            diagReadsByType.put(readoutsChipType, readouts);
        else
            diagReadsByType.get(readoutsChipType).addAll(readouts);

        List<DiagCtrl> diagCtrlsBuf = DiagAccessDispatcher.getDiagCtrls(readouts);        
        readoutDiagCtrls.addAll(diagCtrlsBuf);
        diagCtrls.addAll(diagCtrlsBuf); 

        this.readouts.addAll(readouts);     
    }

    public void setReadeouts() throws DataAccessException, RemoteException, ServiceException {      
        addReadouts(testOptions.getChips("readouts", equipmentDAO));
    }

    public void setPulsers() throws DataAccessException, RemoteException, ServiceException {      
        addPulsers(testOptions.getChips("pulsers", equipmentDAO));
    }

    //TODO - poprawic ten komentarz
    /* it is not possible to allign at the same moment data from strips 
     * (or coder - the latency between the strip data and coder data is fixed) 
     * with the data from mux-er (), because on the 
     * input of the lmux there are delays, different for every LB.
     * therefore the strip (coder) data are aligned here, and the delays applied 
     * on the input of the LMUX must be corrected in the event builder 
     * */
    public void prepareReadouts() throws RemoteException, ServiceException, DataAccessException, XdaqException {            
        System.out.println("Reseting readout diag ctrls");
        DiagAccessDispatcher.reset(readoutDiagCtrls);

        System.out.println("Configuring readouts");        
        Set<Integer>[] mask0 = DiagAccessDispatcher.createMask0(15);        
        System.out.println("using for LB settings " + lbConfigKey);
        for(Map.Entry<ChipType, List<DiagnosticReadout> > diagReadES : diagReadsByType.entrySet()) { 
            if(diagReadES.getKey() == ChipType.SYNCODER) {
                for(DiagnosticReadout readout : diagReadES.getValue()) {
                    Chip curentChip = (Chip) (equipmentDAO.getObject(Chip.class, readout.getDiagId().getOwnerId()));                    
                    SynCoderConf configuration = (SynCoderConf) configurationManager.getConfiguration(curentChip , configurationManager.getLocalConfigKey(lbConfigKey));
                    boolean isMaster = ((LinkBoard)curentChip.getBoard()).isMaster();

                    //int readoutStartTriggerDelay = 0;
                    int readoutStartTriggerDelay = readoutStartTrigDelay.get(ChipType.SYNCODER) + configuration.getBcn0Delay(); //- if the EC0 is delayed the same as BC0

                    int lbExtDataSel = LBExtReadoutData.coder.intVal;
                    if(isMaster)
                        lbExtDataSel = LBExtReadoutData.lmux.intVal;

                    //to align the strip data on all boards
                    int dataReadoutDelay = this.dataReadoutDelay.get(ChipType.SYNCODER) + configuration.getLmuxInDelay();

                    if(isMaster) {
                        dataReadoutDelay -= RpctDelays.LB_SLAVE_MASTER_LATENCY; //because getLmuxInDelay() contains LB_SLAVE_MASTER_LATENCY
                        lmuxInDelay.put(curentChip.getId(), configuration.getLmuxInDelay());
                    }

                    readout.configure(mask0, dataReadoutDelay, 0, lbExtDataSel);                   
                    readout.getDiagCtrl().configure(BigInteger.valueOf(readoutLimit), readoutStartTriggerType, readoutStartTriggerDelay);
                    //configureDiagnosable(diagReadES.getValue(), triggerDataSel, dataTriggerDelay.get(diagReadES.getKey()));
                    HardwareDbChip hardwareDbChip = dbMapper.getChip(curentChip);
                    hardwareDbChip.getHardwareChip().getItem("STATUS1.TRG_DATA_SEL").write(triggerDataSel.getIntVal());
                    if(triggerDataSel == TriggerDataSel.L1A)
                        hardwareDbChip.getHardwareChip().getItem("DATA_TRG_DELAY").write(configuration.getBcn0Delay());
                    else
                        hardwareDbChip.getHardwareChip().getItem("DATA_TRG_DELAY").write(0);

                    hardwareDbChip.getHardwareChip().getItem("DAQ_MASK").write(0xff);
                }                
            }
            else {
                DiagAccessDispatcher.configure(diagReadES.getValue(), mask0, dataReadoutDelay.get(diagReadES.getKey()));
                DiagAccessDispatcher.configureDiagnosable(diagReadES.getValue(), triggerDataSel, dataTriggerDelay.get(diagReadES.getKey()));
                DiagAccessDispatcher.configure(DiagAccessDispatcher.getDiagCtrls(diagReadES.getValue()), BigInteger.valueOf(readoutLimit), 
                        readoutStartTriggerType, readoutStartTrigDelay.get(diagReadES.getKey()));
            }
        }
    }


    public void disableLbPuls() throws RemoteException, ServiceException, DataAccessException, XdaqException {       
        for(Map.Entry<ChipType, List<Pulser> > pulsersES : pulsersByType.entrySet()) { 
            if(pulsersES.getKey() == ChipType.SYNCODER) {                                                                       
                for(Pulser pulser : pulsersES.getValue()) {
                    Chip curentChip = (Chip) (equipmentDAO.getObject(Chip.class, pulser.getDiagId().getOwnerId()));
                    HardwareDbChip hardwareDbChip = dbMapper.getChip(curentChip);
                    hardwareDbChip.getHardwareChip().getItem("PULSER.LOOP_SEL").write(0);
                }     
            }
        }
    }

    public void preparePulsers(String linkDataXmlFileName) throws RemoteException, ServiceException, DataAccessException, XdaqException {
        System.out.println("Configuring pulsers");
        DiagAccessDispatcher.configure(pulsers, "file://" + linkDataXmlFileName, BigInteger.valueOf(pulseLenght), pulseReapet, pulseTarget.getIntVal());

        System.out.println("Reseting pulser diag ctrls");
        DiagAccessDispatcher.reset(pulserDiagCtrls);
        System.out.println("using for LB settings " + lbConfigKey);
        for(Map.Entry<ChipType, List<Pulser> > pulsersES : pulsersByType.entrySet()) { 
            if(pulsersES.getKey() == ChipType.SYNCODER) {                                                                       
                for(Pulser pulser : pulsersES.getValue()) {
                    Chip curentChip = (Chip) (equipmentDAO.getObject(Chip.class, pulser.getDiagId().getOwnerId()));
                    SynCoderConf configuration = (SynCoderConf) configurationManager.getConfiguration(curentChip , configurationManager.getLocalConfigKey(lbConfigKey));

                    boolean isMaster = ((LinkBoard)curentChip.getBoard()).isMaster();
                    //int dataReadoutDelay = this.dataReadoutDelay.get(ChipType.SYNCODER) + configuration.getLmuxInDelay();

                    //int pulserStartTrigDelay = pulserStartOffset - dataReadoutDelay + RpctDelays.TTC_LB_LATENCY + configuration.getBcn0Delay() + 2;
                    int pulserStartTrigDelayLoc = pulserStartTrigDelay.get(ChipType.SYNCODER, PulserTarget.chipOut) + configuration.getBcn0Delay() - configuration.getLmuxInDelay();
                    if(isMaster) 
                        pulserStartTrigDelayLoc += RpctDelays.LB_SLAVE_MASTER_LATENCY; 

                    if(pulseTarget == PulserTarget.lbGol) {
                        if(isMaster) {                        
                            pulserStartTrigDelayLoc = pulserStartTrigDelayLoc + configuration.getLmuxInDelay() + RpctDelays.LB_LATENCY;
                        }
                        else  {//the slave - master test
                            pulserStartTrigDelayLoc = pulserStartTrigDelayLoc + RpctDelays.LB_CODER_LATENCY; 
                        }
                    }

//                    System.out.println(curentChip.getBoard().getName() + " pulserStartTrigDelay " + pulserStartTrigDelayLoc + " Bcn0Delay "
//                            + configuration.getBcn0Delay() + " LmuxInDelay " + configuration.getLmuxInDelay() +" winC " + configuration.getWindowClose() + " incClk " + configuration.isInvertClock());

                    pulser.getDiagCtrl().configure(BigInteger.valueOf(pulserLimit), pulserStartTriggerType, pulserStartTrigDelayLoc);
                }     
            }
            else {
                System.out.println("Configuring pulser diag ctrls");               
                DiagAccessDispatcher.configure(DiagAccessDispatcher.getDiagCtrls(pulsersES.getValue()), BigInteger.valueOf(pulserLimit), 
                        pulserStartTriggerType, pulserStartTrigDelay.get(pulsersES.getKey(), pulseTarget));
            }
        }
    }

    public void preparePulsersForSynchro(Set<Integer> sLines) throws RemoteException, ServiceException, DataAccessException, XdaqException {
        System.out.println("Configuring pulsers");
        System.out.println("Reseting pulser diag ctrls");
        DiagAccessDispatcher.reset(pulserDiagCtrls);

        for(Map.Entry<ChipType, List<Pulser> > pulsersES : pulsersByType.entrySet()) { 
            System.out.println("Configuring pulser diag ctrls");  
            Binary[] data = PulserGeneratorForSynchronization.getGenerators().get(pulsersES.getKey()).generatePulses(sLines, pulseLenght);
            DiagAccessDispatcher.configure(pulsersES.getValue(), data, BigInteger.valueOf(pulseLenght), pulseReapet, pulseTarget.getIntVal());
            DiagAccessDispatcher.configure(DiagAccessDispatcher.getDiagCtrls(pulsersES.getValue()), BigInteger.valueOf(pulserLimit), 
                    pulserStartTriggerType, pulserStartTrigDelay.get(pulsersES.getKey(), pulseTarget));

        }
    }

    public void preparePulsersForSynchro(int sLine) throws RemoteException, ServiceException, DataAccessException, XdaqException {
        System.out.println("Configuring pulsers");
        System.out.println("Reseting pulser diag ctrls");
        DiagAccessDispatcher.reset(pulserDiagCtrls);

        for(Map.Entry<ChipType, List<Pulser> > pulsersES : pulsersByType.entrySet()) { 
            System.out.println("Configuring pulser diag ctrls");  
            Binary[] data = PulserGeneratorForSynchronization.getGenerators().get(pulsersES.getKey()).generatePulses(sLine, pulseLenght);
            DiagAccessDispatcher.configure(pulsersES.getValue(), data, BigInteger.valueOf(pulseLenght), pulseReapet, pulseTarget.getIntVal());
            DiagAccessDispatcher.configure(DiagAccessDispatcher.getDiagCtrls(pulsersES.getValue()), BigInteger.valueOf(pulserLimit), 
                    pulserStartTriggerType, pulserStartTrigDelay.get(pulsersES.getKey(), pulseTarget));

        }
    }

    //random for all lines altogether
    public void preparePulsersForSynchro() throws RemoteException, ServiceException, DataAccessException, XdaqException {
        System.out.println("Configuring pulsers");
        System.out.println("Reseting pulser diag ctrls");
        DiagAccessDispatcher.reset(pulserDiagCtrls);

        for(Map.Entry<ChipType, List<Pulser> > pulsersES : pulsersByType.entrySet()) { 
            System.out.println("Configuring pulser diag ctrls");  
            Binary[] data = PulserGeneratorForSynchronization.getGenerators().get(pulsersES.getKey()).generatePulses(pulseLenght);
            DiagAccessDispatcher.configure(pulsersES.getValue(), data, BigInteger.valueOf(pulseLenght), pulseReapet, pulseTarget.getIntVal());
            DiagAccessDispatcher.configure(DiagAccessDispatcher.getDiagCtrls(pulsersES.getValue()), BigInteger.valueOf(pulserLimit), 
                    pulserStartTriggerType, pulserStartTrigDelay.get(pulsersES.getKey(), pulseTarget));

        }
    }

    public void preparePulsers() throws RemoteException, ServiceException, DataAccessException, XdaqException {
        preparePulsers(xmlFileName);
    }


    public void startPulsers() throws RemoteException, ServiceException {
        DiagAccessDispatcher.start(pulserDiagCtrls);
    }

    public void stopPulsers() throws RemoteException, ServiceException {
        DiagAccessDispatcher.stop(pulserDiagCtrls);
    }

    public void singleRedaouts() throws DataAccessException,
    ServiceException, IOException, InterruptedException, XdaqException, JAXBException {
        prepareReadouts();

        String y_n = "y";
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("take readouts? (y/n)");

        y_n = br.readLine();   
        while(y_n.equals("y")) {
            System.out.println("Starting readouts");
            DiagAccessDispatcher.start(readoutDiagCtrls);
            sendVmeBGoTest();

            while (!DiagAccessDispatcher.checkCountingEnded(readoutDiagCtrls)) {
                System.out.println("Checking if all have finished");
                Thread.sleep(1000);
            }
            DiagAccessDispatcher.stop(readoutDiagCtrls);
            System.out.println("All readout diagnostics have finished");

            System.out.println("diagAccess.readData");
            DiagnosticReadoutData[] datas = DiagAccessDispatcher.readData(readouts);

            EventBuilder eventBuilder = new EventBuilder(equipmentDAO, testOptions);
            eventBuilder.setLmuxInDelay(lmuxInDelay);
            DefOut defLog= new DefOut(System.out);

            eventBuilder.build(datas);
            //System.out.println("eventBuilder.build(datas); commnetged!!!!!!!!!!!!!!");

            eventBuilder.writeToXmlFile(System.getenv("HOME") + "/bin/data/lastEventData.xml");
            System.out.println("diagAccess.readData Done");

            System.out.println("take readouts? (y/n)");
            y_n = br.readLine();   
        }
    }

    public void multipleRedaouts(int readoutCnt, String outFileName, boolean ttcFromCTC, HardwareDbChip ttcCheckChip) throws DataAccessException,
    ServiceException, IOException, InterruptedException, XdaqException, JAXBException {
        prepareReadouts();

        EventBuilder eventBuilder = new EventBuilder(equipmentDAO, testOptions);
        eventBuilder.setLmuxInDelay(lmuxInDelay);

        FileOutputStream file = new FileOutputStream(outFileName);
        PrintStream prntSrm = new PrintStream(file);

        prntSrm.println("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
        prntSrm.println("<rpctDataStream>");       
        prntSrm.flush();


        JAXBContext context = JAXBContext.newInstance(DataStreamXml.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        HardwareDbChip chipForEvnCheck = dbMapper.getChip((Chip) (equipmentDAO.getObject(Chip.class, readouts.get(0).getDiagId().getOwnerId())));

        for(int i = 0; i < readoutCnt; i++) {            
            if(ttcFromCTC) {
                long pretriggerNum = ttcCheckChip.getHardwareChip().getItem("TTC_EVN").read();
                System.out.println("waiting for pretrigger");
                while(pretriggerNum == ttcCheckChip.getHardwareChip().getItem("TTC_EVN").read()) {
                    Thread.sleep(100);
                }
                System.out.println("Starting readouts");
                DiagAccessDispatcher.start(readoutDiagCtrls);
                System.out.println("readouts started");
                if((pretriggerNum+1) != ttcCheckChip.getHardwareChip().getItem("TTC_EVN").read()) {
                    System.out.println("the event num on the ttcCheckChip chenged during readout start");
                    DiagAccessDispatcher.stop(readoutDiagCtrls);
                    continue;
                }

                long eventNum = chipForEvnCheck.getHardwareChip().getItem("TTC_EVN").read();
                while ((eventNum + EventBuilder.getEventsPerReadout()) >= chipForEvnCheck.getHardwareChip().getItem("TTC_EVN").read()) {
                    System.out.println("Checking if all have finished, eventNum " + eventNum);
                    Thread.sleep(200);  
                }
                /*                while (!DiagAccessDispatcher.checkCountingEnded(readoutDiagCtrls)) {
                    System.out.println("Checking if all have finished");
                    Thread.sleep(1000);  
                }*/
            }
            else {
                DiagAccessDispatcher.start(readoutDiagCtrls);
                sendVmeBGoTest();
                long pretriggerNum = ttcCheckChip.getHardwareChip().getItem("TTC_EVN").read();
                sendVmeBGoTest();
                while((pretriggerNum+1) != ttcCheckChip.getHardwareChip().getItem("TTC_EVN").read()) {
                    sendVmeBGoTest();
                    System.out.println("the sendVmeBGoTest() ececuted again!!!!!!!!!!!!!!!!!!");
                }
                System.out.println("readouts started");
                long eventNum = chipForEvnCheck.getHardwareChip().getItem("TTC_EVN").read();
                while ((eventNum + EventBuilder.getEventsPerReadout()) >= chipForEvnCheck.getHardwareChip().getItem("TTC_EVN").read()) {
                    System.out.println("Checking if all have finished, eventNum " + eventNum);
                    Thread.sleep(200);  
                }

                /*int waitRepeats = 0;
                while (!DiagAccessDispatcher.checkCountingEnded(readoutDiagCtrls)) {
                    System.out.println("Checking if all have finished");
                    Thread.sleep(1000); //1 s
                    if(waitRepeats > readoutTimeInSec + 2) {
                        waitRepeats = 0;
                        sendVmeBGoTest();
                        System.out.println("the sendVmeBGoTest() ececuted again!!!!!!!!!!!!!!!!!!");
                    }
                    waitRepeats++;
                }*/
            }
            DiagAccessDispatcher.stop(readoutDiagCtrls);
            System.out.println("readouts stoped");

            System.out.println("diagAccess.readData");
            DiagnosticReadoutData[] datas = DiagAccessDispatcher.readData(readouts);
            eventBuilder.build(datas);
            eventBuilder.appendToXmlFile(file, marshaller);
            System.out.println("diagAccess.readData Done, repetion " + i);
        }
        //eventBuilder.writeToXmlFile(outFileName);
        prntSrm.println("</rpctDataStream>");
        prntSrm.flush();
        file.close();
    }	

    public class MultipleRedaouts implements Runnable {
        int maxReadoutCnt; 
        String outFileName; 
        boolean ttcFromCTC; 
        HardwareDbChip ttcCheckChip;
        HardwareDbChip chipForEvnCheck;
        EventBuilder eventBuilder;
        FileOutputStream file;
        PrintStream prntSrm;
        Marshaller marshaller;

        public MultipleRedaouts(int maxReadoutCnt, String outFileName, boolean ttcFromCTC, HardwareDbChip ttcCheckChip) throws RemoteException, DataAccessException, ServiceException, XdaqException, FileNotFoundException, JAXBException {
            this.maxReadoutCnt = maxReadoutCnt;
            this.outFileName = outFileName;
            this.ttcFromCTC = ttcFromCTC;
            this.ttcCheckChip = ttcCheckChip;

            prepareReadouts();

            this.eventBuilder = new EventBuilder(equipmentDAO, testOptions);
            eventBuilder.setLmuxInDelay(lmuxInDelay);

            this.file = new FileOutputStream(outFileName);
            this.prntSrm = new PrintStream(file);

            prntSrm.println("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
            prntSrm.println("<rpctDataStream>");       
            prntSrm.flush();


            JAXBContext context = JAXBContext.newInstance(DataStreamXml.class);
            this.marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            this.chipForEvnCheck = dbMapper.getChip((Chip) (equipmentDAO.getObject(Chip.class, readouts.get(0).getDiagId().getOwnerId())));
        }

        public void run() {     
            try {
                int eventsCnt = 0;
                boolean wasInterupted = false;
                for(int i = 0; i < maxReadoutCnt; i++) {     
                    if(ttcFromCTC) {
                        long pretriggerNum = ttcCheckChip.getHardwareChip().getItem("TTC_EVN").read();
                        System.out.println("waiting for pretrigger");
                        while(pretriggerNum == ttcCheckChip.getHardwareChip().getItem("TTC_EVN").read()) {
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                wasInterupted = true;
                            }
                        }
                        System.out.println("Starting readouts");
                        DiagAccessDispatcher.start(readoutDiagCtrls);
                        System.out.println("readouts started");
                        if((pretriggerNum+1) != ttcCheckChip.getHardwareChip().getItem("TTC_EVN").read()) {
                            System.out.println("the event num on the ttcCheckChip chenged during readout start");
                            DiagAccessDispatcher.stop(readoutDiagCtrls);
                            continue;
                        }

                        long eventNum = chipForEvnCheck.getHardwareChip().getItem("TTC_EVN").read();
                        while (true) {
                            /*long currentEventNum = chipForEvnCheck.getHardwareChip().findByName("TTC_EVN").read();
                            if(currentEventNum >= (eventNum + EventBuilder.getEventsPerReadout()) )
                                break;
                            System.out.println("Checking if all have finished, eventNum " + currentEventNum);*/
                            if(DiagAccessDispatcher.checkCountingEnded(readoutDiagCtrls))
                                break;
                            try {
                                Thread.sleep(200);
                            } catch (InterruptedException e) {
                                wasInterupted = true;
                            }  
                        }
                        /*                while (!DiagAccessDispatcher.checkCountingEnded(readoutDiagCtrls)) {
                        System.out.println("Checking if all have finished");
                        Thread.sleep(1000);  
                    }*/
                    }
                    else {
                        DiagAccessDispatcher.start(readoutDiagCtrls);
                        long pretriggerNum = ttcCheckChip.getHardwareChip().getItem("TTC_EVN").read();
                        sendVmeBGoTest();
                        while((pretriggerNum+1) != ttcCheckChip.getHardwareChip().getItem("TTC_EVN").read()) {
                            sendVmeBGoTest();
                            System.out.println("the sendVmeBGoTest() ececuted again!!!!!!!!!!!!!!!!!!");
                        }
                        System.out.println("readouts started");
                        long eventNum = chipForEvnCheck.getHardwareChip().getItem("TTC_EVN").read();
                        while (true) {
                            long currentEventNum = chipForEvnCheck.getHardwareChip().getItem("TTC_EVN").read();
                            if(currentEventNum >= (eventNum + EventBuilder.getEventsPerReadout()) )
                                break;
                            if(DiagAccessDispatcher.checkCountingEnded(readoutDiagCtrls))
                                break;
                            System.out.println("Checking if all have finished, eventNum " + currentEventNum);
                            try {
                                Thread.sleep(200);
                            } catch (InterruptedException e) {
                                wasInterupted = true;
                            }  
                        }

                        /*int waitRepeats = 0;
                    while (!DiagAccessDispatcher.checkCountingEnded(readoutDiagCtrls)) {
                        System.out.println("Checking if all have finished");
                        Thread.sleep(1000); //1 s
                        if(waitRepeats > readoutTimeInSec + 2) {
                            waitRepeats = 0;
                            sendVmeBGoTest();
                            System.out.println("the sendVmeBGoTest() executed again!!!!!!!!!!!!!!!!!!");
                        }
                        waitRepeats++;
                    }*/
                    }
                    DiagAccessDispatcher.stop(readoutDiagCtrls);
                    System.out.println("readouts stoped");

                    System.out.println("diagAccess.readData");
                    DiagnosticReadoutData[] datas = DiagAccessDispatcher.readData(readouts);
                    eventBuilder.build(datas);
                    eventBuilder.appendToXmlFile(file, marshaller);
                    if(datas.length > 0) {
                        if(datas[0].getEvents().length > EventBuilder.getEventsPerReadout() )
                            eventsCnt += EventBuilder.getEventsPerReadout();
                        else
                            eventsCnt += datas[0].getEvents().length;
                    }
                    System.out.println("diagAccess.readData Done, repetion " + i + " count of events taken  " + eventsCnt);

                    if(Thread.interrupted() || wasInterupted) {
                        System.out.println("data taking inerupted by user");
                        break;
                    }
                }
            } catch (RemoteException e) {                
                e.printStackTrace();
                System.out.println("due to en error data taking inerupted");
            } catch (ServiceException e) {
                e.printStackTrace();
                System.out.println("due to en error data taking inerupted");
            } catch (MalformedURLException e) {
                e.printStackTrace();
                System.out.println("due to en error data taking inerupted");
            } catch (XdaqException e) {
                e.printStackTrace();
                System.out.println("due to en error data taking inerupted");
            } catch (DataAccessException e) {
                e.printStackTrace();
                System.out.println("due to en error data taking inerupted");
            } catch (JAXBException e) {
                e.printStackTrace();
                System.out.println("due to en error data taking inerupted");
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("due to en error data taking inerupted");
            }

            try {
                prntSrm.println("</rpctDataStream>");
                prntSrm.flush();
                file.close();
                System.out.println("xml file closed, press ENTER to continue");
            } catch (IOException e) {                
                e.printStackTrace();
                System.out.println("due to an error xml file not closed");
            }            
        }
    }

    public void daqPulserTest(boolean comapareWithSource, boolean buildData) throws DataAccessException,
    ServiceException, IOException, InterruptedException, XdaqException, JAXBException {
        preparePulsers();
        prepareReadouts();

        String y_n = "y";
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("type a char to star a pulser");
        y_n = br.readLine(); 
        System.out.println("Starting pulsers");
        DiagAccessDispatcher.start(pulserDiagCtrls);
        //sendVmeBGoTest();

        System.out.println("take readouts? (y/n)");


        y_n = br.readLine();   
        while(y_n.equals("y")) {
            System.out.println("Starting readouts");
            DiagAccessDispatcher.start(readoutDiagCtrls);
            sendVmeBGoTest();

            while (!DiagAccessDispatcher.checkCountingEnded(readoutDiagCtrls)) {
                System.out.println("Checking if all have finished");
                Thread.sleep(1000);
            }
            DiagAccessDispatcher.stop(readoutDiagCtrls);
            System.out.println("All readout diagnostics have finished");

            System.out.println("diagAccess.readData");
            DiagnosticReadoutData[] datas = DiagAccessDispatcher.readData(readouts);

            EventBuilder eventBuilder = new EventBuilder(equipmentDAO, testOptions);
            eventBuilder.setLmuxInDelay(lmuxInDelay);
            DefOut defLog= new DefOut(System.out);

            if(comapareWithSource)
                eventBuilder.loadSourceData(xmlFileName);
            if(buildData)
                eventBuilder.build(datas);

            eventBuilder.writeToXmlFile(System.getenv("HOME") + "/bin/data/lastEventData.xml");
            System.out.println("diagAccess.readData Done");

            System.out.println("take readouts? (y/n)");
            y_n = br.readLine();   
        }

        System.out.println("type a char to stop pulsers");
        y_n = br.readLine(); 
        DiagAccessDispatcher.stop(pulserDiagCtrls);

    }

    /**
     * @param comapareWithSource
     * @param analyseOptLinkConnections
     * @param analyseMasterSlaveConnectionsSlaveChanNum if 1 or 2 runs the the slave - master connections analysis
     * @param repetitions - if 0 - ask user if repeat 
     * @throws DataAccessException
     * @throws ServiceException
     * @throws IOException
     * @throws InterruptedException
     * @throws XdaqException
     * @throws JAXBException
     */
    public void interConnectionTest(boolean comapareWithSource, boolean analyseOptLinkConnections, int analyseMasterSlaveConnectionsSlaveChanNum, int repetitions) throws DataAccessException,
    ServiceException, IOException, InterruptedException, XdaqException, JAXBException {
        /*          
		FileInputStream fis = new FileInputStream(xmlFileName);

		int x = fis.available();
		byte b[] = new byte[x];
		fis.read(b);
		String muonsXml = new String(b);*/
        System.out.println("#--------------------------------------");
        System.out.println("#");
        System.out.println("#  Note: Running: interConnectionTest()");
        System.out.println("#");
        System.out.println("#--------------------------------------");

        String y_n = "y";
        int iteration = 0;
        while(y_n.equals("y")) {
            EventBuilder eventBuilder = new EventBuilder(equipmentDAO, testOptions);
            eventBuilder.setLmuxInDelay(lmuxInDelay);

            if(comapareWithSource)
                eventBuilder.loadSourceData(xmlFileName);

            preparePulsers();
            prepareReadouts();

            if(monitorManager != null)
                monitorManager.resetRecErrorCnt();

            System.out.println("Starting diag ctrls");
            DiagAccessDispatcher.start(diagCtrls);
            //sendVmeBGoTest();
            while (!DiagAccessDispatcher.checkCountingEnded(readoutDiagCtrls)) {//diagCtrls
                System.out.println("Checking if all have finished");
                Thread.sleep(1000);
            }

            //Thread.sleep(1000);
            //System.out.println("All diagnostics have finished");
            DiagAccessDispatcher.stop(diagCtrls);

            System.out.println("diagAccess.readData");
            DiagnosticReadoutData[] datas = DiagAccessDispatcher.readData(readouts);
            System.out.println("diagAccess.readData Done");
            
            DefOut defLog= new DefOut(System.out);

            /*            FileOutputStream file = new FileOutputStream("./out/sortersTest.txt", false);
            PrintStream prntSrm = new PrintStream(file);
            DefOut defLog= new DefOut(prntSrm);	*/		

            System.out.println("eventBuilder.build");
            eventBuilder.build(datas);
            System.out.println("eventBuilder.build Done");
            
            System.out.println("analysing data");
            if(analyseOptLinkConnections) {
                ArrayList<LinkConn> usedLinkConn = new ArrayList<LinkConn>();
                Set<LinkBoard> linkTransm = new HashSet<LinkBoard>();
                Set<Board> linkRec = new HashSet<Board>();
                List<Chip> chips = testOptions.getChips("readouts", equipmentDAO);                
                for(Chip chip : chips) {
                    if(chip.getType() == ChipType.SYNCODER)
                        linkTransm.add((LinkBoard)chip.getBoard());
                    if(chip.getType() == ChipType.PAC || chip.getType() == ChipType.OPTO || chip.getType() == ChipType.RMB) 
                        linkRec.add(chip.getBoard());
                }

                for(Board rec : linkRec) {
                    if(rec.getDisabled() == null) {
                        for (LinkConn linkConn : equipmentDAO.getLinkConns((TriggerBoard)rec, true)) {
                            if(linkTransm.contains(linkConn.getBoard())) {                                
                                usedLinkConn.add(linkConn); 
                            }
                        }
                    }
                }

                eventBuilder.analyseOptLinkConnections(usedLinkConn);
                
            }
            
            if(analyseMasterSlaveConnectionsSlaveChanNum > 0) {
                Set<LinkBoard> lbs = new HashSet<LinkBoard>();
                List<Chip> chips = testOptions.getChips("readouts", equipmentDAO);                
                for(Chip chip : chips) {
                    if(chip.getType() == ChipType.SYNCODER)
                        lbs.add((LinkBoard)chip.getBoard());
                }
                
                eventBuilder.analyseSlaveMasterConnections(lbs, analyseMasterSlaveConnectionsSlaveChanNum);
            }

            boolean analyseMuxer = false;
            if(analyseMuxer) {                
                eventBuilder.analyseLmux2();
            }

            
            eventBuilder.writeToXmlFile(System.getenv("HOME") + "/bin/out/eventData.xml");
            System.out.println("analysing data Done");

            eventBuilder.clear();
            
            if(monitorManager != null)
                System.out.print(monitorManager.readRecErrorCnt());
            
            iteration++;
            if(repetitions == 0) {
                System.out.println("Reapeat? (y/n)");
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                y_n = br.readLine();
            }
            else if (iteration >= repetitions){
                break;
            }               
        }
    } 

    public void algorithmsTest() throws DataAccessException,
    ServiceException, IOException, InterruptedException, XdaqException, JAXBException {
        /*          
		FileInputStream fis = new FileInputStream(xmlFileName);

		int x = fis.available();
		byte b[] = new byte[x];
		fis.read(b);
		String muonsXml = new String(b);*/

        System.out.println("#--------------------------------------");
        System.out.println("#");
        System.out.println("#  Note: Running: algorithmsTest()");
        System.out.println("#");
        System.out.println("#--------------------------------------");
        //String y_n = "y";
        //AK while(y_n.equals("y")) {
        for(int iRun=0;iRun<1;iRun++){
            // /testpulsesFMT96.xml
            // testBxDataFMT96.xml
            String dirName = "/cmssrv0/nfshome0/rpcdev/bin/data/2007.07.19.TestPac.2pl/"; 
            //String linkDataXmlFileName = dirName + "testBxDataFMT" + Integer.toString(iRun*12) + ".xml";
            //String muonDataXmlFileName = dirName + "testpulsesFMT" + Integer.toString(iRun*12) + ".xml";
            String linkDataXmlFileName = dirName + "testBxDataFMT" + Integer.toString(264) + ".xml";
            String muonDataXmlFileName = dirName + "testpulsesFMT" + Integer.toString(264) + ".xml";

            preparePulsers(linkDataXmlFileName);
            prepareReadouts();

            System.out.println("Starting diag ctrls");
            DiagAccessDispatcher.start(diagCtrls);
            sendVmeBGoTest();
            while (!DiagAccessDispatcher.checkCountingEnded(diagCtrls)) {
                System.out.println("Checking if all have finished");
                Thread.sleep(1000);
            }

            Thread.sleep(1000);
            //System.out.println("All diagnostics have finished");
            DiagAccessDispatcher.stop(diagCtrls);

            System.out.println("diagAccess.readData");
            DiagnosticReadoutData[] datas = DiagAccessDispatcher.readData(readouts);

            EventBuilder eventBuilder = new EventBuilder(equipmentDAO, testOptions);
            eventBuilder.setLmuxInDelay(lmuxInDelay);
            DefOut defLog= new DefOut(System.out);

            //eventBuilder.loadSourceData(linkDataXmlFileName);
            eventBuilder.loadSourceData(linkDataXmlFileName, muonDataXmlFileName);

            eventBuilder.build(datas);

            String outputXMLFileName =  dirName + "eventData_" + Integer.toString(iRun*12) + ".xml";
            eventBuilder.writeToXmlFile(outputXMLFileName);
            System.out.println("diagAccess.readData Done");

            //AK System.out.println("Reapeat? (y/n)");
            //AK BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            //AK y_n = br.readLine();  
        }
    } 
    public static void sendVmeBGoTest() throws MalformedURLException,
    XdaqException {
    	
    	try {
    		//Process process = Runtime.getRuntime().exec("/home/tb/bin/ttccommand.exe -p");
    		Process process = Runtime.getRuntime().exec("/home/tb/bin/ttccommand.exe -p");

    		String line;
    		BufferedReader brStdOut = new BufferedReader (new InputStreamReader (process.getInputStream()));
    		while ((line = brStdOut.readLine ()) != null) {
    			System.out.println ("[Stdout] " + line);
    		}
    		brStdOut.close();
    		
    		BufferedReader brStdErr = new BufferedReader (new InputStreamReader (process.getErrorStream()));
    		while ((line = brStdErr.readLine ()) != null) {
    			System.err.println ("[Stderr] " + line);
    		}
    		brStdErr.close();
    		return;
    	} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
            TtcAccess ttcAccess = new TtcAccessImplAxis(new XdaqApplicationInfo(
                    //new URL("http://ttc-rpc:1972"), "TTCciControl", 0), null);
            		new URL("http://pccmsrpct2:1974"), "TTCciControl", 0), null);            		
    
            ttcAccess.sendVmeBGo();
            System.out.println("sendVmeBGo Done.");
        }
        catch (Exception exception) {
            System.err.println("error while sendVmeBGoTest\n" + exception.getMessage() + "\n");
        }
    }

/*    public static void main(String[] args) throws ServiceException,
    DataAccessException, IOException, InterruptedException,
    XdaqException, HardwareDbException {
        long bxInSec = 40000000;

        //String xmlFileName = "/nfshome0/rpcdev/bin/data/testBxDataTC9HPT.xml";
        String xmlFileName = "/nfshome0/rpcdev/bin/data/gentestpulses.xml";
        //String xmlFileName = "/nfshome0/rpcdev/bin/data/testBxData.xml";

        // note: overriden in algorithmsTests()
        //String xmlFileName = "/nfshome0/rpcdev/bin/data/dummyBxData.xml";

        PulserTarget pulseTarget = PulserTarget.chipOut; //TODO choose the pulser output target

        int pulseLenght = 128;

        boolean pulseReapet = false;

        //long pulserLimit = bxInSec * 10000;
        long pulserLimit = 128;

        long readoutLimit = 128;
        //long readoutLimit = 28;

        TriggerType pulserStartTriggerType = TriggerType.PRETRIGGER_0;
        TriggerType readoutStartTriggerType = TriggerType.PRETRIGGER_0;  

        TriggerDataSel triggerDataSel = TriggerDataSel.LOCAL;

        int dataTrgDelay = 0;

        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);

        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);        
        ConfigurationManger configurationManger = new ConfigurationManger(context, equipmentDAO, configurationDAO);

        HardwareDbMapper dbMapper = new HardwareDbMapper(
                new HardwareRegistry(), context);

        TestsManager pulsesManager = new TestsManager(xmlFileName,
                pulseLenght, pulseReapet, pulserLimit,
                readoutLimit, pulserStartTriggerType, readoutStartTriggerType,
                triggerDataSel, equipmentDAO);

        pulsesManager.setPulseTarget(pulseTarget);
        try { //TODO choose the pulser and readouts source 
            TriggerCrate tc = (TriggerCrate)pulsesManager.equipmentDAO.getCrateByName("TC_9");
            TriggerBoard tbp2_9 = (TriggerBoard)equipmentDAO.getBoardByName("TBp2_9");					

            //SorterCrate sc = (SorterCrate)pulsesManager.equipmentDAO.getCrateByName("SC");

            //List<Chip> optos = equipmentDAO.getChipsByTypeAndCrate(ChipType.OPTO, tc);
            List<Chip> optos = equipmentDAO.getChipsByTypeAndBoard(ChipType.OPTO, tbp2_9, true);
            pulsesManager.addPulsers(optos);

            //List<Chip> pacs = equipmentDAO.getChipsByTypeAndCrate(ChipType.PAC, tc); //TODO
            //List<Chip> pacs = equipmentDAO.getChipsByTypeAndBoard(ChipType.PAC, tbp2_9); //TODO
            List<Chip> pacs = new ArrayList<Chip>();
            pacs.add(tbp2_9.getPac(0) );
            pulsesManager.addReadouts(pacs.get(0).getType(), dbMapper.getReadouts(pacs));

            //List<Chip> gbSorts = pulsesManager.equipmentDAO.getChipsByTypeAndCrate(ChipType.GBSORT, tc);
            //pulsesManager.addPulsers(ChipType.GBSORT, dbMapper.getPulsers(gbSorts));
            //pulsesManager.addReadouts(gbSorts.get(0).getType(), dbMapper.getReadouts(gbSorts));

            //List<Chip> tcSorts = pulsesManager.equipmentDAO.getChipsByTypeAndCrate(ChipType.TCSORT, tc);			
            //pulsesManager.addPulsers(ChipType.TCSORT, dbMapper.getPulsers(tcSorts));
            //pulsesManager.addReadouts(ChipType.TCSORT, dbMapper.getReadouts(tcSorts));

            //List<Chip> halfSorts = pulsesManager.equipmentDAO.getChipsByTypeAndCrate(ChipType.HALFSORTER, sc);
            //pulsesManager.addPulsers(ChipType.HALFSORTER, dbMapper.getPulsers(halfSorts));
            //pulsesManager.addReadouts(ChipType.HALFSORTER, dbMapper.getReadouts(halfSorts));

            //List<Chip> finalSort = pulsesManager.equipmentDAO.getChipsByTypeAndBoard(ChipType.FINALSORTER, pulsesManager.equipmentDAO.getBoardByName("FSB"));
            //pulsesManager.addPulsers(ChipType.FINALSORTER, dbMapper.getPulsers(finalSort));
            //pulsesManager.addReadouts(ChipType.FINALSORTER, dbMapper.getReadouts(finalSort));			

            ArrayList<Device> monitorableDevices = new ArrayList<Device>();
            //for(Board board : tc.getBoards()) 
            {
                Board board  = tbp2_9;;
                for(Device device : dbMapper.getBoard(board).getHardwareBoard().getDevices()) 
                    monitorableDevices.add(device);
            }


            MonitorManager monitorManager = new MonitorManager();
            pulsesManager.setMonitorManager(monitorManager);
            //monitorManager.enableTransmissionCheck(true, true); //TODO turn on or off the data checking

            //pulsesManager.interConnectionTest();
            //pulsesManager.daqTest(false);
            pulsesManager.algorithmsTest(); //TODO choose the test type

            System.out.print(monitorManager.readRecErrorCnt());
            //monitorManager.enableTest(true);
        } catch (JAXBException e) {
            e.printStackTrace();
        } finally {
            context.closeSession();
        }
    }*/

    public void setMonitorManager(MonitorManager monitorManager) {
        this.monitorManager = monitorManager;
    }
}
