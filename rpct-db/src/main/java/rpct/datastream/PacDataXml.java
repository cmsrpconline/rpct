package rpct.datastream;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;

public class PacDataXml extends MuonsSet implements AutoMapValueType {
    @XmlAttribute(name="num")
    private int number; //number in TC, 0...8
    
    
    public PacDataXml() {
        super();
    }

    public PacDataXml(int number) {
        super();
        this.number = number;
    }
    
    public Integer getKey() {
        return number;
    }
    
    @XmlTransient
    public int getNumber() {
        return number;
    }
    
/*    public boolean equals(Object obj) {
    	if(this.number != ((PacDataXml)obj).getNumber())
    		return false;
    	return this.getMuons().equals(((PacDataXml)obj).getMuons());
    }*/
    
    public String toString() {
    	String str;
    	str = "          pac " + number + " muons: \n";
    	for(MuonImplXml muon : getMuons())
    		str += muon.toString() + "\n";
    	
    	return str;		
	}
}

