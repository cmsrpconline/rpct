package rpct.datastream;

import javax.xml.bind.annotation.XmlAttribute;

public class FinalSortDataXml extends MuonsSet implements AutoMapValueType {       
    private int be; //barrel = 0; endcapa = 1
    
    public FinalSortDataXml(int be) {
        super();
        this.be = be;
    }

    public FinalSortDataXml() {
    }
    
    @XmlAttribute(name="be")
    public int getBe() {
        return be;
    }

    public void setBe(int be) {
        this.be = be;
    }    

    public Integer getKey() {
        return be;
    }

}

