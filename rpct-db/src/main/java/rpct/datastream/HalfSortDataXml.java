package rpct.datastream;

import javax.xml.bind.annotation.XmlAttribute;

public class HalfSortDataXml extends MuonsSet implements AutoMapValueType {   
    private int number;

    private int be; //barrel = 0; endcapa = 1
    
    public HalfSortDataXml(int number, int be) {
        super();
        this.number = number;
        this.be = be;
    }

    public HalfSortDataXml() {
    }

    public Integer getKey() {
        return createKey(number, be);
    }
    
    public static Integer createKey(int num, int be) {
        return num*10 + be;
    }
    
    @XmlAttribute(name="num")
    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
    
    @XmlAttribute(name="be")
    public int getBe() {
        return be;
    }

    public void setBe(int be) {
        this.be = be;
    }
    
/*    @XmlElement(name="mu")
    public List<MuonImplXml> getGbSortMuons() {
        return gbSortMuons;
    }*/    

}
