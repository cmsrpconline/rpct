package rpct.datastream;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.xdaqaccess.diag.LMuxBxData;

/**
 * @author Karol Bunkowski
 *
 *  partitionData and raw are in the xml in the hex format
 */
@XmlType(name="lmd") 
public class LMuxBxDataImplXml implements LMuxBxData, Comparable<LMuxBxDataImplXml> {    
    private int index;

    private int lbNum;

    private int partitionNum;
       
    private int partitionDelay;
    
    private int partitionData;

    private int endOfData;

    private int halfPartition;
        
    private int raw;
    
    private String notValid;
    
    public LMuxBxDataImplXml() {
    }
    
    public LMuxBxDataImplXml(int index, int lbNum, int partitionNum, int partitionDelay, int partitionData, int endOfData, int halfPartition) {
        super();
        this.index = index;
        this.lbNum = lbNum;
        this.partitionNum = partitionNum;
        this.partitionDelay = partitionDelay;
        this.partitionData = partitionData;
        this.endOfData = endOfData;
        this.halfPartition = halfPartition;
    }

    public LMuxBxDataImplXml(LMuxBxData lMuxBxData) {
        super();
        this.index = lMuxBxData.getIndex();
        this.lbNum = lMuxBxData.getLbNum();
        this.partitionNum = lMuxBxData.getPartitionNum();
        this.partitionDelay = lMuxBxData.getPartitionDelay();
        this.partitionData = lMuxBxData.getPartitionData();
        this.endOfData = lMuxBxData.getEndOfData();
        this.halfPartition = lMuxBxData.getHalfPartition();
    }
    
    @XmlAttribute(name="eod")
    public int getEndOfData() {
        return endOfData;
    }
    
    public void setEndOfData(int endOfData) {
        this.endOfData = endOfData;
    }

    @XmlTransient
    public int getHalfPartition() {
        return halfPartition;
    }
    
    public void setHalfPartition(int halfPartition) {
        this.halfPartition = halfPartition;
    }

    @XmlAttribute(name="ind")
    //@XmlTransient
    public int getIndex() {
        return index;
    }
    
    public void setIndex(int index) {
        this.index = index;
    }

    @XmlAttribute(name="lb")
    public int getLbNum() {
        return lbNum;
    }
    
    public void setLbNum(int lbNum) {
        this.lbNum = lbNum;
    }
   
    @XmlAttribute(name="dat")
    public String getPartitionDataStr() {
        return Integer.toHexString(partitionData);
    }
    
    public void setPartitionDataStr(String partitionData) {
        this.partitionData = Integer.parseInt(partitionData, 16);
    }

    @XmlTransient
    public int getPartitionData() {
        return partitionData;
    }
    
    public void setPartitionData(int partitionData) {
        this.partitionData = partitionData;
    }
    
    @XmlAttribute(name="del")
    public int getPartitionDelay() {
        return partitionDelay;
    }
    
    public void setPartitionDelay(int partitionDelay) {
        this.partitionDelay = partitionDelay;
    }
    
    @XmlAttribute(name="par")
    public int getPartitionNum() {
        return partitionNum;
    }
    
    public void setPartitionNum(int partitionNum) {
        this.partitionNum = partitionNum;
    }   
    
    @XmlAttribute(name="raw")
    public String getRawStr() {
        return Integer.toHexString(raw);
    }

    public void setRawStr(String raw) {
        this.raw = Integer.parseInt(raw, 16);
    }
    
    @XmlTransient
    public int getRaw() {
        return raw;
    }

    public void setRaw(int raw) {
        this.raw = raw;
    }
    
    @XmlAttribute(name="nv")
	public String getNotValid() {
		return notValid;
	}

	public void setNotValid(String notValid) {
		this.notValid = notValid;
	}	
    
	@XmlTransient
    public void setNotValidChip(Chip notValidChip) {
        this.notValid = notValidChip.getType() + " " + notValidChip.getPosition()+ " " + this.notValid;
    }
	
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        if(     //this.getIndex() == ((LMuxBxData)obj).getIndex() &&
        		this.getPartitionDelay() == ((LMuxBxData)obj).getPartitionDelay() &&
        		this.getLbNum() == ((LMuxBxData)obj).getLbNum() &&
        		this.getPartitionNum() == ((LMuxBxData)obj).getPartitionNum() &&
        		this.getPartitionData() == ((LMuxBxData)obj).getPartitionData() 
        		//this.getEndOfData() == ((LMuxBxData)obj).getEndOfData() 
        		)
        	return true;
        
        return false;
    	
    }
    
    public int compareTo(LMuxBxDataImplXml o) {   
        //if(this.getIndex() == ((LMuxBxData)o).getIndex()) 
        {
            if(this.getPartitionDelay() == ((LMuxBxData)o).getPartitionDelay() ) {
                if(this.getLbNum() == ((LMuxBxData)o).getLbNum() ) {
                	if( this.getPartitionNum() == ((LMuxBxData)o).getPartitionNum() ) {
                        if( this.getPartitionData() < ((LMuxBxData)o).getPartitionData())
                            return -1;
                        else if( this.getPartitionData() > ((LMuxBxData)o).getPartitionData())
                            return 1;
                        return 0;
                	}
                	else if( this.getPartitionNum() > ((LMuxBxData)o).getPartitionNum() )
                        return -1;
                    else if ( this.getPartitionNum() < ((LMuxBxData)o).getPartitionNum() )
                        return 1;
                    return 0;
                }
                else {
                    if( this.getLbNum() < ((LMuxBxData)o).getLbNum())
                        return -1;
                    else if( this.getLbNum() > ((LMuxBxData)o).getLbNum())
                        return 1;
                    return 0;
                }
            }
            else {
                if( this.getPartitionDelay() < ((LMuxBxData)o).getPartitionDelay() )
                    return -1;  
                else if( this.getPartitionDelay() > ((LMuxBxData)o).getPartitionDelay() )
                    return 1;
                return 0;
            }
        }
/*        else {
            if( this.getIndex() < ((LMuxBxData)o).getIndex() )
                return -1;  
            else if( this.getIndex() > ((LMuxBxData)o).getIndex() )
                return 1;
            return 0;           
        }*/
    }

    public String toString() {
    	String str;
    	
    	str = "dat " + String.format("%1$2s", Integer.toHexString(partitionData)) + " del " + partitionDelay + " eod " + endOfData + " hp " + halfPartition + 
    	" lb " + lbNum + " par " + partitionNum + " ind " + index;
        
        return str;
    }
    
    public static class Set extends TreeSet<LMuxBxDataImplXml> {
        /**
		 * 
		 */
		private static final long serialVersionUID = -4044374112472052768L;

		public String toString() {
        	String str = "";
        	for(LMuxBxDataImplXml data : this)
        		str += data.toString() + "\n";        	
        	return str;		
    	}
		
		public String toStripString(int stripsPerPartion, int partitionsCnt) {
        	String str = "";
        	int charsPerPartiton = stripsPerPartion/4;
        	ArrayList<String> stripStrings =  new ArrayList<String>();
        	String zeros = new String("000000000".toCharArray(), 0, charsPerPartiton);
        	for(int i = 0; i < partitionsCnt; i++)
        		stripStrings.add(zeros);
        	
        	for(LMuxBxDataImplXml data : this) {        		
        		stripStrings.set(data.getPartitionNum(), String.format("%1$" + charsPerPartiton + "s", Integer.toHexString(data.getPartitionData())));
        	}
        	for(int i = 0; i < partitionsCnt; i++)
        		str = stripStrings.get(i) + str;
        	return str;		
    	}
		
/*		public boolean equals(LMuxBxDataImplXml.Set that) {
			if (that == this)
				return true;

			if (that.size() != size())
				return false;
			
			Iterator<LMuxBxDataImplXml> thisIt = this.iterator();
			Iterator<LMuxBxDataImplXml> thatIt = that.iterator();
			
			while (thisIt.hasNext()) {
			    if (thisIt.next().equals(thatIt.next()))
			    	return false;
			}
			return true;
		}*/
    }
}

