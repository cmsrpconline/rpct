package rpct.db;

/**
 * Created on 2005-03-01
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class DataIntegrityException extends RuntimeException {

    private static final long serialVersionUID = 7758318557420688425L;

    public DataIntegrityException() {
        super();
    }

    public DataIntegrityException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataIntegrityException(String message) {
        super(message);
    }

    public DataIntegrityException(Throwable cause) {
        super(cause);
    }

}
