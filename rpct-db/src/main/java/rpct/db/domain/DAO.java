package rpct.db.domain;

import java.io.Serializable;
import java.util.List;

import rpct.db.DataAccessException;



/**
 * Created on 2005-03-01
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public interface DAO {
    
    public void saveObject(Object o) throws DataAccessException;
    public Object getObject(Class clazz, Serializable id) throws DataAccessException;
    public List getObjects(Class clazz) throws DataAccessException;
    public void deleteObject(Object o) throws DataAccessException;
    public void flush() throws DataAccessException;

}
