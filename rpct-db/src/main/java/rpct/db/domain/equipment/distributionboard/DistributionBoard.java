package rpct.db.domain.equipment.distributionboard;

//import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * Created on 2006-02-20
 *
 * @author Michal Pietrusinski & William Whitaker
 * @version $Id$
 */

@Entity
@Table(name="DISTRIBUTIONBOARD")
@SequenceGenerator(name="seq", sequenceName="S_02_1_DISTRIBUTIONBOARD", allocationSize=1)
public class DistributionBoard {
    private int id;
    private String productId;
//    private Date date;
    @Id
    @Column(name="DISTRIBUTIONBOARDID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
/*    @Column(name="DATE")
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}*/
    
    @Column(name="PRODUCTID")
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}

}