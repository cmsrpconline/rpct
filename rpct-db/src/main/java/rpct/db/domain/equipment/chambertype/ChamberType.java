package rpct.db.domain.equipment.chambertype;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import rpct.db.domain.equipment.chamberloaction.ChamberLocation;
/**
 * Created on 2006-02-20
 *
 * @author Michal Pietrusinski & William Whitaker
 * @version $Id$
 */


@Entity
@Table(name="CHAMBERTYPE")
@SequenceGenerator(name="seq", sequenceName="S_14_1_CHAMBERTYPE", allocationSize=1)
public class ChamberType {
    private int id;
    private List<ChamberLocation> chamberLocations;
    private String typeName;
    private String servicePosition;
    private String chimney;
    private int etaPartitionCnt;
    private int febCnt;
    private int febChainCnt;
    private int febInChainCnt;
    private int stripCnt;
    private int stripInEtaPartitionCnt;
    private int stripInFebCnt;
    
	@Id
    @Column(name="CHAMBERTYPEID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    @OneToMany(mappedBy="chamberType")
    public List<ChamberLocation> getChamberLocations() {
    	return chamberLocations;
    }
	public void setChamberLocations(List<ChamberLocation> chamberLocations) {
		this.chamberLocations = chamberLocations;
	}
    @Column(name="CHIMNEY")
    public String getChimney() {
		return chimney;
	}
	public void setChimney(String chimney) {
		this.chimney = chimney;
	}
    @Column(name="ETAPARTITIONCNT")
	public int getEtaPartitionCnt() {
		return etaPartitionCnt;
	}
	public void setEtaPartitionCnt(int etaPartitionCnt) {
		this.etaPartitionCnt = etaPartitionCnt;
	}
    @Column(name="FEBCHAINCNT")
	public int getFebChainCnt() {
		return febChainCnt;
	}
	public void setFebChainCnt(int febChainCnt) {
		this.febChainCnt = febChainCnt;
	}
    @Column(name="FEBCNT")
	public int getFebCnt() {
		return febCnt;
	}
	public void setFebCnt(int febCnt) {
		this.febCnt = febCnt;
	}
    @Column(name="FEBINCHAINCNT")
	public int getFebInChainCnt() {
		return febInChainCnt;
	}
	public void setFebInChainCnt(int febInChainCnt) {
		this.febInChainCnt = febInChainCnt;
	}
    @Column(name="SERVICEPOSITION")
	public String getServicePosition() {
		return servicePosition;
	}
	public void setServicePosition(String servicePosition) {
		this.servicePosition = servicePosition;
	}
    @Column(name="STRIPCNT")
	public int getStripCnt() {
		return stripCnt;
	}
	public void setStripCnt(int stripCnt) {
		this.stripCnt = stripCnt;
	}
    @Column(name="STRIPINETAPARTITIONCNT")
	public int getStripInEtaPartitionCnt() {
		return stripInEtaPartitionCnt;
	}
	public void setStripInEtaPartitionCnt(int stripInEtaPartitionCnt) {
		this.stripInEtaPartitionCnt = stripInEtaPartitionCnt;
	}
    @Column(name="STRIPINFEBCNT")
	public int getStripInFebCnt() {
		return stripInFebCnt;
	}
	public void setStripInFebCnt(int stripInFebCnt) {
		this.stripInFebCnt = stripInFebCnt;
	}
    @Column(name="TYPENAME")
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	
}
