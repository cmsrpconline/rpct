package rpct.db.domain.equipment;

import rpct.db.DataAccessException;
import rpct.db.domain.DAO;

/**
 * Created on 2006-12-01
 *
 * @author William Whitaker & Michal Pietrusinski
 * @version $Id$
 */
public interface ChipDAO extends DAO {
    Chip getSynCoderChipByBoard(Board board) throws DataAccessException;  
}
