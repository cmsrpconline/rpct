package rpct.db.domain.equipment;

import java.util.List;

import rpct.db.DataAccessException;
import rpct.db.domain.DAO;

/**
 * Created on 2005-03-01
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public interface LinkBoardDAO extends DAO {
    List<LinkBoard> getAll() throws DataAccessException;
    List<LinkBoard> getByLinkBox(Crate linkBox) throws DataAccessException;
    //List<LinkBoard> get(LinkBox linkBox, Boolean master, String linkName, 
    //        Boolean stripInversion, boolean fetchCableTRG, boolean fetchCablesSGN) throws DataAccessException;
}
