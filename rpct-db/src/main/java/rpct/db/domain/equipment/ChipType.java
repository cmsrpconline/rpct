package rpct.db.domain.equipment;

public enum ChipType {   
    SYNCODER("SYNCODER"), 
    TCSORT("TCSORTTCSORT"), 
    OPTO("TB3OPTO"), 
    PAC("TB3LDPAC"), 
    RMB("TB3RMB"), 
    GBSORT("TB3GBSORT"), 
    HALFSORTER("HALFSORT"), 
    FINALSORTER("FINALSORT"), 
    TTUOPTO("TTUOPTO"), 
    TTUTRIG("TTUTRIG"),
    TTUFINAL("TTUFINAL"),
    RBC("RBC"),
    FEBCHIP("FEBCHIP");
    
    private String hwType;
    
    private ChipType(String hardwareType) {
        this.hwType = hardwareType;
    }
    
    public String getHwType() {
        return hwType;
    }
    
    public static ChipType fromHwType(String hwType) {
        for(ChipType type : values()) {
            if(type.getHwType().equals(hwType))
                return type;
        }
        throw new IllegalArgumentException("hardware chip type name " + hwType + " do not fit to eny ChipType");            
    }
}
