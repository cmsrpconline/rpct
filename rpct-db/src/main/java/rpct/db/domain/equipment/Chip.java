package rpct.db.domain.equipment;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.hibernate.bytecode.javassist.FieldHandled;
import org.hibernate.bytecode.javassist.FieldHandler;

import rpct.db.domain.configuration.ChipDisabled;

@Entity
@SequenceGenerator(name="seq", sequenceName="S_11_1_CHIP", allocationSize=1)
public class Chip  implements FieldHandled, Comparable {//FieldHandled to enable lazy loading of disabled
    
    public static final int OPTO_BASE_POS = 2; // first OPTO is has position 2
    public static final int PAC_BASE_POS = 8; // first OPTO is has position 2
    
    private int id;
    private Board board;
    private ChipType type;
    private int position;
    private ChipDisabled disabled;
    
    public Chip() {
    }

    
    private FieldHandler fieldHandler;
    public FieldHandler getFieldHandler() {
    	return fieldHandler;
    }

    public void setFieldHandler(FieldHandler fieldHandler) {
    	this.fieldHandler = fieldHandler;
    }
    	 
    @Id
    @Column(name="CHIPID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }    

    @ManyToOne(fetch=FetchType.LAZY, optional = false)
    //@ManyToOne()
    @JoinColumn(name="BOARD_BOARDID")
    public Board getBoard() {
        return board;
    }
    public void setBoard(Board board) {
        this.board = board;
    }

    @Enumerated(EnumType.STRING)
    public ChipType getType() {
        return type;
    }
    public void setType(ChipType type) {
        this.type = type;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    } 

    @OneToOne(mappedBy="chip", fetch = FetchType.LAZY, optional = true)
    @LazyToOne(LazyToOneOption.NO_PROXY)
    public ChipDisabled getDisabled() {
    	if (fieldHandler != null) {
    		return (ChipDisabled) fieldHandler.readObject(this, "disabled", disabled);
    	}
		return disabled;
	}
	public void setDisabled(ChipDisabled disabled) {
		if (fieldHandler != null) {
			this.disabled = (ChipDisabled) fieldHandler.writeObject(this, "disabled", this.disabled, disabled);
			return;
		}
		
		this.disabled = disabled;
	}       

    public String toString() {
        StringBuilder str = new StringBuilder("Chip" + " ");
        str.append(getBoard().getName()).append(" id = ").append(id).append(", type = ").append(type)
            .append(", position = ").append(position);
        return str.toString();
    }

	public int compareTo(Object o) {		
		return Integer.compare(getId(), ((Chip)o).getId());
	}
}
