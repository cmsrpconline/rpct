package rpct.db.domain.equipment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Transient;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import rpct.db.DataAccessException;
import rpct.db.DataIntegrityException;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.IntArray24;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.RbcConf;


/**
 * Created on 2007-10-04
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */

@Entity
@PrimaryKeyJoinColumn(name="TTUBOARDID")
public class TtuBoard extends Board {   
	private final static Log log = LogFactory.getLog(TtuBoard.class);
    public TtuBoard() {
        super();
        setType(BoardType.TTUBOARD);
    }


    @Override
    public int maxChipCount() {
        return 13; // VME chip is not taken into account
    }  
    
    @Transient
    public List<Chip> getChips(ChipType chipType) {        
        List<Chip> result = new ArrayList<Chip>();
        for (Chip chip : getChips()) {
            if (chip.getType() == chipType) {
                result.add(chip);
            }
        }
        return result;
    }
    
    @Transient
    public List<Chip> getTTUTrigs() {
    	List<Chip> ttutrigs = new ArrayList<Chip>();
    	for(int pos = Chip.PAC_BASE_POS; pos < Chip.PAC_BASE_POS +2; pos++) {
    		Chip ttuTrig = getChipsArray()[pos];        
    		if (ttuTrig != null && ttuTrig.getType() != ChipType.TTUTRIG) {
    			throw new DataIntegrityException("Chip at pos " + pos + " is not TTUTRIG");
    		}
    		ttutrigs.add(ttuTrig);
    	}
    	return ttutrigs;

    }
    
    /**
     * @return
     * number for  TTUTRIG (0...3) or TTUOPTO (0...5) 
     */
    @Transient
    public static int getChipNum(Chip chip) {
        if(chip.getType() == ChipType.TTUOPTO)
            return chip.getPosition() - Chip.OPTO_BASE_POS;
        else if(chip.getType() == ChipType.TTUTRIG)
            return chip.getPosition() - Chip.PAC_BASE_POS;
        else
            throw new DataIntegrityException("the chip is not TTUOPTO or TTUTRIG, the number is not defined");
    }
    

    @Transient
    public List<RbcTtuConn> getRbcConn() {
    	return Arrays.asList(getConnsAsCollectorBoard().toArray(new RbcTtuConn[0]));
    }
    
    @Transient
    public RbcTtuConn getRbcConn(int ttuInputNum) {
        for(RbcTtuConn linkConn : getRbcConn() ) {
            if(linkConn.getCollectorBoardInputNum() == ttuInputNum)
                return linkConn;
        }
        //System.out.println("no LinkConn on " + getName() + " tbInputNum " + tbInputNum);
        return null;
    }
    
    /**
     * @return
     * combined mask of RBCs served by the chip,
     * taking into account the status of involved hardware: LBBox, LB, RbcTtuConn
     * @throws DataAccessException 
     */
	public static IntArray24 getConnectedRbcsMask(final Chip ttuChip, LocalConfigKey localConfigKey, 
			ConfigurationManager manager, EquipmentDAO equipmentDAO) throws DataAccessException {
		log.info("getConnectedRbcsMask() started");
		IntArray24 connectedRbcsMask = new IntArray24();         
        if (ttuChip.getType() != ChipType.TTUTRIG) {
        	throw new IllegalArgumentException("ChipType.TTUTRIG is the only allowed chip type!");
        }
        
        final TtuBoard ttuBoard = (TtuBoard)equipmentDAO.getObject(TtuBoard.class, ttuChip.getBoard().getId());
        
        /// Caching the next finals gives no boost:
/*        final HibernateContext context = new SimpleHibernateContextImpl();
		final EquipmentDAO equipment = new EquipmentDAOHibernate(context);
		final ConfigurationDAO configuration = new ConfigurationDAOHibernate(context);
		final ConfigurationManager manager = new ConfigurationManager(context, equipment, configuration);*/

		// LB to RBC input bit mapping
        //why such a strange order??? KB
		final Map<Integer, Integer> lbPositionToRbcBit= Collections.unmodifiableMap(new HashMap<Integer, Integer>() {
			private static final long serialVersionUID = 1L;
			{
				put(1, 14);
				put(4, 13);
				put(2, 12);
				put(3, 11);
				put(5, 10);
				put(6, 9);
				put(7, 8);
				put(8, 7);
				put(9, 6);
				put(14, 5);
				put(15, 4);
				put(16, 3);
				put(17, 2);
				put(18, 1);
				put(19, 0);
			}
		});

		// RBC "real" chip mask in lower 2 bytes
		final int rbcRealDisabledMask = 0x0000FFFF;
		// RBC "fake" in the higher 2 bytes
		final int fakeMaskOffset = 16;
		final int rbcFakeDisabledMask = rbcRealDisabledMask << fakeMaskOffset;
		List<BoardBoardConn> boardBoardConns = equipmentDAO.getBoardBoardConns(ttuBoard, true, true);
		//log.info(ttuBoard+ " boardBoardConns.size() " + boardBoardConns.size());
		//for (BoardBoardConn conn: ttuBoard.getConnsAsCollectorBoard()) {
		for (BoardBoardConn conn : boardBoardConns) {
			/* 
			 * One TTU_TRIG per wheel rule
			 * TRIG8: W0, W+-1 : TTU_INPUT [0, 1, 3, 4, 6, 7] 
			 * TRIG9:     W+-2 :           [9, 10, 12, 13, 15, 16]
			 * 
			 */
			final int maxTtuInputTrig8 = 7;
			final int minTtuInputTrig9 = 9;
			switch (ttuChip.getPosition()) {
			case 8: /* TRIG8 */
				if (conn.getCollectorBoardInputNum() > maxTtuInputTrig8) continue; // Not TRIG8 realm
				break;
			case 9: /* TRIG9 */
				if (conn.getCollectorBoardInputNum() < minTtuInputTrig9) continue; // Not TRIG9 realm
				break;
			default:
				throw new IllegalStateException("Got TRIG position " + ttuChip.getPosition());
			}


			final RbcBoard rbcBoard = (RbcBoard) conn.getBoard();
			final RbcBoard rbcFakeBoard = rbcBoard.getSlave();
			
			/// TRIG_MASK is indexed by (sector - 1)
			final int rbcTrigMaskIndex = rbcBoard.sectorServed() - 1;
			final int rbcFakeTrigMaskIndex = rbcFakeBoard.sectorServed() - 1;
			
			int rbcMask = rbcRealDisabledMask | rbcFakeDisabledMask;

    		
			// On RBC board and crate and RbcTtuConn not disabled, RBC input is OK
    		if (rbcBoard.getCrate().getDisabled() == null && rbcBoard.getDisabled() == null) {// && ! conn.isDisabled() - we have only enabled links in the boardBoardConns
    			final RbcConf rbcConf = (RbcConf) manager.getConfiguration(rbcBoard.getChip(), localConfigKey);
    			rbcMask = rbcConf.getMask(); // lower 26 or 28 bits matter - 4Bytes are used 
    			
    			/* LB contribution: count for LB that are disabled
    			 * Real RBC: bytes 0, 1
    			 *
    			 * Take into account LB (input) not attached to RBC
    			 *
    			 */
    			final Set<Integer> attachedLBSlots = new HashSet<Integer>();
    			for (Board lb: rbcBoard.getCrate().getBoards()) {
    				if (lb.getType() == BoardType.LINKBOARD) {
    					final int position = lb.getPosition(); //aka slot in the create
    					attachedLBSlots.add(position);
    					final boolean lbEnabled = (lb.getCrate().getDisabled() == null && lb.getDisabled() == null);
    					if (!lbEnabled) {
        					rbcMask = disableRbcBit(rbcMask, lbPositionToRbcBit.get(position));
    					}
    				}
    			}
    			// Disable LB inputs that are not attached
    			final Set<Integer> unattachedLBSlots =  new HashSet<Integer>(lbPositionToRbcBit.keySet());
    			unattachedLBSlots.removeAll(attachedLBSlots);
    			for (Integer position: unattachedLBSlots) {
					rbcMask = disableRbcBit(rbcMask, lbPositionToRbcBit.get(position));
    			}
    			
    			/// Fake processing
    			if (rbcFakeBoard.getCrate().getDisabled() == null && rbcFakeBoard.getDisabled() == null) {
    				attachedLBSlots.clear();
    				for (Board lb: rbcFakeBoard.getCrate().getBoards()) {
    					if (lb.getType() == BoardType.LINKBOARD) {
    						final int position = lb.getPosition();
        					attachedLBSlots.add(position);
    						final boolean lbEnabled = (lb.getCrate().getDisabled() == null && lb.getDisabled() == null);
    						if (!lbEnabled) {
    							rbcMask = disableRbcBit(rbcMask, lbPositionToRbcBit.get(position) + fakeMaskOffset); // higher 2 Bytes
    						}
    					}
    				}
        			// Disable LB inputs that are not attached
    				unattachedLBSlots.clear();
    				unattachedLBSlots.addAll(lbPositionToRbcBit.keySet());
        			unattachedLBSlots.removeAll(attachedLBSlots);
        			for (Integer position: unattachedLBSlots) {
    					rbcMask = disableRbcBit(rbcMask, lbPositionToRbcBit.get(position) + fakeMaskOffset);
        			}

    			}
    			else {
    				/// Fake: Crate or board disabled
        			rbcMask |= rbcFakeDisabledMask;
    			}
    		}
			//lower 2 Bytes for the "Real" board
			final int rbcRealMask = rbcMask & rbcRealDisabledMask;
			connectedRbcsMask.setValue(rbcTrigMaskIndex, rbcRealMask);
			// higher 2 Bytes are for the Fake board
			final int rbcFakeMask = rbcMask >>> fakeMaskOffset;
			connectedRbcsMask.setValue(rbcFakeTrigMaskIndex, rbcFakeMask);
    	}

		/// Masks with indexes 12 - 23 are not loaded thus are permanently disabled 
    	for (int i = 12; i < 24; i ++) { 
    		connectedRbcsMask.setValue(i, rbcRealDisabledMask);
    	}

    	log.info("done");
		return connectedRbcsMask;
	}


	/**
	 * @param rbcMask
	 * @param bitPosition
	 * @return the rbcMask with the bit[bitPosition] disabled(in the RBC/TTU convention: set to 1)
	 */
	private static int disableRbcBit(int rbcMask, int bitPosition) {
		final int bitDisabledValue = 1;

		rbcMask |= (bitDisabledValue << bitPosition);
		
		return rbcMask;
	}
}
