package rpct.db.domain.equipment.chamberstrip;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import rpct.db.DataAccessException;
import rpct.db.domain.DAOHibernate;
import rpct.db.domain.equipment.FebConnector;
import rpct.db.domain.hibernate.HibernateContext;

/**
 * Created on 2006-07-30
 * 
 * @author Michal Pietrusinski & William Whitaker
 * @version $Id$
 */
public class ChamberStripDAOHibernate extends DAOHibernate implements ChamberStripDAO {

    public ChamberStripDAOHibernate(HibernateContext hibernateContext) {
        super(hibernateContext);
    }

	public ChamberStrip getById(int id) throws DataAccessException {
        return (ChamberStrip) getObject(ChamberStrip.class, id);
	}
    public List<ChamberStrip> getStripsByFebCon(FebConnector febConnector) throws DataAccessException {
        try {
            Criteria criteria = currentSession().createCriteria(ChamberStrip.class);
            if (febConnector != null) {
                criteria = criteria.add(Restrictions.eq("febConnector", febConnector));
            }
            criteria = criteria.addOrder(Order.asc("id"));
            return criteria.list();           
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }
}
