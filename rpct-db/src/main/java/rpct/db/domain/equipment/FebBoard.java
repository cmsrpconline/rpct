package rpct.db.domain.equipment;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Transient;

import rpct.db.domain.equipment.chamberloaction.BarrelOrEndcap;
import rpct.db.domain.equipment.controlboard.ControlBoard;
import rpct.db.domain.equipment.feblocation.FebLocation;

/**
 * Created on 2009-12-04
 *
 * @author Nikolay Darmenov
 * @version $Id$
 */
@Entity
@PrimaryKeyJoinColumn(name="FEBBOARDID")
public class FebBoard extends Board {
	public static final int FEB_CHIP_CHANNEL_CNT = 8;
	
    private FebLocation febLocation;
    //private I2cCbChannel i2cCbChannel;
    //private int i2cLocalNumber;

	public FebBoard() {
		super();
        setType(BoardType.FEBBOARD);
	}

	@OneToOne(cascade = CascadeType.ALL, fetch=FetchType.LAZY, optional=false)
    @JoinColumn(name="FL_FEBLOCATIONID"/*, unique = true, nullable = false*/)
	public FebLocation getFebLocation() {
		return febLocation;
	}
	public void setFebLocation(FebLocation febLocation) {
		this.febLocation = febLocation;
	}

/*    @ManyToOne
    @JoinColumn(name="CBC_I2CCBCHANNELID")
	public I2cCbChannel getI2cCbChannel() {
		return i2cCbChannel;
	}
	public void setI2cCbChannel(I2cCbChannel i2cCbChannel) {
		this.i2cCbChannel = i2cCbChannel;
	}

	public int getI2cLocalNumber() {
		return i2cLocalNumber;
	}
	public void setI2cLocalNumber(int i2cLocalNumber) {
		this.i2cLocalNumber = i2cLocalNumber;
	}*/

	@Transient
	public I2cCbChannel getI2cCbChannel() {
		return febLocation.getI2cCbChannel();
	}

	@Transient
	public int getI2cLocalNumber() {
		return febLocation.getI2cLocalNumber();
	}

	
	@Transient
	public ControlBoard getControlBoard() {
		return febLocation.getI2cCbChannel().getControlBoard();
	}
	
	@Transient
	public int getCbChannel() {
		return febLocation.getI2cCbChannel().getCbChannel();
	}
	
	@Override
	public int maxChipCount() {
		/* NOTE:
		 * Different count in Barrel (2) and Endcap(4) 
		 * Using getChips() hits the DB!
		 */
		final int result = (getChips() == null) ? 0 : getChips().size();

		return result;
	}
		
	@Transient
	public BarrelOrEndcap getBarrelOrEndcap() {
		return getFebLocation().getChamberLocation().getBarrelOrEndcap();
	}
	
    @Transient
    @Override
	public String toString() {
		return super.toString() +
		", febLocationId = " + febLocation.getId() +
		", i2cCbChannelId = " + getI2cCbChannel().getId() +
		", i2cLocalNumber = " + getI2cLocalNumber();
	}

}
