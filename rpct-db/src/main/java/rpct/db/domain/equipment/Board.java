package rpct.db.domain.equipment;

import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import rpct.db.DataIntegrityException;
import rpct.db.domain.configuration.BoardDisabled;


@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@SequenceGenerator(name="seq", sequenceName="S_13_1_BOARD", allocationSize=1)
public abstract class Board {
    private int id;
    private String name;
    private Crate crate;
    private int position;
    private BoardType type;
    private String label;
    private Set<Chip> chips;
    private BoardDisabled disabled;
    private List<BoardBoardConn> connsAsBoard;
    private List<BoardBoardConn> connsAsCollectorBoard;

    
    @Transient
    private Chip[] chipsArray;
    
    protected Board() {        
    }
    

    @Id
    @Column(name="BOARDID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    @Column(name="NAME")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    //@ManyToOne(fetch=FetchType.LAZY)
    @ManyToOne()
    @JoinColumn(name="CRATE_CRATEID")
    public Crate getCrate() {
        return crate;
    }
    public void setCrate(Crate crate) {
        this.crate = crate;
    }
    
    @Column(name="POSITION")
    public int getPosition() {
        return position;
    }
    public void setPosition(int position) {
        this.position = position;
    }
    

    @Column(name="LABEL")
    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }    

    @Enumerated(EnumType.STRING)
    public BoardType getType() {
        return type;
    }
    public void setType(BoardType type) {
        this.type = type;
    }    

    @OneToMany(mappedBy="board", fetch=FetchType.LAZY)
    //@OneToMany(mappedBy="board")
    public Set<Chip> getChips() {
        return chips;
    }
    public void setChips(Set<Chip> chips) {
        this.chips = chips;
    }

    @OneToOne(mappedBy="board")
	public BoardDisabled getDisabled() {
		return disabled;
	}
	public void setDisabled(BoardDisabled disabled) {
		this.disabled = disabled;
	}

    @OneToMany(mappedBy="board")
    protected List<BoardBoardConn> getConnsAsBoard() {
        return connsAsBoard;
    }
    protected void setConnsAsBoard(List<BoardBoardConn> connsAsBoard) {
        this.connsAsBoard = connsAsBoard;
    }

    @OneToMany(mappedBy="collectorBoard")
    protected List<BoardBoardConn> getConnsAsCollectorBoard() {
        return connsAsCollectorBoard;
    }
    protected void setConnsAsCollectorBoard(List<BoardBoardConn> connsAsCollectorBoard) {
        this.connsAsCollectorBoard = connsAsCollectorBoard;
    }

    @Transient
    abstract public int maxChipCount();
    
    @Transient
    public Chip[] getChipsArray() {
        if (chipsArray == null) {
            chipsArray = new Chip[maxChipCount()];
            if (getChips() != null) {
                for (Chip chip : getChips()) {
                    int index = chip.getPosition();
                    if (index < 0 || index >= chipsArray.length) {
                        throw new DataIntegrityException("Invalid position of chip " + chip);
                    }
                    if (chipsArray[index] != null) { 
                        throw new DataIntegrityException("Duplicate chip at pos " + index + "(" + chip + "}");
                    }
                    chipsArray[index] = chip;
                }
            }
        }
        return chipsArray;
    }
    
    @Transient
    boolean isDisabled() {
    	if(getDisabled() != null)
    		return true;
    
  		return false; 
    }

    public String toString() {
        StringBuilder str = new StringBuilder(getClass().getSimpleName());
        str.append(" id = ").append(id).append(", type = ").append(type)
            .append(", name = ").append(name)
            .append(", position = ").append(position)
            //.append(", label = ").append(label)
            .append(isDisabled() ? " disabled" : " enabled") 
            ;
        return str.toString();
    }

    /*
    @Override
    public int hashCode() {
        final int PRIME = 31;
        int result = 1;
        result = PRIME * result + ((label == null) ? 0 : label.hashCode());
        result = PRIME * result + ((name == null) ? 0 : name.hashCode());
        result = PRIME * result + position;
        result = PRIME * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Board other = (Board) obj;
        if (label == null) {
            if (other.label != null)
                return false;
        } else if (!label.equals(other.label))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (position != other.position)
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }*/


    
}
