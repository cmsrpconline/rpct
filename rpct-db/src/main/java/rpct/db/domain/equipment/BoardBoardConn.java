package rpct.db.domain.equipment;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import rpct.db.domain.configuration.LinkDisabled;
/**
 * Created on 2010-01-19
 *
 * @author Nikolay Darmenov
 * @version $Id$
 */
@Entity
@Table(name="BOARDBOARDCONN")
@Inheritance(strategy=InheritanceType.JOINED)
@SequenceGenerator(name="seq", sequenceName="S_10_1_BOARDBOARDCONN", allocationSize=1)
public class BoardBoardConn {
    private int id;
    private Board board;
    private Board collectorBoard;    
    private int collectorBoardInputNum;
    private ConnType type;
    protected LinkDisabled disabled;

    @Id
    @Column(name="BOARDBOARDCONNID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }    

    @ManyToOne(/*fetch=FetchType.LAZY*/)
    @JoinColumn(name="BOARD_BOARDID")
    public Board getBoard() {
        return board;
    }
    public void setBoard(Board board) {
        this.board = board;
    }

    @ManyToOne(/*fetch=FetchType.LAZY*/)
    @JoinColumn(name="BOARD_COLLECTORBOARDID")
    public Board getCollectorBoard() {
        return collectorBoard;
    }
    public void setCollectorBoard(Board collectorBoard) {
        this.collectorBoard = collectorBoard;
    }
    
    public int getCollectorBoardInputNum() {
        return collectorBoardInputNum;
    }
    
    public void setCollectorBoardInputNum(int collectorBoardInputNum) {
        this.collectorBoardInputNum = collectorBoardInputNum;
    }
    
    @Enumerated(EnumType.STRING)
    public ConnType getType() {
        return type;
    }
    public void setType(ConnType type) {
        this.type = type;
    }    

    @OneToOne(mappedBy="conn")
    public LinkDisabled getDisabled() {
        return disabled;
    }
    public void setDisabled(LinkDisabled disabled) {
        this.disabled = disabled;
    }
    
    @Transient
    public boolean isDisabled() {
        return disabled != null;
    }
    
    @Transient
    public String toString() {
        String str =
        	"boardBoardCon " + type + " " + getBoard().getName() + " - " + getCollectorBoard().getName() +
        	" input " + getCollectorBoardInputNum() + (isDisabled() ? " disabled, disconneted = " + disabled.getDisconnected() :  " enabled " );        
        return str;
    }

    /*
    @Override
    public int hashCode() {
        final int PRIME = 31;
        int result = 1;
        result = PRIME * result + ((board == null) ? 0 : board.hashCode());
        result = PRIME * result + ((triggerBoard == null) ? 0 : triggerBoard.hashCode());
        result = PRIME * result + triggerBoardInputNum;
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final LinkConn other = (LinkConn) obj;
        if (board == null) {
            if (other.board != null)
                return false;
        } else if (!board.equals(other.board))
            return false;
        if (triggerBoard == null) {
            if (other.triggerBoard != null)
                return false;
        } else if (!triggerBoard.equals(other.triggerBoard))
            return false;
        if (triggerBoardInputNum != other.triggerBoardInputNum)
            return false;
        return true;
    }    */
}
