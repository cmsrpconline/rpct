package rpct.db.domain.equipment.feblocation;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.hibernate.bytecode.javassist.FieldHandled;
import org.hibernate.bytecode.javassist.FieldHandler;

import rpct.db.domain.equipment.FebBoard;
import rpct.db.domain.equipment.FebConnector;
import rpct.db.domain.equipment.I2cCbChannel;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.chamberloaction.ChamberLocation;
/**
 * Created on 2006-02-20
 *
 * @author Michal Pietrusinski & William Whitaker
 * @version $Id$
 */


@Entity
@SequenceGenerator(name="seq", sequenceName="S_15_1_FEBLOCATION", allocationSize=1)
public class FebLocation  implements FieldHandled {//FieldHandled allows lazy loading of the febBoard
    private int id;
    private ChamberLocation chamberLocation;
    private I2cCbChannel i2cCbChannel;
    private LinkBoard linkBoard;
    private String febLocalEtaPartition;
    private String febCmsEtaPartition;
    private Integer posInLocalEtaPartition;
    private Integer posInCmsEtaPartition;
    private Integer i2cLocalNumber;
    private List<FebConnector> febConnectors;
    private FebBoard febBoard;

    @Id
    @Column(name="FEBLOCATIONID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="LB_LINKBOARDID")
    public LinkBoard getLinkBoard() {
        return linkBoard;
    }

    public void setLinkBoard(LinkBoard linkBoard) {
        this.linkBoard = linkBoard;
    }

    @ManyToOne(/*fetch=FetchType.LAZY*/)
    @JoinColumn(name="CBC_I2CCBCHANNELID")
    public I2cCbChannel getI2cCbChannel() {
        return i2cCbChannel;
    }
    public void setI2cCbChannel(I2cCbChannel i2cCbChannel) {
        this.i2cCbChannel = i2cCbChannel;
    }

    @ManyToOne
    @JoinColumn(name="CL_CHAMBERLOCATIONID")
    public ChamberLocation getChamberLocation() {
        return chamberLocation;
    }
    public void setChamberLocation(ChamberLocation chamberLocation) {
        this.chamberLocation = chamberLocation;
    }

    public String getFebCmsEtaPartition() {
        return febCmsEtaPartition;
    }
    public void setFebCmsEtaPartition(String febCmsEtaPartition) {
        this.febCmsEtaPartition = febCmsEtaPartition;
    }

    public String getFebLocalEtaPartition() {
        return febLocalEtaPartition;
    }
    public void setFebLocalEtaPartition(String febLocalEtaPartition) {
        this.febLocalEtaPartition = febLocalEtaPartition;
    }

    public Integer getI2cLocalNumber() {
        return i2cLocalNumber;
    }
    public void setI2cLocalNumber(Integer i2cLocalNumber) {
        this.i2cLocalNumber = i2cLocalNumber;
    }

    public Integer getPosInCmsEtaPartition() {
        return posInCmsEtaPartition;
    }
    public void setPosInCmsEtaPartition(Integer posInCmsEtaPartition) {
        this.posInCmsEtaPartition = posInCmsEtaPartition;
    }

    public Integer getPosInLocalEtaPartition() {
        return posInLocalEtaPartition;
    }
    public void setPosInLocalEtaPartition(Integer posInLocalEtaPartition) {
        this.posInLocalEtaPartition = posInLocalEtaPartition;
    }    

    @OneToMany(mappedBy="febLocation", fetch=FetchType.LAZY) 
    public List<FebConnector> getFebConnectors() {
        return febConnectors;
    }
    public void setFebConnectors(List<FebConnector> febConnectors) {
        this.febConnectors = febConnectors;
    }
    
    private FieldHandler fieldHandler;
    public FieldHandler getFieldHandler() {
    	return fieldHandler;
    }

    public void setFieldHandler(FieldHandler fieldHandler) {
    	this.fieldHandler = fieldHandler;
    }

    @OneToOne(mappedBy="febLocation", fetch=FetchType.LAZY, optional=false)
    @LazyToOne(LazyToOneOption.NO_PROXY)
	public FebBoard getFebBoard() {
    	if (fieldHandler != null) {
    		return (FebBoard) fieldHandler.readObject(this, "febBoard", febBoard);
    	}
		return febBoard;
	}
	public void setFebBoard(FebBoard febBoard) {
		if (fieldHandler != null) {
			this.febBoard = (FebBoard)fieldHandler.writeObject(this, "febBoard", this.febBoard, febBoard);
			return;
		}
		this.febBoard = febBoard;
	}
	
	public String toString() {		
		String str = "FebLocation: LocalEtaPartition " + febLocalEtaPartition
				+ " CmsEtaPartition " + febCmsEtaPartition
				+ " posInLocalEtaPartition " + posInLocalEtaPartition 
				+ " posInCmsEtaPartition " + posInCmsEtaPartition;
				
		str += " febBoard " + (getFebBoard() == null ? "null" : febBoard.getName());
		str += " linkBoard " + (linkBoard == null ? "null" : linkBoard.getName());
		str += " " + i2cCbChannel + " i2cLocalNumber " + i2cLocalNumber; ;
		return str;
	}
	
    /*@OneToOne(mappedBy="febLocation", fetch=FetchType.LAZY, optional=false)
    //@Transient
	public FebBoard getFebBoard() {
		return febBoard;
	}
	public void setFebBoard(FebBoard febBoard) {
		this.febBoard = febBoard;
	}*/
}
