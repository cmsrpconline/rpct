package rpct.db.domain.equipment.distributionboard;

import rpct.db.DataAccessException;
import rpct.db.domain.DAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;

/**
 * Created on 2005-03-01
 * 
 * @author Michal Pietrusinski & William Whitaker
 * @version $Id$
 */
public class DistributionBoardDAOHibernate extends DAOHibernate implements DistributionBoardDAO {

    public DistributionBoardDAOHibernate(HibernateContext hibernateContext) {
        super(hibernateContext);
    }

	public DistributionBoard getById(int id) throws DataAccessException {
        return (DistributionBoard) getObject(DistributionBoard.class, id);
	}
}
