package rpct.db.domain.equipment;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Transient;

import rpct.db.domain.equipment.chamberloaction.BarrelOrEndcap;
import rpct.db.domain.equipment.chamberloaction.ChamberLocation;
import rpct.db.domain.equipment.chamberstrip.ChamberStrip;
import rpct.db.domain.equipment.controlboard.ControlBoard;
import rpct.db.domain.equipment.feblocation.FebLocation;
import rpct.xdaq.axis.Binary;


/**
 * Created on 2005-03-08
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */

@Entity
@PrimaryKeyJoinColumn(name="LINKBOARDID")
public class LinkBoard extends Board {
    public static final int IN_CHANNELS_CNT = 96;
    
    private Board masterBoard;
    private List<LinkBoard> slaves;
    private List<FebLocation> febLocations;
    private List<LinkConn> linkConns;
    
    private byte[] connectedStripsMask;
    //private boolean stripInversion;
    //private CableTRG cableTRG;   
    //private List<CableSGN> cablesSGN = new ArrayList<CableSGN>();
    private Chip[] febChips;

    public LinkBoard() {
        super();
        setType(BoardType.LINKBOARD);
    }

    //@OneToMany(mappedBy="linkBoard", fetch=FetchType.LAZY)
    @OneToMany(mappedBy="linkBoard")
    public List<FebLocation> getFEBLocations() {
        return febLocations;
    }
    public void setFEBLocations(List<FebLocation> febLocations) {
        this.febLocations = febLocations;
    }

    //@ManyToOne(fetch=FetchType.LAZY)
    @ManyToOne()
    @JoinColumn(name="MASTERID")
    public Board getMasterBoard() {
        return masterBoard;
    }
    public void setMasterBoard(Board masterBoard) {
        this.masterBoard = masterBoard;
    }
    
    @Transient
    public LinkBoard getMaster() {
        return (LinkBoard) getMasterBoard();
    }

    //@OneToMany(mappedBy="master", fetch=FetchType.LAZY)
    @OneToMany(mappedBy="masterBoard")
    public List<LinkBoard> getSlaves() {
        return slaves;
    }
    public void setSlaves(List<LinkBoard> slaves) {
        this.slaves = slaves;
    }    
    

    /**
     * @param slaveNum - 0, 1, 2
     * @return if slaveNum == 0 - this master, 1 - returns left slave, if 2 returns right slave,
     * returns null if no there is no requested slave
     */
    @Transient
    public LinkBoard getSlave(int slaveNum) {
        if (slaveNum != 0 && slaveNum != 1 && slaveNum != 2) {
            throw new IllegalArgumentException("slaveNum != 0 && slaveNum != 1 && slaveNum != 2");
        }
        if (slaveNum == 0)
            return this;
        for (Board slave : slaves) {
            if(slaveNum == 1 && ((getPosition() -1) == slave.getPosition()))
                return (LinkBoard) slave;
            else if(slaveNum == 2 && ((getPosition() + 1) == slave.getPosition()))
                return (LinkBoard) slave;
        }
        return null;
    }
    
    @Transient
    public LinkBoard getLeftSlave() {
        return getSlave(1);
    }
    
    @Transient
    public LinkBoard getRightSlave() {
        return getSlave(2);
    }
    
    @Transient
    public Chip getSynCoder() {
        Set<Chip> chips2 = getChips();
        if (chips2.size() != 1) {
            throw new IllegalStateException("Invalid number of chips (" + chips2.size() + ") for " + toString());
        }
        return chips2.iterator().next();
    }

    
    @Transient
    public boolean isMaster() {
        //if(getMaster() == this)
        //    return true;
        
        if (getPosition() < 10) {
            return (getPosition() % 3) == 2;
        }
        return ((getPosition() - 10) % 3) == 2;
    }
    
    @Transient   
    public int getChannelNum() {
        /*if(getMaster() == this)
            return true; nir dziala, master nie jest ustawiany*/
       /* if(getName().contains("CH0"))
            return 0;
        if(getName().contains("CH1"))
            return 1;
        if(getName().contains("CH2"))
            return 2;*/
        
    	if(getPosition() < 10) {
    		if(getPosition()%3 == 2)
    			return 0;
    		if(getPosition()%3 == 1)
    			return 1;
    		if(getPosition()%3 == 0)
    			return 2;
    	}
    	else {
    		if(getPosition()%3 == 0)
    			return 0;
    		if(getPosition()%3 == 2)
    			return 1;
    		if(getPosition()%3 == 1)
    			return 2;
    	}
        
        return 0; 
    }
    
    //@OneToMany(mappedBy="board", fetch=FetchType.LAZY)
    @Transient
    public List<LinkConn> getLinkConns() {
    	final List<BoardBoardConn> bbcs = getConnsAsBoard();
    	linkConns = new ArrayList<LinkConn>();
    	for (BoardBoardConn bbc: bbcs) {
    		linkConns.add((LinkConn) bbc);
    	}
    	
    	return linkConns;
    }
    @Transient
    public void setLinkConns(List<LinkConn> linkConns) {
    	final List<BoardBoardConn> bbcs = new ArrayList<BoardBoardConn>();
    	for (LinkConn lc: linkConns) {
    		bbcs.add(lc);
    	}
    	setConnsAsBoard(bbcs);
 
    	this.linkConns = linkConns; 
    }

    /*
    @Override
    public int hashCode() {
        final int PRIME = 31;
        int result = super.hashCode();
        result = PRIME * result + ((febLocations == null) ? 0 : febLocations.hashCode());
        //result = PRIME * result + ((master == null || master == this) ? 0 : master.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        final LinkBoard other = (LinkBoard) obj;
        if (febLocations == null) {
            if (other.febLocations != null)
                return false;
        } else if (!febLocations.equals(other.febLocations))
            return false;
        if (master == null) {
            if (other.master != null)
                return false;
        } else if (!master.equals(other.master))
            return false;
        return true;
    }*/

    @Override
    public int maxChipCount() {
        return 1;
    }

    private TreeSet<String> chamberNames = new TreeSet<String>();
    private List<ChamberLocation> chamberLocations = new ArrayList<ChamberLocation>();
    @Transient 
    public TreeSet<String> getChamberNames() {
        if(chamberNames.size() == 0) {
            for(FebLocation febLocation : getFEBLocations()) {
                String chamberName = febLocation.getChamberLocation().getChamberLocationName() ;
                if(febLocation.getChamberLocation().getBarrelOrEndcap() == BarrelOrEndcap.Barrel) {
                    chamberName += " " + febLocation.getFebLocalEtaPartition();
                }
                if(chamberNames.add(chamberName)) {
                	chamberLocations.add(febLocation.getChamberLocation());
                }
            }
        }
        return chamberNames;
    }
       
    
    private String chamberName = "";
    /**
     * @return the unique chamber name, i.e. without ++, --, +, - (except RB4 sec 4)
     */
    @Transient
    public String getChamberName() {
    	if(chamberName.equals("") == false)
    		return chamberName;
    	
    	getChamberNames();
    	if(chamberNames.size() > 1) {
    		for(String chN : chamberNames) {
    			chamberName = chN.replaceFirst("\\+ ", "/").replace("- ", "/");
    			if(chN.contains("++") || chN.contains("--"))
    				break;
    		}
    	}
    	else {
    		chamberName = chamberNames.first().replace(" ", "/");;
    	}
    			
    	
    	return chamberName;
    }
    
    @Transient 
    public List<ChamberLocation> getChamberLocations() {
        if(chamberLocations.size() == 0) {
        	getChamberNames();
        }
        return chamberLocations;
    }
    
    
    @Transient 
    public TreeSet<String> getChamberNamesIncludingSlaves() {
        TreeSet<String> chamberNamesIncludingSlaves = new TreeSet<String>();
        chamberNamesIncludingSlaves.addAll(getChamberNames());
        if(isMaster()) {
            for(LinkBoard slb : getSlaves()) {
                chamberNamesIncludingSlaves.addAll(slb.getChamberNames());
            }
        }

        return chamberNamesIncludingSlaves;
    }

    @Transient 
    public byte[] getConnectedStripsMask() {
        if(connectedStripsMask == null) {
            connectedStripsMask = new byte[IN_CHANNELS_CNT/Byte.SIZE];
            for(FebLocation febLocation : getFEBLocations()) {
                for(FebConnector febConnector : febLocation.getFebConnectors() ) {
                    int linkBoardInputNum = febConnector.getLinkBoardInputNum() -1;
                    for(ChamberStrip strip : febConnector.getChamberStrips()) {
                       int cableChannelNum = strip.getCableChannelNum() -1;
                       int channelNumber = linkBoardInputNum * 16 + cableChannelNum;
                       Binary.setBit(connectedStripsMask, channelNumber, 1);
                    }
                }
            }
        }
        return connectedStripsMask;
    }
    
    /**
     * @return the array of the fixed size 12 elements, containing the FEB Chips in the order as connected to the LB inputs
     * the information about this connections is not stored in the DB, thus this function contains this information  
     */
    @Transient
    public Chip[] getFebChips() {
    	if(febChips == null) {
            febChips = new Chip[IN_CHANNELS_CNT / FebBoard.FEB_CHIP_CHANNEL_CNT];
            for(FebLocation febLocation : getFEBLocations()) {    		

                boolean is_re11 = (febLocation.getChamberLocation().getBarrelOrEndcap() == BarrelOrEndcap.Endcap
                                   && Math.abs(febLocation.getChamberLocation().getDiskOrWheel()) == 1
                                   && febLocation.getChamberLocation().getLayer() == 1);

                //assuming that connectors from given FEB are connected to continuous LB inputs
                int minLBinNum = 6;
                for (FebConnector febConnector : febLocation.getFebConnectors())
                    if (febConnector.getLinkBoardInputNum() < minLBinNum)
                        minLBinNum = febConnector.getLinkBoardInputNum();
                minLBinNum -= 1;
                int nChips = febLocation.getFebBoard().getChips().size();
                if (is_re11) // only 2 are in use for RE+1/1
                    nChips = 2;

                boolean reverse = false;
                if (febLocation.getChamberLocation().getBarrelOrEndcap() == BarrelOrEndcap.Endcap) {
                    if (Math.abs(febLocation.getChamberLocation().getDiskOrWheel()) == 1) {
                        if (febLocation.getChamberLocation().getLayer() == 1) // RE+1/1:A,C
                            reverse = (febLocation.getFebLocalEtaPartition().equals("A") || febLocation.getFebLocalEtaPartition().equals("C"));
                        else // RE+1/2:A, RE+1/3:A
                            reverse = (febLocation.getFebLocalEtaPartition().equals("A"));
                    } else // RE+2:A,B, RE+3:A,B
                        reverse = (febLocation.getFebLocalEtaPartition().equals("A") || febLocation.getFebLocalEtaPartition().equals("B"));
                }

                for(Chip chip : febLocation.getFebBoard().getChips()) {
                    int index = minLBinNum * 2;
                    if (febLocation.getChamberLocation().getBarrelOrEndcap() == BarrelOrEndcap.Endcap) {
                        if (is_re11) // 23 becomes 01
                            index += (reverse ? (nChips - (chip.getPosition() % 2) - 1) : (chip.getPosition() % 2)); // 0-3 -> 3-0
                        else
                            index += (reverse ? (nChips - chip.getPosition() - 1) : chip.getPosition()); // 0-3 -> 3-0
                    } else // Barrel
                        index += chip.getPosition();

                    boolean is_connected = true;
                    if (is_re11)
                        if (febLocation.getChamberLocation().getSector() % 2 == 1)
                            is_connected = (reverse ? chip.getPosition() >= 2 : chip.getPosition() <= 1); // strip 1-16
                        else
                            is_connected = (reverse ? chip.getPosition() <= 1 : chip.getPosition() >= 2); // strip 17-32

                    if (is_connected)
                        febChips[index] = chip;
                }
            }
        }
        return febChips;
    }
    
    @Transient
    public LinkBox getLinkBox() {
        return (LinkBox)getCrate();
    }
    
    @Transient
    public ControlBoard getControlBoard() {
        ControlBoard controlBoard = null;        
        if(getPosition() < 10) {
            controlBoard = getLinkBox().getControlBoard10();
        }
        else {
            controlBoard = getLinkBox().getControlBoard20();
        }
        return controlBoard;
    }

	/**
	 * @return will + sector name for barrel, disc + ring name for endcaps (i.e. it is nor sector at all 
	 */
    @Transient
	public String getDetectorSectorName() {
		String sectorName = "";
/*		String firstChamber = getChamberNames().first();
		if(firstChamber.contains("RB")) {
			
			sectorName = firstChamber.substring(0, firstChamber.indexOf(' ', 8));
		}
		else if(firstChamber.contains("RE")) {
			sectorName = firstChamber.substring(0, 6);
		} */
		ChamberLocation chamberLocation = chamberLocations.get(0);
		sectorName = (chamberLocation.getBarrelOrEndcap().equals(BarrelOrEndcap.Barrel) ? "W" : "RE") +  chamberLocation.getDiskOrWheel() + "_sec" + chamberLocation.getSector();
		return sectorName;
	}
    
    @Transient
    public String toString() {
    	String str;
    	str = "LinkBoard " + getName() + " id " + getId() + (isMaster() ? " Master" : " Slave ") + " pos " + getPosition();
    	return str;
    }

    @Transient 
    public int getSector() {
        int sector = 0;
        String sectStr = getName().substring(getName().indexOf('S') + 1);
        sectStr = sectStr.substring(0, sectStr.indexOf("_"));
        sector = Integer.parseInt(sectStr);
        return sector;
    }
}
