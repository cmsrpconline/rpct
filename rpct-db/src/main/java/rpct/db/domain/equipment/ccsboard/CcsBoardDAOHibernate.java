package rpct.db.domain.equipment.ccsboard;


import java.util.List;

import rpct.db.DataAccessException;
import rpct.db.domain.DAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;

/**
 * Created on 2006-02-20
 * 
 * @author Michal Pietrusinski & William Whitaker
 * @version $Id$
 */
public class CcsBoardDAOHibernate extends DAOHibernate implements CcsBoardDAO {

    public CcsBoardDAOHibernate(HibernateContext hibernateContext) {
        super(hibernateContext);
    }
	public CcsBoard getById(int id) throws DataAccessException {
        return (CcsBoard) getObject(CcsBoard.class, id);
	}
    public List<CcsBoard> getAll() throws DataAccessException {
        return getObjects(CcsBoard.class);
    }
}
