package rpct.db.domain.equipment;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import rpct.db.DataAccessException;
import rpct.db.domain.DAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;

/**
 * Created on 2005-03-01
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class LinkBoardDAOHibernate extends DAOHibernate implements LinkBoardDAO {
    
    public LinkBoardDAOHibernate(HibernateContext hibernateContext) {
        super(hibernateContext);
    }

    public List<LinkBoard> getAll() throws DataAccessException {
        return getObjects(LinkBoard.class);
    }

    public List<LinkBoard> getByLinkBox(Crate linkBox) throws DataAccessException {
        try {
            /*return currentSession().createQuery("from LinkBoard " +
                    "where linkBox = :linkBox order by linkBoxSlot")
                    .setParameter("linkBox", linkBox).list();*/
            return currentSession().createCriteria(LinkBoard.class)
                .add(Restrictions.eq("crate", linkBox))
                .addOrder(Order.asc("position"))
                .list();           
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        } 
    }

    /*public List<LinkBoard> get(LinkBox linkBox, Boolean master, 
            String linkName, Boolean stripInversion, 
            boolean fetchCableTRG, boolean fetchCablesSGN) throws DataAccessException {
        try {
            Criteria criteria = currentSession().createCriteria(LinkBoard.class); 
            if (linkBox != null) {
                criteria.add(Restrictions.eq("linkBox", linkBox));
            }
            if (master != null) {
                criteria.add(Restrictions.eq("master", master));
            }
            if (linkName != null) {
                Criteria linkCriteria = criteria.createCriteria("cableTRG");
                linkCriteria.add(Restrictions.like("name", linkName));
            }
            if (stripInversion != null) {
                criteria.add(Restrictions.eq("stripInversion", stripInversion));
            }
            if (fetchCableTRG) {
                criteria.setFetchMode("cableTRG", FetchMode.JOIN);
            }
            if (fetchCablesSGN) {
                criteria.setFetchMode("cablesSGN", FetchMode.JOIN);
            }
            Criteria linkBoxCriteria = criteria.createCriteria("linkBox");
            linkBoxCriteria.addOrder(Order.asc("wheel"));
            linkBoxCriteria.addOrder(Order.asc("sector"));
            criteria.addOrder(Order.asc("linkBoxSlot"));
            return criteria.list();           
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        } 
    }*/
}
