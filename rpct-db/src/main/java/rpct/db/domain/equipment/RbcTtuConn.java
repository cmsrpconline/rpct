package rpct.db.domain.equipment;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * Created on 2010-01-20
 *
 * @author Nikolay Darmenov
 * @version $Id$
 */


@Entity
@Table(name="RBCTTUCONN")
@PrimaryKeyJoinColumn(name="RBCTTUCONNID")
public class RbcTtuConn extends BoardBoardConn {
	@Transient
	public RbcBoard getRbcBoard() {
        return (RbcBoard) getBoard();
    }
	
	@Transient
    public void setRbcBoard(RbcBoard rbcBoard) {
        setBoard(rbcBoard);
    }
    
	@Transient
	public TtuBoard getTtuBoard() {
        return (TtuBoard) getCollectorBoard();
    }
	
	@Transient
    public void setTtuBoard(TtuBoard ttuBoard) {
        setCollectorBoard(ttuBoard);
    }
    
	@Transient
	public int getTtuBoardInputNum() {
        return getCollectorBoardInputNum();
    }
    
    @Transient
    public void setTtuBoardInputNum(int ttuBoardInputNum) {
        setCollectorBoardInputNum(ttuBoardInputNum);
    }
    
    @Transient
    public String toString() {
        String str = ""; 
        str = "rbcTtuCon " + getBoard().getName() + " - " + getTtuBoard().getName() + " ttuInput " + getTtuBoardInputNum() + (isDisabled() ? " disabled " :  " enabled " );        
        return str;
    }
}
