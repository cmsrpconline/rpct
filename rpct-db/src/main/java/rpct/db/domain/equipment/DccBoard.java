package rpct.db.domain.equipment;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;


/**
 * Created on 2005-03-08
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */

@Entity
@PrimaryKeyJoinColumn(name="DCCBOARDID")
public class DccBoard extends Board {   

    private Integer daqId;
    private int fedNumber;

    public DccBoard() {
        super();
        setType(BoardType.DCCBOARD);
    }
    
    public Integer getDaqId() {
        return daqId;
    }

    public void setDaqId(Integer daqId) {
        this.daqId = daqId;
    }

    public int getFedNumber() {
        return fedNumber;
    }

    public void setFedNumber(int fedNumber) {
        this.fedNumber = fedNumber;
    }

    @Override
    public int maxChipCount() {
        return 0;
    }
}
