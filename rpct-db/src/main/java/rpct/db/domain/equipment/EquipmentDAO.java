package rpct.db.domain.equipment;

import java.util.List;
import java.util.Map;

import rpct.db.DataAccessException;
import rpct.db.domain.DAO;
import rpct.db.domain.equipment.ccsboard.CcsBoard;
import rpct.db.domain.equipment.chamberloaction.BarrelOrEndcap;
import rpct.db.domain.equipment.chamberstrip.ChamberStrip;
import rpct.db.domain.equipment.controlboard.ControlBoard;
import rpct.db.service.ChamStripAccessInfo;
import rpct.db.service.FebConnectorStrips;
import rpct.db.service.DTLineFebChipFlatInfo;
import rpct.db.service.RbcInfo;

/**
 * Created on 2005-03-01
 * 
 * @author Michal Pietrusinski
 * @version $Id$
 */
public interface EquipmentDAO extends DAO {

    Crate getCrateById(int id) throws DataAccessException;  
    Crate getCrateByName(String name) throws DataAccessException;        
    List<Crate> getCrates() throws DataAccessException;    
    List<Crate> getCratesByType(CrateType crateType, boolean enabledOnly) throws DataAccessException;
    
    Board getBoardById(int id) throws DataAccessException;  
    Board getBoardByName(String name) throws DataAccessException;
    List<Board> getBoardsByNameLike(String name, boolean enabledOnly) throws DataAccessException;
    List<Board> getBoardsByType(BoardType boardType, boolean enabledOnly) throws DataAccessException;
    List<LinkBoard> getLinkBoardsWithoutChips() throws DataAccessException;
    
    Chip getChipById(int id) throws DataAccessException;
    List<Chip> getChips(List<Integer> ids, boolean loadBoards, boolean loadCrates) throws DataAccessException;
    List<Chip> getChipsByType(ChipType chipType, boolean enabledOnly) throws DataAccessException;
    List<Chip> getChipsByTypeAndBoard(ChipType chipType, Board board, boolean enabledOnly) throws DataAccessException;
    List<Chip> getChipsByTypeAndCrate(ChipType chipType, Crate crate, boolean enabledOnly) throws DataAccessException;

    public List<LinkConn> getLinkConns() throws DataAccessException;
    BoardBoardConn getLinkConnById(int id) throws DataAccessException;
    List<LinkConn> getLinkConns(TriggerBoard triggerBoard, boolean enabledOnly) throws DataAccessException;
    List<BoardBoardConn> getBoardBoardConns(Board collectorBoard, boolean enabledOnly, boolean loadTransmitterCrate) throws DataAccessException;
    List<Integer> getLinkConnsTriggerBoardInputNum(TriggerBoard triggerBoard, boolean enabledOnly) throws DataAccessException;
    List<Integer> getRbcTtuConnsTtuBoardInputNum(TtuBoard ttuBoard, boolean enabledOnly) throws DataAccessException;
    LinkConn getLinkConnByTriggerBoardAndInputNum(TriggerBoard triggerBoard, int triggerBoardInputNum) throws DataAccessException;
    
    I2cCbChannel getI2cCbChannelByControlBoardAndCbChannel(ControlBoard controlBoard,
            int cbChannel) throws DataAccessException;
    
    List<LinkBox> getLinkBoxesByFec(CcsBoard ccsBoard, int fecPosition) throws DataAccessException;
    List<LinkBox> getLinkBoxesByChamberLocation(Integer diskOrWheel, Integer layer, Integer sector, String subsector, BarrelOrEndcap barrelOrEndcap) throws DataAccessException;    

    List<ChamberStrip> getChamberStripsByChamberLocation(int diskOrWheel,
            int layer, int sector, String subsector,
            BarrelOrEndcap barrelOrEndcap) throws DataAccessException;

    List<ChamStripAccessInfo> getChamStripAccessInfosByChamberLocation(int diskOrWheel,
            int layer, int sector, String subsector,
            BarrelOrEndcap barrelOrEndcap) throws DataAccessException;

    List<FebConnectorStrips> getFebConnectorStrips(String xdaqExecutiveHost
                                                   , int xdaqExecutivePort) throws DataAccessException;
    List<DTLineFebChipFlatInfo> getDTLineFebChipFlatInfo(int wheel) throws DataAccessException;

    List<RbcInfo> getAllRbcInfo() throws DataAccessException;

    Map<String, List<LinkBox>> getLinkBoxesByTower(EquipmentDAO equipmentDAO, boolean enabledOnly) throws DataAccessException;
    
    public List<FebBoard> getFebBoards() throws DataAccessException;
    public FebBoard getFebBoard(int id) throws DataAccessException;
}
