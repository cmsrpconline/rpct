package rpct.db.domain.equipment.ccsboard;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;

import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.controlboard.ControlBoard;


/**
 * Created on 2006-02-17
 *
 * @author Michal Pietrusinski & William Whitaker
 * @version $Id$
 */

@Entity
@PrimaryKeyJoinColumn(name="CCSBOARDID")
public class CcsBoard extends Board {
    private List<ControlBoard> controlBoards;
    public CcsBoard() {
        super();
        setType(BoardType.CCSBOARD);
    }
    
    //@OneToMany(mappedBy="ccsBoard", fetch=FetchType.LAZY)
    @OneToMany(mappedBy="ccsBoard")
    public List<ControlBoard> getControlBoards() {
    	return controlBoards;
    }
	public void setControlBoards(List<ControlBoard> controlBoards) {
		this.controlBoards = controlBoards;
	}

    @Override
    public int maxChipCount() {
        return 0;
    }
}