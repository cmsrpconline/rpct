package rpct.db.domain.equipment.chambertype;

import rpct.db.DataAccessException;
import rpct.db.domain.DAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;

/**
 * Created on 2005-03-01
 * 
 * @author Michal Pietrusinski & William Whitaker
 * @version $Id$
 */
public class ChamberTypeDAOHibernate extends DAOHibernate implements ChamberTypeDAO {

    public ChamberTypeDAOHibernate(HibernateContext hibernateContext) {
        super(hibernateContext);
    }

	public ChamberType getById(int id) throws DataAccessException {
        return (ChamberType) getObject(ChamberType.class, id);
	}
}
