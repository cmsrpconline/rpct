package rpct.db.domain.equipment;

import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Transient;


/**
 * Created on 2005-03-08
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */

@Entity
@PrimaryKeyJoinColumn(name="TCBACKPLANEID")
public class TcBackplane extends Board {
    
    private int logSector;

    public TcBackplane() {
        super();
        setType(BoardType.TCBACKPLANE);
    }
    
    public int getLogSector() {
        return logSector;
    }
    public void setLogSector(int logSector) {
        this.logSector = logSector;
    }

    @Transient
    public Chip getTcSorter() {
        Set<Chip> chips2 = getChips();
        if (chips2.size() != 1) {
            throw new IllegalStateException("Invalid number of chips (" + chips2.size() + ") for " + toString());
        }
        return chips2.iterator().next();
    }

    @Override
    public int maxChipCount() {
        return 1;
    }    
}
