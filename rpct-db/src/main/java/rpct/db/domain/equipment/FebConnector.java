package rpct.db.domain.equipment;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import rpct.db.domain.equipment.chamberstrip.ChamberStrip;
import rpct.db.domain.equipment.feblocation.FebLocation;
/**
 * Created on 2006-07-03
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */


@Entity
@SequenceGenerator(name="seq", sequenceName="S_16_1_FEBCONNECTOR", allocationSize=1)
public class FebConnector {
    private int id;
    private FebLocation febLocation;
    public Integer linkBoardInputNum;
    public Integer febConnectorNum;
    private List<ChamberStrip> chamberStrips;

    @Id
    @Column(name="FEBCONNECTORID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    @ManyToOne(/*fetch=FetchType.LAZY*/)
    @JoinColumn(name="FL_FEBLOCATIONID")
    public FebLocation getFebLocation() {
        return febLocation;
    }
    public void setFebLocation(FebLocation febLocation) {
        this.febLocation = febLocation;
    }

    public Integer getFebConnectorNum() {
        return febConnectorNum;
    }
    public void setFebConnectorNum(Integer febConnectorNum) {
        this.febConnectorNum = febConnectorNum;
    }

    public Integer getLinkBoardInputNum() {
        return linkBoardInputNum;
    }
    public void setLinkBoardInputNum(Integer linkBoardInputNum) {
        this.linkBoardInputNum = linkBoardInputNum;
    }

    @OneToMany(mappedBy="febConnector")
    public List<ChamberStrip> getChamberStrips() {
        return chamberStrips;
    }
    public void setChamberStrips(List<ChamberStrip> chamberStrips) {
        this.chamberStrips = chamberStrips;
    }

	public String toString() {
		String str = " febConnectorNum " + febConnectorNum + " linkBoardInputNum " 
				+ linkBoardInputNum + " chamberStrips.size " + chamberStrips.size(); 
		return str;
	}
}
