package rpct.db.domain.equipment;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import rpct.db.DataAccessException;
import rpct.db.domain.DAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;

/**
 * Created on 2005-03-01
 * 
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class CrateDAOHibernate extends DAOHibernate implements CrateDAO {

    public CrateDAOHibernate(HibernateContext hibernateContext) {
        super(hibernateContext);
    }

    public Crate getById(int id) throws DataAccessException {
        return (Crate) getObject(Crate.class, id);
    }

    public Crate getByName(String name) throws DataAccessException {
        try {
            return (Crate) currentSession().createCriteria(Crate.class)
                .add(Restrictions.eq("name", name)).uniqueResult();           
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }    

    public List<Crate> getAll() throws DataAccessException {
        return getObjects(Crate.class);
    }
}
