/**
 * 
 */
package rpct.db.domain.equipment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class TcHsbConnection {
    private TriggerCrate tc;
    private Chip halfSort;
    int channelNum; //0...7

    static private final Map<String, List<String> > tcHsbConnectionMap = new HashMap<String, List<String>>();
    
    public static Map<String, List<String>> getTcHsbConnectionMap() {
        return tcHsbConnectionMap;
    }

    static {
        List<String> hsb0 = new ArrayList<String>();
        hsb0.add("TC_11");
        hsb0.add("TC_0");
        hsb0.add("TC_1");
        hsb0.add("TC_2");
        hsb0.add("TC_3");
        hsb0.add("TC_4");
        hsb0.add("TC_5");
        hsb0.add("TC_6");
        
        tcHsbConnectionMap.put("HSB_0", hsb0);
        
        List<String> hsb1 = new ArrayList<String>();
        hsb1.add("TC_5");
        hsb1.add("TC_6");
        hsb1.add("TC_7");
        hsb1.add("TC_8");
        hsb1.add("TC_9");
        hsb1.add("TC_10");
        hsb1.add("TC_11");
        hsb1.add("TC_0");
        tcHsbConnectionMap.put("HSB_1", hsb1); 
    }
    
    public TcHsbConnection(TriggerCrate tc, Chip hsb, int channelNum) {
        super();
        this.tc = tc;
        this.halfSort = hsb;
        this.channelNum = channelNum;
    }

    //      0..15 - first cable input number
    public int getCableInputNum() {
        return channelNum * 2;
    }

    public int getChannelNum() {
        return channelNum;
    }

    public Chip getHalfSort() {
        return halfSort;
    }

    public TriggerCrate getTc() {
        return tc;
    }   

    public String getDescritpion() {
        return "TcHsbConnection " + tc.getName() + " - " + halfSort.getBoard().getName() + "; channelNum " + channelNum + " cableInputsNum " + getCableInputNum() + " and " + (getCableInputNum() +1);
    }

    public static boolean isOnHcb(int tc, int hsb)
    {
        if (hsb<0 || hsb>1)
            throw new RuntimeException("There is no HSB_"+hsb);
        return getTcHsbConnectionMap().get("HSB_"+hsb).contains("TC_"+tc);
    }
}