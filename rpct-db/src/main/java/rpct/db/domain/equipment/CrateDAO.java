package rpct.db.domain.equipment;

import java.util.List;

import rpct.db.DataAccessException;
import rpct.db.domain.DAO;

/**
 * Created on 2005-03-01
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public interface CrateDAO extends DAO {
    Crate getById(int id) throws DataAccessException;  
    Crate getByName(String name) throws DataAccessException;        
    List<Crate> getAll() throws DataAccessException;
}
