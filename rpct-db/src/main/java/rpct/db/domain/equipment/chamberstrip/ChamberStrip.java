package rpct.db.domain.equipment.chamberstrip;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import rpct.db.domain.equipment.FebConnector;


/**
 * Created on 2006-07-30
 *
 * @author Michal Pietrusinski & William Whitaker
 * @version $Id$
 */

@Entity
@Table(name="CHAMBERSTRIP")
@SequenceGenerator(name="seq", sequenceName="S_03_1_CHAMBERSTRIP")
public class ChamberStrip {
    private int id;
//    private List<StripResponse> stripResponses;
    private FebConnector febConnector;
    private Integer cablePinNumber;
    private Integer chamberStripNumber;
    private Integer cmsStripNumber;
    private Integer cmsGlobalStripNumber;
    private Integer cableChannelNum;
    @Id
    @Column(name="CHAMBERSTRIPID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
/*    @OneToMany(mappedBy="chamberStrip")
    public List<StripResponse> getStripResponses() {
    	return stripResponses;
    }
	public void setStripResponses(List<StripResponse> stripResponses) {
		this.stripResponses = stripResponses;
	}
*/    
    
    @ManyToOne
    @JoinColumn(name="FC_FEBCONNECTORID")
	public FebConnector getFebConnector() {
		return febConnector;
	}

	public void setFebConnector(FebConnector febConnector) {
		this.febConnector = febConnector;
	}
    @Column(name="CABLEPINNUMBER")
	public Integer getCablePinNumber() {
		return cablePinNumber;
	}
	public void setCablePinNumber(Integer cablePinNumber) {
		this.cablePinNumber = cablePinNumber;
	}
    @Column(name="CHAMBERSTRIPNUMBER")
	public Integer getChamberStripNumber() {
		return chamberStripNumber;
	}
	public void setChamberStripNumber(Integer chamberStripNumber) {
		this.chamberStripNumber = chamberStripNumber;
	}
    @Column(name="CMSSTRIPNUMBER")
	public Integer getCmsStripNumber() {
		return cmsStripNumber;
	}
	public void setCmsStripNumber(Integer cmsStripNumber) {
		this.cmsStripNumber = cmsStripNumber;
	}
    @Column(name="CMSGLOBALSTRIPNUMBER")
	public Integer getCmsGlobalStripNumber() {
		return cmsGlobalStripNumber;
	}
	public void setCmsGlobalStripNumber(Integer cmsGlobalStripNumber) {
		this.cmsGlobalStripNumber = cmsGlobalStripNumber;
	}
    @Column(name="CABLECHANNELNUM")
	public Integer getCableChannelNum() {
		return cableChannelNum;
	}
	public void setCableChannelNum(Integer cableChannelNum) {
		this.cableChannelNum = cableChannelNum;
	}
	
	public String toString() {
		String str = "ChamberStrip: chamberStripNumber " + chamberStripNumber + " cmsStripNumber " + cmsStripNumber +
				" cableChannelNum " + cableChannelNum + " cablePinNumber " + cablePinNumber;
		return str;
	}

}