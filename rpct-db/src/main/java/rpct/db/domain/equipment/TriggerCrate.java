package rpct.db.domain.equipment;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;

@Entity
@DiscriminatorValue("TRIGGERCRATE")
public class TriggerCrate extends Crate {   
    private TcBackplane tcBackplane = null;
    private List<TriggerBoard> triggerBoards = null;
    
    public TriggerCrate() {
        setType(CrateType.TRIGGERCRATE);
    }

    @Override
    public int slotCount() {
        return 21;
    }

    @Transient
    public TcBackplane getBackplane() {
        if (tcBackplane == null) {
            for (Board board : getBoards()) {
                if (board instanceof TcBackplane) {
                    tcBackplane = (TcBackplane) board;
                }
            }
            if (tcBackplane == null) {
                throw new IllegalStateException("backplane not found");
            }
        }
        return tcBackplane;
    }    
    
    @Transient
    public List<TriggerBoard> getTriggerBoards() {
        if (triggerBoards == null) {
            triggerBoards = new ArrayList<TriggerBoard>();
            for (Board board : getBoards()) {
                if (board instanceof TriggerBoard) {
                    triggerBoards.add((TriggerBoard) board);
                }
            }
        }

        class NameComparator implements Comparator<TriggerBoard>{
            public int compare(TriggerBoard tb1, TriggerBoard tb2){
                Integer tb1pos = tb1.getPosition();
                Integer tb2pos = tb2.getPosition();
                return tb1pos.compareTo(tb2pos);
            }
        }        
        NameComparator comparator =  new NameComparator();
        
        Collections.sort(triggerBoards, comparator);
        return triggerBoards;        
    }
    
    @Transient
    public int getLogSector() {
        return getBackplane().getLogSector();
    }
    
    private List<TcHsbConnection> tcHsbConnections;
    @Transient
    public List<TcHsbConnection> getTcHsbConnections(EquipmentDAO equipmentDAO) throws DataAccessException, RemoteException, ServiceException {
        if(tcHsbConnections != null)
            return tcHsbConnections;
        
        tcHsbConnections = new ArrayList<TcHsbConnection>();
        
        int logSector = getLogSector();
        if(logSector >= 0 && logSector <= 6) {
            Chip hSort = equipmentDAO.getChipsByTypeAndBoard(ChipType.HALFSORTER, equipmentDAO.getBoardByName("HSB_0"), true).get(0);
            TcHsbConnection tcHsbConnection = new TcHsbConnection(this, hSort, logSector + 1); 
            tcHsbConnections.add(tcHsbConnection);
        }
        if(logSector >= 5 && logSector <= 11) {
            Chip hSort = equipmentDAO.getChipsByTypeAndBoard(ChipType.HALFSORTER, equipmentDAO.getBoardByName("HSB_1"), true).get(0);
            TcHsbConnection tcHsbConnection = new TcHsbConnection(this, hSort, logSector - 5); 
            tcHsbConnections.add(tcHsbConnection);
        }
        if(logSector == 11) {
            Chip hSort = equipmentDAO.getChipsByTypeAndBoard(ChipType.HALFSORTER, equipmentDAO.getBoardByName("HSB_0"), true).get(0);
            TcHsbConnection tcHsbConnection = new TcHsbConnection(this, hSort, 0); 
            tcHsbConnections.add(tcHsbConnection);
        }
        if(logSector == 0) {
            Chip hSort = equipmentDAO.getChipsByTypeAndBoard(ChipType.HALFSORTER, equipmentDAO.getBoardByName("HSB_1"), true).get(0);
            TcHsbConnection tcHsbConnection = new TcHsbConnection(this, hSort, 7); 
            tcHsbConnections.add(tcHsbConnection);
        }
        
        return tcHsbConnections;
    }
}
