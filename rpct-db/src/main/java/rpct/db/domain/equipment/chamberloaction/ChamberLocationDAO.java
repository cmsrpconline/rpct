package rpct.db.domain.equipment.chamberloaction;

import java.util.List;

import rpct.db.DataAccessException;
import rpct.db.domain.DAO;

/**
 * Created on 2006-02-20
 *
 * @author Michal Pietrusinski & William Whitaker
 * @version $Id$
 */
public interface ChamberLocationDAO extends DAO {
	ChamberLocation getById(int id) throws DataAccessException;  
	List<ChamberLocation> getByBarrelOrEndcap(BarrelOrEndcap barrelOrEndcap) throws DataAccessException;  
	List<ChamberLocation> getByDiskOrWheel(int diskOrWheel, BarrelOrEndcap barrelOrEndcap) throws DataAccessException;  
	List<ChamberLocation> getByLayer(int diskOrWheel, int layer, BarrelOrEndcap barrelOrEndcap) throws DataAccessException;  
	List<ChamberLocation> getBySector(int diskOrWheel, int sector, BarrelOrEndcap barrelOrEndcap) throws DataAccessException;  
	List<ChamberLocation> getByLocation(int diskOrWheel, int layer, int sector, String subsector, BarrelOrEndcap barrelOrEndcap) throws DataAccessException;    
    ChamberLocation getByName(String chamberLocationName) throws DataAccessException;
}
