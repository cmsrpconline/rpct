package rpct.db.domain.equipment.chamberloaction;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.sun.xml.bind.v2.runtime.RuntimeUtil.ToStringAdapter;

import rpct.db.domain.equipment.chambertype.ChamberType;
import rpct.db.domain.equipment.feblocation.FebLocation;


/**
 * Created on 2006-02-20
 *
 * @author Michal Pietrusinski & William Whitaker
 * @version $Id$
 */

@Entity
@Table(name="CHAMBERLOCATION")
@SequenceGenerator(name="seq", sequenceName="S_04_1_CHAMLOCATION", allocationSize=1)
public class ChamberLocation{
    private int id;
    private List<FebLocation> febLocations;
    private ChamberType chamberType;
    private int diskOrWheel;
    private int layer;
    private int sector;
    private String subsector;
    private String chamberLocationName;
    private String febZOrnt;
    private String febRadOrnt;
    private BarrelOrEndcap barrelOrEndcap;

    @Id
    @Column(name="CHAMBERLOCATIONID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    @OneToMany(mappedBy="chamberLocation", fetch=FetchType.LAZY)
    public List<FebLocation> getFEBLocations() {
        return febLocations;
    }
    public void setFEBLocations(List<FebLocation> febLocations) {
        this.febLocations = febLocations;
    }

    @ManyToOne(fetch=FetchType.LAZY, optional=false)
    @JoinColumn(name="CT_CHAMBERTYPEID")
    public ChamberType getChamberType() {
        return chamberType;
    }
    public void setChamberType(ChamberType chamberType) {
        this.chamberType = chamberType;
    }

    @Enumerated(EnumType.STRING)
    public BarrelOrEndcap getBarrelOrEndcap() {
        return barrelOrEndcap;
    }
    public void setBarrelOrEndcap(BarrelOrEndcap barrelOrEndcap) {
        this.barrelOrEndcap = barrelOrEndcap;
    }

    public String getChamberLocationName() {
        return chamberLocationName;
    }
    public void setChamberLocationName(String chamberLocationName) {
        this.chamberLocationName = chamberLocationName;
    }

    public int getDiskOrWheel() {
        return diskOrWheel;
    }
    public void setDiskOrWheel(int diskOrWheel) {
        this.diskOrWheel = diskOrWheel;
    }

    public String getFEBRadOrnt() {
        return febRadOrnt;
    }
    public void setFEBRadOrnt(String febRadOrnt) {
        this.febRadOrnt = febRadOrnt;
    }

    public String getFEBZOrnt() {
        return febZOrnt;
    }
    public void setFEBZOrnt(String febZOrnt) {
        this.febZOrnt = febZOrnt;
    }

    public int getLayer() {
        return layer;
    }
    public void setLayer(int layer) {
        this.layer = layer;
    }

    public int getSector() {
        return sector;
    }
    public void setSector(int sector) {
        this.sector = sector;
    }

    public String getSubsector() {
        return subsector;
    }
    public void setSubsector(String subsector) {
        this.subsector = subsector;
    }
    
    public String toString() {
    	String str = getChamberLocationName() + " diskOrWheel " + diskOrWheel + " layer " + layer + " sector " + sector 
    			+ " febZOrnt " + febZOrnt
    			+ " chamberType " + chamberType.getTypeName();
    	return str;
    }
}