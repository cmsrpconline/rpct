package rpct.db.domain.equipment;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import rpct.db.DataAccessException;
import rpct.db.domain.DAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;

/**
 * Created on 2006-12-01
 * 
 * @author William Whitaker & Michal Pietrusinski
 * @version $Id$
 */
public class ChipDAOHibernate extends DAOHibernate implements ChipDAO {

    public ChipDAOHibernate(HibernateContext hibernateContext) {
        super(hibernateContext);
    }

    public Chip getSynCoderChipByBoard(Board board) throws DataAccessException {
        try {
        	return (Chip) currentSession().createCriteria(Chip.class)
        			.add(Restrictions.eq("board", board))
        			.add(Restrictions.eq("type", ChipType.SYNCODER))
        			.uniqueResult();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }
}
