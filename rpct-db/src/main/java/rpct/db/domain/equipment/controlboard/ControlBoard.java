package rpct.db.domain.equipment.controlboard;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.ccsboard.CcsBoard;


/**
 * Created on 2006-02-17
 *
 * @author Michal Pietrusinski & William Whitaker
 * @version $Id$
 */

@Entity
@PrimaryKeyJoinColumn(name="CONTROLBOARDID")
public class ControlBoard extends Board {
    //private List<I2cCbChannel> distributionBoards;
    private CcsBoard ccsBoard;
    private int fecPosition;
    private int positionInCcuChain;
    private int ccuAddress;
    public ControlBoard() {
        super();
        setType(BoardType.CONTROLBOARD);
    }

    /*@OneToMany(mappedBy="controlBoard")
    public List<DistributionBoard> getDistributionBoards() {
    	return distributionBoards;
    }
	public void setDistributionBoards(List<DistributionBoard> distributionBoards) {
		this.distributionBoards = distributionBoards;
	}*/
    
    @ManyToOne
    @JoinColumn(name="CCSBOARD_CCSBOARDID")
	public rpct.db.domain.equipment.ccsboard.CcsBoard getCcsBoard() {
		return ccsBoard;
	}
	public void setCcsBoard(rpct.db.domain.equipment.ccsboard.CcsBoard ccsBoard) {
		this.ccsBoard = ccsBoard;
	}

    public int getFecPosition() {
        return fecPosition;
    }
    public void setFecPosition(int fecPosition) {
        this.fecPosition = fecPosition;
    }


	public int getPositionInCcuChain() {
		return positionInCcuChain;
	}
	public void setPositionInCcuChain(int positionInCcuChain) {
		this.positionInCcuChain = positionInCcuChain;
	}

	public int getCcuAddress() {
		return ccuAddress;
	}
	public void setCcuAddress(int ccuAddress) {
		this.ccuAddress = ccuAddress;
	}
    
    public String toString() {
        StringBuilder str = new StringBuilder(super.toString());
        str.append(" name ").append(getName())
           .append(", fecPosition = ").append(fecPosition)
           .append(", positionInCcuChain = ").append(positionInCcuChain)
           .append(", ccuAddress = 0x").append(Integer.toHexString(ccuAddress));
        return str.toString();
    }

    @Override
    public int maxChipCount() {
        return 0;
    }
}