package rpct.db.domain.equipment.chamberloaction;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import rpct.db.DataAccessException;
import rpct.db.domain.DAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.util.StringUtils;

/**
 * Created on 2006-02-20
 * 
 * @author Michal Pietrusinski & William Whitaker
 * @version $Id: ChamberLocationDAOHibernate.java,v 1.6 2006/07/11 08:57:53 tb
 *          Exp $
 */
public class ChamberLocationDAOHibernate extends DAOHibernate implements
        ChamberLocationDAO {

    public ChamberLocationDAOHibernate(HibernateContext hibernateContext) {
        super(hibernateContext);
    }

    public ChamberLocation getById(int id) throws DataAccessException {
        return (ChamberLocation) getObject(ChamberLocation.class, id);
    }

    public List<ChamberLocation> getByBarrelOrEndcap(
            BarrelOrEndcap barrelOrEndcap) throws DataAccessException {
        try {
            return currentSession().createCriteria(ChamberLocation.class).add(
                    Restrictions.eq("barrelOrEndcap", barrelOrEndcap))
                    .addOrder(Order.asc("id")).list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    public List<ChamberLocation> getByDiskOrWheel(int diskOrWheel,
            BarrelOrEndcap barrelOrEndcap) throws DataAccessException {
        try {
            return currentSession().createCriteria(ChamberLocation.class).add(
                    Restrictions.eq("diskOrWheel", diskOrWheel)).add(
                    Restrictions.eq("barrelOrEndcap", barrelOrEndcap))
                    .addOrder(Order.asc("id")).list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    public List<ChamberLocation> getByLayer(int diskOrWheel, int layer,
            BarrelOrEndcap barrelOrEndcap) throws DataAccessException {
        try {
            return currentSession().createCriteria(ChamberLocation.class).add(
                    Restrictions.eq("diskOrWheel", diskOrWheel)).add(
                    Restrictions.eq("layer", layer)).add(
                    Restrictions.eq("barrelOrEndcap", barrelOrEndcap))
                    .addOrder(Order.asc("id")).list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    public List<ChamberLocation> getBySector(int diskOrWheel, int sector,
            BarrelOrEndcap barrelOrEndcap) throws DataAccessException {
        try {
            return currentSession().createCriteria(ChamberLocation.class).add(
                    Restrictions.eq("diskOrWheel", diskOrWheel)).add(
                    Restrictions.eq("sector", sector)).add(
                    Restrictions.eq("barrelOrEndcap", barrelOrEndcap))
                    .addOrder(Order.asc("id")).list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    public List<ChamberLocation> getByLocation(int diskOrWheel, int layer,
            int sector, String subsector, BarrelOrEndcap barrelOrEndcap)
            throws DataAccessException {
        subsector = StringUtils.trimToNull(subsector);
        if ("Endcap".equals(barrelOrEndcap) && subsector != null) {
            throw new IllegalArgumentException(
                    "subsector cannot be used for Endcap");
        }

        try {
            Criteria criteria = currentSession().createCriteria(
                    ChamberLocation.class);
            criteria = criteria
                    .add(Restrictions.eq("diskOrWheel", diskOrWheel)).add(
                            Restrictions.eq("layer", layer)).add(
                            Restrictions.eq("sector", sector)).add(
                            Restrictions.eq("barrelOrEndcap", barrelOrEndcap));
            System.out.println("Subsector before call: "  + subsector);
            if (subsector != null) {
                criteria = criteria
                        .add(Restrictions.eq("subsector", subsector));
            }
            criteria = criteria.addOrder(Order.asc("id"));
            return criteria.list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    public ChamberLocation getByName(String chamberLocationName)
            throws DataAccessException {
        try {
            return (ChamberLocation) currentSession().createCriteria(
                    ChamberLocation.class)
                    .add(
                            Restrictions.eq("chamberLocationName",
                                    chamberLocationName)).uniqueResult();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }
}
