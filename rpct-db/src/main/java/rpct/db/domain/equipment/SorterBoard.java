package rpct.db.domain.equipment;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrimaryKeyJoinColumn;


/**
 * Created on 2005-03-08
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */

@Entity
@PrimaryKeyJoinColumn(name="SORTERBOARDID")
public class SorterBoard extends Board {
    
    public enum SorterType { FSB, HSB, TTU_BACKPLANE }
    
    //private List<LinkConn> linkConns;
    //private DccBoard dccBoard;
    private SorterType sorterType;
    

    public SorterBoard() {
        super();
        setType(BoardType.SORTERBOARD);
    }

    /*
    @OneToMany(mappedBy="triggerBoard")
    public List<LinkConn> getLinkConns() {
        return linkConns;
    }
    public void setLinkConns(List<LinkConn> linkConns) {
        this.linkConns = linkConns;
    }

    @Transient
    public List<Chip> getChips(ChipType chipType) {        
        List<Chip> result = new ArrayList<Chip>();
        for (Chip chip : getChips()) {
            if (chip.getType() == chipType) {
                result.add(chip);
            }
        }
        return result;
    }
    
    @Transient
    public Chip getOptoForLinkNum(int inputNum) {
        int pos = inputNum / 3 + Chip.OPTO_BASE_POS;
        Chip opto = getChipsArray()[pos];        
        if (opto != null 
                && opto.getType() != ChipType.OPTO) {
            throw new DataIntegrityException("Chip at pos " + pos + " is not OPTO");
        }
        return opto;
    }

    @Transient
    public Chip getPac(int num) {
        int pos = num + Chip.PAC_BASE_POS;
        Chip pac = getChipsArray()[pos];        
        if (pac != null 
                && pac.getType() != ChipType.PAC) {
            throw new DataIntegrityException("Chip at pos " + pos + " is not PAC");
        }
        return pac;
    	
    }*/

    @Override
    public int maxChipCount() {
        return 1; // VME chip is not taken into account
    }    

    @Enumerated(EnumType.STRING)
    public SorterType getSorterType() {
        return sorterType;
    }

    public void setSorterType(SorterType sorterType) {
        this.sorterType = sorterType;
    }

    /*
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="DCCBOARD_DCCBOARDID")
    public DccBoard getDccBoard() {
        return dccBoard;
    }
    public void setDccBoard(DccBoard dccBoard) {
        this.dccBoard = dccBoard;
    }*/
}
