package rpct.db.domain.equipment;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import rpct.db.DataAccessException;
import rpct.db.domain.DAOHibernate;
import rpct.db.domain.equipment.feblocation.FebLocation;
import rpct.db.domain.hibernate.HibernateContext;

/**
 * Created on 2005-03-01
 * 
 * @author Michal Pietrusinski & William Whitaker
 * @version $Id$
 */
public class FebConnectorDAOHibernate extends DAOHibernate implements FebConnectorDAO {

    public FebConnectorDAOHibernate(HibernateContext hibernateContext) {
        super(hibernateContext);
    }

    public FebConnector getById(int id) throws DataAccessException {
        return (FebConnector) getObject(FebConnector.class, id);
    }

    public List<FebConnector>  getConnectionsByFebLoc(FebLocation febLocation) throws DataAccessException {
        try {
            Criteria criteria = currentSession().createCriteria(FebConnector.class);
            if (febLocation != null) {
                criteria = criteria.add(Restrictions.eq("febLocation", febLocation));
            }
            criteria = criteria.addOrder(Order.asc("id"));
            return criteria.list();           
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }
}
