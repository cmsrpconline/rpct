package rpct.db.domain.equipment;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("CONTROLCRATE")
public class ControlCrate extends Crate {

    public ControlCrate() {
        setType(CrateType.CONTROLCRATE);
    }

    @Override
    public int slotCount() {
        return 21;
    }
}
