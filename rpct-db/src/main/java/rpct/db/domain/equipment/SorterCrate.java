package rpct.db.domain.equipment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
@DiscriminatorValue("SORTERCRATE")
public class SorterCrate extends Crate {
	private List<TtuBoard> ttuBoards = null;
	
    public SorterCrate() {
        setType(CrateType.SORTERCRATE);
    }

    @Override
    public int slotCount() {
        return 21;
    }
    
    @Transient
    public List<TtuBoard> getTTUBoards() {
        if (ttuBoards == null) {
        	ttuBoards = new ArrayList<TtuBoard>();
            for (Board board : getBoards()) {
                if (board instanceof TtuBoard) {
                	ttuBoards.add((TtuBoard) board);
                }
            }
        }

        class NameComparator implements Comparator<TtuBoard>{
            public int compare(TtuBoard ttu1, TtuBoard ttu2){
                Integer tb1pos = ttu1.getPosition();
                Integer tb2pos = ttu2.getPosition();
                return tb1pos.compareTo(tb2pos);
            }
        }        
        NameComparator comparator =  new NameComparator();
        
        Collections.sort(ttuBoards, comparator);
        return ttuBoards;        
    }
}
