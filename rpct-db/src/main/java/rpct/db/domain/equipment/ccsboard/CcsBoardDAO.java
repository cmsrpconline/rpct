package rpct.db.domain.equipment.ccsboard;

import rpct.db.DataAccessException;
import rpct.db.domain.DAO;

/**
 * Created on 2006-02-20
 *
 * @author Michal Pietrusinski & William Whitaker
 * @version $Id$
 */
public interface CcsBoardDAO extends DAO {
	CcsBoard getById(int id) throws DataAccessException;  
}
