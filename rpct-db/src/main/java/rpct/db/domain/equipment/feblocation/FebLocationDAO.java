package rpct.db.domain.equipment.feblocation;

import java.util.List;

import rpct.db.DataAccessException;
import rpct.db.domain.DAO;
import rpct.db.domain.equipment.chamberloaction.ChamberLocation;

/**
 * Created on 2006-02-20
 *
 * @author Michal Pietrusinski & William Whitaker
 * @version $Id$
 */
public interface FebLocationDAO extends DAO {
    FebLocation getById(int id) throws DataAccessException;  
    public List<FebLocation> getFebs() throws DataAccessException;
    List<FebLocation> getFebsByChamberLocation(ChamberLocation chamberLocation) throws DataAccessException;
    List<FebLocation> getByLocParameters(ChamberLocation chamberLocation, String febLocalEtaPartition, 
            Integer posInLocalEtaPartition) throws DataAccessException;
}
