package rpct.db.domain.equipment.chamberloaction;

public enum BarrelOrEndcap {
    Barrel, Endcap
}
