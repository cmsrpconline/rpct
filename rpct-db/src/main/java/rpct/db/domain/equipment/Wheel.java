package rpct.db.domain.equipment;

/**
 * Created on 2005-03-03
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public enum Wheel {
    WN2, WN1, W0, W1, W2, YEN1, YEP1;    
}
