package rpct.db.domain.equipment;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Transient;

import rpct.db.DataIntegrityException;


/**
 * Created on 2005-03-08
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */

@Entity
@PrimaryKeyJoinColumn(name="TRIGGERBOARDID")
public class TriggerBoard extends Board {
    
    private List<LinkConn> linkConns;
    private DccBoard dccBoard;
    
    private int[] usedOptLinksInputsMask;

    public TriggerBoard() {
        super();
        setType(BoardType.TRIGGERBOARD);
    }

    @Transient
    public List<LinkConn> getLinkConns() {
    	if(linkConns != null)
    		return linkConns;
    	
    	final List<BoardBoardConn> bbcs = getConnsAsCollectorBoard();
    	linkConns = new ArrayList<LinkConn>();
    	for (BoardBoardConn bbc: bbcs) {
    		linkConns.add((LinkConn) bbc);
    	}
    	
        return linkConns;
    }
    
    @Transient
    public void setLinkConns(List<LinkConn> linkConns) {
    	final List<BoardBoardConn> bbcs = new ArrayList<BoardBoardConn>();
    	for (LinkConn lc: linkConns) {
    		bbcs.add(lc);
    	}
    	setConnsAsCollectorBoard(bbcs);
 
    	this.linkConns = linkConns;
    }

    @Transient
    public LinkConn getLinkConn(int tbInputNum) {
    	LinkConn foundLinkConn = null;
        for(LinkConn linkConn : getLinkConns() ) {
            if(linkConn.getTriggerBoardInputNum() == tbInputNum) {
            	if(foundLinkConn != null) {
            		throw new RuntimeException("anoter link is already at this position: " + foundLinkConn);
            	}
            	foundLinkConn = linkConn;
            }
        }
        //System.out.println("no LinkConn on " + getName() + " tbInputNum " + tbInputNum);
        return foundLinkConn;
    }
    
    @Transient
    public List<Chip> getChips(ChipType chipType) {        
        List<Chip> result = new ArrayList<Chip>();
        for (Chip chip : getChips()) {
            if (chip.getType() == chipType) {
                result.add(chip);
            }
        }
        return result;
    }
    
    @Transient
    public Chip getOptoForLinkNum(int inputNum) {
        int pos = inputNum / 3 + Chip.OPTO_BASE_POS;
        Chip opto = getChipsArray()[pos];        
        if (opto != null 
                && opto.getType() != ChipType.OPTO) {
            throw new DataIntegrityException("Chip at pos " + pos + " is not OPTO");
        }
        return opto;
    }

    @Transient
    public Chip getPac(int num) {
        int pos = num + Chip.PAC_BASE_POS;
        Chip pac = getChipsArray()[pos];        
        if (pac != null 
                && pac.getType() != ChipType.PAC) {
            throw new DataIntegrityException("Chip at pos " + pos + " is not PAC");
        }
        return pac;
    	
    }
    

    @Transient
    public Chip getRmb() {
        int pos = 4 + Chip.PAC_BASE_POS;
        Chip pac = getChipsArray()[pos];        
        if (pac != null 
                && pac.getType() != ChipType.RMB) {
            throw new DataIntegrityException("Chip at pos " + pos + " is not RMB");
        }
        return pac;
        
    }

    @Override
    public int maxChipCount() {
        return 13; // VME chip is not taken into account
    }


    //@ManyToOne(fetch=FetchType.LAZY)
    @ManyToOne()
    @JoinColumn(name="DCCBOARD_DCCBOARDID")
    public DccBoard getDccBoard() {
        return dccBoard;
    }
    public void setDccBoard(DccBoard dccBoard) {
        this.dccBoard = dccBoard;
    }
    
    @Transient
    public int getLogSector() {
        return ((TriggerCrate)getCrate()).getLogSector();
    }
    
    /**
     * @return
     * number on the TC backplane 0...8
     */
    @Transient
    public int getNum() {
        return getPosition() - 11;
    }
    
    /**
     * @return
     * number for  Pac (0...3) or opto (0...5) 
     */
    @Transient
    public static int getChipNum(Chip chip) {
        if(chip.getType() == ChipType.OPTO)
            return chip.getPosition() - Chip.OPTO_BASE_POS;
        else if(chip.getType() == ChipType.PAC)
            return chip.getPosition() - Chip.PAC_BASE_POS;
        else
            throw new DataIntegrityException("the chip is not opto or pac, the number is not defined");
    }
    
    @Transient
    public int getUsedOptLinksInputMask(int optoNum) {
    	if(usedOptLinksInputsMask ==  null) {
    		usedOptLinksInputsMask = new int[6];
    		for(LinkConn linkConn : getLinkConns()) {
    			usedOptLinksInputsMask[getChipNum(linkConn.getOpto())] |= (1 << (linkConn.getTriggerBoardInputNum()%6));
    		}
    	}
    	return usedOptLinksInputsMask[optoNum];
    }
    
    @Transient
    public TriggerCrate getTriggerCrate() {
        return (TriggerCrate)getCrate();
    }
    
    private static final int[][] towerNums = {
        {-16, -15, -14, -13},
        {-12, -11, -10,  -9},
        { -8,  -7,  -6,  -5},
        { -4,  -3,  -2     },
        { -1,   0,   1     },
        {  2,   3,   4     },
        {  5,   6,   7,  8 },
        {  9,  10,  11, 12 },
        { 13,  14,  15, 16 },
    };
    
    @Transient
    public int[] getTowersNums() {
        return towerNums[getNum()];
    }
}
