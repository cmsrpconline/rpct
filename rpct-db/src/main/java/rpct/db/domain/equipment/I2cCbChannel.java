package rpct.db.domain.equipment;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import rpct.db.domain.equipment.controlboard.ControlBoard;
import rpct.db.domain.equipment.feblocation.FebLocation;
/**
 * Created on 2006-07-03
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */


@Entity
@SequenceGenerator(name="seq", sequenceName="S_09_1_I2CCBCHANNEL", allocationSize=1)
public class I2cCbChannel {
    private int id;
    private ControlBoard controlBoard;
    private List<FebLocation> febLocations;
    private Integer cbChannel;
    //private List<FebBoard> febBoards;
    
    @Id
    @Column(name="I2CCBCHANNELID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    @ManyToOne(fetch=FetchType.LAZY, optional=false)
    @JoinColumn(name="CB_CONTROLBOARDID")
    public ControlBoard getControlBoard() {
        return controlBoard;
    }

    public void setControlBoard(ControlBoard controlBoard) {
        this.controlBoard = controlBoard;
    }
    
    @OneToMany(mappedBy="i2cCbChannel", fetch=FetchType.LAZY)
    public List<FebLocation> getFEBLocations() {
        return febLocations;
    }
    public void setFEBLocations(List<FebLocation> febLocations) {
        this.febLocations = febLocations;
    }
    
    public Integer getCbChannel() {
        return cbChannel;
    }
    public void setCbChannel(Integer cbChannel) {
        this.cbChannel = cbChannel;
    }

/*    @OneToMany(mappedBy="i2cCbChannel")
	public List<FebBoard> getFebBoards() {
		return febBoards;
	}
    public void setFebBoards(List<FebBoard> febBoards) {
		this.febBoards = febBoards;
	}*/
    
    @Transient
    public String toString() {
    	String str;
    	str = "I2cCbChannel: id " + getId() + " cb " + (getControlBoard() == null ? "null" :  getControlBoard().getName()) + " cbChannel " + getCbChannel();
    	return str;
    }
}
