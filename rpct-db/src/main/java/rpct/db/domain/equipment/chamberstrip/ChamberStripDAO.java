package rpct.db.domain.equipment.chamberstrip;

import java.util.List;

import rpct.db.DataAccessException;
import rpct.db.domain.DAO;
import rpct.db.domain.equipment.FebConnector;

/**
 * Created on 2006-07-30
 *
 * @author Michal Pietrusinski & William Whitaker
 * @version $Id$
 */
public interface ChamberStripDAO extends DAO {
	ChamberStrip getById(int id) throws DataAccessException;  
    List<ChamberStrip> getStripsByFebCon(FebConnector febConnector) throws DataAccessException;  
}
