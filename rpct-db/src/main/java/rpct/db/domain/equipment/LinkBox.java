package rpct.db.domain.equipment;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

import rpct.db.domain.equipment.controlboard.ControlBoard;


@Entity
@DiscriminatorValue("LINKBOX")
public class LinkBox extends Crate {
    private List<LinkBoard> linkBoards = null;
    
    public LinkBox() {
        setType(CrateType.LINKBOX);
    }

    @Transient
    @Override
	public
    int slotCount() {
        return 21; //0 is backplen (not present in the LBOX), 10 and 20 are CBs
    }
    
    @Transient
    public ControlBoard getControlBoard10() {
        return (ControlBoard) getBoardArray()[10];
    }

    @Transient
    public ControlBoard getControlBoard20() {
        return (ControlBoard) getBoardArray()[20];
    }
    
    /**
     * @return in case of the endcap disks 3 one LBB contains LBs for two consecutive sectors 
     * (12/1, 2/3, ... , 10/11).
     * then the number of the lower sector is given (except 12/1 when the 12 is given)
     */
    @Transient 
    public int getSector() {
        int sector = 0;
        String name = getName();
        String sectStr = "";
        if(name.contains("/")) {
            sectStr = name.substring(name.indexOf('S') + 1, name.indexOf('/'));
        }
        else {
            sectStr = name.substring(name.indexOf('S') + 1);
        }

        sector = Integer.parseInt(sectStr);
        return sector;
    }

    @Transient 
    public String isTowerNearOrFar() {
        int sector = getSector();
        return ((sector < 4 || sector > 9) ? "near" : "far");
    }
    
    @Transient 
    public String getTowerName() {
        String name = getName();
        if(name.contains("PASTEURA_LBB"))
            return name;
        if(name.contains("LBB_904"))
            return name;
        //LBB_YEN3_S2/3
        if(name.contains("_S"))
        	return name.substring(4, name.indexOf("_", 5)) + "_" + isTowerNearOrFar() ;
        return name + "_unknownTowerName";
    }
    
    @Transient 
    public List<LinkBoard> getLinkBoards() {
        if(linkBoards == null) {
            linkBoards = new ArrayList<LinkBoard>();
            for(Board board : getBoards()) {
                if(board.getType() == BoardType.LINKBOARD) {
                    linkBoards.add((LinkBoard)board);
                }
            }
        }
        return linkBoards;
    }
    
    @Transient 
    public String toString() {
    	 return ("LinkBox " + getName()); 
    }
}
