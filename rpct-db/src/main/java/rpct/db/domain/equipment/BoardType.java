package rpct.db.domain.equipment;

public enum BoardType {
    LINKBOARD, 
    CONTROLBOARD, 
    CCSBOARD, 
    TRIGGERBOARD, 
    TCBACKPLANE, 
    SORTERBOARD, 
    DCCBOARD,
    TTUBOARD,
    RBCBOARD,
    RBCFAKEBOARD,
    FEBBOARD
}
