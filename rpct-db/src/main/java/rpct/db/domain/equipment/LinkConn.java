package rpct.db.domain.equipment;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * Created on 2007-02-21
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */


@Entity
@Table(name="LINKTBCONN")
@PrimaryKeyJoinColumn(name="LINKTBCONNID")
public class LinkConn extends BoardBoardConn {
	@Transient
	public TriggerBoard getTriggerBoard() {
        return (TriggerBoard) getCollectorBoard();
    }
	
	@Transient
    public void setTriggerBoard(TriggerBoard triggerBoard) {
        setCollectorBoard(triggerBoard);
    }
    
	@Transient
	public int getTriggerBoardInputNum() {
        return getCollectorBoardInputNum();
    }
    
    @Transient
    public int getOptoInputNum() {
        return getTriggerBoardInputNum()%3;
    }
    
    @Transient
    public void setTriggerBoardInputNum(int triggerBoardInputNum) {
        setCollectorBoardInputNum(triggerBoardInputNum);
    }
    
    @Transient
    public Chip getOpto() {
        return getTriggerBoard().getOptoForLinkNum(getTriggerBoardInputNum());
    }
    
    @Transient
    public Chip getSynCoder() {
        LinkBoard lb = (LinkBoard) getBoard();
        return lb.getSynCoder();
    }

    @Transient
    public String toString() {
        String str = ""; 
        str = "linkCon " + getBoard().getName() + " - " + getTriggerBoard().getName() + " optLinkInput " + getTriggerBoardInputNum() + " optoInput " + getOptoInputNum() +(isDisabled() ? " disabled, disconneted = " + disabled.getDisconnected() :  " enabled " );        
        return str;
    }
}
