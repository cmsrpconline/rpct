package rpct.db.domain.equipment;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Transient;

/**
 * Created on 2009-10-19
 *
 * @author Nikolay Darmenov
 * @version $Id$
 */
@Entity
@PrimaryKeyJoinColumn(name="RBCBOARDID")
public class RbcBoard extends Board {
    private Board masterBoard;
    private int i2cChannel;
    private List<RbcBoard> slaves;

	public RbcBoard() {
		super();
        setType(BoardType.RBCBOARD);
	}

	@Override
	public int maxChipCount() {
		return 1;
	}
	
    @ManyToOne()
    @JoinColumn(name="MASTERID")
    public Board getMasterBoard() {
        return masterBoard;
    }
    public void setMasterBoard(Board masterBoard) {
        this.masterBoard = masterBoard;
    }

    @Transient
    public RbcBoard getMaster() {
        return (RbcBoard) getMasterBoard();
    }

    @OneToMany(mappedBy="masterBoard")
    public List<RbcBoard> getSlaves() {
        return slaves;
    }
    public void setSlaves(List<RbcBoard> slaves) {
        this.slaves = slaves;
    }

    @Transient
    public boolean isMaster() {
    	return this.getId() == masterBoard.getId();
    }
    
    @Transient
    public Chip getChip() {
        final Set<Chip> chips = getChips();
        if (chips.size() != 1) {
            throw new IllegalStateException("Invalid number of chips (" + chips.size() + ") for " + toString());
        }
        return chips.iterator().next();
    }

	public void setI2cChannel(int i2cChannel) {
		this.i2cChannel = i2cChannel;
	}

	public int getI2cChannel() {
		return i2cChannel;
	}

    /**
     * @return If the board is a slave: null; If the board is a master - its slave (null if no slave's attached) 
     */
    @Transient
    public RbcBoard getSlave() {
    	if (slaves.size() > 2)
    		throw new IllegalArgumentException("[ERROR] Found " + slaves.size() + " slaves but expected no more than 1!");

        for (Board slave : slaves) {
        	if (this.getId() != slave.getId())
                return (RbcBoard) slave;
        }
        return null;
    }
    
    /**
     * 
     * @return The sector tower this RBC is attached to (odd number)
     * RBCFAKEBOARDs serve even sectors
     * Deduced from Board.Name 
     */
    @Transient
    public int sectorServed() {
    	int sector = -1;
    	
    	// Get the sector
    	// Board.name example: RBC_RB+1_S9
    	final String rbcName = getName();
    	Pattern pattern = Pattern.compile("^RBC.*_S(\\d+)$");
    	Matcher matcher = pattern.matcher(rbcName);
    	if (matcher.matches()) {
    		sector = Integer.parseInt(matcher.group(1));
    	}
    	else {
    		throw new IllegalArgumentException("Pattern " + pattern + " doesn't match RBC name " + rbcName);
    	}
    	
    	return sector;
    }
    
    private List<RbcTtuConn> linkConns;
    
    @Transient
    public List<RbcTtuConn> getLinkConns() {
    	final List<BoardBoardConn> bbcs = getConnsAsBoard();
    	linkConns = new ArrayList<RbcTtuConn>();
    	for (BoardBoardConn bbc: bbcs) {
    		linkConns.add((RbcTtuConn) bbc);
    	}
    	
    	return linkConns;
    }
}
