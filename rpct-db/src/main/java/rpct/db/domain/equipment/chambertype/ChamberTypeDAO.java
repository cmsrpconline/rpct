package rpct.db.domain.equipment.chambertype;

import rpct.db.DataAccessException;
import rpct.db.domain.DAO;

/**
 * Created on 2006-02-20
 *
 * @author Michal Pietrusinski & William Whitaker
 * @version $Id$
 */
public interface ChamberTypeDAO extends DAO {
	ChamberType getById(int id) throws DataAccessException;  
}
