package rpct.db.domain.equipment;

public enum CrateType {
    LINKBOX, TRIGGERCRATE, SORTERCRATE, CONTROLCRATE
}
