package rpct.db.domain.equipment.distributionboard;

import rpct.db.DataAccessException;
import rpct.db.domain.DAO;

/**
 * Created on 2006-02-20
 *
 * @author Michal Pietrusinski & William Whitaker
 * @version $Id$
 */
public interface DistributionBoardDAO extends DAO {
	DistributionBoard getById(int id) throws DataAccessException;  
}
