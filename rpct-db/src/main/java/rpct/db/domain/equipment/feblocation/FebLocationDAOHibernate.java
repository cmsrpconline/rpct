package rpct.db.domain.equipment.feblocation;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import rpct.db.DataAccessException;
import rpct.db.domain.DAOHibernate;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.chamberloaction.ChamberLocation;
import rpct.db.domain.hibernate.HibernateContext;

/**
 * Created on 2005-03-01
 * 
 * @author Michal Pietrusinski & William Whitaker
 * @version $Id$
 */
public class FebLocationDAOHibernate extends DAOHibernate implements FebLocationDAO {

    public FebLocationDAOHibernate(HibernateContext hibernateContext) {
        super(hibernateContext);
    }

    public FebLocation getById(int id) throws DataAccessException {
        return (FebLocation) getObject(FebLocation.class, id);
    }

/*    public List<FebLocation>  getFebs() throws DataAccessException {
    	return getObjects(FebLocation.class);
    }*/
    
    public List<FebLocation>  getFebs() throws DataAccessException {
        try {
            Criteria criteria = currentSession().createCriteria(FebLocation.class);
            criteria = criteria.add(Restrictions.gt("id", 0));
            criteria = criteria.add(Restrictions.isNotNull("i2cCbChannel"));
            
            criteria = criteria.addOrder(Order.asc("id"));
            return criteria.list();           
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }
    
    public List<FebLocation>  getFebsByChamberLocation(ChamberLocation chamberLocation) throws DataAccessException {
        try {
            Criteria criteria = currentSession().createCriteria(FebLocation.class);
            if (chamberLocation != null) {
                criteria = criteria.add(Restrictions.eq("chamberLocation", chamberLocation));
            }
            criteria = criteria.addOrder(Order.asc("id"));
            return criteria.list();           
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }
    public List<FebLocation> getByLocParameters(ChamberLocation chamberLocation, 
            String febLocalEtaPartition, Integer posInLocalEtaPartition) throws DataAccessException {
        try {
            Criteria criteria = currentSession().createCriteria(FebLocation.class);
            if (chamberLocation != null) {
                criteria = criteria.add(Restrictions.eq("chamberLocation", chamberLocation));
            }
            if (febLocalEtaPartition != null) {
                criteria = criteria.add(Restrictions.eq("febLocalEtaPartition", febLocalEtaPartition));
            }
            if (posInLocalEtaPartition != null) {
                criteria = criteria.add(Restrictions.eq("posInLocalEtaPartition", posInLocalEtaPartition));
            }
            criteria = criteria.addOrder(Order.asc("id"));
            return criteria.list();           
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }
}
