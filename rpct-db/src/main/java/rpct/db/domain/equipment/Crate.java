package rpct.db.domain.equipment;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import rpct.db.DataIntegrityException;
import rpct.db.domain.configuration.CrateDisabled;


@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="type", discriminatorType=DiscriminatorType.STRING)
@SequenceGenerator(name="seq", sequenceName="S_12_1_CRATE", allocationSize=1)
public abstract class Crate {
    private int id;
    private CrateType type;
    private String name;
    private String label;
    private List<Board> boards;
    private CrateDisabled disabled;
    
    @Id
    @Column(name="crateid")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    @Enumerated(EnumType.STRING)
    @Column(updatable=false, insertable=false)
    public CrateType getType() {
        return type;
    }
    public void setType(CrateType type) {
        this.type = type;
    }
    
    @OneToMany(mappedBy="crate", fetch=FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    //@OneToMany(mappedBy="crate")
    public List<Board> getBoards() {
    	return boards;
    }
	public void setBoards(List<Board> boards) {
		this.boards = boards;
	}


    //@OneToOne(mappedBy="crate", fetch=FetchType.LAZY)
    @OneToOne(mappedBy="crate")
	public CrateDisabled getDisabled() {
		return disabled;
	}
	public void setDisabled(CrateDisabled disabled) {
		this.disabled = disabled;
	}	
    
    @Transient
    public abstract int slotCount();
    
    private Board[] boardArray;
    /**
     * if the board.getPosition() is higer then the slotCount() it is not added to the returned Board[]
     * this is a case for the FebBoards, which are in the LBox, but has position > 21
     * @return 
     */    
    @Transient
    public Board[] getBoardArray() {
    	boolean warn = false;
    	if(boardArray == null) {
    		boardArray = new Board[slotCount()];
    		if (getBoards() != null) {
    			for (Board board : getBoards()) {
    				int index = board.getPosition();
    				if(index > slotCount()) {
    					warn = true;
    				}
    				else if (boardArray[index] != null) { 
    					throw new DataIntegrityException("Duplicate board in a slot. " + board.toString());
    				}                
    				else 
    					boardArray[index] = board;
    			}
    		}
    	}
    	/*if(warn)
    		System.out.println("getBoardArray():  board.getPosition() is outside the slotCount() for some boards, board will not be added to the Board[]");
*/    	return boardArray;
    }
    
    public String toString() {
        StringBuilder str = new StringBuilder(getClass().toString());
        str.append(" id = ").append(id).append(", type = ").append(type)
            .append(", name = ").append(name)
            .append(", label = ").append(label);
        return str.toString();
    }
    
    /*
    @Override
    public int hashCode() {
        final int PRIME = 31;
        int result = 1;
        result = PRIME * result + ((label == null) ? 0 : label.hashCode());
        result = PRIME * result + ((name == null) ? 0 : name.hashCode());
        result = PRIME * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Crate other = (Crate) obj;
        if (label == null) {
            if (other.label != null)
                return false;
        } else if (!label.equals(other.label))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }*/
    
    
}