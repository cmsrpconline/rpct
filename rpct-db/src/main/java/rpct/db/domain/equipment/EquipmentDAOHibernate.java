package rpct.db.domain.equipment;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BooleanType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;

import rpct.db.DataAccessException;
import rpct.db.domain.DAOHibernate;
import rpct.db.domain.equipment.ccsboard.CcsBoard;
import rpct.db.domain.equipment.chamberloaction.BarrelOrEndcap;
import rpct.db.domain.equipment.chamberloaction.ChamberLocation;
import rpct.db.domain.equipment.chamberstrip.ChamberStrip;
import rpct.db.domain.equipment.controlboard.ControlBoard;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.service.ChamStripAccessInfo;
import rpct.db.service.FebConnectorStrips;
import rpct.db.service.axis.FebConnectorStripsImplAxis;
import rpct.db.service.DTLineFebChipFlatInfo;
import rpct.db.service.axis.DTLineFebChipFlatInfoImplAxis;
import rpct.db.service.RbcInfo;
import rpct.util.StringUtils;

/**
 * Created on 2005-03-01
 * 
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class EquipmentDAOHibernate extends DAOHibernate implements EquipmentDAO {
	private final static Log log = LogFactory.getLog(EquipmentDAOHibernate.class);
	
    public EquipmentDAOHibernate(HibernateContext hibernateContext) {
        super(hibernateContext);
    }

    public Crate getCrateById(int id) throws DataAccessException {
        return (Crate) getObject(Crate.class, id);
    }

    public Crate getCrateByName(String name) throws DataAccessException {
        try {
            return (Crate) currentSession().createCriteria(Crate.class)
                .add(Restrictions.eq("name", name)).uniqueResult();           
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    public List<Crate> getCrates() throws DataAccessException {
        return getObjects(Crate.class);
    }

    public List<Crate> getCratesByType(CrateType crateType, boolean enabledOnly) throws DataAccessException {
        try {
            String enabledOnlyCond = enabledOnly ? "and not exists (from CrateDisabled cd where cd.crate = c)" : "";
            return currentSession().createQuery(
                    "from Crate c " +
                    "where c.type = :crateType " + 
                    enabledOnlyCond
            ).setParameter("crateType", crateType).list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    public Board getBoardById(int id) throws DataAccessException {
        return (Board) getObject(Board.class, id);    
    }
    public Board getBoardByName(String name) throws DataAccessException {
        try {
            return (Board) currentSession().createCriteria(Board.class)
                .add(Restrictions.eq("name", name)).uniqueResult();           
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }    
    }
    
    public List<Board> getBoardsByNameLike(String name, boolean enabledOnly) throws DataAccessException {
        try {
        	Criteria c = currentSession().createCriteria(Board.class)
            .add(Restrictions.like("name", name));
        	if (enabledOnly) {
        		c.add(Restrictions.isNull("disabled"));
        	}
            return c.list();           
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }    
    }
    
    public List<Board> getBoardsByType(BoardType boardType, boolean enabledOnly) throws DataAccessException {
        try {
            // String enabledOnlyCond = enabledOnly ? " and b.disabled is null " : ""; hibernate bug - does not work
            String enabledOnlyCond = enabledOnly ? "and not exists (from BoardDisabled bd where bd.board = b)" +
            		" and not exists (from CrateDisabled cd where cd.crate = b.crate) " : "";
            return currentSession().createQuery("from Board b where b.type = ? " + enabledOnlyCond)
            .setParameter(0, boardType).list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }  
    }

    public List<LinkBoard> getLinkBoardsWithoutChips() throws DataAccessException {
        try {
            return currentSession().createQuery("from LinkBoard lb where lb.chips.size = 0 ").list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }  
    }

	public Chip getChipById(int id) throws DataAccessException {
		return (Chip) getObject(Chip.class, id);
	}

    public List<Chip> getChips(List<Integer> ids, boolean loadBoards, boolean loadCrates) throws DataAccessException {
        try {
            String joins = "";
            if (loadBoards) {
                joins = "left join fetch c.board " +
                		"left join fetch c.board.disabled ";
            }
            if (loadCrates) {
                joins = joins + 
                        "left join fetch c.board.crate " +
                		"left join fetch c.board.crate.disabled ";
            }
            return currentSession().createQuery(
                    "from Chip c " +
                    "left join fetch c.disabled " + 
                    joins + 
                    "where c.id in (:ids) " )
            .setParameterList("ids", ids).list();          
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }   
    }
    
    public List<Chip> getChipsByType(ChipType chipType, boolean enabledOnly) throws DataAccessException {
        try {        	
        	String joins = "";            
        	/*joins = "left join fetch c.board " +
        			"left join fetch c.board.disabled ";


        	joins = joins + 
        			"left join fetch c.board.crate " +
        			"left join fetch c.board.crate.disabled ";*/
            
            //String enabledOnlyCond = enabledOnly ? " and chip.disabled is null " : ""; hibernate bug
        	String enabledOnlyCond = enabledOnly ? "and not exists (from ChipDisabled cd where cd.chip = c)" +
        			" and not exists (from BoardDisabled bd where bd.board = c.board) " +
        			" and not exists (from CrateDisabled cd where cd.crate = c.board.crate) " : "";
        	
            return currentSession().createQuery("from Chip c " + joins + " where c.type = ? " + enabledOnlyCond)
            .setParameter(0, chipType).list();
            //return currentSession().createCriteria(Chip.class)
            //    .add(Restrictions.eq("type", chipType)).list();           
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }   
    }
    
    public List<Chip> getChipsByTypeAndBoard(ChipType chipType, Board board, boolean enabledOnly) throws DataAccessException {
        try {
            //String enabledOnlyCond = enabledOnly ? " and chip.disabled = null " : ""; hibernate bug
        	String enabledOnlyCond = enabledOnly ? "and not exists (from ChipDisabled cd where cd.chip = c)" +
        			" and not exists (from BoardDisabled bd where bd.board = c.board) " +
        			" and not exists (from CrateDisabled cd where cd.crate = c.board.crate) " : "";
            return currentSession().createQuery(
                    "from Chip c " +
                    "where c.type = :type " +
                    "and c.board = :board " + enabledOnlyCond)
                    .setParameter("type", chipType)
                    .setEntity("board", board)
                    .list();       
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }   
    }
    
    public List<Chip> getChipsByTypeAndCrate(ChipType chipType, Crate crate, boolean enabledOnly) throws DataAccessException {
        try {
        	String enabledOnlyCond = enabledOnly ? "and not exists (from ChipDisabled cd where cd.chip = c)" +
        			" and not exists (from BoardDisabled bd where bd.board = c.board) " +
        			" and not exists (from CrateDisabled cd where cd.crate = c.board.crate) " : "";
            return currentSession().createQuery(
                    "from Chip c " +
                    "where c.type = :type " +
                    "and c.board.crate = :crate " + enabledOnlyCond)
                    .setParameter("type", chipType)
                    .setEntity("crate", crate)
                    .list();       
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }   
    }

    @SuppressWarnings("unchecked")
	public List<LinkConn> getLinkConns() throws DataAccessException {
        return (List<LinkConn>) getObjects(LinkConn.class); // Unchecked cast
    }
    public BoardBoardConn getLinkConnById(int id) throws DataAccessException {
        return (BoardBoardConn) getObject(BoardBoardConn.class, id);    
    }

    @SuppressWarnings("unchecked")
	public List<LinkConn> getLinkConns(TriggerBoard triggerBoard, boolean enabledOnly) throws DataAccessException {
    	//List<LinkConn> result = new ArrayList<LinkConn>();
    	try {
            String enabledOnlyCond = enabledOnly ? "and not exists (from LinkDisabled ld where ld.conn = lc) " 
                    : "";
            return  (List<LinkConn>) currentSession().createQuery(
                    "from LinkConn lc " +
                    "where lc.collectorBoard = :triggerBoard " + enabledOnlyCond)
                    .setEntity("triggerBoard", triggerBoard)
                    .list(); // Unchecked cast
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }   
    }
    
    
    @SuppressWarnings("unchecked")
	public List<BoardBoardConn> getBoardBoardConns(Board collectorBoard, boolean enabledOnly, boolean loadTransmitterCrate) throws DataAccessException {
    	//List<LinkConn> result = new ArrayList<LinkConn>();
    	log.info("getBoardBoardConns() started");
    	try {
            String enabledOnlyCond = enabledOnly ? "and not exists (from LinkDisabled ld where ld.conn = lc) " 
                    : "";
            
            String joins = "";
            if (loadTransmitterCrate) {
                joins = "inner join fetch lc.board " +
                		"left join fetch lc.board.disabled ";
                
                /*joins = joins + 
                	    "left join fetch lc.board.chips ";*/
           
                joins = joins + 
                        "inner join fetch lc.board.crate " +
                		"left join fetch lc.board.crate.disabled ";
                
                
                /* joins = joins + 
                		"inner join fetch lc.board.crate.boards ";
                
                joins = joins + 
                		"left join fetch lc.disabled ";*/
            }
            
            return  (List<BoardBoardConn>) currentSession().createQuery(
                    "from BoardBoardConn lc " + joins +
                    "where lc.collectorBoard = :collectorBoard " + enabledOnlyCond)
                    .setEntity("collectorBoard", collectorBoard)
                    .list(); // Unchecked cast
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }   
    }
    
    @SuppressWarnings("unchecked")
	private List<Integer> getConnsCollectorBoardInputNum(Board collectorBoard, boolean enabledOnly) throws DataAccessException{
        try {
            String enabledOnlyCond = enabledOnly ? "and not exists (from LinkDisabled ld where ld.conn = conn) " 
                    : "";
            return (List<Integer>) currentSession().createQuery(
                    "select conn.collectorBoardInputNum from BoardBoardConn conn " +
                    "where conn.collectorBoard = :collectorBoard " + enabledOnlyCond)
                    .setEntity("collectorBoard", collectorBoard)
                    .list(); // Unchecked cast      
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }   
    }

    public List<Integer> getLinkConnsTriggerBoardInputNum(TriggerBoard triggerBoard, boolean enabledOnly) throws DataAccessException{
    	return getConnsCollectorBoardInputNum(triggerBoard, enabledOnly);
    }

    public List<Integer> getRbcTtuConnsTtuBoardInputNum(TtuBoard ttuBoard, boolean enabledOnly) throws DataAccessException{
    	return getConnsCollectorBoardInputNum(ttuBoard, enabledOnly);
    }

    public LinkConn getLinkConnByTriggerBoardAndInputNum(TriggerBoard triggerBoard, int triggerBoardInputNum) throws DataAccessException {
        try {
            return (LinkConn) currentSession().createQuery(
                    "from LinkConn lc " +
                    "where lc.collectorBoard = :triggerBoard " +
                    "and lc.collectorBoardInputNum = :triggerBoardInputNum ")
                    .setEntity("triggerBoard", triggerBoard)
                    .setParameter("triggerBoardInputNum", triggerBoardInputNum)
                    .uniqueResult();       
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }   
    }

    public I2cCbChannel getI2cCbChannelByControlBoardAndCbChannel(ControlBoard controlBoard, int cbChannel) throws DataAccessException {
        try {
            return (I2cCbChannel) currentSession().createCriteria(I2cCbChannel.class)
                .add(Restrictions.eq("controlBoard", controlBoard))
                .add(Restrictions.eq("cbChannel", cbChannel))
                .uniqueResult();           
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }


    public List<LinkBox> getLinkBoxesByFec(CcsBoard ccsBoard, int fecPosition) throws DataAccessException {
        try {
            return currentSession().createQuery(
                    "from LinkBox lbb " +
                    "where exists (" +
                    "from ControlBoard as cb " +
                    "where cb.ccsBoard = ? and cb.fecPosition = ? " +
                    "and cb.crate = lbb" +
                    ")")
                    .setEntity(0, ccsBoard)
                    .setInteger(1, fecPosition)
                    .list();       
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    public List<LinkBox> getLinkBoxesByChamberLocation(Integer diskOrWheel, Integer layer, Integer sector, String subsector, BarrelOrEndcap barrelOrEndcap) throws DataAccessException {

        String diskOrWheelCond = (diskOrWheel == null) ? "" : "and feb.chamberLocation.diskOrWheel = :diskOrWheel ";
        String layerCond = (layer == null) ? "" : "and feb.chamberLocation.layer = :layer ";
        String sectorCond = (sector == null) ? "" : "and feb.chamberLocation.sector = :sector ";

        subsector = StringUtils.trimToNull(subsector);
        if ("Endcap".equals(barrelOrEndcap) && subsector != null) {
            throw new IllegalArgumentException(
                    "subsector cannot be used for Endcap");
        }
        String subsectorCond = (subsector == null) ? "" : "and feb.chamberLocation.subsector = :subsector ";     
        try {
            Query q = currentSession().createQuery(
                    "select distinct feb.i2cCbChannel.controlBoard.crate " +
                    "from FebLocation as feb " +
                    "where feb.chamberLocation.barrelOrEndcap = :barrelOrEndcap " +
                    diskOrWheelCond +
                    layerCond +
                    sectorCond +
                    subsectorCond +
                    ")");
            q.setParameter("barrelOrEndcap", barrelOrEndcap);
            if (diskOrWheel != null) {
                q.setInteger("diskOrWheel", diskOrWheel);                
            }
            if (layer != null) {
                q.setInteger("layer", layer);                
            }
            if (sector != null) {
                q.setInteger("sector", sector);                
            }
            if (subsector != null) {
                q.setString("subsector", subsector);                
            }
            return q.list();       
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    public List<ChamberStrip> getChamberStripsByChamberLocation(
            int diskOrWheel, int layer, int sector, String subsector,
            BarrelOrEndcap barrelOrEndcap) throws DataAccessException {
        try {
            subsector = StringUtils.trimToNull(subsector);
            String subsectorQuery = (subsector == null) ? "" : " and strip.febConnector.febLocation.chamberLocation.subsector = :subsector";
            Query q = currentSession().createQuery(
                    "from ChamberStrip strip " +
                    "inner join fetch strip.febConnector febConnector " +
                    "inner join fetch febConnector.febLocation febLocation " +
                    "inner join fetch febLocation.chamberLocation chamberLocation " +
                    "inner join fetch febLocation.linkBoard linkBoard " +
                    "inner join fetch febLocation.i2cCbChannel i2cCbChannel " +
                    "inner join fetch i2cCbChannel.controlBoard controlBoard " +
                    "where strip.febConnector.febLocation.chamberLocation.diskOrWheel = :diskOrWheel " +
                    "and strip.febConnector.febLocation.chamberLocation.layer = :layer " +
                    "and strip.febConnector.febLocation.chamberLocation.sector = :sector " +
                    "and strip.febConnector.febLocation.chamberLocation.barrelOrEndcap = :barrelOrEndcap " +
                    subsectorQuery);
            
            q.setParameter("diskOrWheel", diskOrWheel);
            q.setParameter("layer", layer);
            q.setParameter("sector", sector);
            q.setParameter("barrelOrEndcap", barrelOrEndcap);
            if (subsector != null) {
                q.setParameter("subsector", subsector);
            }
            return q.list();       
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }   
    }

    public List<ChamStripAccessInfo> getChamStripAccessInfosByChamberLocation(
            int diskOrWheel, int layer, int sector, String subsector,
            BarrelOrEndcap barrelOrEndcap) throws DataAccessException {
        try {
            subsector = StringUtils.trimToNull(subsector);
            String subsectorQuery = (subsector == null) ? "" : " and strip.febConnector.febLocation.chamberLocation.subsector = :subsector";
            Query q = currentSession().createQuery(
                    "select new rpct.db.service.axis.ChamStripAccessInfoImplAxis(" +
                    "chamberLocation.chamberLocationName, linkBoard.id, app.instance, " +
                    "chip.id, febLocation.id, febConnector.linkBoardInputNum, " +
                    "febLocation.febLocalEtaPartition, febLocation.i2cLocalNumber, " +
                    "strip.cableChannelNum, strip.chamberStripNumber, strip.id) " +
                    "from ChamberStrip strip " +
                    "join strip.febConnector febConnector " +
                    "join febConnector.febLocation febLocation " +
                    "join febLocation.chamberLocation chamberLocation " +
                    "join febLocation.linkBoard linkBoard " +
                    "join febLocation.i2cCbChannel i2cCbChannel " +
                    "join i2cCbChannel.controlBoard controlBoard, " +
                    "XdaqAppLBoxAccess app, " +
                    "Chip chip " +
                    "where strip.febConnector.febLocation.chamberLocation.diskOrWheel = :diskOrWheel " +
                    "and strip.febConnector.febLocation.chamberLocation.layer = :layer " +
                    "and strip.febConnector.febLocation.chamberLocation.sector = :sector " +
                    "and strip.febConnector.febLocation.chamberLocation.barrelOrEndcap = :barrelOrEndcap " +
                    "and size(linkBoard.chips) = 1 " +
                    "and chip.board = linkBoard " +
                    "and app.ccsBoard = controlBoard.ccsBoard " +
                    "and app.fecPosition = controlBoard.fecPosition " +
                    subsectorQuery);
            
            q.setParameter("diskOrWheel", diskOrWheel);
            q.setParameter("layer", layer);
            q.setParameter("sector", sector);
            q.setParameter("barrelOrEndcap", barrelOrEndcap);
            if (subsector != null) {
                q.setParameter("subsector", subsector);
            }
            return q.list();       
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }   
    }

    public List<FebConnectorStrips> getFebConnectorStrips(String xdaqExecutiveHost
                                                          , int xdaqExecutivePort) throws DataAccessException {
        try {
            Query q = currentSession()
                .createSQLQuery("SELECT chamberstrip.fc_febconnectorid AS id, feblocation.lb_linkboardid AS linkBoardId, febconnector.linkboardinputnum AS linkBoardInput, febboard.febboardid AS febBoardId, DECODE(chamberlocation.barrelorendcap, 'Barrel', 0, DECODE(chamberstrip.chamberstripnumber, 16, 1, 17, 1, 0)) AS febBoardConnector, chamberlocation.chamberlocationname AS location, feblocation.feblocaletapartition AS partition, febconnector.febconnectornum AS rollConnector, csmin.pins AS pins, chamberstrip.chamberstripnumber AS firstStrip, DECODE(SIGN(csmax.strip - chamberstrip.chamberstripnumber), 1, 1, -1) AS slope" +
                                " FROM chamberstrip" +
                                " INNER JOIN (SELECT fc_febconnectorid AS febconnectorid, MIN(cablechannelnum) AS pin, SUM(POWER(2, chamberstrip.cablechannelnum-1)) AS pins FROM chamberstrip group by fc_febconnectorid)" +
                                " csmin ON chamberstrip.fc_febconnectorid=csmin.febconnectorid AND chamberstrip.cablechannelnum=csmin.pin" +
                                " INNER JOIN(SELECT chamberstrip.fc_febconnectorid AS febconnectorid, chamberstrip.chamberstripnumber AS strip" +
                                "   FROM chamberstrip" +
                                "   INNER JOIN (SELECT fc_febconnectorid AS febconnectorid, MAX(cablechannelnum) AS pin FROM chamberstrip group by fc_febconnectorid)" +
                                "   csmax ON chamberstrip.fc_febconnectorid=csmax.febconnectorid AND chamberstrip.cablechannelnum=csmax.pin)" +
                                " csmax ON chamberstrip.fc_febconnectorid=csmax.febconnectorid" +
                                " INNER JOIN febconnector ON (chamberstrip.fc_febconnectorid = febconnector.febconnectorid)" +
                                " INNER JOIN febboard ON (febconnector.fl_feblocationid = febboard.fl_feblocationid)" +
                                " INNER JOIN feblocation ON (febconnector.fl_feblocationid = feblocation.feblocationid)" +
                                " INNER JOIN chamberlocation ON (feblocation.cl_chamberlocationid = chamberlocation.chamberlocationid)" +
                                " INNER JOIN i2ccbchannel ON (feblocation.cbc_i2ccbchannelid=i2ccbchannel.i2ccbchannelid)" +
                                " INNER JOIN controlboard ON (i2ccbchannel.cb_controlboardid = controlboard.controlboardid)" +
                                " INNER JOIN xdaqapplboxaccess ON (controlboard.ccsboard_ccsboardid=xdaqapplboxaccess.ccs_ccsboardid AND controlboard.fecposition=xdaqapplboxaccess.fecposition)" +
                                " INNER JOIN xdaqapplication ON (xdaqapplboxaccess.xdaqapplboxaccessid=xdaqapplication.xdaqapplicationid)" +
                                " INNER JOIN xdaqexecutive ON (xdaqapplication.xe_xdaqexecutiveid=xdaqexecutive.xdaqexecutiveid)" +
                                " WHERE xdaqexecutive.host = :xdaqhost AND xdaqexecutive.port = :xdaqport" +
                                " ORDER BY feblocation.lb_linkboardid, febconnector.linkboardinputnum, febboard.febboardid")
                .addScalar("id", IntegerType.INSTANCE)
                .addScalar("linkBoardId", IntegerType.INSTANCE)
                .addScalar("linkBoardInput", IntegerType.INSTANCE)
                .addScalar("febBoardId", IntegerType.INSTANCE)
                .addScalar("febBoardConnector", IntegerType.INSTANCE)
                .addScalar("location", StringType.INSTANCE)
                .addScalar("partition", StringType.INSTANCE)
                .addScalar("rollConnector", IntegerType.INSTANCE)
                .addScalar("pins", IntegerType.INSTANCE)
                .addScalar("firstStrip", IntegerType.INSTANCE)
                .addScalar("slope", IntegerType.INSTANCE)
                .setResultTransformer(Transformers.aliasToBean(FebConnectorStripsImplAxis.class))
                .setString("xdaqhost", xdaqExecutiveHost)
                .setInteger("xdaqport", xdaqExecutivePort);

        return q.list();

        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }   
    }

    public List<DTLineFebChipFlatInfo> getDTLineFebChipFlatInfo(int wheel) throws DataAccessException {
        try {
            Query q = currentSession()
                .createSQLQuery("SELECT"
                                + " chip.chipid AS chipId"
                                + " , febboard.febboardid AS boardId"
                                + " , board.name AS name"
                                + " , febLocation.i2clocalnumber AS i2cAddress"
                                + " , chip.position AS position"
                                + " , DECODE(chipdisabled.chipdisabledid, NULL, 0, 1) AS chipDisabled"
                                + " , DECODE(boarddisabled.boarddisabledid, NULL, 0, 1) AS boardDisabled"
                                + " , chamberlocation.chamberlocationname AS location"
                                + " , feblocation.feblocaletapartition AS partition"
                                // + " , feblocation.feblocationid AS feblocationid"
                                + " FROM febboarddtcontrolled"
                                + " LEFT JOIN boarddisabled ON boarddisabled.board_boardid=febboarddtcontrolled.board_boardid"
                                + " INNER JOIN febboard ON febboard.febboardid=febboarddtcontrolled.board_boardid"
                                + " INNER JOIN board ON board.boardid=febboard.febboardid"
                                + " INNER JOIN feblocation ON feblocation.feblocationid=febboard.fl_feblocationid"
                                + " INNER JOIN chamberlocation ON chamberlocation.chamberlocationid=feblocation.cl_chamberlocationid"
                                + " INNER JOIN chip ON chip.board_boardid=febboarddtcontrolled.board_boardid"
                                + " LEFT JOIN chipdisabled ON chipdisabled.chip_chipid=chip.chipid"
                                + " WHERE chamberlocation.diskorwheel = :wheel"
                                + " ORDER BY board.crate_crateid, board.position, chip.position")
                .addScalar("chipId", IntegerType.INSTANCE)
                .addScalar("boardId", IntegerType.INSTANCE)
                .addScalar("name", StringType.INSTANCE)
                .addScalar("i2cAddress", IntegerType.INSTANCE)
                .addScalar("position", IntegerType.INSTANCE)
                .addScalar("chipDisabled", BooleanType.INSTANCE)
                .addScalar("boardDisabled", BooleanType.INSTANCE)
                .addScalar("location", StringType.INSTANCE)
                .addScalar("partition", StringType.INSTANCE)
                // .addScalar("febLocationId", IntegerType.INSTANCE)
                .setResultTransformer(Transformers.aliasToBean(DTLineFebChipFlatInfoImplAxis.class))
                .setInteger("wheel", wheel);

        return q.list();

        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }   
    }

    public List<RbcInfo> getAllRbcInfo() throws DataAccessException {
        try {
            return currentSession().createQuery(
                    "select new rpct.db.service.axis.RbcInfoImplAxis( " +
                    "app.xdaqExecutive.host, app.xdaqExecutive.port, app.instance, c.name)" +
                    "from Crate c, " +
                    "ControlBoard cb, " +
                    "XdaqAppLBoxAccess app " +
                    "where cb.crate = c " +
                    "and cb.position = 10 " +
                    "and (c.name like 'LBB_RB%_S1' " +
                    "or c.name like 'LBB_RB%_S3' " +
                    "or c.name like 'LBB_RB%_S5' " +
                    "or c.name like 'LBB_RB%_S7' " +
                    "or c.name like 'LBB_RB%_S9' " +
                    "or c.name like 'LBB_RB%_S11') " +
                    "and app.ccsBoard = cb.ccsBoard " +
                    "and app.fecPosition = cb.fecPosition ").list();       
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }   
    }

    public Map<String, List<LinkBox>> getLinkBoxesByTower(EquipmentDAO equipmentDAO, boolean enabledOnly) throws DataAccessException {
    	Map<String, List<LinkBox>> linkBoxesByTower = new HashMap<String, List<LinkBox>>();
    	List<Crate> lboxes = equipmentDAO.getCratesByType(CrateType.LINKBOX, enabledOnly);
    	for (Crate lbox : lboxes) {
    		String towerName = ((LinkBox) lbox).getTowerName();
    		List<LinkBox> linkBoxesInTower = linkBoxesByTower.get(towerName);
    		if (linkBoxesInTower == null) {
    			linkBoxesInTower = new ArrayList<LinkBox>();
    			linkBoxesByTower.put(towerName, linkBoxesInTower);
    		}
    		linkBoxesInTower.add((LinkBox) lbox);
    	}
    	return linkBoxesByTower;
    }

    /* (non-Javadoc)
     * @see rpct.db.domain.equipment.EquipmentDAO#getFebBoards()
     * is 2 times faster then getBoardsByType
     */
    @SuppressWarnings("unchecked")
	public List<FebBoard> getFebBoards() throws DataAccessException {
    	log.info("getFebBoards() started");
    	try {   	    
            String joins = "";
            joins = 
          /*"left join fetch feb.febLocation " +
            "left join fetch feb.chips as ch" +
            "left join fetch ch.disabled" +
            "left join fetch feb.febLocation.chamberLocation " +
            "left join fetch feb.febLocation.i2cCbChannel " +
            "left join fetch feb.febLocation.linkBoard " +
            "left join fetch feb.febLocation.febConnectors "*/
            
            "inner join fetch feb.febLocation " +
            "left join fetch feb.febLocation.febBoard " +
            //"inner join fetch feb.chips " +""
            //"left join fetch feb.crate " +
            "left join fetch feb.disabled " +
            "inner join fetch feb.febLocation.chamberLocation " +
            //"left join fetch feb.febLocation.chamberLocation.chamberType " +            
            "inner join fetch feb.febLocation.i2cCbChannel "
            //"left join fetch feb.febLocation.linkBoard " +
            //"left join fetch feb.febLocation.febConnectors "           
            ; 
                          
            return  (List<FebBoard>) currentSession().createQuery(
                    "from FebBoard feb " + joins)
                    .list(); // Unchecked cast
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }   
    }
    
    @SuppressWarnings("unchecked")
	public FebBoard getFebBoard(int id) throws DataAccessException {
    	//log.info("getFebBoard(int id) started");
    	try {   	    
            String joins = "";
            joins = 
            "inner join fetch feb.febLocation " +
            //"left join fetch feb.febLocation.febBoard " +
            "inner join fetch feb.chips " +
            //"left join fetch feb.crate " +
            "left join fetch feb.disabled " +
            "inner join fetch feb.febLocation.chamberLocation " +
            //"left join fetch feb.febLocation.chamberLocation.chamberType " +            
            "inner join fetch feb.febLocation.i2cCbChannel "
            //"left join fetch feb.febLocation.linkBoard " +
            //"left join fetch feb.febLocation.febConnectors "           
            ;                
            Object obj = currentSession().createQuery(
                    "from FebBoard feb " + joins + " where feb.id = :id").setInteger("id", id).uniqueResult(); // Unchecked cast           
            return  (FebBoard) obj;
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }   
    }

}
