package rpct.db.domain.equipment.controlboard;

import rpct.db.DataAccessException;
import rpct.db.domain.DAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;

/**
 * Created on 2005-03-01
 * 
 * @author Michal Pietrusinski & William Whitaker
 * @version $Id$
 */
public class ControlBoardDAOHibernate extends DAOHibernate implements ControlBoardDAO {

    public ControlBoardDAOHibernate(HibernateContext hibernateContext) {
        super(hibernateContext);
    }

	public ControlBoard getById(int id) throws DataAccessException {
        return (ControlBoard) getObject(ControlBoard.class, id);
	}
}
