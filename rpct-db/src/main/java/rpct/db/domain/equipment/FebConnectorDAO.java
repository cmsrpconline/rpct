package rpct.db.domain.equipment;

import java.util.List;

import rpct.db.DataAccessException;
import rpct.db.domain.DAO;
import rpct.db.domain.equipment.feblocation.FebLocation;

/**
 * Created on 2006-12-01
 *
 * @author William Whitaker & Michal Pietrusinski
 * @version $Id$
 */
public interface FebConnectorDAO extends DAO {
    List<FebConnector> getConnectionsByFebLoc(FebLocation febLocation) throws DataAccessException;  
}
