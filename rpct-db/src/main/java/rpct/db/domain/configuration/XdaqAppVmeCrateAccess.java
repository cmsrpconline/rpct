package rpct.db.domain.configuration;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import rpct.db.domain.equipment.Crate;


/**
 * Created on 2006-07-14
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */

@Entity
@PrimaryKeyJoinColumn(name="XDAQAPPVMECRATEACCESSID")
public class XdaqAppVmeCrateAccess extends XdaqApplication {
    private Crate crate;
    private int pciSlot;
    private int posInVmeChain;


    public XdaqAppVmeCrateAccess() {
        super();
        setClassName("XdaqLBoxAccess");
    }

    @ManyToOne
    @JoinColumn(name="CRATE_CRATEID")
    public Crate getCrate() {
        return crate;
    }
    public void setCrate(Crate crate) {
        this.crate = crate;
    }   

    public int getPciSlot() {
        return pciSlot;
    }
    public void setPciSlot(int pciSlot) {
        this.pciSlot = pciSlot;
    }

    public int getPosInVmeChain() {
        return posInVmeChain;
    }
    public void setPosInVmeChain(int posInVmeChain) {
        this.posInVmeChain = posInVmeChain;
    }    
    
}
