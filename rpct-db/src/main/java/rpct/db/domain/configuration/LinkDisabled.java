package rpct.db.domain.configuration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import rpct.db.domain.equipment.BoardBoardConn;
import rpct.db.domain.equipment.LinkConn;
/**
 * Created on 2007-05-21
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */


@Entity
@SequenceGenerator(name="seq", sequenceName="S_32_1_LINKDISABLED", allocationSize=1)
public class LinkDisabled {
    private int id;
    private BoardBoardConn conn;
    private int disconnected;
    
	@Id
    @Column(name="LINKDISABLEDID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    @ManyToOne
    @JoinColumn(name="BBC_BOARDBOARDCONNID")
    public BoardBoardConn getConn() {
        return conn;
    }
    public void setConn(BoardBoardConn linkConn) {
        this.conn = linkConn;
    }

    public int getDisconnected() {
		return disconnected;
	}
	public void setDisconnected(int disconnected) {
		this.disconnected = disconnected;
	}
    
    @Transient
    public LinkConn getLinkConn() {
        return (LinkConn) getConn();
    }
    @Transient
    public void setLinkConn(BoardBoardConn linkConn) {
        setConn(linkConn);
    }
}
