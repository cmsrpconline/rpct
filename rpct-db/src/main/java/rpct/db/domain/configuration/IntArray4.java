package rpct.db.domain.configuration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

/**
 * Created on 2007-01-24
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */


@Entity
@SequenceGenerator(name="seq", sequenceName="S_30_1_INTARRAY4", allocationSize=1)
public class IntArray4 extends IntArray {
    private int id;
    private int value0;
    private int value1;
    private int value2;
    private int value3;

    public IntArray4() {
        super();
    }    

    public IntArray4(int[] values) {
        super();
        setValues(values);
    }    
    
    @Id
    @Column(name="INTARRAY4ID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    public int getValue0() {
        return value0;
    }
    public void setValue0(int value0) {
        this.value0 = value0;
    }
    public int getValue1() {
        return value1;
    }
    public void setValue1(int value1) {
        this.value1 = value1;
    }
    public int getValue2() {
        return value2;
    }
    public void setValue2(int value2) {
        this.value2 = value2;
    }
    public int getValue3() {
        return value3;
    }
    public void setValue3(int value3) {
        this.value3 = value3;
    }

    @Transient
    public int getValue(int index) {
        switch(index) {
        case 0: return value0;
        case 1: return value1;
        case 2: return value2;
        case 3: return value3;
        default: throw new IllegalArgumentException("index out of range");           
        }
    }

    @Transient
    public void setValue(int index, int value) {
        switch(index) {
        case 0: 
            value0 = value;
            break;        
        case 1: 
            value1 = value;
            break;        
        case 2: 
            value2 = value;
            break;        
        case 3: 
            value3 = value;
            break;  
        default: 
            throw new IllegalArgumentException("index out of range");           
        }
    }
    
    @Transient
    public int[] getValues() {
        return new int[] {
                value0,
                value1,
                value2,
                value3
        };
    }
    
    @Transient
    public void setValues(int[] values) {
        if (values == null) {
            throw new NullPointerException("values == null");
        }
        if (values.length != size()) {
            throw new IllegalArgumentException("values.length != " + size());
        }
        value0 = values[0];
        value1 = values[1];
        value2 = values[2];
        value3 = values[3];
    }
    
    @Transient
    public int size() {
        return 4;
    }
}
