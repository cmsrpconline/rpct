package rpct.db.domain.configuration;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import rpct.db.domain.equipment.ccsboard.CcsBoard;


/**
 * Created on 2006-07-14
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */

@Entity
@PrimaryKeyJoinColumn(name="XDAQAPPLBOXACCESSID")
public class XdaqAppLBoxAccess extends XdaqApplication {
    private CcsBoard ccsBoard;
    private int fecPosition;
    private int pciSlot;
    private int posInVmeChain;


    public XdaqAppLBoxAccess() {
        super();
        setClassName("XdaqLBoxAccess");
    }

    @ManyToOne
    @JoinColumn(name="CCS_CCSBOARDID")
    public CcsBoard getCcsBoard() {
        return ccsBoard;
    }
    public void setCcsBoard(CcsBoard ccsBoard) {
        this.ccsBoard = ccsBoard;
    }

    public int getFecPosition() {
        return fecPosition;
    }
    public void setFecPosition(int fecPosition) {
        this.fecPosition = fecPosition;
    }

    public int getPciSlot() {
        return pciSlot;
    }
    public void setPciSlot(int pciSlot) {
        this.pciSlot = pciSlot;
    }

    public int getPosInVmeChain() {
        return posInVmeChain;
    }
    public void setPosInVmeChain(int posInVmeChain) {
        this.posInVmeChain = posInVmeChain;
    }
    
}
