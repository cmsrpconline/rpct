package rpct.db.domain.configuration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import rpct.db.domain.equipment.Chip;
/**
 * Created on 2006-09-01
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */


@Entity
@SequenceGenerator(name="seq", sequenceName="S_23_1_CHIPDISABLED", allocationSize=1)
public class ChipDisabled {
    private int id;
    private Chip chip;
    
    @Id
    @Column(name="CHIPDISABLEDID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    @OneToOne(optional=false) //it is fetch=FetchType.EAGER by default
    @JoinColumn(name="CHIP_CHIPID")
    public Chip getChip() {
        return chip;
    }
    public void setChip(Chip chip) {
        this.chip = chip;
    }
}
