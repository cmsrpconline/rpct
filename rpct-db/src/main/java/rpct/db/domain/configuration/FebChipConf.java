package rpct.db.domain.configuration;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import rpct.db.domain.equipment.ChipType;


/**
 * Created on 2009-12-07
 *
 * @author Nikolay Darmenov
 * @version $Id$
 */

@Entity
@PrimaryKeyJoinColumn(name="FEBCHIPCONFID")
@Table(name="FEBCHIPCONF")
public class FebChipConf extends StaticConfiguration {
    private int vth;
    private int vthOffset;
    private int vmon;
    private int vmonOffset;

    /*
    "VTH" NUMBER(11) NOT NULL,
    "VTHOFFSET" NUMBER(11) NOT NULL,
    "VMON" NUMBER(11) NOT NULL,
    "VMONOFFSET" NUMBER(11) NOT NULL,
    "DTLINE" NUMBER(1) NOT NULL,
     */

    public FebChipConf() {
        super();
    }    

    public FebChipConf(int vth, int vthOffset, int vmon, int vmonOffset) {
		super();
		this.vth = vth;
		this.vthOffset = vthOffset;
		this.vmon = vmon;
		this.vmonOffset = vmonOffset;
	}
    

    public FebChipConf(FebChipConf febChipConf) {
		super();
		this.vth = febChipConf.vth;
		this.vthOffset = febChipConf.vthOffset;
		this.vmon = febChipConf.vmon;
		this.vmonOffset = febChipConf.vmonOffset;
	}

	public int getVth() {
		return vth;
	}
	public void setVth(int vth) {
		this.vth = vth;
	}

	@Transient
	public void setVth_mV(int treshold) {
		this.vth = mV2dac(treshold);
	}
	
	public int getVthOffset() {
		return vthOffset;
	}
	public void setVthOffset(int vthOffset) {
		this.vthOffset = vthOffset;
	}
	
	@Transient
	public void setVmon_mV(int offset) {
		this.vmon = mV2dac(offset);
	}

	public int getVmon() {
		return vmon;
	}
	public void setVmon(int vmon) {
		this.vmon = vmon;
	}

	public int getVmonOffset() {
		return vmonOffset;
	}
	public void setVmonOffset(int vmonOffset) {
		this.vmonOffset = vmonOffset;
	}

	@Override
    @Transient
    public ChipType getChipType() {
        return ChipType.FEBCHIP;
    }

	@Transient
	public static double dac2mV(int dacValue) {
		return dacValue * 2.5 / 1.024;
	}
	
	@Transient
	public static int mV2dac(double mVvalue) {
		return (int) Math.round(mVvalue / 2.5 * 1.024);
	}
	
	@Transient
	public int getVth_mV() {
		return (int)Math.round(dac2mV(vth));
	}

	@Transient
	public int getVmon_mV() {
		return (int)Math.round(dac2mV(vmon));
	}
	
	@Transient
	public int getVmonOffset_mV() {
		return (int)Math.round(dac2mV(vmonOffset));
	}
	
	@Transient
	public int getVthOffset_mV() {
		return (int)Math.round(dac2mV(vthOffset));
	}
	
	@Transient
	public void setVmonOffset_mV(int offset) {
		this.vmonOffset = mV2dac(offset);
	}
	
	@Transient
	public void setVthOffset_mV(int offset) {
		this.vthOffset = mV2dac(offset);
	}
	
	public String toString() {
		return "FebChipConf"
		+ "\n   * vth        = " + vth + "\t= " +  getVth_mV() + " mV"
		+ "\n   * vthOffset  = " + vthOffset + "\t= " + dac2mV(vthOffset) + " mV"
		+ "\n   * vmon       = " + vmon + "\t= " + dac2mV(vmon) + " mV"
		+ "\n   * vmonOffset = " + vmonOffset + "\t= " + dac2mV(vmonOffset) + " mV"
		+ "\n";
	}
}
