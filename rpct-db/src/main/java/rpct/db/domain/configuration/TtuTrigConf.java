package rpct.db.domain.configuration;

import java.util.Arrays;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import rpct.db.domain.equipment.ChipType;


/**
 * Created on 2009-11-18
 *
 * @author Nikolay Darmenov
 * @version $Id$
 */

@Entity
@PrimaryKeyJoinColumn(name="TTUTRIGCONFID")
@Table(name="TTUTRIGCONF")
public class TtuTrigConf extends StaticConfiguration {
    public static final int REC_MUX_SIZE = 6; // (Ok. 6*8=48 ex. recMuxClk90: word 48)
	private byte[] recMuxClk90 = new byte[REC_MUX_SIZE];
    private byte[] recMuxClkInv = new byte[REC_MUX_SIZE];
    private byte[] recMuxRegAdd = new byte[REC_MUX_SIZE];
    
    private IntArray12 recMuxDelay =  new IntArray12();
    private IntArray12 recDataDelay =  new IntArray12();
    private int taTrgDelay;
    private int taTrgToff;
    private int taTrgSelect;
    private int taTrgCntrGate;
    private IntArray24 taSectorDelay =  new IntArray24();
    private IntArray12 taForceLogic0 =  new IntArray12();
    private IntArray12 taForceLogic1 =  new IntArray12();
    private IntArray12 taForceLogic2 =  new IntArray12();
    private IntArray12 taForceLogic3 =  new IntArray12();
    private IntArray4 trigConfig =  new IntArray4();
    private IntArray24 trigMask =  new IntArray24();
    private IntArray24 trigForce =  new IntArray24();
    private int trigMajority;
    private IntArray12 sectorMajority =  new IntArray12();
    private int sectorTrigMask;
    private int sectorThreshold;
    private int towerThreshold;
    private int wheelThreshold;
    private int sectTrgSel;
    private IntArray16 taConfig2 =  new IntArray16();

    /*
    "RECMUXCLK90" RAW(6) NOT NULL,
    "RECMUXCLKINV" RAW(6) NOT NULL,
    "RECMUXREGADD" RAW(6) NOT NULL,
    "IA12_RECMUXDELAYID" NUMBER(11,0) NOT NULL, 
    "IA12_RECDATADELAYID" NUMBER(11,0) NOT NULL,
    "TATRGDELAY" NUMBER(11,0) NOT NULL,
    "TATRGTOFF" NUMBER(11,0) NOT NULL,
    "TATRGSELECT" NUMBER(11,0) NOT NULL,
    "TATRGCNTRGATE" NUMBER(11,0) NOT NULL,
    "IA24_TASECTORDELAYID" NUMBER(11,0) NOT NULL, 
    "IA12_TAFORCELOGIC0ID" NUMBER(11,0) NOT NULL,
    "IA12_TAFORCELOGIC1ID" NUMBER(11,0) NOT NULL,
    "IA12_TAFORCELOGIC2ID" NUMBER(11,0) NOT NULL,
    "IA12_TAFORCELOGIC3ID" NUMBER(11,0) NOT NULL,
    "IA4_TRIGCONFIGID" NUMBER(11,0) NOT NULL,
    "IA24_TRIGMASKID" NUMBER(11,0) NOT NULL,
    "IA24_TRIGFORCEID" NUMBER(11,0) NOT NULL,
    "TRIGMAJORITY" NUMBER(11,0) NOT NULL,
    "IA12_SECTORMAJORITYID" NUMBER(11,0) NOT NULL,
    "SECTORTRIGMASK" NUMBER(11,0) NOT NULL,
    "SECTORTHRESHOLD" NUMBER(11,0) NOT NULL,
    "TOWERTHRESHOLD" NUMBER(11,0) NOT NULL,
    "WHEELTHRESHOLD" NUMBER(11,0) NOT NULL,
    "SECTTRGSEL" NUMBER(11,0) NOT NULL,
    "IA16_TACONFIG2ID" NUMBER(11,0) NOT NULL,
     */

    public TtuTrigConf() {
        super();
    }    

    public TtuTrigConf(TtuTrigConf conf) {
    	super();
    	
    	this.recMuxClk90 = conf.recMuxClk90.clone();
    	this.recMuxClkInv = conf.recMuxClkInv.clone();
    	this.recMuxRegAdd = conf.recMuxRegAdd.clone();
    	this.recMuxDelay = new IntArray12(conf.recMuxDelay.getValues().clone());
    	this.recDataDelay = new IntArray12(conf.recDataDelay.getValues().clone());
    	this.taTrgDelay = conf.taTrgDelay;
    	this.taTrgToff = conf.taTrgToff;
    	this.taTrgSelect = conf.taTrgSelect;
    	this.taTrgCntrGate = conf.taTrgCntrGate;
    	this.taSectorDelay = new IntArray24(conf.taSectorDelay.getValues().clone());
    	this.taForceLogic0 = new IntArray12(conf.taForceLogic0.getValues().clone());
    	this.taForceLogic1 = new IntArray12(conf.taForceLogic1.getValues().clone());
    	this.taForceLogic2 = new IntArray12(conf.taForceLogic2.getValues().clone());
    	this.taForceLogic3 = new IntArray12(conf.taForceLogic3.getValues().clone());
    	this.trigConfig = new IntArray4(conf.trigConfig.getValues().clone());
    	this.trigMask = new IntArray24(conf.trigMask.getValues().clone());
    	this.trigForce = new IntArray24(conf.trigForce.getValues().clone());
    	this.trigMajority = conf.trigMajority;
    	this.sectorMajority = new IntArray12(conf.sectorMajority.getValues().clone());
    	this.sectorTrigMask = conf.sectorTrigMask;
    	this.sectorThreshold = conf.sectorThreshold;
    	this.towerThreshold = conf.towerThreshold;
    	this.wheelThreshold = conf.wheelThreshold;
    	this.sectTrgSel = conf.sectTrgSel;
    	this.taConfig2 = new IntArray16(conf.taConfig2.getValues().clone());
    }
    
    
    public byte[] getRecMuxClk90() {
        return recMuxClk90;
    }

    public void setRecMuxClk90(byte[] recMuxClk90) {
        this.recMuxClk90 = recMuxClk90;
    }

	public byte[] getRecMuxClkInv() {
        return recMuxClkInv;
    }

    public void setRecMuxClkInv(byte[] recMuxClkInv) {
        this.recMuxClkInv = recMuxClkInv;
    }

    public byte[] getRecMuxRegAdd() {
        return recMuxRegAdd;
    }

    public void setRecMuxRegAdd(byte[] recMuxRegAdd) {
        this.recMuxRegAdd = recMuxRegAdd;
    }

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="IA12_RECMUXDELAYID")
    public IntArray12 getRecMuxDelay() {
        return recMuxDelay;
    }

    public void setRecMuxDelay(IntArray12 recMuxDelay) {
        this.recMuxDelay = recMuxDelay;
    }

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="IA12_RECDATADELAYID")
    public IntArray12 getRecDataDelay() {
        return recDataDelay;
    }

    public void setRecDataDelay(IntArray12 recDataDelay) {
        this.recDataDelay = recDataDelay;
    }

    public int getTaTrgDelay() {
		return taTrgDelay;
	}

	public void setTaTrgDelay(int taTrgDelay) {
		this.taTrgDelay = taTrgDelay;
	}

	public int getTaTrgToff() {
		return taTrgToff;
	}

	public void setTaTrgToff(int taTrgToff) {
		this.taTrgToff = taTrgToff;
	}

	public int getTaTrgSelect() {
		return taTrgSelect;
	}

	public void setTaTrgSelect(int taTrgSelect) {
		this.taTrgSelect = taTrgSelect;
	}

	public int getTaTrgCntrGate() {
		return taTrgCntrGate;
	}

	public void setTaTrgCntrGate(int taTrgCntrGate) {
		this.taTrgCntrGate = taTrgCntrGate;
	}

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="IA24_TASECTORDELAYID")
	public IntArray24 getTaSectorDelay() {
		return taSectorDelay;
	}

	public void setTaSectorDelay(IntArray24 taSectorDelay) {
		this.taSectorDelay = taSectorDelay;
	}

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="IA12_TAFORCELOGIC0ID")
    public IntArray12 getTaForceLogic0() {
		return taForceLogic0;
	}

	public void setTaForceLogic0(IntArray12 taForceLogic0) {
		this.taForceLogic0 = taForceLogic0;
	}

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="IA12_TAFORCELOGIC1ID")
	public IntArray12 getTaForceLogic1() {
		return taForceLogic1;
	}

	public void setTaForceLogic1(IntArray12 taForceLogic1) {
		this.taForceLogic1 = taForceLogic1;
	}

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="IA12_TAFORCELOGIC2ID")
	public IntArray12 getTaForceLogic2() {
		return taForceLogic2;
	}

	public void setTaForceLogic2(IntArray12 taForceLogic2) {
		this.taForceLogic2 = taForceLogic2;
	}

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="IA12_TAFORCELOGIC3ID")
	public IntArray12 getTaForceLogic3() {
		return taForceLogic3;
	}

	public void setTaForceLogic3(IntArray12 taForceLogic3) {
		this.taForceLogic3 = taForceLogic3;
	}

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="IA4_TRIGCONFIGID")
	public IntArray4 getTrigConfig() {
		return trigConfig;
	}

	public void setTrigConfig(IntArray4 trigConfig) {
		this.trigConfig = trigConfig;
	}

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="IA24_TRIGMASKID")
	public IntArray24 getTrigMask() {
		return trigMask;
	}

	public void setTrigMask(IntArray24 trigMask) {
		this.trigMask = trigMask;
	}

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="IA24_TRIGFORCEID")
	public IntArray24 getTrigForce() {
		return trigForce;
	}

	public void setTrigForce(IntArray24 trigForce) {
		this.trigForce = trigForce;
	}

	public int getTrigMajority() {
		return trigMajority;
	}

	public void setTrigMajority(int trigMajority) {
		this.trigMajority = trigMajority;
	}

	@OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="IA12_SECTORMAJORITYID")
	public IntArray12 getSectorMajority() {
		return sectorMajority;
	}

	public void setSectorMajority(IntArray12 sectorMajority) {
		this.sectorMajority = sectorMajority;
	}

	public int getSectorTrigMask() {
		return sectorTrigMask;
	}

	public void setSectorTrigMask(int sectorTrigMask) {
		this.sectorTrigMask = sectorTrigMask;
	}

	public int getSectorThreshold() {
		return sectorThreshold;
	}

	public void setSectorThreshold(int sectorThreshold) {
		this.sectorThreshold = sectorThreshold;
	}

	public int getTowerThreshold() {
		return towerThreshold;
	}

	public void setTowerThreshold(int towerThreshold) {
		this.towerThreshold = towerThreshold;
	}

	public int getWheelThreshold() {
		return wheelThreshold;
	}

	public void setWheelThreshold(int wheelThreshold) {
		this.wheelThreshold = wheelThreshold;
	}

	public int getSectTrgSel() {
		return sectTrgSel;
	}

	public void setSectTrgSel(int sectTrgSel) {
		this.sectTrgSel = sectTrgSel;
	}

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="IA16_TACONFIG2ID")
	public IntArray16 getTaConfig2() {
		return taConfig2;
	}

	public void setTaConfig2(IntArray16 taConfig2) {
		this.taConfig2 = taConfig2;
	}

	@Override
    @Transient
    public ChipType getChipType() {
        return ChipType.TTUTRIG;
    }

	@Override
    @Transient
	public String toString() {
		return "TtuTrigConf [Id=" + getId()
		+ "\n   * recMuxClk90=" + Arrays.toString(recMuxClk90)
		+ "\n   * recMuxClkInv=" + Arrays.toString(recMuxClkInv)
		+ "\n   * recMuxRegAdd="	+ Arrays.toString(recMuxRegAdd)
		+ "\n   * recMuxDelay=" + Arrays.toString(recMuxDelay.getValues()) 
		+ "\n   * recDataDelay=" + Arrays.toString(recDataDelay.getValues())
		+ "\n   * taTrgDelay=" + taTrgDelay
		+ "\n   * taTrgToff="+ taTrgToff
		+ "\n   * taTrgSelect=" + taTrgSelect
		+ "\n   * taTrgCntrGate=" + taTrgCntrGate
		+ "\n   * taSectorDelay=" + Arrays.toString(taSectorDelay.getValues())
		+ "\n   * taForceLogic0=" + Arrays.toString(taForceLogic0.getValues())
		+ "\n   * taForceLogic1=" + Arrays.toString(taForceLogic1.getValues())
		+ "\n   * taForceLogic2=" + Arrays.toString(taForceLogic2.getValues())
		+ "\n   * taForceLogic3=" + Arrays.toString(taForceLogic3.getValues())
		+ "\n   * trigConfig=" + Arrays.toString(trigConfig.getValues())
		+ "\n   * trigMask=" + Arrays.toString(trigMask.getValues())
		+ "\n   * trigForce=" + Arrays.toString(trigForce.getValues())
		+ "\n   * trigMajority=" + trigMajority 
		+ "\n   * sectorMajority=" + Arrays.toString(sectorMajority.getValues())
		+ "\n   * sectorTrigMask=" + sectorTrigMask
		+ "\n   * sectorThreshold="+ sectorThreshold
		+ "\n   * towerThreshold=" + towerThreshold
		+ "\n   * wheelThreshold=" + wheelThreshold
		+ "\n   * sectTrgSel=" + sectTrgSel
		+ "\n   * taConfig2=" + Arrays.toString(taConfig2.getValues())
		+ "\n]";
	}
}
