package rpct.db.domain.configuration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

/**
 * Created on 2006-09-01
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */


@Entity
@SequenceGenerator(name="seq", sequenceName="S_24_1_CONFIGKEYASSIGNMENT", allocationSize=1)
public class ConfigKeyAssignment {
    private int id;
    private String configKey;
    private LocalConfigKey localConfigKey;
    
    @Id
    @Column(name="CONFIGKEYASSIGNMENTID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    public String getConfigKey() {
        return configKey;
    }
    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }
    
    @ManyToOne
    @JoinColumn(name="LCK_LOCALCONFIGKEYID")
    public LocalConfigKey getLocalConfigKey() {
        return localConfigKey;
    }
    public void setLocalConfigKey(LocalConfigKey localConfigKey) {
        this.localConfigKey = localConfigKey;
    }
}
