package rpct.db.domain.configuration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

/**
 * Created on 2009-11-17
 *
 * @author Nikolay Darmenov
 * @version $Id$
 */


@Entity
@SequenceGenerator(name="seq", sequenceName="S_36_1_INTARRAY12", allocationSize=1)
public class IntArray12 extends IntArray{
    private int id;
    private int value0;
    private int value1;
    private int value2;
    private int value3;
    private int value4;
    private int value5;
    private int value6;
    private int value7;
    private int value8;
    private int value9;
    private int value10;
    private int value11;
    
    public IntArray12() {
        super();
    }    

    public IntArray12(int[] values) {
        super();
        setValues(values);
    }    
    
    public IntArray12(IntArray12 array) {
        super();
        setValues(array.getValues());
    }    
    
    @Id
    @Column(name="INTARRAY12ID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    public int getValue0() {
        return value0;
    }
    public void setValue0(int value0) {
        this.value0 = value0;
    }
    public int getValue1() {
        return value1;
    }
    public void setValue1(int value1) {
        this.value1 = value1;
    }
    public int getValue2() {
        return value2;
    }
    public void setValue2(int value2) {
        this.value2 = value2;
    }
    public int getValue3() {
        return value3;
    }
    public void setValue3(int value3) {
        this.value3 = value3;
    }
    public int getValue4() {
        return value4;
    }
    public void setValue4(int value4) {
        this.value4 = value4;
    }
    public int getValue5() {
        return value5;
    }
    public void setValue5(int value5) {
        this.value5 = value5;
    }
    public int getValue6() {
        return value6;
    }
    public void setValue6(int value6) {
        this.value6 = value6;
    }
    public int getValue7() {
        return value7;
    }
    public void setValue7(int value7) {
        this.value7 = value7;
    }
    public int getValue8() {
        return value8;
    }
    public void setValue8(int value8) {
        this.value8 = value8;
    }
    public int getValue9() {
        return value9;
    }
    public void setValue9(int value9) {
        this.value9 = value9;
    }   
    public int getValue10() {
        return value10;
    }
    public void setValue10(int value10) {
        this.value10 = value10;
    }
    public int getValue11() {
        return value11;
    }
    public void setValue11(int value11) {
        this.value11 = value11;
    }

    @Transient
    public int getValue(int index) {
        switch(index) {
        case 0: return value0;
        case 1: return value1;
        case 2: return value2;
        case 3: return value3;
        case 4: return value4;
        case 5: return value5;
        case 6: return value6;
        case 7: return value7;
        case 8: return value8;
        case 9: return value9;
        case 10: return value10;
        case 11: return value11;
        default: throw new IllegalArgumentException("index out of range");           
        }
    }

    @Transient
    public void setValue(int index, int value) {
        switch(index) {
        case 0: 
            value0 = value;
            break;        
        case 1: 
            value1 = value;
            break;        
        case 2: 
            value2 = value;
            break;        
        case 3: 
            value3 = value;
            break;        
        case 4: 
            value4 = value;
            break;        
        case 5: 
            value5 = value;
            break;        
        case 6: 
            value6 = value;
            break;        
        case 7: 
            value7 = value;
            break;        
        case 8: 
            value8 = value;
            break;        
        case 9: 
            value9 = value;
            break;        
        case 10: 
            value10 = value;
            break;
        case 11: 
            value11 = value;
            break;
        default: 
            throw new IllegalArgumentException("index out of range");           
        }
    }
    
    @Transient
    public int[] getValues() {
        return new int[] {
                value0,
                value1,
                value2,
                value3,
                value4,
                value5,
                value6,
                value7,
                value8,
                value9,
                value10,
                value11
        };
    }
    
    @Transient
    public void setValues(int[] values) {
        if (values == null) {
            throw new NullPointerException("values == null");
        }
        if (values.length != size()) {
            throw new IllegalArgumentException("values.length != " + size());
        }
        value0 = values[0];
        value1 = values[1];
        value2 = values[2];
        value3 = values[3];
        value4 = values[4];
        value5 = values[5];
        value6 = values[6];
        value7 = values[7];
        value8 = values[8];
        value9 = values[9];
        value10 = values[10];
        value11 = values[11];
    }
    
    @Transient
    public int size() {
        return 12;
    }
}
