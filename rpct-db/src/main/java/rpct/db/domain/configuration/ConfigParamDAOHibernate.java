package rpct.db.domain.configuration;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import rpct.db.DataAccessException;
import rpct.db.domain.DAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;



/**
 * Created on 2005-04-05
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class ConfigParamDAOHibernate extends DAOHibernate implements ConfigParamDAO {

    public ConfigParamDAOHibernate(HibernateContext hibernateContext) {
        super(hibernateContext);
    }

    public List<ConfigParamDefinition> getAllDefinitions() throws DataAccessException {
        return getObjects(ConfigParamDefinition.class);
    }

    public ConfigParamDefinition getByName(String name) throws DataAccessException {
        try {
            return (ConfigParamDefinition) currentSession().createCriteria(ConfigParamDefinition.class)
                .add(Restrictions.eq("name", name))
                .uniqueResult();           
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }  
    }

    public ConfigParamValue getValue(int id) throws DataAccessException {
        return (ConfigParamValue) getObject(ConfigParamValue.class, id);
    }

    public ConfigParamValue getValue(ConfigParamDefinition definition, int id) throws DataAccessException {
        switch(definition.getType()) {
        case STRING:
            return (ConfigParamValue) getObject(ConfigParamStringValue.class, id);
        case INTEGER:
            return (ConfigParamValue) getObject(ConfigParamIntegerValue.class, id);
        default:
            throw new IllegalArgumentException("Unknown type '" + definition.getType() + "'");    
        }
    }
    /*
    public static void main(String[] args) {        
        HibernateContextImpl.setStandalone(true);
        ConfigParamDAO dao = new ConfigParamDAOHibernate();
        try {
            for (int i=0; i<1000; i++) {
                ConfigParamDefinition definition = new ConfigParamDefinition(
                        "test" + System.currentTimeMillis() + i, ConfigParamType.STRING, "testowa");
                dao.saveObject(definition);   
                HibernateContext.getInstance().closeSession();   
                System.out.println("String Def saved");
                for (int j=0; j<20; j++) {
                    ConfigParamValue val = new ConfigParamStringValue();
                    val.setDefinition(definition);
                    val.setValue("wartosc testowa");
                    dao.saveObject(val);   
                    System.out.println("String Val saved");
                }
                HibernateContext.getInstance().closeSession();
                
                
                ConfigParamDefinition intDef = new ConfigParamDefinition(
                        "test" + System.currentTimeMillis() + i, ConfigParamType.INTEGER, "testowa integer");
                dao.saveObject(intDef);   
                HibernateContext.getInstance().closeSession();   
                System.out.println("Integer Def saved");
                for (int j=0; j<20; j++) {
                    ConfigParamValue intVal = new ConfigParamIntegerValue();
                    intVal.setDefinition(intDef);
                    intVal.setValue(125);
                    dao.saveObject(intVal);
                }
                HibernateContext.getInstance().closeSession();
            }

            for (int i=0; i < 10; i++) {
                int id = 40695 + 10 * i;
                System.out.println("id = " + id + " = " + dao.getValue(id).getValue());
            }
        } catch(Exception e) {
            System.err.println(e.getMessage());
            
        }
        System.out.println("Done");
    }*/
}
