package rpct.db.domain.configuration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

/**
 * Created on 2009-11-17
 *
 * @author Nikolay Darmenov
 * @version $Id$
 */


@Entity
@SequenceGenerator(name="seq", sequenceName="S_35_1_INTARRAY6", allocationSize=1)
public class IntArray6 extends IntArray {
    private int id;
    private int value0;
    private int value1;
    private int value2;
    private int value3;
    private int value4;
    private int value5;

    public IntArray6() {
        super();
    }    

    public IntArray6(int[] values) {
        super();
        setValues(values);
    }    
    
    @Id
    @Column(name="INTARRAY6ID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    public int getValue0() {
        return value0;
    }
    public void setValue0(int value0) {
        this.value0 = value0;
    }
    public int getValue1() {
        return value1;
    }
    public void setValue1(int value1) {
        this.value1 = value1;
    }
    public int getValue2() {
        return value2;
    }
    public void setValue2(int value2) {
        this.value2 = value2;
    }
    public int getValue3() {
        return value3;
    }
    public void setValue3(int value3) {
        this.value3 = value3;
    }
	public int getValue4() {
		return value4;
	}
    public void setValue4(int value4) {
		this.value4 = value4;
	}
	public int getValue5() {
		return value5;
	}
	public void setValue5(int value5) {
		this.value5 = value5;
	}

	@Transient
    public int getValue(int index) {
        switch(index) {
        case 0: return value0;
        case 1: return value1;
        case 2: return value2;
        case 3: return value3;
        case 4: return value4;
        case 5: return value5;
        default: throw new IllegalArgumentException("index out of range");           
        }
    }

    @Transient
    public void setValue(int index, int value) {
        switch(index) {
        case 0: 
            value0 = value;
            break;        
        case 1: 
            value1 = value;
            break;        
        case 2: 
            value2 = value;
            break;        
        case 3: 
            value3 = value;
            break;  
        case 4: 
            value4 = value;
            break;  
        case 5: 
            value5 = value;
            break;  
        default: 
            throw new IllegalArgumentException("index out of range");           
        }
    }
    
    @Transient
    public int[] getValues() {
        return new int[] {
                value0,
                value1,
                value2,
                value3,
                value4,
                value5
        };
    }
    
    @Transient
    public void setValues(int[] values) {
        if (values == null) {
            throw new NullPointerException("values == null");
        }
        if (values.length != size()) {
            throw new IllegalArgumentException("values.length != " + size());
        }
        value0 = values[0];
        value1 = values[1];
        value2 = values[2];
        value3 = values[3];
        value4 = values[4];
        value5 = values[5];
    }
    
    @Transient
    public int size() {
        return 6;
    }
}
