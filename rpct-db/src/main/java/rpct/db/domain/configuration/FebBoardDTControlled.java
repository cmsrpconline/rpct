package rpct.db.domain.configuration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import rpct.db.domain.equipment.Board;
/**
 * Created on 2010-10-08
 * 
 * @author Karol Bunkowski
 * @version $Id$
 */

@Entity
@SequenceGenerator(name="seq", sequenceName="S_38_1_FEBBOARDDTCONTROLLED", allocationSize=1)
public class FebBoardDTControlled {
    private int id;
    private Board board;
    
    @Id
    @Column(name="FEBBOARDDTCONTROLLEDID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    //@ManyToOne(fetch=FetchType.LAZY)
    @ManyToOne()
    @JoinColumn(name="BOARD_BOARDID")
    public Board getBoard() {
        return board;
    }
    public void setBoard(Board board) {
        this.board = board;
    }
}
