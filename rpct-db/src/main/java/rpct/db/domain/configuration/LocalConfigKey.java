package rpct.db.domain.configuration;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import rpct.db.domain.equipment.ChipType;
/**
 * Created on 2006-09-01
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */


@Entity
@SequenceGenerator(name="seq", sequenceName="S_22_1_LOCALCONFIGKEY", allocationSize=1)
public class LocalConfigKey {
    private int id;
    private String name;
    private Subsystem subsystem;
    
    @Id
    @Column(name="LOCALCONFIGKEYID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    @Enumerated(EnumType.STRING)
	public Subsystem getSubsystem() {
		return subsystem;
	}
	public void setSubsystem(Subsystem subsystem) {
		/*if(Subsytem.valueOf(subsystem).equals(null)) {
			throw new RuntimeException("the subsystem " + subsystem + " is not defined in the LocalConfigKey.Subsytem");
		}*/
		this.subsystem = subsystem;
	}
	
	@Transient
	public String toString() {
		return name + " subsystem " + subsystem;
	}
	
	@Transient
	private final static ChipType[] lbs = { ChipType.SYNCODER };
	@Transient
	private final static ChipType[] tcs = { ChipType.OPTO, ChipType.PAC, ChipType.RMB, ChipType.GBSORT, ChipType.TCSORT };
	@Transient
	private final static ChipType[] sc = { ChipType.HALFSORTER, ChipType.FINALSORTER};
	@Transient
	private final static ChipType[] rbcs_ttus = { ChipType.RBC, ChipType.TTUOPTO, ChipType.TTUTRIG, ChipType.TTUFINAL};
	@Transient
	private final static ChipType[] febs = {ChipType.FEBCHIP};
	@Transient
	private final static ChipType[] all = {};

	public enum Subsystem {		
		LBS(lbs),
		TCS(tcs),
		SC(sc),
		RBCS_TTUS(rbcs_ttus),
		FEBS(febs),
		ALL(all);
		
		Subsystem(ChipType[] chipTypes) {
			for(ChipType chipType : chipTypes) {
				this.chipTypes.add(chipType);
			}
		}
		private final Set<ChipType> chipTypes = new HashSet<ChipType>();
		
		public static Subsystem getSybsytem(ChipType chipType) {
			for(Subsystem subsytem : Subsystem.values()) {
				if(subsytem.chipTypes.contains(chipType)) {
					return subsytem;
				}
			}
			throw new IllegalArgumentException("the " + chipType + " is not assigened to any Subsytem");
		}
	}
}
