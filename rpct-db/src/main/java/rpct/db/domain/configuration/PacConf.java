package rpct.db.domain.configuration;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import rpct.db.domain.equipment.ChipType;


/**
 * Created on 2005-03-08
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */

@Entity
@PrimaryKeyJoinColumn(name="TBPACCONFID")
@Table(name="TBPACCONF")
//@SequenceGenerator(name="indexedSettingSeq", sequenceName="S_25_1_INDEXEDSETTING", allocationSize=1)
public class PacConf extends StaticConfiguration {

    private byte[] recMuxClkInv; 
    private byte[] recMuxClk90; 
    private byte[] recMuxRegAdd; 
    private IntArray18 recMuxDelay; 
    private IntArray18 recDataDelay; 
    private int bxOfCoincidence;
    private int pacEna;
    private int pacConfigId;
    
    public PacConf() {
        super();
        
        recMuxDelay = new IntArray18();
        recDataDelay = new IntArray18();
        
        recMuxClkInv = new byte[7]; 
        recMuxClk90 = new byte[7]; 
        recMuxRegAdd = new byte[7];
    }    

    public PacConf(byte[] recMuxClkInv, byte[] recMuxClk90, byte[] recMuxRegAdd,
            IntArray18 recMuxDelay, IntArray18 recDataDelay, int bxOfCoincidence,
            int pacEna, int pacConfigId) {
        super();
        this.recMuxClkInv = recMuxClkInv;
        this.recMuxClk90 = recMuxClk90;
        this.recMuxRegAdd = recMuxRegAdd;
        this.recMuxDelay = recMuxDelay;
        this.recDataDelay = recDataDelay;
        this.bxOfCoincidence = bxOfCoincidence;
        this.pacEna = pacEna;
        this.pacConfigId = pacConfigId;
    }

    public PacConf(PacConf conf) {
        super();
        this.recMuxClkInv = conf.getRecMuxClkInv().clone();
        this.recMuxClk90 = conf.getRecMuxClk90().clone();
        this.recMuxRegAdd = conf.getRecMuxRegAdd().clone();
        this.recMuxDelay = new IntArray18(conf.getRecMuxDelay().getValues());
        this.recDataDelay = new IntArray18(conf.getRecDataDelay().getValues());
        this.bxOfCoincidence = conf.getBxOfCoincidence();
        this.pacEna = conf.getPacEna();
        this.pacConfigId = conf.getPacConfigId();
    }

    public byte[] getRecMuxClkInv() {
        return recMuxClkInv;
    }

    public void setRecMuxClkInv(byte[] recMuxClkInv) {
        this.recMuxClkInv = recMuxClkInv;
    }

    public byte[] getRecMuxClk90() {
        return recMuxClk90;
    }

    public void setRecMuxClk90(byte[] recMuxClk90) {
        this.recMuxClk90 = recMuxClk90;
    }

    public byte[] getRecMuxRegAdd() {
        return recMuxRegAdd;
    }

    public void setRecMuxRegAdd(byte[] recMuxRegAdd) {
        this.recMuxRegAdd = recMuxRegAdd;
    }

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="IA18_RECMUXDELAYID")
    public IntArray18 getRecMuxDelay() {
        return recMuxDelay;
    }

    public void setRecMuxDelay(IntArray18 recMuxDelay) {
        this.recMuxDelay = recMuxDelay;
    }

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="IA18_RECDATADELAYID")
    public IntArray18 getRecDataDelay() {
        return recDataDelay;
    }

    public void setRecDataDelay(IntArray18 recDataDelay) {
        this.recDataDelay = recDataDelay;
    }

    public int getBxOfCoincidence() {
        return bxOfCoincidence;
    }

    public void setBxOfCoincidence(int bxOfCoincidence) {
        this.bxOfCoincidence = bxOfCoincidence;
    }

    public int getPacEna() {
        return pacEna;
    }

    public void setPacEna(int pacEna) {
        this.pacEna = pacEna;
    }

    public int getPacConfigId() {
        return pacConfigId;
    }

    public void setPacConfigId(int pacConfigId) {
        this.pacConfigId = pacConfigId;
    }

    @Override
    @Transient
    public ChipType getChipType() {
        return ChipType.PAC;
    }
    
    @Transient
    public String toString() {
    	return  "pacConfigId 0x" + Integer.toHexString(pacConfigId) + " bxOfCoincidence " + bxOfCoincidence + " RecDataDelay " + getRecDataDelay().getValue(5);
    }
}
