package rpct.db.domain.configuration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import rpct.db.domain.equipment.Board;
/**
 * Created on 2007-05-21
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */


@Entity
@SequenceGenerator(name="seq", sequenceName="S_29_1_BOARDDISABLED", allocationSize=1)
public class BoardDisabled {
    private int id;
    private Board board;
    
    @Id
    @Column(name="BOARDDISABLEDID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    @OneToOne()    
    @JoinColumn(name="BOARD_BOARDID")
    public Board getBoard() {
        return board;
    }
    public void setBoard(Board board) {
        this.board = board;
    }
}
