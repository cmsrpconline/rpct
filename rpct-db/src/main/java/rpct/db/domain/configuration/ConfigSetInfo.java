/**
 * 
 */
package rpct.db.domain.configuration;

import java.util.Date;

public class ConfigSetInfo
    implements java.io.Serializable {

    private int localConfigKeyId;
    private Date creationDate;

    public ConfigSetInfo() {
        super();
    }

    public ConfigSetInfo(int localConfigKeyId, Date creationDate) {
        super();
        this.localConfigKeyId = localConfigKeyId;
        this.creationDate = creationDate;
    }

    public void setLocalConfigKeyId(int localConfigKeyId) {
        this.localConfigKeyId = localConfigKeyId;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public int getLocalConfigKeyId() {
        return localConfigKeyId;
    }

    public Date getCreationDate() {
        return creationDate;
    }
}
