package rpct.db.domain.configuration;

/**
 * Created on 2005-04-05
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public enum ConfigParamType {
    STRING, INTEGER
}
