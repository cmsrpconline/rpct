package rpct.db.domain.configuration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

/**
 * Created on 2009-11-17
 *
 * @author Nikolay Darmenov
 * @version $Id$
 */


@Entity
@SequenceGenerator(name="seq", sequenceName="S_37_1_INTARRAY24", allocationSize=1)
public class IntArray24 extends IntArray {
    private int id;
    private int value0;
    private int value1;
    private int value2;
    private int value3;
    private int value4;
    private int value5;
    private int value6;
    private int value7;
    private int value8;
    private int value9;
    private int value10;
    private int value11;
    private int value12;
    private int value13;
    private int value14;
    private int value15;
    private int value16;
    private int value17;
    private int value18;
    private int value19;
    private int value20;
    private int value21;
    private int value22;
    private int value23;

    
    public IntArray24() {
        super();
    }    

    public IntArray24(int[] values) {
        super();
        setValues(values);
    }    
    
    @Id
    @Column(name="INTARRAY24ID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    public int getValue0() {
        return value0;
    }
    public void setValue0(int value0) {
        this.value0 = value0;
    }
    public int getValue1() {
        return value1;
    }
    public void setValue1(int value1) {
        this.value1 = value1;
    }
    public int getValue2() {
        return value2;
    }
    public void setValue2(int value2) {
        this.value2 = value2;
    }
    public int getValue3() {
        return value3;
    }
    public void setValue3(int value3) {
        this.value3 = value3;
    }
    public int getValue4() {
        return value4;
    }
    public void setValue4(int value4) {
        this.value4 = value4;
    }
    public int getValue5() {
        return value5;
    }
    public void setValue5(int value5) {
        this.value5 = value5;
    }
    public int getValue6() {
        return value6;
    }
    public void setValue6(int value6) {
        this.value6 = value6;
    }
    public int getValue7() {
        return value7;
    }
    public void setValue7(int value7) {
        this.value7 = value7;
    }
    public int getValue8() {
        return value8;
    }
    public void setValue8(int value8) {
        this.value8 = value8;
    }
    public int getValue9() {
        return value9;
    }
    public void setValue9(int value9) {
        this.value9 = value9;
    }   
    public int getValue10() {
        return value10;
    }
    public void setValue10(int value10) {
        this.value10 = value10;
    }
    public int getValue11() {
        return value11;
    }
    public void setValue11(int value11) {
        this.value11 = value11;
    }
    public int getValue12() {
        return value12;
    }
    public void setValue12(int value12) {
        this.value12 = value12;
    }
    public int getValue13() {
        return value13;
    }
    public void setValue13(int value13) {
        this.value13 = value13;
    }
    public int getValue14() {
        return value14;
    }
    public void setValue14(int value14) {
        this.value14 = value14;
    }
    public int getValue15() {
        return value15;
    }
    public void setValue15(int value15) {
        this.value15 = value15;
    }
    public int getValue16() {
        return value16;
    }
    public void setValue16(int value16) {
        this.value16 = value16;
    }
    public int getValue17() {
        return value17;
    }
    public void setValue17(int value17) {
        this.value17 = value17;
    }
	public int getValue18() {
		return value18;
	}
	public void setValue18(int value18) {
		this.value18 = value18;
	}
	public int getValue19() {
		return value19;
	}
	public void setValue19(int value19) {
		this.value19 = value19;
	}
	public int getValue20() {
		return value20;
	}
	public void setValue20(int value20) {
		this.value20 = value20;
	}
	public int getValue21() {
		return value21;
	}
	public void setValue21(int value21) {
		this.value21 = value21;
	}
	public int getValue22() {
		return value22;
	}
	public void setValue22(int value22) {
		this.value22 = value22;
	}
	public int getValue23() {
		return value23;
	}
	public void setValue23(int value23) {
		this.value23 = value23;
	}


    @Transient
    public int getValue(int index) {
        switch(index) {
        case 0: return value0;
        case 1: return value1;
        case 2: return value2;
        case 3: return value3;
        case 4: return value4;
        case 5: return value5;
        case 6: return value6;
        case 7: return value7;
        case 8: return value8;
        case 9: return value9;
        case 10: return value10;
        case 11: return value11;
        case 12: return value12;
        case 13: return value13;
        case 14: return value14;
        case 15: return value15;
        case 16: return value16;
        case 17: return value17;
        case 18: return value18;
        case 19: return value19;
        case 20: return value20;
        case 21: return value21;
        case 22: return value22;
        case 23: return value23;
        default: throw new IllegalArgumentException("index out of range");           
        }
    }

    @Transient
    public void setValue(int index, int value) {
        switch(index) {
        case 0: 
            value0 = value;
            break;        
        case 1: 
            value1 = value;
            break;        
        case 2: 
            value2 = value;
            break;        
        case 3: 
            value3 = value;
            break;        
        case 4: 
            value4 = value;
            break;        
        case 5: 
            value5 = value;
            break;        
        case 6: 
            value6 = value;
            break;        
        case 7: 
            value7 = value;
            break;        
        case 8: 
            value8 = value;
            break;        
        case 9: 
            value9 = value;
            break;        
        case 10: 
            value10 = value;
            break;
        case 11: 
            value11 = value;
            break;
        case 12: 
            value12 = value;
            break;
        case 13: 
            value13 = value;
            break;
        case 14: 
            value14 = value;
            break;
        case 15: 
            value15 = value;
            break;
        case 16: 
            value16 = value;
            break;
        case 17: 
            value17 = value;
            break;
        case 18: 
            value18 = value;
            break;
        case 19: 
            value19 = value;
            break;
        case 20: 
            value20 = value;
            break;
        case 21: 
            value21 = value;
            break;
        case 22: 
            value22 = value;
            break;
        case 23: 
            value23 = value;
            break;
        default: 
            throw new IllegalArgumentException("index out of range");           
        }
    }
    
    @Transient
    public int[] getValues() {
        return new int[] {
                value0,
                value1,
                value2,
                value3,
                value4,
                value5,
                value6,
                value7,
                value8,
                value9,
                value10,
                value11,
                value12,
                value13,
                value14,
                value15,
                value16,
                value17,
                value18,
                value19,
                value20,
                value21,
                value22,
                value23
        };
    }
    
    @Transient
    public void setValues(int[] values) {
        if (values == null) {
            throw new NullPointerException("values == null");
        }
        if (values.length != size()) {
            throw new IllegalArgumentException("values.length != " + size());
        }
        value0 = values[0];
        value1 = values[1];
        value2 = values[2];
        value3 = values[3];
        value4 = values[4];
        value5 = values[5];
        value6 = values[6];
        value7 = values[7];
        value8 = values[8];
        value9 = values[9];
        value10 = values[10];
        value11 = values[11];
        value12 = values[12];
        value13 = values[13];
        value14 = values[14];
        value15 = values[15];
        value16 = values[16];
        value17 = values[17];        
        value18 = values[18];        
        value19 = values[19];        
        value20 = values[20];        
        value21 = values[21];        
        value22 = values[22];        
        value23 = values[23];        
    }
    
    @Transient
    public int size() {
        return 24;
    }
}
