/**
 * 
 */
package rpct.db.domain.configuration;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * Created on 2012-02-01
 *
 * @author Karol Bunkowski
 * @version $Id: $
 */


@Entity
public class SynCoderConfInFlash {
	/*private Chip chip;
	private SynCoderConf synCoderConf;	*/
	private int chipId;
	private Integer synCoderConfId;
	private int beginAddress;
	private int endAddress;
	private Date uploadTime = new Date();

	public SynCoderConfInFlash() {
		super();
		this.chipId = 0;
		this.synCoderConfId = 0;
		this.beginAddress = 0;
		this.endAddress = 0;
	}
	
	public SynCoderConfInFlash(int chipId) {
		super();
		this.chipId = chipId;
		this.synCoderConfId = 0;
		this.beginAddress = 0;
		this.endAddress = 0;
	}
	
	public SynCoderConfInFlash(int chipId, int synCoderConfId,
			int beginAddress, int endAddress) {
		super();
		this.chipId = chipId;
		if(synCoderConfId == 0) {
			this.synCoderConfId = null;
		}
		else {
			this.synCoderConfId = synCoderConfId;
		}
		this.beginAddress = beginAddress;
		this.endAddress = endAddress;
	}
	/*
	public Chip getChip() {
		return chip;
	}
	public void setChip(Chip chip) {
		this.chip = chip;
	}
	
	@ManyToOne
    @JoinColumn(name="LCSC_SYNCODERCONFID")
	public SynCoderConf getSynCoderConf() {
		return synCoderConf;
	}
	public void setSynCoderConf(SynCoderConf synCoderConf) {
		this.synCoderConf = synCoderConf;
	}*/
	
	
	@Id
    @Column(name="CHIP_CHIPID")
	public int getChipId() {
		return this.chipId;
	}
	public void setChipId(int chipId) {
		this.chipId = chipId;
	}
	
	@Column(name="LCSC_SYNCODERCONFID")
	public Integer getSynCoderConfId() {
		return synCoderConfId;
	}
	public void setSynCoderConfId(Integer synCoderConfId) {
		this.synCoderConfId = synCoderConfId;
	}
	
	public int getBeginAddress() {
		return beginAddress;
	}
	public void setBeginAddress(int beginAddress) {
		this.beginAddress = beginAddress;
	}
		
	public int getEndAddress() {
		return endAddress;
	}
	public void setEndAddress(int endAddress) {
		this.endAddress = endAddress;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	public Date getUploadTime() {
		return uploadTime;
	}
	public void setUploadTime(Date uploadTime) {
		this.uploadTime = uploadTime;
	}	
}
