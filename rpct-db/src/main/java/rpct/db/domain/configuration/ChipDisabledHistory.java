package rpct.db.domain.configuration;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import rpct.db.domain.equipment.Chip;
/**
 * Created on 2011-06-02
 *
 * @author Karol Bunkowski
 * @version $Id: ChipDisabledHistory.java 1563 2010-02-08 12:30:40Z kbunkow $
 */


@Entity
@SequenceGenerator(name="seq", sequenceName="S_39_1_CHIPDISABLEDHISTORY", allocationSize=1)
public class ChipDisabledHistory {
    private int id;
    private Chip chip;
    private String status;
    private Date modificationDate = new Date();
    
    @Id
    @Column(name="CHIPDISABLEDHISTORYID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    @ManyToOne
    @JoinColumn(name="CHIP_CHIPID")
    public Chip getChip() {
        return chip;
    }
    public void setChip(Chip chip) {
        this.chip = chip;
    }
  
    public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
    public Date getModificationDate() {
        return modificationDate;
    }
    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }
    
    public String toString() {
    	return chip.toString() + " " + status + " from " + modificationDate;
    }
}
