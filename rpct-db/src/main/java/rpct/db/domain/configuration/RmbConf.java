package rpct.db.domain.configuration;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import rpct.db.domain.equipment.ChipType;


/**
 * Created on 2005-03-08
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */

@Entity
@PrimaryKeyJoinColumn(name="TBRMBCONFID")
@Table(name="TBRMBCONF")
public class RmbConf extends StaticConfiguration {

    private byte[] recMuxClkInv; 
    private byte[] recMuxClk90; 
    private byte[] recMuxRegAdd; 
    private IntArray18 recMuxDelay; 
    private IntArray18 recDataDelay; 
    
    private int chanEna;
    private int preTriggerVal; 
    private int postTriggerVal; 
    private int dataDelay; 
    private int trgDelay; 
    
    public RmbConf() {
        super();
        
        recMuxDelay = new IntArray18();
        recDataDelay = new IntArray18();
        
        recMuxClkInv = new byte[7]; 
        recMuxClk90 = new byte[7]; 
        recMuxRegAdd = new byte[7];
    }

    
    public RmbConf(byte[] recMuxClkInv, byte[] recMuxClk90, byte[] recMuxRegAdd,
            IntArray18 recMuxDelay, IntArray18 recDataDelay, int chanEna,
            int preTriggerVal, int postTriggerVal, int dataDelay, int trgDelay) {
        super();
        this.recMuxClkInv = recMuxClkInv;
        this.recMuxClk90 = recMuxClk90;
        this.recMuxRegAdd = recMuxRegAdd;
        this.recMuxDelay = recMuxDelay;
        this.recDataDelay = recDataDelay;
        this.chanEna = chanEna;
        this.preTriggerVal = preTriggerVal;
        this.postTriggerVal = postTriggerVal;
        this.dataDelay = dataDelay;
        this.trgDelay = trgDelay;
    }

    public RmbConf(RmbConf conf) {
        super();
        this.recMuxClkInv = conf.getRecMuxClkInv().clone();
        this.recMuxClk90 = conf.getRecMuxClk90().clone();
        this.recMuxRegAdd = conf.getRecMuxRegAdd().clone();
        this.recMuxDelay = new IntArray18(conf.getRecMuxDelay().getValues());
        this.recDataDelay = new IntArray18(conf.getRecDataDelay().getValues());
        this.chanEna = conf.getChanEna();
        this.preTriggerVal = conf.getPreTriggerVal();
        this.postTriggerVal = conf.getPostTriggerVal();
        this.dataDelay = conf.getDataDelay();
        this.trgDelay = conf.getTrgDelay();
    }


    public byte[] getRecMuxClkInv() {
        return recMuxClkInv;
    }

    public void setRecMuxClkInv(byte[] recMuxClkInv) {
        this.recMuxClkInv = recMuxClkInv;
    }

    public byte[] getRecMuxClk90() {
        return recMuxClk90;
    }

    public void setRecMuxClk90(byte[] recMuxClk90) {
        this.recMuxClk90 = recMuxClk90;
    }

    public byte[] getRecMuxRegAdd() {
        return recMuxRegAdd;
    }

    public void setRecMuxRegAdd(byte[] recMuxRegAdd) {
        this.recMuxRegAdd = recMuxRegAdd;
    }

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="IA18_RECMUXDELAYID")
    public IntArray18 getRecMuxDelay() {
        return recMuxDelay;
    }

    public void setRecMuxDelay(IntArray18 recMuxDelay) {
        this.recMuxDelay = recMuxDelay;
    }

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="IA18_RECDATADELAYID")
    public IntArray18 getRecDataDelay() {
        return recDataDelay;
    }

    public void setRecDataDelay(IntArray18 recDataDelay) {
        this.recDataDelay = recDataDelay;
    }
    
    public int getChanEna() {
        return chanEna;
    }

    public void setChanEna(int chanEna) {
        this.chanEna = chanEna;
    }

    public int getDataDelay() {
        return dataDelay;
    }

    public void setDataDelay(int dataDelay) {
        this.dataDelay = dataDelay;
    }

    public int getPostTriggerVal() {
        return postTriggerVal;
    }

    public void setPostTriggerVal(int postTriggerVal) {
        this.postTriggerVal = postTriggerVal;
    }

    public int getPreTriggerVal() {
        return preTriggerVal;
    }

    public void setPreTriggerVal(int preTriggerVal) {
        this.preTriggerVal = preTriggerVal;
    }

    public int getTrgDelay() {
        return trgDelay;
    }

    public void setTrgDelay(int trgDelay) {
        this.trgDelay = trgDelay;
    }

    @Override
    @Transient
    public ChipType getChipType() {
        return ChipType.RMB;
    }
    
    public String toString() {
    	String str = "RmbConf: chanEna " + chanEna + " preTriggerVal " + preTriggerVal + " postTriggerVal " + postTriggerVal
    			+ " dataDelay " + dataDelay + " trgDelay " + trgDelay;
    	return str;
    }
    
}
