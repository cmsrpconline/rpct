package rpct.db.domain.configuration;

/**
 * Created on 2005-04-05
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class ConfigParamDefinition {    
    
    private int id;
    private String name;
    private ConfigParamType type;
    private String description;
    
    public ConfigParamDefinition() {
        super();
    }
    
    public ConfigParamDefinition(String name, ConfigParamType type, String description) {
        super();
        this.name = name;
        this.type = type;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public ConfigParamType getType() {
        return type;
    }
    
    public void setType(ConfigParamType type) {
        this.type = type;
    }
    
    
}
