package rpct.db.domain.configuration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;



@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@SequenceGenerator(name="seq", sequenceName="S_20_1_XDAQAPPLICATION", allocationSize=1)
public abstract class XdaqApplication {
    private int id;   
    private XdaqExecutive xdaqExecutive;
    private String className;
    private int instance;
    private int appId;
    
    @Id
    @Column(name="xdaqapplicationid")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    public int getAppId() {
        return appId;
    }
    public void setAppId(int appId) {
        this.appId = appId;
    }
    
    public String getClassName() {
        return className;
    }
    public void setClassName(String className) {
        this.className = className;
    }
    
    public int getInstance() {
        return instance;
    }
    public void setInstance(int instance) {
        this.instance = instance;
    }    

    @ManyToOne
    @JoinColumn(name="XE_XDAQEXECUTIVEID")
    public XdaqExecutive getXdaqExecutive() {
        return xdaqExecutive;
    }
    public void setXdaqExecutive(XdaqExecutive xdaqExecutive) {
        this.xdaqExecutive = xdaqExecutive;
    }
    
    public String toString() {
        StringBuilder str = new StringBuilder(getClass().toString());
        str.append(" id = ").append(id).append(", className = ").append(className)
            .append(", instance = ").append(instance)
            .append(", appId = ").append(appId);
        return str.toString();
    }	
}