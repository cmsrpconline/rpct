package rpct.db.domain.configuration;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import rpct.db.domain.equipment.ChipType;


/**
 * Created on 2005-03-08
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */

@Entity
@PrimaryKeyJoinColumn(name="TBGBSORTCONFID")
@Table(name="TBGBSORTCONF")
//@SequenceGenerator(name="indexedSettingSeq", sequenceName="S_25_1_INDEXEDSETTING", allocationSize=1)
public class TbGbSortConf extends StaticConfiguration {

    private byte[] recMuxClkInv; 
    private byte[] recMuxClk90; 
    private byte[] recMuxRegAdd; 
    private IntArray4 recMuxDelay; 
    private IntArray4 recDataDelay; 
    
    public TbGbSortConf() {
        super();
        
        recMuxDelay = new IntArray4();
        recDataDelay = new IntArray4();
        
        recMuxClkInv = new byte[7]; 
        recMuxClk90 = new byte[7]; 
        recMuxRegAdd = new byte[7];
    }

    public TbGbSortConf(byte[] recMuxClkInv, byte[] recMuxClk90, byte[] recMuxRegAdd,
            IntArray4 recMuxDelay, IntArray4 recDataDelay) {
        super();
        this.recMuxClkInv = recMuxClkInv;
        this.recMuxClk90 = recMuxClk90;
        this.recMuxRegAdd = recMuxRegAdd;
        this.recMuxDelay = recMuxDelay;
        this.recDataDelay = recDataDelay;
    }
    
    public TbGbSortConf(TbGbSortConf conf) {
        super();
        this.recMuxClkInv = conf.getRecMuxClkInv().clone();
        this.recMuxClk90 = conf.getRecMuxClk90().clone();
        this.recMuxRegAdd = conf.getRecMuxRegAdd().clone();
        this.recMuxDelay = new IntArray4(conf.getRecMuxDelay().getValues());
        this.recDataDelay = new IntArray4(conf.getRecDataDelay().getValues());
    }

    public byte[] getRecMuxClkInv() {
        return recMuxClkInv;
    }

    public void setRecMuxClkInv(byte[] recMuxClkInv) {
        this.recMuxClkInv = recMuxClkInv;
    }
    
    public byte[] getRecMuxClk90() {
        return recMuxClk90;
    }

    public void setRecMuxClk90(byte[] recMuxClk90) {
        this.recMuxClk90 = recMuxClk90;
    }

    public byte[] getRecMuxRegAdd() {
        return recMuxRegAdd;
    }

    public void setRecMuxRegAdd(byte[] recMuxRegAdd) {
        this.recMuxRegAdd = recMuxRegAdd;
    }

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="IA4_RECMUXDELAYID")
    public IntArray4 getRecMuxDelay() {
        return recMuxDelay;
    }

    public void setRecMuxDelay(IntArray4 recMuxDelay) {
        this.recMuxDelay = recMuxDelay;
    }

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="IA4_RECDATADELAYID")
    public IntArray4 getRecDataDelay() {
        return recDataDelay;
    }

    public void setRecDataDelay(IntArray4 recDataDelay) {
        this.recDataDelay = recDataDelay;
    }

    @Override
    @Transient
    public ChipType getChipType() {
        return ChipType.GBSORT;
    }
}
