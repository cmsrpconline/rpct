package rpct.db.domain.configuration;

import java.util.List;

import rpct.db.DataAccessException;
import rpct.db.domain.DAO;

/**
 * Created on 2005-03-01
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public interface ConfigParamDAO extends DAO {
    List<ConfigParamDefinition> getAllDefinitions() throws DataAccessException;
    ConfigParamDefinition getByName(String name) throws DataAccessException;
    ConfigParamValue getValue(int id) throws DataAccessException;
    ConfigParamValue getValue(ConfigParamDefinition definition, int id) throws DataAccessException;
}
