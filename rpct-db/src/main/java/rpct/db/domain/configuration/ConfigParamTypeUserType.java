package rpct.db.domain.configuration;

import rpct.db.domain.hibernate.EnumUserType;

/**
 * Created on 2005-04-05
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class ConfigParamTypeUserType extends EnumUserType<ConfigParamType>{
    public ConfigParamTypeUserType() { 
        super(ConfigParamType.class); 
    } 
}