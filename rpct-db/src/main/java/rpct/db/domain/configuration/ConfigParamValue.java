package rpct.db.domain.configuration;

/**
 * Created on 2005-04-05
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public abstract class ConfigParamValue {
    private int id;
    private ConfigParamDefinition definition;
    public ConfigParamDefinition getDefinition() {
        return definition;
    }

    protected abstract void checkType(ConfigParamDefinition definition);
    public void setDefinition(ConfigParamDefinition definition) {
        checkType(definition);
        this.definition = definition;
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public abstract Object getValue();
    public abstract void setValue(Object value);
}
