package rpct.db.domain.configuration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created on 2006-02-20
 *
 * @author Michal Szleper
 * @version $Id$
 */


@Entity
@Table(name="CURRENTCONFIGKEY")
@SequenceGenerator(name="seq", sequenceName="S_34_1_CURRENTCONFIGKEY", initialValue= 1, allocationSize = 1)
public class CurrentConfigKey {
    private int id;
    private String name;
//    private int run;
    
    public CurrentConfigKey(){
    }

        @Id
    @Column(name="KEYID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    @Column(name="KEYNAME")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
//    @Column(name="RUN")
//	public Integer getRun() {
//		return run;
//	}
//	public void setRun(Integer run) {
//		this.run = run;
//	}
	
}
