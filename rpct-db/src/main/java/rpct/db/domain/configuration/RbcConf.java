package rpct.db.domain.configuration;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import rpct.db.domain.equipment.ChipType;

/**
 * Created on 2009-10-20
 * 
 * @author Nikolay Darmenov
 * @version $Id$
 */
@Entity
@PrimaryKeyJoinColumn(name = "RBCCHIPSTATICCONFID")
@Table(name = "RBCCHIPCONF")
public class RbcConf extends StaticConfiguration {
	private int config = 0;
	private int configIn = 0;
	private int configVer = 0;
	private int majority = 0;
	private int shape = 0;
	private int mask = 0;
	private int ctrl = 0;

	public RbcConf() {
		super();
	}

	public RbcConf(RbcConf conf) {
		super();
		this.config = conf.config;
		this.configIn = conf.configIn;
		this.configVer = conf.configVer;
		this.majority = conf.majority;
		this.shape = conf.shape;
		this.mask = conf.mask;
		this.ctrl = conf.ctrl;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see rpct.db.domain.configuration.StaticConfiguration#getChipType()
	 */
	@Override
	@Transient
	public ChipType getChipType() {
		return ChipType.RBC;
	}

	public int getConfig() {
		return config;
	}

	public void setConfig(int config) {
		this.config = config;
	}

	public int getConfigIn() {
		return configIn;
	}

	public void setConfigIn(int configIn) {
		this.configIn = configIn;
	}

	public int getConfigVer() {
		return configVer;
	}

	public void setConfigVer(int configVer) {
		this.configVer = configVer;
	}

	public int getMajority() {
		return majority;
	}

	public void setMajority(int majority) {
		this.majority = majority;
	}

	public int getShape() {
		return shape;
	}

	public void setShape(int shape) {
		this.shape = shape;
	}

	public int getMask() {
		return mask;
	}

	public void setMask(int mask) {
		this.mask = mask;
	}

	public int getCtrl() {
		return ctrl;
	}

	public void setCtrl(int ctrl) {
		this.ctrl = ctrl;
	}

	@Override
	@Transient
	public String toString() {
		return "RbcConf [config=" + config + ", configIn=" + configIn
				+ ", configVer=" + configVer + ", majority=" + majority
				+ ", shape=" + shape
				+ ", mask=0x" + Integer.toHexString(mask)
				+ ", ctrl=" + ctrl + "]";
	}
}
