package rpct.db.domain.configuration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
/**
 * Created on 2006-09-01
 *
 * @author Karol Bunkowski
 * @version $Id: ConfigKey.java 1563 2010-02-08 12:30:40Z kbunkow $
 */


@Entity
//@Table(name="CONFIGKEY")
public class ConfigKey {
    private String id;
    private String description;
    
    @Id
    @Column(name="CONFIGKEYID")
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
}
