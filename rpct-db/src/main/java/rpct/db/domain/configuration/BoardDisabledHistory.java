package rpct.db.domain.configuration;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import rpct.db.domain.equipment.Board;
/**
 * Created on 2011-06-02
 *
 * @author Karol Bunkowski
 * @version $Id: BoardDisabledHistory.java 1563 2010-02-08 12:30:40Z kbunkow $
 */


@Entity
@SequenceGenerator(name="seq", sequenceName="S_40_1_BOARDDISABLEDHISTORY", allocationSize=1)
public class BoardDisabledHistory {
    private int id;
    private Board board;
    private String status;
    private Date modificationDate = new Date();
    
    @Id
    @Column(name="BOARDDISABLEDHISTORYID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    @ManyToOne
    @JoinColumn(name="BOARD_BOARDID")
    public Board getBoard() {
        return board;
    }
    public void setBoard(Board board) {
        this.board = board;
    }
  
    public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
    public Date getModificationDate() {
        return modificationDate;
    }
    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }
    
    public String toString() {
    	return board.toString() + " " + status + " from " + modificationDate;
    }
}
