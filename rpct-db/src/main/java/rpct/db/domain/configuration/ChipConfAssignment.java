package rpct.db.domain.configuration;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import rpct.db.domain.equipment.Chip;
/**
 * Created on 2006-09-01
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */


@Entity
@SequenceGenerator(name="seq", sequenceName="S_17_1_CHIPCONFASSIGNMENT", allocationSize=1)
public class ChipConfAssignment {
    private int id;
    private LocalConfigKey localConfigKey;
    private StaticConfiguration staticConfiguration;
    private Chip chip;
    
    //private int version;
    private ChipConfigTag tag;
    private Date creationDate = new Date();
    
    @Id
    @Column(name="CHIPCONFASSIGNMENTID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    @ManyToOne
    @JoinColumn(name="LCK_LOCALCONFIGKEYID")
    public LocalConfigKey getLocalConfigKey() {
        return localConfigKey;
    }
    public void setLocalConfigKey(LocalConfigKey localConfigKey) {
        this.localConfigKey = localConfigKey;
    }

    //@ManyToOne(fetch=FetchType.LAZY)
    @ManyToOne()
    @JoinColumn(name="SC_STATICCONFIGURATIONID")
    public StaticConfiguration getStaticConfiguration() {
        return staticConfiguration;
    }    
    public void setStaticConfiguration(StaticConfiguration staticConfiguration) {
        this.staticConfiguration = staticConfiguration;
    }

    //@ManyToOne(fetch=FetchType.LAZY)
    @ManyToOne()
    @JoinColumn(name="CHIP_CHIPID")
    public Chip getChip() {
        return chip;
    }
    public void setChip(Chip chip) {
        this.chip = chip;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    public Date getCreationDate() {
        return creationDate;
    }
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
    
    @ManyToOne
    @JoinColumn(name="TAG")
    public ChipConfigTag getTag() {
        return tag;
    }
    public void setTag(ChipConfigTag tag) {
        this.tag = tag;
    }
    
    /*public int getVersion() {
        return version;
    }
    public void setVersion(int version) {
        this.version = version;
    }*/
}
