package rpct.db.domain.configuration;

import java.util.List;

import rpct.db.DataAccessException;
import rpct.db.domain.DAO;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardBoardConn;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.LinkConn;

/**
 * Created on 2005-03-01
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public interface ConfigurationDAO extends DAO {

    XdaqExecutive getXdaqExecutive(String host, int port) throws DataAccessException;
    XdaqApplication getXdaqApplication(XdaqExecutive executive, String className, int instance) throws DataAccessException;
    XdaqApplication getXdaqApplication(String host, int port, String className, int instance) throws DataAccessException;
    XdaqApplication getXdaqApplication(Crate crate) throws DataAccessException;
    XdaqApplication getXdaqApplication(Board board) throws DataAccessException;
    XdaqApplication getXdaqApplication(Chip chip) throws DataAccessException;
    //XdaqAppLBoxAccess getXdaqApplication(LinkBox linkBox) throws DataAccessException;
    
    List<LocalConfigKey> getLocalConfigKeysBySubsystem(LocalConfigKey.Subsystem subsystem) throws DataAccessException;
    LocalConfigKey getLocalConfigKeyByName(String name) throws DataAccessException;    
    LocalConfigKey getLocalConfigKeyByGlobalConfigKey(String globalConfigKey) throws DataAccessException;
    List<LocalConfigKey> getLocalConfigKeysByGlobalConfigKey(String globalConfigKey) throws DataAccessException;
    List<String> getGlobalConfigKeysByLocalConfigKey(LocalConfigKey localConfigKey) throws DataAccessException;
    List<ConfigKey> getGlobalConfigKeys() throws DataAccessException ;
    ConfigKey getGlobalConfigKeyByName(String name) throws DataAccessException; 
    List<LocalConfigKey> getLocalConfigKeys() throws DataAccessException;
     
    List<Chip> getDisabledChips() throws DataAccessException;    
    List<Integer> getDisabledChipsIds() throws DataAccessException;    
    ChipDisabled getChipDisabled(Chip chip) throws DataAccessException;
    List<ChipDisabledHistory> getChipDisablingHistory(Chip chip) throws DataAccessException;

    List<Integer> getDisabledCrateIds() throws DataAccessException; 
    CrateDisabled getCrateDisabled(Crate crate) throws DataAccessException;
    List<CrateDisabledHistory> getCrateDisablingHistory(Crate crate) throws DataAccessException;
    
    List<Integer> getDisabledBoardIds() throws DataAccessException; 
    BoardDisabled getBoardDisabled(Board board) throws DataAccessException;
    List<BoardDisabledHistory> getBoardDisablingHistory(Board board) throws DataAccessException;
    
    List<Integer> getDisabledLinksIds() throws DataAccessException; 
    LinkDisabled getLinkDisabled(BoardBoardConn linkConn) throws DataAccessException;
    List<LinkDisabledHistory> getLinkDisabledHistory(BoardBoardConn boardBoardConn) throws DataAccessException;

    List<Integer> getFebDtControlledIds() throws DataAccessException; 
    FebBoardDTControlled getFebDtControlled(Board board) throws DataAccessException;
    List<FebDTControlledHistory> getFebDTControlledHistory(Board board) throws DataAccessException;
    
    ChipConfAssignment getChipConfAssignment(Chip chip, 
            LocalConfigKey localConfigKey) throws DataAccessException;
    
	List<ChipConfAssignment> getChipConfAssignments(ChipType chipType,
			LocalConfigKey localConfigKey) throws DataAccessException;
	
    List<ChipConfAssignment> getChipConfAssignments(ChipType chipType,
            ChipConfigTag tag) throws DataAccessException;
	
    List<ChipConfAssignment> getChipConfAssignmentHistory(Chip chip,
            LocalConfigKey localConfigKey) throws DataAccessException; 
    
    List<ChipConfAssignment> getChipConfAssignments(List<Chip> chips, 
            LocalConfigKey localConfigKey) throws DataAccessException;
    
    List<ChipConfAssignment> getChipConfAssignmentsByChipsAndGlobalConfigKey(List<Chip> chips, 
            String globalConfigKey) throws DataAccessException;
    
    List<ChipConfAssignment> getChipConfAssignmentsByChipIdsAndGlobalConfigKey(List<Integer> chipIds, 
            String globalConfigKey) throws DataAccessException;
    
    List<ChipConfAssignment> getChipConfAssignmentsByChipIdsAndLocalConfigKey(List<Integer> chipIds, 
            String localConfigKey) throws DataAccessException;

    ConfigSetInfo getConfigSetInfoByChipIdsAndGlobalConfigKey(List<Integer> chipIds, 
            String globalConfigKey) throws DataAccessException;
    
    ConfigSetInfo getConfigSetInfoByChipIdsAndLocalConfigKey(
            List<Integer> chipIds, String localConfigKey) throws DataAccessException;
/* MichalS */
    CurrentConfigKey getCurrentKey() throws DataAccessException;
    void writeCurrentKey(String currentKeyValue) throws DataAccessException;
/* MichalS */
    
    public ChipConfigTag getChipConfigTag(String tagName) throws DataAccessException; 
    
    public SynCoderConfInFlash getSynCoderConfInFlash(int chipId) throws DataAccessException;
}
