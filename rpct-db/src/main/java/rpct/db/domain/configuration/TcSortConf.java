package rpct.db.domain.configuration;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import rpct.db.domain.equipment.ChipType;
import rpct.xdaq.axis.Binary;


/**
 * Created on 2005-03-08
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */

@Entity
@PrimaryKeyJoinColumn(name="TCSORTCONFID")
@Table(name="TCSORTCONF")
//@SequenceGenerator(name="indexedSettingSeq", sequenceName="S_25_1_INDEXEDSETTING", allocationSize=1)
public class TcSortConf extends StaticConfiguration {

    private byte[] recMuxClkInv; 
    private byte[] recMuxClk90; 
    private byte[] recMuxRegAdd; 
    private IntArray9 recMuxDelay; 
    private IntArray9 recDataDelay; 
    private String bootScript;
    
    public TcSortConf() {
        super();
        
        recMuxClkInv = new byte[11]; 
        recMuxClk90 = new byte[11]; 
        recMuxRegAdd = new byte[11]; 
        recMuxDelay = new IntArray9(); 
        recDataDelay = new IntArray9();
    }

    public TcSortConf(byte[] recMuxClkInv, byte[] recMuxClk90, byte[] recMuxRegAdd,
            IntArray9 recMuxDelay, IntArray9 recDataDelay, String bootScript) {
        super();
        this.recMuxClkInv = recMuxClkInv;
        this.recMuxClk90 = recMuxClk90;
        this.recMuxRegAdd = recMuxRegAdd;
        this.recMuxDelay = recMuxDelay;
        this.recDataDelay = recDataDelay;
        this.bootScript = bootScript;
    }

    public TcSortConf(TcSortConf conf) {
        super();
        this.recMuxClkInv = conf.getRecMuxClkInv().clone();
        this.recMuxClk90 = conf.getRecMuxClk90().clone();
        this.recMuxRegAdd = conf.getRecMuxRegAdd().clone();
        this.recMuxDelay = new IntArray9(conf.getRecMuxDelay().getValues());
        this.recDataDelay = new IntArray9(conf.getRecDataDelay().getValues());
        this.bootScript = new String(conf.getBootScript());
    }
    
    public byte[] getRecMuxClkInv() {
        return recMuxClkInv;
    }

    public void setRecMuxClkInv(byte[] recMuxClkInv) {
        this.recMuxClkInv = recMuxClkInv;
    }

    public byte[] getRecMuxClk90() {
        return recMuxClk90;
    }

    public void setRecMuxClk90(byte[] recMuxClk90) {
        this.recMuxClk90 = recMuxClk90;
    }

    public byte[] getRecMuxRegAdd() {
        return recMuxRegAdd;
    }

    public void setRecMuxRegAdd(byte[] recMuxRegAdd) {
        this.recMuxRegAdd = recMuxRegAdd;
    }

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="IA9_RECMUXDELAYID")
    public IntArray9 getRecMuxDelay() {
        return recMuxDelay;
    }

    public void setRecMuxDelay(IntArray9 recMuxDelay) {
        this.recMuxDelay = recMuxDelay;
    }

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="IA9_RECDATADELAYID")
    public IntArray9 getRecDataDelay() {
        return recDataDelay;
    }

    public void setRecDataDelay(IntArray9 recDataDelay) {
        this.recDataDelay = recDataDelay;
    }    

    public String getBootScript() {
        return bootScript;
    }
    
    public void setBootScript(String bootScript) {
        this.bootScript = bootScript;
    }

    @Override
    @Transient
    public ChipType getChipType() {
        return ChipType.TCSORT;
    }
    
    @Transient
    public String toString() {
    	String str = "bootScript " + bootScript + "\n";
    	Binary b = new Binary(recMuxClkInv);
    	str += "recMuxClkInv " + b.toString() + "\n";
    	b = new Binary(recMuxClk90);
    	str += "recMuxClk90 " + b.toString() + "\n";
    	b = new Binary(recMuxRegAdd);
    	str += "recMuxRegAdd " + b.toString() + "\n";
    	
    	return str;
    }
}
