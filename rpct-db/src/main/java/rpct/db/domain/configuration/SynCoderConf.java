package rpct.db.domain.configuration;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.axis.types.HexBinary;

import rpct.db.domain.equipment.ChipType;
import rpct.xdaq.axis.Binary;


/**
 * Created on 2005-03-08
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */

/**
 * @author rpcdev
 *
 */
@Entity
@PrimaryKeyJoinColumn(name="LINKCHIPSTATICCONFID")
@Table(name="LINKCHIPCONF")
public class SynCoderConf extends StaticConfiguration {

    private int windowOpen = 0;
    private int windowClose = 0;
    private boolean invertClock = false;
    private int lmuxInDelay = 0;
    private int rbcDelay = 0;
    private int bcn0Delay = 0;
    private int dataTrgDelay = 0;
    private int dataDaqDelay = 0;
    private int pulserTimerTrgDelay = 0;
    private byte[] inChannelsEna = HexBinary.decode("ffffffffffffffffffffffff");; 
    
    public SynCoderConf() {
        super();
    }

    public SynCoderConf(SynCoderConf conf) {
        super();
        windowOpen = conf.getWindowOpen();
        windowClose = conf.getWindowClose();
        invertClock = conf.isInvertClock();
        lmuxInDelay = conf.getLmuxInDelay();
        rbcDelay = conf.getRbcDelay();
        bcn0Delay = conf.getBcn0Delay();
        dataTrgDelay = conf.getDataTrgDelay();
        dataDaqDelay = conf.getDataDaqDelay();
        pulserTimerTrgDelay = conf.getPulserTimerTrgDelay();
        inChannelsEna = new byte[conf.getInChannelsEna().length];
        for(int i = 0; i < conf.getInChannelsEna().length; i++) {
            inChannelsEna[i] = conf.getInChannelsEna()[i];
        }
    }

    public int getBcn0Delay() {
        return bcn0Delay;
    }

    public void setBcn0Delay(int bcn0Delay) {
        if(bcn0Delay < 0)
            throw new IllegalArgumentException("bcn0Delay < 0");
        this.bcn0Delay = bcn0Delay;
    }
    
    /**
     * @return we are using this field for storing the optLinkSendDelay
     */
    public int getDataDaqDelay() {
        return dataDaqDelay;
    }

    public void setDataDaqDelay(int dataDaqDelay) {
        this.dataDaqDelay = dataDaqDelay;
    }

    /**
     * @return we are using this field for storing the hits timing!
     */
    public int getDataTrgDelay() {
        return dataTrgDelay;
    }

    public void setDataTrgDelay(int dataTrgDelay) {
        this.dataTrgDelay = dataTrgDelay;
    }

    public boolean isInvertClock() {
        return invertClock;
    }

    public void setInvertClock(boolean invertClock) {
        this.invertClock = invertClock;
    }

    public int getLmuxInDelay() {
        return lmuxInDelay;
    }

    public void setLmuxInDelay(int lmuxInDelay) {
        if(lmuxInDelay < 0)
           throw new IllegalArgumentException("lmuxInDelay < 0");
        this.lmuxInDelay = lmuxInDelay;
    }

    public int getPulserTimerTrgDelay() {
        return pulserTimerTrgDelay;
    }

    public void setPulserTimerTrgDelay(int pulserTimerTrgDelay) {
        this.pulserTimerTrgDelay = pulserTimerTrgDelay;
    }

    public int getRbcDelay() {
        return rbcDelay;
    }

    public void setRbcDelay(int rbcDelay) {
        if(rbcDelay < 0)
            throw new IllegalArgumentException("rbcDelay < 0");
        this.rbcDelay = rbcDelay;
    }

    public int getWindowClose() {
        return windowClose;
    }

    public void setWindowClose(int windowClose) {
        this.windowClose = windowClose;
    }

    public int getWindowOpen() {
        return windowOpen;
    }

    public void setWindowOpen(int windowOpen) {
        this.windowOpen = windowOpen;
    }

    public byte[] getInChannelsEna() {
        return inChannelsEna;
    }
    

    public void setInChannelsEna(byte[] inChannelsEna) {
        this.inChannelsEna = inChannelsEna;
    }

    @Transient
    public String getInChannelsEnaStr() {
    	Binary b = new Binary(inChannelsEna);
        return b.toString();
    }
    
    @Transient
    public boolean equalInChannelsEna(byte[] inChannelsEna) {
    	for(int i = 0; i < this.inChannelsEna.length; i++) {
    		if(this.inChannelsEna[i] != inChannelsEna[i])
    			return false;
    	}
        return true;
    }
    
    @Override
    @Transient
    public ChipType getChipType() {
        return ChipType.SYNCODER;
    }
    
    @Transient 
    @Override
    public String toString() {
    	String str;
    	str = 
    	"id  " + getId() +
        "\nwindowOpen " + windowOpen +
        "\nwindowClose " + windowClose +
        "\ninvertClock " + invertClock +
        "\nlmuxInDelay " + lmuxInDelay +
        "\nrbcDelay " + rbcDelay +
        "\nbcn0Delay " + bcn0Delay +
        "\nOptLinkSendDelay " + getOptLinkSendDelay() +
    	"\ninChannelsEna " + getInChannelsEnaStr();
        //"\ndataTrgDelay " + dataTrgDelay +
        //"\ndataDaqDelay " + dataDaqDelay +
        //"\npulserTimerTrgDelay " + pulserTimerTrgDelay +
        //"\ninChannelsEna " + inChannelsEna;
    	return str;
    }
    
    @Transient 
    public double getTiming() {
        return ((double)dataTrgDelay)/100.;
    }

    @Transient 
    public void setTiming(double timing) {
        dataTrgDelay = (int)Math.round(timing * 100);
    }
    
    @Transient 
    public int getOptLinkSendDelay() {
        return dataDaqDelay;
    }
    
    @Transient
    public void setOptLinkSendDelay(int sendDelay) {
        dataDaqDelay = sendDelay;
    }

}
