package rpct.db.domain.configuration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import rpct.db.domain.equipment.Crate;
/**
 * Created on 2007-05-21
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */


@Entity
@SequenceGenerator(name="seq", sequenceName="S_28_1_CRATEDISABLED", allocationSize=1)
public class CrateDisabled {
    private int id;
    private Crate crate;
    
    @Id
    @Column(name="CRATEDISABLEDID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    @ManyToOne
    @JoinColumn(name="CRATE_CRATEID")
    public Crate getCrate() {
        return crate;
    }
    public void setCrate(Crate crate) {
        this.crate = crate;
    }
}
