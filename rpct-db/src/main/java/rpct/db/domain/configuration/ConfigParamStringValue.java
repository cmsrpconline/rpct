package rpct.db.domain.configuration;

/**
 * Created on 2005-04-05
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class ConfigParamStringValue extends ConfigParamValue {

    private String stringValue;
        
    @Override
    public Object getValue() {
        return stringValue;
    }

    @Override
    public void setValue(Object value) {
        this.stringValue = (String) value;
    }

    @Override
    protected void checkType(ConfigParamDefinition definition) {
        if (definition.getType() != ConfigParamType.STRING)
            throw new IllegalArgumentException("Illegal type");
    }

    public String getStringValue() {
        return stringValue;
    }
    

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }
}
