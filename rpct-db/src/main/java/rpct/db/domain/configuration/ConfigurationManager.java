package rpct.db.domain.configuration;

import java.util.ArrayList;
import java.util.List;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.LocalConfigKey.Subsystem;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardBoardConn;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.controlboard.ControlBoard;
import rpct.db.domain.hibernate.HibernateContext;

public class ConfigurationManager {
	//obsolete keys - for whole system
/*    public final static String LOCAL_CONF_KEY_DEFAULT = "DEFAULT";
    public final static String LOCAL_CONF_KEY_COSMIC = "COSMIC";
    public final static String LOCAL_CONF_KEY_COSMIC1 = "COSMIC1";
    public final static String LOCAL_CONF_KEY_COSMIC_TIGHT = "COSMIC_TIGHT";

    public final static String LOCAL_CONF_KEY_LHC_COSMIC1 = "LHC_COSMIC1";
    public final static String LOCAL_CONF_KEY_LHC_COSMIC1_2BX = "LHC_COSMIC1_2BX";
    public final static String LOCAL_CONF_KEY_LHC_COSMIC1_3BX = "LHC_COSMIC1_3BX";
    public final static String LOCAL_CONF_KEY_LHC_COSMIC_BOTTOM1 = "LHC_COSMIC_BOTTOM1";

    public final static String LOCAL_CONF_KEY_LHC_COSMIC2 = "LHC_COSMIC2";
    public final static String LOCAL_CONF_KEY_LHC_COSMIC2_2BX = "LHC_COSMIC2_2BX";
    public final static String LOCAL_CONF_KEY_LHC_COSMIC2_3BX = "LHC_COSMIC2_3BX";
    public final static String LOCAL_CONF_KEY_LHC_COSMIC2_BOTTOM = "LHC_COSMIC2_BOTTOM";
    */
    public final static String LOCAL_CONF_KEY_LHC3 = "LHC3";
    public final static String LOCAL_CONF_KEY_LHC3_2BX = "LHC3_2BX";
    public final static String LOCAL_CONF_KEY_LHC3_3BX = "LHC3_3BX";
    public final static String LOCAL_CONF_KEY_LHC3_BOTTOM = "LHC3_BOTTOM";

    //new config keys - for subsystems
    public final static String LOCAL_CONF_KEY_LBS_DEFAULT = "LBS_DEFAULT";
    public final static String LOCAL_CONF_KEY_TCS_DEFAULT = "TCS_DEFAULT";
    
    public final static String LOCAL_CONF_KEY_FEBS_DEFAULT = "FEBS_DEFAULT";
    public final static String LOCAL_CONF_KEY_FEBS_TEST_CONF = "FEBS_TEST_CONF";    
    public final static String LOCAL_CONF_KEY_FEBS_240mV = "FEBS_240MV";
    public final static String LOCAL_CONF_KEY_FEBS_220mV = "FEBS_220MV";
    public final static String LOCAL_CONF_KEY_FEBS_215mV = "FEBS_215MV";
    public final static String LOCAL_CONF_KEY_FEBS_210mV = "FEBS_210MV";     
    public final static String LOCAL_CONF_KEY_FEBS_200mV = "FEBS_200MV";
    public final static String LOCAL_CONF_KEY_FEBS_195mV = "FEBS_195MV";
    public final static String LOCAL_CONF_KEY_FEBS_190mV = "FEBS_190MV"; 
    public final static String LOCAL_CONF_KEY_FEBS_10mV = "FEBS_10MV"; 
    public final static String LOCAL_CONF_KEY_FEBS_MIN_THR = "FEBS_MIN_THR";
    
/////////////////////////////////////////////////////////////////////////    
    public final static String LOCAL_CONF_KEY_TCS_V1022 = "TCS_V1022";
    public final static String LOCAL_CONF_KEY_TCS_V1026 = "TCS_V1026";
    public final static String LOCAL_CONF_KEY_TCS_V1029 = "TCS_V1029";
    public final static String LOCAL_CONF_KEY_TCS_V1030 = "TCS_V1030";
    public final static String LOCAL_CONF_KEY_TCS_V1035 = "TCS_V1035";
    public final static String LOCAL_CONF_KEY_TCS_V1036 = "TCS_V1036";
    
    public final static String LOCAL_CONF_KEY_TCS_V1038 = "TCS_V1038";
    public final static String LOCAL_CONF_KEY_TCS_V1038_1EX = "TCS_V1038_1EX";

    public final static String LOCAL_CONF_KEY_TCS_V1039 = "TCS_V1039";
    public final static String LOCAL_CONF_KEY_TCS_V1039_1EX = "TCS_V1039_1EX";
    
    public final static String LOCAL_CONF_KEY_TCS_V1039_plus12BX = "TCS_V1039_12BX"; //plus 12 BX in the RMB_DATA_DELAY (DAQ delay)

    public final static String LOCAL_CONF_KEY_TCS_V1046 = "TCS_V1046";//includes plus 12 BX in the RMB_DATA_DELAY (DAQ delay)
    public final static String LOCAL_CONF_KEY_TCS_V1047 = "TCS_V1047";//includes plus 12 BX in the RMB_DATA_DELAY (DAQ delay)
    public final static String LOCAL_CONF_KEY_TCS_V1049 = "TCS_V1049";//includes plus 12 BX in the RMB_DATA_DELAY (DAQ delay)
    

    public final static String CURRENT_TCS_CONF_KEY = LOCAL_CONF_KEY_TCS_V1049; //TODO !!!!    
        
/////////////////////////////////////////////////////////////////////////    

    public final static String LOCAL_CONF_KEY_SC_FULL = "SC_FULL";
    public final static String LOCAL_CONF_KEY_SC_BOTTOM = "SC_BOTTOM";
    public final static String LOCAL_CONF_KEY_SC_TOP = "SC_TOP";

/////////////////////////////////////////////////////////////////////////
    public final static String LOCAL_CONF_KEY_LBS_LHC1 = "LBS_LHC1";
    public final static String LOCAL_CONF_KEY_LBS_LHC2 = "LBS_LHC2";
    
    public final static String CURRENT_LBS_CONF_KEY = LOCAL_CONF_KEY_LBS_LHC2; //TODO !!!!    
/////////////////////////////////////////////////////////////////////////    
    
    public final static String LOCAL_CONF_KEY_RBCS_TTUS_LHC1 = "RBCS_TTUS_LHC1"; 

    private HibernateContext hibernateContext;
    private EquipmentDAO equipmentDAO;
    private ConfigurationDAO configurationDAO;
    private LocalConfigKey defaultConfigKey;

    public ConfigurationManager(HibernateContext hibernateContext,
            EquipmentDAO equipmentDAO, ConfigurationDAO configurationDAO) {
        super();
        this.hibernateContext = hibernateContext;
        this.equipmentDAO = equipmentDAO;
        this.configurationDAO = configurationDAO;
    }

    public ConfigKey createGlobalConfigKey(String id, String description) throws DataAccessException {
        ConfigKey globalConfigKey = new ConfigKey();
        globalConfigKey.setId(id);
        globalConfigKey.setDescription(description);
        configurationDAO.saveObject(globalConfigKey);
        return globalConfigKey;
    }
    
    public LocalConfigKey getLocalConfigKey(String localConfigKey)
            throws DataAccessException {
        LocalConfigKey key = configurationDAO
                .getLocalConfigKeyByName(localConfigKey);
        if (key == null) {
           throw new IllegalArgumentException("no localConfigKey "  + localConfigKey + " in the DB");
        }
        return key;
    }
    
    public LocalConfigKey createLocalConfigKey(String localConfigKey, LocalConfigKey.Subsystem subsystem) throws DataAccessException {
    	LocalConfigKey key = configurationDAO.getLocalConfigKeyByName(localConfigKey);
    	if (key == null) {
    		key = new LocalConfigKey();
    		key.setName(localConfigKey);
    		key.setSubsystem(subsystem);
    		configurationDAO.saveObject(key);
    		configurationDAO.flush();
    	}
    	else {
    		throw new IllegalArgumentException("localConfigKey "  + localConfigKey + " already exist in the DB");
    	}
    	return key;
    }

    public LocalConfigKey getCurrnetLBsConfigKey() throws DataAccessException {
    	return getLocalConfigKey(CURRENT_LBS_CONF_KEY);
    }
    /**
     * obsolete, to be removed
     * @return
     * @throws DataAccessException
     */
    public LocalConfigKey getDefaultLocalConfigKey() throws DataAccessException {
    	throw new RuntimeException("DefaultLocalConfigKey is obsolete, do not use it");
/*        if (defaultConfigKey == null) {
            defaultConfigKey = configurationDAO
                    .getLocalConfigKeyByName(LOCAL_CONF_KEY_DEFAULT);
            if (defaultConfigKey == null) {
                defaultConfigKey = new LocalConfigKey();
                defaultConfigKey.setName(LOCAL_CONF_KEY_DEFAULT);
                configurationDAO.saveObject(defaultConfigKey);
                configurationDAO.flush();
            }
        }
        return defaultConfigKey;*/
    }
    
    public List<LocalConfigKey> getLocalConfigKeys() throws DataAccessException {
    	List<LocalConfigKey> localConfigKeys = configurationDAO.getObjects(LocalConfigKey.class);
    	return localConfigKeys;
	}

    public void assignConfigKey(String globalConfigKey, LocalConfigKey localConfigKey) throws DataAccessException {

/*        LocalConfigKey lck = configurationDAO.getLocalConfigKeyByGlobalConfigKey(globalConfigKey);
        if (lck == null) {       
            ConfigKeyAssignment configKeyAssignment = new ConfigKeyAssignment();
            configKeyAssignment.setConfigKey(globalConfigKey);
            configKeyAssignment.setLocalConfigKey(localConfigKey);
            configurationDAO.saveObject(configKeyAssignment);   
        }
        else if (!lck.equals(localConfigKey)) {
            throw new RuntimeException("Not yet implemented");
        }*/
        
    	if(localConfigKey ==  null) {
    		throw new IllegalArgumentException("localConfigKey ==  null");
    	}
    	configurationDAO.getGlobalConfigKeyByName(globalConfigKey); //to check if the key exists
        ConfigKeyAssignment configKeyAssignment = new ConfigKeyAssignment();
        configKeyAssignment.setConfigKey(globalConfigKey);
        configKeyAssignment.setLocalConfigKey(localConfigKey);
        configurationDAO.saveObject(configKeyAssignment); 
    }

    public void assignConfigKey(String globalConfigKey, String localConfigKey) throws DataAccessException {
        assignConfigKey(globalConfigKey, configurationDAO.getLocalConfigKeyByName(localConfigKey));
        System.out.println("assinging globalConfigKey " + globalConfigKey + " localConfigKey " + localConfigKey);
    }
    
    public void assignConfigKey(ConfigKey globalConfigKey, LocalConfigKey localConfigKey) throws DataAccessException {
    	assignConfigKey(globalConfigKey.getId(), localConfigKey);
    }

    /*
     * public LocalConfigKey getDefaultConfigKey() throws DataAccessException {
     * LocalConfigKey localConfigKey =
     * configurationDAO.getLocalConfigKeyByGlobalConfigKey("DEFAULT"); if
     * (localConfigKey == null) { localConfigKey = getDefaultLocalConfigKey();
     * ConfigKeyAssignment configKeyAssignment = new ConfigKeyAssignment();
     * configKeyAssignment.setConfigKey("DEFAULT");
     * configKeyAssignment.setLocalConfigKey(localConfigKey);
     * configurationDAO.saveObject(configKeyAssignment); } return
     * localConfigKey; }
     */

    // public PacConf createPacConfiguration(byte[] recMuxClkInv,
    // byte[] recMuxRegAdd,
    // int[] recMuxDelay,
    // int[] recDataDelay) {
    // IntArray18 iaRecMuxDelay = new IntArray18();
    // iaRecMuxDelay.setValues(recMuxDelay);
    // configurationDAO.
    // }
    /**
     * Runs in a separate transaction
     * 
     * @param chips
     * @param conf
     * @param localConfigKey
     * @throws DataAccessException
     */
    public void assignConfiguration(List<Chip> chips, StaticConfiguration conf,
    		LocalConfigKey key) throws DataAccessException {
        if (chips == null) {
            throw new NullPointerException("chips == null");
        }
        if (conf == null) {
            throw new NullPointerException("conf == null");
        }
        try {
            for (Chip chip : chips) {
                if (chip.getType() != conf.getChipType()) {
                    throw new IllegalArgumentException(
                            "Configuration does not match chip type: chip type = "
                                    + chip.getType()
                                    + ", configuration is for chip type = "
                                    + conf.getChipType());
                }

                ChipConfAssignment a = new ChipConfAssignment();
                a.setChip(chip);
                a.setStaticConfiguration(conf);
                a.setLocalConfigKey(key);
                configurationDAO.saveObject(a);
            }
        } catch (DataAccessException e) {
            // hibernateContext.rollback();
            throw e;
        } finally {
            // hibernateContext.closeSession();
        }
    }

    public void assignConfiguration(Chip chip, StaticConfiguration conf,
    		LocalConfigKey localConfigKey) throws DataAccessException {
        List<Chip> l = new ArrayList<Chip>(1);
        l.add(chip);
        assignConfiguration(l, conf, localConfigKey);
    }
    
    public void assignConfiguration(Chip chip, StaticConfiguration conf,
            String localConfigKey) throws DataAccessException {
    	LocalConfigKey key = getLocalConfigKey(localConfigKey);
    	assignConfiguration(chip, conf, key);
    }
    

    public ConfigurationDAO getConfigurationDAO() {
        return configurationDAO;
    }

    public StaticConfiguration getConfiguration(Chip chip,
            LocalConfigKey localConfigKey) throws DataAccessException {
        ChipConfAssignment assignment = configurationDAO.getChipConfAssignment(
                chip, localConfigKey);
        if (assignment == null) {
            throw new NullPointerException("ChipConfAssignment is null fo chip " + chip.getBoard().getName() + " " + chip.getType());
        }
        return assignment.getStaticConfiguration();
    }

    public boolean enableBoard(Board board, boolean enable)
            throws DataAccessException {
        if(board == null) {
            throw new NullPointerException("board is null");
        }
            
        boolean wasChanged = false;
        BoardDisabled boardDisabled = configurationDAO.getBoardDisabled(board);
        if (!enable && boardDisabled == null) {
            boardDisabled = new BoardDisabled();
            boardDisabled.setBoard(board);
            configurationDAO.saveObject(boardDisabled);
            System.out.println("Disabled board " + board.getName());
            wasChanged = true;
        } else if (enable && boardDisabled != null) {
            configurationDAO.deleteObject(boardDisabled);
            System.out.println("Enabled board " + board.getName());
            wasChanged = true;
        }
        return wasChanged;
    }

    public boolean enableCrate(Crate crate, boolean enable)
            throws DataAccessException {
        if(crate == null) {
            throw new NullPointerException("crate is null");
        }
        
        boolean wasChanged = false;
        CrateDisabled crateDisabled = configurationDAO.getCrateDisabled(crate);
        if (!enable && crateDisabled == null) {
            crateDisabled = new CrateDisabled();
            crateDisabled.setCrate(crate);
            configurationDAO.saveObject(crateDisabled);
            System.out.println("Disabled crate " + crate.getName());
            wasChanged = true;
        } else if (enable && crateDisabled != null) {
            configurationDAO.deleteObject(crateDisabled);
            System.out.println("Enabled crate " + crate.getName());
            wasChanged = true;
        }
        return wasChanged;
    }

    public void enableChip(Chip chip, boolean enable)
            throws DataAccessException {
        if(chip == null) {
            throw new NullPointerException("chip is null");
        }
        ChipDisabled chipDisabled = configurationDAO.getChipDisabled(chip);
        if (!enable && chipDisabled == null) {
            chipDisabled = new ChipDisabled();
            chipDisabled.setChip(chip);
            configurationDAO.saveObject(chipDisabled);
            System.out.println("Disabled chip " + chip.getBoard().getName() + "." +  chip.getType());
        } else if (enable && chipDisabled != null) {
            configurationDAO.deleteObject(chipDisabled);
            System.out.println("Enabled chip " + chip.getBoard().getName() + "." +  chip.getType());
        }
    }

    public void enableLinkConn(BoardBoardConn linkConn, boolean enable)
            throws DataAccessException {
        if(linkConn == null) {
            throw new NullPointerException("linkConn is null");
        }
        LinkDisabled linkDisabled = configurationDAO.getLinkDisabled(linkConn);
        if (!enable && linkDisabled == null) {
            linkDisabled = new LinkDisabled();
            linkDisabled.setLinkConn(linkConn);
            configurationDAO.saveObject(linkDisabled);
            System.out.println("Disabled linkConn " + linkConn);
        } else if (enable && linkDisabled != null) {
        	if(linkDisabled.getDisconnected() == 1) {
        		System.out.println("linkConn " + linkConn + " is disconnected, thus will not be enabled");
        	}
        	else {
        		configurationDAO.deleteObject(linkDisabled);
        		System.out.println("Enabled linkConn " + linkConn);
        	}
        }
    }
    
    public void disconnectLinkConn(BoardBoardConn linkConn) throws DataAccessException {
    	if(linkConn == null) {
            throw new NullPointerException("linkConn is null");
        }
        LinkDisabled linkDisabled = configurationDAO.getLinkDisabled(linkConn);
        if (linkDisabled == null) {
            linkDisabled = new LinkDisabled();
            linkDisabled.setLinkConn(linkConn);
        }
        linkDisabled.setDisconnected(1);
        configurationDAO.saveObject(linkDisabled);
        System.out.println("Disconnecting linkConn " + linkConn);
    }
    
    public void enableLinkConn(TriggerBoard triggerBoard, int triggerBoardInputNum, boolean enable)
    throws DataAccessException {
        if(triggerBoard == null) {
            throw new NullPointerException("triggerBoard is null");
        }
        enableLinkConn(equipmentDAO.getLinkConnByTriggerBoardAndInputNum(triggerBoard, triggerBoardInputNum), 
                enable);       
    }

    /**
     * @param linkBoard
     * @param enable
     * @throws DataAccessException
     * enables/disables the LB, and if is a master, also its slaves and LinkConn
     */
    public boolean enableLinkBoard(LinkBoard linkBoard, boolean enable) throws DataAccessException {
        if(linkBoard == null) {
            throw new NullPointerException("linkBoard is null");
        }
        
        if(linkBoard.getControlBoard().getDisabled() != null) {
            System.err.println("the Control Board for the selected Link Board " + linkBoard.getName() + " is disabled. Cannot enable or disable that Link Board");
            return false;
        }
        
        boolean wasChanged = false;
        if(linkBoard.isMaster()) {
            wasChanged = enableBoard(linkBoard, enable);
            if(wasChanged) {
                System.out.println("the selected board " + linkBoard.getName() + " is a master Link Board. The corresponding slave LBs will be also enabled/disabled, as well as the LinkConnetions." );
                for(LinkBoard slave : linkBoard.getSlaves()) {
                    enableBoard(slave, enable);
                }

                for(LinkConn conn : linkBoard.getLinkConns()) {
                    enableLinkConn(conn, enable);
                }
            }
            System.out.println();
        }
        else {
            if(linkBoard.getMaster().getDisabled() != null) {
                System.err.println("the Master LB for the selected Link Board " + linkBoard.getName() + " is disabled. Cannot enable or disable that Link Board");
            }
            else
                wasChanged = enableBoard(linkBoard, enable);
        }
        return wasChanged;
    }
    
    /**
     * @param linkBox
     * @param enable
     * @throws DataAccessException
     * enables/disables the LinkBox, all its LB and LinkConn
     */
    public boolean enableLinkBox(LinkBox linkBox, boolean enable) throws DataAccessException {
        if(linkBox == null) {
            throw new NullPointerException("linkBox is null");
        }

        boolean wasChanged = enableCrate(linkBox, enable);
        boolean wasLinksChanged = false;

        if(wasChanged) {
            System.out.println("the selected crate " + linkBox.getName() + " is a LINKBOX. The corresponding LBs and LinkConnetions will be also enabled/disable" );
            enableBoard(linkBox.getControlBoard10(), enable);
            enableBoard(linkBox.getControlBoard20(), enable);
            
            for(LinkBoard linkBoard : linkBox.getLinkBoards()) {
                enableBoard(linkBoard, enable);
                if(linkBoard.isMaster()) {                    
                    for(LinkConn conn : linkBoard.getLinkConns()) {
                        enableLinkConn(conn, enable);
                    }
                    wasLinksChanged = true;
                }
            }
            
        }

        return wasLinksChanged;
    }
    
    public boolean enableHalfBox(LinkBox linkBox, int halfBoxId, boolean enable) throws DataAccessException {
        if(linkBox == null) {
            throw new NullPointerException("linkBox is null");
        }
        
        if(linkBox.getDisabled() != null) {
            System.err.println("the selected Half Box " + halfBoxId + " is in  crate " + linkBox.getName() + " which is disabled now. Connot enable or disbale that Half Box");
            return false;
        }
        
        if(enable)
            System.out.println("enabling halfBox " + linkBox.getName() + " " + halfBoxId);
        else 
            System.out.println("disabling halfBox " + linkBox.getName() + " " + halfBoxId);
        
        ControlBoard controlBoard = null;
        if(halfBoxId == 10)
            controlBoard = linkBox.getControlBoard10();
        else if(halfBoxId == 20)
            controlBoard = linkBox.getControlBoard20();
        else {
            throw new IllegalArgumentException("halfBoxId is not 10 nor 20");
        }
        
        boolean wasLinksChanged = false;
        if(controlBoard != null) {
            enableBoard(controlBoard, enable);
            for(LinkBoard linkBoard : linkBox.getLinkBoards()) {
                if(linkBoard.getPosition() >= halfBoxId - 10  && linkBoard.getPosition() < halfBoxId) {
                    enableBoard(linkBoard, enable);
                    if(linkBoard.isMaster()) {                    
                        for(LinkConn conn : linkBoard.getLinkConns()) {
                            enableLinkConn(conn, enable);
                        }
                        wasLinksChanged = true;
                    }                              
                }
            }     
        }
        
        return wasLinksChanged;
    }

    public boolean setFebDtControlled(Board board, boolean dtControlled)
    throws DataAccessException {
    	if(board == null) {
    		throw new NullPointerException("board is null");
    	}

    	boolean wasChanged = false;
    	FebBoardDTControlled febBoardDTControlled = configurationDAO.getFebDtControlled(board);
    	if (dtControlled && febBoardDTControlled == null) {
    		febBoardDTControlled = new FebBoardDTControlled();
    		febBoardDTControlled.setBoard(board);
    		configurationDAO.saveObject(febBoardDTControlled);
    		System.out.println("FebBoard " + board.getName() + " is dtControlled now");
    		wasChanged = true;
    	} else if (!dtControlled && febBoardDTControlled != null) {
    		configurationDAO.deleteObject(febBoardDTControlled);
    		System.out.println("FebBoard " + board.getName() + " is not dtControlled now");
    		wasChanged = true;
    	}
    	return wasChanged;
    }
    
    public void tagCurrentConfigs(LocalConfigKey localConfigKey, ChipType chipType, String tagName, String comment) throws DataAccessException {
    	if(configurationDAO.getChipConfigTag(tagName) != null) {
    		throw new IllegalArgumentException("Tag with name " + tagName + " already exists in the DB");
    	}
    	ChipConfigTag tag = new ChipConfigTag(tagName, comment);
    	configurationDAO.saveObject(tag);
    	List<ChipConfAssignment> assignments = configurationDAO.getChipConfAssignments(chipType, localConfigKey);
    	for(ChipConfAssignment assignment : assignments) {
    		if(assignment.getTag() != null) {
    			System.out.println(assignment.getChip() + " " + assignment.getChip().getBoard().getName() + 
    					" overwritig tag: " + assignment.getTag());
    		}
    		assignment.setTag(tag);
    	}
    }
}