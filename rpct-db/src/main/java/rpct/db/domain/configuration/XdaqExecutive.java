package rpct.db.domain.configuration;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;


@Entity
@SequenceGenerator(name="seq", sequenceName="S_19_1_XDAQEXECUTIVE", allocationSize=1)
public class XdaqExecutive {
    private int id;
    private String host;
    private int port;
    private List<XdaqApplication> xdaqApplications;
    
    public XdaqExecutive() {
    }
    
    @Id
    @Column(name="xdaqexecutiveid")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    
    public String getHost() {
        return host;
    }
    public void setHost(String host) {
        this.host = host;
    }
    public int getPort() {
        return port;
    }
    public void setPort(int port) {
        this.port = port;
    }

    @OneToMany(mappedBy="xdaqExecutive")
    public List<XdaqApplication> getXdaqApplications() {
        return xdaqApplications;
    }
    public void setXdaqApplications(List<XdaqApplication> xdaqApplications) {
        this.xdaqApplications = xdaqApplications;
    }   
    
    public String toString() {
        StringBuilder str = new StringBuilder(getClass().toString());
        str.append(" id = ").append(id).append(", host = ").append(host)
            .append(", port = ").append(port);
        return str.toString();
    }
}