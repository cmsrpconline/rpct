package rpct.db.domain.configuration;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.sun.xml.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.TcHsbConnection;
import rpct.xdaq.axis.Binary;


/**
 * Created on 2008-04-23
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */

@Entity
@PrimaryKeyJoinColumn(name="HALFSORTCONFID")
@Table(name="HALFSORTCONF")
public class HalfSortConf extends StaticConfiguration {

    private byte[] recChanEna; 
    private IntArray16 recFastClkInv; 
    private IntArray16 recFastClk90; 
    private IntArray16 recFastRegAdd; 
    private byte[] recFastDataDelay; 
    private byte[] recDataDelay; 
    
    public HalfSortConf() {
        super();
    }    

    public HalfSortConf(byte[] recChanEna, IntArray16 recFastClkInv, IntArray16 recFastClk90, 
            IntArray16 recFastRegAdd, byte[] recFastDataDelay, byte[] recDataDelay) {
        super();
        this.recChanEna = recChanEna;
        this.recFastClkInv = recFastClkInv;
        this.recFastClk90 = recFastClk90;
        this.recFastRegAdd = recFastRegAdd;
        this.recFastDataDelay = recFastDataDelay;
        this.recDataDelay = recDataDelay;
    }
    
    public HalfSortConf(HalfSortConf halfSortConf) {
        this(halfSortConf.getRecChanEna().clone(),
             new IntArray16(halfSortConf.getRecFastClkInv()),
             new IntArray16(halfSortConf.getRecFastClk90()),
             new IntArray16(halfSortConf.getRecFastRegAdd()),
             halfSortConf.getRecFastDataDelay().clone(),
             halfSortConf.getRecDataDelay().clone());
    }

    public byte[] getRecChanEna() {
        return recChanEna;
    }

    public void setRecChanEna(byte[] recChanEna) {
        this.recChanEna = recChanEna;
    }

    @Transient
    public int getRecChanEnaMask() {
        int mask = 0xff & recChanEna[0] ;
        mask = mask << Byte.SIZE;
        int m0 = 0xff & recChanEna[1];
        mask = mask | m0 ;               
        return mask;
    }
    
    @Transient
    public String getRecChanEnaStr(String hsb_name) {
        int recChanEnaMask = getRecChanEnaMask();
        String str = new String();
        int mask = 3;
        int pos = 0;
        List<String> tcHsbConnections = TcHsbConnection.getTcHsbConnectionMap().get(hsb_name);
        for(String tc : tcHsbConnections) {
            int ena = ( (recChanEnaMask & mask) >> pos ) & 3;
            pos += 2;
            mask = mask << 2;            
            str += tc + " " + ena + "\n";
        }
        
        return str;
    }

        @Transient
    public int getRecChanEnaFlag(String hsb_name, String tc_name) {
        int recChanEnaMask = getRecChanEnaMask();
        int mask = 3;
        int pos = 0;
        List<String> tcHsbConnections = TcHsbConnection.getTcHsbConnectionMap().get(hsb_name);
        /* we loop through all tc's in give hsb to find the right one
         * perhaps could be done better, but this was copy&paste from getRecCHanEnaStr
         */
        for(String tc : tcHsbConnections) {
            int ena = ( (recChanEnaMask & mask) >> pos ) & 3;
            pos += 2;
            mask = mask << 2;
            
            if (tc.equals(tc_name))
                /* tc found */
                return ena;
        }

        /* tc_name not found in given hsb */
        throw new RuntimeException("invalid tc name " + tc_name + "on hsb " + hsb_name);
    }
    
    @Transient
    public void setRecChanEna(int recChanEnaMask) {
        recChanEna[0] = (byte)( (0xff00 & recChanEnaMask) >> 8);
        recChanEna[1] = (byte)(0xff & recChanEnaMask); //!!! odwrotna numeracja bytow w tablicy!!!
                
        //recChanEna[0] = (byte)(0xff & recChanEnaMask); 
        //recChanEna[1] = (byte)( (0xff00 & recChanEnaMask) >> 8);
    }
    
    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="IA16_RECFASTCLKINVID")
    public IntArray16 getRecFastClkInv() {
        return recFastClkInv;
    }

    public void setRecFastClkInv(IntArray16 recFastClkInv) {
        this.recFastClkInv = recFastClkInv;
    }

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="IA16_RECFASTCLK90ID")
    public IntArray16 getRecFastClk90() {
        return recFastClk90;
    }

    public void setRecFastClk90(IntArray16 recFastClk90) {
        this.recFastClk90 = recFastClk90;
    }

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="IA16_RECFASTREGADDID")
    public IntArray16 getRecFastRegAdd() {
        return recFastRegAdd;
    }

    public void setRecFastRegAdd(IntArray16 recFastRegAdd) {
        this.recFastRegAdd = recFastRegAdd;
    }

    public byte[] getRecFastDataDelay() {
        return recFastDataDelay;
    }

    public void setRecFastDataDelay(byte[] recFastDataDelay) {
        this.recFastDataDelay = recFastDataDelay;
    }

    public byte[] getRecDataDelay() {
        return recDataDelay;
    }

    public void setRecDataDelay(byte[] recDataDelay) {
        this.recDataDelay = recDataDelay;
    }

    @Override
    @Transient
    public ChipType getChipType() {
        return ChipType.HALFSORTER;
    }
    
    @Transient
    public String toString() {
    	///return "recChanEna: "+ Integer.toHexString(recChanEna[1]) + " " + Integer.toHexString(recChanEna[0]);
    	
    	Binary b = new Binary(recDataDelay);
    	String str = 
    			"recDataDelay: " + b.toString() + "\n" +
    			"recChanEna: " + Integer.toHexString(recChanEna[0]) + " " + Integer.toHexString(recChanEna[1]) + "\n" ;
    	
    	b = new Binary(recFastDataDelay);
    	str += "recFastDataDelay: " + b.toString() + "\n";
    	
    	str += "recFastClk90: " + recFastClk90 + "\n";
    	str += "recFastClkInv: " + recFastClkInv + "\n";
    	str += "recFastRegAdd: " + recFastRegAdd + "\n";
    	
    	return str;
    }
    
}
