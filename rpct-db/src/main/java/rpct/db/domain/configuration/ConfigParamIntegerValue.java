package rpct.db.domain.configuration;

/**
 * Created on 2005-04-05
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class ConfigParamIntegerValue extends ConfigParamValue {

    private int intValue;
    
    @Override
    public Object getValue() {
        return intValue;
    }
    
    @Override
    public void setValue(Object value) {
        this.intValue = (Integer) value;
    }

    @Override
    protected void checkType(ConfigParamDefinition definition) {
        if (definition.getType() != ConfigParamType.INTEGER)
            throw new IllegalArgumentException("Illegal type");
    }

    public int getIntValue() {
        return intValue;
    }
    

    public void setIntValue(int intValue) {
        this.intValue = intValue;
    }    
}
