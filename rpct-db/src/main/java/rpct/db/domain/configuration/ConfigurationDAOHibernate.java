package rpct.db.domain.configuration;

import java.util.Collections;
import java.util.List;

import javax.management.RuntimeErrorException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import org.hibernate.transform.Transformers;
import org.hibernate.type.IntegerType;
import org.hibernate.type.TimestampType;

import rpct.db.DataAccessException;
import rpct.db.domain.DAOHibernate;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardBoardConn;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.hibernate.HibernateContext;

/**
 * Created on 2005-03-01
 * 
 * @author Michal Pietrusinski
 * @version $Id: ConfigurationDAOHibernate.java,v 1.11 2007/09/24 15:26:34 tb
 *          Exp $
 */
public class ConfigurationDAOHibernate extends DAOHibernate implements
        ConfigurationDAO {

	private final static Log log = LogFactory.getLog(ConfigurationDAOHibernate.class);
	
    public ConfigurationDAOHibernate(HibernateContext hibernateContext) {
        super(hibernateContext);
    }

    public XdaqExecutive getXdaqExecutive(String host, int port)
            throws DataAccessException {
        try {
            return (XdaqExecutive) currentSession().createCriteria(
                    XdaqExecutive.class).add(Restrictions.eq("host", host))
                    .add(Restrictions.eq("port", port)).uniqueResult();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    public XdaqApplication getXdaqApplication(XdaqExecutive executive,
            String className, int instance) throws DataAccessException {
        try {
            return (XdaqApplication) currentSession().createCriteria(
                    XdaqApplication.class).add(
                    Restrictions.eq("xdaqExecutive", executive)).add(
                    Restrictions.eq("className", className)).add(
                    Restrictions.eq("instance", instance)).uniqueResult();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    public XdaqApplication getXdaqApplication(String host, int port,
            String className, int instance) throws DataAccessException {
        try {
            return (XdaqApplication) currentSession().createQuery(
                    "select distinct app " + "from XdaqApplication app "
                            + "where app.xdaqExecutive.host = :host "
                            + "and app.xdaqExecutive.port = :port "
                            + "and app.className = :className "
                            + "and app.instance = :instance ").setString(
                    "host", host).setInteger("port", port).setString(
                    "className", className).setInteger("instance", instance)
                    .uniqueResult();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    /*
     * public XdaqApplication getXdaqApplication(Crate crate) throws
     * DataAccessException { try { return (XdaqApplication)
     * currentSession().createQuery( "select distinct app " + "from
     * XdaqAppVmeCrateAccess app, " + " Board board " + "where app.crate =
     * board.crate " + "and board = :board ").setEntity("board", board)
     * .uniqueResult(); } catch (HibernateException e) { rollback(); throw new
     * DataAccessException(e); } }
     */
    public XdaqApplication getXdaqApplication(Crate crate)
            throws DataAccessException {
        try {
            if (crate instanceof LinkBox) {
                return (XdaqApplication) currentSession().createQuery(
                        "select distinct app " + "from XdaqAppLBoxAccess app, "
                                + "     ControlBoard cb "
                                + "where app.ccsBoard = cb.ccsBoard "
                                + "and app.fecPosition = cb.fecPosition "
                                + "and cb.crate = :crate ").setEntity("crate",
                        crate).uniqueResult();
            } else {
                return (XdaqApplication) currentSession().createQuery(
                        "select distinct app "
                                + "from XdaqAppVmeCrateAccess app "
                                + "where app.crate = :crate ").setEntity(
                        "crate", crate).uniqueResult();
            }
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    public XdaqApplication getXdaqApplication(Board board)
            throws DataAccessException {
        try {
            if (board.getType() == BoardType.CONTROLBOARD
                    || board.getType() == BoardType.LINKBOARD) {
                return (XdaqApplication) currentSession().createQuery(
                        "select distinct app " + "from XdaqAppLBoxAccess app, "
                                + "     ControlBoard cb," + "     Board board "
                                + "where app.ccsBoard = cb.ccsBoard "
                                + "and app.fecPosition = cb.fecPosition "
                                + "and board = :board "
                                + "and board.crate = cb.crate").setEntity(
                        "board", board).uniqueResult();
            }

            return (XdaqApplication) currentSession().createQuery(
                    "select distinct app " + "from XdaqAppVmeCrateAccess app, "
                            + "     Board board "
                            + "where app.crate = board.crate "
                            + "and board = :board ").setEntity("board", board)
                    .uniqueResult();

        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    public XdaqApplication getXdaqApplication(Chip chip)
            throws DataAccessException {

        try {

            if (chip.getType() == ChipType.SYNCODER) {
                return (XdaqApplication) currentSession().createQuery(
                        "select distinct app from XdaqAppLBoxAccess app, "
                                + "   ControlBoard cb, Chip chip "
                                + "where app.ccsBoard = cb.ccsBoard "
                                + "and app.fecPosition = cb.fecPosition "
                                + "and chip = :chip "
                                + "and chip.board.crate = cb.crate").setEntity(
                        "chip", chip).uniqueResult();
            }

            return (XdaqApplication) currentSession().createQuery(
                    "select distinct app " + "from XdaqAppVmeCrateAccess app, "
                            + "     Chip chip "
                            + "where app.crate = chip.board.crate "
                            + "and chip = :chip ").setEntity("chip", chip)
                    .uniqueResult();

        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    @SuppressWarnings("unchecked")
    public List<LocalConfigKey> getLocalConfigKeysBySubsystem(LocalConfigKey.Subsystem subsystem)
    throws DataAccessException {
    	try {
    		return currentSession().createQuery(
    		"from LocalConfigKey where (subsystem) = :subsystem").setString(
    				"subsystem", subsystem.name()).list();
    	} catch (HibernateException e) {
    		rollback();
    		throw new DataAccessException(e);
    	}
    }

    
    public LocalConfigKey getLocalConfigKeyByName(String name)
            throws DataAccessException {
        try {
            return (LocalConfigKey) currentSession().createQuery(
                    "from LocalConfigKey where upper(name) = :name").setString(
                    "name", name.toUpperCase()).uniqueResult();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    public LocalConfigKey getLocalConfigKeyByGlobalConfigKey(
            String globalConfigKey) throws DataAccessException {
        try {
            return (LocalConfigKey) currentSession()
                    .createQuery(
                            "select cka.localConfigKey "
                                    + "from ConfigKeyAssignment cka "
                                    + "where upper(cka.configKey) = :globalConfigKey ")
                    .setString("globalConfigKey", globalConfigKey.toUpperCase())
                    .uniqueResult();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }
    
    public LocalConfigKey getLocalConfigKeyByGlobalConfigKey(
            String globalConfigKey, String subsystem) throws DataAccessException {
        try {
        	LocalConfigKey selLocalConfigKey = null;
        	for(LocalConfigKey localConfigKey : getLocalConfigKeysByGlobalConfigKey(globalConfigKey)) {
        		if(localConfigKey.getSubsystem().equals(subsystem)) {
        			if(selLocalConfigKey != null) {
        				throw new RuntimeException("multiple LocalConfigKeys for the subsystem " 
        						+ subsystem + " are assigned to globalConfigKey " + globalConfigKey);
        			}
        			selLocalConfigKey = localConfigKey;
        		}
        	}
        	return null; 
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }
    
    @SuppressWarnings("unchecked")
    public List<LocalConfigKey> getLocalConfigKeysByGlobalConfigKey(
            String globalConfigKey) throws DataAccessException {
        try {
            return currentSession()
                    .createQuery(
                            "select cka.localConfigKey "
                                    + "from ConfigKeyAssignment cka "
                                    + "where upper(cka.configKey) = :globalConfigKey ")
                    .setString("globalConfigKey", globalConfigKey.toUpperCase())
                    .list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }
    
    @SuppressWarnings("unchecked")
    public List<String> getGlobalConfigKeysByLocalConfigKey(
    		LocalConfigKey localConfigKey) throws DataAccessException {
        try {
            return currentSession()
                    .createQuery(
                            "select cka.configKey "
                                    + "from ConfigKeyAssignment cka "
                                    + "where upper(cka.localConfigKey) = :localConfigKey ")
                    .setEntity("localConfigKey", localConfigKey)
                    .list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }
    
    @SuppressWarnings("unchecked")
    public List<ConfigKey> getGlobalConfigKeys() throws DataAccessException {
    	return getObjects(ConfigKey.class);
    }
    
    @SuppressWarnings("unchecked")
    public List<LocalConfigKey> getLocalConfigKeys() throws DataAccessException {
        try {
        	return getObjects(LocalConfigKey.class);
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }
    
    public List<Chip> getDisabledChips() throws DataAccessException {
        try {
            return currentSession().createQuery(
                    "from Chip chip " + "where exists ("
                            + "from ChipDisabled as cd "
                            + "where cd.chip = chip" + ")").list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    public ChipDisabled getChipDisabled(Chip chip) throws DataAccessException {
        try {
            return (ChipDisabled) currentSession().createCriteria(
                    ChipDisabled.class).add(Restrictions.eq("chip", chip))
                    .uniqueResult();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    public List<Integer> getDisabledChipsIds() throws DataAccessException {
        try {
            return currentSession().createQuery(
                    "select cd.chip.id from ChipDisabled cd").list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }
    
    public List<ChipDisabledHistory> getChipDisablingHistory(Chip chip) throws DataAccessException {
        try {
            return currentSession().createQuery(
                    "from ChipDisabledHistory cdh "
                            + "where cdh.chip = :chip " 
                            + "order by cdh.modificationDate"
                    ).setEntity("chip", chip).list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    public List<Integer> getDisabledBoardIds() throws DataAccessException {
        try {
            return currentSession().createQuery(
                    "select bd.board.id from BoardDisabled bd").list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    public BoardDisabled getBoardDisabled(Board board)
            throws DataAccessException {
        try {
            return (BoardDisabled) currentSession().createCriteria(
                    BoardDisabled.class).add(Restrictions.eq("board", board))
                    .uniqueResult();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }
    
    public List<BoardDisabledHistory> getBoardDisablingHistory(Board board) throws DataAccessException {
        try {
            return currentSession().createQuery(
                    "from BoardDisabledHistory bdh "
                            + "where board = :board " 
                            + "order by bdh.modificationDate"
                    ).setEntity("board", board).list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    public List<Integer> getDisabledCrateIds() throws DataAccessException {
        try {
            return currentSession().createQuery(
                    "select cd.crate.id from CrateDisabled cd").list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    public CrateDisabled getCrateDisabled(Crate crate)
            throws DataAccessException {
        try {
            return (CrateDisabled) currentSession().createCriteria(
                    CrateDisabled.class).add(Restrictions.eq("crate", crate))
                    .uniqueResult();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }
    
    public List<CrateDisabledHistory> getCrateDisablingHistory(Crate crate) throws DataAccessException {
        try {
            return currentSession().createQuery(
                    "from CrateDisabledHistory cdh "
                            + "where crate = :crate " 
                            + "order by cdh.modificationDate"
                    ).setEntity("crate", crate).list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    @SuppressWarnings("unchecked")
	public List<Integer> getDisabledLinksIds() throws DataAccessException {
        try {
            return (List<Integer>) currentSession().createQuery(
                    "select ld.conn.id from LinkDisabled ld").list(); // Unchecked cast
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    public LinkDisabled getLinkDisabled(BoardBoardConn linkConn)
            throws DataAccessException {
        try {
            return (LinkDisabled) currentSession().createCriteria(
                    LinkDisabled.class).add(
                    Restrictions.eq("conn", linkConn)).uniqueResult();
        } catch (HibernateException e) {
        	rollback();
        	throw new DataAccessException(e);
        }
    }
    
    public List<LinkDisabledHistory> getLinkDisabledHistory(BoardBoardConn boardBoardConn) throws DataAccessException {
        try {
            return currentSession().createQuery(
                    "from LinkDisabledHistory ldh "
                            + "where boardBoardConn = :boardBoardConn " 
                            + "order by ldh.modificationDate"
                    ).setEntity("boardBoardConn", boardBoardConn).list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    public FebBoardDTControlled getFebDtControlled(Board board) throws DataAccessException {
    	try {
            return (FebBoardDTControlled) currentSession().createCriteria(
            		FebBoardDTControlled.class).add(Restrictions.eq("board", board))
                    .uniqueResult();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    @SuppressWarnings("unchecked")
    public List<Integer> getFebDtControlledIds() throws DataAccessException {
        try {
            return currentSession().createQuery(
                    "select fd.board.id from FebBoardDTControlled fd").list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }
    
    public List<FebDTControlledHistory> getFebDTControlledHistory(Board board) throws DataAccessException {
        try {
            return currentSession().createQuery(
                    "from FebDTControlledHistory bdh "
                            + "where board = :board " 
                            + "order by bdh.modificationDate"
                    ).setEntity("board", board).list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    private final static String MAX_CREATION_DATE_COND = "csa.creationDate = ( "
            + "  select max(csa1.creationDate) from ChipConfAssignment csa1 "
            + "  where csa1.localConfigKey = csa.localConfigKey "
            + "  and csa1.chip = csa.chip )";

    public ChipConfAssignment getChipConfAssignment(Chip chip,
            LocalConfigKey localConfigKey) throws DataAccessException {
        try {
        	if(chip == null)  {
        		throw new IllegalArgumentException("chip is null, localConfigKey = " + localConfigKey);
        	}
        	if(localConfigKey == null)  {
        		throw new IllegalArgumentException("localConfigKey is null, chip = " + chip);
        	}
            return (ChipConfAssignment) currentSession().createQuery(
                    "from ChipConfAssignment csa "
                            + "where csa.localConfigKey = :localConfigKey "
                            + "and csa.chip = :chip " + "and "
                            + MAX_CREATION_DATE_COND
            // "and not exists (" +
                    // "from ChipDisabled as cd where cd.chip = csa.chip )"
                    ).setEntity("localConfigKey", localConfigKey).setEntity(
                            "chip", chip).uniqueResult();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException("error for chip" + chip.getType() + " id " + chip.getId() + " on board " + chip.getBoard() + " and localConfigKey" + localConfigKey.getName(), e);
        }
    }
    
    public List<ChipConfAssignment> getChipConfAssignmentHistory(Chip chip,
            LocalConfigKey localConfigKey) throws DataAccessException {
        try {
            return currentSession().createQuery(
                    "from ChipConfAssignment csa "
                            + "where csa.localConfigKey = :localConfigKey "
                            + "and csa.chip = :chip " 
                            + "order by csa.creationDate"
                    ).setEntity("localConfigKey", localConfigKey).setEntity(
                            "chip", chip).list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    public List<ChipConfAssignment> getChipConfAssignments(List<Chip> chips,
            LocalConfigKey localConfigKey) throws DataAccessException {
        if (chips == null || chips.isEmpty()) {
            return Collections.emptyList();
        }
        try {
            return currentSession().createQuery(
                    "from ChipConfAssignment csa "
                            + "where csa.localConfigKey = :localConfigKey "
                            + "and csa.chip in (:chips) " + "and "
                            + MAX_CREATION_DATE_COND
            /*
             * "from ChipConfAssignment csa " + "where csa.localConfigKey =
             * :localConfigKey " + "and csa.chip in (:chips) " + "and not exists (" +
             * "from ChipDisabled as cd where cd.chip = csa.chip )"
             */
            ).setEntity("localConfigKey", localConfigKey).setParameterList(
                    "chips", chips).list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }
    
    public List<ChipConfAssignment> getChipConfAssignments(ChipType chipType,
            LocalConfigKey localConfigKey) throws DataAccessException {
        try {
            return currentSession().createQuery(
                    "from ChipConfAssignment csa "
            		+ "left join fetch csa.staticConfiguration "
                    + "left join fetch csa.chip "
                            + "where csa.localConfigKey = :localConfigKey "
                            + "and csa.chip.type = :type " + "and "
                            + MAX_CREATION_DATE_COND
            /*
             * "from ChipConfAssignment csa " + "where csa.localConfigKey =
             * :localConfigKey " + "and csa.chip in (:chips) " + "and not exists (" +
             * "from ChipDisabled as cd where cd.chip = csa.chip )"
             */
            ).setEntity("localConfigKey", localConfigKey).setParameter("type", chipType).list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }
    
    public List<ChipConfAssignment> getChipConfAssignments(ChipType chipType,
            ChipConfigTag tag) throws DataAccessException {
        try {
            return currentSession().createQuery(
                    "from ChipConfAssignment csa "
            		+ "left join fetch csa.staticConfiguration "
                    + "left join fetch csa.chip "
                            + "where csa.tag = :tag "
                            + "and csa.chip.type = :type "
            /*
             * "from ChipConfAssignment csa " + "where csa.localConfigKey =
             * :localConfigKey " + "and csa.chip in (:chips) " + "and not exists (" +
             * "from ChipDisabled as cd where cd.chip = csa.chip )"
             */
            ).setEntity("tag", tag).setParameter("type", chipType).list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    public List<ChipConfAssignment> getChipConfAssignmentsByChipsAndGlobalConfigKey(
            List<Chip> chips, String globalConfigKey)
            throws DataAccessException {
        try {
            return currentSession()
                    .createQuery(
                            "select csa "
                                    + "from ChipConfAssignment csa, ConfigKeyAssignment cka "
                                    + "where csa.localConfigKey = cka.localConfigKey "
                                    + "and upper(cka.configKey) = :globalConfigKey "
                                    + "and csa.chip in (:chips) " + "and "
                                    + MAX_CREATION_DATE_COND
                    // + "and not exists ("
                    // + "from ChipDisabled as cd where cd.chip = csa.chip "
                    // + ")")
                    ).setString("globalConfigKey",
                            globalConfigKey.toUpperCase()).setParameterList(
                            "chips", chips).list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    public List<ChipConfAssignment> getChipConfAssignmentsByChipIdsAndGlobalConfigKey(
            List<Integer> chipIds, String globalConfigKey)
            throws DataAccessException {
        try {
            return currentSession()
                    .createQuery(
                            "select csa "
                                    + "from ChipConfAssignment csa, ConfigKeyAssignment cka "
                                    + "where csa.localConfigKey = cka.localConfigKey "
                                    + "and upper(cka.configKey) = :globalConfigKey "
                                    + "and csa.chip.id in (:chipIds) " + "and "
                                    + MAX_CREATION_DATE_COND
                    // + "and not exists ("
                    // + "from ChipDisabled as cd where cd.chip = csa.chip "
                    // + ")"
                    ).setString("globalConfigKey",
                            globalConfigKey.toUpperCase()).setParameterList(
                            "chipIds", chipIds).list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }


    public List<ChipConfAssignment> getChipConfAssignmentsByChipIdsAndLocalConfigKey(
            List<Integer> chipIds, String localConfigKey)
            throws DataAccessException {
        try {
            return currentSession()
                    .createQuery(
                            "select csa " +
                            "from ChipConfAssignment csa " +
                            "left join fetch csa.staticConfiguration " +
                            "left join fetch csa.chip " +
//                            "left join fetch csa.chip.disabled " +
//                            "left join fetch csa.chip.board " +
//                            "left join fetch csa.chip.board.disabled " +
                          //  "left join fetch csa.chip.board.crate " +
                          //  "left join fetch csa.chip.board.crate.disabled " +
                            "where upper(csa.localConfigKey.name) = :localConfigKey " +
                            "and csa.chip.id in (:chipIds) " +
                            "and " + MAX_CREATION_DATE_COND
                    // + "and not exists ("
                    // + "from ChipDisabled as cd where cd.chip = csa.chip "
                    // + ")"
                    ).setString("localConfigKey", localConfigKey.toUpperCase())
                    .setParameterList("chipIds", chipIds).list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }
    
    public ConfigSetInfo getConfigSetInfoByChipIdsAndGlobalConfigKey(
            List<Integer> chipIds, String globalConfigKey)
            throws DataAccessException {
        try {
            return (ConfigSetInfo) currentSession()
            .createQuery(
                    "select new rpct.db.domain.configuration.ConfigSetInfo(" +
                    "  cka.localConfigKey.id, max(csa.creationDate))" +
                    "from ChipConfAssignment csa, ConfigKeyAssignment cka " +
                    "where csa.localConfigKey = cka.localConfigKey " +
                    "and upper(cka.configKey) = :globalConfigKey " +
                    "and csa.chip.id in (:chipIds) " +
                    "and " + MAX_CREATION_DATE_COND +
                    " group by cka.localConfigKey"
                    // + "and not exists ("
                    // + "from ChipDisabled as cd where cd.chip = csa.chip "
                    // + ")"
                    ).setString("globalConfigKey",
                            globalConfigKey.toUpperCase()).setParameterList(
                            "chipIds", chipIds).uniqueResult();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }

    
    public ConfigSetInfo getConfigSetInfoByChipIdsAndLocalConfigKey(
            List<Integer> chipIds, String localConfigKey)
            throws DataAccessException {
        try {
            return (ConfigSetInfo) currentSession().
                createSQLQuery("SELECT"
                               + " localconfigkey.localconfigkeyid AS localConfigKeyId"
                               + ", max(chipconfassignment.creationdate) AS creationDate"
                               + " FROM chipconfassignment"
                               + " INNER JOIN localconfigkey"
                               + " ON chipconfassignment.lck_localconfigkeyid=localconfigkey.localconfigkeyid"
                               + " WHERE upper(localconfigkey.name) = :localConfigKey"
                               + " AND chipconfassignment.chip_chipid in (:chipIds)"
                               + " GROUP BY localconfigkey.localconfigkeyid")
                .addScalar("creationDate", TimestampType.INSTANCE)
                .addScalar("localConfigKeyId", IntegerType.INSTANCE)
                .setResultTransformer(Transformers.aliasToBean(ConfigSetInfo.class))
                .setString("localConfigKey", localConfigKey.toUpperCase())
                .setParameterList("chipIds", chipIds)
                .uniqueResult();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }
    /*
    static public class ConfigSetInfo {
        private int localConfigKeyId;
        private Date creationDate;
        private List<ChipConfAssignment> assignments;

        public ConfigSetInfo(int localConfigKeyId, Date creationDate,
                List<ChipConfAssignment> assignments) {
            super();
            this.localConfigKeyId = localConfigKeyId;
            this.creationDate = creationDate;
            this.assignments = assignments;
        }

        public int getLocalConfigKeyId() {
            return localConfigKeyId;
        }

        public Date getCreationDate() {
            return creationDate;
        }

        public List<ChipConfAssignment> getAssignments() {
            return assignments;
        }
    }
    public ConfigSetInfo getConfigSetInfoByChipIdsAndLocalConfigKey(
            List<Integer> chipIds, String localConfigKey)
            throws DataAccessException {
        try {
            return currentSession()
                    .createQuery(
                            "select new rpct.db.domain.configuration.ConfigurationDAOHibernate.ConfigSetInfo(" +
                            " ) "
                                    + "from ChipConfAssignment csa "
                                    + "where upper(csa.localConfigKey.name) = :localConfigKey "
                                    + "and csa.chip.id in (:chipIds) " + "and "
                                    + MAX_CREATION_DATE_COND
                    // + "and not exists ("
                    // + "from ChipDisabled as cd where cd.chip = csa.chip "
                    // + ")"
                    ).setString("localConfigKey", localConfigKey.toUpperCase())
                    .setParameterList("chipIds", chipIds).list();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }*/

/* MichalS */
    public CurrentConfigKey getCurrentKey()
      throws DataAccessException {
      try {
          return (CurrentConfigKey) currentSession().createCriteria(
                    CurrentConfigKey.class).uniqueResult();
      } catch (HibernateException e) {
          rollback();
          throw new DataAccessException(e);
      }
    }

    public void writeCurrentKey(String currentKeyValue)
      throws DataAccessException {
      try {
          CurrentConfigKey currentConfigKey = getCurrentKey();
          deleteObject(currentConfigKey);
          currentConfigKey = new CurrentConfigKey();
//          currentConfigKey.setId(id);
          currentConfigKey.setName(currentKeyValue);
          saveObject(currentConfigKey);
          flush();
      } catch (HibernateException e) {
          rollback();
          throw new DataAccessException(e);
      }
    }
/* MichalS */

	public ConfigKey getGlobalConfigKeyByName(String name)
			throws DataAccessException {
        try {
            return (ConfigKey) currentSession().createQuery(
                    "from ConfigKey where upper(CONFIGKEYID) = :name").setString(
                    "name", name.toUpperCase()).uniqueResult();
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
	}
	
	public ChipConfigTag getChipConfigTag(String tagName)
	throws DataAccessException {
		try {
			return (ChipConfigTag) currentSession().createQuery(
					"from ChipConfigTag where upper(CHIPCONFIGTAGID) = :tagName").setString(
							"tagName", tagName).uniqueResult();
		} catch (HibernateException e) {
			rollback();
			throw new DataAccessException(e);
		}
	}
	
	public SynCoderConfInFlash getSynCoderConfInFlash(int chipId) throws DataAccessException  {
		try {
			return (SynCoderConfInFlash) getObject(SynCoderConfInFlash.class, chipId);   
		} catch (HibernateException e) {
			rollback();
			throw new DataAccessException(e);
		}
	}

}
