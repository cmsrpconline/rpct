package rpct.db.domain.configuration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import rpct.db.domain.equipment.ChipType;
/**
 * Created on 2006-09-01
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */


@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@SequenceGenerator(name="seq", sequenceName="S_18_1_STATICCONFIGURATION", allocationSize=1)
public abstract class StaticConfiguration {
    private int id;
    
    @Id
    @Column(name="STATICCONFIGURATIONID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }    
    
    @Transient
    public abstract ChipType getChipType();    
}
