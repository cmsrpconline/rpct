package rpct.db.domain.configuration;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * Created on 2011-11-06
 *
 * @author Karol Bunkowski
 * @version $Id: $
 */


@Entity
public class ChipConfigTag {
    private String chipConfigTagId;
    private Date creationDate = new Date();
    private String description;
    
    public ChipConfigTag() {
    	super();
    }
    
    public ChipConfigTag(String chipConfigTagId, String comment) {
		super();
		this.chipConfigTagId = chipConfigTagId;
		this.description = comment;
	}
    
	@Id
    @Column(name="CHIPCONFIGTAGID")
    public String getChipConfigTagId() {
		return chipConfigTagId;
	}
	public void setChipConfigTagId(String chipConfigTagId) {
		this.chipConfigTagId = chipConfigTagId;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Column(name="DESCRIPTION")
	public String getDescription() {
		return description;
	}
	public void setDescription(String comment) {
		this.description = comment;
	}
    
	
    @Transient 
    @Override
    public String toString() {
    	return chipConfigTagId + " " + creationDate + " " + description;
    }
}
