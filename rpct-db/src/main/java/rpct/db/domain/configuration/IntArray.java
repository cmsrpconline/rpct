package rpct.db.domain.configuration;

import java.util.Arrays;

/**
 * Created on 2010-01-19
 *
 * @author Nikolay Darmenov
 * @version $Id$
 * 
 * from getValues() and setValues(() for the reason of performance - 
 * Subtypes are not backed up by int[] but by individual "valueXX" fields
 * TODO: check performance and use get/setValues() in (get/set)Value() 
 */
public abstract class IntArray {
	public abstract int getValue(int index);
    public abstract void setValue(int index, int value);
    public abstract int[] getValues();
    public abstract void setValues(int[] values);
    
	@Override
	public String toString() {
		
		return Arrays.toString(getValues());
	}
}
