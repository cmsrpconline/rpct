package rpct.db.domain.configuration;

import java.util.Arrays;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import rpct.db.domain.equipment.ChipType;


/**
 * Created on 2009-11-18
 *
 * @author Nikolay Darmenov
 * @version $Id$
 */

@Entity
@PrimaryKeyJoinColumn(name="TTUFINALCONFID")
@Table(name="TTUFINALCONF")
public class TtuFinalConf extends StaticConfiguration {
    private IntArray6 trigConfig =  new IntArray6();
    private IntArray12 trigConfig2 =  new IntArray12();
    private int maskTtu;
    private int maskPointing;
    private int maskBlast;
    private int delayTtu;
    private int delayPointing;
    private int delayBlast;
    private int shapeTtu;
    private int shapePointing;
    private int shapeBlast;


    /*
    "IA6_TRIGCONFIGID" NUMBER(11,0) NOT NULL,
    "IA12_TRIGCONFIG2ID" NUMBER(11,0) NOT NULL,
    "MASKTTU" NUMBER(11,0) NOT NULL,
    "MASKPOINTING" NUMBER(11,0) NOT NULL,
    "MASKBLAST" NUMBER(11,0) NOT NULL,
    "DELAYTTU" NUMBER(11,0) NOT NULL,
    "DELAYPOINTING" NUMBER(11,0) NOT NULL,
    "DELAYBLAST" NUMBER(11,0) NOT NULL,
    "SHAPETTU" NUMBER(11,0) NOT NULL,
    "SHAPEPOINTING" NUMBER(11,0) NOT NULL,
    "SHAPEBLAST" NUMBER(11,0) NOT NULL,
     */

    public TtuFinalConf() {
        super();
    }    

    public TtuFinalConf(TtuFinalConf conf) {
    	super();
    	
		this.trigConfig = new IntArray6(conf.trigConfig.getValues().clone());
		this.trigConfig2 = new IntArray12(conf.trigConfig2.getValues().clone());
		this.maskTtu = conf.maskTtu;
		this.maskPointing = conf.maskPointing;
		this.maskBlast = conf.maskBlast;
		this.delayTtu = conf.delayTtu;
		this.delayPointing = conf.delayPointing;
		this.delayBlast = conf.delayBlast;
		this.shapeTtu = conf.shapeTtu;
		this.shapePointing = conf.shapePointing;
		this.shapeBlast = conf.shapeBlast;
    }
    
    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="IA6_TRIGCONFIGID")
    public IntArray6 getTrigConfig() {
		return trigConfig;
	}

	public void setTrigConfig(IntArray6 trigConfig) {
		this.trigConfig = trigConfig;
	}

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="IA12_TRIGCONFIG2ID")
	public IntArray12 getTrigConfig2() {
		return trigConfig2;
	}

	public void setTrigConfig2(IntArray12 trigConfig2) {
		this.trigConfig2 = trigConfig2;
	}

	public int getMaskTtu() {
		return maskTtu;
	}

	public void setMaskTtu(int maskTtu) {
		this.maskTtu = maskTtu;
	}

	public int getMaskPointing() {
		return maskPointing;
	}

	public void setMaskPointing(int maskPointing) {
		this.maskPointing = maskPointing;
	}

	public int getMaskBlast() {
		return maskBlast;
	}

	public void setMaskBlast(int maskBlast) {
		this.maskBlast = maskBlast;
	}

	public int getDelayTtu() {
		return delayTtu;
	}

	public void setDelayTtu(int delayTtu) {
		this.delayTtu = delayTtu;
	}

	public int getDelayPointing() {
		return delayPointing;
	}

	public void setDelayPointing(int delayPointing) {
		this.delayPointing = delayPointing;
	}

	public int getDelayBlast() {
		return delayBlast;
	}

	public void setDelayBlast(int delayBlast) {
		this.delayBlast = delayBlast;
	}

	public int getShapeTtu() {
		return shapeTtu;
	}

	public void setShapeTtu(int shapeTtu) {
		this.shapeTtu = shapeTtu;
	}

	public int getShapePointing() {
		return shapePointing;
	}

	public void setShapePointing(int shapePointing) {
		this.shapePointing = shapePointing;
	}

	public int getShapeBlast() {
		return shapeBlast;
	}

	public void setShapeBlast(int shapeBlast) {
		this.shapeBlast = shapeBlast;
	}

	@Override
    @Transient
    public ChipType getChipType() {
        return ChipType.TTUFINAL;
    }

	@Override
	@Transient
	public String toString() {
		return this.getClass().getSimpleName() + " [Id=" + getId()
		+ "\n   * trigConfig="	+ Arrays.toString(trigConfig.getValues())
		+ "\n   * trigConfig2=" + Arrays.toString(trigConfig2.getValues())
		+ "\n   * maskTtu=" + maskTtu
		+ "\n   * maskPointing=" + maskPointing
		+ "\n   * maskBlast=" + maskBlast
		+ "\n   * delayTtu=" + delayTtu
		+ "\n   * delayPointing=" + delayPointing
		+ "\n   * delayBlast=" + delayBlast
		+ "\n   * shapeTtu=" + shapeTtu
		+ "\n   * shapePointing=" + shapePointing
		+ "\n   * shapeBlast=" + shapeBlast
		+ "\n]";
	}
}
