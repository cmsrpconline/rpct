package rpct.db.domain.condition;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created on 2006-02-20
 *
 * @author Michal Pietrusinski & William Whitaker
 * @version $Id$
 */


@Entity
@Table(name="FebCondition")
@SequenceGenerator(name="seq", sequenceName="S_01_1_FEBCONDITION", initialValue= 1, allocationSize = 1)
public class FebCondition {
    private int id;
    private int febLocationId;
    private int run;
    private int th1;
    private int th2;
    private int th3;
    private int th4;
    private int vmon1;
    private int vmon2;
    private int vmon3;
    private int vmon4;
    private int unixTime;
    
    public FebCondition(){
    }
    
	@Id
    @Column(name="FEBCONDITIONID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    @Column(name="FL_FEBLOCATIONID")
	public Integer getFebLocationId() {
		return febLocationId;
	}
	public void setFebLocationId(Integer febLocationId) {
		this.febLocationId = febLocationId;
	}
		
    @Column(name="RUN")
	public Integer getRun() {
		return run;
	}
	public void setRun(Integer run) {
		this.run = run;
	}

	@Column(name="TH1")
	public Integer getTh1() {
		return th1;
	}
	public void setTh1(Integer th1) {
		this.th1 = th1;
	}

	@Column(name="TH2")
	public Integer getTh2() {
		return th2;
	}
	public void setTh2(Integer th2) {
		this.th2 = th2;
	}

	@Column(name="TH3")
	public Integer getTh3() {
		return th3;
	}
	public void setTh3(Integer th3) {
		this.th3 = th3;
	}

	@Column(name="TH4")
	public Integer getTh4() {
		return th4;
	}
	public void setTh4(Integer th4) {
		this.th4 = th4;
	}

	@Column(name="VMON1")
	public Integer getVmon1() {
		return vmon1;
	}
	public void setVmon1(Integer vmon1) {
		this.vmon1 = vmon1;
	}

	@Column(name="VMON2")
	public Integer getVmon2() {
		return vmon2;
	}
	public void setVmon2(Integer vmon2) {
		this.vmon2 = vmon2;
	}

	@Column(name="VMON3")
	public Integer getVmon3() {
		return vmon3;
	}
	public void setVmon3(Integer vmon3) {
		this.vmon3 = vmon3;
	}

	@Column(name="VMON4")
	public Integer getVmon4() {
		return vmon4;
	}
	public void setVmon4(Integer vmon4) {
		this.vmon4 = vmon4;
	}

	@Column(name="UNIXTIME")
	public Integer getUnixTime() {
		return unixTime;
	}
	public void setUnixTime(Integer unixTime) {
		this.unixTime = unixTime;
	}
	
}
