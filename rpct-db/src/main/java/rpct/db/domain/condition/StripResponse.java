package rpct.db.domain.condition;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created on 2006-02-20
 *
 * @author Michal Pietrusinski & William Whitaker
 * @version $Id$
 */


@Entity
@Table(name="STRIPRESPONSE")
@SequenceGenerator(name="seq", sequenceName="S_17_1_STRIPRESPONSE", initialValue= 1, allocationSize = 1)
public class StripResponse {
    private int id;
    private int chamberStripId;
    private int count;
    private int run;
    private int unixTime;
    
    public StripResponse(){
    }
    
	@Id
    @Column(name="STRIPRESPONSEID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    @Column(name="CS_CHAMBERSTRIPID")
	public Integer getChamberStripId() {
		return chamberStripId;
	}
	public void setChamberStripId(Integer chamberStripId) {
		this.chamberStripId = chamberStripId;
	}
		
    @Column(name="COUNT")
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
    @Column(name="RUN")
	public Integer getRun() {
		return run;
	}
	public void setRun(Integer run) {
		this.run = run;
	}
    @Column(name="UNIXTIME")
	public Integer getUnixTime() {
		return unixTime;
	}
	public void setUnixTime(Integer unixTime) {
		this.unixTime = unixTime;
	}
	
}
