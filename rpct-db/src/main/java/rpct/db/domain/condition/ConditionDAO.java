package rpct.db.domain.condition;

import java.math.BigInteger;
import java.util.List;

import rpct.db.DataAccessException;
import rpct.db.domain.DAO;

/**
 * Created on 2005-03-01
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public interface ConditionDAO extends DAO {

	StripResponse getStripResponseById(int id) throws DataAccessException;
    public FebCondition getFebConditionById(int id) throws DataAccessException;
    public List<StripResponse> getStripResonseByChamberStripId(Integer chamberStripId ) throws DataAccessException;
    public List<FebCondition> getFebConditionByFebConditionId(Integer febConditionId ) throws DataAccessException;
    public BigInteger setStripResponse(/*Integer id,*/ Integer chamberStrip, Integer count, Integer run, Integer unixtime) throws DataAccessException;
    public BigInteger setFebCondition(/*Integer id,*/ Integer febLocation, Integer run, Integer th1, Integer th2,	Integer th3, Integer th4, Integer vmon1, Integer vmon2, Integer vmon3, Integer vmon4, Integer unixtime) throws DataAccessException;
}
