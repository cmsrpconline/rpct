package rpct.db.domain.condition.hibernate;

import java.io.File;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;


/**
 * Created on 2005-03-01
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class SimpleCondHibernateUtil {

    private static final SessionFactory sessionFactory;
    public final static String HIBERNATE_CONFIG_FILE_NAME = "hibernate-cond.cfg.xml";
    static {
        try {
        	Configuration config;                
        	File configFile = new File(HIBERNATE_CONFIG_FILE_NAME);
        	if (configFile.exists()) {
        		config = new AnnotationConfiguration().configure(configFile);
        	}
        	else {
        		System.out.println("I'm here and can't find the xml file"); // I get this.
                        config = new AnnotationConfiguration().configure(HIBERNATE_CONFIG_FILE_NAME);
                        System.out.println("Configured successfully by file name");
                        
                        // When I add this, it connects with
        		// hibernate.cfg.xml and writes fine to
        		// db in this file.
        		//throw new FileNotFoundException(configFile.toString());     // This was what was in originally
        																	// I never found any reference saying
        																	// that the file was not found.
        	}
        	CondHibernateConfig.configure((AnnotationConfiguration)config);        	
    		System.out.println("I'm here after the annotations");
            sessionFactory = config.buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }
    
    public static final ThreadLocal<Session> session = new ThreadLocal<Session>();
    
    public static Session currentSession() throws HibernateException {
        Session s = session.get();
        // Open a new Session, if this Thread has none yet
        if (s == null) {
            s = sessionFactory.openSession();
            session.set(s);
        }
        return s;
    }
    
    public static void closeSession() throws HibernateException {
        Session s = session.get();
        session.set(null);
        if (s != null)
            s.close();
    }
}
