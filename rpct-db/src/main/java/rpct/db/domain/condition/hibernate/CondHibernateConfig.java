package rpct.db.domain.condition.hibernate;

import org.hibernate.cfg.AnnotationConfiguration;

import rpct.db.domain.condition.FebCondition;
import rpct.db.domain.condition.StripResponse;

/**
 * Created on 2005-03-01
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class CondHibernateConfig {
    
    public static void configure(AnnotationConfiguration configuration) {
        
        configuration.addAnnotatedClass(StripResponse.class);
        configuration.addAnnotatedClass(FebCondition.class);
        //configuration.addAnnotatedClass(Crate.class);
        
    }
}
