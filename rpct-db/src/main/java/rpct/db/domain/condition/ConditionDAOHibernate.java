package rpct.db.domain.condition;

import java.math.BigInteger;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import rpct.db.DataAccessException;
import rpct.db.domain.DAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;

/**
 * Created on 2005-03-01
 * 
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class ConditionDAOHibernate extends DAOHibernate implements ConditionDAO {

    public ConditionDAOHibernate(HibernateContext hibernateContext) {
        super(hibernateContext);
    }

    public StripResponse getStripResponseById(int id) throws DataAccessException {
        return (StripResponse) getObject(StripResponse.class, id);
    }
    public List<StripResponse> getStripResonseByChamberStripId(Integer chamberStripId) throws DataAccessException {
        try {
        	Criteria criteria = currentSession().createCriteria(StripResponse.class);
            if (chamberStripId != null) {
                criteria = criteria.add(Restrictions.eq("chamberStripId", chamberStripId));
            }
            criteria = criteria.addOrder(Order.asc("id"));
            return criteria.list();           
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }
    public BigInteger setStripResponse(/*Integer id,*/ Integer chamberStrip, Integer count, Integer run,
    		Integer unixtime) throws DataAccessException {
    	try {
        	StripResponse stripResponse = new StripResponse();
//        	stripResponse.setId(id);
        	stripResponse.setChamberStripId(chamberStrip);
        	stripResponse.setCount(count);
        	stripResponse.setRun(run);
        	stripResponse.setUnixTime(unixtime);
    		System.out.println("I'm here now");
    		saveObject(stripResponse);
    		System.out.println("I'm here after save");
    		flush();
    		return BigInteger.ZERO;
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }
    public FebCondition getFebConditionById(int id) throws DataAccessException {
        return (FebCondition) getObject(FebCondition.class, id);
    }
    public List<FebCondition> getFebConditionByFebConditionId(Integer febConditionId) throws DataAccessException {
        try {
        	Criteria criteria = currentSession().createCriteria(FebCondition.class);
            if (febConditionId != null) {
                criteria = criteria.add(Restrictions.eq("febConditionId", febConditionId));
            }
            criteria = criteria.addOrder(Order.asc("id"));
            return criteria.list();           
        } catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }
    }
    public BigInteger setFebCondition(/*Integer id,*/ Integer febLocation, Integer run, Integer th1,
    		Integer th2, Integer th3, Integer th4, Integer vmon1, Integer vmon2, Integer vmon3,
    		Integer vmon4, Integer unixtime) throws DataAccessException{
    	try {
	    	FebCondition febCondition = new FebCondition();
//	    	febCondition.setId(id);
	    	febCondition.setFebLocationId(febLocation);
	    	febCondition.setRun(run);
	    	febCondition.setTh1(th1);
	    	febCondition.setTh2(th2);
	    	febCondition.setTh3(th3);
	    	febCondition.setTh4(th4);
	    	febCondition.setVmon1(vmon1);
	    	febCondition.setVmon2(vmon2);
	    	febCondition.setVmon3(vmon3);
	    	febCondition.setVmon4(vmon4);
	    	febCondition.setUnixTime(unixtime);
			saveObject(febCondition);
    		flush();
    		return BigInteger.ZERO;
	    } catch (HibernateException e) {
	        rollback();
	        throw new DataAccessException(e);
	    }
    }
}
