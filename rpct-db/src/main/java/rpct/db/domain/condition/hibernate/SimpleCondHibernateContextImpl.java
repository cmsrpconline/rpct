package rpct.db.domain.condition.hibernate;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import rpct.db.DataAccessException;
import rpct.db.domain.hibernate.HibernateContext;

/**
 * Created on 2005-03-16
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */

public class SimpleCondHibernateContextImpl implements HibernateContext {
    protected Transaction tx = null;            
     
    
    public Session currentSession() throws DataAccessException {
        Session session = null;
        try {
            session = SimpleCondHibernateUtil.currentSession();
            if (tx == null)
                tx = session.beginTransaction();
        }
        catch (HibernateException ex) {
            throw new DataAccessException(ex);
        }
        return session;
    }

    public void commit() throws DataAccessException {
        try {
            tx.commit();
            tx = null;
        }
        catch (HibernateException ex) {
            throw new DataAccessException(ex);
        }
    }


    public void rollback() throws DataAccessException {
        try {
            tx.rollback();
            tx = null;
        }
        catch (HibernateException ex) {
            throw new DataAccessException(ex);
        }
    }

    public void closeSession() throws DataAccessException {
        try {
            if ((tx != null) && !tx.wasCommitted() && !tx.wasRolledBack()) {
                tx.commit();
                tx = null;
            }
            SimpleCondHibernateUtil.closeSession();
        }
        catch (HibernateException ex) {
            tx = null;
            throw new DataAccessException(ex);
        }
    }
}
