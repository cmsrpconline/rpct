package rpct.db.domain.hibernate.context.web;

import javax.servlet.ServletRequest;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import rpct.db.DataAccessException;
import rpct.db.domain.hibernate.HibernateContext;

/**
 * Created on 2005-03-16
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */

public final class WebHibernateContextImpl implements HibernateContext {
	
	public final static String HIBERNATE_CONTEXT_ATTR_NAME = "hibernateContext";
	
    private Transaction tx = null;
    
    public static HibernateContext getInstance(ServletRequest request, boolean create) {
        HibernateContext instance = (HibernateContext) request.getAttribute(HIBERNATE_CONTEXT_ATTR_NAME);
        if ((instance == null) && create) {
            instance = new WebHibernateContextImpl();
            request.setAttribute(HIBERNATE_CONTEXT_ATTR_NAME, instance);
        }
        return instance;
    } 
    
    /*public static HibernateContext getInstance(boolean create) {
    	
    	FacesContext facesContext = FacesContext.getCurrentInstance();
    	if (facesContext == null) {
    		if (create) {
    			throw new IllegalStateException("getInstance called outside JSF context");
    		}
    		return null;
    	}
    	
    	ServletRequest request =
    		(ServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    	return getInstance(request, create);
    }*/


    public Session currentSession() throws DataAccessException {
        Session session = null;
        try {
            session = WebHibernateUtil.currentSession();
            if (tx == null)
                tx = session.beginTransaction();
        }
        catch (HibernateException ex) {
            throw new DataAccessException(ex);
        }
        return session;
    }

    public void commit() throws DataAccessException {
        try {
            tx.commit();
            tx = null;
        }
        catch (HibernateException ex) {
            throw new DataAccessException(ex);
        }
    }


    public void rollback() throws DataAccessException {
        try {
            tx.rollback();
            tx = null;
        }
        catch (HibernateException ex) {
            throw new DataAccessException(ex);
        }
    }

    public void closeSession() throws DataAccessException {
        try {
            if ((tx != null) && !tx.wasCommitted() && !tx.wasRolledBack()) {
                tx.commit();
                tx = null;
            }
            WebHibernateUtil.closeSession();
        }
        catch (HibernateException ex) {
            tx = null;
            throw new DataAccessException(ex);
        }
    }
}
