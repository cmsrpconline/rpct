package rpct.db.domain.hibernate.context.web;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

import rpct.db.domain.hibernate.HibernateConfig;

/**
 * Created on 2005-03-01
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class WebHibernateUtil {

    private static final SessionFactory sessionFactory;
    static {
        try {
            // Create the SessionFactory
            Configuration config = new AnnotationConfiguration().configure();
            HibernateConfig.configure((AnnotationConfiguration) config);
            sessionFactory = config.buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }
    
    public static final ThreadLocal<Session> session = new ThreadLocal<Session>();
    
    public static Session currentSession() throws HibernateException {
        Session s = session.get();
        // Open a new Session, if this Thread has none yet
        if (s == null) {
            s = sessionFactory.openSession();
            session.set(s);
        }
        return s;
    }
    
    public static void closeSession() throws HibernateException {
        Session s = session.get();
        session.set(null);
        if (s != null)
            s.close();
    }
}
