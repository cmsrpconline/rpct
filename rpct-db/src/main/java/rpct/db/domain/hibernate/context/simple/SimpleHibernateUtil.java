package rpct.db.domain.hibernate.context.simple;

import java.io.File;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import rpct.db.domain.hibernate.HibernateConfig;


/**
 * Created on 2005-03-01
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class SimpleHibernateUtil {

    private static final SessionFactory sessionFactory;
    static {
        try {
        	Configuration config;
        	File configFile = new File("hibernate.cfg.xml");        	
        	if (configFile.exists()) {
        		System.out.println("configuring the DB connectionn from the " + configFile.getAbsolutePath());
        		config = new Configuration().configure(configFile);
        	}
        	else {
        		config = new Configuration().configure();        	
        	}
        	HibernateConfig.configure((Configuration)config);        	
            sessionFactory = config.buildSessionFactory();
        } catch (Throwable ex) {
        	System.out.println("exception in SimpleHibernateUtil.static " + ex.getMessage());
            throw new ExceptionInInitializerError(ex);
        }
    }
    
    public static final ThreadLocal<Session> session = new ThreadLocal<Session>();
    
    public static Session currentSession() throws HibernateException {
        Session s = session.get();
        // Open a new Session, if this Thread has none yet
        if (s == null) {
            s = sessionFactory.openSession();
            session.set(s);
        }
        return s;
    }
    
    public static void closeSession() throws HibernateException {
        Session s = session.get();
        session.set(null);
        if (s != null)
            s.close();
    }
}
