package rpct.db.domain.hibernate;

import org.hibernate.Session;

import rpct.db.DataAccessException;

public interface HibernateContext {

    /** Begins a transaction if it is not yet created */    
    Session currentSession() throws DataAccessException;

    void commit() throws DataAccessException;

    void rollback() throws DataAccessException;

    void closeSession() throws DataAccessException;
}