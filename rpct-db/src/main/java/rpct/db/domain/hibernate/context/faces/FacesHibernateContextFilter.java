package rpct.db.domain.hibernate.context.faces;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import rpct.db.DataAccessException;
import rpct.db.domain.hibernate.HibernateContext;

/**
 * Created on 2005-03-16
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class FacesHibernateContextFilter implements Filter {

    public void destroy() {        
    }

    public void doFilter(ServletRequest request, ServletResponse response, 
            FilterChain chain) throws IOException, ServletException {

        try {
            //HibernateContext hibernateContext = HibernateContext.getInstance(request, true);
            //hibernateContext.currentSession();
            chain.doFilter(request, response);
            HibernateContext hibernateContext = FacesHibernateContextImpl.getInstance(request, false);
            if (hibernateContext != null) {
                hibernateContext.closeSession();
            }
        } catch (DataAccessException e) {
            throw new ServletException(e);
        }
    }

    public void init(FilterConfig arg0) throws ServletException {        
    }

}
