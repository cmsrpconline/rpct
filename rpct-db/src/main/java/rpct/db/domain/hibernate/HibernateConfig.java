package rpct.db.domain.hibernate;

import org.hibernate.cfg.Configuration;

import rpct.db.domain.condition.FebCondition;
import rpct.db.domain.condition.StripResponse;
import rpct.db.domain.configuration.BoardDisabled;
import rpct.db.domain.configuration.BoardDisabledHistory;
import rpct.db.domain.configuration.ChipConfAssignment;
import rpct.db.domain.configuration.ChipConfigTag;
import rpct.db.domain.configuration.ChipDisabled;
import rpct.db.domain.configuration.ChipDisabledHistory;
import rpct.db.domain.configuration.ConfigKey;
import rpct.db.domain.configuration.ConfigKeyAssignment;
import rpct.db.domain.configuration.CrateDisabled;
import rpct.db.domain.configuration.CrateDisabledHistory;
import rpct.db.domain.configuration.CurrentConfigKey;
import rpct.db.domain.configuration.FebBoardDTControlled;
import rpct.db.domain.configuration.FebChipConf;
import rpct.db.domain.configuration.FebDTControlledHistory;
import rpct.db.domain.configuration.HalfSortConf;
import rpct.db.domain.configuration.IntArray12;
import rpct.db.domain.configuration.IntArray16;
import rpct.db.domain.configuration.IntArray18;
import rpct.db.domain.configuration.IntArray24;
import rpct.db.domain.configuration.IntArray4;
import rpct.db.domain.configuration.IntArray6;
import rpct.db.domain.configuration.IntArray9;
import rpct.db.domain.configuration.LinkDisabled;
import rpct.db.domain.configuration.LinkDisabledHistory;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.PacConf;
import rpct.db.domain.configuration.RbcConf;
import rpct.db.domain.configuration.RmbConf;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.configuration.SynCoderConf;
import rpct.db.domain.configuration.SynCoderConfInFlash;
import rpct.db.domain.configuration.TbGbSortConf;
import rpct.db.domain.configuration.TcSortConf;
import rpct.db.domain.configuration.TtuFinalConf;
import rpct.db.domain.configuration.TtuTrigConf;
import rpct.db.domain.configuration.XdaqAppLBoxAccess;
import rpct.db.domain.configuration.XdaqAppVmeCrateAccess;
import rpct.db.domain.configuration.XdaqApplication;
import rpct.db.domain.configuration.XdaqExecutive;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardBoardConn;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ControlCrate;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.DccBoard;
import rpct.db.domain.equipment.FebBoard;
import rpct.db.domain.equipment.FebConnector;
import rpct.db.domain.equipment.I2cCbChannel;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.equipment.RbcBoard;
import rpct.db.domain.equipment.RbcTtuConn;
import rpct.db.domain.equipment.SorterBoard;
import rpct.db.domain.equipment.SorterCrate;
import rpct.db.domain.equipment.TcBackplane;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.equipment.TtuBoard;
import rpct.db.domain.equipment.ccsboard.CcsBoard;
import rpct.db.domain.equipment.chamberloaction.ChamberLocation;
import rpct.db.domain.equipment.chamberstrip.ChamberStrip;
import rpct.db.domain.equipment.chambertype.ChamberType;
import rpct.db.domain.equipment.controlboard.ControlBoard;
import rpct.db.domain.equipment.distributionboard.DistributionBoard;
import rpct.db.domain.equipment.feblocation.FebLocation;

/**
 * Created on 2005-03-01
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class HibernateConfig {
    
    public static void configure(Configuration configuration) {
        
        // equipment
        configuration.addAnnotatedClass(Crate.class);
        configuration.addAnnotatedClass(ControlCrate.class);
        configuration.addAnnotatedClass(LinkBox.class);
        configuration.addAnnotatedClass(TriggerCrate.class);
        configuration.addAnnotatedClass(SorterCrate.class);
        configuration.addAnnotatedClass(BoardBoardConn.class);
        configuration.addAnnotatedClass(LinkConn.class);
        configuration.addAnnotatedClass(RbcTtuConn.class);
        
        configuration.addAnnotatedClass(Board.class);
        configuration.addAnnotatedClass(LinkBoard.class);
        configuration.addAnnotatedClass(ControlBoard.class);
        configuration.addAnnotatedClass(TcBackplane.class);
        configuration.addAnnotatedClass(TriggerBoard.class);
        configuration.addAnnotatedClass(TtuBoard.class);
        configuration.addAnnotatedClass(RbcBoard.class);
        configuration.addAnnotatedClass(SorterBoard.class);
        configuration.addAnnotatedClass(DccBoard.class);
        configuration.addAnnotatedClass(FebBoard.class);
        configuration.addAnnotatedClass(FebLocation.class);
        configuration.addAnnotatedClass(CcsBoard.class);
        configuration.addAnnotatedClass(ChamberLocation.class);
        configuration.addAnnotatedClass(ChamberType.class);
        configuration.addAnnotatedClass(DistributionBoard.class);
        configuration.addAnnotatedClass(FebConnector.class);
        configuration.addAnnotatedClass(I2cCbChannel.class);
        configuration.addAnnotatedClass(Chip.class);        
        configuration.addAnnotatedClass(XdaqExecutive.class);
        configuration.addAnnotatedClass(XdaqApplication.class);
        configuration.addAnnotatedClass(XdaqAppLBoxAccess.class);
        configuration.addAnnotatedClass(XdaqAppVmeCrateAccess.class);
        configuration.addAnnotatedClass(ChamberStrip.class);
        
        
        //Condition
        configuration.addAnnotatedClass(StripResponse.class);
        configuration.addAnnotatedClass(FebCondition.class);
        
        // configuration
        configuration.addAnnotatedClass(ConfigKey.class);
        configuration.addAnnotatedClass(LocalConfigKey.class);
        configuration.addAnnotatedClass(ConfigKeyAssignment.class);
        configuration.addAnnotatedClass(CurrentConfigKey.class);
        configuration.addAnnotatedClass(ChipConfAssignment.class);
        configuration.addAnnotatedClass(StaticConfiguration.class);
        configuration.addAnnotatedClass(SynCoderConf.class);
        configuration.addAnnotatedClass(PacConf.class);
        configuration.addAnnotatedClass(TbGbSortConf.class);
        configuration.addAnnotatedClass(RmbConf.class);
        configuration.addAnnotatedClass(TcSortConf.class);
        configuration.addAnnotatedClass(HalfSortConf.class);
        configuration.addAnnotatedClass(RbcConf.class);
        configuration.addAnnotatedClass(TtuTrigConf.class);
        configuration.addAnnotatedClass(TtuFinalConf.class);
        configuration.addAnnotatedClass(FebChipConf.class);
        configuration.addAnnotatedClass(ChipDisabled.class);
        configuration.addAnnotatedClass(BoardDisabled.class);
        configuration.addAnnotatedClass(CrateDisabled.class);
        configuration.addAnnotatedClass(LinkDisabled.class);
        configuration.addAnnotatedClass(IntArray18.class);
        configuration.addAnnotatedClass(IntArray16.class);
        configuration.addAnnotatedClass(IntArray9.class);
        configuration.addAnnotatedClass(IntArray4.class);
        configuration.addAnnotatedClass(IntArray6.class);
        configuration.addAnnotatedClass(IntArray12.class);
        configuration.addAnnotatedClass(IntArray24.class);
        configuration.addAnnotatedClass(FebBoardDTControlled.class);
        configuration.addAnnotatedClass(ChipDisabledHistory.class);
        configuration.addAnnotatedClass(BoardDisabledHistory.class);
        configuration.addAnnotatedClass(CrateDisabledHistory.class);
        configuration.addAnnotatedClass(FebDTControlledHistory.class);       
        configuration.addAnnotatedClass(LinkDisabledHistory.class);
        configuration.addAnnotatedClass(ChipConfigTag.class);
        configuration.addAnnotatedClass(SynCoderConfInFlash.class);
    }
}
