package rpct.db.domain;

import java.io.Serializable;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import rpct.db.DataAccessException;
import rpct.db.domain.hibernate.HibernateContext;

/**
 * Created on 2005-03-01
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */


public class DAOHibernate implements DAO {

    //protected Transaction tx = null;
    protected HibernateContext hibernateContext;

    public DAOHibernate(HibernateContext hibernateContext) {
        super();
        this.hibernateContext = hibernateContext;
    }

    public Session currentSession() throws DataAccessException {
        return hibernateContext.currentSession();
    }

    public void commit() throws DataAccessException {
        hibernateContext.commit();
    }


    public void rollback() throws DataAccessException {
        hibernateContext.rollback();
    }
    
    public void flush() throws DataAccessException {
        hibernateContext.currentSession().flush();
    }
    
    public void saveObject(Object o) throws DataAccessException {
        try {
            currentSession().saveOrUpdate(o);
        } 
        catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        } 
    }

    public Object getObject(Class clazz, Serializable id) throws DataAccessException {
        try {
            return currentSession().get(clazz, id);
        } 
        catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        }  
    }
    
    public List getObjects(Class clazz) throws DataAccessException {
        try {
            return currentSession().createCriteria(clazz).list();
        } 
        catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        } 
    }
    
    public void deleteObject(Object o) throws DataAccessException {

        try {
            currentSession().delete(o);
        } 
        catch (HibernateException e) {
            rollback();
            throw new DataAccessException(e);
        } 
    }

}
