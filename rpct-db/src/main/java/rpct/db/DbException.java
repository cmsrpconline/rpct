package rpct.db;

public class DbException extends Exception {

    private static final long serialVersionUID = 8716609706548422749L;

    public DbException() {
        super();
    }

    public DbException(String message, Throwable cause) {
        super(message, cause);
    }

    public DbException(String message) {
        super(message);
    }

    public DbException(Throwable cause) {
        super(cause);
    }
}
