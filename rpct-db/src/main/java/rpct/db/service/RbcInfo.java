package rpct.db.service;

public interface RbcInfo {
    String getXdaqUrl();
    int getXdaqAppInstance();
    String getLinkBoxName(); 
}
