package rpct.db.service;

import rpct.xdaq.axis.Binary;

public interface HalfSortConfiguration extends ChipConfiguration {
    Binary getRecChanEna();
    int[] getRecFastClkInv();
    int[] getRecFastClk90();
    int[] getRecFastRegAdd();
    Binary getRecFastDataDelay();
    Binary getRecDataDelay();
}
