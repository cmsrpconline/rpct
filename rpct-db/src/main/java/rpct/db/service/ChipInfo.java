package rpct.db.service;

public interface ChipInfo {
    int getId(); 
    String getType();
    int getPosition();
    boolean isDisabled();
}
