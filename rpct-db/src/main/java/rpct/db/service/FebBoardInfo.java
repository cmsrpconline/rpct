package rpct.db.service;
/**
 * Created on 2010-10-05
 * 
 * @author Karol Bunkowski
 * @version $Id$
 */
public interface FebBoardInfo extends BoardInfo {
	public int getControlBoardId();
	public int getCbChannel();
	public int getFebLocationId();
	public boolean isDtControlled(); 
}
