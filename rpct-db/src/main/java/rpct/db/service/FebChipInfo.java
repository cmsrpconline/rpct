/**
 * Created on 2011-03-02
 *
 * @author Karol Bunkowski
 * @version $Id: FebChipInfo 1563 2010-02-08 12:30:40Z kbunkow $
 */

package rpct.db.service;

public interface FebChipInfo {
	//from Chip
	public abstract int getChipId();	
	public abstract int getPosition();
	
	//from ChamberLocation
	public abstract int getBarrelOrEndcap();
	public abstract int getDiskOrWheel();
	public abstract int getLayer();
	public abstract int getSector();
	public abstract String getSubsector();
	
	//from FebLocation
	public abstract int getFebLocationId();
	public abstract String getFebLocalEtaPartition();
	public abstract int getI2cLocalNumber();
	
	//from i2ccbchannel
	public abstract int getCbChannel();
}