package rpct.db.service;

import rpct.xdaq.axis.Binary;

public interface TbGbSortConfiguration extends ChipConfiguration {
    Binary getRecMuxClkInv();
    Binary getRecMuxClk90();
    Binary getRecMuxRegAdd();
    int[] getRecMuxDelay();
    int[] getRecDataDelay();
}
