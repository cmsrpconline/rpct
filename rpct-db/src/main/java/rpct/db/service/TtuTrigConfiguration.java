package rpct.db.service;

import rpct.xdaq.axis.Binary;

/**
 * Created on 2009-11-30
 * 
 * @author Nikolay Darmenov
 * @version $Id$
 */
public interface TtuTrigConfiguration extends ChipConfiguration {
	Binary getRecMuxClk90();
	Binary getRecMuxClkInv();
	Binary getRecMuxRegAdd();

	int[] getRecMuxDelay();
	int[] getRecDataDelay();

	int getTaTrgDelay();
	int getTaTrgToff();
	int getTaTrgSelect();
	int getTaTrgCntrGate();

	int[] getTaSectorDelay();
	int[] getTaForceLogic0();
	int[] getTaForceLogic1();
	int[] getTaForceLogic2();
	int[] getTaForceLogic3();

	int[] getTrigConfig();
	int[] getTrigMask();
	int[] getTrigForce();
	int getTrigMajority();

	int[] getSectorMajority();
	int getSectorTrigMask();
	int getSectorThreshold();
	
	int getTowerThreshold();
	int getWheelThreshold();
	int getSectTrgSel();
	
	int[] getTaConfig2();
	
	int[] getConnectedRbcsMask();
}
