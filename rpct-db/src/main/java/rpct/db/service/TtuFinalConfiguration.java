package rpct.db.service;

/**
 * Created on 2009-11-30
 * 
 * @author Nikolay Darmenov
 * @version $Id$
 */
public interface TtuFinalConfiguration extends ChipConfiguration {
	int[] getTrigConfig();
	int[] getTrigConfig2();
	
	int getMaskTtu();
	int getMaskPointing();
	int getMaskBlast();
	
	int getDelayTtu();
	int getDelayPointing();
	int getDelayBlast();
	
	int getShapeTtu();
	int getShapePointing();
	int getShapeBlast();
}
