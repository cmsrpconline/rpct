/**
 * Created on 2015-02-24
 * Filip Thyssen
 */

package rpct.db.service;

public interface DTLineFebChipFlatInfo {
    public abstract int getChipId();
    public abstract int getBoardId();
    public abstract String getName();
    public abstract int getI2cAddress();
    public abstract int getPosition();
    public abstract boolean getChipDisabled();
    public abstract boolean getBoardDisabled();
    public abstract String getLocation();
    public abstract String getPartition();
    // public abstract int getFebLocationId();
}
