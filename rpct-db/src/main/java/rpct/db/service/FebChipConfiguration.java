package rpct.db.service;

/**
 * Created on 2009-12-07
 * 
 * @author Nikolay Darmenov
 * @version $Id$
 */
public interface FebChipConfiguration extends ChipConfiguration {
	int getVth();
	int getVthOffset();
	int getVmon();
	int getVmonOffset();	
}
