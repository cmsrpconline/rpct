/**
 * Created on 2015-02-24
 * Filip Thyssen
 */

package rpct.db.service;

import rpct.db.service.DTLineFebChipInfo;

public interface DTLineFebInfo {
    public abstract int getId();
    public abstract String getName();
    public abstract int getI2cAddress();
    public abstract boolean getDisabled();
    public abstract String getLocation();
    public abstract String getPartition();
    // public abstract int getFebLocationId();

    public abstract DTLineFebChipInfo[] getFebChipInfos();
}
