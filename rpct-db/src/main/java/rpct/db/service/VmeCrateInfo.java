package rpct.db.service;

import rpct.db.domain.equipment.CrateType;

public interface VmeCrateInfo {
    int getId();
    String getName();
    String getLabel();
    int getPciSlot();
    int getPosInVmeChain();    
    CrateType getType();
    int getLogSector(); // needed by trigger crate
    boolean isDisabled();
    BoardInfo[] getBoardInfos();
}
