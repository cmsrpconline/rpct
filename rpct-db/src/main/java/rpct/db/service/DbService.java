package rpct.db.service;

import java.math.BigInteger;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import rpct.db.domain.equipment.chamberloaction.BarrelOrEndcap;
import rpct.db.service.axis.ConfigurationSetImplAxis;
import rpct.xdaq.axis.bag.Bag;



public interface DbService {

    FebAccessInfo[] getFebsByBarrelOrEndcap(BarrelOrEndcap barrelOrEndcap) throws RemoteException, ServiceException;
    FebAccessInfo[] getFebsByDiskOrWheel(int diskOrWheel, BarrelOrEndcap barrelOrEndcap) throws RemoteException, ServiceException;
    FebAccessInfo[] getFebsByLayer(int diskOrWheel, int layer, BarrelOrEndcap barrelOrEndcap) throws RemoteException, ServiceException;
    FebAccessInfo[] getFebsBySector(int diskOrWheel, int sector, BarrelOrEndcap barrelOrEndcap) throws RemoteException, ServiceException;
    FebAccessInfo[] getFebsByChamberLocation(int diskOrWheel, int layer, int sector, String subsector, BarrelOrEndcap barrelOrEndcap) throws RemoteException, ServiceException;
    FebAccessInfo[] getFebsByLocationId(int locationId) throws RemoteException, ServiceException;  
    FebAccessInfo[] getSingleFebByLocParameters(int diskOrWheel, int layer, int sector, String subsector, BarrelOrEndcap barrelOrEndcap, String febLocalEtaPartition, int posInLocalEtaPartition) throws RemoteException, ServiceException;
	FebChipInfo[]  getFebChipInfoVector() throws RemoteException, ServiceException; 
	ConfigurationSetImplAxis getConfigurationSetForLocalConfigKey(BigInteger[] chipIds, String localConfigKey) throws RemoteException, ServiceException;
}