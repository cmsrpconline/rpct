/**
 * Created on 2015-02-24
 * Filip Thyssen
 */

package rpct.db.service;

public interface DTLineFebChipInfo {
    public abstract int getId();
    public abstract int getPosition();
    public abstract boolean getDisabled();
}
