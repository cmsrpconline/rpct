package rpct.db.service;


public interface OptoConfiguration extends ChipConfiguration {
    boolean isEnableLink0();
    boolean isEnableLink1();
    boolean isEnableLink2();
}
