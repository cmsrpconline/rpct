/**
 * Created on 2013-08-14
 * Filip Thyssen
 */

package rpct.db.service.axis;

import java.math.BigInteger;
import java.util.Map;

import rpct.db.service.FebConnectorStrips;
import rpct.xdaq.axis.bag.Bag;
import rpct.xdaq.axis.bag.HashBag;

public class FebConnectorStripsImplAxis extends HashBag<Object> implements FebConnectorStrips {

    public final static String KEY_ID = "id";
    public final static String KEY_LINKBOARD_ID = "linkBoardId";
    public final static String KEY_LINKBOARD_INPUT = "linkBoardInput";
    public final static String KEY_FEBBOARD_ID = "febBoardId";
    public final static String KEY_FEBBOARD_CONNECTOR = "febBoardConnector";
    public final static String KEY_LOCATION = "location";
    public final static String KEY_PARTITION = "partition";
    public final static String KEY_ROLL_CONNECTOR = "rollConnector";
    public final static String KEY_PINS = "pins";
    public final static String KEY_FIRST_STRIP = "firstStrip";
    public final static String KEY_SLOPE = "slope";

    public FebConnectorStripsImplAxis() {}
    public FebConnectorStripsImplAxis(int id
                                      , int linkBoardId, int linkBoardInput
                                      , int febBoardId, int febBoardConnector
                                      , String location, String partition, int rollConnector
                                      , int pins, int firstStrip, int slope) {
        setId(id);
        setLinkBoardId(linkBoardId);
        setLinkBoardInput(linkBoardInput);
        setFebBoardId(febBoardId);
        setFebBoardConnector(febBoardConnector);
        setLocation(location);
        setPartition(partition);
        setRollConnector(rollConnector);
        setPins(pins);
        setFirstStrip(firstStrip);
        setSlope(slope);
    }

    public FebConnectorStripsImplAxis(Bag<Object> bag) {
        super(bag);
    }

    public int getId() {
        return ((BigInteger) get(KEY_ID)).intValue();
    }
    public int getLinkBoardId() {
        return ((BigInteger) get(KEY_LINKBOARD_ID)).intValue();
    }
    public int getLinkBoardInput() {
        return ((BigInteger) get(KEY_LINKBOARD_INPUT)).intValue();
    }
    public int getFebBoardId() {
        return ((BigInteger) get(KEY_FEBBOARD_ID)).intValue();
    }
    public int getFebBoardConnector() {
        return ((BigInteger) get(KEY_FEBBOARD_CONNECTOR)).intValue();
    }
    public String getLocation() {
        return (String) get(KEY_LOCATION);
    }
    public String getPartition() {
        return (String) get(KEY_PARTITION);
    }
    public int getRollConnector() {
        return ((BigInteger) get(KEY_ROLL_CONNECTOR)).intValue();
    }
    public int getPins() {
        return ((BigInteger) get(KEY_PINS)).intValue();
    }
    public int getFirstStrip() {
        return ((BigInteger) get(KEY_FIRST_STRIP)).intValue();
    }
    public int getSlope() {
        return ((BigInteger) get(KEY_SLOPE)).intValue();
    }

    public void setId(int id) {
        put(KEY_ID, BigInteger.valueOf(id));
    }
    public void setLinkBoardId(int linkBoardId) {
        put(KEY_LINKBOARD_ID, BigInteger.valueOf(linkBoardId));
    }
    public void setLinkBoardInput(int linkBoardInput) {
        put(KEY_LINKBOARD_INPUT, BigInteger.valueOf(linkBoardInput));
    }
    public void setFebBoardId(int febBoardId) {
        put(KEY_FEBBOARD_ID, BigInteger.valueOf(febBoardId));
    }
    public void setFebBoardConnector(int febBoardConnector) {
        put(KEY_FEBBOARD_CONNECTOR, BigInteger.valueOf(febBoardConnector));
    }
    public void setLocation(String location) {
        put(KEY_LOCATION, location);
    }
    public void setPartition(String partition) {
        put(KEY_PARTITION, partition);
    }
    public void setRollConnector(int rollConnector) {
        put(KEY_ROLL_CONNECTOR, BigInteger.valueOf(rollConnector));
    }
    public void setPins(int pins) {
        put(KEY_PINS, BigInteger.valueOf(pins));
    }
    public void setFirstStrip(int firstStrip) {
        put(KEY_FIRST_STRIP, BigInteger.valueOf(firstStrip));
    }
    public void setSlope(int slope) {
        put(KEY_SLOPE, BigInteger.valueOf(slope));
    }

    public String toString() {
        StringBuilder str = new StringBuilder("[");
        for (Map.Entry<String, Object> entry : this.entrySet()) {
            str.append(entry.getKey()).append('=')
                .append(entry.getValue()).append(", ");
        }
        str.append(']');
        return str.toString();
    }
}
