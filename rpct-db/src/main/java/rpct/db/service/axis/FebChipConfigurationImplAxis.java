package rpct.db.service.axis;

import java.math.BigInteger;

import rpct.db.service.FebChipConfiguration;
import rpct.xdaq.axis.bag.HashBag;

/**
 * Created on 2009-12-07
 * 
 * @author Nikolay Darmenov
 * @version $Id$
 */
public class FebChipConfigurationImplAxis extends HashBag<Object> implements FebChipConfiguration {

    public final static String KEY_ID = "id";
    public final static String KEY_VTH ="vth";
    public final static String KEY_VTHOFFSET ="vthOffset";
    public final static String KEY_VMON ="vmon";
    public final static String KEY_VMONOFFSET ="vmonOffset";

    public FebChipConfigurationImplAxis(int id, 
    		int vth,
    		int vthOffset,
    		int vmon,
    		int vmonOffset) {
        setConfigurationId(id);
        setVth(vth);
        setVthOffset(vthOffset);
        setVmon(vmon);
        setVmonOffset(vmonOffset);
    }
    
    public int getConfigurationId() {
        return ((BigInteger) get(KEY_ID)).intValue();
    }
    public void setConfigurationId(int id) {
        put(KEY_ID, BigInteger.valueOf(id));
    }    

    public int getVth() {
        return ((BigInteger) get(KEY_VTH)).intValue();
    }  
    public void setVth(int vth) {
        put(KEY_VTH, BigInteger.valueOf(vth));
    }
    
    public int getVthOffset() {
        return ((BigInteger) get(KEY_VTHOFFSET)).intValue();
    }  
    public void setVthOffset(int vthOffset) {
        put(KEY_VTHOFFSET, BigInteger.valueOf(vthOffset));
    }
    
    public int getVmon() {
        return ((BigInteger) get(KEY_VMON)).intValue();
    }  
    public void setVmon(int vmon) {
        put(KEY_VMON, BigInteger.valueOf(vmon));
    }
    
    public int getVmonOffset() {
        return ((BigInteger) get(KEY_VMONOFFSET)).intValue();
    }  
    public void setVmonOffset(int vmonOffset) {
        put(KEY_VMONOFFSET, BigInteger.valueOf(vmonOffset));
    }
}
