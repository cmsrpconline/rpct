/**
 * Created on 2006-07-15
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
package rpct.db.service.axis;

import java.math.BigInteger;
import java.util.Map;

import rpct.db.service.CcuRingInfo;
import rpct.db.service.FebBoardInfo;
import rpct.db.service.LinkBoxInfo;
import rpct.xdaq.axis.bag.Bag;
import rpct.xdaq.axis.bag.HashBag;

public class CcuRingInfoImplAxis extends HashBag<Object> implements CcuRingInfo {

    public final static String KEY_PCI_SLOT = "pciSlot";
    public final static String KEY_CCS_POSITION = "ccsPosition";
    public final static String KEY_FEC_POSITION = "fecPosition";
    public final static String KEY_LINK_BOX_INFOS = "linkBoxInfos";
    public final static String KEY_FEB_BOARD_INFOS = "febBoardInfos";
    
    public CcuRingInfoImplAxis(int pciSlot, int ccsPosition, int fecPosition, 
            LinkBoxInfoImplAxis[] linkBoxInfos, FebBoardInfoImplAxis[] febBoardInfos) {
        setPciSlot(pciSlot);
        setCcsPosition(ccsPosition);
        setFecPosition(fecPosition);
        setLinkBoxInfos(linkBoxInfos);
        setFebBoardInfos(febBoardInfos);
    }
    public CcuRingInfoImplAxis(Bag<Object> bag) {
        super(bag);
    }
    
    public int getPciSlot() {
        return ((BigInteger) get(KEY_PCI_SLOT)).intValue();
    }
    public void setPciSlot(int pciSlot) {
        put(KEY_PCI_SLOT, BigInteger.valueOf(pciSlot));
    }

    public int getCcsPosition() {
        return ((BigInteger) get(KEY_CCS_POSITION)).intValue();
    }
    public void setCcsPosition(int ccsPosition) {
        put(KEY_CCS_POSITION, BigInteger.valueOf(ccsPosition));
    }
    
    public int getFecPosition() {
        return ((BigInteger) get(KEY_FEC_POSITION)).intValue();
    }
    public void setFecPosition(int fecPosition) {
        put(KEY_FEC_POSITION, BigInteger.valueOf(fecPosition));
    }
    
    public LinkBoxInfo[] getLinkBoxInfos() {
        return (LinkBoxInfo[]) get(KEY_LINK_BOX_INFOS);
    }
    public void setLinkBoxInfos(LinkBoxInfo[] linkBoxInfos) {
        put(KEY_LINK_BOX_INFOS, linkBoxInfos);
    }
    
	public FebBoardInfo[] getFebBoardInfos() {
		return (FebBoardInfo[]) get(KEY_FEB_BOARD_INFOS);
	}
    public void setFebBoardInfos(FebBoardInfo[] febBoardInfos) {
        put(KEY_FEB_BOARD_INFOS, febBoardInfos);
    }
	
    public String toString() {
        StringBuilder str = new StringBuilder("[");
        for (Map.Entry<String, Object> entry : this.entrySet()) {
            str.append(entry.getKey()).append('=')
            .append(entry.getValue()).append(",\n");
        }
        str.append(']');
        return str.toString();
    }
}