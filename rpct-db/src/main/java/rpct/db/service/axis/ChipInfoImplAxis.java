/**
 * Created on 2006-07-15
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
package rpct.db.service.axis;

import java.math.BigInteger;
import java.util.Map;

import rpct.db.service.ChipInfo;
import rpct.xdaq.axis.bag.Bag;
import rpct.xdaq.axis.bag.HashBag;

public class ChipInfoImplAxis extends HashBag<Object> implements ChipInfo {

    public final static String KEY_ID = "id";
    public final static String KEY_POSITION = "position";
    public final static String KEY_TYPE = "type";
    public final static String KEY_DISABLED = "disabled";

    public ChipInfoImplAxis(int id, int position, String type, boolean disabled) {
        setId(id);
        setPosition(position);
        setType(type);
        setDisabled(disabled);
    }
    public ChipInfoImplAxis(Bag<Object> bag) {
        super(bag);
    }
    
    public int getId() {
        return ((BigInteger) get(KEY_ID)).intValue();
    }
    public void setId(int id) {
        put(KEY_ID, BigInteger.valueOf(id));
    }
    
    public int getPosition() {
        return ((BigInteger) get(KEY_POSITION)).intValue();
    }
    public void setPosition(int position) {
        put(KEY_POSITION, BigInteger.valueOf(position));
    }
    
    public String getType() {
        return (String) get(KEY_TYPE);
    }
    public void setType(String type) {
        put(KEY_TYPE, type);
    }
    
    public boolean isDisabled() {
        return (Boolean) get(KEY_DISABLED);
    }
    public void setDisabled(boolean disabled) {
        put(KEY_DISABLED, disabled);
    }
    
    public String toString() {
        StringBuilder str = new StringBuilder("[");
        for (Map.Entry<String, Object> entry : this.entrySet()) {
            str.append(entry.getKey()).append('=')
            .append(entry.getValue()).append(", ");
        }
        str.append(']');
        return str.toString();
    }
}