package rpct.db.service.axis;

import java.math.BigInteger;

import rpct.db.domain.equipment.CrateType;
import rpct.db.service.TriggerCrateInfo;
import rpct.xdaq.axis.bag.Bag;

public class TriggerCrateInfoImplAxis extends VmeCrateInfoImplAxis implements
        TriggerCrateInfo {

    public final static String KEY_LOGSECTOR = "logSector";

    public TriggerCrateInfoImplAxis(int id, String name, String label, 
            int pciSlot, int posInVmeChain, boolean disabled, 
            BoardInfoImplAxis[] boardInfos, int logSector) {
        super(id, name, label, pciSlot, posInVmeChain, CrateType.TRIGGERCRATE, logSector, disabled, boardInfos);
        setLogSector(logSector);
    }    
    
    public TriggerCrateInfoImplAxis(Bag<Object> bag) {
        super(bag);
    }
    
    public int getLogSector() {
        return ((BigInteger) get(KEY_LOGSECTOR)).intValue();
    }
    public void setLogSector(int logSector) {
        put(KEY_LOGSECTOR, BigInteger.valueOf(logSector));
    }

}
