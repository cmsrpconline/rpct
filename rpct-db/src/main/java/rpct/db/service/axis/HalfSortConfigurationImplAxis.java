package rpct.db.service.axis;

import java.math.BigInteger;

import org.apache.axis.types.HexBinary;

import rpct.db.service.HalfSortConfiguration;
import rpct.xdaq.axis.Binary;
import rpct.xdaq.axis.bag.HashBag;

public class HalfSortConfigurationImplAxis extends HashBag<Object> implements HalfSortConfiguration {

    public final static String KEY_ID = "id";
    public final static String KEY_REC_CHAN_ENA = "recChanEna";
    public final static String KEY_REC_FAST_CLK_INV = "recFastClkInv";
    public final static String KEY_REC_FAST_CLK_90 = "recFastClk90";
    public final static String KEY_REC_FAST_REG_ADD = "recFastRegAdd";
    public final static String KEY_REC_FAST_DATA_DELAY = "recFastDataDelay";
    public final static String KEY_REC_DATA_DELAY = "recDataDelay";

    public HalfSortConfigurationImplAxis(int id, 
            Binary recChanEna, 
            int[] recFastClkInv, 
            int[] recFastClk90, 
            int[] recFastRegAdd,
            Binary recFastDataDelay,
            Binary recDataDelay) {
        setConfigurationId(id);
        setRecChanEna(recChanEna);
        setRecFastClkInv(recFastClkInv);
        setRecFastClk90(recFastClk90);
        setRecFastRegAdd(recFastRegAdd);
        setRecFastDataDelay(recFastDataDelay);
        setRecDataDelay(recDataDelay);
    }
    
    public int getConfigurationId() {
        return ((BigInteger) get(KEY_ID)).intValue();
    }
    public void setConfigurationId(int id) {
        put(KEY_ID, BigInteger.valueOf(id));
    }    

    public Binary getRecChanEna() {
        return new Binary(((HexBinary) get(KEY_REC_CHAN_ENA)).getBytes());
    }  
    public void setRecChanEna(Binary recChanEna) {
        put(KEY_REC_CHAN_ENA, new HexBinary(recChanEna.getBytes()));
    } 

    public int[] getRecFastClkInv() {
        BigInteger[] recFastClkInv = (BigInteger[]) get(KEY_REC_FAST_CLK_INV);
        int[] result = new int[recFastClkInv.length];
        for (int i = 0; i < recFastClkInv.length; i++) {
            result[i] = recFastClkInv[i].intValue();
        }
        return result;
    }  
    public void setRecFastClkInv(int[] recFastClkInv) {
        BigInteger[] val = new BigInteger[recFastClkInv.length];
        for (int i = 0; i < recFastClkInv.length; i++) {
            val[i] = BigInteger.valueOf(recFastClkInv[i]);
        }        
        put(KEY_REC_FAST_CLK_INV, val);
    }

    public int[] getRecFastClk90() {
        BigInteger[] recFastClk90 = (BigInteger[]) get(KEY_REC_FAST_CLK_90);
        int[] result = new int[recFastClk90.length];
        for (int i = 0; i < recFastClk90.length; i++) {
            result[i] = recFastClk90[i].intValue();
        }
        return result;
    }  
    public void setRecFastClk90(int[] recFastClk90) {
        BigInteger[] val = new BigInteger[recFastClk90.length];
        for (int i = 0; i < recFastClk90.length; i++) {
            val[i] = BigInteger.valueOf(recFastClk90[i]);
        }        
        put(KEY_REC_FAST_CLK_90, val);
    }

    public int[] getRecFastRegAdd() {
        BigInteger[] recFastRegAdd = (BigInteger[]) get(KEY_REC_FAST_REG_ADD);
        int[] result = new int[recFastRegAdd.length];
        for (int i = 0; i < recFastRegAdd.length; i++) {
            result[i] = recFastRegAdd[i].intValue();
        }
        return result;
    }  
    public void setRecFastRegAdd(int[] recFastRegAdd) {
        BigInteger[] val = new BigInteger[recFastRegAdd.length];
        for (int i = 0; i < recFastRegAdd.length; i++) {
            val[i] = BigInteger.valueOf(recFastRegAdd[i]);
        }        
        put(KEY_REC_FAST_REG_ADD, val);
    }   

    public Binary getRecFastDataDelay() {
        return new Binary(((HexBinary) get(KEY_REC_FAST_DATA_DELAY)).getBytes());
    }  
    public void setRecFastDataDelay(Binary recFastDataDelay) {
        put(KEY_REC_FAST_DATA_DELAY, new HexBinary(recFastDataDelay.getBytes()));
    }  

    public Binary getRecDataDelay() {
        return new Binary(((HexBinary) get(KEY_REC_DATA_DELAY)).getBytes());
    }  
    public void setRecDataDelay(Binary recDataDelay) {
        put(KEY_REC_DATA_DELAY, new HexBinary(recDataDelay.getBytes()));
    } 
}
