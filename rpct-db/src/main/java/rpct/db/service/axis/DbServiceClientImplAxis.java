package rpct.db.service.axis;

import java.math.BigInteger;
import java.net.URL;
import java.rmi.RemoteException;

import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;
import javax.xml.rpc.ServiceException;

import org.apache.axis.Constants;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;

import rpct.db.domain.equipment.chamberloaction.BarrelOrEndcap;
import rpct.db.service.ChamStripAccessInfo;
import rpct.db.service.FebConnectorStrips;
import rpct.db.service.ConfigurationSet;
import rpct.db.service.DbService;
import rpct.db.service.FebAccessInfo;
import rpct.db.service.FebChipConfiguration;
import rpct.db.service.FebChipInfo;
import rpct.xdaq.axis.bag.Bag;
import rpct.xdaq.axis.bag.BagUtils;

/**
 * Created on 2005-05-05
 * 
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class DbServiceClientImplAxis implements DbService {

    public static final String DB_SERVICE_URI = "urn:rpct-dbservice";
    protected final Service service = new Service();

    private URL endpoint;

    public DbServiceClientImplAxis(URL endpoint) {
        this.endpoint = endpoint;
        BagUtils.registerType(service);
    }

    public URL getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(URL endpoint) {
        this.endpoint = endpoint;
    }

    protected Call prepareCall(String name) throws ServiceException {
        Call call = (Call) service.createCall();
        call.setTargetEndpointAddress(endpoint);
        // call.setSOAPActionURI(soapActionURI);
        call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS,
                Boolean.FALSE);
        call.setOperationName(new QName(DB_SERVICE_URI, name));
        call.setEncodingStyle(Constants.URI_SOAP11_ENC);
        return call;
    }

    public FebAccessInfo[] getFebsByBarrelOrEndcap(BarrelOrEndcap barrelOrEndcap)
            throws RemoteException, ServiceException {
        Call call = prepareCall("getFebsByBarrelOrEndcap");
        call.addParameter(new QName(DB_SERVICE_URI, "barrelOrEndcap"),
                Constants.XSD_STRING, ParameterMode.IN);
        call.setReturnType(Constants.SOAP_ARRAY);
        Bag<Object>[] bags = (Bag<Object>[]) call
                .invoke(new Object[] { barrelOrEndcap.toString() });
        FebAccessInfo[] result = new FebAccessInfo[bags.length];
        for (int i = 0; i < bags.length; i++) {
            result[i] = new FebAccessInfoImplAxis(bags[i]);
        }
        return result;
    }

    public FebAccessInfo[] getFebsByDiskOrWheel(int diskOrWheel,
            BarrelOrEndcap barrelOrEndcap) throws RemoteException,
            ServiceException {
        Call call = prepareCall("getFebsByDiskOrWheel");
        call.addParameter(new QName(DB_SERVICE_URI, "diskOrWheel"),
                Constants.XSD_INTEGER, ParameterMode.IN);
        call.addParameter(new QName(DB_SERVICE_URI, "barrelOrEndcap"),
                Constants.XSD_STRING, ParameterMode.IN);
        call.setReturnType(Constants.SOAP_ARRAY);
        Bag<Object>[] bags = (Bag<Object>[]) call.invoke(new Object[] {
                BigInteger.valueOf(diskOrWheel), barrelOrEndcap.toString() });
        FebAccessInfo[] result = new FebAccessInfo[bags.length];
        for (int i = 0; i < bags.length; i++) {
            result[i] = new FebAccessInfoImplAxis(bags[i]);
        }
        return result;
    }

    public FebAccessInfo[] getFebsByLayer(int diskOrWheel, int layer,
            BarrelOrEndcap barrelOrEndcap) throws RemoteException,
            ServiceException {
        Call call = prepareCall("getFebsByLyaer");
        call.addParameter(new QName(DB_SERVICE_URI, "diskOrWheel"),
                Constants.XSD_INTEGER, ParameterMode.IN);
        call.addParameter(new QName(DB_SERVICE_URI, "layer"),
                Constants.XSD_INTEGER, ParameterMode.IN);
        call.addParameter(new QName(DB_SERVICE_URI, "barrelOrEndcap"),
                Constants.XSD_STRING, ParameterMode.IN);
        call.setReturnType(Constants.SOAP_ARRAY);
        Bag<Object>[] bags = (Bag<Object>[]) call.invoke(new Object[] {
                BigInteger.valueOf(diskOrWheel), BigInteger.valueOf(layer),
                barrelOrEndcap.toString() });
        FebAccessInfo[] result = new FebAccessInfo[bags.length];
        for (int i = 0; i < bags.length; i++) {
            result[i] = new FebAccessInfoImplAxis(bags[i]);
        }
        return result;
    }

    public FebAccessInfo[] getFebsBySector(int diskOrWheel, int sector,
            BarrelOrEndcap barrelOrEndcap) throws RemoteException,
            ServiceException {
        Call call = prepareCall("getFebsBySector");
        call.addParameter(new QName(DB_SERVICE_URI, "diskOrWheel"),
                Constants.XSD_INTEGER, ParameterMode.IN);
        call.addParameter(new QName(DB_SERVICE_URI, "sector"),
                Constants.XSD_INTEGER, ParameterMode.IN);
        call.addParameter(new QName(DB_SERVICE_URI, "barrelOrEndcap"),
                Constants.XSD_STRING, ParameterMode.IN);
        call.setReturnType(Constants.SOAP_ARRAY);
        Bag<Object>[] bags = (Bag<Object>[]) call.invoke(new Object[] {
                BigInteger.valueOf(diskOrWheel), BigInteger.valueOf(sector),
                barrelOrEndcap.toString() });
        FebAccessInfo[] result = new FebAccessInfo[bags.length];
        for (int i = 0; i < bags.length; i++) {
            result[i] = new FebAccessInfoImplAxis(bags[i]);
        }
        return result;
    }

    public FebAccessInfo[] getFebsByChamberLocation(int diskOrWheel, int layer,
            int sector, String subsector, BarrelOrEndcap barrelOrEndcap)
            throws RemoteException, ServiceException {
        Call call = prepareCall("getFebsByChamberLocation");
        call.addParameter(new QName(DB_SERVICE_URI, "diskOrWheel"),
                Constants.XSD_INTEGER, ParameterMode.IN);
        call.addParameter(new QName(DB_SERVICE_URI, "layer"),
                Constants.XSD_INTEGER, ParameterMode.IN);
        call.addParameter(new QName(DB_SERVICE_URI, "sector"),
                Constants.XSD_INTEGER, ParameterMode.IN);
        call.addParameter(new QName(DB_SERVICE_URI, "subsector"),
                Constants.XSD_STRING, ParameterMode.IN);
        call.addParameter(new QName(DB_SERVICE_URI, "barrelOrEndcap"),
                Constants.XSD_STRING, ParameterMode.IN);
        call.setReturnType(Constants.SOAP_ARRAY);
        Bag<Object>[] bags = (Bag<Object>[]) call.invoke(new Object[] {
                BigInteger.valueOf(diskOrWheel), BigInteger.valueOf(layer),
                BigInteger.valueOf(sector), subsector,
                barrelOrEndcap.toString() });
        FebAccessInfo[] result = new FebAccessInfo[bags.length];
        for (int i = 0; i < bags.length; i++) {
            result[i] = new FebAccessInfoImplAxis(bags[i]);
        }
        return result;
    }

    public ChamStripAccessInfo[] getChamStripByChamLoc(int diskOrWheel,
            int layer, int sector, String subsector,
            BarrelOrEndcap barrelOrEndcap) throws RemoteException,
            ServiceException {
        Call call = prepareCall("getChamStripByChamLoc");
        call.addParameter(new QName(DB_SERVICE_URI, "diskOrWheel"),
                Constants.XSD_INTEGER, ParameterMode.IN);
        call.addParameter(new QName(DB_SERVICE_URI, "layer"),
                Constants.XSD_INTEGER, ParameterMode.IN);
        call.addParameter(new QName(DB_SERVICE_URI, "sector"),
                Constants.XSD_INTEGER, ParameterMode.IN);
        call.addParameter(new QName(DB_SERVICE_URI, "subsector"),
                Constants.XSD_STRING, ParameterMode.IN);
        call.addParameter(new QName(DB_SERVICE_URI, "barrelOrEndcap"),
                Constants.XSD_STRING, ParameterMode.IN);
        call.setReturnType(Constants.SOAP_ARRAY);
        Bag<Object>[] bags = (Bag<Object>[]) call.invoke(new Object[] {
                BigInteger.valueOf(diskOrWheel), BigInteger.valueOf(layer),
                BigInteger.valueOf(sector), subsector,
                barrelOrEndcap.toString() });
        ChamStripAccessInfo[] result = new ChamStripAccessInfo[bags.length];
        for (int i = 0; i < bags.length; i++) {
            result[i] = new ChamStripAccessInfoImplAxis(bags[i]);
        }
        return result;
    }

    public FebConnectorStrips[] getFebConnectorStrips(String xdaqExecutiveHost
                                                      , int xdaqExecutivePort) throws RemoteException,
                                                                                      ServiceException {
        Call call = prepareCall("getFebConnectorStrips");
        call.addParameter(new QName(DB_SERVICE_URI, "xdaqExecutiveHost"),
                Constants.XSD_STRING, ParameterMode.IN);
        call.addParameter(new QName(DB_SERVICE_URI, "xdaqExecutivePort"),
                Constants.XSD_INTEGER, ParameterMode.IN);
        call.setReturnType(Constants.SOAP_ARRAY);
        Bag<Object>[] bags = (Bag<Object>[]) call.invoke(new Object[] {
                xdaqExecutiveHost, BigInteger.valueOf(xdaqExecutivePort) });
        FebConnectorStrips[] result = new FebConnectorStrips[bags.length];
        for (int i = 0; i < bags.length; i++) {
            result[i] = new FebConnectorStripsImplAxis(bags[i]);
        }
        return result;
    }

    public FebAccessInfo[] getSingleFebByLocParameters(int diskOrWheel,
            int layer, int sector, String subsector,
            BarrelOrEndcap barrelOrEndcap, String febLocalEtaPartition,
            int posInLocalEtaPartition) throws RemoteException,
            ServiceException {
        Call call = prepareCall("getSingleFebByLocParameters");
        call.addParameter(new QName(DB_SERVICE_URI, "diskOrWheel"),
                Constants.XSD_INTEGER, ParameterMode.IN);
        call.addParameter(new QName(DB_SERVICE_URI, "layer"),
                Constants.XSD_INTEGER, ParameterMode.IN);
        call.addParameter(new QName(DB_SERVICE_URI, "sector"),
                Constants.XSD_INTEGER, ParameterMode.IN);
        call.addParameter(new QName(DB_SERVICE_URI, "subsector"),
                Constants.XSD_STRING, ParameterMode.IN);
        call.addParameter(new QName(DB_SERVICE_URI, "barrelOrEndcap"),
                Constants.XSD_STRING, ParameterMode.IN);
        call.addParameter(new QName(DB_SERVICE_URI, "febLocalEtaPartition"),
                Constants.XSD_STRING, ParameterMode.IN);
        call.addParameter(new QName(DB_SERVICE_URI, "posInLocalEtaPartition"),
                Constants.XSD_INTEGER, ParameterMode.IN);
        call.setReturnType(Constants.SOAP_ARRAY);
        Bag<Object>[] bags = (Bag<Object>[]) call.invoke(new Object[] {
                BigInteger.valueOf(diskOrWheel), BigInteger.valueOf(layer),
                BigInteger.valueOf(sector), subsector,
                barrelOrEndcap.toString(), febLocalEtaPartition,
                BigInteger.valueOf(posInLocalEtaPartition) });
        FebAccessInfo[] result = new FebAccessInfo[bags.length];
        for (int i = 0; i < bags.length; i++) {
            result[i] = new FebAccessInfoImplAxis(bags[i]);
        }
        return result;
    }

    public FebAccessInfo[] getFebsByLocationId(int locationId)
            throws RemoteException, ServiceException {
        Call call = prepareCall("getFebsByLocationId");
        call.addParameter(new QName(DB_SERVICE_URI, "locationId"),
                Constants.XSD_INTEGER, ParameterMode.IN);
        call.setReturnType(Constants.SOAP_ARRAY);
        Bag<Object>[] bags = (Bag<Object>[]) call
                .invoke(new Object[] { BigInteger.valueOf(locationId) });
        FebAccessInfo[] result = new FebAccessInfo[bags.length];
        for (int i = 0; i < bags.length; i++) {
            result[i] = new FebAccessInfoImplAxis(bags[i]);
        }
        return result;
    }
    
    public FebChipInfo[] getFebChipInfoVector() throws RemoteException,
            ServiceException {
        Call call = prepareCall("getFebChipInfoVector");
        call.setReturnType(Constants.SOAP_ARRAY);
        Bag<Object>[] bags = (Bag<Object>[]) call.invoke(new Object[] {});
        FebChipInfo[] result = new FebChipInfo[bags.length];
        for (int i = 0; i < bags.length; i++) {
            result[i] = new FebChipInfoImplAxis(bags[i]);
        }
        return result;
    }

	public ConfigurationSetImplAxis getConfigurationSetForLocalConfigKey(BigInteger[] chipIds,
            String localConfigKey)
			throws RemoteException, ServiceException {
		Call call = prepareCall("getConfigurationSetForLocalConfigKey");
		
		call.addParameter(new QName(DB_SERVICE_URI, "chipIds"),
                Constants.SOAP_ARRAY, ParameterMode.IN);
		call.addParameter(new QName(DB_SERVICE_URI, "localConfigKey"),
                Constants.XSD_STRING, ParameterMode.IN);
        call.setReturnType(Constants.XSD_ANYTYPE);
        Bag<Object> bag = (Bag<Object>) call.invoke(new Object[] {
        		chipIds,
        		localConfigKey
        });
        
        return new ConfigurationSetImplAxis(bag);
	}
}
