/**
 * Created on 2006-01-16
 *
 * @author Michal Pietrusinski & William Whitaker
 * @version $Id$
 */
package rpct.db.service.axis;

import java.math.BigInteger;
import java.util.Map;

import rpct.db.service.FebAccessInfo;
import rpct.xdaq.axis.bag.Bag;
import rpct.xdaq.axis.bag.HashBag;

public class FebAccessInfoImplAxis extends HashBag<Object> implements FebAccessInfo {

    public final static String KEY_FEB_ID = "febId";
    public final static String KEY_XDAQ_URL = "xdaqUrl";
    public final static String KEY_XDAQ_APP_INSTANCE = "xdaqAppInstance";
    public final static String KEY_FEB_LOCAL_ETA_PARTITION = "febLocalEtaPartition";
    public final static String KEY_CHAMBER_LOCATION_NAME = "chamberLocationName";
    public final static String KEY_CCU_ADDRESS = "ccuAddress";
    public final static String KEY_I2C_CHANNEL = "i2cChannel";
    public final static String KEY_FEB_ADDRESS = "febAddress";
    public final static String KEY_POS_IN_LOCAL_ETA_PARTITION = "posInLocalEtaPartition";

    public FebAccessInfoImplAxis(int febId, String xdaqUrl, int xdaqAppInstance, String febLocalEtaPartition, String chamberLocationName, int ccuAddress,
            int i2cChannel, int febAddress, int posInLocalEtaPartition) {
        setFebId(febId);
        setXdaqUrl(xdaqUrl);
        setXdaqAppInstance(xdaqAppInstance);
        setFebLocalEtaPartition(febLocalEtaPartition);
        setChamberLocationName(chamberLocationName);
        setCcuAddress(ccuAddress);
        setI2cChannel(i2cChannel);
        setFebAddress(febAddress);
		setPosInLocalEtaPartition(posInLocalEtaPartition);
    }
    public FebAccessInfoImplAxis(Bag<Object> bag) {
        super(bag);
    }
    public int getFebId() {
        return ((BigInteger) get(KEY_FEB_ID)).intValue();
    }
    public void setFebId(int febId) {
        put(KEY_FEB_ID, BigInteger.valueOf(febId));
    }
    public String getFebLocalEtaPartition() {
        return (String) get(KEY_FEB_LOCAL_ETA_PARTITION);
    }
    public void setFebLocalEtaPartition(String febLocalEtaPartition) {
        put(KEY_FEB_LOCAL_ETA_PARTITION, febLocalEtaPartition);
    }
    public String getChamberLocationName() {
        return (String) get(KEY_CHAMBER_LOCATION_NAME);
    }
    public void setChamberLocationName(String chamberLocationName) {
        put(KEY_CHAMBER_LOCATION_NAME, chamberLocationName);
    }
    public int getCcuAddress() {
        return ((BigInteger) get(KEY_CCU_ADDRESS)).intValue();
    }
    public void setCcuAddress(int ccuAddress) {
        put(KEY_CCU_ADDRESS, BigInteger.valueOf(ccuAddress));
    }
    public int getFebAddress() {
        return ((BigInteger) get(KEY_FEB_ADDRESS)).intValue();
    }
    public void setFebAddress(int febAddress) {
        put(KEY_FEB_ADDRESS, BigInteger.valueOf(febAddress));
    }
    public int getI2cChannel() {
        return ((BigInteger) get(KEY_I2C_CHANNEL)).intValue();
    }
    public void setI2cChannel(int channel) {
        put(KEY_I2C_CHANNEL, BigInteger.valueOf(channel));
    }
    public String getXdaqUrl() {
        return (String) get(KEY_XDAQ_URL);
    }
    public void setXdaqUrl(String xdaqUrl) {
        put(KEY_XDAQ_URL, xdaqUrl);
    }
    public int getXdaqAppInstance() {
        return ((BigInteger) get(KEY_XDAQ_APP_INSTANCE)).intValue();
    }
    public void setXdaqAppInstance(int xdaqAppInstance) {
        put(KEY_XDAQ_APP_INSTANCE, BigInteger.valueOf(xdaqAppInstance));
    }
	public int getPosInLocalEtaPartition() {
        return ((BigInteger) get(KEY_POS_IN_LOCAL_ETA_PARTITION)).intValue();
	}
	public void setPosInLocalEtaPartition(int posInLocalEtaPartition) {
        put(KEY_POS_IN_LOCAL_ETA_PARTITION, BigInteger.valueOf(posInLocalEtaPartition));
	}
    public String toString() {
        StringBuilder str = new StringBuilder("[");
        for (Map.Entry<String, Object> entry : this.entrySet()) {
            str.append(entry.getKey()).append('=')
            .append(entry.getValue()).append(", ");
        }
        str.append(']');
        return str.toString();
    }
}