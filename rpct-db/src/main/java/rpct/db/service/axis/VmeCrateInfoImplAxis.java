/**
 * Created on 2006-07-15
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
package rpct.db.service.axis;

import java.math.BigInteger;
import java.util.Map;

import rpct.db.domain.equipment.CrateType;
import rpct.db.service.BoardInfo;
import rpct.db.service.VmeCrateInfo;
import rpct.xdaq.axis.bag.Bag;
import rpct.xdaq.axis.bag.HashBag;

public class VmeCrateInfoImplAxis extends HashBag<Object> implements VmeCrateInfo {

    public final static String KEY_ID = "id";
    public final static String KEY_NAME = "name";
    public final static String KEY_LABEL = "label";
    public final static String KEY_PCI_SLOT = "pciSlot";
    public final static String KEY_POS_IN_VME_CHAIN = "posInVmeChain";
    public final static String KEY_TYPE = "type";
    public final static String KEY_LOGSECTOR = "logSector";
    public final static String KEY_DISABLED = "disabled";
    public final static String KEY_BOARD_INFOS = "boardInfos";

    public VmeCrateInfoImplAxis(int id, String name, String label, 
            int pciSlot, int posInVmeChain, CrateType type, int logSector, boolean disabled, BoardInfoImplAxis[] boardInfos) {
        setId(id);
        setName(name);
        setLabel(label);
        setPciSlot(pciSlot);
        setPosInVmeChain(posInVmeChain);
        setType(type);
        setLogSector(logSector);
        setDisabled(disabled);
        setBoardInfos(boardInfos);
    }
    public VmeCrateInfoImplAxis(Bag<Object> bag) {
        super(bag);
    }
    
    public int getId() {
        return ((BigInteger) get(KEY_ID)).intValue();
    }
    public void setId(int id) {
        put(KEY_ID, BigInteger.valueOf(id));
    }

    public String getName() {
        return (String) get(KEY_NAME);
    }
    public void setName(String name) {
        put(KEY_NAME, name);
    }

    public String getLabel() {
        return (String) get(KEY_LABEL);
    }
    public void setLabel(String label) {
        put(KEY_LABEL, label);
    }
    
    public int getPciSlot() {
        return ((BigInteger) get(KEY_PCI_SLOT)).intValue();
    }
    public void setPciSlot(int pciSlot) {
        put(KEY_PCI_SLOT, BigInteger.valueOf(pciSlot));
    }
    
    public int getPosInVmeChain() {
        return ((BigInteger) get(KEY_POS_IN_VME_CHAIN)).intValue();
    }
    public void setPosInVmeChain(int posInVmeChain) {
        put(KEY_POS_IN_VME_CHAIN, BigInteger.valueOf(posInVmeChain));
    }
    
    public CrateType getType() {
        return CrateType.valueOf((String) get(KEY_TYPE));
    }
    public void setType(CrateType type) {
        put(KEY_TYPE, type.name());
    }
    
    public int getLogSector() {
        return ((BigInteger) get(KEY_LOGSECTOR)).intValue();
    }
    public void setLogSector(int logSector) {
        put(KEY_LOGSECTOR, BigInteger.valueOf(logSector));
    }
    
    public boolean isDisabled() {
        return (Boolean) get(KEY_DISABLED);
    }
    public void setDisabled(boolean disabled) {
        put(KEY_DISABLED, disabled);
    }
    
    public BoardInfo[] getBoardInfos() {
        return (BoardInfo[]) get(KEY_BOARD_INFOS);
    }
    public void setBoardInfos(BoardInfo[] boardInfos) {
        put(KEY_BOARD_INFOS, boardInfos);
    }
    
    
    public String toString() {
        StringBuilder str = new StringBuilder("[");
        for (Map.Entry<String, Object> entry : this.entrySet()) {
            str.append(entry.getKey()).append('=')
            .append(entry.getValue()).append(", ");
        }
        str.append(']');
        return str.toString();
    }
}