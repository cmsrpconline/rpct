/**
 * Created on 2006-12-11
 *
 * @author Michal Pietrusinski & William Whitaker
 */
package rpct.db.service.axis;

import java.math.BigInteger;
import java.util.Map;

import rpct.db.service.ChamStripAccessInfo;
import rpct.xdaq.axis.bag.Bag;
import rpct.xdaq.axis.bag.HashBag;

public class ChamStripAccessInfoImplAxis extends HashBag<Object> implements ChamStripAccessInfo {

    public final static String KEY_CHAMBER_LOCATION_NAME = "chamberLocationName";
    public final static String KEY_BOARDID = "boardId";
    public final static String KEY_XDAQ_APP_INSTANCE = "xdaqAppInstance";
    public final static String KEY_CHIPID = "chipId";
    public final static String KEY_FEBID = "febId";
    public final static String KEY_FEB_CONNECTOR_NUM = "febConnectorNum";
	public final static String KEY_FEB_LOCAL_ETA_PARTITION = "febLocalEtaPartition";
    public final static String KEY_FEB_I2C_ADDRESS = "febI2cAddress";
    public final static String KEY_CABLE_CHANNEL_NUM = "cableChanNum";
    public final static String KEY_CHAMBER_STRIP_NUM = "chamberStripNum";
    public final static String KEY_CHAMBER_STRIP_ID = "chamberStripId";

    public ChamStripAccessInfoImplAxis(String chamberLocationName, int boardId, int xdaqAppInstance, int chipId, int febId,
    		int febConnectorNum, String febLocalEtaPartition, int febI2cAddress, int cableChanNum,
    		int chamberStripNum, int chamberStripId) {

	    	setChamberLocationName(chamberLocationName);
	    	setBoardId(boardId);
	        setXdaqAppInstance(xdaqAppInstance);
	    	setChipId(chipId);
	    	setFebId(febId);
	    	setFebConnectorNum(febConnectorNum);
	    	setFebLocalEtaPartition(febLocalEtaPartition);
	    	setFebI2cAddress(febI2cAddress);
	    	setCableChanNum(cableChanNum);
	    	setChamberStripNum(chamberStripNum);
	    	setChamberStripId(chamberStripId);

    }
    public ChamStripAccessInfoImplAxis(Bag<Object> bag) {
        super(bag);
    }
    public String getChamberLocationName() {
        return (String) get(KEY_CHAMBER_LOCATION_NAME);
    }
    public void setChamberLocationName(String chamberLocationName) {
        put(KEY_CHAMBER_LOCATION_NAME, chamberLocationName);
    }
    public int getBoardId() {
        return ((BigInteger) get(KEY_BOARDID)).intValue();
    }
    public void setBoardId(int boardId) {
        put(KEY_BOARDID, BigInteger.valueOf(boardId));
    }
    public int getXdaqAppInstance() {
        return ((BigInteger) get(KEY_XDAQ_APP_INSTANCE)).intValue();
    }
    public void setXdaqAppInstance(int xdaqAppInstance) {
        put(KEY_XDAQ_APP_INSTANCE, BigInteger.valueOf(xdaqAppInstance));
    }
    public int getChipId() {
        return ((BigInteger) get(KEY_CHIPID)).intValue();
    }
    public void setChipId(int chipId) {
        put(KEY_CHIPID, BigInteger.valueOf(chipId));
    }
    public int getFebId() {
        return ((BigInteger) get(KEY_FEBID)).intValue();
    }
    public void setFebId(int febId) {
        put(KEY_FEBID, BigInteger.valueOf(febId));
    }
    public int getFebConnectorNum() {
        return ((BigInteger) get(KEY_FEB_CONNECTOR_NUM)).intValue();
    }
    public void setFebConnectorNum(int febConnectorNum) {
        put(KEY_FEB_CONNECTOR_NUM, BigInteger.valueOf(febConnectorNum));
    }
    public String getFebLocalEtaPartition() {
        return (String) get(KEY_FEB_LOCAL_ETA_PARTITION);
    }
    public void setFebLocalEtaPartition(String febLocalEtaPartition) {
        put(KEY_FEB_LOCAL_ETA_PARTITION, febLocalEtaPartition);
    }
    public int getFebI2cAddress() {
        return ((BigInteger) get(KEY_FEB_I2C_ADDRESS)).intValue();
    }
    public void setFebI2cAddress(int febI2cAddress) {
        put(KEY_FEB_I2C_ADDRESS, BigInteger.valueOf(febI2cAddress));
    }
    public int getCableChanNum() {
        return ((BigInteger) get(KEY_CABLE_CHANNEL_NUM)).intValue();
    }
    public void setCableChanNum(int cableChanNum) {
        put(KEY_CABLE_CHANNEL_NUM, BigInteger.valueOf(cableChanNum));
    }
    public int getChamberStripNum() {
        return ((BigInteger) get(KEY_CHAMBER_STRIP_NUM)).intValue();
    }
    public void setChamberStripNum(int chamberStripNum) {
        put(KEY_CHAMBER_STRIP_NUM, BigInteger.valueOf(chamberStripNum));
    }
    public int getChamberStripId() {
        return ((BigInteger) get(KEY_CHAMBER_STRIP_ID)).intValue();
    }
    public void setChamberStripId(int chamberStripId) {
        put(KEY_CHAMBER_STRIP_ID, BigInteger.valueOf(chamberStripId));
    }
    public String toString() {
        StringBuilder str = new StringBuilder("[");
        for (Map.Entry<String, Object> entry : this.entrySet()) {
            str.append(entry.getKey()).append('=')
            .append(entry.getValue()).append(", ");
        }
        str.append(']');
        return str.toString();
    }
}