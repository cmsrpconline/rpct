package rpct.db.service.axis;

import java.math.BigInteger;

import rpct.db.service.SynCoderConfInFlashInfo;
import rpct.xdaq.axis.bag.HashBag;

/**
 * Created on 2012-02-01
 *
 * @author Karol Bunkowski
 * @version $Id: $
 */
public class SynCoderConfInFlashInfoImplAxis extends HashBag<Object> implements SynCoderConfInFlashInfo {
    public final static String KEY_CHIP_ID = "chipId";
    public final static String KEY_CONFIGURATION_ID = "confId";
    public final static String KEY_BEGINADDRESS = "begAddr";
    public final static String KEY_ENDADDRESS = "endAddr";
    
    public SynCoderConfInFlashInfoImplAxis(int chipId, int confId, int begAddr, int endAddr) {
    	setChipId(chipId);
    	setSynCoderConfId(confId);
    	setBeginAddress(begAddr);
    	setEndAddress(endAddr);
    }
    
	public int getChipId() {
		return ((BigInteger) get(KEY_CHIP_ID)).intValue();
	}
	
	void setChipId(int chipId) {
		put(KEY_CHIP_ID, BigInteger.valueOf(chipId));
	}

	
	public int getSynCoderConfId() {
		return ((BigInteger) get(KEY_CONFIGURATION_ID)).intValue();
	}
	
	void setSynCoderConfId(int confId) {
		put(KEY_CONFIGURATION_ID, BigInteger.valueOf(confId));
	}

	
	public int getBeginAddress() {
		return ((BigInteger) get(KEY_BEGINADDRESS)).intValue();
	}
	void setBeginAddress(int begAddr) {
		put(KEY_BEGINADDRESS, BigInteger.valueOf(begAddr));
	}
	

	public int getEndAddress() {
		return ((BigInteger) get(KEY_ENDADDRESS)).intValue();
	}
	void setEndAddress(int endAddr) {
		put(KEY_ENDADDRESS, BigInteger.valueOf(endAddr));
	}
	   
    public String toString() {
    	String str = "";
    	str = "chipId " + getChipId() + " ConfId " + get(KEY_CONFIGURATION_ID) + " BeginAddr " + get(KEY_BEGINADDRESS) + " EndAddr " + get(KEY_ENDADDRESS); 
    	return str;
    }
}
