package rpct.db.service.axis;

import java.math.BigInteger;
import java.util.Map;

import rpct.db.service.FebBoardInfo;
/**
 * Created on 2010-10-06
 * 
 * @author Karol Bunkowski
 * @version $Id$
 */
public class FebBoardInfoImplAxis extends BoardInfoImplAxis implements
		FebBoardInfo {

    public final static String KEY_CB_ID = "cbId";
    public final static String KEY_CB_CHANNEL = "cbChannel";
    public final static String KEY_FEBLOCATION_ID = "febLocationId";
    public final static String KEY_DTCONTROLLED = "dtControlled";
    
    public FebBoardInfoImplAxis(int id, String name, String label, String type, int i2cAddress,
            boolean disabled, ChipInfoImplAxis[] chipInfos, int controlBoardId, int cbChannel, boolean dtControlled) {
    	super(id, name, label, type, i2cAddress, disabled, chipInfos);
    	setControlBoardId(controlBoardId);
    	setCbChannel(cbChannel);
    	setDtControlled(dtControlled);
    }
    
 	public int getControlBoardId() {
		return ((BigInteger) get(KEY_CB_ID)).intValue();
	}
    public void setControlBoardId(int id) {
        put(KEY_CB_ID, BigInteger.valueOf(id));
    }       

	public int getCbChannel() {
		return ((BigInteger) get(KEY_CB_CHANNEL)).intValue();
	}
    public void setCbChannel(int cbChannel) {
        put(KEY_CB_CHANNEL, BigInteger.valueOf(cbChannel));
    }
    
	public int getFebLocationId() {
		return ((BigInteger) get(KEY_FEBLOCATION_ID)).intValue();
	}
    public void setFebLocationId(int febLocationId) {
        put(KEY_FEBLOCATION_ID, BigInteger.valueOf(febLocationId));
    }
	
	public boolean isDtControlled() {
		return ((Boolean) get(KEY_DTCONTROLLED));
	}
    public void setDtControlled(boolean dtControlled) {
        put(KEY_DTCONTROLLED, dtControlled);
    }
    
    public String toString() {
        StringBuilder str = new StringBuilder("[");
        for (Map.Entry<String, Object> entry : this.entrySet()) {
            str.append(entry.getKey()).append('=')
            .append(entry.getValue()).append(", ");
        }
        str.append(']');
        return str.toString();
    }
}
