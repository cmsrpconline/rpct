package rpct.db.service.axis;

import java.math.BigInteger;

import org.apache.axis.types.HexBinary;

import rpct.db.service.TtuTrigConfiguration;
import rpct.xdaq.axis.Binary;
import rpct.xdaq.axis.bag.HashBag;

/**
 * Created on 2009-11-30
 * 
 * @author Nikolay Darmenov
 * @version $Id$
 */
public class TtuTrigConfigurationImplAxis extends HashBag<Object> implements TtuTrigConfiguration {

    public final static String KEY_ID = "id";
    
    public final static String KEY_REC_MUX_CLK_90 = "recMuxClk90";
    public final static String KEY_REC_MUX_CLK_INV = "recMuxClkInv";
    public final static String KEY_REC_MUX_REG_ADD = "recMuxRegAdd";
    public final static String KEY_REC_MUX_DELAY = "recMuxDelay";

    public final static String KEY_REC_DATA_DELAY = "recDataDelay";

    public final static String KEY_TA_TRG_DELAY = "taTrgDelay";
    public final static String KEY_TA_TRG_TOFF = "taTrgToff";
    public final static String KEY_TA_TRG_SELECT = "taTrgSelect";
    public final static String KEY_TA_TRG_CNTR_GATE = "taTrgCntrGate";

    public final static String KEY_TA_SECTOR_DELAY = "taSectorDelay";
    
    public final static String KEY_TA_FORCE_LOGIC_0 = "taForceLogic0";
    public final static String KEY_TA_FORCE_LOGIC_1 = "taForceLogic1";
    public final static String KEY_TA_FORCE_LOGIC_2 = "taForceLogic2";
    public final static String KEY_TA_FORCE_LOGIC_3 = "taForceLogic3";

    public final static String KEY_TRIG_CONFIG = "trigConfig";
    public final static String KEY_TRIG_MASK = "trigMask";
    public final static String KEY_TRIG_FORCE = "trigForce";
    public final static String KEY_TRIG_MAJORITY = "trigMajority";

    public final static String KEY_SECTOR_MAJORITY = "sectorMajority";
    public final static String KEY_SECTOR_TRIG_MASK = "sectorTrigMask";
    public final static String KEY_SECTOR_THRESHOLD = "sectorThreshold";

    public final static String KEY_TOWER_THRESHOLD = "towerThreshold";
    public final static String KEY_WHEEL_THRESHOLD = "wheelThreshold";
    public final static String KEY_SECT_TRG_SEL = "sectTrgSel";
    public final static String KEY_TA_CONFIG2 = "taConfig2";
    public final static String KEY_CONNECTED_RBCS_MASK = "connectedRbcsMask";

    public TtuTrigConfigurationImplAxis(int id, 
            Binary recMuxClk90,
            Binary recMuxClkInv,
            Binary recMuxRegAdd,
            int[] recMuxDelay,
            int[] recDataDelay,
            int taTrgDelay,
            int taTrgToff,
            int taTrgSelect,
            int taTrgCntrGate,
            int[] taSectorDelay,
            int[] taForceLogic0,
            int[] taForceLogic1,
            int[] taForceLogic2,
            int[] taForceLogic3,
            int[] trigConfig,
            int[] trigMask,
            int[] trigForce,
            int trigMajority,
            int[] sectorMajority,
            int sectorTrigMask,
            int sectorThreshold,
            int towerThreshold,
            int wheelThreshold,
            int sectTrgSel,
            int[] taConfig2,
            int[] connectedRbcsMask) {
        setConfigurationId(id);
        setRecMuxClk90(recMuxClk90);
        setRecMuxClkInv(recMuxClkInv);
        setRecMuxRegAdd(recMuxRegAdd);
        setRecMuxDelay(recMuxDelay);
        setRecDataDelay(recDataDelay);
        setTaTrgDelay(taTrgDelay);
        setTaTrgToff(taTrgToff);
        setTaTrgSelect(taTrgSelect);
        setTaTrgCntrGate(taTrgCntrGate);
        setTaSectorDelay(taSectorDelay);
        setTaForceLogic0(taForceLogic0);
        setTaForceLogic1(taForceLogic1);
        setTaForceLogic2(taForceLogic2);
        setTaForceLogic3(taForceLogic3);
        setTrigConfig(trigConfig);
        setTrigMask(trigMask);
        setTrigForce(trigForce);
        setTrigMajority(trigMajority);
        setSectorMajority(sectorMajority);
        setSectorTrigMask(sectorTrigMask);
        setSectorThreshold(sectorThreshold);
        setTowerThreshold(towerThreshold);
        setWheelThreshold(wheelThreshold);
        setSectTrgSel(sectTrgSel);
        setTaConfig2(taConfig2);
        setConnectedRbcsMask(connectedRbcsMask);
    }
    
    public int getConfigurationId() {
        return ((BigInteger) get(KEY_ID)).intValue();
    }
    public void setConfigurationId(int id) {
        put(KEY_ID, BigInteger.valueOf(id));
    }    

    public Binary getRecMuxClk90() {
        return new Binary(((HexBinary) get(KEY_REC_MUX_CLK_90)).getBytes());
    }  
    public void setRecMuxClk90(Binary recMuxClk90) {
        put(KEY_REC_MUX_CLK_90, new HexBinary(recMuxClk90.getBytes()));
    }   

    public Binary getRecMuxClkInv() {
        return new Binary(((HexBinary) get(KEY_REC_MUX_CLK_INV)).getBytes());
    }  
    public void setRecMuxClkInv(Binary recMuxClkInv) {
        put(KEY_REC_MUX_CLK_INV, new HexBinary(recMuxClkInv.getBytes()));
    }   

    public Binary getRecMuxRegAdd() {
        return new Binary(((HexBinary) get(KEY_REC_MUX_REG_ADD)).getBytes());
    }  
    public void setRecMuxRegAdd(Binary recMuxRegAdd) {
        put(KEY_REC_MUX_REG_ADD, new HexBinary(recMuxRegAdd.getBytes()));
    }   

    public int[] getRecMuxDelay() {
        BigInteger[] recMuxDelay = (BigInteger[]) get(KEY_REC_MUX_DELAY);
        int[] result = new int[recMuxDelay.length];
        for (int i = 0; i < recMuxDelay.length; i++) {
            result[i] = recMuxDelay[i].intValue();
        }
        return result;
    }  
    public void setRecMuxDelay(int[] recMuxDelay) {
        BigInteger[] v = new BigInteger[recMuxDelay.length];
        for (int i = 0; i < recMuxDelay.length; i++) {
            v[i] = BigInteger.valueOf(recMuxDelay[i]);
        }
        
        put(KEY_REC_MUX_DELAY, v);
    }

    public int[] getRecDataDelay() {
        BigInteger[] recDataDelay = (BigInteger[]) get(KEY_REC_DATA_DELAY);
        int[] result = new int[recDataDelay.length];
        for (int i = 0; i < recDataDelay.length; i++) {
            result[i] = recDataDelay[i].intValue();
        }
        return result;
    }  
    public void setRecDataDelay(int[] recDataDelay) {
        BigInteger[] v = new BigInteger[recDataDelay.length];
        for (int i = 0; i < recDataDelay.length; i++) {
            v[i] = BigInteger.valueOf(recDataDelay[i]);
        }
        
        put(KEY_REC_DATA_DELAY, v);
    }

    public int getTaTrgDelay() {
        return ((BigInteger) get(KEY_TA_TRG_DELAY)).intValue();
    }  
    public void setTaTrgDelay(int taTrgDelay) {
        put(KEY_TA_TRG_DELAY, BigInteger.valueOf(taTrgDelay));
    }

    public int getTaTrgToff() {
        return ((BigInteger) get(KEY_TA_TRG_TOFF)).intValue();
    }  
    public void setTaTrgToff(int taTrgToff) {
        put(KEY_TA_TRG_TOFF, BigInteger.valueOf(taTrgToff));
    }

    public int getTaTrgSelect() {
        return ((BigInteger) get(KEY_TA_TRG_SELECT)).intValue();
    }  
    public void setTaTrgSelect(int taTrgSelect) {
        put(KEY_TA_TRG_SELECT, BigInteger.valueOf(taTrgSelect));
    }

    public int getTaTrgCntrGate() {
        return ((BigInteger) get(KEY_TA_TRG_CNTR_GATE)).intValue();
    }  
    public void setTaTrgCntrGate(int taTrgCntrGate) {
        put(KEY_TA_TRG_CNTR_GATE, BigInteger.valueOf(taTrgCntrGate));
    }

    public int[] getTaSectorDelay() {
        BigInteger[] v = (BigInteger[]) get(KEY_TA_SECTOR_DELAY);
        int[] result = new int[v.length];
        for (int i = 0; i < v.length; i++) {
            result[i] = v[i].intValue();
        }
        return result;
    }  
    public void setTaSectorDelay(int[] taSectorDelay) {
        BigInteger[] v = new BigInteger[taSectorDelay.length];
        for (int i = 0; i < taSectorDelay.length; i++) {
            v[i] = BigInteger.valueOf(taSectorDelay[i]);
        }
        
        put(KEY_TA_SECTOR_DELAY, v);
    }

    public int[] getTaForceLogic0() {
        BigInteger[] v = (BigInteger[]) get(KEY_TA_FORCE_LOGIC_0);
        int[] result = new int[v.length];
        for (int i = 0; i < v.length; i++) {
            result[i] = v[i].intValue();
        }
        return result;
    }  
    public void setTaForceLogic0(int[] taForceLogic0) {
        BigInteger[] v = new BigInteger[taForceLogic0.length];
        for (int i = 0; i < taForceLogic0.length; i++) {
            v[i] = BigInteger.valueOf(taForceLogic0[i]);
        }
        
        put(KEY_TA_FORCE_LOGIC_0, v);
    }

    public int[] getTaForceLogic1() {
        BigInteger[] v = (BigInteger[]) get(KEY_TA_FORCE_LOGIC_1);
        int[] result = new int[v.length];
        for (int i = 0; i < v.length; i++) {
            result[i] = v[i].intValue();
        }
        return result;
    }  
    public void setTaForceLogic1(int[] taForceLogic1) {
        BigInteger[] v = new BigInteger[taForceLogic1.length];
        for (int i = 0; i < taForceLogic1.length; i++) {
            v[i] = BigInteger.valueOf(taForceLogic1[i]);
        }
        
        put(KEY_TA_FORCE_LOGIC_1, v);
    }

    public int[] getTaForceLogic2() {
        BigInteger[] v = (BigInteger[]) get(KEY_TA_FORCE_LOGIC_2);
        int[] result = new int[v.length];
        for (int i = 0; i < v.length; i++) {
            result[i] = v[i].intValue();
        }
        return result;
    }  
    public void setTaForceLogic2(int[] taForceLogic2) {
        BigInteger[] v = new BigInteger[taForceLogic2.length];
        for (int i = 0; i < taForceLogic2.length; i++) {
            v[i] = BigInteger.valueOf(taForceLogic2[i]);
        }
        
        put(KEY_TA_FORCE_LOGIC_2, v);
    }

    public int[] getTaForceLogic3() {
        BigInteger[] v = (BigInteger[]) get(KEY_TA_FORCE_LOGIC_3);
        int[] result = new int[v.length];
        for (int i = 0; i < v.length; i++) {
            result[i] = v[i].intValue();
        }
        return result;
    }  
    public void setTaForceLogic3(int[] taForceLogic3) {
        BigInteger[] v = new BigInteger[taForceLogic3.length];
        for (int i = 0; i < taForceLogic3.length; i++) {
            v[i] = BigInteger.valueOf(taForceLogic3[i]);
        }
        
        put(KEY_TA_FORCE_LOGIC_3, v);
    }

    public int[] getTrigConfig() {
        BigInteger[] v = (BigInteger[]) get(KEY_TRIG_CONFIG);
        int[] result = new int[v.length];
        for (int i = 0; i < v.length; i++) {
            result[i] = v[i].intValue();
        }
        return result;
    }  
    public void setTrigConfig(int[] trigConfig) {
        BigInteger[] v = new BigInteger[trigConfig.length];
        for (int i = 0; i < trigConfig.length; i++) {
            v[i] = BigInteger.valueOf(trigConfig[i]);
        }
        
        put(KEY_TRIG_CONFIG, v);
    }

    public int[] getTrigMask() {
        BigInteger[] v = (BigInteger[]) get(KEY_TRIG_MASK);
        int[] result = new int[v.length];
        for (int i = 0; i < v.length; i++) {
            result[i] = v[i].intValue();
        }
        return result;
    }  
    public void setTrigMask(int[] trigMask) {
        BigInteger[] v = new BigInteger[trigMask.length];
        for (int i = 0; i < trigMask.length; i++) {
            v[i] = BigInteger.valueOf(trigMask[i]);
        }
        
        put(KEY_TRIG_MASK, v);
    }

    public int[] getTrigForce() {
        BigInteger[] v = (BigInteger[]) get(KEY_TRIG_FORCE);
        int[] result = new int[v.length];
        for (int i = 0; i < v.length; i++) {
            result[i] = v[i].intValue();
        }
        return result;
    }  
    public void setTrigForce(int[] trigForce) {
        BigInteger[] v = new BigInteger[trigForce.length];
        for (int i = 0; i < trigForce.length; i++) {
            v[i] = BigInteger.valueOf(trigForce[i]);
        }
        
        put(KEY_TRIG_FORCE, v);
    }

    public int getTrigMajority() {
        return ((BigInteger) get(KEY_TRIG_MAJORITY)).intValue();
    }  
    public void setTrigMajority(int trigMajority) {
        put(KEY_TRIG_MAJORITY, BigInteger.valueOf(trigMajority));
    }

    public int[] getSectorMajority() {
        BigInteger[] v = (BigInteger[]) get(KEY_SECTOR_MAJORITY);
        int[] result = new int[v.length];
        for (int i = 0; i < v.length; i++) {
            result[i] = v[i].intValue();
        }
        return result;
    }  
    public void setSectorMajority(int[] sectorMajority) {
        BigInteger[] v = new BigInteger[sectorMajority.length];
        for (int i = 0; i < sectorMajority.length; i++) {
            v[i] = BigInteger.valueOf(sectorMajority[i]);
        }
        
        put(KEY_SECTOR_MAJORITY, v);
    }

    public int getSectorTrigMask() {
        return ((BigInteger) get(KEY_SECTOR_TRIG_MASK)).intValue();
    }  
    public void setSectorTrigMask(int sectorTrigMask) {
        put(KEY_SECTOR_TRIG_MASK, BigInteger.valueOf(sectorTrigMask));
    }

    public int getSectorThreshold() {
        return ((BigInteger) get(KEY_SECTOR_THRESHOLD)).intValue();
    }  
    public void setSectorThreshold(int sectorThreshold) {
        put(KEY_SECTOR_THRESHOLD, BigInteger.valueOf(sectorThreshold));
    }

    public int getTowerThreshold() {
        return ((BigInteger) get(KEY_TOWER_THRESHOLD)).intValue();
    }  
    public void setTowerThreshold(int towerThreshold) {
        put(KEY_TOWER_THRESHOLD, BigInteger.valueOf(towerThreshold));
    }

    public int getWheelThreshold() {
        return ((BigInteger) get(KEY_WHEEL_THRESHOLD)).intValue();
    }  
    public void setWheelThreshold(int wheelThreshold) {
        put(KEY_WHEEL_THRESHOLD, BigInteger.valueOf(wheelThreshold));
    }

    public int getSectTrgSel() {
        return ((BigInteger) get(KEY_SECT_TRG_SEL)).intValue();
    }  
    public void setSectTrgSel(int sectTrgSel) {
        put(KEY_SECT_TRG_SEL, BigInteger.valueOf(sectTrgSel));
    }

    public int[] getTaConfig2() {
        BigInteger[] v = (BigInteger[]) get(KEY_TA_CONFIG2);
        int[] result = new int[v.length];
        for (int i = 0; i < v.length; i++) {
            result[i] = v[i].intValue();
        }
        return result;
    }  
    public void setTaConfig2(int[] taConfig2) {
        BigInteger[] v = new BigInteger[taConfig2.length];
        for (int i = 0; i < taConfig2.length; i++) {
            v[i] = BigInteger.valueOf(taConfig2[i]);
        }
        
        put(KEY_TA_CONFIG2, v);
    }

    public int[] getConnectedRbcsMask() {
        BigInteger[] v = (BigInteger[]) get(KEY_CONNECTED_RBCS_MASK);
        int[] result = new int[v.length];
        for (int i = 0; i < v.length; i++) {
            result[i] = v[i].intValue();
        }
        return result;
    }  
    public void setConnectedRbcsMask(int[] ConnectedRbcsMask) {
        BigInteger[] v = new BigInteger[ConnectedRbcsMask.length];
        for (int i = 0; i < ConnectedRbcsMask.length; i++) {
            v[i] = BigInteger.valueOf(ConnectedRbcsMask[i]);
        }
        
        put(KEY_CONNECTED_RBCS_MASK, v);
    }
    
    public String toString() {
    	String str = "";
    	int[] connectedRbcsMask = getConnectedRbcsMask();
    	for(int i = 0; i < connectedRbcsMask.length; i++) {
    		str += i + " " + Integer.toHexString(connectedRbcsMask[i]) + "\n";
    	}
    	return str;    	
    }
}
