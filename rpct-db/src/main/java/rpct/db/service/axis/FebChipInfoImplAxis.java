/**
 * Created on 2011-03-02
 *
 * @author Karol Bunkowski
 * @version $Id: FebChipInfo 1563 2010-02-08 12:30:40Z kbunkow $
 */
package rpct.db.service.axis;

import java.math.BigInteger;

import rpct.db.service.FebChipInfo;
import rpct.xdaq.axis.bag.Bag;
import rpct.xdaq.axis.bag.HashBag;

public class FebChipInfoImplAxis  extends HashBag<Object> implements FebChipInfo {
	//from Chip
	public final static String ID = "id";
    public final static String POSITION = "pos";
    
    //from ChamberLocation
    public final static String BARREL_OR_ENDCAP = "boe"; 
    public final static String DISK_OR_WHEEL = "wheel";
    public final static String LAYER = "layer";
    public final static String SECTOR = "sect";
    public final static String SUBSECTOR = "subsect";
    
    //from FebLocation
    public final static String FEB_LOCATION_ID = "flid";
    public final static String FEB_LOCAL_ETA_PARTITION = "part";
    public final static String I2C_LOCAL_NUMBER = "add";
    
    
    //from i2ccbchannel
    public final static String CB_CHANNEL = "ch";
    
    public FebChipInfoImplAxis(int id, int febLocationId, int barrelOrEndcap, int cbChannel,
    		int diskOrWheel, String febLocalEtaPartition, int i2cLocalNumber,
    	int layer, int position, int sector, String subsector) {
    	setChipId(id);
    	setFebLocationId(febLocationId);
    	setBarrelOrEndcap(barrelOrEndcap);
    	setCbChannel(cbChannel);
    	setDiskOrWheel(diskOrWheel);
    	setFebLocalEtaPartition(febLocalEtaPartition);
    	setI2cLocalNumber(i2cLocalNumber);
    	setLayer(layer);
    	setPosition(position);
    	setSector(sector);
    	setSubsector(subsector);
    }
      
    public FebChipInfoImplAxis(Bag<Object> bag) {
        super(bag);
    }

	public int getChipId() {
		return ((BigInteger) get(ID)).intValue();
	}
	
	void setChipId(int id) {
		put(ID, BigInteger.valueOf(id));
	}

	public int getFebLocationId() {
		return ((BigInteger) get(FEB_LOCATION_ID)).intValue();
	}
	
	void setFebLocationId(int febLocationId) {
		put(FEB_LOCATION_ID, BigInteger.valueOf(febLocationId));
	}

	public int getBarrelOrEndcap() {
		return ((BigInteger) get(BARREL_OR_ENDCAP)).intValue();
	}
	
	void setBarrelOrEndcap(int barrelOrEndcap) {
		put(BARREL_OR_ENDCAP, BigInteger.valueOf(barrelOrEndcap));
	}

	public int getCbChannel() {
		return ((BigInteger) get(CB_CHANNEL)).intValue();
	}
	
	void setCbChannel(int cbChannel) {
		put(CB_CHANNEL, BigInteger.valueOf(cbChannel));
	}

	public int getDiskOrWheel() {
		return ((BigInteger) get(DISK_OR_WHEEL)).intValue();
	}
	
	void setDiskOrWheel(int diskOrWheel) {
		put(DISK_OR_WHEEL, BigInteger.valueOf(diskOrWheel));
	}

	public String getFebLocalEtaPartition() {
		return (String) get(FEB_LOCAL_ETA_PARTITION);
	}
	
	void setFebLocalEtaPartition(String febLocalEtaPartition) {
		put(FEB_LOCAL_ETA_PARTITION, febLocalEtaPartition);
	}

	public int getI2cLocalNumber() {
		return ((BigInteger) get(I2C_LOCAL_NUMBER)).intValue();
	}
	
	void setI2cLocalNumber(int i2cLocalNumber) {
		put(I2C_LOCAL_NUMBER, BigInteger.valueOf(i2cLocalNumber));
	}

	public int getLayer() {
		return ((BigInteger) get(LAYER)).intValue();
	}
	
	void setLayer(int layer) {
		put(LAYER, BigInteger.valueOf(layer));
	}

	public int getPosition() {
		return ((BigInteger) get(POSITION)).intValue();
	}
	
	void setPosition(int position) {
		put(POSITION, BigInteger.valueOf(position));
	}

	public int getSector() {
		return ((BigInteger) get(SECTOR)).intValue();
	}
	
	void setSector(int sector) {
		put(SECTOR, BigInteger.valueOf(sector));
	}

	public String getSubsector() {
		return ((String) get(SUBSECTOR));
	}
	
	void setSubsector(String subsector) {
		put(SUBSECTOR, subsector);
	}
	
	public String toString() {
		String str = "chipId " + getChipId() + " pos " +
		getPosition() + " boe "	+ getBarrelOrEndcap() + " dow "	+ getDiskOrWheel() + 
		" lay "	+ getLayer() + " sec "	+ getSector() + " subsec "	+ getSubsector() + 
		" fli " + getFebLocationId() + " flep "	+ getFebLocalEtaPartition() + " i2cNum "	+ getI2cLocalNumber() + " cbCh "	+ getCbChannel();
		
		return str;
	}

}
