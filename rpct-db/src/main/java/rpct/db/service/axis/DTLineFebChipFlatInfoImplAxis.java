/**
 * Created on 2015-02-24
 * Filip Thyssen
 */

package rpct.db.service.axis;

import java.math.BigInteger;
import java.util.Map;

import rpct.db.service.DTLineFebChipFlatInfo;
import rpct.xdaq.axis.bag.Bag;
import rpct.xdaq.axis.bag.HashBag;

public class DTLineFebChipFlatInfoImplAxis extends HashBag<Object> implements DTLineFebChipFlatInfo {

    public final static String KEY_CHIP_ID = "chipId";
    public final static String KEY_BOARD_ID = "boardId";
    public final static String KEY_NAME = "name";
    public final static String KEY_I2C_ADDRESS = "i2cAddress";
    public final static String KEY_POSITION = "position";
    public final static String KEY_CHIP_DISABLED = "chipDisabled";
    public final static String KEY_BOARD_DISABLED = "boardDisabled";
    public final static String KEY_LOCATION = "location";
    public final static String KEY_PARTITION = "partition";
    // public final static String KEY_FEB_LOCATION_ID = "febLocationId";

    public DTLineFebChipFlatInfoImplAxis() {}
    public DTLineFebChipFlatInfoImplAxis(int chipId, int boardId
                                         , String name
                                         , int i2cAddress, int position
                                         , boolean chipDisabled, boolean boardDisabled
                                         , String location, String partition) {
        // , int febLocationId) {
        setChipId(chipId);
        setBoardId(boardId);
        setName(name);
        setI2cAddress(i2cAddress);
        setPosition(position);
        setChipDisabled(chipDisabled);
        setBoardDisabled(boardDisabled);
        setLocation(location);
        setPartition(partition);
        // setFebLocationId(febLocationId);
    }

    public DTLineFebChipFlatInfoImplAxis(Bag<Object> bag) {
        super(bag);
    }

    public int getChipId() {
        return ((BigInteger) get(KEY_CHIP_ID)).intValue();
    }
    public int getBoardId() {
        return ((BigInteger) get(KEY_BOARD_ID)).intValue();
    }
    public String getName() {
        return (String) get(KEY_NAME);
    }
    public int getI2cAddress() {
        return ((BigInteger) get(KEY_I2C_ADDRESS)).intValue();
    }
    public int getPosition() {
        return ((BigInteger) get(KEY_POSITION)).intValue();
    }
    public boolean getChipDisabled() {
        return (Boolean) get(KEY_CHIP_DISABLED);
    }
    public boolean getBoardDisabled() {
        return (Boolean) get(KEY_BOARD_DISABLED);
    }
    public String getLocation() {
        return (String) get(KEY_LOCATION);
    }
    public String getPartition() {
        return (String) get(KEY_PARTITION);
    }
    // public int getFebLocationId() {
    //     return ((BigInteger) get(KEY_FEB_LOCATION_ID)).intValue();
    // }

    public void setChipId(int chipId) {
        put(KEY_CHIP_ID, BigInteger.valueOf(chipId));
    }
    public void setBoardId(int boardId) {
        put(KEY_BOARD_ID, BigInteger.valueOf(boardId));
    }
    public void setName(String name) {
        put(KEY_NAME, name);
    }
    public void setI2cAddress(int i2cAddress) {
        put(KEY_I2C_ADDRESS, BigInteger.valueOf(i2cAddress));
    }
    public void setPosition(int position) {
        put(KEY_POSITION, BigInteger.valueOf(position));
    }
    public void setChipDisabled(boolean chipDisabled) {
        put(KEY_CHIP_DISABLED, chipDisabled);
    }
    public void setBoardDisabled(boolean boardDisabled) {
        put(KEY_BOARD_DISABLED, boardDisabled);
    }
    public void setLocation(String location) {
        put(KEY_LOCATION, location);
    }
    public void setPartition(String partition) {
        put(KEY_PARTITION, partition);
    }
    // public void setFebLocationId(int febLocationId) {
    //     put(KEY_FEB_LOCATION_ID, BigInteger.valueOf(febLocationId));
    // }

    public String toString() {
        StringBuilder str = new StringBuilder("[");
        for (Map.Entry<String, Object> entry : this.entrySet()) {
            str.append(entry.getKey()).append('=')
                .append(entry.getValue()).append(", ");
        }
        str.append(']');
        return str.toString();
    }
}
