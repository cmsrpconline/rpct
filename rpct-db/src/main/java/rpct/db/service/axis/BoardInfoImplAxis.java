/**
 * Created on 2006-07-15
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
package rpct.db.service.axis;

import java.math.BigInteger;
import java.util.Map;

import rpct.db.service.BoardInfo;
import rpct.db.service.ChipInfo;
import rpct.xdaq.axis.bag.Bag;
import rpct.xdaq.axis.bag.HashBag;

public class BoardInfoImplAxis extends HashBag<Object> implements BoardInfo {

    public final static String KEY_ID = "id";
    public final static String KEY_NAME = "name";
    public final static String KEY_LABEL = "label";
    public final static String KEY_TYPE = "type";
    public final static String KEY_POSITION_OR_ADDRESS = "positionOrAddress";
    public final static String KEY_DISABLED = "disabled";
    public final static String KEY_CHIP_INFOS = "chipInfos";

    public BoardInfoImplAxis(int id, String name, String label, String type, int positionOrAddress,
            boolean disabled, ChipInfoImplAxis[] chipInfos) {
        setId(id);
        setName(name);
        setLabel(label);
        setType(type);
        setPositionOrAddress(positionOrAddress);
        setDisabled(disabled);
        setChipInfos(chipInfos);
    }
    public BoardInfoImplAxis(Bag<Object> bag) {
        super(bag);
    }
    
    public int getId() {
        return ((BigInteger) get(KEY_ID)).intValue();
    }
    public void setId(int id) {
        put(KEY_ID, BigInteger.valueOf(id));
    }

    public String getName() {
        return (String) get(KEY_NAME);
    }
    public void setName(String name) {
        put(KEY_NAME, name);
    }

    public String getLabel() {
        return (String) get(KEY_LABEL);
    }
    public void setLabel(String label) {
        put(KEY_LABEL, label);
    }
    
    public String getType() {
        return (String) get(KEY_TYPE);
    }
    public void setType(String type) {
        put(KEY_TYPE, type);
    }
    
    public int getPositionOrAddress() {
        return ((BigInteger) get(KEY_POSITION_OR_ADDRESS)).intValue();
    }
    public void setPositionOrAddress(int positionOrAddress) {
        put(KEY_POSITION_OR_ADDRESS, BigInteger.valueOf(positionOrAddress));
    }
    
    public boolean isDisabled() {
        return (Boolean) get(KEY_DISABLED);
    }
    public void setDisabled(boolean disabled) {
        put(KEY_DISABLED, disabled);
    }

    public ChipInfo[] getChipInfos() {
        return (ChipInfo[]) get(KEY_CHIP_INFOS);
    }
    public void setChipInfos(ChipInfo[] chipInfos) {
        put(KEY_CHIP_INFOS, chipInfos);
    }
    
    public String toString() {
        StringBuilder str = new StringBuilder("[");
        for (Map.Entry<String, Object> entry : this.entrySet()) {
            str.append(entry.getKey()).append('=')
            .append(entry.getValue()).append(", ");
        }
        str.append(']');
        return str.toString();
    }
}