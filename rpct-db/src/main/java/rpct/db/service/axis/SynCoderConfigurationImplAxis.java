package rpct.db.service.axis;

import java.math.BigInteger;

import org.apache.axis.types.HexBinary;

import rpct.db.service.SynCoderConfiguration;
import rpct.xdaq.axis.Binary;
import rpct.xdaq.axis.bag.HashBag;

public class SynCoderConfigurationImplAxis extends HashBag<Object> implements SynCoderConfiguration {

    public final static String KEY_ID = "id";
    public final static String KEY_WINDOW_OPEN = "windowOpen";
    public final static String KEY_WINDOW_CLOSE = "windowClose";
    public final static String KEY_INVERT_CLOCK = "invertClock";
    public final static String KEY_LMUX_IN_DELAY = "lmuxInDelay";
    public final static String KEY_RBC_DELAY = "rbcDelay";
    public final static String KEY_BCN0_DELAY = "bcn0Delay";
    public final static String KEY_DATA_TRG_DELAY = "dataTrgDelay";
    public final static String KEY_DATA_DAQ_DELAY = "dataDaqDelay";
    public final static String KEY_PULSER_TIMER_TRG_DELAY = "pulserTimerTrgDelay";
    public final static String KEY_IN_CHANNELS_ENA = "inChannelsEna"; 

    public SynCoderConfigurationImplAxis(int id, 
            int windowOpen, 
            int windowClose,
            boolean invertClock,
            int lmuxInDelay,
            int rbcDelay,
            int bcn0Delay,
            int dataTrgDelay,
            int dataDaqDelay,
            int pulserTimerTrgDelay,
            Binary inChannelsEna) {
        setConfigurationId(id);
        setWindowOpen(windowOpen);
        setWindowClose(windowClose);
        setInvertClock(invertClock);
        setLmuxInDelay(lmuxInDelay);
        setRbcDelay(rbcDelay);
        setBcn0Delay(bcn0Delay);
        setDataTrgDelay(dataTrgDelay);
        setDataDaqDelay(dataDaqDelay);
        setPulserTimerTrgDelay(pulserTimerTrgDelay);
        setInChannelsEna(inChannelsEna);
    }
    
    public int getConfigurationId() {
        return ((BigInteger) get(KEY_ID)).intValue();
    }
    public void setConfigurationId(int id) {
        put(KEY_ID, BigInteger.valueOf(id));
    }
    
    public int getBcn0Delay() {
        return ((BigInteger) get(KEY_BCN0_DELAY)).intValue();
    }    
    public void setBcn0Delay(int bcn0Delay) {
        put(KEY_BCN0_DELAY, BigInteger.valueOf(bcn0Delay));
    }

    public int getDataDaqDelay() {
        return ((BigInteger) get(KEY_DATA_DAQ_DELAY)).intValue();
    }  
    public void setDataDaqDelay(int dataDaqDelay) {
        put(KEY_DATA_DAQ_DELAY, BigInteger.valueOf(dataDaqDelay));
    }

    public int getDataTrgDelay() {
        return ((BigInteger) get(KEY_DATA_TRG_DELAY)).intValue();
    }  
    public void setDataTrgDelay(int dataTrgDelay) {
        put(KEY_DATA_TRG_DELAY, BigInteger.valueOf(dataTrgDelay));
    }

    public Binary getInChannelsEna() {
        return new Binary(((HexBinary) get(KEY_IN_CHANNELS_ENA)).getBytes());
    }  
    public void setInChannelsEna(Binary inChannelsEna) {
        //put(KEY_IN_CHANNELS_ENA, inChannelsEna);
        put(KEY_IN_CHANNELS_ENA, new HexBinary(inChannelsEna.getBytes()));
    }

    public int getLmuxInDelay() {
        return ((BigInteger) get(KEY_LMUX_IN_DELAY)).intValue();
    }  
    public void setLmuxInDelay(int lmuxInDelay) {
        put(KEY_LMUX_IN_DELAY, BigInteger.valueOf(lmuxInDelay));
    }

    public int getPulserTimerTrgDelay() {
        return ((BigInteger) get(KEY_PULSER_TIMER_TRG_DELAY)).intValue();
    }  
    public void setPulserTimerTrgDelay(int pulserTimerTrgDelay) {
        put(KEY_PULSER_TIMER_TRG_DELAY, BigInteger.valueOf(pulserTimerTrgDelay));
    }

    public int getRbcDelay() {
        return ((BigInteger) get(KEY_RBC_DELAY)).intValue();
    }  
    public void setRbcDelay(int rbcDelay) {
        put(KEY_RBC_DELAY, BigInteger.valueOf(rbcDelay));
    }

    public int getWindowClose() {
        return ((BigInteger) get(KEY_WINDOW_CLOSE)).intValue();
    }  
    public void setWindowClose(int windowClose) {
        put(KEY_WINDOW_CLOSE, BigInteger.valueOf(windowClose));
    }

    public int getWindowOpen() {
        return ((BigInteger) get(KEY_WINDOW_OPEN)).intValue();
    }  
    public void setWindowOpen(int windowOpen) {
        put(KEY_WINDOW_OPEN, BigInteger.valueOf(windowOpen));
    }

    public boolean isInvertClock() {
        return (Boolean) get(KEY_INVERT_CLOCK);
    }  
    public void setInvertClock(boolean invertClock) {
        put(KEY_INVERT_CLOCK, invertClock);
    }
}
