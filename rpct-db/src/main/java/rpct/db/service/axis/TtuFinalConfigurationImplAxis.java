package rpct.db.service.axis;

import java.math.BigInteger;

import rpct.db.service.TtuFinalConfiguration;
import rpct.xdaq.axis.bag.HashBag;

/**
 * Created on 2009-11-30
 * 
 * @author Nikolay Darmenov
 * @version $Id$
 */
public class TtuFinalConfigurationImplAxis extends HashBag<Object> implements TtuFinalConfiguration {

    public final static String KEY_ID = "id";
    public final static String KEY_TRIG_CONFIG = "trigConfig";
    public final static String KEY_TRIG_CONFIG2 = "trigConfig2";
    public final static String KEY_MASK_TTU = "maskTtu";
    public final static String KEY_MASK_POINTING = "maskPointing";
    public final static String KEY_MASK_BLAST = "maskBlast";
    public final static String KEY_DELAY_TTU = "delayTtu";
    public final static String KEY_DELAY_POINTING = "delayPointing";
    public final static String KEY_DELAY_BLAST = "delayBlast";
    public final static String KEY_SHAPE_TTU = "shapeTtu";
    public final static String KEY_SHAPE_POINTING = "shapePointing";
    public final static String KEY_SHAPE_BLAST = "shapeBlast";

    public TtuFinalConfigurationImplAxis(int id, 
            int[] trigConfig,
            int[] trigConfig2,
            int maskTtu,
            int maskPointing,
            int maskBlast,
            int delayTtu,
            int delayPointing,
            int delayBlast,
            int shapeTtu,
            int shapePointing,
            int shapeBlast) {
        setConfigurationId(id);
        setTrigConfig(trigConfig);
        setTrigConfig2(trigConfig2);
        setMaskTtu(maskTtu);
        setMaskPointing(maskPointing);
        setMaskBlast(maskBlast);
        setDelayTtu(delayTtu);
        setDelayPointing(delayPointing);
        setDelayBlast(delayBlast);
        setShapeTtu(shapeTtu);
        setShapePointing(shapePointing);
        setShapeBlast(shapeBlast);
    }
    
    public int getConfigurationId() {
        return ((BigInteger) get(KEY_ID)).intValue();
    }
    public void setConfigurationId(int id) {
        put(KEY_ID, BigInteger.valueOf(id));
    }    

    public int[] getTrigConfig() {
        BigInteger[] trigConfig = (BigInteger[]) get(KEY_TRIG_CONFIG);
        int[] result = new int[trigConfig.length];
        for (int i = 0; i < trigConfig.length; i++) {
            result[i] = trigConfig[i].intValue();
        }
        return result;
    }  
    public void setTrigConfig(int[] trigConfig) {
        BigInteger[] v = new BigInteger[trigConfig.length];
        for (int i = 0; i < trigConfig.length; i++) {
            v[i] = BigInteger.valueOf(trigConfig[i]);
        }
        
        put(KEY_TRIG_CONFIG, v);
    }

    public int[] getTrigConfig2() {
        BigInteger[] trigConfig2 = (BigInteger[]) get(KEY_TRIG_CONFIG2);
        int[] result = new int[trigConfig2.length];
        for (int i = 0; i < trigConfig2.length; i++) {
            result[i] = trigConfig2[i].intValue();
        }
        return result;
    }  
    public void setTrigConfig2(int[] trigConfig2) {
        BigInteger[] v = new BigInteger[trigConfig2.length];
        for (int i = 0; i < trigConfig2.length; i++) {
            v[i] = BigInteger.valueOf(trigConfig2[i]);
        }
        
        put(KEY_TRIG_CONFIG2, v);
    }

    public int getMaskTtu() {
        return ((BigInteger) get(KEY_MASK_TTU)).intValue();
    }  
    public void setMaskTtu(int maskTtu) {
        put(KEY_MASK_TTU, BigInteger.valueOf(maskTtu));
    }

    public int getMaskPointing() {
        return ((BigInteger) get(KEY_MASK_POINTING)).intValue();
    }  
    public void setMaskPointing(int maskPointing) {
        put(KEY_MASK_POINTING, BigInteger.valueOf(maskPointing));
    }

    public int getMaskBlast() {
        return ((BigInteger) get(KEY_MASK_BLAST)).intValue();
    }  
    public void setMaskBlast(int maskBlast) {
        put(KEY_MASK_BLAST, BigInteger.valueOf(maskBlast));
    }

    public int getDelayTtu() {
        return ((BigInteger) get(KEY_DELAY_TTU)).intValue();
    }  
    public void setDelayTtu(int delayTtu) {
        put(KEY_DELAY_TTU, BigInteger.valueOf(delayTtu));
    }

    public int getDelayPointing() {
        return ((BigInteger) get(KEY_DELAY_POINTING)).intValue();
    }  
    public void setDelayPointing(int delayPointing) {
        put(KEY_DELAY_POINTING, BigInteger.valueOf(delayPointing));
    }

    public int getDelayBlast() {
        return ((BigInteger) get(KEY_DELAY_BLAST)).intValue();
    }  
    public void setDelayBlast(int delayBlast) {
        put(KEY_DELAY_BLAST, BigInteger.valueOf(delayBlast));
    }

    public int getShapeTtu() {
        return ((BigInteger) get(KEY_SHAPE_TTU)).intValue();
    }  
    public void setShapeTtu(int shapeTtu) {
        put(KEY_SHAPE_TTU, BigInteger.valueOf(shapeTtu));
    }

    public int getShapePointing() {
        return ((BigInteger) get(KEY_SHAPE_POINTING)).intValue();
    }  
    public void setShapePointing(int shapePointing) {
        put(KEY_SHAPE_POINTING, BigInteger.valueOf(shapePointing));
    }

    public int getShapeBlast() {
        return ((BigInteger) get(KEY_SHAPE_BLAST)).intValue();
    }  
    public void setShapeBlast(int shapeBlast) {
        put(KEY_SHAPE_BLAST, BigInteger.valueOf(shapeBlast));
    }
}
