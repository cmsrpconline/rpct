/**
 * Created on 2006-07-15
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
package rpct.db.service.axis;

import java.math.BigInteger;
import java.util.Map;

import rpct.db.service.BoardInfo;
import rpct.db.service.LinkBoxInfo;
import rpct.xdaq.axis.bag.Bag;
import rpct.xdaq.axis.bag.HashBag;

public class LinkBoxInfoImplAxis extends HashBag<Object> implements LinkBoxInfo {

    public final static String KEY_ID = "id";
    public final static String KEY_CCU10_ADDRESS = "ccu10Address";
    public final static String KEY_CCU20_ADDRESS = "ccu20Address";
    public final static String KEY_NAME = "name";
    public final static String KEY_LABEL = "label";
    public final static String KEY_DISABLED = "disabled";
    public final static String KEY_BOARD_INFOS = "boardInfos";

    public LinkBoxInfoImplAxis(int id, int ccu10Address, int ccu20Address, 
            String name, String label, boolean disabled, BoardInfoImplAxis[] boardInfos) {
        setId(id);
        setCcu10Address(ccu10Address);
        setCcu20Address(ccu20Address);
        setName(name);
        setLabel(label);
        setDisabled(disabled);
        setBoardInfos(boardInfos);
    }
    public LinkBoxInfoImplAxis(Bag<Object> bag) {
        super(bag);
    }
    
    public int getId() {
        return ((BigInteger) get(KEY_ID)).intValue();
    }
    public void setId(int id) {
        put(KEY_ID, BigInteger.valueOf(id));
    }

    public String getName() {
        return (String) get(KEY_NAME);
    }
    public void setName(String name) {
        put(KEY_NAME, name);
    }

    public String getLabel() {
        return (String) get(KEY_LABEL);
    }
    public void setLabel(String label) {
        put(KEY_LABEL, label);
    }
    
    public int getCcu10Address() {
        return ((BigInteger) get(KEY_CCU10_ADDRESS)).intValue();
    }
    public void setCcu10Address(int ccu10Address) {
        put(KEY_CCU10_ADDRESS, BigInteger.valueOf(ccu10Address));
    }
    
    public int getCcu20Address() {
        return ((BigInteger) get(KEY_CCU20_ADDRESS)).intValue();
    }
    public void setCcu20Address(int ccu20Address) {
        put(KEY_CCU20_ADDRESS, BigInteger.valueOf(ccu20Address));
    }
    
    public boolean isDisabled() {
        return (Boolean) get(KEY_DISABLED);
    }
    public void setDisabled(boolean disabled) {
        put(KEY_DISABLED, disabled);
    }

    public BoardInfo[] getBoardInfos() {
        return (BoardInfo[]) get(KEY_BOARD_INFOS);
    }
    public void setBoardInfos(BoardInfo[] boardInfos) {
        put(KEY_BOARD_INFOS, boardInfos);
    }
    
    public String toString() {
        StringBuilder str = new StringBuilder("[");
        for (Map.Entry<String, Object> entry : this.entrySet()) {
            str.append(entry.getKey()).append('=')
            .append(entry.getValue()).append(", ");
        }
        str.append(']');
        return str.toString();
    }
}