package rpct.db.service.axis;

import java.math.BigInteger;

import com.sun.xml.bind.v2.runtime.RuntimeUtil.ToStringAdapter;

import rpct.db.service.ChipConfigurationAssignment;
import rpct.db.service.ConfigurationSet;
import rpct.db.service.FebChipConfiguration;
import rpct.db.service.HalfSortConfiguration;
import rpct.db.service.OptoConfiguration;
import rpct.db.service.PacConfiguration;
import rpct.db.service.RbcConfiguration;
import rpct.db.service.RmbConfiguration;
import rpct.db.service.SynCoderConfInFlashInfo;
import rpct.db.service.SynCoderConfiguration;
import rpct.db.service.TbGbSortConfiguration;
import rpct.db.service.TcSortConfiguration;
import rpct.db.service.TtuFinalConfiguration;
import rpct.db.service.TtuTrigConfiguration;
import rpct.xdaq.axis.bag.Bag;
import rpct.xdaq.axis.bag.HashBag;

public class ConfigurationSetImplAxis extends HashBag<Object> implements ConfigurationSet {

    public final static String KEY_LOCAL_CONFIG_KEY_ID = "localConfigKeyId";
    public final static String KEY_TIMESTAMP = "timestamp";    
    public final static String KEY_CHIP_CONFIGURATION_ASSIGNMENTS = "chipConfigurationAssignments";
    public final static String KEY_SYNCODER_CONFIGURATIONS = "synCoderConfigurations";
    public final static String KEY_OPTO_CONFIGURATIONS = "optoConfigurations";
    public final static String KEY_PAC_CONFIGURATIONS = "pacConfigurations";
    public final static String KEY_TBGBSORT_CONFIGURATIONS = "tbGbSortConfigurations";
    public final static String KEY_RMB_CONFIGURATIONS = "rmbConfigurations";
    public final static String KEY_TCSORT_CONFIGURATIONS = "tcSortConfigurations";
    public final static String KEY_HALFSORT_CONFIGURATIONS = "halfSortConfigurations";
    public final static String KEY_RBC_CONFIGURATIONS = "rbcConfigurations";
    public final static String KEY_TTUTRIG_CONFIGURATIONS = "ttuTrigConfigurations";
    public final static String KEY_TTUFINAL_CONFIGURATIONS = "ttuFinalConfigurations";
    public final static String KEY_FEBCHIP_CONFIGURATIONS = "febChipConfigurations";
    public final static String KEY_SYNCODERCONFINFLASHES = "synCoderConfInFlashes";
    
    public ConfigurationSetImplAxis(
            int localConfigKeyId,
            int timestamp,
            ChipConfigurationAssignment[] chipConfigurationAssignments,
            SynCoderConfiguration[] synCoderConfigurations,
            OptoConfiguration[] optoConfigurations,
            PacConfiguration[] pacConfigurations,
            TbGbSortConfiguration[] tbGbSortConfigurations,
            RmbConfiguration[] rmbConfigurations,
            TcSortConfiguration[] tcSortConfigurations,
            HalfSortConfiguration[] halfSortConfigurations,
            RbcConfiguration[] rbcConfigurations,
            TtuTrigConfiguration[] ttuTrigConfigurations,
            TtuFinalConfiguration[] ttuFinalConfigurations,
            FebChipConfiguration[] febChipConfigurations,
            SynCoderConfInFlashInfo[] synCoderConfInFlashInfos) {
        setLocalConfigKeyId(localConfigKeyId);
        setTimestamp(timestamp);
        setChipConfigurationAssignments(chipConfigurationAssignments);
        setSynCoderConfigurations(synCoderConfigurations); 
        setOptoConfigurations(optoConfigurations);
        setPacConfigurations(pacConfigurations);
        setTbGbSortConfigurations(tbGbSortConfigurations);
        setRmbConfigurations(rmbConfigurations);
        setTcSortConfigurations(tcSortConfigurations);
        setHalfSortConfigurations(halfSortConfigurations);
        setRbcConfigurations(rbcConfigurations);
        setTtuTrigConfigurations(ttuTrigConfigurations);
        setTtuFinalConfigurations(ttuFinalConfigurations);
        setFebChipConfigurations(febChipConfigurations);
        setSynCoderConfInFlashes(synCoderConfInFlashInfos);
    }
    
    public ConfigurationSetImplAxis(Bag<Object> bag) {
        super(bag);
    }
    
    public int getLocalConfigKeyId() {
        return ((BigInteger) get(KEY_LOCAL_CONFIG_KEY_ID)).intValue();
    }
    public void setLocalConfigKeyId(int localConfigKeyId) {
        put(KEY_LOCAL_CONFIG_KEY_ID, BigInteger.valueOf(localConfigKeyId));
    }
    
    public int getTimestamp() {
        return ((BigInteger) get(KEY_TIMESTAMP)).intValue();
    }
    public void setTimestamp(int timestamp) {
        put(KEY_TIMESTAMP, BigInteger.valueOf(timestamp));
    }
    
    public ChipConfigurationAssignment[] getChipConfigurationAssignments() {
        return (ChipConfigurationAssignment[]) get(KEY_CHIP_CONFIGURATION_ASSIGNMENTS);
    }
    public void setChipConfigurationAssignments(ChipConfigurationAssignment[] chipConfigurationAssignments) {
        put(KEY_CHIP_CONFIGURATION_ASSIGNMENTS, chipConfigurationAssignments);
    }

    public SynCoderConfiguration[] getSynCoderConfigurations() {
        return (SynCoderConfiguration[]) get(KEY_SYNCODER_CONFIGURATIONS);
    }
    public void setSynCoderConfigurations(SynCoderConfiguration[] synCoderConfigurations) {
        put(KEY_SYNCODER_CONFIGURATIONS, synCoderConfigurations);
    }

    public OptoConfiguration[] getOptoConfigurations() {
        return (OptoConfiguration[]) get(KEY_OPTO_CONFIGURATIONS);
    }
    public void setOptoConfigurations(OptoConfiguration[] optoConfigurations) {
        put(KEY_OPTO_CONFIGURATIONS, optoConfigurations);
    }

    public PacConfiguration[] getPacConfigurations() {
        return (PacConfiguration[]) get(KEY_PAC_CONFIGURATIONS);
    }
    public void setPacConfigurations(PacConfiguration[] pacConfigurations) {
        put(KEY_PAC_CONFIGURATIONS, pacConfigurations);
    }

    public TbGbSortConfiguration[] getTbGbSortConfigurations() {
        return (TbGbSortConfiguration[]) get(KEY_TBGBSORT_CONFIGURATIONS);
    }
    public void setTbGbSortConfigurations(TbGbSortConfiguration[] tbGbSortConfigurations) {
        put(KEY_TBGBSORT_CONFIGURATIONS, tbGbSortConfigurations);
    }

    public RmbConfiguration[] getRmbConfigurations() {
        return (RmbConfiguration[]) get(KEY_RMB_CONFIGURATIONS);
    }
    public void setRmbConfigurations(RmbConfiguration[] rmbConfigurations) {
        put(KEY_RMB_CONFIGURATIONS, rmbConfigurations);
    }

    public TcSortConfiguration[] getTcSortConfigurations() {
        return (TcSortConfiguration[]) get(KEY_TCSORT_CONFIGURATIONS);
    }
    public void setTcSortConfigurations(TcSortConfiguration[] tcSortConfigurations) {
        put(KEY_TCSORT_CONFIGURATIONS, tcSortConfigurations);
    }

    public HalfSortConfiguration[] getHalfSortConfigurations() {
        return (HalfSortConfiguration[]) get(KEY_HALFSORT_CONFIGURATIONS);
    }
    public void setHalfSortConfigurations(HalfSortConfiguration[] rmbConfigurations) {
        put(KEY_HALFSORT_CONFIGURATIONS, rmbConfigurations);
    }

    public RbcConfiguration[] getRbcConfigurations() {
        return (RbcConfiguration[]) get(KEY_RBC_CONFIGURATIONS);
    }
    public void setRbcConfigurations(RbcConfiguration[] configurations) {
        put(KEY_RBC_CONFIGURATIONS, configurations);
    }

    public TtuTrigConfiguration[] getTtuTrigConfigurations() {
        return (TtuTrigConfiguration[]) get(KEY_TTUTRIG_CONFIGURATIONS);
    }
    public void setTtuTrigConfigurations(TtuTrigConfiguration[] configurations) {
        put(KEY_TTUTRIG_CONFIGURATIONS, configurations);
    }

    public TtuFinalConfiguration[] getTtuFinalConfigurations() {
        return (TtuFinalConfiguration[]) get(KEY_TTUFINAL_CONFIGURATIONS);
    }
    public void setTtuFinalConfigurations(TtuFinalConfiguration[] configurations) {
        put(KEY_TTUFINAL_CONFIGURATIONS, configurations);
    }

    public FebChipConfiguration[] getFebChipConfigurations() {
        return (FebChipConfiguration[]) get(KEY_FEBCHIP_CONFIGURATIONS);
    }
    public void setFebChipConfigurations(FebChipConfiguration[] configurations) {
        put(KEY_FEBCHIP_CONFIGURATIONS, configurations);
    }
    
    public SynCoderConfInFlashInfo[] getSynCoderConfInFlashes() {
        return (SynCoderConfInFlashInfo[]) get(KEY_SYNCODERCONFINFLASHES);
    }
    public void setSynCoderConfInFlashes(SynCoderConfInFlashInfo[] confInFlashInfos) {
        put(KEY_SYNCODERCONFINFLASHES, confInFlashInfos);
    }
    
    public String toString() {
    	String str = "";
    	for(SynCoderConfInFlashInfo synCoderConfInFlashInfo : getSynCoderConfInFlashes()) {
    		str += "\n" + synCoderConfInFlashInfo.toString();
    	}
    	
    	for(TtuTrigConfiguration configuration : getTtuTrigConfigurations()) {
    		str += "\n" + configuration.toString();
    	}
    	return str;
    }
    
}
