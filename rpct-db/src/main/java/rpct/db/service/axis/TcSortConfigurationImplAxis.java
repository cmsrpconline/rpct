package rpct.db.service.axis;

import java.math.BigInteger;

import org.apache.axis.types.HexBinary;

import rpct.db.service.TcSortConfiguration;
import rpct.xdaq.axis.Binary;
import rpct.xdaq.axis.bag.HashBag;

public class TcSortConfigurationImplAxis extends HashBag<Object> implements TcSortConfiguration {

    public final static String KEY_ID = "id";
    public final static String KEY_REC_MUX_CLK_INV = "recMuxClkInv";
    public final static String KEY_REC_MUX_CLK_90 = "recMuxClk90";
    public final static String KEY_REC_MUX_REG_ADD = "recMuxRegAdd";
    public final static String KEY_REC_MUX_DELAY = "recMuxDelay";
    public final static String KEY_REC_DATA_DELAY = "recDataDelay";
    public final static String KEY_BOOT_SCRIPT = "bootScript";

    public TcSortConfigurationImplAxis(int id, 
            Binary recMuxClkInv, 
            Binary recMuxClk90, 
            Binary recMuxRegAdd,
            int[] recMuxDelay,
            int[] recDataDelay,
            String bootScript) {
        setConfigurationId(id);
        setRecMuxClkInv(recMuxClkInv);
        setRecMuxClk90(recMuxClk90);
        setRecMuxRegAdd(recMuxRegAdd);
        setRecMuxDelay(recMuxDelay);
        setRecDataDelay(recDataDelay);
        setBootScript(bootScript);
    }
    
    public int getConfigurationId() {
        return ((BigInteger) get(KEY_ID)).intValue();
    }
    public void setConfigurationId(int id) {
        put(KEY_ID, BigInteger.valueOf(id));
    }    

    public Binary getRecMuxClkInv() {
        return new Binary(((HexBinary) get(KEY_REC_MUX_CLK_INV)).getBytes());
    }  
    public void setRecMuxClkInv(Binary recMuxClkInv) {
        put(KEY_REC_MUX_CLK_INV, new HexBinary(recMuxClkInv.getBytes()));
    }    

    public Binary getRecMuxClk90() {
        return new Binary(((HexBinary) get(KEY_REC_MUX_CLK_90)).getBytes());
    }  
    public void setRecMuxClk90(Binary recMuxClk90) {
        put(KEY_REC_MUX_CLK_90, new HexBinary(recMuxClk90.getBytes()));
    }

    public Binary getRecMuxRegAdd() {
        return new Binary(((HexBinary) get(KEY_REC_MUX_REG_ADD)).getBytes());
    }  
    public void setRecMuxRegAdd(Binary recMuxRegAdd) {
        put(KEY_REC_MUX_REG_ADD, new HexBinary(recMuxRegAdd.getBytes()));
    }

    public int[] getRecMuxDelay() {
        BigInteger[] recMuxDelay = (BigInteger[]) get(KEY_REC_MUX_DELAY);
        int[] result = new int[recMuxDelay.length];
        for (int i = 0; i < recMuxDelay.length; i++) {
            result[i] = recMuxDelay[i].intValue();
        }
        return result;
    }  
    public void setRecMuxDelay(int[] recMuxDelay) {
        BigInteger[] rmd = new BigInteger[recMuxDelay.length];
        for (int i = 0; i < recMuxDelay.length; i++) {
            rmd[i] = BigInteger.valueOf(recMuxDelay[i]);
        }
        
        put(KEY_REC_MUX_DELAY, rmd);
    }

    public int[] getRecDataDelay() {
        BigInteger[] recDataDelay = (BigInteger[]) get(KEY_REC_DATA_DELAY);
        int[] result = new int[recDataDelay.length];
        for (int i = 0; i < recDataDelay.length; i++) {
            result[i] = recDataDelay[i].intValue();
        }
        return result;
    }  
    public void setRecDataDelay(int[] recDataDelay) {
        BigInteger[] rdd = new BigInteger[recDataDelay.length];
        for (int i = 0; i < recDataDelay.length; i++) {
            rdd[i] = BigInteger.valueOf(recDataDelay[i]);
        }
        
        put(KEY_REC_DATA_DELAY, rdd);
    }

    public String getBootScript() {
        return (String) get(KEY_BOOT_SCRIPT);
    }  
    public void setBootScript(String bootScript) {
        put(KEY_BOOT_SCRIPT, bootScript == null ? "" : bootScript);
    }
}
