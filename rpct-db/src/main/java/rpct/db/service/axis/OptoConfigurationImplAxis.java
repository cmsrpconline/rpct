package rpct.db.service.axis;

import java.math.BigInteger;

import rpct.db.service.OptoConfiguration;
import rpct.xdaq.axis.bag.HashBag;

public class OptoConfigurationImplAxis extends HashBag<Object> implements OptoConfiguration {

    public final static String KEY_ID = "id";
    public final static String KEY_ENABLE_LINK_0 = "enableLink0";
    public final static String KEY_ENABLE_LINK_1 = "enableLink1";
    public final static String KEY_ENABLE_LINK_2 = "enableLink2";

    public OptoConfigurationImplAxis(int id, 
            boolean enableLink0, 
            boolean enableLink1, 
            boolean enableLink2) {
        setConfigurationId(id);
        setEnableLink0(enableLink0);
        setEnableLink1(enableLink1);
        setEnableLink2(enableLink2);
    }
    
    public int getConfigurationId() {
        return ((BigInteger) get(KEY_ID)).intValue();
    }
    public void setConfigurationId(int id) {
        put(KEY_ID, BigInteger.valueOf(id));
    }

    public boolean isEnableLink0() {
        return (Boolean) get(KEY_ENABLE_LINK_0);
    }
    public void setEnableLink0(boolean enable) {
        put(KEY_ENABLE_LINK_0, enable);
    }

    public boolean isEnableLink1() {
        return (Boolean) get(KEY_ENABLE_LINK_1);
    }
    public void setEnableLink1(boolean enable) {
        put(KEY_ENABLE_LINK_1, enable);
    }

    public boolean isEnableLink2() {
        return (Boolean) get(KEY_ENABLE_LINK_2);
    }
    public void setEnableLink2(boolean enable) {
        put(KEY_ENABLE_LINK_2, enable);
    }
}
