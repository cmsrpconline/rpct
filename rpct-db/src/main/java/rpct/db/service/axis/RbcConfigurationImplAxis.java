package rpct.db.service.axis;

import java.math.BigInteger;

import rpct.db.service.RbcConfiguration;
import rpct.xdaq.axis.bag.HashBag;

/**
 * Created on 2009-11-30
 * 
 * @author Nikolay Darmenov
 * @version $Id$
 */
public class RbcConfigurationImplAxis extends HashBag<Object> implements RbcConfiguration {

    public final static String KEY_ID = "id";
    public final static String KEY_CONFIG ="config";
    public final static String KEY_CONFIG_IN ="configIn";
    public final static String KEY_CONFIG_VER ="configVer";
    public final static String KEY_MAJORITY ="majority";
    public final static String KEY_SHAPE ="shape";
    public final static String KEY_MASK ="mask";
    public final static String KEY_CTRL ="ctrl";

    public RbcConfigurationImplAxis(int id, 
    		int config,
    		int configIn,
    		int configVer,
    		int majority,
    		int shape,
    		int mask,
    		int ctrl) {
        setConfigurationId(id);
        setConfig(config);
        setConfigIn(configIn);
        setConfigVer(configVer);
        setMajority(majority);
        setShape(shape);
        setMask(mask);
        setCtrl(ctrl);
    }
    
    public int getConfigurationId() {
        return ((BigInteger) get(KEY_ID)).intValue();
    }
    public void setConfigurationId(int id) {
        put(KEY_ID, BigInteger.valueOf(id));
    }    


    public int getConfig() {
        return ((BigInteger) get(KEY_CONFIG)).intValue();
    }  
    public void setConfig(int config) {
        put(KEY_CONFIG, BigInteger.valueOf(config));
    }
    
    public int getConfigIn() {
        return ((BigInteger) get(KEY_CONFIG_IN)).intValue();
    }  
    public void setConfigIn(int configIn) {
        put(KEY_CONFIG_IN, BigInteger.valueOf(configIn));
    }
    
    public int getConfigVer() {
        return ((BigInteger) get(KEY_CONFIG_VER)).intValue();
    }  
    public void setConfigVer(int configVer) {
        put(KEY_CONFIG_VER, BigInteger.valueOf(configVer));
    }
    
    public int getMajority() {
        return ((BigInteger) get(KEY_MAJORITY)).intValue();
    }  
    public void setMajority(int majority) {
        put(KEY_MAJORITY, BigInteger.valueOf(majority));
    }
    
    public int getShape() {
        return ((BigInteger) get(KEY_SHAPE)).intValue();
    }  
    public void setShape(int shape) {
        put(KEY_SHAPE, BigInteger.valueOf(shape));
    }

    public int getMask() {
        return ((BigInteger) get(KEY_MASK)).intValue();
    }  
    public void setMask(int mask) {
        put(KEY_MASK, BigInteger.valueOf(mask));
    }

    public int getCtrl() {
        return ((BigInteger) get(KEY_CTRL)).intValue();
    }  
    public void setCtrl(int ctrl) {
        put(KEY_CTRL, BigInteger.valueOf(ctrl));
    }
}
