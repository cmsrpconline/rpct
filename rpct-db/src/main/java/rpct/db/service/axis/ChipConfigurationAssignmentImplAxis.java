package rpct.db.service.axis;

import java.math.BigInteger;

import rpct.db.service.ChipConfigurationAssignment;
import rpct.xdaq.axis.bag.HashBag;

public class ChipConfigurationAssignmentImplAxis extends HashBag<Object> implements
        ChipConfigurationAssignment {
    
    public final static String KEY_CHIP_ID = "chipId";
    public final static String KEY_CONFIGURATION_ID = "configurationId";

    public ChipConfigurationAssignmentImplAxis(int chipId, int configurationId) {
        setChipId(chipId);
        setConfigurationId(configurationId);
    }
    
    public int getChipId() {
        return ((BigInteger) get(KEY_CHIP_ID)).intValue();
    }   
    public void setChipId(int chipId) {
        put(KEY_CHIP_ID, BigInteger.valueOf(chipId));
    }

    public int getConfigurationId() {
        return ((BigInteger) get(KEY_CONFIGURATION_ID)).intValue();
    } 
    public void setConfigurationId(int configurationId) {
        put(KEY_CONFIGURATION_ID, BigInteger.valueOf(configurationId));
    }
}
