/**
 * Created on 2015-02-24
 * Filip Thyssen
 */

package rpct.db.service.axis;

import java.math.BigInteger;
import java.util.Map;

import rpct.db.service.DTLineFebInfo;
import rpct.db.service.DTLineFebChipInfo;
import rpct.xdaq.axis.bag.Bag;
import rpct.xdaq.axis.bag.HashBag;

public class DTLineFebInfoImplAxis extends HashBag<Object> implements DTLineFebInfo {

    public final static String KEY_ID = "id";
    public final static String KEY_NAME = "name";
    public final static String KEY_I2C_ADDRESS = "i2cAddress";
    public final static String KEY_DISABLED = "disabled";
    public final static String KEY_LOCATION = "location";
    public final static String KEY_PARTITION = "partition";
    // public final static String KEY_FEB_LOCATION_ID = "febLocationId";
    public final static String KEY_FEB_CHIP_INFOS = "febChipInfos";

    public DTLineFebInfoImplAxis() {}
    public DTLineFebInfoImplAxis(int id
                                 , String name
                                 , int i2cAddress
                                 , boolean disabled
                                 , String location, String partition
                                 // , int febLocationId
                                 , DTLineFebChipInfoImplAxis[] febChipInfos) {
        setId(id);
        setName(name);
        setI2cAddress(i2cAddress);
        setDisabled(disabled);
        setLocation(location);
        setPartition(partition);
        // setFebLocationId(febLocationId);
        setFebChipInfos(febChipInfos);
    }

    public DTLineFebInfoImplAxis(Bag<Object> bag) {
        super(bag);
    }

    public int getId() {
        return ((BigInteger) get(KEY_ID)).intValue();
    }
    public String getName() {
        return (String) get(KEY_NAME);
    }
    public int getI2cAddress() {
        return ((BigInteger) get(KEY_I2C_ADDRESS)).intValue();
    }
    public boolean getDisabled() {
        return (Boolean) get(KEY_DISABLED);
    }
    public String getLocation() {
        return (String) get(KEY_LOCATION);
    }
    public String getPartition() {
        return (String) get(KEY_PARTITION);
    }
    // public int getFebLocationId() {
    //     return ((BigInteger) get(KEY_FEB_LOCATION_ID)).intValue();
    // }
    public DTLineFebChipInfo[] getFebChipInfos() {
        return (DTLineFebChipInfo[]) get(KEY_FEB_CHIP_INFOS);
    }

    public void setId(int id) {
        put(KEY_ID, BigInteger.valueOf(id));
    }
    public void setName(String name) {
        put(KEY_NAME, name);
    }
    public void setI2cAddress(int i2cAddress) {
        put(KEY_I2C_ADDRESS, BigInteger.valueOf(i2cAddress));
    }
    public void setDisabled(boolean disabled) {
        put(KEY_DISABLED, disabled);
    }
    public void setLocation(String location) {
        put(KEY_LOCATION, location);
    }
    public void setPartition(String partition) {
        put(KEY_PARTITION, partition);
    }
    // public void setFebLocationId(int febLocationId) {
    //     put(KEY_FEB_LOCATION_ID, BigInteger.valueOf(febLocationId));
    // }
    public void setFebChipInfos(DTLineFebChipInfo[] febChipInfos) {
        put(KEY_FEB_CHIP_INFOS, febChipInfos);
    }

    public String toString() {
        StringBuilder str = new StringBuilder("[");
        for (Map.Entry<String, Object> entry : this.entrySet()) {
            str.append(entry.getKey()).append('=')
                .append(entry.getValue()).append(", ");
        }
        str.append(']');
        return str.toString();
    }
}
