/**
 * Created on 2015-02-24
 * Filip Thyssen
 */

package rpct.db.service.axis;

import java.math.BigInteger;
import java.util.Map;

import rpct.db.service.DTLineFebChipInfo;
import rpct.xdaq.axis.bag.Bag;
import rpct.xdaq.axis.bag.HashBag;

public class DTLineFebChipInfoImplAxis extends HashBag<Object> implements DTLineFebChipInfo {

    public final static String KEY_ID = "id";
    public final static String KEY_POSITION = "position";
    public final static String KEY_DISABLED = "disabled";

    public DTLineFebChipInfoImplAxis() {}
    public DTLineFebChipInfoImplAxis(int id
                                     , int position
                                     , boolean disabled) {
        setId(id);
        setPosition(position);
        setDisabled(disabled);
    }

    public DTLineFebChipInfoImplAxis(Bag<Object> bag) {
        super(bag);
    }

    public int getId() {
        return ((BigInteger) get(KEY_ID)).intValue();
    }
    public int getPosition() {
        return ((BigInteger) get(KEY_POSITION)).intValue();
    }
    public boolean getDisabled() {
        return (Boolean) get(KEY_DISABLED);
    }

    public void setId(int id) {
        put(KEY_ID, BigInteger.valueOf(id));
    }
    public void setPosition(int position) {
        put(KEY_POSITION, BigInteger.valueOf(position));
    }
    public void setDisabled(boolean disabled) {
        put(KEY_DISABLED, disabled);
    }

    public String toString() {
        StringBuilder str = new StringBuilder("[");
        for (Map.Entry<String, Object> entry : this.entrySet()) {
            str.append(entry.getKey()).append('=')
                .append(entry.getValue()).append(", ");
        }
        str.append(']');
        return str.toString();
    }
}
