/**
 * Created on 2006-01-16
 *
 * @author Michal Pietrusinski & William Whitaker
 * @version $Id$
 */
package rpct.db.service.axis;

import java.math.BigInteger;
import java.util.Map;

import rpct.db.domain.configuration.XdaqApplication;
import rpct.db.service.RbcInfo;
import rpct.xdaq.axis.bag.Bag;
import rpct.xdaq.axis.bag.HashBag;

public class RbcInfoImplAxis extends HashBag<Object> implements RbcInfo {

    public final static String KEY_XDAQ_URL = "xdaqUrl";
    public final static String KEY_XDAQ_APP_INSTANCE = "xdaqAppInstance";
    public final static String KEY_LINK_BOX_NAME = "linkBoxName";

    public RbcInfoImplAxis(String xdaqUrl, int xdaqAppInstance, String linkBoxName) {
        setXdaqUrl(xdaqUrl);
        setXdaqAppInstance(xdaqAppInstance);
        setLinkBoxName(linkBoxName);
    }
    public RbcInfoImplAxis(String host, int port, int xdaqAppInstance, String linkBoxName) {
        this("http://" + host + ':' + port, xdaqAppInstance, linkBoxName);
    }
    public RbcInfoImplAxis(Bag<Object> bag) {
        super(bag);
    }
    public String getXdaqUrl() {
        return (String) get(KEY_XDAQ_URL);
    }
    public void setXdaqUrl(String xdaqUrl) {
        put(KEY_XDAQ_URL, xdaqUrl);
    }
    public int getXdaqAppInstance() {
        return ((BigInteger) get(KEY_XDAQ_APP_INSTANCE)).intValue();
    }
    public void setXdaqAppInstance(int xdaqAppInstance) {
        put(KEY_XDAQ_APP_INSTANCE, BigInteger.valueOf(xdaqAppInstance));
    }
    public String getLinkBoxName() {
        return (String) get(KEY_LINK_BOX_NAME);
    }
    public void setLinkBoxName(String linkBoxName) {
        put(KEY_LINK_BOX_NAME, linkBoxName);
    }
    public String toString() {
        StringBuilder str = new StringBuilder("[");
        for (Map.Entry<String, Object> entry : this.entrySet()) {
            str.append(entry.getKey()).append('=')
            .append(entry.getValue()).append(", ");
        }
        str.append(']');
        return str.toString();
    }
}