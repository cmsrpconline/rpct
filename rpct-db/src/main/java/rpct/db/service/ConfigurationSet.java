package rpct.db.service;

public interface ConfigurationSet {    
    SynCoderConfiguration[] getSynCoderConfigurations();
    OptoConfiguration[] getOptoConfigurations();
    PacConfiguration[] getPacConfigurations();
    TbGbSortConfiguration[] getTbGbSortConfigurations();
    RmbConfiguration[] getRmbConfigurations();
    TcSortConfiguration[] getTcSortConfigurations();
    RbcConfiguration[] getRbcConfigurations();
    TtuTrigConfiguration[] getTtuTrigConfigurations();
    TtuFinalConfiguration[] getTtuFinalConfigurations();
    FebChipConfiguration[] getFebChipConfigurations();    
    //ChipStatus[] getChipStatuses(); TODO
    SynCoderConfInFlashInfo[] getSynCoderConfInFlashes();
    
    ChipConfigurationAssignment[] getChipConfigurationAssignments();
}
