package rpct.db.service;

public interface ChipConfigurationAssignment {
    int getConfigurationId();
    int getChipId();
}
