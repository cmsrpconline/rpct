package rpct.db.service;

public interface CcuRingInfo {
    int getPciSlot();
    int getCcsPosition();
    int getFecPosition();
    LinkBoxInfo[] getLinkBoxInfos();
    FebBoardInfo[] getFebBoardInfos();
}
