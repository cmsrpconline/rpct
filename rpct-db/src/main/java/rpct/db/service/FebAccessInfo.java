/**
 * Created on 2006-01-16
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */

package rpct.db.service;

public interface FebAccessInfo {
	public abstract int getFebId();
	public abstract String getXdaqUrl();
	public abstract int getXdaqAppInstance();
	public abstract String getFebLocalEtaPartition();
	public abstract String getChamberLocationName();
	public abstract int getCcuAddress();
	public abstract int getI2cChannel();
	public abstract int getFebAddress();
	public abstract int getPosInLocalEtaPartition();
}