package rpct.db.service;

/**
 * Created on 2012-02-01
 *
 * @author Karol Bunkowski
 * @version $Id: $
 */
public interface SynCoderConfInFlashInfo {
	public int getChipId();
	public int getSynCoderConfId();
	public int getBeginAddress();
	public int getEndAddress();
}
