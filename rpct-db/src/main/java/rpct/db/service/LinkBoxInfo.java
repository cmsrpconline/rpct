package rpct.db.service;

public interface LinkBoxInfo {
    int getId();
    int getCcu10Address();
    int getCcu20Address();
    String getName();
    String getLabel();
    boolean isDisabled();
    BoardInfo[] getBoardInfos();
}
