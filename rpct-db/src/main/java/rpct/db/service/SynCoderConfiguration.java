package rpct.db.service;

import rpct.xdaq.axis.Binary;

public interface SynCoderConfiguration extends ChipConfiguration {

    int getWindowOpen();
    int getWindowClose();
    boolean isInvertClock();
    int getLmuxInDelay();
    int getRbcDelay();
    int getBcn0Delay();
    int getDataTrgDelay();
    int getDataDaqDelay();
    int getPulserTimerTrgDelay();
    Binary getInChannelsEna();
}
