package rpct.db.service;

/**
 * Created on 2009-11-30
 * 
 * @author Nikolay Darmenov
 * @version $Id$
 */
public interface RbcConfiguration extends ChipConfiguration {
	int getConfig();
	int getConfigIn();
	int getConfigVer();
	int getMajority();
	int getShape();
	int getMask();
	int getCtrl();
}
