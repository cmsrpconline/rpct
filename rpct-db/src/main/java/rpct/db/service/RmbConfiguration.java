package rpct.db.service;

import rpct.xdaq.axis.Binary;


public interface RmbConfiguration extends ChipConfiguration {
    Binary getRecMuxClkInv();
    Binary getRecMuxClk90();
    Binary getRecMuxRegAdd();
    int[] getRecMuxDelay();
    int[] getRecDataDelay();
    int getChanEna();
    int getPreTriggerVal(); 
    int getPostTriggerVal(); 
    int getDataDelay(); 
    int getTrgDelay(); 
}
