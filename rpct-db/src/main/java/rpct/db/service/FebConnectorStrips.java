/**
 * Created on 2013-08-14
 * Filip Thyssen
 */

package rpct.db.service;

public interface FebConnectorStrips {
    public abstract int getId();
    public abstract int getLinkBoardId();
    public abstract int getLinkBoardInput();
    public abstract int getFebBoardId();
    public abstract int getFebBoardConnector();
    public abstract String getLocation();
    public abstract String getPartition();
    public abstract int getRollConnector();
    public abstract int getPins();
    public abstract int getFirstStrip();
    public abstract int getSlope();
}
