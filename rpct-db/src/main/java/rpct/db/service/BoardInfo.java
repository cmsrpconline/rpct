package rpct.db.service;

public interface BoardInfo {
    int getId();
    String getName();
    String getLabel();    
    String getType();
    int getPositionOrAddress();
    boolean isDisabled();
    ChipInfo[] getChipInfos();
}
