/**
 * Created on 2006-12-11
 *
 * @author William Whitaker & Michal Pietrusinski
 * @version $Id$
 */

package rpct.db.service;


public interface ChamStripAccessInfo {
	public abstract String getChamberLocationName();
	public abstract int getBoardId();
	public abstract int getXdaqAppInstance();
	public abstract int getChipId();
	public abstract int getFebId();
	public abstract int getFebConnectorNum();
	public abstract String getFebLocalEtaPartition();
	public abstract int getFebI2cAddress();
	public abstract int getCableChanNum();
	public abstract int getChamberStripNum();
    public abstract int getChamberStripId();
}