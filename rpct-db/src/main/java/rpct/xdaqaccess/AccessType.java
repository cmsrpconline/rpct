package rpct.xdaqaccess;

public enum AccessType {
    NONE, READ, WRITE, BOTH;
}
