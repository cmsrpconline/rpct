package rpct.xdaqaccess;

import java.rmi.RemoteException;
import java.util.List;

import javax.xml.rpc.ServiceException;

import rpct.xdaqaccess.diag.Diag;
import rpct.xdaqaccess.diag.DiagnosticReadout;
import rpct.xdaqaccess.diag.HistoMgr;
import rpct.xdaqaccess.diag.Pulser;


public interface Device extends HardwareItem {
    DeviceDesc getDeviceDesc();
    //DeviceItem[] getItems();
    //DeviceItem findById(int id);
    DeviceItem getItem(String name);  
    DeviceItem getItem(DeviceDesc.ItemDesc itemDesc);   
    Board getBoard();
    int getPosition();
    
    List<Diag> getDiags() throws RemoteException, ServiceException;
    List<Pulser> getPulsers() throws RemoteException, ServiceException;
    List<HistoMgr> getHistoMgrs() throws RemoteException, ServiceException;
    List<DiagnosticReadout> getDiagnosticReadouts() throws RemoteException, ServiceException;
}
