package rpct.xdaqaccess;


import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.DeviceDesc;


public interface DeviceItem {
    Device getDevice();
    DeviceDesc.ItemDesc getDesc();
    int getId();
    String getName();
    int getNumber();
    DeviceItemType getType();
    
    long read() throws RemoteException, ServiceException;
    void write(long value) throws RemoteException, ServiceException;
    Binary readBinary() throws RemoteException, ServiceException;
    void writeBinary(Binary value) throws RemoteException, ServiceException;
}
