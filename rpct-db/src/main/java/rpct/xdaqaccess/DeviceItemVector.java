package rpct.xdaqaccess;


public interface DeviceItemVector extends DeviceItem {
    DeviceItemBits[] getBits();
}
