package rpct.xdaqaccess.hardwaredb;

import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.synchro.LVDSTransmDevice;
import rpct.xdaqaccess.synchro.LVDSTransmDeviceHSB;

/**
 * @author Karol BUnkowski
 * The class for the device, which is transmitter or reciever in the fast muxed lvds trnasmission 
 *
 */
public class HardwareDbMuxTransChip extends HardwareDbChipImpl {
	private LVDSTransmDevice transmDevice;
	
	public HardwareDbMuxTransChip(Device device, Chip chip,
			HardwareDbMapper hardwareDbMapper) {
		super(device, chip, hardwareDbMapper);
		if(chip.getType() == ChipType.HALFSORTER)
		    transmDevice = new LVDSTransmDeviceHSB(getHardwareChip());
		else   
		    transmDevice = new LVDSTransmDevice(getHardwareChip());
	}

	
    public static class Factory implements HardwareDbChipFactory {
        public HardwareDbChip createHardwareDbChip(Device device, Chip chip, HardwareDbMapper hardwareDbMapper) {
            return new HardwareDbMuxTransChip(device, chip, hardwareDbMapper);
        }
    }


	public LVDSTransmDevice getTransmDevice() {
		return transmDevice;
	}
}
