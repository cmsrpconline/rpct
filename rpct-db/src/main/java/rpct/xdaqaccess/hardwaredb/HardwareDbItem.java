package rpct.xdaqaccess.hardwaredb;

import rpct.xdaqaccess.HardwareItem;

public interface HardwareDbItem {  
    int getId();
    String getName(); 
    HardwareItem getHardwareItem();
    Object getEntity();  
    String getFullName();
}
