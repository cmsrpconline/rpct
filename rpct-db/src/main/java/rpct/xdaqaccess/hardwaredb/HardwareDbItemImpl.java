package rpct.xdaqaccess.hardwaredb;

import rpct.xdaqaccess.HardwareItem;

public abstract class HardwareDbItemImpl implements HardwareDbItem {

    private HardwareItem hardwareItem;
    private Object entity;
    protected HardwareDbMapper hardwareDbMapper;
    
    public HardwareDbItemImpl(HardwareItem hardwareItem, Object entity, HardwareDbMapper hardwareDbMapper) {
    	if (hardwareItem == null || entity == null || hardwareDbMapper == null) {
    		throw new NullPointerException();
    	}
        this.hardwareItem = hardwareItem;
        this.entity = entity;
        this.hardwareDbMapper = hardwareDbMapper;
    }
    
    public Object getEntity() {
        return entity;
    }

    public HardwareItem getHardwareItem() {
        return hardwareItem;
    }

    public int getId() {
        return hardwareItem.getId();
    }

    public String getName() {
        return hardwareItem.getName();
    }
}
