package rpct.xdaqaccess.hardwaredb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.rpc.ServiceException;

import rpct.xdaqaccess.synchro.OptLink;
import rpct.xdaqaccess.synchro.OptLinkSender;

public class HOSender implements OptLinkSender {
    private List<OptLink> usedOptLinks = new ArrayList<OptLink>();
    public void reset() throws RemoteException, ServiceException {
        // TODO Auto-generated method stub
        
    }
    
    public void enableFindOptLinksSynchrDelay(boolean enable, long testData)
            throws ServiceException {
        String y_n = "y";
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("enableFindOptLinksSynchrDelay and press a key to continue");

        try {
            y_n = br.readLine();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } 
    }

    public void enableTransmissionCheck(boolean enableBCNcheck,
            boolean enableDataCheck) throws RemoteException, ServiceException {
        String y_n = "y";
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("enableTransmissionCheck and press a key to continue");

        try {
            y_n = br.readLine();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } 
    }

    public void normalLinkOperation() throws RemoteException, ServiceException {
        String y_n = "y";
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("normalLinkOperation and press a key to continue");

        try {
            y_n = br.readLine();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } 
    }

    public void synchronizeLink() throws RemoteException, ServiceException {
        String y_n = "y";
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("synchronizeLink and press a key to continue");

        try {
            y_n = br.readLine();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } 
    }

    public void enableTest(int testEna, int rndTestEna)
            throws RemoteException, ServiceException {
        String y_n = "y";
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("enableTest and press a key to continue");

        try {
            y_n = br.readLine();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void registerUsedOptLink(OptLink optLink) {
        usedOptLinks.add(optLink);        
    }

    public List<OptLink> getUsedOptLinks() {
        return usedOptLinks;
    }

    public String getFullName() {
        // TODO Auto-generated method stub
        return "HOSender getName not implemented";
    }

    public void setSendDelay(int foundDelay) throws RemoteException,
            ServiceException {        
        throw new RuntimeException("setSendDelay not impmented");
    }

    public int getSendDelay() {
        // TODO Auto-generated method stub
        return 0;
    }



}
