package rpct.xdaqaccess.hardwaredb;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.DataIntegrityException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.XdaqApplication;
import rpct.db.domain.configuration.XdaqExecutive;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.xdaq.XdaqApplicationInfo;
import rpct.xdaqaccess.Board;
import rpct.xdaqaccess.Crate;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.diag.Diag;
import rpct.xdaqaccess.diag.DiagId;
import rpct.xdaqaccess.diag.DiagnosticReadout;
import rpct.xdaqaccess.diag.Pulser;
import rpct.xdaqaccess.diag.axis.DiagIdImplAxis;

public class HardwareDbMapper {
    private HardwareRegistry hardwareRegistry;
    private HibernateContext hibernateContext;
    private ConfigurationDAO configurationDAO;

    private Map<Integer, HardwareDbCrate> cratesById = new HashMap<Integer, HardwareDbCrate>();
    private Map<Integer, HardwareDbBoard> boardsById = new HashMap<Integer, HardwareDbBoard>();
    private Map<Integer, HardwareDbChip> chipsById = new HashMap<Integer, HardwareDbChip>();

    private Map<CrateType, HardwareDbCrateFactory> crateFactories = new HashMap<CrateType, HardwareDbCrateFactory>();
    private Map<BoardType, HardwareDbBoardFactory> boardFactories = new HashMap<BoardType, HardwareDbBoardFactory>();
    private Map<ChipType, HardwareDbChipFactory> chipFactories = new HashMap<ChipType, HardwareDbChipFactory>();
    
    public HardwareDbMapper(HardwareRegistry hardwareRegistry,
            HibernateContext hibernateContext) {
        super();
        this.hardwareRegistry = hardwareRegistry;
        this.hibernateContext = hibernateContext;
        this.configurationDAO = new ConfigurationDAOHibernate(hibernateContext);
        
        crateFactories.put(CrateType.TRIGGERCRATE, new HardwareDbTriggerCrate.Factory());
        crateFactories.put(CrateType.SORTERCRATE, new HardwareDbSorterCrate.Factory());
        boardFactories.put(BoardType.TRIGGERBOARD, new HardwareDBTriggerBoard.Factory());
        boardFactories.put(BoardType.TTUBOARD, new HardwareDBTTUBoard.Factory());
        chipFactories.put(ChipType.SYNCODER, new HardwareDbSynCoder.Factory());
        chipFactories.put(ChipType.OPTO, new HardwareDbOpto.Factory());
        chipFactories.put(ChipType.PAC, new HardwareDbMuxRecChip.Factory());
        chipFactories.put(ChipType.RMB, new HardwareDbMuxRecChip.Factory());
        chipFactories.put(ChipType.GBSORT, new HardwareDbMuxRecChip.Factory());
        chipFactories.put(ChipType.TCSORT, new HardwareDbMuxRecChip.Factory());
        chipFactories.put(ChipType.HALFSORTER, new HardwareDbMuxRecChip.Factory());
        
        chipFactories.put(ChipType.TTUOPTO, new HardwareDbOpto.Factory());
        chipFactories.put(ChipType.TTUTRIG, new HardwareDbMuxRecChip.Factory());
    }

    public HardwareRegistry getHardwareRegistry() {
        return hardwareRegistry;
    }

    public HibernateContext getHibernateContext() {
        return hibernateContext;
    }

    public Diag getDiag(Chip chip, String name) throws DataAccessException,
            RemoteException, ServiceException {
        DiagId diagId = new DiagIdImplAxis(chip.getId(), name);
        Diag diag = hardwareRegistry.findDiagById(diagId);
        if (diag == null) {
            try {
                XdaqApplication xdaqApplication = configurationDAO
                        .getXdaqApplication(chip);

                if (xdaqApplication == null) {
                    throw new IllegalArgumentException(
                            "XdaqApplication not found for chip " + chip);
                }
                XdaqExecutive xdaqExecutive = xdaqApplication
                        .getXdaqExecutive();
                URL url = new URL("http://" + xdaqExecutive.getHost() + ":"
                        + xdaqExecutive.getPort());
                XdaqApplicationInfo info = new XdaqApplicationInfo(url,
                        xdaqApplication.getClassName(), 
                        xdaqApplication.getInstance());
                if (hardwareRegistry.isXdaqApplicationRegistered(info)) {
                	throw new DataIntegrityException("Expected diag " + diagId
                        + " not found in xdaq " + info);                	
                }
                hardwareRegistry.registerXdaqApplication(info);
                diag = hardwareRegistry.findDiagById(diagId);
                if (diag == null) {
                    throw new DataIntegrityException("Expected diag " + diagId
                            + " not found in xdaq " + info);
                }
            } catch (MalformedURLException e) {
                throw new DataIntegrityException(e);
            } finally {
                //hibernateContext.closeSession();
            }

        }
        return diag;
    }

	public List<Diag> getDiags(List<Chip> chips, String name) throws DataAccessException, RemoteException, ServiceException {
		List<Diag> diags = new ArrayList<Diag>(chips.size());
		for (Chip chip : chips) {
			diags.add(getDiag(chip, name));
		}
		return diags;
	}

	public List<Pulser> getPulsers(List<Chip> chips) throws DataAccessException, RemoteException, ServiceException {
		List<Pulser> diags = new ArrayList<Pulser>(chips.size());
		for (Chip chip : chips) {
			diags.add((Pulser) getDiag(chip, "PULSER"));
		}
		return diags;
	}
	
	public List<DiagnosticReadout> getReadouts(List<Chip> chips) throws DataAccessException, RemoteException, ServiceException {
		List<DiagnosticReadout> diags = new ArrayList<DiagnosticReadout>(chips.size());
		for (Chip chip : chips) {
			diags.add((DiagnosticReadout) getDiag(chip, "READOUT"));
		}
		return diags;
	}
	
	private Crate getHardwareCrate(rpct.db.domain.equipment.Crate crate) throws DataAccessException,
            RemoteException, ServiceException {
    	Crate result = hardwareRegistry.findCrateById(crate.getId());
        if (result == null) {
            try {
                XdaqApplication xdaqApplication = configurationDAO
                        .getXdaqApplication(crate);

                if (xdaqApplication == null) {
                    throw new IllegalArgumentException(
                            "XdaqApplication not found for crate " + crate);
                }
                XdaqExecutive xdaqExecutive = xdaqApplication.getXdaqExecutive();
                URL url = new URL("http://" + xdaqExecutive.getHost() + ":"
                        + xdaqExecutive.getPort());
                XdaqApplicationInfo info = new XdaqApplicationInfo(url,
                        xdaqApplication.getClassName(), 
                        xdaqApplication.getInstance());
                hardwareRegistry.registerXdaqApplication(info);
                result = hardwareRegistry.findCrateById(crate.getId());
                if (result == null) {
                    throw new DataIntegrityException("Expected crate " + crate
                            + " not found in xdaq " + info);
                }
            } catch (MalformedURLException e) {
                throw new DataIntegrityException(e);
			} finally {
                //hibernateContext.closeSession();
            }
        }
        return result;
    }

    /*private Board getHardwareBoard(rpct.db.domain.equipment.Board board) throws DataAccessException,
            RemoteException, ServiceException {
        Board result = hardwareRegistry.findBoardById(board.getId());
        if (result == null) {
            // it is possible that only crate was loaded, but boards not.            
            Crate crate = hardwareRegistry.findCrateById(board.getCrate().getId());
            if (crate != null) {
                crate.getBoards(); 
                result = hardwareRegistry.findBoardById(board.getId());
            }
            else {
                try {
                    XdaqApplication xdaqApplication = configurationDAO
                            .getXdaqApplication(board);

                    if (xdaqApplication == null) {
                        throw new IllegalArgumentException(
                                "XdaqApplication not found for board " + board);
                    }
                    XdaqExecutive xdaqExecutive = xdaqApplication.getXdaqExecutive();
                    URL url = new URL("http://" + xdaqExecutive.getHost() + ":"
                            + xdaqExecutive.getPort());
                    XdaqApplicationInfo info = new XdaqApplicationInfo(url,
                            xdaqApplication.getClassName(), 
                            xdaqApplication.getInstance());
                    if (hardwareRegistry.getXdaqApplicationAccess(info) != null) {
                        // data from this xdaq is already retrieved, there is not such board
                        throw new NoSuchElementException("No board " + board.getId() + " in xdaq " + info);
                    }
                    hardwareRegistry.registerXdaqApplication(info);
                    result = hardwareRegistry.findBoardById(board.getId());
                    if (result == null) {
                        throw new DataIntegrityException("Expected board " + board
                                + " not found in xdaq " + info);
                    }
                } catch (MalformedURLException e) {
                    throw new DataIntegrityException(e);
                } finally {
                    //hibernateContext.closeSession();
                }                
            }
        }
        
        /*if (result == null) {
            try {
                XdaqApplication xdaqApplication = configurationDAO
                        .getXdaqApplication(board);

                if (xdaqApplication == null) {
                    throw new IllegalArgumentException(
                            "XdaqApplication not found for board " + board);
                }
                XdaqExecutive xdaqExecutive = xdaqApplication.getXdaqExecutive();
                URL url = new URL("http://" + xdaqExecutive.getHost() + ":"
                        + xdaqExecutive.getPort());
                XdaqApplicationInfo info = new XdaqApplicationInfo(url,
                        xdaqApplication.getClassName(), 
                        xdaqApplication.getInstance());
                if (hardwareRegistry.getXdaqApplicationAccess(info) != null) {
                	// data from this xdaq is already retrieved, there is not such board
                	throw new NoSuchElementException("No board " + board.getId() + " in xdaq " + info);
                }
                hardwareRegistry.registerXdaqApplication(info);
                result = hardwareRegistry.findBoardById(board.getId());
                if (result == null) {
                    throw new DataIntegrityException("Expected board " + board
                            + " not found in xdaq " + info);
                }
            } catch (MalformedURLException e) {
                throw new DataIntegrityException(e);
			} finally {
                //hibernateContext.closeSession();
            }
        }*/
        /*return result;
    }*/

    private Board getHardwareBoard(rpct.db.domain.equipment.Board board) throws DataAccessException,
            RemoteException, ServiceException {
        Board result = hardwareRegistry.findBoardById(board.getId());
        if (result == null) {
            // it is possible that only crate was loaded, but boards not. 
            Crate crate = getHardwareCrate(board.getCrate());
            crate.getBoards(); // force crate to load boards            
            result = hardwareRegistry.findBoardById(board.getId());
            if (result == null) {
                throw new NoSuchElementException("Expected board " + board + " not found");
            }
        }
        
        return result;
    }

    private Device getHardwareDevice(rpct.db.domain.equipment.Chip chip) throws DataAccessException,
            RemoteException, ServiceException {
        Device result = hardwareRegistry.findDeviceById(chip.getId());
        if (result == null) {
            // it is possible that only crate or board was loaded, but devices not. 
            Board board = getHardwareBoard(chip.getBoard());
            board.getDevices(); // force board to load devices   
            result = hardwareRegistry.findDeviceById(chip.getId());
            if (result == null) {
                throw new NoSuchElementException("Expected chip " + chip + " not found");            
            }
        }
        
        return result;
    }

    /*private Device getHardwareDevice(rpct.db.domain.equipment.Chip chip) throws DataAccessException,
            RemoteException, ServiceException {
    	Device result = hardwareRegistry.findDeviceById(chip.getId());
        if (result == null) {
            try {
                XdaqApplication xdaqApplication = configurationDAO
                        .getXdaqApplication(chip);

                if (xdaqApplication == null) {
                    throw new IllegalArgumentException(
                            "XdaqApplication not found for chip " + chip);
                }
                XdaqExecutive xdaqExecutive = xdaqApplication.getXdaqExecutive();
                URL url = new URL("http://" + xdaqExecutive.getHost() + ":"
                        + xdaqExecutive.getPort());
                XdaqApplicationInfo info = new XdaqApplicationInfo(url,
                        xdaqApplication.getClassName(), 
                        xdaqApplication.getInstance());
                hardwareRegistry.registerXdaqApplication(info);
                result = hardwareRegistry.findDeviceById(chip.getId());
                if (result == null) {
                    throw new DataIntegrityException("Expected chip " + chip
                            + " not found in xdaq " + info);
                }
            } catch (MalformedURLException e) {
                throw new DataIntegrityException(e);
			} finally {
                //hibernateContext.closeSession();
            }
        }
        return result;
    }*/

    public HardwareDbCrate getCrate(rpct.db.domain.equipment.Crate crate) throws DataAccessException,
            RemoteException, ServiceException {
        HardwareDbCrate result = cratesById.get(crate.getId());
        if (result == null) {
            HardwareDbCrateFactory f = crateFactories.get(crate.getType());
            if (f == null) {
                f = new HardwareDbCrateImpl.Factory();
            }
            result = f.createHardwareDbCrate(getHardwareCrate(crate), crate, this);
            cratesById.put(crate.getId(), result);
        }
        return result;
    }

    public HardwareDbBoard getBoard(rpct.db.domain.equipment.Board board) throws DataAccessException,
            RemoteException, ServiceException {
        HardwareDbBoard result = boardsById.get(board.getId());
        if (result == null) {
            HardwareDbBoardFactory f = boardFactories.get(board.getType());
            if (f == null) {
                f = new HardwareDbBoardImpl.Factory();
            }
            result = f.createHardwareDbBoard(getHardwareBoard(board), board, this);
            boardsById.put(board.getId(), result);
        }
        return result;
    }


    public HardwareDbChip getChip(rpct.db.domain.equipment.Chip chip) throws DataAccessException,
            RemoteException, ServiceException {
        HardwareDbChip result = chipsById.get(chip.getId());
        if (result == null) {
            HardwareDbChipFactory f = chipFactories.get(chip.getType());
            if (f == null) {
                f = new HardwareDbChipImpl.Factory();
            }
            result = f.createHardwareDbChip(getHardwareDevice(chip), chip, this);
            chipsById.put(chip.getId(), result);
        }
        return result;
    }

    public ConfigurationDAO getConfigurationDAO() {
        return configurationDAO;
    }

}
