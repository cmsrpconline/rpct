package rpct.xdaqaccess.hardwaredb;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.xdaqaccess.Crate;
import rpct.xdaqaccess.synchro.LVDSLink;

public class HardwareDbSorterCrate extends HardwareDbCrateImpl implements CrateWithLVDSLinks {
	private ArrayList<HardwareDBTTUBoard> hardwareDBTTUBoards;
	
	private  ArrayList<HardwareDbBoard> halfSorterBoards = new ArrayList<HardwareDbBoard>();;
	
    private HardwareDbBoard finalSorterBoard;
    
//    private HardwareDbBoard tcBackplane;
    
//    private HardwareDbChip tcSort;
    
//    private int usedTbMask;
        
    public HardwareDbSorterCrate(Crate hardCrate,
            rpct.db.domain.equipment.Crate crate,
            HardwareDbMapper hardwareDbMapper) {
        super(hardCrate, crate, hardwareDbMapper);              
    }

    public TriggerCrate getDbTriggerCrate() {
        return (TriggerCrate)getDbCrate();
    }       
    
    private boolean boardsCollectionsBuild = false;
    
    private void buildBoardsCollections() throws HardwareDbException {
        if(boardsCollectionsBuild)
            return;
        hardwareDBTTUBoards = new ArrayList<HardwareDBTTUBoard>();
        for(HardwareDbBoard board : getBoards()) {
            if(board.getDbBoard().getType() == BoardType.TTUBOARD) {
                hardwareDBTTUBoards.add((HardwareDBTTUBoard)board);
            }
            else if(board.getDbBoard().getType() == BoardType.SORTERBOARD) {
                if(board.getDbBoard().getName().contains("HSB")) {
                    halfSorterBoards.add(board);
                }
                else if(board.getDbBoard().getName().contains("FSB")) {
                    finalSorterBoard = board;
                }
            }
            /*else if(board.getDbBoard().getType() == BoardType.TCBACKPLANE) {
                tcBackplane = board;
            } */           
        }
        /*for(HardwareDbChip chip : tcBackplane.getChips()) {
            if(chip.getDbChip().getType() == ChipType.TCSORT)
                tcSort = chip;
        }*/
        boardsCollectionsBuild = true;
    }
      
    public List<HardwareDBTTUBoard> getHardwareDBTTUBoards() throws HardwareDbException {
        buildBoardsCollections();
        return hardwareDBTTUBoards;
    }

    public ArrayList<HardwareDbBoard> getHalfSorterBoards() {
        return halfSorterBoards;
    }

    public HardwareDbBoard getFinalSorterBoard() {
        return finalSorterBoard;
    }
    
    
    private List<LVDSLink> lvdsLinks = new ArrayList<LVDSLink>();
    
    public List<LVDSLink> getLVDSLinks() throws Exception {
        if(lvdsLinks.size() != 0) { 
            return lvdsLinks;
        }
        
        for(HardwareDBTTUBoard ttu : getHardwareDBTTUBoards()) {
            lvdsLinks.addAll(ttu.getLVDSLinks());
        }
        
        return lvdsLinks;
    }

    public List<HardwareDbMuxRecChip> getMuxRecChips() throws HardwareDbException {
        List<HardwareDbMuxRecChip> muxRecChips = new ArrayList<HardwareDbMuxRecChip>();
        for(HardwareDBTTUBoard ttu : getHardwareDBTTUBoards()) {
            muxRecChips.addAll(ttu.getMuxRecChips());            
        }
        return muxRecChips;
    }
    
 /*   public HardwareDbBoard getTcBackplane() throws HardwareDbException {
        buildBoardsCollections();
        return tcBackplane;
    }

    public HardwareDbChip getTcSort() throws HardwareDbException {
        buildBoardsCollections();
        return tcSort;
    }*/
    
    public static class Factory implements HardwareDbCrateFactory {
        public HardwareDbCrate createHardwareDbCrate(Crate hardCrate,
                rpct.db.domain.equipment.Crate crate, HardwareDbMapper hardwareDbMapper) {
            return new HardwareDbSorterCrate(hardCrate, crate, hardwareDbMapper);
        }        
    }

/*	public int getUsedTBsMask() {
		return usedTbMask;
	}
*/    
}
