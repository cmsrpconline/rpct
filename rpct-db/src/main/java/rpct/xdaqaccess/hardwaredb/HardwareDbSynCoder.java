package rpct.xdaqaccess.hardwaredb;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.rpc.ServiceException;

import rpct.db.domain.equipment.Chip;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.DeviceItem;
import rpct.xdaqaccess.synchro.OptLink;
import rpct.xdaqaccess.synchro.OptLinkSender;

public class HardwareDbSynCoder extends HardwareDbChipImpl implements OptLinkSender {
    private List<OptLink> usedOptLinks = new ArrayList<OptLink>();
    
    private int sendDelay = -1;
    
    public void registerUsedOptLink(OptLink optLink) {
        usedOptLinks.add(optLink);        
    }

    public List<OptLink> getUsedOptLinks() {
        return usedOptLinks;
    }
    
/*    public String getName() {       
            //return "SynCoder " + getBoard().getName();
        return getFullName();
        
    }*/
    
    public void reset() throws RemoteException, ServiceException {
        getHardwareChip().getItem("SEND.TEST_ENA").write(0);
        getHardwareChip().getItem("PULSER.OUT_ENA").write(0);
        getHardwareChip().getItem("SEND.TEST_RND_ENA").write(0);       
        //getHardwareChip().findByName("PULSER.LOOP_SEL").write(0);
        enableTransmissionCheck(false, false);
    }
    
	public HardwareDbSynCoder(Device device, Chip chip,
			HardwareDbMapper hardwareDbMapper) {
		super(device, chip, hardwareDbMapper);
	}

	public void synchronizeLink() throws RemoteException, ServiceException {
		getHardwareChip().getItem("GOL.TX_ENA").write(0);
		getHardwareChip().getItem("GOL.TX_ERR").write(0);
	}
	    
	public void normalLinkOperation() throws RemoteException, ServiceException {
		getHardwareChip().getItem("GOL.TX_ENA").write(1);
		getHardwareChip().getItem("GOL.TX_ERR").write(0);     
	}

	public void enableFindOptLinksSynchrDelay(boolean enable, long testData) throws RemoteException, ServiceException {
	    if(enable) {
	    	getHardwareChip().getItem("SEND.CHECK_DATA_ENA").write(0);
	    	getHardwareChip().getItem("SEND.CHECK_ENA").write(0);
	    	getHardwareChip().getItem("SEND.TEST_RND_ENA").write(0);
	    	getHardwareChip().getItem("SEND.TEST_ENA").write(1);
	    	getHardwareChip().getItem("SEND_TEST_DATA").write(testData);
	    }
	    else {
	    	getHardwareChip().getItem("SEND.TEST_ENA").write(0);
	    	getHardwareChip().getItem("SEND_TEST_DATA").write(0);    
	    }
	}
	
	public void enableTransmissionCheck(boolean enableBCNcheck, boolean enableDataCheck) throws RemoteException, ServiceException {
		boolean checkDataEna = enableBCNcheck;
		boolean checkEna = enableDataCheck;
	    
	    if(checkDataEna) {
	        getHardwareChip().getItem("SEND.CHECK_DATA_ENA").write(1);
	    }   
	    else {
	    	getHardwareChip().getItem("SEND.CHECK_DATA_ENA").write(0);  
	    }   
	    
	    if(checkEna) {  
	    	getHardwareChip().getItem("SEND.CHECK_ENA").write(1);
	    }
	    else {
	    	getHardwareChip().getItem("SEND.CHECK_ENA").write(0);
	    }           
	}
	
	public static class Factory implements HardwareDbChipFactory {
		public HardwareDbChip createHardwareDbChip(Device device, Chip chip, HardwareDbMapper hardwareDbMapper) {
			return new HardwareDbSynCoder(device, chip, hardwareDbMapper);
		}
	}

    public void enableTest(int testEna, int rndTestEna)
            throws RemoteException, ServiceException {
        getHardwareChip().getItem("SEND.TEST_ENA").write(testEna); 
        getHardwareChip().getItem("SEND.TEST_RND_ENA").write(rndTestEna);
    }

    public void setSendDelay(int sendDelay) throws RemoteException, ServiceException {
        getHardwareChip().getItem("SEND_DELAY").write(sendDelay);   
        this.sendDelay = sendDelay;
    }

    public int getSendDelay() {
        return this.sendDelay;
    }
}
