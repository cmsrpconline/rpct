package rpct.xdaqaccess.hardwaredb;

import java.util.List;

import rpct.xdaqaccess.synchro.LVDSLink;

public interface CrateWithLVDSLinks {

    public abstract List<LVDSLink> getLVDSLinks() throws Exception;
    public abstract List<HardwareDbMuxRecChip> getMuxRecChips() throws HardwareDbException;

}