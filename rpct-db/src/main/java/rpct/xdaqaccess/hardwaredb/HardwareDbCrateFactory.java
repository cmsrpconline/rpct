package rpct.xdaqaccess.hardwaredb;

import rpct.xdaqaccess.Crate;

public interface HardwareDbCrateFactory {
    HardwareDbCrate createHardwareDbCrate(Crate hardCrate, rpct.db.domain.equipment.Crate crate, HardwareDbMapper hardwareDbMapper);
}
