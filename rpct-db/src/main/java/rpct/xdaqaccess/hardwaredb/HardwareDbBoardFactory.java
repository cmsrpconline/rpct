package rpct.xdaqaccess.hardwaredb;

import rpct.xdaqaccess.Board;

public interface HardwareDbBoardFactory {
    HardwareDbBoard createHardwareDbBoard(Board hBoard, rpct.db.domain.equipment.Board eBoard, HardwareDbMapper hardwareDbMapper);
}
