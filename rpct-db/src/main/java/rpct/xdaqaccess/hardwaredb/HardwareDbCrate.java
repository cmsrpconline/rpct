package rpct.xdaqaccess.hardwaredb;

import java.util.List;

import rpct.xdaqaccess.Crate;

public interface HardwareDbCrate extends HardwareDbItem {
    List<HardwareDbBoard> getBoards() throws HardwareDbException;
    HardwareDbBoard[] getBoardsArray() throws HardwareDbException;
    Crate getHardwareCrate();
    rpct.db.domain.equipment.Crate getDbCrate();
}
