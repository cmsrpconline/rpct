package rpct.xdaqaccess.hardwaredb;

import java.util.ArrayList;
import java.util.List;

import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.xdaqaccess.Board;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.synchro.LVDSLink;

public class HardwareDBTriggerBoard extends HardwareDbBoardImpl {
	private Device control;
	
	private List<HardwareDbOpto> optos;

    private List<HardwareDbChip> pacs;

    private HardwareDbChip rmb;

    private HardwareDbChip gsSort;
    
    private int usedPacksMask;

    public HardwareDBTriggerBoard(Board hardBoard,
            rpct.db.domain.equipment.Board board,
            HardwareDbMapper hardwareDbMapper) {
        super(hardBoard, board, hardwareDbMapper);     
    }

    boolean chipsCollectionBuiled = false;
    private void buildChipsCollections() throws HardwareDbException {
        if(chipsCollectionBuiled)
            return;
        optos = new ArrayList<HardwareDbOpto>();
        pacs = new ArrayList<HardwareDbChip>();
        for(int i = 0; i < 6; i++) {
            optos.add(null);
        }
        for(int i = 0; i < getDBTriggerBoard().getTowersNums().length; i++) {
            pacs.add(null);
        }
        
        for(HardwareDbChip hdbChip : getChips()) {
            if(hdbChip.getDbChip().getType() == ChipType.OPTO) {
                optos.set(TriggerBoard.getChipNum(hdbChip.getDbChip()), (HardwareDbOpto)hdbChip);
            }
            else if(hdbChip.getDbChip().getType() == ChipType.PAC) {
                pacs.set(TriggerBoard.getChipNum(hdbChip.getDbChip()), hdbChip);
                usedPacksMask |= (1 << TriggerBoard.getChipNum(hdbChip.getDbChip())); 
            }
            else if(hdbChip.getDbChip().getType() == ChipType.RMB) {
                rmb = hdbChip;
            }        
            else if(hdbChip.getDbChip().getType() == ChipType.GBSORT) {
                gsSort = hdbChip;
            }             
        }  
        
        for (Device device : getHardwareBoard().getDevices()) {
			if (device.getType().equals("TB3CONTROL") == true) {
				control = device;
				break;
			}
        }
        
        chipsCollectionBuiled = true;
    }

    public rpct.db.domain.equipment.TriggerBoard getDBTriggerBoard() {
        return (rpct.db.domain.equipment.TriggerBoard)getDbBoard();
    }

    public Device getControlChip() throws HardwareDbException {
    	buildChipsCollections();
    	return control;
    }
    public HardwareDbChip getGbSort() throws HardwareDbException{
        buildChipsCollections();
        return gsSort;
    }

    public List<HardwareDbOpto> getOptos() throws HardwareDbException {
        buildChipsCollections();
        return optos;
    }

    public List<HardwareDbChip> getPacs() throws HardwareDbException {
        buildChipsCollections();
        return pacs;
    }

    public HardwareDbChip getRmb() throws HardwareDbException {
        buildChipsCollections();
        return rmb;
    }

    public List<HardwareDbMuxRecChip> getMuxRecChips() throws HardwareDbException {
        List<HardwareDbMuxRecChip> muxRecChips = new ArrayList<HardwareDbMuxRecChip>();
        for(HardwareDbChip chip : getPacs()) {
            muxRecChips.add((HardwareDbMuxRecChip)chip);
        }
        muxRecChips.add((HardwareDbMuxRecChip)getRmb());
        muxRecChips.add((HardwareDbMuxRecChip)getGbSort());
        return muxRecChips;
    }

    private List<LVDSLink> lvdsLinks = new ArrayList<LVDSLink>();
    
    public List<LVDSLink> getLVDSLinks() throws Exception {
        if(lvdsLinks.size() != 0) { 
            return lvdsLinks;
        }
        for(HardwareDbChip pac : getPacs()) {
        	if(pac == null) {
        		System.out.println(getFullName() + " pac == null");
        	} 
            for(HardwareDbChip opto : getOptos()) {
                for(int iLinkNum = 0; iLinkNum < 3; iLinkNum++) {
                    lvdsLinks.add(new LVDSLink(((HardwareDbMuxTransChip)opto).getTransmDevice(), 
                    		((HardwareDbMuxTransChip)pac).getTransmDevice(), 
                            3 * TriggerBoard.getChipNum(opto.getDbChip()) + iLinkNum));
                }
            }
        }

        if(getRmb() != null)
            for(HardwareDbChip opto : getOptos()) {
                for(int iLinkNum = 0; iLinkNum < 3; iLinkNum++) {
                    lvdsLinks.add(new LVDSLink(((HardwareDbMuxTransChip)opto).getTransmDevice(), 
                    		((HardwareDbMuxTransChip)getRmb()).getTransmDevice(), 
                            3 * TriggerBoard.getChipNum(opto.getDbChip()) + iLinkNum));
                }
            }

        if(getGbSort() != null)
            for(HardwareDbChip pac : getPacs()) {
                lvdsLinks.add(new LVDSLink(((HardwareDbMuxTransChip)pac).getTransmDevice(), 
                		((HardwareDbMuxTransChip)getGbSort()).getTransmDevice(), 
                		TriggerBoard.getChipNum(pac.getDbChip())));
            }

        return lvdsLinks;
    }

    public int getUsedPacMask() throws HardwareDbException {
    	buildChipsCollections();
    	return usedPacksMask;
    }
    
    public static class Factory implements HardwareDbBoardFactory {
        public HardwareDbBoard createHardwareDbBoard(Board hardBoard,
                rpct.db.domain.equipment.Board board, HardwareDbMapper hardwareDbMapper) {
            return new HardwareDBTriggerBoard(hardBoard, board, hardwareDbMapper);
        }        
    }
}
