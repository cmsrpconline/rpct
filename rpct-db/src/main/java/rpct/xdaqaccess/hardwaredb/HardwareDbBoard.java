package rpct.xdaqaccess.hardwaredb;

import java.util.List;

import rpct.xdaqaccess.Board;

public interface HardwareDbBoard extends HardwareDbItem {
    HardwareDbCrate getCrate() throws HardwareDbException;
    List<HardwareDbChip> getChips() throws HardwareDbException;
    HardwareDbChip[] getChipsArray() throws HardwareDbException;
    Board getHardwareBoard();
    rpct.db.domain.equipment.Board getDbBoard();
}
