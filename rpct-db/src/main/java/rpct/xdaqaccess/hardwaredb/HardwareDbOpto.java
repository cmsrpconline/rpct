package rpct.xdaqaccess.hardwaredb;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.xml.rpc.ServiceException;

import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.DeviceItem;
import rpct.xdaqaccess.DeviceItemWord;
import rpct.xdaqaccess.synchro.DefOut;
import rpct.xdaqaccess.synchro.OptLink;

public class HardwareDbOpto extends HardwareDbMuxTransChip {
    private List<LinkConn> linkConns;
    
    private int usedOptLinksInputsMask = 0;	
    private List<OptLink> usedOptLinks = new ArrayList<OptLink>();
        
    public List<LinkConn> getLinkConns(boolean enabledOnly) {
        if(linkConns == null) {
            linkConns = new ArrayList<LinkConn>();
            for(LinkConn linkConn : ((TriggerBoard)getDbChip().getBoard()).getLinkConns()) {
                if(linkConn.getOpto() == getDbChip() ) {
                    if(enabledOnly) {
                        if(linkConn.isDisabled() == false)
                            linkConns.add(linkConn);
                    }                    
                    else
                        linkConns.add(linkConn);
                }
                
            }
        }
        return linkConns;
    }
    
    public List<OptLink> getUsedOptLinks() {
        return usedOptLinks;
    }


    private long[] foundDealys = new long[3]; //opt links delays
     
    public HardwareDbOpto(Device device, Chip chip,
            HardwareDbMapper hardwareDbMapper) {
        super(device, chip, hardwareDbMapper);	
    }	

    public void enableOptoLinks(boolean enable) throws HardwareDbException, RemoteException, ServiceException {
        if(enable) {
            getHardwareChip().getItem("TLK.ENABLE").write(usedOptLinksInputsMask); //usedOptLinksInputsMask
            getHardwareChip().getItem("TLK.LCK_REFN").write(usedOptLinksInputsMask);
            getHardwareChip().getItem("REC_TEST_ENA").write((~usedOptLinksInputsMask) & 7);
        }
        else {
            getHardwareChip().getItem("TLK.ENABLE").write(0);
            getHardwareChip().getItem("TLK.LCK_REFN").write(0);
            getHardwareChip().getItem("REC_TEST_ENA").write(7);
        }
    }

    public int getUsedOptLinksInputsMask() {
        return usedOptLinksInputsMask;
    }

    public void setUsedOptLinksInputsMask(int usedOptLinksInputsMask) {
        this.usedOptLinksInputsMask = usedOptLinksInputsMask;
    }

    public void setUsedOptLinksInputsMaskFromHardware() throws RemoteException, ServiceException {
        usedOptLinksInputsMask = (int) getHardwareChip().getItem("TLK.ENABLE").read();         
    }
    
    public void registerUsedOptLink(OptLink optLink) {  
        usedOptLinks.add(optLink);
        usedOptLinksInputsMask |= (1<<optLink.getOptoInputNum());
    }

    public boolean isLinkUsed(int linkNum) {
        if((usedOptLinksInputsMask & (1 << linkNum)) != 0)
            return true;
        return false;
    }
    public boolean CheckLinksSynchronization() throws HardwareDbException, RemoteException, ServiceException {
        boolean good = true;
        
        getHardwareChip().getItem("TLK.RX_NO_DATA").read();
        getHardwareChip().getItem("TLK.RX_ERROR").read();
        
        long noData = getHardwareChip().getItem("TLK.RX_NO_DATA").read(); //link synchronization error? should be 0; 1 if error appierd on given link
        long err = getHardwareChip().getItem("TLK.RX_ERROR").read(); //should be 0
        //LOG4CPLUS_DEBUG(logger, getDescription()<<" CheckLinksSynchronization: TLK.RX_DV "<<noData<<" TLK.RX_ERR "<<err );

        if( (usedOptLinksInputsMask & noData)  != usedOptLinksInputsMask ||
                (usedOptLinksInputsMask & err) != 0   ) {
            good = false;
            DefOut.out.println(getBoard().getName() + " " + getName()  + " CheckLinksSynchronization: TLK.RX_NO_DATA " + noData + " TLK.RX_ERR " + err + " NOT GOOD!!!");	      
        }

        for(int iLink = 0; iLink < 3; iLink++) {
            if(isLinkUsed(iLink) == false)
                continue;

            long rxdata =  ((DeviceItemWord)getHardwareChip().getItem("REC_TEST_DATA")).read(iLink);

            DefOut.out.println(getBoard().getName() + " " + getName()  + " CheckLinksSynchronization: iLink " + iLink + " TLK.RXDATA " + Long.toHexString(rxdata));
            if(rxdata != 0x50bc50bc) {
                good = false;	           
                //DefOut.out.println(getBoard().getName() + " " + getName()  + " CheckLinksSynchronization: iLink " + iLink + " TLK.RXDATA " + rxdata + "  != 0x50bc50bc");
            }
        }
        return good;
    }

    public boolean CheckLinksOperation() throws HardwareDbException, RemoteException, ServiceException {
        boolean good = true;
        getHardwareChip().getItem("TLK.RX_NO_DATA").read(); //
        getHardwareChip().getItem("TLK.RX_ERROR").read(); //
        long noData = getHardwareChip().getItem("TLK.RX_NO_DATA").read(); //
        long err = getHardwareChip().getItem("TLK.RX_ERROR").read(); //

        if( (usedOptLinksInputsMask & noData)  != 0 ||
                (usedOptLinksInputsMask & err) != 0) {
            good = false;	        
            DefOut.out.println(getBoard().getName() + " " + getName()  + " CheckLinksOperation: TLK.RX_NO_DATA " + noData + 
                    " TLK.RX_ERR " + err + " usedLinksMask "+ usedOptLinksInputsMask + " NOT GOOD!!!!");
        }

        return good;
    }

    public static class Factory implements HardwareDbChipFactory {
        public HardwareDbChip createHardwareDbChip(Device device, Chip chip, HardwareDbMapper hardwareDbMapper) {
            return new HardwareDbOpto(device, chip, hardwareDbMapper);
        }
    }

    private void setRecFastDelay(int linkNum, long delay) throws RemoteException, ServiceException, HardwareDbException {	
        DeviceItem recDataDelay =  getHardwareChip().getItem("VREC_FAST_DATA_DELAY.REC_FAST_DATA_DELAY");		
        long value = recDataDelay.read();
        value &= ~(7 << (linkNum * 3));
        value |= delay << (linkNum * 3);
        recDataDelay.write(value);		
        
        for(int i = 1; i < 10; i++) {
            recDataDelay.write(value);
            long delayWordRead = recDataDelay.read();
            if(value != delayWordRead) {
                //DefOut.out.println(getBoard().getName() + " " + getName()  + " FindOptLinksDelayStep1: delayWord written " + delayWord + " readOut " + delayWordRead + "!!!!!!!!!!!!!");
                System.out.println(getBoard().getName() + " " + getName()  + " setRecFastDelay: value written " + value + " readOut " + delayWordRead + "!!!!!!!!!!!!!");
            }
            else 
                break;
        }
    }

    private void setRecDelay(long delayWord) throws RemoteException, ServiceException, HardwareDbException {                  
        for(int i = 1; i < 10; i++) {
            getHardwareChip().getItem("VREC_DATA_DELAY.REC_DATA_DELAY").write(delayWord);
            long delayWordRead = getHardwareChip().getItem("VREC_DATA_DELAY.REC_DATA_DELAY").read();
            if(delayWord != delayWordRead) {
                //DefOut.out.println(getBoard().getName() + " " + getName()  + " FindOptLinksDelayStep1: delayWord written " + delayWord + " readOut " + delayWordRead + "!!!!!!!!!!!!!");
                System.out.println(getBoard().getName() + " " + getName()  + " FindOptLinksDelayStep1: delayWord written " + delayWord + " readOut " + delayWordRead + "!!!!!!!!!!!!!");
            }
            else 
                break;
        }       
    }
    
    
    public void FindOptLinksTLKDelay(long testData) throws HardwareDbException, RemoteException, ServiceException {	
        DeviceItemWord recTestData =  (DeviceItemWord)getHardwareChip().getItem("REC_TEST_DATA");
        //for(int iLink = 0; iLink < 3; iLink++) {
        for(OptLink link:  usedOptLinks) {
            int iLink = link.getOptoInputNum();
             //DefOut.out.println(getBoard().getName() + " " + getName()  + " FindOptLinksTLKDelay: linkNum " + iLink + " REC_TEST_DATA ");
            boolean[] recTestsData = new boolean[8];
            for(long del = 0; del < 8; del++) {
                setRecFastDelay(iLink, del);
                boolean ok = true;
                for(int i = 0; i < 5; i++) {					
                    //DefOut.out.println(del + " " + Long.toHexString(recTestsData[(int)del]));
                    long buff = recTestData.read(iLink);
                    if(buff != testData)
                        ok = false;
                }

                recTestsData[(int)del] = ok;	
            }
            int firsGood = -1;
            int lastGood = -1;
            int foundDel = -1;
            if(recTestsData[0]) {
                firsGood = 0;
            }
            for(int del = 0; del < 7; del++) {
                if(recTestsData[del] != true && recTestsData[del+1] == true)
                    firsGood = del + 1;
                else if(recTestsData[del] == true && recTestsData[del+1] != true)
                    lastGood = del;
            }
            if(lastGood == -1 && recTestsData[7] == true ) {
                lastGood = 7;
            }

            if(firsGood != -1 && lastGood != -1) {
                if(firsGood == 0 && lastGood == 7) {
                    foundDel = 3;					
                    DefOut.out.println(getBoard().getName() + " " + getName()  + " FindOptLinksTLKDelay: linkNum " + iLink + " foundDel " + foundDel);
                }
                else {
                    if(lastGood >= firsGood)
                        foundDel = (firsGood + lastGood ) / 2;
                    else {
                        foundDel = ((firsGood + lastGood + 8) / 2) % 8;
                    }
                    DefOut.out.println(getBoard().getName() + " " + getName()  + " FindOptLinksTLKDelay: linkNum " + iLink + " foundDel " + foundDel);

                    setRecFastDelay(iLink, foundDel);
                }
            }
            else {				
                DefOut.out.println("FindOptLinksTLKDelay: " + link + ". delay not found, synchronization failed!!! " );
                setRecFastDelay(iLink, 0);
                link.setIsOK(false);
            }
        }
    }

    public void FindOptLinksDelay() throws HardwareDbException, RemoteException, ServiceException, InterruptedException {		
        DeviceItem recDataDelay =  getHardwareChip().getItem("VREC_DATA_DELAY.REC_DATA_DELAY");

        DeviceItemWord recTestOrData =  (DeviceItemWord)getHardwareChip().getItem("REC_TEST_OR_DATA");
        //long[] foundDealys = new long[3];
        for(int iLink = 0; iLink < 3; iLink++) 
            foundDealys[iLink] = -1;
        
        for(int del = 0; del < 8; del++) {
            recDataDelay.write(del | (del<<2) | (del<<4));
            //DefOut.out.println("del " + del);
            for(int iLink = 0; iLink < 3; iLink++) {
                if(isLinkUsed(iLink) == false || foundDealys[iLink] != -1)
                    continue;
                recTestOrData.read(iLink);
                Thread.sleep(500);
                long recTestOr =  recTestOrData.read(iLink);
                //DefOut.out.println("recTestOr " + recTestOr);
                if (recTestOr == 0) {    				
                    DefOut.out.println(getBoard().getName() + " " + getName()  + " FindOptLinkDelay: linkNum " + iLink + " delay "+ del);
                    foundDealys[iLink] = del;
                }
            }				
        }
        for(int iLink = 0; iLink < 3; iLink++) {
            if(isLinkUsed(iLink) == false) {
                foundDealys[iLink] = 0;
                continue;
            }
            if(foundDealys[iLink] == -1) {
                DefOut.out.println(getBoard().getName() + " " + getName()  + " FindOptLinkDelay: linkNum " + iLink + " delay not found, synchronization failed!!!");
                foundDealys[iLink] = 0;
            }
        }

        recDataDelay.write(foundDealys[0] | (foundDealys[1]<<2) | (foundDealys[2]<<4));
    }
    
    public void FindOptLinksDelay1() throws HardwareDbException, RemoteException, ServiceException, InterruptedException {       
        DeviceItem recDataDelay =  getHardwareChip().getItem("VREC_DATA_DELAY.REC_DATA_DELAY");

        DeviceItemWord recTestOrData =  (DeviceItemWord)getHardwareChip().getItem("REC_TEST_OR_DATA");
        //long[] foundDealys = new long[3];
        for(int iLink = 0; iLink < 3; iLink++) 
            foundDealys[iLink] = -1;

        for(int del = 0; del < 8; del++) {
            recDataDelay.write(del | (del<<2) | (del<<4));
            //DefOut.out.println("del " + del);
            for(int iLink = 0; iLink < 3; iLink++) {
                if(isLinkUsed(iLink) == false || foundDealys[iLink] != -1)
                    continue;
                recTestOrData.read(iLink);
            }

            Thread.sleep(500);

            //DefOut.out.println("del " + del);
            for(int iLink = 0; iLink < 3; iLink++) {
                if(isLinkUsed(iLink) == false || foundDealys[iLink] != -1)
                    continue;
                long recTestOr =  recTestOrData.read(iLink);
                //DefOut.out.println("recTestOr " + recTestOr);
                if (recTestOr == 0) {                   
                    DefOut.out.println(getBoard().getName() + " " + getName()  + " FindOptLinkDelay: linkNum " + iLink + " delay "+ del);
                    foundDealys[iLink] = del;
                }
            }               
        }
        for(int iLink = 0; iLink < 3; iLink++) {
            if(isLinkUsed(iLink) == false) {
                foundDealys[iLink] = 0;
                continue;
            }
            if(foundDealys[iLink] == -1) {
                DefOut.out.print(getBoard().getName() + " " + getName()  + " FindOptLinkDelay: linkNum " + iLink + " delay not found, synchronization failed!!!");                                
                for (LinkConn linkConn :  ((TriggerBoard)(getBoard().getDbBoard())).getLinkConns()) {
                    if(linkConn.getOpto() == this.getDbChip() && linkConn.getOptoInputNum() == iLink) {
                        DefOut.out.println(" " + linkConn.getSynCoder().getBoard().getName());        
                    }
                }
                
                foundDealys[iLink] = 0;
                
                usedOptLinksInputsMask &=  ~(1 << iLink);
                DefOut.out.print("link was disabled");
                //getHardwareChip().findByName("TLK.LCK_REFN").write(usedOptLinksInputsMask);
                //getHardwareChip().findByName("TLK.ENABLE").write(7);
                enableOptoLinks(true);
            }
        }

        recDataDelay.write(foundDealys[0] | (foundDealys[1]<<2) | (foundDealys[2]<<4));
    }
    
    
    public void FindOptLinksDelayStep1(int delay) throws RemoteException, ServiceException, HardwareDbException {
        if(delay == 0)
            for(int iLink = 0; iLink < 3; iLink++) 
                foundDealys[iLink] = -1;
        
        DeviceItemWord recTestOrData =  (DeviceItemWord)getHardwareChip().getItem("REC_TEST_OR_DATA");
        
        long delayWord = delay | (delay<<2) | (delay<<4);
        setRecDelay(delayWord);
        
        
        //DefOut.out.println("del " + del);
        for(int iLink = 0; iLink < 3; iLink++) {
            if(isLinkUsed(iLink) == false || foundDealys[iLink] != -1)
                continue;
            recTestOrData.read(iLink);
        }
    }
    
    public void FindOptLinksDelayStep1(int lbDelay, int delay) throws RemoteException, ServiceException, InterruptedException, HardwareDbException {       
        DeviceItemWord recTestOrData = (DeviceItemWord)getHardwareChip().getItem("REC_TEST_OR_DATA");
        
        long delayWord = delay | (delay<<2) | (delay<<4);
        setRecDelay(delayWord);
        
        for(OptLink link:  usedOptLinks) {
            if(link.isOK() == false || link.getLbOptoDealysMap()[lbDelay] != -1)//link.getLbOptoDealysMap()[lbDelay] != -1  means that the delay was found already
                continue;
            
            //Thread.sleep(5);
            recTestOrData.read(link.getOptoInputNum());
            //recTestOrData.read(link.getOptoInputNum());
        }
        
/*        long delayWordRead = getHardwareChip().getItem("VREC_DATA_DELAY.REC_DATA_DELAY").read();
        if(delayWord != delayWordRead) {
            DefOut.out.println(getBoard().getName() + " " + getName()  + " FindOptLinksDelayStep1: delayWord written " + delayWord + " readOut " + delayWordRead + "!!!!!!!!!!!!!");
            System.out.println(getBoard().getName() + " " + getName()  + " FindOptLinksDelayStep1: delayWord written " + delayWord + " readOut " + delayWordRead + "!!!!!!!!!!!!!");
        }*/
    }
    
    public void FindOptLinksDelayStep2(int delay) throws RemoteException, ServiceException, HardwareDbException {
        DeviceItemWord recTestOrData =  (DeviceItemWord)getHardwareChip().getItem("REC_TEST_OR_DATA");
        
        for(int iLink = 0; iLink < 3; iLink++) {
            if(isLinkUsed(iLink) == false || foundDealys[iLink] != -1)
                continue;
            long recTestOr =  recTestOrData.read(iLink);
            //DefOut.out.println("recTestOr " + recTestOr);
            if (recTestOr == 0) {                   
                DefOut.out.print(getBoard().getName() + " " + getName()  + " FindOptLinkDelay: linkNum " + iLink + " delay "+ delay);
                 for (LinkConn linkConn :  ((TriggerBoard)(getBoard().getDbBoard())).getLinkConns()) {
                    if(linkConn.getOpto() == this.getDbChip() && linkConn.getOptoInputNum() == iLink) {
                        DefOut.out.println(" " + linkConn.getSynCoder().getBoard().getName() + " tbInput " + linkConn.getTriggerBoardInputNum());        
                    }
                }
                foundDealys[iLink] = delay;
            }
        } 
    }
    
    public void FindOptLinksDelayStep2(int lbDelay, int delay) throws RemoteException, ServiceException, HardwareDbException {
        DeviceItemWord recTestOrData =  (DeviceItemWord)getHardwareChip().getItem("REC_TEST_OR_DATA");       
        for(OptLink link:  usedOptLinks) {
            if(link.isOK() == false || link.getLbOptoDealysMap()[lbDelay] != -1)
                continue;
            long recTestOr =  recTestOrData.read(link.getOptoInputNum());
            //DefOut.out.println("recTestOr " + recTestOr);
            if (recTestOr == 0) {                   
               // DefOut.out.println("FindOptLinkDelay: " + link  +". lbDelay " + lbDelay + " optoDelay "+ delay);                
                link.getLbOptoDealysMap()[lbDelay] = delay;
            }
        }
          
/*        long delayWord = delay | (delay<<2) | (delay<<4);
        long delayWordRead = getHardwareChip().getItem("VREC_DATA_DELAY.REC_DATA_DELAY").read();
        if(delayWord != delayWordRead) {
            DefOut.out.println(getBoard().getName() + " " + getName()  + " FindOptLinksDelayStep2: delayWord written " + delayWord + " readOut " + delayWordRead + "!!!!!!!!!!!!!");
            System.out.println(getBoard().getName() + " " + getName()  + " FindOptLinksDelayStep2: delayWord written " + delayWord + " readOut " + delayWordRead + "!!!!!!!!!!!!!");
        }*/
    }

    public void FindOptLinksDelayStep3() throws HardwareDbException, RemoteException, ServiceException {
        for(int iLink = 0; iLink < 3; iLink++) {
            if(isLinkUsed(iLink) == false) {
                foundDealys[iLink] = 0;
                continue;
            }
            if(foundDealys[iLink] == -1) {                
                DefOut.out.print(getBoard().getName() + " " + getName()  + " FindOptLinkDelay: linkNum " + iLink + " delay not found, synchronization failed!!!");                
                for (LinkConn linkConn :  ((TriggerBoard)(getBoard().getDbBoard())).getLinkConns()) {
                    if(linkConn.getOpto() == this.getDbChip() && linkConn.getOptoInputNum() == iLink) {
                        DefOut.out.println(" " + linkConn.getSynCoder().getBoard().getName() + " tbInput " + linkConn.getTriggerBoardInputNum());        
                    }
                }
                
                foundDealys[iLink] = 0;
                usedOptLinksInputsMask &=  ~(1 << iLink);
                DefOut.out.println("link was disabled");               
                enableOptoLinks(true); //applies usedOptLinksInputsMask               
            }
            
            getHardwareChip().getItem("REC_CHECK_DATA_ENA").write(usedOptLinksInputsMask);
            getHardwareChip().getItem("REC_CHECK_ENA").write(usedOptLinksInputsMask);
            getHardwareChip().getItem("REC_TEST_ENA").write((~usedOptLinksInputsMask) & 7);
        }

        //getHardwareChip().getItem("VREC_DATA_DELAY.REC_DATA_DELAY").write(foundDealys[0] | (foundDealys[1]<<2) | (foundDealys[2]<<4));
        
        long delayWord = foundDealys[0] | (foundDealys[1]<<2) | (foundDealys[2]<<4);
        setRecDelay(delayWord);
    }
    
    
    public void FindOptLinksDelayStep3a(SortedSet<String> failedLinks ) throws HardwareDbException, RemoteException, ServiceException {             
        //long[] foundDealys = new long[3];
        long delay = 0;
        for(OptLink link:  usedOptLinks) {
            if(link.getFoundDelay() == -1) {                
                DefOut.out.println("FindOptLinkDelay: " + link + ". delay not found, synchronization failed!!!");       
                failedLinks.add(link.toString());
                
                foundDealys[link.getOptoInputNum()] = 0;
                usedOptLinksInputsMask &=  ~(1 << link.getOptoInputNum());
                DefOut.out.println("link was disabled");               
                enableOptoLinks(true); //applies usedOptLinksInputsMask                       
            }
            else {
                foundDealys[link.getOptoInputNum()] = link.getFoundDelay();
                delay |= (link.getFoundDelay() << (link.getOptoInputNum() * 2));
                DefOut.out.println("FindOptLinkDelay: " + link + ". found optoDealy: " + link.getFoundDelay()); 
            }            
        }

        getHardwareChip().getItem("REC_CHECK_DATA_ENA").write(usedOptLinksInputsMask);
        getHardwareChip().getItem("REC_CHECK_ENA").write(usedOptLinksInputsMask);
        getHardwareChip().getItem("REC_TEST_ENA").write((~usedOptLinksInputsMask) & 7);
                
        //getHardwareChip().getItem("VREC_DATA_DELAY.REC_DATA_DELAY").write(delay);
        
        long delayWord = delay;
        setRecDelay(delayWord);
    }
    /*	public void enableTransmissionCheck(boolean enableBCNcheck, boolean enableDataCheck) throws RemoteException, ServiceException {
		boolean checkDataEna = enableBCNcheck;
		boolean checkEna = enableDataCheck;

	    if(checkDataEna) {
	        getHardwareChip().findByName("REC_CHECK_DATA_ENA").write(usedOptLinksInputsMask);
	    }   
	    else {
	    	getHardwareChip().findByName("REC_CHECK_DATA_ENA").write(0);  
	    }   

	    if(checkEna) {  
	    	getHardwareChip().findByName("REC_CHECK_ENA").write(usedOptLinksInputsMask);
	    }
	    else {
	    	getHardwareChip().findByName("REC_CHECK_ENA").write(0);
	    }           
	}*/
}
