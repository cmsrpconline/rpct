package rpct.xdaqaccess.hardwaredb;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.xdaqaccess.Crate;

public class HardwareDbCrateImpl extends HardwareDbItemImpl implements
        HardwareDbCrate {
    
    private Crate hardCrate;
    private rpct.db.domain.equipment.Crate crate;
    private List<HardwareDbBoard> boards;
    private HardwareDbBoard[] boardsArray;
    
    public HardwareDbCrateImpl(Crate hardCrate, rpct.db.domain.equipment.Crate crate, HardwareDbMapper hardwareDbMapper) {
        super(hardCrate, crate, hardwareDbMapper);
        this.hardCrate = hardCrate;
        this.crate = crate;
    }

    private void buildBoardsCollections() throws HardwareDbException {
        if (boards != null || boardsArray != null) {
            throw new IllegalStateException("board collection already initialized");
        }
        
        Board[] dbBoards = crate.getBoardArray();
        
        boards = new ArrayList<HardwareDbBoard>(crate.getBoards().size());        
        boardsArray = new HardwareDbBoard[dbBoards.length];
        try {
            for (int i = 0; i < dbBoards.length; i++) {
                Board board = dbBoards[i];
                if (board != null && board.getDisabled() == null 
                		&& board.getType() != BoardType.CONTROLBOARD  //CONTROLBOARD is no tan II board, is not given by the XDAQs
                		&& board.getType() != BoardType.RBCBOARD) {//RBCBOARD is no tan II board, is not given by the XDAQs
                    HardwareDbBoard hardwareDbBoard = hardwareDbMapper.getBoard(board);
                    boards.add(hardwareDbBoard);
                    boardsArray[i] = hardwareDbBoard;
                }                
            }
        } catch (DataAccessException e) {
            boards = null;
            boardsArray = null;
            throw new HardwareDbException(e);
        } catch (RemoteException e) {
            boards = null;
            boardsArray = null;
            throw new HardwareDbException(e);
        } catch (ServiceException e) {
            boards = null;
            boardsArray = null;
            throw new HardwareDbException(e);
        }
    }
    
    public List<HardwareDbBoard> getBoards() throws HardwareDbException {
        if (boards == null) {
            buildBoardsCollections();
        }
        return boards;
    }

    public HardwareDbBoard[] getBoardsArray() throws HardwareDbException {
        if (boardsArray == null) {
            buildBoardsCollections();
        }
        return boardsArray;
    }

    public Crate getHardwareCrate() {
        return hardCrate;
    }

    public rpct.db.domain.equipment.Crate getDbCrate() {
        return crate;
    }
    
    public static class Factory implements HardwareDbCrateFactory {
        public HardwareDbCrate createHardwareDbCrate(Crate hardCrate,
                rpct.db.domain.equipment.Crate crate, HardwareDbMapper hardwareDbMapper) {
            return new HardwareDbCrateImpl(hardCrate, crate, hardwareDbMapper);
        }        
    }

	public String getFullName() {
		return getName();
	}
}
