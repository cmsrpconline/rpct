package rpct.xdaqaccess.hardwaredb;

import java.util.ArrayList;
import java.util.List;

import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TtuBoard;
import rpct.xdaqaccess.Board;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.synchro.LVDSLink;

public class HardwareDBTTUBoard extends HardwareDbBoardImpl {
	private Device control;
	
	private List<HardwareDbOpto> optos;     
    //HardwareDbChip ttuTrig;
    private List<HardwareDbChip> ttuTrigs;
    //private int usedPacksMask;

    public HardwareDBTTUBoard(Board hardBoard,
            rpct.db.domain.equipment.Board board,
            HardwareDbMapper hardwareDbMapper) {
        super(hardBoard, board, hardwareDbMapper);     
    }

    boolean chipsCollectionBuiled = false;
    private void buildChipsCollections() throws HardwareDbException {
        if(chipsCollectionBuiled)
            return;
        optos = new ArrayList<HardwareDbOpto>();
        ttuTrigs = new ArrayList<HardwareDbChip>();
        
        for(HardwareDbChip hdbChip : getChips()) {
            if(hdbChip.getDbChip().getType() == ChipType.TTUOPTO) {
                optos.add((HardwareDbOpto)hdbChip);
            }
            else if(hdbChip.getDbChip().getType() == ChipType.TTUTRIG) {
            	ttuTrigs.add(hdbChip);
            }                    
        }  
        
        for (Device device : getHardwareBoard().getDevices()) {
			if (device.getType().equals("TTUCONTROL") == true) {
				control = device;
				break;
			}
        }
        
        chipsCollectionBuiled = true;
    }

    public rpct.db.domain.equipment.TtuBoard getDBTtuBoard() {
        return (rpct.db.domain.equipment.TtuBoard)getDbBoard();
    }

    public Device getControlChip() throws HardwareDbException {
    	buildChipsCollections();
    	return control;
    }

    public List<HardwareDbOpto> getOptos() throws HardwareDbException {
        buildChipsCollections();
        return optos;
    }

    public List<HardwareDbChip> getTTUTrigs() throws HardwareDbException {
        buildChipsCollections();
        return ttuTrigs;
    }

    public List<HardwareDbMuxRecChip> getMuxRecChips() throws HardwareDbException {
        List<HardwareDbMuxRecChip> muxRecChips = new ArrayList<HardwareDbMuxRecChip>();
        for(HardwareDbChip chip : getTTUTrigs()) {
            muxRecChips.add((HardwareDbMuxRecChip)chip);
        }
        return muxRecChips;
    }
    
    
    private List<LVDSLink> lvdsLinks = new ArrayList<LVDSLink>();
    
    public List<LVDSLink> getLVDSLinks() throws Exception {
        if(lvdsLinks.size() != 0) { 
            return lvdsLinks;
        }
        for(HardwareDbChip pac : getTTUTrigs()) {
            for(HardwareDbChip opto : getOptos()) {
                for(int iLinkNum = 0; iLinkNum < 2; iLinkNum++) {
                    lvdsLinks.add(new LVDSLink(((HardwareDbMuxTransChip)opto).getTransmDevice(), 
                            ((HardwareDbMuxTransChip)pac).getTransmDevice(), 
                            2 * TtuBoard.getChipNum(opto.getDbChip()) + iLinkNum));
                }
            }
        }

        return lvdsLinks;
    }
    
    public static class Factory implements HardwareDbBoardFactory {
        public HardwareDbBoard createHardwareDbBoard(Board hardBoard,
                rpct.db.domain.equipment.Board board, HardwareDbMapper hardwareDbMapper) {
            return new HardwareDBTTUBoard(hardBoard, board, hardwareDbMapper);
        }        
    }
}
