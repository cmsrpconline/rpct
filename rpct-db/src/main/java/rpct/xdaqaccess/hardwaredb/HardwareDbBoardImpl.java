package rpct.xdaqaccess.hardwaredb;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.equipment.Chip;
import rpct.xdaqaccess.Board;

public class HardwareDbBoardImpl extends HardwareDbItemImpl implements
        HardwareDbBoard {
    private Board hardBoard;
    private rpct.db.domain.equipment.Board board;
    private List<HardwareDbChip> chips;
    private HardwareDbChip[] chipsArray;
    private HardwareDbCrate crate;

    public HardwareDbBoardImpl(Board hardBoard, rpct.db.domain.equipment.Board board, HardwareDbMapper hardwareDbMapper) {
        super(hardBoard, board, hardwareDbMapper);
        this.hardBoard = hardBoard;
        this.board = board;
    }
    
    private void buildChipsCollections() throws HardwareDbException {
        if (chips != null || chipsArray != null) {
            throw new IllegalStateException("chip collection already initialized");
        }
        
        Chip[] dbChips = board.getChipsArray();
        
        chips = new ArrayList<HardwareDbChip>(board.getChips().size());        
        chipsArray = new HardwareDbChip[dbChips.length];
        try {
            for (int i = 0; i < dbChips.length; i++) {
                Chip chip = dbChips[i];
                if (chip != null && chip.getDisabled() == null) {
                    HardwareDbChip hardwareDbChip = hardwareDbMapper.getChip(chip);
                    chips.add(hardwareDbChip);
                    chipsArray[i] = hardwareDbChip;
                }                
            }
        } catch (DataAccessException e) {
            chips = null;
            chipsArray = null;
            throw new HardwareDbException(e);
        } catch (RemoteException e) {
            chips = null;
            chipsArray = null;
            throw new HardwareDbException(e);
        } catch (ServiceException e) {
            chips = null;
            chipsArray = null;
            throw new HardwareDbException(e);
        }
    }
        
    public List<HardwareDbChip> getChips() throws HardwareDbException {
        if (chips == null) {      
            buildChipsCollections();
        }
        return chips;
    }

    public HardwareDbChip[] getChipsArray() throws HardwareDbException {
        if (chipsArray == null) {      
            buildChipsCollections();
        }
        return chipsArray;        
    }

    public HardwareDbCrate getCrate() throws HardwareDbException {
        if (crate == null) {
            try {
                crate = hardwareDbMapper.getCrate(board.getCrate());
            } catch (DataAccessException e) {
                throw new HardwareDbException(e);
            } catch (RemoteException e) {
                throw new HardwareDbException(e);
            } catch (ServiceException e) {
                throw new HardwareDbException(e);
            }
        }
        return crate;
    }    

    public rpct.db.domain.equipment.Board getDbBoard() {
        return board;        
    }

    public Board getHardwareBoard() {
        return hardBoard;
    }
    
    public static class Factory implements HardwareDbBoardFactory {
        public HardwareDbBoard createHardwareDbBoard(Board hardBoard,
                rpct.db.domain.equipment.Board board, HardwareDbMapper hardwareDbMapper) {
            return new HardwareDbBoardImpl(hardBoard, board, hardwareDbMapper);
        }        
    }

	public String getFullName()  {
		return (new StringBuilder(board.getCrate().getName()))
		.append('/').append(getName()).toString();
	}
}
