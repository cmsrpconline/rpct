package rpct.xdaqaccess.hardwaredb;

public class HardwareDbException extends Exception {
    private static final long serialVersionUID = 8498987577274546813L;

    public HardwareDbException() {
    }

    public HardwareDbException(String message) {
        super(message);
    }

    public HardwareDbException(Throwable cause) {
        super(cause);
    }

    public HardwareDbException(String message, Throwable cause) {
        super(message, cause);
    }

}
