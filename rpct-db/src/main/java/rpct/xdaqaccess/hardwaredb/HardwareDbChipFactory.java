package rpct.xdaqaccess.hardwaredb;

import rpct.db.domain.equipment.Chip;
import rpct.xdaqaccess.Device;

public interface HardwareDbChipFactory {
    HardwareDbChip createHardwareDbChip(Device device, Chip chip, HardwareDbMapper hardwareDbMapper);
}
