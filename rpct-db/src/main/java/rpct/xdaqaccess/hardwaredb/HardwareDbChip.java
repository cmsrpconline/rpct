package rpct.xdaqaccess.hardwaredb;

import rpct.db.domain.equipment.Chip;
import rpct.xdaqaccess.Device;

public interface HardwareDbChip extends HardwareDbItem {
    HardwareDbBoard getBoard() throws HardwareDbException;
    Device getHardwareChip();
    Chip getDbChip();
}
