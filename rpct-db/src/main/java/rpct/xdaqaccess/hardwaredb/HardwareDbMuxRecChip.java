package rpct.xdaqaccess.hardwaredb;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import rpct.datastream.RpctDelays;
import rpct.db.domain.configuration.HalfSortConf;
import rpct.db.domain.configuration.IntArray16;
import rpct.db.domain.configuration.IntArray18;
import rpct.db.domain.configuration.IntArray4;
import rpct.db.domain.configuration.IntArray9;
import rpct.db.domain.configuration.PacConf;
import rpct.db.domain.configuration.RmbConf;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.configuration.TbGbSortConf;
import rpct.db.domain.configuration.TcSortConf;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.DeviceItemWord;

/**
 * @author Karol Bunkowski the class for the chip, which is reciver in the fast
 *         multiplexed lvds tranmsission
 */
public class HardwareDbMuxRecChip extends HardwareDbMuxTransChip {
    // RMB configuration
    private static int chanEna = 0; // 0 = all channels enable
    private static int preTriggerVal = RpctDelays.PRE_TRIGGER_VAL;
    private static int postTriggerVal = RpctDelays.POST_TRIGGER_VAL;
    private static int daqDataDelay = RpctDelays.RMB_DATA_DELAY;
    private static int trgDelay = 0;

    public HardwareDbMuxRecChip(Device device, Chip chip,
            HardwareDbMapper hardwareDbMapper) {
        super(device, chip, hardwareDbMapper);
        // TODO Auto-generated constructor stub
    }

    public StaticConfiguration createConfigurationFromCurrentSettings()
            throws RemoteException, ServiceException {
        if (getDbChip().getType() == ChipType.PAC) {
            byte[] recMuxClkInv = getHardwareChip().getItem(
                    "REC_MUX_CLK_INV").readBinary().getBytes();
            byte[] recMuxClk90 = getHardwareChip().getItem("REC_MUX_CLK90")
                    .readBinary().getBytes();
            byte[] recMuxRegAdd = getHardwareChip().getItem(
                    "REC_MUX_REG_ADD").readBinary().getBytes();
            IntArray18 recMuxDelay = new IntArray18();
            IntArray18 recDataDelay = new IntArray18();

            DeviceItemWord muxDelay = ((DeviceItemWord) getHardwareChip()
                    .getItem("REC_MUX_DELAY"));
            for (int i = 0; i < muxDelay.getDesc().getNumber(); i++)
                recMuxDelay.setValue(i, (int) muxDelay.read(i));

            DeviceItemWord dataDelay = ((DeviceItemWord) getHardwareChip()
                    .getItem("REC_DATA_DELAY"));
            for (int i = 0; i < dataDelay.getDesc().getNumber(); i++)
                recDataDelay.setValue(i, (int) dataDelay.read(i));
            
            int bxOfCoincidence = 0;//getHardwareChip().getItem("STATUS.PAC_DATA_OR").readBinary().intValue();
            int pacEna = 0xfff;//getHardwareChip().getItem("WORD_PAC_ENA").readBinary().intValue();
            int pacConfigId = getHardwareChip().getItem("MPAC_CFG_ID").readBinary().intValue();
            
            PacConf conf = new PacConf(recMuxClkInv, recMuxClk90, recMuxRegAdd,
                    recMuxDelay, recDataDelay, bxOfCoincidence, pacEna, pacConfigId);  

            return conf;
        } else if (getDbChip().getType() == ChipType.RMB) {
            byte[] recMuxClkInv = getHardwareChip().getItem(
                    "REC_MUX_CLK_INV").readBinary().getBytes();
            byte[] recMuxClk90 = getHardwareChip().getItem("REC_MUX_CLK90")
                    .readBinary().getBytes();
            byte[] recMuxRegAdd = getHardwareChip().getItem(
                    "REC_MUX_REG_ADD").readBinary().getBytes();
            IntArray18 recMuxDelay = new IntArray18();
            IntArray18 recDataDelay = new IntArray18();

            DeviceItemWord muxDelay = ((DeviceItemWord) getHardwareChip()
                    .getItem("REC_MUX_DELAY"));
            for (int i = 0; i < muxDelay.getDesc().getNumber(); i++)
                recMuxDelay.setValue(i, (int) muxDelay.read(i));

            DeviceItemWord dataDelay = ((DeviceItemWord) getHardwareChip()
                    .getItem("REC_DATA_DELAY"));
            for (int i = 0; i < dataDelay.getDesc().getNumber(); i++)
                recDataDelay.setValue(i, (int) dataDelay.read(i));

            RmbConf conf = new RmbConf(recMuxClkInv, recMuxClk90, recMuxRegAdd,
                    recMuxDelay, recDataDelay, chanEna, preTriggerVal,
                    postTriggerVal, daqDataDelay, trgDelay);

            return conf;
        } else if (getDbChip().getType() == ChipType.GBSORT) {
            byte[] recMuxClkInv = getHardwareChip().getItem(
                    "REC_MUX_CLK_INV").readBinary().getBytes();
            byte[] recMuxClk90 = getHardwareChip().getItem("REC_MUX_CLK90")
                    .readBinary().getBytes();
            byte[] recMuxRegAdd = getHardwareChip().getItem(
                    "REC_MUX_REG_ADD").readBinary().getBytes();
            IntArray4 recMuxDelay = new IntArray4();
            IntArray4 recDataDelay = new IntArray4();

            DeviceItemWord muxDelay = ((DeviceItemWord) getHardwareChip()
                    .getItem("REC_MUX_DELAY"));
            for (int i = 0; i < muxDelay.getDesc().getNumber(); i++)
                recMuxDelay.setValue(i, (int) muxDelay.read(i));

            DeviceItemWord dataDelay = ((DeviceItemWord) getHardwareChip()
                    .getItem("REC_DATA_DELAY"));
            for (int i = 0; i < dataDelay.getDesc().getNumber(); i++)
                recDataDelay.setValue(i, (int) dataDelay.read(i));

            TbGbSortConf conf = new TbGbSortConf(recMuxClkInv, recMuxClk90,
                    recMuxRegAdd, recMuxDelay, recDataDelay);

            return conf;
        } else if (getDbChip().getType() == ChipType.TCSORT) {
            byte[] recMuxClkInv = getHardwareChip().getItem(
                    "REC_MUX_CLK_INV").readBinary().getBytes();
            byte[] recMuxClk90 = getHardwareChip().getItem("REC_MUX_CLK90")
                    .readBinary().getBytes();
            byte[] recMuxRegAdd = getHardwareChip().getItem(
                    "REC_MUX_REG_ADD").readBinary().getBytes();
            IntArray9 recMuxDelay = new IntArray9();
            IntArray9 recDataDelay = new IntArray9();

            DeviceItemWord muxDelay = ((DeviceItemWord) getHardwareChip()
                    .getItem("REC_MUX_DELAY"));
            for (int i = 0; i < muxDelay.getDesc().getNumber(); i++)
                recMuxDelay.setValue(i, (int) muxDelay.read(i));

            DeviceItemWord dataDelay = ((DeviceItemWord) getHardwareChip()
                    .getItem("REC_DATA_DELAY"));
            for (int i = 0; i < dataDelay.getDesc().getNumber(); i++)
                recDataDelay.setValue(i, (int) dataDelay.read(i));

            TcSortConf conf = new TcSortConf(recMuxClkInv, recMuxClk90,
                    recMuxRegAdd, recMuxDelay, recDataDelay, "");

            return conf;
        } 
        else if (getDbChip().getType() == ChipType.HALFSORTER) {
            byte[] recChanEna = new byte[2];
            recChanEna[0] = Byte.valueOf("-3f", 16);
            recChanEna[1] = Byte.valueOf("-3f", 16);
            
            IntArray16 recFastClkInv = new IntArray16();
            IntArray16 recFastClk90 = new IntArray16();            
            IntArray16 recFastRegAdd = new IntArray16();
            
            DeviceItemWord muxClkInv = ((DeviceItemWord) getHardwareChip().getItem("REC_FAST_CLK_INV"));
            DeviceItemWord muxClk90 = ((DeviceItemWord) getHardwareChip().getItem("REC_FAST_CLK90"));
            if(muxClk90 == null) {
                System.out.println("Warning: muxClk90 == null !!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            }
            DeviceItemWord muxRegAdd = ((DeviceItemWord) getHardwareChip().getItem("REC_FAST_REG_ADD"));
            
            for (int iChan = 0; iChan < muxRegAdd.getNumber(); iChan++) {
                if(muxClkInv != null) {
                    recFastClkInv.setValue(iChan, (int) muxClkInv.read(iChan));
                }
                if(muxClk90 != null) {
                    recFastClk90.setValue(iChan, (int) muxClk90.read(iChan));
                }
                recFastRegAdd.setValue(iChan, (int) muxRegAdd.read(iChan));
            }      

            byte[] recFastDataDelay = getHardwareChip().getItem("REC_FAST_DATA_DELAY").readBinary().getBytes();
            //byte[] recFastDataDelay = {0,0};
            //System.out.println("REC_FAST_DATA_DELAY not written!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            
            //byte[] recDataDelay = getHardwareChip().findByName("REC_DATA_DELAY").readBinary().getBytes();
            byte[] recDataDelay = {0,0,0,0};
            
            HalfSortConf conf = new HalfSortConf(recChanEna, recFastClkInv, recFastClk90, 
                    recFastRegAdd, recFastDataDelay, recDataDelay);
            return conf;
        } else {
            throw new IllegalArgumentException(
                    "the chip cannot return the configuration");
        }
    }

    public static class Factory implements HardwareDbChipFactory {
        public HardwareDbChip createHardwareDbChip(Device device, Chip chip,
                HardwareDbMapper hardwareDbMapper) {
            return new HardwareDbMuxRecChip(device, chip, hardwareDbMapper);
        }
    }
}
