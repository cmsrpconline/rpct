package rpct.xdaqaccess.hardwaredb;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.DeviceItem;
import rpct.xdaqaccess.DeviceItemWord;

public class HardwareDbChipImpl extends HardwareDbItemImpl implements
        HardwareDbChip {    

    private Device device;
    private Chip chip;
    private HardwareDbBoard board;

    public HardwareDbChipImpl(Device device, Chip chip, HardwareDbMapper hardwareDbMapper) {
        super(device, chip, hardwareDbMapper);
        this.device = device;
        this.chip = chip;
    }
    
    public HardwareDbBoard getBoard() throws HardwareDbException {
        if (board == null) {
            try {
                board = hardwareDbMapper.getBoard(chip.getBoard());
            } catch (DataAccessException e) {
                throw new HardwareDbException(e);
            } catch (RemoteException e) {
                throw new HardwareDbException(e);
            } catch (ServiceException e) {
                throw new HardwareDbException(e);
            }
        }
        return board;
    } 

    public Device getHardwareChip() {
        return device;
    }  

    public Chip getDbChip() {
        return chip;
    }     
    
    public void enableSendTransCheck(boolean enableBCNcheck, boolean enableDataCheck) throws RemoteException, ServiceException {
    	DeviceItem reg;
        if(enableBCNcheck) {                
            reg = device.getItem("SEND_CHECK_DATA_ENA");
            if(reg != null)
            	reg.write((1<<reg.getDesc().getWidth()) - 1);  
        }
        else {                
            reg = device.getItem("SEND_CHECK_DATA_ENA");
            if(reg != null)
                reg.write(0);
        }
        
        if(enableDataCheck) {
            reg = device.getItem("SEND_CHECK_ENA");
            if(reg != null)
            	reg.write((1<<reg.getDesc().getWidth()) - 1); 
        }
        else {
            reg = device.getItem("SEND_CHECK_ENA");
            if(reg != null)
                reg.write(0);
        }
    }
    
    public void enableRecTransCheck(boolean enableBCNcheck, boolean enableDataCheck) throws RemoteException, ServiceException, HardwareDbException {
    	DeviceItem reg;
    	long inputMask = 0;
    	if(chip.getType() == ChipType.OPTO) {
    		inputMask = ((HardwareDbOpto)this).getUsedOptLinksInputsMask();
    	}
    	else if(chip.getType() == ChipType.PAC) {
    		inputMask = 0x3ffff;
    	}
    	else if(chip.getType() == ChipType.RMB) {
    		inputMask = 0x3ffff;
    	}
    	else if(chip.getType() == ChipType.GBSORT) {
    		inputMask = ((HardwareDBTriggerBoard)getBoard()).getUsedPacMask();
    	}
    	else if(chip.getType() == ChipType.TCSORT) {
    		inputMask = ((HardwareDbTriggerCrate)getBoard().getCrate()).getUsedTBsMask();
    	}
    	
        if(enableBCNcheck) {                
            reg = device.getItem("REC_CHECK_DATA_ENA");
            if(reg != null)
            	reg.write(inputMask);  
        }
        else {                
            reg = device.getItem("REC_CHECK_DATA_ENA");
            if(reg != null)
                reg.write(0);
        }
        
        if(enableDataCheck) {
            reg = device.getItem("REC_CHECK_ENA");
            if(reg != null)
            	reg.write(inputMask); 
        }
        else {
            reg = device.getItem("REC_CHECK_ENA");
            if(reg != null)
                reg.write(0);
        }
    }
    
    public void enableTransmissionCheck(boolean enableBCNcheck, boolean enableDataCheck) throws RemoteException, ServiceException, HardwareDbException {
    	enableSendTransCheck(enableBCNcheck, enableDataCheck);
    	enableRecTransCheck(enableBCNcheck, enableDataCheck);
    }
    
    public void resetRecErrorCnt() throws RemoteException, ServiceException {
    	DeviceItem reg;
    	reg = device.getItem("REC_ERROR_COUNT");
    	if(reg != null) {
    		reg.write(0);
    		
            DeviceItemWord recTestOrData = (DeviceItemWord)device.getItem("REC_TEST_OR_DATA");
            for(int i = 0; i < recTestOrData.getNumber(); i++) {
                recTestOrData.read(i);
            }
    	}

    	if(device.getType().equals("TB3RMB")) {
    		reg = device.getItem("STATUS.RMB_CNT_RESET");
    		reg.write(1);	
    		reg.write(0);
    	}
    } 
          
    public long readErrorCnt(StringBuilder msg) throws RemoteException, ServiceException {
        long recErrorCnt = 0;
        DeviceItem reg;
        reg = device.getItem("REC_ERROR_COUNT");
        if(reg != null) {
            recErrorCnt= reg.read() ;
            msg.append("REC_ERROR_COUNT " + recErrorCnt + "\n") ;
            //"REC_ERROR_COUNT " + recErrorCnt + "\n";
                
            DeviceItemWord recTestOrData = (DeviceItemWord)device.getItem("REC_TEST_OR_DATA");
            for(int i = 0; i < recTestOrData.getNumber(); i++) {
                msg.append(" REC_TEST_OR_DATA " + i + " " + recTestOrData.read(i) + "\n") ;
                //msg += " REC_TEST_OR_DATA " + i + " " + recTestOrData.read(i) + "\n";
            }
        }
        
/*        if(device.getType().equals("TB3RMB")) {
            reg = device.getItem("STATUS.RMB_CNT_RESET");
            reg.write(1);   
            reg.write(0);
        }*/
        
        return recErrorCnt;
    } 
	
    public static class Factory implements HardwareDbChipFactory {
        public HardwareDbChip createHardwareDbChip(Device device, Chip chip, HardwareDbMapper hardwareDbMapper) {
            return new HardwareDbChipImpl(device, chip, hardwareDbMapper);
        }
    }

	public String getFullName() {
		return (new StringBuilder(chip.getBoard().getCrate().getName()))
		.append('/').append(chip.getBoard().getName())
		.append('/').append(getName()).toString();
	}
}
