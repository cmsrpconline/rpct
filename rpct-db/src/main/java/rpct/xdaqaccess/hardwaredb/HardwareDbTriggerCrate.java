package rpct.xdaqaccess.hardwaredb;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.TcHsbConnection;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.xdaqaccess.Crate;
import rpct.xdaqaccess.synchro.LVDSLink;

public class HardwareDbTriggerCrate extends HardwareDbCrateImpl implements CrateWithLVDSLinks {
	private ArrayList<HardwareDBTriggerBoard> hardwareDBTriggerBoards;
	
	private ArrayList<HardwareDBTTUBoard> hardwareDBTTUBoards;
    
    private HardwareDbBoard tcBackplane;
    
    private HardwareDbChip tcSort;
    
    private int usedTbMask;
    
    private static int usedTCsMask; 
    
    public HardwareDbTriggerCrate(Crate hardCrate,
            rpct.db.domain.equipment.Crate crate,
            HardwareDbMapper hardwareDbMapper) {
        super(hardCrate, crate, hardwareDbMapper);
        
        usedTCsMask |= (1 << getDbTriggerCrate().getLogSector());
    }

    public TriggerCrate getDbTriggerCrate() {
        return (TriggerCrate)getDbCrate();
    }       
    
    private boolean boardsCollectionsBuild = false;
    
    private void buildBoardsCollections() throws HardwareDbException {
        if(boardsCollectionsBuild)
            return;
        hardwareDBTriggerBoards = new ArrayList<HardwareDBTriggerBoard>();
        hardwareDBTTUBoards = new ArrayList<HardwareDBTTUBoard>();
        for(HardwareDbBoard board : getBoards()) {
            if(board.getDbBoard().getType() == BoardType.TRIGGERBOARD) {
                hardwareDBTriggerBoards.add((HardwareDBTriggerBoard)board);
                usedTbMask |= (1 << ((HardwareDBTriggerBoard)board).getDBTriggerBoard().getNum());
            }
            else if(board.getDbBoard().getType() == BoardType.TCBACKPLANE)
                tcBackplane = board;
            else if(board.getDbBoard().getType() == BoardType.TTUBOARD) {
                hardwareDBTTUBoards.add((HardwareDBTTUBoard)board);
            }
        }
        for(HardwareDbChip chip : tcBackplane.getChips()) {
            if(chip.getDbChip().getType() == ChipType.TCSORT)
                tcSort = chip;
        }
        boardsCollectionsBuild = true;
    }
    
    public List<HardwareDBTriggerBoard> getHardwareDBTriggerBoards() throws HardwareDbException {
        buildBoardsCollections();
        return hardwareDBTriggerBoards;
    }
    
    public List<HardwareDBTTUBoard> getHardwareDBTTUBoards() throws HardwareDbException {
        buildBoardsCollections();
        return hardwareDBTTUBoards;
    }
    
    
    private List<LVDSLink> lvdsLinks = new ArrayList<LVDSLink>();
    
    /* (non-Javadoc)
     * @see rpct.xdaqaccess.hardwaredb.CrateWithLVDSLinks#getLVDSLinks()
     */
    public List<LVDSLink> getLVDSLinks() throws Exception {   
        if(lvdsLinks.size() != 0) { 
            return lvdsLinks;
        }
        
        for(HardwareDBTriggerBoard tb : getHardwareDBTriggerBoards()) {
            lvdsLinks.addAll(tb.getLVDSLinks());
        }
        for(HardwareDBTriggerBoard tb : getHardwareDBTriggerBoards()) {
            if(tb.getGbSort() != null) {
                lvdsLinks.add(new LVDSLink(((HardwareDbMuxTransChip)tb.getGbSort()).getTransmDevice(), 
                		((HardwareDbMuxTransChip)getTcSort()).getTransmDevice(), tb.getDBTriggerBoard().getNum()));
            }
        }
        
        for(HardwareDBTTUBoard ttu : getHardwareDBTTUBoards()) {
            lvdsLinks.addAll(ttu.getLVDSLinks());
        }
        
        return lvdsLinks;
    }

    public List<HardwareDbMuxRecChip> getMuxRecChips() throws HardwareDbException {
        List<HardwareDbMuxRecChip> muxRecChips = new ArrayList<HardwareDbMuxRecChip>();
        for(HardwareDBTriggerBoard tb : getHardwareDBTriggerBoards()) {
            muxRecChips.addAll(tb.getMuxRecChips());
        }
        muxRecChips.add((HardwareDbMuxRecChip)getTcSort());
        
        for(HardwareDBTTUBoard ttu : getHardwareDBTTUBoards()) {
            muxRecChips.addAll(ttu.getMuxRecChips());            
        }
        return muxRecChips;
    }
    
    public HardwareDbBoard getTcBackplane() throws HardwareDbException {
        buildBoardsCollections();
        return tcBackplane;
    }

    public HardwareDbChip getTcSort() throws HardwareDbException {
        buildBoardsCollections();
        return tcSort;
    }
    
    public static class Factory implements HardwareDbCrateFactory {
        public HardwareDbCrate createHardwareDbCrate(Crate hardCrate,
                rpct.db.domain.equipment.Crate crate, HardwareDbMapper hardwareDbMapper) {
            return new HardwareDbTriggerCrate(hardCrate, crate, hardwareDbMapper);
        }        
    }

	public int getUsedTBsMask() {
		return usedTbMask;
	}

	public static int getUsedTCsMask() {
		return usedTCsMask;
	}
	
	public List<TcHsbConnection> getTcHsbConnections(EquipmentDAO equipmentDAO) throws DataAccessException, RemoteException, ServiceException { 
	    return getDbTriggerCrate().getTcHsbConnections(equipmentDAO);
	}
}
