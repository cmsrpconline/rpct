package rpct.xdaqaccess;

public class DeviceItemInfo {
    private int id;
    private String name;
    private String description;
    private DeviceItemType type;
    private int width;
    private int number;
    private int parentId;
    private AccessType accessType;
    public DeviceItemInfo(
            int id,
            String name,
            String description,
            DeviceItemType type,
            int width,
            int number,
            int parentId,
            AccessType accessType) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.type = type;
        this.width = width;
        this.number = number;
        //this.parentId = parentId;
        this.accessType = accessType;
    }
    public AccessType getAccessType() {
        return accessType;
    }
    public String getDescription() {
        return description;
    }
    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public int getNumber() {
        return number;
    }
    public int getParentId() {
        return parentId;
    }
    public DeviceItemType getType() {
        return type;
    }
    public int getWidth() {
        return width;
    }
}
