package rpct.xdaqaccess;


import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import rpct.xdaq.axis.Binary;


public interface DeviceItemBits extends DeviceItem {
    DeviceItemVector getVector();

    long read(int index) throws RemoteException, ServiceException;
    void write(int index, long value) throws RemoteException, ServiceException;
    
    Binary readBinary(int index) throws RemoteException, ServiceException;
    void writeBinary(int index, Binary value) throws RemoteException, ServiceException;
}