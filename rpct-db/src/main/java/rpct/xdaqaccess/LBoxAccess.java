package rpct.xdaqaccess;


import rpct.xdaq.XdaqApplicationInfo;
import rpct.xdaq.XdaqException;
import rpct.xdaq.axis.bag.Bag;
import rpct.xdaqaccess.axis.FecStatusResponse;

public interface LBoxAccess {
    XdaqApplicationInfo getXdaqApplicationInfo();
    String getName();
	public String getSelctedConfigKey();
	public void setSelctedConfigKey(String selctedConfigKey);
    void sendConfigure() throws XdaqException;
    void sendSynchronizeLinks() throws XdaqException;    
    FecStatusResponse sendResetFEC() throws XdaqException;
    String sendStartMonitoring() throws XdaqException;
    String sendStopMonitoring() throws XdaqException;
    String sendStartHistoMonitoring() throws XdaqException;
    String sendStopHistoMonitoring() throws XdaqException;
    Bag<Object>[] sendGetMonitoring() throws XdaqException;
    void sendMaskAllChannels() throws XdaqException;
    void sendApplyDBMasks() throws XdaqException;    
    ////////////////////////////////////////// MC - 26 Aug 2009
    void sendMaskNoisyChannels() throws XdaqException;
    void sendResetFSM() throws XdaqException;
    void sendSetupBoards() throws XdaqException;
    void sendLoadFromFlash() throws XdaqException;
    ///////////////////////////////////////////////////////////
	
}
