package rpct.xdaqaccess;


import java.rmi.RemoteException;

import rpct.xdaq.XdaqApplicationInfo;
import rpct.xdaq.XdaqException;

public interface TCAccess {
    XdaqApplicationInfo getXdaqApplicationInfo();
    void sendResetRMBs() throws XdaqException, RemoteException;
    String getName();
}
