package rpct.xdaqaccess.diag;

public interface Muon {

    // 5 bits, 0-31.
    int getPtCode();

    // 3 bits, 0-7.
    int getQuality();

    // 1 bit, 0 - negative, 1 - positive.
    int getSign();

    int getEtaAddress();

    int getPhiAddress();

    // 2 bits,
    // 0 00 - this muon did not kill nothing on the sector edge
    // 1 01 - this muon killed muon from segment 0 (the "right" sector edge), or is in segment 0
    // 2 10 - this muon killed muon from segment 11 (the "left" sector edge), or is in segment 11
    // 3 11 - this muon killed muon from segment 0 and 11 
    int getGBData(); 
    
    int getIndex();
}
