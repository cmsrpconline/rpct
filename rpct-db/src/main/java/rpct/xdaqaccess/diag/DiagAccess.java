/**
 * Created on 2005-05-05
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */

package rpct.xdaqaccess.diag;

import java.math.BigInteger;
import java.rmi.RemoteException;
import java.util.Set;

import javax.xml.rpc.ServiceException;

import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.diag.DiagCtrl.State;
import rpct.xdaqaccess.diag.DiagCtrl.TriggerType;

public interface DiagAccess {

	// Diagnosable
	// !!! configures not the diagnostic itself but the whole chip
	// - owner of the diag ctrl
	void configureDiagnosable(Integer[] diagnosableIds,
			TriggerDataSel triggerDataSel, int dataTrgDelay) throws RemoteException,
			ServiceException;

	void configureDiagnosableLB(Integer[] diagnosableIds,
			TriggerDataSel triggerDataSel, int dataTrgDelay, int triggerAreaEna,
			boolean synFullOutSynchWindow, boolean synPulseEdgeSynchWindow)
			throws RemoteException, ServiceException;

	// DiagCtrl
	DiagCtrl[] getDiagCtrlList() throws RemoteException, ServiceException;

	void reset(DiagCtrl[] diagCtrls) throws RemoteException, ServiceException;

	void configure(DiagCtrl[] diagCtrls, BigInteger counterLimit,
			TriggerType triggerType, int triggerDelay) throws RemoteException, ServiceException;

	void start(DiagCtrl[] diagCtrls) throws RemoteException, ServiceException;

	void stop(DiagCtrl[] diagCtrls) throws RemoteException, ServiceException;

	State getState(DiagCtrl diagCtrl) throws RemoteException, ServiceException;

	boolean checkCountingEnded(DiagCtrl[] diagCtrls) throws RemoteException,
			ServiceException;

	// HistoMgr
	HistoMgr[] getHistoMgrList() throws RemoteException, ServiceException;

	void reset(HistoMgr[] histoMgrs) throws RemoteException, ServiceException;

	Binary[] readData(HistoMgr histoMgr) throws RemoteException,
			ServiceException;

	// Pulser
	Pulser[] getPulserList() throws RemoteException, ServiceException;

	void configure(Pulser[] pulsers, Binary[] data, BigInteger pulseLength,
			boolean repeat, int target) throws RemoteException,
			ServiceException;

	void configure(Pulser[] pulsers, String muonsXml, BigInteger pulseLength,
			boolean repeat, int target) throws RemoteException,
			ServiceException;

	// Diagnostic Readout
	DiagnosticReadout[] getDiagnosticReadoutList() throws RemoteException,
			ServiceException;

	void configure(DiagnosticReadout[] diagReadouts, Set<Integer>[] masks,
			int daqDelay) throws RemoteException, ServiceException;

	void configure(DiagnosticReadout[] diagReadouts, Set<Integer>[] masks,
			int daqDelay, int dataSel, int extSel) throws RemoteException,
			ServiceException;

	DiagnosticReadoutData[] readData(DiagnosticReadout[] diagReadouts)
			throws RemoteException, ServiceException;

}