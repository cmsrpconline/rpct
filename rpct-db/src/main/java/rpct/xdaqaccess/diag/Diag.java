package rpct.xdaqaccess.diag;

public interface Diag {
    DiagId getDiagId();    
    DiagAccess getDiagAccess();
    DiagCtrl getDiagCtrl();
}