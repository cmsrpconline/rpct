package rpct.xdaqaccess.diag;

public interface LMuxBxData {
    int getPartitionData();
    int getPartitionNum();
    int getPartitionDelay();
    int getEndOfData();
    int getHalfPartition();
    int getLbNum();
    int getIndex();
}
