package rpct.xdaqaccess.diag;

import java.math.BigInteger;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.rpc.ServiceException;

import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.diag.DiagCtrl.TriggerType;

public class DiagAccessDispatcher {

	/*private HardwareRegistry hardwareRegistry;

    public void addDiagAccess(HardwareRegistry hardwareRegistry) {
        this.hardwareRegistry = hardwareRegistry;
    } */   

    public static Set<Integer>[] createMask0(int bxNum) {
		Set<Integer> masks[] = new HashSet[] { new HashSet() };
		for (int i = 0; i < bxNum; i++) {
			masks[0].add(i + 1);
		}
		return masks;
    }
    
	private static <T extends Diag> Map<DiagAccess, List<T>> createMapByDiagAccess(List<T> diags) {
		Map<DiagAccess, List<T>> map = new HashMap<DiagAccess, List<T>>();
		for (T diag : diags) {
			DiagAccess access = diag.getDiagAccess();
			List<T> l = map.get(access);
			if (l == null) {
				l = new ArrayList<T>();
				map.put(access, l);
			}
			l.add(diag);
		}
		return map;
	}
	

	private static <T extends Diag> Map<DiagAccess, Set<Integer>> createOwnerMapByDiagAccess(List<T> diags) {

		Map<DiagAccess, Set<Integer>> map = new HashMap<DiagAccess, Set<Integer>>();
		for (Diag diag : diags) {
			DiagAccess access = diag.getDiagAccess();
			Set<Integer> s = map.get(access);
			if (s == null) {
				s = new HashSet<Integer>();
				map.put(access, s);
			}
			s.add(diag.getDiagId().getOwnerId());
		}	
		return map;
	}

	public static List<DiagCtrl> getDiagCtrls(List<? extends Diag> diags) {
		List<DiagCtrl> diagCtrls = new ArrayList<DiagCtrl>(diags.size());
		for (Diag diag : diags) {
			diagCtrls.add(diag.getDiagCtrl());
		}
		return diagCtrls;
	}

	public static <T extends Diag> void configureDiagnosable(List<T> diags, TriggerDataSel triggerDataSel, int dataTrgDelay) throws RemoteException,
			ServiceException {		
		Map<DiagAccess, Set<Integer>> map = createOwnerMapByDiagAccess(diags);		
		for (Map.Entry<DiagAccess, Set<Integer>> e : map.entrySet() ) {
			e.getKey().configureDiagnosable(
					e.getValue().toArray(new Integer[] {}), triggerDataSel, dataTrgDelay);
		}
	}

	public static <T extends Diag> void configureDiagnosableLB(List<T> diags, TriggerDataSel triggerDataSel, int dataTrgDelay, int triggerAreaEna,
			boolean synFullOutSynchWindow, boolean synPulseEdgeSynchWindow) throws RemoteException,
			ServiceException {		
		Map<DiagAccess, Set<Integer>> map = createOwnerMapByDiagAccess(diags);		
		for (Map.Entry<DiagAccess, Set<Integer>> e : map.entrySet() ) {
			e.getKey().configureDiagnosableLB(
					e.getValue().toArray(new Integer[] {}), triggerDataSel, dataTrgDelay,
					triggerAreaEna, synFullOutSynchWindow, synPulseEdgeSynchWindow);
		}
	}

	public static void reset(List<DiagCtrl> diagCtrls) throws RemoteException, ServiceException {
		Map<DiagAccess, List<DiagCtrl>> diagMap = createMapByDiagAccess(diagCtrls);
		for (Map.Entry<DiagAccess, List<DiagCtrl>> e : diagMap.entrySet() ) {
			e.getKey().reset(e.getValue().toArray(new DiagCtrl[] {}));
		}
	}

	public static void configure(List<DiagCtrl> diagCtrls, BigInteger counterLimit,
			TriggerType triggerType, int triggerDelay) throws RemoteException, ServiceException {
		Map<DiagAccess, List<DiagCtrl>> diagMap = createMapByDiagAccess(diagCtrls);
		for (Map.Entry<DiagAccess, List<DiagCtrl>> e : diagMap.entrySet() ) {
			e.getKey().configure(e.getValue().toArray(new DiagCtrl[] {}),
					counterLimit, triggerType, triggerDelay);
		}
	}

	public static void start(List<DiagCtrl> diagCtrls) throws RemoteException, ServiceException {
		Map<DiagAccess, List<DiagCtrl>> diagMap = createMapByDiagAccess(diagCtrls);
		for (Map.Entry<DiagAccess, List<DiagCtrl>> e : diagMap.entrySet() ) {
			e.getKey().start(e.getValue().toArray(new DiagCtrl[] {}));
		}
	}

	public static void stop(List<DiagCtrl> diagCtrls) throws RemoteException, ServiceException {

		Map<DiagAccess, List<DiagCtrl>> diagMap = createMapByDiagAccess(diagCtrls);
		for (Map.Entry<DiagAccess, List<DiagCtrl>> e : diagMap.entrySet() ) {
			e.getKey().stop(e.getValue().toArray(new DiagCtrl[] {}));
		}
	}

	//State getState(DiagCtrl diagCtrl) throws RemoteException, ServiceException;

	public static boolean checkCountingEnded(List<DiagCtrl> diagCtrls) throws RemoteException,
	ServiceException {
		Map<DiagAccess, List<DiagCtrl>> diagMap = createMapByDiagAccess(diagCtrls);
		for (Map.Entry<DiagAccess, List<DiagCtrl>> e : diagMap.entrySet() ) {
			if (!e.getKey().checkCountingEnded(e.getValue().toArray(new DiagCtrl[] {}))) {
				return false;
			}
		}
		return true;
	}

	// Pulser
	//Pulser[] getPulserList() throws RemoteException, ServiceException;

	public static void configure(List<Pulser> pulsers, Binary[] data, BigInteger pulseLength,
			boolean repeat, int target) throws RemoteException, ServiceException {

		Map<DiagAccess, List<Pulser>> diagMap = createMapByDiagAccess(pulsers);
		for (Map.Entry<DiagAccess, List<Pulser>> e : diagMap.entrySet() ) {
			e.getKey().configure(e.getValue().toArray(new Pulser[] {}), 
					data, pulseLength, repeat, target);
		}
	}

	public static void configure(List<Pulser> pulsers, String muonsXml, BigInteger pulseLength,
			boolean repeat, int target) throws RemoteException, ServiceException {
		Map<DiagAccess, List<Pulser>> diagMap = createMapByDiagAccess(pulsers);
		for (Map.Entry<DiagAccess, List<Pulser>> e : diagMap.entrySet() ) {
			e.getKey().configure(e.getValue().toArray(new Pulser[] {}), 
					muonsXml, pulseLength, repeat, target);
		}
	}


	//  Diagnostic Readout
	//DiagnosticReadout[] getDiagnosticReadoutList() throws RemoteException, ServiceException;

	public static void configure(List<DiagnosticReadout> diagReadouts, Set<Integer>[] masks,
			int daqDelay) throws RemoteException, ServiceException {
		Map<DiagAccess, List<DiagnosticReadout>> diagMap = createMapByDiagAccess(diagReadouts);
		for (Map.Entry<DiagAccess, List<DiagnosticReadout>> e : diagMap.entrySet() ) {
			e.getKey().configure(e.getValue().toArray(new DiagnosticReadout[] {}), 
					masks, daqDelay);
		}
	}

	public static void configure(List<DiagnosticReadout> diagReadouts, Set<Integer>[] masks,
			int daqDelay, int dataSel, int extSel) throws RemoteException,
			ServiceException {
		Map<DiagAccess, List<DiagnosticReadout>> diagMap = createMapByDiagAccess(diagReadouts);
		for (Map.Entry<DiagAccess, List<DiagnosticReadout>> e : diagMap.entrySet() ) {
			e.getKey().configure(e.getValue().toArray(new DiagnosticReadout[] {}), 
					masks, daqDelay, dataSel, extSel);
		}
	}

	public static DiagnosticReadoutData[] readData(List<DiagnosticReadout> diagReadouts) throws RemoteException,
	ServiceException {
		List<DiagnosticReadoutData> result = new ArrayList<DiagnosticReadoutData>();
		Map<DiagAccess, List<DiagnosticReadout>> diagMap = createMapByDiagAccess(diagReadouts);
		for (Map.Entry<DiagAccess, List<DiagnosticReadout>> e : diagMap.entrySet() ) {
			Collections.addAll(result, e.getKey().readData(e.getValue().toArray(new DiagnosticReadout[] {})));
		}
		return result.toArray(new DiagnosticReadoutData[] {});
	}
}
