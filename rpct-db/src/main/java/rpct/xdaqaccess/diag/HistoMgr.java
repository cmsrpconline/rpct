package rpct.xdaqaccess.diag;

import rpct.xdaq.XdaqException;
import rpct.xdaq.axis.Binary;

public interface HistoMgr extends Diag {	

    DiagId getDiagId();
    int getBinCount();
    DiagCtrl getDiagCtrl();

    void reset() throws XdaqException;
    Binary[] readData() throws XdaqException;
}
