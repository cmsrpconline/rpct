package rpct.xdaqaccess.diag;

import java.math.BigInteger;

import rpct.xdaq.XdaqException;

public interface DiagCtrl extends Diag {
	
	public enum State { 
		INIT,    
		RESET,  
		IDLE,    
		RUNNING 
	}
	
	public enum TriggerType {
		MANUAL, 
		L1A,
		PRETRIGGER_0, 
		PRETRIGGER_1,
		PRETRIGGER_2,
		BCN0,
		LOCAL  
	}
	
	DiagId getDiagId();
	
	
	void reset() throws XdaqException;
	void configure(BigInteger counterLimit, TriggerType triggerType, int triggerDelay) throws XdaqException;
	void start() throws XdaqException;
	void stop() throws XdaqException;

	State getState() throws XdaqException;
	boolean checkCountingEnded() throws XdaqException;	
}
