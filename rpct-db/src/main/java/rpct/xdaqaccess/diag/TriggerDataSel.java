package rpct.xdaqaccess.diag;

public enum TriggerDataSel {
	NONE(0),
	L1A(1),
	PRETRIGGER_0(2), 
	PRETRIGGER_1(3),
	PRETRIGGER_2(4),
	BCN0(5),
	LOCAL(6);
	
	private final int intVal;
	
	TriggerDataSel(int intVal) {
		this.intVal = intVal;
	}
	
	public int getIntVal() {
		return intVal;
	}
}
