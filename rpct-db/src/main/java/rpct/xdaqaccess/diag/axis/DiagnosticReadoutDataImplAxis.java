package rpct.xdaqaccess.diag.axis;

import rpct.xdaq.axis.bag.Bag;
import rpct.xdaq.axis.bag.HashBag;
import rpct.xdaqaccess.diag.DiagId;
import rpct.xdaqaccess.diag.DiagnosticReadoutData;
import rpct.xdaqaccess.diag.DiagnosticReadoutEvent;

public class DiagnosticReadoutDataImplAxis extends HashBag<Object> implements DiagnosticReadoutData {    

    public final static String KEY_DIAG_ID = "id";
    public final static String KEY_RUNNING = "running";
    public final static String KEY_LOST = "lost";
    public final static String KEY_EVENTS = "events";
    
    private DiagId diagId;
    private DiagnosticReadoutEvent[] events;

    @SuppressWarnings("unchecked")
    public DiagnosticReadoutDataImplAxis(HashBag<Object> bag) {
        super(bag);
        this.diagId = new DiagIdImplAxis((HashBag<Object>)bag.get(KEY_DIAG_ID));
    }

    public DiagId getDiagId() {
        return diagId;
    }

    public boolean isRunning() {
        return (Boolean) get(KEY_RUNNING);
    }

    public boolean isLost() {
        return (Boolean) get(KEY_LOST);
    }
    
    @SuppressWarnings("unchecked")
    public DiagnosticReadoutEvent[] getEvents() {
        if (events == null) {
            Bag<Object>[] eventInfos = (Bag<Object>[]) get(KEY_EVENTS);
            events = new DiagnosticReadoutEvent[eventInfos.length];   
            for (int i = 0; i < eventInfos.length; i++) {
                Bag<Object> eventInfo = (Bag<Object>) eventInfos[i];
                events[i] = new DiagnosticReadoutEventImplAxis(eventInfo);
            }
        }   
        return events;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder("id = ");
        return str.append(getDiagId()).append(", running = ").append(isRunning())
        .append(", lost = ").append(isLost()).toString();
    }
}
