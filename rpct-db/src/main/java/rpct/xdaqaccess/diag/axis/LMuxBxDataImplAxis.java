package rpct.xdaqaccess.diag.axis;

import java.math.BigInteger;

import rpct.xdaq.axis.bag.Bag;
import rpct.xdaq.axis.bag.HashBag;
import rpct.xdaqaccess.diag.LMuxBxData;

public class LMuxBxDataImplAxis extends HashBag<Object> implements LMuxBxData {

    public final static String KEY_PARTITIONDATA = "partitionData";
    public final static String KEY_PARTITIONNUM = "partitionNum";
    public final static String KEY_PARTITIONDELAY = "partitionDelay";
    public final static String KEY_ENDOFDATA = "endOfData";
    public final static String KEY_HALFPARTITION = "halfPartition";
    public final static String KEY_LBNUM = "lbNum";
    public final static String KEY_INDEX = "index";

    public LMuxBxDataImplAxis(Bag<Object> bag) {
        super(bag);        
    }
    
    public int getPartitionData() {
        return ((BigInteger)get(KEY_PARTITIONDATA)).intValue();
    }

    public int getPartitionNum() {
        return ((BigInteger)get(KEY_PARTITIONNUM)).intValue();
    }

    public int getPartitionDelay() {
        return ((BigInteger)get(KEY_PARTITIONDELAY)).intValue();
    }
    
    public int getEndOfData() {
        return ((BigInteger)get(KEY_ENDOFDATA)).intValue();
    }

    public int getHalfPartition() {
        return ((BigInteger)get(KEY_HALFPARTITION)).intValue();
    }

    public int getLbNum() {
        return ((BigInteger)get(KEY_LBNUM)).intValue();
    }

    public int getIndex() {
        return ((BigInteger)get(KEY_INDEX)).intValue();
    }
    
    public String toString() {
    	return "lmx indx " + getIndex() + " partN " + getPartitionNum() + " del " + getPartitionDelay() + " data " + getPartitionData();
    }

}
