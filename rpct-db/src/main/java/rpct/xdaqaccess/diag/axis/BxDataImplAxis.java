package rpct.xdaqaccess.diag.axis;

import java.math.BigInteger;

import rpct.xdaq.axis.bag.Bag;
import rpct.xdaq.axis.bag.HashBag;
import rpct.xdaqaccess.diag.BxData;
import rpct.xdaqaccess.diag.LMuxBxData;
import rpct.xdaqaccess.diag.Muon;

public class BxDataImplAxis extends HashBag<Object> implements BxData {

    public final static String KEY_INDEX = "index";
    public final static String KEY_POS = "pos";
    public final static String KEY_TRIGGERS = "triggers";
    public final static String KEY_CHANNELSVALID = "channelsValid";
    public final static String KEY_LMUXBXDATA = "lmuxBxData";
    public final static String KEY_MUONS = "muons";

    private LMuxBxData[] lmuxBxData;
    private Muon[] muons;
    
    public BxDataImplAxis(Bag<Object> bag) {
        super(bag);        
    }
    
    public int getIndex() {
        return ((BigInteger)get(KEY_INDEX)).intValue();
    }
    
    public int getPos() {
        return ((BigInteger)get(KEY_POS)).intValue();
    }

    public int getTriggers() {
        return ((BigInteger)get(KEY_TRIGGERS)).intValue();
    }    
    
    public int getChannelsValid() {
        return ((BigInteger)get(KEY_CHANNELSVALID)).intValue();
    }

    @SuppressWarnings("unchecked")
    public LMuxBxData[] getLMuxBxData() {
        if (lmuxBxData == null) {
            Bag<Object>[] infos = (Bag<Object>[]) get(KEY_LMUXBXDATA);
            lmuxBxData = new LMuxBxData[infos.length];   
            for (int i = 0; i < infos.length; i++) {
                Bag<Object> info = (Bag<Object>) infos[i];
                lmuxBxData[i] = new LMuxBxDataImplAxis(info);
            }                
        }   
        return lmuxBxData;
    }

    @SuppressWarnings("unchecked")
    public Muon[] getMuons() {
        if (muons == null) {
            Bag<Object>[] infos = (Bag<Object>[]) get(KEY_MUONS);
            muons = new Muon[infos.length];   
            for (int i = 0; i < infos.length; i++) {
                Bag<Object> info = (Bag<Object>) infos[i];
                muons[i] = new MuonImplAxis(info);
            }                
        }   
        return muons;
    }

    public String toString() {
    	return "BxDataImplAxis: pos " + getPos();
    }
}
