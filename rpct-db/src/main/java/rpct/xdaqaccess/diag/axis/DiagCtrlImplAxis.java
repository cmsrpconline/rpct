package rpct.xdaqaccess.diag.axis;

import java.math.BigInteger;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.diag.DiagAccess;
import rpct.xdaqaccess.diag.DiagCtrl;
import rpct.xdaqaccess.diag.DiagId;

public class DiagCtrlImplAxis implements DiagCtrl {
    private DiagAccessImplAxis diagAccess;

    private DiagId diagCtrlId;

    public DiagCtrlImplAxis(DiagId diagCtrlId, DiagAccessImplAxis diagAccess) {
        this.diagAccess = diagAccess;
        this.diagCtrlId = diagCtrlId;
    }

    public DiagId getDiagId() {
        return diagCtrlId;
    }

    public DiagAccess getDiagAccess() {
        return diagAccess;
    }

    public void reset() throws XdaqException {
        try {
            diagAccess.reset(new DiagCtrl[] { this });
        } catch (RemoteException e) {
            throw new XdaqException(e);
        } catch (ServiceException e) {
            throw new XdaqException(e);
        }
    }

    public void configure(BigInteger counterLimit, TriggerType triggerType, int triggerDelay)
            throws XdaqException {
        try {
            diagAccess.configure(new DiagCtrl[] { this }, counterLimit,
                    triggerType, triggerDelay);
        } catch (RemoteException e) {
            throw new XdaqException(e);
        } catch (ServiceException e) {
            throw new XdaqException(e);
        }
    }

    public void start() throws XdaqException {
        try {
            diagAccess.start(new DiagCtrl[] { this });
        } catch (RemoteException e) {
            throw new XdaqException(e);
        } catch (ServiceException e) {
            throw new XdaqException(e);
        }
    }

    public void stop() throws XdaqException {
        try {
            diagAccess.stop(new DiagCtrl[] { this });
        } catch (RemoteException e) {
            throw new XdaqException(e);
        } catch (ServiceException e) {
            throw new XdaqException(e);
        }
    }

    public State getState() throws XdaqException {
        try {
            return diagAccess.getState(this);
        } catch (RemoteException e) {
            throw new XdaqException(e);
        } catch (ServiceException e) {
            throw new XdaqException(e);
        }
    }

    public boolean checkCountingEnded() throws XdaqException {
        try {
            return diagAccess.checkCountingEnded(new DiagCtrl[] { this });
        } catch (RemoteException e) {
            throw new XdaqException(e);
        } catch (ServiceException e) {
            throw new XdaqException(e);
        }
    }

	public DiagCtrl getDiagCtrl() {
		return null;
	}
}
