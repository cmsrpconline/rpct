package rpct.xdaqaccess.diag.axis;

import java.math.BigInteger;

import rpct.xdaq.axis.bag.Bag;
import rpct.xdaq.axis.bag.HashBag;
import rpct.xdaqaccess.diag.BxData;
import rpct.xdaqaccess.diag.DiagnosticReadoutEvent;

public class DiagnosticReadoutEventImplAxis extends HashBag<Object> implements DiagnosticReadoutEvent {

    public final static String KEY_BCN = "bcn";
    public final static String KEY_EVNUM = "evNum";
    public final static String KEY_BXDATA = "bxData";

    private BxData[] bxData;
    
    public DiagnosticReadoutEventImplAxis(Bag<Object> bag) {
        super(bag);        
    }
    
    public int getBcn() {
        return ((BigInteger) get(KEY_BCN)).intValue();
    }

    public int getEvNum() {
        return ((BigInteger) get(KEY_EVNUM)).intValue();
    }

    @SuppressWarnings("unchecked")
    public BxData[] getBxData() {
        if (bxData == null) {
            Bag<Object>[] bxDataInfos = (Bag<Object>[]) get(KEY_BXDATA);
            bxData = new BxData[bxDataInfos.length];   
            for (int i = 0; i < bxDataInfos.length; i++) {
                Bag<Object> bxDataInfo = (Bag<Object>) bxDataInfos[i];
                bxData[i] = new BxDataImplAxis(bxDataInfo);
            }                
        }   
        return bxData;
    }
    
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder("bcn = ");
        return str.append(getBcn()).append(", evNum = ").append(getEvNum()).toString();
    }

}
