package rpct.xdaqaccess.diag.axis;

import java.math.BigInteger;

import rpct.xdaq.axis.bag.HashBag;
import rpct.xdaqaccess.diag.DiagId;

public class DiagIdImplAxis extends HashBag<Object> implements DiagId {
    public final static String KEY_OWNER_ID = "ownerId";
    public final static String KEY_NAME = "name";

    public DiagIdImplAxis(int ownerId, String name) {
        setOwnerId(ownerId);
        setName(name);
    }
    
    public DiagIdImplAxis(DiagId diagId) {
        setOwnerId(diagId.getOwnerId());
        setName(diagId.getName());
    }

    public DiagIdImplAxis(HashBag<Object> bag) {
        super(bag);
    }

    /* (non-Javadoc)
     * @see rpct.diag.access.axis.DiagId#getOwnerId()
     */
    public int getOwnerId() {
        return ((BigInteger) get(KEY_OWNER_ID)).intValue();
    }

    public void setOwnerId(int ownerId) {
        put(KEY_OWNER_ID, BigInteger.valueOf(ownerId));
    }
    
    public String getName() {
        return (String) get(KEY_NAME);
    }

    public void setName(String name) {
        put(KEY_NAME, name);
    }

    @Override
    public int hashCode() {
        final int PRIME = 31;
        String name = getName();
        int ownerId = getOwnerId();
        int result = 1;//super.hashCode();
        result = PRIME * result + ((name == null) ? 0 : name.hashCode());
        result = PRIME * result + ownerId;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        //if (!super.equals(obj))
        //    return false;
        if (getClass() != obj.getClass())
            return false;

        String name = getName();
        int ownerId = getOwnerId();

        final DiagId other = (DiagId) obj;
        if (name == null) {
            if (other.getName() != null)
                return false;
        } else if (!name.equals(other.getName()))
            return false;
        if (ownerId != other.getOwnerId())
            return false;
        return true;
    }    
    
    @Override
    public String toString() {
        return getOwnerId() + "." + getName();
    }

}
