package rpct.xdaqaccess.diag.axis;

import java.math.BigInteger;

import rpct.xdaq.axis.bag.Bag;
import rpct.xdaq.axis.bag.HashBag;
import rpct.xdaqaccess.diag.Muon;

public class MuonImplAxis extends HashBag<Object> implements Muon {

    public final static String KEY_PTCODE = "ptCode";
    public final static String KEY_QUALITY = "quality";
    public final static String KEY_SIGN = "sign";
    public final static String KEY_ETAADDRESSE = "etaAddress";
    public final static String KEY_PHIADDRESS = "phiAddress";
    public final static String KEY_GBDATA = "gbData";
    public final static String KEY_INDEX = "index";


    public MuonImplAxis(Bag<Object> bag) {
        super(bag);        
    }
    
    public int getPtCode() {
        return ((BigInteger)get(KEY_PTCODE)).intValue();
    }

    public int getQuality() {
        return ((BigInteger)get(KEY_QUALITY)).intValue();
    }

    public int getSign() {
        return ((BigInteger)get(KEY_SIGN)).intValue();
    }
    
    public int getEtaAddress() {
        return ((BigInteger)get(KEY_ETAADDRESSE)).intValue();
    }

    public int getPhiAddress() {
        return ((BigInteger)get(KEY_PHIADDRESS)).intValue();
    }

    public int getGBData() {
        return ((BigInteger)get(KEY_GBDATA)).intValue();
    }

    public int getIndex() {
        return ((BigInteger)get(KEY_INDEX)).intValue();
    }
}
