package rpct.xdaqaccess.diag.axis;

import java.math.BigInteger;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import rpct.xdaq.XdaqException;
import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.diag.DiagAccess;
import rpct.xdaqaccess.diag.DiagCtrl;
import rpct.xdaqaccess.diag.DiagId;
import rpct.xdaqaccess.diag.Pulser;

public class PulserImplAxis implements Pulser {
    private DiagAccessImplAxis diagAccess;

    private DiagId diagId;

    private int maxLength;

    private int width;

    private DiagId diagCtrlId;

    public PulserImplAxis(DiagId diagId, int maxLength, int width,
            DiagId diagCtrlId, DiagAccessImplAxis diagAccess) {
        this.diagId = diagId;
        this.maxLength = maxLength;
        this.width = width;
        this.diagCtrlId = diagCtrlId;
        this.diagAccess = diagAccess;
    }
    
    public DiagId getDiagId() {
        return diagId;
    }

    public DiagAccess getDiagAccess() {
        return diagAccess;
    }

    public int getMaxLength() {
        return maxLength;
    }

    public int getWidth() {
        return width;
    }

    public DiagId getDiagCtrlId() {
        return diagCtrlId;
    }

    public DiagCtrl getDiagCtrl() {
        DiagCtrl diagCtrl = diagAccess.getDiagCtrl(diagCtrlId);
        if (diagCtrl == null) {
            throw new IllegalStateException("getDiagCtrlList should be called");
        }
        return diagCtrl;
    }

    public void configure(Binary[] data, BigInteger pulseLength, boolean repeat, int target)
            throws XdaqException {
        try {
            diagAccess.configure(new Pulser[] { this }, data, pulseLength,
                    repeat, target);
        } catch (RemoteException e) {
            throw new XdaqException(e);
        } catch (ServiceException e) {
            throw new XdaqException(e);
        }
    }

}
