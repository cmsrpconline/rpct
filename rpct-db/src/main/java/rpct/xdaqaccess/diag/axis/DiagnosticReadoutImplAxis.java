package rpct.xdaqaccess.diag.axis;

import java.rmi.RemoteException;
import java.util.Set;

import javax.xml.rpc.ServiceException;

import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.diag.DiagCtrl;
import rpct.xdaqaccess.diag.DiagId;
import rpct.xdaqaccess.diag.DiagnosticReadout;

public class DiagnosticReadoutImplAxis implements DiagnosticReadout {
    private DiagAccessImplAxis diagAccess;

    private DiagId diagId;

    private int areaLen;

    private int areaWidth;

    private int maskNum;

    private int maskWidth;

    private int dataWidth;

    private int trgNum;

    private int timerWidth;

    private DiagId diagCtrlId;

    public DiagnosticReadoutImplAxis(DiagId diagId, int areaLen, int areaWidth, 
            int maskNum, int maskWidth, int dataWidth, int trgNum, int timerWidth,
            DiagId diagCtrlId, DiagAccessImplAxis diagAccess) {
        this.diagId = diagId;
        this.areaLen = areaLen;
        this.areaWidth = areaWidth;
        this.maskNum = maskNum;
        this.maskWidth = maskWidth;
        this.dataWidth = dataWidth;
        this.trgNum = trgNum;
        this.timerWidth = timerWidth;
        this.diagCtrlId = diagCtrlId;
        this.diagAccess = diagAccess;
    }


    public DiagId getDiagId() {
        return diagId;
    }

    public DiagAccessImplAxis getDiagAccess() {
        return diagAccess;
    }
    
    public String getName() {
        return diagId.getName();
    }

    public int getOwnerId() {
        return diagId.getOwnerId();
    }

    public int getAreaLen() {
        return areaLen;
    }

    public int getAreaWidth() {
        return areaWidth;
    }

    public int getDataWidth() {
        return dataWidth;
    }

    public DiagId getDiagCtrlId() {
        return diagCtrlId;
    }

    public int getMaskNum() {
        return maskNum;
    }

    public int getMaskWidth() {
        return maskWidth;
    }

    public int getTimerWidth() {
        return timerWidth;
    }

    public int getTrgNum() {
        return trgNum;
    }

    public void configure(Set<Integer>[] masks, int daqDelay) throws XdaqException {
        try {
            diagAccess.configure(new DiagnosticReadout[] { this }, masks, daqDelay);
        } catch (RemoteException e) {
            throw new XdaqException(e);
        } catch (ServiceException e) {
            throw new XdaqException(e);
        }        
    }

    public void configure(Set<Integer>[] masks, int daqDelay, int dataSel, int extSel) throws XdaqException {

        try {
            diagAccess.configure(new DiagnosticReadout[] { this }, masks, daqDelay, dataSel, extSel);
        } catch (RemoteException e) {
            throw new XdaqException(e);
        } catch (ServiceException e) {
            throw new XdaqException(e);
        }        
        
    }

    public DiagCtrl getDiagCtrl() {
        DiagCtrl diagCtrl = diagAccess.getDiagCtrl(diagCtrlId);
        if (diagCtrl == null) {
            throw new IllegalStateException("getDiagCtrlList should be called");
        }
        return diagCtrl;
    }

}
