package rpct.xdaqaccess.diag.axis;

import java.math.BigInteger;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;
import javax.xml.rpc.ServiceException;

import org.apache.axis.Constants;
import org.apache.axis.client.Call;
import org.apache.axis.types.HexBinary;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import rpct.xdaq.XdaqApplicationInfo;
import rpct.xdaq.axis.Binary;
import rpct.xdaq.axis.XdaqAccessImplAxisBase;
import rpct.xdaq.axis.bag.Bag;
import rpct.xdaq.axis.bag.HashBag;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.diag.DiagAccess;
import rpct.xdaqaccess.diag.DiagCtrl;
import rpct.xdaqaccess.diag.DiagId;
import rpct.xdaqaccess.diag.DiagnosticReadout;
import rpct.xdaqaccess.diag.DiagnosticReadoutData;
import rpct.xdaqaccess.diag.HistoMgr;
import rpct.xdaqaccess.diag.Pulser;
import rpct.xdaqaccess.diag.TriggerDataSel;
import rpct.xdaqaccess.diag.DiagCtrl.State;
import rpct.xdaqaccess.diag.DiagCtrl.TriggerType;

/**
 * Created on 2005-05-05
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class DiagAccessImplAxis extends XdaqAccessImplAxisBase implements DiagAccess  {    

    private static Log log = LogFactory.getLog(DiagAccessImplAxis.class); 

    public static String RPCT_DIAG_ACCESS_NS = "urn:rpct-diag-access:1.0";

    private Map<DiagIdImplAxis, DiagCtrlImplAxis> diagCtrlMap = new HashMap<DiagIdImplAxis, DiagCtrlImplAxis>();
    private HardwareRegistry hardwareRegistry;
    
    public DiagAccessImplAxis(XdaqApplicationInfo xdaqApplicationInfo, HardwareRegistry hardwareRegistry) {
        super(xdaqApplicationInfo, RPCT_DIAG_ACCESS_NS);
        this.hardwareRegistry = hardwareRegistry;
    }

	public void configureDiagnosable(Integer[] diagnosableIds,
			TriggerDataSel triggerDataSel, int dataTrgDelay) throws RemoteException, ServiceException {

        Call call = prepareCall("configureDiagnosable"); 
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "diagnosableIds"), 
                Constants.SOAP_ARRAY,
                ParameterMode.IN);		
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "triggerDataSel"), 
                Constants.XSD_STRING,
                ParameterMode.IN);			
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "dataTrgDelay"), 
                Constants.XSD_INTEGER,
                ParameterMode.IN);		
        call.setReturnType(Constants.XSD_ANY);
        
        BigInteger[] bigDiagnosableIds = new BigInteger[diagnosableIds.length];
        for (int i = 0; i < bigDiagnosableIds.length; i++) {
        	bigDiagnosableIds[i] = BigInteger.valueOf(diagnosableIds[i]);
        }
        
        call.invoke(new Object[] { 
        		bigDiagnosableIds,
        		triggerDataSel.toString(),
        		BigInteger.valueOf(dataTrgDelay)
        });		
	}
	

	public void configureDiagnosableLB(Integer[] diagnosableIds,
			TriggerDataSel triggerDataSel, int dataTrgDelay, int triggerAreaEna,
			boolean synFullOutSynchWindow, boolean synPulseEdgeSynchWindow) throws RemoteException, ServiceException {

        Call call = prepareCall("configureDiagnosableLB"); 
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "diagnosableIds"), 
                Constants.SOAP_ARRAY,
                ParameterMode.IN);		
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "triggerDataSel"), 
                Constants.XSD_STRING,
                ParameterMode.IN);				
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "dataTrgDelay"), 
                Constants.XSD_INTEGER,
                ParameterMode.IN);			
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "triggerAreaEna"), 
                Constants.XSD_INTEGER,
                ParameterMode.IN);		
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "synFullOutSynchWindow"), 
                Constants.XSD_BOOLEAN,
                ParameterMode.IN);		
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "synPulseEdgeSynchWindow"), 
                Constants.XSD_BOOLEAN,
                ParameterMode.IN);	
        call.setReturnType(Constants.XSD_ANY);
        
        BigInteger[] bigDiagnosableIds = new BigInteger[diagnosableIds.length];
        for (int i = 0; i < bigDiagnosableIds.length; i++) {
        	bigDiagnosableIds[i] = BigInteger.valueOf(diagnosableIds[i]);
        }
        
        call.invoke(new Object[] { 
        		bigDiagnosableIds,
        		triggerDataSel.toString(),
        		BigInteger.valueOf(dataTrgDelay),
        		triggerAreaEna,
        		synFullOutSynchWindow,
        		synPulseEdgeSynchWindow
        });		
	}

    @SuppressWarnings("unchecked")
    public DiagCtrl[] getDiagCtrlList() throws RemoteException, ServiceException {
        Call call = prepareCall("getDiagCtrlList");
        Bag[] bagsArray = (Bag[]) call.invoke(new Object[] {});
        diagCtrlMap.clear();
        DiagCtrl[] diagCtrls = new DiagCtrl[bagsArray.length];
        for (int i = 0; i < bagsArray.length; i++) {
            DiagIdImplAxis diagCtrlId = new DiagIdImplAxis((HashBag<Object>) bagsArray[i]);
            DiagCtrlImplAxis diagCtrl = new DiagCtrlImplAxis(diagCtrlId, this);
            diagCtrls[i] = diagCtrl;
            diagCtrlMap.put(diagCtrlId, diagCtrl);
        }
        return diagCtrls;
    }

    public DiagCtrl getDiagCtrl(DiagId diagCtrlId) {
        return diagCtrlMap.get(diagCtrlId);
    }


    public void reset(DiagCtrl[] diagCtrls) throws RemoteException, ServiceException {
        Call call = prepareCall("resetDiagCtrl"); 
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "diagCtrlIds"), 
                Constants.SOAP_ARRAY,
                ParameterMode.IN);
        call.setReturnType(Constants.XSD_ANY);
        Bag[] diagCtrlIds = new Bag[diagCtrls.length];
        for (int i = 0; i < diagCtrls.length; i++) {
            diagCtrlIds[i] = (Bag) diagCtrls[i].getDiagId();
        }
        call.invoke(new Object[] { diagCtrlIds });
    } 

    public void configure(DiagCtrl[] diagCtrls, BigInteger counterLimit, 
            TriggerType triggerType, int triggerDelay) throws RemoteException, ServiceException {
        Call call = prepareCall("configureDiagCtrl"); 
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "diagCtrlIds"), 
                Constants.SOAP_ARRAY,
                ParameterMode.IN);
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "counterLimit"), 
                Constants.XSD_HEXBIN,
                ParameterMode.IN);			
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "triggerType"), 
                Constants.XSD_STRING,
                ParameterMode.IN);		
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "triggerDelay"), 
                Constants.XSD_INTEGER,
                ParameterMode.IN);	
        call.setReturnType(Constants.XSD_ANY);
        Bag[] diagCtrlIds = new Bag[diagCtrls.length];
        for (int i = 0; i < diagCtrls.length; i++) {
            diagCtrlIds[i] = (Bag) diagCtrls[i].getDiagId();
        }
        call.invoke(new Object[] { 
                diagCtrlIds,
                new HexBinary(counterLimit.toByteArray()),
                triggerType.toString(),
                BigInteger.valueOf(triggerDelay)
        });
    } 


    public void start(DiagCtrl[] diagCtrls) throws RemoteException, ServiceException {
        Call call = prepareCall("startDiagCtrl"); 
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "diagCtrlIds"), 
                Constants.SOAP_ARRAY,
                ParameterMode.IN);
        call.setReturnType(Constants.XSD_ANY);
        Bag[] diagCtrlIds = new Bag[diagCtrls.length];
        for (int i = 0; i < diagCtrls.length; i++) {
            diagCtrlIds[i] = (Bag) diagCtrls[i].getDiagId();
        }
        call.invoke(new Object[] { diagCtrlIds });		
    }


    public void stop(DiagCtrl[] diagCtrls) throws RemoteException, ServiceException {
        Call call = prepareCall("stopDiagCtrl"); 
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "diagCtrlIds"), 
                Constants.SOAP_ARRAY,
                ParameterMode.IN);
        call.setReturnType(Constants.XSD_ANY);
        Bag[] diagCtrlIds = new Bag[diagCtrls.length];
        for (int i = 0; i < diagCtrls.length; i++) {
            diagCtrlIds[i] = (Bag) diagCtrls[i].getDiagId();
        }
        call.invoke(new Object[] { diagCtrlIds });
    }

    public State getState(DiagCtrl diagCtrl) throws RemoteException, ServiceException {
        Call call = prepareCall("getStateDiagCtrl"); 
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "diagCtrlId"), 
                Bag.XML_TYPE,
                ParameterMode.IN);
        call.setReturnType(Constants.XSD_STRING);		
        String state = (String) call.invoke(new Object[] { diagCtrl.getDiagId() });
        return State.valueOf(state);
    }

    public boolean checkCountingEnded(DiagCtrl[] diagCtrls) throws RemoteException, ServiceException {
        Call call = prepareCall("checkCountingEndedDiagCtrl"); 
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "diagCtrlIds"), 
                Constants.SOAP_ARRAY,
                ParameterMode.IN);
        call.setReturnType(Constants.XSD_BOOLEAN);	
        Bag[] diagCtrlIds = new Bag[diagCtrls.length];
        for (int i = 0; i < diagCtrls.length; i++) {
            diagCtrlIds[i] = (Bag) diagCtrls[i].getDiagId();
        }	
        return (Boolean) call.invoke(new Object[] { diagCtrlIds });
    }

    @SuppressWarnings("unchecked")
    public HistoMgr[] getHistoMgrList() throws RemoteException, ServiceException {
        Call call = prepareCall("getHistoMgrList");
        //call.setReturnType(Constants.SOAP_ARRAY);
        Bag[] bagsArray  = (Bag[]) call.invoke(new Object[] {});
        HistoMgr[] histoMgrList = new HistoMgr[bagsArray.length];
        for (int i = 0; i < bagsArray.length; i++) {
            Bag<Object> hitoMgrInfoBag = (Bag<Object>) bagsArray[i];
            DiagId diagId = new DiagIdImplAxis((HashBag<Object>) hitoMgrInfoBag.get("id"));
            int binCount = ((BigInteger) hitoMgrInfoBag.get("binCount")).intValue();
            DiagId diagCtrlId = new DiagIdImplAxis((HashBag<Object>) hitoMgrInfoBag.get("diagCtrlId"));
            HistoMgrImplAxis histoMgr = new HistoMgrImplAxis(diagId, binCount, diagCtrlId, this); 
            histoMgrList[i] = histoMgr;
            hardwareRegistry.registerDiag(histoMgr);
        }
        return histoMgrList;
    }

    public void reset(HistoMgr[] histoMgrs) throws RemoteException, ServiceException {
        Call call = prepareCall("resetHistoMgr"); 
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "histoMgrIds"), 
                Constants.SOAP_ARRAY,
                ParameterMode.IN);
        call.setReturnType(Constants.XSD_ANY);

        Bag[] histoMgrIds = new Bag[histoMgrs.length];
        for (int i = 0; i < histoMgrs.length; i++) {
            histoMgrIds[i] = (Bag) histoMgrs[i].getDiagId();
        }	

        call.invoke(new Object[] { histoMgrIds });	
    }

    public Binary[] readData(HistoMgr histoMgr) throws RemoteException, ServiceException {
        Call call = prepareCall("readDataHistoMgr"); 
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "histoMgrId"), 
                Bag.XML_TYPE,
                ParameterMode.IN);
        call.setReturnType(Constants.SOAP_ARRAY);

        byte[][] byteArrayList = (byte[][]) call.invoke(new Object[] {
                histoMgr.getDiagId()
        });
        Binary[] result = new Binary[byteArrayList.length];            
        for (int i = 0; i < result.length; i++) {
            result[i] = new Binary(byteArrayList[i]);
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public Pulser[] getPulserList() throws RemoteException, ServiceException {
        Call call = prepareCall("getPulserList");
        //call.setReturnType(Constants.SOAP_ARRAY);
        Bag[] bagsArray  = (Bag[]) call.invoke(new Object[] {});
        Pulser[] pulserList = new Pulser[bagsArray.length];
        for (int i = 0; i < bagsArray.length; i++) {
            Bag<Object> pulserInfoBag = (Bag<Object>) bagsArray[i];
            DiagId diagId = new DiagIdImplAxis((HashBag<Object>) pulserInfoBag.get("id"));
            int maxLength = ((BigInteger) pulserInfoBag.get("maxLength")).intValue();
            int width = ((BigInteger) pulserInfoBag.get("width")).intValue();
            DiagId diagCtrlId = new DiagIdImplAxis((HashBag<Object>) pulserInfoBag.get("diagCtrlId"));
            PulserImplAxis pulser = new PulserImplAxis(diagId, maxLength, width, diagCtrlId, this); 
            pulserList[i] = pulser;
            hardwareRegistry.registerDiag(pulser);
        }
        return pulserList;
    }

    public void configure(Pulser[] pulsers, Binary[] data, BigInteger pulseLength, boolean repeat, int target) throws RemoteException, ServiceException {
        Call call = prepareCall("configurePulser"); 
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "pulserIds"), 
                Constants.SOAP_ARRAY,
                ParameterMode.IN);		
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "data"), 
                Constants.SOAP_ARRAY,
                ParameterMode.IN);			
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "pulseLength"), 
                Constants.XSD_HEXBIN,
                ParameterMode.IN);			
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "repeat"), 
                Constants.XSD_BOOLEAN,
                ParameterMode.IN);	                     
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "target"), 
                Constants.XSD_INTEGER,
                ParameterMode.IN);      	
        call.setReturnType(Constants.XSD_ANY);


        Bag[] pulserIds = new Bag[pulsers.length];
        for (int i = 0; i < pulsers.length; i++) {
            pulserIds[i] = (Bag) pulsers[i].getDiagId();
        }	

        HexBinary[] arrayData = new HexBinary[data.length];
        for (int i = 0; i < data.length; i++) {
            arrayData[i] =  new HexBinary(data[i].getBytes());
        }

        call.invoke(new Object[] { 
                pulserIds,
                arrayData,
                new HexBinary(pulseLength.toByteArray()),
                repeat,
                BigInteger.valueOf(target)
        });
    }
    
    public void configure(Pulser[] pulsers, String muonsXml, BigInteger pulseLength, boolean repeat, int target) throws RemoteException, ServiceException {
        Call call = prepareCall("configurePulserMuons"); 
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "pulserIds"), 
                Constants.SOAP_ARRAY,
                ParameterMode.IN);              
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "muonsXml"), 
                Constants.XSD_STRING,
                ParameterMode.IN);                      
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "pulseLength"), 
                Constants.XSD_HEXBIN,
                ParameterMode.IN);                      
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "repeat"), 
                Constants.XSD_BOOLEAN,
                ParameterMode.IN);                        
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "target"), 
                Constants.XSD_INTEGER,
                ParameterMode.IN);              
        call.setReturnType(Constants.XSD_ANY);

        Bag[] pulserIds = new Bag[pulsers.length];
        for (int i = 0; i < pulsers.length; i++) {
            pulserIds[i] = (Bag) pulsers[i].getDiagId();
        } 

        call.invoke(new Object[] { 
                pulserIds,
                muonsXml,
                new HexBinary(pulseLength.toByteArray()),
                repeat,
                BigInteger.valueOf(target)
        });
    }
    
    @SuppressWarnings("unchecked")
    public DiagnosticReadout[] getDiagnosticReadoutList() throws RemoteException, ServiceException {
        Call call = prepareCall("getDiagnosticReadoutList");
        //call.setReturnType(Constants.SOAP_ARRAY);
        Bag[] bagsArray  = (Bag[]) call.invoke(new Object[] {});
        DiagnosticReadout[] readoutList = new DiagnosticReadout[bagsArray.length];
        for (int i = 0; i < bagsArray.length; i++) {
            Bag<Object> readoutInfoBag = (Bag<Object>) bagsArray[i];
            DiagId diagId = new DiagIdImplAxis((HashBag<Object>) readoutInfoBag.get("id"));;
            DiagId diagCtrlId = new DiagIdImplAxis((HashBag<Object>) readoutInfoBag.get("diagCtrlId"));
            DiagnosticReadoutImplAxis readout = new DiagnosticReadoutImplAxis(diagId, 
                    ((BigInteger) readoutInfoBag.get("areaLen")).intValue(), 
                    ((BigInteger) readoutInfoBag.get("areaWidth")).intValue(), 
                    ((BigInteger) readoutInfoBag.get("maskNum")).intValue(), 
                    ((BigInteger) readoutInfoBag.get("maskWidth")).intValue(), 
                    ((BigInteger) readoutInfoBag.get("dataWidth")).intValue(), 
                    ((BigInteger) readoutInfoBag.get("trgNum")).intValue(), 
                    ((BigInteger) readoutInfoBag.get("timerWidth")).intValue(), 
                    diagCtrlId, 
                    this);
            readoutList[i] = readout;
            hardwareRegistry.registerDiag(readout);
        }
        return readoutList;
    }


    public void configure(DiagnosticReadout[] diagReadouts, Set<Integer>[] masks, int daqDelay) throws ServiceException, RemoteException {
        Call call = prepareCall("configureDiagnosticReadout"); 
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "diagReadoutIds"), 
                Constants.SOAP_ARRAY,
                ParameterMode.IN);              
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "masks"), 
                Constants.SOAP_ARRAY,
                ParameterMode.IN);                      
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "daqDelay"), 
                Constants.XSD_INTEGER,
                ParameterMode.IN);          
        call.setReturnType(Constants.XSD_ANY);        

        Bag[] readoutIds = new Bag[diagReadouts.length];
        for (int i = 0; i < diagReadouts.length; i++) {
            readoutIds[i] = (Bag) diagReadouts[i].getDiagId();
        } 
        
        List<BigInteger[]> masks1 = new ArrayList<BigInteger[]>(masks.length);
        for (int i = 0; i < masks.length; i++) {
            Set<Integer> mask = masks[i];
            BigInteger[] mask1 = new BigInteger[mask.size()];
            masks1.add(mask1);
            int j = 0;
            for (Integer integer : mask) {
                mask1[j] = BigInteger.valueOf(integer);
                j++;
            }
        }        

        call.invoke(new Object[] { 
                readoutIds,
                masks1,
                BigInteger.valueOf(daqDelay)
        });        
    }

    public void configure(DiagnosticReadout[] diagReadouts, Set<Integer>[] masks, int daqDelay, int dataSel, int extSel) throws ServiceException, RemoteException {
        Call call = prepareCall("configureDiagnosticReadoutLB"); 
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "diagReadoutIds"), 
                Constants.SOAP_ARRAY,
                ParameterMode.IN);              
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "masks"), 
                Constants.SOAP_ARRAY,
                ParameterMode.IN);                      
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "daqDelay"), 
                Constants.XSD_INTEGER,
                ParameterMode.IN);                        
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "dataSel"), 
                Constants.XSD_INTEGER,
                ParameterMode.IN);                       
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "extSel"), 
                Constants.XSD_INTEGER,
                ParameterMode.IN);         
        call.setReturnType(Constants.XSD_ANY);        

        Bag[] readoutIds = new Bag[diagReadouts.length];
        for (int i = 0; i < diagReadouts.length; i++) {
            readoutIds[i] = (Bag) diagReadouts[i].getDiagId();
        } 
        
        List<BigInteger[]> masks1 = new ArrayList<BigInteger[]>(masks.length);
        for (int i = 0; i < masks.length; i++) {
            Set<Integer> mask = masks[i];
            BigInteger[] mask1 = new BigInteger[mask.size()];
            masks1.add(mask1);
            int j = 0;
            for (Integer integer : mask) {
                mask1[j] = BigInteger.valueOf(integer);
                j++;
            }
        }        

        call.invoke(new Object[] { 
                readoutIds,
                masks1,
                BigInteger.valueOf(daqDelay),
                BigInteger.valueOf(dataSel),
                BigInteger.valueOf(extSel)
        });   
    }

    @SuppressWarnings("unchecked")
    public DiagnosticReadoutData[] readData(DiagnosticReadout[] diagReadouts) throws RemoteException, ServiceException {
        Call call = prepareCall("readDataDiagnosticReadout");
        call.addParameter(new QName(RPCT_DIAG_ACCESS_NS, "diagReadoutIds"), 
                Constants.SOAP_ARRAY,
                ParameterMode.IN);              
        call.setReturnType(Constants.XSD_ANY);
        
        Bag[] readoutIds = new Bag[diagReadouts.length];
        for (int i = 0; i < diagReadouts.length; i++) {
            readoutIds[i] = (Bag) diagReadouts[i].getDiagId();
        } 
        Bag[] bagsArray = (Bag[]) call.invoke(new Object[] { readoutIds });
        DiagnosticReadoutData[] readoutData = new DiagnosticReadoutData[bagsArray.length];
        for (int i = 0; i < bagsArray.length; i++) {
            HashBag<Object> readoutDataBag = (HashBag<Object>) bagsArray[i];
            readoutData[i] = new DiagnosticReadoutDataImplAxis(readoutDataBag);
            
            /*DiagId diagId = new DiagIdImplAxis((HashBag<Object>) pulserInfoBag.get("id"));
            int maxLength = ((BigInteger) pulserInfoBag.get("maxLength")).intValue();
            int width = ((BigInteger) pulserInfoBag.get("width")).intValue();
            DiagId diagCtrlId = new DiagIdImplAxis((HashBag<Object>) pulserInfoBag.get("diagCtrlId"));
            pulserList[i] = new PulserImplAxis(diagId, maxLength, width, diagCtrlId, this);
            */
        }
        return readoutData;
    }
}
