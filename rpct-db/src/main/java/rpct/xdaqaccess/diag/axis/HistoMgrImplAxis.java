package rpct.xdaqaccess.diag.axis;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import rpct.xdaq.XdaqException;
import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.diag.DiagAccess;
import rpct.xdaqaccess.diag.DiagCtrl;
import rpct.xdaqaccess.diag.DiagId;
import rpct.xdaqaccess.diag.HistoMgr;

public class HistoMgrImplAxis implements HistoMgr {

    private DiagAccessImplAxis diagAccess;

    private DiagId diagId;

    private int binCount;

    private DiagId diagCtrlId;

    public HistoMgrImplAxis(DiagId diagId, int binCount, DiagId diagCtrlId,
            DiagAccessImplAxis diagAccess) {
        this.diagId = diagId;
        this.binCount = binCount;
        this.diagCtrlId = diagCtrlId;
        this.diagAccess = diagAccess;
    }

    public DiagId getDiagId() {
        return diagId;
    }

    public DiagAccess getDiagAccess() {
        return diagAccess;
    }

    public int getBinCount() {
        return binCount;
    }

    public DiagId getDiagCtrlId() {
        return diagCtrlId;
    }

    public DiagCtrl getDiagCtrl() {
        DiagCtrl diagCtrl = diagAccess.getDiagCtrl(diagCtrlId);
        if (diagCtrl == null) {
            throw new IllegalStateException("getDiagCtrlList should be called");
        }
        return diagCtrl;
    }

    public void reset() throws XdaqException {
        try {
            diagAccess.reset(new HistoMgr[] { this });
        } catch (RemoteException e) {
            throw new XdaqException(e);
        } catch (ServiceException e) {
            throw new XdaqException(e);
        }
    }

    public Binary[] readData() throws XdaqException {
        try {
            return diagAccess.readData(this);
        } catch (RemoteException e) {
            throw new XdaqException(e);
        } catch (ServiceException e) {
            throw new XdaqException(e);
        }
    }

}
