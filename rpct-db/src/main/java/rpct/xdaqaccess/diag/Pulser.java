package rpct.xdaqaccess.diag;

import java.math.BigInteger;

import rpct.xdaq.XdaqException;
import rpct.xdaq.axis.Binary;

public interface Pulser extends Diag {	

    int getMaxLength();  
    int getWidth();
    DiagCtrl getDiagCtrl();

    void configure(Binary[] data, BigInteger pulseLength, boolean repeat, int target) throws XdaqException;
}
