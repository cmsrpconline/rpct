package rpct.xdaqaccess.diag;


public interface DiagnosticReadoutEvent {
    int getBcn();
    int getEvNum();
    
    BxData[] getBxData();
}
