package rpct.xdaqaccess.diag;

import java.io.IOException;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;

import org.apache.axis.Constants;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.encoding.XMLType;

import rpct.xdaq.XdaqApplicationInfo;
import rpct.xdaq.XdaqException;
import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.diag.DiagCtrl.TriggerType;

public class Test {

    public static void test1() throws RemoteException, ServiceException, MalformedURLException, 
    XdaqException, InterruptedException {

        XdaqApplicationInfo info = new XdaqApplicationInfo(
                new URL("http://pccms8.igf.fuw.edu.pl:1972"), "XdaqTestBench", 2);
        
        HardwareRegistry hr = new HardwareRegistry();
        hr.registerXdaqApplication(info);
        DiagAccess diagAccess = hr.getXdaqApplicationAccess(info).getDiagAccess();

        DiagCtrl[] diagCtrls = diagAccess.getDiagCtrlList();
        for (DiagCtrl diagCtrl : diagCtrls) {
            System.out.println("DiagCtrl " + diagCtrl.getDiagId());
            System.out.println("State: " + diagCtrl.getState().toString());
        }
        HistoMgr[] histoMgrs = diagAccess.getHistoMgrList();
        for (HistoMgr histoMgr : histoMgrs) {
            System.out.println("HistoMgr " + histoMgr.getDiagId());
        }
        diagAccess.reset(histoMgrs);

        Pulser[] pulsers = diagAccess.getPulserList();
        for (Pulser pulser : pulsers) {
            System.out.println("Pulser " + pulser.getDiagId());
        }

        System.out.println("Reseting diag ctrls");
        diagAccess.reset(diagCtrls);

        Binary[] pulserData = new Binary[128];
        for (int i = 0; i < pulserData.length; i++) {
            pulserData[i] = new Binary((i % 8 == 0) ? 0xffffff : 0);
            System.out.println(pulserData[i].toString());
        }
        System.out.println("Configuring pulsers");
        diagAccess.configure(pulsers, pulserData, BigInteger
                .valueOf(pulserData.length), false, 0);

        System.out.println("Configuring diag ctrls");
        diagAccess.configure(diagCtrls, BigInteger.valueOf(40000000 * 10),
                TriggerType.MANUAL, 0);

        for (int i = 0; i < 1; i++) {
            System.out.println("Diagnostic run #" + (i + 1));
            System.out.println("Starting diag ctrls");
            diagAccess.start(diagCtrls);
            while (!diagAccess.checkCountingEnded(diagCtrls)) {
                System.out.println("Checking if all have finished");
                Thread.sleep(1000);
            }
            System.out.println("All diagnostics have finished");
            diagAccess.stop(diagCtrls);

            for (HistoMgr histoMgr : histoMgrs) {
                System.out.println("Data " + histoMgr.getDiagId() + ":");
                Binary[] data = histoMgr.readData();
                for (int bin = 0; bin < histoMgr.getBinCount(); bin++) {
                    System.out.print(data[bin].longValue());
                    System.out.print(' ');
                }
                System.out.println();
            }
        }
    }

    public static void test2() throws ServiceException, XdaqException, InterruptedException, IOException {

        XdaqApplicationInfo info = new XdaqApplicationInfo(
                new URL("http://pccms8.igf.fuw.edu.pl:1972"), "XdaqTestBench", 2);
        
        HardwareRegistry hr = new HardwareRegistry();
        hr.registerXdaqApplication(info);
        DiagAccess diagAccess = hr.getXdaqApplicationAccess(info).getDiagAccess();

        DiagCtrl[] diagCtrls = diagAccess.getDiagCtrlList();
        for (DiagCtrl diagCtrl : diagCtrls) {
            System.out.println("DiagCtrl " + diagCtrl.getDiagId());
            System.out.println("State: " + diagCtrl.getState().toString());
        }

        Pulser[] pulsers = diagAccess.getPulserList();
        for (Pulser pulser : pulsers) {
            System.out.println("Pulser " + pulser.getDiagId());
        }
        
        DiagnosticReadout[] readouts = diagAccess.getDiagnosticReadoutList();
        for (DiagnosticReadout readout : readouts) {
            System.out.println("Readout " + readout.getDiagId());            
        }

        System.out.println("Reseting diag ctrls");
        diagAccess.reset(diagCtrls);

        Binary[] pulserData = new Binary[128];
        for (int i = 0; i < pulserData.length; i++) {
            pulserData[i] = new Binary((i % 8 == 0) ? 0xffffff : 0);
            System.out.println(pulserData[i].toString());
        }
        System.out.println("Configuring pulsers");
        diagAccess.configure(pulsers, pulserData, BigInteger
                .valueOf(pulserData.length), false, 0);
        
       /* FileInputStream fis = new FileInputStream("/home/mpietrus/bin/testBxData.xml");
        int x = fis.available();
        byte b[]= new byte[x];
        fis.read(b);
        String muonsXml = new String(b);
        
        diagAccess.configure(pulsers, muonsXml, BigInteger.valueOf(10), false);
        System.out.println("Done");*/
        
        /*Set<Integer> masks[] = new HashSet[] { new HashSet() };
        masks[0].add(1);
        masks[0].add(2);
        masks[0].add(3);
        diagAccess.configure(readouts, masks, 0);*/
        
        System.out.println("Configuring diag ctrls");
        diagAccess.configure(diagCtrls, BigInteger.valueOf(1000),
                TriggerType.MANUAL, 0);

        System.out.println("Starting diag ctrls");
        diagAccess.start(diagCtrls);
        while (!diagAccess.checkCountingEnded(diagCtrls)) {
            System.out.println("Checking if all have finished");
            Thread.sleep(1000);
        }
        System.out.println("All diagnostics have finished");
        diagAccess.stop(diagCtrls);
        
        System.out.println("diagAccess.readData");
        DiagnosticReadoutData[] datas = diagAccess.readData(readouts);
        for (DiagnosticReadoutData data : datas) {            
            System.out.println(data);
            for (DiagnosticReadoutEvent event : data.getEvents()) {
                System.out.println(event);
                for (BxData bxData : event.getBxData()) {
                    System.out.println(bxData);      
                    for (LMuxBxData lmuxBxData : bxData.getLMuxBxData()) {
                        System.out.println(lmuxBxData);                              
                    }
                    for (Muon muon : bxData.getMuons()) {
                        System.out.println(muon);                        
                    }                    
                }                
            } 
        }
        System.out.println("diagAccess.readData Done");
        
        /*System.out.println("Configuring diag ctrls");
        diagAccess.configure(diagCtrls, BigInteger.valueOf(40000000 * 10),
                TriggerType.MANUAL);

        for (int i = 0; i < 1; i++) {
            System.out.println("Diagnostic run #" + (i + 1));
            System.out.println("Starting diag ctrls");
            diagAccess.start(diagCtrls);
            while (!diagAccess.checkCountingEnded(diagCtrls)) {
                System.out.println("Checking if all have finished");
                Thread.sleep(1000);
            }
            System.out.println("All diagnostics have finished");
            diagAccess.stop(diagCtrls);

            for (HistoMgr histoMgr : histoMgrs) {
                System.out.println("Data " + histoMgr.getOwnerId() + "."
                        + histoMgr.getName() + ":");
                Binary[] data = histoMgr.readData();
                for (int bin = 0; bin < histoMgr.getBinCount(); bin++) {
                    System.out.print(data[bin].longValue());
                    System.out.print(' ');
                }
                System.out.println();
            }
        }*/
    }

    public static void test3() throws ServiceException, XdaqException, InterruptedException, IOException {

        Service service = new Service();
        Call call = (Call) service.createCall();  
        call.setTargetEndpointAddress(new URL("http://localhost:8080/rpct-dbservice/services/rpctdbservice"));        
        //call.setSOAPActionURI(soapActionURI);
        call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS,
                Boolean.FALSE); 
        call.setOperationName(new QName("urn:xdaq-soap:3.0", "getNestedArray")); 
        call.setEncodingStyle(Constants.URI_SOAP11_ENC);
        call.setReturnType(XMLType.XSD_ANY);        
        //String ret =  (String) call.invoke( new Object [] {});
        Object ret = call.invoke( new Object [] {});
                                                                                 
        System.out.println("Got result : " + ret);
    }

    
    
    public static void main(String[] args) {
        try {
            test2();
            //test3();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
