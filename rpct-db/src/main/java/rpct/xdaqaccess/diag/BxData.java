package rpct.xdaqaccess.diag;


public interface BxData {
	int getIndex(); 
	
    int getPos();

    int getTriggers();

    int getChannelsValid();

    LMuxBxData[] getLMuxBxData();

    Muon[] getMuons();
}
