package rpct.xdaqaccess.diag;

import java.util.Set;

import rpct.xdaq.XdaqException;

public interface DiagnosticReadout extends Diag {

    DiagId getDiagId();

    int getAreaLen();

    int getAreaWidth();

    int getMaskNum();

    int getMaskWidth();

    int getDataWidth();

    int getTrgNum();

    int getTimerWidth();

    DiagCtrl getDiagCtrl();
        
    void configure(Set<Integer>[] masks, int daqDelay) throws XdaqException;

    void configure(Set<Integer>[] masks, int daqDelay, int dataSel, int extSel)
            throws XdaqException;
}
