package rpct.xdaqaccess.diag;

public interface DiagnosticReadoutData {

    DiagId getDiagId();

    boolean isRunning();
    boolean isLost();

    DiagnosticReadoutEvent[] getEvents();
}
