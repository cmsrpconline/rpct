package rpct.xdaqaccess.diag;

public interface DiagId {

    public abstract int getOwnerId();

    public abstract String getName();

}