package rpct.xdaqaccess;

public class XdaqAccessException extends RuntimeException {

    public XdaqAccessException() {
    }

    public XdaqAccessException(String message) {
        super(message);
    }

    public XdaqAccessException(Throwable cause) {
        super(cause);
    }

    public XdaqAccessException(String message, Throwable cause) {
        super(message, cause);
    }

}
