package rpct.xdaqaccess.axis;

import java.math.BigInteger;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import rpct.xdaq.axis.bag.Bag;
import rpct.xdaqaccess.Board;
import rpct.xdaqaccess.Crate;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.XdaqAccessException;

public class BoardImplAxis extends HardwareItemImplAxis implements Board {

    private CrateImplAxis crate;
    int position;
    private DeviceImplAxis[] devices;
    
    public BoardImplAxis(CrateImplAxis crate, Bag<Object> bag, IIAccessImplAxis iiAccessImplAxis, HardwareRegistry hardwareRegistry) {
        super(bag, iiAccessImplAxis, hardwareRegistry);
        this.position = ((BigInteger) bag.get("position")).intValue();
        this.crate = crate;  
        getHardwareRegistry().registerItem(this); 
    }
    
    protected void setDevices(DeviceImplAxis[] devices) {
        if (devices == null) {
            throw new NullPointerException("devices");
        }
        if (this.devices != null) {
            throw new IllegalStateException("devices already initialized");
        }
        this.devices = devices;
    }

    public Device[] getDevices() {
        if (devices == null) {
            try {
                getIIAccess().loadDevices(this);
            } catch (RemoteException e) {
                throw new XdaqAccessException(e);
            } catch (ServiceException e) {
                throw new XdaqAccessException(e);
            }
        }
        return devices;
    }

    public Crate getCrate() {
        return crate;
    }
    
    public int getPosition() {
        return position;
    }
}
