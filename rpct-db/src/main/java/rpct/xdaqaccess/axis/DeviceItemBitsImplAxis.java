package rpct.xdaqaccess.axis;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.DeviceDesc;
import rpct.xdaqaccess.DeviceItemBits;
import rpct.xdaqaccess.DeviceItemInfo;
import rpct.xdaqaccess.DeviceItemVector;

public class DeviceItemBitsImplAxis extends DeviceItemImplAxis implements
        DeviceItemBits {

    DeviceItemVectorImplAxis vector = null;
    
    protected DeviceItemBitsImplAxis(DeviceImplAxis device, DeviceDesc.ItemDesc desc, 
            IIAccessImplAxis iiAccessImplAxis) {
        super(device, desc, iiAccessImplAxis);
    }

    public DeviceItemVector getVector() {
        if (vector == null) {
            vector = (DeviceItemVectorImplAxis) getDevice().getItem(getDesc().getParent().getName());
        }
        return vector;
    }

    public Binary readBinary(int index) throws RemoteException, ServiceException {
        return iiAccessImplAxis.readII(this, index, 1)[0];
    }

    public void writeBinary(int index, Binary value) throws RemoteException, ServiceException {
        iiAccessImplAxis.writeII(this, index, new Binary[] {value});
    }

    public long read(int index) throws RemoteException, ServiceException {
        return readBinary(index).longValue();
    }

    public void write(int index, long value) throws RemoteException, ServiceException {
        writeBinary(index, Binary.valueOf(value));
    }
}
