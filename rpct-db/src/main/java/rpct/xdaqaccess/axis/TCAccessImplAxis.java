package rpct.xdaqaccess.axis;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;
import javax.xml.rpc.ServiceException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.axis.AxisFault;
import org.apache.axis.Constants;
import org.apache.axis.client.Call;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.XdaqApplication;
import rpct.db.domain.configuration.XdaqExecutive;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqApplicationInfo;
import rpct.xdaq.XdaqException;
import rpct.xdaq.axis.XdaqAccessImplAxisBase;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.TCAccess;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;

public class TCAccessImplAxis extends XdaqAccessImplAxisBase implements TCAccess {

    private HardwareRegistry hardwareRegistry;
    private String name;
    
    public TCAccessImplAxis(XdaqApplicationInfo xdaqApplicationInfo, HardwareRegistry hardwareRegistry, String name) {
        super(xdaqApplicationInfo, XDAQ_NS_URI);
        this.hardwareRegistry = hardwareRegistry;
        this.name = name;
    }

    public String getName() {
        return name;
    }    
    
    public void sendResetRMBs() throws XdaqException, RemoteException {
        Call call;
        try {
            call = prepareCall("resetRmbs");
        } catch (ServiceException e1) {
            throw new XdaqException(e1);
        }  
/*        call.addParameter(new QName(XDAQ_NS_URI, "CommandPar"), 
                Constants.XSD_STRING,
                ParameterMode.IN);*/
        call.setReturnType(Constants.XSD_ANY); 

        call.invoke(new Object[] { });
         
    }
        
    /*public void sendResetRMBs() throws XdaqException, RemoteException {
        Call call;
        try {
            call = prepareCall("resetRmbs");
        } catch (ServiceException e1) {
            throw new XdaqException(e1);
        }  
        call.addParameter(new QName(XDAQ_NS_URI, "CommandPar"), 
                Constants.XSD_STRING,
                ParameterMode.IN);
        call.setReturnType(Constants.XSD_ANY); 

        call.invoke(new Object[] { });
         
    }*/

    public static void main(String[] args) throws DataAccessException {
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        if(args.length == 0) {            
            String[] argsL = {
/*                    "TC_0",
                    "TC_1",
                    "TC_2",
                    "TC_3",
                    "TC_4",
                    "TC_5",
                    "TC_6",
                    "TC_7",
                    "TC_8",                             
                    "TC_9",
                    "TC_10",
                    "TC_11",*/
                    "PASTEURA_TC"
            };

            args = argsL;
        }

        System.out.println("Selected crates");
        for(int i = 0; i < args.length; i++) {
            System.out.print(args[i] + " ");
        }          
           
        
        try {
            List<TCAccess> tcAccesses = new ArrayList<TCAccess>();
            for(int i = 0; i < args.length; i++) {
                TriggerCrate triggerCrate = (TriggerCrate)equipmentDAO.getCrateByName(args[i]);
                XdaqApplication xdaqApplication = configurationDAO.getXdaqApplication(triggerCrate);

                if (xdaqApplication == null) {
                    throw new IllegalArgumentException(
                            "XdaqApplication not found for triggerCrate " + triggerCrate);
                }
                XdaqExecutive xdaqExecutive = xdaqApplication.getXdaqExecutive();
                URL url = new URL("http://" + xdaqExecutive.getHost() + ":" + xdaqExecutive.getPort());
                XdaqApplicationInfo info = new XdaqApplicationInfo(url, xdaqApplication.getClassName(), xdaqApplication.getInstance());

                TCAccess tcAccess = new TCAccessImplAxis(info, null, triggerCrate.getName());
                tcAccesses.add(tcAccess);
            }
            
            System.out.println("Commands:");
            System.out.println("r - resetRMB");
            System.out.println("q - quit");
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            
            String command = null;
            while (true) {
                command = br.readLine() ;
                if(command.equals("r")) {
                    for(TCAccess tcAccess : tcAccesses) {
                        try {
                            tcAccess.sendResetRMBs();
                            System.out.println("sendResetRMBs Done.");
                        } catch (XdaqException e) {
                            // TODO Auto-generated catch block
                            System.err.println("error during connetion with the xdaq " + tcAccess.getName());
                            System.err.println(e.getMessage());
                        }
                        catch (Exception e) {
                            // TODO Auto-generated catch block
                            System.err.println(e.getMessage());
                            System.err.println("error during connetion with the xdaq " + tcAccess.getName());
                        }
                    }
                    
                }
                else if(command.equals("q")) {
                    break;
                }
            }
            
            
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            context.closeSession();
        }
    }
}
