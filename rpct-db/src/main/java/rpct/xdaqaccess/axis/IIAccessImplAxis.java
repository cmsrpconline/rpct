package rpct.xdaqaccess.axis;

import java.math.BigInteger;
import java.rmi.RemoteException;

import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;
import javax.xml.rpc.ServiceException;

import org.apache.axis.Constants;
import org.apache.axis.client.Call;
import org.apache.axis.types.HexBinary;

import rpct.xdaq.XdaqApplicationInfo;
import rpct.xdaq.axis.Binary;
import rpct.xdaq.axis.XdaqAccessImplAxisBase;
import rpct.xdaq.axis.bag.Bag;
import rpct.xdaqaccess.AccessType;
import rpct.xdaqaccess.Board;
import rpct.xdaqaccess.Crate;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.DeviceDesc;
import rpct.xdaqaccess.DeviceItem;
import rpct.xdaqaccess.DeviceItemInfo;
import rpct.xdaqaccess.DeviceItemType;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.IIAccess;

public class IIAccessImplAxis extends XdaqAccessImplAxisBase implements IIAccess {

    private HardwareRegistry hardwareRegistry;

    public IIAccessImplAxis(XdaqApplicationInfo xdaqApplicationInfo, HardwareRegistry hardwareRegistry) {
        super(xdaqApplicationInfo, XDAQ_NS_URI);
        this.hardwareRegistry = hardwareRegistry;
    }    
    
    public Crate[] getCrates(boolean nested) throws RemoteException, ServiceException {

        Call call = prepareCall("getCratesInfo");
        call.addParameter(new QName(XDAQ_NS_URI, "nested"), Constants.XSD_BOOLEAN, ParameterMode.IN);
        call.setReturnType(Constants.SOAP_ARRAY);
        Bag<Object>[] crateInfos = (Bag<Object>[]) call.invoke(new Object[] { Boolean.valueOf(nested) });
        CrateImplAxis[] crates = new CrateImplAxis[crateInfos.length];
        for (int c = 0; c < crateInfos.length; c++) {
            Bag<Object> crateInfo = crateInfos[c];
            CrateImplAxis crate = new CrateImplAxis(crateInfo, this, hardwareRegistry);
            crates[c] = crate;

            if (nested) {
                Bag<Object>[] boardInfos = (Bag<Object>[]) crateInfo.get("boards");
                BoardImplAxis[] boards = new BoardImplAxis[boardInfos.length];
                for (int b = 0; b < boardInfos.length; b++) {
                    Bag<Object> boardInfo = boardInfos[b];
                    BoardImplAxis board = new BoardImplAxis(crate, boardInfo, this, hardwareRegistry);
                    boards[b] = board;

                    Bag<Object>[] deviceInfos = (Bag<Object>[]) boardInfo.get("devices");
                    DeviceImplAxis[] devices = new DeviceImplAxis[deviceInfos.length];
                    for (int d = 0; d < deviceInfos.length; d++) {
                        Bag<Object> deviceInfo = deviceInfos[d];
                        String deviceType = (String) deviceInfo.get("type");
                        DeviceDesc deviceDesc = hardwareRegistry.findDeviceDesc(deviceType);
                        if (deviceDesc == null) {
                            deviceDesc = new DeviceDesc(deviceType);
                            hardwareRegistry.registerDeviceDesc(deviceDesc);
                            
                            Bag<Object>[] deviceItemInfos = (Bag<Object>[]) deviceInfo.get("items");
                            fillDeviceItemInfos(deviceDesc, deviceItemInfos);
                        }
                        
                        DeviceImplAxis device = new DeviceImplAxis(board, deviceDesc, deviceInfo, this, hardwareRegistry);
                        devices[d] = device;
                    }
                    board.setDevices(devices);
                }

                crate.setBoards(boards);
            }
        }
        return crates;
    }


    private void fillDeviceItemInfos(DeviceDesc deviceDesc, Bag<Object>[] deviceItemInfos) {
        for (int i = 0; i < deviceItemInfos.length; i++) {
            Bag<Object> info = deviceItemInfos[i];
            deviceDesc.addItem(
                    ((BigInteger) info.get("id")).intValue(), 
                    ((String) info.get("name")).trim().toUpperCase(), 
                    ((String) info.get("description")).trim().toUpperCase(), 
                    DeviceItemType.valueOf((String) info.get("type")), 
                    ((BigInteger) info.get("width")).intValue(), 
                    ((BigInteger) info.get("number")).intValue(), 
                    AccessType.valueOf((String) info.get("accessType")),
                    ((BigInteger) info.get("parentId")).intValue());
        }
    }

    public Board[] loadBoards(Crate crate) throws RemoteException, ServiceException {

        Call call = prepareCall("getBoardsInfo");
        call.addParameter(new QName(XDAQ_NS_URI, "crateId"), Constants.XSD_INTEGER, ParameterMode.IN);
        call.setReturnType(Constants.SOAP_ARRAY);
        Bag<Object>[] boardInfos = (Bag<Object>[]) call.invoke(new Object[] { BigInteger.valueOf(crate
                .getId()) });
        BoardImplAxis[] boards = new BoardImplAxis[boardInfos.length];
        for (int b = 0; b < boardInfos.length; b++) {
            Bag<Object> boardInfo = boardInfos[b];
            BoardImplAxis board = new BoardImplAxis((CrateImplAxis) crate, boardInfo, this, hardwareRegistry);
            boards[b] = board;
        }
        ((CrateImplAxis) crate).setBoards(boards);
        return boards;
    }

    public Device[] loadDevices(Board board) throws RemoteException, ServiceException {

        Call call = prepareCall("getDevicesInfo");
        call.addParameter(new QName(XDAQ_NS_URI, "boardId"), Constants.XSD_INTEGER, ParameterMode.IN);
        call.setReturnType(Constants.SOAP_ARRAY);
        Bag<Object>[] deviceInfos = (Bag<Object>[]) call.invoke(new Object[] { BigInteger.valueOf(board
                .getId()) });
        DeviceImplAxis[] devices = new DeviceImplAxis[deviceInfos.length];
        for (int d = 0; d < deviceInfos.length; d++) {
            Bag<Object> deviceInfo = deviceInfos[d];
            String deviceType = (String) deviceInfo.get("type");
            DeviceDesc deviceDesc = hardwareRegistry.findDeviceDesc(deviceType);
            if (deviceDesc == null) {
                deviceDesc = new DeviceDesc(deviceType);
                hardwareRegistry.registerDeviceDesc(deviceDesc);
            }            
            DeviceImplAxis device = new DeviceImplAxis((BoardImplAxis) board, deviceDesc, deviceInfo, this, hardwareRegistry);            
            devices[d] = device;
        }
        ((BoardImplAxis) board).setDevices(devices);
        return devices;
    }

    public void loadDeviceItems(Device device) throws RemoteException, ServiceException {

        DeviceDesc deviceDesc = device.getDeviceDesc();
        if (deviceDesc == null) {
            throw new NullPointerException("device.getDeviceDesc() == null");
        }
        if (deviceDesc.getItems() != null) {
            throw new IllegalStateException("cannot load items, items already loaded");
        }
        
        Call call = prepareCall("getDeviceItemsInfo");
        call.addParameter(new QName(XDAQ_NS_URI, "deviceId"), Constants.XSD_INTEGER, ParameterMode.IN);
        call.setReturnType(Constants.SOAP_ARRAY);
        Bag<Object>[] deviceItemInfos = (Bag<Object>[]) call.invoke(new Object[] { BigInteger.valueOf(device
                .getId()) });
        fillDeviceItemInfos(deviceDesc, deviceItemInfos);

        /*DeviceItemInfo[] itemInfos = new DeviceItemInfo[deviceItemInfos.length];
        for (int i = 0; i < deviceItemInfos.length; i++) {
            Bag<Object> info = deviceItemInfos[i];
            itemInfos[i] = new DeviceItemInfo(((BigInteger) info.get("id")).intValue(), ((String) info
                    .get("name")).trim().toUpperCase(), ((String) info.get("description")).trim()
                    .toUpperCase(), DeviceItemType.valueOf((String) info.get("type")), ((BigInteger) info
                    .get("width")).intValue(), ((BigInteger) info.get("number")).intValue(),
                    ((BigInteger) info.get("parentId")).intValue(), AccessType.valueOf((String) info
                            .get("accessType")));
        }
        ((DeviceImplAxis) device).createItems(itemInfos);*/
    }

    public Binary[] readII(DeviceItem deviceItem, int startPos, int count) throws RemoteException,
            ServiceException {

        Call call = prepareCall("readII");
        call.addParameter(new QName(XDAQ_NS_URI, "deviceId"), Constants.XSD_INTEGER, ParameterMode.IN);
        call.addParameter(new QName(XDAQ_NS_URI, "deviceItemId"), Constants.XSD_INTEGER, ParameterMode.IN);
        call.addParameter(new QName(XDAQ_NS_URI, "startPos"), Constants.XSD_INTEGER, ParameterMode.IN);
        call.addParameter(new QName(XDAQ_NS_URI, "count"), Constants.XSD_INTEGER, ParameterMode.IN);
        call.setReturnType(Constants.SOAP_ARRAY);
        byte[][] byteArrayList = (byte[][]) call.invoke(new Object[] {
                BigInteger.valueOf(deviceItem.getDevice().getId()), BigInteger.valueOf(deviceItem.getId()),
                BigInteger.valueOf(startPos), BigInteger.valueOf(count) });
        Binary[] result = new Binary[byteArrayList.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = new Binary(byteArrayList[i], deviceItem.getDesc().getWidth());
        }
        return result;
    }

    public void writeII(DeviceItem deviceItem, int startPos, Binary[] data) throws RemoteException,
            ServiceException {

        Call call = prepareCall("writeII");
        call.addParameter(new QName(XDAQ_NS_URI, "deviceId"), Constants.XSD_INTEGER, ParameterMode.IN);
        call.addParameter(new QName(XDAQ_NS_URI, "deviceItemId"), Constants.XSD_INTEGER, ParameterMode.IN);
        call.addParameter(new QName(XDAQ_NS_URI, "startPos"), Constants.XSD_INTEGER, ParameterMode.IN);
        call.addParameter(new QName(XDAQ_NS_URI, "data"), Constants.SOAP_ARRAY, ParameterMode.IN);
        call.setReturnType(Constants.XSD_ANY);

        HexBinary[] arrayData = new HexBinary[data.length];
        for (int i = 0; i < data.length; i++) {
            arrayData[i] = new HexBinary(data[i].getBytes());
        }
        call.invoke(new Object[] { BigInteger.valueOf(deviceItem.getDevice().getId()),
                BigInteger.valueOf(deviceItem.getId()), BigInteger.valueOf(startPos), arrayData });
    }
}
