package rpct.xdaqaccess.axis;

import java.util.ArrayList;
import java.util.List;

import rpct.xdaqaccess.DeviceDesc;
import rpct.xdaqaccess.DeviceItemBits;
import rpct.xdaqaccess.DeviceItemInfo;
import rpct.xdaqaccess.DeviceItemVector;

public class DeviceItemVectorImplAxis extends DeviceItemImplAxis implements
        DeviceItemVector {

    List<DeviceItemBits> bitsList = new ArrayList<DeviceItemBits>();
    
    protected DeviceItemVectorImplAxis(DeviceImplAxis device, DeviceDesc.ItemDesc desc, 
            IIAccessImplAxis iiAccessImplAxis) {
        super(device, desc, iiAccessImplAxis);
    }
    
    protected void addBits(DeviceItemBitsImplAxis bits) {
        bitsList.add(bits);
    }

    public DeviceItemBits[] getBits() {
        return bitsList.toArray(new DeviceItemBits[0]);
    }
    
}
