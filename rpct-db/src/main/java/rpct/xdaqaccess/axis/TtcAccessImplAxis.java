package rpct.xdaqaccess.axis;

import java.io.IOException;

import javax.xml.rpc.ServiceException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.axis.AxisFault;
import org.apache.axis.client.Call;

import rpct.xdaq.XdaqApplicationInfo;
import rpct.xdaq.XdaqException;
import rpct.xdaq.axis.XdaqAccessImplAxisBase;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.TtcAccess;

public class TtcAccessImplAxis extends XdaqAccessImplAxisBase implements TtcAccess {
    
    private HardwareRegistry hardwareRegistry;
    
    public TtcAccessImplAxis(XdaqApplicationInfo xdaqApplicationInfo, HardwareRegistry hardwareRegistry) {
        super(xdaqApplicationInfo, XDAQ_NS_URI);
        this.hardwareRegistry = hardwareRegistry;
    }
    
    public void sendVmeBGo() throws XdaqException {
    	try {
    		MessageFactory mf = MessageFactory.newInstance();
    		SOAPMessage message = mf.createMessage();
    		SOAPPart     part     = message.getSOAPPart();
    		SOAPEnvelope envelope = part.getEnvelope();
    		SOAPBody     body     = envelope.getBody();

    		// Add command element
			SOAPElement body_element = body.addBodyElement(
				envelope.createName("userCommand", "xdaq", XDAQ_NS_URI));
			
			body.addAttribute(envelope.createName("CommandPar", "xdaq", XDAQ_NS_URI), "Send VME-BGO");
	    	// Set SOAPAction header with a default target
	    	message.getMimeHeaders().addHeader("SOAPAction", getSoapActionURI());
	    	//message.writeTo(System.out);
			Call call = prepareCall("userCommand");
	    	call.invoke((org.apache.axis.Message) message);
	    	
		} catch (SOAPException e) {
            throw new XdaqException(e);
		} catch (ServiceException e) {
            throw new XdaqException(e);
		} catch (AxisFault e) {
            throw new XdaqException(e);
		} catch (IOException e) {
            throw new XdaqException(e);
		}
    	 

        // !!! The following does not work as TTCciControl SOAP interface is somewhat strange
        /*Call call;
		try {
			call = prepareCall("userCommand");
		} catch (ServiceException e1) {
            throw new XdaqException(e1);
		}  
        call.addParameter(new QName(XDAQ_NS_URI, "CommandPar"), 
                Constants.XSD_STRING,
                ParameterMode.IN);
        call.setReturnType(Constants.XSD_ANY); 
        
        try {       
            call.invoke(new Object[] {"Send VME-BGO"});           
        } catch (RemoteException e) {
            throw new XdaqException(e);
        }  */
    } 
    
    
    /*public void sendVmeBGo1() throws XdaqException {
        // !!! The following does not work as TTCciControl SOAP interface is somewhat strange
        Call call;
		try {
			call = prepareCall("userCommand");
		} catch (ServiceException e1) {
            throw new XdaqException(e1);
		}  
        call.addParameter(new QName(XDAQ_NS_URI, "CommandPar"), 
                Constants.XSD_STRING,
                ParameterMode.IN);
        call.setReturnType(Constants.XSD_ANY); 
        
        try {       
            call.invoke(new Object[] {"Send VME-BGO"});           
        } catch (RemoteException e) {
            throw new XdaqException(e);
        }  
    } */
    
    public void sendVmeBGo2() throws XdaqException {
    	try {
    		MessageFactory mf = MessageFactory.newInstance();
    		SOAPMessage message = mf.createMessage();
    		SOAPPart     part     = message.getSOAPPart();
    		SOAPEnvelope envelope = part.getEnvelope();
    		SOAPBody     body     = envelope.getBody();

    		// Add command element
			SOAPElement body_element = body.addBodyElement(
				envelope.createName("userCommand", "xdaq", XDAQ_NS_URI));
			
			body.addAttribute(envelope.createName("CommandPar", "xdaq", XDAQ_NS_URI), "Send VME-BGO");
	    	// Set SOAPAction header with a default target
	    	message.getMimeHeaders().addHeader("SOAPAction", getSoapActionURI());
	    	//message.writeTo(System.out);
			Call call = prepareCall("userCommand");
	    	call.invoke((org.apache.axis.Message) message);
	    	
		} catch (SOAPException e) {
            throw new XdaqException(e);
		} catch (ServiceException e) {
            throw new XdaqException(e);
		} catch (AxisFault e) {
            throw new XdaqException(e);
		} catch (IOException e) {
            throw new XdaqException(e);
		}    
    } 
}
