package rpct.xdaqaccess.axis;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import rpct.xdaq.axis.bag.Bag;
import rpct.xdaqaccess.Board;
import rpct.xdaqaccess.Crate;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.XdaqAccessException;

public class CrateImplAxis extends HardwareItemImplAxis implements Crate {

    private BoardImplAxis[] boards = null;
    
    public CrateImplAxis(Bag<Object> bag, IIAccessImplAxis iiAccessImplAxis, HardwareRegistry hardwareRegistry) {
        super(bag, iiAccessImplAxis, hardwareRegistry);
        getHardwareRegistry().registerItem(this);
    }
    
    public void setBoards(BoardImplAxis[] boards) {
        this.boards = boards;
    }

    public Board[] getBoards() {
        if (boards == null) {
            try {
                getIIAccess().loadBoards(this);
            } catch (RemoteException e) {
                throw new XdaqAccessException(e);
            } catch (ServiceException e) {
                throw new XdaqAccessException(e);
            }
        }
        return boards; 
    }
}
