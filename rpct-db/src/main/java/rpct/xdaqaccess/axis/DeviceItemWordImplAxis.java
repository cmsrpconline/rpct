package rpct.xdaqaccess.axis;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.DeviceDesc;
import rpct.xdaqaccess.DeviceItemInfo;
import rpct.xdaqaccess.DeviceItemWord;

public class DeviceItemWordImplAxis extends DeviceItemImplAxis implements
DeviceItemWord {    
    
    protected DeviceItemWordImplAxis(DeviceImplAxis device, DeviceDesc.ItemDesc desc, IIAccessImplAxis iiAccessImplAxis) {
        super(device, desc, iiAccessImplAxis);
    }
    
    public Binary readBinary(int index) throws RemoteException, ServiceException {
        return iiAccessImplAxis.readII(this, index, 1)[0];
    }
    
    public void writeBinary(int index, Binary value) throws RemoteException, ServiceException {
        iiAccessImplAxis.writeII(this, index, new Binary[] {value});
    }
    
    public long read(int index) throws RemoteException, ServiceException {
        return readBinary(index).longValue();
    }
    
    public void write(int index, long value) throws RemoteException, ServiceException {
        writeBinary(index, Binary.valueOf(value));
    }
}
