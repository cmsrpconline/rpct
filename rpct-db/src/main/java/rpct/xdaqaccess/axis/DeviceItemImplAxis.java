package rpct.xdaqaccess.axis;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.DeviceItem;
import rpct.xdaqaccess.DeviceItemType;
import rpct.xdaqaccess.DeviceDesc;

public class DeviceItemImplAxis implements DeviceItem {

    private DeviceDesc.ItemDesc desc;
    private DeviceImplAxis device; 
    protected IIAccessImplAxis iiAccessImplAxis;
    
    protected DeviceItemImplAxis(DeviceImplAxis device, DeviceDesc.ItemDesc desc, IIAccessImplAxis iiAccessImplAxis) {
        this.device = device;
        this.desc = desc;
        this.iiAccessImplAxis = iiAccessImplAxis;
    }

    public DeviceDesc.ItemDesc getDesc() {
        return desc;
    }

    public Device getDevice() {
        return device;
    }

    public int getId() {
        return desc.getId();
    }

    public String getName() {
        return desc.getName();
    }
    public int getNumber() {
        return desc.getNumber();
    }

    public DeviceItemType getType() {
        return desc.getType();
    }

    public long read() throws RemoteException, ServiceException {
        return readBinary().longValue();
    }

    public void write(long value) throws RemoteException, ServiceException {
        writeBinary(Binary.valueOf(value));
    }

    public Binary readBinary() throws RemoteException, ServiceException {
        return iiAccessImplAxis.readII(this, 0, 1)[0];
    }

    public void writeBinary(Binary value) throws RemoteException, ServiceException {
        iiAccessImplAxis.writeII(this, 0, new Binary[] {value});
    }
    
}
