package rpct.xdaqaccess.axis;


import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.DeviceDesc;
import rpct.xdaqaccess.DeviceItemArea;
import rpct.xdaqaccess.DeviceItemInfo;

public class DeviceItemAreaImplAxis extends DeviceItemImplAxis implements
        DeviceItemArea {

    protected DeviceItemAreaImplAxis(DeviceImplAxis device, DeviceDesc.ItemDesc desc, IIAccessImplAxis iiAccessImplAxis) {
        super(device, desc, iiAccessImplAxis);
    }

    public Binary[] readAreaBinary() throws RemoteException, ServiceException {
        return iiAccessImplAxis.readII(this, 0, getDesc().getNumber());
    }

    public Binary[] readAreaBinary(int startPos, int count) throws RemoteException, ServiceException {
        if (count > (getDesc().getNumber() - startPos)) {
            throw new IllegalArgumentException("count exceeds memory size");
        }
        return iiAccessImplAxis.readII(this, startPos, count);
    }

    public void writeAreaBinary(Binary[] value) throws RemoteException, ServiceException {
        iiAccessImplAxis.writeII(this, 0, value);                        
    }

    public void writeAreaBinary(int startPos, Binary[] value) throws RemoteException, ServiceException {
        if (value.length > (getDesc().getNumber() - startPos)) {
            throw new IllegalArgumentException("count exceeds memory size");
        }
        iiAccessImplAxis.writeII(this, startPos, value);                        
    }

}
