/**
 * 
 */
package rpct.xdaqaccess.axis;

public class FecStatusResponse {
    private boolean fecIsGood;
    private int fecStatus;

    public FecStatusResponse(boolean fecIsGood, int fecStatus) {
        super();
        this.fecIsGood = fecIsGood;
        this.fecStatus = fecStatus;
    }
    public boolean isFecIsGood() {
        return fecIsGood;
    }
    public void setFecIsGood(boolean fecIsGood) {
        this.fecIsGood = fecIsGood;
    }
    public int getFecStatus() {
        return fecStatus;
    }
    public void setFecStatus(int fecStatus) {
        this.fecStatus = fecStatus;
    }
    
}