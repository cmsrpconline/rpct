package rpct.xdaqaccess.axis;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Calendar;
import java.text.SimpleDateFormat;

import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;
import javax.xml.rpc.ServiceException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.axis.AxisFault;
import org.apache.axis.Constants;
import org.apache.axis.client.Call;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.XdaqApplication;
import rpct.db.domain.configuration.XdaqExecutive;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqApplicationInfo;
import rpct.xdaq.XdaqException;
import rpct.xdaq.axis.XdaqAccessImplAxisBase;
import rpct.xdaq.axis.bag.Bag;
import rpct.xdaq.axis.bag.HashBag;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.LBoxAccess;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;
import rpct.xdaqaccess.synchro.DefOut;

public class LBoxAccessImplAxis extends XdaqAccessImplAxisBase implements LBoxAccess, Runnable {
    private HardwareRegistry hardwareRegistry;
    private String name;
    private PrintStream outStrm;
    private PrintStream errStrm;
    private String commandToExecute;
    private Object retunedObj;
    private String selctedConfigKey;

	public String getSelctedConfigKey() {
		return selctedConfigKey;
	}

	public void setSelctedConfigKey(String selctedConfigKey) {
		this.selctedConfigKey = selctedConfigKey;
	}

	public void setCommandToExecute(String commandToExecute) {
		this.commandToExecute = commandToExecute;
	}
    
    /**
     * @return the Object returned by the SOAP function called in the run()
     */
    public Object getRetunedObj() {
		return retunedObj;
	}
    
	public String getName() {
        return name;
    }
    public static String RPCT_LBOX_ACCESS_NS = "urn:rpct-lbox-access:1.0";
    public LBoxAccessImplAxis(XdaqApplicationInfo xdaqApplicationInfo, HardwareRegistry hardwareRegistry, String name) {
        //public static String RPCT_LBOX_ACCESS_NS = "urn:rpct-lbox-access:1.0";
        super(xdaqApplicationInfo, RPCT_LBOX_ACCESS_NS);
        this.hardwareRegistry = hardwareRegistry;
        this.name = name;
        
        outStrm = System.out;
        errStrm = System.err;
    }

    public void sendConfigure() throws XdaqException {
        Call call;
        try {
            call = prepareCall("configure");           
        } catch (ServiceException e1) {
            throw new XdaqException(e1);
        }  

        call.setReturnType(Constants.XSD_ANY); 

        try {
            call.invoke(new Object[] { });
        } catch (RemoteException e) {
            //e.printStackTrace();
            throw new XdaqException(e);
        }           
    }

    public void sendSynchronizeLinks() throws XdaqException {
        Call call;
        try {
            call = prepareCall("synchronizeLinks");
        } catch (ServiceException e1) {
            throw new XdaqException(e1);
        }  

        call.setReturnType(Constants.XSD_ANY); 

        try {
            call.invoke(new Object[] { });
        } catch (RemoteException e) {
            //e.printStackTrace();
            throw new XdaqException(e);
        }           
    }
    
    public FecStatusResponse sendResetFEC() throws XdaqException {
        Call call;
        try {
            call = prepareCall("resetFec");
        } catch (ServiceException e1) {
            throw new XdaqException(e1);
        }  

        call.setReturnType(Constants.XSD_ANY); 

        try {
            HashBag<Object> bag = (HashBag<Object>)call.invoke(new Object[] { });
            Boolean fecIsGood = (Boolean) bag.get("FecIsGood");
            BigInteger fecStatus = (BigInteger) bag.get("FecStatus");
            
            FecStatusResponse fecStatusResponse = new FecStatusResponse(fecIsGood.booleanValue(), fecStatus.intValue());                        
            
            return fecStatusResponse;
        } catch (RemoteException e) {
            //e.printStackTrace(); 
            throw new XdaqException(e);
        }   
        //return new FecStatusResponse(false, -1);
    } 

    public String sendStartMonitoring() throws XdaqException {
        Call call;
        try {
            call = prepareCall("startMonitoring");
        } catch (ServiceException e1) {
            throw new XdaqException(e1);
        }  

        call.setReturnType(Constants.XSD_ANY); 

        try {
            String result = (String)call.invoke(new Object[] { });
            return result;
        } catch (RemoteException e) {            
            //e.printStackTrace();
            throw new XdaqException(e);
            //return "error";
        }           
    }
    
    public String sendStopMonitoring() throws XdaqException {
        Call call;
        try {
            call = prepareCall("stopMonitoring");
        } catch (ServiceException e1) {
            throw new XdaqException(e1);
        }  

        call.setReturnType(Constants.XSD_ANY); 

        try {
            String result = (String)call.invoke(new Object[] { });
            return result;
        } catch (RemoteException e) {            
            //e.printStackTrace();
            throw new XdaqException(e);
            //return "error";
        }           
    }
    
    public String sendStartHistoMonitoring() throws XdaqException {
        Call call;
        try {
            call = prepareCall("startHistoMonitoring");
        } catch (ServiceException e1) {
            throw new XdaqException(e1);
        }  

        call.setReturnType(Constants.XSD_ANY); 

        try {
            String result = (String)call.invoke(new Object[] { });
            return result;
        } catch (RemoteException e) {            
            //e.printStackTrace();
            throw new XdaqException(e);            
        }           
    }
    
    public String sendStopHistoMonitoring() throws XdaqException {
        Call call;
        try {
            call = prepareCall("stopHistoMonitoring");
        } catch (ServiceException e1) {
            throw new XdaqException(e1);
        }  

        call.setReturnType(Constants.XSD_ANY); 

        try {
            String result = (String)call.invoke(new Object[] { });
            return result;
        } catch (RemoteException e) {            
            //e.printStackTrace();
            //return "error";
            throw new XdaqException(e);
        }           
    }
    
    public Bag<Object>[] sendGetMonitoring() throws XdaqException {
        Call call;
        try {
            call = prepareCall("getMonitoringData");
        } catch (ServiceException e1) {
            throw new XdaqException(e1);
        }  

        call.setReturnType(Constants.SOAP_ARRAY); 

        try {
            Bag<Object>[] monBags = (Bag<Object>[])call.invoke(new Object[] { });
/*        	for(Bag monBag : monBags) {
        		MonitorableStatusInfoImplAxis monInfo = new MonitorableStatusInfoImplAxis(monBag);
        		System.out.println(monInfo);
        		DefOut.out.println(monInfo);
        	}*/
            return monBags;
        } catch (RemoteException e) {
            //e.printStackTrace(); 
            throw new XdaqException(e);
        }   
        //return new FecStatusResponse(false, -1);
    } 
    
    public void sendMaskAllChannels() throws XdaqException {
        Call call;
        try {
            call = prepareCall("maskAllChannels");
        } catch (ServiceException e1) {
            throw new XdaqException(e1);
        }  

        call.setReturnType(Constants.XSD_ANY); 

        try {
            call.invoke(new Object[] { });
        } catch (RemoteException e) {            
            //e.printStackTrace();
            throw new XdaqException(e);
        }           
    }
    
    public void sendApplyDBMasks() throws XdaqException {
        Call call;
        try {
            call = prepareCall("applyDbMasks");
        } catch (ServiceException e1) {
            throw new XdaqException(e1);
        }  

        call.setReturnType(Constants.XSD_ANY); 

        try {
            call.invoke(new Object[] { });
        } catch (RemoteException e) {            
            //e.printStackTrace();
            throw new XdaqException(e);
        }           
    }
    
    public void sendResetFSM() throws XdaqException {
        Call call;
        try {
            call = prepareCall("ResetFSM");
        } catch (ServiceException e1) {
            throw new XdaqException(e1);
        }  

        call.setReturnType(Constants.XSD_ANY); 

        try {
            call.invoke(new Object[] { });
        } catch (RemoteException e) {            
            //e.printStackTrace();
            throw new XdaqException(e);
        }           
    }

    public void sendLoadFromFlash() throws XdaqException {
        Call call;
        try {
            call = prepareCall("loadFromFlash");
        } catch (ServiceException e1) {
            throw new XdaqException(e1);
        }  

        call.setReturnType(Constants.XSD_ANY); 

        try {
            call.invoke(new Object[] { });
        } catch (RemoteException e) {            
            //e.printStackTrace();
            throw new XdaqException(e);
        }           
    }

    public void sendMaskNoisyChannels() throws XdaqException {
        Call call;
        try {
            call = prepareCall("maskNoisyChannels");
        } catch (ServiceException e1) {
            throw new XdaqException(e1);
        }  

        call.setReturnType(Constants.XSD_ANY); 

        try {
            call.invoke(new Object[] { });
        } catch (RemoteException e) {            
            //e.printStackTrace();
            throw new XdaqException(e);
        }           
    }

    public void sendSetupBoards() throws XdaqException {
        Call call;
        try {
            call = prepareCall("setupBoards");
            call.addParameter(new QName(RPCT_LBOX_ACCESS_NS, "forceCold"), 
                    Constants.XSD_BOOLEAN, ParameterMode.IN);
            call.addParameter(new QName(RPCT_LBOX_ACCESS_NS, "localConfigKey"), 
                    Constants.XSD_STRING, ParameterMode.IN);
        } catch (ServiceException e1) {
            throw new XdaqException(e1);
        }  

        call.setReturnType(Constants.XSD_ANY); 

        try {
            call.invoke(new Object[] { false, selctedConfigKey});
        } catch (RemoteException e) {            
            //e.printStackTrace();
            throw new XdaqException(e);
        }           
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    static public void sendConfigure(List<LBoxAccess> lBoxAccesses, PrintStream outStrm, PrintStream errStrm) {
        for(LBoxAccess lBoxAccess : lBoxAccesses) {
            try {
                lBoxAccess.sendConfigure();
                outStrm.println(lBoxAccess.getName() + " sendConfigure Done.");
            } catch (XdaqException e) {
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
                errStrm.println(e.getMessage());
            }
            catch (Exception e) {
                errStrm.println(e.getMessage());
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
            }
        }
    }

    static public void sendSynchronizeLinks(List<LBoxAccess> lBoxAccesses, PrintStream outStrm, PrintStream errStrm) {        
        for(LBoxAccess lBoxAccess : lBoxAccesses) {
            try {
                lBoxAccess.sendSynchronizeLinks();
                outStrm.println(lBoxAccess.getName() + " sendSynchronizeLinks Done.");
            } catch (XdaqException e) {
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
                errStrm.println(e.getMessage());
            }
            catch (Exception e) {
                errStrm.println(e.getMessage());
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
            }
        }
    }
     
    static public void sendResetFEC(List<LBoxAccess> lBoxAccesses, PrintStream outStrm, PrintStream errStrm) {
        for(LBoxAccess lBoxAccess : lBoxAccesses) {
            try {
                FecStatusResponse statusResponse = lBoxAccess.sendResetFEC();                
                outStrm.println(lBoxAccess.getName() + " fecIsGood " + statusResponse.isFecIsGood() + ", fecStatus 0x" + Integer.toHexString(statusResponse.getFecStatus()));
                outStrm.println("sendResetFEC Done.");
            } catch (XdaqException e) {
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
                errStrm.println(e.getMessage());
            }
            catch (Exception e) {
                errStrm.println(e.getMessage());
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
            }
        }
    }
    
    static public void sendStartMonitoring(List<LBoxAccess> lBoxAccesses, PrintStream outStrm, PrintStream errStrm) {
        for(LBoxAccess lBoxAccess : lBoxAccesses) {
            try {
                String result = lBoxAccess.sendStartMonitoring();
                outStrm.println(lBoxAccess.getName() + " " + result + " sendStartMonitoring Done.");
            } catch (XdaqException e) {
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
                errStrm.println(e.getMessage());
            }
            catch (Exception e) {
                errStrm.println(e.getMessage());
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
            }
        }
    }
    
    static public void sendStopMonitoring(List<LBoxAccess> lBoxAccesses, PrintStream outStrm, PrintStream errStrm) {
        for(LBoxAccess lBoxAccess : lBoxAccesses) {
            try {
                String result = lBoxAccess.sendStopMonitoring();
                outStrm.println(lBoxAccess.getName() + " " + result + " sendStopMonitoring Done.");
            } catch (XdaqException e) {
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
                errStrm.println(e.getMessage());
            }
            catch (Exception e) {
                errStrm.println(e.getMessage());
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
            }
        }
    }
    
    static public void sendStartHistoMonitoring(List<LBoxAccess> lBoxAccesses, PrintStream outStrm, PrintStream errStrm) {
        for(LBoxAccess lBoxAccess : lBoxAccesses) {
            try {
                String result = lBoxAccess.sendStartHistoMonitoring();
                outStrm.println(lBoxAccess.getName() + " " +result + " sendStartHistoMonitoring Done.");
            } catch (XdaqException e) {
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
                errStrm.println(e.getMessage());
            }
            catch (Exception e) {
                errStrm.println(e.getMessage());
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
            }
        }
    }
    
    static public void sendStopHistoMonitoring(List<LBoxAccess> lBoxAccesses, PrintStream outStrm, PrintStream errStrm) {
        for(LBoxAccess lBoxAccess : lBoxAccesses) {
            try {
                String result = lBoxAccess.sendStopHistoMonitoring();
                outStrm.println(lBoxAccess.getName() + " " +result + " sendStopHistoMonitoring Done.");
            } catch (XdaqException e) {
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
                errStrm.println(e.getMessage());
            }
            catch (Exception e) {
                errStrm.println(e.getMessage());
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
            }
        }
    }
    
    static public void sendGetMonitoring(List<LBoxAccess> lBoxAccesses, PrintStream outStrm, PrintStream errStrm) {
        for(LBoxAccess lBoxAccess : lBoxAccesses) {
            try {
                Bag<Object>[] monBags = lBoxAccess.sendGetMonitoring();
    			for(Bag monBag : monBags) {
    				MonitorableStatusInfoImplAxis monInfo = new MonitorableStatusInfoImplAxis(monBag);
    				System.out.println(monInfo);
    				DefOut.out.println(monInfo);
    			}   
                
                outStrm.println(lBoxAccess.getName() + " " +"" + " sendGetMonitoring Done.");
            } catch (XdaqException e) {
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
                errStrm.println(e.getMessage());
            }
            catch (Exception e) {
                errStrm.println(e.getMessage());
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
            }
        }
    }
    
    static public void sendMaskAllChannels(List<LBoxAccess> lBoxAccesses, PrintStream outStrm, PrintStream errStrm) {
        for(LBoxAccess lBoxAccess : lBoxAccesses) {
            try {
                lBoxAccess.sendMaskAllChannels();
                outStrm.println(lBoxAccess.getName() + " " +"sendMaskAllChannels Done.");
            } catch (XdaqException e) {
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
                errStrm.println(e.getMessage());
            }
            catch (Exception e) {
                errStrm.println(e.getMessage());
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
            }
        }
    }
    
    static public void sendApplyDBMasks(List<LBoxAccess> lBoxAccesses, PrintStream outStrm, PrintStream errStrm) {
        for(LBoxAccess lBoxAccess : lBoxAccesses) {
            try {
                lBoxAccess.sendApplyDBMasks();
                outStrm.println(lBoxAccess.getName() + " " +"sendApplyDBMasks Done.");
            } catch (XdaqException e) {
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
                errStrm.println(e.getMessage());
            }
            catch (Exception e) {
                errStrm.println(e.getMessage());
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
            }
        }
    }

    static public void sendMaskNoisyChannels(List<LBoxAccess> lBoxAccesses, PrintStream outStrm, PrintStream errStrm) {
        for(LBoxAccess lBoxAccess : lBoxAccesses) {
            try {
                lBoxAccess.sendMaskNoisyChannels();
                outStrm.println(lBoxAccess.getName() + " " +"MaskNoisyChannels Done.");
            } catch (XdaqException e) {
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
                errStrm.println(e.getMessage());
            }
            catch (Exception e) {
                errStrm.println(e.getMessage());
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
            }
        }
    }
    
    static public void sendResetFSM(List<LBoxAccess> lBoxAccesses, PrintStream outStrm, PrintStream errStrm) {
        for(LBoxAccess lBoxAccess : lBoxAccesses) {
            try {
                lBoxAccess.sendResetFSM();
                outStrm.println(lBoxAccess.getName() + " " +"sendResetFSM Done.");
            } catch (XdaqException e) {
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
                errStrm.println(e.getMessage());
            }
            catch (Exception e) {
                errStrm.println(e.getMessage());
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
            }
        }
    }
    
    static public void sendSetupBoards(List<LBoxAccess> lBoxAccesses, PrintStream outStrm, PrintStream errStrm) {
        for(LBoxAccess lBoxAccess : lBoxAccesses) {
            try {
                lBoxAccess.sendSetupBoards();
                outStrm.println(lBoxAccess.getName() + " " +"sendSetupBoards Done.");
            } catch (XdaqException e) {
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
                errStrm.println(e.getMessage());
            }
            catch (Exception e) {
                errStrm.println(e.getMessage());
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
            }
        }    	    
    }
    
    static public void sendLoadFromFlash(List<LBoxAccess> lBoxAccesses, PrintStream outStrm, PrintStream errStrm) {
        for(LBoxAccess lBoxAccess : lBoxAccesses) {
            try {
                lBoxAccess.sendLoadFromFlash();
                outStrm.println(lBoxAccess.getName() + " " +"sendLoadFromFlash Done.");
            } catch (XdaqException e) {
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
                errStrm.println(e.getMessage());
            }
            catch (Exception e) {
                errStrm.println(e.getMessage());
                errStrm.println("error during connection with the xdaq " + lBoxAccess.getName());
            }
        }
    }
    
	public void run() {
		try {
			Class parameterTypes[] = {};
			Object args[] = {};
			retunedObj = LBoxAccessImplAxis.class.getDeclaredMethod(commandToExecute, parameterTypes).invoke(this, args);
            outStrm.println(getName() + " " +commandToExecute +" Done.");
        }
        catch (Exception e) {
            errStrm.println(e.getMessage());
            errStrm.println("error during connection with the xdaq " + getName());
        }
	}
	
    static public void executeCommand(List<LBoxAccessImplAxis> lBoxAccesses, String commandName ) {    	    	
        List<Thread> lBoxAccessThreads = new ArrayList<Thread>();            
        for(LBoxAccess lBoxAccess : lBoxAccesses) {
        	Thread lBoxAccessTread = new Thread((LBoxAccessImplAxis)lBoxAccess);
        	lBoxAccessThreads.add(lBoxAccessTread);
        }
                   
    	for(LBoxAccessImplAxis lBoxAccess : lBoxAccesses) {
    		lBoxAccess.setCommandToExecute(commandName);
    	}

    	for(Thread lBoxAccessThread : lBoxAccessThreads) {
    		lBoxAccessThread.start();
    	}

    	for(Thread lBoxAccessThread : lBoxAccessThreads) {
/*    		while (lBoxAccessThread.isAlive()) {
    			try {
    				Thread.sleep(1000);
    			} catch (InterruptedException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    		}*/
    		
    		try {
				lBoxAccessThread.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	
    	if(commandName.equals("sendResetFEC")) {
    		for(LBoxAccessImplAxis lBoxAccess : lBoxAccesses) {
    			FecStatusResponse statusResponse =  (FecStatusResponse)lBoxAccess.getRetunedObj();
    			if(statusResponse != null) {
    				System.out.println(lBoxAccess.getName() + " fecIsGood " + statusResponse.isFecIsGood() + ", fecStatus 0x" + Integer.toHexString(statusResponse.getFecStatus()));
    			}
    		}
    	}
    	else if(commandName.equals("sendGetMonitoring")) {
    		for(LBoxAccessImplAxis lBoxAccess : lBoxAccesses) {
    			Bag<Object>[] monBags = (Bag<Object>[])lBoxAccess.getRetunedObj();
    			if(monBags != null) {
    				for(Bag monBag : monBags) {
    					MonitorableStatusInfoImplAxis monInfo = new MonitorableStatusInfoImplAxis(monBag);
    					System.out.println(monInfo);
    					DefOut.out.println(monInfo);
    				}    		
    			}
    		}
    	}
    }

    public static void main(String[] args) throws DataAccessException {
     
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);
        if(args.length == 0) {            
            String[] argsL =  {                
//                    "PASTEURA_LBB",
//                    "LBB_904",                      
                    "YEP1_near",
                    "YEP1_far",
                    "YEP3_near",
                    "YEP3_far",
                    "YEN1_near",
                    "YEN1_far",
                    "YEN3_near",
                    "YEN3_far",
                   
                    "RB-2_near",
                    "RB-2_far",
                    "RB-1_near",
                    "RB-1_far",
                    "RB0_near",
                    "RB0_far",
                    "RB+1_near",
                    "RB+1_far",
                    "RB+2_near",
                    "RB+2_far",
            };
            args = argsL;
        }

        System.out.println("Selected XDAQs: ");
        for(int i = 0; i < args.length; i++) {
            System.out.println(args[i] + " ");
        }                     
        
        try {
          	FileOutputStream file = new FileOutputStream(System.getenv("HOME") + "/bin/out/" +  "LBoxAccess.log", true);
            PrintStream prntSrm = new PrintStream(file);
            DefOut defLog= new DefOut(prntSrm);
            
            List<Crate> linkBoxes = equipmentDAO.getCratesByType(CrateType.LINKBOX, true);
            
            List<LBoxAccessImplAxis> lBoxAccesses = new ArrayList<LBoxAccessImplAxis>();
            for(int i = 0; i < args.length; i++) {
                LinkBox linkBox = null;
                for(Crate crate : linkBoxes) {                    
                    if( ((LinkBox)crate).getTowerName().equals(args[i]) ) {
                        linkBox = (LinkBox)crate;
                        break;
                    }
                }
                if(linkBox == null) {
                    throw new IllegalArgumentException(
                            "LinkBox not found for name " + args[i]);
                }
                
                XdaqApplication xdaqApplication = configurationDAO.getXdaqApplication(linkBox);

                if (xdaqApplication == null) {
                    throw new IllegalArgumentException(
                            "XdaqApplication not found for linkBox " + linkBox);
                }
                System.out.println("XDAQ from DB : " + linkBox.getTowerName());
                XdaqExecutive xdaqExecutive = xdaqApplication.getXdaqExecutive();
                URL url = new URL("http://" + xdaqExecutive.getHost() + ":" + xdaqExecutive.getPort());
                XdaqApplicationInfo info = new XdaqApplicationInfo(url, xdaqApplication.getClassName(), xdaqApplication.getInstance());

                LBoxAccessImplAxis lBoxAccess = new LBoxAccessImplAxis(info, null, linkBox.getTowerName());
                lBoxAccesses.add(lBoxAccess);
            }
 	
            String manual = new String();            

            manual = "List of XDAQs:";
            for(LBoxAccess lBoxAccess : lBoxAccesses) {
                manual += " " + lBoxAccess.getName();
            }          
            manual += "\nCommands:\n" +
                    "cf       - configure XDAQs\n" + 
                    "loadf    - load firmware from Flash\n" +
                    "setupb   - setup boards\n" + 
                    "maskall  - mask all channels\n" +
                    "mask     - mask noisy channels: dynamic+DB\n" +
                    "maskdb   - apply mask strips from DB\n" +
            		"sl       - synchronize optical links\n" +
            		"startm   - start monitoring\n" +
            		"stopm    - stop monitoring\n" +
            		"starth   - start histo monitoring\n" +
                    "stoph    - stop histo monitoring\n" +
                    "getm     - get monitoring\n" +
                    "resetfsm - reset XDAQs FSM\n" +
                    "rf       - reset FEC\n" +
            		"q        - quit this program\n";

            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            
            SimpleDateFormat formatter = new SimpleDateFormat("EEE dd MMM HH:mm:ss z yyyy");
            Calendar cal = Calendar.getInstance();
            String formattedDate = null;

            while (true) {
            	String command = null;
                String commandName = null;
                System.out.println("\n"+ manual);
                command = br.readLine() ;

                cal = Calendar.getInstance();
                formattedDate = formatter.format(cal.getTime());
                System.out.println(command + " started at: " + formattedDate);

                if(command.equals("cf")) {
                    //sendConfigure(lBoxAccesses, System.out, System.err);
                	commandName = "sendConfigure";
                    System.err.flush();
                    System.out.flush();
                }
                else if(command.equals("setupb")) {
                	List<LocalConfigKey> localConfigKeys = configurationManager.getLocalConfigKeys();
                	for(LocalConfigKey configKey : localConfigKeys) {
                		System.out.println(configKey.getName());
                	}
                	System.out.println("selcet local config key:");
                	String selctedConfigKey = null;
                	boolean ok = false;
                	while (!ok) {
                		selctedConfigKey = br.readLine();
                		for(LocalConfigKey configKey : localConfigKeys) {
                			if(configKey.getName().equals(selctedConfigKey)) {
                				ok = true;
                				break;
                			}
                		}
                		if(!ok) {
                			System.out.println("you gave not correct local config key, try again:");
                		}
                	}
                	for(LBoxAccess lBoxAccess : lBoxAccesses) {
                		lBoxAccess.setSelctedConfigKey(selctedConfigKey);
                    }
                	commandName = "sendSetupBoards";
                    System.err.flush();
                    System.out.flush();
                }
                else if(command.equals("resetfsm")) {
                    //sendResetFSM(lBoxAccesses, System.out, System.err);
                    System.err.flush();
                    System.out.flush();
                }
                else if(command.equals("loadf")) {
                    //sendLoadFromFlash(lBoxAccesses, System.out, System.err);
                    commandName = "sendLoadFromFlash";
                    System.err.flush();
                    System.out.flush();
                }
                else if(command.equals("mask")) {
                    //sendMaskNoisyChannels(lBoxAccesses, System.out, System.err);
                    commandName = "sendMaskNoisyChannels";
                    System.err.flush();
                    System.out.flush();
                }
                else if(command.equals("sl")) {
                    System.out.println("checking the BC0...");
                    TriggerBoard tb = (TriggerBoard)equipmentDAO.getBoardByName("TB0_0");
                    HardwareDbChip pac = dbMapper.getChip(tb.getPac(0));
                    long bcoCnt = pac.getHardwareChip().getItem("TEST_CNT.BCN0").read();
                    int i = 0;
                    while(bcoCnt == pac.getHardwareChip().getItem("TEST_CNT.BCN0").read()) {
                        System.out.println("waitnig for BC0");
                        Thread.sleep(100);
                        if(i > 10) {
                            throw new RuntimeException("the bc0 is not changing");
                        }
                        i++;
                    }
                    
                    //sendSynchronizeLinks(lBoxAccesses, System.out, System.err);
                    commandName = "sendSynchronizeLinks";
                    System.err.flush();
                    System.out.flush();
                }
                else if(command.equals("rf")) {
                    //sendResetFEC(lBoxAccesses, System.out, System.err);
                	commandName = "sendResetFEC";                  
                }
                else if(command.equals("maskall")) {
                    //sendMaskAllChannels(lBoxAccesses, System.out, System.err);
                	commandName = "sendMaskAllChannels";
                }
                else if(command.equals("maskdb")) {
                    //sendApplyDBMasks(lBoxAccesses, System.out, System.err);
                    commandName = "sendApplyDBMasks";
                }
                else if(command.equals("startm")) {
                    //sendStartMonitoring(lBoxAccesses, System.out, System.err);
                	commandName = "sendStartMonitoring";
                }
                else if(command.equals("stopm")) {
                    //sendStopMonitoring(lBoxAccesses, System.out, System.err);
                	commandName = "sendStopMonitoring";
                    System.err.flush();
                    System.out.flush();
                }
                else if(command.equals("starth")) {
                    //sendStartHistoMonitoring(lBoxAccesses, System.out, System.err);
                	commandName = "sendStartHistoMonitoring";
                }
                else if(command.equals("stoph")) {
                    //sendStopHistoMonitoring(lBoxAccesses, System.out, System.err);
                	commandName = "sendStopHistoMonitoring";
                }
                else if(command.equals("getm")) {
                	/*formattedDate = formatter.format(Calendar.getInstance().getTime());
                	DefOut.out.println(command + " started at: " + formattedDate);*/
                    //sendGetMonitoring(lBoxAccesses, System.out, System.err);
                	commandName = "sendGetMonitoring";
                    /*formattedDate = formatter.format(Calendar.getInstance().getTime());
                    DefOut.out.println(command + " stoped at: " + formattedDate);*/
                }
                else if(command.equals("q")) {
                    break;
                }
                else {
                    System.out.println("unknown command");
                }

                if(commandName != null) {
                	executeCommand(lBoxAccesses, commandName);
                }
                cal = Calendar.getInstance();
                formattedDate = formatter.format(cal.getTime());
                System.out.println(command + " ended at: " + formattedDate);

            }
            
            
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            context.closeSession();
        }
    }
}
