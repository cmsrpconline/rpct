/**
 * Created on Oct 28, 2009
 *
 * @author Karol Bunkowski
 * @version $Id$
 */
package rpct.xdaqaccess.axis;

import java.math.BigInteger;

import org.apache.axis.types.UnsignedLong;

import rpct.xdaq.axis.bag.Bag;
import rpct.xdaqaccess.MonitorableStatusInfo;

/**
 * @author Karol Bunkowski
 *
 */
public class MonitorableStatusInfoImplAxis implements MonitorableStatusInfo {
	private String boardName;
	private String chipName;
	private String name;
	private WarnigLevel level;
	private String message;
	private long value;
	
	public MonitorableStatusInfoImplAxis(String boardName, String chipName,
			String name, int level, String message, long value) {
		super();
		this.boardName = boardName;
		this.chipName = chipName;
		this.name = name;
		this.level =  WarnigLevel.fromInt(level);
		this.message = message;
		this.value = value;
	}

	MonitorableStatusInfoImplAxis(Bag<Object> bag) {
		this((String)bag.get("boardName"), (String) bag.get("chipName"), (String) bag.get("name"), 
				((BigInteger) bag.get("level")).intValue(), (String) bag.get("message"), ((UnsignedLong) bag.get("value")).intValue());
	}
	
	public String getBoardName() {
		return boardName;
	}
	public String getChipName() {
		return chipName;
	}
	public String getName() {
		return name;
	}
	public WarnigLevel getLevel() {
		return level;
	}
	public String getMessage() {
		return message;
	}
	public long getValue() {
		return value;
	}	
	
	public String toString() {
		String str =  level + " " + boardName + " " + chipName + " " + name + " " + message + " " + value;
		return str;
	}
}
