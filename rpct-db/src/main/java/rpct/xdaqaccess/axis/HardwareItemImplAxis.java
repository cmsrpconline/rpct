package rpct.xdaqaccess.axis;

import java.math.BigInteger;

import rpct.xdaq.axis.bag.Bag;
import rpct.xdaqaccess.HardwareItem;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.IIAccess;

public class HardwareItemImplAxis implements HardwareItem {

    private int id;

    private String name;

    private String type;

    private IIAccessImplAxis iiAccess;

    private HardwareRegistry hardwareRegistry;

    public HardwareItemImplAxis(int id, String name, String type,
            IIAccessImplAxis iiAccess, HardwareRegistry hardwareRegistry) {
        super();
        this.id = id;
        this.name = name;
        this.type = type;
        this.iiAccess = iiAccess;
        this.hardwareRegistry = hardwareRegistry;
    }

    public HardwareItemImplAxis(Bag<Object> bag, IIAccessImplAxis iiAccess,
            HardwareRegistry hardwareRegistry) {
        this(((BigInteger) bag.get("id")).intValue(), (String) bag.get("name"),
                (String) bag.get("type"), iiAccess, hardwareRegistry);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public IIAccess getIIAccess() {
        return iiAccess;
    }

    public HardwareRegistry getHardwareRegistry() {
        return hardwareRegistry;
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder("id = ");
        return b.append(id).append(" name = ").append(name).append(" type = ")
                .append(type).toString();
    }
}
