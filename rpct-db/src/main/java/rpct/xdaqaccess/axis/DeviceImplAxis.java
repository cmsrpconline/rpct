package rpct.xdaqaccess.axis;

import java.math.BigInteger;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.xml.rpc.ServiceException;

import rpct.xdaq.axis.bag.Bag;
import rpct.xdaqaccess.Board;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.DeviceDesc;
import rpct.xdaqaccess.DeviceItem;
import rpct.xdaqaccess.DeviceItemBits;
import rpct.xdaqaccess.DeviceItemInfo;
import rpct.xdaqaccess.DeviceItemType;
import rpct.xdaqaccess.DeviceItemVector;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.XdaqAccessException;
import rpct.xdaqaccess.diag.Diag;
import rpct.xdaqaccess.diag.DiagnosticReadout;
import rpct.xdaqaccess.diag.HistoMgr;
import rpct.xdaqaccess.diag.Pulser;

public class DeviceImplAxis extends HardwareItemImplAxis implements Device {

    private BoardImplAxis board;
    private int position;
    private DeviceDesc deviceDesc;
    /*private DeviceItem[] deviceItems;
    private Map<Integer, DeviceItem> deviceItemsById = new HashMap<Integer, DeviceItem>();
    private Map<String, DeviceItem> deviceItemsByName = new HashMap<String, DeviceItem>();*/
    private IIAccessImplAxis iiAccessImplAxis;
    
    private List<Diag> diags;
    private List<Pulser> pulsers;
    private List<HistoMgr> histoMgrs;
    private List<DiagnosticReadout> diagnosticRedouts;   
    
    public DeviceImplAxis(BoardImplAxis board, DeviceDesc deviceDesc, Bag<Object> bag, IIAccessImplAxis iiAccessImplAxis, HardwareRegistry hardwareRegistry) {
        super(bag, iiAccessImplAxis, hardwareRegistry);
        this.position = ((BigInteger) bag.get("position")).intValue();
        this.board = board;
        this.deviceDesc = deviceDesc;
        this.iiAccessImplAxis = iiAccessImplAxis;
        getHardwareRegistry().registerItem(this);
    }

    /*protected void mapDeviceItem(DeviceItem item) {
        deviceItemsById.put(item.getId(), item);
        String name;
        if (item.getType().equals(DeviceItemType.BITS)) {
            DeviceItemVector vector = ((DeviceItemBits)item).getVector();
            StringBuilder builder = new StringBuilder(vector.getName());
            builder.append('.').append(item.getName());
            name = builder.toString();
        }
        else {
            name = item.getName();
        }
        deviceItemsByName.put(name.toUpperCase(), item);
    }
    
    protected void createItems(DeviceItemInfo[] itemInfos) { 
        if (itemInfos == null) {
            throw new NullPointerException("itemInfos");
        }        
        if (!deviceItemsById.isEmpty()) {
            throw new IllegalStateException("items already created");
        }
        
        List<DeviceItem> deviceItemList = new ArrayList<DeviceItem>();
        
        for (DeviceItemInfo info : itemInfos) {
            DeviceItemImplAxis deviceItem = null;
            switch (info.getType()) {
            case WORD:
                deviceItem = new DeviceItemWordImplAxis(this, info, iiAccessImplAxis);
                break;
                
            case AREA:
                deviceItem = new DeviceItemAreaImplAxis(this, info, iiAccessImplAxis);
                break;
                
            case VECT:
                deviceItem = new DeviceItemVectorImplAxis(this, info, iiAccessImplAxis);
                break;
                
            case BITS:
                DeviceItemVectorImplAxis vector = (DeviceItemVectorImplAxis) deviceItemsById.get(info.getParentId());
                DeviceItemBitsImplAxis deviceItemBits = new DeviceItemBitsImplAxis(this, info, vector, iiAccessImplAxis);
                vector.addBits(deviceItemBits);
                mapDeviceItem(deviceItemBits);
                break;

            default:
                break;
            }
            
            if (deviceItem != null) {  
                mapDeviceItem(deviceItem);
                deviceItemList.add(deviceItem);
            }
        }
        deviceItems = deviceItemList.toArray(new DeviceItem[0]);
    }*/

    public void loadItems() {
        if (deviceDesc.getItems() == null) {
            try {
                getIIAccess().loadDeviceItems(this);
            } catch (RemoteException e) {
                throw new XdaqAccessException(e);
            } catch (ServiceException e) {
                throw new XdaqAccessException(e);
            }
        }
    }
    
    public DeviceDesc getDeviceDesc() {
        return deviceDesc;
    }
    
    /*public DeviceItem[] getItems() {
        loadDeviceItems(); 
        return deviceItems; 
    } */   

    /*public DeviceItem findById(int id) {
        loadDeviceItems(); 
        return deviceItemsById.get(id);
    }*/

    public DeviceItem getItem(String name) {
        loadItems(); 
        rpct.xdaqaccess.DeviceDesc.ItemDesc item = deviceDesc.getItem(name);  
        if (item == null) {
            return null;
            //throw new NoSuchElementException(name);
        }

        switch (item.getType()) {
        case WORD:
            return new DeviceItemWordImplAxis(this, item, iiAccessImplAxis);

        case AREA:
            return new DeviceItemAreaImplAxis(this, item, iiAccessImplAxis);

        case VECT:
            return new DeviceItemVectorImplAxis(this, item, iiAccessImplAxis);

        case BITS:
            /*DeviceItemVectorImplAxis vector = (DeviceItemVectorImplAxis) deviceItemsById.get(info.getParentId());
            DeviceItemBitsImplAxis deviceItemBits = new DeviceItemBitsImplAxis(this, info, vector, iiAccessImplAxis);
            vector.addBits(deviceItemBits);
            mapDeviceItem(deviceItemBits);*/
            return new DeviceItemBitsImplAxis(this, item, iiAccessImplAxis);

        default:
            throw new IllegalArgumentException("Invalid item type '" + item.getType() + "'");
        }        
    }

    public DeviceItem getItem(DeviceDesc.ItemDesc itemDesc) {
        return getItem(itemDesc.getName());
    }

    public Board getBoard() {
        return board;
    }
    
    public int getPosition() {
        return position;
    }

    private void loadDiags() throws RemoteException, ServiceException {
        if (diags == null) {
            diags = getHardwareRegistry().findDiagsByOwnerId(getId());
            if (diags == null) {
                diags = Collections.emptyList();
            }
            else {
                pulsers = new ArrayList<Pulser>();
                histoMgrs = new ArrayList<HistoMgr>();
                diagnosticRedouts = new ArrayList<DiagnosticReadout>();
                for (Diag diag : diags) {
                    if (diag instanceof Pulser) {
                        pulsers.add((Pulser) diag);
                    }
                    else if (diag instanceof HistoMgr) {
                        histoMgrs.add((HistoMgr) diag);
                    }
                    else if (diag instanceof DiagnosticReadout) {
                        diagnosticRedouts.add((DiagnosticReadout) diag);
                    }
                }
            }
        }
    }

    public List<Diag> getDiags() throws RemoteException, ServiceException {
        loadDiags();
        return diags;
    }

    public List<Pulser> getPulsers() throws RemoteException, ServiceException {
        loadDiags();
        return pulsers;
    }

    public List<HistoMgr> getHistoMgrs() throws RemoteException, ServiceException {
        loadDiags();
        return histoMgrs;
    }
    
    public List<DiagnosticReadout> getDiagnosticReadouts() throws RemoteException, ServiceException {
        loadDiags();
        return diagnosticRedouts;
    }
}
