package rpct.xdaqaccess;

public interface HardwareItem {
    int getId();  // unique
    String getName(); // unique
    String getType();
    IIAccess getIIAccess();
}
