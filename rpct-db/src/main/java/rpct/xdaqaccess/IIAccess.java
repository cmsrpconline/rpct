package rpct.xdaqaccess;


import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import rpct.xdaq.XdaqApplicationInfo;
import rpct.xdaq.axis.Binary;

public interface IIAccess {
    XdaqApplicationInfo getXdaqApplicationInfo();
    Crate[] getCrates(boolean nested) throws RemoteException, ServiceException;
    Board[] loadBoards(Crate crate) throws RemoteException, ServiceException;
    Device[] loadDevices(Board board) throws RemoteException, ServiceException;
    void loadDeviceItems(Device device) throws RemoteException, ServiceException;
    Binary[] readII(DeviceItem deviceItem, int startPos, int count) throws RemoteException, ServiceException;
    void writeII(DeviceItem deviceItem, int startPos, Binary[] data) throws RemoteException, ServiceException;
}
