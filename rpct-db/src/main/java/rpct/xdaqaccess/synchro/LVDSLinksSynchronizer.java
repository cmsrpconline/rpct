package rpct.xdaqaccess.synchro;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import javax.xml.rpc.ServiceException;

import rpct.datastream.TestPresets;
import rpct.datastream.TestsManager;
import rpct.db.DataAccessException;
import rpct.db.domain.equipment.ChipType;
import rpct.synch.LVDSLinkDef;
import rpct.synch.PulserGeneratorForSynchronization;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.DeviceItem;
import rpct.xdaqaccess.diag.Pulser;

public class LVDSLinksSynchronizer {
//  private RpctChip[] transmitters;
//  private RpctChip[] recivers;
//  private LVDSLink[] lvdsLinks;

    private Vector<LVDSTransmDevice> transmitters;
    private Vector<LVDSTransmDevice> recivers;
    private Vector<LVDSLink> lvdsLinks;
    private int maxLinesCntPerLink;

    static private int maxMuxDelay = 8;

    private String[] testDataTab;

    public LVDSLinksSynchronizer(int _linesCntPerLink, int maxMuxDelay) {       
        maxLinesCntPerLink = _linesCntPerLink;
        this.maxMuxDelay  = maxMuxDelay;
        String[] testDatabuf = {     
                //new String("1010101010101010101010100101"),
               // new String("5555555555555555555555555555"),
               // new String("aaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
//                new String("1111111111111111111111111111"),
//                new String("2222222222222222222222222222"),
//                new String("4444444444444444444444444444"),
//                new String("8888888888888888888888888888"),
                new String("0101010101010101010101010101"),
                new String("1010101010101010101010101010"),
//              new String("8181818181818181818181818181"),  
//              new String("adadadadadadadadadadadadadad")                
//              new String("1e57234cd45a315edace57a35c36"),
//              new String("8ae912838ad3fe4a624674fa7469"),
//              new String("ce57a34cd459318edace57a34cd2"), 
//              0x101010,
//              0x010101,
//              0x123456,
//              0x654321,
//              0x999999,
//              0x666666,
//              0x777777,
//              0xEEEEEE
        };
        testDataTab = testDatabuf;

        transmitters = new Vector<LVDSTransmDevice>();
        recivers = new Vector<LVDSTransmDevice>();
        lvdsLinks = new Vector<LVDSLink>(); 
    }

    /*    void addTransmitter(RpctChip transmitter) {
        transmitters.add(transmitter);
    }

    void addReciver(RpctChip reciver) {
        recivers.add(reciver);
    }*/

    public void addLVDSLink(LVDSLink link) {
        lvdsLinks.add(link);
        if(transmitters.contains(link.getTransmitter()) == false)            
            transmitters.add(link.getTransmitter());

        if(recivers.contains(link.getReciver()) == false)            
            recivers.add(link.getReciver()); 
    }

    public void addLVDSLinks(List<LVDSLink> lvdsLinks) {
        for(LVDSLink link: lvdsLinks) {
            addLVDSLink(link); 
        }        
    }
    
    public void scanSynchroParamsWithStaticAndPulsers(String[] testDataTab, TestsManager testsManager, boolean sharpen, int finalParamsTuneRep) throws RemoteException, ServiceException, InterruptedException, DataAccessException, XdaqException {
        Date date = new Date();
        DefOut.out.println(date.toString() + ":");
        DefOut.out.println("scaning synchro params with static------------------------------------------");
        System.out.println("scaning synchro params with static");     
        
        for (LVDSTransmDevice transmitter : transmitters) {
            transmitter.resetLVDSLinkTrans();   
        }

        for (LVDSTransmDevice reciver : recivers) {
            reciver.resetLVDSLinkRec();
            reciver.setRecTestEna();            
        }

        for (LVDSTransmDevice reciver : recivers) {
            reciver.setMuxRegAdd(0);           
        }

        enableTransmissionCheck(false, false); //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       
        for (int delay = 0; delay < maxMuxDelay; delay++) {
            for (LVDSTransmDevice reciver : recivers) {
                //reciver.setMuxDelays(delay);      
                reciver.setRecMuxDelayAndDataDelay(delay);
            }
            for(int clkPhase = 0; clkPhase < LVDSLink.clkPhaseSteps; clkPhase++) {
                for (int regAdd=0; regAdd<2; regAdd++) //TODO
                {
                    for (LVDSTransmDevice reciver : recivers) {
                        if(LVDSLink.clkPhaseSteps == 2)
                            reciver.setMuxClkInv(clkPhase);                         
                        else if(LVDSLink.clkPhaseSteps == 4)
                            reciver.setMuxClkPhase(clkPhase);
                        
                        reciver.setMuxRegAdd(regAdd); //TODO
                    }

                    for (String testData : testDataTab) {      
                        DefOut.out.println("testData "+ testData);
                        for (LVDSTransmDevice transmitter : transmitters) {
                            transmitter.setSendTestStaticData(testData);            
                        }              

                        for (LVDSLink lvdsLink : lvdsLinks) {
                            //lvdsLink.fillShiftTab(clkPhase, delay);        
                            lvdsLink.fillDelayTab_StaticTestData(clkPhase, regAdd, delay, testData);
                        }
                    }
                }
            }
        }

        for (LVDSLink lvdsLink : lvdsLinks) {
            lvdsLink.convert_delayIsOkTab_to_shiftTab();
        }
        
        for (LVDSLink lvdsLink : lvdsLinks) {
            for(int sLine = 0; sLine < lvdsLink.getLinkDef().getLinesInSplot(); sLine++) {
                lvdsLink.addToActivatedLines(sLine);
            }
        }
        
        DefOut.out.println("\n found parameters ---------------------------------------------------------------------------------------- \n");
        for (LVDSLink lvdsLink : lvdsLinks) {                                 
            boolean wasSharpen = false;
            if(sharpen) {
                wasSharpen = lvdsLink.sharpenShifTab();
            }
            if(lvdsLink.findShift()) {
                lvdsLink.setSynchroParams();
                continue;
            }  
            else if(wasSharpen) {
                lvdsLink.unSharpenShifTab(); 
                if(lvdsLink.findShift()) {
                    lvdsLink.setSynchroParams();
                    continue;
                } 
            }

            DefOut.out.println(lvdsLink.getName() + " synch params not found!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            DefOut.out.println("");
            System.out.println(lvdsLink.getName() + " synch params not found!!!");
            
        }
        
        
        //-----------------------------------------
        for (LVDSTransmDevice transmitter : transmitters) {
            transmitter.setNoSendTestEnaa();   
        }

        for (LVDSTransmDevice reciver : recivers) {
            reciver.setNoRecTestEna();                     
        }
        
        //-----------------------------------------------------------
        enableTransmissionCheck(true, true);
        
        System.out.println("static scan finshed, resetRecTestOrData() and resetRecErrorCounter()");
        for (LVDSLink lvdsLink : lvdsLinks) {                       
            lvdsLink.resetRecTestOrData();
        }
        for (LVDSTransmDevice reciver : recivers) {
            reciver.resetRecErrorCounter();            
        }

        int wait = 1000;
        DefOut.out.println("waiting " + wait/1000 + "s");  
        Thread.sleep(wait);

        date = new Date();
        DefOut.out.println(date.toString() + ":");
        /*for (LVDSLink lvdsLink : lvdsLinks) {                       
            lvdsLink.readRecTestOrData(false);
        } */ 
        for (LVDSTransmDevice reciver : recivers) {
            reciver.readRecErrorCounter();            
        }        
        
        DefOut.out.println("\n begin finalParametrsTune() ---------------------------------------------------------------------------------------- \n");
                
        for(int sLine = 0; sLine < LVDSLinkDef.getMaxLinesInSplot(); sLine++) {
            for (LVDSLink lvdsLink : lvdsLinks) {  
                lvdsLink.addToActivatedLines(sLine);
            }
        }
        
        System.out.println("finalParametrsTune() for sLine with check bits");    
        DefOut.out.println("finalParametrsTune() for sLine with check bits");
        for(int iRep = 0; iRep < finalParamsTuneRep; iRep++) {
            for(int sLine = 0; sLine < 1; sLine++) {               
                System.out.println("finalParametrsTune() " + iRep + ", sline " + sLine);    
                DefOut.out.println("finalParametrsTune() " + iRep + ", sline " + sLine + " ----------------------------------"); 
                testsManager.preparePulsersForSynchro(sLine);
                testsManager.startPulsers();
                
                for (LVDSLink lvdsLink : lvdsLinks) {  
                    lvdsLink.resetRecTestOrData();
                }
                for (LVDSTransmDevice reciver : recivers) {
                    reciver.resetRecErrorCounter();            
                }
                
                wait = 60000;
                DefOut.out.println("waiting " + wait/1000 + "s"); 
                for(int i = 0; i < 1000; i++) {                     
                    for (LVDSTransmDevice reciver : recivers) {
                        reciver.getDevice().getItem("REC_ERROR_COUNT").read(); //to generate VME access, which may disturb the transmission
                        reciver.getDevice().getItem("REC_TEST_OR_DATA").read();
                        if(reciver.getDevice().getItem("MEM_PULSE") != null)
                        	reciver.getDevice().getItem("MEM_PULSE").read();
                    } 
                    Thread.sleep(wait/1000);
                }
                                
                for (LVDSLink lvdsLink : lvdsLinks) {                       
                    while(lvdsLink.finalParametrsTune(sLine, sharpen))
                        Thread.sleep(2000); 
                }
                                
                for (LVDSTransmDevice reciver : recivers) {
                    reciver.resetRecErrorCounter();            
                }
                Thread.sleep(1000);
                for (LVDSTransmDevice reciver : recivers) {
                    reciver.readRecErrorCounter();            
                } 
                
                testsManager.stopPulsers();
            }
        }
        
        System.out.println("finalParametrsTune() for all sLines");    
        DefOut.out.println("finalParametrsTune() for all sLines");
        for(int iRep = 0; iRep < finalParamsTuneRep; iRep++) {
            for(int sLine = 0; sLine < LVDSLinkDef.getMaxLinesInSplot(); sLine++) {               
                System.out.println("finalParametrsTune() " + iRep + ", sline " + sLine);    
                DefOut.out.println("finalParametrsTune() " + iRep + ", sline " + sLine + " ----------------------------------"); 
                testsManager.preparePulsersForSynchro(sLine);
                testsManager.startPulsers();
                
                for (LVDSLink lvdsLink : lvdsLinks) {  
                    lvdsLink.resetRecTestOrData();
                }
                for (LVDSTransmDevice reciver : recivers) {
                    reciver.resetRecErrorCounter();            
                }
                
                wait = 60000;
                DefOut.out.println("waiting " + wait/1000 + "s"); 
                for(int i = 0; i < 10; i++) {                     
                    for (LVDSTransmDevice reciver : recivers) {
                        reciver.getDevice().getItem("REC_ERROR_COUNT").read(); //to generate VME access, which may disturb the transmission          
                    } 
                    Thread.sleep(wait/10);
                }
                                
                for (LVDSLink lvdsLink : lvdsLinks) {                       
                    while(lvdsLink.finalParametrsTune(sLine, sharpen))
                        Thread.sleep(2000); 
                }
                                
                for (LVDSTransmDevice reciver : recivers) {
                    reciver.resetRecErrorCounter();            
                }
                Thread.sleep(1000);
                for (LVDSTransmDevice reciver : recivers) {
                    reciver.readRecErrorCounter();            
                } 
                
                testsManager.stopPulsers();
            }
        }
        printParams();
    }
    
    /**
     * @param sharpen
     * @param finalParamsTuneNoPulserRep
     * @param wait milis
     * @throws RemoteException
     * @throws ServiceException
     * @throws InterruptedException
     * it is done only for transmissions from the OPTOS 
     */
    public void finalParametrsTuneOnNormalData(boolean sharpen, int finalParamsTuneNoPulserRep, long wait) throws RemoteException, ServiceException, InterruptedException {
        for(int iRep = 0; iRep < finalParamsTuneNoPulserRep; iRep++) {
            for(int sLine = 0; sLine < 1; sLine++) {        //only for sLine 0 it has sense       
                System.out.println("finalParametrsTune() - no pulser" + iRep + ", sline " + sLine);    
                DefOut.out.println("finalParametrsTune() - no pulser " + iRep + ", sline " + sLine + " ----------------------------------"); 
                
                for (LVDSLink lvdsLink : lvdsLinks) {  
                    if(lvdsLink.getTransmitter().getDevice().getType().equals("TB3OPTO") ) { //<<<<<<<<<<<<
                        lvdsLink.resetRecTestOrData();
                    }
                }
                for (LVDSTransmDevice reciver : recivers) {
                    reciver.resetRecErrorCounter();            
                }
                
                DefOut.out.println("waiting " + wait/1000 + "s"); 
                for(int i = 0; i < 100; i++) {                     
                    for (LVDSTransmDevice reciver : recivers) {
                    	if(reciver.getDevice().getBoard().getName().equals("TBn3_0") && reciver.getDevice().getType().equals("GBSORT")) {   
                    		System.out.println("skipping generation of the VME accesses for "+ reciver.getDevice().getName());
                    	}
                    	else 
                    		reciver.getDevice().getItem("REC_ERROR_COUNT").read(); //to generate VME access, which may disturb the transmission
                    } 
                    Thread.sleep(wait/100);
                }
                                
                for (LVDSLink lvdsLink : lvdsLinks) {   
                    if(lvdsLink.getTransmitter().getDevice().getType().equals("TB3OPTO") ) { //<<<<<<<<<<<<
                        while(lvdsLink.finalParametrsTune(sLine, sharpen))
                            Thread.sleep(10000); 
                    }
                }
                                
                for (LVDSTransmDevice reciver : recivers) {
                    reciver.resetRecErrorCounter();            
                }
                Thread.sleep(1000);
                for (LVDSTransmDevice reciver : recivers) {
                    reciver.readRecErrorCounter();            
                } 
            }
        }
        
        printParams();
    }

    public void scanSynchroParams() throws RemoteException, ServiceException, InterruptedException {
        Date date = new Date();
        DefOut.out.println(date.toString() + ":");
        DefOut.out.println("scaning synchro params with random------------------------------------------");
        System.out.println("scaning synchro params with random");     
        
        for (LVDSTransmDevice transmitter : transmitters) {
            transmitter.resetLVDSLinkTrans();   
        }

        for (LVDSTransmDevice reciver : recivers) {
            reciver.resetLVDSLinkRec();
            reciver.setRecTRandomEna();            
        }

        for (LVDSTransmDevice transmitter : transmitters) {
            transmitter.setSendTestRandomData();       
            transmitter.setRandomTestData();
        } 

        for (LVDSTransmDevice reciver : recivers) {
            reciver.setMuxRegAdd(0);           
        }

        enableTransmissionCheck(false, false); //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        for (int delay = 0; delay < maxMuxDelay; delay++) {
            for (LVDSTransmDevice reciver : recivers) {
                //reciver.setMuxDelays(delay);      
                reciver.setRecMuxDelayAndDataDelay(delay);
            }
            for(int clkPhase = 0; clkPhase < LVDSLink.clkPhaseSteps; clkPhase++) {
                for (int regAdd=0; regAdd<2; regAdd++) //TODO
                {
                    for (LVDSTransmDevice reciver : recivers) {
                        if(LVDSLink.clkPhaseSteps == 2)
                            reciver.setMuxClkInv(clkPhase);                         
                        else if(LVDSLink.clkPhaseSteps == 4)
                            reciver.setMuxClkPhase(clkPhase);
                        
                        reciver.setMuxRegAdd(regAdd); //TODO
                    }

                    for (LVDSTransmDevice reciver : recivers) {
                        reciver.resetRecTestOrData();         		    
                    }

                    Thread.sleep(2000);

                    for (LVDSLink lvdsLink : lvdsLinks) {
                        //lvdsLink.fillShiftTab(clkPhase, delay);		 
                        lvdsLink.fillShiftTab(clkPhase, regAdd, delay); //TODO
                    }
                }
            }
        }

        for (LVDSLink lvdsLink : lvdsLinks) {
            if(lvdsLink.findShift()) {
                lvdsLink.setSynchroParams();
            }    
        } 

        for (LVDSLink lvdsLink : lvdsLinks) {                       
            lvdsLink.resetRecTestOrData();
        }
        for (LVDSTransmDevice reciver : recivers) {
            reciver.resetRecErrorCounter();            
        }

        int wait = 1000;
        DefOut.out.println("waiting " + wait/1000 + "s");  
        Thread.sleep(wait);

        date = new Date();
        DefOut.out.println(date.toString() + ":");
        for (LVDSLink lvdsLink : lvdsLinks) {                       
            lvdsLink.readRecTestOrData(false);
        }  
        for (LVDSTransmDevice reciver : recivers) {
            reciver.readRecErrorCounter();            
        }        
    }
    
    public void scanSynchroParamsPulser3(TestsManager testsManager, int clkPhaseSteps, int finalParamsTuneRep) throws RemoteException, ServiceException, InterruptedException, DataAccessException, XdaqException {
        Date date = new Date();
        DefOut.out.println(date.toString() + ":");
        DefOut.out.println("scaning synchro params with pulser------------------------------------------");
        System.out.println("scaning synchro params with pulser");

        if(false)  {//TODO sprawdzic jakie to ma zanczenie na synchronizacje linkow
            DefOut.out.println("turning on the OPTO input");
            System.out.println("turning on the OPTO input");
            for (LVDSTransmDevice transmitter : transmitters) {
                if(transmitter.getDevice().getType().equals(ChipType.OPTO.getHwType())) {
                    transmitter.getDevice().getItem("TLK.ENABLE").write(7);
                    transmitter.getDevice().getItem("TLK.LCK_REFN").write(7);
                    transmitter.getDevice().getItem("REC_CHECK_DATA_ENA").write(7);
                    transmitter.getDevice().getItem("REC_CHECK_ENA").write(7);
                    transmitter.getDevice().getItem("REC_TEST_ENA").write(0);
                }
            } 
        }        
            
        for (LVDSTransmDevice transmitter : transmitters) {
            transmitter.resetLVDSLinkTrans();   
        }

        for (LVDSTransmDevice reciver : recivers) {
            reciver.resetLVDSLinkRec();          
        }

        for (LVDSTransmDevice reciver : recivers) {
            reciver.setMuxRegAdd(0);  
            reciver.setMuxClkPhase(0);        
        }

        enableTransmissionCheck(true, true); //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


        for (int delay = 0; delay < maxMuxDelay; delay++) {
            for (LVDSTransmDevice reciver : recivers) { 
                reciver.setRecMuxDelayAndDataDelay(delay);               
            }
            for(int sLine = 0; sLine < LVDSLinkDef.getMaxLinesInSplot(); sLine++) {
                testsManager.preparePulsersForSynchro(sLine);
                testsManager.startPulsers();
               
                for (LVDSLink lvdsLink : lvdsLinks) {
                    lvdsLink.getActivatedLines().clear();                
                }

                DefOut.out.println("delay " + delay + " sLine " + sLine);

                for(int clkPhase = 0; clkPhase < clkPhaseSteps; clkPhase++) {
                    for (int regAdd = 0; regAdd < 2; regAdd++) //TODO
                    {
                        for (LVDSTransmDevice reciver : recivers) {
                            reciver.setMuxClkPhase(clkPhase, sLine);
                            reciver.setMuxRegAdd(regAdd, sLine); //TODO
                        }

                        for (LVDSTransmDevice reciver : recivers) {
                            reciver.resetRecTestOrData();                       
                        }

                        Thread.sleep(1000);

                        for (LVDSLink lvdsLink : lvdsLinks) {
                            //lvdsLink.fillShiftTabForLinesGroup(clkPhase, delay, sLine);
                            lvdsLink.addToActivatedLines(sLine);
                            lvdsLink.fillShiftTabForLinesGroup(clkPhase, regAdd, delay, sLine); //TODO    
                        }                                                 
                    }
                }
                if(sLine == 0) {
                    for (LVDSLink lvdsLink : lvdsLinks) {
                        if(lvdsLink.findSynchParams(delay, false)) {
                            //lvdsLink.setSynchroParams();
                            lvdsLink.setFoundClkPhaseAndRegAdd();
                        }    
                    }
                }
                testsManager.stopPulsers(); 
            }                       
        }                     

        for (LVDSLink lvdsLink : lvdsLinks) {
            for(int sLine = 0; sLine < lvdsLink.getLinkDef().getLinesInSplot(); sLine++) {
                lvdsLink.addToActivatedLines(sLine);
            }
        }
        
        DefOut.out.println("\n found parameters ---------------------------------------------------------------------------------------- \n");
        for (LVDSLink lvdsLink : lvdsLinks) {                       
            boolean wasSharpen = lvdsLink.sharpenShifTab();            
            if(lvdsLink.findShift()) {
                lvdsLink.setSynchroParams();
                continue;
            }  
            else if(wasSharpen) {
                lvdsLink.unSharpenShifTab(); 
                if(lvdsLink.findShift()) {
                    lvdsLink.setSynchroParams();
                    continue;
                } 
            }

            DefOut.out.println(lvdsLink.getName() + " synch params not found!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            DefOut.out.println("");
            System.out.println(lvdsLink.getName() + " synch params not found!!!");
            
        }

        DefOut.out.println("pulsers stoped, resetRecTestOrData() and resetRecErrorCounter()");
        for (LVDSLink lvdsLink : lvdsLinks) {                       
            lvdsLink.resetRecTestOrData();
        }
        for (LVDSTransmDevice reciver : recivers) {
            reciver.resetRecErrorCounter();            
        }

        int wait = 1000;
        DefOut.out.println("waiting " + wait/1000 + "s");  
        Thread.sleep(wait);

        date = new Date();
        DefOut.out.println(date.toString() + ":");
        /*for (LVDSLink lvdsLink : lvdsLinks) {                       
            lvdsLink.readRecTestOrData(false);
        } */ 
        for (LVDSTransmDevice reciver : recivers) {
            reciver.readRecErrorCounter();            
        }        
        
        DefOut.out.println("\n begin finalParametrsTune() ---------------------------------------------------------------------------------------- \n");
                
        for(int sLine = 0; sLine < LVDSLinkDef.getMaxLinesInSplot(); sLine++) {
            for (LVDSLink lvdsLink : lvdsLinks) {  
                lvdsLink.addToActivatedLines(sLine);
            }
        }
        
        for(int iRep = 0; iRep < finalParamsTuneRep; iRep++) {
            for(int sLine = 0; sLine < LVDSLinkDef.getMaxLinesInSplot(); sLine++) {               
                System.out.println("finalParametrsTune() " + iRep + ", sline " + sLine);    
                DefOut.out.println("finalParametrsTune() " + iRep + ", sline " + sLine + " ----------------------------------"); 
                testsManager.preparePulsersForSynchro(sLine);
                testsManager.startPulsers();
                
                for (LVDSLink lvdsLink : lvdsLinks) {  
                    lvdsLink.resetRecTestOrData();
                }
                for (LVDSTransmDevice reciver : recivers) {
                    reciver.resetRecErrorCounter();            
                }
                
                wait = 60000;
                DefOut.out.println("waiting " + wait/1000 + "s");  
                Thread.sleep(wait);
                                
                for (LVDSLink lvdsLink : lvdsLinks) {                       
                    while(lvdsLink.finalParametrsTune(sLine, true))
                        Thread.sleep(2000); 
                }
                                
                for (LVDSTransmDevice reciver : recivers) {
                    reciver.resetRecErrorCounter();            
                }
                Thread.sleep(1000);
                for (LVDSTransmDevice reciver : recivers) {
                    reciver.readRecErrorCounter();            
                } 
                
                testsManager.stopPulsers();
            }
        }
        
        for(int iRep = 0; iRep < finalParamsTuneRep; iRep++) {
            for(int sLine = 0; sLine < LVDSLinkDef.getMaxLinesInSplot(); sLine++) {               
                System.out.println("finalParametrsTune() - no pulser" + iRep + ", sline " + sLine);    
                DefOut.out.println("finalParametrsTune() - no pulser " + iRep + ", sline " + sLine + " ----------------------------------"); 
                
                for (LVDSLink lvdsLink : lvdsLinks) {  
                    lvdsLink.resetRecTestOrData();
                }
                for (LVDSTransmDevice reciver : recivers) {
                    reciver.resetRecErrorCounter();            
                }
                
                wait = 60000;
                DefOut.out.println("waiting " + wait/1000 + "s");  
                Thread.sleep(wait);
                                
                for (LVDSLink lvdsLink : lvdsLinks) {                       
                    while(lvdsLink.finalParametrsTune(sLine, true))
                        Thread.sleep(10000); 
                }
                                
                for (LVDSTransmDevice reciver : recivers) {
                    reciver.resetRecErrorCounter();            
                }
                Thread.sleep(1000);
                for (LVDSTransmDevice reciver : recivers) {
                    reciver.readRecErrorCounter();            
                } 
            }
        }
        
        printParams();
    }
    
    public void  findSynchroParams() throws RemoteException, ServiceException, InterruptedException {        
        //for(int iTD = 0; iTD < testData.length; iTD++) {
        Date date = new Date();
        DefOut.out.println(date.toString() + ":");
        DefOut.out.println("testing synchro params with static------------------------------------------");
        System.out.println("testing synchro params with static");

        for (LVDSTransmDevice transmitter : transmitters) {
            transmitter.resetLVDSLinkTrans();   
        }

        for (LVDSTransmDevice reciver : recivers) {
            reciver.resetLVDSLinkRec();
            reciver.setRecTestEna();            
        }

        for (String testData : testDataTab) {      
            DefOut.out.println("testData "+ testData);
            for (LVDSTransmDevice transmitter : transmitters) {
                transmitter.setSendTestStaticData(testData);            
            }   
            for (LVDSLink lvdsLink : lvdsLinks) {
                lvdsLink.scanSynchroParams(testData);
            }            
        }

        for (LVDSLink lvdsLink : lvdsLinks) {
            if(lvdsLink.findShift()) {
                lvdsLink.setSynchroParams();
                lvdsLink.checkTestData(testDataTab[testDataTab.length-1]);
            }    
        } 

        date = new Date();
        DefOut.out.println(date.toString() + ":");

        enableTransmissionCheck(false, true); //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        DefOut.out.println("testing synchro params with random------------------------------------------");
        System.out.println("testing synchro params with random");
        for (LVDSTransmDevice transmitter : transmitters) {
            transmitter.setSendTestRandomData();       
            transmitter.setRandomTestData();
        }        
        for (LVDSTransmDevice reciver : recivers) {
            reciver.setRecTRandomEna();            
        }        

        /*        for (int iClkInv = 0; iClkInv < 2; iClkInv++) {
            for (int iRegAdd=0; iRegAdd<2; iRegAdd++) {  
                for (int iLineNum = 0; iLineNum < maxLinesCntPerLink; iLineNum++) {
                    for (LVDSLink lvdsLink : lvdsLinks) {
                        lvdsLink.setSynchroParams(iLineNum, iClkInv, iRegAdd);                          
                        lvdsLink.resetResTestOrData();
                    }
                    Thread.sleep(1000);

                    for (LVDSLink lvdsLink : lvdsLinks) {
                        lvdsLink.updateShiftTab(iLineNum, iClkInv, iRegAdd);  
                    }
                }
            }
        }*/

        /*	for (int iClkInv = 0; iClkInv < 2; iClkInv++) {
	    for (int iRegAdd=0; iRegAdd<2; iRegAdd++) {
		for (LVDSLink lvdsLink : lvdsLinks) {
		    lvdsLink.setRecMuxClkInv(iClkInv);
		    lvdsLink.setRecMuxRegAdd(iRegAdd);                        
		}            	
		for (int iLineNum = 0; iLineNum < maxLinesCntPerLink; iLineNum++) {
		    for (LVDSLink lvdsLink : lvdsLinks) {
			lvdsLink.setRecMuxDalay(iLineNum, iClkInv, iRegAdd);                            
			lvdsLink.resetRecTestOrData();
		    }
		    Thread.sleep(1000);

		    for (LVDSLink lvdsLink : lvdsLinks) {
			lvdsLink.updateShiftTab(iLineNum, iClkInv, iRegAdd);  
		    }
		}
	    }
	}
         */
        for (int iClkInv = 0; iClkInv < 2; iClkInv++) {
            for (int iRegAdd=0; iRegAdd<2; iRegAdd++) {
                for (LVDSLink lvdsLink : lvdsLinks) {
                    lvdsLink.setRecMuxClkInv(iClkInv);
                    lvdsLink.setRecMuxRegAdd(iRegAdd);                        
                }            	

                for (LVDSLink lvdsLink : lvdsLinks) {
                    for (int iLineNum = 0; iLineNum < lvdsLink.getLinesCnt(); iLineNum++) {
                        lvdsLink.setRecMuxDalay(iLineNum, iClkInv, iRegAdd);                            
                        lvdsLink.resetRecTestOrData();
                    }
                }
                Thread.sleep(1000);

                for (LVDSLink lvdsLink : lvdsLinks) {
                    for (int iLineNum = 0; iLineNum < lvdsLink.getLinesCnt(); iLineNum++) {
                        lvdsLink.updateShiftTab(iLineNum, iClkInv, iRegAdd);  
                    }
                }
            }

        }

        for (LVDSLink lvdsLink : lvdsLinks) {
            if(lvdsLink.findShift())
                lvdsLink.setSynchroParams();
        }

        for (LVDSLink lvdsLink : lvdsLinks) {                       
            lvdsLink.resetRecTestOrData();
        }
        for (LVDSTransmDevice reciver : recivers) {
            reciver.resetRecErrorCounter();            
        }

        int wait = 1000;
        DefOut.out.println("waiting " + wait/1000 + "s");  
        Thread.sleep(wait);

        date = new Date();
        DefOut.out.println(date.toString() + ":");
        for (LVDSLink lvdsLink : lvdsLinks) {                       
            lvdsLink.readRecTestOrData(false);
        }  
        for (LVDSTransmDevice reciver : recivers) {
            reciver.readRecErrorCounter();            
        }        

        /*        for (LVDSTransmDevice transmitter : transmitters) {
            transmitter.setSendCheckDataEna();   
        }
        for (LVDSTransmDevice reciver : recivers) {
            reciver.setRecCheckDataEna();            
        } */ 
    }
   

    public void finalParametrsTune(boolean setRandomTestData) throws RemoteException, ServiceException, InterruptedException {
        Date date = new Date();
        DefOut.out.println(date.toString() + ":");

        for (LVDSTransmDevice reciver : recivers) {
            reciver.readRecErrorCounter();            
        }

        DefOut.out.println("finalParametrsTune    ------------------------------------------");
        boolean wasError = false;
        for (LVDSLink lvdsLink : lvdsLinks) {                       
            lvdsLink.finalParametrsTune();
        }       

        if(setRandomTestData) {
            for (LVDSTransmDevice transmitter : transmitters) {     
                transmitter.setRandomTestData();
            } 
        }

        for (LVDSLink lvdsLink : lvdsLinks) {                       
            lvdsLink.resetRecTestOrData();
        }
        for (LVDSTransmDevice reciver : recivers) {
            reciver.resetRecErrorCounter();            
        }

        int wait = 1000;
        DefOut.out.println("waiting " + wait/1000 + "s");  
        Thread.sleep(wait);

        date = new Date();
        DefOut.out.println(date.toString() + ":");
        for (LVDSLink lvdsLink : lvdsLinks) {                       
            lvdsLink.readRecTestOrData(false);
        }  
        for (LVDSTransmDevice reciver : recivers) {
            reciver.readRecErrorCounter();            
        }

        if(wasError) {
            DefOut.out.println("there was error, curent paremetrs");        
            for (LVDSLink lvdsLink : lvdsLinks) {                       
                DefOut.out.println(lvdsLink.printParams());
            }
        } 
    }

    public void cleanTransAndSend() throws RemoteException, ServiceException {
        for (LVDSTransmDevice transmitter : transmitters) {
            transmitter.setSendTestStaticData(0);
        }
        /*        for (RpctChip reciver : recivers) {
            reciver.setRecTestEna();
        }*/
    }

    public void printParams() {
        DefOut.out.println("");
        DefOut.out.println("found parametrs");
        for (LVDSLink lvdsLink : lvdsLinks) {                       
            DefOut.out.println(lvdsLink.printParams());
        }        
    }

    public enum TestType {
        off,
        stat,
        rnd
    }

    public void enableTransmissionCheck(boolean enableBCNcheck, boolean enableDataCheck) throws RemoteException, ServiceException {		
        DefOut.out.println("enableBCNcheck: " + enableBCNcheck + " enableDataCheck " + enableDataCheck);
        System.out.println("enableBCNcheck: " + enableBCNcheck + " enableDataCheck " + enableDataCheck);
        for (LVDSTransmDevice transmitter : transmitters) {
            transmitter.enableTransmissionCheck(enableBCNcheck, enableDataCheck); 
        }
        for (LVDSTransmDevice reciver : recivers) {
            reciver.enableTransmissionCheck(enableBCNcheck, enableDataCheck);            
        }
    }

    public void randomTest(TestType testType, boolean enableBCNcheck, boolean enableDataCheck, int waitSeconds) throws RemoteException, ServiceException, InterruptedException {
        int optoRecCheckDataEna = 0;
        DefOut.out.println("testType: " + testType + " enableBCNcheck: " + enableBCNcheck + " enableDataCheck: " + enableDataCheck + " optoRecCheckDataEna: " + optoRecCheckDataEna);   	        
        /*for (LVDSTransmDevice transmitter : transmitters) {
            transmitter.enableTransmissionCheck(enableBCNcheck, enableDataCheck);     
            if(transmitter.getDevice().getType().equals("TB3OPTO")) {            	
                transmitter.getDevice().findByName("REC_CHECK_DATA_ENA").write(optoRecCheckDataEna); 
                //TODO - disable (0) or enable (7) the input data validation. 
                //In theory it should not affect the transmission, but in practice is does
            }
        } */
        
        for (LVDSTransmDevice transmitter : transmitters) {
            transmitter.enableTransmissionCheck(enableBCNcheck, enableDataCheck);
        }
        for (LVDSTransmDevice reciver : recivers) {
            reciver.enableTransmissionCheck(enableBCNcheck, enableDataCheck);            
        }

        if (testType == TestType.stat) {
            for (LVDSTransmDevice transmitter : transmitters) {
                transmitter.setSendTestStaticData(0);
            }
            for (LVDSTransmDevice reciver : recivers) {
                reciver.setRecTestEna();
            }
        }
        else if (testType == TestType.rnd) {
            for (LVDSTransmDevice transmitter : transmitters) {
                transmitter.setSendTestRandomData();
            }
            for (LVDSTransmDevice transmitter : transmitters) {
                transmitter.setRandomTestData();
            }
            for (LVDSTransmDevice reciver : recivers) {
                reciver.setRecTRandomEna();
            }
        }
        else {
            for (LVDSTransmDevice transmitter : transmitters) {
                transmitter.setSendTestRandomData(false);
            }
            for (LVDSTransmDevice reciver : recivers) {
                reciver.setRecTRandomEna(false);
            }
        }

        for (LVDSLink lvdsLink : lvdsLinks) {                       
            lvdsLink.resetRecTestOrData();
        }        
        for (LVDSTransmDevice reciver : recivers) {
            reciver.resetRecErrorCounter();            
        }

        Thread.sleep(waitSeconds * 1000);

        for (LVDSTransmDevice reciver : getRecivers()) {
            reciver.readRecErrorCounter(System.out);            
        }
        System.out.println("randomTest finished");
    }
    
    public void randomPulsTest(TestsManager testsManager, boolean enableBCNcheck, boolean enableDataCheck, int waitSeconds) throws RemoteException, ServiceException, InterruptedException, DataAccessException, XdaqException {        
        DefOut.out.println(" enableBCNcheck: " + enableBCNcheck + " enableDataCheck: " + enableDataCheck);                       
        for (LVDSTransmDevice transmitter : transmitters) {
            transmitter.enableTransmissionCheck(enableBCNcheck, enableDataCheck);             
        }
        for (LVDSTransmDevice reciver : recivers) {
            reciver.enableTransmissionCheck(enableBCNcheck, enableDataCheck);            
        }               

        Set<Integer> sLines =  new TreeSet<Integer>();
        sLines.add(1);
        //sLines.add(3);
        testsManager.preparePulsersForSynchro();
        testsManager.startPulsers();       
        
        for (LVDSLink lvdsLink : lvdsLinks) {                       
            lvdsLink.resetRecTestOrData();
        }        
        for (LVDSTransmDevice reciver : recivers) {
            reciver.resetRecErrorCounter();            
        }
        
        Thread.sleep(waitSeconds * 1000);
                
        for (LVDSTransmDevice reciver : getRecivers()) {
            reciver.readRecErrorCounter(System.out);            
        }
        
        testsManager.stopPulsers();
        System.out.println("randomTest finished");
    }

    public Vector<LVDSTransmDevice> getRecivers() {
        return recivers;
    }

    public Vector<LVDSLink> getLvdsLinks() {
        return lvdsLinks;
    }

    public void checkInterconnections() throws RemoteException, ServiceException, InterruptedException {
        Date date = new Date();
        DefOut.out.println(date.toString() + ":");
        DefOut.out.println("testing the interconnections------------------------------------------");
        System.out.println("testing the interconnections------------------------------------------");
        //enableTransmissionCheck(false, false); robinoe w resetach
        
        for (LVDSTransmDevice transmitter : transmitters) {
            transmitter.resetLVDSLinkTrans();   
            
            DeviceItem deviceItem = transmitter.getDevice().getItem("PULSER.OUT_ENA");
            if(deviceItem != null)
                deviceItem.write(0x0);
        }

        for (LVDSTransmDevice reciver : recivers) {
            reciver.resetLVDSLinkRec();
            reciver.setRecTestEna();            
        }

        for (LVDSTransmDevice transmitter : transmitters) {
            transmitter.setSendTestStaticData("00000000000000000000000000000000000");            
        }            

        for(int i = 0; i <= 1; i++) { //walking 1, thatm walking 0
            System.out.println("testing with walking " + i + "'s -----------------------------");				       
            for (LVDSLink lvdsLink : lvdsLinks) {                       
                lvdsLink.checkInterconnections(i);
            } 
        }
        
        for (LVDSTransmDevice transmitter : transmitters) {
            transmitter.setNoSendTestEnaa();   
        }

        for (LVDSTransmDevice reciver : recivers) {
            reciver.setNoRecTestEna();                     
        }

        DefOut.out.println("interconnection test ended -----------------------------------------");
        System.out.println("interconnection test ended -----------------------------------------");
    }

    public static int getMaxMuxDelay() {
        return maxMuxDelay;
    }

    public static void setMaxMuxDelay(int maxDelay) {
        LVDSLinksSynchronizer.maxMuxDelay = maxDelay;
    }
    
    public void saveLVDSLinksData(String fileName) throws IOException {
        FileOutputStream fos = new FileOutputStream(fileName); //
    	ObjectOutputStream oos = new ObjectOutputStream(fos);
    	
    	HashMap<String, LVDSLinkData> linkDataMap = new HashMap<String, LVDSLinkData>();
    	for(LVDSLink link : lvdsLinks) {
    		LVDSLinkData previous = linkDataMap.put(link.getName(), link.getData());
    		if(previous != null)
    			throw new RuntimeException("LVDSLinkData for name " + link.getName() + " already exist in the linkDataMap");
    	}
    	
    	oos.writeObject(linkDataMap);

    	oos.close();
    }
    
    public void loadLVDSLinksData(String fileName) throws IOException, ClassNotFoundException {
    	FileInputStream fileIn = new FileInputStream(fileName);
    	ObjectInputStream in = new ObjectInputStream(fileIn);
    	HashMap<String, LVDSLinkData> linkDataMap = (HashMap<String, LVDSLinkData>) in.readObject();
    	
    	for(LVDSLink link : lvdsLinks) {
    		link.setData(linkDataMap.get(link.getName()));
    	}
    	
    	in.close();
    	fileIn.close();
    }
}
