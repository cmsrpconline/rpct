package rpct.xdaqaccess.synchro;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.db.service.axis.OptoConfigurationImplAxis;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbOpto;
import rpct.xdaqaccess.hardwaredb.HardwareDbSynCoder;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;
import rpct.xdaqaccess.hardwaredb.HOSender;

public class OptLinksSynchronizerHO {
    // List<HardwareDbSynCoder> synCoders = new ArrayList<HardwareDbSynCoder>();
    List<OptLinkSender> senders = new ArrayList<OptLinkSender>();
    List<HardwareDbOpto> optos = new ArrayList<HardwareDbOpto>();
    List<OptLink> optLinks = new ArrayList<OptLink>();

    public OptLinksSynchronizerHO() {
        super();
    }

    void addLink(OptLink optLink) {
        optLinks.add(optLink);
        if (optLink.getSender() != null
                && senders.contains(optLink.getSender()) == false)
            senders.add(optLink.getSender());
        
        if (optos.contains(optLink.getOpto()) == false) {
            optos.add(optLink.getOpto());
        }
    }

    void addLinks(List<OptLink> optLinks) {
        for (OptLink optLink : optLinks)
            addLink(optLink);
    }

    void addLinkConn(LinkConn linkConn, HardwareDbMapper dbMapper)
            throws DataAccessException, RemoteException, ServiceException {
        OptLink optLink = new OptLink((HardwareDbSynCoder) dbMapper
                .getChip(linkConn.getSynCoder()), (HardwareDbOpto) dbMapper
                .getChip(linkConn.getOpto()), linkConn.getOptoInputNum(), linkConn.getTriggerBoardInputNum());
        addLink(optLink);
    }

    void synchronize(long testData) throws DataAccessException,
            RemoteException, ServiceException, HardwareDbException,
            InterruptedException {
        for (OptLinkSender synCoder : senders) {
/*          ((HardwareDbChip) synCoder).getHardwareChip().findByName(
                    "SEND.TEST_ENA").write(0);
            ((HardwareDbChip) synCoder).getHardwareChip().findByName(
                    "PULSER.OUT_ENA").write(0);
            ((HardwareDbChip) synCoder).getHardwareChip().findByName(
                    "PULSER.LOOP_ENA").write(0);
            synCoder.enableTransmissionCheck(false, false);*/
            
            synCoder.reset();
        }

        for (HardwareDbOpto opto : optos) {
            opto.enableTransmissionCheck(false, false);
            ((HardwareDbChip) opto).getHardwareChip()
                    .getItem("REC_TEST_ENA").write(0);
        }

        for (HardwareDbOpto opto : optos) {
            opto.enableOptoLinks(true);
            opto.enableTransmissionCheck(false, false);
        }

        for (OptLinkSender synCoder : senders) {
            synCoder.enableTransmissionCheck(false, false);
            synCoder.synchronizeLink();
        }

        for (HardwareDbOpto opto : optos) {
            opto.CheckLinksSynchronization();
        }

        for (OptLinkSender synCoder : senders) {
            synCoder.normalLinkOperation();
        }

        for (HardwareDbOpto opto : optos) {
            opto.CheckLinksOperation();
        }

        for (OptLinkSender synCoder : senders) {
            synCoder.enableFindOptLinksSynchrDelay(true, testData);
        }

        for (HardwareDbOpto opto : optos) {
            opto.FindOptLinksTLKDelay(testData);
        }

        for (OptLinkSender synCoder : senders) {
            synCoder.enableFindOptLinksSynchrDelay(false, 0);
/*            ((HardwareDbChip) synCoder).getHardwareChip().findByName(
                    "SEND.TEST_ENA").write(1);*/
            
            synCoder.enableTest(1, 0);
            synCoder.enableTransmissionCheck(true, false);
        }

        /*for (HardwareDbOpto opto : optos) {
            // opto.getHardwareChip().findByName("BCN0_DELAY").write(26+2);
            opto.enableTransmissionCheck(true, false);
            opto.getHardwareChip().findByName("REC_TEST_ENA").write(opto.getUsedOptLinksInputsMask());
            opto.FindOptLinksDelay1();
        }*/

        //instead of FindOptLinksDelay
        for (HardwareDbOpto opto : optos) {
            // opto.getHardwareChip().findByName("BCN0_DELAY").write(26+2);
            opto.enableTransmissionCheck(true, false);
            opto.getHardwareChip().getItem("REC_TEST_ENA").write(opto.getUsedOptLinksInputsMask());
        }
        
        for(int delay = 0; delay < 8; delay++) {
            for (HardwareDbOpto opto : optos) 
                opto.FindOptLinksDelayStep1(delay);
            
            Thread.sleep(500);
            
            for (HardwareDbOpto opto : optos) 
                opto.FindOptLinksDelayStep2(delay);
        }
        
        for (HardwareDbOpto opto : optos) 
            opto.FindOptLinksDelayStep3();
        //end instead of FindOptLinksDelay
        
/*        for (OptLinkSender synCoder : senders) {
            // ((HardwareDbChip)synCoder).getHardwareChip().findByName("SEND.TEST_ENA").write(1);
            ((HardwareDbChip) synCoder).getHardwareChip().findByName(
                    "SEND.TEST_ENA").write(0);
            synCoder.enableTransmissionCheck(true, true);
        }

        for (HardwareDbOpto opto : optos) {
            opto.enableTransmissionCheck(true, true);
            // ((HardwareDbChip)opto).getHardwareChip().findByName("REC_TEST_ENA").write(opto.getUsedOptLinksInputsMask());
            ((HardwareDbChip) opto).getHardwareChip()
                    .findByName("REC_TEST_ENA").write(0);
        }*/
    }

    public static void main(String[] args) throws DataAccessException {
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        // ConfigurationDAO configurationDAO = new
        // ConfigurationDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(
                new HardwareRegistry(), context);
        DefOut defLog = new DefOut(System.out);
        OptLinksSynchronizerHO linksSynchronizer = new OptLinksSynchronizerHO();

        try {
            // hardware pieces selection
            HardwareDbTriggerCrate tcHdb = (HardwareDbTriggerCrate) dbMapper
                    .getCrate(equipmentDAO.getCrateByName("TC_9"));
/*
            List<HardwareDBTriggerBoard> hdTbs = new ArrayList<HardwareDBTriggerBoard>();
            HardwareDBTriggerBoard tbHdb = (HardwareDBTriggerBoard) dbMapper.getBoard(equipmentDAO.getBoardByName("TBp1_9"));
            hdTbs.add(tbHdb);
            tbHdb = (HardwareDBTriggerBoard) dbMapper.getBoard(equipmentDAO.getBoardByName("TBp2_9"));
            hdTbs.add(tbHdb);

            for (HardwareDBTriggerBoard hdTb : hdTbs) {
                for (LinkConn linkConn : hdTb.getDBTriggerBoard().getLinkConns()) {
                    if(linkConn.getSynCoder().getBoard().getName().contains("LB_RB+1_S10") ||
                            linkConn.getSynCoder().getBoard().getName().contains("LB_RB+1_S11") )

                    {
                        linksSynchronizer.addLinkConn(linkConn, dbMapper);
                    }
                }
            }

            for (HardwareDBTriggerBoard hdTb : hdTbs) {
                DefOut.out.println(hdTb.getName());
                for (LinkConn linkConn : hdTb.getDBTriggerBoard()
                        .getLinkConns()) {
                    DefOut.out.println("inputNum "
                            + linkConn.getTriggerBoardInputNum() + " "
                            + linkConn.getSynCoder().getBoard().getName());
                }
            }*/
            // ---------------------------------- HO ---------------
            
            List<HardwareDBTriggerBoard> hdTbs = new ArrayList<HardwareDBTriggerBoard>();
            HardwareDBTriggerBoard tbHdb =
                (HardwareDBTriggerBoard)dbMapper.getBoard(equipmentDAO.getBoardByName("TBn3_9"));

            HOSender hoSender = new HOSender();
            
/*            linksSynchronizer.addLink(new OptLink(hoSender, tbHdb.getOptos().get(0), 0)); 
            
            linksSynchronizer.addLink(new OptLink(hoSender, tbHdb.getOptos().get(0), 1));
            
            linksSynchronizer.addLink(new OptLink(hoSender, tbHdb.getOptos().get(0), 2));

            linksSynchronizer.addLink(new OptLink(hoSender, tbHdb.getOptos().get(1), 0)); 
            
            linksSynchronizer.addLink(new OptLink(hoSender, tbHdb.getOptos().get(1), 1));
            
            linksSynchronizer.addLink(new OptLink(hoSender, tbHdb.getOptos().get(1), 2));
            
            linksSynchronizer.addLink(new OptLink(hoSender, tbHdb.getOptos().get(2), 0));*/
             

            // -------------------------------------
            long testData = 0x12345678;
            linksSynchronizer.synchronize(testData);

/*            int tcSortINputMask = 0x40;
            if (false) {
                tcHdb.getTcSort().getHardwareChip().findByName("REC_CHAN_ENA")
                        .write(tcSortINputMask);
                System.out
                        .println(tcHdb.getTcSort().getFullName()
                                + " tc gb-sort inouts: "
                                + Integer.toHexString(tcSortINputMask)
                                + " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            }
            for (HardwareDBTriggerBoard t : tcHdb.getHardwareDBTriggerBoards()) {
                // if(t.getName().equals("TBp2_9") == false)
                {
                    if (false) {
                        t.getRmb().getHardwareChip().findByName("RMB_CHAN_ENA")
                                .write(0x3ffff);
                        System.out
                                .println(t.getRmb().getFullName()
                                        + " input channels disabled!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                    }
                    if (false) {
                        t.getRmb().getHardwareChip().findByName("RMB_CHAN_ENA")
                                .write(0);
                        System.out
                                .println(t.getRmb().getFullName()
                                        + " input channels enabled!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                    }

                }
                t.getRmb().getHardwareChip().findByName("STATUS.RMB_RESET")
                        .write(1);
                Thread.sleep(10);
                t.getRmb().getHardwareChip().findByName("STATUS.RMB_RESET")
                        .write(0);
                System.out.println(t.getRmb().getFullName() + " reseted");
            }*/

        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (HardwareDbException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}
