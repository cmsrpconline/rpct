package rpct.xdaqaccess.synchro;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.DeviceItem;
import rpct.xdaqaccess.DeviceItemWord;

public class LVDSTransmDeviceHSB extends LVDSTransmDevice {

    public LVDSTransmDeviceHSB(Device device) {
        super(device);
        inLVDSlinksCnt = device.getItem("REC_FAST_REG_ADD").getDesc().getNumber();
        //inLinesInLinkCnt = reMuxClkInv.getInfo().getWidth()/inLVDSlinksCnt; 
        inLinesInLinkCnt = device.getItem("REC_FAST_REG_ADD").getDesc().getWidth();
    }

    public void resetLVDSLinkRec() throws RemoteException, ServiceException, InterruptedException {
        DeviceItem deviceItem;
        deviceItem = device.getItem("REC_CHECK_ENA");
        if(deviceItem != null)
            deviceItem.write(0x0);
        deviceItem = device.getItem("REC_CHECK_DATA_ENA");
        if(deviceItem != null)
            deviceItem.write(0x0);

        deviceItem = device.getItem("REC_TEST_ENA");
        if(deviceItem != null)
            deviceItem.write(0x0);


        device.getItem("REC_FAST_DATA_DELAY").write(0x0);
        //device.findByName("REC_DATA_DELAY").write(0x0); //set during boards setup, don't reset here

        DeviceItemWord recClkInv = (DeviceItemWord) device.getItem("REC_FAST_CLK_INV");       
        DeviceItemWord recRegAdd = (DeviceItemWord) device.getItem("REC_FAST_REG_ADD");
        for(int i = 0; i < inLVDSlinksCnt; i++) {
            if(recClkInv != null)
                recClkInv.write(i, 0);
            if(recRegAdd != null)
                recRegAdd.write (i, 0 );  
        }        
    }

    public void setMuxRegAdd(int regAdd) throws RemoteException, ServiceException {
        DeviceItemWord recRegAdd = (DeviceItemWord) device.getItem("REC_FAST_REG_ADD");
        for(int i = 0; i < inLVDSlinksCnt; i++) {   
            if(regAdd == 0) {
                recRegAdd.write (i, 0 ); 
            }
            else if(regAdd == 1) {
                recRegAdd.write (i, 0xffffff);  
            }
            else
                throw new IllegalArgumentException("setMuxRegAdd(int regAdd): regAdd != 0 || regAdd != 1");
        }
    }

    public void setMuxClkInv(int clkInv) throws RemoteException, ServiceException {
        DeviceItemWord recClkInv = (DeviceItemWord) device.getItem("REC_FAST_CLK_INV");
        for(int i = 0; i < inLVDSlinksCnt; i++) {   
            if(clkInv == 0) {
                recClkInv.write (i, 0 ); 
            }
            else if(clkInv == 1) {
                recClkInv.write (i, 0xffffff);  
            }
            else
                throw new IllegalArgumentException("setMuxClkInv(int clkInv): clkInv != 0 || clkInv != 1");
        }
    }

    public void setMuxClk90(int clkInv) throws RemoteException, ServiceException {
        DeviceItemWord recClk90 = (DeviceItemWord) device.getItem("REC_FAST_CLK90");
        for(int i = 0; i < inLVDSlinksCnt; i++) {   
            if(clkInv == 0) {
                recClk90.write (i, 0 ); 
            }
            else if(clkInv == 1) {
                recClk90.write (i, 0xffffff);  
            }
            else
                throw new IllegalArgumentException("setMuxClk90(int clkInv): clkInv != 0 || clkInv != 1");
        }
    }
    
    public void setMuxClkPhase(int clkPhase) throws RemoteException, ServiceException {
        setMuxClkInv(clkPhase/2);
        setMuxClk90(clkPhase%2);        
    }
    
    public void setRecMuxDelayAndDataDelay(int muxDelay) throws RemoteException, ServiceException {
        if(muxDelay > 1)
            return;

        int dataDelay;
        if(muxDelay >= 1)
            dataDelay = 0;
        else
            dataDelay = 0;

        int muxDelayVal = 0;
        int dataDelayVal = 0;
        for(int i = 0; i < inLVDSlinksCnt; i++) {
            muxDelayVal |= muxDelay << (i);
            dataDelayVal |= dataDelay << (2 * i);
        }

        DeviceItemWord recMuxDealy = (DeviceItemWord) device.getItem("REC_FAST_DATA_DELAY");
        DeviceItemWord dataDealy = (DeviceItemWord) device.getItem("REC_DATA_DELAY");

        recMuxDealy.write(muxDelayVal);
        //dataDealy.write(dataDelayVal); //we dont want to set this here
    }

    
    public void setRecMuxDelayAndDataDelay(int muxDelay, int linkNum) throws RemoteException, ServiceException {
        if(muxDelay > 1)
            return;

        DeviceItemWord recMuxDealy = (DeviceItemWord) device.getItem("REC_FAST_DATA_DELAY");
        DeviceItemWord dataDealy = (DeviceItemWord) device.getItem("REC_DATA_DELAY");
        
        long muxDelayVal = recMuxDealy.read();
        long dataDelayVal = dataDealy.read();
        
        int dataDelay;
        if(muxDelay >= 1)
            dataDelay = 0;
        else
            dataDelay = 0;

        muxDelayVal &= ~(1l << (linkNum)); //kasowanie bitu        
        muxDelayVal |= (long)muxDelay << (linkNum); //ustawianie bitu
        muxDelayVal &= 0xffffl;
        
        dataDelayVal &= ~(3l << (2 * linkNum));
        dataDelayVal |= (long)dataDelay << (2 * linkNum);

       
        recMuxDealy.write(muxDelayVal);
        //dataDealy.write(dataDelayVal); nie chcemy tutaj w ogole ustawiac tego
    }
    
    public void setRecFastDataDelay(int recFastDataDelay, int linkNum) throws RemoteException, ServiceException {
        if(recFastDataDelay > 7)
            throw new IllegalArgumentException("recFastDataDelay > 7");

        DeviceItemWord recMuxDealy = (DeviceItemWord) device.getItem("REC_FAST_DATA_DELAY");      
        if(recMuxDealy.getDesc().getWidth() == 48) {            
            Binary muxDelayVal = recMuxDealy.readBinary();
            byte[] muxDelaybytes = muxDelayVal.getBytes();        
            Binary.setBits(muxDelaybytes, 3 * linkNum, recFastDataDelay, 3);

            recMuxDealy.writeBinary(new Binary(muxDelaybytes));
        } 
        else if(recMuxDealy.getDesc().getWidth() == 32) {  
            setRecFastDataDelayAndClkInv(recFastDataDelay/2, recFastDataDelay%2, linkNum);
        }        
        else {
            throw new RuntimeException("REC_FAST_DATA_DELAY != 48  this function is not sutable to thath version of firmware");
        }
    }
    
    public void setRecFastDataDelayAndClkInv(int recFastDataDelay, int clkInv, int linkNum) throws RemoteException, ServiceException {
        if(recFastDataDelay > 3)
            throw new IllegalArgumentException("recFastDataDelay > 7");

        DeviceItemWord recMuxDealy = (DeviceItemWord) device.getItem("REC_FAST_DATA_DELAY");      
        if(recMuxDealy.getDesc().getWidth() != 32) {
            throw new RuntimeException("REC_FAST_DATA_DELAY != 48  this function is not sutable to thath version of firmware");
        }        
        
        Binary muxDelayVal = recMuxDealy.readBinary();
        byte[] muxDelaybytes = muxDelayVal.getBytes();        
        Binary.setBits(muxDelaybytes, 2 * linkNum, recFastDataDelay, 2);
        
        recMuxDealy.writeBinary(new Binary(muxDelaybytes));
        
        DeviceItemWord recClkInv = (DeviceItemWord) device.getItem("REC_FAST_CLK_INV");
        if(clkInv == 0)
            recClkInv.write (linkNum, 0);
        else
            recClkInv.write (linkNum, 0xffffff);
    }
    
    public void setMuxClkPhase(int clkPhase, int sLine) throws RemoteException, ServiceException {
        DeviceItemWord recClkInv = (DeviceItemWord) device.getItem("REC_FAST_CLK_INV");
        DeviceItemWord recClk90 = (DeviceItemWord) device.getItem("REC_FAST_CLK90");
        for(int iLink = 0; iLink < inLVDSlinksCnt; iLink++) {
            long invVal = recClkInv.read(iLink);
            long clk90Val = recClk90.read(iLink);
            for(int iLine = 0; iLine < inLinesInLinkCnt; iLine++) {
                if(inLinkDef.isLineActive(sLine, iLine)) {
                    if(clkPhase/2 == 0) {
                        invVal = invVal & (~(1<<iLine));
                    }
                    else if(clkPhase/2 == 1) {
                        invVal = invVal | (1<<iLine);
                    }
                    
                    if(clkPhase%2 == 0) {
                        clk90Val = clk90Val & (~(1<<iLine));
                    }
                    else if(clkPhase%2 == 1) {
                        clk90Val = clk90Val | (1<<iLine);
                    }                   
                   //throw new IllegalArgumentException("setMuxClkPhase(int clkPhase, int sLine): clkInv != 0 || clkInv != 1");                                       
                }                 
            }
            recClkInv.write (iLink, invVal & 0xffffff);
            recClk90.write (iLink, clk90Val & 0xffffff);
        }
    }
    
    public void setMuxRegAdd(int regAdd, int sLine) throws RemoteException, ServiceException {
        DeviceItemWord recRegAdd = (DeviceItemWord) device.getItem("REC_FAST_REG_ADD");
        for(int iLink = 0; iLink < inLVDSlinksCnt; iLink++) {
            long data = recRegAdd.read(iLink);
            for(int iLine = 0; iLine < inLinesInLinkCnt; iLine++) {
                if(inLinkDef.isLineActive(sLine, iLine)) {
                    if(regAdd == 0) {
                        data = data & (~(1<<iLine));
                    }
                    else if(regAdd == 1) {
                        data = data | (1<<iLine);
                    }
                    else
                        throw new IllegalArgumentException("setMuxRegAdd(int regAdd, int sLine): clkInv != 0 || clkInv != 1");                                         
                }                
            }
            recRegAdd.write (iLink, data & 0xffffff);
        }
    }
    
    public void setMuxClkInv(int[] clkInvTab, int offset) throws RemoteException, ServiceException {
        long clkINv = 0;        
        for(int i = 0; i < clkInvTab.length; i++) {  
            if(clkInvTab[i] > 2) 
                throw new IllegalArgumentException("clkInvab[i] > 2");
            if(clkInvTab[i] >= 0)
                clkINv |= clkInvTab[i] << i;
        }
        ((DeviceItemWord)device.getItem("REC_FAST_CLK_INV")).write(offset/inLinesInLinkCnt, clkINv); 
    }
    
    public void setMuxClkPhase(int[] clkInvTab, int offset) throws RemoteException, ServiceException {
        long clkINv = 0;   
        long clk90= 0;
        for(int i = 0; i < clkInvTab.length; i++) {  
            if(clkInvTab[i] >= 4) 
                throw new IllegalArgumentException("clkInvab[i] >= 4");
            if(clkInvTab[i] >= 0) {
                clkINv |= clkInvTab[i]/2 << i;
                clk90 |= clkInvTab[i]%2 << i;
            }
        }
        ((DeviceItemWord)device.getItem("REC_FAST_CLK_INV")).write(offset/inLinesInLinkCnt, clkINv); 
        ((DeviceItemWord)device.getItem("REC_FAST_CLK90")).write(offset/inLinesInLinkCnt, clk90); 
    }
    
    public void setMuxRegAdd(int[] regAddTab, int offset) throws RemoteException, ServiceException {
        long regAdd = 0;        
        for(int i = 0; i < regAddTab.length; i++) {  
            if(regAddTab[i] > 2) 
                throw new IllegalArgumentException("regAddTab[i] > 2");
            if(regAddTab[i] >= 0)
                regAdd |= regAddTab[i] << i;
        }
        ((DeviceItemWord)device.getItem("REC_FAST_REG_ADD")).write(offset/inLinesInLinkCnt, regAdd); 
    }
    
}