package rpct.xdaqaccess.synchro;

import java.rmi.RemoteException;
import java.util.List;

import javax.xml.rpc.ServiceException;

public interface OptLinkSender {
    public void reset() throws RemoteException, ServiceException;
    
	public void synchronizeLink() throws RemoteException, ServiceException;
	
	public void normalLinkOperation() throws RemoteException, ServiceException;
	
	public void enableFindOptLinksSynchrDelay(boolean enable, long testData) throws RemoteException, ServiceException;
	
	public void enableTransmissionCheck(boolean enableBCNcheck, boolean enableDataCheck) throws RemoteException, ServiceException;
	
	public void enableTest(int testEna, int rndTestEna) throws RemoteException, ServiceException;

    public void registerUsedOptLink(OptLink optLink);
    
    public List<OptLink> getUsedOptLinks();
    
    public String getFullName();

    public void setSendDelay(int foundDelay) throws RemoteException, ServiceException;
    
    public int getSendDelay();
}
