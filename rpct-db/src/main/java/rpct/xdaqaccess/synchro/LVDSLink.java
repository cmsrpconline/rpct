package rpct.xdaqaccess.synchro;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;

import javax.xml.rpc.ServiceException;

import rpct.db.domain.equipment.ChipType;
import rpct.synch.LVDSLinkDef;
import rpct.synch.PulserGeneratorForSynchronization;
import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.DeviceItemArea;
import rpct.xdaqaccess.DeviceItemWord;


/**
 * @author Karol
 * one link connects two chips: transmitter with reciver. One link contains linesCnt of LVDS lines (2 wires)
 * linkNum is the number of LVDSLink in the reciver
 */
/**
 * @author Karol
 *
 */
public class LVDSLink {
    LVDSLinkDef linkDef;

    private LVDSTransmDevice transmitter;
    private LVDSTransmDevice reciver;    
    private int linkNum;
    private int linesCnt;

    private LVDSLinkData data = new LVDSLinkData();
    
    private Set<Integer> activatedLines = new HashSet<Integer>();

    private String name;

    private int bitsPerLine;

    static public int clkPhaseSteps = 4;

    public LVDSLink(LVDSTransmDevice transmitter, LVDSTransmDevice reciver, int number) throws Exception {
        this.transmitter = transmitter;
        this.reciver  = reciver;
        linkNum = number;
        linesCnt = this.reciver.getInLinesInLinkCnt();

        name = transmitter.getDevice().getBoard().getName() + " " + 
        transmitter.getDevice().getName() + " - " + reciver.getDevice().getBoard().getName() + " " + reciver.getDevice().getName() + " linkNum " + linkNum;        

        data.foundClkInv = new int [linesCnt];
        data.foundRegAdd = new int [linesCnt];

        data.shiftTab = new int[linesCnt][clkPhaseSteps][2];
        data.delayIsOkTab = new boolean[linesCnt][clkPhaseSteps][2][LVDSLinksSynchronizer.getMaxMuxDelay()];
        for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {
        	data.shiftTab[iLineNum] = new int[clkPhaseSteps][2];     
        	data.delayIsOkTab[iLineNum] = new boolean[clkPhaseSteps][2][LVDSLinksSynchronizer.getMaxMuxDelay()];
            for(int iclkPhase = 0; iclkPhase <clkPhaseSteps; iclkPhase++){
            	data.shiftTab[iLineNum][iclkPhase] = new int[2];  
            	data.delayIsOkTab[iLineNum][iclkPhase] = new boolean[2][LVDSLinksSynchronizer.getMaxMuxDelay()];
                for(int iRegAdd = 0; iRegAdd < 2; iRegAdd++) {
                	data.shiftTab[iLineNum][iclkPhase][iRegAdd] = -100;
                	data.delayIsOkTab[iLineNum][iclkPhase][iRegAdd] = new boolean[LVDSLinksSynchronizer.getMaxMuxDelay()];
                    for (int delay = 0; delay < LVDSLinksSynchronizer.getMaxMuxDelay(); delay++) {
                    	data.delayIsOkTab[iLineNum][iclkPhase][iRegAdd][delay] = true;
                    }
                    
                }
            }
            /*            int[][] t = {
                    {-10, -10, -10, -10},
                    {-10, -10, -10, -10}
            };
            data.shiftTab[iLineNum] = t;*/
        }

        linkDef = transmitter.getOutLinkDef(); 
        bitsPerLine = linkDef.getBitsPerLine();

        this.reciver.registerInputLink(number, this);            
        this.transmitter.registerOutLink(this);                       
    }
    
    public LVDSLinkData getData() {
    	return data;    	
    }

    public void setData(LVDSLinkData data) {
    	this.data = data;    	
    }
    
    public int getDataBitsCnt(int lineNum) {
        return 0;
    }   

    /*    public int[] getSelClkInv() {
        return foundClkInv;
    }

    public int[] getSelRegAdd() {
        return data.foundRegAdd;
    }

    public int[][][] getShiftTab() {
        return data.shiftTab;
    }
     */

    public int getFoundShift() {
        /*if(foundClkInv[0] != -1)                
            return data.shiftTab[0][foundClkInv[0]][data.foundRegAdd[0]];
        else
            return -1;*/

        return data.foundShift;
    }

    public void setRecMuxClkInv(int lineNum, int value) throws RemoteException, ServiceException  {
        Binary b = reciver.getDevice().getItem("REC_MUX_CLK_INV").readBinary();
        byte[] bytes = b.getBytes();
        int bitPos = linesCnt * linkNum + lineNum;
        int bitInByte = bitPos % 8; 
        int bytePos = bytes.length - 1 - bitPos / 8;
        if (value == 1)
            bytes[bytePos] |= (1 << bitInByte);
        else
            bytes[bytePos] &= (~(1 << bitInByte));
        reciver.getDevice().getItem("REC_MUX_CLK_INV").writeBinary(new Binary(bytes));   
    }

    public void setRecMuxRegAdd(int lineNum, int value) throws RemoteException, ServiceException  {
        Binary b = reciver.getDevice().getItem("REC_MUX_REG_ADD").readBinary();
        byte[] bytes = b.getBytes();
        int bitPos = linesCnt * linkNum + lineNum;
        int bitInByte = bitPos % 8; 
        int bytePos = bytes.length - 1 - bitPos / 8;
        if (value == 1)
            bytes[bytePos] |= (1 << bitInByte);
        else
            bytes[bytePos] &= (~(1 << bitInByte));
        reciver.getDevice().getItem("REC_MUX_REG_ADD").writeBinary(new Binary(bytes));   
    }

    /**
     * @param value 0 or 1 only 
     * @throws RemoteException, ServiceException
     * sets the same value for all lines
     */
    public void setRecMuxClkInv(int value) throws RemoteException, ServiceException {
        Binary b = reciver.getDevice().getItem("REC_MUX_CLK_INV").readBinary();
        byte[] bytes = b.getBytes();
        for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {
            int bitPos = linesCnt * linkNum + iLineNum;
            int bitInByte = bitPos % 8; 
            int bytePos = bytes.length - 1 - bitPos / 8;
            if (value == 1)
                bytes[bytePos] |= (1 << bitInByte);
            else
                bytes[bytePos] &= (~(1 << bitInByte));
        }
        reciver.getDevice().getItem("REC_MUX_CLK_INV").writeBinary(new Binary(bytes));   
    }

    /**
     * @param value 0 or 1 only 
     * @throws RemoteException, ServiceException
     * sets the same value for all lines
     */
    public void setRecMuxRegAdd(int value) throws RemoteException, ServiceException {
        Binary b = reciver.getDevice().getItem("REC_MUX_REG_ADD").readBinary();
        byte[] bytes = b.getBytes();
        for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {
            int bitPos = linesCnt * linkNum + iLineNum;
            int bitInByte = bitPos % 8; 
            int bytePos = bytes.length - 1 - bitPos / 8;
            if (value == 1)
                bytes[bytePos] |= (1 << bitInByte);
            else
                bytes[bytePos] &= (~(1 << bitInByte));
        }
        reciver.getDevice().getItem("REC_MUX_REG_ADD").writeBinary(new Binary(bytes));   
    }

    public void setRecMuxDelay(int value) throws RemoteException, ServiceException {
        DeviceItemWord recDelay = (DeviceItemWord) reciver.getDevice().getItem("REC_MUX_DELAY");
        recDelay.write (linkNum, value );       
    }

    void setRecDataDelay(int value) throws RemoteException, ServiceException {
        DeviceItemWord recDelay = (DeviceItemWord) reciver.getDevice().getItem("REC_DATA_DELAY");
        recDelay.write (linkNum, value );       
    }

    public void setRecMuxDelayAndDataDelay(int muxDelay) throws RemoteException, ServiceException {
        setRecMuxDelay(muxDelay);

        int dataDelay;
        if(muxDelay >= 0 && muxDelay <= 2)
            dataDelay = 1;
        else
            dataDelay = 0;

        setRecDataDelay(dataDelay);
    }

    public void checkTestData(String testData) throws RemoteException, ServiceException, InterruptedException { 
        Binary recTestData = ((DeviceItemWord)reciver.getDevice().getItem("REC_TEST_DATA")).readBinary(linkNum);
        Binary testDataBynary = new Binary(testData.substring(0,(linesCnt*8)/4), linesCnt*8);
        if(!recTestData.equals(testDataBynary)) {
            DefOut.out.println(name + " checkTestData: recTestData != testData: " + recTestData + " " + testDataBynary);
        }
    }

    /**
     * @param testData
     * @throws RemoteException, ServiceException
     * @throws InterruptedException
     * 
     * reciver.setRecTestEna()  and
     * transmitter.setSendTestData(testData) should be called first
     */
    public void scanSynchroParams(String testData) throws RemoteException, ServiceException, InterruptedException {                  
        //setRecMuxDelay(1);
        boolean isOK[] = new boolean[linesCnt];
        for (int iClkInv = 0; iClkInv < clkPhaseSteps; iClkInv++) {
            for (int iRegAdd=0; iRegAdd<2; iRegAdd++) {       
                //ustawiam paraemtry wszystkim liniom
                /*            for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {    
                    setRecMuxClkInv(iLineNum, iClkInv);
                    setRecMuxRegAdd(iLineNum, iRegAdd);                                       
                } */       
                setRecMuxClkInv(iClkInv);
                setRecMuxRegAdd(iRegAdd);
                Binary recTestData = ((DeviceItemWord)reciver.getDevice().getItem("REC_TEST_DATA")).readBinary(linkNum);
                //recTestData = ((DeviceItemWord)reciver.getDevice().findByName("REC_TEST_DATA")).readBinary(linkNum);
                //szukam sziftu dla kazdej linii
                for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {
                    int foundShift = -1;
                    byte recTestDataByte = recTestData.getBytes()[linesCnt -1 - iLineNum];
                    Binary testDataBynary = new Binary(testData.substring(0,(linesCnt*8)/4));
                    byte sentTestDataByte = testDataBynary.getBytes()[linesCnt -1 - iLineNum];
                    if(sentTestDataByte != 0x01 && sentTestDataByte != 0x10) {
                        if((recTestDataByte == 0) || (recTestDataByte == 0xff) ) {
                            DefOut.out.print(name + " LineNum " + iLineNum); 
                            DefOut.out.println(" iClkInv " + iClkInv + " iRegAdd " + iRegAdd + "  recTestDataByte for that line = " + recTestDataByte + " possible line damage!!!!!!!!!");
                        }
                    }
                    //byte recTestDataByteBuf = recTestDataByte;
                    int shift = 0;
                    for ( ; shift < bitsPerLine ; shift++ ) {
                        if (recTestDataByte ==  sentTestDataByte) {                            
                            foundShift = shift;
                            break;                                                                       
                        }

                        int a = 0;
                        if((recTestDataByte & 0x80) == 0x80)
                            a = 1;

                        recTestDataByte = (byte)(((recTestDataByte << 1) & 0xff) | a);

                    }             
                    /*                    if(shift == bitsPerLine)
                        foundShift = -1; */       
                    if(foundShift != -1)
                        isOK[iLineNum] = true;
                    if(data.shiftTab[iLineNum][iClkInv][iRegAdd] == -100) //pierwszy raz to robimy
                    	data.shiftTab[iLineNum][iClkInv][iRegAdd] = foundShift;
                    else {
                        if(data.shiftTab[iLineNum][iClkInv][iRegAdd] != foundShift) //sprawdzamy, czy z inna testData wyszlo to samo
                        	data.shiftTab[iLineNum][iClkInv][iRegAdd] = -21;                       
                    }
                }
            }
        }
        for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {
            if(isOK[iLineNum] = false) {
                int asd = 0;	
            }	
        }
    }

    /**
     * @throws RemoteException, ServiceException
     * @throws InterruptedException
     *  transmitter.setSendTestRandomData() and reciver.setRecTRandomEna() should be called first
     */
    /*    void testSynchroParamsWithRnd() throws RemoteException, ServiceException, InterruptedException {              
        for (int iClkInv = 0; iClkInv < 2; iClkInv++) {
            for (int iRegAdd=0; iRegAdd<2; iRegAdd++) {  
                for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {    
                    setRecMuxClkInv(iLineNum, iClkInv);
                    setRecMuxRegAdd(iLineNum, iRegAdd);

                    setRecDelay(bitsPerLine - data.shiftTab[iLineNum][iClkInv][iRegAdd]);                    
                    ((DeviceItemWord)reciver.getDevice().findByName("REC_TEST_OR_DATA")).read(linkNum);
                    ((DeviceItemWord)reciver.getDevice().findByName("REC_TEST_OR_DATA")).read(linkNum);
                    Thread.sleep(1000);

                    long recTestOrData = ((DeviceItemWord)reciver.getDevice().findByName("REC_TEST_OR_DATA")).read(linkNum);
                    if(((1<<iLineNum) & recTestOrData) != 0)
                        data.shiftTab[iLineNum][iClkInv][iRegAdd] = -2;                                                  
                }
            }
        }
    }*/ 

    boolean findShift() {
        //for (int shift = bitsPerLine -1; shift >= 0; shift--) { //TODO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        for (int shift = 0; shift < bitsPerLine; shift++) {
            for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {
                if(activatedLines.contains(iLineNum) == false)
                    continue;
                data.foundClkInv[iLineNum] = -1;                
                data.foundRegAdd[iLineNum] = -1;                              
                for (int iRegAdd=0; iRegAdd<2; iRegAdd++) {
                    boolean was = false;
                    for (int iClkInv=0; iClkInv < clkPhaseSteps; iClkInv++)  {
                        if (data.shiftTab [iLineNum][iClkInv][iRegAdd] == shift){
                            data.foundClkInv[iLineNum]= iClkInv;
                            data.foundRegAdd[iLineNum]= iRegAdd;
                            was = true;
                            break;
                        }
                    }
                    if(was)
                        break;
                }
            }
            boolean isOk = true;
            for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {
                if(activatedLines.contains(iLineNum) == false)
                    continue;
                if(data.foundClkInv[iLineNum] == -1) {
                    isOk = false;
                    break;
                }   
            }
            if(isOk) {
                //printing results
                DefOut.out.println(name);                
                DefOut.out.println("clkInv\\regAdd shift table");                
                for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {     
                    if(activatedLines.contains(iLineNum) == false)
                        continue;
                    DefOut.out.println("LineNum" + iLineNum);
                    for (int iClkInv = 0; iClkInv < clkPhaseSteps; iClkInv++)  {  
                        String str = "";
                        for (int iRegAdd = 0; iRegAdd < 2; iRegAdd++) 
                            str = str + data.shiftTab [iLineNum][iClkInv][iRegAdd] + " ";
                        DefOut.out.println(str);
                    }
                }

                String foundClkInvStr = "foundClkInv ";
                String foundRegAddStr = "foundRegAdd ";
                String shiftTabStr = "founfShift  ";
                for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {
                    if(activatedLines.contains(iLineNum) == false)
                        continue;
                    foundClkInvStr =  foundClkInvStr  + "[" + data.foundClkInv[iLineNum] +"] ";
                    foundRegAddStr =  foundRegAddStr  + "[" + data.foundRegAdd[iLineNum] +"] ";
                    shiftTabStr = shiftTabStr + "[" + data.shiftTab[iLineNum][data.foundClkInv[iLineNum]][data.foundRegAdd[iLineNum]]+"] ";
                    data.foundShift = data.shiftTab[iLineNum][data.foundClkInv[iLineNum]][data.foundRegAdd[iLineNum]];
                }
                DefOut.out.println(foundClkInvStr);
                DefOut.out.println(foundRegAddStr);
                DefOut.out.println(shiftTabStr);
                DefOut.out.println("");
                return true;
            }               
        }
        DefOut.out.println(name);                
        DefOut.out.println("clkInv\\regAdd shift table");                
        for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {                    
            DefOut.out.println("LineNum" + iLineNum);
            for (int iClkInv = 0; iClkInv < clkPhaseSteps; iClkInv++)  {  
                String str = "";
                for (int iRegAdd = 0; iRegAdd < 2; iRegAdd++) 
                    str = str + data.shiftTab [iLineNum][iClkInv][iRegAdd] + " ";
                DefOut.out.println(str);
            }
        }
        return false;
    } 

    boolean findSynchParams(int shift, boolean print) {
        for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {
            if(activatedLines.contains(iLineNum) == false)
                continue;
            data.foundClkInv[iLineNum] = -1;                
            data.foundRegAdd[iLineNum] = -1;                              
            for (int iRegAdd=0; iRegAdd<2; iRegAdd++) {
                boolean was = false;
                for (int iClkInv=0; iClkInv < clkPhaseSteps; iClkInv++)  {
                    if (data.shiftTab [iLineNum][iClkInv][iRegAdd] == shift){
                        data.foundClkInv[iLineNum]= iClkInv;
                        data.foundRegAdd[iLineNum]= iRegAdd;
                        was = true;
                        break;
                    }
                }
                if(was)
                    break;
            }
        }
        boolean isOk = true;
        for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {
            if(activatedLines.contains(iLineNum) == false)
                continue;
            if(data.foundClkInv[iLineNum] == -1) {
                isOk = false;
                break;
            }   
        }
        if(isOk) {
        	data.foundShift = shift;
            if(print) {
                DefOut.out.println(name);                
                DefOut.out.println("clkInv\\regAdd shift table");                
                for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {                     
                    DefOut.out.println("LineNum" + iLineNum);
                    if(activatedLines.contains(iLineNum) == false)
                        continue;
                    for (int iClkInv = 0; iClkInv < clkPhaseSteps; iClkInv++)  {  
                        String str = "";
                        for (int iRegAdd = 0; iRegAdd < 2; iRegAdd++) 
                            str = str + data.shiftTab [iLineNum][iClkInv][iRegAdd] + " ";
                        DefOut.out.println(str);
                    }
                }

                String foundClkInvStr = "foundClkInv ";
                String foundRegAddStr = "foundRegAdd ";
                String shiftTabStr = "founfShift  ";
                for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {
                    if(activatedLines.contains(iLineNum) == false)
                        continue;
                    foundClkInvStr =  foundClkInvStr  + "[" + data.foundClkInv[iLineNum] +"] ";
                    foundRegAddStr =  foundRegAddStr  + "[" + data.foundRegAdd[iLineNum] +"] ";
                    shiftTabStr = shiftTabStr + "[" + data.shiftTab[iLineNum][data.foundClkInv[iLineNum]][data.foundRegAdd[iLineNum]]+"] ";

                }
                DefOut.out.println(foundClkInvStr);
                DefOut.out.println(foundRegAddStr);
                DefOut.out.println(shiftTabStr);
                DefOut.out.println("");
            }
            return true;
        }                       

        data.foundShift = -1;
        if(print) {
            DefOut.out.println(name);                
            DefOut.out.println("clkInv\\regAdd shift table");                
            for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {                    
                DefOut.out.println("LineNum" + iLineNum);
                if(activatedLines.contains(iLineNum) == false)
                    continue;
                for (int iClkInv = 0; iClkInv < clkPhaseSteps; iClkInv++)  {  
                    String str = "";
                    for (int iRegAdd = 0; iRegAdd < 2; iRegAdd++) 
                        str = str + data.shiftTab [iLineNum][iClkInv][iRegAdd] + " ";
                    DefOut.out.println(str);
                }
            }
            DefOut.out.println(name + " synch params not found!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            DefOut.out.println("");
            System.out.println(name + " synch params not found!!!");
        }
        return false;
    } 

    public boolean sharpenShifTab() {
        boolean wasSharpen = false;
        for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {            
            boolean allNotGoodRegAdd0 =  true;
            boolean allNotGoodRegAdd1 =  true;
            for (int iClkPhase = 0; iClkPhase < clkPhaseSteps; iClkPhase++)  {  
                if(data.shiftTab [iLineNum][iClkPhase][0] >= 0)
                    allNotGoodRegAdd0 = false;

                if(data.shiftTab [iLineNum][iClkPhase][1] >= 0)
                    allNotGoodRegAdd1 = false;
            }

            if(allNotGoodRegAdd0 || allNotGoodRegAdd1) {
                //System.out.println("sharpenShifTab() "+ name + " line " + iLineNum + " allNotGoodRegAdd0 || allNotGoodRegAdd1 - this is strange");
            }
            else {
                int notGoodClkPhaseCnt = 0;
                int notGoodClkPhase = -1;                       
                for (int iClkPhase = 0; iClkPhase < clkPhaseSteps; iClkPhase++)  {  
                    if(data.shiftTab [iLineNum][iClkPhase][0] < 0  && data.shiftTab [iLineNum][iClkPhase][1] >=0 ) {
                        data.shiftTab [iLineNum][iClkPhase][1] = data.shiftTab [iLineNum][iClkPhase][1] * (-1) -10;
                        wasSharpen = true;
                    }
                    else if(data.shiftTab [iLineNum][iClkPhase][1] < 0 && data.shiftTab [iLineNum][iClkPhase][0] >= 0) {
                        data.shiftTab [iLineNum][iClkPhase][0] = data.shiftTab [iLineNum][iClkPhase][0] * (-1) -10;     
                        wasSharpen = true;
                    }

                    if(data.shiftTab [iLineNum][iClkPhase][0] < 0) {
                        notGoodClkPhaseCnt++;
                        notGoodClkPhase = iClkPhase;
                    }
                }

                if(notGoodClkPhaseCnt == 1) {
                    int clkPhase = (notGoodClkPhase + 1) % clkPhaseSteps;
                    for(int i = 0; i < 2; i++) {                       
                        for (int iRegAdd = 0; iRegAdd < 2; iRegAdd++) {
                            data.shiftTab[iLineNum][clkPhase][iRegAdd] = data.shiftTab[iLineNum][clkPhase][iRegAdd] * (-1) -10;
                            wasSharpen = true;
                        }                        
                        clkPhase = (notGoodClkPhase - 1 + clkPhaseSteps) % clkPhaseSteps;
                    }
                }            
            }
        }
        return wasSharpen;
    }

    public void unSharpenShifTab() {
        DefOut.out.println("unSharpenShifTab");
        for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {   
            for (int iClkPhase = 0; iClkPhase < clkPhaseSteps; iClkPhase++)  {
                for (int iRegAdd = 0; iRegAdd < 2; iRegAdd++) {
                    if(data.shiftTab[iLineNum][iClkPhase][iRegAdd] < 0 && data.shiftTab[iLineNum][iClkPhase][iRegAdd] != -20) {
                        data.shiftTab[iLineNum][iClkPhase][iRegAdd] = (data.shiftTab[iLineNum][iClkPhase][iRegAdd] + 10) * -1;
                    }                        
                }
            }
        }
    }
    public boolean setFoundClkPhaseAndRegAdd () throws RemoteException, ServiceException{
        boolean allFound = true;
        //DeviceItemWord recDelay = (DeviceItemWord) reciver.getDevice().findByName("REC_DELAY");
        for(int iLineNum = 0; iLineNum < linesCnt; iLineNum++){
            if(activatedLines.contains(iLineNum) == false)
                continue;
            if(data.foundClkInv[iLineNum] < 0) {
                DefOut.out.println("setSynchroParams: synchro params for line "+ iLineNum + " not found, their are not changed");
                allFound = false;
            }
        }
        if(clkPhaseSteps == 4)
            reciver.setMuxClkPhase(data.foundClkInv, linesCnt * linkNum);
        else if(clkPhaseSteps == 2)
            reciver.setMuxClkInv(data.foundClkInv, linesCnt * linkNum);

        reciver.setMuxRegAdd(data.foundRegAdd, linesCnt * linkNum);

        return allFound;
    }

    public void setSynchroParams () throws RemoteException, ServiceException{        
        setFoundClkPhaseAndRegAdd();

        //recDelay.write (linkNum, bitsPerLine - getFoundShift());  //TODO check
        //int muxDelay = bitsPerLine - getFoundShift();        
        int muxDelay = getFoundShift();      

        reciver.setRecMuxDelayAndDataDelay(muxDelay, linkNum);                 
    }

    public void setSynchroParams(int iLineNum, int iClkInv, int iRegAdd) throws RemoteException, ServiceException {
        if(iLineNum >= linesCnt)
            return;
        if(data.shiftTab[iLineNum][iClkInv][iRegAdd] > 0) {
            setRecMuxClkInv(iLineNum, iClkInv);
            setRecMuxRegAdd(iLineNum, iRegAdd);

            //setRecMuxDelay(bitsPerLine - data.shiftTab[iLineNum][iClkInv][iRegAdd]);
            setRecMuxDelay(data.shiftTab[iLineNum][iClkInv][iRegAdd]);
        }
    }

    /**
     * @param iLineNum
     * @param iClkInv
     * @param iRegAdd
     * @throws RemoteException, ServiceException
     * sets the dalay according to the data.shiftTab
     */
    public void setRecMuxDalay(int iLineNum, int iClkInv, int iRegAdd) throws RemoteException, ServiceException {
        if(iLineNum >= linesCnt)
            return;
        if(data.shiftTab[iLineNum][iClkInv][iRegAdd] > 0) {
            setRecMuxDelay(data.shiftTab[iLineNum][iClkInv][iRegAdd]);
        }
    }

    /*    *//**
     * @param iClkInv
     * @param iRegAdd
     * @throws RemoteException, ServiceException
     * sets the iClkInv and iRegAdd for all lines,
     * the MuxDelay and DataDelay is not set
     *//*
    public void setSynchroParams(int iClkInv, int iRegAdd) throws RemoteException, ServiceException {
        for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {    
            setRecMuxClkInv(iLineNum, iClkInv);
            setRecMuxRegAdd(iLineNum, iRegAdd);                                       
        } 
    }*/



    void resetRecTestOrData() throws RemoteException, ServiceException {
        ((DeviceItemWord)reciver.getDevice().getItem("REC_TEST_OR_DATA")).read(linkNum);
        //((DeviceItemWord)reciver.getDevice().findByName("REC_TEST_OR_DATA")).read(linkNum);
    }

    void updateShiftTab(int iLineNum, int iClkInv, int iRegAdd) throws RemoteException, ServiceException {
        if(iLineNum >= linesCnt)
            return;
        if(data.shiftTab[iLineNum][iClkInv][iRegAdd] > 0) {
            long recTestOrData = ((DeviceItemWord)reciver.getDevice().getItem("REC_TEST_OR_DATA")).read(linkNum);
            if(((1<<iLineNum) & recTestOrData) != 0) 
                if(data.shiftTab[iLineNum][iClkInv][iRegAdd] >= 0)
                    data.shiftTab[iLineNum][iClkInv][iRegAdd] = data.shiftTab[iLineNum][iClkInv][iRegAdd] * (-1) - 10; 
        }
    }

/*    public void fillDelayTab_StaticTestData(int clkPhase, int regAdd, int muxDelay,
            String testData) throws RemoteException, ServiceException {
        Binary recTestData = ((DeviceItemWord)reciver.getDevice().findByName("REC_TEST_DATA")).readBinary(linkNum);
        Binary testDataBynary = new Binary(testData.substring(0,(recTestData.getBitNum())/4));
        for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {         
            if(testDataBynary.getBits(iLineNum * bitsPerLine, bitsPerLine) == 0)
                continue;
            if(recTestData.getBits(iLineNum * bitsPerLine, bitsPerLine) == testDataBynary.getBits(iLineNum * bitsPerLine, bitsPerLine)) {
                if(data.shiftTab[iLineNum][clkPhase][regAdd] == -100) { //first good value
                    data.shiftTab[iLineNum][clkPhase][regAdd] = muxDelay;
                }
                else if(data.shiftTab[iLineNum][clkPhase][regAdd] >= 0) {
                    if(data.shiftTab[iLineNum][clkPhase][regAdd] != muxDelay) {
                        System.out.println(name + " line " + iLineNum + " clkPhase " + clkPhase + " regAdd " + regAdd +" muxDelay " + muxDelay + 
                            " the data.shiftTab has alreadey difernt positive value = "  + data.shiftTab[iLineNum][clkPhase][regAdd] );
                        data.shiftTab[iLineNum][clkPhase][regAdd] = -20;
                    }
                }              
            }
            else if (data.shiftTab[iLineNum][clkPhase][regAdd] == muxDelay || data.shiftTab[iLineNum][clkPhase][regAdd] == -100) {
                data.shiftTab[iLineNum][clkPhase][regAdd] = -20;
            }
        }  
        
    }*/
    
    public void fillDelayTab_StaticTestData(int clkPhase, int regAdd, int muxDelay,
            String testData) throws RemoteException, ServiceException {
        Binary recTestData = ((DeviceItemWord)reciver.getDevice().getItem("REC_TEST_DATA")).readBinary(linkNum);
        Binary testDataBynary = new Binary(testData.substring(0,(recTestData.getBitNum())/4));
        for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {         
            if(testDataBynary.getBits(iLineNum * bitsPerLine, bitsPerLine) == 0) {                       
                continue;
            }
            if(recTestData.getBits(iLineNum * bitsPerLine, bitsPerLine) == testDataBynary.getBits(iLineNum * bitsPerLine, bitsPerLine)) {
                        
            }
            else {
            	data.delayIsOkTab[iLineNum][clkPhase][regAdd][muxDelay] = false; 
            }
        }  
        
    }
    
    public void convert_delayIsOkTab_to_shiftTab() {
        for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {
            for(int clkPhase = 0; clkPhase < LVDSLink.clkPhaseSteps; clkPhase++) {
                for (int regAdd=0; regAdd<2; regAdd++) {
                    for (int delay = 0; delay < LVDSLinksSynchronizer.getMaxMuxDelay(); delay++) {
                        if(data.delayIsOkTab[iLineNum][clkPhase][regAdd][delay] == true) {
                            if(data.shiftTab[iLineNum][clkPhase][regAdd] >= 0) {
                                System.out.println(name + " line " + iLineNum + " clkPhase " + clkPhase + " regAdd " + regAdd +" muxDelay " + delay + 
                                        " the data.shiftTab has alreadey difernt positive value = "  + data.shiftTab[iLineNum][clkPhase][regAdd] );
                                    data.shiftTab[iLineNum][clkPhase][regAdd] = -20;
                                    break;
                            }
                            else {
                                data.shiftTab[iLineNum][clkPhase][regAdd] = delay;
                            }
                        }
                    }
                }
            }
        }
    }
    
    void fillShiftTab(int clkInv, int muxDelay) throws RemoteException, ServiceException, InterruptedException {               
        long recTestOrData = ((DeviceItemWord)reciver.getDevice().getItem("REC_TEST_OR_DATA")).read(linkNum);
        for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) { 
            if(((1<<iLineNum) & recTestOrData) == 0) {
                if(data.shiftTab[iLineNum][clkInv][0] >= 0) {
                    //throw new RuntimeException(description + " linkNum " + linkNum + " line " + iLineNum + "the data.shiftTab has alreadey positive value");
                    System.out.println(name + " line " + iLineNum + " the data.shiftTab has alreadey positive value = "  + data.shiftTab[iLineNum][clkInv][0] );
                }
                data.shiftTab[iLineNum][clkInv][0] = muxDelay;
                data.shiftTab[iLineNum][clkInv][1] = (muxDelay - 1 + 8) % 8;
            }
        }       
    }

    void fillShiftTabForLinesGroup(int clkInv, int muxDelay, int sLine) throws RemoteException, ServiceException, InterruptedException {  
        if(sLine > linkDef.getLinesInSplot())
            return;

        long recTestOrData = ((DeviceItemWord)reciver.getDevice().getItem("REC_TEST_OR_DATA")).read(linkNum);
        for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {
            if(linkDef.isLineActive(sLine, iLineNum)) {
                if(((1<<iLineNum) & recTestOrData) == 0) {
                    if(data.shiftTab[iLineNum][clkInv][0] >= 0) {
                        //throw new RuntimeException(description + " linkNum " + linkNum + " line " + iLineNum + "the data.shiftTab has alreadey positive value");
                        System.out.println(name + " line " + iLineNum + " the data.shiftTab has alreadey positive value = "  + data.shiftTab[iLineNum][clkInv][0] );
                    }
                    data.shiftTab[iLineNum][clkInv][0] = muxDelay;
                    data.shiftTab[iLineNum][clkInv][1] = (muxDelay - 1 + 8) % 8;
                }
            }
        }       
    }

    void fillShiftTabForLinesGroup(int clkInv, int regAdd, int muxDelay, int sLine) throws RemoteException, ServiceException, InterruptedException {  
        if(sLine > linkDef.getLinesInSplot())
            return;

        long recTestOrData = ((DeviceItemWord)reciver.getDevice().getItem("REC_TEST_OR_DATA")).read(linkNum);
        for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {
            if(linkDef.isLineActive(sLine, iLineNum)) {
                if(((1<<iLineNum) & recTestOrData) == 0) {
                    if(data.shiftTab[iLineNum][clkInv][regAdd] >= 0) {
                        //throw new RuntimeException(description + " linkNum " + linkNum + " line " + iLineNum + "the data.shiftTab has alreadey positive value");
                        System.out.println(name + " line " + iLineNum + " the data.shiftTab has alreadey positive value = "  + data.shiftTab[iLineNum][clkInv][regAdd] );
                    }
                    data.shiftTab[iLineNum][clkInv][regAdd] = muxDelay;
                    //data.shiftTab[iLineNum][clkInv][1] = (muxDelay - 1 + 8) % 8;
                }
            }
        }       
    }

    void fillShiftTab(int clkInv, int regAdd, int muxDelay) throws RemoteException, ServiceException, InterruptedException {               
        long recTestOrData = ((DeviceItemWord)reciver.getDevice().getItem("REC_TEST_OR_DATA")).read(linkNum);
        for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) { 
            if(((1<<iLineNum) & recTestOrData) == 0) {
                if(data.shiftTab[iLineNum][clkInv][regAdd] >= 0) {
                    //throw new RuntimeException(description + " linkNum " + linkNum + " line " + iLineNum + "the data.shiftTab has alreadey positive value");
                    System.out.println(name + " line " + iLineNum + " the data.shiftTab has alreadey positive value = "  + data.shiftTab[iLineNum][clkInv][regAdd] );
                }
                //else
                data.shiftTab[iLineNum][clkInv][regAdd] = muxDelay;                
            }
        }       
    }

    /**
     * assuming, that parameters were set and random data are running 
     * reads ONCE the REC_TEST_OR_DATA, if shows error, setSynchroParams() are called
     * @throws RemoteException, ServiceException
     * @throws InterruptedException
     */
    boolean finalParametrsTune() throws RemoteException, ServiceException, InterruptedException {               
        long recTestOrData = ((DeviceItemWord)reciver.getDevice().getItem("REC_TEST_OR_DATA")).read(linkNum);
        boolean wasError = false;

        for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) { 
            if(data.foundClkInv[iLineNum] <= -1) {
                DefOut.out.println(name + " line " + iLineNum + " synch params were not found, could not run findShift(), breaking");
                return true;
            }
            if(((1<<iLineNum) & recTestOrData) != 0) {
                DefOut.out.println(name + " found errors in line " + iLineNum + " running findShift() again"); 
                data.shiftTab[iLineNum][data.foundClkInv[iLineNum]][data.foundRegAdd[iLineNum]] =  data.shiftTab[iLineNum][data.foundClkInv[iLineNum]][data.foundRegAdd[iLineNum]] * (-1) - 10;
                if(data.shiftTab[iLineNum][data.foundClkInv[iLineNum]][(data.foundRegAdd[iLineNum] + 1)%2] >= 0)

                    data.shiftTab[iLineNum][data.foundClkInv[iLineNum]][(data.foundRegAdd[iLineNum] + 1)%2] = data.shiftTab[iLineNum][data.foundClkInv[iLineNum]][(data.foundRegAdd[iLineNum] + 1)%2] * (-1) - 10;
                wasError = true;;
            }
        }

        if(wasError) {
            findShift();
            setSynchroParams();
        }
        return wasError;
    }

    /**
     * @param sLine
     * @return true if the procedure should be repeated
     * @throws RemoteException
     * @throws ServiceException
     * @throws InterruptedException
     */
    boolean finalParametrsTune(int sLine, boolean sharpen) throws RemoteException, ServiceException, InterruptedException {               
        long recTestOrData = ((DeviceItemWord)reciver.getDevice().getItem("REC_TEST_OR_DATA")).read(linkNum);
        boolean wasError = false;

        for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {
            if(linkDef.isLineActive(sLine % linkDef.getLinesInSplot(), iLineNum)) {
                if(data.foundClkInv[iLineNum] <= -1) {
                    DefOut.out.println(name + " line " + iLineNum + " synch params were not found, could not run findShift(), breaking");
                    return false;
                }
                if(((1<<iLineNum) & recTestOrData) != 0) {
                    DefOut.out.println(name + " found errors in line " + iLineNum); 
                    data.shiftTab[iLineNum][data.foundClkInv[iLineNum]][data.foundRegAdd[iLineNum]] = data.shiftTab[iLineNum][data.foundClkInv[iLineNum]][data.foundRegAdd[iLineNum]] * (-1) - 10; 
                    if(sharpen) {
                        if(data.shiftTab[iLineNum][data.foundClkInv[iLineNum]][(data.foundRegAdd[iLineNum] + 1)%2] >= 0) {
                            data.shiftTab[iLineNum][data.foundClkInv[iLineNum]][(data.foundRegAdd[iLineNum] + 1)%2] = data.shiftTab[iLineNum][data.foundClkInv[iLineNum]][(data.foundRegAdd[iLineNum] + 1)%2] * (-1) -10;
                        } 
                    }
                    wasError = true;;
                }
            }
        }

        if(wasError) {        
            DefOut.out.println(name + " running findShift() again"); 
            boolean wasSharpen = false;
            if(sharpen)
                wasSharpen = sharpenShifTab();
            if(findShift()) {
                setSynchroParams();
                ((DeviceItemWord)reciver.getDevice().getItem("REC_TEST_OR_DATA")).read(linkNum);
                return true;
            }
            else if(wasSharpen) {
                unSharpenShifTab(); 
                if(findShift()) {
                    setSynchroParams();
                    ((DeviceItemWord)reciver.getDevice().getItem("REC_TEST_OR_DATA")).read(linkNum);
                    return true;
                }
            }
            DefOut.out.println(getName() + " synch params not found!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            DefOut.out.println("");
            System.out.println(getName() + " synch params not found!!!");
        }       

        return false;
    }

    /**
     * @param printMode - if 0 prints anly if non zero errors 
     * @throws RemoteException, ServiceException
     * reads ONCE the REC_TEST_DATA and REC_TEST_OR_DATA
     */
    public void readRecTestOrData(boolean printMode) throws RemoteException, ServiceException {
        long recTestOrData_ = ((DeviceItemWord)reciver.getDevice().getItem("REC_TEST_OR_DATA")).read(linkNum);
        long recTestData_ = ((DeviceItemWord)reciver.getDevice().getItem("REC_TEST_DATA")).read(linkNum);                        

        if(printMode || recTestOrData_ != 0)
            DefOut.out.println(name + " REC_TEST_DATA " + recTestData_ + " REC_TEST_OR_DATA " + recTestOrData_ );        
    }


    public int getLinesCnt() {
        return linesCnt;
    }


    public int getLinkNum() {
        return linkNum;
    }   

    String printParams() {
        StringBuilder strb = new StringBuilder();
        strb.append(name); 
        strb.append("\n");
        String foundClkInvStr = "foundClkInv ";
        String foundRegAddStr = "foundRegAdd ";
        String shiftTabStr = "founfShift  ";
        boolean notFound = false;
        for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {
            foundClkInvStr =  foundClkInvStr  + "[" + data.foundClkInv[iLineNum] +"] ";            
            foundRegAddStr =  foundRegAddStr  + "[" + data.foundRegAdd[iLineNum] +"] ";
            if(data.foundClkInv[iLineNum] != -1)
                shiftTabStr = shiftTabStr + "[" + data.shiftTab[iLineNum][data.foundClkInv[iLineNum]][data.foundRegAdd[iLineNum]]+"] ";
            else {
                shiftTabStr = shiftTabStr + "[-1]";
                notFound = true;
            }
        }   
        strb.append(foundClkInvStr);
        strb.append("\n");
        strb.append(foundRegAddStr);
        strb.append("\n");
        strb.append(shiftTabStr);
        strb.append("\n");
        if(notFound) {
            strb.append("Synchro params not found !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            System.out.println(name + " Synchro params not found !!!!!!!!!!!!!!!!");
            strb.append("\n");
        }
        return strb.toString();
    }

    /**
     * @param type - 0 walking 0's, 1 - walking 1's
     * worls only if bitsPerLine = 8
     * @throws ServiceException 
     * @throws RemoteException 
     */
    void checkInterconnections(int type) throws RemoteException, ServiceException {
        /*    	if(bitsPerLine != 8) {
    		throw new IllegalArgumentException("checkInterconnections works only if bitsPerLine == 8");
    	}*/

        //byte[] testDataBytes = new byte[linesCnt]; //reciver.getDevice().findByName("REC_TEST_DATA").getInfo().getWidth();

        Binary  sendTestData = ((DeviceItemWord)transmitter.getDevice().getItem("SEND_TEST_DATA")).readBinary(0);
        byte[] testDataBytes = sendTestData.getBytes();
        for(int i = 0; i < testDataBytes.length; i++) {
            if(type == 0) {
                testDataBytes[i] = (byte)0xff;
            }
            else if(type == 1) {
                testDataBytes[i] = (byte)0x0;
            }
        }

        for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {    		
            for(int iBit = 0; iBit < bitsPerLine; iBit++) {
                if(type == 0) {
                    //testDataBytes[iLineNum] = 0;
                    Binary.setBit(testDataBytes, iLineNum * bitsPerLine + iBit, 0);
                }
                else {
                    //testDataBytes[iLineNum] = (byte)0xff;
                    Binary.setBit(testDataBytes, iLineNum * bitsPerLine + iBit, 1);
                }

            }

            Binary testData =  new Binary(testDataBytes);

            ((DeviceItemWord)transmitter.getDevice().getItem("SEND_TEST_DATA")).writeBinary(linkNum % transmitter.getDevice().getItem("SEND_TEST_DATA").getNumber(), testData);
            //((DeviceItemWord)transmitter.getDevice().findByName("SEND_TEST_DATA")).writeBinary(linkNum, testData);

            Binary  recTestData = ((DeviceItemWord)reciver.getDevice().getItem("REC_TEST_DATA")).readBinary(linkNum);

            if(recTestData.equals(testData) == false) {
                DefOut.out.println(name + " line " + iLineNum + " sendTestData = " + testData + " : recTestData " + recTestData);
                System.out.println(name + " line " + iLineNum + " sendTestData = " + testData + " : recTestData " + recTestData);
            }

            for(int iBit = 0; iBit < bitsPerLine; iBit++) {
                if(type == 0) {
                    //testDataBytes[iLineNum] = (byte)0xff;
                    Binary.setBit(testDataBytes, iLineNum * bitsPerLine + iBit, 1);
                }
                else {
                    //testDataBytes[iLineNum] = 0;
                    Binary.setBit(testDataBytes, iLineNum * bitsPerLine + iBit, 0);
                }

            }
        }
    }

    public LVDSTransmDevice getTransmitter() {
        return transmitter;
    }


    public LVDSTransmDevice getReciver() {
        return reciver;
    }


    public String getName() {
        return name;
    }

    public LVDSLinkDef getLinkDef() {
        return linkDef;
    }

    public Set<Integer> getActivatedLines() {
        return activatedLines;
    }

    public void addToActivatedLines(int sLine) {
        for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {
            if(linkDef.isLineActive(sLine, iLineNum))
                activatedLines.add(iLineNum);
        }
    }

}
