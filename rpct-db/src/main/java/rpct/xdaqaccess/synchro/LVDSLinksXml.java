package rpct.xdaqaccess.synchro;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Transient;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.rpc.ServiceException;

import rpct.datastream.TestOptionsXml;

@XmlRootElement(name="LVDSLinks")
public class LVDSLinksXml {
    private List<LVDSLinkXml> links = new ArrayList<LVDSLinkXml>();
    private String date;
    
    public LVDSLinksXml() {                
    }       
   
    public LVDSLinksXml(String date) {
        super();
        this.date = date;
    }

    @XmlElement(name="link")
    public List<LVDSLinkXml> getLink() {
        return links;
    }

    public void setLink(List<LVDSLinkXml> links) {
        this.links = links;
    }

    @XmlAttribute(name="date")
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public static void main(String[] args) {
        final File xmlFile = new File("LVDSLinks.xml");
        try {
            JAXBContext context1 = JAXBContext.newInstance(LVDSLinksXml.class);
            
            System.out.println("writing to the file " + xmlFile);
            Marshaller m = context1.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); 
            
            LVDSLinksXml linksXml = new LVDSLinksXml();
            LVDSLinkXml linkXml =new LVDSLinkXml(3, "dummy");
            
            linksXml.getLink().add(linkXml);
                                   
            m.marshal(linksXml, new FileOutputStream(xmlFile));
                      
            Unmarshaller um = context1.createUnmarshaller();
            LVDSLinksXml linksXml1 = (LVDSLinksXml) um.unmarshal(xmlFile);

        } catch (JAXBException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        };


    }

}
