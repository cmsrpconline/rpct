package rpct.xdaqaccess.synchro;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlValue;


public class LVDSLinkXml {
    static public class TableXml<E> {
        static public class ValueXml<E> {            
            int index;
            E value;
            
            public ValueXml() {
                
            }
            
            public ValueXml(int index, E value) {
                super();
                this.index = index;
                this.value = value;
            }
            
            @XmlAttribute(name="i")
            public int getIndex() {
                return index;
            }
            public void setIndex(int index) {
                this.index = index;
            }
                       
            public E getValue() {
                return value;
            }
            public void setValue(E value) {
                this.value = value;
            }                       
        }
        
        String name;
        private List<ValueXml<E> > values;
        private int size;
        
        public TableXml(E[] tab) {
            this.values =  new ArrayList<ValueXml<E> >();
            for(int i = 0; i < tab.length; i++)
                values.add(new ValueXml<E>(i, tab[i]) );

        }
        
        public TableXml() {
            
        }

        @XmlElement(name="val")
        public List<ValueXml<E> > getValues() {
            return values;
        }

        public void setValues(List<ValueXml<E> >  values) {
            this.values = values;
        }   

/*        @XmlTransient
        public void setValues(E[] tab) {
            for(int i = 0; i < tab.length; i++)
                values[i] = new ValueXml<E>(i, tab[i]);
        } */        
    }
            
    private int linesCnt;
    
    private String name;
    
    TableXml<Integer> foundClkInv;
    TableXml<Integer> foundRegAdd;
    private int[][][] shiftTab; //shiftTab[iLineNum][clkInv][regAdd]
    private int foundShift = -1;
    
    public LVDSLinkXml() {
        Integer[] tab = new Integer[linesCnt];
        for(int i = 0; i < tab.length; i++) {
            tab[i] = 0;
        }
        foundClkInv = new TableXml<Integer>(new Integer[linesCnt]) ;
        foundRegAdd = new TableXml<Integer>(new Integer[linesCnt]);
    }

    public LVDSLinkXml(int linesCnt, String name) {
        super();
        this.linesCnt = linesCnt;
        this.name = name;
        
        shiftTab = new int[linesCnt][LVDSLink.clkPhaseSteps][2];
        for (int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {
            shiftTab[iLineNum] = new int[LVDSLink.clkPhaseSteps][2];     
            for(int iclkPhase = 0; iclkPhase < LVDSLink.clkPhaseSteps; iclkPhase++){
                shiftTab[iLineNum][iclkPhase] = new int[2];  
                for(int iRegAdd = 0; iRegAdd < 2; iRegAdd++) {
                    shiftTab[iLineNum][iclkPhase][iRegAdd] = -10;
                }
            }
        }
 
        Integer[] tab = new Integer[linesCnt];
        for(int i = 0; i < tab.length; i++) {
            tab[i] = new Integer(i);
        }
        foundClkInv = new TableXml<Integer>(tab) ;
        foundRegAdd = new TableXml<Integer>(tab);
    }

    @XmlAttribute(name="name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement
    TableXml<Integer> getFoundClkInv() {
        return foundClkInv;
    }

    public void setFoundClkInv(TableXml<Integer>foundClkInv) {
        this.foundClkInv = foundClkInv;
    }

    @XmlElement
    TableXml<Integer> getFoundRegAdd() {
        return foundRegAdd;
    }

    public void setFoundRegAdd(TableXml<Integer> foundRegAdd) {
        this.foundRegAdd = foundRegAdd;
    }

    @XmlElement
    public int[][][] getShiftTab() {
        return shiftTab;
    }

    public void setShiftTab(int[][][] shiftTab) {
        this.shiftTab = shiftTab;
    }

    @XmlElement
    public int getFoundShift() {
        return foundShift;
    }

    public void setFoundShift(int foundShift) {
        this.foundShift = foundShift;
    }       
}
