package rpct.xdaqaccess.synchro;

import java.io.PrintStream;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.xml.rpc.ServiceException;

import rpct.datastream.HardwareMuon;
import rpct.db.domain.equipment.ChipType;
import rpct.synch.LVDSLinkDef;
import rpct.synch.PulserGeneratorForSynchronization;
import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.DeviceItem;
import rpct.xdaqaccess.DeviceItemArea;
import rpct.xdaqaccess.DeviceItemWord;


public class LVDSTransmDevice {
    protected Device device;
    protected int inLVDSlinksCnt; //jesli zostanie zwikszone na long, to  long mask = 0x00000000ffffffff; powinno zostac zwiksone na long long
    private int outLVDSlinksCnt;

    protected int inLinesInLinkCnt;
    int outLinesInLinkCnt;

    private long inputMask; 
    
    private List<LVDSLink> outLinks = new ArrayList<LVDSLink>();
    
    LVDSLinkDef inLinkDef;
    LVDSLinkDef outLinkDef;
    
    public LVDSTransmDevice(Device device) {
        //TODO - nie wszytki czipy sa jednoczenie senderem i reciverem, ale ten sposobrozrozniania moze bys sliski, 
        this.device = device;
        DeviceItem reMuxClkInv = device.getItem("REC_MUX_CLK_INV");
        if(reMuxClkInv != null) {        	      
            inLVDSlinksCnt = device.getItem("REC_MUX_DELAY").getDesc().getNumber();
            //inLinesInLinkCnt = reMuxClkInv.getInfo().getWidth()/inLVDSlinksCnt; 
            inLinesInLinkCnt = device.getItem("REC_TEST_DATA").getDesc().getWidth()/8;
        }
        DeviceItem sendTestEna = device.getItem("SEND_TEST_ENA");
        if(sendTestEna != null) {
            outLVDSlinksCnt = sendTestEna.getDesc().getWidth();
            DeviceItem sendTestData = device.getItem("SEND_TEST_DATA");
            int dataBitsCnt = 0; //including check bits
            int bitsPerLine = 8;
            if(device.getType().equals(ChipType.OPTO.getHwType()) || device.getType().equals(ChipType.TTUOPTO.getHwType()))
                dataBitsCnt = ((DeviceItemArea)getDevice().getItem("MEM_PULSE")).getDesc().getWidth();
            else if(device.getType().equals(ChipType.PAC.getHwType()))
                dataBitsCnt = HardwareMuon.PacOutMuon.getMuonBitsCnt() * 12;
            else if(device.getType().equals(ChipType.GBSORT.getHwType()))
                dataBitsCnt = HardwareMuon.TbGbSortOutMuon.getMuonBitsCnt() * 4;
            else if(device.getType().equals(ChipType.TCSORT.getHwType())) {
                dataBitsCnt = HardwareMuon.TcSortOutMuon.getMuonBitsCnt() * 4;
                bitsPerLine = 2;
            }
            
            outLinesInLinkCnt = sendTestData.getDesc().getWidth()/bitsPerLine;

            int checkBitsCnt = sendTestData.getDesc().getWidth() - dataBitsCnt/getOutLVDSlinksCnt();

            /*lvdsTransmitterDef = LvdsTransmDeviceDef.getTransmittersDefs().get(ChipType.fromHwType(getDevice().getType()));
            if(lvdsTransmitterDef == null) {
                lvdsTransmitterDef = new LvdsTransmDeviceDef(outLVDSlinksCnt, outLinesInLinkCnt, dataBitsCnt/outLVDSlinksCnt, checkBitsCnt);
                LvdsTransmDeviceDef.getTransmittersDefs().put(ChipType.fromHwType(getDevice().getType()), lvdsTransmitterDef);
            }*/
                        
            outLinkDef = LVDSLinkDef.getLvdesLinksDefs().get(ChipType.fromHwType(getDevice().getType())); 
            if(outLinkDef == null) {
                outLinkDef = new LVDSLinkDef(outLinesInLinkCnt, dataBitsCnt/outLVDSlinksCnt, checkBitsCnt, bitsPerLine);   
                LVDSLinkDef.getLvdesLinksDefs().put(ChipType.fromHwType(getDevice().getType()), outLinkDef);
            }
            
            PulserGeneratorForSynchronization pulserGenerator =  PulserGeneratorForSynchronization.getGenerators().get(ChipType.fromHwType(getDevice().getType()));
            if(pulserGenerator == null) {
                pulserGenerator = new PulserGeneratorForSynchronization(outLVDSlinksCnt, outLinkDef);
                PulserGeneratorForSynchronization.getGenerators().put(ChipType.fromHwType(getDevice().getType()), pulserGenerator);
            }
        }            
    }

    public Device getDevice() {
        return device;
    };

    /*    public void resetLVDSLinkRec() throws XdaqException, InterruptedException;

    public void setTrndEna() throws XdaqException, InterruptedException;*/
    public void resetLVDSLinkTrans() throws RemoteException, ServiceException, InterruptedException {
        DeviceItem deviceItem;
        deviceItem = device.getItem("SEND_CHECK_ENA");
        if(deviceItem != null)
            deviceItem.write(0x0);   

        deviceItem = device.getItem("SEND_CHECK_DATA_ENA");
        if(deviceItem != null)
            deviceItem.write(0x0);
        
        deviceItem = device.getItem("SEND_TEST_ENA");
        if(deviceItem != null)
            deviceItem.write(0x0);
        
        deviceItem = device.getItem("SEND_TEST_RND_ENA");
        if(deviceItem != null)
            deviceItem.write(0x0);       
    }

    public void resetLVDSLinkRec() throws RemoteException, ServiceException, InterruptedException {
        DeviceItem deviceItem;
        deviceItem = device.getItem("REC_CHECK_ENA");
        if(deviceItem != null)
            deviceItem.write(0x0);
        deviceItem = device.getItem("REC_CHECK_DATA_ENA");
        if(deviceItem != null)
            deviceItem.write(0x0);
        
        deviceItem = device.getItem("REC_TEST_ENA");
        if(deviceItem != null)
            deviceItem.write(0x0);

        device.getItem("REC_TEST_RND_ENA").write(0);

        device.getItem("REC_MUX_CLK_INV").write(0);       

        device.getItem("REC_MUX_REG_ADD").write(0);        

        DeviceItemWord recMuxDelay = (DeviceItemWord) device.getItem("REC_MUX_DELAY");
        DeviceItemWord recDataDelay = (DeviceItemWord) device.getItem("REC_DATA_DELAY");        
        for(int i = 0; i < inLVDSlinksCnt; i++) {
            recMuxDelay.write(i, 0);   
            recDataDelay.write (i, 0 );  
        }        
    }

    void registerInputLink(int linkNum, LVDSLink link) {    	    
        inputMask |= (1<<linkNum);
        inLinkDef = link.getLinkDef();
    }

    void registerOutLink(LVDSLink link) {           
        outLinks.add(link);
    }
    
    public void setNoSendTestEnaa() throws RemoteException, ServiceException {               
        device.getItem("SEND_TEST_ENA").write(0); 
    }
    
    public void setSendTestStaticData(int testData) throws RemoteException, ServiceException {
        long mask = (1 << (outLVDSlinksCnt + 1)) -1;        
        device.getItem("SEND_TEST_ENA").write(mask); 

        DeviceItemWord sendTestData = (DeviceItemWord) device.getItem("SEND_TEST_DATA"); 
        for(int iLinkNum = 0; iLinkNum < outLVDSlinksCnt; iLinkNum++)
            sendTestData.write(iLinkNum, testData);  
        device.getItem("SEND_TEST_RND_ENA").write(0x0);
    }

    public void setSendTestData(String testData) throws RemoteException, ServiceException {       
        DeviceItemWord sendTestData = (DeviceItemWord) device.getItem("SEND_TEST_DATA"); 
        for(int iLinkNum = 0; iLinkNum < outLVDSlinksCnt; iLinkNum++)
            sendTestData.writeBinary(iLinkNum, new Binary(testData, sendTestData.getDesc().getWidth()));
    }


    public void setSendTestData(String testData, int linkNum) throws RemoteException, ServiceException {       
        DeviceItemWord sendTestData = (DeviceItemWord) device.getItem("SEND_TEST_DATA"); 
        sendTestData.writeBinary(linkNum, new Binary(testData,  sendTestData.getDesc().getWidth()));
    }

    public void setRandomTestData() throws RemoteException, ServiceException {
        Random generator = new Random();
        DeviceItemWord sendTestData = (DeviceItemWord) device.getItem("SEND_TEST_DATA");
        byte[] bytes = new  byte[sendTestData.getDesc().getWidth()/8];        
        for(int iLinkNum = 0; iLinkNum < outLVDSlinksCnt; iLinkNum++) {        	        	
            generator.nextBytes(bytes);
            sendTestData.writeBinary(iLinkNum, new Binary(bytes));
        }      
        device.getItem("SEND_TEST_ENA").write(0);
        long mask = (1 << (outLVDSlinksCnt + 1)) -1;        
        device.getItem("SEND_TEST_ENA").write(mask); 
    }

    public void setSendTestStaticData(String testData) throws RemoteException, ServiceException {
        long mask = (1 << (outLVDSlinksCnt + 1)) -1;

        device.getItem("SEND_TEST_ENA").write(mask); 

        DeviceItemWord sendTestData = (DeviceItemWord) device.getItem("SEND_TEST_DATA"); 
        for(int iLinkNum = 0; iLinkNum < outLVDSlinksCnt; iLinkNum++)
            sendTestData.writeBinary(iLinkNum, new Binary(testData.substring(0,sendTestData.getDesc().getWidth()/4)));
      
        if(device.getItem("SEND_TEST_RND_ENA") != null)
        device.getItem("SEND_TEST_RND_ENA").write(0x0);
    }

    public void setSendTestRandomData() throws RemoteException, ServiceException {
        long mask = (1 << (outLVDSlinksCnt + 1)) -1;

        device.getItem("SEND_TEST_ENA").write(mask); 
        device.getItem("SEND_TEST_RND_ENA").write(mask); 

        //DefOut.out.println("setSendTestRandomData");
    }

    public void setSendTestRandomData(boolean on) throws RemoteException, ServiceException {
        long mask;
        if(on)
            mask= (1 << (outLVDSlinksCnt + 1)) -1;
        else 
            mask = 0;

        device.getItem("SEND_TEST_ENA").write(mask); 
        device.getItem("SEND_TEST_RND_ENA").write(mask); 

        //DefOut.out.println("setSendTestRandomData");
    }

    public void setSendCheckDataEna() throws RemoteException, ServiceException {
        long mask = (1 << (outLVDSlinksCnt + 1)) -1;
        if(device.getItem("SEND_CHECK_DATA_ENA") == null)
            return;
        device.getItem("SEND_CHECK_DATA_ENA").write(mask); 
        //DefOut.out.println("setSendTestRandomData");
    }
    /**
     * @throws RemoteException, ServiceException 
     * @throws RemoteException, ServiceException
     * @throws InterruptedException
     * enables static data sending, disables random data sending
     */
    public void setRecTestEna() throws RemoteException, ServiceException {    	
        device.getItem("REC_TEST_ENA").write(inputMask); //0x3FFFF
        if(device.getItem("REC_TEST_RND_ENA") != null)
            device.getItem("REC_TEST_RND_ENA").write(0); 
    }

    public void setNoRecTestEna() throws RemoteException, ServiceException {      
        device.getItem("REC_TEST_ENA").write(0); //0x3FFFF
        if(device.getItem("REC_TEST_RND_ENA") != null)
            device.getItem("REC_TEST_RND_ENA").write(0); 
    }
    
    /**
     * @param linkNum
     * @throws RemoteException, ServiceException
     * enables static data sending, disables random data sending - both for selected link, rest stays unchanched
     */
    public void setRecTestEna(int linkNum) throws RemoteException, ServiceException {
        long one = 1;
        one<<=linkNum;        

        long mask = device.getItem("REC_TEST_ENA").read();
        mask |= one;
        device.getItem("REC_TEST_ENA").write(mask); //0x3FFFF

        mask = device.getItem("REC_TEST_RND_ENA").read();
        mask &= (~one);
        device.getItem("REC_TEST_RND_ENA").write(mask); //<<<<<>        
    }

    /*    public void setRecTestEnaForOneLink(int linkNum) throws RemoteException, ServiceException {
        long mask = 0;
        long one = 1;
        one<<=linkNum;
        mask |= one;
        device.findByName("REC_TEST_ENA").write(mask); //0x3FFFF
        device.findByName("REC_TEST_RND_ENA").write(0); //<<<<<> 
    }*/

    public void setRecTRandomEna() throws RemoteException, ServiceException, InterruptedException {
        device.getItem("REC_TEST_ENA").write(inputMask); //0x3FFFF
        device.getItem("REC_TEST_RND_ENA").write(inputMask); //0x3FFFF
    }

    public void setRecTRandomEna(boolean on) throws RemoteException, ServiceException, InterruptedException {
        if(on) {
            device.getItem("REC_TEST_ENA").write(inputMask); //0x3FFFF
            device.getItem("REC_TEST_RND_ENA").write(inputMask); //0x3FFFF
        }
        else {
            device.getItem("REC_TEST_ENA").write(0); //0x3FFFF
            device.getItem("REC_TEST_RND_ENA").write(0); //0x3FFFF
        }
    }

    public void setRecCheckDataEna() throws RemoteException, ServiceException, InterruptedException {
        if(device.getItem("REC_CHECK_DATA_ENA") == null)
            return;
        device.getItem("REC_CHECK_DATA_ENA").write(inputMask); 
    }

    public void setRecTRandomEna(int linkNum) throws RemoteException, ServiceException, InterruptedException {
        long one = 1;
        one<<=linkNum;

        long mask = device.getItem("REC_TEST_ENA").read();
        mask |= one;
        device.getItem("REC_TEST_ENA").write(mask); //0x3FFFF
        mask = device.getItem("REC_TEST_RND_ENA").read();
        mask |= one;
        device.getItem("REC_TEST_RND_ENA").write(mask); //0x3FFFF
    }

    /*    public void setRecTRandomEnaForOneLink(int linkNum) throws RemoteException, ServiceException, InterruptedException {
        long mask = 0;
        long one = 1;
        one<<=linkNum;
        mask |= one;
        device.findByName("REC_TEST_ENA").write(mask); //0x3FFFF
        device.findByName("REC_TEST_RND_ENA").write(mask); //0x3FFFF
    }*/

    public void resetRecErrorCounter() throws RemoteException, ServiceException {
        device.getItem("REC_ERROR_COUNT").write(0);
    }

    public long readRecErrorCounter(PrintStream out) throws RemoteException, ServiceException {
        long recErrorCnt = device.getItem("REC_ERROR_COUNT").read();

        out.println(device.getBoard().getName() + " " + device.getName() + " REC_ERROR_COUNT " + recErrorCnt);        

        DeviceItemWord recOrData = (DeviceItemWord)device.getItem("REC_TEST_OR_DATA");
        for(int i = 0; i < recOrData.getNumber(); i++) {
            long val = recOrData.read(i);
            if(val != 0)
                out.println(" line " + i + " recOrData " + Long.toHexString(val));
        }
            
        
        return recErrorCnt;
    }

    public long readRecErrorCounter() throws RemoteException, ServiceException {
        return readRecErrorCounter(DefOut.out);
    }
    
    public long readRecErrorCounterAndTestOr(PrintStream out) throws RemoteException, ServiceException {
        long recErrorCnt = device.getItem("REC_ERROR_COUNT").read();

        out.println(device.getBoard().getName() + " " + device.getName() + " REC_ERROR_COUNT " + recErrorCnt);        

        DeviceItemWord recOrData = (DeviceItemWord)device.getItem("REC_TEST_OR_DATA");
        for(int i = 0; i < recOrData.getNumber(); i++) {
            long val = recOrData.read(i);
            if(val != 0)
                out.println(" line " + i + " recOrData " + Long.toHexString(val));
        }
            
        
        return recErrorCnt;
    }

    public int getInLinesInLinkCnt() {
        return inLinesInLinkCnt;
    }

    public void enableTransmissionCheck(boolean enableBCNcheck, boolean enableDataCheck) throws RemoteException, ServiceException { 			
        DeviceItem reg;
        if(enableBCNcheck) {                
            reg = device.getItem("REC_CHECK_DATA_ENA");
            if(reg != null) {
                reg.write(inputMask);
            }

            reg = device.getItem("SEND_CHECK_DATA_ENA");
            if(reg != null)
                reg.write((1<<reg.getDesc().getWidth()) - 1);  
        }
        else {                
            reg = device.getItem("REC_CHECK_DATA_ENA");
            if(reg != null)
                reg.write(0);

            reg = device.getItem("SEND_CHECK_DATA_ENA");
            if(reg != null)
                reg.write(0);
        }

        if(enableDataCheck) {
            reg = device.getItem("REC_CHECK_ENA");
            if(reg != null) {
                reg.write(inputMask); 
            }
            reg = device.getItem("SEND_CHECK_ENA");
            if(reg != null)
                reg.write((1<<reg.getDesc().getWidth()) - 1); 
        }
        else {
            reg = device.getItem("REC_CHECK_ENA");
            if(reg != null)
                reg.write(0);

            reg = device.getItem("SEND_CHECK_ENA");
            if(reg != null)
                reg.write(0);
        }
    }

    public void setMuxRegAdd(int regAdd) throws RemoteException, ServiceException {
        if(regAdd == 0) {
            device.getItem("REC_MUX_REG_ADD").writeBinary(new Binary("0000000000000000000000000000000000000000", inLVDSlinksCnt * inLinesInLinkCnt)); 
        }
        else if(regAdd == 1) {
            device.getItem("REC_MUX_REG_ADD").writeBinary(new Binary("ffffffffffffffffffffffffffffffffffffffff", inLVDSlinksCnt * inLinesInLinkCnt)); 
        }
        else
            throw new IllegalArgumentException("setMuxRegAdd(int regAdd): regAdd != 0 || regAdd != 1");
    }

    public void setMuxClkInv(int clkInv) throws RemoteException, ServiceException {
        if(clkInv == 0) {
            device.getItem("REC_MUX_CLK_INV").writeBinary(new Binary("0000000000000000000000000000000000000000", inLVDSlinksCnt * inLinesInLinkCnt)); 
        }
        else if(clkInv == 1) {
            device.getItem("REC_MUX_CLK_INV").writeBinary(new Binary("ffffffffffffffffffffffffffffffffffffffff", inLVDSlinksCnt * inLinesInLinkCnt)); 
        }
        else
            throw new IllegalArgumentException("setMuxClkInv(int clkInv): clkInv != 0 || clkInv != 1");
    }
    
    public void setMuxClkPhase(int clkPhase) throws RemoteException, ServiceException {
        if(clkPhase/2 == 0) {
            device.getItem("REC_MUX_CLK_INV").writeBinary(new Binary("0000000000000000000000000000000000000000", inLVDSlinksCnt * inLinesInLinkCnt)); 
        }
        else if(clkPhase/2 == 1) {
            device.getItem("REC_MUX_CLK_INV").writeBinary(new Binary("ffffffffffffffffffffffffffffffffffffffff", inLVDSlinksCnt * inLinesInLinkCnt)); 
        }
        if(clkPhase%2 == 0) {
            if(device.getItem("REC_MUX_CLK90") != null)
                device.getItem("REC_MUX_CLK90").writeBinary(new Binary("0000000000000000000000000000000000000000", inLVDSlinksCnt * inLinesInLinkCnt));
        }
        else if(clkPhase%2 == 1) {
            device.getItem("REC_MUX_CLK90").writeBinary(new Binary("ffffffffffffffffffffffffffffffffffffffff", inLVDSlinksCnt * inLinesInLinkCnt)); 
        }
        else
            throw new IllegalArgumentException("REC_MUX_CLK90(int clkPhase): clkPhase not in range 0...3");
    }

    
    /**
     * @param clkPhase
     * @throws RemoteException
     * @throws ServiceException
     * sets the bits in REC_MUX_CLK_INV to clkPhase[i]/2 and REC_MUX_CLK90 to clkPhase[i]%2
     * if the clkPhase[i] < 0, the corresponding bit values in the REC_MUX_CLK_INV and REC_MUX_CLK90 are not changed
     */
    public void setMuxClkPhase(int[] clkPhase, int offset) throws RemoteException, ServiceException {
        byte[] clkInv = device.getItem("REC_MUX_CLK_INV").readBinary().getBytes();
        byte[] clk90 = device.getItem("REC_MUX_CLK90").readBinary().getBytes();

        for(int i = 0; i < clkPhase.length; i++) {  
            if(clkPhase[i] > LVDSLink.clkPhaseSteps) 
                throw new IllegalArgumentException("clkPhase[i] > LVDSLink.clkPhaseSteps");
            
            if(clkPhase[i] >= 0 ) {
                Binary.setBit(clkInv, i + offset, clkPhase[i]/2);
                Binary.setBit(clk90, i + offset, clkPhase[i]%2);
            }
        }
        device.getItem("REC_MUX_CLK_INV").writeBinary(new Binary(clkInv));
        device.getItem("REC_MUX_CLK90").writeBinary(new Binary(clk90));
    }
    
    public void setMuxClkPhase(int clkPhase, int sLine) throws RemoteException, ServiceException {
        boolean hasClk90 = false;
        if(device.getItem("REC_MUX_CLK90") != null)
            hasClk90 = true;
        
        if(sLine > inLinkDef.getLinesInSplot())
            return;
        
        if(clkPhase > LVDSLink.clkPhaseSteps) 
            throw new IllegalArgumentException("clkPhase[i] > LVDSLink.clkPhaseSteps");
        
        byte[] clkInv = device.getItem("REC_MUX_CLK_INV").readBinary().getBytes();
        
        byte[] clk90 = null;
        if(hasClk90)
            clk90 = device.getItem("REC_MUX_CLK90").readBinary().getBytes();
        
        for(int iLink = 0; iLink < inLVDSlinksCnt; iLink++) {
            for(int iLine = 0; iLine < inLinesInLinkCnt; iLine++) {
                if(inLinkDef.isLineActive(sLine, iLine)) {
                    Binary.setBit(clkInv, iLink * inLinesInLinkCnt + iLine, clkPhase/2);
                    if(hasClk90)
                        Binary.setBit(clk90, iLink * inLinesInLinkCnt + iLine, clkPhase%2);
                }
            }
        }
        device.getItem("REC_MUX_CLK_INV").writeBinary(new Binary(clkInv));
        if(hasClk90)
            device.getItem("REC_MUX_CLK90").writeBinary(new Binary(clk90));
    }
    
    
    public void setMuxRegAdd(int regAdd, int sLine) throws RemoteException, ServiceException {
        if(sLine > inLinkDef.getLinesInSplot())
            return;
        
        if(regAdd > 1) 
            throw new IllegalArgumentException("regAdd > 1");
        
        byte[] regAddTab = device.getItem("REC_MUX_REG_ADD").readBinary().getBytes();
        
        for(int iLink = 0; iLink < inLVDSlinksCnt; iLink++) {
            for(int iLine = 0; iLine < inLinesInLinkCnt; iLine++) {
                if(inLinkDef.isLineActive(sLine, iLine)) {
                    Binary.setBit(regAddTab, iLink * inLinesInLinkCnt + iLine, regAdd);                    
                }
            }
        }
        device.getItem("REC_MUX_REG_ADD").writeBinary(new Binary(regAddTab));        
    }
    
    /**
     * @param regAddTab
     * @throws RemoteException
     * @throws ServiceException
     * sets the bits int REC_MUX_REG_ADD to regAddTab[i]
     * if the regAddTab[i] < 0, the corresponding bit values in the REC_MUX_REG_ADD are not changed
     */
    public void setMuxRegAdd(int[] regAddTab, int offset) throws RemoteException, ServiceException {
        byte[] regAdd = device.getItem("REC_MUX_REG_ADD").readBinary().getBytes();        
        for(int i = 0; i < regAddTab.length; i++) {  
            if(regAddTab[i] > 2) 
                throw new IllegalArgumentException("regAddTab[i] > 2");
            if(regAddTab[i] >= 0)
                Binary.setBit(regAdd, i + offset, regAddTab[i]);
        }
        device.getItem("REC_MUX_REG_ADD").writeBinary(new Binary(regAdd));
    }
    
    public void setMuxClkInv(int[] regAddTab, int offset) throws RemoteException, ServiceException {
        byte[] regAdd = device.getItem("REC_MUX_CLK_INV").readBinary().getBytes();        
        for(int i = 0; i < regAddTab.length; i++) {  
            if(regAddTab[i] > 2) 
                throw new IllegalArgumentException("regAddTab[i] > 2");
            if(regAddTab[i] >= 0)
                Binary.setBit(regAdd, i + offset, regAddTab[i]);
        }
        device.getItem("REC_MUX_CLK_INV").writeBinary(new Binary(regAdd));
    }
    
    public void setRecMuxDelayAndDataDelay(int muxDelay) throws RemoteException, ServiceException {	
        int dataDelay;
        if(muxDelay >= 0 && muxDelay <= 2)
            dataDelay = 1;
        else
            dataDelay = 0;

        DeviceItemWord recMuxDealy = (DeviceItemWord) device.getItem("REC_MUX_DELAY");
        DeviceItemWord dataDealy = (DeviceItemWord) device.getItem("REC_DATA_DELAY");
        for(int i = 0; i < inLVDSlinksCnt; i++) {
            recMuxDealy.write(i, muxDelay);
            dataDealy.write(i, dataDelay);
        }
    }
    
    public void setRecMuxDelayAndDataDelay(int muxDelay, int linkNum) throws RemoteException, ServiceException { 
        int dataDelay;
        /*if(device.getType().equals("TB3LDPAC")) { //w pacu nie ma zapsu delay zeby zmniejszyc latencje
            dataDelay = 0;
        }
        else*/ 
        {
            if(muxDelay >= 0 && muxDelay <= 2)
                dataDelay = 1;
            else
                dataDelay = 0;
        }
        DeviceItemWord recMuxDealy = (DeviceItemWord) device.getItem("REC_MUX_DELAY");
        DeviceItemWord dataDealy = (DeviceItemWord) device.getItem("REC_DATA_DELAY");

        recMuxDealy.write(linkNum, muxDelay);
        dataDealy.write(linkNum, dataDelay);
    }

    public void setMuxDelays(int delay) throws RemoteException, ServiceException {	
        DeviceItemWord recMuxDealy = (DeviceItemWord) device.getItem("REC_MUX_DELAY");
        for(int i = 0; i < inLVDSlinksCnt; i++) {
            recMuxDealy.write(i, delay);
        }
    }

    public void resetRecTestOrData() throws RemoteException, ServiceException {
        DeviceItemWord recTestOrData = (DeviceItemWord) device.getItem("REC_TEST_OR_DATA");
        for(int i = 0; i < inLVDSlinksCnt; i++) {
            recTestOrData.read(i);
        }
    }

    public int getOutLVDSlinksCnt() {
        return outLVDSlinksCnt;
    }

    public List<LVDSLink> getOutLinks() {
        return outLinks;
    }

    public LVDSLinkDef getInLinkDef() {
        return inLinkDef;
    }

    public LVDSLinkDef getOutLinkDef() {
        return outLinkDef;
    }
}
