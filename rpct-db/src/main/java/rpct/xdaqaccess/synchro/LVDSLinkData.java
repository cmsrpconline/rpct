package rpct.xdaqaccess.synchro;

public class LVDSLinkData implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7194928406742168570L;
	
	protected int [] foundClkInv;
	protected int [] foundRegAdd;
	protected int[][][] shiftTab; //data.shiftTab[iLineNum][clkInv][regAdd]

	protected boolean[][][][] delayIsOkTab; //data.shiftTab[iLineNum][clkInv][regAdd]
	protected int foundShift = -1;
}