package rpct.xdaqaccess.synchro;

import rpct.datastream.RpctDelays;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbOpto;

public class OptLink {    
	private OptLinkSender sender;
	private HardwareDbOpto opto;
	private int optoInputNum;
	private int tbInputNum;
	private long[] lbOptoDealysMap = new long[RpctDelays.LB_DELAY_MAX]; //opt links delays
	private boolean isok = true;
    private long foundDelay = -1;
	
	public OptLink(OptLinkSender sender, HardwareDbOpto opto, int optoInputNum, int tbInputNum) {
		this.sender = sender;
		this.opto = opto;
		this.optoInputNum = optoInputNum;
		this.tbInputNum = tbInputNum;
		
		//opto.registerUsedOptLink(optoInputNum);
		opto.registerUsedOptLink(this);
		sender.registerUsedOptLink(this);
		for(int i = 0; i < RpctDelays.LB_DELAY_MAX; i++) {
		    lbOptoDealysMap[i] = -1;
		}
	}

	public OptLinkSender getSender() {
		return sender;
	}

	public HardwareDbOpto getOpto() {
		return opto;
	}

	public int getOptoInputNum() {
		return optoInputNum;
	}

    public long[] getLbOptoDealysMap() {
        return lbOptoDealysMap;
    }

    public boolean isOK() {
        return isok;
    }
    
    public void setIsOK(boolean isok) {
        this.isok = isok;
    }
    
    public String toString() {
        return new String(" " + sender.getFullName() + "->" + opto.getFullName() +" optoIn " + optoInputNum + " tbIn " + tbInputNum);
    }

    public void setFoundDelay(long foundDelay) {
        this.foundDelay = foundDelay;        
    }

    public long getFoundDelay() {
        return foundDelay;
    }

    public int getTbInputNum() {
        return tbInputNum;
    }
}
