package rpct.xdaqaccess.synchro;

public class SynchronizationException extends Exception {
    private static final long serialVersionUID = 8498987577274546813L;

    public SynchronizationException() {
    }

    public SynchronizationException(String message) {
        super(message);
    }

    public SynchronizationException(Throwable cause) {
        super(cause);
    }

    public SynchronizationException(String message, Throwable cause) {
        super(message, cause);
    }

}
