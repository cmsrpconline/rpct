package rpct.xdaqaccess;


import rpct.xdaq.XdaqApplicationInfo;
import rpct.xdaq.XdaqException;

public interface TtcAccess {
    XdaqApplicationInfo getXdaqApplicationInfo();
    void sendVmeBGo() throws XdaqException;
}
