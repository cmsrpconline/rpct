package rpct.xdaqaccess;

public enum DeviceItemType {
    PAGE, AREA, WORD, VECT, BITS;
}
