package rpct.xdaqaccess;

public interface Board extends HardwareItem {
    Crate getCrate();
    Device[] getDevices();
    int getPosition();
}
