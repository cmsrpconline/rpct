package rpct.xdaqaccess;

import java.io.IOException;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqApplicationInfo;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.axis.TtcAccessImplAxis;
import rpct.xdaqaccess.diag.BxData;
import rpct.xdaqaccess.diag.Diag;
import rpct.xdaqaccess.diag.DiagAccessDispatcher;
import rpct.xdaqaccess.diag.DiagCtrl;
import rpct.xdaqaccess.diag.DiagnosticReadout;
import rpct.xdaqaccess.diag.DiagnosticReadoutData;
import rpct.xdaqaccess.diag.DiagnosticReadoutEvent;
import rpct.xdaqaccess.diag.LMuxBxData;
import rpct.xdaqaccess.diag.Muon;
import rpct.xdaqaccess.diag.Pulser;
import rpct.xdaqaccess.diag.DiagCtrl.TriggerType;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;

public class Test {

	public static void synCoderAccessTest() throws DataAccessException,
			ServiceException, IOException, InterruptedException, XdaqException {

		HibernateContext context = new SimpleHibernateContextImpl();
		EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
		HardwareDbMapper dbMapper = new HardwareDbMapper(
				new HardwareRegistry(), context);

        String muonsFileName = "/nfshome0/rpcdev/bin/data/testBxDataOpto.xml";
		//FileInputStream fis = new FileInputStream(muonsFileName);
//		int x = fis.available();
//		byte b[] = new byte[x];
//		fis.read(b);
//		String muonsXml = new String(b);

		try {
			List<Pulser> pulsers = new ArrayList<Pulser>();
			List<DiagnosticReadout> readouts = new ArrayList<DiagnosticReadout>();
			TriggerBoard tb = (TriggerBoard) equipmentDAO
					.getBoardByName("TBn3_9");
			pulsers.add((Pulser) dbMapper.getDiag(tb.getOptoForLinkNum(0),
					"PULSER"));
			pulsers.add((Pulser) dbMapper.getDiag(tb.getOptoForLinkNum(5),
					"PULSER"));
			readouts.add((DiagnosticReadout) dbMapper.getDiag(tb.getPac(0),
					"READOUT"));

			List<DiagCtrl> diagCtrls = DiagAccessDispatcher
					.getDiagCtrls(pulsers);
			diagCtrls.addAll(DiagAccessDispatcher.getDiagCtrls(readouts));

			System.out.println("Configuring pulsers");
			/*Binary[] pulserData = new Binary[128];
			for (int i = 0; i < pulserData.length; i++) {
				pulserData[i] = new Binary((i % 8 == 0) ? 0xffffff : 0);
				System.out.println(pulserData[i].toString());
			}
			DiagAccessDispatcher.configure(pulsers, pulserData, BigInteger
					.valueOf(pulserData.length), false, 0);*/
			DiagAccessDispatcher.configure(pulsers, "file://" + muonsFileName,
			BigInteger.valueOf(16), false, 0);

			System.out.println("Configuring readouts");
			DiagAccessDispatcher.configure(readouts, DiagAccessDispatcher.createMask0(3), 0);

			System.out.println("Reseting diag ctrls");
			DiagAccessDispatcher.reset(diagCtrls);

			System.out.println("Configuring diag ctrls");
			DiagAccessDispatcher.configure(diagCtrls, BigInteger
					.valueOf(200000000), TriggerType.PRETRIGGER_0, 0);

/*			
			MonitorManager monitorManager = new MonitorManager(dbMapper.getBoard(tb).getDevices());
			monitorManager.enableTransmissionCheck(true, true);
			monitorManager.resetRecErrorCnt();*/
			
			System.out.println("Starting diag ctrls");
			DiagAccessDispatcher.start(diagCtrls);
			sendVmeBGoTest();
			while (!DiagAccessDispatcher.checkCountingEnded(diagCtrls)) {
				System.out.println("Checking if all have finished");
				Thread.sleep(1000);
			}
			System.out.println("All diagnostics have finished");
			DiagAccessDispatcher.stop(diagCtrls);

			System.out.println("diagAccess.readData");
			DiagnosticReadoutData[] datas = DiagAccessDispatcher
					.readData(readouts);
			for (DiagnosticReadoutData data : datas) {
				System.out.println(data);
				for (DiagnosticReadoutEvent event : data.getEvents()) {
					System.out.println(event);
					for (BxData bxData : event.getBxData()) {
						System.out.println(bxData);
						for (LMuxBxData lmuxBxData : bxData.getLMuxBxData()) {
							System.out.println(lmuxBxData);
						}
						for (Muon muon : bxData.getMuons()) {
							System.out.println(muon);
						}
					}
				}
			}
			System.out.println("diagAccess.readData Done");
			/*
			 * List<LinkBox> linkBoxes =
			 * equipmentDAO.getLinkBoxesByChamberLocation(1, null, 12, null,
			 * BarrelOrEndcap.Barrel); for (LinkBox linkBox : linkBoxes) { for
			 * (rpct.db.domain.equipment.Board board : linkBox.getBoards()) { if
			 * (board instanceof LinkBoard) { LinkBoard linkBoard = (LinkBoard)
			 * board; Chip synCoder = linkBoard.getSynCoder();
			 * System.out.println("synCoder " + synCoder); List<LinkConn>
			 * linkConns = linkBoard.getLinkConns(); for (LinkConn conn :
			 * linkConns) { System.out.println("Opto " + conn.getOpto());
			 * 
			 * System.out.println(conn.getTriggerBoard().getCrate()); } //Pulser
			 * pulser = (Pulser) dbMapper.getDiag(synCoder, "PULSER");
			 * //System.out.println("Pulser " + pulser); } } }
			 */
		} finally {
			context.closeSession();
		}

	}

	public static void xdaqOnlyTest() throws RemoteException, ServiceException {
		HardwareRegistry hr = new HardwareRegistry();

		// hr.registerXdaqApplication(new XdaqApplicationInfo(
		// new URL("http://pccms8.igf.fuw.edu.pl:1972"), "XdaqTestBench", 2));
		/*
		 * List<Crate> crates = hr.getAllCrates(); for (Crate crate : crates) {
		 * System.out.println("Crate " + crate.getName()); Board[] boards =
		 * crate.getBoards(); for (Board board : boards) {
		 * System.out.println("Board " + board.getName()); Device[] devices =
		 * board.getDevices(); for (Device device : devices) {
		 * System.out.println("Device " + device.getName() + " type = " +
		 * device.getType() + " position = " + device.getPosition()); } } }
		 */

		System.out.println("TB3OPTO list:");
		for (HardwareItem item : hr.findByType("TB3OPTO")) {
			System.out.println("Device " + item);

			// Pulser pulser = (Pulser) hr.findDiagById(new
			// DiagIdImplAxis(item.getId(), "PULSER"));
			// System.out.println("Pulser " + pulser);
			for (Diag diag : ((Device) item).getDiags()) {
				System.out.println("Diag: " + diag);
			}
		}

		/*
		 * Device rmb = (Device) HardwareCatalog.findById(1314); DeviceItemArea
		 * memGohDebug = (DeviceItemArea) rmb.findByName("MEM_GOH_DEBUG");
		 * Binary[] values = memGohDebug.readAreaBinary(); for (int i = 0; i <
		 * values.length; i++) { System.out.println(values[i].toString()); }
		 */
	}

	public static void sendVmeBGoTest() throws MalformedURLException,
			XdaqException {
		TtcAccess ttcAccess = new TtcAccessImplAxis(new XdaqApplicationInfo(
				new URL("http://ttc-rpc:1972"), "TTCciControl", 0), null);

		ttcAccess.sendVmeBGo();
		System.out.println("sendVmeBGo Done.");
	}

	public static void main(String[] args) throws ServiceException,
			DataAccessException, IOException, InterruptedException,
			XdaqException {
		synCoderAccessTest();
		//sendVmeBGoTest();
	}
}
