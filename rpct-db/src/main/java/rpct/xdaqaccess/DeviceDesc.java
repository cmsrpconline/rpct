package rpct.xdaqaccess;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DeviceDesc {    

    public class ItemDesc {
        private int id;
        private String name;
        private String description;
        private DeviceItemType type;
        private int width;
        private int number;
        private AccessType accessType;
        private ItemDesc parent;
        public ItemDesc(
                int id,
                String name,
                String description,
                DeviceItemType type,
                int width,
                int number,
                AccessType accessType,
                ItemDesc parent) {
            this.id = id;
            this.name = name;
            this.description = description;
            this.type = type;
            this.width = width;
            this.number = number;
            this.accessType = accessType;
            this.parent = parent;
        }
        public AccessType getAccessType() {
            return accessType;
        }
        public String getDescription() {
            return description;
        }
        public int getId() {
            return id;
        }
        public String getName() {
            return name;
        }
        public int getNumber() {
            return number;
        }
        public DeviceItemType getType() {
            return type;
        }
        public int getWidth() {
            return width;
        }
        public ItemDesc getParent() {
            return parent;
        }
    }
    
    
    
    private String type;
    private List<ItemDesc> items = null;
    private Map<Integer, ItemDesc> itemsById = new HashMap<Integer, ItemDesc>();
    private Map<String, ItemDesc> itemsByName = new HashMap<String, ItemDesc>();
    
    public DeviceDesc(String type) {
        this.type = type;
    }

    public ItemDesc addItem(int id, String name, String description, DeviceItemType type, int width,
            int number, AccessType accessType, int parentId) {
        if (items == null) {
            items = new ArrayList<ItemDesc>();
        }
        
        ItemDesc parent = null;
        if (type == DeviceItemType.BITS) {
            parent = itemsById.get(parentId);
            if (parent == null) {
                throw new IllegalArgumentException("Parent not found, parentId = " + parentId);
            }

            // prepend name with [VECTOR_NAME].
            StringBuilder builder = new StringBuilder(parent.getName());
            builder.append('.').append(name);
            name = builder.toString();
        }
        ItemDesc item = new ItemDesc(id, name, description, type, width, number, accessType, parent);
        itemsById.put(item.getId(), item);
        itemsByName.put(name, item);        
        items.add(item);
        return item;
    }
    

    public String getType() {
        return type;
    }
    
    public List<ItemDesc> getItems() {
        return items == null ?  null : Collections.unmodifiableList(items);
    }
    
    public ItemDesc getItem(int id) {
        return itemsById.get(id);
    }
    
    public ItemDesc getItem(String name) {
        return itemsByName.get(name.toUpperCase());
    }
}
