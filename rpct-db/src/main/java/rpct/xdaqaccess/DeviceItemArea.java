package rpct.xdaqaccess;


import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import rpct.xdaq.axis.Binary;

public interface DeviceItemArea extends DeviceItem {

    public Binary[] readAreaBinary() throws RemoteException, ServiceException;
    public Binary[] readAreaBinary(int startPos, int count) throws RemoteException, ServiceException;

    public void writeAreaBinary(Binary[] value) throws RemoteException, ServiceException;
    public void writeAreaBinary(int startPos, Binary[] value) throws RemoteException, ServiceException;
}
