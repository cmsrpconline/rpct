/**
 * Created on Oct 28, 2009
 *
 * @author Karol Bunkowski
 * @version $Id$
 */
package rpct.xdaqaccess;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import rpct.datastream.ReadoutItemId.LbDataSource;

/**
 * @author Karol Bunkowski
 *
 */
public interface MonitorableStatusInfo {
    public enum WarnigLevel {
        ok(0),
        warning(1),
        error(2);
        
        private final int intVal;

        private WarnigLevel(int intVal) {
            this.intVal = intVal;
        }

        public int getIntVal() {
            return intVal;
        }
        
        public static WarnigLevel fromInt(int intVal) {
            for(WarnigLevel value : WarnigLevel.values()) {
                if (value.getIntVal() == intVal)
                    return value;
            }
            throw new IllegalArgumentException("intVal " + intVal + "has now corresponiding LbDataSource");
        }
    }	
    
	public String getBoardName();
	public String getChipName();
	public String getName();
	public WarnigLevel getLevel();
	public String getMessage();
	public long getValue();
}
