package rpct.xdaqaccess;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.rpc.ServiceException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import rpct.xdaq.XdaqApplicationInfo;
import rpct.xdaqaccess.axis.IIAccessImplAxis;
import rpct.xdaqaccess.diag.Diag;
import rpct.xdaqaccess.diag.DiagAccess;
import rpct.xdaqaccess.diag.DiagCtrl;
import rpct.xdaqaccess.diag.DiagId;
import rpct.xdaqaccess.diag.DiagnosticReadout;
import rpct.xdaqaccess.diag.HistoMgr;
import rpct.xdaqaccess.diag.Pulser;
import rpct.xdaqaccess.diag.axis.DiagAccessImplAxis;


public class HardwareRegistry {
    private static Log log = LogFactory.getLog(HardwareRegistry.class);
    //private static HardwareRegistry instance = new HardwareRegistry();
    
    public final class XdaqApplicationAccess {
        private XdaqApplicationInfo xdaqApplicationInfo;
        private IIAccess iiAccess;   
        private List<HardwareItem> hardwareItems = new ArrayList<HardwareItem>();
        private Crate[] crates;
        
        private DiagAccess diagAccess;
        private DiagCtrl[] diagCtrls;
        private Pulser[] pulsers;
        private HistoMgr[] histoMgrs;
        private DiagnosticReadout[] diagnosticReadouts;
                 
        public XdaqApplicationAccess(XdaqApplicationInfo xdaqApplicationInfo) {
            super();
            this.xdaqApplicationInfo = xdaqApplicationInfo;
        }
        
        public IIAccess getIIAccess() throws RemoteException, ServiceException {
            if (iiAccess == null) {
                iiAccess = new IIAccessImplAxis(xdaqApplicationInfo, HardwareRegistry.this);
                crates = iiAccess.getCrates(false);
            }
            return iiAccess;
        } 

        public List<HardwareItem> getHardwareItems() {
            return hardwareItems;
        }

        public Crate[] getCrates() throws RemoteException, ServiceException {
            getIIAccess();
            return crates;
        }
        
        public DiagAccess getDiagAccess() throws RemoteException, ServiceException {
            if (diagAccess == null) {
                diagAccess = new DiagAccessImplAxis(xdaqApplicationInfo, HardwareRegistry.this);
                diagCtrls = diagAccess.getDiagCtrlList();
                pulsers = diagAccess.getPulserList();
                histoMgrs = diagAccess.getHistoMgrList();
                diagnosticReadouts = diagAccess.getDiagnosticReadoutList();
            }
            return diagAccess;
        }

        public DiagCtrl[] getDiagCtrls() throws RemoteException, ServiceException {            
            return diagCtrls;
        }
    }
    
    private Map<XdaqApplicationInfo, XdaqApplicationAccess> xdaqApplicationAccessMap = new HashMap<XdaqApplicationInfo, XdaqApplicationAccess>();
    
    private Map<Integer, Crate> cratesById = new HashMap<Integer, Crate>();
    private Map<Integer, Board> boardsById = new HashMap<Integer, Board>();
    private Map<Integer, Device> devicesById = new HashMap<Integer, Device>();
    private Map<String, HardwareItem> itemsByName = new HashMap<String, HardwareItem>();    
    private Map<String, List<HardwareItem>> itemsByType = new HashMap<String, List<HardwareItem>>();
    private Map<String, DeviceDesc> deviceDescsByType = new HashMap<String, DeviceDesc>();
    
    private Map<DiagId, Diag> diagsById = new HashMap<DiagId, Diag>();    
    private Map<Integer, List<Diag>> diagsByOwner = new HashMap<Integer, List<Diag>>();
    
    private boolean hardwareItemsLoaded = false;
    private boolean diagsLoaded = false;

    
    public HardwareRegistry() {
    }    
    
    private void loadHardwareItems() throws RemoteException, ServiceException {
        if (!hardwareItemsLoaded) {
            for (XdaqApplicationAccess access : xdaqApplicationAccessMap.values()) {
                access.getIIAccess();
            }
            hardwareItemsLoaded = true;
        }
    }   
    
    private void loadDiags() throws RemoteException, ServiceException {
        if (!diagsLoaded) {
            for (XdaqApplicationAccess access : xdaqApplicationAccessMap.values()) {
                access.getDiagAccess();
            }
            diagsLoaded = true;
        }
    }
    
    public boolean isXdaqApplicationRegistered(XdaqApplicationInfo info) {
    	return xdaqApplicationAccessMap.containsKey(info);
    }
    
    public void registerXdaqApplication(XdaqApplicationInfo info) {
    	if (xdaqApplicationAccessMap.containsKey(info)) {
    		throw new IllegalStateException(info + " is already registered");
    	}
        xdaqApplicationAccessMap.put(info, new XdaqApplicationAccess(info));
        hardwareItemsLoaded = false;
        diagsLoaded = false;
    }
    public XdaqApplicationAccess getXdaqApplicationAccess(XdaqApplicationInfo info) {
        return xdaqApplicationAccessMap.get(info);
    }
    
    public Crate findCrateById(int id) throws RemoteException, ServiceException {
        loadHardwareItems();
        return cratesById.get(id);
    }
    
    public Board findBoardById(int id) throws RemoteException, ServiceException {
        loadHardwareItems();
        return boardsById.get(id);
    }
    
    public Device findDeviceById(int id) throws RemoteException, ServiceException {
        loadHardwareItems();
        return devicesById.get(id);
    }
    
    public HardwareItem findByName(String name) throws RemoteException, ServiceException {
        loadHardwareItems();
        return itemsByName.get(name.trim().toUpperCase());
    }
    
    public List<HardwareItem> findByType(String type) throws RemoteException, ServiceException {
        loadHardwareItems();
        return Collections.unmodifiableList(itemsByType.get(type.toUpperCase()));
    }
    
    public List<Crate> getAllCrates() throws RemoteException, ServiceException {
        ArrayList<Crate> crates = new ArrayList<Crate>();
        for (XdaqApplicationAccess access : xdaqApplicationAccessMap.values()) {
            Crate[] c = access.getCrates();
            for (Crate crate : c) {
                crates.add(crate);
            }
        }
        return crates;
    }
    
    public Diag findDiagById(DiagId id) throws RemoteException, ServiceException {
        loadDiags();
        return diagsById.get(id);        
    }
    
    public List<Diag> findDiagsByOwnerId(int ownerId) throws RemoteException, ServiceException {
        loadDiags();
        return Collections.unmodifiableList(diagsByOwner.get(ownerId));
    }
    
    public DeviceDesc findDeviceDesc(String type) {
        return deviceDescsByType.get(type);
    }
    
    public List<Diag> getAllDiags() {
        return new ArrayList<Diag>(diagsById.values());
    }
    
    public void registerItem(HardwareItem item) {
        HardwareItem previousItem;
        
        XdaqApplicationAccess xdaqApplicationAccess = xdaqApplicationAccessMap.get(item.getIIAccess().getXdaqApplicationInfo());
        if (xdaqApplicationAccess == null) {
            throw new IllegalArgumentException("xdaqApplicationAccess not found for item " + item);
        }
        xdaqApplicationAccess.getHardwareItems().add(item);
        
        if (item instanceof Crate) {
            previousItem = cratesById.put(item.getId(), (Crate) item);
            if (previousItem != null) {
                log.warn("Duplicate element with id = " + item.getId() 
                        + " (element name = " + item.getName() + ")");
            }            
        }
        else if (item instanceof Board) {
            previousItem = boardsById.put(item.getId(), (Board) item);
            if (previousItem != null) {
                log.warn("Duplicate element with id = " + item.getId() 
                        + " (element name = " + item.getName() + ")");
            }            
        }
        else if (item instanceof Device) {
            previousItem = devicesById.put(item.getId(), (Device) item);
            if (previousItem != null) {
                log.warn("Duplicate element with id = " + item.getId() 
                        + " (element name = " + item.getName() + ")");
            }            
        }
        
        previousItem = itemsByName.put(item.getName(), item);
        if (previousItem != null) {
            log.warn("Duplicate element with name = " + item.getName());
        }
        
        List<HardwareItem> list = itemsByType.get(item.getType().toUpperCase());
        if (list == null) {
            list = new ArrayList<HardwareItem>();
            itemsByType.put(item.getType().toUpperCase(), list);
        }
        list.add(item);
    }
    
    public void registerDiag(Diag diag) {        
        Diag previousItem = diagsById.put(diag.getDiagId(), diag);
        if (previousItem != null) {
            log.warn("Duplicate diag with id = " + diag.getDiagId());
        }

        List<Diag> list = diagsByOwner.get(diag.getDiagId().getOwnerId());
        if (list == null) {
            list = new ArrayList<Diag>();
            diagsByOwner.put(diag.getDiagId().getOwnerId(), list);
        }
        list.add(diag);
    }    
    
    public void registerDeviceDesc(DeviceDesc deviceDesc) {     
        if (deviceDescsByType.get(deviceDesc.getType()) != null) {
            throw new IllegalArgumentException("deviceDesc already registered: " + deviceDesc.getType());
        }
        deviceDescsByType.put(deviceDesc.getType(), deviceDesc);
    }
}
