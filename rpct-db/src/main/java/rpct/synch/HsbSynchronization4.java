package rpct.synch;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.rpc.ServiceException;

import rpct.datastream.TestPresets;
import rpct.datastream.TestPulsesGenerator;
import rpct.datastream.TestsManager;
import rpct.datastream.TestsManager.PulserTarget;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.SorterCrate;
import rpct.db.domain.equipment.TcHsbConnection;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.DeviceItemWord;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.diag.TriggerDataSel;
import rpct.xdaqaccess.diag.DiagCtrl.TriggerType;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbMuxTransChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;
import rpct.xdaqaccess.synchro.DefOut;
import rpct.xdaqaccess.synchro.LVDSLink;
import rpct.xdaqaccess.synchro.LVDSLinksSynchronizer;
import rpct.xdaqaccess.synchro.LVDSTransmDevice;
import rpct.xdaqaccess.synchro.LVDSTransmDeviceHSB;

public class HsbSynchronization4 {

	public static void main(String[] args) throws ServiceException,
	DataAccessException, IOException, InterruptedException,
	XdaqException, HardwareDbException {
       
        if(args.length == 0) {            
            String[] argsL = {
                    "TC_0",
                    "TC_1",
                    "TC_2",
                    "TC_3",
                    "TC_4",
                    "TC_5",
                    "TC_6",
                    "TC_7",
                    "TC_8",
                    "TC_9",
                    "TC_10",
                    "TC_11"
                    };
            
            args = argsL;
        }
        
		HibernateContext context = new SimpleHibernateContextImpl();
		EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
		HardwareDbMapper dbMapper = new HardwareDbMapper(
				new HardwareRegistry(), context);
        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);

        String pulseXmlFileName =  System.getenv("HOME") + "/bin/data/gentestpulses_sorts.xml";
        TestPresets.TestPreset preset = new TestPresets.LongPulser_SnapshotBc0(128, Long.MAX_VALUE);        
        //TestPresets.TestPreset preset = new TestPresets.ReapetPulserBC0_SnapshotL1A(3000);

        TestsManager testsManager = new TestsManager(pulseXmlFileName, preset, null, equipmentDAO, configurationManager, dbMapper, "");
        testsManager.setPulseTarget(TestsManager.PulserTarget.chipOut);
      
		try {
            Set<HardwareDbChip> halfSorts = new HashSet<HardwareDbChip>();            
            List<TcHsbConnection> tcHsbConnections = new ArrayList<TcHsbConnection>();
                                  
                       
            String fileName = "";
            System.out.println("Selected crates");
            
            List<HardwareDbTriggerCrate> crates = new ArrayList<HardwareDbTriggerCrate>();
            for(int i = 0; i < args.length; i++) {
                System.out.print(args[i] + " ");
                fileName = fileName + "HSB-" + args[i];
                crates.add((HardwareDbTriggerCrate)dbMapper.getCrate(equipmentDAO.getCrateByName(args[i])));
            }
            
            FileOutputStream file = new FileOutputStream(System.getenv("HOME") + "/bin/out/" + fileName + "_puls.txt", false);
            PrintStream prntSrm = new PrintStream(file);
            DefOut defLog= new DefOut(prntSrm);

            LVDSLink.clkPhaseSteps = 4;
            LVDSLinksSynchronizer tcLinksSynchronizer = new LVDSLinksSynchronizer(2, 2);           
            
            List<Chip> trnasmChips = new ArrayList<Chip>();
                      
            for (HardwareDbTriggerCrate hdbTc : crates) {                
                List<TcHsbConnection> thisTcHsbConnections = hdbTc.getTcHsbConnections(equipmentDAO);
                tcHsbConnections.addAll(thisTcHsbConnections);
                for(TcHsbConnection tcHsbConnection : thisTcHsbConnections) {
                    halfSorts.add(dbMapper.getChip(tcHsbConnection.getHalfSort()));
                    
                    tcLinksSynchronizer.addLVDSLink(new LVDSLink(((HardwareDbMuxTransChip)hdbTc.getTcSort()).getTransmDevice(), 
                            ((HardwareDbMuxTransChip)dbMapper.getChip(tcHsbConnection.getHalfSort())).getTransmDevice(), tcHsbConnection.getCableInputNum()));
                    
                    tcLinksSynchronizer.addLVDSLink(new LVDSLink(((HardwareDbMuxTransChip)hdbTc.getTcSort()).getTransmDevice(), 
                            ((HardwareDbMuxTransChip)dbMapper.getChip(tcHsbConnection.getHalfSort())).getTransmDevice(), tcHsbConnection.getCableInputNum() + 1));
                    
                    System.out.println(tcHsbConnection.getDescritpion());
                }
                
                trnasmChips.addAll(equipmentDAO.getChipsByTypeAndCrate(ChipType.TCSORT, hdbTc.getDbCrate(), true)); //TODO
            } 
            
            testsManager.addPulsers(trnasmChips);

            //checking, if the BC0 is ticking
            if(true) {                
                if(trnasmChips.size() != 0) {
                    HardwareDbChip opto = dbMapper.getChip(trnasmChips.get(0));
                    long bcoCnt = opto.getHardwareChip().getItem("TEST_CNT.BCN0").read();
                    int i = 0;
                    while(bcoCnt == opto.getHardwareChip().getItem("TEST_CNT.BCN0").read()) {
                        DefOut.out.println("waitnig for BC0");
                        Thread.sleep(100);
                        if(i > 100) {
                            throw new RuntimeException("the bc0 is not changing");
                        }
                    }
                }
            }
            
            if(true) //TODO
                tcLinksSynchronizer.checkInterconnections();

            if (true) { //TODO turn off or the synchronization process
                System.out.println("");
                DefOut.out.println("");
                tcLinksSynchronizer.enableTransmissionCheck(true, true);
                for(LVDSTransmDevice deviceHSB : tcLinksSynchronizer.getRecivers()) {
                    deviceHSB.getDevice().getItem("REC_DATA_DELAY").write(0x0);
                    deviceHSB.getDevice().getItem("STATUS.TTC_CLK_INV").write(0);
                }
                
                testsManager.preparePulsersForSynchro();
                testsManager.startPulsers();
                
                for(LVDSLink link : tcLinksSynchronizer.getLvdsLinks()) {
                    boolean[] isOKTab = new boolean[8];
                    ((DeviceItemWord)link.getReciver().getDevice().getItem("REC_FAST_REG_ADD")).write(link.getLinkNum(), 0);                    
                    DeviceItemWord recFastDealy = (DeviceItemWord) link.getReciver().getDevice().getItem("REC_FAST_DATA_DELAY"); 
                    int maxDelay = 8; //recFastDealy.getInfo().getWidth() / 16;
                    for(int fastDelay = 0; fastDelay < maxDelay; fastDelay++) {
                        ((LVDSTransmDeviceHSB)link.getReciver()).setRecFastDataDelay(fastDelay, link.getLinkNum());                      
                        //((LVDSTransmDeviceHSB)link.getReciver()).setRecFastDataDelayAndClkInv(fastDelay, 0, link.getLinkNum());
                        long orData = ((DeviceItemWord)link.getReciver().getDevice().getItem("REC_TEST_OR_DATA")).read(link.getLinkNum());
                        Thread.sleep(100);
                        orData = ((DeviceItemWord)link.getReciver().getDevice().getItem("REC_TEST_OR_DATA")).read(link.getLinkNum());
                        if(orData != 0) {
                            isOKTab[fastDelay] = false;
                            System.out.print("_ ");
                            DefOut.out.print("_ ");
                        }
                        else {
                            isOKTab[fastDelay] = true;
                            System.out.print("1 ");
                            DefOut.out.print("1 ");
                        }
                    }
                     int firsGood = -1;
                    int lastGood = -1;
                    int foundDel = -1;
                    if(isOKTab[0]) {
                        firsGood = 0;
                    }
                    for(int del = 0; del < 7; del++) {
                        if(isOKTab[del] != true && isOKTab[del+1] == true)
                            firsGood = del + 1;
                        else if(isOKTab[del] == true && isOKTab[del+1] != true)
                            lastGood = del;
                    }
                    if(lastGood == -1 && isOKTab[7] == true ) {
                        lastGood = 7;
                    }

                    if(firsGood != -1 && lastGood != -1) {
                        if(firsGood == 0 && lastGood == 7) {
                            foundDel = 3;                   
                            System.out.println(link.getName() + " foundDel " + foundDel);
                            DefOut.out.println(link.getName() + " foundDel " + foundDel);
                        }
                        else {
                            if(lastGood >= firsGood) {
                                if(lastGood == 7 && firsGood == 6)
                                    foundDel = 7;
                                else if(lastGood == 1 && firsGood == 0)
                                    foundDel = 0;
                                else
                                    foundDel = (firsGood + lastGood ) / 2;
                            }
                            else {
                                foundDel = ((firsGood + lastGood + maxDelay) / 2) % maxDelay;
                            }
                            System.out.println(link.getName() + " foundDel " + foundDel);
                            DefOut.out.println(link.getName() + " foundDel " + foundDel);

                            ((LVDSTransmDeviceHSB)link.getReciver()).setRecFastDataDelay(foundDel, link.getLinkNum());
                            //((LVDSTransmDeviceHSB)link.getReciver()).setRecFastDataDelayAndClkInv(foundDel, 0, link.getLinkNum());
                        }
                    }
                    else {              
                        System.out.println(link.getName() + ". delay not found, synchronization failed!!! " );
                        DefOut.out.println(link.getName() + ". delay not found, synchronization failed!!! " );
                        ((LVDSTransmDeviceHSB)link.getReciver()).setRecFastDataDelay(0, link.getLinkNum());
                        //((LVDSTransmDeviceHSB)link.getReciver()).setRecFastDataDelayAndClkInv(0, 0, link.getLinkNum());
                    }
                }
                testsManager.stopPulsers();
                
            }

            //defLog = new DefOut(System.out); //TODO choose where the output will be directed
            if (false) 
                tcLinksSynchronizer.randomPulsTest(testsManager, true, true, 10);//TODO choose the details o random test          
                        
            System.out.println("end");
           
		} catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
			context.closeSession();
		}
	}

}
