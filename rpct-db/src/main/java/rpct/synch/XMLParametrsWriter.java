package rpct.synch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.Board;
import rpct.xdaqaccess.Crate;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDBTTUBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbCrate;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbSorterCrate;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;
import rpct.xdaqaccess.hardwaredb.HardwareDbMuxRecChip;
import rpct.xdaqaccess.synchro.LVDSTransmDevice;

public class XMLParametrsWriter {
    static List<String> registersNames = new ArrayList<String>();
    static List<String> tabRegistersNames = new ArrayList<String>();
    
    private EquipmentDAO equipmentDAO;
    private HardwareDbMapper dbMapper;
    
    static {
        registersNames.add("REC_MUX_CLK90");
        registersNames.add("REC_MUX_CLK_INV");
        registersNames.add("REC_MUX_REG_ADD");    
        
        tabRegistersNames.add("REC_MUX_DELAY"); 
        tabRegistersNames.add("REC_DATA_DELAY");         
    }
    
    public XMLParametrsWriter(EquipmentDAO equipmentDAO, HardwareDbMapper dbMapper) {
        this.equipmentDAO =  equipmentDAO;
        this.dbMapper = dbMapper;
    }

    public void readParameters(List<HardwareDbCrate> crates, String xmlFileName) throws JAXBException, HardwareDbException, RemoteException, XdaqException, ServiceException, FileNotFoundException {
        final File xmlSettingsFile = new File(xmlFileName);
        JAXBContext context1 = JAXBContext.newInstance(RpctSettingsXml.class);
        System.out.println("reading settings from boards and saving them to the " + xmlSettingsFile);
        
        Marshaller m = context1.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        RpctSettingsXml setings = new RpctSettingsXml();

        for (HardwareDbCrate crate : crates) {            
            System.out.println("Crate " + crate.getName());
            if(crate.getDbCrate().getType() == CrateType.TRIGGERCRATE) {
                for(HardwareDBTriggerBoard board : ((HardwareDbTriggerCrate)crate).getHardwareDBTriggerBoards()) {                                                                                        
                    XmlBoard xmlTb = new XmlBoard(board.getHardwareBoard());
                    setings.addBoard(xmlTb);
                    for(HardwareDbMuxRecChip chip : board.getMuxRecChips()) {
                        XmlDevice xmlDevice = new XmlDevice(chip.getHardwareChip(), dbMapper.getHardwareRegistry());
                        for(String regName : registersNames) {
                            xmlDevice.addRegister(regName, "");
                        }

                        for(String tabRegName : tabRegistersNames) {               
                            int size = chip.getHardwareChip().getItem(tabRegName).getDesc().getNumber();
                            xmlDevice.addTabRegister(new XmlTabRegister(tabRegName, size));
                        }                       
                        xmlTb.addXmlDevice(xmlDevice); 
                    }                                       
                }    
                
                HardwareDbBoard board = ((HardwareDbTriggerCrate)crate).getTcBackplane();                                                                                         
                XmlBoard xmlTcBack = new XmlBoard(board.getHardwareBoard());
                setings.addBoard(xmlTcBack);
                HardwareDbChip chip = ((HardwareDbTriggerCrate)crate).getTcSort();
                XmlDevice xmlDevice = new XmlDevice(chip.getHardwareChip(), dbMapper.getHardwareRegistry());
                for(String regName : registersNames) {
                    xmlDevice.addRegister(regName, "");
                }

                for(String tabRegName : tabRegistersNames) {               
                    int size = chip.getHardwareChip().getItem(tabRegName).getDesc().getNumber();
                    xmlDevice.addTabRegister(new XmlTabRegister(tabRegName, size));
                }                       
                xmlTcBack.addXmlDevice(xmlDevice);  
            }

            if(crate.getDbCrate().getType() == CrateType.SORTERCRATE) {
                for(HardwareDBTTUBoard board : ((HardwareDbSorterCrate)crate).getHardwareDBTTUBoards()) {                                                                                        
                    XmlBoard xmlTb = new XmlBoard(board.getHardwareBoard());
                    setings.addBoard(xmlTb);
                    for(HardwareDbMuxRecChip chip : board.getMuxRecChips()) {
                        XmlDevice xmlDevice = new XmlDevice(chip.getHardwareChip(), dbMapper.getHardwareRegistry());
                        for(String regName : registersNames) {
                            xmlDevice.addRegister(regName, "");
                        }

                        for(String tabRegName : tabRegistersNames) {               
                            int size = chip.getHardwareChip().getItem(tabRegName).getDesc().getNumber();
                            xmlDevice.addTabRegister(new XmlTabRegister(tabRegName, size));
                        }                       
                        xmlTb.addXmlDevice(xmlDevice); 
                    }                                       
                }
            }
        }           
                       
        setings.readRegisters();

        m.marshal(setings, new FileOutputStream(xmlSettingsFile));
        System.out.println("end");
    }
       
    public void setParamters(String xmlFileName) throws JAXBException, RemoteException, XdaqException, ServiceException, DataAccessException {
        final File xmlSettingsFile = new File(xmlFileName);
        JAXBContext context1 = JAXBContext.newInstance(RpctSettingsXml.class);
        System.out.println("applaying settings from the " + xmlSettingsFile);
        Unmarshaller um = context1.createUnmarshaller();
        RpctSettingsXml setings = (RpctSettingsXml) um.unmarshal(xmlSettingsFile);

        //Marshaller m = context1.createMarshaller();
        //m.marshal(setings, System.out);
        System.out.println("");

        XmlDevice.setHardwareRegistry(dbMapper.getHardwareRegistry());
        XmlDevice.setEquipmentDAOandDbMapper(dbMapper, equipmentDAO);
        
        setings.aplyRegisters();
        System.out.println("end");
    }
    
    public static void main(String[] args) throws DataAccessException {          
        if(args.length == 0) {            
            String[] argsL = {
//                    "TC_0",                    
//                    "TC_1",
//                    "TC_2",                   
//                    "TC_3",                    
//                    "TC_4",
//                    "TC_5",                    
//                    "TC_6",                    
//                    "TC_7",
//                    "TC_8",                   
//                    "TC_9",                    
//                    "TC_10",
//                    "TC_11",
            		"PASTEURA_TC"
                    //"SC"
                    };
            
            args = argsL;
        }
        
        System.out.println("Selected crates");
        for(int i = 0; i < args.length; i++) {
            System.out.print(args[i] + " ");
        }
             
        System.out.println("");
        HibernateContext context = new SimpleHibernateContextImpl();
        try {
            String r_s = "r";
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("read seting from hardware and store to the file (r) or set from xml (s)? ");
            r_s = br.readLine();                             

            EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
            HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
            XMLParametrsWriter xmlParametrsWriter = new XMLParametrsWriter(equipmentDAO, dbMapper);
            
            for(int i = 0; i < args.length; i++) {
                String xmlFileName = System.getenv("HOME") + "/bin/out/" + args[i] + ".xml";
                System.out.println("xmlFileName "+ xmlFileName);
                //one crate to one file
                List<HardwareDbCrate> crates = new ArrayList<HardwareDbCrate>();
                crates.add(dbMapper.getCrate(equipmentDAO.getCrateByName(args[i])));
                
                //dbMapper.getBoard(equipmentDAO.getBoardByName("TTU_1"));
                
                if (r_s.equals("r")) {
                    System.out.println("the file "+ xmlFileName + " will be overwritten, continue? (y/n)");
                    String y_n = "n";
                    y_n = br.readLine();
                    if (y_n.equals("y"))
                        xmlParametrsWriter.readParameters(crates, xmlFileName);                       
                } 
                else if (r_s.equals("s")) {
                    xmlParametrsWriter.setParamters(xmlFileName);
                }
            }
            System.out.println("End");
        }
        catch (XdaqException e) {
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JAXBException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (HardwareDbException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } 
        finally {
            context.closeSession();
        }
    }
}
