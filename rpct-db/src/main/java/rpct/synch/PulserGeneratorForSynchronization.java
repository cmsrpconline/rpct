package rpct.synch;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import rpct.db.domain.equipment.ChipType;
import rpct.xdaq.axis.Binary;

public class PulserGeneratorForSynchronization {
    private int linkCnt;

    LVDSLinkDef linkDef; //output link

    public PulserGeneratorForSynchronization(int linkCnt, LVDSLinkDef linkDef) {
        this.linkCnt = linkCnt;    
        this.linkDef = linkDef;
    }      

    public Binary[] generatePulses(int sLine, int pulseLenght) {  
        Random generator = new Random();
        int pulseWidth = linkDef.getDataBitsCnt() * linkCnt;                 
        int pulseByteWidth = pulseWidth / Byte.SIZE;
        if (pulseWidth % Byte.SIZE > 0)
            pulseByteWidth++;

        Binary[] data = new Binary[pulseLenght];
        for(int iBx = 0; iBx < pulseLenght; iBx++) {   
            byte[] bytes = new byte[pulseByteWidth];            
            for(int iLink = 0; iLink < linkCnt; iLink++) {
                for(int iLine = 0; iLine < linkDef.getLinesCnt(); iLine++) {
                    if(linkDef.isLineActive(sLine, iLine)) {
                        int offset = iLink * linkDef.getDataBitsCnt() + linkDef.getDataBitsOffsetForLine()[iLine];
                        int val = generator.nextInt(1 << linkDef.getBitsPerLine());
                        for(int iBit = 0; iBit < linkDef.getDataBitsCntForLine()[iLine]; iBit++) {
                            Binary.setBit(bytes, offset + iBit, val & (1<<iBit));
                        }
                    }
                }
            }
            data[iBx] = new Binary(bytes);
        }
        return data;
    }

    
    public Binary[] generatePulses(Set<Integer> sLines, int pulseLenght) {  
        Random generator = new Random();
        int pulseWidth = linkDef.getDataBitsCnt() * linkCnt;                 
        int pulseByteWidth = pulseWidth / Byte.SIZE;
        if (pulseWidth % Byte.SIZE > 0)
            pulseByteWidth++;

        Binary[] data = new Binary[pulseLenght];
        for(int iBx = 0; iBx < pulseLenght; iBx++) {   
            byte[] bytes = new byte[pulseByteWidth];            
            for(int iLink = 0; iLink < linkCnt; iLink++) {
                for(Integer sLine : sLines)
                    for(int iLine = 0; iLine < linkDef.getLinesCnt(); iLine++) {
                        if(linkDef.isLineActive(sLine, iLine)) {
                            int offset = iLink * linkDef.getDataBitsCnt() + linkDef.getDataBitsOffsetForLine()[iLine];
                            int val = generator.nextInt(1 << linkDef.getBitsPerLine());
                            for(int iBit = 0; iBit < linkDef.getDataBitsCntForLine()[iLine]; iBit++) {
                                Binary.setBit(bytes, offset + iBit, val & (1<<iBit));
                            }
                        }
                        else {
                            int offset = iLink * linkDef.getDataBitsCnt() + linkDef.getDataBitsOffsetForLine()[iLine];
                            int val = 0x55;
                            for(int iBit = 0; iBit < linkDef.getDataBitsCntForLine()[iLine]; iBit++) {
                                Binary.setBit(bytes, offset + iBit, val & (1<<iBit));
                            }
                        }
                    }
            }
            data[iBx] = new Binary(bytes);
        }
        return data;
    }
    
    public Binary[] generatePulses(int pulseLenght) {  
        Random generator = new Random();
        int pulseWidth = linkDef.getDataBitsCnt() * linkCnt;                 
        int pulseByteWidth = pulseWidth / Byte.SIZE;
        if (pulseWidth % Byte.SIZE > 0)
            pulseByteWidth++;

        Binary[] data = new Binary[pulseLenght];
        for(int iBx = 0; iBx < pulseLenght; iBx++) {   
            byte[] bytes = new byte[pulseByteWidth];
            generator.nextBytes(bytes);                                     
            data[iBx] = new Binary(bytes);
        }
        return data;
    }
    
    private static Map<ChipType, PulserGeneratorForSynchronization> generators = new HashMap<ChipType, PulserGeneratorForSynchronization>();

    public static Map<ChipType, PulserGeneratorForSynchronization> getGenerators() {
        return generators;
    }
}
