package rpct.synch;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.MalformedURLException;

import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;
import rpct.xdaqaccess.synchro.DefOut;
import rpct.xdaqaccess.synchro.LVDSLinksSynchronizer;

public class SynchroTest {         
    public static void main(String[] args) throws DataAccessException {
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
        ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);
        try {             
            FileOutputStream file = new FileOutputStream(System.getenv("HOME") + "/bin/out/PASTEURA_TC_synchTest.txt", false);
            PrintStream prntSrm = new PrintStream(file);
            DefOut defLog= new DefOut(prntSrm);
            
            LVDSLinksSynchronizer tcLinksSynchronizer = new LVDSLinksSynchronizer(14, 8);           
            
            TriggerCrate crate = (TriggerCrate) equipmentDAO.getCrateByName("PASTEURA_TC");  
            HardwareDbTriggerCrate hdbTc = (HardwareDbTriggerCrate)dbMapper.getCrate(crate);

            //Board tb = equipmentDAO.getBoardByName("TBp2_9");
            //HardwareDBTriggerBoard tbHdb = (HardwareDBTriggerBoard)dbMapper.getBoard(tb);
            
            tcLinksSynchronizer.addLVDSLinks(hdbTc.getLVDSLinks()); //TODO   	
            //tcLinksSynchronizer.addLVDSLinks(tbHdb.getLVDSLinks());
            
/*            for(LVDSLink link : tcLinksSynchronizer.getLvdsLinks()) {
            	if(link.getDescription().equals("TBp2_9 OPTO 6 - PAC 9") && link.getLinkNum() == 12) {
//            		link.setRecMuxClkInv(0, 0);
//            		link.setRecMuxRegAdd(0, 0);
//            		link.setRecMuxClkInv(1, 0);
//            		link.setRecMuxRegAdd(1, 0);
            		link.setRecMuxClkInv(2, 1);
            		link.setRecMuxRegAdd(2, 1);
            		//link.setRecMuxDelay(7);
            		System.out.println("changing the synchro params fo the " + link.getDescription());
            	}
            }*/
			defLog = new DefOut(System.out); //TODO choose where the output will be directed
            tcLinksSynchronizer.randomTest(LVDSLinksSynchronizer.TestType.rnd, false, false, 1);//TODO choose the details o random test          
            
            System.out.println("end with ");
            
        } 
        catch (MalformedURLException e) {
            e.printStackTrace();
        } 
        catch (XdaqException e) {
            e.printStackTrace();
        } 
        catch (IOException e) {
            e.printStackTrace();
        } catch (ServiceException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            context.closeSession();
        }
    }  
}
