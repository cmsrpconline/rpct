package rpct.synch;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.DeviceItemWord;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDBTTUBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbCrate;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbOpto;
import rpct.xdaqaccess.hardwaredb.HardwareDbSorterCrate;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;

public class CheckOptLinks {

	/**
	 * 
	 * @param triggerCrates
	 * @return failed links count
	 * @throws HardwareDbException
	 * @throws ServiceException
	 * @throws InterruptedException
	 * @throws IOException
	 */
    static int checkOptLinks(List<HardwareDbTriggerCrate> triggerCrates) throws HardwareDbException, ServiceException, InterruptedException, IOException {
        for(HardwareDbTriggerCrate tc : triggerCrates) {
            for(HardwareDBTriggerBoard tb : tc.getHardwareDBTriggerBoards()) {
                for (HardwareDbOpto opto : tb.getOptos()) { 
                    opto.setUsedOptLinksInputsMaskFromHardware();                   
                    opto.getLinkConns(true);
                }
            }
        }

//clears REC_TEST_OR_DATA
/*        for(HardwareDbTriggerCrate tc : triggerCrates) {
            for(HardwareDBTriggerBoard tb : tc.getHardwareDBTriggerBoards()) {
                for (HardwareDbOpto opto : tb.getOptos()) { 
                    //opto.setUsedOptLinksInputsMaskFromHardware();

                    for(LinkConn optLink : opto.getLinkConns(true) ) {      
                        if(optLink.isDisabled()) 
                            continue;                        
                        ((rpct.xdaqaccess.DeviceItemWord)((HardwareDbChip)opto).getHardwareChip().getItem("REC_TEST_OR_DATA")).read(optLink.getOptoInputNum());

                    }            
                }
            }                   
        }*/

        int totalLinks = 0;
        int notOKLinks = 0;
        
        for(HardwareDbTriggerCrate tc : triggerCrates) {
            for(HardwareDBTriggerBoard tb : tc.getHardwareDBTriggerBoards()) {
                for (HardwareDbOpto opto : tb.getOptos()) { 
                    //opto.setUsedOptLinksInputsMaskFromHardware();

                    /*if(false) {
                        long synchReq = ((HardwareDbChip) opto).getHardwareChip().getItem("STATUS.REC_SYNCH_REQ").read();
                        ((HardwareDbChip) opto).getHardwareChip().getItem("STATUS.REC_SYNCH_REQ").write(0);
                        ((HardwareDbChip) opto).getHardwareChip().getItem("STATUS.REC_SYNCH_REQ").write(synchReq);
                    }*/

                    long fastDealy = ((HardwareDbChip) opto).getHardwareChip().getItem("REC_FAST_DATA_DELAY.REC_FAST_DATA_DELAY").read();
                    long dataDealy = ((HardwareDbChip) opto).getHardwareChip().getItem("REC_DATA_DELAY.REC_DATA_DELAY").read();

                    long synchAck = ((HardwareDbChip) opto).getHardwareChip().getItem("STATUS.REC_SYNCH_ACK").read();
                    long tlkRxError = ((HardwareDbChip) opto).getHardwareChip().getItem("TLK.RX_ERROR").read();
                    long errCnt =  ((HardwareDbChip) opto).getHardwareChip().getItem("REC_ERROR_COUNT").read();                  
                    ((HardwareDbChip) opto).getHardwareChip().getItem("REC_ERROR_COUNT").write(0); //clears error counter
                    
                    for(LinkConn optLink : opto.getLinkConns(true) ) {      
                        if(optLink.isDisabled()) 
                            continue;

                        totalLinks++;
                        //long fastD = (fastDealy & (7 << (optLink.getOptoInputNum() * 3) ) ) >> (optLink.getOptoInputNum() * 3);
                        long fastD = ((rpct.xdaqaccess.DeviceItemBits)((HardwareDbChip) opto).getHardwareChip().getItem("REC_FAST_DATA_DELAY.REC_FAST_DATA_DELAY")).read(optLink.getOptoInputNum());
                        //long dataD = (dataDealy & (3 << (optLink.getOptoInputNum() * 2) ) ) >> (optLink.getOptoInputNum() * 2);
                        long dataD = ((rpct.xdaqaccess.DeviceItemBits)((HardwareDbChip) opto).getHardwareChip().getItem("REC_DATA_DELAY.REC_DATA_DELAY")).read(optLink.getOptoInputNum());
                        		
                        //((rpct.xdaqaccess.DeviceItemWord)((HardwareDbChip)opto).getHardwareChip().getItem("REC_TEST_OR_DATA")).read(optLink.getOptoInputNum());

                        long orData = ((rpct.xdaqaccess.DeviceItemWord)((HardwareDbChip)opto).getHardwareChip().getItem("REC_TEST_OR_DATA")).read(optLink.getOptoInputNum());
                        long testData = ((rpct.xdaqaccess.DeviceItemWord)((HardwareDbChip)opto).getHardwareChip().getItem("REC_TEST_DATA")).read(optLink.getOptoInputNum());
                        String status = "";
                        long thisSynchAck = (synchAck & (1l << optLink.getOptoInputNum()) );
                        
                        long thisTlkRxError = (tlkRxError & (1l << optLink.getOptoInputNum()) );
                        
                        if(orData != 0 || thisSynchAck == 0 || thisTlkRxError != 0) {
                            status = "NOT OK !!!!!!!!!!!!!!!!";
                            log.error(optLink.toString() + "\ttlkRxError " + thisTlkRxError + " synchAck " + thisSynchAck + " fastDealy " + fastD + " dataDealy " + dataD 
                            		+ " orData " + orData + " " + " errCnt " + errCnt + " " + status);
                            notOKLinks++;
                        }
                        else {
                            status = "OK"; 
                            log.error(optLink.toString() + "\ttlkRxError " + thisTlkRxError + " synchAck " + thisSynchAck + " fastDealy " + fastD + " dataDealy " + dataD 
                            		+ " orData " + orData + " " + " errCnt " + errCnt + " " + status);
                        }

                    }                                                            
                }
            }                   
        }

        if(notOKLinks != 0)
        	log.error("Failed links " + notOKLinks + " / " + totalLinks + " = " + ((double)notOKLinks) / totalLinks);
        else
        	log.info("Failed links " + notOKLinks + " / " + totalLinks + " = " + ((double)notOKLinks) / totalLinks);
        
        return notOKLinks;
    }

    static int checkOptLinksTTU(HardwareDbSorterCrate sc) throws HardwareDbException, ServiceException, InterruptedException, IOException {
        for(HardwareDBTTUBoard tb : sc.getHardwareDBTTUBoards()) {
            for (HardwareDbOpto opto : tb.getOptos()) { 
                opto.setUsedOptLinksInputsMaskFromHardware();                   
                //opto.getLinkConns(true);
            }
        }


        for(HardwareDBTTUBoard tb : sc.getHardwareDBTTUBoards()) {
            for (HardwareDbOpto opto : tb.getOptos()) { 
                //opto.setUsedOptLinksInputsMaskFromHardware();

                for(int iOl = 0; iOl < ((DeviceItemWord)((HardwareDbChip)opto).getHardwareChip().getItem("REC_TEST_OR_DATA")).getNumber(); iOl++ ) {                          
                    ((DeviceItemWord)((HardwareDbChip)opto).getHardwareChip().getItem("REC_TEST_OR_DATA")).read(iOl);
                }                                                            
            }
        }     

        int totalLinks = 0;
        int notOKLinks = 0;
        for(HardwareDBTTUBoard tb : sc.getHardwareDBTTUBoards()) {
            for (HardwareDbOpto opto : tb.getOptos()) { 
               /* if(false) {
                    long synchReq = ((HardwareDbChip) opto).getHardwareChip().getItem("STATUS.REC_SYNCH_REQ").read();
                    ((HardwareDbChip) opto).getHardwareChip().getItem("STATUS.REC_SYNCH_REQ").write(0);
                    ((HardwareDbChip) opto).getHardwareChip().getItem("STATUS.REC_SYNCH_REQ").write(synchReq);
                }*/

                long fastDealy = ((HardwareDbChip) opto).getHardwareChip().getItem("REC_FAST_DATA_DELAY.REC_FAST_DATA_DELAY").read();
                long dataDealy = ((HardwareDbChip) opto).getHardwareChip().getItem("REC_DATA_DELAY.REC_DATA_DELAY").read();

                long synchAck = ((HardwareDbChip) opto).getHardwareChip().getItem("STATUS.REC_SYNCH_ACK").read();
                long errCnt =  ((HardwareDbChip) opto).getHardwareChip().getItem("REC_ERROR_COUNT").read();
                	
                for(int iOl = 0; iOl < ((DeviceItemWord)((HardwareDbChip)opto).getHardwareChip().getItem("REC_TEST_OR_DATA")).getNumber(); iOl++ ) {     
                    if( (opto.getUsedOptLinksInputsMask() & (1 << iOl)) == 0 ) 
                        continue;

                    totalLinks++;
                    long fastD = (fastDealy & (7 << (iOl * 3) ) ) >> (iOl * 3);
                    long dataD = (dataDealy & (0xf << (iOl * 4) ) ) >> (iOl * 4);

                    //((rpct.xdaqaccess.DeviceItemWord)((HardwareDbChip)opto).getHardwareChip().getItem("REC_TEST_OR_DATA")).read(optLink.getOptoInputNum());

                    long orData = ((rpct.xdaqaccess.DeviceItemWord)((HardwareDbChip)opto).getHardwareChip().getItem("REC_TEST_OR_DATA")).read(iOl);
                    orData = ((rpct.xdaqaccess.DeviceItemWord)((HardwareDbChip)opto).getHardwareChip().getItem("REC_TEST_OR_DATA")).read(iOl);
                    
                    String status = "";
                    long thisSynchAck = (synchAck & (1l << iOl) );
                    if(orData != 0 || thisSynchAck == 0) {
                        status = "NOT OK !!!!!!!!!!!!!!!!";

                        log.info(opto.getFullName() + " optlink " + iOl + " synchAck " + thisSynchAck + " fastDealy " + fastD + " dataDealy " + dataD + " orData " + orData + " errCnt " + errCnt + " " +status);
                        notOKLinks++;
                    }
                    else {
                        status = "OK"; 
                        //log.info(opto.getFullName() + " optlink " + iOl + " synchAck " + thisSynchAck + " fastDealy " + fastD + " dataDealy " + dataD + " orData " + orData + " errCnt " + errCnt + " " +status);
                    }

                }                                                            
            }
        }                   

        log.info("Failed TTU links " + notOKLinks + " / " + totalLinks + " = " + ((double)notOKLinks) / totalLinks);
        
        return notOKLinks;
    }
    
    private final static Log log = LogFactory.getLog(CheckOptLinks.class);
    
    public static void main(String[] args) {
    	int notOKLinks = -1;
        try {	           	
        	//PropertyConfigurator.configure("checkOptLinks_log4j.properties");
        	//PropertyConfigurator.configure("log4j.properties");
            
            HibernateContext context = new SimpleHibernateContextImpl();
            EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
            //ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
            HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);

            if(args.length == 0) {            
                String[] argsL = {
                        "TC_0",
                        "TC_1",
                        "TC_2",
                        "TC_3",
                        "TC_4",
                        "TC_5",
                        "TC_6",
                        "TC_7",
                        "TC_8",                             
                        "TC_9",
                        "TC_10",
                        "TC_11",
                        //"PASTEURA_TC"
                };

                args = argsL;
            }

            System.out.println("Selected crates");

            List<HardwareDbTriggerCrate> crates = new ArrayList<HardwareDbTriggerCrate>();
            HardwareDbSorterCrate sc = null;
            for(int i = 0; i < args.length; i++) {
            	if(args[i].contains("TC")) {
            		System.out.print(args[i] + " ");
            		crates.add((HardwareDbTriggerCrate)dbMapper.getCrate(equipmentDAO.getCrateByName(args[i])));
            	}
            	else if(args[i].contains("SC")) {
            		System.out.print(args[i] + " ");
            		sc = (HardwareDbSorterCrate)dbMapper.getCrate(equipmentDAO.getCrateByName(args[i]));
            	}
            }            
                 
            String r_s = "y";
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
           
            while(true) {
            	log.info("CheckOptLinks started");
            	notOKLinks = checkOptLinks(crates);
            	log.info("CheckOptLinks finshed");

            	if (sc != null) {
            		notOKLinks += checkOptLinksTTU(sc);
            	}    

            	System.out.println("repeat (y/n)?");
            	r_s = br.readLine();

            	if (r_s.equals("n") == true) {
            		break;
            	}
            }
 
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (HardwareDbException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        System.exit(notOKLinks);
    }

}
