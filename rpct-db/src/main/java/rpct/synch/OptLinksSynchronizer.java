package rpct.synch;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

//import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;

import rpct.datastream.RpctDelays;
import rpct.datastream.TestOptionsXml;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.configuration.SynCoderConf;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbChipImpl;
import rpct.xdaqaccess.hardwaredb.HardwareDbCrate;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbOpto;
import rpct.xdaqaccess.hardwaredb.HardwareDbSynCoder;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;
import rpct.xdaqaccess.synchro.DefOut;
import rpct.xdaqaccess.synchro.OptLink;
import rpct.xdaqaccess.synchro.OptLinkSender;
import rpct.xdaqaccess.synchro.SynchronizationException;

public class OptLinksSynchronizer {
    private static boolean enableRandomTest = false;
    // List<HardwareDbSynCoder> synCoders = new ArrayList<HardwareDbSynCoder>();
    private List<OptLinkSender> senders = new ArrayList<OptLinkSender>();
    private List<HardwareDbOpto> optos = new ArrayList<HardwareDbOpto>();
    private List<OptLink> optLinks = new ArrayList<OptLink>();

    public OptLinksSynchronizer() {
        super();
    }

    private void addLink(OptLink optLink) {
        optLinks.add(optLink);
        if (optLink.getSender() != null
                && senders.contains(optLink.getSender()) == false)
            senders.add(optLink.getSender());

        if (optos.contains(optLink.getOpto()) == false) {
            optos.add(optLink.getOpto());
        }
    }

    private void addLinks(List<OptLink> optLinks) {
        for (OptLink optLink : optLinks)
            addLink(optLink);
    }

    private void addLinkConn(LinkConn linkConn, HardwareDbMapper dbMapper)
    throws DataAccessException, RemoteException, ServiceException {
        OptLink optLink = new OptLink((HardwareDbSynCoder) dbMapper.getChip(linkConn.getSynCoder()), 
                (HardwareDbOpto) dbMapper.getChip(linkConn.getOpto()), linkConn.getOptoInputNum(), linkConn.getTriggerBoardInputNum());
        addLink(optLink);
    }

    private void synchronize(long testData, SynchroType synchroType) throws DataAccessException,
    RemoteException, ServiceException, HardwareDbException,
    InterruptedException 
    {        
        Map<String, Set<Integer> > lbDelaysInTowers = new TreeMap<String, Set<Integer>>();
        Map<String, Set<Integer> > lbDelaysInBoxes = new TreeMap<String, Set<Integer>>();
        
        for (OptLinkSender synCoder : senders) {
            synCoder.reset();
        }

        for (HardwareDbOpto opto : optos) {
            opto.enableRecTransCheck(false, false);
            ((HardwareDbChip) opto).getHardwareChip().getItem("REC_TEST_ENA").write(0);
            ((HardwareDbChip) opto).getHardwareChip().getItem("REC_TEST_RND_ENA").write(0);
            
            ((HardwareDbChip) opto).getHardwareChip().getItem("STATUS.REC_SYNCH_REQ").write(0);
        }

        for (HardwareDbOpto opto : optos) {
            opto.enableOptoLinks(true);
            //opto.enableRecTransCheck(false, false);
        }

        for (OptLinkSender synCoder : senders) {
            synCoder.enableTransmissionCheck(false, false);
            synCoder.synchronizeLink();
        }

        for (HardwareDbOpto opto : optos) {
            opto.CheckLinksSynchronization();
        }

        for (OptLinkSender synCoder : senders) {
            synCoder.normalLinkOperation();
        }

        for (HardwareDbOpto opto : optos) {
            opto.CheckLinksOperation();
        }

        for (OptLinkSender synCoder : senders) {
            synCoder.enableFindOptLinksSynchrDelay(true, testData);
        }

        for (HardwareDbOpto opto : optos) {
            opto.FindOptLinksTLKDelay(testData);
        }

        for (OptLinkSender synCoder : senders) {
            synCoder.enableFindOptLinksSynchrDelay(false, 0);
            synCoder.enableTest(1, 0);
            synCoder.enableTransmissionCheck(true, true);
        }

        for (HardwareDbOpto opto : optos) {
            opto.enableRecTransCheck(true, true);
            opto.getHardwareChip().getItem("REC_TEST_ENA").write(opto.getUsedOptLinksInputsMask());
        }

        if(synchroType == SynchroType.fast) { //selection of the synchronization algorithm
            for(int delay = 0; delay < 4; delay++) {
                for (HardwareDbOpto opto : optos) 
                    opto.FindOptLinksDelayStep1(delay);
    
                Thread.sleep(500);
    
                for (HardwareDbOpto opto : optos) 
                    opto.FindOptLinksDelayStep2(delay);
            }
    
            for (HardwareDbOpto opto : optos) 
                opto.FindOptLinksDelayStep3();
        }
        else if(synchroType == SynchroType.full) { //this algorithm has to be used, if the opt links delay is set also in the LBs
            for(int lbDelay = 0; lbDelay < RpctDelays.LB_DELAY_MAX; lbDelay++) {
                for (OptLinkSender synCoder : senders) {
                    synCoder.setSendDelay(lbDelay);
                }
                for(int delay = 0; delay < 4; delay++) {
                    for (HardwareDbOpto opto : optos) 
                        opto.FindOptLinksDelayStep1(lbDelay, delay);

                    Thread.sleep(500);

                    for (HardwareDbOpto opto : optos) 
                        opto.FindOptLinksDelayStep2(lbDelay, delay);
                }
            }
            
            DefOut.out.println("\n Final results");
            SortedSet<String> failedLinks = new TreeSet<String>();
            for (OptLinkSender synCoder : senders) {                               
                //checking
                for(OptLink link:  synCoder.getUsedOptLinks()) {
                    boolean ok = false;
                    DefOut.out.print("FindOptLinksDelay: LbOptoDealysMap ");
                    for(int lbDelay = 0; lbDelay < RpctDelays.LB_DELAY_MAX; lbDelay++) {                        
                        if(link.getLbOptoDealysMap()[lbDelay] >= 0) {
                            ok = true;
                            DefOut.out.print(lbDelay + " " + link.getLbOptoDealysMap()[lbDelay]  + "; ");
                        }  
                        else
                            DefOut.out.print(lbDelay + " " + "_"  + "; ");
                    }
                    if(!ok) {
                        DefOut.out.println(" " + link + " OPTO_DELAY not found");
                    }
                    else
                        DefOut.out.println(" " + link);
                }
                //------------
                int minDelay = -1;
                int maxDelay = -1;
                for(int lbDelay = 0; lbDelay < RpctDelays.LB_DELAY_MAX; lbDelay++) {
                    boolean allOK = true;
                    for(OptLink link:  synCoder.getUsedOptLinks()) {
                        if(link.isOK()) {
                            if(link.getLbOptoDealysMap()[lbDelay] < 0) {
                                allOK = false;
                            }
                        }
                    }
                    if(allOK && minDelay == -1)
                        minDelay = lbDelay;
                    if(allOK)
                        maxDelay = lbDelay;
                }
                
                if(minDelay != -1 && maxDelay != -1) {
                    int foundDelay = (maxDelay + minDelay)/2;
                    synCoder.setSendDelay(foundDelay);
                    
                    Set<Integer> lbDelaysInTower = lbDelaysInTowers.get(((LinkBoard)((HardwareDbChip) synCoder).getDbChip().getBoard()).getLinkBox().getTowerName());
                    if(lbDelaysInTower == null) {
                        lbDelaysInTower =  new TreeSet<Integer>();
                        lbDelaysInTowers.put(((LinkBoard)((HardwareDbChip) synCoder).getDbChip().getBoard()).getLinkBox().getTowerName(), lbDelaysInTower);                        
                    } 
                    
                    Set<Integer> lbDelaysInBox = lbDelaysInBoxes.get(((LinkBoard)((HardwareDbChip) synCoder).getDbChip().getBoard()).getLinkBox().getName());
                    if(lbDelaysInBox == null) {
                        lbDelaysInBox =  new TreeSet<Integer>();
                        lbDelaysInBoxes.put(((LinkBoard)((HardwareDbChip) synCoder).getDbChip().getBoard()).getLinkBox().getName(), lbDelaysInBox);                        
                    }
                    
                    for(OptLink link:  synCoder.getUsedOptLinks()) {
                        if(link.isOK()) {
                            link.setFoundDelay(link.getLbOptoDealysMap()[foundDelay]);
                                                                         
                            lbDelaysInTower.add(foundDelay + (int)link.getLbOptoDealysMap()[foundDelay]);
                            
                            lbDelaysInBox.add(foundDelay + (int)link.getLbOptoDealysMap()[foundDelay]);
                        }
                    }
                    DefOut.out.println("FindOptLinksDelay: " + synCoder.getFullName() + " found lbDelay " + foundDelay + "\n");                    
                }
                else {
                    DefOut.out.println("SENDER_DEALY not found for the " + synCoder.getFullName() + " !!!!!!!!!!!!!!!\n");
                }
            }
            
            for (HardwareDbOpto opto : optos) {
                opto.FindOptLinksDelayStep3a(failedLinks);
            }
            
            
            try {
                PrintStream failedLinsFile = new PrintStream(new FileOutputStream(System.getenv("HOME") + "/bin/out/failedLinks.txt", true));
                Date date = new Date();
                failedLinsFile.println(date);
                SortedSet<String> tcs = new TreeSet<String>();
                for(String str : failedLinks) {
                    failedLinsFile.println(str);
                    tcs.add(str.substring(str.indexOf("TC_"), str.indexOf("TC_") + 5));
                }
                
                for(String str : tcs) {
                    failedLinsFile.println(str);
                }
                failedLinsFile.println("");
                
            } catch (FileNotFoundException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }

        }
        
        
        for (OptLinkSender synCoder : senders) {
            // ((HardwareDbChip)synCoder).getHardwareChip().findByName("SEND.TEST_ENA").write(1);
            ((HardwareDbChip) synCoder).getHardwareChip().getItem("SEND.TEST_ENA").write(0);
            synCoder.enableTransmissionCheck(true, true);            
        }

        //for optos it is done it FindOptLinksDelayStep3
        //for (HardwareDbOpto opto : optos) {
            //opto.enableTransmissionCheck(true, true);            
            //opto.enableRecTransCheck(true, true); it is done it FindOptLinksDelayStep3
        //}
        
        DefOut.out.println("link Delay InTowers");
        for(Map.Entry<String, Set<Integer>> e : lbDelaysInTowers.entrySet()) {
            DefOut.out.println(e.getKey() + "\t" + e.getValue());
        }
        
        for(Map.Entry<String, Set<Integer>> e : lbDelaysInBoxes.entrySet()) {
            DefOut.out.println(e.getKey() + "\t" + e.getValue());
        }
    }
    
           
    private void synchronizeAutomatic() throws DataAccessException,
    RemoteException, ServiceException, HardwareDbException,
    InterruptedException 
    {        
        for (OptLinkSender synCoder : senders) {
            synCoder.reset();
            synCoder.enableTransmissionCheck(true, true);
        }

        for (HardwareDbOpto opto : optos) {            
            ((HardwareDbChip) opto).getHardwareChip().getItem("REC_TEST_ENA").write(0);
            ((HardwareDbChip) opto).getHardwareChip().getItem("REC_TEST_RND_ENA").write(0);            
            
            opto.enableRecTransCheck(true, true);
            opto.enableOptoLinks(true);
            
            ((HardwareDbChip) opto).getHardwareChip().getItem("STATUS.REC_SYNCH_REQ").write(opto.getUsedOptLinksInputsMask());
        }
       

        for (OptLinkSender synCoder : senders) {            
            synCoder.synchronizeLink();
        }
       
        //sparwdzanie, czy jak jest synchronizacja linkow (slanie 50bxc), to TLK jest we wlasciwym stanie
        for (HardwareDbOpto opto : optos) { 
             boolean good = true;
            ((HardwareDbChip) opto).getHardwareChip().getItem("TLK.RX_NO_DATA").read(); //
            ((HardwareDbChip) opto).getHardwareChip().getItem("TLK.RX_ERROR").read(); //
            long noData = ((HardwareDbChip) opto).getHardwareChip().getItem("TLK.RX_NO_DATA").read(); //
            long err = ((HardwareDbChip) opto).getHardwareChip().getItem("TLK.RX_ERROR").read(); //
            long synchAck = ((HardwareDbChip) opto).getHardwareChip().getItem("STATUS.REC_SYNCH_ACK").read();

            if( (opto.getUsedOptLinksInputsMask() & noData)  != opto.getUsedOptLinksInputsMask() ||
                    (opto.getUsedOptLinksInputsMask() & err) != 0 ||
               (opto.getUsedOptLinksInputsMask() & synchAck) != 0 ) {
                good = false;           
                System.out.println(opto.getBoard().getName() + " " + opto.getName()  + " CheckLinksOperation: TLK.RX_NO_DATA " + noData + 
                        " TLK.RX_ERR " + err + " usedLinksMask "+ opto.getUsedOptLinksInputsMask() + " NOT GOOD!!!!");
                
                DefOut.out.println(opto.getBoard().getName() + " " + opto.getName()  + " CheckLinksOperation: TLK.RX_NO_DATA " + noData + 
                        " TLK.RX_ERR " + err + " usedLinksMask "+ opto.getUsedOptLinksInputsMask() + " NOT GOOD!!!!");
            }
            
        }
        
        
        for (OptLinkSender synCoder : senders) {
            synCoder.normalLinkOperation();
        }

        for (HardwareDbOpto opto : optos) { 
            ((HardwareDbChip) opto).getHardwareChip().getItem("STATUS.REC_SYNCH_REQ").write(0); //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<!!!!!!!!!!!
            ((HardwareDbChip) opto).getHardwareChip().getItem("STATUS.REC_SYNCH_REQ").write(opto.getUsedOptLinksInputsMask());//!!!!!!!!!!!!!!!!!!!!!!!!
        }
        
        for (HardwareDbOpto opto : optos) {
            opto.CheckLinksOperation();
        }
                
        for (HardwareDbOpto opto : optos) { 
            for(OptLink optLink : opto.getUsedOptLinks() ) {      
                 ((HardwareDbChip)opto).getHardwareChip().getItem("REC_ERROR_COUNT").write(0);              
            }
        }
        Thread.sleep(1000);
        System.out.println("found delays");
        int totalLinks = 0;
        int notOKLinks = 0;
        for (HardwareDbOpto opto : optos) { 
            long fastDealy = ((HardwareDbChip) opto).getHardwareChip().getItem("VREC_FAST_DATA_DELAY.REC_FAST_DATA_DELAY").read();
            long dataDealy = ((HardwareDbChip) opto).getHardwareChip().getItem("VREC_DATA_DELAY.REC_DATA_DELAY").read();
            
            long synchAck = ((HardwareDbChip) opto).getHardwareChip().getItem("STATUS.REC_SYNCH_ACK").read();
            for(OptLink optLink : opto.getUsedOptLinks() ) {      
                long fastD = (fastDealy & (7 << (optLink.getOptoInputNum() * 3) ) ) >> (optLink.getOptoInputNum() * 3);
                long dataD = (dataDealy & (3 << (optLink.getOptoInputNum() * 2) ) ) >> (optLink.getOptoInputNum() * 2);
                
                ((rpct.xdaqaccess.DeviceItemWord)((HardwareDbChip)opto).getHardwareChip().getItem("REC_TEST_OR_DATA")).read(optLink.getOptoInputNum());
                long orData = ((rpct.xdaqaccess.DeviceItemWord)((HardwareDbChip)opto).getHardwareChip().getItem("REC_TEST_OR_DATA")).read(optLink.getOptoInputNum());
                String status = "";
                if(orData != 0 || (synchAck & (1 << optLink.getOptoInputNum()) ) == 0) {
                    status = "NOT OK !!!!!!!!!!!!!!!!";
                    notOKLinks++;
                }
                else
                    status = "OK"; 
                if(status == "NOT OK !!!!!!!!!!!!!!!!")
                    System.out.println(optLink.getSender().getFullName() + " - " + optLink.getOpto().getFullName() + " synchAck " + synchAck + " fastDealy " + fastD + " dataDealy " + dataD + " orData " + orData + " " +status);
                
                DefOut.out.println(optLink.getSender().getFullName() + " - " + optLink.getOpto().getFullName() + " synchAck " + synchAck + " fastDealy " + fastD + " dataDealy " + dataD + " orData " + orData + " " +status);
                
                totalLinks++;
            }
        }
        
        System.out.println("Failed links " + notOKLinks + " / " + totalLinks + " = " + ((double)notOKLinks) / totalLinks);
        DefOut.out.println("Failed links " + notOKLinks + " / " + totalLinks + " = " + ((double)notOKLinks) / totalLinks);
    }
    
    public void putFoudDelaysToConfDB(ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) throws DataAccessException {
        for (OptLinkSender synCoder : senders) {
            if(synCoder.getSendDelay() >=0 ) {
                StaticConfiguration configuration = configurationManager.getConfiguration(((HardwareDbSynCoder)synCoder).getDbChip(), configurationManager.getDefaultLocalConfigKey());
    
                if(configuration != null) {
                    SynCoderConf coderConf = (SynCoderConf) configuration;
                    coderConf.setOptLinkSendDelay(synCoder.getSendDelay());
                    configurationDAO.saveObject(coderConf);
                    System.out.println("the configuration updated for the " + synCoder.getFullName() + " synCoder.getSendDelay() " + synCoder.getSendDelay());
                }
            }
        }
    }
    
    public void testWithRnd() throws NumberFormatException, IOException, ServiceException, HardwareDbException, InterruptedException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("how long (s)? ");
        int testTime = Integer.parseInt(br.readLine());
        
        for (OptLinkSender synCoder : senders) {                       
            ((HardwareDbChip) synCoder).getHardwareChip().getItem("SEND.TEST_RND_ENA").write(1);
            ((HardwareDbChip) synCoder).getHardwareChip().getItem("SEND.TEST_ENA").write(1);
            synCoder.enableTransmissionCheck(true, true);
        }

        for (HardwareDbOpto opto : optos) {
            opto.enableRecTransCheck(true, true); 
            ((HardwareDbChip)opto).getHardwareChip().getItem("REC_TEST_ENA").write(7);
            ((HardwareDbChip) opto).getHardwareChip().getItem("REC_TEST_RND_ENA").write(opto.getUsedOptLinksInputsMask());
        }
        
        for (HardwareDbOpto opto : optos) {
            opto.resetRecErrorCnt();
            for(int iLikn = 0; iLikn < 3; iLikn++) {
                ((rpct.xdaqaccess.DeviceItemWord)((HardwareDbChip)opto).getHardwareChip().getItem("REC_TEST_OR_DATA")).read(iLikn);
            }
        }
        
        int checkCount = 10;
        for(int i = 0; i < checkCount; i++) {
            long wait = 1000 * testTime / checkCount;
            System.out.println("\nwaiting " + wait / 1000 + "s");
            Thread.sleep(wait);
            Date date = new Date();
            System.out.println(date.toString() + ":");
            for (HardwareDbOpto opto : optos) {
                long errCnt = ((HardwareDbChip)opto).getHardwareChip().getItem("REC_ERROR_COUNT").read();     
                
                String str;
                boolean notGood = false;
                if(errCnt != 0)
                    notGood = true;
                //System.out.print(opto.getFullName() + " - " + Long.toHexString(errCnt) + " ; orData: "); 
                str = opto.getFullName() + " - errCnt " + Long.toHexString(errCnt) + " ; orData: ";
                for(int iLikn = 0; iLikn < 3; iLikn++) {
                    long orData = ((rpct.xdaqaccess.DeviceItemWord)((HardwareDbChip)opto).getHardwareChip().getItem("REC_TEST_OR_DATA")).read(iLikn);
                    if((opto.getUsedOptLinksInputsMask() & (1 << iLikn)) != 0) {
                        //System.out.print(iLikn + " - " + Long.toHexString(orData) + ", ");
                        str += iLikn + " - " + Long.toHexString(orData) + ", ";
                        if(orData != 0)
                            notGood = true;
                    }
                    else {
                        //System.out.print(iLikn + " - " + "_" + ", ");
                        str += iLikn + " - " + "_" + ", ";
                    }
                }
                
                long err = ((HardwareDbChip)opto).getHardwareChip().getItem("TLK.RX_ERROR").read();
                if( (opto.getUsedOptLinksInputsMask() & err) != 0 ) {
                    str += " rxError " + (opto.getUsedOptLinksInputsMask() & err);
                    notGood = true;
                }
                
                if(notGood) {
                    System.out.println(str);
                }

            }
        }  
        
        for (HardwareDbOpto opto : optos) {
            opto.enableRecTransCheck(true, true);
            ((HardwareDbChip)opto).getHardwareChip().getItem("REC_TEST_ENA").write( ~(opto.getUsedOptLinksInputsMask()) & 7);
            ((HardwareDbChip) opto).getHardwareChip().getItem("REC_TEST_RND_ENA").write(0);           
        }
        
        for (OptLinkSender synCoder : senders) {                       
            ((HardwareDbChip) synCoder).getHardwareChip().getItem("SEND.TEST_RND_ENA").write(0);
            ((HardwareDbChip) synCoder).getHardwareChip().getItem("SEND.TEST_ENA").write(0);
        }
    }
    
    
    enum SynchroType {
        full,
        fast,
        authomatic,        
    }
    
    public static void synchronize(EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO, HardwareDbMapper dbMapper, OutputStream outstream) throws SynchronizationException, JAXBException {
        TestOptionsXml testOptionsXml = TestOptionsXml.readFromFile(System.getenv("HOME") + "/bin/config/optLinksSynchConf.xml");
        synchronize(SynchroType.fast, equipmentDAO, configurationManager, configurationDAO, dbMapper, outstream, testOptionsXml);
    }
    
    
    public static void synchronize(SynchroType synchroType, EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO, HardwareDbMapper dbMapper, OutputStream outstream, TestOptionsXml testOptionsXml) throws SynchronizationException {
        try {
            DefOut defLog= new DefOut(new PrintStream(outstream));
            OptLinksSynchronizer linksSynchronizer = new OptLinksSynchronizer();
            DefOut.out.println("synchroType " + synchroType);
            // hardware pieces selection
            Set<Board> linkTransm = testOptionsXml.getBoards("linkTransm", equipmentDAO, true);
            //List<Board> linkRec = testOptionsXml.getBoards("linkRec", equipmentDAO);
            Set<Board> linkRec = testOptionsXml.getBoards("linkRec", equipmentDAO, true);
            
            List<HardwareDBTriggerBoard> hdTbs = new ArrayList<HardwareDBTriggerBoard>();
            ArrayList<LinkConn> usedLinkConn = new ArrayList<LinkConn>();
            for(Board rec : linkRec) {
                if(rec.getDisabled() == null) {
                    HardwareDBTriggerBoard tbHdb = (HardwareDBTriggerBoard) dbMapper.getBoard(((TriggerBoard)rec));
                    hdTbs.add(tbHdb);
                    for (LinkConn linkConn : equipmentDAO.getLinkConns((TriggerBoard)rec, true)) {
                        if(linkTransm.contains(linkConn.getBoard())) {
                            linksSynchronizer.addLinkConn(linkConn, dbMapper);  
                            usedLinkConn.add(linkConn); 
                        }
                    }
                }
           }                       
            
            Date date = new Date();
            DefOut.out.println(date.toString());

            DefOut.out.println("\nAll links for that TBs");
            for (HardwareDBTriggerBoard hdTb : hdTbs) {
                DefOut.out.println(hdTb.getName());
                //for (LinkConn linkConn : hdTb.getDBTriggerBoard().getLinkConns()) {
                for (LinkConn linkConn : equipmentDAO.getLinkConns(hdTb.getDBTriggerBoard(), true)) {
                    DefOut.out.println("inputNum "
                            + linkConn.getTriggerBoardInputNum() + " "
                            + linkConn.getBoard().getName());
                }
            }


            DefOut.out.println("\nselected links, that will be synchronized");
            for(LinkConn linkConn : usedLinkConn) {
                DefOut.out.println(linkConn.getTriggerBoard().getName() + " inputNum "
                        + linkConn.getTriggerBoardInputNum() + " "
                        + linkConn.getSynCoder().getBoard().getName());
            }

            //disabling all links at first
            for (HardwareDBTriggerBoard hdTb : hdTbs) {
                for(HardwareDbOpto opto : hdTb.getOptos()) {
                    opto.enableOptoLinks(false);
                }
            }

            
/*            System.out.println("disabling LB inpunts");
            for (Board mlb : linkTransm) {                
                HardwareDbChip dbChip = dbMapper.getChip(((LinkBoard)mlb).getSynCoder());
                dbChip.getHardwareChip().getItem("CHAN_ENA").writeBinary(new Binary("0"));  ;
                
                for(LinkBoard slb  : ((LinkBoard)mlb).getSlaves()) {
                    HardwareDbChip dbChip1 = dbMapper.getChip(slb.getSynCoder());
                    dbChip1.getHardwareChip().getItem("CHAN_ENA").writeBinary(new Binary("0"));
                }
            }*/
            
            //checking, if the BC0 is ticking
            if(linksSynchronizer.getOptos().size() != 0) {
                HardwareDbOpto opto = linksSynchronizer.getOptos().get(0);
                DefOut.out.println("checking if the BC is ticking on " + opto.getFullName());
                long bcoCnt = opto.getHardwareChip().getItem("TEST_CNT.BCN0").read();
                while(bcoCnt == opto.getHardwareChip().getItem("TEST_CNT.BCN0").read()) {
                    DefOut.out.println("waitnig for BC0");
                    Thread.sleep(1000);
                }
            }
            
            
            // -------------------------------------
            long testData = 0x12345678;
            if(synchroType == SynchroType.authomatic) {
                linksSynchronizer.synchronizeAutomatic();  
            }
            else {
                linksSynchronizer.synchronize(testData, synchroType);
            }
            
            if(enableRandomTest) {
                char y_n = 'n';
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("start random test (y/n)? ");
                y_n = (char) br.read();                   
                if(y_n == 'y') {
                    linksSynchronizer.testWithRnd();
                }
            }
            
            if(synchroType == SynchroType.full) {
                char y_n = 'n';
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("put found delays to Conf DB (y/n)? ");
                y_n = (char) br.read();                   
                if(y_n == 'y') {
                    linksSynchronizer.putFoudDelaysToConfDB(configurationManager, configurationDAO);
                }
                
            }
                       
            for (HardwareDBTriggerBoard hdTb : hdTbs) {
                hdTb.getRmb().getHardwareChip().getItem("STATUS.RMB_RESET").write(1);
                Thread.sleep(10);
                hdTb.getRmb().getHardwareChip().getItem("STATUS.RMB_RESET").write(0);
                DefOut.out.println(hdTb.getRmb().getFullName() + " reseted");  
            }
            
            HashSet<HardwareDbTriggerCrate> tcs = new HashSet<HardwareDbTriggerCrate>();
            for (HardwareDBTriggerBoard hdTb : hdTbs) {
                for(HardwareDbChip chip : hdTb.getChips()) {          
                    ((HardwareDbChipImpl)chip).resetRecErrorCnt();
                }
                tcs.add((HardwareDbTriggerCrate)hdTb.getCrate());
            }
            for(HardwareDbTriggerCrate tc : tcs) {
                ((HardwareDbChipImpl)tc.getTcSort()).resetRecErrorCnt();
            }
            DefOut.out.println("error counters reseted");
        }
        catch (DataAccessException e) {
            throw new SynchronizationException(e);
        } catch (RemoteException e) {
            throw new SynchronizationException(e);
        } catch (ServiceException e) {
            throw new SynchronizationException(e);
        } catch (HardwareDbException e) {
            throw new SynchronizationException(e);
        } catch (InterruptedException e) {
            throw new SynchronizationException(e);
        } catch (IOException e) {
            throw new SynchronizationException(e);
        }
    }

    public List<OptLink> getOptLinks() {
        return optLinks;
    }

    public List<HardwareDbOpto> getOptos() {
        return optos;
    }
    
    public static void main(String[] args) throws IOException, DataAccessException {               
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(
                new HardwareRegistry(), context);
        
        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);
         
        FileOutputStream file = new FileOutputStream(System.getenv("HOME") + "/bin/out/optLinksSych.txt", false);                
        //optLinksSynchConf.xml"
        try { 
            TestOptionsXml testOptionsXml = TestOptionsXml.readFromFile(System.getenv("HOME") + "/bin/config/" + args[0]);
            OptLinksSynchronizer.enableRandomTest = true;
            //synchronize(equipmentDAO, dbMapper, file);
            SynchroType synchroType = SynchroType.authomatic;
            if(args.length > 1) {
                if(args[1].equals("full"))
                    synchroType = SynchroType.full;
                else if(args[1].equals("fast"))
                    synchroType = SynchroType.fast;
                else if(args[1].equals("authomatic"))
                    synchroType = SynchroType.authomatic;
                else 
                    System.out.println("wrong argument: 'synchronization type'. Defoult 'authomatic' will be executed");
            }
            synchronize(synchroType , equipmentDAO, configurationManager, configurationDAO, dbMapper, file, testOptionsXml);
        } 
        catch (SynchronizationException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }   
        catch (Exception e) {
            e.printStackTrace();            
            context.rollback();
            System.out.println("context.rollback()");
        } 
        finally {
            file.close();
            context.closeSession();
        }
    }

    public List<OptLinkSender> getSenders() {
        return senders;
    }


}
