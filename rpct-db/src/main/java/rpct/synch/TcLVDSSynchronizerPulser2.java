package rpct.synch;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

//import javax.xml.rpc.ServiceException;

import rpct.datastream.RpctDelays;
import rpct.datastream.TestPresets;
import rpct.datastream.TestPulsesGenerator;
import rpct.datastream.TestsManager;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.xdaq.XdaqException;
import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.diag.Pulser;
import rpct.xdaqaccess.hardwaredb.CrateWithLVDSLinks;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbCrate;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbOpto;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;
import rpct.xdaqaccess.synchro.DefOut;
import rpct.xdaqaccess.synchro.LVDSLink;
import rpct.xdaqaccess.synchro.LVDSLinksSynchronizer;
import rpct.xdaqaccess.synchro.LVDSTransmDevice;

public class TcLVDSSynchronizerPulser2 {

    public static void main(String[] args) throws DataAccessException {
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
        ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);

        String pulseXmlFileName =  System.getenv("HOME") + "/bin/data/gentestpulses.xml";
        TestPresets.TestPreset preset = new TestPresets.LongPulser_SnapshotNone(128, Long.MAX_VALUE);        
        //TestPresets.TestPreset preset = new TestPresets.ReapetPulserBC0_SnapshotL1A(3000);

        TestsManager testsManager = new TestsManager(pulseXmlFileName, preset, null, equipmentDAO, configurationManager, dbMapper);
        testsManager.setPulseTarget(TestsManager.PulserTarget.chipOut);
        try {             
            if(args.length == 0) {            
                String[] argsL = {//TODO
//                        "TC_0",
//                        "TC_1",
//                        "TC_2",
//                        "TC_3",
//                        "TC_4",
                        "TC_5",
//                        "TC_6",
//                        "TC_7",
//                        "TC_8",                        
//                        "TC_9",
//                        "TC_10",
//                        "TC_11",
//                		"PASTEURA_TC"
//                        "SC"
                	//	"TC_904"
                        };
                
                args = argsL;
            }
            
            //TODO
    		boolean synchronizePACs = true;
    		boolean synchronizeRMBs = true;
    		boolean synchronizeTTUs = false;
    		boolean synchronizeTBGBSs = false; //the pulser in the PAC is not implemented now
    		boolean synchronizeTCGBSs = true;

    		boolean scan = true;//TODO
    		
    		boolean interactive = true;
    		
    		//AddBoardsSettingsToDB.localConfigKey = ConfigurationManager.LOCAL_CONF_KEY_TCS_V1039; //TODO        	
    		//AddBoardsSettingsToDB.localConfigKey = ConfigurationManager.LOCAL_CONF_KEY_TCS_V1049; //TODO
    		AddBoardsSettingsToDB.localConfigKey = ConfigurationManager.LOCAL_CONF_KEY_TCS_V1049; //TODO
    		
    		
            String fileName = AddBoardsSettingsToDB.localConfigKey;
            System.out.println("Selected crates");
            
            List<HardwareDbCrate> crates = new ArrayList<HardwareDbCrate>();
            for(int i = 0; i < args.length; i++) {
                System.out.print(args[i] + " ");
                fileName = fileName + "-" + args[i];
                crates.add(dbMapper.getCrate(equipmentDAO.getCrateByName(args[i])));
            }
            
            String linksDataFileName = System.getenv("HOME") + "/bin/out/LVDSLinksSynchronizer_" +  fileName + ".ser"; //TODO !!!!!!!!!!!!!!!!!!
            //String linksDataFileName = System.getenv("HOME") + "/bin/out/LVDSLinksSynchronizer_TCS_V1049-TC_5-TC_9-TC_10.ser"; //TODO
            
            
            FileOutputStream file = new FileOutputStream(System.getenv("HOME") + "/bin/out/" + "puls" + fileName + ".txt", false);
            PrintStream prntSrm = new PrintStream(file);
            DefOut defLog= new DefOut(prntSrm);

            LVDSLinksSynchronizer tcLinksSynchronizer = new LVDSLinksSynchronizer(14, 8);                 
            
            List<Chip> trnasmChips = new ArrayList<Chip>();
                                    
            for (HardwareDbCrate hdbTc : crates) {
                for(LVDSLink link : ((CrateWithLVDSLinks)hdbTc).getLVDSLinks()) {
                	/*if(link.getTransmitter().getDevice().getBoard().getName().equals("TBn3_10") == false) { //TODO
                		continue;
                	}*/
                	//System.out.println("adding link " + link.getName());                	
                    if(synchronizePACs && link.getTransmitter().getDevice().getType().equals("TB3OPTO") &&
                                  link.getReciver().getDevice().getType().equals("TB3LDPAC") 	) { 
                    	
                    	HardwareDbBoard hardwareDbBoard = dbMapper.getBoard(equipmentDAO.getBoardById(link.getReciver().getDevice().getBoard().getId()));
                    	rpct.db.domain.equipment.TriggerBoard tb = (rpct.db.domain.equipment.TriggerBoard)(hardwareDbBoard.getDbBoard());
                    	
                    	if(tb.getLinkConn(link.getLinkNum()) != null)                    	                    
                    		tcLinksSynchronizer.addLVDSLink(link); 
                    	else {
                    		System.out.println(link.getName() + " skipped since the optical link is not used"); 
                    		//if there is no optical link, int he PAC firmware the reciver module is not generated
                    		//bur registers are still there, giving nonsese responce
                    		//however still therea are links which are coonected but not sued inside given PAC
                    	}
                    }
                    if(synchronizeRMBs && link.getTransmitter().getDevice().getType().equals("TB3OPTO") &&
                    		      link.getReciver().getDevice().getType().equals("TB3RMB") 	) {
                    	tcLinksSynchronizer.addLVDSLink(link); 
                    }
                    if(synchronizeTTUs && link.getTransmitter().getDevice().getType().equals(ChipType.TTUOPTO.getHwType())) {
                        tcLinksSynchronizer.addLVDSLink(link); 
                    }
                    if(synchronizeTBGBSs && link.getTransmitter().getDevice().getType().equals("TB3LDPAC")) {
                   		tcLinksSynchronizer.addLVDSLink(link);
                    }
                    if(synchronizeTCGBSs && link.getTransmitter().getDevice().getType().equals("TB3GBSORT")) {
                        tcLinksSynchronizer.addLVDSLink(link); 
                    }
                        
                }
                if(synchronizeTTUs)
                	trnasmChips.addAll(equipmentDAO.getChipsByTypeAndCrate(ChipType.TTUOPTO, hdbTc.getDbCrate(), true)); 
                if(synchronizePACs || synchronizeRMBs)
                	trnasmChips.addAll(equipmentDAO.getChipsByTypeAndCrate(ChipType.OPTO, hdbTc.getDbCrate(), true)); 
                if(synchronizeTBGBSs)
                	trnasmChips.addAll(equipmentDAO.getChipsByTypeAndCrate(ChipType.PAC, hdbTc.getDbCrate(), true));    
                if(synchronizeTCGBSs)
                	trnasmChips.addAll(equipmentDAO.getChipsByTypeAndCrate(ChipType.GBSORT, hdbTc.getDbCrate(), true));

            }
            
            testsManager.addPulsers(trnasmChips);
            
            
/*            for(int i = 0; i < 1000; i++) {                     
                for (LVDSTransmDevice reciver : tcLinksSynchronizer.getRecivers()) {
                    reciver.getDevice().getItem("REC_ERROR_COUNT").read(); //to generate VME access, which may disturb the transmission
                    reciver.getDevice().getItem("REC_TEST_OR_DATA").read();
                    reciver.getDevice().getItem("MEM_PULSE").read();
                } 
                Thread.sleep(6000/1000);
            }
            if(true)
            	return;*/

            //checking, if the BC0 is ticking
            if(true) {                
                if(trnasmChips.size() != 0) {
                    HardwareDbChip opto = dbMapper.getChip(trnasmChips.get(0));
                    long bcoCnt = opto.getHardwareChip().getItem("TEST_CNT.BCN0").read();
                    int i = 0;
                    while(bcoCnt == opto.getHardwareChip().getItem("TEST_CNT.BCN0").read()) {
                        System.out.println("waitnig for BC0");
                        Thread.sleep(100);
                        if(i > 100) {
                            throw new RuntimeException("the bc0 is not changing");
                        }
                        i++;
                    }
                }
            }
            
            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!TODO
            /*for (HardwareDbCrate hdbTc : crates) {
            	for(HardwareDbBoard board : hdbTc.getBoards()) {
            		for(HardwareDbChip chip : board.getChips()) {
            			if(chip.getDbChip().getType() == ChipType.RMB) {
            				chip.getHardwareChip().getItem("BCN0_DELAY").write(0x33);
            			}
            		}
            	}
            }*/
            
            if(false) //TODO
                tcLinksSynchronizer.checkInterconnections();

            if (true) { //TODO turn off or the synchronization process
                System.out.println("opto_pacsLinksSynchronizer.findSynchroParams()");
                //tcLinksSynchronizer.findSynchroParams();

                //tcLinksSynchronizer.scanSynchroParamsPulser3(testsManager, 4, 0);
                String[] testDatabuf = {     
                        new String("0101010101010101010101010101"),
                        new String("1010101010101010101010101010"),
                };
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("how many iterations shoould the progam do?");
                int finalParamsTuneRep = 7;
                int finalParamsTuneNoPulserRep = 3;
                if(interactive) 
                	finalParamsTuneRep = Integer.parseInt(br.readLine());
                                
                if(scan) {
                	System.out.println("will perform scanSynchroParamsWithStaticAndPulsers");
                	tcLinksSynchronizer.scanSynchroParamsWithStaticAndPulsers(testDatabuf, testsManager, false, finalParamsTuneRep);
                }
                else {
                	System.out.println("will read previously saved parametrs and perform only finalParametrsTuneOnNormalData");
                	tcLinksSynchronizer.loadLVDSLinksData(linksDataFileName);
                }
                tcLinksSynchronizer.finalParametrsTuneOnNormalData(false, finalParamsTuneNoPulserRep, 60000);
            }

            //defLog = new DefOut(System.out); //TODO choose where the output will be directed
            if (false) 
                tcLinksSynchronizer.randomPulsTest(testsManager, true, true, 10);//TODO choose the details o random test          
            
            if(false) {                
                char r_s = 'y';
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("read seting from hardware and store to the file (y/n)?");
                //r_s = (char) br.read();                             
                if (r_s == 'y') {
                    XMLParametrsWriter xmlParametrsWriter = new XMLParametrsWriter(equipmentDAO, dbMapper);                                
                          
                    for(HardwareDbCrate hdbTc : crates) {
                        String xmlFileName = System.getenv("HOME") + "/bin/out/" + hdbTc.getName() + ".xml";                        
                        //one crate to one file
                        List<HardwareDbCrate> cratesL = new ArrayList<HardwareDbCrate>();
                        cratesL.add(hdbTc);

                        xmlParametrsWriter.readParameters(cratesL, xmlFileName);
                    }
                } 
            }
            
            if(true) {                
                char r_s = 'y';
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                if(interactive)  {
                	System.out.println("read seting from hardware and store to the Database (y/n)?");
                	r_s = (char) br.read();
                }
                if (r_s == 'y') {
                	//RMB configuration //TODO
                	AddBoardsSettingsToDB.rmbDataDelay = RpctDelays.RMB_DATA_DELAY;        
                	AddBoardsSettingsToDB.chanEna = 0; //0 = all channels enable
                	AddBoardsSettingsToDB.preTriggerVal = RpctDelays.PRE_TRIGGER_VAL; //zakers BX branych do DAQ
                	AddBoardsSettingsToDB.postTriggerVal = RpctDelays.POST_TRIGGER_VAL; 
                	AddBoardsSettingsToDB.trgDelay = 0; 

                	AddBoardsSettingsToDB.bxOfCoincidence = 0; //TODO
                	AddBoardsSettingsToDB.pacEna = 0xfff;
                	AddBoardsSettingsToDB.pacConfigId = 0x1049; //TODO	
                	
                	AddBoardsSettingsToDB.addPACsettings = synchronizePACs; 
                	AddBoardsSettingsToDB.addRMBsettings = synchronizeRMBs;
                	AddBoardsSettingsToDB.addTBGBSsettings = synchronizeTBGBSs;
                	AddBoardsSettingsToDB.addTCGBSsettings = synchronizeTCGBSs;
                	//AddBoardsSettingsToDB.localConfigKey = ConfigurationManager.CURRENT_TCS_CONF_KEY;
                	
                    AddBoardsSettingsToDB.addBoardsSettingsToDB(crates, configurationDAO, configurationManager);
                } 
            }           
            
            char r_s = 'y';
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            if(interactive)  {
            	System.out.println("sarialise parameters into the file (y/n)?");
            	r_s = (char) br.read();
            }
            if (r_s == 'y') {
            	tcLinksSynchronizer.saveLVDSLinksData(linksDataFileName);
            	System.out.println("found parameters sarailised into the file " + linksDataFileName);
            }
            
            System.out.println("end");
        } 
        catch (MalformedURLException e) {
            e.printStackTrace();
        } 
        catch (XdaqException e) {
            e.printStackTrace();
        } 
        catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            context.closeSession();
        }
    }  
}
