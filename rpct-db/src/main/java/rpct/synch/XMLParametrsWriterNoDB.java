package rpct.synch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.Vector;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import rpct.xdaq.XdaqApplicationInfo;
import rpct.xdaqaccess.Board;
import rpct.xdaqaccess.Crate;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.synchro.LVDSTransmDevice;

public class XMLParametrsWriterNoDB {

    public XMLParametrsWriterNoDB() {

    }

    public static void main(String[] args) throws Exception {
        final File xmlSettingsFile = new File("./out/pasteura_TBB.xml");

        // Illustrate two methods to create JAXBContext for j2s binding.
        // (1) by root classes newInstance(Class ...)
        JAXBContext context1 = JAXBContext.newInstance(RpctSettingsXml.class);

        char r_s = 'r';
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("read seting from hardware (r) or set from xml (s)? ");
        r_s = (char) br.read();                     
        
        HardwareRegistry hardwareRegistry = new HardwareRegistry();
        hardwareRegistry.registerXdaqApplication(new XdaqApplicationInfo(new URL("http://pccms8.igf.fuw.edu.pl:1972"), "XdaqTestBench", 2));            
        
        List<Crate> crates = hardwareRegistry.getAllCrates();
                
        XmlDevice.setHardwareRegistry(hardwareRegistry);
        
        if (r_s == 'r') {
            System.out.println("reading settings from boards and saving them to the " + xmlSettingsFile);
            Marshaller m = context1.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            RpctSettingsXml setings = new RpctSettingsXml();
            
            for (Crate crate : crates) {
                System.out.println("Crate " + crate.getName());
                Device tcSorter = null;
                Vector<TriggerBoard> tbVec = new Vector<TriggerBoard>();
                for(Board board : crate.getBoards()) {	              	
                	if(board.getType().compareTo("TB3") == 0) {
                  		TriggerBoard tb = new TriggerBoard(board);
                  		tbVec.add(tb);
                  		
                        XmlBoard xmlTb = new XmlBoard(board);
                        
                        for (LVDSTransmDevice optoTB : tb.getOptoTBTab()) {
                            XmlDevice xmlOpto = new XmlDevice(optoTB.getDevice());                                                       
                            xmlOpto.addTabRegister(new XmlTabRegister("SEND_TEST_DATA", 3));                            
                            xmlTb.addXmlDevice(xmlOpto);
                        }
                        
                        for (LVDSTransmDevice pac : tb.getPacTab()) { 
                            XmlDevice xmlPac = new XmlDevice(pac.getDevice());
                            xmlPac.addRegister("USER_REG1", "1234");
                            xmlPac.addRegister("REC_MUX_CLK_INV", "");
                            xmlPac.addRegister("REC_MUX_REG_ADD", "");
                            xmlPac.addTabRegister(new XmlTabRegister("REC_MUX_DELAY", 18));
                            xmlPac.addTabRegister(new XmlTabRegister("REC_DATA_DELAY", 18));
                            
                            xmlTb.addXmlDevice(xmlPac); 
                        }
                        
                        if(tb.getRmb() != null) {
                        	XmlDevice xmlRmb = new XmlDevice(tb.getRmb().getDevice());
                        	xmlRmb.addRegister("USER_REG1", "1234");
                        	xmlRmb.addRegister("REC_MUX_CLK_INV", "");
                        	xmlRmb.addRegister("REC_MUX_REG_ADD", "");
                        	xmlRmb.addTabRegister(new XmlTabRegister("REC_MUX_DELAY", 18));
                        	xmlRmb.addTabRegister(new XmlTabRegister("REC_DATA_DELAY", 18));

                        	xmlTb.addXmlDevice(xmlRmb);
                        }
                        
                        if(tb.getTbSorter()!= null) {
	                        XmlDevice xmlTbSorter = new XmlDevice(tb.getTbSorter().getDevice());
	                        xmlTbSorter.addRegister("USER_REG1", "1234");
	                        xmlTbSorter.addRegister("REC_MUX_CLK_INV", "");
	                        xmlTbSorter.addRegister("REC_MUX_REG_ADD", "");
	                        xmlTbSorter.addTabRegister(new XmlTabRegister("REC_MUX_DELAY", 4));
	                        xmlTbSorter.addTabRegister(new XmlTabRegister("REC_DATA_DELAY", 4));
	                        
	                        xmlTb.addXmlDevice(xmlTbSorter);
	                    }
                        
                        setings.addBoard(xmlTb);
                  	}           
                	else if(board.getType().compareTo("TCSORT") == 0) { 
                		for(Device device : board.getDevices()) {
                			if(device.getType().compareTo("TCSORTTCSORT") == 0) {                		
                	            XmlBoard tcSortBoard = new XmlBoard(board);
                     	         
                	            XmlDevice xmlTcSorter = new XmlDevice(device);
                	            xmlTcSorter.addRegister("USER_REG1", "1234");
                	            xmlTcSorter.addRegister("REC_MUX_CLK_INV", "");
                	            xmlTcSorter.addRegister("REC_MUX_REG_ADD", "");
                	            xmlTcSorter.addTabRegister(new XmlTabRegister("REC_MUX_DELAY", 9));
                	            xmlTcSorter.addTabRegister(new XmlTabRegister("REC_DATA_DELAY", 9));
                	           
                	            tcSortBoard.addXmlDevice(xmlTcSorter);    
                	            setings.addBoard(tcSortBoard);
                				break;
                			}	
                		}	              		
                  	} 
                }                
            }           
            setings.readRegisters();

            m.marshal(setings, new FileOutputStream(xmlSettingsFile));
            System.out.println("end");
        } 
        else if (r_s == 's') {
            System.out.println("applaying settings from the " + xmlSettingsFile);
            Unmarshaller um = context1.createUnmarshaller();
            RpctSettingsXml setings = (RpctSettingsXml) um.unmarshal(xmlSettingsFile);
            
            Marshaller m = context1.createMarshaller();
            m.marshal(setings, System.out);
            System.out.println("");
            
            setings.aplyRegisters();
            System.out.println("end");
        }
    }
}
