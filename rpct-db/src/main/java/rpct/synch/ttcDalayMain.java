package rpct.synch;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqApplicationInfo;
import rpct.xdaqaccess.Board;
import rpct.xdaqaccess.Crate;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;

public class ttcDalayMain {

    /*	static List<Crate> getCratesNoDB() throws MalformedURLException, RemoteException {
		HardwareRegistry hardwareRegistry = new HardwareRegistry();
		hardwareRegistry.registerXdaqApplication(new XdaqApplicationInfo(new URL("http://pccms8.igf.fuw.edu.pl:1972"), "XdaqTestBench", 2));            

		List<Crate> crates = hardwareRegistry.getAllCrates();

		return crates;
	}*/

    static List<Crate> getCratesDB() throws DataAccessException, RemoteException, ServiceException {
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);        	                   

        List<Crate> crates = new ArrayList<Crate>();

        crates.add(dbMapper.getCrate(equipmentDAO.getCrateByName("PASTEURA_TC")).getHardwareCrate());

        return crates;
    }

    public static void findTTCDelays(List<Crate> crates) throws RemoteException, ServiceException {
        long ttcTestData = 0x25;

        Map<String, ArrayList<Integer> > tccDelays = new TreeMap<String, ArrayList<Integer>>();

        for (Crate crate : crates) {
            System.out.println("Crate " + crate.getName());
            for (Board tcBoard : crate.getBoards()) {
                if (tcBoard.getType().equals("TCSORT")) {
                    for (Device device : tcBoard.getDevices()) {
                        if (device.getType().equals("TCSORTTCSORT") == true) {
                            device.getItem("TTC.TEST_ENA").write(0x1);
                            device.getItem("TTC_TEST_DATA").write(ttcTestData);
                        }
                    }
                }
            }

            for (Board tb : crate.getBoards()) {
                if (tb.getType().equals("TB3")) {
                    for (Device device : tb.getDevices()) {
                        if (device.getType().equals("TB3CONTROL") == true) {																			
                            long readTtcTestData = device.getItem("TTC_TC_TEST_DATA").read();

                            if (readTtcTestData != ttcTestData) {
                                System.out.println(device.getBoard().getName() + " " + device.getName() + " ttcTestData " + Long.toHexString(readTtcTestData));

                            } else {
                                System.out.println(device.getBoard().getName() + " " + device.getName() + " OK ");
                            }
                        }
                    }

                    for (Device device : tb.getDevices()) {
                        if (device.getType().equals("TB3CONTROL") == false && device.getType().equals("TB3VME") == false ) {
                            System.out.println(device.getBoard().getName() + " " + device.getName() );

                            ArrayList<Integer> delayValus = tccDelays.get(device.getType());
                            if(delayValus == null) {
                                delayValus = new ArrayList<Integer>();
                                for(int i = 0; i < 8; i++)
                                    delayValus.add(1);
                                tccDelays.put(device.getType(), delayValus);
                            }

                            for(long delay = 0; delay < 8; delay++) {
                                device.getItem("STATUS.TTC_DATA_DELAY").write(delay);

                                long readTtcTestData = device.getItem("TTC_TEST_DATA").read();

                                System.out.print(delay + " - " + Long.toHexString(readTtcTestData) );
                                if (readTtcTestData != ttcTestData) {
                                    System.out.println(" !!!!!!!");
                                    delayValus.set((int)delay, 0);
                                } else {
                                    System.out.println(" OK");
                                }
                            }
                        }
                    }
                }
            }

            System.out.println("The delays now set in the FixedHardwareSettings");
            System.out.println("TBCONTROL_TTC_DATA_DELAY = 7;");
            System.out.println("OPTO_TTC_DATA_DELAY = 3;");
            System.out.println("PAC_TTC_DATA_DELAY = 2;");
            System.out.println("RMB_TTC_DATA_DELAY = 3;	");
            System.out.println("TBSORT_TTC_DATA_DELAY = 3;");

            for (Board tcBoard : crate.getBoards()) {
                if (tcBoard.getType().equals("TCSORT")) {
                    for (Device device : tcBoard.getDevices()) {
                        if (device.getType().equals("TCSORTTCSORT") == true) {
                            device.getItem("TTC.TEST_ENA").write(0x0);
                        }
                    }
                }
            }

            //for(String deviceType ; )
            System.out.println(tccDelays);
        }
    }

    public static void findTTCDelaysControl(List<Crate> crates) throws RemoteException, ServiceException {
        long ttcTestData[] = {
                0x1,
                0x2,
                0x4,
                0x8,
                0x10,
                0x20,
                0x12,
                0x9,
                0x2d,
                0x25,
                0x3f,
        };

        Map<String, ArrayList<Integer> > tccDelays = new TreeMap<String, ArrayList<Integer>>();

        ArrayList<Integer> delayValus = tccDelays.get("TB3CONTROL");
        if(delayValus == null) {
            delayValus = new ArrayList<Integer>();
            for(int i = 0; i < 8; i++)
                delayValus.add(1);
            tccDelays.put("TB3CONTROL", delayValus);
        }

        for(long testData : ttcTestData) {
            System.out.println("\ntestData " + Long.toHexString(testData));
            for (Crate crate : crates) {
                System.out.println("Crate " + crate.getName());
                for (Board tcBoard : crate.getBoards()) {
                    if (tcBoard.getType().equals("TCSORT")) {
                        for (Device device : tcBoard.getDevices()) {
                            if (device.getType().equals("TCSORTTCSORT") == true) {
                                device.getItem("TTC.TEST_ENA").write(0x1);
                                device.getItem("TTC_TEST_DATA").write(testData);
                            }
                        }
                    }
                }
                for (Board tb : crate.getBoards()) {
                    if (tb.getType().equals("TB3")) {
                        for (Device device : tb.getDevices()) {
                            if (device.getType().equals("TB3CONTROL") == true) {                                                                                                        
                                System.out.println(device.getBoard().getName() + " " + device.getName() );
                                for(long delay = 0; delay < 8; delay++) {
                                    device.getItem("TTC.TC_DELAY").write(delay);

                                    long readTtcTestData = device.getItem("TTC_TC_TEST_DATA").read();

                                    System.out.print(delay + " - " + Long.toHexString(readTtcTestData) );
                                    if (readTtcTestData != testData) {
                                        System.out.println(" !!!!!!!");
                                        delayValus.set((int)delay, 0);
                                    } else {
                                        System.out.println(" OK");
                                    }
                                }

                            }
                        }  
                       // break; //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<TODO
                    }
                }
                
                for (Board tcBoard : crate.getBoards()) {
                    if (tcBoard.getType().equals("TCSORT")) {
                        for (Device device : tcBoard.getDevices()) {
                            if (device.getType().equals("TCSORTTCSORT") == true) {
                                device.getItem("TTC.TEST_ENA").write(0x0);
                            }
                        }
                    }
                }
            }
        }
        System.out.println(tccDelays);
    }

    public static void findTTCDelaysFromCopntrol(List<Crate> crates, long ttcTestData) throws RemoteException, ServiceException {        

        Map<String, ArrayList<Integer> > tccDelays = new TreeMap<String, ArrayList<Integer>>();

        for (Crate crate : crates) {
            System.out.println("Crate " + crate.getName());

            for (Board tb : crate.getBoards()) {
                if (tb.getType().equals("TB3")) {
                    for (Device device : tb.getDevices()) {
                        if (device.getType().equals("TB3CONTROL") == true) {         
                            device.getItem("TTC.TEST_ENA").write(0x1);
                            device.getItem("TTC_TEST_DATA").write(ttcTestData);                                                       
                        }
                    }

                    for (Device device : tb.getDevices()) {
                        if (device.getType().equals("TB3CONTROL") == false && device.getType().equals("TB3VME") == false ) {
                            System.out.println(device.getBoard().getName() + " " + device.getName() );

                            ArrayList<Integer> delayValus = tccDelays.get(device.getType());
                            if(delayValus == null) {
                                delayValus = new ArrayList<Integer>();
                                for(int i = 0; i < 8; i++)
                                    delayValus.add(1);
                                tccDelays.put(device.getType(), delayValus);
                            }

                            for(long delay = 0; delay < 8; delay++) {
                                device.getItem("STATUS.TTC_DATA_DELAY").write(delay);

                                long readTtcTestData = device.getItem("TTC_TEST_DATA").read();

                                System.out.print(delay + " - " + Long.toHexString(readTtcTestData) );
                                if (readTtcTestData != ttcTestData) {
                                    System.out.println(" !!!!!!!");
                                    delayValus.set((int)delay, 0);
                                } else {
                                    System.out.println(" OK");
                                }
                            }
                        }
                    }
                }
            }

            System.out.println("The delays now set in the FixedHardwareSettings");
            System.out.println("TBCONTROL_TTC_DATA_DELAY = 7;");
            System.out.println("OPTO_TTC_DATA_DELAY = 3;");
            System.out.println("PAC_TTC_DATA_DELAY = 3;");
            System.out.println("RMB_TTC_DATA_DELAY = 3; ");
            System.out.println("TBSORT_TTC_DATA_DELAY = 3;");

            for (Board tcBoard : crate.getBoards()) {
                if (tcBoard.getType().equals("TCSORT")) {
                    for (Device device : tcBoard.getDevices()) {
                        if (device.getType().equals("TCSORTTCSORT") == true) {
                            device.getItem("TTC.TEST_ENA").write(0x0);
                        }
                    }
                }
            }

            //for(String deviceType ; )
            System.out.println(tccDelays);
        }
    }

    public static void main(String[] args) {
        try {	        	                    
            List<Crate> crates = getCratesDB();
            //findTTCDelaysControl(crates);
            findTTCDelays(crates);
            
            //findTTCDelaysFromCopntrol(crates, 0x20);

        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}
