package rpct.synch;
import java.rmi.RemoteException;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.rpc.ServiceException;

import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.Device;

@XmlType 
public class XmlRegister {
    @XmlAttribute
    private String regName;    
    @XmlAttribute
    private String regValue;
    
    public XmlRegister() {};
    
    public XmlRegister(String regName, String regValue) {        
        this.regName = regName;
        this.regValue = regValue;
    }

    void readRegister(Device device) throws RemoteException, ServiceException {
        Binary b = device.getItem(regName).readBinary();
        regValue = b.toString();
    }    
    
    void aplyRegister(Device device) throws RemoteException, ServiceException {
        device.getItem(regName).writeBinary(new Binary(regValue));
    }
}
