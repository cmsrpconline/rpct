package rpct.synch;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Vector;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;

@XmlType
public class XmlDevice {
	private static HardwareRegistry hardwareRegistry ;
	private static HardwareDbMapper dbMapper;
	private static EquipmentDAO equipmentDAO;
	
    @XmlAttribute
    private String name;
    
    @XmlAttribute
    private int id;

    private List<XmlRegister> registers;
    
    private List<XmlTabRegister> tabRegisters;
    
    public XmlDevice() {};
    
    public XmlDevice(String name, int id) {
        this.id = id;
        this.name = name;
        
        registers =  new Vector<XmlRegister>();
        tabRegisters  =  new Vector<XmlTabRegister>();
    } 

    public XmlDevice(Device device) {
        this.id = device.getId();
        this.name = device.getName();
        
        registers =  new Vector<XmlRegister>();
        tabRegisters  =  new Vector<XmlTabRegister>();   
    }
    
    public XmlDevice(Device device, HardwareRegistry hardwareRegistry_) {
        this.id = device.getId();
        this.name = device.getName();
        
        registers =  new Vector<XmlRegister>();
        tabRegisters  =  new Vector<XmlTabRegister>();   
        hardwareRegistry = hardwareRegistry_;
    }
    
/*    public XmlDevice(Device device){
        this.id = device.getId();
        this.name = device.getName();
        
        registers =  new Vector<XmlRegister>();
        tabRegisters  =  new Vector<XmlTabRegister>();        
    }*/
    
    @XmlElement(name="register")
    public List<XmlRegister>  getRegisters() {
        return registers;
    }

    public void setRegisters(List<XmlRegister>  registers) {
        this.registers = registers;
    }
    
    public void addRegister(XmlRegister  register) {
        this.registers.add(register);
    }
    
    public void addRegister(String regName, String regValue) {
        this.registers.add(new XmlRegister(regName, regValue));
    }
        
    @XmlElement(name="tabRegister")
    public List<XmlTabRegister> getTabRegisters() {
        return tabRegisters;
    }

    public void setTabRegisters(List<XmlTabRegister> tabRegisters) {
        this.tabRegisters = tabRegisters;
    }
    
    public void addTabRegister(XmlTabRegister  tabRegister) {
        tabRegisters.add(tabRegister);
    }
    
    void readRegisters() throws XdaqException, RemoteException, ServiceException {       
        Device device = (Device) hardwareRegistry.findDeviceById(id);
        //Device device = dbMapper.getChip((Chip)equipmentDAO.getObject(Chip.class, id)).getHardwareChip();;
        if(device == null)
            throw new IllegalArgumentException("No device found with id " );
        
        for (XmlRegister register : registers) {
            register.readRegister(device);
        }
        
        for (XmlTabRegister tabRegister : tabRegisters) {
            tabRegister.readRegister(device);
        }
    }
    
    void aplyRegisters() throws XdaqException, RemoteException, ServiceException, DataAccessException {
        //Device device = (Device) hardwareRegistry.findDeviceById(id);
        Device device = dbMapper.getChip((Chip)equipmentDAO.getObject(Chip.class, id)).getHardwareChip();;
        if(device == null) {
            //throw new IllegalArgumentException("No device found with id " + id);
            System.out.println("No device found with id " + id + "settings are not applied");
            return;
        }
        if(registers != null)
            for (XmlRegister register : registers) {
                register.aplyRegister(device);
            }
        
        if(tabRegisters != null)
            for (XmlTabRegister tabRegister : tabRegisters) {
                tabRegister.aplyRegister(device);
            }
    }

	public static void setHardwareRegistry(HardwareRegistry hardwareRegistry) {
		XmlDevice.hardwareRegistry = hardwareRegistry;
	}
	
	public static void setEquipmentDAOandDbMapper(HardwareDbMapper dbMapper, EquipmentDAO equipmentDAO) {
	    XmlDevice.dbMapper = dbMapper;
	    XmlDevice.equipmentDAO = equipmentDAO;
	}

}
