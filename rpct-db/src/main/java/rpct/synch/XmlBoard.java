package rpct.synch;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Vector;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.Board;

@XmlType
public class XmlBoard {
    @XmlAttribute
    private String name;
    
    @XmlAttribute
    private int id;
    
    private List<XmlDevice> xmlDevices;
    
    public XmlBoard() {};
    
    public XmlBoard(String name, int id) {
        this.id = id;
        this.name = name;
        
        xmlDevices =  new Vector<XmlDevice>();
    }
    
    public XmlBoard(Board board) {
        this.id = board.getId();
        this.name = board.getName();
        
        xmlDevices =  new Vector<XmlDevice>();
    }
    
    @XmlElement(name="device")
    public List<XmlDevice>  getXmlDevices() {
        return xmlDevices;
    }

    public void setXmlDevices(List<XmlDevice>  xmlDevices) {
        this.xmlDevices = xmlDevices;
    }
    
    public void addXmlDevice(XmlDevice xmlDevices) {
        this.xmlDevices.add(xmlDevices);
    }
    
    void readDevices() throws XdaqException, RemoteException, ServiceException {       
        for (XmlDevice xmlDevice : xmlDevices) {
            xmlDevice.readRegisters();
        }
    }
    
    void aplyDevices() throws XdaqException, RemoteException, ServiceException, DataAccessException {
        for (XmlDevice xmlDevice : xmlDevices) {
            xmlDevice.aplyRegisters();
        }
    }
}
