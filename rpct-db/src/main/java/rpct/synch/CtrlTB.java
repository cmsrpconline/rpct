package rpct.synch;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import rpct.xdaqaccess.Device;


public class CtrlTB {
    static void initCtrl(Device ctrl) throws InterruptedException, RemoteException, ServiceException {
        ctrl.getItem("STATUS.TTC_RESET").write(1);
        ctrl.getItem("STATUS.TTC_RESET").write(0);
        ctrl.getItem("QPLL.CLK_LOCK").write(0);
        ctrl.getItem("QPLL.SELECT").write(0x30);
        ctrl.getItem("QPLL.MODE").write(1);
        ctrl.getItem("QPLL.CTRL").write(0); 
        Thread.sleep(500);
        long qpll_locked2 = ctrl.getItem("QPLL.LOCKED").read();
        
        if (qpll_locked2 == 1){
            System.out.println("      ---------- QPLL TB IS LOCK ---------    ");
        }
    }

        
}


