package rpct.synch;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.rpc.ServiceException;

import rpct.datastream.RpctDelays;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.PacConf;
import rpct.db.domain.configuration.RmbConf;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.configuration.TcSortConf;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.CrateWithLVDSLinks;
import rpct.xdaqaccess.hardwaredb.HardwareDbCrate;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbMuxRecChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;

public class AddBoardsSettingsToDB {    
	//RMB configuration //TODO
	static int rmbDataDelay = RpctDelays.RMB_DATA_DELAY;        
	static int chanEna = 0; //0 = all channels enable
	static int preTriggerVal = RpctDelays.PRE_TRIGGER_VAL; //zakers BX branych do DAQ
	static int postTriggerVal = RpctDelays.POST_TRIGGER_VAL; 
	static int trgDelay = 0; 

	static int bxOfCoincidence = 0; //TODO
	static int pacEna = 0xfff;
	static int pacConfigId = 0x1036; //TODO	
	
	static boolean addPACsettings = true; //TODO
	static boolean addRMBsettings = true; //TODO
	static boolean addTBGBSsettings = true; //TODO
	static boolean addTCGBSsettings = true; //TODO

	//static String localConfigKey = ConfigurationManager.LOCAL_CONF_KEY_TCS_V1036; //TODO
	static String localConfigKey = ConfigurationManager.CURRENT_TCS_CONF_KEY; //TODO
	
	public static void addBoardsSettingsToDB(List<HardwareDbCrate> crates, ConfigurationDAO configurationDAO, ConfigurationManager configurationManager ) throws RemoteException, DataAccessException, HardwareDbException, ServiceException {
		String bootScript = "boot_lhc_v" + Integer.toHexString(pacConfigId) + ".sh"; //TODO
		for(HardwareDbCrate tcHdb : crates) {   
			for(HardwareDbMuxRecChip chip : ((CrateWithLVDSLinks)tcHdb).getMuxRecChips()) {
				if( (chip.getDbChip().getType() == ChipType.PAC && addPACsettings) || 
						(chip.getDbChip().getType() == ChipType.RMB && addRMBsettings) ||
						(chip.getDbChip().getType() == ChipType.GBSORT && addTBGBSsettings) ||
						(chip.getDbChip().getType() == ChipType.TCSORT && addTCGBSsettings) ) {

					StaticConfiguration  conf = ((HardwareDbMuxRecChip)chip).createConfigurationFromCurrentSettings();                        
					if(RmbConf.class.isInstance(conf) ) {
						((RmbConf)conf).setDataDelay(rmbDataDelay);
						((RmbConf)conf).setChanEna(chanEna);
						((RmbConf)conf).setPreTriggerVal(preTriggerVal);
						((RmbConf)conf).setPostTriggerVal(postTriggerVal);
						((RmbConf)conf).setTrgDelay(trgDelay);
					}  
					else if(PacConf.class.isInstance(conf) ) {
						((PacConf)conf).setBxOfCoincidence(bxOfCoincidence);
						((PacConf)conf).setPacEna(pacEna);
						((PacConf)conf).setPacConfigId(pacConfigId);
					}
					else if(TcSortConf.class.isInstance(conf) ) {
						((TcSortConf)conf).setBootScript(bootScript);
					}

					configurationDAO.saveObject(conf);
                    
					configurationManager.assignConfiguration(chip.getDbChip(), conf, 
							 configurationManager.getLocalConfigKey(localConfigKey));
					 System.out.println("the configuration " + localConfigKey + " was added for the " + chip.getFullName());
				}
			}
		}   
    }
	public static void main(String[] args) throws DataAccessException {
		if(args.length == 0) {            
			String[] argsL = {
					//"TC_8",
					"TC_0",                    
					"TC_1",
					"TC_2",                   
					"TC_3",                    
					"TC_4",
					"TC_5",                    
					"TC_6",                    
					"TC_7",
					"TC_8",                   
					"TC_9",                    
					"TC_10",
					"TC_11",
//					"SC"
					//"PASTEURA_TC"
			};

			args = argsL;
		}

		
		
		System.out.println("Selected crates");
		for(int i = 0; i < args.length; i++) {
			System.out.print(args[i] + " ");
		}

		HibernateContext context = new SimpleHibernateContextImpl();
		EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
		HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
		ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
		ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);   	

		try {
			List<HardwareDbCrate> crates = new ArrayList<HardwareDbCrate>();
			for(int i = 0; i < args.length; i++) {                
				crates.add(dbMapper.getCrate(equipmentDAO.getCrateByName(args[i])));
			}

			
			addBoardsSettingsToDB(crates, configurationDAO, configurationManager);
			//configurationManger.getLocalConfigKey(localConfigKey).getName();
			//configurationManger.assignConfigKey(localConfigKey, localConfigKey);
		} catch (DataAccessException e) {
			e.printStackTrace();
			context.rollback();
		} catch (RemoteException e) {
			e.printStackTrace();
			context.rollback();
		} catch (ServiceException e) { 
			e.printStackTrace();
			context.rollback();
		} catch (HardwareDbException e) {			
			e.printStackTrace();
		}
		finally {
			context.closeSession();
		}
	}

}
