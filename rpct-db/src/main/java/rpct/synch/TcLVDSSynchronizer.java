package rpct.synch;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;
import rpct.xdaqaccess.synchro.DefOut;
import rpct.xdaqaccess.synchro.LVDSLink;
import rpct.xdaqaccess.synchro.LVDSLinksSynchronizer;

public class TcLVDSSynchronizer {         
    public static void main(String[] args) throws DataAccessException {
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
        ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);
        try {             
            String tcName = "TC_10";

            FileOutputStream file = new FileOutputStream(System.getenv("HOME") + "/bin/out/" + tcName + ".txt", false);
            PrintStream prntSrm = new PrintStream(file);
            DefOut defLog= new DefOut(prntSrm);

            LVDSLinksSynchronizer tcLinksSynchronizer = new LVDSLinksSynchronizer(14, 8);           
            List<HardwareDbTriggerCrate> crates = new ArrayList<HardwareDbTriggerCrate>();

            TriggerCrate crate = (TriggerCrate) equipmentDAO.getCrateByName(tcName);  
            HardwareDbTriggerCrate hdbTc = (HardwareDbTriggerCrate)dbMapper.getCrate(crate);
            crates.add(hdbTc);

            Board tb = equipmentDAO.getBoardByName("TBp2_9");
            HardwareDBTriggerBoard tbHdb = (HardwareDBTriggerBoard)dbMapper.getBoard(tb);

            
            List<LVDSLink> links = tbHdb.getLVDSLinks();
            List<LVDSLink> linksSelected = new ArrayList<LVDSLink>();
            for(LVDSLink link : links) {
                if(link.getTransmitter().getDevice().getName().equals("PAC 8"))
                //if(link.getReciver().getDevice().getType().equals("TB3LDPAC"))
                    linksSelected.add(link);
            }

            tcLinksSynchronizer.addLVDSLinks(linksSelected); //TODO
            //tcLinksSynchronizer.addLVDSLinks(hdbTc.getLVDSLinks()); //TODO
            //tcLinksSynchronizer.addLVDSLinks(tbHdb.getLVDSLinks());
            //tcLinksSynchronizer.addLVDSLinks(hdbTc.getHardwareDBTriggerBoards().get(0).getLVDSLinks());

            if(false) //TODO
                tcLinksSynchronizer.checkInterconnections();

            if (true) { //TODO turn off or the synchronization process
                System.out.println("opto_pacsLinksSynchronizer.findSynchroParams()");
                //tcLinksSynchronizer.findSynchroParams();

                tcLinksSynchronizer.scanSynchroParams();

                long wait = 1000 * 60;
                DefOut.out.println("waiting " + wait / 1000 + "s");
                Thread.sleep(wait);
                System.out.println("opto_pacsLinksSynchronizer.finalParametrsTune()");
                tcLinksSynchronizer.finalParametrsTune(true);
                for (int i = 0; i < 2; i++) {
                    wait = 1000 * 60;
                    DefOut.out.println("waiting " + wait / 1000 + "s");
                    Thread.sleep(wait);
                    System.out.println("opto_pacsLinksSynchronizer.finalParametrsTune()");
                    tcLinksSynchronizer.finalParametrsTune(true);
                }
                tcLinksSynchronizer.printParams();
            }

            //defLog = new DefOut(System.out); //TODO choose where the output will be directed
            if (true) 
                tcLinksSynchronizer.randomTest(LVDSLinksSynchronizer.TestType.off, true, true, 100);//TODO choose the details o random test          

/*            if(false) {                
                String xmlFileName = tcName + ".xml";
                char r_s = 'r';
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("read seting from hardware and store to the file (y/n)?");
                r_s = (char) br.read();                             
                if (r_s == 'y') {
                    XMLParametrsWriter xmlParametrsWriter = new XMLParametrsWriter(equipmentDAO, dbMapper);                                
                    xmlParametrsWriter.readParameters(crates, xmlFileName);                       
                } 
            }*/
            System.out.println("end");
        } 
        catch (MalformedURLException e) {
            e.printStackTrace();
        } 
        catch (XdaqException e) {
            e.printStackTrace();
        } 
        catch (IOException e) {
            e.printStackTrace();
        } catch (ServiceException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            context.closeSession();
        }
    }  
}
