package rpct.synch;

import java.util.HashMap;
import java.util.Map;

import rpct.db.domain.equipment.ChipType;

/**
 * @author rpcdev
 *
 */
public class LVDSLinkDef {
    private int bitsPerLine = 8;
    private int dataBitsCnt; //excluding check bits
    private int linesCnt;
    private int checkBitsCnt;
    int[] dataBitsCntForLine;
    int[] dataBitsOffsetForLine;    

    private static int div(int v, int r){
        int res = v/r;
        if ((v-res*r)<((res+1)*r-v)) return(res);
        return (res+1);
    }
    
    /* splot is a group of lines which have common check bit(s) 
     * check bit is on the last line of a splot
     * in a splot lines are enumerated in the reverse order
     * the index of line in splot is sLine, the index of line in link is iLine
     * if sLine and iLine corresponds to the same line, the line is "active" (lineIsActive())
     */
    private int splotsCnt;
    
    //most of the lines
    private int linesPerSplot;
    private int checkBitsPerLine;    
    //last lines (last group)
    private int linesInLastSplot;
    private int checkBitsInLastLine;
    
    static int maxLinesInSplot;
    
    public LVDSLinkDef(int linesCnt, int dataBitsCnt, int checkBitsCnt, int bitsPerLine) {
        this.linesCnt = linesCnt;
        this.checkBitsCnt = checkBitsCnt;
                              
        this.dataBitsCnt = dataBitsCnt;
        this.bitsPerLine =  bitsPerLine;
        
        linesPerSplot = Math.max(div(linesCnt, checkBitsCnt),1);
        checkBitsPerLine = Math.max(div(checkBitsCnt,linesCnt),1);
        splotsCnt = Math.min(checkBitsCnt/checkBitsPerLine, Math.min(((linesCnt-1)/linesPerSplot+1),checkBitsCnt)-1) + 1;
        linesInLastSplot = linesCnt- (splotsCnt -1) * linesPerSplot;
        checkBitsInLastLine = checkBitsCnt- (splotsCnt -1) *checkBitsPerLine;
        System.out.println  ("linesCnt= " +linesCnt+ ", checkBitsCnt = " + checkBitsCnt + ", linesPerSplot=" + linesPerSplot+ ", checkBitsPerLine="+ checkBitsPerLine);
        System.out.println  (", splotsCnt= " + splotsCnt + ", linesInLastSplot = " + linesInLastSplot + ", checkBitsInLastLine = " + checkBitsInLastLine);
       
        //System.out.println(", check="+(checkBitsPerLine*splotsCnt+checkBitsInLastLine)+",part="+(linesPerSplot*splotsCnt+linesInLastSplot));

        //check_size - liczba bitow CHECK_DATA
        //part_num - liczba partycji (liczba lini)
        /*
        1) pierwszy ciag podzialu:
            check_part_num - liczba partycji przypadajacych na jeden bit CHECK
            check_part_size - liczba bitow CHECK przypadajaca na jedna partycje
            check_part_rep - liczba podzialow

            2) ostatni podzial (resztowka):
            check_lpart_num - liczba partycji przypadajacych na ostatni bit CHECK
            check_part_size - liczba bitow CHECK przypadajaca na ostatnia partycje */

        dataBitsCntForLine = new int [linesCnt];
        dataBitsOffsetForLine = new int [linesCnt]; 
            
        for(int iLineNum = 0; iLineNum < linesCnt; iLineNum++) {
            dataBitsCntForLine[iLineNum] = bitsPerLine;
        }
        for(int iLineNum = 0; iLineNum < linesCnt - linesInLastSplot; iLineNum++) {
            if( iLineNum%linesPerSplot == linesPerSplot-1 )
                dataBitsCntForLine[iLineNum] = bitsPerLine - checkBitsPerLine;                     
        }
        dataBitsCntForLine[linesCnt-1] = bitsPerLine - checkBitsInLastLine;

        for(int iLineNum = 1; iLineNum < linesCnt; iLineNum++) {
            dataBitsOffsetForLine[iLineNum] = dataBitsOffsetForLine[iLineNum - 1] + dataBitsCntForLine[iLineNum - 1];
        }
        
        if(maxLinesInSplot < getLinesInSplot())
            maxLinesInSplot = getLinesInSplot();
        
        System.out.println  ("dataBitsCntForLine " + dataBitsCntForLine);
        System.out.println  ("dataBitsOffsetForLine " + dataBitsOffsetForLine);
    }
    
    //sLine = 0 gives line with the check bit
    public boolean isLineActive(int sLine, int iLine) {
        if(iLine < (linesCnt - linesInLastSplot) ) {
            if(iLine%linesPerSplot == linesPerSplot-1 - sLine)
                return true;
        }
        else if(iLine == linesCnt -1 - sLine)
                return true;

        return false;
    }
    
    public int getSplotsCnt() {
        return splotsCnt;
    }

    public static int getMaxLinesInSplot() {
        return maxLinesInSplot;
    }       
    
    public int getLinesInSplot() {
        return Math.max(linesPerSplot, linesInLastSplot);
    }
    
    private static Map<ChipType, LVDSLinkDef> lvdesLinksDefs = new HashMap<ChipType, LVDSLinkDef>();

    
    /**
     * the key in the map is the type of a transmitter
     * @return
     */
    public static Map<ChipType, LVDSLinkDef> getLvdesLinksDefs() {
        return lvdesLinksDefs;
    }

    public int getBitsPerLine() {
        return bitsPerLine;
    }

    public int getDataBitsCnt() {
        return dataBitsCnt;
    }

    public int getLinesCnt() {
        return linesCnt;
    }

    public int getCheckBitsCnt() {
        return checkBitsCnt;
    }

    public int[] getDataBitsCntForLine() {
        return dataBitsCntForLine;
    }

    public int[] getDataBitsOffsetForLine() {
        return dataBitsOffsetForLine;
    }

    public int getLinesPerSplot() {
        return linesPerSplot;
    }

    public int getCheckBitsPerLine() {
        return checkBitsPerLine;
    }

    public int getLinesInLastSplot() {
        return linesInLastSplot;
    }

    public int getCheckBitsInLastLine() {
        return checkBitsInLastLine;
    }
}
