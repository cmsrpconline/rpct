package rpct.synch;

import java.util.Vector;

import rpct.xdaqaccess.Board;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.synchro.LVDSLink;
import rpct.xdaqaccess.synchro.LVDSTransmDevice;

public class TriggerBoard {
    int tbId;
    
    int position;
    
    int optoLinksCnt = 18;    
    //int pacsCnt = 4;    
    
    Board tb;
    
    LVDSTransmDevice[] optoTBTab;
    //Pac[] pacTab; 
    Vector<LVDSTransmDevice> pacVec;
    
    LVDSTransmDevice rmb; 
    LVDSTransmDevice tbSorter;
    
    LVDSLink[][] optoPacLinks;
    LVDSLink[]   pacSorterLinks;
    LVDSLink[]   optoRmbLinks;
    
    Vector<LVDSLink> links;
    
/*    TriggerBoard(int _baseAddress) throws Exception {
        tbId = _baseAddress;
        System.out.println("tb " + tbId + " added devices: ");
        tb = (Board)HardwareCatalog.findById(tbId);
        
    } */   
    
    TriggerBoard(Board _tb) throws Exception {
    	tb = _tb;
    	tbId = tb.getId();
    	String tbName = tb.getName() + " " + tb.getId(); 
        position = tb.getPosition();
        optoTBTab = new LVDSTransmDevice[6];
        //pacTab = new Pac[pacsCnt];
        pacVec = new Vector<LVDSTransmDevice>();
        links = new Vector<LVDSLink>(); 
        
        int iOpto = 0;
        int pacI = 0;
        
        for(Device device : tb.getDevices()) {
            //if(cgetType() == "opto") {  
            if(device.getType().equals("TB3OPTO") == true) {    
                optoTBTab[iOpto] = new LVDSTransmDevice(device);
                System.out.println("Opto Id " + device.getId() + " name " + device.getName());  
                iOpto++;
            }
            else if(device.getType().equals("TB3LDPAC") == true) {             
                pacVec.add(new LVDSTransmDevice(device));
                
                System.out.println("Pac Id " + device.getId() + " name " + device.getName());  
                pacI++;
            }
            //else if(device.getType() == "gbsort") {
            else if(device.getType().equals("TB3GBSORT") == true) {
                tbSorter = new LVDSTransmDevice(device);
                System.out.println("TbSorter Id " + device.getId() + " name " + device.getName());                
            }
            //else if(device.getType() == "rmb") {
            else if(device.getType().equals("TB3RMB") == true) {
                rmb = new LVDSTransmDevice(device);
                System.out.println("Rmb Id " + device.getId() + " name " + device.getName());
            }
        }
               
        optoPacLinks = new LVDSLink[pacVec.size()][optoLinksCnt];
        for(int iPac = 0; iPac < pacVec.size(); iPac++) {            
            for(int iLinkNum = 0; iLinkNum < optoLinksCnt; iLinkNum++) {
                if(optoTBTab[iLinkNum/3] != null) {
                    optoPacLinks[iPac][iLinkNum] = new LVDSLink(optoTBTab[iLinkNum/3], pacVec.get(iPac), iLinkNum);
                    links.add(optoPacLinks[iPac][iLinkNum]);
                }
            }                        
        }
        
        if(rmb != null) {
        	optoRmbLinks = new LVDSLink[optoLinksCnt];
	        for(int iLinkNum = 0; iLinkNum < optoLinksCnt; iLinkNum++) {
    	        if(optoTBTab[iLinkNum/3] != null) {
        	        optoRmbLinks[iLinkNum] = new LVDSLink(optoTBTab[iLinkNum/3], rmb, iLinkNum);
            	    links.add(optoRmbLinks[iLinkNum]);
	            }
    	    } 
        }
        
        if(tbSorter != null)  {
	        pacSorterLinks = new LVDSLink[pacVec.size()];
	        for(int iPac = 0; iPac < pacVec.size(); iPac++) {            
	            pacSorterLinks[iPac] = new LVDSLink(pacVec.elementAt(iPac), tbSorter, iPac);
	            links.add(pacSorterLinks[iPac]);
	        }
        }
    }
    
    int getId() {
    	return tb.getId();
    }
    
    LVDSTransmDevice getTBSotrer() {
        return tbSorter;
    }

    public LVDSTransmDevice[] getOptoTBTab() {
        return optoTBTab;
    }

    public Vector<LVDSTransmDevice> getPacTab() {
        return pacVec;
    }

    public LVDSTransmDevice getRmb() {
        return rmb;
    }

    public LVDSTransmDevice getTbSorter() {
        return tbSorter;
    }

    public LVDSLink[][] getOptoPacLinks() {
        return optoPacLinks;
    }

    public LVDSLink[] getPacSorterLinks() {
        return pacSorterLinks;
    }

    public LVDSLink[] getOptoRmbLinks() {
        return optoRmbLinks;
    }

    public Vector<LVDSLink> getLinks() {
        return links;
    }

    public int getPosition() {
        return position;
    }
    
    public String getName() {
        return tb.getName();
    }
}
