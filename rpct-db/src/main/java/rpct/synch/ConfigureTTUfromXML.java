package rpct.synch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.Board;
import rpct.xdaqaccess.Crate;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDBTTUBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbCrate;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbSorterCrate;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;
import rpct.xdaqaccess.hardwaredb.HardwareDbMuxRecChip;
import rpct.xdaqaccess.synchro.LVDSTransmDevice;

public class ConfigureTTUfromXML {
    public static void main(String[] args) throws DataAccessException {          
        if(args.length == 0) {            
            String[] argsL = {
                    "ttuConfigs.xml"
                    };
            
            args = argsL;
        }
        
        HibernateContext context = new SimpleHibernateContextImpl();
        try {
            String xmlFileName = System.getenv("HOME") + "/bin/config/" + args[0];
            String r_s = "r";
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("the TTUs in the SC will be configured (algorithm paramters only) from the file " + xmlFileName);
            System.out.println("continue (y/n)? ");
            r_s = br.readLine();                             
            if (r_s.equals("n")) {
                System.out.println("End");
                return;
            }
            
            EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
            HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
            XMLParametrsWriter xmlParametrsWriter = new XMLParametrsWriter(equipmentDAO, dbMapper);
            
            List<HardwareDbCrate> crates = new ArrayList<HardwareDbCrate>();
            crates.add(dbMapper.getCrate(equipmentDAO.getCrateByName("SC")));

            dbMapper.getBoard(equipmentDAO.getBoardByName("TTU_1"));

            xmlParametrsWriter.setParamters(xmlFileName);

            System.out.println("End");
        }
        catch (XdaqException e) {
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JAXBException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } 
        finally {
            context.closeSession();
        }
    }
}
