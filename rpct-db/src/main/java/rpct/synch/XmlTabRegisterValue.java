package rpct.synch;

import javax.xml.bind.annotation.XmlAttribute;

public class XmlTabRegisterValue {
    @XmlAttribute
    protected int index; 
    @XmlAttribute
    protected String regValue;

    XmlTabRegisterValue() {};
    
    public XmlTabRegisterValue(String regValue, int index) {
        this.regValue = regValue;
        this.index = index;
    };
}
