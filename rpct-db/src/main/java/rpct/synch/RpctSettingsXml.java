package rpct.synch;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Vector;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.xdaq.XdaqException;

@XmlRootElement 
public class RpctSettingsXml {
    private List<XmlBoard> boards;
    
    RpctSettingsXml() {
        boards = new Vector<XmlBoard>();
    }
    
    @XmlElement(name="board")
    public List<XmlBoard> getBoards() {
        return boards;
    }
    
    public void setBoards(List<XmlBoard> boards) {
        this.boards = boards;
    }
    
    public void addBoard(XmlBoard board) {
        boards.add(board);
    }    
    
    void readRegisters() throws XdaqException, RemoteException, ServiceException {
        for (XmlBoard board : boards) {
            board.readDevices();
        }
    }
    
    void aplyRegisters() throws XdaqException, RemoteException, ServiceException, DataAccessException {
        for (XmlBoard board : boards) {
            board.aplyDevices();
        }
    }
}
