package rpct.synch;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.diag.TriggerDataSel;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;

public class TTCTests {
    private  static Map<ChipType, Integer> currentTccDelays = new TreeMap<ChipType, Integer>();
    static int cuurentControlDElay = 7;
    static {		
        currentTccDelays.put(ChipType.OPTO, 3);
        currentTccDelays.put(ChipType.PAC, 2);
        currentTccDelays.put(ChipType.RMB, 3);
        currentTccDelays.put(ChipType.GBSORT, 3);
    }

    public static void checkTTCDelays(List<HardwareDbTriggerCrate> triggerCrates) throws RemoteException, ServiceException, HardwareDbException {
        long ttcTestData = 0x25;

        int[] delayMod = {-1, 1, 0 };

        for(HardwareDbTriggerCrate tc : triggerCrates) {
            boolean ok = true;
            tc.getTcSort().getHardwareChip().getItem("TTC.TEST_ENA").write(0x1);
            tc.getTcSort().getHardwareChip().getItem("TTC_TEST_DATA").write(ttcTestData);

            for(HardwareDBTriggerBoard tb : tc.getHardwareDBTriggerBoards()) {
                //on control 
                for(int i = 0; i < delayMod.length; i++) {
                    int delay = (cuurentControlDElay + delayMod[i])%8;

                    tb.getControlChip().getItem("TTC.TC_DELAY").write(delay);

                    long readTtcTestData = tb.getControlChip().getItem("TTC_TC_TEST_DATA").read();

                    if (readTtcTestData != ttcTestData) {
                        System.out.println(tb.getControlChip().getBoard().getName() + " " + 
                                tb.getControlChip().getName() + " delay " + delay + ": readTtcTestData: " + Long.toHexString(readTtcTestData)  + " not equal to send: " + ttcTestData);
                        ok = false;

                    } /*else {
						System.out.println(tb.getControlChip().getBoard().getName() + " " + 
								tb.getControlChip().getName() + " OK ");
					}*/
                }

                for(HardwareDbChip chip : tb.getChips()) {
                    for(int i = 0; i < delayMod.length; i++) {
                        int delay = (currentTccDelays.get(chip.getDbChip().getType()) + delayMod[i])%8;
                        chip.getHardwareChip().getItem("STATUS.TTC_DATA_DELAY").write(delay);

                        long readTtcTestData = chip.getHardwareChip().getItem("TTC_TEST_DATA").read();

                        if (readTtcTestData != ttcTestData) {
                            System.out.println(chip.getFullName() + " delay " + delay + ": readTtcTestData: " + Long.toHexString(readTtcTestData)  + " not equal to send: " + ttcTestData);
                            ok = false;
                        } 
                    }
                }
            }

            tc.getTcSort().getHardwareChip().getItem("TTC.TEST_ENA").write(0);
            if(ok)
                System.out.println("checkTTCDelays: " + tc.getFullName() + ": OK");
        }
    }

    static void testTTCSignals(List<HardwareDbTriggerCrate> triggerCrates) throws HardwareDbException, RemoteException, ServiceException, InterruptedException {
        int reapatCnt = 30;
        TriggerDataSel[] trgDataSel = {TriggerDataSel.BCN0, TriggerDataSel.L1A, TriggerDataSel.PRETRIGGER_0}; 
        for(HardwareDbTriggerCrate tc : triggerCrates) {
            boolean ok = true;
            for(HardwareDBTriggerBoard tb : tc.getHardwareDBTriggerBoards()) {
                for(HardwareDbChip chip : tb.getChips()) {
                    if(chip.getDbChip().getType() != ChipType.RMB) {
                        long bcn0TestCnt = chip.getHardwareChip().getItem("TEST_CNT.BCN0").read();
                        int i = 0;
                        for(i = 0; i < reapatCnt; i++) {
                            Thread.sleep(100);
                            if(bcn0TestCnt != chip.getHardwareChip().getItem("TEST_CNT.BCN0").read()) {
                                break;
                            }
                        }
                        if (i == reapatCnt) {
                            System.out.println(chip.getFullName() + ": testTTCSignals: TEST_CNT.BCN0 is not changing !!!!!!!!!!!");
                            ok = false;
                        }

                    }

                    for(int iTrgDataSel = 0; iTrgDataSel < trgDataSel.length; iTrgDataSel++) {
                        if(chip.getDbChip().getType() != ChipType.RMB) 
                            chip.getHardwareChip().getItem("STATUS.TRG_DATA_SEL").write(trgDataSel[iTrgDataSel].getIntVal());
                        else
                            chip.getHardwareChip().getItem("STATUS.TRG_RMB_SEL").write(trgDataSel[iTrgDataSel].getIntVal());

                        chip.getHardwareChip().getItem("TTC_EVN").read();					
                        long ententNum = chip.getHardwareChip().getItem("TTC_EVN").read();
                        int i = 0;
                        for(i = 0; i < reapatCnt; i++) {
                            Thread.sleep(100);
                            if(ententNum != chip.getHardwareChip().getItem("TTC_EVN").read()) {
                                break;
                            }
                        }
                        if (i == reapatCnt) {
                            System.out.println(chip.getFullName() + ": testTTCSignals: TTC_EVN for the trigger type " + trgDataSel[iTrgDataSel] + " is not changing !!!!!!!!!!!");
                            ok = false;
                        }
                    }
                }	
                if(ok)
                    System.out.println("testTTCSignals: " + tc.getFullName() + ": OK");
            }
        }
    }

    static void testTTCSignals2(List<HardwareDbTriggerCrate> triggerCrates) throws HardwareDbException, ServiceException, InterruptedException, IOException {
        int reapatCnt = 30;
        TriggerDataSel[] trgDataSel = {
                TriggerDataSel.BCN0, TriggerDataSel.L1A, 
                TriggerDataSel.PRETRIGGER_0 
                };
        
        for(int iTrgDataSel = 0; iTrgDataSel < trgDataSel.length; iTrgDataSel++) {            
            for(HardwareDbTriggerCrate tc : triggerCrates) {
                tc.getTcSort().getHardwareChip().getItem("STATUS.TRG_DATA_SEL").write(trgDataSel[iTrgDataSel].getIntVal());
                //long tcEventNum = tc.getTcSort().getHardwareChip().findByName("TTC_EVN").();          
                for(HardwareDBTriggerBoard tb : tc.getHardwareDBTriggerBoards()) {
                    for(HardwareDbChip chip : tb.getChips()) {                        
                        if(chip.getDbChip().getType() != ChipType.RMB) 
                            chip.getHardwareChip().getItem("STATUS.TRG_DATA_SEL").write(trgDataSel[iTrgDataSel].getIntVal());
                        else
                            chip.getHardwareChip().getItem("STATUS.TRG_RMB_SEL").write(trgDataSel[iTrgDataSel].getIntVal());
                    }
                }                   
            }
            
            System.out.println("send EC0 and start sending the " + trgDataSel[iTrgDataSel]);
            String y_n = "y";
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("type a char to stop the test");
            y_n = br.readLine();  
            
            for(HardwareDbTriggerCrate tc : triggerCrates) {
                tc.getTcSort().getHardwareChip().getItem("STATUS.TRG_DATA_SEL").write(trgDataSel[iTrgDataSel].getIntVal());
                long tcEventNum = tc.getTcSort().getHardwareChip().getItem("TTC_EVN").read();
                System.out.println(tc.getTcSort().getFullName() + ": the trigger type " + trgDataSel[iTrgDataSel] + " tcEventNum " + tcEventNum);
                boolean ok = true;            
                for(HardwareDBTriggerBoard tb : tc.getHardwareDBTriggerBoards()) {
                    for(HardwareDbChip chip : tb.getChips()) {                        
                        if(chip.getDbChip().getType() != ChipType.RMB) 
                            chip.getHardwareChip().getItem("STATUS.TRG_DATA_SEL").write(trgDataSel[iTrgDataSel].getIntVal());
                        else
                            chip.getHardwareChip().getItem("STATUS.TRG_RMB_SEL").write(trgDataSel[iTrgDataSel].getIntVal());

                        chip.getHardwareChip().getItem("TTC_EVN").read();                    
                        long eventNum = chip.getHardwareChip().getItem("TTC_EVN").read();
                        if(tcEventNum != eventNum) {
                            System.out.println(chip.getFullName() + ": the trigger type " + trgDataSel[iTrgDataSel] + " eventNum " + eventNum + " tcEventNum " + tcEventNum);
                        }                           
                    }
                }   
            }
        }
    }

    public static void main(String[] args) {
        try {	        	 
            HibernateContext context = new SimpleHibernateContextImpl();
            EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
            //ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
            HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);

            if(args.length == 0) {            
                String[] argsL = {
                        //"TC_0",
                        //"TC_1",
                        //"TC_2",                        
                        //"TC_9",
                        //"TC_10",
                        //"TC_11",
                        "PASTEURA_TC"
                };

                args = argsL;
            }

            System.out.println("Selected crates");

            List<HardwareDbTriggerCrate> crates = new ArrayList<HardwareDbTriggerCrate>();
            for(int i = 0; i < args.length; i++) {
                System.out.print(args[i] + " ");
                crates.add((HardwareDbTriggerCrate)dbMapper.getCrate(equipmentDAO.getCrateByName(args[i])));
            }            

            checkTTCDelays(crates);

            //testTTCSignals2(crates);

        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (HardwareDbException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}
