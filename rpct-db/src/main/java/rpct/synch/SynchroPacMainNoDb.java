
package rpct.synch;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Vector;

import rpct.xdaq.XdaqApplicationInfo;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.Board;
import rpct.xdaqaccess.Crate;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.synchro.DefOut;
import rpct.xdaqaccess.synchro.LVDSLink;
import rpct.xdaqaccess.synchro.LVDSLinksSynchronizer;
import rpct.xdaqaccess.synchro.LVDSTransmDevice;

public class SynchroPacMainNoDb {         
    public static void main(String[] args) {
        try {                      
            FileOutputStream file = new FileOutputStream("./out/pasteuraTB_2_7c.txt", false);
            PrintStream prntSrm = new PrintStream(file);
            DefOut defLog= new DefOut(prntSrm);
            
            LVDSLinksSynchronizer tcLinksSynchronizer = new LVDSLinksSynchronizer(14, 8);
                        
            HardwareRegistry hardwareRegistry = new HardwareRegistry();
            hardwareRegistry.registerXdaqApplication(new XdaqApplicationInfo(new URL("http://pccms8.igf.fuw.edu.pl:1972"), "XdaqTestBench", 2));            
            
            List<Crate> crates = hardwareRegistry.getAllCrates();
            for (Crate crate : crates) {
                System.out.println("Crate " + crate.getName());
                LVDSTransmDevice tcSorter = null;
                Vector<TriggerBoard> tbVec = new Vector<TriggerBoard>();
	            for(Board board : crate.getBoards()) {	              	
	            	if(board.getType().compareTo("TB3") == 0) {
	              		TriggerBoard tb = new TriggerBoard(board);
	              		tbVec.add(tb);
	              		for (LVDSLink link :  tb.getLinks())
	                          tcLinksSynchronizer.addLVDSLink(link);  
	              	}           
	            	else if(board.getType().compareTo("TCSORT") == 0) { 
	            		for(Device device : board.getDevices()) {
	            			if(device.getType().compareTo("TCSORTTCSORT") == 0) {
	            				tcSorter = new LVDSTransmDevice(device);
	            				break;
	            			}	
	            		}	              		
	              	} 
	            }
	            
	            if(tcSorter != null) {	            	
	            	for(TriggerBoard tb : tbVec) {
	            		if(tb.getTBSotrer() != null)
	            			tcLinksSynchronizer.addLVDSLink(new LVDSLink(tb.getTBSotrer(), tcSorter, tb.getPosition()-11));                            	
	            	}	            
	            }  
            }
                                   
            System.out.println("opto_pacsLinksSynchronizer.findSynchroParams()");
            tcLinksSynchronizer.findSynchroParams();
            
            long wait = 1000 * 60;           
            DefOut.out.println("waiting " + wait/1000 + "s");  
            Thread.sleep(wait);
            System.out.println("opto_pacsLinksSynchronizer.finalParametrsTune()");
            tcLinksSynchronizer.finalParametrsTune(true);
            
            for (int i = 0; i < 4 ; i++) {                         
                wait = 1000 * 30 * 1;         
                DefOut.out.println("waiting " + wait/1000 + "s");  
                Thread.sleep(wait);
                System.out.println("opto_pacsLinksSynchronizer.finalParametrsTune()");
                tcLinksSynchronizer.finalParametrsTune(true);
            }
            
            tcLinksSynchronizer.printParams();   
            
            tcLinksSynchronizer.cleanTransAndSend();
            
            //tcLinksSynchronizer.setRandomTest();///!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            
            System.out.println("end with ");
        } 
        catch (MalformedURLException e) {
            e.printStackTrace();
        } 
        catch (XdaqException e) {
            e.printStackTrace();
        } 
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }  
    }  
}
