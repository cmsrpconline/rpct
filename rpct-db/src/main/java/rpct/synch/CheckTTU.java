package rpct.synch;

import java.io.BufferedReader;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.DeviceItemWord;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDBTTUBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbChipImpl;
import rpct.xdaqaccess.hardwaredb.HardwareDbCrate;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbOpto;
import rpct.xdaqaccess.hardwaredb.HardwareDbSorterCrate;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;
import rpct.xdaqaccess.synchro.DefOut;

public class CheckTTU {
	static void checkTTU(HardwareDbSorterCrate sc) throws HardwareDbException,
			ServiceException, InterruptedException, IOException {
		for (HardwareDBTTUBoard tb : sc.getHardwareDBTTUBoards()) {
			for (HardwareDbOpto opto : tb.getOptos()) {
				opto.setUsedOptLinksInputsMaskFromHardware();
				// opto.getLinkConns(true);
			}
		}

		int totalLinks = 0;
		int notOKLinks = 0;
		
		for (HardwareDBTTUBoard tb : sc.getHardwareDBTTUBoards()) {

			// loop over TTUBoards
			String tbname = tb.getName();

			for (HardwareDbOpto opto : tb.getOptos()) {
				// OPTO chips First
				System.out.println("Checking: " + opto.getFullName());

				long fastDealy = ((HardwareDbChip) opto).getHardwareChip()
						.getItem("REC_FAST_DATA_DELAY.REC_FAST_DATA_DELAY")
						.read();
				long dataDealy = ((HardwareDbChip) opto).getHardwareChip()
						.getItem("REC_DATA_DELAY.REC_DATA_DELAY").read();

				long synchAck = ((HardwareDbChip) opto).getHardwareChip()
						.getItem("STATUS.REC_SYNCH_ACK").read();

				// This is the standard TEST
				for (int iOl = 0; iOl < ((DeviceItemWord) ((HardwareDbChip) opto)
						.getHardwareChip().getItem("REC_TEST_OR_DATA"))
						.getNumber(); iOl++) {
					if ((opto.getUsedOptLinksInputsMask() & (1 << iOl)) == 0)
						continue;

					totalLinks++;
					long fastD = (fastDealy & (7 << (iOl * 3))) >> (iOl * 3);
					long dataD = (dataDealy & (0xf << (iOl * 4))) >> (iOl * 4);

					long orData = ((rpct.xdaqaccess.DeviceItemWord) ((HardwareDbChip) opto)
							.getHardwareChip().getItem("REC_TEST_OR_DATA"))
							.read(iOl);
					String status = "";
					long thisSynchAck = (synchAck & (1l << iOl));
					if (orData != 0 || thisSynchAck == 0) {
						status = "NOT OK !!!!!!!!!!!!!!!!";

						System.out.println(opto.getFullName() + " optlink "
								+ iOl + " synchAck " + thisSynchAck
								+ " fastDealy " + fastD + " dataDealy " + dataD
								+ " orData " + orData + " " + status);
						
						DefOut.out.println(opto.getFullName() + " optlink "
								+ iOl + " synchAck " + thisSynchAck
								+ " fastDealy " + fastD + " dataDealy " + dataD
								+ " orData " + orData + " " + status);
						
						notOKLinks++;
					} else {
						status = "OK";

					}
				}

				// Print REC_TEST_DATA from OPTO
				for (int iLink = 0; iLink < 2; iLink++) {
					String optoname = opto.getName();
					if (tbname.equals("TTU_2")
							&& (optoname.equals("TTUOPTO 5")
									|| optoname.equals("TTUOPTO 6") || optoname
									.equals("TTUOPTO 7")))
						continue;
					long rxdata = ((rpct.xdaqaccess.DeviceItemWord) ((HardwareDbChip) opto)
							.getHardwareChip().getItem("REC_TEST_DATA"))
							.read(iLink);
					System.out.println("RecTestData Opto: " + iLink + " x"
							+ Integer.toHexString((int) rxdata));
				}

			}

			for (HardwareDbChip ttutrig : tb.getTTUTrigs()) {
				// Check now OPTO -> PAC chips

				String trigname = ttutrig.getName();
				if (tbname.equals("TTU_2") && trigname.equals("TTUTRIG 9"))
					continue;

				System.out.println("Checking: " + ttutrig.getFullName());

				DeviceItemWord testData = (DeviceItemWord) ttutrig
						.getHardwareChip().getItem("REC_TEST_DATA");

				for (int i = 0; i < testData.getDesc().getNumber(); ++i) {
					if (tbname.equals("TTU_2") && trigname.equals("TTUTRIG 8")
							&& (i > 5))
						continue; // no need for this second half
					System.out
							.println("REC_TEST_DATA: "
									+ i
									+ "x"
									+ Integer.toHexString((int) testData
											.read(i) & 0x3fffffff));
				}

				((HardwareDbChipImpl) ttutrig).resetRecErrorCnt();
			}

		}

		System.out.println("------------------------------------------------------------------");
		
		System.out.println("Failed links " + notOKLinks + " / " + totalLinks
				+ " = " + ((double) notOKLinks) / totalLinks);
		DefOut.out.println("Failed links " + notOKLinks + " / " + totalLinks
				+ " = " + ((double) notOKLinks) / totalLinks);

		
	}

	public static void main(String[] args) {
		try {

			FileOutputStream file = new FileOutputStream(System.getenv("HOME")
					+ "/bin/out/" + "CheckTTU.log", true);
			PrintStream prntSrm = new PrintStream(file);
			DefOut defLog = new DefOut(prntSrm);

			HibernateContext context = new SimpleHibernateContextImpl();
			EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
			// ConfigurationDAO configurationDAO = new
			// ConfigurationDAOHibernate(context);
			HardwareDbMapper dbMapper = new HardwareDbMapper(
					new HardwareRegistry(), context);

			String r_s = "y";
			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));
			SimpleDateFormat formatter = new SimpleDateFormat(
					"EEE dd MMM HH:mm:ss z yyyy");
			Calendar cal = Calendar.getInstance();
			String formattedDate = null;

			HardwareDbSorterCrate sc = (HardwareDbSorterCrate) dbMapper
					.getCrate(equipmentDAO.getCrateByName("SC"));

			while (true) {
				formattedDate = formatter.format(Calendar.getInstance()
						.getTime());
				DefOut.out.println("started at: " + formattedDate);
				checkTTU(sc);
				formattedDate = formatter.format(Calendar.getInstance()
						.getTime());
				DefOut.out.println("Check finished at: " + formattedDate);
				System.out.println("Last check at: " + formattedDate);

				System.out.println("------------------------------------------------------------------");
				
				System.out.println("repeat (y/n)?");
				r_s = br.readLine();
				if (r_s.equals("n")) {
					break;
				}
			}

		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (HardwareDbException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
