package rpct.synch;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import rpct.xdaq.XdaqException;
import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.DeviceItemWord;

public class HalfSort {
    private Device device;

    public HalfSort(Device _device) {
        device = _device;
    }

    public void setBitInBinary(String registerName, int bitNum, boolean bit)
            throws XdaqException, RemoteException, ServiceException {
        if (bitNum >= device.getItem(registerName).getDesc().getWidth())
            throw new IndexOutOfBoundsException("bitNum >= register width ");
        Binary b = device.getItem(registerName).readBinary();
        byte[] bytes = b.getBytes();
        int bitPos = bitNum;
        int bitInByte = bitPos % 8;
        int bytePos = bytes.length - 1 - bitPos / 8;
        if (bit)
            bytes[bytePos] |= (1 << bitInByte);
        else
            bytes[bytePos] &= (~(1 << bitInByte));
        device.getItem(registerName).writeBinary(new Binary(bytes));
    }

    public void setBitInLong(String registerName, int bitNum, boolean bit)
            throws XdaqException, RemoteException, ServiceException {
        if (bitNum >= device.getItem(registerName).getDesc().getWidth())
            throw new IndexOutOfBoundsException("bitNum >= register width ");
        long mask = 1 << bitNum;
        long value = device.getItem(registerName).read();
        if (bit)
            value |= mask;
        else
            value &= (~mask);

        device.getItem(registerName).write(value);
    }

    public void setBit(String registerName, int bitNum, boolean bit)
            throws XdaqException, RemoteException, ServiceException {
        if (device.getItem(registerName).getDesc().getWidth() > Long.SIZE)
            setBitInBinary(registerName, bitNum, bit);
        else
            setBitInLong(registerName, bitNum, bit);
    }

    void setBit(String registerName, int regIndex, int bitNum, boolean bit)
            throws XdaqException, RemoteException, ServiceException {
        DeviceItemWord item = (DeviceItemWord) device.getItem(registerName);
        long value = item.read(regIndex);
        long mask = 1 << bitNum;
        if (bit)
            value |= mask;
        else
            value &= (~mask);
        item.write(regIndex, value);
    }

    void setBits(String registerName, int bitFrom, int bitsCnt, long bits)
            throws XdaqException, RemoteException, ServiceException {
        if ((bitFrom + bits) >= device.getItem(registerName).getDesc()
                .getWidth())
            throw new IndexOutOfBoundsException(
                    "bitFrom + bits >= register width ");
        long value = device.getItem(registerName).read();
        bits <<= bitFrom;
        long mask = ((1 << (bitsCnt + 1)) - 1) << bitFrom;
        bits &= mask;
        value &= (~mask);
        value |= bits;
        device.getItem(registerName).write(value);
    }
    
    void setBits(String registerName, int regIndex, int bitFrom, int bitsCnt, long bits)
            throws XdaqException, RemoteException, ServiceException {
        if ((bitFrom + bits) >= device.getItem(registerName).getDesc().getWidth())
            throw new IndexOutOfBoundsException(
                    "bitFrom + bits >= register width ");
        DeviceItemWord item = (DeviceItemWord) device.getItem(registerName);
        long value = item.read(regIndex);
        bits <<= bitFrom;
        long mask = ((1 << (bitsCnt + 1)) - 1) << bitFrom;
        bits &= mask;
        value &= (~mask);
        value |= bits;
        item.write(regIndex, value);
    }    
    
    void findSynch(int inputNum) throws XdaqException, RemoteException, ServiceException {        
        DeviceItemWord item = (DeviceItemWord) device.getItem("REC_TEST_OR_DATA");
        item.read(inputNum); 
        long value = item.read(inputNum);    
        if(value != 0) {
            for(int iChunk = 0; iChunk < 6; iChunk++) {
                value = item.read(inputNum);
                if((value & (1 << iChunk)) != 0) {
                    for(long inv = 0; inv < 16; inv++) {
                        setBits("REC_FCLK_INV", inputNum, iChunk * 4, 4, inv);
                        value = item.read(inputNum);
                        value = item.read(inputNum);
                        if((value & (1 << iChunk)) == 0)
                            break;
                    }
                }
            }
        }
    }
    /*public static void main(String[] args) {
        try {     
            HalfSort hsb = null;  
            TCSorter tcSorter = null;
            IIAccess iiAccess = new IIAccessImplAxis(new URL("http://pccmsrpct2:1972"), "XdaqTestBench", 3);
            Crate[] crates = iiAccess.getCrates();
            for (Crate crate : crates) {
                System.out.println("Crate " + crate.getName());                                
                for(Board board : crate.getBoards()) {                  
                    if(board.getType().compareTo("HSB") == 0) {
                        for(Device device : board.getDevices()) {                            
                            if(device.getType().compareTo("HALFSORT") == 0)
                            hsb = new HalfSort(device);
                        }
                    }    
                    else if(board.getType().compareTo("TCSORT") == 0) { 
                        for(Device device : board.getDevices()) {
                            if(device.getType().compareTo("TCSORTTCSORT") == 0) {
                                tcSorter = new TCSorter(device, 9, 9, 2, 6, "tcSorter");
                            }   
                        }                       
                    } 
                }
            }
            tcSorter.setSendTestStaticData("1234567abcdef");
            tcSorter.getDevice().findByName("SEND_TRND_ENA").write(3);
            
            hsb.setBit("REC_TEST_ENA", 6, true);
            hsb.setBit("REC_TEST_ENA", 7, true);
            hsb.setBit("REC_TEST_ENA", 12, true);
            hsb.setBit("REC_TEST_ENA", 13, true);
            
            hsb.setBit("REC_TRND_ENA", 6, true);
            hsb.setBit("REC_TRND_ENA", 7, true);
            hsb.setBit("REC_TRND_ENA", 12, true);
            hsb.setBit("REC_TRND_ENA", 13, true);
            
            hsb.setBits("REC_FAST_DELAY", 2 * 7, 2, 1);
            System.out.println("end with ");
        } 
        catch (MalformedURLException e) {
            e.printStackTrace();
        } 
        catch (XdaqException e) {
            e.printStackTrace();
        } 
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }  
    }  */
}
