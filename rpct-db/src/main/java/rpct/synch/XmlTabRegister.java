package rpct.synch;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Vector;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.rpc.ServiceException;

import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.DeviceItemWord;

@XmlType
public class XmlTabRegister {   
    @XmlAttribute
    String regName;
    @XmlAttribute
    private int size;
    
    private List<XmlTabRegisterValue> valuesTab;
    
    XmlTabRegister() {}

    public XmlTabRegister(String regName, int size) {
        this.regName = regName;
        this.size = size;
        this.valuesTab = new Vector<XmlTabRegisterValue>();
        for(int i = 0; i < size; i++) {
            valuesTab.add(new XmlTabRegisterValue("", i));
        }
    }
    
        
    public void addValue(XmlTabRegisterValue value) {
        valuesTab.add(value);
    }
    
    public void addValue(String regName, int size) {
        valuesTab.add(new XmlTabRegisterValue(regName, size));
    }
    
    void readRegister(Device device) throws RemoteException, ServiceException {
        DeviceItemWord itemWord = (DeviceItemWord) device.getItem(regName);
        for (XmlTabRegisterValue value : valuesTab) {
            Binary b = itemWord.readBinary(value.index);
            value.regValue = b.toString();            
        }                
    }    
    
    void aplyRegister(Device device) throws RemoteException, ServiceException {
        DeviceItemWord itemWord = (DeviceItemWord) device.getItem(regName);
        for (XmlTabRegisterValue value : valuesTab) {
            itemWord.writeBinary (value.index, new Binary(value.regValue));
        }         
    }

    public List<XmlTabRegisterValue> getValuesTab() {
        return valuesTab;
    }

    public void setValuesTab(List<XmlTabRegisterValue> valuesTab) {
        this.valuesTab = valuesTab;
    }


    

}
