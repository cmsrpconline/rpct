package rpct.xdaq;

public class XdaqException extends Exception {

    private static final long serialVersionUID = -1902326834970311077L;

    public XdaqException() {
        super();
    }

    public XdaqException(String message, Throwable cause) {
        super(message, cause);
    }

    public XdaqException(String message) {
        super(message);
    }

    public XdaqException(Throwable cause) {
        super(cause);
    }
}
