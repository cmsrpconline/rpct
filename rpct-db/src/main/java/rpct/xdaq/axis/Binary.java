package rpct.xdaq.axis;

import java.math.BigInteger;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import org.apache.axis.types.HexBinary;

public class Binary extends Number {
    
    private static final long serialVersionUID = 2065209261938581891L;
    
    private HexBinary hexBinary;
    private int bitNum = 0;
    
    public Binary(byte[] val) {
        hexBinary = new HexBinary(val);
        if(bitNum == 0)
            bitNum = val.length * 8;       //kb
    }
    
    public Binary(byte[] val, int bitNum) {
        this(val);
        this.bitNum = bitNum;
    }
    
    public Binary(String val) {
        if ((val.length() % 2) > 0) {
            val = "0" + val;
        }
        hexBinary = new HexBinary(val);
        if(bitNum == 0)
            bitNum = val.length() * 4; //kb
    }
    
    public Binary(String val, int bitNum) {
    	if(val.length() * 4 < bitNum)
    		throw new IllegalArgumentException("Binary(String val, int bitNum): val.length()/4 < bitNum");


    	val = val.substring(0, bitNum/4 + (bitNum % 4 == 0 ? 0 : 1) );

    	if ((val.length() % 2) > 0) {
    		val = "0" + val;
    	}

    	hexBinary = new HexBinary(val);

    	this.bitNum = bitNum;
    }
        
    public Binary(long val) {
        byte[] buffer = new byte[8];
        buffer[0] = (byte)(val >>> 56);
        buffer[1] = (byte)(val >>> 48);
        buffer[2] = (byte)(val >>> 40);
        buffer[3] = (byte)(val >>> 32);
        buffer[4] = (byte)(val >>> 24);
        buffer[5] = (byte)(val >>> 16);
        buffer[6] = (byte)(val >>>  8);
        buffer[7] = (byte)(val >>>  0);
        hexBinary = new HexBinary(buffer);        
    }
       
    public byte[] getBytes() {
        return hexBinary.getBytes();
    }

    @Override
    public int intValue() {
        return (new BigInteger(hexBinary.getBytes())).intValue();
    }

    @Override
    public long longValue() {
        return (new BigInteger(hexBinary.getBytes())).longValue();
    }

    @Override
    public float floatValue() {
        return (new BigInteger(hexBinary.getBytes())).floatValue();
    }

    @Override
    public double doubleValue() {
        return (new BigInteger(hexBinary.getBytes())).doubleValue();
    }
    
    public String toString() {
        if (bitNum != 0) {
            String str = hexBinary.toString();
            int len = (bitNum - 1) / 4 + 1;
            if (str.length() > len) {
                return str.substring(str.length() - len);
            }
            if (str.length() < len) {
                throw new IllegalStateException("str.length() < len");
            }
            return str;
        }
        return hexBinary.toString();
    }
    
    public static Binary valueOf(long val) {
        return new Binary(BigInteger.valueOf(val).toByteArray());
    }
    
    public static Binary valueOf(long val, int bitNum) {
        return new Binary(BigInteger.valueOf(val).toByteArray(), bitNum);
    }
    
    public static Binary valueOf(String val, int bitNum) {
        int len = (bitNum - 1) / 4 + 1;
        if (val.length() > len) {
        	val = val.substring(val.length() - len);
        }
    	if(bitNum%4 > 0) {
    		String oldestChar = val.substring(0, 1);
    		int oldestCharVal = Integer.valueOf(oldestChar, 16);
    		oldestCharVal = oldestCharVal & ((1 << bitNum%4)-1);
    		val = oldestCharVal + val.substring(1);
    	}
    	
    	return new Binary(val, bitNum);
    }

    public int getBitNum() {
        return bitNum;
    }

    @Override
    public int hashCode() {
        final int PRIME = 31;
        int result = 1;
        result = PRIME * result + bitNum;
        result = PRIME * result + ((hexBinary == null) ? 0 : hexBinary.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Binary other = (Binary) obj;
        if (bitNum != other.bitNum)
            return false;
        if (hexBinary == null) {
            if (other.hexBinary != null)
                return false;
        } else if (!hexBinary.equals(other.hexBinary))
            return false;
        return true;
    }
    
    
    /**
     * @param pos
     * @param count
     * returns bits in range pos....pos+count-1
     * @return
     */
    public byte getBits(int pos, int count) {
        if((pos % Byte.SIZE + count) > Byte.SIZE )
            throw new IllegalArgumentException("(pos % Byte.SIZE + count) > Byte.SIZE - not supported now");
        byte bits = 0;
        byte mask = 1;
                
        mask = (byte)( ((mask << count) - 1) <<pos%Byte.SIZE );
        
        int bytePos = getBytes().length - 1 - pos / Byte.SIZE;
        byte oneByte = getBytes()[bytePos];
        
        //bits = (byte)( ((byte)(oneByte & mask) >> pos%Byte.SIZE);
        bits = (byte)(oneByte & mask);
        String str = Integer.toHexString(bits);
        return bits;
    }
    
    public static void setBit(byte[] bytes, int pos, int value) {        
        if(pos < 0 || pos >= bytes.length * Byte.SIZE)
            throw new IllegalArgumentException("pos < 0 || pos >= bytes.length * Byte.SIZE; pos = " + pos);
        int bitInByte = pos % Byte.SIZE; 
        int bytePos = bytes.length - 1 - pos / Byte.SIZE;        
        if (value >= 1)
            bytes[bytePos] |= (1 << bitInByte);
        else
            bytes[bytePos] &= (~(1 << bitInByte));;   
    }
    
    
    /**
     * set bits in @param bytes , starting from the position 
     * @param pos. The number of bits defined by  @param count is copied  from the 
     * @param value     
     */
    public static void setBits(byte[] bytes, int pos, int value, int count) {                   
        for(int i = 0; i < count; i++) {
            setBit(bytes, pos + i, (value & (1 << i)) >> i );
        }
    }
       
    public static boolean compareBytes(byte[] l, byte[] r) {
        if(l == r)
            return true;
        if(l == null)
            return false;
        if(r == null)
            return false;
        
        if(l.length != r.length)
            return false;
        
        for(int i = 0; i < l.length; i++) {
            if(l[i] != r[i])
                return false;
        }
        
        return true;
    }
    
/* ufff - to complicated....   
 * void setBits(int pos, int bitsCout, boolean value) {
    	byte byteValu;
    	if (value == false)
    		byteValu = 0;
    	else
    		byteValu = (byte)0xff; //(byte)((1 << Byte.SIZE + 1)-1);
    	
    	byte[] bytes = hexBinary.getBytes();
    	int byteNum = pos/Byte.SIZE;
    	
    	byte mask = (byte)((1 << (bitsCout +1) - 1) << (pos % Byte.SIZE));
    	if(value)
    		bytes[byteNum] |= mask;
    	else
    		bytes[byteNum] &= (~mask);

    	for(int i = 0; i < bitsCout/Byte.SIZE; i++) {
    		byteNum++;
    		mask = (byte)((1 << (bitsCout +1 - ) - 1));
    	}
    }*/
}
