package rpct.xdaq.axis.bag;

import javax.xml.rpc.encoding.TypeMapping;
import javax.xml.rpc.encoding.TypeMappingRegistry;

import org.apache.axis.Constants;
import org.apache.axis.client.Service;


public class BagUtils {

    public static void registerType(Service service) {
        TypeMappingRegistry registry = service.getTypeMappingRegistry();
        TypeMapping mapping = registry.createTypeMapping();
        mapping.register(Bag.class, Bag.XML_TYPE,
                new BagSerializerFactory(),        
                new BagDeserializerFactory());
        registry.register(Constants.URI_SOAP11_ENC, mapping);        
    }

    /*public static void registerType(Service service, Class clazz) {
        TypeMappingRegistry registry = service.getTypeMappingRegistry();
        TypeMapping mapping = registry.createTypeMapping();
        mapping.register(clazz, Bag.XML_TYPE,
                new BagSerializerFactory(),        
                new BagDeserializerFactory());
        registry.register(Constants.URI_SOAP11_ENC, mapping);
    }*/

}
