package rpct.xdaq.axis.bag;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import javax.xml.namespace.QName;

import org.apache.axis.Constants;
import org.apache.axis.encoding.SerializationContext;
import org.apache.axis.encoding.Serializer;
import org.apache.axis.wsdl.fromJava.Types;
import org.w3c.dom.Element;
import org.xml.sax.Attributes;

public class BagSerizalizer implements Serializer {
	private static final long serialVersionUID = -7257692603677833464L;

	//public static final QName xmlType = Bag.XML_TYPE;

    public void serialize(QName name, Attributes attributes, Object value,
            SerializationContext context) throws IOException {
        if (!(value instanceof Bag))
            throw new IOException("Can't serialize a "
                    + value.getClass().getName() + " with a BagSerizalizer.");
        
        //System.out.println("Serializing " + name);
        
        Bag bag = (Bag) value;
        context.startElement(name, attributes);
        for (Iterator iter = bag.entrySet().iterator(); iter.hasNext();) {
            Map.Entry entry = (Map.Entry) iter.next();
            context.serialize(new QName(name.getNamespaceURI(), (String) entry.getKey()), 
                    null, entry.getValue()); 
        }        
        context.endElement();
    }

    public String getMechanismType() {
        return Constants.AXIS_SAX;
    }

    public Element writeSchema(Class javaType, Types types) throws Exception {
        return null;
    }
}
