package rpct.xdaq.axis.bag;

import javax.xml.namespace.QName;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;
import org.apache.axis.encoding.DeserializationContext;
import org.apache.axis.encoding.Deserializer;
import org.apache.axis.encoding.DeserializerImpl;
import org.apache.axis.encoding.DeserializerTarget;
import org.apache.axis.message.SOAPHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;


public class BagDeserializer extends DeserializerImpl {
    
    protected static Log log =
        LogFactory.getLog(BagDeserializer.class.getName());

    /*public BagDeserializer() {
        value = new HashBag<Object>();
    }*/
    

    /**
     * This method is invoked after startElement when the element requires
     * deserialization (i.e. the element is not an href and the value is not nil.)
     * 
     * Simply creates HashBag.
     * @param namespace is the namespace of the element
     * @param localName is the name of the element
     * @param prefix is the prefix of the element
     * @param attributes are the attributes on the element...used to get the type
     * @param context is the DeserializationContext
     */
    public void onStartElement(String namespace, String localName,
                               String prefix, Attributes attributes,
                               DeserializationContext context)
        throws SAXException {
        if (log.isDebugEnabled()) {
            log.debug("Enter BagDeserializer::startElement()");
        }
        
        if (context.isNil(attributes)) {
            return;
        }
        
        // Create a HashBag to hold the deserialized values.
        setValue(new HashBag<Object>());
        
        if (log.isDebugEnabled()) {
            log.debug("Exit: BagDeserializer::startElement()");
        }
    }

    /**
     * This method is invoked when an element start tag is encountered.
     * @param namespace is the namespace of the element
     * @param localName is the name of the element
     * @param prefix is the element's prefix
     * @param attributes are the attributes on the element...used to get the type
     * @param context is the DeserializationContext
     */
    public SOAPHandler onStartChild(String namespace,
            String localName,
            String prefix,
            Attributes attributes,
            DeserializationContext context)
    throws SAXException {
        
        if (log.isDebugEnabled()) {
            log.debug("Enter BagDeserializer::onStartChild()");
        }
        
        QName typeQName = context.getTypeFromAttributes(namespace,
                localName,
                attributes);

        Deserializer dSer = context.getDeserializerForType(typeQName);

        if (dSer == null)
            throw new SAXException("No deserializer for a " + typeQName + "???");

        dSer.registerValueTarget(new DeserializerTarget(this, localName));
        addChildDeserializer(dSer);

        if (log.isDebugEnabled()) {
            log.debug("Exit: BagDeserializer::onStartChild()");
        }
        
        return (SOAPHandler)dSer;
    }
    
    @Override
    public void onEndChild(String namespace, String localName, DeserializationContext context) throws SAXException {

        if (log.isDebugEnabled()) {
            log.debug("Enter BagDeserializer::onEndChild()");
        }
        
        super.onEndChild(namespace, localName, context);
        
        if (log.isDebugEnabled()) {
            log.debug("Exit: BagDeserializer::onEndChild()");
        }
        
    }

    public void setChildValue(Object value, Object hint) throws SAXException {
        Bag<Object> bag = (Bag<Object>) this.value;
        bag.put((String) hint, value);
    }
}
