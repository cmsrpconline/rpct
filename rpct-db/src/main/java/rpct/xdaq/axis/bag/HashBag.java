package rpct.xdaq.axis.bag;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

public class HashBag<V> implements Bag<V> {

    private static final long serialVersionUID = 5445715181194991633L;
    
    private Map<String, V> map;

    public HashBag() {
        map = new HashMap<String, V>();
    }

    public HashBag(int initialCapacity, float loadFactor) {
        map = new HashMap<String, V>(initialCapacity, loadFactor);
    }

    public HashBag(int initialCapacity) {
        map = new HashMap<String, V>(initialCapacity);
    }

    public HashBag(Map<String, V> m) {
        map = new HashMap<String, V>(m);
    }

    public HashBag(HashBag<V> b) {
        map = new HashMap<String, V>(b.map);
    }
    
    public HashBag(Bag<V> b) {
        map = new HashMap<String, V>(b.size());
        for (Map.Entry<String, V> v : b.entrySet()) {
			map.put(v.getKey(), v.getValue());
		}
    }
    
    public void clear() {
        map.clear();
    }

    public boolean containsKey(String key) {
        return map.containsKey(key);
    }

    public boolean containsValue(V value) {
        return map.containsValue(value);
    }

    public Set<Entry<String, V>> entrySet() {
        return map.entrySet();
    }

    public boolean equals(Object o) {
        return map.equals(o);
    }

    public V get(String key) {
        return map.get(key);
    }

    public int hashCode() {
        return map.hashCode();
    }

    public boolean isEmpty() {
        return map.isEmpty();
    }

    public Set<String> keySet() {
        return map.keySet();
    }

    public V put(String key, V value) {
        return map.put(key, value);
    }

    public void putAll(Bag<? extends V> t) {
        map.putAll(((HashBag<V>) t).map);
    }

    public V remove(String key) {
        return map.remove(key);
    }

    public int size() {
        return map.size();
    }

    public Collection<V> values() {
        return map.values();
    }
}
