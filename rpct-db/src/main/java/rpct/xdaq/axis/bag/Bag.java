package rpct.xdaq.axis.bag;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;

import org.apache.axis.Constants;

public interface Bag<V> {

    public final static QName XML_TYPE = new QName(Constants.URI_SOAP11_ENC, "Struct");
    
    // Query Operations
    int size();
    boolean isEmpty();
    boolean containsKey(String key);
    boolean containsValue(V value);
    V get(String key);

    // Modification Operations
    V put(String key, V value);
    V remove(String key);

    // Bulk Operations
    void putAll(Bag<? extends V> t);
    void clear();


    // Views
    Set<String> keySet();
    Collection<V> values();
    Set<Map.Entry<String, V>> entrySet();    

    // Comparison and hashing
    boolean equals(Object o);
    int hashCode();
}
