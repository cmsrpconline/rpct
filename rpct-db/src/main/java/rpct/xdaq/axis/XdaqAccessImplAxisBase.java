package rpct.xdaq.axis;

import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;

import org.apache.axis.Constants;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;

import rpct.xdaq.XdaqApplicationInfo;
import rpct.xdaq.axis.bag.BagUtils;


public class XdaqAccessImplAxisBase {

    public static final String XDAQ_NS_URI = "urn:xdaq-soap:3.0";
    protected final Service service = new Service();
    
    private XdaqApplicationInfo xdaqApplicationInfo;
    private String namespace;
    
    private String soapActionURI;
    

    private void updateSoapActionUri() {
        soapActionURI = "urn:xdaq-application:class=" + xdaqApplicationInfo.getClassName() 
        + ",instance=" + xdaqApplicationInfo.getInstance();
    }
    
    public XdaqAccessImplAxisBase(XdaqApplicationInfo xdaqApplicationInfo, String namespace) {
        super();
        this.xdaqApplicationInfo = xdaqApplicationInfo;
        this.namespace = namespace;
        BagUtils.registerType(service);
        updateSoapActionUri();
    }   
    
    

    /*public URL getEndpoint() {
        return endpoint;
    }
    
    public void setEndpoint(URL endpoint) {
        this.endpoint = endpoint;
    }
    
    public int getInstance() {
        return instance;
    }
    
    public void setInstance(int instance) {
        this.instance = instance;
        updateSoapActionUri();
    }

    public String getXdaqClassName() {
        return xdaqClassName;
    }

    public void setXdaqClassName(String xdaqClassName) {
        this.xdaqClassName = xdaqClassName;
        updateSoapActionUri();
    }*/

	public XdaqApplicationInfo getXdaqApplicationInfo() {
        return xdaqApplicationInfo;
    }

    public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
    
    protected Call prepareCall(String name) throws ServiceException {
        Call call = (Call) service.createCall();  
        call.setTargetEndpointAddress(xdaqApplicationInfo.getUrl());        
        call.setSOAPActionURI(soapActionURI);
        call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS,
                Boolean.FALSE); 
        call.setOperationName(new QName(namespace, name)); 
        call.setEncodingStyle(Constants.URI_SOAP11_ENC);
        return call;
    }

	public String getSoapActionURI() {
		return soapActionURI;
	}

	public void setSoapActionURI(String soapActionURI) {
		this.soapActionURI = soapActionURI;
	}
}
