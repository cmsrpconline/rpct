package rpct.xdaq;

import java.net.URL;

/**
 * Created on 2007-04-16
 *
 * @author Michal Pietrusinski
 * @version $Id$
 */
public class XdaqApplicationInfo {
    private URL url;
    private String className;
    private int instance;
    public XdaqApplicationInfo(URL url, String className, int instance) {
        super();
        this.url = url;
        this.className = className;
        this.instance = instance;
    }
    public String getClassName() {
        return className;
    }
    public int getInstance() {
        return instance;
    }
    public URL getUrl() {
        return url;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 31;
        int result = 1;
        result = PRIME * result + ((className == null) ? 0 : className.hashCode());
        result = PRIME * result + instance;
        result = PRIME * result + ((url == null) ? 0 : url.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final XdaqApplicationInfo other = (XdaqApplicationInfo) obj;
        if (className == null) {
            if (other.className != null)
                return false;
        } else if (!className.equals(other.className))
            return false;
        if (instance != other.instance)
            return false;
        if (url == null) {
            if (other.url != null)
                return false;
        } else if (!url.equals(other.url))
            return false;
        return true;
    }    

    @Override
    public String toString() {
    	return (new StringBuilder("URL = ")).append(url).append(", classname = ")
    	.append(className).append(", instance = ").append(instance).toString();
    }
    
}