package rpct.testAndUtils;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import rpct.datastream.*;
import cern.jet.random.Poisson;

public class LBMonteCarlo {
	static final int stripCnt = 3*96; //GEM 128
	static final int stripsPerPartion = 8; //GEM 4
	static final int partitionsCnt = stripCnt/stripsPerPartion;
	
	static final int bxPerFrame = 1; //GEM 1
	
	private static int hitsCnt = 0; 
	/**
	 * @param hitProb hit probability per BX
	 * @param clusterSize
	 * @param randomGen
	 * @return LMuxBxDataImplXml.Set - the strip data are already dvided into partions, everything is easier in this way
	 */
	static LMuxBxDataImplXml.Set generateBxData(double hitProb, int clusterSize, Random randomGen) {
		LMuxBxDataImplXml.Set lMuxBxDatas = new LMuxBxDataImplXml.Set();
		int clustersCnt =  Poisson.staticNextInt(hitProb);
		hitsCnt += clustersCnt;
		for(int iCluster = 0; iCluster < clustersCnt; iCluster++) {
			int firstStrip = randomGen.nextInt(stripCnt);
			LMuxBxDataImplXml lMuxBxData = new LMuxBxDataImplXml();
			int partitionNum = firstStrip/stripsPerPartion;
			lMuxBxData.setPartitionNum(partitionNum);
			lMuxBxData.setPartitionData(0xf);
			lMuxBxDatas.add(lMuxBxData);
			
			if(clusterSize > stripsPerPartion) {
				throw new RuntimeException("clusterSize > stripsPerPartion not implemented");
			}
			//correct for cluster size > stripsPerPartion
			if(partitionNum != (firstStrip + clusterSize -1) / stripsPerPartion  && //if the cluster is outside one partition 
					partitionNum != partitionsCnt-1) {
				LMuxBxDataImplXml lMuxBxData2 = new LMuxBxDataImplXml();
				lMuxBxData2.setPartitionNum(partitionNum + 1);
				lMuxBxData2.setPartitionData(0xf);
				lMuxBxDatas.add(lMuxBxData2);
			}
		}
		return lMuxBxDatas;
	}
	
	static void coderMC(int bxCnt, double hitProb, int clusterSize) {
		int inputPartitionsCnt = 0;
		int outputPartitionsCnt = 0;
		hitsCnt = 0;
		Random randomGen = new Random();
		TreeMap<Integer, LMuxBxDataImplXml.Set> stripBxDataMap = new TreeMap<Integer, LMuxBxDataImplXml.Set>();
		for(int iBx = 0; iBx < bxCnt; iBx++) {
			LMuxBxDataImplXml.Set lMuxBxDatas = generateBxData(hitProb, clusterSize, randomGen);
			if(lMuxBxDatas.size() > 0) {
				stripBxDataMap.put(iBx, lMuxBxDatas);
				inputPartitionsCnt += lMuxBxDatas.size();
			}
		}
		
		TreeMap<Integer, LMuxBxDataImplXml.Set> codedeData = LinkCoderAlgoritms.code(stripBxDataMap, 0, 0, bxPerFrame, 0);
		
		int eodCnt = 0;
		for(Map.Entry<Integer, LMuxBxDataImplXml.Set> es : codedeData.entrySet()) {
			for(LMuxBxDataImplXml lMuxBxData : es.getValue()) {
				outputPartitionsCnt++;
				if(lMuxBxData.getEndOfData() != 0) {
					eodCnt++;
				}
			}
		}
		
		//LinkCoderAlgoritms.printCoder(stripBxDataMap, codedeData, stripsPerPartion, partitionsCnt);
		
		System.out.println("stripCnt " + stripCnt);
		System.out.println("stripsPerPartion " + stripsPerPartion);
		System.out.println("bxPerFrame " + bxPerFrame);
		System.out.println("LinkCoderAlgoritms.maxDelay " + LinkCoderAlgoritms.maxDelay);
		System.out.println("");
		
		System.out.println("bxCnt " + bxCnt + " hitProb " + hitProb + " clusterSize " + clusterSize);
		System.out.println("hitsCnt " + hitsCnt + " hitsCnt/bx " + hitsCnt/(double)bxCnt);
		System.out.println("partitions / hit " + inputPartitionsCnt/(double)hitsCnt);
		System.out.println("inputPartitionsCnt " + inputPartitionsCnt + ",  inputPartitionsCnt/bx " + inputPartitionsCnt/(double)bxCnt);		
		System.out.println("outputPartitionsCnt " + outputPartitionsCnt + ",  outputPartitionsCnt/bx " + outputPartitionsCnt/(double)bxCnt);		
		System.out.println("lost partitions " + (inputPartitionsCnt - outputPartitionsCnt) + ",  lost partitions/inputPartitionsCnt " + (inputPartitionsCnt - outputPartitionsCnt)/(double)inputPartitionsCnt);
		
		System.out.println(" eodCnt " + eodCnt + " eodCnt/inputPartitionsCnt " + eodCnt/(double)inputPartitionsCnt);
		
		System.out.println("");
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {		
		int clusterSize = 2; //GEM 3
		for(int i = 0; i < 5; i++)
			//coderMC(10000000, 0.091, clusterSize);
		
		//coderMC(10000000, 0.02, clusterSize);
		
		//coderMC(10000000, 0.05, clusterSize);
		
		//coderMC(10000000, 0.1, clusterSize);
		
		coderMC(10000000, 0.2, clusterSize);
	}

}
