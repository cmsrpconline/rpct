/**
 * Created on 2010-03-14
 *
 * @author Karol Bunkowski
 * @version $Id: DynamicHistogram.java 1563 2010-02-08 12:30:40Z kbunkow $
 */
package rpct.testAndUtils;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class DynamicHistogram {
	public class Bin {
		public double lowerEdge;
		public double upperEdge;
		public double value;
		
		public Bin(double lowerEdge, double upperEdge, double value) {
			super();
			this.lowerEdge = lowerEdge;
			this.upperEdge = upperEdge;
			this.value = value;
		}
		
		public String toString() {
			String str = lowerEdge + "\n";
			str += "\t" + value + "\n";			
			str += upperEdge + "\n";
			
			return str;
		}
	}
	
	private TreeMap<Double, Double> bins = new TreeMap<Double, Double>();

	public DynamicHistogram(boolean overAndUnderflowBins) {
		if(overAndUnderflowBins) {
			addBin(Double.NEGATIVE_INFINITY,0);
			addBin(Double.POSITIVE_INFINITY,0);
		}
	}
	
	public void addBin(double binLowerEdge, double binValue) {
		bins.put(binLowerEdge, binValue);
	}

	public Bin getBin(double x) {
		if(bins.size() < 2) {
			throw new RuntimeException("bins.size() < 2");
		}
		Iterator<Map.Entry<Double, Double> > it = bins.entrySet().iterator();
		Map.Entry<Double, Double> preVentry = null;
		Map.Entry<Double, Double> thisEntry = ( Map.Entry<Double, Double> )it.next();
		while(it.hasNext()) {
			 preVentry = thisEntry;
			 thisEntry = ( Map.Entry<Double, Double> )it.next();
			 if(x >= preVentry.getKey() && x < thisEntry.getKey()) {
				 Bin bin = new Bin(preVentry.getKey(), thisEntry.getKey(), preVentry.getValue());
				 return bin;
			 }
		}
		
		throw new RuntimeException("the given x value is not in the range of the bins");
	}
	
	public void fill(double x, double weight) {
		if(bins.size() < 2) {
			throw new RuntimeException("bins.size() < 2");
		}
		Iterator<Map.Entry<Double, Double> > it = bins.entrySet().iterator();
		Map.Entry<Double, Double> preVentry = null;
		Map.Entry<Double, Double> thisEntry = ( Map.Entry<Double, Double> )it.next();
		while(it.hasNext()) {
			 preVentry = thisEntry;
			 thisEntry = ( Map.Entry<Double, Double> )it.next();
			 if(x >= preVentry.getKey() && x < thisEntry.getKey()) {
				 preVentry.setValue(preVentry.getValue() + weight);
				 return;
			 }
		}
		
		throw new RuntimeException("the given x = " + x + " value is not in the range of the bins");
	}
	
	public String toString() {
		String str = "";
		for(Map.Entry<Double, Double> e : bins.entrySet()) {
			str += e.getKey() + "\n";
			str += "\t" + e.getValue() + "\n";;			
		}
		return str;
	}
	
	public static void main(String[] args) {
		//testing
		DynamicHistogram dynamicHistogram = new DynamicHistogram(true);
		dynamicHistogram.addBin(-1, 0.1);
		dynamicHistogram.addBin(-0.9, 0.1);
		dynamicHistogram.addBin(0, 1.1);
		dynamicHistogram.addBin(0.9, 0.11);
		dynamicHistogram.addBin(1, 0.);
		
		System.out.println(dynamicHistogram.toString());
		
		double x = -0.99;
		Bin bin = dynamicHistogram.getBin(-0.99);
		System.out.println("bin for x " + x + "\n" + bin);
		
	}
}
