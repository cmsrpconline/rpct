package rpct.testAndUtils;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqApplicationInfo;
import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.AccessType;
import rpct.xdaqaccess.Board;
import rpct.xdaqaccess.Crate;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.DeviceDesc;
import rpct.xdaqaccess.DeviceItem;
import rpct.xdaqaccess.DeviceItemArea;
import rpct.xdaqaccess.DeviceItemType;
import rpct.xdaqaccess.DeviceItemWord;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.axis.DeviceImplAxis;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;
import rpct.xdaqaccess.synchro.DefOut;

public class IIRegistersTest {
    private List<Device> devices;

    private Set<String> skippedRegisters = new TreeSet<String>();

    private Set<String> setTo0Registers = new TreeSet<String>();

    private static String[] testDataTab = {     
        new String("10101010101010101010101001011010101010101010101010100101"),
        new String("81818181818181818181818181818181818181818181818181818181"),  
        new String("adaadadadadadadadadadadadadadaddadadadadadadadadadadadad"),                
        new String("1e57234cd45a315edace57a35c361e57234cd45a315edace57a35c36"),
        //new String("8ae912838ad3fe4a624674fa74698ae912838ad3fe4a624674fa7469"),
        //new String("ce57a34cd459318edace57a34cd2ce57a34cd459318edace57a34cd2"), 
        //          0x101010,
        //          0x010101,
        //          0x123456,
        //          0x654321,
        //          0x999999,
        //          0x666666,
        //          0x777777,
        //          0xEEEEEE
    };   

    public IIRegistersTest(List<Device> devices) {
        super();        
        this.devices = devices;

        skippedRegisters.add("ALTERA");
        skippedRegisters.add("ALTERA_BOOT");
        skippedRegisters.add("I2C");
        skippedRegisters.add("REC_ERROR_COUNT");
        skippedRegisters.add("I2C_ADDR");
        skippedRegisters.add("STAT_TRIG_COUNT");
        skippedRegisters.add("BOOT_INIT");
        
        //skippedRegisters.add("MEM_DAQ_DIAG");        
    }

    static List<Crate> getCratesNoDB() throws MalformedURLException, RemoteException, ServiceException {
        HardwareRegistry hardwareRegistry = new HardwareRegistry();
        hardwareRegistry.registerXdaqApplication(new XdaqApplicationInfo(new URL("http://pccms8.igf.fuw.edu.pl:1972"), "XdaqTestBench", 2));            

        List<Crate> crates = hardwareRegistry.getAllCrates();

        return crates;
    }

    static List<Crate> getCratesDB() throws DataAccessException, RemoteException, ServiceException {
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);        	                   

        List<Crate> crates = new ArrayList<Crate>();

        crates.add(dbMapper.getCrate(equipmentDAO.getCrateByName("PASTEURA_TC")).getHardwareCrate());

        return crates;
    }

    void readAndComapre(Device device, DeviceItem deviceItem, Binary writtenValue) throws RemoteException, ServiceException {
        if(deviceItem.getType() == DeviceItemType.WORD) {
            for(int i = 0; i < ((DeviceItemWord)deviceItem).getNumber(); i++) {
                Binary readValue = ((DeviceItemWord)deviceItem).readBinary(i);

                if(writtenValue.toString().equals(readValue.toString()) == false) {
                    DefOut.out.println(device.getBoard().getName() + " " + device.getBoard().getId() + " " +
                            device.getName() + " " + device.getId() + " " + 
                            deviceItem.getName() + " " + deviceItem.getNumber() + " " + 
                            "writtenValue " + writtenValue + " readValue " + readValue);
                }				
            }

        }
        Binary readValue = deviceItem.readBinary();        
        if(writtenValue.toString().equals(readValue.toString()) == false) {
            DefOut.out.println(device.getBoard().getName() + " " + device.getBoard().getId() + " " +
                    device.getName() + " " + device.getId() + " " + 
                    deviceItem.getName() + " " + deviceItem.getNumber() + " " + 
                    "writtenValue " + writtenValue + " readValue " + readValue);
        }	
    }

    String makeString(int size, String pattern) {
        String str = new String();
        for(int i = 0; i < size/pattern.length(); i++)
            str = pattern + str;
        str = pattern.substring(0, size%pattern.length()) + str;
        if(size != str.length())
            throw new RuntimeException("size != str.length()");
        return str;
    }

    void testRegisters(String[] testDataTab, boolean checkFirst0) throws RemoteException, ServiceException {
        for(Device device : devices) {    
            if(device.getType().equals("TB3CONTROL"))
                continue;
            if (device.getDeviceDesc().getItems() == null) {
            	// force loading device items            	
            	((DeviceImplAxis)device).loadItems();
            }
            for (DeviceDesc.ItemDesc itemDesc : device.getDeviceDesc().getItems()) {
                DeviceItemType type = itemDesc.getType();              
                if( (skippedRegisters.contains(itemDesc.getName()) ) == false &&
                        (itemDesc.getType().equals(DeviceItemType.WORD) || 
                                //deviceItem.getType().equals(DeviceItemType.VECT) ||
                                //itemDesc.getType().equals(DeviceItemType.BITS) || 
                                itemDesc.getType().equals(DeviceItemType.AREA)) &&                       
                                (itemDesc.getAccessType().equals(AccessType.BOTH) || 
                                        itemDesc.getAccessType().equals(AccessType.WRITE))) {

                    if (device.getType().equals("TB3OPTO") && itemDesc.getName().equals("MEM_DAQ_DIAG"))
                        continue; //no daq now on opto!!!!!
                    
                    int bitsCnt = itemDesc.getWidth();
                    if(checkFirst0) {    
                        String str = new String();
                        StringBuilder builder = new StringBuilder();
                        Binary writtenValue = Binary.valueOf(makeString(bitsCnt/4 +1, "0000"), bitsCnt);						
                        if(itemDesc.getType() == DeviceItemType.WORD) {		                        	
                            for(int i = 0; i < itemDesc.getNumber(); i++) {
                                Binary readValue = ((DeviceItemWord)device.getItem(itemDesc)).readBinary(i);                		        
                                if(writtenValue.toString().equals(readValue.toString()) == false) {
                                    DefOut.out.println(device.getBoard().getName() + " " + device.getBoard().getId() + " " +
                                            device.getName() + " " + device.getId() + " " + 
                                            itemDesc.getName() + " " + itemDesc.getNumber() + " " + 
                                            "writtenValue " + writtenValue + " readValue " + readValue);
                                }				
                            }
                        }
                        if (itemDesc.getType() == DeviceItemType.AREA) {
                            Binary[] readValue = ((DeviceItemArea)device.getItem(itemDesc)).readAreaBinary(); 							
                            for(int i = 0; i < itemDesc.getNumber(); i++) {								               		       
                                if(writtenValue.toString().equals(readValue[i].toString()) == false) {
                                    DefOut.out.println(device.getBoard().getName() + " " + device.getBoard().getId() + " " +
                                            device.getName() + " " + device.getId() + " " + 
                                            itemDesc.getName() + " " + i + " " + 
                                            "writtenValue " + writtenValue + " readValue " + readValue[i]);
                                }				
                            }
                        }
                        else {                            
                            Binary readValue = device.getItem(itemDesc).readBinary();	                        
                            if(writtenValue.toString().equals(readValue.toString()) == false) {
                                DefOut.out.println(device.getBoard().getName() + " " + device.getBoard().getId() + " " +
                                        device.getName() + " " + device.getId() + " " + 
                                        itemDesc.getName() + " " + itemDesc.getNumber() + " " + 
                                        " first readValue: " + readValue + " not equal 0");
                            }     
                        }
                    }
                    for (String testData : testDataTab) {                                 	                                        	
                        Binary writtenValue = Binary.valueOf(makeString(bitsCnt/4 + 1, testData), bitsCnt);
                        if(itemDesc.getType() == DeviceItemType.WORD) {      
                        	DeviceItemWord item = (DeviceItemWord)device.getItem(itemDesc);
                            for(int i = 0; i < item.getNumber(); i++) {                            	
                                item.writeBinary(i, writtenValue);                			
                                Binary readValue = item.readBinary(i);                		        
                                if(writtenValue.toString().equals(readValue.toString()) == false) {
                                    DefOut.out.println(device.getBoard().getName() + " " + device.getBoard().getId() + " " +
                                            device.getName() + " " + device.getId() + " " + 
                                            itemDesc.getName() + " " + itemDesc.getNumber() + " " + 
                                            "writtenValue " + writtenValue + " readValue " + readValue);
                                }				
                            }
                        }
                        if (itemDesc.getType() == DeviceItemType.AREA) {
                        	DeviceItemArea item = (DeviceItemArea)device.getItem(itemDesc);
                            Binary[] writtenValueTab = new Binary[item.getNumber()];
                            for(int i = 0; i < item.getNumber(); i++) {	
                                writtenValueTab[i] =  writtenValue;
                            }								
                            item.writeAreaBinary(writtenValueTab);							
                            Binary[] readValue = item.readAreaBinary(); 							
                            for(int i = 0; i < item.getNumber(); i++) {								               		       
                                if (writtenValue.toString().equals(readValue[i].toString()) == false) {
                                    DefOut.out.println(device.getBoard().getName() + " " + device.getBoard().getId() + " " +
                                            device.getName() + " " + device.getId() + " " + 
                                            itemDesc.getName() + " " + i + " " + 
                                            "writtenValue " + writtenValue + " readValue " + readValue[i]);
                                }				
                            }
                        }
                        else {
                            DeviceItem item = device.getItem(itemDesc);
                            item.writeBinary(writtenValue);
                            Binary readValue = item.readBinary();
                            if(writtenValue.toString().equals(readValue.toString()) == false) {
                                DefOut.out.println(device.getBoard().getName() + " " + device.getBoard().getId() + " " +
                                        device.getName() + " " + device.getId() + " " + 
                                        itemDesc.getName() + " " + itemDesc.getNumber() + " " + 
                                        "writtenValue " + writtenValue + " readValue " + readValue);
                            }
                        }
                    }
                }                                  
            }
        }   
    }

    void test() throws RemoteException, ServiceException {
        String[] testDataTab0 = { new String("000000000000000000000000000000000000000000000000000000000000") };
        testRegisters(testDataTab0, false);

        testRegisters(testDataTab, true);

        testRegisters(testDataTab0, false);
    }

    void simpleTest() throws RemoteException, ServiceException {
        Random rndGen = new Random();
       
        String[] registers = {
                "VREC_DATA_DELAY.REC_DATA_DELAY",
                "VREC_FAST_DATA_DELAY.REC_FAST_DATA_DELAY",
                "REC_CHECK_ENA",
                "STATUS.TRG_DATA_SEL"
        };
        
        for(String register : registers) {
            int testsCnt = 0;
            int notMatch = 0;
            System.out.println("Testing the register " + register);
            for(Device device : devices) {    
                if(device.getType().equals("TB3OPTO")) {                
                    device.getItem("STATUS.REC_SYNCH_REQ").write(0);
                    device.getItem("TLK.ENABLE").write(0);
                    device.getItem("TLK.LCK_REFN").write(0);
                    

                    for(int i = 0; i < 100; i++) {
                        testsCnt++;
                        long delayWord = rndGen.nextInt((1 << device.getItem(register).getDesc().getWidth()) -1);
                        device.getItem(register).write(delayWord);
                        //DefOut.out.println("del " + del);
                        long delayWordRead = device.getItem(register).read();
                        if(delayWord != delayWordRead) {                        
                            System.out.println(device.getBoard().getName() + " " + device.getName()  + " simpleTest: word written " + Long.toHexString(delayWord) + " readOut " + Long.toHexString(delayWordRead) + "!!!!!!!!!!!!!");
                            notMatch++;
                        } 
                    }
                }

            }
        
            System.out.println(register +" errors rate =  " + notMatch + "/" + testsCnt + " = " + ((double)notMatch)/testsCnt);
        }
    }

    public static void main(String[] args) throws MalformedURLException {
        try {
        	FileOutputStream file = new FileOutputStream(System.getenv("HOME") + "/bin/out/IIregistersTests.txt", false);
            PrintStream prntSrm = new PrintStream(file);
            DefOut defLog= new DefOut(prntSrm);
            
            HibernateContext context = new SimpleHibernateContextImpl();
            EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
            HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
            if(args.length == 0) {            
                String[] argsL = {
                        //"TC_0",
//                        "TC_1",
//                        "TC_2",
//                        "TC_3",
//                        "TC_4",
//                        "TC_5",
//                        "TC_6",
//                        "TC_7",
//                        "TC_8",                             
//                        "TC_9",
//                        "TC_10",
//                        "TC_11",
                        "PASTEURA_TC"
                        //"LBB_RB+1_S3"
                		//"PASTEURA_LBB"                      
                        };
                
                args = argsL;
            }
            
            System.out.println("Selected crates");
            
            List<Device> devices = new ArrayList<Device>();
            List<Crate> crates = new ArrayList<Crate>();
            
            for(int i = 0; i < args.length; i++) {
                System.out.print(args[i] + " ");
                crates.add((dbMapper.getCrate(equipmentDAO.getCrateByName(args[i]))).getHardwareCrate());
            }
            
            
            for (Crate crate : crates) {
                System.out.println("Crate " + crate.getName());              
                for(Board board : crate.getBoards()) {                  
                    for(Device device : board.getDevices()) {
                        if(device.getType().contains("VME") == false)
                            devices.add(device);
                    }
                    //break;
                }
            }

            IIRegistersTest registersTest = new IIRegistersTest(devices);
            registersTest.test();
            //registersTest.simpleTest();

            System.out.println("end");
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}         
    }

}
