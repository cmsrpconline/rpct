package rpct.testAndUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import rpct.db.DataAccessException;
import rpct.db.domain.equipment.LinkBoard;
import rpct.testAndUtils.SplitersXlsxGenerator.LongFiber;


public class SplitersConnectionGen {
	static class Connection {
		public String linkName;
		public String sbNum;
		public String outNum;
		public String outName;
		public String fiberName;		
	}
	
	static List<Connection> tbConnections = new ArrayList<SplitersConnectionGen.Connection>();
	static List<Connection>  omtfConnections = new ArrayList<SplitersConnectionGen.Connection>();
	static List<Connection>  tmConnections = new ArrayList<SplitersConnectionGen.Connection>();
	static List<Connection>  cppfConnections = new ArrayList<SplitersConnectionGen.Connection>();
		
	static void addConnection(
			String linkName,
			String sbNum,
			String outNum,
			String outName,
			String fiberName) {
		Connection connection = new Connection();
		connection.linkName = linkName;
		connection.sbNum = sbNum;
		connection.outNum = outNum;
		connection.outName = outName;
		connection.fiberName = fiberName;
		
		if(outName.contains("TB"))
			tbConnections.add(connection);
		else if(outName.contains("OMTF"))
			omtfConnections.add(connection);
		else if(outName.contains("TwinMux"))
			tmConnections.add(connection);
		else if(outName.contains("CPPF"))
			cppfConnections.add(connection);				
	}
			
	static void writeConnectionsByType(XSSFSheet sheet, List<Connection> connections) {
		int iRow = 0;
		Row row = sheet.createRow(iRow++);
		row.createCell(0).setCellValue("LinkName");
		row.createCell(1).setCellValue("SB num");
		row.createCell(2).setCellValue("out name");
		row.createCell(3).setCellValue("out num");
		row.createCell(4).setCellValue("fiber name");
		
		for(Connection entry : connections ) {
			row = sheet.createRow(iRow++);
			row.createCell(0).setCellValue(entry.linkName);
			row.createCell(1).setCellValue(Integer.decode(entry.sbNum));
			row.createCell(2).setCellValue(entry.outName);
			row.createCell(3).setCellValue(entry.outNum);
			row.createCell(4).setCellValue(entry.fiberName);
		}
	}
			
	static void writeConnectionsToXml(String xlsFileName) throws IOException {
		File myFile = new File(xlsFileName);
		FileInputStream fis = new FileInputStream(myFile);

		XSSFWorkbook myWorkBook = new XSSFWorkbook (fis);
		XSSFSheet mySheet = myWorkBook.getSheetAt(0);
		
		int aOutNum = 0;
		int bOutNum = 0;
		int previousSBNum = 0;
    	for(int iRow = 1; iRow < 47; iRow++) {       		
    		Row row = mySheet.getRow(iRow);
    		if(row.getCell(CellReference.convertColStringToIndex("A")) ==  null)
    			break;
    		
    		String linkName = row.getCell(CellReference.convertColStringToIndex("F")).getStringCellValue();
    		
    		int currentSBNum = (int)row.getCell(CellReference.convertColStringToIndex("A")).getNumericCellValue();
    		if(previousSBNum != currentSBNum) {
    			aOutNum = 0;
    			bOutNum = 0;	
    			previousSBNum = currentSBNum;
    		}
    		
    		Cell psOut = row.getCell(CellReference.convertColStringToIndex("I")); 
    		if(psOut.getStringCellValue().length() > 0) {
    			Cell psOutNum = row.getCell(CellReference.convertColStringToIndex("J"));
    			psOutNum.setCellValue("B " + bOutNum++);
    			
    			addConnection(linkName, 
    					Integer.toString(currentSBNum), 
    					psOutNum.getStringCellValue(), 
    					psOut.getStringCellValue(), 
    					"");
    		}
    		
    		int asOutColIndx = CellReference.convertColStringToIndex("N");
    		for(int i = 0; i < 8; i++) {
    			Cell asOut = row.getCell(asOutColIndx);
    			int asType = 0;
				if(i < 4)
					asType = (int)row.getCell(CellReference.convertColStringToIndex("L")).getNumericCellValue();
				else
					asType = (int)row.getCell(CellReference.convertColStringToIndex("M")).getNumericCellValue();
				Cell asOutNum = row.getCell(asOutColIndx+2); 
    			if(asOut.getStringCellValue().length() > 0) {    				   				    		
    				if(asType == 1)
    					asOutNum.setCellValue("B " + bOutNum++);
    				else if(asType == 2)
    					asOutNum.setCellValue("B " + bOutNum++);    				
    			}
    			if(asType == 4)
					asOutNum.setCellValue("A " + aOutNum++);
    			
    			if(asOutNum.getStringCellValue().equals("") == false)
    				addConnection(linkName, 
    						Integer.toString(currentSBNum), 
    						asOutNum.getStringCellValue(), 
    						asOut.getStringCellValue(),     						
    						row.getCell(asOutColIndx+1).getStringCellValue());
    			asOutColIndx += 3;
    		}

    	}
    	
    	writeConnectionsByType(myWorkBook.createSheet("TB_conn"), tbConnections);
    	writeConnectionsByType(myWorkBook.createSheet("OMTF_conn"), omtfConnections);
    	writeConnectionsByType(myWorkBook.createSheet("TM_conn"), tmConnections);
    	writeConnectionsByType(myWorkBook.createSheet("CPPF_conn"), cppfConnections);
    	
    	myWorkBook.setForceFormulaRecalculation(true);
    	File splitersFile = new File(xlsFileName.replace("v2", "v3"));
    	FileOutputStream os = new FileOutputStream(splitersFile);
        myWorkBook.write(os);
        
        os.close();
        myWorkBook.close();
        
        System.out.println("Writing on XLSX file " + splitersFile.getName() + " Finished ...");
        fis.close();
        
	}
	
	
    public static void main(String[] args) throws DataAccessException {
    	try {
			writeConnectionsToXml("/nfshome0/kbunkow/rpc_links/Splitters_TC_6_v2_as_used_inP5.xlsx");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
}
