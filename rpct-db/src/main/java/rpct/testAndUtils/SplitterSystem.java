package rpct.testAndUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.LinkDisabled;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;

public class SplitterSystem implements Serializable {
	private static final long serialVersionUID = 6091487835461091426L;
	
	static EquipmentDAO equipmentDAO;
	static ConfigurationManager configurationManager;
	
	static Map<String, LinkBoard> linkBoardsByName = new TreeMap<String, LinkBoard>();
	
	enum SplitterOutputType {
		PS,
		Direct,
		AS2,
		AS4, 
	}
	
	static public SplitterOutputType asWays(int ways) {
		if(ways == 1)
			return SplitterOutputType.Direct;
		if(ways == 2)
			return SplitterOutputType.AS2;
		else if(ways == 4)
			return SplitterOutputType.AS4;
		throw new RuntimeException(ways + " is wrong ways number");
	}

	public class OutputFiber implements Serializable {
		private static final long serialVersionUID = 4186941432320469619L;
		
		public SplitterOutput splitterOutput;
	}
	
	public class TBFiber extends OutputFiber {
		private static final long serialVersionUID = 3442201075465793832L;
		
		public int linkConnId;
		
		public String label;
		
		LinkConn getLinkConn() throws DataAccessException {
			return (LinkConn) equipmentDAO.getLinkConnById(linkConnId);
		}
		
		public String toString() {
			try {
				return "OutputFiber: linkConnId " + linkConnId + " label " + label + " " + getLinkConn().getBoard().getName() + " splitterOut " + splitterOutput;
			} catch (DataAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new RuntimeException(e.getMessage());
			}
		}
	}
	
	public class MtfFiber extends OutputFiber {
		private static final long serialVersionUID = -6543612164017770019L;
		
		public String type;
		
		public String board;
		
		public int fiberChennel;
		
		public int firmwareChennel;
		
		public String cableLabel;
		
		public String fiberColor;
		
		public TBFiber formerFiber;
		
		public String getLabel() {
			if(type.equals("TwinMux"))
				return board + " " + fiberChennel;
			return cableLabel + " " + fiberChennel;
		}
		
		public String toString() {
			return "OutputFiber: type " + type + " board " + board + " fiberChennel " + fiberChennel + " firmwarechennel " 
		+ firmwareChennel + " cableLabel " + cableLabel; // + " splitterOut " + splitterOutput;
		}
	}
	
	public class SplitterOutput implements Serializable {
		private static final long serialVersionUID = 8793871969815355615L;
		
		SplitterOutput(Splitter splitter) {
			this.splitter = splitter;
		}
		public SplitterOutputType type;
		
		Splitter splitter;
		/**
		 * 0...7
		 */
		public int number;
		
		//front pannel label, e.g. A1
		public String name;
		
		public OutputFiber outputFiber;
		
		public String toString() {
			return "SplitterOutput: type " + type + " number " + number + " name " + name + " outputFiber " + outputFiber;
		}
	}
	
	public class InputFiber implements Serializable {
		private static final long serialVersionUID = -6597609946105680633L;

		public String trunkCableColor;
		
		public String fiberColor;
		
		//public int lbID;
		
		public String lbName;
		
/*		LinkBoard getLB() throws DataAccessException {
			return (LinkBoard) equipmentDAO.getBoardById(lbID);
		}*/
		
		public String toString() {
			return "InputFiber: " + lbName + " trunkCableColor " + trunkCableColor + " fiberColor " + fiberColor;
		}
	}
	
	public class Splitter implements Serializable {
		private static final long serialVersionUID = 7393588260071607323L;
		
		SplitterBoard splitterBoard;
		
		public int inputNumber;
		
		public InputFiber inputFiber;

		public String passiveSplitterType;
		
		public String name;
		
		public List<SplitterOutput> splitterOutputs = new ArrayList<SplitterSystem.SplitterOutput>();	
	}

	
	public class SplitterBoard implements Serializable {
		private static final long serialVersionUID = -3190331589785657949L;
		
		public String tc;
		
		public int number;
		
		List<Splitter> splitters = new ArrayList<Splitter>();
	}
	
	
	Map<String, List<SplitterBoard> > spliiterBoardsByTC = new TreeMap<String, List<SplitterBoard> >();
	
	void readSplittersXls(String xlsFileName, int tcNum) throws IOException, DataAccessException {
		System.out.println("readSplittersXls " + xlsFileName + " started");
		File myFile = new File(xlsFileName);
		FileInputStream fis = new FileInputStream(myFile);

		XSSFWorkbook myWorkBook = new XSSFWorkbook (fis);
		XSSFSheet mySheet = myWorkBook.getSheetAt(0);

		String tcName = "TC_" + tcNum; //TODO
		
		List<SplitterBoard> splitterBoards = new ArrayList<SplitterSystem.SplitterBoard>(6);
		splitterBoards.add(null); //splitter boards are enumerated from 1
		for(int i = 1; i <= 5; i++) {
			SplitterBoard splitterBoard = new SplitterBoard();
			splitterBoard.number = i;
			splitterBoard.tc = tcName;
			splitterBoards.add(splitterBoard);
		}
		
		spliiterBoardsByTC.put(tcName, splitterBoards);
		
    	for(int iRow = 1; iRow < 47; iRow++) {       		
    		Row row = mySheet.getRow(iRow);
    		if(row.getCell(CellReference.convertColStringToIndex("A")) ==  null )
    			break;
    		
    		int splitterBoardNum = (int)row.getCell(CellReference.convertColStringToIndex("A")).getNumericCellValue();
    		if(splitterBoardNum == 0)
    			break;
    		
    		SplitterBoard splitterBoard = splitterBoards.get(splitterBoardNum);
    		
    		Splitter splitter = new Splitter();
    		splitterBoard.splitters.add(splitter);
    		splitter.splitterBoard = splitterBoard;
    		splitter.inputNumber = (int)row.getCell(CellReference.convertColStringToIndex("B")).getNumericCellValue(); 
    		splitter.name = row.getCell(CellReference.convertColStringToIndex("C")).getStringCellValue();
    		
    		String lbName = row.getCell(CellReference.convertColStringToIndex("F")).getStringCellValue();
    		if(lbName.length() == 0 || lbName.equals("spare"))
    			continue;
    		splitter.inputFiber =  new InputFiber();
    		splitter.inputFiber.trunkCableColor = row.getCell(CellReference.convertColStringToIndex("D")).getStringCellValue();    		
    		splitter.inputFiber.fiberColor = row.getCell(CellReference.convertColStringToIndex("E")).getStringCellValue();
    		LinkBoard lb = linkBoardsByName.get(lbName + "_CH0");
    		if(lb == null) {
    			throw new RuntimeException("cpuld not find LB with name " + lbName);
    		}
    		splitter.inputFiber.lbName = lb.getName();
    		splitter.passiveSplitterType = row.getCell(CellReference.convertColStringToIndex("H")).getStringCellValue();  
    	
    		
    		int as1Type = (int)row.getCell(CellReference.convertColStringToIndex("L")).getNumericCellValue();
    		int as2Type = (int)row.getCell(CellReference.convertColStringToIndex("M")).getNumericCellValue();
    		
    		int colNum = CellReference.convertColStringToIndex("N");
    		for(int asOutNum = 0; asOutNum < 8; asOutNum++) {
    			String asOutName = row.getCell(colNum+2).getStringCellValue();
    			if(asOutName.length() > 0) {
    				SplitterOutput asOut = new SplitterOutput(splitter);//active splitter out
    				if(asOutNum < 4) {
    					asOut.type= asWays(as1Type);
    				}
    				else  {
    					asOut.type= asWays(as2Type);
    				}
    				asOut.name = asOutName;
    				asOut.number = asOutNum;
    				splitter.splitterOutputs.add(asOut);
    				
    				String fiberName = row.getCell(colNum).getStringCellValue();
    				if(fiberName.contains("TB")) {
    					TBFiber tbFiber = new TBFiber();
    					tbFiber.label = row.getCell(colNum +1).getStringCellValue();
    					String[] parts = fiberName.split(" ");
    					for(LinkConn conn : lb.getLinkConns()) {
    						if(conn.getCollectorBoard().getName().equals(parts[0])) {
    							if(conn.getCollectorBoardInputNum() == Integer.parseInt(parts[1])) {
    								tbFiber.linkConnId = conn.getId();
    								asOut.outputFiber = tbFiber;
    								
    		    					if(tcNum == 9) {//fix for missing labels in the TC_9
    		    						DecimalFormat decimalFormat = new DecimalFormat("00");
    		    						String tbFiberLabel = "TC" + decimalFormat.format(conn.getTriggerBoard().getTriggerCrate().getLogSector()) + "/" + 
    		    								conn.getTriggerBoard().getPosition() + 
    		    		        				"/" + decimalFormat.format(conn.getTriggerBoardInputNum());
    		    		        		row.getCell(colNum +1).setCellValue(tbFiberLabel);
    		    		        		tbFiber.label = tbFiberLabel;
    		    					}
    							}
    							else {
    								throw new RuntimeException("TB input number do not match");
    							}
    						}
    					}
    					if(tbFiber.linkConnId == 0) {
    						throw new RuntimeException("LinkConn not found");
    					}
    					
    				}
    			}
    			colNum+=3;
    		}
    		    		
    		//we want the ps to be at the end of the splitter.splitterOutputs
    		SplitterOutput psOut = new SplitterOutput(splitter); //passive splitter out
    		psOut.name = row.getCell(CellReference.convertColStringToIndex("J")).getStringCellValue();
    		if(psOut.name.length() > 0) {
    			psOut.type = SplitterOutputType.PS;
    			String outFiberName = row.getCell(CellReference.convertColStringToIndex("I")).getStringCellValue();
    			/*if(outFiberName.length() > 0) {
    				psOut.outputFiber = new MtfFiber();  
    				((MtfFiber)psOut.outputFiber).cableLabel = outFiberName;
    			}*/
    			splitter.splitterOutputs.add(psOut);
    		}
    	}
    	
        myWorkBook.close();
        fis.close();		
	}
	
	public class MtfConnection {
		public String linkBoardName;
		public int sectorOffset;
		public int firmwareChannel;
		
		MtfConnection(int firmwareChannel, int sectorOffset, String linkBoardName) {
			this.firmwareChannel = firmwareChannel;
			this.sectorOffset = sectorOffset;
			this.linkBoardName = linkBoardName;
		}
	}
	

	public List<MtfConnection> omtfConnectionsN =  new ArrayList<SplitterSystem.MtfConnection>();
	public List<MtfConnection> emtfConnectionsN =  new ArrayList<SplitterSystem.MtfConnection>();
		
	SplitterSystem() throws DataAccessException {
		List<Board> linkBoards = equipmentDAO.getBoardsByType(BoardType.LINKBOARD, false);
		for(Board board : linkBoards) {
			linkBoardsByName.put(board.getName(), (LinkBoard)board);
		}
		
		omtfConnectionsN.add(new MtfConnection(0,  0, "LB_RB-2_SXX_BN2A_CH0")); 
		omtfConnectionsN.add(new MtfConnection(1,  0, "LB_RB-2_SXX_BN2B_CH0")); 
		omtfConnectionsN.add(new MtfConnection(2,  0, "LB_RB-2_SXX_BN2C_CH0")); 
		omtfConnectionsN.add(new MtfConnection(3,  0, "LB_RB-2_SXX_BN2E_CH0")); 
		omtfConnectionsN.add(new MtfConnection(4,  1, "LB_RB-2_SXX_BN2A_CH0"));
		omtfConnectionsN.add(new MtfConnection(5,  1, "LB_RB-2_SXX_BN2B_CH0"));
		omtfConnectionsN.add(new MtfConnection(6,  1, "LB_RB-2_SXX_BN2C_CH0"));
		omtfConnectionsN.add(new MtfConnection(7,  1, "LB_RB-2_SXX_BN2E_CH0"));
		omtfConnectionsN.add(new MtfConnection(8,  2, "LB_RB-2_SXX_BN2A_CH0"));
		omtfConnectionsN.add(new MtfConnection(9,  2, "LB_RB-2_SXX_BN2B_CH0"));
		omtfConnectionsN.add(new MtfConnection(10, 2, "LB_RB-2_SXX_BN2C_CH0"));
		omtfConnectionsN.add(new MtfConnection(11, 2, "LB_RB-2_SXX_BN2E_CH0"));
		omtfConnectionsN.add(new MtfConnection(12, 0, "LB_RE-1_SXX_EN13_CH0"));
		omtfConnectionsN.add(new MtfConnection(13, 0, "LB_RE-1_SXX_EN23_CH0"));
		omtfConnectionsN.add(new MtfConnection(14, 1, "LB_RE-1_SXX_EN13_CH0"));
		omtfConnectionsN.add(new MtfConnection(15, 1, "LB_RE-1_SXX_EN23_CH0"));
		omtfConnectionsN.add(new MtfConnection(16, 0, "LB_RE-1_SXX_EN12_CH0"));
		omtfConnectionsN.add(new MtfConnection(17, 1, "LB_RE-1_SXX_EN12_CH0"));
		omtfConnectionsN.add(new MtfConnection(18, 1, "LB_RE-3_SXX_EN33_CH0"));
		omtfConnectionsN.add(new MtfConnection(19, 2, "LB_RE-1_SXX_EN13_CH0"));
		omtfConnectionsN.add(new MtfConnection(20, 0, "LB_RE-3_SXX_EN33_CH0"));
		omtfConnectionsN.add(new MtfConnection(21, 2, "LB_RE-1_SXX_EN12_CH0"));
		omtfConnectionsN.add(new MtfConnection(22, 2, "LB_RE-1_SXX_EN23_CH0"));
		omtfConnectionsN.add(new MtfConnection(23, 2, "LB_RE-3_SXX_EN33_CH0"));

		emtfConnectionsN.add(new MtfConnection( 0, 0, "LB_RE-1_SXX_EN12_CH0"));
	    //emtfConnectionsN.add(new MtfConnection( 1, 0, "LB_RE-1_SXX_EN22_CH0")); this fiber is not split, so go only to the TBs now
		emtfConnectionsN.add(new MtfConnection( 2, 0, "LB_RE-3_SXX_EN32_CH0"));
		emtfConnectionsN.add(new MtfConnection( 3, 0, "LB_RE-3_SXX_EN33_CH0"));
		emtfConnectionsN.add(new MtfConnection( 4, 0, "LB_RE-4_SXX_EN42_CH0"));
		emtfConnectionsN.add(new MtfConnection( 5, 0, "LB_RE-4_SXX_EN43_CH0"));
		
		emtfConnectionsN.add(new MtfConnection( 6, 1, "LB_RE-1_SXX_EN12_CH0"));
		emtfConnectionsN.add(new MtfConnection( 7, 1, "LB_RE-1_SXX_EN22_CH0"));
		emtfConnectionsN.add(new MtfConnection( 8, 1, "LB_RE-3_SXX_EN32_CH0"));
		emtfConnectionsN.add(new MtfConnection( 9, 1, "LB_RE-3_SXX_EN33_CH0"));
		emtfConnectionsN.add(new MtfConnection(10, 1, "LB_RE-4_SXX_EN42_CH0"));
		emtfConnectionsN.add(new MtfConnection(11, 1, "LB_RE-4_SXX_EN43_CH0"));

		emtfConnectionsN.add(new MtfConnection(12, 2, "LB_RE-1_SXX_EN12_CH0"));
		emtfConnectionsN.add(new MtfConnection(13, 2, "LB_RE-1_SXX_EN22_CH0"));
		emtfConnectionsN.add(new MtfConnection(14, 2, "LB_RE-3_SXX_EN32_CH0"));
		emtfConnectionsN.add(new MtfConnection(15, 2, "LB_RE-3_SXX_EN33_CH0"));
		emtfConnectionsN.add(new MtfConnection(16, 2, "LB_RE-4_SXX_EN42_CH0"));
		emtfConnectionsN.add(new MtfConnection(17, 2, "LB_RE-4_SXX_EN43_CH0"));
	}
	
	Map<String, List<MtfFiber> > omtfFibersConnections = new TreeMap<String, List<SplitterSystem.MtfFiber>>();//key is the LB name
	Map<String, List<MtfFiber> > emtfFibersConnections = new TreeMap<String, List<SplitterSystem.MtfFiber>>();//key is the LB name
	Map<String, MtfFiber > twinMuxFibersConnections = new TreeMap<String, SplitterSystem.MtfFiber>();
	

	String getTwinMuxFiberColor(String cableLable) {
		if(cableLable.contains("YB+1")) return "white";
		if(cableLable.contains("YB+2")) return "red";
		if(cableLable.contains("YB-1")) return "black";
		if(cableLable.contains("YB-2")) return "green";
		if(cableLable.contains("YB0")) return "blue";
		return "";
	}
	
	void generateTwinMuxFibersConnections() throws DataAccessException {
		System.out.println("generateTwinMuxFibersConnections");
		
		Map<String, Integer> w0ChannelsMap = new TreeMap<String, Integer>();
		w0ChannelsMap.put("BN0B", 0);
		w0ChannelsMap.put("BP0B", 1);
		w0ChannelsMap.put("BM0C", 2);
		w0ChannelsMap.put("BN0E", 3);
		w0ChannelsMap.put("BP0E", 4);
		List<LinkConn> linkConns = equipmentDAO.getLinkConns();
		for(LinkConn linkConn : linkConns) {
			String lbName = linkConn.getBoard().getName();
			if(lbName.contains("RB")) {
				MtfFiber mtfFiber = new MtfFiber();
				String sectorName = "YB" + lbName.substring(5, lbName.indexOf('B', 6) -1);
				mtfFiber.board = "TM " + sectorName;
				mtfFiber.firmwareChennel = lbName.charAt(lbName.length() -5) - 'A';
				if(lbName.contains("RB0")) {
					String str = lbName.substring(lbName.indexOf('B', 7), lbName.indexOf('_', 13));
					mtfFiber.firmwareChennel = w0ChannelsMap.get(str);
				}
				mtfFiber.fiberChennel = mtfFiber.firmwareChennel +1;
				mtfFiber.type = "TwinMux";
				mtfFiber.cableLabel = "TM "+ sectorName;
				mtfFiber.fiberColor = getTwinMuxFiberColor(sectorName);
				twinMuxFibersConnections.put(lbName, mtfFiber);
			}
		}
	}
	
	public static Comparator<MtfFiber> mtfFiberComparator = new Comparator<MtfFiber>() {
		public int compare(MtfFiber f1, MtfFiber f2) {
			int result = f1.cableLabel.compareTo(f2.cableLabel);
			if(result == 0)
				result = Integer.compare(f1.fiberChennel, f2.fiberChennel);
			return result;
		}
	};
		
	void printFibers(List<MtfFiber> mtfFibers) {
		Collections.sort(mtfFibers, mtfFiberComparator);
		
		for(MtfFiber fiber : mtfFibers) {
			if(fiber.splitterOutput != null) {
				System.out.print(fiber.cableLabel + "\t" + fiber.fiberColor + "\t" + fiber.fiberChennel);
				System.out.print(
					  "\t" +    fiber.splitterOutput.splitter.splitterBoard.tc
					+ "\tSB " + fiber.splitterOutput.splitter.splitterBoard.number + "\t" + fiber.splitterOutput.name
					+ "\t" +    fiber.splitterOutput.splitter.inputFiber.lbName
					+ "\t" + (fiber.formerFiber != null ? fiber.formerFiber.label : " ") );
				for(SplitterOutput splitterOutput : fiber.splitterOutput.splitter.splitterOutputs) {
					System.out.print("\t" + splitterOutput.name);
				}
			}
			else {
				System.out.print(fiber.cableLabel);
			}
			System.out.println();
		}
	}
	
	//for OMTF swatch cell
	void printFibersToFile(List<MtfFiber> mtfFibers, String fileName) throws FileNotFoundException, DataAccessException {
        FileOutputStream file = new FileOutputStream(fileName, false);
        PrintStream fout = new PrintStream(file);
		
		Collections.sort(mtfFibers, mtfFiberComparator);
		
		for(MtfFiber fiber : mtfFibers) {
			if(fiber.splitterOutput == null)//HO fibers
				continue;
			LinkBoard mlb = linkBoardsByName.get(fiber.splitterOutput.splitter.inputFiber.lbName);
    		if(mlb == null) {
    			throw new RuntimeException("cpuld not find LB with name " + fiber.splitterOutput.splitter.inputFiber.lbName);
    		}
    		
			fout.print(fiber.board + " " + fiber.firmwareChennel + " " + fiber.splitterOutput.splitter.inputFiber.lbName.replace("_CH0", "") +
					" " + mlb.getId());
			
			/*for(int i = 0; i < 3; i ++) {
				LinkBoard lb = mlb.getSlave(i);
				if(lb != null) {
					fout.print("\tCH" + i + ": ");
					for(String chamberName : lb.getChamberNames())
						fout.print(chamberName + " ");
				}
			}*/
			fout.println();
		}
		
		fout.close();
	}
	
	void printTwinMuxFibersConnections(String fileName) throws FileNotFoundException, DataAccessException {
		List<MtfFiber> twinMuxFibers = new ArrayList<MtfFiber>();
		for(Map.Entry<String, MtfFiber > entry : twinMuxFibersConnections.entrySet()) {
			twinMuxFibers.add(entry.getValue());
			//System.out.println(entry.getKey() + "  " + entry.getValue());
		}
		
		//HO fibers connections
		Map<Integer, Integer> wheelToSBMap = new TreeMap<Integer, Integer>();
		wheelToSBMap.put(-1, 1);
		wheelToSBMap.put(-2, 2);
		wheelToSBMap.put(-0, 3);
		wheelToSBMap.put(2, 4);
		wheelToSBMap.put(1, 5);
		for(int tc = 0; tc < 12; tc++ ) {
			for(int wheel = -2; wheel <= 2; wheel++) {
				for(int i = 5; i < 8; i++) {
					MtfFiber fiber = new MtfFiber();
					String sectorName = "YB" + (wheel > 0 ? "+" : "") + wheel + "_S" + (tc + 1);
					fiber.board = "TM " + sectorName;
					fiber.firmwareChennel = i;
					fiber.fiberChennel = fiber.firmwareChennel +1;
					fiber.type = "TwinMux";
					fiber.fiberColor = getTwinMuxFiberColor(sectorName);
					fiber.cableLabel = "RPC TC_" + tc + " TM "+ sectorName 
							+ "\t" + fiber.fiberColor 
							+ "\t" + fiber.fiberChennel 							
							+"\tTC_" + tc + "\tSB " + wheelToSBMap.get(wheel) + "\tB " + (i + 11) + "\tHO";
					
					twinMuxFibers.add(fiber);
				}
			}
		}//end HO fibers connections
		
		
		printFibers(twinMuxFibers);
		
		printFibersToFile(twinMuxFibers, fileName);
	}
	
	void printTwinMuxCableLables() {
		Set<String> cableLables = new TreeSet<String>();
		for(Map.Entry<String, MtfFiber > entry : twinMuxFibersConnections.entrySet()) {
			cableLables.add(entry.getValue().cableLabel);
			//System.out.println(entry.getKey() + "  " + entry.getValue());
		}
		
		for(String label : cableLables) {
			System.out.println(label);
		}
	}
	
	/**
	 * @param mtfConnectionsN omtf or emtf connections
	 * @param baordType: EMTF or OMTF
	 */
	void generateMtfConnections(List<MtfConnection> mtfConnectionsN, String baordType, Map<String, List<MtfFiber> > resultConnections) {
		for(int np = 0; np < 2; np++) {
			for(int omtfNum =  1; omtfNum <=6; omtfNum++) {
				int firstSector = omtfNum * 2 -1;
				for(int i = 0; i < mtfConnectionsN.size(); i++) {
					int sector = firstSector +  mtfConnectionsN.get(i).sectorOffset;
					if(sector > 12)
						sector-=12;
					String linkName = mtfConnectionsN.get(i).linkBoardName.replace("XX", Integer.toString(sector));
					if(np == 1) {
						linkName = linkName.replace('N', 'P');
						linkName = linkName.replace("-", "+");
					}
					MtfFiber mtfFiber = new MtfFiber();
					mtfFiber.board = baordType + (np == 0 ? "n" : "p") + omtfNum;
					mtfFiber.firmwareChennel = mtfConnectionsN.get(i).firmwareChannel;
					mtfFiber.fiberChennel = mtfFiber.firmwareChennel%12 +1;
					mtfFiber.type = baordType;
					mtfFiber.cableLabel = mtfFiber.board + " RX" + (mtfFiber.firmwareChennel/12 + 4);// + " " + mtfFiber.fiberChennel + " " + linkName;
					//TODO handle properly EMTF cables
					
					List<MtfFiber> mtfFibers = resultConnections.get(linkName);
					if(mtfFibers == null) {
						mtfFibers =  new ArrayList<SplitterSystem.MtfFiber>();
						resultConnections.put(linkName, mtfFibers);
					}
					mtfFibers.add(mtfFiber);
				}
			}
		}
	}
	
	public void printMtfFibersConnectionsByLb(Map<String, List<MtfFiber> > mtfFibersConnections) {
		for(Map.Entry<String, List<MtfFiber> > entry : mtfFibersConnections.entrySet()) {
			System.out.println(entry.getKey());
			for(MtfFiber mtfFiber : entry.getValue()) {
				System.out.println("  " + mtfFiber);
			}
		}
	}
	
	public void printMtfFibersConnectionsByCable(Map<String, List<MtfFiber> > mtfFibersConnections, String fileName) throws FileNotFoundException, DataAccessException {
		List<MtfFiber> mtfFibers = new ArrayList<MtfFiber>();
		for(Map.Entry<String, List<MtfFiber> > entry : mtfFibersConnections.entrySet()) {
			for(MtfFiber mtfFiber : entry.getValue()) {
				mtfFibers.add(mtfFiber);
			}
		}
		
		printFibers(mtfFibers);
		
		if(fileName.equals("") == false)
			printFibersToFile(mtfFibers, fileName);
	}
	
	void connectMtfFiber(SplitterOutput splitterOutput, MtfFiber fiber, boolean disableDisconnetedLinks) throws DataAccessException {
		if(splitterOutput.outputFiber != null ) {
			if((TBFiber)(splitterOutput.outputFiber) != null ) {
				if(disableDisconnetedLinks) {
					System.out.println("disconneting fiber " + splitterOutput.outputFiber);
					configurationManager.disconnectLinkConn( ((TBFiber)splitterOutput.outputFiber).getLinkConn());
				}
				fiber.formerFiber = (TBFiber)(splitterOutput.outputFiber);
			}
			else {
				throw new RuntimeException(splitterOutput + " has already connected other MtfFiber " + splitterOutput.outputFiber);
			}
		}

		splitterOutput.outputFiber = fiber;
		fiber.splitterOutput = splitterOutput;
	}
	
	void connectMtfFibers(boolean disableDisconnetedLinks) throws DataAccessException {
		for(Map.Entry<String, List<SplitterBoard> > tcSBs : spliiterBoardsByTC.entrySet()) {
			for(SplitterBoard splitterBoard : tcSBs.getValue()) {
				if(splitterBoard == null)
					continue;
								
				for(Splitter splitter : splitterBoard.splitters) {
					if(splitter.inputFiber != null) {
						String lbName = splitter.inputFiber.lbName;
						List<MtfFiber> omtfFibers = omtfFibersConnections.get(lbName);
						List<MtfFiber> emtfFibers = emtfFibersConnections.get(lbName);
						MtfFiber twinMuxFiber = twinMuxFibersConnections.get(lbName);
						int mtfConnectionsCnt =  0;
						mtfConnectionsCnt += (omtfFibers == null ? 0 : omtfFibers.size() );
						mtfConnectionsCnt += (emtfFibers == null ? 0 : emtfFibers.size());
						mtfConnectionsCnt += (twinMuxFiber == null ? 0 : 1);
						
						int spliterOutNum = splitter.splitterOutputs.size() - mtfConnectionsCnt;
						if(spliterOutNum < 1 ) {//one is for the TB connection
							System.out.println(lbName + " requires " + mtfConnectionsCnt + " mtfConnectionsCnt, but there is only  "
									//omtfFibers.size() + " omtf connections, " 
									//+ emtfFibers.size() + " emtfConnections and " + (twinMuxFiber == null ? 0 : 1) + " twinMux connections, but there is only " 
									+  splitter.splitterOutputs.size() + " avaialble splitter outputs !!!!!!!!!!!!!!!!!!!!!!!!!!");
							throw new RuntimeException();
						}
						else {
							while(splitter.splitterOutputs.get(spliterOutNum -1).outputFiber == null) {//shifting to the first empty output
								spliterOutNum--;
							}
						}
						
						
						if(twinMuxFiber != null) {
							connectMtfFiber(splitter.splitterOutputs.get(spliterOutNum), twinMuxFiber, disableDisconnetedLinks);
							
							twinMuxFiber.cableLabel = "RPC " + tcSBs.getKey() + " " + twinMuxFiber.cableLabel;
							spliterOutNum++;
						}
						
						if(omtfFibers != null) {
							for(MtfFiber fiber :omtfFibers) {								
								connectMtfFiber(splitter.splitterOutputs.get(spliterOutNum), fiber, disableDisconnetedLinks);
								spliterOutNum++;
							}
						}
						if(emtfFibers != null) {
							for(MtfFiber fiber :emtfFibers) {
								connectMtfFiber(splitter.splitterOutputs.get(spliterOutNum), fiber, disableDisconnetedLinks);
								spliterOutNum++;
							}
						}
					}
				}
			}
		}
	}
	
	
	void printSplitters() {
		for(Map.Entry<String, List<SplitterBoard> > entry : spliiterBoardsByTC.entrySet()) {
			System.out.println(entry.getKey());
			for(SplitterBoard splitterBoard : entry.getValue()) {
				if(splitterBoard == null)
					continue;
				
				System.out.println("splitterBoard tc " + splitterBoard.tc + " num " + splitterBoard.number);
				
				for(Splitter splitter : splitterBoard.splitters) {
					System.out.println("  splitter " + splitter.name + " num " + splitter.inputNumber + " " + splitter.passiveSplitterType);
					System.out.println("    " + splitter.inputFiber);
					for(SplitterOutput splitterOutput : splitter.splitterOutputs) {
						System.out.println("      " + splitterOutput);
					}
				}
			}
		}
	}
	
	
	void setCEllFormat(Cell cell, CellStyle emptyStyle, CellStyle omtfStyle, CellStyle twinMuxStyle, CellStyle cppfStyle) {
		String fibeLable = cell.getStringCellValue();
		if(fibeLable.equals("")) {
			cell.setCellStyle(emptyStyle);
		}
		else if(fibeLable.contains("OMTF")) {
			cell.setCellStyle(omtfStyle);
		}
		else if(fibeLable.contains("TM")) {
			cell.setCellStyle(twinMuxStyle);
		}
		else if(fibeLable.contains("CPPF") || fibeLable.contains("EMTF")) {
			cell.setCellStyle(cppfStyle);
		}
	}
	
	void writeConnectionsToXml(String xlsFileName, String tc) throws IOException {
		System.out.println("writeConnectionsToXml " + xlsFileName + " started");
		File myFile = new File(xlsFileName);
		FileInputStream fis = new FileInputStream(myFile);

		XSSFWorkbook myWorkBook = new XSSFWorkbook (fis);
		XSSFSheet mySheet = myWorkBook.getSheetAt(0);
		for(int i = 1; i < myWorkBook.getNumberOfSheets(); i++) {
			myWorkBook.removeSheetAt(i);
		}		
		int columnWidth = (int)(13.5*256);
		mySheet.setColumnWidth(CellReference.convertColStringToIndex("I"), columnWidth);
		mySheet.setColumnWidth(CellReference.convertColStringToIndex("Q"), columnWidth);
		mySheet.setColumnWidth(CellReference.convertColStringToIndex("T"), columnWidth);
		mySheet.setColumnWidth(CellReference.convertColStringToIndex("W"), columnWidth);
		
		CellStyle emptyStyle = mySheet.getRow(6).getCell(CellReference.convertColStringToIndex("G")).getCellStyle();
    	CellStyle omtfStyle = mySheet.getRow(54).getCell(CellReference.convertColStringToIndex("B")).getCellStyle();
    	CellStyle twinMuxStyle = mySheet.getRow(55).getCell(CellReference.convertColStringToIndex("B")).getCellStyle();
    	CellStyle cppfStyle = mySheet.getRow(56).getCell(CellReference.convertColStringToIndex("B")).getCellStyle();
    	
		List<SplitterBoard> spliiterBoards = spliiterBoardsByTC.get(tc);
    	for(int iRow = 1; iRow < 47; iRow++) {       		
    		Row row = mySheet.getRow(iRow);
    		if(row.getCell(CellReference.convertColStringToIndex("A")) ==  null)
    			break;
    		
    		String linkName = row.getCell(CellReference.convertColStringToIndex("F")).getStringCellValue();
    		
    		int sbNum = (int)row.getCell(CellReference.convertColStringToIndex("A")).getNumericCellValue();
    		SplitterBoard splitterBoard = spliiterBoards.get(sbNum);
    		int splliiterNum = (int)row.getCell(CellReference.convertColStringToIndex("B")).getNumericCellValue(); 
    		Splitter splitter = splitterBoard.splitters.get(splliiterNum);
    		if(splitter.inputNumber != splliiterNum) {
    			//System.out.println("splitter.inputNumber != splliiterNum " + splitter + "!!!!!!!!!!!!!!!!!!!!!!!!");
    			throw new RuntimeException("splitter.inputNumber != splliiterNum");
    		}
    		if(splitter.splitterOutputs.size() == 0)
    			continue;
    		
    		Cell psOut = row.getCell(CellReference.convertColStringToIndex("I")); 
    		SplitterOutput psSpliterOut = splitter.splitterOutputs.get(splitter.splitterOutputs.size() - 1); //if passive splitter exists, it is on the end
    		if(psSpliterOut.type == SplitterOutputType.PS) {
    			if(psSpliterOut.outputFiber != null) {
    				psOut.setCellValue( ((MtfFiber)psSpliterOut.outputFiber).getLabel() );
    			}
    			else {
    				psOut.setCellValue("");
    			}
				setCEllFormat(psOut, emptyStyle, omtfStyle, twinMuxStyle, cppfStyle);
    		}
    		int asOutColIndx = CellReference.convertColStringToIndex("N");
    		for(int i = 0; i < splitter.splitterOutputs.size(); i++) {
    			Cell asOut = row.getCell(asOutColIndx);
    			SplitterOutput asSpliterOut = splitter.splitterOutputs.get(i);
    			if(asSpliterOut.type != SplitterOutputType.PS 
    					&& asSpliterOut.outputFiber != null
    					&& asSpliterOut.outputFiber.getClass() == MtfFiber.class) {
    				asOut.setCellValue( ((MtfFiber)asSpliterOut.outputFiber).getLabel());
    				row.getCell(asOutColIndx +1).setCellValue("");
    			}
    			else if(asSpliterOut.type != SplitterOutputType.PS && asSpliterOut.outputFiber == null) {
    				asOut.setCellValue("");
    				row.getCell(asOutColIndx +1).setCellValue("");
    			}
				setCEllFormat(asOut, emptyStyle, omtfStyle, twinMuxStyle, cppfStyle);
    			asOutColIndx += 3;
    		}

    	}
    	
    	myWorkBook.setForceFormulaRecalculation(true);
    	File splitersFile = new File(xlsFileName.replace("v3", "v4"));
    	FileOutputStream os = new FileOutputStream(splitersFile);
        myWorkBook.write(os);
        
        os.close();
        myWorkBook.close();
        
        System.out.println("Writing on XLSX file " + splitersFile.getName() + " Finished ...");
        fis.close();
        
	}
	
    public static void main(String[] args) throws DataAccessException {
		HibernateContext context = new SimpleHibernateContextImpl();
    	try {
    		SplitterSystem.equipmentDAO = new EquipmentDAOHibernate(context);

            ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
            SplitterSystem.configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);	
    		
    		SplitterSystem splitterSystem = new SplitterSystem();
    		
    		splitterSystem.generateMtfConnections(splitterSystem.omtfConnectionsN, "OMTF", splitterSystem.omtfFibersConnections);
    		splitterSystem.printMtfFibersConnectionsByLb(splitterSystem.omtfFibersConnections);
    		

    		splitterSystem.generateMtfConnections(splitterSystem.emtfConnectionsN, "EMTF", splitterSystem.emtfFibersConnections);
    		splitterSystem.printMtfFibersConnectionsByLb(splitterSystem.emtfFibersConnections);
    		
    		splitterSystem.generateTwinMuxFibersConnections();

    		//splitterSystem.readSplittersXls("/cms904nfshome0/nfshome0/kbunkow/omtf/rpcSplitters/Splitters_TC_5_v3.xlsx");
    		for(int tc = 0; tc < 12; tc++) {
    			splitterSystem.readSplittersXls("/nfshome0/kbunkow/RPC_splitters/Splitters_TC_" + tc + "_v3.xlsx", tc);
    		}
    		
    		splitterSystem.connectMtfFibers(false); //TODO
    	
    		splitterSystem.printSplitters();
    		
    		splitterSystem.printTwinMuxCableLables();
    		
    		splitterSystem.printTwinMuxFibersConnections("TwinMuxRpcLinksMap.txt");
    		
    		splitterSystem.printMtfFibersConnectionsByCable(splitterSystem.omtfFibersConnections, "OmtfRpcLinksMap.txt");

    		splitterSystem.printMtfFibersConnectionsByCable(splitterSystem.emtfFibersConnections, "");
    		
    		for(int tc = 0; tc < 12; tc++) {
    			splitterSystem.writeConnectionsToXml("/nfshome0/kbunkow/RPC_splitters/Splitters_TC_" + tc + "_v3.xlsx", "TC_" + tc);
    		}
    		
    		/*
    		//this links were disconnected since there is a problem in the spliter
    		List<LinkConn> linkConns = ((LinkBoard)equipmentDAO.getBoardByName("LB_RB-2_S8_BN2E_CH0")).getLinkConns();
    		for(LinkConn linkConn : linkConns) {
    			if(linkConn.getCollectorBoard().getName().equals("TBn2_6") || linkConn.getCollectorBoard().getName().equals("TBn1_6")) {
    				configurationManager.disconnectLinkConn(linkConn);
    			}
    			else {
    				LinkDisabled linkDisabled = configurationDAO.getLinkDisabled(linkConn);
    				configurationDAO.deleteObject(linkDisabled);
            		System.out.println("Enabled linkConn " + linkConn);
    			}
    		}*/
    		
    		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			context.rollback();
            System.out.println("context.rollback()");
        } 
        finally {
            context.closeSession();
        }    	
    }
	
}
