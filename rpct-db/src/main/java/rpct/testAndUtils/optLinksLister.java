package rpct.testAndUtils;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import rpct.db.DataAccessException;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.synchro.DefOut;

public class optLinksLister {

    /**
     * @param args
     */
    public static void main(String[] args) {
        try {
            DefOut defLog= new DefOut(System.out);
            HibernateContext context = new SimpleHibernateContextImpl();
            EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
            HardwareDbMapper dbMapper = new HardwareDbMapper(
                    new HardwareRegistry(), context);

            TriggerCrate tc = (TriggerCrate)equipmentDAO.getCrateByName("PASTEURA_TC");
            ArrayList<TriggerBoard> tbs = new ArrayList<TriggerBoard>();
            for(int i = 0; i < 9; i++) {
                tbs.add(null);
            }
            
            for (TriggerBoard tb : tc.getTriggerBoards()) {
                tbs.set(tb.getPosition() -11, tb);
            }
            
            for (TriggerBoard tb : tbs) {
                if(tb == null) {
                    continue;
                }
                DefOut.out.println(tb.getName());
                //for (LinkConn linkConn : hdTb.getDBTriggerBoard().getLinkConns()) {

                ArrayList<String> linkConns = new ArrayList<String>(18);
                for(int i = 0; i < 18; i++) 
                    linkConns.add("");
                
                
                for (LinkConn linkConn : equipmentDAO.getLinkConns(tb, false)) {
                    linkConns.set(linkConn.getTriggerBoardInputNum(), linkConn.getBoard().getName() + " " + (linkConn.isDisabled() ? "disabled" : "enabled"));
                }
                
/*                for (LinkConn linkConn : equipmentDAO.getLinkConns(tb, false)) {
                    DefOut.out.println("inputNum "
                            + linkConn.getTriggerBoardInputNum() + " "
                            + linkConn.getBoard().getName());
                }*/
                for(int i = 0; i < 18; i++) {
                    DefOut.out.println("inputNum " + i + " " + linkConns.get(i));
                }

            }
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
