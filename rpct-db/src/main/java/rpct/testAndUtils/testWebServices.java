package rpct.testAndUtils;

import java.util.List;

import org.apache.log4j.Logger;

import rpct.testAndUtils.boardConfigs.ConfUtil;

/**
 * Created on 2009-12-03
 * 
 * @author Nikolay Darmenov
 * @version $Id$
 */
public class testWebServices {
    static final Logger logger = Logger.getLogger(testWebServices.class);
    
	public static void main(String[] args) {
		logger.info("Will test web services ...");

		// Some FEB chips
		final List<Integer> chipIds = ConfUtil.getIntListProperty("chipIds");
		final String globalConfigKey = "DEFAULT";

		try {
			ConfUtil.wsGetConfigurationSet(ConfUtil.getProperty("url"), chipIds, globalConfigKey );
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			logger.info("Call ends.");
		}
	}
}

