package rpct.testAndUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;

import org.apache.axis.types.UnsignedByte;

import rpct.datastream.MonitorManager;
import rpct.datastream.RpctDelays;
import rpct.datastream.TestOptionsXml.ChipSource;
import rpct.datastream.TestOptionsXml.HardwareType;
import rpct.datastream.TestOptionsXml.SourcesList;
import rpct.datastream.TestsManager.PulserTarget;
import rpct.db.DataAccessException;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.SorterCrate;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.diag.TriggerDataSel;
import rpct.xdaqaccess.diag.DiagCtrl.TriggerType;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;
import rpct.xdaqaccess.synchro.DefOut;

public class setRegistersOnTB {

    public static void main(String[] args) throws ServiceException,
    DataAccessException, IOException, InterruptedException,
    XdaqException, HardwareDbException {
        String xmlFileName =  System.getenv("HOME") + "/bin/data/gentestpulses.xml";

        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(
                new HardwareRegistry(), context);

        try {
            
            if(args.length == 0) {            
                String[] argsL = {
                        "TC_0",                    
                        "TC_1",
                        "TC_2",                        
                        "TC_3",                    
                        "TC_4",
                        "TC_5",                        
                        "TC_6",                    
                        "TC_7",
                        "TC_8",                        
                        "TC_9",                    
                        "TC_10",
                        "TC_11",
                        };
                
                args = argsL;
            }

            System.out.println("Selected crates");
            for(int i = 0; i < args.length; i++) {
                System.out.print(args[i] + " ");
            }
            
            for(int i = 0; i < args.length; i++) {                
                HardwareDbTriggerCrate tcHdb = (HardwareDbTriggerCrate)dbMapper.getCrate(equipmentDAO.getCrateByName(args[i]));
                for(HardwareDBTriggerBoard tb : tcHdb.getHardwareDBTriggerBoards()) {
                	//if(tb.getName().contains("TB0")) 
                	{
                		for(HardwareDbChip pac : tb.getPacs()) {
                			//pac.getHardwareChip().getItem("!!!!!").write(0); //TODO
                		}

                		for(HardwareDbChip opto : tb.getOptos()) {
                			opto.getHardwareChip().getItem("SEND_TEST_RND_ENA").write(0); //TODO
                			opto.getHardwareChip().getItem("SEND_TEST_ENA").write(0);
                		}
                		//tb.getRmb().getHardwareChip().getItem("!!!!!").write(0);
                	}
                }                
            }           
           
        } finally {
            context.closeSession();
        }
    }

}
