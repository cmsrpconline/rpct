package rpct.testAndUtils;

import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.axis.types.HexBinary;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.SynCoderConf;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.ConnType;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.FebConnector;
import rpct.db.domain.equipment.I2cCbChannel;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.chamberloaction.BarrelOrEndcap;
import rpct.db.domain.equipment.chamberloaction.ChamberLocation;
import rpct.db.domain.equipment.chamberloaction.ChamberLocationDAO;
import rpct.db.domain.equipment.chamberloaction.ChamberLocationDAOHibernate;
import rpct.db.domain.equipment.chamberstrip.ChamberStrip;
import rpct.db.domain.equipment.chambertype.ChamberType;
import rpct.db.domain.equipment.controlboard.ControlBoard;
import rpct.db.domain.equipment.feblocation.FebLocation;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;

public class MakeRE11 {
	static LinkBoard makeLB(boolean saveToDB, EquipmentDAO equipmentDAO, 
			LinkBox linkBox, String name, int position,
			ChamberLocationDAO chamberLocationDAO, ConfigurationDAO configurationDAO, ConfigurationManager configurationManager) throws DataAccessException {
		
		LinkBoard newLinkBoard = new LinkBoard();		                    		                   		                   
        newLinkBoard.setType(BoardType.LINKBOARD);	
        newLinkBoard.setPosition(position);
        newLinkBoard.setName(name);
        newLinkBoard.setLabel(name);
        if(newLinkBoard.isMaster()) {
        	newLinkBoard.setMasterBoard(newLinkBoard);
        }
        
        newLinkBoard.setCrate(linkBox);        
        if(saveToDB) {
			equipmentDAO.saveObject(newLinkBoard);
		}
        System.out.println("newLinkBoard " + newLinkBoard);
        //-----------------------
        Chip newSynCoderChip = new Chip();
        newSynCoderChip.setType(ChipType.SYNCODER);
        newSynCoderChip.setPosition(0);
        newSynCoderChip.setBoard(newLinkBoard);
        
        if(saveToDB) {
			equipmentDAO.saveObject(newSynCoderChip);			
			SynCoderConf newCoderConf = new SynCoderConf();	
			if(newLinkBoard.isMaster()) {	
				newCoderConf.setLmuxInDelay(2);
			}
			newCoderConf.setInChannelsEna(HexBinary.decode("ffffffffffffffffffffffff"));
			if(saveToDB) {
				configurationDAO.saveObject(newCoderConf);
				configurationManager.assignConfiguration(newSynCoderChip, newCoderConf, ConfigurationManager.CURRENT_LBS_CONF_KEY);
				System.out.println("new configuration for chip " + newLinkBoard.getName() + " was saved ");
			}
		}
        
        //------------------------
        
        if(newLinkBoard.isMaster()) {		                    	
        	LinkConn newLinkConn = new LinkConn();
        	newLinkConn.setBoard(newLinkBoard);
        	
        	newLinkConn.setCollectorBoard(equipmentDAO.getBoardByName("TBp3_1"));
        	if(position == 2)
        		newLinkConn.setCollectorBoardInputNum(0);
        	else if(position == 12)
        		newLinkConn.setCollectorBoardInputNum(9);
        	
        	newLinkConn.setType(ConnType.LINKCONN);
        	System.out.println("new LinkConn " + newLinkConn);
        	if(saveToDB) {
        		equipmentDAO.saveObject(newLinkConn);
        		
        		//configurationManager.enableLinkConn(newLinkConn, true);
        	}
        }
        	//-------------------------------------
        	System.out.println();
        	return newLinkBoard;
	}
	
	static ChamberLocation makeChamberLocation(boolean saveToDB, 
			String chamberLocationName, 
			int sector,
			ControlBoard cb,
			int cbChannel,
			LinkBox linkBox,
			LinkBoard lb,
			EquipmentDAO equipmentDAO, 
			ChamberLocationDAO chamberLocationDAO, ConfigurationDAO configurationDAO, ConfigurationManager configurationManager) throws DataAccessException {
		
/*		ChamberLocation chamberLocation =  new ChamberLocation();
		chamberLocation.setBarrelOrEndcap(BarrelOrEndcap.Endcap);
		chamberLocation.setChamberLocationName(chamberLocationName);
		ChamberType chamberType = (ChamberType) chamberLocationDAO.getObject(ChamberType.class, 1); //id=1 is RRE1/1		
		chamberLocation.setChamberType(chamberType);
		chamberLocation.setDiskOrWheel(1);
		chamberLocation.setLayer(1);		
		chamberLocation.setSector(sector);
					
		if(saveToDB) {
			chamberLocationDAO.saveObject(chamberLocation);
		}*/
		
		ChamberLocation chamberLocation = chamberLocationDAO.getByName(chamberLocationName);
		
		String[] etaPart = {
				"D", "C", "B", "A"
		};
		
		for(int i = 0; i < 4; i++) {
			FebLocation febLocation = new FebLocation();
			febLocation.setChamberLocation(chamberLocation);			
			febLocation.setFebCmsEtaPartition(etaPart[i]);
			febLocation.setFebLocalEtaPartition(etaPart[i]);
			febLocation.setPosInLocalEtaPartition(1);
			febLocation.setPosInCmsEtaPartition(1);
			
			febLocation.setI2cLocalNumber(3-i);
								
			I2cCbChannel i2cCbChannel = new I2cCbChannel();
	    	i2cCbChannel.setControlBoard(cb);
	    	i2cCbChannel.setCbChannel(cbChannel);
	    	if(saveToDB)
	    		equipmentDAO.saveObject(i2cCbChannel);
	    	
	    	System.out.println("added I2cCbChannel " + i2cCbChannel.toString());
	    	
	    	febLocation.setI2cCbChannel(i2cCbChannel);
	    	
	    	String name = "FEB_" + febLocation.getChamberLocation().getChamberLocationName() + "_" + 
					i2cCbChannel.getCbChannel() + "_" + febLocation.getI2cLocalNumber();	    	
	    	int position = cb.getPosition() * 100 + i2cCbChannel.getCbChannel() * 10 + febLocation.getI2cLocalNumber();					    		
			int chipCnt = 4;					    		
			FEBsGenerator.createFebBoard(saveToDB, equipmentDAO, febLocation, linkBox, name, position, chipCnt);	
			
			if(saveToDB) {
				febLocation.setLinkBoard(lb);
				equipmentDAO.saveObject(febLocation);
			}
			System.out.println(febLocation);
		}
		
		
		return chamberLocation;
	}
	
	static void makeRE11(boolean saveToDB, EquipmentDAO equipmentDAO, 
			ChamberLocationDAO chamberLocationDAO, ConfigurationDAO configurationDAO, ConfigurationManager configurationManager) throws DataAccessException {
		LinkBox linkBox = (LinkBox) equipmentDAO.getCrateByName("LBB_YEP1_S3");
		LinkBoard mlb2 = makeLB(saveToDB, equipmentDAO, linkBox, "LB_RE+1_S2_EP11_CH0", 2, chamberLocationDAO, configurationDAO, configurationManager);
		LinkBoard slb3 = makeLB(saveToDB, equipmentDAO, linkBox, "LB_RE+1_S2_EP11_CH2", 3, chamberLocationDAO, configurationDAO, configurationManager);		
		slb3.setMasterBoard(mlb2);
		
		LinkBoard mlb12 = makeLB(saveToDB, equipmentDAO, linkBox, "LB_RE+1_S1_EP11_CH0", 12, chamberLocationDAO, configurationDAO, configurationManager);
		LinkBoard slb13 = makeLB(saveToDB, equipmentDAO, linkBox, "LB_RE+1_S1_EP11_CH2", 13, chamberLocationDAO, configurationDAO, configurationManager);
		slb13.setMasterBoard(mlb12);
		
		ControlBoard cb10 = linkBox.getControlBoard10();
		ControlBoard cb20 = linkBox.getControlBoard20();
		makeChamberLocation(saveToDB, "RE+1/1/1", 1, cb20, 7, linkBox, mlb12, equipmentDAO, chamberLocationDAO, configurationDAO, configurationManager);
		makeChamberLocation(saveToDB, "RE+1/1/2", 1, cb20, 8, linkBox, slb13, equipmentDAO, chamberLocationDAO, configurationDAO, configurationManager);
		makeChamberLocation(saveToDB, "RE+1/1/3", 1, cb10, 7, linkBox, mlb2, equipmentDAO, chamberLocationDAO, configurationDAO, configurationManager);
		makeChamberLocation(saveToDB, "RE+1/1/4", 1, cb10, 8, linkBox, slb3, equipmentDAO, chamberLocationDAO, configurationDAO, configurationManager);
	}

	static void makeStrips(boolean saveToDB, 
			String chamberLocationName, 		
			EquipmentDAO equipmentDAO, 
			ChamberLocationDAO chamberLocationDAO, ConfigurationDAO configurationDAO, ConfigurationManager configurationManager) throws DataAccessException 
	{

		Map<String, Integer> linkBoardInputNumFromEtaPart = new TreeMap<String, Integer>();
		linkBoardInputNumFromEtaPart.put("A", 3);
		linkBoardInputNumFromEtaPart.put("B", 4);
		linkBoardInputNumFromEtaPart.put("C", 5);
		linkBoardInputNumFromEtaPart.put("D", 6);
		
		ChamberLocation chamberLocation = chamberLocationDAO.getByName(chamberLocationName);
		System.out.println("chamberLocation " + chamberLocation);
		for(FebLocation febLocation : chamberLocation.getFEBLocations()) {
			System.out.println("febLocation " + febLocation);
			for(int iFebConnector = 0; iFebConnector < 1; iFebConnector++) {//only half of the chamber is connected
				int febConnectorNum = 1;
				if(chamberLocation.getSector()%2 == 1)
					febConnectorNum = 1;
				else
					febConnectorNum = 2; //only half of the chamber is connected
					
				FebConnector febConnector = new FebConnector();
				febConnector.setFebLocation(febLocation);
				febConnector.setFebConnectorNum(febConnectorNum);
				int linkBoardInputNum = linkBoardInputNumFromEtaPart.get(febLocation.getFebLocalEtaPartition());
				febConnector.setLinkBoardInputNum(linkBoardInputNum);
				
				if(saveToDB) {
					equipmentDAO.saveObject(febConnector);
				}
				
				System.out.println("febConnector: febConnectorNum " + febConnectorNum + " LinkBoardInputNum " + febConnector.getLinkBoardInputNum());
				
					
				for(int iStrip = 0; iStrip < 16; iStrip++) {
					ChamberStrip chamberStrip = new ChamberStrip();
					
					int cableChannelNum = 0;				
					int chamberStripNumber = 0;
					int cmsStripNumber = 0;
					
					chamberStripNumber = (febConnectorNum - 1) * 16 + iStrip + 1;
					if(	(febLocation.getFebLocalEtaPartition().equals("A") )  ||
							(febLocation.getFebLocalEtaPartition().equals("C") ) ) {
						cableChannelNum = 16 - iStrip;								 
					}
					else {
						cableChannelNum = iStrip + 1;
					}

					if(chamberLocation.getSector()%2 == 1) {
						cmsStripNumber = 32 - chamberStripNumber ;						
					}
					else {
						cmsStripNumber = chamberStripNumber ;
					}
					
					
					int cmsGlobalStripNumber = cmsStripNumber + (chamberLocation.getSector() -1) * 32;
					
					
					chamberStrip.setCableChannelNum(cableChannelNum);
					chamberStrip.setCablePinNumber(cableChannelNum*2 -1);
					chamberStrip.setChamberStripNumber(chamberStripNumber);
					
					chamberStrip.setCmsStripNumber(cmsStripNumber);
					chamberStrip.setCmsGlobalStripNumber(cmsGlobalStripNumber);
					
					chamberStrip.setFebConnector(febConnector);
					
					System.out.println("chamberStrip: cableChannelNum " + cableChannelNum + " chamberStripNumber " + chamberStripNumber 
							+ " cmsStripNumber " + cmsStripNumber + " cmsGlobalStripNumber " + cmsGlobalStripNumber);
					if(saveToDB) {
						equipmentDAO.saveObject(chamberStrip);
					}
				}
			}
		}
		System.out.println("\n");
	}
	/**
	 * @param args
	 * @throws DataAccessException 
	 */
	public static void main(String[] args) throws DataAccessException {

		HibernateContext context = new SimpleHibernateContextImpl();
		EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
		ChamberLocationDAO chamberLocationDAO = new ChamberLocationDAOHibernate(context);
		ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
		ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);
		
		//List<Board> lbs;

		try {
			//makeRE11(true, equipmentDAO, chamberLocationDAO, configurationDAO, configurationManager);
			makeStrips(true, "RE+1/1/1", equipmentDAO, chamberLocationDAO, configurationDAO, configurationManager);
			makeStrips(true, "RE+1/1/2", equipmentDAO, chamberLocationDAO, configurationDAO, configurationManager);
			makeStrips(true, "RE+1/1/3", equipmentDAO, chamberLocationDAO, configurationDAO, configurationManager);
			makeStrips(true, "RE+1/1/4", equipmentDAO, chamberLocationDAO, configurationDAO, configurationManager);
			
		}        
		catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			context.rollback();
		} 
		catch (Error e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			context.rollback();
		}
		finally {
			context.closeSession();
		}
	}

}
