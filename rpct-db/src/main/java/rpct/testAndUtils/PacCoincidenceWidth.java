package rpct.testAndUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;

import org.apache.axis.types.UnsignedByte;

import rpct.datastream.MonitorManager;
import rpct.datastream.RpctDelays;
import rpct.datastream.TestOptionsXml.ChipSource;
import rpct.datastream.TestOptionsXml.HardwareType;
import rpct.datastream.TestOptionsXml.SourcesList;
import rpct.datastream.TestsManager.PulserTarget;
import rpct.db.DataAccessException;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.SorterCrate;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.diag.TriggerDataSel;
import rpct.xdaqaccess.diag.DiagCtrl.TriggerType;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;
import rpct.xdaqaccess.synchro.DefOut;

public class PacCoincidenceWidth {

    public static void main(String[] args) throws ServiceException,
    DataAccessException, IOException, InterruptedException,
    XdaqException, HardwareDbException {
        String xmlFileName =  System.getenv("HOME") + "/bin/data/gentestpulses.xml";

        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(
                new HardwareRegistry(), context);

        try {
            
            if(args.length == 0) {            
                String[] argsL = {
/*                        "TC_0",                    
                        "TC_1",
                        "TC_2",                        
                        "TC_3",                    
                        "TC_4",
                        "TC_5",                        
                        "TC_6",                    
                        "TC_7",
                        "TC_8",                        
                        "TC_9",                    
                        "TC_10",
                        "TC_11",*/
                		"TC_904"
                        };
                
                args = argsL;
            }
            
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("pacCoincidenceWidth (number of additional BXs in the coincidence)?");            
            String line = br.readLine();
            
            long pacCoincidenceWidth = new Integer(line); //additional BXs, i.e. 0 = 1 BX, 1 = 2 BX, etc //TODO
            
            System.out.println("range of the towers to aplly the pacCoincidenceWidth? towerNum => ");            
            line = br.readLine();
            int towersRange = new Integer(line);           
            
            System.out.println("Selected crates");
            for(int i = 0; i < args.length; i++) {
                System.out.print(args[i] + " ");
            }
            
            for(int i = 0; i < args.length; i++) {                
                HardwareDbTriggerCrate tcHdb = (HardwareDbTriggerCrate)dbMapper.getCrate(equipmentDAO.getCrateByName(args[i]));
                for(HardwareDBTriggerBoard tb : tcHdb.getHardwareDBTriggerBoards()) {
                    /*tb.getRmb().getHardwareChip().getItem("STATUS.RMB_RESET").write(1);
                    Thread.sleep(10);
                    tb.getRmb().getHardwareChip().getItem("STATUS.RMB_RESET").write(0);
                    System.out.println(tb.getRmb().getFullName() + " reseted");*/
                	
/*                	tb.getRmb().getHardwareChip().getItem("RMB_PRETRG_VAL").write(RpctDelays.PRE_TRIGGER_VAL + 1);
                	tb.getRmb().getHardwareChip().getItem("RMB_POSTTRG_VAL").write(RpctDelays.POST_TRIGGER_VAL - 1);
                	
                    System.out.println(tb.getRmb().getFullName() + " PRETRG_VAL and POSTTRG_VAL written");*/
                                       
                    for(int iT = 0; iT < tb.getDBTriggerBoard().getTowersNums().length; iT++) {
                        if(Math.abs(tb.getDBTriggerBoard().getTowersNums()[iT] ) >= towersRange) {
                            tb.getPacs().get(iT).getHardwareChip().getItem("STATUS.PAC_DATA_OR").write(pacCoincidenceWidth);
                        }
                    }
                }
                
            }           
           
        } finally {
            context.closeSession();
        }
    }

}
