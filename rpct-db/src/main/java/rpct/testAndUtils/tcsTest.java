package rpct.testAndUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.DeviceItemWord;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDBTTUBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbCrate;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbSorterCrate;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;

public class tcsTest {
    public static void main(String[] args) throws DataAccessException {                  
        
        HibernateContext context = new SimpleHibernateContextImpl();
        try {           
            String[] tcs = {
                    "TC_0",
                    "TC_1",
                    "TC_2",
                    "TC_3",
                    "TC_4",
                    "TC_5",
                    "TC_6",
                    "TC_7",
                    "TC_8",                        
                    "TC_9",
                    "TC_10",
                    "TC_11",
                    //"PASTEURA_TC"
                    //"SC"
            };
            
            //
            //1ff0800000ff0400001ff
            EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
            HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
            List<HardwareDbTriggerCrate> crates = new ArrayList<HardwareDbTriggerCrate>();
            
            for(int i = 0; i < tcs.length; i++) {
                crates.add((HardwareDbTriggerCrate)(dbMapper.getCrate(equipmentDAO.getCrateByName(tcs[i]))));
            }
                                       
            for (HardwareDbTriggerCrate hdbTc : crates) {
                hdbTc.getTcSort().getHardwareChip().getItem("REC_MUX_CLK90").writeBinary(new Binary("1ffffffffffffffffffff"));
                hdbTc.getTcSort().getHardwareChip().getItem("REC_MUX_CLK_INV").writeBinary(new Binary("1ffffffffffffffffffff"));
                hdbTc.getTcSort().getHardwareChip().getItem("REC_MUX_REG_ADD").writeBinary(new Binary("1ffffffffffffffffffff"));
                DeviceItemWord recMuxDealy = (DeviceItemWord) hdbTc.getTcSort().getHardwareChip().getItem("REC_MUX_DELAY");
                for(int i = 0; i < recMuxDealy.getNumber(); i++) {
                    recMuxDealy.write(i, 4);
                }
                 
                DeviceItemWord recDataDealy = (DeviceItemWord) hdbTc.getTcSort().getHardwareChip().getItem("REC_DATA_DELAY");
                for(int i = 0; i < recDataDealy.getNumber(); i++) {
                    recDataDealy.write(i, 4);
                }
                
                hdbTc.getTcSort().getHardwareChip().getItem("REC_ERROR_COUNT").write(0);
                Thread.sleep(100);
                System.out.println(hdbTc.getName() + " REC_ERROR_COUNT " + hdbTc.getTcSort().getHardwareChip().getItem("REC_ERROR_COUNT").read());
            }
            
            System.out.println("End");
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (HardwareDbException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } 
        finally {
            context.closeSession();
        }
    }
}
