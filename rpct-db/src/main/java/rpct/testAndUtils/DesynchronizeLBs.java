package rpct.testAndUtils;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;

public class DesynchronizeLBs {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HibernateContext context = new SimpleHibernateContextImpl();
		EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
		HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
		ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
		ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO); 

		if(args.length == 0) {            
			String[] argsL =  {                
					//"PASTEURA_LBB"
					"YEP1_near",
					"YEP1_far",
					"YEP3_near",
					"YEP3_far",
					"YEN1_near",
					"YEN1_far",
					"YEN3_near",
					"YEN3_far",

					"RB-2_near",
					"RB-2_far",
					"RB-1_near",
					"RB-1_far",
					"RB0_near",
					"RB0_far",
					"RB+1_near",
					"RB+1_far",
					"RB+2_near",
					"RB+2_far",
			};

			args = argsL;
		}

		HashSet<String> selTowers = new HashSet<String>();
        System.out.println("Selected XDAQs");
        for(int i = 0; i < args.length; i++) {
            System.out.println(args[i] + " ");
            selTowers.add(args[i]);
        } 
        
		try {
			List<Board> lbs;

			lbs = equipmentDAO.getBoardsByType(BoardType.LINKBOARD, true);

			Random rndGen = new Random();
			for(Board board : lbs) {
				LinkBoard lb = (LinkBoard)board;
				if(selTowers.contains(lb.getLinkBox().getTowerName()) ) {
					HardwareDbChip syncoder = dbMapper.getChip(lb.getSynCoder());
					int delay = 0;
					String delays = "";
					if(lb.isMaster()) {
						delay = rndGen.nextInt(8);
						delays = delays + " " + delay; 
						syncoder.getHardwareChip().getItem("LMUX.MASTER_DELAY").write(delay);
						delay = rndGen.nextInt(8);
						delays = delays + " " + delay;
						syncoder.getHardwareChip().getItem("LMUX.SLAVE0_DELAY").write(delay);
						delay = rndGen.nextInt(8);
						delays = delays + " " + delay;
						syncoder.getHardwareChip().getItem("LMUX.SLAVE1_DELAY").write(delay);						
					}
					delay = rndGen.nextInt(8);
					delays = delays + " " + delay;
					syncoder.getHardwareChip().getItem("STATUS2.CSC_RBC_DELAY").write(delay);
					System.out.println("delays changed for " + lb.getName() + " delays " + delays);
				}
			}       
		}
		catch (DataAccessException e1) {
			//TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

