package rpct.testAndUtils;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.PropertyConfigurator;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.CrateDAO;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.FebBoard;
import rpct.db.domain.equipment.I2cCbChannel;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkBoardDAO;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.equipment.chamberloaction.BarrelOrEndcap;
import rpct.db.domain.equipment.chamberloaction.ChamberLocation;
import rpct.db.domain.equipment.chamberloaction.ChamberLocationDAO;
import rpct.db.domain.equipment.chamberloaction.ChamberLocationDAOHibernate;
import rpct.db.domain.equipment.chambertype.ChamberType;
import rpct.db.domain.equipment.controlboard.ControlBoard;
import rpct.db.domain.equipment.distributionboard.DistributionBoardDAO;
import rpct.db.domain.equipment.feblocation.FebLocation;
import rpct.db.domain.equipment.feblocation.FebLocationDAO;
import rpct.db.domain.equipment.feblocation.FebLocationDAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import sun.security.action.GetBooleanAction;

/**
 * Created on 2010-10-03
 * 
 * @author Karol Bunkowski
 * @version $Id$
 */
public class FEBsGenerator {
    private FebLocationDAO daoFebLocation;
    private ChamberLocationDAO chamberLocationDAO;
    
    
    private HibernateContext hibernateContext;
    private EquipmentDAO equipmentDAO;
    
    public FEBsGenerator(HibernateContext hibernateContext) {
        this.hibernateContext = hibernateContext;
        this.equipmentDAO = new EquipmentDAOHibernate(this.hibernateContext);
        this.daoFebLocation =  new FebLocationDAOHibernate(hibernateContext);
        this.chamberLocationDAO = new ChamberLocationDAOHibernate(hibernateContext);
    }
    
    ChamberType cretateChamberType(boolean saveToDB, String typeName) throws DataAccessException {
    	ChamberType chamberType = new ChamberType();
    	
    	chamberType.setTypeName(typeName);
    	if(saveToDB)
    		equipmentDAO.saveObject(chamberType);
    	
    	System.out.println("added chamberType " + chamberType.toString());
    	return chamberType;
    }

    
    //TODO add other parameters of ChamberLocation
    ChamberLocation createChamberLocation(boolean saveToDB, String chamberLocationName, ChamberType chamberType) throws DataAccessException {
    	ChamberLocation chamberLocation = new ChamberLocation();
    	chamberLocation.setChamberLocationName(chamberLocationName);
    	chamberLocation.setChamberType(chamberType);
    	
    	if(saveToDB)
    		equipmentDAO.saveObject(chamberLocation);
    	
    	System.out.println("added chamberLocation " + chamberLocation.toString());
    	return chamberLocation;
    }
    
    I2cCbChannel createI2cCbChannel(boolean saveToDB, ControlBoard cb, Integer cbChannel) throws DataAccessException {
    	I2cCbChannel i2cCbChannel = new I2cCbChannel();
    	i2cCbChannel.setControlBoard(cb);
    	i2cCbChannel.setCbChannel(cbChannel);
    	
    	if(saveToDB)
    		equipmentDAO.saveObject(i2cCbChannel);
    	
    	System.out.println("added I2cCbChannel " + i2cCbChannel.toString());
    	return i2cCbChannel;
    }
    
    I2cCbChannel getOrCreateI2cCbChannel(boolean saveToDB, ControlBoard cb, Integer cbChannel) throws DataAccessException {
    	I2cCbChannel i2cCbChannel = equipmentDAO.getI2cCbChannelByControlBoardAndCbChannel(cb, cbChannel);
    	if(i2cCbChannel == null) {
    		System.out.println("adding I2cCbChannel... ");
    		i2cCbChannel = createI2cCbChannel(saveToDB, cb, cbChannel);
    	}
    	return i2cCbChannel;
    }
    
    //TODO add remaining parameters
    FebLocation createFebLocation(boolean saveToDB, LinkBoard linkBoard, I2cCbChannel i2cCbChannel, Integer i2cLocalNumber, String febLocalEtaPartition, ChamberLocation chamberLocation, int posInLocalEtaPartition, int posInCmsEtaPartition, String etaPartition)  throws DataAccessException {
    	FebLocation febLocation = new FebLocation();
    	febLocation.setLinkBoard(linkBoard);
    	febLocation.setChamberLocation(chamberLocation);    	
    	febLocation.setI2cCbChannel(i2cCbChannel);
    	febLocation.setI2cLocalNumber(i2cLocalNumber);
    	
    	febLocation.setFebLocalEtaPartition(febLocalEtaPartition);
    	febLocation.setFebCmsEtaPartition(etaPartition);
    	
    	febLocation.setPosInLocalEtaPartition(posInLocalEtaPartition);
    	febLocation.setPosInCmsEtaPartition(posInCmsEtaPartition);
    	
    	if(saveToDB)
    		equipmentDAO.saveObject(febLocation);

    	System.out.println("added FebLocation " + febLocation.toString());
    	return febLocation;
    }
       
    
    public static FebBoard createFebBoard(boolean saveToDB, EquipmentDAO equipmentDAO, FebLocation febLocation, LinkBox linkBox, String name, int position, int chipCnt) throws DataAccessException {
    	FebBoard febBoard = new FebBoard();
    	
    	febBoard.setCrate(linkBox);
    	febBoard.setFebLocation(febLocation);
    	febBoard.setLabel(name);
    	febBoard.setName(name);
    	febBoard.setPosition(position);
    	
     	if(saveToDB)
    		equipmentDAO.saveObject(febBoard);   
     	
    	System.out.println("added FebBoard " + febBoard.toString());
    	
    	for(int i = 0; i < chipCnt; i++) {
    		Chip febChip = new Chip();
    		febChip.setType(ChipType.FEBCHIP);
    		febChip.setBoard(febBoard);
    		febChip.setPosition(i);
    		equipmentDAO.saveObject(febChip);
    	}
    	return febBoard;
    }
    
    void test(boolean saveToDB) throws DataAccessException {
    	//ControlBoard cb = (ControlBoard)(equipmentDAO.getBoardByName("CB_LBB_904_L10"));
    	ControlBoard cb = (ControlBoard)(equipmentDAO.getBoardById(3462));
    	System.out.println("FEBs will be added to the: " + cb.toString() + " in "+ cb.getCrate());
    	
    	int cbChannel = 1;
/*    	ChamberType chamberType = cretateChamberType(saveToDB, "dummy904"); 
    	ChamberLocation chamberLocation = createChamberLocation(saveToDB, "test_904_0", chamberType);
    	I2cCbChannel i2cCbChannel = createI2cCbChannel(saveToDB, cb, 0);
    	FebLocation febLocation = createFebLocation(saveToDB, i2cCbChannel, 0, "test_904_0", chamberLocation);
    	
    	FebBoard febBoard = createFebBoard(saveToDB, febLocation, (LinkBox)cb.getCrate(), "test_904_0");*/
    }
        
    void generateFebs904(boolean saveToDB) throws DataAccessException {
    	LinkBox linkBox = (LinkBox) equipmentDAO.getCrateByName("LBB_904_2");
    	//ControlBoard cb = (ControlBoard)(equipmentDAO.getBoardByName("CB_LBB_904_L10"));
    	ControlBoard cb = linkBox.getControlBoard20();
    	System.out.println("FEBs will be added to the: " + cb.toString() + " in "+ cb.getCrate());
    	        	
    	String chamberLocations [] = {
    			"W0/RB1in/1",
    			"W0/RB1out/1",
    			"W0/RB2in/1",
    			"W+1/RB1in/1",
    			"W+1/RB1out/1",
    			"W+1/RB2in/1"
    	};

    	for(int cbChannel = 4; cbChannel <= 6; cbChannel++) {
    		I2cCbChannel i2cCbChannel = getOrCreateI2cCbChannel(saveToDB, cb, cbChannel);    
    		ChamberLocation chamberLocation = chamberLocationDAO.getByName(chamberLocations[cbChannel -3]);
    		LinkBoard linkBoard = (LinkBoard) linkBox.getBoardArray()[cbChannel -3];
    		for(int i2cAddress = 0; i2cAddress < 6; i2cAddress++) {
    			String name = "FEB_" + cb.getCrate().getName() + "_L" + cb.getPosition() + "_C" + cbChannel + "_A" + i2cAddress;   			
    			
    			FebLocation febLocation = new FebLocation();
    	    	febLocation.setLinkBoard(linkBoard);
    	    	febLocation.setChamberLocation(chamberLocation);    	
    	    	febLocation.setI2cCbChannel(i2cCbChannel);
    	    	febLocation.setI2cLocalNumber(i2cAddress);
    	    	
    	    	febLocation.setFebLocalEtaPartition("Backward");
    	    	febLocation.setFebCmsEtaPartition("3");
    	    	
    	    	febLocation.setPosInLocalEtaPartition(i2cAddress +1);
    	    	febLocation.setPosInCmsEtaPartition(i2cAddress);
    	    	
    	    	if(saveToDB)
    	    		equipmentDAO.saveObject(febLocation);

    	    	System.out.println("added FebLocation " + febLocation.toString());
    			
    			int position = cb.getPosition() * 100 + i2cCbChannel.getCbChannel() * 10 + i2cAddress;
    			FebBoard febBoard = createFebBoard(saveToDB, equipmentDAO, febLocation, linkBox, name, position, 2);    			
    		}
    	}
    }
    
    void generateI2cCbChannels(boolean saveToDB) throws DataAccessException {
    	ControlBoard cb = (ControlBoard)(equipmentDAO.getBoardByName("CB_LBB_904_2_L10"));
    	//ControlBoard cb = (ControlBoard)(equipmentDAO.getBoardById(3462));
    	System.out.println("I2cCbChannel will be added to the: " + cb.toString() + " in "+ cb.getCrate());
    	        	
    	for(int cbChannel = 4; cbChannel <= 6; cbChannel++) {
    		I2cCbChannel i2cCbChannel = getOrCreateI2cCbChannel(saveToDB, cb, cbChannel);    	    		
    	}
    }
    
    
    void correct() throws DataAccessException {
    	List<Board> febBoards =new ArrayList<Board>(); 
    	febBoards.add(equipmentDAO.getBoardByName("FEB_LBB_904_L10_C4_A0"));
    	febBoards.add(equipmentDAO.getBoardByName("FEB_LBB_904_L10_C4_A1"));
    	febBoards.add(equipmentDAO.getBoardByName("FEB_LBB_904_L10_C4_A2"));
    	for(Board board : febBoards) {
    		System.out.println(board);
    		for(int i = 2; i < 4; i++) {
    			Chip febChip = new Chip();
    			febChip.setType(ChipType.FEBCHIP);
    			febChip.setBoard(board);
    			febChip.setPosition(i);
    			equipmentDAO.saveObject(febChip);
    		}
    	}
    }
    
    
    void generateFEBBoardsP5(boolean saveToDB) throws DataAccessException {
    	List<FebLocation> febLocations = daoFebLocation.getFebs();
    	System.out.println("found " + febLocations.size() + " FebLocations");
    	for(FebLocation febLocation : febLocations) {
    		ControlBoard cb = febLocation.getI2cCbChannel().getControlBoard();
    		if(cb == null)
    			continue;
    			
    		String name = "FEB_" + febLocation.getChamberLocation().getChamberLocationName() + "_" + 
    						febLocation.getI2cCbChannel().getCbChannel() + "_" + febLocation.getI2cLocalNumber();
    		
    		int position = cb.getPosition() * 100 + febLocation.getI2cCbChannel().getCbChannel() * 10 + febLocation.getI2cLocalNumber();
    		
    		int chipCnt = 0;
    		if(febLocation.getChamberLocation().getBarrelOrEndcap() == BarrelOrEndcap.Barrel)
    			chipCnt = 2;
    		else if(febLocation.getChamberLocation().getBarrelOrEndcap() == BarrelOrEndcap.Endcap)
    			chipCnt = 4;
    		
    		createFebBoard(saveToDB, equipmentDAO, febLocation, (LinkBox)cb.getCrate(), name, position, chipCnt);
    	}
    }
    
    void deleteFEBBoardsAndChips(boolean saveToDB) throws DataAccessException {
    	List<Board> febBoards = equipmentDAO.getBoardsByType(BoardType.FEBBOARD, false);
    	System.out.println("found " + febBoards.size() + " FebBoards");
    	for(Board board : febBoards) {
    		System.out.println("board " + board.getName() + " contains " + board.getChips().size() + " chips");
    		for(Chip chip : board.getChips()) {    			
    			if(saveToDB)
    				equipmentDAO.deleteObject(chip);
    		}
    		if(saveToDB)
    			equipmentDAO.deleteObject((FebBoard)board); //lokks that it trays to delete alse the FebLocation!!!!????
    	}
    }
	/**
	 * @param args
	 * @throws DataAccessException 
	 */       
	public static void main(String[] args) throws DataAccessException {
		PropertyConfigurator.configure("log4j.properties");
        HibernateContext hibernateContext = new SimpleHibernateContextImpl();
        
		try {
			FEBsGenerator generator = new FEBsGenerator(hibernateContext);
			//generator.test(false);
			generator.generateFebs904(true);
			//generator.correct();
			//generator.generateFEBBoardsP5(true);
			//generator.deleteFEBBoardsAndChips(false);
			//generator.generateI2cCbChannels(true);
			
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
			hibernateContext.rollback();
		} finally {
			hibernateContext.closeSession();
		}
	}

}
