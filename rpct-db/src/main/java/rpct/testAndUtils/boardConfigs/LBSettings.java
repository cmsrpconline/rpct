package rpct.testAndUtils.boardConfigs;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ChipConfAssignment;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.LinkDisabled;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.configuration.SynCoderConf;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.FebConnector;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.equipment.chamberloaction.BarrelOrEndcap;
import rpct.db.domain.equipment.chamberloaction.ChamberLocation;
import rpct.db.domain.equipment.chamberstrip.ChamberStrip;
import rpct.db.domain.equipment.feblocation.FebLocation;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.axis.Binary;

public class LBSettings {
    static void printLBChamberAssigment(EquipmentDAO equipmentDAO) throws DataAccessException, FileNotFoundException {       
        Map<String, PrintStream> towersMap = new TreeMap<String, PrintStream>();      
        for(Crate crate : equipmentDAO.getCratesByType(CrateType.LINKBOX, false) ) {
            LinkBox linkBox = (LinkBox)crate;
            
            PrintStream prntSrm = towersMap.get(linkBox.getTowerName());
            if(prntSrm == null) {                
                FileOutputStream file = new FileOutputStream(System.getenv("HOME") + "/bin/out/lbChamberMaps/lbChamberMap_" + linkBox.getTowerName().replace('-', 'n').replace('+', 'p')  + ".txt", false);
                prntSrm = new PrintStream(file);
                towersMap.put(linkBox.getTowerName(), prntSrm);
            }
            
            //if(crate.getName().contains("LBB_YEP1")) 
            {
                //prntSrm.println(crate.getName());
                Map<Integer, Board> boardsMap = new TreeMap<Integer, Board>();
                for(Board board : crate.getBoards()) {
                    if(board.getType() == BoardType.LINKBOARD)
                        boardsMap.put(board.getPosition(), board);
                }
                
                for(Map.Entry<Integer, Board> e : boardsMap.entrySet()) {
                    LinkBoard lb = (LinkBoard)e.getValue();
                    prntSrm.println(lb.getName() + " " + lb.getPosition() + " " + lb.getChamberName() + " 1");
                }
                //prntSrm.println("\n\n");
            }
        }
    }
    
    static void printLBoxes(EquipmentDAO equipmentDAO) throws DataAccessException, FileNotFoundException {       
    	//FileOutputStream file = new FileOutputStream(System.getenv("HOME") + "/bin/out/lboxes.txt", false);
    	//PrintStream prntSrm = new PrintStream(file);
    	PrintStream prntSrm = System.out;
    	for(Crate crate : equipmentDAO.getCratesByType(CrateType.LINKBOX, false) ) {
    		LinkBox linkBox = (LinkBox)crate;
    		linkBox.getTowerName();
    		prntSrm.println(linkBox.getName());
    		
    		for(int pos = 0 ;pos <  linkBox.getBoardArray().length;pos++) {
    			prntSrm.print(pos + " ");
    			if(linkBox.getBoardArray()[pos] != null) {
    				prntSrm.print(linkBox.getBoardArray()[pos]);
    				if(linkBox.getBoardArray()[pos].getType() == BoardType.LINKBOARD) {
    					prntSrm.print(" master: " +((LinkBoard)linkBox.getBoardArray()[pos]).getMaster());
    				}
    				prntSrm.println();
    			}
    			else 
    				prntSrm.println();
    		}
    	}
    }

    //for twinMux connections
    static void printOptLinkChamberAssigment(EquipmentDAO equipmentDAO) throws DataAccessException, FileNotFoundException {                                 
        FileOutputStream file = new FileOutputStream(System.getenv("HOME") + "/bin/optLink-chamber.txt", false);
        PrintStream prntSrm = new PrintStream(file);
        Map<String, LinkBox> lBoxMap = new TreeMap<String, LinkBox>(); 
        for(Crate crate : equipmentDAO.getCratesByType(CrateType.LINKBOX, false) ) {
            LinkBox linkBox = (LinkBox)crate;
            lBoxMap.put(linkBox.getName(), linkBox);
        }
        
        for(Map.Entry<String, LinkBox> eLinBox : lBoxMap.entrySet()) {
            LinkBox linkBox = (LinkBox)eLinBox.getValue();
            if( (linkBox.getName().contains("RB-1") || linkBox.getName().contains("RB-2")) && 
              ( linkBox.getSector() == 9 || linkBox.getSector() == 10 || linkBox.getSector() == 11 ) 	) {      
            	
            }
            else {
        		continue;
        	}
            
            //prntSrm.println(linkBox.getName());
            Map<Integer, Board> boardsMap = new TreeMap<Integer, Board>();
            for(Board board : linkBox.getBoards()) {
                if(board.getType() == BoardType.LINKBOARD)
                    boardsMap.put(20 - board.getPosition(), board); //to reverse the order
            }

            for(Map.Entry<Integer, Board> e : boardsMap.entrySet()) {
                LinkBoard lb = (LinkBoard)e.getValue();
                if(lb.isMaster() == true) {
                	prntSrm.print(lb.getName());
                	int[] iLBs = {0, 1, 2};        			
        			for(int iLB : iLBs) {
        				LinkBoard linkBoard = lb.getSlave(iLB);
        				if(linkBoard != null) {									
        					prntSrm.print("\t" + lb.getChamberNames().first());
        				}
        				else 
        					prntSrm.print("\t");
        			}
        			prntSrm.print("\n");
                }
            }
            //prntSrm.println("\n");

        }
    }
    
    static void printLBChamberAssigment1(EquipmentDAO equipmentDAO) throws DataAccessException, FileNotFoundException {                                 
        FileOutputStream file = new FileOutputStream(System.getenv("HOME") + "/bin/lb-chamber3.txt", false);
        PrintStream prntSrm = new PrintStream(file);
        Map<String, LinkBox> lBoxMap = new TreeMap<String, LinkBox>(); 
        for(Crate crate : equipmentDAO.getCratesByType(CrateType.LINKBOX, false) ) {
            LinkBox linkBox = (LinkBox)crate;
            lBoxMap.put(linkBox.getName(), linkBox);
        }
        
        for(Map.Entry<String, LinkBox> eLinBox : lBoxMap.entrySet()) {
            LinkBox linkBox = (LinkBox)eLinBox.getValue();
            //prntSrm.println(linkBox.getName());
            Map<Integer, Board> boardsMap = new TreeMap<Integer, Board>();
            for(Board board : linkBox.getBoards()) {
                if(board.getType() == BoardType.LINKBOARD)
                    boardsMap.put(board.getPosition(), board);
            }

            for(Map.Entry<Integer, Board> e : boardsMap.entrySet()) {
                LinkBoard lb = (LinkBoard)e.getValue();
                //prntSrm.println(lb.getName() + " " + lb.getPosition() + " " + lb.getChamberNames().first().replaceFirst(" ", ""));
                //prntSrm.print(lb.getName() + " " + lb.getPosition());
                for(String chamberName : lb.getChamberNames()) {
                    prntSrm.print(" " + chamberName.replaceFirst(" ", " "));
                }
                prntSrm.print("\n");
            }
            prntSrm.println("\n");

        }
    }
    
    static void printOptoLBChamberAssigment(EquipmentDAO equipmentDAO) throws DataAccessException, FileNotFoundException {
        FileOutputStream file = new FileOutputStream(System.getenv("HOME") + "/bin/out/optLinksMap_Current" +  ".txt", false);
        PrintStream prntSrm = new PrintStream(file);

        for(Crate crate : equipmentDAO.getCratesByType(CrateType.TRIGGERCRATE, false) ) {            
            TriggerCrate triggerCrate = (TriggerCrate)crate;
            prntSrm.println(triggerCrate.getName());
            for(TriggerBoard triggerBoard : triggerCrate.getTriggerBoards()) {
                prntSrm.println(triggerBoard.getName());
                for(int tbInputNum = 0; tbInputNum < 18; tbInputNum++) {
                    LinkConn linkConn = triggerBoard.getLinkConn(tbInputNum);
                    if(linkConn != null) {
                    	if(linkConn.getDisabled() != null && linkConn.getDisabled().getDisconnected() == 1)
                    		continue;
                        LinkBoard mlb = (LinkBoard)linkConn.getBoard();
                        String otherLiks = ""; 
                        for(LinkConn linkConn2 : mlb.getLinkConns() ) {
                        	if(linkConn2.getDisabled() != null && linkConn2.getDisabled().getDisconnected() == 1)
                        		continue;
                        	
                        	otherLiks += linkConn2.getCollectorBoard().getName() + " " + linkConn2.getCollectorBoardInputNum() + ", ";
                        }
                        for(int iCh = 0; iCh < 3; iCh++) {
                            LinkBoard lb = mlb.getSlave(iCh);
                            prntSrm.print(tbInputNum + " " + iCh);
                            if(lb != null) {
                                prntSrm.print("   " + lb.getName() + "    ");
                                for(String chmName : lb.getChamberNames())
                                    prntSrm.print(chmName + ";  ");
                            }
                            if(iCh == 0) {
                            	prntSrm.print("  " + otherLiks);
                            }
                            prntSrm.print("\n");
                        }
                    }
                    else {
                        for(int iCh = 0; iCh < 3; iCh++) {
                            prntSrm.println(tbInputNum + " " + iCh);
                        }
                    }
                }
                prntSrm.println();
            }
            
        }
    }
    
    static void printSelectedOptLinks(EquipmentDAO equipmentDAO) throws DataAccessException, FileNotFoundException {                                 
        //FileOutputStream file = new FileOutputStream(System.getenv("HOME") + "/bin/selectedOptLinks.txt", false);
        //PrintStream prntSrm = new PrintStream(file);
        PrintStream prntSrm = System.out;
        Map<String, LinkBox> lBoxMap = new TreeMap<String, LinkBox>(); 
        for(Crate crate : equipmentDAO.getCratesByType(CrateType.LINKBOX, false) ) {
            LinkBox linkBox = (LinkBox)crate;
            lBoxMap.put(linkBox.getName(), linkBox);
        }
        
        for(Map.Entry<String, LinkBox> eLinBox : lBoxMap.entrySet()) {
            LinkBox linkBox = (LinkBox)eLinBox.getValue();
            //prntSrm.println(linkBox.getName());
            if(linkBox.getSector() == 10 ||  //linkBox.getSector() == 10 || linkBox.getSector() == 11 ||
            		linkBox.getName().contains("S8/9")) {
            	Map<Integer, Board> boardsMap = new TreeMap<Integer, Board>();
            	for(Board board : linkBox.getBoards()) {
            		if(board.getType() == BoardType.LINKBOARD && board.getName().contains("S10"))
            			boardsMap.put(board.getPosition(), board);
            	}

            	for(Map.Entry<Integer, Board> e : boardsMap.entrySet()) {
            		LinkBoard lb = (LinkBoard)e.getValue();
            		if(lb.isMaster() == false)
            			continue;
            	
            		ChamberLocation chamberLocation = lb.getChamberLocations().get(0);
    				if(chamberLocation != null) {
    					//if( chamberLocation.getDiskOrWheel()< 0) 
    					{
    						prntSrm.print(lb.getName() + "\t" +chamberLocation.getBarrelOrEndcap() + "\t" +chamberLocation.getDiskOrWheel() + "\t");
    					}
    				}
    				

    				for(int i = 0; i < 3; i++) {
    					LinkBoard slave = lb.getSlave(i);
    					if(slave != null) {
    						prntSrm.print(slave.getChamberNames().first().replaceFirst(" ", "") + "\t");
    					}  
    					else {
    						prntSrm.print("\t");
        				}
    				}      
            	            		
            		for(LinkConn linkConn : lb.getLinkConns()) {
    					prntSrm.print(linkConn.getCollectorBoard().getName() + " " + linkConn.getCollectorBoardInputNum() + "\t");
    				}
    				prntSrm.print("\n");
            	}
            }
        }
    }

    static void createDummyCongiguration(EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) throws DataAccessException {
        SynCoderConf masterSynCoderConf = new SynCoderConf();
        masterSynCoderConf.setLmuxInDelay(2);
        SynCoderConf slaveSynCoderConf = new SynCoderConf();
        
        configurationDAO.saveObject(masterSynCoderConf);
        configurationDAO.saveObject(slaveSynCoderConf);
        
        for(Chip c : equipmentDAO.getChipsByType(ChipType.SYNCODER, false) ) {            
            //StaticConfiguration configuration = configurationManger.getConfiguration(c, configurationManger.getDefaultLocalConfigKey());
            ChipConfAssignment assignment = configurationDAO.getChipConfAssignment(c, configurationManager.getDefaultLocalConfigKey());
            if(assignment == null 
               && c.getBoard().getName().contains("RE-")      
            ) {
                if(((LinkBoard)c.getBoard()).isMaster()) {
                    configurationManager.assignConfiguration(c, masterSynCoderConf, ConfigurationManager.CURRENT_LBS_CONF_KEY);
                }
                else {
                    configurationManager.assignConfiguration(c, slaveSynCoderConf, ConfigurationManager.CURRENT_LBS_CONF_KEY);
                }
                System.out.println("the dummy configuration was added for the " + c.getBoard().getName());
            }
        }
    }
    
    static void printConfiguration(EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) throws DataAccessException {
    	for(Chip c : equipmentDAO.getChipsByType(ChipType.SYNCODER, false) ) {   
    		//if(c.getBoard().getName().contains("RB0"))
    		{
    			try {
    				StaticConfiguration configuration = configurationManager.getConfiguration(c, configurationManager.getLocalConfigKey(ConfigurationManager.CURRENT_LBS_CONF_KEY));
    				//configurationManger.getDefaultLocalConfigKey(
    				//System.out.println(configuration + "\n");
    				//System.out.print("configuration for the " + c.getBoard().getName());
    				SynCoderConf coderConf = (SynCoderConf)configuration;
    				System.out.println(c.getBoard().getName() + " "+ coderConf.getWindowOpen());
    			}
    			catch(NullPointerException ex) {
    				System.out.println("no configuration for the " + c.getBoard().getName() + " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
    			}
    		}
    	}
    }
    
    static void createNewConfiguration(LocalConfigKey localConfigKey, ConfigurationManager configurationManger, ConfigurationDAO configurationDAO, Chip c) throws DataAccessException {
    	//SynCoderConf oldConf = (SynCoderConf) configurationManger.getConfiguration(c, configurationManger.getDefaultLocalConfigKey());
    	SynCoderConf newConf = new  SynCoderConf();//
    	//newConf.setWindowOpen(219);
    	//newConf.setWindowClose(219);
    	//newConf.setLmuxInDelay(1);
    	configurationDAO.saveObject(newConf);
    	configurationManger.assignConfiguration(c, newConf, localConfigKey); 
    }
    
    static void countNotConnectedStrips(EquipmentDAO equipmentDAO) throws DataAccessException, FileNotFoundException {
    	long start = System.currentTimeMillis();
    	int notConnectedStrips = 0;
    	for(Board board : equipmentDAO.getBoardsByType(BoardType.LINKBOARD, true)) {
    		LinkBoard lb = (LinkBoard)board;
    		Binary mask = new Binary(lb.getConnectedStripsMask());
			for (int i=0; i < mask.getBitNum();i++){
				boolean bitValue = mask.getBits(i, 1) != 0;	
				if(!bitValue)
					notConnectedStrips++;

			}
    	}
    	System.out.println("countNotConnectedStrips() " + (System.currentTimeMillis() - start)/1000. + " s");
    	System.out.println("countNotConnectedStrips: " + notConnectedStrips);
    }
    
    static void printNotConnectedStrips(EquipmentDAO equipmentDAO) throws DataAccessException, FileNotFoundException {
    	long start = System.currentTimeMillis();
    	int notConnectedStrips = 0;
    	Set<Byte> byteSet = new TreeSet<Byte>();
    	Set<String> stripsSet = new TreeSet<String>();
    	for(Board board : equipmentDAO.getBoardsByType(BoardType.LINKBOARD, true)) {
    		LinkBoard lb = (LinkBoard)board;
    		if(lb.getChamberLocations().get(0).getBarrelOrEndcap() == BarrelOrEndcap.Endcap)
    			continue;
    		
    		Binary mask = new Binary(lb.getConnectedStripsMask());
			/*for (int i=0; i < mask.getBitNum();i++){
				boolean bitValue = mask.getBits(i, 1) != 0;	
				if(!bitValue)
					notConnectedStrips++;
			}*/
    		System.out.print(String.format("%1$" + 50 + "s", lb.getName() + " " + lb.getChamberName() + "\t" )); // + mask.toString()
    		String stripsStr = "";
    		for(byte b : lb.getConnectedStripsMask()) {
    			//String str = Integer.toBinaryString(b);
    			//String str = Integer.toHexString(b);
    			//str = str.substring(str.length() - 2);
    			String str = ("0000000" + Integer.toBinaryString(0xFF & b)).replaceAll(".*(.{8})$", "$1");
    			stripsStr =  stripsStr + " " + str;
    			System.out.print(str + " ");
    			byteSet.add(b);
    		}
    		stripsSet.add(stripsStr);
    		System.out.println(mask.toString());
    	}
    	for(Byte b : byteSet) {
    		String str = ("0000000" + Integer.toBinaryString(0xFF & b)).replaceAll(".*(.{8})$", "$1");
    		System.out.println(str);
    	}
    	
    	for(String stripsStr :  stripsSet) {
    		System.out.println(stripsStr);
    	}
    	System.out.println("printNotConnectedStrips() " + (System.currentTimeMillis() - start)/1000. + " s");
    	//System.out.println("countNotConnectedStrips: " + notConnectedStrips);
    }
    
    static void countMaskedStrips(LocalConfigKey localConfigKey, EquipmentDAO equipmentDAO, ConfigurationDAO configurationDAO, ConfigurationManager configurationManager) throws DataAccessException, FileNotFoundException {
    	long start = System.currentTimeMillis();
    	List<ChipConfAssignment> chipConfAssignments = configurationDAO.getChipConfAssignments(ChipType.SYNCODER, localConfigKey);    	System.out.println("getChipConfAssignments() " + (System.currentTimeMillis() - start)/1000. + " s");
    	
    	int maskedStripsCnt = 0;
    	for(ChipConfAssignment chipConfAssignment : chipConfAssignments) {   
    		SynCoderConf synCoderConf  = (SynCoderConf)chipConfAssignment.getStaticConfiguration();

    		Binary connectedMask = new Binary( ((LinkBoard)chipConfAssignment.getChip().getBoard()).getConnectedStripsMask() );
    		
    		Binary mask = new Binary(synCoderConf.getInChannelsEna());    		
    		for (int i=0; i < mask.getBitNum();i++){
				boolean bitValue = mask.getBits(i, 1) != 0;		
				boolean connectedbitValue = connectedMask.getBits(i, 1) != 0;
				if(!bitValue && connectedbitValue) {
					maskedStripsCnt++;
				}
			}
    	}

    	System.out.println("countMaskedStrips() " + (System.currentTimeMillis() - start)/1000. + " s");
    	System.out.println("countMaskedStrips: " + maskedStripsCnt);
    }
    
/*    public static BitSet bitsetFromBytes(byte[] byteArray) {
    	BitSet bitSet = new BitSet(byteArray.)
    }*/
    static void printMaskedStrips(LocalConfigKey localConfigKey, EquipmentDAO equipmentDAO, ConfigurationDAO configurationDAO, ConfigurationManager configurationManager) throws DataAccessException, FileNotFoundException {
    	long start = System.currentTimeMillis();
    	List<ChipConfAssignment> chipConfAssignments = configurationDAO.getChipConfAssignments(ChipType.SYNCODER, localConfigKey);
    	System.out.println("getChipConfAssignments() " + (System.currentTimeMillis() - start)/1000. + " s");
    	
    	int maskedStripsCnt = 0;
    	int iMasked = 0;
    	for(ChipConfAssignment chipConfAssignment : chipConfAssignments) {   
    		SynCoderConf synCoderConf  = (SynCoderConf)chipConfAssignment.getStaticConfiguration();

    		LinkBoard linkBoard = ((LinkBoard)chipConfAssignment.getChip().getBoard());
    		Binary connectedMask = new Binary( linkBoard.getConnectedStripsMask() );
    		
    		Binary mask = new Binary(synCoderConf.getInChannelsEna());
    		
    		for (int i=0; i < mask.getBitNum();i++){
				boolean bitValue = mask.getBits(i, 1) != 0;		
				boolean connectedbitValue = connectedMask.getBits(i, 1) != 0;
				if(!bitValue && connectedbitValue) {
					maskedStripsCnt++;
				}
			}
    		    		
    		for(FebLocation febLocation : linkBoard.getFEBLocations()) {
    			/*System.out.print(febLocation.getChamberLocation().getChamberLocationName() + " " +
    					febLocation.getI2cLocalNumber() + " " + febLocation.getFebLocalEtaPartition() + " ");*/
    			for(FebConnector febConnector : febLocation.getFebConnectors() ) {
    				int linkBoardInputNum = febConnector.getLinkBoardInputNum() -1;
    				for(ChamberStrip strip : febConnector.getChamberStrips()) {
    					int cableChannelNum = strip.getCableChannelNum() -1;
    					int channelNumber = linkBoardInputNum * 16 + cableChannelNum;

    					boolean bitValue = mask.getBits(channelNumber, 1) != 0;		
    					boolean connectedbitValue = connectedMask.getBits(channelNumber, 1) != 0;
    					if(!bitValue && connectedbitValue) {
    						iMasked++;
    						System.out.println(iMasked + " " + febLocation.getChamberLocation().getChamberLocationName() + "_" +
    								febLocation.getI2cCbChannel().getCbChannel() + "_" + febLocation.getI2cLocalNumber() + " " + 
    								febLocation.getFebLocalEtaPartition()  +
    		    					" LB channel number "  + channelNumber + " strip number " +  strip.getChamberStripNumber());
    					}
    				}
    			}
    		}
    		
    		/*for(int i = 0; i <  linkBoard.getFebChips().length; i++ ) {
    			System.out.print(iChip++ + " ");
    			Chip chip = linkBoard.getFebChips()[i];
    			if(chip != null) {
	    			FebBoard febBoard = (FebBoard)chip.getBoard();
	    			System.out.print(febBoard.getFebLocation().getChamberLocation().getChamberLocationName() + " " +
	    					febBoard.getFebLocation().getI2cLocalNumber() + " " + chip.getPosition() + " ");
	    			for(int j = 0; j < FebBoard.FEB_CHIP_CHANNEL_CNT; j++) {
	    				int channelNum = i * FebBoard.FEB_CHIP_CHANNEL_CNT + j;
	    				if(connectedMask.getBits(channelNum, 1) == 0) {
	    					System.out.print("x");
	    				}
	    				else if ( mask.getBits(channelNum, 1) == 0){
	    					System.out.print("0");
	    				}
	    				else {
	    					System.out.print("1");
	    				}
	    			}	    			
    			}
    			System.out.print("\n");
    		}*/
    	}

    	System.out.println("countMaskedStrips() " + (System.currentTimeMillis() - start)/1000. + " s");
    	System.out.println("countMaskedStrips: " + maskedStripsCnt);
    }
    
    static void printStrps(String lbName, EquipmentDAO equipmentDAO) throws DataAccessException {
    	Map<String, Integer> etaPartitionToNum =  new TreeMap<String, Integer>();
    	etaPartitionToNum.put("Backward", 1);
    	etaPartitionToNum.put("Middle", 2);
    	etaPartitionToNum.put("Forward", 3);
    	
    	etaPartitionToNum.put("A", 1);
    	etaPartitionToNum.put("B", 2);
    	etaPartitionToNum.put("C", 3);
    	
    	
    	
    	LinkBoard lb = (LinkBoard)(equipmentDAO.getBoardByName(lbName));
    	System.out.println(lbName + " " + lb.getChamberName());
    	for(FebLocation febLocation : lb.getFEBLocations()) {
    		for(FebConnector febConnector : febLocation.getFebConnectors() ) {
    			int linkBoardInputNum = febConnector.getLinkBoardInputNum() -1;
    			for(ChamberStrip strip : febConnector.getChamberStrips()) {
    				int cableChannelNum = strip.getCableChannelNum() -1;
    				int channelNumber = linkBoardInputNum * 16 + cableChannelNum;
    				System.out.println(channelNumber + " " + etaPartitionToNum.get(febLocation.getFebLocalEtaPartition()) + " " + strip.getChamberStripNumber());
    			}
    		}
    	}  
    	System.out.println();
    }
    
    static void modifySetting(String lbName, int bcn0Delay, LocalConfigKey localConfigKey, EquipmentDAO equipmentDAO, ConfigurationDAO configurationDAO, ConfigurationManager configurationManager) throws DataAccessException, FileNotFoundException {
    	 LinkBoard lb = (LinkBoard)equipmentDAO.getBoardByName(lbName);
    	 
    	 SynCoderConf oldSynCoderConf  = (SynCoderConf)configurationDAO.getChipConfAssignment(lb.getSynCoder(), localConfigKey).getStaticConfiguration();
    	 SynCoderConf newSynCoderConf = new SynCoderConf(oldSynCoderConf);
    	 
    	 //newSynCoderConf.setOptLinkSendDelay(optLinkSendDelay); //TODO modify here what is needed
    	 newSynCoderConf.setBcn0Delay(bcn0Delay); //TODO modify here what is needed
    	 
    	 configurationDAO.saveObject(newSynCoderConf);
    	 configurationManager.assignConfiguration(lb.getSynCoder(), newSynCoderConf, localConfigKey);    	
    }
    
    static void disconnectLinks(EquipmentDAO equipmentDAO, ConfigurationDAO configurationDAO, ConfigurationManager configurationManager) throws DataAccessException {
		
		//this links were disconnected since there is a problem in the spliter
/*		List<LinkConn> linkConns = ((LinkBoard)equipmentDAO.getBoardByName("LB_RB-2_S8_BN2E_CH0")).getLinkConns();
		for(LinkConn linkConn : linkConns) {
			if(linkConn.getCollectorBoard().getName().equals("TBn2_6") || linkConn.getCollectorBoard().getName().equals("TBn1_6")) {
				configurationManager.disconnectLinkConn(linkConn);
			}
			else {
				LinkDisabled linkDisabled = configurationDAO.getLinkDisabled(linkConn);
				configurationDAO.deleteObject(linkDisabled);
        		System.out.println("Enabled linkConn " + linkConn);
			}
		}*/
		
    	//changing links connections, since the TBp1_1 -> DCC connection is not working, and the below links were going only to the TBp1_1
		//List<LinkConn> linkConns = ((LinkBoard)equipmentDAO.getBoardByName("LB_RB+1_S3_BP1E_CH0")).getLinkConns();
/*		for(LinkConn linkConn : linkConns) {
			if(linkConn.getCollectorBoard().getName().equals("TBp1_1") ) {//13
				configurationManager.disconnectLinkConn(linkConn);
				System.out.println("disconnectLinkConn " + linkConn);
			}
			else if(linkConn.getCollectorBoard().getName().equals("TBp1_2") ) {//4
				LinkDisabled linkDisabled = configurationDAO.getLinkDisabled(linkConn);
				if(linkDisabled != null) {
					configurationDAO.deleteObject(linkDisabled);
					System.out.println("Enabled linkConn " + linkConn);
				}
			}
		}*/
		
/*		linkConns = ((LinkBoard)equipmentDAO.getBoardByName("LB_RB+1_S3_BP1C_CH0")).getLinkConns();
		for(LinkConn linkConn : linkConns) {
			if(linkConn.getCollectorBoard().getName().equals("TBp1_1") ) {//15
				configurationManager.disconnectLinkConn(linkConn);
				System.out.println("disconnectLinkConn " + linkConn);
			}
			else if(linkConn.getCollectorBoard().getName().equals("TBp1_2") ) {//2
				LinkDisabled linkDisabled = configurationDAO.getLinkDisabled(linkConn);
				if(linkDisabled != null) {
					configurationDAO.deleteObject(linkDisabled);
					System.out.println("Enabled linkConn " + linkConn);
				}
			}
		}*/
		
				
    	List<LinkConn> linkConns = ((LinkBoard)equipmentDAO.getBoardByName("LB_RB+1_S3_BP1A_CH0")).getLinkConns();
		for(LinkConn linkConn : linkConns) {
			if(linkConn.getCollectorBoard().getName().equals("TBp1_1") ) {//17
				configurationManager.disconnectLinkConn(linkConn);
				System.out.println("disconnectLinkConn " + linkConn);
			}
			else if(linkConn.getCollectorBoard().getName().equals("TBp1_2") ) {//0
				LinkDisabled linkDisabled = configurationDAO.getLinkDisabled(linkConn);
				if(linkDisabled != null) {
					configurationDAO.deleteObject(linkDisabled);
					System.out.println("Enabled linkConn " + linkConn);
        		}
			}
		}
    }
    
    public static void main(String[] args) throws DataAccessException {
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        //HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);	 

        try {
            //Chip c = (equipmentDAO.getBoardByName("LB_M14").getChips().get(0));
            
            /*StaticConfiguration configuration = configurationManger.getConfiguration(c, configurationManger.getDefaultLocalConfigKey());
            if(configuration != null) {
                System.out.println("the configuration for the " + c.getBoard().getName() + " is \n" + configuration);
            }*/
             
            
             //SynCoderConf configuration = (SynCoderConf) configurationManger.getConfiguration(c, configurationManger.getDefaultLocalConfigKey());
/*
            configurationManger.assignConfiguration(equipmentDAO.getBoardByName("LB_M4").getChips().get(0), configuration, ConfigurationManger.LOCAL_CONF_KEY_DEFAULT);         
             configurationManger.assignConfiguration(equipmentDAO.getBoardByName("LB_M7").getChips().get(0), configuration, ConfigurationManger.LOCAL_CONF_KEY_DEFAULT);
             configurationManger.assignConfiguration(equipmentDAO.getBoardByName("LB_M14").getChips().get(0), configuration, ConfigurationManger.LOCAL_CONF_KEY_DEFAULT);
             configurationManger.assignConfiguration(equipmentDAO.getBoardByName("LB_M17").getChips().get(0), configuration, ConfigurationManger.LOCAL_CONF_KEY_DEFAULT);
             */	     	     

            //createDummyCongiguration(equipmentDAO, configurationManager, configurationDAO);
            //printConfiguration(equipmentDAO, configurationManager, configurationDAO);;
            //printLBChamberAssigment(equipmentDAO);;
            //printSelectedOptLinks(equipmentDAO);
            
        	//printNotConnectedStrips(equipmentDAO);
        	
/*            try {
				printOptoLBChamberAssigment(equipmentDAO);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}*/
        	
        	//disconnectLinks(equipmentDAO, configurationDAO, configurationManager);
        	
            try {
				printLBChamberAssigment1(equipmentDAO);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
            
            //printLBoxes(equipmentDAO);
        	//printOptoLBChamberAssigment(equipmentDAO);
        	//countNotConnectedStrips(equipmentDAO);
        	//countMaskedStrips(localConfigKey, equipmentDAO, configurationDAO, configurationManager);
        	
        	LocalConfigKey localConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.CURRENT_LBS_CONF_KEY);
        	//LocalConfigKey localConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_LBS_LHC1);
        	//LocalConfigKey localConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_LBS_DEFAULT);

        	//modifySetting("LB_RE-1_S6_EN22_CH0", localConfigKey, equipmentDAO, configurationDAO, configurationManager);
        	//modifySetting("LB_RE-1_S6_EN23_CH0", localConfigKey, equipmentDAO, configurationDAO, configurationManager);
        	
        	//modifySetting("LB_RE-4_S4_EN42_CH0", 3, localConfigKey, equipmentDAO, configurationDAO, configurationManager);
        	//modifySetting("LB_RE+1_S6_EP12_CH0", 2, localConfigKey, equipmentDAO, configurationDAO, configurationManager);
        	
        	//printOptLinkChamberAssigment(equipmentDAO);
        	
        	//printMaskedStrips(localConfigKey, equipmentDAO, configurationDAO, configurationManager);
        	//createNewConfiguration(localConfigKey, configurationManager, configurationDAO,  ((LinkBoard)equipmentDAO.getBoardByName("LB_LBB_904_S3")).getSynCoder());
            
            //StaticConfiguration configuration = configurationManger.getConfiguration( (equipmentDAO.getBoardByName("LB_S15").getChips().get(0)), configurationManger.getDefaultLocalConfigKey());
            //configurationManger.assignConfiguration(equipmentDAO.getBoardByName("LB_S13").getChips().get(0), configuration, ConfigurationManger.LOCAL_CONF_KEY_DEFAULT);
        	
        	//----------------------------------------------------------
        	/*SynCoderConf masterSynCoderConf = new SynCoderConf();
            masterSynCoderConf.setLmuxInDelay(2);
            SynCoderConf slaveSynCoderConf = new SynCoderConf();
            byte[] inChannelsEna = slaveSynCoderConf.getInChannelsEna();
            inChannelsEna[0] = (byte) 0xff;
            inChannelsEna[1] = (byte) 0xff;
            inChannelsEna[2] = (byte) 0xff;
            slaveSynCoderConf.setInChannelsEna(inChannelsEna);
            
            configurationDAO.saveObject(masterSynCoderConf);
            configurationDAO.saveObject(slaveSynCoderConf);*/
            
            
            /*LocalConfigKey localConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_LBS_DEFAULT);
            LinkBoard slb = (LinkBoard)equipmentDAO.getBoardByName("LB_LBB_904_2_S4");
            LinkBoard mlb = (LinkBoard)equipmentDAO.getBoardByName("LB_LBB_904_2_M5");
            
            StaticConfiguration slaveSynCoderConf = configurationManager.getConfiguration(slb.getSynCoder(), localConfigKey);
            StaticConfiguration masterSynCoderConf = configurationManager.getConfiguration(mlb.getSynCoder(), localConfigKey);
            
        	LinkBox linkBox = (LinkBox)equipmentDAO.getCrateByName("LBB_904");
        	for(LinkBoard linkBoard : linkBox.getLinkBoards()) {
        		if(linkBoard.isMaster())
        			configurationManager.assignConfiguration(linkBoard.getSynCoder(), masterSynCoderConf, ConfigurationManager.LOCAL_CONF_KEY_LBS_DEFAULT);
        		else 
        			configurationManager.assignConfiguration(linkBoard.getSynCoder(), slaveSynCoderConf, ConfigurationManager.LOCAL_CONF_KEY_LBS_DEFAULT);
        	}*/
        	//-----------------------------------------------------------
        	//printConfiguration(equipmentDAO, configurationManager, configurationDAO);
        	//configurationManager.tagCurrentConfigs(localConfigKey, ChipType.SYNCODER, "11_11_07_COLLISIONS", "used duriong 2011 proton runs, last timing update 2011 05 24, +1/2BX w.r.t the 2010, mask tuned by hand only");
        	
        	//printStrps("LB_RB0_S1_BP0B_CH2", equipmentDAO);
        	//printStrps("LB_RE+1_S1_EP12_CH0", equipmentDAO);        	       
        } 
        catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            context.rollback();
        } /*catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} */        
        finally {
            context.closeSession();
        }
    }

}
