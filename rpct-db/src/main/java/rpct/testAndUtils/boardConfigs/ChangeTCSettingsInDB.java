package rpct.testAndUtils.boardConfigs;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.rpc.ServiceException;

import rpct.datastream.RpctDelays;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.IntArray18;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.PacConf;
import rpct.db.domain.configuration.RmbConf;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.configuration.TcSortConf;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbMuxRecChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;

public class ChangeTCSettingsInDB {
    public static void main(String[] args) throws DataAccessException {
        if(args.length == 0) {            
            String[] argsL = {                    
                    "TC_0",                    
                    "TC_1",
                    "TC_2",

                    "TC_3",                    
                    "TC_4",
                    "TC_5",

                    "TC_6",                    
                    "TC_7",
                    "TC_8",

                    "TC_9",                    
                    "TC_10",
                    "TC_11",
//            		"PASTEURA_TC"
            };

            args = argsL;
        }

        System.out.println("Selected crates");
        for(int i = 0; i < args.length; i++) {
            System.out.print(args[i] + " ");
        }

        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);   

		boolean saveChangesToDb = true; //TODO

		boolean changePACsettings = true;
		boolean changeRMBsettings = false;
		boolean changeTBGBSsettings = false;
		boolean changeTCGBSsettings = false;
		
        //RMB configuration
/*        int rmbDataDelay = RpctDelays.RMB_DATA_DELAY;        
        int chanEna = 0; //0 = all channels enable
        int preTriggerVal = RpctDelays.PRE_TRIGGER_VAL; //zakers BX branych do DAQ
        int postTriggerVal = RpctDelays.POST_TRIGGER_VAL; 
        int trgDelay = 0; */
        
        int bxOfCoincidence = 0;
        int pacEna = 0xfff;
        int pacConfigId = 0x3141; //TODO     
        
        String bootScript = "boot_lhc_cosmic_v" + Integer.toHexString(pacConfigId) + ".sh"; //TODO

        LocalConfigKey outConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_TCS_V1049); //TODO
        //this changes were not used to change the P5 settings
        try {
            List<TriggerCrate> crates = new ArrayList<TriggerCrate>();
            for(int i = 0; i < args.length; i++) {                
                crates.add((TriggerCrate)equipmentDAO.getCrateByName(args[i]));
            }

            for(TriggerCrate tc : crates) {   
                for(TriggerBoard tb : tc.getTriggerBoards()) {
                    for(Chip chip : tb.getChips()) {
                        StaticConfiguration conf = null;
                        if(chip.getType() == ChipType.PAC && chip.getDisabled() == null && changePACsettings
                        		&& tb.getName().equals("TB0") && chip.getPosition() == 8) {
                        	try {
                        		conf = configurationManager.getConfiguration(chip, outConfigKey);
                        		PacConf pacConf = (PacConf)conf; 
                        		/*for(int i = 0; i < pacConf.getRecDataDelay().size(); i++) {
                                if( pacConf.getRecDataDelay().getValues()[i] != 1 ) {
                                    throw new IllegalArgumentException(chip + "pacConf.getRecDataDelay().getValues()[i] != 1");
                                }
                                pacConf.getRecDataDelay().setValue(i, 0);  
                                System.out.println("Changing config for " + chip);
                            }*/

                                //pacConf.setPacConfigId(pacConfigId);
                                //pacConf.setBxOfCoincidence(bxOfCoincidence);
                                
                                /*does not work like that ( IntArray18() recDataDelay must be crated or saved explicite?)
                                 * but beside it cannot be done like that, becosue then the tranmission errors appears
                                 * for(int i = 0; i < 18; i++) {
                                	pacConf.getRecDataDelay().setValue(i, 1); //needed for the firmware under the key LHC10 i.e. LOCAL_CONF_KEY_TCS_V1049,where the latency is 1 bx smaller
                                }*/
                        	}
                        	catch (NullPointerException e) {
                        		System.out.println(e.getMessage());                        		
                        	}
                        }
                        else if(chip.getType() == ChipType.RMB && changeRMBsettings) {
                        	conf = configurationManager.getConfiguration(chip, outConfigKey);
                            RmbConf rmbConf = (RmbConf)conf;
                            rmbConf.setDataDelay(RpctDelays.RMB_DATA_DELAY);
                            //((RmbConf)conf).setChanEna(chanEna); //TODO
                            //((RmbConf)conf).setPreTriggerVal(RpctDelays.PRE_TRIGGER_VAL);
                            //((RmbConf)conf).setPostTriggerVal(RpctDelays.POST_TRIGGER_VAL);
                            //((RmbConf)conf).setTrgDelay(trgDelay);
                        }
                        else if(chip.getType() == ChipType.GBSORT && changeTBGBSsettings) {
                        }
                        
                        if(conf != null && saveChangesToDb) {
                        	System.out.println("Changing config for " + chip);
                            configurationDAO.saveObject(conf);
                        }
                    }  
                    if(changeTCGBSsettings) {
                    	Chip chip = tc.getBackplane().getTcSorter();
        				TcSortConf conf = (TcSortConf)configurationManager.getConfiguration(chip, outConfigKey);
        				((TcSortConf)conf).setBootScript(bootScript);
        				if(conf != null && saveChangesToDb) {
        					configurationDAO.saveObject(conf);
        					System.out.println("Changing config for " + chip);
        				}
                    }
                }
            }
            
            //configurationManager.assignConfigKey(outConfigKey.getName(), outConfigKey); //TODO
        }              
        catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            context.rollback();
        }
        finally {
            context.closeSession();
        }
    }

}
