package rpct.testAndUtils.boardConfigs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.configuration.SynCoderConf;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.axis.Binary;

public class PutStripMasksToDB {

	private static PrintStream prntSrm = null;
	private static double maxTTCFiber = 0;    


	static void applyMasksFromFile(boolean safeToDb, File inputFile, EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO, 
			LocalConfigKey[] configKeys) throws DataAccessException, FileNotFoundException {

		Scanner scanner = new Scanner(inputFile);

		Map<String, String> chamberStripMaskMap = new HashMap<String, String>(); //<chaberName, stripMask>

		if(safeToDb) {
			System.out.println("Config will be seved to DB"); 
		}
		else {
			System.out.println("Config will NOT be seved to DB, simuation only!!!!!!!!!!!!!!!!!!!!!!!!!!!"); 
		}

		prntSrm.println("applyMasksFromFile fileName " + inputFile.getPath() + "/" + inputFile.getName());

		Map<String, LinkBoard> chamberBoardsMap = new HashMap<String, LinkBoard>();

		for(Crate crate : equipmentDAO.getCratesByType(CrateType.LINKBOX, false) ) {
			{
				//prntSrm.println(crate.getName());
				Map<Integer, Board> boardsMap = new TreeMap<Integer, Board>();                
				for(Board board : crate.getBoards()) {
					if(board.getType() == BoardType.LINKBOARD) {
						boardsMap.put(board.getPosition(), board);
						LinkBoard lb = (LinkBoard) board;
						/*for(String chamberName : lb.getChamberNames()) {
							chamberBoardsMap.put(chamberName.replaceFirst(" ", ""), lb); //<!!!!!!!!!!!!!!!!!!!!!!
						}*/
						
						chamberBoardsMap.put(lb.getChamberName(), lb);
					}
				}                                                              
			}
		}

		//reading the file                
		while(scanner.hasNextLine()) {
			if(scanner.hasNext() == false) {
				break;
			}
			String chamberName = scanner.next();
			//String chamberName = scanner.next() + " " + scanner.next(); //<!!!!!!!!!!!!!!!!!!!!!!       
			//LinkBoard lb = chamberBoardsMap.get(chamberName);
			String stripMask = scanner.next();

			String stripMaskPrev = chamberStripMaskMap.get(chamberName);
			if(stripMaskPrev != null) {
				if(!stripMaskPrev.equals(stripMask)) {
					prntSrm.println("differnt values alrady read from the file for the " + chamberName);
					prntSrm.println(stripMaskPrev);
					prntSrm.println(stripMask);
				}
			}
			else {
				chamberStripMaskMap.put(chamberName, stripMask);
			}
		}

		boolean noLbForChamberErr = false;
		for(Map.Entry<String, String> e : chamberStripMaskMap.entrySet()) {
			LinkBoard lb = chamberBoardsMap.get(e.getKey());
			if(lb != null) {
				prntSrm.println(lb.getName() + " " + e.getKey());
				for(LocalConfigKey configKey : configKeys) {
					prntSrm.println("localConfigKey " + configKey);
					StaticConfiguration configuration = configurationManager.getConfiguration(lb.getSynCoder(), configKey);

					if(configuration != null) {
						SynCoderConf coderConf = (SynCoderConf) configuration;                                        
						Binary newMask = new Binary(e.getValue());
						byte[] newMaskBytes = newMask.getBytes();                    
						for(int i = 0; i < newMaskBytes.length; i++) {
							newMaskBytes[i] = (byte) (newMaskBytes[i] & lb.getConnectedStripsMask()[i]);
						}

						prntSrm.println("oldMaskBytes " + new Binary(coderConf.getInChannelsEna()).toString() );
						if(Binary.compareBytes(newMaskBytes, coderConf.getInChannelsEna()) == false) {
							SynCoderConf newCoderConf = new SynCoderConf(coderConf);
							prntSrm.println("newMaskBytes " + (new Binary(newMaskBytes)).toString() +"\n");
							newCoderConf.setInChannelsEna(newMaskBytes);

							if(safeToDb) {
								configurationDAO.saveObject(newCoderConf); //TODO
								configurationManager.assignConfiguration(lb.getSynCoder(), newCoderConf, configKey.getName());                      
							}    
						}
						else {
							prntSrm.println("old and new masks are the same, skipped\n");
						}
					}
					else {
						prntSrm.println("no configuration found for the LB "  +lb.getName() + " !!!!!!!!!!!!!!!!!!\n");
					}
				}
			}
			else {
				prntSrm.println("no LB found for the chamber " + e.getKey() + " !!!!!!!!!!!!!!!!!!\n");
				noLbForChamberErr = true;
			}
		}
		if(noLbForChamberErr)
			throw new IllegalArgumentException("no LB found for some chamber names");

		//System.out.println("the outpuit is in " + outfileName);
	}

	public static void main(String[] args) throws DataAccessException {
		if(args.length != 1) {
			throw new IllegalArgumentException("illegal number of program arguments. There should be one argument - name of the file with strip masks");
		}  

		String fileName = args[0];
		String fileDir = System.getenv("HOME") + "/bin/data/";
		File inputFile =  null;

		if(fileName.contains("~")) {
			fileName = fileName.replaceFirst("~", System.getenv("HOME"));
		}

		inputFile =  new File(fileName);
		if(inputFile.canRead() == false) { //i tak zawsze jakis sie tworzy, nawet jak jest zla sciezka
			System.err.println("the file:\n" + fileName + "cannot be opened");
			return;
		}

		HibernateContext context = new SimpleHibernateContextImpl();
		EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
		//HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
		ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
		ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);	 

		try {
			Date date = new Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy MM dd  HH:mm");
			String outfileName = System.getenv("HOME") + "/bin/out/LBTimingSettings/stripMasksOut.txt";
			FileOutputStream file = new FileOutputStream(outfileName, true);

			//String fileName = "NoiseMask_0.3_210mV.list";
			//String fileName = "24_7_2009_210mV.list";
			LocalConfigKey[] configKeys = new LocalConfigKey[1];
			configKeys[0] = configurationManager.getLocalConfigKey(ConfigurationManager.CURRENT_LBS_CONF_KEY);
			//configKeys[0] = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_LBS_LHC1);
			//configKeys[1] = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_LBS_LHC2);

			try {
				prntSrm = System.out;
				System.out.println("validating the strip masks file");
				applyMasksFromFile(false, inputFile, equipmentDAO, configurationManager, configurationDAO, 
						configKeys) ; //the key, under which the corrected data will be stored
			}

			catch (IllegalArgumentException e) {
				System.err.println("error during validation of the file:\n" + e.getMessage());
				System.err.println("check the chaber names in the file");
				return;
			}


			System.out.println("validation of the strip masks file successful. Put mask to the database (y/n)?");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			String y_n = br.readLine();
			if(y_n.equals("y")) {
				prntSrm = new PrintStream(file);
				prntSrm.println("\n" + dateFormat.format(date) + "\n" + fileName);

				for(LocalConfigKey localConfigKey : configKeys) {
					System.out.print("\nsetting will be updates for the localConfigKey: " + localConfigKey + "\n used for the global keys: ");
					for(String configKey : configurationDAO.getGlobalConfigKeysByLocalConfigKey(localConfigKey)) {
						System.out.print(configKey + ", ");
					}					
				}
				System.out.println("if you want to used other keys, contact Karol :) \n");
				
				applyMasksFromFile(true, inputFile, equipmentDAO, configurationManager, configurationDAO, 
						configKeys) ; //the key, under which the corrected data will be stored

				System.out.println("finished successfully");
			}
			else {
				System.out.println("finished without saving the changes to the DB");
			}

		} 
		/* catch (DataAccessException e) {
            e.printStackTrace();            
            context.rollback();
            System.out.println("context.rollback()");
        } */
		/*  catch (FileNotFoundException e) {            
            e.printStackTrace();
        }*/
		catch (Exception e) {
			//e.printStackTrace();
			System.err.println(e.getMessage());
			context.rollback();
			System.out.println("context.rollback()");
		} 
		finally {
			context.closeSession();
		}
	}

}
