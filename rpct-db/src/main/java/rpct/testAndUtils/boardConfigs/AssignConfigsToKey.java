package rpct.testAndUtils.boardConfigs;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;

public class AssignConfigsToKey {
    public static void main(String[] args) throws DataAccessException {
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);	 

        boolean saveNewConfToDb = true; //TODO
        ChipType chipTypes[] = { //TODO
//        		ChipType.SYNCODER,
        		
//        		ChipType.PAC,
        		ChipType.RMB,
//        		ChipType.GBSORT,
//        		ChipType.TCSORT,
        		
//        		ChipType.HALFSORTER,
//        		ChipType.TTUTRIG,
//        		ChipType.TTUFINAL,
//        		ChipType.RBC
        		
//        		ChipType.SYNCODER
        };
        
    	LocalConfigKey inConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_TCS_V1036); //TODO
    	LocalConfigKey[] outConfigKeys =  {
    			//configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_LHC3), //TODO
//    			configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_LHC3_2BX), //TODO
//    			configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_LHC3_3BX), //TODO
//    			configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_LHC3_BOTTOM), //TODO
    			configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_TCS_V1038), //TODO
    			configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_TCS_V1038_1EX), //TODO
    			//configurationManager.createLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_RBCS_TTUS_LHC1, LocalConfigKey.Subsystem.RBCS_TTUS ), //TODO
    			//configurationManager.createLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_SC_BOTTOM, LocalConfigKey.Subsystem.SC), //TODO
    	};

    	boolean assignGlobalToLocal = false; //TODO
		String globalConfigKey = "LHC4_BOTTOM"; //TODO
        try {  	
        	for(ChipType chipType : chipTypes) {
        		for(Chip chip : equipmentDAO.getChipsByType(chipType, true) ) {
        			StaticConfiguration configuration = configurationManager.getConfiguration(chip, inConfigKey);
        			if(configuration != null) {                                                             
        				if(saveNewConfToDb) {
        					for(LocalConfigKey outConfigKey : outConfigKeys) {
        						configurationManager.assignConfiguration(chip, configuration, outConfigKey.getName());
        						System.out.println("Configuration for chip " + chip.getBoard().getName() + " " + chip.getType() + " saved to DB under key " + outConfigKey.getName());
        					}
        				}
        				else {
        					System.out.println("Configuration for chip " + chip.getBoard().getName() + " " + chip.getType() + " not saved in the DB!!!!!!!!");
        				}
        			}
        			else {
        				System.out.println("Configuration for chip " + chip.getBoard().getName() + " " + chip.getType() + " no configuration under key " + inConfigKey.getName());
        			}
        		}
        	}
        	
        	//this assigns the local config key to the global
        	if(assignGlobalToLocal) {
        		for(LocalConfigKey outConfigKey : outConfigKeys) {
        			configurationManager.assignConfigKey(globalConfigKey, outConfigKey); 
        			System.out.println("assignConfigKey: local " + outConfigKey + " to global " + globalConfigKey);
        		}
        	}
        } 
        catch (Exception e) {
            e.printStackTrace();            
            context.rollback();
            System.out.println("context.rollback()");
        } 
        finally {
            context.closeSession();
        }
    }

}
