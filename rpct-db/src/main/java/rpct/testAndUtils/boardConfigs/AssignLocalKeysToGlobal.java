package rpct.testAndUtils.boardConfigs;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.ConfigKey;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.LocalConfigKey.Subsystem;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.synchro.DefOut;

public class AssignLocalKeysToGlobal {

    /**
     * @param args
     * @throws DataAccessException 
     */
	
	static void assignLocalKeysToGlobal(ConfigurationManager configurationManager, ConfigurationDAO configurationDAO ) throws DataAccessException {
		//ConfigKey globalConfigKey = configurationDAO.getGlobalConfigKeyByName("LHC9"); //TODO
		ConfigKey globalConfigKey = configurationManager.createGlobalConfigKey("LHC9_BOTTOM", "LHC patterns, pacConfigId = 0x1046, patterns the same as in the LHC9, only bottom sectors; RMB_DATA_DELAY includes +12BXs, BX_OR is 0, "); //TODO
				
		configurationManager.assignConfigKey(globalConfigKey.getId(), ConfigurationManager.LOCAL_CONF_KEY_FEBS_DEFAULT);
		configurationManager.assignConfigKey(globalConfigKey.getId(), ConfigurationManager.CURRENT_LBS_CONF_KEY);//LOCAL_CONF_KEY_LBS_DEFAULT is only for 904 and pasteura 
		configurationManager.assignConfigKey(globalConfigKey.getId(), ConfigurationManager.LOCAL_CONF_KEY_TCS_V1046);//LOCAL_CONF_KEY_TCS_V1039_plus12BX
		configurationManager.assignConfigKey(globalConfigKey.getId(), ConfigurationManager.LOCAL_CONF_KEY_SC_BOTTOM); //LOCAL_CONF_KEY_SC_FULL
		configurationManager.assignConfigKey(globalConfigKey.getId(), ConfigurationManager.LOCAL_CONF_KEY_RBCS_TTUS_LHC1);
	}
	
    public static void main(String[] args) throws DataAccessException {
    	HibernateContext context = new SimpleHibernateContextImpl();
        try {
            
            EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
            ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
            ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);	 

            //configurationManager.getLocalConfigKeys();
/*            
            ConfigKey globalConfigKey = new ConfigKey();
            globalConfigKey.setId("LHC4");
            globalConfigKey.setDescription("LHC patterns, LHC patterns, pacConfigId = 0x1030, patterns the same as in pacConfigId = 0x1022");
            configurationDAO.saveObject(globalConfigKey);

            LocalConfigKey localConfigKey = configurationManager.createLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_TCS_V1030, Subsystem.TCS);
            
            configurationManager.assignConfigKey(globalConfigKey.getId(), localConfigKey);
            */
            
            //configurationManager.assignConfigKey("DEFAULT", configurationManager.getLocalConfigKey("DEFAULT"));
            //configurationManager.assignConfigKey("DEFAULT", configurationManager.getLocalConfigKey("COSMIC"));
            
/*            LocalConfigKey localConfigKey1 = new LocalConfigKey();
            localConfigKey1.setName(ConfigurationManager.LOCAL_CONF_KEY_LBS_DEFAULT);
            localConfigKey1.setSubsystem(LocalConfigKey.Subsytem.LBS.name());
    
            
            LocalConfigKey localConfigKey2 = new LocalConfigKey();
            localConfigKey2.setName(ConfigurationManager.LOCAL_CONF_KEY_TCS_DEFAULT);
            localConfigKey2.setSubsystem(LocalConfigKey.Subsytem.TCS.name());*/
            
/*            configurationManager.assignConfigKey("DEFAULT", configurationManager.getLocalConfigKey("LBS_DEFAULT"));
            configurationManager.assignConfigKey("DEFAULT", configurationManager.getLocalConfigKey("TCS_DEFAULT"));*/
            
            
/*            LocalConfigKey localConfigKey2 = new LocalConfigKey();
            localConfigKey2.setName("FEBS_TEST");
            localConfigKey2.setSubsystem(LocalConfigKey.Subsytem.FEBS);
            configurationDAO.saveObject(localConfigKey2);*/
            
            //configurationManager.assignConfigKey("DEFAULT", configurationManager.getLocalConfigKey("FEBS_DEFAULT"));
            
            //assignLocalKeysToGlobal(configurationManager, configurationDAO);      
            
            configurationManager.assignConfigKey("LHC9", ConfigurationManager.LOCAL_CONF_KEY_TCS_V1047); //TODO
            
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            context.rollback();
        } 
        finally {
            context.closeSession();
        }
    }

}
