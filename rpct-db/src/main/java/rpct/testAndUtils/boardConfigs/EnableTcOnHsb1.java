package rpct.testAndUtils.boardConfigs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Set;

import javax.xml.rpc.ServiceException;

import org.apache.log4j.PropertyConfigurator;

import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.HalfSortConf;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.TcHsbConnection;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;

public class EnableTcOnHsb1 {

    /**
     * @param args
     * @throws DataAccessException 
     * @throws IOException 
     */
    public static void main(String[] args) throws DataAccessException, IOException {
        //PropertyConfigurator.configure("log4j.properties");
        HibernateContext context = new SimpleHibernateContextImpl();
        try {
            EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
            //HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
            ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
            ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);	 
            LocalConfigKey configKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_SC_BOTTOM);
            
            if(args.length == 0) {                
                System.out.println("no argumetn given, nothing will be changed in the DB");
                System.out.println("selected key " + configKey.getName());
                System.out.println("current setting in the DB:");
                
                
                for(int i = 0; i < 2; i++) {
                    Board hsb = equipmentDAO.getBoardByName("HSB_" + i);
                    Set<Chip> chips = hsb.getChips();
                    if (chips.size() != 1) {
                        throw new RuntimeException("invalid number of chips");
                    }
                    Chip hsbChip = chips.iterator().next();

                    HalfSortConf currentConf = (HalfSortConf) configurationManager.getConfiguration(hsbChip, configKey);
                    System.out.println(hsb.getName() + " recChanEna " + Integer.toHexString(currentConf.getRecChanEnaMask()));
                    System.out.println(currentConf.getRecChanEnaStr(hsb.getName()));
                    System.out.println();
                }
                
                return;
            }
            if (args.length < 1 || args.length > 3) {
                System.err.println("Syntax: TC_0 [enabledCablesInputMask = 0 | 1 | 2 | 3 ]");
                return;
            }

            String name = args[0];
            Crate crate = equipmentDAO.getCrateByName(name);
            if(crate == null) {
                throw new RuntimeException("crate with name " + name + " does not exists");
            }
            if(crate.getType() != CrateType.TRIGGERCRATE) {
                throw new RuntimeException("crate with name " + name + " is not TRIGGERCRATE");
            }
            
            int inputCablesMask  = Integer.parseInt(args[1]);
            if(inputCablesMask < 0 || inputCablesMask > 3)
                throw new RuntimeException("the inputCablesMask is not correct. Allowed values: 0...3");
            
            TriggerCrate tc = (TriggerCrate)crate;
            List<TcHsbConnection> tcHsbConnections = tc.getTcHsbConnections(equipmentDAO);
            for(TcHsbConnection tcHsbConnection : tcHsbConnections) {
                HalfSortConf currentConf = (HalfSortConf) configurationManager.getConfiguration(tcHsbConnection.getHalfSort(), configKey);
                System.out.println("selected key " + configKey.getName());
                System.out.println("Current config " + tcHsbConnection.getHalfSort().getBoard().getName() + " recChanEna " 
                        + Integer.toHexString(currentConf.getRecChanEnaMask()));
  
                int recChanEnaMask = currentConf.getRecChanEnaMask();
                
                recChanEnaMask &=  ~(3 << tcHsbConnection.getCableInputNum()); //reset
                recChanEnaMask |=  inputCablesMask << tcHsbConnection.getCableInputNum(); //set
                                       
                HalfSortConf newConf = new HalfSortConf(currentConf);
                newConf.setId(0); //czemu tak???????????????????????? KB
                newConf.setRecChanEna(recChanEnaMask);
                configurationDAO.saveObject(newConf);
                configurationManager.assignConfiguration(tcHsbConnection.getHalfSort(), newConf, configKey.getName());
                //configurationManger.assignConfiguration(tcHsbConnection.getHalfSort(), newConf, ConfigurationManger.LOCAL_CONF_KEY_COSMIC);
                System.out.println("New config " + tcHsbConnection.getHalfSort().getBoard().getName() + " recChanEna " 
                        + Integer.toHexString(newConf.getRecChanEnaMask()));

            }
            context.commit();
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			context.rollback();
		} catch (ServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
		finally {
			context.closeSession();
		}
    }

}
