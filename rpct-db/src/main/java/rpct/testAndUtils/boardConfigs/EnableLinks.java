package rpct.testAndUtils.boardConfigs;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import rpct.datastream.TestOptionsXml;
import rpct.datastream.TestOptionsXml.BoardSource;
import rpct.datastream.TestOptionsXml.OpticalLink;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.equipment.RbcBoard;
import rpct.db.domain.equipment.RbcTtuConn;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;

public class EnableLinks {

    public static void printLinks(boolean enabledOnly, EquipmentDAO equipmentDAO, ConfigurationManager configurationManager) throws DataAccessException {
        List<Crate> tcs = equipmentDAO.getCratesByType(CrateType.TRIGGERCRATE, true);
        for(Crate tc : tcs) {
            System.out.println("\n------------- " + tc.getName() + " ----------------------");
            for(Board tb : tc.getBoards()) {
                if (tb.getType() == BoardType.TRIGGERBOARD) {
                    TriggerBoard triggerBoard = (TriggerBoard)tb;
                    System.out.println(triggerBoard.getName());
                    for(LinkConn linkConn : equipmentDAO.getLinkConns(triggerBoard, enabledOnly)) {
                        System.out.println(linkConn.toString());
                    }
                    System.out.println();
                }
            }
            
            
        }
        
    }
    
/*    public static void getLinksStrings(Set<String> disabledLinks, Set<String> enabledLinks, EquipmentDAO equipmentDAO) throws DataAccessException {               
        List<Crate> tcs = equipmentDAO.getCratesByType(CrateType.TRIGGERCRATE, true);       
        for(Crate tc : tcs) {
            for(Board tb : tc.getBoards()) {
                if (tb.getType() == BoardType.TRIGGERBOARD) {
                    TriggerBoard triggerBoard = (TriggerBoard)tb;
                    for(LinkConn linkConn : equipmentDAO.getLinkConns(triggerBoard, false)) {
                        if(linkConn.isDisabled()) {
                            disabledLinks.add(linkConn.toString());
                        }
                        else {
                            enabledLinks.add(linkConn.toString());
                        }
                        
                    }                    
                }
            }            
            
        }        
    }*/
    
    public static void getDisabledLinksStrings(Set<String> disabledLinks, EquipmentDAO equipmentDAO, ConfigurationDAO configurationDAO) throws DataAccessException {               
/*        List<Crate> tcs = equipmentDAO.getCratesByType(CrateType.TRIGGERCRATE, true);       
        for(Crate tc : tcs) {
            for(Board tb : tc.getBoards()) {
                if (tb.getType() == BoardType.TRIGGERBOARD) {
                    TriggerBoard triggerBoard = (TriggerBoard)tb;
                    for(LinkConn linkConn : equipmentDAO.getLinkConns(triggerBoard, false)) {
                        if(linkConn.isDisabled()) {
                            disabledLinks.add(linkConn.toString());
                        }
                        
                    }                    
                }
            }            
            
        } */ 
        
        List<Integer> ids = configurationDAO.getDisabledLinksIds();
        for(Integer id : ids) {
            disabledLinks.add(equipmentDAO.getLinkConnById(id).toString());
        }
    }
    
    public static void printLinksToFile(EquipmentDAO equipmentDAO, ConfigurationDAO configurationDAO) throws DataAccessException, FileNotFoundException {
        PrintStream disabledOptLinksFile = new PrintStream(new FileOutputStream(System.getenv("HOME") + "/bin/out/disabledOptLinks.txt", true));
        Date date = new Date();
        disabledOptLinksFile.println(date);
        
        Set<String> disabledLinks = new TreeSet<String>();
        Set<String> enabledLinks = new TreeSet<String>();
        getDisabledLinksStrings(disabledLinks, equipmentDAO, configurationDAO);
        for(String link : disabledLinks) {
            disabledOptLinksFile.println(link);
        }
        disabledOptLinksFile.println("");
        
        System.out.println("all links, which are now disabled, were printed to the file ~/bin/out/disabledOptLinks.txt");
    }

    
    /**
     * @param args
     * @throws DataAccessException 
     * @throws IOException 
     */
    public static void main(String[] args) throws DataAccessException, IOException {
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        //HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);	 

        boolean onlyEnabledLBs = true;
        if(args.length > 0) {
        	if(args[0].equals("-onlyEnabledLBs=false"))
        		onlyEnabledLBs = false;
        }
        //printLinks(false, equipmentDAO, configurationManger);        
        try { 
        	String fileDir = System.getenv("HOME") + "/bin/config/enableLinks.xml";
            System.out.println("changing links enable/disable in the DB from the file "+ fileDir); 
            
            //TestOptionsXml testOptionsXml = TestOptionsXml.readFromFile(System.getenv("HOME") + "/bin/config/enableLinks.xml");
            TestOptionsXml testOptionsXml = TestOptionsXml.readFromFile(fileDir);
            TestOptionsXml.SourcesList sourcesList = testOptionsXml.getSourcesList("enableLiks");                      
            
            List<BoardSource> boardSources = sourcesList.getBoardSources();
            for(BoardSource boardSource : boardSources) {
                Set<Board> boards = testOptionsXml.getBoards(boardSource, equipmentDAO, onlyEnabledLBs);
                /*
                 * jesli ja,is LB jest diasbled, to jesli onlyEnabledLBs = true nie pojawi sie w boards. oznacza to, ze nie bedzie go mozna
                 * ani zdisablowac, ani zenablowac
                 */
                for(Board board : boards) {
                	if(board.getType() == BoardType.LINKBOARD) {
                		for(LinkConn linkConn : ((LinkBoard)board).getLinkConns() ) {
                			if(boardSource.getOptions().equals("enable")) {
                				configurationManager.enableLinkConn(linkConn, true);
                				//System.out.println("enablig " + linkConn);
                			}
                			else if(boardSource.getOptions().equals("disable")) {
                				configurationManager.enableLinkConn(linkConn, false);
                				//System.out.println("disablig " + linkConn);
                			}
                			else 
                				throw new IllegalArgumentException("the opt value is not enable or disable");
                		}
                	}
                	else if(board.getType() == BoardType.RBCBOARD) {
                		for(RbcTtuConn linkConn : ((RbcBoard)board).getLinkConns() ) {
                			if(boardSource.getOptions().equals("enable")) {
                				configurationManager.enableLinkConn(linkConn, true);
                				//System.out.println("enablig " + linkConn);
                			}
                			else if(boardSource.getOptions().equals("disable")) {
                				configurationManager.enableLinkConn(linkConn, false);
                				//System.out.println("disablig " + linkConn);
                			}
                			else 
                				throw new IllegalArgumentException("the opt value is not enable or disable");
                		}                		
                	}
                	else {
                		throw new IllegalArgumentException("the board is not LinkBoard nor RBCBOARD");
                	}
                }
            }
            
            for(OpticalLink opticalLink : testOptionsXml.getOpticalLinks()) {
                TriggerBoard triggerBoard = (TriggerBoard)equipmentDAO.getBoardByName(opticalLink.getTbName());
                if(triggerBoard == null) {
                    throw new IllegalArgumentException("TriggerBoard with name " + opticalLink.getTbName() + " not found");
                }
                LinkConn linkConn = triggerBoard.getLinkConn(opticalLink.getTbInput());
                if(linkConn == null) {
                    throw new IllegalArgumentException("TriggerBoard with name " + opticalLink.getTbName() + " has no link for input " + opticalLink.getTbInput());
                }
                     
                if(opticalLink.getOption().equals("enable")) {
                    if(onlyEnabledLBs) {
                        if(linkConn.getBoard().getDisabled() == null) {
                            configurationManager.enableLinkConn(linkConn, true);   
                        }
                    }
                    else {
                        configurationManager.enableLinkConn(linkConn, true);
                    }
                    //System.out.println("enablig " + linkConn);
                }
                else if(opticalLink.getOption().equals("disable")) {
                    configurationManager.enableLinkConn(linkConn, false);
                    //System.out.println("disablig " + linkConn);
                }
                else 
                    throw new IllegalArgumentException("the opt value is not enable or disable");

            }
            
            context.commit();
            
            printLinksToFile(equipmentDAO, configurationDAO);
            
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            context.rollback();
        }
        finally {
            context.closeSession();
        }
    }

    
    /*
     * 
     * List<Crate> lboxes = equipmentDAO.getCratesByType(CrateType.LINKBOX, true);
            
            for(Crate lbox : lboxes) {
                if( ((LinkBox)lbox).getTowerName().contains("YEN") 
                        //|| ((LinkBox)lbox).getTowerName().contains("YEP3")
                        ) {
                    continue;                    
                }
                if(
                    ((LinkBox)lbox).getTowerName().equals("YEP1_near") == false &&  
                    ((LinkBox)lbox).getTowerName().equals("YEP1_far") == false  &&  
                    ((LinkBox)lbox).getTowerName().equals("YEP3_near") == false &&  
                    ((LinkBox)lbox).getTowerName().equals("YEP3_far") == false  &&  
                    //((LinkBox)lbox).getTowerName().equals("RB+1_near") == false  &&
                    ((LinkBox)lbox).getTowerName().equals("RB+1_near") == false
                    //((LinkBox)lbox).getTowerName().equals("RB+2_near") == false  &&
                    //((LinkBox)lbox).getTowerName().equals("RB+2_far") == false 
                    ) {
                    System.out.println("enabling link conn for " + lbox.getName() + " tower " + ((LinkBox)lbox).getTowerName());
                    for(Board lb : lbox.getBoards()) {  
                        if(lb.getType() == BoardType.LINKBOARD) {
                            for(LinkConn linkConn : ((LinkBoard)lb).getLinkConns() ) {
                                configurationManger.enableLinkConn(linkConn, true);
                            }    
                        }
                    }
                }
                else {
                    System.out.println("disabling link conn for " + lbox.getName());
                    for(Board lb : lbox.getBoards()) {  
                        if(lb.getType() == BoardType.LINKBOARD) {
                            if(lb.getDisabled() == null) {
                                for(LinkConn linkConn : ((LinkBoard)lb).getLinkConns() ) {
                                    configurationManger.enableLinkConn(linkConn, false);
                                }       
                            }
                        }
                    }
                }
            }
            
//            LBB_RB-1_S12/LB_RB-1_S12_BN1E_CH0/STDLB_SYNCODER->TC_10/TBn1_10/OPTO 6 optoIn 1 tbIn 13
//            LBB_RB-1_S12/LB_RB-1_S12_BN1E_CH0/STDLB_SYNCODER->TC_11/TBn1_11/OPTO 3 optoIn 1 tbIn 4
//            LBB_RB0_S11/LB_RB0_S11_BP0B_CH0/STDLB_SYNCODER->TC_10/TB0_10/OPTO 2 optoIn 1 tbIn 1
//            LBB_RB0_S11/LB_RB0_S11_BP0B_CH0/STDLB_SYNCODER->TC_9/TB0_9/OPTO 7 optoIn 1 tbIn 16
            
            configurationManger.enableLinkConn(((TriggerBoard)equipmentDAO.getBoardByName("TBn1_10")).getLinkConn(13), false);
            configurationManger.enableLinkConn(((TriggerBoard)equipmentDAO.getBoardByName("TBn1_11")).getLinkConn(4), false);
            configurationManger.enableLinkConn(((TriggerBoard)equipmentDAO.getBoardByName("TB0_10")).getLinkConn(1), false);
            configurationManger.enableLinkConn(((TriggerBoard)equipmentDAO.getBoardByName("TB0_9")).getLinkConn(16), false);
            //configurationManger.enableLinkConn(((TriggerBoard)equipmentDAO.getBoardByName("TBn1_10")).getLinkConn(13), false);

//            LBB_YEP1_S4/LB_RE+1_S4_EP22_CH0/STDLB_SYNCODER->TC_3/TBp3_3/OPTO 3 optoIn 2 tbIn 5
//            LBB_YEP1_S4/LB_RE+1_S4_EP12_CH0/STDLB_SYNCODER->TC_3/TBp3_3/OPTO 2 optoIn 2 tbIn 2            
//            LBB_YEP1_S4/LB_RE+1_S4_EP12_CH0/STDLB_SYNCODER->TC_2/TBp3_2/OPTO 7 optoIn 0 tbIn 15            
//            LBB_YEP1_S4/LB_RE+1_S4_EP13_CH0/STDLB_SYNCODER->TC_2/TBp2_2/OPTO 5 optoIn 1 tbIn 10   
//            LBB_YEP1_S4/LB_RE+1_S4_EP13_CH0/STDLB_SYNCODER->TC_3/TBp2_3/OPTO 4 optoIn 1 tbIn 7  
//            LBB_YEP1_S5/LB_RE+1_S5_EP12_CH0/STDLB_SYNCODER->TC_4/TBp3_4/OPTO 2 optoIn 2 tbIn 2  
//            LBB_YEP1_S5/LB_RE+1_S5_EP12_CH0/STDLB_SYNCODER->TC_3/TBp3_3/OPTO 7 optoIn 0 tbIn 15                       
//            LBB_YEP1_S7/LB_RE+1_S7_EP12_CH0/STDLB_SYNCODER->TC_6/TBp3_6/OPTO 2 optoIn 2 tbIn 2
//            LBB_YEP1_S7/LB_RE+1_S7_EP12_CH0/STDLB_SYNCODER->TC_5/TBp3_5/OPTO 7 optoIn 0 tbIn 15            
//            LBB_YEP1_S7/LB_RE+1_S7_EP13_CH0/STDLB_SYNCODER->TC_6/TBp2_6/OPTO 4 optoIn 1 tbIn 7
//            LBB_YEP1_S7/LB_RE+1_S7_EP13_CH0/STDLB_SYNCODER->TC_5/TBp2_5/OPTO 5 optoIn 1 tbIn 10              
//            LBB_YEP1_S8/LB_RE+1_S8_EP12_CH0/STDLB_SYNCODER->TC_6/TBp3_6/OPTO 7 optoIn 0 tbIn 15
//            LBB_YEP1_S8/LB_RE+1_S8_EP12_CH0/STDLB_SYNCODER->TC_7/TBp3_7/OPTO 2 optoIn 2 tbIn 2
//            LBB_YEP1_S8/LB_RE+1_S8_EP13_CH0/STDLB_SYNCODER->TC_7/TBp2_7/OPTO 4 optoIn 1 tbIn 7   
//            LBB_YEP1_S8/LB_RE+1_S8_EP13_CH0/STDLB_SYNCODER->TC_6/TBp2_6/OPTO 5 optoIn 1 tbIn 10
            
            configurationManger.enableLinkConn(((TriggerBoard)equipmentDAO.getBoardByName("TBp3_3")).getLinkConn(5), false);
            configurationManger.enableLinkConn(((TriggerBoard)equipmentDAO.getBoardByName("TBp3_3")).getLinkConn(2), false);
            configurationManger.enableLinkConn(((TriggerBoard)equipmentDAO.getBoardByName("TBp3_2")).getLinkConn(15), false);
            configurationManger.enableLinkConn(((TriggerBoard)equipmentDAO.getBoardByName("TBp2_2")).getLinkConn(10), false);
            configurationManger.enableLinkConn(((TriggerBoard)equipmentDAO.getBoardByName("TBp2_3")).getLinkConn(7), false);
            configurationManger.enableLinkConn(((TriggerBoard)equipmentDAO.getBoardByName("TBp3_4")).getLinkConn(2), false);
            configurationManger.enableLinkConn(((TriggerBoard)equipmentDAO.getBoardByName("TBp3_3")).getLinkConn(15), false);
            configurationManger.enableLinkConn(((TriggerBoard)equipmentDAO.getBoardByName("TBp3_6")).getLinkConn(2), false);
            configurationManger.enableLinkConn(((TriggerBoard)equipmentDAO.getBoardByName("TBp3_5")).getLinkConn(15), false);
            configurationManger.enableLinkConn(((TriggerBoard)equipmentDAO.getBoardByName("TBp2_6")).getLinkConn(7), false);
            configurationManger.enableLinkConn(((TriggerBoard)equipmentDAO.getBoardByName("TBp2_5")).getLinkConn(10), false);
            configurationManger.enableLinkConn(((TriggerBoard)equipmentDAO.getBoardByName("TBp3_6")).getLinkConn(15), false);
            configurationManger.enableLinkConn(((TriggerBoard)equipmentDAO.getBoardByName("TBp3_7")).getLinkConn(2), false);
            configurationManger.enableLinkConn(((TriggerBoard)equipmentDAO.getBoardByName("TBp2_7")).getLinkConn(7), false);
            configurationManger.enableLinkConn(((TriggerBoard)equipmentDAO.getBoardByName("TBp2_6")).getLinkConn(10), false);
            
            //printLinks(true, equipmentDAO, configurationManger);

     */
}
