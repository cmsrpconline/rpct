package rpct.testAndUtils.boardConfigs;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ChipConfAssignment;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.FebChipConf;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.LocalConfigKey.Subsystem;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.FebBoard;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;

public class PutFEBSettingsToDB {   
    private static PrintStream prntSrm = null;
   

    static void putSettingsFromFile(boolean saveToDb, File inputFile, EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO, 
            LocalConfigKey inputConfigKey, LocalConfigKey outputConfigKey) throws DataAccessException, FileNotFoundException {

        Scanner scanner = new Scanner(inputFile);        
                
        if(saveToDb) {
            System.out.println("Config will be seved to DB"); 
        }
        else {
            System.out.println("Config will NOT be seved to DB, simuation only!!!!!!!!!!!!!!!!!!!!!!!!!!!"); 
        }

       
        System.out.println("getting curent configs from the DB ");

        //List<ChipConfAssignment> chipConfAssignments = configurationDAO.getChipConfAssignments(ChipType.FEBCHIP, inputConfigKey);        
       
        System.out.println("applaing settings from file fileName " + inputFile.getPath() + "/" + inputFile.getName());
        
        //reading the file     
        int i =0;
        while(scanner.hasNextLine()) {
            if(scanner.hasNext() == false) {
                break;
            }
            /*String febName = scanner.next();
            int chipNum  = scanner.nextInt();
            int chipId  = scanner.nextInt();
            
            int vth  = scanner.nextInt();
            int vmon  = scanner.nextInt();
            int vthoffset  = scanner.nextInt();
            int vmonoffset  = scanner.nextInt();*/
            
            int chipId  = scanner.nextInt();
            int vth  = scanner.nextInt();
            String lbName = scanner.next();
            if(scanner.hasNext())            	
            	scanner.nextLine();
            
            Chip febChip = equipmentDAO.getChipById(chipId);
            FebBoard febBoard = (FebBoard)febChip.getBoard();
            String febNameTest = febBoard.getFebLocation().getChamberLocation().getChamberLocationName() + "_" +
            febBoard.getFebLocation().getFebLocalEtaPartition() + ":" +
            febBoard.getCbChannel() + "." + febBoard.getI2cLocalNumber();
            
            //System.out.println(febNameTest + "\t" + febChip.getPosition() + "\t| " + febName + "\t" + chipNum + " " + chipId + " " + vth + " " + vmon + " " + vthoffset + " " + vmonoffset);
            /*febNameTest = febNameTest.replace("Backward", "Bwd");
            febNameTest = febNameTest.replace("Forward", "Fwd");
            febNameTest = febNameTest.replace("Central", "Ctrl");
            if(febNameTest.equals(febName.replace("/0", "/")) == false) {
            	System.out.println("!!!!!!!!!!!!!!");
            }*/
            /*if(febChip.getPosition() != chipNum)
            	throw new RuntimeException("febChip.getPosition() != chipNum for " + febName + " " + chipNum);*/
            
            FebChipConf oldConf = (FebChipConf)configurationManager.getConfiguration(febChip, inputConfigKey);
            FebChipConf newConf = new FebChipConf(oldConf);
            newConf.setVth_mV(vth);
            //newConf.setVmon(vmon);
            //newConf.setVthOffset(vthoffset);
            //newConf.setVmonOffset(vmonoffset);
            
            //System.out.println(febNameTest + "\t" + febChip.getPosition() + "\t| " + febName + "\t" + chipNum + " " + chipId + " " + vth + " " + vmon + " " + vthoffset + " " + vmonoffset + " \n" + newConf);
            System.out.println(febNameTest + "\t" + febChip.getPosition() + "\t| " + chipId + " " + chipId + " " + vth);
            if(saveToDb) {
                configurationDAO.saveObject(newConf); //TODO
                configurationManager.assignConfiguration(febChip, newConf, outputConfigKey.getName()); //TODO                     
            }              
            /*i++;
            if(i == 2000)
            	break;*/
        }        
    }
    
    static void putSettingsFromFile1(boolean saveToDb, List<File> inputFiles, EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO, 
            LocalConfigKey inputConfigKey, LocalConfigKey outputConfigKey) throws DataAccessException, FileNotFoundException {

               
                
        if(saveToDb) {
            System.out.println("Config will be seved to DB"); 
        }
        else {
            System.out.println("Config will NOT be seved to DB, simuation only!!!!!!!!!!!!!!!!!!!!!!!!!!!"); 
        }

       
        System.out.println("getting curent configs from the DB ");

        List<ChipConfAssignment> chipConfAssignments = configurationDAO.getChipConfAssignments(ChipType.FEBCHIP, inputConfigKey);        
        Map<Integer, Integer> chipTresholdMap = new HashMap<Integer, Integer>();
        
        for(File inputFile : inputFiles) { 
        	Scanner scanner = new Scanner(inputFile); 
        	System.out.println("applaing settings from file fileName " + inputFile.getPath() + "/" + inputFile.getName());

        	//reading the file     
        	while(scanner.hasNextLine()) {
        		if(scanner.hasNext() == false) {
        			break;
        		}
        		/*String febName = scanner.next();
            int chipNum  = scanner.nextInt();
            int chipId  = scanner.nextInt();

            int vth  = scanner.nextInt();
            int vmon  = scanner.nextInt();
            int vthoffset  = scanner.nextInt();
            int vmonoffset  = scanner.nextInt();*/

        		int chipId  = scanner.nextInt();
        		int vth  = scanner.nextInt();
        		String lbName = scanner.next();
        		String chamberName = scanner.next(); //<<<<<<<<<<<<<<<<<<<<<<<<,,
        		if(scanner.hasNext())            	
        			scanner.nextLine();

        		chipTresholdMap.put(chipId, vth);
        	}
        }
        
        for(ChipConfAssignment confAssignment : chipConfAssignments) {
            Chip febChip = confAssignment.getChip();
            
            /*FebBoard febBoard = (FebBoard)febChip.getBoard();
            String febNameTest = febBoard.getFebLocation().getChamberLocation().getChamberLocationName() + "_" +
            febBoard.getFebLocation().getFebLocalEtaPartition() + ":" +
            febBoard.getCbChannel() + "." + febBoard.getI2cLocalNumber();*/
            
            
            FebChipConf oldConf = (FebChipConf)confAssignment.getStaticConfiguration();
            FebChipConf newConf = new FebChipConf(oldConf);
            Integer vth =  chipTresholdMap.get(febChip.getId());
            if(vth != null)                       
            	newConf.setVth_mV(vth);
            else {
            	System.out.println("chipId " + febChip.getId() + " " + vth + " not updated (chip disabled?)");
            	continue;
            }
            //newConf.setVmon(vmon);
            //newConf.setVthOffset(vthoffset);
            //newConf.setVmonOffset(vmonoffset);
            
            //System.out.println(febNameTest + "\t" + febChip.getPosition() + "\t| " + febName + "\t" + chipNum + " " + chipId + " " + vth + " " + vmon + " " + vthoffset + " " + vmonoffset + " \n" + newConf);
            //System.out.println(febNameTest + "\t" + febChip.getPosition() + "\t| " + " chipId " + febChip.getId() + " " + vth);
            
            System.out.println("chipId " + febChip.getId() + " " + vth);
            if(saveToDb) {
                configurationDAO.saveObject(newConf); //TODO
                configurationManager.assignConfiguration(febChip, newConf, outputConfigKey); //TODO                     
            }              
            /*i++;
            if(i == 2000)
            	break;*/
        }        
    }
  
    public static void main(String[] args) throws DataAccessException {

        
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        //HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);	 

        try {
        	LocalConfigKey inputConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_FEBS_DEFAULT);
        	//LocalConfigKey outputConfigKey = configurationManager.getLocalConfigKey(configurationManager.LOCAL_CONF_KEY_FEBS_DEFAULT);
        	
        	LocalConfigKey outputConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_FEBS_MIN_THR);
        	
        	List<File> inputFiles =  new ArrayList<File>();
        	
            /*String fileName = "High_rate_chips_with_RollName.txt"; //PrimaryTHR_results_orig.txt
            String fileDir = System.getenv("HOME") + "/bin/data/FEBConfigs";            
            String fileDirName = fileDir + "/" + fileName;
            File inputFile = new File(fileDirName);
            if(inputFile.canRead() ==  false) { //i tak zawsze jakis sie tworzy, nawet jak jest zla sciezka
                System.err.println("the file:\n" + fileDirName + " cannot be opened");
                return;
            }            
            inputFiles.add(inputFile);
            
            fileDirName = fileDir + "/" + "PrimaryTHR_results_orig.txt";
            inputFile =  new File(fileDirName);
            if(inputFile.canRead() ==  false) { //i tak zawsze jakis sie tworzy, nawet jak jest zla sciezka
                System.err.println("the file:\n" + fileDirName + " cannot be opened");
                return;
            }
            inputFiles.add(inputFile);*/
                        
            String fileName = "ChipID_THValue_LBID_ChamberID_12_2012_Corrected.txt"; //PrimaryTHR_results_orig.txt
            String fileDir = System.getenv("HOME") + "/bin/data/FEBConfigs";            
            String fileDirName = fileDir + "/" + fileName;
            File inputFile = new File(fileDirName);
            if(inputFile.canRead() ==  false) { //i tak zawsze jakis sie tworzy, nawet jak jest zla sciezka
                System.err.println("the file:\n" + fileDirName + " cannot be opened");
                return;
            }            
            inputFiles.add(inputFile);
            
            putSettingsFromFile1(true, inputFiles, equipmentDAO, configurationManager, configurationDAO, inputConfigKey, outputConfigKey);
                         
        } 
       /* catch (DataAccessException e) {
            e.printStackTrace();            
            context.rollback();
            System.out.println("context.rollback()");
        } */
        /*  catch (FileNotFoundException e) {            
            e.printStackTrace();
        }*/
        catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getMessage());
            context.rollback();
            System.out.println("context.rollback()");
        } 
        finally {
            context.closeSession();
        }
    }

}
