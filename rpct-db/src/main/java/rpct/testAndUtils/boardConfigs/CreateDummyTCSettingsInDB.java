package rpct.testAndUtils.boardConfigs;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.rpc.ServiceException;

import rpct.datastream.RpctDelays;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ChipConfAssignment;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.PacConf;
import rpct.db.domain.configuration.RmbConf;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.configuration.TbGbSortConf;
import rpct.db.domain.configuration.TcSortConf;
import rpct.db.domain.configuration.LocalConfigKey.Subsystem;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbMuxRecChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;

public class CreateDummyTCSettingsInDB {
    public static void main(String[] args) throws DataAccessException {
        if(args.length == 0) {            
            String[] argsL = {  
            		//"TC_904"
                 /*   "TC_0",                    
                    "TC_1",
                    "TC_2",

                    "TC_3",                    
                    "TC_4",
                    "TC_5",

                    "TC_6",                    
                    "TC_7",
                    "TC_8",

                    "TC_9",                    
                    "TC_10",
                    "TC_11",*/
            		"PASTEURA_TC"
            };

            args = argsL;
        }

        System.out.println("Selected crates");
        for(int i = 0; i < args.length; i++) {
            System.out.print(args[i] + " ");
        }

        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);   

        //RMB configuration
/*        int rmbDataDelay = RpctDelays.RMB_DATA_DELAY;        
        int chanEna = 0; //0 = all channels enable
        int preTriggerVal = RpctDelays.PRE_TRIGGER_VAL; //zakers BX branych do DAQ
        int postTriggerVal = RpctDelays.POST_TRIGGER_VAL; 
        int trgDelay = 0; */
        
        int pacConfigId = 0x1036; //TODO
        //int pacConfigId = 0x0000;        

        try {
        	//LocalConfigKey localConfigKey = configurationManager.createLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_TCS_V1036, Subsystem.TCS);
        	LocalConfigKey localConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_TCS_V1036);
            List<TriggerCrate> crates = new ArrayList<TriggerCrate>();
            for(int i = 0; i < args.length; i++) {                
                crates.add((TriggerCrate)equipmentDAO.getCrateByName(args[i]));
            }

            PacConf pacConf = new PacConf();                             
            pacConf.setPacConfigId(pacConfigId);
            pacConf.setBxOfCoincidence(0);
            pacConf.setPacEna(0xfff);
            configurationDAO.saveObject(pacConf);
                 
            RmbConf rmbConf = new RmbConf();
            rmbConf.setDataDelay(RpctDelays.RMB_DATA_DELAY);
            rmbConf.setChanEna(0); //0 = all channels enable
            rmbConf.setPreTriggerVal( RpctDelays.PRE_TRIGGER_VAL);
            rmbConf.setPostTriggerVal(RpctDelays.POST_TRIGGER_VAL);
            rmbConf.setTrgDelay(0);
            configurationDAO.saveObject(rmbConf);
            
            TbGbSortConf gbSortConf = new TbGbSortConf();
            configurationDAO.saveObject(gbSortConf);
            
            TcSortConf tcSortConf = new TcSortConf();
            tcSortConf.setBootScript("boot_lhc_v" + Integer.toHexString(pacConfigId) + ".sh"); //TODO
            configurationDAO.saveObject(tcSortConf);
            
            for(TriggerCrate tc : crates) {   
                for(TriggerBoard tb : tc.getTriggerBoards()) {
                    for(Chip chip : tb.getChips()) {
                        StaticConfiguration  conf = null;
                        if(chip.getType() == ChipType.PAC) { //&& chip.getDisabled() == null
                        	conf = pacConf;               
                        }
                        else if(chip.getType() == ChipType.RMB) { //&& chip.getDisabled() == null
                        	conf = rmbConf;               
                        }  
                        else if(chip.getType() == ChipType.GBSORT) { //&& chip.getDisabled() == null
                        	conf = gbSortConf;               
                        } 
                        
                        if(conf != null) {
                        	configurationManager.assignConfiguration(chip, conf, localConfigKey);//TODO
                        	System.out.println("adding dummy configuration for " + chip);
                        }
                        else {
                        	System.out.println("no configs assignex for " + chip);
                        }
                        	
                    }  
                }
                
                Chip chip = tc.getBackplane().getTcSorter();                
                if(chip.getType() == ChipType.TCSORT && chip.getDisabled() == null) {
                    configurationManager.assignConfiguration(chip, tcSortConf, localConfigKey);//TODO
                    System.out.println("adding dummy configuration for " + chip);                            
                }  
            }
        }              
        catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            context.rollback();
        }
        finally {
            context.closeSession();
        }
    }

}
