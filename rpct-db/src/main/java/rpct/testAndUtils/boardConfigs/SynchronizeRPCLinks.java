package rpct.testAndUtils.boardConfigs;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import rpct.datastream.TestOptionsXml;
import rpct.datastream.TestOptionsXml.BoardSource;
import rpct.datastream.TestOptionsXml.OpticalLink;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.equipment.RbcBoard;
import rpct.db.domain.equipment.RbcTtuConn;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbOpto;
import rpct.xdaqaccess.hardwaredb.HardwareDbSynCoder;

public class SynchronizeRPCLinks {
	/**
	 * @param args
	 * @throws DataAccessException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws DataAccessException, IOException {
		HibernateContext context = new SimpleHibernateContextImpl();
		EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
		HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);

		boolean onlyEnabledLBs = true;
		if(args.length > 0) {
			if(args[0].equals("-onlyEnabledLBs=false"))
				onlyEnabledLBs = false;
		}
		//printLinks(false, equipmentDAO, configurationManger);   

		Map<Integer, LinkBoard> lbMap = new TreeMap<Integer, LinkBoard>();

		try { 
			//String fileDir = System.getenv("HOME") + "/bin/config/enableLinks.xml";
			String fileDir = System.getenv("HOME") + "/bin/config/synchronizeRPCLinks.xml";
			System.out.println("synchronizig links defined in the file "+ fileDir); 

			//TestOptionsXml testOptionsXml = TestOptionsXml.readFromFile(System.getenv("HOME") + "/bin/config/enableLinks.xml");
			TestOptionsXml testOptionsXml = TestOptionsXml.readFromFile(fileDir);
			TestOptionsXml.SourcesList sourcesList = testOptionsXml.getSourcesList("linkConnectionTest");                      

			List<BoardSource> boardSources = sourcesList.getBoardSources();
			for(BoardSource boardSource : boardSources) {
				Set<Board> boards = testOptionsXml.getBoards(boardSource, equipmentDAO, onlyEnabledLBs);
				/*
				 * jesli ja,is LB jest diasbled, to jesli onlyEnabledLBs = true nie pojawi sie w boards. oznacza to, ze nie bedzie go mozna
				 * ani zdisablowac, ani zenablowac
				 */
				for(Board board : boards) {
					if(board.getType() == BoardType.LINKBOARD) {
						lbMap.put(board.getId(), (LinkBoard)board);                		                	
					}                	
					else {
						throw new IllegalArgumentException("the board is not LinkBoard");
					}
				}
			}

			
			String r_s = "y";
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			while(true) {
				System.out.println("s - synchronize links");
				System.out.println("n - normal link operation");
				System.out.println("i - interconnection test");
				System.out.println("q - quit");
				r_s = br.readLine();
				
				if (r_s.equals("s") == true) {
					for (Map.Entry<Integer, LinkBoard> lbIt : lbMap.entrySet()) {            	
						HardwareDbSynCoder syncoder = (HardwareDbSynCoder)dbMapper.getChip(lbIt.getValue().getSynCoder());
						syncoder.synchronizeLink();
					}
				}
				
				else if (r_s.equals("n") == true) {
					for (Map.Entry<Integer, LinkBoard> lbIt : lbMap.entrySet()) {            	
						HardwareDbSynCoder syncoder = (HardwareDbSynCoder)dbMapper.getChip(lbIt.getValue().getSynCoder());
						syncoder.normalLinkOperation();
						syncoder.enableTransmissionCheck(true, true);
						syncoder.enableTest(0, 0);
					}
				}
				
				else if (r_s.equals("i") == true) {
					for (Map.Entry<Integer, LinkBoard> lbIt : lbMap.entrySet()) {            	
						HardwareDbSynCoder syncoder = (HardwareDbSynCoder)dbMapper.getChip(lbIt.getValue().getSynCoder());
						syncoder.normalLinkOperation();
						syncoder.enableTransmissionCheck(false, false);
						syncoder.enableTest(1, 0);
						syncoder.getHardwareChip().getItem("SEND_TEST_DATA").write(lbIt.getKey());
					}
				}
				else if (r_s.equals("j") == true) {
					System.out.println("TEST_DATA (hex)?");
					Scanner in = new Scanner(System.in);
					int num = in.nextInt(16);
					for (Map.Entry<Integer, LinkBoard> lbIt : lbMap.entrySet()) {            	
						HardwareDbSynCoder syncoder = (HardwareDbSynCoder)dbMapper.getChip(lbIt.getValue().getSynCoder());
						syncoder.normalLinkOperation();
						syncoder.enableTransmissionCheck(true, true);
						syncoder.enableTest(1, 0);
						
						syncoder.getHardwareChip().getItem("SEND_TEST_DATA").write(num);
					}
				}
				
				else if (r_s.equals("q") == true) {
					for (Map.Entry<Integer, LinkBoard> lbIt : lbMap.entrySet()) {            	
						HardwareDbSynCoder syncoder = (HardwareDbSynCoder)dbMapper.getChip(lbIt.getValue().getSynCoder());
						syncoder.normalLinkOperation();
						syncoder.enableTransmissionCheck(true, true);
						syncoder.enableTest(0, 0);
					}
					break;
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			context.rollback();
		}
		finally {
			context.closeSession();
		}
	}
}
