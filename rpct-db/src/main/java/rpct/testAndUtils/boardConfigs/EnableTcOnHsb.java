package rpct.testAndUtils.boardConfigs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Set;

import org.apache.log4j.PropertyConfigurator;

import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.HalfSortConf;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;

public class EnableTcOnHsb {

    /**
     * @param args
     * @throws DataAccessException 
     * @throws IOException 
     */
    public static void main(String[] args) throws DataAccessException, IOException {
        PropertyConfigurator.configure("log4j.properties");
        HibernateContext context = new SimpleHibernateContextImpl();
        try {
            EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
            //HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
            ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
            ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);	 

            if (args.length < 1 || args.length > 3) {
                System.err.println("Syntax: HSB_0|HSB_1 [hex_value]");
                return;
            }

            String hsbName = args[0];

            Board hsb = equipmentDAO.getBoardByName(hsbName);
            Set<Chip> chips = hsb.getChips();
            if (chips.size() != 1) {
                throw new RuntimeException("invalid number of chips");
            }
            Chip hsbChip = chips.iterator().next();
            HalfSortConf currentConf = (HalfSortConf) configurationManager.getConfiguration(hsbChip, configurationManager.getDefaultLocalConfigKey());
            System.out.println("Current config " + hsb.getName() + " recChanEna " 
                    + Integer.toHexString(currentConf.getRecChanEna()[1] & 0xff)
                    + Integer.toHexString(currentConf.getRecChanEna()[0] & 0xff));
            
            boolean readOnly = args.length == 1;
            if (readOnly) {
                return;
            }
            
            int recChanEnaMask = Integer.parseInt(args[1], 16);
            byte[] recChanEna = new byte[2];
            recChanEna[0] = (byte)(0xff & recChanEnaMask); 
            recChanEna[1] = (byte)( (0xff00 & recChanEnaMask) >> 8); //to jest chyba nie dobrze, KB
            
            HalfSortConf newConf = new HalfSortConf(currentConf);
            newConf.setId(0);
            newConf.setRecChanEna(recChanEna);
            configurationDAO.saveObject(newConf);
            configurationManager.assignConfiguration(hsbChip, newConf, ConfigurationManager.LOCAL_CONF_KEY_SC_FULL);
            configurationManager.assignConfiguration(hsbChip, newConf, ConfigurationManager.LOCAL_CONF_KEY_SC_BOTTOM);
            System.out.println("New config " + hsb.getName() + " recChanEna " 
                    + Integer.toHexString(newConf.getRecChanEna()[1] & 0xff)
                    + Integer.toHexString(newConf.getRecChanEna()[0] & 0xff));
               	
            context.commit();
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			context.rollback();
		}
		finally {
			context.closeSession();
		}
    }

}
