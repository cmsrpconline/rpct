package rpct.testAndUtils.boardConfigs;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.log4j.Logger;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.IntArray12;
import rpct.db.domain.configuration.IntArray16;
import rpct.db.domain.configuration.IntArray24;
import rpct.db.domain.configuration.IntArray4;
import rpct.db.domain.configuration.IntArray6;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.configuration.TtuTrigConf;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.db.service.ConfigurationSet;
import rpct.xdaq.axis.bag.BagDeserializerFactory;
import rpct.xdaq.axis.bag.BagSerializerFactory;
import rpct.xdaq.axis.bag.HashBag;

/**
 * Created on 2009-12-07
 * 
 * @author Nikolay Darmenov
 * @version $Id$
 */
public class ConfUtil {
	public static final Logger logger = Logger.getLogger(ConfUtil.class);

	/**
	 * Bundles DB root accessors
	 */
	public static class DbHandler {
		public static final HibernateContext context = new SimpleHibernateContextImpl();
		public static final EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(
				context);

		public static final ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(
				context);
		public static final ConfigurationManager configurationManager = new ConfigurationManager(
				context, equipmentDAO, configurationDAO);
		private static LocalConfigKey inputConfigKey;
		private static LocalConfigKey outputConfigKey;
		static {
			try {
				inputConfigKey = configurationManager.getDefaultLocalConfigKey();
				outputConfigKey = configurationManager
						.getDefaultLocalConfigKey();
			} catch (DataAccessException e) {
				throw new ExceptionInInitializerError(e);
			}
		}
		public static LocalConfigKey getInputConfigKey() {
			return inputConfigKey;
		}
		public static void setInputConfigKey(String inputLck) throws DataAccessException {
			inputConfigKey = configurationDAO.getLocalConfigKeyByName(inputLck);
		}
		public static LocalConfigKey getOutputConfigKey() {
			return outputConfigKey;
		}
		public static void setOutputConfigKey(String outputLck) throws DataAccessException {
			outputConfigKey = configurationDAO.getLocalConfigKeyByName(outputLck);
		}
	}
	
	public static void rollbackDb() throws DataAccessException {
		try {
			System.err.println("[ERROR] context to be rollbacked");
			ConfUtil.DbHandler.context.rollback();
		} finally {
			ConfUtil.DbHandler.context.closeSession();
			logger.info("DB session closed.");
		}
	}

	/*
	 * Facade to main DB access 
	 */
	public static List<Board> getBoards(BoardType type, boolean enabledOnly) throws DataAccessException {
		return DbHandler.equipmentDAO.getBoardsByType(type, enabledOnly);
	}
	public static List<Chip> getChips(ChipType type, boolean enabledOnly) throws DataAccessException {
		return DbHandler.equipmentDAO.getChipsByType(type, enabledOnly);
	}
	/**
	 * Print chip info (static & configuration)
	 * @param chip
	 * @throws DataAccessException
	 */
	public static void print(final Chip chip) throws DataAccessException {
		System.out.println("===> id #" + chip.getId() + ": " + chip);

		final StaticConfiguration configuration = getChipConf(chip);
		try {
			System.out.format("   Configuration: id#%d %s%n", configuration
					.getId(), configuration);

		} catch (NullPointerException e) {
			logger.warn("   No configuration found for chipId #" + chip.getId());
		}
	}

	public static StaticConfiguration getChipConf(Chip chip)
			throws DataAccessException, NullPointerException {
		/// ConfigurationManager().getConfiguration(Chip, LocalConfigKey) throws NullPointerException
		/// if no configuration is found
		final StaticConfiguration configuration = DbHandler.configurationManager
				.getConfiguration(chip, DbHandler.inputConfigKey);
		return configuration;
	}

	
	public static void doConf(List<Chip> chips, boolean saveToDb)
			throws DataAccessException {
		for (Chip chip : chips) {
			print(chip);

			if (saveToDb) {
				final StaticConfiguration newConf = generateDummyConf(chip);

				System.out.format("   New Configuration: id#%d %s%n", newConf
						.getId(), newConf);

				DbHandler.configurationDAO.saveObject(newConf);
				DbHandler.configurationManager.assignConfiguration(chip,
						newConf, DbHandler.outputConfigKey.getName());
				System.out.println("      === To be saved to DB ===");
				System.out.println();
			}
		}
	}

	public static StaticConfiguration generateDummyConf(Chip chip)
			throws DataAccessException {
		StaticConfiguration newConf = null;

		// Deduce and instantiate newConf
		try {
			final Map<ChipType, Class<?>> chipConf = new HashMap<ChipType, Class<?>>() {
				private static final long serialVersionUID = 1L;

				{
					put(
							ChipType.TTUTRIG,
							Class
									.forName("rpct.db.domain.configuration.TtuTrigConf"));
					put(
							ChipType.TTUFINAL,
							Class
									.forName("rpct.db.domain.configuration.TtuFinalConf"));
					put(
							ChipType.FEBCHIP,
							Class
									.forName("rpct.db.domain.configuration.FebChipConf"));
				}
			};

			newConf = (StaticConfiguration) chipConf.get(chip.getType())
					.newInstance();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		// Set newConf fields with dummy values
		for (Method method : newConf.getClass().getDeclaredMethods()) {
			printDebug("Got method: " + method);
			if (method.getName().startsWith("set")) {
				printDebug("Got setter: " + method);
				boolean isTransient = false;
				for (Annotation annotation : method.getDeclaredAnnotations()) {
					if (annotation.toString().endsWith(".Transient")) {
						isTransient = true;
						break;
					}
				}
				if (!isTransient) {
					printDebug("The method is not @Transient");
					final Class<?> argClass = method.getParameterTypes()[0];
					printDebug("Arg class: " + argClass + ", simpleName "
							+ argClass.getSimpleName() + ", canonicalName "
							+ argClass.getCanonicalName());

					Object arg = null;
					final String argClassName = argClass.getSimpleName();

					String argType = argClassName;
					if (argClassName.equals("byte[]")) {
						/* need to check on method name if byte[].length varies */
						argType = "byte[" + TtuTrigConf.REC_MUX_SIZE + "]"; 
					}
					arg = DummyGenerator.generate(argType);

					printDebug("dummy generated arg: " + arg);

					try {
						method.invoke(newConf, arg);
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
				}
			}
		}

		return newConf;
	}

	/**
	 * Dummy values generation as "SimpleFactory" on generate()
	 */
	public static class DummyGenerator {
		private static int dummyVal = (int) Calendar.getInstance()
				.getTimeInMillis() / 1000;

		private static int intVal() {
			return dummyVal++;
		}

		private static boolean booleanVal() {
			return (dummyVal++ % 2 == 0) ? false : true;
		}

		private static byte[] bytes(int size) {
			final byte[] result = new byte[size];
			for (int i = 0; i < size; i++)
				result[i] = (byte) intVal();

			return result;
		}

		private static int[] integers(int size) {
			final int[] result = new int[size];
			for (int i = 0; i < size; i++)
				result[i] = intVal();

			return result;
		}

		// It would be nice IntArrayXX to have a common interface
		// in order to reduce the dummyIntArrayXX to one
		// "IntArray dummyIntArray(String)"
		private static IntArray4 intArray4() {
			return new IntArray4(integers(4));
		}

		private static IntArray6 intArray6() {
			return new IntArray6(integers(6));
		}

		private static IntArray12 intArray12() {
			return new IntArray12(integers(12));
		}

		private static IntArray16 intArray16() {
			return new IntArray16(integers(16));
		}

		private static IntArray24 intArray24() {
			return new IntArray24(integers(24));
		}

		public static Object generate(String type) {
			Object result = null;

			if (type.equals("int"))
				result = DummyGenerator.intVal();
			else if (type.equals("boolean"))
				result = DummyGenerator.booleanVal();
			else if (type.startsWith("byte[")) {
				// "byte[dim]" is sent, not null "dim" expected
				final String dimString = type.substring(new String("byte[")
						.length(), type.length() - 1);
				final int dim = Integer.parseInt(dimString);
				result = DummyGenerator.bytes(dim);
			} else if (type.equals("IntArray4"))
				result = DummyGenerator.intArray4();
			else if (type.equals("IntArray6"))
				result = DummyGenerator.intArray6();
			else if (type.equals("IntArray12"))
				result = DummyGenerator.intArray12();
			else if (type.equals("IntArray16"))
				result = DummyGenerator.intArray16();
			else if (type.equals("IntArray24"))
				result = DummyGenerator.intArray24();
			else
				throw new IllegalArgumentException("Type '" + type
						+ "' not known to DummyGenerator!");

			return result;
		}
	}

	/**
	 *  I/O 
	 */
	public static String getProperty(String name) {
		return System.getProperty(name);
	}

	public static boolean getBooleanProperty(String propertyName) {
		final String prop = System.getProperty(propertyName);

		return (prop != null) ? Boolean.valueOf(prop) : false;
	}

	public static List<Integer> getIntListProperty(String propertyName) throws IllegalArgumentException {
		try {
			final String prop = System.getProperty(propertyName).trim();
			final String delims = "[^\\d]+";
		
			final List<Integer> intList = new ArrayList<Integer>(); 
			for (String e: prop.split(delims)) {
				intList.add(Integer.valueOf(e));
			}
		
			return intList;
		}
		catch (Exception e) {
			throw new IllegalArgumentException(e); 
		}
	}


	public static class RegexFormatter {
		private static final RegexFormatter INSTANCE = new RegexFormatter();

		private static String filter(String str, String capture, Method filter) {
			Pattern p = Pattern.compile("(" + capture + ")");
		    Matcher m = p.matcher(str);
		    StringBuffer s = new StringBuffer();
		    while (m.find()) {
		    	String matched = m.group(1);
		    	String result = matched;
				try {
					result = (String) filter.invoke(INSTANCE, matched);
				} catch (Exception e) {
					e.printStackTrace();
					// swallowed
				}
		    	m.appendReplacement(s, result);
		    }
		    
			return s.toString();
		}
		
		@SuppressWarnings("unused")
		private static String toBin(String str) {
			return "B" + Long.toBinaryString(Long.parseLong(str));
		}
		/**
		 * Convert all decimals to bin.
		 * @param str
		 * @return converted str
		 */
		public static String binFormat(String str) {
			try {
				return filter(str, "\\d+", RegexFormatter.class.getDeclaredMethod("toBin", String.class));
			} catch (Exception e) {
				e.printStackTrace();
				return str; // fall back
			}
		}

		@SuppressWarnings("unused")
		private static String toHex(String str) {
			return "0x" + Long.toHexString(Long.parseLong(str)).toUpperCase();
		}
		/**
		 * Convert all decimals to hex.
		 * @param str
		 * @return converted str
		 */
		public static String hexFormat(String str) {
			try {
				return filter(str, "\\d+", RegexFormatter.class.getDeclaredMethod("toHex", String.class));
			} catch (Exception e) {
				e.printStackTrace();
				return str; // fall back
			}
		}
	}
	

	public static boolean proceed(String message) {
		Scanner in = new Scanner(System.in);

		System.out.print(message + " (yes/NO): ");
		String answer = in.nextLine();

		return answer.toLowerCase().equals("yes");
	}

	private static void printDebug(String message) {
		logger.debug(message);
	}
	
	/**
	 * Web Services
	 */
	public static class WebServiceHandler {
		public static final String DB_SERVICE_URI = "urn:rpct-dbservice";
	}
	
	@SuppressWarnings("unchecked")
	public static void wsGetConfigurationSet(String endpoint, List<Integer> chipIds, String globalConfigKey) throws ServiceException, RemoteException, MalformedURLException {
		/* "http://localhost:8080/rpct-dbservice/services/rpctdbservice" is a typical endpoint */
		if (endpoint == null) {
			logger.error("No service url specified!");
			System.exit(1);
		}
		logger.info("Will connect to '" + endpoint + "'");
			
		Service service = new Service();
		Call call = (Call) service.createCall();
		call.setTargetEndpointAddress(new java.net.URL(endpoint) );
		
		final String operationName = "getConfigurationSet";
		call.setOperationName(new QName(DB_SERVICE_URI, operationName));
		
		final List<BigInteger> wsChipIds = new ArrayList<BigInteger>();
		for (int id: chipIds) {
			wsChipIds.add(BigInteger.valueOf(id));
		}
		
		final Object[] methArgs = new Object[] {wsChipIds, globalConfigKey};

		call.registerTypeMapping(ConfigurationSet.class, new QName("http://schemas.xmlsoap.org/soap/encoding/", "Struct"),
				BagSerializerFactory.class, BagDeserializerFactory.class);
		
		
		logger.info("Calling '" + operationName + "' for chips " + chipIds + " and globalConfigKey '" + globalConfigKey + "'");
		HashBag<Object> ret = (HashBag<Object>) call.invoke(methArgs);
		logger.debug("Sent '" + Arrays.toString(methArgs) + "', got '" + ret.keySet() + "' -> '" + ret.values());
		
		for (String configName: ret.keySet()) {
			logger.debug("configName: " + configName);
			final Object result = ret.get(configName);
			
			if (result instanceof List) {
				List<HashBag<Object>> configurationsList = (List<HashBag<Object>>) result;
				logger.debug(configName + ": " + configurationsList);
				for (HashBag<Object> configuration: configurationsList) {
					List<String> presentation = new ArrayList<String>();
					for (Entry<String, Object> entry: configuration.entrySet()) {
						presentation.add(entry.getKey() + " -> " + 
								((entry.getValue().getClass().isArray()) ? Arrays.deepToString(Collections.singletonList(entry.getValue()).toArray()) : entry.getValue()));
					}
					System.out.println("configuration." + configName + ": " + presentation); 
				}
			}
		}
	}

	private static final String DB_SERVICE_URI = "urn:rpct-dbservice";
}
