package rpct.testAndUtils.boardConfigs;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.TreeSet;

import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ChipConfAssignment;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.SynCoderConf;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;

public class LBMasksDumpLast {
	private static PrintStream prntSrm;

	static void dumnpStripMasksFromDB(EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) throws DataAccessException, IOException {        
		LocalConfigKey configKey1 = configurationManager.getCurrnetLBsConfigKey();
		Date lastDate = new Date();
		List<Board> lbs = equipmentDAO.getBoardsByType(BoardType.LINKBOARD, true);
		int k =0;
		for(Board board : lbs) {
			LinkBoard lb = (LinkBoard)board;
			List<ChipConfAssignment> confAssignmentList = configurationDAO.getChipConfAssignmentHistory(lb.getSynCoder(), configKey1);
			java.util.ListIterator<ChipConfAssignment> iteraror = confAssignmentList.listIterator(confAssignmentList.size());

			while(iteraror.hasPrevious()) {
				ChipConfAssignment confAssignment1 = iteraror.previous(); 
				Calendar cal = new GregorianCalendar();
				cal.setTime(confAssignment1.getCreationDate());
				cal.set(Calendar.SECOND, 0);
				cal.set(Calendar.MILLISECOND, 0);
				if(cal.get(Calendar.MINUTE)%2 == 1) {
					cal.add(Calendar.MINUTE, -1);
				}                
				lastDate=cal.getTime();
				System.out.println("+++ Found date: "+lastDate);
			}            
			k++;
		}

		Calendar cal = new GregorianCalendar();
		cal.setTime(lastDate);
		cal.add(Calendar.MINUTE, 2);
		lastDate = cal.getTime();
		System.out.println("+++ Final date: "+lastDate);

		k =0;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yy_MM_dd__HH_mm");
		String outfileName = System.getenv("HOME") + "/bin/out/StripMasks/LBMaskDumpDB_" + dateFormat.format(lastDate) + ".txt";
		FileOutputStream file = new FileOutputStream(outfileName, false);
		prntSrm = new PrintStream(file);

		prntSrm.println("mask dump from DB");
		prntSrm.println("configuration state on " + lastDate);
		prntSrm.println("\nmasksDump");        

		for(Board board : lbs) {
			LinkBoard lb = (LinkBoard)board;
			List<ChipConfAssignment> confAssignmentList = configurationDAO.getChipConfAssignmentHistory(lb.getSynCoder(), configKey1);
			java.util.ListIterator<ChipConfAssignment> iteraror = confAssignmentList.listIterator(confAssignmentList.size());

			Date selConfigDate = null;
			String str = null;
			while(iteraror.hasPrevious()) {
				ChipConfAssignment confAssignment1 = iteraror.previous(); 
				if(selConfigDate == null) {
					if(confAssignment1.getCreationDate().before(lastDate)) {
						selConfigDate = confAssignment1.getCreationDate();
						SynCoderConf coderConf = (SynCoderConf) confAssignment1.getStaticConfiguration();
						//lb.getName() + " " + 

                        String mask =  new Binary(coderConf.getInChannelsEna()).toString();
						if(!mask.equals("ffffffffffffffffffffffff")) {
							str = lb.getChamberNames().first() + " " + new Binary(coderConf.getInChannelsEna()).toString(); 
						}              
					}
				}
			}
			if(str != null)
				prntSrm.println(str);

			k++;
			//if (k > 10) break;
		}
	}


	public static void main(String[] args) throws DataAccessException {
		HibernateContext context = new SimpleHibernateContextImpl();
		EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
		HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
		ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
		ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO); 

		try {
			Date date = new Date();
			dumnpStripMasksFromDB(equipmentDAO, configurationManager, configurationDAO);            
		}
		catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			context.closeSession();
		}
	}
}
