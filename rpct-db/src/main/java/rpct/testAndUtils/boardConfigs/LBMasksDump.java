package rpct.testAndUtils.boardConfigs;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.TreeSet;

import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ChipConfAssignment;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.SynCoderConf;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;

public class LBMasksDump {
    private static PrintStream prntSrm;
    
    static void dumnpStripMasksFromDB(EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) throws DataAccessException, IOException {        
        LocalConfigKey configKey1 = configurationManager.getLocalConfigKey(ConfigurationManager.CURRENT_LBS_CONF_KEY);       
        TreeSet<Date> dates = new TreeSet<Date>();        
        
        List<Board> lbs = equipmentDAO.getBoardsByType(BoardType.LINKBOARD, true);
        int k =0;
        for(Board board : lbs) {
            LinkBoard lb = (LinkBoard)board;
            List<ChipConfAssignment> confAssignmentList = configurationDAO.getChipConfAssignmentHistory(lb.getSynCoder(), configKey1);
            java.util.ListIterator<ChipConfAssignment> iteraror = confAssignmentList.listIterator(confAssignmentList.size());
                               
            while(iteraror.hasPrevious()) {
                ChipConfAssignment confAssignment1 = iteraror.previous(); 
/*                SynCoderConf coderConf1 = (SynCoderConf) confAssignment1.getStaticConfiguration();;
                
                prntSrm.println(confAssignment1.getCreationDate());*/               
                
                Calendar cal = new GregorianCalendar();
                cal.setTime(confAssignment1.getCreationDate());
                //cal.set(Calendar.HOUR_OF_DAY, 0);
                //cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
                if(cal.get(Calendar.MINUTE)%2 == 1) {
                    cal.add(Calendar.MINUTE, -1);
                }
                
                dates.add(cal.getTime());
            }
            
            k++;
            //if (k > 10) break;
        }
               
        ArrayList<Date> dateArray = new ArrayList<Date>(dates);
        int i = 0;
        for(Date date: dateArray) {
            System.out.println(i + ") " + date);
            i++;
        }
        
        System.out.println("choose the number of the date ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line = br.readLine();
        Integer sel = new Integer(line); 
        Date selDate = dateArray.get(sel);
        
        Calendar cal = new GregorianCalendar();
        cal.setTime(selDate);
        cal.add(Calendar.MINUTE, 2);
        selDate = cal.getTime();
        System.out.println("chosen date is " + selDate);
        
        k =0;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yy_MM_dd__HH_mm");
        String outfileName = System.getenv("HOME") + "/bin/out/StripMasks/LBMaskDumpDB_" + dateFormat.format(selDate) + ".txt";
        FileOutputStream file = new FileOutputStream(outfileName, false);
        prntSrm = new PrintStream(file);
        
        prntSrm.println("mask dump from DB");
        prntSrm.println("configuration state on " + selDate);
        prntSrm.println("\nmasksDump");        
        
        for(Board board : lbs) {
            LinkBoard lb = (LinkBoard)board;
            List<ChipConfAssignment> confAssignmentList = configurationDAO.getChipConfAssignmentHistory(lb.getSynCoder(), configKey1);
            java.util.ListIterator<ChipConfAssignment> iteraror = confAssignmentList.listIterator(confAssignmentList.size());
                
            System.out.println("\n" + lb.getName());
            Date selConfigDate = null;
            String str = null;
            while(iteraror.hasPrevious()) {
                ChipConfAssignment confAssignment1 = iteraror.previous(); 
                System.out.print(confAssignment1.getCreationDate());               
                if(selConfigDate == null) {
                    if(confAssignment1.getCreationDate().before(selDate)) {
                        selConfigDate = confAssignment1.getCreationDate();
                        System.out.print(" <--");
                        SynCoderConf coderConf = (SynCoderConf) confAssignment1.getStaticConfiguration();
                        //lb.getName() + " " + 
                        
                        String mask =  new Binary(coderConf.getInChannelsEna()).toString();
                        if(!mask.equals("ffffffffffffffffffffffff")) {
                            str = lb.getChamberNames().first() + " " + new Binary(coderConf.getInChannelsEna()).toString(); 
                        }              
                    }
                }
                System.out.print("\n");                
            }
            System.out.println("date of the selcted config is \n" + selConfigDate);
            if(str != null)
                prntSrm.println(str);
            
            k++;
            //if (k > 10) break;
        }
        System.out.println("the dump is in file " + outfileName);
    }
    
    public static void dumpMasksFromHardware(HashSet<String> selTowers, EquipmentDAO equipmentDAO, HardwareDbMapper dbMapper) throws DataAccessException, RemoteException, ServiceException {
        prntSrm.println("mask dump from hardware");
        Date date = new Date();
        prntSrm.println("dump time " + date);
        prntSrm.println("slected towers");
        for(String towerName : selTowers) {
            prntSrm.println(towerName); 
        }
        prntSrm.println("\nmasksDump");
        
        List<Board> lbs = equipmentDAO.getBoardsByType(BoardType.LINKBOARD, true);
        for(Board board : lbs) {
            LinkBoard lb = (LinkBoard)board;
            if(selTowers.contains(lb.getLinkBox().getTowerName()) ) {
                HardwareDbChip syncoder = dbMapper.getChip(lb.getSynCoder());
                try {
                    String mask =  syncoder.getHardwareChip().getItem("CHAN_ENA").readBinary().toString();
                    if(!mask.equals("ffffffffffffffffffffffff")) {
                        prntSrm.println(lb.getChamberNames().first() + " " + mask);
                    }
                }
                catch (RemoteException e) {
                    System.err.println("Exception during reading the " + lb.getName() + e.getMessage());
                    System.err.println("traing to continue");
                }
                catch (ServiceException e) {
                    System.err.println("Exception during reading the " + lb.getName() + e.getMessage());
                    System.err.println("traing to continue");
                }
            }
        }       
    }
    
    public static void main(String[] args) throws DataAccessException {
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO); 
        
        if(args.length == 0) {            
            String[] argsL =  {                
                    //"PASTEURA_LBB"
                    "YEP1_near",
                    "YEP1_far",
                    "YEP3_near",
                    "YEP3_far",
                    "YEN1_near",
                    "YEN1_far",
                    "YEN3_near",
                    "YEN3_far",
                    
                    "RB-2_near",
                    "RB-2_far",
                    "RB-1_near",
                    "RB-1_far",
                    "RB0_near",
                    "RB0_far",
                    "RB+1_near",
                    "RB+1_far",
                    "RB+2_near",
                    "RB+2_far",
            };

            args = argsL;
        }

                
             
        try {
            Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yy_MM_dd__HH_mm");            
            //prntSrm = System.out;
                        
            System.out.println("dump mask from hardware (h) or from DB (d)?");
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String opt = br.readLine();
            if(opt.equals("h")) {
                HashSet<String> selTowers = new HashSet<String>();
                System.out.println("Selected XDAQs");
                for(int i = 0; i < args.length; i++) {
                    System.out.println(args[i] + " ");
                    selTowers.add(args[i]);
                }  
                
                String outfileName = System.getenv("HOME") + "/bin/out/StripMasks/LBMaskDumpHardware_" + dateFormat.format(date) + ".txt";                
                FileOutputStream file = new FileOutputStream(outfileName, false);
                prntSrm = new PrintStream(file);
                dumpMasksFromHardware(selTowers, equipmentDAO, dbMapper);
                System.out.println("the dump is in file " + outfileName);
            }
            else if(opt.equals("d")) {                 
                dumnpStripMasksFromDB(equipmentDAO, configurationManager, configurationDAO);
            }
            
            System.out.println("End");
        }
        catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            context.closeSession();
        }
    }
}
