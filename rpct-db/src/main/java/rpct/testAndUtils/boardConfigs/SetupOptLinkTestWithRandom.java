package rpct.testAndUtils.boardConfigs;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

//import javax.xml.rpc.ServiceException;

import rpct.datastream.TestPresets;
import rpct.datastream.TestPulsesGenerator;
import rpct.datastream.TestsManager;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.SynCoderConf;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.diag.Pulser;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbCrate;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbOpto;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;
import rpct.xdaqaccess.synchro.DefOut;
import rpct.xdaqaccess.synchro.LVDSLink;
import rpct.xdaqaccess.synchro.LVDSLinksSynchronizer;
import rpct.xdaqaccess.synchro.LVDSTransmDevice;

public class SetupOptLinkTestWithRandom {

	public static void main(String[] args) throws DataAccessException {
		HibernateContext context = new SimpleHibernateContextImpl();
		EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
		ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
		HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
		ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);

		try {             
			List<HardwareDbCrate> crates = new ArrayList<HardwareDbCrate>();
			List<Board> linkBoards = equipmentDAO.getBoardsByType(BoardType.LINKBOARD, true);

			for(Board board : linkBoards) {
				if(board.getName().contains("LB_LBB_904_M")) 
				{
					//System.out.print(board.getName() + " ");
					HardwareDbBoard hdbBoard = dbMapper.getBoard(board);					
					if(((LinkBoard)board).isMaster()) {
						hdbBoard.getHardwareBoard().getDevices()[0].getItem("SEND.CHECK_ENA").write(1);
						hdbBoard.getHardwareBoard().getDevices()[0].getItem("SEND.CHECK_DATA_ENA").write(0);
						//hdbBoard.getHardwareBoard().getDevices()[0].getItem("SEND.TEST_ENA").write(1);
						//hdbBoard.getHardwareBoard().getDevices()[0].getItem("SEND.TEST_RND_ENA").write(1);
					}														
				}
			}
			
			Board tb = equipmentDAO.getBoardByName("TB0_904");
			HardwareDBTriggerBoard hdbTB= (HardwareDBTriggerBoard)dbMapper.getBoard(tb);
			
			for(HardwareDbChip opt : hdbTB.getOptos()) {
				opt.getHardwareChip().getItem("REC_CHECK_ENA").write(7);
				opt.getHardwareChip().getItem("REC_CHECK_DATA_ENA").write(0);
				//opt.getHardwareChip().getItem("REC_TEST_ENA").write(7);
				//opt.getHardwareChip().getItem("REC_TEST_RND_ENA").write(7);
			}
					

		} 
		catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			context.closeSession();
		}
	}  
}
