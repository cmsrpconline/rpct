package rpct.testAndUtils.boardConfigs;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ChipConfAssignment;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.configuration.SynCoderConf;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;

public class CheckChipConfigs {	
	static List<Chip> getChips(ChipType chipType, String crateName, String boardName, EquipmentDAO equipmentDAO, 
		ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) throws DataAccessException {
		List<Chip> chips = null;
		if(boardName.length() > 0) {
			Board board = equipmentDAO.getBoardByName(boardName);
			if(board == null) {
				throw new IllegalArgumentException("no board with name " + boardName);
			}
			chips = equipmentDAO.getChipsByTypeAndBoard(chipType, board, true);			
		}
		else if(crateName.length() > 0) {
			Crate crate = equipmentDAO.getCrateByName(crateName);
			//crate = equipmentDAO.getCrateById(300);
			if(crate == null) {
				throw new IllegalArgumentException("no crate with name " + crateName);
			}
			chips = equipmentDAO.getChipsByTypeAndCrate(chipType, crate, true);			
		}
		return chips;
	}
	
	public static void printConfigs(ChipType chipType, String crateName, String boardName, EquipmentDAO equipmentDAO, LocalConfigKey localConfigKey,
			ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) throws DataAccessException {
		System.out.println("localConfigKey " + localConfigKey);
		
		List<Chip> chips = getChips(chipType, crateName, boardName, equipmentDAO, configurationManager, configurationDAO);
				
		List<ChipConfAssignment>  assignments = configurationDAO.getChipConfAssignments(chips, localConfigKey);
		for(ChipConfAssignment assignment : assignments) {
			System.out.println(assignment.getChip().getBoard() + " chip " + assignment.getChip().getType() + " position = " + assignment.getChip().getPosition() 
					+ "\nconfig date " + assignment.getCreationDate() + "\n"
					+ assignment.getStaticConfiguration().toString() + "\n");
		}
		
	}
	
	public static void printConfigHistory(Chip chip, int notOlderThanMmonths, int notOlderThanDays, EquipmentDAO equipmentDAO, LocalConfigKey localConfigKey,
			ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) throws DataAccessException {
		System.out.println("localConfigKey " + localConfigKey);
					
		Date today = new Date();
		Calendar cal = new GregorianCalendar();
		cal.setTime(today);
		cal.add(Calendar.MONTH, -notOlderThanMmonths);
		cal.add(Calendar.DAY_OF_YEAR, -notOlderThanDays);
		System.out.println("only configurations not older than " + cal.getTime());
	
		List<ChipConfAssignment> confAssignmentList = configurationDAO.getChipConfAssignmentHistory(chip, localConfigKey);
        java.util.ListIterator<ChipConfAssignment> iteraror = confAssignmentList.listIterator(confAssignmentList.size());

        String str = "";
        while(iteraror.hasPrevious()) {                        
            ChipConfAssignment confAssignment1 = iteraror.previous(); 
            StaticConfiguration chipConf = confAssignment1.getStaticConfiguration();;
            if(confAssignment1.getCreationDate().before(cal.getTime() ) ) {
                break;
            }                        
                                      
            
            str += confAssignment1.getCreationDate() + "\n";
            //" timingCalc " + java.lang.Math.round(timing1) +
            str += chipConf.toString() + "\n";
        }
        
        if(str.length() != 0) {
        	System.out.println(chip.getBoard().getName());       
        	System.out.println(str);
        	System.out.print("\n");
        }
		
	}
	
	static void backToPreviousConfigs(Chip chip, LocalConfigKey inConfigKey, boolean saveNewConfToDb, EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) throws DataAccessException {

		List<ChipConfAssignment> confAssignmentList = configurationDAO.getChipConfAssignmentHistory(chip, inConfigKey);
		java.util.ListIterator<ChipConfAssignment> iteraror = confAssignmentList.listIterator(confAssignmentList.size());
		ChipConfAssignment confAssignment1 = iteraror.previous();//the latest assigment
		ChipConfAssignment confAssignment2 = iteraror.previous();//iteraror.previous();
		
		StaticConfiguration configuration1 = null;
		StaticConfiguration configuration2 = null; 
		if(confAssignment1 != null && confAssignment2 != null) {
			configuration1 = confAssignment1.getStaticConfiguration();
			configuration2 = confAssignment2.getStaticConfiguration();

			System.out.println("curent confAssignment1: " + confAssignment1.getLocalConfigKey().getName() + " " + confAssignment1.getCreationDate());
			System.out.println(configuration1);
			
			System.out.println("previous confAssignment2: " + confAssignment2.getLocalConfigKey().getName() + " " + confAssignment2.getCreationDate());
			System.out.println(configuration2);
		}                                                                       


		if(saveNewConfToDb) {
			//configurationDAO.saveObject(newCoderConf); //TODO
			configurationManager.assignConfiguration(chip, configuration2, inConfigKey);
			//configurationManger.assignConfiguration(lb.getSynCoder(), newCoderConf, ConfigurationManger.LOCAL_CONF_KEY_COSMIC);
			//configurationManger.assignConfiguration(lb.getSynCoder(), coderConf, ConfigurationManger.LOCAL_CONF_KEY_COSMIC_TIGHT);
			System.out.println("saved to DB!!!!!!!!!!!!!!!!!!!!!!!!!");
		}



	}
    
	/**
	 * @param args
	 * @throws DataAccessException 
	 */
	public static void main(String[] args) throws DataAccessException {
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);
        
		try {
			
			/*printConfigs(ChipType.TTUFINAL, "SC", "", equipmentDAO, configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_LHC3), configurationManager, configurationDAO);
			printConfigs(ChipType.TTUFINAL, "SC", "", equipmentDAO, configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_LHC3_BOTTOM), configurationManager, configurationDAO);
*/			
			/*LocalConfigKey localConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_RBCS_TTUS_LHC1);
			printConfigs(ChipType.TTUFINAL, "SC", "", equipmentDAO, localConfigKey, configurationManager, configurationDAO);*/
			
			LocalConfigKey localConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.CURRENT_TCS_CONF_KEY);
			/*			for(int iTc = 0; iTc < 1; iTc++) {
				//printConfigs(ChipType.GBSORT, "TC_" + iTc, "", equipmentDAO, localConfigKey, configurationManager, configurationDAO);
				printConfigs(ChipType.PAC, "TC_0", "", equipmentDAO, localConfigKey, configurationManager, configurationDAO);
				//printConfigs(ChipType.TCSORT, "TC_0", "", equipmentDAO, localConfigKey, configurationManager, configurationDAO);
			}*/
			
			//printConfigs(ChipType.RMB, "TC_1", "", equipmentDAO, localConfigKey, configurationManager, configurationDAO);
			//printConfigHistory(equipmentDAO.getChipById(2780), 5, 0, equipmentDAO, localConfigKey, configurationManager, configurationDAO);
//			printConfigs(ChipType.TCSORT, "TC_5", "", equipmentDAO, localConfigKey, configurationManager, configurationDAO);
			//printConfigs(ChipType.TCSORT, "TC_7", "", equipmentDAO, localConfigKey, configurationManager, configurationDAO);
			//LocalConfigKey localConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_TCS_V1036);
			//printConfigs(ChipType.TCSORT, "TC_904", "", equipmentDAO, localConfigKey, configurationManager, configurationDAO);
			
			//LocalConfigKey localConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.CURRENT_LBS_CONF_KEY);			
			//printConfigs(ChipType.SYNCODER, "LBB_YEN3_RE4_S4/5", "", equipmentDAO, localConfigKey, configurationManager, configurationDAO);
			//LocalConfigKey localConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_LBS_DEFAULT);
			
			//LocalConfigKey localConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_SC_FULL);
			//LocalConfigKey localConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_SC_BOTTOM);
			//printConfigs(ChipType.HALFSORTER, "SC", "", equipmentDAO, localConfigKey, configurationManager, configurationDAO);
			//localConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_SC_TOP);
			//printConfigs(ChipType.HALFSORTER, "SC", "", equipmentDAO, localConfigKey, configurationManager, configurationDAO);
			
			//LocalConfigKey localConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_FEBS_220mV);
			//printConfigs(ChipType.FEBCHIP, "LBB_904", "", equipmentDAO, localConfigKey, configurationManager, configurationDAO);
			
//			LocalConfigKey localConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_LBS_DEFAULT);
//			printConfigs(ChipType.SYNCODER, "LBB_904", "", equipmentDAO, localConfigKey, configurationManager, configurationDAO);
//			printConfigs(ChipType.SYNCODER, "LBB_904_2", "", equipmentDAO, localConfigKey, configurationManager, configurationDAO);
			
/*			LocalConfigKey localConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.CURRENT_TCS_CONF_KEY);
			Chip rmb = equipmentDAO.getChipById(1826);
			printConfigHistory(rmb, 12, 0, equipmentDAO, localConfigKey, configurationManager, configurationDAO);
*/
			
			backToPreviousConfigs(equipmentDAO.getChipById(2780), localConfigKey, true, equipmentDAO, configurationManager,configurationDAO);
		} catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            context.rollback();
        } 
        finally {
            context.closeSession();
        }
        

	}

}
