package rpct.testAndUtils.boardConfigs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;

public class EnableDevicesPasteura {

	/**
	 * @param args
	 * @throws DataAccessException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws DataAccessException, IOException {
		HibernateContext context = new SimpleHibernateContextImpl();
		EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
		HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
		ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
		ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);	 


		try {
		    configurationManager.enableBoard(equipmentDAO.getBoardByName("TC_BP"), true);
		    configurationManager.enableBoard(equipmentDAO.getBoardByName("TBn4"), false);
		    configurationManager.enableBoard(equipmentDAO.getBoardByName("TBn3"), false);
		    configurationManager.enableBoard(equipmentDAO.getBoardByName("TBn2"), false);		    
		    configurationManager.enableBoard(equipmentDAO.getBoardByName("TBn1"), true);
		    configurationManager.enableBoard(equipmentDAO.getBoardByName("TB0"), true);
		    configurationManager.enableBoard(equipmentDAO.getBoardByName("TBp1"), true);
		    configurationManager.enableBoard(equipmentDAO.getBoardByName("TBp2"), false);
		    configurationManager.enableBoard(equipmentDAO.getBoardByName("TBp3"), false);
		    configurationManager.enableBoard(equipmentDAO.getBoardByName("TBp4"), false);
		    for(Chip chip : equipmentDAO.getBoardByName("TBp2").getChips() ) {
		        if(chip.getPosition() == 8) {
		            configurationManager.enableChip(chip, true);
		        }
		        else if(chip.getType().equals(ChipType.PAC))
		            configurationManager.enableChip(chip, true);
		    }
		    
/*		    configurationManger.enableBoard(equipmentDAO.getBoardByName("LB_M17"), true);
		    configurationManger.enableBoard(equipmentDAO.getBoardByName("LB_M7"), true);
		    configurationManger.enableBoard(equipmentDAO.getBoardByName("LB_M4"), true);
		    configurationManger.enableBoard(equipmentDAO.getBoardByName("LB_M1"), true);*/

		    System.out.println("done");		    		   	
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			context.rollback();
		}
		finally {
			context.closeSession();
		}
	}

}
