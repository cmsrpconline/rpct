package rpct.testAndUtils.boardConfigs;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.rpc.ServiceException;

import rpct.datastream.RpctDelays;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigKey;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.PacConf;
import rpct.db.domain.configuration.RmbConf;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.configuration.TbGbSortConf;
import rpct.db.domain.configuration.TcSortConf;
import rpct.db.domain.configuration.LocalConfigKey.Subsystem;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.CrateWithLVDSLinks;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbMuxRecChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;

public class NewPacFirmware {    
	/*    public static void addBoardsSettingsToDB(List<CrateWithLVDSLinks> crates,
            HibernateContext context,
            EquipmentDAO equipmentDAO,
            HardwareDbMapper dbMapper,
            ConfigurationDAO configurationDAO,
            ConfigurationManger configurationManger) {


    }*/
	public static void main(String[] args) throws DataAccessException {
		if(args.length == 0) {            
			String[] argsL = { //TODO
					//"TC_904",
					"TC_0",                    
					"TC_1",
					"TC_2",                   
					"TC_3",                    
					"TC_4",
					"TC_5",                    
					"TC_6",                    
					"TC_7",
					"TC_8",                   
					"TC_9",                    
					"TC_10",
					"TC_11",
					//"PASTEURA_TC"
			};

			args = argsL;
		}

		boolean saveNewConfToDb = true; //TODO
		boolean newGlobalConfigKey = true; //TODO

		System.out.println("Selected crates");
		for(int i = 0; i < args.length; i++) {
			System.out.print(args[i] + " ");
		}

		HibernateContext context = new SimpleHibernateContextImpl();
		EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
		HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
		ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
		ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);   

		LocalConfigKey inConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_TCS_V1047); //TODO
		LocalConfigKey newLocalConfigKey = configurationManager.createLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_TCS_V1049, Subsystem.TCS);//TODO

		if(newGlobalConfigKey) {
			ConfigKey globalConfigKey = new ConfigKey();
			globalConfigKey.setId("LHC10"); //TODO
			globalConfigKey.setDescription("LHC patterns, pacConfigId = 0x1049, patterns including RE4, patterns the same as in the V1047, but extended quality in endcap; BX_OR is 0"); //TODO
			configurationDAO.saveObject(globalConfigKey);
			configurationManager.assignConfigKey(globalConfigKey.getId(), newLocalConfigKey); //TODO
			
			configurationManager.assignConfigKey(globalConfigKey.getId(), ConfigurationManager.LOCAL_CONF_KEY_FEBS_DEFAULT);//TODO
			configurationManager.assignConfigKey(globalConfigKey.getId(), ConfigurationManager.CURRENT_LBS_CONF_KEY);///TODO /LOCAL_CONF_KEY_LBS_DEFAULT is only for 904 and pasteura 			
			configurationManager.assignConfigKey(globalConfigKey.getId(), ConfigurationManager.LOCAL_CONF_KEY_SC_FULL); //TODO //LOCAL_CONF_KEY_SC_FULL
			configurationManager.assignConfigKey(globalConfigKey.getId(), ConfigurationManager.LOCAL_CONF_KEY_RBCS_TTUS_LHC1);//TODO
			
			globalConfigKey = new ConfigKey();
			globalConfigKey.setId("LHC10_BOTTOM"); //TODO
			globalConfigKey.setDescription("LHC patterns, pacConfigId = 0x1049, patterns including RE4, patterns the same as in the V1047, but extended quality in endcap; BX_OR is 0"); //TODO
			configurationDAO.saveObject(globalConfigKey);
			configurationManager.assignConfigKey(globalConfigKey.getId(), newLocalConfigKey); //TODO
			
			configurationManager.assignConfigKey(globalConfigKey.getId(), ConfigurationManager.LOCAL_CONF_KEY_FEBS_DEFAULT);//TODO
			configurationManager.assignConfigKey(globalConfigKey.getId(), ConfigurationManager.CURRENT_LBS_CONF_KEY);///TODO /LOCAL_CONF_KEY_LBS_DEFAULT is only for 904 and pasteura 			
			configurationManager.assignConfigKey(globalConfigKey.getId(), ConfigurationManager.LOCAL_CONF_KEY_SC_BOTTOM); //TODO //LOCAL_CONF_KEY_SC_FULL
			configurationManager.assignConfigKey(globalConfigKey.getId(), ConfigurationManager.LOCAL_CONF_KEY_RBCS_TTUS_LHC1);//TODO
		}
		else {
			//configurationManager.assignConfigKey("LHC9", newLocalConfigKey); //TODO
			//remove previous assigment TODO
		}

		//we do not change the RMB configuration
/*		//RMB configuration
		int rmbDataDelay = RpctDelays.RMB_DATA_DELAY;        
		int chanEna = 0; //0 = all channels enable
		int preTriggerVal = RpctDelays.PRE_TRIGGER_VAL; //zakers BX branych do DAQ
		int postTriggerVal = RpctDelays.POST_TRIGGER_VAL; 
		int trgDelay = 0; */

		int bxOfCoincidence = 0; //TODO 0 means HSCP triggering OFF
		int pacEna = 0xfff;
		int pacConfigId = 0x1049; //TODO
 

		String bootScript = "boot_lhc_v" + Integer.toHexString(pacConfigId) + ".sh"; //TODO

		try {
			List<TriggerCrate> crates = new ArrayList<TriggerCrate>();
			for(int i = 0; i < args.length; i++) {                
				crates.add((TriggerCrate)equipmentDAO.getCrateByName(args[i]));
			}

			for(TriggerCrate tc : crates) {   
				for(TriggerBoard tb : tc.getTriggerBoards()) {
					for(Chip chip : tb.getChips()) {
						StaticConfiguration  newConf = null;
						if(chip.getDisabled() != null)
							continue;
						if( (chip.getType() == ChipType.PAC) || 
								(chip.getType() == ChipType.RMB) ||
								(chip.getType() == ChipType.GBSORT) ) {
							
							StaticConfiguration oldConf = null;
							try {
								oldConf = configurationManager.getConfiguration(chip, inConfigKey);
							}
							catch(NullPointerException e) {
								System.out.println(e.getMessage() + " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
							}
							if(RmbConf.class.isInstance(oldConf) ) {
								/*newConf = new RmbConf((RmbConf)oldConf);

								((RmbConf)newConf).setDataDelay(rmbDataDelay);
								((RmbConf)newConf).setChanEna(chanEna);
								((RmbConf)newConf).setPreTriggerVal(preTriggerVal);
								((RmbConf)newConf).setPostTriggerVal(postTriggerVal);
								((RmbConf)newConf).setTrgDelay(trgDelay);*/
								
								configurationManager.assignConfiguration(chip, oldConf, newLocalConfigKey.getName());
								System.out.println("the old RMB configuration was assigned to " + newLocalConfigKey.getName() + " for the " + chip.getBoard().getName() + " " + chip.getType() );

							} 
							else if(TbGbSortConf.class.isInstance(oldConf) ) {									
								configurationManager.assignConfiguration(chip, oldConf, newLocalConfigKey.getName());
								System.out.println("the old TbGbSortConf was assigned to " + newLocalConfigKey.getName() + " for the " + chip.getBoard().getName() + " " + chip.getType() );
							}
							else if(PacConf.class.isInstance(oldConf) ) {
								newConf = new PacConf((PacConf)oldConf);

								((PacConf)newConf).setBxOfCoincidence(bxOfCoincidence);
								((PacConf)newConf).setPacEna(pacEna);
								((PacConf)newConf).setPacConfigId(pacConfigId);

								configurationDAO.saveObject(newConf);
								configurationManager.assignConfiguration(chip, newConf, newLocalConfigKey.getName());
								System.out.println("the configuration " + newLocalConfigKey.getName() + " was added for the " + chip.getBoard().getName() + " " + chip.getType() );

							}

						}
					}
				} //end TB loop
				
				Chip chip = tc.getBackplane().getTcSorter();
				TcSortConf newConf = new TcSortConf((TcSortConf)configurationManager.getConfiguration(chip, inConfigKey));
				((TcSortConf)newConf).setBootScript(bootScript);
				if(saveNewConfToDb && newConf != null) {
					configurationDAO.saveObject(newConf);

					configurationManager.assignConfiguration(chip, newConf, newLocalConfigKey.getName());
					System.out.println("the configuration " + newLocalConfigKey.getName() + " was added for the " + chip.getBoard().getName() + " " + chip.getType() );
				}	

			}   

		} catch (DataAccessException e) {
			e.printStackTrace();
			context.rollback();
		}
		finally {
			context.closeSession();
		}
	}
}