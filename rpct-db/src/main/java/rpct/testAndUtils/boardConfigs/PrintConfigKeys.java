package rpct.testAndUtils.boardConfigs;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.ConfigKey;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.LocalConfigKey.Subsystem;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.synchro.DefOut;

public class PrintConfigKeys {

    /**
     * @param args
     * @throws DataAccessException 
     */
	
    public static void main(String[] args) throws DataAccessException {
    	HibernateContext context = new SimpleHibernateContextImpl();
        try {
            
            EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
            ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
            ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);	 
            
            System.out.println("LocalConfigKeys by subsystems");
            for(LocalConfigKey.Subsystem subsytem : LocalConfigKey.Subsystem.values()) {
            	System.out.println("\nsubsytem " + subsytem + ":");
	            for(LocalConfigKey localConfigKey : configurationDAO.getLocalConfigKeysBySubsystem(subsytem)) {
	            	System.out.print("   " + localConfigKey + " used for the global keys: ");
	            	for(String configKey : configurationDAO.getGlobalConfigKeysByLocalConfigKey(localConfigKey)) {
	            		System.out.print(configKey + ", ");
	            	}
	            	System.out.println("\n");
	            }
            }
            System.out.println();
            
            System.out.println("GlobalConfigKeys:");
            for(ConfigKey configKey : configurationDAO.getGlobalConfigKeys()) {
            	System.out.println(configKey.getId() + " " + configKey.getDescription());
            	for(LocalConfigKey localConfigKey : configurationDAO.getLocalConfigKeysByGlobalConfigKey(configKey.getId())) {
                	System.out.println("    "+ localConfigKey);
                }
            	System.out.println("");
            }
            
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            context.rollback();
        } 
        finally {
            context.closeSession();
        }
    }

}
