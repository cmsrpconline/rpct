package rpct.testAndUtils.boardConfigs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Comparator;

import javax.xml.rpc.ServiceException;

import org.apache.log4j.PropertyConfigurator;

import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.HalfSortConf;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.configuration.LocalConfigKey.Subsystem;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.TcHsbConnection;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;

public class NewHsbConfig {

    /**
     * @param args
     * @throws DataAccessException 
     * @throws IOException 
     */
    public static void main(String[] args) throws DataAccessException, IOException {
        //PropertyConfigurator.configure("log4j.properties");
        HibernateContext context = new SimpleHibernateContextImpl();
        try {

        	Integer[] tcCablesMask = {
        			0, //"TC_0", 
        			3, //"TC_1", 
        			3, //"TC_2", 
        			3, //"TC_3", 
        			3, //"TC_4", 
        			0, //"TC_5", 
        			0, //"TC_6", 
        			0, //"TC_7", 
        			0, //"TC_8", 
        			0, //"TC_9", 
        			0, //"TC_10",
        			0, //"TC_11",
        	};                
                       
            boolean addToDB = true; 
                        
            
            EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
            //HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
            ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
            ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);	 
            LocalConfigKey configKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_SC_FULL);
            //LocalConfigKey configKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_SC_BOTTOM);
            //LocalConfigKey newConfigKey = configurationManager.createLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_SC_TOP, Subsystem.SC);
            LocalConfigKey newConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_SC_TOP);
            
            Comparator<Chip> comparator = new Comparator<Chip>() {
                public int compare(Chip chip1, Chip chip2) {
                    return (chip1.getId() - chip2.getId());

                }
            };
            
            Map<Chip, HalfSortConf> newHSBConfs = new TreeMap<Chip, HalfSortConf>(comparator); 
            for(int i = 0; i < 2; i++) {
            	Board hsb = equipmentDAO.getBoardByName("HSB_" + i);
            	Set<Chip> chips = hsb.getChips();
            	if (chips.size() != 1) {
            		throw new RuntimeException("invalid number of chips");
            	}
            	Chip hsbChip = chips.iterator().next();

            	HalfSortConf currentConf = (HalfSortConf) configurationManager.getConfiguration(hsbChip, configKey);
            	            	
            	HalfSortConf newConf = new HalfSortConf(currentConf);
                newConf.setId(0); //czemu tak???????????????????????? KB
                //byte[] recDataDelay = {0x55,0x55,0x55,0x55}; //TODO change into 0,0,0,0
				//newConf.setRecDataDelay(recDataDelay);
				
				newHSBConfs.put(hsbChip, newConf);				                              
            	System.out.println(hsb.getName() + " recChanEna " + Integer.toHexString(currentConf.getRecChanEnaMask()));
            	System.out.println(currentConf.getRecChanEnaStr(hsb.getName()));
            	System.out.println();
            }                                  
            
            for(int iTC = 0; iTC < 12; iTC++) {
            	String crateName = "TC_" + iTC; 
            	Crate crate = equipmentDAO.getCrateByName(crateName);
            	if(crate == null) {
            		throw new RuntimeException("crate with name " + crateName + " does not exists");
            	}
            	if(crate.getType() != CrateType.TRIGGERCRATE) {
            		throw new RuntimeException("crate with name " + crateName + " is not TRIGGERCRATE");
            	}

            	int inputCablesMask  = tcCablesMask[iTC];
            	if(inputCablesMask < 0 || inputCablesMask > 3)
            		throw new RuntimeException("the inputCablesMask is not correct. Allowed values: 0...3");

            	TriggerCrate tc = (TriggerCrate)crate;
            	List<TcHsbConnection> tcHsbConnections = tc.getTcHsbConnections(equipmentDAO);
            	for(TcHsbConnection tcHsbConnection : tcHsbConnections) {
            		int recChanEnaMask = newHSBConfs.get(tcHsbConnection.getHalfSort() ).getRecChanEnaMask();

            		recChanEnaMask &=  ~(3 << tcHsbConnection.getCableInputNum()); //reset
            		recChanEnaMask |=  inputCablesMask << tcHsbConnection.getCableInputNum(); //set

            		newHSBConfs.get(tcHsbConnection.getHalfSort() ).setRecChanEna(recChanEnaMask);            		            	
            	}
            }
                        
            
            for(Map.Entry<Chip, HalfSortConf> entry : newHSBConfs.entrySet()) {
            	if(addToDB) {
            		configurationDAO.saveObject(entry.getValue());
            		configurationManager.assignConfiguration(entry.getKey(), entry.getValue(), newConfigKey.getName());
            	}
            	
                System.out.println("New config " + entry.getKey().getBoard().getName() + " recChanEna " 
        				+ Integer.toHexString(entry.getValue().getRecChanEnaMask()));
                
                System.out.println(entry.getValue().getRecChanEnaStr(entry.getKey().getBoard().getName()));

            }
            context.commit();
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			context.rollback();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		finally {
			context.closeSession();
		}
    }

}
