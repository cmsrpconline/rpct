package rpct.testAndUtils.boardConfigs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Set;

import javax.xml.rpc.ServiceException;

import org.apache.log4j.PropertyConfigurator;

import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.HalfSortConf;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.TcHsbConnection;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;

public class ChangeInputDelayOnHsb {

    /**
     * @param args
     * @throws DataAccessException 
     * @throws IOException 
     */
    public static void main(String[] args) throws DataAccessException, IOException {
        //PropertyConfigurator.configure("log4j.properties");
        HibernateContext context = new SimpleHibernateContextImpl();
        try {
            EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
            //HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
            ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
            ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);	 
            LocalConfigKey configKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_SC_FULL);
            //LocalConfigKey configKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_SC_BOTTOM);
            

            for(int i = 0; i < 1; i++) {
            	Board hsb = equipmentDAO.getBoardByName("HSB_" + i);
            	Set<Chip> chips = hsb.getChips();
            	if (chips.size() != 1) {
            		throw new RuntimeException("invalid number of chips");
            	}
            	Chip hsbChip = chips.iterator().next();

            	HalfSortConf currentConf = (HalfSortConf) configurationManager.getConfiguration(hsbChip, configKey);
            	            	
            	HalfSortConf newConf = new HalfSortConf(currentConf);
                newConf.setId(0); //czemu tak???????????????????????? KB
                //byte[] recDataDelay = {0x55,0x55,0x55,0x55}; //TODO change into 0,0,0,0
                //byte[] recDataDelay = {(byte)0xaa, (byte)0xaa, (byte)0xaa, (byte)0xaa}; //TODO change into 0,0,0,0
				//newConf.setRecDataDelay(recDataDelay);
                newConf.getRecFastRegAdd().setValue(8, -1);
                configurationDAO.saveObject(newConf);
                configurationManager.assignConfiguration(hsbChip, newConf, configKey.getName());
                
            	//System.out.println(hsb.getName() + " recChanEna " + Integer.toHexString(currentConf.getRecChanEnaMask()));
            	//System.out.println(currentConf.getRecChanEnaStr(hsb.getName()));
            	
            	System.out.println(hsb.getName());
            	System.out.println(newConf);
            	System.out.println();
            }
            context.commit();
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			context.rollback();
		} 
		finally {
			context.closeSession();
		}
    }

}
