package rpct.testAndUtils.boardConfigs;

import java.util.List;

import org.apache.log4j.Logger;

import rpct.db.DataAccessException;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.FebBoard;

/**
 * Created on 2009-12-04
 * 
 * @author Nikolay Darmenov
 * @version $Id$
 */
public class febConfTest {
	private static final boolean saveToDb = ConfUtil.getBooleanProperty("saveToDb");
    private static final Logger logger = Logger.getLogger(febConfTest.class);
	
	public static void main(String[] args) throws DataAccessException {
		init();

		testDirectly();

		//testWebServices();
	}

	private static void init() {
		if (saveToDb) {
			System.out.println("A dummy config will be saved to DB!");
			if (!ConfUtil.proceed("Proceed?")) {
				System.exit(2);
			}
		} else {
			System.out.println("WARNING: Config will NOT be saved to DB, simulation only!");
		}
	}

	private static void testDirectly() throws RuntimeException,
			DataAccessException {
		logger.info("Will test direct connection to the DB ...");
		try {
			logger.info("Dumping FEBBOARDs and FEB chips confs ...");
			final boolean enabledOnlyBoards = true;
			for (Board board : ConfUtil.getBoards(
					BoardType.FEBBOARD, enabledOnlyBoards)) {
				System.out.println(board);
				System.out
						.println("board's maxChipCount = " + board.maxChipCount());

				if (!(board instanceof FebBoard)) {
					throw new RuntimeException("Got board of class "
							+ board.getClass());
				}

				// Dump this board chips config
				final boolean enabledOnlyChips = true;
				ConfUtil.doConf(ConfUtil.getChips(ChipType.FEBCHIP, enabledOnlyChips), saveToDb);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			System.err.println("[ERROR] context to be rollbacked");
			ConfUtil.DbHandler.context.rollback();
		} finally {
			ConfUtil.DbHandler.context.closeSession();
			logger.info("DB session closed.");
		}
		
		System.out.println("Done.");
	}

	private static void testWebServices() {
		logger.info("Will test web services ...");

		// Some FEB chips
		final List<Integer> chipIds = ConfUtil.getIntListProperty("chipIds");
		final String globalConfigKey = "DEFAULT";

		try {
			ConfUtil.wsGetConfigurationSet(ConfUtil.getProperty("url"), chipIds, globalConfigKey );
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			logger.info("Call ends.");
		}
	}

}