package rpct.testAndUtils.boardConfigs;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ChipConfAssignment;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.FebChipConf;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.FebBoard;
import rpct.db.domain.equipment.I2cCbChannel;
import rpct.db.domain.equipment.chamberloaction.BarrelOrEndcap;
import rpct.db.domain.equipment.chamberloaction.ChamberLocation;
import rpct.db.domain.equipment.chamberloaction.ChamberLocationDAO;
import rpct.db.domain.equipment.chamberloaction.ChamberLocationDAOHibernate;
import rpct.db.domain.equipment.feblocation.FebLocation;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;

/**
 * Created on 2010-10-18
 * 
 * @author Karol Bunkowski
 * @version $Id$
 */
public class FebSettings {

    static void printConfiguration(EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) throws DataAccessException {
    	int i = 0;
    	for(Chip c : equipmentDAO.getChipsByType(ChipType.FEBCHIP, false) ) {   
    		//if(c.getBoard().getName().contains("RB0"))
    		{
    			try {
    				StaticConfiguration configuration = configurationManager.getConfiguration(c, configurationManager.getLocalConfigKey("FEBS_MIN_PLUS_"+5+"MV"));
    				//configurationManger.getDefaultLocalConfigKey(
    				System.out.println("configuration for the " + c.getBoard().getName());
    				System.out.println(configuration + "\n");
    			}
    			catch(NullPointerException ex) {
    				System.out.println("no configuration for the " + c.getBoard().getName() + " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
    			}
    		}
    		if(++i == 10)
    			break;
    	}
    }
    
    static void printConfigurationToFile(String localCOnfigKey, EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) throws DataAccessException, FileNotFoundException {
    	String fileName =  System.getenv("HOME") + "/bin/out/febTresholdsDump_" + localCOnfigKey + ".txt";
        PrintStream outFile = new PrintStream(new FileOutputStream(fileName, false));
        
        LocalConfigKey localConfigKey = configurationManager.getLocalConfigKey(localCOnfigKey);
        
        long start = System.currentTimeMillis();
        List<ChipConfAssignment> confAssignments = configurationDAO.getChipConfAssignments(ChipType.FEBCHIP, localConfigKey);
        System.out.println("getChipConfAssignments() " + (System.currentTimeMillis() - start)/1000. + " s");
        
        start = System.currentTimeMillis();
        List<FebBoard> febBoards = equipmentDAO.getFebBoards();
        System.out.println("getFebBoards() " + (System.currentTimeMillis() - start)/1000. + " s");
        
        for(ChipConfAssignment confAssignment : confAssignments ) {   
    		{
    			try {
    				FebChipConf conf = (FebChipConf)confAssignment.getStaticConfiguration();
    				//FebBoard febBoard = equipmentDAO.getFebBoard(confAssignment.getChip().getBoard().getId());
    				for(FebBoard febBoard : febBoards) {
    					if(febBoard.getId() == confAssignment.getChip().getBoard().getId()) {
    						outFile.println(confAssignment.getChip().getId() + "\t" +    						
    								febBoard.getName() + "\t" +
    								febBoard.getFebLocation().getFebLocalEtaPartition() + "\t" + 
    								confAssignment.getChip().getPosition() + "\t" +
    								conf.getVth_mV());
    						//+ "\t" + conf.getVthOffset_mV());
    						break;
    					}
    				}

    			}
    			catch(NullPointerException ex) {
    				System.out.println("no configuration for the " + confAssignment.getChip().getBoard().getName() + " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
    			}
    		}
    	}
    }
    
    static void generateConfigurations(boolean saveToDb, LocalConfigKey localConfigKey, EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) throws DataAccessException {
    	List<FebChipConf> febChipConfs = new ArrayList<FebChipConf>();
    	for(int i = 1; i <= 20; i++) {
    		FebChipConf febChipConf = new FebChipConf(i * 10, 0, 3500, 0);
    		if(saveToDb) {
    			configurationDAO.saveObject(febChipConf);   
    			febChipConfs.add(febChipConf);
    		}
    	}

    	
    	//for(Board board : equipmentDAO.getCrateByName("LBB_904").getBoards()) {
    	int i = 0;
    	for(Board board : equipmentDAO.getBoardsByType(BoardType.FEBBOARD, true)) {    	
    		if(board.getType() == BoardType.FEBBOARD) {
    			System.out.println(board);
    			for(Chip chip : board.getChips()) {
    				if(saveToDb) 
    					configurationManager.assignConfiguration(chip, febChipConfs.get(i%febChipConfs.size()), localConfigKey);
    				i++;
    			}
    		}
    	}
    }
    
    static void generateConfigurations(boolean saveToDb, int threshold_mV, LocalConfigKey localConfigKey, EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) throws DataAccessException {    	
    	/*FebChipConf febChipConfBarrel = new FebChipConf(FebChipConf.mV2dac(threshold_mV), 0, 1434, 0);
		if(saveToDb) {
			configurationDAO.saveObject(febChipConfBarrel);   
		}*/
		
		FebChipConf febChipConfEndcap = new FebChipConf(FebChipConf.mV2dac(threshold_mV), 0, 1434, 0);
		if(saveToDb) {
			configurationDAO.saveObject(febChipConfEndcap);   
		}
		
		//System.out.println("febChipConfBarrel\n" +febChipConfBarrel);
		System.out.println("febChipConfEndcap\n" +febChipConfEndcap);
    	
    	//for(Board board : equipmentDAO.getCrateByName("LBB_904").getBoards()) {
		int febNum = 0;
    	for(Board board : equipmentDAO.getBoardsByType(BoardType.FEBBOARD, true)) {    		
    		if(board.getType() == BoardType.FEBBOARD) {
    			//if( board.getCrate().getName().contains("RE4")) 
    			{ //TODO
    				System.out.println((febNum++) + " " + board);    			
    				for(Chip chip : board.getChips()) {
    			       /*if(configurationDAO.getChipConfAssignment(chip, localConfigKey) != null) {
    					throw new RuntimeException("some configuration already exists for "+ board);
    				    }*/
    					if(saveToDb)
    						configurationManager.assignConfiguration(chip, febChipConfEndcap, localConfigKey);
    				}
    			}
    		}
    	}
    }
          
/*    static void generateConfigurations(boolean saveToDb, int threshold_mV, LocalConfigKey localConfigKey, EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) throws DataAccessException {    	
    	
    	List<FebChipConf> febChipConfs = new ArrayList<FebChipConf>();
    	for(int i = 0; i < 4; i++) {
    		FebChipConf febChipConf = new FebChipConf(200 + i * 50, 0, 3500, 0);
    		if(saveToDb) {
    			configurationDAO.saveObject(febChipConf);   
    			febChipConfs.add(febChipConf);
    		}
    	}


    	FebChipConf febChipConfBarrel = new FebChipConf(FebChipConf.mV2dac(threshold_mV), 0, 1434, 0);
		if(saveToDb) {
			configurationDAO.saveObject(febChipConfBarrel);   
		}
		
		FebChipConf febChipConfEndcap = new FebChipConf(FebChipConf.mV2dac(threshold_mV), 0, 1434, 0);
		if(saveToDb) {
			configurationDAO.saveObject(febChipConfEndcap);   
		}
		
		//System.out.println("febChipConfBarrel\n" +febChipConfBarrel);
		System.out.println("febChipConfEndcap\n" +febChipConfEndcap);
    	
    	//for(Board board : equipmentDAO.getCrateByName("LBB_904").getBoards()) {
    	for(Board board : equipmentDAO.getBoardsByType(BoardType.FEBBOARD, true)) {
    		if(board.getType() == BoardType.FEBBOARD && Math.abs(((FebBoard)board).getFebLocation().getChamberLocation().getDiskOrWheel() ) == 4) {
    			System.out.println(board);    			
    			for(Chip chip : board.getChips()) {
    				if(configurationDAO.getChipConfAssignment(chip, localConfigKey) != null) {
    					throw new RuntimeException("some configuration already exists for "+ board);
    				}
    				//if( ((FebBoard)board).getBarrelOrEndcap() == BarrelOrEndcap.Barrel) 
    				{
    					if(saveToDb)
    						configurationManager.assignConfiguration(chip, febChipConfBarrel, localConfigKey);
    				}
    				//else if( ((FebBoard)board).getBarrelOrEndcap() == BarrelOrEndcap.Endcap) { 
    					if(saveToDb)
    						configurationManager.assignConfiguration(chip, febChipConfEndcap, localConfigKey);
    				//}
    			}
    		}
    	}
    }*/
    
    static void generateConfigurationsFortresholdScan(boolean saveToDb, EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) throws DataAccessException {
    	LocalConfigKey inputConfigKey =  configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_FEBS_DEFAULT);
    	//LOCAL_CONF_KEY_FEBS_MIN_THR
    	long start = System.currentTimeMillis();
    	
    	List<ChipConfAssignment> chipConfAssignments = configurationDAO.getChipConfAssignments(ChipType.FEBCHIP, inputConfigKey);
    	System.out.println("getChipConfAssignments() " + (System.currentTimeMillis() - start)/1000. + " s");
    	
		/*
		 * Antont, 5 June 2015
		 * For the update you have to copy the actual DEFAULT key to
		 * FEBS_MIN_PLUS_5MV togerther with offsets and respectively for
		 * FEBS_MIN_THR to subtract 5mV for each chip, for FEBS_MIN_PLUS_10MV to
		 * add 5 mV, for FEBS_MIN_PLUS_15MV to add 10mV to each chip etc... You
		 * can copy the same offsets to all the keys.
		 * 
		 * Anton 2 October 2015
		 * In view of the forthcoming threshold scan, I kindly ask you to update the following 5 keys, as per below:
			FEBS_MIN_THR = FEBS_DEFAULT - 5 mV
			FEBS_MIN_PLUS_5MV = FEBS_DEFAULT
			FEBS_MIN_PLUS_10MV = FEBS_DEFAULT + 5mV
			FEBS_MIN_PLUS_15MV = FEBS_DEFAULT + 10 mV
			FEBS_MIN_PLUS_20MV = FEBS_DEFAUL + 15 mV

		 */
				
    	for(int i = -5; i <= 15; i+=5) {
    		int treshold = i;
 		
    		String keyName = "FEBS_MIN_PLUS_" + treshold + "MV";
    		if(i==-5) {
    			keyName = "FEBS_MIN_THR";
    		}
    		else {
    			keyName = "FEBS_MIN_PLUS_" + (5 + treshold) + "MV";
    		}
    		
    		LocalConfigKey localConfigKey = configurationDAO.getLocalConfigKeyByName(keyName);
    		if(localConfigKey == null && saveToDb)
    			localConfigKey = configurationManager.createLocalConfigKey(keyName, LocalConfigKey.Subsystem.FEBS );
    		
    		System.out.println("generating settings for treshold " + treshold + " mV key " + localConfigKey);
    		
    		for(ChipConfAssignment chipConfAssignment : chipConfAssignments) {     			
    			FebChipConf oldFebChipConf = (FebChipConf)chipConfAssignment.getStaticConfiguration();
    			FebChipConf febChipConf = new FebChipConf(oldFebChipConf);
    			febChipConf.setVth_mV(febChipConf.getVth_mV() + treshold);
    			if(saveToDb) {
    				configurationDAO.saveObject(febChipConf);   
    				configurationManager.assignConfiguration(chipConfAssignment.getChip(), febChipConf, localConfigKey);
    			}
    		}    		
    	}
    }
    
    static void generateConfigurationsFortresholdScan1(boolean saveToDb, EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) throws DataAccessException {
    	LocalConfigKey inputConfigKey =  configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_FEBS_DEFAULT);
    	//LOCAL_CONF_KEY_FEBS_MIN_THR
    	long start = System.currentTimeMillis();
    	
    	List<ChipConfAssignment> chipConfAssignments = configurationDAO.getChipConfAssignments(ChipType.FEBCHIP, inputConfigKey);
    	System.out.println("getChipConfAssignments() " + (System.currentTimeMillis() - start)/1000. + " s");
    				
    	for(int treshold = 200; treshold <= 300; ) {    				 
    		String keyName = "FEBS_" + treshold + "MV";

    		LocalConfigKey localConfigKey = configurationDAO.getLocalConfigKeyByName(keyName);
    		if(localConfigKey == null && saveToDb)
    			localConfigKey = configurationManager.createLocalConfigKey(keyName, LocalConfigKey.Subsystem.FEBS );
    		
    		System.out.println("generating settings for treshold " + treshold + " mV key " + localConfigKey);
    		
    		for(ChipConfAssignment chipConfAssignment : chipConfAssignments) {   
    			if( chipConfAssignment.getChip().getBoard().getCrate().getName().contains("RE4") == false) //TODO
    				continue;
    			//System.out.println("generating settings for chip in FEB " + chipConfAssignment.getChip().getBoard().getName());
    			
    			FebChipConf oldFebChipConf = (FebChipConf)chipConfAssignment.getStaticConfiguration();
    			FebChipConf febChipConf = new FebChipConf(oldFebChipConf);
    			febChipConf.setVth_mV(treshold);
    			//System.out.println(febChipConf.toString());
    			if(saveToDb) {
    				configurationDAO.saveObject(febChipConf);   
    				configurationManager.assignConfiguration(chipConfAssignment.getChip(), febChipConf, localConfigKey);
    			}
    		}
    		    		
    		if(treshold <= 235)
    			treshold+=5;
    		else
    			treshold+=10;
    		
    		if(treshold == 290)
    			treshold = 300;
    	}
    }
    
    static void reassignConfigsToKeys(boolean saveToDb, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO,
    		LocalConfigKey inputConfigKey, LocalConfigKey outputConfigKey) throws DataAccessException {
    	
    	System.out.println("asssingnin settings from the key " +inputConfigKey + " to the key " + outputConfigKey);
    	long start = System.currentTimeMillis();
    	
    	List<ChipConfAssignment> chipConfAssignments = configurationDAO.getChipConfAssignments(ChipType.FEBCHIP, inputConfigKey);
    	System.out.println("getChipConfAssignments() " + (System.currentTimeMillis() - start)/1000. + " s");
    					
    	for(ChipConfAssignment chipConfAssignment : chipConfAssignments) {    		
    		FebChipConf oldFebChipConf = (FebChipConf)chipConfAssignment.getStaticConfiguration();    		
    		if(saveToDb) {
    			configurationManager.assignConfiguration(chipConfAssignment.getChip(), oldFebChipConf, outputConfigKey);
    		}
    	}    	
    }   
    
    
    static void changeCbChannel(boolean saveToDb, ChamberLocationDAO chamberLocationDAO ) throws DataAccessException {
    	
    	System.out.println("changeCbChannel");
    	
    	ChamberLocation chamber = chamberLocationDAO.getByName("W+2/RB1out/8");
    	System.out.println(chamber);
    	
    	for(FebLocation febLocation : chamber.getFEBLocations()) {
    		System.out.println(febLocation + " " + febLocation.getI2cCbChannel());
    	};
   	
    }  
    
	/**
	 * @param args
	 * @throws DataAccessException 
	 */
	public static void main(String[] args) throws DataAccessException {
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        ChamberLocationDAO chamberLocationDAO = new ChamberLocationDAOHibernate(context);
        ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);	 
        try {
        	//printConfigurationToFile(equipmentDAO, configurationManager, configurationDAO);
        	
        	//LocalConfigKey localConfigKey = configurationManager.createLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_FEBS_10mV, LocalConfigKey.Subsystem.FEBS );
        	//LocalConfigKey localConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_FEBS_210mV);        	
        	//LocalConfigKey localConfigKey = configurationManager.createLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_FEBS_195mV, LocalConfigKey.Subsystem.FEBS );
        	//LocalConfigKey localConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_FEBS_DEFAULT);
        	
        	//generateConfigurations(true, 195, localConfigKey, equipmentDAO, configurationManager, configurationDAO);
        	
        		//generateConfigurations(true, localConfigKey, equipmentDAO, configurationManager, configurationDAO);
        	generateConfigurationsFortresholdScan(true, equipmentDAO, configurationManager, configurationDAO);
  
        	//String localCOnfigKey = "FEBS_MIN_PLUS_5MV";
        	//printConfigurationToFile(localCOnfigKey, equipmentDAO, configurationManager, configurationDAO);
        	
        	//LocalConfigKey inputConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_FEBS_DEFAULT);
        	
        	
        	/*LocalConfigKey inputConfigKey = configurationManager.getLocalConfigKey("FEBS_MIN_PLUS_5MV");          			
        	//LocalConfigKey outputConfigKey = configurationManager.createLocalConfigKey("FEBS_DEFAULT_2012", LocalConfigKey.Subsystem.FEBS );
        	LocalConfigKey outputConfigKey = configurationManager.getLocalConfigKey("FEBS_DEFAULT");
        	reassignConfigsToKeys(true, configurationManager, configurationDAO, inputConfigKey, outputConfigKey);
        	configurationManager.tagCurrentConfigs(outputConfigKey, ChipType.FEBCHIP, "12_01_24_COPY_MIN_PLUS_5MV", "copy of FEBS_MIN_PLUS_5MV");
*/        	
        	//configurationManager.tagCurrentConfigs(localConfigKey, ChipType.FEBCHIP, "11_11_07_FEBS_DEFAULT", "used during 2011 proton runs, 220 mV barrel, 215 mV endcap, then tuned by hand only");
        	//changeCbChannel(false, chamberLocationDAO);
        } 
        catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            context.rollback();
        } 
       /* catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/
        catch (Error e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            context.rollback();
        } /*catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} */
        finally {
            context.closeSession();
        }
	}

}
