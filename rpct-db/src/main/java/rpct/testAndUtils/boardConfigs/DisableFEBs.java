package rpct.testAndUtils.boardConfigs;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.FebBoard;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;

public class DisableFEBs {
   
    private static PrintStream prntSrm = null;
    private static double maxTTCFiber = 0;    
   

    static void putDiasblesFromFile(boolean saveToDb, File inputFile, EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) throws DataAccessException, FileNotFoundException {

        Scanner scanner = new Scanner(inputFile);
        
        if(saveToDb) {
            System.out.println("Configs will be seved to DB"); 
        }
        else {
            System.out.println("Config will NOT be seved to DB, simuation only!!!!!!!!!!!!!!!!!!!!!!!!!!!"); 
        }

        System.out.println("applaing settings from file fileName " + inputFile.getPath() + "/" + inputFile.getName());

        //reading the file     
        int i =0;
        while(scanner.hasNextLine()) {
            if(scanner.hasNext() == false) {
                break;
            }
            
            long febFilipsId =  scanner.nextLong();
            int chipId  = scanner.nextInt();
            String febName = scanner.next();

            Chip febChip = equipmentDAO.getChipById(chipId);
            FebBoard febBoard = (FebBoard)febChip.getBoard();
            
            String febNameTest = febBoard.getFebLocation().getChamberLocation().getChamberLocationName() + "_" +
            febBoard.getFebLocation().getFebLocalEtaPartition() + ":" +
            febBoard.getCbChannel() + "." + febBoard.getI2cLocalNumber() + "#" + febChip.getPosition();

            System.out.println(febNameTest + "\t" + febChip.getPosition() + "\t| " + febName + "\t" + chipId);
            if(febNameTest.equals(febName) == false) {
            	System.out.println("!!!!!!!!!!!!!!");
            	throw new RuntimeException("feb name from file do not much the DB " + febNameTest + " " +  febName);
            }
            if(saveToDb) {
                configurationManager.enableChip(febChip, false);                
            }              
            /*i++;
            if(i == 2000)
            	break;*/
        }        
    }
  
    
    static void printDisabledFebs(ConfigurationDAO configurationDAO) throws DataAccessException {
        System.out.println("\nAll FEBs which are disabled now:");

        for(Chip chip : configurationDAO.getDisabledChips()) {
        	if(chip.getType() == ChipType.FEBCHIP) {
        		//System.out.println(chip.toString());
        		System.out.println(chip.getId());
        	}
        }
    }
    public static void main(String[] args) throws DataAccessException {
        String fileName = "disabled_2011_03_18.txt";
        String fileDir = System.getenv("HOME") + "/data/FEBsetup";
        File inputFile =  null;
        
        inputFile =  new File(fileDir + "/" + fileName);
        if(inputFile.canRead() ==  false) {
            System.err.println("the file:\n" + fileName + "cannot be opened");
            return;
        }
        
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);	 

        try {
        	//LocalConfigKey inputConfigKey = configurationManager.getLocalConfigKey(configurationManager.LOCAL_CONF_KEY_FEBS_DEFAULT);
        	//LocalConfigKey outputConfigKey = configurationManager.getLocalConfigKey(configurationManager.LOCAL_CONF_KEY_FEBS_DEFAULT);
        	//putDiasblesFromFile(true, inputFile, equipmentDAO, configurationManager, configurationDAO);
                     
        	printDisabledFebs(configurationDAO);
        } 
       /* catch (DataAccessException e) {
            e.printStackTrace();            
            context.rollback();
            System.out.println("context.rollback()");
        } */
        /*  catch (FileNotFoundException e) {            
            e.printStackTrace();
        }*/
        catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getMessage());
            context.rollback();
            System.out.println("context.rollback()");
        } 
        finally {
            context.closeSession();
        }
    }

}
