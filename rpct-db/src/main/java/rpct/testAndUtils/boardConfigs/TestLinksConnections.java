package rpct.testAndUtils.boardConfigs;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import rpct.datastream.TestOptionsXml;
import rpct.datastream.TestOptionsXml.BoardSource;
import rpct.datastream.TestOptionsXml.OpticalLink;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.equipment.RbcBoard;
import rpct.db.domain.equipment.RbcTtuConn;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbOpto;
import rpct.xdaqaccess.hardwaredb.HardwareDbSynCoder;

public class TestLinksConnections {
	/**
	 * @param args
	 * @throws DataAccessException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws DataAccessException, IOException {
		HibernateContext context = new SimpleHibernateContextImpl();
		EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
		//HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
		ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
		ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);	 
		HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);

		boolean onlyEnabledLBs = true;
		if(args.length > 0) {
			if(args[0].equals("-onlyEnabledLBs=false"))
				onlyEnabledLBs = false;
		}
		//printLinks(false, equipmentDAO, configurationManger);   

		Map<Integer, LinkBoard> lbMap = new TreeMap<Integer, LinkBoard>();
		Set<Chip> optos = new TreeSet<Chip>();
		Map<Chip, HardwareDbOpto> hdbOptos = new HashMap<Chip, HardwareDbOpto>();

		try { 
			//String fileDir = System.getenv("HOME") + "/bin/config/enableLinks.xml";
			String fileDir = System.getenv("HOME") + "/bin/config/testLinksConnections.xml";
			System.out.println("testing the connections for links from file "+ fileDir); 

			//TestOptionsXml testOptionsXml = TestOptionsXml.readFromFile(System.getenv("HOME") + "/bin/config/enableLinks.xml");
			TestOptionsXml testOptionsXml = TestOptionsXml.readFromFile(fileDir);
			TestOptionsXml.SourcesList sourcesList = testOptionsXml.getSourcesList("linkConnectionTest");                      

			List<BoardSource> boardSources = sourcesList.getBoardSources();
			for(BoardSource boardSource : boardSources) {
				Set<Board> boards = testOptionsXml.getBoards(boardSource, equipmentDAO, onlyEnabledLBs);
				/*
				 * jesli ja,is LB jest diasbled, to jesli onlyEnabledLBs = true nie pojawi sie w boards. oznacza to, ze nie bedzie go mozna
				 * ani zdisablowac, ani zenablowac
				 */
				for(Board board : boards) {
					if(board.getType() == BoardType.LINKBOARD) {
						lbMap.put(board.getId(), (LinkBoard)board);                		                	
					}                	
					else {
						throw new IllegalArgumentException("the board is not LinkBoard");
					}
				}
			}

			for(OpticalLink opticalLink : testOptionsXml.getOpticalLinks()) {
				TriggerBoard triggerBoard = (TriggerBoard)equipmentDAO.getBoardByName(opticalLink.getTbName());
				if(triggerBoard == null) {
					throw new IllegalArgumentException("TriggerBoard with name " + opticalLink.getTbName() + " not found");
				}
				LinkConn linkConn = triggerBoard.getLinkConn(opticalLink.getTbInput());
				if(linkConn == null) {
					throw new IllegalArgumentException("TriggerBoard with name " + opticalLink.getTbName() + " has no link for input " + opticalLink.getTbInput());
				}

				if(onlyEnabledLBs) {
					if(linkConn.getBoard().getDisabled() == null && linkConn.getDisabled() == null) {
						lbMap.put(linkConn.getBoard().getId(), (LinkBoard)linkConn.getBoard());                          	
					}
				}
				else {
					lbMap.put(linkConn.getBoard().getId(), (LinkBoard)linkConn.getBoard()); 
				}
				//System.out.println("enablig " + linkConn);              
			}
					
			for (Map.Entry<Integer, LinkBoard> lbIt : lbMap.entrySet()) {            	
				//HardwareDbSynCoder syncoder = (HardwareDbSynCoder)dbMapper.getChip(lbIt.getValue().getSynCoder());            	
				for(LinkConn  linkConn : lbIt.getValue().getLinkConns()) {
					optos.add(linkConn.getOpto());
				}            	
			}

			for(Chip opto : optos) {
				HardwareDbOpto hdbOpto = (HardwareDbOpto)dbMapper.getChip(opto);
				hdbOptos.put(opto, hdbOpto);
			}

			System.out.println("selected links connections:");
			for (Map.Entry<Integer, LinkBoard> lbIt : lbMap.entrySet()) {            	          	
				for(LinkConn  linkConn : lbIt.getValue().getLinkConns()) {
					HardwareDbOpto hdbOpto = hdbOptos.get(linkConn.getOpto());
					System.out.println(linkConn.getBoard().getName() + " -> " +hdbOpto.getBoard().getFullName() + " optInput" + linkConn.getTriggerBoardInputNum());
				}
			}
			
			String r_s = "y";
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			while(true) {
				System.out.println("configuring LBs");
				for (Map.Entry<Integer, LinkBoard> lbIt : lbMap.entrySet()) {            	
					HardwareDbSynCoder syncoder = (HardwareDbSynCoder)dbMapper.getChip(lbIt.getValue().getSynCoder());
					syncoder.enableTransmissionCheck(false, false);
					syncoder.enableTest(1, 0);
					syncoder.getHardwareChip().getItem("SEND_TEST_DATA").write(lbIt.getKey());
				}
				
				System.out.println("configuring OPTOs");
				for(Map.Entry<Chip, HardwareDbOpto> hdbOpto : hdbOptos.entrySet()) {
					hdbOpto.getValue().setUsedOptLinksInputsMaskFromHardware();
					hdbOpto.getValue().enableRecTransCheck(false, false);
					//hdbOpto.getValue().getHardwareChip().getItem("REC_TEST_ENA").write(0);
				}


				System.out.println("Checking OPTOs");
				int tested = 0;
				int notOK = 0;
				for (Map.Entry<Integer, LinkBoard> lbIt : lbMap.entrySet()) {            	
					//HardwareDbSynCoder syncoder = (HardwareDbSynCoder)dbMapper.getChip(lbIt.getValue().getSynCoder());            	
					for(LinkConn  linkConn : lbIt.getValue().getLinkConns()) {
						if(onlyEnabledLBs) {
							if(linkConn.getBoard().getDisabled() != null || linkConn.getDisabled() != null) {
								continue;                          	
							}
						}
						HardwareDbOpto hdbOpto = hdbOptos.get(linkConn.getOpto());
						tested++;
						long testData = ((rpct.xdaqaccess.DeviceItemWord)(hdbOpto).getHardwareChip().getItem("REC_TEST_DATA")).read(linkConn.getOptoInputNum());						
						if(testData != lbIt.getKey()) {
							notOK++;
							System.out.print("testing " + linkConn.getBoard().getName() + " -> " +hdbOpto.getBoard().getFullName() + " optInput" + linkConn.getTriggerBoardInputNum());
							System.out.println();
							//checking if the link is sumchronized
							long synchAck = ((HardwareDbChip) hdbOpto).getHardwareChip().getItem("STATUS.REC_SYNCH_ACK").read();
							long tlkRxError = ((HardwareDbChip) hdbOpto).getHardwareChip().getItem("TLK.RX_ERROR").read();     

							long thisSynchAck = (synchAck & (1l << linkConn.getOptoInputNum()) );                        
							long thisTlkRxError = (tlkRxError & (1l << linkConn.getOptoInputNum()) );

							System.out.print(hdbOpto.getBoard().getFullName() + " optInput" + linkConn.getTriggerBoardInputNum() + ": ");

							if(thisSynchAck == 0 || thisTlkRxError != 0) {
								System.out.print("TlkRxError = " + thisTlkRxError + " SynchAck = " + thisSynchAck + "\n");
							}
							else {
								LinkBoard foundLB = lbMap.get((int)testData);
								if(foundLB != null) {
									System.out.print(" should be the " + linkConn.getBoard().getName() + " but found " + foundLB.getName() + "\n");
								}
								else {
									System.out.print(" REC_TEST_DATA = 0x" + Long.toHexString(testData) + " not crrespsonds to any LB ID " + "\n");
								}
							}
							System.out.println();
						}   
						else {
							//System.out.println(" - OK");
						}
					}            	
				}
								
				System.out.println("cleannig LBs");
				for (Map.Entry<Integer, LinkBoard> lbIt : lbMap.entrySet()) {            	
					HardwareDbSynCoder syncoder = (HardwareDbSynCoder)dbMapper.getChip(lbIt.getValue().getSynCoder());
					syncoder.enableTransmissionCheck(true, true);
					syncoder.enableTest(0, 0);
					//syncoder.getHardwareChip().getItem("SEND_TEST_DATA").write(lbIt.getKey());
				}

				System.out.println("cleanig OPTOs");
				for(Map.Entry<Chip, HardwareDbOpto> hdbOpto : hdbOptos.entrySet()) {
					hdbOpto.getValue().enableRecTransCheck(true, true);
					//hdbOpto.getValue().getHardwareChip().getItem("REC_TEST_ENA").write(0);
				}

				System.out.println("synchronizing links");
				for (Map.Entry<Integer, LinkBoard> lbIt : lbMap.entrySet()) {            	
					HardwareDbSynCoder syncoder = (HardwareDbSynCoder)dbMapper.getChip(lbIt.getValue().getSynCoder());
					syncoder.synchronizeLink();
				}
				Thread.sleep(500);
				for (Map.Entry<Integer, LinkBoard> lbIt : lbMap.entrySet()) {            	
					HardwareDbSynCoder syncoder = (HardwareDbSynCoder)dbMapper.getChip(lbIt.getValue().getSynCoder());
					syncoder.normalLinkOperation();
				}

				System.out.println("tested links connections: " + tested + ", not OK: " + notOK);
				System.out.println("repeat (y/n)?");
				r_s = br.readLine();

				if (r_s.equals("n") == true) {
					break;
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			context.rollback();
		}
		finally {
			context.closeSession();
		}
	}
}
