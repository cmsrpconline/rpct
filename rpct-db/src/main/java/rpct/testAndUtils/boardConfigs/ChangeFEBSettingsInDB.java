package rpct.testAndUtils.boardConfigs;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ChipConfAssignment;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.FebChipConf;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.LocalConfigKey.Subsystem;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.FebBoard;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;

public class ChangeFEBSettingsInDB {

    static void loadSettingsFromFile(boolean saveToDb
                                     , boolean updateOffsets
                                     , boolean updateThreshold
                                     , File inputFile
                                     , EquipmentDAO equipmentDAO
                                     , ConfigurationManager configurationManager
                                     , ConfigurationDAO configurationDAO
                                     , LocalConfigKey inputConfigKey
                                     , LocalConfigKey outputConfigKey) throws DataAccessException, FileNotFoundException
    {
        Scanner scanner = new Scanner(inputFile);

        System.out.println("Load settings from file " + inputFile.getPath());

        while(scanner.hasNextLine()) {
            if(scanner.hasNext() == false)
                break;

            String connectorName = scanner.next();
            int chip   = scanner.nextInt();
            int chipId = scanner.nextInt();
            int vthOffsetUsed = scanner.nextInt();
            int vthOffsetRead = scanner.nextInt();
            int vmonOffsetUsed = scanner.nextInt();
            int vmonOffsetRead = scanner.nextInt();
            int vthRefRate = scanner.nextInt();
            if(scanner.hasNext())
            	scanner.nextLine();

            Chip febChip = equipmentDAO.getChipById(chipId);
            if (febChip.getType() != ChipType.FEBCHIP) {
                System.err.println("Chip with invalid type " + febChip.getType() + ", id " + chipId);
                return;
            }

            // FebBoard febBoard = (FebBoard)febChip.getBoard();
            FebBoard febBoard = equipmentDAO.getFebBoard(febChip.getBoard().getId());
            String febName = febBoard.getFebLocation().getChamberLocation().getChamberLocationName() + ":" +
                febBoard.getFebLocation().getFebLocalEtaPartition() + "_" +
                febBoard.getCbChannel() + "." + febBoard.getI2cLocalNumber();

            FebChipConf newConf;
            try { // Get Existing
                FebChipConf oldConf = (FebChipConf)configurationManager.getConfiguration(febChip, inputConfigKey);
                newConf = new FebChipConf(oldConf);
                if (updateOffsets) {
                    newConf.setVthOffset(vthOffsetUsed);
                    newConf.setVmonOffset(vmonOffsetUsed);
                }
                if (updateThreshold)
                    newConf.setVth(vthRefRate);
            } catch(NullPointerException error) { // Or Create New
                newConf = new FebChipConf(vthRefRate, vthOffsetUsed, 1434, vmonOffsetUsed);
            }

            System.out.format("%-24s %-24s %10d %10d %10d %10d %10d\n", connectorName, febName, chipId, newConf.getVth(), newConf.getVthOffset(), newConf.getVmon(), newConf.getVmonOffset());

            if(saveToDb) {
                System.out.println("Saving to DB");
                configurationDAO.saveObject(newConf);
                configurationManager.assignConfiguration(febChip, newConf, outputConfigKey.getName());
            }
        }
    }

    public static void main(String[] args) throws DataAccessException
    {
        //String location = "/";
        //if (args.length > 0)
        //    location = args[0] + "/";

        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);

        try {
            LocalConfigKey inputConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_FEBS_DEFAULT);
            LocalConfigKey outputConfigKey = configurationManager.getLocalConfigKey(configurationManager.LOCAL_CONF_KEY_FEBS_DEFAULT);

            //String[] towers = {"RB0_Far", "RB0_Near"
            //                   , "RBN1_Far", "RBN1_Near"
            //                   , "RBN2_Far", "RBN2_Near"
            //                   , "RBP1_Far", "RBP1_Near"
            //                   , "RBP2_Far", "RBP2_Near"
            //                   , "YEN1_Far", "YEN1_Near"
            //                   , "YEN3_Far", "YEN3_Near"
            //                   , "YEP1_Far", "YEP1_Near"
            //                   , "YEP3_Far", "YEP3_Near"};

            List<File> inputFiles =  new ArrayList<File>();

            //for (String tower : towers) {
            //    String fileName = System.getenv("HOME") + "/bin/data/FEBConfigs/" + location + tower + "-config.txt";
            for (String fileName : args) {
                File inputFile = new File(fileName);

                if(inputFile.canRead() ==  false) {
                    System.err.println("the file " + fileName + " cannot be opened.");
                    return;
                }

                inputFiles.add(inputFile);
            }

            for (File inputFile : inputFiles) {
                loadSettingsFromFile(true, true, false, inputFile,  equipmentDAO, configurationManager, configurationDAO, inputConfigKey, outputConfigKey);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getMessage());
            context.rollback();
            System.out.println("context.rollback()");
        } finally {
            context.closeSession();
        }
    }
}
