package rpct.testAndUtils.boardConfigs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;

public class EnableDevices {

    /**
     * @param args
     * @throws DataAccessException 
     * @throws IOException 
     */
    public static void main(String[] args) throws DataAccessException, IOException {
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);	 


        try {
            /*
			for (Crate crate : equipmentDAO.getCrates()) {
				System.out.println(crate.getName());
				for (Board board : crate.getBoards()) {
					//System.out.println("  " + board.getName() + (board.getDisabled() == null ? "" : " DISABLED"));
					if(board.getDisabled() != null) {
						System.out.println("  " + board.getName() + " DISABLED");
					}
				}
			}*/


            /*			Crate c = equipmentDAO.getCrateByName("LBB_RB+1_S11");
			for(Board b : c.getBoards()) {
				if(b.getPosition() >= 14) {
					System.out.println(c.getName() + " " + b.getName() + " - diasable?");
			        char yn = 'y';
			        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			        yn = (char) br.read();  
			        if(yn == 'y') {
			        	configurationManger.enableBoard(b, false);		
			        	System.out.println("done");
			        }
				}				
			}*/


            //configurationManger.enableBoard(equipmentDAO.getBoardByName("LB_RB+1_S2_BP1B_CH0"), true);

            /*			for(Board b : c.getBoards()) {
			System.out.println("  " + b.getName() + (b.getDisabled() == null ? "" : " DISABLED"));			



		}*/
            /*		    for(Chip chip : equipmentDAO.getChipsByTypeAndBoard(ChipType.PAC, equipmentDAO.getBoardByName("TBp1_1"), true)) {
		        if(chip.getPosition() == 11)
		            configurationManger.enableChip(chip, false);
		    }
		    for(Chip chip : equipmentDAO.getChipsByTypeAndBoard(ChipType.PAC, equipmentDAO.getBoardByName("TBn1_1"), true)) {
                if(chip.getPosition() == 11)
                    configurationManger.enableChip(chip, false);
            }
		    configurationManger.enableCrate(equipmentDAO.getCrateByName("TC_1"), true);
             */
            //	   	configurationManger.enableCrate(equipmentDAO.getCrateByName("TC_10"), false);

         /*   configurationManger.enableBoard(equipmentDAO.getBoardByName("TBn3_11"), true);
            configurationManger.enableBoard(equipmentDAO.getBoardByName("TBn2_11"), true);
            configurationManger.enableBoard(equipmentDAO.getBoardByName("TBn1_11"), true);//<<<<<<
            configurationManger.enableBoard(equipmentDAO.getBoardByName("TB0_11"), true);
            configurationManger.enableBoard(equipmentDAO.getBoardByName("TBp1_11"), true);
            configurationManger.enableBoard(equipmentDAO.getBoardByName("TBp2_11"), true);
            configurationManger.enableBoard(equipmentDAO.getBoardByName("TBp3_11"), true);*/

            
		  /*  configurationManger.enableBoard(equipmentDAO.getBoardByName("TBn3_10"), true);
		    configurationManger.enableBoard(equipmentDAO.getBoardByName("TBn2_10"), true);
		    configurationManger.enableBoard(equipmentDAO.getBoardByName("TBn1_10"), true);
		    configurationManger.enableBoard(equipmentDAO.getBoardByName("TB0_10"), true);
		    configurationManger.enableBoard(equipmentDAO.getBoardByName("TBp1_10"), true);
		    configurationManger.enableBoard(equipmentDAO.getBoardByName("TBp2_10"), true);
		    configurationManger.enableBoard(equipmentDAO.getBoardByName("TBp3_10"), true);*/

            /*		    configurationManger.enableBoard(equipmentDAO.getBoardByName("TBn3_9"), true);
		    configurationManger.enableBoard(equipmentDAO.getBoardByName("TBn2_9"), true);
		    configurationManger.enableBoard(equipmentDAO.getBoardByName("TBn1_9"), true);
		    configurationManger.enableBoard(equipmentDAO.getBoardByName("TB0_9"), true);
		    configurationManger.enableBoard(equipmentDAO.getBoardByName("TBp1_9"), true);
		    configurationManger.enableBoard(equipmentDAO.getBoardByName("TBp2_9"), true);
		    configurationManger.enableBoard(equipmentDAO.getBoardByName("TBp3_9"), true);*/

            //	    configurationManger.enableCrate(equipmentDAO.getCrateByName("TC_11"), true);

            //configurationManger.enableBoard(equipmentDAO.getBoardByName("HSB_0"), true);

/*            configurationManger.enableBoard(equipmentDAO.getBoardByName("TBn3_1"), true);
            configurationManger.enableBoard(equipmentDAO.getBoardByName("TBn2_1"), true);
            configurationManger.enableBoard(equipmentDAO.getBoardByName("TBn1_1"), true);//<<<<<<
            configurationManger.enableBoard(equipmentDAO.getBoardByName("TB0_1"), true);
            configurationManger.enableBoard(equipmentDAO.getBoardByName("TBp1_1"), true);
            configurationManger.enableBoard(equipmentDAO.getBoardByName("TBp2_1"), true);
            configurationManger.enableBoard(equipmentDAO.getBoardByName("TBp3_1"), true);*/

            /*configurationManger.enableBoard(equipmentDAO.getBoardByName("TBn3_4"), false);
            configurationManger.enableBoard(equipmentDAO.getBoardByName("TBn2_4"), false);
            configurationManger.enableBoard(equipmentDAO.getBoardByName("TBn1_4"), false);
            configurationManger.enableBoard(equipmentDAO.getBoardByName("TB0_4"), false);
            configurationManger.enableBoard(equipmentDAO.getBoardByName("TBp1_4"), false);
            configurationManger.enableBoard(equipmentDAO.getBoardByName("TBp2_4"), false);
            configurationManger.enableBoard(equipmentDAO.getBoardByName("TBp3_4"), false);*/

//            for(Board b : equipmentDAO.getBoardsByType(BoardType.TTUBOARD, false)) {
//		        configurationManger.enableBoard(b, false);
//		    }
//          configurationManger.enableBoard(equipmentDAO.getBoardByName("TBn3_9"), false);
//          configurationManger.enableBoard(equipmentDAO.getBoardByName("TBn2_9"), false);
//          configurationManger.enableBoard(equipmentDAO.getBoardByName("TBn1_9"), false);
//          configurationManger.enableBoard(equipmentDAO.getBoardByName("TB0_9"), true);
//          configurationManger.enableBoard(equipmentDAO.getBoardByName("TBn3 "), true);
//          configurationManger.enableBoard(equipmentDAO.getBoardByName("TBn2"), true);

//          configurationManger.enableBoard(equipmentDAO.getBoardByName("TBn1"), true);
//          configurationManger.enableBoard(equipmentDAO.getBoardByName("TB0"), true);
//          configurationManger.enableBoard(equipmentDAO.getBoardByName("TBp1"), true);
//          configurationManger.enableBoard(equipmentDAO.getBoardByName("TBp2"), true);


//          configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_RB+1_S1"), true);
//          configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_RB+1_S2"), true);
//          configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_RB+1_S3"), true);			
//          configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_RB+1_S12"), true);

//          configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_RB+2_S11"), true);

//          configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_RB+2_S1"), false);
//          configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_RB+2_S2"), true);
//          configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_RB+2_S3"), true);         
//          configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_RB+2_S12"), false);
            //configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_RB+2_S8"), false);



/*            configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_RB+1_S1"), true);
			configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_RB+1_S2"), true);
			configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_RB+1_S3"), true);
			configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_RB+1_S10"), true);
            configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_RB+1_S11"), true); 
			configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_RB+1_S12"), true);*/

            /*			 configurationManger.enableLinkConn((TriggerBoard) equipmentDAO.getBoardByName("TBp1_9"), 5, true); 
		     configurationManger.enableLinkConn((TriggerBoard) equipmentDAO.getBoardByName("TBp1_9"), 6, true);
		     configurationManger.enableLinkConn((TriggerBoard) equipmentDAO.getBoardByName("TBp1_9"), 11, true);
		     configurationManger.enableLinkConn((TriggerBoard) equipmentDAO.getBoardByName("TBp1_9"), 12, true);

		     configurationManger.enableLinkConn((TriggerBoard) equipmentDAO.getBoardByName("TBp2_9"), 0, true);
             configurationManger.enableLinkConn((TriggerBoard) equipmentDAO.getBoardByName("TBp2_9"), 1, true);
             configurationManger.enableLinkConn((TriggerBoard) equipmentDAO.getBoardByName("TBp2_9"), 2, true);
             configurationManger.enableLinkConn((TriggerBoard) equipmentDAO.getBoardByName("TBp2_9"), 3, true);
             configurationManger.enableLinkConn((TriggerBoard) equipmentDAO.getBoardByName("TBp2_9"), 4, true);

		     configurationManger.enableLinkConn((TriggerBoard) equipmentDAO.getBoardByName("TBp2_9"), 13, true);
		     configurationManger.enableLinkConn((TriggerBoard) equipmentDAO.getBoardByName("TBp2_9"), 14, true);
		     configurationManger.enableLinkConn((TriggerBoard) equipmentDAO.getBoardByName("TBp2_9"), 15, true);
		     configurationManger.enableLinkConn((TriggerBoard) equipmentDAO.getBoardByName("TBp2_9"), 16, true);
		     configurationManger.enableLinkConn((TriggerBoard) equipmentDAO.getBoardByName("TBp2_9"), 17, true);*/
            
//            configurationManger.enableChip( ((TriggerBoard)equipmentDAO.getBoardByName("TBn2_8")).getRmb(), true);
//            configurationManger.enableChip( ((TriggerBoard)equipmentDAO.getBoardByName("TBp2_8")).getRmb(), true);
            
           // configurationManger.enableChip( ((TriggerBoard)equipmentDAO.getBoardByName("TBn2_0")).getRmb(), true);
           // configurationManger.enableBoard(equipmentDAO.getBoardByName("TBn2_0"), true);
/*            configurationManger.enableBoard(equipmentDAO.getBoardByName("LB_RE+3_S12_EP33_CH0"), true);
            configurationManger.enableBoard(equipmentDAO.getBoardByName("LB_RE+3_S12_EP33_CH1"), true);
            configurationManger.enableBoard(equipmentDAO.getBoardByName("LB_RE+3_S12_EP33_CH2"), true);*/
            
/*            configurationManger.enableBoard(equipmentDAO.getBoardByName("LB_RB0_S12_BN0B_CH0"), true);
            configurationManger.enableBoard(equipmentDAO.getBoardByName("LB_RB0_S12_BN0B_CH1"), true);
            configurationManger.enableBoard(equipmentDAO.getBoardByName("LB_RB0_S12_BN0B_CH2"), true);                   
            for(LinkConn linkConn : ((LinkBoard)equipmentDAO.getBoardByName("LB_RB0_S12_BN0B_CH0")).getLinkConns() ) {
                configurationManger.enableLinkConn(linkConn, true);
            }
            
            configurationManger.enableBoard(equipmentDAO.getBoardByName("LB_RB+2_S5_BP2D_CH0"), true);
            //configurationManger.enableBoard(equipmentDAO.getBoardByName("LB_RB+2_S5_BP2D_CH1"), false);
            configurationManger.enableBoard(equipmentDAO.getBoardByName("LB_RB+2_S5_BP2D_CH2"), true);                   
            for(LinkConn linkConn : ((LinkBoard)equipmentDAO.getBoardByName("LB_RB+2_S5_BP2D_CH0")).getLinkConns() ) {
                configurationManger.enableLinkConn(linkConn, true);
            }

            configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_RB0_S11"), true);*/
            
/*            for(Board b : equipmentDAO.getCrateByName("LBB_YEP1_S1").getBoards() ) {
               System.out.println(b.getName() + " " + b.getDisabled());                
            }*/
            
/*            LinkBox linkBox = (LinkBox)equipmentDAO.getCrateByName("LBB_RB+1_S11");
            configurationManger.enableHalfBox(linkBox, 10,  true);*/
            
/*            for(Chip chip : equipmentDAO.getChipsByTypeAndBoard(ChipType.PAC, equipmentDAO.getBoardByName("TBp1_7"), false)) {
                if(chip.getPosition() == 9)
                    configurationManger.enableChip(chip, true);
            }*/
             
/*            configurationManager.enableCrate(equipmentDAO.getCrateByName("LBB_RB-2_S10"), true);
            configurationManager.enableCrate(equipmentDAO.getCrateByName("LBB_RB-2_S11"), true);
            configurationManager.enableCrate(equipmentDAO.getCrateByName("LBB_RB-2_S12"), true);
            configurationManager.enableCrate(equipmentDAO.getCrateByName("LBB_RB-2_S3"), true);
            configurationManager.enableCrate(equipmentDAO.getCrateByName("LBB_RB-2_S2"), true);
            configurationManager.enableCrate(equipmentDAO.getCrateByName("LBB_RB-2_S1"), true);*/
            
  /*          configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_RB-2_S9"), true);
            configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_RB-2_S8"), true);
            configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_RB-2_S7"), true);
            configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_RB-2_S6"), true);
            configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_RB-2_S5"), true);
            configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_RB-2_S4"), true);
            */
/*
            configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_YEN1_S10"), true);
            configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_YEN1_S11"), true);
            configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_YEN1_S12"), true);
            configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_YEN1_S3"), true);
            configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_YEN1_S2"), true);
            configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_YEN1_S1"), true);
            
            configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_YEN1_S9"), true);
            configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_YEN1_S8"), true);
            configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_YEN1_S7"), true);
            configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_YEN1_S6"), true);
            configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_YEN1_S5"), true);
            configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_YEN1_S4"), true);

            configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_YEN3_S12/1"), true);
            configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_YEN3_S2/3"), true);
            configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_YEN3_S4/5"), true);
            configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_YEN3_S6/7"), true);
            configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_YEN3_S8/9"), true);
            configurationManger.enableCrate(equipmentDAO.getCrateByName("LBB_YEN3_S10/11"), true);
*/            
/*            
            configurationManger.enableCrate(equipmentDAO.getCrateByName("TC_4"), true);
*/
            //EnableLinks.printLinksToFile(equipmentDAO);
        	configurationManager.enableChip(equipmentDAO.getChipById(1838), true);
        	
            
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            context.rollback();
        }
        finally {
            context.closeSession();
        }
    }

}
