package rpct.testAndUtils.boardConfigs;

import java.util.List;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.SynCoderConfInFlash;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;

public class ClearSynCoderConfInFlash {
	public static void main(String[] args) throws DataAccessException {
		HibernateContext context = new SimpleHibernateContextImpl();
		ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context); 

		try {
			@SuppressWarnings("unchecked")
			List<SynCoderConfInFlash> synCoderConfInFlashList = configurationDAO.getObjects(SynCoderConfInFlash.class);

			for(SynCoderConfInFlash synCoderConfInFlash : synCoderConfInFlashList) {
				synCoderConfInFlash.setSynCoderConfId(null);
				configurationDAO.saveObject(synCoderConfInFlash);
			}
		}
		catch (DataAccessException e) {
			e.printStackTrace();
			context.rollback();
		} 
		finally {
			context.closeSession();
		}
	}
}
