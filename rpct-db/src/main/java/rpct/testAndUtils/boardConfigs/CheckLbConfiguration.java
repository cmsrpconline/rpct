package rpct.testAndUtils.boardConfigs;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

//import javax.xml.rpc.ServiceException;

import rpct.datastream.TestPresets;
import rpct.datastream.TestPulsesGenerator;
import rpct.datastream.TestsManager;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.SynCoderConf;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.diag.Pulser;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbCrate;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbOpto;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;
import rpct.xdaqaccess.synchro.DefOut;
import rpct.xdaqaccess.synchro.LVDSLink;
import rpct.xdaqaccess.synchro.LVDSLinksSynchronizer;
import rpct.xdaqaccess.synchro.LVDSTransmDevice;

public class CheckLbConfiguration {

	public static void main(String[] args) throws DataAccessException {
		HibernateContext context = new SimpleHibernateContextImpl();
		EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
		ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
		HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
		ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);

		try {             
			List<HardwareDbCrate> crates = new ArrayList<HardwareDbCrate>();
			List<Board> linkBoards = equipmentDAO.getBoardsByType(BoardType.LINKBOARD, true);

			int checked = 0;
			int badCnt = 0;
			for(Board board : linkBoards) {
				if(board.getName().contains("RB0")) 
				{
					//System.out.print(board.getName() + " ");
					HardwareDbBoard hdbBoard = dbMapper.getBoard(board);
					SynCoderConf synCoderConf = (SynCoderConf)configurationManager.getConfiguration(board.getChips().iterator().next(), configurationManager.getLocalConfigKey("DEFAULT"));

					synCoderConf.getBcn0Delay();
					synCoderConf.getLmuxInDelay();
					synCoderConf.getOptLinkSendDelay();
					synCoderConf.isInvertClock();

					if(((LinkBoard)board).isMaster()) {

					}
					boolean bad = false;
					long bcn0Delay = hdbBoard.getHardwareBoard().getDevices()[0].getItem("BCN0_DELAY").read();
					if(bcn0Delay != synCoderConf.getBcn0Delay()) {
						System.out.println(board.getName() + " BCN0_DELAY not correct. Is " + bcn0Delay + " should be " + synCoderConf.getBcn0Delay());
						bad = true;
					}
					long optLinkSendDelay = hdbBoard.getHardwareBoard().getDevices()[0].getItem("SEND_DELAY").read();
					if(optLinkSendDelay != synCoderConf.getOptLinkSendDelay()) {
						System.out.println(board.getName() + " SEND_DELAY not correct. Is " + optLinkSendDelay + " should be " + synCoderConf.getOptLinkSendDelay());
						bad = true;
					}

					long invertClock = hdbBoard.getHardwareBoard().getDevices()[0].getItem("STATUS1.INV_CLOCK").read();
					if( (invertClock == 1) != synCoderConf.isInvertClock()) {
						System.out.println(board.getName() + " INV_CLOCK not correct. Is " + invertClock + " should be " + synCoderConf.isInvertClock());
						bad = true;
					}
					checked ++;
					if(bad)
						badCnt++;
				}
			}

			System.out.println("checked " + checked + " bad " + badCnt);
		} 
		catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			context.closeSession();
		}
	}  
}
