package rpct.testAndUtils.boardConfigs;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.rpc.ServiceException;

import rpct.datastream.RpctDelays;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.PacConf;
import rpct.db.domain.configuration.RmbConf;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.configuration.TbGbSortConf;
import rpct.db.domain.configuration.TcSortConf;
import rpct.db.domain.configuration.LocalConfigKey.Subsystem;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.CrateWithLVDSLinks;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbMuxRecChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;

public class CopyBoardsSettings {    
	/*    public static void addBoardsSettingsToDB(List<CrateWithLVDSLinks> crates,
            HibernateContext context,
            EquipmentDAO equipmentDAO,
            HardwareDbMapper dbMapper,
            ConfigurationDAO configurationDAO,
            ConfigurationManger configurationManger) {


    }*/
	public static void main(String[] args) throws DataAccessException {
		if(args.length == 0) {            
			String[] argsL = {
					//"TC_8",
					"TC_0",                    
					"TC_1",
					"TC_2",                   
					"TC_3",                    
					"TC_4",
					"TC_5",                    
					"TC_6",                    
					"TC_7",
					"TC_8",                   
					"TC_9",                    
					"TC_10",
					"TC_11",
					//"PASTEURA_TC"
			};

			args = argsL;
		}

		boolean saveNewConfToDb = true; //TODO

		boolean updatePACsettings = false; //TODO
		boolean updateRMBsettings = true; //TODO
		boolean updateTBGBSsettings = false; //TODO
		boolean updateTCGBSsettings = false; //TODO

		boolean copyPACsettings = false; //TODO
		boolean copyRMBsettings = false; //TODO
		boolean copyTBGBSsettings = false; //TODO
		boolean copyTCGBSsettings = false; //TODO
		
		System.out.println("Selected crates");
		for(int i = 0; i < args.length; i++) {
			System.out.print(args[i] + " ");
		}

		HibernateContext context = new SimpleHibernateContextImpl();
		EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
		HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
		ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
		ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);   

		LocalConfigKey inConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_TCS_V1047); //TODO
		//LocalConfigKey outConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_TCS_V1038); //TODO
		//LocalConfigKey outConfigKey = configurationManager.createLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_TCS_V1039_1EX, Subsystem.TCS); //TODO
		//LocalConfigKey outConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_TCS_V1039_1EX); //TODO
		//LocalConfigKey outConfigKey = configurationManager.createLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_TCS_V1039_plus12BX, Subsystem.TCS); //TODO
		LocalConfigKey outConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_TCS_V1047); //TODO
		
		//RMB configuration
		int rmbDataDelay = RpctDelays.RMB_DATA_DELAY;        
		int chanEna = 0; //0 = all channels enable
		int preTriggerVal = RpctDelays.PRE_TRIGGER_VAL; //zakers BX branych do DAQ
		int postTriggerVal = RpctDelays.POST_TRIGGER_VAL; 
		int trgDelay = 0; 

		int bxOfCoincidence = 0; //TODO
		int pacEna = 0xfff;
		int pacConfigId = 0x1047; //TODO


		String bootScript = "boot_lhc_v" + Integer.toHexString(pacConfigId) + ".sh"; //TODO

		try {
			List<TriggerCrate> crates = new ArrayList<TriggerCrate>();
			for(int i = 0; i < args.length; i++) {                
				crates.add((TriggerCrate)equipmentDAO.getCrateByName(args[i]));
			}

			for(TriggerCrate tc : crates) {   
				for(TriggerBoard tb : tc.getTriggerBoards()) {
					for(Chip chip : tb.getChips()) {
						StaticConfiguration  newConf = null;
						if(chip.getDisabled() != null)
							continue;
						if( (chip.getType() == ChipType.PAC) || 
								(chip.getType() == ChipType.RMB) ||
								(chip.getType() == ChipType.GBSORT) ) {
							
							StaticConfiguration oldConf = null;
							try {
								oldConf = configurationManager.getConfiguration(chip, inConfigKey);
							}
							catch(NullPointerException e) {
								System.out.println(e.getMessage() + " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
							}
							if(RmbConf.class.isInstance(oldConf) ) {
								if(updateRMBsettings) {
									newConf = new RmbConf((RmbConf)oldConf);

									((RmbConf)newConf).setDataDelay(rmbDataDelay);
									/*((RmbConf)newConf).setChanEna(chanEna);
									((RmbConf)newConf).setPreTriggerVal(preTriggerVal);
									((RmbConf)newConf).setPostTriggerVal(postTriggerVal);
									((RmbConf)newConf).setTrgDelay(trgDelay);*/
									
									if(saveNewConfToDb)
										configurationDAO.saveObject(newConf);
									System.out.println("new configuration was created for the RMB " + chip);
								}
								else if(copyRMBsettings) {
									newConf = oldConf;
									System.out.println("old configuration will be assigned for the RMB " + chip);
								}
							}  
							else if(PacConf.class.isInstance(oldConf) ) {
								if(updatePACsettings) {
									newConf = new PacConf((PacConf)oldConf);

									((PacConf)newConf).setBxOfCoincidence(bxOfCoincidence);
									((PacConf)newConf).setPacEna(pacEna);
									((PacConf)newConf).setPacConfigId(pacConfigId);
									
									if(saveNewConfToDb)
										configurationDAO.saveObject(newConf);
									System.out.println("new configuration was created for the PAC " + chip);
								}
								else if(copyPACsettings) {
									newConf = oldConf;
									System.out.println("old configuration will be assigned for the PAC " + chip);
								}
							}
							else if(TbGbSortConf.class.isInstance(oldConf) ) {
								if(updateTBGBSsettings) {
									newConf = new TbGbSortConf((TbGbSortConf)oldConf);
									
									if(saveNewConfToDb)
										configurationDAO.saveObject(newConf);
									System.out.println("new configuration was created for the TBGBS " + chip);
								}
								else if(copyTBGBSsettings) {
									newConf = oldConf;
									System.out.println("old configuration will be assigned for the TBGBS " + chip);
								}
							}
						}

						if(saveNewConfToDb && newConf != null) {							
							configurationManager.assignConfiguration(chip, newConf, outConfigKey.getName());
							System.out.println("the configuration " + outConfigKey.getName() + " was assigned for the " + chip.getBoard().getName() + " " + chip.getType() );
						}														
					}
				}
				
				{
					Chip chip = tc.getBackplane().getTcSorter();
					TcSortConf newConf = null;
					TcSortConf oldConf = (TcSortConf)configurationManager.getConfiguration(chip, inConfigKey);
					if(updateTCGBSsettings) {
						newConf = new TcSortConf(oldConf);					
						newConf.setBootScript(bootScript);
						
						if(saveNewConfToDb)
							configurationDAO.saveObject(newConf);
						System.out.println("new configuration was created for the TCGBS " + chip);
					}
					else if(copyTCGBSsettings) {
						newConf = oldConf;
						System.out.println("old configuration will be assigned for the TCGBS " + chip);
					}
										
					if(saveNewConfToDb && newConf != null) {						
						configurationManager.assignConfiguration(chip, newConf, outConfigKey.getName());
						System.out.println("the configuration " + outConfigKey.getName() + " was added for the " + chip.getBoard().getName() + " " + chip.getType() );
					}	
				}
			}   

			//configurationManger.getLocalConfigKey(localConfigKey).getName();
			//configurationManager.assignConfigKey(localConfigKey, localConfigKey);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			context.rollback();
		}
		finally {
			context.closeSession();
		}
	}
}