package rpct.testAndUtils.boardConfigs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import javax.xml.rpc.ServiceException;

import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.BoardDisabled;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.CrateDisabled;
import rpct.db.domain.configuration.HalfSortConf;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.equipment.TcHsbConnection;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;

public class EnableHardware {

    public static String printDisabledHardware(ConfigurationDAO configurationDAO, EquipmentDAO equipmentDAO) throws DataAccessException {
        String msg = new String("All boards which are disabled now:\n");
        List<Integer> ids = configurationDAO.getDisabledBoardIds();
        for(Integer id : ids) {
            msg += equipmentDAO.getBoardById(id) + "\n";
        }

        msg +="\nAll crates which are disabled now:\n";
        ids = configurationDAO.getDisabledCrateIds();
        for(Integer id : ids) {
            msg += equipmentDAO.getCrateById(id) + "\n";
        }

        return msg;       
    }

    /**
     * @param args
     * @throws DataAccessException 
     * @throws IOException 
     */
    public static void main(String[] args) throws DataAccessException, IOException {
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);	 

        if (args.length < 2) {
            String str = printDisabledHardware(configurationDAO, equipmentDAO);
            
            System.out.println("\nSyntax: [enable/disable] name1 name2 name3 ...");
            System.out.println("no devices to enable/disable were given");
            
            System.out.println("\nThe current status in the DB:\n" + str);
            
            return;
        }

        boolean enable;
        String en = args[0];
        if (en.equalsIgnoreCase("enable")) {
            enable = true;
        }
        else if (en.equalsIgnoreCase("disable")) {
            enable = false;
        }
        else {
            System.err.println("Syntax: [enable/disable] name1 name2 name3 ...");
            return;		    
        }


        boolean linksModyfied = false;
        try {
            for (int i = 1; i < args.length; i++) {
                String name = args[i];
                if(name.contains(":")) {
                    String lboxName = name.substring(0, name.indexOf(":"));
                    int halfBoxId = Integer.parseInt(name.substring(name.indexOf(":") +1) );
                    Crate crate = equipmentDAO.getCrateByName(lboxName);
                    if(crate == null)
                        System.err.println("Could not find lbox " + lboxName);
                    
                    linksModyfied = configurationManager.enableHalfBox((LinkBox)crate, halfBoxId, enable);
                }
                else if(name.contains("_CB_")) {
                    String lboxName = name.substring(0, name.indexOf("_CB_"));
                    int halfBoxId = Integer.parseInt(name.substring(name.indexOf("_CB_") +4) );
                    Crate crate = equipmentDAO.getCrateByName(lboxName);
                    if(crate == null)
                        System.err.println("Could not find lbox " + lboxName);
                    
                    linksModyfied = configurationManager.enableHalfBox((LinkBox)crate, halfBoxId, enable);
                }
                else {
                    Crate crate = equipmentDAO.getCrateByName(name);
                    if (crate != null) {
                        if(crate.getType() == CrateType.TRIGGERCRATE) {
                            boolean wasChanged = true;
                                                        
                            CrateDisabled crateDisabled = crate.getDisabled();
                            //BoardDisabled crateDisabled = ((TriggerCrate)crate).getBackplane().getDisabled();
                            if (!enable && crateDisabled == null) {
                                wasChanged = true;
                            } else if (enable && crateDisabled != null) {
                                wasChanged = true;
                            }                            
                            configurationManager.enableCrate(crate, enable);
                            /*for(Board board : crate.getBoards()) {
                                configurationManger.enableBoard(board, enable);
                            }*/ //kazda z opcji powoduje, ze xdaq sie wykrzacza*/
                            
                            if(wasChanged) {
                                System.out.println("the selected crate " + name + " is a TRIGGERCRATE. The HSB configuration data will be updated to enable/disable the inputs from that crate." );
                                TriggerCrate tc = (TriggerCrate)crate;
                                List<TcHsbConnection> tcHsbConnections = tc.getTcHsbConnections(equipmentDAO);
                                for(TcHsbConnection tcHsbConnection : tcHsbConnections) {
                                    HalfSortConf currentConf = (HalfSortConf) configurationManager.getConfiguration(tcHsbConnection.getHalfSort(), configurationManager.getDefaultLocalConfigKey());
                                    System.out.println("Current config " + tcHsbConnection.getHalfSort().getBoard().getName() + " recChanEna " 
                                            + Integer.toHexString(currentConf.getRecChanEnaMask()));
                      
                                    int recChanEnaMask = currentConf.getRecChanEnaMask();

                                    if(enable)
                                        recChanEnaMask |= 3 << tcHsbConnection.getCableInputNum();
                                    else 
                                        recChanEnaMask &=  ~(3 << tcHsbConnection.getCableInputNum());

                                    HalfSortConf newConf = new HalfSortConf(currentConf);
                                    newConf.setRecChanEna(recChanEnaMask);
                                    configurationDAO.saveObject(newConf);
                                    configurationManager.assignConfiguration(tcHsbConnection.getHalfSort(), newConf, ConfigurationManager.LOCAL_CONF_KEY_SC_FULL);
                                    configurationManager.assignConfiguration(tcHsbConnection.getHalfSort(), newConf, ConfigurationManager.LOCAL_CONF_KEY_SC_BOTTOM);
                                    System.out.println("New config " + tcHsbConnection.getHalfSort().getBoard().getName() + " recChanEna " 
                                            + Integer.toHexString(newConf.getRecChanEnaMask()));

                                }
                            }
                        }
                        else if(crate.getType() == CrateType.LINKBOX) {                            
                            linksModyfied = configurationManager.enableLinkBox((LinkBox)crate, enable);                            
                        }
                        else 
                            configurationManager.enableCrate(crate, enable);             
                    }
                    else {
                        Board board = equipmentDAO.getBoardByName(name);
                        if (board != null) {
                            if(board.getCrate().getDisabled() != null) {
                                System.err.println("the selected board " + name + " is in  crate " + board.getCrate().getName() + " which is disabled now. Connot enable or disbale that board");
                            }
                            else {
                                if(board.getType() == BoardType.LINKBOARD) {                                                                        
                                    if( ((LinkBoard)board).isMaster())
                                        linksModyfied = configurationManager.enableLinkBoard((LinkBoard)board, enable);
                                    else
                                        configurationManager.enableLinkBoard((LinkBoard)board, enable);
                                }
                                else
                                    configurationManager.enableBoard(board, enable);
                            }
                        }
                        else {
                            System.err.println("Could not find " + name);                   
                        }
                    } 
                }
            }	

            System.out.println("");

            System.out.println(printDisabledHardware(configurationDAO, equipmentDAO));
            
            if(linksModyfied) {
                EnableLinks.printLinksToFile(equipmentDAO, configurationDAO);
            }
        } 
        catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            context.rollback();
        } catch (ServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            context.closeSession();
        }
    }

}
