package rpct.testAndUtils.boardConfigs;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigKey;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.HalfSortConf;
import rpct.db.domain.configuration.IntArray16;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.configuration.LocalConfigKey.Subsystem;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.TcHsbConnection;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbCrate;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbMuxRecChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;

public class AddHSBSettingsToDB1 {
    public static void main(String[] args) throws DataAccessException {
        if(args.length == 0) {            
            String[] argsL = {
                    "TC_0", "0",
                    "TC_1", "0",
                    "TC_2", "0",
                    "TC_3", "0",
                    "TC_4", "0",
                    "TC_5", "0",
                    "TC_6", "0",
                    "TC_7", "0",
                    "TC_8", "0",
                    "TC_9", "0",
                    "TC_10", "0",
                    "TC_11", "0",
                    };
            
            args = argsL;
        }
        if(args.length != 24) {
            throw new RuntimeException("illegal number of arguments");
        }
        
        boolean addToDB = true; 

        
        System.out.println("Selected Crates, that will be enabled on the HSB inputs");
        
        String[] usedTCs = new String[12];
        int[] inputCablesMask = new int[12];
        for(int i = 0; i < args.length; i+=2) {
            System.out.print(args[i] + " " + args[i +1]);
            usedTCs[i/2]  = args[i];
            inputCablesMask[i/2]  = Integer.parseInt(args[i + 1]);
        }
        System.out.println("");     
       
        String hsb0DataDealy = "00000000";//"05550005";
        String hsb1DataDealy = "00000000";//"05550005";
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);   
        
        try {            
    		String localConfigKey = ConfigurationManager.LOCAL_CONF_KEY_SC_FULL; //TODO
    		ConfigKey globalConfigKey = configurationDAO.getGlobalConfigKeyByName("LHC6"); //TODO
    		//configurationManager.createLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_SC_FULL, Subsystem.SC);
    		configurationManager.assignConfigKey(globalConfigKey.getId(), ConfigurationManager.LOCAL_CONF_KEY_SC_FULL);
    		
            HardwareDbCrate scHdb = (HardwareDbCrate)dbMapper.getCrate(equipmentDAO.getCrateByName("SC_904"));
            for(HardwareDbBoard hsb : scHdb.getBoards()) {
                if(hsb.getHardwareBoard().getType().equals("HSB")) {
                    for(HardwareDbChip hsChip : hsb.getChips()) {
                        HardwareDbMuxRecChip muxRecChip = (HardwareDbMuxRecChip)hsChip;
                        if(muxRecChip != null) {                                                                                                                                        
                            //System.out.println("\nthe configuration " + "DEFOULT" + "-----------------------------");
                            
                            //empty configuration
                            IntArray16 recFastClkInv = new IntArray16();
                            IntArray16 recFastClk90 = new IntArray16();            
                            IntArray16 recFastRegAdd = new IntArray16();
                            byte[] recFastDataDelay = {0,0};
                            byte[] recDataDelay = {0,0,0,0};
                            byte[] recChanEna = {0, 0};
                            HalfSortConf conf = new HalfSortConf(recChanEna, recFastClkInv, recFastClk90, 
                                    recFastRegAdd, recFastDataDelay, recDataDelay);
                            
                            
                            //HalfSortConf  conf = (HalfSortConf)(muxRecChip.createConfigurationFromCurrentSettings());

                            if(hsb.getName().equals("HSB_0")) {
                                Binary recDataDelayVal = new Binary(hsb0DataDealy);    
                                conf.setRecDataDelay(recDataDelayVal.getBytes());
                            }
                            else if(hsb.getName().equals("HSB_1")) {
                                Binary recDataDelayVal = new Binary(hsb1DataDealy);                            
                                conf.setRecDataDelay(recDataDelayVal.getBytes());
                            }
                        
                            /*int recChanEnaMask = 0;
                            
                            for(int iTc = 0; iTc < usedTCs.length; iTc++){
                                if(inputCablesMask[iTc] == 0)
                                    continue;
                                
                                TriggerCrate tc = (TriggerCrate)equipmentDAO.getCrateByName(usedTCs[iTc]);
                                for(TcHsbConnection tcHsbConnection : tc.getTcHsbConnections(equipmentDAO)) {
                                    if(tcHsbConnection.getHalfSort().equals(hsChip.getDbChip())) {
                                        //recChanEnaMask |= 1 << tcHsbConnection.getChannelNum();
                                        recChanEnaMask |= inputCablesMask[iTc] << tcHsbConnection.getCableInputNum();
                                    }
                                }
                            }
                            
                            //recChanEna[0] = (byte)recChanEnaMask;
                            
                            //recChanEna[1] = (byte)(0xff & recChanEnaMask); //!!! odwrotna numeracja bytow w tablicy!!!
                            //recChanEna[0] = (byte)( (0xff00 & recChanEnaMask) >> 8);
                            //System.out.println(hsb.getFullName() + " recChanEna " + Integer.toHexString(recChanEna[1] & 0xff) + Integer.toHexString(recChanEna[0] & 0xff));
                            System.out.println(hsb.getFullName() + " recChanEna " + Integer.toHexString(recChanEnaMask));
                            conf.setRecChanEna(recChanEnaMask);*/
                            
                            if(addToDB) {
                                configurationDAO.saveObject(conf);
                        /*        configurationManger.assignConfiguration(hsChip.getDbChip(), conf, ConfigurationManger.LOCAL_CONF_KEY_DEFAULT);
                                System.out.println("the configuration " + ConfigurationManger.LOCAL_CONF_KEY_DEFAULT + " was added for the " + hsChip.getFullName());
                        */        
                                configurationManager.assignConfiguration(hsChip.getDbChip(), conf, 
                                        configurationManager.getLocalConfigKey(localConfigKey).getName());
                                System.out.println("the configuration " + localConfigKey + " was added for the " + hsChip.getFullName());
                            }
                            else {
                                System.out.println("the configuration was not added to DB, simulation only");
                            }
                        }
                    }
                }
            }
            
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            context.rollback();
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (HardwareDbException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            context.closeSession();
        }
    }

}
