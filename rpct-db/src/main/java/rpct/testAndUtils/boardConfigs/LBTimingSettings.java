package rpct.testAndUtils.boardConfigs;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.management.RuntimeErrorException;

import com.sun.org.apache.xpath.internal.operations.Equals;
import com.sun.xml.bind.v2.runtime.reflect.ListIterator;

import rpct.datastream.LMuxBxDataImplXml;
import rpct.datastream.RpctDelays;
import rpct.db.DataAccessException;
import rpct.db.domain.DAO;
import rpct.db.domain.configuration.ChipConfAssignment;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.configuration.SynCoderConf;
import rpct.db.domain.configuration.LocalConfigKey.Subsystem;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.equipment.chamberloaction.ChamberLocation;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.testAndUtils.DynamicHistogram;
import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.synchro.DefOut;

/**
 * @author rpcpro
 *
 */
public class LBTimingSettings {
    static int maxTotalTime = 6; //in BX units, 
    static int invClkBegin = 130; //in the clkDeskew units, 
    static int invClkEnd = 185;
    static int notClkInvLatch = 160;//value measured on 26.02.09, previous: 140; does not matter in practice, it just should be in the above range//the position of the main TTC clock latching the synchronized signal, if the ClkInv is false
    static int invClkLatch = 40;//value measured on 26.02.09, previous: 20; //the position of the main TTC clock latching the synchronized signal, if the ClkInv is true
    //static int ofset = 1; //[bx]
    //static int ofset1 = 1; //[bx] //used in the calcualteSettins
    static int ofset2 = -1;
    static int nsPerBX = 25;

    static double nsPerDeskewStep = 25./240.;

    static double fiberLightSpeed = 5; // [ns/m]

    private static Map<String, Double> ttcFiberLenghts = new HashMap<String, Double>();

    private static PrintStream prntSrm = null;
    private static double maxTTCFiber = 0;    
    static {      
        ttcFiberLenghts.put(    "YEP3_near" ,   97. );
        ttcFiberLenghts.put(    "YEP3_far"  ,   119. );
        ttcFiberLenghts.put(    "YEP1_near" ,   78.5 );
        ttcFiberLenghts.put(    "YEP1_far"  ,   100.5 );

        ttcFiberLenghts.put(    "RB+2_near" ,   72. );
        ttcFiberLenghts.put(    "RB+2_far"  ,   88. );
        ttcFiberLenghts.put(    "RB+1_near" ,   75. - 5. ); //tutaj timing wychodzi o 1 bx za duzo, w zwiazku z tym, aby zmniejszyc delay linkow zwiekszam dely BCO na LB
        ttcFiberLenghts.put(    "RB+1_far"  ,   91. );

        ttcFiberLenghts.put(    "RB0_near"  ,   78.  );
        ttcFiberLenghts.put(    "RB0_far"   ,   94. ); //(deltaTTC % nsPerBX) == 0

        ttcFiberLenghts.put(    "RB-1_near" ,   68.  );
        ttcFiberLenghts.put(    "RB-1_far"  ,   84.  ); //(deltaTTC % nsPerBX) == 0
        ttcFiberLenghts.put(    "RB-2_near" ,   69. ); //(deltaTTC % nsPerBX) == 0
        ttcFiberLenghts.put(    "RB-2_far"  ,   85. );

        ttcFiberLenghts.put(    "YEN1_near" ,   75.5 );
        ttcFiberLenghts.put(    "YEN1_far"  ,   97.5 - 5.);//splashe pokazly ze hity sa o 1 bx za pozno, w zwiazku z tym, aby zmniejszyc delay linkow zwiekszam dely BCO na LB
        ttcFiberLenghts.put(    "YEN3_near" ,   94. ); //(deltaTTC % nsPerBX) == 0
        ttcFiberLenghts.put(    "YEN3_far"  ,   116.);//splashe pokazly ze hity sa o 1 bx za pozno, w zwiazku z tym, aby zmniejszyc delay linkow zwiekszam dely BCO na LB
        //niestety spowodowalo to ze linki sie nie synchronizuja, wiec zostawiam ta dlugosc taka jak byla

        for(Map.Entry<String, Double> e : ttcFiberLenghts.entrySet()) {
            if(e.getValue() > maxTTCFiber) {
                maxTTCFiber = e.getValue();
            }
        }        
    }

    /**
     * @param towerName
     * @return the difference of the TTC signal propagation time in the TTC fiber 
     * between the LB from the towerName
     * and the the tower with the longest TTC fiber
     */
    public static double getDeltaTTC(String towerName) {
        if(ttcFiberLenghts.get(towerName) == null) {
            throw new IllegalArgumentException("now entry in the ttcFiberLenghts for the "  + towerName);
        }
        return (maxTTCFiber - ttcFiberLenghts.get(towerName)) * fiberLightSpeed;               
    }

    /**
     * @param winC in the clkDeskew units
     * @param clkInv
     * @return correction (0 or 1 BX) depending on the position of the winC and clkInv
     * see (Appendix in my PhD)
     */
    static int getCorrectionWin(int winC, boolean clkInv) {        
        if(winC > notClkInvLatch || clkInv == true) //eqivalent to if(winC > invClkBegin) or to if( (winC > InvClkStart && clkInv == true)  || winC > notClkInvStart )
            return 0;
        else
            return 1;
    }


    /**
     * @param config
     * @return timing of the muon hits on the LB input in the ns units
     */
    static double calcualteTimingFromLBConfig(SynCoderConf config, double deltaTTC, boolean isMaster) {
        double timing = 0;

        int dTTCcorr = 0;
        if( (deltaTTC % nsPerBX) > 0)
            dTTCcorr = 1;

        double timinigMod25 = config.getWindowClose() * nsPerDeskewStep - deltaTTC%nsPerBX ;
        if(config.getWindowClose() * nsPerDeskewStep < deltaTTC%nsPerBX) {
            timinigMod25 = timinigMod25 + nsPerBX;
        }
        
        
        int rbcDelayCorr = 0;
        if(deltaTTC%nsPerBX == 0) {
            rbcDelayCorr = 0;
        }
        else if( (timinigMod25 + deltaTTC % nsPerBX ) < nsPerBX) {
            rbcDelayCorr = 1;
        }
        
        int lmuxInDelay = config.getLmuxInDelay(); 
        if(isMaster)
            lmuxInDelay -= RpctDelays.LB_SLAVE_MASTER_LATENCY; 
        //config.getRbcDelay will not work for endcaps
        timing = (-lmuxInDelay + maxTotalTime + ofset2 + rbcDelayCorr + getCorrectionWin(config.getWindowClose(), config.isInvertClock()) ) * nsPerBX  + timinigMod25;
        
/*        timing = (-config.getRbcDelay() + maxTotalTime + getCorrectionWin(config.getWindowClose(), config.isInvertClock()) 
                + ofset - dTTCcorr) * nsPerBX  
                - deltaTTC % nsPerBX + config.getWindowClose() * nsPerDeskewStep; */       
        
        return timing;
    }

    private static class WrongSettingsError extends Exception {
        private static final long serialVersionUID = -7936441287534002636L;

        public WrongSettingsError(String msg) {
            super(msg);
        } 
        
    }
    
    /**
     * @param timing [ns]
     * @param deltaTTC [ns]
     * @param timingOffset [ns]
     * @param isMaster 
     * @return     
     * @throws WrongSettingsError 
     */
    static SynCoderConf calcualteSettins(double timing, double deltaTTC, double timingOffset, boolean isMaster) throws WrongSettingsError {
        SynCoderConf config = new SynCoderConf();

        //int winO = (int)(((int)java.lang.Math.round(timing + dTTC % nsPerBX + timingOffset) % nsPerBX) / nsPerDeskewStep );

        int winO = (int)( ( (timing + timingOffset + deltaTTC % nsPerBX ) % nsPerBX) / nsPerDeskewStep );
        int winC = winO;
        config.setWindowClose((int)winC);
        config.setWindowOpen((int)winO);

        boolean clkInv = false;
        if(winC > invClkBegin && winC < invClkEnd)
            clkInv = true;
        config.setInvertClock(clkInv);

        int dTTCcorr = 0;
        if( (deltaTTC % nsPerBX) > 0)
            dTTCcorr = 1;

        int rbcDelayCorr = 0;
        if(deltaTTC%nsPerBX == 0) {
            rbcDelayCorr = 0;
        }
        else if( ((timing + timingOffset)%nsPerBX + deltaTTC % nsPerBX ) < nsPerBX) {
            rbcDelayCorr = 1;
        }

        int rbcDelay = maxTotalTime + ofset2 - (int)( (timing + timingOffset) / nsPerBX) + rbcDelayCorr + getCorrectionWin(winC, clkInv);
        
        //int rbcDelay = maxTotalTime + ofset1 - (int)( (timing + deltaTTC % nsPerBX + timingOffset) / nsPerBX) - dTTCcorr + getCorrectionWin(winC, clkInv);
        if(rbcDelay < 0) {
            throw new WrongSettingsError("rbcDelay < 0; rbcDelay is " + rbcDelay);
            //System.out.println("rbcDelay < 0; rbcDelay is " + rbcDelay + " rbcDelay set to 0");
            //prntSrm.println("rbcDelay < 0; rbcDelay is " + rbcDelay + " rbcDelay set to 0");
            //rbcDelay = 0;
        }
        config.setRbcDelay(rbcDelay);

        int lmuxInDelay = rbcDelay;
        if(isMaster)
            lmuxInDelay += RpctDelays.LB_SLAVE_MASTER_LATENCY; 
        config.setLmuxInDelay(lmuxInDelay);

        int bcn0Delay = (int)(deltaTTC / nsPerBX) + dTTCcorr + getCorrectionWin(winC, clkInv);
        //System.out.println("\ndeltaTTC " + deltaTTC + " deltaTTC / nsPerBX " + deltaTTC / nsPerBX);
        if(bcn0Delay < 0) {
            throw new IllegalStateException("bcn0Delay < 0");
        }
        config.setBcn0Delay(bcn0Delay);

        config.setTiming(timing + timingOffset);
        
        return config;
    }

    static void test() throws WrongSettingsError {
        double timing =113;
        double deltaTTC = (maxTTCFiber - 94) * fiberLightSpeed; ; 
        double timingOffset = 0;
        boolean isMaster = false;
        SynCoderConf newCoderConf1 = calcualteSettins(timing, deltaTTC, timingOffset, isMaster);
        System.out.println("deltaTTC " + deltaTTC);
        System.out.println(newCoderConf1.toString() + "\n");
        
        deltaTTC = (maxTTCFiber - 93.9) * fiberLightSpeed;
        newCoderConf1 = calcualteSettins(timing, deltaTTC, timingOffset, isMaster);
        System.out.println("deltaTTC " + deltaTTC);
        System.out.println(newCoderConf1.toString() + "\n");
        
        deltaTTC = (maxTTCFiber - 94.1) * fiberLightSpeed;
        newCoderConf1 = calcualteSettins(timing, deltaTTC, timingOffset, isMaster);
        System.out.println("deltaTTC " + deltaTTC);
        System.out.println(newCoderConf1.toString() + "\n");
    }
    
    static void printTimingFromLBConfig(EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) throws DataAccessException, WrongSettingsError {
        prntSrm = System.out;

        for(Crate crate : equipmentDAO.getCratesByType(CrateType.LINKBOX, false) ) {
            if(crate.getName().contains("LBB_RB"))     
            //if(crate.getName().contains("LBB_YEP"))                
            {
                prntSrm.println(crate.getName());
                Map<Integer, Board> boardsMap = new TreeMap<Integer, Board>();
                for(Board board : crate.getBoards()) {
                    if(board.getType() == BoardType.LINKBOARD) {
                        boardsMap.put(board.getPosition(), board);
                    }
                }

                for(Map.Entry<Integer, Board> e : boardsMap.entrySet()) {
                    LinkBoard lb = (LinkBoard)e.getValue();
                    //if(lb.getName().equals("LB_RB-2_S3_BN2D_CH2") == false) continue;                    
                    prntSrm.print(lb.getName() + " " + lb.getPosition() + " " + lb.getChamberNames().iterator().next());                    

                    StaticConfiguration configuration = configurationManager.getConfiguration(lb.getSynCoder(), configurationManager.getDefaultLocalConfigKey());
                                        
                    ChipConfAssignment confAssignment =  configurationDAO.getChipConfAssignment(lb.getSynCoder(), configurationManager.getDefaultLocalConfigKey());
                    confAssignment.getCreationDate();
                    if(configuration != null) {
                        SynCoderConf coderConf = (SynCoderConf) configuration;
                        //double dTTC = 0;
                        double dTTC = getDeltaTTC(lb.getLinkBox().getTowerName());
                        double timing = calcualteTimingFromLBConfig(coderConf, dTTC, lb.isMaster()) ;
                        double diff = timing - coderConf.getTiming();
                        diff = java.lang.Math.round(diff);
                        prntSrm.print("\t calculated timing " +  java.lang.Math.round(timing) + " from getDataTrgDelay " + coderConf.getTiming() + " diff "+ diff + (diff == 0 ? "" : " !!!!!!!!!!!!!!") +" " + confAssignment.getCreationDate() );                        
                        double timingOffset = 0;
                        SynCoderConf coderConf1 = calcualteSettins(timing, dTTC, timingOffset, lb.isMaster());

                        if(java.lang.Math.abs(coderConf.getWindowClose() - coderConf1.getWindowClose()) > 1) {//roznica o jedne jest  mozliwa w yniku 
                            //bledow numerycznych, czasami moze ona powodawac zmiane InvertClock, a co za tym idzie i incy parametrow
                            prntSrm.print("\n getWindowClose " + coderConf.getWindowClose() + " " +coderConf1.getWindowClose() + " !!!!!!!!!");
                        }
                        if(coderConf.isInvertClock() != coderConf1.isInvertClock()) {
                            prntSrm.print("\n isInvertClock " + coderConf.isInvertClock() + " " +coderConf1.isInvertClock() + " !!!!!!!!!");
                        }
                        if(coderConf.getRbcDelay() != coderConf1.getRbcDelay()) {
                            prntSrm.print("\n getRbcDelay " + coderConf.getRbcDelay() +" " +coderConf1.getRbcDelay() + " !!!!!!!!!");
                        }
                        if(coderConf.getLmuxInDelay() != coderConf1.getLmuxInDelay()) {
                            prntSrm.print("\n getLmuxInDelay " + coderConf.getLmuxInDelay() +" " +coderConf1.getLmuxInDelay() + " !!!!!!!!!");
                        }
                        if(coderConf.getBcn0Delay() != coderConf1.getBcn0Delay()) {
                            prntSrm.print("\n getBcn0Delay " + coderConf.getBcn0Delay() +" " +coderConf1.getBcn0Delay() + " !!!!!!!!!");
                        }
                    }
                    prntSrm.print("\n");
                }

                prntSrm.println("\n\n");
            }
        }
    }
    
    static void printTiming(EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, LocalConfigKey localConfigKey) throws DataAccessException {
        prntSrm = System.out;

        for(Crate crate : equipmentDAO.getCratesByType(CrateType.LINKBOX, false) ) {
            //if(crate.getName().contains("LBB_RB"))     
            if(crate.getName().contains("LBB_YEN"))                
            {
                //prntSrm.println(crate.getName());
                Map<Integer, Board> boardsMap = new TreeMap<Integer, Board>();
                for(Board board : crate.getBoards()) {
                    if(board.getType() == BoardType.LINKBOARD) {
                        boardsMap.put(board.getPosition(), board);
                    }
                }

                for(Map.Entry<Integer, Board> e : boardsMap.entrySet()) {
                    LinkBoard lb = (LinkBoard)e.getValue();
                    //if(lb.getName().equals("LB_RB-2_S3_BN2D_CH2") == false) continue;                    
                    prntSrm.print(lb.getName() + " " + lb.getChamberNames().iterator().next());                    

                    StaticConfiguration configuration = configurationManager.getConfiguration(lb.getSynCoder(), localConfigKey);
                    if(configuration != null) {
                        SynCoderConf coderConf = (SynCoderConf) configuration;
                        prntSrm.print(" " + coderConf.getTiming() + " win " + coderConf.getWindowClose() + " lmuxDel " + coderConf.getLmuxInDelay() + " invClk " + coderConf.isInvertClock() + " bcoDel " + coderConf.getBcn0Delay() + " sendDel " + coderConf.getOptLinkSendDelay());                                                
                    }
                    prntSrm.print("\n");
                }

                //prntSrm.println("\n\n");
            }
        }
    }

    static void compareConfigs(EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) throws DataAccessException {
        prntSrm = System.out;
        LocalConfigKey configKey1 = configurationManager.getDefaultLocalConfigKey();
        //LocalConfigKey configKey2 = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_COSMIC);
        prntSrm.println("configKey1 " + configKey1.getName()); //+ " configKey2 " + configKey2.getName()

        for(Crate crate : equipmentDAO.getCratesByType(CrateType.LINKBOX, false) ) {
            if(crate.getName().contains("RB+2") || crate.getName().contains("RB-2")) 
            {
                prntSrm.println(crate.getName());
                Map<Integer, Board> boardsMap = new TreeMap<Integer, Board>();
                for(Board board : crate.getBoards()) {
                    if(board.getType() == BoardType.LINKBOARD) {
                        boardsMap.put(board.getPosition(), board);
                    }
                }

                for(Map.Entry<Integer, Board> e : boardsMap.entrySet()) {
                    LinkBoard lb = (LinkBoard)e.getValue();
                    prntSrm.print(lb.getName() + " " + lb.getPosition() + " " + lb.getChamberNames().iterator().next());                    

                    List<ChipConfAssignment> confAssignmentList = configurationDAO.getChipConfAssignmentHistory(lb.getSynCoder(), configKey1);
                    java.util.ListIterator<ChipConfAssignment> iteraror = confAssignmentList.listIterator(confAssignmentList.size());
                    ChipConfAssignment confAssignment1 = iteraror.previous();
                    ChipConfAssignment confAssignment2 = iteraror.previous();
                    Date when = new Date(2008 - 1900, 8 -1, 13, 9, 0, 0);
/*                    Calendar calendar = new ;
                    when = Calendar.*/
                    while(confAssignment2.getCreationDate().after(when) && iteraror.hasPrevious()) {
                        confAssignment2 = iteraror.previous();
                    }
                    
                    StaticConfiguration configuration1 = null;
                    StaticConfiguration configuration2 = null; 
                    if(confAssignment1 != null && confAssignment2 != null) {
                        configuration1 = confAssignment1.getStaticConfiguration();
                        configuration2 = confAssignment2.getStaticConfiguration();

                        prntSrm.println("\t confAssignment1: " + confAssignment1.getLocalConfigKey().getName() + " " + confAssignment1.getCreationDate() + 
                                        " -- confAssignment2: " + confAssignment2.getLocalConfigKey().getName() + " " + confAssignment2.getCreationDate());
                    }
                    /*StaticConfiguration configuration1 = configurationManger.getConfiguration(lb.getSynCoder(), configKey1);
                    StaticConfiguration configuration2 = configurationManger.getConfiguration(lb.getSynCoder(), configKey2); */                                                                           

                    if(configuration1 != null && configuration2 != null) {
                        SynCoderConf coderConf1 = (SynCoderConf) configuration1;
                        SynCoderConf coderConf2 = (SynCoderConf) configuration2;   

                        double dTTC = getDeltaTTC(lb.getLinkBox().getTowerName());

                        double timing1 = calcualteTimingFromLBConfig(coderConf1, dTTC, lb.isMaster()) ;
                        double timing2 = calcualteTimingFromLBConfig(coderConf2, dTTC, lb.isMaster()) ;

                        prntSrm.println("\t dTTC "+ dTTC + "\ttiming1 " + java.lang.Math.round(timing1) + " timing2 " + java.lang.Math.round(timing2) + " diff " + java.lang.Math.round(timing1 - timing2));                        
                        prntSrm.println("\t dTTC "+ dTTC + "\ttiming1 " + java.lang.Math.round(coderConf1.getTiming()) + " timing2 " + java.lang.Math.round(coderConf2.getTiming()) + " diff " + java.lang.Math.round(timing1 - timing2));                        

                        
                        prntSrm.print("\n getWindowClose " + coderConf1.getWindowClose() + " " +coderConf2.getWindowClose());
                        prntSrm.print("\n isInvertClock " + coderConf1.isInvertClock() + " " +coderConf2.isInvertClock());
                        prntSrm.print("\n getRbcDelay " + coderConf1.getRbcDelay() +" " +coderConf2.getRbcDelay());
                        prntSrm.print("\n getLmuxInDelay " + coderConf1.getLmuxInDelay() +" " +coderConf2.getLmuxInDelay());
                        prntSrm.print("\n getBcn0Delay " + coderConf1.getBcn0Delay() +" " +coderConf2.getBcn0Delay());

                    }
                    prntSrm.print("\n");
                }

                prntSrm.println("\n");
            }
        }
    }
    
    static void compareConfigs1(EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) throws DataAccessException {
        prntSrm = System.out;
        //LocalConfigKey configKey1 = configurationManager.getDefaultLocalConfigKey();
        LocalConfigKey configKey1 =  configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_LBS_LHC1);
        LocalConfigKey configKey2 = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_LBS_LHC2);
        prntSrm.println("configKey1 " + configKey1.getName() + " configKey2 " + configKey2.getName());

        for(Crate crate : equipmentDAO.getCratesByType(CrateType.LINKBOX, false) ) {
            if(crate.getName().contains("LBB_RB0")) 
            {
                prntSrm.println(crate.getName());
                Map<Integer, Board> boardsMap = new TreeMap<Integer, Board>();
                for(Board board : crate.getBoards()) {
                    if(board.getType() == BoardType.LINKBOARD) {
                        boardsMap.put(board.getPosition(), board);
                    }
                }

                for(Map.Entry<Integer, Board> e : boardsMap.entrySet()) {
                    LinkBoard lb = (LinkBoard)e.getValue();
                    StaticConfiguration configuration1 = configurationManager.getConfiguration(lb.getSynCoder(), configKey1);
                    StaticConfiguration configuration2 = configurationManager.getConfiguration(lb.getSynCoder(), configKey2);                                                                            

                    if(configuration1 != null && configuration2 != null) {
                        SynCoderConf coderConf1 = (SynCoderConf) configuration1;
                        SynCoderConf coderConf2 = (SynCoderConf) configuration2;   

                        double dTTC = getDeltaTTC(lb.getLinkBox().getTowerName());

                        //double timing1 = calcualteTimingFromLBConfig(coderConf1, dTTC, lb.isMaster()) ;
                        //double timing2 = calcualteTimingFromLBConfig(coderConf2, 0, lb.isMaster()) ;
                        double timing1 = coderConf1.getTiming() ;
                        double timing2 = coderConf2.getTiming() ;
                        
                        if(coderConf1.getTiming() != coderConf2.getTiming()) { //!!!!!!!!!!!!!!!!!!!!!
                        	prntSrm.println(lb.getName() + " " + lb.getPosition() + " " + lb.getChamberNames().iterator().next()); 
                        	prntSrm.println("\t dTTC "+ dTTC + "\ttiming1 " + java.lang.Math.round(timing1) + " timing2 " + java.lang.Math.round(timing2) + " diff " + java.lang.Math.round(timing1 - timing2));                        
                        	//prntSrm.println("\t dTTC "+ dTTC + "\ttiming1 " + java.lang.Math.round(coderConf1.getTiming()) + " timing2 " + java.lang.Math.round(coderConf2.getTiming()) + " diff " + java.lang.Math.round(timing1 - timing2));                        


                        	prntSrm.print("\n getWindowClose " + coderConf1.getWindowClose() + " " +coderConf2.getWindowClose());
                        prntSrm.print("\n isInvertClock " + coderConf1.isInvertClock() + " " +coderConf2.isInvertClock());
                        prntSrm.print("\n getRbcDelay " + coderConf1.getRbcDelay() +" " +coderConf2.getRbcDelay());
                        prntSrm.print("\n getLmuxInDelay " + coderConf1.getLmuxInDelay() +" " +coderConf2.getLmuxInDelay());
                        prntSrm.print("\n getBcn0Delay " + coderConf1.getBcn0Delay() +" " +coderConf2.getBcn0Delay());
                        prntSrm.print("\n");
                        }
                       /* if(coderConf1.equalInChannelsEna(coderConf2.getInChannelsEna()) == false) { //!!!!!!!!!!!!!!!!!!!!!
                        	prntSrm.println(lb.getName() + " " + lb.getPosition() + " " + lb.getChamberNames().iterator().next()); 
                        	prntSrm.println("\t masks1 length " + coderConf1.getInChannelsEna().length + " " +coderConf1.getInChannelsEnaStr() ); 
                        	prntSrm.println("\t masks2 length " + coderConf1.getInChannelsEna().length + " " + coderConf2.getInChannelsEnaStr() ) ;                      
                        	//prntSrm.println("\t dTTC "+ dTTC + "\ttiming1 " + java.lang.Math.round(coderConf1.getTiming()) + " timing2 " + java.lang.Math.round(coderConf2.getTiming()) + " diff " + java.lang.Math.round(timing1 - timing2));                        


                        	prntSrm.print("\n getWindowClose " + coderConf1.getWindowClose() + " " +coderConf2.getWindowClose());
                        prntSrm.print("\n isInvertClock " + coderConf1.isInvertClock() + " " +coderConf2.isInvertClock());
                        prntSrm.print("\n getRbcDelay " + coderConf1.getRbcDelay() +" " +coderConf2.getRbcDelay());
                        prntSrm.print("\n getLmuxInDelay " + coderConf1.getLmuxInDelay() +" " +coderConf2.getLmuxInDelay());
                        prntSrm.print("\n getBcn0Delay " + coderConf1.getBcn0Delay() +" " +coderConf2.getBcn0Delay());
                        prntSrm.print("\n");
                        }*/
                    }
                    //prntSrm.print("\n");
                }

                prntSrm.println("\n");
            }
        }
    }
    
    static void copyMasks(EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO, LocalConfigKey inConfigKey, LocalConfigKey outConfigKey) throws DataAccessException {
    	prntSrm = System.out;
    	prntSrm.println("inConfigKey " + inConfigKey.getName() + " outConfigKey " + outConfigKey.getName());
    	boolean saveTodb = false;
    	for(Crate crate : equipmentDAO.getCratesByType(CrateType.LINKBOX, false) ) {
    		//if(crate.getName().contains("LBB_RB+2_S11") == false) continue;
    		prntSrm.println(crate.getName());
    		Map<Integer, Board> boardsMap = new TreeMap<Integer, Board>();
    		for(Board board : crate.getBoards()) {
    			if(board.getType() == BoardType.LINKBOARD) {
    				boardsMap.put(board.getPosition(), board);
    			}
    		}

    		for(Map.Entry<Integer, Board> e : boardsMap.entrySet()) {
    			LinkBoard lb = (LinkBoard)e.getValue();
    			StaticConfiguration configuration1 = configurationManager.getConfiguration(lb.getSynCoder(), inConfigKey);
    			StaticConfiguration configuration2 = configurationManager.getConfiguration(lb.getSynCoder(), outConfigKey);                                                                            

    			if(configuration1 != null && configuration2 != null) {
    				SynCoderConf inCoderConf = (SynCoderConf) configuration1;
    				SynCoderConf outCoderConf = (SynCoderConf) configuration2;   
    				if(Binary.compareBytes(inCoderConf.getInChannelsEna(), outCoderConf.getInChannelsEna()) == false) {
    					SynCoderConf newCoderConf =  new SynCoderConf(outCoderConf);
    					newCoderConf.setInChannelsEna(inCoderConf.getInChannelsEna().clone());
    					if(saveTodb) {
	    					configurationDAO.saveObject(newCoderConf);
	    					configurationManager.assignConfiguration(lb.getSynCoder(), newCoderConf, outConfigKey.getName()); 
	    					System.out.println("new configuration for chip " + lb.getName() + " was saved under key " + outConfigKey.getName());
    					}
    					else {
    						System.out.println("mask for chip " + lb.getName() + " are differnt both keys!!!! ");
    					}
    				}
    				else {
    					//System.out.println("mask for chip " + lb.getName() + " are the same in both keys ");
    				}
    			}
    			//prntSrm.print("\n");
    		}

    		prntSrm.println("\n");
    	}
    }
    
    static void printAppliedTimingCorrection(EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, 
    		ConfigurationDAO configurationDAO, LocalConfigKey configKey,
    		Calendar updateDate) throws DataAccessException {
        prntSrm = System.out;        
        prntSrm.println("configKey1 " + configKey.getName());
        
        int number = 0;
        for(Crate crate : equipmentDAO.getCratesByType(CrateType.LINKBOX, false) ) {
            if(crate.getName().contains("RB-2_S10")) //<<<<<<<<<<<<<<<<<<<<<<!!!!!!!!!!!!!!!!!!!!!!!
            //if(((LinkBox)crate).getTowerName().equals("RB+2_far") || ((LinkBox)crate).getTowerName().contains("YEN1")     )
            {
                //prntSrm.println(crate.getName());
                Map<Integer, Board> boardsMap = new TreeMap<Integer, Board>();
                for(Board board : crate.getBoards()) {
                    if(board.getType() == BoardType.LINKBOARD) {
                        boardsMap.put(board.getPosition(), board);
                    }
                }

                
                for(Map.Entry<Integer, Board> e : boardsMap.entrySet()) {                    
                    LinkBoard lb = (LinkBoard)e.getValue();
                    //prntSrm.println(lb.getName() + " " + lb.getChamberNames().iterator().next());                    

                    List<ChipConfAssignment> confAssignmentList = configurationDAO.getChipConfAssignmentHistory(lb.getSynCoder(), configKey);
                    java.util.ListIterator<ChipConfAssignment> iteraror = confAssignmentList.listIterator(confAssignmentList.size());

                    while(iteraror.hasPrevious()) {                        
                        ChipConfAssignment confAssignment1 = iteraror.previous(); 
                        
                        Calendar cal = new GregorianCalendar();
                        cal.setTime(confAssignment1.getCreationDate());
                        if(		cal.get(Calendar.YEAR) == updateDate.get(Calendar.YEAR) &&
                        		cal.get(Calendar.MONTH) == updateDate.get(Calendar.MONTH)	&&
                        		cal.get(Calendar.DAY_OF_MONTH) == updateDate.get(Calendar.DAY_OF_MONTH)) {
                            
                        	SynCoderConf coderConf1 = (SynCoderConf) confAssignment1.getStaticConfiguration();

                        	ChipConfAssignment confAssignment2 = iteraror.previous(); 
                        	if(confAssignment2 ==  null)
                        		break;
                        	
                        	SynCoderConf coderConf2 = (SynCoderConf) confAssignment2.getStaticConfiguration();
                        	
                        	double timingDiff = coderConf1.getTiming() - coderConf2.getTiming();
                        	
                        	number++;
                        	prntSrm.println();
                        	prntSrm.println(number + " " + lb.getName() + " " + lb.getChamberNames().iterator().next() + " timingCorrection " + timingDiff ); 
                        	
                            prntSrm.println(confAssignment1.getCreationDate() + " \n " + coderConf1);
                            prntSrm.println(confAssignment2.getCreationDate() + " \n " + coderConf2);
                            
                        	break;
                        }                        
                    }
                }

                prntSrm.println("");
            }
        }
    }

    
    static void printConfigsHistory(EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, 
    		ConfigurationDAO configurationDAO, LocalConfigKey configKey,
    		int notOlderThanMmonths, int notOlderThanDays) throws DataAccessException {
        prntSrm = System.out;        
        prntSrm.println("configKey1 " + configKey.getName());

        Date today = new Date();
        Calendar cal = new GregorianCalendar();
        cal.setTime(today);
        cal.add(Calendar.MONTH, -notOlderThanMmonths);
        cal.add(Calendar.DAY_OF_YEAR, -notOlderThanDays);
        System.out.println("only configurations not older than " + cal.getTime());
        
        for(Crate crate : equipmentDAO.getCratesByType(CrateType.LINKBOX, false) ) {
            //if(crate.getName().contains("RB0")) //<<<<<<<<<<<<<<<<<<<<<<!!!!!!!!!!!!!!!!!!!!!!!
            //if(((LinkBox)crate).getTowerName().equals("RB+2_far") || ((LinkBox)crate).getTowerName().contains("YEN1")     )
            {
                //prntSrm.println(crate.getName());
                Map<Integer, Board> boardsMap = new TreeMap<Integer, Board>();
                for(Board board : crate.getBoards()) {
                    if(board.getType() == BoardType.LINKBOARD) {
                        boardsMap.put(board.getPosition(), board);
                    }
                }

                for(Map.Entry<Integer, Board> e : boardsMap.entrySet()) {                    
                    LinkBoard lb = (LinkBoard)e.getValue();
                    
                    List<ChipConfAssignment> confAssignmentList = configurationDAO.getChipConfAssignmentHistory(lb.getSynCoder(), configKey);
                    java.util.ListIterator<ChipConfAssignment> iteraror = confAssignmentList.listIterator(confAssignmentList.size());

                    String str = "";
                    while(iteraror.hasPrevious()) {                        
                        ChipConfAssignment confAssignment1 = iteraror.previous(); 
                        SynCoderConf coderConf1 = (SynCoderConf) confAssignment1.getStaticConfiguration();;
                        if(confAssignment1.getCreationDate().before(cal.getTime() ) ) {
                            break;
                        }                        
                            
                        //double dTTC = getDeltaTTC(lb.getLinkBox().getTowerName());

                        //double timing1 = calcualteTimingFromLBConfig(coderConf1, dTTC, lb.isMaster()) ;                        
                        
                        str += confAssignment1.getCreationDate() + " tag " + confAssignment1.getTag() + " timingFromDataTrgDelay " + coderConf1.getTiming() + "\n";
                        //" timingCalc " + java.lang.Math.round(timing1) +
                        str += "getWindowClose " + coderConf1.getWindowClose() + "\n";
                        str += "isInvertClock " + coderConf1.isInvertClock() + "\n";
                        str += "getRbcDelay " + coderConf1.getRbcDelay() + "\n";
                        str += "getLmuxInDelay " + coderConf1.getLmuxInDelay() + "\n";
                        str += "getBcn0Delay " + coderConf1.getBcn0Delay() + "\n";
                        str += "getOptLinkSendDelay " + coderConf1.getOptLinkSendDelay() + "\n";
                        str += "getInChannelsEna " + coderConf1.getInChannelsEnaStr() + "\n";
                    }
                    
                    if(str.length() != 0) {
                    	prntSrm.println(lb.getName() + " " + lb.getPosition());// + " " + lb.getChamberNames().iterator().next());                    
                    	prntSrm.println(str);
                    	prntSrm.print("\n");
                    }
                }
            }
        }
    }

    
    static TreeMap<Date, Double> getTimingChangeHistory(LinkBoard lb, ConfigurationDAO configurationDAO, LocalConfigKey configKey,
    		int notOlderThanMmonths) throws DataAccessException {
    	TreeMap<Date, Double> timingChangeHistory = new TreeMap<Date, Double>();
    	
    	List<ChipConfAssignment> confAssignmentList = configurationDAO.getChipConfAssignmentHistory(lb.getSynCoder(), configKey);
        java.util.ListIterator<ChipConfAssignment> iteraror = confAssignmentList.listIterator(confAssignmentList.size());
        
        Date today = new Date();
        Calendar cal = new GregorianCalendar();
        cal.setTime(today);
        cal.add(Calendar.MONTH, -notOlderThanMmonths);
        //System.out.println("only configurations not older than " + cal.getTime());
        
        double newerTiming = -1;
        Date date = null;
        while(iteraror.hasPrevious()) {                        
            ChipConfAssignment confAssignment1 = iteraror.previous(); 
            SynCoderConf coderConf1 = (SynCoderConf) confAssignment1.getStaticConfiguration();;
            if(confAssignment1.getCreationDate().before(cal.getTime() ) ) {
                break;
            }                        
             
            coderConf1.getTiming();
            
            if(newerTiming != coderConf1.getTiming()) {
            	if(newerTiming != -1) {
            		double diff = newerTiming - coderConf1.getTiming();
            		timingChangeHistory.put(date, diff);
            	}
            	newerTiming = coderConf1.getTiming();
            	date = confAssignment1.getCreationDate();
            }
        }
        
        return timingChangeHistory;
    }
    
    static boolean checkTimingChangeHistory(TreeMap<Date, Double> timingChangeHistory) {
    	boolean ok = true;
    	int previousSign = -2;
    	int cnt = 0;
    	for(Map.Entry<Date, Double> e : timingChangeHistory.descendingMap().entrySet()) {
    		int sign = 0;
    		if(e.getValue() > 0) {
    			sign = 1;
    		}
    		else if(e.getValue() < 0)
    			sign = -1;
    		
    		if(previousSign != -2) {
    			if(previousSign != sign) {
    				ok = false;
    				
    			}
    			break;
    		}
    		previousSign =  sign;
    		cnt++;
    	}
    	
    	if(!ok) {
    		prntSrm.println("TimingChangeHistory");
    		for(Map.Entry<Date, Double> e : timingChangeHistory.descendingMap().entrySet()) {
    			System.out.println(e.getKey() + " " + e.getValue());
    		}
    	}
    	return ok;
    }
    
    /**
     * to correct the results of bug in the Stip Masking GUI , introduced 2011 08 26 
     * - timing setting were set to 0 when the mask were updated
     * @param saveNewConfToDb
     * @param equipmentDAO
     * @param configurationManager
     * @param configurationDAO
     * @throws DataAccessException
     */
    static void restoreGoodConfigs(boolean saveNewConfToDb, EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) throws DataAccessException {
    	String[] lbsNames = {
    			/*"LB_RB-2_S1_BN2B_CH2",
    			"LB_RB0_S11_BM0C_CH0",
    			"LB_RB0_S11_BM0C_CH2",
    			"LB_RB0_S12_BN0B_CH1",
    			"LB_RB0_S12_BP0B_CH1",*/
    			"LB_RB+1_S9_BP1A_CH0"
    	};
    	
    	LocalConfigKey configKey = configurationManager.getLocalConfigKey(ConfigurationManager.CURRENT_LBS_CONF_KEY);
    	for(String lbName : lbsNames) {
    		 LinkBoard lb = (LinkBoard)equipmentDAO.getBoardByName(lbName);
    		 System.out.println(lb.getName() + " " + lb.getPosition() + " " + lb.getChamberName());


             List<ChipConfAssignment> confAssignmentList = configurationDAO.getChipConfAssignmentHistory(lb.getSynCoder(), configKey);
             java.util.ListIterator<ChipConfAssignment> iteraror = confAssignmentList.listIterator(confAssignmentList.size());
             ChipConfAssignment confAssignment1 = iteraror.previous();//the latest assigment
             SynCoderConf coderConf1 = (SynCoderConf) confAssignment1.getStaticConfiguration();
             System.out.println("current settings:");
        	 System.out.println(confAssignment1.getCreationDate());
        	 System.out.println(coderConf1);
        	 System.out.println("");
             
             ChipConfAssignment confAssignment2 = confAssignment1;//iteraror.previous();
             while(iteraror.hasPrevious()) {
                 confAssignment2 = iteraror.previous();
                 SynCoderConf coderConf2 = (SynCoderConf) confAssignment2.getStaticConfiguration();
                 if(coderConf2.getTiming() != 0) {
                	 System.out.println("found last correct settings:");
                	 System.out.println(confAssignment2.getCreationDate());
                	 System.out.println(coderConf2);
                	 System.out.println("");
                	 
                	 SynCoderConf newCoderConf = new SynCoderConf(coderConf2);
                	 newCoderConf.setInChannelsEna(coderConf1.getInChannelsEna().clone());
            		 
            		 System.out.println("new setting will be:");
            		 System.out.println(newCoderConf);
                	 if(saveNewConfToDb) {
                		 configurationDAO.saveObject(newCoderConf); 
                         configurationManager.assignConfiguration(lb.getSynCoder(), newCoderConf, configKey);
                         System.out.println("saved to DB");
                	 }
                	 break;
                 }
                 else {
                	 System.out.println("not correct settings:" + confAssignment2.getCreationDate());
                 }
             }
             
             System.out.println("--------------------------------------------------------");
    	}
    }
    
    
    static void backToPreviousConfigs(boolean saveNewConfToDb, EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) throws DataAccessException {
        prntSrm = System.out;
        LocalConfigKey inConfigKey1 = configurationManager.getDefaultLocalConfigKey();
        LocalConfigKey outConfigKey2 = configurationManager.getDefaultLocalConfigKey(); //configurationManger.getLocalConfigKey(ConfigurationManger.LOCAL_CONF_KEY_COSMIC);
        prntSrm.println("configKey1 " + inConfigKey1.getName() + " configKey2 " + outConfigKey2.getName());
        Date when = new Date(2008 - 1900, 8 -1, 13, 9, 0, 0);
        prntSrm.println("we will back to coniguration before " + when);
        for(Crate crate : equipmentDAO.getCratesByType(CrateType.LINKBOX, false) ) {
            if(crate.getName().contains("RB+2") || crate.getName().contains("RB-2")) 
            {
                prntSrm.println(crate.getName());
                Map<Integer, Board> boardsMap = new TreeMap<Integer, Board>();
                for(Board board : crate.getBoards()) {
                    if(board.getType() == BoardType.LINKBOARD) {
                        boardsMap.put(board.getPosition(), board);
                    }
                }

                for(Map.Entry<Integer, Board> e : boardsMap.entrySet()) {
                    LinkBoard lb = (LinkBoard)e.getValue();
                    prntSrm.print(lb.getName() + " " + lb.getPosition() + " " + lb.getChamberNames().iterator().next());                    

                    List<ChipConfAssignment> confAssignmentList = configurationDAO.getChipConfAssignmentHistory(lb.getSynCoder(), inConfigKey1);
                    java.util.ListIterator<ChipConfAssignment> iteraror = confAssignmentList.listIterator(confAssignmentList.size());
                    ChipConfAssignment confAssignment1 = iteraror.previous();//the latest assigment
                    ChipConfAssignment confAssignment2 = confAssignment1;//iteraror.previous();
                    int stepsBack = 1;
                    while(confAssignment2.getCreationDate().after(when) && iteraror.hasPrevious()) {
                        confAssignment2 = iteraror.previous();
                        stepsBack++;
                    }
                    
                    StaticConfiguration configuration1 = null;
                    StaticConfiguration configuration2 = null; 
                    if(confAssignment1 != null && confAssignment2 != null) {
                        configuration1 = confAssignment1.getStaticConfiguration();
                        configuration2 = confAssignment2.getStaticConfiguration();

                        prntSrm.println("\t curent confAssignment1: " + confAssignment1.getLocalConfigKey().getName() + " " + confAssignment1.getCreationDate() + 
                                        " -- selected previous confAssignment2: " + confAssignment2.getLocalConfigKey().getName() + " " + confAssignment2.getCreationDate() + " stepsBack " + stepsBack);
                    }                                                                       

                    if(configuration1 != null && configuration2 != null) {
                        SynCoderConf coderConf1 = (SynCoderConf) configuration1;
                        SynCoderConf coderConf2 = (SynCoderConf) configuration2;   

                        double dTTC = getDeltaTTC(lb.getLinkBox().getTowerName());

                        double timing1 = calcualteTimingFromLBConfig(coderConf1, dTTC, lb.isMaster()) ;
                        double timing2 = calcualteTimingFromLBConfig(coderConf2, dTTC, lb.isMaster()) ;

                        prntSrm.println("\t dTTC "+ dTTC + "\ttiming1 " + java.lang.Math.round(timing1) + " timing2 " + java.lang.Math.round(timing2) + " diff " + java.lang.Math.round(timing1 - timing2));                        
                        prntSrm.println("\t dTTC "+ dTTC + "\ttiming1 " + java.lang.Math.round(coderConf1.getTiming()) + " timing2 " + java.lang.Math.round(coderConf2.getTiming()) + " diff " + java.lang.Math.round(timing1 - timing2));                        

                        
                        prntSrm.print("\n getWindowClose " + coderConf1.getWindowClose() + " " +coderConf2.getWindowClose());
                        prntSrm.print("\n isInvertClock " + coderConf1.isInvertClock() + " " +coderConf2.isInvertClock());
                        prntSrm.print("\n getRbcDelay " + coderConf1.getRbcDelay() +" " +coderConf2.getRbcDelay());
                        prntSrm.print("\n getLmuxInDelay " + coderConf1.getLmuxInDelay() +" " +coderConf2.getLmuxInDelay());
                        prntSrm.print("\n getBcn0Delay " + coderConf1.getBcn0Delay() +" " +coderConf2.getBcn0Delay());
                        
                        if(saveNewConfToDb) {
                            //configurationDAO.saveObject(newCoderConf); //TODO
                            configurationManager.assignConfiguration(lb.getSynCoder(), coderConf2, outConfigKey2.getName());
                            //configurationManger.assignConfiguration(lb.getSynCoder(), newCoderConf, ConfigurationManger.LOCAL_CONF_KEY_COSMIC);
                            //configurationManger.assignConfiguration(lb.getSynCoder(), coderConf, ConfigurationManger.LOCAL_CONF_KEY_COSMIC_TIGHT);
                            prntSrm.println("saved to DB!!!!!!!!!!!!!!!!!!!!!!!!!");
                        }

                    }
                    prntSrm.print("\n");
                }

                prntSrm.println("\n");
            }
        }
    }
    
    static void assignConfigsToKey(boolean saveNewConfToDb, EquipmentDAO equipmentDAO, 
    		ConfigurationManager configurationManager, ConfigurationDAO configurationDAO,
    		LocalConfigKey inConfigKey, LocalConfigKey outConfigKey) throws DataAccessException {
        prntSrm = System.out;
        for(Crate crate : equipmentDAO.getCratesByType(CrateType.LINKBOX, false) ) {
            if(crate.getName().contains("YE")) 
            {
                prntSrm.println(crate.getName());
                Map<Integer, Board> boardsMap = new TreeMap<Integer, Board>();
                for(Board board : crate.getBoards()) {
                    if(board.getType() == BoardType.LINKBOARD) {
                        boardsMap.put(board.getPosition(), board);
                    }
                }

                for(Map.Entry<Integer, Board> e : boardsMap.entrySet()) {
                    LinkBoard lb = (LinkBoard)e.getValue();
                    prntSrm.print(lb.getName() + " " + lb.getPosition() + " " + lb.getChamberNames().iterator().next());                    

                    StaticConfiguration configuration = configurationManager.getConfiguration(lb.getSynCoder(), inConfigKey);

                    if(configuration != null) {                                                             
                        if(saveNewConfToDb) {
                            configurationManager.assignConfiguration(lb.getSynCoder(), configuration, outConfigKey.getName());
                            prntSrm.println("saved to DB!!!!!!!!!!!!!!!!!!!!!!!!!");
                        }

                    }
                    prntSrm.print("\n");
                }

                prntSrm.println("\n");
            }
        }
    }
    
    static class MinAndMax {
    	public double min = Double.MAX_VALUE;
    	public double max = -Double.MAX_VALUE;

    	//public MinAndMax() {};
    	
    	public String toString() {
    		return "min " + min + " max " + max;
    	}
    }   
    
    static void analyseConfigs(EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO,
    		LocalConfigKey configKey) throws DataAccessException, WrongSettingsError {
        prntSrm = System.out;      
        prntSrm.println("configKey1 " + configKey.getName() + "\n");

        Map<String, Set<Integer> > bc0DelayInTowers = new TreeMap<String, Set<Integer>>();
        Map<String, Set<Integer> > lmuxInDelayTowers = new TreeMap<String, Set<Integer>>();
        Map<String, Set<Integer> > optLinksDelayInTowers = new TreeMap<String, Set<Integer>>();
        

        Map<String, MinAndMax> minAndMaxTimingInTowers = new TreeMap<String, MinAndMax>();
        boolean printFull = false;
        for(Crate crate : equipmentDAO.getCratesByType(CrateType.LINKBOX, false) ) {
            //if(crate.getName().contains("S") && crate.getName().contains("RB0")) 
            //if(crate.getName().contains("YEN3") || crate.getName().contains("YEP3"))
        	//if(crate.getName().contains("YE"))
        	//if(crate.getName().contains("LB_RE+1_S12_EP23"))
            {
               /* if(crate.getName().contains("YEN")) {
                    continue;
                }*/
                prntSrm.println(crate.getName());
                Map<Integer, Board> boardsMap = new TreeMap<Integer, Board>();
                for(Board board : crate.getBoards()) {
                    if(board.getType() == BoardType.LINKBOARD) {
                        boardsMap.put(board.getPosition(), board);
                    }
                }

                for(Map.Entry<Integer, Board> e : boardsMap.entrySet()) {
                    LinkBoard lb = (LinkBoard)e.getValue();
                    if(printFull)
                        prntSrm.print(lb.getName() + " " + lb.getPosition() + " " + lb.getChamberNames().iterator().next());                    
                      
                    List<ChipConfAssignment> confAssignmentList = configurationDAO.getChipConfAssignmentHistory(lb.getSynCoder(), configKey);
                    java.util.ListIterator<ChipConfAssignment> iteraror = confAssignmentList.listIterator(confAssignmentList.size());
                    //iteraror.previous(); //TODO
                    ChipConfAssignment confAssignment1 = iteraror.previous();
                    if(printFull)
                        prntSrm.print(" " + confAssignment1.getCreationDate());
                    
                    StaticConfiguration configuration1 = confAssignment1.getStaticConfiguration();
                    
                    //StaticConfiguration configuration1 = configurationManger.getConfiguration(lb.getSynCoder(), configKey1);
                    
                    if(configuration1 != null) {
                        SynCoderConf coderConf1 = (SynCoderConf) configuration1;   
                        
                        int lmuxInDelay = coderConf1.getLmuxInDelay(); 
                        if(lb.isMaster())
                            lmuxInDelay -= RpctDelays.LB_SLAVE_MASTER_LATENCY; 
                        
                        if(printFull) {
                        	double dTTC = getDeltaTTC(lb.getLinkBox().getTowerName());

                            double timing1 = calcualteTimingFromLBConfig(coderConf1, dTTC, lb.isMaster()) ; 
                        	long diff = java.lang.Math.round(timing1 - coderConf1.getTiming());
                            prntSrm.print("\t dTTC "+ dTTC + "\ttimingCalculated " + java.lang.Math.round(timing1) + " timing from getDataTrgDelay " + coderConf1.getTiming() + " diff " + diff);                        
                            if(diff != 0) {
                            	throw new RuntimeException("diff != 0");
                            }
                            prntSrm.print("\n getWindowClose " + coderConf1.getWindowClose() );
                            prntSrm.print("\n isInvertClock " + coderConf1.isInvertClock());
                            prntSrm.print("\n getRbcDelay " + coderConf1.getRbcDelay());
                            prntSrm.print("\n lmuxInDelay " + lmuxInDelay);
                            prntSrm.print("\n getBcn0Delay " + coderConf1.getBcn0Delay());
                            prntSrm.print("\n getOptLinkSendDelay " + coderConf1.getOptLinkSendDelay());
                        }
                        else {
                        	prntSrm.print(lb.getName() + " " + lb.getPosition() + " " + lb.getChamberNames().iterator().next());
                        	prntSrm.print("\ttiming " + coderConf1.getTiming() + "\twinC " + coderConf1.getWindowClose()+  "\tBcn0Delay " + coderConf1.getBcn0Delay() + " lmuxInDelay " + lmuxInDelay + " optLinkSendDelay " + coderConf1.getOptLinkSendDelay());                        	
                        }                    

                        Set<Integer> bc0Delays = bc0DelayInTowers.get(lb.getLinkBox().getName());
                        if(bc0Delays == null) {
                            bc0Delays =  new TreeSet<Integer>();
                            bc0DelayInTowers.put(lb.getLinkBox().getName(), bc0Delays);                        
                        }                    
                        bc0Delays.add(coderConf1.getBcn0Delay());

                        Set<Integer> lmuxInDelays = lmuxInDelayTowers.get(lb.getLinkBox().getName());
                        if(lmuxInDelays == null) {
                            lmuxInDelays =  new TreeSet<Integer>();
                            lmuxInDelayTowers.put(lb.getLinkBox().getName(), lmuxInDelays);                        
                        }                   
                        
                       
                        
                        lmuxInDelays.add(lmuxInDelay);
                        
                        MinAndMax minAndMax = minAndMaxTimingInTowers.get(lb.getLinkBox().getName());
                        if(minAndMax == null) {
                        	minAndMax = new MinAndMax();
                        	minAndMaxTimingInTowers.put(lb.getLinkBox().getName(), minAndMax);
                        }
                        if(coderConf1.getTiming() < minAndMax.min) {
                        	minAndMax.min = coderConf1.getTiming();                        	
                        }
                        if(coderConf1.getTiming() > minAndMax.max) {
                        	minAndMax.max = coderConf1.getTiming();                        	
                        }      
                        
                        if(lb.isMaster()) {
                        	Set<Integer> optLinkDelays = optLinksDelayInTowers.get(lb.getLinkBox().getName());
                        	if(optLinkDelays == null) {
                        		optLinkDelays =  new TreeSet<Integer>();
                        		optLinksDelayInTowers.put(lb.getLinkBox().getName(), optLinkDelays);                        
                        	}                    
                        	optLinkDelays.add(coderConf1.getOptLinkSendDelay());
                        }
                    }
                    prntSrm.print("\n");
                }

                prntSrm.println("\n");
            }
        }

        prntSrm.println("bc0DelayInTowers");
        for(Map.Entry<String, Set<Integer>> e : bc0DelayInTowers.entrySet()) {
            prntSrm.println(e.getKey() + "\t" + e.getValue());
        }

        prntSrm.println("lmuxInDelayTowers");
        for(Map.Entry<String, Set<Integer>> e : lmuxInDelayTowers.entrySet()) {
            prntSrm.println(e.getKey() + "\t" + e.getValue());
        }
        
        prntSrm.println("optLinksDelayInTowers");
        for(Map.Entry<String, Set<Integer>> e : optLinksDelayInTowers.entrySet()) {
            prntSrm.println(e.getKey() + "\t" + e.getValue());
        }
        
        prntSrm.println("minAndMaxTimingInTowers");
        for(Map.Entry<String, MinAndMax> e : minAndMaxTimingInTowers.entrySet()) {
            prntSrm.println(e.getKey() + "\t" + e.getValue());
        }
    }

    static class TimingCorrection {
    	static int hitsDistibutionBinCnt = 8;
        double meanHitsBX = 0;
        double rms;
        
        int[] hitsDistibution = new int[hitsDistibutionBinCnt];
        int[] sorted =  new int [hitsDistibutionBinCnt]; //indexes in the hitsDistibution, sorted by the values in the hitsDistibution        
        int allHitsCnt = -1;
        
        public String toString() {
        	String format = "meanHitsBX %1$.5f rms %2$.5f";        	
        	String str = String.format(format, meanHitsBX, rms) + "\t";
        	for(int i = 0; i < hitsDistibution.length; i++) {
        		str += hitsDistibution[i] + "\t";
        	}
            return str;
        }

        public boolean equals(TimingCorrection t) {
            if(this.meanHitsBX != t.meanHitsBX)
                return false;
            if(this.rms != t.rms)
                return false;
            return true;
        }
        
        public void analyse() {
        	//calulating sum
        	allHitsCnt = 0;
        	meanHitsBX = 0;
        	for(int i = 0; i < hitsDistibution.length; i++) {
        		allHitsCnt += hitsDistibution[i];	
        		meanHitsBX += hitsDistibution[i] * (i-3);//recalculate meanHitsBX
        		//FIXME recalculate RMS as well
        	}
        	if(allHitsCnt != 0)
        		meanHitsBX = meanHitsBX/allHitsCnt;
        	
        	//sorting bins-------------------------------------
        	for(int i = 0; i < sorted.length; i++) {
        		sorted[i] = i;
        	}
        	
        	for(int i = 0; i < sorted.length; i++) {
        		for(int j = i + 1; j < sorted.length; j++) {
            		if(hitsDistibution[sorted[j]] > hitsDistibution[sorted[i]]) {
            			int buf = sorted[i];
            			sorted[i] =  sorted[j];
            			sorted[j] = buf;
            		}
            	}
        	}
        }
        
        int getAllHitsCnt() {   
        	if(allHitsCnt == -1)
        		analyse();
        	return allHitsCnt;
        }
        
        //numberOfBin is counted from the bin with the max content, and then by the bin conted
        double getHitsCntInABin(int numberOfBin) {
        	return hitsDistibution[sorted[numberOfBin]];
        }
        
        double getHitsCntInMaxBins(int numberOfBins) {
        	double b = 0;
        	for(int i = 0; i < numberOfBins; i++) {
        		b += hitsDistibution[sorted[i]];
        	}
        	return b;
        }
        
        double getMeanFromMaxBins(int numberOfBins) {
        	double mean = 0;

        	double a = 0;
        	double b = 0;
        	for(int i = 0; i < numberOfBins; i++) {
        		a += hitsDistibution[sorted[i]] * sorted[i];
        		b += hitsDistibution[sorted[i]];
        	}
        	mean = a/b;
        	return mean;
        }
        
        double getHitCntArroundMaxBin() {
        	double b = 0;
        	
        	for(int i = -1; i <= 1; i++) {
        		int k = i + sorted[0];
        		if(k < 0)
        			continue;
        		b += hitsDistibution[k];
        	}
        	return b;
        }
        
        double getMeanArroundMaxBin() {
        	double mean = 0;

        	double a = 0;
        	double b = 0;
        	
        	for(int i = -1; i <= 1; i++) {
        		int k = i + sorted[0];
        		if(k < 0)
        			continue;
        		a += hitsDistibution[k] * k;
        		b += hitsDistibution[k];
        	}
        	mean = a/b;
        	return mean;
        }
        
        int getBiggestBeside3MaxBins() {      	
        	int max = 0;
        	for(int k = 0; k < hitsDistibution.length; k++) {
        		if( k < sorted[0] -1 || k > sorted[0] + 1) {
        			if(hitsDistibution[k] > max)
        				max = hitsDistibution[k];
        		}
        	}
        	
        	return max;
        }
        
        int getCountInSecondInPeak() {      	
        	if(hitsDistibution[sorted[0] - 1] > hitsDistibution[sorted[0] + 1])
        		return hitsDistibution[sorted[0] - 1];
        	else 
        		return hitsDistibution[sorted[0] + 1];
        }
        
        int getCountInThirdInPeak() {      	
        	if(hitsDistibution[sorted[0] - 1] < hitsDistibution[sorted[0] + 1])
        		return hitsDistibution[sorted[0] - 1];
        	else 
        		return hitsDistibution[sorted[0] + 1];
        }
    }

    
    static public Map<String, TimingCorrection> readTimingFile(String fileName) throws FileNotFoundException, WrongSettingsError {
    	Map<String, TimingCorrection> timingCorrectionMap = new HashMap<String, TimingCorrection>();
        File inputFile =  new File(fileName);
        Scanner scanner = new Scanner(inputFile);
        int notAcceptedCnt = 0;
        while(scanner.hasNextLine()) { 
        	String lbName = "";
    		//String chamberName = "";
        	TimingCorrection  timingCorrection = new TimingCorrection();
        	try {
        		lbName = scanner.next();
        		
        		String str = scanner.next();

        		timingCorrection.meanHitsBX = scanner.nextDouble();
        		scanner.next();
        		timingCorrection.rms = scanner.nextDouble();
        		scanner.next(); //counts:
        		for(int i = 0; i < TimingCorrection.hitsDistibutionBinCnt; i++) {
        			timingCorrection.hitsDistibution[i] = scanner.nextInt();
        		}
        	}
        	catch(RuntimeException e) {        		
        		System.out.println("error at " + lbName);
        		throw e;
        	}
            TimingCorrection  timingCorrectionPrev = timingCorrectionMap.get(lbName);
            if(timingCorrectionPrev != null) {
                if(!timingCorrectionPrev.equals(timingCorrection)) {
                    prntSrm.println("differnt values alrady read from the file for the " + lbName);
                    prntSrm.println(timingCorrectionPrev);
                    prntSrm.println(timingCorrection);
                    throw new WrongSettingsError("differnt values alrady read from the file for the " + lbName);
                }
            }
            else {
            	timingCorrectionMap.put(lbName, timingCorrection);            	
            }

            scanner.nextLine();
        }
        
        return timingCorrectionMap;
    }
    
    static void tranformCorectionFile(String fileName, EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) throws FileNotFoundException, WrongSettingsError, DataAccessException {
    	FileOutputStream file = new FileOutputStream(fileName + "_", false);
    	prntSrm = new PrintStream(file);

    	Map<String, TimingCorrection> timingCorrectionMap = readTimingFile(fileName);        

    	int n = 0;
    	int pefectTimngCnt = 0;
    	int noHitsCnt = 0;
    	
    	for(Map.Entry<String, TimingCorrection> e : timingCorrectionMap.entrySet()) {
    		TimingCorrection timingCorrection = e.getValue();

    		timingCorrection.analyse();

    		LinkBoard lb = (LinkBoard)equipmentDAO.getBoardByName(e.getKey());
    		ChamberLocation chamber = lb.getChamberLocations().get(0);
    		
        	String format = "%1$-21s %2$-22s";
        	String board_chamber_name = String.format(format, e.getKey(), lb.getChamberName());
    		
    		prntSrm.print(++n + " " + board_chamber_name);
    		prntSrm.print("\t" + chamber.getBarrelOrEndcap()+ "\t" + chamber.getDiskOrWheel() + "\t" + chamber.getLayer() + "\t" + chamber.getSector());
    		prntSrm.print("\t" + timingCorrection);
    		prntSrm.print("\n");
    	}
    }
    
    static void changeChamberToLb(Map<String, TimingCorrection> timingCorrectionMap, EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) 
    		throws WrongSettingsError, DataAccessException {
    	
    	List<Board> linkBoards = equipmentDAO.getBoardsByType(BoardType.LINKBOARD, true);
    	for(Board linkBoard : linkBoards) {   				 
    		System.out.println(linkBoard.getName() + " " + ((LinkBoard)linkBoard).getChamberName());
    	}			
				
    	Iterator<Map.Entry<String, TimingCorrection> > it = timingCorrectionMap.entrySet().iterator(); 
    	while(it.hasNext() ) {
    		
    		{
    			Map.Entry<String, TimingCorrection> e = it.next();
    			if(e.getKey().startsWith("LB_") == false) {
    				String chamberName = e.getKey();
    				
    				chamberName = chamberName.replaceFirst("Middle", "Central");
    				chamberName = chamberName.replaceFirst("W\\+0", "W0");
    				
    				if( (chamberName.contains("S04") || chamberName.contains("S10")) && chamberName.contains("RB4")) {
    					if(chamberName.contains("++_") || chamberName.contains("+_")) {
    						chamberName = chamberName.replaceFirst("\\++_", "/");
    						chamberName = chamberName.replaceFirst("\\+_", "/");
    						chamberName = chamberName.replaceFirst("S04_", "4+/");
    						chamberName = chamberName.replaceFirst("S10_", "10+/");
    					}
    					else if(chamberName.contains("--_") || chamberName.contains("-_")) {
    						chamberName = chamberName.replaceFirst("--_", "/");
    						chamberName = chamberName.replaceFirst("-_", "/");
    						chamberName = chamberName.replaceFirst("S04_", "4-/");
    						chamberName = chamberName.replaceFirst("S10_", "10-/");
    					}
    				}
    				chamberName = chamberName.replaceFirst("\\+_", "/");
    				chamberName = chamberName.replaceFirst("-_", "/");
    				chamberName = chamberName.replace('_', '/');
    				chamberName = chamberName.replaceFirst("S0", "S");
    				chamberName = chamberName.replaceFirst("S", "");
    				boolean converted = false;
    				for(Board linkBoard : linkBoards) {   				
    					//if( ((LinkBoard)linkBoard).getChamberNames().contains(chamberName) ) 
    					if( ((LinkBoard)linkBoard).getChamberName().equals(chamberName) ) 
    					{//the key in the timingCorrectionMap is valid chamber name 
    						TimingCorrection previous = timingCorrectionMap.get(linkBoard.getName());
    						if(previous != null) {
    							System.out.println("merging " + e.getKey() + " with " + linkBoard.getName() + " " + ((LinkBoard)linkBoard).getChamberName()); 
    							for(int i = 0; i < previous.hitsDistibution.length; i++) {
    								previous.hitsDistibution[i] += e.getValue().hitsDistibution[i];
    							}
    							it.remove();
    						}
    						else {
    							it.remove();
    							timingCorrectionMap.put(linkBoard.getName(), e.getValue());
    							System.out.println("converting " + e.getKey() + " to " + linkBoard.getName() + " " + ((LinkBoard)linkBoard).getChamberName()); 
    							it = timingCorrectionMap.entrySet().iterator(); 
    						}    					 		
    						converted = true;
    					}
    				}   				
    				if(!converted) {
    					throw new WrongSettingsError("bad chamber name " + e.getKey() + " " + chamberName);
    				}
    			}
    		}
     	}
    }
    
    //the previpous versioin was checking the LB - chamber assigmened, this does not
    static void applyCorrectionFromFile(boolean saveToDb, String fileName, EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO, 
            LocalConfigKey inputConfigs, LocalConfigKey outputConfigs) throws DataAccessException, FileNotFoundException, WrongSettingsError 
    {
        if(saveToDb) {
            System.out.println("Config will seved to DB"); 
        }
        else {
            System.out.println("Config will NOT be seved to DB, simuation only!!!!!!!!!!!!!!!!!!!!!!!!!!!"); 
        }
        
        Date date = new Date();
    	SimpleDateFormat dateFormat = new SimpleDateFormat("yy_MM_dd__HH_mm");
    	
        //String outfileName = System.getenv("HOME") + "/bin/out/LBTimingSettings/" + dateFormat.format(date) + ".txt";
    	String outfileName = "/nfshome0/rpcpro/bin/out/LBTimingSettings/" + dateFormat.format(date) + ".txt";
        if(saveToDb) {        
        	FileOutputStream file = new FileOutputStream(outfileName, false);
        	prntSrm = new PrintStream(file);
        }
        else {
        	prntSrm = System.out;
        }
    
        prntSrm.println("applyCorrectionFromFile fileName " + fileName);
        prntSrm.println("inputConfigs " + inputConfigs.getName());
        prntSrm.println("outputConfigs " + outputConfigs.getName());

        DynamicHistogram timingCorrectionHist = new DynamicHistogram(true);
        timingCorrectionHist.addBin(-1.025	, -25  ); //ns
        timingCorrectionHist.addBin(-0.975	, -22  );
        timingCorrectionHist.addBin(-0.925	, -19.5);
        timingCorrectionHist.addBin(-0.85	, -17  );
        timingCorrectionHist.addBin(-0.75	, -14.5);
        timingCorrectionHist.addBin(-0.65	, -13.5);
        timingCorrectionHist.addBin(-0.55	, -12.5);
        timingCorrectionHist.addBin(-0.45	, -11.5);
        timingCorrectionHist.addBin(-0.35	, -10.5);
        timingCorrectionHist.addBin(-0.25	, -8   );
        timingCorrectionHist.addBin(-0.15	, -5.5 );
        timingCorrectionHist.addBin(-0.075	, -3   );
        timingCorrectionHist.addBin(-0.025	, -2   );
        timingCorrectionHist.addBin(-0.005	, -1   );
        timingCorrectionHist.addBin(-0.0005	, 0    );
        timingCorrectionHist.addBin( 0.0005	, 1    );
        timingCorrectionHist.addBin(0.005	, 2   );
        timingCorrectionHist.addBin(0.025	, 3    );
        timingCorrectionHist.addBin(0.075	, 5.5  );
        timingCorrectionHist.addBin(0.15	, 8    );
        timingCorrectionHist.addBin(0.25	, 10.5 );
        timingCorrectionHist.addBin(0.35	, 11.5 );
        timingCorrectionHist.addBin(0.45	, 12.5 );
        timingCorrectionHist.addBin(0.55	, 13.5 );
        timingCorrectionHist.addBin(0.65	, 14.5 );
        timingCorrectionHist.addBin(0.75	, 17   );
        timingCorrectionHist.addBin(0.85	, 19.5 );
        timingCorrectionHist.addBin(0.925	, 22   );
        timingCorrectionHist.addBin(0.975	, 25   );
        timingCorrectionHist.addBin(1.025	, 25   );
        
        prntSrm.println("timingCorrectionHist");
        prntSrm.println(timingCorrectionHist);
        int notAcceptedCnt = 0;
        //reading the file
        Map<String, TimingCorrection> timingCorrectionMapOrg = readTimingFile(fileName);        
        Map<String, TimingCorrection> timingCorrectionMap = new HashMap<String, TimingCorrection>();
        
        Map<String, TimingCorrection> perfectTimingMap = new HashMap<String, TimingCorrection>();
        Map<String, TimingCorrection> toWidthTimingMap = new HashMap<String, TimingCorrection>();
        
        DynamicHistogram appliedCorrectionHist = new DynamicHistogram(true);
        appliedCorrectionHist.addBin(-20, 0); //ns
        appliedCorrectionHist.addBin(-15, 0); //ns
        appliedCorrectionHist.addBin(-10, 0); //ns
        appliedCorrectionHist.addBin(-5, 0); //ns
        appliedCorrectionHist.addBin(-3, 0); //ns
        appliedCorrectionHist.addBin(-2, 0); //ns
        appliedCorrectionHist.addBin(-1, 0); //ns
        appliedCorrectionHist.addBin( 0, 0); //ns
        appliedCorrectionHist.addBin( 1, 0); //ns
        appliedCorrectionHist.addBin( 2, 0); //ns
        appliedCorrectionHist.addBin( 3, 0); //ns
        appliedCorrectionHist.addBin( 5, 0); //ns
        appliedCorrectionHist.addBin( 10, 0); //ns
        appliedCorrectionHist.addBin( 15, 0); //ns
        appliedCorrectionHist.addBin( 20, 0); //ns
        
        DynamicHistogram meanForAppliedCorectionHist = new DynamicHistogram(true);
        meanForAppliedCorectionHist.addBin(-0.02, 0); //ns
        meanForAppliedCorectionHist.addBin(-0.01, 0); //ns
        meanForAppliedCorectionHist.addBin(-0.001, 0); //ns
        meanForAppliedCorectionHist.addBin( 0, 0); //ns
        meanForAppliedCorectionHist.addBin( 0.001, 0); //ns
        meanForAppliedCorectionHist.addBin( 0.01, 0); //ns
        meanForAppliedCorectionHist.addBin( 0.02, 0); //ns
        
        
        DynamicHistogram meanHist = new DynamicHistogram(true);
        meanHist.addBin(-0.02, 0); //ns
        meanHist.addBin(-0.01, 0); //ns
        meanHist.addBin(-0.001, 0); //ns
        meanHist.addBin( 0, 0); //ns
        meanHist.addBin( 0.001, 0); //ns
        meanHist.addBin( 0.01, 0); //ns
        meanHist.addBin( 0.02, 0); //ns
        
        changeChamberToLb(timingCorrectionMapOrg, equipmentDAO, configurationManager, configurationDAO);
        String format = "%1$-21s %2$-22s";
        //removing the LBs which do not meet the creatiera
        for(Map.Entry<String, TimingCorrection> e : timingCorrectionMapOrg.entrySet()) {
        	//if( (e.getKey().contains("RE-4") || e.getKey().contains("RE+4")) ==  false)
        		//continue; //TODO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        	
        	TimingCorrection timingCorrection = e.getValue();
        	        	
        	timingCorrection.analyse();
        	Double hitsMean = -(3 - e.getValue().getMeanArroundMaxBin());
        	if(!hitsMean.equals(Double.NaN))
        		meanHist.fill(hitsMean, 1);
        	
        	LinkBoard board = (LinkBoard)(equipmentDAO.getBoardByName(e.getKey()));
        	
        	if(board == null)
        		throw new NullPointerException(e.getKey() +": board == null");
        	if(board.getChamberName() == null) {
        		throw new NullPointerException(board.getName() +" board.getChamberName() == null");
        	}
        	String board_chamber_name = String.format(format, e.getKey(), board.getChamberName());
        	
        	if(timingCorrection.hitsDistibution[3] - timingCorrection.allHitsCnt == 0) {
        		perfectTimingMap.put(board_chamber_name, e.getValue());
        	}        	
        	else if(timingCorrection.getAllHitsCnt() >= 500 &&
            		timingCorrection.getAllHitsCnt() - timingCorrection.getHitCntArroundMaxBin() <= 20 && //most hits in three max bins
            		(timingCorrection.getCountInSecondInPeak() - timingCorrection.getBiggestBeside3MaxBins() ) >= 8 && //significant difference between second bin and bin outside 3 max bins
            		timingCorrection.getCountInSecondInPeak() - timingCorrection.getCountInThirdInPeak() >= 8 //more then 8 hits in the second bin        			
        			/*timingCorrection.getAllHitsCnt() >= 4000 &&
        		timingCorrection.getAllHitsCnt() - timingCorrection.getHitsCntInMaxBins(3) <= 200 && //most hits in three max bins
        		timingCorrection.getHitsCntInMaxBins(2) - timingCorrection.getHitsCntInMaxBins(1) >= 8 //more then 8 hits in the second bin */ 
       	        //timingCorrection.getAllHitsCnt() - timingCorrection.getHitCntArroundMaxBin() <= 2 && //2 or less hits outside three cetral bins
        	    //timingCorrection.getHitCntArroundMaxBin() - timingCorrection.getHitsCntInMaxBins(1) >= 6 //more then 6 hits in two bins arround peak
        	    //not appropirate if peak is not at 0 // && (timingCorrection.hitsDistibution[2] <= 1 || timingCorrection.hitsDistibution[4] <= 1)  //on one side of the peak must be les than 2
        	    )
        	{    
        		timingCorrectionMap.put(e.getKey(), e.getValue());
        	}
/*        	else if(timingCorrection.hitsDistibution[2] > 0 && timingCorrection.hitsDistibution[4] > 0 && //more than 0 hits at both sides
        			Math.abs(timingCorrection.hitsDistibution[2] - timingCorrection.hitsDistibution[4]) < 4) { //and 4 more before than after
        		toWidthTimingMap.put(board_chamber_name, e.getValue());
        	}*/
        	else {
        		notAcceptedCnt++;
        		prntSrm.println(notAcceptedCnt +" data for " + board_chamber_name + " not accepted " + timingCorrection);
        		//timingCorrectionMap.remove(e.getKey());
        	}
        }        
        
        prntSrm.println("\n");
        int toWidthTimingCnt = 0;
        for(Map.Entry<String, TimingCorrection> e : toWidthTimingMap.entrySet()) {
        	prntSrm.println(toWidthTimingCnt +" too wide timig " + e.getKey() + " " + e.getValue());
        	toWidthTimingCnt++;
        }
        prntSrm.println("\n");
        int perfectTimingCnt = 0;
        for(Map.Entry<String, TimingCorrection> e : perfectTimingMap.entrySet()) {
        	prntSrm.println(perfectTimingCnt +" perfect timig " + e.getKey() + " " + e.getValue());
        	perfectTimingCnt++;
        }
        prntSrm.println("\n");
        
        Map<String, MinAndMax> minAndMaxTimingInSectors = new TreeMap<String, MinAndMax>();
    	Map<String, MinAndMax> minAndMaxTimingDiffInSectors = new TreeMap<String, MinAndMax>();
    	Map<String, MinAndMax> minAndMaxLmuxInDelayInSectors = new TreeMap<String, MinAndMax>();
    	
    	int updatedLBsCnt = 0;
    	Date today = new Date();
    	int cnt = 0;
    	List<ChipConfAssignment> chipConfAssignments = configurationDAO.getChipConfAssignments(ChipType.SYNCODER, inputConfigs);
        for(Map.Entry<String, TimingCorrection> e : timingCorrectionMap.entrySet()) {
            LinkBoard lb = (LinkBoard)equipmentDAO.getBoardByName(e.getKey()); //chamberBoardsMap.get(e.getKey());
            if(lb != null) {
            	cnt++;
            	String board_chamber_name = String.format(format, e.getKey(), lb.getChamberName());
            	prntSrm.println(cnt + " " + board_chamber_name + " " + e.getValue());
                
                //StaticConfiguration configuration = configurationManger.getConfiguration(lb.getSynCoder(), configurationManger.getLocalConfigKey(ConfigurationManger.LOCAL_CONF_KEY_COSMIC));
                //StaticConfiguration configuration = configurationManger.getConfiguration(lb.getSynCoder(), configurationManger.getDefaultLocalConfigKey());
                StaticConfiguration configuration = null; //configurationManager.getConfiguration(lb.getSynCoder(), inputConfigs);
                for(ChipConfAssignment assignment : chipConfAssignments) {
                	if(assignment.getChip() == lb.getSynCoder())
                		configuration = assignment.getStaticConfiguration();
                }
                if(configuration == null) {
                	throw new RuntimeException(" configuration == null for " + lb.getName());
                }
                
                
                if(configuration != null) {
                    SynCoderConf oldCoderConf = (SynCoderConf) configuration;                                        
                    double dTTC = getDeltaTTC(lb.getLinkBox().getTowerName());
                    //double timing = calcualteTimingFromLBConfig(coderConf, dTTC) ;
                    double oldTiming = oldCoderConf.getTiming();
                    
                    //double timingCorrection = -(3 - e.getValue().getMeanArroundMaxBin()) * nsPerBX;       
                    double hitsMean = -(3 - e.getValue().getMeanArroundMaxBin());
                    double timingCorrection = 0;
                    if(hitsMean < -1) {
                    	timingCorrection = timingCorrectionHist.getBin(-1).value;   
                    	prntSrm.println("hitsMean < -1)!!!!!!!!!!");
                    }
                    else if(hitsMean > 1.02) {
                    	timingCorrection = timingCorrectionHist.getBin(1).value;
                    	prntSrm.println("hitsMean > 1)!!!!!!!!!!");
                    }
                    else 
                    	timingCorrection = timingCorrectionHist.getBin(hitsMean).value;                    
                    
                    //double timingCorrection = -(3 - e.getValue().getMeanArroundMaxBin()) * nsPerBX;
                    //double timingOffset = 0;
                    prntSrm.println("current timing " +  java.lang.Math.round(oldTiming) + " ns, hitsMean " + hitsMean + " found timingCorrection " + java.lang.Math.round(timingCorrection) + " ns");                    
                   
                    appliedCorrectionHist.fill(timingCorrection, 1);
                    meanForAppliedCorectionHist.fill(hitsMean, 1);
                    /*if(timingCorrection > 2 || timingCorrection < -2) {
                    	prntSrm.println("????");
                    }*/
                                       
                    
                    SynCoderConf newCoderConf;
                    try {
                        newCoderConf = calcualteSettins(oldTiming, dTTC, timingCorrection, lb.isMaster());
                    } catch (WrongSettingsError e1) {
                        System.out.println(lb.getName() + " " + e.getKey() + " "+ e1.toString());
                        System.out.println("current settings " + oldCoderConf.toString());                    
                        System.out.println("current timing " +  java.lang.Math.round(oldTiming) + " ns, found timingCorrection " + java.lang.Math.round(timingCorrection) + " ns");                    
                        throw new WrongSettingsError(lb.getName() + " " + e.getKey() + " "+ e1.toString());
                    }
                    newCoderConf.setInChannelsEna(oldCoderConf.getInChannelsEna().clone());
                    newCoderConf.setOptLinkSendDelay(oldCoderConf.getOptLinkSendDelay());
                    if(lb.getName().contains("RE")) {
                        newCoderConf.setRbcDelay(0);
                        System.out.println(lb.getName() + " " + e.getKey() + " RbcDelay set to 0");
                    }                    

                    if(saveToDb) 
                    {
                    	prntSrm.println(newCoderConf);
                    }
                    if(timingCorrection != 0) {
                    	if(saveToDb) {                    	
                    		configurationDAO.saveObject(newCoderConf); 
                    		configurationManager.assignConfiguration(lb.getSynCoder(), newCoderConf, outputConfigs.getName());
                    		prntSrm.println("saved to the DB");
                    		
                    	}
                    	updatedLBsCnt++;
                    }
                    
                    TreeMap<Date, Double> timingChangeHistory = getTimingChangeHistory(lb, configurationDAO, inputConfigs, 7);
                    timingChangeHistory.put(today, timingCorrection);
                    checkTimingChangeHistory(timingChangeHistory);
                    
                    double timingDiff = newCoderConf.getTiming() - oldCoderConf.getTiming();
                    //System.out.println(" timingDiff (oldTiming - newTiming) " + java.lang.Math.round(timingDiff) + " ns");//should be the same as timingCorrection
                    int lmuxInDelay = newCoderConf.getLmuxInDelay(); 
                    if(lb.isMaster())
                        lmuxInDelay -= RpctDelays.LB_SLAVE_MASTER_LATENCY; 
                    
					MinAndMax minAndMax = minAndMaxTimingInSectors.get(lb.getLinkBox().getName());
                    if(minAndMax == null) {
                    	minAndMax = new MinAndMax();
                    	minAndMaxTimingInSectors.put(lb.getLinkBox().getName(), minAndMax);
                    }
                    if(newCoderConf.getTiming() < minAndMax.min) {
                    	minAndMax.min = newCoderConf.getTiming();                        	
                    }
                    if(newCoderConf.getTiming() > minAndMax.max) {
                    	minAndMax.max = newCoderConf.getTiming();                        	
                    }  
					                        
                    MinAndMax minAndMaxDiff = minAndMaxTimingDiffInSectors.get(lb.getLinkBox().getName());
                    if(minAndMaxDiff == null) {
                    	minAndMaxDiff = new MinAndMax();
                    	minAndMaxTimingDiffInSectors.put(lb.getLinkBox().getName(), minAndMaxDiff);
                    }
                    if(timingDiff < minAndMaxDiff.min) {
                    	minAndMaxDiff.min = timingDiff;                        	
                    }
                    if(timingDiff > minAndMaxDiff.max) {
                    	minAndMaxDiff.max = timingDiff;                        	
                    }
                    
                    MinAndMax minAndMaxLmuxInDelay = minAndMaxLmuxInDelayInSectors.get(lb.getDetectorSectorName());
                    if(minAndMaxLmuxInDelay == null) {
                    	minAndMaxLmuxInDelay = new MinAndMax();
                    	minAndMaxLmuxInDelayInSectors.put(lb.getDetectorSectorName(), minAndMaxLmuxInDelay);
                    }
                    if(lmuxInDelay < minAndMaxLmuxInDelay.min) {
                    	minAndMaxLmuxInDelay.min = lmuxInDelay;                        	
                    }
                    if(lmuxInDelay > minAndMaxLmuxInDelay.max) {
                    	minAndMaxLmuxInDelay.max = lmuxInDelay;                        	
                    }

                    if(java.lang.Math.abs(oldCoderConf.getWindowClose() - newCoderConf.getWindowClose()) > 1) {
                        prntSrm.print("\n getWindowClose " + oldCoderConf.getWindowClose() + " " +newCoderConf.getWindowClose() + " !!!!!!!!!!!!!!!!!");
                    }
                    if(oldCoderConf.isInvertClock() != newCoderConf.isInvertClock()) {
                        prntSrm.print("\n isInvertClock " + oldCoderConf.isInvertClock() + " " +newCoderConf.isInvertClock() + " !!!!!!!!!!!!!!!!!");
                    }
                    if(oldCoderConf.getRbcDelay() != newCoderConf.getRbcDelay()) {
                        prntSrm.print("\n getRbcDelay " + oldCoderConf.getRbcDelay() +" " +newCoderConf.getRbcDelay() + " !!!!!!!!!!!!!!!!!");
                    }
                    if(oldCoderConf.getLmuxInDelay() != newCoderConf.getLmuxInDelay()) {
                        prntSrm.print("\n getLmuxInDelay " + oldCoderConf.getLmuxInDelay() +" " +newCoderConf.getLmuxInDelay() + " !!!!!!!!!!!!!!!!!");
                    }
                    if(oldCoderConf.getBcn0Delay() != newCoderConf.getBcn0Delay()) {
                        prntSrm.print("\n getBcn0Delay " + oldCoderConf.getBcn0Delay() +" " +newCoderConf.getBcn0Delay() + " !!!!!!!!!!!!!!!!!");
                    }
                    prntSrm.print("\n \n");
                    
                    /*if(updatedLBsCnt > 40) {
                    	System.out.println("stopping now!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                    	break;
                    }*/
                }
                else {
                    prntSrm.println("no configuration for the LB "  +lb.getName() + " !!!!!!!!!!!!!!!!!!");
                }
            }
            else {
                prntSrm.println("no LB found for the chamber " + e.getKey() + " !!!!!!!!!!!!!!!!!!");
            }
        }
        
        prntSrm.println("minAndMaxTimingInSectors");
        for(Map.Entry<String, MinAndMax> e : minAndMaxTimingInSectors.entrySet()) {
            prntSrm.println(e.getKey() + "\t" + e.getValue());
        }
        
        prntSrm.println("minAndMaxTimingDiffInSectors (new - old)");
        for(Map.Entry<String, MinAndMax> e : minAndMaxTimingDiffInSectors.entrySet()) {
            prntSrm.println(e.getKey() + "\t" + "min " + java.lang.Math.round(((MinAndMax)e.getValue()).min) + " max " + java.lang.Math.round(((MinAndMax)e.getValue()).max));
        }

        prntSrm.println("minAndMaxLmuxInDelayInSectors");
        for(Map.Entry<String, MinAndMax> e : minAndMaxLmuxInDelayInSectors.entrySet()) {
            prntSrm.println(e.getKey() + "\t" + "min " + java.lang.Math.round(((MinAndMax)e.getValue()).min) + " max " + java.lang.Math.round(((MinAndMax)e.getValue()).max));
        }
        
        prntSrm.println("updatedLBsCnt " + updatedLBsCnt);
        
        prntSrm.println("appliedCorrectionHist" + "\n" + appliedCorrectionHist.toString());
        
        prntSrm.println("meanHist" + "\n" + meanHist.toString());
        
        prntSrm.println("meanForAppliedCorectionHist" + "\n" + meanForAppliedCorectionHist.toString());
        
        if(saveToDb) {        
        	System.out.println("updatedLBsCnt " + updatedLBsCnt);
        	System.out.println("the outpuit is in " + outfileName);
        }
        
        
    }
    
    
    static class TofAndCableProp {
        double timeOfFlight = 0;
        double cablePropTime;

        public String toString() {
            return "timeOfFlight " + timeOfFlight + " cablePropTime " + cablePropTime;
        }

        public boolean equals(TofAndCableProp t) {
            if(this.timeOfFlight != t.timeOfFlight)
                return false;
            if(this.cablePropTime != t.cablePropTime)
                return false;
            return true;
        }
    }
    
    static void calcualteSettingsFromTiming(boolean safeToDb, String fileName, EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO, 
    		LocalConfigKey inputConfigs, LocalConfigKey outputConfigs, double commonTimingOffset) throws DataAccessException, FileNotFoundException, WrongSettingsError {
    	PrintStream prntSrm = System.out;
    	
    	Date date = new Date();
    	SimpleDateFormat dateFormat = new SimpleDateFormat("yy_MM_dd__HH_mm");
    	String outfileName = System.getenv("HOME") + "/bin/out/LBTimingSettings/" + dateFormat.format(date) + ".txt";
    	
    	if(safeToDb) {
    		System.out.println("Config will seved to DB under the key " + outputConfigs.getName());
    		FileOutputStream file = new FileOutputStream(outfileName, false);
    		prntSrm = new PrintStream(file);
    	}
    	else {
    		System.out.println("Config will NOT be seved to DB, simuation only!!!!!!!!!!!!!!!!!!!!!!!!!!!"); 
    	}
    	    	
    	prntSrm.println("calcualteSettingsFromTiming fileName " + fileName);       
    	prntSrm.println("commonTimingOffset " + commonTimingOffset);
    	if(safeToDb)
    		prntSrm.println("Config will seved to DB under the key " + outputConfigs.getName());

    	Map<String, MinAndMax> minAndMaxTimingInSectors = new TreeMap<String, MinAndMax>();
    	Map<String, MinAndMax> minAndMaxTimingDiffInSectors = new TreeMap<String, MinAndMax>();
    	Map<String, MinAndMax> minAndMaxLmuxInDelayInSectors = new TreeMap<String, MinAndMax>();
    	
    	//reading the file
    	Map<LinkBoard, Double> timingMap = new HashMap<LinkBoard, Double>();
    	File inputFile =  new File(fileName);
    	Scanner scanner = new Scanner(inputFile);
    	int minLmuxInDelay = 10000;
    	while(scanner.hasNextLine()) {
    		String lbName = "";
    		String chamberName = "";
    		double timing;
    		try {
    			lbName = scanner.next();
    			chamberName = scanner.next();
    			timing = scanner.nextDouble();

    			Board board = equipmentDAO.getBoardByName(lbName);
    			if(board != null) {
    				LinkBoard lb = (LinkBoard)board;
    				if(lb.getChamberNames().contains(chamberName) == false) {
    					throw new RuntimeException("not correct chamber name " + chamberName + " for " + lbName);
    				} 

    				if(timingMap.containsKey(lb)) {
    					throw new RuntimeException("the timing for  "  + lbName + " " + chamberName + " already read");
    				}

    				StaticConfiguration configuration = configurationManager.getConfiguration(lb.getSynCoder(), inputConfigs);

    				if(configuration != null) {
    					SynCoderConf oldCoderConf = (SynCoderConf) configuration;                                        
    					double dTTC = getDeltaTTC(lb.getLinkBox().getTowerName());

    					//double commonTimingOffset = 0;
    					//double timingOffset = 0;
    					//prntSrm.println("current timing " +  java.lang.Math.round(timing) + " ns, found timingOffset " + java.lang.Math.round(timingOffset) + " ns");                    

    					SynCoderConf newCoderConf;
    					try {
    						newCoderConf = calcualteSettins(timing, dTTC, commonTimingOffset, lb.isMaster());
    					} catch (WrongSettingsError e1) {
    						System.out.println(lb.getName() + " "+ e1.toString());
    						System.out.println("current settings " + oldCoderConf.toString());                    
    						System.out.println("new timing " +  java.lang.Math.round(timing) + " ns, timingOffset " + java.lang.Math.round(commonTimingOffset) + " ns");                    
    						throw new WrongSettingsError(lb.getName() + " "+ e1.toString());
    					}
    					newCoderConf.setInChannelsEna(oldCoderConf.getInChannelsEna());
    					newCoderConf.setOptLinkSendDelay(oldCoderConf.getOptLinkSendDelay());
    					if(lb.getName().contains("RE")) {
    						newCoderConf.setRbcDelay(0);
    						System.out.println(lb.getName() + " " + " RbcDelay set to 0");
    					}
    					
    					int lmuxInDelay = newCoderConf.getLmuxInDelay(); 
                        if(lb.isMaster())
                            lmuxInDelay -= RpctDelays.LB_SLAVE_MASTER_LATENCY;
                        
    					//prntSrm.println(lb.getName() + "\n" + newCoderConf);
    					double timingDiff = oldCoderConf.getTiming() - (timing + commonTimingOffset);
    					//if(Math.abs(timingDiff) > 0.01)
    					{
    						prntSrm.println(lb.getName() + " " + chamberName+ " old timing " + java.lang.Math.round(oldCoderConf.getTiming()) + " new timing + offset " 
    							+ java.lang.Math.round(timing + commonTimingOffset) + " timingDiff " + timingDiff + " " + java.lang.Math.round(timingDiff)
    							+ " lmuxInDelay " + lmuxInDelay);
    					}
    					if(safeToDb) {
    						configurationDAO.saveObject(newCoderConf); 
    						configurationManager.assignConfiguration(lb.getSynCoder(), newCoderConf, outputConfigs.getName());                      
    					}
    					    					
                        
    					if(minLmuxInDelay > lmuxInDelay) {
    						minLmuxInDelay = lmuxInDelay;
    					}

    					MinAndMax minAndMax = minAndMaxTimingInSectors.get(lb.getLinkBox().getName());
                        if(minAndMax == null) {
                        	minAndMax = new MinAndMax();
                        	minAndMaxTimingInSectors.put(lb.getLinkBox().getName(), minAndMax);
                        }
                        if(newCoderConf.getTiming() < minAndMax.min) {
                        	minAndMax.min = newCoderConf.getTiming();                        	
                        }
                        if(newCoderConf.getTiming() > minAndMax.max) {
                        	minAndMax.max = newCoderConf.getTiming();                        	
                        }  
    					                        
                        MinAndMax minAndMaxDiff = minAndMaxTimingDiffInSectors.get(lb.getLinkBox().getName());
                        if(minAndMaxDiff == null) {
                        	minAndMaxDiff = new MinAndMax();
                        	minAndMaxTimingDiffInSectors.put(lb.getLinkBox().getName(), minAndMaxDiff);
                        }
                        if(timingDiff < minAndMaxDiff.min) {
                        	minAndMaxDiff.min = timingDiff;                        	
                        }
                        if(timingDiff > minAndMaxDiff.max) {
                        	minAndMaxDiff.max = timingDiff;                        	
                        } 
                        
                        MinAndMax minAndMaxLmuxInDelay = minAndMaxLmuxInDelayInSectors.get(lb.getDetectorSectorName());
                        if(minAndMaxLmuxInDelay == null) {
                        	minAndMaxLmuxInDelay = new MinAndMax();
                        	minAndMaxLmuxInDelayInSectors.put(lb.getDetectorSectorName(), minAndMaxLmuxInDelay);
                        }
                        if(lmuxInDelay < minAndMaxLmuxInDelay.min) {
                        	minAndMaxLmuxInDelay.min = lmuxInDelay;                        	
                        }
                        if(lmuxInDelay > minAndMaxLmuxInDelay.max) {
                        	minAndMaxLmuxInDelay.max = lmuxInDelay;                        	
                        }
                        
    					/*if(java.lang.Math.abs(coderConf.getWindowClose() - newCoderConf.getWindowClose()) > 1) {
    						prntSrm.print("\n getWindowClose " + coderConf.getWindowClose() + " " +newCoderConf.getWindowClose() + " !!!!!!!!!!!!!!!!!");
    					}
    					if(coderConf.isInvertClock() != newCoderConf.isInvertClock()) {
    						prntSrm.print("\n isInvertClock " + coderConf.isInvertClock() + " " +newCoderConf.isInvertClock() + " !!!!!!!!!!!!!!!!!");
    					}
    					if(coderConf.getRbcDelay() != newCoderConf.getRbcDelay()) {
    						prntSrm.print("\n getRbcDelay " + coderConf.getRbcDelay() +" " +newCoderConf.getRbcDelay() + " !!!!!!!!!!!!!!!!!");
    					}
    					if(coderConf.getLmuxInDelay() != newCoderConf.getLmuxInDelay()) {
    						prntSrm.print("\n getLmuxInDelay " + coderConf.getLmuxInDelay() +" " +newCoderConf.getLmuxInDelay() + " !!!!!!!!!!!!!!!!!");
    					}
    					if(coderConf.getBcn0Delay() != newCoderConf.getBcn0Delay()) {
    						prntSrm.print("\n getBcn0Delay " + coderConf.getBcn0Delay() +" " +newCoderConf.getBcn0Delay() + " !!!!!!!!!!!!!!!!!");
    					}
    					prntSrm.print("\n \n");*/
    				}
    			}
    		}
    		catch(RuntimeException e) {        		
    			System.out.println("error at " + chamberName);
    			throw e;
    		}                		
    	}
    	prntSrm.println("minLmuxInDelay " + minLmuxInDelay);

        prntSrm.println("minAndMaxTimingInSectors");
        for(Map.Entry<String, MinAndMax> e : minAndMaxTimingInSectors.entrySet()) {
            prntSrm.println(e.getKey() + "\t" + e.getValue());
        }
        
        prntSrm.println("minAndMaxTimingDiffInSectors");
        for(Map.Entry<String, MinAndMax> e : minAndMaxTimingDiffInSectors.entrySet()) {
            prntSrm.println(e.getKey() + "\t" + "min " + java.lang.Math.round(((MinAndMax)e.getValue()).min) + " max " + java.lang.Math.round(((MinAndMax)e.getValue()).max));
        }
        
        prntSrm.println("minAndMaxLmuxInDelayInSectors");
        for(Map.Entry<String, MinAndMax> e : minAndMaxLmuxInDelayInSectors.entrySet()) {
            prntSrm.println(e.getKey() + "\t" + "min " + java.lang.Math.round(((MinAndMax)e.getValue()).min) + " max " + java.lang.Math.round(((MinAndMax)e.getValue()).max));
        }
        
        if(safeToDb) {
        	System.out.println("the outpuit is in " + outfileName);
        }
    }
    
    static void calucateSettingsFromTOFandProp(boolean safeToDb, String tofFileName, String cablePropFileName, EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO, 
            LocalConfigKey outputConfigs) throws DataAccessException, FileNotFoundException, WrongSettingsError {
        //PrintStream prntSrm = System.out;
        if(safeToDb) {
            System.out.println("Config will seved to DB"); 
        }
        else {
            System.out.println("Config will NOT be seved to DB, simuation only!!!!!!!!!!!!!!!!!!!!!!!!!!!"); 
        }
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yy_MM_dd__HH_mm");
        String outfileName = System.getenv("HOME") + "/bin/out/LBTimingSettings/" + dateFormat.format(date) + ".txt";
        FileOutputStream file = new FileOutputStream(outfileName, false);
        prntSrm = new PrintStream(file);

        prntSrm.println("calucateSettingsFromTOFandProp tofFileName " + tofFileName + " cablePropFileName " + cablePropFileName);

        Map<String, LinkBoard> chamberBoardsMap = new HashMap<String, LinkBoard>();

        for(Crate crate : equipmentDAO.getCratesByType(CrateType.LINKBOX, false) ) {
            //if(crate.getName().contains("LBB_RB+2")) 
            {
                prntSrm.println(crate.getName());
                Map<Integer, Board> boardsMap = new TreeMap<Integer, Board>();                
                for(Board board : crate.getBoards()) {
                    if(board.getType() == BoardType.LINKBOARD) {
                        boardsMap.put(board.getPosition(), board);
                        LinkBoard lb = (LinkBoard) board;
                        for(String chamberName : lb.getChamberNames()) {
                            chamberBoardsMap.put(chamberName, lb);
                        }
                    }
                }                                                              
            }
        }

        //reading the file
        Map<LinkBoard, TofAndCableProp> timingParamsMap = new HashMap<LinkBoard, TofAndCableProp>();
        if(tofFileName.length() != 0) { //poprawic
            /*File inputFile =  new File(tofFileName);
            Scanner scanner = new Scanner(inputFile);
            while(scanner.hasNextLine()) { 
                String chamberName = scanner.next() + " " + scanner.next();            
                LinkBoard lb = chamberBoardsMap.get(chamberName);
                scanner.next();

                TofAndCableProp  tofAndCableProp = new TofAndCableProp();
                tofAndCableProp.timeOfFlight = scanner.nextDouble();
                            scanner.next();
            tofAndCableProp.rms = scanner.nextDouble();

                TofAndCableProp  timingCorrectionPrev = timingParamsMap.get(chamberName);
                if(timingCorrectionPrev != null) {
                    if(!timingCorrectionPrev.equals(tofAndCableProp)) {
                        prntSrm.println("differnt values alrady read from the file for the " + chamberName);
                        prntSrm.println(timingCorrectionPrev);
                        prntSrm.println(tofAndCableProp);
                    }
                }
                else {
                    timingParamsMap.put(chamberName, tofAndCableProp);
                }

                scanner.nextLine();
            }*/
        }
        else {
            for(Map.Entry<String, LinkBoard> e : chamberBoardsMap.entrySet()) {
                if(e.getValue().getName().contains("RE-")) {
                    TofAndCableProp  tofAndCableProp = new TofAndCableProp();
                    tofAndCableProp.timeOfFlight = 0;
                    tofAndCableProp.cablePropTime = 50;
                    timingParamsMap.put(e.getValue(), tofAndCableProp);
                }
            }
        }

        //int minRbcDelay = 10000;
        for(Map.Entry<LinkBoard, TofAndCableProp> e : timingParamsMap.entrySet()) {
            //LinkBoard lb = chamberBoardsMap.get(e.getKey());
            LinkBoard lb = e.getKey();

            prntSrm.println(lb.getName() + " " + e.getKey() + " " + e.getValue());

            double dTTC = getDeltaTTC(lb.getLinkBox().getTowerName());
            //double timing = calcualteTimingFromLBConfig(coderConf, dTTC) ;

            double timing = e.getValue().timeOfFlight + e.getValue().cablePropTime ;
            double timingOffset = 0;
            SynCoderConf newCoderConf;
            try {
                newCoderConf = calcualteSettins(timing, dTTC, timingOffset, lb.isMaster());
            } catch (WrongSettingsError e1) {
                System.out.println(lb.getName() + " " + e.getKey() + " "+ e1.toString());                   
                System.out.println(" timing " +  java.lang.Math.round(timing) + " ns");                    
                throw new WrongSettingsError(lb.getName() + " " + e.getKey() + " "+ e1.toString());
            }
            //newCoderConf.setInChannelsEna(coderConf.getInChannelsEna());
            //newCoderConf.setOptLinkSendDelay(coderConf.getOptLinkSendDelay());
            if(lb.getName().contains("RE")) {
                newCoderConf.setRbcDelay(0);
                System.out.println(lb.getName() + " " + e.getKey() + " RbcDelay set to 0");
            }

            //newCoderConf.setTiming(timing + timingOffset); 

            prntSrm.println(lb.getName() + " " + newCoderConf);
            if(safeToDb) {
                configurationDAO.saveObject(newCoderConf); //TODO
                //configurationManger.assignConfiguration(lb.getSynCoder(), newCoderConf, ConfigurationManger.LOCAL_CONF_KEY_DEFAULT); //TODO
                //configurationManger.assignConfiguration(lb.getSynCoder(), newCoderConf, ConfigurationManger.LOCAL_CONF_KEY_COSMIC);
                configurationManager.assignConfiguration(lb.getSynCoder(), newCoderConf, outputConfigs.getName()); //TODO                        
            }               
            prntSrm.print("\n \n");

        }
        //prntSrm.println("minRbcDelay " + minRbcDelay);

        System.out.println("the outpuit is in " + outfileName);
    }

    static void correctSettings(EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO, 
            LocalConfigKey inputConfigs, LocalConfigKey outputConfigs, boolean saveNewConfToDb, double timingCorrection) throws DataAccessException, WrongSettingsError {

        prntSrm = System.out;
        prntSrm.println("inputConfiKey " + inputConfigs + " outputConfig key " + outputConfigs);
        Map<String, Set<Integer> > bc0DelayInTowers = new TreeMap<String, Set<Integer>>();
        Map<String, Set<Integer> > lmuxInDelayTowers = new TreeMap<String, Set<Integer>>();

        for(Crate crate : equipmentDAO.getCratesByType(CrateType.LINKBOX, false) ) {
            //if(crate.getName().contains("LBB_RB0"))
            /*if( ( ((LinkBox)crate).getTowerName().contains("RB-2_far") || ((LinkBox)crate).getTowerName().contains("RB+2_far") )
                    && ((LinkBox)crate).getSector() >= 5 && ((LinkBox)crate).getSector() <= 9)*/
            //if(crate.getName().contains("LBB_YEN") == false)
        	//if( ((LinkBox)crate).getTowerName().contains("YEN3_far") )
            {
                //if(crate.getName().contains("LBB_YEN3") || crate.getName().contains("LBB_YEP3"))
                //  continue;

                prntSrm.println(crate.getName());
                Map<Integer, Board> boardsMap = new TreeMap<Integer, Board>();
                for(Board board : crate.getBoards()) {
                    if(board.getType() == BoardType.LINKBOARD) {
                        boardsMap.put(board.getPosition(), board);
                    }
                }

                for(Map.Entry<Integer, Board> e : boardsMap.entrySet()) {
                    LinkBoard lb = (LinkBoard)e.getValue();
                    prntSrm.println(lb.getName() + " " + lb.getPosition() + " " + lb.getChamberNames().iterator().next());  
                    StaticConfiguration configuration = configurationManager.getConfiguration(lb.getSynCoder(), inputConfigs);

                    if(configuration != null) {
                        SynCoderConf coderConf = (SynCoderConf) configuration;                   
                        double timing = coderConf.getTiming();
                        double dTTC = getDeltaTTC(lb.getLinkBox().getTowerName());

                        double timingCal = calcualteTimingFromLBConfig(coderConf, dTTC, lb.isMaster()) ;

                        prntSrm.println("old timing " +  java.lang.Math.round(timing));
                        prntSrm.println("old timingCalc " +  java.lang.Math.round(timingCal) + "\t dTTC " +  java.lang.Math.round(dTTC)  );
                        //prntSrm.println("dTTC " +  java.lang.Math.round(dTTC)  );
                        double timingOffset = 0;                       

                        SynCoderConf newCoderConf = calcualteSettins(timing + timingCorrection, dTTC, timingOffset, lb.isMaster());
                        newCoderConf.setOptLinkSendDelay(coderConf.getOptLinkSendDelay());
                        newCoderConf.setInChannelsEna(coderConf.getInChannelsEna().clone());
                        if(lb.getName().contains("RE")) {
                            newCoderConf.setRbcDelay(0);
                            System.out.println(lb.getName() + " " + e.getKey() + " RbcDelay set to 0");
                        }
                        prntSrm.println("new timing " +  java.lang.Math.round(newCoderConf.getTiming()));
                        
                        //newCoderConf.setBcn0Delay(newCoderConf.getBcn0Delay() + 1); //TODO
                        //newCoderConf.setRbcDelay(newCoderConf.getRbcDelay() -1);
                        //newCoderConf.setLmuxInDelay(newCoderConf.getLmuxInDelay() -1);

/*                        prntSrm.println("getWindowClose " + newCoderConf.getWindowClose());
                        
                        if(java.lang.Math.abs(coderConf.getWindowClose() - newCoderConf.getWindowClose()) > 1) {
                            prntSrm.println("getWindowClose " + coderConf.getWindowClose() + " " +newCoderConf.getWindowClose() + " !!!!!!!!!!!!!!!!!");
                        }
                        if(coderConf.isInvertClock() != newCoderConf.isInvertClock()) {
                            prntSrm.println("isInvertClock " + coderConf.isInvertClock() + " " +newCoderConf.isInvertClock() + " !!!!!!!!!!!!!!!!!");
                        }
                        if(coderConf.getRbcDelay() != newCoderConf.getRbcDelay()) {
                            prntSrm.println("getRbcDelay " + coderConf.getRbcDelay() +" " +newCoderConf.getRbcDelay() + " !!!!!!!!!!!!!!!!!");
                        }
                        if(coderConf.getLmuxInDelay() != newCoderConf.getLmuxInDelay()) {
                            prntSrm.println("getLmuxInDelay " + coderConf.getLmuxInDelay() +" " +newCoderConf.getLmuxInDelay() + " !!!!!!!!!!!!!!!!!");
                        }
                        if(coderConf.getBcn0Delay() != newCoderConf.getBcn0Delay()) {
                            prntSrm.println("getBcn0Delay " + coderConf.getBcn0Delay() +" " +newCoderConf.getBcn0Delay() + " !!!!!!!!!!!!!!!!!");
                        }*/
                        
                       // prntSrm.println(newCoderConf.toString() + "\n");

                        Set<Integer> bc0Delays = bc0DelayInTowers.get(lb.getLinkBox().getName());
                        if(bc0Delays == null) {
                            bc0Delays =  new TreeSet<Integer>();
                            bc0DelayInTowers.put(lb.getLinkBox().getName(), bc0Delays);
                        }

                        bc0Delays.add(newCoderConf.getBcn0Delay());

                        Set<Integer> lmuxInDelays = lmuxInDelayTowers.get(lb.getLinkBox().getName());
                        if(lmuxInDelays == null) {
                            lmuxInDelays =  new TreeSet<Integer>();
                            lmuxInDelayTowers.put(lb.getLinkBox().getName(), lmuxInDelays);                        
                        }                   
                        
                        int lmuxInDelay = newCoderConf.getLmuxInDelay(); 
                        if(lb.isMaster())
                            lmuxInDelay -= RpctDelays.LB_SLAVE_MASTER_LATENCY; 
                        
                        lmuxInDelays.add(lmuxInDelay);
                        
                        
                        if(saveNewConfToDb) {
                            configurationDAO.saveObject(newCoderConf);
                            configurationManager.assignConfiguration(lb.getSynCoder(), newCoderConf, outputConfigs.getName());
                            prntSrm.println("saved to DB!!!!!!!!!!!!!!!!!!!!!!!!!");
                        }

                    }
                    prntSrm.print("\n");
                }

                prntSrm.println("\n\n");
            }
        }

        prntSrm.println("bc0DelayInTowers");
        for(Map.Entry<String, Set<Integer>> e : bc0DelayInTowers.entrySet()) {
            prntSrm.println(e.getKey() + "\t" + e.getValue());
        }

        prntSrm.println("\nlmuxInDelayTowers");
        for(Map.Entry<String, Set<Integer>> e : lmuxInDelayTowers.entrySet()) {
            prntSrm.println(e.getKey() + "\t" + e.getValue());
        }
    }
    
    static void changeToSpecialMasks(EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO, 
            LocalConfigKey inputConfigs, LocalConfigKey outputConfigs, boolean saveNewConfToDb) throws DataAccessException, WrongSettingsError {

        prntSrm = System.out;
        prntSrm.println("inputConfiKey " + inputConfigs + " outputConfig key " + outputConfigs);

        for(Crate crate : equipmentDAO.getCratesByType(CrateType.LINKBOX, false) ) {
            {
                
                prntSrm.println(crate.getName());
                Map<Integer, Board> boardsMap = new TreeMap<Integer, Board>();
                for(Board board : crate.getBoards()) {
                    if(board.getType() == BoardType.LINKBOARD) {
                        boardsMap.put(board.getPosition(), board);
                    }
                }

                for(Map.Entry<Integer, Board> e : boardsMap.entrySet()) {
                    LinkBoard lb = (LinkBoard)e.getValue();
                    prntSrm.println(lb.getName() + " " + lb.getPosition() + " " + lb.getChamberNames().iterator().next());  
                    StaticConfiguration configuration = configurationManager.getConfiguration(lb.getSynCoder(), inputConfigs);

                    if(configuration != null) {
                        SynCoderConf coderConf = (SynCoderConf) configuration;                   
                                             
                        SynCoderConf newCoderConf = new SynCoderConf(coderConf);
                        
                       /* byte[] inChannelsEna = coderConf.getInChannelsEna().clone();
                        for(int i = 0; i < inChannelsEna.length; i++) {
                            if(i != inChannelsEna.length/2)
                            	inChannelsEna[i] = 0;
                        }
                        newCoderConf.setInChannelsEna(inChannelsEna);      */
                        
                        //all enabled excpet disconnecterd
                        newCoderConf.setInChannelsEna(lb.getConnectedStripsMask());
                        

                        if(saveNewConfToDb) {
                            configurationDAO.saveObject(newCoderConf);
                            configurationManager.assignConfiguration(lb.getSynCoder(), newCoderConf, outputConfigs.getName());
                            prntSrm.println("saved to DB!!!!!!!!!!!!!!!!!!!!!!!!!");
                        }

                    }
                    prntSrm.print("\n");
                }

                prntSrm.println("\n\n");
            }
        }
    }
    
    static void changeCSCDelay(EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO, 
            LocalConfigKey inputConfigs, LocalConfigKey outputConfigs, boolean saveNewConfToDb) throws DataAccessException {

        prntSrm = System.out;

         for(Crate crate : equipmentDAO.getCratesByType(CrateType.LINKBOX, false) ) {
            if(crate.getName().contains("LBB_YEP"))
            {              
                prntSrm.println(crate.getName());
                Map<Integer, Board> boardsMap = new TreeMap<Integer, Board>();
                for(Board board : crate.getBoards()) {
                    if(board.getType() == BoardType.LINKBOARD) {
                        boardsMap.put(board.getPosition(), board);
                    }
                }

                for(Map.Entry<Integer, Board> e : boardsMap.entrySet()) {
                    LinkBoard lb = (LinkBoard)e.getValue();
                      
                    StaticConfiguration configuration = configurationManager.getConfiguration(lb.getSynCoder(), inputConfigs);

                    if(configuration != null) {
                        SynCoderConf coderConf = (SynCoderConf) configuration;
                        
                        SynCoderConf newCoderConf = new SynCoderConf(coderConf);
                        newCoderConf.setRbcDelay(0);
                        prntSrm.println(lb.getName() + " " + lb.getPosition() + " " + lb.getChamberNames().iterator().next() + " old RbcDelay " + coderConf.getRbcDelay() + " new RbcDelay " + newCoderConf.getRbcDelay());
                        
                        if(saveNewConfToDb) {
                            configurationDAO.saveObject(newCoderConf); //TODO
                            configurationManager.assignConfiguration(lb.getSynCoder(), newCoderConf, outputConfigs.getName());
                            //configurationManger.assignConfiguration(lb.getSynCoder(), newCoderConf, ConfigurationManger.LOCAL_CONF_KEY_COSMIC);
                            //configurationManger.assignConfiguration(lb.getSynCoder(), coderConf, ConfigurationManger.LOCAL_CONF_KEY_COSMIC_TIGHT);
                            prntSrm.println("saved to DB!!!!!!!!!!!!!!!!!!!!!!!!!");
                        }

                    }
                    //prntSrm.print("\n");
                }

                prntSrm.println("\n\n");
            }
        }
    }
    
    static void putTimingToDataTrgDelay(EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO, 
            LocalConfigKey inputConfigs, boolean saveNewConfToDb) throws DataAccessException {

        prntSrm = System.out;

        Map<String, Set<Integer> > bc0DelayInTowers = new HashMap<String, Set<Integer>>();
        Map<String, Set<Integer> > rbcDelayInTowers = new HashMap<String, Set<Integer>>();

        for(Crate crate : equipmentDAO.getCratesByType(CrateType.LINKBOX, false) ) {
            //if(crate.getName().contains("LBB_RB+1"))
            //if(((LinkBox)crate).getTowerName().equals("RB-2_near") ||
            //        ((LinkBox)crate).getTowerName().equals("RB-2_far")     )
            if(crate.getName().contains("LBB_YEN") == false)
            {
                //if(crate.getName().contains("LBB_YEN3") || crate.getName().contains("LBB_YEP3"))
                //  continue;

                prntSrm.println(crate.getName());
                Map<Integer, Board> boardsMap = new TreeMap<Integer, Board>();
                for(Board board : crate.getBoards()) {
                    if(board.getType() == BoardType.LINKBOARD) {
                        boardsMap.put(board.getPosition(), board);
                    }
                }

                for(Map.Entry<Integer, Board> e : boardsMap.entrySet()) {
                    LinkBoard lb = (LinkBoard)e.getValue();                                       
                    //StaticConfiguration configuration = configurationManger.getConfiguration(lb.getSynCoder(), configurationManger.getLocalConfigKey(ConfigurationManger.LOCAL_CONF_KEY_COSMIC_TIGHT));
                    StaticConfiguration configuration = configurationManager.getConfiguration(lb.getSynCoder(), inputConfigs);

                    if(configuration != null) {
                        SynCoderConf coderConf = (SynCoderConf) configuration;
                        //double timing = calcualteTimingFromLBConfig(coderConf, 0) ;                        
                        double timing = coderConf.getTiming();
                        
                        if(true) 
                        {
                            //prntSrm.println(lb.getName() + " " + lb.getPosition() + " "); 
                            double deltaTTC = getDeltaTTC(lb.getLinkBox().getTowerName());
                            double timingCalc = calcualteTimingFromLBConfig(coderConf, deltaTTC, lb.isMaster());

                            //prntSrm.println("current timing " +  java.lang.Math.round(timing) + "\t dTTC " +  java.lang.Math.round(deltaTTC)  );
                            //prntSrm.println("timingCalc     " + timingCalc);
                            double timingOffset = 0;

                            //SynCoderConf newCoderConf = new SynCoderConf(coderConf);
                           /* SynCoderConf coderConf1 = calcualteSettins((int)java.lang.Math.round(timingCalc), deltaTTC, timingOffset, lb.isMaster());
                            double timingCalc1 = calcualteTimingFromLBConfig(coderConf1, deltaTTC);
                            prntSrm.println("timingCalc1     " + timingCalc1);
                            
                            if(java.lang.Math.abs(coderConf.getWindowClose() - coderConf1.getWindowClose()) > 1) {//roznica o jedne jest  mozliwa w yniku 
                                //bledow numerycznych, czasami moze ona powodawac zmiane InvertClock, a co za tym idzie i incy parametrow
                                prntSrm.println("getWindowClose " + coderConf.getWindowClose() + " " +coderConf1.getWindowClose() + " !!!!!!!!!");
                            }
                            if(coderConf.isInvertClock() != coderConf1.isInvertClock()) {
                                prntSrm.println("isInvertClock " + coderConf.isInvertClock() + " " +coderConf1.isInvertClock() + " !!!!!!!!!");
                            }
                            if(coderConf.getRbcDelay() != coderConf1.getRbcDelay()) {
                                prntSrm.println("getRbcDelay " + coderConf.getRbcDelay() +" " +coderConf1.getRbcDelay() + " !!!!!!!!!");
                            }
                            if(coderConf.getLmuxInDelay() != coderConf1.getLmuxInDelay()) {
                                prntSrm.println("getLmuxInDelay " + coderConf.getLmuxInDelay() +" " +coderConf1.getLmuxInDelay() + " !!!!!!!!!");
                            }
                            if(coderConf.getBcn0Delay() != coderConf1.getBcn0Delay()) {
                                prntSrm.println("getBcn0Delay " + coderConf.getBcn0Delay() +" " +coderConf1.getBcn0Delay() + " !!!!!!!!!");
                            }*/
                            //prntSrm.print("\n");
                            if( Math.abs(timingCalc - timing) > 1) {
                                prntSrm.println(lb.getName() + " " + lb.getPosition() + " "); 
                                prntSrm.println("current timing " +  java.lang.Math.round(timing) + "\t dTTC " +  java.lang.Math.round(deltaTTC)  );
                                prntSrm.println("timingCalc     " + timingCalc);
                                if(saveNewConfToDb) {
                                    coderConf.setTiming(timingCalc);
                                    configurationDAO.saveObject(coderConf); 
                                    prntSrm.println("saved to DB!!!!!!!!!!!!!!!!!!!!!!!!!");
                                }
                            }
                        } 
                    }
                    
                }
            }
        }       
    }

    static void analyseCorrectionFromFile(String fileName, EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO, 
            LocalConfigKey inputConfigs) throws DataAccessException, FileNotFoundException {
        //PrintStream prntSrm = System.out;
        /* Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yy_MM_dd__HH_mm");
        String outfileName = System.getenv("HOME") + "/bin/out/LBTimingSettings/" + dateFormat.format(date) + ".txt";
        FileOutputStream file = new FileOutputStream(outfileName, false);
        prntSrm = new PrintStream(file);
         */        
        prntSrm = System.out;

        prntSrm.println("analyseCorrectionFromFile fileName " + fileName);

        Map<String, LinkBoard> chamberBoardsMap = new HashMap<String, LinkBoard>();

        for(Crate crate : equipmentDAO.getCratesByType(CrateType.LINKBOX, false) ) {
            //if(crate.getName().contains("LBB_RB+2")) 
            {
                prntSrm.println(crate.getName());
                Map<Integer, Board> boardsMap = new TreeMap<Integer, Board>();                
                for(Board board : crate.getBoards()) {
                    if(board.getType() == BoardType.LINKBOARD) {
                        boardsMap.put(board.getPosition(), board);
                        LinkBoard lb = (LinkBoard) board;
                        for(String chamberName : lb.getChamberNames()) {
                            chamberBoardsMap.put(chamberName, lb);
                        }
                    }
                }                                                              
            }
        }

        //reading the file
        Map<String, TimingCorrection> timingCorrectionMap = new HashMap<String, TimingCorrection>();
        File inputFile =  new File(fileName);
        Scanner scanner = new Scanner(inputFile);
        while(scanner.hasNextLine()) { 
            String chamberName = scanner.next() + " " + scanner.next();            
            LinkBoard lb = chamberBoardsMap.get(chamberName);
            scanner.next();

            TimingCorrection  timingCorrection = new TimingCorrection();
            timingCorrection.meanHitsBX = scanner.nextDouble();
            scanner.next();
            timingCorrection.rms = scanner.nextDouble();

            TimingCorrection  timingCorrectionPrev = timingCorrectionMap.get(chamberName);
            if(timingCorrectionPrev != null) {
                if(!timingCorrectionPrev.equals(timingCorrection)) {
                    prntSrm.println("differnt values alrady read from the file for the " + chamberName);
                    prntSrm.println(timingCorrectionPrev);
                    prntSrm.println(timingCorrection);
                }
            }
            else {
                timingCorrectionMap.put(chamberName, timingCorrection);
            }

            scanner.nextLine();
        }

        Set<String> sotedOutput = new TreeSet<String>();
        class TimingRange {
            public double min = 1000; 
            public double max = -1;
            public double average = 0;
            int entries = 0;
            public String toString() {
                return "min " + min  + "  max " + max + " average " + average/entries;
            }
        };
        Map<String, TimingRange> timingCorrectioBySect = new TreeMap<String, TimingRange>();
        int minRbcDelay = 10000;
        for(Map.Entry<String, TimingCorrection> e : timingCorrectionMap.entrySet()) {
            LinkBoard lb = chamberBoardsMap.get(e.getKey());
            if(lb != null) {
                String str = lb.getName() + " " + e.getKey();
                String spces = "                        ";
                str = str + spces.substring(0, 50 - str.length()) + e.getValue();
                //prntSrm.print(str);
                //StaticConfiguration configuration = configurationManger.getConfiguration(lb.getSynCoder(), configurationManger.getLocalConfigKey(ConfigurationManger.LOCAL_CONF_KEY_COSMIC));
                //StaticConfiguration configuration = configurationManger.getConfiguration(lb.getSynCoder(), configurationManger.getDefaultLocalConfigKey());
                StaticConfiguration configuration = configurationManager.getConfiguration(lb.getSynCoder(), inputConfigs);

                if(configuration != null) {
                    SynCoderConf coderConf = (SynCoderConf) configuration;                                        
                    double dTTC = getDeltaTTC(lb.getLinkBox().getTowerName());
                    double timing = calcualteTimingFromLBConfig(coderConf, dTTC, lb.isMaster()) ;
                    double timingOffset = -(3 - e.getValue().meanHitsBX) * nsPerBX;
                    //double timingOffset = 0;
                    //prntSrm.println("\t current timing " +  java.lang.Math.round(timing) + " ns, found timingOffset " + java.lang.Math.round(timingOffset) + " ns");
                    //String format = new Forma
                    //prntSrm.format(format, lb.getName(), e.getKey(), e.getValue(), "\t current timing ", java.lang.Math.round(timing), " ns, found timingOffset ", java.lang.Math.round(timingOffset), " ns");
                    str = str + "\t current timing " +  java.lang.Math.round(timing) + " ns, found timingOffset " + java.lang.Math.round(timingOffset) + " ns";
                    sotedOutput.add(str);

                    TimingRange timingRange = timingCorrectioBySect.get(lb.getLinkBox().getName());
                    if(timingRange ==  null) {
                        timingRange = new TimingRange();
                        timingCorrectioBySect.put(lb.getLinkBox().getName(), timingRange);
                    }
                    if(timingRange.min > e.getValue().meanHitsBX) {
                        timingRange.min = e.getValue().meanHitsBX;
                    }
                    if(timingRange.max < e.getValue().meanHitsBX) {
                        timingRange.max = e.getValue().meanHitsBX;
                    }

                    timingRange.average += e.getValue().meanHitsBX;
                    timingRange.entries++;
                }
                else {
                    prntSrm.println("no configuration for the LB "  +lb.getName() + " !!!!!!!!!!!!!!!!!!");
                }
            }
            else {
                prntSrm.println("no LB found for the chamber " + e.getKey() + " !!!!!!!!!!!!!!!!!!");
            }
        }

        for(String str : sotedOutput) {
            prntSrm.println(str);
        }

        for(Map.Entry<String, TimingRange> e : timingCorrectioBySect.entrySet()) {
            prntSrm.println(e.getKey() + " " + e.getValue());
        }
        prntSrm.println("minRbcDelay " + minRbcDelay);

        //System.out.println("the outpuit is in " + outfileName);
    }


    static void applyTTCFibberCorrection(boolean saveToDb, EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) throws DataAccessException, FileNotFoundException, WrongSettingsError {
        prntSrm = System.out;

        Map<String, Set<Integer> > bc0DelayInTowers = new HashMap<String, Set<Integer>>();
        Map<String, Set<Integer> > rbcDelayInTowers = new HashMap<String, Set<Integer>>();

        for(Crate crate : equipmentDAO.getCratesByType(CrateType.LINKBOX, false) ) {
            /*if( (crate.getName().contains("LBB_RB-2") || crate.getName().contains("LBB_RB+2") ) &&
                (   ((LinkBox)crate).getSector() >= 5 && ((LinkBox)crate).getSector() <= 9)        ) */            	
        	//if( ((LinkBox)crate).getTowerName().contains("YE") )
            if( ((LinkBox)crate).getTowerName().contains("YEN3_far") )
            {
                prntSrm.println(crate.getName());
                Map<Integer, Board> boardsMap = new TreeMap<Integer, Board>();
                for(Board board : crate.getBoards()) {
                    if(board.getType() == BoardType.LINKBOARD) {
                        boardsMap.put(board.getPosition(), board);
                    }
                }

                for(Map.Entry<Integer, Board> e : boardsMap.entrySet()) {
                    LinkBoard lb = (LinkBoard)e.getValue();
                    prntSrm.println(lb.getName() + " " + lb.getPosition() + " ");                    
                    //StaticConfiguration configuration = configurationManger.getConfiguration(lb.getSynCoder(), configurationManger.getLocalConfigKey(ConfigurationManger.LOCAL_CONF_KEY_COSMIC_TIGHT));
                    StaticConfiguration configuration = configurationManager.getConfiguration(lb.getSynCoder(), configurationManager.getDefaultLocalConfigKey());

                    if(configuration != null) {
                        SynCoderConf coderConf = (SynCoderConf) configuration;
                        //double timing = calcualteTimingFromLBConfig(coderConf, 0, lb.isMaster()) ;
                        double timing = coderConf.getTiming();
                        double dTTC = getDeltaTTC(lb.getLinkBox().getTowerName());

                        prntSrm.println("current timing " +  timing + "\t dTTC " +  java.lang.Math.round(dTTC)  );
                        //double timingOffset = 2; //do poprawienia bledu ktory kiedys byl
                        double timingOffset = 0;
                        SynCoderConf newCoderConf = calcualteSettins(timing, dTTC, timingOffset, lb.isMaster());
                        newCoderConf.setInChannelsEna(coderConf.getInChannelsEna().clone());
                        newCoderConf.setOptLinkSendDelay(coderConf.getOptLinkSendDelay());
                        
                        if(lb.getName().contains("RE")) {
                            newCoderConf.setRbcDelay(0);                            
                            System.out.println(lb.getName() + " " + e.getKey() + " RbcDelay set to 0");
                        }
                        //prntSrm.println(lb.getName() + " " + newCoderConf);
                        //prntSrm.println(lb.getName());

                        Set<Integer> bc0Delays = bc0DelayInTowers.get(lb.getLinkBox().getTowerName());
                        Set<Integer> rbcDelays = rbcDelayInTowers.get(lb.getLinkBox().getTowerName());
                        if(bc0Delays == null) {
                            bc0Delays =  new TreeSet<Integer>();
                            bc0DelayInTowers.put(lb.getLinkBox().getTowerName(), bc0Delays);

                            rbcDelays =  new TreeSet<Integer>();
                            rbcDelayInTowers.put(lb.getLinkBox().getTowerName(), rbcDelays);
                        }

                        bc0Delays.add(newCoderConf.getBcn0Delay());
                        rbcDelays.add(newCoderConf.getRbcDelay());
                        if(saveToDb) {
                        	configurationDAO.saveObject(newCoderConf); //TODO
                        	configurationManager.assignConfiguration(lb.getSynCoder(), newCoderConf, ConfigurationManager.CURRENT_LBS_CONF_KEY);
                                 
                        	//configurationManger.assignConfiguration(lb.getSynCoder(), newCoderConf, ConfigurationManger.LOCAL_CONF_KEY_COSMIC);
                        	//configurationManger.assignConfiguration(lb.getSynCoder(), coderConf, ConfigurationManger.LOCAL_CONF_KEY_COSMIC_TIGHT);
                        } 
                        if(java.lang.Math.abs(coderConf.getWindowClose() - newCoderConf.getWindowClose()) > 1) {
                            prntSrm.print("\n getWindowClose " + coderConf.getWindowClose() + " " +newCoderConf.getWindowClose());
                        }
                        if(coderConf.isInvertClock() != newCoderConf.isInvertClock()) {
                            prntSrm.print("\n isInvertClock " + coderConf.isInvertClock() + " " +newCoderConf.isInvertClock());
                        }
                        if(coderConf.getRbcDelay() != newCoderConf.getRbcDelay()) {
                            prntSrm.print("\n getRbcDelay " + coderConf.getRbcDelay() +" " +newCoderConf.getRbcDelay());
                        }
                        if(coderConf.getLmuxInDelay() != newCoderConf.getLmuxInDelay()) {
                            prntSrm.print("\n getLmuxInDelay " + coderConf.getLmuxInDelay() +" " +newCoderConf.getLmuxInDelay());
                        }
                        if(coderConf.getBcn0Delay() != newCoderConf.getBcn0Delay()) {
                            prntSrm.print("\n getBcn0Delay " + coderConf.getBcn0Delay() +" " +newCoderConf.getBcn0Delay());
                        }
                    }
                    prntSrm.print("\n\n\n");
                }

                prntSrm.println("\n\n");
            }
        }        

        prntSrm.println("bc0DelayInTowers");
        for(Map.Entry<String, Set<Integer>> e : bc0DelayInTowers.entrySet()) {
            prntSrm.println(e.getKey() + "\t" + e.getValue());
        }

        prntSrm.println("\nrbcDelayInTowers");
        for(Map.Entry<String, Set<Integer>> e : rbcDelayInTowers.entrySet()) {
            prntSrm.println(e.getKey() + "\t" + e.getValue());
        }
    }

    static void createSettings(EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) throws DataAccessException, WrongSettingsError {
        prntSrm = System.out;

        Map<String, Set<Integer> > bc0DelayInTowers = new HashMap<String, Set<Integer>>();
        Map<String, Set<Integer> > rbcDelayInTowers = new HashMap<String, Set<Integer>>();

        for(Crate crate : equipmentDAO.getCratesByType(CrateType.LINKBOX, false) ) {
            if(crate.getName().contains("LBB_YEP") )
            {
                prntSrm.println(crate.getName());
                Map<Integer, Board> boardsMap = new TreeMap<Integer, Board>();
                for(Board board : crate.getBoards()) {
                    if(board.getType() == BoardType.LINKBOARD) {
                        boardsMap.put(board.getPosition(), board);
                    }
                }

                for(Map.Entry<Integer, Board> e : boardsMap.entrySet()) {
                    LinkBoard lb = (LinkBoard)e.getValue();
                    prntSrm.println(lb.getName() + " " + lb.getPosition() + " ");                    
                    //StaticConfiguration configuration = configurationManger.getConfiguration(lb.getSynCoder(), configurationManger.getLocalConfigKey(ConfigurationManger.LOCAL_CONF_KEY_COSMIC_TIGHT));
                    //StaticConfiguration configuration = configurationManger.getConfiguration(lb.getSynCoder(), configurationManger.getDefaultLocalConfigKey());
                    //SynCoderConf coderConf = (SynCoderConf) configuration;
                    
                    double timing = 100;  //ns                     
                    double dTTC = getDeltaTTC(lb.getLinkBox().getTowerName());

                    prntSrm.println("current timing " +  java.lang.Math.round(timing) + "\t dTTC " +  java.lang.Math.round(dTTC)  );
                    double timingOffset = 2;
                    SynCoderConf newCoderConf = calcualteSettins(timing, dTTC, timingOffset, lb.isMaster());
                    //newCoderConf.setDataTrgDelay((int)java.lang.Math.round(timing + timingOffset)); //we are using this field for storing the timing!!!!!!!!!!!!!!!!!!!!!!
                    
                    prntSrm.println(lb.getName() + " " + newCoderConf + "\n");

                    Set<Integer> bc0Delays = bc0DelayInTowers.get(lb.getLinkBox().getTowerName());
                    Set<Integer> rbcDelays = rbcDelayInTowers.get(lb.getLinkBox().getTowerName());
                    if(bc0Delays == null) {
                        bc0Delays =  new TreeSet<Integer>();
                        bc0DelayInTowers.put(lb.getLinkBox().getTowerName(), bc0Delays);

                        rbcDelays =  new TreeSet<Integer>();
                        rbcDelayInTowers.put(lb.getLinkBox().getTowerName(), rbcDelays);
                    }
                    bc0Delays.add(newCoderConf.getBcn0Delay());
                    rbcDelays.add(newCoderConf.getRbcDelay());

                    configurationDAO.saveObject(newCoderConf); //TODO
                    configurationManager.assignConfiguration(lb.getSynCoder(), newCoderConf, ConfigurationManager.CURRENT_LBS_CONF_KEY);
                    //configurationManger.assignConfiguration(lb.getSynCoder(), newCoderConf, ConfigurationManger.LOCAL_CONF_KEY_COSMIC);
                    //configurationManger.assignConfiguration(lb.getSynCoder(), coderConf, ConfigurationManger.LOCAL_CONF_KEY_COSMIC_TIGHT);
                }

                prntSrm.println("\n\n");
            }
        }        

        prntSrm.println("bc0DelayInTowers");
        for(Map.Entry<String, Set<Integer>> e : bc0DelayInTowers.entrySet()) {
            prntSrm.println(e.getKey() + "\t" + e.getValue());
        }

        prntSrm.println("\nrbcDelayInTowers");
        for(Map.Entry<String, Set<Integer>> e : rbcDelayInTowers.entrySet()) {
            prntSrm.println(e.getKey() + "\t" + e.getValue());
        }
    }

    static void createSettingsByHand(EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO,
    		String[] lbNames, int[] timings) throws DataAccessException, WrongSettingsError {
        prntSrm = System.out;
        for(int i = 0; i < lbNames.length; i++) {
        	LinkBoard linkBoard = (LinkBoard)equipmentDAO.getBoardByName(lbNames[i]);
        	double dTTC = getDeltaTTC("RB0_near");
        	SynCoderConf newCoderConf = calcualteSettins(timings[i], dTTC, 0, linkBoard.isMaster());
            configurationDAO.saveObject(newCoderConf); //TODO
            configurationManager.assignConfiguration(linkBoard.getSynCoder(), newCoderConf, ConfigurationManager.CURRENT_LBS_CONF_KEY);          
        }        
    }
 
    static void printSettingsByTag(ConfigurationManager configurationManager, ConfigurationDAO configurationDAO, String tag) throws DataAccessException {
    	prntSrm = System.out;
    	
    	List<ChipConfAssignment> assignments = configurationDAO.getChipConfAssignments(ChipType.SYNCODER, configurationDAO.getChipConfigTag(tag));

        for(ChipConfAssignment assignment : assignments) {            	
        	String str = "";
            SynCoderConf coderConf1 = (SynCoderConf) assignment.getStaticConfiguration();;
                                                     
            str += assignment.getCreationDate() + " tag " + assignment.getTag() + " timingFromDataTrgDelay " + coderConf1.getTiming() + "\n";
            //" timingCalc " + java.lang.Math.round(timing1) +
            str += "getWindowClose " + coderConf1.getWindowClose() + "\n";
            str += "isInvertClock " + coderConf1.isInvertClock() + "\n";
            str += "getRbcDelay " + coderConf1.getRbcDelay() + "\n";
            str += "getLmuxInDelay " + coderConf1.getLmuxInDelay() + "\n";
            str += "getBcn0Delay " + coderConf1.getBcn0Delay() + "\n";
            str += "getOptLinkSendDelay " + coderConf1.getOptLinkSendDelay() + "\n";
            str += "getInChannelsEna " + coderConf1.getInChannelsEnaStr() + "\n";
            
            prntSrm.println(assignment.getChip().getBoard().getName() + " ");// + " " + lb.getChamberNames().iterator().next());                    
        	prntSrm.println(str);
        	prntSrm.print("\n");
        }
    }
    
    public static void main(String[] args) throws DataAccessException {
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);	 

        try {
            //printTimingFromLBConfig(equipmentDAO, configurationManger, configurationDAO) ;
            //createSettings(equipmentDAO, configurationManger, configurationDAO);
            //String fileName = System.getenv("HOME") + "/bin/data/timing/correct_endcapN_2009_11_24.txt";          //TODO  
        	//String fileName = System.getenv("HOME") + "/bin/data/timing/2010_03_19_afterSplashes.txt";          //TODO
        	//String fileName = System.getenv("HOME") + "/bin/data/timing/delays-firstColision7TeV/delaysRPCmuonRequired.txt";          //TODO
        	//String fileName = System.getenv("HOME") + "/bin/data/timing/delays-firstColision7TeV/delaysRPCMuonNotRequired.txt";   
        	//String fileName = System.getenv("HOME") + "/bin/data/timing/delays-firstColision7TeV/delaysAorB_doRun132661.txt"; 
        	//String fileName = "/nfshome0/rpcpro/" + "/bin/data/timing/2015/l1RpcDelays_runs_246930-246951.txt";
        	//String fileName = "/nfshome0/rpcpro/" + "/bin/data/timing/2015/l1RpcDelays_07_2015_runs_251168-251252.txt";
        	//String fileName = "/nfshome0/rpcpro/" + "/bin/data/timing/2015/l1RpcDelaysD4_29_10_2015_257682_258159.txt";
        	//String fileName = "/nfshome0/rpcpro/" + "/bin/data/timing/2016/2016_fill5020_a.txt"; not used
        	//String fileName = "/nfshome0/rpcpro/" + "/bin/data/timing/2017/lbYbx_run297219_2.txt";
        	String fileName = "/nfshome0/rpcpro/" + "/bin/data/timing/2017/l1RpcDelays_297050-297723.txt";
        	
        	//TODO
        	//tranformCorectionFile(fileName, equipmentDAO, configurationManager, configurationDAO);
        	        	        
        	//LocalConfigKey localConfigKey = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_LBS_LHC1);
        	LocalConfigKey localConfigKey = configurationManager.getCurrnetLBsConfigKey();
        	
        	//the tag mechanism is not very good, since it 
        	//configurationManager.tagCurrentConfigs(localConfigKey, ChipType.SYNCODER, "15_06_18_LBS_LHC2", "LB setting used during 2012 colisions and LS1");
        	//configurationManager.tagCurrentConfigs(localConfigKey, ChipType.SYNCODER, "17_07_24_LBS_LHC2", "LB setting used during 2017, before timing update");
        	
        	//printSettingsByTag(configurationManager, configurationDAO, "15_06_18_LBS_LHC2");
        	
            applyCorrectionFromFile(true, fileName, equipmentDAO, configurationManager, configurationDAO, 
            		localConfigKey, //the source of the configuration data, on which the correction will be applied
            		localConfigKey) ; //the key, under which the corrected data will be stored           
          
            //calucateSettingsFromTOFandProp(true, "", "", equipmentDAO, configurationManger, configurationDAO,  configurationManger.getDefaultLocalConfigKey());
       /*    changeCSCDelay(equipmentDAO, configurationManger, configurationDAO, 
                   configurationManger.getDefaultLocalConfigKey(), //the source of the configuration data, on which the correction will be applied
                   configurationManger.getDefaultLocalConfigKey(), true) ; //the key, under which the corrected data will be stored
*/
           
            /*           analyseCorrectionFromFile(fileName, equipmentDAO, configurationManger, configurationDAO, 
                    configurationManger.getDefaultLocalConfigKey()); //the source of the configuration data, on which the correction will be applied
             */ 
           //compareConfigs1(equipmentDAO, configurationManager, configurationDAO) ;
            /*copyMasks(equipmentDAO, configurationManager, configurationDAO, 
            		configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_COSMIC), 
            		configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_DEFAULT) );*/
            //backToPreviousConfigs(true, equipmentDAO, configurationManger, configurationDAO) ;   
            /*analyseConfigs(equipmentDAO, configurationManager, configurationDAO, 
            		configurationManager.getLocalConfigKey(ConfigurationManager.CURRENT_LBS_CONF_KEY)) ;*/
            //printTiming(equipmentDAO, configurationManager, configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_DEFAULT));
           //printConfigsHistory(equipmentDAO, configurationManager, configurationDAO, localConfigKey, 6, 0) ;
           
           //restoreGoodConfigs(true, equipmentDAO, configurationManager, configurationDAO);
           
            //applyTTCFibberCorrection(true, equipmentDAO, configurationManager, configurationDAO) ;
             /*correctSettings(equipmentDAO, configurationManger, configurationDAO, 
                    configurationManger.getDefaultLocalConfigKey(), //the source of the configuration data, on which the correction will be applied
                    configurationManger.getDefaultLocalConfigKey(), true) ; //the key, under which the corrected data will be stored
             */

       //     test();
          //  putTimingToDataTrgDelay(equipmentDAO, configurationManger, configurationDAO, configurationManger.getDefaultLocalConfigKey(), true);
/*            assignConfigsToKey(false, equipmentDAO, configurationManager, configurationDAO, 
            		configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_LHC_COSMIC1), configurationManager.getDefaultLocalConfigKey());
 */       
            //String timingFileName = System.getenv("HOME") + "/bin/data/timing/LHC_final_delays_MK/centerMC-barrel_delays_tofprop-jit-rpc.txt";
//            String timingFileName = System.getenv("HOME") + "/bin/data/timing/LHC_final_delays_MK/centerMC-endcap_delays_tofprop-jit-rpc_Coaxial.txt";
            
            //String timingFileName = System.getenv("HOME") + "/bin/data/timing/LHC_final_delays_MK_corrected/centerMC-endcap_delays_tofprop-jit-rpc_Coaxial.txt";
            //String timingFileName = System.getenv("HOME") + "/bin/data/timing/LHC_final_delays_MK_corrected/centerMC-endcap_delays_tofprop-jit-rpc_Coaxial_selected.txt";
            //String timingFileName = System.getenv("HOME") + "/bin/data/timing/LHC_final_delays_MK_corrected/centerMC-endcap_delays_tofprop-jit-rpc_Coaxial_corrByHandAfterSplash.txt";
            
           /* calcualteSettingsFromTiming(false, timingFileName, equipmentDAO, configurationManager, configurationDAO, 
            		configurationManager.getDefaultLocalConfigKey(), 
            		configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_DEFAULT), 5); */
        	
        	/*LocalConfigKey newLocalConfigKey = configurationManager.createLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_LBS_LHC2, Subsystem.LBS);
            correctSettings(equipmentDAO, configurationManager, configurationDAO, 
            		configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_LBS_LHC1), 
            		newLocalConfigKey, true, -25./2);*/
            
/*            String[] lbNames = {"LB_LBB_904_S6", "LB_LBB_904_M7", "LB_LBB_904_S8"};
            int[] timings = {101, 101, 101};
            createSettingsByHand(equipmentDAO, configurationManager, configurationDAO, lbNames, timings);*/
            
         /*   changeToSpecialMasks(equipmentDAO, configurationManager, configurationDAO, configurationManager.getLocalConfigKey(ConfigurationManager.CURRENT_LBS_CONF_KEY), 
            		configurationManager.getLocalConfigKey(ConfigurationManager.CURRENT_LBS_CONF_KEY),  true);*/
        	
/*        	Calendar updateDate = new GregorianCalendar();
        	updateDate.set(2010, Calendar.APRIL, 23);
        	printAppliedTimingCorrection(equipmentDAO, configurationManager, configurationDAO, 
        			configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_DEFAULT),
            		updateDate);*/
        } 
       /* catch (DataAccessException e) {
            e.printStackTrace();            
            context.rollback();
            System.out.println("context.rollback()");
        } */
        /*  catch (FileNotFoundException e) {            
            e.printStackTrace();
        }*/
        catch (Exception e) {
            e.printStackTrace();            
            context.rollback();
            System.out.println("context.rollback()");
        } 
        finally {
            context.closeSession();
        }
    }

}
