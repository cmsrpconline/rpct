package rpct.testAndUtils.boardConfigs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.BoardDisabledHistory;
import rpct.db.domain.configuration.ChipDisabledHistory;
import rpct.db.domain.configuration.CrateDisabledHistory;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.FebDTControlledHistory;
import rpct.db.domain.configuration.LinkDisabledHistory;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;

public class PrintDisablingHistory {

    /**
     * @param args
     * @throws DataAccessException 
     * @throws IOException 
     */
    public static void main(String[] args) throws DataAccessException, IOException {
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);	 

        try {
            /*Chip chip = equipmentDAO.getChipById(1);
            List<ChipDisabledHistory> chipDisabledHistories = configurationDAO.getChipDisablingHistory(chip);
            for(ChipDisabledHistory chipDisabledHistory :chipDisabledHistories) {
            	System.out.println(chipDisabledHistory);
            }
            
            Board board = equipmentDAO.getBoardById(3013);
            List<BoardDisabledHistory> boardDisabledHistories = configurationDAO.getBoardDisablingHistory(board);
            for(BoardDisabledHistory disabledHistory :boardDisabledHistories) {
            	System.out.println(disabledHistory);
            }
            
            Crate crate = equipmentDAO.getCrateById(200);
            List<CrateDisabledHistory> crateDisabledHistories = configurationDAO.getCrateDisablingHistory(crate);
            for(CrateDisabledHistory disabledHistory : crateDisabledHistories) {
            	System.out.println(disabledHistory);
            }
            
            Board feb = equipmentDAO.getBoardById(4619);
            List<FebDTControlledHistory> dtControlledHistories = configurationDAO.getFebDTControlledHistory(feb);
            for(FebDTControlledHistory disabledHistory :dtControlledHistories) {
            	System.out.println(disabledHistory);
            }
            
            LinkConn linkConn = (LinkConn)equipmentDAO.getLinkConnById(1391);
            List<LinkDisabledHistory> linkDisabledHistories = configurationDAO.getLinkDisabledHistory(linkConn);
            for(LinkDisabledHistory disabledHistory :linkDisabledHistories) {
            	System.out.println(disabledHistory);
            }*/
            
            Board board = equipmentDAO.getBoardByName("TBp3_4");
            List<BoardDisabledHistory> boardDisabledHistories = configurationDAO.getBoardDisablingHistory(board);
            for(BoardDisabledHistory disabledHistory :boardDisabledHistories) {
            	System.out.println(disabledHistory);
            }
            
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            context.rollback();
        }
        finally {
            context.closeSession();
        }
    }

}
