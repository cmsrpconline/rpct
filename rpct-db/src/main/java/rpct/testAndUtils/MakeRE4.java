package rpct.testAndUtils;

import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.axis.types.HexBinary;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.SynCoderConf;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.FebConnector;
import rpct.db.domain.equipment.I2cCbChannel;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.chamberloaction.BarrelOrEndcap;
import rpct.db.domain.equipment.chamberloaction.ChamberLocation;
import rpct.db.domain.equipment.chamberloaction.ChamberLocationDAO;
import rpct.db.domain.equipment.chamberloaction.ChamberLocationDAOHibernate;
import rpct.db.domain.equipment.chamberstrip.ChamberStrip;
import rpct.db.domain.equipment.controlboard.ControlBoard;
import rpct.db.domain.equipment.feblocation.FebLocation;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;

public class MakeRE4 {

	static void makeLboxes(boolean saveToDB, String towerName, EquipmentDAO equipmentDAO, 
			ChamberLocationDAO chamberLocationDAO, ConfigurationDAO configurationDAO, ConfigurationManager configurationManager) throws DataAccessException {
		Map<String, String> lboxNameLabelMap = new TreeMap<String, String>();
		lboxNameLabelMap.put("LBB_YEN3_RE4_S12/1" , "X3V51_f");
		lboxNameLabelMap.put("LBB_YEN3_RE4_S2/3"  , "X4V51_j");
		lboxNameLabelMap.put("LBB_YEN3_RE4_S4/5"  , "X4S51_j");
		lboxNameLabelMap.put("LBB_YEN3_RE4_S6/7"  , "X3S51_d");
		lboxNameLabelMap.put("LBB_YEN3_RE4_S8/9"  , "X2S52_d");
		lboxNameLabelMap.put("LBB_YEN3_RE4_S10/11", "X2V52_g");
		lboxNameLabelMap.put("LBB_YEP3_RE4_S12/1" , "X3J51_f");
		lboxNameLabelMap.put("LBB_YEP3_RE4_S2/3"  , "X4J51_j");
		lboxNameLabelMap.put("LBB_YEP3_RE4_S4/5"  , "X4A51_j");
		lboxNameLabelMap.put("LBB_YEP3_RE4_S6/7"  , "X3A51_i");
		lboxNameLabelMap.put("LBB_YEP3_RE4_S8/9"  , "X2A52_d");
		lboxNameLabelMap.put("LBB_YEP3_RE4_S10/11", "X2J52_e");		
		
		Map<String, List<LinkBox>> linkBoxesByTower = equipmentDAO.getLinkBoxesByTower(equipmentDAO, false);
		List<LinkBox> linkBoxs  = linkBoxesByTower.get(towerName);
				
		if(linkBoxs != null) {
			for(LinkBox linkBox : linkBoxs) {
				System.out.println("old " + linkBox + " " + linkBox.getLabel());
				LinkBox newLinkBox = new LinkBox();
				newLinkBox.setType(CrateType.LINKBOX);
				newLinkBox.setName(linkBox.getName().replace("S", "RE4_S"));
				newLinkBox.setLabel(lboxNameLabelMap.get(newLinkBox.getName()));
				System.out.println("new " + newLinkBox + " " + newLinkBox.getLabel());
				if(saveToDB) {
					equipmentDAO.saveObject(newLinkBox);
				}					
				
				LinkBoard[] boardArray = new LinkBoard[linkBox.slotCount()]; 
				for(Board board : linkBox.getBoards()) {
					//System.out.println(board);
					if(board.getType() == BoardType.LINKBOARD) {
	                    LinkBoard linkBoard = (LinkBoard)board;
	                    
	                    LinkBoard newLinkBoard = new LinkBoard();		                    		                   		                   
	                    newLinkBoard.setType(BoardType.LINKBOARD);	
	                    newLinkBoard.setPosition(linkBoard.getPosition());
	                    String newName = null;
	                    if(linkBoard.getName().contains("RE+3")) {
	                    	newName = linkBoard.getName().replace("RE+3", "RE+4").replace("EP3", "EP4");
	                    }
	                    else if(linkBoard.getName().contains("RE-3")) {
	                    	newName = linkBoard.getName().replace("RE-3", "RE-4").replace("EN3", "EN4");
	                    }
	                    newLinkBoard.setName(newName);
	                    newLinkBoard.setLabel(linkBoard.getLabel().replace("S", "RE4_S"));
	                    if(linkBoard.isMaster()) {
	                    	newLinkBoard.setMasterBoard(newLinkBoard);
	                    }
	                    
	                    newLinkBoard.setCrate(newLinkBox);
	                    boardArray[newLinkBoard.getPosition()] = newLinkBoard;
	                    
	                    if(saveToDB) {
							equipmentDAO.saveObject(newLinkBoard);
						}
	                    System.out.println("newLinkBoard " + newLinkBoard);
	                    //-----------------------
	                    Chip newSynCoderChip = new Chip();
	                    newSynCoderChip.setType(ChipType.SYNCODER);
	                    newSynCoderChip.setPosition(linkBoard.getSynCoder().getPosition());
	                    newSynCoderChip.setBoard(newLinkBoard);
	                    
	                    if(saveToDB) {
							equipmentDAO.saveObject(newSynCoderChip);
							//configurationDAO.getChipConfAssignment(chip, localConfigKey)
							SynCoderConf coderConf = (SynCoderConf)configurationManager.getConfiguration(linkBoard.getSynCoder(), 
									configurationManager.getLocalConfigKey(ConfigurationManager.CURRENT_LBS_CONF_KEY));
							
							SynCoderConf newCoderConf = new SynCoderConf(coderConf);							                                                
							newCoderConf.setInChannelsEna(HexBinary.decode("ffffffffffffffffffffffff"));
							if(saveToDB) {
								configurationDAO.saveObject(newCoderConf);
								configurationManager.assignConfiguration(newSynCoderChip, newCoderConf, ConfigurationManager.CURRENT_LBS_CONF_KEY);
								System.out.println("new configuration for chip " + newLinkBoard.getName() + " was save ");
	    					}
						}
	                    
	                    //------------------------
	                    
	                    if(linkBoard.isMaster()) {		                    	
	                    	List<LinkConn> linkConns = linkBoard.getLinkConns();
	                    	for(LinkConn linkConn : linkConns) {
	                    		System.out.println("old LinkConn " + linkConn);
	                    		LinkConn newLinkConn = new LinkConn();
	                    		newLinkConn.setBoard(newLinkBoard);
	                    		newLinkConn.setCollectorBoard(linkConn.getCollectorBoard());
	                    		int collectorBoardInputNum = -1;
	                    		if(linkConn.getCollectorBoardInputNum() == 3 || linkConn.getCollectorBoardInputNum() == 4) {
	                    			collectorBoardInputNum = linkConn.getCollectorBoardInputNum() + 4;
	                    		}
	                    		else if(linkConn.getCollectorBoardInputNum() == 13 || linkConn.getCollectorBoardInputNum() == 14) {
	                    			collectorBoardInputNum = linkConn.getCollectorBoardInputNum() -2;
	                    		}
	                    		
	                    		if( ((TriggerBoard)linkConn.getCollectorBoard()).getLinkConn(collectorBoardInputNum) != null) {
	                    			throw new RuntimeException(linkConn.getCollectorBoard() + " inputNum " +collectorBoardInputNum + " already occupied");
	                    		}
	                    		
	                    		newLinkConn.setCollectorBoardInputNum(collectorBoardInputNum);
	                    		newLinkConn.setType(linkConn.getType());
	                    		System.out.println("new LinkConn " + newLinkConn);
	                    		if(saveToDB) {
									equipmentDAO.saveObject(newLinkConn);
								}
	                    	}
	                    	//-------------------------------------
	                    	System.out.println();
	                    }
	                    List<ChamberLocation> chamberLocationsForLb = linkBoard.getChamberLocations();
                    	for(ChamberLocation chamberLocation : chamberLocationsForLb) {
            				System.out.println(chamberLocation);
            				
            				String newChamberName = "";
            				if(chamberLocation.getDiskOrWheel() == -3) {
            					newChamberName = chamberLocation.getChamberLocationName().replace("RE-3", "RE-4");
            				}
            				else if(chamberLocation.getDiskOrWheel() == 3) {
            					newChamberName = chamberLocation.getChamberLocationName().replace("RE+3", "RE+4");
            				}
            				else {
            					throw new RuntimeException("not good diskOrWheel");
            				}
            				
            				ChamberLocation newChamberLocation = chamberLocationDAO.getByName(newChamberName);
            				System.out.println(newChamberLocation);
            				
            				List<FebLocation> newFebLocations = newChamberLocation.getFEBLocations();
            				for(FebLocation febLocation : newFebLocations) {	            					
            					if(saveToDB) {
            						febLocation.setLinkBoard(newLinkBoard);
    								equipmentDAO.saveObject(febLocation);
    							}
            					System.out.println(febLocation);
            					
            					for(FebConnector connector : febLocation.getFebConnectors() ){
            						int linkBoardInputNum = -1;
            						if(febLocation.getFebLocalEtaPartition().equals("A") ){
            							linkBoardInputNum = 0;
            						}
            						if(febLocation.getFebLocalEtaPartition().equals("B") ){
            							linkBoardInputNum = 1;
            						}
            						if(febLocation.getFebLocalEtaPartition().equals("C") ){
            							linkBoardInputNum = 2;
            						}
            						linkBoardInputNum = 2 * linkBoardInputNum + connector.getFebConnectorNum();
            						if(saveToDB) {
            							connector.setLinkBoardInputNum(linkBoardInputNum);
	    								equipmentDAO.saveObject(connector);
	    							}
            						System.out.println(connector + "  new linkBoardInputNum " + linkBoardInputNum);
            					}
            				}
            				
            				/*System.out.println("old febLocations");
            				List<FebLocation> febLocations = chamberLocation.getFEBLocations();
            				for(FebLocation febLocation : febLocations) {
            					System.out.println(febLocation);
            					for(FebConnector connector : febLocation.getFebConnectors() ){
            						System.out.println(connector);
            					}
            				}*/
            				System.out.println();
            				
            				
            			}
                    	//-------------------------------------
                    	System.out.println();
	                }
					else if(board.getType() == BoardType.CONTROLBOARD) {
						System.out.println("old " + board);
						ControlBoard controlBoard  = (ControlBoard)board;
						
						ControlBoard newControlBoard = new ControlBoard();
						
						newControlBoard.setName(controlBoard.getName().replace("S", "RE4_S"));
						newControlBoard.setCrate(newLinkBox);
						newControlBoard.setPosition(controlBoard.getPosition());
						newControlBoard.setType(BoardType.CONTROLBOARD);
						newControlBoard.setLabel(newControlBoard.getName());
						
						newControlBoard.setCcsBoard(controlBoard.getCcsBoard());
						newControlBoard.setFecPosition(controlBoard.getFecPosition());
						newControlBoard.setPositionInCcuChain(controlBoard.getPositionInCcuChain() + 6);
						newControlBoard.setCcuAddress(controlBoard.getCcuAddress() + 0x30);
						System.out.println("new " + newControlBoard);
						if(saveToDB) {
							equipmentDAO.saveObject(newControlBoard);
						}
						System.out.println();
	                }
					else if(board.getType() == BoardType.FEBBOARD) {
	                    
	                }
											
				}
				
				for(LinkBoard newLinkBoard : boardArray) {
					if(newLinkBoard != null) {							
						Board master = newLinkBoard.getMasterBoard();
						if(master == null) {
							if(newLinkBoard.getName().contains("CH1") ) {
								master = boardArray[newLinkBoard.getPosition() +1];
							}
							if(newLinkBoard.getName().contains("CH2") ) {
								master = boardArray[newLinkBoard.getPosition() -1];
							}
							if(master != null) {
								newLinkBoard.setMasterBoard(master);
								if(saveToDB) {
									equipmentDAO.saveObject(newLinkBoard);
								}
							}
							else {
								throw new RuntimeException("master is null");
							}
						}

						//System.out.println(newLinkBoard + " master is " + newLinkBoard.getMaster().getName());
					}
				}
			}
		}
	}
	
	static void reverseRE4Chambers(boolean saveToDB, EquipmentDAO equipmentDAO, ChamberLocationDAO chamberLocationDAO) throws DataAccessException {
		List<ChamberLocation> chamberLocationsREp4 = chamberLocationDAO.getByDiskOrWheel(4, BarrelOrEndcap.Endcap);
		List<ChamberLocation> chamberLocationsREn4 = chamberLocationDAO.getByDiskOrWheel(-4, BarrelOrEndcap.Endcap);
		
		List<ChamberLocation> chamberLocations = new ArrayList<ChamberLocation>();
		chamberLocations.addAll(chamberLocationsREn4);
		chamberLocations.addAll(chamberLocationsREp4);
		
		//List<ChamberLocation> chamberLocations = chamberLocationDAO.getByLayer(-2, 2, BarrelOrEndcap.Endcap);
		//List<ChamberLocation> chamberLocations = chamberLocationDAO.getByLocation(-4, 3, 1, "",  BarrelOrEndcap.Endcap);
		for(ChamberLocation chamberLocation : chamberLocations) {
			if(saveToDB) {
				if(chamberLocation.getFEBZOrnt().equals("-z")) {
					chamberLocation.setFEBZOrnt("+z");
				}
				else if(chamberLocation.getFEBZOrnt().equals("+z")) {
					chamberLocation.setFEBZOrnt("-z");
				}
				else {
					throw new RuntimeException("not correct FEBZOrnt");
				}

				equipmentDAO.saveObject(chamberLocation);
			}
			
			System.out.println(chamberLocation);
			List<FebLocation> febLocations = chamberLocation.getFEBLocations();
			for(FebLocation febLocation : febLocations) {
				System.out.println(febLocation);
				for(FebConnector connector : febLocation.getFebConnectors() ){
					System.out.println(connector);
					Map<Integer, ChamberStrip> chamberStripMap = new TreeMap<Integer, ChamberStrip>();
					for(ChamberStrip chamberStrip : connector.getChamberStrips()) {						
						/*if(chamberStrip.getCmsStripNumber() != chamberStrip.getChamberStripNumber()) {
							throw new RuntimeException("chamberStrip.getCmsStripNumber() != chamberStrip.getChamberStripNumber()");
						}*/
						if(saveToDB) {							
							chamberStrip.setCmsStripNumber(33 - chamberStrip.getCmsStripNumber());						
							equipmentDAO.saveObject(chamberStrip);
						}
						chamberStripMap.put(chamberStrip.getChamberStripNumber(), chamberStrip);
					}
					for(Map.Entry<Integer, ChamberStrip> e : chamberStripMap.entrySet()) {
						System.out.println("  " + e.getValue());
						//break;
					}
				}
				System.out.println();
			}
			System.out.println();
		}
	}
	
	static void makeFebBoards(boolean saveToDB, String towerName, EquipmentDAO equipmentDAO, ChamberLocationDAO chamberLocationDAO) throws DataAccessException {
		Map<String, List<LinkBox>> linkBoxesByTower = equipmentDAO.getLinkBoxesByTower(equipmentDAO, false);
		List<LinkBox> linkBoxs  = linkBoxesByTower.get(towerName);
								
		if(linkBoxs != null) {
			for(LinkBox linkBox : linkBoxs) {
				if(linkBox.getName().contains("RE4")) 
				{
					int cbChannel = 0;
					for(Board board : linkBox.getBoardArray()) {											
						if(board != null && board.getType() == BoardType.LINKBOARD) {							
							LinkBoard linkBoard = (LinkBoard)board;
							if(linkBoard.getChamberLocations().size() != 1) {
								throw new RuntimeException("linkBoard.getChamberLocations().size() != 1");
							}
							
							if(cbChannel == 6)
								cbChannel = 0;							
							cbChannel++;
							
							ControlBoard cb = linkBoard.getControlBoard();
							ChamberLocation chamberLocation = linkBoard.getChamberLocations().get(0);
							System.out.println(linkBoard + " " + chamberLocation);
							//System.out.println(chamberLocation);
							
							I2cCbChannel i2cCbChannel = new I2cCbChannel();
					    	i2cCbChannel.setControlBoard(cb);
					    	i2cCbChannel.setCbChannel(cbChannel);
					    	
					    	if(saveToDB)
					    		equipmentDAO.saveObject(i2cCbChannel);
					    	
					    	System.out.println("added I2cCbChannel " + i2cCbChannel.toString());
					    	
							for(FebLocation febLocation : chamberLocation.getFEBLocations()) {															
					    		if(cb == null)
					    			throw new RuntimeException("cb == null");
					    		
					    		if(saveToDB) {
					    			febLocation.setI2cCbChannel(i2cCbChannel);					    			
					    			equipmentDAO.saveObject(febLocation);
					    								    			
					    			String name = "FEB_" + febLocation.getChamberLocation().getChamberLocationName() + "_" + 
					    					i2cCbChannel.getCbChannel() + "_" + febLocation.getI2cLocalNumber();

					    			int position = cb.getPosition() * 100 + i2cCbChannel.getCbChannel() * 10 + febLocation.getI2cLocalNumber();					    		
					    			int chipCnt = 4;					    		
					    			FEBsGenerator.createFebBoard(saveToDB, equipmentDAO, febLocation, linkBox, name, position, chipCnt);
					    		}
					    		
					    		System.out.println(" " + febLocation);
					    	}
						}
					}
				}
				System.out.println();
			}
		}
	}
	/**
	 * @param args
	 * @throws DataAccessException 
	 */
	public static void main(String[] args) throws DataAccessException {

		HibernateContext context = new SimpleHibernateContextImpl();
		EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
		ChamberLocationDAO chamberLocationDAO = new ChamberLocationDAOHibernate(context);
		ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
		ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);
		
		//List<Board> lbs;

		try {
			boolean saveToDB = true;	
			String[] towerNames = {
					"YEP3_near",
					"YEP3_far",
					"YEN3_near",
					"YEN3_far"
			};
			/*for(String towerName : towerNames) {
				makeLboxes(saveToDB, towerName, equipmentDAO, chamberLocationDAO, configurationDAO, configurationManager); 				
			}*/
			/*for(String towerName : towerNames) {
				makeFebBoards(saveToDB, towerName, equipmentDAO, chamberLocationDAO);
			}*/
			
			reverseRE4Chambers(saveToDB, equipmentDAO, chamberLocationDAO);
				
			
			
		}        
		catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			context.rollback();
		} 
		catch (Error e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			context.rollback();
		}
		finally {
			context.closeSession();
		}
	}

}
