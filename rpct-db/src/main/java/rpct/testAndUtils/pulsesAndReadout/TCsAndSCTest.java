package rpct.testAndUtils.pulsesAndReadout;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;

import rpct.datastream.MonitorManager;
import rpct.datastream.TestOptionsXml;
import rpct.datastream.TestPresets;
import rpct.datastream.TestPulsesGenerator;
import rpct.datastream.TestsManager;
import rpct.datastream.TestOptionsXml.ChipSource;
import rpct.datastream.TestOptionsXml.HardwareType;
import rpct.datastream.TestOptionsXml.SourcesList;
import rpct.datastream.TestPresets.ReapetPulserPretrigger0_SnapshotPretrigger0;
import rpct.datastream.TestPresets.TestPreset;
import rpct.datastream.TestsManager.PulserTarget;
import rpct.db.DataAccessException;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.SorterCrate;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.diag.TriggerDataSel;
import rpct.xdaqaccess.diag.DiagCtrl.TriggerType;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;

public class TCsAndSCTest {

    public static void main(String[] args) throws ServiceException,
    DataAccessException, IOException, InterruptedException,
    XdaqException, HardwareDbException {
        long bxInSec = 40000000;

        String xmlFileName =  System.getenv("HOME") + "/bin/data/genTestpulses_opto.xml";
        //String xmlFileName =  System.getenv("HOME") + "/bin/data/eventData.xml";

        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(
                new HardwareRegistry(), context);

        //TestPresets.TestPreset preset = new TestPresets.SinglePulser_SnapshotPretrigger0(); //ReapetPulserBC0_SnapshotBC0
        //TestPresets.TestPreset preset = new TestPresets.ReapetPulserBC0_SnapshotBC0(); 
       TestPresets.TestPreset preset = new TestPresets.ReapetPulserPretrigger0_SnapshotPretrigger0();
        //TestPresets.TestPreset preset = new TestPresets.SinglePulser_SnapshotL1A();
      //  TestPresets.TestPreset preset = new TestPresets.ReapetPulserBC0_DaqLike();
        try {
            PulserTarget pulseTarget = PulserTarget.chipOut; //TODO            
            TestOptionsXml testOptions = new TestOptionsXml();
            
            TestPulsesGenerator generator = new TestPulsesGenerator();
            int[] tcNum = { 
                    //0,
                    //1,
                    //2, 
                    9, 
                    //10, 
                    //11
                    };
            //generator.generateLinkData(tcNum); //TODO to jest generoawny plik z pulsami            
                                     
            
            
            for(int tc : tcNum) {
                testOptions.addChips("pulsers", HardwareType.CRATE, "TC_" + tc, ChipType.OPTO);
                
                testOptions.addChips("readouts", HardwareType.CRATE, "TC_" + tc, ChipType.PAC); 
                testOptions.addChips("readouts", HardwareType.CRATE, "TC_" + tc, ChipType.GBSORT); 
                testOptions.addChips("readouts", HardwareType.CRATE, "TC_" + tc, ChipType.TCSORT); 
            }
            
            
            //testOptions.addChips("readouts", HardwareType.CRATE, "SC", ChipType.HALFSORTER);
            //testOptions.addChips("readouts", HardwareType.BOARD, "HSB_0", ChipType.HALFSORTER);
            //testOptions.addChips("readouts", HardwareType.BOARD, "HSB_1", ChipType.HALFSORTER);
            
           // testOptions.addChips("readouts", HardwareType.CRATE, "SC", ChipType.FINALSORTER);
            
            List<Chip> chips = testOptions.getChips("readouts", equipmentDAO);
            
            TestsManager testsManager = new TestsManager(xmlFileName, preset, testOptions, equipmentDAO, null, dbMapper);
            testsManager.setPulsers();
            testsManager.setPulseTarget(pulseTarget);
            testsManager.setReadeouts();
                        		
            //MonitorManager monitorManager = new MonitorManager();            
            
            //monitorManager.addChips(chips, dbMapper);
            //testsManager.setMonitorManager(monitorManager);
            //monitorManager.enableTransmissionCheck(false, false); //TODO

            while(true) {
                generator.generateLinkDataRnd1(tcNum, 1);
                //generator.generateLinkData2(tcNum);
                generator.writeFile(xmlFileName); 
                testsManager.interConnectionTest(false, false, 0, 0);

                //testsManager.daqPulserTest(true, true);
                //testsManager.algorithmsTest();
                
                System.out.println("Reapeat? (y/n)");
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                String y_n = br.readLine();
                if(y_n.equals("n"))
                    break;
            }
            //monitorManager.enableTest(true);
        } catch (JAXBException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            context.closeSession();
        }
    }

}
