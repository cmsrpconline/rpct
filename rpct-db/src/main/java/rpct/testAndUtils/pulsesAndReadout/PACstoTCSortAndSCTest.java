package rpct.testAndUtils.pulsesAndReadout;

import java.io.IOException;

import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;

import rpct.datastream.TestOptionsXml;
import rpct.datastream.TestOptionsXml.EventBuilderOptions.ChipDataSel;
import rpct.datastream.TestOptionsXml.HardwareType;
import rpct.datastream.TestPresets;
import rpct.datastream.TestPulsesGenerator;
import rpct.datastream.TestsManager;
import rpct.datastream.TestsManager.PulserTarget;
import rpct.db.DataAccessException;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;

public class PACstoTCSortAndSCTest {

    public static void main(String[] args) throws ServiceException,
    DataAccessException, IOException, InterruptedException,
    XdaqException, HardwareDbException {
        long bxInSec = 40000000;

        //String xmlFileName =  System.getenv("HOME") + "/bin/data/genTestpulses.xml";
        String xmlFileName =  System.getenv("HOME") + "/bin/data/0_RPCBbx0_RPCFbx1_events.xml";
        //String xmlFileName =  System.getenv("HOME") + "/bin/data/eventData.xml";
        //String xmlFileName =  System.getenv("HOME") + "/bin/data/event_961850917_1.xml";
        
        boolean testPacTBGbs =  false; //true = testPacTBGbs, fasle = testTBGbs - TCGBs
        
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(
                new HardwareRegistry(), context);

        TestPresets.TestPreset preset = new TestPresets.SinglePulser_SnapshotPretrigger0(); //ReapetPulserBC0_SnapshotBC0
        //TestPresets.TestPreset preset = new TestPresets.ReapetPulserBC0_SnapshotBC0(); 
        //TestPresets.TestPreset preset = new TestPresets.ReapetPulserPretrigger0_SnapshotPretrigger0();
        //TestPresets.TestPreset preset = new TestPresets.LongPulser_SnapshotPretrigger0(128, 0xffffffffffl);
        //TestPresets.TestPreset preset = new TestPresets.SinglePulser_SnapshotL1A();
        //  TestPresets.TestPreset preset = new TestPresets.ReapetPulserBC0_DaqLike();
        try {
            PulserTarget pulseTarget = PulserTarget.chipOut; //TODO            
            
            TestPulsesGenerator generator = new TestPulsesGenerator();
            
            String[] tcs= { 
//                     "TC_0",
                     "TC_1",
//                     "TC_2",
//                     "TC_3",
//                     "TC_4",
//                     "TC_5",
//                     "TC_6",
//                     "TC_7",
//                     "TC_8",                             
//                     "TC_9",
//                     "TC_10",
//                     "TC_11",
                     //"PASTEURA_TC"                  
             };
            
            int[] tcNum = new int[tcs.length];
            for(int i = 0; i < tcs.length; i++) {
                if(tcs[i].equals("PASTEURA_TC")) {
                    tcNum[i] = 9; 
                }
                else {
                    tcNum[i] = Integer.parseInt(tcs[i].substring(3));
                }
            }
            
            //generator.generatePACOutRnd(tcNum, 120);
            generator.generatePACBXnum(tcNum);
            //generator.writeFile(xmlFileName);                          

            TestOptionsXml testOptions = new TestOptionsXml();

            for(String tc : tcs) { 

                System.out.println("testing crate " + tc);
                testOptions.addChips("pulsers", HardwareType.CRATE, tc, ChipType.OPTO);
                //testOptions.addChips("pulsers", HardwareType.CRATE, tc, ChipType.PAC);

                testOptions.addChips("readouts", HardwareType.CRATE, tc, ChipType.PAC); 
                testOptions.addChips("readouts", HardwareType.CRATE, tc, ChipType.GBSORT);

                if(testPacTBGbs) { 
                    testOptions.getEventBuilderOptions().setTbsDataSel(ChipDataSel.in);
                }   
                else {
                    testOptions.addChips("readouts", HardwareType.CRATE,  tc, ChipType.TCSORT); 
                    testOptions.getEventBuilderOptions().setTbsDataSel(ChipDataSel.inAndOut);
                    testOptions.getEventBuilderOptions().setTcsDataSel(ChipDataSel.in);
                }
            }

            //testOptions.addChips("readouts", HardwareType.CRATE, "SC", ChipType.HALFSORTER);
            testOptions.addChips("readouts", HardwareType.BOARD, "HSB_0", ChipType.HALFSORTER);
            testOptions.addChips("readouts", HardwareType.BOARD, "HSB_1", ChipType.HALFSORTER);

            testOptions.addChips("readouts", HardwareType.CRATE, "SC", ChipType.FINALSORTER);
            
            TestsManager testsManager = new TestsManager(xmlFileName, preset, testOptions, equipmentDAO, null, dbMapper);
            testsManager.setPulsers();
            testsManager.setPulseTarget(pulseTarget);
            testsManager.setReadeouts();


            //MonitorManager monitorManager = new MonitorManager();            

            //monitorManager.addChips(chips, dbMapper);
            //testsManager.setMonitorManager(monitorManager);
            //monitorManager.enableTransmissionCheck(false, false); //TODO

            boolean comapareWithSource = false;
            if(testPacTBGbs) 
                comapareWithSource = true;

            testsManager.interConnectionTest(comapareWithSource, false, 0, 1);
            //testsManager.daqPulserTest(comapareWithSource, true);
            //testsManager.algorithmsTest();

            //monitorManager.enableTest(true);

            //testsManager.


        } catch (JAXBException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            context.closeSession();
        }
    }

}
