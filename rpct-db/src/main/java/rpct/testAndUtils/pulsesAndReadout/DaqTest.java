package rpct.testAndUtils.pulsesAndReadout;

import java.io.IOException;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;

import rpct.datastream.MonitorManager;
import rpct.datastream.TestOptionsXml;
import rpct.datastream.TestPresets;
import rpct.datastream.TestsManager;
import rpct.datastream.TestOptionsXml.HardwareType;
import rpct.datastream.TestPresets.NoPulser_SnapshotL1A;
import rpct.datastream.TestPresets.TestPreset;
import rpct.datastream.TestsManager.PulserTarget;
import rpct.db.DataAccessException;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;

public class DaqTest {

    public static void main(String[] args) throws ServiceException,
    DataAccessException, IOException, InterruptedException,
    XdaqException, HardwareDbException {
        String xmlFileName =  System.getenv("HOME") + "/bin/data/gentestpulses.xml";

        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(
                new HardwareRegistry(), context);

        //TestPresets.TestPreset preset = new TestPresets.SinglePulser_SnapshotPretrigger0(); //ReapetPulserBC0_SnapshotBC0
        //TestPresets.TestPreset preset = new TestPresets.ReapetPulserBC0_SnapshotBC0(); 
        //TestPresets.TestPreset preset = new TestPresets.NoPulser_DaqLike(5);
        TestPresets.TestPreset preset = new TestPresets.NoPulser_SnapshotL1A(1);
        
        try {
            PulserTarget pulseTarget = PulserTarget.chipOut; //TODO            
            TestOptionsXml testOptions = new TestOptionsXml();
                         
            //testOptions.addChips("pulsers", HardwareType.CRATE, "TC_9", ChipType.OPTO);
            //testOptions.addChips("pulsers", HardwareType.CRATE, "TC_1", ChipType.OPTO);
                        
            //testOptions.addChips("readouts", HardwareType.CRATE, "TC_9", ChipType.RMB);
            testOptions.addChips("readouts", HardwareType.CRATE, "TC_1", ChipType.RMB);
            
            //testOptions.addChips("readouts", HardwareType.CRATE, "TC_9", ChipType.PAC);
            //testOptions.addChips("readouts", HardwareType.CRATE, "TC_1", ChipType.PAC);
            
            TestsManager testsManager = new TestsManager(xmlFileName, preset, testOptions, equipmentDAO, null, dbMapper);
            //testsManager.setPulsers();
            testsManager.setPulseTarget(pulseTarget);
            testsManager.setReadeouts();
                        		
            MonitorManager monitorManager = new MonitorManager();            
            List<Chip> chips = testOptions.getChips("readouts", equipmentDAO);
            monitorManager.addChips(chips, dbMapper);
            testsManager.setMonitorManager(monitorManager);
            //monitorManager.enableTransmissionCheck(false, false); //TODO

            //testsManager.interConnectionTest(true, false);
            testsManager.singleRedaouts();
            //pulsesManager.algorithmsTest();

            monitorManager.readRecErrorCnt();
        } catch (JAXBException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            context.closeSession();
        }
    }

}
