package rpct.testAndUtils.pulsesAndReadout;

import java.io.IOException;

import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;

import rpct.datastream.TestOptionsXml;
import rpct.datastream.TestOptionsXml.EventBuilderOptions.ChipDataSel;
import rpct.datastream.TestOptionsXml.HardwareType;
import rpct.datastream.TestPresets;
import rpct.datastream.TestPulsesGenerator;
import rpct.datastream.TestsManager;
import rpct.datastream.TestsManager.PulserTarget;
import rpct.db.DataAccessException;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;

public class PulsesFromOptoTest {

    public static void main(String[] args) throws ServiceException,
    DataAccessException, IOException, InterruptedException,
    XdaqException, HardwareDbException {
        long bxInSec = 40000000;
        boolean generatePulses = false; //TODO
        
        //String xmlFileName =  System.getenv("HOME") + "/bin/data/myEvent119094.xml"; //TODO
        String xmlFileName =  System.getenv("HOME") + "/bin/data/myEvent119094_1762042707_scan.xml"; //TODO
        
        if(generatePulses)
            xmlFileName =  System.getenv("HOME") + "/bin/data/genTestpulses.xml";
        
        boolean comapareWithSource = false; //TODO
        
        TestOptionsXml testOptions = new TestOptionsXml();
        
        boolean readoutPacs =  true; //TODO
        testOptions.getEventBuilderOptions().setPacDataSel(ChipDataSel.inAndOut); //TODO
        
        boolean readoutTBGBS =  true; //TODO
        testOptions.getEventBuilderOptions().setTbsDataSel(ChipDataSel.inAndOut); //TODO
                
        boolean readoutTCGBS =  true; //TODO
        testOptions.getEventBuilderOptions().setTcsDataSel(ChipDataSel.inAndOut); //TODO
        
        boolean readoutHalfGBS =  true; //TODO
        testOptions.getEventBuilderOptions().setHsDataSel(ChipDataSel.inAndOut); //TODO
        
        boolean readoutFinalGBS =  true; //TODO
        testOptions.getEventBuilderOptions().setFsDataSel(ChipDataSel.inAndOut);//TODO
        
        TestPresets.TestPreset preset = new TestPresets.SinglePulser_SnapshotPretrigger0(); //ReapetPulserBC0_SnapshotBC0 //TODO
        //TestPresets.TestPreset preset = new TestPresets.ReapetPulserBC0_SnapshotBC0(); 
        //TestPresets.TestPreset preset = new TestPresets.ReapetPulserPretrigger0_SnapshotPretrigger0();
        //TestPresets.TestPreset preset = new TestPresets.LongPulser_SnapshotPretrigger0(128, 0xffffffffffl);
        //TestPresets.TestPreset preset = new TestPresets.SinglePulser_SnapshotL1A();
        //TestPresets.TestPreset preset = new TestPresets.ReapetPulserBC0_DaqLike();
        
        
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);

        try {
            PulserTarget pulseTarget = PulserTarget.chipOut;            
            
            TestPulsesGenerator generator = new TestPulsesGenerator();
            
            String[] tcs= { //TODO
                     "TC_0",
                     "TC_1",
                     "TC_2",
                     "TC_3",
                     "TC_4",
                     "TC_5",
                     "TC_6",
                     "TC_7",
                     "TC_8",                             
                     "TC_9",
                     "TC_10",
                     "TC_11",
                     //"PASTEURA_TC"                  
             };
            
            int[] tcNum = new int[tcs.length];
            for(int i = 0; i < tcs.length; i++) {
                if(tcs[i].equals("PASTEURA_TC")) {
                    tcNum[i] = 9; 
                }
                else {
                    tcNum[i] = Integer.parseInt(tcs[i].substring(3));
                }
            }
            
            if(generatePulses) { //TODO
                //generator.generatePACOutRnd(tcNum, 120);
                generator.generatePACBXnum(tcNum);
                //generator.writeFile(xmlFileName);                          
            }
            
            

            for(String tc : tcs) { 
                System.out.println("testing crate " + tc);
                testOptions.addChips("pulsers", HardwareType.CRATE, tc, ChipType.OPTO);
                //testOptions.addChips("pulsers", HardwareType.CRATE, tc, ChipType.PAC);

                if(readoutPacs)
                    testOptions.addChips("readouts", HardwareType.CRATE, tc, ChipType.PAC);
                
                if(readoutTBGBS)
                    testOptions.addChips("readouts", HardwareType.CRATE, tc, ChipType.GBSORT);
                
                if(readoutTCGBS)
                    testOptions.addChips("readouts", HardwareType.CRATE,  tc, ChipType.TCSORT);
            }

            if(readoutHalfGBS) {
                testOptions.addChips("readouts", HardwareType.BOARD, "HSB_0", ChipType.HALFSORTER);
                testOptions.addChips("readouts", HardwareType.BOARD, "HSB_1", ChipType.HALFSORTER);
            }
            if(readoutFinalGBS)
                testOptions.addChips("readouts", HardwareType.CRATE, "SC", ChipType.FINALSORTER);
            
            TestsManager testsManager = new TestsManager(xmlFileName, preset, testOptions, equipmentDAO, null, dbMapper);
            testsManager.setPulsers();
            testsManager.setPulseTarget(pulseTarget);
            testsManager.setReadeouts();


            //MonitorManager monitorManager = new MonitorManager();            

            //monitorManager.addChips(chips, dbMapper);
            //testsManager.setMonitorManager(monitorManager);
            //monitorManager.enableTransmissionCheck(false, false); //TODO

            testsManager.interConnectionTest(comapareWithSource, false, 0, 1);
            //testsManager.daqPulserTest(comapareWithSource, true);
            //testsManager.algorithmsTest();

            //monitorManager.enableTest(true);

            //testsManager.


        } catch (JAXBException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            context.closeSession();
        }
    }

}
