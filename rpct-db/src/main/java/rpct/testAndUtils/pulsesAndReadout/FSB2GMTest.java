package rpct.testAndUtils.pulsesAndReadout;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;

import rpct.datastream.MonitorManager;
import rpct.datastream.TestOptionsXml;
import rpct.datastream.TestPresets;
import rpct.datastream.TestPulsesGenerator;
import rpct.datastream.TestsManager;
import rpct.datastream.TestOptionsXml.ChipSource;
import rpct.datastream.TestOptionsXml.HardwareType;
import rpct.datastream.TestOptionsXml.SourcesList;
import rpct.datastream.TestPresets.ReapetPulserBC0_SnapshotBC0;
import rpct.datastream.TestPresets.TestPreset;
import rpct.datastream.TestsManager.PulserTarget;
import rpct.db.DataAccessException;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.SorterCrate;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.diag.TriggerDataSel;
import rpct.xdaqaccess.diag.DiagCtrl.TriggerType;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;

public class FSB2GMTest {

    public static void main(String[] args) throws ServiceException,
    DataAccessException, IOException, InterruptedException,
    XdaqException, HardwareDbException {
        long bxInSec = 40000000;

        //String xmlFileName = "/nfshome0/rpcdev/bin/data/testBxDataTC9HPT.xml"; //TODO
        //String xmlFileName = "/nfshome0/rpcdev/bin/data/gentestpulses.xml";
        //String xmlFileName = "/nfshome0/rpcdev/bin/data/testBxData.xml";
        //String xmlFileName = "/nfshome0/rpcdev/bin/data/testpulses2.xml";
        String xmlFileName =  System.getenv("HOME") + "/bin/data/genTestpulses_sorts.xml";
        //String xmlFileName = "/nfshome0/rpcdev/bin/data/wo_conv.xml";
        //String xmlFileName = "/nfshome0/rpcdev/bin/data/wo42_conv.xml";
        //String xmlFileName = "/nfshome0/rpcdev/bin/data/wz_conv.xml";
        //String xmlFileName = "/nfshome0/rpcdev/bin/data/2007.07.TestGMTConnection/testpulsesFMT0.xml";
        //String xmlFileName = "/nfshome0/rpcdev/bin/data/2007.07.TestGMTConnection/testpulsesFSB8mu.xml";
        //String xmlFileName = "/nfshome0/rpcdev/bin/data/2007.07.TestGMTConnection/testpulsesAddr.xml";
        //String xmlFileName = "/nfshome0/rpcdev/bin/data/2007.07.TestSorters/testpulsesFMT112.xml";

        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(
                new HardwareRegistry(), context);

        //TestPresets.TestPreset preset = new TestPresets.SinglePulser_SnapshotPretrigger0(); //ReapetPulserBC0_SnapshotBC0
        TestPresets.TestPreset preset = new TestPresets.ReapetPulserBC0_SnapshotBC0(13*256-1); 
        //TestPresets.TestPreset preset = new TestPresets.ReapetPulserPretrigger0_SnapshotPretrigger0();
        try {
            PulserTarget pulseTarget = PulserTarget.chipOut; //TODO            
            TestOptionsXml testOptions = new TestOptionsXml();
            
            TestPulsesGenerator generator = new TestPulsesGenerator();
            generator.generateFinalSortOutWalking1();
            //generator.generateFinalSortOutWalking0();
            //generator.generateFinalSortOutRND();
            generator.writeFile(xmlFileName);                          
                        
            testOptions.addChips("pulsers", HardwareType.BOARD, "FSB", ChipType.FINALSORTER);  
            testOptions.addChips("readouts", HardwareType.BOARD, "FSB", ChipType.FINALSORTER); 
                        
            TestsManager testsManager = new TestsManager(xmlFileName, preset, testOptions, equipmentDAO, null, dbMapper);
            testsManager.setPulsers();
            testsManager.setPulseTarget(pulseTarget);
            testsManager.setReadeouts();
                        	
            //testsManager.singleRedaouts();
            //testsManager.interConnectionTest(false, false, false);
            testsManager.daqPulserTest(false, false);
            //testsManager.algorithmsTest();

            //monitorManager.enableTest(true);
        } catch (JAXBException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            context.closeSession();
        }
    }

}
