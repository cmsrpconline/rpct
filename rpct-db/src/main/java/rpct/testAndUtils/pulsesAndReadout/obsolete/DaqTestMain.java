package rpct.testAndUtils.pulsesAndReadout.obsolete;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;

import rpct.datastream.MonitorManager;
import rpct.datastream.TestOptionsXml;
import rpct.datastream.TestPresets;
import rpct.datastream.TestsManager;
import rpct.datastream.TestPresets.NoPulser_SnapshotL1A;
import rpct.datastream.TestPresets.TestPreset;
import rpct.datastream.TestsManager.PulserTarget;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.SorterCrate;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.diag.TriggerDataSel;
import rpct.xdaqaccess.diag.DiagCtrl.TriggerType;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbCrate;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;

public class DaqTestMain {

    /**
     * @param args
     * @throws DataAccessException 
     * @throws HardwareDbException 
     */
    public static void main(String[] args) throws DataAccessException, HardwareDbException {
        long bxInSec = 40000000;

        //String xmlFileName = System.getenv("HOME") + "/bin/data/dummyBxData.xml";  
        //String xmlFileName =  System.getenv("HOME") + "/bin/data/bxData-L1A_TBp2_9.xml";
        //String xmlFileName =  System.getenv("HOME") + "/bin/data/bxData-L1A_from_TBp2_9.xml";
        //String xmlFileName = System.getenv("HOME") + "/bin/data/otpLinkRndTestPulses.xml";
        String xmlFileName =  System.getenv("HOME") + "/bin/data/gentestpulses.xml";
        // String xmlFileName = "/nfshome0/rpcdev/bin/data/dummyBxData-small.xml";
        //String xmlFileName = "/nfshome0/rpcdev/bin/data/eventData.xml";
        //String xmlFileName = "/nfshome0/rpcdev/bin/data/testBxData.xml";
        TestsManager.PulserTarget pulseTarget = TestsManager.PulserTarget.chipOut;

        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);

        //ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        //ConfigurationManger configurationManger = new ConfigurationManger(context, equipmentDAO, configurationDAO);
        ConfigurationManager configurationManager = null;
        /*TestsManager pulsesManager = new TestsManager(xmlFileName,
                pulseLenght, pulseReapet, pulserLimit,
                readoutLimit, pulserStartTriggerType, readoutStartTriggerType,
                triggerDataSel, equipmentDAO);*/

        //TestPresets.TestPreset preset = new TestPresets.ReapetPulserBC0_SnapshotBC0(); //ReapetPulserBC0_SnapshotBC0

        //TestPresets.TestPreset preset = new TestPresets.ReapetPulserBC0_SnapshotL1A();
        //TestPresets.TestPreset preset = new TestPresets.ReapetPulserBC0_SnapshotBC0();
        //TestPresets.TestPreset preset = new TestPresets.NoPulser_DaqLike(20);
        TestPresets.TestPreset preset = new TestPresets.NoPulser_SnapshotL1A(3);
        //TestPresets.TestPreset preset = new TestPresets.SinglePulser_SnapshotL1A();
        //TestPresets.TestPreset preset = new TestPresets.SinglePulser_SnapshotPretrigger0();
        //TestPresets.TestPreset preset = new TestPresets.LongPulser_SnapshotPretrigger0(128, Long.MAX_VALUE); 
        TestsManager testsManager = new TestsManager(xmlFileName, preset, null, equipmentDAO, configurationManager, dbMapper);
        testsManager.setPulseTarget(pulseTarget);
        try {            
            TestOptionsXml testOptions = TestOptionsXml.readFromFile(System.getenv("HOME") + "/bin/config/testOptions.xml");

            HardwareDbTriggerCrate tcHdb1 = (HardwareDbTriggerCrate)dbMapper.getCrate(equipmentDAO.getCrateByName("TC_1"));
            HardwareDbTriggerCrate tcHdb9 = (HardwareDbTriggerCrate)dbMapper.getCrate(equipmentDAO.getCrateByName("TC_9"));
            //Board tb = equipmentDAO.getBoardByName("TBp2_9");
            //HardwareDBTriggerBoard tbHdb = (HardwareDBTriggerBoard)dbMapper.getBoard(tb);

            SorterCrate sc = (SorterCrate)equipmentDAO.getCrateByName("SC");
            HardwareDbCrate scHdb = dbMapper.getCrate(equipmentDAO.getCrateByName("SC"));
            
            List<Chip> optos = equipmentDAO.getChipsByTypeAndCrate(ChipType.OPTO, tcHdb1.getDbCrate(), true);
            optos.addAll(equipmentDAO.getChipsByTypeAndCrate(ChipType.OPTO, tcHdb9.getDbCrate(), true));
            //List<Chip> optos = equipmentDAO.getChipsByTypeAndBoard(ChipType.OPTO, tbHdb.getDbBoard(), true);
            testsManager.addPulsers(optos);

            List<Chip> pacs = equipmentDAO.getChipsByTypeAndCrate(ChipType.PAC, tcHdb1.getDbCrate(), true);
            //pacs.addAll(equipmentDAO.getChipsByTypeAndCrate(ChipType.PAC, tcHdb9.getDbCrate(), true));
            //List<Chip> pacs = equipmentDAO.getChipsByTypeAndBoard(ChipType.PAC, tbHdb.getDbBoard(), true);
            //testsManager.addReadouts(pacs.get(0).getType(), dbMapper.getReadouts(pacs));

            List<Chip> rmbs = equipmentDAO.getChipsByTypeAndCrate(ChipType.RMB, tcHdb1.getDbCrate(), true);
            rmbs.addAll(equipmentDAO.getChipsByTypeAndCrate(ChipType.RMB, tcHdb9.getDbCrate(), true));
            //List<Chip> rmbs = new ArrayList<Chip>();
            //rmbs.add(tbHdb.getRmb().getDbChip());
            testsManager.addReadouts(rmbs.get(0).getType(), dbMapper.getReadouts(rmbs));   

            //List<Chip> gbSorts = equipmentDAO.getChipsByTypeAndCrate(ChipType.GBSORT, tcHdb1.getDbCrate(), true);
            //List<Chip> gbSorts = equipmentDAO.getChipsByTypeAndBoard(ChipType.GBSORT, tbHdb.getDbBoard(), true);
            //testsManager.addPulsers(ChipType.GBSORT, dbMapper.getPulsers(gbSorts), pulseTarget);
            //testsManager.addReadouts(gbSorts.get(0).getType(), dbMapper.getReadouts(gbSorts));

            //List<Chip> tcSorts = equipmentDAO.getChipsByTypeAndBoard(ChipType.TCSORT, tcHdb1.getTcBackplane().getDbBoard(), true);	
            //List<Chip> tcSorts = equipmentDAO.getChipsByTypeAndBoard(ChipType.TCSORT, tcBpHdb.getDbBoard(), true);
            ////testsManager.addPulsers(ChipType.TCSORT, dbMapper.getPulsers(tcSorts), pulseTarget);
            //testsManager.addReadouts(ChipType.TCSORT, dbMapper.getReadouts(tcSorts));

            //List<Chip> halfSorts = equipmentDAO.getChipsByTypeAndCrate(ChipType.HALFSORTER, sc, true);
            //List<Chip> halfSorts = equipmentDAO.getChipsByTypeAndBoard(ChipType.HALFSORTER, equipmentDAO.getBoardByName("HSB_1"), true);
            //testsManager.addPulsers(ChipType.HALFSORTER, dbMapper.getPulsers(halfSorts), pulseTarget);
            //testsManager.addReadouts(ChipType.HALFSORTER, dbMapper.getReadouts(halfSorts));

            //List<Chip> finalSort = equipmentDAO.getChipsByTypeAndBoard(ChipType.FINALSORTER, equipmentDAO.getBoardByName("FSB"), true);
            //testsManager.addPulsers(ChipType.FINALSORTER, dbMapper.getPulsers(finalSort), pulseTarget);
            //testsManager.addReadouts(ChipType.FINALSORTER, dbMapper.getReadouts(finalSort));	

            MonitorManager monitorManager = new MonitorManager();
            for(HardwareDbBoard board :  tcHdb1.getBoards()) {
            	monitorManager.addChips(board.getChips());
            }
/*            for(HardwareDbBoard board :  tcHdb9.getBoards()) {
                monitorManager.addChips(board.getChips());
            }*/
            for(HardwareDbBoard board :  scHdb.getBoards()) {
                monitorManager.addChips(board.getChips());
            }
                       
            //monitorManager.enableTransmissionCheck(true, true);
                        
            monitorManager.resetRecErrorCnt();
            
            testsManager.daqPulserTest(false, false); //TODO
            //testsManager.singleRedaouts();
            //testsManager.singleRedaouts(); //TODO
            //testsManager.interConnectionTest(false);

            System.out.print(monitorManager.readRecErrorCntShorted());

            testsManager. disableLbPuls();
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (DataAccessException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (ServiceException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (XdaqException e) {
            e.printStackTrace();
        } finally {
            context.closeSession();
        }
    }

}
