package rpct.testAndUtils.pulsesAndReadout;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;

import rpct.datastream.MonitorManager;
import rpct.datastream.TestOptionsXml;
import rpct.datastream.TestPresets;
import rpct.datastream.TestsManager;
import rpct.datastream.TestPresets.NoPulser_DaqLike;
import rpct.datastream.TestPresets.TestPreset;
import rpct.datastream.TestsManager.MultipleRedaouts;
import rpct.datastream.TestsManager.PulserTarget;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.diag.TriggerDataSel;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;
import rpct.xdaqaccess.synchro.DefOut;

public class TBReadouts {
    private static class GetLine implements Runnable {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        public void run() {
            try {
                br.readLine();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws DataAccessException, IOException, InterruptedException, XdaqException, ServiceException  {
        long bxInSec = 40000000;
        DefOut defLog= new DefOut(System.out);
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(
                new HardwareRegistry(), context);
        
        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);

        System.out.println("XDAQ search ");
        dbMapper.getConfigurationDAO().getXdaqApplication((Chip)equipmentDAO.getObject(Chip.class, 311));
        System.out.println("XDAQ found");

        String xmlFileName =  "";
        
        int redaoutLimit = 10;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("readout cylce time (in sceonds)?");
        redaoutLimit = Integer.parseInt(br.readLine());

        TestPresets.TestPreset preset = new TestPresets.NoPulser_DaqLike(redaoutLimit);
        try {            
                                   
            TestOptionsXml testOptions = TestOptionsXml.readFromFile(System.getenv("HOME") + "/bin/config/tbReadoutConfig.xml");
            HardwareDbChip ttcCheckChip = null;
            List<Chip> chips = testOptions.getChips("readouts", equipmentDAO);
            for(Chip chip : chips) {
                if(chip.getType() == ChipType.PAC) {
                    HardwareDBTriggerBoard tbHdb = (HardwareDBTriggerBoard)dbMapper.getBoard(chip.getBoard());
                    ttcCheckChip = tbHdb.getOptos().get(0);
                    break;
                }
            }
           
            if(ttcCheckChip == null) {
                throw new RuntimeException("ttcCheckChip == null");
            }
            ttcCheckChip.getHardwareChip().getItem("STATUS.TRG_DATA_SEL").write(TriggerDataSel.PRETRIGGER_0.getIntVal());
            boolean ttcFromCTC = true; //<<<<<!!!!!!!!!!!!!!!!!!!!!!!!
            
            TestsManager testsManager = new TestsManager(xmlFileName, preset, testOptions, equipmentDAO, configurationManager, dbMapper);
            testsManager.setReadeouts();

            String y_n = "y";
            while(y_n.equals("y")) {
                int readoutCnt = 5;
                System.out.println("readout cylce repetitions?");
                readoutCnt = Integer.parseInt(br.readLine());

                Date date = new Date();
                SimpleDateFormat dateFormat = new SimpleDateFormat("yy_MM_dd__HH_mm");
                String outFileName = System.getenv("HOME") + "/bin/readouts/tbReadouts_" + dateFormat.format(date) + ".xml";
                                
                TestsManager.MultipleRedaouts multipleRedaouts = testsManager.new MultipleRedaouts(readoutCnt, outFileName, ttcFromCTC, ttcCheckChip);
                //testsManager.multipleRedaouts(readoutCnt, outFileName, ttcFromCTC, ttcCheckChip);
                Thread threadReadouts = new Thread(multipleRedaouts);
                Thread threadGetLine = new Thread(new GetLine());
                System.out.println("press Enter to stop");
                threadReadouts.start();
                threadGetLine.start();
                threadGetLine.join();
                
                if (threadGetLine.isAlive() == false &&
                        threadReadouts.isAlive()) {
                    threadReadouts.interrupt();
                    threadReadouts.join();
                }

                System.out.println("reapet? (y/n)");
                y_n = br.readLine();
            }
            //monitorManager.enableTest(true);
        } 
        catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JAXBException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (HardwareDbException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            context.closeSession();
        }
    }

}
