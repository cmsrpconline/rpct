package rpct.testAndUtils.pulsesAndReadout;

import java.io.IOException;

import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;

import rpct.datastream.TestOptionsXml;
import rpct.datastream.TestOptionsXml.EventBuilderOptions.ChipDataSel;
import rpct.datastream.TestOptionsXml.HardwareType;
import rpct.datastream.TestPresets;
import rpct.datastream.TestPulsesGenerator;
import rpct.datastream.TestsManager;
import rpct.datastream.TestsManager.PulserTarget;
import rpct.db.DataAccessException;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;

public class PulsesMuonsTest {

    public static void main(String[] args) throws ServiceException,
    DataAccessException, IOException, InterruptedException,
    XdaqException, HardwareDbException {
        long bxInSec = 40000000;
        boolean generatePulses = false; //TODO
        
        //String xmlFileName =  System.getenv("HOME") + "/bin/data/0_RPCBbx0_RPCFbx1_events.xml"; //TODO
        //String xmlFileName =  System.getenv("HOME") + "/bin/data/testEventData.xml"; //TODO
        String xmlFileName =  System.getenv("HOME") + "/bin/data/tbGbsTest.xml"; //TODO
        
        
        if(generatePulses)
            xmlFileName =  System.getenv("HOME") + "/bin/data/genTestpulses.xml";
        
        boolean comapareWithSource = false; //TODO
        
        TestOptionsXml testOptions = new TestOptionsXml();
        
      //TODO
        boolean pulsesFromPacs = true;
        boolean pulsesFromTBGBS = false;
        boolean pulsesFromTCGBS = false;
        boolean pulsesFromHalfGBS = false;
        boolean pulsesFromFinalGBS = false;
        
        boolean readoutPacs =  false; 
        testOptions.getEventBuilderOptions().setPacDataSel(ChipDataSel.inAndOut); //TODO
        
        boolean readoutTBGBS =  true; //TODO        
        testOptions.getEventBuilderOptions().setTbsDataSel(ChipDataSel.inAndOut); //TODO
                
        boolean readoutTCGBS =  true; //TODO
        testOptions.getEventBuilderOptions().setTcsDataSel(ChipDataSel.inAndOut); //TODO
        
        boolean readoutHalfGBS =  true; //TODO
        testOptions.getEventBuilderOptions().setHsDataSel(ChipDataSel.inAndOut); //TODO
        
        boolean readoutFinalGBS =  true; //TODO
        testOptions.getEventBuilderOptions().setFsDataSel(ChipDataSel.inAndOut);//TODO
        
        if(pulsesFromTBGBS) {
            readoutTBGBS = false;
        }
        
        if(pulsesFromTCGBS) {
            readoutTBGBS = false;
            readoutTCGBS  = false;
        }
        
        if(pulsesFromHalfGBS) {
            readoutTBGBS = false;
            readoutTCGBS  = false;
            readoutHalfGBS =  false;
        }
        
        if(pulsesFromFinalGBS) {
            readoutTBGBS = false;
            readoutTCGBS  = false;
            readoutHalfGBS =  false;
            readoutHalfGBS =  false;
        }
        
        TestPresets.TestPreset preset = new TestPresets.SinglePulser_SnapshotPretrigger0(); //ReapetPulserBC0_SnapshotBC0 //TODO
        //TestPresets.TestPreset preset = new TestPresets.ReapetPulserBC0_SnapshotBC0(); 
        //TestPresets.TestPreset preset = new TestPresets.ReapetPulserPretrigger0_SnapshotPretrigger0();
        //TestPresets.TestPreset preset = new TestPresets.LongPulser_SnapshotPretrigger0(128, 0xffffffffffl);
        //TestPresets.TestPreset preset = new TestPresets.SinglePulser_SnapshotL1A();
        //TestPresets.TestPreset preset = new TestPresets.ReapetPulserBC0_DaqLike();
        
        
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);

        try {
            PulserTarget pulseTarget = PulserTarget.chipOut;            
            
            TestPulsesGenerator generator = new TestPulsesGenerator();
            
            String[] tcs= { //TODO
                     "TC_0",
                     "TC_1",
                     "TC_2",
                     "TC_3",
                     "TC_4",
                     "TC_5",
                     "TC_6",
                     "TC_7",
                     "TC_8",                             
                     "TC_9",
                     "TC_10",
                     "TC_11",
                     //"PASTEURA_TC"                  
             };
            
            int[] tcNum = new int[tcs.length];
            for(int i = 0; i < tcs.length; i++) {
                if(tcs[i].equals("PASTEURA_TC")) {
                    tcNum[i] = 9; 
                }
                else {
                    tcNum[i] = Integer.parseInt(tcs[i].substring(3));
                }
            }
            
            if(generatePulses) { //TODO
                //generator.generatePACOutRnd(tcNum, 120);
                generator.generatePACBXnum(tcNum);
                
                generator.writeFile(xmlFileName);                          
            }
            
            

            for(String tc : tcs) { 
                System.out.println("testing crate " + tc);
                //testOptions.addChips("pulsers", HardwareType.CRATE, tc, ChipType.OPTO);
                if(pulsesFromPacs)
                    testOptions.addChips("pulsers", HardwareType.CRATE, tc, ChipType.PAC);
                
                if(pulsesFromTBGBS)
                    testOptions.addChips("pulsers", HardwareType.CRATE, tc, ChipType.GBSORT);
                                               
                if(pulsesFromTCGBS)
                    testOptions.addChips("pulsers", HardwareType.CRATE, tc, ChipType.TCSORT);
                                
                
                if(readoutPacs)
                    testOptions.addChips("readouts", HardwareType.CRATE, tc, ChipType.PAC);
                
                if(readoutTBGBS)
                    testOptions.addChips("readouts", HardwareType.CRATE, tc, ChipType.GBSORT);
                
                if(readoutTCGBS)
                    testOptions.addChips("readouts", HardwareType.CRATE,  tc, ChipType.TCSORT);
            }

            if(pulsesFromHalfGBS)
                testOptions.addChips("pulsers", HardwareType.CRATE, "SC", ChipType.HALFSORTER);
            
            if(pulsesFromFinalGBS)
                testOptions.addChips("pulsers", HardwareType.CRATE, "SC", ChipType.FINALSORTER);
            
            
            if(readoutHalfGBS) {
                testOptions.addChips("readouts", HardwareType.BOARD, "HSB_0", ChipType.HALFSORTER);
                testOptions.addChips("readouts", HardwareType.BOARD, "HSB_1", ChipType.HALFSORTER);
            }
            if(readoutFinalGBS)
                testOptions.addChips("readouts", HardwareType.CRATE, "SC", ChipType.FINALSORTER);
            
            TestsManager testsManager = new TestsManager(xmlFileName, preset, testOptions, equipmentDAO, null, dbMapper);
            testsManager.setPulsers();
            testsManager.setPulseTarget(pulseTarget);
            testsManager.setReadeouts();


            //MonitorManager monitorManager = new MonitorManager();            

            //monitorManager.addChips(chips, dbMapper);
            //testsManager.setMonitorManager(monitorManager);
            //monitorManager.enableTransmissionCheck(false, false); //TODO

            testsManager.interConnectionTest(comapareWithSource, false, 0, 1); //TODO
            //testsManager.daqPulserTest(comapareWithSource, true);
            //testsManager.algorithmsTest();

            //monitorManager.enableTest(true);

            //testsManager.


        } catch (JAXBException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            context.closeSession();
        }
    }

}
