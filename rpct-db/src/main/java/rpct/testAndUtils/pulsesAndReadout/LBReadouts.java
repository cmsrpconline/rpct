package rpct.testAndUtils.pulsesAndReadout;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;

import rpct.datastream.MonitorManager;
import rpct.datastream.TestOptionsXml;
import rpct.datastream.TestPresets;
import rpct.datastream.TestsManager;
import rpct.datastream.TestPresets.NoPulser_SnapshotL1A;
import rpct.datastream.TestPresets.TestPreset;
import rpct.datastream.TestsManager.PulserTarget;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;
import rpct.xdaqaccess.synchro.DefOut;

public class LBReadouts {

    public static void main(String[] args) throws DataAccessException, IOException, InterruptedException, XdaqException, ServiceException  {
        long bxInSec = 40000000;

        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(
                new HardwareRegistry(), context);
        
        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);

        System.out.println("XDAQ search ");
        dbMapper.getConfigurationDAO().getXdaqApplication((Chip)equipmentDAO.getObject(Chip.class, 311));
        System.out.println("XDAQ found");

        String lbConfigKey = ConfigurationManager.LOCAL_CONF_KEY_LBS_LHC2; //TODO
        
        String xmlFileName =  System.getenv("HOME") + "bin/data/gentestpulses.xml";
        
        int redaoutLimit = 10;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("readout cylce time (in sceonds)?");
        redaoutLimit = Integer.parseInt(br.readLine());

        //TestPresets.TestPreset preset = new TestPresets.NoPulser_DaqLike(redaoutLimit);
        TestPresets.TestPreset preset = new TestPresets.NoPulser_SnapshotL1A(2);
        
        try {            
/*            Set<Chip> synCodersSet = new HashSet<Chip>();

            List<String> lboxesNames = new ArrayList<String>();
            lboxesNames.add("LBB_RB+2_S10");
            for(String lboxesName : lboxesNames) {
                LinkBox linkBox = (LinkBox)equipmentDAO.getCrateByName(lboxesName);
                if(linkBox != null)  {
                    synCodersSet.addAll(equipmentDAO.getChipsByTypeAndCrate(ChipType.SYNCODER, linkBox, true));
                    System.out.println("adding LBOX " + lboxesName);
                }
                else
                    System.out.println("the LBOX " + lboxesName + " not fund");
            }
            
            List<Chip> synCoders = new ArrayList<Chip>(synCodersSet);            
            for(Chip synCoder : synCoders) {
                System.out.println(synCoder.getBoard().getName() + " " + ((LinkBoard)synCoder.getBoard()).getFEBLocations().get(0).getChamberLocation().getChamberLocationName());
            }

            testsManager.addReadouts(ChipType.SYNCODER, dbMapper.getReadouts(synCoders));*/
            
            TestOptionsXml testOptions = TestOptionsXml.readFromFile(System.getenv("HOME") + "/bin/config/lbReadoutConfig.xml");
                        
            
            TestsManager testsManager = new TestsManager(xmlFileName, preset, testOptions, equipmentDAO, configurationManager, dbMapper, lbConfigKey);
            testsManager.setReadeouts();
            
            int readoutCnt = 5;
            System.out.println("readout cylce repetitions?");
            readoutCnt = Integer.parseInt(br.readLine());
            
            Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yy_MM_dd__HH_mm");
            String outFileName = System.getenv("HOME") + "/bin/readouts/lbReadouts_" + dateFormat.format(date) + ".xml";
            //testsManager.multipleRedaouts(readoutCnt, outFileName, false, null);
            testsManager.singleRedaouts();
            //monitorManager.enableTest(true);
        } 
        catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JAXBException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            context.closeSession();
        }
    }

}
