package rpct.testAndUtils.pulsesAndReadout;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;

import rpct.datastream.MonitorManager;
import rpct.datastream.TestOptionsXml;
import rpct.datastream.TestPresets;
import rpct.datastream.TestPulsesGenerator;
import rpct.datastream.TestsManager;
import rpct.datastream.TestOptionsXml.ChipSource;
import rpct.datastream.TestOptionsXml.HardwareType;
import rpct.datastream.TestOptionsXml.SourcesList;
import rpct.datastream.TestOptionsXml.EventBuilderOptions.ChipDataSel;
import rpct.datastream.TestPresets.ReapetPulserPretrigger0_SnapshotPretrigger0;
import rpct.datastream.TestPresets.TestPreset;
import rpct.datastream.TestsManager.PulserTarget;
import rpct.db.DataAccessException;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.SorterCrate;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.diag.TriggerDataSel;
import rpct.xdaqaccess.diag.DiagCtrl.TriggerType;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;

public class TC_SC_interconnectionTest {

    public static void main(String[] args) throws ServiceException,
    DataAccessException, IOException, InterruptedException,
    XdaqException, HardwareDbException {
        long bxInSec = 40000000;

        //String xmlFileName = "/nfshome0/rpcdev/bin/data/testBxDataTC9HPT.xml"; //TODO
        //String xmlFileName = "/nfshome0/rpcdev/bin/data/gentestpulses.xml";
        //String xmlFileName = "/nfshome0/rpcdev/bin/data/testBxData.xml";
        //String xmlFileName = "/nfshome0/rpcdev/bin/data/testpulses2.xml";
        String xmlFileName =  System.getenv("HOME") + "/bin/data/genTestpulses_sorts.xml";
        //String xmlFileName = "/nfshome0/rpcdev/bin/data/wo_conv.xml";
        //String xmlFileName = "/nfshome0/rpcdev/bin/data/wo42_conv.xml";
        //String xmlFileName = "/nfshome0/rpcdev/bin/data/wz_conv.xml";
        //String xmlFileName = "/nfshome0/rpcdev/bin/data/2007.07.TestGMTConnection/testpulsesFMT0.xml";
        //String xmlFileName = "/nfshome0/rpcdev/bin/data/2007.07.TestGMTConnection/testpulsesFSB8mu.xml";
        //String xmlFileName = "/nfshome0/rpcdev/bin/data/2007.07.TestGMTConnection/testpulsesAddr.xml";
        //String xmlFileName = "/nfshome0/rpcdev/bin/data/2007.07.TestSorters/testpulsesFMT112.xml";

        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(
                new HardwareRegistry(), context);

        //TestPresets.TestPreset preset = new TestPresets.SinglePulser_SnapshotPretrigger0(); //ReapetPulserBC0_SnapshotBC0
        //       TestPresets.TestPreset preset = new TestPresets.ReapetPulserBC0_SnapshotBC0(); 
        TestPresets.TestPreset preset = new TestPresets.ReapetPulserPretrigger0_SnapshotPretrigger0(128);
        try {
            boolean compareWithSource = false;
            PulserTarget pulseTarget = PulserTarget.chipOut; //TODO            
            TestOptionsXml testOptions = new TestOptionsXml();

            TestPulsesGenerator generator = new TestPulsesGenerator();
            String[] tcInputs = {
                    "TC_0", "11",
                    "TC_1", "11",
                    "TC_2", "11",
                    "TC_3", "00",
                    "TC_4", "11",
                    "TC_5", "11",
                    "TC_6", "11",
                    "TC_7", "11",
                    "TC_8", "11",
                    "TC_9", "11",
                    "TC_10", "11",
                    "TC_11", "11",
            };
            
/*            int[] tcNum = { 
                    0, 
                    1, 
                    2, 
                    3, 
                    4, 
                    5, 
                    6, 
                    7, 
                    8, 
                    9, 
                    10, 
                    //11
                    };*/
            generator.generateTcSortOut();
            //generator.generateTcSortOutRnd(tcInputs, 120);
            //generator.generateTcSortOut(tcNum);
            generator.writeFile(xmlFileName);

            for(int i =0; i < 12; i++) {                
                if (! tcInputs[i*2 +1].equals("00") ) {
                    testOptions.addChips("pulsers", HardwareType.CRATE, tcInputs[i*2], ChipType.TCSORT); 
                    //testOptions.addChips("readouts", HardwareType.CRATE, "TC_" + tc, ChipType.TCSORT); 
                }
            }
            //  testOptions.addChips("pulsers", HardwareType.CRATE, "TC_1", ChipType.TCSORT);
            //testOptions.addChips("pulsers", HardwareType.CRATE, "TC_9", ChipType.TCSORT);
            //testOptions.addChips("pulsers", HardwareType.CRATE, "TC_11", ChipType.TCSORT);            
            
           //testOptions.addChips("pulsers", HardwareType.CRATE, "SC", ChipType.HALFSORTER);

            testOptions.addChips("readouts", HardwareType.BOARD, "HSB_0", ChipType.HALFSORTER);
            testOptions.addChips("readouts", HardwareType.BOARD, "HSB_1", ChipType.HALFSORTER);           
            if(compareWithSource) {          
                testOptions.getEventBuilderOptions().setHsDataSel(ChipDataSel.in);
            }
            else{
                testOptions.getEventBuilderOptions().setHsDataSel(ChipDataSel.inAndOut);
                testOptions.addChips("readouts", HardwareType.BOARD, "FSB", ChipType.FINALSORTER);
            }


            TestsManager testsManager = new TestsManager(xmlFileName, preset, testOptions, equipmentDAO, null, dbMapper);
            testsManager.setPulsers();
            testsManager.setPulseTarget(pulseTarget);
            testsManager.setReadeouts();

            //MonitorManager monitorManager = new MonitorManager();            
            List<Chip> chips = testOptions.getChips("readouts", equipmentDAO);
            //monitorManager.addChips(chips, dbMapper);
            //testsManager.setMonitorManager(monitorManager);
            //monitorManager.enableTransmissionCheck(false, false); //TODO

            //testsManager.interConnectionTest(compareWithSource, false, 0, 0);
            //for(int i = 0; i < 100; i++)
            testsManager.daqPulserTest(compareWithSource, true);
            //pulsesManager.algorithmsTest();

            //monitorManager.enableTest(true);
        } catch (JAXBException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            context.closeSession();
        }
    }

}
