package rpct.testAndUtils.pulsesAndReadout;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.rpc.ServiceException;

import rpct.datastream.BxDataXml;
import rpct.datastream.DataStreamXml;
import rpct.datastream.EventXml;
import rpct.datastream.RpctDelays;
import rpct.datastream.TbDataXml;
import rpct.datastream.TcDataXml;
import rpct.datastream.TestOptionsXml;
import rpct.datastream.TestPresets;
import rpct.datastream.TestPulsesGenerator;
import rpct.datastream.TestsManager;
import rpct.datastream.TestOptionsXml.ChipSource;
import rpct.datastream.TestOptionsXml.HardwareType;
import rpct.datastream.TestOptionsXml.SourcesList;
import rpct.datastream.TestOptionsXml.EventBuilderOptions.ChipDataSel;
import rpct.datastream.TestsManager.PulserTarget;
import rpct.db.DataAccessException;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.SorterCrate;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.diag.TriggerDataSel;
import rpct.xdaqaccess.diag.DiagCtrl.TriggerType;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;

public class TestPulsesTest {

	static List<String> preparePulsesXml(String cmsswXmlName, int bxPerEvent, int eventsPerFile) throws JAXBException, IOException {
		List<String> filesNames = new ArrayList<String>();

		JAXBContext context = JAXBContext.newInstance(DataStreamXml.class);
		System.out.println("reading the " + cmsswXmlName);
		Unmarshaller um = context.createUnmarshaller();
		File cmsswXmlFile = new File(cmsswXmlName);
		DataStreamXml cmsswData = (DataStreamXml) um.unmarshal(cmsswXmlFile);

		System.out.println("data1.getEvents().size() " + cmsswData.getEvents().size());
		int evntCnt = 0;
		DataStreamXml dataStream = null;
		EventXml pulseEventXml = null;
		for( Map.Entry<Integer, EventXml>  eventXmlEntry : cmsswData.getEvents().getMap().entrySet()) {
			EventXml cmsswEventXml = eventXmlEntry.getValue();             
			if(evntCnt % eventsPerFile == 0) {
				dataStream = new DataStreamXml();   
				//pulseEventXml = dataStream.getOrAddEvent(cmsswEventXml.getNumber(), cmsswEventXml.getBxNum());
				pulseEventXml = dataStream.getOrAddEvent(0, 0); //event builder requires such numbers for comparing the source and hardware data
			}

			int firstBxNum = cmsswEventXml.getBxData().getFirst().getNumber();                       
			for(BxDataXml bxDataXml : cmsswEventXml.getBxData()) {
				bxDataXml.setNumber( (evntCnt % eventsPerFile) * bxPerEvent + bxDataXml.getNumber() - firstBxNum);
				pulseEventXml.getBxData().add(bxDataXml);          
			}
			if(evntCnt % eventsPerFile == eventsPerFile-1 || evntCnt == cmsswData.getEvents().getMap().size() -1) {
				String fileName = cmsswXmlFile.getParentFile() + File.separator + Integer.toString(evntCnt / eventsPerFile) + "_" + cmsswXmlFile.getName();
				final File xmlFile = new File(fileName);
				System.out.println("writing to the file" + xmlFile);
				Marshaller m = context.createMarshaller();
				m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);                                                                                                     
				FileOutputStream  fileOutputStream = new FileOutputStream(xmlFile);
				m.marshal(dataStream, fileOutputStream);

				fileOutputStream.close();
				filesNames.add(fileName);
			}
			evntCnt++;
		}

		return filesNames;
	}

	/*
	 * W testBxData siedza dane opto. Jest jeden event, bxData sa z numerami co 10.
	 * W testpulses sedza miony. Jest wiele eventow, numery maja co jeden. W kazdym siedzi jeden bxData z num 0.
	 */
	static List<String> preparePulsesXmlFromEmu(String optoDataXmlName, String muonDataXmlName, int bxPerEvent, int eventsPerFile) throws Exception {
		List<String> filesNames = new ArrayList<String>();

		JAXBContext context = JAXBContext.newInstance(DataStreamXml.class);
		Unmarshaller um = context.createUnmarshaller();

		System.out.println("reading the " + optoDataXmlName);        
		File cmsswXmlFile = new File(optoDataXmlName);
		DataStreamXml optoData = (DataStreamXml) um.unmarshal(cmsswXmlFile);

		if(optoData.getEvents().size() != 1) {
			throw new Exception("optoData.getEvents().size() != 1");
		}
		EventXml optoEvent = optoData.getEvents().getFirst();
		System.out.println("optoEvent.getBxData().size() " + optoEvent.getBxData().size());

		context = JAXBContext.newInstance(DataStreamXml.class);
		System.out.println("reading the " + muonDataXmlName);
		cmsswXmlFile = new File(muonDataXmlName);
		DataStreamXml muonData = (DataStreamXml) um.unmarshal(cmsswXmlFile);
		System.out.println("muonData.getEvents().size() " + muonData.getEvents().size());

		int evntCnt = 0;
		DataStreamXml dataStream = null;
		EventXml pulseEventXml = null;

		int optBxNum = 0;
		int bxNum = 0;
		for( Map.Entry<Integer, EventXml>  eventXmlEntry : muonData.getEvents().getMap().entrySet()) {
			EventXml muonEventXml = eventXmlEntry.getValue();             
			if(evntCnt % eventsPerFile == 0) {
				dataStream = new DataStreamXml();   
				//pulseEventXml = dataStream.getOrAddEvent(cmsswEventXml.getNumber(), cmsswEventXml.getBxNum());
				pulseEventXml = dataStream.getOrAddEvent(0, 0); //event builder requires such numbers for comparing the source and hardware data
				bxNum = 0;
			}

			BxDataXml muonBxDataXml = null;     
			if(muonEventXml.getBxData().size() == 1) {
				muonBxDataXml = muonEventXml.getBxData().getFirst();
			}

			optBxNum = muonEventXml.getNumber(); 
			BxDataXml optoBxDataXml = optoEvent.getBxData().get(optBxNum);
			if(optoBxDataXml == null) {
				throw new Exception("bxData " + optBxNum + " not found in the optoBxDataXml");
			}

			BxDataXml bxDataXml = pulseEventXml.getOrAddBxData(bxNum);
			bxNum += 8;

			for(int tcNum = 0; tcNum < 12; tcNum++) {
				TcDataXml tcDataXml =  optoBxDataXml.getTcData().get(tcNum);
				if(tcDataXml != null) {
					bxDataXml.getTcData().add(tcDataXml); //opto data

					if(muonBxDataXml != null) {
						TcDataXml muonTcDataXml = muonBxDataXml.getTcData().get(tcNum);
						if(muonTcDataXml != null && muonTcDataXml.getTbData() != null) {
							for(TbDataXml muontTbDataXml : muonTcDataXml.getTbData()) {
								if(tcDataXml.getTbData().get(muontTbDataXml.getNumber()) == null) {
									throw new Exception("tbData " + muontTbDataXml.getNumber() + " not found in the opto tcDataXml.getTbData");
								}
								tcDataXml.getTbData().get(muontTbDataXml.getNumber()).setPacs(muontTbDataXml.getPacs());

								tcDataXml.getTbData().get(muontTbDataXml.getNumber()).setGbSortMuons(muontTbDataXml.getGbSortMuons());
							}
						}
						else {
							int a =0;
						}
					}

				}
			}

			if(evntCnt % eventsPerFile == eventsPerFile-1 || evntCnt == muonData.getEvents().getMap().size() -1) {
				String fileName = cmsswXmlFile.getParentFile() + File.separator + Integer.toString(evntCnt / eventsPerFile) + "_" + cmsswXmlFile.getName();
				final File xmlFile = new File(fileName);
				System.out.println("writing to the file" + xmlFile);
				Marshaller m = context.createMarshaller();
				m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);                                                                                                     
				FileOutputStream  fileOutputStream = new FileOutputStream(xmlFile);
				m.marshal(dataStream, fileOutputStream);

				fileOutputStream.close();
				filesNames.add(fileName);
			}
			evntCnt++;
		}

		return filesNames;
	}


/*
 * w muonDataXml bxData num jest chyba zawsze "0"
 * w optoDataXml bxData num odpowiadajacy L1A jest chyba o +1 od BXa eventu
 */
	static List<String> preparePulsesXmlFromData(String optoDataXmlName, String muonDataXmlName, int bxPerEvent, int eventsPerFile) throws Exception {
		List<String> filesNames = new ArrayList<String>();

		JAXBContext context = JAXBContext.newInstance(DataStreamXml.class);
		Unmarshaller um = context.createUnmarshaller();

		System.out.println("reading the " + optoDataXmlName);        
		File cmsswXmlFile = new File(optoDataXmlName);
		DataStreamXml optoData = (DataStreamXml) um.unmarshal(cmsswXmlFile);
		System.out.println("optoData().size() " + optoData.getEvents().size());

		context = JAXBContext.newInstance(DataStreamXml.class);
		System.out.println("reading the " + muonDataXmlName);
		cmsswXmlFile = new File(muonDataXmlName);
		DataStreamXml muonData = (DataStreamXml) um.unmarshal(cmsswXmlFile);
		System.out.println("muonData.getEvents().size() " + muonData.getEvents().size());

		int evntCnt = 0;
		DataStreamXml dataStream = null;
		EventXml pulseEventXml = null;

		//int optBxNum = 0;
		int bxNum = 0;
		String events = "";
		for( Map.Entry<Integer, EventXml>  eventXmlEntry : muonData.getEvents().getMap().entrySet()) {
			EventXml muonEventXml = eventXmlEntry.getValue();             

			int eventNum = muonEventXml.getNumber();
			EventXml optoEvent = optoData.getEvents().get(eventNum);
			if(optoEvent == null)
				throw new RuntimeException(" no event number " + eventNum + " in the optoData");

			if(evntCnt % eventsPerFile == 0) {
				dataStream = new DataStreamXml();   
				//pulseEventXml = dataStream.getOrAddEvent(cmsswEventXml.getNumber(), cmsswEventXml.getBxNum());
				pulseEventXml = dataStream.getOrAddEvent(0, 0); //event builder requires such numbers for comparing the source and hardware data
				bxNum = 0;
				events = "";
			}

			events = events + "_"+ eventNum;
			
			for(BxDataXml muonBxDataXml : muonEventXml.getBxData()) {     
				BxDataXml optoBxDataXml = optoEvent.getBxData().get(muonBxDataXml.getNumber() + muonEventXml.getBxNum() + 1); //!!!!!!!!!!!!!!!!!!!!!!
				if(optoBxDataXml == null) {
					throw new Exception("bxData " + muonBxDataXml.getNumber() + " not found in the optoBxDataXml");
				}

				BxDataXml bxDataXml = pulseEventXml.getOrAddBxData(bxNum);
				bxNum += 8;

				for(int tcNum = 0; tcNum < 12; tcNum++) {
					TcDataXml tcDataXml =  optoBxDataXml.getTcData().get(tcNum);
					if(tcDataXml != null) {
						bxDataXml.getTcData().add(tcDataXml); //opto data

						if(muonBxDataXml != null) {
							TcDataXml muonTcDataXml = muonBxDataXml.getTcData().get(tcNum);
							if(muonTcDataXml != null && muonTcDataXml.getTbData() != null) {
								for(TbDataXml muontTbDataXml : muonTcDataXml.getTbData()) {
									if(tcDataXml.getTbData().get(muontTbDataXml.getNumber()) == null) {
										throw new Exception("tbData " + muontTbDataXml.getNumber() + " not found in the opto tcDataXml.getTbData");
									}
									tcDataXml.getTbData().get(muontTbDataXml.getNumber()).setPacs(muontTbDataXml.getPacs());

									tcDataXml.getTbData().get(muontTbDataXml.getNumber()).setGbSortMuons(muontTbDataXml.getGbSortMuons());
								}
								//TODO add TCGBS Data, and HSB, and FSB
							}
							else {
								int a =0;
							}
						}

					}
				}
			}

			if(evntCnt % eventsPerFile == eventsPerFile-1 || evntCnt == muonData.getEvents().getMap().size() -1) {
				String fileName = cmsswXmlFile.getParentFile() + File.separator + events + "_" + cmsswXmlFile.getName();
				final File xmlFile = new File(fileName);
				System.out.println("writing to the file" + xmlFile);
				Marshaller m = context.createMarshaller();
				m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);                                                                                                     
				FileOutputStream  fileOutputStream = new FileOutputStream(xmlFile);
				m.marshal(dataStream, fileOutputStream);

				fileOutputStream.close();
				filesNames.add(fileName);
			}
			evntCnt++;
		}

		return filesNames;
	}



	public static void main(String[] args) throws ServiceException,
	DataAccessException, IOException, InterruptedException,
	XdaqException, HardwareDbException {
		long bxInSec = 40000000;

		//String xmlFileName =  System.getenv("HOME") + "/bin/data/2009.07.TestPulses/100334/testBxData.xml";
		//String cmsswXmlFileName =  System.getenv("HOME") + "/bin/data/RPCBbx0_RPCFbx1_events.xml";

		String optoXmlFileName =  System.getenv("HOME") + "/bin/data/testPulses/myEvent_163657.xml";
		String muonXmlFileName =  System.getenv("HOME") + "/bin/data/testPulses/testpulses_163657.xml";

		//String xmlFileName =  System.getenv("HOME") + "/bin/data/genTestpulses_opto.xml";
		//String xmlFileName =  System.getenv("HOME") + "/bin/data/eventData.xml";

		HibernateContext context = new SimpleHibernateContextImpl();
		EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
		HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);

		TestPresets.TestPreset preset = new TestPresets.SinglePulser_SnapshotPretrigger0(); //ReapetPulserBC0_SnapshotBC0
		//TestPresets.TestPreset preset = new TestPresets.ReapetPulserBC0_SnapshotBC0(); 
		//TestPresets.TestPreset preset = new TestPresets.ReapetPulserPretrigger0_SnapshotPretrigger0();
		//TestPresets.TestPreset preset = new TestPresets.SinglePulser_SnapshotL1A();
		//  TestPresets.TestPreset preset = new TestPresets.ReapetPulserBC0_DaqLike();
		try {
			PulserTarget pulseTarget = PulserTarget.chipOut; //TODO            

			TestPulsesGenerator generator = new TestPulsesGenerator();
			String[] tcs= { 
					/*"TC_0",
					"TC_1",
					"TC_2",
					"TC_3",
					"TC_4",
					"TC_5",
					"TC_6",
					"TC_7",
					"TC_8",                             
					"TC_9",
					"TC_10",
					"TC_11",*/
					// "PASTEURA_TC"
					"TC_904"
			};

			boolean pacs = false;
			boolean rmbs = false;
			boolean tbgs = true;
			boolean tcgs = false;

			boolean hsb = false;
			boolean fsb = false;

			//List<String> pulsesFilesNames = preparePulsesXmlFromData(optoXmlFileName, muonXmlFileName, 16, 1);
			List<String> pulsesFilesNames = new ArrayList<String>();
			pulsesFilesNames.add(System.getenv("HOME") + "/bin/testPulses/_23746931_testpulses_163657.xml");

			TestOptionsXml testOptions = new TestOptionsXml();

			for(String tc : tcs) {              
				System.out.println("adding crate " + tc);
				testOptions.addChips("pulsers", HardwareType.CRATE, tc, ChipType.OPTO);

				if(pacs)
					testOptions.addChips("readouts", HardwareType.CRATE, tc, ChipType.PAC); 

				if(rmbs)
					testOptions.addChips("readouts", HardwareType.CRATE, tc, ChipType.RMB);             


				if(tbgs)
					testOptions.addChips("readouts", HardwareType.CRATE, tc, ChipType.GBSORT);

				if(tcgs)
					testOptions.addChips("readouts", HardwareType.CRATE, tc, ChipType.TCSORT);

				if(rmbs) {
					List<Chip> chips = testOptions.getChips("readouts", equipmentDAO);
					for(Chip ch : chips) {     
						if(ch.getType() == ChipType.RMB) {
							dbMapper.getChip(ch).getHardwareChip().getItem("RMB_DATA_DELAY").write(RpctDelays.RMB_DATA_DELAY - 40); //skad taka wartosc to nie wiem 
						}
					}
				}
			}

			//testOptions.addChips("readouts", HardwareType.CRATE, "SC", ChipType.HALFSORTER);

			if(hsb) {
				testOptions.addChips("readouts", HardwareType.BOARD, "HSB_0", ChipType.HALFSORTER);
				testOptions.addChips("readouts", HardwareType.BOARD, "HSB_1", ChipType.HALFSORTER);
			}

			if(fsb)
				testOptions.addChips("readouts", HardwareType.BOARD, "FSB", ChipType.FINALSORTER);

			testOptions.getEventBuilderOptions().setPacDataSel(ChipDataSel.inAndOut);
			testOptions.getEventBuilderOptions().setTbsDataSel(ChipDataSel.inAndOut);
			testOptions.getEventBuilderOptions().setTcsDataSel(ChipDataSel.inAndOut);
			testOptions.getEventBuilderOptions().setHsDataSel(ChipDataSel.inAndOut);
			testOptions.getEventBuilderOptions().setFsDataSel(ChipDataSel.inAndOut);

			int i = 0;
			for(String xmlFileName : pulsesFilesNames) { 
				/*if(xmlFileName.contains("118") == false)
					continue;*/
				TestsManager testsManager = new TestsManager(xmlFileName, preset, testOptions, equipmentDAO, null, dbMapper);
				System.out.println("testing with the data from: " + xmlFileName);
				//testsManager.setXmlFileName(xmlFileName);
				testsManager.setPulsers();
				testsManager.setPulseTarget(pulseTarget);
				testsManager.setReadeouts(); 

				testsManager.interConnectionTest(true, false, 0, 1);
				
				if(i == 10)
					break; //TODO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1111
				i++;
				
			}
			//testsManager.daqPulserTest(true, true);
			//testsManager.algorithmsTest();

			//monitorManager.enableTest(true);

			List<Chip> chips = testOptions.getChips("readouts", equipmentDAO);
			for(Chip ch : chips) {     
				if(ch.getType() == ChipType.RMB) {
					dbMapper.getChip(ch).getHardwareChip().getItem("RMB_DATA_DELAY").write(RpctDelays.RMB_DATA_DELAY); 
				}
			}


		} catch (JAXBException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			//context.closeSession(); //TODO
		}
	}

}
