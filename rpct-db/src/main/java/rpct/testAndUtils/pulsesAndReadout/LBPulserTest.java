package rpct.testAndUtils.pulsesAndReadout;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;

import rpct.datastream.MonitorManager;
import rpct.datastream.TestOptionsXml;
import rpct.datastream.TestPresets;
import rpct.datastream.TestPulsesGenerator;
import rpct.datastream.TestsManager;
import rpct.datastream.TestPresets.SinglePulser_SnapshotPretrigger0;
import rpct.datastream.TestPresets.TestPreset;
import rpct.datastream.TestsManager.PulserTarget;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;
import rpct.xdaqaccess.synchro.DefOut;

public class LBPulserTest {

    public static void main(String[] args) throws DataAccessException  {
        HibernateContext context = new SimpleHibernateContextImpl();
        try {
            EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
            HardwareDbMapper dbMapper = new HardwareDbMapper(
                    new HardwareRegistry(), context);

            ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
            ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);

            boolean generateData = false;
 /*           System.out.println("XDAQ search ");
            dbMapper.getConfigurationDAO().getXdaqApplication((Chip)equipmentDAO.getObject(Chip.class, 311));
            System.out.println("XDAQ found");*/

            String xmlFileName =  System.getenv("HOME") + "/bin/data/gentestpulses.xml";

            String lbConfigKey = ConfigurationManager.LOCAL_CONF_KEY_LBS_LHC2; //TODO
            
            TestPresets.TestPreset preset = new TestPresets.SinglePulser_SnapshotPretrigger0();
            preset.setReadoutLimit(100);

            TestOptionsXml testOptions = TestOptionsXml.readFromFile(System.getenv("HOME") + "/bin/config/lbReadoutConfig.xml");
            PulserTarget pulseTarget = PulserTarget.chipIn; //TODO

            TestPulsesGenerator generator = new TestPulsesGenerator();                      

            List<Chip> synCoders = testOptions.getChips("pulsers", equipmentDAO);
            List<Integer> lbIds = new ArrayList<Integer>();
            for(Chip ch : synCoders) {
                if(ch.getType() == ChipType.SYNCODER)
                    lbIds.add(ch.getBoard().getId());
            }
            if(generateData) {
            	generator.generateLBDataOneBx(lbIds);
            	generator.writeFile(xmlFileName);
            }

            TestsManager testsManager = new TestsManager(xmlFileName, preset, testOptions, equipmentDAO, configurationManager, dbMapper, lbConfigKey);
            testsManager.setPulsers();
            testsManager.setReadeouts();
            testsManager.setPulseTarget(pulseTarget);                       
            testsManager.interConnectionTest(false, false, 0, 0);
            //multipleRedaouts(readoutCnt, outFileName, false, null);           
        } 
        catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JAXBException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (XdaqException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            context.closeSession();
        }
    }

}
