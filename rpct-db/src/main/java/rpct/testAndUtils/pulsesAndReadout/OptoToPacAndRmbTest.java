package rpct.testAndUtils.pulsesAndReadout;

import java.io.IOException;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;

import rpct.datastream.RpctDelays;
import rpct.datastream.TestOptionsXml;
import rpct.datastream.TestOptionsXml.EventBuilderOptions.ChipDataSel;
import rpct.datastream.TestOptionsXml.HardwareType;
import rpct.datastream.TestPresets;
import rpct.datastream.TestPulsesGenerator;
import rpct.datastream.TestsManager;
import rpct.datastream.TestsManager.PulserTarget;
import rpct.db.DataAccessException;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;

public class OptoToPacAndRmbTest {

    public static void main(String[] args) throws ServiceException,
    DataAccessException, IOException, InterruptedException,
    XdaqException, HardwareDbException {
        long bxInSec = 40000000;

        String xmlFileName =  System.getenv("HOME") + "/bin/data/genTestpulses_opto.xml";
        //String xmlFileName =  System.getenv("HOME") + "/bin/data/eventData.xml";

        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(
                new HardwareRegistry(), context);

        //TestPresets.TestPreset preset = new TestPresets.SinglePulser_SnapshotPretrigger0(); //ReapetPulserBC0_SnapshotBC0
        //TestPresets.TestPreset preset = new TestPresets.ReapetPulserBC0_SnapshotBC0(); 
        TestPresets.TestPreset preset = new TestPresets.ReapetPulserPretrigger0_SnapshotPretrigger0();
        //TestPresets.TestPreset preset = new TestPresets.SinglePulser_SnapshotL1A();
        //  TestPresets.TestPreset preset = new TestPresets.ReapetPulserBC0_DaqLike();
        try {
            PulserTarget pulseTarget = PulserTarget.chipOut; //TODO            
            

            TestPulsesGenerator generator = new TestPulsesGenerator();
            int[] tcNum = { 
                    0,
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7,                  
                    8,
                    9, 
                    10,  
                    11                    
            };
            //generator.generateLinkData(tcNum); //TODO to jest generoawny plik z pulsami     
            //generator.generateLinkDataBxNum(tcNum, 63);
            //generator.generateLinkDataRnd(tcNum, 59);
            //generator.generateLinkData2(tcNum);
            generator.generateLinkDataDetecorLike(1, 1, equipmentDAO);
            generator.writeFile(xmlFileName);                          


            boolean rmbs = false;
            TestsManager testsManager = null;
            for(int tc : tcNum) {
                TestOptionsXml testOptions = new TestOptionsXml();
                System.out.println("testing crate TC_" + tc);
                testOptions.addChips("pulsers", HardwareType.CRATE, "TC_" + tc, ChipType.OPTO);

                testOptions.addChips("readouts", HardwareType.CRATE, "TC_" + tc, ChipType.PAC); 
                if(rmbs)
                    testOptions.addChips("readouts", HardwareType.CRATE, "TC_" + tc, ChipType.RMB);             

                List<Chip> chips = testOptions.getChips("readouts", equipmentDAO);
                if(rmbs) {
                    for(Chip ch : chips) {     
                        if(ch.getType() == ChipType.RMB) {
                            dbMapper.getChip(ch).getHardwareChip().getItem("RMB_DATA_DELAY").write(0x28); //skad taka wartosc to nie wiem TODO
                        }
                    }
                }


                testsManager = new TestsManager(xmlFileName, preset, testOptions, equipmentDAO, null, dbMapper);
                testsManager.setPulsers();
                testsManager.setPulseTarget(pulseTarget);
                testsManager.setReadeouts();

                //MonitorManager monitorManager = new MonitorManager();            

                //monitorManager.addChips(chips, dbMapper);
                //testsManager.setMonitorManager(monitorManager);
                //monitorManager.enableTransmissionCheck(false, false); //TODO


                testOptions.getEventBuilderOptions().setPacDataSel(ChipDataSel.in);

                //testsManager.interConnectionTest(true, false, false, 1);
                testsManager.daqPulserTest(true, true);
                //testsManager.algorithmsTest();

                //monitorManager.enableTest(true);
                if(rmbs) {
                    for(Chip ch : chips) {     
                        if(ch.getType() == ChipType.RMB) {
                            dbMapper.getChip(ch).getHardwareChip().getItem("RMB_DATA_DELAY").write(RpctDelays.RMB_DATA_DELAY); 
                        }
                    }
                }
            }

        } catch (JAXBException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            context.closeSession();
        }
    }

}
