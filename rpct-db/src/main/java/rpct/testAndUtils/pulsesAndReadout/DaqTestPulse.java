package rpct.testAndUtils.pulsesAndReadout;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;

import rpct.datastream.MonitorManager;
import rpct.datastream.TestOptionsXml;
import rpct.datastream.TestPresets;
import rpct.datastream.TestPulsesGenerator;
import rpct.datastream.TestsManager;
import rpct.datastream.TestOptionsXml.ChipSource;
import rpct.datastream.TestOptionsXml.HardwareType;
import rpct.datastream.TestOptionsXml.SourcesList;
import rpct.datastream.TestOptionsXml.EventBuilderOptions.ChipDataSel;
import rpct.datastream.TestPresets.SinglePulser_SnapshotL1A;
import rpct.datastream.TestPresets.TestPreset;
import rpct.datastream.TestsManager.PulserTarget;
import rpct.db.DataAccessException;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.SorterCrate;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.diag.TriggerDataSel;
import rpct.xdaqaccess.diag.DiagCtrl.TriggerType;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;

public class DaqTestPulse {

    public static void main(String[] args) throws ServiceException,
    DataAccessException, IOException, InterruptedException,
    XdaqException, HardwareDbException {
        String xmlFileName =  System.getenv("HOME") + "/bin/data/genTestpulses_DetecorLike.xml";

        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(
                new HardwareRegistry(), context);

        //TestPresets.TestPreset preset = new TestPresets.SinglePulser_SnapshotPretrigger0(); //ReapetPulserBC0_SnapshotBC0
        //TestPresets.TestPreset preset = new TestPresets.ReapetPulserBC0_SnapshotBC0(); 
        //TestPresets.TestPreset preset = new TestPresets.ReapetPulserPretrigger0_SnapshotL1A();
        //TestPresets.TestPreset preset = new TestPresets.ReapetPulserBC0_DaqLike();
        //TestPresets.TestPreset preset = new TestPresets.ReapetPulserBC0_SnapshotL1A();
        TestPresets.TestPreset preset = new TestPresets.SinglePulser_SnapshotL1A();
        
        try {
                       

            TestPulsesGenerator generator = new TestPulsesGenerator();
            String[] tcs = { 
                    /*"TC_0",                    
                    "TC_1",
                    "TC_2",

                    "TC_3",                    
                    "TC_4",
                    "TC_5",

                    "TC_6",                    
                    "TC_7",
                    "TC_8",

                    "TC_9",                    
                    "TC_10",
                    "TC_11",*/
            		"PASTEURA_TC"                 
            };
            //generator.generateLinkDataDetecorLike(128, 10, equipmentDAO);
            int[] tcNum =  {9};
            generator.generateLinkData(tcNum);
            generator.writeFile(xmlFileName);
            
            TestOptionsXml testOptions = new TestOptionsXml();
            TestsManager testsManager = new TestsManager(xmlFileName, preset, testOptions, equipmentDAO, null, dbMapper);
            PulserTarget pulseTarget = PulserTarget.chipOut; //TODO       
            
            for(String tc : tcs) {              
                System.out.println("adding crate TC_" + tc);
                testOptions.addChips("pulsers", HardwareType.CRATE, tc, ChipType.OPTO);

                //testOptions.addChips("readouts", HardwareType.CRATE, "TC_" + tc, ChipType.PAC); 
                //testOptions.addChips("readouts", HardwareType.CRATE, "TC_" + tc, ChipType.RMB);             

                /*List<Chip> chips = testOptions.getChips("readouts", equipmentDAO);
                for(Chip ch : chips) {     
                    if(ch.getType() == ChipType.RMB) {
                        dbMapper.getChip(ch).getHardwareChip().getItem("RMB_DATA_DELAY").write(0x29); //skad taka wartosc to nie wiem 
                    }
                }*/
            }

           testOptions.getEventBuilderOptions().setPacDataSel(ChipDataSel.in);

           testsManager.setPulsers();
           testsManager.setPulseTarget(pulseTarget);
           //testsManager.setReadeouts(); 
           
           //testsManager.interConnectionTest(true, false, false, 1);           
           //testsManager.daqPulserTest(true, true);
           testsManager.preparePulsers();
           testsManager.startPulsers();
           System.out.println("type a char to stop pulsers");
           BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
           String y_n = br.readLine(); 
           testsManager.stopPulsers();
           

        } catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
            context.closeSession();
        }
    }

}
