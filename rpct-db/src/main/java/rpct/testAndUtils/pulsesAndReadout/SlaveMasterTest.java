package rpct.testAndUtils.pulsesAndReadout;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;

import rpct.datastream.MonitorManager;
import rpct.datastream.RpctDelays;
import rpct.datastream.TestOptionsXml;
import rpct.datastream.TestPresets;
import rpct.datastream.TestPulsesGenerator;
import rpct.datastream.TestsManager;
import rpct.datastream.TestPresets.SinglePulser_SnapshotPretrigger0;
import rpct.datastream.TestPresets.TestPreset;
import rpct.datastream.TestsManager.PulserTarget;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.XdaqApplication;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.DeviceItem;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;

public class SlaveMasterTest {

    public static void main(String[] args) throws DataAccessException {
        HibernateContext context = new SimpleHibernateContextImpl();
        try { 
            EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
            HardwareDbMapper dbMapper = new HardwareDbMapper(
                    new HardwareRegistry(), context);

            ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
            ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);

            

            String xmlFileName =  System.getenv("HOME") + "/bin/data/linksTest.xml";  
            PulserTarget pulseTarget = PulserTarget.chipIn; //TODO 

            //TestPresets.TestPreset preset = new TestPresets.LongPulser_SnapshotPretrigger0(20, bxInSec * 100);
            //TestPresets.TestPreset preset = new TestPresets.SinglePulser_SnapshotPretrigger0();
            //TestPresets.TestPreset preset = new TestPresets.SinglePulser_SnapshotL1A();
            //TestPresets.TestPreset preset = new TestPresets.ReapetPulserBC0_DaqLike();
            TestPresets.TestPreset preset = new TestPresets.ReapetPulserBC0_SnapshotBC0();
            
            String lbConfigKey = ConfigurationManager.LOCAL_CONF_KEY_LBS_DEFAULT; //TODO LOCAL_CONF_KEY_LBS_LHC2
            
            TestOptionsXml testOptions = TestOptionsXml.readFromFile(System.getenv("HOME") + "/bin/config/optLinksTestConfig.xml");

            TestPulsesGenerator generator = new TestPulsesGenerator();                      

            List<Chip> synCoders = testOptions.getChips("pulsers", equipmentDAO);
            List<Integer> lbIds = new ArrayList<Integer>();            
            List<Integer> leftSlaveIds = new ArrayList<Integer>();
            List<Integer> rightSlaveIds = new ArrayList<Integer>();
            
            for(Chip ch : synCoders) {
                if(ch.getType() == ChipType.SYNCODER) {
                	System.out.println(ch.getBoard());
                    lbIds.add(ch.getBoard().getId());
                    if( ((LinkBoard)ch.getBoard()).getChannelNum() == 1 ) {
                        leftSlaveIds.add(ch.getBoard().getId());
                    }
                    if( ((LinkBoard)ch.getBoard()).getChannelNum() == 2 ) {
                        rightSlaveIds.add(ch.getBoard().getId());
                    }
                }
            }
            
            System.out.println("XDAQ search, found: ");
            XdaqApplication appl = dbMapper.getConfigurationDAO().getXdaqApplication(synCoders.get(0));            
            System.out.println(appl.getXdaqExecutive().toString());
            System.out.println(appl);
            System.out.println("XDAQ found");
            
            TestsManager testsManager = new TestsManager(xmlFileName, preset, testOptions, equipmentDAO, configurationManager, dbMapper, lbConfigKey);
            testsManager.setPulsers();
            testsManager.setPulseTarget(pulseTarget);
            testsManager.setReadeouts();			

            MonitorManager monitorManager = new MonitorManager();
            List<Chip> chips = testOptions.getChips("readouts", equipmentDAO);
            List<HardwareDbChip> hdbChips = new ArrayList<HardwareDbChip>();            
            for(Chip ch : chips) {               
                hdbChips.add(dbMapper.getChip(ch));                
            }                   

            testsManager.setMonitorManager(monitorManager);
            monitorManager.resetRecErrorCnt();
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String y_n = "y"; 
            
            if(true) { //testing slave - master conncetions
                System.out.println("test slave - master conncetions?");
                //y_n = br.readLine();
                if(y_n.equals("y"))  {
                    if(generator.dataStream.getEvents() != null)
                        generator.dataStream.getEvents().clear();
                    
                    System.out.println("\ntesting left slave - master conncetions");
                    generator.generateLBDataForSlaveMasterTest(leftSlaveIds);
                    generator.writeFile(xmlFileName);
                    testsManager.interConnectionTest(false, false, 1, 1);

                    System.out.println("\ntesting right slave - master conncetions");
                    generator = new TestPulsesGenerator();
                    generator.dataStream.getEvents().clear();
                    generator.generateLBDataForSlaveMasterTest(rightSlaveIds);
                    generator.writeFile(xmlFileName);
                    testsManager.interConnectionTest(false, false, 2, 1);
                    
                    for(HardwareDbChip chip : hdbChips) {
                    	if(((LinkBoard)chip.getBoard().getDbBoard()).isMaster()) {
                    		DeviceItem reg0 = chip.getHardwareChip().getItem("SLAVE0_CHK_ERR_COUNT");
                    		DeviceItem reg1 = chip.getHardwareChip().getItem("SLAVE1_CHK_ERR_COUNT");
                    		if(reg0 != null && reg1 != null) {
                    			System.out.println(chip.getHardwareChip().getBoard().getName() + " " + 
                    					": SLAVE0_CHK_ERR_COUNT " + reg0.read() +
                    					": SLAVE1_CHK_ERR_COUNT " + reg1.read()  );
                    			//System.out.println("\n");
                    		}
                    	}
                    }
                }
            }

            testsManager.disableLbPuls();
            
        } catch (DataAccessException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (ServiceException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (XdaqException e) {
            e.printStackTrace();
        } catch (HardwareDbException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
            context.closeSession();
        }
    }

}
