package rpct.testAndUtils.pulsesAndReadout;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;

import rpct.datastream.MonitorManager;
import rpct.datastream.TestOptionsXml;
import rpct.datastream.TestPresets;
import rpct.datastream.TestsManager;
import rpct.datastream.TestOptionsXml.ChipSource;
import rpct.datastream.TestOptionsXml.HardwareType;
import rpct.datastream.TestOptionsXml.SourcesList;
import rpct.datastream.TestPresets.NoPulser_SnapshotL1A;
import rpct.datastream.TestPresets.TestPreset;
import rpct.datastream.TestsManager.PulserTarget;
import rpct.db.DataAccessException;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.SorterCrate;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.diag.TriggerDataSel;
import rpct.xdaqaccess.diag.DiagCtrl.TriggerType;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;

public class DaqSnapshot {

    public static void main(String[] args) throws ServiceException,
    DataAccessException, IOException, InterruptedException,
    XdaqException, HardwareDbException {
        String xmlFileName =  System.getenv("HOME") + "/bin/data/gentestpulses.xml";

        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(
                new HardwareRegistry(), context);

        //TestPresets.TestPreset preset = new TestPresets.NoPulser_SnapshotTriggerLocal(3);
        TestPresets.TestPreset preset = new TestPresets.NoPulser_SnapshotL1A(1);
        //TestPresets.TestPreset preset = new TestPresets.NoPulser_SnapshotBC0();
        try {
            PulserTarget pulseTarget = PulserTarget.chipOut; //TODO            
            TestOptionsXml testOptions = new TestOptionsXml();
              
            
            //testOptions.addChips("readouts", HardwareType.CRATE, "TC_0", ChipType.RMB);
           // testOptions.addChips("readouts", HardwareType.CRATE, "TC_1", ChipType.PAC);
          //  testOptions.addChips("readouts", HardwareType.CRATE, "TC_2", ChipType.RMB);
          //  testOptions.addChips("readouts", HardwareType.CRATE, "TC_2", ChipType.PAC);
//            testOptions.addChips("readouts", HardwareType.CRATE, "TC_3", ChipType.RMB);
//            testOptions.addChips("readouts", HardwareType.CRATE, "TC_4", ChipType.RMB);
//            testOptions.addChips("readouts", HardwareType.CRATE, "TC_5", ChipType.RMB);
//            testOptions.addChips("readouts", HardwareType.CRATE, "TC_6", ChipType.RMB);
//            testOptions.addChips("readouts", HardwareType.CRATE, "TC_7", ChipType.RMB);
//            testOptions.addChips("readouts", HardwareType.CRATE, "TC_8", ChipType.RMB);
            //testOptions.addChips("readouts", HardwareType.CRATE, "TC_2", ChipType.RMB);
            
            testOptions.addChips("readouts", HardwareType.CRATE, "PASTEURA_TC", ChipType.RMB);
                                
            //testOptions.addChips("readouts", HardwareType.CRATE, "TC_1", ChipType.PAC);
            //testOptions.addChips("readouts", HardwareType.CRATE, "TC_1", ChipType.GBSORT);
            //testOptions.addChips("readouts", HardwareType.CRATE, "TC_1", ChipType.TCSORT);
            
            
            List<Chip> chips = testOptions.getChips("readouts", equipmentDAO);
/*            for(Chip ch : chips) {     
                if(ch.getType() == ChipType.RMB) {
                    dbMapper.getChip(ch).getHardwareChip().getItem("RMB_DATA_DELAY").write(RpctDelays.RMB_DATA_DELAY +2); //TODO - 
                }
            }*/
            
            TestsManager testsManager = new TestsManager(xmlFileName, preset, testOptions, equipmentDAO, null, dbMapper);
            //testsManager.setPulsers();
            //testsManager.setPulseTarget(pulseTarget);
            testsManager.setReadeouts();
                        		
            MonitorManager monitorManager = new MonitorManager();            
            //List<Chip> chips = testOptions.getChips("readouts", equipmentDAO);
            monitorManager.addChips(chips, dbMapper);
            testsManager.setMonitorManager(monitorManager);
            //monitorManager.enableTransmissionCheck(false, false); //TODO

            //testsManager.interConnectionTest(true, false);
            testsManager.singleRedaouts();
            //pulsesManager.algorithmsTest();

            monitorManager.readRecErrorCnt();
        } catch (JAXBException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            context.closeSession();
        }
    }

}
