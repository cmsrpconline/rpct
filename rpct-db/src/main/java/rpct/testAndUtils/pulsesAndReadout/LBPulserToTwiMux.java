package rpct.testAndUtils.pulsesAndReadout;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;

import rpct.datastream.MonitorManager;
import rpct.datastream.TestOptionsXml;
import rpct.datastream.TestPresets;
import rpct.datastream.TestPulsesGenerator;
import rpct.datastream.TestsManager;
import rpct.datastream.TestOptionsXml.HardwareType;
import rpct.datastream.TestOptionsXml.EventBuilderOptions.ChipDataSel;
import rpct.datastream.TestPresets.ReapetPulserBC0_SnapshotBC0;
import rpct.datastream.TestPresets.TestPreset;
import rpct.datastream.TestsManager.PulserTarget;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;
import rpct.xdaqaccess.synchro.DefOut;

public class LBPulserToTwiMux {

	public static void main(String[] args) throws ServiceException,
	DataAccessException, IOException, InterruptedException,
	XdaqException, HardwareDbException 
	{
		HibernateContext context = new SimpleHibernateContextImpl();
		EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
		HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);	
        

        String lbConfigKey = ConfigurationManager.LOCAL_CONF_KEY_LBS_DEFAULT; //TODO
        
		long bxInSec = 40000000;
		boolean generatePulses = true; //TODO				
		//String xmlFileName =  System.getenv("HOME") + "/bin/testPulses/lbErrorPulses.xml"; //TODO
		String xmlFileName =  System.getenv("HOME") + "/bin/testPulses/testPulses.xml"; //TODO
		if(generatePulses)
			xmlFileName =  System.getenv("HOME") + "/bin/data/genTestpulses.xml";

		boolean comapareWithSource = false; //TODO

		PulserTarget pulseTarget = PulserTarget.lbGol; //TODO 

		TestOptionsXml testOptions = new TestOptionsXml();
		testOptions.getEventBuilderOptions().setStoreLbCoderData(false); //TODO
		testOptions.getEventBuilderOptions().setStoreLbLmuxData(true);
		testOptions.getEventBuilderOptions().setStoreLbStripData(false);

		 int[] data = new int[]{
				 0x020100,
				 0x0504bc,
				 0x090807,
				 0x0d0c0b,
				 0x11100f,
				 0x151413,
				 0x191817,
				 0x1d1c1b,
				 0x21201f,
				 0x252423,
				 0x292827,
				 0x2d2c2b,
				 0x31302f,
				 0x353433,
				 0x393837,
				 0x3d3c3b,
			    };
			    
		 
		//TestPresets.TestPreset preset = new TestPresets.ReapetPulserBC0_SnapshotBC0(); //ReapetPulserBC0_SnapshotBC0 //TODO
		//TestPresets.TestPreset preset = new TestPresets.LongPulser_SnapshotBc0(128, 0xffffffff); //ReapetPulserBC0_SnapshotBC0 //TODO
		TestPresets.TestPreset preset = new TestPresets.LongPulser_SnapshotBc0(data.length, 0xfffffff);
		//preset.setReadoutLimit(120);

		try {			           
			TestPulsesGenerator generator = new TestPulsesGenerator();

			/*for(Crate crate : equipmentDAO.getCratesByType(CrateType.LINKBOX, true) ) {
				if(crate.getName().contains("904") )
				//if(crate.getName().contains("RB0_S11") ) //&& 
				//if(((LinkBox)crate).getTowerName().equals("RB-1_far") ) 
				{
					System.out.println(crate.getName());
					testOptions.addChips("pulsers", HardwareType.CRATE, crate.getName(), ChipType.SYNCODER);
					testOptions.addChips("readouts", HardwareType.CRATE, crate.getName(), ChipType.SYNCODER);
				}
			}	*/
			
			/*testOptions.addChips("pulsers", HardwareType.BOARD, "LB_LBB_904_S0", ChipType.SYNCODER);
			testOptions.addChips("readouts", HardwareType.BOARD, "LB_LBB_904_S0", ChipType.SYNCODER);*/
			testOptions.addChips("pulsers", HardwareType.BOARD, "LB_LBB_904_M2", ChipType.SYNCODER);
			testOptions.addChips("readouts", HardwareType.BOARD, "LB_LBB_904_M2", ChipType.SYNCODER);
			/*testOptions.addChips("pulsers", HardwareType.BOARD, "LB_LBB_904_S2", ChipType.SYNCODER);
			testOptions.addChips("readouts", HardwareType.BOARD, "LB_LBB_904_S2", ChipType.SYNCODER);*/
			
			
		   
			List<Chip> synCoders = testOptions.getChips("pulsers", equipmentDAO);
			if(generatePulses) {
				List<Integer> lbIds = new ArrayList<Integer>();
				for(Chip ch : synCoders) {
					if(ch.getType() == ChipType.SYNCODER)
						lbIds.add(ch.getBoard().getId());
				}          
				generator.generateLBDataForTwinMux(lbIds, data);
				//generator.generateLBDataRandom(lbIds, 10, 5);
				generator.writeFile(xmlFileName);
			}

			TestsManager testsManager = new TestsManager(xmlFileName, preset, testOptions, equipmentDAO, configurationManager, dbMapper, lbConfigKey);
			testsManager.setPulsers();
			testsManager.setPulseTarget(pulseTarget);
			testsManager.setReadeouts();
                    
			
            //testsManager.interConnectionTest(false, false, false, 1);
			testsManager.daqPulserTest(comapareWithSource, false);
			
			//testsManager.algorithmsTest();
                        
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			context.closeSession();
		}
	}

}
