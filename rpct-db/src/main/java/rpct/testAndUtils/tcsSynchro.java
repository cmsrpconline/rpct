package rpct.testAndUtils;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.rpc.ServiceException;

import rpct.datastream.TestOptionsXml;
import rpct.datastream.TestPresets;
import rpct.datastream.TestPulsesGenerator;
import rpct.datastream.TestsManager;
import rpct.datastream.TestOptionsXml.HardwareType;
import rpct.datastream.TestOptionsXml.EventBuilderOptions.ChipDataSel;
import rpct.datastream.TestsManager.PulserTarget;
import rpct.db.DataAccessException;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.XdaqException;
import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.Device;
import rpct.xdaqaccess.DeviceItemWord;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDBTTUBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbChipImpl;
import rpct.xdaqaccess.hardwaredb.HardwareDbCrate;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbSorterCrate;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;

public class tcsSynchro {
    
    static void setParametrs(Device device, int phase) throws RemoteException, ServiceException {
        Binary muxClk90 = null;
        Binary muxClInv = null;
        Binary muxRegAdd = null;
        int muxDelay = 0;
        int dataDelay = 0;

        if(phase == 0) {
            muxClk90 = new Binary("000000000000000000000");
            muxClInv = new Binary("000000000000000000000");
            muxRegAdd = new Binary("1ffffffffffffffffffff");
            
            muxDelay = 5;
            dataDelay = 0;
        }
        else if(phase == 1) {
            muxClk90 = new Binary("1ffffffffffffffffffff");
            muxClInv = new Binary("000000000000000000000");
            muxRegAdd = new Binary("1ffffffffffffffffffff");
            
            muxDelay = 5;
            dataDelay = 0;
        }
        else if(phase == 2) {
            muxClk90 = new Binary("000000000000000000000");
            muxClInv = new Binary("1ffffffffffffffffffff");
            muxRegAdd = new Binary("1ffffffffffffffffffff");
            
            muxDelay = 5;
            dataDelay = 0;
        }
        else if(phase == 3) {
            muxClk90 = new Binary("1ffffffffffffffffffff");
            muxClInv = new Binary("1ffffffffffffffffffff");
            muxRegAdd = new Binary("1ffffffffffffffffffff");
            
            muxDelay = 4;
            dataDelay = 0;
        }
        
        device.getItem("REC_MUX_CLK90").writeBinary(muxClk90);
        device.getItem("REC_MUX_CLK_INV").writeBinary(muxClInv);
        device.getItem("REC_MUX_REG_ADD").writeBinary(muxRegAdd);
                
        DeviceItemWord recMuxDealy = (DeviceItemWord) device.getItem("REC_MUX_DELAY");
        for(int i = 0; i < recMuxDealy.getNumber(); i++) {
            recMuxDealy.write(i, muxDelay);
        }
         
        DeviceItemWord recDataDealy = (DeviceItemWord) device.getItem("REC_DATA_DELAY");
        for(int i = 0; i < recDataDealy.getNumber(); i++) {
            recDataDealy.write(i, dataDelay);
        }
    }
    
    public static void main(String[] args) throws DataAccessException {                  
        
        HibernateContext context = new SimpleHibernateContextImpl();
        try {           
            String[] tcs = {
                    "TC_0",
//                    "TC_1",
//                    "TC_2",
//                    "TC_3",
//                    "TC_4",
//                    "TC_5",
//                    "TC_6",
//                    "TC_7",
//                    "TC_8",                        
//                    "TC_9",
//                    "TC_10",
//                    "TC_11",
                    //"PASTEURA_TC"
                    //"SC"
            };
            
            //
            //1ff0800000ff0400001ff
            EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
            HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
            List<HardwareDbTriggerCrate> crates = new ArrayList<HardwareDbTriggerCrate>();
            
            for(int i = 0; i < tcs.length; i++) {
                crates.add((HardwareDbTriggerCrate)(dbMapper.getCrate(equipmentDAO.getCrateByName(tcs[i]))));
            }
            
            String xmlFileName =  System.getenv("HOME") + "/bin/data/genTestpulses.xml";
            TestPresets.TestPreset preset = new TestPresets.LongPulser_SnapshotPretrigger0(128, 0xffffffffffl);
            PulserTarget pulseTarget = PulserTarget.chipOut; //TODO            
            
            TestPulsesGenerator generator = new TestPulsesGenerator();
            
            TestOptionsXml testOptions = new TestOptionsXml();
            for(String tc : tcs) {                
                System.out.println("testing crate " + tc);
                testOptions.addChips("pulsers", HardwareType.CRATE, tc, ChipType.PAC);

                //testOptions.addChips("readouts", HardwareType.CRATE, "TC_" + tc, ChipType.PAC); 
                //testOptions.addChips("readouts", HardwareType.CRATE, "TC_" + tc, ChipType.GBSORT);
                //testOptions.addChips("readouts", HardwareType.CRATE, "TC_" + tc, ChipType.TCSORT);               
            }
            
            //testOptions.getEventBuilderOptions().setTbsDataSel(ChipDataSel.inAndOut);
            //testOptions.getEventBuilderOptions().setTcsDataSel(ChipDataSel.in); 
            
            TestsManager testsManager = new TestsManager(xmlFileName, preset, testOptions, equipmentDAO, null, dbMapper, "");
            testsManager.setPulsers();
            testsManager.setPulseTarget(pulseTarget);
            //testsManager.setReadeouts();


            testsManager.preparePulsers();
            testsManager.startPulsers();
            
            for (HardwareDbTriggerCrate hdbTc : crates) {
                long[] recErrorCnt = new long[4];
                
                for(int phase = 0; phase < 4; phase++) {
                    
                    setParametrs(hdbTc.getTcSort().getHardwareChip(), phase);
                    System.out.println("phase " + phase);
                    ((HardwareDbChipImpl)hdbTc.getTcSort()).resetRecErrorCnt();
                                        
                    Thread.sleep(1000);
                    StringBuilder msg = new StringBuilder("aaa ");
                    recErrorCnt[phase] = ((HardwareDbChipImpl)hdbTc.getTcSort()).readErrorCnt(msg);
                    System.out.println(hdbTc.getTcSort().getFullName() + " " + msg.toString());   
                }
                
                int goodPhaseCnt = 0;
                for(int phase = 0; phase < 4; phase++) { 
                    if(recErrorCnt[phase] == 0)
                        goodPhaseCnt++;
                }
                
                int goodPhase = -1;
                if(goodPhaseCnt == 1) {
                    for(int phase = 0; phase < 4; phase++) { 
                        if(recErrorCnt[phase] == 0)
                            goodPhase = phase;
                    }
                }
                else if(goodPhaseCnt == 2) {
                    for(int phase = 0; phase < 4; phase++) { 
                        if(recErrorCnt[phase] == 0 && recErrorCnt[ (phase+1)%4 ] == 0) {                            
                            if(recErrorCnt[ (4 + phase - 1)%4 ] < recErrorCnt[ (phase+2)%4 ])
                                goodPhase = phase;
                            else 
                                goodPhase = (phase+1)%4;
                            break;
                        }
                    }
                }
                else if(goodPhaseCnt == 3) {
                    for(int phase = 0; phase < 4; phase++) { 
                        if(recErrorCnt[phase] == 0) {
                            goodPhase = (phase+2)%4 ;
                            break;
                        }
                    }
                }
                
                if(goodPhase == -1)
                    System.out.println(hdbTc.getName() + " good phase not found !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                else {
                    setParametrs(hdbTc.getTcSort().getHardwareChip(), goodPhase);
                    System.out.println(hdbTc.getName() + " found goodPhase " + goodPhase);
                }
                
            }
            testsManager.stopPulsers();
            
            System.out.println("End");
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (HardwareDbException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (XdaqException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } 
        finally {
            context.closeSession();
        }
    }
}
