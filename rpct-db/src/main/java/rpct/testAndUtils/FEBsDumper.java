package rpct.testAndUtils;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.PropertyConfigurator;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.CrateDAO;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.FebBoard;
import rpct.db.domain.equipment.I2cCbChannel;
import rpct.db.domain.equipment.LinkBoardDAO;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.equipment.chamberloaction.BarrelOrEndcap;
import rpct.db.domain.equipment.chamberloaction.ChamberLocation;
import rpct.db.domain.equipment.chamberloaction.ChamberLocationDAO;
import rpct.db.domain.equipment.chamberloaction.ChamberLocationDAOHibernate;
import rpct.db.domain.equipment.chambertype.ChamberType;
import rpct.db.domain.equipment.controlboard.ControlBoard;
import rpct.db.domain.equipment.distributionboard.DistributionBoardDAO;
import rpct.db.domain.equipment.feblocation.FebLocation;
import rpct.db.domain.equipment.feblocation.FebLocationDAO;
import rpct.db.domain.equipment.feblocation.FebLocationDAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;

/**
 * Created on 2010-10-03
 * 
 * @author Karol Bunkowski
 * @version $Id$
 */
public class FEBsDumper {
    private FebLocationDAO daoFebLocation;
    private ChamberLocationDAO chamberLocationDAO;
    
    
    private HibernateContext hibernateContext;
    private EquipmentDAO equipmentDAO;
    
    public FEBsDumper(HibernateContext hibernateContext) {
        this.hibernateContext = hibernateContext;
        this.equipmentDAO = new EquipmentDAOHibernate(this.hibernateContext);
        this.daoFebLocation =  new FebLocationDAOHibernate(hibernateContext);
        this.chamberLocationDAO = new ChamberLocationDAOHibernate(hibernateContext);
    }
    
   
   
    
    void correct() throws DataAccessException {
    	List<Board> febBoards =new ArrayList<Board>(); 
    	febBoards.add(equipmentDAO.getBoardByName("FEB_LBB_904_L10_C4_A0"));
    	febBoards.add(equipmentDAO.getBoardByName("FEB_LBB_904_L10_C4_A1"));
    	febBoards.add(equipmentDAO.getBoardByName("FEB_LBB_904_L10_C4_A2"));
    	for(Board board : febBoards) {
    		System.out.println(board);
    		for(int i = 2; i < 4; i++) {
    			Chip febChip = new Chip();
    			febChip.setType(ChipType.FEBCHIP);
    			febChip.setBoard(board);
    			febChip.setPosition(i);
    			equipmentDAO.saveObject(febChip);
    		}
    	}
    }
    
    void printSelectedFebsByLocation() throws DataAccessException {
    	int i = 1;
    	List<FebLocation>  febLocations = daoFebLocation.getFebs();
    	for(FebLocation febLocation : febLocations) {
    		if(febLocation.getChamberLocation().getChamberLocationName().contains("W0/RB2out/8")) {
    			System.out.println(
    					i  + 
    					" ChamberLocation " + febLocation.getChamberLocation().getChamberLocationName() + " id "+ febLocation.getChamberLocation().getId());   				
    			System.out.println("febLocation id " + febLocation.getId() + " etaPart " + febLocation.getFebLocalEtaPartition() + 
    					" " + febLocation.getI2cCbChannel().toString() +
    					" I2cLocalNumber " + febLocation.getI2cLocalNumber() +
    					" LB " + febLocation.getLinkBoard()    				
    					);

    			System.out.print("FebBoard ");		
    			if(febLocation.getFebBoard() != null && febLocation.getFebBoard().getChips() != null) {
    				System.out.println(febLocation.getFebBoard().getName() + " id "+ febLocation.getFebBoard().getId());
    				System.out.print("chips postions ");
    				for(Chip chip : febLocation.getFebBoard().getChipsArray()) {    			
    					System.out.print(chip.getPosition() + " ");
    				}
    			}
    			System.out.println("\n");
    			i++;
    		}
    	}
    }
    
    void printFebsByLocation() throws DataAccessException {
    	int i = 1;
    	List<FebLocation>  febLocations = daoFebLocation.getFebs();
    	for(FebLocation febLocation : febLocations) {
    		System.out.println(
    				i  + 
    				" ChamberLocation " + febLocation.getChamberLocation().getChamberLocationName() + " id "+ febLocation.getChamberLocation().getId());   				
    		System.out.println("febLocation id " + febLocation.getId() + " etaPart " + febLocation.getFebLocalEtaPartition() + 
    				" " + febLocation.getI2cCbChannel().toString() +
    				" I2cLocalNumber " + febLocation.getI2cLocalNumber() +
    				" LB " + febLocation.getLinkBoard()    				
    		);
    		
    		System.out.print("FebBoard ");		
    		if(febLocation.getFebBoard() != null && febLocation.getFebBoard().getChips() != null) {
    			System.out.println(febLocation.getFebBoard().getName() + " id "+ febLocation.getFebBoard().getId());
    			System.out.print("chips postions ");
    			for(Chip chip : febLocation.getFebBoard().getChipsArray()) {    			
    				System.out.print(chip.getPosition() + " ");
    			}
    		}
    		System.out.println("\n");
    		i++;
    	}
    }
    
   
	/**
	 * @param args
	 * @throws DataAccessException 
	 */       
	public static void main(String[] args) throws DataAccessException {
		PropertyConfigurator.configure("log4j.properties");
        HibernateContext hibernateContext = new SimpleHibernateContextImpl();
        
		try {
			FEBsDumper febsDumper = new FEBsDumper(hibernateContext);
			febsDumper.printSelectedFebsByLocation();
			//generator.test(false);
			//generator.generateFebs904(true);
			//generator.correct();
			//generator.generateFEBBoardsP5(true);
			//generator.deleteFEBBoardsAndChips(false);
			
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
			hibernateContext.rollback();
		} finally {
			hibernateContext.closeSession();
		}
	}

}
