package rpct.testAndUtils.rbcAndTtu;

import java.util.Calendar;
import java.util.List;
import java.util.Vector;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.RbcConf;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.RbcBoard;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;

/**
 * Created on 2009-10-20
 * 
 * @author Nikolay Darmenov - Andres Osorio
 * @version $Id: rbcConfTest.java 1563 2010-02-08 12:30:40Z kbunkow $
 * dump RBC DB configuration
 */
public class RbcCheckDBConfig {

	static Vector<String> rbcNames = new Vector<String>();

	static void addNames() {
		String[] wheels = { "+2", "+1", "0", "-1", "-2" };
		int[] sectors = { 1, 3, 5, 7, 9, 11 };

		for (String wheel : wheels) {
			for (int sector : sectors) {
				rbcNames.add("RBC_RB" + wheel + "_S" + sector);
			}
			// example: "RBC_RB+2_S9"

		}
	}

	public static void main(String[] args) throws DataAccessException {
		final HibernateContext context = new SimpleHibernateContextImpl();
		final EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);

		/* Configuration handling based on PutStripMasksToDB */
		final ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(
				context);
		final ConfigurationManager configurationManager = new ConfigurationManager(
				context, equipmentDAO, configurationDAO);

		// final LocalConfigKey inputConfigKey = configurationManager
		// .getDefaultLocalConfigKey();

		addNames();

		final LocalConfigKey[] outputConfigKeys = {
				configurationManager
						.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_RBCS_TTUS_LHC1),	
		};

		try {
			final List<Crate> crates = equipmentDAO.getCratesByType(
					CrateType.LINKBOX, true);

			// System.out.println("=== Dumping all RbcBoard info ===");
			for (Crate crate : crates) {
				final List<Board> boards = crate.getBoards();
				for (Board board : boards) {
					if (board.getType() == BoardType.RBCBOARD) {
						for (String rbcname : rbcNames) {
							// if (board.getName().equals("RBC_RB+2_S9")) {
							if (board.getName().equals(rbcname)) {
								final RbcBoard rbc = (RbcBoard) board;

								try {

									for (final LocalConfigKey inputConfigKey : outputConfigKeys) {
										final StaticConfiguration configuration = configurationManager
												.getConfiguration(
														rbc.getChip(),
														inputConfigKey);

										final RbcConf rbcConf = (RbcConf) configuration;

										System.out.format(board.getName() + " "
												+ inputConfigKey.getName()
												+ " configuration: id#%d %s%n",
												rbcConf.getId(), rbcConf);
									}

								} catch (NullPointerException e) {
									System.out
											.println("[WARNING] No configuration found for RBC "
													+ rbc.getName());
									System.out.println("[WARNING]    Caught \""
											+ e.getMessage() + "\"");
								}
							}
						}
					}
				}
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			System.err.println("[ERROR] context to be rollbacked");
			context.rollback();
		} finally {
			context.closeSession();
		}
	}
}
