package rpct.testAndUtils.rbcAndTtu;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.RbcConf;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardBoardConn;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.equipment.RbcBoard;
import rpct.db.domain.equipment.RbcTtuConn;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TtuBoard;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.testAndUtils.boardConfigs.ConfUtil;
import rpct.testAndUtils.boardConfigs.ConfUtil.DbHandler;
import rpct.testAndUtils.boardConfigs.ConfUtil.RegexFormatter;

/**
 * Created on 2009-12-22
 * 
 * @author Nikolay Darmenov
 * @version $Id$
 */
public class connectedRbcsMaskTest {
    private static final Logger logger = Logger.getLogger(connectedRbcsMaskTest.class);
	
	public static void main(String[] args) throws DataAccessException {
		init();

		testDirectly();
	}

	private static void init() {
	}

	private static void testDirectly() throws RuntimeException,
			DataAccessException {
		logger.info("Will test direct connection to DB ...");

		try {
			final HibernateContext context = new SimpleHibernateContextImpl();
			final EquipmentDAO equipment = new EquipmentDAOHibernate(context);
			final ConfigurationDAO configuration = new ConfigurationDAOHibernate(context);
			final ConfigurationManager manager = new ConfigurationManager(context, equipment, configuration);

				
			logger.info("Dumping some LinkConn ...");
			logger.info("* getDisabledLinksIds(): " + ConfUtil.DbHandler.configurationDAO.getDisabledLinksIds());
			final List<Integer> linkConnIds = ConfUtil.getIntListProperty("linkConnIds");
			///System.out.println("equipmentDAO.getLinkConns(): " + ConfUtil.DbHandler.equipmentDAO.getLinkConns());

			for (int connId: linkConnIds) {
				final BoardBoardConn conn = (BoardBoardConn) ((EquipmentDAOHibernate) ConfUtil.DbHandler.equipmentDAO).getObject(BoardBoardConn.class, connId);

				System.out.println("LinkConn Id #" + connId + ": " + conn);
				switch (conn.getType()) {
				case LINKCONN:
				{
					final LinkConn ltc = (LinkConn) conn;
					final TriggerBoard tb = ltc.getTriggerBoard();
					final LinkBoard lb = (LinkBoard) ltc.getBoard();
					System.out.println("   * TB: " + tb);
					System.out.println("   * LB: " + lb);
					System.out.println("   * getLinkDisabled(ltc): " + ConfUtil.DbHandler.configurationDAO.getLinkDisabled(ltc));
					System.out.println("      * getLinkConnsTriggerBoardInputNum(): " + ConfUtil.DbHandler.equipmentDAO.getLinkConnsTriggerBoardInputNum(tb, true));
					System.out.println("      * getLinkConns(): " + ConfUtil.DbHandler.equipmentDAO.getLinkConns(tb, true));
					System.out.println("      * getLinkConnByTriggerBoardAndInputNum(): " + ConfUtil.DbHandler.equipmentDAO.getLinkConnByTriggerBoardAndInputNum(tb, ltc.getTriggerBoardInputNum()));
					assert ltc.getId() == ConfUtil.DbHandler.equipmentDAO.getLinkConnByTriggerBoardAndInputNum(tb, ltc.getTriggerBoardInputNum()).getId();
				}
					break;
				case RBCTTUCONN:
				{
					final RbcTtuConn rtc = (RbcTtuConn) conn;
					final TtuBoard ttuBoard = rtc.getTtuBoard();
					final RbcBoard rbcBoard = rtc.getRbcBoard();
					System.out.println("   * TTU: " + ttuBoard);
					for (Chip rbc: ttuBoard.getChips(ChipType.TTUTRIG)) {
						System.out.println("      * ConnectedRbcMask of " + rbc);
						System.out.println("        " + TtuBoard.getConnectedRbcsMask(rbc, ConfUtil.DbHandler.getInputConfigKey(), manager, equipment));
					}

					System.out.println("   * RBC: " + rbcBoard);
				}
					break;
				default:
					throw new IllegalArgumentException("Connection \"" + conn + "\" is of unhandled type " + conn.getType());
				}
			}

			logger.info("Dumping some TTU_TRIG confs ...");
			try {
				final List<Integer> chipIds = ConfUtil.getIntListProperty("chipIds");
				final List<Chip> chips = ConfUtil.DbHandler.equipmentDAO.getChips(chipIds, false, false);
				for (Chip chip: chips) {
					ConfUtil.print(chip);
					System.out.println(ConfUtil.RegexFormatter.binFormat("   * connectedRbcsMask=" + TtuBoard.getConnectedRbcsMask(chip, ConfUtil.DbHandler.getInputConfigKey(), manager, equipment)));
				}


				final Chip chip = chips.get(chips.size() - 1);
				logger.info("Examining chip: " + chip + "...");
				
		        final TtuBoard ttuBoard = (TtuBoard) chip.getBoard();
		        logger.info("   Attached to " + ttuBoard);
		        System.out.println(ttuBoard.getRbcConn().size() + " conns: " + ttuBoard.getRbcConn());
		        assert ttuBoard.getRbcConn().size() == (ttuBoard.getName().equals("TTU_2") ? 6 : (ttuBoard.getName().equals("TTU_1") || ttuBoard.getName().equals("TTU_3") ? 12 : -1));

		        logger.debug("=== Looping over RbcTtuConn");
		        for (BoardBoardConn conn: ttuBoard.getRbcConn()) {
		        	logger.debug("==== " + conn);
					final RbcBoard rbcBoard = (RbcBoard) conn.getBoard();
					final RbcBoard rbcFakeBoard = rbcBoard.getSlave();

					for (RbcBoard board: Arrays.asList(rbcBoard, rbcFakeBoard)) {
						logger.info(board.getType() + ": " + board + ", " + (board.getDisabled() == null ? "enabled" : "disabled"));
						logger.info("   crate " + board.getCrate() + ", " + (board.getCrate().getDisabled() == null ? " enabled" : "disabled"));
						if (board.getType() == BoardType.RBCBOARD) {
							final Chip rbcChip =  board.getChip();
							assert rbcChip.getType() == ChipType.RBC;
							final RbcConf rbcConf = (RbcConf) ConfUtil.getChipConf(rbcChip);
							final int rbcMask = rbcConf.getMask();
							logger.info("   chip #" + rbcChip.getId() +	" mask: " +
									rbcMask + 
									", B" + Integer.toBinaryString(rbcMask) +
									", 0x" + Integer.toHexString(rbcMask));
						}
						
						logger.info("      LBs: " + board.getCrate().getBoards().size());
						for (Board lb: board.getCrate().getBoards()) {
							if (lb.getType() == BoardType.LINKBOARD) {
								logger.info("         " + lb + ", " +
										(lb.getDisabled() == null ? "enabled" : "disabled") +
										", in " + lb.getCrate().getName() + " "+ (lb.getCrate().getDisabled() == null ? "enabled" : "disabled"));
							}
						}
					}
		        }

			}
			catch (IllegalArgumentException e) {
				System.err.println("\"chipIds\" is empty. Use \"java ... -DchipIds='id1, id2, ...'\" to load it");
				System.exit(1);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			System.err.println("[ERROR] context to be rollbacked");
			ConfUtil.DbHandler.context.rollback();
		} finally {
			ConfUtil.DbHandler.context.closeSession();
			logger.info("DB session closed.");
		}
		
		System.out.println("Done.");
	}
}