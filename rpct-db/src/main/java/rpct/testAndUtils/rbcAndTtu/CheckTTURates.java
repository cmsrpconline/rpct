package rpct.testAndUtils.rbcAndTtu;

import java.util.Vector;

import java.io.BufferedReader;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.DeviceItemWord;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDBTTUBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbChipImpl;
import rpct.xdaqaccess.hardwaredb.HardwareDbCrate;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbOpto;
import rpct.xdaqaccess.hardwaredb.HardwareDbSorterCrate;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;
import rpct.xdaqaccess.synchro.DefOut;

public class CheckTTURates {

	static int start;
	static int timeWindow;
	static int refreshTime;
	static int outputType;

	static void setTimeWindow(HardwareDbSorterCrate sc)
			throws HardwareDbException, ServiceException, InterruptedException,
			IOException {

		for (HardwareDBTTUBoard tb : sc.getHardwareDBTTUBoards()) {

			for (HardwareDbChip ttutrig : tb.getTTUTrigs()) {

				DeviceItemWord cntr_gate = (DeviceItemWord) ttutrig
						.getHardwareChip().getItem("TA_TRG_CNTR_GATE");

				cntr_gate.write(timeWindow);

			}
		}
	}

	static void checkTTURates(HardwareDbSorterCrate sc)
			throws HardwareDbException, ServiceException, InterruptedException,
			IOException {

		int[] SectorRates = new int[12];
		Vector<Integer> finalRates = new Vector<Integer>();
		Vector<String> tbNames = new Vector<String>();

		for (HardwareDBTTUBoard tb : sc.getHardwareDBTTUBoards()) {

			String tbname = tb.getName();
			for (HardwareDbChip ttutrig : tb.getTTUTrigs()) {

				String trigname = ttutrig.getName();
				if (tbname.equals("TTU_2") && trigname.equals("TTUTRIG 9"))
					continue;

				tbNames.add(tbname + " " + trigname);

				// System.out.println("Checking: " + ttutrig.getFullName());

				/*
				 * check this one long wheelRate = ((DeviceItemWord)
				 * ttutrig.getHardwareChip() .getItem("TA_TRG_CNTR")).read();
				 * 
				 * System.out.println("TA_TRG_CNTR: " + (int)wheelRate );
				 */

				DeviceItemWord sectorRate = (DeviceItemWord) ttutrig
						.getHardwareChip().getItem("SECTOR_TRG_CNTR");

				int totRate = 0;
				int maxSector = sectorRate.getDesc().getNumber();
				for (int i = 0; i < maxSector; ++i) {
					int iRate = (int) sectorRate.read(i);
					SectorRates[i] = iRate;
					/*
					 * System.out.println("SECTOR_TRG_CNTR: " + i + ": " + iRate
					 * );
					 */
					totRate += iRate;
				}

				finalRates.add(totRate);

				String output = "";

				for (int i = 0; i < maxSector; ++i) {
					output += Integer.toString(SectorRates[i]);
					output += " ";
					finalRates.add(SectorRates[i]);
				}

				// System.out.println("Tot rate= " + totRate + " " + output);
				((HardwareDbChipImpl) ttutrig).resetRecErrorCnt();

			}

		}

		int segment = 0;
		String output = "";

		if (start == 0)
			System.out.println("===START===");

		int k = 0;

		for (int i = 0; i < finalRates.size(); ++i) {

			if (outputType == 0) {
				output += Integer.toString(finalRates.elementAt(i));
				output += " ";
			} else {
				output += Integer.toString(finalRates.elementAt(i));
				output += " | ";
			}

			if (segment == 12) {
				if (outputType == 0)
					System.out.println(output);
				else
					System.out.println(tbNames.elementAt(k) + " " + output);
				segment = 0;
				output = "";
				++k;
			} else
				++segment;
		}
		if (outputType != 0)
			System.out
					.println("----------------------------------------------------------------------");

		Thread.sleep(refreshTime);

		finalRates.clear();
		tbNames.clear();

	}

	public static void main(String[] args) {

		// ... fetch desired time window to read rates
		timeWindow = 0;
		refreshTime = 1000; // ms
		outputType = 0;
		if (args.length > 0) {
			timeWindow = Integer.parseInt(args[0]);
			refreshTime = Integer.parseInt(args[1]);
			outputType = Integer.parseInt(args[2]);
		}
		// ... set start
		start = 0;

		try {
			FileOutputStream file = new FileOutputStream(System.getenv("HOME")
					+ "/bin/out/" + "CheckTTURates.log", true);
			PrintStream prntSrm = new PrintStream(file);
			DefOut defLog = new DefOut(prntSrm);

			HibernateContext context = new SimpleHibernateContextImpl();
			EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);

			HardwareDbMapper dbMapper = new HardwareDbMapper(
					new HardwareRegistry(), context);

			String r_s = "y";
			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));
			SimpleDateFormat formatter = new SimpleDateFormat(
					"EEE dd MMM HH:mm:ss z yyyy");
			Calendar cal = Calendar.getInstance();
			String formattedDate = null;

			HardwareDbSorterCrate sc = (HardwareDbSorterCrate) dbMapper
					.getCrate(equipmentDAO.getCrateByName("SC"));

			setTimeWindow(sc);

			while (true) {
			//for (int k = 0; k < 1000; ++k) {
				checkTTURates(sc);
				++start;
			}

			//System.out.println(-1);

		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (HardwareDbException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
