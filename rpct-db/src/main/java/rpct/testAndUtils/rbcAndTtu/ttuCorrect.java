package rpct.testAndUtils.rbcAndTtu;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.DeviceItemWord;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDBTTUBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbChipImpl;
import rpct.xdaqaccess.hardwaredb.HardwareDbCrate;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbSorterCrate;

public class ttuCorrect {
    public static void main(String[] args) throws DataAccessException {                  
        
        HibernateContext context = new SimpleHibernateContextImpl();
        try {           
            
            EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
            HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
            List<HardwareDbCrate> crates = new ArrayList<HardwareDbCrate>();
            crates.add(dbMapper.getCrate(equipmentDAO.getCrateByName("SC")));
                                   
          //HardwareDBTTUBoard ttuBoard = (HardwareDBTTUBoard)dbMapper.getBoard(equipmentDAO.getBoardByName("TTU_1"));
            HardwareDbSorterCrate dbSorterCrate = (HardwareDbSorterCrate)dbMapper.getCrate(equipmentDAO.getCrateByName("SC"));            
            for(HardwareDBTTUBoard ttuBoard : dbSorterCrate.getHardwareDBTTUBoards()) {                
/*                for(HardwareDbChip chip : ttuBoard.getOptos()) {
                    //System.out.println(chip.getFullName() + " " + chip.getHardwareChip().getItem("STATUS.REC_SYNCH_ACK").read()); 
                    
                    chip.getHardwareChip().getItem("SEND_CHECK_ENA").write(3);
                    chip.getHardwareChip().getItem("SEND_CHECK_DATA_ENA").write(3);
                    chip.getHardwareChip().getItem("BCN0_DELAY").write(0x32);
                    
                    chip.getHardwareChip().getItem("SEND_CHECK_ENA").write(0);
                    chip.getHardwareChip().getItem("SEND_CHECK_DATA_ENA").write(0);                   
                }*/
                
/*                for(HardwareDbChip chip : ttuBoard.getTTUTrigs()) {
                    chip.getHardwareChip().getItem("REC_CHECK_ENA").write(0xfff);
                    chip.getHardwareChip().getItem("REC_CHECK_DATA_ENA").write(0xfff);
                    chip.getHardwareChip().getItem("BCN0_DELAY").write(0x37);    
                    chip.getHardwareChip().getItem("REC_CHECK_ENA").write(0);
                    chip.getHardwareChip().getItem("REC_CHECK_DATA_ENA").write(0);
                }*/
                
/*                for(HardwareDbChip chip : ttuBoard.getTTUTrigs()) {                  
                    ((HardwareDbChipImpl)chip).resetRecErrorCnt();
                }*/
            	
            	
                ttuBoard.getControlChip().getItem("TTC.TC_DELAY").write(7);
            }
            
/*            for(HardwareDBTTUBoard ttuBoard : dbSorterCrate.getHardwareDBTTUBoards()) {                               
                for(HardwareDbChip chip : ttuBoard.getTTUTrigs()) {
                    StringBuilder msg = new StringBuilder();
                    System.out.println(chip.getFullName() + msg.toString());  
                }
            }*/

            System.out.println("End");
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (HardwareDbException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } 
        finally {
            context.closeSession();
        }
    }
}
