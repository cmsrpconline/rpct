package rpct.testAndUtils.rbcAndTtu;

import java.util.Vector;

import java.io.BufferedReader;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.rpc.ServiceException;

import org.apache.axis.types.HexBinary;

import rpct.db.DataAccessException;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.DeviceItemWord;
import rpct.xdaqaccess.DeviceItemBits;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDBTTUBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDBTriggerBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbChipImpl;
import rpct.xdaqaccess.hardwaredb.HardwareDbCrate;
import rpct.xdaqaccess.hardwaredb.HardwareDbException;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;
import rpct.xdaqaccess.hardwaredb.HardwareDbOpto;
import rpct.xdaqaccess.hardwaredb.HardwareDbSorterCrate;
import rpct.xdaqaccess.hardwaredb.HardwareDbTriggerCrate;
import rpct.xdaqaccess.synchro.DefOut;
import rpct.xdaq.axis.Binary;

public class CheckTTUConfig {

	static Vector<String> optoRegisters = new Vector<String>();
	static Vector<String> optoBitRegisters = new Vector<String>();
	static Vector<String> trigRegisters = new Vector<String>();
	static Vector<String> sectRegisters = new Vector<String>();

	static void addRegisters() {

		optoBitRegisters.add("TLK.OPTO_SIG");
		optoBitRegisters.add("TLK.ENABLE");
		optoBitRegisters.add("TLK.LCK_REFN");
		optoBitRegisters.add("STATUS.REC_SYNCH_REQ");

		optoRegisters.add("REC_CHECK_ENA");
		optoRegisters.add("REC_CHECK_DATA_ENA");
		optoRegisters.add("SEND_CHECK_ENA");
		optoRegisters.add("SEND_CHECK_DATA_ENA");
		optoRegisters.add("SEND_TEST_ENA");
		optoRegisters.add("BCN0_DELAY");

		trigRegisters.add("REC_MUX_DELAY");
		trigRegisters.add("REC_MUX_CLK90");
		trigRegisters.add("REC_MUX_CLK_INV");
		trigRegisters.add("REC_MUX_REG_ADD");
		trigRegisters.add("BCN0_DELAY");
		trigRegisters.add("REC_DATA_DELAY");

		sectRegisters.add("TRIG_MASK");
		sectRegisters.add("TRIG_FORCE");
		sectRegisters.add("SECTOR_MAJORITY");

	}

	static void checkTTUConfig(HardwareDbSorterCrate sc)
			throws HardwareDbException, ServiceException, InterruptedException,
			IOException {

		for (HardwareDBTTUBoard tb : sc.getHardwareDBTTUBoards()) {

			String tbname = tb.getName();

			// OPTO chips
			for (HardwareDbOpto opto : tb.getOptos()) {

				for (String register : optoRegisters) {

					DeviceItemWord regValue = (DeviceItemWord) ((HardwareDbChip) opto)
							.getHardwareChip().getItem(register.toString());

					System.out.println(opto.getFullName() + " " + register
							+ " " + Integer.toHexString((int) regValue.read()));
				}

				for (String register : optoBitRegisters) {

					DeviceItemBits regValue = (DeviceItemBits) ((HardwareDbChip) opto)
							.getHardwareChip().getItem(register.toString());

					System.out.println(opto.getFullName() + " " + register + " "
							+ Integer.toHexString((int) regValue.read()));
				}

			}

			System.out
					.println("------------------------------------------------------------");

			// TTUTRIG Chips
			for (HardwareDbChip ttutrig : tb.getTTUTrigs()) {

				String trigname = ttutrig.getName();
				if (tbname.equals("TTU_2") && trigname.equals("TTUTRIG 9"))
					continue;

				for (String register : trigRegisters) {

					DeviceItemWord regValue = (DeviceItemWord) ttutrig
							.getHardwareChip().getItem(register.toString());

					if (register.equals("REC_MUX_DELAY")
							|| register.equals("REC_DATA_DELAY")) {
						for (int i = 0; i < regValue.getDesc().getNumber(); ++i) {
							System.out.println(ttutrig.getFullName() + " "
									+ register + "[" + i + "] "
									+ regValue.read(i));

						}
					} else {
						System.out.println(ttutrig.getFullName() + " "
								+ register + " x"
								+ Long.toHexString(regValue.read()));
					}
				}

			}

			System.out
					.println("------------------------------------------------------------");

			// /////////////////////////////////////////////////////////////////

		}

		/*
		 * quick test: byte[] arg = new Binary("ff0fffdffff0", 48).getBytes();
		 * for( int k=0; k < 6; ++k){ System.out.println("testing: " + k + ":" +
		 * arg[k] + " " + Integer.toHexString(arg[k])); }
		 */

	}

	static void checkTTUOptions(HardwareDbSorterCrate sc)
			throws HardwareDbException, ServiceException, InterruptedException,
			IOException {

		for (HardwareDBTTUBoard tb : sc.getHardwareDBTTUBoards()) {

			String tbname = tb.getName();

			// TTUTRIG Chips
			for (HardwareDbChip ttutrig : tb.getTTUTrigs()) {

				String trigname = ttutrig.getName();
				if (tbname.equals("TTU_2") && trigname.equals("TTUTRIG 9"))
					continue;

				for (String register : sectRegisters) {

					DeviceItemWord regValue = (DeviceItemWord) ttutrig
							.getHardwareChip().getItem(register.toString());

					for (int i = 0; i < regValue.getDesc().getNumber(); ++i) {
						if (i > 11)
							break;
						System.out.println(ttutrig.getFullName() + " "
								+ register + "[" + i + "] " + " x" + 
								Long.toHexString(regValue.read(i)));

					}

				}

				System.out
						.println("------------------------------------------------------------");
			}

			// .........................................................................

		}

	}

	public static void main(String[] args) {

		try {

			HibernateContext context = new SimpleHibernateContextImpl();
			EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);

			HardwareDbMapper dbMapper = new HardwareDbMapper(
					new HardwareRegistry(), context);

			String r_s = "y";
			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));

			HardwareDbSorterCrate sc = (HardwareDbSorterCrate) dbMapper
					.getCrate(equipmentDAO.getCrateByName("SC"));

			addRegisters();

			while (true) {
				checkTTUConfig(sc);
				System.out.println("repeat (y/n)?");
				r_s = br.readLine();
				if (r_s.equals("n")) {
					break;
				}
			}

			// check also mask,force,majority ...
			System.out.println("check mask,force,mayority (y/n)?");
			r_s = br.readLine();

			if (r_s.equals("y")) {
				checkTTUOptions(sc);
			}

		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (HardwareDbException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
