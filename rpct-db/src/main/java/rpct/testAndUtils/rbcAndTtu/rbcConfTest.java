package rpct.testAndUtils.rbcAndTtu;

import java.util.Calendar;
import java.util.List;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.RbcConf;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.RbcBoard;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;

/**
 * Created on 2009-10-20
 * 
 * @author Nikolay Darmenov
 * @version $Id$
 */
public class rbcConfTest {

	public static void main(String[] args) throws DataAccessException {
		final HibernateContext context = new SimpleHibernateContextImpl();
		final EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);

		/* Configuration handling based on PutStripMasksToDB */
		final ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(
				context);
		final ConfigurationManager configurationManager = new ConfigurationManager(
				context, equipmentDAO, configurationDAO);

		final LocalConfigKey inputConfigKey = configurationManager
				.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_RBCS_TTUS_LHC1);	
		final LocalConfigKey outputConfigKey = configurationManager
				.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_RBCS_TTUS_LHC1);	

		final String saveToDbProp = System.getProperty("saveToDb");
		final boolean saveToDb = (saveToDbProp != null) ? Boolean
				.valueOf(saveToDbProp) : false;

		if (saveToDb) {
			System.out.println("[WARNING] Config will be saved to DB!");
		} else {
			System.out
					.println("[WARNING] Config will NOT be saved to DB, simulation only!");
		}

		try {
			final List<Crate> crates = equipmentDAO.getCratesByType(
					CrateType.LINKBOX, true);

			System.out.println("=== Dumping all RbcBoard info ===");
			for (Crate crate : crates) {
				final List<Board> boards = crate.getBoards();
				for (Board board : boards) {
					if (board.getType() == BoardType.RBCBOARD) {
						final RbcBoard rbc = (RbcBoard) board;

						print(rbc, configurationManager, inputConfigKey);

						/*if (saveToDb) {
							RbcConf newRbcConf = new RbcConf();
							int regval = (int) Calendar.getInstance()
									.getTimeInMillis() / 1000;
							newRbcConf.setConfig(regval++);
							newRbcConf.setConfigIn(regval++);
							newRbcConf.setConfigVer(regval++);
							newRbcConf.setMajority(regval++);
							newRbcConf.setShape(regval++);
							newRbcConf.setMask(regval++);
							newRbcConf.setCtrl(regval++);
							System.out.format(
									"   New Configuration: id#%d %s%n",
									newRbcConf.getId(), newRbcConf);

							configurationDAO.saveObject(newRbcConf);
							configurationManager.assignConfiguration(rbc
									.getChip(), newRbcConf, outputConfigKey
									.getName());
							System.out.println("      === Saved to DB ===");
						}*/
					}
				}
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			System.err.println("[ERROR] context to be rollbacked");
			context.rollback();
		} finally {
			context.closeSession();
		}
	}

	/**
	 * @param rbc
	 * @param configurationManager
	 * @param inputConfigKey
	 * @throws DataAccessException
	 * 
	 *             Print structure and configuration of an RbcBoard
	 */
	private static void print(final RbcBoard rbc,
			final ConfigurationManager configurationManager,
			final LocalConfigKey inputConfigKey) throws DataAccessException {
		System.out.println("id #" + rbc.getId() + ": " + rbc);
		System.out.println("   isMaster: " + rbc.isMaster());
		System.out.println("   master: " + rbc.getMaster());
		System.out.println("   i2cChannel: " + rbc.getI2cChannel());
		System.out.println("   slaves attached: " + rbc.getSlaves());
		System.out.println("   slave attached: " + rbc.getSlave());
		System.out.println("   chips: " + rbc.getChips());

		try {
			final StaticConfiguration configuration = configurationManager
					.getConfiguration(rbc.getChip(), inputConfigKey);
			final RbcConf rbcConf = (RbcConf) configuration;
			System.out.format("   Configuration: id#%d %s%n", rbcConf.getId(),
					rbcConf);

		} catch (NullPointerException e) {
			System.out.println("[WARNING] No configuration found for RBC "
					+ rbc.getName());
			System.out
					.println("[WARNING]    Caught \"" + e.getMessage() + "\"");
		}
	}
}
