package rpct.testAndUtils.rbcAndTtu;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import rpct.db.DataAccessException;
import rpct.db.domain.DAO;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.IntArray12;
import rpct.db.domain.configuration.IntArray16;
import rpct.db.domain.configuration.IntArray24;
import rpct.db.domain.configuration.IntArray4;
import rpct.db.domain.configuration.IntArray6;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.configuration.TtuTrigConf;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;

/**
 * Created on 2009-11-18
 * 
 * @author Nikolay Darmenov
 * @version $Id$
 */
public class ttuConfTest {
	private static boolean debug = false;

	public static void main(String[] args) throws DataAccessException {
		final HibernateContext context = new SimpleHibernateContextImpl();
		final EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);

		/* Configuration handling based on PutStripMasksToDB */
		final ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(
				context);
		final ConfigurationManager configurationManager = new ConfigurationManager(
				context, equipmentDAO, configurationDAO);

		final LocalConfigKey inputConfigKey = configurationManager
				.getDefaultLocalConfigKey();
		final LocalConfigKey outputConfigKey = configurationManager
				.getDefaultLocalConfigKey();

		final String debugProp = System.getProperty("debug");
		debug = (debugProp != null) ? Boolean
				.valueOf(debugProp) : false;
		
		final String saveToDbProp = System.getProperty("saveToDb");
		final boolean saveToDb = (saveToDbProp != null) ? Boolean
				.valueOf(saveToDbProp) : false;
				
		if (saveToDb) {
			System.out.println("[WARNING] A dummy config will be saved to DB!");
			if (! proceed("Proceed?")) {
				System.exit(2);
			}
		} else {
			System.out
					.println("[WARNING] Config will NOT be saved to DB, simulation only!");
		}

		try {
			final List<Chip> chips = new ArrayList<Chip>();
			for (ChipType chipType : new ChipType[] {
					ChipType.TTUTRIG,
					ChipType.TTUFINAL })
				chips.addAll(equipmentDAO.getChipsByType(chipType, false));

			System.out.println("=== Dumping all TTU chip info ===");
			for (Chip chip : chips) {
				print(chip, configurationManager, inputConfigKey);

				if (saveToDb) {
					final StaticConfiguration newConf = 
						generateDummyConf(chip, configurationDAO, configurationManager, outputConfigKey);
					
					System.out.format("   New Configuration: id#%d %s%n", newConf.getId(),
							newConf);

					configurationDAO.saveObject(newConf);
					configurationManager.assignConfiguration(chip, newConf, outputConfigKey
							.getName());
					System.out.println("      === To be saved to DB ===");
					System.out.println();
				}
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			System.err.println("[ERROR] context to be rollbacked");
			context.rollback();
		} finally {
			context.closeSession();
		}
	}

	private static StaticConfiguration generateDummyConf(Chip chip,
			DAO configurationDAO, ConfigurationManager configurationManager,
			LocalConfigKey outputConfigKey) throws DataAccessException {

		StaticConfiguration newConf = null;
		
		// Deduce and instantiate newConf		
		try {
			final Map<ChipType, Class<?>> chipConf = new HashMap<ChipType, Class<?>>() {
				private static final long serialVersionUID = 1L;

				{
					put(ChipType.TTUTRIG, Class.forName("rpct.db.domain.configuration.TtuTrigConf"));
					put(ChipType.TTUFINAL, Class.forName("rpct.db.domain.configuration.TtuFinalConf"));
				}
			};

			newConf = (StaticConfiguration) chipConf.get(chip.getType()).newInstance();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		// Set newConf fields with dummy values 
		for (Method method: newConf.getClass().getDeclaredMethods()) {
			printDebug("Got method: " + method);
			if (method.getName().startsWith("set")) {
				printDebug("Got setter: " + method);
				boolean isTransient = false;
				for (Annotation annotation: method.getDeclaredAnnotations()) {
					if (annotation.toString().endsWith(".Transient")) {
						isTransient = true;
						break;
					}
				}
				if (! isTransient) {
					printDebug("The method is not @Transient");
					final Class<?> argClass = method.getParameterTypes()[0];
					printDebug("Arg class: " + argClass + ", simpleName " + argClass.getSimpleName() + ", canonicalName " + argClass.getCanonicalName());

					Object arg = null;
					final String argClassName = argClass.getSimpleName();
					
					String argType = argClassName;
					if (argClassName.equals("byte[]")) {
						argType = "byte[" + TtuTrigConf.REC_MUX_SIZE + "]"; // need to check on method name if byte[].length varies
					}
					arg = DummyGenerator.generate(argType);
					
					printDebug("dummy generated arg: " + arg);

					try {
						method.invoke(newConf, arg);
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
				}
			}
			
		}

		return newConf;
	}

	/**
	 * @param chip
	 * @param configurationManager
	 * @param inputConfigKey
	 * @throws DataAccessException
	 * 
	 *             Print structure and configuration of chip
	 */
	private static void print(final Chip chip,
			final ConfigurationManager configurationManager,
			final LocalConfigKey inputConfigKey) throws DataAccessException {
		System.out.println("===> id #" + chip.getId() + ": " + chip);

		try {
			final StaticConfiguration configuration = configurationManager
					.getConfiguration(chip, inputConfigKey);
			System.out.format("   Configuration: id#%d %s%n", configuration
					.getId(), configuration);

		} catch (NullPointerException e) {
			System.out
					.println("   [WARNING] No configuration found for chipId #"
							+ chip.getId());
		}
	}
	
	/**
	 * Dummy values generation
	 * as "SimpleFactory"
	 */
	public static class DummyGenerator {
		private static int dummyVal = (int) Calendar.getInstance().getTimeInMillis() / 1000;

		private static int integer() { return dummyVal++; }

		private static byte[] bytes(int size) {
			final byte[] result = new byte[size];
			for (int i = 0; i < size; i ++)
				result[i] = (byte) integer();
			
			return result;
		}

		private static int[] integers(int size) {
			final int[] result = new int[size];
			for (int i = 0; i < size; i ++)
				result[i] = integer();
			
			return result;
		}

		// It would be nice IntArrayXX to have a common interface
		// in order to reduce the dummyIntArrayXX to one "IntArray dummyIntArray(String)"
		private static IntArray4 intArray4() {
			return new IntArray4(integers(4));
		}

		private static IntArray6 intArray6() {
			return new IntArray6(integers(6));
		}

		private static IntArray12 intArray12() {
			return new IntArray12(integers(12));
		}

		private static IntArray16 intArray16() {
			return new IntArray16(integers(16));
		}

		private static IntArray24 intArray24() {
			return new IntArray24(integers(24));
		}

		public static Object generate(String type) {
			Object result = null;

			if (type.equals("int"))
				result = DummyGenerator.integer();
			else if (type.startsWith("byte[")) {
				// "byte[dim]" is sent, not null "dim" expected
				final String dimString = type.substring(new String("byte[").length(), type.length() - 1);
				final int dim = Integer.parseInt(dimString);
				result = DummyGenerator.bytes(dim);
			}
			else if (type.equals("IntArray4"))
				result = DummyGenerator.intArray4();
			else if (type.equals("IntArray6"))
				result = DummyGenerator.intArray6();
			else if (type.equals("IntArray12"))
				result = DummyGenerator.intArray12();
			else if (type.equals("IntArray16"))
				result = DummyGenerator.intArray16();
			else if (type.equals("IntArray24"))
				result = DummyGenerator.intArray24();
			else
				throw new IllegalArgumentException("Type '" + type + "' not known to DummyGenerator!");
			
			return result;
		}
	}
	
	private static boolean proceed(String message) {
		Scanner in = new Scanner(System.in);

		System.out.print(message + " (yes/NO): ");
		String answer = in.nextLine();
		
		return answer.toLowerCase().equals("yes");
	}
	
	private static void printDebug(String message) {
		if (debug)
			System.out.println(message);
	}
}
