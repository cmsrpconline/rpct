package rpct.testAndUtils.rbcAndTtu;

import rpct.db.DataAccessException;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.equipment.RbcBoard;
import rpct.db.domain.equipment.RbcTtuConn;
import rpct.db.domain.equipment.SorterCrate;
import rpct.db.domain.equipment.TtuBoard;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;

public class RbcTTULinks {
	static void printOptoLBChamberAssigment(EquipmentDAO equipmentDAO) throws DataAccessException {
		SorterCrate sorterCrate = (SorterCrate)(equipmentDAO.getCratesByType(CrateType.SORTERCRATE, false).get(0));
		System.out.println(sorterCrate.getName());
		for(TtuBoard ttuBoard : sorterCrate.getTTUBoards()) {
			System.out.println(ttuBoard.getName());
			for(int tbInputNum = 0; tbInputNum < 18; tbInputNum++) {
				RbcTtuConn linkConn = ttuBoard.getRbcConn(tbInputNum);
				if(linkConn != null) {
					RbcBoard rbc = (RbcBoard)linkConn.getBoard();
					System.out.println(tbInputNum + " " + rbc.getName());
				}
				else {
					System.out.println(tbInputNum + " ");
				}
			}
			System.out.println();
		}
	}

	public static void main(String[] args) throws DataAccessException {
		HibernateContext context = new SimpleHibernateContextImpl();
		EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
		try {
			printOptoLBChamberAssigment(equipmentDAO);
		}
		catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			context.rollback();
		} 
		/* catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }*/
		finally {
			context.closeSession();
		}

	}

}
