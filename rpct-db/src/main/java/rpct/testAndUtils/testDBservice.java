package rpct.testAndUtils;

import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.equipment.chamberloaction.BarrelOrEndcap;
import rpct.db.service.DbService;
import rpct.db.service.FebAccessInfo;
import rpct.db.service.FebChipInfo;
import rpct.db.service.axis.ConfigurationSetImplAxis;
import rpct.db.service.axis.DbServiceClientImplAxis;
import rpct.xdaq.axis.bag.Bag;

public class testDBservice {

	/**
	 * @param args
	 * @throws MalformedURLException 
	 * @throws ServiceException 
	 * @throws RemoteException 
	 */
	public static void main(String[] args) throws MalformedURLException, RemoteException, ServiceException {      
			DbService dbService = new DbServiceClientImplAxis(
					new URL("http://localhost:8080/rpct-dbservice/services/urn:rpct-dbservice"));
			
			/*FebChipInfo[] febChipInfos = dbService.getFebChipInfoVector();
			for(FebChipInfo febChipInfo : febChipInfos) {
				System.out.println(febChipInfo);
			}*/
			
			BigInteger[] chipIds =  new BigInteger[] {
					BigInteger.valueOf(79818),
					BigInteger.valueOf(79819),
					BigInteger.valueOf(79817),
					BigInteger.valueOf(79816),
					BigInteger.valueOf(79815),
					BigInteger.valueOf(79814),
					BigInteger.valueOf(79813),
					BigInteger.valueOf(79812),
					BigInteger.valueOf(79811),
					BigInteger.valueOf(79810),
					BigInteger.valueOf(79809),
					BigInteger.valueOf(79808),
					BigInteger.valueOf(79807),
					BigInteger.valueOf(79806),
					BigInteger.valueOf(79805),
					BigInteger.valueOf(79804),
					BigInteger.valueOf(79803),
					BigInteger.valueOf(79802),
					BigInteger.valueOf(79801),
					BigInteger.valueOf(79800),
					BigInteger.valueOf(79799),
					BigInteger.valueOf(79798),
					BigInteger.valueOf(79797)
			};
			String localConfigKey = ConfigurationManager.LOCAL_CONF_KEY_FEBS_DEFAULT;
			ConfigurationSetImplAxis configurationSetImplAxis = dbService.getConfigurationSetForLocalConfigKey(chipIds, localConfigKey);
			//System.out.println("configurationSetImplAxis.getFebChipControlLines().length " + configurationSetImplAxis.getFebChipControlLines().length);
			System.out.println("configurationSetImplAxis.getFebChipConfigurations().length " + configurationSetImplAxis.getFebChipConfigurations().length);
			int a = 0;
	}

}
