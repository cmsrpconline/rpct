package rpct.testAndUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.management.RuntimeErrorException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import rpct.db.DataAccessException;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.chamberloaction.BarrelOrEndcap;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.testAndUtils.SplitersXlsxGenerator.LongFiber;


public class OmtfRpcConnectionGen {			
	static void writeConnectionsToXml(String xlsFileName, Map<String, LinkBoard> linkBoardMap) throws IOException {
		File myFile = new File(xlsFileName);
		FileInputStream fis = new FileInputStream(myFile);

		XSSFWorkbook myWorkBook = new XSSFWorkbook (fis);
		XSSFSheet inputSheet = myWorkBook.getSheetAt(1);
		XSSFSheet outputSheet = myWorkBook.getSheetAt(2);

		FileOutputStream file = new FileOutputStream("/nfshome0/kbunkow/rpc_links/omtfRpcConnections.txt", false);
    	PrintStream prntSrm = new PrintStream(file);
		
		int iOutRow = 1;
		for(int iRow = 1; iRow < 290; iRow++) {       		
			Row inRow = inputSheet.getRow(iRow);
			if(inRow ==  null)
				break;

			String linkName = inRow.getCell(CellReference.convertColStringToIndex("A")).getStringCellValue() + "_CH0";

			linkName =  linkName.replace("S0", "S");
			LinkBoard masterLinkBoard = linkBoardMap.get(linkName);
			if(masterLinkBoard == null) {
				myWorkBook.close();
				throw new RuntimeException("no LB with name " + linkName + " found in the DB)");
			}
			
			String omtfName = inRow.getCell(CellReference.convertColStringToIndex("H")).getStringCellValue();
			int fiberChannel = (int) inRow.getCell(CellReference.convertColStringToIndex("I")).getNumericCellValue();
			int firmwareChannel = (int) inRow.getCell(CellReference.convertColStringToIndex("J")).getNumericCellValue();
		
			int[] iLBs = {1, 0, 2};
			
			for(int iLB : iLBs) {
				LinkBoard linkBoard = masterLinkBoard.getSlave(iLB);
				if(linkBoard != null) {									
					int layer = 0;
					int numInLayer = 0;
					int omtfNum = Integer.parseInt(omtfName.substring(6));
					if(linkBoard.getChamberLocations().get(0).getBarrelOrEndcap() == BarrelOrEndcap.Barrel) {
						layer = linkBoard.getChamberLocations().get(0).getLayer();
						if(layer == 6)
							continue;
						
						if( (layer == 4 || layer == 5) && linkBoard.getChamberName().contains("Backward") ) //RB2out & RB3
							continue;						
						
						layer = linkBoard.getChamberLocations().get(0).getLayer() + 9; //OMTF convention
						
						int shift = 2*omtfNum;
						if(omtfNum ==  6 && linkBoard.getChamberLocations().get(0).getSector() <= 3)
							shift = 0;
						
						if(layer == 14) //RB3
							numInLayer = 2 * (linkBoard.getChamberLocations().get(0).getSector() - shift ); 
						else if(linkBoard.getChamberName().contains("Backward") || linkBoard.getChamberName().contains("Central")) {
							numInLayer = 4 * (linkBoard.getChamberLocations().get(0).getSector() - shift );
						}
						else
							numInLayer = 4 * (linkBoard.getChamberLocations().get(0).getSector() - shift ) + 2;
						
					}
					else { //ENDCAP
						layer = Math.abs(linkBoard.getChamberLocations().get(0).getDiskOrWheel()) + 14;
						numInLayer = 2* (linkBoard.getChamberLocations().get(0).getSector() - 6*omtfNum +3);
						
						if(omtfNum ==  6 && linkBoard.getChamberLocations().get(0).getSector() <= 3)
							numInLayer = 2* (linkBoard.getChamberLocations().get(0).getSector()  +3);	
						
						if(numInLayer < 0 || numInLayer > 13) {
							continue;
						}
						
					}
					if(numInLayer < 0 || numInLayer > 13) {
						myWorkBook.close();
						throw new RuntimeException("numInLayer < 0 || numInLayer > 13");
					}
					
					Row outRow = outputSheet.createRow(iOutRow);
					outRow.createCell(CellReference.convertColStringToIndex("A")).setCellValue(linkBoard.getName());
					outRow.createCell(CellReference.convertColStringToIndex("B")).setCellValue(linkBoard.getChamberName());
					outRow.createCell(CellReference.convertColStringToIndex("C")).setCellValue(omtfName);
					outRow.createCell(CellReference.convertColStringToIndex("D")).setCellValue(firmwareChannel);
					
					outRow.createCell(CellReference.convertColStringToIndex("E")).setCellValue(layer);
					outRow.createCell(CellReference.convertColStringToIndex("F")).setCellValue(numInLayer);
					outRow.createCell(CellReference.convertColStringToIndex("G")).setCellValue(numInLayer + 1);
					
					if(omtfName.equals("OMTF_n1")) {
						prntSrm.println("(" + firmwareChannel + ", " + linkBoard.getChannelNum() 
							+ ", " + layer + ", " + numInLayer + ", " + (numInLayer + 1) + "), --" + linkBoard.getName() 
							+ " " +linkBoard.getChamberName());
					}
					iOutRow++;
				}
			}
			
		}

		myWorkBook.setForceFormulaRecalculation(true);
		File splitersFile = new File(xlsFileName.replace("v1", "v2"));
		FileOutputStream os = new FileOutputStream(splitersFile);
		myWorkBook.write(os);

		os.close();
		myWorkBook.close();

		System.out.println("Writing on XLSX file " + splitersFile.getName() + " Finished ...");
		fis.close();

	}


	public static void main(String[] args) throws DataAccessException {
		HibernateContext context = new SimpleHibernateContextImpl();
		try {           
			EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
			Map<String, LinkBoard> linkBoardMap = new TreeMap<String, LinkBoard>(); 
			for(Board board : equipmentDAO.getBoardsByType(BoardType.LINKBOARD, false) ) {
				LinkBoard linkBoard = (LinkBoard)board;
				if(linkBoard.isMaster())
					linkBoardMap.put(linkBoard.getName(), linkBoard);
			}

			writeConnectionsToXml("/nfshome0/kbunkow/rpc_links/OMTF_connections_v1a.xlsx", linkBoardMap);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			context.rollback();
		}
	} 
}
