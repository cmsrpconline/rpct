package rpct.testAndUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ChipConfAssignment;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.configuration.SynCoderConf;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.FebConnector;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.equipment.chamberloaction.BarrelOrEndcap;
import rpct.db.domain.equipment.chamberloaction.ChamberLocation;
import rpct.db.domain.equipment.chamberstrip.ChamberStrip;
import rpct.db.domain.equipment.feblocation.FebLocation;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.axis.Binary;

public class SplitersXlsxGenerator {
    static void printOptoLBChamberAssigment(EquipmentDAO equipmentDAO) throws DataAccessException, FileNotFoundException {
        FileOutputStream file = new FileOutputStream(System.getenv("HOME") + "/bin/out/optLinksMap" +  ".txt", false);
        PrintStream prntSrm = new PrintStream(file);

        for(Crate crate : equipmentDAO.getCratesByType(CrateType.TRIGGERCRATE, false) ) {            
            TriggerCrate triggerCrate = (TriggerCrate)crate;
            prntSrm.println(triggerCrate.getName());
            for(TriggerBoard triggerBoard : triggerCrate.getTriggerBoards()) {
                prntSrm.println(triggerBoard.getName());
                for(int tbInputNum = 0; tbInputNum < 18; tbInputNum++) {
                    LinkConn linkConn = triggerBoard.getLinkConn(tbInputNum);
                    if(linkConn != null) {
                        LinkBoard mlb = (LinkBoard)linkConn.getBoard();
                        String otherLiks = ""; 
                        for(LinkConn linkConn2 : mlb.getLinkConns() ) {
                        	otherLiks += linkConn2.getCollectorBoard().getName() + " " + linkConn2.getCollectorBoardInputNum() + ", ";
                        }
                        for(int iCh = 0; iCh < 3; iCh++) {
                            LinkBoard lb = mlb.getSlave(iCh);
                            prntSrm.print(tbInputNum + " " + iCh);
                            if(lb != null) {
                                prntSrm.print("   " + lb.getName() + "    ");
                                for(String chmName : lb.getChamberNames())
                                    prntSrm.print(chmName + ";  ");
                            }
                            if(iCh == 0) {
                            	prntSrm.print("  " + otherLiks);
                            }
                            prntSrm.print("\n");
                        }
                    }
                    else {
                        for(int iCh = 0; iCh < 3; iCh++) {
                            prntSrm.println(tbInputNum + " " + iCh);
                        }
                    }
                }
                prntSrm.println();
            }
            
        }
    }
    
    static void printSelectedOptLinks(EquipmentDAO equipmentDAO) throws DataAccessException, FileNotFoundException {                                 
        //FileOutputStream file = new FileOutputStream(System.getenv("HOME") + "/bin/selectedOptLinks.txt", false);
        //PrintStream prntSrm = new PrintStream(file);
        PrintStream prntSrm = System.out;
        Map<String, LinkBox> lBoxMap = new TreeMap<String, LinkBox>(); 
        for(Crate crate : equipmentDAO.getCratesByType(CrateType.LINKBOX, false) ) {
            LinkBox linkBox = (LinkBox)crate;
            lBoxMap.put(linkBox.getName(), linkBox);
        }
        
        for(Map.Entry<String, LinkBox> eLinBox : lBoxMap.entrySet()) {
            LinkBox linkBox = (LinkBox)eLinBox.getValue();
            //prntSrm.println(linkBox.getName());
            if(linkBox.getSector() == 10 ||  //linkBox.getSector() == 10 || linkBox.getSector() == 11 ||
            		linkBox.getName().contains("S8/9")) {
            	Map<Integer, Board> boardsMap = new TreeMap<Integer, Board>();
            	for(Board board : linkBox.getBoards()) {
            		if(board.getType() == BoardType.LINKBOARD && board.getName().contains("S10"))
            			boardsMap.put(board.getPosition(), board);
            	}

            	for(Map.Entry<Integer, Board> e : boardsMap.entrySet()) {
            		LinkBoard lb = (LinkBoard)e.getValue();
            		if(lb.isMaster() == false)
            			continue;
            	
            		ChamberLocation chamberLocation = lb.getChamberLocations().get(0);
    				if(chamberLocation != null) {
    					//if( chamberLocation.getDiskOrWheel()< 0) 
    					{
    						prntSrm.print(lb.getName() + "\t" +chamberLocation.getBarrelOrEndcap() + "\t" +chamberLocation.getDiskOrWheel() + "\t");
    					}
    				}
    				

    				for(int i = 0; i < 3; i++) {
    					LinkBoard slave = lb.getSlave(i);
    					if(slave != null) {
    						prntSrm.print(slave.getChamberNames().first().replaceFirst(" ", "") + "\t");
    					}  
    					else {
    						prntSrm.print("\t");
        				}
    				}      
            	            		
            		for(LinkConn linkConn : lb.getLinkConns()) {
    					prntSrm.print(linkConn.getCollectorBoard().getName() + " " + linkConn.getCollectorBoardInputNum() + "\t");
    				}
    				prntSrm.print("\n");
            	}
            }
        }
    }
    
    static class LongFiber {
    	public String linkName;
    	public String cableColor;
    	public String fiberColor;
    	public String tc;
    	
    	public String toString() {
    		return linkName + " -> " + tc + " " + cableColor + " " + fiberColor + " isBarrel " + isBarrel() + " DiscOrWheel " + getDiscOrWheel();
    	}
    	
    	int isBarrel() {
    		if(linkName.contains("RB") ==  true)
    			return 1;
    		else if(linkName.contains("RE") ==  true)
    			return 0;
    		throw new RuntimeException("link " + linkName + " not caontains RB nor RE");
    	}
    	
    	int getDiscOrWheel() {
    		return Integer.valueOf(linkName.substring(5, linkName.indexOf('_', 5)));
    	}
    	
    	/*
    	 * defoult assigment
    	 */
    	
		/*int getSplitterBoardNum() {
			if(linkName.contains("RE-3"))
				return 1;
			else if(linkName.contains("RE-4"))
				return 1;
			else if(linkName.contains("RE-1"))
				return 2;
			else if(linkName.contains("RE+1"))
				return 4;
			else if(linkName.contains("RE+3"))
				return 5;
			else if(linkName.contains("RE+4"))
				return 5;
			else if(linkName.contains("RB-2"))
				return 1;
			else if(linkName.contains("RB-1"))
				return 2;
			else if(linkName.contains("RB0"))
				return 3;
			else if(linkName.contains("RB+1"))
				return 4;
			else if(linkName.contains("RB+2"))
				return 5;
			
			return 0;
		}*/
		
		int getSplitterBoardNum() {
			if(linkName.contains("RE-3"))
				return 2;
			else if(linkName.contains("RE-4"))
				return 2;
			else if(linkName.contains("RE-1"))
				return 1;
			else if(linkName.contains("RE+1"))
				return 5;
			else if(linkName.contains("RE+3"))
				return 4;
			else if(linkName.contains("RE+4"))
				return 4;
			
			else if(linkName.contains("RB-2"))
				return 2;
			else if(linkName.contains("RB-1"))
				return 1;
			else if(linkName.contains("RB0"))
				return 3;
			else if(linkName.contains("RB+1"))
				return 5;
			else if(linkName.contains("RB+2"))
				return 4;
			
			return 0;
		}
    }
    

    
    static void readColumn(int colNum, Map<String, LongFiber> longFiberMap, Iterator<Row> rowIterator) {
    	String cableColor = "";
    	while (rowIterator.hasNext()) {
    		Row row = rowIterator.next();  
    		Cell cell =  row.getCell(colNum);
    		if(cell != null) {
    			String lbName = cell.getStringCellValue();
    			
    			if(lbName.contains("LB_") ) {
    				lbName = lbName.substring(0, lbName.lastIndexOf("_"));
    				LongFiber longFiber = new LongFiber();
    				longFiber.linkName = lbName;
    				if(row.getCell(colNum -2).getStringCellValue().equals("") == false) {
    					cableColor = row.getCell(colNum -2).getStringCellValue();
    				}
    				longFiber.cableColor = cableColor;
    				longFiber.fiberColor = row.getCell(colNum -1).getStringCellValue();
    				longFiber.tc = row.getCell(colNum +1).getStringCellValue();
    				longFiberMap.put(lbName, longFiber);
    				
    				System.out.println(longFiber);
    			}
    		}

    	}
    };
	
	static Map<String, LongFiber> readLongFiberMap(String fileName) throws IOException {
    	Map<String, LongFiber> longFiberMap = new TreeMap<String, LongFiber>();
    	File myFile = new File(fileName);
    	FileInputStream fis = new FileInputStream(myFile);

    	// Finds the workbook instance for XLSX file
    	XSSFWorkbook myWorkBook = new XSSFWorkbook (fis);

    	// Return first sheet from the XLSX workbook
    	XSSFSheet mySheet = myWorkBook.getSheetAt(0);

    	// Get iterator to all the rows in current sheet
    	Iterator<Row> rowIterator = mySheet.iterator();
    	
    	int colNum = CellReference.convertColStringToIndex("C");
    	readColumn(colNum, longFiberMap, rowIterator);
    	
    	
    	colNum = CellReference.convertColStringToIndex("I");
    	rowIterator = mySheet.iterator();
    	readColumn(colNum, longFiberMap, rowIterator);
    	fis.close();
    	myWorkBook.close();
    	return longFiberMap;
    }
	
	static Set<String> sectorsForTM = new TreeSet<String>();
	static boolean linkForTM(String lbName) {
		for(String sectorName : sectorsForTM) {
			if(lbName.contains(sectorName))
				return true;
		}
		return false;
	}

	
	static Set<String> linksForOMTF = new TreeSet<String>();
	static boolean linkForOMTF(String lbName) {
		for(String sectorName : linksForOMTF) {
			if(lbName.contains(sectorName))
				return true;
		}
		return false;
	}

	/**
	 * 
	 * @param templateFileName
	 * @param tcName
	 * @param longFibers - list of long fibers that goes to given TC
	 * @throws IOException 
	 */
	static void writeSplitersXml(String templateFileName, String tcName, ArrayList<LongFiber> longFibers, Map<String, LinkBoard> linkBoardMap ) throws IOException {
		File myFile = new File(templateFileName);
    	FileInputStream fis = new FileInputStream(myFile);

    	XSSFWorkbook myWorkBook = new XSSFWorkbook (fis);
    	XSSFSheet mySheet = myWorkBook.getSheetAt(0);	
    	
    	
    	/*CellStyle cableColorStyle = myWorkBook.createCellStyle();;*/ //bug in POI in copying the fill style????
    	Map<String, CellStyle> fiberColorStyleMap = new HashMap<String, CellStyle>();
    	int colorRow = 54; 
    	fiberColorStyleMap.put("orange", mySheet.getRow(colorRow++).getCell(CellReference.convertColStringToIndex("A")).getCellStyle());
    	fiberColorStyleMap.put("green", mySheet.getRow(colorRow++).getCell(CellReference.convertColStringToIndex("A")).getCellStyle());
    	fiberColorStyleMap.put("blue", mySheet.getRow(colorRow++).getCell(CellReference.convertColStringToIndex("A")).getCellStyle());
    	fiberColorStyleMap.put("white", mySheet.getRow(colorRow++).getCell(CellReference.convertColStringToIndex("A")).getCellStyle());
    	fiberColorStyleMap.put("brown", mySheet.getRow(colorRow++).getCell(CellReference.convertColStringToIndex("A")).getCellStyle());
    	fiberColorStyleMap.put("grey", mySheet.getRow(colorRow++).getCell(CellReference.convertColStringToIndex("A")).getCellStyle());
    	
    	CellStyle omtfStyle = mySheet.getRow(54).getCell(CellReference.convertColStringToIndex("B")).getCellStyle();
    	CellStyle twinMuxStyle = mySheet.getRow(55).getCell(CellReference.convertColStringToIndex("B")).getCellStyle();
    	CellStyle cppfStyle = mySheet.getRow(56).getCell(CellReference.convertColStringToIndex("B")).getCellStyle();
    	//omtfStyle.setFillForegroundColor(IndexedColors.AQUA.getIndex());
    	
    	Map<String, Integer> omtfCnt = new TreeMap<String, Integer>();
    	
    	int rowNum = 1;
    	int splitterBoardNum = 0;
    	int splitterInputNum = 0;
    	DecimalFormat decimalFormat = new DecimalFormat("00");
    	for(LongFiber longFiber : longFibers) {
    		LinkBoard lb = linkBoardMap.get(longFiber.linkName + "_CH0");
    		if(lb == null)
    			System.out.println("no lb with the name " +  longFiber.linkName);
    		
    		Row row = mySheet.getRow(rowNum);
    		/////////////////
    		Cell cell = row.getCell(CellReference.convertColStringToIndex("A"));    		
    		cell.setCellValue(longFiber.getSplitterBoardNum());    		
    		/////////////////
    		if(splitterBoardNum != longFiber.getSplitterBoardNum()) {
    			splitterInputNum = 0;
    			splitterBoardNum = longFiber.getSplitterBoardNum();
    		}
    		cell = row.getCell(CellReference.convertColStringToIndex("B"));    		
    		cell.setCellValue(splitterInputNum++);    		
    		/////////////////
    		cell = row.getCell(CellReference.convertColStringToIndex("D"));
    		cell.setCellValue(longFiber.cableColor);
    		/*if(rowNum == 1) {
    			cableColorStyle.cloneStyleFrom(cell.getCellStyle());
    			cableColorStyle.setFillForegroundColor(IndexedColors.valueOf(longFiber.cableColor.toUpperCase()).getIndex());
    			//cableColorStyle.setFillBackgroundColor(IndexedColors.valueOf(longFiber.cableColor.toUpperCase()).getIndex());
    		}
    		cell.setCellStyle(cableColorStyle);*/
    		/////////////////
    		cell = row.getCell(CellReference.convertColStringToIndex("E"));
    		cell.setCellValue(longFiber.fiberColor);
    		/*if(fiberColorStyleMap.get(longFiber.fiberColor) == null) {
    			CellStyle fiberColorStyle = myWorkBook.createCellStyle();
    			fiberColorStyle.cloneStyleFrom(cell.getCellStyle());
    			fiberColorStyle.setFillForegroundColor(IndexedColors.valueOf(longFiber.fiberColor.toUpperCase()).getIndex());
    			//fiberColorStyle.setFillBackgroundColor(IndexedColors.valueOf(longFiber.fiberColor.toUpperCase()).getIndex());
    			fiberColorStyleMap.put(longFiber.fiberColor, fiberColorStyle);
    		}*/
    		cell.setCellStyle(fiberColorStyleMap.get(longFiber.fiberColor));
    		/////////////////
    		cell = row.getCell(CellReference.convertColStringToIndex("F"));
    		cell.setCellValue(longFiber.linkName);
    		/////////////////
    		cell = row.getCell(CellReference.convertColStringToIndex("H"));    		
    		if(linkForTM(longFiber.linkName) ) {
    			cell.setCellValue("PS_50%");
    		}
    		else if( longFiber.linkName.contains("RE")) {
    			cell.setCellValue("PS_60%");
    		}
    		
    		/////////////////  
    		
    		cell = row.getCell(CellReference.convertColStringToIndex("I"));    		
    		if(linkForTM(longFiber.linkName)) {
    			cell.setCellValue("TwinMux");
    			cell.setCellStyle(twinMuxStyle);
    		}
    		else if(longFiber.linkName.contains("RE")) {
    			cell.setCellValue("CPPF");
    			//cell.setCellStyle(cppfStyle);
    		}
    		/////////////////
    		String links = "";       
    		int asCellIndex = CellReference.convertColStringToIndex("N");
    		for(LinkConn linkConn : lb.getLinkConns()) {
    			String tbIn = linkConn.getTriggerBoard().getName() + " " + linkConn.getTriggerBoardInputNum();
    			cell = row.getCell(asCellIndex);
    			if(cell == null){
    				throw new RuntimeException("no cell for asCellIndex " + asCellIndex);
    			}
        		cell.setCellValue(tbIn);        		
        		links += tbIn  + " ";
        		asCellIndex++;
        		//linkConn.getTriggerBoard().getTriggerCrate().getLogSector();
        		String tbFiberLabel = "TC" + decimalFormat.format(linkConn.getTriggerBoard().getTriggerCrate().getLogSector()) + "/" + 
        				linkConn.getTriggerBoard().getPosition() + 
        				"/" + decimalFormat.format(linkConn.getTriggerBoardInputNum());
        		cell = row.getCell(asCellIndex);
        		cell.setCellValue(tbFiberLabel);
        		asCellIndex+=2;
    		}
    		/////////////////
    		if(linkForOMTF(longFiber.linkName)) {    			
    			int omtfSplitting = 0;
    			int omtfNum = 0;
    			if(longFiber.linkName.contains("23")) {
    				if(lb.getSector()%2 == 0) 
    					omtfSplitting = 1;
    				else 
    					omtfSplitting = 2;
    				omtfNum = (lb.getSector())/2;
    			}
    			else {
    				if(lb.getSector()%2 == 0) 
    					omtfSplitting = 2;
    				else 
    					omtfSplitting = 1;
    				omtfNum = (lb.getSector()-1)/2;
    			}
    			if(omtfNum == 0 || omtfNum == -1)
    				omtfNum = 6;
    			
    			for(int i = 0; i < omtfSplitting; i++) {    			
    				cell = row.getCell(asCellIndex);
    				omtfNum = omtfNum+i;
    				if(omtfNum == 7)
    					omtfNum = 1;
    				
    				String omtfName = "";
    				if(longFiber.linkName.contains("+"))
    					omtfName = "OMTF_p" + omtfNum;
    				else if(longFiber.linkName.contains("-"))
    					omtfName = "OMTF_n" + omtfNum;
    				else 
    					throw new RuntimeException("link name " + longFiber.linkName + " not contains + nor -");
    				
    				cell.setCellValue(omtfName);
    				Integer cnt = omtfCnt.get(omtfName);
    				if(cnt == null) {
    					cnt = new Integer(0);
    				}
    				omtfCnt.put(omtfName, new Integer(cnt+1));
    				
    				
    				cell.setCellStyle(omtfStyle);
    				asCellIndex+=3;
    				links += " OMTF" + i + " ";
    			}
    		}
    		
    		System.out.println( (rowNum++) + " " + longFiber + " " + links);    		
    	}
    	
    	rowNum = 54;
    	for(Map.Entry<String, Integer> entry : omtfCnt.entrySet() ) {
    		Row row = mySheet.getRow(rowNum++);
    		Cell cell = row.getCell(CellReference.convertColStringToIndex("F"));  
    		cell.setCellValue(entry.getKey());
    		cell = row.getCell(CellReference.convertColStringToIndex("G"));  
    		cell.setCellValue(entry.getValue());
    	}
    	 
    	myWorkBook.setForceFormulaRecalculation(true);
    	File splitersFile = new File(templateFileName.replace("TC_template", tcName));
    	FileOutputStream os = new FileOutputStream(splitersFile);
        myWorkBook.write(os);
        
        os.close();
        myWorkBook.close();
        fis.close();
        System.out.println("Writing on XLSX file Finished ...");
	}
	
/*	static interface ISplitterBoardsAssigement {
		int getSplitterBoardNum(LongFiber longFiber);
	}
	
	static class SplitterBoardsAssigement_S6_7_8 {
		int getSplitterBoardNum(LongFiber longFiber) {
			if(longFiber.linkName.contains("RE-3"))
				return 1;
			else if(longFiber.linkName.contains("RE-4"))
				return 1;
			else if(longFiber.linkName.contains("RE-1"))
				return 2;
			else if(longFiber.linkName.contains("RE+1"))
				return 4;
			else if(longFiber.linkName.contains("RE+3"))
				return 5;
			else if(longFiber.linkName.contains("RE+4"))
				return 5;
			else if(longFiber.linkName.contains("RB-2"))
				return 1;
			else if(longFiber.linkName.contains("RB-1"))
				return 2;
			else if(longFiber.linkName.contains("RB0"))
				return 3;
			else if(longFiber.linkName.contains("RB+1"))
				return 4;
			else if(longFiber.linkName.contains("RB+2"))
				return 5;
			
			return 0;
		}
	}*/
    public static void main(String[] args) throws DataAccessException {
    	sectorsForTM.add("RB+2_S6");
    	sectorsForTM.add("RB+2_S7");
    	sectorsForTM.add("RB+2_S8");
    	
    	sectorsForTM.add("RB-1_S9");
    	sectorsForTM.add("RB-1_S10");
    	sectorsForTM.add("RB-1_S11");
    	
    	sectorsForTM.add("RB-2_S9");
    	sectorsForTM.add("RB-2_S10");
    	sectorsForTM.add("RB-2_S11");
    	
    	linksForOMTF.add("BN2A");
    	linksForOMTF.add("BN2B");
    	linksForOMTF.add("BN2C");
    	linksForOMTF.add("BN2E");
    	
    	linksForOMTF.add("EN12");
    	linksForOMTF.add("EN13");
    	linksForOMTF.add("EN23");
    	linksForOMTF.add("EN33");
    	linksForOMTF.add("EN43");
    	
    	linksForOMTF.add("BP2A");
    	linksForOMTF.add("BP2B");
    	linksForOMTF.add("BP2C");
    	linksForOMTF.add("BP2E");
    	                   
    	linksForOMTF.add("EP12");
    	linksForOMTF.add("EP13");
    	linksForOMTF.add("EP23");
    	linksForOMTF.add("EP33");
    	linksForOMTF.add("EP43");
    	

        //HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
         try {
/*        	 File myFile = new File("/nfshome0/kbunkow/rpc_links/TC_detector_fibres.xlsx");
             FileInputStream fis = new FileInputStream(myFile);*/

             Map<String, LongFiber> longFiberMapByLB = readLongFiberMap("/nfshome0/kbunkow/rpc_links/TC_detector_fibres_15042015.xlsx");
             Map<String, ArrayList<LongFiber> > longFiberMapByTc = new TreeMap<String, ArrayList<LongFiber> >();
             for(Map.Entry<String, LongFiber> entry : longFiberMapByLB.entrySet() ) {
            	 System.out.println(entry.getValue());
            	 String tc = entry.getValue().tc;
            	 ArrayList<LongFiber> tcSet = longFiberMapByTc.get(tc);
            	 if(tcSet == null) {
            		 tcSet = new ArrayList<LongFiber>();
            		 longFiberMapByTc.put(tc, tcSet);
            	 }
            	 tcSet.add(entry.getValue());
             }
             
             Comparator<LongFiber> longFiberComparator = new Comparator<LongFiber>() {
                 public int compare(LongFiber longFiber1, LongFiber longFiber2) {
                     if(longFiber1.isBarrel() == longFiber2.isBarrel()) {
                    	 if(longFiber1.getDiscOrWheel() == longFiber2.getDiscOrWheel()) {
                    		 return longFiber1.linkName.compareTo(longFiber2.linkName);
                    	 }
                    	 else 
                    		 return (longFiber1.getDiscOrWheel() - longFiber2.getDiscOrWheel());
                     }
                     else 
                    	 return (longFiber1.isBarrel() - longFiber2.isBarrel());
                 }
             };
             
             Comparator<LongFiber> longFiberComparator2 = new Comparator<LongFiber>() {
            	 public int compare(LongFiber longFiber1, LongFiber longFiber2) {
            		 if(longFiber1.getSplitterBoardNum() == longFiber2.getSplitterBoardNum()) {
            			 if(longFiber1.isBarrel() == longFiber2.isBarrel()) {
                        	 return longFiber1.linkName.compareTo(longFiber2.linkName);
                         }
                         else 
                        	 return (longFiber1.isBarrel() - longFiber2.isBarrel());
            		 }
            		 else 
            			 return (longFiber1.getSplitterBoardNum() - longFiber2.getSplitterBoardNum());
            	 }
             };
             
             System.out.println("by TCs");
             for(Map.Entry<String, ArrayList<LongFiber>> entry : longFiberMapByTc.entrySet() ) {
            	 Collections.sort(entry.getValue(), longFiberComparator2);
            	 int num = 0;
            	 for(LongFiber longFiber : entry.getValue() ) {
            		 System.out.println( (num++) + " " + longFiber);
            	 }
             }
                                                  

             HibernateContext context = new SimpleHibernateContextImpl();
             EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
             Map<String, LinkBoard> linkBoardMap = new TreeMap<String, LinkBoard>(); 
             for(Board board : equipmentDAO.getBoardsByType(BoardType.LINKBOARD, false) ) {
            	 LinkBoard linkBoard = (LinkBoard)board;
            	 if(linkBoard.isMaster())
            		 linkBoardMap.put(linkBoard.getName(), linkBoard);
             }

             for(Map.Entry<String, ArrayList<LongFiber>> entry : longFiberMapByTc.entrySet() ) {
            	 int num = 1;
            	 System.out.println( entry.getKey() );
            	 Set<String> lboxes = new TreeSet<String>();
            	 for(LongFiber longFiber : entry.getValue() ) {
            		 LinkBoard lb = linkBoardMap.get(longFiber.linkName + "_CH0");
            		 if(lb == null)
            			 System.out.println("no lb with the name " +  longFiber.linkName);
            		 String links = "";            		 
            		 for(LinkConn linkConn : lb.getLinkConns())
            			 links += linkConn.getTriggerBoard().getName() + " " + linkConn.getTriggerBoardInputNum() + " ";
            		 //System.out.println( (num++) + " " + longFiber + " " + links);
            		 lboxes.add(lb.getLinkBox().getName());
            	 }
            	 for(String lbox : lboxes)
            		 System.out.println( lbox );
             }
             
             String tcName = "TC_0";
             writeSplitersXml("/nfshome0/kbunkow/rpc_links/Splitters_TC_template_v2.xlsx", tcName, longFiberMapByTc.get(tcName), linkBoardMap);
        	
        } 
        /*catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            context.rollback();
        }*/ catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}         
        finally {
            //context.closeSession(); //TODO uncomment!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
        }
    }

}
