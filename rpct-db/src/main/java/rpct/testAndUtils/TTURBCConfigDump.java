package rpct.testAndUtils;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.TreeSet;

import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ChipConfAssignment;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.configuration.SynCoderConf;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.RbcBoard;
import rpct.db.domain.equipment.TtuBoard;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;

public class TTURBCConfigDump {
	
	//..1 TTU backplane:
	
	//..2
    static void dumpTTUConfigFromDB(LocalConfigKey configKey, EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) throws DataAccessException, IOException {                
        List<Board> ttus = equipmentDAO.getBoardsByType(BoardType.TTUBOARD, true);
       
        for(Board board : ttus) {
            TtuBoard ttu = (TtuBoard)board;
            for(Chip ttuTrig : ttu.getTTUTrigs()) {
            	System.out.println(board.getName() + " "+ ttuTrig.getType()  + " pos " + ttuTrig.getPosition());
            	StaticConfiguration configuration = configurationManager.getConfiguration(ttuTrig, configKey);
            	System.out.println(configuration);
            }
        }
    }
   
    //..3
    static void dumpRbcConfigFromDB(LocalConfigKey configKey, EquipmentDAO equipmentDAO, ConfigurationManager configurationManager, ConfigurationDAO configurationDAO) throws DataAccessException, IOException {                
        List<Board> rbcs = equipmentDAO.getBoardsByType(BoardType.RBCBOARD, true);
       
        for(Board board : rbcs) {
            RbcBoard rbc = (RbcBoard)board;
            Chip rbcCHip = rbc.getChip();
            System.out.println(board.getName() + " "+ rbcCHip.getType()  + " pos " + rbcCHip.getPosition());
            StaticConfiguration configuration = configurationManager.getConfiguration(rbcCHip, configKey);
            System.out.println(configuration);

        }
    }
    
    public static void main(String[] args) throws DataAccessException {
        HibernateContext context = new SimpleHibernateContextImpl();
        EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
        HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);
        ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
        ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO); 
        
        try {
            //dumpTTUConfigFromDB(configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_LHC3_3BX),
        	dumpTTUConfigFromDB(configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_RBCS_TTUS_LHC1),
            		equipmentDAO, configurationManager, configurationDAO);     
            
            //dumpRbcConfigFromDB(configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_LHC3_3BX),
        	dumpRbcConfigFromDB(configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_RBCS_TTUS_LHC1),
                	equipmentDAO, configurationManager, configurationDAO);
        }
        catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            context.closeSession();
        }
    }
}
