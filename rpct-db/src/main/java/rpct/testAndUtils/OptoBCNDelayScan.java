package rpct.testAndUtils;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbBoard;
import rpct.xdaqaccess.hardwaredb.HardwareDbChip;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;

public class OptoBCNDelayScan {
	
	public static void main(String[] args) throws DataAccessException {
		HibernateContext context = new SimpleHibernateContextImpl();
		EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
		ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
		HardwareDbMapper dbMapper = new HardwareDbMapper(new HardwareRegistry(), context);

		try {		
			
			LinkBoard linkBoard = (LinkBoard)equipmentDAO.getBoardByName("LB_LBB_904_2_M8");
			HardwareDbChip syncoder = dbMapper.getChip(linkBoard.getSynCoder());
			//board.
			String tbName = "TB0_904";
			
			for(int bcn0delay = 0; bcn0delay < 40; bcn0delay+=2) {	
				for(int iLink= 0; iLink < 18; iLink+=3) {
					TriggerBoard triggerBoard = (TriggerBoard)equipmentDAO.getBoardByName(tbName);
					HardwareDbChip opto = dbMapper.getChip(triggerBoard.getOptoForLinkNum(iLink));

					opto.getHardwareChip().getItem("BCN0_DELAY").write(bcn0delay);
				}
				
				syncoder.getHardwareChip().getItem("GOL.TX_ENA").write(0);
				
				for(int iLink= 15; iLink < 17; iLink+=3) {
					TriggerBoard triggerBoard = (TriggerBoard)equipmentDAO.getBoardByName(tbName);
					HardwareDbChip opto = dbMapper.getChip(triggerBoard.getOptoForLinkNum(iLink));

					long rxNoData = opto.getHardwareChip().getItem("TLK.RX_NO_DATA").read();
					rxNoData = opto.getHardwareChip().getItem("TLK.RX_NO_DATA").read();
					//System.out.println(opto.getFullName() + " rxNoData " + rxNoData);
				}
				syncoder.getHardwareChip().getItem("GOL.TX_ENA").write(1);
				
				//rxNoData = opto.getHardwareChip().getItem("TLK.RX_NO_DATA").read();
				//rxNoData = opto.getHardwareChip().getItem("TLK.RX_NO_DATA").read();
				//System.out.print(" rxNoData " + rxNoData);
				
				for(int iLink= 15; iLink < 17; iLink+=3) {
					TriggerBoard triggerBoard = (TriggerBoard)equipmentDAO.getBoardByName(tbName);
					HardwareDbChip opto = dbMapper.getChip(triggerBoard.getOptoForLinkNum(iLink));

					long synchAck = opto.getHardwareChip().getItem("STATUS.REC_SYNCH_ACK").read();
					synchAck = opto.getHardwareChip().getItem("STATUS.REC_SYNCH_ACK").read();
					System.out.print(opto.getFullName() + " bcn0delay " + bcn0delay + " synchAck " + synchAck);
					if(synchAck != 0)
						System.out.println(" !!!!!!!!!!!!!!!!!!!!!");
					else
						System.out.println(); 
				}
				System.out.println();
			}
			
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
            context.closeSession();
        }
	}
}
