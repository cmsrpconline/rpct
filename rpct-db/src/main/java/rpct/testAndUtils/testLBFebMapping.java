package rpct.testAndUtils;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Date;
import java.util.List;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.FebChipConf;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.SynCoderConf;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.FebBoard;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.feblocation.FebLocation;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.synchro.DefOut;

public class testLBFebMapping {

	/**
	 * @param args
	 * @throws DataAccessException 
	 */
	public static void main(String[] args) throws DataAccessException {
		// TODO Auto-generated method stub
		HibernateContext context = new SimpleHibernateContextImpl();
		EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(context);
		ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(context);
		ConfigurationManager configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);	 

		List<Board> linkBoards;
		try {
	        PrintStream lbFebChipMapFile = new PrintStream(new FileOutputStream(System.getenv("HOME") + "/bin/out/lbFebChipMap.txt", true));
	        
			linkBoards = equipmentDAO.getBoardsByType(BoardType.LINKBOARD, true);	
			
			LocalConfigKey localConfigKeyLBs = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_LBS_LHC1);
			LocalConfigKey localConfigKeyFEBs = configurationManager.getLocalConfigKey(ConfigurationManager.LOCAL_CONF_KEY_FEBS_DEFAULT);
			int i= 0;
			for(Board board :linkBoards) {
				//System.out.println(board.getName());
				int position = 0;
				for(Chip febChip :((LinkBoard)board).getFebChips()) {					
					lbFebChipMapFile.print(board.getName() + " " + position++);
					if(febChip != null) {
						/*System.out.println(" " + febChip.getBoard().getName() + " "
							+ ((FebBoard)febChip.getBoard()).getFebLocation().getFebLocalEtaPartition() + " "  
							+ ((FebBoard)febChip.getBoard()).getFebLocation().getPosInLocalEtaPartition() +" " + febChip.getPosition());
*/						
						lbFebChipMapFile.println(" " + febChip.getId() + " " + ((FebBoard)febChip.getBoard()).getFebLocation().getChamberLocation().getChamberLocationName()
								+ " " + ((FebBoard)febChip.getBoard()).getFebLocation().getFebLocalEtaPartition() );
						//to take the settings from the DB
						//FebChipConf synCoderConf =  (FebChipConf)configurationManager.getConfiguration( febChip, localConfigKeyFEBs);
					}
					else
						lbFebChipMapFile.println(" null"); 
						
				}
				
				//to take the settings from the DB
				//SynCoderConf synCoderConf =  (SynCoderConf)configurationManager.getConfiguration( ((LinkBoard)board).getSynCoder(), localConfigKeyLBs);
				
				i++;
				/*if(i== 100)
					break;*/
				
			}
		} catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            context.rollback();
        } catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
/*        catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/
        finally {
            context.closeSession();
        }
	}

}
