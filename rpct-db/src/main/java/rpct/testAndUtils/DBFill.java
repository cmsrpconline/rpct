package rpct.testAndUtils;

import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.stream.XMLStreamException;

import org.apache.log4j.PropertyConfigurator;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.BoardDisabled;
import rpct.db.domain.configuration.ChipConfAssignment;
import rpct.db.domain.configuration.ChipDisabled;
import rpct.db.domain.configuration.ConfigKeyAssignment;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.CrateDisabled;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.configuration.SynCoderConf;
import rpct.db.domain.configuration.XdaqAppLBoxAccess;
import rpct.db.domain.configuration.XdaqAppVmeCrateAccess;
import rpct.db.domain.configuration.XdaqApplication;
import rpct.db.domain.configuration.XdaqExecutive;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.ControlCrate;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.DccBoard;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.equipment.SorterBoard;
import rpct.db.domain.equipment.SorterCrate;
import rpct.db.domain.equipment.TcBackplane;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.equipment.TtuBoard;
import rpct.db.domain.equipment.ccsboard.CcsBoard;
import rpct.db.domain.equipment.controlboard.ControlBoard;
import rpct.db.domain.equipment.feblocation.FebLocation;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;


/**
 * Created on 2006-01-13
 * 
 * @author Michal Pietrusinski
 * @version $Id: MtccFill.java,v 1.43 2009/06/19 18:26:01 tb Exp $
 */
public class DBFill {
	public class Constants {
	    //public final static String DUMMY_CCS_NAME = "DUMMY_CCS";
	    //public final static String DUMMY_VME_NAME = "DUMMY_VME";
	    
	    //public final static String CCS_NAME = "CCS_MTCC";
	    //public final static String CCS_LABEL = "CCS_MTCC";
	    //public final static int CCS_POSITION = 6;
	    

	    public final static String CONTROL_VME_CRATE_NAME = "VME_CONTROL";
	    public final static String CONTROL_VME_CRATE_LABEL = "VME_CONTROL";
	    
	    public final static String XDAQ_HOST = "pccmsrpct";
	    public final static int XDAQ_PORT = 1972;

	    public final static String XDAQ_TC_HOST = "pccmsrpct";
	    public final static int XDAQ_TC_PORT = 1973;
	}

    enum CBPos {
        CB_10, CB_20
    }

    private HibernateContext hibernateContext;
    private EquipmentDAO equipmentDAO;
    private ConfigurationDAO configurationDAO;
    private ConfigurationManager configurationManger;

    public DBFill(HibernateContext hibernateContext) {
        this.hibernateContext = hibernateContext;
        this.equipmentDAO = new EquipmentDAOHibernate(hibernateContext);
        this.configurationDAO = new ConfigurationDAOHibernate(hibernateContext);
        this.configurationManger = new ConfigurationManager(hibernateContext, equipmentDAO, configurationDAO);
    }

    public Crate getControlCrate() throws DataAccessException {

        ControlCrate crate = (ControlCrate) equipmentDAO.getCrateByName(Constants.CONTROL_VME_CRATE_NAME);
        if (crate == null) {
            crate = new ControlCrate();
            crate.setLabel(Constants.CONTROL_VME_CRATE_LABEL);
            crate.setName(Constants.CONTROL_VME_CRATE_NAME);
            equipmentDAO.saveObject(crate);
            equipmentDAO.flush();
        }
        return crate;
    }

    public Crate getControlCrate(String suffix) throws DataAccessException {
        String name = "VME_CONTROL_" + suffix;
        ControlCrate crate = (ControlCrate) equipmentDAO.getCrateByName(name);
        if (crate == null) {
            crate = new ControlCrate();
            crate.setLabel(name);
            crate.setName(name);
            equipmentDAO.saveObject(crate);
            equipmentDAO.flush();
        }
        return crate;
    }

    public CcsBoard createOrGetCCSBoard(String suffix) throws DataAccessException {
        String name = "CCS_" + suffix;
        CcsBoard ccsBoard = (CcsBoard) equipmentDAO.getBoardByName(name);
        if (ccsBoard == null) {
            ccsBoard = new CcsBoard();
            ccsBoard.setLabel(name);
            ccsBoard.setName(name);
            ccsBoard.setPosition(6);
            ccsBoard.setCrate(getControlCrate(suffix));
            equipmentDAO.saveObject(ccsBoard);
            equipmentDAO.flush();
        }
        return ccsBoard;
    }

    public CcsBoard getCCSBoard(int pos) throws DataAccessException {
        String name = "CCS_" + pos;
        CcsBoard ccsBoard = (CcsBoard) equipmentDAO.getBoardByName(name);
        if (ccsBoard == null) {
            ccsBoard = new CcsBoard();
            ccsBoard.setLabel(name);
            ccsBoard.setName(name);
            ccsBoard.setPosition(pos);
            ccsBoard.setCrate(getControlCrate());
            equipmentDAO.saveObject(ccsBoard);
            equipmentDAO.flush();
        }
        return ccsBoard;
    }

    public void setupControlBoard(String linkBoxName, CBPos cbPos, int ccuAddress, int posInCcuChain, int ccsPos,
            int fecPos) throws DataAccessException {
        // String linkBoxName = "LBB_" + wheelSector;
        LinkBox linkBox = (LinkBox) equipmentDAO.getCrateByName(linkBoxName);
        if (linkBox == null) {
            throw new IllegalArgumentException("Could not find lbox " + linkBoxName);
        }
        // System.out.println(linkBox.toString());
        ControlBoard cb = cbPos == CBPos.CB_10 ? linkBox.getControlBoard10() : linkBox.getControlBoard20();
        if (cb == null) {
            cb = new ControlBoard();
            cb.setCrate(linkBox);
            cb.setPosition(cbPos == CBPos.CB_10 ? 10 : 20);
            //throw new IllegalArgumentException("Could not find controlBoard " + cbPos + " in " + linkBoxName);
        }
        CcsBoard ccsBoard = getCCSBoard(ccsPos);
        cb.setCcsBoard(ccsBoard);
        cb.setFecPosition(fecPos);
        cb.setCcuAddress(ccuAddress);
        String name = "CB_" + linkBox.getName().substring(4) + '_' + (cbPos == CBPos.CB_10 ? "L10" : "L20");
        cb.setName(name);
        cb.setLabel(name);
        cb.setPositionInCcuChain(posInCcuChain);
        //cb.setRearPanelChannel(rearPanelChannel);
        // cb.setCcsbInputConnector(ccsbInputConnector);
        // System.out.println(cb.toString());
        // System.out.println(ccsBoard.toString());
        equipmentDAO.saveObject(cb);
    }

    private XdaqAppLBoxAccess setupXdaqLBoxAccess(XdaqExecutive xdaq, int fecPos, int instance, int appId,
            CcsBoard ccsBoard) throws DataAccessException {
        XdaqAppLBoxAccess application = (XdaqAppLBoxAccess) configurationDAO.getXdaqApplication(xdaq, "XdaqLBoxAccess",
                instance);
        if (application == null) {
            application = new XdaqAppLBoxAccess();
            application.setXdaqExecutive(xdaq);
            application.setCcsBoard(ccsBoard);
            application.setFecPosition(fecPos);
            application.setInstance(instance);
            application.setAppId(appId);
            equipmentDAO.saveObject(application);
        } else {
            application.setCcsBoard(ccsBoard);
            application.setFecPosition(fecPos);
            application.setAppId(appId);
            equipmentDAO.saveObject(application);
        }
        return application;
    }

    private XdaqAppLBoxAccess setupXdaqLBoxAccess(String host, int port, int ccsPos, int fecPos, int appInstance,
            int appId) throws DataAccessException {

        return setupXdaqLBoxAccess(getOrCreateXdaqExecutive(host, port), fecPos, appInstance, appId,
                getCCSBoard(ccsPos));

    }

    /*
     * public void setupSoftware() throws DataAccessException { XdaqExecutive
     * xdaq = configurationDAO.getXdaqExecutive(Constants.XDAQ_HOST,
     * Constants.XDAQ_PORT); if (xdaq == null) { xdaq = new XdaqExecutive();
     * xdaq.setHost(Constants.XDAQ_HOST); xdaq.setPort(Constants.XDAQ_PORT);
     * equipmentDAO.saveObject(xdaq); }
     * 
     * setupXdaqLBoxAccess(xdaq, 0, 0, 7001); setupXdaqLBoxAccess(xdaq, 1, 1,
     * 7002); setupXdaqLBoxAccess(xdaq, 2, 2, 7003); }
     */

    public LocalConfigKey getDefaultLocalConfigKey() throws DataAccessException {
        LocalConfigKey configKey = configurationDAO.getLocalConfigKeyByName("DEFAULT");
        if (configKey == null) {
            configKey = new LocalConfigKey();
            configKey.setName("DEFAULT");
            configurationDAO.saveObject(configKey);
            configurationDAO.flush();
        }
        return configKey;
    }

    public LocalConfigKey getDefaultConfigKey() throws DataAccessException {
        LocalConfigKey localConfigKey = configurationDAO.getLocalConfigKeyByGlobalConfigKey("DEFAULT");
        if (localConfigKey == null) {
            localConfigKey = getDefaultLocalConfigKey();
            ConfigKeyAssignment configKeyAssignment = new ConfigKeyAssignment();
            configKeyAssignment.setConfigKey("DEFAULT");
            configKeyAssignment.setLocalConfigKey(localConfigKey);
            configurationDAO.saveObject(configKeyAssignment);

        }
        return localConfigKey;
    }

    public SynCoderConf getDefaultSynCoderConfig() throws DataAccessException {

        SynCoderConf synCoderConf = (SynCoderConf) configurationDAO.getObject(SynCoderConf.class, 20);
        if (synCoderConf == null) {
            synCoderConf = new SynCoderConf();
            synCoderConf.setBcn0Delay(1);
            synCoderConf.setDataDaqDelay(2);
            synCoderConf.setDataTrgDelay(3);
            synCoderConf.setInChannelsEna(BigInteger.valueOf(0x1234).toByteArray());
            synCoderConf.setInvertClock(true);
            synCoderConf.setLmuxInDelay(4);
            synCoderConf.setPulserTimerTrgDelay(5);
            synCoderConf.setRbcDelay(6);
            synCoderConf.setWindowClose(7);
            synCoderConf.setWindowOpen(8);
            configurationDAO.saveObject(synCoderConf);
            configurationDAO.flush();
        }
        return synCoderConf;
    }

    public void createDefaultSetup() throws DataAccessException {
        LocalConfigKey configKey = getDefaultConfigKey();
        System.out.println("ConfigKey = " + configKey.getId());
        SynCoderConf synCoderConf = getDefaultSynCoderConfig();
        System.out.println("synCoderConf = " + synCoderConf.getId());
        /*
         * //List<Chip> chips = equipmentDAO.getChipsByType(ChipType.SYNCODER);
         * //for (Chip chip : chips) { Chip chip = (Chip)
         * equipmentDAO.getObject(Chip.class, 1); System.out.println("chip = " +
         * chip.getId()); System.out.println(" " + chip.toString());
         * ChipConfAssignment assignment = new ChipConfAssignment();
         * assignment.setLocalConfigKey(configKey);
         * assignment.setStaticConfiguration(synCoderConf);
         * assignment.setChip(chip); configurationDAO.saveObject(assignment);
         * //}
         * 
         */
        Chip chip = (Chip) equipmentDAO.getObject(Chip.class, 1);
        /*
         * ChipDisabled chipDisabled = new ChipDisabled();
         * chipDisabled.setChip(chip);
         * configurationDAO.saveObject(chipDisabled);
         */
        /*
         * SortedSet<Integer> sortedDisabledChipIds = new TreeSet<Integer>(configurationDAO.getDisabledChipsIds());
         * for (Integer id : sortedDisabledChipIds) {
         * System.out.println("Disabled id " + id); }
         */
        List<Chip> chips = new ArrayList<Chip>();
        chips.add(chip);
        // List<Chip> chips = equipmentDAO.getChipsByType(ChipType.SYNCODER);
        List<ChipConfAssignment> chipConfAssignments = configurationDAO
                .getChipConfAssignmentsByChipsAndGlobalConfigKey(chips, "DEFAULT");
        for (ChipConfAssignment assignment : chipConfAssignments) {
            System.out.println("Assignment " + assignment.getId());
            System.out.println("config class = " + assignment.getStaticConfiguration().getClass());
            System.out.println("config id = " + assignment.getStaticConfiguration().getId());
            SynCoderConf synCoderConf2 = (SynCoderConf) assignment.getStaticConfiguration();
            System.out.println("synCoderConf2 id = " + synCoderConf2.getId());
        }
    }

    private void assignSettingToChips(int confId, String localConfigKey, List<Chip> chips) throws DataAccessException {
        StaticConfiguration conf = (StaticConfiguration) configurationDAO.getObject(StaticConfiguration.class, confId);
        LocalConfigKey key = configurationDAO.getLocalConfigKeyByName(localConfigKey);
        for (Chip chip : chips) {
            ChipConfAssignment chipConfAssignment = new ChipConfAssignment();
            chipConfAssignment.setChip(chip);
            chipConfAssignment.setLocalConfigKey(key);
            chipConfAssignment.setStaticConfiguration(conf);
            System.out.println("Assigning conf for " + chip.getBoard().getName());
            configurationDAO.saveObject(chipConfAssignment);
        }

        System.out.println("Number of assigned chips " + chips.size());
    }

/*    private void disableLinkBoardsNotHavingSettings(List<LinkBoard> linkBoards, String settingsFile, boolean disableOnly)
            throws FileNotFoundException, DataAccessException, XMLStreamException {
        Set<LinkBoard> lbSet = new HashSet<LinkBoard>();
        lbSet.addAll(linkBoards);
        ImportLinkBoardSettingsXml settingsXml = new ImportLinkBoardSettingsXml(hibernateContext);
        settingsXml.setReadOnly(disableOnly);
        settingsXml.importSettings(settingsFile);
        lbSet.removeAll(settingsXml.getSettings().keySet());
        for (LinkBoard board : lbSet) {
            System.out.println("Disabling lb " + board.toString());
            ChipDisabled chipDisabled = new ChipDisabled();
            chipDisabled.setChip(board.getSynCoder());
            configurationDAO.saveObject(chipDisabled);
        }
    }

    private void importLinkBoardSettings(String settingsFile) throws FileNotFoundException, DataAccessException,
            XMLStreamException {
        ImportLinkBoardSettingsXml settingsXml = new ImportLinkBoardSettingsXml(hibernateContext);
        settingsXml.importSettings(settingsFile);
    }*/

    private void printFecChain() throws DataAccessException {
        for (int i = 0; i < 1; i++) {
            System.out.println("FEC " + i);
            List<LinkBox> linkBoxes = equipmentDAO.getLinkBoxesByFec(getCCSBoard(6), i);
            for (LinkBox linkBox : linkBoxes) {
                System.out.println(linkBox.toString());
                List<Board> boards = linkBox.getBoards();
                for (Board board : boards) {
                    System.out.println("   " + board.toString());
                    if (board instanceof LinkBoard) {
                        LinkBoard linkBoard = (LinkBoard) board;
                        List<FebLocation> febLocations = linkBoard.getFEBLocations();
                        for (FebLocation febLocation : febLocations) {
                            System.out.println("           "
                                    + febLocation.getChamberLocation().getChamberLocationName()
                                    + " febLocalEtaPartition: " + febLocation.getFebLocalEtaPartition());
                        }
                    }
                    Set<Chip> chips = board.getChips();
                    if (chips != null) {
                        for (Chip chip : chips) {
                            System.out.println("       " + chip.toString());
                        }
                    }
                }
            }
        }
    }

    private void enableBoard(Board board, boolean enable) throws DataAccessException {
        BoardDisabled boardDisabled = configurationDAO.getBoardDisabled(board);
        if (!enable && boardDisabled == null) {
            boardDisabled = new BoardDisabled();
            boardDisabled.setBoard(board);
            configurationDAO.saveObject(boardDisabled);
            System.out.println("Disabled board " + board.getName());
        } else if (enable && boardDisabled != null) {
            configurationDAO.deleteObject(boardDisabled);
            System.out.println("Enabled board " + board.getName());
        }
    }

    private void enableBoard(String name, boolean enable) throws DataAccessException {
        enableBoard(equipmentDAO.getBoardByName(name), enable);
    }

    private void enableCrate(Crate crate, boolean enable) throws DataAccessException {
        CrateDisabled crateDisabled = configurationDAO.getCrateDisabled(crate);
        if (!enable && crateDisabled == null) {
            crateDisabled = new CrateDisabled();
            crateDisabled.setCrate(crate);
            configurationDAO.saveObject(crateDisabled);
            System.out.println("Disabled crate " + crate.getName());
        } else if (enable && crateDisabled != null) {
            configurationDAO.deleteObject(crateDisabled);
            System.out.println("Enabled crate " + crate.getName());
        }
    }

    private void enableCrate(String name, boolean enable) throws DataAccessException {
        enableCrate(equipmentDAO.getCrateByName(name), enable);
    }

    private void enableCrates(String[] names, boolean enable) throws DataAccessException {
        for (String name : names) {
            enableCrate(name, enable);
        }
    }

    private void disableHalfBox(String lbbName, CBPos cbPos) throws DataAccessException {
        LinkBox linkBox = (LinkBox) equipmentDAO.getCrateByName(lbbName);
        for (Board board : linkBox.getBoards()) {
            if (board instanceof LinkBoard && ((cbPos == CBPos.CB_10 && board.getPosition() < 10))
                    || (cbPos == CBPos.CB_20 && board.getPosition() > 10)) {
                LinkBoard lb = (LinkBoard) board;
                ChipDisabled chipDisabled = new ChipDisabled();
                chipDisabled.setChip(lb.getSynCoder());
                configurationDAO.saveObject(chipDisabled);
                System.out.println("Disabled lb " + lb.getName());
            }
        }
    }

    private void disableChipById(int id) throws DataAccessException {
        Chip chip = (Chip) configurationDAO.getObject(Chip.class, id);

        ChipDisabled chipDisabled = new ChipDisabled();
        chipDisabled.setChip(chip);
        configurationDAO.saveObject(chipDisabled);
        System.out.println("Disabled " + chip.getType().toString() + " " + chip.getId());
    }

    private void configureChain() throws DataAccessException {

        int ccsPos;
        int fecPos;
        int posInChain;

        // ring for RB+1 near
        /*
         * ccsPos = 6; fecPos = 0; posInChain = 0;
         * setupControlBoard("LBB_RB+1_S10", CBPos.CB_10, 0x10, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB+1_S10", CBPos.CB_20, 0x15,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB+1_S12",
         * CBPos.CB_10, 0x20, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB+1_S12", CBPos.CB_20, 0x25, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB+1_S2", CBPos.CB_10, 0x30,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB+1_S2",
         * CBPos.CB_20, 0x35, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB+1_S3", CBPos.CB_10, 0x40, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB+1_S3", CBPos.CB_20, 0x45,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB+1_S1",
         * CBPos.CB_10, 0x50, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB+1_S1", CBPos.CB_20, 0x55, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB+1_S11", CBPos.CB_10, 0x60,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB+1_S11",
         * CBPos.CB_20, 0x65, posInChain++, ccsPos, fecPos);
         */

        // ring for RB+1 far
        /*
         * ccsPos = 6; fecPos = 3; posInChain = 0;
         * setupControlBoard("LBB_RB+1_S9", CBPos.CB_10, 0x10, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB+1_S9", CBPos.CB_20, 0x15,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB+1_S7",
         * CBPos.CB_10, 0x20, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB+1_S7", CBPos.CB_20, 0x25, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB+1_S5", CBPos.CB_10, 0x30,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB+1_S5",
         * CBPos.CB_20, 0x35, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB+1_S4", CBPos.CB_10, 0x40, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB+1_S4", CBPos.CB_20, 0x45,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB+1_S6",
         * CBPos.CB_10, 0x50, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB+1_S6", CBPos.CB_20, 0x55, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB+1_S8", CBPos.CB_10, 0x60,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB+1_S8",
         * CBPos.CB_20, 0x65, posInChain++, ccsPos, fecPos);
         */
        /*
         * // RB+2 near ccsPos = 6; fecPos = 2; posInChain = 0;
         * setupControlBoard("LBB_RB+2_S10", CBPos.CB_10, 0x10, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB+2_S10", CBPos.CB_20, 0x15,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB+2_S12",
         * CBPos.CB_10, 0x20, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB+2_S12", CBPos.CB_20, 0x25, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB+2_S2", CBPos.CB_10, 0x30,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB+2_S2",
         * CBPos.CB_20, 0x35, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB+2_S3", CBPos.CB_10, 0x40, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB+2_S3", CBPos.CB_20, 0x45,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB+2_S1",
         * CBPos.CB_10, 0x50, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB+2_S1", CBPos.CB_20, 0x55, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB+2_S11", CBPos.CB_10, 0x60,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB+2_S11",
         * CBPos.CB_20, 0x65, posInChain++, ccsPos, fecPos); // RB+2 far ccsPos =
         * 6; fecPos = 3; posInChain = 0; setupControlBoard("LBB_RB+2_S9",
         * CBPos.CB_10, 0x10, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB+2_S9", CBPos.CB_20, 0x15, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB+2_S7", CBPos.CB_10, 0x20,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB+2_S7",
         * CBPos.CB_20, 0x25, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB+2_S5", CBPos.CB_10, 0x30, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB+2_S5", CBPos.CB_20, 0x35,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB+2_S4",
         * CBPos.CB_10, 0x40, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB+2_S4", CBPos.CB_20, 0x45, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB+2_S6", CBPos.CB_10, 0x50,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB+2_S6",
         * CBPos.CB_20, 0x55, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB+2_S8", CBPos.CB_10, 0x60, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB+2_S8", CBPos.CB_20, 0x65,
         * posInChain++, ccsPos, fecPos);
         * 
         * 
         * //!!!!! different LBOX order 
         * // RB0 near ccsPos = 6; fecPos = 4;
         * posInChain = 0; setupControlBoard("LBB_RB0_S10", CBPos.CB_10, 0x10,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB0_S10",
         * CBPos.CB_20, 0x15, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB0_S2", CBPos.CB_10, 0x30, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB0_S2", CBPos.CB_20, 0x35,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB0_S3",
         * CBPos.CB_10, 0x40, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB0_S3", CBPos.CB_20, 0x45, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB0_S11", CBPos.CB_10, 0x60,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB0_S11",
         * CBPos.CB_20, 0x65, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB0_S1", CBPos.CB_10, 0x50, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB0_S1", CBPos.CB_20, 0x55,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB0_S12",
         * CBPos.CB_10, 0x20, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB0_S12", CBPos.CB_20, 0x25, posInChain++,
         * ccsPos, fecPos); // RB0 far ccsPos = 6; fecPos = 5; posInChain = 0;
         * setupControlBoard("LBB_RB0_S9", CBPos.CB_10, 0x10, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB0_S9", CBPos.CB_20, 0x15,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB0_S5",
         * CBPos.CB_10, 0x30, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB0_S5", CBPos.CB_20, 0x35, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB0_S4", CBPos.CB_10, 0x40,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB0_S4",
         * CBPos.CB_20, 0x45, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB0_S8", CBPos.CB_10, 0x60, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB0_S8", CBPos.CB_20, 0x65,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB0_S6",
         * CBPos.CB_10, 0x50, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB0_S6", CBPos.CB_20, 0x55, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB0_S7", CBPos.CB_10, 0x20,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB0_S7",
         * CBPos.CB_20, 0x25, posInChain++, ccsPos, fecPos);
         * 
         * ccsPos = 6; fecPos = 0; posInChain = 0;
         * setupControlBoard("LBB_RB+1_S10", CBPos.CB_10, 0x10, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB+1_S10", CBPos.CB_20, 0x15,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB+1_S12",
         * CBPos.CB_10, 0x20, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB+1_S12", CBPos.CB_20, 0x25, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB+1_S2", CBPos.CB_10, 0x30,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB+1_S2",
         * CBPos.CB_20, 0x35, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB+1_S3", CBPos.CB_10, 0x40, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB+1_S3", CBPos.CB_20, 0x45,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB+1_S1",
         * CBPos.CB_10, 0x50, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB+1_S1", CBPos.CB_20, 0x55, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB+1_S11", CBPos.CB_10, 0x60,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB+1_S11",
         * CBPos.CB_20, 0x65, posInChain++, ccsPos, fecPos); // ring for RB-1
         * near ccsPos = 5; fecPos = 0; posInChain = 0;
         * setupControlBoard("LBB_RB-1_S10", CBPos.CB_10, 0x10, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB-1_S10", CBPos.CB_20, 0x15,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB-1_S12",
         * CBPos.CB_10, 0x20, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB-1_S12", CBPos.CB_20, 0x25, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB-1_S2", CBPos.CB_10, 0x30,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB-1_S2",
         * CBPos.CB_20, 0x35, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB-1_S3", CBPos.CB_10, 0x40, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB-1_S3", CBPos.CB_20, 0x45,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB-1_S1",
         * CBPos.CB_10, 0x50, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB-1_S1", CBPos.CB_20, 0x55, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB-1_S11", CBPos.CB_10, 0x60,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB-1_S11",
         * CBPos.CB_20, 0x65, posInChain++, ccsPos, fecPos);
         * 
         * setupXdaqLBoxAccess(getOrCreateXdaqExecutive("pcrpct03", 1973),
         * fecPos, 10, 7010, getCCSBoard(ccsPos)); // ring for RB-1 far ccsPos =
         * 5; fecPos = 1; posInChain = 0; setupControlBoard("LBB_RB-1_S9",
         * CBPos.CB_10, 0x10, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB-1_S9", CBPos.CB_20, 0x15, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB-1_S7", CBPos.CB_10, 0x20,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB-1_S7",
         * CBPos.CB_20, 0x25, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB-1_S5", CBPos.CB_10, 0x30, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB-1_S5", CBPos.CB_20, 0x35,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB-1_S4",
         * CBPos.CB_10, 0x40, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB-1_S4", CBPos.CB_20, 0x45, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB-1_S6", CBPos.CB_10, 0x50,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB-1_S6",
         * CBPos.CB_20, 0x55, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB-1_S8", CBPos.CB_10, 0x60, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB-1_S8", CBPos.CB_20, 0x65,
         * posInChain++, ccsPos, fecPos);
         * 
         * setupXdaqLBoxAccess(getOrCreateXdaqExecutive("pcrpct03", 1973),
         * fecPos, 11, 7011, getCCSBoard(ccsPos));
         */

        // ring for RB-2 near
        /*
         * ccsPos = 5; fecPos = 2; posInChain = 0;
         * setupControlBoard("LBB_RB-2_S10", CBPos.CB_10, 0x10, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB-2_S10", CBPos.CB_20, 0x15,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB-2_S12",
         * CBPos.CB_10, 0x20, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB-2_S12", CBPos.CB_20, 0x25, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB-2_S2", CBPos.CB_10, 0x30,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB-2_S2",
         * CBPos.CB_20, 0x35, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB-2_S3", CBPos.CB_10, 0x40, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB-2_S3", CBPos.CB_20, 0x45,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB-2_S1",
         * CBPos.CB_10, 0x50, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB-2_S1", CBPos.CB_20, 0x55, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB-2_S11", CBPos.CB_10, 0x60,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB-2_S11",
         * CBPos.CB_20, 0x65, posInChain++, ccsPos, fecPos);
         * 
         * 
         * setupXdaqLBoxAccess(getOrCreateXdaqExecutive("pcrpct03", 1973),
         * fecPos, 12, 7012, getCCSBoard(ccsPos)); // ring for RB-2 far ccsPos =
         * 5; fecPos = 3; posInChain = 0; setupControlBoard("LBB_RB-2_S9",
         * CBPos.CB_10, 0x10, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB-2_S9", CBPos.CB_20, 0x15, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB-2_S7", CBPos.CB_10, 0x20,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB-2_S7",
         * CBPos.CB_20, 0x25, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB-2_S5", CBPos.CB_10, 0x30, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB-2_S5", CBPos.CB_20, 0x35,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB-2_S4",
         * CBPos.CB_10, 0x40, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB-2_S4", CBPos.CB_20, 0x45, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB-2_S6", CBPos.CB_10, 0x50,
         * posInChain++, ccsPos, fecPos); setupControlBoard("LBB_RB-2_S6",
         * CBPos.CB_20, 0x55, posInChain++, ccsPos, fecPos);
         * setupControlBoard("LBB_RB-2_S8", CBPos.CB_10, 0x60, posInChain++,
         * ccsPos, fecPos); setupControlBoard("LBB_RB-2_S8", CBPos.CB_20, 0x65,
         * posInChain++, ccsPos, fecPos);
         * 
         * setupXdaqLBoxAccess(getOrCreateXdaqExecutive("pcrpct03", 1973),
         * fecPos, 13, 7013, getCCSBoard(ccsPos));
         */

/*        // ring for RE+1 near (YEP1)
        ccsPos = 5;
        fecPos = 4;
        posInChain = 0;
        setupControlBoard("LBB_YEP1_S10", CBPos.CB_10, 0x10, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP1_S10", CBPos.CB_20, 0x15, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP1_S12", CBPos.CB_10, 0x20, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP1_S12", CBPos.CB_20, 0x25, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP1_S2", CBPos.CB_10, 0x30, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP1_S2", CBPos.CB_20, 0x35, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP1_S3", CBPos.CB_10, 0x40, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP1_S3", CBPos.CB_20, 0x45, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP1_S1", CBPos.CB_10, 0x50, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP1_S1", CBPos.CB_20, 0x55, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP1_S11", CBPos.CB_10, 0x60, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP1_S11", CBPos.CB_20, 0x65, posInChain++, ccsPos, fecPos);

        setupXdaqLBoxAccess(getOrCreateXdaqExecutive("pcrpct03", 1973), fecPos, 14, 7014, getCCSBoard(ccsPos));

        // ring for RE+1 far (YEP1)
        ccsPos = 5;
        fecPos = 5;
        posInChain = 0;
        setupControlBoard("LBB_YEP1_S9", CBPos.CB_10, 0x10, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP1_S9", CBPos.CB_20, 0x15, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP1_S7", CBPos.CB_10, 0x20, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP1_S7", CBPos.CB_20, 0x25, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP1_S5", CBPos.CB_10, 0x30, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP1_S5", CBPos.CB_20, 0x35, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP1_S4", CBPos.CB_10, 0x40, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP1_S4", CBPos.CB_20, 0x45, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP1_S6", CBPos.CB_10, 0x50, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP1_S6", CBPos.CB_20, 0x55, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP1_S8", CBPos.CB_10, 0x60, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP1_S8", CBPos.CB_20, 0x65, posInChain++, ccsPos, fecPos);

        setupXdaqLBoxAccess(getOrCreateXdaqExecutive("pcrpct03", 1973), fecPos, 15, 7015, getCCSBoard(ccsPos));*/
        
        /*
        // ring for RE+3 near (YEP3)
        ccsPos = 4;
        fecPos = 0;
        posInChain = 0;
        setupControlBoard("LBB_YEP3_S2/3", CBPos.CB_10, 0x10, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP3_S2/3", CBPos.CB_20, 0x15, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP3_S12/1", CBPos.CB_10, 0x20, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP3_S12/1", CBPos.CB_20, 0x25, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP3_S10/11", CBPos.CB_10, 0x30, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP3_S10/11", CBPos.CB_20, 0x35, posInChain++, ccsPos, fecPos);
       
        setupXdaqLBoxAccess("pcrpct03", 1940, ccsPos, fecPos, 20, 7020); // Slot 4 FEC 0 RE+3near
        
        // ring for RE+3 far (YEP3)
        ccsPos = 4;
        fecPos = 1;
        posInChain = 0;
        setupControlBoard("LBB_YEP3_S4/5", CBPos.CB_10, 0x10, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP3_S4/5", CBPos.CB_20, 0x15, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP3_S6/7", CBPos.CB_10, 0x20, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP3_S6/7", CBPos.CB_20, 0x25, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP3_S8/9", CBPos.CB_10, 0x30, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEP3_S8/9", CBPos.CB_20, 0x35, posInChain++, ccsPos, fecPos);
        
        setupXdaqLBoxAccess("pcrpct03", 1941, ccsPos, fecPos, 21, 7021); // Slot 4 FEC 1 RE+3far
        */
        
        
        // ring for RE-1 near (YEN1)
        ccsPos = 4;
        fecPos = 2;
        posInChain = 0;
        setupControlBoard("LBB_YEN1_S10", CBPos.CB_10, 0x10, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN1_S10", CBPos.CB_20, 0x15, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN1_S12", CBPos.CB_10, 0x20, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN1_S12", CBPos.CB_20, 0x25, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN1_S2", CBPos.CB_10, 0x30, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN1_S2", CBPos.CB_20, 0x35, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN1_S3", CBPos.CB_10, 0x40, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN1_S3", CBPos.CB_20, 0x45, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN1_S1", CBPos.CB_10, 0x50, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN1_S1", CBPos.CB_20, 0x55, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN1_S11", CBPos.CB_10, 0x60, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN1_S11", CBPos.CB_20, 0x65, posInChain++, ccsPos, fecPos);

        setupXdaqLBoxAccess("vmepcs2g18-13.cms", 1942, ccsPos, fecPos, 22, 7022); //RE-1 near

        // ring for RE-1 far (YEN1)Rxdaq
        ccsPos = 4;
        fecPos = 3;
        posInChain = 0;
        setupControlBoard("LBB_YEN1_S9", CBPos.CB_10, 0x10, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN1_S9", CBPos.CB_20, 0x15, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN1_S7", CBPos.CB_10, 0x20, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN1_S7", CBPos.CB_20, 0x25, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN1_S5", CBPos.CB_10, 0x30, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN1_S5", CBPos.CB_20, 0x35, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN1_S4", CBPos.CB_10, 0x40, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN1_S4", CBPos.CB_20, 0x45, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN1_S6", CBPos.CB_10, 0x50, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN1_S6", CBPos.CB_20, 0x55, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN1_S8", CBPos.CB_10, 0x60, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN1_S8", CBPos.CB_20, 0x65, posInChain++, ccsPos, fecPos);

        setupXdaqLBoxAccess("vmepcs2g18-13.cms", 1943, ccsPos, fecPos, 23, 7023); //RE-1 far
        
        
        // ring for RE-3 near (YEN3)
        ccsPos = 4;
        fecPos = 4;
        posInChain = 0;
        setupControlBoard("LBB_YEN3_S2/3", CBPos.CB_10, 0x10, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN3_S2/3", CBPos.CB_20, 0x15, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN3_S12/1", CBPos.CB_10, 0x20, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN3_S12/1", CBPos.CB_20, 0x25, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN3_S10/11", CBPos.CB_10, 0x30, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN3_S10/11", CBPos.CB_20, 0x35, posInChain++, ccsPos, fecPos);
       
        setupXdaqLBoxAccess("vmepcs2g18-13.cms", 1944, ccsPos, fecPos, 24, 7024); // RE-3 near
        
        // ring for RE-3 far (YEN3)
        ccsPos = 4;
        fecPos = 5;
        posInChain = 0;
        setupControlBoard("LBB_YEN3_S4/5", CBPos.CB_10, 0x10, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN3_S4/5", CBPos.CB_20, 0x15, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN3_S6/7", CBPos.CB_10, 0x20, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN3_S6/7", CBPos.CB_20, 0x25, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN3_S8/9", CBPos.CB_10, 0x30, posInChain++, ccsPos, fecPos);
        setupControlBoard("LBB_YEN3_S8/9", CBPos.CB_20, 0x35, posInChain++, ccsPos, fecPos);
        
        setupXdaqLBoxAccess("vmepcs2g18-13.cms", 1945, ccsPos, fecPos, 25, 7025); // RE-3 far
    }

    private void createTriggerBoardChips(TriggerBoard triggerBoard) throws DataAccessException {

        Set<Integer> tbPositonsWithLessPacs = new HashSet<Integer>();
        tbPositonsWithLessPacs.add(14);
        tbPositonsWithLessPacs.add(15);
        tbPositonsWithLessPacs.add(16);

        Set<Chip> chips = triggerBoard.getChips();
        if (chips == null || chips.isEmpty()) {

            Chip gbsort = new Chip();
            gbsort.setType(ChipType.GBSORT);
            gbsort.setBoard(triggerBoard);
            gbsort.setPosition(1);
            equipmentDAO.saveObject(gbsort);

            for (int i = 0; i < 6; i++) {
                Chip opto = new Chip();
                opto.setType(ChipType.OPTO);
                opto.setBoard(triggerBoard);
                opto.setPosition(Chip.OPTO_BASE_POS + i);
                equipmentDAO.saveObject(opto);
            }

            int pacNum = tbPositonsWithLessPacs.contains(triggerBoard.getPosition()) ? 3 : 4;
            for (int i = 0; i < pacNum; i++) {
                Chip pac = new Chip();
                pac.setType(ChipType.PAC);
                pac.setBoard(triggerBoard);
                pac.setPosition(8 + i);
                equipmentDAO.saveObject(pac);
            }
            Chip rmb = new Chip();
            rmb.setType(ChipType.RMB);
            rmb.setBoard(triggerBoard);
            rmb.setPosition(12);
            equipmentDAO.saveObject(rmb);
        }
    }

    private void createTriggerBoardChips(List<TriggerBoard> triggerBoards) throws DataAccessException {
        List<? extends Board> boards = triggerBoards == null ? equipmentDAO.getBoardsByType(BoardType.TRIGGERBOARD,
                false) : triggerBoards;
        for (Board board : boards) {
            createTriggerBoardChips((TriggerBoard) board);
        }
    }

    private void createBackplane(Crate triggerCrate, int logSector) throws DataAccessException {
        TcBackplane backplane = new TcBackplane();
        backplane.setLabel(triggerCrate.getLabel() + "_BP");
        backplane.setName(triggerCrate.getName() + "_BP");
        backplane.setLogSector(logSector);
        backplane.setPosition(0);
        backplane.setCrate(triggerCrate);
        Chip tcsort = new Chip();
        tcsort.setType(ChipType.TCSORT);
        tcsort.setBoard(backplane);
        tcsort.setPosition(0);
        equipmentDAO.saveObject(backplane);
        equipmentDAO.saveObject(tcsort);
    }

    private void deleteConfigurationsForBoard(Board board, LocalConfigKey key) throws DataAccessException {
        for (Chip chip : board.getChips()) {
            ChipConfAssignment chipConfAssignment = configurationDAO.getChipConfAssignment(chip, key);
            if (chipConfAssignment != null) {
                configurationDAO.deleteObject(chipConfAssignment);
            }
        }
    }

    private void deleteCrate(Crate crate) throws DataAccessException {
        for (Board b : crate.getBoards()) {
            for (Chip chip : b.getChips()) {
                equipmentDAO.deleteObject(chip);
            }
            equipmentDAO.deleteObject(b);
        }
        equipmentDAO.deleteObject(crate);
    }

    private void disableCratesNotConnectedToXdaq(CrateType crateType) throws DataAccessException {
        List<Crate> cratesByType = equipmentDAO.getCratesByType(CrateType.TRIGGERCRATE, true);
        for (Crate crate : cratesByType) {
            // System.out.println(crate);
            XdaqApplication xdaqApplication = configurationDAO.getXdaqApplication(crate);
            // System.out.println(xdaqApplication);
            if (xdaqApplication == null) {
                System.out.println("Disabling crate " + crate);
                CrateDisabled crateDisabled = new CrateDisabled();
                crateDisabled.setCrate(crate);
                configurationDAO.saveObject(crateDisabled);
            }
        }
    }

    private void createLinkConn(LinkBoard lb, TriggerBoard tb, int inputNum) throws DataAccessException {
        LinkConn lc = new LinkConn();
        lc.setTriggerBoard(tb);
        lc.setBoard(lb);
        lc.setTriggerBoardInputNum(inputNum);
        equipmentDAO.saveObject(lc);
    }

    private void deleteLinkConn(LinkBoard lb, TriggerBoard tb, int inputNum) throws DataAccessException {
        for (LinkConn lc : lb.getLinkConns()) {
            if (lc.getTriggerBoard().getId() == tb.getId() && lc.getTriggerBoardInputNum() == inputNum) {
            	if(lc.getDisabled() != null) {
            		equipmentDAO.deleteObject(lc.getDisabled());
            		System.out.println("deleting " + lc.getDisabled());
            	}
            	System.out.println("deleting " + lc.getId() + " " + lc);
                //equipmentDAO.deleteObject(lc);
            }
        }
    }
    
    private void setPortNumbersForXdaqAppLBoxAccess() throws DataAccessException {

        List<XdaqAppLBoxAccess> apps = configurationDAO.getObjects(XdaqAppLBoxAccess.class);
        for (XdaqAppLBoxAccess app : apps) {
            int port = 1950 + app.getInstance();
            if (app.getXdaqExecutive().getPort() != port) {
                XdaqExecutive ex = getOrCreateXdaqExecutive(app.getXdaqExecutive().getHost(), port);
                app.setXdaqExecutive(ex);
                configurationDAO.saveObject(ex);
            }
        }
    }
    
    void createLinkBoard(boolean saveToDB, LinkBox linkBox, int position) throws DataAccessException {
    	LinkBoard lb = new LinkBoard();

    	lb.setCrate(linkBox);
    	if(position >= 1 && position < 20 && position != 10)
    		lb.setPosition(position);
    	else 
    		throw new IllegalArgumentException("position =" + position);

    	String name = null; 
    	if(lb.isMaster()) { 
    		name  = "LB_" + linkBox.getName() + "_M" + (position -1);
    	}
    	else { 
    		name  = "LB_" + linkBox.getName() + "_S" + (position -1);
    	}
    	lb.setName(name);
    	lb.setLabel(name);

    	if(saveToDB)
    		equipmentDAO.saveObject(lb);
    	
    	Chip c = new Chip();
    	c.setBoard(lb);
    	c.setPosition(0);
    	c.setType(ChipType.SYNCODER);
    	if(saveToDB)
    		equipmentDAO.saveObject(c);
    	
    	System.out.println("Added to DB: " + lb.toString());
    }
    
    /**
     * @param lbbName
     * @param ccs_suffix
     * @param posInCcuChain from 0
     * @throws DataAccessException
     */
    private void createLinkBox(boolean saveToDB, String lbbName, String ccs_suffix, int posInCcuChain) throws DataAccessException {
        LinkBox linkBox = (LinkBox) equipmentDAO.getCrateByName(lbbName);
        if (linkBox == null) {
            linkBox = new LinkBox();
            linkBox.setName(lbbName);
            linkBox.setLabel(lbbName);
            if(saveToDB) {
            	equipmentDAO.saveObject(linkBox);
            	equipmentDAO.flush();
            }
        }

        System.out.println("Added to DB: " + linkBox.toString());
        CcsBoard ccs = createOrGetCCSBoard(ccs_suffix);
        createControlBoardTestBench(saveToDB, linkBox, CBPos.CB_10, (posInCcuChain+1)<<4,    2 * posInCcuChain,    0, lbbName);
        createControlBoardTestBench(saveToDB, linkBox, CBPos.CB_20, ((posInCcuChain+1)<<4) +5, 2 * posInCcuChain +1, 0, lbbName);

        for(int position = 1; position < 20; position++) {
        	if(position != 10)
        		createLinkBoard(saveToDB, linkBox, position);
        }
    }

    private void temp2() throws DataAccessException, RemoteException {

        String lbbName = "LBB_1_PASTEURA";
        LinkBox linkBox = (LinkBox) equipmentDAO.getCrateByName(lbbName);
        if (linkBox == null) {
            linkBox = new LinkBox();
            linkBox.setName(lbbName);
            linkBox.setLabel(lbbName);
            equipmentDAO.saveObject(linkBox);
            equipmentDAO.flush();
        }

        CcsBoard ccs = createOrGetCCSBoard("PASTEURA");
        createControlBoardTestBench(linkBox, CBPos.CB_10, 0x60, 2, 0, "PASTEURA");
        createControlBoardTestBench(linkBox, CBPos.CB_20, 0x65, 3, 0, "PASTEURA");

        //XdaqExecutive xdaq = getOrCreateXdaqExecutive("pccmsrpct2", 1972);
        //setupXdaqLBoxAccess(xdaq, 0, 0, 7001, ccs);
    	///////////////////
    	
    	
    	
        /*List<LinkBoard> linkBoardsWithoutChips = equipmentDAO.getLinkBoardsWithoutChips();
        for (LinkBoard linkBoard : linkBoardsWithoutChips) {
            System.out.println(linkBoard);
            Chip c = new Chip();
            c.setBoard(linkBoard);
            c.setPosition(0);
            c.setType(ChipType.SYNCODER);
            equipmentDAO.saveObject(c);
        }*/
        
        //Crate crate = equipmentDAO.getCrateByName("PASTEURA_LBB");
        //System.out.println(crate);
        //createTtuBoard(crate, "TTU_3", "TTU_3", 18);         
        
        /*crateSorterBoard("TTU_BACKPLANE", "TTU_BACKPLANE", 0, SorterType.TTU_BACKPLANE);*/
        
        /*SorterCrate sc = createOrGetSorterCrate();
        SorterBoaLird b = new SorterBoard();
        b.setCrate(sc);
        b.setName("TTU_BACKPLANE");
        b.setLabel("TTU_BACKPLANE");
        b.setPosition(0);
        b.setSorterType(SorterType.TTU_BACKPLANE);
        equipmentDAO.saveObject(b);
        equipmentDAO.flush();
        Chip chip = new Chip();
        chip.setBoard(b);
        chip.setPosition(0);
        chip.setType(sorterType == SorterBoard.SorterType.FSB ? ChipType.FINALSORTER : ChipType.HALFSORTER);
        equipmentDAO.saveObject(chip);
        return b;*/
        
        // configurationManger.enableBoard(board, enable)
        // Crate crate = (Crate) equipmentDAO.getObject(Crate.class, 76);
        // crate.getBoards();
        // configurationDAO.getObjects(ChipConfAssignment.class);
        // equipmentDAO.getChipsByType(ChipType.SYNCODER, false);
        // for (Board b : equipmentDAO.getBoardsByType(BoardType.LINKBOARD,
        // false)) {
        /*
         * for (Object b : equipmentDAO.getObjects(LinkBoard.class)) {
         * //System.out.println(b); //b.getChips(); //System.out.println("ok"); }
         */

        /*
         * try { HalfSortConf conf = new HalfSortConf( new byte[] {1, 2}, new
         * IntArray16(), new IntArray16(), new IntArray16(), new byte[] {3, 4},
         * new byte[] {3, 4}); configurationDAO.saveObject(conf);
         * hibernateContext.rollback(); } catch (DataAccessException e) { throw
         * e; }
         */

        /*
         * TtuBoard board = (TtuBoard) equipmentDAO.getBoardByName("TTU_1");
         * 
         * Chip pac = new Chip(); pac.setType(ChipType.TTUTRIG);
         * pac.setBoard(board); pac.setPosition(Chip.PAC_BASE_POS + 1);
         * equipmentDAO.saveObject(pac);
         */

        // createAllElementsForTriggerCrate("TC_4", 4);
        /**/

        /*
         * List<RbcInfo> list = equipmentDAO.getAllRbcInfo(); for (RbcInfo
         * rbcInfo : list) { System.out.println(rbcInfo); }
         */

        /*
         * ConfigSetInfo configSetInfo =
         * configurationDAO.getConfigSetInfoByChipIdsAndGlobalConfigKey(
         * Arrays.asList(new Integer[] {2561, 2562, 2563}), "DEFAULT");
         * System.out.println(configSetInfo.getLocalConfigKeyId());
         * System.out.println(configSetInfo.getCreationDate());
         */

        // createAllElementsForTriggerCrate("TC_10", 10);
        // setupXdaqLBoxAccess(getOrCreateXdaqExecutive("pcrpct03", 1973),
        // 4, 4, 7005);
        // setupXdaqLBoxAccess(getOrCreateXdaqExecutive("pcrpct03", 1973),
        // 5, 5, 7006);
        // configurationManger.enableLinkConn((TriggerBoard)
        // equipmentDAO.getBoardByName("TBp2_9"), 6, false);
        // configurationManger.enableLinkConn((TriggerBoard)
        // equipmentDAO.getBoardByName("TBp2_9"), 7, false);
        // configurationManger.enableLinkConn((TriggerBoard)
        // equipmentDAO.getBoardByName("TBp2_9"), 10, false);
        /*
         * TriggerBoard triggerBoard = (TriggerBoard)
         * equipmentDAO.getBoardByName("TBn3_4"); for (LinkConn linkConn :
         * triggerBoard.getLinkConns()) {
         * System.out.println(linkConn.getTriggerBoardInputNum()); }
         * configurationManger.enableLinkConn(triggerBoard, 1, true);
         * System.out.println("Potem"); for (LinkConn linkConn :
         * equipmentDAO.getLinkConns(triggerBoard, true)) {
         * System.out.println(linkConn.getTriggerBoardInputNum()); }
         */
        /*
         * Chip c = (Chip) equipmentDAO.getObject(Chip.class, 311);
         * System.out.println(c); System.out.println(c.getBoard());
         * System.out.println(c.getBoard().getCrate());
         * System.out.println(((LinkBox)
         * c.getBoard().getCrate()).getControlBoard10());
         * 
         * configurationDAO.getXdaqApplication(c);
         */

        /*
         * TriggerCrate tc = (TriggerCrate) equipmentDAO.getCrateByName("TC_9");
         * //createTtuBoard(tc, "TTU_1", "TTU_1", 11); for (Board board :
         * tc.getBoards()) { System.out.println(board); if (board.getType() ==
         * BoardType.TTUBOARD) { TtuBoard ttuBoard = (TtuBoard) board; for (Chip
         * chip : board.getChips()) { System.out.println(chip); } } }
         */

        /*
         * LinkBox linkBox = (LinkBox)
         * equipmentDAO.getCrateByName("PASTEURA_LBB"); Integer[] masters = new
         * Integer[] {2, 5, 8, 12, 15, 18}; for (Integer mPos : masters) {
         * LinkBoard master = (LinkBoard) linkBox.getBoardArray()[mPos]; Chip
         * syncoder;
         * 
         * LinkBoard left = new LinkBoard(); left.setCrate(linkBox);
         * left.setPosition(mPos - 1); left.setLabel("LB_S" +
         * (left.getPosition() - 1)); left.setName(left.getLabel());
         * left.setMaster(master); equipmentDAO.saveObject(left); syncoder = new
         * Chip(); syncoder.setBoard(left); syncoder.setPosition(0);
         * syncoder.setType(ChipType.SYNCODER);
         * equipmentDAO.saveObject(syncoder);
         * 
         * LinkBoard right = new LinkBoard(); right.setCrate(linkBox);
         * right.setPosition(mPos + 1); right.setLabel("LB_S" +
         * (right.getPosition() - 1)); right.setName(right.getLabel());
         * right.setMaster(master); equipmentDAO.saveObject(right); syncoder =
         * new Chip(); syncoder.setBoard(right); syncoder.setPosition(0);
         * syncoder.setType(ChipType.SYNCODER);
         * equipmentDAO.saveObject(syncoder); }
         */

        // getDefaultConfigKey();
        // configurationDAO.getObject(StaticConfiguration.class, 1980);
        // setupXdaqLBoxAccess(getOrCreateXdaqExecutive("pccms8", 1972),
        // 0, 0, 7001);
        /*
         * TriggerBoard tb = (TriggerBoard) equipmentDAO.getBoardByName("TBp2");
         * String[] lbNames = new String[] {"LB_M14", "LB_M17"}; int inputNum =
         * 12; for (String lbName : lbNames) { LinkBoard lb = (LinkBoard)
         * equipmentDAO.getBoardByName(lbName); createLinkConn(lb, tb,
         * inputNum++); createLinkConn(lb, tb, inputNum++); createLinkConn(lb,
         * tb, inputNum++); createLinkConn(lb, tb, inputNum++); }
         */

        /*
         * List<Board> boards =
         * equipmentDAO.getBoardsByType(BoardType.LINKBOARD, false); for (Board
         * board : boards) { LinkBoard lb = (LinkBoard) board; }
         */
        // enableBoard("TBp3_9", false);
        // setupXdaqLBoxAccess(getOrCreateXdaqExecutive("pcrpct03", 1973),
        // 3, 3, 7004);
        /*
         * TriggerCrate tc = (TriggerCrate) equipmentDAO.getCrateByName("TC_9");
         * System.out.println(tc); for (TriggerBoard b : tc.getTriggerBoards()) {
         * System.out.println(b); for (LinkConn conn : b.getLinkConns()) {
         * System.out.println(conn.getSynCoder()); } }
         */

        // enableCrates(new String[] {"LBB_RB+2_S12", "LBB_RB+2_S2",
        // "LBB_RB+2_S3", "LBB_RB+2_S1"}, true);
        // getOrCreateXdaqExecutive("pcrpct03", 11973);
        // TriggerCrate tc9 = (TriggerCrate)
        // equipmentDAO.getCrateByName("TC_11");
        // System.out.println(tc9.getLogSector());
        /*
         * XdaqExecutive xdaq = configurationDAO.getXdaqExecutive("pcrpct03",
         * 1973); for (XdaqApplication app : xdaq.getXdaqApplications()) {
         * System.out.println(app); }
         */
        // ((ConfigurationDAOHibernate)configurationDAO).commit();
        // System.out.println("22: " +
        // configurationDAO.getObject(XdaqExecutive.class, 22));*/
        /*
         * try { PacConf pacConf = new PacConf( new byte[] {1, 2}, new byte[]
         * {3, 4}, new IntArray18(), new IntArray18());
         * configurationDAO.saveObject(pacConf); TriggerCrate crate =
         * (TriggerCrate) equipmentDAO.getCrateByName("TC_9"); Chip c =
         * crate.getTriggerBoards().get(0).getPac(0); ConfigurationManger
         * configurationManger = new ConfigurationManger(hibernateContext,
         * configurationDAO); configurationManger.assignConfiguration(c,
         * pacConf, ConfigurationManger.LOCAL_CONF_KEY_DEFAULT);
         * hibernateContext.rollback(); } catch (DataAccessException e) { throw
         * e; }
         */

        /*
         * List<Crate> cratesByType =
         * equipmentDAO.getCratesByType(CrateType.CONTROLCRATE, true); for
         * (Crate crate : cratesByType) { System.out.println(crate); }
         */
        // disableCratesNotConnectedToXdaq(CrateType.TRIGGERCRATE);
        // deleteCrate(equipmentDAO.getCrateById(144));
        /*
         * List<Chip> chips = equipmentDAO.getChipsByTypeAndBoard(ChipType.PAC,
         * equipmentDAO.getBoardByName("TB0_9"));
         * 
         * assignSettingToChips(1801, "DEFAULT", chips); LocalConfigKey
         * configKey = configurationDAO.getLocalConfigKeyByName("DEFAULT"); List<ChipConfAssignment>
         * chipConfAssignments = configurationDAO.getChipConfAssignments(chips,
         * configKey);
         * 
         * for (ChipConfAssignment assignment : chipConfAssignments) {
         * System.out.println(assignment.getId() + " " +
         * assignment.getCreationDate() + " " +
         * assignment.getChip().toString()); }
         */

    }

    private XdaqExecutive getOrCreateXdaqExecutive(String host, int port) throws DataAccessException {

        XdaqExecutive xdaq = configurationDAO.getXdaqExecutive(host, port);
        if (xdaq == null) {
            xdaq = new XdaqExecutive();
            xdaq.setHost(host);
            xdaq.setPort(port);
            equipmentDAO.saveObject(xdaq);
            equipmentDAO.flush();
        }
        return xdaq;
    }

    public void setupTcSoftware(String host, int port, int instance, int appId, String crateName, int pciSlot,
            int posInVmeChain) throws DataAccessException {
        XdaqExecutive xdaq = getOrCreateXdaqExecutive(host, port);
        setupXdaqVmeCrateAccess(xdaq, equipmentDAO.getCrateByName(crateName), pciSlot, posInVmeChain, "XdaqTestBench",
                instance, appId);
    }

    public void setupScSoftware() throws DataAccessException {
        /*
         * XdaqExecutive xdaq =
         * configurationDAO.getXdaqExecutive(Constants.XDAQ_TC_HOST,
         * Constants.XDAQ_TC_PORT); if (xdaq == null) { xdaq = new
         * XdaqExecutive(); xdaq.setHost(Constants.XDAQ_TC_HOST);
         * xdaq.setPort(Constants.XDAQ_TC_PORT); equipmentDAO.saveObject(xdaq); }
         * 
         * setupXdaqVmeCrateAccess(xdaq,
         * equipmentDAO.getCrateByName("VME_TRIGGER"), 1, 0, "XdaqTcAccess", 0,
         * 8001);
         */
        XdaqExecutive xdaq;

        xdaq = getOrCreateXdaqExecutive("pccmsrpct2", 2910);
        setupXdaqVmeCrateAccess(xdaq, createOrGetSorterCrate("SC_904"), 2, 0, "rpcttsworker::TBCell", 10, 110);
    }

    private XdaqAppVmeCrateAccess setupXdaqVmeCrateAccess(XdaqExecutive xdaq, Crate crate, int pciSlot,
            int posInVmeChain, String className, int instance, int appId) throws DataAccessException {
        XdaqAppVmeCrateAccess application = (XdaqAppVmeCrateAccess) configurationDAO.getXdaqApplication(xdaq,
                className, instance);
        if (application == null) {
            application = new XdaqAppVmeCrateAccess();
            application.setXdaqExecutive(xdaq);
            application.setClassName(className);
            application.setInstance(instance);
            application.setAppId(appId);

            application.setCrate(crate);
            application.setPciSlot(pciSlot);
            application.setPosInVmeChain(posInVmeChain);
            equipmentDAO.saveObject(application);
        } else {
            application.setAppId(appId);
            application.setCrate(crate);
            application.setPciSlot(pciSlot);
            application.setPosInVmeChain(posInVmeChain);
            equipmentDAO.saveObject(application);
        }
        return application;
    }

    private XdaqAppVmeCrateAccess createXdaqVmeCrateAccessFomExisitignOne(String host, int port, 
    		String crateName, String className, int instance, int appId) throws DataAccessException {    	    
    	Crate crate = equipmentDAO.getCrateByName(crateName);
    	
    	XdaqExecutive newXdaqExecutive = getOrCreateXdaqExecutive(host, port);    	    
    	XdaqAppVmeCrateAccess oldApplication = (XdaqAppVmeCrateAccess)configurationDAO.getXdaqApplication(crate);

    	XdaqAppVmeCrateAccess newApplication = new XdaqAppVmeCrateAccess();
    	newApplication.setXdaqExecutive(newXdaqExecutive);
    	newApplication.setClassName(className);
    	newApplication.setInstance(instance);
    	newApplication.setAppId(appId);

    	newApplication.setCrate(crate);
    	newApplication.setPciSlot(oldApplication.getPciSlot());
    	newApplication.setPosInVmeChain(oldApplication.getPosInVmeChain());
    	equipmentDAO.saveObject(newApplication);

    	return newApplication;
    }
    
    private void deleteBoardByNameAndConfigAssignments(String boardName) throws DataAccessException {
        LocalConfigKey key = configurationDAO.getLocalConfigKeyByName("DEFAULT");
        Board board = equipmentDAO.getBoardByName(boardName);
        deleteConfigurationsForBoard(board, key);
        equipmentDAO.flush();
        for (Chip chip : board.getChips()) {
            equipmentDAO.deleteObject(chip);
            equipmentDAO.flush();
        }
        equipmentDAO.deleteObject(board);
    }

    private void createAllElementsForTriggerCrate(String tcName, int logSector) throws DataAccessException {
        TriggerCrate crate = (TriggerCrate) equipmentDAO.getCrateByName(tcName);
        createBackplane(crate, logSector);
        createTriggerBoardChips(crate.getTriggerBoards());
    }

    public DccBoard getDefaultDccBoard() throws DataAccessException {
        DccBoard dccBoard = (DccBoard) equipmentDAO.getBoardByName("DUMMY DCC");
        if (dccBoard == null) {
            dccBoard = new DccBoard();
            dccBoard.setName("DUMMY DCC");
            dccBoard.setLabel("DUMMY DCC");
            dccBoard.setCrate(createOrGetControlCrate());
            equipmentDAO.saveObject(dccBoard);
            equipmentDAO.flush();
        }
        return dccBoard;
    }

    public ControlCrate createOrGetControlCrate() throws DataAccessException {
        ControlCrate cc = (ControlCrate) equipmentDAO.getCrateByName("VME_CONTROL");
        if (cc == null) {
            cc = new ControlCrate();
            cc.setName("VME_CONTROL");
            cc.setLabel("S1F03E");
            equipmentDAO.saveObject(cc);
            equipmentDAO.flush();
        }
        return cc;
    }

    public SorterCrate createOrGetSorterCrate(String name) throws DataAccessException {
        SorterCrate crate = (SorterCrate) equipmentDAO.getCrateByName(name);
        if (crate == null) {
            crate = new SorterCrate();
            crate.setName(name);
            crate.setLabel(name);
            equipmentDAO.saveObject(crate);
            equipmentDAO.flush();
        }
        return crate;
    }

    public TriggerCrate createOrGetTriggerCrate(String name) throws DataAccessException {

        TriggerCrate tc = (TriggerCrate) equipmentDAO.getCrateByName(name);
        if (tc == null) {
            tc = new TriggerCrate();
            tc.setName(name);
            tc.setLabel(name);
            equipmentDAO.saveObject(tc);
            equipmentDAO.flush();
        }
        return tc;
    }

    public TriggerBoard createTriggerBoard(TriggerCrate triggerCrate, String name, String label, int position)
            throws DataAccessException {
        TriggerBoard tb = new TriggerBoard();
        tb.setCrate(triggerCrate);
        tb.setName(name);
        tb.setLabel(label);
        tb.setPosition(position);
        tb.setDccBoard(getDefaultDccBoard());
        equipmentDAO.saveObject(tb);
        equipmentDAO.flush();
        return tb;
    }

    public TtuBoard createTtuBoard(Crate crate, String name, String label, int position)
            throws DataAccessException {
        TtuBoard tb = new TtuBoard();
        tb.setCrate(crate);
        tb.setName(name);
        tb.setLabel(label);
        tb.setPosition(position);
        equipmentDAO.saveObject(tb);
        equipmentDAO.flush();

        for (int i = 0; i < 6; i++) {
            Chip opto = new Chip();
            opto.setType(ChipType.TTUOPTO);
            opto.setBoard(tb);
            opto.setPosition(Chip.OPTO_BASE_POS + i);
            equipmentDAO.saveObject(opto);
        }

        Chip pac = new Chip();
        pac.setType(ChipType.TTUTRIG);
        pac.setBoard(tb);
        pac.setPosition(Chip.PAC_BASE_POS);
        equipmentDAO.saveObject(pac);

        pac = new Chip();
        pac.setType(ChipType.TTUTRIG);
        pac.setBoard(tb);
        pac.setPosition(Chip.PAC_BASE_POS + 1);
        equipmentDAO.saveObject(pac);

        return tb;
    }

    public SorterBoard crateSorterBoard(String scName, String name, String label, int position, SorterBoard.SorterType sorterType)
            throws DataAccessException {
        SorterCrate sc = createOrGetSorterCrate(scName);
        SorterBoard b = new SorterBoard();
        b.setCrate(sc);
        b.setName(name);
        b.setLabel(label);
        b.setPosition(position);
        b.setSorterType(sorterType);
        equipmentDAO.saveObject(b);
        equipmentDAO.flush();
        Chip chip = new Chip();
        chip.setBoard(b);
        chip.setPosition(0);
        chip.setType(sorterType == SorterBoard.SorterType.FSB ? ChipType.FINALSORTER 
                : sorterType == SorterBoard.SorterType.HSB ? ChipType.HALFSORTER
                        : ChipType.TTUFINAL);
        equipmentDAO.saveObject(chip);
        return b;
    }
    
    public SorterBoard crateSorterBoardPasteura(String name, String label, int position, SorterBoard.SorterType sorterType)
    throws DataAccessException {
    	//SorterCrate sc = createOrGetSorterCrate();
    	Crate crate = equipmentDAO.getCrateByName("PASTEURA_TC");
    	SorterBoard b = new SorterBoard();
    	b.setCrate(crate);
    	b.setName(name);
    	b.setLabel(label);
    	b.setPosition(position);
    	b.setSorterType(sorterType);
    	equipmentDAO.saveObject(b);
    	equipmentDAO.flush();
    	Chip chip = new Chip();
    	chip.setBoard(b);
    	chip.setPosition(0);
    	chip.setType(sorterType == SorterBoard.SorterType.FSB ? ChipType.FINALSORTER 
    			: sorterType == SorterBoard.SorterType.HSB ? ChipType.HALFSORTER
    					: ChipType.TTUFINAL);
    	equipmentDAO.saveObject(chip);
    	return b;
    }

    public void createSorterBoards(String scName) throws DataAccessException {
        crateSorterBoard(scName, "HSB_0", "HSB_0", 1, SorterBoard.SorterType.HSB);
        crateSorterBoard(scName, "FSB", "FSB", 2, SorterBoard.SorterType.FSB);
        crateSorterBoard(scName, "HSB_1", "HSB_1", 3, SorterBoard.SorterType.HSB);
    }

    public void createControlBoardTestBench(boolean saveToDB, LinkBox linkBox, CBPos cbPos, int ccuAddress, int posInCcuChain,
            int fecPos, String suffix) throws DataAccessException {
        ControlBoard cb = cbPos == CBPos.CB_10 ? linkBox.getControlBoard10() : linkBox.getControlBoard20();
        if (cb == null) {
            cb = new ControlBoard();
            cb.setCrate(linkBox);
            cb.setPosition(cbPos == CBPos.CB_10 ? 10 : 20);
            CcsBoard ccsBoard = createOrGetCCSBoard(suffix);
            cb.setCcsBoard(ccsBoard);
            cb.setFecPosition(fecPos);
            cb.setCcuAddress(ccuAddress);
            String name = "CB_" + linkBox.getName() + '_' + (cbPos == CBPos.CB_10 ? "L10" : "L20");
            cb.setName(name);
            cb.setLabel(name);
            cb.setPositionInCcuChain(posInCcuChain);
            if(saveToDB)
            	equipmentDAO.saveObject(cb);
            
            System.out.println("Added to DB: " + cb.toString());
        }
    }
    
    public void createControlBoardTestBench(LinkBox linkBox, CBPos cbPos, int ccuAddress, int posInCcuChain,
            int fecPos, String suffix) throws DataAccessException {
    	createControlBoardTestBench(true, linkBox, cbPos, ccuAddress, posInCcuChain, fecPos, suffix);
    }

    private void configLinkBoxTestBench(String suffix) throws DataAccessException {

        String lbbName = "LBB_" + suffix;
        LinkBox linkBox = (LinkBox) equipmentDAO.getCrateByName(lbbName);
        if (linkBox == null) {
            linkBox = new LinkBox();
            linkBox.setName(lbbName);
            linkBox.setLabel(lbbName);
            equipmentDAO.saveObject(linkBox);
            equipmentDAO.flush();
        }
        /*
         * Integer[] masters = new Integer[] {2, 5, 8, 12, 15, 18}; for (Integer
         * mPos : masters) { Chip syncoder; LinkBoard master = new LinkBoard();
         * master.setCrate(linkBox); master.setPosition(mPos);
         * master.setLabel("LB_" + suffix + "_M" + (master.getPosition() - 1));
         * master.setName(master.getLabel()); master.setMaster(master);
         * equipmentDAO.saveObject(master); syncoder = new Chip();
         * syncoder.setBoard(master); syncoder.setPosition(0);
         * syncoder.setType(ChipType.SYNCODER);
         * equipmentDAO.saveObject(syncoder);
         * 
         * LinkBoard left = new LinkBoard(); left.setCrate(linkBox);
         * left.setPosition(mPos - 1); left.setLabel("LB_" + suffix + "_S" +
         * (left.getPosition() - 1)); left.setName(left.getLabel());
         * left.setMaster(master); equipmentDAO.saveObject(left); syncoder = new
         * Chip(); syncoder.setBoard(left); syncoder.setPosition(0);
         * syncoder.setType(ChipType.SYNCODER);
         * equipmentDAO.saveObject(syncoder);
         * 
         * LinkBoard right = new LinkBoard(); right.setCrate(linkBox);
         * right.setPosition(mPos + 1); right.setLabel("LB_" + suffix + "_S" +
         * (right.getPosition() - 1)); right.setName(right.getLabel());
         * right.setMaster(master); equipmentDAO.saveObject(right); syncoder =
         * new Chip(); syncoder.setBoard(right); syncoder.setPosition(0);
         * syncoder.setType(ChipType.SYNCODER);
         * equipmentDAO.saveObject(syncoder); }
         */

        CcsBoard ccs = createOrGetCCSBoard(suffix);
        createControlBoardTestBench(linkBox, CBPos.CB_10, 0x20, 0, 0, suffix);
        createControlBoardTestBench(linkBox, CBPos.CB_20, 0x25, 1, 0, suffix);

        XdaqExecutive xdaq = getOrCreateXdaqExecutive("pccmsrpct2", 1972);
        setupXdaqLBoxAccess(xdaq, 0, 0, 7001, ccs);
    }

    public void configTestBench(String suffix) throws DataAccessException {
        // configLinkBoxTestBench("LBB_" + suffix);

        TriggerCrate tc = createOrGetTriggerCrate("TC_" + suffix);

        String[] tbNames = new String[] { "TBn4", "TBn3", "TBn2", "TBn1", "TB0", "TBp1", "TBp2", "TBp3", "TBp4" };
        int tbPos = 11;
        for (String tbName : tbNames) {

            tbName = tbName + "_" + suffix;
            TriggerBoard tb = (TriggerBoard) equipmentDAO.getBoardByName(tbName);
            createTriggerBoardChips(tb);
            /*
             * if (tb != null) { continue; } tb = createTriggerBoard(tc, tbName,
             * tbName, tbPos++); equipmentDAO.flush();
             * createTriggerBoardChips(tb); equipmentDAO.flush();
             * 
             * String[] lbNames = new String[] { "LB_LBB_" + suffix + "_M1",
             * "LB_LBB_" + suffix + "_M4", "LB_LBB_" + suffix + "_M7", "LB_LBB_" +
             * suffix + "_M14", "LB_LBB_" + suffix + "_M17"}; int inputNum = 0;
             * for (String lbName : lbNames) { LinkBoard lb = (LinkBoard)
             * equipmentDAO.getBoardByName(lbName); for (int n = 0; n < 4 &&
             * inputNum < 18; n++, inputNum++) { createLinkConn(lb, tb,
             * inputNum); //deleteLinkConn(lb, tb, inputNum); } }
             */
        }
        // createBackplane(tc, 9);
        // createTriggerBoardChips(tc.getTriggerBoards());

        // XdaqExecutive xdaq = getOrCreateXdaqExecutive("pccmsrpct2", 1972);
        // setupXdaqVmeCrateAccess(xdaq, tc, 1, 0, "XdaqTestBench", 2, 8002);
    }

    void createDummyCongiguration() throws DataAccessException {
        SynCoderConf masterSynCoderConf = new SynCoderConf();
        masterSynCoderConf.setLmuxInDelay(2);
        SynCoderConf slaveSynCoderConf = new SynCoderConf();

        configurationDAO.saveObject(masterSynCoderConf);
        configurationDAO.saveObject(slaveSynCoderConf);
        LocalConfigKey key = configurationManger.getDefaultLocalConfigKey();

        for (Chip c : equipmentDAO.getChipsByType(ChipType.SYNCODER, false)) {
            StaticConfiguration configuration = configurationManger.getConfiguration(c, key);
            if (configuration == null) {
                if (((LinkBoard) c.getBoard()).isMaster()) {
                    configurationManger.assignConfiguration(c, masterSynCoderConf,
                            ConfigurationManager.LOCAL_CONF_KEY_LBS_DEFAULT);
                } else {
                    configurationManger.assignConfiguration(c, slaveSynCoderConf,
                            ConfigurationManager.LOCAL_CONF_KEY_LBS_DEFAULT);
                }
                System.out.println("the dummy configuration was added for the " + c.getBoard().getName());
            }
        }
    }

    public static void main(String[] args) throws DataAccessException, RemoteException {
        PropertyConfigurator.configure("log4j.properties");
        HibernateContext hibernateContext = new SimpleHibernateContextImpl();
        try {
            DBFill fill = new DBFill(hibernateContext);
            // fill.createDummyCongiguration();
            // fill.configurationManger.assignConfigKey("COSMIC", "COSMIC");
            // fill.configurationManger.assignConfigKey("COSMIC_TIGHT",
            // "COSMIC_TIGHT");
            // fill.configLinkBoxTestBench("904");
            // fill.configTestBench("904");
            
            //fill.temp2();

//            fill.setupTcSoftware("vmepcS2G18-11", 1973, 0, 8000, "TC_0", 1, 0);
//            fill.setupTcSoftware("pcrpct01", 1974, 2, 8002, "TC_2", 0, 0);
//            fill.setupTcSoftware("pcrpct02", 1974, 3, 8003, "TC_3", 1, 0);
//            fill.setupTcSoftware("pcrpct02", 1973, 4, 8004, "TC_4", 2, 0);
//
//            fill.setupTcSoftware("pcrpct04", 1974, 5, 8005, "TC_5", 0, 0);
//            fill.setupTcSoftware("pcrpct05", 1972, 6, 8004, "TC_6", 1, 0);
//            fill.setupTcSoftware("pcrpct05", 1973, 7, 8007, "TC_7", 2, 0);
//            fill.setupTcSoftware("pcrpct05", 1974, 8, 8008, "TC_8", 0, 0);
//            fill.setupTcSoftware("pcrpct02", 1972, 11, 8011, "TC_11", 0, 0);
             
            
            //fill.setupXdaqLBoxAccess("pcrpct03", 1955, 6, 5, 5, 7006); // RB0far           
            //
            
            // fill.createTriggerBoardChips(null);
            // fill.createAllElementsForTriggerCrate("TC_5", 5);
            // fill.disableHalfBox("LBB_RB+1_S11", CBPos.CB_10);
            // fill.disableChipById(410);
            // fill.createDefaultSetup();
            // fill.printFecChain();
            // fill.createSorterBoards();
            // fill.setupScSoftware();
            // fill.importLinkBoardSettings("lbSettings_904.xml");
            // fill.disableLinkBoardsNotHavingSettings(linkBoards,
            // "c:\\temp\\lbSettingsDB3.xml", false);
            
            //fill.configureChain();
            
            // fill.setupSoftware();
            
            //fill.crateSorterBoardPasteura("FSB", "FSB", 2, SorterBoard.SorterType.FSB);
            
            //fill.createLinkBox(false, "LBB_904_2", "904", 1);
            
            
            /*XdaqExecutive xdaq;

            xdaq = fill.getOrCreateXdaqExecutive("pccmsrpct2", 2910);
            Crate crate = fill.createOrGetSorterCrate("SC_904");
            fill.setupXdaqVmeCrateAccess(xdaq, crate, 2, 0, "rpcttsworker::TBCell", 10, 110);
            fill.crateSorterBoard("SC_904", "HSB_0", "HSB_0", 0xa, SorterBoard.SorterType.HSB);
            fill.crateSorterBoard("SC_904", "FSB", "FSB", 3, SorterBoard.SorterType.FSB);  */    
            
            fill.deleteLinkConn((LinkBoard)fill.equipmentDAO.getBoardByName("LB_LBB_904_M15"), 
            		(TriggerBoard)fill.equipmentDAO.getBoardByName("TB0_904"), 14);
            fill.deleteLinkConn((LinkBoard)fill.equipmentDAO.getBoardByName("LB_LBB_904_M15"), 
            		(TriggerBoard)fill.equipmentDAO.getBoardByName("TB0_904"), 15);
            
            fill.deleteLinkConn((LinkBoard)fill.equipmentDAO.getBoardByName("LB_LBB_904_M18"), 
            		(TriggerBoard)fill.equipmentDAO.getBoardByName("TB0_904"), 16);
            fill.deleteLinkConn((LinkBoard)fill.equipmentDAO.getBoardByName("LB_LBB_904_M18"), 
            		(TriggerBoard)fill.equipmentDAO.getBoardByName("TB0_904"), 17);
            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
            hibernateContext.rollback();
        } finally {
            hibernateContext.closeSession();
        }
        System.out.println("Done");
    }
}