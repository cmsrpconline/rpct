#include "rpct/i2c/TRPCCCUI2CMasterCore.h"

#include "log4cplus/loggingmacros.h"

using namespace log4cplus;
using namespace std;

namespace rpct {


unsigned short TRPCCCUI2CMasterCore::memoryRead(uint32_t address) {
    try {
        return LBox.memoryRead(address);
    }
    catch (CCUException& e) {
        // try once again
        LOG4CPLUS_WARN(logger, "CCUException during memoryRead from address " << address << " reseting FEC and trying once again");
        LBox.getFecManager().resetPlxFecForAutoRecovery();
        return LBox.memoryRead(address);
    }
}

void TRPCCCUI2CMasterCore::memoryWrite(uint32_t address, unsigned short data) {
    try {
        LBox.memoryWrite(address, data);
    }
    catch (CCUException& e) {
        // try once again
        LOG4CPLUS_WARN(logger, "CCUException during memoryWrite from address " << address << " reseting FEC and trying once again");
        LBox.getFecManager().resetPlxFecForAutoRecovery();
        LBox.memoryWrite(address, data);
    }
}

unsigned short TRPCCCUI2CMasterCore::memoryRead16(uint32_t address) {
    try {
        return LBox.memoryRead16(address);
    }
    catch (CCUException& e) {
        // try once again
        LOG4CPLUS_WARN(logger, "CCUException during memoryRead16 from address " << address << " reseting FEC and trying once again");
        LBox.getFecManager().resetPlxFecForAutoRecovery();
        return LBox.memoryRead16(address);
    }
}

void TRPCCCUI2CMasterCore::memoryWrite16(uint32_t address, unsigned short data) {
    try {
        LBox.memoryWrite16(address, data);
    }
    catch (CCUException& e) {
        // try once again
        LOG4CPLUS_WARN(logger, "CCUException during memoryWrite16 from address " << address << " reseting FEC and trying once again");
        LBox.getFecManager().resetPlxFecForAutoRecovery();
        LBox.memoryWrite16(address, data);
    }
}


}
