#include "rpct/i2c/TI2CMasterCore.h"

#include "log4cplus/loggingmacros.h"

using namespace log4cplus;
using namespace std;


namespace rpct {
	
Logger TI2CMasterCore::logger = Logger::getInstance("TI2CMasterCore");
void TI2CMasterCore::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}

void TI2CMasterCore::Sleep() {         
    tbsleep(10);
}


void TI2CMasterCore::SetPrescaleValue(unsigned short value) {
  unsigned char control = ReadControlReg();

  if ((control & BIT_EN) > 0) {
    WriteControlReg(control ^ BIT_EN);
    WritePrescaleReg(value);
    if (value != ReadPrescaleReg())
      throw TException("Could not set prescale register");
                               
    WriteControlReg(control);
  }
  else {
    WritePrescaleReg(value);
    if (value != ReadPrescaleReg())
      throw TException("Could not set prescale register");
  }
}



void TI2CMasterCore::Write8 (unsigned char address, unsigned char value) {
  LOG4CPLUS_DEBUG(logger, "TI2CMasterCore::Write8: address = " << hex << (int)address
            << " value = " << (int)value);

  // generate start command, set WR bit and write address
  WriteTransmitCommandReg((BuildSlaveAddress(address, true)) | BIT_STA | BIT_WR);

  Sleep();
  
  if (!WaitRxAck())
    throw EI2CNoAck("Write8: Acknoledge not received from slave after sending address");
  
  /*// receive acknoledge from slave
  for(int i=0; true; i++)
    if ((ReadReceiveStatusReg() & BIT_RxACK) == 0)
      break;
    else
      if (i == REPEAT) {
        // send STOP sequence
        WriteTransmitCommandReg(BIT_STO);
        throw EI2CNoAck("Write8: Acknoledge not received from slave after sending address");
      }*/

  Sleep();

  // write data to slave, set STO bit
  WriteTransmitCommandReg(BuildSlaveData(value) | BIT_STO | BIT_WR);


  Sleep();

  // receive acknoledge from slave
  for(int i=0; true; i++)
    if ((ReadReceiveStatusReg() & BIT_RxACK) == 0)
      break;
    else
      if (i == REPEAT)
        throw EI2CNoAck("Write8: Acknoledge not received from slave after sending value");

}



 /*
unsigned char TI2CMasterCore::Read8(unsigned char address)
{
  // generate start command, set WR bit and write address
  WriteTransmitCommandReg((BuildSlaveAddress(address, false)) | BIT_STA | BIT_WR);

  Sleep();

  // receive acknoledge from slave
  for(int i=0; true; i++)
    if ((ReadReceiveStatusReg() & BIT_RxACK) == 0)
      break;
    else
      if (i == REPEAT) {
        // send STOP sequence
        WriteTransmitCommandReg(BIT_STO);
        throw EI2CNoAck("Read8: Acknoledge not received from slave after sending address");
      }

  Sleep();

  // write data to slave, set STO bit
  WriteTransmitCommandReg(BIT_STA | BIT_RD);


  Sleep();

  // receive acknoledge from slave
  for(int i=0; true; i++) {
    unsigned short status = ReadReceiveStatusReg();
    if ((status & BIT_RxACK) == 0)
      return status >> 8;
    else
      if (i == REPEAT)
        throw EI2CNoAck("Read8: Acknoledge not received from slave after sending value");
  };
}



unsigned char TI2CMasterCore::Read8(unsigned char address)
{
  // generate start command, set WR bit and write address
  WriteTransmitCommandReg((BuildSlaveAddress(address, true)) | BIT_STA | BIT_WR);

  Sleep();

  // receive acknoledge from slave
  for(int i=0; true; i++)
    if ((ReadReceiveStatusReg() & BIT_RxACK) == 0)
      break;
    else
      if (i == REPEAT) {
        // send STOP sequence
        WriteTransmitCommandReg(BIT_STO);
        throw EI2CNoAck("Read8: Acknoledge not received from slave after sending address");
      }

  Sleep();

  // write data to slave, set STO bit
  WriteTransmitCommandReg(BIT_STA | BIT_RD);


  Sleep();

  // receive acknoledge from slave
  for(int i=0; true; i++) {
    unsigned short status = ReadReceiveStatusReg();
    if ((status & BIT_RxACK) == 0)
      return status >> 8;
    else
      if (i == REPEAT)
        throw EI2CNoAck("Read8: Acknoledge not received from slave after sending value");
  };
}        */



             
bool TI2CMasterCore::WaitRxAck() {  	
	// receive acknoledge from slave
	for (int i = 0; true; i++) {
        unsigned int status = ReadReceiveStatusReg();
        if ((status & (BIT_RxACK | BIT_TIP)) == 0) {
      		return true;
    	}
    	else {
      		//cout << "Still waiting for ack" << endl;
      		Sleep();
      		if (i == REPEAT) {
                if (status & BIT_TIP) {
                	WriteTransmitCommandReg(BIT_STO);
                    throw EI2C("I2C timeout waiting for acknowledge: transfer still in progress");
                }
        		// send STOP sequence
                WriteTransmitCommandReg(BIT_STO);
        		return false;
      		}
    	}
	}	
}

unsigned char TI2CMasterCore::Read8(unsigned char address, unsigned char reg) {
  
  // generate start command, set WR bit and write address
  WriteTransmitCommandReg(BuildSlaveAddress(address, true) | BIT_STA | BIT_WR);

  Sleep();

  // receive acknoledge from slave
  for(int i=0; true; i++)
    if ((ReadReceiveStatusReg() & BIT_RxACK) == 0)
      break;
    else
      if (i == REPEAT) {
        // send STOP sequence
        WriteTransmitCommandReg(BIT_STO);
        throw EI2CNoAck("Read8: Acknoledge not received from slave after sending address");
      }

  Sleep();

  WriteTransmitCommandReg(BuildSlaveData(reg) | BIT_WR);

  // receive acknoledge from slave
  for(int i=0; true; i++)
    if ((ReadReceiveStatusReg() & BIT_RxACK) == 0)
      break;
    else
      if (i == REPEAT) {
        // send STOP sequence
        WriteTransmitCommandReg(BIT_STO);
        throw EI2CNoAck("Read8: Acknoledge not received from slave after sending address");
      }

       
  // generate start command, set WR bit and write address
  WriteTransmitCommandReg(BuildSlaveAddress(address, false) | BIT_STA | BIT_WR);


  Sleep();

  // receive acknoledge from slave
  for(int i=0; true; i++) {
    unsigned short status = ReadReceiveStatusReg();
    if ((status & BIT_RxACK) == 0)
      return status >> 8;
    else
      if (i == REPEAT)
        throw EI2CNoAck("Read8: Acknoledge not received from slave after sending value");
  };
}






unsigned char TI2CMasterCore::Read8(unsigned char address, bool ack) {
  
  LOG4CPLUS_DEBUG(logger, "TI2CMasterCore::Read8: address = " << hex << (int)address);

  // generate start command, set WR bit and write address
  WriteTransmitCommandReg((BuildSlaveAddress(address, false)) | BIT_STA | BIT_WR);

  Sleep();

  // receive acknoledge from slave
  for(int i=0; true; i++) {
    if ((ReadReceiveStatusReg() & BIT_RxACK) == 0)
      break;
    else
      if (i == REPEAT)
        throw EI2CNoAck("Read8: Acknoledge not received from slave after sending address");
  };

  Sleep();

  // generate start command, set WR bit and write address
  WriteTransmitCommandReg(BIT_RD | (ack ? BIT_ACK : 0) | BIT_STO);

  Sleep();

  // receive acknoledge from slave
  return (ReadReceiveStatusReg() >> 8);
}


void TI2CMasterCore::WriteADD(unsigned char address, unsigned char value1,
                        unsigned char value2)
{
  throw EI2C("TI2CMasterCore::WriteADD: not implememted");
}


void TI2CMasterCore::ReadADD(unsigned char address, unsigned char& value1,
                        unsigned char& value2)
{
  throw EI2C("TI2CMasterCore::ReadADD: not implememted");
}


void TI2CMasterCore::Write(unsigned char address, unsigned char* data, size_t count)
{
  throw EI2C("TI2CMasterCore::Write: not implememted");
}

}
