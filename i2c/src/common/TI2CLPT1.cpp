//---------------------------------------------------------------------------

#include "precompiled.h"
#pragma hdrstop

#include "TI2CLPT1.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

/* Based on: */
/* File I2C.c    10 July 2001
Created by F. Loddo
Isituto Nazionale di Fisica Nucleare  Sez. di BARI

This code contains the main Routines to drive I2C lines

10-7-2001 Write threshold with feedback
*/


void TI2CLPT1::SCL()
{      
  long j;
#ifdef DEBUG
  fprintf(logfile,"SCL: outb(LPT1+2): CLK=high  Pin-17(Control)=0 => SCL=1\n");
  printf("_SCL: outb(LPT1+2): CLK=low  Pin-17(Control)=1 => SCL=0\n");
#endif
  for(j = 0;j < Repeat; j++)
    outp(LPT1+2,0x08);
    //outp(LPT1+2,0x00);
}

void TI2CLPT1::_SCL()
{
  long j;
#ifdef DEBUG
  fprintf(logfile,"_SCL: outb(LPT1+2): CLK=low  Pin-17(Control)=1 => SCL=0\n");
  printf("_SCL: outb(LPT1+2): CLK=low  Pin-17(Control)=1 => SCL=0\n");
#endif
  for(j = 0;j < Repeat; j++)
    outp(LPT1+2,0x00);
    //outp(LPT1+2,0x08);
}

void TI2CLPT1::SDA()
{
  long j;
#ifdef DEBUG
  fprintf(logfile,"SDA: outb(LPT1): DATA=high  Pin-9(DATA7)=0 => SDA=1\n");
  printf("SDA: outb(LPT1): DATA=high  Pin-9(DATA7)=0 => SDA=1\n");
#endif

  for(j = 0;j < Repeat; j++)
    outp(LPT1,0x00);
}

void TI2CLPT1::_SDA()
{   
  long j;
#ifdef DEBUG
  fprintf(logfile,"_SDA: outb(LPT1): DATA=low  Pin-9(DATA7)=1 => SDA=0\n");
  printf("_SDA: outb(LPT1): DATA=low  Pin-9(DATA7)=1 => SDA=0\n");
#endif
  for(j = 0;j < Repeat; j++)
    outp(LPT1,0x80);
}

int TI2CLPT1::SDA_IN()
{ 
#ifndef DEBUG
  if (inp(LPT1+1) & 0x80)
    return 1;
  else
    return 0;
#else
  int result;

  if (inp(LPT1+1) & 0x80)
    result = 1;
  else
    result = 0;

  fprintf(logfile,"SDA_IN: inp(LPT1+1): Pin-11(Status7)=%d\n", result);

  printf("SDA_IN: inp(LPT+1): Pin-11(Status7)=%d\n", result);

  return result;
#endif
}




/*********  Routine: I2C START function *************/
void TI2CLPT1::Start()
{
#ifdef DEBUG
   fprintf(logfile,"--- start_i2c --- BEGIN\n");
   printf("--- start_i2c --- BEGIN\n");
#endif
   SDA();
   SCL();
  _SDA();
  _SCL();
#ifdef DEBUG
  fprintf(logfile,"--- start_i2c --- END\n");
  printf("--- start_i2c --- END\n");
#endif
}  /* end start_i2c */
/*****************************************************/

/********* Routine: I2C STOP function *************/
void TI2CLPT1::Stop()
{
#ifdef DEBUG
  fprintf(logfile,"--- stop_i2c --- BEGIN\n");
  printf("--- stop_i2c --- BEGIN\n");
#endif
  _SDA();
  SCL();
  SDA();
  //SDA_IN();
#ifdef DEBUG
  fprintf(logfile,"--- stop_i2c --- END\n");
  printf("--- stop_i2c --- END\n");
#endif
} /* end stop_i2c */
/*****************************************************/

/*********  Routine: Write of one byte ************/
void TI2CLPT1::Write(unsigned char data)
{
  int i=0;
  int noack;
#ifdef DEBUG
  fprintf(logfile,"--- wr_i2c --- BEGIN\n");
  printf("--- wr_i2c --- BEGIN\n");
#endif

  _SCL();
  for (i=0;i<8;i++) {
    if ((data & 0x80) == 0x80)
      SDA();
    else
      _SDA();

    data = data << 1;

    SCL();
    _SCL();
  } /* end for i */

  SDA(); /* Release SDA for Acknowledge */
  SCL();
  
  if(SDA_IN()==1)
    noack = 1; /*check for acknowledge: if noack=1 error*/
  else
    noack=0;


  _SCL();

  if (noack)
    throw EI2CNoAck("");
    
  //return noack;
#ifdef DEBUG
  if (noack==1) {
     printf("############## WARNING: No acknowledge !!!\n");
     fprintf(logfile,"############## WARNING: No acknowledge !!!\n");
  };
  fprintf(logfile,"--- wr_i2c --- END\n");
  printf("--- wr_i2c --- END\n");
#endif
} /* end wr_i2c */
/*****************************************************/

/******  Routine: ADDRESS FOR R/W_ (W=0, R=1) *****/
void TI2CLPT1::AddressRW(unsigned char addr,unsigned char rw)
{
  addr = (addr <<1) + rw;
  Start();
  Write(addr);
}
/***************************************************************************/

/******  Routine: Read 1 or more bytes *****/
unsigned int TI2CLPT1::ReadNBytes(unsigned char addr, unsigned char nbyte)
{
  int i=0;
  unsigned int datain=0;

#ifdef DEBUG
  fprintf(logfile,"--- rd_i2c_nb --- BEGIN / addr=%d / nbyte=%d\n",addr,nbyte);
  printf("--- rd_i2c_nb --- BEGIN / addr=%d / nbyte=%d\n",addr,nbyte);
#endif

 AddressRW(addr,1); /* Address the chip for reading */


  /*  - - - - - - - - - - - - - - - - - - - - - - */
  /* set D7=low before reading from lpt port (MC) */
  SDA();
  /*  - - - - - - - - - - - - - - - - - - - - - - */
  for (i=0;i<8;i++)
	{
 	  SCL();
	datain = (datain << 1) + SDA_IN();
	_SCL();
	} /* end for i*/

if (nbyte == 2)  /* If read 2 bytes */
 {
  _SDA(); /* Assert SDA for Acknowledge 1'st byte */
  SCL();     /* 9th clock */
 _SCL();
  SDA(); /* Release SDA */;

  /*  - - - - - - - - - - - - - - - - - - - - - - */
  /* set D7=low before reading from lpt port (MC) */
  SDA();
  /*  - - - - - - - - - - - - - - - - - - - - - - */
  for (i=0;i<8;i++)
	{
	  SCL();
	datain = (datain << 1) + SDA_IN();
	_SCL();
	} /* end for i */
 } /* end if */
  SDA(); /* Release SDA for No Acknowledge */
  SCL();     /* 9th clock */
  _SCL();

  Stop();

#ifdef DEBUG
  fprintf(logfile,"--- rd_i2c_nb --- END\n");
  printf("--- rd_i2c_nb --- END\n");
#endif


  return(datain);
} /* end rd_i2c_nb */
