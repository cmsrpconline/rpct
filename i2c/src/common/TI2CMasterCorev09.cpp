#include "rpct/i2c/TI2CMasterCorev09.h"

#include <unistd.h>

#include "log4cplus/loggingmacros.h"

using namespace log4cplus;
using namespace std;

namespace rpct {

Logger TI2CMasterCorev09::logger = Logger::getInstance("TI2CMasterCorev09");
void TI2CMasterCorev09::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}

void TI2CMasterCorev09::Sleep() {
    //tbsleep(10);
    //tbsleep(1);
    usleep(10);
}

void TI2CMasterCorev09::SetPrescaleValue(unsigned short value) {
    unsigned char control = ReadControlReg();

    if ((control & BIT_EN) > 0) {
        WriteControlReg(control ^ BIT_EN);
        WritePrescaleReg(value);
        unsigned short readval = ReadPrescaleReg();
        if (value != readval) {
            LOG4CPLUS_ERROR(logger, "Could not set prescale register. Expecting " << std::hex << value << " received "
                    << readval);
            throw TException("Could not set prescale register");
        }

        WriteControlReg(control);
    } else {
        WritePrescaleReg(value);
        unsigned short readval = ReadPrescaleReg();
        if (value != readval) {
            LOG4CPLUS_ERROR(logger, "Could not set prescale register. Expecting " << std::hex << value << " received "
                    << readval);
            throw TException("Could not set prescale register");
        }
    }
}

void TI2CMasterCorev09::Write8(unsigned char address, unsigned char value) {
    LOG4CPLUS_DEBUG(logger, "TI2CMasterCorev09::Write8: address = " << hex << (int)address << " value = " << (int)value);

    // generate start command, set WR bit and write address
    WriteTransmitReg(BuildSlaveAddress(address, true));
    if (verbose_) {
        unsigned short status = ReadStatusReg();
        LOG4CPLUS_DEBUG(logger, "TI2CMasterCorev09::Write8: status = " << hex << status);
        if ((status & BIT_BUSY) != 0) {
            throw EI2C("Write8: BIT_BUSY set before starting");
        }
        if ((status & BIT_TIP) != 0) {
            throw EI2C("Write8: BIT_TIP set before starting");
        }
    }
    WriteCommandReg(BIT_STA | BIT_WR);
    Sleep();

    // receive acknoledge from slave
    if (!WaitRxAck())
        throw EI2CNoAck("Write8: Acknoledge not received from slave after sending address");

    Sleep();

    // write data to slave, set STO bit
    WriteTransmitReg(BuildSlaveData(value));
    WriteCommandReg(BIT_STO | BIT_WR);

    Sleep();

    // receive acknoledge from slave
    if (!WaitRxAck())
        throw EI2CNoAck("Write8: Acknoledge not received from slave after sending value");
}

/*unsigned char TI2CMasterCorev09::Read8(unsigned char address, unsigned char reg)
 {
 // generate start command, set WR bit and write address
 WriteTransmitReg(BuildSlaveAddress(address, true));
 WriteCommandReg(BIT_STA | BIT_WR);

 Sleep();

 // receive acknoledge from slave
 if (!WaitRxAck())
 throw EI2CNoAck("Read8: Acknoledge not received from slave after sending address");

 Sleep();

 WriteTransmitReg(BuildSlaveData(reg));
 WriteCommandReg(BIT_WR);

 // receive acknoledge from slave
 if (!WaitRxAck())
 throw EI2CNoAck("Read8: Acknoledge not received from slave after sending address");


 // generate start command, set WR bit and write address
 WriteTransmitReg(BuildSlaveAddress(address, false));
 WriteCommandReg(BIT_STA | BIT_WR);


 Sleep();

 // receive acknoledge from slave
 if (!WaitRxAck())
 throw EI2CNoAck("Read8: Acknoledge not received from slave after sending value");
 } */

unsigned char TI2CMasterCorev09::Read8(unsigned char address, bool ack) {
    LOG4CPLUS_DEBUG(logger, "TI2CMasterCorev09::Read8: address = " << hex << (int)address);

    // generate start command, set WR bit and write address
    WriteTransmitReg(BuildSlaveAddress(address, false));
    WriteCommandReg(BIT_STA | BIT_WR);

    Sleep();

    // receive acknoledge from slave
    if (!WaitRxAck()) {
        throw EI2CNoAck("Read8: Acknoledge not received from slave after sending address");
    }

    Sleep();

    // generate start command, set WR bit and write address
    WriteCommandReg(BIT_RD | (ack ? BIT_ACK : 0) | BIT_STO);
    //WriteCommandReg(BIT_RD | BIT_STO);

    Sleep();
    
    // wait for TIP to be set to 0
    if (!WaitTip()) {
        throw EI2C("Read8: TIP still up after sending read command");
    }

    // receive acknoledge from slave
    unsigned char data = ReadReceiveReg();
    LOG4CPLUS_DEBUG(logger, " data = " << hex << (int)data);
    return data;
}

bool TI2CMasterCorev09::WaitRxAck() {
    // receive acknoledge from slave
    for (int i = 0; true; i++) {
        unsigned int status = ReadStatusReg();
        if ((status & (BIT_RxACK | BIT_TIP| BIT_AL)) == 0) {
            return true;
        } else {
            //cout << "Still waiting for ack" << endl;
            if (status & BIT_AL) {
                WriteCommandReg(BIT_STO);
                throw EI2C("WaitRxAck: I2C arbitration lost");
            }
            Sleep();
            if (i == REPEAT) {
                if (status & BIT_TIP) {
                    WriteCommandReg(BIT_STO);
                    throw EI2C("WaitRxAck: I2C timeout waiting for acknowledge: transfer still in progress");
                }
                // send STOP sequence
                WriteCommandReg(BIT_STO);
                return false;
            }
        }
    }
}

bool TI2CMasterCorev09::WaitTip() {
    // receive acknoledge from slave
    for (int i = 0; true; i++) {
        unsigned int status = ReadStatusReg();
        if ((status & (BIT_TIP/*| BIT_AL*/)) == 0) {
            return true;
        } else {
            //cout << "Still waiting for end of transfer" << endl;
            /*if (status & BIT_AL) {
                //WriteCommandReg(BIT_STO);
                throw EI2C("WaitTip: I2C arbitration lost");
            }*/
            Sleep();
            if (i == REPEAT) {
                if (status & BIT_TIP) {
                    //WriteCommandReg(BIT_STO);
                    throw EI2C("WaitTip: I2C timeout waiting for acknowledge: transfer still in progress");
                }
                // send STOP sequence
                //WriteCommandReg(BIT_STO);
                return false;
            }
        }
    }
}

void TI2CMasterCorev09::WriteADD(unsigned char address, unsigned char value1, unsigned char value2) {
    LOG4CPLUS_DEBUG(logger, "TI2CMasterCorev09::WriteADD: address = " << hex << (int)address << " value1 = "
            << (int)value1 << " value2 = " << (int)value2);

    // generate start command, set WR bit and write address
    WriteTransmitReg(BuildSlaveAddress(address, true));
    WriteCommandReg(BIT_STA | BIT_WR);

    Sleep();

    if (!WaitRxAck())
        throw EI2CNoAck("WriteADD: Acknoledge not received from slave after sending address");

    Sleep();

    // write data to slave, set STO bit
    WriteTransmitReg(BuildSlaveData(value1));
    WriteCommandReg(BIT_WR);

    Sleep();

    if (!WaitRxAck())
        throw EI2CNoAck("WriteADD: Acknoledge not received from slave after sending value1");

    // write data to slave, set STO bit
    WriteTransmitReg(BuildSlaveData(value2));
    WriteCommandReg(BIT_STO | BIT_WR);

    Sleep();

    if (!WaitRxAck())
        throw EI2CNoAck("WriteADD: Acknoledge not received from slave after sending value2");

}

void TI2CMasterCorev09::ReadADD(unsigned char address, unsigned char& value1, unsigned char& value2) {
    LOG4CPLUS_DEBUG(logger, "TI2CMasterCorev09::ReadADD: address = " << hex << (int)address);

    // generate start command, set WR bit and write address
    WriteTransmitReg(BuildSlaveAddress(address, false));
    WriteCommandReg(BIT_STA | BIT_WR);

    Sleep();

    // receive acknoledge from slave
    if (!WaitRxAck())
        throw EI2CNoAck("Read8: Acknoledge not received from slave after sending address");

    Sleep();

    // generate start command, set WR bit and write address
    WriteCommandReg(BIT_RD);

    Sleep();

    // wait for TIP to be set to 0
    if (!WaitTip()) {
        throw EI2C("ReadADD: TIP still up after sending first read command");
    }
    value1 = ReadReceiveReg();
    LOG4CPLUS_DEBUG(logger, " value1 = " << hex << (unsigned int)value1);

    Sleep();

    // generate start command, set WR bit and write address
    WriteCommandReg(BIT_RD | BIT_ACK | BIT_STO);

    Sleep();

    // wait for TIP to be set to 0
    if (!WaitTip()) {
        throw EI2C("ReadADD: TIP still up after sending second read command");
    }
    value2 = ReadReceiveReg();
    LOG4CPLUS_DEBUG(logger, " value2 = " << hex << (unsigned int)value2);
}

void TI2CMasterCorev09::Write(unsigned char address, unsigned char* data, size_t count) {
    //G_Log.out() << "TI2CMasterCorev09::Write: address = " << (int)address;
    //for (int i = 0; i < count; i++)
    //  G_Log.out() << "\n   value[" << i << "] = " << (int)data[i];
    //G_Log.out() << std::endl;

    // generate start command, set WR bit and write address
    WriteTransmitReg(BuildSlaveAddress(address, true));
    WriteCommandReg(BIT_STA | BIT_WR);

    Sleep();

    if (!WaitRxAck())
        throw EI2CNoAck("Write: Acknoledge not received from slave after sending address");

    Sleep();

    for (size_t i = 0; i < (count-1); i++) {
        // write data to slave
        WriteTransmitReg(BuildSlaveData(data[i]));
        WriteCommandReg(BIT_WR);

        Sleep();

        if (!WaitRxAck())
            throw EI2CNoAck("Write: Acknoledge not received from slave after sending data");

        Sleep();
    }

    // write data to slave, set STO bit
    WriteTransmitReg(BuildSlaveData(data[count-1]));
    WriteCommandReg(BIT_STO | BIT_WR);

    Sleep();

    if (!WaitRxAck())
        throw EI2CNoAck("WriteADD: Acknoledge not received from slave after sending last value");

}

void TI2CMasterCorev09::Enable() {
    WriteControlReg(BIT_EN);
    //WriteControlReg(BIT_EN | BIT_IEN);
}

void TI2CMasterCorev09::Disable() {
    WriteControlReg(0);
}

/*
 void TI2CMasterCorev09::Write(unsigned char address, unsigned char* data, size_t count)
 {
 //G_Log.out() << "TI2CMasterCorev09::Write: address = " << (int)address;
 //for (int i = 0; i < count; i++)
 //  G_Log.out() << "\n   value[" << i << "] = " << (int)data[i];
 //G_Log.out() << std::endl;

 // generate start command, set WR bit and write address
 WriteTransmitReg(BuildSlaveAddress(address, true));
 WriteCommandReg(BIT_STA | BIT_WR);

 Sleep();

 if (!WaitRxAck())
 throw EI2CNoAck("Write: Acknoledge not received from slave after sending address");

 Sleep();

 
 for (int i = 0; i < count; i++) {
 // write data to slave
 WriteTransmitReg(BuildSlaveData(data[i]));
 WriteCommandReg(BIT_WR);

 Sleep();

 if (!WaitRxAck())
 throw EI2CNoAck("Write: Acknoledge not received from slave after sending data");

 Sleep();
 }


 WriteCommandReg(BIT_STO);
 }
 */

}
