//---------------------------------------------------------------------------

#ifndef TIII2CMasterCoreH
#define TIII2CMasterCoreH
//---------------------------------------------------------------------------

#include "TI2CMasterCore.h"
#include "tb_ii.h"


namespace rpct {
    
class TIII2CMasterCore: public TI2CMasterCore {
private:
  TIIDevice& Owner;
  IDevice::TID AreaID;
public:
  TIII2CMasterCore(TIIDevice& owner, IDevice::TID area_id)
    : Owner(owner), AreaID(area_id) {}
  virtual ~TIII2CMasterCore() {}

  //
  // access to registers
  //

  // Prescale Register
  virtual unsigned short ReadPrescaleReg()
    {
      unsigned short value = 0;

      Owner.readArea(AreaID, (void*)&value, 0x0, 1);
      return value;
    }

  virtual void WritePrescaleReg(unsigned short value)
    {
      Owner.writeArea(AreaID, (void*)&value, 0x0, 1);
    }

  virtual unsigned char ReadControlReg()
    {
      unsigned short value = 0;

      Owner.readArea(AreaID, (void*)&value, 0x1, 1);
      return value;
    }

  virtual void WriteControlReg(unsigned char value)
    {
      unsigned short svalue = value;

      Owner.writeArea(AreaID, (void*)&svalue, 0x1, 1);
    }

  virtual unsigned short ReadTransmitCommandReg()
    {
      unsigned short value = 0;

      Owner.readArea(AreaID, (void*)&value, 0x2, 1);
      return value;
    }

  virtual void WriteTransmitCommandReg(unsigned short value)
    {
      Owner.writeArea(AreaID, (void*)&value, 0x2, 1);
    }


  virtual unsigned short ReadReceiveStatusReg() 
    {
      unsigned short value = 0;

      Owner.readArea(AreaID, (void*)&value, 0x3, 1);
      return value;
    }

};


}

#endif
