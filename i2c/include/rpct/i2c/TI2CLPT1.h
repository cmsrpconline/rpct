//---------------------------------------------------------------------------

#ifndef TI2CLPT1H
#define TI2CLPT1H
//---------------------------------------------------------------------------

#include "TI2CInterface.h"
#include "TDLPortIO.h"


class TI2CLPT1: public TI2CInterface {
private:
  int Repeat;
  TDLPortIO *DLPortIO;

  static const int LPT1 = 0x378; /* Address of LPT1 */
     
  void outp(int base, int value)
    {
      DLPortIO->Port[base]=value;
    }

  char inp(int base)
    {
      return DLPortIO->Port[base];
    }
public:
  TI2CLPT1(): Repeat(10)
    {
      DLPortIO = new TDLPortIO(NULL);

      // Open the DriverLINX driver
      DLPortIO->OpenDriver();

      if (!DLPortIO->ActiveHW)
        throw TException("Could not open the DriverLINX driver.");
    }
  virtual ~TI2CLPT1() {}

  // lines manipulation
  void SCL();
  void _SCL();
  void SDA();
  void _SDA();
  int SDA_IN();

    
  void Start();
  void Stop();
  void Write(unsigned char);
  void AddressRW(unsigned char addr,unsigned char rw);
  unsigned int ReadNBytes(unsigned char addr, unsigned char nbyte);

  virtual void Write8 (unsigned char address, unsigned char value)
    {
      try {
        AddressRW(address, 0);
        Write(value);
        Stop();
      }
      catch(EI2CNoAck&) {
        Stop();
        throw;
      };
    }
  virtual unsigned char Read8 (unsigned char address)
    {
      throw EI2C("Not implemented Read8");
    }
  /*virtual void Write (unsigned char address, char * data, size_t  count)  = 0;
  virtual void Write32 (unsigned char address, unsigned long int  value)  = 0;
  virtual void Write16 (unsigned char address, unsigned short int value)  = 0;

  virtual void Read (unsigned char address, char * data, size_t count)  = 0;
  virtual unsigned long  Read32 (unsigned char address)  = 0;
  virtual unsigned short Read16 (unsigned char address)  = 0;*/
};






#endif
