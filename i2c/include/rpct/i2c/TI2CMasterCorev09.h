//---------------------------------------------------------------------------

#ifndef TI2CMasterCorev09H
#define TI2CMasterCorev09H
//---------------------------------------------------------------------------

#include "TI2CInterface.h"   
#include <log4cplus/logger.h>

namespace rpct {

class TI2CMasterCorev09 : public TI2CInterface {
protected:
    static log4cplus::Logger logger;
    static const int REPEAT = 20;
    bool WaitRxAck();
    bool WaitTip();
    void Sleep();
    bool verbose_;
public:
    static void setupLogger(std::string namePrefix);
    TI2CMasterCorev09() : verbose_(false) {
    }
    
    TI2CMasterCorev09(bool verbose) : verbose_(verbose) {
    }

    virtual ~TI2CMasterCorev09() {
    }

    //
    // access to registers
    //

    // Prescale Register
    virtual unsigned short ReadPrescaleReg() = 0;
    virtual void WritePrescaleReg(unsigned short) = 0;

    enum {BIT_EN = 0x80, BIT_IEN = 0x40};
    virtual unsigned char ReadControlReg() = 0;
    virtual void WriteControlReg(unsigned char) = 0;

    // Transmit and Command Registers
    enum {
        BIT_STA = 0x80,
        BIT_STO = 0x40,
        BIT_RD = 0x20,
        BIT_WR = 0x10,
        BIT_ACK = 0x08,
        BIT_IACK = 0x01
    };

    unsigned short BuildSlaveAddress(unsigned char addr, bool Write) {
        unsigned short usaddr = addr;
        return ((usaddr << 1) | (Write ? 0 : 1));// << 8;
    }

    unsigned short BuildSlaveData(unsigned char data) {
        unsigned short usdata = data;
        return usdata;// << 8;
    }

    virtual void WriteTransmitReg(unsigned short) = 0;
    virtual unsigned short ReadReceiveReg() = 0;

    // Status Registers
    enum {
        BIT_RxACK = 0x80,
        BIT_BUSY = 0x40,
        BIT_AL = 0x20,
        BIT_TIP = 0x02,
        BIT_IF = 0x01
    };

    virtual unsigned short ReadStatusReg() = 0;

    virtual void WriteCommandReg(unsigned short) = 0;

    //
    // specific functions
    //
    virtual void SetPrescaleValue(unsigned short value);
    virtual void Reset() {
        throw EI2C("Reset not implemented");
    }

    virtual void Write8(unsigned char address, unsigned char value);

    virtual unsigned char Read8(unsigned char address, bool ack = true);
    //virtual unsigned char Read8(unsigned char address, unsigned char reg);

    virtual void WriteADD(unsigned char address, unsigned char value1, unsigned char value2);
    virtual void ReadADD(unsigned char address, unsigned char& value1, unsigned char& value2);

    virtual void Write(unsigned char address, unsigned char* data, size_t count);
    
    virtual void Enable();
    
    virtual void Disable();
};

}

#endif
