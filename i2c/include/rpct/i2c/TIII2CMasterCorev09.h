//---------------------------------------------------------------------------

#ifndef TIII2CMasterCorev09H
#define TIII2CMasterCorev09H
//---------------------------------------------------------------------------

#include "TI2CMasterCorev09.h"
#include "tb_ii.h"

namespace rpct {

class TIII2CMasterCorev09 : public TI2CMasterCorev09 {
private:
    TIIDevice& Owner;
    IDevice::TID AreaID;
public:
    TIII2CMasterCorev09(TIIDevice& owner, IDevice::TID area_id) :
        Owner(owner), AreaID(area_id) {
    }
    
    TIII2CMasterCorev09(TIIDevice& owner, IDevice::TID area_id, bool verbose) :
        TI2CMasterCorev09(verbose), Owner(owner), AreaID(area_id) {
    }

    virtual ~TIII2CMasterCorev09() {
    }

    //
    // access to registers
    //

    // Prescale Register
    virtual unsigned short ReadPrescaleReg() {        
        unsigned short value = 0;

        Owner.readArea(AreaID, (void*)&value, 0x0, 1);
        
        LOG4CPLUS_DEBUG(logger, "ReadPrescaleReg: value = " << std::hex << value);
        return value;
    }

    virtual void WritePrescaleReg(unsigned short value) {
        LOG4CPLUS_DEBUG(logger, "WritePrescaleReg: value = " << std::hex << value);
        Owner.writeArea(AreaID, (void*)&value, 0x0, 1);
    }

    virtual unsigned char ReadControlReg() {
        unsigned short value = 0;

        Owner.readArea(AreaID, (void*)&value, 0x1, 1);
        LOG4CPLUS_DEBUG(logger, "ReadControlReg: value = " << std::hex << value);
        return value;
    }

    virtual void WriteControlReg(unsigned char value) {
        unsigned short svalue = value;

        LOG4CPLUS_DEBUG(logger, "WriteControlReg: value = " << std::hex << svalue);
        Owner.writeArea(AreaID, (void*)&svalue, 0x1, 1);
    }

    virtual void WriteTransmitReg(unsigned short value) {
        LOG4CPLUS_DEBUG(logger, "WriteTransmitReg: value = " << std::hex << value);
        unsigned short v = value << 8;
        Owner.writeArea(AreaID, (void*)&v, 0x2, 1);
    }

    virtual unsigned short ReadReceiveReg() {
        unsigned short value = 0;

        Owner.readArea(AreaID, (void*)&value, 0x2, 1);
        LOG4CPLUS_DEBUG(logger, "ReadReceiveReg: value = " << std::hex << value);
        return value;
    }

    virtual unsigned short ReadStatusReg() {
        unsigned short value = 0;

        Owner.readArea(AreaID, (void*)&value, 0x3, 1);
        LOG4CPLUS_DEBUG(logger, "ReadStatusReg: value = " << std::hex << value);
        return value;
    }

    virtual void WriteCommandReg(unsigned short value) {
        LOG4CPLUS_DEBUG(logger, "WriteCommandReg: value = " << std::hex << value);
        unsigned short v = value << 8;
        Owner.writeArea(AreaID, (void*)&v, 0x3, 1);
    }
};

}

#endif
