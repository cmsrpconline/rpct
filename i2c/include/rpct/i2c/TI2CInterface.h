#ifndef I2CINTERFACEH
#define I2CINTERFACEH

#include "tb_std.h"

namespace rpct {

class EI2C : public TException {
public:
    EI2C(const std::string& msg) throw()
	: TException( "I2C Error" + msg ) {}
    EXCFASTCALL virtual ~EI2C() throw() {}
};


class EI2COpen : public EI2C {
public:
    EI2COpen(const std::string& msg) throw()
	: EI2C(": could not open " + msg) {}
    EXCFASTCALL virtual ~EI2COpen() throw() {}
};

class EI2CHardware : public EI2C {
public:
    EI2CHardware(const std::string& msg) throw()
	: EI2C(": hardware error " + msg) {}
    EXCFASTCALL virtual ~EI2CHardware() throw() {}
};

class EI2CWrite : public EI2C {
public:
    EI2CWrite(const std::string& msg) throw()
	: EI2C(": write error " + msg) {}
    EXCFASTCALL virtual ~EI2CWrite() throw() {}
};

class EI2CRead : public EI2C {
public:
    EI2CRead(const std::string& msg) throw()
	: EI2C(": read error " + msg) {}
    EXCFASTCALL virtual ~EI2CRead() throw() {}
};


class EI2CNoAck : public EI2C {
public:
    EI2CNoAck(const std::string& msg) throw()
	: EI2C(": no acknowledge " + msg) {}
    EXCFASTCALL virtual ~EI2CNoAck() throw() {}
};


class TI2CInterface {  
public:
    TI2CInterface() {}
    virtual ~TI2CInterface() {}

    virtual void Write8(unsigned char address, unsigned char value) = 0;
    virtual unsigned char Read8(unsigned char address, bool ack = true) = 0;

    virtual void WriteADD(unsigned char address, unsigned char value1,
                        unsigned char value2) = 0;   
    virtual void ReadADD(unsigned char address, unsigned char& value1,
                        unsigned char& value2) = 0;

    virtual void Write(unsigned char address, unsigned char* data, size_t count) = 0;
};

}

#endif
