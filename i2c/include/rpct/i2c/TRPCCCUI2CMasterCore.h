//---------------------------------------------------------------------------

#ifndef TRPCCCUI2CMasterCoreH
#define TRPCCCUI2CMasterCoreH
//---------------------------------------------------------------------------

#include <stdint.h>
#include "TI2CMasterCorev09.h"
//#include "TRPCCCU.h"
#include "rpct/lboxaccess/FecManager.h"
#include "rpct/lboxaccess/CCUException.h"

#include "log4cplus/loggingmacros.h"

namespace rpct {

class TRPCCCUI2CMasterCore : public TI2CMasterCorev09 {
private:
    //TRPCCCU& RPCCCU;
    StdLinkBoxAccess& LBox;
    uint32_t PrescaleLoAddr;
    uint32_t PrescaleHiAddr;
    uint32_t ControlAddr;
    uint32_t TransmitReceiveAddr;
    uint32_t CommandStatusAddr;
    bool ByteAccess;
public:
    TRPCCCUI2CMasterCore(StdLinkBoxAccess& lbox, unsigned int prescaleLoAddr,
                         unsigned int prescaleHiAddr, unsigned int controlAddr,
                         uint32_t transmitReceiveAddr, uint32_t commandStatusAddr,
                         bool byteAccess)
            : LBox(lbox), PrescaleLoAddr(prescaleLoAddr), PrescaleHiAddr(prescaleHiAddr),
            ControlAddr(controlAddr),
            TransmitReceiveAddr(transmitReceiveAddr), CommandStatusAddr(commandStatusAddr),
            ByteAccess(byteAccess)
    {
        //G_Log.out() << "TRPCCCUI2CMasterCore: ByteAccess  = " << (ByteAccess ? "true" : "false") << std::endl;
    }

    virtual ~TRPCCCUI2CMasterCore() {}



    //
    // access to registers
    //
    
    virtual unsigned short memoryRead(uint32_t address);

    virtual void memoryWrite(uint32_t address, unsigned short data);
    
    virtual unsigned short memoryRead16(uint32_t address);
    
    virtual void memoryWrite16(uint32_t address, unsigned short data) ;

    // Prescale Register
    virtual unsigned short ReadPrescaleReg()
    {
        if (ByteAccess) {
            int hi = memoryRead(PrescaleHiAddr);
            hi = (hi << 8) & 0xff00;
            int lo = memoryRead(PrescaleLoAddr) & 0xff;
            return hi | lo;
        }
        else {
            int hi = memoryRead16(PrescaleHiAddr);
            hi = (hi << 8) & 0xff00;
            int lo = memoryRead16(PrescaleLoAddr) & 0xff;
            return hi | lo;
        }
    }

    virtual void WritePrescaleReg(unsigned short value)
    {
        if (ByteAccess) {
            memoryWrite(PrescaleLoAddr, value & 0xff);
            memoryWrite(PrescaleHiAddr, (value >> 8) & 0xff);
        }
        else {
            memoryWrite16(PrescaleLoAddr, value << 8);
            memoryWrite16(PrescaleHiAddr, value);
        }
    }

    virtual unsigned char ReadControlReg()
    {
        if (ByteAccess) {
            return memoryRead(ControlAddr);
        }
        else {
            return memoryRead16(ControlAddr);
        }
    }

    virtual void WriteControlReg(unsigned char value)
    {
        unsigned short svalue = value;
        if (ByteAccess) {
            memoryWrite(ControlAddr, svalue);
        }
        else {
            memoryWrite16(ControlAddr, svalue << 8);
        }
    }

    virtual void WriteTransmitReg(unsigned short value)
    {
        if (ByteAccess) {
            memoryWrite(TransmitReceiveAddr, value);
        }
        else {
            memoryWrite16(TransmitReceiveAddr, value << 8);
        }
    }

    virtual unsigned short ReadReceiveReg()
    {
        unsigned short val;
        if (ByteAccess) {
            val = memoryRead(TransmitReceiveAddr);
        }
        else {
            val = memoryRead16(TransmitReceiveAddr);
        }
        LOG4CPLUS_DEBUG(logger, "ReadReceiveReg=" << std::hex << val);
        return val;
    }

    virtual unsigned short ReadStatusReg()
    {
        unsigned short val;
        if (ByteAccess) {
            val = memoryRead(CommandStatusAddr);
        }
        else {
            val = memoryRead16(CommandStatusAddr);
        }
        //LOG4CPLUS_DEBUG(logger, "ReadStatusReg=" << val);
        return val;
    }

    virtual void WriteCommandReg(unsigned short value)
    {
        if (ByteAccess) {
            memoryWrite(CommandStatusAddr, value);
        }
        else {
            memoryWrite16(CommandStatusAddr, value << 8);
        }
    }


};

}

#endif
