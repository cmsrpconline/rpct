#include "rpct/FebConnectivityTest/FebConfiguration.h"

#include <stdint.h>

#include "rpct/hwd/DeviceConfiguration.h"
#include "rpct/hwd/StandaloneDeviceConfiguration.h"

#include "rpct/FebConnectivityTest/LinkBoard.h"

namespace rpct {
namespace fct {

FebConfiguration::FebConfiguration(hwd::System & _system
                                   , hwd::integer_type _id
                                   , hwd::DeviceType const & _devicetype
                                   , hwd::DeviceConfiguration const & _properties)
    : hwd::Device(_system, _id, _devicetype, _properties)
{
    hwd::Parameter const & _roll_selection_selected
        = getDeviceType().getParameter("RollSelectionSelected", 0);
    if (_properties.hasParameterSetting(_roll_selection_selected))
        {
            std::vector< ::uint32_t> _selected;
            _properties.getParameterSetting(_roll_selection_selected, _selected);
            for (std::vector< ::uint32_t>::const_iterator _roll = _selected.begin()
                     ; _roll != _selected.end() ; ++_roll)
                roll_selection_.select(*_roll);
        }
    hwd::Parameter const & _roll_selection_masked
        = getDeviceType().getParameter("RollSelectionMasked", 0);
    if (_properties.hasParameterSetting(_roll_selection_masked))
        {
            std::vector< ::uint32_t> _masked;
            _properties.getParameterSetting(_roll_selection_masked, _masked);
            for (std::vector< ::uint32_t>::const_iterator _roll = _masked.begin()
                     ; _roll != _masked.end() ; ++_roll)
                roll_selection_.mask(*_roll);
        }
    hwd::Parameter const & _count_duration_type = getDeviceType().getParameter("CountDuration", 0);
    if (_properties.hasParameterSetting(_count_duration_type))
        {
            ::int64_t _duration(0);
            _properties.getParameterSetting(_count_duration_type, _duration);
            count_duration_.set(0, _duration * rpct::fct::LinkBoard::nanoseconds_per_bx_);
        }
    hwd::Parameter const & _vth = getDeviceType().getParameter("VTh", 0);
    if (_properties.hasParameterSetting(_vth))
        _properties.getParameterSetting(_vth, vth_);
    hwd::Parameter const & _vmon = getDeviceType().getParameter("VMon", 0);
    if (_properties.hasParameterSetting(_vmon))
        _properties.getParameterSetting(_vmon, vmon_);
    hwd::Parameter const & _auto_correct = getDeviceType().getParameter("AutoCorrect", 0);
    if (_properties.hasParameterSetting(_auto_correct))
        _properties.getParameterSetting(_auto_correct, auto_correct_);
    hwd::Parameter const & _include_disabled = getDeviceType().getParameter("IncludeDisabled", 0);
    if (_properties.hasParameterSetting(_include_disabled))
        _properties.getParameterSetting(_include_disabled, include_disabled_);

    hwd::Parameter const & _snapshot_name = getDeviceType().getParameter("SnapshotName", 0);
    if (_properties.hasParameterSetting(_snapshot_name))
        _properties.getParameterSetting(_snapshot_name, snapshot_name_);
}

void FebConfiguration::registerDeviceTypeHandler(hwd::System & _system)
{
    _system.registerDeviceTypeHandler<FebConfiguration>("FebConfiguration");
}

hwd::DeviceType & FebConfiguration::registerDeviceType(hwd::System & _system)
{
    hwd::DeviceType & _febconfigurationtype
        = _system.registerDeviceType("FebConfiguration");

    hwd::ParameterType const & _roll_selection_selected_type
        =  _system.registerParameterType<std::vector< ::uint32_t> >("RollSelectionSelected");
    _febconfigurationtype.registerParameter(_roll_selection_selected_type, 0, hwd::Parameter::is_property_);
    hwd::ParameterType const & _roll_selection_masked_type
        =  _system.registerParameterType<std::vector< ::uint32_t> >("RollSelectionMasked");
    _febconfigurationtype.registerParameter(_roll_selection_masked_type, 0, hwd::Parameter::is_property_);
    hwd::ParameterType const & _count_duration_type
        =  _system.registerParameterType< ::int64_t>("CountDuration");
    _febconfigurationtype.registerParameter(_count_duration_type, 0, hwd::Parameter::is_property_);

    hwd::ParameterType const & _vth_type
        =  _system.registerParameterType<unsigned int>("VTh");
    _febconfigurationtype.registerParameter(_vth_type, 0, hwd::Parameter::is_property_);
    hwd::ParameterType const & _vmon_type
        =  _system.registerParameterType<unsigned int>("VMon");
    _febconfigurationtype.registerParameter(_vmon_type, 0, hwd::Parameter::is_property_);

    hwd::ParameterType const & _auto_correct_type
        =  _system.registerParameterType<bool>("AutoCorrect");
    _febconfigurationtype.registerParameter(_auto_correct_type, 0, hwd::Parameter::is_property_);
    hwd::ParameterType const & _include_disabled_type
        =  _system.registerParameterType<bool>("IncludeDisabled");
    _febconfigurationtype.registerParameter(_include_disabled_type, 0, hwd::Parameter::is_property_);

    hwd::ParameterType const & _snapshot_name_type
        =  _system.registerParameterType<std::string>("SnapshotName");
    _febconfigurationtype.registerParameter(_snapshot_name_type, 0, hwd::Parameter::is_property_);

    return _febconfigurationtype;
}

FebConfiguration & FebConfiguration::registerDevice(hwd::System & _system
                                                    , rpct::tools::RollSelection const &  _roll_selection
                                                    , rpct::tools::Time const & _count_duration
                                                    , unsigned int _vth, unsigned int _vmon
                                                    , bool _auto_correct
                                                    , bool _include_disabled
                                                    , std::string const & _snapshot_name)
{
    hwd::DeviceType const & _febconfigurationtype = _system.getDeviceType("FebConfiguration");
    hwd::StandaloneDeviceConfiguration _properties
        = _system.getHardwareStorage().registerDeviceTypeConfiguration(_system, _febconfigurationtype);


    {
        std::vector< ::uint32_t> _selected(_roll_selection.getSelected().begin(), _roll_selection.getSelected().end());
        _properties.registerParameterSetting(_febconfigurationtype.getParameter("RollSelectionSelected", 0), _selected);
    }
    {
        std::vector< ::uint32_t> _masked(_roll_selection.getMasked().begin(), _roll_selection.getMasked().end());
        _properties.registerParameterSetting(_febconfigurationtype.getParameter("RollSelectionMasked", 0), _masked);
    }

    {
        ::int64_t _duration = _count_duration.seconds() * rpct::fct::LinkBoard::bx_per_second_
            + _count_duration.nanoseconds() / rpct::fct::LinkBoard::nanoseconds_per_bx_;
        _properties.registerParameterSetting(_febconfigurationtype.getParameter("CountDuration", 0), _duration);
    }

    _properties.registerParameterSetting(_febconfigurationtype.getParameter("VTh", 0), _vth);
    _properties.registerParameterSetting(_febconfigurationtype.getParameter("VMon", 0), _vmon);

    _properties.registerParameterSetting(_febconfigurationtype.getParameter("AutoCorrect", 0), _auto_correct);
    _properties.registerParameterSetting(_febconfigurationtype.getParameter("IncludeDisabled", 0), _include_disabled);

    _properties.registerParameterSetting(_febconfigurationtype.getParameter("SnapshotName", 0), _snapshot_name);

    hwd::Device & _febconfiguration = _system.registerDevice(_febconfigurationtype, _properties);

    return dynamic_cast<FebConfiguration &>(_febconfiguration);
}

} // namespace fct
} // namespace rpct
