#include "rpct/FebConnectivityTest/FebSystem.h"

#include <stdexcept>

#include "rpct/hwd/Device.h"
#include "rpct/hwd/DataType.h"
#include "rpct/hwd/StandaloneDeviceConfiguration.h"

#include "rpct/FebConnectivityTest/LinkBox.h"
#include "rpct/FebConnectivityTest/LinkBoard.h"
#include "rpct/FebConnectivityTest/ControlBoard.h"
#include "rpct/FebConnectivityTest/FebDistributionBoard.h"
#include "rpct/FebConnectivityTest/FebBoard.h"
#include "rpct/FebConnectivityTest/FebPart.h"
#include "rpct/FebConnectivityTest/FebChip.h"
#include "rpct/FebConnectivityTest/FebConnector.h"

#include "rpct/FebConnectivityTest/FebConfiguration.h"
#include "rpct/FebConnectivityTest/FebConnectivityTest.h"
#include "rpct/FebConnectivityTest/FebThresholdScan.h"

namespace rpct {
namespace fct {

FebSystem::FebSystem(hwd::HardwareStorage & _hardwarestorage
                     , std::string const & _name)
    : hwd::System(_hardwarestorage, _name)
    , tower_("DefaultTower")
    , linkbox_type_(0)
    , febconfiguration_type_(0)
    , febconnectivitytest_type_(0)
    , febthresholdscan_type_(0)
{
    LinkBox::registerDeviceTypeHandler(*this);
    LinkBoard::registerDeviceTypeHandler(*this);
    ControlBoard::registerDeviceTypeHandler(*this);
    FebDistributionBoard::registerDeviceTypeHandler(*this);
    FebBoard::registerDeviceTypeHandler(*this);
    FebPart::registerDeviceTypeHandler(*this);
    FebChip::registerDeviceTypeHandler(*this);
    FebConnector::registerDeviceTypeHandler(*this);

    FebConfiguration::registerDeviceTypeHandler(*this);
    FebConnectivityTest::registerDeviceTypeHandler(*this);
    FebThresholdScan::registerDeviceTypeHandler(*this);
}

std::vector<LinkBox *> FebSystem::getLinkBoxes()
{
    std::vector<hwd::Device *> const & _devices = getDevices(getLinkBoxType());
    std::vector<LinkBox *> _linkboxes;
    _linkboxes.reserve(_devices.size());
    for (std::vector<hwd::Device *>::const_iterator _device = _devices.begin()
             ; _device != _devices.end() ; ++_device)
        _linkboxes.push_back(dynamic_cast<LinkBox *>(*_device));
    return _linkboxes;
}

FebConfiguration & FebSystem::getFebConfiguration()
{
    std::vector<hwd::Device *> const & _devices = getDevices(getFebConfigurationType());
    if (_devices.empty())
        throw std::runtime_error("Could not find requested FebConfiguration");
    return dynamic_cast<FebConfiguration &>(*(_devices.front()));
}

FebConnectivityTest & FebSystem::getFebConnectivityTest()
{
    std::vector<hwd::Device *> const & _devices = getDevices(getFebConnectivityTestType());
    if (_devices.empty())
        throw std::runtime_error("Could not find requested FebConnectivityTest");
    return dynamic_cast<FebConnectivityTest &>(*(_devices.front()));
}

FebThresholdScan & FebSystem::getFebThresholdScan()
{
    std::vector<hwd::Device *> const & _devices = getDevices(getFebThresholdScanType());
    if (_devices.empty())
        throw std::runtime_error("Could not find requested FebThresholdScan");
    return dynamic_cast<FebThresholdScan &>(*(_devices.front()));
}

void FebSystem::registerSystem(std::string const & _tower)
{
    tower_ = _tower;

    // register DeviceType
    hwd::DeviceType & _febsystemtype = registerDeviceType("FebSystem");

    hwd::ParameterType const & _towertype = registerParameterType<std::string>("Tower");
    _febsystemtype.registerParameter(_towertype, 0, hwd::Parameter::is_property_);

    // register Properties
    hwd::StandaloneDeviceConfiguration _properties
        = getHardwareStorage().registerDeviceTypeConfiguration(*this, _febsystemtype);

    _properties.registerParameterSetting(_febsystemtype.getParameter("Tower", 0), _tower);

    // register System
    getHardwareStorage().registerSystem(*this, _febsystemtype, _properties);

    LinkBox::registerDeviceType(*this);
    LinkBoard::registerDeviceType(*this);
    ControlBoard::registerDeviceType(*this);
    FebDistributionBoard::registerDeviceType(*this);
    FebBoard::registerDeviceType(*this);
    FebPart::registerDeviceType(*this);
    FebChip::registerDeviceType(*this);
    FebConnector::registerDeviceType(*this);

    FebConfiguration::registerDeviceType(*this);
    FebConnectivityTest::registerDeviceType(*this);
    FebThresholdScan::registerDeviceType(*this);
}

void FebSystem::loadSystem()
{
    getHardwareStorage().loadSystem(*this);
}

void FebSystem::iconfigure(hwd::DeviceFlagsMask const & _mask)
{
    std::vector<hwd::Device *> const & _linkboxes = getDevices(getLinkBoxType());
    for (std::vector<hwd::Device *>::const_iterator _linkbox = _linkboxes.begin()
             ; _linkbox != _linkboxes.end() ; ++_linkbox)
        (*_linkbox)->configure(_mask);
}

void FebSystem::iconfigure(hwd::Configuration const & _configuration, hwd::DeviceFlagsMask const & _mask)
{
    std::vector<hwd::Device *> const & _linkboxes = getDevices(getLinkBoxType());
    for (std::vector<hwd::Device *>::const_iterator _linkbox = _linkboxes.begin()
             ; _linkbox != _linkboxes.end() ; ++_linkbox)
        (*_linkbox)->configure(_configuration, _mask);
}

void FebSystem::imonitor(hwd::DeviceFlagsMask const & _mask)
{
    std::vector<hwd::Device *> const & _linkboxes = getDevices(getLinkBoxType());
    for (std::vector<hwd::Device *>::const_iterator _linkbox = _linkboxes.begin()
             ; _linkbox != _linkboxes.end() ; ++_linkbox)
        (*_linkbox)->monitor(_mask);
}

void FebSystem::imonitor(hwd::Observables const & _observables, hwd::DeviceFlagsMask const & _mask)
{
    std::vector<hwd::Device *> const & _linkboxes = getDevices(getLinkBoxType());
    for (std::vector<hwd::Device *>::const_iterator _linkbox = _linkboxes.begin()
             ; _linkbox != _linkboxes.end() ; ++_linkbox)
        (*_linkbox)->monitor(_observables, _mask);
}

void FebSystem::iassume(hwd::Configuration const & _configuration)
{
    std::vector<hwd::Device *> const & _linkboxes = getDevices(getLinkBoxType());
    for (std::vector<hwd::Device *>::const_iterator _linkbox = _linkboxes.begin()
             ; _linkbox != _linkboxes.end() ; ++_linkbox)
        (*_linkbox)->assume(_configuration);
}

void FebSystem::iassume(hwd::DeviceConfiguration const & _deviceconfiguration)
{
    hwd::Parameter const & _tower = getDeviceType().getParameter("Tower", 0);
    if (_deviceconfiguration.hasParameterSetting(_tower))
        _deviceconfiguration.getParameterSetting(_tower, tower_);

}

hwd::DeviceType & FebSystem::addDeviceType(hwd::integer_type _id
                                           , std::string const & _name)
{
    hwd::DeviceType & _devicetype = hwd::System::addDeviceType(_id, _name);
    if (_devicetype.getName() == "LinkBox")
        linkbox_type_ = &_devicetype;
    else if (_devicetype.getName() == "FebConfiguration")
        febconfiguration_type_ = &_devicetype;
    else if (_devicetype.getName() == "FebConnectivityTest")
        febconnectivitytest_type_ = &_devicetype;
    else if (_devicetype.getName() == "FebThresholdScan")
        febthresholdscan_type_ = &_devicetype;

    return _devicetype;
}

} // namespace fct
} // namespace rpct
