#ifndef rpct_FebConnectivityTest_FebConfiguration_inl_h
#define rpct_FebConnectivityTest_FebConfiguration_inl_h

#include "rpct/FebConnectivityTest/FebConfiguration.h"

namespace rpct {
namespace fct {

inline rpct::tools::RollSelection const & FebConfiguration::getRollSelection() const
{
    return roll_selection_;
}

inline rpct::tools::Time const & FebConfiguration::getCountDuration() const
{
    return count_duration_;
}

inline unsigned int FebConfiguration::getVTh() const
{
    return vth_;
}

inline unsigned int FebConfiguration::getVMon() const
{
    return vmon_;
}

inline bool FebConfiguration::getAutoCorrect() const
{
    return auto_correct_;
}

inline bool FebConfiguration::getIncludeDisabled() const
{
    return include_disabled_;
}

inline std::string const & FebConfiguration::getSnapshotName() const
{
    return snapshot_name_;
}

} // namespace fct
} // namespace rpct

#endif // rpct_FebConnectivityTest_FebConfiguration_inl_h
