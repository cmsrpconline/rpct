#ifndef rpct_FebConnectivityTest_FebConfiguration_h
#define rpct_FebConnectivityTest_FebConfiguration_h

#include "rpct/tools/Time.h"
#include "rpct/tools/RollSelection.h"

#include "rpct/hwd/fwd.h"
#include "rpct/hwd/Device.h"
#include "rpct/hwd/DeviceType.h"

#include "rpct/FebConnectivityTest/fwd.h"

#include <cstddef>
#include <string>

namespace rpct {
namespace fct {

class FebConfiguration
    : public hwd::Device
{
public:
    FebConfiguration(hwd::System & _system
                     , hwd::integer_type _id
                     , hwd::DeviceType const & _devicetype
                     , hwd::DeviceConfiguration const & _properties);

    static void registerDeviceTypeHandler(hwd::System & _system);
    static hwd::DeviceType & registerDeviceType(hwd::System & _system);
    static FebConfiguration & registerDevice(hwd::System & _system
                                             , rpct::tools::RollSelection const &  _roll_selection
                                             , rpct::tools::Time const & _count_duration
                                             , unsigned int _vth, unsigned int _vmon
                                             , bool _auto_correct
                                             , bool _include_disabled
                                             , std::string const & _snapshot_name);

    rpct::tools::RollSelection const & getRollSelection() const;
    rpct::tools::Time const & getCountDuration() const;

    unsigned int getVTh() const;
    unsigned int getVMon() const;

    bool getAutoCorrect() const;
    bool getIncludeDisabled() const;

    std::string const & getSnapshotName() const;

protected:
    rpct::tools::RollSelection roll_selection_;
    rpct::tools::Time count_duration_;

    unsigned int vth_, vmon_;

    bool auto_correct_;
    bool include_disabled_;

    std::string snapshot_name_;
};

} // namespace fct
} // namespace rpct

#include "rpct/FebConnectivityTest/FebConfiguration-inl.h"

#endif // rpct_FebConnectivityTest_FebConfiguration_h
