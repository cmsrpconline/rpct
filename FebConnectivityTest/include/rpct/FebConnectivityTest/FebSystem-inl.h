#ifndef rpct_FebConnectivityTest_FebSystem_inl_h
#define rpct_FebConnectivityTest_FebSystem_inl_h

#include "rpct/FebConnectivityTest/FebSystem.h"

namespace rpct {
namespace fct {

inline std::string const & FebSystem::getTower() const
{
    return tower_;
}

inline hwd::DeviceType const & FebSystem::getLinkBoxType() const
{
    if (linkbox_type_)
        return *linkbox_type_;
    throw std::logic_error("Attempt to get LinkBoxType of an incomplete FebSystem");
}

inline hwd::DeviceType const & FebSystem::getFebConfigurationType() const
{
    if (febconfiguration_type_)
        return *febconfiguration_type_;
    throw std::logic_error("Attempt to get FebConfigurationType of an incomplete FebSystem");
}

inline hwd::DeviceType const & FebSystem::getFebConnectivityTestType() const
{
    if (febconnectivitytest_type_)
        return *febconnectivitytest_type_;
    throw std::logic_error("Attempt to get FebConnectivityTestType of an incomplete FebSystem");
}

inline hwd::DeviceType const & FebSystem::getFebThresholdScanType() const
{
    if (febthresholdscan_type_)
        return *febthresholdscan_type_;
    throw std::logic_error("Attempt to get FebThresholdScanType of an incomplete FebSystem");
}

} // namespace fct
} // namespace rpct

#endif // rpct_FebConnectivityTest_FebSystem_inl_h
