//---------------------------------------------------------------------------

#ifndef TVMEFactoryH
#define TVMEFactoryH
//---------------------------------------------------------------------------
#include "TVMEInterface.h"

#include <map>
#include <list>

class TVMEFactory {
private:

  class TVMETypeInfo {
  public:
    virtual ~TVMETypeInfo() {}
    virtual std::string GetName() = 0;
    virtual TVMEInterface* Create() = 0;
    virtual TVMEInterface* Create(std::string unit) = 0;
  };
  typedef std::map<std::string, TVMETypeInfo*> TVMETypeMap;
  TVMETypeMap VMETypeMap;

  /*class TVMEBit3TypeInfo : public TVMETypeInfo {
  public:
    virtual std::string GetName() {
        return "Bit3";
    }
            
    virtual TVMEInterface* Create();        
    virtual TVMEInterface* Create(std::string unit);              
  };    

  class TVMEEppTypeInfo : public TVMETypeInfo {
  public:
    virtual std::string GetName() {
        return "VME-Epp";
    }
            
    virtual TVMEInterface* Create();   
    virtual TVMEInterface* Create(std::string unit);                         
  };        */

  class TVMECaenTypeInfo : public TVMETypeInfo {
  public:
    virtual std::string GetName() {
        return "CAEN";
    }
            
    virtual TVMEInterface* Create();  
    virtual TVMEInterface* Create(std::string unit);                          
  };
  
  class TVMESimulTypeInfo : public TVMETypeInfo {
  public:
    virtual std::string GetName() {
        return "VMESimul";
    }
            
    virtual TVMEInterface* Create();  
    virtual TVMEInterface* Create(std::string unit);                          
  };

  void AddVMEType(TVMETypeInfo* info)
    {
      VMETypeMap.insert(TVMETypeMap::value_type(info->GetName(), info));
    }

public:

  TVMEFactory()
    {
      //AddVMEType(new TVMEBit3TypeInfo());
      //AddVMEType(new TVMEEppTypeInfo());
      AddVMEType(new TVMECaenTypeInfo());
      AddVMEType(new TVMESimulTypeInfo());
    }
  
  ~TVMEFactory()
    {
      TVMETypeMap::iterator iType = VMETypeMap.begin();
      for (; iType != VMETypeMap.end(); ++iType)
        delete iType->second;
    }

  TVMEInterface* Create();
  TVMEInterface* Create(std::string type);
  TVMEInterface* Create(std::string type, std::string unit);
  std::string GetDefaultVMEType();
  void SetDefaultVMEType(std::string type);

  typedef std::list<std::string> TVMETypes;
  TVMETypes GetVMETypes()
    {
      TVMETypes types;     
      TVMETypeMap::iterator iType = VMETypeMap.begin();
      for (; iType != VMETypeMap.end(); ++iType)
        types.push_back(iType->first);
      return types;
    }
};

#endif
