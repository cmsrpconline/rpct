#ifdef __BORLANDC__
#include <vcl.h>
#pragma hdrstop
#endif

#include "TVMEHAL.h"


using namespace std;
using namespace HAL;


TVMEHAL::TVMEHAL(VMEBusAdapterInterface* adapter, bool owner) 
: busAdapter(adapter), adapterOwner(owner) {
}



TVMEHAL::~TVMEHAL() {
    if (adapterOwner) {
        delete busAdapter;
    }
}




void TVMEHAL::Reset() {
    busAdapter->resetBus();
}

void TVMEHAL::Write(uint32_t address, char * data, size_t  count) {
    #ifdef TB_DEBUG
        G_Debug.AddMessage("write block to adr=", TDebug::VME_OPER);
        G_Debug.AddHexCont(address, TDebug::VME_OPER);
        G_Debug.AddMessageCont( " count=", TDebug::VME_OPER );
        G_Debug.AddHexCont(count, TDebug::VME_OPER);
        G_Debug.AddMessageLn( "", TDebug::VME_OPER );
    #endif
    
    try {
        //busAdapter->writeBlock(NULL, address, count, 0x39, 4, data);
        assert((count % 4) == 0);
        //size_t words = count / 4;
        for (size_t i = 0; i < count; i += 4) {
            Write32(address + i, *(uint32_t*) (data + i));
        }
    }
    catch(BusAdapterException& e) {
        throw EVMEWrite(e.what());
    }        
}

void TVMEHAL::Write32(uint32_t address, uint32_t  value) {
    #ifdef TB_DEBUG
        G_Debug.AddMessage("write32: adr=", TDebug::VME_OPER);
        G_Debug.AddHexCont(address, TDebug::VME_OPER);
        G_Debug.AddMessageCont(" val=", TDebug::VME_OPER);
        G_Debug.AddHexCont(value, TDebug::VME_OPER);
        G_Debug.AddMessageLn("", TDebug::VME_OPER);
    #endif        
    
    try {
        busAdapter->write(NULL, address, 0x39, 4, value);
    }
    catch(BusAdapterException& e) {
        throw EVMEWrite(e.what());
    }
}

void TVMEHAL::Write16(uint32_t address, unsigned short value) {
    #ifdef TB_DEBUG
        G_Debug.AddMessage("write16: adr=", TDebug::VME_OPER);
        G_Debug.AddHexCont(address, TDebug::VME_OPER);
        G_Debug.AddMessageCont(" val=", TDebug::VME_OPER);
        G_Debug.AddHexCont(value, TDebug::VME_OPER);
        G_Debug.AddMessageLn("", TDebug::VME_OPER);
    #endif        
        
    try {
        busAdapter->write(NULL, address, 0x39, 4, value);
    }
    catch(BusAdapterException& e) {
        throw EVMEWrite(e.what());
    }
}

void TVMEHAL::Read(uint32_t address, char * data, size_t count) {
    #ifdef TB_DEBUG
        G_Debug.AddMessage("read block from adr=", TDebug::VME_OPER);
        G_Debug.AddHexCont(address, TDebug::VME_OPER);
        G_Debug.AddMessageCont( " count=", TDebug::VME_OPER );
        G_Debug.AddHexCont(count, TDebug::VME_OPER);
        G_Debug.AddMessageLn( ":", TDebug::VME_OPER );
    #endif
    try {
        //busAdapter->readBlock(NULL, address, count, 0x39, 4, data);
        assert((count % 4) == 0);
        for (size_t i = 0; i < count; i += 4) {
            *(uint32_t*) (data + i) = Read32(address + i);
        }
        
        #ifdef TB_DEBUG
            unsigned int i;
            union {
                char t[4];
                uint32_t ul;
            } u4;
            for( i=0; i<count; i+=4 ) {
                u4.t[0]=data[i];
                u4.t[1]=data[i+1];
                u4.t[2]=data[i+2];
                u4.t[3]=data[i+3];
                G_Debug.AddMessage( "read: adr=", TDebug::VME_OPER );
                G_Debug.AddHexCont( address+i, TDebug::VME_OPER );
                G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
                G_Debug.AddHexCont( u4.ul, TDebug::VME_OPER );
                G_Debug.AddMessageLn( "", TDebug::VME_OPER );
            }
        #endif
    }
    catch(BusAdapterException& e) {
        throw EVMERead(e.what());
    }        
}

uint32_t TVMEHAL::Read32(uint32_t address) {
    #ifdef TB_DEBUG
        G_Debug.AddMessage("read32: adr=", TDebug::VME_OPER);
        G_Debug.AddHexCont(address, TDebug::VME_OPER);
    #endif
    try {
        uint32_t value;
        busAdapter->read(NULL, address, 0x39, sizeof(value), &value);
        #ifdef TB_DEBUG
            G_Debug.AddMessageCont(" val=", TDebug::VME_OPER);
            G_Debug.AddHexCont(value, TDebug::VME_OPER);
            G_Debug.AddMessageLn("", TDebug::VME_OPER);
        #endif
            
        return value;
    }
    catch(BusAdapterException& e) {
        throw EVMERead(e.what());
    }
}

unsigned short TVMEHAL::Read16(uint32_t address) {
    #ifdef TB_DEBUG
        G_Debug.AddMessage("read16: adr=", TDebug::VME_OPER);
        G_Debug.AddHexCont(address, TDebug::VME_OPER);
    #endif
    
    try {
        uint32_t value;
        busAdapter->read(NULL, address, 0x39, sizeof(value), &value);
        #ifdef TB_DEBUG
            G_Debug.AddMessageCont(" val=", TDebug::VME_OPER);
            G_Debug.AddHexCont(value, TDebug::VME_OPER);
            G_Debug.AddMessageLn("", TDebug::VME_OPER);
        #endif
        return value;
    }
    catch(BusAdapterException& e) {
        throw EVMERead(e.what());
    }
}

