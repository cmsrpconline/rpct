#ifndef TVMEHAL_HH
#define TVMEHAL_HH


#include "TVMEInterface.h"
#include "tb_std.h"
#include "hal/VMEBusAdapterInterface.hh"


class TVMEHAL : public TVMEInterface {
private:
    HAL::VMEBusAdapterInterface* busAdapter;
    bool adapterOwner;
    HAL::DeviceIdentifier* deviceId;
public:
    TVMEHAL(HAL::VMEBusAdapterInterface*, bool owner);
    virtual ~TVMEHAL();
    
	virtual void Reset();
    
    virtual void Write(uint32_t address, char * data, size_t  count);
    
    virtual void Write32(uint32_t address, uint32_t  value);
    
    virtual void Write16(uint32_t address, unsigned short value);

    virtual void Read(uint32_t address, char * data, size_t count);
    
    virtual uint32_t  Read32(uint32_t address);
    
    virtual unsigned short Read16(uint32_t address);
};



#endif
