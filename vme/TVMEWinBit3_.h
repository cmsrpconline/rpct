#ifndef TVMEWINBIT3_HH
#define TVMEWINBIT3_HH



#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


#define BT973
#include "btapi.h"


#include <string.h>
#include "TVMEInterface.h"
#include "tb_std.h"


class TVMEWinBit3 : public TVMEInterface {

	// Device descriptor 
	bt_desc_t	Btd;   

	// Device name 
    char		DevName[BT_MAX_DEV_NAME]; 

	
	const int	BUF_SIZE;
	char*		Buffer;

     // !!!!!!!! Bit3 24-bit data bus error !!!!!!!!!
    const uint32_t VME_DATA_MASK;

	// returns NULL if no error, or description of the error if available
	char* CheckBit3Status(bt_error_t status);

	void AutoConfigure();

public:
    TVMEWinBit3(int unit = 0, bt_dev_t logdev = BT_DEV_A24);
    virtual ~TVMEWinBit3();
	virtual void Reset();
    virtual void Write (uint32_t int address, char * data, size_t  count);
    virtual void Write32 (uint32_t int address, uint32_t int  value);
    virtual void Write16 (uint32_t int address, unsigned short int value);

    virtual void Read (uint32_t int address, char * data, size_t count);
    virtual uint32_t  Read32 (uint32_t int address);
    virtual unsigned short Read16 (uint32_t int address);
};





inline 
void TVMEWinBit3::Write32 (uint32_t int address, uint32_t int value)
{
    Write(address,(char *)&value,sizeof(value));
}



inline 
void TVMEWinBit3::Write16 (uint32_t int address, unsigned short int value)
{
    Write(address,(char *)&value,sizeof(value));
}





inline
uint32_t TVMEWinBit3::Read32 (uint32_t int address)
{
    uint32_t ul;
    Read( address, (char *)&ul, sizeof(uint32_t) );
    return ul & VME_DATA_MASK;
}



inline
unsigned short TVMEWinBit3::Read16 (uint32_t int address)
{
    unsigned short us;
    Read( address, (char *)&us, sizeof(unsigned short) );
    return us;
}


#endif
