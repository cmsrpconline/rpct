#ifndef TVMEINTERFACE_HH
#define TVMEINTERFACE_HH

#include <stdint.h>
#include "tb_std.h"

class EVME : public TException {
public:
    EVME(const std::string& msg) throw()
    : TException( "VME Error" + msg )  {} 
    //EVME( std::string msg = "" ): TException( "VME Error" + msg ) {}
    EXCFASTCALL virtual ~EVME() throw() {}
};


class EVMEOpen : public EVME {
public:
    EVMEOpen(const std::string& msg) throw() 
	: EVME(": could not open " + msg) {}
    EXCFASTCALL virtual ~EVMEOpen() throw() {}
};

class EVMEHardware : public EVME {
public:
    EVMEHardware(const std::string& msg) throw()
	: EVME(": hardware error " + msg)  {}
    EXCFASTCALL virtual ~EVMEHardware() throw() {}
};

class EVMEWrite : public EVME {
public:
    EVMEWrite(const std::string& msg) throw() 
	: EVME(": write error " + msg) {}
    EXCFASTCALL virtual ~EVMEWrite() throw() {}
};

class EVMERead : public EVME {
public:
    EVMERead(const std::string& msg) throw()
	: EVME(": read error " + msg) {}
    EXCFASTCALL virtual ~EVMERead() throw() {}
};


class TVMEInterface {
public:
    enum AddressingMode {
        am16, am24, am32
    };


    TVMEInterface() {}
    virtual ~TVMEInterface() {}

	virtual void Reset() = 0;
    virtual void SetDefaultAddressingMode(AddressingMode am) = 0;

    virtual void Write(uint32_t address, char * data, size_t  count) = 0;
    virtual void Write32(uint32_t address, uint32_t  value) = 0;
    virtual void Write16(uint32_t address, unsigned short value) = 0;
    virtual void MultiWrite16(uint32_t* addresses, uint16_t* values, size_t count) = 0;
    virtual void MultiWrite32(uint32_t* addresses, uint32_t* values, size_t count) = 0;

    virtual void Read(uint32_t address, char * data, size_t count) = 0;
    virtual uint32_t  Read32(uint32_t address) = 0;
    virtual unsigned short Read16(uint32_t address) = 0;
};




#endif



