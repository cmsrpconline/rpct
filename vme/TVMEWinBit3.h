#ifndef TVMEWINBIT3_HH
#define TVMEWINBIT3_HH



#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


#include "btapi.h"


#include <string.h>
#include "TVMEInterface.h"
#include "rpct/std/ENotImplemented.h"
#include "tb_std.h"


class TVMEWinBit3 : public TVMEInterface {

	// Device descriptor 
	bt_desc_t	Btd;   

	// Device name 
        char		DevName[BT_MAX_DEV_NAME]; 

	
	const int	BUF_SIZE;
	char*		Buffer;

	// returns NULL if no error, or description of the error if available
	char* CheckBit3Status(bt_error_t status);

	void AutoConfigure();

        bool Busy;

        int MaxDMA; // if block of data is more than MaxDMA bytes
        // chop block to blocks of MaxDMA bytes
public:
    TVMEWinBit3(int unit = 0, bt_dev_t logdev = BT_DEV_A24, int max_dma = -1);
    virtual ~TVMEWinBit3();
	virtual void Reset();
    
    virtual void SetDefaultAddressingMode(AddressingMode am) {
        throw ENotImplemented("TVMEWinBit3::SetDefaultAddressingMode");
    }
    
    virtual void Write (uint32_t int address, char * data, size_t  count);
    virtual void Write32 (uint32_t int address, uint32_t int  value);
    virtual void Write16 (uint32_t int address, unsigned short int value);
    
    virtual void MultiWrite16(uint32_t* addresses, uint16_t* values, size_t count) {
        throw ENotImplemented("TVMEWinBit3::MultiWrite16");
    }
    
    virtual void MultiWrite32(uint32_t* addresses, uint32_t* values, size_t count){
        throw ENotImplemented("TVMEWinBit3::MultiWrite32");
    }

    virtual void Read (uint32_t int address, char * data, size_t count);
    virtual uint32_t  Read32 (uint32_t int address);
    virtual unsigned short Read16 (uint32_t int address);
};





inline 
void TVMEWinBit3::Write32 (uint32_t int address, uint32_t int value)
{
  #ifdef TB_DEBUG
    Write(address,(char *)&value,sizeof(value));
  #else 
    size_t amt_writen;
    bt_error_t   status;

    status = bt_write(Btd, &value, address, sizeof(uint32_t int), &amt_writen);

    if ( BT_SUCCESS != status) {

      /* check if all the message was transfered */
      if (amt_writen != sizeof(uint32_t int)) {
              throw EVMEWrite( std::string("device ") + DevName + CheckBit3Status(status) );
      }

      throw EVMEWrite( std::string("device ") + DevName + CheckBit3Status(status) );
    }

  #endif
}



inline 
void TVMEWinBit3::Write16 (uint32_t int address, unsigned short int value)
{     
  #ifdef TB_DEBUG
    Write(address,(char *)&value,sizeof(value));      
  #else 
    size_t amt_writen;
    bt_error_t   status;

    status = bt_write(Btd, &value, address, sizeof(unsigned short int), &amt_writen);

    if ( BT_SUCCESS != status) {

      /* check if all the message was transfered */
      if (amt_writen != sizeof(unsigned short int)) {
              throw EVMEWrite( std::string("device ") + DevName + CheckBit3Status(status) );
      }

      throw EVMEWrite( std::string("device ") + DevName + CheckBit3Status(status) );
    }

  #endif
}





inline
uint32_t TVMEWinBit3::Read32 (uint32_t int address)
{
    uint32_t ul;
    Read( address, (char *)&ul, sizeof(uint32_t) );
    return ul;
}



inline
unsigned short TVMEWinBit3::Read16 (uint32_t int address)
{
    unsigned short us;
    Read( address, (char *)&us, sizeof(unsigned short) );
    return us;
}


#endif
