#ifndef TVMESIMUL_HH
#define TVMESIMUL_HH


#include <stdio.h>
#include <string>

#include "tb_std.h"
#include "TVMEInterface.h"
#include "rpct/std/ENotImplemented.h"

class TVMESimul : public TVMEInterface {
private:
    char err_msg[64];
public:
    TVMESimul();
    virtual ~TVMESimul();
	
	virtual void Reset();
    
    virtual void SetDefaultAddressingMode(AddressingMode am) {
    }

    virtual void Write (uint32_t address, char * data, size_t  count);
    virtual void Write32 (uint32_t address, uint32_t  value);
    virtual void Write16 (uint32_t address, unsigned short int value);
    virtual void MultiWrite16(uint32_t* addresses, uint16_t* values, size_t count);
    virtual void MultiWrite32(uint32_t* addresses, uint32_t* values, size_t count);

    virtual void Read (uint32_t address, char * data, size_t count);
    virtual uint32_t  Read32 (uint32_t address);
    virtual unsigned short Read16 (uint32_t address);
};




inline
TVMESimul::TVMESimul() : TVMEInterface()
{         
	G_Debug.AddMessageLn("VMEOpen", TDebug::VME_OPER);
#if (__BCPLUSPLUS__ >= 0x0550)
	randomize();
#endif
}


inline
TVMESimul::~TVMESimul()
{
}

inline
void TVMESimul::Reset()
{
	G_Debug.AddMessageLn( "VMEReset", TDebug::VME_OPER );
}


inline
void TVMESimul::Write32 (uint32_t address, uint32_t value)
{
    Write(address,(char *)&value,sizeof(value));
}



inline
void TVMESimul::Write16 (uint32_t address, unsigned short int value)
{
    Write(address,(char *)&value,sizeof(value));
}





inline
uint32_t TVMESimul::Read32 (uint32_t address)
{
    uint32_t ul;
    Read( address, (char *)&ul, sizeof(uint32_t) );
    return ul;
}



inline
unsigned short TVMESimul::Read16 (uint32_t address)
{
    unsigned short us;
    Read( address, (char *)&us, sizeof(unsigned short) );
    return us;
}


#endif
