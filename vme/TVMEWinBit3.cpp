#ifdef __BORLANDC__
#include <vcl.h>
#pragma hdrstop
#endif

#include "TVMEWinBit3.h"


using namespace std;


char* TVMEWinBit3::CheckBit3Status(bt_error_t status)
{
	if (status != BT_SUCCESS) {

		char *error_desc;
        error_desc = bt_strerror(Btd, status, "", Buffer, BUF_SIZE );
		if (error_desc == NULL)
			return "error description anavailable";
		else 
			return error_desc;
    }

	return NULL;
}




void TVMEWinBit3::AutoConfigure()
{
	const unsigned A_MOD = 0x39;
    bt_error_t   status;                
	bt_devdata_t info_data;

	// check DMA address modifier
	status = bt_get_info(Btd, BT_INFO_DMA_AMOD, &info_data);
    if (BT_SUCCESS != status) {
        throw EVME( string("device ") + DevName + CheckBit3Status(status) );
    }

	if (info_data != A_MOD) {
		// set DMA addres modifier 0x39 
		status = bt_set_info(Btd, BT_INFO_DMA_AMOD, A_MOD);
		if (BT_SUCCESS != status) {
			throw EVME( string("device ") + DevName + CheckBit3Status(status) );
		} 
	}


	// check PIO address modifier
	status = bt_get_info(Btd, BT_INFO_PIO_AMOD, &info_data);
    if (BT_SUCCESS != status) {
        throw EVME( string("device ") + DevName + CheckBit3Status(status) );
    } 

	if (info_data != A_MOD) {
		// set PIO addres modifier 0x39 
		status = bt_set_info(Btd, BT_INFO_PIO_AMOD, A_MOD);
		if (BT_SUCCESS != status) {
			throw EVME( string("device ") + DevName + CheckBit3Status(status) );
		} 
	}
		
                   
    // set DMA addres modifier 0x39
    //status = bt_set_info(Btd, BT_INFO_PAUSE, TRUE);
    //if (BT_SUCCESS != status) {
    //        throw EVME( string("device ") + DevName + CheckBit3Status(status) );
    //		}


/*	// set data width 32 bits 
    status = bt_set_info(btd, BT_INFO_DATAWIDTH, BT_WIDTH_D32);
    //status = bt_set_info(btd, BT_INFO_DATAWIDTH, BT_WIDTH_ANY);
    if (BT_SUCCESS != status) {
        throw EVME( string("device ") + DevName + CheckBit3Status(status) );
    } 
*/

}




TVMEWinBit3::TVMEWinBit3(int unit, bt_dev_t logdev, int max_dma)
: TVMEInterface(), BUF_SIZE(256), Busy(false), MaxDMA(max_dma) 
{

	Buffer = new char [BUF_SIZE];
    bt_error_t   status;                   /* SBS Bit 3 API return value */

	DevName[0] = 0;

    char * tmp_name = bt_gen_name(unit, logdev, DevName, BT_MAX_DEV_NAME);
    status = bt_open(&Btd, tmp_name, BT_RDWR);

    // Open the logical device
    //status = bt_open(&Btd, bt_gen_name(unit, logdev, DevName, BT_MAX_DEV_NAME),
   //                    BT_RDWR);
    if (status != BT_SUCCESS) {
        /* It is safe to call bt_perror with any status returned from
           bt_open() or bt_close() with the device descriptor, despite
           the fact that the descriptor is not valid to do anything
           else. */
	   	throw EVMEOpen( string("device ") + DevName + CheckBit3Status(status) );
    }

    G_Debug.AddMessageLn( string("Opened vme device ") + DevName, TDebug::VME_OPER );

	AutoConfigure();
}




TVMEWinBit3::~TVMEWinBit3()
{

    bt_error_t   status;
    // Close the logical device.

    status = bt_close(Btd);
    if (status != BT_SUCCESS) {
        throw EVME( string("device ") + DevName + CheckBit3Status(status) );
    }

	delete [] Buffer;
}





void TVMEWinBit3::Reset()
{
  #ifdef BT983
    bt_error_t   status;
    // Reset the remote bus.
    status = bt_reset(Btd);
    if (status != BT_SUCCESS) {
        throw EVME( string("device ") + DevName + CheckBit3Status(status) );
    } 
   #endif
}



void TVMEWinBit3::Write (uint32_t int address, char * data, size_t count)
{
  if (MaxDMA != -1) {
    if (count > MaxDMA) {
      Write(address, data, MaxDMA);
      Write(address + MaxDMA, data + MaxDMA, count - MaxDMA);
      return;
    };
  };

  if (Busy)
    throw TException("TVMEWinBit3::Write: parallel accesss");

  Busy = true;
  try {

    #ifdef TB_DEBUG
          G_Debug.AddMessage( "VMEWrite: address=", TDebug::VME_OPER );
          G_Debug.AddHexCont( address, TDebug::VME_OPER );
          G_Debug.AddMessageCont( " count=", TDebug::VME_OPER );
          G_Debug.AddDecCont( count, TDebug::VME_OPER );
          G_Debug.AddMessageLn( "", TDebug::VME_OPER );

          switch (count) {
          case 1:
              //fprintf( stderr, "   write: adr=%lx val=%x\n", (address), *data & 0xff );
              G_Debug.AddMessage( "   write: adr=", TDebug::VME_OPER );
              G_Debug.AddHexCont( address, TDebug::VME_OPER );
              G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
              G_Debug.AddHexCont( *data & 0xff, TDebug::VME_OPER );
              G_Debug.AddMessageLn( "", TDebug::VME_OPER );
              break;
          case 2:
              union {
                  char t[2];
                  unsigned short us;
              } u2;
              u2.t[0] = data[0];
              u2.t[1] = data[1];
              //fprintf( stderr, "   write: adr=%lx val=%x\n", (address),  u2.us );
              G_Debug.AddMessage( "   write: adr=", TDebug::VME_OPER );
              G_Debug.AddHexCont( address, TDebug::VME_OPER );
              G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
              G_Debug.AddHexCont( u2.us, TDebug::VME_OPER );
              G_Debug.AddMessageLn( "", TDebug::VME_OPER );
              break;
          default:
              unsigned int i;
              union {
                  char t[4];
                  uint32_t ul;
              } u4;
              for( i=0; i<count; i+=4 ) {
                  u4.t[0]=data[i];
                  u4.t[1]=data[i+1];
                  u4.t[2]=data[i+2];
                  u4.t[3]=data[i+3];
                  /*u4.t[3]=data[i];
                    u4.t[2]=data[i+1];
                    u4.t[1]=data[i+2];
                    u4.t[0]=data[i+3];*/
                  //fprintf( stderr, "   write: adr=%lx val=%lx\n", (address+i),  u4.ul );
                  //cerr << "   write: adr=" << hex << address << " val=" << u4.ul << endl;
                  G_Debug.AddMessage( "   write: adr=", TDebug::VME_OPER );
                  G_Debug.AddHexCont( address+i, TDebug::VME_OPER );
                  G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
                  G_Debug.AddHexCont( u4.ul, TDebug::VME_OPER );
                  G_Debug.AddMessageLn( "", TDebug::VME_OPER );
              }
          }
    #endif


          size_t amt_writen;

      bt_error_t   status;

          //->>>>>
          char* base_buffer_p;
          char* buffer_p;

      /* get a buffer that is aligned on an 8 byte boudary. */
      base_buffer_p = (char *) malloc(count + 8);
      if (NULL == base_buffer_p) {
          throw EVME( string("device ") + DevName + "Could not allocate buffer");
      }

      /* get to the next highest 8 byte address boundary. */
      buffer_p = (char *)((bt_data32_t)base_buffer_p + ((bt_data32_t)8 -
                  ((bt_data32_t)base_buffer_p & 0x00000007)));

      memcpy(buffer_p, data, count);

          // now use buffer_p instead of original data
          data = buffer_p;

          //<<<<-

          status = bt_write(Btd, data, address, count, &amt_writen);

          free(base_buffer_p);

          if ( BT_SUCCESS != status) {

                  /* check if all the message was transfered */
                  if (amt_writen != count) {
                          throw EVMEWrite( string("device ") + DevName + CheckBit3Status(status) );
                          //fprintf(stderr, "Warning: Only %d bytes read from device.\n", amt_read);
                  }

                  throw EVMEWrite( string("device ") + DevName + CheckBit3Status(status) );
          }

    Busy = false;
  }
  catch(...) {
    Busy = false;
    throw;
  };
}




void TVMEWinBit3::Read (uint32_t int address, char * data, size_t count)
{
  if (MaxDMA != -1) {
    if (count > MaxDMA) {
      Read(address, data, MaxDMA);
      Read(address + MaxDMA, data + MaxDMA, count - MaxDMA);
      return;
    };
  };


  if (Busy)
    throw TException("TVMEWinBit3::Write: parallel accesss");

  Busy = true;
  try {

	size_t	amt_read;
        bt_error_t   status;

	status = bt_read(Btd, data, address, count, &amt_read);

	if ( BT_SUCCESS != status) {

		// check if all the message was transfered 
		if (amt_read != count) {
			throw EVMERead( string("device ") + DevName + CheckBit3Status(status) );
			//fprintf(stderr, "Warning: Only %d bytes read from device.\n", amt_read);
		}
		      
		throw EVMERead( string("device ") + DevName + CheckBit3Status(status) );
	}



    #ifdef TB_DEBUG


        switch (count) {
        case 1:
            //fprintf( stderr, "   read: adr=%lx val=%x\n", (address), *data & 0xff );
            G_Debug.AddMessage( "   read: adr=", TDebug::VME_OPER );
            G_Debug.AddHexCont( address, TDebug::VME_OPER );
            G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
            G_Debug.AddHexCont( *data & 0xff, TDebug::VME_OPER );
            G_Debug.AddMessageLn( "", TDebug::VME_OPER );
            break;
        case 2:
            union {
                char t[2];
                unsigned short us;
            } u2;
            u2.t[0] = data[0];
            u2.t[1] = data[1];
            //fprintf( stderr, "   read: adr=%lx val=%x\n", (address),  u2.us );
            G_Debug.AddMessage( "   read: adr=", TDebug::VME_OPER );
            G_Debug.AddHexCont( address, TDebug::VME_OPER );
            G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
            G_Debug.AddHexCont( u2.us, TDebug::VME_OPER );
            G_Debug.AddMessageLn( "", TDebug::VME_OPER );
            break;
        default: 
            unsigned int i;
            union {
                char t[4];
                uint32_t ul;
            } u4;
            for( i=0; i<count; i+=4 ) {
                u4.t[0]=data[i];
                u4.t[1]=data[i+1];
                u4.t[2]=data[i+2];
                u4.t[3]=data[i+3];
                //fprintf( stderr, "   read: adr=%lx val=%lx\n", (address+i),  u4.ul );
                G_Debug.AddMessage( "   read: adr=", TDebug::VME_OPER );
                G_Debug.AddHexCont( address+i, TDebug::VME_OPER );
                G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
                G_Debug.AddHexCont( u4.ul, TDebug::VME_OPER );
                G_Debug.AddMessageLn( "", TDebug::VME_OPER );
            }
        }
	#endif

        

    Busy = false;
  }
  catch(...) {
    Busy = false;
    throw;
  };

}

