#ifndef TVMEBIT3_HH
#define TVMEBIT3_HH


#include        <unistd.h>
#include        <stdio.h>
#include        <stdlib.h>
#include        <ctype.h>
#include        <signal.h>
#include        <errno.h>
#include        <fcntl.h>
#include        <sys/stat.h>
#include        <sys/file.h>
#include        <sys/fcntl.h>
#include        <sys/ioccom.h>
#include        <sys/ioctl.h>
#include        <sys/param.h>
#include        <sys/types.h>
#include        <sys/btio.h>


#include <string.h>
#include "TVMEInterface.hh"
#include "TDebug.hh"


class TVMEBit3 : public TVMEInterface {
    int chan;
    int bit3_perror();
    char err_msg[64];
public:
    TVMEBit3();
    virtual ~TVMEBit3();
    virtual void Write (uint32_t int address, char * data, size_t  count);
    virtual void Write32 (uint32_t int address, uint32_t int  value);
    virtual void Write16 (uint32_t int address, unsigned short int value);

    virtual void Read (uint32_t int address, char * data, size_t count);
    virtual uint32_t  Read32 (uint32_t int address);
    virtual unsigned short Read16 (uint32_t int address);
};



inline 
int TVMEBit3::bit3_perror()
{
    bt_status_t data=0;
    if (ioctl(chan, BIOC_STATUS, &data)) {
        sprintf(err_msg,"BIOC_STATUS returned errno = %d.\n", errno);
        return(1);
    }
    if (data &= BT_STATUS_MASK) {
        sprintf(err_msg, "Status error 0x%02X:\n", (unsigned int)data>>BT_INTR_ERR_SHFT);
        if (data & BT_INTR_POWER) {
            sprintf(err_msg, "\tRemote chassis off or cable disconnected.\n");
        } else {
            if (data & BT_INTR_PARITY)
                sprintf(err_msg, "\tInterface parity error.\n");
            if (data & BT_INTR_REMBUS)
                sprintf(err_msg, "\tRemote bus error.\n");
            if (data & BT_INTR_TIMEOUT)
                sprintf(err_msg, "\tInterface timeout.\n");
        }
        return (1);
    }
    return (0);
}

inline 
TVMEBit3::TVMEBit3() : TVMEInterface()
{
    chan=open("/dev/bts96",O_RDWR);
    if( chan<0 ) 
	throw EVMEcantOpen( strerror(errno) );
    if( bit3_perror() ) {
	close(chan);
	throw EVMEhardware(string(":")+err_msg);
    }
}


inline 
TVMEBit3::~TVMEBit3()
{
    close(chan);
}
    


inline 
void TVMEBit3::Write (uint32_t int address, char * data, size_t count)
{     
        
  #ifdef TB_DEBUG
        G_Debug.AddMessage( "VMEWrite: address=", TDebug::VME_OPER );
        G_Debug.AddHexCont( address, TDebug::VME_OPER );
        G_Debug.AddMessageCont( " count=", TDebug::VME_OPER );
        G_Debug.AddDecCont( count, TDebug::VME_OPER );
        G_Debug.AddMessageLn( "", TDebug::VME_OPER );

        switch (count) {
        case 1: 
            //fprintf( stderr, "   write: adr=%lx val=%x\n", (address), *data & 0xff );
            G_Debug.AddMessage( "   write: adr=", TDebug::VME_OPER );
            G_Debug.AddHexCont( address, TDebug::VME_OPER );
            G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
            G_Debug.AddHexCont( *data & 0xff, TDebug::VME_OPER );
            G_Debug.AddMessageLn( "", TDebug::VME_OPER );
            break;
        case 2:  
            union {
                char t[2];
                unsigned short us;
            } u2;
            u2.t[0] = data[0];
            u2.t[1] = data[1];
            //fprintf( stderr, "   write: adr=%lx val=%x\n", (address),  u2.us );
            G_Debug.AddMessage( "   write: adr=", TDebug::VME_OPER );
            G_Debug.AddHexCont( address, TDebug::VME_OPER );
            G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
            G_Debug.AddHexCont( u2.us, TDebug::VME_OPER );
            G_Debug.AddMessageLn( "", TDebug::VME_OPER );
            break;
        default: 
            unsigned int i;
            union {
                char t[4];
                uint32_t ul;
            } u4;
            for( i=0; i<count; i+=4 ) {
                u4.t[0]=data[i];
                u4.t[1]=data[i+1];
                u4.t[2]=data[i+2];
                u4.t[3]=data[i+3]; 
                /*u4.t[3]=data[i];
                  u4.t[2]=data[i+1];
                  u4.t[1]=data[i+2];
                  u4.t[0]=data[i+3];*/
                //fprintf( stderr, "   write: adr=%lx val=%lx\n", (address+i),  u4.ul );
                //cerr << "   write: adr=" << hex << address << " val=" << u4.ul << endl;
                G_Debug.AddMessage( "   write: adr=", TDebug::VME_OPER );
                G_Debug.AddHexCont( address, TDebug::VME_OPER );
                G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
                G_Debug.AddHexCont( u4.ul, TDebug::VME_OPER );
                G_Debug.AddMessageLn( "", TDebug::VME_OPER );
            } 
        }
  #endif
    

    if(lseek(chan,address,SEEK_SET)<0) 
        throw EVMEwrite( string(": can not set the address :")+strerror(errno) );
    
    if(write(chan,data,count)!=(int)count) 
        throw EVMEwrite( strerror(errno) );
}



inline 
void TVMEBit3::Write32 (uint32_t int address, uint32_t int value)
{
    Write(address,(char *)&value,sizeof(value));
}



inline 
void TVMEBit3::Write16 (uint32_t int address, unsigned short int value)
{
    Write(address,(char *)&value,sizeof(value));
}




inline 
void TVMEBit3::Read (uint32_t int address, char * data, size_t count)
{
    if(lseek(chan,address,SEEK_SET)<0)
	throw EVMEread( string(": cannot set the address :")+strerror(errno) );
    
    if(read(chan,data,count)!=(int)count) 
	throw EVMEread( strerror(errno) );
 
    #ifdef TB_DEBUG


        switch (count) {
        case 1: 
            //fprintf( stderr, "   read: adr=%lx val=%x\n", (address), *data & 0xff );
            G_Debug.AddMessage( "   read: adr=", TDebug::VME_OPER );
            G_Debug.AddHexCont( address, TDebug::VME_OPER );
            G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
            G_Debug.AddHexCont( *data & 0xff, TDebug::VME_OPER );
            G_Debug.AddMessageLn( "", TDebug::VME_OPER );
            break;
        case 2:
            union {
                char t[2];
                unsigned short us;
            } u2;
            u2.t[0] = data[0];
            u2.t[1] = data[1];
            //fprintf( stderr, "   read: adr=%lx val=%x\n", (address),  u2.us );
            G_Debug.AddMessage( "   read: adr=", TDebug::VME_OPER );
            G_Debug.AddHexCont( address, TDebug::VME_OPER );
            G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
            G_Debug.AddHexCont( u2.us, TDebug::VME_OPER );
            G_Debug.AddMessageLn( "", TDebug::VME_OPER );
            break;
        default: 
            unsigned int i;
            union {
                char t[4];
                uint32_t ul;
            } u4;
            for( i=0; i<count; i+=4 ) {
                u4.t[0]=data[i];
                u4.t[1]=data[i+1];
                u4.t[2]=data[i+2];
                u4.t[3]=data[i+3];
                //fprintf( stderr, "   read: adr=%lx val=%lx\n", (address+i),  u4.ul );
                G_Debug.AddMessage( "   read: adr=", TDebug::VME_OPER );
                G_Debug.AddHexCont( address, TDebug::VME_OPER );
                G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
                G_Debug.AddHexCont( u4.ul, TDebug::VME_OPER );
                G_Debug.AddMessageLn( "", TDebug::VME_OPER );
            } 
        }
	#endif
    
}


                     
inline 
uint32_t TVMEBit3::Read32 (uint32_t int address)
{
    uint32_t ul;
    Read( address, (char *)&ul, sizeof(uint32_t) );
    return ul;
}



inline
unsigned short TVMEBit3::Read16 (uint32_t int address)
{
    unsigned short us;
    Read( address, (char *)&us, sizeof(unsigned short) );
    return us;
}


#endif
