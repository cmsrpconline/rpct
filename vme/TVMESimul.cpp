#include "TVMESimul.h"

void TVMESimul::Write (uint32_t address, char * data, size_t count)
{
  #ifdef TB_DEBUG
        G_Debug.AddMessage( "VMEWrite: address=", TDebug::VME_OPER );
        G_Debug.AddHexCont( address, TDebug::VME_OPER );
        G_Debug.AddMessageCont( " count=", TDebug::VME_OPER );
        G_Debug.AddDecCont( count, TDebug::VME_OPER );
        G_Debug.AddMessageLn( "", TDebug::VME_OPER );

        //cerr << hex << address << " count=" << dec << count << endl;

        switch (count) {
        case 1:
            //fprintf( stderr, "   write: adr=%lx val=%x\n", (address), *data & 0xff );
            //cerr << "   write: adr=" << hex << address << " val=" << (*data & 0xff) << endl;
            G_Debug.AddMessage( "   write: adr=", TDebug::VME_OPER );
            G_Debug.AddHexCont( address, TDebug::VME_OPER );
            G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
            G_Debug.AddHexCont( *data & 0xff, TDebug::VME_OPER );
            G_Debug.AddMessageLn( "", TDebug::VME_OPER );
            break;
        case 2:
            union {
                char t[2];
                unsigned short us;
            } u2;
            u2.t[0] = data[0];
            u2.t[1] = data[1];
            //fprintf( stderr, "   write: adr=%lx val=%x\n", (address),  u2.us );
            //cerr << "   write: adr=" << hex << address << " val=" << u2.us << endl;
            G_Debug.AddMessage( "   write: adr=", TDebug::VME_OPER );
            G_Debug.AddHexCont( address, TDebug::VME_OPER );
            G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
            G_Debug.AddHexCont( u2.us, TDebug::VME_OPER );
            G_Debug.AddMessageLn( "", TDebug::VME_OPER );
            break;
        default:
            unsigned int i;
            union {
                char t[4];
                uint32_t ul;
            } u4;
            for( i=0; i<count; i+=4 ) {
                u4.t[0]=data[i];
                u4.t[1]=data[i+1];
                u4.t[2]=data[i+2];
                u4.t[3]=data[i+3];
                /*u4.t[3]=data[i];
                  u4.t[2]=data[i+1];
                  u4.t[1]=data[i+2];
                  u4.t[0]=data[i+3];*/
                //fprintf( stderr, "   write: adr=%lx val=%lx\n", (address+i),  u4.ul );
                //cerr << "   write: adr=" << hex << address << " val=" << u4.ul << endl;
                G_Debug.AddMessage( "   write: adr=", TDebug::VME_OPER );
                G_Debug.AddHexCont( address, TDebug::VME_OPER );
                G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
                G_Debug.AddHexCont( u4.ul, TDebug::VME_OPER );
                G_Debug.AddMessageLn( "", TDebug::VME_OPER );
            }
        }
  #endif
}


void TVMESimul::MultiWrite16(uint32_t* addresses, uint16_t* values, size_t count) {
    for (size_t i = 0; i < count; i++) {
        Write16(addresses[i], values[i]);
    }
}

void TVMESimul::MultiWrite32(uint32_t* addresses, uint32_t* values, size_t count) {
    for (size_t i = 0; i < count; i++) {
        Write32(addresses[i], values[i]);
    }
}

void TVMESimul::Read (uint32_t address, char * data, size_t count)
{
    for (size_t i=0; i<count; i++)
      data[i] = rand();

    #ifdef TB_DEBUG  
        G_Debug.AddMessage( "VMERead: address=", TDebug::VME_OPER );
        G_Debug.AddHexCont( address, TDebug::VME_OPER );
        G_Debug.AddMessageCont( " count=", TDebug::VME_OPER );
        G_Debug.AddDecCont( count, TDebug::VME_OPER );
        G_Debug.AddMessageLn( "", TDebug::VME_OPER );

        switch (count) {
        case 1:
            //fprintf( stderr, "   read: adr=%lx val=%x\n", (address), *data & 0xff );
            //cerr << "   read: adr=" << hex << address << " val=" << (*data & 0xff) << endl;
            G_Debug.AddMessage( "   read: adr=", TDebug::VME_OPER );
            G_Debug.AddHexCont( address, TDebug::VME_OPER );
            G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
            G_Debug.AddHexCont( *data & 0xff, TDebug::VME_OPER );
            G_Debug.AddMessageLn( "", TDebug::VME_OPER );
            break;
        case 2:
            union {
                char t[2];
                unsigned short us;
            } u2;
            u2.t[0] = data[0];
            u2.t[1] = data[1];
            //fprintf( stderr, "   read: adr=%lx val=%x\n", (address),  u2.us );
            //cerr << "   read: adr=" << hex << address << " val=" << u2.us << endl;
            G_Debug.AddMessage( "   read: adr=", TDebug::VME_OPER );
            G_Debug.AddHexCont( address, TDebug::VME_OPER );
            G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
            G_Debug.AddHexCont( u2.us, TDebug::VME_OPER );
            G_Debug.AddMessageLn( "", TDebug::VME_OPER );
            break;
        default:
            unsigned int i;
            union {
                char t[4];
                uint32_t ul;
            } u4;
            for( i=0; i<count; i+=4 ) {
                u4.t[0]=data[i];
                u4.t[1]=data[i+1];
                u4.t[2]=data[i+2];
                u4.t[3]=data[i+3];
                //fprintf( stderr, "   read: adr=%lx val=%lx\n", (address+i),  u4.ul );
                //cerr << "   read: adr=" << hex << address << " val=" << u4.ul << endl;
                G_Debug.AddMessage( "   read: adr=", TDebug::VME_OPER );
                G_Debug.AddHexCont( address, TDebug::VME_OPER );
                G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
                G_Debug.AddHexCont( u4.ul, TDebug::VME_OPER );
                G_Debug.AddMessageLn( "", TDebug::VME_OPER );
            }
        }
	#endif
}

