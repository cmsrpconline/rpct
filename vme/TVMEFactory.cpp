//---------------------------------------------------------------------------
#ifdef __BORLANDC__
#include <vcl.h>
#pragma hdrstop
                          
#include <registry.hpp>
#endif

#include "TVMEFactory.h"

//#include "TVMEWinBit3.h"     
#include "TVMESimul.h"
//#include "TVMEEpp.h"  
#include "TVMEHAL.h"  
#include "TVMECAEN.h"  
#include "hal/CAENLinuxBusAdapter.hh" 
#include <boost/tokenizer.hpp>
#include <sstream> 
//---------------------------------------------------------------------------

                    
using namespace std;
using namespace HAL;


/*TVMEInterface* TVMEFactory::TVMEBit3TypeInfo::Create() {
    return new TVMEWinBit3();
}  
TVMEInterface* TVMEFactory::TVMEBit3TypeInfo::Create(string unit) {
    return new TVMEWinBit3();
} */ 
/*
TVMEInterface* TVMEFactory::TVMEEppTypeInfo::Create() {
    return new TVMEEpp();
}  
TVMEInterface* TVMEFactory::TVMEEppTypeInfo::Create(string unit) {
    return new TVMEEpp();
}  

TVMEInterface* TVMEFactory::TVMECaenTypeInfo::Create() {
    return new TVMEHAL(new CAENLinuxBusAdapter(CAENLinuxBusAdapter::V2718), true);
}  
TVMEInterface* TVMEFactory::TVMECaenTypeInfo::Create(string unit) {
    istringstream istr(unit);
    int u;
    istr >> u;
    return new TVMEHAL(new CAENLinuxBusAdapter(CAENLinuxBusAdapter::V2718, u), true);
}  */

TVMEInterface* TVMEFactory::TVMECaenTypeInfo::Create() {
    return new TVMECAEN();
}  
TVMEInterface* TVMEFactory::TVMECaenTypeInfo::Create(string unit) {
    int link = 0;
    int bdNum = 0;
    boost::tokenizer<> tok(unit);
    boost::tokenizer<>::iterator beg = tok.begin();
    if (beg != tok.end()) {
        istringstream istr(*beg);
        istr >> link;        
        ++beg;
        if (beg != tok.end()) {
            istringstream istr(*beg);
            istr >> bdNum;
        }
    }
    return new TVMECAEN(link, bdNum);
}  

TVMEInterface* TVMEFactory::TVMESimulTypeInfo::Create() {
    return new TVMESimul();
}  
TVMEInterface* TVMEFactory::TVMESimulTypeInfo::Create(string unit) {
    return new TVMESimul();
}  


           
/*
[HKEY_LOCAL_MACHINE\Software\tb]
"VME"="Bit3"
*/
          
string TVMEFactory::GetDefaultVMEType()
{
#ifdef __BORLANDC__
  string type;
  TRegistry* pRegistry = new TRegistry;
  try {
    pRegistry->RootKey = HKEY_LOCAL_MACHINE;
    // False because we do not want to create it if it doesn't exist
    if (!pRegistry->OpenKey("Software\\tb",false))
      throw TException("Could not open registry key: HKEY_LOCAL_MACHINE\\Software\\tb");

    type = pRegistry->ReadString("VME").c_str();

    if (type == "")
      throw TException("Could not open registry: HKEY_LOCAL_MACHINE\\Software\\tb, Name: VME.");

    delete pRegistry;
  }
  catch(...) {
    delete pRegistry;
    throw;
  }
  return type;
#else
  char* type = getenv("VME");
  if (type == NULL)
      throw TException("Environment variable 'VME' not set");
  return type;
#endif
}


void TVMEFactory::SetDefaultVMEType(std::string type)
{      
#ifdef __BORLANDC__
  TVMETypeMap::iterator iType = VMETypeMap.find(type);
  if (iType == VMETypeMap.end())
    throw TException("Unknown vme interface type '" + type + "'");

  TRegistry* pRegistry = new TRegistry;
  try {
    pRegistry->RootKey = HKEY_LOCAL_MACHINE;
    // False because we do not want to create it if it doesn't exist
    if (!pRegistry->OpenKey("Software\\tb", true))
      throw TException("Could not open registry key: HKEY_LOCAL_MACHINE\\Software\\tb");

    pRegistry->WriteString("VME", type.c_str());
    delete pRegistry;
  }
  catch(...) {
    delete pRegistry;
    throw;
  }
#else
  throw TException("TVMEFactory::SetDefaultVMEType not implemented");
#endif
}



TVMEInterface* TVMEFactory::Create()
{
  string type = GetDefaultVMEType();
  TVMETypeMap::iterator iType = VMETypeMap.find(type);
  if (iType == VMETypeMap.end())
    throw TException("Unknown vme interface type '" + type + "'");

  return iType->second->Create();
}

TVMEInterface* TVMEFactory::Create(string type) 
{
  TVMETypeMap::iterator iType = VMETypeMap.find(type);
  if (iType == VMETypeMap.end())
    throw TException("Unknown vme interface type '" + type + "'");

  return iType->second->Create();    
}


TVMEInterface* TVMEFactory::Create(string type, string unit) 
{
  TVMETypeMap::iterator iType = VMETypeMap.find(type);
  if (iType == VMETypeMap.end())
    throw TException("Unknown vme interface type '" + type + "'");

  return iType->second->Create(unit);    
}
