#include "TVMECAEN.h"
#include "CAENVMElib.h"
#include <sstream>
#include "rpct/std/IllegalArgumentException.h"

using namespace std;
using namespace rpct;

static const char* getErrorString(CVErrorCodes errorCode) {
    /*switch (errorCode)  {
        case cvSuccess:
            return "Success";
        case cvBusError:
            return "Bus error";
        case cvCommError:
            return "Communication error";
        case cvGenericError:
            return "Unspecified error";
        case cvInvalidParam:
            return "Invalid parameter";
        default:
            return "Unknown";            
    }     */
    return CAENVME_DecodeError(errorCode);
}

TVMECAEN::TVMECAEN(int link, int bdNum) 
: handle_(0), addressingMode_(am24) {
    
    #ifdef TB_DEBUG
        G_Debug.AddMessage("Creating TVMECAEN link = ", TDebug::VME_OPER);
        G_Debug.AddDecCont(link, TDebug::VME_OPER );
        G_Debug.AddMessageCont(" bdNum=", TDebug::VME_OPER );
        G_Debug.AddDecCont(bdNum, TDebug::VME_OPER );
        G_Debug.AddMessageLn("", TDebug::VME_OPER );
    #endif
    
        //TODO choose cvA3818 or cvV2718
    cout<<"starting CAENVME_Init with cvA3818...."<<endl;
    CVErrorCodes status = CAENVME_Init(cvA3818, link, bdNum, &handle_);
    if (status != cvSuccess) {
        throw EVMEOpen(getErrorString(status));
    }
    #ifdef TB_DEBUG
        G_Debug.AddMessage("CAENVME_Init handle_ = ", TDebug::VME_OPER);
        G_Debug.AddHexCont(handle_, TDebug::VME_OPER );
        G_Debug.AddMessageLn("", TDebug::VME_OPER );
    #endif
    CAENVME_End(handle_);

    cout<<"starting CAENVME_Init with cvV2718...."<<endl;
    status = CAENVME_Init(cvV2718, link, bdNum, &handle_);
    #ifdef TB_DEBUG
        G_Debug.AddMessage("CAENVME_Init handle_ = ", TDebug::VME_OPER);
        G_Debug.AddHexCont(handle_, TDebug::VME_OPER );
        G_Debug.AddMessageLn("", TDebug::VME_OPER );
    #endif

    if (status != cvSuccess) {
        throw EVMEOpen(getErrorString(status));
    }
    cout<<"starting CAENVME_Init successful"<<endl;
    
    
    /*#ifdef TB_DEBUG
        char fwRelease[256];
        status = CAENVME_BoardFWRelease(handle_, fwRelease);
        if (status != cvSuccess) {
            stringstream str;
            str << "Could not determine firmware version of V2718 (status: "
                             << status 
                             << ")"
                             << ends;
                throw EVMEOpen(str.str());
        }
        G_Debug.AddMessage("CAEN Firmware release ", TDebug::VME_OPER);
        G_Debug.AddMessageCont(fwRelease, TDebug::VME_OPER );
        G_Debug.AddMessageLn( "", TDebug::VME_OPER );
    #endif*/
    
    /*if ( ( status = CAENVME_BoardFWRelease( handle__, V2718FwRelease_ )) != cvSuccess ) {
    stringstream errorMessage;
        errorMessage << "Could not determine firmware version of V2718 (status: "
                     << status 
                     << ")"
                     << ends;
        throw (BusAdapterException( errorMessage.str(), __FILE__, __LINE__, __FUNCTION__ ) );
    }
    CAENVME_SWRelease( VMELibRelease_ );*/
}



TVMECAEN::~TVMECAEN() {
    if (handle_ != 0) {
        CVErrorCodes status = CAENVME_End(handle_);
        if (status != cvSuccess) {
            throw EVMEOpen(getErrorString(status));
        }
    }
}




void TVMECAEN::Reset() {
    CVErrorCodes status = CAENVME_SystemReset(handle_);
    if (status != cvSuccess) {
        throw EVMEOpen(string("Could not reset vme: ") + getErrorString(status));
    }
}

void TVMECAEN::SetDefaultAddressingMode(AddressingMode am) {
    addressingMode_ = am;
}

void TVMECAEN::Write(uint32_t address, char * data, size_t  count) {
    #ifdef TB_DEBUG
        G_Debug.AddMessage("write block to adr=0x", TDebug::VME_OPER);
        G_Debug.AddHexCont(address, TDebug::VME_OPER);
        G_Debug.AddMessageCont( " count=0x", TDebug::VME_OPER );
        G_Debug.AddHexCont(count, TDebug::VME_OPER);
        G_Debug.AddMessageLn( "", TDebug::VME_OPER );
    #endif

    /*assert((count % 4) == 0);
    for (size_t i = 0; i < count; i += 4) {
        Write32(address + i, *(uint32_t*) (data + i));
    }*/

    int actualTransferred;
    CVAddressModifier am;
    switch (addressingMode_) {
        //case am16: am = cvA16_U_BLT; break;
        case am24: am = cvA24_U_BLT; break;
        case am32: am = cvA32_U_BLT; break;
        default: throw IllegalArgumentException("TVMECAEN::Write: Invalid addressingMode");
    }
    CVErrorCodes status = CAENVME_BLTWriteCycle(handle_, address, (unsigned char*)data, count, am,
        cvD32, &actualTransferred);
    if (status != cvSuccess) {
        throw EVMEWrite(getErrorString(status));
    }
    if (actualTransferred != (int)count) {
        stringstream message;
        message << "Block transfer has only transferred " << dec << actualTransferred
                << " bytes instead of the requested " << count
                << ends;
        throw EVMEWrite(message.str());
    }

/*  does not solve the VME bus errors on CC7
 *  uint32_t stride = 128;
    uint32_t a = 0;
    if(count > stride) {
        for (; a < count - stride; a += stride) {
            cout<<hex<<"a = 0x"<<a<<", address + a  = 0x"<<address + a<<", data + a = 0x"<<(uint64_t)(data + a)<<endl;
            CVErrorCodes status = CAENVME_BLTWriteCycle(handle_, address + a, (unsigned char*)(data + a), stride, am,
                    cvD32, &actualTransferred);
            if (status != cvSuccess) {
                throw EVMEWrite(getErrorString(status));
            }
            if (actualTransferred != (int)stride) {
                stringstream message;
                message << "Block transfer has only transferred " << dec << actualTransferred
                        << " bytes instead of the requested " << stride
                        << ends;
                throw EVMEWrite(message.str());
            }
        }
    }

    cout<<hex<<"a = 0x"<<a<<", address + a = 0x"<<address + a<<", data + a = 0x"<<(uint64_t)(data + a)<<", count - a = 0x"<<count - a<<endl;
    CVErrorCodes status = CAENVME_BLTWriteCycle(handle_, address + a, (unsigned char*)(data + a), count - a, am,
            cvD32, &actualTransferred);
    if (status != cvSuccess) {
        throw EVMEWrite(getErrorString(status));
    }
    if (actualTransferred != (int)(count - a)) {
        stringstream message;
        message << "Block transfer has only transferred " << dec << actualTransferred
                << " bytes instead of the requested " << (count - a)
                << ends;
        throw EVMEWrite(message.str());
    }*/
    /*for(size_t  i = 0; i < count; i++) {
        cout<<hex<<"i = 0x"<<i<<" 0x"<<(uint32_t)data[i]<<endl;
    }*/

/*    size_t  i = 0;
    for(; i < count; i += 4) {
        uint32_t* value = (uint32_t*)(data + i);
        Write32(address + i, *value);
    }
    if(i != count) {
        throw IllegalArgumentException("TVMECAEN::Write: cout%4 != 0");
    }*/
}


void TVMECAEN::Write32(uint32_t address, uint32_t  value) {
    #ifdef TB_DEBUG
        G_Debug.AddMessage("write32: adr=", TDebug::VME_OPER);
        G_Debug.AddHexCont(address, TDebug::VME_OPER);
        G_Debug.AddMessageCont(" val=", TDebug::VME_OPER);
        G_Debug.AddHexCont(value, TDebug::VME_OPER);
        G_Debug.AddMessageLn("", TDebug::VME_OPER);
    #endif        
    
    
    CVAddressModifier am;
    switch (addressingMode_) {
        case am16: am = cvA16_U; break;
        case am24: am = cvA24_U_DATA; break;
        case am32: am = cvA32_U_DATA; break;
        default: throw IllegalArgumentException("TVMECAEN::Write32: Invalid addressingMode");
    }
    
    CVErrorCodes status = CAENVME_WriteCycle(handle_, address, &value, am, cvD32);
    
    if (status != cvSuccess) {
        throw EVMEWrite(getErrorString(status));
    }    
}

void TVMECAEN::Write16(uint32_t address, unsigned short value) {
    #ifdef TB_DEBUG
        G_Debug.AddMessage("write16: adr=", TDebug::VME_OPER);
        G_Debug.AddHexCont(address, TDebug::VME_OPER);
        G_Debug.AddMessageCont(" val=", TDebug::VME_OPER);
        G_Debug.AddHexCont(value, TDebug::VME_OPER);
        G_Debug.AddMessageLn("", TDebug::VME_OPER);
    #endif
    
    CVAddressModifier am;
    switch (addressingMode_) {
        case am16: am = cvA16_U; break;
        case am24: am = cvA24_U_DATA; break;
        case am32: am = cvA32_U_DATA; break;
        default: throw IllegalArgumentException("TVMECAEN::Write16: Invalid addressingMode");
    }
    /*CVErrorCodes status = CAENVME_WriteCycle(handle_, address, &value, (CVAddressModifier)0x39, 
        (CVDataWidth) sizeof(value));*/
    CVErrorCodes status = CAENVME_WriteCycle(handle_, address, &value, am, cvD16);
    
    if (status != cvSuccess) {
        throw EVMEWrite(getErrorString(status));
    }    
}


void TVMECAEN::MultiWrite16(uint32_t* addresses, uint16_t* values, size_t count) {
    
    CVAddressModifier am;
    switch (addressingMode_) {
        case am16: am = cvA16_U; break;
        case am24: am = cvA24_U_DATA; break;
        case am32: am = cvA32_U_DATA; break;
        default: throw IllegalArgumentException("TVMECAEN::MultiWrite16: Invalid addressingMode");
    }
    
    uint32_t data[count];
    CVAddressModifier ams[count];
    CVDataWidth widths[count];
    CVErrorCodes errorCodes[count];
    for (size_t i = 0; i < count; i++) {
        data[i] = values[i];
        ams[i] = am;
        widths[i] = cvD16;
    }
    
    CVErrorCodes status = CAENVME_MultiWrite(handle_, addresses, data, count, ams, widths, errorCodes);
    
    if (status != cvSuccess) {
        throw EVMEWrite(getErrorString(status));
    }    
}
    
void TVMECAEN::MultiWrite32(uint32_t* addresses, uint32_t* values, size_t count) {
    CVAddressModifier am;
    switch (addressingMode_) {
        case am16: am = cvA16_U; break;
        case am24: am = cvA24_U_DATA; break;
        case am32: am = cvA32_U_DATA; break;
        default: throw IllegalArgumentException("TVMECAEN::MultiWrite32: Invalid addressingMode");
    }
    
    CVAddressModifier ams[count];
    CVDataWidth widths[count];
    CVErrorCodes errorCodes[count];
    for (size_t i = 0; i < count; i++) {
        ams[i] = am;
        widths[i] = cvD32;
    }
    
    /*cout << hex;
    for (size_t i = 0; i < count; i++) {
    	cout << "MultiWrite32 addr = " << addresses[i] << " data = " << values[i] << endl;
    }*/
    
    CVErrorCodes status = CAENVME_MultiWrite(handle_, addresses, values, count, ams, widths, errorCodes);
    
    if (status != cvSuccess) {
        throw EVMEWrite(getErrorString(status));
    }    
}

void TVMECAEN::Read(uint32_t address, char * data, size_t count) {
    #ifdef TB_DEBUG
        G_Debug.AddMessage("read block from adr=", TDebug::VME_OPER);
        G_Debug.AddHexCont(address, TDebug::VME_OPER);
        G_Debug.AddMessageCont( " count=", TDebug::VME_OPER );
        G_Debug.AddHexCont(count, TDebug::VME_OPER);
        G_Debug.AddMessageLn( ":", TDebug::VME_OPER );
    #endif
    /*busAdapter->readBlock(NULL, address, count, 0x39, 4, data);*/
    
    int actualTransferred;
    CVAddressModifier am;
    switch (addressingMode_) {
        //case am16: am = cvA16_U_BLT; break;
        case am24: am = cvA24_U_BLT; break;
        case am32: am = cvA32_U_BLT; break;
        default: throw IllegalArgumentException("TVMECAEN::Write: Invalid addressingMode");
    }
    CVErrorCodes status = CAENVME_BLTReadCycle(handle_, address, (unsigned char*)data, count, am,
                cvD32, &actualTransferred);
    if (status != cvSuccess) {
        throw EVMERead(getErrorString(status));
    }
    if (actualTransferred != (int)count) {
        stringstream message;
        message << "Block transfer has only transferred " << dec << actualTransferred
                << " bytes instead of the requested " << count
                << ends;
        throw EVMERead(message.str());
    }
    /*assert((count % 4) == 0);
    for (size_t i = 0; i < count; i += 4) {
        *(uint32_t*) (data + i) = Read32(address + i);
    }*/
    
    #ifdef TB_DEBUG
        unsigned int i;
        union {
            char t[4];
            uint32_t ul;
        } u4;
        for( i=0; i<count; i+=4 ) {
            u4.t[0]=data[i];
            u4.t[1]=data[i+1];
            u4.t[2]=data[i+2];
            u4.t[3]=data[i+3];
            G_Debug.AddMessage( "read: adr=", TDebug::VME_OPER );
            G_Debug.AddHexCont( address+i, TDebug::VME_OPER );
            G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
            G_Debug.AddHexCont( u4.ul, TDebug::VME_OPER );
            G_Debug.AddMessageLn( "", TDebug::VME_OPER );
        }
    #endif
}


uint32_t TVMECAEN::Read32(uint32_t address) {
    #ifdef TB_DEBUG
        G_Debug.AddMessage("read32: adr=", TDebug::VME_OPER);
        G_Debug.AddHexCont(address, TDebug::VME_OPER);
    #endif
    
    CVAddressModifier am;
    switch (addressingMode_) {
        case am16: am = cvA16_U; break;
        case am24: am = cvA24_U_DATA; break;
        case am32: am = cvA32_U_DATA; break;
        default: throw IllegalArgumentException("TVMECAEN::Read32: Invalid addressingMode");
    }

    #ifdef TB_DEBUG
        G_Debug.AddMessageCont(" CVAddressModifier = ", TDebug::VME_OPER);
        G_Debug.AddHexCont(am, TDebug::VME_OPER);
        G_Debug.AddMessageCont(" handle_ = ", TDebug::VME_OPER);
        G_Debug.AddHexCont(handle_, TDebug::VME_OPER);
    #endif

    uint32_t value = 0;
    CVErrorCodes status = CAENVME_ReadCycle(handle_, address, &value, am, cvD32);
    if (status != cvSuccess) {
        throw EVMERead(getErrorString(status));
    }    
    
    #ifdef TB_DEBUG
        G_Debug.AddMessageCont(" val=", TDebug::VME_OPER);
        G_Debug.AddHexCont(value, TDebug::VME_OPER);
        G_Debug.AddMessageLn("", TDebug::VME_OPER);
    #endif
        
    return value;
}

unsigned short TVMECAEN::Read16(uint32_t address) {
    #ifdef TB_DEBUG
        G_Debug.AddMessage("read16: adr=", TDebug::VME_OPER);
        G_Debug.AddHexCont(address, TDebug::VME_OPER);
    #endif
    
    CVAddressModifier am;
    switch (addressingMode_) {
        case am16: am = cvA16_U; break;
        case am24: am = cvA24_U_DATA; break;
        case am32: am = cvA32_U_DATA; break;
        default: throw IllegalArgumentException("TVMECAEN::Write16: Invalid addressingMode");
    }
    unsigned short value;  
    CVErrorCodes status = CAENVME_ReadCycle(handle_, address, &value, am, cvD16);
    if (status != cvSuccess) {
        throw EVMERead(getErrorString(status));
    }    
    #ifdef TB_DEBUG
        G_Debug.AddMessageCont(" val=", TDebug::VME_OPER);
        G_Debug.AddHexCont(value, TDebug::VME_OPER);
        G_Debug.AddMessageLn("", TDebug::VME_OPER);
    #endif
    return value;
}

