#ifndef TVMECAEN_HH
#define TVMECAEN_HH


#include "TVMEInterface.h"
#include "tb_std.h"

class TVMECAEN : public TVMEInterface {
private:
    int32_t handle_;
    //const char* getErrorString(CVErrorCodes errorCode);
    AddressingMode addressingMode_;
public:
    TVMECAEN(int link = 0, int bdNum = 0);
    virtual ~TVMECAEN();
    
	virtual void Reset();
    
    virtual void SetDefaultAddressingMode(AddressingMode am);
    
    virtual void Write(uint32_t address, char * data, size_t  count);
    
    virtual void Write32(uint32_t address, uint32_t  value);
    
    virtual void Write16(uint32_t address, unsigned short value);
    
    virtual void MultiWrite16(uint32_t* addresses, uint16_t* values, size_t count);
    
    virtual void MultiWrite32(uint32_t* addresses, uint32_t* values, size_t count);

    virtual void Read(uint32_t address, char * data, size_t count);
    
    virtual uint32_t  Read32(uint32_t address);
    
    virtual unsigned short Read16(uint32_t address);
};



#endif
