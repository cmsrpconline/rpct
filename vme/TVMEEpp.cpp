/*
 *  TVMEEpp.cpp		``VME over parallel port'' driver for Windows
 *			(Borland C++) and for Linux.
 *			
 *			The Linux vesion needs kernel vesion > 2.4.x
 *			(the ppdev driver).
 */
#ifdef __BORLANDC__
#include <vcl.h>
#pragma hdrstop
#endif

#include "TVMEEpp.h"
#include <boost/timer.hpp>
#include <sstream>
//---------------------------------------------------------------------------

#ifdef __BORLANDC__
#pragma package(smart_init)
#pragma link "TDLPortIO"
#endif // __BORLANDC__

#ifdef linux
#include <linux/parport.h>
#include <linux/ppdev.h>
#include <sys/io.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>

#include <fcntl.h>
#include <errno.h>
#endif

using namespace std;


const int TVMEEpp::DATA_SIZE_32 = 0;
const int TVMEEpp::DATA_SIZE_24 = 1;
const int TVMEEpp::DATA_SIZE_16 = 2;
const int TVMEEpp::DATA_SIZE_8  = 3;

const int TVMEEpp::ADDR_SIZE_24 = 0;
const int TVMEEpp::ADDR_SIZE_16 = 1;
const int TVMEEpp::ADDR_SIZE_8  = 2;
const int TVMEEpp::ADDR_SIZE_0  = 3;
  

#ifdef linux
void epp_delay()
{
  //int volatile a;
  //for (int i=0; i<10000; i++)
  //  a++;
}

/*
TVMEEpp::TVMEEpp(const char *name) : PPDevMode(0)
{
	int mode;
	PPDev = open(name, O_RDWR);
	if ((PPDev = open(name, O_RDWR)) == -1)
		throw TException(string("cannot open the pport driver: ``")
				+ name + "'': " + strerror(errno));
	if (ioctl(PPDev, PPEXCL)) {
		close(PPDev);
		throw TException(string("can't set the driver in"
				" exclusive mode: ")
				+ strerror(errno));
	}
	if (ioctl(PPDev, PPCLAIM)) {
		close(PPDev);
		throw TException(string("can't claim the driver: ")
				+ strerror(errno));
	}
	PPDevSetMode(0);		// set driver in EPP mode
    Init();
}

void TVMEEpp::PPDevSetMode(int mode)
{
	mode |= IEEE1284_MODE_EPP;      // the IEEE1284 EPP mode forced
	if (PPDevMode != mode) {
		if (ioctl(PPDev, PPSETMODE, &mode)) {
			close(PPDev);
			throw TException(string("can't set the driver's mode: " )
					+ strerror(errno));
		}
		PPDevMode = mode;
	}
}

int TVMEEpp::LPTReadData()
{
	char c;
	PPDevSetMode(IEEE1284_DATA);
	switch (read(PPDev, (void *) &c, 1)) {
	case 0:
		throw TException(string("No data read from driver."));
	case -1:
		throw TException(string("error reading data: ")
				+ strerror(errno));
	}
       
    #ifdef TB_DEBUG 
      G_Log.out() << "LPTReadData " << (int)c << "\n";
    #endif
    epp_delay();
	return c;
}

void  TVMEEpp::LPTWriteData(int value)
{
	PPDevSetMode(IEEE1284_DATA);

	switch (write(PPDev, (void *) &value, 1)) {
	case 0:
		throw TException(string("No data written to driver."));
	case -1:
		throw TException(string("error writing data: ")
				+ strerror(errno));
	}
    #ifdef TB_DEBUG 
      G_Log.out() << "LPTWriteData " << value << "\n";
    #endif
    epp_delay();
}

int TVMEEpp::LPTReadAddress()
{
	char c;
	PPDevSetMode(IEEE1284_ADDR);
	switch (read(PPDev, (void *) &c, 1)) {
	case 0:
		throw TException(string("Warning: no data (address"
					" mode) read from driver.\n"));
	case -1:
		throw TException(string("error reading address: ")
				+ strerror(errno));
	}

    
    #ifdef TB_DEBUG
      G_Log.out() << "LPTReadAddress " << (int)c << "\n";
    #endif
    epp_delay();
	return c;
}

void TVMEEpp::LPTWriteAddress(int value)
{
	PPDevSetMode(IEEE1284_ADDR);
	switch (write(PPDev, (void *) &value, 1)) {
	case 0:
		throw TException(string("Warning: no data (address"
					" mode) written to driver.\n"));
	case -1:
		throw TException(string("error writing address: ")
				+ strerror(errno));
	}
    
    #ifdef TB_DEBUG 
      G_Log.out() << "LPTWriteData " << value << "\n";
    #endif
    epp_delay();
} 

int TVMEEpp::LPTReadStatus()
{
	throw TException("LPTReadStatus: operation not supported");
}

void TVMEEpp::LPTWriteStatus(int value)
{
	throw TException("LPTWriteStatus: operation not supported");
}

void TVMEEpp::CheckEppStatus()
{                   
}*/


#define ECR_OFFSET      0x402
#define CONTROL_PORT    2

typedef unsigned char Byte; 


void begin_config_mode ( int chip )
{
  Byte init_code;

  switch(chip)
  {
  case 666:  init_code = 0x44; break;
  case 665:  init_code = 0x55; break;
  default:   fprintf(stderr, "Chip %d not supported!!!\n", chip); exit(1);
  }

  //disable();
  outb(init_code, 0x3f0);
  outb(init_code, 0x3f0);
  //enable();
}

void end_config_mode ( void )
{
  outb(0xaa, 0x3f0);
}

void begin_EPP( int port_addr, int chip )
{
  begin_config_mode( chip );
                                                                                                               
  outb(1, 0x3f0); 
  if (port_addr == 0x378)
    outb(0x96, 0x3f1); // use 0x378
  else
    outb(0x97, 0x3f1);  // use 0x278

  outb(4, 0x3f0);   // use CR4
  outb(3, 0x3f1);     // use EPP
                                                                                                               
  outb(0x34, port_addr + ECR_OFFSET);
                                                                                                               
                                                                                                               
  outb(0x00, port_addr + CONTROL_PORT);
  end_config_mode();
                                                                                                               
  outb(0x80, port_addr + ECR_OFFSET);  
                                                                                                               
  outb(0x04, port_addr + CONTROL_PORT);
}

TVMEEpp::TVMEEpp(const char *name)
{
  //cout << "TVMEEpp::TVMEEpp" << endl;
	int statusport = 0x378 + 1;
	int controlport = 0x378 + 2;
	unsigned char status;
	unsigned char ctl;
	
	//if (ioperm(0x378, 8, 1))
    if (iopl(3))
	  throw TException(string("ioperm failed: ") + strerror(errno));
	begin_EPP(0x378, 666);
	ctl = inb(controlport);
	ctl = (ctl &= 0xF0) | 0x4;
	outb(ctl, controlport);

	// clear timeouts
	if ((inb(statusport) & 0x01)) {
	  status = inb(statusport); 
	  outb(status | 0x01, statusport); 
	  outb(status & 0xfe, statusport); 
    }
    
    Init();
}

int TVMEEpp::LPTReadData()
{
  epp_delay();
  int value = inb(0x378 + 4);

  if (ShowDebugMsg)
    G_Log.out() << "LPTReadData " << (value & 0xff) << "\n";

  return value;
}
//---------------------------------------------------------------------------

void  TVMEEpp::LPTWriteData(int value)
{
  epp_delay();
  outb(value, 0x378 + 4);

  if (ShowDebugMsg)
    G_Log.out() << "LPTWriteData " << (value & 0xff) << "\n";
}
//---------------------------------------------------------------------------

int TVMEEpp::LPTReadAddress()
{
  epp_delay();
  int value = inb(0x378 + 3);
  
  if (ShowDebugMsg)
    G_Log.out() << "LPTReadAddress " << (value & 0xff) << "\n";

  return value;
}
//---------------------------------------------------------------------------

void TVMEEpp::LPTWriteAddress(int value)
{
  epp_delay();
  // Base + 3
  outb(value, 0x378 + 3);
  
  if (ShowDebugMsg)
    G_Log.out() << "LPTWriteAddress " << (value & 0xff) << "\n";
}
//---------------------------------------------------------------------------

int TVMEEpp::LPTReadStatus()
{
  epp_delay();
  int status = inb(0x378 + 1);

  if (ShowDebugMsg)
    G_Log.out() << "LPTReadStatus " << status << "\n";

  return status;
}

void TVMEEpp::LPTWriteStatus(int value)
{
  epp_delay();
  outb(value, 0x378 + 1);
  
  if (ShowDebugMsg)
    G_Log.out() << "LPTWriteStatus " << value << "\n";
}



void TVMEEpp::CheckEppStatus()
{                
  //return;
       
  const int EPP_STATUS_TIMEOUT = 0x01;
  const int EPP_STATUS_nERROR  = 0x08;
  const int EPP_STATUS_SELECT  = 0x10;
  const int EPP_STATUS_PAPEREND = 0x20;
  const int EPP_STATUS_nACK    = 0x40;
  const int EPP_STATUS_nBUSY   = 0x80;

  //cout << "TVMEEpp::CheckEppStatus" << endl;
  //return;

  int status = LPTReadStatus();
  if (status & EPP_STATUS_TIMEOUT) {
    // clear timeout
    LPTWriteStatus(status);

    // To clear timeout some chips require double read
    LPTReadStatus();
    status = LPTReadStatus();
    LPTWriteStatus(status | EPP_STATUS_TIMEOUT);  // Some reset by writing 1
    LPTWriteStatus(status & ~EPP_STATUS_TIMEOUT); // Others by writing 0
    if (LPTReadStatus() & EPP_STATUS_TIMEOUT)
      throw TException("Couldn't clear EPP timeout");
    else
      throw TException("EPP timeout");
  }     
  if ((status & EPP_STATUS_nERROR) == 0) {
    throw TException("EPP nERROR");
  }
  if ((status & EPP_STATUS_SELECT) == 0) {
    throw TException("EPP not SELECT");
  }
  if ((status & EPP_STATUS_nBUSY) == 0) {
    throw TException("EPP nBUSY");
  }
  //cout << "TVMEEpp::CheckEppStatus 1" << endl;
}

#else 			// not linux --- __BORLANDC__


int TVMEEpp::LPTReadData()
{
  int value = DLPortIO->Port[LPTAddress + 4];
  
  if (ShowDebugMsg)
    G_Log.out() << "LPTReadData " << (value & 0xff) << "\n";

  return value;
}
//---------------------------------------------------------------------------

void  TVMEEpp::LPTWriteData(int value)
{
  DLPortIO->Port[LPTAddress + 4] = value;
  
  if (ShowDebugMsg)
    G_Log.out() << "LPTWriteData " << (value & 0xff) << "\n";
}
//---------------------------------------------------------------------------

int TVMEEpp::LPTReadAddress()
{
  int value = DLPortIO->Port[LPTAddress + 3];
  
  if (ShowDebugMsg)
    G_Log.out() << "LPTReadAddress " << (value & 0xff) << "\n";

  return value;
}
//---------------------------------------------------------------------------

void TVMEEpp::LPTWriteAddress(int value)
{
  // Base + 3
  DLPortIO->Port[LPTAddress + 3] = value;
  
  if (ShowDebugMsg)
    G_Log.out() << "LPTWriteAddress " << (value & 0xff) << "\n";
}
//---------------------------------------------------------------------------

int TVMEEpp::LPTReadStatus()
{
  return DLPortIO->Port[LPTAddress + 1];
}

void TVMEEpp::LPTWriteStatus(int value)
{
  DLPortIO->Port[LPTAddress + 1] = value;
}



void TVMEEpp::CheckEppStatus()
{                   
  const int EPP_STATUS_TIMEOUT = 0x01;
  const int EPP_STATUS_nERROR  = 0x08;
  const int EPP_STATUS_SELECT  = 0x10;
  const int EPP_STATUS_PAPEREND = 0x20;
  const int EPP_STATUS_nACK    = 0x40;
  const int EPP_STATUS_nBUSY   = 0x80;

  //return;

  int status = LPTReadStatus();
  if (status & EPP_STATUS_TIMEOUT) {
    // clear timeout
    LPTWriteStatus(status);

    // To clear timeout some chips require double read
    LPTReadStatus();
    status = LPTReadStatus();
    LPTWriteStatus(status | EPP_STATUS_TIMEOUT);  // Some reset by writing 1
    LPTWriteStatus(status & ~EPP_STATUS_TIMEOUT); // Others by writing 0
    if (LPTReadStatus() & EPP_STATUS_TIMEOUT)
      throw TException("Couldn't clear EPP timeout");
    else    
      throw TException("EPP timeout");
  }     
  if ((status & EPP_STATUS_nERROR) == 0) {
    throw TException("EPP nERROR");
  }
  if ((status & EPP_STATUS_SELECT) == 0) {
    throw TException("EPP not SELECT");
  }
  if ((status & EPP_STATUS_nBUSY) == 0) {
    throw TException("EPP nBUSY");
  }
}

#endif	// linux

void TVMEEpp::Reset()
{
  LPTReadAddress();
}


void TVMEEpp::WriteVMEWrAddr(uint32_t address)
{
    WriteFunc(FUNC_VME_WR_ADDR);

    TAddress* addr = (TAddress*)(&address);

    LPTWriteData(addr->Address0);
    LPTWriteData(addr->Address1);
    LPTWriteData(addr->Address2);

    uint32_t readAddress = 0;
    TAddress* readAddr = (TAddress*)(&readAddress);

    readAddr->Address0 = LPTReadData();
    readAddr->Address1 = LPTReadData();
    readAddr->Address2 = LPTReadData();

    WriteFunc(FUNC_VME_ACCESS);
    if (address != readAddress) {
      std::ostringstream ost;
      ost << "Could not set VMEWrAddr. Expected value " << std::hex << address
          << " received value " << readAddress;
      throw EVME(ost.str());
    }
}

void TVMEEpp::WriteVMERdAddr(uint32_t address)
{
    WriteFunc(FUNC_VME_RD_ADDR);

    TAddress* addr = (TAddress*)(&address);

    LPTWriteData(addr->Address0);
    LPTWriteData(addr->Address1);
    LPTWriteData(addr->Address2);

    uint32_t readAddress = 0;
    TAddress* readAddr = (TAddress*)(&readAddress);

    readAddr->Address0 = LPTReadData();
    readAddr->Address1 = LPTReadData();
    readAddr->Address2 = LPTReadData();

    WriteFunc(FUNC_VME_ACCESS);
    if (address != readAddress) {
      std::ostringstream ost;
      ost << "Could not set VMERdAddr. Expected value " << std::hex << address
          << " received value " << readAddress;
      throw EVME(ost.str());
    }
}


void TVMEEpp::Init()
{
  Reset();
  EPPModeCheck = true;
  EPPModeDataLeftShift = 0;
  EPPModeDataSize = DATA_SIZE_32;
  EPPModeAddrSize = ADDR_SIZE_24;
  EPPModeAMInternal = true;

  VMEModeDTACKWaitHold = false;
  VMEModeAddrIncr = 0;
  VMEModeAddrStep = 0;
  VMEModeDataSize = DATA_SIZE_32;
  VMEModeAddrSize = ADDR_SIZE_24;

  AutoOptimize = false;
  LastWriteAddress = 0;
  LastWriteAddressCount = 0;
  LastReadAddress = 0;
  LastReadAddressCount = 0;
  AutoOptimizeAddressThreshold = 4;
  OrigEPPModeAddrSize = ADDR_SIZE_24;

  SetEppOperMode(EPPModeCheck, EPPModeDataLeftShift, EPPModeDataSize,
          EPPModeAddrSize, EPPModeAMInternal);
  SetVmeOperMode(VMEModeDTACKWaitHold, VMEModeAddrIncr, VMEModeAddrStep,
          VMEModeDataSize, VMEModeAddrSize);
}


void TVMEEpp::CheckAutoOptimizeWriteAddress(uint32_t address)
{
  if (!AutoOptimize)
    return;

  if (address == LastWriteAddress) {
    if (LastWriteAddressCount == AutoOptimizeAddressThreshold) {
      if (EPPModeAddrSize != ADDR_SIZE_0) {

        if (ShowDebugMsg)
          G_Log.out() << "<***** wlaczam ADDR_SIZE_0 (Write)*****\n";

        OrigEPPModeAddrSize = EPPModeAddrSize;
        SetEppOperMode(EPPModeCheck, EPPModeDataLeftShift, EPPModeDataSize,
          ADDR_SIZE_0, EPPModeAMInternal);
        if (ShowDebugMsg)
          G_Log.out() << ">****************************\n";
      }

      if (ShowDebugMsg)
        G_Log.out() << "<***** ustawiam VMEWrAddr na " << address << " *****(Write)\n";

      WriteVMEWrAddr(address);
      if (ShowDebugMsg)
        G_Log.out() << ">*******************\n";
    }
    LastWriteAddressCount++;
  }
  else {
    if (EPPModeAddrSize == ADDR_SIZE_0) {
      // revert the original EPPModeAddrSize
      if (ShowDebugMsg)
        G_Log.out() << "<***** ustawiam oryginalny addr size *****(Write)\n";

      SetEppOperMode(EPPModeCheck, EPPModeDataLeftShift, EPPModeDataSize,
        OrigEPPModeAddrSize, EPPModeAMInternal);
      if (ShowDebugMsg)
        G_Log.out() << ">***********\n";
    }
    LastWriteAddress = address;
    LastWriteAddressCount = 1;
  }
}

void TVMEEpp::CheckAutoOptimizeReadAddress(uint32_t address)
{
  if (!AutoOptimize)
    return;
    
  if (address == LastReadAddress) {
    if (LastReadAddressCount == AutoOptimizeAddressThreshold) {
      if (EPPModeAddrSize != ADDR_SIZE_0) {
        if (ShowDebugMsg)
          G_Log.out() << "<***** wlaczam ADDR_SIZE_0 (Read)*****\n";

        OrigEPPModeAddrSize = EPPModeAddrSize;
        SetEppOperMode(EPPModeCheck, EPPModeDataLeftShift, EPPModeDataSize,
          ADDR_SIZE_0, EPPModeAMInternal);
        if (ShowDebugMsg)
          G_Log.out() << ">****************************\n";
      }
      if (ShowDebugMsg)
        G_Log.out() << "<***** ustawiam VMERdAddr na " << address << " *****(Read)\n";

      WriteVMERdAddr(address);
      if (ShowDebugMsg)
        G_Log.out() << ">***********\n";
    }
    LastReadAddressCount++;
  }
  else {
    if (EPPModeAddrSize == ADDR_SIZE_0) {
      // revert the original EPPModeAddrSize
      if (ShowDebugMsg)
        G_Log.out() << "<***** ustawiam oryginalny addr size *****(Read)\n";

      SetEppOperMode(EPPModeCheck, EPPModeDataLeftShift, EPPModeDataSize,
        OrigEPPModeAddrSize, EPPModeAMInternal);
      if (ShowDebugMsg)
        G_Log.out() << ">***********\n";
    }
    LastReadAddress = address;
    LastReadAddressCount = 1;
  }
}

//---------------------------------------------------------------------------
/*
void TVMEEpp::Write(uint32_t address, char* data, size_t count)
{
  if (!(count/4))
    throw TException("TVMEEpp::Write: operation not supported for count / 4 != 0");

  for (size_t i = 0; i < count; i += 4)
    Write32(address + i, *(uint32_t*)(data + i));

  #ifdef TB_DEBUG
    G_Debug.AddMessage( "VMEWrite: address=", TDebug::VME_OPER );
    G_Debug.AddHexCont( address, TDebug::VME_OPER );
    G_Debug.AddMessageCont( " count=", TDebug::VME_OPER );
    G_Debug.AddDecCont( count, TDebug::VME_OPER );
    G_Debug.AddMessageLn( "", TDebug::VME_OPER );

    switch (count) {
    case 1:
        //fprintf( stderr, "   write: adr=%lx val=%x\n", (address), *data & 0xff );
        G_Debug.AddMessage( "   write: adr=", TDebug::VME_OPER );
        G_Debug.AddHexCont( address, TDebug::VME_OPER );
        G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
        G_Debug.AddHexCont( *data & 0xff, TDebug::VME_OPER );
        G_Debug.AddMessageLn( "", TDebug::VME_OPER );
        break;
    case 2:
        union {
            char t[2];
            unsigned short us;
        } u2;
        u2.t[0] = data[0];
        u2.t[1] = data[1];
        //fprintf( stderr, "   write: adr=%lx val=%x\n", (address),  u2.us );
        G_Debug.AddMessage( "   write: adr=", TDebug::VME_OPER );
        G_Debug.AddHexCont( address, TDebug::VME_OPER );
        G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
        G_Debug.AddHexCont( u2.us, TDebug::VME_OPER );
        G_Debug.AddMessageLn( "", TDebug::VME_OPER );
        break;
    default:
        unsigned int i;
        union {
            char t[4];
            uint32_t ul;
        } u4;
        for( i=0; i<count; i+=4 ) {
            u4.t[0]=data[i];
            u4.t[1]=data[i+1];
            u4.t[2]=data[i+2];
            u4.t[3]=data[i+3];
            //fprintf( stderr, "   write: adr=%lx val=%lx\n", (address+i),  u4.ul );
            //cerr << "   write: adr=" << hex << address << " val=" << u4.ul << endl;
            G_Debug.AddMessage( "   write: adr=", TDebug::VME_OPER );
            G_Debug.AddHexCont( address+i, TDebug::VME_OPER );
            G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
            G_Debug.AddHexCont( u4.ul, TDebug::VME_OPER );
            G_Debug.AddMessageLn( "", TDebug::VME_OPER );
        }
    }
  #endif
} */


void TVMEEpp::Write(uint32_t address, char* data, size_t count)
{
  if (VMEModeDataSize != DATA_SIZE_32)
    SetVmeOperMode(VMEModeDTACKWaitHold, VMEModeAddrIncr, VMEModeAddrStep,
                   DATA_SIZE_32, VMEModeAddrSize);

  if (!(count/4))
    throw TException("TVMEEpp::Write: operation not supported for count / 4 != 0");

  if ((count < 20) || (!AutoOptimize)) {
    for (size_t i = 0; i < count; i += 4)
      Write32(address + i, *(uint32_t*)(data + i));
  }
  else {
    const int MAX_COUNT = 2048;
    if (count > MAX_COUNT) {
      Write(address, data, MAX_COUNT);
      Write(address + MAX_COUNT, data + MAX_COUNT, count - MAX_COUNT);
      return;
    }
    // send first 4 bytes with address
    Write32(address, *(uint32_t*)(data));

    // send the rest with address auto incrementation
    bool oldEPPModeCheck = EPPModeCheck;
    int oldEPPModeAddrSize = EPPModeAddrSize;
    bool oldVMEModeAddrIncr = VMEModeAddrIncr;
    int oldVMEModeAddrStep = VMEModeAddrStep;
    SetEppOperMode(false, EPPModeDataLeftShift, EPPModeDataSize,
          ADDR_SIZE_0, EPPModeAMInternal);
    SetVmeOperMode(VMEModeDTACKWaitHold, true, 0, VMEModeDataSize, VMEModeAddrSize);

    int check;
    for (size_t i = 4; i < count; i += 4)  {
      //Write32(address + i, *(uint32_t*)(data + i));
      // send VME data
      TData* dat = (TData*)(data + i);
      check = dat->Data0;
      LPTWriteData(dat->Data0);
      //exit(0);
      if (EPPModeDataSize != DATA_SIZE_8) {
        check += dat->Data1;
        LPTWriteData(dat->Data1);
        if (EPPModeDataSize != DATA_SIZE_16) {
          check += dat->Data2;
          LPTWriteData(dat->Data2);
          if (EPPModeDataSize != DATA_SIZE_24) {
            check += dat->Data3;
            LPTWriteData(dat->Data3);
          }
        }
      }

      if (EPPModeCheck)
        LPTWriteData(check);

      try {
        CheckStatus();
      }
      catch(EVME& e) {
        ostringstream ost;
        ost << " Operation Write address=0x" << hex << address
            << " count=" << dec << count;
        e.what((e.what() + ost.str()).c_str());
        throw;
      }
    }

    SetEppOperMode(oldEPPModeCheck, EPPModeDataLeftShift, EPPModeDataSize,
          oldEPPModeAddrSize, EPPModeAMInternal);
    SetVmeOperMode(VMEModeDTACKWaitHold, oldVMEModeAddrIncr, oldVMEModeAddrStep,
          VMEModeDataSize, VMEModeAddrSize);
  }

  #ifdef TB_DEBUG
    G_Debug.AddMessage( "VMEWrite: address=", TDebug::VME_OPER );
    G_Debug.AddHexCont( address, TDebug::VME_OPER );
    G_Debug.AddMessageCont( " count=", TDebug::VME_OPER );
    G_Debug.AddDecCont( count, TDebug::VME_OPER );
    G_Debug.AddMessageLn( "", TDebug::VME_OPER );

    switch (count) {
    case 1:
        //fprintf( stderr, "   write: adr=%lx val=%x\n", (address), *data & 0xff );
        G_Debug.AddMessage( "   write: adr=", TDebug::VME_OPER );
        G_Debug.AddHexCont( address, TDebug::VME_OPER );
        G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
        G_Debug.AddHexCont( *data & 0xff, TDebug::VME_OPER );
        G_Debug.AddMessageLn( "", TDebug::VME_OPER );
        break;
    case 2:
        union {
            char t[2];
            unsigned short us;
        } u2;
        u2.t[0] = data[0];
        u2.t[1] = data[1];
        //fprintf( stderr, "   write: adr=%lx val=%x\n", (address),  u2.us );
        G_Debug.AddMessage( "   write: adr=", TDebug::VME_OPER );
        G_Debug.AddHexCont( address, TDebug::VME_OPER );
        G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
        G_Debug.AddHexCont( u2.us, TDebug::VME_OPER );
        G_Debug.AddMessageLn( "", TDebug::VME_OPER );
        break;
    default:
        unsigned int i;
        union {
            char t[4];
            uint32_t ul;
        } u4;
        for( i=0; i<count; i+=4 ) {
            u4.t[0]=data[i];
            u4.t[1]=data[i+1];
            u4.t[2]=data[i+2];
            u4.t[3]=data[i+3];

            //fprintf( stderr, "   write: adr=%lx val=%lx\n", (address+i),  u4.ul );
            //cerr << "   write: adr=" << hex << address << " val=" << u4.ul << endl;
            G_Debug.AddMessage( "   write: adr=", TDebug::VME_OPER );
            G_Debug.AddHexCont( address+i, TDebug::VME_OPER );
            G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
            G_Debug.AddHexCont( u4.ul, TDebug::VME_OPER );
            G_Debug.AddMessageLn( "", TDebug::VME_OPER );
        }
    }
  #endif
}


//---------------------------------------------------------------------------

void TVMEEpp::Read(uint32_t address, char* data, size_t count)
{
  if (VMEModeDataSize != DATA_SIZE_32)
    SetVmeOperMode(VMEModeDTACKWaitHold, VMEModeAddrIncr, VMEModeAddrStep,
                   DATA_SIZE_32, VMEModeAddrSize);

  if (!(count/4))
    throw TException("TVMEEpp::Read: operation not supported for count / 4 != 0");

  for (size_t i = 0; i < count; i += 4)
    *(uint32_t*)(data + i) = Read32(address + i);

  #ifdef TB_DEBUG
    switch (count) {
    case 1:
        //fprintf( stderr, "   read: adr=%lx val=%x\n", (address), *data & 0xff );
        G_Debug.AddMessage( "   read: adr=", TDebug::VME_OPER );
        G_Debug.AddHexCont( address, TDebug::VME_OPER );
        G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
        G_Debug.AddHexCont( *data & 0xff, TDebug::VME_OPER );
        G_Debug.AddMessageLn( "", TDebug::VME_OPER );
        break;
    case 2:
        union {
            char t[2];
            unsigned short us;
        } u2;
        u2.t[0] = data[0];
        u2.t[1] = data[1];
        //fprintf( stderr, "   read: adr=%lx val=%x\n", (address),  u2.us );
        G_Debug.AddMessage( "   read: adr=", TDebug::VME_OPER );
        G_Debug.AddHexCont( address, TDebug::VME_OPER );
        G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
        G_Debug.AddHexCont( u2.us, TDebug::VME_OPER );
        G_Debug.AddMessageLn( "", TDebug::VME_OPER );
        break;
    default:
        unsigned int i;
        union {
            char t[4];
            uint32_t ul;
        } u4;
        for( i=0; i<count; i+=4 ) {
            u4.t[0]=data[i];
            u4.t[1]=data[i+1];
            u4.t[2]=data[i+2];
            u4.t[3]=data[i+3];
            //fprintf( stderr, "   read: adr=%lx val=%lx\n", (address+i),  u4.ul );
            G_Debug.AddMessage( "   read: adr=", TDebug::VME_OPER );
            G_Debug.AddHexCont( address+i, TDebug::VME_OPER );
            G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
            G_Debug.AddHexCont( u4.ul, TDebug::VME_OPER );
            G_Debug.AddMessageLn( "", TDebug::VME_OPER );
        }
    }
  #endif
}

//---------------------------------------------------------------------------

void TVMEEpp::Write32(uint32_t address, uint32_t data)
{
  if (VMEModeDataSize != DATA_SIZE_32)
    SetVmeOperMode(VMEModeDTACKWaitHold, VMEModeAddrIncr, VMEModeAddrStep,
                   DATA_SIZE_32, VMEModeAddrSize);

  CheckAutoOptimizeWriteAddress(address);
  TAddress* addr = (TAddress*)(&address);

  TData* dat = (TData*)(&data);
  int check = 0;

  // send VME address
  if (EPPModeAddrSize != ADDR_SIZE_0) {
    check += addr->Address0;
    LPTWriteData(addr->Address0);
    if (EPPModeAddrSize != ADDR_SIZE_8) {
      check += addr->Address1;
      LPTWriteData(addr->Address1);
      if (EPPModeAddrSize != ADDR_SIZE_16) {
        check += addr->Address2;
        LPTWriteData(addr->Address2);
      }
    }
  }

  // send VME data
  check += dat->Data0;
  LPTWriteData(dat->Data0);
  if (EPPModeDataSize != DATA_SIZE_8) {
    check += dat->Data1;
    LPTWriteData(dat->Data1);
    if (EPPModeDataSize != DATA_SIZE_16) {
      check += dat->Data2;
      LPTWriteData(dat->Data2);
      if (EPPModeDataSize != DATA_SIZE_24) {
        check += dat->Data3;
        LPTWriteData(dat->Data3);
      }
    }
  }

  if (EPPModeCheck)
    LPTWriteData(check);

  try {
    CheckStatus();
  }
  catch(EVME& e) {
    ostringstream ost;
    ost << " Operation Write32 address=0x" << hex << address
        << " data=0x" << data;
    e.what((e.what() + ost.str()).c_str());
    throw;
  }

  #ifdef TB_DEBUG
    G_Debug.AddMessage( "Write32: adr=", TDebug::VME_OPER );
    G_Debug.AddHexCont( address, TDebug::VME_OPER );
    G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
    G_Debug.AddHexCont( data, TDebug::VME_OPER );
    G_Debug.AddMessageLn( "", TDebug::VME_OPER );
  #endif

}

void TVMEEpp::Write16(uint32_t address, unsigned short data)
{
  SetVmeOperMode(VMEModeDTACKWaitHold, VMEModeAddrIncr, VMEModeAddrStep,
                   DATA_SIZE_16, VMEModeAddrSize);
  UpdateEppOperMode();
  int oldEPPModeDataSize = EPPModeDataSize;
  SetEppModeDataSize(DATA_SIZE_16);
  try {
    CheckAutoOptimizeWriteAddress(address);
    TAddress* addr = (TAddress*)(&address);

    uint32_t ldata = data;
    //if (address & 0x2)
    //  ldata <<= 16;

    TData* dat = (TData*)(&ldata);
    int check = 0;

    // send VME address
    if (EPPModeAddrSize != ADDR_SIZE_0) {
      check += addr->Address0;
      LPTWriteData(addr->Address0);
      if (EPPModeAddrSize != ADDR_SIZE_8) {
        check += addr->Address1;
        LPTWriteData(addr->Address1);
        if (EPPModeAddrSize != ADDR_SIZE_16) {
          check += addr->Address2;
          LPTWriteData(addr->Address2);
        }
      }
    }

    check += dat->Data0;
    LPTWriteData(dat->Data0);
    if (EPPModeDataSize != DATA_SIZE_8) {
      check += dat->Data1;
      LPTWriteData(dat->Data1);
      if (EPPModeDataSize != DATA_SIZE_16) {
        check += dat->Data2;
        LPTWriteData(dat->Data2);
        if (EPPModeDataSize != DATA_SIZE_24) {
          check += dat->Data3;
          LPTWriteData(dat->Data3);
        }
      }
    }

    if (EPPModeCheck)
      LPTWriteData(check);

    try {
      CheckStatus();
    }
    catch(EVME& e) {
      ostringstream ost;
      ost << " Operation Write16 address=0x" << hex << address
          << " data=0x" << data;
      e.what((e.what() + ost.str()).c_str());
      throw;
    }

    #ifdef TB_DEBUG
      G_Debug.AddMessage( "Write16: adr=", TDebug::VME_OPER );
      G_Debug.AddHexCont( address, TDebug::VME_OPER );
      G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
      G_Debug.AddHexCont( data, TDebug::VME_OPER );
      G_Debug.AddMessageLn( "", TDebug::VME_OPER );
    #endif
  }
  catch(...) {
    SetVmeOperMode(VMEModeDTACKWaitHold, VMEModeAddrIncr, VMEModeAddrStep,
                   DATA_SIZE_32, VMEModeAddrSize);
    SetEppModeDataSize(oldEPPModeDataSize);
    throw;
  }
  SetVmeOperMode(VMEModeDTACKWaitHold, VMEModeAddrIncr, VMEModeAddrStep,
                   DATA_SIZE_32, VMEModeAddrSize);    
  SetEppModeDataSize(oldEPPModeDataSize);
}

//---------------------------------------------------------------------------
uint32_t TVMEEpp::Read32(uint32_t address)
{               
  if (VMEModeDataSize != DATA_SIZE_32)
    SetVmeOperMode(VMEModeDTACKWaitHold, VMEModeAddrIncr, VMEModeAddrStep,
                   DATA_SIZE_32, VMEModeAddrSize);

  CheckAutoOptimizeReadAddress(address);
  uint32_t data;
  TAddress* addr = (TAddress*)(&address);
  TData* dat = (TData*)(&data);     
  int check = 0;

  // send VME address
  if (EPPModeAddrSize != ADDR_SIZE_0) {
    check += addr->Address0;
    LPTWriteData(addr->Address0);
    if (EPPModeAddrSize != ADDR_SIZE_8) {
      check += addr->Address1;
      LPTWriteData(addr->Address1);
      if (EPPModeAddrSize != ADDR_SIZE_16) {
        check += addr->Address2;
        LPTWriteData(addr->Address2);
      }
    }
  }

  // get VME data
  check += (dat->Data0 = LPTReadData());
  if (EPPModeDataSize != DATA_SIZE_8) {
    check += (dat->Data1 = LPTReadData());
    if (EPPModeDataSize != DATA_SIZE_16) {
      check += (dat->Data2 = LPTReadData());
      if (EPPModeDataSize != DATA_SIZE_24)
        check += (dat->Data3 = LPTReadData());
    }
  }

  if (EPPModeCheck)
    LPTWriteData(check);

  try {
    CheckStatus();
  }
  catch(EVME& e) {
    ostringstream ost;
    ost << " Operation Read32 address=0x" << hex << address;
    e.what((e.what() + ost.str()).c_str());
    throw;
  }
  
  #ifdef TB_DEBUG
    G_Debug.AddMessage( "Read32: adr=", TDebug::VME_OPER );
    G_Debug.AddHexCont( address, TDebug::VME_OPER );
    G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
    G_Debug.AddHexCont( data, TDebug::VME_OPER );
    G_Debug.AddMessageLn( "", TDebug::VME_OPER );
  #endif

  return data;
}


unsigned short TVMEEpp::Read16(uint32_t address)
{
  SetVmeOperMode(VMEModeDTACKWaitHold, VMEModeAddrIncr, VMEModeAddrStep,
                   DATA_SIZE_16, VMEModeAddrSize);
  UpdateEppOperMode();
  int oldEPPModeDataSize = EPPModeDataSize;
  SetEppModeDataSize(DATA_SIZE_16);

  uint32_t data;
  try {
    CheckAutoOptimizeReadAddress(address);
    TAddress* addr = (TAddress*)(&address);
    TData* dat = (TData*)(&data);
    int check = 0;

    // send VME address
    if (EPPModeAddrSize != ADDR_SIZE_0) {
      check += addr->Address0;
      LPTWriteData(addr->Address0);
      if (EPPModeAddrSize != ADDR_SIZE_8) {
        check += addr->Address1;
        LPTWriteData(addr->Address1);
        if (EPPModeAddrSize != ADDR_SIZE_16) {
          check += addr->Address2;
          LPTWriteData(addr->Address2);
        }
      }
    }

    // get VME data
    check += (dat->Data0 = LPTReadData());
    if (EPPModeDataSize != DATA_SIZE_8) {
      check += (dat->Data1 = LPTReadData());
      if (EPPModeDataSize != DATA_SIZE_16) {
        check += (dat->Data2 = LPTReadData());
        if (EPPModeDataSize != DATA_SIZE_24)
          check += (dat->Data3 = LPTReadData());
      }
    }

    if (EPPModeCheck)
      LPTWriteData(check);

    try {
      CheckStatus();
    }
    catch(EVME& e) {
      ostringstream ost;
      ost << " Operation Read16 address=0x" << hex << address;
      e.what((e.what() + ost.str()).c_str());
      throw;
    }

    #ifdef TB_DEBUG
      G_Debug.AddMessage( "Read16: adr=", TDebug::VME_OPER );
      G_Debug.AddHexCont( address, TDebug::VME_OPER );
      G_Debug.AddMessageCont( " val=", TDebug::VME_OPER );
      G_Debug.AddHexCont( data, TDebug::VME_OPER );
      G_Debug.AddMessageLn( "", TDebug::VME_OPER );
    #endif

  }
  catch(...) {
    SetVmeOperMode(VMEModeDTACKWaitHold, VMEModeAddrIncr, VMEModeAddrStep,
                   DATA_SIZE_32, VMEModeAddrSize);
    SetEppModeDataSize(oldEPPModeDataSize);
    throw;
  }
  SetVmeOperMode(VMEModeDTACKWaitHold, VMEModeAddrIncr, VMEModeAddrStep,
                   DATA_SIZE_32, VMEModeAddrSize);
  SetEppModeDataSize(oldEPPModeDataSize);

  //return (address & 0x2) ? (data >> 16) : data;
  return data;
}
//---------------------------------------------------------------------------

void TVMEEpp::CheckStatus()
{
  CheckEppStatus();
  unsigned char status;
  boost::timer timer;   
  do {
    status = LPTReadAddress();
    if (timer.elapsed() > 10) {
      ostringstream ost;
      ost << ": status not correct after 10 seconds (" << hex << (int)status << ")." << flush;
      throw EVME(ost.str());
    }
  } while ((status & 0xf8) == 0);

  if (status & 0xc0)
    throw EVME(": error.");
  if (status & 0x20)
    throw EVME(": bus error.");
  if (status & 0x40)
    throw EVME(": checking error.");
  if (status & 0x80)
    throw EVME(": protocol error.");
  if (status & 0x08)
    throw EVME(": timeout.");
}
