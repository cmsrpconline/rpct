/*
 *  TVMEEpp.h		``VME over parallel port'' driver for Windows
 *			(Borland C++) and for Linux.
 *			
 *			The Linux vesion needs kernel vesion > 2.4.x
 *			(the ppdev driver).
 */
#ifndef TVMEEppH
#define TVMEEppH
//---------------------------------------------------------------------------

#include "TVMEInterface.h"
#include <sstream>

#ifdef linux
#    include <unistd.h>
#else
#    include "TDLPortIO.h"
#endif // linux


#include "rpct/std/ENotImplemented.h"

class TVMEEpp : public TVMEInterface {
public:
  static const int DATA_SIZE_32;
  static const int DATA_SIZE_24;
  static const int DATA_SIZE_16;
  static const int DATA_SIZE_8;

  static const int ADDR_SIZE_24;
  static const int ADDR_SIZE_16;
  static const int ADDR_SIZE_8;
  static const int ADDR_SIZE_0;

private:
    static const bool ShowDebugMsg = false;
#ifdef linux
    int PPDev;
#else
    TDLPortIO* DLPortIO;
    int LPTAddress;
#endif // linux

    // struct for data
    struct TAddress { // Address 24 bits
      char Address0;
      char Address1;
      char Address2;
    };

    struct TData { // Data 32 bits
      char Data0;
      char Data1;
      char Data2;
      char Data3;
    };

    int LPTReadData();
    void LPTWriteData(int value);
    int LPTReadAddress();
    void LPTWriteAddress(int value);
    int LPTReadStatus();
    void LPTWriteStatus(int value);

    void CheckStatus();

#ifdef linux
    int PPDevMode;
    void PPDevSetMode(int mode);
#endif

  void CheckEppStatus();


  static const int FUNC_VME_ACCESS = 0;
  static const int FUNC_VME_WR_ADDR = 1;
  static const int FUNC_VME_RD_ADDR = 2;
  static const int FUNC_VME_ADDR_MOD = 3;
  static const int FUNC_EPP_OPER_MODE = 4;
  static const int FUNC_VME_OPER_MODE = 5;
  static const int FUNC_TIME_OUT = 6;
  static const int FUNC_USER_REGISTER = 7;

  void WriteFunc(int func)
    {
      LPTWriteAddress(func);
    }

  void WriteVMEWrAddr(uint32_t address);
  void WriteVMERdAddr(uint32_t address);


  bool EPPModeCheck;
  int EPPModeDataLeftShift;
  int EPPModeDataSize;
  int EPPModeAddrSize;
  bool EPPModeAMInternal;

  bool VMEModeDTACKWaitHold;
  bool VMEModeAddrIncr;
  int VMEModeAddrStep;
  int VMEModeDataSize;
  int VMEModeAddrSize;

  // variables needed for AutoOptimize
  bool AutoOptimize;
  uint32_t LastWriteAddress;
  int LastWriteAddressCount;
  uint32_t LastReadAddress;
  int LastReadAddressCount;
  int AutoOptimizeAddressThreshold;
  int OrigEPPModeAddrSize;


  void Init();

  void CheckAutoOptimizeWriteAddress(uint32_t address);

  void CheckAutoOptimizeReadAddress(uint32_t address);


public:
#ifdef linux
    TVMEEpp(const char *name = "/dev/parport0");
#else
    TVMEEpp(int lptAddr = 0x378) : DLPortIO(NULL), LPTAddress(lptAddr)
      {
        DLPortIO = new TDLPortIO(NULL);
        DLPortIO->OpenDriver();
        Init();
      }
#endif //linux

    virtual ~TVMEEpp()
      {
#ifdef linux
        close(PPDev);
#else
        delete DLPortIO;
#endif //linux
      }

	virtual void Reset();
    
    virtual void SetDefaultAddressingMode(AddressingMode am) {
        throw ENotImplemented("TVMEEpp::SetDefaultAddressingMode");
    }
    
    virtual void Write(uint32_t address, char* data, size_t count);
    virtual void Write32(uint32_t address, uint32_t value);
    virtual void Write16(uint32_t address, unsigned short value);
    
    virtual void MultiWrite16(uint32_t* addresses, uint16_t* values, size_t count) {
        throw ENotImplemented("TVMEWinBit3::MultiWrite16");
    }
    
    virtual void MultiWrite32(uint32_t* addresses, uint32_t* values, size_t count){
        throw ENotImplemented("TVMEWinBit3::MultiWrite32");
    }

    virtual void Read(uint32_t address, char* data, size_t count);
    virtual uint32_t Read32(uint32_t address);
    virtual unsigned short Read16(uint32_t address);
    
    void SetEppOperMode(bool check, int dataLeftShift, int dataSize, int addrSize, bool amInternal)
      {
        if (ShowDebugMsg)
          G_Log.out() << "Setting EPP_MODE: "
                    << " check = " << (check ? "true" : "false")
                    << " dataLeftShift = " << dataLeftShift
                    << " dataSize = " << dataSize
                    << " addrSize = " << addrSize
                    << " amInternal = " << (amInternal ? "true" : "false")
                    << std::endl;

        WriteFunc(FUNC_EPP_OPER_MODE);
        int data = (check ? 0 : 0x80)
                     | ((dataLeftShift << 5) & 0x60)
                     | ((dataSize << 3) & 0x18)
                     | ((addrSize << 1) & 0x6)
                     | (amInternal ? 0 : 0x1);
        LPTWriteData(data);
        int readData = LPTReadData() & 0xff;
        WriteFunc(FUNC_VME_ACCESS);
        if (data != readData) {
          std::ostringstream ost;
          ost << "Could not set EPPOperMode. Expected value " << std::hex << data
              << " received value " << readData;
          throw EVME(ost.str());
        }

        EPPModeCheck = check;
        EPPModeDataLeftShift = dataLeftShift;
        EPPModeDataSize = dataSize;
        EPPModeAddrSize = addrSize;
        EPPModeAMInternal = amInternal;
      }
      
    void UpdateEppOperMode()
      {
        WriteFunc(FUNC_EPP_OPER_MODE);
        int data = LPTReadData();
        EPPModeCheck = (data & 0x80) == 0;
        EPPModeDataLeftShift = ((data >> 5) & 0x3);
        EPPModeDataSize = ((data >> 3) & 0x3);
        EPPModeAddrSize = ((data >> 1) & 0x3);
        EPPModeAMInternal = (data & 0x1) == 0;
        WriteFunc(FUNC_VME_ACCESS);
      }

    void SetEppModeDataSize(int dataSize)
      {
        if (dataSize != EPPModeDataSize) {
          SetEppOperMode(EPPModeCheck, EPPModeDataLeftShift, dataSize, EPPModeAddrSize,
                         EPPModeAMInternal);
        }
      }

    int GetEppModeDataSize()
      {
        return EPPModeDataSize;
      }

    void SetEppModeAddrSize(int addrSize)
      {
        if (addrSize != EPPModeAddrSize) {
          SetEppOperMode(EPPModeCheck, EPPModeDataLeftShift, EPPModeDataSize, addrSize,
                         EPPModeAMInternal);
        }
      }

    int GetEppModeAddrSize()
      {
        return EPPModeAddrSize;
      }

    void SetVmeOperMode(bool DTACKWaitHold, bool addrIncr, int addrStep, int dataSize,
                          int addrSize)
      {
        if (ShowDebugMsg)
          G_Log.out() << "Setting VME_MODE: "
                    << " DTACKWaitHold = " << (DTACKWaitHold ? "true" : "false")
                    << " addrIncr = " << addrIncr
                    << " addrStep = " << addrStep
                    << " dataSize = " << dataSize
                    << " addrSize = " << addrSize
                    << std::endl;
                    
        WriteFunc(FUNC_VME_OPER_MODE);
        int data = (DTACKWaitHold ? 0 : 0x80)
                     | (addrIncr ? 0x40 : 0)
                     | ((addrStep << 3) & 0x38)
                     | ((dataSize << 1) & 0x6)
                     | (dataSize & 0x1);
        LPTWriteData(data);
        int readData = LPTReadData() & 0xff;
        WriteFunc(FUNC_VME_ACCESS);
        if (data != readData) {
          std::ostringstream ost;
          ost << "Could not set VMEOperMode. Expected value " << std::hex << data
              << " received value " << readData;
          throw EVME(ost.str());
        }
          
        VMEModeDTACKWaitHold = DTACKWaitHold;
        VMEModeAddrIncr = addrIncr;
        VMEModeAddrStep = addrStep;
        VMEModeDataSize = dataSize;
        VMEModeAddrSize = addrSize;
      }

    void SetVmeOperMode(bool DTACKWaitHold, int dataSize, int addrSize)
      {
        SetVmeOperMode(DTACKWaitHold, false, 0, dataSize, addrSize);
      }

    void SetAutoOptimize(bool autoOptimize)
      {
        AutoOptimize = autoOptimize;
      }

    bool IsAutoOptimize()
      {
        return AutoOptimize;
      }

    void SetAutoOptimizeAddressThreshold(int threshold)
      {
        AutoOptimizeAddressThreshold = threshold;
      }

    int GetAutoOptimizeAddressThreshold()
      {
        return AutoOptimizeAddressThreshold;
      }
};

#endif

