#ifndef _RPCTPULSERINFO_H_
#define _RPCTPULSERINFO_H_


#include "xdata/Bag.h"
#include "xdata/Vector.h"
#include "xdata/String.h"
#include "xdata/Integer.h"

#include "rpct/xdaqdiagaccess/DiagId.h"

namespace rpct {
namespace xdaqdiagaccess {



class PulserInfo {
private:
    DiagIdBag id;
    xdata::Integer maxLength;
    xdata::Integer width;
    DiagIdBag diagCtrlId;
public:
    void registerFields(xdata::Bag<PulserInfo>* bag) {
        bag->addField("id", &id);
        bag->addField("maxLength", &maxLength);
        bag->addField("width", &width);
        bag->addField("diagCtrlId", &diagCtrlId);
    }    
    
    void init(DiagId id, int maxLength, int width, DiagId diagCtrlId) {
        this->id.bag.init(id);
        this->maxLength = maxLength;
        this->width = width;
        this->diagCtrlId.bag.init(diagCtrlId);
    }    
    
    
    DiagIdBag& getIdBag() {
    	return id;
    }
        
    xdata::Integer& getMaxLength(){
    	return maxLength;
    }

    xdata::Integer& getWidth(){
    	return width;
    }
    
        DiagIdBag& getDiagCtrlId() {
    	return diagCtrlId;
    }
    
    
    std::string toString() {
        return id.bag.toString();
    }
};
typedef xdata::Bag<PulserInfo> PulserInfoBag;

}}


#endif 
