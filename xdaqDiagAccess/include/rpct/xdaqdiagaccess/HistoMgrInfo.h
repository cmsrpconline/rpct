#ifndef _RPCTHISTOMGRINFO_H_
#define _RPCTHISTOMGRINFO_H_


#include "xdata/Bag.h"
#include "xdata/Vector.h"
#include "xdata/String.h"
#include "xdata/Integer.h"

#include "rpct/xdaqdiagaccess/DiagId.h"

namespace rpct {
namespace xdaqdiagaccess {



class HistoMgrInfo {
private:
    DiagIdBag id;
    xdata::Integer binCount;
    DiagIdBag diagCtrlId;
public:
    void registerFields(xdata::Bag<HistoMgrInfo>* bag) {
        bag->addField("id", &id);
        bag->addField("binCount", &binCount);
        bag->addField("diagCtrlId", &diagCtrlId);
    }    
    
    void init(DiagId id, int binCount, DiagId diagCtrlId) {
        this->id.bag.init(id);
        this->binCount = binCount;
        this->diagCtrlId.bag.init(diagCtrlId);
    }   
    
    DiagIdBag& getIdBag() {
    	return id;
    }    
    
    int getBinCount() {
    	return (int)binCount;
    }    

    DiagIdBag& getCtlIdBag() {
    	return diagCtrlId;
    }    
        
        
    std::string toString() {
        return id.bag.toString();
    }
};
typedef xdata::Bag<HistoMgrInfo> HistoMgrInfoBag;

}}


#endif /*_CRATEINFO_H_*/
