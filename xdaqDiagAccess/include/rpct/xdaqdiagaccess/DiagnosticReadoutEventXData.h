#ifndef _RPCTDIAGNOSTICREADOUTEVENTXDATA_H_
#define _RPCTDIAGNOSTICREADOUTEVENTXDATA_H_


#include "xdata/Bag.h"
#include "xdata/Vector.h"

#include "rpct/xdaqdiagaccess/DiagId.h"
#include "rpct/xdaqdiagaccess/BxDataXData.h"
#include "rpct/diag/TStandardDiagnosticReadout.h"

namespace rpct {
namespace xdaqdiagaccess {



class DiagnosticReadoutEventXData {
public:
    typedef xdata::Vector<BxDataBag> BxDataVector;
private:
    xdata::Integer bcn_;
    xdata::Integer evNum_;
    BxDataVector bxData_;
public:
    void registerFields(xdata::Bag<DiagnosticReadoutEventXData>* bag) {
        bag->addField("bcn", &bcn_);
        bag->addField("evNum", &evNum_);
        bag->addField("bxData", &bxData_);
    }    
    
    void init(int bcn, int evNum) {
        this->bcn_ = bcn;
        this->evNum_ = evNum;
    }     
    
    void init(IDiagnosticReadout::Event& e) {
        uint32_t bcn, evNum;
        
        TStandardDiagnosticReadout* r = dynamic_cast<TStandardDiagnosticReadout*>(&e.getEvents().getReadout());
        if (r == 0) {
            throw TException("DiagnosticReadoutEventXData::init: not a TStandardDiagnosticReadout");
        }
        r->ParseTimer(e, bcn, evNum);
        this->bcn_ = bcn;
        this->evNum_ = evNum;
                
        typedef IDiagnosticReadout::Event::BxDataList BxDataList;
        const BxDataList& bxDataList = e.getBxDataList();
        for (BxDataList::const_iterator iBxData = bxDataList.begin(); iBxData != bxDataList.end(); ++iBxData) {
            if ((*iBxData)->isNotEmpty()) {
                addBxData(**iBxData);
            }
        }
    } 
            
    int getBcn() {
    	return bcn_;
    }

    int getEvNum() {
    	return evNum_;
    }    

    BxDataVector& getBxData() {
        return bxData_;
    }    
    
    BxDataXData& addBxData() {
        bxData_.push_back(BxDataBag());
        return bxData_.back().bag;
    } 
    
    BxDataXData& addBxData(int index, int pos, int triggers, int channelsValid) {
        BxDataXData& bxData = addBxData();
        bxData.init(index, pos, triggers, channelsValid);
        return bxData;
    }
    
    BxDataXData& addBxData(IDiagnosticReadout::BxData& bx) {
        BxDataXData& bxData = addBxData();
        bxData.init(bx);
        return bxData;
    }
};
typedef xdata::Bag<DiagnosticReadoutEventXData> DiagnosticReadoutEventBag;

}}


#endif 
