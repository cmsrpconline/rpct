#ifndef _RPCTLMUXBXDATAXDATA_H_
#define _RPCTLMUXBXDATAXDATA_H_


#include "xdata/Bag.h"
#include "xdata/Vector.h"

#include "rpct/devices/LmuxBxData.h"
#include "rpct/xdaqdiagaccess/DiagId.h"

namespace rpct {
namespace xdaqdiagaccess {

class LMuxBxDataXData {
private:

    xdata::Integer partitionData_;
    xdata::Integer partitionNum_;
    xdata::Integer partitionDelay_;
    xdata::Integer endOfData_;
    xdata::Integer halfPartition_;
    xdata::Integer lbNum_;
    xdata::Integer index_;
public:
    void registerFields(xdata::Bag<LMuxBxDataXData>* bag) {
        bag->addField("partitionData", &partitionData_);
        bag->addField("partitionNum", &partitionNum_);
        bag->addField("partitionDelay", &partitionDelay_);
        bag->addField("endOfData", &endOfData_);
        bag->addField("halfPartition", &halfPartition_);
        bag->addField("lbNum", &lbNum_);
        bag->addField("index", &index_);
    }    
    
    void init(int partitionData, int partitionNum, int partitionDelay, int endOfData, int halfPartition, int lbNum, int index) {
        this->partitionData_ = partitionData;
        this->partitionNum_ = partitionNum;
        this->partitionDelay_ = partitionDelay;
        this->endOfData_ = endOfData;
        this->halfPartition_ = halfPartition;
        this->lbNum_ = lbNum;
        this->index_ = index;
    }     
    
    void init(LmuxBxData& d) {
        this->partitionData_ = d.getPartitionData();
        this->partitionNum_ = d.getPartitionNum();
        this->partitionDelay_ = d.getPartitionDelay();
        this->endOfData_ = d.getEndOfData();
        this->halfPartition_ = d.getHalfPartition();
        this->lbNum_ = d.getLbNum();
        this->index_ = d.getIndex();
    }    
    
    int getPartitionData() {
        return partitionData_;
    }   
    
    int getPartitionNum() {
        return partitionNum_;
    }   
    
    int getPartitionDelay() {
        return partitionDelay_;
    }   
    
    int getEndOfData() {
        return endOfData_;
    }   
    
    int getHalfPartition() {
        return halfPartition_;
    }   
    
    int getLbNum() {
        return lbNum_;
    } 
    
    int getIndex() {
        return index_;
    }
};

typedef xdata::Bag<LMuxBxDataXData> LMuxBxDataBag;

}}


#endif 
