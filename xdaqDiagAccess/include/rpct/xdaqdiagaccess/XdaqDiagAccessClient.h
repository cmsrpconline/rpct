#ifndef _RPCTXDAQDIAGACCESSCLIENT_H_
#define _RPCTXDAQDIAGACCESSCLIENT_H_

#include "xdaq/Application.h" 

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"

#include "xdata/Serializable.h"
#include "xdata/rpctsoap/Serializer.h"
#include "xdata/Binary.h"

#include "rpct/xdaqutils/XdaqSoapAccess.h"
#include "rpct/xdaqdiagaccess/XdaqDiagAccess.h"
#include "rpct/xdaqdiagaccess/HistoMgrInfo.h"
#include "rpct/xdaqdiagaccess/PulserInfo.h"
#include "rpct/xdaqdiagaccess/DiagnosticReadoutInfo.h"

#include "rpct/diag/IDiagnosable.h"
#include "rpct/diag/IDiagnosticReadout.h"

#include <list>
#include <map>

namespace rpct {
namespace xdaqdiagaccess {
 
class XdaqDiagAccessClient { 
protected:
    xdaq::Application* application;
    xdaqutils::XdaqSoapAccess soapAccess;
public:
    XdaqDiagAccessClient(xdaq::Application* app) 
    : application(app), soapAccess(app)  {}
    
    typedef xdaqutils::XdaqSoapAccess::TSOAPParamList TSOAPParamList;
    typedef xdata::Vector<xdata::Integer> DiagnosableIdVector;
    typedef xdata::Vector<DiagIdBag> DiagIdVector;  
     
     
    /*** General ***/ 
    void configureDiagnosable(DiagnosableIdVector& diagnosableIds, 
            IDiagnosable::TTriggerDataSel triggerDataSel, const char* className, 
            int appInstance) throw (xoap::exception::Exception) {        
        TSOAPParamList paramList;
        xdata::String xTriggerDataSel = XdaqDiagAccess::triggerDataSelToString(triggerDataSel);
        paramList.push_back(TSOAPParamList::value_type("diagnosableIds", &diagnosableIds));
        paramList.push_back(TSOAPParamList::value_type("triggerDataSel", &xTriggerDataSel));
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "sending configureDiagnosable request");
        xoap::MessageReference response = soapAccess.sendSOAPRequest(
            XdaqDiagAccess::CONFIGURE_DIAGNOSABLE_NAME, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_PREFIX, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS,             
            paramList, className, appInstance, false);  
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "configureDiagnosable request sent");        
    }
    
    void configureDiagnosable(DiagnosableIdVector& diagnosableIds, 
            IDiagnosable::TTriggerDataSel triggerDataSel, uint32_t triggerAreaEna,
            bool synFullOutSynchWindow, bool synPulseEdgeSynchWindow, const char* className, 
            int appInstance) throw (xoap::exception::Exception) {        
        TSOAPParamList paramList;
        xdata::String xTriggerDataSel = XdaqDiagAccess::triggerDataSelToString(triggerDataSel);
        xdata::Integer xTriggerAreaEna = triggerAreaEna;
        xdata::Boolean xSynFullOutSynchWindow = synFullOutSynchWindow;
        xdata::Boolean xSynPulseEdgeSynchWindowconst = synPulseEdgeSynchWindow;
        paramList.push_back(TSOAPParamList::value_type("diagnosableIds", &diagnosableIds));
        paramList.push_back(TSOAPParamList::value_type("triggerDataSel", &xTriggerAreaEna));
        paramList.push_back(TSOAPParamList::value_type("triggerAreaEna", &xTriggerAreaEna));
        paramList.push_back(TSOAPParamList::value_type("synFullOutSynchWindow", &xSynFullOutSynchWindow));
        paramList.push_back(TSOAPParamList::value_type("synPulseEdgeSynchWindowconst", &xSynPulseEdgeSynchWindowconst));
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "sending configureDiagnosable request");
        xoap::MessageReference response = soapAccess.sendSOAPRequest(
            XdaqDiagAccess::CONFIGURE_DIAGNOSABLE_LB_NAME, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_PREFIX, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS,             
            paramList, className, appInstance, false);  
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "configureDiagnosable request sent");        
    }
     
     
    /*** DiagCtrl ***/ 
     
    // !!! Caller should delete the result
    DiagIdVector* getDiagCtrlList(const char* className, int appInstance) {
        TSOAPParamList paramList;
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "sending getDiagCtrlList request");
        xoap::MessageReference response = soapAccess.sendSOAPRequest(
            XdaqDiagAccess::GET_DIAG_CTRL_LIST_NAME, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_PREFIX, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS,             
            paramList, className, appInstance, false);  
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "getDiagCtrlList request sent");
        
        DiagIdVector* diagIdVector = new DiagIdVector();
        soapAccess.parseSOAPResponse(response, 
            XdaqDiagAccess::GET_DIAG_CTRL_LIST_NAME, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_PREFIX, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS, 
            *diagIdVector);
        return diagIdVector;
    }
    
    void resetDiagCtrl(DiagIdVector& diagIds, const char* className, int appInstance) throw (xoap::exception::Exception) {        
        TSOAPParamList paramList;
        paramList.push_back(TSOAPParamList::value_type("diagCtrlIds", &diagIds));
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "sending resetDiagCtrl request");
        xoap::MessageReference response = soapAccess.sendSOAPRequest(
            XdaqDiagAccess::RESET_DIAG_CTRL_NAME, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_PREFIX, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS,             
            paramList, className, appInstance, false);  
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "resetDiagCtrl request sent");        
    } 
    
    void configureDiagCtrl(DiagIdVector& diagIds, xdata::Binary counterLimit,  
            IDiagCtrl::TTriggerType triggerType, const char* className, int appInstance) throw (xoap::exception::Exception) {        
        TSOAPParamList paramList;
        xdata::String sTriggerType = XdaqDiagAccess::triggerTypeToString(triggerType);
        paramList.push_back(TSOAPParamList::value_type("diagCtrlIds", &diagIds));
        paramList.push_back(TSOAPParamList::value_type("counterLimit", &counterLimit));
        paramList.push_back(TSOAPParamList::value_type("triggerType", &sTriggerType));
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "sending configureDiagCtrl request");
        xoap::MessageReference response = soapAccess.sendSOAPRequest(
            XdaqDiagAccess::CONFIGURE_DIAG_CTRL_NAME, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_PREFIX, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS,             
            paramList, className, appInstance, false);  
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "configureDiagCtrl request sent");        
    }
    
    void startDiagCtrl(DiagIdVector& diagIds, const char* className, int appInstance) throw (xoap::exception::Exception) {        
        TSOAPParamList paramList;
        paramList.push_back(TSOAPParamList::value_type("diagCtrlIds", &diagIds));
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "sending startDiagCtrl request");
        xoap::MessageReference response = soapAccess.sendSOAPRequest(
            XdaqDiagAccess::START_DIAG_CTRL_NAME, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_PREFIX, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS,             
            paramList, className, appInstance, false);  
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "startDiagCtrl request sent");        
    }  
    
    void stopDiagCtrl(DiagIdVector& diagIds, const char* className, int appInstance) throw (xoap::exception::Exception) {        
        TSOAPParamList paramList;
        paramList.push_back(TSOAPParamList::value_type("diagCtrlIds", &diagIds));
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "sending stopDiagCtrl request");
        xoap::MessageReference response = soapAccess.sendSOAPRequest(
            XdaqDiagAccess::STOP_DIAG_CTRL_NAME, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_PREFIX, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS,             
            paramList, className, appInstance, false);  
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "stopDiagCtrl request sent");        
    }  
        
    IDiagCtrl::TState getDiagCtrlState(DiagIdBag& diagCtrlId, const char* className, int appInstance) {
        TSOAPParamList paramList;
        paramList.push_back(TSOAPParamList::value_type("diagCtrlId", &diagCtrlId));
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "sending getDiagCtrlState request");
        xoap::MessageReference response = soapAccess.sendSOAPRequest(
            XdaqDiagAccess::GET_DIAG_CTRL_STATE_NAME, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_PREFIX, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS,             
            paramList, className, appInstance, false);  
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "getDiagCtrlState request sent");
        
        xdata::String stateString;
        soapAccess.parseSOAPResponse(response, 
            XdaqDiagAccess::GET_DIAG_CTRL_STATE_NAME, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_PREFIX, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS, 
            stateString);
        
        return XdaqDiagAccess::stringToDiagCtrlState(stateString);
    }    
        
    bool checkCountingEnded(DiagIdVector& diagIds, const char* className, int appInstance) {
        TSOAPParamList paramList;
        paramList.push_back(TSOAPParamList::value_type("diagCtrlIds", &diagIds));
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "sending checkCountingEnded request");
        xoap::MessageReference response = soapAccess.sendSOAPRequest(
            XdaqDiagAccess::CHECK_COUNTING_ENDED_NAME, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_PREFIX, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS,             
            paramList, className, appInstance, false);  
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "checkCountingEnded request sent");
        
        xdata::Boolean ended;
        soapAccess.parseSOAPResponse(response, 
            XdaqDiagAccess::CHECK_COUNTING_ENDED_NAME, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_PREFIX, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS, 
            ended);
        
        return (bool)ended;
    }


    /*** HistoMgr ***/ 

    typedef xdata::Vector<HistoMgrInfoBag> HistoMgrInfoVector;  
    
    // !!! Caller should delete the result
    HistoMgrInfoVector* getHistoMgrList(const char* className, int appInstance) {
        TSOAPParamList paramList;
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "sending getHistoMgrList request");
        xoap::MessageReference response = soapAccess.sendSOAPRequest(
            XdaqDiagAccess::GET_HISTO_MGR_LIST_NAME, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_PREFIX, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS,             
            paramList, className, appInstance, false);  
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "getHistoMgrList request sent");
        
        HistoMgrInfoVector* histoMgrInfoVector = new HistoMgrInfoVector();
        soapAccess.parseSOAPResponse(response, 
            XdaqDiagAccess::GET_HISTO_MGR_LIST_NAME, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_PREFIX, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS, 
            *histoMgrInfoVector);
        return histoMgrInfoVector;
    }
    
    void resetHistoMgr(DiagIdVector& histoMgrIds, const char* className, int appInstance) throw (xoap::exception::Exception) {        
        TSOAPParamList paramList;
        paramList.push_back(TSOAPParamList::value_type("histoMgrIds", &histoMgrIds));
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "sending resetHistoMgr request");
        xoap::MessageReference response = soapAccess.sendSOAPRequest(
            XdaqDiagAccess::RESET_HISTO_MGR_NAME, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_PREFIX, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS,             
            paramList, className, appInstance, false);  
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "resetHistoMgr request sent");        
    } 
    
    void resetHistoMgr(HistoMgrInfoVector& histoMgrInfos, const char* className, int appInstance) throw (xoap::exception::Exception) {                
        DiagIdVector histoMgrIds;
        for (HistoMgrInfoVector::iterator iInfo = histoMgrInfos.begin(); iInfo != histoMgrInfos.end(); iInfo++) {
        	histoMgrIds.push_back(iInfo->bag.getIdBag());
        }   
		resetHistoMgr(histoMgrIds, className, appInstance);  
    } 
    
    // !!! Caller should delete the result
    xdata::Vector<xdata::Binary>* readDataHistoMgr(DiagIdBag& histoMgrId, const char* className, int appInstance) {
        TSOAPParamList paramList;
        paramList.push_back(TSOAPParamList::value_type("histoMgrId", &histoMgrId));
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "sending readDataHistoMgr request");
        xoap::MessageReference response = soapAccess.sendSOAPRequest(
            XdaqDiagAccess::READ_DATA_HISTO_MGR_NAME, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_PREFIX, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS,             
            paramList, className, appInstance, false);  
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "readDataHistoMgr request sent");
        
        xdata::Vector<xdata::Binary>* data = new xdata::Vector<xdata::Binary>();
        soapAccess.parseSOAPResponse(response, 
            XdaqDiagAccess::READ_DATA_HISTO_MGR_NAME, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_PREFIX, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS, 
            *data);
        
        return data;
    }
    


    /*** Pulser ***/ 
    
    typedef xdata::Vector<PulserInfoBag> PulserInfoVector;  
    
    // !!! Caller should delete the result
    PulserInfoVector* getPulserList(const char* className, int appInstance) {
        TSOAPParamList paramList;
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "sending getPulserList request");
        xoap::MessageReference response = soapAccess.sendSOAPRequest(
            XdaqDiagAccess::GET_PULSER_LIST_NAME, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_PREFIX, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS,             
            paramList, className, appInstance, false);  
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "getPulserList request sent");
        
        PulserInfoVector* pulserInfoVector = new PulserInfoVector();
        soapAccess.parseSOAPResponse(response, 
            XdaqDiagAccess::GET_PULSER_LIST_NAME, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_PREFIX, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS, 
            *pulserInfoVector);
        return pulserInfoVector;
    }
    
    void configurePulser(DiagIdVector& pulserIds, xdata::Vector<xdata::Binary>& data,  
            xdata::Binary& pulseLength, bool repeat, const char* className, int appInstance) throw (xoap::exception::Exception) {        
        TSOAPParamList paramList;
        xdata::Boolean xRepeat = repeat;
        paramList.push_back(TSOAPParamList::value_type("pulserIds", &pulserIds));
        paramList.push_back(TSOAPParamList::value_type("data", &data));
        paramList.push_back(TSOAPParamList::value_type("pulseLength", &pulseLength));
        paramList.push_back(TSOAPParamList::value_type("repeat", &xRepeat));
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "sending configurePulser request");
        xoap::MessageReference response = soapAccess.sendSOAPRequest(
            XdaqDiagAccess::CONFIGURE_PULSER_NAME, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_PREFIX, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS,             
            paramList, className, appInstance, false);  
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "configurePulser request sent");        
    }
    
    void configurePulser(PulserInfoVector& pulserInfos, xdata::Vector<xdata::Binary>& data,  
            xdata::Binary& pulseLength, bool repeat, const char* className, int appInstance) throw (xoap::exception::Exception) {        
        // we need here only id's and not whole infos
        DiagIdVector pulserIds;
        for (PulserInfoVector::iterator iInfo = pulserInfos.begin(); iInfo != pulserInfos.end(); iInfo++) {
        	pulserIds.push_back(iInfo->bag.getIdBag());
        }
        configurePulser(pulserIds, data, pulseLength, repeat, className, appInstance);
    }
    
    
    /*** Diagnostic Readout ***/ 
    
    typedef xdata::Vector<DiagnosticReadoutInfoBag> DiagnosticReadoutInfoVector;  
    
    // !!! Caller should delete the result
    DiagnosticReadoutInfoVector* getDiagnosticReadoutList(const char* className, int appInstance) {
        TSOAPParamList paramList;
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "sending getDiagnosticReadoutList request");
        xoap::MessageReference response = soapAccess.sendSOAPRequest(
            XdaqDiagAccess::GET_DIAGNOSTIC_READOUT_LIST_NAME, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_PREFIX, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS,             
            paramList, className, appInstance, false);  
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "getDiagnosticReadoutList request sent");
        
        DiagnosticReadoutInfoVector* diagnosticReadoutInfoVector = new DiagnosticReadoutInfoVector();
        soapAccess.parseSOAPResponse(response, 
            XdaqDiagAccess::GET_DIAGNOSTIC_READOUT_LIST_NAME, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_PREFIX, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS, 
            *diagnosticReadoutInfoVector);
        return diagnosticReadoutInfoVector;
    }
    
    void configureDiagnosticReadout(DiagIdVector& diagReadoutIds, IDiagnosticReadout::TMasks& masks, unsigned int daqDelay, 
            const char* className, int appInstance) throw (xoap::exception::Exception) {        
        TSOAPParamList paramList;     
        
        XdaqDiagAccess::XMasks xMasks;
        for (IDiagnosticReadout::TMasks::iterator iMask = masks.begin(); iMask != masks.end(); iMask++) {
            XdaqDiagAccess::XMask xMask;
            for (IDiagnosticReadout::TMask::iterator iter = iMask->begin(); iter != iMask->end(); ++iter) {
                xMask.push_back(*iter);
            }
            xMasks.push_back(xMask);
        }    
        
        xdata::Integer xDaqDelay = daqDelay;
        
        paramList.push_back(TSOAPParamList::value_type("diagReadoutIds", &diagReadoutIds));
        paramList.push_back(TSOAPParamList::value_type("masks", &xMasks));
        paramList.push_back(TSOAPParamList::value_type("daqDelay", &xDaqDelay));
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "sending configureDiagnosticReadout request");
        xoap::MessageReference response = soapAccess.sendSOAPRequest(
            XdaqDiagAccess::CONFIGURE_DIAGNOSTIC_READOUT_NAME, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_PREFIX, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS,             
            paramList, className, appInstance, false);  
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "configureDiagnosticReadout request sent");       
                
    }
    
    void configureDiagnosticReadout(DiagIdVector& diagReadoutIds, IDiagnosticReadout::TMasks& masks, 
            unsigned int daqDelay, unsigned int dataSel, unsigned int extSel, 
            const char* className, int appInstance) throw (xoap::exception::Exception) {        
        TSOAPParamList paramList;     
        
        XdaqDiagAccess::XMasks xMasks;
        for (IDiagnosticReadout::TMasks::iterator iMask = masks.begin(); iMask != masks.end(); iMask++) {
            XdaqDiagAccess::XMask xMask;
            for (IDiagnosticReadout::TMask::iterator iter = iMask->begin(); iter != iMask->end(); ++iter) {
                xMask.push_back(*iter);
            }
            xMasks.push_back(xMask);
        }    
        
        xdata::Integer xDaqDelay = daqDelay;
        xdata::Integer xDataSel = dataSel;
        xdata::Integer xExtSel = extSel;
        
        paramList.push_back(TSOAPParamList::value_type("diagReadoutIds", &diagReadoutIds));
        paramList.push_back(TSOAPParamList::value_type("masks", &xMasks));
        paramList.push_back(TSOAPParamList::value_type("daqDelay", &xDaqDelay));
        paramList.push_back(TSOAPParamList::value_type("dataSel", &xDataSel));
        paramList.push_back(TSOAPParamList::value_type("extSel", &xExtSel));
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "sending configureDiagnosticReadout request");
        xoap::MessageReference response = soapAccess.sendSOAPRequest(
            XdaqDiagAccess::CONFIGURE_DIAGNOSTIC_READOUT_LB_NAME, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_PREFIX, 
            XdaqDiagAccess::RPCT_DIAG_ACCESS_NS,             
            paramList, className, appInstance, false);  
        LOG4CPLUS_DEBUG(application->getApplicationLogger(), "configureDiagnosticReadout request sent");       
                
    }
    

};
        
}
}


#endif 
