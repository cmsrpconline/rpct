#ifndef _RPCTXDAQDIAGACCESS_H_
#define _RPCTXDAQDIAGACCESS_H_

#include "rpct/diag/IDiag.h"
#include "rpct/diag/IDiagnosable.h"
#include "rpct/xdaqutils/XdaqSoapAccess.h"
#include "rpct/xdaqutils/PairBag.h"
#include "rpct/xdaqdiagaccess/DiagId.h"

#include "xdata/String.h"
#include "xdata/Integer.h"
#include "xdata/Vector.h"

#include "toolbox/BSem.h"


namespace rpct {
namespace xdaqdiagaccess {
 
class XdaqDiagAccess { 
public:
    typedef std::vector<IDiagnosable*> Diagnosables;
    typedef std::vector<IDiag*> Diags;
    typedef std::vector<IDiagCtrl*> DiagCtrls;
    
    static const char* RPCT_DIAG_ACCESS_NS;
    static const char* RPCT_DIAG_ACCESS_PREFIX;
    
    // general
    static std::string CONFIGURE_DIAGNOSABLE_NAME;
    static std::string CONFIGURE_DIAGNOSABLE_LB_NAME;
    
    //  diagCtrl  
    static std::string GET_DIAG_CTRL_LIST_NAME;
    static std::string RESET_DIAG_CTRL_NAME;
    static std::string CONFIGURE_DIAG_CTRL_NAME;
    static std::string START_DIAG_CTRL_NAME;
    static std::string STOP_DIAG_CTRL_NAME;
    static std::string GET_DIAG_CTRL_STATE_NAME;
    static std::string CHECK_COUNTING_ENDED_NAME; 
        
    // histoMgr
    static std::string GET_HISTO_MGR_LIST_NAME;
    static std::string RESET_HISTO_MGR_NAME;
    static std::string READ_DATA_HISTO_MGR_NAME;
    
    // pulser
    static std::string GET_PULSER_LIST_NAME;
    static std::string CONFIGURE_PULSER_NAME;
    static std::string CONFIGURE_PULSER_MUONS_NAME;
    
    // diagnostic Readout
    static std::string GET_DIAGNOSTIC_READOUT_LIST_NAME;
    static std::string CONFIGURE_DIAGNOSTIC_READOUT_NAME;
    static std::string CONFIGURE_DIAGNOSTIC_READOUT_LB_NAME;
    static std::string READ_DATA_DIAGNOSTIC_READOUT_NAME;
private:    
    xdaq::Application* application;
    log4cplus::Logger logger;
    rpct::xdaqutils::XdaqSoapAccess xdaqSoapAccess;
    
    Diagnosables diagnosables;
    Diags diags;
    DiagCtrls diagCtrls;  
    
    typedef std::map<int, IDiagnosable*> DiagnosableMap;
    DiagnosableMap diagnosableMap;
    
    typedef std::map<DiagId, IDiag*> DiagMap;
    DiagMap diagMap;
    
    typedef std::map<DiagId, IDiagCtrl*> DiagCtrlMap;
    DiagCtrlMap diagCtrlMap;   

    toolbox::BSem* hardwareMutex_;
    
    void mapDiagnosable(IDiagnosable& diagnosable);
    void mapDiag(IDiag& diag);
    void mapDiagCtrl(IDiagCtrl& diagCtrl);
    
    void takeHardware() {
        if (hardwareMutex_ != 0) {
            hardwareMutex_->take();
        }
    }
    
    void giveHardware() {
        if (hardwareMutex_ != 0) {
            hardwareMutex_->give();
        }
    }
    
public:
    XdaqDiagAccess(xdaq::Application* app, Diagnosables& d, toolbox::BSem* hardwareMutex = 0);
    
    /*
     * If this constructor
     *  is used, the Diagnosables must be added later with use of the void mapDiagnosables(Diagnosables& d)
     */
    XdaqDiagAccess(xdaq::Application* app, toolbox::BSem* hardwareMutex = 0);

    void mapDiagnosables(Diagnosables& d);

    /*
     * use in the resetFSM if you used the XdaqDiagAccess(xdaq::Application* app, toolbox::BSem* hardwareMutex = 0);
     */
    void clearDiagnosables();

    Diagnosables& getDiagnosables() {
    	return diagnosables;
    }
    
    Diags& getDiags() {
    	return diags;
    }
    
    DiagCtrls& getDiagCtrls() {
    	return diagCtrls;
    }
           
           
    // general    
    static std::string triggerDataSelToString(IDiagnosable::TTriggerDataSel triggerDataSel);
    static IDiagnosable::TTriggerDataSel stringToTriggerDataSel(const std::string& triggerDataSel);
    xoap::MessageReference configureDiagnosable(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference configureDiagnosableLB(xoap::MessageReference msg) throw (xoap::exception::Exception);
    
    
    //  diagCtrl            
    static IDiagCtrl::TState stringToDiagCtrlState(const std::string& state);    
    static std::string diagCtrlStateToString(IDiagCtrl::TState state);
    
    static std::string triggerTypeToString(IDiagCtrl::TTriggerType triggerType);
    static IDiagCtrl::TTriggerType stringToTriggerType(const std::string& triggerType);
     
    xoap::MessageReference getDiagCtrlList(xoap::MessageReference msg) throw (xoap::exception::Exception);    
    xoap::MessageReference resetDiagCtrl(xoap::MessageReference msg) throw (xoap::exception::Exception);  
    xoap::MessageReference configureDiagCtrl(xoap::MessageReference msg) throw (xoap::exception::Exception);  
    xoap::MessageReference startDiagCtrl(xoap::MessageReference msg) throw (xoap::exception::Exception);  
    xoap::MessageReference stopDiagCtrl(xoap::MessageReference msg) throw (xoap::exception::Exception);   
    xoap::MessageReference getDiagCtrlState(xoap::MessageReference msg) throw (xoap::exception::Exception);  
    xoap::MessageReference checkCountingEnded(xoap::MessageReference msg) throw (xoap::exception::Exception);          
    
    //  histoMgr 
    xoap::MessageReference getHistoMgrList(xoap::MessageReference msg) throw (xoap::exception::Exception);    
    xoap::MessageReference resetHistoMgr(xoap::MessageReference msg) throw (xoap::exception::Exception);    
    xoap::MessageReference readDataHistoMgr(xoap::MessageReference msg) throw (xoap::exception::Exception);
    
    //  pulser 
    xoap::MessageReference getPulserList(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference configurePulser(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference configurePulserMuons(xoap::MessageReference msg) throw (xoap::exception::Exception);
    
    //  diagnostic readout 
    typedef xdata::Vector<xdata::Integer> XMask;
    typedef xdata::Vector<XMask> XMasks;
    xoap::MessageReference getDiagnosticReadoutList(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference configureDiagnosticReadout(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference configureDiagnosticReadoutLB(xoap::MessageReference msg) throw (xoap::exception::Exception);   
    xoap::MessageReference readDataDiagnosticReadout(xoap::MessageReference msg) throw (xoap::exception::Exception);
};
        
}}


#endif 
