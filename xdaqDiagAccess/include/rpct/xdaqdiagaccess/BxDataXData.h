#ifndef _RPCTBXDATAXDATA_H_
#define _RPCTBXDATAXDATA_H_


#include "xdata/Bag.h"
#include "xdata/Vector.h"

#include "rpct/diag/TStandardDiagnosticReadout.h"

#include "rpct/devices/StandardBxData.h"

#include "rpct/xdaqdiagaccess/DiagId.h"
#include "rpct/xdaqdiagaccess/LMuxBxDataXData.h"
#include "rpct/xdaqdiagaccess/MuonXData.h"

namespace rpct {
namespace xdaqdiagaccess {

/*
    public final static String KEY_POS = "pos";
    public final static String KEY_TRIGGERS = "triggers";
    public final static String KEY_CHANNELSVALID = "channelsValid";
    public final static String KEY_LMUXBXDATA = "lmuxBxData";
    public final static String KEY_MUONS = "muons";*/
class BxDataXData {
public:
    typedef xdata::Vector<LMuxBxDataBag> LMuxBxDataVector;
    typedef xdata::Vector<MuonBag> MuonVector;
private:
	xdata::Integer index_;
    xdata::Integer pos_;
    xdata::Integer triggers_;
    xdata::Integer channelsValid_;
    LMuxBxDataVector lmuxBxData_;
    MuonVector muons_;
public:
    void registerFields(xdata::Bag<BxDataXData>* bag) {
        bag->addField("index", &index_);
        bag->addField("pos", &pos_);
        bag->addField("triggers", &triggers_);
        bag->addField("channelsValid", &channelsValid_);
        bag->addField("lmuxBxData", &lmuxBxData_);
        bag->addField("muons", &muons_);
    }    
       
    
    void init(int index, int pos, int triggers, int channelsValid) {
    	this->index_ = index;
        this->pos_ = pos;
        this->triggers_ = triggers;
        this->channelsValid_ = channelsValid;
    }    
    
    void init(int index, int pos, int triggers, int channelsValid, LMuxBxDataVector& lmuxBxData, MuonVector& muons) {
    	this->index_ = index;
        this->pos_ = pos;
        this->triggers_ = triggers;
        this->channelsValid_ = channelsValid;
        this->lmuxBxData_ = lmuxBxData;
        this->muons_ = muons;
    }    
    
    void init(IDiagnosticReadout::BxData& bxData) {
    	this->index_ = bxData.getIndex();
        this->pos_ = bxData.getPos();
        this->triggers_ = bxData.getTriggers();
        StandardBxData* s = dynamic_cast<StandardBxData*>(&bxData.getConvertedData());
        if (s == 0) {
            throw TException("BxDataXData::init: not a StandardBxData");
        }        
        this->channelsValid_ = s->getChannelsValid();
         
        typedef StandardBxData::LmuxBxDataVector LBDV;
        LBDV& lbdv = s->getLmuxBxDataVector();
        for (LBDV::iterator iLmuxData = lbdv.begin(); iLmuxData != lbdv.end(); ++iLmuxData) {
            //std::cout << "************ addLMuxBxData" << std::endl;
            addLMuxBxData(**iLmuxData);
        }
        
        typedef StandardBxData::MuonVector MuonVector;
        MuonVector& muonVector = s->getMuonVector();
        for (MuonVector::iterator iMuon = muonVector.begin(); iMuon != muonVector.end(); ++iMuon) { 
            //std::cout << "************ addMuon" << std::endl;
            addMuon(**iMuon);
        }    
    }    
    
    int getPos() {
        return pos_;
    }   
    
    int getTriggers() {
        return triggers_;
    }   
    
    int getChannelsValid() {
        return channelsValid_;
    }   
    
    LMuxBxDataVector& getLMuxBxData() {
        return lmuxBxData_;
    }   
    
    LMuxBxDataXData& addLMuxBxData() {
        lmuxBxData_.push_back(LMuxBxDataBag());
        return lmuxBxData_.back().bag;
    }
    
    LMuxBxDataXData& addLMuxBxData(int partitionData, int partitionNum, int partitionDelay, int endOfData, int halfPartition, int lbNum, int index) {
        LMuxBxDataXData& lmuxBxData = addLMuxBxData();
        lmuxBxData.init(partitionData, partitionNum, partitionDelay, endOfData, halfPartition, lbNum, index);        
        return lmuxBxData;
    }
    
    LMuxBxDataXData& addLMuxBxData(LmuxBxData& d) {
        LMuxBxDataXData& lmuxBxData = addLMuxBxData();
        lmuxBxData.init(d);        
        return lmuxBxData;
    }
    
    MuonVector& getMuons() {
        return muons_;
    }   
    
    MuonXData& addMuon() {
        muons_.push_back(MuonBag());
        return muons_.back().bag;
    }
    
    MuonXData& addMuon(int ptCode, int quality, int sign, int etaAddress, int phiAddress, int gbData, int index) {
        MuonXData& muon = addMuon();
        muon.init(ptCode, quality, sign, etaAddress, phiAddress, gbData, index);
        return muon;
    }
    
    MuonXData& addMuon(RPCHardwareMuon& m) {
        MuonXData& muon = addMuon();
        muon.init(m);
        return muon;
    }
};

typedef xdata::Bag<BxDataXData> BxDataBag;

}}


#endif 
