#ifndef _RPCTDIAGNOSTICREADOUTINFO_H_
#define _RPCTDIAGNOSTICREADOUTINFO_H_


#include "xdata/Bag.h"
#include "xdata/String.h"
#include "xdata/Integer.h"

#include "rpct/xdaqdiagaccess/DiagId.h"

namespace rpct {
namespace xdaqdiagaccess {



class DiagnosticReadoutInfo {
private:
    DiagIdBag id;
    
    xdata::Integer areaLen;
    xdata::Integer areaWidth;
    xdata::Integer maskNum;
    xdata::Integer maskWidth;
    xdata::Integer dataWidth;
    xdata::Integer trgNum;
    xdata::Integer timerWidth;    
    
    DiagIdBag diagCtrlId;
public:
    void registerFields(xdata::Bag<DiagnosticReadoutInfo>* bag) {
        bag->addField("id", &id);
        bag->addField("areaLen", &areaLen);
        bag->addField("areaWidth", &areaWidth);
        bag->addField("maskNum", &maskNum);
        bag->addField("maskWidth", &maskWidth);
        bag->addField("dataWidth", &dataWidth);
        bag->addField("trgNum", &trgNum);
        bag->addField("timerWidth", &timerWidth);
        bag->addField("diagCtrlId", &diagCtrlId);
    }    
    
    void init(DiagId id, int areaLen, int areaWidth, int maskNum, int maskWidth, 
            int dataWidth, int trgNum, int timerWidth, DiagId diagCtrlId) {
        this->id.bag.init(id);
        this->areaLen = areaLen;
        this->areaWidth = areaWidth;
        this->maskNum = maskNum;
        this->maskWidth = maskWidth;
        this->dataWidth = dataWidth;
        this->trgNum = trgNum;
        this->timerWidth = timerWidth;
        this->diagCtrlId.bag.init(diagCtrlId);
    }    
    
    
    DiagIdBag& getIdBag() {
    	return id;
    }    
    
    std::string toString() {
        return id.bag.toString();
    }
};
typedef xdata::Bag<DiagnosticReadoutInfo> DiagnosticReadoutInfoBag;

}}


#endif 
