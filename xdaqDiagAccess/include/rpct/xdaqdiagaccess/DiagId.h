#ifndef _RPCTDIAGID_H_
#define _RPCTDIAGID_H_


#include "xdata/Bag.h"
#include "xdata/String.h"
#include "xdata/Integer.h"
#include <sstream>

namespace rpct {
namespace xdaqdiagaccess {




class DiagId {
private:
    xdata::Integer ownerId;
    xdata::String name;
public:
    DiagId() {
    }
    
    DiagId(int ownerId, std::string name) {
        init(ownerId, name);
    }
    
    void registerFields(xdata::Bag<DiagId>* bag) {
        bag->addField("ownerId", &ownerId);
        bag->addField("name", &name);
    }    
    
    void init(int ownerId, std::string name) {
        this->ownerId = ownerId;
        this->name = name;
    }    
    
    void init(const DiagId& diagId) {
        this->ownerId = diagId.ownerId;
        this->name = diagId.name;
    }    
    
    std::string toString() {
        std::stringstream str;
        str << '(' << ownerId.toString() << ',' << name.toString() << ')';
        return str.str();
    }
    
    
    bool operator<(const DiagId& other) const { 
        return this->ownerId.value_ < other.ownerId.value_ || 
         (!(other.ownerId.value_ < (int)this->ownerId.value_) && this->name.value_ < other.name.value_); 
    }
    
    bool operator==(const DiagId& other) const { 
        return this->ownerId.value_ == other.ownerId.value_ && 
                this->name.value_ == other.name.value_; 
    }

};
typedef xdata::Bag<DiagId> DiagIdBag;




}}


#endif /*_CRATEINFO_H_*/
