#ifndef _RPCTDIAGNOSTICREADOUTDATAXDATA_H_
#define _RPCTDIAGNOSTICREADOUTDATAXDATA_H_


#include "xdata/Bag.h"
#include "xdata/Vector.h"
#include "xdata/Boolean.h"

#include "rpct/xdaqdiagaccess/DiagId.h"
#include "rpct/xdaqdiagaccess/DiagnosticReadoutEventXData.h"

namespace rpct {
namespace xdaqdiagaccess {



class DiagnosticReadoutDataXData {
public: 
    typedef xdata::Vector<DiagnosticReadoutEventBag> Events;
private:
    DiagIdBag id_;
    xdata::Boolean running_;
    xdata::Boolean lost_;
    Events events_;
public:
    void registerFields(xdata::Bag<DiagnosticReadoutDataXData>* bag) {
        bag->addField("id", &id_);
        bag->addField("running", &running_);
        bag->addField("lost", &lost_);
        bag->addField("events", &events_);
    }    
    
    void init(DiagId id, bool running, bool lost, Events& events) {
        this->id_.bag.init(id);
        this->running_ = running;
        this->lost_ = lost;
        this->events_ = events;
    }      
    
    void init(DiagId id, bool running, bool lost) {
        this->id_.bag.init(id);
        this->running_ = running;
        this->lost_ = lost;
    }         
    
    DiagIdBag& getIdBag() {
    	return id_;
    }
        
    bool isRunning(){
        return running_;
    }  
        
    bool isLost(){
    	return lost_;
    }   
    
    Events& getEvents() {
        return events_;
    } 
    
    DiagnosticReadoutEventXData& addEvent() {
        events_.push_back(DiagnosticReadoutEventBag());
        return events_.back().bag;
    }
    
    DiagnosticReadoutEventXData& addEvent(int bcn, int evNum) {
        DiagnosticReadoutEventXData& event = addEvent();
        event.init(bcn, evNum);
        return event;
    }
    
    DiagnosticReadoutEventXData& addEvent(IDiagnosticReadout::Event& e) {
        DiagnosticReadoutEventXData& event = addEvent();
        event.init(e);
        return event;
    }
    
    std::string toString() {
        return id_.bag.toString();
    }
};
typedef xdata::Bag<DiagnosticReadoutDataXData> DiagnosticReadoutDataBag;

}}


#endif 
