#ifndef _RPCTMUONXDATA_H_
#define _RPCTMUONXDATA_H_


#include "xdata/Bag.h"
#include "xdata/Vector.h"

#include "rpct/devices/RPCHardwareMuon.h"
#include "rpct/xdaqdiagaccess/DiagId.h"

namespace rpct {
namespace xdaqdiagaccess {



class MuonXData {
private:
    xdata::Integer ptCode_;
    xdata::Integer quality_;
    xdata::Integer sign_;
    xdata::Integer etaAddress_;
    xdata::Integer phiAddress_;
    xdata::Integer gbData_;
    xdata::Integer index_;
public:
    void registerFields(xdata::Bag<MuonXData>* bag) {
        bag->addField("ptCode", &ptCode_);
        bag->addField("quality", &quality_);
        bag->addField("sign", &sign_);
        bag->addField("etaAddress", &etaAddress_);
        bag->addField("phiAddress", &phiAddress_);
        bag->addField("gbData", &gbData_);
        bag->addField("index", &index_);
    }    
    
    void init(int ptCode, int quality, int sign, int etaAddress, int phiAddress, int gbData, int index) {
        this->ptCode_ = ptCode;
        this->quality_ = quality;
        this->sign_ = sign;
        this->etaAddress_ = etaAddress;
        this->phiAddress_ = phiAddress;
        this->gbData_ = gbData;
        this->index_ = index;
    }   
    
    void init(RPCHardwareMuon& muon) {
        this->ptCode_ = muon.getPtCode();
        this->quality_ = muon.getQuality();
        this->sign_ = muon.getSign();
        this->etaAddress_ = muon.getEtaAddr();
        this->phiAddress_ = muon.getPhiAddr();
        this->gbData_ = muon.getGBData();
        this->index_ = muon.getIndex();
    }    
            
    int getPtCode(){
    	return ptCode_;
    }  
            
    int getQuality(){
        return quality_;
    }  
            
    int getSign(){
        return sign_;
    }  
            
    int getEtaAddress(){
        return etaAddress_;
    }  
            
    int getPhiAddress(){
        return phiAddress_;
    }  
            
    int getGbData(){
        return gbData_;
    }  
            
    int getIndex(){
        return index_;
    }  
};

typedef xdata::Bag<MuonXData> MuonBag;

}}


#endif 
