/* 
 *  Author: Michal Pietrusinski
 *  Version: $Id: XdaqDiagAccess.cpp,v 1.35 2008/02/11 16:17:42 tb Exp $ 
 *  
 */
 
#include "rpct/xdaqdiagaccess/XdaqDiagAccess.h"

#include "xdaq/NamespaceURI.h"

#include "xdata/Serializable.h"
#include "xdata/exception/Exception.h"
#include "xdata/XStr.h"
#include "xdata/Bag.h"
#include "xdata/Boolean.h"
#include "xdata/Binary.h"
#include "xoap/DOMParser.h" 
#include "xcept/tools.h"
#include "tb_std.h"

#include "rpct/xdaqdiagaccess/HistoMgrInfo.h"
#include "rpct/xdaqdiagaccess/PulserInfo.h"
#include "rpct/xdaqdiagaccess/DiagnosticReadoutInfo.h"
#include "rpct/xdaqdiagaccess/DiagnosticReadoutDataXData.h"
#include "rpct/diag/IHistoMgr.h"
#include "rpct/diag/IPulser.h"
#include "rpct/diag/TStandardDiagnosticReadout.h"
#include "rpct/devices/ILBDiagnosticReadout.h"
#include "rpct/devices/ILBDiagnosable.h"
#include "rpct/devices/StandardBxData.h"
#include "rpct/hardwareTests/XmlBxDataReader.h"
#include "rpct/hardwareTests/DiagReadWriter.h"

#include <algorithm>
#include <unistd.h>

using namespace std;
using namespace rpct;
using namespace rpct::xdaqutils;

namespace rpct {
namespace xdaqdiagaccess {
    
const char* XdaqDiagAccess::RPCT_DIAG_ACCESS_NS = "urn:rpct-diag-access:1.0";
const char* XdaqDiagAccess::RPCT_DIAG_ACCESS_PREFIX = "rda";
    
std::string XdaqDiagAccess::CONFIGURE_DIAGNOSABLE_NAME = "configureDiagnosable";
std::string XdaqDiagAccess::CONFIGURE_DIAGNOSABLE_LB_NAME = "configureDiagnosableLB";

std::string XdaqDiagAccess::GET_DIAG_CTRL_LIST_NAME = "getDiagCtrlList";
std::string XdaqDiagAccess::RESET_DIAG_CTRL_NAME = "resetDiagCtrl";
std::string XdaqDiagAccess::CONFIGURE_DIAG_CTRL_NAME = "configureDiagCtrl";
std::string XdaqDiagAccess::START_DIAG_CTRL_NAME = "startDiagCtrl";
std::string XdaqDiagAccess::STOP_DIAG_CTRL_NAME = "stopDiagCtrl";
std::string XdaqDiagAccess::GET_DIAG_CTRL_STATE_NAME = "getStateDiagCtrl";
std::string XdaqDiagAccess::CHECK_COUNTING_ENDED_NAME = "checkCountingEndedDiagCtrl";

std::string XdaqDiagAccess::GET_HISTO_MGR_LIST_NAME = "getHistoMgrList";
std::string XdaqDiagAccess::RESET_HISTO_MGR_NAME = "resetHistoMgr";
std::string XdaqDiagAccess::READ_DATA_HISTO_MGR_NAME = "readDataHistoMgr";

std::string XdaqDiagAccess::GET_PULSER_LIST_NAME = "getPulserList";
std::string XdaqDiagAccess::CONFIGURE_PULSER_NAME = "configurePulser";
std::string XdaqDiagAccess::CONFIGURE_PULSER_MUONS_NAME = "configurePulserMuons";

std::string XdaqDiagAccess::GET_DIAGNOSTIC_READOUT_LIST_NAME = "getDiagnosticReadoutList";
std::string XdaqDiagAccess::CONFIGURE_DIAGNOSTIC_READOUT_NAME = "configureDiagnosticReadout";
std::string XdaqDiagAccess::CONFIGURE_DIAGNOSTIC_READOUT_LB_NAME = "configureDiagnosticReadoutLB";
std::string XdaqDiagAccess::READ_DATA_DIAGNOSTIC_READOUT_NAME = "readDataDiagnosticReadout";

    
XdaqDiagAccess::XdaqDiagAccess(xdaq::Application* app, Diagnosables& d, toolbox::BSem* hardwareMutex) 
: application(app), logger(app->getApplicationLogger()), xdaqSoapAccess(app), diagnosables(d), hardwareMutex_(hardwareMutex) {
/*    for (Diagnosables::iterator iDiagnosable = diagnosables.begin(); iDiagnosable != diagnosables.end(); ++iDiagnosable) {
        LOG4CPLUS_DEBUG(logger, "Mapping diagnosable " << (*iDiagnosable)->getId());
        mapDiagnosable(**iDiagnosable);
    }  */
	mapDiagnosables(d);
}

XdaqDiagAccess::XdaqDiagAccess(xdaq::Application* app, toolbox::BSem* hardwareMutex)
: application(app), logger(app->getApplicationLogger()), xdaqSoapAccess(app), hardwareMutex_(hardwareMutex) {

}

void XdaqDiagAccess::mapDiagnosables(Diagnosables& d) {
	diagnosables = d;
    for (Diagnosables::iterator iDiagnosable = diagnosables.begin(); iDiagnosable != diagnosables.end(); ++iDiagnosable) {
        LOG4CPLUS_DEBUG(logger, "Mapping diagnosable " << (*iDiagnosable)->getId());
        mapDiagnosable(**iDiagnosable);
    }
}

void XdaqDiagAccess::clearDiagnosables() {
    diagnosableMap.clear();
    diagMap.clear();
    diagCtrlMap.clear();
}

void XdaqDiagAccess::mapDiagnosable(IDiagnosable& diagnosable) {
    if (diagnosableMap.find(diagnosable.getId()) != diagnosableMap.end()) {
        LOG4CPLUS_WARN(logger, "Warning! Duplicate diagnosable " << diagnosable.getId());
        return; 
    }
    diagnosableMap.insert(DiagnosableMap::value_type(diagnosable.getId(), &diagnosable));
    
    //LOG4CPLUS_DEBUG(logger, "copy1 diagnosableMap.insert " << diagnosable.getId());
    for (DiagCtrls::iterator iDiagCtrl = diagnosable.GetDiagCtrls().begin(); iDiagCtrl != diagnosable.GetDiagCtrls().end(); ++iDiagCtrl) {
        //DiagCtrl* diagCtrl = *iDiagCtrl;
        //if (diagCtrl)
        mapDiagCtrl(**iDiagCtrl);
    }
    //LOG4CPLUS_DEBUG(logger, "copy1 diagnosableMap.insert " << diagnosable.getId() << " end");
    
    //copy(diagnosable.GetDiags().begin(), diagnosable.GetDiags().end(), diags.end());
    for (Diags::iterator iDiag = diagnosable.GetDiags().begin(); iDiag != diagnosable.GetDiags().end(); ++iDiag) {
        mapDiag(**iDiag);
    } 
}
    
    
void XdaqDiagAccess::mapDiag(IDiag& diag) {
	diags.push_back(&diag);
    DiagId id(diag.GetOwner().getId(), diag.GetName());
    LOG4CPLUS_DEBUG(logger, "Mapping diag " << id.toString());
    if (diagMap.find(id) != diagMap.end()) {
        LOG4CPLUS_WARN(logger, "Warning! Duplicate diag " << id.toString());
        return; 
    }
    diagMap.insert(DiagMap::value_type(id, &diag));
    //mapDiagCtrl(diag.GetDiagCtrl());
}

void XdaqDiagAccess::mapDiagCtrl(IDiagCtrl& diagCtrl) {
	diagCtrls.push_back(&diagCtrl);
    DiagId id(diagCtrl.GetOwner().getId(), diagCtrl.GetName());
    LOG4CPLUS_DEBUG(logger, "Mapping diagCtrl " << id.toString());
    if (diagCtrlMap.find(id) != diagCtrlMap.end()) {
        LOG4CPLUS_WARN(logger, "Warning! Duplicate diagCtrl " << id.toString());
        return; 
    }
    diagCtrlMap.insert(DiagCtrlMap::value_type(id, &diagCtrl));
}



std::string XdaqDiagAccess::triggerDataSelToString(IDiagnosable::TTriggerDataSel triggerDataSel) {

    switch (triggerDataSel) {
    case IDiagnosable::tdsNone:
        return "NONE";
        break;
    case IDiagnosable::tdsL1A:
        return "L1A";
        break;
    case IDiagnosable::tdsPretrigger0:
        return "PRETRIGGER_0";
        break;
    case IDiagnosable::tdsPretrigger1:
        return "PRETRIGGER_1";
        break;
    case IDiagnosable::tdsPretrigger2:
        return "PRETRIGGER_2";
        break;
    case IDiagnosable::tdsBCN0:
        return "BCN0";
        break;
    case IDiagnosable::tdsLocal:
        return "LOCAL";
        break;
    default:
        throw TException("Invalid triggerDataSel");
    }
}
   
IDiagnosable::TTriggerDataSel XdaqDiagAccess::stringToTriggerDataSel(const std::string& triggerDataSel) {
    if (triggerDataSel == "NONE") {
        return IDiagnosable::tdsNone;
    }
    if (triggerDataSel == "L1A") {
        return IDiagnosable::tdsL1A;
    }
    if (triggerDataSel == "PRETRIGGER_0") {
        return IDiagnosable::tdsPretrigger0;
    }
    if (triggerDataSel == "PRETRIGGER_1") {
        return IDiagnosable::tdsPretrigger1;
    }
    if (triggerDataSel == "PRETRIGGER_2") {
        return IDiagnosable::tdsPretrigger2;
    }
    if (triggerDataSel == "BCN0") {
        return IDiagnosable::tdsBCN0;
    }
    if (triggerDataSel == "LOCAL") {
        return IDiagnosable::tdsLocal;
    }
    throw TException("Invalid triggerDataSel '" + triggerDataSel + "'.");
}

xoap::MessageReference XdaqDiagAccess::configureDiagnosable(xoap::MessageReference msg) throw (xoap::exception::Exception) { 
    
    typedef xdata::Vector<xdata::Integer> DiagnosableIds;
    DiagnosableIds diagnosableIds;
    xdata::String triggerDataSel;   
    xdata::Integer dataTrgDelay;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("diagnosableIds", &diagnosableIds));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("triggerDataSel", &triggerDataSel));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("dataTrgDelay", &dataTrgDelay));
    
    xdaqSoapAccess.parseSOAPRequest(msg, CONFIGURE_DIAGNOSABLE_NAME, RPCT_DIAG_ACCESS_PREFIX, RPCT_DIAG_ACCESS_NS, paramMap);     
    IDiagnosable::TTriggerDataSel tds = stringToTriggerDataSel((string) triggerDataSel);
    cout << "*********** triggerDataSel " << (string)triggerDataSel << " tds = " << tds << endl; 
    
    
    for (DiagnosableIds::iterator iDiagId = diagnosableIds.begin(); iDiagId != diagnosableIds.end(); ++iDiagId) {
        DiagnosableMap::iterator iDiagnosableEntry = diagnosableMap.find(*iDiagId);
        if (iDiagnosableEntry == diagnosableMap.end()) {
            XCEPT_RAISE(xoap::exception::Exception, "Could not find diagnosable with id = " + iDiagId->toString());
        }
        
        takeHardware();
        try {
            iDiagnosableEntry->second->ConfigureDiagnosable(tds, dataTrgDelay);
            giveHardware();
        }
        catch (TException& e) {
            giveHardware();
            LOG4CPLUS_ERROR(logger, e.what());
            XCEPT_RAISE(xoap::exception::Exception, e.what());
        }
    }     
    xoap::MessageReference reply = xoap::createMessage();
    return reply;                              
}

xoap::MessageReference XdaqDiagAccess::configureDiagnosableLB(xoap::MessageReference msg) throw (xoap::exception::Exception) {
    typedef xdata::Vector<xdata::Integer> DiagnosableIds;
    DiagnosableIds diagnosableIds;
    xdata::String triggerDataSel;    
    xdata::Integer dataTrgDelay;
    xdata::Integer triggerAreaEna;   
    xdata::Boolean synFullOutSynchWindow;   
    xdata::Boolean synPulseEdgeSynchWindow;    
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("diagnosableIds", &diagnosableIds));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("triggerDataSel", &triggerDataSel));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("dataTrgDelay", &dataTrgDelay));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("triggerAreaEna", &triggerAreaEna));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("synFullOutSynchWindow", &synFullOutSynchWindow));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("synPulseEdgeSynchWindow", &synPulseEdgeSynchWindow));
        
    xdaqSoapAccess.parseSOAPRequest(msg, CONFIGURE_DIAGNOSABLE_LB_NAME, RPCT_DIAG_ACCESS_PREFIX, RPCT_DIAG_ACCESS_NS, paramMap);     
      
    for (DiagnosableIds::iterator iDiagId = diagnosableIds.begin(); iDiagId != diagnosableIds.end(); ++iDiagId) {
        DiagnosableMap::iterator iDiagnosableEntry = diagnosableMap.find(*iDiagId);
        if (iDiagnosableEntry == diagnosableMap.end()) {
            XCEPT_RAISE(xoap::exception::Exception, "Could not find diagnosable with id = " + iDiagId->toString());
        }
        
        ILBDiagnosable* lbDiagnosable = dynamic_cast<ILBDiagnosable*>(iDiagnosableEntry->second);
        if (lbDiagnosable == 0) {
            XCEPT_RAISE(xoap::exception::Exception, "The diagnosable is not a ILBDiagnosable, id = " + iDiagId->toString());
        }        
        
        takeHardware();
        try {          
            lbDiagnosable->ConfigureDiagnosable(stringToTriggerDataSel((string) triggerDataSel),
                    dataTrgDelay, triggerAreaEna, synFullOutSynchWindow, synPulseEdgeSynchWindow);
            giveHardware();
        }
        catch (TException& e) {
            giveHardware();
            LOG4CPLUS_ERROR(logger, e.what());
            XCEPT_RAISE(xoap::exception::Exception, e.what());
        }    
    }     
    xoap::MessageReference reply = xoap::createMessage();
    return reply;           
}



               
xoap::MessageReference XdaqDiagAccess::getDiagCtrlList(xoap::MessageReference msg) 
throw (xoap::exception::Exception) {     
	LOG4CPLUS_DEBUG(logger, __FUNCTION__);
    xdata::Vector<DiagIdBag> diagCtrlIds;    
    for (DiagCtrlMap::iterator iEntry = diagCtrlMap.begin(); iEntry != diagCtrlMap.end(); ++iEntry) {
        DiagIdBag diagCtrlIdBag;
        diagCtrlIdBag.bag.init(iEntry->first);
        diagCtrlIds.push_back(diagCtrlIdBag);
    }
    try {
        return xdaqSoapAccess.sendSOAPResponse(GET_DIAG_CTRL_LIST_NAME, RPCT_DIAG_ACCESS_PREFIX, 
            RPCT_DIAG_ACCESS_NS, diagCtrlIds);   
    }
    catch (TException& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        XCEPT_RAISE(xoap::exception::Exception, e.what());
    }
    LOG4CPLUS_DEBUG(logger, __FUNCTION__<<" done");
}   


xoap::MessageReference XdaqDiagAccess::resetDiagCtrl(xoap::MessageReference msg) 
throw (xoap::exception::Exception) {     
    typedef xdata::Vector<DiagIdBag> DiagIds;
    DiagIds diagIds;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("diagCtrlIds", &diagIds));
    xdaqSoapAccess.parseSOAPRequest(msg, RESET_DIAG_CTRL_NAME, RPCT_DIAG_ACCESS_PREFIX, RPCT_DIAG_ACCESS_NS, paramMap);     
            
    for (DiagIds::iterator iDiagId = diagIds.begin(); iDiagId != diagIds.end(); ++iDiagId) {
        DiagCtrlMap::iterator iDiagCtrlEntry = diagCtrlMap.find(iDiagId->bag);
        if (iDiagCtrlEntry == diagCtrlMap.end()) {
            XCEPT_RAISE(xoap::exception::Exception, "Could not find diagCtrl with id = " + iDiagId->bag.toString());
        }
        IDiagCtrl& diagCtrl = *iDiagCtrlEntry->second;
        
        takeHardware();
        try {    
            diagCtrl.Reset();
            giveHardware();
        }
        catch (TException& e) {
            giveHardware();
            LOG4CPLUS_ERROR(logger, e.what());
            XCEPT_RAISE(xoap::exception::Exception, e.what());
        }  
    }                   
    xoap::MessageReference reply = xoap::createMessage();
    return reply;                                              
}   


IDiagCtrl::TState XdaqDiagAccess::stringToDiagCtrlState(const std::string& state){
    if (state == "INIT") {
        return IDiagCtrl::sInit;
    }
    if (state == "RESET") {
        return IDiagCtrl::sReset;
    }
    if (state == "IDLE") {
        return IDiagCtrl::sIdle;
    }
    if (state == "RUNNING") {
        return IDiagCtrl::sRunning;
    }
    throw TException("Invalid diagCtrl state '" + state + "'.");
}


std::string XdaqDiagAccess::diagCtrlStateToString(IDiagCtrl::TState state) {
    switch (state) {
        case IDiagCtrl::sInit: 
            return "INIT";
        case IDiagCtrl::sReset:
            return "RESET";
        case IDiagCtrl::sIdle:
            return "IDLE";
        case IDiagCtrl::sRunning:
            return "RUNNING";
        default:
            ostringstream ostr;
            ostr << "Invalid diagCtrlState " << ((int)state); 
            throw TException(ostr.str());
    }
}

std::string XdaqDiagAccess::triggerTypeToString(IDiagCtrl::TTriggerType triggerType) {    	    	
	switch (triggerType) {
		case IDiagCtrl::ttManual:
			return "MANUAL";
			break;
		case IDiagCtrl::ttL1A:
			return "L1A";
			break;
		case IDiagCtrl::ttPretrigger0:
			return "PRETRIGGER_0";
			break;
		case IDiagCtrl::ttPretrigger1:
			return "PRETRIGGER_1";
			break;
		case IDiagCtrl::ttPretrigger2:
			return "PRETRIGGER_2";
			break;
		case IDiagCtrl::ttBCN0:
			return "BCN0";
			break;
		case IDiagCtrl::ttLocal:
			return "LOCAL";
			break;
		default:
		    throw TException("Invalid trigger type");
	}
}

IDiagCtrl::TTriggerType XdaqDiagAccess::stringToTriggerType(const std::string& triggerType) {
	if (triggerType == "MANUAL") {
		return IDiagCtrl::ttManual;
	}
	if (triggerType == "L1A") {
		return IDiagCtrl::ttL1A;
	}
	if (triggerType == "PRETRIGGER_0") {
		return IDiagCtrl::ttPretrigger0;
	}
	if (triggerType == "PRETRIGGER_1") {
		return IDiagCtrl::ttPretrigger1;
	}
	if (triggerType == "PRETRIGGER_2") {
		return IDiagCtrl::ttPretrigger2;
	}
	if (triggerType == "BCN0") {
		return IDiagCtrl::ttBCN0;
	}
	if (triggerType == "LOCAL") {
		return IDiagCtrl::ttLocal;
	}
	throw TException("Invalid trigger type '" + triggerType + "'.");
}

xoap::MessageReference XdaqDiagAccess::configureDiagCtrl(xoap::MessageReference msg) 
throw (xoap::exception::Exception) {     
    typedef xdata::Vector<DiagIdBag> DiagIds;
    DiagIds diagIds;
    xdata::Binary counterLimit;
    xdata::String triggerType;  
    xdata::Integer triggerDelay;    
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("diagCtrlIds", &diagIds));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("counterLimit", &counterLimit));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("triggerType", &triggerType));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("triggerDelay", &triggerDelay));
    xdaqSoapAccess.parseSOAPRequest(msg, CONFIGURE_DIAG_CTRL_NAME, RPCT_DIAG_ACCESS_PREFIX, RPCT_DIAG_ACCESS_NS, paramMap);     
      
    for (DiagIds::iterator iDiagId = diagIds.begin(); iDiagId != diagIds.end(); ++iDiagId) {
        
        DiagCtrlMap::iterator iDiagCtrlEntry = diagCtrlMap.find(iDiagId->bag);
        if (iDiagCtrlEntry == diagCtrlMap.end()) {
            XCEPT_RAISE(xoap::exception::Exception, "Could not find diagCtrl with id = " + iDiagId->bag.toString());
        }
        takeHardware();
        try {          
            iDiagCtrlEntry->second->Configure((uint64_t) counterLimit,
            	stringToTriggerType((string) triggerType), (int) triggerDelay);
            giveHardware();
        }
        catch (TException& e) {
            giveHardware();
            LOG4CPLUS_ERROR(logger, e.what());
            XCEPT_RAISE(xoap::exception::Exception, e.what());
        }    
    }     
    xoap::MessageReference reply = xoap::createMessage();
    return reply;                                                             
}   




xoap::MessageReference XdaqDiagAccess::startDiagCtrl(xoap::MessageReference msg) 
throw (xoap::exception::Exception) {     
    
    typedef xdata::Vector<DiagIdBag> DiagIds;
    DiagIds diagIds;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("diagCtrlIds", &diagIds));
    xdaqSoapAccess.parseSOAPRequest(msg, START_DIAG_CTRL_NAME, RPCT_DIAG_ACCESS_PREFIX, RPCT_DIAG_ACCESS_NS, paramMap);     
      
    for (DiagIds::iterator iDiagId = diagIds.begin(); iDiagId != diagIds.end(); ++iDiagId) {
        DiagCtrlMap::iterator iDiagCtrlEntry = diagCtrlMap.find(iDiagId->bag);
        if (iDiagCtrlEntry == diagCtrlMap.end()) {
            XCEPT_RAISE(xoap::exception::Exception, "Could not find diagCtrl with id = " + iDiagId->bag.toString());
        }
        takeHardware();
        try {          
            iDiagCtrlEntry->second->Start();
            giveHardware();
        }
        catch (TException& e) {
            giveHardware();
            LOG4CPLUS_ERROR(logger, e.what());
            XCEPT_RAISE(xoap::exception::Exception, e.what());
        }   
    }          
    xoap::MessageReference reply = xoap::createMessage();
    return reply;                                                         
}   


xoap::MessageReference XdaqDiagAccess::stopDiagCtrl(xoap::MessageReference msg) 
throw (xoap::exception::Exception) {     
    
    typedef xdata::Vector<DiagIdBag> DiagIds;
    DiagIds diagIds;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("diagCtrlIds", &diagIds));
    xdaqSoapAccess.parseSOAPRequest(msg, STOP_DIAG_CTRL_NAME, RPCT_DIAG_ACCESS_PREFIX, RPCT_DIAG_ACCESS_NS, paramMap);     
      
    for (DiagIds::iterator iDiagId = diagIds.begin(); iDiagId != diagIds.end(); ++iDiagId) {
        DiagCtrlMap::iterator iDiagCtrlEntry = diagCtrlMap.find(iDiagId->bag);
        if (iDiagCtrlEntry == diagCtrlMap.end()) {
            XCEPT_RAISE(xoap::exception::Exception, "Could not find diagCtrl with id = " + iDiagId->bag.toString());
        }
        takeHardware();
        try {          
            iDiagCtrlEntry->second->Stop();
            giveHardware();
        }
        catch (TException& e) {
            giveHardware();
            LOG4CPLUS_ERROR(logger, e.what());
            XCEPT_RAISE(xoap::exception::Exception, e.what());
        }        
    } 
    xoap::MessageReference reply = xoap::createMessage();
    return reply;                                                             
}   



xoap::MessageReference XdaqDiagAccess::getDiagCtrlState(xoap::MessageReference msg) 
throw (xoap::exception::Exception) {     
    
    DiagIdBag idBag;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("diagCtrlId", &idBag));
    xdaqSoapAccess.parseSOAPRequest(msg, GET_DIAG_CTRL_STATE_NAME, RPCT_DIAG_ACCESS_PREFIX, 
                RPCT_DIAG_ACCESS_NS, paramMap);     
      
    try {          
        //DiagId id((int)idBag.bag.getFirst(), (string)idBag.bag.getSecond());
        DiagCtrlMap::iterator iDiagCtrl = diagCtrlMap.find(idBag.bag);
        if (iDiagCtrl == diagCtrlMap.end()) {
            XCEPT_RAISE(xoap::exception::Exception, "Could not find diagCtrl with id = " + idBag.bag.toString());
        }
        xdata::String result;
        if (hardwareMutex_ == 0) {
            result = diagCtrlStateToString(iDiagCtrl->second->GetState());
        }
        else {
            hardwareMutex_->take();
            try {
                result = diagCtrlStateToString(iDiagCtrl->second->GetState());
                hardwareMutex_->give();
            }
            catch (...) {
                hardwareMutex_->give();
                throw;
            }            
        }
        return xdaqSoapAccess.sendSOAPResponse(GET_DIAG_CTRL_STATE_NAME, RPCT_DIAG_ACCESS_PREFIX, 
                    RPCT_DIAG_ACCESS_NS, result); 
    }
    catch (TException& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        XCEPT_RAISE(xoap::exception::Exception, e.what());
    }                                                                  
}   

xoap::MessageReference XdaqDiagAccess::checkCountingEnded(xoap::MessageReference msg) 
throw (xoap::exception::Exception) {     
    
    typedef xdata::Vector<DiagIdBag> DiagIds;
    DiagIds diagIds;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("diagCtrlIds", &diagIds));
    xdaqSoapAccess.parseSOAPRequest(msg, CHECK_COUNTING_ENDED_NAME, RPCT_DIAG_ACCESS_PREFIX, 
                RPCT_DIAG_ACCESS_NS, paramMap);     
                
    xdata::Boolean result = true;
    for (DiagIds::iterator iDiagId = diagIds.begin(); iDiagId != diagIds.end(); ++iDiagId) {
        DiagCtrlMap::iterator iDiagCtrlEntry = diagCtrlMap.find(iDiagId->bag);
        if (iDiagCtrlEntry == diagCtrlMap.end()) {
            XCEPT_RAISE(xoap::exception::Exception, "Could not find diagCtrl with id = " + iDiagId->bag.toString());
        }
        takeHardware();
        try {          
            bool ended = iDiagCtrlEntry->second->CheckCountingEnded();
            giveHardware();
            if (ended == false) {
                result = false;
                break;
            };
        }
        catch (TException& e) {
            giveHardware();
            LOG4CPLUS_ERROR(logger, e.what());
            XCEPT_RAISE(xoap::exception::Exception, e.what());
        }   
    }   
    return xdaqSoapAccess.sendSOAPResponse(CHECK_COUNTING_ENDED_NAME, RPCT_DIAG_ACCESS_PREFIX, 
                RPCT_DIAG_ACCESS_NS, result);                                                             
}   

xoap::MessageReference XdaqDiagAccess::getHistoMgrList(xoap::MessageReference msg) 
throw (xoap::exception::Exception) {     
    
    xdata::Vector<HistoMgrInfoBag> histoMgrInfos;    
    
    for (DiagMap::iterator iEntry = diagMap.begin(); iEntry != diagMap.end(); ++iEntry) {
        IHistoMgr* histoMgr = dynamic_cast<IHistoMgr*>(iEntry->second);
        if (histoMgr != 0) {
            HistoMgrInfoBag histoMgrInfo;
            histoMgrInfo.bag.init(iEntry->first, histoMgr->GetBinCount(), 
                DiagId(histoMgr->GetDiagCtrl().GetOwner().getId(), histoMgr->GetDiagCtrl().GetName()));
            histoMgrInfos.push_back(histoMgrInfo);
        }
    }
    
    try {          
        return xdaqSoapAccess.sendSOAPResponse(GET_HISTO_MGR_LIST_NAME, RPCT_DIAG_ACCESS_PREFIX, 
            RPCT_DIAG_ACCESS_NS, histoMgrInfos);   
    }
    catch (TException& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        XCEPT_RAISE(xoap::exception::Exception, e.what());
    }                                                                      
}   

xoap::MessageReference XdaqDiagAccess::resetHistoMgr(xoap::MessageReference msg) 
throw (xoap::exception::Exception) {     
    
    typedef xdata::Vector<DiagIdBag> DiagIds;
    DiagIds diagIds;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("histoMgrIds", &diagIds));
    xdaqSoapAccess.parseSOAPRequest(msg, RESET_HISTO_MGR_NAME, RPCT_DIAG_ACCESS_PREFIX, RPCT_DIAG_ACCESS_NS, paramMap);     
              
    for (DiagIds::iterator iDiagId = diagIds.begin(); iDiagId != diagIds.end(); ++iDiagId) {
        DiagMap::iterator iEntry = diagMap.find(iDiagId->bag);
        if (iEntry == diagMap.end()) {
            XCEPT_RAISE(xoap::exception::Exception, "Could not find histoMgr with id = " + iDiagId->bag.toString());
        }
        
        IHistoMgr* histoMgr = dynamic_cast<IHistoMgr*>(iEntry->second);
        if (histoMgr == 0) {
            XCEPT_RAISE(xoap::exception::Exception, "invalid histoMgr id " + iDiagId->bag.toString() + ": not a histoMgr");
        }
            
        takeHardware();
        try {  
            histoMgr->Reset();
            giveHardware();
        }
        catch (TException& e) {
            giveHardware();
            LOG4CPLUS_ERROR(logger, e.what());
            XCEPT_RAISE(xoap::exception::Exception, e.what());
        }  
    }    
    xoap::MessageReference reply = xoap::createMessage();
    return reply;                                                                
}   
  
  
xoap::MessageReference XdaqDiagAccess::readDataHistoMgr(xoap::MessageReference msg) 
throw (xoap::exception::Exception) {     
    
    DiagIdBag idBag;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("histoMgrId", &idBag));
    xdaqSoapAccess.parseSOAPRequest(msg, READ_DATA_HISTO_MGR_NAME, RPCT_DIAG_ACCESS_PREFIX, RPCT_DIAG_ACCESS_NS, paramMap);     
              
    DiagMap::iterator iEntry = diagMap.find(idBag.bag);
    if (iEntry == diagMap.end()) {
        XCEPT_RAISE(xoap::exception::Exception, "Could not find histoMgr with id = " + idBag.bag.toString());
    }
    
    IHistoMgr* histoMgr = dynamic_cast<IHistoMgr*>(iEntry->second);
    if (histoMgr == 0) {
        XCEPT_RAISE(xoap::exception::Exception, "invalid histoMgr id " + idBag.bag.toString() + ": not a histoMgr");
    }

    xdata::Vector<xdata::Binary> xData;
    takeHardware();
    try {      	
        histoMgr->Refresh();
        uint32_t* data = histoMgr->GetLastData();
        int binCount = histoMgr->GetBinCount();
        for (int i = 0; i < binCount; i++) {
            xData.push_back(xdata::Binary(data[i]));
        }
        giveHardware();
    }
    catch (TException& e) {
        giveHardware();
        LOG4CPLUS_ERROR(logger, e.what());
        XCEPT_RAISE(xoap::exception::Exception, e.what());
    }   
    return xdaqSoapAccess.sendSOAPResponse(READ_DATA_HISTO_MGR_NAME, RPCT_DIAG_ACCESS_PREFIX, 
        RPCT_DIAG_ACCESS_NS, xData);                                                                
} 


xoap::MessageReference XdaqDiagAccess::getPulserList(xoap::MessageReference msg) 
throw (xoap::exception::Exception) {     
    
    xdata::Vector<PulserInfoBag> pulserInfos;    
    
    for (DiagMap::iterator iEntry = diagMap.begin(); iEntry != diagMap.end(); ++iEntry) {
        IPulser* pulser = dynamic_cast<IPulser*>(iEntry->second);
        if (pulser != 0) {
            PulserInfoBag pulserInfo;
            pulserInfo.bag.init(iEntry->first, pulser->GetMaxLength(), pulser->GetWidth(), 
                DiagId(pulser->GetDiagCtrl().GetOwner().getId(), pulser->GetDiagCtrl().GetName()));
            pulserInfos.push_back(pulserInfo);
        }
    }
    
    try {          
        return xdaqSoapAccess.sendSOAPResponse(GET_PULSER_LIST_NAME, RPCT_DIAG_ACCESS_PREFIX, 
            RPCT_DIAG_ACCESS_NS, pulserInfos);   
    }
    catch (TException& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        XCEPT_RAISE(xoap::exception::Exception, e.what());
    }                                                                      
}     


xoap::MessageReference XdaqDiagAccess::configurePulser(xoap::MessageReference msg) 
throw (xoap::exception::Exception) {     
    
    typedef xdata::Vector<DiagIdBag> DiagIds;
    DiagIds diagIds;
    xdata::Vector<xdata::Binary> data;
    xdata::Binary pulseLength;
    xdata::Boolean repeat; 
    xdata::Integer target; 
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("pulserIds", &diagIds));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("data", &data));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("pulseLength", &pulseLength));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("repeat", &repeat));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("target", &target));
    xdaqSoapAccess.parseSOAPRequest(msg, CONFIGURE_PULSER_NAME, RPCT_DIAG_ACCESS_PREFIX, RPCT_DIAG_ACCESS_NS, paramMap);     
      
      
    cout << "Pulser data:" << endl;
    std::vector<BigInteger> data1;          
    size_t cnt = data.size();
    for (size_t i = 0; i < cnt; i++) {
        xdata::Binary& val = data[i];
        cout << val.toString() << endl;
        BigInteger val1(val.buffer(), val.size());
        data1.push_back(val1);
    }       
        
    for (DiagIds::iterator iDiagId = diagIds.begin(); iDiagId != diagIds.end(); ++iDiagId) {
        DiagMap::iterator iEntry = diagMap.find(iDiagId->bag);
        if (iEntry == diagMap.end()) {
            XCEPT_RAISE(xoap::exception::Exception, "Could not find diag with id = " + iDiagId->bag.toString());
        }
        
        IPulser* pulser = dynamic_cast<IPulser*>(iEntry->second);
        if (pulser == 0) {
            XCEPT_RAISE(xoap::exception::Exception, "invalid pulser id " + iDiagId->bag.toString() + ": not a pulser");
        }
            
        
        takeHardware();
        try {              
            pulser->Configure(data1, (uint64_t) pulseLength, (bool)repeat, (int)target);
            giveHardware();
        }
        catch (TException& e) {
            giveHardware();
            LOG4CPLUS_ERROR(logger, e.what());
            XCEPT_RAISE(xoap::exception::Exception, e.what());
        }   
    }      
    
    xoap::MessageReference reply = xoap::createMessage();
    return reply;                                                    
}   

xoap::MessageReference XdaqDiagAccess::configurePulserMuons(xoap::MessageReference msg) 
throw (xoap::exception::Exception) {     
    
    typedef xdata::Vector<DiagIdBag> DiagIds;
    DiagIds diagIds;
    xdata::String muonsXml;
    xdata::Binary pulseLength;
    xdata::Boolean repeat; 
    xdata::Integer target; 
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("pulserIds", &diagIds));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("muonsXml", &muonsXml));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("pulseLength", &pulseLength));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("repeat", &repeat));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("target", &target));
    xdaqSoapAccess.parseSOAPRequest(msg, CONFIGURE_PULSER_MUONS_NAME, RPCT_DIAG_ACCESS_PREFIX, RPCT_DIAG_ACCESS_NS, paramMap);     
      
    string xml = (string)muonsXml;
    //cout << "Pulser data:" << xml << endl;
    //cout << "Parsing xml" << endl;
        
    XmlBxDataReader reader;
    RPCDataStreamPtr dataStream;
    string filePrefix = "file://";
    try { 
    	if (xml.find_first_of(filePrefix) == 0) {
    		dataStream = reader.read(xml.substr(filePrefix.size()));
    	}
    	else {
    		dataStream = reader.readString(xml.c_str());
    	}
    } 
    catch (TException& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        XCEPT_RAISE(xoap::exception::Exception, e.what());
    }   
    
    
    for (DiagIds::iterator iDiagId = diagIds.begin(); iDiagId != diagIds.end(); ++iDiagId) {
        DiagMap::iterator iEntry = diagMap.find(iDiagId->bag);
        if (iEntry == diagMap.end()) {
            XCEPT_RAISE(xoap::exception::Exception, "Could not find diag with id = " + iDiagId->bag.toString());
        }
        
        IPulser* pulser = dynamic_cast<IPulser*>(iEntry->second);
        if (pulser == 0) {
            XCEPT_RAISE(xoap::exception::Exception, "invalid pulser id " + iDiagId->bag.toString() + ": not a pulser");
        }
         
        takeHardware();
        try {                        
            std::vector<rpct::BigInteger> pulseData = dataStream->getPuslsesFor(&(pulser->GetOwner()), (int) target, (uint64_t) pulseLength);
            pulser->Configure(pulseData, (uint64_t) pulseLength, (bool) repeat, (int) target);
            giveHardware();
            LOG4CPLUS_INFO(logger, "Pulser configured on " << pulser->GetOwner().getDescription() );
        }
        catch (TException& e) {
            giveHardware();
            LOG4CPLUS_ERROR(logger, e.what());
            XCEPT_RAISE(xoap::exception::Exception, e.what());
        }   
    }      
    
    xoap::MessageReference reply = xoap::createMessage();
    return reply;                                                    
}   


xoap::MessageReference XdaqDiagAccess::getDiagnosticReadoutList(xoap::MessageReference msg) 
throw (xoap::exception::Exception) {     
    
    xdata::Vector<DiagnosticReadoutInfoBag> drInfos;    
    
    for (DiagMap::iterator iEntry = diagMap.begin(); iEntry != diagMap.end(); ++iEntry) {
        IDiagnosticReadout* dr = dynamic_cast<IDiagnosticReadout*>(iEntry->second);
        if (dr != 0) {
            DiagnosticReadoutInfoBag drInfo;
            drInfo.bag.init(iEntry->first, dr->GetAreaLen(), dr->GetAreaWidth(), 
                dr->GetMaskNum(), dr->GetMaskWidth(), dr->GetDataWidth(),
                dr->GetTrgNum(), dr->GetTimerWidth(),
                DiagId(dr->GetDiagCtrl().GetOwner().getId(), dr->GetDiagCtrl().GetName()));
            drInfos.push_back(drInfo);
        }
    }
    
    try {          
        return xdaqSoapAccess.sendSOAPResponse(GET_DIAGNOSTIC_READOUT_LIST_NAME, 
            RPCT_DIAG_ACCESS_PREFIX, RPCT_DIAG_ACCESS_NS, drInfos);   
    }
    catch (TException& e) {
        LOG4CPLUS_ERROR(logger, e.what());
        XCEPT_RAISE(xoap::exception::Exception, e.what());
    }                                                                      
}     


xoap::MessageReference XdaqDiagAccess::configureDiagnosticReadout(xoap::MessageReference msg) 
throw (xoap::exception::Exception) {     
    
    typedef xdata::Vector<DiagIdBag> DiagIds;
    DiagIds diagIds;
    XMasks xMasks;
    xdata::Integer daqDelay;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("diagReadoutIds", &diagIds));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("masks", &xMasks));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("daqDelay", &daqDelay));
    xdaqSoapAccess.parseSOAPRequest(msg, CONFIGURE_DIAGNOSTIC_READOUT_NAME, RPCT_DIAG_ACCESS_PREFIX, RPCT_DIAG_ACCESS_NS, paramMap);     
      
    IDiagnosticReadout::TMasks masks;
    for (XMasks::iterator iMask = xMasks.begin(); iMask != xMasks.end(); ++iMask) {
        IDiagnosticReadout::TMask mask;
        for (XMask::iterator iter = iMask->begin(); iter != iMask->end(); ++iter) {
            mask.insert((int)(*iter));          
        }   
        masks.push_back(mask);
    }   

    /*cout << "**** configureDiagnosticReadout masks size" << masks.size() << endl; 
    for (size_t m = 0; m < masks.size(); m++) {
        IDiagnosticReadout::TMask& mask = masks[m];
        cout << "mask" << m << ": " << flush; 
        for (IDiagnosticReadout::TMask::iterator iter = mask.begin(); iter != mask.end(); ++iter) {
            cout << *iter; 
        }
        cout << endl;
    }*/
        
    for (DiagIds::iterator iDiagId = diagIds.begin(); iDiagId != diagIds.end(); ++iDiagId) {
        DiagMap::iterator iEntry = diagMap.find(iDiagId->bag);
        if (iEntry == diagMap.end()) {
            XCEPT_RAISE(xoap::exception::Exception, "Could not find diag with id = " + iDiagId->bag.toString());
        }
        
        IDiagnosticReadout* readout = dynamic_cast<IDiagnosticReadout*>(iEntry->second);
        if (readout == 0) {
            XCEPT_RAISE(xoap::exception::Exception, "invalid diagnostic readout id " + iDiagId->bag.toString() + ": not a diagnostic readout");
        }
            
        takeHardware();
        try {              
            readout->Configure(masks, (int)daqDelay);
            giveHardware();
        }
        catch (TException& e) {
            giveHardware();
            LOG4CPLUS_ERROR(logger, e.what());
            XCEPT_RAISE(xoap::exception::Exception, e.what());
        }   
    }      
    
    xoap::MessageReference reply = xoap::createMessage();
    return reply;                                                    
}   

xoap::MessageReference XdaqDiagAccess::configureDiagnosticReadoutLB(xoap::MessageReference msg) 
throw (xoap::exception::Exception) {     
    
    typedef xdata::Vector<DiagIdBag> DiagIds;
    DiagIds diagIds;
    XMasks xMasks;
    xdata::Integer daqDelay;
    xdata::Integer dataSel;
    xdata::Integer extSel;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("diagReadoutIds", &diagIds));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("masks", &xMasks));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("daqDelay", &daqDelay));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("dataSel", &dataSel));
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("extSel", &extSel));
    xdaqSoapAccess.parseSOAPRequest(msg, CONFIGURE_DIAGNOSTIC_READOUT_LB_NAME, RPCT_DIAG_ACCESS_PREFIX, RPCT_DIAG_ACCESS_NS, paramMap);     
      
    IDiagnosticReadout::TMasks masks;
    for (XMasks::iterator iMask = xMasks.begin(); iMask != xMasks.end(); ++iMask) {
        IDiagnosticReadout::TMask mask;
        for (XMask::iterator iter = iMask->begin(); iter != iMask->end(); ++iter) {
            mask.insert((int)(*iter));
        }       
    }      
        
    for (DiagIds::iterator iDiagId = diagIds.begin(); iDiagId != diagIds.end(); ++iDiagId) {
        DiagMap::iterator iEntry = diagMap.find(iDiagId->bag);
        if (iEntry == diagMap.end()) {
            XCEPT_RAISE(xoap::exception::Exception, "Could not find diag with id = " + iDiagId->bag.toString());
        }
        
        ILBDiagnosticReadout* readout = dynamic_cast<ILBDiagnosticReadout*>(iEntry->second);
        if (readout == 0) {
            XCEPT_RAISE(xoap::exception::Exception, "invalid diagnostic readout id " + iDiagId->bag.toString() + ": not a LinkBoard diagnostic readout");
        }
            
        takeHardware();
        try {              
        	ILBDiagnosticReadout::DataSel dataSelE = ILBDiagnosticReadout::dsSynchWinData;
        	if (dataSel == ILBDiagnosticReadout::dsSynchWinData)
        		dataSelE = ILBDiagnosticReadout::dsSynchWinData;
        	else if (dataSel == ILBDiagnosticReadout::dsDiagSynchData)
        		dataSelE = ILBDiagnosticReadout::dsDiagSynchData;        	
        	
        	ILBDiagnosticReadout::ExtDataSel extSelE = ILBDiagnosticReadout::edsNone;
        	if(extSel == ILBDiagnosticReadout::edsNone)
        		extSelE = ILBDiagnosticReadout::edsNone;
        	else if(extSel == ILBDiagnosticReadout::edsCSC)
        		extSelE = ILBDiagnosticReadout::edsCSC;
        	else if(extSel == ILBDiagnosticReadout::edsLMUX)
        		extSelE = ILBDiagnosticReadout::edsLMUX;
        	else if(extSel == ILBDiagnosticReadout::edsCoder)
        	    extSelE = ILBDiagnosticReadout::edsCoder;
        	else if(extSel == ILBDiagnosticReadout::edsSlave0)
        	    extSelE = ILBDiagnosticReadout::edsSlave0;
        	else if(extSel == ILBDiagnosticReadout::edsSlave1)
        	    extSelE = ILBDiagnosticReadout::edsSlave1;
        	
            readout->Configure(masks, (int)daqDelay, dataSelE, extSelE);
            giveHardware();
        }
        catch (TException& e) {
            giveHardware();
            LOG4CPLUS_ERROR(logger, e.what());
            XCEPT_RAISE(xoap::exception::Exception, e.what());
        }   
    }      
    
    xoap::MessageReference reply = xoap::createMessage();
    return reply;                                                    
}   



  
xoap::MessageReference XdaqDiagAccess::readDataDiagnosticReadout(xoap::MessageReference msg) 
throw (xoap::exception::Exception) {     
    cout << "readDataDiagnosticReadout" << endl;
            
    typedef xdata::Vector<DiagIdBag> DiagIds;
    DiagIds diagIds;
    XdaqSoapAccess::TSOAPParamMap paramMap;
    paramMap.insert(XdaqSoapAccess::TSOAPParamMap::value_type("diagReadoutIds", &diagIds));
    xdaqSoapAccess.parseSOAPRequest(msg, READ_DATA_DIAGNOSTIC_READOUT_NAME, RPCT_DIAG_ACCESS_PREFIX, RPCT_DIAG_ACCESS_NS, paramMap);

    
    xdata::Vector<DiagnosticReadoutDataBag> result;
    
    vector<TStandardDiagnosticReadout*> diagReadoutsVec;

    takeHardware();
    try {    
        for (DiagIds::iterator iDiagId = diagIds.begin(); iDiagId != diagIds.end(); ++iDiagId) {                 
            DiagMap::iterator iEntry = diagMap.find(iDiagId->bag);
            if (iEntry == diagMap.end()) {
                XCEPT_RAISE(xoap::exception::Exception, "Could not find diag with id = " + iDiagId->bag.toString());
            }

            TStandardDiagnosticReadout* readout = dynamic_cast<TStandardDiagnosticReadout*>(iEntry->second);
            if (readout == 0) {
                XCEPT_RAISE(xoap::exception::Exception, "invalid diagnostic readout id " + iDiagId->bag.toString() + ": not a TStandardDiagnosticReadout");
            }

            result.push_back(DiagnosticReadoutDataBag());
            DiagnosticReadoutDataXData& xData = result.back().bag;

            // Fill result with data until we find a readout which is still running
            /*if (!readout->GetDiagCtrl().CheckCountingEnded()) {
            xData.init(iDiagId->bag, true, false);
            break;    
        }*/

            if (readout->GetDiagCtrl().GetState() == IDiagCtrl::sRunning) {
                //readout->GetDiagCtrl().Stop();
                xData.init(iDiagId->bag, true, false);
                break;                 
            }

            if (readout->GetLastData().isEmpty()) {
                xData.init(iDiagId->bag, false, false);
            }
            else { 
                IDiagnosticReadout::Events& e = readout->GetLastEvents();
                xData.init(iDiagId->bag, false,  e.getLost());

                typedef IDiagnosticReadout::Events::EventList EventList;
                EventList& events = e.getEventList();
                for (EventList::iterator iEvent = events.begin(); iEvent != events.end(); ++iEvent) {
                    xData.addEvent(**iEvent);
                }
                diagReadoutsVec.push_back(readout);
            }
        }
        


        string str;
        if(diagReadoutsVec.size() > 0) {
            str = (dynamic_cast<IDevice&>(diagReadoutsVec[0]->GetOwner())).getBoard().getCrate()->getDescription();
        }

        string fileName = string("/data/")  + "lastReadoutData_" + str + ".txt";
        char* tmp = getenv("RPCT_DATA_PATH");
        if (tmp != 0) {
        	fileName = string(tmp) + "/"  + "lastReadoutData_" + str + ".txt";
        }
        else {
        	XCEPT_RAISE(xoap::exception::Exception, (string)(__FUNCTION__) + string(": Environment variable RPCT_DATA_PATH not defined"));
        }

        ofstream out(fileName.c_str());          
        DiagReadWriter diagReadWriter(&out, diagReadoutsVec);           

        diagReadWriter.enableEVNCheck = false;
        diagReadWriter.writeEvents();  
        out.close();
        giveHardware();
        cout << "diag readouts data were written to " << fileName << endl;
    }
    catch (...) {
        giveHardware();
        throw;
    }

               
    return xdaqSoapAccess.sendSOAPResponse(READ_DATA_DIAGNOSTIC_READOUT_NAME, RPCT_DIAG_ACCESS_PREFIX, 
            RPCT_DIAG_ACCESS_NS, result);       
                                                
} 

}}
