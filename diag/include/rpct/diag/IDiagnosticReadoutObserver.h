#ifndef IDIAGNOSTCREADOUTOBSERVER_H_
#define IDIAGNOSTCREADOUTOBSERVER_H_


namespace rpct {
    
class IDiagnosticReadout;

//one of implementation of IHistoObserver is THistoObserverImpl in GraphHistFrm.h inside class THistGraphFrm
class IDiagnosticReadoutObserver {
public:
  virtual void ReadoutChanged(IDiagnosticReadout* readout) = 0;
  virtual ~IDiagnosticReadoutObserver() {}
};

}

#endif /*ISELECTIVEREADOUTOBSERVER_H_*/
