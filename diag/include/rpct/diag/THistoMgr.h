#ifndef THistoMgrH
#define THistoMgrH


//
//  Michal Pietrusinski, Karol Bunkowski 2001
//  Warsaw University
//

#include <vector>
#include "rpct/diag/IHistoCtrl.h"
#include "rpct/diag/IHistoMgr.h"
#include "rpct/std/ENotImplemented.h"



namespace rpct {

class EObserverNotRegistered : public TException {
public:
    EObserverNotRegistered(const std::string& msg)
	: TException( "Observer not registered: " + msg ) {}
    //virtual ~EI2C() {}
};
//------------------------
class THistoMgrBase: public IHistoMgr {
protected:
  typedef std::vector<IHistoObserver*> TObserversVector;
  TObserversVector Observers;
  virtual void CallObservers();
public:
  THistoMgrBase() {}
  virtual ~THistoMgrBase() {}
  virtual void Refresh()
    { throw ENotImplemented("THistoMgrBase::Refresh"); }

  virtual void Reset() 
    { throw ENotImplemented("THistoMgrBase::Reset"); }
  virtual void RegisterObserver(IHistoObserver* observer);
  virtual void UnregisterObserver(IHistoObserver* observer);
};

//------------------------
class THistoMgr: public THistoMgrBase {
private:
  int BinCount;
  std::string HistoId;
  IHistoCtrl* HistoCtr;
  uint32_t* Data;
  uint32_t* PrevData;
  bool SkipSame;
public:         
  THistoMgr(IHistoCtrl* histoCtr, bool skip_same = false);
  virtual ~THistoMgr() {}

  virtual IHistoCtrl* GetHistoCtrl()
    {
      return HistoCtr;
    }
  virtual std::string GetID()
    {
      return HistoId;
    }
  virtual int GetBinCount()
    {
      return BinCount;
    }

  virtual void Refresh()
    {
      HistoCtr->ReadData(Data);
      if (SkipSame) {
        if (memcmp(Data, PrevData, BinCount * sizeof (uint32_t)) != 0) {
          CallObservers();
          memcpy(PrevData, Data, BinCount * sizeof (uint32_t));
        };
      }
      else {
        CallObservers();
      }
    }

  virtual void Reset()
    {
      HistoCtr->Reset();
      //memset(Data, 0, BinCount* sizeof (uint32_t)); commented out to allow analysis of the data after reseting the counters in the hardware

      CallObservers();
    }
    
  virtual uint32_t* GetLastData()
    {
      return Data;
    }
};


// allows grouping and masking bits in bin numbers
class THistoMgrMask: public THistoMgrBase {
public:
  enum TItemMask { imNone, imSelect0, imSelect1, imMask };
private:
  int BinCount;
  //int CounterSize;
  std::string HistoId;
  IHistoCtrl* HistoCtr;
  uint32_t* Data;
  uint32_t* MaskedData;
  int BitsNum;
  TItemMask* Masks;

  int MaskNone;
  int MaskSelect0;
  int MaskSelect1;
  //int MaskMask;

  void PrepareMaskedData();

public:
  THistoMgrMask(IHistoCtrl* histoCtr);
  virtual ~THistoMgrMask() {}

  void SetMask(int bit_no, TItemMask mask);

  virtual IHistoCtrl* GetHistoCtrl()
    {
      return HistoCtr;
    }
  virtual std::string GetID()
    {
      return HistoId;
    }
  virtual int GetBinCount()
    {
      return BinCount;
    }

  virtual void Refresh()
    {
      HistoCtr->ReadData(Data);

      PrepareMaskedData();
      CallObservers();
    }

  virtual void Reset()
    {
      HistoCtr->Reset();
      memset(Data, 0, BinCount* sizeof (uint32_t));
      memset(MaskedData, 0, BinCount* sizeof (uint32_t));

      CallObservers();
    }

  virtual uint32_t* GetLastData()
    {
      return MaskedData;
    }
};





class THistoMgrRate: public THistoMgrBase, public IHistoObserver {
private:
  int BinCount;
  std::string ID;
  IHistoMgr* SourceHisto;
  uint32_t* LastSourceData;
  uint32_t* Data;
  bool Enabled;
  bool Connected;
public:
  THistoMgrRate(IHistoMgr* sourceHisto)
   : SourceHisto(sourceHisto), LastSourceData(NULL), Data(NULL), Enabled(true),
   Connected(true)
    {
      if (SourceHisto == NULL)
        throw TException("THistoMgrRate: sourceHisto == NULL");

      SourceHisto->RegisterObserver(this);

      ID = std::string("Rate: ") + SourceHisto->GetID();

      BinCount = SourceHisto->GetBinCount();
      LastSourceData = new uint32_t[BinCount];
      Data = new uint32_t[BinCount];

      memset(LastSourceData, 0, BinCount * sizeof(uint32_t));
      memset(Data, 0, BinCount * sizeof(uint32_t));
    }


  virtual ~THistoMgrRate()
    {
      if (Connected)
        Disconnect();
        
      if (LastSourceData != NULL)
        delete LastSourceData;
      if (Data != NULL)
        delete Data;
    }
    
  virtual void Disconnect()
    {
      if (SourceHisto)
        SourceHisto->UnregisterObserver(this);
      Connected = false;
    }

  virtual std::string GetID()
    {
      return ID;
    }
  virtual int GetBinCount()
    {
      return BinCount;
    }

  virtual void SetEnabled(bool value)
    {
      Enabled = value;
    }

  virtual void HistoChanged(IHistoMgr* histo)
    {
      if (Enabled) {
        uint32_t* newdata = histo->GetLastData();

        for(int i=0; i<BinCount; i++)
          Data[i] = newdata[i] - LastSourceData[i];

        memcpy(LastSourceData, newdata, BinCount * sizeof(uint32_t));

        CallObservers();
      };
    }

  virtual uint32_t* GetLastData()
    {
      return Data;
    }

};


// converts BinNo -> Number of bits != 0 in Binno
class THistoMgrBitNo: public THistoMgrBase, public IHistoObserver {
private:
  int BinCount;
  int SourceBinCount;
  std::string ID;
  IHistoMgr* SourceHisto;
  uint32_t* Data;
  bool Enabled;
public:
  THistoMgrBitNo(IHistoMgr* sourceHisto)
   : SourceHisto(sourceHisto), Data(NULL), Enabled(true)
    {
      if (SourceHisto == NULL)
        throw TException("THistoMgrRate: sourceHisto == NULL");

      SourceHisto->RegisterObserver(this);

      ID = std::string("Number of bits: ") + SourceHisto->GetID();

      SourceBinCount = sourceHisto->GetBinCount();

      BinCount = 0;

      for (int i=31; i>=0; i--)
        if ((SourceBinCount >> i) & 1)  {
          BinCount = i + 1;
          break;
        };                        

      Data = new uint32_t[BinCount];

      memset(Data, 0, BinCount * sizeof(uint32_t));
    }


  virtual ~THistoMgrBitNo()
    {
      if (Data != NULL)
        delete Data;
    }

  virtual std::string GetID()
    {
      return ID;
    }
  virtual int GetBinCount()
    {
      return BinCount;
    }

  virtual void SetEnabled(bool value)
    {
      Enabled = value;
    }

  virtual void HistoChanged(IHistoMgr* histo)
    {
      if (Enabled) {
        uint32_t* newdata = histo->GetLastData();

        memset(Data, 0, BinCount * sizeof(uint32_t));

        unsigned bits;

        for(int i=0; i<SourceBinCount; i++) {
          bits = 0;
          for(int j=0; j<BinCount; j++)
            if ((1 << j) & i)
              bits++;                   

          Data[bits] += newdata[i];
        };

        CallObservers();
      };
    }

  virtual uint32_t* GetLastData()
    {
      return Data;
    }
};


class THistoMgrOper: public THistoMgrBase, public IHistoObserver {
public:
  enum TOper { opAdd, opSub, opMult, opDiv };
private:
  int BinCount;
  std::string ID;  
  IHistoMgr* SourceHisto1;
  IHistoMgr* SourceHisto2;
  bool ObserveHisto1;
  bool ObserveHisto2;
  uint32_t* Data;
  bool Enabled;
  TOper Oper;
  bool Percent;
  bool Connected;
public:
  THistoMgrOper(IHistoMgr* sourceHisto1, IHistoMgr* sourceHisto2,
                bool observeHisto1 = false, bool observeHisto2 = true,
                TOper oper = opAdd, bool percent = false)
   : SourceHisto1(sourceHisto1), SourceHisto2(sourceHisto2),
     ObserveHisto1(observeHisto1), ObserveHisto2(observeHisto2),
     Data(NULL), Enabled(true), Oper(oper), Percent(percent), Connected(true)
    {
      if (SourceHisto1 == NULL)
        throw TException("THistoMgrDivide: sourceHisto1 == NULL");
      if (SourceHisto2 == NULL)
        throw TException("THistoMgrDivide: sourceHisto2 == NULL");

      if (SourceHisto1->GetBinCount() != SourceHisto2->GetBinCount())
        throw TException("THistoMgrDivide: different bin counts");

      if (ObserveHisto1)
        SourceHisto1->RegisterObserver(this);

      if (ObserveHisto2)
        SourceHisto2->RegisterObserver(this);

      ID = SourceHisto1->GetID()
           +  ((Oper==opAdd) ? " + "
            : ((Oper==opSub) ? " - "
            : ((Oper==opMult) ? " * " : " / ")))
           + SourceHisto2->GetID()
                + (Percent && (Oper==opDiv) ? " [%]" : "" );

      BinCount = SourceHisto1->GetBinCount();
      Data = new uint32_t[BinCount];
    }


  virtual ~THistoMgrOper()
    {
      if (Connected)
        Disconnect();
        
      if (Data != NULL)
        delete Data;
    }

  virtual void Disconnect()
    {
      if (ObserveHisto1)
        SourceHisto1->UnregisterObserver(this);

      if (ObserveHisto2)
        SourceHisto2->UnregisterObserver(this);

      Connected = false;
    } 

  virtual std::string GetID()
    {
      return ID;
    }
  virtual int GetBinCount()
    {
      return BinCount;
    }

  virtual void SetEnabled(bool value)
    {
      Enabled = value;
    }

  virtual void HistoChanged(IHistoMgr* histo)
    {
      if (Enabled) {
        uint32_t* data1 = SourceHisto1->GetLastData();
        uint32_t* data2 = SourceHisto2->GetLastData();


        if (Oper == opAdd)
          for(int i=0; i<BinCount; i++)
            Data[i] = data1[i] + data2[i];
        else
          if (Oper == opSub)
            for(int i=0; i<BinCount; i++)
              Data[i] = data1[i] - data2[i];
          else
            if (Oper == opMult)
              for(int i=0; i<BinCount; i++)
                Data[i] = data1[i] * data2[i];
            else
              // it must be division
              if (Percent)
                for(int i=0; i<BinCount; i++)
                  Data[i] = (uint32_t)((data2[i] == 0) ? 0 : ((double)data1[i] / (double)data2[i] * 100));
              else
                for(int i=0; i<BinCount; i++)
                  Data[i] = (uint32_t)((data2[i] == 0) ? 0 : ((double)data1[i] / (double)data2[i]));

        CallObservers();
      };
    }

  virtual uint32_t* GetLastData()
    {
      return Data;
    }
};
   
class THistoMgrMemory: public THistoMgrBase {
private:
  std::string HistoId;
  int BinCount;
  uint32_t* Data;
public:
  THistoMgrMemory(std::string id, int bincount)
    : HistoId(id), BinCount(bincount)
    {
      Data = new uint32_t[BinCount];
    }

  virtual ~THistoMgrMemory()
    {
      delete Data;
    }
    
  virtual std::string GetID()
    {
      return HistoId;
    }
  virtual int GetBinCount()
    {
      return BinCount;
    }
    
  virtual uint32_t* GetLastData()
    {
      return Data;
    } 

  virtual void SetData(unsigned char* data)
    {
      for (int i=0; i<BinCount; i++)
        Data[i] = data[i];

      CallObservers();
    }
};

}

#endif



