//---------------------------------------------------------------------------

#ifndef TFlashMgrH
#define TFlashMgrH
//---------------------------------------------------------------------------


//
//  Michal Pietrusinski, Karol Bunkowski 2001
//  Warsaw University
//

#include <vector>
#include "rpct/diag/IHistoCtrl.h"
#include "tb_ii.h"
 //------------------------


namespace rpct {

class IFlashMgr;
 //------------------------

//one of implementation of IHistoObserver is THistoObserverImpl in GraphHistFrm.h inside class THistGraphFrm
class IFlashObserver {
public:
  virtual void FlashChanged(IFlashMgr* flash) = 0;

  virtual ~IFlashObserver() {}
};


class IFlashMgr {
public:
  virtual std::string GetName() = 0;
  virtual int GetWidth() = 0;
  virtual int GetLength() = 0;
  virtual void* GetLastData() = 0;
  virtual void RegisterObserver(IFlashObserver* observer) = 0;
  virtual void UnregisterObserver(IFlashObserver* observer) = 0;

  virtual ~IFlashMgr() {}
};

//------------------------
class TFlashMgr: public IFlashMgr {
protected:
  typedef std::vector<IFlashObserver*> TObserversVector;
  TObserversVector Observers;
  virtual void CallObservers();
  int Width;
  int Length;
  std::string Name;
  unsigned char* Data;
  int DataSize;
  IDevice& Device;
  const IDevice::TID AREA_ID;   
  const IDevice::TID BITS_STATUS_DAQ_FLASH_READY;
  const IDevice::TID WORD_FLASH_LEN;
  bool Ready;
public:
  TFlashMgr(IDevice& device, IDevice::TID area_id, IDevice::TID bitsStatusDaqFlashReady,
            IDevice::TID wordFlashLen)
    : Device(device), AREA_ID(area_id), BITS_STATUS_DAQ_FLASH_READY(bitsStatusDaqFlashReady),
      WORD_FLASH_LEN(wordFlashLen), Ready(true)
    {
      IDevice::TItemDesc desc = Device.getItemDesc(area_id);
      if (desc.Type != IDevice::itArea)
        throw TException("TFlashMgr: given id is not an area");

      Width = desc.Width;
      Length = desc.Number;
      Name = desc.Name;

      DataSize = ((Width-1)/8+1) * Length;   
      Data = new unsigned char[DataSize];
    }
    
  virtual ~TFlashMgr() {}

  virtual void RegisterObserver(IFlashObserver* observer);
  virtual void UnregisterObserver(IFlashObserver* observer);
  
  virtual std::string GetName()
    {
      return Name;
    }

  IDevice& GetDevice()
    {
      return Device;
    }


  virtual int GetWidth()
    {
      return Width;
    }  

  virtual int GetLength()
    {
      return Length;
    }

  virtual bool CheckForNewData()
    {
      Ready = Device.readBits(BITS_STATUS_DAQ_FLASH_READY) == 1;
      return Ready;
    }

  virtual void Refresh()
    {
      if (Ready) {
        Device.readArea(AREA_ID, Data);
        CallObservers();
      }
    }

  virtual void Reset()
    {
      memset(Data, 0, DataSize);
      Device.writeArea(AREA_ID, Data);
      CallObservers();
    }
    
  virtual void* GetLastData()
    {
      return Data;
    }
  virtual void WriteFlashLen(uint32_t flashLen)
    {
      Device.writeWord(WORD_FLASH_LEN, 0, flashLen);
    }
};


}



#endif
