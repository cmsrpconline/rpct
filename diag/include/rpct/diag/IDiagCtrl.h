#ifndef IDIAGCTRL_H_
#define IDIAGCTRL_H_

#include "tb_ii.h"
#include "EIllegalStateTransition.h"

namespace rpct {

class IDiagCtrl {
public:    
    enum TState { 
        sInit,    // initial, 'unknown' state - Reset() should be called
        sReset,   // all configuration may be daone at this point
        sIdle,    // configuration is done, ready to start
        sRunning  // the test is now running
    };
    
    enum TTriggerType {
        ttManual = 0, 
        ttL1A,
        ttPretrigger0, 
        ttPretrigger1,
        ttPretrigger2,
        ttBCN0,
        ttLocal  
    };
    
    /*struct TConfigData {
        uint64_t CounterLimit;
        TTriggerType TriggerType;
        TConfigData(uint64_t counterLimit, TTriggerType triggerType)
            : CounterLimit(counterLimit), TriggerType(triggerType) {
        }
    };*/
    
        
    virtual std::string GetName() = 0;
    virtual IDevice& GetOwner() = 0;        
    virtual TState GetState() = 0;
    
    // state transitions and configuration
    virtual void Reset() = 0; 
    virtual void Configure(uint64_t counterLimit, TTriggerType triggerType, uint32_t triggerDelay) = 0;
    virtual void Start() = 0; 
    virtual void Stop() = 0;
    
    
    virtual bool CheckCountingEnded() = 0;
    
    virtual ~IDiagCtrl() {
    }
};

}

#endif /*IDIAGCTRL_H_*/
