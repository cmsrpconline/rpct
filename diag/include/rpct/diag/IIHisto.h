//---------------------------------------------------------------------------

//
//  Michal Pietrusinski 2001
//  Warsaw University
//


#ifndef IIHistoH                                   
#define IIHistoH
//---------------------------------------------------------------------------

#include "tb_ii.h"
#include "IHistoCtrl.h"


namespace rpct {
    
class TIIHistoCtrl: public IHistoCtrl {
private:
  TIIDevice& Owner;
  IDevice::TID AreaID;
  int BinCount;
  int CounterSize;
  std::string Name;
  int AreaStartIdx;

  static uint32_t* NullData;
  static int NullDataSize;
public:
  TIIHistoCtrl(TIIDevice& owner, IDevice::TID area_id, int areaStartIdx = 0, int areaLength = IDevice::npos);

  virtual ~TIIHistoCtrl()
    {
    }

  virtual void ReadData(uint32_t* data)
    {
      Owner.readArea(AreaID, data, AreaStartIdx, BinCount);
    }

  virtual int GetBinCount()
    { return BinCount; }

  virtual int GetCounterSize()
    { return CounterSize; }

  virtual std::string GetId()
    { return Name; }

  virtual void Reset();
  virtual TIIDevice& GetDevice() {
    return Owner;
  }
};


class TIIHistoCtrlMask : public TIIHistoCtrl {
public:
  typedef std::list<int> TMaskedBins;
private:
  TMaskedBins MaskedBins;
public:
  TIIHistoCtrlMask(TIIDevice& owner, IDevice::TID area_id, TMaskedBins mb)
    : TIIHistoCtrl(owner, area_id), MaskedBins(mb) {}
    
  virtual void ReadData(uint32_t* data);
};

/*
class TIIHistoRunCtrl {
public:
  enum TCountingState {
    csIndeterminate, csCounting, csReady, csPaused
  };
  typedef IDevice::TID TID;
private:
  TIIDevice& Owner;
  TCountingState CountingState;

  TID VECT_STATUS;
  TID BITS_STATUS_PROC_REQ;
  TID BITS_STATUS_PROC_ACK;
  TID BITS_STATUS_RATE_ENA;
  TID BITS_STATUS_RATE_START;
  TID BITS_STATUS_RATE_STOP;
  TID WORD_HIST_TIME_LIMIT;
  TID WORD_HIST_TIME_COUNT;   
public:
  TIIHistoRunCtrl(TIIDevice& owner, 
                  TID vectStatus,
                  TID bitProcReq,
                  TID bitProcAck,
                  TID bitRateEna,
                  TID bitRateStart,
                  TID bitRateStop,
                  TID timeLimit,
                  TID timeCount )
    : Owner(owner),
      CountingState(csIndeterminate),
      VECT_STATUS(vectStatus),
      BITS_STATUS_PROC_REQ(bitProcReq),
      BITS_STATUS_PROC_ACK(bitProcAck),
      BITS_STATUS_RATE_ENA(bitRateEna),
      BITS_STATUS_RATE_START(bitRateStart),
      BITS_STATUS_RATE_STOP(bitRateStop),
      WORD_HIST_TIME_LIMIT(timeLimit),
      WORD_HIST_TIME_COUNT(timeCount) {}

  TCountingState GetCountingState()
    { return CountingState; }

  void StartCounting();

  void StopCounting();

  bool CountingEnded()
    {
      return Owner.readBits(BITS_STATUS_RATE_STOP) == 1;
    }

  void PauseCounting();

  void ResumeCounting();


  void SetCounterLimit(char* value)
    {
      Owner.writeWord(WORD_HIST_TIME_LIMIT, 0, value);
    }

  void GetCounterLimit(char* value)
    {
      Owner.readWord(WORD_HIST_TIME_LIMIT, 0, value);
    }

  void GetCounterValue(char* value)
    {
      Owner.readWord(WORD_HIST_TIME_COUNT, 0, value);
    }

};
*/

}



#endif
