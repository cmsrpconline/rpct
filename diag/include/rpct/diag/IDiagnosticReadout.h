#ifndef IDIAGNOSTICREADOUT_H_
#define IDIAGNOSTICREADOUT_H_

#include "rpct/diag/IDiag.h"
#include "rpct/diag/IDiagnosticReadoutObserver.h"
#include "rpct/std/ENotImplemented.h"
#include <set>
//#include <boost/shared_ptr.hpp>


namespace rpct {

class IDiagnosticReadout : virtual public IDiag {
public:
    typedef std::set<int> TMask;
    typedef std::vector<TMask> TMasks;

    class Data {
    private:
      	void* data_;
      	bool lost_;
      	bool empty_;
      	int length_;
      	uint32_t wrAddr_;
      	uint32_t prevWrAddr_;
    public:
      	Data() : data_(0) {
        }
        
      	void* getData() {
      		return data_;
      	}        
      	void setData(void* data) {
      		data_ = data;
      	}
        
      	bool isLost() {
      		return lost_;
      	}        
      	void setLost(bool lost) {
      		lost_ = lost;
      	}
        
      	bool isEmpty() {
      		return empty_;
      	}
      	void setEmpty(bool empty) {
      		empty_ = empty;
      	}
        
      	int getLength() {
      		return length_;
      	}
      	void setLength(int length) {
			length_ = length;
      	}
        
      	uint32_t getWrAddr() {
      		return wrAddr_;
      	}
      	void setWrAddr(uint32_t wrAddr) {
      		wrAddr_ = wrAddr;
      	}
        
      	uint32_t getPrevWrAddr() {
      		return prevWrAddr_;
      	}
      	void setPrevWrAddr(uint32_t prevWrAddr) {
      		prevWrAddr_ = prevWrAddr;
      	}      
    };

    class BxData;
        
    class ConvertedBxData {
    public:
        virtual ~ConvertedBxData() {
        }        
        virtual int getIndex() = 0;
        virtual BxData& getBxData() = 0;
        virtual std::string toString() = 0; 
        virtual bool isNotEmpty() = 0;
    };
    
    class ConvertedBxDataFactory {
    public:
        virtual ~ConvertedBxDataFactory() {
        }
        
        virtual ConvertedBxData* create(BxData& bxData, IDiagnosticReadout& ownerReadout) = 0;
    };
    
    
    class Event;
    class BxData {
    private:
      	uint32_t triggers_;
      	uint32_t pos_;
      	int index_;
      	Event& event_;  
      	unsigned char* data_; 
        ConvertedBxData* convertedBxData_;
      	/*BxData(const BxData& data) {
            throw ENotImplemented("BxData(const BxData& data)");
        }*/
    public:
      	BxData(Event& event) 
        : event_(event), data_(new unsigned char[event.getEvents().getDataWidthBytes()]),
        convertedBxData_(0) {
        }
        
      	virtual ~BxData() {
          	delete [] data_;
            delete convertedBxData_;
        }
        
      	uint32_t getTriggers() {
          	return triggers_;
        }
      	void setTriggers(uint32_t triggers) {
          	triggers_ = triggers;
        }
      	uint32_t getPos() {
          	return pos_;
        }
      	void setPos(uint32_t pos) {
          	pos_ = pos;
        }
      	int getIndex() {
      		return index_;
      	}
      	void setIndex(int index) {
      		index_ = index;
      	}
      	
      	void* getData() {
          	return data_;
        }
        
        ConvertedBxData& getConvertedData() {
            if (convertedBxData_ == 0) {
                ConvertedBxDataFactory* f = event_.getEvents().getConvertedBxDataFactory(); 
                if (f == 0) {
                    throw TException("BxData::getConvertedData: ConvertedBxDataFactory not set");
                }
                convertedBxData_ = f->create(*this, event_.getEvents().getReadout());
            }
            return *convertedBxData_;
        }
        
        bool isNotEmpty() {
            getConvertedData();
            return convertedBxData_->isNotEmpty();
        }
    };
    
    class Events;
    class Event {
    public:
        typedef std::list<BxData*> BxDataList; 
    private:
      	unsigned char* timer_;
        BxDataList bxDataList_;        
      	Events& events_;
        
      	//Event(const Event& data) {
        //    throw ENotImplemented("Event(const Event& data)");
        //}
    public:
      	//typedef std::list<TBXData*> inherited;
      	//typedef inherited::iterator iterator;
      	Event(Events& events) 
        : timer_(new unsigned char[events.getTimerWidthBytes()]), events_(events) {
      	}
        
      	virtual ~Event() {            
          	for (BxDataList::iterator iBxData = bxDataList_.begin(); iBxData != bxDataList_.end(); ++iBxData)
            	delete *iBxData;
          	delete [] timer_;
        }

		Events& getEvents() {
			return events_;
		}

      	void* getTimer() {
          return timer_;
        }
        
      	BxData* addBxData() {
          	BxData* bxdata = new BxData(*this);
          	bxdata->setIndex(bxDataList_.size());
          	bxDataList_.push_back(bxdata);
          	return bxdata;
        }   
        
        const BxDataList& getBxDataList() {
            return bxDataList_;
        }
    };
    
    class Events {
    public:
        typedef std::list<Event*> EventList;
    private:
        IDiagnosticReadout& readout_;
      	int timerWidthBytes_;
      	int dataWidthBytes_;
      	bool lost_;
        EventList eventList_;
        ConvertedBxDataFactory* convertedBxDataFactory_;
    public:     

      	Events(IDiagnosticReadout& readout, int timerWidthBytes, int dataWidthBytes, bool lost, ConvertedBxDataFactory* convertedBxDataFactory)
        : readout_(readout), timerWidthBytes_(timerWidthBytes), dataWidthBytes_(dataWidthBytes), lost_(lost),
        convertedBxDataFactory_(convertedBxDataFactory) {
        }
        
      	virtual ~Events() {
          	for (EventList::iterator iEvent = eventList_.begin(); iEvent != eventList_.end(); ++iEvent) {
            	delete *iEvent;
            }            
        }
        
        IDiagnosticReadout& getReadout() {
            return readout_;
        }
        
        
      	Event* addEvent() {
          	Event* ev = new Event(*this);
          	eventList_.push_back(ev);
          	return ev;
        }
        
      	int getTimerWidthBytes() {
          	return timerWidthBytes_;
        }
           
      	int getDataWidthBytes() {
          	return dataWidthBytes_;
        }
        
        bool getLost() {
        	return lost_;
        }
        
        EventList& getEventList() {
            return eventList_;
        }
        
        ConvertedBxDataFactory* getConvertedBxDataFactory() {
            return convertedBxDataFactory_;
        }        
    };   
    
    // access to masks by one mask
    virtual TMask& GetMask(int index) = 0;
    virtual void SetMask0(int bxNum = -1) = 0; // -1 sets all available bxNums for mask 0
    virtual void WriteMasks() = 0;
    
    virtual void Configure(TMasks& masks, unsigned int daqDataDelay = 0) = 0;
    virtual void Configure(unsigned int daqDataDelay = 0) = 0; // writes also currently set masks

    //virtual void Reset(); - should be automatically called by implementing class method on proper diagCtrl state change
    //virtual void Init(); - should be automatically called by implementing class method on diagCtrl state change
    
    virtual void RegisterObserver(IDiagnosticReadoutObserver* observer) = 0;
    virtual void UnregisterObserver(IDiagnosticReadoutObserver* observer) = 0;

    virtual bool CheckForNewData() = 0; // should be automatically called by implementing class method on diagCtrl Stop()

    // before calling Refresh() CheckForNewData() must be called with TIMER_ENA = false
    // and TIMER_REQ = true on diag control
    //virtual void Refresh(); // should be automatically called by implementing class method on diagCtrl Stop()

    virtual Data& GetLastData() = 0;
    virtual Events& GetLastEvents() = 0;
    //virtual TEvents* InterpreteData(TData& data);
    
    virtual int GetAreaLen() = 0;
    virtual int GetAreaWidth() = 0;
    virtual int GetMaskNum() = 0;
    virtual int GetMaskWidth() = 0;
    virtual int GetDataWidth() = 0;
    virtual int GetTrgNum() = 0;
    virtual int GetTimerWidth() = 0;
    virtual ~IDiagnosticReadout() {}    
};


}


#endif /*IDIAGNOSTICREADOUT_H_*/
