#ifndef EINVALIDFORMAT_H_
#define EINVALIDFORMAT_H_

#include "tb_std.h"

namespace rpct {
    
    
class EInvalidFormat : public TException {
public:
    EInvalidFormat(const std::string& msg) throw()
    : TException(msg) {}
    //EVME( std::string msg = "" ): TException( "VME Error" + msg ) {}
    EXCFASTCALL virtual ~EInvalidFormat() throw() {}
};

}

#endif /*EINVALIDFORMAT_H_*/
