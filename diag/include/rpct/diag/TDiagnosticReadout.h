//---------------------------------------------------------------------------

#ifndef TDiagnosticReadoutH
#define TDiagnosticReadoutH
//---------------------------------------------------------------------------

#include "rpct/diag/IDiagnosticReadout.h"
#include "rpct/diag/TDiagCtrl.h"
#include <vector>
#include <set>
#include <log4cplus/logger.h>


namespace rpct {

class TDiagnosticReadout : virtual public IDiagnosticReadout {
private:
	static log4cplus::Logger logger;
public:
	static void setupLogger(std::string namePrefix);
    typedef IDevice::TID TID;
protected:
    IDiagnosable& Owner;
    //TIIDevice& Owner;
    //IDiagnosable* DiagnosableOwner;
    std::string Name;
    ConvertedBxDataFactory* convertedBxDataFactory_;
    IDiagCtrl& DiagCtrl;

    TID VECT_DIAG_DAQ;
    TID BITS_DIAG_DAQ_EMPTY;
    TID BITS_DIAG_DAQ_EMPTY_ACK;
    TID BITS_DIAG_DAQ_LOST;
    TID BITS_DIAG_DAQ_LOST_ACK;

    TID VECT_DIAG_DAQ_WR;
    TID BITS_DIAG_DAQ_WR_ADDR;
    TID BITS_DIAG_DAQ_WR_ACK;

    TID VECT_DIAG_DAQ_RD;
    TID BITS_DIAG_DAQ_RD_ADDR;
    TID BITS_DIAG_DAQ_RD_ACK;

    TID WORD_DIAG_DAQ_MASK;
    TID WORD_DATA_DAQ_DELAY;
    TID AREA_MEM_DIAG_DAQ;

    static const int AckRep = 10;
    typedef std::vector<IDiagnosticReadoutObserver*> TObserversVector;
    TObserversVector Observers;
    virtual void CallObservers();
    int AreaLen;
    int AreaWidth;
    int MaskWidth;
    int MaskNum;
    int DataWidth;
    int TrgNum;
    int TimerWidth;
    int HeaderPositions;

    int AreaWidthBytes;
    int TimerWidthBytes;
    int DataWidthBytes;


    uint32_t WrAddr;

    void ReadEmptyLost();
    void WriteDiagDaqRdAddr(uint32_t value);
    uint32_t ReadDiagDaqRdAddr();
    //void WriteDiagDaqWrAddr(uint32_t value);

    TMasks Masks;
    //unsigned char* Data;
    Data data_;
    Events* events_;

    class TInitPhaseObserver : public TDiagCtrl::IInitPhaseObserver {
    private:
        TDiagnosticReadout& Readout;
    public:
        TInitPhaseObserver(TDiagnosticReadout& readout)
                : Readout(readout) {
        }

        virtual void BeforeInitPhaseChange(TDiagCtrl& diagCtrl,
                                           TDiagCtrl::TInitPhase currentPhase, TDiagCtrl::TInitPhase newPhase);
        virtual void AfterInitPhaseChange(TDiagCtrl& diagCtrl,
                                          TDiagCtrl::TInitPhase previousPhase, TDiagCtrl::TInitPhase currentPhase);
    };

    TInitPhaseObserver* InitPhaseObserver;

    void ReadNewData();
    uint32_t GetTriggers(void* evData);
    uint32_t GetPos(void* evData);
    void GetTimer(void* evData, void* dest, int headerPos);
    void GetData(void* evData, void* dest);
public:
    TDiagnosticReadout(IDiagnosable& owner,//TIIDevice& owner,
                       const char* name,
                       ConvertedBxDataFactory* convertedBxDataFactory,
                       IDiagCtrl& diagCtrl,
                       TID vectDiagDaq,
                       TID bitsDiagDaqEmpty,
                       TID bitsDiagDaqEmptyAck,
                       TID bitsDiagDaqLost,
                       TID bitsDiagDaqLostAck,
                       TID vectDiagDaqWr,
                       TID bitsDiagDaqWrAddr,
                       TID bitsDiagDaqWrAck,
                       TID vectDiagDaqRd,
                       TID bitsDiagDaqRdAddr,
                       TID bitsDiagDaqRdAck,
                       TID wordDiagDaqMask,
                       TID wordDataDaqDelay,
                       TID areaDiagDaq,
                       int dataWidth,
                       int trgNum,
                       int timerWidth);

    virtual ~TDiagnosticReadout();

    virtual int GetAreaLen() {
        return AreaLen;
    }

    virtual int GetAreaWidth() {
        return AreaWidth;
    }

    virtual std::string GetName() {
        return Name;
    }

    virtual IDiagnosable& GetOwner() {
        //return *DiagnosableOwner;
        return Owner;
    }

    virtual IDiagCtrl& GetDiagCtrl() {
        return DiagCtrl;
    }

    virtual TMask& GetMask(int index);
    virtual void SetMask0(int bxNum = -1); // -1 sets all available bxNums for mask 0
    virtual void WriteMasks();

    virtual void Configure(TMasks& masks, unsigned int daqDataDelay = 0);
    virtual void Configure(unsigned int daqDataDelay = 0); // writes also currently set masks

    virtual void Reset();
    virtual void Init();


    virtual void RegisterObserver(IDiagnosticReadoutObserver* observer);
    virtual void UnregisterObserver(IDiagnosticReadoutObserver* observer);
    //void UnregisterAllObservers();

    virtual bool CheckForNewData();

    // before calling Refresh() CheckForNewData() must be called with TIMER_ENA = false
    // and TIMER_REQ = true on diag control
    virtual void Refresh() {
        //Data.Length = ReadNewData(Data.Data, Data.Lost);
        ReadNewData();
        CallObservers();
    }

    virtual Data& GetLastData() {
        return data_;
    }
    virtual Events& GetLastEvents();

    virtual Events* InterpreteData(Data& data);

    virtual int GetMaskNum() {
        return MaskNum;
    }

    virtual int GetMaskWidth() {
        return MaskWidth;
    }

    virtual int GetDataWidth() {
        return DataWidth;
    }

    virtual int GetTrgNum() {
        return TrgNum;
    }

    virtual int GetTimerWidth() {
        return TimerWidth;
    }
    virtual uint32_t ReadDiagDaqWrAddr();
};

}

#endif
