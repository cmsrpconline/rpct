#ifndef IDIAG_
#define IDIAG_

#include "rpct/diag/IDiagnosable.h"

namespace rpct {
    
    
class IDiag {
public:	
    virtual std::string GetName() = 0;
    virtual IDiagnosable& GetOwner() = 0;
    virtual IDiagCtrl& GetDiagCtrl() = 0;
	virtual ~IDiag() {}	
};


}

#endif /*IDIAG_*/
