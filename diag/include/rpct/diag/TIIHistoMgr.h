#ifndef TIIHISTOMGR_H_
#define TIIHISTOMGR_H_

#include "rpct/diag/THistoMgr.h"


namespace rpct {

class TIIHistoMgr : public THistoMgr {
private:
    IDiagnosable& DiagnosableOwner;
    std::string Name;
    IDiagCtrl& DiagCtrl;
public:
	TIIHistoMgr(TIIDevice& owner, const char* name, IDiagCtrl& diagCtrl, IDevice::TID area_id, 
			int areaStartIdx = 0, int areaLength = IDevice::npos);	    
	virtual ~TIIHistoMgr();
	
	virtual std::string GetName() {
		return Name;
	}
    virtual IDiagnosable& GetOwner() {
    	return DiagnosableOwner;
    }
    virtual IDiagCtrl& GetDiagCtrl() {
    	return DiagCtrl;
    }
};

}

#endif /*TIIHISTOMGR_H_*/
