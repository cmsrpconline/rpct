#ifndef IHistoCtrH
#define IHistoCtrH

#include <string>
#include <stdint.h>
#include <cstring> 

namespace rpct {
    
class IHistoCtrl {
public:
  virtual void ReadData(uint32_t*) = 0;
  // virtual void WriteData(uint32_t*) = 0;     //opcjonalnie
  virtual int GetBinCount() = 0;   //liczba binow
  virtual int GetCounterSize() = 0;  //liczba bitow w jednym binie
  virtual std::string GetId() = 0;
  virtual void Reset() = 0;

  virtual ~IHistoCtrl() {}
};



class THistoCtrlMem: public IHistoCtrl {
protected:
  std::string ID;
  int BinCount;
  int CounterSize;
  uint32_t* Data;
public:
  THistoCtrlMem(std::string id, int bincount, int countersize )
   : ID(id), BinCount(bincount), CounterSize(countersize)
    {
      Data = new uint32_t[BinCount];
    }

  virtual ~THistoCtrlMem()
    {
      delete [] Data;
    }

  virtual int GetBinCount()
    { return BinCount; }

  virtual void SetData(unsigned char* data)
    {
      for (int i=0; i<BinCount; i++)
        Data[i] = data[i];
    }

  virtual void ReadData(uint32_t* data)
    {
      memcpy(data, Data, BinCount * sizeof(uint32_t));
    }

  virtual std::string GetId()
    {
      return ID;
    }
  virtual int GetCounterSize()
    { return CounterSize; }

  virtual void Reset() {}
} ;

}


#endif
