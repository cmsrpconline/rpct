#ifndef IRPCTPULSER_H_
#define IRPCTPULSER_H_

#include <string>
#include "rpct/diag/IHistoObserver.h"
#include "rpct/diag/IDiag.h"
#include "rpct/std/BigInteger.h"

namespace rpct {

class IPulser : public IDiag {
public:
    virtual int GetMaxLength() = 0;
    virtual int GetWidth() = 0;
    virtual void Configure(std::vector<BigInteger>& data, uint64_t pulseLength,
                    bool repeat, int target) = 0;
};

}

#endif /*IHISTOMGR_H_*/
