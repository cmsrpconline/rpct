//---------------------------------------------------------------------------

#ifndef TDiagCtrlH
#define TDiagCtrlH
//---------------------------------------------------------------------------  
#include "tb_ii.h"
#include "IDiagCtrl.h"
 
#include <log4cplus/logger.h>

namespace rpct {

class TDiagCtrl : virtual public IDiagCtrl {
public:
	static void setupLogger(std::string namePrefix);
    enum TInitPhase {
    	ipReqOffEnaOff,
    	ipReqOnEnaOff,
    	ipReqOnEnaOn,
    };
    class IInitPhaseObserver {
    public:
    	virtual void BeforeInitPhaseChange(TDiagCtrl& diagCtrl, 
    		TInitPhase currentPhase, TInitPhase newPhase) = 0;
    	virtual void AfterInitPhaseChange(TDiagCtrl& diagCtrl, 
    		TInitPhase previousPhase, TInitPhase currentPhase) = 0;
    	virtual ~IInitPhaseObserver() {}
    };
    
    typedef IDevice::TID TID;
private:
    static log4cplus::Logger logger;
    static const int AckRep = 4;
    TIIDevice& Owner;
    TState State;
    bool Local;

    TID VECT_STATUS;
    TID BITS_STATUS_PROC_REQ;
    TID BITS_STATUS_PROC_ACK;
    TID BITS_STATUS_LOC_ENA;
    TID BITS_STATUS_GLO_ENA;
    TID BITS_STATUS_START;
    TID BITS_STATUS_STOP;
    TID BITS_STATUS_TRIG;
    TID WORD_TIMER_LIMIT;   
    TID WORD_TIMER_COUNT;
    TID WORD_TRIGGER_DELAY;

    std::string Name;
    
    typedef std::list<IInitPhaseObserver*> TInitPhaseObservers;
    TInitPhaseObservers InitPhaseObservers;
    
    //static bool Tracking;
    void CheckStateTransition(TState newState);
    void WaitForBits(TID bits, uint32_t expectedValue);
    uint32_t ClearBits();
    
    TInitPhase InitPhase;
    void NotifyBeforeInitPhaseChange(TInitPhase newPhase);
    void ChangeInitPhase(TInitPhase newPhase); // change phase and notify observers
public:
    TDiagCtrl(TIIDevice& owner, TID vectStatus, TID bitProcReq, TID bitProcAck, TID bitLocEna,
        TID bitGloEna, TID bitStart, TID bitStop, TID bitTrig, TID wordTimerLimit,
        TID wordTimerCount, TID wordTriggerDelay, std::string name);

    virtual std::string GetName() {
        return Name;
    }    
    
    virtual IDevice& GetOwner() {
        return Owner;
    }     
    
    virtual TState GetState() {
        return State;
    }

    // deprecated
    void StartCounting(bool local);

    // deprecated
    void StopCounting();

    // deprecated
    bool CountingEnded();

    // deprecated
    //void PauseCounting();

    // deprecated
    //void ResumeCounting();

    // deprecated
    void SetCounterLimit(unsigned char* value);

    // deprecated
    void GetCounterLimit(unsigned char* value);

    // deprecated
    void GetCounterValue(unsigned char* value);

    // deprecated
    void SetCounterEna(bool value); 

    /*static void SetTracking(bool value) {
        Tracking = value;
    }*/

    bool IsLocal() {
        return Local;
    }

    void SetLocal(bool value) {
        Local = (BITS_STATUS_GLO_ENA != 0) ? value : true;
    }
    
    TTriggerType GetTriggerType();
    // deprecated
    void SetTriggerType(TTriggerType triggerType);

    uint32_t GetTriggerDelay();
    void SetTriggerDelay(uint32_t triggerDelay);
    
    // state transitions and configuration
    virtual void Reset();
    virtual void Configure(uint64_t counterLimit, TTriggerType triggerType, uint32_t triggerDelay);
    virtual void Start();
    virtual void Stop();
    
    virtual bool CheckCountingEnded();
    
    virtual void RegisterInitPhaseObserver(IInitPhaseObserver& observer);
    virtual void UnregisterInitPhaseObserver(IInitPhaseObserver& observer);
};

}

#endif
