//---------------------------------------------------------------------------

#ifndef TStandardDiagnosticReadoutH
#define TStandardDiagnosticReadoutH
//---------------------------------------------------------------------------

#include "rpct/diag/TDiagnosticReadout.h" 
#include "rpct/std/bitcpy.h"      


namespace rpct {

class TStandardDiagnosticReadout : public TDiagnosticReadout {
private:
    int BCNWidth;
    int EvNumWidth;
public:
    TStandardDiagnosticReadout(IDiagnosable& owner,
                         const char* name,
                         ConvertedBxDataFactory* convertedBxDataFactory,
                         IDiagCtrl& diagCtrl,
                         TID vectDiagDaq,
                         TID bitsDiagDaqEmpty,
                         TID bitsDiagDaqEmptyAck,
                         TID bitsDiagDaqLost,
                         TID bitsDiagDaqLostAck,
                         TID vectDiagDaqWr,
                         TID bitsDiagDaqWrAddr,
                         TID bitsDiagDaqWrAck,
                         TID vectDiagDaqRd,
                         TID bitsDiagDaqRdAddr,
                         TID bitsDiagDaqRdAck,
                         TID wordDiagDaqMask,
                         TID wordDataDaqDelay,
                         TID areaDiagDaq,
                         int dataWidth,
                         int trgNum,
                         int timerWidth,
                         int bcnWidth)
            : TDiagnosticReadout(owner, name, convertedBxDataFactory, diagCtrl, vectDiagDaq, bitsDiagDaqEmpty,
                                 bitsDiagDaqEmptyAck, bitsDiagDaqLost, bitsDiagDaqLostAck,
                                 vectDiagDaqWr, bitsDiagDaqWrAddr, bitsDiagDaqWrAck, vectDiagDaqRd,
                                 bitsDiagDaqRdAddr, bitsDiagDaqRdAck, wordDiagDaqMask, wordDataDaqDelay,
                                 areaDiagDaq,  dataWidth, trgNum, timerWidth),
    BCNWidth(bcnWidth), EvNumWidth(bcnWidth * 2) {
    }

    virtual void ParseTimer(Event& ev, uint32_t& bcn, uint32_t& evnum) {
        bcn = 0;
        bitcpy(&bcn, 0, ev.getTimer(), 0, BCNWidth);
        evnum = 0;
        bitcpy(&evnum, 0, ev.getTimer(), BCNWidth, EvNumWidth);
    }
};

}

#endif
