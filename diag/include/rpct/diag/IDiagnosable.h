#ifndef IDIAGNOSABLE_H_
#define IDIAGNOSABLE_H_

#include <vector>
#include "rpct/diag/IDiagCtrl.h"


namespace rpct {

class IDiag;

class IDiagnosable : virtual public IDevice {
public:

    enum TTriggerDataSel {
    	tdsNone = 0,
        tdsL1A,
        tdsPretrigger0, 
        tdsPretrigger1,
        tdsPretrigger2,
        tdsBCN0,
        tdsLocal  
    };

	typedef std::vector<IDiagCtrl*> TDiagCtrlVector;
	typedef std::vector<IDiag*> TDiagVector;
	
	virtual TDiagCtrlVector& GetDiagCtrls() = 0;	
	virtual TDiagVector& GetDiags() = 0;
	virtual ~IDiagnosable() {}
    
    virtual void ConfigureDiagnosable(TTriggerDataSel triggerDataSel, uint32_t dataTrgDelay) = 0;
};

}

#endif /*IDIAGNOSABLE_H_*/
