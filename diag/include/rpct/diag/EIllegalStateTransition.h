#ifndef EILLEGALSTATETRANSITION_H_
#define EILLEGALSTATETRANSITION_H_

#include "tb_std.h"

namespace rpct {
    
class EIllegalStateTransition : public TException {
public:
	EIllegalStateTransition() throw() {
    }
    
    EIllegalStateTransition(const std::string& msg) throw()
        : TException(msg) {
    }
	virtual ~EIllegalStateTransition() throw() {
    }
};

}
#endif /*EILLEGALSTATETRANSITION_H_*/
