#ifndef IHISTOOBSERVER_H_
#define IHISTOOBSERVER_H_


namespace rpct {

class IHistoMgr;

class IHistoObserver {
public:
  virtual void HistoChanged(IHistoMgr* histo) = 0;

  virtual ~IHistoObserver() {}
};

}

#endif /*IHISTOOBSERVER_H_*/
