//---------------------------------------------------------------------------

#ifndef THistoRateCtrlH
#define THistoRateCtrlH
//---------------------------------------------------------------------------

#include "THistoMgr.h"
#include "tb_std.h"


namespace rpct {

class THistoRateCtrl: public IHistoCtrl, public IHistoObserver {
private:
  THistoMgr* SourceHisto;
  uint32_t* LastSourceData;
  uint32_t* Data;
  int BinCount;
  std::string ID;
public:
  THistoRateCtrl(THistoMgr* sourceHisto, bool autorefresh = true)
   : SourceHisto(sourceHisto), LastSourceData(NULL), Data(NULL)
    {
      if (SourceHisto == NULL)
        throw TException("THistoRateCtrl: sourceHisto == NULL");

      SourceHisto->RegisterObserver(this);

      ID = std::string("Rate:") + SourceHisto->GetID();

      BinCount = SourceHisto->GetBinCount();
      LastSourceData = new uint32_t[BinCount];
      Data = new uint32_t[BinCount];

      memset(LastSourceData, 0, BinCount * sizeof(uint32_t));
    }

  virtual ~THistoRateCtrl()
    {                   
      if (LastSourceData != NULL)
        delete LastSourceData;
      if (Data != NULL)
        delete Data;
    }
    
  virtual void HistoChanged(IHistoMgr* histo)
    {
      uint32_t* newdata = histo->GetLastData();

      for(int i=0; i<BinCount; i++)
        Data[i] = newdata[i] - LastSourceData[i];

      memcpy(LastSourceData, newdata, BinCount * sizeof(uint32_t));
    }

  virtual void ReadData(uint32_t* data)
    {
      memcpy(data, Data, BinCount * sizeof(uint32_t));
    }

  // virtual void WriteData(uint32_t*) = 0;     //opcjonalnie
  virtual int GetBinCount()
    {
      return BinCount;
    }

  virtual int GetCounterSize()
    {
      return SourceHisto->GetHistoCtrl()->GetCounterSize();
    }

  virtual std::string GetId()
    {
      return ID;
    }
  virtual void Reset()
    {
      throw TException("Reset not implemented");
    }

};

}

#endif
