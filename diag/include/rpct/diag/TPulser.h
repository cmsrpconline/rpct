//---------------------------------------------------------------------------

#ifndef RPCT_PULSER_H
#define RPCT_PULSER_H
//---------------------------------------------------------------------------  
#include "tb_ii.h"
#include "rpct/diag/IPulser.h"
#include "rpct/diag/TDiagCtrl.h"
 
#include <log4cplus/logger.h>

namespace rpct {

class TPulser : virtual public IPulser {
public:    
    typedef IDevice::TID TID;
private:
    static log4cplus::Logger logger;
    IDiagnosable& Owner;
    std::string Name;
    IDiagCtrl& DiagCtrl;

    TID AREA_MEM_PULSE;
    TID WORD_PULSER_LENGTH;
    TID BITS_REPEAT_ENA;
    TID BITS_PULSER_OUT_ENA;
    
    unsigned int MaxLength;
    unsigned int Width;
    unsigned int WidthBytes;

    class TInitPhaseObserver : public TDiagCtrl::IInitPhaseObserver {
    private:
    	TPulser& Pulser;
    public:
    	TInitPhaseObserver(TPulser& pulser)
    	: Pulser(pulser) {
    	}

    	virtual void BeforeInitPhaseChange(TDiagCtrl& diagCtrl,
    			TDiagCtrl::TInitPhase currentPhase, TDiagCtrl::TInitPhase newPhase);
    	virtual void AfterInitPhaseChange(TDiagCtrl& diagCtrl,
    			TDiagCtrl::TInitPhase previousPhase, TDiagCtrl::TInitPhase currentPhase);
    };

    TInitPhaseObserver* InitPhaseObserver;
public:
    TPulser(IDiagnosable& owner, const char* name, IDiagCtrl& diagCtrl, 
        TID areaMemPulse, TID wordPulserLength, TID bitsRepeatEna, TID bitsPulserOutEna);
    
    virtual ~TPulser();

    virtual std::string GetName() {
        return Name;
    }    
    
    virtual IDiagnosable& GetOwner() {
        return Owner;
    }
    
    virtual IDiagCtrl& GetDiagCtrl() {
        return DiagCtrl;
    }

    virtual int GetMaxLength() {
        return MaxLength;
    }
    
    virtual int GetWidth() {
        return Width;
    }
    
    virtual void writeTarget(int target) {
        if (target != 0) {
            throw TException("TPulser::WriteTarget: do not know how to set target");
        }
    }
    
    virtual void Configure(std::vector<BigInteger>& data, uint64_t pulseLength,
                    bool repeat, int target);
    
    virtual void Start();
    virtual void Stop();

};

}

#endif
