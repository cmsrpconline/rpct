#ifndef IHISTOMGR_H_
#define IHISTOMGR_H_

#include <string>
#include <vector>
#include "rpct/diag/IHistoObserver.h"
#include "rpct/diag/IDiag.h"

namespace rpct {
    
class IHistoMgr : public IDiag {
public:
  virtual std::string GetID() = 0;
  virtual int GetBinCount() = 0;
  virtual uint32_t* GetLastData() = 0;
  virtual void RegisterObserver(IHistoObserver* observer) = 0;
  virtual void UnregisterObserver(IHistoObserver* observer) = 0;
  virtual void Reset() = 0;
  virtual void Refresh() = 0;
};

typedef std::vector<IHistoMgr*> HistoMgrVector;
}


#endif /*IHISTOMGR_H_*/
