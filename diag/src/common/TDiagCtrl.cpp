//---------------------------------------------------------------------------
#ifdef __BORLANDC__
#include "precompiled.h"
#pragma hdrstop
#endif

#include <sstream>
#include "rpct/diag/TDiagCtrl.h"
#include "rpct/devices/FixedHardwareSettings.h"
//---------------------------------------------------------------------------

using namespace std;
using namespace log4cplus;


namespace rpct {

Logger TDiagCtrl::logger = Logger::getInstance("TDiagCtrl");
void TDiagCtrl::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}

//bool TDiagCtrl::Tracking = false;

TDiagCtrl::TDiagCtrl(TIIDevice& owner, TID vectStatus, TID bitProcReq, TID bitProcAck,
                     TID bitLocEna, TID bitGloEna, TID bitStart, TID bitStop, TID bitTrig,
                     TID wordTimerLimit, TID wordTimerCount, TID wordTriggerDelay, string name)
: Owner(owner), State(sInit), VECT_STATUS(vectStatus),
BITS_STATUS_PROC_REQ(bitProcReq), BITS_STATUS_PROC_ACK(bitProcAck), 
BITS_STATUS_LOC_ENA(bitLocEna),  BITS_STATUS_GLO_ENA(bitGloEna), 
BITS_STATUS_START(bitStart), BITS_STATUS_STOP(bitStop),
BITS_STATUS_TRIG(bitTrig), WORD_TIMER_LIMIT(wordTimerLimit), 
WORD_TIMER_COUNT(wordTimerCount), WORD_TRIGGER_DELAY(wordTriggerDelay), 
Name(name), InitPhase(ipReqOffEnaOff) {
  	//Name = Owner.GetName() + " Diag Control";
    SetLocal(true);
}

void TDiagCtrl::WaitForBits(TID bits, uint32_t expectedValue) {
	int repeat = 0;
  	while(Owner.readBits(bits) != expectedValue) {
  		//usleep(FixedHardwareSettings::CONFIGURE_FROM_FLASH_TIME); //should be at least FixedHardwareSettings::CONFIGURE_FROM_FLASH_TIME, as the LB reloading can happen when the TDiagCtrl::StartCounting is done
    	if (repeat++ > AckRep) {
    		ostringstream ostr;
    		ostr<<Owner.getBoard().getDescription()<<" "<<Owner.getName()<<" "<<Owner.getType().getType()<<" pos "<<Owner.getPosition()<<" "
    		<< "Couldn't set " << Owner.getItemDesc(bits).Name << " to " << expectedValue;
    		LOG4CPLUS_DEBUG(logger, Name << " "<<__FUNCTION__<< " Couldn't set " << Owner.getItemDesc(bits).Name << " to " << expectedValue<<endl);
      		throw TException(ostr.str());
    	}
  	}
}

uint32_t TDiagCtrl::ClearBits() {
	uint32_t status = Owner.readVector(VECT_STATUS);

  	Owner.setBitsInVector(BITS_STATUS_PROC_REQ, 0, status);
   	Owner.setBitsInVector(BITS_STATUS_LOC_ENA, 0, status);
   	if (BITS_STATUS_GLO_ENA != 0) {
      	Owner.setBitsInVector(BITS_STATUS_GLO_ENA, 0, status);
   	}
  	Owner.setBitsInVector(BITS_STATUS_START, 0, status);
  	Owner.writeVector(VECT_STATUS, status);
  	InitPhase = ipReqOffEnaOff;
	return status;
}
    
void TDiagCtrl::StartCounting(bool local)
{
    LOG4CPLUS_DEBUG(logger, Name << " StartCounting local = " << (local ? "true" : "false"));
 
  if (BITS_STATUS_GLO_ENA != 0)
    Local = local;
  else
    Local = true;


  uint32_t status = ClearBits();

  Owner.setBitsInVector(BITS_STATUS_PROC_REQ, 1, status);
  Owner.writeVector(VECT_STATUS, status);  
  WaitForBits(BITS_STATUS_PROC_ACK, 1);
  
  Owner.setBitsInVector(BITS_STATUS_START, 1, status);
  Owner.writeVector(VECT_STATUS, status);
  if (Owner.readBits(BITS_STATUS_START) != 1 )
    throw TException(Owner.getBoard().getDescription() +  string(" StartCounting: Couldn't set BITS_STATUS_START to 1"));
  
 
    LOG4CPLUS_DEBUG(logger, Name << " StartCounting Done ");
}

void TDiagCtrl::StopCounting()
{
    LOG4CPLUS_DEBUG(logger, Name << " StopCounting");
      
  uint32_t status = Owner.readVector(VECT_STATUS);;
 
  Owner.setBitsInVector(BITS_STATUS_PROC_REQ, 0, status);    
  Owner.setBitsInVector(BITS_STATUS_START, 0, status);
  Owner.writeVector(VECT_STATUS, status);

  int repeat = 0;
  while(Owner.readBits(BITS_STATUS_PROC_ACK) != 0) {
	  usleep(10000);
    if (repeat++ > AckRep)
      throw TException(Owner.getBoard().getDescription() + " StopCounting: Couldn't set BITS_STATUS_PROC_ACK to 0");
    //if during that function Hard Reset occur, it is OK to get the exception and break TDiagCtrl::Stop()
    //the readout will not be performed, and the state will remain sRunning
  }

  LOG4CPLUS_DEBUG(logger, Name << " StopCounting Done ");
}


bool TDiagCtrl::CountingEnded()
{
    if (logger.getLogLevel() <= DEBUG_LOG_LEVEL) {
      bool value = Owner.readBits(BITS_STATUS_STOP) == 1;  
      LOG4CPLUS_DEBUG(logger, Name << " CountingEnded = " << (value ? "true" : "false"));
      return value;
    }
    else {
      return Owner.readBits(BITS_STATUS_STOP) == 1;
    }
}


/*void TDiagCtrl::PauseCounting()
{
  #ifdef TB_DEBUG
    if (Tracking)
      G_Log.out() << Name << " PauseCounting" << std::endl;
  #endif

  uint32_t status = Owner.readVector(VECT_STATUS);
  Owner.setBitsInVector(BITS_STATUS_PROC_REQ, 0, status);
  Owner.writeVector(VECT_STATUS, status);

  int repeat = 0;
  while(Owner.readBits(BITS_STATUS_PROC_ACK) != 0)
    if (repeat++ > AckRep)
      throw TException("PauseCounting: Couldn't set BITS_STATUS_PROC_ACK to 0");

          
  #ifdef TB_DEBUG
    if (Tracking)
      G_Log.out() << Name << " PauseCounting Done" << std::endl;
  #endif
}

void TDiagCtrl::ResumeCounting()
{
  #ifdef TB_DEBUG
    if (Tracking)
      G_Log.out() << Name << " ResumeCounting" << std::endl;
  #endif

  uint32_t status = Owner.readVector(VECT_STATUS);

  Owner.setBitsInVector(BITS_STATUS_PROC_REQ, 1, status);
  Owner.writeVector(VECT_STATUS, status);
  if (Owner.readBits(BITS_STATUS_PROC_REQ) != 1 )
    throw TException("ResumeCounting: Couldn't set BITS_STATUS_PROC_REQ to 1");

  int repeat = 0;
  while(Owner.readBits(BITS_STATUS_PROC_ACK) != 1)
    if (repeat++ > AckRep)
      throw TException("ResumeCounting: Couldn't set BITS_STATUS_PROC_ACK to 1");
  
  #ifdef TB_DEBUG
    if (Tracking)
      G_Log.out() << Name << " ResumeCounting Done" << std::endl;
  #endif
}*/

void TDiagCtrl::SetCounterLimit(unsigned char* value) {
  	LOG4CPLUS_DEBUG(logger, Name << "Setting counter limit");
  	Owner.writeWord(WORD_TIMER_LIMIT, 0, value);
}

void TDiagCtrl::GetCounterLimit(unsigned char* value) {
  	LOG4CPLUS_DEBUG(logger, Name << "Getting counter limit");
    Owner.readWord(WORD_TIMER_LIMIT, 0, value);
}

void TDiagCtrl::GetCounterValue(unsigned char* value) {
  	LOG4CPLUS_DEBUG(logger, Name << "Getting counter value");
    Owner.readWord(WORD_TIMER_COUNT, 0, value);
}


void TDiagCtrl::SetCounterEna(bool value)
{            
  LOG4CPLUS_DEBUG(logger, Name << " SetCounterEna " << (value ? "true" : "false"));
  /*TID id = Local ? BITS_STATUS_LOC_ENA : BITS_STATUS_GLO_ENA;
  uint32_t val = value ? 1 : 0;
  Owner.writeBits(id, val);
  if (Owner.readBits(id) != val)
    throw TException("SetCounterEna: Couldn't set BITS_STATUS_LOC_ENA or BITS_STATUS_GLO_ENA");
    */
  if (value) {   
    uint32_t status = Owner.readVector(VECT_STATUS);
    Owner.setBitsInVector(Local ? BITS_STATUS_LOC_ENA
                                : BITS_STATUS_GLO_ENA, 1, status);

    Owner.writeVector(VECT_STATUS, status);
    if (Owner.readBits(Local ? BITS_STATUS_LOC_ENA
                                : BITS_STATUS_GLO_ENA) != 1 )
      throw TException(Owner.getBoard().getDescription() + " SetCounterEna: Couldn't set BITS_STATUS_LOC_ENA to 1");
  }
  else {   
    uint32_t status = Owner.readVector(VECT_STATUS);
    Owner.setBitsInVector(BITS_STATUS_LOC_ENA, 0, status);
    if (BITS_STATUS_GLO_ENA != 0) {
      Owner.setBitsInVector(BITS_STATUS_GLO_ENA, 0, status);
    }
    Owner.writeVector(VECT_STATUS, status);  
    if (Owner.readBits(Local ? BITS_STATUS_LOC_ENA
                                : BITS_STATUS_GLO_ENA) != 0 ) {
      throw TException(Owner.getBoard().getDescription() + " SetCounterEna: Couldn't set BITS_STATUS_LOC_ENA to 0");
    }
  }
  LOG4CPLUS_DEBUG(logger, Name << " SetCounterEna Done");
}


TDiagCtrl::TTriggerType TDiagCtrl::GetTriggerType() {
	if (BITS_STATUS_TRIG == 0) {
		G_Log.out() << "TDiagCtrl::GetTriggerType: trigger type cannot be read: BITS_STATUS_TRIG = 0" << endl;
		return ttManual;
	}
    return (TTriggerType) Owner.readBits(BITS_STATUS_TRIG);
}

void TDiagCtrl::SetTriggerType(TDiagCtrl::TTriggerType triggerType) {
    LOG4CPLUS_DEBUG(logger, Name << " SetTriggerType " << triggerType);
	if (BITS_STATUS_TRIG == 0) {
		if (triggerType != ttManual) {
			throw TException(Owner.getBoard().getDescription() + " TDiagCtrl: SetTriggerType called with triggerType != ttManual. Only ttManual is supprted for this diag ctrl.");
		}
	}
	else {
    	Owner.writeBits(BITS_STATUS_TRIG, triggerType);
	}
}


uint32_t TDiagCtrl::GetTriggerDelay() {
    if(WORD_TRIGGER_DELAY != 0)
        return Owner.readWord(WORD_TRIGGER_DELAY, 0);
    
    LOG4CPLUS_WARN(logger, Name << " WORD_TRIGGER_DELAY not existing, GetTriggerDelay() returned 0 ");
    return 0;
}

void TDiagCtrl::SetTriggerDelay(uint32_t triggerDelay) {
    LOG4CPLUS_DEBUG(logger, Name << " SetTriggerDelay " << triggerDelay);
    if(WORD_TRIGGER_DELAY != 0) {
        Owner.writeWord(WORD_TRIGGER_DELAY, 0, triggerDelay);
    }
    else {
        LOG4CPLUS_WARN(logger, Name << " WORD_TRIGGER_DELAY not existing, SetTriggerDelay() did nothing ");
    }
}

void TDiagCtrl::CheckStateTransition(TState newState) {
    if (newState == sReset) {
        return;
    }
    else if (State == sReset) {
        if (newState == sIdle) {
            return;
        }
        throw EIllegalStateTransition("TDiagCtrl::CheckStateTransition: Illegal state transistion");            
    }
    else if (State == sIdle) {
        if (newState == sRunning) {
            return;
        }
        throw EIllegalStateTransition("TDiagCtrl::CheckStateTransition: Illegal state transistion");            
    }
    else if (State == sRunning) {
        if (newState == sIdle) {
            return;
        }
        throw EIllegalStateTransition("TDiagCtrl::CheckStateTransition: Illegal state transistion");            
    }
}

void TDiagCtrl::Reset() { 
    LOG4CPLUS_DEBUG(logger, Name << " Reset");
    try {
	    SetCounterEna(false);
	    StopCounting();
	    State = sReset;
    }    
    catch (TException& e) {
    	LOG4CPLUS_ERROR(logger, "Exception during reset of " <<  Name 
    		<< ": " << e.what());
		throw;
    }     
}

void TDiagCtrl::Configure(uint64_t counterLimit, TDiagCtrl::TTriggerType triggerType, uint32_t triggerDelay) {
    CheckStateTransition(sIdle);
    SetCounterLimit((unsigned char*)&counterLimit);
    SetTriggerType(triggerType);
    SetTriggerDelay(triggerDelay);
    //StartCounting(true);
    State = sIdle;
}

void TDiagCtrl::Start() {
    CheckStateTransition(sRunning);
    NotifyBeforeInitPhaseChange(ipReqOnEnaOff);    
    StartCounting(true);
	ChangeInitPhase(ipReqOnEnaOff);
    NotifyBeforeInitPhaseChange(ipReqOnEnaOn); 
    SetCounterEna(true);
	ChangeInitPhase(ipReqOnEnaOn);
    State = sRunning;
}
    
void TDiagCtrl::Stop() {
    CheckStateTransition(sIdle);
    NotifyBeforeInitPhaseChange(ipReqOnEnaOff); 
    SetCounterEna(false);
	ChangeInitPhase(ipReqOnEnaOff);
    NotifyBeforeInitPhaseChange(ipReqOffEnaOff); 
    StopCounting();
	ChangeInitPhase(ipReqOffEnaOff);
    State = sIdle;
}

bool TDiagCtrl::CheckCountingEnded() {
    return CountingEnded();
}

void TDiagCtrl::RegisterInitPhaseObserver(IInitPhaseObserver& observer) {
	InitPhaseObservers.push_back(&observer);
}

void TDiagCtrl::UnregisterInitPhaseObserver(IInitPhaseObserver& observer) {
	InitPhaseObservers.remove(&observer);
}

    
void TDiagCtrl::NotifyBeforeInitPhaseChange(TInitPhase newPhase) {
	for (TInitPhaseObservers::iterator iObs = InitPhaseObservers.begin();
		 	iObs != InitPhaseObservers.end(); ++iObs) {
		 (*iObs)->BeforeInitPhaseChange(*this, InitPhase, newPhase);
	}
}

void TDiagCtrl::ChangeInitPhase(TInitPhase newPhase) {
	TInitPhase previous = InitPhase;
	InitPhase = newPhase;
	for (TInitPhaseObservers::iterator iObs = InitPhaseObservers.begin();
		 	iObs != InitPhaseObservers.end(); ++iObs) {
		 (*iObs)->AfterInitPhaseChange(*this, previous, InitPhase);
	}
}

}
