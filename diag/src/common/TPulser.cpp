#include "rpct/diag/TPulser.h"
#include "rpct/std/IllegalArgumentException.h"

using namespace std;
namespace rpct {

TPulser::TPulser(IDiagnosable& owner, const char* name, IDiagCtrl& diagCtrl, 
        TID areaMemPulse, TID wordPulserLength, TID bitsRepeatEna, TID bitsPulserOutEna) 
: Owner(owner), Name(name), DiagCtrl(diagCtrl), AREA_MEM_PULSE(areaMemPulse),
WORD_PULSER_LENGTH(wordPulserLength), BITS_REPEAT_ENA(bitsRepeatEna),
BITS_PULSER_OUT_ENA(bitsPulserOutEna), InitPhaseObserver(0) {	
    
    IDevice::TItemDesc desc = owner.getItemDesc(AREA_MEM_PULSE);
    if (desc.Type != IDevice::itArea) {
        throw TException("TPulser: areaMemPulse id is not an area");
    }

    Width = desc.Width;
    WidthBytes =  (Width - 1) / 8 + 1;
    MaxLength = desc.Number;
    
    TDiagCtrl* dc = dynamic_cast<TDiagCtrl*>(&DiagCtrl);
    if (dc != NULL) {
        InitPhaseObserver = new TInitPhaseObserver(*this);
        dc->RegisterInitPhaseObserver(*InitPhaseObserver);
    }
}

TPulser::~TPulser() {
    delete InitPhaseObserver;
}


void TPulser::Configure(std::vector<BigInteger>& data, uint64_t pulseLength,
        bool repeat, int target) {
    if ((data.size() < MaxLength) && (pulseLength > data.size())) {
        throw IllegalArgumentException("pulseLength longer then data length, but data length shorter then memory length");
    }
    
    // create data in a temporary buffer
    int len = data.size();
    char* buffer = new char[WidthBytes * len];
    memset(buffer, 0, WidthBytes * len);  		
    try {
        for (int i = 0, idx = 0; i < len; i++, idx += WidthBytes) {
        	BigInteger& val = data[i];
        	if (val.size() != 0) {
            	memcpy(buffer + idx, val.buffer(), val.size());
        	}
        }
        Owner.writeArea(AREA_MEM_PULSE, (void*)buffer, 0, len);
        uint64_t pulseLengthVal = pulseLength - 1;
        Owner.writeWord(WORD_PULSER_LENGTH, 0, &pulseLengthVal);
        Owner.writeBits(BITS_REPEAT_ENA, repeat ? 1 : 0);
        writeTarget(target);
    }
    catch (...) {
        delete [] buffer;
        throw;
    }
    delete [] buffer;
}

void TPulser::Start() {
    Owner.writeBits(BITS_PULSER_OUT_ENA, 1);	
}

void TPulser::Stop() {
    Owner.writeBits(BITS_PULSER_OUT_ENA, 0);
}


void TPulser::TInitPhaseObserver::BeforeInitPhaseChange(TDiagCtrl& diagCtrl,
        TDiagCtrl::TInitPhase currentPhase, TDiagCtrl::TInitPhase newPhase) {
    if ((currentPhase == TDiagCtrl::ipReqOffEnaOff)
            && (newPhase == TDiagCtrl::ipReqOnEnaOff)) {
        Pulser.Start();
    }
}

void TPulser::TInitPhaseObserver::AfterInitPhaseChange(TDiagCtrl& diagCtrl,
        TDiagCtrl::TInitPhase previousPhase, TDiagCtrl::TInitPhase currentPhase) {
	if ((previousPhase == TDiagCtrl::ipReqOnEnaOff)
             && (currentPhase == TDiagCtrl::ipReqOffEnaOff)) {
		Pulser.Stop();
    }
}

}
