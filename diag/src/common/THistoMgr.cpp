
//
//  Michal Pietrusinski, Karol Bunkowski 2001
//  Warsaw University
//
#ifdef __BORLANDC__
#include "precompiled.h"
#pragma hdrstop
#endif

#include "rpct/diag/THistoMgr.h"
#include "rpct/diag/IHistoCtrl.h"

using namespace std;

namespace rpct {
    
    
void THistoMgrBase::RegisterObserver(IHistoObserver* observer)
{
  if (find(Observers.begin(), Observers.end(), observer) == Observers.end())
    Observers.push_back(observer);
}


void THistoMgrBase::UnregisterObserver(IHistoObserver* observer)
{
  TObserversVector::iterator iter = Observers.begin();
  for(; iter != Observers.end(); ++iter)
    if (*iter == observer) {
      Observers.erase(iter);
      return;
    }

  throw EObserverNotRegistered("THistoMgrBase::UnregisterObserver: observer not registered");
}


void THistoMgrBase::CallObservers()
{
  TObserversVector::iterator iobserver=Observers.begin();

  for(; iobserver!=Observers.end(); iobserver++)
   (*iobserver)->HistoChanged(this);
}

THistoMgr::THistoMgr(IHistoCtrl* histoCtr, bool skip_same)
: Data(NULL), PrevData(NULL), SkipSame(skip_same)
{
  HistoCtr=histoCtr;
  BinCount=HistoCtr->GetBinCount();
  Data = new uint32_t[BinCount];
  if (SkipSame)
    PrevData = new uint32_t[BinCount];
  HistoId=HistoCtr->GetId();
}

THistoMgrMask::THistoMgrMask(IHistoCtrl* histoCtr)
{
  HistoCtr=histoCtr;
  BinCount=HistoCtr->GetBinCount();

  BitsNum = 0;

  for (int i=31; i>=0; i--)
    if ((BinCount >> i) & 1)  {
      BitsNum = i + 1;
      break;
    };

  Data = new uint32_t[BinCount];
  MaskedData = new uint32_t[BinCount];
  
  Masks = new TItemMask[BitsNum];
  for(int i=0; i<BitsNum; i++)
    Masks[i] = imNone;

  MaskNone = 0;
  MaskSelect0 = 0;
  MaskSelect1 = 0;
  //MaskMask = 0;

  HistoId = std::string("Masked ") + HistoCtr->GetId();
}


    
void THistoMgrMask::SetMask(int bit_no, TItemMask mask)
{
  if (bit_no >= BitsNum)
    throw TException("THistoMgrMask::SetMask: bit_no out of range");

  Masks[bit_no] = mask;
  MaskNone = 0;
  MaskSelect0 = 0;
  MaskSelect1 = 0;
  //MaskMask = 0;

  for(int i=0; i<BitsNum; i++) {
    if (Masks[i] == imNone)
      MaskNone |= (1 << i);
    else
      if (Masks[i] == imSelect0)
        MaskSelect0 |= (1 << i);
      else
        if (Masks[i] == imSelect1)
          MaskSelect1 |= (1 << i);
        //else
        //  if (Masks[i] == imMask)
        //    MaskMask |= (1 << i);
  };

  PrepareMaskedData();
  CallObservers();
}

   
void THistoMgrMask::PrepareMaskedData()
{
  memset(MaskedData, 0, BinCount* sizeof (uint32_t));

  int newidx;
  for(int i=0; i<BinCount; i++) {

    if ((i & MaskSelect0) != 0)
      continue;

    if ((i & MaskSelect1) != MaskSelect1)
      continue;

    newidx = i & MaskNone;

    MaskedData[newidx] += Data[i];
  };
}
}
