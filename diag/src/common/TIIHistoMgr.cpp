#include "rpct/diag/TIIHistoMgr.h"
#include "rpct/diag/IIHisto.h"


namespace rpct {
    
    
TIIHistoMgr::TIIHistoMgr(TIIDevice& owner, const char* name, IDiagCtrl& diagCtrl, 
		IDevice::TID area_id, int areaStartIdx, int areaLength) 
: THistoMgr(new TIIHistoCtrl(owner, area_id, areaStartIdx, areaLength)),
DiagnosableOwner(dynamic_cast<IDiagnosable&>(owner)), Name(name), DiagCtrl(diagCtrl) {	
}

TIIHistoMgr::~TIIHistoMgr() {
	delete GetHistoCtrl();
}

}
