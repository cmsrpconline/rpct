//---------------------------------------------------------------------------
          
//
//  Michal Pietrusinski 2001
//  Warsaw University
//
#ifdef __BORLANDC__
#include "precompiled.h"
#pragma hdrstop
#endif

#include "rpct/diag/IIHisto.h"
#include <sstream>

//---------------------------------------------------------------------------


using namespace std;

namespace rpct {
                       
uint32_t* TIIHistoCtrl::NullData = NULL;
int TIIHistoCtrl::NullDataSize = 0;


TIIHistoCtrl::TIIHistoCtrl(TIIDevice& owner, IDevice::TID area_id,
    int areaStartIdx, int areaLength)
  : Owner(owner), AreaID(area_id), AreaStartIdx(areaStartIdx)
{
  IDevice::TItemDesc desc = owner.getItemDesc(area_id);
  if (desc.Type != IDevice::itArea)
    throw TException("TIIHistoCtrl: given id is not an area");

  CounterSize = desc.Width;
  if (CounterSize > (int)sizeof(uint32_t)*8)
    throw TException("TIIHistoCtrl: CounterSize bigger than 32");

  BinCount = (areaLength == IDevice::npos) ? (desc.Number - AreaStartIdx) : areaLength;

  if ((areaStartIdx != 0) || (areaLength != IDevice::npos)) {
    ostringstream ost;
    ost << desc.Name << ' ' << areaStartIdx << ".." << (areaStartIdx + BinCount - 1);
    Name = ost.str();
  }
  else {
    Name = desc.Name;
  }
}


void TIIHistoCtrl::Reset()
{
  if (NullData == NULL) {
    NullDataSize = BinCount;
    NullData = new uint32_t[NullDataSize];
    memset(NullData, 0, NullDataSize * sizeof(uint32_t));
  };

  if (NullDataSize < BinCount) {
    // allocate more null data
    delete [] NullData;
    NullData = NULL; 
    NullDataSize = BinCount;
    NullData = new uint32_t[NullDataSize];
    memset(NullData, 0, NullDataSize * sizeof(uint32_t));
  };


  Owner.writeArea(AreaID, NullData, AreaStartIdx, BinCount);
}   

void TIIHistoCtrlMask::ReadData(uint32_t* data)
{
  TIIHistoCtrl::ReadData(data);
  TMaskedBins::iterator iBin;
  for (iBin = MaskedBins.begin(); iBin != MaskedBins.end(); ++iBin)
    data[*iBin] = 0;
}

/*
void TIIHistoRunCtrl::StartCounting()
{
  if (CountingState == csCounting)
    return;

  uint32_t status = Owner.readVector(VECT_STATUS);

  Owner.setBitsInVector(BITS_STATUS_PROC_REQ, 0, status);
  Owner.setBitsInVector(BITS_STATUS_RATE_ENA, 0, status);
  Owner.setBitsInVector(BITS_STATUS_RATE_START, 0, status);
  Owner.writeVector(VECT_STATUS, status);

  Owner.setBitsInVector(BITS_STATUS_PROC_REQ, 1, status);
  Owner.writeVector(VECT_STATUS, status);

  int repeat = 0;
  while(Owner.readBits(BITS_STATUS_PROC_ACK) != 1)
    if (repeat++ > 1000)
      throw TException("StartCounting: Couldn't set BITS_STATUS_PROC_ACK to 1");


  Owner.setBitsInVector(BITS_STATUS_RATE_START, 1, status);
  Owner.writeVector(VECT_STATUS, status);
  if (Owner.readBits(BITS_STATUS_RATE_START) != 1 )
    throw TException("StartCounting: Couldn't set BITS_STATUS_RATE_START to 1");

  Owner.setBitsInVector(BITS_STATUS_RATE_ENA, 1, status);
  Owner.writeVector(VECT_STATUS, status);
  if (Owner.readBits(BITS_STATUS_RATE_ENA) != 1 )
    throw TException("StartCounting: Couldn't set BITS_STATUS_RATE_ENA to 1");

  CountingState = csCounting;
}

void TIIHistoRunCtrl::StopCounting()
{
  uint32_t status = Owner.readVector(VECT_STATUS);;

  Owner.setBitsInVector(BITS_STATUS_PROC_REQ, 0, status);
  Owner.setBitsInVector(BITS_STATUS_RATE_ENA, 0, status);
  Owner.setBitsInVector(BITS_STATUS_RATE_START, 0, status);
  Owner.writeVector(VECT_STATUS, status);

  int repeat = 0;
  while(Owner.readBits(BITS_STATUS_PROC_ACK) != 0)
    if (repeat++ > 1000)
      throw TException("StopCounting: Couldn't set BITS_STATUS_PROC_ACK to 0");

  CountingState = csReady;
}


void TIIHistoRunCtrl::PauseCounting()
{          
  if (CountingState == csPaused)
    return;

  uint32_t status = Owner.readVector(VECT_STATUS);

  Owner.setBitsInVector(BITS_STATUS_RATE_ENA, 0, status);
  Owner.writeVector(VECT_STATUS, status);

  if (Owner.readBits(BITS_STATUS_RATE_ENA) != 0 )
    throw TException("PauseCounting: Couldn't set BITS_STATUS_RATE_ENA to 0");

  Owner.setBitsInVector(BITS_STATUS_PROC_REQ, 0, status);
  Owner.writeVector(VECT_STATUS, status);

  int repeat = 0;
  while(Owner.readBits(BITS_STATUS_PROC_ACK) != 0)
    if (repeat++ > 1000)
      throw TException("PauseCounting: Couldn't set BITS_STATUS_PROC_ACK to 0");

  CountingState = csPaused;

}

void TIIHistoRunCtrl::ResumeCounting()
{     
  if (CountingState == csCounting)
    return;

  uint32_t status = Owner.readVector(VECT_STATUS);

  Owner.setBitsInVector(BITS_STATUS_PROC_REQ, 1, status);
  Owner.writeVector(VECT_STATUS, status);
  if (Owner.readBits(BITS_STATUS_PROC_REQ) != 1 )
    throw TException("ResumeCounting: Couldn't set BITS_STATUS_PROC_REQ to 1");

  int repeat = 0;
  while(Owner.readBits(BITS_STATUS_PROC_ACK) != 1)
    if (repeat++ > 1000)
      throw TException("ResumeCounting: Couldn't set BITS_STATUS_PROC_ACK to 1");

  Owner.setBitsInVector(BITS_STATUS_RATE_ENA, 1, status);
  Owner.writeVector(VECT_STATUS, status);
  if (Owner.readBits(BITS_STATUS_RATE_ENA) != 1 )
    throw TException("ResumeCounting: Couldn't set BITS_STATUS_RATE_ENA to 1");

  CountingState = csCounting;
}*/

}
