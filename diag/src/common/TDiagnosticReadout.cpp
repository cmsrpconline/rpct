//---------------------------------------------------------------------------
#ifdef __BORLANDC__
#include "precompiled.h"
#pragma hdrstop
#endif

#include "rpct/diag/TDiagnosticReadout.h"
#include "rpct/diag/EInvalidFormat.h"
#include "rpct/std/bitcpy.h"
#include <assert.h>

//---------------------------------------------------------------------------


using namespace std;
using namespace log4cplus;

namespace rpct {
Logger TDiagnosticReadout::logger = Logger::getInstance("TDiagnosticReadout");
void TDiagnosticReadout::setupLogger(std::string namePrefix) {
	logger = Logger::getInstance(namePrefix + "." + logger.getName());
}

TDiagnosticReadout::TDiagnosticReadout(
    IDiagnosable& owner, //TIIDevice& owner,
    const char* name,
    ConvertedBxDataFactory* convertedBxDataFactory,
    IDiagCtrl& diagCtrl,
    TID vectDiagDaq,
    TID bitsDiagDaqEmpty,
    TID bitsDiagDaqEmptyAck,
    TID bitsDiagDaqLost,
    TID bitsDiagDaqLostAck,
    TID vectDiagDaqWr,
    TID bitsDiagDaqWrAddr,
    TID bitsDiagDaqWrAck,
    TID vectDiagDaqRd,
    TID bitsDiagDaqRdAddr,
    TID bitsDiagDaqRdAck,
    TID wordDiagDaqMask,
    TID wordDataDaqDelay,
    TID areaDiagDaq,
    int dataWidth,
    int trgNum,
    int timerWidth)
        : Owner(owner), Name(name), convertedBxDataFactory_(convertedBxDataFactory), DiagCtrl(diagCtrl),
        VECT_DIAG_DAQ(vectDiagDaq),
        BITS_DIAG_DAQ_EMPTY(bitsDiagDaqEmpty),
        BITS_DIAG_DAQ_EMPTY_ACK(bitsDiagDaqEmptyAck),
        BITS_DIAG_DAQ_LOST(bitsDiagDaqLost),
        BITS_DIAG_DAQ_LOST_ACK(bitsDiagDaqLostAck),
        VECT_DIAG_DAQ_WR(vectDiagDaqWr),
        BITS_DIAG_DAQ_WR_ADDR(bitsDiagDaqWrAddr),
        BITS_DIAG_DAQ_WR_ACK(bitsDiagDaqWrAck),
        VECT_DIAG_DAQ_RD(vectDiagDaqRd),
        BITS_DIAG_DAQ_RD_ADDR(bitsDiagDaqRdAddr),
        BITS_DIAG_DAQ_RD_ACK(bitsDiagDaqRdAck),
        WORD_DIAG_DAQ_MASK(wordDiagDaqMask),
        WORD_DATA_DAQ_DELAY(wordDataDaqDelay),
        AREA_MEM_DIAG_DAQ(areaDiagDaq),
        DataWidth(dataWidth), TrgNum(trgNum), TimerWidth(timerWidth), events_(0),
InitPhaseObserver(0) {
    /*DiagnosableOwner = dynamic_cast<IDiagnosable*>(&Owner);
    if (DiagnosableOwner == NULL) {
    	throw TException("Owner does not implement IDiagnosable interface");
    }*/
    IDevice::TItemDesc desc = Owner.getItemDesc(AREA_MEM_DIAG_DAQ);
    if (desc.Type != IDevice::itArea) {
        throw TException("TDiagnosticReadout: given id is not an area");
    }

    AreaWidth = desc.Width;
    //if (AreaWidth > sizeof(uint32_t)*8)
    //  throw TException("TDiagnosticReadout: AreaWidth bigger than 32");

    AreaLen = desc.Number;

    desc = Owner.getItemDesc(WORD_DIAG_DAQ_MASK);
    MaskWidth = desc.Width;
    MaskNum = desc.Number;
    //Name = desc.Name;

    Masks.resize(MaskNum);

    //int width = (readout.GetAreaWidth() - 1) / 8 + 1;
    //int size = width * readout.GetAreaLen();
    AreaWidthBytes = (AreaWidth - 1) / 8 + 1;
    data_.setData(new unsigned char[AreaLen * AreaWidthBytes]);
    HeaderPositions = SLVPartNum(TimerWidth, DataWidth);

    TimerWidthBytes = (TimerWidth - 1) / 8 + 1;
    DataWidthBytes = (DataWidth - 1) / 8 + 1;

    TDiagCtrl* dc = dynamic_cast<TDiagCtrl*>(&DiagCtrl);
    if (dc != NULL) {
        InitPhaseObserver = new TInitPhaseObserver(*this);
        dc->RegisterInitPhaseObserver(*InitPhaseObserver);
    }
}


TDiagnosticReadout::~TDiagnosticReadout() {
    delete [] (unsigned char*) data_.getData();
    delete InitPhaseObserver;
    //delete convertedBxDataFactory_;
}


void TDiagnosticReadout::ReadEmptyLost() {
    //G_Log.out() << "TDiagnosticReadout::ReadDiagDaq:" << flush;
    uint32_t value;
    int rep = 0;
    do {
        value = Owner.readVector(VECT_DIAG_DAQ);
        if (rep++ > AckRep)
            throw TException("TDiagnosticReadout::ReadDiagDaq: no ack");
    } while ((Owner.getBitsInVector(BITS_DIAG_DAQ_EMPTY_ACK, value) == 0)
             ||(Owner.getBitsInVector(BITS_DIAG_DAQ_LOST_ACK, value) == 0));

    data_.setEmpty(Owner.getBitsInVector(BITS_DIAG_DAQ_EMPTY, value));
    data_.setLost(Owner.getBitsInVector(BITS_DIAG_DAQ_LOST, value));

    //G_Log.out() << " empty = " << (empty ? "true" : "false")
    //            << ", lost = " << (lost ? "true" : "false") << endl;
}


uint32_t TDiagnosticReadout::ReadDiagDaqWrAddr() {
    uint32_t vect_last_addr;
    int rep = 0;
    do {
        vect_last_addr = Owner.readVector(VECT_DIAG_DAQ_WR);
        if (rep++ > AckRep)
            throw TException("TDiagnosticReadout::ReadDiagDaqWrAddr: no ack");
    } while (Owner.getBitsInVector(BITS_DIAG_DAQ_WR_ACK, vect_last_addr) == 0);

    return Owner.getBitsInVector(BITS_DIAG_DAQ_WR_ADDR, vect_last_addr);
}



void TDiagnosticReadout::WriteDiagDaqRdAddr(uint32_t value) {
    for (int retry = 0; retry < 3; retry++)  {
        try {
            uint32_t vect_free_addr = 0;
            Owner.setBitsInVector(BITS_DIAG_DAQ_RD_ADDR, value, vect_free_addr);

            Owner.writeVector(VECT_DIAG_DAQ_RD, vect_free_addr);

            // check if was succesfully writen
            int rep = 0;
            do {
                vect_free_addr = Owner.readVector(VECT_DIAG_DAQ_RD);
                if (rep++ > AckRep)
                    throw TException("TDiagnosticReadout::WriteDiagDaqRdAddr: no ack");
            } while (Owner.getBitsInVector(BITS_DIAG_DAQ_RD_ACK, vect_free_addr) == 0);

            if (Owner.getBitsInVector(BITS_DIAG_DAQ_RD_ADDR, vect_free_addr) != value) {
                ostringstream ostr;
                ostr << Owner.getDescription() << " " << Owner.getId() << " TDiagnosticReadout::WriteDiagDaqRdAddr: writen and read values differ";
                throw TException(ostr.str());
            }    
            return;
        }
        catch(...)  {
            if (retry == 2)
                throw;
        }
    }
}

uint32_t TDiagnosticReadout::ReadDiagDaqRdAddr() {
    uint32_t vect_last_addr;
    int rep = 0;
    do {
        vect_last_addr = Owner.readVector(VECT_DIAG_DAQ_RD);
        if (rep++ > AckRep)
            throw TException("TDiagnosticReadout::ReadDiagDaqRdAddr: no ack");
    } while (Owner.getBitsInVector(BITS_DIAG_DAQ_RD_ACK, vect_last_addr) == 0);


    //G_Log.out() << "TDiagnosticReadout::ReadDiagDaqRdAddr: BITS_DIAG_DAQ_RD_ADDR = "
    //            << Owner.getBitsInVector(BITS_DIAG_DAQ_RD_ADDR, vect_last_addr) << endl;

    return Owner.getBitsInVector(BITS_DIAG_DAQ_RD_ADDR, vect_last_addr);
}


/*
void TDiagnosticReadout::WriteDiagDaqWrAddr(uint32_t value)
{
  uint32_t vect_free_addr;
  Owner.setBitsInVector(BITS_DIAG_DAQ_WR_ADDR, value, vect_free_addr);

  Owner.writeVector(VECT_DIAG_DAQ_WR, vect_free_addr);

  // check if was succesfully writen
  int rep = 0;
  do {
    vect_free_addr = Owner.readVector(VECT_DIAG_DAQ_WR);
    if (rep++ > AckRep)
      throw TException("TDiagnosticReadout::WriteDiagDaqWrAddr: no ack");
  } while (Owner.getBitsInVector(BITS_DIAG_DAQ_WR_ACK, vect_free_addr) == 0);


  //G_Log.out() << "TDiagnosticReadout::WriteDiagDaqWrAddr: value = " << value << endl;

  if (Owner.getBitsInVector(BITS_DIAG_DAQ_WR_ADDR, vect_free_addr) != value)
    throw TException("TDiagnosticReadout::WriteDiagDaqWrAddr: writen and read values differ");
}   */

/*
int TDiagnosticReadout::ReadNewData(uint32_t* data, bool& lost)
{
  bool empty;
  ReadDiagDaq(empty, lost);

  if (empty)
    return 0;
  else {
    uint32_t prev_addr = WrAddr;
    WrAddr = ReadDiagDaqWrAddr();

    if (prev_addr < WrAddr) {   
      Owner.readArea(AREA_MEM_DIAG_DAQ, data, prev_addr, WrAddr - prev_addr);
      WriteDiagDaqRdAddr(WrAddr);
      return WrAddr - prev_addr;
    }
    else {
      Owner.readArea(AREA_MEM_DIAG_DAQ, data, prev_addr);
      Owner.readArea(AREA_MEM_DIAG_DAQ, data + AreaLen - prev_addr, 0, WrAddr);
      WriteDiagDaqRdAddr(WrAddr);
      return AreaLen - (prev_addr - WrAddr);
    };
  };
} */

bool TDiagnosticReadout::CheckForNewData() {
    ReadEmptyLost();
    LOG4CPLUS_DEBUG(logger, __FUNCTION__<<" "<<Name<<" "<< (data_.isEmpty() ? "empty" : "not empty") );

    return !data_.isEmpty();
}

/*
int TDiagnosticReadout::ReadNewData(void* data, bool& lost)
{
  bool empty;
  ReadDiagDaq(empty, lost);

  if (empty)
    return 0;
  else {

    uint32_t start_addr = (WrAddr + 1) % AreaLen;

    uint32_t prev_addr = WrAddr;
    WrAddr = ReadDiagDaqWrAddr();

    if (start_addr <= WrAddr) {
      Owner.readArea(AREA_MEM_DIAG_DAQ, data, start_addr, WrAddr - start_addr + 1);
      WriteDiagDaqRdAddr(WrAddr);
      return WrAddr - start_addr + 1;
    }
    else {
      int width = (AreaWidth - 1) / 8 + 1;
      Owner.readArea(AREA_MEM_DIAG_DAQ, data, start_addr);
      Owner.readArea(AREA_MEM_DIAG_DAQ,
                     (void*)(((char*)data) + (AreaLen - start_addr) * width),
                     0, WrAddr+1);
      WriteDiagDaqRdAddr(WrAddr);
      return AreaLen - (start_addr - WrAddr) + 1;
    };
  };
} */

void TDiagnosticReadout::ReadNewData() {
    LOG4CPLUS_DEBUG(logger, __FUNCTION__<<" started "<<Name<<" "<< (data_.isEmpty() ? "empty" : "not empty") );

    delete events_;
    events_ = NULL;

    if (data_.isEmpty())
        return;

    uint32_t start_addr = (WrAddr + 1) % AreaLen;

    data_.setPrevWrAddr(WrAddr);

    // uint32_t prev_addr = WrAddr;
    WrAddr = ReadDiagDaqWrAddr();

    data_.setWrAddr(WrAddr);

    if (start_addr <= WrAddr) {
        Owner.readArea(AREA_MEM_DIAG_DAQ, data_.getData(), start_addr, WrAddr - start_addr + 1);
        WriteDiagDaqRdAddr(WrAddr);
        data_.setLength(WrAddr - start_addr + 1);
    }
    else {
        //int width = (AreaWidth - 1) / 8 + 1;
        Owner.readArea(AREA_MEM_DIAG_DAQ, data_.getData(), start_addr);
        Owner.readArea(AREA_MEM_DIAG_DAQ,
                       (void*)(((unsigned char*)data_.getData()) + (AreaLen - start_addr) * AreaWidthBytes),
                       0, WrAddr+1);
        WriteDiagDaqRdAddr(WrAddr);
        data_.setLength(AreaLen - (start_addr - WrAddr) + 1);
    };
    LOG4CPLUS_DEBUG(logger, __FUNCTION__<<" finished");
}

void TDiagnosticReadout::Reset() {

    //int width = (AreaWidth - 1) / 8 + 1;
    int size = AreaWidthBytes * AreaLen;
    unsigned char* buf = new unsigned char[size];
    memset(buf, 0, size);
    Owner.writeArea(AREA_MEM_DIAG_DAQ, buf);
    delete [] buf;
    Init();
}

void TDiagnosticReadout::Init() {
    //WrAddr = (ReadDiagDaqWrAddr() + AreaLen - 1) % AreaLen;
    WrAddr = (0 + AreaLen - 1) % AreaLen;
    WriteDiagDaqRdAddr(WrAddr);
}

TDiagnosticReadout::TMask& TDiagnosticReadout::GetMask(int index) {
    return Masks.at(index);
}

void TDiagnosticReadout::SetMask0(int bxNum) {
    if (bxNum == -1) {
        bxNum = GetMaskWidth();
    }

    TMask& mask = GetMask(0);
    mask.clear();
    for (int i = 1; i <= bxNum; i++) {
        mask.insert(i);
    }
}


void TDiagnosticReadout::WriteMasks() {
    if (MaskWidth > 32)
        throw TException("TDiagnosticReadout::WriteMasks: MaskWidth > 32");

    for (int i = 0; i < MaskNum; i++) {
        TMask& mask = Masks.at(i);

        uint32_t value = 0;

        for (TMask::iterator iter = mask.begin(); iter != mask.end(); ++iter) {
            value |= (1 << (*iter - 1));
        }        

        Owner.writeWord(WORD_DIAG_DAQ_MASK, i, value);
    }
}

void TDiagnosticReadout::Configure(TMasks& masks, unsigned int daqDataDelay) {
    if (masks.size() > (TMasks::size_type) GetMaskNum()) {
        throw TException("TDiagnosticReadout::Configure: masks too long");
    }
    for (TMasks::size_type i = 0; i < (TMasks::size_type) Masks.size(); i++) {
        if (i < masks.size()) {
            Masks[i] = masks[i];
        }
        else {
            Masks[i].clear();
        }
    }
    

   /* cout << "**** Configure masks size" << masks.size() << endl; 
    for (size_t m = 0; m < masks.size(); m++) {
        IDiagnosticReadout::TMask& mask = masks[m];
        cout << "mask" << m << ": " << flush; 
        for (IDiagnosticReadout::TMask::iterator iter = mask.begin(); iter != mask.end(); ++iter) {
            cout << *iter; 
        }
        cout << endl;
    }
    cout << "**** Configure Masks size" << Masks.size() << endl; 
    for (size_t m = 0; m < Masks.size(); m++) {
        IDiagnosticReadout::TMask& mask = Masks[m];
        cout << "Masks" << m << ": " << flush; 
        for (IDiagnosticReadout::TMask::iterator iter = mask.begin(); iter != mask.end(); ++iter) {
            cout << *iter; 
        }
        cout << endl;
    }*/
    
    Configure(daqDataDelay);
}


void TDiagnosticReadout::Configure(unsigned int daqDataDelay) {
    WriteMasks();
    if (WORD_DATA_DAQ_DELAY == 0 && daqDataDelay != 0) {
        throw TException("TDiagnosticReadout::Configure: trying to set daqDelay, but the register is not specified");
    }
    if (WORD_DATA_DAQ_DELAY != 0) {
        Owner.writeWord(WORD_DATA_DAQ_DELAY, 0, daqDataDelay);
    }
}

void TDiagnosticReadout::RegisterObserver(IDiagnosticReadoutObserver* observer) {
    TObserversVector::iterator iter = Observers.begin();
    for(; iter != Observers.end(); ++iter)
        if (*iter == observer)
            return;

    Observers.push_back(observer);
}


void TDiagnosticReadout::UnregisterObserver(IDiagnosticReadoutObserver* observer) {
    TObserversVector::iterator iter = Observers.begin();
    for(; iter != Observers.end(); ++iter)
        if (*iter == observer) {
            Observers.erase(iter);
            return;
        }

    throw TException("TDiagnosticReadout::UnregisterObserver: observer not registered");
}

/*void TDiagnosticReadout::UnregisterAllObservers()
{
  Observers.clear();
} */


void TDiagnosticReadout::CallObservers() {
    TObserversVector::iterator iobserver=Observers.begin();

    for(; iobserver!=Observers.end(); iobserver++)
        (*iobserver)->ReadoutChanged(this);
}


uint32_t TDiagnosticReadout::GetTriggers(void* evData) {
    uint32_t result = 0;
    bitcpy(&result, 0, evData, DataWidth, TrgNum);
    return result;
}

uint32_t TDiagnosticReadout::GetPos(void* evData) {
    uint32_t result = 0;
    bitcpy(&result, 0, evData, DataWidth + TrgNum, AreaWidth - DataWidth - TrgNum);
    return result;
}


void TDiagnosticReadout::GetTimer(void* evData, void* dest, int headerPos) {
    if (headerPos == 0)
        memset(dest, 0, TimerWidthBytes);
    int cnt = TimerWidth - headerPos * DataWidth;
    if (cnt > DataWidth)
        cnt = DataWidth;
    if (cnt < 0) {
        ostringstream ostr;
        ostr << "TDiagnosticReadout::GetTimer: cnt = (dec)" << dec << cnt
        << " TimerWidth = " << TimerWidth
        << " headerPos = " << headerPos
        << " DataWidth = " << DataWidth;
        throw EInvalidFormat(ostr.str());
    }
    bitcpy(dest, headerPos * DataWidth, evData, 0, cnt);
}

void TDiagnosticReadout::GetData(void* evData, void* dest) {
    memset(dest, 0, DataWidthBytes);
    bitcpy(dest, 0, evData, 0, DataWidth);
}

TDiagnosticReadout::Events* TDiagnosticReadout::InterpreteData(Data& data) {
    Events* events = new Events(*this, TimerWidthBytes, DataWidthBytes, data.isLost(), convertedBxDataFactory_);
    if (data.isEmpty()) {
        return events;
    }

    try {
        //TEvents* events = new TEvents(TimerWidthBytes, DataWidthBytes, data.IsLost());
        uint32_t pos;
        uint32_t triggers;
        int index = 0;

        unsigned char* pdata = (unsigned char*)data.getData();

        int expectedHeaderPos = 0;
        bool header;
        for (int i = 0; i < data.getLength(); i++) {
            pos = GetPos(pdata);
            triggers = GetTriggers(pdata);

            if ((pos + triggers) == 0) {
                // This may be the header except of the case, when the counter overlapped due
                // to the repeated trigger before that event. If 'pos' in the next row of data
                // is 0, it must be indeed the header.
                if (expectedHeaderPos > 0)
                    header = true;
                else if ((i + 1) >= data.getLength())
                    header = false;
                else if (GetPos(pdata + AreaWidthBytes) == 0)
                    header = true;
                else
                    header = false;
            }
            else
                header = false;

            if (header) {
                if (expectedHeaderPos == 0) {
                    events->addEvent();
                    index = 0;
                }
                GetTimer(pdata, events->getEventList().back()->getTimer(), expectedHeaderPos);
                expectedHeaderPos++;
            }
            else {
                if (events->getEventList().size() == 0)
                    throw EInvalidFormat("TDiagnosticReadout::InterpreteData: data format error - no header");
                expectedHeaderPos = 0;
                Event* event = events->getEventList().back();
                BxData* bxdata = event->addBxData();
                bxdata->setTriggers(triggers);
                bxdata->setPos(pos);
                bxdata->setIndex(index);
                index++;
                GetData(pdata, bxdata->getData());
                if (event->getBxDataList().size() == 1) {
                    if (bxdata->getPos() != 0)
                        throw EInvalidFormat("TDiagnosticReadout::InterpreteData: data format error - no zero position data");

                    if (bxdata->getTriggers() == 0)
                        throw EInvalidFormat("TDiagnosticReadout::InterpreteData: data format error - no trigger at zero position data");
                }
            }
            pdata += AreaWidthBytes;
        };
        return events;
    }
    catch(EInvalidFormat& e) {
        ostringstream ostr;
        ostr << e.what();

        int width = (GetAreaWidth() - 1) / 8 + 1;
        unsigned char* pdata = (unsigned char*)data.getData();

        ostr << "\nReadout: size = " << data.getLength()
        << ", lost = " << (data.isLost() ? "true" : "false");

        for (int i = 0; i < data.getLength(); i++) {
            ostr << '\n' << dataToHex(pdata, GetAreaWidth());
            pdata += width;
        };

        ofstream fout("tbcc.dump", ios::app | ios::out);
        fout << ostr.str() << endl;
        G_Log.out() << "!!! " << ostr.str() << endl;

        //throw;
    }
    return events;
}

IDiagnosticReadout::Events& TDiagnosticReadout::GetLastEvents() {
    if (events_ == 0) {
        events_ = InterpreteData(data_);
    }
    return *events_;
}

void TDiagnosticReadout::TInitPhaseObserver::BeforeInitPhaseChange(TDiagCtrl& diagCtrl,
        TDiagCtrl::TInitPhase currentPhase, TDiagCtrl::TInitPhase newPhase) {
    if ((currentPhase == TDiagCtrl::ipReqOffEnaOff)
            && (newPhase == TDiagCtrl::ipReqOnEnaOff)) {
        //Readout.Reset();
        Readout.Init();
    }
}

void TDiagnosticReadout::TInitPhaseObserver::AfterInitPhaseChange(TDiagCtrl& diagCtrl,
        TDiagCtrl::TInitPhase previousPhase, TDiagCtrl::TInitPhase currentPhase) {
    if ((previousPhase == TDiagCtrl::ipReqOnEnaOn)
            && (currentPhase == TDiagCtrl::ipReqOnEnaOff)) {
        Readout.CheckForNewData();
    }
    else if ((previousPhase == TDiagCtrl::ipReqOnEnaOff)
             && (currentPhase == TDiagCtrl::ipReqOffEnaOff)) {
        Readout.Refresh();
    }
}

}
