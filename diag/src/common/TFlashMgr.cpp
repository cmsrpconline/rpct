//---------------------------------------------------------------------------
#ifdef __BORLANDC__
#include "precompiled.h"
#pragma hdrstop
#endif

#include "rpct/diag/TFlashMgr.h"

//---------------------------------------------------------------------------

namespace rpct {

           
void TFlashMgr::RegisterObserver(IFlashObserver* observer)
{
  Observers.push_back(observer);
}


void TFlashMgr::UnregisterObserver(IFlashObserver* observer)
{
  TObserversVector::iterator iter = Observers.begin();
  for(; iter != Observers.end(); ++iter)
    if (*iter == observer) {
      Observers.erase(iter);
      return;
    }

  throw TException("TFlashMgr::UnregisterObserver: observer not registered");
}


void TFlashMgr::CallObservers()
{
  TObserversVector::iterator iobserver=Observers.begin();

  for(; iobserver!=Observers.end(); iobserver++)
   (*iobserver)->FlashChanged(this);
}

}
