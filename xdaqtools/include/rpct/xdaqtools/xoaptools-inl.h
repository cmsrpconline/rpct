/** Filip Thyssen */

#ifndef rpct_xdaqtools_xoaptools_inl_h_
#define rpct_xdaqtools_xoaptools_inl_h_

#include "rpct/xdaqtools/xoaptools.h"

namespace rpct {
namespace xdaqtools {
namespace xoaptools {

template<typename T>
inline xoap::Method<T> * bind(toolbox::lang::Class & _class
                              , T & obj
                              , xoap::MessageReference (T::*func)(xoap::MessageReference) throw (xoap::exception::Exception)
                              , const std::string & command
                              , const std::string & namespaceURI)
{
    xoap::Method<T> * f = new xoap::Method<T>;
    f->obj_ = &obj;
    f->func_ = func;
    f->namespaceURI_ = namespaceURI;
    f->name_ = command;
    std::string name = namespaceURI + ':' + command;
    _class.removeMethod(name); // calls delete if necessary
    _class.addMethod(f, name);
    return f;
}

inline void unbind(toolbox::lang::Class & _class
                   , const std::string & command)
{
    _class.removeMethod(command); // calls delete if necessary
}

} // namespace xoaptools
} // namespace xdaqtools
} // namespace rpct

#endif // rpct_xdaqtools_xoaptools_inl_h_
