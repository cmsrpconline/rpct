/** Filip Thyssen */

#ifndef rpct_xdaqtools_xoaptools_h_
#define rpct_xdaqtools_xoaptools_h_

#include "xdaq/NamespaceURI.h"
#include "toolbox/lang/Class.h"
#include "xoap/Method.h"

namespace rpct {
namespace xdaqtools {
namespace xoaptools {

/** bind a function of an object of type T to a xoap namespaceURI:command for _class */
template<typename T>
xoap::Method<T> * bind(toolbox::lang::Class & _class
                       , T & obj
                       , xoap::MessageReference (T::*func)(xoap::MessageReference) throw (xoap::exception::Exception)
                       , const std::string & command
                       , const std::string & namespaceURI = std::string(XDAQ_NS_URI));

void unbind(toolbox::lang::Class & _class
            , const std::string & command);

} // namespace xoaptools
} // namespace xdaqtools
} // namespace rpct

#include "rpct/xdaqtools/xoaptools-inl.h"

#endif // rpct_xdaqtools_xoaptools_h_
