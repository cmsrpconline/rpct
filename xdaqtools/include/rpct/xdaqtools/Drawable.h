/** Filip Thyssen */

#ifndef rpct_xdaqtools_Drawable_h_
#define rpct_xdaqtools_Drawable_h_

#include "xgi/Output.h"
#include "rpct/tools/Drawable.h"

namespace rpct {
namespace xdaqtools {

class Drawable : public virtual rpct::tools::Drawable
{
public:
    void printSVGDocument(xgi::Output & os, int depth = 0) const;
};

} // namespace xdaqtools
} // namespace rpct

#endif // rpct_xdaqtools_Drawable_h_
