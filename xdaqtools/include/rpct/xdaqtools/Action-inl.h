/** Filip Thyssen */

#ifndef rpct_xdaqtools_Action_inl_h_
#define rpct_xdaqtools_Action_inl_h_

#include "rpct/xdaqtools/Action.h"

namespace rpct {
namespace xdaqtools {

inline ActionSignature::~ActionSignature()
{}

inline std::string ActionSignature::type()
{
    return "ActionSignature";
}
inline int ActionSignature::operator()() const
{
    return invoke();
}
inline std::string ActionSignature::name() const
{
    return "undefinedActionSignature";
}

template<typename Listener>
Action<Listener>::Action(Listener & listener
                         , int (Listener::*func)()
                         , const std::string & name)
    : listener_(listener)
    , func_(func)
    , name_(name)
{}

template<typename Listener>
inline Action<Listener>::~Action()
{}

template<typename Listener>
inline std::string Action<Listener>::type()
{
    return "Action";
}
template<typename Listener>
inline int Action<Listener>::invoke() const
{
    if (func_)
        return (listener_.*func_)();
    else
        return -1;
}
template<typename Listener>
inline std::string Action<Listener>::name() const
{
    return name_;
}

template <typename Listener>
inline Action<Listener> * bind(toolbox::lang::Class & _class
                               , Listener & listener
                               , int (Listener::*func)()
                               , const std::string & name)
{
    Action<Listener> * f = new Action<Listener>(listener, func, name);
    _class.removeMethod(name); // calls delete if necessary
    _class.addMethod(f, name);
    return f;
}

template<typename T>
inline ObjectActionSignature<T>::~ObjectActionSignature()
{}
template<typename T>
inline std::string ObjectActionSignature<T>::type()
{
    return "ObjectActionSignature";
}
template<typename T>
inline int ObjectActionSignature<T>::operator()(T obj) const
{
    return invoke(obj);
}
template<typename T>
inline std::string ObjectActionSignature<T>::name() const
{
    return "undefinedObjectActionSignature";
}

template<typename T, typename Listener>
inline ObjectAction<T, Listener>::ObjectAction(Listener & listener
                                               , int (Listener::*func)(T)
                                               , const std::string & name)
    : listener_(listener)
    , func_(func)
    , name_(name)
{}
template<typename T, typename Listener>
inline ObjectAction<T, Listener>::~ObjectAction()
{}

template<typename T, typename Listener>
inline std::string ObjectAction<T, Listener>::type()
{
    return "ObjectAction";
}
template<typename T, typename Listener>
inline int ObjectAction<T, Listener>::invoke(T obj) const
{
    if (func_)
        return (listener_.*func_)(obj);
    else
        return -1;
}
template<typename T, typename Listener>
inline std::string ObjectAction<T, Listener>::name() const
{
    return name_;
}

template <typename T, typename Listener>
inline ObjectAction<T, Listener> * bind(toolbox::lang::Class & _class
                                        , Listener & listener
                                        , int (Listener::*func)(T)
                                        , const std::string & name)
{
    ObjectAction<T, Listener> * f = new ObjectAction<T, Listener>(listener, func, name);
    _class.removeMethod(name); // calls delete if necessary
    _class.addMethod(f, name);
    return f;
}

} // namespace xdaqtools
} // namespace rpct

#endif // rpct_xdaqtools_Action_inl_h_
