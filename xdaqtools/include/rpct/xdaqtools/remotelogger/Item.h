/** Filip Thyssen */

#ifndef rpct_xdaqtools_remotelogger_Item_h_
#define rpct_xdaqtools_remotelogger_Item_h_

#include <ostream>

#include "log4cplus/spi/loggingevent.h"
#include "log4cplus/loglevel.h"

#include "xdata/Integer.h"
#include "xdata/String.h"

namespace xdata {
class AbstractBag;
} // namespace xdata

namespace rpct {
namespace xdaqtools {
namespace remotelogger {

class ItemList;

class Item
{
    friend class ItemList;
public:
    Item(log4cplus::LogLevel loglevel = log4cplus::NOT_SET_LOG_LEVEL
         , const std::string & message = std::string("")
         , const std::string & file = std::string("")
         , int line = 0);
    Item(log4cplus::spi::InternalLoggingEvent const & event);

    void registerFields(xdata::AbstractBag * bag);

    log4cplus::spi::InternalLoggingEvent getLogEvent() const;
    void setLogEvent(log4cplus::spi::InternalLoggingEvent const & event);

    log4cplus::LogLevel getLogLevel() const;
    void setLogLevel(log4cplus::LogLevel loglevel);

    const std::string & getMessage() const;
    void setMessage(const std::string & message);

    const std::string & getFile() const;
    void setFile(const std::string & file);

    int getLine() const;
    void setLine(int line);

protected:
    xdata::Integer loglevel_;
    xdata::String message_, file_;
    xdata::Integer line_;
};

std::ostream & operator <<(std::ostream & os, const Item & item);

} // namespace remotelogger
} // namespace xdaqtools
} // namespace rpct

#include "rpct/xdaqtools/remotelogger/Item-inl.h"

#endif // rpct_xdaqtools_remotelogger_Item_h_
