/** Filip Thyssen */

#ifndef rpct_xdaqtools_remotelogger_Service_h_
#define rpct_xdaqtools_remotelogger_Service_h_

#include <log4cplus/logger.h>

#include "xoap/MessageReference.h"
#include "xoap/exception/Exception.h"

#include "rpct/tools/Action.h"
#include "rpct/xdaqtools/QueueWorkLoop.h"
#include "rpct/xdaqtools/remotelogger/Item.h"
#include "rpct/xdaqtools/remotelogger/ItemList.h"

namespace xdaq {
class Application;
} // namespace xdaq

namespace rpct {
namespace xdaqtools {
namespace remotelogger {

class Service
{
public:
    Service(xdaq::Application & application
            , log4cplus::Logger const & logger);
    ~Service();

    xoap::MessageReference receive(xoap::MessageReference msg)
        throw (xoap::exception::Exception);
private:
    log4cplus::Logger logger_;
protected:
    int list_action(ItemList & itemlist);

    xdaq::Application & application_;

    rpct::tools::ObjectAction<ItemList &, Service> * list_action_;
    rpct::xdaqtools::QueueWorkLoop<ItemList> * qwl_;
};

} // namespace remotelogger
} // namespace xdaqtools
} // namespace rpct

#endif // rpct_xdaqtools_remotelogger_Service_h_
