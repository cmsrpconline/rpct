/** Filip Thyssen */

#ifndef rpct_xdaqtools_remotelogger_ItemList_inl_h_
#define rpct_xdaqtools_remotelogger_ItemList_inl_h_

#include "rpct/xdaqtools/remotelogger/ItemList.h"

namespace rpct {
namespace xdaqtools {
namespace remotelogger {

inline ItemList::const_iterator & ItemList::const_iterator::operator++()
{
    ++n_;
    change_ = true;

    size_t size = il_.size();
    if (n_ > size)
        n_ = size;
    return *this;
}

inline ItemList::const_iterator ItemList::const_iterator::operator++(int)
{
    const_iterator it(*this);
    operator++();
    return it;
}

inline ItemList::const_iterator & ItemList::const_iterator::operator--()
{
    if (n_ > 0)
        --n_;
    change_ = true;

    size_t size = il_.size();
    if (n_ > size)
        n_ = size;
    return *this;
}

inline ItemList::const_iterator ItemList::const_iterator::operator--(int)
{
    const_iterator it(*this);
    operator--();
    return it;
}

inline bool ItemList::const_iterator::operator==(const_iterator const & it)
{
    size_t size = il_.size();
    return (&il_ == &(it.il_) && (n_ == it.n_ || (n_ >= size && it.n_ >= size)));
}

inline bool ItemList::const_iterator::operator!=(const_iterator const & it)
{
    return !(operator==(it));
}

inline size_t ItemList::size() const
{
    return loglevel_.size();
}

inline size_t ItemList::max_size() const
{
    size_t _max_size = loglevel_.max_size();
    size_t _size = message_.max_size();
    if (_size < _max_size)
        _max_size = _size;
    if ((_size = file_.max_size()) < _max_size)
        _max_size = _size;
    if ((_size = line_.max_size()) < _max_size)
        _max_size = _size;
    return _max_size;
}

inline ItemList::const_iterator ItemList::begin() const
{
    return const_iterator(*this, 0);
}

inline ItemList::const_iterator ItemList::end() const
{
    return const_iterator(*this, this->size());
}

inline void ItemList::push_back(Item const & item)
    throw (xdata::exception::Exception)
{
    this->push_back(item.getLogLevel(), item.getMessage(), item.getFile(), item.getLine());
}
inline void ItemList::push_back(log4cplus::spi::InternalLoggingEvent const & event)
    throw (xdata::exception::Exception)
{
    this->push_back(event.getLogLevel(), event.getMessage(), event.getFile(), event.getLine());
}

inline void ItemList::pop_back()
{
    try {
        size_t _size = this->check_size();
        if (_size)
            {
                loglevel_.pop_back();
                message_.pop_back();
                file_.pop_back();
                line_.pop_back();
            }
    } catch (xdata::exception::Exception & e) {
        // no reason to forward it.
    }
}

inline size_t ItemList::check_size()
    throw (xdata::exception::Exception)
{
    size_t _size = loglevel_.size();
    if (message_.size() != _size || file_.size() != _size || line_.size() != _size)
        {
            this->clear();
            _size = 0;
            XCEPT_RAISE(xdata::exception::Exception, "sizes of the vectors in itemlist don't match; cleared.");
        }
    return _size;
}

} // namespace xdaqtools
} // namespace remotelogger
} // namespace rpct

#endif // rpct_xdaqtools_remotelogger_ItemList_inl_h_
