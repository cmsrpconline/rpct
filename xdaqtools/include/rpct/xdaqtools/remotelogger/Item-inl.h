/** Filip Thyssen */

#ifndef rpct_xdaqtools_remotelogger_Item_inl_h_
#define rpct_xdaqtools_remotelogger_Item_inl_h_

#include "rpct/xdaqtools/remotelogger/Item.h"

namespace rpct {
namespace xdaqtools {
namespace remotelogger {

inline log4cplus::LogLevel Item::getLogLevel() const
{
    return loglevel_.value_;
}
inline void Item::setLogLevel(log4cplus::LogLevel loglevel)
{
    loglevel_ = loglevel;
}

inline const std::string & Item::getMessage() const
{
    return message_.value_;
}
inline void Item::setMessage(const std::string & message)
{
    message_ = message;
}

inline const std::string & Item::getFile() const
{
    return file_.value_;
}
inline void Item::setFile(const std::string & file)
{
    file_ = file;
}

inline int Item::getLine() const
{
    return line_.value_;
}
inline void Item::setLine(int line)
{
    line_ = line;
}

} // namespace remotelogger
} // namespace xdaqtools
} // namespace rpct

#endif // rpct_xdaqtools_remotelogger_Item_inl_h_
