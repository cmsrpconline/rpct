/** Filip Thyssen */

#ifndef rpct_xdaqtools_remotelogger_const_h_
#define rpct_xdaqtools_remotelogger_const_h_

namespace rpct {
namespace xdaqtools {
namespace remotelogger {
namespace preset {

extern const unsigned int itemlist_size_;

extern const unsigned int client_itemlist_size_;
extern const int client_timeout_;

extern const char send_command_[];
extern const char attachment_id_[];

} // namespace preset
} // namespace remotelogger
} // namespace xdaqtools
} // namespace rpct

#endif // rpct_xdaqtools_remotelogger_const_h_
