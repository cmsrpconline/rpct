/** Filip Thyssen */

#ifndef rpct_xdaqtools_publisher_Service_inl_h_
#define rpct_xdaqtools_publisher_Service_inl_h_

#include "rpct/xdaqtools/publisher/Service.h"

namespace rpct {
namespace xdaqtools {
namespace publisher {

inline int PublisherService::action(Item & item)
{
    publisher_.publish(item.getId(), item.getType(), item.getValue());
    return 0;
}

} // namespace publisher
} // namespace xdaqtools
} // namespace rpct

#endif // rpct_xdaqtools_publisher_Service_inl_h_
