/** Filip Thyssen */

#ifndef rpct_xdaqtools_publisher_ItemList_inl_h_
#define rpct_xdaqtools_publisher_ItemList_inl_h_

#include "rpct/xdaqtools/publisher/ItemList.h"

namespace rpct {
namespace xdaqtools {
namespace publisher {

inline ItemList::const_iterator & ItemList::const_iterator::operator++()
{
    ++n_;
    change_ = true;

    size_t size = il_.size();
    if (n_ > size)
        n_ = size;
    return *this;
}

inline ItemList::const_iterator ItemList::const_iterator::operator++(int)
{
    const_iterator it(*this);
    operator++();
    return it;
}

inline ItemList::const_iterator & ItemList::const_iterator::operator--()
{
    if (n_ > 0)
        --n_;
    change_ = true;

    size_t size = il_.size();
    if (n_ > size)
        n_ = size;
    return *this;
}

inline ItemList::const_iterator ItemList::const_iterator::operator--(int)
{
    const_iterator it(*this);
    operator--();
    return it;
}

inline bool ItemList::const_iterator::operator==(const_iterator const & it)
{
    size_t size = il_.size();
    return (&il_ == &(it.il_) && (n_ == it.n_ || (n_ >= size && it.n_ >= size)));
}

inline bool ItemList::const_iterator::operator!=(const_iterator const & it)
{
    return !(operator==(it));
}

inline size_t ItemList::size() const
{
    return id_.size();
}

inline size_t ItemList::max_size() const
{
    size_t _max_size = id_.max_size();
    size_t _size = type_.max_size();
    if (_size < _max_size)
        _max_size = _size;
    if ((_size = value_.max_size()) < _max_size)
        _max_size = _size;
    return _max_size;
}

inline ItemList::const_iterator ItemList::begin() const
{
    return const_iterator(*this, 0);
}

inline ItemList::const_iterator ItemList::end() const
{
    return const_iterator(*this, this->size());
}

inline void ItemList::push_back(Item const & item)
    throw (xdata::exception::Exception)
{
    this->push_back(item.id_.value_, item.type_.value_, item.value_.value_);
}

inline void ItemList::pop_back()
{
    try {
        size_t _size = this->check_size();
        if (_size)
            {
                id_.pop_back();
                type_.pop_back();
                value_.pop_back();
            }
    } catch (xdata::exception::Exception & e) {
        // no reason to forward it.
    }
}

inline size_t ItemList::check_size()
    throw (xdata::exception::Exception)
{
    size_t _size = id_.size();
    if (type_.size() != _size || value_.size() != _size)
        {
            this->clear();
            _size = 0;
            XCEPT_RAISE(xdata::exception::Exception, "sizes of the vectors in itemlist don't match; cleared.");
        }
    return _size;
}

} // namespace xdaqtools
} // namespace publisher
} // namespace rpct

#endif // rpct_xdaqtools_publisher_ItemList_inl_h_
