/** Filip Thyssen */

#ifndef rpct_xdaqtools_publisher_ItemList_h_
#define rpct_xdaqtools_publisher_ItemList_h_

#include <iterator>
#include <cstddef>

#include "xdata/Integer.h"
#include "xdata/Double.h"
#include "xdata/Vector.h"

#include "rpct/xdaqtools/publisher/const.h"
#include "rpct/xdaqtools/publisher/Item.h"

namespace xdata {
class AbstractBag;
} // namespace xdata

namespace rpct {
namespace xdaqtools {
namespace publisher {

class Item;

class ItemList
{
public:
    class const_iterator
        : public virtual std::iterator<std::bidirectional_iterator_tag
                                       , Item, ptrdiff_t, Item *, Item &>
    {
    public:
        const_iterator(ItemList const & il, size_t n = 0);
        const_iterator(const_iterator const & it);
        virtual ~const_iterator();

        const_iterator & operator++();
        const_iterator operator++(int);
        const_iterator & operator--();
        const_iterator operator--(int);

        bool operator==(const_iterator const & it);
        bool operator!=(const_iterator const & it);

        Item const & operator*();
        Item const * operator->();
    private:
        ItemList const & il_;
        size_t n_;
        Item item_;
        bool change_;
    };

    friend class const_iterator;

    ItemList(size_t reserved = preset::itemlist_size_);
    virtual ~ItemList();

    void registerFields(xdata::AbstractBag * bag);

    size_t size() const;
    size_t max_size() const;

    const_iterator begin() const;
    const_iterator end() const;

    void reserve(size_t n = 0);

    void push_back(Item const & item)
        throw (xdata::exception::Exception);
    void push_back(int id, int type, double value)
        throw (xdata::exception::Exception);

    void pop_back();

    void clear();

protected:
    size_t check_size()
        throw (xdata::exception::Exception);

    size_t reserved_;

    // three vectors instead of a bag of vectors to make serialized version smaller
    // no table to keep from having to cast all the time
    // all types compatible with exdr
    xdata::Vector<xdata::Integer> id_;
    xdata::Vector<xdata::Integer> type_;
    xdata::Vector<xdata::Double> value_;
};

} // namespace publisher
} // namespace xdaqtools
} // namespace rpct

#include "rpct/xdaqtools/publisher/ItemList-inl.h"

#endif // rpct_xdaqtools_publisher_ItemList_h_
