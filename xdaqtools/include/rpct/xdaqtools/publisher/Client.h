/** Filip Thyssen */

#ifndef rpct_xdaqtools_publisher_Client_h_
#define rpct_xdaqtools_publisher_Client_h_

#include <log4cplus/logger.h>

#include "xdata/Bag.h"

#include "rpct/tools/Publisher.h"

#include "rpct/tools/RunningTimeVal.h"
#include "rpct/tools/Action.h"
#include "rpct/xdaqtools/QueueWorkLoop.h"
#include "rpct/xdaqtools/publisher/Item.h"
#include "rpct/xdaqtools/publisher/ItemList.h"

namespace xdaq {
class Application;
} // namespace xdaq

namespace rpct {
namespace xdaqtools {
namespace publisher {

class Client : public virtual rpct::tools::Publisher
{
public:
    Client(xdaq::Application & application
           , log4cplus::Logger const & logger
           , const std::string & service_name
           , unsigned int service_instance = 0
           , size_t max_size = preset::client_itemlist_size_);
    virtual ~Client();

    void publish(const Item & item);
    // Publisher
    void publish(int id, int type, double value);

    int process(Item & item);
    int send();

protected:
    xdaq::Application & application_;
    log4cplus::Logger logger_;
    const std::string service_name_;
    const unsigned int service_instance_;

    size_t max_size_;
    rpct::tools::RunningTimeVal timeout_;

    xdata::Bag<ItemList> itemsbag_;
    ItemList & items_;

    rpct::tools::ObjectAction<Item &, Client> * wl_action_;
    rpct::tools::Action<Client> * wl_send_;
    rpct::xdaqtools::QueueWorkLoop<Item> * qwl_;
};

} // namespace publisher
} // namespace xdaqtools
} // namespace rpct

#endif // rpct_xdaqtools_publisher_Client_h_
