/** Filip Thyssen */

#ifndef rpct_xdaqtools_publisher_Service_h_
#define rpct_xdaqtools_publisher_Service_h_

#include <log4cplus/logger.h>

#include "xoap/MessageReference.h"
#include "xoap/exception/Exception.h"

#include "rpct/tools/Publisher.h"

#include "rpct/tools/Action.h"
#include "rpct/xdaqtools/QueueWorkLoop.h"
#include "rpct/xdaqtools/publisher/Item.h"
#include "rpct/xdaqtools/publisher/ItemList.h"

namespace xdaq {
class Application;
} // namespace xdaq

namespace rpct {
namespace xdaqtools {
namespace publisher {

class Service
{
public:
    Service(xdaq::Application & application
            , const rpct::tools::ObjectActionSignature<Item &> * action = 0);
    virtual ~Service();

    xoap::MessageReference receive(xoap::MessageReference msg)
        throw (xoap::exception::Exception);
private:
    static log4cplus::Logger logger_;
protected:
    int list_action(ItemList & itemlist);

    xdaq::Application & application_;

    const rpct::tools::ObjectActionSignature<Item &> * action_;

    rpct::tools::ObjectAction<ItemList &, Service> * list_action_;
    rpct::xdaqtools::QueueWorkLoop<ItemList> * qwl_;
};
/** to allow for invisible use of the publisher client/service */
class PublisherService : public Service
{
public:
    PublisherService(xdaq::Application & application
                     , rpct::tools::Publisher & publisher);
    ~PublisherService();

protected:
    int action(Item & item);

    rpct::tools::Publisher & publisher_;
};

} // namespace publisher
} // namespace xdaqtools
} // namespace rpct

#include "rpct/xdaqtools/publisher/Service-inl.h"

#endif // rpct_xdaqtools_publisher_Service_h_
