/** Filip Thyssen */

#ifndef rpct_xdaqtools_publisher_Item_h_
#define rpct_xdaqtools_publisher_Item_h_

#include <ostream>

#include "xdata/Integer.h"
#include "xdata/Double.h"

namespace xdata {
class AbstractBag;
} // namespace xdata

namespace rpct {
namespace xdaqtools {
namespace publisher {

class ItemList;

class Item
{
    friend class ItemList;
public:
    Item(int id = 0, int type = 0, double value = 0);
    virtual ~Item();

    void registerFields(xdata::AbstractBag * bag);

    void setId(int id);
    int getId() const;

    void setType(int type);
    int getType() const;

    void setValue(double value);
    double getValue() const;
protected:
    xdata::Integer id_, type_;
    xdata::Double value_;
};

std::ostream & operator <<(std::ostream & os, const Item & item);

} // namespace publisher
} // namespace xdaqtools
} // namespace rpct

#include "rpct/xdaqtools/publisher/Item-inl.h"

#endif // rpct_xdaqtools_publisher_Item_h_
