/** Filip Thyssen */

#ifndef rpct_xdaqtools_publisher_Item_inl_h_
#define rpct_xdaqtools_publisher_Item_inl_h_

#include "rpct/xdaqtools/publisher/Item.h"

namespace rpct {
namespace xdaqtools {
namespace publisher {

inline void Item::setId(int id)
{
    id_ = id;
}
inline int Item::getId() const
{
    return id_.value_;
}

inline void Item::setType(int type)
{
    type_ = type;
}
inline int Item::getType() const
{
    return type_.value_;
}

inline void Item::setValue(double value)
{
    value_ = value;
}
inline double Item::getValue() const
{
    return value_.value_;
}

} // namespace publisher
} // namespace xdaqtools
} // namespace rpct

#endif // rpct_xdaqtools_publisher_Item_inl_h_
