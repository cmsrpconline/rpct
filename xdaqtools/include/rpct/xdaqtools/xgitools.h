/** Filip Thyssen */

#ifndef rpct_xdaqtools_xgitools_h_
#define rpct_xdaqtools_xgitools_h_

#include "toolbox/lang/Class.h"
#include "xgi/Method.h"
#include "xdaq/Application.h"

namespace rpct {
namespace xdaqtools {
namespace xgitools {

/** bind a function of an object of type T to a xgi command for _class */
template<typename T>
xgi::Method<T> * bind(toolbox::lang::Class & _class
                      , T & obj
                      , void (T::*func)(xgi::Input *, xgi::Output *) throw (xgi::exception::Exception)
                      , const std::string & command);

void unbind(toolbox::lang::Class & _class
            , const std::string & command);

std::string getFullURL(xdaq::Application & application);
void redirect(xdaq::Application & application, xgi::Input & in, const std::string & location);

} // namespace xgitools
} // namespace xdaqtools
} // namespace rpct

#include "rpct/xdaqtools/xgitools-inl.h"

#endif // rpct_xdaqtools_xgitools_h_
