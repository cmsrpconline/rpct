#ifndef rpct_xdaqtools_Timer_h_
#define rpct_xdaqtools_Timer_h_

#include <string>
#include <map>

#include "log4cplus/logger.h"

#include "rpct/tools/Action.h"

#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerListener.h"

#include "rpct/tools/RWMutex.h"

namespace rpct {
namespace xdaqtools {

/** An actionsignature for a timer listener */
typedef rpct::tools::ObjectActionSignature<toolbox::task::TimerEvent &> TimerActionSignature;

class Timer : protected toolbox::task::TimerListener
{
protected:
    void timeExpired(toolbox::task::TimerEvent & e);

    void scheduleAtFixedRate(TimerActionSignature & tas
                             , toolbox::TimeVal & start
                             , toolbox::TimeInterval & period
                             , void * context) throw (toolbox::task::exception::Exception);
    void schedule(TimerActionSignature & tas
                  , toolbox::TimeVal & time
                  , void * context) throw (toolbox::task::exception::Exception);

    void clear();

public:
    Timer(log4cplus::Logger & logger, const std::string & name = "undefinedTimer");
    ~Timer();

    template<typename Obj>
    void scheduleAtFixedRate(Obj & obj
                             , int (Obj::*func)(toolbox::task::TimerEvent &)
                             , const std::string & name
                             , toolbox::TimeVal & start
                             , toolbox::TimeInterval & period
                             , void * context) throw (toolbox::task::exception::Exception);
    template<typename Obj>
    void schedule(Obj & obj
                  , int (Obj::*func)(toolbox::task::TimerEvent &)
                  , const std::string & name
                  , toolbox::TimeVal & time
                  , void * context) throw (toolbox::task::exception::Exception);

    void remove(const std::string & name) throw (toolbox::task::exception::Exception);

    void start() throw (toolbox::task::exception::Exception);
    void stop() throw (toolbox::task::exception::Exception);

    bool isActive();

protected:
    log4cplus::Logger logger_;

    rpct::tools::RWMutex rwmutex_;
    std::map<std::string, TimerActionSignature *> vtas_;

    toolbox::task::Timer * timer_;
};

} // namespace xdaqtools
} // namespace rpct

#include "rpct/xdaqtools/Timer-inl.h"

#endif // rpct_xdaqtools_Timer_h_
