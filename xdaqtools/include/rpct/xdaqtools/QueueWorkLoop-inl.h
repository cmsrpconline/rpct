/** Filip Thyssen */

#ifndef rpct_xdaqtools_QueueWorkLoop_inl_h_
#define rpct_xdaqtools_QueueWorkLoop_inl_h_

#include "rpct/xdaqtools/QueueWorkLoop.h"

namespace rpct {
namespace xdaqtools {

template<typename T>
inline QueueWorkLoop<T>::QueueWorkLoop(const std::string & name
                                       , rpct::tools::ObjectActionSignature<T &> * action
                                       , rpct::tools::ObjectActionSignature<T &> * end_function
                                       , rpct::tools::RunningTimeVal * timeout
                                       , rpct::tools::ActionSignature * timeout_function)
    throw (toolbox::task::exception::Exception)
    : toolbox::task::WorkLoop(name, "queue")
    , action_(action)
    , timeout_(timeout)
    , timeout_function_(timeout_function)
    , pause_(1)
    , paused_(false)
    , disabled_(false)
{
    queue_ = new rpct::tools::squeue<T>(0, std::string("squeue_")+name, end_function);
}

template<typename T>
inline QueueWorkLoop<T>::~QueueWorkLoop()
{
    cancel();
    delete queue_;
}
template<typename T>
inline void QueueWorkLoop<T>::submit(toolbox::task::ActionSignature*)
    throw (toolbox::task::exception::Exception)
{}
template<typename T>
inline void QueueWorkLoop<T>::remove(toolbox::task::ActionSignature*)
    throw (toolbox::task::exception::Exception)
{}

template<typename T>
inline const std::string & QueueWorkLoop<T>::getName() const
{
    return name_;
}
template<typename T>
inline std::string QueueWorkLoop<T>::getState() const
{
    return (disabled_?"disabled":
            (paused_?"paused":
             (active_?"active":
              "not active")
             )
            );
}
template<typename T>
inline void QueueWorkLoop<T>::submit(const T & qobject, bool priority)
    throw (toolbox::task::exception::Exception)
{
    try {
        bool disabled(0);
        {
            rpct::tools::Lock plock(pause_);
            disabled = disabled_;
        }
        if (!disabled)
            {
                if (priority)
                    queue_->push_front(qobject);
                else
                    queue_->push(qobject);
            }
        else
            throw toolbox::task::exception::Exception
                ("task::exception::Exception"
                 , std::string("Failed to submit qobject, QueueWorkLoop") + name_ + "is disabled"
                 , __FILE__, __LINE__, __FUNCTION__);
    } catch (rpct::tools::exception::squeue_error & e) {
        throw toolbox::task::exception::Exception
            ("task::exception::Exception"
             , std::string("Failed to submit qobject") + e.what()
             , __FILE__, __LINE__, __FUNCTION__);
    }
}
template<typename T>
inline void QueueWorkLoop<T>::remove(const T & qobject)
    throw (toolbox::task::exception::Exception)
{
    try {
        queue_->remove(qobject);
    } catch(rpct::tools::exception::squeue_error & e) {
        throw toolbox::task::exception::Exception
            ("task::exception::Exception"
             , std::string("Failed to remove qobject") + e.what()
             , __FILE__, __LINE__, __FUNCTION__);
    }
}
template<typename T>
inline void QueueWorkLoop<T>::pause()
{
    rpct::tools::Lock plock(pause_);
    paused_ = true;
}
template<typename T>
inline void QueueWorkLoop<T>::resume() throw (toolbox::task::exception::Exception)
{
    rpct::tools::Lock plock(pause_);
    if (disabled_)
        throw toolbox::task::exception::Exception
            ("task::exception::Exception"
             , std::string("QueueWorkLoop ") + name_ + " could not resume - it's disabled"
             , __FILE__, __LINE__, __FUNCTION__);
    else if (paused_)
        {
            paused_ = false;
            pause_.free_one(false);
        }
}
template<typename T>
inline bool QueueWorkLoop<T>::isPaused()
{
    return paused_;
}
template<typename T>
inline void QueueWorkLoop<T>::disable()
{
    rpct::tools::Lock plock(pause_);
    disabled_ = true;
    paused_ = true;
    queue_->clear();
}
template<typename T>
inline void QueueWorkLoop<T>::enable()
{
    rpct::tools::Lock plock(pause_);
    disabled_ = false;
}
template<typename T>
inline bool QueueWorkLoop<T>::isDisabled()
{
    return disabled_;
}
template<typename T>
inline bool QueueWorkLoop<T>::isStopped()
{
    return stopped_;
}
template<typename T>
inline void QueueWorkLoop<T>::activate() throw (toolbox::task::exception::Exception)
{
    this->start();
}
template<typename T>
inline void QueueWorkLoop<T>::start() throw (toolbox::task::exception::Exception)
{
    rpct::tools::Lock plock(pause_);
    if (disabled_)
        throw toolbox::task::exception::Exception
            ("task::exception::Exception"
             , std::string("QueueWorkLoop ") + name_ + " could not be activated - it's disabled"
             , __FILE__, __LINE__, __FUNCTION__);
    else if (!active_)
        {
            plock.give();
            toolbox::task::WorkLoop::activate();
        }
    else if (paused_)
        {
            paused_ = false;
            pause_.free_one(false);
        }
    else
        throw toolbox::task::exception::Exception
            ("task::exception::Exception"
             , std::string("QueueWorkLoop ") + name_ + " could not be started - it's allready active"
             , __FILE__, __LINE__, __FUNCTION__);
}
template<typename T>
inline void QueueWorkLoop<T>::reset() throw (toolbox::task::exception::Exception)
{
    if (active_)
        cancel();
    enable();
}
template<typename T>
inline void QueueWorkLoop<T>::cancel() throw (toolbox::task::exception::Exception)
{
    if (active_)
        {
            rpct::tools::Lock plock(pause_);

            stopped_ = true; // stopped, the running process will be the last one

            bool disabled = disabled_;
            disabled_ = true; // no new submit

            if (paused_)
                {
                    paused_ = false;
                    pause_.free_one(false); // thread will await free pause_ mutex
                }
            queue_->free();
            queue_->clear();

            if (active_)
                {
                    plock.give(); // free mutex to allow for stopping
                    toolbox::task::WorkLoop::cancel();
                    plock.take();
                }

            disabled_ = disabled;
        }
    else // so it's already cancelled
        throw toolbox::task::exception::Exception
            ("task::exception::Exception"
             , std::string("QueueWorkLoop ") + name_ + " is not active"
             , __FILE__, __LINE__, __FUNCTION__);
}
template<typename T>
inline void QueueWorkLoop<T>::clear() throw (toolbox::task::exception::Exception)
{
    queue_->clear();
}
template<typename T>
inline size_t QueueWorkLoop<T>::size()
{
    return queue_->size();
}
template<typename T>
inline void QueueWorkLoop<T>::resize(size_t n) throw (toolbox::task::exception::Exception)
{
    queue_->max_size(n);
}
template<typename T>
inline void QueueWorkLoop<T>::process()
{
    if (timeout_)
        {
            try{
                { // pause and sync
                    rpct::tools::Lock plock(pause_);
                    if (paused_)
                        pause_.wait();
                }
                if (!stopped_)
                    {
                        timeval timeout = timeout_->getTimeVal();
                        timespec endtime = rpct::tools::timeval_to_abs_timespec(timeout);

                        T qobject = queue_->wait_pop(endtime);
                        if (!stopped_)
                            action_->invoke(qobject);
                        else // to ensure endfunction is called
                            queue_->push_front(qobject);
                    }
            } catch(rpct::tools::exception::squeue_timeout_error & e) {
                if (timeout_function_)
                    timeout_function_->invoke();
                timeout_->reset();
            } catch(rpct::tools::exception::squeue_error & e) {
            }
        }
    else
        {
            try{
                {
                    rpct::tools::Lock plock(pause_);
                    if (paused_)
                        pause_.wait();
                }
                if (!stopped_)
                    {
                        T qobject = queue_->wait_pop();
                        if (!stopped_)
                            action_->invoke(qobject);
                        else // to ensure endfunction is called
                            queue_->push_front(qobject);
                    }
            } catch(rpct::tools::exception::squeue_error & e) {
            }
        }
}

} // namespace xdaqtools
} // namespace rpct

#endif // rpct_xdaqtools_QueueWorkLoop_inl_h_
