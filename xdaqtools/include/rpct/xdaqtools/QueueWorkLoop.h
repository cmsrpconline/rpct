/** Filip Thyssen */

#ifndef rpct_xdaqtools_QueueWorkLoop_h_
#define rpct_xdaqtools_QueueWorkLoop_h_

#include <queue>
#include <string>

#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/Action.h"

#include "rpct/tools/squeue.h"
#include "rpct/tools/Action.h"
#include "rpct/tools/RunningTimeVal.h"
#include "rpct/tools/Condition.h"

namespace rpct {
namespace xdaqtools {

/** A WorkLoop that does the same job for different objects of type T */
template<typename T>
class QueueWorkLoop : public toolbox::task::WorkLoop
{
public:
    /** Constructor for QueueWorkLoop
     * \param name name for the QueueWorkLoop
     * \param action the action to perform
     * \param end_function the action to perform when an element is removed without processing
     * \param timeout the timeout to be used by wait_pop
     *                  if used as timeval it's a simple limit on wait_pop caught with timeout_function_ if present
     *                  if used as countdown it performs timeout_function_ every (timeout) time if present
     */
    QueueWorkLoop(const std::string & name
                  , rpct::tools::ObjectActionSignature<T &> * action
                  , rpct::tools::ObjectActionSignature<T &> * end_function = 0
                  , rpct::tools::RunningTimeVal * timeout = 0
                  , rpct::tools::ActionSignature * timeout_function = 0)
        throw (toolbox::task::exception::Exception);
    virtual ~QueueWorkLoop();

    const std::string & getName() const;
    std::string getState() const;

    // WorkLoop
    /** to inherit from WorkLoop, no meaning here */
    void submit(toolbox::task::ActionSignature*)
        throw (toolbox::task::exception::Exception);
    /** to inherit from WorkLoop, no meaning here */
    void remove(toolbox::task::ActionSignature*)
        throw (toolbox::task::exception::Exception);

    /** submit a new object; if it has priority, it's pushed to the front, otherwise to the back */
    void submit(const T & qobject, bool priority = false)
        throw (toolbox::task::exception::Exception);
    /** remove an existing object */
    void remove(const T & qobject)
        throw (toolbox::task::exception::Exception);

    /** pause the processing of the queue, but don't cancel it */
    void pause();
    /** resume the processing of the queue */
    void resume() throw (toolbox::task::exception::Exception);
    bool isPaused();

    /** disable processing */
    void disable();
    /** enable processing */
    void enable();
    bool isDisabled();
    bool isStopped();

    /** not virtual :-( ; activate or resume the processing of the queue */
    void activate() throw (toolbox::task::exception::Exception);
    /** activate or resume the processing of the queue */
    void start() throw (toolbox::task::exception::Exception);
    void reset() throw (toolbox::task::exception::Exception);
    void cancel() throw (toolbox::task::exception::Exception);

    /** clear the queue */
    void clear() throw (toolbox::task::exception::Exception);
    size_t size();
    /** resize the queue */
    void resize(size_t n) throw (toolbox::task::exception::Exception);

protected:
    void process();
protected:
    rpct::tools::ObjectActionSignature<T &> * action_;
    rpct::tools::RunningTimeVal * timeout_;
    rpct::tools::ActionSignature * timeout_function_;
    rpct::tools::squeue<T> * queue_;
    rpct::tools::Condition pause_;
    volatile bool paused_, disabled_;
};

} // namespace xdaqtoolbox
} // namespace rpct

#include "rpct/xdaqtools/QueueWorkLoop-inl.h"

#endif // rpct_xdaqtools_QueueWorkLoop_h_
