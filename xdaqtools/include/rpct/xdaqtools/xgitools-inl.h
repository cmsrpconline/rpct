/** Filip Thyssen */

#ifndef rpct_xdaqtools_xgitools_inl_h_
#define rpct_xdaqtools_xgitools_inl_h_

#include "rpct/xdaqtools/xgitools.h"

namespace rpct {
namespace xdaqtools {
namespace xgitools {

template<typename T>
inline xgi::Method<T> * bind(toolbox::lang::Class & _class
                             , T & obj
                             , void (T::*func)(xgi::Input *, xgi::Output *) throw (xgi::exception::Exception)
                             , const std::string & command)
{
    xgi::Method<T> * f = new xgi::Method<T>;
    f->obj_ = &obj;
    f->func_ = func;
    f->name_ = command;
    _class.removeMethod(command); // calls delete if necessary
    _class.addMethod(f, command);
    return f;
}

inline void unbind(toolbox::lang::Class & _class
                   , const std::string & command)
{
    _class.removeMethod(command); // calls delete if necessary
}

inline std::string getFullURL(xdaq::Application & application)
{
    return application.getApplicationContext()->getContextDescriptor()->getURL() + "/" + application.getApplicationDescriptor()->getURN() + "/";
}

inline void redirect(xdaq::Application & application, xgi::Output & out, const std::string & location) throw (xgi::exception::Exception)
{
    std::string url = getFullURL(application) + location;
    out.getHTTPResponseHeader().getStatusCode(303);
    out.getHTTPResponseHeader().addHeader("Location", url.c_str());
}

} // namespace xgitools
} // namespace xdaqtools
} // namespace rpct

#endif // rpct_xdaqtools_xgitools_inl_h_
