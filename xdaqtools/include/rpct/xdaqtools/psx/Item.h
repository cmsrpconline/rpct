/** Filip Thyssen */

#ifndef rpct_xdaqtools_psx_Item_h_
#define rpct_xdaqtools_psx_Item_h_

#include <string>
#include <ostream>

namespace rpct {
namespace xdaqtools {
namespace psx {

class Item
{
public:
    template<typename T>
    Item(const std::string & name, const std::string & type, const T & value);
    Item(const std::string & name, const std::string & type, const std::string & value);
    
    const std::string & getName() const;
    void setName(const std::string & name);

    const std::string & getType() const;
    void setType(const std::string & type);

    const std::string & getValue() const;
    template<typename T>
    void setValue(const T & value);
    void setValue(const std::string & value);
protected:
    std::string name_, type_, value_;
};

std::ostream & operator<<(std::ostream & os, const Item & item);

} // namespace publisher
} // namespace xdaqtools
} // namespace rpct

#include "rpct/xdaqtools/psx/Item-inl.h"

#endif // rpct_xdaqtools_psx_Item_h_
