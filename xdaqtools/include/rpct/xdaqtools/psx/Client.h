/** Filip Thyssen */

#ifndef rpct_xdaqtools_psx_Client_h_
#define rpct_xdaqtools_psx_Client_h_

#include <string>

#include <log4cplus/logger.h>

#include "xoap/MessageReference.h"

#include "rpct/tools/RunningTimeVal.h"
#include "rpct/tools/Action.h"
#include "rpct/xdaqtools/QueueWorkLoop.h"
#include "rpct/xdaqtools/psx/const.h"
#include "rpct/xdaqtools/psx/Item.h"

namespace xdaq {
class Application;
} // namespace xdaq

namespace rpct {
namespace xdaqtools {
namespace psx {

class Client
{
protected:
    void sendMessage(xoap::MessageReference & message);

public:
    Client(xdaq::Application & application
           , log4cplus::Logger const & logger
           , const std::string & psx_service_name
           , unsigned int psx_service_instance
           , const std::string & dcs_service_name
           , size_t max_size = preset::psx_itemlist_size_);
    virtual ~Client();

    void publish(const Item & item);
    template<typename T>
    void publish (const std::string & name, const std::string & type, const T & value);

    int process(Item & item);
    int send();

protected:
    xdaq::Application & application_;
    log4cplus::Logger logger_;
    const std::string psx_service_name_;
    const unsigned int psx_service_instance_;
    const std::string dcs_service_name_;

    size_t max_size_;
    rpct::tools::RunningTimeVal timeout_;

    std::vector<Item> items_;

    rpct::tools::ObjectAction<Item &, Client> * wl_action_;
    rpct::tools::Action<Client> * wl_send_;
    rpct::xdaqtools::QueueWorkLoop<Item> * qwl_;
};

} // namespace psx
} // namespace xdaqtools
} // namespace rpct

#include "rpct/xdaqtools/psx/Client-inl.h"

#endif // rpct_xdaqtools_psx_Client_h_
