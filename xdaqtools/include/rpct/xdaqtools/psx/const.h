/** Filip Thyssen */

#ifndef rpct_xdaqtools_psx_const_h_
#define rpct_xdaqtools_psx_const_h_

namespace rpct {
namespace xdaqtools {
namespace psx {
namespace preset {

extern const unsigned int psx_itemlist_size_;
extern const int psx_timeout_;

} // namespace preset
} // namespace psx
} // namespace xdaqtools
} // namespace rpct

#endif // rpct_xdaqtools_psx_const_h_
