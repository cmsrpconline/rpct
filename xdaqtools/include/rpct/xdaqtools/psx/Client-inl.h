/** Filip Thyssen */

#ifndef rpct_xdaqtools_psx_Client_inl_h_
#define rpct_xdaqtools_psx_Client_inl_h_

#include "rpct/xdaqtools/psx/Client.h"

namespace rpct {
namespace xdaqtools {
namespace psx {

template<typename T>
inline void Client::publish(const std::string & name, const std::string & type, const T & value)
{
    publish(Item(name, type, value));
}
} // namespace psx
} // namespace xdaqtools
} // namespace rpct

#endif // rpct_xdaqtools_psx_Client_inl_h_
