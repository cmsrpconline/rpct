/** Filip Thyssen */

#ifndef rpct_xdaqtools_psx_Item_inl_h_
#define rpct_xdaqtools_psx_Item_inl_h_

#include "rpct/xdaqtools/psx/Item.h"

#include <sstream>

namespace rpct {
namespace xdaqtools {
namespace psx {

template<typename T>
inline Item::Item(const std::string & name, const std::string & type, const T & value)
    : name_(name)
    , type_(type)
{
    setValue(value);
}
inline Item::Item(const std::string & name, const std::string & type, const std::string & value)
    : name_(name)
    , type_(type)
    , value_(value)
{}

inline const std::string & Item::getName() const
{
    return name_;
}
inline void Item::setName(const std::string & name)
{
    name_ = name;
}

inline const std::string & Item::getType() const
{
    return type_;
}
inline void Item::setType(const std::string & type)
{
    type_ = type;
}

inline const std::string & Item::getValue() const
{
    return value_;
}
template<typename T>
inline void Item::setValue(const T & value)
{
    std::stringstream val;
    val << value;
    value_ = val.str();
}
template<>
inline void Item::setValue(const std::string & value)
{
    value_ = value;
}

} // namespace publisher
} // namespace xdaqtools
} // namespace rpct

#endif // rpct_xdaqtools_psx_Item_inl_h_
