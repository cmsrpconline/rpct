#ifndef rpct_xdaqtools_Timer_inl_h_
#define rpct_xdaqtools_Timer_inl_h_

#include "rpct/xdaqtools/Timer.h"

namespace rpct {
namespace xdaqtools {

template<typename Obj>
void Timer::scheduleAtFixedRate(Obj & obj
                                , int (Obj::*func)(toolbox::task::TimerEvent &)
                                , const std::string & name
                                , toolbox::TimeVal & start
                                , toolbox::TimeInterval & period
                                , void * context) throw (toolbox::task::exception::Exception)
{
    rpct::tools::ObjectAction<toolbox::task::TimerEvent &, Obj> * tas
        = new rpct::tools::ObjectAction<toolbox::task::TimerEvent &, Obj>(obj, func, name);
    scheduleAtFixedRate(*tas, start, period, context);
}

template<typename Obj>
void Timer::schedule(Obj & obj
                     , int (Obj::*func)(toolbox::task::TimerEvent &)
                     , const std::string & name
                     , toolbox::TimeVal & time
                     , void * context) throw (toolbox::task::exception::Exception)
{
    rpct::tools::ObjectAction<toolbox::task::TimerEvent &, Obj> * tas
        = new rpct::tools::ObjectAction<toolbox::task::TimerEvent &, Obj>(obj, func, name);
    scheduleAtFixedRate(*tas, start, time, context);
}



} // namespace xdaqtools
} // namespace rpct

#endif // rpct_xdaqtools_Timer_inl_h_
