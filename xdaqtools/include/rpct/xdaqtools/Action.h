/** Filip Thyssen */

#ifndef rpct_xdaqtools_Action_h_
#define rpct_xdaqtools_Action_h_

#include <string>

#include "toolbox/lang/Method.h"
#include "toolbox/lang/Class.h"

namespace rpct {
namespace xdaqtools {

class ActionSignature : public toolbox::lang::Method
{
public:
    virtual ~ActionSignature();

    // Method
    std::string type();

    virtual int invoke() const = 0;
    int operator()() const;

    virtual std::string name() const;
};
template<typename Listener>
class Action : public ActionSignature
{
public:
    Action(Listener & listener
           , int (Listener::*func)()
           , const std::string & name = "undefinedAction");
    virtual ~Action();

    // Method
    std::string type();

    // ActionSignature
    int invoke() const;
    std::string name() const;

protected:
    Listener & listener_;
    int (Listener::* const func_)();
    const std::string name_;
};

template <typename Listener>
Action<Listener> * bind(toolbox::lang::Class & _class
                        , Listener & listener
                        , int (Listener::*func)()
                        , const std::string & name = "undefinedAction");

/** An actionsignature for the QueueWorkLoop */
template<typename T>
class ObjectActionSignature : public toolbox::lang::Method
{
public:
    virtual ~ObjectActionSignature();

    // Method
    std::string type();

    virtual int invoke(T obj) const = 0;
    int operator()(T obj) const;

    virtual std::string name() const;
};
template<typename T, typename Listener>
class ObjectAction
    : public ObjectActionSignature<T>
{
public:
    ObjectAction(Listener & listener
                 , int (Listener::*func)(T)
                 , const std::string & name = "undefinedObjectAction");
    virtual ~ObjectAction();

    // Method
    std::string type();

    int invoke(T obj) const;
    std::string name() const;

protected:
    Listener & listener_;
    int (Listener::* const func_)(T);
    const std::string name_;
};

template <typename T, typename Listener>
ObjectAction<T, Listener> * bind(toolbox::lang::Class & _class
                                 , Listener & listener
                                 , int (Listener::*func)(T)
                                 , const std::string & name = "undefinedObjectAction");

} // namespace xdaqtools
} // namespace rpct

#include "rpct/xdaqtools/Action-inl.h"

#endif // rpct_xdaqtools_Action_h_
