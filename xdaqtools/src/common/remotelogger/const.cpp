#include "rpct/xdaqtools/remotelogger/const.h"

namespace rpct {
namespace xdaqtools {
namespace remotelogger {
namespace preset {

const unsigned int itemlist_size_ = 20;

const unsigned int client_itemlist_size_ = 20;
const int client_timeout_ = 5;

const char send_command_[] = "remotelogger_SendItemList";
const char attachment_id_[] = "ItemList";

} // namespace preset
} // namespace remotelogger
} // namespace xdaqtools
} // namespace rpct
