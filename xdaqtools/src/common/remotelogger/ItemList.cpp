#include "rpct/xdaqtools/remotelogger/ItemList.h"

#include <stdexcept>
#include <sstream>

#include "xdata/AbstractBag.h"

#include "rpct/xdaqtools/remotelogger/Item.h"

namespace rpct {
namespace xdaqtools {
namespace remotelogger {

ItemList::const_iterator::const_iterator(ItemList const & il, size_t n)
    : il_(il)
    , n_(n)
    , change_(true)
{
    size_t size = il_.size();
    if (n_ > size)
        n_ = size;
    else if (n_ < 0)
        n_ = 0;
}

ItemList::const_iterator::const_iterator(const_iterator const & it)
    : il_(it.il_)
    , n_(it.n_)
    , change_(true)
{}

ItemList::const_iterator::~const_iterator()
{}

Item const & ItemList::const_iterator::operator*()
{
    if (n_ < il_.size())
        {
            if (change_)
                {
                    item_.setLogLevel(il_.loglevel_[n_].value_);
                    item_.setMessage(il_.message_[n_].value_);
                    item_.setFile(il_.file_[n_].value_);
                    item_.setLine(il_.line_[n_].value_);

                    change_ = false;
                }
        }
    else
        throw std::out_of_range("dereferencing end const_iterator.");
    return item_;
}

Item const * ItemList::const_iterator::operator->()
{
    if (n_ < il_.size())
        {
            if (change_)
                {
                    item_.setLogLevel(il_.loglevel_[n_].value_);
                    item_.setMessage(il_.message_[n_].value_);
                    item_.setFile(il_.file_[n_].value_);
                    item_.setLine(il_.line_[n_].value_);

                    change_ = false;
                }
            return &item_;
        }
    else
        return 0;
}

ItemList::ItemList(size_t reserved)
    : reserved_(reserved)
{
    this->reserve();
}

void ItemList::registerFields(xdata::AbstractBag * bag)
{
    bag->addField("loglevel", &loglevel_);
    bag->addField("message", &message_);
    bag->addField("file", &file_);
    bag->addField("line", &line_);
}

void ItemList::reserve(size_t n)
{
    if (n)
        reserved_ = n;
    try {
        loglevel_.reserve(reserved_);
        message_.reserve(reserved_);
        file_.reserve(reserved_);
        line_.reserve(reserved_);
    } catch(std::bad_alloc & e) {
    }
}

void ItemList::push_back(log4cplus::LogLevel loglevel, const std::string & message, const std::string & file, int line)
    throw (xdata::exception::Exception)
{
    size_t _size = this->check_size();
    try {
        loglevel_.push_back(loglevel);
        message_.push_back(message);
        file_.push_back(file);
        line_.push_back(line);
    } catch (std::bad_alloc & e) {
        if (loglevel_.size() > _size)
            {
                loglevel_.pop_back();
                if (message_.size() > _size)
                    {
                        message_.pop_back();
                        if (file_.size() > _size)
                            {
                                file_.pop_back();
                                if (line_.size() > _size)
                                    line_.pop_back();
                            }
                    }
            }
        std::stringstream estr;
        estr << "the item could not be added, bad_alloc: " << e.what();
        XCEPT_RAISE(xdata::exception::Exception, estr.str());
    }
}

void ItemList::clear()
{
    loglevel_.clear();
    message_.clear();
    file_.clear();
    line_.clear();

    this->reserve();
}

} // namespace remotelogger
} // namespace xdaqtools
} // namespace rpct
