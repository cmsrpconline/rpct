#include "rpct/xdaqtools/remotelogger/Service.h"

#include "xdaq/Application.h"

#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"

#include "xdata/Bag.h"
#include "xdata/Vector.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"

#include "rpct/xdaqtools/xoaptools.h"
#include "rpct/xdaqtools/remotelogger/const.h"

namespace rpct {
namespace xdaqtools {
namespace remotelogger {

Service::Service(xdaq::Application & application
                 , log4cplus::Logger const & logger)
    : logger_(logger)
    , application_(application)
{
    rpct::xdaqtools::xoaptools::bind(application_, *this, &Service::receive, preset::send_command_);

    list_action_ = new rpct::tools::ObjectAction<ItemList &, Service>(*this, &Service::list_action, "ListAction");
    qwl_ = new rpct::xdaqtools::QueueWorkLoop<ItemList> ("ListWorkLoop", list_action_);
    qwl_->start();
}

Service::~Service()
{
    rpct::xdaqtools::xoaptools::unbind(application_, preset::send_command_);
    delete qwl_;
    delete list_action_;
}

xoap::MessageReference Service::receive(xoap::MessageReference msg)
    throw (xoap::exception::Exception)
{
    if (msg->countAttachments() > 0)
        {
            std::list<xoap::AttachmentPart*> attachments = msg->getAttachments();
            std::list<xoap::AttachmentPart*>::iterator IattEnd = attachments.end();
            for (std::list<xoap::AttachmentPart*>::iterator Iatt = attachments.begin()
                     ; Iatt != IattEnd
                     ; ++Iatt)
                {
                    if ((*Iatt)->getContentId() == preset::attachment_id_
                        && (*Iatt)->getContentEncoding() == "exdr"
                        && (*Iatt)->getContentType() == "application/xdata+bag")
                        {
                            xdata::exdr::Serializer serializer;
                            xdata::exdr::FixedSizeInputStreamBuffer buffer((*Iatt)->getContent(),(*Iatt)->getSize());
                            xdata::Bag<ItemList> itemsbag;
                            try {
                                serializer.import(&itemsbag, &buffer);
                                qwl_->submit(itemsbag.bag);
                            } catch(xdata::exception::Exception & e) {
                                LOG4CPLUS_ERROR(logger_, "Failed to extract item list from soap attachment: "
                                                << e.what());
                            } catch(toolbox::task::exception::Exception & e) {
                                LOG4CPLUS_ERROR(logger_, "Failed to add item list to incoming workloop: "
                                                << e.what());
                            }
                        }
                    else
                        {
                            LOG4CPLUS_DEBUG(logger_, "Service::receive(): attachment with unknown content id "
                                            << (*Iatt)->getContentId());
                        }
                }
        }
    else
        {
            LOG4CPLUS_DEBUG(logger_, "Service::receive() called but message without attachment.");
        }

    xoap::MessageReference reply = xoap::createMessage();
    xoap::SOAPEnvelope env = reply->getSOAPPart().getEnvelope();
    xoap::SOAPName command = env.createName(std::string(preset::send_command_) + "Response"
                                            , "xdaq"
                                            , XDAQ_NS_URI);
    xoap::SOAPBodyElement command_el = env.getBody().addBodyElement(command);
    return reply;
}

int Service::list_action(ItemList & itemlist)
{
    Item item;
    // we have to reverse the order back to the original
    ItemList::const_iterator IitemEnd = itemlist.end();
    for (ItemList::const_iterator Iitem = itemlist.begin()
             ; Iitem != IitemEnd
             ; ++Iitem)
        {
            item = *Iitem;
            logger_.log(item.getLogLevel(), item.getMessage(), item.getFile().c_str(), item.getLine());
        }
    return 0;
}

} // namespace xdaqtools
} // namespace remotelogger
} // namespace rpct
