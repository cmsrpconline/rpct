#include "rpct/xdaqtools/remotelogger/Item.h"

#include "xdata/AbstractBag.h"

namespace rpct {
namespace xdaqtools {
namespace remotelogger {

Item::Item(log4cplus::LogLevel loglevel
           , const std::string & message
           , const std::string & file
           , int line)
    : loglevel_(loglevel)
    , message_(message)
    , file_(file)
    , line_(line)
{}

Item::Item(log4cplus::spi::InternalLoggingEvent const & event)
    : loglevel_(event.getLogLevel())
    , message_(event.getMessage())
    , file_(event.getFile())
    , line_(event.getLine())
{}

void Item::registerFields(xdata::AbstractBag * bag)
{
    bag->addField("loglevel", &loglevel_);
    bag->addField("message", &message_);
    bag->addField("file", &file_);
    bag->addField("line", &line_);
}

log4cplus::spi::InternalLoggingEvent Item::getLogEvent() const
{
    return log4cplus::spi::InternalLoggingEvent("", loglevel_.value_, message_.value_, file_.value_.c_str(), line_.value_);
}

void Item::setLogEvent(log4cplus::spi::InternalLoggingEvent const & event)
{
    loglevel_ = event.getLogLevel();
    message_ = event.getMessage();
    file_ = event.getFile();
    line_ = event.getLine();
}

std::ostream & operator <<(std::ostream & os, const Item & item)
{
    os << "item(" << log4cplus::getLogLevelManager().toString(item.getLogLevel()) << " at " << item.getFile() << ':' << item.getLine() << ": " << item.getMessage() << ')';
    return os;
}

} // namespace remotelogger
} // namespace xdaqtools
} // namespace rpct
