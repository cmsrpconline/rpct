#include "rpct/xdaqtools/remotelogger/Client.h"

#include "xdaq/NamespaceURI.h"
#include "xdaq/Application.h"
#include "xdaq/ApplicationDescriptor.h"

#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"

#include "xdata/exdr/Serializer.h"
#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"

#include "rpct/xdaqtools/remotelogger/const.h"

namespace rpct {
namespace xdaqtools {
namespace remotelogger {

Client::Client(xdaq::Application & application
               , log4cplus::Logger const & logger
               , const std::string & service_name
               , unsigned int service_instance
               , size_t max_size)
    : application_(application)
    , logger_(log4cplus::Logger::getInstance(logger.getName() + ".Remotelogger"))
    , service_name_(service_name)
    , service_instance_(service_instance)
    , max_size_(max_size)
    , timeout_(rpct::tools::RunningTimeVal::countdown_, preset::client_timeout_)
    , itemsbag_()
    , items_(itemsbag_.bag)
{
    items_.reserve(max_size_);

    wl_action_ = new rpct::tools::ObjectAction<Item &, Client>(*this, &Client::process, "ClientAction");
    wl_send_ = new rpct::tools::Action<Client>(*this, &Client::send, "ClientSend");
    qwl_ = new rpct::xdaqtools::QueueWorkLoop<Item> ("ClientWorkLoop", wl_action_, 0, &timeout_, wl_send_);

    qwl_->start();
}

Client::~Client()
{
    delete qwl_;
    delete wl_send_;
    delete wl_action_;
}

void Client::append(log4cplus::spi::InternalLoggingEvent const & event)
{
    try {
        if (event.getLoggerName() != logger_.getName()) // avoid client messages
            qwl_->submit(event);
    } catch(toolbox::task::exception::Exception & e) {
        LOG4CPLUS_ERROR(logger_, "remotelogger::Client::publish() exception for " << event << ": " << e.what());
    }
}

int Client::process(Item & item)
{
    LOG4CPLUS_TRACE(logger_, "remotelogger::Client::process()");

    try {
        items_.push_back(item);
    } catch(xdata::exception::Exception & e) {
        LOG4CPLUS_WARN(logger_, "Failed to add item to the item list; trying again after calling send(): " << e.what());
        this->send();
        try {
            items_.push_back(item);
        } catch(xdata::exception::Exception & e) {
            LOG4CPLUS_ERROR(logger_, "Failed to add item to the item list again; giving up: " << e.what());
        }
    }

    if (items_.size() >= max_size_)
        {
            LOG4CPLUS_DEBUG(logger_, "remotelogger::Client::process() calls send()");
            this->send();
        }

    return 0;
}

int Client::send()
{
    LOG4CPLUS_TRACE(logger_, "remotelogger::Client::send()");

    if (items_.size() > 0)
        {
            // Create SOAP message
            xoap::MessageReference message = xoap::createMessage();
            try {
                xoap::SOAPEnvelope env = message->getSOAPPart().getEnvelope();
                xoap::SOAPBody body = env.getBody();
                xoap::SOAPName command = env.createName(preset::send_command_, "xdaq", XDAQ_NS_URI);
                xoap::SOAPBodyElement command_el = body.addBodyElement(command);

                xdata::exdr::Serializer serializer;
                xdata::exdr::AutoSizeOutputStreamBuffer buffer;

                serializer.exportAll(&itemsbag_, &buffer);
                xoap::AttachmentPart * attachment = message->createAttachmentPart(buffer.getBuffer()
                                                                                  , buffer.tellp()
                                                                                  , "application/xdata+bag");
                attachment->setContentEncoding("exdr");
                attachment->setContentId(preset::attachment_id_);
                message->addAttachmentPart(attachment);
            } catch(xdata::exception::Exception & e) {
                LOG4CPLUS_ERROR(logger_, "Failed to create SOAP message: " << e.what());
            } catch(xoap::exception::Exception & e) {
                LOG4CPLUS_ERROR(logger_, "Failed to create SOAP message: " << e.what());
            } catch(...) {
                LOG4CPLUS_ERROR(logger_, "Failed to create SOAP message.");
            }

            // Send SOAP message
            try {
                const xdaq::ApplicationDescriptor * orig = application_.getApplicationDescriptor();
                const xdaq::ApplicationDescriptor * dest = application_.getApplicationContext()->getDefaultZone()
                    ->getApplicationDescriptor(service_name_, service_instance_);

                LOG4CPLUS_DEBUG(logger_, "remotelogger::Client::sending...");
                xoap::MessageReference reply = application_.getApplicationContext()->postSOAP(message
                                                                                              , *orig
                                                                                              , *dest);
                LOG4CPLUS_DEBUG(logger_, "remotelogger::Client::sent.");

                xoap::SOAPEnvelope replyEnv = reply->getDocument()->getDocumentElement();
                xoap::SOAPBody replyBody = replyEnv.getBody();
                if (replyBody.hasFault())
                    {
                        LOG4CPLUS_ERROR(logger_, "Fault in SOAP Message reply: " << replyBody.getFault().getFaultString());
                    }
            } catch (xdaq::exception::Exception & e) {
                LOG4CPLUS_ERROR(logger_, "Failed to send SOAP message to remotelogger service: " << e.what());
            }
        }

    items_.clear();
    timeout_.reset();
    return 0;
}

} // namespace remotelogger
} // namespace xdaqtools
} // namespace rpct
