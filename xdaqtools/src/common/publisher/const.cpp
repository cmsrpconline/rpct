#include "rpct/xdaqtools/publisher/const.h"

namespace rpct {
namespace xdaqtools {
namespace publisher {
namespace preset {

const unsigned int itemlist_size_ = 1000;

const unsigned int client_itemlist_size_ = 1000;
const int client_timeout_ = 30;

const char send_command_[] = "publisher_SendItemList";
const char attachment_id_[] = "ItemList";

} // namespace preset
} // namespace publisher
} // namespace xdaqtools
} // namespace rpct
