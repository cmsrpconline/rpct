#include "rpct/xdaqtools/publisher/ItemList.h"

#include <stdexcept>
#include <sstream>

#include "xdata/AbstractBag.h"

#include "rpct/xdaqtools/publisher/Item.h"

namespace rpct {
namespace xdaqtools {
namespace publisher {

ItemList::const_iterator::const_iterator(ItemList const & il, size_t n)
    : il_(il)
    , n_(n)
    , change_(true)
{
    size_t size = il_.size();
    if (n_ > size)
        n_ = size;
    else if (n_ < 0)
        n_ = 0;
}

ItemList::const_iterator::const_iterator(const_iterator const & it)
    : il_(it.il_)
    , n_(it.n_)
    , change_(true)
{}

ItemList::const_iterator::~const_iterator()
{}

Item const & ItemList::const_iterator::operator*()
{
    if (n_ < il_.size())
        {
            if (change_)
                {
                    item_.setId(il_.id_[n_].value_);
                    item_.setType(il_.type_[n_].value_);
                    item_.setValue(il_.value_[n_].value_);

                    change_ = false;
                }
        }
    else
        throw std::out_of_range("dereferencing end const_iterator.");
    return item_;
}

Item const * ItemList::const_iterator::operator->()
{
    if (n_ < il_.size())
        {
            if (change_)
                {
                    item_.setId(il_.id_[n_].value_);
                    item_.setType(il_.type_[n_].value_);
                    item_.setValue(il_.value_[n_].value_);

                    change_ = false;
                }
            return &item_;
        }
    else
        return 0;
}

ItemList::ItemList(size_t reserved)
    : reserved_(reserved)
{
    this->reserve();
}
ItemList::~ItemList()
{}

void ItemList::registerFields(xdata::AbstractBag * bag)
{
    bag->addField("id", &id_);
    bag->addField("type", &type_);
    bag->addField("value", &value_);
}

void ItemList::reserve(size_t n)
{
    if (n)
        reserved_ = n;
    try {
        id_.reserve(reserved_);
        type_.reserve(reserved_);
        value_.reserve(reserved_);
    } catch(std::bad_alloc & e) {
    }
}

void ItemList::push_back(int id, int type, double value)
    throw (xdata::exception::Exception)
{
    size_t _size = this->check_size();
    try {
        id_.push_back(id);
        type_.push_back(type);
        value_.push_back(value);
    } catch (std::bad_alloc & e) {
        if (id_.size() > _size)
            {
                id_.pop_back();
                if (type_.size() > _size)
                    {
                        type_.pop_back();
                        if (value_.size() > _size)
                            value_.pop_back();
                    }
            }
        std::stringstream estr;
        estr << "the item could not be added, bad_alloc: " << e.what();
        XCEPT_RAISE(xdata::exception::Exception, estr.str());
    }
}

void ItemList::clear()
{
    id_.clear();
    type_.clear();
    value_.clear();

    this->reserve();
}

} // namespace publisher
} // namespace xdaqtools
} // namespace rpct
