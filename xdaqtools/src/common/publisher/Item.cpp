#include "rpct/xdaqtools/publisher/Item.h"

#include "xdata/AbstractBag.h"

namespace rpct {
namespace xdaqtools {
namespace publisher {

Item::Item(int id, int type, double value)
    : id_(id)
    , type_(type)
    , value_(value)
{}

Item::~Item()
{}

void Item::registerFields(xdata::AbstractBag * bag)
{
    bag->addField("id", &id_);
    bag->addField("type", &type_);
    bag->addField("value", &value_);
}

std::ostream & operator <<(std::ostream & os, const Item & item)
{
    os << "item(" << item.getId() << '.' << item.getType() << ':' << item.getValue() << ')';
    return os;
}

} // namespace publisher
} // namespace xdaqtools
} // namespace rpct
