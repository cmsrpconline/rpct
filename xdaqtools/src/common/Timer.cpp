#include "rpct/xdaqtools/Timer.h"

#include "log4cplus/loggingmacros.h"

namespace rpct {
namespace xdaqtools {

Timer::Timer(log4cplus::Logger & logger, const std::string & name)
    : toolbox::task::TimerListener()
    , logger_(logger.getInstance(logger.getName() + "." + name))
    , timer_(new toolbox::task::Timer(name))
{}

Timer::~Timer()
{
    delete timer_;
    clear();
}

void Timer::clear()
{
    std::map<std::string, TimerActionSignature *>::iterator itEnd = vtas_.end();
    for (std::map<std::string, TimerActionSignature *>::iterator it = vtas_.begin()
             ; it != itEnd
             ; ++it)
        delete it->second;
    vtas_.clear();
}

void Timer::timeExpired(toolbox::task::TimerEvent & e)
{
    LOG4CPLUS_TRACE(logger_, "Timer expired, event " << e.getTimerTask()->name);
    bool erased = false;
    TimerActionSignature * tas = 0;
    {
        rpct::tools::WLock wlock(rwmutex_);
        std::map<std::string, TimerActionSignature *>::iterator it = vtas_.find(e.getTimerTask()->name);
        if (it != vtas_.end())
            {
                tas = it->second;
                if (!(e.getTimerTask()->fixedRate))
                    {
                        vtas_.erase(it);
                        erased = true;
                    }
            }
    }
    if (tas)
        {
            tas->invoke(e);
            if (erased)
                delete tas;
        }
    else
        {
            LOG4CPLUS_WARN(logger_, "could not find action for event " << e.getTimerTask()->name);
        }
}

void Timer::scheduleAtFixedRate(TimerActionSignature & tas
                                , toolbox::TimeVal & start
                                , toolbox::TimeInterval & period
                                , void * context)
    throw (toolbox::task::exception::Exception)
{
    std::map<std::string, TimerActionSignature *>::iterator it;
    {
        rpct::tools::WLock wlock(rwmutex_);
        if (vtas_.find(tas.name()) != vtas_.end())
            {
                delete &tas;
                throw toolbox::task::exception::Exception
                    ("xcept::Exception"
                     , "Exception scheduling action" + tas.name() + ": timer with same name already exists."
                     , __FILE__, __LINE__, __FUNCTION__);
            }
        else
            it = (vtas_.insert(std::pair<std::string, TimerActionSignature *>(tas.name(), &tas))).first;
    }
    try {
        timer_->scheduleAtFixedRate(start, this, period, context, tas.name());
    } catch (toolbox::task::exception::Exception & e) {
        rpct::tools::WLock wlock(rwmutex_);
        it = vtas_.find(tas.name());
        if (it != vtas_.end())
            {
                delete it->second;
                vtas_.erase(it);
            }
        throw e;
    }
}

void Timer::schedule(TimerActionSignature & tas
                     , toolbox::TimeVal & time
                     , void * context)
    throw (toolbox::task::exception::Exception)
{
    std::map<std::string, TimerActionSignature *>::iterator it;
    {
        rpct::tools::WLock wlock (rwmutex_);
        if (vtas_.find(tas.name()) != vtas_.end())
            {
                delete &tas;
                throw toolbox::task::exception::Exception
                    ("xcept::Exception"
                     , "Exception scheduling action" + tas.name() + ": timer with same name already exists."
                     , __FILE__, __LINE__, __FUNCTION__);
            }
        else
            it = (vtas_.insert(std::pair<std::string, TimerActionSignature *>(tas.name(), &tas))).first;
    }
    try {
        timer_->schedule(this, time, context, tas.name());
    } catch (toolbox::task::exception::Exception & e) {
        rpct::tools::WLock wlock(rwmutex_);
        it = vtas_.find(tas.name());
        if (it != vtas_.end())
            {
                delete it->second;
                vtas_.erase(it);
            }
        throw e;
    }
}

void Timer::remove(const std::string & name)
    throw (toolbox::task::exception::Exception)
{
    {
        rpct::tools::WLock wlock (rwmutex_);
        std::map<std::string, TimerActionSignature *>::iterator it = vtas_.find(name);
        if (it != vtas_.end())
            {
                delete it->second;
                vtas_.erase(it);
            }
        else
            throw toolbox::task::exception::Exception
                ("xcept::Exception"
                 , "Error removing action: could not find action for  " + name
                 , __FILE__, __LINE__, __FUNCTION__);
    }
    timer_->remove(name);
}

void Timer::start()
    throw (toolbox::task::exception::Exception)
{
    timer_->start();
}

void Timer::stop()
    throw (toolbox::task::exception::Exception)
{
    try {
        timer_->stop();
        clear();
    } catch (toolbox::task::exception::Exception & e) {
        clear();
        throw e;
    }
}

bool Timer::isActive()
{
    return timer_->isActive();
}

} // namespace xdaqtools
} // namespace rpct
