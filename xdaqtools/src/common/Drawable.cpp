#include "rpct/xdaqtools/Drawable.h"

namespace rpct {
namespace xdaqtools {

void Drawable::printSVGDocument(xgi::Output & os, int depth) const
{
    os.getHTTPResponseHeader().addHeader("Content-Type", "image/svg+xml");
    os.getHTTPResponseHeader().addHeader("Expires", "-1");
    os.getHTTPResponseHeader().addHeader("Cache-Control", "no-cache");
    rpct::tools::Drawable::printSVGDocument(os, depth);
}

} // namespace xdaqtools
} // namespace rpct
