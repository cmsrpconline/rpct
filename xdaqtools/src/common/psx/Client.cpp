#include "rpct/xdaqtools/psx/Client.h"

#include "xdaq/NamespaceURI.h"
#include "xdaq/Application.h"
#include "xdaq/ApplicationDescriptor.h"

#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"

#include "rpct/xdaqtools/psx/const.h"

namespace rpct {
namespace xdaqtools {
namespace psx {

Client::Client(xdaq::Application & application
               , log4cplus::Logger const & logger
               , const std::string & psx_service_name
               , unsigned int psx_service_instance
               , const std::string & dcs_service_name
               , size_t max_size)
    : application_(application)
    , logger_(log4cplus::Logger::getInstance(logger.getName() + ".psx"))
    , psx_service_name_(psx_service_name)
    , psx_service_instance_(psx_service_instance)
    , dcs_service_name_(dcs_service_name)
    , max_size_(max_size)
    , timeout_(rpct::tools::RunningTimeVal::countdown_, preset::psx_timeout_)
{
    items_.reserve(max_size_);

    wl_action_ = new rpct::tools::ObjectAction<Item &, Client>(*this, &Client::process, "ClientAction");
    wl_send_ = new rpct::tools::Action<Client>(*this, &Client::send, "ClientSend");
    qwl_ = new xdaqtools::QueueWorkLoop<Item> ("ClientWorkLoop", wl_action_, 0, &timeout_, wl_send_);

    qwl_->start();
}

Client::~Client()
{
    delete qwl_;
    delete wl_send_;
    delete wl_action_;
}

void Client::publish(const Item & item)
{
    LOG4CPLUS_TRACE(logger_, "psx::Client::publish()");
    try {
        qwl_->submit(item);
    } catch(toolbox::task::exception::Exception & e) {
        LOG4CPLUS_ERROR(logger_, "psx::Client::publish() exception for " << item << ": " << e.what());
    }
}

int Client::process(Item & item)
{
    LOG4CPLUS_TRACE(logger_, "psx::Client::process()");

    try {
        items_.push_back(item);
    } catch(std::bad_alloc & e) {
        LOG4CPLUS_WARN(logger_, "Failed to add item to the item list; trying again after calling send(): " << e.what());
        this->send();
        try {
            items_.push_back(item);
        } catch(std::bad_alloc & e) {
            LOG4CPLUS_ERROR(logger_, "Failed to add item to the item list again; giving up: " << e.what());
        }
    }

    if (items_.size() >= max_size_)
        {
            LOG4CPLUS_DEBUG(logger_, "psx::Client::process() calls send()");
            this->send();
        }

    return 0;
}

int Client::send()
{
    LOG4CPLUS_TRACE(logger_, "psx::Client::send()");

    if (items_.size() > 0)
        {
            std::stringstream ssdpname;

            // Create SOAP message
            xoap::MessageReference message = xoap::createMessage();
            try {
                xoap::SOAPEnvelope env = message->getSOAPPart().getEnvelope();
                xoap::SOAPBody body = env.getBody();
                xoap::SOAPName command = env.createName("dpSet", "psx", "http://xdaq.cern.ch/xdaq/xsd/2006/psx-pvss-10.xsd");
                xoap::SOAPBodyElement command_el = body.addBodyElement(command);

                std::vector<Item>::const_iterator IitemEnd = items_.end();
                for (std::vector<Item>::const_iterator Iitem = items_.begin()
                         ; Iitem != IitemEnd
                         ; ++Iitem)
                    {
                        ssdpname.str("");
                        ssdpname << dcs_service_name_ << ":"
                                 << Iitem->getName() << "."
                                 << Iitem->getType() << ":_original.._value";
                        xoap::SOAPName dp_name =  env.createName("dp","psx","http://xdaq.cern.ch/xdaq/xsd/2006/psx-pvss-10.xsd");
                        xoap::SOAPName name_attr = env.createName("name","","");
                        xoap::SOAPElement dp_el = command_el.addChildElement(dp_name);
                        dp_el.addAttribute(name_attr, ssdpname.str());
                        dp_el.addTextNode(Iitem->getValue());
                    }
            } catch(xoap::exception::Exception & e) {
                LOG4CPLUS_ERROR(logger_, "Failed to create PSX SOAP message: " << e.what());
            } catch(...) {
                LOG4CPLUS_ERROR(logger_, "Failed to create PSX SOAP message.");
            }

            // Send SOAP message
            try {
                sendMessage(message);
            } catch (xdaq::exception::Exception & e) {
                LOG4CPLUS_INFO(logger_, "Failed to send SOAP message to PSX: " << e.what() << ", trying once more.");
                try {
                    sendMessage(message);
                } catch (xdaq::exception::Exception & e) {
                    LOG4CPLUS_ERROR(logger_, "Failed to send SOAP message to PSX: " << e.what());
                }
            }
        }

    items_.clear();
    timeout_.reset();
    return 0;
}

void Client::sendMessage(xoap::MessageReference & message)
{
	const xdaq::ApplicationDescriptor * orig = application_.getApplicationDescriptor();
	const xdaq::ApplicationDescriptor * dest = application_.getApplicationContext()->getDefaultZone()
        ->getApplicationDescriptor(psx_service_name_, psx_service_instance_);

    LOG4CPLUS_DEBUG(logger_, "psx::Client::sending...");
    xoap::MessageReference reply = application_.getApplicationContext()->postSOAP(message
                                                                                  , *orig
                                                                                  , *dest);
    LOG4CPLUS_DEBUG(logger_, "publisher::Client::sent.");

    xoap::SOAPEnvelope reply_env = reply->getDocument()->getDocumentElement();
    xoap::SOAPBody reply_body = reply_env.getBody();
    if (reply_body.hasFault())
        {
            LOG4CPLUS_ERROR(logger_, "Fault in PSX SOAP Message reply: " << reply_body.getFault().getFaultString());
        }
}

} // namespace psx
} // namespace xdaqtools
} // namespace rpct
