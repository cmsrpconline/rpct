#include "rpct/xdaqtools/psx/Item.h"

namespace rpct {
namespace xdaqtools {
namespace psx {

std::ostream & operator<<(std::ostream & os, const Item & item)
{
    os << item.getName() << ':' << item.getType() << ':' << item.getValue();
    return os;
}

} // namespace psx
} // namespace xdaqtools
} // namespace rpct
