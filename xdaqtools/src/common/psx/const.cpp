#include "rpct/xdaqtools/psx/const.h"

namespace rpct {
namespace xdaqtools {
namespace psx {
namespace preset {

const unsigned int psx_itemlist_size_ = 100;
const int psx_timeout_ = 30;

} // namespace preset
} // namespace psx
} // namespace xdaqtools
} // namespace rpct
