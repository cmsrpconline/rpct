<?xml version="1.0" encoding="iso-8859-2" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl" lang="pl">
<head>
	<title>HSB status</title>
	<style type="text/css">
	</style>
</head>
<body>
<%@ page import="rpct.db.service.tconhsb.*" %>
<%@ page import="javax.servlet.*" %>
<%@ page import="javax.servlet.http.*" %>
<%! 
	private DbServiceImplTcOnHSB resp = new DbServiceImplTcOnHSB();
	private boolean isOp(HttpServletRequest request, String s)
	{
		if (request.getParameter("op") != null)
			return ((String) request.getParameter("op")).equals(s);
		return false;
	};
%>
	<% if (isOp(request, "set_key") && request.getParameter("key") != null)
	{
		session.setAttribute("key", request.getParameter("key"));

	}

	%>
		
	<form action="" method="POST">
		<p> Select key:
		<select name="key">
			<%
			for (String k: resp.getLocalConfigKeys())
			{ %>
			<option value="<%= k %>"<% if (k.equals(session.getAttribute("key"))) { %> selected="selected" <% } %>> <%= k %></option>
			<% } %>

		</select> 
		<input type="hidden" name="op"  value="set_key" />
		<input type="submit" value="OK" />
		</p>
	</form>
	<hr />
	<% if (session.getAttribute("key") != null)
	{
		String ckey = (String) session.getAttribute("key");
		if (isOp(request, "set_tcs"))
			if (request.getParameter("user")!=null && !(request.getParameter("user").equals("")))
				resp.processTcsFlagSetting(request, ckey); 
			else {
			%>
				<p><font color="red"> Username is required to make changes</font></p>
			<% } %>
		<p>Key: <%= ckey %> </p>

		<p> <form action="" method="POST">
			<input type="hidden" name="op" value="set_tcs" />
			Username: <input type="text" name="user"  value="" /><br />
			<ul>
				<% for (int i=0; i<resp.numhsb; ++i) 
				{ %>
					<li><%= resp.getTcFlagOrError(i, ckey) %>TC_<%= i %></li>
				<% }  %>
			</ul>
			<input type="submit" value="Set" />
		</form> </p>
	<% } %>
</body>
</html>
