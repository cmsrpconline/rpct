/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rpct.db.service.enablelinks;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.log4j.Logger;

import rpct.datastream.TestOptionsXml;
import rpct.datastream.TestOptionsXml.BoardSource;
import rpct.datastream.TestOptionsXml.OpticalLink;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBoard;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.equipment.LinkConn;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.xdaqaccess.HardwareRegistry;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;

/**
 *
 * @author maciekz
 */
public class DbServiceImplEnableLinks {

    private HibernateContext context;
    private EquipmentDAO equipmentDAO;
    private ConfigurationDAO configurationDAO;
    private ConfigurationManager configurationManager;
    private boolean initialized = false;
    public final Logger logger = Logger.getLogger("DbServiceImplEnableLinks.java");

    private void init() throws DataAccessException {
        if (initialized)
            return;
        
        context = new SimpleHibernateContextImpl();
        equipmentDAO = new EquipmentDAOHibernate(context);
        configurationDAO = new ConfigurationDAOHibernate(context);
        configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);
        initialized = true;
    }

    private Map<String, List<LinkBox>> getLinkBoxesByTower() throws DataAccessException {
        init();
        return equipmentDAO.getLinkBoxesByTower(equipmentDAO, true);
    }

    public Set<String> getTowers() throws DataAccessException {
        return getLinkBoxesByTower().keySet();
    }

    public Set<LinkBox> allLinkBoxes() throws DataAccessException {
        Set<LinkBox> result = new HashSet<LinkBox>();
        for (List<LinkBox> l : getLinkBoxesByTower().values()) {
            result.addAll(l);
        }
        return result;
    }

    public void doRequest() throws DataAccessException {

        for (String tower : getTowers()) {
            boolean val = true;
            for (LinkBox lbx : getLinkBoxesByTower().get(tower)) {
                for (LinkBoard lb : lbx.getLinkBoards()) {
                    logger.info("Setting LB"+lb.getName()+" to state "+val);//+"on behalf of "+user+"."
                    if (lb.getType() != BoardType.LINKBOARD) {
                        throw new IllegalArgumentException("the board is not LinkBoard");
                    } else {
                        for (LinkConn linkConn : ((LinkBoard) lb).getLinkConns()) {
                            configurationManager.enableLinkConn(linkConn, val);
                        }
                    }
                }
            }
        }

    }
}
