/*
 *  Author: Michal Pietrusinski, William Whitacher & Giovanni Polese
 *  Version: $Id: test_XdaqRpcConnectivityTest.cpp,v 1.1 2008/11/21 11:39:42 tb Exp $ 
 *  
 */

#include "XdaqRpcConnectivityTest.h"

#include "xdaq/ApplicationGroup.h"

#include "xdata/soap/NamespaceURI.h"
#include "xdata/Serializable.h"
#include "xdata/exception/Exception.h"
#include "xdata/XStr.h"
#include "xdata/Float.h"
#include "xdata/Bag.h"
#include "xdata/Boolean.h"
#include "xdata/Integer.h"
#include "xdata/String.h"


#include "xoap/domutils.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "rpct/devices/System.h"
#include "rpct/devices/XmlSystemBuilder.h"
#include "rpct/xdaqutils/FebAccessInfo.h"

#include <log4cplus/configurator.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <time.h>
#include <string>
#include <vector>
#include <map>
#include <set>

using namespace std;
using namespace rpct;
using namespace rpct::xdaqutils;
using namespace rpct::xdaqdiagaccess;
using namespace cgicc;

XDAQ_INSTANTIATOR_IMPL(XdaqRpcConnectivityTest);

const char* XdaqRpcConnectivityTest::RPCT_RPC_CONFIG_NS = "urn:rpct-rpc-connectivityTest:1.0";
const char* XdaqRpcConnectivityTest::RPCT_RPC_CONFIG_PREFIX = "rrc";



XdaqRpcConnectivityTest::XdaqRpcConnectivityTest(xdaq::ApplicationStub * s) : 
  xdaq::Application(s),
  dbServiceClient(this), lboxAccessClient(this), diagAccessClient(this),
  str_addedESectors(""),str_addedBSectors(""),str_maskedBChambers(""),str_maskedEChambers("")
{
  
  
  LOG4CPLUS_INFO(getApplicationLogger(),"Start the RPC connectivity test!");
  xgi::bind(this,&XdaqRpcConnectivityTest::Default,"Default");
  xgi::bind(this,&XdaqRpcConnectivityTest::MaskForm, "MaskForm");
  xgi::bind(this,&XdaqRpcConnectivityTest::AddBSectors, "AddBSectors");
  xgi::bind(this,&XdaqRpcConnectivityTest::AddESectors, "AddESectors");
  xgi::bind(this,&XdaqRpcConnectivityTest::AddMaskedBChambers, "AddMaskedBChambers"); 
  xgi::bind(this,&XdaqRpcConnectivityTest::AddMaskedEChambers, "AddMaskedEChambers"); 
  xgi::bind(this,&XdaqRpcConnectivityTest::ConnectivityTest,"ConnectivityTest");
  xgi::bind(this,&XdaqRpcConnectivityTest::Reset, "Reset");
}



XdaqRpcConnectivityTest::~XdaqRpcConnectivityTest() {
  LOG4CPLUS_INFO(getApplicationLogger(),"Stopping the RPC connectivity test!");

}




void XdaqRpcConnectivityTest::Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
  LOG4CPLUS_INFO(getApplicationLogger(),"(re)creating default form");

  string maintitle = "RPC Connectivity test";
  
  *out << cgicc::HTMLDoctype(cgicc::HTMLDocType::eStrict) << std::endl;
  *out << cgicc::html().set("lang","en").set("dir","ltr") << std::endl;
  *out << cgicc::head() << std::endl;
  *out << cgicc::title(maintitle) << std::endl;
  *out << cgicc::style() << cgicc::comment() << std::endl;
  *out << "body{font-size:3;font-family:Arial, Times New Roman, Times, Serif;}" << std::endl;
  *out << "h1{text-align:center;font-size:4;color:#003366;font-weight:bold;}" << std::endl;
  *out << "td.ok{background-color:white;font-weight:normal;}" << std::endl;
  *out << "td.problem{background-color:red;font-weight:bold;}" << std::endl;
  *out << cgicc::comment() << cgicc::style() << std::endl;
  *out << cgicc::head() << std::endl;
  *out << cgicc::body() << std::endl;
  xgi::Utils::getPageHeader(
			    out, 
			    maintitle,
			    getApplicationDescriptor()->getContextDescriptor()->getURL(),
			    getApplicationDescriptor()->getURN(),
			    "/hyperdaq/images/Application.gif"
			    );
  
  *out << "  <font size=\"3\" face=\"Arial, Times New Roman, Times, serif\">"<< std::endl;
  
  //*************************************
  //*************ADDSECTORS**************
  //*************************************
  
  //*************start table, start first row***************************
  *out << cgicc::table().set("width", "600").set("align","center").set("border", "1").set("bordercolor", "#003366").set("rules","all")<< std::endl;
  *out << cgicc::tr().set("align","center")<< std::endl;
  //*******************************************************************************

  //*************start left column (barrel) start barrelform*********************************************
  std::string actionAddBSectors =
    toolbox::toString("/%s/AddBSectors",getApplicationDescriptor()->getURN().c_str());
  *out << cgicc::td().set("width","300")<<std::endl;
  *out << cgicc::form().set("method","POST").set("action", actionAddBSectors).set("enctype","multipart/form-data") << std::endl;
  //*************SELECTION WHEEL*******************

  *out <<"<table border=\"0\">"<<std::endl;
  *out <<"<tr><td></td><td>"<<std::endl;
  *out << "Wheel"<<std::endl;
  *out<<"</td><td>"<<std::endl;
  
  *out << cgicc::select().set("name","wheel").set("id","wheel")<<std::endl;
  *out<<cgicc::option().set("value","100").set("selected","selected")<<"ALL"<<cgicc::option()<<std::endl;
  for(int i=-2;i<=2;i++){
    *out<<"<option value=\""<<i<<"\">"<<i<<"</option>"<<std::endl;
  }
  *out << cgicc::select()<<std::endl;
  
  *out<<"</td></tr><tr><td>"<<std::endl;
  
  //************SELECTION TOWER OR SECTOR, tower********************************************
  *out << cgicc::input().set("type","radio").set("name","secOrTow").set("id","secOrTow").set("value","tow").set("checked","checked")<<std::endl;
  
  *out<<"</td><td>"<<std::endl;
  *out << "Tower" <<std::endl;
  *out<<"</td><td>"<<std::endl;

  *out << cgicc::select().set("name","tower").set("id","tower")<<std::endl;
  *out<<cgicc::option().set("value","100").set("selected","selected")<<"both"<<cgicc::option()<<std::endl;
  *out<<cgicc::option().set("value","near")<<"near"<<cgicc::option()<<std::endl;
  *out<<cgicc::option().set("value","far")<<"far"<<cgicc::option()<<std::endl;
  *out<<cgicc::select()<<std::endl;

  *out<<"</td></tr><tr><td>"<<std::endl;

  //************SELECTION TOWER OR SECTOR, sector********************************************
  *out << cgicc::input().set("type","radio").set("name","secOrTow").set("id","secOrTow").set("value","sec")<<std::endl;

  *out<<"</td><td>"<<std::endl;
  *out << "Sector"<<std::endl;
  *out<<"</td><td>"<<std::endl;

  *out << cgicc::select().set("name","sector").set("id","sector")<<std::endl;
  *out << cgicc::option().set("value","100").set("selected","selected")<<"all"<<cgicc::option()<<std::endl;
  for(int i=1;i<13;i++){
    *out<<"<option value=\""<<i<<"\">"<<i<<"</option>"<<std::endl;
  }
  *out<<cgicc::select()<<std::endl;

  *out<<"</td></tr></table>"<<std::endl;

  //************button, end barrelform, end left column************************************************************
  *out << cgicc::p() <<  cgicc::input().set("type", "submit").set("align","center").set("name", "send").set("value", "Add Barrel Sectors")<< cgicc::p() << std::endl;
  *out << cgicc::form() << std::endl;
  *out << cgicc::td()<< std::endl;



  //*************start right column (endcap) start endcapform*********************************************
  std::string actionAddESectors =
    toolbox::toString("/%s/AddESectors",getApplicationDescriptor()->getURN().c_str());
  *out << cgicc::td().set("width","300")<<std::endl;
  *out << cgicc::form().set("method","POST").set("action", actionAddESectors).set("enctype","multipart/form-data") << std::endl;
  //*************SELECTION WHEEL*******************

  *out <<"<table border=\"0\">"<<std::endl;
  *out <<"<tr><td></td><td>"<<std::endl;
  *out << "Disc"<<std::endl;
  *out<<"</td><td>"<<std::endl;

  *out << cgicc::select().set("name","wheel").set("id","wheel")<<std::endl;
  *out<<cgicc::option().set("value","100").set("selected","selected")<<"ALL"<<cgicc::option()<<std::endl;
  for(int i=-3;i<=3;i++){
    if(i==0) continue;
    if (i==1) 
      *out<<"<option value=\""<<i<<"\" selected=\"selected\" >"<<i<<"</option>"<<std::endl;
    else
      *out<<"<option value=\""<<i<<"\">"<<i<<"</option>"<<std::endl;
  }
  *out << cgicc::select()<<std::endl;

  *out<<"</td></tr><tr><td>"<<std::endl;

  //************SELECTION TOWER OR SECTOR, tower********************************************
  *out << cgicc::input().set("type","radio").set("name","secOrTow").set("id","secOrTow").set("value","tow").set("checked","checked")<<std::endl;

  *out<<"</td><td>"<<std::endl;
  *out << "Tower" <<std::endl;
  *out<<"</td><td>"<<std::endl;

  *out << cgicc::select().set("name","tower").set("id","tower")<<std::endl;
  *out<<cgicc::option().set("value","100").set("selected","selected")<<"both"<<cgicc::option()<<std::endl;
  *out<<cgicc::option().set("value","near")<<"near"<<cgicc::option()<<std::endl;
  *out<<cgicc::option().set("value","far")<<"far"<<cgicc::option()<<std::endl;
  *out<<cgicc::select()<<std::endl;

  *out<<"</td></tr><tr><td>"<<std::endl;

  //************SELECTION TOWER OR SECTOR, sector********************************************
  *out << cgicc::input().set("type","radio").set("name","secOrTow").set("id","secOrTow").set("value","sec")<<std::endl;

  *out<<"</td><td>"<<std::endl;
  *out << "Sector"<<std::endl;
  *out<<"</td><td>"<<std::endl;

  *out << cgicc::select().set("name","sector").set("id","sector")<<std::endl;
  *out << cgicc::option().set("value","100").set("selected","selected")<<"all"<<cgicc::option()<<std::endl;
  for(int i=1;i<=36;i++){
    *out<<"<option value=\""<<i<<"\">"<<i<<"</option>"<<std::endl;
  }
  *out<<cgicc::select()<<std::endl;
  *out<<"</td></tr><tr><td>"<<std::endl;

  *out << cgicc::input().set("type","radio").set("name","secOrTow").set("id","secOrTow").set("value","lsec")<<std::endl;

  *out<<"</td><td>"<<std::endl;
  *out << "Link-sector"<<std::endl;
  *out<<"</td><td>"<<std::endl;

  *out << cgicc::select().set("name","lsector").set("id","lsector")<<std::endl;
  *out << cgicc::option().set("value","100").set("selected","selected")<<"all"<<cgicc::option()<<std::endl;
  for(int i=1;i<=12;i++){
    *out<<"<option value=\""<<i<<"\">"<<i<<"</option>"<<std::endl;
  }
  *out<<cgicc::select()<<std::endl;

  *out<<"</td></tr></table>"<<std::endl;

  //************button, end endcapform, end right column************************************************************
  *out << cgicc::p() <<  cgicc::input().set("type", "submit").set("align","center").set("name", "send").set("value", "Add Endcap Sectors")<< cgicc::p() << std::endl;
  *out << cgicc::form() << std::endl;
  *out << cgicc::td()<< std::endl;
  //***********************************************************************
  *out << cgicc::tr()<<std::endl;
  *out << cgicc::table()<<std::endl;



  //************************************************
  //**************LIST OF ADDED SECTORS************
  //*************LIST OF MASKED CHAMBERS************
  //************************************************
  if(str_maskedBChambers !="" || str_addedBSectors !="" || str_maskedEChambers !="" || str_addedESectors !=""){
    *out <<cgicc::br();
    *out << cgicc::table().set("width", "600").set("align","center").set("border", "1").set("bordercolor", "#003366").set("rules","all")<< std::endl;
    *out << cgicc::tr().set("align","center")<< std::endl;

    *out << cgicc::th().set("width", "300").set("cellspacing", "2").set("align","center").set("cellpadding","5")<<std::endl;
    *out << "Barrel sectors in test"<<std::endl;
    *out << cgicc::th() <<std::endl;

    *out << cgicc::th().set("width", "300").set("cellspacing", "2").set("align","center").set("cellpadding","5")<<std::endl;
    *out << "Endcap sectors in test"<<std::endl;
    *out << cgicc::th() <<std::endl;

    *out << cgicc::tr()<< std::endl;  
    *out << cgicc::tr().set("align","left")<<std::endl;

    *out << cgicc::td()<<std::endl;
    *out << cgicc::p()<<"<ul>"<<str_addedBSectors<<"</ul>"<<cgicc::p()<<std::endl;
    *out << cgicc::td()<<std::endl;

    *out << cgicc::td()<<std::endl;
    *out << cgicc::p()<<"<ul>"<<str_addedESectors<<"</ul>"<<cgicc::p()<<std::endl;
    *out << cgicc::td()<<std::endl;

    *out << cgicc::tr()<< std::endl;  
    *out << cgicc::tr().set("align","left")<<std::endl;

    *out << cgicc::th().set("width", "300").set("cellspacing", "2").set("align","center").set("cellpadding","5")<<std::endl;
    *out << "Barrel Chambers to mask"<<std::endl;
    *out << cgicc::th() <<std::endl;

    *out << cgicc::th().set("width", "300").set("cellspacing", "2").set("align","center").set("cellpadding","5")<<std::endl;
    *out << "Endcap Chambers to mask"<<std::endl;
    *out << cgicc::th() <<std::endl;

    *out << cgicc::tr()<< std::endl;  
    *out << cgicc::tr().set("align","left")<<std::endl;

    *out << cgicc::td()<<std::endl;
    *out << cgicc::p()<<"<ul>"<<str_maskedBChambers<<"</ul>"<<cgicc::p()<<std::endl;
    *out << cgicc::td()<<std::endl;

    *out << cgicc::td()<<std::endl;
    *out << cgicc::p()<<"<ul>"<<str_maskedEChambers<<"</ul>"<<cgicc::p()<<std::endl;
    *out << cgicc::td()<<std::endl;

    *out << cgicc::tr()<<std::endl;
    *out << cgicc::table() <<std::endl;
  }
  else
    LOG4CPLUS_INFO(getApplicationLogger(),"no");


  //*************************************************************
  //*************buttons to mask form and to reset**************
  //*************************************************************


  //*****************start table and row ***********************
  *out << cgicc::br()<<std::endl;
  *out << cgicc::table().set("width", "600").set("align","center").set("border", "1").set("bordercolor", "#003366").set("rules","all").set("cellpadding","5")<< std::endl;
  *out << cgicc::tr().set("align","center")<<std::endl;

  //******************maskform**********************************
  *out << cgicc::td().set("width","300")<<std::endl;
  std::string actionToMaskForm =
    toolbox::toString("/%s/MaskForm",getApplicationDescriptor()->getURN().c_str());
  *out << cgicc::form().set("method","POST").set("action", actionToMaskForm).set("enctype","multipart/form-data") << std::endl;
  *out << cgicc::input().set("type", "submit").set("align","center").set("name", "send").set("value", "To mask form")<<std::endl;
  *out << cgicc::form() <<std::endl;
  *out << cgicc::td() <<std::endl;

  //*****************resetform*********************************
  *out << cgicc::td().set("width","300")<<std::endl;
  std::string actionReset =
    toolbox::toString("/%s/Reset",getApplicationDescriptor()->getURN().c_str());
  *out << cgicc::form().set("method","POST").set("action", actionReset).set("enctype","multipart/form-data") << std::endl;
  *out << cgicc::input().set("type", "submit").set("align","center").set("name", "send").set("value", "Reset")<<std::endl;
  *out << cgicc::form() <<std::endl;
  *out << cgicc::td() <<std::endl;
  //******************end table and row***********************
  *out << cgicc::tr() <<std::endl;
  *out << cgicc::table() << std::endl;


  *out << cgicc::br();
  //****************************************
  //*******STARTTest FORM*******************
  //****************************************

  //*************begin table, begin form*************************************
  *out << cgicc::table().set("width", "600").set("align","center").set("border", "1").set("bordercolor", "#003366").set("rules","groups").set("cellpadding","5")<< std::endl;
  std::string actionScan =
    toolbox::toString("/%s/ConnectivityTest",getApplicationDescriptor()->getURN().c_str());
  *out << cgicc::form().set("method","POST").set("action", actionScan).set("enctype","multipart/form-data") << std::endl;
  *out << cgicc::tr().set("align","center")<< std::endl;

  //*************Selection Starting Threshold**********************************************
  *out << cgicc::td().set("width", "300").set("align","center")<<std::endl;
  *out << "  High Threshold Value"<< std::endl;
  *out << "  <select name=\"fthr\" size=\"1\" id=\"fthr\">" << std::endl;
  *out << "    <option value=\"400\">400</option>" << std::endl;
  *out << "    <option value=\"300\">300</option>" << std::endl;
  *out << "    <option value=\"250\">250</option>" << std::endl;
  *out << "    <option value=\"200\">200</option>" << std::endl;
  *out << cgicc::td() << std::endl;
  //*************Selection Ending Threshold************************************************
  *out << cgicc::td().set("width", "300").set("align","center")<<std::endl;
  *out << "  </select> " << std::endl;  
  *out << "  Low Threshold Value"<< std::endl;
  *out << "  <select name=\"lthr\" size=\"1\" id=\"lthr\">" << std::endl;
  *out << "    <option value=\"60\">60</option>" << std::endl;
  *out << "    <option value=\"50\">50</option>" << std::endl;
  *out << "    <option value=\"40\" selected=\"selected\" >40</option>" << std::endl;
  *out << "    <option value=\"30\">30</option>" << std::endl;
  *out << td() << std::endl;
  //*************Selection Step Threshold
  /*
  *out << "  </select> " << std::endl;  
  *out << "  <font size=\"4\" face=\"Times New Roman, Times, serif\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Step Thr</font>"<< std::endl;
  *out << "  <select name=\"sthr\" size=\"1\" id=\"sthr\">" << std::endl;
  *out << "    <option value=\"20\">20</option>" << std::endl;
  *out << "    <option value=\"30\">30</option>" << std::endl;
  *out << "    <option value=\"40\">40</option>" << std::endl;
  *out << "    <option value=\"50\">50</option>" << std::endl;
  *out << "  </select>" <<std::endl;
  */
  
  //*************Button***********************************************
  *out << cgicc::tr();
  *out << cgicc::tr();
  *out << cgicc::td().set("width", "600").set("align","center").set("colspan","2")<<std::endl;
  *out <<cgicc::input().set("type", "submit").set("align","center").set("name", "send").set("value", "Start the Connectivity Test")<<std::endl;
  //*************TITELS, LAYOUT AND MAIN TAGS***********************************************
  *out << cgicc::td()<<std::endl;
  *out << cgicc::tr()<<std::endl;
  *out << cgicc::form()<<std::endl;
  *out << cgicc::table()<<std::endl;
  //***************************************************************************
  //***************************************************************************
  
  
  
  //*************end font, body, html *******************************
  *out << "</font>" << std::endl;
  xgi::Utils::getPageFooter(*out);
  *out << body() << std::endl;
  *out << html() << std::endl;
}

void XdaqRpcConnectivityTest::MaskForm(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
  xgi::Utils::getPageHeader(
			    out, 
			    "RPC Connectivity test",
			    getApplicationDescriptor()->getContextDescriptor()->getURL(),
			    getApplicationDescriptor()->getURN(),
			    "/hyperdaq/images/Application.gif"
			    );
  *out <<cgicc::h1().set("align","center").set("font size","4") << "Mask Chambers" << cgicc::h1()<< std::endl;	 
  *out << "  <font size=\"3\" face=\"Arial, Times New Roman, Times, serif\">"<< std::endl;
  
  
  //************start table, start first row ****************************************************
  *out << cgicc::table().set("width", "600").set("align","center").set("border", "1").set("bordercolor", "#003366").set("rules","all").set("cellpadding","5")<< std::endl;
  *out << cgicc::tr().set("align","center")<<std::endl;
  //***********start left column (barrel), start form
  *out << cgicc::td().set("width", "300").set("cellspacing", "2").set("align","center")<<std::endl;  
  std::string actionAddMaskedBChamber =
    toolbox::toString("/%s/AddMaskedBChambers",getApplicationDescriptor()->getURN().c_str());
  *out << cgicc::form().set("action",actionAddMaskedBChamber).set("method","POST")<<std::endl;
  //*************SELECTION WHEEL*******************

  *out <<"<table border=\"0\"><tr><td>"<<std::endl;
  *out << "wheel"<<std::endl;
  *out <<"</td><td>"<<std::endl;

  *out << cgicc::select().set("name","wheel").set("id","wheel")<<std::endl;
  *out<<"<option value=\"-2\" selected=\"selected\">-2</option>"<<std::endl;
  for(int i=-1;i<3;i++){
    *out<<"<option value=\""<<i<<"\">"<<i<<"</option>"<<std::endl;
  }
  *out<<cgicc::select()<<std::endl;

  *out<<"</td></tr><tr><td>"<<std::endl;

  //************SELECTION SECTOR******************
  *out << "sector"<<std::endl;
  *out <<"</td><td>"<<std::endl;

  *out << cgicc::select().set("name","sector").set("id","sector")<<std::endl;
  *out<<"<option value=\"1\" selected=\"selected\">1</option>"<<std::endl;
  for(int i=2;i<13;i++){
    *out<<"<option value=\""<<i<<"\">"<<i<<"</option>"<<std::endl;
  }
  *out<<cgicc::select()<<std::endl;

  *out<<"</td></tr><tr><td>"<<std::endl;

  //************SELECTION RB***********************
  *out << "rb"<<std::endl;
  *out <<"</td><td>"<<std::endl;

  *out << cgicc::select().set("name","rb").set("id","rb")<<std::endl;
  std::vector<string> stringV;
  stringV.push_back("1out");
  stringV.push_back("2in");
  stringV.push_back("2out");
  stringV.push_back("3");
  stringV.push_back("4");
  *out<<"<option value=\"1in\" selected=\"selected\">1in</option>"<<std::endl;
  for(int i=0;i<5;i++){
    *out<<cgicc::option().set("value",stringV[i])<<stringV[i]<<cgicc::option()<<std::endl;
  }
  *out<<cgicc::select()<<std::endl;

  *out<<"</td></tr><tr><td>"<<std::endl;

  //************SELECTION SUBSECTOR***********************
  *out << "subsector"<<std::endl;
  *out <<"</td><td>"<<std::endl;

  *out << cgicc::select().set("name","sSector").set("id","sSector")<<std::endl;
  *out<<cgicc::option().set("value","").set("selected","selected")<<""<<cgicc::option()<<std::endl;
  stringV.clear();
  stringV.push_back("-");
  stringV.push_back("+");
  stringV.push_back("--");
  stringV.push_back("-+");
  stringV.push_back("+-");
  stringV.push_back("++");
  for(int i=0;i<6;i++){
    *out<<cgicc::option().set("value",stringV[i])<<stringV[i]<<cgicc::option()<<std::endl;
  }
  *out<<cgicc::select()<<std::endl;

  *out<<"</td></tr></table>"<<std::endl;

  //*******BUTTON*************************
  *out << cgicc::p();
  *out <<cgicc::input().set("type", "submit").set("align","center").set("name", "send").set("value", "mask barrel chamber");
  *out << cgicc::p();
  //*******end form, end left column (barrel)****************************************
  *out << cgicc::form()<<std::endl;
  *out << cgicc::td()<< std::endl;



  //***********start right column (endcap), start form
  *out << cgicc::td().set("width","300")<<std::endl;
  std::string actionAddMaskedEChamber =
    toolbox::toString("/%s/AddMaskedEChambers",getApplicationDescriptor()->getURN().c_str());
  *out << cgicc::form().set("action",actionAddMaskedEChamber).set("method","POST")<<std::endl;
  //***********selection disc*********************************8

  *out << "<table border=\"0\">"<<std::endl;
  *out << "<tr>" << std::endl;
  *out << "<td></td>"<<std::endl;
  *out << "<td>disc</td>"<<std::endl;

  *out <<" <td>"<<std::endl;
  *out << cgicc::select().set("name","wheel").set("id","wheel")<<std::endl;
  for(int i=-3;i<=3;i++){
    if (i==0)continue;
    if (i==1)
      *out<<"<option value=\"" << i << "\" selected=\"selected\" >" << i << "</option()>"<<std::endl;
    else
      *out<<"<option value=\"" << i << "\" >" << i << "</option()>"<<std::endl;
      
  }
  *out << cgicc::select()<<std::endl;
  *out << "</td>" << std::endl;
  *out << "</tr>" << std::endl;

  //************SELECTION SECTOR******************
  *out << "<tr>" << std::endl;
  *out << "<td>" ;
  *out << cgicc::input().set("type","radio").set("name","secOrLsec").set("id","secOrLsec").set("value","sec").set("checked","checked")<<std::endl;
  *out << "</td>" << std::endl;
  *out << "<td>sector</td>"<<std::endl;

  *out << "<td>"<<std::endl;
  *out << cgicc::select().set("name","sector").set("id","sector")<<std::endl;
  *out <<"<option value=\"1\" selected=\"selected\" >1</option>"<<std::endl;
  for(int i=2;i<=36;i++){
    *out<<"<option value=\"" << i << "\" >" << i << "</option>"<<std::endl;
  }
  *out << cgicc::select()<<std::endl;
  *out << "</td>" << std::endl;
  *out << "</tr>" << std::endl;

  //************SELECTION LINKSECTOR******************
  *out << "<tr>" << std::endl;
  *out << "<td>" ;
  *out << cgicc::input().set("type","radio").set("name","secOrLsec").set("id","secOrLsec").set("value","lsec")<<std::endl;
  *out << "</td>" << std::endl;
  *out << "<td>link-sector</td>"<<std::endl;

  *out << "<td>";
  *out << cgicc::select().set("name","lsector").set("id","lsector")<<std::endl;
  *out <<"<option value=\"1\" selected=\"selected\" >1</option>"<<std::endl;
  for(int i=2;i<13;i++){
    *out<<"<option value=\"" << i << "\" >" << i << "</option>"<<std::endl;
  }
  *out << cgicc::select()<<std::endl;
  *out << "</td>" << std::endl;
  *out << "</tr>" << std::endl;


  //************SELECTION RING(rb)***********************
  *out << "<tr>" << std::endl;
  *out << "<td></td>"<<std::endl;
  *out << "<td>layer</td>"<<std::endl; // layer instead of ring for consistency with rpcConfig

  *out << "<td>";
  *out << cgicc::select().set("name","rb").set("id","rb")<<std::endl;  
  *out << "<option value=\"1\" >1</option>"<<std::endl;
  *out << "<option value=\"2\" selected=\"selected\" >2</option>"<<std::endl;
  *out << "<option value=\"3\" >3</option>"<<std::endl;
  *out << cgicc::select()<<std::endl;
  *out << cgicc::input().set("type","hidden").set("name","sSector").set("id","sSector").set("value","");

  *out << "</td>" << std::endl;
  *out << "</tr></table>"<<std::endl;

  //*******BUTTON*************************
  *out << cgicc::p();
  *out <<cgicc::input().set("type", "submit").set("align","center").set("name", "send").set("value", "mask endcap chamber");
  *out << cgicc::p();
  //*******end form, end right column (endcap)****************************************
  *out << cgicc::form()<<std::endl;
  *out << cgicc::td()<< std::endl;


  //*******end first row, table********************
  *out << cgicc::tr()<<std::endl;
  *out << cgicc::table();


  //************************************************
  //**************LIST OF ADDED SECTORS************
  //*************LIST OF MASKED CHAMBERS************
  //************************************************
  if(str_maskedBChambers !="" || str_addedBSectors !="" || str_maskedEChambers !="" || str_addedESectors !=""){
    *out <<cgicc::br();
    *out << cgicc::table().set("width", "600").set("align","center").set("border", "1").set("bordercolor", "#003366").set("rules","all")<< std::endl;
    *out << cgicc::tr().set("align","center")<< std::endl;

    *out << cgicc::td().set("width", "300").set("cellspacing", "2").set("align","center")<<std::endl;
    *out << cgicc::p() << std::endl;
    *out << "Barrel sectors in test"<<std::endl;
    *out << cgicc::p() << std::endl;
    *out << cgicc::td() <<std::endl;

    *out << cgicc::td().set("width", "300").set("cellspacing", "2").set("align","center")<<std::endl;
    *out << cgicc::p() << std::endl;
    *out << "Endcap sectors in test"<<std::endl;
    *out << cgicc::p() << std::endl;
    *out << cgicc::td() <<std::endl;

    *out << cgicc::tr()<< std::endl;  
    *out << cgicc::tr().set("align","left")<<std::endl;

    *out << cgicc::td()<<std::endl;
    *out << cgicc::p()<<"<ul>"<<str_addedBSectors<<"</ul>"<<cgicc::p()<<std::endl;
    *out << cgicc::td()<<std::endl;

    *out << cgicc::td()<<std::endl;
    *out << cgicc::p()<<"<ul>"<<str_addedESectors<<"</ul>"<<cgicc::p()<<std::endl;
    *out << cgicc::td()<<std::endl;

    *out << cgicc::tr()<< std::endl;  
    *out << cgicc::tr().set("align","left")<<std::endl;

    *out << cgicc::td().set("width", "300").set("cellspacing", "2").set("align","center")<<std::endl;
    *out << cgicc::p() << std::endl;
    *out << "Barrel Chambers to mask"<<std::endl;
    *out << cgicc::p() << std::endl;
    *out << cgicc::td() <<std::endl;

    *out << cgicc::td().set("width", "300").set("cellspacing", "2").set("align","center")<<std::endl;
    *out << cgicc::p() << std::endl;
    *out << "Endcap Chambers to mask"<<std::endl;
    *out << cgicc::p() << std::endl;
    *out << cgicc::td() <<std::endl;

    *out << cgicc::tr()<< std::endl;  
    *out << cgicc::tr().set("align","left")<<std::endl;

    *out << cgicc::td()<<std::endl;
    *out << cgicc::p()<<"<ul>"<<str_maskedBChambers<<"</ul>"<<cgicc::p()<<std::endl;
    *out << cgicc::td()<<std::endl;

    *out << cgicc::td()<<std::endl;
    *out << cgicc::p()<<"<ul>"<<str_maskedEChambers<<"</ul>"<<cgicc::p()<<std::endl;
    *out << cgicc::td()<<std::endl;

    *out << cgicc::tr()<<std::endl;
    *out << cgicc::table() <<std::endl;
  }


  //*************************************************************
  //*************buttons to mask form and to reset**************
  //*************************************************************


  //*****************start table and row ***********************
  *out << cgicc::br()<<std::endl;
  *out << cgicc::table().set("width", "600").set("align","center").set("border", "1").set("bordercolor", "#003366").set("rules","all").set("cellpadding","5")<< std::endl;
  *out << cgicc::tr().set("align","center")<<std::endl;

  //******************maskform**********************************
  *out << cgicc::td().set("width","300")<<std::endl;
  std::string actionToDefault =
    toolbox::toString("/%s/Default",getApplicationDescriptor()->getURN().c_str());
  *out << cgicc::form().set("method","POST").set("action", actionToDefault).set("enctype","multipart/form-data") << std::endl;
  *out << cgicc::input().set("type", "submit").set("align","center").set("name", "send").set("value", "Back to start form")<<std::endl;
  *out << cgicc::form() <<std::endl;
  *out << cgicc::td() <<std::endl;

  //*****************resetform*********************************
  *out << cgicc::td().set("width","300")<<std::endl;
  std::string actionReset =
    toolbox::toString("/%s/Reset",getApplicationDescriptor()->getURN().c_str());
  *out << cgicc::form().set("method","POST").set("action", actionReset).set("enctype","multipart/form-data") << std::endl;
  *out << cgicc::input().set("type", "submit").set("align","center").set("name", "send").set("value", "Reset")<<std::endl;
  *out << cgicc::form() <<std::endl;
  *out << cgicc::td() <<std::endl;
  //******************end table and row***********************
  *out << cgicc::tr() <<std::endl;
  *out << cgicc::table() << std::endl;

  //*****end font, body, html****************************************************************
  *out << "</font>"<<std::endl;
  xgi::Utils::getPageFooter(*out);
}


void XdaqRpcConnectivityTest::AddBSectors(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
  try
    {

      LOG4CPLUS_INFO(getApplicationLogger(),"Adding a Barrel Sector");
 
      XdaqDbServiceClient::BarrelOrEndcap boe = XdaqDbServiceClient::BARREL;
      string wheeltype = "wheel";
      int wheel=-2;
      string secOrTow="tow";
      string tower="near";
      int sector=10;
      int lsector=0;
      
      this->GetPostData(in,wheel,secOrTow,tower,sector,lsector);
      this->AddSectorsToString(str_addedBSectors,wheeltype,wheel,secOrTow,tower,sector, lsector);
      this->AddSectorsToFAIVV(boe,wheel,secOrTow,tower,sector, lsector);
      this->Default(in,out);
    }
  catch (xdaq::exception::Exception& e) {
    LOG4CPLUS_ERROR(getApplicationLogger(), "Error");
    XCEPT_RETHROW(xgi::exception::Exception, "Cannot send message", e);
  }
}

void XdaqRpcConnectivityTest::AddESectors(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
  try
    {
      
      LOG4CPLUS_INFO(getApplicationLogger(),"Adding a Endcap sector");

      XdaqDbServiceClient::BarrelOrEndcap boe = XdaqDbServiceClient::ENDCAP;
      string wheeltype = "disc";
      int wheel=-2;
      string secOrTow="tow";
      string tower="near";
      int sector=10;
      int lsector=0;
      
      this->GetPostData(in,wheel,secOrTow,tower,sector,lsector);
      this->AddSectorsToString(str_addedESectors,wheeltype,wheel,secOrTow,tower,sector, lsector);
      this->AddSectorsToFAIVV(boe,wheel,secOrTow,tower,sector,lsector);
      this->Default(in,out);
    }

  catch (xdaq::exception::Exception& e) {
    LOG4CPLUS_ERROR(getApplicationLogger(), "Error");
    XCEPT_RETHROW(xgi::exception::Exception, "Cannot send message", e);
  }
}


void XdaqRpcConnectivityTest::GetPostData(xgi::Input * in,int& wheel,string& secOrTow,string& tower,int& sector, int& lsector){
  try
    {
      LOG4CPLUS_INFO(getApplicationLogger(),"Getting the form data");

      Cgicc formData(in);      
      
      form_iterator iWheel  = formData.getElement("wheel");
      form_iterator iSecOrTow = formData.getElement("secOrTow");
      form_iterator iTower  = formData.getElement("tower");
      form_iterator iSector = formData.getElement("sector");
      form_iterator iLsector = formData.getElement("lsector");
      
      if (iWheel != formData.getElements().end() &&	  
	  iSecOrTow!= formData.getElements().end() &&
	  iSector != formData.getElements().end() &&
	  iTower != formData.getElements().end()){
	wheel = iWheel->getIntegerValue();
	secOrTow = iSecOrTow->getValue();
	tower  = iTower->getValue();	
	sector  = iSector->getIntegerValue();
      }
      if (iLsector != formData.getElements().end())
	lsector = iLsector->getIntegerValue();

      return;
    }
  catch (xdaq::exception::Exception& e) {
    LOG4CPLUS_ERROR(getApplicationLogger(), "Error");
    XCEPT_RETHROW(xgi::exception::Exception, "Cannot send message", e);
  }
}


void XdaqRpcConnectivityTest::AddSectorsToString(std::string& str_addedSectors,std::string wheeltype,int wheel,
					      std::string secOrTow,std::string tower,int sector,int lsector){
  try{
    
    LOG4CPLUS_INFO(getApplicationLogger(),"Adding new sectors to string");

    stringstream strstr;
    
    if(wheel==100){
      strstr <<"<li>all "<<wheeltype<<"s";
    }
    else{
      strstr <<"<li>"<<wheeltype<<" "<<wheel;
    }
    if(secOrTow=="sec"){
      if(sector==100)
	strstr <<", all sectors</li>";
      else{
	strstr <<", sector "<<sector<<"</li>";
      }
    }
    else if (secOrTow=="lsec"){
      if(lsector==100)
	strstr <<", all linksectors</li>";
      else{
	strstr <<", linksector "<<lsector<<"</li>";
      }
    }
    else{
      if(tower=="100")
	strstr <<", both towers</li>";
      else{
	strstr <<", "<<tower<<" tower"<<"</li>";
      }
    }

    str_addedSectors += strstr.str();
    return;
  }
  
  catch (xdaq::exception::Exception& e) {
    LOG4CPLUS_ERROR(getApplicationLogger(), "Error");
    XCEPT_RETHROW(xgi::exception::Exception, "Cannot send message", e);
  }
}



void XdaqRpcConnectivityTest::AddSectorsToFAIVV(XdaqDbServiceClient::BarrelOrEndcap boe,int wheel,string secOrTow,
					    string tower,int sector, int lsector) 
{
  try{
    LOG4CPLUS_INFO(getApplicationLogger(),"Adding new sectors to FAIV Vector");
    

    FAIV* febAccessInfoVector;
    std::vector<int> wheelV;
    std::vector<int> sectorV;
    
    if (boe==XdaqDbServiceClient::BARREL){
      if(wheel==100)
	for(int i=-2;i<=2;i++)
	  wheelV.push_back(i);
      else
	wheelV.push_back(wheel);
      
      if ((secOrTow=="sec" && sector==100)||(secOrTow=="tow" && tower=="100"))
	for(int i=1;i<13;i++)
	  sectorV.push_back(i);
      else{
	if(secOrTow=="tow"){
	  if(tower=="near"){
	    sectorV.push_back(3);
	    sectorV.push_back(2);
	    sectorV.push_back(1);
	    sectorV.push_back(12);
	    sectorV.push_back(11);
	    sectorV.push_back(10);
	  }
	  else if(tower=="far"){
	    sectorV.push_back(4);
	    sectorV.push_back(5);
	    sectorV.push_back(6);
	    sectorV.push_back(7);
	    sectorV.push_back(8);
	    sectorV.push_back(9);
	  }
	}
	else
	  sectorV.push_back(sector);
      }
      
    }
    else{
      wheelV.push_back(wheel);
      int chambersector = 36;
      if (wheel==2 || wheel==-2)
	chambersector = 2;

      if ((secOrTow=="sec" && sector==100)||(secOrTow=="tow" && tower=="100"))
        for(int i=1;i<=36;i++)
          sectorV.push_back(i);
      else{
        if(secOrTow=="tow"){
          if(tower=="near"){
            for (int i=0;i<18;i++){
              if (chambersector >36)
		chambersector %= 36;
              sectorV.push_back(chambersector++);
	      if (i==9) chambersector += 18;
	    }
	  }
	  else if(tower=="far"){
	    chambersector += 9;
            for (int i=0;i<18;i++){
              if (chambersector >36)
		chambersector %= 36;
              sectorV.push_back(chambersector++);
	    }
	  }
	}
	else if (secOrTow=="lsec"){
	  chambersector += (lsector-1)*3;
	  for (int i=0;i<3;i++){
	    if (chambersector >36)
	      chambersector %= 36;
	    sectorV.push_back(chambersector++);
	  }
	}
	else
	  sectorV.push_back(sector);
      }
    }
  

    LOG4CPLUS_INFO(getApplicationLogger(),"wheelV and sectorV filled");

    for (std::vector<int>::iterator iw=wheelV.begin();iw<wheelV.end();iw++){
      for (std::vector<int>::iterator is=sectorV.begin();is<sectorV.end();is++){
	febAccessInfoVector = dbServiceClient.getFebsBySector(*iw,*is,boe);
	FAIVsecVector.push_back(*febAccessInfoVector);
      }
    }
    return;
  }
  catch (xdaq::exception::Exception& e) {
    LOG4CPLUS_ERROR(getApplicationLogger(), "Error");
    XCEPT_RETHROW(xgi::exception::Exception, "Cannot send message", e);
  }
}



void XdaqRpcConnectivityTest::AddMaskedBChambers(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
  try
    {
      
      LOG4CPLUS_INFO(getApplicationLogger(),"Adding masked Barrel chambers");

      XdaqDbServiceClient::BarrelOrEndcap boe = XdaqDbServiceClient::BARREL;
      std::string wheeltype="W";
      int wheel;
      int sector;
      std::string ringtype="RB";
      std::string rb;
      std::string subs;
      bool secOrLsec=false;
      int lsector=1;
      
      this->GetPostMaskData(in,wheel,sector,rb,subs,secOrLsec,lsector);
      this->AddChambersToString(str_maskedBChambers,wheeltype,wheel,sector,ringtype,rb,subs,secOrLsec,lsector);
      this->AddChambersToSet(boe,wheel,sector,rb,subs,secOrLsec,lsector);
      this->MaskForm(in,out);
    }

  catch (xdaq::exception::Exception& e) {
    LOG4CPLUS_ERROR(getApplicationLogger(), "Error");
    XCEPT_RETHROW(xgi::exception::Exception, "Cannot send message", e);
  }
}

void XdaqRpcConnectivityTest::AddMaskedEChambers(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
  try
    {

      LOG4CPLUS_INFO(getApplicationLogger(),"Adding masked Endcap chambers");
      
      XdaqDbServiceClient::BarrelOrEndcap boe = XdaqDbServiceClient::ENDCAP;
      std::string wheeltype = "RE";
      int wheel;
      int sector;
      std::string ringtype="";
      std::string rb;
      std::string subs;
      bool secOrLsec = false;
      int lsector = 1;

      this->GetPostMaskData(in,wheel,sector,rb,subs,secOrLsec,lsector);
      if (!(rb=="1" && sector>18) && !(rb=="1" && secOrLsec && wheel!=1 && wheel!=-1)){
	this->AddChambersToString(str_maskedEChambers,wheeltype,wheel,sector,ringtype,rb,subs,secOrLsec,lsector);
	this->AddChambersToSet(boe,wheel,sector,rb,subs,secOrLsec,lsector);
	this->MaskForm(in,out);
      }
      else
	*out << "Sorry, linksectors have only been defined for layer 2 and 3, and layer 1 has only 18 sectors for disks 2,3,-2,-3" << std::endl;
    }

  catch (xdaq::exception::Exception& e) {
    LOG4CPLUS_ERROR(getApplicationLogger(), "Error");
    XCEPT_RETHROW(xgi::exception::Exception, "Cannot send message", e);
  }
}





void XdaqRpcConnectivityTest::GetPostMaskData(xgi::Input * in,int& wheel,int& sector,std::string& rb,std::string& subs, bool& secOrLsec, int& lsector)
{
  try
    {

      LOG4CPLUS_INFO(getApplicationLogger(),"Getting the form data");

      Cgicc formData(in);      
      
      form_iterator iWheel   = formData.getElement("wheel");
      form_iterator iSector = formData.getElement("Sector");
      form_iterator iRb   = formData.getElement("rb");
      form_iterator isubs = formData.getElement("sSector");
      form_iterator iSecOrLsec = formData.getElement("secOrLsec");
      form_iterator iLsector = formData.getElement("lsector");
      
      if (iWheel  != formData.getElements().end() &&
	  iSector!= formData.getElements().end() &&
	  iRb!=formData.getElements().end() &&
	  isubs!= formData.getElements().end()){
	wheel = iWheel->getIntegerValue();
	sector  = iSector->getIntegerValue();
	rb = iRb->getValue();
	subs = isubs->getValue();
      }
      if (iSecOrLsec!= formData.getElements().end() && iLsector!= formData.getElements().end()){
	secOrLsec  = (bool)(iSecOrLsec->getValue()=="lsec");
	lsector  = iLsector->getIntegerValue();
      }
    }
  catch (xdaq::exception::Exception& e) {
    LOG4CPLUS_ERROR(getApplicationLogger(), "Error");
    XCEPT_RETHROW(xgi::exception::Exception, "Cannot send message", e);
  }
}


void XdaqRpcConnectivityTest::AddChambersToString(string& str_maskedChambers,string wheeltype,int wheel,
					      int sector,string ringtype, string rb,string subs, bool secOrLsec, int lsector){
  try
    {

      LOG4CPLUS_INFO(getApplicationLogger(),"Adding masked chambers to string");
      std::stringstream s_sector;
      int chambersector = 36;
      if (wheel==2 || wheel==-2)
	chambersector = 2;
      if (!secOrLsec){
	s_sector << sector;
      }
      else{
	chambersector += (lsector-1)*3;
	for (int i=0;i<3;i++){
	  if (chambersector >36)
	    chambersector %= 36;
	  s_sector << chambersector++;
	  if (i!=2) s_sector << ",";
	}
      }
      stringstream strstr;
      strstr <<"<li>"<<wheeltype<<wheel<<"/"<<ringtype<<rb<<"/"<<s_sector.str()<<subs<<"</li>";
      str_maskedChambers += strstr.str();
    }
  catch (xdaq::exception::Exception& e) {
    LOG4CPLUS_ERROR(getApplicationLogger(), "Error");
    XCEPT_RETHROW(xgi::exception::Exception, "Cannot send message", e);
  }
}


void XdaqRpcConnectivityTest::AddChambersToSet(XdaqDbServiceClient::BarrelOrEndcap boe, int wheel,
					    int sector,string rb,string subs,bool secOrLsec, int lsector){
  try{

    LOG4CPLUS_INFO(getApplicationLogger(),"Adding masked chamber to set");

    int layer;

    if(boe==XdaqDbServiceClient::BARREL){
      if(rb == "1in")layer=1;
      if(rb == "1out")layer=2;
      if(rb == "2in")layer=3;
      if(rb == "2out")layer=4;
      if(rb == "3")layer=5;
      if(rb == "4")layer=6;
    }
    else{
      layer = atoi(rb.c_str());
      //      stringstream strstr;
      //      strstr << rb;
      //      strstr >> layer;
    }
    int chambersector = 36;
    if (wheel==2 || wheel==-2)
      chambersector = 2;
    if (!secOrLsec){
      chIndex x(XdaqDbServiceClient::toString(boe),wheel,sector,layer,subs);
      chambers.insert(x);
    }
    else{
      for (int i=0;i<3;i++){
	sector = chambersector++;
	if (chambersector > 36)
	  chambersector %= 36;
	chIndex x(XdaqDbServiceClient::toString(boe),wheel,sector,layer,subs);
	chambers.insert(x);
      }
    }
  }
  catch (xdaq::exception::Exception& e) {
    LOG4CPLUS_ERROR(getApplicationLogger(), "Error");
    XCEPT_RETHROW(xgi::exception::Exception, "Cannot send message", e);
  }

}


void XdaqRpcConnectivityTest::Reset(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
  try
    {
      
      LOG4CPLUS_INFO(getApplicationLogger(),"Reset");
      
      str_maskedBChambers ="";
      str_addedBSectors="";
      str_maskedEChambers ="";
      str_addedESectors="";
      FAIVsecVector.clear();
      chambers.clear();
      this->Default(in,out);
    }
  catch (xdaq::exception::Exception& e) {
    LOG4CPLUS_ERROR(getApplicationLogger(), "Error");
    XCEPT_RETHROW(xgi::exception::Exception, "Cannot send message", e);
  }
}





void XdaqRpcConnectivityTest::ConnectivityTest(xgi::Input* in,xgi::Output* out) throw (xgi::exception::Exception)
{
  
  try{

    LOG4CPLUS_INFO(getApplicationLogger(),"starting Connectivity Test");
    std::vector<int> thrvalues;
    int high_thr,low_thr;
    GetStartFormData(in,high_thr,low_thr);
    thrvalues.push_back(high_thr);
    thrvalues.push_back(low_thr);
    std::set<chIndex>::iterator chamber_;
    PerformeTest(out,thrvalues);
  }
  catch (xdaq::exception::Exception& e) {
    LOG4CPLUS_ERROR(getApplicationLogger(), "Error");
    XCEPT_RETHROW(xgi::exception::Exception, "Cannot send message", e);
  }
}


void XdaqRpcConnectivityTest::GetStartFormData(xgi::Input * in,int& fthr, int& lthr){
  try{

    Cgicc formData(in);      
    
    form_iterator iFThr = formData.getElement("fthr");
    form_iterator iLThr = formData.getElement("lthr");
    
    if (iFThr != formData.getElements().end() &&
	iLThr != formData.getElements().end()){
      
      fthr = iFThr->getIntegerValue();	
      lthr = iLThr->getIntegerValue();	
    }
  }
  catch (xdaq::exception::Exception& e) {
    LOG4CPLUS_ERROR(getApplicationLogger(), "Error");
    XCEPT_RETHROW(xgi::exception::Exception, "Cannot send message", e);
  }
}
  


void XdaqRpcConnectivityTest::fromNametoLocation(const std::string& name, std::string& boe_str, 
					    int& wheel, int& sector, 
					    int& layer, std::string& subs)
{
  try
    {


      unsigned int ip1,ep1,lp1;
      ip1=name.find("W");
      bool isbarrel = (ip1!=name.npos);
      if(isbarrel){
	ip1++;
	boe_str="Barrel";
	ep1=name.find("RB")-1;
      }
      else{
	ip1=name.find("RE")+2;
	boe_str="Endcap";
	ep1 =name.find("/");
      }
      lp1=ep1-ip1;
      std::string pz1=name.substr(ip1,lp1);


      {
      std::stringstream is;
      is << pz1;
      is >> wheel; 
      }

      int ip2, ep2, lp2;
      if
	(isbarrel) ip2=name.find("RB")+2;
      else 
	ip2=name.find("/")+1;

      ep2 = name.rfind("/");
      lp2=ep2-ip2;
      std::string pz2=name.substr(ip2,lp2);
	
      {
	std::stringstream is;
	int prel;
	is << pz2.substr(0,1);
	is >> prel;

	if(! isbarrel) 
	  layer=prel;
	else
	  {
	    if (prel > 2)
	      layer=prel+2;
	    else
	      if (pz2.find("in") != pz2.npos)
		layer=prel*2-1;
	      else if (pz2.find("out") != pz2.npos)
		layer=prel*2;
	      else	
		LOG4CPLUS_INFO (getApplicationLogger(), "Strange name " << name <<" sector part is " <<pz2);
	  }
      }
      
      int ip3=name.rfind("/")+1;
      std::string pz3=name.substr(ip3,name.npos);
     
      {
	std::stringstream is;
	if(! isbarrel){
	  is << pz3;
	  is >> sector;
	  subs="";
	}
	else{
	  unsigned int p=pz3.find("+");
	  unsigned int n=pz3.find("-");
	  unsigned int t=0;
	  if (p!=pz3.npos && n!=pz3.npos)
	    t = (p>n) ? n : p;
	  else if (p!=pz3.npos)
	    t=p;
	  else  
	    t=n;
	  is << pz3.substr(0,t);
	  is >>sector;
	  if (t!=pz3.npos)
	    subs=pz3.substr(t,pz3.npos);
	  else
	    subs="";
	}
      }
      LOG4CPLUS_INFO (getApplicationLogger(), "Name " << name << " " << boe_str << "w" << wheel << "s" << sector << "l" << layer);

    }
  catch (xdaq::exception::Exception& e) {
    LOG4CPLUS_ERROR(getApplicationLogger(), "Error");
XCEPT_RETHROW(xgi::exception::Exception, "Cannot send message", e);
  }
}


void XdaqRpcConnectivityTest::PerformeTest(xgi::Output* out, std::vector<int> thrvalues) {

  try{
  int highvalue = thrvalues[0];
  int lowvalue = thrvalues[1];

  LOG4CPLUS_INFO(getApplicationLogger(),"Perform the RPC Connectivity Test!");

  XdaqSoapAccess::TSOAPParamList paramList;

    // connect to DBService - get information about which Febs should be contacted
    // and how


  for( FAIVV::iterator iFAIV = FAIVsecVector.begin();iFAIV != FAIVsecVector.end(); ++iFAIV){
    FAIV  febAccessInfoVector = *iFAIV;
    for (FAIV::iterator iFAI = febAccessInfoVector.begin();iFAI != febAccessInfoVector.end(); ++iFAI) {
      FebAccessInfo& febAccessInfo = iFAI->bag;
      std::string chName = (febAccessInfo.getChamberLocationName());
      std::string boe_str;
      int wheel;
      int sector;
      int layer;
      std::string subs;
      this->fromNametoLocation(chName, boe_str, wheel, sector, layer, subs);
      chIndex x(boe_str,wheel, sector, layer, subs);
      if(chambers.find(x)==chambers.end()){
	LOG4CPLUS_INFO(getApplicationLogger(),"Chamber "<<chName<<" ready to be tested");
	instanceReadMap instanceForReadLBBox;
	this->setThreshold(highvalue,febAccessInfo,instanceForReadLBBox);
	this->readThreshold(instanceForReadLBBox);
      }
      else{
	LOG4CPLUS_INFO(getApplicationLogger(),"Chamber "<<chName<<" Masked"<<x);
      }
    }
  }
  
  for( FAIVV::iterator iFAIV = FAIVsecVector.begin();iFAIV != FAIVsecVector.end(); ++iFAIV){
    FAIV  febAccessInfoVector = *iFAIV;
    for (FAIV::iterator iFAI = febAccessInfoVector.begin();iFAI != febAccessInfoVector.end(); ++iFAI) {
      FebAccessInfo& febAccessInfo = iFAI->bag;
      std::string chName = (febAccessInfo.getChamberLocationName());
      std::string boe_str;
      int wheel;
      int sector;
      int layer;
      std::string subs;
      this->fromNametoLocation(chName, boe_str, wheel, sector, layer, subs);
      chIndex x(boe_str,wheel, sector, layer, subs);
      std::set<chIndex>::iterator found = chambers.find(x);
      if(chambers.find(x)==chambers.end()){
	{
	  instanceReadMap instanceForReadLBBox;
	  LOG4CPLUS_INFO(getApplicationLogger(),"Setting threshold to " <<lowvalue<<" mV");
	  this->setThreshold(lowvalue,febAccessInfo,instanceForReadLBBox);
	  this->readThreshold(instanceForReadLBBox);
	  this->readNoise(febAccessInfo);
	}
	{
	  instanceReadMap instanceForReadLBBox;
	  this->setThreshold(highvalue,febAccessInfo,instanceForReadLBBox);
	  this->readThreshold(instanceForReadLBBox);
	  this->readNoise(febAccessInfo);
	}
      }
      else{
	LOG4CPLUS_INFO(getApplicationLogger(),"skipping found " << *found << " found==x " << (*found==x) << " x " << x << " found<x " << (*found < x) << " x<found " << (x < *found));
      }
    }
  }
  
  }catch (xcept::Exception& e) {
    LOG4CPLUS_ERROR(this->getApplicationLogger(), e.what());
    XCEPT_RETHROW(xgi::exception::Exception, e.what(), e);
  }
  catch (std::exception& e) {
    LOG4CPLUS_ERROR(this->getApplicationLogger(), e.what());
    XCEPT_RAISE(xgi::exception::Exception, e.what());
  }
}



void XdaqRpcConnectivityTest::setThreshold(int thrvalue, FebAccessInfo& febAccessInfo, instanceReadMap& instanceForReadLBBox)
{
  try{
    
    
    std::string name = febAccessInfo.getChamberLocationName();
    bool isEndcap = (name.find("W") == name.npos);

  instanceMap  instanceForLBBox;
  int xdaqInstanceNumber = febAccessInfo.getXdaqAppInstance(); 
  MassiveWriteRequest& writeRequest = instanceForLBBox[xdaqInstanceNumber].bag;
  
  if(instanceForLBBox[xdaqInstanceNumber].bag.getProperties().empty()){
    writeRequest.getProperties().push_back(MassiveWriteRequest::PROP_VTH1);
    writeRequest.getProperties().push_back(MassiveWriteRequest::PROP_VTH2);
    writeRequest.getProperties().push_back(MassiveWriteRequest::PROP_VMON1);
    writeRequest.getProperties().push_back(MassiveWriteRequest::PROP_VMON2);
    if (isEndcap)
      {
	writeRequest.getProperties().push_back(MassiveWriteRequest::PROP_VTH3);
	writeRequest.getProperties().push_back(MassiveWriteRequest::PROP_VTH4);
	writeRequest.getProperties().push_back(MassiveWriteRequest::PROP_VMON3);
	writeRequest.getProperties().push_back(MassiveWriteRequest::PROP_VMON4);
      }
  }
  xdata::Bag<FebValues> valuesBag;
  valuesBag.bag.getFeb().bag.setCcuAddress(febAccessInfo.getCcuAddress());
  valuesBag.bag.getFeb().bag.setChannel(febAccessInfo.getI2cChannel());
  valuesBag.bag.getFeb().bag.setAddress(febAccessInfo.getFebAddress());
  valuesBag.bag.getFeb().bag.setChamberLocationName(febAccessInfo.getChamberLocationName());
  valuesBag.bag.getFeb().bag.setFebLocalEtaPartition(febAccessInfo.getFebLocalEtaPartition());
  valuesBag.bag.getFeb().bag.setEndcap(isEndcap);
  

  std::stringstream osfile;
  osfile <<"ConnectivityTest.log";
  std::ofstream ofile(osfile.str().c_str(),ios::app);
  ofile.close();
  ofile.open(osfile.str().c_str(),ios::app);
  ofile<<"\n ==> setThreshold to "<<thrvalue
       <<": CCU ring="<<febAccessInfo.getCcuAddress()
       <<" FebAddress="<<febAccessInfo.getFebAddress()
       <<" I2cCBChannel="<<febAccessInfo.getI2cChannel()
       <<" Chamber="<<(string)febAccessInfo.getChamberLocationName()    
       <<" Partition="<<(string)febAccessInfo.getFebLocalEtaPartition()<<std::endl;


  valuesBag.bag.getValues().push_back(thrvalue); // vth1
  valuesBag.bag.getValues().push_back(thrvalue); // vth2
  valuesBag.bag.getValues().push_back(3500); // vmon1
  valuesBag.bag.getValues().push_back(3500); // vmon2
    if (isEndcap)
      {
	valuesBag.bag.getValues().push_back(thrvalue); // vth3
	valuesBag.bag.getValues().push_back(thrvalue); // vth4
	valuesBag.bag.getValues().push_back(3500); // vmon3
	valuesBag.bag.getValues().push_back(3500); // vmon4
      }
  writeRequest.getFebValues().push_back(valuesBag);

         
  for(instanceMap::iterator iMap = instanceForLBBox.begin(); iMap != instanceForLBBox.end(); ++iMap){
    LOG4CPLUS_INFO(getApplicationLogger()," Map number "<< iMap->first);
    try {
      ofile <<" Massive Writing LB "<<std::endl; 
      instanceForReadLBBox[iMap->first] =lboxAccessClient.massiveWrite(iMap->second, iMap->first);
    } catch (exception& e) {
      LOG4CPLUS_ERROR(getApplicationLogger(), "std exception: " << e.what());
    } catch (...){
      ofile <<"Communication problem, maybe the FEB is off!"<<std::endl;
    }
	
  }
  ofile.close();
  }catch (xcept::Exception& e) {
    LOG4CPLUS_ERROR(this->getApplicationLogger(), e.what());
    XCEPT_RETHROW(xgi::exception::Exception, e.what(), e);
  }
  catch (std::exception& e) {
    LOG4CPLUS_ERROR(this->getApplicationLogger(), e.what());
    XCEPT_RAISE(xgi::exception::Exception, e.what());
  }
}


void
XdaqRpcConnectivityTest::readThreshold(instanceReadMap& instanceForReadLBBox)
{
  try{
  std::stringstream osfile;
  osfile <<"ConnectivityTest.log";
  std::ofstream ofile(osfile.str().c_str(),ios::app);
  ofile.close();
  ofile.open(osfile.str().c_str(),ios::app);
  
  for(instanceReadMap::iterator iMap = instanceForReadLBBox.begin(); iMap != instanceForReadLBBox.end(); ++iMap){
    try {
      MassiveReadResponse& readResponse = instanceForReadLBBox[iMap->first]->bag;

    FebProperties& props = readResponse.getProperties();
    for (FebProperties::iterator iProp = props.begin(); iProp != props.end(); ++iProp) {       
      ofile <<"Feb property " << (string)(*iProp) <<std::endl;
      LOG4CPLUS_INFO (getApplicationLogger(), "Feb property " << (string)(*iProp));
    }


    typedef FebValuesVector ValuesVector;
    ValuesVector& valuesVector = readResponse.getFebValues();
    for (ValuesVector::iterator iVal = valuesVector.begin(); iVal != valuesVector.end(); ++iVal) {
      FebValues& febValues = iVal->bag;
      FebInfo& febInfo = febValues.getFeb().bag;
      LOG4CPLUS_INFO (getApplicationLogger(), "Chamber Location Name " << (string)febInfo.getChamberLocationName() );
      LOG4CPLUS_INFO (getApplicationLogger(), "Chamber Eta Partition " << (string)febInfo.getFebLocalEtaPartition() );
      LOG4CPLUS_INFO (getApplicationLogger(), "CCU Address           " << febInfo.getCcuAddress());
      LOG4CPLUS_INFO (getApplicationLogger(), "CBChannel             " << febInfo.getChannel());
      LOG4CPLUS_INFO (getApplicationLogger(), "FebAddress            " << febInfo.getAddress());
      ofile <<"Chamber Name "<<(string)febInfo.getChamberLocationName()
	    <<" Partition " << (string)febInfo.getFebLocalEtaPartition()
	    <<" CCU Address " << febInfo.getCcuAddress()
	    <<" CBChannel " << febInfo.getChannel()
	    <<" Address               " << febInfo.getAddress()<<std::endl;
      typedef FebValues::Values Values;
      Values& values = febValues.getValues();
      for (Values::iterator iValue = values.begin(); iValue != values.end(); ++iValue) {
	LOG4CPLUS_INFO (getApplicationLogger(), "Value :               " << (*iValue) );
	ofile <<" Values = "<<(*iValue);
      } 
      ofile <<std::endl;
      
    }
    //    delete instanceForReadLBBox[iMap->first];
   
    }catch (...){
      ofile <<"Communication problem. Maybe the feb is off!!"<<std::endl;
    }
  }
  ofile.close();
  }catch  (xcept::Exception& e) {
    LOG4CPLUS_ERROR(this->getApplicationLogger(), e.what());
    XCEPT_RETHROW(xgi::exception::Exception, e.what(), e);
  }
  catch (std::exception& e) {
    LOG4CPLUS_ERROR(this->getApplicationLogger(), e.what());
    XCEPT_RAISE(xgi::exception::Exception, e.what());
  }
}



void
XdaqRpcConnectivityTest::readNoise(FebAccessInfo& febAccessInfo)
{


  try{
  std::stringstream osfile;
  osfile <<"ConnectivityTest.log";
  cout <<" +++++++++++++++++ "<<osfile.str()<<std::endl;
  std::ofstream ofile(osfile.str().c_str(),ios::app);
  ofile.close();
  ofile.open(osfile.str().c_str(),ios::app);
  time_t rawtime;
  time(&rawtime);
  ofile<<"\nToday is "<<ctime(&rawtime)<<endl;      
  std::string boe_str;
  int whe;
  int sec;
  int lay;
  std::string subs;
  XdaqDbServiceClient::BarrelOrEndcap boe; 
  this->fromNametoLocation(febAccessInfo.getChamberLocationName(), boe_str, whe, sec, lay, subs);
  LOG4CPLUS_INFO(getApplicationLogger()," Chamber name ="<<(string)febAccessInfo.getChamberLocationName()
		 <<"Barrel or Wheel"<<boe_str
		 <<" Wheel="<<whe
		 <<" Sector="<<sec
		 <<" Layer="<<lay
		 <<" SubSector "<<subs);
  if(boe_str=="Barrel")
    boe = XdaqDbServiceClient::BARREL;
  else
    boe = XdaqDbServiceClient::ENDCAP;
  XdaqDbServiceClient::ChamStripAccessInfoVector* chamStripAccessInfoVector
    = dbServiceClient.getChamStripByChamLoc(whe, lay, sec, subs,boe);
  if(chamStripAccessInfoVector->empty()){
    LOG4CPLUS_INFO(getApplicationLogger(),"No results were returned from the database. Perhaps this chamber does not have a link board assignment in the database.");
  }
  else{
    std::map<int,std::map<int,int> > febIds;
    std::map<int,int> febToI2CAddress;
    std::map<int,std::set<int> > chipIds;
    std::map<int,int> chipToinstance;
    std::map<int, std::string > boardToChamber;
    std::map<int, std::string > boardToPartition;
    LOG4CPLUS_INFO(getApplicationLogger(),"Size of stripAccess vector = "<<chamStripAccessInfoVector->size());
    
    for (XdaqDbServiceClient::ChamStripAccessInfoVector::iterator iCSAI = chamStripAccessInfoVector->begin();
	 iCSAI != chamStripAccessInfoVector->end(); ++iCSAI) {
      ChamStripAccessInfo& chamStripAccessInfo = iCSAI->bag;
      xdata::String chamberName = chamStripAccessInfo.getChamberLocationName();
      xdata::Integer boardId = chamStripAccessInfo.getBoardId();
      xdata::Integer xdaqAppInstance = chamStripAccessInfo.getXdaqAppInstance();
      xdata::Integer chipId = chamStripAccessInfo.getChipId();
      xdata::Integer febId = chamStripAccessInfo.getFebId();
      xdata::Integer febConnectorNum = chamStripAccessInfo.getFebConnectorNum();
      xdata::String febLocalEtaPartition = chamStripAccessInfo.getFebLocalEtaPartition();
      xdata::Integer febI2cAddress = chamStripAccessInfo.getFebI2cAddress();
      xdata::Integer cableChanNum = chamStripAccessInfo.getCableChanNum();
      xdata::Integer chamberStripNum = chamStripAccessInfo.getChamberStripNum();
      LOG4CPLUS_INFO(getApplicationLogger(), "Chamber Name: " << (string)chamberName << "  "
		     << "Board Id: " << (int)boardId << "  "
		     << "Instance: " << (int)xdaqAppInstance << "  "
		     << "Chip Id: " << (int)chipId << "  "
		     << "Feb Id: " << (int)febId << "  "
		     << "FebConnectorNum: " << (int)febConnectorNum << "  "
		     << "FebLocalEtaPartition: " << (string)febLocalEtaPartition << "  "
		     << "FebI2cAddress: " << (int)febI2cAddress << "  "
		     << "CableChanNum: " << (int)cableChanNum << "  "
		     << "ChamberStripNum: " << (int)chamberStripNum << endl);
      //      ofile <<"Chamber Name: " << (string)chamberName << "  "
      //	    << "Board Id: " << (int)boardId << "  "
      //	    << "Instance: " << (int)xdaqAppInstance << "  "
      //	    << "Chip Id: " << (int)chipId << "  "
      //	    << "Feb Id: " << (int)febId << "  "
      //	    << "FebConnectorNum: " << (int)febConnectorNum << "  "
      //	    << "FebLocalEtaPartition: " << (string)febLocalEtaPartition << "  "
      //	    << "FebI2cAddress: " << (int)febI2cAddress << "  "
      //	    << "CableChanNum: " << (int)cableChanNum << "  "
      //	    << "ChamberStripNum: " << (int)chamberStripNum << endl;
      
      std::map<int,int> strips;
      if (febIds.find((int)febId) != febIds.end()){
	strips = febIds[ (int)febId];
      }else{
	febToI2CAddress[ (int) febId] = (int) febI2cAddress;
      }
      strips[ (int) chamberStripNum] = (int)(febConnectorNum-1)*16 + 
	(int) cableChanNum - 1;
      febIds[(int)febId]=strips;
      

      std::set<int> febs;
      if (chipIds.find((int)chipId)!= chipIds.end()){
	febs = chipIds[ (int)chipId];
      } else {
	chipToinstance[ (int) chipId ] = (int)xdaqAppInstance;
	boardToChamber [(int) chipId ] = (string) chamberName;
	boardToPartition [(int)chipId] = (string)febLocalEtaPartition;
      }

      
      febs.insert( (int) febId);
      chipIds[ (int) chipId] = febs;

 
    }
    LOG4CPLUS_INFO(getApplicationLogger(), 
		   " Number of FEBS "<<febIds.size()
		   <<" Number of CHIPS " <<chipIds.size()<<endl);
    


    std::map<int, std::set<int> >::iterator ich;
    for (ich=chipIds.begin(); ich!=chipIds.end(); ich++){
      int chip = ich->first;

      //     osfile << boardToChamber[chip] <<"_Connectivity.log";
      ofile <<"--> Chamber "<<boardToChamber[chip]<<" "
	    <<boardToPartition[chip]<<endl;

      LOG4CPLUS_INFO(getApplicationLogger(), 
		     "chip="<<chip);


      int instance = chipToinstance[chip];
      ofile <<"Chip Id = " <<chip<<" instance="<<instance<<std::endl;
      double data[96];
      for (int i=0;i<96;i++)
	data[i]=0;
      this->linkBoardCounters(instance, chip, data);


      std::set<int> allfebs = ich->second;
      ofile <<"Number of FEBS for chip="<<chip<<" ="<<allfebs.size()<<std::endl;
      std::set<int>::iterator ifb;
      for(ifb=allfebs.begin(); ifb!=allfebs.end(); ifb++){
	
	LOG4CPLUS_INFO(getApplicationLogger(), 
		       " feb="<<*ifb<<endl);
	ofile <<"FEB Id = "<<*ifb
	      <<" I2c Address ="<<febToI2CAddress[*ifb]<<endl;
	std::map<int,int> romap = febIds[*ifb];
	std::map<int,int>::iterator istr;
	ofile<<"->|";
	for (istr=romap.begin();istr!=romap.end(); istr++){
 	  int rpcstrip  = istr->first;
	  int lbchannel = istr->second;
	  double noise  = data[lbchannel];
	  ofile << "strip "<<setw(2)<<rpcstrip<<" noise="<<noise<<"|";
	  LOG4CPLUS_INFO(getApplicationLogger(), 
			 " strip "<<rpcstrip<<" noise="<<noise<<"|");
	}
	LOG4CPLUS_INFO(getApplicationLogger(), endl);
	ofile <<std::endl;
	
      }
    }
    ofile.close();

  }
  }catch (xcept::Exception& e) {
    LOG4CPLUS_ERROR(this->getApplicationLogger(), e.what());
    XCEPT_RETHROW(xgi::exception::Exception, e.what(), e);
  }
  catch (std::exception& e) {
    LOG4CPLUS_ERROR(this->getApplicationLogger(), e.what());
    XCEPT_RAISE(xgi::exception::Exception, e.what());
  }
}


void 
XdaqRpcConnectivityTest::linkBoardCounters(int instance, int chipId, 
					   double rate[96]) 
{
  try{

  const int binCount=96;
 
  typedef XdaqDiagAccessClient::DiagIdVector DiagIds;
  typedef XdaqDiagAccessClient::HistoMgrInfoVector Histos;
  
  // DiagIdBag diagCtrlId1;
  // DiagIdBag diagCtrlId2;
  DiagIdBag diagCtrlId3;
  //  diagCtrlId1.bag.init(chipId, "DAQ_DIAG_CTRL");
  //diagCtrlId2.bag.init(chipId, "PULSER_DIAG_CTRL");
  diagCtrlId3.bag.init(chipId, "STATISTICS_DIAG_CTRL");
  DiagIds diagCtrlIds;
  //diagCtrlIds.push_back(diagCtrlId1);
  //diagCtrlIds.push_back(diagCtrlId2);
  diagCtrlIds.push_back(diagCtrlId3);
  
  DiagId rateId2;
  rateId2.init(chipId, "RATE");
  HistoMgrInfoBag histoInfoBag2;
  histoInfoBag2.bag.init(rateId2, binCount, rateId2);
  Histos histos;
  histos.push_back(histoInfoBag2);

  diagAccessClient.resetDiagCtrl(diagCtrlIds, "XdaqLBoxAccess", instance);
  diagAccessClient.resetHistoMgr(histos, "XdaqLBoxAccess", instance);
  
  LOG4CPLUS_INFO (getApplicationLogger(),
		  "" << endl);
  xdata::Binary counterLimit(40000000ul);
  diagAccessClient.configureDiagCtrl(diagCtrlIds, counterLimit, IDiagCtrl::ttManual, "XdaqLBoxAccess", instance);
  
  LOG4CPLUS_INFO (getApplicationLogger(),
		  "Starting diag ctrls" << endl);
  diagAccessClient.startDiagCtrl(diagCtrlIds, "XdaqLBoxAccess", instance);
  while (!diagAccessClient.checkCountingEnded(diagCtrlIds, "XdaqLBoxAccess", instance)) {
    //    LOG4CPLUS_INFO (getApplicationLogger(),
    //	    "Checking if all have finished" << endl);
  }
  LOG4CPLUS_INFO (getApplicationLogger(),
		  "All diagnostics have finished" << endl);
  diagAccessClient.stopDiagCtrl(diagCtrlIds, "XdaqLBoxAccess", instance);
  

  for (Histos::iterator iHisto = histos.begin(); iHisto != histos.end(); 
       ++iHisto) {
  LOG4CPLUS_INFO (getApplicationLogger(),
		  "Data " << iHisto->bag.toString() << endl);
    xdata::Vector<xdata::Binary>* data = 
      diagAccessClient.readDataHistoMgr(iHisto->bag.getIdBag(), 
					"XdaqLBoxAccess", instance);
    for (int bin = 0, k=0; bin < iHisto->bag.getBinCount(); bin++, k++) {
      LOG4CPLUS_INFO (getApplicationLogger(),
		      dec << ((unsigned long)data->at(bin)) << ' ');
      rate[k] = ((unsigned long)data->at(bin));
    }
    LOG4CPLUS_INFO (getApplicationLogger(),endl);
  }
  }catch (xcept::Exception& e) {
    LOG4CPLUS_ERROR(this->getApplicationLogger(), e.what());
    XCEPT_RETHROW(xgi::exception::Exception, e.what(), e);
  }
  catch (std::exception& e) {
    LOG4CPLUS_ERROR(this->getApplicationLogger(), e.what());
    XCEPT_RAISE(xgi::exception::Exception, e.what());
  }
}
