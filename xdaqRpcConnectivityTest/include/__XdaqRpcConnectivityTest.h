#ifndef _XDAQRPCCONNECTIVITYTEST_H_
#define _XDAQRPCCONNECTIVITYTEST_H_

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/exception/Exception.h" 
#include "xdaq/NamespaceURI.h"
 
#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"

#include "xdata/Serializable.h"
#include "xdata/rpctsoap/Serializer.h"
#include "xdata/String.h"
#include "xgi/Utils.h"
#include "xgi/Method.h"

#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/fsm/FailedEvent.h"

#include "rpct/xdaqlboxaccess/XdaqLBoxAccessClient.h"
#include "rpct/xdaqutils/XdaqDbServiceClient.h"
#include "rpct/xdaqdiagaccess/XdaqDiagAccessClient.h"

using namespace rpct;
using namespace rpct::xdaqutils;
using namespace rpct::xdaqdiagaccess;
 
struct chIndex{
  chIndex(std::string b, int w, int s, int l, std::string subs) :
    boe_str(b),wheel(w),sector(s),layer(l),subsector(subs){}
  std::string boe_str;
  int wheel;
  int sector;
  int layer;
  std::string subsector;
  bool operator < (const chIndex& c)const{
    if (c.boe_str==this->boe_str){
      if (c.wheel == this->wheel){
        if (c.sector == this->sector){
          if (c.layer == this->layer){
            return c.subsector < this->subsector;
          }
          else{
            return c.layer < this->layer;
          }
        }
        else{
          return c.layer < this->layer;
        }
      }
      else{
        return c.wheel < this->wheel;
      }
    }
    else{
      return c.boe_str < this->boe_str;
    }
  }
};



class XdaqRpcConnectivityTest: public xdaq::Application { 

 private:

  rpct::xdaqutils::XdaqDbServiceClient dbServiceClient;
  rpct::XdaqLBoxAccessClient lboxAccessClient;
  rpct::xdaqdiagaccess::XdaqDiagAccessClient diagAccessClient;
  typedef XdaqDbServiceClient::FebAccessInfoVector FAIV;
  typedef std::vector<XdaqDbServiceClient::FebAccessInfoVector> FAIVV;
  typedef std::map<int ,rpct::XdaqLBoxAccessClient::MassiveWriteRequestBag >        instanceMap;
  typedef std::map<int ,rpct::XdaqLBoxAccessClient::MassiveWriteResponseBag*> instanceReadMap;

  FAIVV FAIVsecVector;
  std::set<chIndex> chambers;
  std::string str_addedESectors;
  std::string str_addedBSectors;
  std::string str_maskedBChambers;
  std::string str_maskedEChambers;

 private:
  void PerformeTest(xgi::Output* out, std::vector<int> thrvalues);
  void setThreshold(int thrvalue, rpct::FebAccessInfo& febAccessInfo, instanceReadMap& map);
  void readThreshold(instanceReadMap& map);
  void readNoise(rpct::FebAccessInfo& febAccessInfo);
  void fromNametoLocation(const std::string& name, std::string& boe_str, int&w, int&l, int&s, std::string& subs);
  void linkBoardCounters(int instance, int chipId, double rate[96]);
  void GetPostData(xgi::Input* in, int& wheel, std::string& secOrTow, std::string& tower, int& sector);
  void GetPostMaskData(xgi::Input* in, int& wheel, int& sector, std::string& rb,std::string& subs);
  void AddSectorsToString(std::string& addedSectors,std::string wheeltype,int wheel, std::string secOrTow, std::string tower, int sector);
  void AddChambersToString(std::string& maskedChambers,std::string wheeltype,int wheel, int sector,std::string ringtype, std::string rb, std::string subs);
  void AddSectorsToFAIVV(XdaqDbServiceClient::BarrelOrEndcap boe, int wheel, std::string secOrTow, std::string tower, int sector);
  void AddChambersToSet(XdaqDbServiceClient::BarrelOrEndcap boe, int wheel,int sector,std::string rb, std::string subs);
  void GetStartFormData(xgi::Input * in, int& fthr, int& lthr);

 public:
    static const char* RPCT_RPC_CONFIG_NS;
    static const char* RPCT_RPC_CONFIG_PREFIX;

    XDAQ_INSTANTIATOR();    
 
    XdaqRpcConnectivityTest(xdaq::ApplicationStub * s);      

    virtual ~XdaqRpcConnectivityTest();
                
    void Default(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
    void ConnectivityTest(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
    void MaskForm(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
    void AddBSectors(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
    void AddESectors(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
    void AddMaskedEChambers(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
    void AddMaskedBChambers(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
    void Reset(xgi::Input * in, xgi::Output * out)throw (xgi::exception::Exception);

};

#endif 
