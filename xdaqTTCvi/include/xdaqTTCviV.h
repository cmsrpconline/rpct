#ifndef _xdaqTTCviV_h_
#define _xdaqTTCviV_h_

#include "PackageInfo.h"

namespace xdaqTTCvi
{
	const string package  =  "rpct/xdaqTTCvi";
	const string versions =  "1.1";
	const string description = "TTCvi access application";
	const string link = "";
	toolbox::PackageInfo getPackageInfo();
	void checkPackageDependencies() throw (toolbox::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
