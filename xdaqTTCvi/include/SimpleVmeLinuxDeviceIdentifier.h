#ifndef __SimpleVmeLinuxDeviceIdentifier
#define __SimpleVmeLinuxDeviceIdentifier

#include <string>
#include "hal/BusAdapterException.hh"
#include "hal/DeviceIdentifier.hh"
#include "tb_vme.h"




class SimpleVmeLinuxDeviceIdentifier : public HAL::DeviceIdentifier {
public:
  SimpleVmeLinuxDeviceIdentifier(TVMEInterface* vme)
    throw (HAL::BusAdapterException);

  ~SimpleVmeLinuxDeviceIdentifier();

  TVMEInterface* getVME();
 
  std::string printString() const;

private:
  TVMEInterface* VME;
};

#endif
