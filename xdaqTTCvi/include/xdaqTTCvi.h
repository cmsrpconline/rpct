#ifndef __TTCvi_h__
#define __TTCvi_h__


#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/exception/Exception.h" 
#include "xdaq/NamespaceURI.h"

#include "xdata/String.h"
#include "xdata/UnsignedLong.h"
#include "xdata/Integer.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"


#include "TVMEAddressInfo.h"
#include "hal/VMEAddressTable.hh"
#include "hal/VMEAddressTableASCIIReader.hh"
//#include "hal/SBS620x86LinuxBusAdapter.hh"
//#include "CAEN2718LinuxPCIBusAdapter.hh"
#include "hal/CAENLinuxBusAdapter.hh"
#include "hal/VMEDummyBusAdapter.hh"
#include "SimpleVmeLinuxBusAdapter.h"
#include "xdaqTTCviConstants.h"
#include "xdaqTTCviAccess.h"


class xdaqTTCvi : public xdaq::Application {
public:
    XDAQ_INSTANTIATOR();
	
    xdaqTTCvi(xdaq::ApplicationStub * s);
    virtual ~xdaqTTCvi();
	
protected:
    xdata::String boardAddress;
    xdata::String ttcviType;
    //SBS620x86LinuxBusAdapter busAdapter;
    //VMEDummyBusAdapter busAdapter;
    //SimpleVmeLinuxBusAdapter busAdapter;
    //CAEN2718LinuxPCIBusAdapter busAdapter;
    HAL::CAENLinuxBusAdapter busAdapter;
    HAL::VMEAddressTable* addressTable;
    TVMEAddressInfo addressInfo;
    TTCviAccess* ttcvi;  
    
    void InitBusAdapter();
    
    void Configure();
    typedef std::map<std::string, xdata::Serializable*> TSOAPParamMap;
    void parseSOAPRequest(xoap::MessageReference msg, std::string functionName, 
            std::string prefix, std::string uri, TSOAPParamMap& paramMap) throw (xoap::exception::Exception);            
   
    xoap::MessageReference sendSOAPResponse(std::string functionName, std::string prefix, std::string uri, 
            xdata::Serializable* value) throw (xoap::exception::Exception);
            
    xoap::MessageReference onConfig(xoap::MessageReference msg) throw (xoap::exception::Exception);    
    xoap::MessageReference onRead(xoap::MessageReference msg) throw (xoap::exception::Exception);
    xoap::MessageReference onWrite(xoap::MessageReference msg) throw (xoap::exception::Exception);
    
  
    class TAddressTableItemExt: public HAL::AddressTableItem {
    private:
        xdata::UnsignedLong value;
    public:
        TAddressTableItemExt(const HAL::AddressTableItem& item)
  	    : AddressTableItem(item.getKey(), item.getDescription(), 
  	         (HAL::GeneralHardwareAddress&)(item.getGeneralHardwareAddress()), 
          item.getMask(), item.isWritable(), item.isReadable()),
          value(0) {}                      
                      
        TAddressTableItemExt(std::string key,
	                     std::string description,
	                     HAL::GeneralHardwareAddress& address,
	                     unsigned long mask,
	                     bool writableFlag,
	                     bool readableFlag)
	       : HAL::AddressTableItem(key, description, address, mask, writableFlag, 
	                     readableFlag), value(0) {}
	                     
        TAddressTableItemExt(std::string key,
                         unsigned long addressModifier,
                         unsigned long dataWidth,
  	                     unsigned long address, 
	                     unsigned long mask,
	                     bool readableFlag,
	                     bool writableFlag,
	                     std::string description)
	       : HAL::AddressTableItem(key, description, 
	                     *new HAL::VMEHardwareAddress(address, addressModifier, dataWidth), 
	                     mask, writableFlag, readableFlag),value(0) {}
  	      
  	     xdata::UnsignedLong& GetValueRef() {
  		    return value;
  	     }
  	
  	     unsigned long GetValue() {
  		    return (unsigned long)value;
  	     }
  	
  	     void SetValue(unsigned long val) {
  		    value = val;
  	     }
    };
  
    void BuildAddressInfo();
    void AddAddressInfo(HAL::AddressTableItem* extItem);
};


#endif








