#ifndef __TTCviAccess
#define __TTCviAccess

#include "hal/VMEDevice.hh"


class TTCviAccess : public HAL::VMEDevice {
public :
  TTCviAccess(HAL::VMEAddressTable & addressTable,
            HAL::VMEBusAdapterInterface & busAdapter,
            unsigned long baseAddress);

private:

};

#endif /* __TTCviAccess */





