#ifndef __TVMEAddressInfo
#define __TVMEAddressInfo

#include <string>
#include <map>
#include "hal/AddressTableItem.hh"
#include "hal/VMEHardwareAddress.hh"
#include "hal/AddressTableReader.hh"
#include "hal/IllegalValueException.hh"
#include "hal/NoSuchItemException.hh"



class TVMEAddressInfo {
public:
  typedef std::map<std::string, HAL::AddressTableItem*> TItemMap;
protected:
  TItemMap ItemMap;
  
  class TAddressTableReader : public HAL::AddressTableReader {
  public:  
    TAddressTableReader(TItemMap& itemMap);
  };
  
  TAddressTableReader* Reader;
public:
  TVMEAddressInfo() : Reader(NULL) {}
  virtual ~TVMEAddressInfo();
  
  void Add(const std::string& name, HAL::AddressTableItem* item) {
  	TItemMap::iterator iItem = ItemMap.find(name);
  	if (iItem == ItemMap.end()) {
  	  ItemMap.insert(TItemMap::value_type(name, item));
    }
  	else {
        throw HAL::IllegalValueException("Duplicate name'" + name + "'", __FILE__, __LINE__, __FUNCTION__);
    }
  }
  
  const TItemMap& GetItemMap() {
    return ItemMap;
  };
  
  HAL::AddressTableReader& GetAddressTableReader();
  
  HAL::AddressTableItem* Get(const std::string& name) {
  	TItemMap::iterator iItem = ItemMap.find(name);
  	if (iItem == ItemMap.end()) {
        throw HAL::NoSuchItemException(name, __FILE__, __LINE__, __FUNCTION__);
    }
  	return iItem->second;
  }
};


#endif /* _TVMEAddressInfo */
