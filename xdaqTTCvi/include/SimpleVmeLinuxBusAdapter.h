#ifndef __EppVmeLinuxBusAdapter
#define __EppVmeLinuxBusAdapter

#include "hal/IllegalValueException.hh"
#include "tb_vme.h"
#include "hal/VMEBusAdapterInterface.hh"
#include "hal/VMEAddressTable.hh"
#include "hal/DeviceIdentifier.hh"
#include "hal/BusAdapterException.hh"
#include "SimpleVmeLinuxDeviceIdentifier.h"


class SimpleVmeLinuxBusAdapter : public HAL::VMEBusAdapterInterface {
public:
  /**
   * The constructor of the SBS620x86LinuxBusAdapter.
   * The BusAdapter sets up the SBS driver to work with the HAL.
   * Driver parameters are preset here. Be aware that no other application
   * on the system should change these parameters as long as the HAL is 
   * used. The bute swapping strategy is defined in the constructor. 
   * ATTENTION: SBS default byte swap strategies change from model to 
   * model and even from revision to revision of the SBS card. So avoid
   * to choose the BT_SWAP_DEFAULT method since your software might not 
   * work anymore as soon as you exchange your SBS module. 
   *
   * @param unit is a parameter which lets you distinguish between 
   *        various SBS-busAdapters in the case that you have more
   *        than one interface hosted by your PC. The interfaces are
   *        numbered from 0 to n.
   * @param swapStrategy let's you choose the strategy to swap bytes 
   *        between the PC and the VME bus. (PCs are big endian whereas
   *        the VME bus is little endian). The default is set to 
   *        "Byte-swap-on-Byte-data". This means that bytes are only
   *        swapped when you do a single byte access. This makes that 
   *        single byte accesses to unpair addresses are transfered on
   *        bits 0...7. If you do a long word access bytes are not swapped.
   *        This means that a 32 bit number on you PC is transfered in a 
   *        way that the least significant bit comes arrives at D0 and the 
   *        most significant at bit 31. This strategy has been chosen since 
   *        then the single byte accesses for VME64x configuration are 
   *        transfered on bytes 0..7 like it is assumed by many hardware
   *        designers. If your modules are designed differently you can 
   *        change the strategy here. Note that currently there is a 
   *        problem if you need different byte swapping strategies in the 
   *        same crate. Please notify the author in case you encounter 
   *        such a problem. 
   * 
   */
  SimpleVmeLinuxBusAdapter(TVMEInterface* vme, bool owner) 
    throw( HAL::BusAdapterException );
  virtual ~SimpleVmeLinuxBusAdapter() throw( HAL::BusAdapterException );

  /**
   * This function registers a standard VMEDevice with the BusAdapter.
   * @param VMEAddressTable is currently not needed by this specific
   *        busAdapter, but it is part of the interface in order to 
   *        allow a busAdapter to do memory mapping.
   *                 
   */
  void openDevice(const HAL::VMEAddressTable& VMEAddressTable,
                  unsigned long vmeBaseaddress,
                  HAL::DeviceIdentifier** deviceIdentifierPtr,
                  unsigned long* baseAddressPtr)
    throw(HAL::BusAdapterException);

  /**
   * This function registers a VME64x module with the BusAdapter.
   * @param VMEAddressTable is currently not needed by this specific
   *        busAdapter, but it is part of the interface in order to 
   *        allow a busAdapter to do memory mapping.
   *                 
   */
  void openDevice(const HAL::VMEAddressTable& VMEAddressTable,
                  std::vector<unsigned long> vmeBaseaddresses,
                  HAL::DeviceIdentifier** deviceIdentifierPtr,
                  std::vector<unsigned long>* baseAddressesPtr)
    throw(HAL::BusAdapterException);

  /**
   * Here the deviceIdentifier is destroyed.
   */
  void closeDevice( HAL::DeviceIdentifier* deviceIdentifier ) 
    throw();

  void write( HAL::DeviceIdentifier* vmeDevice,
              unsigned long address,
              unsigned long addressModifier,
              unsigned long dataWidth,
              unsigned long data)
    throw( HAL::BusAdapterException );
  
  void read( HAL::DeviceIdentifier* vmeDevice,
             unsigned long address,
             unsigned long addressModifier,
             unsigned long dataWidth,
             unsigned long* result)
    throw( HAL::BusAdapterException );

  void writeBlock( HAL::DeviceIdentifier* deviceIdentifierPtr,
                   unsigned long startAddress,
                   unsigned long length,      // in bytes
                   unsigned long addressModifier,
                   unsigned long dataWidth,
                   char *buffer,
                   HAL::HalAddressIncrement addressBehaviour = HAL::HAL_DO_INCREMENT )
    throw( HAL::UnsupportedException,
           HAL::BusAdapterException );
  
  void readBlock( HAL::DeviceIdentifier* deviceIdentifierPtr,
                  unsigned long startAddress,
                  unsigned long length,      // in bytes
                  unsigned long addressModifier,
                  unsigned long dataWidth,
                  char *buffer,
                  HAL::HalAddressIncrement addressBehaviour = HAL::HAL_DO_INCREMENT )
    throw( HAL::UnsupportedException,
           HAL::BusAdapterException );

  virtual void resetBus( )
    throw( HAL::BusAdapterException, HAL::UnsupportedException ); 
  
private:
  TVMEInterface* VME;
  bool Owner;
};

#endif
