#include "SimpleVmeLinuxBusAdapter.h"
#include <iomanip>

using namespace std;
using namespace HAL;

SimpleVmeLinuxBusAdapter::SimpleVmeLinuxBusAdapter(TVMEInterface* vme, bool owner)
  throw (BusAdapterException)
  : VME(vme), Owner(owner) 
{
}

SimpleVmeLinuxBusAdapter::~SimpleVmeLinuxBusAdapter() 
  throw( BusAdapterException )
{
  if (Owner) {
     try {
       delete VME;
     }
     catch (TException& e) {
       throw BusAdapterException(e.what(), __FILE__, __LINE__, __FUNCTION__ );
     }
  }  
}

void SimpleVmeLinuxBusAdapter::openDevice(const VMEAddressTable& vmeAddressTable,
					  unsigned long vmeBaseAddress,
					  DeviceIdentifier** deviceIdentifierPtr,
					  unsigned long* baseAddressPtr) 
  throw (BusAdapterException) 
{  
  *deviceIdentifierPtr = new SimpleVmeLinuxDeviceIdentifier(VME);

  *baseAddressPtr = vmeBaseAddress;
}

void SimpleVmeLinuxBusAdapter::openDevice(const VMEAddressTable& vmeAddressTable,
					  vector<unsigned long> vmeBaseAddresses,
					  DeviceIdentifier** deviceIdentifierPtr,
					  vector<unsigned long>* baseAddressesPtr) 
  throw (BusAdapterException) 
{
  
  // Not really needed here...we only have one device descriptor per SBS interface
  // which is a class variable of the BusAdapter.
  *deviceIdentifierPtr= new SimpleVmeLinuxDeviceIdentifier(VME);
  
  *baseAddressesPtr = vmeBaseAddresses;
}

void SimpleVmeLinuxBusAdapter::closeDevice( DeviceIdentifier* vmeDevice ) 
  throw() 
{
  delete(vmeDevice);
}

void SimpleVmeLinuxBusAdapter::read( DeviceIdentifier* vmeDevice, 
                                     unsigned long address,
                                     unsigned long addressModifier,
                                     unsigned long dataWidth,
                                     unsigned long *resultPtr )
    throw (BusAdapterException) 
{  
  try {
      if (dataWidth == 4) {
        *resultPtr = VME->Read32(address);
      }
      else if (dataWidth == 2) {
        *resultPtr = VME->Read16(address);
      }
      else {
        throw BusAdapterException("SimpleVmeLinuxBusAdapter::read: not supported dataWidth", __FILE__, __LINE__, __FUNCTION__ );
      };
  }
  catch (TException& e) {      
      stringstream text;
      text << "Could not read from address " 
         << hex << setw(8) << setfill('0') << address 
         << " with width of "
         << dec << setfill(' ') << dataWidth << " bytes";
      throw BusAdapterException(text.str() + e.what(), __FILE__, __LINE__, __FUNCTION__ );
  }     
}

void SimpleVmeLinuxBusAdapter::write( DeviceIdentifier* vmeDevice, 
                                      unsigned long address, 
                                      unsigned long addressModifier,
                                      unsigned long dataWidth,
                                      unsigned long data)
    throw (BusAdapterException) 
{
  try {
      if (dataWidth == 4) {
        VME->Write32(address, data);
      }
      else if (dataWidth == 2) {
        VME->Write16(address, data);
      }
      else {
        throw BusAdapterException("SimpleVmeLinuxBusAdapter::write: not supported dataWidth", __FILE__, __LINE__, __FUNCTION__ );
      };
  }
  catch (TException& e) {      
      stringstream text;
      text << "Could not write to address " 
         << hex << setw(8) << setfill('0') << address 
         << " data " << data
         << " with width of "
         << dec << setfill(' ') << dataWidth << " bytes";
      throw BusAdapterException(text.str() + e.what(), __FILE__, __LINE__, __FUNCTION__ );
  }     
}


    
void SimpleVmeLinuxBusAdapter::resetBus( )
    throw( BusAdapterException, UnsupportedException )
{
  throw UnsupportedException( "Not implemented in driver\n     (SBS620x86LinuxBusAdapter::resetBus)", 
        __FILE__, __LINE__, __FUNCTION__ );
}

void SimpleVmeLinuxBusAdapter::readBlock( DeviceIdentifier *vmeDevice,
                                          unsigned long startAddress,
                                          unsigned long length,      // in bytes
                                          unsigned long addressModifier,
                                          unsigned long dataWidth,
                                          char *buffer,
                                          HalAddressIncrement addressBehaviour )
  throw (UnsupportedException, BusAdapterException)
{  
  try {
      VME->Read(startAddress, buffer, length);
  }
  catch (TException& e) {      
      stringstream text;
      text << "Could not read block from address " 
         << hex << setw(8) << setfill('0') << startAddress 
         << " with width of "
         << dec << setfill(' ') << dataWidth << " bytes";
      throw BusAdapterException(text.str() + e.what(), __FILE__, __LINE__, __FUNCTION__ );
  }     
}

void SimpleVmeLinuxBusAdapter::writeBlock( DeviceIdentifier *vmeDevice,
                                           unsigned long startAddress,
                                           unsigned long length,      // in bytes
                                           unsigned long addressModifier,
                                           unsigned long dataWidth,
                                           char *buffer,
                                           HalAddressIncrement addressBehaviour )
  throw (UnsupportedException, BusAdapterException )
{
  try {
      VME->Write(startAddress, buffer, length);
  }
  catch (TException& e) {      
      stringstream text;
      text << "Could not write block from address " 
         << hex << setw(8) << setfill('0') << startAddress 
         << " with width of "
         << dec << setfill(' ') << dataWidth << " bytes";
      throw BusAdapterException(text.str() + e.what(), __FILE__, __LINE__, __FUNCTION__ );
  }     
}


