#include "xdaqTTCviAccess.h"

using namespace HAL;

TTCviAccess::TTCviAccess(VMEAddressTable & addressTable,
			 VMEBusAdapterInterface & busAdapter,
			 unsigned long baseAddress ) :
  VMEDevice( addressTable, busAdapter, baseAddress ) 
{
}
