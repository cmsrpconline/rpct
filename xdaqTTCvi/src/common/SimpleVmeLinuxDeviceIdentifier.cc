#include "SimpleVmeLinuxDeviceIdentifier.h"

using namespace std;
using namespace HAL;

SimpleVmeLinuxDeviceIdentifier::SimpleVmeLinuxDeviceIdentifier(TVMEInterface* vme)  
  throw (BusAdapterException)
  : VME(vme) 
{
}

SimpleVmeLinuxDeviceIdentifier::~SimpleVmeLinuxDeviceIdentifier() 
{
}

TVMEInterface* SimpleVmeLinuxDeviceIdentifier::getVME() 
{
  return VME;
}

string SimpleVmeLinuxDeviceIdentifier::printString() const {
  return "there is nothing to print in SimpleVmeLinuxDeviceIdentifier::print()";
}
