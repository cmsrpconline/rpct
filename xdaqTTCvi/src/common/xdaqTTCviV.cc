#include "config/toolboxV.h"
#include "xoap/version.h"
#include "xdaqV.h"
#include "xdaqTTCviV.h"

#include <string>
#include <set>

GETPACKAGEINFO(xdaqTTCvi)

void xdaqTTCvi::checkPackageDependencies() throw (toolbox::PackageInfo::VersionException)
{
	CHECKDEPENDENCY(xdaq)
}

std::set<std::string, std::less<std::string> > xdaqTTCvi::getPackageDependencies()
{
    std::set<std::string, std::less<std::string> > dependencies;
    ADDDEPENDENCY(dependencies,xdaq);
    return dependencies;
}	
