#include "xdaqTTCvi.h"
//#include "xdaqTTCviSO.h"

#include "xcept/tools.h"

#include "xoap/DOMParser.h"

#include "xdata/soap/Serializer.h"



 
XDAQ_INSTANTIATOR_IMPL(xdaqTTCvi);

using namespace std;
using namespace HAL;


xdaqTTCvi::xdaqTTCvi(xdaq::ApplicationStub * s)
  : xdaq::Application(s), boardAddress("0x1234"), ttcviType("MkII"), 
  /*busAdapter(TVMEFactory().Create(), true),*/ 
  busAdapter(HAL::CAENLinuxBusAdapter::V2718, 0), addressTable(NULL), ttcvi(NULL)
{
  LOG4CPLUS_INFO(this->getApplicationLogger(), "constructing xdaqTTCvi");
  
  xdata::InfoSpace* is = getApplicationInfoSpace();
  
  is->fireItemAvailable("boardAddress", &boardAddress);
  is->fireItemAvailable("ttcviType", &ttcviType);
  
  BuildAddressInfo();
  InitBusAdapter();
  
  /*typedef TVMEAddressInfo::TItemMap TItemMap;
  TItemMap::const_iterator iItem = addressInfo.GetItemMap().begin();
  TItemMap::const_iterator iItemEnd = addressInfo.GetItemMap().end();
  for(; iItem != iItemEnd; ++iItem) {
    is->fireItemAvailable(iItem->first, 
         &dynamic_cast<TAddressTableItemExt*>(iItem->second)->GetValueRef());
         
    is->addItemChangedListener(iItem->first, this);
    is->addItemRetrieveListener(iItem->first, this);
  }*/
    
  addressTable = new VMEAddressTable("TTCvi address table", 
                                     addressInfo.GetAddressTableReader());
  
  xoap::bind(this, &xdaqTTCvi::onConfig, "config", XDAQ_NS_URI); 
  xoap::bind(this, &xdaqTTCvi::onRead, "read", XDAQ_NS_URI); 
  xoap::bind(this, &xdaqTTCvi::onWrite, "write", XDAQ_NS_URI); 
}



xdaqTTCvi::~xdaqTTCvi() 
{
  delete addressTable;
  delete ttcvi;
}

void xdaqTTCvi::InitBusAdapter()
{    
}

void xdaqTTCvi::Configure() 
{
    LOG4CPLUS_INFO(this->getApplicationLogger(), "Configuring");
    unsigned int addr;
    if (sscanf(((std::string)boardAddress).c_str(), "%x", &addr) != 1) {
        LOG4CPLUS_ERROR(getApplicationLogger(), "Invalid board address format");
        XCEPT_RAISE(xoap::exception::Exception, "Invalid board address format");
    }
    
    LOG4CPLUS_INFO(getApplicationLogger(), toolbox::toString("TTCvi address = %x", addr));
    try {
        ttcvi = new TTCviAccess(*addressTable, busAdapter, addr);   
    }
    catch(std::exception& e) {
        LOG4CPLUS_ERROR(getApplicationLogger(), e.what());
        XCEPT_RAISE(xoap::exception::Exception, e.what());

    }
    catch(...) {
        LOG4CPLUS_ERROR(getApplicationLogger(), "Unknown exception");
        XCEPT_RAISE(xoap::exception::Exception, "Unknown exception");
    }
    LOG4CPLUS_INFO(this->getApplicationLogger(), "Config done");
}


void xdaqTTCvi::parseSOAPRequest(xoap::MessageReference msg, std::string functionName, 
    std::string prefix, std::string uri, TSOAPParamMap& paramMap) 
throw (xoap::exception::Exception) {        
    try {
        typedef vector<xoap::SOAPElement> Elements;
        xdata::soap::Serializer serializer;     
        
        xoap::SOAPName sName(functionName, prefix, uri);
        Elements elements = msg->getSOAPPart().getEnvelope().getBody().getChildElements(sName);
        if (elements.size() != 1) {
            std::string errorMsg("Syntax error - no child elements found for function " + functionName);
            LOG4CPLUS_ERROR(getApplicationLogger(), errorMsg);
            XCEPT_RAISE(xcept::Exception, errorMsg);
        }
        xoap::SOAPElement reqElement = elements.front();
        Elements paramElements = reqElement.getChildElements();
        for (Elements::iterator iter = paramElements.begin(); iter != paramElements.end(); iter++) {
            TSOAPParamMap::iterator iParam = paramMap.find(iter->getElementName().getLocalName());
            if (iParam != paramMap.end()) {
                serializer.import(iParam->second, iter->getDOM());
            }   
        }             
    }
    catch(xcept::Exception& e) {
        LOG4CPLUS_ERROR(getApplicationLogger(), xcept::stdformat_exception_history(e) );
        XCEPT_RAISE(xoap::exception::Exception, e.what());
    }
}

xoap::MessageReference xdaqTTCvi::sendSOAPResponse(std::string functionName, std::string prefix, 
    std::string uri, xdata::Serializable* value) 
throw (xoap::exception::Exception) {                                  
    xoap::MessageReference reply = xoap::createMessage();
    xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
    xoap::SOAPName responseName = envelope.createName(functionName + "Response", prefix, uri);
    xoap::SOAPBodyElement responseElement = envelope.getBody().addBodyElement ( responseName );    
    responseElement.addNamespaceDeclaration("xsi","http://www.w3.org/2001/XMLSchema-instance");
    responseElement.addNamespaceDeclaration("xsd","http://www.w3.org/2001/XMLSchema");
    xoap::SOAPName returnName = envelope.createName(functionName + "Return");
    xoap::SOAPElement returnElement = responseElement.addChildElement(returnName);
    
    xoap::SOAPName returnType = envelope.createName("type", "xsi", "http://www.w3.org/2001/XMLSchema-instance"); 
    if (dynamic_cast<xdata::Integer*>(value))  {
        xdata::Integer* i = dynamic_cast<xdata::Integer*>(value);
        returnElement.addAttribute(returnType, "xsd:Long");
        returnElement.addTextNode(i->toString());
    }     
    else  {
        XCEPT_RAISE(xoap::exception::Exception, "Not supported return type");
    }

    
    /*LOG4CPLUS_INFO(getApplicationLogger(), toolbox::toString("Tworze serializer"));
    xdata::soap::Serializer serializer; 
    LOG4CPLUS_INFO(getApplicationLogger(), toolbox::toString("Utworzylem"));
    serializer.exportAll(value, dynamic_cast<DOMElement*>(returnElement.getDOMNode()), true);
    LOG4CPLUS_INFO(getApplicationLogger(), toolbox::toString("Wyeksportowalem"));*/
    
    return reply;      
}


xoap::MessageReference xdaqTTCvi::onRead(xoap::MessageReference msg) 
throw (xoap::exception::Exception)     
{ 
    xdata::String xName; 
    TSOAPParamMap paramMap;
    paramMap.insert(TSOAPParamMap::value_type("name", &xName));
    parseSOAPRequest(msg, "read", "xdaq", XDAQ_NS_URI, paramMap);    
     
    std::string name = (std::string)xName;   
    TAddressTableItemExt* extItem;  
    try {   
        extItem = dynamic_cast<TAddressTableItemExt*>(addressInfo.Get(name));
    }
    catch(NoSuchItemException& e) {
        LOG4CPLUS_ERROR(getApplicationLogger(), e.what());
        XCEPT_RAISE(xoap::exception::Exception, e.what());
    };  
  
    u_int32_t value;
    if (extItem->isReadable()) {
        try {
            ttcvi->read(name, &value);
            //extItem->SetValue(value);
        }
        catch(exception& e) {
            LOG4CPLUS_ERROR(getApplicationLogger(), e.what());
            XCEPT_RAISE(xoap::exception::Exception, e.what());
        }
    }
    else {
        LOG4CPLUS_ERROR(getApplicationLogger(), "Register '" + name + "' not readable");
        XCEPT_RAISE(xoap::exception::Exception, "Register '" + name + "' not readable");
    }    
    
    xdata::Integer xValue = value;
    LOG4CPLUS_INFO(getApplicationLogger(), toolbox::toString("read %x", value));                                                         
     
    return sendSOAPResponse("read", "xdaq", XDAQ_NS_URI, &xValue);                              
}

xoap::MessageReference xdaqTTCvi::onWrite(xoap::MessageReference msg) 
throw (xoap::exception::Exception)     
{
    xdata::String xName; 
    xdata::Integer xValue;
    TSOAPParamMap paramMap;
    paramMap.insert(TSOAPParamMap::value_type("name", &xName));
    paramMap.insert(TSOAPParamMap::value_type("value", &xValue));
    parseSOAPRequest(msg, "write", "xdaq", XDAQ_NS_URI, paramMap); 
    
    std::string name = (std::string)xName;   
    unsigned long value = (int)xValue;
       
    TAddressTableItemExt* extItem;
    try {  
        extItem = dynamic_cast<TAddressTableItemExt*>(addressInfo.Get(name));
    }
    catch(NoSuchItemException& e) {
        LOG4CPLUS_ERROR(getApplicationLogger(), e.what());
        XCEPT_RAISE(xoap::exception::Exception, e.what());
    };
  
    if (extItem->isWritable()) {
        try {
            ttcvi->write(name, value);
        }
        catch(exception& e) {
            LOG4CPLUS_ERROR(getApplicationLogger(), e.what());
            XCEPT_RAISE(xoap::exception::Exception, e.what());
        }
    }
    else {
        LOG4CPLUS_ERROR(getApplicationLogger(), "Register '" + name + "' not writable");
        XCEPT_RAISE(xoap::exception::Exception, "Register '" + name + "' not writable");
    }
                        
    xoap::MessageReference reply = xoap::createMessage();
    return reply;      
}

xoap::MessageReference xdaqTTCvi::onConfig(xoap::MessageReference msg) 
throw (xoap::exception::Exception)     
{
    Configure();
                               
    xoap::MessageReference reply = xoap::createMessage();
    xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
    xoap::SOAPName responseName = envelope.createName("ConfigResponse", "xdaq", XDAQ_NS_URI);
    envelope.getBody().addBodyElement ( responseName );  
    return reply;      
}

void xdaqTTCvi::AddAddressInfo(AddressTableItem* item)
{
  addressInfo.Add(item->getKey(), item);
}

void xdaqTTCvi::BuildAddressInfo()
{
  //* TTCvi address map
  //*****************************************************************************************************************
  //*  key                 AM       width    address     mask      read write   description
  //*****************************************************************************************************************

  //* short format asynchronous broadcast command
  AddAddressInfo(new TAddressTableItemExt("BChannelShortBroadcast",	0x39,       2,        0x000000c4,    0x000000ff,    false,    true, ""));

  //* long-format async adressed access (broadcast if addr = 0)
  //* write starts after LSB is loaded. No need to reload MSB if unchanged.
  AddAddressInfo(new TAddressTableItemExt("BChannelLongData",	0x39,       2,        0x000000c0,    0x000000ff,    false,    true, ""));
  AddAddressInfo(new TAddressTableItemExt("BChannelLongSubAddr",	0x39,       2,        0x000000c0,    0x0000ff00,    false,    true, ""));
  AddAddressInfo(new TAddressTableItemExt("BChannelLongMSB",	0x39,       2,        0x000000c0,    0x0000ffff,    false,    true, ""));
  AddAddressInfo(new TAddressTableItemExt("BChannelLongExt",	0x39,       2,        0x000000c2,    0x00000001,    false,    true,    "0= int, 1=ext regs"));
  AddAddressInfo(new TAddressTableItemExt("BChannelLongAddr",	0x39,       2,        0x000000c2,    0x00007ffe,    false,    true,    "14 bit TTCrx addr"));
  AddAddressInfo(new TAddressTableItemExt("BChannelLongOne",	0x39,       2,        0x000000c2,    0x00008000,    false,    true,    "always set to 1"));
  AddAddressInfo(new TAddressTableItemExt("BChannelLongLSB",	0x39,       2,        0x000000c2,    0x0000ffff,    false,    true, ""));

  //* CSR1
  AddAddressInfo(new TAddressTableItemExt("CSR1",       	0x39,       2,        0x00000080,    0x0000ffff,    true,    true, ""));
  AddAddressInfo(new TAddressTableItemExt("L1ATriggerSelect",	0x39,       2,        0x00000080,    0x00000007,    true,    true,  "4:VME 5:random 0-3:front panel"));
  AddAddressInfo(new TAddressTableItemExt("OrbitSignalSelect",	0x39,       2,        0x00000080,    0x00000008,    true,    true,	"0=external "));
  AddAddressInfo(new TAddressTableItemExt("L1AFIFOFull",	0x39,       2,        0x00000080,    0x00000010,    true,    false, ""));
  AddAddressInfo(new TAddressTableItemExt("L1AFIFOEmpty",	0x39,       2,        0x00000080,    0x00000020,    true,    false, ""));
  AddAddressInfo(new TAddressTableItemExt("L1AFIFOReset",	0x39,       2,        0x00000080,    0x00000040,    false,    true, ""));
  AddAddressInfo(new TAddressTableItemExt("VMETransferPending",	0x39,       2,        0x00000080,    0x00000080,    true,    false, ""));
  AddAddressInfo(new TAddressTableItemExt("BCDelay",    	0x39,       2,        0x00000080,    0x00000F00,    true,    true,      "Must be inverted"));
  AddAddressInfo(new TAddressTableItemExt("RandomTriggerRate",	0x39,       2,        0x00000080,    0x00007000,    true,    true, ""));
  AddAddressInfo(new TAddressTableItemExt("CountSelect",	0x39,       2,        0x00000080,    0x00008000,    true,    true,      "0=event 1=orbit"));

  //*CSR2 : FIFO status
  AddAddressInfo(new TAddressTableItemExt("CSR2",       	0x39,       2,        0x00000082,    0x0000ffff,    true,    true, ""));
  AddAddressInfo(new TAddressTableItemExt("BGoFIFO0Empty",	0x39,       2,        0x00000082,    0x00000001,    true,    false, ""));
  AddAddressInfo(new TAddressTableItemExt("BGoFIFO0Full",	0x39,       2,        0x00000082,    0x00000002,    true,    false, ""));
  AddAddressInfo(new TAddressTableItemExt("BGoFIFO1Empty",	0x39,       2,        0x00000082,    0x00000004,    true,    false, ""));
  AddAddressInfo(new TAddressTableItemExt("BGoFIFO1Full",	0x39,       2,        0x00000082,    0x00000008,    true,    false, ""));
  AddAddressInfo(new TAddressTableItemExt("BGoFIFO2Empty",	0x39,       2,        0x00000082,    0x00000010,	true,    false, ""));
  AddAddressInfo(new TAddressTableItemExt("BGoFIFO2Full",	0x39,       2,        0x00000082,    0x00000020,	true,    false, ""));
  AddAddressInfo(new TAddressTableItemExt("BGoFIFO3Empty",	0x39,       2,        0x00000082,    0x00000040,	true,    false, ""));
  AddAddressInfo(new TAddressTableItemExt("BGoFIFO3Full",	0x39,       2,        0x00000082,    0x00000080,	true,    false, ""));
  AddAddressInfo(new TAddressTableItemExt("BGoFIFO0Retransmit",	0x39,       2,        0x00000082,    0x00000100,	true,    true,     "set to 0 when empty "));
  AddAddressInfo(new TAddressTableItemExt("BGoFIFO1Retransmit",	0x39,       2,        0x00000082,    0x00000200,	true,    true,     "set to 0 when empty "));
  AddAddressInfo(new TAddressTableItemExt("BGoFIFO2Retransmit",	0x39,       2,        0x00000082,    0x00000400,	true,    true,     "set to 0 when empty "));
  AddAddressInfo(new TAddressTableItemExt("BGoFIFO3Retransmit",	0x39,       2,        0x00000082,    0x00000800,	true,    true,     "set to 0 when empty"));
  AddAddressInfo(new TAddressTableItemExt("BGoFIFO0Reset",	0x39,       2,        0x00000082,    0x00001000,	true,    true,     "set to 1"));
  AddAddressInfo(new TAddressTableItemExt("BGoFIFO1Reset",	0x39,       2,        0x00000082,    0x00002000,	true,    true,     "set to 1"));
  AddAddressInfo(new TAddressTableItemExt("BGoFIFO2Reset",	0x39,       2,        0x00000082,    0x00004000,	true,    true,     "set to 1"));
  AddAddressInfo(new TAddressTableItemExt("BGoFIFO3Reset",	0x39,       2,        0x00000082,    0x00008000,	true,    true,     "set to 1"));

  AddAddressInfo(new TAddressTableItemExt("SoftReset",	0x39,       2,        0x00000084,    0x00000000,	false,    true,     "data-less"));

  AddAddressInfo(new TAddressTableItemExt("L1ASend",	0x39,       2,        0x00000086,    0x00000000,	false,    true,     "data-less"));

  AddAddressInfo(new TAddressTableItemExt("CounterMSB",	0x39,       2,        0x00000088,    0x000000ff,	true,    true,   "bits 23 to 16, event or orbit"));
  AddAddressInfo(new TAddressTableItemExt("CounterLSB",	0x39,       2,        0x0000008a,    0x0000ffff,	true,    true,   "bits 15 to 0, event or orbit"));

  AddAddressInfo(new TAddressTableItemExt("CounterReset",	0x39,       2,        0x0000008c,    0x00000000,	false,    true,   "data-less, event or orbit"));

  AddAddressInfo(new TAddressTableItemExt("Inhibit0Delay",	0x39,       2,        0x00000092,    0x00000fff,	true,    true, ""));
  AddAddressInfo(new TAddressTableItemExt("Inhibit0Duration",	0x39,       2,        0x00000094,    0x000000ff,	true,    true,   "no inhibit<0> if =0"));
  AddAddressInfo(new TAddressTableItemExt("Inhibit1Delay",	0x39,       2,        0x0000009a,    0x00000fff,	true,    true, ""));
  AddAddressInfo(new TAddressTableItemExt("Inhibit1Duration",	0x39,       2,        0x0000009c,    0x000000ff,	true,    true,   "no inhibit<1> if =0"));
  AddAddressInfo(new TAddressTableItemExt("Inhibit2Delay",	0x39,       2,        0x000000a2,    0x00000fff,	true,    true, ""));
  AddAddressInfo(new TAddressTableItemExt("Inhibit2Duration",	0x39,       2,        0x000000a4,    0x000000ff,	true,    true,   "no inhibit<2> if =0"));
  AddAddressInfo(new TAddressTableItemExt("Inhibit3Delay",	0x39,       2,        0x000000aa,    0x00000fff,	true,    true, ""));
  AddAddressInfo(new TAddressTableItemExt("Inhibit3Duration",	0x39,       2,        0x000000ac,    0x000000ff,	true,    true,   "no inhibit<3> if =0"));

  AddAddressInfo(new TAddressTableItemExt("BGo0Mode",	0x39,       2,        0x00000090,    0x0000000f,    true,    true,   "0=enable 1=disable"));
  AddAddressInfo(new TAddressTableItemExt("BGo0Enable",	0x39,       2,        0x00000090,    0x00000001,    true,    true,   "0=enable 1=disable"));
  AddAddressInfo(new TAddressTableItemExt("BGo0Sync",	0x39,       2,        0x00000090,    0x00000002,    true,    true,   "0=sync 1=async"));
  AddAddressInfo(new TAddressTableItemExt("BGo0Single",	0x39,       2,        0x00000090,    0x00000004,    true,    true,   "0=single 1=repeat"));
  AddAddressInfo(new TAddressTableItemExt("BGo0FIFO",	0x39,       2,        0x00000090,    0x00000008,    true,    true,   "0=start if not empty 1=ignore"));
  AddAddressInfo(new TAddressTableItemExt("BGo0Zero",	0x39,       2,        0x00000090,    0x00000010,	true,    true,   "must be 0"));
  AddAddressInfo(new TAddressTableItemExt("BGo0Send",	0x39,       2,        0x00000096,    0x00000000,	false,   true,   "dataless"));

  AddAddressInfo(new TAddressTableItemExt("BGo1Mode",	0x39,       2,        0x00000098,    0x0000000f,    true,    true,   "0=enable 1=disable"));
  AddAddressInfo(new TAddressTableItemExt("BGo1Enable",	0x39,       2,        0x00000098,    0x00000001,    true,    true,   "0=enable 1=disable"));
  AddAddressInfo(new TAddressTableItemExt("BGo1Sync",	0x39,       2,        0x00000098,    0x00000002,    true,    true,   "0=sync 1=async"));
  AddAddressInfo(new TAddressTableItemExt("BGo1Single",	0x39,       2,        0x00000098,    0x00000004,    true,    true,   "0=single 1=repeat"));
  AddAddressInfo(new TAddressTableItemExt("BGo1FIFO",	0x39,       2,        0x00000098,    0x00000008,    true,    true,   "0=start if not empty 1=ignore"));
  AddAddressInfo(new TAddressTableItemExt("BGo1Zero",	0x39,       2,        0x00000098,    0x00000010,	true,    true,   "must be 0"));
  AddAddressInfo(new TAddressTableItemExt("BGo1Send",	0x39,       2,        0x0000009e,    0x00000000,	false,   true,   "dataless"));

  AddAddressInfo(new TAddressTableItemExt("BGo2Mode",	0x39,       2,        0x000000a0,    0x0000000f,    true,    true,   "0=enable 1=disable"));
  AddAddressInfo(new TAddressTableItemExt("BGo2Enable",	0x39,       2,        0x000000a0,    0x00000001,    true,    true,   "0=enable 1=disable"));
  AddAddressInfo(new TAddressTableItemExt("BGo2Sync",	0x39,       2,        0x000000a0,    0x00000002,    true,    true,   "0=sync 1=async"));
  AddAddressInfo(new TAddressTableItemExt("BGo2Single",	0x39,       2,        0x000000a0,    0x00000004,    true,    true,   "0=single 1=repeat"));
  AddAddressInfo(new TAddressTableItemExt("BGo2FIFO",	0x39,       2,        0x000000a0,    0x00000008,    true,    true,   "0=start if not empty 1=ignore"));
  AddAddressInfo(new TAddressTableItemExt("BGo2Zero",	0x39,       2,        0x000000a0,    0x00000010,	true,    true,   "must be 0"));
  AddAddressInfo(new TAddressTableItemExt("BGo2Send",	0x39,       2,        0x000000a6,    0x00000000,	false,   true,   "dataless"));

  AddAddressInfo(new TAddressTableItemExt("BGo3Mode",	0x39,       2,        0x000000a8,    0x0000000f,    true,    true,   "0=enable 1=disable"));
  AddAddressInfo(new TAddressTableItemExt("BGo3Enable",	0x39,       2,        0x000000a8,    0x00000001,    true,    true,   "0=enable 1=disable"));
  AddAddressInfo(new TAddressTableItemExt("BGo3Sync",	0x39,       2,        0x000000a8,    0x00000002,    true,    true,   "0=sync 1=async"));
  AddAddressInfo(new TAddressTableItemExt("BGo3Single",	0x39,       2,        0x000000a8,    0x00000004,    true,    true,   "0=single 1=repeat"));
  AddAddressInfo(new TAddressTableItemExt("BGo3FIFO",	0x39,       2,        0x000000a8,    0x00000008,    true,    true,   "0=start if not empty 1=ignore"));
  AddAddressInfo(new TAddressTableItemExt("BGo3Zero",	0x39,       2,        0x000000a8,    0x00000010,	true,    true,   "must be 0"));
  AddAddressInfo(new TAddressTableItemExt("BGo3Send",	0x39,       2,        0x000000ae,    0x00000000,	false,   true,   "dataless"));

  AddAddressInfo(new TAddressTableItemExt("TrigWordAddress",	0x39,       2,        0x000000c8,    0x00003fff,	true,    true, ""));
  AddAddressInfo(new TAddressTableItemExt("TrigWordLSW",	0x39,       2,        0x000000ca,    0x00003fff,	true,    true, ""));
  AddAddressInfo(new TAddressTableItemExt("TrigWordSubAddress",	0x39,       2,        0x000000ca,    0x000000fc,	true,    true, ""));
  AddAddressInfo(new TAddressTableItemExt("TrigWordSize",	0x39,       2,        0x000000ca,    0x00000200,	true,    true,	"0=short frames= no counter/trig type transfer"));
  AddAddressInfo(new TAddressTableItemExt("TrigWordIE", 	0x39,       2,        0x000000ca,    0x00000100,	true,    true,	"0=int 1=ext"));

  AddAddressInfo(new TAddressTableItemExt("BData0Data", 	0x39,       4,        0x000000b0,    0xffffffff  ,	true,    true,	"32 bit"));
  AddAddressInfo(new TAddressTableItemExt("BData0LongData",	0x39,       2,        0x000000b2,    0x000000ff  ,	true,    true, ""));
  AddAddressInfo(new TAddressTableItemExt("BData0LongSubAddr",	0x39,       2,        0x000000b2,    0x0000ff00  ,	true,    true, ""));
  AddAddressInfo(new TAddressTableItemExt("BData0LongE",	0x39,       2,        0x000000b0,    0x00000001  ,	true,    true,	"??"));
  AddAddressInfo(new TAddressTableItemExt("BData0LongAddr",	0x39,       2,        0x000000b0,    0x00007ffe  ,	true,    true,	"14 bit TTCrx address"));
  AddAddressInfo(new TAddressTableItemExt("BData0One",  	0x39,       2,        0x000000b0,    0x00008000  ,	true,    true,	"must be 1"));
  AddAddressInfo(new TAddressTableItemExt("BData0ShortCommand",	0x39,       2,        0x000000b0,    0x00007f80  ,	true,    true,	"8 bit command"));

  AddAddressInfo(new TAddressTableItemExt("BData1Data", 	0x39,       4,        0x000000b4,    0xffffffff  ,	true,    true,	"32 bit"));
  AddAddressInfo(new TAddressTableItemExt("BData1LongData",	0x39,       2,        0x000000b6,    0x000000ff  ,	true,    true, ""));
  AddAddressInfo(new TAddressTableItemExt("BData1LongSubAddr",	0x39,       2,        0x000000b6,    0x0000ff00  ,	true,    true, ""));
  AddAddressInfo(new TAddressTableItemExt("BData1LongE",	0x39,       2,        0x000000b4,    0x00000001  ,	true,    true,	"??"));
  AddAddressInfo(new TAddressTableItemExt("BData1LongAddr",	0x39,       2,        0x000000b4,    0x00007ffe  ,	true,    true,	"14 bit TTCrx address"));
  AddAddressInfo(new TAddressTableItemExt("BData1One",  	0x39,       2,        0x000000b4,    0x00008000  ,	true,    true,	"must be 1"));
  AddAddressInfo(new TAddressTableItemExt("BData1ShortCommand",	0x39,       2,        0x000000b4,    0x00007f80  ,	true,    true,	"8 bit command"));

  AddAddressInfo(new TAddressTableItemExt("BData2Data", 	0x39,       4,        0x000000b8,    0xffffffff  ,	true,    true,	"32 bit"));
  AddAddressInfo(new TAddressTableItemExt("BData2LongData",	0x39,       2,        0x000000ba,    0x000000ff  ,	true,    true, ""));
  AddAddressInfo(new TAddressTableItemExt("BData2LongSubAddr",	0x39,       2,        0x000000ba,    0x0000ff00  ,	true,    true, ""));
  AddAddressInfo(new TAddressTableItemExt("BData2LongE",	0x39,       2,        0x000000b8,    0x00000001  ,	true,    true,	"??"));
  AddAddressInfo(new TAddressTableItemExt("BData2LongAddr",	0x39,       2,        0x000000b8,    0x00007ffe  ,	true,    true,	"14 bit TTCrx address"));
  AddAddressInfo(new TAddressTableItemExt("BData2One",  	0x39,       2,        0x000000b8,    0x00008000  ,	true,    true,	"must be 1"));
  AddAddressInfo(new TAddressTableItemExt("BData2ShortCommand",	0x39,       2,        0x000000b8,    0x00007f80  ,	true,    true,	"8 bit command"));

  AddAddressInfo(new TAddressTableItemExt("BData3Data", 	0x39,       4,        0x000000bc,    0xffffffff  ,	true,    true,	"32 bit"));
  AddAddressInfo(new TAddressTableItemExt("BData3LongData",	0x39,       2,        0x000000be,    0x000000ff  ,	true,    true, ""));
  AddAddressInfo(new TAddressTableItemExt("BData3LongSubAddr",	0x39,       2,        0x000000be,    0x0000ff00  ,	true,    true, ""));
  AddAddressInfo(new TAddressTableItemExt("BData3LongE",	0x39,       2,        0x000000bc,    0x00000001  ,	true,    true,	"??"));
  AddAddressInfo(new TAddressTableItemExt("BData3LongAddr",	0x39,       2,        0x000000bc,    0x00007ffe  ,	true,    true,	"14 bit TTCrx address"));
  AddAddressInfo(new TAddressTableItemExt("BData3One",  	0x39,       2,        0x000000bc,    0x00008000  ,	true,    true,	"must be 1"));
  AddAddressInfo(new TAddressTableItemExt("BData3ShortCommand",	0x39,       2,        0x000000bc,    0x00007f80  ,	true,    true,	"8 bit command"));

  AddAddressInfo(new TAddressTableItemExt("Manufacturer2",	0x39,       2,        0x00000026,    0x000000ff,	true,    true,  "MSByte"));
  AddAddressInfo(new TAddressTableItemExt("Manufacturer1",	0x39,       2,        0x0000002a,    0x000000ff,	true,    true, ""));
  AddAddressInfo(new TAddressTableItemExt("Manufacturer0",	0x39,       2,        0x0000002e,    0x000000ff,	true,    true,  "LSByte"));
  AddAddressInfo(new TAddressTableItemExt("SerialNo3",	0x39,       2,        0x00000032,    0x000000ff,	true,    true,  "MSByte"));
  AddAddressInfo(new TAddressTableItemExt("SerialNo2",	0x39,       2,        0x00000036,    0x000000ff,	true,    true, ""));
  AddAddressInfo(new TAddressTableItemExt("SerialNo1",	0x39,       2,        0x0000003a,    0x000000ff,	true,    true, ""));
  AddAddressInfo(new TAddressTableItemExt("SerialNo0",	0x39,       2,        0x0000003e,    0x000000ff,	true,    true,  "LSByte"));
  AddAddressInfo(new TAddressTableItemExt("Revision3",	0x39,       2,        0x00000042,    0x000000ff,	true,    true,  "MSByte"));
  AddAddressInfo(new TAddressTableItemExt("Revision2",	0x39,       2,        0x00000046,    0x000000ff,	true,    true, ""));
  AddAddressInfo(new TAddressTableItemExt("Revision1",	0x39,       2,        0x0000004a,    0x000000ff,	true,    true, ""));
  AddAddressInfo(new TAddressTableItemExt("Revision0",	0x39,       2,        0x0000004e,    0x000000ff,	true,    true,  "LSByte"));
}



