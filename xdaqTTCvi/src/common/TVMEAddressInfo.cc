#include "TVMEAddressInfo.h"
#include "hal/IllegalOperationException.hh"



using namespace HAL;

TVMEAddressInfo::~TVMEAddressInfo()
{
	if (Reader == NULL) {
		TItemMap::iterator iItem = ItemMap.begin();
		for(; iItem != ItemMap.end(); ++iItem)
			delete iItem->second;
	}
}


AddressTableReader& TVMEAddressInfo::GetAddressTableReader()
{
	if (Reader == NULL) {
		Reader = new TAddressTableReader(ItemMap);		
		return *Reader;
	};
	
	throw IllegalOperationException(
	  "Subsequent call of TVMEAddressInfo::GetAddressTableReader()", 
      __FILE__, __LINE__, __FUNCTION__ ); 
}


TVMEAddressInfo::TAddressTableReader::TAddressTableReader(
                        TItemMap& itemMap)
{
	TItemMap::iterator iItem = itemMap.begin();
	for(; iItem != itemMap.end(); ++iItem)
		itemPointerList.push_back(iItem->second);
}


