#include "processcontrol/exception/SystemException.h"

#include <string.h>
#include <errno.h>

#include <sstream>

namespace processcontrol {
namespace exception {

std::string SystemException::strerror_r(int _errno)
{
    std::string _description(256, ' ');
    if (!::strerror_r(_errno, &(_description.at(0)), 256)) {
        std::ostringstream _osstream("SystemException ");
        _osstream << _errno << ": could not retrieve description";
        return _osstream.str();
    }
    return _description.substr(0, _description.find('\0'));
}

SystemException::SystemException(std::string const & _name, int _errno
                                 , std::string const & _module, int _line, std::string const & _function)
    : Exception(_name, strerror_r(_errno)
                , _module, _line, _function)
{}

SystemException::SystemException(std::string const & _name, std::string const & _message
                                 , std::string const & _module, int _line, std::string const & _function)
    : Exception(_name, _message + ": " + strerror_r(errno)
                , _module, _line, _function)
{}

SystemException::SystemException(std::string const & _name, int _errno
                                 , std::string const & _module, int _line, std::string const & _function
                                 , xcept::Exception & _error)
    : Exception(_name, strerror_r(_errno)
                , _module, _line, _function
                , _error)
{}

SystemException::SystemException(std::string const & _name, std::string const & _message
                                 , std::string const & _module, int _line, std::string const & _function
                                 , xcept::Exception & _error)
    : Exception(_name, _message + ": " + strerror_r(errno)
                , _module, _line, _function
                , _error)
{}

} // namespace exception
} // namespace processcontrol
