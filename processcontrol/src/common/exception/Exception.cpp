#include "processcontrol/exception/Exception.h"

namespace processcontrol {
namespace exception {

Exception::Exception(std::string const & _name, std::string const & _message
                     , std::string const & _module, int _line, std::string const & _function)
    : xcept::Exception(_name, _message
                       , _module, _line, _function)
{}

Exception::Exception(std::string const & _name, std::string const & _message
                     , std::string const & _module, int _line, std::string const & _function
                     , xcept::Exception & _error)
    : xcept::Exception(_name, _message
                       , _module, _line, _function
                       , _error)
{}

} // namespace exception
} // namespace processcontrol
