#include "processcontrol/Process.h"

#include<ostream>

#include "xdata/AbstractBag.h"

namespace processcontrol {

Process::Process(std::string const & _group
                 , std::string const & _host
                 , std::string const & _zone
                 , std::string const & _service)
    : group_(_group)
    , host_(_host)
    , zone_(_zone)
    , service_(_service)
{}

void Process::registerFields(xdata::AbstractBag * _bag)
{
    _bag->addField("group"  , &group_);
    _bag->addField("host"   , &host_);
    _bag->addField("zone"   , &zone_);
    _bag->addField("service", &service_);
}

std::string Process::getDescription() const
{
    return getGroup() + '.' + getHost() + '.' + getZone() + '.' + getService();
}

std::ostream & operator<<(std::ostream & _ostream, Process const & _process)
{
    return _ostream << _process.getGroup() << '.' << _process.getHost()
                    << '.' << _process.getZone() << '.' << _process.getService();
}

} // namespace processcontrol
