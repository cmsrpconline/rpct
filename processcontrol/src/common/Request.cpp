#include "processcontrol/Request.h"

#include "toolbox/task/Guard.h"
#include "xdata/AbstractBag.h"

namespace processcontrol {

char const * const Request::char_action_names_[]
= { "is-active" //"status"
   , "start"
   , "stop"
   , "restart"
   , "try_restart"
   , "reload"
   , "force_reload"
   , "unknown"
   , "destroy"
};

std::vector<std::string> const Request::action_names_(char_action_names_
                                                      , char_action_names_ + sizeof(char_action_names_) / sizeof(char const *));

std::string const Request::unknown_name_("unknown");

toolbox::BSem Request::max_id_mutex_(toolbox::BSem::FULL);
unsigned int Request::max_id_(0);

Request::Request(std::string const & _response_listener
                 , unsigned int _action
                 , Process const & _process
                 , unsigned int _timeout)
    : response_listener_(_response_listener)
    , id_(0)
    , action_(_action)
    , timeout_(_timeout)
{
    process_.bag = _process;
    setId();
    setTime();
}

void Request::setId(unsigned int _id)
{
    if (_id == 0) {
        toolbox::task::Guard<toolbox::BSem> _guard(max_id_mutex_);
        id_.value_ = ++max_id_;
    } else
        id_.value_ = _id;
}

void Request::registerFields(xdata::AbstractBag * _bag)
{
    _bag->addField("responseListener" , &response_listener_);
    _bag->addField("id"     , &id_);
    _bag->addField("action" , &action_);
    _bag->addField("process", &process_);
    _bag->addField("time"   , &time_);
    _bag->addField("timeout", &timeout_);
}

} // namespace processcontrol
