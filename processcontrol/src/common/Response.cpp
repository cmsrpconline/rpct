#include "processcontrol/Response.h"

#include "xercesc/util/XercesDefs.hpp"
#include "xercesc/util/Base64.hpp"

#include "xdata/AbstractBag.h"

namespace processcontrol {

char const * const Response::char_return_descriptions_[]
= {"ok"
   , "generic or unspecified error"
   , "invalid or excess argument(s)"
   , "unimplemented feature"
   , "user had insufficient privilege"
   , "program is not installed"
   , "program is not configured"
   , "program is not running"
};

char const * const Response::char_status_return_descriptions_[]
= {"program is running or service is OK"
   , "program is dead and /var/run pid file exists"
   , "program is dead and /var/lock lock file exists"
   , "program is not running"
   , "program or service status is unknown"
};

char const * const Response::char_other_descriptions_[]
= {"not set"
   , "queued"
   , "failed"
   , "passed"
   , "warning"
   , "unknown"
};

std::vector<std::string> const Response::return_descriptions_(char_return_descriptions_
                                                              , char_return_descriptions_ + sizeof(char_return_descriptions_)/sizeof(char const *));
std::vector<std::string> const Response::status_return_descriptions_(char_status_return_descriptions_
                                                                     , char_status_return_descriptions_ + sizeof(char_status_return_descriptions_)/sizeof(char const *));
std::vector<std::string> const Response::other_descriptions_(char_other_descriptions_
                                                             , char_other_descriptions_ + sizeof(char_other_descriptions_)/sizeof(char const *));
std::string const Response::unknown_description_("unknown");

Response::Response(unsigned int _id
                   , unsigned int _action
                   , toolbox::TimeVal const & _time
                   , unsigned int _return_value
                   , std::string const & _description)
    : id_(_id)
    , action_(_action)
    , time_(_time)
    , return_value_(_return_value)
{
    setDescription(_description);
}

std::string Response::getDescription() const
{
    std::string _description;
    XMLSize_t _length(0);
    unsigned char * _buffer
        = xercesc::Base64::decode((unsigned char *)(description_.value_.c_str())
                                  , &_length);
    if (_buffer) {
        _description.assign((char const *)_buffer, _length);
        delete _buffer;
    }
    return _description;
}

void Response::setDescription(std::string const & _description)
{
    XMLSize_t _length(0);
    unsigned char * _buffer
        = xercesc::Base64::encode((unsigned char const *)(_description.c_str())
                                  , _description.size()
                                  , &_length);
    if (_buffer) {
        description_.value_ = (char const *)(_buffer);
        delete _buffer;
    }
}

void Response::registerFields(xdata::AbstractBag * _bag)
{
    _bag->addField("id", &id_);
    _bag->addField("action", &action_);
    _bag->addField("time", &time_);
    _bag->addField("returnValue", &return_value_);
    _bag->addField("description", &description_);
}

} // namespace processcontrol
