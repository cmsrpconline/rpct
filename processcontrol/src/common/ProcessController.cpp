#include "processcontrol/ProcessController.h"

#include "toolbox/net/URL.h"
#include "xdaq/Application.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/ContextDescriptor.h"

namespace processcontrol {

ProcessController::ProcessController(xdaq::Application & _application
                                     , CallbackInterface & _callback
                                     , unsigned int _max_timeout)
    : application_(_application)
    , logger_(log4cplus::Logger::getInstance(application_.getApplicationLogger().getName()
                                             + ".ProcessControlProcessController"))
    , callback_(_callback)
    , url_(application_.getApplicationDescriptor()->getContextDescriptor()->getURL()
           + "/" + application_.getApplicationDescriptor()->getURN())
    , host_(toolbox::net::URL(application_.getApplicationDescriptor()->getContextDescriptor()->getURL()).getHost())
    , mutex_(toolbox::BSem::FULL)
    , max_timeout_(_max_timeout)
{}

ProcessController::~ProcessController()
{}

} // namespace processcontrol
