#include "processcontrol/Client.h"

#include "log4cplus/loggingmacros.h"

#include "toolbox/task/Guard.h"
#include "toolbox/utils.h"
#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/ApplicationGroup.h"
#include "xdaq/NamespaceURI.h"
#include "xdata/Vector.h"
#include "xdata/soap/Serializer.h"
#include "xoap/MessageFactory.h"
#include "xoap/Method.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPElement.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPName.h"

namespace processcontrol {

std::string const Client::service_classname_("processcontrol::Application");
std::string const Client::service_name_("processcontrol-service");

Client::Client(xdaq::Application & _application
               , CallbackInterface & _callback
               , unsigned int _max_timeout)
    : ProcessController(_application, _callback, _max_timeout)
{
    // Find services
    std::set<const xdaq::ApplicationDescriptor*> _descriptors
        = application_.getApplicationContext()->getDefaultZone()->getApplicationDescriptors(service_classname_);

    for (std::set<const xdaq::ApplicationDescriptor*>::const_iterator _descriptor = _descriptors.begin()
             ; _descriptor != _descriptors.end() ; ++_descriptor) {
        LOG4CPLUS_INFO(logger_, "line " << __LINE__);
        if ((*_descriptor)->getAttribute("service") == service_name_) {
            LOG4CPLUS_INFO(logger_, "line " << __LINE__ << " " << toolbox::net::URL((*_descriptor)->getContextDescriptor()->getURL()).getHost());
            host_service_.insert(std::pair<std::string
                                 ,const xdaq::ApplicationDescriptor *>(toolbox::net::URL((*_descriptor)->getContextDescriptor()->getURL()).getHost()
                                                                  , *_descriptor));
        }
    }

    // Find processes
    std::set<std::string> _zones = application_.getApplicationContext()->getZoneNames();
    for (std::set<std::string>::const_iterator _zone_it = _zones.begin()
             ; _zone_it != _zones.end() ; ++_zone_it) {
        std::string const & _zone(*_zone_it);
        std::set<const xdaq::ApplicationGroup *> _groups
            = application_.getApplicationContext()->getZone(_zone)->getGroups();
        for (std::set<const xdaq::ApplicationGroup *>::const_iterator _group_it = _groups.begin()
                 ; _group_it != _groups.end() ; ++_group_it) {
            std::string _group((*_group_it)->getName());
            std::set<const xdaq::ApplicationDescriptor *> _descriptors
                = (*_group_it)->getApplicationDescriptors();
            for (std::set<const xdaq::ApplicationDescriptor *>::const_iterator _descriptor_it = _descriptors.begin()
                     ; _descriptor_it != _descriptors.end() ; ++_descriptor_it) {
            	const xdaq::ApplicationDescriptor & _descriptor(*(*_descriptor_it));
                if (_descriptor.getClassName() != "processcontrol::Application") {
                    std::string _host(toolbox::net::URL(_descriptor.getContextDescriptor()->getURL()).getHost());
                    std::string _service(_descriptor.getAttribute("process"));
                    std::string _app_zone(_descriptor.getAttribute("zone"));
                    if (_app_zone.empty())
                        _app_zone = _zone;
                    LOG4CPLUS_INFO(logger_, "line " << __LINE__ << " " << Process(_group, _host, _app_zone, _service));
                    if (!_service.empty() && host_service_.count(_host))
                        insertProcess(Process(_group, _host, _app_zone, _service));
                }
            }
        }
    }

    xoap::deferredbind(&_application, this, &Client::xoapResponse, "ProcessControlResponse" , XDAQ_NS_URI);
}

Client::~Client()
{
    toolbox::task::Guard<toolbox::BSem> _guard(mutex_);
    for (std::map<unsigned int, Request>::const_iterator _request = id_request_.begin()
             ; _request != id_request_.end() ; ++_request) {
        Response _response(_request->first
                           , _request->second.getAction()
                           , toolbox::TimeVal::gettimeofday()
                           , Response::not_set_
                           , std::string("Client destructor called"));
        callback_(_request->second, _response);
    }
}

Response Client::action(Request _request)
{
    Response _response(_request.getId()
                       , _request.getAction()
                       , toolbox::TimeVal::gettimeofday()
                       , Response::not_set_
                       , _request.getActionName() + " request queued");
    if (!Request::isAction(_request.getAction())) {
        _response.setReturnValue(Response::not_implemented_);
        _response.setDescription(std::string("The request contains an invalid action ")
                                 + _request.getActionName());
        return _response;
    }

    if (!hasProcess(_request.getProcess())) {
        _response.setReturnValue(_request.getAction() == Request::status_
                                 ? Response::status_unknown_
                                 : Response::not_installed_);
        _response.setDescription(std::string("The request contains an unknown process ")
                                 + _request.getProcess().getDescription());
        return _response;
    }

    if (_request.getTimeout() > getMaxTimeout())
        _request.setTimeout(getMaxTimeout());

    const xdaq::ApplicationDescriptor * _origin = application_.getApplicationDescriptor();

    // Find destination
    std::map<std::string, const xdaq::ApplicationDescriptor *>::const_iterator
        _host_service(host_service_.find(_request.getProcess().getHost()));
    if (_host_service == host_service_.end()) {
        _response.setReturnValue(_request.getAction() == Request::status_
                                 ? Response::status_unknown_
                                 : Response::not_installed_);
        _response.setDescription(std::string("Destination descriptor not found for the request (")
                                 + _request.getProcess().getHost() + ")");
        return _response;
    }

    // Create request
    xdata::Vector<xdata::Bag<Request> > _requests;
    xdata::Bag<Request> _request_bag;
    _request_bag.bag = _request;
    _requests.push_back(_request_bag);

    bool _added(false);
    try {
        // Create message
        xoap::MessageReference _message = xoap::createMessage();
        xoap::SOAPEnvelope _envelope = _message->getSOAPPart().getEnvelope();

        _envelope.addNamespaceDeclaration("xsi"    , "http://www.w3.org/2001/XMLSchema-instance");
        _envelope.addNamespaceDeclaration("xsd"    , "http://www.w3.org/2001/XMLSchema");
        _envelope.addNamespaceDeclaration("soapenc", "http://schemas.xmlsoap.org/soap/encoding/");

        xoap::SOAPName _request_name = _envelope.createName("ProcessControlRequest", "xdaq", XDAQ_NS_URI);
        xoap::SOAPBodyElement _request_element = _envelope.getBody().addBodyElement(_request_name);

        xdata::soap::Serializer _serializer;
        try {
            _serializer.exportAll(&_requests, dynamic_cast<DOMElement*>(_request_element.getDOM()));
        } catch (xdata::exception::Exception & _error) {
            XCEPT_RETHROW(processcontrol::exception::Exception
                          , "Could not serialize Requests", _error);
        }

        { // Add request
            toolbox::task::Guard<toolbox::BSem> _guard(mutex_);
            id_request_.insert(std::pair<unsigned int, Request>(_request.getId(), _request));
            _added = true;

            // and clean out old requests
            if (!id_request_.empty()) {
                toolbox::TimeVal _now(toolbox::TimeVal::gettimeofday());
                for (std::map<unsigned int, Request>::iterator _id_request = id_request_.begin()
                         ; _id_request != id_request_.end() ; ) {
                    if ((_id_request->second.getTime()
                         + toolbox::TimeVal(_id_request->second.getTimeout()))
                        < _now)
                        id_request_.erase(_id_request++);
                    else
                        ++_id_request;
                }
            }
        }

        xoap::MessageReference _reply;
        try { // Post SOAP
            _reply
                = application_.getApplicationContext()->postSOAP(_message
                                                                 , *_origin
                                                                 , *(_host_service->second));
        } catch (xoap::exception::Exception & _error) {
            XCEPT_RETHROW(processcontrol::exception::Exception
                          , "Could not send SOAP request", _error);
        }

        // Parse Reply
        xoap::SOAPEnvelope _reply_env = _reply->getDocument()->getDocumentElement();
        xoap::SOAPBody _reply_body = _reply_env.getBody();
        if (_reply_body.hasFault()) {
            XCEPT_RAISE(processcontrol::exception::Exception
                        , std::string("Fault in SOAP Message reply: ")
                        + _reply_body.getFault().getFaultString());
        }

        xdata::Vector<xdata::Bag<Response> > _responses;

        bool _found(false);
        std::vector<xoap::SOAPElement> _body_elements
            = _reply->getSOAPPart().getEnvelope().getBody().getChildElements();
        for (std::vector<xoap::SOAPElement>::iterator _body_element = _body_elements.begin()
                 ; _body_element != _body_elements.end() && !_found ; ++_body_element)
            if (_body_element->getElementName().getLocalName() == "ProcessControlRequest") {
                try {
                    xdata::soap::Serializer _serializer;
                    _serializer.import(&_responses, _body_element->getDOMNode());
                    _found = true;
                } catch (xdata::exception::Exception & _error) {
                    XCEPT_RETHROW(processcontrol::exception::Exception
                                  , "Could not deserialize reply", _error);
                }
            }
        if (!_found || _responses.empty() || _responses.front().bag.getId() != _request.getId())
            XCEPT_RAISE(processcontrol::exception::Exception, "No valid request found in message");

        _response = _responses.front().bag;

    } catch (xcept::Exception const & _error) {
        if (_added) {
            toolbox::task::Guard<toolbox::BSem> _guard(mutex_);
            id_request_.erase(_request.getId());
        }

        _response.setReturnValue(_request.getAction() == Request::status_
                                 ? Response::status_unknown_
                                 : Response::unknown_);
        _response.setDescription(_error.what());
    }

    callback_(_request, _response);

    return _response;
}

xoap::MessageReference Client::xoapResponse(xoap::MessageReference _message)
    throw (xoap::exception::Exception)
{
    xdata::Bag<Response> _response_bag;
    {
        bool _found(false);
        std::vector<xoap::SOAPElement> _body_elements
            = _message->getSOAPPart().getEnvelope().getBody().getChildElements();
        for (std::vector<xoap::SOAPElement>::iterator _body_element = _body_elements.begin()
                 ; _body_element != _body_elements.end() && !_found ; ++_body_element)
            if (_body_element->getElementName().getLocalName() == "ProcessControlResponse") {
                try {
                    xdata::soap::Serializer _serializer;
                    _serializer.import(&_response_bag, _body_element->getDOMNode());
                    _found = true;
                } catch (xdata::exception::Exception & _error) {
                    XCEPT_RETHROW(xoap::exception::Exception
                                  , "Could not deserialize Response", _error);
                }
            }
        if (!_found)
            XCEPT_RAISE(xoap::exception::Exception, "No valid response found in message");
    }

    Request _request;
    {
        toolbox::task::Guard<toolbox::BSem> _guard(mutex_);
        std::map<unsigned int, Request>::iterator _id_request = id_request_.find(_response_bag.bag.getId());
        if (_id_request == id_request_.end())
            XCEPT_RAISE(xoap::exception::Exception, "Received response for unkown request");
        _request = _id_request->second;
        id_request_.erase(_id_request);
    }

    callback_(_request, _response_bag.bag);

    xoap::MessageReference _reply = xoap::createMessage();
    xoap::SOAPEnvelope _envelope = _reply->getSOAPPart().getEnvelope();

    _envelope.addNamespaceDeclaration("xsi"    , "http://www.w3.org/2001/XMLSchema-instance");
    _envelope.addNamespaceDeclaration("xsd"    , "http://www.w3.org/2001/XMLSchema");
    _envelope.addNamespaceDeclaration("soapenc", "http://schemas.xmlsoap.org/soap/encoding/");

    return _reply;
}

} // namespace processcontrol
