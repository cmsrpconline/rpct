#include "processcontrol/Callback.h"

namespace processcontrol {

CallbackInterface::~CallbackInterface()
{}

Callback::Callback(void (* _callback)(Request const &, Response const &))
    : callback_(_callback)
{}

} // namespace processcontrol
