#include "processcontrol/Application.h"

#include <vector>

#include "cgicc/Cgicc.h"

#include "toolbox/net/URL.h"
#include "toolbox/task/Guard.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/ContextDescriptor.h"
#include "xgi/framework/Method.h"

#include "processcontrol/Client.h"
#include "processcontrol/Service.h"

XDAQ_INSTANTIATOR_IMPL(processcontrol::Application);

namespace processcontrol {

std::string const Application::time_format_("%Y-%m-%d %H:%M:%S");

Application::Application(xdaq::ApplicationStub * _application_stub)
    : xdaq::Application(_application_stub)
    , xgi::framework::UIManager(this)
    , callback_(*this, &Application::callback)
    , url_(getApplicationDescriptor()->getContextDescriptor()->getURL()
           + "/" + getApplicationDescriptor()->getURN())
    , host_(toolbox::net::URL(getApplicationDescriptor()->getContextDescriptor()->getURL()).getHost())
    , urn_(getApplicationDescriptor()->getURN())
    , mutex_(toolbox::BSem::FULL)
{
    if (getApplicationDescriptor()->getAttribute("service") == "processcontrol-client")
        controller_ = new Client(*this, callback_);
    else
        controller_ = new Service(*this, callback_);

    _application_stub->getDescriptor()->setAttribute("icon16x16", "/jobcontrol/images/jobcontrol-icon.png");
    _application_stub->getDescriptor()->setAttribute("icon","/jobcontrol/images/jobcontrol-icon.png");

    // Find processes
    std::set<std::string> _zones = getApplicationContext()->getZoneNames();
    for (std::set<std::string>::const_iterator _zone_it = _zones.begin()
             ; _zone_it != _zones.end() ; ++_zone_it) {
        std::string const & _zone(*_zone_it);
        std::set<const xdaq::ApplicationGroup *> _groups
            = getApplicationContext()->getZone(_zone)->getGroups();
        for (std::set<const xdaq::ApplicationGroup *>::const_iterator _group_it = _groups.begin()
                 ; _group_it != _groups.end() ; ++_group_it) {
            std::string _group((*_group_it)->getName());
            std::set<const xdaq::ApplicationDescriptor *> _descriptors
                = (*_group_it)->getApplicationDescriptors();
            for (std::set<const xdaq::ApplicationDescriptor *>::const_iterator _descriptor_it = _descriptors.begin()
                     ; _descriptor_it != _descriptors.end() ; ++_descriptor_it) {
                const xdaq::ApplicationDescriptor & _descriptor(*(*_descriptor_it));
                if (_descriptor.getClassName() != "processcontrol::Application") {
                    std::string _host(toolbox::net::URL(_descriptor.getContextDescriptor()->getURL()).getHost());
                    std::string _service(_descriptor.getAttribute("process"));
                    std::string _app_zone(_descriptor.getAttribute("zone"));
                    if (_app_zone.empty())
                        _app_zone = _zone;
                    Process _process(_group, _host, _app_zone, _service);
                    if (_service.empty() || !controller_->hasProcess(_process))
                        continue;
                    unsigned int _port(toolbox::net::URL(_descriptor.getContextDescriptor()->getURL()).getPort());
                    process_port_response_[_process]
                        = std::pair<unsigned int, Response>(_port, Response());
                }
            }
        }
    }

    xgi::framework::deferredbind(this, this, &processcontrol::Application::xgiDefault, "Default");
    xgi::framework::deferredbind(this, this, &processcontrol::Application::xgiProcessTable, "Process");
    xgi::framework::deferredbind(this, this, &processcontrol::Application::xgiProcessAction, "ProcessAction");
}

Application::~Application()
{
    delete controller_;
}

void Application::xgiDefault(xgi::Input * _in, xgi::Output * _out)
    throw (xgi::exception::Exception)
{
    LOG4CPLUS_INFO(getApplicationLogger(), "Application::xgiDefault");

    // html > body > #xdaq-wrapper > #xdaq-main-wrapper > #xdaq-main >
    // *_out << "<script type=\"text/javascript\" src=\"/jobcontrol/html/js/xdaq-jobcontrol.js\"></script>" << std::endl;

    *_out << "<div class=\"xdaq-tab-wrapper\" id=\"xdaq-processcontrol-tabs\">" << std::endl;
    *_out << "  <div class=\"xdaq-tab\" title=\"processes\" id=\"xdaq-processcontrol-processes-tab\">" << std::endl;
    xgiProcessTable(_in, _out);
    *_out << "  </div>" << std::endl;
    *_out << "</div>" << std::endl;
}

void Application::xgiProcessTable(xgi::Input * _in, xgi::Output * _out)
    throw (xgi::exception::Exception)
{
    toolbox::task::Guard<toolbox::BSem> _lock(mutex_);

    if (process_port_response_.empty()) {
        *_out << "    <p>No processes registered</p>" << std::endl;
        return;
    }

    *_out << "    <table class=\"xdaq-table-tree\" id=\"processcontrol-processes\" data-expand=\"true\">" << std::endl;
    *_out << "      <thead>" << std::endl;
    *_out << "        <tr><th>Group</th><th>Host</th><th>Zone</th><th>Service</th><th>Action</th><th>Last update</th><th style=\"width:400px;\">Last command</th></tr>" << std::endl;
    *_out << "      </thead>" << std::endl;
    *_out << "      <tbody>" << std::endl;
    int _process_count(0), _group_count(-1), _host_count(-1), _zone_count(-1);
    std::string _last_group(":"), _last_host(":"), _last_zone(":");
    for (std::map<Process, std::pair<unsigned int, Response> >::const_iterator _ppr_it = process_port_response_.begin()
             ; _ppr_it != process_port_response_.end() ; ++_ppr_it, ++_process_count) {
        Process const & _process(_ppr_it->first);
        unsigned int _port(_ppr_it->second.first);
        Response const & _response(_ppr_it->second.second);

        if (_process.getGroup() != _last_group) {
            _last_group = _process.getGroup();
            _group_count = _process_count++;
            _last_host = ":";
            xgiProcessRow(_in, _out
                          , Process(_last_group)
                          , _port, _group_count, -1
                          , _response);
        }
        if (_process.getHost() != _last_host) {
            _last_host = _process.getHost();
            _host_count = _process_count++;
            _last_zone = ":";
            xgiProcessRow(_in, _out
                          , Process(_last_group, _last_host)
                          , _port, _host_count, _group_count
                          , _response);
        }
        if (_process.getZone() != _last_zone) {
            _last_zone = _process.getZone();
            _zone_count = _process_count++;
            xgiProcessRow(_in, _out
                          , Process(_last_group, _last_host, _last_zone)
                          , _port, _zone_count, _host_count
                          , _response);
        }
        xgiProcessRow(_in, _out
                      , Process(_last_group, _last_host, _last_zone, _process.getService())
                      , _port, _process_count, _zone_count
                      , _response);
    }
    *_out << "      </tbody>" << std::endl;
    *_out << "    </table>" << std::endl;
}

void Application::xgiProcessRow(xgi::Input * _in, xgi::Output * _out
                                , Process const & _process, unsigned int _port
                                , int _process_count, int _parent_count
                                , Response const & _response)
    throw (xgi::exception::Exception)
{
    *_out << "        <tr data-treeid=\"" << _process_count;
    if (_parent_count >= 0)
        *_out << "\" data-treeparent=\"" << _parent_count << "\">";
    else
        *_out << "\">";
    if (_process.getHost().empty())
        *_out << "\n          <td>" << _process.getGroup() << "</td>";
    else
        *_out << "\n          <td/>";
    if (_process.getZone().empty())
        *_out << "<td>" << _process.getHost() << "</td>";
    else
        *_out << "<td />";
    if (_process.getService().empty())
        *_out << "<td>" << _process.getZone() << "</td><td />";
    else
        *_out << "<td /><td><a target=\"_blank\"href=\"http://"
              << _process.getHost() << ":" << _port  << "\">"
              << _process.getService() << "</a></td>";
    *_out << "\n          <td><form method=\"post\" action=\"/" << urn_ << "/ProcessAction\">"
          << "<input type=\"hidden\" name=\"group\" value=\"" << _process.getGroup() << "\" />"
          << "<input type=\"hidden\" name=\"host\" value=\"" << _process.getHost() << "\" />"
          << "<input type=\"hidden\" name=\"zone\" value=\"" << _process.getZone() << "\" />"
          << "<input type=\"hidden\" name=\"service\" value=\"" << _process.getService() << "\" />"
          << "<input type=\"submit\" name=\"action\" value=\"is-active\" /> " //status
          << "<input type=\"submit\" name=\"action\" value=\"start\" /> "
          << "<input type=\"submit\" name=\"action\" value=\"stop\" /> "
          << "<input type=\"submit\" name=\"action\" value=\"restart\" />"
          << "</form></td>";
    if (_process.getService().empty())
        *_out << "<td /><td />";
    else
        *_out << "\n          <td>" << _response.getTime().toString(time_format_, _response.getTime().tz()) << "</td>"
              << "<td style=\"background-color:"
              << _response.getWarningLevel().getBgColor() << ";\">"
              << _response.getDescription() << "</td>";
    *_out << "\n        </tr>" << std::endl;
}

void Application::xgiProcessAction(xgi::Input * _in, xgi::Output * _out)
    throw (xgi::exception::Exception)
{
    cgicc::Cgicc _cgi(_in);

    cgicc::form_iterator _action_element = _cgi.getElement("action");
    if (_action_element == _cgi.getElements().end()) {
        LOG4CPLUS_WARN(getApplicationLogger(), "Missing action in request");
        return xgiRedirect(_in, _out);
    }
    std::string _action(_action_element->getValue());
    if (!Request::isAction(_action)) {
        LOG4CPLUS_WARN(getApplicationLogger(), "Invalid action in request (" << _action << ")");
        return xgiRedirect(_in, _out);
    }

    cgicc::form_iterator _group_element = _cgi.getElement("group");
    if (_group_element == _cgi.getElements().end()) {
        LOG4CPLUS_WARN(getApplicationLogger(), "Missing group in request");
        return xgiRedirect(_in, _out);
    }
    std::string _group(_group_element->getValue());

    cgicc::form_iterator _host_element = _cgi.getElement("host");
    if (_host_element == _cgi.getElements().end()) {
        LOG4CPLUS_WARN(getApplicationLogger(), "Missing host in request");
        return xgiRedirect(_in, _out);
    }
    std::string _host(_host_element->getValue());

    cgicc::form_iterator _zone_element = _cgi.getElement("zone");
    if (_zone_element == _cgi.getElements().end()) {
        LOG4CPLUS_WARN(getApplicationLogger(), "Missing zone in request");
        return xgiRedirect(_in, _out);
    }
    std::string _zone(_zone_element->getValue());

    cgicc::form_iterator _service_element = _cgi.getElement("service");
    if (_service_element == _cgi.getElements().end()) {
        LOG4CPLUS_WARN(getApplicationLogger(), "Missing service in request");
        return xgiRedirect(_in, _out);
    }
    std::string _service(_service_element->getValue());

    bool _ajax(false);
    cgicc::form_iterator _ajax_element = _cgi.getElement("ajax");
    _ajax = (_ajax_element != _cgi.getElements().end());

    Process _process(_group, _host, _zone, _service);
    std::set<Process> const & _processes(controller_->getProcesses());
    std::set<Process>::const_iterator _process_it(_processes.lower_bound(_process));
    if (_process_it == controller_->getProcesses().end()
        || _process.getGroup() != _process_it->getGroup()
        || (_process.getHost() != _process_it->getHost() && !_process.getHost().empty())
        || (_process.getZone() != _process_it->getZone() && !_process.getZone().empty())
        || (_process.getService() != _process_it->getService() && !_process.getService().empty())) {
        LOG4CPLUS_WARN(getApplicationLogger(), "Requested action for unknown process "
                       << _process);
        return xgiRedirect(_in, _out);
    }

    { // move this logic to the controllers
        std::string _host(_process.getHost());
        std::vector<Request> _requests;
        for ( ; _process_it != _processes.end() ; ++_process_it) {
            if (_process.getGroup() != _process_it->getGroup()
                || (_process.getHost() != _process_it->getHost() && !_process.getHost().empty())
                || (_process.getZone() != _process_it->getZone() && !_process.getZone().empty())
                || (_process.getService() != _process_it->getService() && !_process.getService().empty()))
                break;
            if (_process_it->getHost() == _host && !_requests.empty()) {
                controller_->action(_requests);
                _requests.clear();
                _host = _process_it->getHost();
            }
            _requests.push_back(Request(url_, Request::getAction(_action), *_process_it));
        }
        if (!_requests.empty())
            controller_->action(_requests);
    }

    if (_ajax) {
        xgiProcessTable(_in, _out);
    } else
        xgiRedirect(_in, _out);
}

void Application::callback(Request const & _request, Response const & _response)
{
    toolbox::task::Guard<toolbox::BSem> _lock(mutex_);

    std::map<Process, std::pair<unsigned int, Response> >::iterator _ppr_it
        = process_port_response_.find(_request.getProcess());
    if (_ppr_it == process_port_response_.end())
        return;

    _ppr_it->second.second = _response;

    // now overcome lack of correct return values from xdaqd
    if (_response.getReturnValue() == 0) {
        std::string const & _description(_response.getDescription());
        std::string::size_type _last_line(_description.find_last_of('\n', _description.size() - 1));
        if (_last_line == std::string::npos)
            _last_line = 0;
        if (_description.find("FAILED") != std::string::npos)
            _ppr_it->second.second.setReturnValue(Response::failed_);
    }

    LOG4CPLUS_INFO(getApplicationLogger(), "Request " << _request.getId() << " for "
                   << _request.getActionName() << " on "
                   << _request.getProcess()
                   << " returned "
                   << _response.getReturnValue() << ": "
                   << _response.getReturnDescription() << ": "
                   << _response.getDescription());
}

void Application::xgiRedirect(xgi::Input * _in, xgi::Output * _out)
    throw (xgi::exception::Exception)
{
    std::string _url(url_);
    _out->getHTTPResponseHeader().getStatusCode(303);
    _out->getHTTPResponseHeader().addHeader("Location", _url.c_str());
}

} // namespace processcontrol
