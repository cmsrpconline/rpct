#include "processcontrol/Service.h"

#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/resource.h>

#include <sstream>
#include <iostream>

#include "log4cplus/loggingmacros.h"

#include "pt/PeerTransportAgent.h"
#include "pt/PeerTransportSender.h"
#include "pt/SOAPMessenger.h"
#include "toolbox/utils.h"
#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/ApplicationGroup.h"
#include "xdaq/NamespaceURI.h"
#include "xdaq/Zone.h"
#include "xdata/Vector.h"
#include "xdata/soap/Serializer.h"
#include "xoap/MessageFactory.h"
#include "xoap/Method.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPElement.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPName.h"

#include "processcontrol/exception/SystemException.h"

namespace processcontrol {

Service::Service(xdaq::Application & _application
                 , CallbackInterface & _callback
                 , unsigned int _max_timeout
                 , unsigned int _n_workloops)
    : ProcessController(_application, _callback, _max_timeout)
{
    // Find processes
    std::set<std::string> _zones = application_.getApplicationContext()->getZoneNames();
    for (std::set<std::string>::const_iterator _zone_it = _zones.begin()
             ; _zone_it != _zones.end() ; ++_zone_it) {
        std::string const & _zone(*_zone_it);
        std::set<const xdaq::ApplicationGroup *> _groups
            = application_.getApplicationContext()->getZone(_zone)->getGroups();
        for (std::set<const xdaq::ApplicationGroup *>::const_iterator _group_it = _groups.begin()
                 ; _group_it != _groups.end() ; ++_group_it) {
            std::string _group((*_group_it)->getName());
            std::set<const xdaq::ApplicationDescriptor *> _descriptors
                = (*_group_it)->getApplicationDescriptors();
            for (std::set<const xdaq::ApplicationDescriptor *>::const_iterator _descriptor_it = _descriptors.begin()
                     ; _descriptor_it != _descriptors.end() ; ++_descriptor_it) {
            	const xdaq::ApplicationDescriptor & _descriptor(*(*_descriptor_it));
                if (_descriptor.getClassName() != "processcontrol::Application") {
                    std::string _host(toolbox::net::URL(_descriptor.getContextDescriptor()->getURL()).getHost());
                    std::string _service(_descriptor.getAttribute("process"));
                    std::string _app_zone(_descriptor.getAttribute("zone"));
                    if (_app_zone.empty())
                        _app_zone = _zone;
                    if (!_service.empty() && _host == getHost())
                        insertProcess(Process(_group, _host, _app_zone, _service));
                }
            }
        }
    }

    workloop_action_.obj_ = this;
    workloop_action_.func_ = &processcontrol::Service::workloopFunction;
    workloop_action_.name_ = "workloop_action";

    for (unsigned int _workloop = 0 ; _workloop < _n_workloops ; ++_workloop) {
        std::ostringstream _name_ss("workloop_");
        _name_ss << _workloop;
        workloops_.push_back(new toolbox::task::WaitingWorkLoop(_name_ss.str()));
        workloops_.back()->submit(&workloop_action_);
        workloops_.back()->WorkLoop::activate();
    }

    xoap::deferredbind(&application_, this, &Service::xoapRequest, "ProcessControlRequest" , XDAQ_NS_URI);
}

Service::~Service()
{
    try {
        while (true)
            workloop_requests_.pop(0, 1000);
    } catch (toolbox::exception::Timeout) {}

    try {
        Request _destroy(getURL(), Request::destroy_);
        for (unsigned int _workloop = 0 ; _workloop < workloops_.size() ; ++_workloop)
            workloop_requests_.push(_destroy);
        for (std::list<toolbox::task::WaitingWorkLoop *>::iterator _workloop = workloops_.begin()
                 ; _workloop != workloops_.end() ; ++_workloop) {
            (*_workloop)->cancel();
            delete (*_workloop);
        }
    } catch (...) {}
}

Response Service::action(Request _request)
{
    Response _response(_request.getId()
                       , _request.getAction()
                       , toolbox::TimeVal::gettimeofday()
                       , Response::not_set_
                       , _request.getActionName() + " request queued");
    if (!Request::isAction(_request.getAction())) {
        _response.setReturnValue(Response::not_implemented_);
        _response.setDescription(std::string("The request contains an invalid action ")
                                 + _request.getActionName());
    } else if (!hasProcess(_request.getProcess())) {
        _response.setReturnValue(_request.getAction() == Request::status_
                                 ? Response::status_unknown_
                                 : Response::not_installed_);
        _response.setDescription(std::string("The request contains an unknown process ")
                                 + _request.getProcess().getDescription());
    } else {
        if (_request.getTimeout() == 0 || _request.getTimeout() > getMaxTimeout())
            _request.setTimeout(getMaxTimeout());

        workloop_requests_.push(_request);
    }

    callback_(_request, _response);

    return _response;
}

xoap::MessageReference Service::xoapRequest(xoap::MessageReference _message)
    throw (xoap::exception::Exception)
{
    xdata::Vector<xdata::Bag<Request> > _requests;
    {
        bool _found(false);
        std::vector<xoap::SOAPElement> _body_elements
            = _message->getSOAPPart().getEnvelope().getBody().getChildElements();
        for (std::vector<xoap::SOAPElement>::iterator _body_element = _body_elements.begin()
                 ; _body_element != _body_elements.end() && !_found ; ++_body_element)
            if (_body_element->getElementName().getLocalName() == "ProcessControlRequest") {
                try {
                    xdata::soap::Serializer _serializer;
                    _serializer.import(&_requests, _body_element->getDOMNode());
                    _found = true;
                } catch (xdata::exception::Exception & _error) {
                    XCEPT_RETHROW(xoap::exception::Exception
                                  , "Could not deserialize Requests", _error);
                }
            }
        if (!_found)
            XCEPT_RAISE(xoap::exception::Exception, "No valid request found in message");
    }

    xdata::Vector<xdata::Bag<Response> > _reply_responses;
    for (xdata::Vector<xdata::Bag<Request> >::iterator _request_bag = _requests.begin()
             ; _request_bag != _requests.end() ; ++_request_bag) {
        Request const & _request(_request_bag->bag);
        xdata::Bag<Response> _response_bag;
        _response_bag.bag = action(_request);
        _reply_responses.push_back(_response_bag);
    }

    xoap::MessageReference _reply = xoap::createMessage();
    xoap::SOAPEnvelope _envelope = _reply->getSOAPPart().getEnvelope();

    _envelope.addNamespaceDeclaration("xsi"    , "http://www.w3.org/2001/XMLSchema-instance");
    _envelope.addNamespaceDeclaration("xsd"    , "http://www.w3.org/2001/XMLSchema");
    _envelope.addNamespaceDeclaration("soapenc", "http://schemas.xmlsoap.org/soap/encoding/");

    xoap::SOAPName _responses_name = _envelope.createName("ProcessControlRequest", "xdaq", XDAQ_NS_URI);
    xoap::SOAPBodyElement _responses_element = _envelope.getBody().addBodyElement(_responses_name);

    xdata::soap::Serializer _serializer;
    try {
        _serializer.exportAll(&_reply_responses, dynamic_cast<DOMElement*>(_responses_element.getDOM()));
    } catch (xdata::exception::Exception & _error) {
        XCEPT_RETHROW(xoap::exception::Exception
                      , "Could not serialize reply", _error);
    }

    return _reply;
}

bool Service::workloopFunction(toolbox::task::WorkLoop *)
{
    Request _request;
    while ((_request = workloop_requests_.pop()).getAction() != Request::destroy_) {
        try {
            std::pair<int, std::string> _result = execute(_request);
            if (_result.first < 0)
                _result.first = Response::unknown_;
            if (_request.getAction() == Request::status_)
                _result.first += 256;
            postResponse(_request
                         , _result.first
                         , _result.second);
        } catch (processcontrol::exception::Exception const & _error) {
            postResponse(_request
                         , (_request.getAction() == Request::status_
                            ? Response::status_unknown_
                            : Response::error_)
                         , _error.what());
        } catch (std::exception const & _error) {
            postResponse(_request
                         , (_request.getAction() == Request::status_
                            ? Response::status_unknown_
                            : Response::error_)
                         , _error.what());
        } catch (...) {
            postResponse(_request
                         , (_request.getAction() == Request::status_
                            ? Response::status_unknown_
                            : Response::error_)
                         , "Unknown error occurred");
        }
    }
    return false;
}

std::pair<int, std::string> Service::execute(Request const & _request) const
    throw (processcontrol::exception::Exception)
{
    std::cout<<" processcontrol Service::execute line "<<__LINE__<<std::endl;
    std::string hostN = _request.getProcess().getHost();
         if(hostN == "l1ts-rpc-01.cms") hostN = "vmepc-s2g17-25-01.service";
    else if(hostN == "l1ts-rpc-02.cms") hostN = "vmepc-s2g17-26-01.service";
    else if(hostN == "l1ts-rpc-03.cms") hostN = "vmepc-s2g17-27-01.service";
    else if(hostN == "l1ts-rpc-04.cms") hostN = "vmepc-s2g17-28-01.service";
    else if(hostN == "l1ts-rpc-05.cms") hostN = "vmepc-s2g17-29-01.service";
         //the systemctl dpose not like the l1ts-rpc-05.cms aliases

    if (_request.getProcess().getService().empty()) {
        LOG4CPLUS_INFO(logger_, "executing command "
                <<"systemctl"
                << _request.getActionName().c_str()
                << "rpc.target" );
        std::cout<<"executing command "
                <<"systemctl"
                << _request.getActionName().c_str()
                << "rpc.target"<<std::endl;
    }
    else {
        std::cout<<" processcontrol Service::execute line "<<__LINE__<<std::endl;
        std::string proc = _request.getProcess().getZone() + "."
                + _request.getProcess().getService() + "@" + hostN;
        LOG4CPLUS_INFO(logger_, "executing command "
                <<"/usr/bin/systemctl "
                << _request.getActionName().c_str()<<" "
                <<proc );
        std::cout<<"executing command "
                <<"/usr/bin/systemctl "
                << _request.getActionName().c_str()<<" "
                <<proc <<std::endl;
    }

    // set up pipe
    int _pipe_stdout_in_out[2];
    if (::pipe(_pipe_stdout_in_out) == -1) {
        XCEPT_RAISE(processcontrol::exception::SystemException, "Unable to create cout pipe");
    }

    // create fork
    ::pid_t _pid = ::fork();
    if (_pid < 0) { // problem
        std::string _system_description(processcontrol::exception::SystemException::strerror_r(errno));
        ::close(_pipe_stdout_in_out[0]);
        ::close(_pipe_stdout_in_out[1]);
        XCEPT_RAISE(processcontrol::exception::SystemException
                    , std::string("Unable to create fork: ") + _system_description);
    }

    if (_pid == 0) { // child
        // close file descriptors, including sockets
        rlimit _descriptor_limits;
        getrlimit(RLIMIT_NOFILE, &_descriptor_limits);
        int _descriptor_max(_descriptor_limits.rlim_cur);
        for (int _fd = 3 ; _fd < _descriptor_max ; ++_fd)
            if (_fd != _pipe_stdout_in_out[1])
                ::close(_fd);

        // set up pipes
        // ::close(_pipe_stdout_in_out[0]);
        while (::dup2(_pipe_stdout_in_out[1], STDOUT_FILENO) == -1) {
            if (errno != EINTR) {
                std::cout << "Unable to connect cout pipe"
                          << processcontrol::exception::SystemException::strerror_r(errno)
                          << std::endl;
                ::exit(EXIT_FAILURE);
            }
        }
        while (::dup2(STDOUT_FILENO, STDERR_FILENO) == -1) {
            if (errno != EINTR) {
                std::cout << "Unable redirect cerr to cout"
                          << processcontrol::exception::SystemException::strerror_r(errno)
                          << std::endl;
                ::exit(EXIT_FAILURE);
            }
        }
        ::close(_pipe_stdout_in_out[1]);

        // run command
/*        if (_request.getProcess().getService().empty())
            ::execl("/sbin/service", "service", "xdaqd"
                    , _request.getActionName().c_str()
                    , _request.getProcess().getZone().c_str()
                    , (char *)0);
        else
            ::execl("/sbin/service", "service", "xdaqd"
                    , _request.getActionName().c_str()
                    , _request.getProcess().getZone().c_str(), _request.getProcess().getService().c_str()
                    , (char *)0);*/

        if (_request.getProcess().getService().empty()) {
            ::execl( "/usr/bin/systemctl", "systemctl"
                    , _request.getActionName().c_str()
                    , "rpc.target"
                    , (char *)0);
        }
        else {
            std::string proc = _request.getProcess().getZone() + "."
                    + _request.getProcess().getService() + "@" + hostN;

            ::execl("/usr/bin/systemctl", "systemctl"
                    ,_request.getActionName().c_str()
                    , proc.c_str()
                    , (char *)0 );
        }
        XCEPT_RAISE(processcontrol::exception::SystemException, "Unable to execute service");
        exit (-1);
    }
    // service
    ::close(_pipe_stdout_in_out[1]);

    // wait for output and send it as a response
    int _status(0);
    unsigned int _duration(0);
    ::pid_t _waitpid_return(0);
    while ((_waitpid_return = ::waitpid(_pid, &_status, WNOHANG)) < 1
           && _duration < (_request.getTimeout() + 5)) {
        if (_waitpid_return == -1) { // -1: possibly a problem, 0: continue waiting
            int _errno(errno);
            std::string _system_description(processcontrol::exception::SystemException::strerror_r(_errno));
            if (_errno != EINTR) {
                _status = -1;
                ::close(_pipe_stdout_in_out[0]);
                XCEPT_RAISE(processcontrol::exception::SystemException
                            , std::string("Unable to wait for the child: ") + _system_description);
                break;
            }
        }
        ++_duration;
        if (_duration == _request.getTimeout())
            ::kill(_pid, SIGTERM);
        if (_duration == _request.getTimeout() + 5)
            ::kill(_pid, SIGKILL);
        toolbox::u_sleep(1000000);
    }

    if (_duration == _request.getTimeout() + 5) {
        ::close(_pipe_stdout_in_out[0]);
        XCEPT_RAISE(processcontrol::exception::SystemException, "Timeout while waiting for child, had to call SIGKILL");
    } else if (_duration >= _request.getTimeout()) {
        ::close(_pipe_stdout_in_out[0]);
        XCEPT_RAISE(processcontrol::exception::SystemException, "Timeout while waiting for child, had to call SIGTERM");
    }

    std::string _buffer(4096, '\0');
    ::size_t _position(0);
    ::ssize_t _read(0);
    while ((_read = ::read(_pipe_stdout_in_out[0], &(_buffer.at(_position)), _buffer.size() - _position)) > 0)
        _position += _read;
    if (_read < 0) {
        int _errno(errno);
        std::string _system_description(processcontrol::exception::SystemException::strerror_r(_errno));
        ::close(_pipe_stdout_in_out[0]);
        XCEPT_RAISE(processcontrol::exception::SystemException
                    , std::string("Could not read cout pipe: ") + _system_description);
    }

    _buffer.resize(_position);

    ::close(_pipe_stdout_in_out[0]);

    if (_status != EXIT_SUCCESS) {
        std::ostringstream _sstream;
        _sstream << "Exit code " << _status << " for child process.\n"
                 << "Output: \n" << _buffer << "\n";

        XCEPT_RAISE(processcontrol::exception::SystemException, _sstream.str());
    } else
        return std::pair<int, std::string>(_status, _buffer);
}

void Service::postResponse(Request const & _request, unsigned int _status, std::string const & _description)
{
    xdata::Bag<Response> _response_bag;
    _response_bag.bag = Response(_request.getId()
                                 , _request.getAction()
                                 , toolbox::TimeVal::gettimeofday()
                                 , _status
                                 , _description);

    callback_(_request, _response_bag.bag);

    if (_request.getResponseListener() == getURL())
        return;

    xoap::MessageReference _reply = xoap::createMessage();
    _reply->getMimeHeaders()->setHeader("SOAPAction", _request.getResponseListener());
    xoap::SOAPEnvelope _envelope = _reply->getSOAPPart().getEnvelope();

    _envelope.addNamespaceDeclaration("xsi"    , "http://www.w3.org/2001/XMLSchema-instance");
    _envelope.addNamespaceDeclaration("xsd"    , "http://www.w3.org/2001/XMLSchema");
    _envelope.addNamespaceDeclaration("soapenc", "http://schemas.xmlsoap.org/soap/encoding/");

    xoap::SOAPName _response_name = _envelope.createName("ProcessControlResponse", "xdaq", XDAQ_NS_URI);
    xoap::SOAPBodyElement _response_element = _envelope.getBody().addBodyElement(_response_name);

    xdata::soap::Serializer _serializer;
    try {
        _serializer.exportAll(&_response_bag, dynamic_cast<DOMElement*>(_response_element.getDOM()));
    } catch (xdata::exception::Exception const & _error) {
        LOG4CPLUS_ERROR(logger_, "Could not serialize reply for "
                        << _request.getResponseListener() << "::" << _request.getId()
                        << ": " << _error.what());
        return;
    }

    try {
        pt::Address::Reference _client
            = pt::getPeerTransportAgent()->createAddress(_request.getResponseListener(), "soap");
        pt::Address::Reference _service
            = pt::getPeerTransportAgent()->createAddress(getURL(), "soap");

        std::string _protocol = _client->getProtocol();

        pt::PeerTransportSender * _sender
            = dynamic_cast<pt::PeerTransportSender*>
            (pt::getPeerTransportAgent()->getPeerTransport(_protocol, "soap", pt::Sender));
        pt::Messenger::Reference _messenger = _sender->getMessenger(_client, _service);
        pt::SOAPMessenger & _soap_messenger = dynamic_cast<pt::SOAPMessenger&>(*_messenger);
        // xoap::MessageReference _answer =
        _soap_messenger.send(_reply);
    } catch (xcept::Exception const & _error) {
        LOG4CPLUS_ERROR(logger_, "Could not post reply for "
                        << _request.getResponseListener() << "::" << _request.getId()
                        << ": " << _error.what());
    } catch (std::exception const & _error) {
        LOG4CPLUS_ERROR(logger_, "Could not post reply for "
                        << _request.getResponseListener() << "::" << _request.getId()
                        << ": " << _error.what());
    } catch (...) {
        LOG4CPLUS_ERROR(logger_, "Could not post reply for "
                        << _request.getResponseListener() << "::" << _request.getId()
                        << ": Unknown exception");
    }
}

} // namespace processcontrol
