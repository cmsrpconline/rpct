#include "processcontrol/xgitools/WarningLevel.h"

#include <ostream>

namespace processcontrol {
namespace xgitools {

char const * const WarningLevel::char_names_[]
= {"ok", "notset", "info", "warning", "error", "unknown"};
std::vector<std::string> const WarningLevel::names_(char_names_
                                                    , char_names_ + sizeof(char_names_)/sizeof(char const *));

char const * const WarningLevel::char_bgcolors_[]
= {"#dfd", "#efefef", "#ddf", "#ffb", "#fdd", "#ffb"};
std::vector<std::string> const WarningLevel::bgcolors_(char_bgcolors_
                                                       , char_bgcolors_ + sizeof(char_bgcolors_)/sizeof(char const *));

char const * const WarningLevel::char_fgcolors_[]
= {"#67e667", "#eee", "#6C8Ce5", "#ffd073", "#ff7373", "#ffd073"};
std::vector<std::string> const WarningLevel::fgcolors_(char_fgcolors_
                                                       , char_fgcolors_ + sizeof(char_fgcolors_)/sizeof(char const *));

char const * const WarningLevel::char_images_[]
= {"/extern/icons/accept.png"
   , "/extern/icons/control_stop.png"
   , "/extern/icons/information.png"
   , "/extern/icons/error.png"
   , "/extern/icons/exclamation.png"
   , "/extern/icons/lightning.png"
};
std::vector<std::string> const WarningLevel::images_(char_images_
                                                     , char_images_ + sizeof(char_images_)/sizeof(char const *));

WarningLevel::WarningLevel(unsigned int _level)
    : level_(_level)
{
    if (level_ > unknown_)
        level_ = unknown_;
}

std::ostream & operator<<(std::ostream & _ostream, WarningLevel const & _level)
{
    return (_ostream << _level.getName());
}

} // namespace xgitools
} // namespace processcontrol
