#ifndef rpc_processcontrol_Service_h_
#define rpc_processcontrol_Service_h_

#include "processcontrol/ProcessController.h"

#include <list>

#include "toolbox/SyncQueue.h"
#include "toolbox/task/WaitingWorkLoop.h"
#include "toolbox/task/WorkLoop.h"
#include "xoap/MessageReference.h"
#include "xoap/exception/Exception.h"

namespace processcontrol {

class Service
    : public processcontrol::ProcessController
{
public:
    Service(xdaq::Application & _application
            , CallbackInterface & _callback
            , unsigned int _max_timeout = 60
            , unsigned int _n_workloops = 4);
    ~Service();

    Response action(Request _request);

protected:
    xoap::MessageReference xoapRequest(xoap::MessageReference _message) throw (xoap::exception::Exception);

    bool workloopFunction(toolbox::task::WorkLoop *);
    std::pair<int, std::string> execute(Request const & _request) const
        throw (processcontrol::exception::Exception);
    void postResponse(Request const & _request, unsigned int _status, std::string const & _description);

protected:
    toolbox::SyncQueue<Request> workloop_requests_;
    toolbox::task::Action<Service> workloop_action_;
    std::list<toolbox::task::WaitingWorkLoop *> workloops_;
};

} // namespace processcontrol

#endif // rpc_processcontrol_service_Service_h_
