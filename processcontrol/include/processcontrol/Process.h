#ifndef rpc_processcontrol_Process_h_
#define rpc_processcontrol_Process_h_

#include <string>
#include <iosfwd>

#include "xdata/String.h"

namespace xdata {
class AbstractBag;
} // namespace xdata

namespace processcontrol {

class Process
{
public:
    Process(std::string const & _group = std::string("")
            , std::string const & _host = std::string("")
            , std::string const & _zone = std::string("")
            , std::string const & _service = std::string(""));

    std::string const & getGroup() const;
    std::string const & getHost() const;
    std::string const & getZone() const;
    std::string const & getService() const;

    void setGroup(std::string const & _group);
    void setHost(std::string const & _host);
    void setZone(std::string const & _zone);
    void setService(std::string const & _service);

    void registerFields(xdata::AbstractBag * _bag);

    std::string getDescription() const;

    bool operator<(Process const & _process) const;

protected:
    xdata::String group_;
    xdata::String host_;
    xdata::String zone_;
    xdata::String service_;
};

std::ostream & operator<<(std::ostream & _ostream, Process const & _process);

} // namespace processcontrol

#include "processcontrol/Process-inl.h"

#endif // rpc_processcontrol_Process_h_
