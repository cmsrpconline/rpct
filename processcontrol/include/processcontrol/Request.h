#ifndef rpc_processcontrol_Request_h_
#define rpc_processcontrol_Request_h_

#include <string>

#include "toolbox/BSem.h"
#include "toolbox/TimeVal.h"
#include "xdata/Bag.h"
#include "xdata/String.h"
#include "xdata/TimeVal.h"
#include "xdata/UnsignedInteger.h"

#include "processcontrol/Process.h"

namespace xdata {
class AbstractBag;
} // namespace xdata

namespace processcontrol {

class Request
{
protected:
    static char const * const char_action_names_[9];

public:
    // cf LSB iniscrptact
    static unsigned int const status_       = 0;
    static unsigned int const start_        = 1;
    static unsigned int const stop_         = 2;
    static unsigned int const restart_      = 3;
    static unsigned int const try_restart_  = 4;
    static unsigned int const reload_       = 5;
    static unsigned int const force_reload_ = 6;

    static unsigned int const unknown_      = 7;

    static unsigned int const destroy_      = 8;

    static std::vector<std::string> const action_names_;
    static std::string const unknown_name_;

protected:
    static toolbox::BSem max_id_mutex_;
    static unsigned int max_id_;

public:
    Request(std::string const & _response_listener = std::string("")
            , unsigned int _action = status_
            , Process const & _process = Process()
            , unsigned int _timeout = 25);

    static bool isAction(unsigned int _action);
    static bool isAction(std::string const & _action_name);
    static std::string const & getActionName(unsigned int _action);
    static unsigned int getAction(std::string const & _action_name);

    std::string const & getResponseListener() const;
    unsigned int getId() const;
    unsigned int getAction() const;
    std::string const & getActionName() const;
    Process const & getProcess() const;
    toolbox::TimeVal const & getTime() const;
    unsigned int getTimeout() const;

    void setResponseListener(std::string const & _response_listener);
    void setId(unsigned int _id = 0);
    void setAction(unsigned int _action);
    void setAction(std::string const & _action_name);
    void setProcess(Process const & _process);
    void setProcess(std::string const & _group, std::string const & _host, std::string const & _zone
                    , std::string const & _service);
    void setTime(toolbox::TimeVal const & _time = toolbox::TimeVal());
    void setTimeout(unsigned int _timeout);

    void registerFields(xdata::AbstractBag * _bag);

protected:
    xdata::String response_listener_;
    xdata::UnsignedInteger id_;
    xdata::UnsignedInteger action_;
    xdata::Bag<Process> process_;
    xdata::TimeVal time_;
    xdata::UnsignedInteger timeout_;
};

} // namespace processcontrol

#include "processcontrol/Request-inl.h"

#endif // rpc_processcontrol_Request_h_
