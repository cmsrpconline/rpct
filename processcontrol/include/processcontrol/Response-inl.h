#ifndef rpc_processcontrol_Response_inl_h_
#define rpc_processcontrol_Response_inl_h_

#include "processcontrol/Response.h"

namespace processcontrol {

inline std::string const & Response::getReturnDescription(unsigned int _action, unsigned int _return_value)
{
    if (_return_value < 0)
        return unknown_description_;
    if (_return_value < return_descriptions_.size())
        return return_descriptions_.at(_return_value);
    if (_return_value >= 256 && _return_value < 256 + status_return_descriptions_.size())
        return status_return_descriptions_.at(_return_value - 256);
    if (_return_value >= 512 && _return_value < 512 + other_descriptions_.size())
        return other_descriptions_.at(_return_value - 512);
    return unknown_description_;
}

inline unsigned int Response::getId() const
{
    return id_.value_;
}

inline unsigned int Response::getAction() const
{
    return action_.value_;
}

inline std::string const & Response::getActionName() const
{
    return Request::getActionName(action_.value_);
}

inline toolbox::TimeVal Response::getTime() const
{
    return time_.value_;
}

inline unsigned int Response::getReturnValue() const
{
    return return_value_.value_;
}

inline std::string const & Response::getReturnDescription() const
{
    return getReturnDescription(action_.value_, return_value_.value_);
}

inline xgitools::WarningLevel Response::getWarningLevel() const
{
    switch (return_value_.value_) {
    case ok_:
    case status_ok_:
        return xgitools::WarningLevel(xgitools::WarningLevel::ok_);         break;
    case not_set_:
        return xgitools::WarningLevel(xgitools::WarningLevel::not_set_);    break;
    case queued_:
        return xgitools::WarningLevel(xgitools::WarningLevel::info_);       break;
    }
    return xgitools::WarningLevel(xgitools::WarningLevel::error_);
}

inline void Response::setId(unsigned int _id)
{
    id_.value_ = _id;
}

inline void Response::setAction(unsigned int _action)
{
    action_.value_ = _action;
}

inline void Response::setAction(std::string const & _action_name)
{
    action_.value_ = Request::getAction(_action_name);
}

inline void Response::setTime(toolbox::TimeVal const & _time)
{
    time_.value_ = _time;
}

inline void Response::setReturnValue(unsigned int _return_value)
{
    return_value_.value_ = _return_value;
}

} // namespace processcontrol

#endif // rpc_processcontrol_Response_inl_h_
