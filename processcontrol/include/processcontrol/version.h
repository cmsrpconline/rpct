#ifndef rpc_processcontrol_version_h_
#define rpc_processcontrol_version_h_

#include "config/PackageInfo.h"

#define PROCESSCONTROL_VERSION_MAJOR 0
#define PROCESSCONTROL_VERSION_MINOR 1
#define PROCESSCONTROL_VERSION_PATCH 0
#undef PROCESSCONTROL_PREVIOUS_VERSIONS

#define PROCESSCONTROL_VERSION_CODE PACKAGE_VERSION_CODE(PROCESSCONTROL_VERSION_MAJOR,PROCESSCONTROL_VERSION_MINOR,PROCESSCONTROL_VERSION_PATCH)
#ifndef PROCESSCONTROL_PREVIOUS_VERSIONS
#define PROCESSCONTROL_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(PROCESSCONTROL_VERSION_MAJOR,PROCESSCONTROL_VERSION_MINOR,PROCESSCONTROL_VERSION_PATCH)
#else
#define PROCESSCONTROL_FULL_VERSION_LIST  PROCESSCONTROL_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(PROCESSCONTROL_VERSION_MAJOR,PROCESSCONTROL_VERSION_MINOR,PROCESSCONTROL_VERSION_PATCH)
#endif
namespace processcontrol
{
const std::string package  =  "processcontrol";
const std::string versions =  PROCESSCONTROL_FULL_VERSION_LIST;
const std::string description = "Process Controller for XDAQ applications using xdaqd";
const std::string authors = "Filip Thyssen";
const std::string summary = "Process Controller for XDAQ applications using xdaqd";
const std::string link = "";
config::PackageInfo getPackageInfo();
void checkPackageDependencies() throw (config::PackageInfo::VersionException);
std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif // rpc_processcontrol_version_h_
