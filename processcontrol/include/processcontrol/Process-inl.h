#ifndef rpc_processcontrol_Process_inl_h_
#define rpc_processcontrol_Process_inl_h_

#include "processcontrol/Process.h"

namespace processcontrol {

inline std::string const & Process::getGroup() const
{
    return group_.value_;
}

inline std::string const & Process::getHost() const
{
    return host_.value_;
}

inline std::string const & Process::getZone() const
{
    return zone_.value_;
}

inline std::string const & Process::getService() const
{
    return service_.value_;
}

inline void Process::setGroup(std::string const & _group)
{
    group_.value_ = _group;
}

inline void Process::setHost(std::string const & _host)
{
    host_.value_ = _host;
}

inline void Process::setZone(std::string const & _zone)
{
    zone_.value_ = _zone;
}

inline void Process::setService(std::string const & _service)
{
    service_.value_ = _service;
}

inline bool Process::operator<(Process const & _process) const
{
    return (group_.value_ < _process.group_.value_
            || (group_.value_ == _process.group_.value_
                && (host_.value_ < _process.host_.value_
                    || (host_.value_ == _process.host_.value_
                        && (zone_.value_ < _process.zone_.value_
                            || (zone_.value_ == _process.zone_.value_
                                && (service_.value_ < _process.service_.value_))
                            )
                        )
                    )
                )
            );
}

} // namespace processcontrol

#endif // rpc_processcontrol_Process_inl_h_
