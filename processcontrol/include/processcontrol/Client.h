#ifndef rpc_processcontrol_Client_h_
#define rpc_processcontrol_Client_h_

#include "processcontrol/ProcessController.h"

#include <map>
#include <string>

#include "xoap/MessageReference.h"
#include "xoap/exception/Exception.h"

namespace xdaq {
class ApplicationDescriptor;
} // namespace xdaq

namespace processcontrol {

class Client
    : public processcontrol::ProcessController
{
public:
    static std::string const service_classname_;
    static std::string const service_name_;

public:
    Client(xdaq::Application & _application
           , CallbackInterface & _callback
           , unsigned int _max_timeout = 60);
    ~Client();

    Response action(Request _request);
    // todo: std::vector<Response> action(std::vector<Request> _requests);

protected:
    xoap::MessageReference xoapResponse(xoap::MessageReference _message) throw (xoap::exception::Exception);

protected:
    std::map<std::string, const xdaq::ApplicationDescriptor *> host_service_;
    std::map<unsigned int, Request> id_request_;
};

} // namespace processcontrol

#endif // rpc_processcontrol_Client_h_
