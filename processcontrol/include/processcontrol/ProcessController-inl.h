#ifndef rpc_processcontrol_ProcessController_inl_h_
#define rpc_processcontrol_ProcessController_inl_h_

#include "processcontrol/ProcessController.h"

#include "toolbox/task/Guard.h"

namespace processcontrol {

inline std::set<Process> const & ProcessController::getProcesses() const
{
    return processes_;
}

inline bool ProcessController::hasProcess(Process const & _process) const
{
    toolbox::task::Guard<toolbox::BSem> _guard(mutex_);
    return processes_.count(_process);
}

inline bool ProcessController::insertProcess(Process const & _process)
{
    toolbox::task::Guard<toolbox::BSem> _guard(mutex_);
    return processes_.insert(_process).second;
}

inline bool ProcessController::eraseProcess(Process const & _process)
{
    toolbox::task::Guard<toolbox::BSem> _guard(mutex_);
    return processes_.erase(_process);
}

inline unsigned int ProcessController::getMaxTimeout() const
{
    toolbox::task::Guard<toolbox::BSem> _guard(mutex_);
    return max_timeout_;
}

inline void ProcessController::setMaxTimeout(unsigned int _seconds)
{
    toolbox::task::Guard<toolbox::BSem> _guard(mutex_);
    max_timeout_ = _seconds;
}

inline std::string const & ProcessController::getURL() const
{
    return url_;
}

inline std::string const & ProcessController::getHost() const
{
    return host_;
}

inline log4cplus::Logger const & ProcessController::getLogger() const
{
    return logger_;
}

inline std::vector<Response> ProcessController::action(std::vector<Request> _requests)
{
    std::vector<Response> _responses;
    for (std::vector<Request>::const_iterator _request = _requests.begin()
             ; _request != _requests.end() ; ++_request)
        _responses.push_back(action(*_request));
    return _responses;
}

inline Response ProcessController::action(unsigned int _action, Process const & _process
                                          , unsigned int _timeout)
{
    return action(Request(url_, _action, _process, (_timeout > 0 ? _timeout : getMaxTimeout())));
}

inline Response ProcessController::action(std::string const & _action, Process const & _process
                                          , unsigned int _timeout)
{
    return action(Request::getAction(_action), _process, _timeout);
}

inline Response ProcessController::status(Process const & _process, unsigned int _timeout)
{
    return action(Request::status_, _process, _timeout);
}

inline Response ProcessController::start(Process const & _process, unsigned int _timeout)
{
    return action(Request::start_, _process, _timeout);
}

inline Response ProcessController::stop(Process const & _process, unsigned int _timeout)
{
    return action(Request::stop_, _process, _timeout);
}

inline Response ProcessController::restart(Process const & _process, unsigned int _timeout)
{
    return action(Request::restart_, _process, _timeout);
}

} // namespace processcontrol

#endif // rpc_processcontrol_ProcessController_inl_h_
