#ifndef rpc_processcontrol_exception_SystemException_h
#define rpc_processcontrol_exception_SystemException_h

#include "processcontrol/exception/Exception.h"

namespace processcontrol {
namespace exception {

class SystemException
    : public Exception
{
public:
    static std::string strerror_r(int _errno);

public:
    SystemException(std::string const & _name, int _errno
                    , std::string const & _module, int _line, std::string const & _function);
    SystemException(std::string const & _name, std::string const & _message
                    , std::string const & _module, int _line, std::string const & _function);
    SystemException(std::string const & _name, int _errno
                    , std::string const & _module, int _line, std::string const & _function
                    , xcept::Exception & _error);
    SystemException(std::string const & _name, std::string const & _message
                    , std::string const & _module, int _line, std::string const & _function
                    , xcept::Exception & _error);
};

} // namespace exception
} // namespace processcontrol

#endif // rpc_processcontrol_exception_Exception_h
