#ifndef rpc_processcontrol_exception_Exception_h
#define rpc_processcontrol_exception_Exception_h

#include "xcept/Exception.h"

namespace processcontrol {
namespace exception {

class Exception
    : public xcept::Exception
{
public:
    Exception(std::string const & _name, std::string const & _message
              , std::string const & _module, int _line, std::string const & _function);
    Exception(std::string const & _name, std::string const & _message
              , std::string const & _module, int _line, std::string const & _function
              , xcept::Exception & _error);
};

} // namespace exception
} // namespace processcontrol

#endif // rpc_processcontrol_exception_Exception_h
