#ifndef rpc_processcontrol_xgitools_WarningLevel_h_
#define rpc_processcontrol_xgitools_WarningLevel_h_

#include <iosfwd>
#include <vector>
#include <string>

namespace processcontrol {
namespace xgitools {

class WarningLevel
{
protected:
    static char const * const char_names_[6];
    static std::vector<std::string> const names_;

    static char const * const char_bgcolors_[6];
    static std::vector<std::string> const bgcolors_;

    static char const * const char_fgcolors_[6];
    static std::vector<std::string> const fgcolors_;

    static char const * const char_images_[6];
    static std::vector<std::string> const images_;

public:
    static unsigned int const ok_      = 0;
    static unsigned int const not_set_ = 1;
    static unsigned int const info_    = 2;
    static unsigned int const warning_ = 3;
    static unsigned int const error_   = 4;

    static unsigned int const unknown_ = 5;

public:
    explicit WarningLevel(unsigned int _level = not_set_);

    static unsigned int getLevel(std::string const & _name);

    static std::string const & getName(unsigned int _level);
    static std::string const & getBgColor(unsigned int _level);
    static std::string const & getFgColor(unsigned int _level);
    static std::string const & getImage(unsigned int _level);

    operator unsigned int() const;
    unsigned int getLevel() const;
    std::string const & getName() const;
    std::string const & getBgColor() const;
    std::string const & getFgColor() const;
    std::string const & getImage() const;

    WarningLevel & operator=(unsigned int _level);
    void setLevel(unsigned int _level);

    bool operator<(WarningLevel const & _level) const;
    bool operator<(unsigned int _level) const;

protected:
    unsigned int level_;
};

std::ostream & operator<<(std::ostream & _ostream, WarningLevel const & _level);

} // namespace xgitools
} // namespace processcontrol

#include "processcontrol/xgitools/WarningLevel-inl.h"

#endif // rpc_processcontrol_xgitools_WarningLevel_h_
