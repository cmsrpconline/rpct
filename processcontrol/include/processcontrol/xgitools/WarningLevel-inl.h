#ifndef rpc_processcontrol_xgitools_WarningLevel_inl_h_
#define rpc_processcontrol_xgitools_WarningLevel_inl_h_

#include "processcontrol/xgitools/WarningLevel.h"

#include <algorithm>

namespace processcontrol {
namespace xgitools {

inline unsigned int WarningLevel::getLevel(std::string const & _name)
{
    std::vector<std::string>::const_iterator _index
        = std::find(names_.begin(), names_.end(), _name);
    if (_index != names_.end())
        return (_index - names_.begin());
    return unknown_;
}

inline std::string const & WarningLevel::getName(unsigned int _level)
{
    if (_level < names_.size())
        return names_.at(_level);
    return names_.back();
}

inline std::string const & WarningLevel::getBgColor(unsigned int _level)
{
    if (_level < bgcolors_.size())
        return bgcolors_.at(_level);
    return bgcolors_.back();
}

inline std::string const & WarningLevel::getFgColor(unsigned int _level)
{
    if (_level < fgcolors_.size())
        return fgcolors_.at(_level);
    return fgcolors_.back();
}

inline std::string const & WarningLevel::getImage(unsigned int _level)
{
    if (_level < images_.size())
        return images_.at(_level);
    return images_.back();
}

inline WarningLevel::operator unsigned int() const
{
    return level_;
}

inline unsigned int WarningLevel::getLevel() const
{
    return level_;
}

inline std::string const & WarningLevel::getName() const
{
    return getName(level_);
}

inline std::string const & WarningLevel::getBgColor() const
{
    return getBgColor(level_);
}

inline std::string const & WarningLevel::getFgColor() const
{
    return getFgColor(level_);
}

inline std::string const & WarningLevel::getImage() const
{
    return getImage(level_);
}

inline WarningLevel & WarningLevel::operator=(unsigned int _level)
{
    level_ = _level;
    if (level_ > unknown_)
        level_ = unknown_;
    return *this;
}

inline void WarningLevel::setLevel(unsigned int _level)
{
    level_ = _level;
    if (level_ > unknown_)
        level_ = unknown_;
}

inline bool WarningLevel::operator<(WarningLevel const & _level) const
{
    return level_ < _level.level_;
}

inline bool WarningLevel::operator<(unsigned int _level) const
{
    return level_ < _level;
}

} // namespace xgitools
} // namespace processcontrol

#endif // rpc_processcontrol_xgitools_WarningLevel_inl_h_
