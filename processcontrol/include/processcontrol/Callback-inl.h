#ifndef rpc_processcontrol_Callback_inl_h_
#define rpc_processcontrol_Callback_inl_h_

#include "processcontrol/Callback.h"

namespace processcontrol {

inline void Callback::operator()(Request const & _request, Response const & _response) throw()
{
    try {
        (*callback_)(_request, _response);
    } catch(...) {}
}

template<typename t_object_type>
inline ObjectCallback<t_object_type>::ObjectCallback(t_object_type & _object
                                                     , void (t_object_type::* _callback)(Request const &, Response const &))
    : object_(_object)
    , callback_(_callback)
{}

template<typename t_object_type>
inline void ObjectCallback<t_object_type>::operator()(Request const & _request, Response const & _response) throw()
{
    try {
        (object_.*callback_)(_request, _response);
    } catch(...) {}
}

} // namespace processcontrol

#endif // rpc_processcontrol_Callback_inl_h_
