#ifndef rpc_processcontrol_Response_h_
#define rpc_processcontrol_Response_h_

#include <vector>
#include <string>

#include "toolbox/TimeVal.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/TimeVal.h"
#include "xdata/String.h"

#include "processcontrol/Request.h"
#include "processcontrol/xgitools/WarningLevel.h"

namespace xdata {
class AbstractBag;
} // namespace xdata

namespace processcontrol {

class Response
{
protected:
    static char const * const char_return_descriptions_[8];
    static char const * const char_status_return_descriptions_[5];
    static char const * const char_other_descriptions_[6];

public:
    // cf LSB iniscrptact
    static unsigned int const ok_               = 0;
    static unsigned int const error_            = 1;
    static unsigned int const arguments_        = 2;
    static unsigned int const not_implemented_  = 3;
    static unsigned int const not_privileged_   = 4;
    static unsigned int const not_installed_    = 5;
    static unsigned int const not_configured_   = 6;
    static unsigned int const not_running_      = 7;

    static unsigned int const status_ok_             = 256 + 0;
    static unsigned int const status_dead_with_pid_  = 256 + 1;
    static unsigned int const status_dead_with_lock_ = 256 + 2;
    static unsigned int const status_not_running_    = 256 + 3;
    static unsigned int const status_unknown_        = 256 + 4;

    static unsigned int const not_set_  = 512 + 0;
    static unsigned int const queued_   = 512 + 1;
    static unsigned int const failed_   = 512 + 2;
    static unsigned int const passed_   = 512 + 3;
    static unsigned int const warning_  = 512 + 4;
    static unsigned int const unknown_  = 512 + 5;

    static std::vector<std::string> const return_descriptions_;
    static std::vector<std::string> const status_return_descriptions_;
    static std::vector<std::string> const other_descriptions_;
    static std::string const unknown_description_;

public:
    Response(unsigned int _id = 0
             , unsigned int _action = Request::status_
             , toolbox::TimeVal const & _timeval = toolbox::TimeVal()
             , unsigned int _return_value = not_set_
             , std::string const & _description = std::string(""));

    static std::string const & getReturnDescription(unsigned int _action, unsigned int _return_value);

    unsigned int getId() const;
    unsigned int getAction() const;
    std::string const & getActionName() const;
    toolbox::TimeVal getTime() const;
    unsigned int getReturnValue() const;
    std::string const & getReturnDescription() const;
    std::string getDescription() const;

    xgitools::WarningLevel getWarningLevel() const;

    void setId(unsigned int _id);
    void setAction(unsigned int _action);
    void setAction(std::string const & _action_name);
    void setTime(toolbox::TimeVal const & _time);
    void setReturnValue(unsigned int _return_value);
    void setDescription(std::string const & _description);

    void registerFields(xdata::AbstractBag * _bag);

protected:
    xdata::UnsignedInteger id_;
    xdata::UnsignedInteger action_;
    xdata::TimeVal time_;
    xdata::UnsignedInteger return_value_;
    xdata::String description_;
};

} // namespace processcontrol

#include "processcontrol/Response-inl.h"

#endif // rpc_processcontrol_Response_h_
