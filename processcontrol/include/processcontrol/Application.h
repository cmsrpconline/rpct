#ifndef rpc_processcontrol_Application_h_
#define rpc_processcontrol_Application_h_

#include <map>
#include <string>
#include <utility>

#include "toolbox/BSem.h"
#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "processcontrol/Callback.h"
#include "processcontrol/ProcessController.h"

namespace processcontrol {

class Application
    : public xdaq::Application
    , public xgi::framework::UIManager
{
public:
    static std::string const time_format_;

public:
    XDAQ_INSTANTIATOR();

    Application(xdaq::ApplicationStub * _application_stub);
    ~Application();

    void xgiDefault(xgi::Input * _in, xgi::Output * _out) throw (xgi::exception::Exception);
    void xgiProcessRow(xgi::Input * _in, xgi::Output * _out
                       , Process const & _process, unsigned int _port
                       , int _process_count, int _parent_count
                       , Response const & _response) throw (xgi::exception::Exception);
    void xgiProcessTable(xgi::Input * _in, xgi::Output * _out) throw (xgi::exception::Exception);
    void xgiProcessAction(xgi::Input * _in, xgi::Output * _out) throw (xgi::exception::Exception);

    void xgiRedirect(xgi::Input * _in, xgi::Output * _out) throw (xgi::exception::Exception);

protected:

    void callback(Request const & _requests, Response const & _response);

protected:
    ObjectCallback<Application> callback_;
    ProcessController * controller_;

    std::string const url_, host_, urn_;
    mutable toolbox::BSem mutex_;
    std::map<Process, std::pair<unsigned int, Response> > process_port_response_;
};

} // namespace processcontrol

#endif // rpc_processcontrol_Application_h_
