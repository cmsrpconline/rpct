#ifndef rpc_processcontrol_Request_inl_h_
#define rpc_processcontrol_Request_inl_h_

#include "processcontrol/Request.h"

#include <algorithm>

namespace processcontrol {

inline bool Request::isAction(unsigned int _action)
{
    return _action < unknown_;
}

inline bool Request::isAction(std::string const & _action_name)
{
    return isAction(getAction(_action_name));
}

inline std::string const & Request::getActionName(unsigned int _action)
{
    if (_action < 9)
        return action_names_.at(_action);
    return unknown_name_;
}

inline unsigned int Request::getAction(std::string const & _action_name)
{
    std::vector<std::string>::const_iterator _action_it
        = std::find(action_names_.begin(), action_names_.end(), _action_name);
    if (_action_it != action_names_.end())
        return (_action_it - action_names_.begin());
    return unknown_;
}

inline std::string const & Request::getResponseListener() const
{
    return response_listener_.value_;
}

inline unsigned int Request::getId() const
{
    return id_.value_;
}

inline unsigned int Request::getAction() const
{
    return action_.value_;
}

inline std::string const & Request::getActionName() const
{
    return getActionName(action_.value_);
}

inline Process const & Request::getProcess() const
{
    return process_.bag;
}

inline toolbox::TimeVal const & Request::getTime() const
{
    return time_.value_;
}

inline unsigned int Request::getTimeout() const
{
    return timeout_.value_;
}

inline void Request::setResponseListener(std::string const & _response_listener)
{
    response_listener_.value_ = _response_listener;
}

inline void Request::setAction(unsigned int _action)
{
    action_.value_ = _action;
}

inline void Request::setAction(std::string const & _action_name)
{
    action_.value_ = getAction(_action_name);
}

inline void Request::setProcess(Process const & _process)
{
    process_.bag = _process;
}

inline void Request::setProcess(std::string const & _group, std::string const & _host, std::string const & _zone
                                , std::string const & _service)
{
    process_.bag = Process(_group, _host, _zone, _service);
}

inline void Request::setTime(toolbox::TimeVal const & _time)
{
    if (_time == toolbox::TimeVal())
        time_.value_ = toolbox::TimeVal::gettimeofday();
    else
        time_.value_ = _time;
}

inline void Request::setTimeout(unsigned int _timeout)
{
    timeout_.value_ = _timeout;
}

} // namespace processcontrol

#endif // rpc_processcontrol_Request_inl_h_
