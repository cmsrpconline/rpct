#ifndef rpc_processcontrol_Callback_h_
#define rpc_processcontrol_Callback_h_

namespace processcontrol {

class Request;
class Response;

class CallbackInterface
{
public:
    virtual ~CallbackInterface();

    virtual void operator()(Request const &, Response const &) throw() = 0;
};

class Callback
    : public CallbackInterface
{
public:
    Callback(void (* _callback)(Request const &, Response const &));

    void operator()(Request const & _request, Response const & _response) throw();

protected:
    void (* callback_)(Request const &, Response const &);
};

template<typename t_object_type>
class ObjectCallback
    : public CallbackInterface
{
public:
    ObjectCallback(t_object_type & _object
                   , void (t_object_type::* _callback)(Request const &, Response const &));

    void operator()(Request const & _request, Response const & _response) throw();

protected:
    t_object_type & object_;
    void (t_object_type::* callback_)(Request const &, Response const &);
};

} // namespace processcontrol

#include "processcontrol/Callback-inl.h"

#endif // rpc_processcontrol_Callback_h_
