#ifndef rpc_processcontrol_ProcessController_h_
#define rpc_processcontrol_ProcessController_h_

#include <set>
#include <string>

#include "log4cplus/logger.h"

#include "toolbox/BSem.h"

#include "processcontrol/Callback.h"
#include "processcontrol/Process.h"
#include "processcontrol/Request.h"
#include "processcontrol/Response.h"
#include "processcontrol/exception/Exception.h"

namespace xdaq {
class Application;
} // namespace xdaq

namespace processcontrol {

class ProcessController
{
public:
    ProcessController(xdaq::Application & _application
                      , CallbackInterface & _callback
                      , unsigned int _max_timeout = 60);
    virtual ~ProcessController();

    std::set<Process> const & getProcesses() const;
    bool hasProcess(Process const & _process) const;
    bool insertProcess(Process const & _process);
    bool eraseProcess(Process const & _process);

    unsigned int getMaxTimeout() const;
    void setMaxTimeout(unsigned int _seconds);

    std::string const & getURL() const;
    std::string const & getHost() const;

    log4cplus::Logger const & getLogger() const;

    virtual Response action(Request _request) = 0;
    virtual std::vector<Response> action(std::vector<Request> _requests);

    Response action(unsigned int _action, Process const & _process
                    , unsigned int _timeout = 0);
    Response action(std::string const & _action, Process const & _process
                    , unsigned int _timeout = 0);

    Response status (Process const & _process, unsigned int _timeout = 0);
    Response start  (Process const & _process, unsigned int _timeout = 0);
    Response stop   (Process const & _process, unsigned int _timeout = 0);
    Response restart(Process const & _process, unsigned int _timeout = 0);

protected:
    xdaq::Application & application_;
    log4cplus::Logger logger_;

    CallbackInterface & callback_;

    std::string const url_, host_;
    mutable toolbox::BSem mutex_;
    unsigned int max_timeout_;

    std::set<Process> processes_;
};

} // namespace processcontrol

#include "processcontrol/ProcessController-inl.h"

#endif // rpc_processcontrol_ProcessController_h_
