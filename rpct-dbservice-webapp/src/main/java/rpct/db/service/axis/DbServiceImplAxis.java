package rpct.db.service.axis;


import java.math.BigInteger;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Comparator;

import org.apache.axis.AxisFault;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import rpct.db.DataAccessException;
import rpct.db.domain.condition.ConditionDAO;
import rpct.db.domain.condition.ConditionDAOHibernate;
import rpct.db.domain.condition.hibernate.SimpleCondHibernateContextImpl;
import rpct.db.domain.configuration.ChipConfAssignment;
import rpct.db.domain.configuration.ConfigSetInfo;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.FebChipConf;
import rpct.db.domain.configuration.HalfSortConf;
import rpct.db.domain.configuration.IntArray24;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.configuration.PacConf;
import rpct.db.domain.configuration.RbcConf;
import rpct.db.domain.configuration.RmbConf;
import rpct.db.domain.configuration.StaticConfiguration;
import rpct.db.domain.configuration.SynCoderConf;
import rpct.db.domain.configuration.SynCoderConfInFlash;
import rpct.db.domain.configuration.TbGbSortConf;
import rpct.db.domain.configuration.TcSortConf;
import rpct.db.domain.configuration.TtuFinalConf;
import rpct.db.domain.configuration.TtuTrigConf;
import rpct.db.domain.configuration.XdaqAppLBoxAccess;
import rpct.db.domain.configuration.XdaqAppVmeCrateAccess;
import rpct.db.domain.configuration.XdaqExecutive;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipDAO;
import rpct.db.domain.equipment.ChipDAOHibernate;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.FebBoard;
import rpct.db.domain.equipment.FebConnectorDAO;
import rpct.db.domain.equipment.FebConnectorDAOHibernate;
import rpct.db.domain.equipment.I2cCbChannel;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.equipment.TriggerBoard;
import rpct.db.domain.equipment.TriggerCrate;
import rpct.db.domain.equipment.TtuBoard;
import rpct.db.domain.equipment.chamberloaction.BarrelOrEndcap;
import rpct.db.domain.equipment.chamberloaction.ChamberLocation;
import rpct.db.domain.equipment.chamberloaction.ChamberLocationDAO;
import rpct.db.domain.equipment.chamberloaction.ChamberLocationDAOHibernate;
import rpct.db.domain.equipment.chamberstrip.ChamberStripDAO;
import rpct.db.domain.equipment.chamberstrip.ChamberStripDAOHibernate;
import rpct.db.domain.equipment.controlboard.ControlBoard;
import rpct.db.domain.equipment.feblocation.FebLocation;
import rpct.db.domain.equipment.feblocation.FebLocationDAO;
import rpct.db.domain.equipment.feblocation.FebLocationDAOHibernate;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.db.service.CcuRingInfo;
import rpct.db.service.ChamStripAccessInfo;
import rpct.db.service.FebConnectorStrips;
import rpct.db.service.DTLineFebChipFlatInfo;
import rpct.db.service.DTLineFebInfo;
import rpct.db.service.DTLineFebChipInfo;
import rpct.db.service.ChipConfigurationAssignment;
import rpct.db.service.ConfigurationSet;
import rpct.db.service.FebAccessInfo;
import rpct.db.service.FebChipConfiguration;
import rpct.db.service.FebChipInfo;
import rpct.db.service.HalfSortConfiguration;
import rpct.db.service.OptoConfiguration;
import rpct.db.service.PacConfiguration;
import rpct.db.service.RbcConfiguration;
import rpct.db.service.RbcInfo;
import rpct.db.service.RmbConfiguration;
import rpct.db.service.SynCoderConfInFlashInfo;
import rpct.db.service.SynCoderConfiguration;
import rpct.db.service.TbGbSortConfiguration;
import rpct.db.service.TcSortConfiguration;
import rpct.db.service.TtuFinalConfiguration;
import rpct.db.service.TtuTrigConfiguration;
import rpct.xdaq.axis.Binary;
import rpct.xdaqaccess.hardwaredb.HardwareDbMapper;

public class DbServiceImplAxis {

    private final static Log log = LogFactory.getLog(DbServiceImplAxis.class);
    
    private final String[] HSB0_INPUTS = {"TC_11", "TC_0", "TC_1", "TC_2", "TC_3", "TC_4", "TC_5", "TC_6"};
    private final String[] HSB1_INPUTS = {"TC_5", "TC_6", "TC_7", "TC_8", "TC_9", "TC_10", "TC_11", "TC_0"};

    private HibernateContext hibernateContext = new SimpleHibernateContextImpl();

    private HibernateContext condHibernateContext = new SimpleCondHibernateContextImpl();

    private ChipDAO chipDAO = new ChipDAOHibernate(hibernateContext);

    private ChamberLocationDAO locationDAO = new ChamberLocationDAOHibernate(
            hibernateContext);

    private ChamberStripDAO chamberStripDAO = new ChamberStripDAOHibernate(hibernateContext);

    private FebConnectorDAO febConnectorDAO = new FebConnectorDAOHibernate(hibernateContext);

    private FebLocationDAO febLocationDAO = new FebLocationDAOHibernate(
            hibernateContext);

    private EquipmentDAO equipmentDAO = new EquipmentDAOHibernate(
            hibernateContext);

    private ConfigurationDAO configurationDAO = new ConfigurationDAOHibernate(
            hibernateContext);
    
    private ConditionDAO conditionDAO = new ConditionDAOHibernate(
    		condHibernateContext);
    
    private ConfigurationManager configurationManager = new ConfigurationManager(hibernateContext, equipmentDAO, configurationDAO);

    private HardwareDbMapper dbMapper; // TODO temporary place

    public DbServiceImplAxis() {
    }

    private void fillFebResultList(Iterable<FebLocation> febLocations,
            List<FebAccessInfo> resultList) throws DataAccessException {
        // TODO optimize !!!
        Map<Integer, Integer> cbToAppInstanceMap = new HashMap<Integer, Integer>();
        for (FebLocation febLocation : febLocations) {
            ChamberLocation location = febLocation.getChamberLocation();
            I2cCbChannel channel = febLocation.getI2cCbChannel();
            ControlBoard cb = channel.getControlBoard();
            Integer xdaqAppInstance = -1;
            if (cb != null) {
                xdaqAppInstance = cbToAppInstanceMap.get(cb.getId());
                if (xdaqAppInstance == null) {
                    xdaqAppInstance = configurationDAO.getXdaqApplication(cb).getInstance();
                    cbToAppInstanceMap.put(cb.getId(), xdaqAppInstance);
                }
            }
            int ccuAddress = (cb != null) ? cb.getCcuAddress() : -1;
            if (ccuAddress == -1) {
                log.warn("No control board for "
                        + location.getChamberLocationName() + "  "
                        + "FEB Location: " + febLocation.getId() + "  "
                        + "FEB Local Eta Partition: "
                        + febLocation.getFebLocalEtaPartition() + "  "
                        + "FEB I2C Address: " + febLocation.getI2cLocalNumber()
                        + "  " + "I2cCbChannel id: " + channel.getId() + "  "
                        + "ControlBoardChannel: " + channel.getCbChannel());

            } else {
                log.debug(location.getChamberLocationName() + "  "
                        + "FEB Location: " + febLocation.getId() + "  "
                        + "FEB Local Eta Partition: "
                        + febLocation.getFebLocalEtaPartition() + "  "
                        + "FEB I2C Address: " + febLocation.getI2cLocalNumber()
                        + "  " + "I2cCbChannel id: " + channel.getId() + "  "
                        + "ControlBoardChannel: " + channel.getCbChannel()
                        + "  " + "CCU Address:" + ccuAddress);
            }
            resultList.add(new FebAccessInfoImplAxis(febLocation.getId(), "http://localhost:1972",
                    xdaqAppInstance, febLocation.getFebLocalEtaPartition(),
                    location.getChamberLocationName(), ccuAddress, channel
                            .getCbChannel(), febLocation.getI2cLocalNumber(),
                    febLocation.getPosInLocalEtaPartition()));
        }
    }

    public FebAccessInfo[] getFebsByBarrelOrEndcap(String barrelOrEndcap)
            throws RemoteException {
        List<FebAccessInfo> resultList = new ArrayList<FebAccessInfo>();
        try {
            List<ChamberLocation> locations = locationDAO
                    .getByBarrelOrEndcap(BarrelOrEndcap.valueOf(barrelOrEndcap));
            for (ChamberLocation location : locations) {
                fillFebResultList(location.getFEBLocations(), resultList);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                hibernateContext.closeSession();
            } catch (Exception e) {
                log.error(e);
            }
        }
        return resultList.toArray(new FebAccessInfo[] {});
    }

    public FebAccessInfo[] getFebsByDiskOrWheel(BigInteger diskOrWheel,
            String barrelOrEndcap) throws RemoteException {
        List<FebAccessInfo> resultList = new ArrayList<FebAccessInfo>();
        try {
            List<ChamberLocation> locations = locationDAO.getByDiskOrWheel(
                    diskOrWheel.intValue(), BarrelOrEndcap
                            .valueOf(barrelOrEndcap));
            for (ChamberLocation location : locations) {
                fillFebResultList(location.getFEBLocations(), resultList);
            }
        } catch (Exception e) {
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                hibernateContext.closeSession();
            } catch (Exception e) {
                log.error(e);
            }
        }
        return resultList.toArray(new FebAccessInfo[] {});
    }

    public FebAccessInfo[] getFebsByLayer(BigInteger diskOrWheel,
            BigInteger layer, String barrelOrEndcap) throws RemoteException {
        List<FebAccessInfo> resultList = new ArrayList<FebAccessInfo>();
        try {
            List<ChamberLocation> locations = locationDAO.getByLayer(
                    diskOrWheel.intValue(), layer.intValue(), BarrelOrEndcap
                            .valueOf(barrelOrEndcap));
            for (ChamberLocation location : locations) {
                fillFebResultList(location.getFEBLocations(), resultList);
            }
        } catch (Exception e) {
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                hibernateContext.closeSession();
            } catch (Exception e) {
                log.error(e);
            }
        }
        return resultList.toArray(new FebAccessInfo[] {});
    }

    public FebAccessInfo[] getFebsBySector(BigInteger diskOrWheel,
            BigInteger sector, String barrelOrEndcap) throws RemoteException {
        List<FebAccessInfo> resultList = new ArrayList<FebAccessInfo>();
        try {
            List<ChamberLocation> locations = locationDAO.getBySector(
                    diskOrWheel.intValue(), sector.intValue(), BarrelOrEndcap
                            .valueOf(barrelOrEndcap));
            for (ChamberLocation location : locations) {
                fillFebResultList(location.getFEBLocations(), resultList);
            }
        } catch (Exception e) {
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                hibernateContext.closeSession();
            } catch (Exception e) {
                log.error(e);
            }
        }
        return resultList.toArray(new FebAccessInfo[] {});
    }

    public FebAccessInfo[] getFebsByChamberLocation(BigInteger diskOrWheel,
            BigInteger layer, BigInteger sector, String subsector,
            String barrelOrEndcap) throws RemoteException {
        List<FebAccessInfo> resultList = new ArrayList<FebAccessInfo>();
        try {
            List<ChamberLocation> locations = locationDAO.getByLocation(
                    diskOrWheel.intValue(), layer.intValue(),
                    sector.intValue(), subsector, BarrelOrEndcap
                            .valueOf(barrelOrEndcap));
            for (ChamberLocation location : locations) {
                fillFebResultList(location.getFEBLocations(), resultList);
            }
        } catch (Exception e) {
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                hibernateContext.closeSession();
            } catch (Exception e) {
                log.error(e);
            }
        }
        return resultList.toArray(new FebAccessInfo[] {});
    }

    public FebAccessInfo[] getFebsByLocationId(BigInteger locationId)
            throws RemoteException {
        List<FebAccessInfo> resultList = new ArrayList<FebAccessInfo>();
        try {
            FebLocation febLocation = febLocationDAO.getById(locationId
                    .intValue());
            ChamberLocation chamberLocation = febLocation.getChamberLocation();
            I2cCbChannel channel = febLocation.getI2cCbChannel();
            ControlBoard cb = channel.getControlBoard();
            System.out.println(chamberLocation.getChamberLocationName() + "  "
                    + "FEB Location: " + febLocation.getId() + "  "
                    + "FEB Local Eta Partition: "
                    + febLocation.getFebLocalEtaPartition() + "  "
                    + "FEB I2C Address: " + febLocation.getI2cLocalNumber()
                    + "  " + "Distribution board: " + channel.getId() + "  "
                    + "ControlBoardChannel: " + channel.getCbChannel() + "  "
                    + "CCU Address:" + cb.getCcuAddress());
            resultList.add(new FebAccessInfoImplAxis(febLocation.getId(), "http://localhost:1972",
                    cb.getFecPosition(), febLocation.getFebLocalEtaPartition(),
                    chamberLocation.getChamberLocationName(), cb
                            .getCcuAddress(), channel.getCbChannel(),
                    febLocation.getI2cLocalNumber(), febLocation
                            .getPosInLocalEtaPartition()));
        } catch (Exception e) {
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                hibernateContext.closeSession();
            } catch (Exception e) {
                log.error(e);
            }
        }
        return resultList.toArray(new FebAccessInfo[] {});
    }

    public FebAccessInfo[] getSingleFebByLocParameters(BigInteger diskOrWheel,
            BigInteger layer, BigInteger sector, String subsector,
            String barrelOrEndcap, String febLocalEtaPartition,
            BigInteger posInLocalEtaPartition) throws RemoteException {
        List<FebAccessInfo> resultList = new ArrayList<FebAccessInfo>();
        try {
            List<ChamberLocation> locations = locationDAO.getByLocation(
                    diskOrWheel.intValue(), layer.intValue(),
                    sector.intValue(), subsector, BarrelOrEndcap
                            .valueOf(barrelOrEndcap));
            for (ChamberLocation location : locations) {
                List<FebLocation> febLocations = febLocationDAO
                        .getByLocParameters(location, febLocalEtaPartition,
                                posInLocalEtaPartition.intValue());
                fillFebResultList(febLocations, resultList);
            }
        } catch (Exception e) {
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                hibernateContext.closeSession();
            } catch (Exception e) {
                log.error(e);
            }
        }
        return resultList.toArray(new FebAccessInfo[] {});
    }


    public ChamStripAccessInfo[] getChamStripByChamLoc(BigInteger diskOrWheel,
            BigInteger layer, BigInteger sector, String subsector,
            String barrelOrEndcap) throws RemoteException {
        
        System.out.println("Original Subsector " + subsector);
        if ("none".equals(subsector)) { 
            subsector = null;
        }        
        System.out.println("Subsector after reset " + subsector);

        List<ChamStripAccessInfo> resultList = new ArrayList<ChamStripAccessInfo>();
        try {    
            return equipmentDAO.getChamStripAccessInfosByChamberLocation(
                    diskOrWheel.intValue(), layer.intValue(),
                    sector.intValue(), subsector, BarrelOrEndcap
                            .valueOf(barrelOrEndcap)).toArray(new ChamStripAccessInfo[]{});
        
        } catch (Exception e) {
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                hibernateContext.closeSession();
            } catch (Exception e) {
                log.error(e);
            }
        }
    }
    

    public FebConnectorStrips[] getFebConnectorStrips(String xdaqExecutiveHost
                                                      , BigInteger xdaqExecutivePort) throws RemoteException {
        List<FebConnectorStrips> resultList = new ArrayList<FebConnectorStrips>();
        try {    
            return equipmentDAO.getFebConnectorStrips(xdaqExecutiveHost, xdaqExecutivePort.intValue())
                .toArray(new FebConnectorStrips[]{});
        } catch (Exception e) {
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                hibernateContext.closeSession();
            } catch (Exception e) {
                log.error(e);
            }
        }
    }


    public DTLineFebInfo[] getDTLineFebInfo(BigInteger wheel) throws RemoteException {
        log.info("called getDTLineFebInfo for wheel " + wheel);
        List<DTLineFebInfoImplAxis> resultList = new ArrayList<DTLineFebInfoImplAxis>();
        int lastBoardId = -1;
        try {
            List<DTLineFebChipFlatInfo> flatChips = equipmentDAO.getDTLineFebChipFlatInfo(wheel.intValue());
            for (DTLineFebChipFlatInfo flatChip : flatChips) {
                if (lastBoardId != flatChip.getBoardId() || resultList.isEmpty()) {
                    resultList.add(new DTLineFebInfoImplAxis(flatChip.getBoardId()
                                                             , flatChip.getName()
                                                             , flatChip.getI2cAddress()
                                                             , flatChip.getBoardDisabled()
                                                             , flatChip.getLocation()
                                                             , flatChip.getPartition()
                                                             // , flatChip.getFebLocationId()
                                                             , new DTLineFebChipInfoImplAxis[2]));
                    lastBoardId = flatChip.getBoardId();
                }
                resultList.get(resultList.size() - 1).getFebChipInfos()[flatChip.getPosition()]
                    = new DTLineFebChipInfoImplAxis(flatChip.getChipId()
                                                    , flatChip.getPosition()
                                                    , flatChip.getChipDisabled());
            }
            DTLineFebInfo[] dtlinefebinfos = resultList.toArray(new DTLineFebInfo[] {});
        return dtlinefebinfos;
        } catch (Exception e) {
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                hibernateContext.closeSession();
            } catch (Exception e) {
                log.error(e);
            }
        }
    }


    public RbcInfo[] getAllRbcsInfo() throws RemoteException {
        try {    
            return equipmentDAO.getAllRbcInfo().toArray(new RbcInfo[]{});        
        } catch (Exception e) {
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                hibernateContext.closeSession();
            } catch (Exception e) {
                log.error(e);
            }
        }
    }

    public CcuRingInfo getCcuRingInfoForXdaqApp(String host, BigInteger port,
            BigInteger instance) throws RemoteException {

        try {
            XdaqExecutive xdaqExecutive = configurationDAO.getXdaqExecutive(
                    host, port.intValue());
            if (xdaqExecutive == null) {
                throw new RemoteException("Xdaq executive " + host + ":" + port
                        + " not found");
            }
            XdaqAppLBoxAccess xdaqApplication = (XdaqAppLBoxAccess) configurationDAO
                    .getXdaqApplication(xdaqExecutive, "rpcttsworker::LBCell",
                            instance.intValue());
            if (xdaqApplication == null) {
                throw new RemoteException(
                        "Xdaq application XdaqLBoxAccess instance " + instance
                                + " on executive " + host + ":" + port
                                + " not found");
            }
            List<LinkBox> linkBoxes = equipmentDAO.getLinkBoxesByFec(
                    xdaqApplication.getCcsBoard(), xdaqApplication
                            .getFecPosition());

            if (linkBoxes == null || linkBoxes.isEmpty()) {
                throw new RemoteException(
                        "No link boxes found for xdaq application XdaqLBoxAccess instance "
                                + instance + " on executive " + host + ":"
                                + port);
            }

            SortedSet<Integer> disabledChipIds = new TreeSet<Integer>(
                    configurationDAO.getDisabledChipsIds());
            
            SortedSet<Integer> disabledBoardIds = new TreeSet<Integer>(
                    configurationDAO.getDisabledBoardIds());
            
            SortedSet<Integer> disabledCrateIds = new TreeSet<Integer>(
                    configurationDAO.getDisabledCrateIds());

            SortedSet<Integer> febDtControlledIds = new TreeSet<Integer>(
                    configurationDAO.getFebDtControlledIds());
            
            
            List<LinkBoxInfoImplAxis> linkBoxInfoList = new ArrayList<LinkBoxInfoImplAxis>();
            
            List<FebBoardInfoImplAxis> febBoardInfoList = new ArrayList<FebBoardInfoImplAxis>();

            for (LinkBox linkBox : linkBoxes) {
                log.debug(linkBox.toString());
                boolean lbbDisabled = disabledCrateIds.contains(linkBox.getId());                
                List<Board> boards = lbbDisabled ? Collections.EMPTY_LIST : linkBox.getBoards();
                List<BoardInfoImplAxis> boardInfoList = new ArrayList<BoardInfoImplAxis>();

                for (Board board : boards) {
                    log.debug("   " + board.toString());
                    boolean boardDisabled = disabledBoardIds.contains(board.getId()); 
                    Set<Chip> chips = boardDisabled ? null : board.getChips(); //if board disabled no chips added to the chips
                    List<ChipInfoImplAxis> chipInfoList = new ArrayList<ChipInfoImplAxis>();
                    if (chips != null) {
                        for (Chip chip : chips) {
                            log.debug("       " + chip.toString());
                            chipInfoList.add(new ChipInfoImplAxis(chip.getId(),
                                    chip.getPosition(), chip.getType().toString(), 
                                    disabledChipIds.contains(chip.getId())));
                        }
                    } else {
                        chipInfoList.add(new ChipInfoImplAxis(0, 0, "", false));
                    }
                    if(board.getType() == BoardType.FEBBOARD) {
                    	febBoardInfoList.add(new FebBoardInfoImplAxis(board.getId(),
                                board.getName(), board.getLabel(), board.getType().toString(), 
                                ((FebBoard)board).getI2cLocalNumber(), boardDisabled,
                                chipInfoList.toArray(new ChipInfoImplAxis[] {}), ((FebBoard)board).getControlBoard().getId(), 
                                ((FebBoard)board).getCbChannel(), 
                                febDtControlledIds.contains(board.getId())));
                    }
                    else {
                    	boardInfoList.add(new BoardInfoImplAxis(board.getId(),
                            board.getName(), board.getLabel(), board.getType()
                                    .toString(), board.getPosition(), boardDisabled,
                            chipInfoList.toArray(new ChipInfoImplAxis[] {})));
                    }
                }         
                
                final Comparator<BoardInfoImplAxis> BOARDS_ORDER = 
                		new Comparator<BoardInfoImplAxis>() {
                	public int compare(BoardInfoImplAxis o1, BoardInfoImplAxis o2) {
                		if(o1.getPositionOrAddress() < o2.getPositionOrAddress())
                    		return -1;
                    	if(o1.getPositionOrAddress() > o2.getPositionOrAddress())
                    		return 1;
                    	else
                    		return 0; 
                	}
                };
                
                Collections.sort(boardInfoList, BOARDS_ORDER);
                linkBoxInfoList.add(new LinkBoxInfoImplAxis(linkBox.getId(),
                        linkBox.getControlBoard10().getCcuAddress(), linkBox
                                .getControlBoard20().getCcuAddress(), linkBox
                                .getName(), linkBox.getLabel(), lbbDisabled, 
                                boardInfoList.toArray(new BoardInfoImplAxis[] {})));
            }

            return new CcuRingInfoImplAxis(xdaqApplication.getPciSlot(), xdaqApplication.getCcsBoard()
                    .getPosition(), xdaqApplication.getFecPosition(),
                    linkBoxInfoList.toArray(new LinkBoxInfoImplAxis[] {}), febBoardInfoList.toArray(new FebBoardInfoImplAxis[] {}));
        } catch (Exception e) {
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                hibernateContext.closeSession();
            } catch (Exception e) {
                log.error(e);
            }
        }
    }
    
    public VmeCrateInfoImplAxis getVmeCrateInfo(String host, BigInteger port,
            String className, BigInteger instance) throws RemoteException {
        log.info("called getVmeCrateInfo: " + host + " port " + port + " className " + className + " instance " + instance);
        try {
        	// TODO optimize - find xdaqApplication in one query
            XdaqAppVmeCrateAccess xdaqApplication = (XdaqAppVmeCrateAccess) configurationDAO
                    .getXdaqApplication(host, port.intValue(), className, instance.intValue());
            if (xdaqApplication == null) {
                throw new AxisFault(
                        "Xdaq application " + className + " instance " + instance
                                + " on executive " + host + ":" + port
                                + " not found");
            }
            Crate crate = (Crate) xdaqApplication.getCrate();
            if (crate == null) {
                throw new AxisFault(
                        "No crate found for xdaq application " + className + " instance "
                                + instance + " on executive " + host + ":"
                                + port);
            }
            int logSector = (crate instanceof TriggerCrate) ? ((TriggerCrate) crate).getLogSector() : -1;
            boolean crateDisabled = configurationDAO.getCrateDisabled(crate) != null;
            log.info(" getVmeCrateInfo: " + host + " port " + port + " className " + className + " instance " + instance +
            		" found crate " + crate + " crateDisabled: " + crateDisabled);
            
            if (crateDisabled) {
                return new VmeCrateInfoImplAxis(crate.getId(), crate.getName(), crate.getLabel(),
                        xdaqApplication.getPciSlot(), xdaqApplication.getPosInVmeChain(),
                        crate.getType(), logSector, true, new BoardInfoImplAxis[]{});
            }
            else {
                SortedSet<Integer> disabledChipIds = new TreeSet<Integer>(
                        configurationDAO.getDisabledChipsIds());

                SortedSet<Integer> disabledBoardIds = new TreeSet<Integer>(
                        configurationDAO.getDisabledBoardIds());

                log.debug(crate.toString());
                List<Board> boards = crate.getBoards();
                List<BoardInfoImplAxis> boardInfoList = new ArrayList<BoardInfoImplAxis>();

                for (Board board : boards) {
                    log.debug("   " + board.toString());
                    boolean boardDisabled = disabledBoardIds.contains(board.getId());
                    Set<Chip> chips = boardDisabled ? new HashSet<Chip>() : board.getChips();
                    List<ChipInfoImplAxis> chipInfoList = new ArrayList<ChipInfoImplAxis>();
                    if (chips != null) {
                        for (Chip chip : chips) {
                            log.debug("       " + chip.toString());
                            chipInfoList.add(new ChipInfoImplAxis(chip.getId(),
                                    chip.getPosition(), chip.getType().toString(), 
                                    disabledChipIds.contains(chip.getId())));
                        }
                    } else {
                        chipInfoList.add(new ChipInfoImplAxis(0, 0, "", false));
                    }
                    boardInfoList.add(new BoardInfoImplAxis(board.getId(),
                            board.getName(), board.getLabel(), board.getType()
                            .toString(), board.getPosition(), boardDisabled,
                            chipInfoList.toArray(new ChipInfoImplAxis[] {})));
                }
                log.info("finshed getVmeCrateInfo: " + host + " port " + port + " className " + className + " instance " + instance);
                return new VmeCrateInfoImplAxis(crate.getId(), crate.getName(), crate.getLabel(),
                        xdaqApplication.getPciSlot(), xdaqApplication.getPosInVmeChain(), crate.getType(), 
                        logSector, false, boardInfoList.toArray(new BoardInfoImplAxis[] {}));

            }

            
        } catch (Exception e) {
        	log.error(e);
            throw new AxisFault("Xdaq application " + className + " instance " + instance + ": " + e.getMessage(), 
                    e);
        } finally {
            try {
                hibernateContext.closeSession();
            } catch (Exception e) {
                log.error(e);
            }
        }
    }

    public TriggerCrateInfoImplAxis getTriggerCrateInfo(String host, BigInteger port,
            String className, BigInteger instance) throws RemoteException {
    	log.info("called getTriggerCrateInfo: " + host + " port " + port + " className " + className + " instance " + instance);
        try {
            XdaqExecutive xdaqExecutive = configurationDAO.getXdaqExecutive(
                    host, port.intValue());
            if (xdaqExecutive == null) {
                throw new RemoteException("Xdaq executive " + host + ":" + port
                        + " not found");
            }
            XdaqAppVmeCrateAccess xdaqApplication = (XdaqAppVmeCrateAccess) configurationDAO
                    .getXdaqApplication(xdaqExecutive, className,
                            instance.intValue());
            if (xdaqApplication == null) {
                throw new RemoteException(
                        "Xdaq application " + className + " instance " + instance
                                + " on executive " + host + ":" + port
                                + " not found");
            }
            TriggerCrate crate = (TriggerCrate) xdaqApplication.getCrate();
            
            if (crate == null) {
                throw new RemoteException(
                        "No crate found for xdaq application " + className + " instance "
                                + instance + " on executive " + host + ":"
                                + port);
            }   
            
            boolean crateDisabled = configurationDAO.getCrateDisabled(crate) != null;

            if (crateDisabled) {
                return new TriggerCrateInfoImplAxis(crate.getId(), crate.getName(), crate.getLabel(),
                        xdaqApplication.getPciSlot(), xdaqApplication.getPosInVmeChain(),
                        true, new BoardInfoImplAxis[]{}, crate.getLogSector());
            }
            else {
                SortedSet<Integer> disabledChipIds = new TreeSet<Integer>(
                        configurationDAO.getDisabledChipsIds());

                SortedSet<Integer> disabledBoardIds = new TreeSet<Integer>(
                        configurationDAO.getDisabledBoardIds());

                log.debug(crate.toString());
                List<Board> boards = crate.getBoards();
                List<BoardInfoImplAxis> boardInfoList = new ArrayList<BoardInfoImplAxis>();

                for (Board board : boards) {
                    log.debug("   " + board.toString());
                    boolean boardDisabled = disabledBoardIds.contains(board.getId());
                    Set<Chip> chips = boardDisabled ? new HashSet<Chip>() : board.getChips();
                    List<ChipInfoImplAxis> chipInfoList = new ArrayList<ChipInfoImplAxis>();
                    if (chips != null) {
                        for (Chip chip : chips) {
                            log.debug("       " + chip.toString());
                            chipInfoList.add(new ChipInfoImplAxis(chip.getId(),
                                    chip.getPosition(), chip.getType().toString(), 
                                    disabledChipIds.contains(chip.getId())));
                        }
                    } else {
                        chipInfoList.add(new ChipInfoImplAxis(0, 0, "", false));
                    }
                    boardInfoList.add(new BoardInfoImplAxis(board.getId(),
                            board.getName(), board.getLabel(), board.getType()
                            .toString(), board.getPosition(), boardDisabled,
                            chipInfoList.toArray(new ChipInfoImplAxis[] {})));
                }
                log.info("finished getTriggerCrateInfo: " + host + " port " + port + " className " + className + " instance " + instance);
                return new TriggerCrateInfoImplAxis(crate.getId(), crate.getName(), crate.getLabel(),
                        xdaqApplication.getPciSlot(), xdaqApplication.getPosInVmeChain(), false, 
                        boardInfoList.toArray(new BoardInfoImplAxis[] {}), crate.getLogSector());

            }

            
        } catch (Exception e) {
            throw new RemoteException("Xdaq application " + className + " instance " + instance + ": " + e.getMessage(), 
                    e);
        } finally {
            try {
                hibernateContext.closeSession();
            } catch (Exception e) {
                log.error(e);
            }
        }
    }

    static private int indexOf(String[] array, String value) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(value)) {
                return i;
            }
        }
        return -1;
    }
     
    private ConfigurationSet getConfigurationSet(List<LocalConfigKey> localConfigKeys, int timestamp, List<Integer> chipIds, List<ChipConfAssignment> chipConfAssignments) throws RemoteException {
    	log.info("started getConfigurationSet(): ");
        Map<Integer, SynCoderConfiguration> synCoderConfigMap = new HashMap<Integer, SynCoderConfiguration>();
        Map<Integer, OptoConfiguration> optoConfigMap = new HashMap<Integer, OptoConfiguration>();
        Map<Integer, PacConfiguration> pacConfigMap = new HashMap<Integer, PacConfiguration>();
        Map<Integer, TbGbSortConfiguration> tbGbSortConfigMap = new HashMap<Integer, TbGbSortConfiguration>();
        Map<Integer, RmbConfiguration> rmbConfigMap = new HashMap<Integer, RmbConfiguration>();
        Map<Integer, TcSortConfiguration> tcSortConfigMap = new HashMap<Integer, TcSortConfiguration>();
        Map<Integer, HalfSortConfiguration> halfSortConfigMap = new HashMap<Integer, HalfSortConfiguration>();
        Map<Integer, RbcConfiguration> rbcConfigMap = new HashMap<Integer, RbcConfiguration>();
        Map<Integer, TtuTrigConfiguration> ttuTrigConfigMap = new HashMap<Integer, TtuTrigConfiguration>();
        Map<Integer, TtuFinalConfiguration> ttuFinalConfigMap = new HashMap<Integer, TtuFinalConfiguration>();
        Map<Integer, FebChipConfiguration> febConfigMap = new HashMap<Integer, FebChipConfiguration>();
        
        List<ChipConfigurationAssignmentImplAxis> chipConfiAssignList = new ArrayList<ChipConfigurationAssignmentImplAxis>();

        List<SynCoderConfInFlashInfo> synCoderConfInFlashes = new ArrayList<SynCoderConfInFlashInfo>();
        try {        	
        	LocalConfigKey ttuRbcConfigKey = null;
        	int combinedKeyId = 0;
        	/* keyId together with the timestamp are used in the xdaq for checking, if the configuration in the hardware 
        	 * needs to be updated. 
        	 * However this mechanism is potentially dangerous - the below implementation takes only 8 bits of the configKey.getId()
        	 * so it is possible that the combinedKeyId s not unique
        	 * therefore the SC configuration does not depend on that Id and timestamp  
        	 * the same is for the LBoxes.
        	 * for the TC it should work until only one config key is possible 
        	 * */
        	int i = 0;
        	for(LocalConfigKey configKey : localConfigKeys) {
        		if(configKey.getSubsystem() == LocalConfigKey.Subsystem.RBCS_TTUS) {
        			ttuRbcConfigKey = configKey;
        		}
        		combinedKeyId |=  configKey.getId() << i * 8;
        		i++;
        	}
            for (ChipConfAssignment assignment : chipConfAssignments) {
                StaticConfiguration staticConfiguration = assignment
                        .getStaticConfiguration();
                if (staticConfiguration instanceof SynCoderConf) {
                    if (!synCoderConfigMap.containsKey(staticConfiguration
                            .getId())) {
                        SynCoderConf synCoderConf = (SynCoderConf) staticConfiguration;
                        SynCoderConfigurationImplAxis synCoderConfiguration = new SynCoderConfigurationImplAxis(
                                synCoderConf.getId(),
                                synCoderConf.getWindowOpen(),
                                synCoderConf.getWindowClose(),
                                synCoderConf.isInvertClock(),
                                synCoderConf.getLmuxInDelay(),
                                synCoderConf.getRbcDelay(),
                                synCoderConf.getBcn0Delay(),
                                synCoderConf.getDataTrgDelay(),
                                synCoderConf.getDataDaqDelay(),
                                synCoderConf.getPulserTimerTrgDelay(),
                                new Binary(synCoderConf.getInChannelsEna()));
                        synCoderConfigMap.put(synCoderConf.getId(), synCoderConfiguration);
                    }
                    
                    SynCoderConfInFlash synCoderConfInFlash = configurationDAO.getSynCoderConfInFlash(assignment.getChip().getId());
                    if(synCoderConfInFlash ==  null) {
                    	synCoderConfInFlash = new SynCoderConfInFlash(assignment.getChip().getId());
                    }
                    
                    int synCoderConfId = 0;
                	if(synCoderConfInFlash.getSynCoderConfId() != null) {
                		synCoderConfId = synCoderConfInFlash.getSynCoderConfId();
                	}
                    if(synCoderConfId != staticConfiguration.getId() ) {                    
                    	SynCoderConfInFlashInfoImplAxis confInFlashInfoImplAxis = new SynCoderConfInFlashInfoImplAxis(
                    			synCoderConfInFlash.getChipId(),
                    			synCoderConfId,
                    			synCoderConfInFlash.getBeginAddress(),
                    			synCoderConfInFlash.getEndAddress() );
                    	synCoderConfInFlashes.add(confInFlashInfoImplAxis);
                    }
                }
                else if (staticConfiguration instanceof PacConf) {
                    if (!pacConfigMap.containsKey(staticConfiguration
                            .getId())) {
                        PacConf pacConf = (PacConf) staticConfiguration;
                        PacConfigurationImplAxis pacConfiguration = new PacConfigurationImplAxis(
                                pacConf.getId(),
                                new Binary(pacConf.getRecMuxClkInv()),
                                new Binary(pacConf.getRecMuxClk90()),
                                new Binary(pacConf.getRecMuxRegAdd()),
                                pacConf.getRecMuxDelay().getValues(),
                                pacConf.getRecDataDelay().getValues(),
                                pacConf.getBxOfCoincidence(),
                                pacConf.getPacEna(),
                                pacConf.getPacConfigId());
                        pacConfigMap.put(pacConf.getId(), pacConfiguration);
                    }
                }
                else if (staticConfiguration instanceof TbGbSortConf) {
                    if (!tbGbSortConfigMap.containsKey(staticConfiguration
                            .getId())) {
                        TbGbSortConf tbGbSortConf = (TbGbSortConf) staticConfiguration;
                        TbGbSortConfigurationImplAxis tbGbSortConfiguration = new TbGbSortConfigurationImplAxis(
                                tbGbSortConf.getId(),
                                new Binary(tbGbSortConf.getRecMuxClkInv()),
                                new Binary(tbGbSortConf.getRecMuxClk90()),
                                new Binary(tbGbSortConf.getRecMuxRegAdd()),
                                tbGbSortConf.getRecMuxDelay().getValues(),
                                tbGbSortConf.getRecDataDelay().getValues());
                        tbGbSortConfigMap.put(tbGbSortConf.getId(), tbGbSortConfiguration);
                    }
                }
                else if (staticConfiguration instanceof RmbConf) {
                    if (!rmbConfigMap.containsKey(staticConfiguration
                            .getId())) {
                        RmbConf rmbConf = (RmbConf) staticConfiguration;
                        RmbConfigurationImplAxis rmbConfiguration = new RmbConfigurationImplAxis(
                                rmbConf.getId(),
                                new Binary(rmbConf.getRecMuxClkInv()),
                                new Binary(rmbConf.getRecMuxClk90()),
                                new Binary(rmbConf.getRecMuxRegAdd()),
                                rmbConf.getRecMuxDelay().getValues(),
                                rmbConf.getRecDataDelay().getValues(),
                                rmbConf.getChanEna(),
                                rmbConf.getPreTriggerVal(),
                                rmbConf.getPostTriggerVal(),
                                rmbConf.getDataDelay(),
                                rmbConf.getTrgDelay());
                        rmbConfigMap.put(rmbConf.getId(), rmbConfiguration);
                    }
                }
                else if (staticConfiguration instanceof TcSortConf) {
                    if (!tbGbSortConfigMap.containsKey(staticConfiguration
                            .getId())) {
                        TcSortConf tcSortConf = (TcSortConf) staticConfiguration;
                        TcSortConfigurationImplAxis tcSortConfiguration = new TcSortConfigurationImplAxis(
                                tcSortConf.getId(),
                                new Binary(tcSortConf.getRecMuxClkInv()),
                                new Binary(tcSortConf.getRecMuxClk90()),
                                new Binary(tcSortConf.getRecMuxRegAdd()),
                                tcSortConf.getRecMuxDelay().getValues(),
                                tcSortConf.getRecDataDelay().getValues(),
                                tcSortConf.getBootScript());
                        tcSortConfigMap.put(tcSortConf.getId(), tcSortConfiguration);
                    }
                } 
                else if (staticConfiguration instanceof HalfSortConf) {
                    if (!halfSortConfigMap.containsKey(staticConfiguration
                            .getId())) {
                        HalfSortConf hsbConf = (HalfSortConf) staticConfiguration;
                        HalfSortConfigurationImplAxis hsbConfiguration = new HalfSortConfigurationImplAxis(
                                hsbConf.getId(),
                                new Binary(hsbConf.getRecChanEna()),
                                hsbConf.getRecFastClkInv().getValues(),
                                hsbConf.getRecFastClk90().getValues(),
                                hsbConf.getRecFastRegAdd().getValues(),
                                new Binary(hsbConf.getRecFastDataDelay()),
                                new Binary(hsbConf.getRecDataDelay()));
                        halfSortConfigMap.put(hsbConf.getId(), hsbConfiguration);
                    }
                }
                else if (staticConfiguration instanceof RbcConf) {
                	if (!rbcConfigMap.containsKey(staticConfiguration
                			.getId())) {
                		RbcConf rbcConf = (RbcConf) staticConfiguration;
                		RbcConfigurationImplAxis rbcConfiguration = new RbcConfigurationImplAxis(
                				rbcConf.getId(),
                				rbcConf.getConfig(),
                				rbcConf.getConfigIn(),
                				rbcConf.getConfigVer(),
                				rbcConf.getMajority(),
                				rbcConf.getShape(),
                				rbcConf.getMask(),
                				rbcConf.getCtrl());
                		rbcConfigMap.put(rbcConf.getId(), rbcConfiguration);
                	}
                }
                else if (staticConfiguration instanceof TtuTrigConf) {
                	if (!ttuTrigConfigMap.containsKey(staticConfiguration
                			.getId())) {
                		log.info("Chip: " + assignment.getChip());
                		TtuTrigConf ttuTrigConf = (TtuTrigConf) staticConfiguration;
                		//* ND, 2010-01-20
                		//* Calculate connectedRbcsMask
                		IntArray24 connectedRbcsMask = TtuBoard.getConnectedRbcsMask(assignment.getChip(), ttuRbcConfigKey, configurationManager, equipmentDAO);
                		
                		TtuTrigConfigurationImplAxis ttuTrigConfiguration = new TtuTrigConfigurationImplAxis(
                				ttuTrigConf.getId(),
                				new Binary(ttuTrigConf.getRecMuxClk90()),
                				new Binary(ttuTrigConf.getRecMuxClkInv()),
                				new Binary(ttuTrigConf.getRecMuxRegAdd()),
                				ttuTrigConf.getRecMuxDelay().getValues(),
                				ttuTrigConf.getRecDataDelay().getValues(),
                				ttuTrigConf.getTaTrgDelay(),
                				ttuTrigConf.getTaTrgToff(),
                				ttuTrigConf.getTaTrgSelect(),
                				ttuTrigConf.getTaTrgCntrGate(),
                				ttuTrigConf.getTaSectorDelay().getValues(),
                				ttuTrigConf.getTaForceLogic0().getValues(),
                				ttuTrigConf.getTaForceLogic1().getValues(),
                				ttuTrigConf.getTaForceLogic2().getValues(),
                				ttuTrigConf.getTaForceLogic3().getValues(),
                				ttuTrigConf.getTrigConfig().getValues(),
                				ttuTrigConf.getTrigMask().getValues(),
                				ttuTrigConf.getTrigForce().getValues(),
                				ttuTrigConf.getTrigMajority(),
                				ttuTrigConf.getSectorMajority().getValues(),
                				ttuTrigConf.getSectorTrigMask(),
                				ttuTrigConf.getSectorThreshold(),
                				ttuTrigConf.getTowerThreshold(),
                				ttuTrigConf.getWheelThreshold(),
                				ttuTrigConf.getSectTrgSel(),
                				ttuTrigConf.getTaConfig2().getValues(),
                				connectedRbcsMask.getValues());
                		ttuTrigConfigMap.put(ttuTrigConf.getId(), ttuTrigConfiguration);
                	}
                }
                else if (staticConfiguration instanceof TtuFinalConf) {
                	if (!ttuFinalConfigMap.containsKey(staticConfiguration
                			.getId())) {
                		TtuFinalConf ttuFinalConf = (TtuFinalConf) staticConfiguration;
                		TtuFinalConfigurationImplAxis ttuFinalConfiguration = new TtuFinalConfigurationImplAxis(
                				ttuFinalConf.getId(),
                				ttuFinalConf.getTrigConfig().getValues(),
                				ttuFinalConf.getTrigConfig2().getValues(),
                				ttuFinalConf.getMaskTtu(),
                				ttuFinalConf.getMaskPointing(),
                				ttuFinalConf.getMaskBlast(),
                				ttuFinalConf.getDelayTtu(),
                				ttuFinalConf.getDelayPointing(),
                				ttuFinalConf.getDelayBlast(),
                				ttuFinalConf.getShapeTtu(),
                				ttuFinalConf.getShapePointing(),
                				ttuFinalConf.getShapeBlast());
                		ttuFinalConfigMap.put(ttuFinalConf.getId(), ttuFinalConfiguration);
                	}
                }
                else if (staticConfiguration instanceof FebChipConf) {
                	if (!febConfigMap.containsKey(staticConfiguration
                			.getId())) {
                		FebChipConf febChipConf = (FebChipConf) staticConfiguration;
                		FebChipConfigurationImplAxis febChipConfAxis = new FebChipConfigurationImplAxis(
                				febChipConf.getId(),
                				febChipConf.getVth(),
                				febChipConf.getVthOffset(),
                				febChipConf.getVmon(),
                				febChipConf.getVmonOffset() );
                		
                		febConfigMap.put(febChipConf.getId(), febChipConfAxis);                		                	
                	}                	
                }
                else {
                    throw new IllegalArgumentException(
                            "Unsupported staticConfiguration type " + staticConfiguration.getClass());
                }                
                chipConfiAssignList.add(new ChipConfigurationAssignmentImplAxis(
                        assignment.getChip().getId(), assignment.getStaticConfiguration().getId()));                
            }
            
            
            
            // OPTO chips confgurations are retrieved from the DB in a different way - we must
            // retrieve connections to linkBoards. So check if in the are any chips,
            // which still do not have assigned configuration and if this is the case find OPTOs among them.            
            if (chipConfAssignments.size() != chipIds.size()) {
                int manualConfId = -1;
                //for (Integer id : chipIds) {
                //    Chip chip = (Chip) equipmentDAO.getObject(Chip.class, id);
                                
                for (Chip chip : equipmentDAO.getChips(chipIds, false, false)) {
                    if (chip.getType() == ChipType.OPTO) {
                    	//Board b = chip.getBoard();
                        //TriggerBoard tb = (TriggerBoard) chip.getBoard();
                    	//log.info("Board b: " + b + " , Class: " + b.getClass());
                    	/*if it is done in the above way, the b is not TriggerBoard but Board, the only way to obtain TriggerBoard
                    	 * is  to ask for it explicitly
                    	 */
                    	TriggerBoard tb = (TriggerBoard)equipmentDAO.getObject(TriggerBoard.class, chip.getBoard().getId());
                        boolean enableLink0 = false;
                        boolean enableLink1 = false;
                        boolean enableLink2 = false;
                        int firstLink = (chip.getPosition() - Chip.OPTO_BASE_POS) * 3;
                        
                        /*for (LinkConn linkConn : equipmentDAO.getLinkConns(tb, true)) {                            
                            if (linkConn.getTriggerBoardInputNum() == firstLink) {
                                enableLink0 = true;
                            }
                            else if (linkConn.getTriggerBoardInputNum() == (firstLink + 1)) {
                                enableLink1 = true;
                            }
                            else if (linkConn.getTriggerBoardInputNum() == (firstLink + 2)) {
                                enableLink2 = true;
                            }
                        } */
                        for (Integer inputNum : equipmentDAO.getLinkConnsTriggerBoardInputNum(tb, true)) {                            
                            if (inputNum == firstLink) {
                                enableLink0 = true;
                            }
                            else if (inputNum == (firstLink + 1)) {
                                enableLink1 = true;
                            }
                            else if (inputNum == (firstLink + 2)) {
                                enableLink2 = true;
                            }
                        }
                        OptoConfigurationImplAxis optoConfiguration = new OptoConfigurationImplAxis(
                                manualConfId, enableLink0, enableLink1, enableLink2);
                        optoConfigMap.put(manualConfId, optoConfiguration);         
                        chipConfiAssignList.add(new ChipConfigurationAssignmentImplAxis(
                                chip.getId(), manualConfId));
                        manualConfId--;
                    }
                    if (chip.getType() == ChipType.TTUOPTO) {
                    	TtuBoard ttu = (TtuBoard)equipmentDAO.getObject(TtuBoard.class, chip.getBoard().getId());
                        boolean enableLink0 = false;
                        boolean enableLink1 = false;
                        boolean enableLink2 = false;
                        int firstLink = (chip.getPosition() - Chip.OPTO_BASE_POS) * 3;
                        
                        /*for (LinkConn linkConn : equipmentDAO.getLinkConns(tb, true)) {                            
                            if (linkConn.getTriggerBoardInputNum() == firstLink) {
                                enableLink0 = true;
                            }
                            else if (linkConn.getTriggerBoardInputNum() == (firstLink + 1)) {
                                enableLink1 = true;
                            }
                            else if (linkConn.getTriggerBoardInputNum() == (firstLink + 2)) {
                                enableLink2 = true;
                            }
                        } */
                        for (Integer inputNum : equipmentDAO.getRbcTtuConnsTtuBoardInputNum(ttu, true)) {                            
                            if (inputNum == firstLink) {
                                enableLink0 = true;
                            }
                            else if (inputNum == (firstLink + 1)) {
                                enableLink1 = true;
                            }
                            else if (inputNum == (firstLink + 2)) {
                                enableLink2 = true;
                            }
                        }
                        OptoConfigurationImplAxis optoConfiguration = new OptoConfigurationImplAxis(
                                manualConfId, enableLink0, enableLink1, enableLink2);
                        optoConfigMap.put(manualConfId, optoConfiguration);         
                        chipConfiAssignList.add(new ChipConfigurationAssignmentImplAxis(
                                chip.getId(), manualConfId));
                        manualConfId--;
                    }
                    /*else if (chip.getType() == ChipType.HALFSORTER) {
                        List<Crate> triggerCrates = equipmentDAO.getCratesByType(CrateType.TRIGGERCRATE, true);                        
                        String[] inputs = chip.getBoard().getName().equals("HSB_0") ? HSB0_INPUTS : HSB1_INPUTS;
                        byte enabledInputs = 0;
                        for (Crate crate : triggerCrates) {                            
                            int index = indexOf(inputs, crate.getName());
                            if (index == -1) {
                                //throw new RemoteException("Invalid trigger crate name " + crate.getName());
                            }
                            else {
                                enabledInputs |= (1 << index);
                            }
                        }                       
                        
                        HalfSortConfigurationImplAxis halfSortConfiguration = new HalfSortConfigurationImplAxis(
                                manualConfId, new Binary(new byte[] { enabledInputs }));
                        halfSortConfigMap.put(manualConfId, halfSortConfiguration);         
                        chipConfiAssignList.add(new ChipConfigurationAssignmentImplAxis(
                                chip.getId(), manualConfId));
                        manualConfId--;
                    }*/
                }                
            }
            //log.info(boards.size());
            log.info("finshed getConfigurationSet()");
            return new ConfigurationSetImplAxis(
            		combinedKeyId, timestamp,
                    chipConfiAssignList.toArray(new ChipConfigurationAssignment[] {}),
                    synCoderConfigMap.values().toArray(new SynCoderConfiguration[] {}),
                    optoConfigMap.values().toArray(new OptoConfiguration[] {}),
                    pacConfigMap.values().toArray(new PacConfiguration[] {}),
                    tbGbSortConfigMap.values().toArray(new TbGbSortConfiguration[] {}),
                    rmbConfigMap.values().toArray(new RmbConfiguration[] {}),
                    tcSortConfigMap.values().toArray(new TcSortConfiguration[] {}),
                    halfSortConfigMap.values().toArray(new HalfSortConfiguration[] {}),
                    rbcConfigMap.values().toArray(new RbcConfiguration[] {}),
                    ttuTrigConfigMap.values().toArray(new TtuTrigConfiguration[] {}),
                    ttuFinalConfigMap.values().toArray(new TtuFinalConfiguration[] {}),
                    febConfigMap.values().toArray(new FebChipConfiguration[] {}),
                    synCoderConfInFlashes.toArray(new SynCoderConfInFlashInfo[] {})
                    );
        } catch (Exception e) {
        	e.printStackTrace();
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                hibernateContext.closeSession();
            } catch (Exception e) {
                log.error(e);
            }
        }
    }
    
    public ConfigurationSet getConfigurationSetForLocalConfigKey(BigInteger[] chipIds,
            String localConfigKey) throws RemoteException {
        List<Integer> ids = new ArrayList<Integer>(chipIds.length);
        for (BigInteger bigInt : chipIds) {
            ids.add(bigInt.intValue());
        }
        
        return getConfigurationSetForLocalConfigKeyInt(ids, localConfigKey);
    }
    
    public ConfigurationSet getConfigurationSetForLocalConfigKeyInt(List<Integer> ids,
            String localConfigKey) throws RemoteException {
        try {        	
            /*System.out.println("getConfigurationSetForLocalConfigKey: localConfigKey " + localConfigKey);
            System.out.print("chip ids: ");
            for(Integer id : ids) {
            	System.out.print(id + " ");            	
            }
            System.out.print("\n");*/
        	log.info("started configurationDAO.getConfigSetInfoByChipIdsAndLocalConfigKey");
            ConfigSetInfo configSetInfo = configurationDAO.getConfigSetInfoByChipIdsAndLocalConfigKey(ids,
                            localConfigKey);   
            log.info("finshed configurationDAO.getConfigSetInfoByChipIdsAndLocalConfigKey");
            
            log.info("started configurationDAO.getChipConfAssignmentsByChipIdsAndLocalConfigKey");
            List<ChipConfAssignment> chipConfAssignments = new ArrayList<ChipConfAssignment>();
            chipConfAssignments.addAll(configurationDAO
                                       .getChipConfAssignmentsByChipIdsAndLocalConfigKey(ids,
                                                                                         localConfigKey)); 
            log.info("finshed configurationDAO.getChipConfAssignmentsByChipIdsAndLocalConfigKey");
            
            List<LocalConfigKey> localConfigKeys = new ArrayList<LocalConfigKey>();
            localConfigKeys.add(configurationDAO.getLocalConfigKeyByName(localConfigKey));
            if (configSetInfo == null) {
                return getConfigurationSet(localConfigKeys, 0, ids, chipConfAssignments);
            }
            return getConfigurationSet(localConfigKeys, (int) configSetInfo.getCreationDate().getTime(), ids, chipConfAssignments);
        } catch (Exception e) {
/*        	String str = new String();
        	for(StackTraceElement element : e.getStackTrace()) {
        		str += element.toString() + "\n";
        	}
        	log.error(str);*/
        	e.printStackTrace();
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                hibernateContext.closeSession();
            } catch (Exception e) {
                log.error(e);
            }
        }        
    }

/* the old version   
 * public ConfigurationSet getConfigurationSet(BigInteger[] chipIds,
            String globalConfigKey) throws RemoteException {

        try {
            List<Integer> ids = new ArrayList<Integer>(chipIds.length);
            for (BigInteger bigInt : chipIds) {
                ids.add(bigInt.intValue());
            }
            ConfigSetInfo configSetInfo = configurationDAO.getConfigSetInfoByChipIdsAndGlobalConfigKey(ids,
                            globalConfigKey); 
            
            List<ChipConfAssignment> chipConfAssignments = configurationDAO
                    .getChipConfAssignmentsByChipIdsAndGlobalConfigKey(ids,
                            globalConfigKey);            
            
            if (configSetInfo == null) {
                return getConfigurationSet(0, 0, ids, chipConfAssignments);
            }
            return getConfigurationSet(configSetInfo.getLocalConfigKeyId(), (int)configSetInfo.getCreationDate().getTime(), ids, chipConfAssignments);
        } catch (Exception e) {
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                hibernateContext.closeSession();
            } catch (Exception e) {
                log.error(e);
            }
        }

    }*/

    public ConfigurationSet getConfigurationSetForLocalConfigKeys(BigInteger[] chipIds,
    		List<String> localConfigKeys) throws RemoteException {

    	try {
    		List<Integer> ids = new ArrayList<Integer>(chipIds.length);
    		for (BigInteger bigInt : chipIds) {
    			ids.add(bigInt.intValue());
    		}

    		List<LocalConfigKey> configKeys = new ArrayList<LocalConfigKey>();
    		for(String localConfigKey : localConfigKeys) {
    			configKeys.add(configurationDAO.getLocalConfigKeyByName(localConfigKey));
    		}

    		return getConfigurationSetForLocalConfigKeys(ids, configKeys);
    	} 
    	catch (Exception e) {
    		log.error(e);
    		throw new RemoteException(e.getMessage(), e);
    	} finally {
    		try {
    			hibernateContext.closeSession();
    		} catch (Exception e) {
    			log.error(e);
    		}
    	}
    }
    
    public ConfigurationSet getConfigurationSetForLocalConfigKeys(List<Integer> allIds,
    		List<LocalConfigKey> localConfigKeys) throws RemoteException {    	
    	try {              	
    		//dividing keys by subsystem
    		Map<LocalConfigKey.Subsystem, List<Integer>> chipsBySubsystem = new HashMap<LocalConfigKey.Subsystem, List<Integer> >();
    		log.info("getConfigurationSetForLocalConfigKeys(): started getChips");
    		List<Chip> chips = equipmentDAO.getChips(allIds, false, false);
    		log.info("getConfigurationSetForLocalConfigKeys(): finshed getChips");
    		for(Chip chip : chips) {
    			List<Integer> ids = chipsBySubsystem.get(LocalConfigKey.Subsystem.getSybsytem(chip.getType()));
    			if(!chipsBySubsystem.containsKey(LocalConfigKey.Subsystem.getSybsytem(chip.getType()))) {
    				ids = new ArrayList<Integer>();
    				chipsBySubsystem.put(LocalConfigKey.Subsystem.getSybsytem(chip.getType()), ids);
    			}

    			ids.add(chip.getId());
    		}

    		List<ChipConfAssignment> chipConfAssignments = new ArrayList<ChipConfAssignment>();
    		ConfigSetInfo configSetInfo = null;
    		long timestamp = 0;
    		for(LocalConfigKey localConfigKey : localConfigKeys) {
    			List<Integer> ids = chipsBySubsystem.get(localConfigKey.getSubsystem());

    			if(ids != null) {
    				log.info("getConfigurationSetForLocalConfigKeys(): started getConfigSetInfoByChipIdsAndLocalConfigKey");
    				configSetInfo = configurationDAO.getConfigSetInfoByChipIdsAndLocalConfigKey(ids,
    						localConfigKey.getName());   

    				chipConfAssignments.addAll(configurationDAO.getChipConfAssignmentsByChipIdsAndLocalConfigKey(ids,
    						localConfigKey.getName()));     
    				log.info("getConfigurationSetForLocalConfigKeys(): finshed getConfigSetInfoByChipIdsAndLocalConfigKey");
    				if(configSetInfo.getCreationDate().getTime() > timestamp) {
    					timestamp = configSetInfo.getCreationDate().getTime();
    				}
    			}    			
    		}

    		if (configSetInfo == null) {
    			return getConfigurationSet(localConfigKeys, 0, allIds, chipConfAssignments); 
    		}
    		return getConfigurationSet(localConfigKeys, (int)timestamp, allIds, chipConfAssignments);
    		
    	} catch (Exception e) {
    		log.error(e);
    		e.printStackTrace();
    		throw new RemoteException(e.getMessage(), e);
    	} finally {
    		try {
    			hibernateContext.closeSession();
    		} catch (Exception e) {
    			log.error(e);
    		}
    	}
    }
    
    public ConfigurationSet getConfigurationSet(BigInteger[] chipIds,
    		String globalConfigKey) throws RemoteException {
    	log.info("started getConfigurationSet(BigInteger[] chipIds, String globalConfigKey)" );
    	try {              	
    		List<Integer> allIds= new ArrayList<Integer>(chipIds.length);
    		for (BigInteger bigInt : chipIds) {
    			allIds.add(bigInt.intValue());
    		}
    		List<LocalConfigKey> localConfigKeys = configurationDAO.getLocalConfigKeysByGlobalConfigKey(globalConfigKey);
    		return getConfigurationSetForLocalConfigKeys(allIds, localConfigKeys);    		
    	} catch (Exception e) {
    		log.error(e);
    		throw new RemoteException(e.getMessage(), e);
    	} finally {
    		try {
    			hibernateContext.closeSession();
    		} catch (Exception e) {
    			log.error(e);
    		}
    	}

    }

    public String[] getLocalConfigKeys() throws RemoteException {
    	try {
    		List<LocalConfigKey> localConfigKeys = locationDAO.getObjects(LocalConfigKey.class);
    		String[] keys = new String[localConfigKeys.size()];
    		for (int i = 0; i < keys.length; i++) {
    			keys[i] = localConfigKeys.get(i).getName();
    		}
    		return keys;
    	} catch (Exception e) {
    		throw new RemoteException(e.getMessage(), e);
    	} finally {
    		try {
    			hibernateContext.closeSession();
    		} catch (Exception e) {
    			log.error(e);
    		}
    	}
    }

    public String[] getLocalConfigKeys(String subsytem) throws RemoteException {
    	try {    		
    		List<LocalConfigKey> localConfigKeys = 
    			configurationDAO.getLocalConfigKeysBySubsystem(LocalConfigKey.Subsystem.valueOf(subsytem));
    		String[] keys = new String[localConfigKeys.size()];
    		for (int i = 0; i < keys.length; i++) {
    				keys[i] = localConfigKeys.get(i).getName();
    		}
    		return keys;
    	} catch (Exception e) {
    		throw new RemoteException(e.getMessage(), e);
    	} finally {
    		try {
    			hibernateContext.closeSession();
    		} catch (Exception e) {
    			log.error(e);
    		}
    	}
    }
    
/* MichalS */
    public void writeCurrentKeyValue (String currentKeyValue) throws RemoteException {
      try {
        configurationDAO.writeCurrentKey(currentKeyValue);
      } catch (Exception e) {
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                hibernateContext.closeSession();
            } catch (Exception e) {
                log.error(e);
            }
        }
    }
/* MichalS */

    public BigInteger setStripResponseValues(/*BigInteger id,*/ BigInteger chamberStrip, BigInteger count, BigInteger run,
    		BigInteger unixTime) throws RemoteException {
		try{
	    	BigInteger response = BigInteger.ONE;
	    	response = conditionDAO.setStripResponse(chamberStrip.intValue(), count.intValue(), run.intValue(), unixTime.intValue());
    		System.out.println("Now I'm back");
	    	return response;
        } catch (Throwable e) {
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
            	condHibernateContext.closeSession();
            } catch (Exception e) {
                log.error(e);
            }
        }
    }

    public BigInteger setFebConditionVaules(/*Integer id,*/ BigInteger febLocation, BigInteger run, BigInteger th1,
    		BigInteger th2, BigInteger th3, BigInteger th4, BigInteger vmon1, BigInteger vmon2, BigInteger vmon3,
    		BigInteger vmon4, BigInteger unixtime) throws RemoteException {
    	try{
	    	BigInteger response = BigInteger.ONE;
	    	response = conditionDAO.setFebCondition(febLocation.intValue(), run.intValue(), th1.intValue(), th2.intValue(),
	    			th3.intValue(), th4.intValue(), vmon1.intValue(), vmon2.intValue(), vmon3.intValue(),
	    			vmon4.intValue(), unixtime.intValue());
	    	return response;
        } catch (Exception e) {
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
            	condHibernateContext.closeSession();
            } catch (Exception e) {
                log.error(e);
            }
        }
    }
    
    public FebChipInfo[] getFebChipInfoVector() throws RemoteException {
    	log.info("executing DbServiceImplAxis.getFebChipInfoVector()");
    	List<FebChipInfo> resultList = new ArrayList<FebChipInfo>();
        try {
        	List<FebBoard> febBoards = equipmentDAO.getFebBoards();
        	//List<FebBoard> febBoards = equipmentDAO.getObjects(FebBoard.class);
        	log.info("DbServiceImplAxis.getFebChipInfoVector() - febBoards list taken, size: " + febBoards.size() + " febBoards");
        	for (FebBoard febBoard : febBoards) {        		
        		int barrelOrEndcap = febBoard.getFebLocation().getChamberLocation().getBarrelOrEndcap() == BarrelOrEndcap.Barrel ? 0 : 1;
        		int diskOrWheel = febBoard.getFebLocation().getChamberLocation().getDiskOrWheel();
        		int layer = febBoard.getFebLocation().getChamberLocation().getLayer();
        		int sector = febBoard.getFebLocation().getChamberLocation().getSector();
        		String subsector = febBoard.getFebLocation().getChamberLocation().getSubsector();
        		if(subsector == null)
        			subsector = "";
        		
        		int febLocationId = febBoard.getFebLocation().getId();        		
        		String febLocalEtaPartition = febBoard.getFebLocation().getFebLocalEtaPartition();
        		if(febLocalEtaPartition == null)
        			throw new RuntimeException("febLocalEtaPartition is null ");
        		
        		int cbChannel = febBoard.getI2cCbChannel().getCbChannel();
        		
        		int i2cLocalNumber = febBoard.getI2cLocalNumber();
        		

        		for(Chip chip : febBoard.getChips()) {
        			int id =  chip.getId();
        			int position = chip.getPosition(); 
        			FebChipInfo febChipInfo = new  FebChipInfoImplAxis(id, febLocationId, barrelOrEndcap, 
        					cbChannel, diskOrWheel, febLocalEtaPartition, i2cLocalNumber, layer, position, 
        					sector, subsector);   
        			
        			resultList.add(febChipInfo);
        		}
        	}
        } catch (Exception e) {
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                hibernateContext.closeSession();
            } catch (Exception e) {
                log.error(e);
            }
        }
        log.info("DbServiceImplAxis.getFebChipInfoVector(): found " + resultList.size() + " FebChipInfo's");
        return resultList.toArray(new FebChipInfo[] {});
    }

    
    /**
     * does not work....
     * @param synCoderConfInFlashes
     * @return
     * @throws RemoteException
     */
    public BigInteger putSynCoderConfInFlashes(SynCoderConfInFlashInfoImplAxis[] synCoderConfInFlashes ) throws RemoteException {
    	try {    
    		log.info("starting putSynCoderConfInFlashes(): synCoderConfInFlashes.length = " + synCoderConfInFlashes.length );
    		for(SynCoderConfInFlashInfoImplAxis synCoderConfInFlashAixs : synCoderConfInFlashes) {
    			SynCoderConfInFlash synCoderConfInFlash = new SynCoderConfInFlash (
    					synCoderConfInFlashAixs.getChipId(),
    					synCoderConfInFlashAixs.getSynCoderConfId(),
    					synCoderConfInFlashAixs.getBeginAddress(),
    					synCoderConfInFlashAixs.getEndAddress() );
    			configurationDAO.saveObject(synCoderConfInFlash);    			
    		}
    		BigInteger response = BigInteger.ZERO;
    		return response;
    	} catch (Exception e) {
    		throw new RemoteException(e.getMessage(), e);
    	} finally {
    		try {    			
    			hibernateContext.closeSession();
    		} catch (Exception e) {
    			log.error(e);
    		}
    	}
    }
    
    /**
     * does not worl as well
     * @param synCoderConfInFlash
     * @return
     * @throws RemoteException
     */
    public BigInteger putSynCoderConfInFlash(SynCoderConfInFlashInfoImplAxis synCoderConfInFlash) throws RemoteException {
    	try {    
    		log.info("starting putSynCoderConfInFlash()");
    		SynCoderConfInFlash synCoderConfInFlashDB = new SynCoderConfInFlash (
    				synCoderConfInFlash.getChipId(),
    				synCoderConfInFlash.getSynCoderConfId(),
    				synCoderConfInFlash.getBeginAddress(),
    				synCoderConfInFlash.getEndAddress() );
    		configurationDAO.saveObject(synCoderConfInFlashDB);

    		log.info("finshed putSynCoderConfInFlash()");
    		BigInteger response = BigInteger.ZERO;
    		return response;
    	} catch (Exception e) {
    		throw new RemoteException(e.getMessage(), e);
    	} finally {
    		try {    			
    			hibernateContext.closeSession();
    		} catch (Exception e) {
    			log.error(e);
    		}
    	}
    }
    
    public BigInteger putSynCoderConfInFlash(BigInteger chipId, BigInteger confId, BigInteger begAddr,BigInteger endAddr) throws RemoteException {
    	try {    
    		log.info("starting putSynCoderConfInFlash(int chipId, int confId,int begAddr,int endAddr)");
    		SynCoderConfInFlash synCoderConfInFlashDB = new SynCoderConfInFlash (
    				chipId.intValue(), confId.intValue(), begAddr.intValue(), endAddr.intValue() );
    		configurationDAO.saveObject(synCoderConfInFlashDB);

    		log.info("finshed putSynCoderConfInFlash()");
    		
    		BigInteger response = BigInteger.ZERO;
    		return response;
    	} catch (Exception e) {
    		throw new RemoteException(e.getMessage(), e);
    	} finally {
    		try {    			
    			hibernateContext.closeSession();
    		} catch (Exception e) {
    			log.error(e);
    		}
    	}
    }
    
    public void synchronizeLinks() throws RemoteException {
        /*MessageContext context = MessageContext.getCurrentContext();
        ServletContext servletContext = ((HttpServlet)context.getProperty(HTTPConstants.MC_HTTP_SERVLET)).getServletContext();
        OutputStream s;
        try {
            s = new FileOutputStream(servletContext.getRealPath("synchronizeLinks.log"));
        } catch (FileNotFoundException e) {
            log.error(e);
            throw new RemoteException("Error in synchronizeLinks", e);
        }
        
        try {
            log.warn("synchronizeLinks enter"); 
            if (dbMapper == null) {
                dbMapper = new HardwareDbMapper(new HardwareRegistry(), hibernateContext);
            };            
            OptLinksSynchronizer.synchronize(equipmentDAO, dbMapper, s);
        } catch (SynchronizationException e) {
            log.error(e);
            throw new RemoteException("Error in synchronizeLinks", e);
        } catch (JAXBException e) {
            log.error(e);
            throw new RemoteException("Error in synchronizeLinks", e);
        }
        finally {
            try {
                s.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        log.warn("synchronizeLinks exit");*/
    }
}
