package test;

import java.math.BigInteger;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import rpct.datastream.LMuxBxDataImplXml;
import rpct.datastream.ReadoutItemId;
import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigParamDAO;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.CrateType;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.db.service.CcuRingInfo;
import rpct.db.service.ChipConfigurationAssignment;
import rpct.db.service.ConfigurationSet;
import rpct.db.service.FebChipInfo;
import rpct.db.service.VmeCrateInfo;
import rpct.db.service.axis.CcuRingInfoImplAxis;
import rpct.db.service.axis.ConfigurationSetImplAxis;
import rpct.db.service.axis.DbServiceImplAxis;
import rpct.db.service.axis.SynCoderConfInFlashInfoImplAxis;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;

public class test1 {
	private static DbServiceImplAxis dbServiceImplAxis;
	
	private static EquipmentDAO equipmentDAO;
	private static ConfigurationDAO configurationDAO;
	private static ConfigurationManager configurationManager;
	
	private final static Log log = LogFactory.getLog(test1.class);
	 
	public static void main(String[] args) throws DataAccessException {
		PropertyConfigurator.configure("log4j.properties");

		HibernateContext context = new SimpleHibernateContextImpl();
		try {
			 dbServiceImplAxis = new DbServiceImplAxis();
		
			equipmentDAO = new EquipmentDAOHibernate(context);
			configurationDAO = new ConfigurationDAOHibernate(context);
			configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);
			
			long start = System.currentTimeMillis();
			
			Map<BoardType, Integer> boardsByTypeCnt = new HashMap<BoardType, Integer>();
			
			
			List<Crate> crates = equipmentDAO.getCratesByType(CrateType.LINKBOX, true);
			System.out.println("crates.size() " + crates.size());			
			for(Crate crate : crates) {
				for(Board board : crate.getBoards()) {
					if(boardsByTypeCnt.containsKey(board.getType())) {
						Integer cnt = boardsByTypeCnt.get(board.getType());
						cnt=cnt+1;
						boardsByTypeCnt.put(board.getType(), cnt);
					}
					else {
						boardsByTypeCnt.put(board.getType(), 0);
					}
				}
				//break;
			}
			
			
/*			List<Board> boards = equipmentDAO.getBoardsByType(BoardType.LINKBOARD, true);
			
			boards.addAll(equipmentDAO.getBoardsByType(BoardType.RBCBOARD, true));
			boards.addAll(equipmentDAO.getBoardsByType(BoardType.RBCFAKEBOARD, true));
			for(Board board : boards) {
				if(boardsByTypeCnt.containsKey(board.getType())) {
					Integer cnt = boardsByTypeCnt.get(board.getType());
					cnt=cnt+1;
					boardsByTypeCnt.put(board.getType(), cnt);
				}
				else {
					boardsByTypeCnt.put(board.getType(), 0);
				}
			}*/
			
			System.out.println("boardsByTypeCnt ");
			for(Map.Entry<BoardType, Integer>  es : boardsByTypeCnt.entrySet()) {
				System.out.println(es.getKey() + " " + es.getValue());
			}
			
	    	System.out.println("operaton ended after " + (System.currentTimeMillis() - start)/1000. + " s");

/*	    	start = System.currentTimeMillis();
	    	testFebsConfigs();
	    	System.out.println("operaton ended after " + (System.currentTimeMillis() - start)/1000. + " s");*/
			
		}/* catch (RemoteException e) {
			e.printStackTrace();
			context.rollback();			
		}*//* catch (DataAccessException e) {
			context.rollback();
			e.printStackTrace();
		} */
		catch (Exception e) {
			context.rollback();
			e.printStackTrace();
		} 
		finally {
            context.closeSession();
        }

	}

}
