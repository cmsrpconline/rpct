package test;

import java.math.BigInteger;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import rpct.db.DataAccessException;
import rpct.db.domain.configuration.ConfigParamDAO;
import rpct.db.domain.configuration.ConfigurationDAO;
import rpct.db.domain.configuration.ConfigurationDAOHibernate;
import rpct.db.domain.configuration.ConfigurationManager;
import rpct.db.domain.configuration.LocalConfigKey;
import rpct.db.domain.equipment.Board;
import rpct.db.domain.equipment.BoardType;
import rpct.db.domain.equipment.Chip;
import rpct.db.domain.equipment.ChipType;
import rpct.db.domain.equipment.Crate;
import rpct.db.domain.equipment.EquipmentDAO;
import rpct.db.domain.equipment.EquipmentDAOHibernate;
import rpct.db.domain.equipment.FebBoard;
import rpct.db.domain.equipment.LinkBox;
import rpct.db.domain.hibernate.HibernateContext;
import rpct.db.domain.hibernate.context.simple.SimpleHibernateContextImpl;
import rpct.db.service.CcuRingInfo;
import rpct.db.service.ChipConfigurationAssignment;
import rpct.db.service.ConfigurationSet;
import rpct.db.service.FebChipInfo;
import rpct.db.service.VmeCrateInfo;
import rpct.db.service.axis.CcuRingInfoImplAxis;
import rpct.db.service.axis.ConfigurationSetImplAxis;
import rpct.db.service.axis.DbServiceImplAxis;
import rpct.db.service.axis.SynCoderConfInFlashInfoImplAxis;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;

public class test {
	private static DbServiceImplAxis dbServiceImplAxis;
	
	private static EquipmentDAO equipmentDAO;
	private static ConfigurationDAO configurationDAO;
	private static ConfigurationManager configurationManager;
	
	static void testConfigKeys(DbServiceImplAxis dbServiceImplAxis, EquipmentDAO equipmentDAO) throws DataAccessException, RemoteException {
		/*List<Chip> chips = new ArrayList<Chip>();
		for(Board board : equipmentDAO.getCrateByName("PASTEURA_TC").getBoards() ) {
			chips.addAll(board.getChips());
		}


		BigInteger[] chipIds = new BigInteger[chips.size()];
		for(int i = 0; i < chips.size(); i++) {
			chipIds[i] = new BigInteger( String.valueOf(chips.get(i).getId()));
		}


		ConfigurationSetImplAxis configurationSet = (ConfigurationSetImplAxis)dbServiceImplAxis.getConfigurationSet(chipIds, "DEFAULT");
		System.out.println(configurationSet.getChipConfigurationAssignments().length);
		System.out.println("getTimestamp " + configurationSet.getTimestamp());
		System.out.println("getLocalConfigKeyId " + configurationSet.getLocalConfigKeyId());
		for(ChipConfigurationAssignment assignment : configurationSet.getChipConfigurationAssignments()) {
			Chip chip = equipmentDAO.getChipById(assignment.getChipId());
			System.out.println(chip.getBoard() + " " + chip.getType());
		}*/
		
		List<Chip> chips = new ArrayList<Chip>();
		for(Board board : equipmentDAO.getCrateByName("PASTEURA_LBB").getBoards() ) {
			chips.addAll(board.getChips());
		}


		BigInteger[] chipIds = new BigInteger[chips.size()];
		for(int i = 0; i < chips.size(); i++) {
			chipIds[i] = new BigInteger( String.valueOf(chips.get(i).getId()));
		}

		List<String> localConfigKeys = new ArrayList<String>();
		localConfigKeys.add(ConfigurationManager.LOCAL_CONF_KEY_LBS_DEFAULT);
		ConfigurationSetImplAxis configurationSet = (ConfigurationSetImplAxis)dbServiceImplAxis.getConfigurationSetForLocalConfigKeys(chipIds, localConfigKeys);
		System.out.println(configurationSet.getChipConfigurationAssignments().length);
		System.out.println("getTimestamp " + configurationSet.getTimestamp());
		System.out.println("getLocalConfigKeyId " + configurationSet.getLocalConfigKeyId());
		for(ChipConfigurationAssignment assignment : configurationSet.getChipConfigurationAssignments()) {
			Chip chip = equipmentDAO.getChipById(assignment.getChipId());
			System.out.println(chip.getBoard() + " " + chip.getType());
		}
		
/*			
		System.out.println("dbServiceImplAxis.getLocalConfigKeys(\"LBS\") : ");
		for(String localConfigKey : dbServiceImplAxis.getLocalConfigKeys("LBS")) {
			System.out.println(localConfigKey);
		}*/
	}
	
	static void testFebsConfigs() throws DataAccessException, RemoteException {
		int j =0;
		List<Integer> ids = new ArrayList<Integer>();
    	//for(Board board : equipmentDAO.getCrateByName("LBB_904").getBoards()) {
		/*for(Board board : equipmentDAO.getBoardsByType(BoardType.FEBBOARD, true)) {
    		if(board.getType() == BoardType.FEBBOARD) {
    			System.out.println(board);
    			for(Chip chip : board.getChips()) {
    				ids.add(chip.getId());
    			}
    		}
    		j++;
    		if(j > 100)
    			break;
    	}*/
    	
		log.info("equipmentDAO.getChipsByType start");
		for(Chip chip : equipmentDAO.getChipsByType(ChipType.FEBCHIP, false)) {
			ids.add(chip.getId());
			j++;
    		if(j >= 1000)
    			break;
		}
		
		log.info("equipmentDAO.getChipsByType done");
		
    	BigInteger[] chipIds = new BigInteger[ids.size()];
    	for (int i = 0; i < chipIds.length; i++) {
            chipIds[i] = BigInteger.valueOf(ids.get(i));            
        }
    	
    	ConfigurationSet configurationSet = dbServiceImplAxis.getConfigurationSetForLocalConfigKey(chipIds, ConfigurationManager.LOCAL_CONF_KEY_FEBS_DEFAULT);
    	    	
    	
/*    	for(ChipStatus chipStatus : configurationSet.getChipStatuses()) {
    		System.out.println(chipStatus.getChipId() + " " + chipStatus.getStatus());    		
    	}*/
    	
    	log.info("configurationSet.getFebChipConfigurations().length " + configurationSet.getFebChipConfigurations().length);
    	log.info("configurationSet.getChipConfigurationAssignments().length " + configurationSet.getChipConfigurationAssignments().length);
    	log.info("configurationSet.getFebChipConfigurations().length " + configurationSet.getFebChipConfigurations().length);
    	//log.info("configurationSet.getChipStatuses()).length " + configurationSet.getChipStatuses().length);
	}
	
	static void testTTUsConfigs(String configKey) throws DataAccessException, RemoteException {
		List<Integer> ids = new ArrayList<Integer>();
		System.out.println("adding chips to configure");
    	for(Board board : equipmentDAO.getCrateByName("SC").getBoards()) {
    		if(board.getType() == BoardType.TTUBOARD) {
    			System.out.println(board);
    			for(Chip chip : board.getChips()) {
    				ids.add(chip.getId());
    				System.out.println(chip);
    			}
    		}
    	}
    	System.out.println("count of the TTU chips " + ids.size());
    	BigInteger[] chipIds = new BigInteger[ids.size()];
    	for (int i = 0; i < chipIds.length; i++) {
            chipIds[i] = BigInteger.valueOf(ids.get(i));
            
        }
    	ConfigurationSet configurationSet = dbServiceImplAxis.getConfigurationSetForLocalConfigKey(chipIds, configKey);
    	System.out.println(configurationSet.toString());
	}
	
	static void testTCConfigs(String configKey, String tcName) throws DataAccessException, RemoteException {
		List<Integer> ids = new ArrayList<Integer>();
    	/*for(Board board : equipmentDAO.getCrateByName(tcName).getBoards()) {
    		//if(board.getType() == BoardType.TTUBOARD) 
    		{
    			System.out.println(board);
    			for(Chip chip : board.getChips()) {
    				ids.add(chip.getId());
    			}
    		}
    	}*/
		
		ids.add(2720);
		ids.add(2727);
		ids.add(2728);
		ids.add(2718);
		ids.add(2500);
    	
    	BigInteger[] chipIds = new BigInteger[ids.size()];
    	for (int i = 0; i < chipIds.length; i++) {
            chipIds[i] = BigInteger.valueOf(ids.get(i));
            
        }
    	//ConfigurationSet configurationSet = dbServiceImplAxis.getConfigurationSetForLocalConfigKey(chipIds, configKey);
    	ConfigurationSet configurationSet = dbServiceImplAxis.getConfigurationSet(chipIds, configKey);
    	System.out.println(configurationSet.toString());
	}
	
	
	static void testLBConfigs(String configKey) throws DataAccessException, RemoteException {
		List<Integer> chipIds =  new ArrayList<Integer>();
		chipIds.add(2570);
		ConfigurationSet configurationSet = dbServiceImplAxis.getConfigurationSetForLocalConfigKeyInt(chipIds, configKey);
		System.out.println(configurationSet.toString());
	}
	
	static void testLBConfigsForLbox(String configKey, String lboxName) throws DataAccessException, RemoteException {
		Crate crate = equipmentDAO.getCrateByName(lboxName);
		List<Chip> chips = equipmentDAO.getChipsByTypeAndCrate(ChipType.SYNCODER, crate, true);
		List<Integer> chipIds =  new ArrayList<Integer>();
		for(Chip chip : chips) {
			chipIds.add(chip.getId());
		}
		ConfigurationSet configurationSet = dbServiceImplAxis.getConfigurationSetForLocalConfigKeyInt(chipIds, configKey);
		System.out.println(configurationSet.toString());
	}
	
	static void testLBConfigs(String configKey, String towerName) throws DataAccessException, RemoteException {
		List<Integer> chipIds = new ArrayList<Integer>();

		Map<String, List<LinkBox>> lboxesMap = equipmentDAO.getLinkBoxesByTower(equipmentDAO, true);
		for(LinkBox linkBox :  lboxesMap.get(towerName)) {
			for(Board board : linkBox.getBoards()) {
				if(board.getType() == BoardType.LINKBOARD) 
				{
					System.out.println(board);
					for(Chip chip : board.getChips()) {
						chipIds.add(chip.getId());
					}
				}
			}
		}
		
		ConfigurationSet configurationSet = dbServiceImplAxis.getConfigurationSetForLocalConfigKeyInt(chipIds, configKey);
		System.out.println(configurationSet.toString());
	}
	
	static void testFebChipInfos() throws RemoteException {
		FebChipInfo[] febChipInfos = dbServiceImplAxis.getFebChipInfoVector();
		for(FebChipInfo febChipInfo : febChipInfos) {
			//System.out.println(febChipInfo);
		}
	}
	
	static void testSynCoderConfInFlash() throws RemoteException {		
		SynCoderConfInFlashInfoImplAxis[] synCoderConfInFlashes = {
			new SynCoderConfInFlashInfoImplAxis(2570, 1980, 9, 10)
		};
		dbServiceImplAxis.putSynCoderConfInFlashes(synCoderConfInFlashes);
	}
	
	static void testTCinfo() throws RemoteException {	
		BigInteger port = new BigInteger("1973");
		BigInteger instance = new BigInteger("0");
		dbServiceImplAxis.getVmeCrateInfo("l1ts-rpc-01.cms", port, "rpcttsworker::TBCell", instance);
	}
	
	private final static Log log = LogFactory.getLog(test.class);
	 
	public static void main(String[] args) throws DataAccessException {
		PropertyConfigurator.configure("log4j.properties");

		HibernateContext context = new SimpleHibernateContextImpl();
		try {
			 dbServiceImplAxis = new DbServiceImplAxis();
		
			equipmentDAO = new EquipmentDAOHibernate(context);
			configurationDAO = new ConfigurationDAOHibernate(context);
			configurationManager = new ConfigurationManager(context, equipmentDAO, configurationDAO);
			
			long start = System.currentTimeMillis();
			
/*			CcuRingInfo ccuRingInfo = dbServiceImplAxis.getCcuRingInfoForXdaqApp("l1ts-rpc-03.cms", BigInteger.valueOf(1950), BigInteger.valueOf(0));
			System.out.println(ccuRingInfo.toString());*/
			
			testFebsConfigs();
			
			//testTTUsConfigs(ConfigurationManager.LOCAL_CONF_KEY_RBCS_TTUS_LHC1);
			
			//testTCConfigs("LHC8", "TC_0");
			
			//testLBConfigs(ConfigurationManager.LOCAL_CONF_KEY_LBS_DEFAULT);
			//testLBConfigsForLbox(ConfigurationManager.LOCAL_CONF_KEY_LBS_DEFAULT, "LBB_904");
			//testSynCoderConfInFlash();
			
/*			VmeCrateInfo vmeCrateInfo = dbServiceImplAxis.getVmeCrateInfo("l1ts-rpc-01.cms", BigInteger.valueOf(1973), "rpcttsworker::TBCell", BigInteger.valueOf(0));
			System.out.println(vmeCrateInfo.toString());*/
/*			
			VmeCrateInfo vmeCrateInfo1 = dbServiceImplAxis.getVmeCrateInfo("pccmsrpct2.cern.ch", BigInteger.valueOf(2984), "rpcttsworker::TBCell", BigInteger.valueOf(9));
			System.out.println(vmeCrateInfo1.toString());*/
			
			//configurationManager.setFebDtControlled(equipmentDAO.getBoardByName("test_904_0"), true);
			
			//System.out.println(configurationDAO.getFebDtControlled(equipmentDAO.getBoardByName("test_904_0"))); 
			//List<Integer> febDtControlledIds = configurationDAO.getFebDtControlledIds();
			//System.out.println(febDtControlledIds.toString());
			
			//testFebChipInfos();
			//System.out.println(" equipmentDAO.getObjects(FebBoard.class).size() " + equipmentDAO.getObjects(FebBoard.class).size());
			//testFebsConfigs();
			//Board board = equipmentDAO.getBoardById(4619);
			//System.out.println(board);
			//List<Board> boards = equipmentDAO.getBoardsByType(BoardType.FEBBOARD, false);
			//List<FebBoard> boards = equipmentDAO.getFebBoards();
			//System.out.println(((FebBoard) boards.get(0)).getBarrelOrEndcap());
			//equipmentDAO.getChipsByType(ChipType.FEBCHIP, false);
			
			//FebBoard board = equipmentDAO.getFebBoard(4619);
			//System.out.println("dupa");
			//System.out.println(board);
			//testTCinfo();
	    	System.out.println("operaton ended after " + (System.currentTimeMillis() - start)/1000. + " s");

/*	    	start = System.currentTimeMillis();
	    	testFebsConfigs();
	    	System.out.println("operaton ended after " + (System.currentTimeMillis() - start)/1000. + " s");*/
			
		}/* catch (RemoteException e) {
			e.printStackTrace();
			context.rollback();			
		}*//* catch (DataAccessException e) {
			context.rollback();
			e.printStackTrace();
		} */
		catch (Exception e) {
			context.rollback();
			e.printStackTrace();
		} 
		finally {
            context.closeSession();
        }

	}

}
